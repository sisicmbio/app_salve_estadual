package br.gov.icmbio

import groovy.sql.Sql

// http://grails-plugins.github.io/grails-quartz/ref/Triggers/cron.html
// http://plugins.grails.org/plugin/grails/quartz versao 2 não está utilizando esta
// http://grails-plugins.github.io/grails-quartz/guide/configuration.html
/*
http://www.quartz-scheduler.org/api/previous_versions/1.8.5/org/quartz/CronTrigger.html
http://www.quartz-scheduler.org/api/previous_versions/1.8.5/org/quartz/CronExpression.html
cronExpression: "s m h D M W Y"
                 | | | | | | `- Year [optional]
                 | | | | | `- Day of Week, 1-7 or SUN-SAT, ?
                 | | | | `- Month, 1-12 or JAN-DEC
                 | | | `- Day of Month, 1-31, ?
                 | | `- Hour, 0-23
                 | `- Minute, 0-59
                 `- Second, 0-59

Year is the only optional field and may be omitted, the rest are mandatory
Day-of-Week and Month are case insensitive, so "DEC" = "dec" = "Dec"
Either Day-of-Week or Day-of-Month must be "?", or you will get an error since support
by the underlying library is not complete. So you can't specify both fields, nor leave
both as the all values wildcard "*"; this is a departure from the unix crontab specification.
*/
class CheckVencConsultasJob {

    def dataSource
    def fichaService
    def logService
    static triggers = {

      // execute job once de 5 em 5 seconds
      //simple repeatInterval: 1000l

      // execute job once de 10 em 10 seconds
      // simple repeatInterval: 10000l

      // de 10 em 10 segundo 1 vez
      //simple name:'jobVencimentoConsultas', startDelay:1000, repeatInterval: 10000, repeatCount: 0

      // executar 1 vez as 01:00 da manha
      cron name:'checkVencConsultasJob', startDelay:10000, cronExpression: '1 0 0 * * ?' // todos dias as 00:00:01 da manha
    }

    def execute() {
        println ' '
        println 'job: checkVencConsultasJob iniciado as ' + new Date().format('dd/MM/yyyy HH:mm:ss')
        String sqlCmd
        CicloConsultaFicha.withNewSession {
            DadosApoio tbSituacaoFicha = DadosApoio.findByCodigo('TB_SITUACAO_FICHA')
            if (!tbSituacaoFicha) {
                println ' '
                println 'job: checkVencConsultasJob tabela TB_SITUACAO_FICHA nao encontrada'
                return
            }
            DadosApoio tbSituacaoColaboracao = DadosApoio.findByCodigo('TB_SITUACAO_COLABORACAO')
            if (!tbSituacaoColaboracao) {
                println ' '
                println 'job: checkVencConsultasJob tabela TB_SITUACAO_COLABORACAO nao encontrada'
                return
            }

            // situacao da ficha
            DadosApoio consulta = DadosApoio.findByPaiAndCodigo(tbSituacaoFicha, 'CONSULTA')
            DadosApoio consultaFinalizada = DadosApoio.findByPaiAndCodigo(tbSituacaoFicha, 'CONSULTA_FINALIZADA')
            DadosApoio avaliada = DadosApoio.findByPaiAndCodigo(tbSituacaoFicha, 'POS_OFICINA')
            DadosApoio avaliadaRevisao = DadosApoio.findByPaiAndCodigo(tbSituacaoFicha, 'AVALIADA_EM_REVISAO')
            DadosApoio validadaRevisao = DadosApoio.findByPaiAndCodigo(tbSituacaoFicha, 'VALIDADA_EM_REVISAO')
            DadosApoio validada = DadosApoio.findByPaiAndCodigo(tbSituacaoFicha, 'VALIDADA')
            // situacao da consulta
            DadosApoio finalizada = DadosApoio.findByPaiAndCodigo(tbSituacaoFicha, 'CONSULTA_FINALIZADA')
            DadosApoio consolidada = DadosApoio.findByPaiAndCodigo(tbSituacaoFicha, 'CONSOLIDADA')
            DadosApoio colaboracaoNaoAvaliada = DadosApoio.findByPaiAndCodigo(tbSituacaoColaboracao, 'NAO_AVALIADA')
            //DadosApoio resolverEmOficina = DadosApoio.findByPaiAndCodigo(tbSituacaoColaboracao, 'RESOLVER_OFICINA')
            if ( !validada ) {
                println 'Erro no job checkVencConsultasJob'
                println 'Situacao VALIDADA nao cadastrada na tabela DADOS_APOIO'
                println ' '
                return
            }
            if ( !validadaRevisao ) {
                println 'Erro no job checkVencConsultasJob'
                println 'Situacao VALIDADA_EM_REVISAO nao cadastrada na tabela DADOS_APOIO'
                println ' '
                return
            }
            if ( !avaliadaRevisao ) {
                println 'Erro no job checkVencConsultasJob'
                println 'Situacao AVALIADA_EM_REVISAO nao cadastrada na tabela DADOS_APOIO'
                println ' '
                return
            }
            if ( !consulta ) {
                println 'Erro no job checkVencConsultasJob'
                println 'Situacao CONSULTA nao cadastrada na tabela DADOS_APOIO'
                println ' '
                return
            }
            if ( !consultaFinalizada ) {
                println 'Erro no job checkVencConsultasJob'
                println 'Situacao CONSULTA_FINALIZADA nao cadastrada na tabela DADOS_APOIO'
                println ' '
                return
            }
            if ( !avaliada ) {
                println 'Erro no job checkVencConsultasJob'
                println 'Situacao POS_OFICINA nao cadastrada na tabela DADOS_APOIO'
                println ' '
                return
            }
            if (!finalizada || !consolidada || !avaliada) {
                println 'Erro no job checkVencConsultasJob'
                println 'Situacao CONSOLIDADA, CONSULTA ou CONSULTA_FINALIZADA nao cadastrada na tabela DADOS_APOIO'
                println ' '
                return
            }
            /*
            // ler as fichas que estão em consulta vencida
            sqlCmd = """SELECT tmp.sq_ficha,
                   tmp.cd_sistema,
                   tmp.cd_categoria,
                   tmp.cd_categoria_final,
                   max(tmp.dt_fim) AS dt_fim,
                   tmp.nu_pendencia,
                   sum(tmp.nu_colaboracao) AS nu_colaboracao
            FROM
              (SELECT a.sq_ficha,
                      d.cd_sistema,
                      b.dt_fim,
                      categoria.cd_sistema as cd_categoria,
                      categoria_final.cd_sistema as cd_categoria_final,
                      c.nu_pendencia,
                      sum(
                            (SELECT count(x.sq_ficha_colaboracao)
                             FROM salve.ficha_colaboracao x
                             WHERE x.sq_ciclo_consulta_ficha = a.sq_ciclo_consulta_ficha
                               AND sq_situacao IN ( ${ colaboracaoNaoAvaliada.id.toString()}) ) +
                            (SELECT count(y.sq_ficha_ocorrencia_consulta)
                             FROM salve.ficha_ocorrencia_consulta y, salve.ficha_ocorrencia z
                             WHERE y.sq_ficha_ocorrencia = z.sq_ficha_ocorrencia
                               AND y.sq_ciclo_consulta_ficha = a.sq_ciclo_consulta_ficha
                               AND z.sq_situacao in ( ${colaboracaoNaoAvaliada.id.toString()} ) ) ) AS nu_colaboracao
               FROM salve.ciclo_consulta_ficha a
               INNER JOIN salve.ciclo_consulta b ON b.sq_ciclo_consulta = a.sq_ciclo_consulta
               INNER JOIN salve.vw_ficha c ON c.sq_ficha = a.sq_ficha
               INNER JOIN salve.dados_apoio d ON d.sq_dados_apoio = c.sq_situacao_ficha
               left outer join salve.dados_apoio categoria on categoria.sq_dados_apoio = c.sq_categoria_iucn
               left outer join salve.dados_apoio categoria_final on categoria_final.sq_dados_apoio = c.sq_categoria_final
               WHERE d.cd_sistema IN ('CONSULTA', 'AVALIADA_EM_REVISAO')
               GROUP BY a.sq_ficha,
                        b.dt_fim,
                        d.cd_sistema,
                        categoria.cd_sistema,
                        categoria_final.cd_sistema,
                        c.nu_pendencia
                        ) tmp
               GROUP BY tmp.sq_ficha,
                     tmp.cd_sistema,
                     tmp.cd_categoria,
                     tmp.cd_categoria_final,
                     tmp.nu_pendencia
               HAVING max(tmp.dt_fim) < ${"'" + new Date().format('yyyy-MM-dd') + "'"}
               """
*/
            sqlCmd = """SELECT tmp.sq_ficha,
                   tmp.cd_sistema,
                   tmp.cd_categoria,
                   tmp.cd_categoria_final,
                   max(tmp.dt_fim) AS dt_fim,
                   pendencia.nu_pendencia,
                   sum(tmp.nu_colaboracao) AS nu_colaboracao
            FROM
              (SELECT a.sq_ficha,
                      d.cd_sistema,
                      b.dt_fim,
                      categoria.cd_sistema as cd_categoria,
                      categoria_final.cd_sistema as cd_categoria_final,
                      sum(
                            (SELECT count(x.sq_ficha_colaboracao)
                             FROM salve.ficha_colaboracao x
                             WHERE x.sq_ciclo_consulta_ficha = a.sq_ciclo_consulta_ficha
                               AND sq_situacao IN ( ${ colaboracaoNaoAvaliada.id.toString()}) ) +
                            (SELECT count(y.sq_ficha_ocorrencia_consulta)
                             FROM salve.ficha_ocorrencia_consulta y, salve.ficha_ocorrencia z
                             WHERE y.sq_ficha_ocorrencia = z.sq_ficha_ocorrencia
                               AND y.sq_ciclo_consulta_ficha = a.sq_ciclo_consulta_ficha
                               AND z.sq_situacao in ( ${colaboracaoNaoAvaliada.id.toString()} ) ) ) AS nu_colaboracao
               FROM salve.ciclo_consulta_ficha a
               INNER JOIN salve.ciclo_consulta b ON b.sq_ciclo_consulta = a.sq_ciclo_consulta
               INNER JOIN salve.ficha c ON c.sq_ficha = a.sq_ficha
               INNER JOIN salve.dados_apoio d ON d.sq_dados_apoio = c.sq_situacao_ficha
               left outer join salve.dados_apoio categoria on categoria.sq_dados_apoio = c.sq_categoria_iucn
               left outer join salve.dados_apoio categoria_final on categoria_final.sq_dados_apoio = c.sq_categoria_final
               WHERE d.cd_sistema IN ('CONSULTA', 'AVALIADA_EM_REVISAO')
               GROUP BY a.sq_ficha,
                        b.dt_fim,
                        d.cd_sistema,
                        categoria.cd_sistema,
                        categoria_final.cd_sistema
                        ) tmp
               left join lateral(
                   select count( ficha_pendencia.sq_ficha ) as nu_pendencia
                   from salve.ficha_pendencia
                   where ficha_pendencia.sq_ficha = tmp.sq_ficha
                   and ficha_pendencia.st_pendente = 'S'
                   and ficha_pendencia.de_assunto = 'Campos obrigatórios'
               ) as pendencia on true
               GROUP BY tmp.sq_ficha,
                     tmp.cd_sistema,
                     tmp.cd_categoria,
                     tmp.cd_categoria_final,
                     pendencia.nu_pendencia
               HAVING max(tmp.dt_fim) < ${"'" + new Date().format('yyyy-MM-dd' ) + "'"}
               """

            Map qryParams = [:]
            Sql sql = new Sql(dataSource)
            Map voltarPara = ['CONSULTA': [], 'CONSULTA_FINALIZADA': [], 'CONSOLIDADA': [], 'AVALIADA': [], 'AVALIADA_EM_REVISAO': [], 'VALIDADA': [], 'VALIDADA_EM_REVISAO': []]
            sql.eachRow(sqlCmd, qryParams) { row ->
                // se a ficha estiver em AVALIADA_EM_REVISAO ela deve sempre ir para CONSULTA_FINALIZADA
                if ( row.cd_sistema == 'AVALIADA_EM_REVISAO') {
                    voltarPara.CONSULTA_FINALIZADA.push( row.sq_ficha )
                } else if( row.cd_sistema =~ /^(CONSULTA|COMPILACAO)$/) {
                    if ( row.nu_colaboracao == 0 && row.nu_pendencia == 0 ) {
                            voltarPara.CONSOLIDADA.push( row.sq_ficha )
                        /*
                        // se ao final da consulta a ficha já possuir categorias é porque ja foi avaliada e/ou validada
                        if (row.cd_categoria_final && row.cd_categoria_final != 'NE') {
                            if( row.nu_pendencia == 0 ) {
                                voltarPara.VALIDADA.push(row.sq_ficha)
                            } else {
                                voltarPara.VALIDADA_EM_REVISAO.push(row.sq_ficha)
                            }
                        } else if (row.cd_categoria && row.cd_categoria != 'NE') {
                            if( row.nu_pendencia == 0 ) {
                                voltarPara.AVALIADA.push(row.sq_ficha)
                            } else {
                                voltarPara.AVALIADA_EM_REVISAO.push(row.sq_ficha)
                            }
                        } else {
                            if( row.nu_pendencia == 0 ) {
                                voltarPara.CONSOLIDADA.push(row.sq_ficha)
                            } else {
                                voltarPara.CONSULTA_FINALIZADA.push(row.sq_ficha)
                            }
                        }*/
                    } else {
						voltarPara.CONSULTA_FINALIZADA.push(row.sq_ficha)
                        /*if (row.cd_categoria_final && row.cd_categoria_final != 'NE') {
                            voltarPara.VALIDADA_EM_REVISAO.push(row.sq_ficha)
                        } else if (row.cd_categoria && row.cd_categoria != 'NE') {
                            voltarPara.AVALIADA_EM_REVISAO.push(row.sq_ficha)
                        } else {
                            voltarPara.CONSULTA_FINALIZADA.push(row.sq_ficha)
                        }*/
                    }
                }
            }

            logService.log('JOBS','CheckVencConsultasJob','SALVE-JOB','00000000000','0.0.0.0',voltarPara.toString())
            //println ' '
            if (voltarPara.CONSULTA) {
                Ficha.executeUpdate("update Ficha ficha set ficha.situacaoFicha = :situacao where ficha.id in(:ids)", [ids: voltarPara.CONSULTA, situacao: consulta])
                sql.executeUpdate("update salve.ficha set  sq_situacao = ${consulta.id.toString()} where ficha.id in( ${ voltarPara.CONSULTA.join(',') })")
            }
            if (voltarPara.CONSOLIDADA) {
                Ficha.executeUpdate("update Ficha ficha set ficha.situacaoFicha = :situacao where ficha.id in(:ids)", [ids: voltarPara.CONSOLIDADA, situacao: consolidada])
                // se a ficha for LC e passar para consolidade, o campo st_manter_lc da ficha deve ser marcado com true se tiver null
                fichaService.verificarStManterLc(voltarPara.CONSOLIDADA)
            }
            if (voltarPara.CONSULTA_FINALIZADA) {
                Ficha.executeUpdate("update Ficha ficha set ficha.situacaoFicha = :situacao where ficha.id in(:ids)", [ids: voltarPara.CONSULTA_FINALIZADA, situacao: consultaFinalizada])
            }
            if (voltarPara.AVALIADA) {
                Ficha.executeUpdate("update Ficha ficha set ficha.situacaoFicha = :situacao where ficha.id in(:ids)", [ids: voltarPara.AVALIADA, situacao: avaliada])
            }
            if (voltarPara.AVALIADA_EM_REVISAO) {
                Ficha.executeUpdate("update Ficha ficha set ficha.situacaoFicha = :situacao where ficha.id in(:ids)", [ids: voltarPara.AVALIADA_EM_REVISAO, situacao: avaliadaRevisao])
            }
            if (voltarPara.VALIDADA) {
                Ficha.executeUpdate("update Ficha ficha set ficha.situacaoFicha = :situacao where ficha.id in(:ids)", [ids: voltarPara.VALIDADA, situacao: validada])
            }
            if (voltarPara.VALIDADA_EM_REVISAO) {
                Ficha.executeUpdate("update Ficha ficha set ficha.situacaoFicha = :situacao where ficha.id in(:ids)", [ids: voltarPara.VALIDADA_EM_REVISAO, situacao: validadaRevisao])
            }
        } // fim atualizacao situacao fichas que sairam da consulta

        println 'job: checkVencConsultasJob finalizado com SUCESSO as ' + new Date().format('dd/MM/yyyy HH:mm:ss')
        println "-" * 100
        println ' '
    } // fim execute
}
