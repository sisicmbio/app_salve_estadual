package br.gov.icmbio
// http://grails-plugins.github.io/grails-quartz/ref/Triggers/cron.html
// http://plugins.grails.org/plugin/grails/quartz versao 2 não está utilizando esta
// http://grails-plugins.github.io/grails-quartz/guide/configuration.html
/*
http://www.quartz-scheduler.org/api/previous_versions/1.8.5/org/quartz/CronTrigger.html
http://www.quartz-scheduler.org/api/previous_versions/1.8.5/org/quartz/CronExpression.html
cronExpression: "s m h D M W Y"
                 | | | | | | `- Year [optional]
                 | | | | | `- Day of Week, 1-7 or SUN-SAT, ?
                 | | | | `- Month, 1-12 or JAN-DEC
                 | | | `- Day of Month, 1-31, ?
                 | | `- Hour, 0-23
                 | `- Minute, 0-59
                 `- Second, 0-59

Year is the only optional field and may be omitted, the rest are mandatory
Day-of-Week and Month are case insensitive, so "DEC" = "dec" = "Dec"
Either Day-of-Week or Day-of-Month must be "?", or you will get an error since support
by the underlying library is not complete. So you can't specify both fields, nor leave
both as the all values wildcard "*"; this is a departure from the unix crontab specification.
*/

class GarbageCollectJob {
    static triggers = {
      //simple repeatInterval: 30000l // execute job once in 30 seconds
      //simple name:'jobClearTempFile', startDelay:10000, repeatInterval: 10000//, repeatCount: 10
      //cron name:'cronClearTempFiles', startDelay:10000, cronExpression: '0 05 11 * * ?' // todos dias as 11:05 da manha
      //cron name:'cronClearTempFiles', startDelay:10000, cronExpression: '0 14 00 * * ?' // todos dias as 02:00 da manha
      // executar 1 vez por dia
      cron name:'cronGarbageCollect', startDelay:10000, cronExpression: '0 0 7 * * ?' // todos dias as 07:00 da manha
    }

    def execute() {
        println ' '
        println 'job: garbageCollect iniciado as ' + new Date().format('dd/MM/yyyy HH:mm:ss')
        try {
            long maxMemoryAntes = Runtime.getRuntime().maxMemory()
            long totalMemoryAntes = Runtime.getRuntime().totalMemory()
            long freeMemoryAntes = Runtime.getRuntime().freeMemory()
            /*println 'Informações da Memória ANTES da Limpeza'
            println 'Maxima..........: ' + (maxMemoryAntes/1024/1024).toInteger().toString() + ' MB'
            println 'Total...........: ' + (totalMemoryAntes/1024/1024).toInteger().toString() + ' MB'
            println 'Livre...........: ' + (freeMemoryAntes/1024/1024).toInteger().toString() + ' MB'
            println 'Utilizada.......: ' + ( (totalMemoryAntes - freeMemoryAntes)/1024/1024).toInteger().toString() + ' MB'
            println 'Processadores...: ' + Runtime.getRuntime().availableProcessors()*/
            //**************************************
            System.gc();
            Thread.sleep(200);
            System.runFinalization();
            Thread.sleep(200);
            System.gc();
            Thread.sleep(200);
            System.runFinalization();
            Thread.sleep(200);
            //********************************************************
            long maxMemoryDepois = Runtime.getRuntime().maxMemory()
            long totalMemoryDepois = Runtime.getRuntime().totalMemory()
            long freeMemoryDepois = Runtime.getRuntime().freeMemory()
            println 'Informações da Memória ANTES E DEPOIS da Limpeza'
            println 'Maxima.....: ' + (maxMemoryAntes/1024/1024).toInteger().toString() + ' MB e ' + (maxMemoryDepois/1024/1024).toInteger().toString() + ' MB'
            println 'Total......: ' + (totalMemoryAntes/1024/1024).toInteger().toString() + ' MB e ' + (totalMemoryDepois/1024/1024).toInteger().toString() + ' MB'
            println 'Livre......: ' + (freeMemoryAntes/1024/1024).toInteger().toString() + ' MB e ' + (freeMemoryDepois/1024/1024).toInteger().toString() + ' MB'
            println 'Utilizada..: ' + ( (totalMemoryAntes - freeMemoryAntes)/1024/1024).toInteger().toString() + ' MB e ' + ( (totalMemoryDepois - freeMemoryDepois)/1024/1024).toInteger().toString() + ' MB'
            println 'Nº de Processadores...: ' + Runtime.getRuntime().availableProcessors()
        } catch ( InterruptedException e ) {
            //ex.printStackTrace();
            println 'Erro cron GarbageColect - ' + e.getMessage()
        }
        println 'job: garbageCollect finalizado as ' + new Date().format('dd/MM/yyyy HH:mm:ss')
    } // fim execute
}
