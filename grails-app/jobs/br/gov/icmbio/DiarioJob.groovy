package br.gov.icmbio

import grails.converters.JSON
// http://grails-plugins.github.io/grails-quartz/ref/Triggers/cron.html
// http://plugins.grails.org/plugin/grails/quartz versao 2 não está utilizando esta
// http://grails-plugins.github.io/grails-quartz/guide/configuration.html
/*
http://www.quartz-scheduler.org/api/previous_versions/1.8.5/org/quartz/CronTrigger.html
http://www.quartz-scheduler.org/api/previous_versions/1.8.5/org/quartz/CronExpression.html
cronExpression: "s m h D M W Y"
                 | | | | | | `- Year [optional]
                 | | | | | `- Day of Week, 1-7 or SUN-SAT, ?
                 | | | | `- Month, 1-12 or JAN-DEC
                 | | | `- Day of Month, 1-31, ?
                 | | `- Hour, 0-23
                 | `- Minute, 0-59
                 `- Second, 0-59

Year is the only optional field and may be omitted, the rest are mandatory
Day-of-Week and Month are case insensitive, so "DEC" = "dec" = "Dec"
Either Day-of-Week or Day-of-Month must be "?", or you will get an error since support
by the underlying library is not complete. So you can't specify both fields, nor leave
both as the all values wildcard "*"; this is a departure from the unix crontab specification.
*/

class DiarioJob {
    def grailsApplication
    def emailService
    def sqlService
    static triggers = {

        // execute job once de 10 em 10 seconds
        //simple repeatInterval: 10000l

        // execute job once de 10 em 10 seconds
        //simple repeatInterval: 10000l

        // de 10 em 10 segundo 1 vez
        //simple name:'jobVencimentoConsultas', startDelay:1000, repeatInterval: 10000, repeatCount: 0

        // executar 1 vez as 01:00 da manha
        //cron name:'checkVencConsultasJob', startDelay:10000, cronExpression: '0 0 1 * * ?' // todos dias as 01:00 da manha
        //cron name:'diarioJob', startDelay:5000, cronExpression: '0 35 16 * * ?' // teste
        cron name:'diarioJob', startDelay:5000, cronExpression: '0 0 0,1 * * ?' //   00:00 e 01:00

    }

    def execute() {
        String SEGUNDA = '1'
        String horaAtual = new Date().format('HH')
        String diaDaSemana = new Date().format('u')
        String diaDoMes = new Date().format('d')
        println ' '
        println 'job: doiDiario iniciado as ' + new Date().format('dd/MM/yyyy HH:mm:ss')

        // diariamente AS 0hs
        if( horaAtual == '00' ){

            // somente dia 1 e 16
            if( diaDoMes == '1' || diaDoMes == '16' ){

                // enviar email cordenacao
                _enviarEmailCordenacaoRevisoesResolvidas()

            } else if( diaDoMes == '30' ){
                // limpar os arquivos de log
                _zerarArquivosLog()
            }

            // Limpar planilhas importadas
            _limparDadosImportacaoPlanilha()
        }

        // TODAS AS SEGUNDAS 01:00 da manha
        if( horaAtual == '01' ) {
            if ( diaDaSemana == SEGUNDA ) {
                _enviarEmailPendenciaRevisao()
            }
        }


        println 'job: diarioJob finalizado com SUCESSO as ' + new Date().format('dd/MM/yyyy HH:mm:ss')
        println "-" * 100
        println ' '
    } // fim execute


    /**
     * enviar email para cordenação com as fichas com pendencia de revisão resolvida pelo centro responsável
     * e que precisam ser revisadas
     */
    protected _enviarEmailCordenacaoRevisoesResolvidas(){
        DadosApoio tbApoio = DadosApoio.findByCodigo('TB_CONTEXTO_PENDENCIA')
        DadosApoio contexto = DadosApoio.findByPaiAndCodigo(tbApoio, 'REVISAO')
        List listFichas = []
        FichaPendencia.createCriteria().list{
            eq('contexto', contexto)
            isNull('dtRevisao')
            isNotNull('dtResolvida')
        }.each {
            String noCientifico = it.ficha.taxon.noCientifico
            if( !listFichas.contains( noCientifico ) ) {
                listFichas.push( noCientifico);
            }
        }
        if( listFichas ){
            listFichas.sort()
            String fromEmail = grailsApplication.config.app.email
            String assuntoEmail = """Fichas com pendência de revisão resolvida"""
            String mensagemEmail = """<p>Este é um e-mail quinzenal do SALVE para informar que as fichas abaixo tiveram
                    suas pendências de revisão resolvidas pelos centros responsáveis e que precisam ser revisadas pela cordenação da avaliação.<p>
                    <p>Fichas:</p>
                    <p><b>${listFichas.join(', ') }</b></p>"""
            if( emailService.sendTo(fromEmail,assuntoEmail, mensagemEmail,fromEmail) ){
                println '  - Enviado email para cordenacao com '+listFichas.size().toString()+' fichas com pendencia de revisão resolvidas.'
            } else {
                println ' '
                println ' '
                println 'Erro: DiarioJob._enviarEmailCordenacaoRevisoesResolvidas(): enviando email para coordenacao'
            }
        }
    }

    /**
     * enviar email para os pontos focais com as fichas com pendência de revisão
     */
    protected void _enviarEmailPendenciaRevisao() {
        int qtdEnvios = 0
        DadosApoio tbPapelFicha = DadosApoio.findByCodigo('TB_PAPEL_FICHA')
        DadosApoio papelPontoFocal = DadosApoio.findByPaiAndCodigo(tbPapelFicha, 'PONTO_FOCAL')
        try {
            // ler os emails pendentes de envio e criar lista com nome dos PFs e suas fichas
            Map emailsEnviar = [:]
            List listFichaPendenciaEmails = FichaPendenciaEmail.createCriteria().list {
                or {
                    isNull('dtEnvio') // ainda não houve envio ou
                    lt('dtEnvio', new Date()-1) // evitar 2 emails no mesmo dia
                }
                //isNull('dtEnvio')
                //gt('dtInclusao', new Date() + 5 )
                fichaPendencia{
                    eq('stPendente','S')
                    isNull('dtRevisao')
                }
            }
            listFichaPendenciaEmails.each { fichaPendenciaEmail ->
                // localizar o ponto focal da ficha
                List listFichasPontoFocal = FichaPessoa.findAllByFichaAndPapelAndInAtivo(fichaPendenciaEmail.fichaPendencia.ficha, papelPontoFocal, 'S')
                List log = []
                if( listFichasPontoFocal ) {
                    listFichasPontoFocal.each { fichaPessoa ->
                        String email = fichaPessoa.pessoa.email
                        if (email) {
                            email = email.toLowerCase()
                            log.push(fichaPessoa.pessoa.noPessoa + '|' + email)
                            if (!emailsEnviar[email]) {
                                emailsEnviar[email] = [fichas: [], ids: [], nome: Util.capitalize(fichaPessoa.pessoa.noPessoa)]
                            }
                            String nmCientifico = fichaPendenciaEmail.fichaPendencia.ficha.taxon.noCientifico
                            if (!emailsEnviar[email].fichas.contains(nmCientifico)) {
                                emailsEnviar[email].fichas.push(nmCientifico)
                                if (!emailsEnviar[email].ids.contains(fichaPendenciaEmail.id)) {
                                    emailsEnviar[email].ids.push(fichaPendenciaEmail.id)
                                }
                            }
                        }
                    }
                    fichaPendenciaEmail.txLog = log.join('<br>')
                } else {
                    fichaPendenciaEmail.txLog = 'Ponto focal não cadastrado'
                }
                fichaPendenciaEmail.save()
            }
            if (emailsEnviar) {
                emailsEnviar.each{ key, value ->
                    String nome = emailsEnviar[key].nome
                    String fichas = emailsEnviar[key].fichas.sort().join(', ')
                    String emailAssunto = "Pendências de revisão de ficha"
                    String emailFrom = grailsApplication.config.app.email
                    String emailTo = key
                    String emailMessage = """<p>Prezado(a) ${nome},</p>
                                <p>Segue abaixo relação das ficha(s) que precisa(m) de sua atenção para solucionar pendências de revisão para sua publicação:</p>
                                <p><b>${fichas}</b></p>
                                <p>Atenciosamente,<br>
                                <p>Equipe SALVE</p>
                                """
                    if( emailService.sendTo(emailTo,emailAssunto,emailMessage,emailFrom) ) {
                        qtdEnvios++
                        emailsEnviar[key].ids.each {
                            FichaPendenciaEmail fpe = FichaPendenciaEmail.get(it)
                            fpe.dtEnvio = new Date()
                            fpe.save()
                        }
                    }
                }
            }
        } catch( RuntimeException e ){
            println e.getMessage()
        }
        println '  - '+qtdEnvios + ' Email(s) enviado(s) para pontos focais sobre as pendências de revisao nas fichas'
    }


    /**
     * limpar os registros das planilhas de importação de fichas e registros
     */
    protected void _limparDadosImportacaoPlanilha(){
        int dias = 60;
        sqlService.execSql("""delete from salve.planilha_linha where sq_planilha_linha in (
               select planilha_linha.sq_planilha_linha
                 from salve.planilha_linha
                inner join salve.planilha on planilha.sq_planilha = planilha_linha.sq_planilha
                where date_part('day', now() - dt_envio) >= ${dias});""")
        println " - Dados das Planilhas de importacao registros e fichas com mais de ${dias} dias foram excluidos."
    }

    /**
     * limpar o conteúdo dos arquivos de log
     */
    protected void _zerarArquivosLog(){
        List arquivos = [
            '/data/portal/log/grails.log',
            '/data/portal/log/production.log',
            '/data/portal/log/root.log'
        ]
        println ' '
        println 'Iniciando limpeza dos arquivos de LOG - ' + new Date().format('dd/MM/yyyy HH:mm:ss')
        arquivos.each{ arquivo->
            try {
                File file = new File( arquivo )
                if( file.exists() && file.isFile() ){
                    file.write('')
                    println '  - Arquivo log ' + arquivo + ' foi limpo'
                }
            } catch( Exception e ){
                println ' '
                println 'DiarioJob._zerarArquivoLog() - Erro ao zerar o arquivo ' + arquivo + ' - ' + new Date().format('dd/MM/yyyy HH:mm:ss')
                println e.getMessage()
                println ' '
            }
        }
        println 'Fim limpeza dos arquivos de LOG - ' + new Date().format('dd/MM/yyyy HH:mm:ss')
        println ' '

    }
}
