package br.gov.icmbio

import groovy.json.internal.LazyMap
import groovy.sql.Sql
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import groovy.json.JsonSlurper

// http://grails-plugins.github.io/grails-quartz/ref/Triggers/cron.html
// http://plugins.grails.org/plugin/grails/quartz versao 2 não está utilizando esta
// http://grails-plugins.github.io/grails-quartz/guide/configuration.html
/*
http://www.quartz-scheduler.org/api/previous_versions/1.8.5/org/quartz/CronTrigger.html
http://www.quartz-scheduler.org/api/previous_versions/1.8.5/org/quartz/CronExpression.html
cronExpression: "s m h D M W Y"
                 | | | | | | `- Year [optional]
                 | | | | | `- Day of Week, 1-7 or SUN-SAT, ?
                 | | | | `- Month, 1-12 or JAN-DEC
                 | | | `- Day of Month, 1-31, ?
                 | | `- Hour, 0-23
                 | `- Minute, 0-59
                 `- Second, 0-59

Year is the only optional field and may be omitted, the rest are mandatory
Day-of-Week and Month are case insensitive, so "DEC" = "dec" = "Dec"
Either Day-of-Week or Day-of-Month must be "?", or you will get an error since support
by the underlying library is not complete. So you can't specify both fields, nor leave
both as the all values wildcard "*"; this is a departure from the unix crontab specification.
*/

class DoiJob {
    def grailsApplication
    def sqlService
    def dataSource
    def doiService
    def dadosApoioService
    static triggers = {

      // execute job once de 10 em 10 seconds
      //simple repeatInterval: 10000l

      // execute job once de 10 em 10 seconds
      //simple repeatInterval: 10000l

      // de 10 em 10 segundo 1 vez
      //simple name:'jobVencimentoConsultas', startDelay:1000, repeatInterval: 10000, repeatCount: 0

      // executar 1 vez as 01:00 da manha
      //cron name:'checkVencConsultasJob', startDelay:10000, cronExpression: '0 0 1 * * ?' // todos dias as 01:00 da manha

      // todos dias as 8,13hs
      //cron name:'doiJob', startDelay:10000, cronExpression: '0 0 0,1,2 * * ?'
      cron name:'doiJob', startDelay:10000, cronExpression: '0 0 2,3 * * ?' // todos dias as 02:00 e 03:00 da manha
    }

    def execute() {
        String DOMINGO = '7'
        // TODO controle para liberar o módulo | Aguardando definição do xml do DOI
        def moduloFinalizado = false

        String horaAtual = new Date().format('HH')
        //String diaDaSemana = new Date().format('F')
        String diaDaSemana = new Date().format('u')
        if ( diaDaSemana == DOMINGO )
        {
            println ' '
            println 'job: doiJob iniciado as ' + new Date().format('dd/MM/yyyy HH:mm:ss')

            if( horaAtual == '02' ) {
            //if( horaAtual == '08' ) {
                // Job que envia os dataset ao crosref para geração do DOI
                println "== JOB DE ENVIO DOS DATASET =="
                String prefixICMBio             = grailsApplication.config.doi.crossref.prefix_icmbio
                String tipoIdentificacaoDoi     = 'ficha'
                DadosApoio tbSituacaoFicha      = DadosApoio.findByCodigo('TB_SITUACAO_FICHA')
                DadosApoio situacaoPublicada    = DadosApoio.findByPaiAndCodigo(tbSituacaoFicha, 'PUBLICADA')

                // selecionar todas as fichas aptas a ter um DOI
                String cmdSql="""SELECT sq_ficha
                    FROM salve.ficha
                    WHERE sq_situacao_ficha = ${situacaoPublicada.id.toString()}
                        AND ds_doi IS NULL
                        AND ds_citacao IS NOT NULL
                        AND dt_publicacao IS NOT NULL
                    """
                List idsFicha = []
                List lista = sqlService.execSql( cmdSql ).each{ row ->
                    idsFicha.push(row.sq_ficha)
                }

                if( moduloFinalizado ){
                    doiService.enviarDoiFicha(idsFicha.join(','))
                }else{
                    println ' '
                    println "Fichas que iriam ser preparadas para envio para crossref: " + idsFicha.join(',')
                }
            } else if( horaAtual == '03' ) {
                // Job que verifica o DOI no crosref e executa UPDATE na tabela: ficha.ds_doi

                println "== JOB DE VERIFICAÇÃO DO DOI=="

                String prefixICMBio         = grailsApplication.config.doi.crossref.prefix_icmbio
                String tipoIdentificacaoDoi = 'ficha'
                DadosApoio situacaoPublicada = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'PUBLICADA')
                String cmdSql="""SELECT sq_ficha
                FROM salve.ficha
                WHERE sq_situacao_ficha = ${situacaoPublicada.id.toString()}
                    AND ds_doi IS NULL
                    AND ds_citacao IS NOT NULL
                    AND dt_publicacao IS NOT NULL
                """

                println "Query das fichas aptas para envio da verificação: " + cmdSql
                List listSqlUpdates = []
                List lista = sqlService.execSql( cmdSql ).each{ row ->
                    // consulta API crossref
                    String urlCrossrefWorks = grailsApplication.config.doi.crossref.works.url ?: 'http://api.crossref.org/works/'
                    String path_doi = "${prefixICMBio}/salve.${tipoIdentificacaoDoi}.${row.sq_ficha}"
                    String url = urlCrossrefWorks + URLEncoder.encode( path_doi, 'UTF-8')

                    println "URL de checagem do DOI: " + url

                    if( moduloFinalizado ) {
                        LazyMap parsedResponse
                        try{
                            parsedResponse = new JsonSlurper().parse(new URL(url))
                        }catch(Exception e){
                            println ' '
                            println "ERRO AO RECUPERAR DADOS DA URL: " + url
                        }
                        if(parsedResponse){
                            if(parsedResponse["message"]["DOI"].toString() == path_doi.toString() ) {
                                // update set ds_doi
                                listSqlUpdates.push("UPDATE salve.ficha SET ds_doi='${path_doi}' WHERE sq_ficha = ${row.sq_ficha};")
                                //cmdSql="UPDATE salve.ficha SET ds_doi='${path_doi}' WHERE sq_ficha = ${row.sq_ficha}"
                                ///sqlService.execSql(cmdSql)
                            }
                        }
                    }
                }
                // executar todos os update de uma só vez
                if( listSqlUpdates ){
                    sqlService.execSql( listSqlUpdates.join('\n') )
                }
            }
            println 'job: doiJob finalizado com SUCESSO as ' + new Date().format('dd/MM/yyyy HH:mm:ss')
            println "-" * 100
            println ' '
        }

    } // fim execute
}
