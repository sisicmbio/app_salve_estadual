package br.gov.icmbio

import groovy.sql.Sql

import static groovy.io.FileType.FILES

// http://grails-plugins.github.io/grails-quartz/ref/Triggers/cron.html
// http://plugins.grails.org/plugin/grails/quartz versao 2 não está utilizando esta
// http://grails-plugins.github.io/grails-quartz/guide/configuration.html
/*
http://www.quartz-scheduler.org/api/previous_versions/1.8.5/org/quartz/CronTrigger.html
http://www.quartz-scheduler.org/api/previous_versions/1.8.5/org/quartz/CronExpression.html
cronExpression: "s m h D M W Y"
                 | | | | | | `- Year [optional]
                 | | | | | `- Day of Week, 1-7 or SUN-SAT, ?
                 | | | | `- Month, 1-12 or JAN-DEC
                 | | | `- Day of Month, 1-31, ?
                 | | `- Hour, 0-23
                 | `- Minute, 0-59
                 `- Second, 0-59

Year is the only optional field and may be omitted, the rest are mandatory
Day-of-Week and Month are case insensitive, so "DEC" = "dec" = "Dec"
Either Day-of-Week or Day-of-Month must be "?", or you will get an error since support
by the underlying library is not complete. So you can't specify both fields, nor leave
both as the all values wildcard "*"; this is a departure from the unix crontab specification.
*/
/**
 * Job para atualização das visões materializadas
 */
class UpdateMVsJob {

    static triggers = {

        // execute job once de 20 em 20 seconds
        // simple repeatInterval: 60000l // 1 em 1 minuto

        // todos dias as 8,13hs
        cron name:'UpdateMVs', startDelay:10000, cronExpression: '0 0 0,2,4,5 * * ?'

    }

    def dataSource

    def execute() {
        String horaAtual = new Date().format('HH')
        println ' '
        println 'job: updateMVs iniciado as ' + new Date().format('dd/MM/yyyy HH:mm:ss')

        // atualizar grupo recorte módulo publico em função do grupo avaliado
        Sql sql = new Sql(dataSource)

        if( horaAtual == '00' ) {
            String cmdSql = """
            update salve.ficha set sq_grupo_salve = 1258 where sq_grupo in (12, 1275, 1274, 1272, 1268, 1273, 1271, 1267, 1270, 1269) and sq_grupo_salve is null;
            -- mamiferos para para 1262
            update salve.ficha set sq_grupo_salve = 1262 where sq_grupo in (1237, 1210, 1212, 1224, 1227, 1231, 1239, 1242, 1245, 1246) and sq_grupo_salve is null;
            -- anfibios para anfibios 11
            update salve.ficha set sq_grupo_salve = 1257 where sq_grupo in (11) and sq_grupo_salve is null;
            -- Répteis 1266
            update salve.ficha set sq_grupo_salve = 1266 where sq_grupo in (1206,1215,1223,1240,1243,14) and sq_grupo_salve is null;
            -- invertebrados terrestres 1261
            update salve.ficha set sq_grupo_salve = 1261 where sq_grupo in (1205,1207,1208) and sq_grupo_salve is null;
            -- invertebrados de agua doce 1259
            update salve.ficha set sq_grupo_salve = 1259 where sq_grupo in (1233) and sq_grupo_salve is null;
            -- mamiferos aquaticos 1263
            update salve.ficha set sq_grupo_salve = 1263  where sq_grupo in (1225) and sq_grupo_salve is null;
            -- invertebrados marionhos 1260
            update salve.ficha set sq_grupo_salve = 1260  where sq_grupo in (1276,1220) and sq_grupo_salve is null;
            --Peixes Continentais 1264
            update salve.ficha set sq_grupo_salve = 1264 where sq_grupo in (15,1236) and sq_grupo_salve is null;
            -- Peixes Marinhos 1265
            update salve.ficha set sq_grupo_salve = 1265 where sq_grupo in (16) and sq_grupo_salve is null;
            """
            sql.executeUpdate(cmdSql)
            println '  - fichas com recorte modulo publico nulos foram atualizados em função do grupo avaliado as '+ new Date().format('dd/MM/yyyy HH:mm:ss')
        }


        if( horaAtual == '02' ) {
            // Existe a situação em que a ficha está validada e o
            // resultado da validação ficou em branco
            // Este script atualiza as oficinas de Validação que
            // estão com este problema.
            // Avalidar se vale a pena executar aqui até descobrir a origem do problema.
            /*String cmdSql = """update salve.oficina_ficha
                                set sq_categoria_iucn = ficha.sq_categoria_final
                                ,ds_criterio_aval_iucn = ficha.ds_criterio_aval_iucn_final
                                ,st_possivelmente_extinta= ficha.st_possivelmente_extinta_final
                                ,ds_justificativa = ficha.ds_justificativa_final
                                from salve.oficina_ficha as oficina_ficha_1
                                inner join salve.oficina on oficina.sq_oficina = oficina_ficha_1.sq_oficina
                                inner join salve.ficha on ficha.sq_ficha = oficina_ficha_1.sq_ficha
                                where oficina_ficha_1.sq_situacao_ficha = 7
                                and oficina_ficha_1.sq_categoria_iucn is null
                                and oficina.sq_tipo_oficina = 1084
                                and oficina_ficha_1.st_transferida = false
                                and ficha.sq_categoria_final is not null
                                and ficha.ds_justificativa_final is not null
                                and ficha.sq_categoria_final <> 137
                                and oficina_ficha_1.sq_oficina_ficha = oficina_ficha.sq_oficina_ficha"""
            */
            sql.executeUpdate('REFRESH MATERIALIZED VIEW salve.mv_relatorio_taxon_analitico')
            println '  - visao materializada salve.mv_relatorio_taxon_analitico foi atualizada em ' + new Date().format('dd/MM/yyyy HH:mm:ss')
        }

        if( horaAtual == '04' ) {
            sql.executeUpdate('REFRESH MATERIALIZED VIEW salve.mv_registros');
            println '  - visao materializada salve.mv_registros foi atualizada em ' + new Date().format('dd/MM/yyyy HH:mm:ss')
        }


        // atualizar a visao materializada
        if( horaAtual == '05' /*|| horaAtual == '13' */) {
            sql.executeUpdate('REFRESH MATERIALIZED VIEW salve.mv_dados_modulo_publico')
            println '  - visao materializada salve.mv_dados_modulo_publico foi atualizada em ' + new Date().format('dd/MM/yyyy HH:mm:ss')
            // limpar dados do cache
            def dir = new File("/data/salve-estadual/cache")
            def files = []
            if( dir.isDirectory() ) {
                dir.traverse(type: FILES, maxDepth: 0) { files.add(it) }
                files.each {
                    if (it.toString() =~ /public-.+\.cache$/) {
                        if (!it.delete()) {
                            println 'erro ao excluir o arquivo ' + it.toString()
                        }
                    }
                }
            }
            println '  - cache modulo publico limpo. /data/salve-estadual/cache/public-*'
        }
        println 'job: updateMVs finalizado com SUCESSO as ' + new Date().format('dd/MM/yyyy HH:mm:ss')
        println "-" * 100
        println ' '
    } // fim execute
}
