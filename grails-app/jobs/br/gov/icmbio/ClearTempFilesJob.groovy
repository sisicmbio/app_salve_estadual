package br.gov.icmbio

import java.nio.file.Files
import java.nio.file.attribute.BasicFileAttributes

import static groovy.io.FileType.DIRECTORIES
import static groovy.io.FileType.FILES

// http://grails-plugins.github.io/grails-quartz/ref/Triggers/cron.html
// http://plugins.grails.org/plugin/grails/quartz versao 2 não está utilizando esta
// http://grails-plugins.github.io/grails-quartz/guide/configuration.html

/*
http://www.quartz-scheduler.org/api/previous_versions/1.8.5/org/quartz/CronTrigger.html
http://www.quartz-scheduler.org/api/previous_versions/1.8.5/org/quartz/CronExpression.html
cronExpression: "s m h D M W Y"
                 | | | | | | `- Year [optional]
                 | | | | | `- Day of Week, 1-7 or SUN-SAT, ?
                 | | | | `- Month, 1-12 or JAN-DEC
                 | | | `- Day of Month, 1-31, ?
                 | | `- Hour, 0-23
                 | `- Minute, 0-59
                 `- Second, 0-59

Year is the only optional field and may be omitted, the rest are mandatory
Day-of-Week and Month are case insensitive, so "DEC" = "dec" = "Dec"
Either Day-of-Week or Day-of-Month must be "?", or you will get an error since support
by the underlying library is not complete. So you can't specify both fields, nor leave
both as the all values wildcard "*"; this is a departure from the unix crontab specification.
*/

class ClearTempFilesJob {
    static triggers = {
      //simple repeatInterval: 30000l // execute job once in 30 seconds
      //simple name:'jobClearTempFile', startDelay:10000, repeatInterval: 10000//, repeatCount: 10
      //cron name:'cronClearTempFiles', startDelay:10000, cronExpression: '0 05 11 * * ?' // todos dias as 11:05 da manha
      //cron name:'cronClearTempFiles', startDelay:10000, cronExpression: '0 14 00 * * ?' // todos dias as 02:00 da manha
      // executar 1 vez as 02:00 da manha
      cron name:'cronClearTempFiles', startDelay:10000, cronExpression: '0 0 19 * * ?' // todos dias as 22:00
    }

    def execute() {
        println ' '
        println 'job: clearTempFiles iniciado as ' + new Date().format('dd/MM/yyyy HH:mm:ss')
        List listDirs = [
            [path:'/data/salve-estadual/temp',days:5],
            [path:'/data/salve-estadual/temp/mapas',days:-1, type:DIRECTORIES],
            [path:'/data/salve-estadual/iucn',days :-1],
            [path:'/data/salve-estadual/cache',days:1],
            [path:'/data/salve-estadual/logs',days:30],
            [path:'/data/salve-estadual/temp/mapas',days:7],
        ]
        listDirs.each { item ->
            def dir = new File(item.path)
            def files = []
            def type = item.type ?: FILES
            if ( dir.exists() ) {
                println '-' * 80
                println '  - diretorio: ' + item.path + '. Verificando e excluindo itens com mais de ' + item.days + ' dias.'
                dir.traverse(type: type, maxDepth: 1) { files.add(it) }
                files.each {
                    BasicFileAttributes attr = Files.readAttributes(it.toPath(), BasicFileAttributes.class);
                    Date dataCriacao = new Date(attr.creationTime().toMillis())
                    Date hoje = new Date()
                    if (((hoje - dataCriacao)) > item.days.toInteger()) {
                        try {
                            if (it.isDirectory()) {
                                if (!it.deleteDir()) {
                                    println '   - erro ao excluir doretorio ' + it.toString()
                                } else {
                                    println '     - excluido: ' + it.toString()
                                }
                            } else {
                                if (!it.delete()) {
                                    println '   - erro ao excluir arquivo ' + it.toString()
                                } else {
                                    println '     - excluido: ' + it.toString()
                                }
                            }
                        } catch (Exception e) {
                            println e.getMessage()
                            println ' '
                        }
                    }
                }
            } else {
                println '-' * 80
                println 'ATENCAO: o diretorio '+ dir.getPath()+ ' nao existe'
            }
        }
        println '-'*80
        println 'Exclusao de arquivos finalizada com SUCESSO as ' + new Date().format('dd/MM/yyyy HH:mm:ss')
        println ' '
        println ' '
        println 'Inciando exclusao das contas desativadas dos usuarios web iniciado as ' + new Date().format('dd/MM/yyyy HH:mm:ss')
        // excluir usuários web que não ativaram suas contas há 7 dias
        Integer qtd = 0
        WebUsuario.createCriteria().list {
            lt('dtInclusao', new Date() - 7 )
            eq('enabled', false)
        }.each {
            if ( FichaColaboracao.countByUsuario( it ) == 0 ) {
                it.delete(flush: true)
                qtd++
            }
        }
        println '   ' + qtd.toString()+' usuarios Web com contas nao ativadas em 7 dias foram excluidos da tabela web_usuario.'
        println 'job: clearTempFiles finalizado as ' + new Date().format('dd/MM/yyyy HH:mm:ss')
        println ' '

    } // fim execute
}
