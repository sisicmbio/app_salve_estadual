package br.gov.icmbio

import grails.util.Holders
import org.codehaus.groovy.grails.plugins.DomainClassGrailsPlugin
import org.springframework.context.ApplicationContext

import java.nio.file.Files
import java.nio.file.attribute.BasicFileAttributes

import static groovy.io.FileType.FILES

// http://grails-plugins.github.io/grails-quartz/ref/Triggers/cron.html
// http://plugins.grails.org/plugin/grails/quartz versao 2 não está utilizando esta
// http://grails-plugins.github.io/grails-quartz/guide/configuration.html

/*
http://www.quartz-scheduler.org/api/previous_versions/1.8.5/org/quartz/CronTrigger.html
http://www.quartz-scheduler.org/api/previous_versions/1.8.5/org/quartz/CronExpression.html
cronExpression: "s m h D M W Y"
                 | | | | | | `- Year [optional]
                 | | | | | `- Day of Week, 1-7 or SUN-SAT, ?
                 | | | | `- Month, 1-12 or JAN-DEC
                 | | | `- Day of Month, 1-31, ?
                 | | `- Hour, 0-23
                 | `- Minute, 0-59
                 `- Second, 0-59

Year is the only optional field and may be omitted, the rest are mandatory
Day-of-Week and Month are case insensitive, so "DEC" = "dec" = "Dec"
Either Day-of-Week or Day-of-Month must be "?", or you will get an error since support
by the underlying library is not complete. So you can't specify both fields, nor leave
both as the all values wildcard "*"; this is a departure from the unix crontab specification.
*/
class SincronizarOcorrenciasSisbioJob {

    def sessionFactory
    def asyncService
    def propertyInstanceMap = DomainClassGrailsPlugin.PROPERTY_INSTANCE_MAP

    static triggers = {
      // simple startDelay:20000, repeatInterval: 20000l, repeatCount: 0 // execute job once in 20 seconds
      // simple name:'jobClearTempFile', startDelay:10000, repeatInterval: 10000//, repeatCount: 10

     // cron desabilitado por enquanto
     // cron name:'cronSincronizarOcorrencias', startDelay:10000, cronExpression: '0 0 12 ? * SAT' // SABADOS AS 12:00
    }

    def execute() {
        Long contador = 0
        String horaInicio = new Date().format('dd/MM/yyyy HH:mm:ss')
        Integer anoAtual  = new Date().format('yyyy').toInteger();
        println ' '
        println ' '
        println 'job: sincronizar ocorrencias iniciado as ' + horaInicio

        ApplicationContext ctx = Holders.grailsApplication.mainContext
        String url = ctx.grailsApplication.config.url.search.portalbio

        //CicloAvaliacao ciclo = CicloAvaliacao.findAllByInSituacao('A').last()
        List ciclo = CicloAvaliacao.createCriteria().list {
            eq('inSituacao', 'A')
            between('nuAno', anoAtual-4i, anoAtual )
            maxResults(1)
        }
        if( ciclo  )
        {
            Integer idCiclo = ciclo[0].id.toString().toInteger()
            println ' - Ciclo Ano Final: ' + ciclo[0].anoFinal
            //---------------------------------------------------

            DadosApoio tbSituacaoFicha = DadosApoio.findByCodigo('TB_SITUACAO_FICHA')
            DadosApoio compilacao      = DadosApoio.findByPaiAndCodigo(tbSituacaoFicha, 'COMPILACAO')
            DadosApoio consulta        = DadosApoio.findByPaiAndCodigo(tbSituacaoFicha, 'CONSULTA')
            Long fichasProcessadas     = 0
            Long fichaCount = VwFicha.createCriteria().get{
                projections {
                    count "id"
                }
                eq('sqCicloAvaliacao', idCiclo )
                'in'('sqSituacaoFicha',[compilacao.id.toInteger(),consulta.id.toInteger()])
            }
            println ' - Quantidade de fichas ' + fichaCount
            VwFicha.createCriteria().list {
                eq('sqCicloAvaliacao', idCiclo )
                'in'('sqSituacaoFicha',[compilacao.id.toInteger(),consulta.id.toInteger()])
                //eq('id',15014l)
            }.eachWithIndex {it, index ->

                //if( index < 5 ) {
                Map data     = ["idFicha": it.id, "callback": '']
                data.url     = url
                data.taxon   = Util.ncItalico(it.nmCientifico,true,false)
                data.sqTaxon = it.sqTaxon

                String inicio = new Date().format('dd/MM/yyyy HH:mm:ss')
                asyncService._impOcoPortalbio( data )
                fichasProcessadas++
                //print fichasProcessadas + '/' + fichaCount + ' - Sicronizado ' + data.taxon + ' Inicio: '+inicio+'   Fim: ' + new Date().format('dd/mm/YYYY HH:mm:ss')

                def sessionF = sessionFactory.currentSession
                sessionF.flush()
                sessionF.clear()
                propertyInstanceMap.get().clear()
            }
        }

        //sleep(5000)
        println ' - job sicoronizar ocorrencias inciado as '+horaInicio+' e finalizado com SUCESSO as ' + new Date().format('dd/MM/yyyy HH:mm:ss')
        println ' '
    } // fim execute
}
