package br.gov.icmbio

import grails.converters.JSON

//import grails.transaction.Transactional
//@Transactional
class UserJobService {
    def emailService
    UserJobs createJob( Map jobParams = [:] ) {
        UserJobs uj = new UserJobs( jobParams )
        uj.stCancelado=false
        uj.stExecutado=false

        return uj
    }
    UserJobs start( Long sqUserJobs ) {
        return start( UserJobs.get(sqUserJobs) )
    }
    UserJobs start( UserJobs uj ) {
            uj.stCancelado=false
            uj.stExecutado=false
            uj.dtInicio = new Date()
            uj.vlAtual = 0
            uj.deAndamento = '0%'
            uj.save(flush: true)
            return uj
    }
    UserJobs error( Long sqUserJobs , String message) {
        UserJobs uj = UserJobs.get(sqUserJobs)
        uj.deErro = message
        uj.dtFim = new Date()
        uj.dtUltimaAtualizacao = uj.dtFim
        uj.save(flush: true)
        return uj
    }

    Map asMap( UserJobs uj, List columns = []) {
        Map data = [type:'job'] // para diferenciar dos workers de sessão atuais
        try {
            if( uj ) {
                columns = columns ?: ['id', 'usuario', 'deRotulo'
                                      , 'deAndamento', 'vlMax'
                                      , 'vlAtual', 'jsExecutar'
                                      , 'deErro', 'deMensagem'
                                      , 'dtInclusao', 'dtInicio'
                                      , 'dtFim', 'dtUltimaAtualizacao'
                                      , 'idElemento', 'deEmail'
                                      , 'deEmailAssunto', 'noArquivoAnexo'
                                      , 'stEmailEnviado', 'stCancelado']
                columns.each { colName ->
                    if (colName == 'usuario') {
                        data.usuario = [  noPessoa  : uj?.usuario?.noPessoa ?:''
                                        , sqPessoa  : uj?.usuario?.id ?: ''
                                        , nuCpf     : uj?.usuario?.nuCpf ?Util.formatCpf(uj.usuario.nuCpf) : ''
                                        ]
                    } else if ((colName =~ /^dt/) && uj[colName]) {
                        data[colName] = uj[colName].format('dd/MM/yyyy HH:mm:ss')
                    } else if (colName == 'nuCpf' && uj[colName]) {
                        data[colName] = Util.formatCpf(uj[colName])
                    } else {
                        data[colName] = uj[colName] ?: ''
                    }
                }
                // colunas que sempre deverão retornar para funcionar o loop de andamento dos jobs (chkWorkers.js)
                data['vlPercentual'] = uj.percentual()
                data['stCancelado'] = uj.stCancelado
                data['stExecutado'] = uj.stExecutado
                data['jsExecutar'] = uj.jsExecutar
                data['noArquivoAnexo'] = uj.noArquivoAnexo
                data['deEmail'] = uj.deEmail
                data['deSituacao'] = uj.situacao()
            }
        } catch ( Exception e ){
            data.id=0
            data.deError = e.getMessage()
        }
        return data
    }
    Map asMap( Long sqUserJobs=0, List columns=[]) {
        UserJobs uj = UserJobs.get(sqUserJobs)
        return asMap(uj, columns)
    }
    List getUserJobsAsList( Long sqUserJob = 0, List columns = [] ) {
        List data = []
        columns = columns ?: ['id', 'deAndamento', 'deMensagem', 'deErro', 'dtFim','dtUltimaAtualizacao', 'deEmail']
        try {
            UserJobs.findAllByUsuario( PessoaFisica.get(sqUserJob)).each { job ->
                data.push(asMap(job, columns ) )
            }
        } catch( Exception e ){
            Util.printLog('UserJobsService.getUserJobsAsList','Colunas: '+columns.join(', '),e.getMessage())
            data = []
        }
        return data
    }
    Long cancelUserJob(Long sqUserJob = 0, Long sqUsuario = 0 ) {
        if (!sqUserJob) {
            return 0
        }
        PessoaFisica pf = PessoaFisica.get(sqUsuario)
        UserJobs uj = UserJobs.get(sqUserJob)
        if (!uj) {
            throw new Exception('Id da tarefa inválido')
        }
        if (uj.usuario != pf) {
            throw new Exception('Tarefa pertence a outro usuário')
        }
        // primeiro cancela depois exclui
        if (uj.stCancelado || uj.stExecutado) {
            uj.delete(flush: true)
        } else {
            uj.stCancelado = true
            uj.save(flush: true)
            println 'JOB CANCELADO PELO USUARIO - ' + uj?.usuario?.noPessoa
            println 'Em.......: ' + new Date().format('dd/MM/yyyy HH:mm:ss')
            println 'id.......: ' + uj.id
            println 'Rotulo...: ' + uj.deRotulo
            println 'Andamento: ' + uj.deAndamento
            println '-'*80
        }
        return uj.id
    }
    void cancelAllUserJobs(Long sqUsuario = 0 ){
        PessoaFisica pf = PessoaFisica.get( sqUsuario )
        UserJobs.findAllByUsuario(pf).each {
            // primeiro cancelar e depois excluir
            if( it.stCancelado || it.stExecutado ){
                 it.delete(flush:true)
            } else {
                it.stCancelado = true
                it.save(flush:true)
            }
        }
    }


    void enviarEmail( Long sqUserJobs ){
        UserJobs uj = UserJobs.get( sqUserJobs )
        asyncMail(uj)
    }

    void asyncMail(UserJobs uj ){
        if( !uj.deEmail || uj.stEmailEnviado || uj.deErro ){
            return
        }
        // evitar enviar mais de uma vez
        uj.stEmailEnviado = true
        uj.save(flush: true)

        runAsync {
            try {
                String noArquivoAnexo = uj.noArquivoAnexo
                File fileAnexo
                if (noArquivoAnexo) {
                    // procurar o arquivo no diretorio temp
                    fileAnexo = new File(noArquivoAnexo)
                    if (!fileAnexo.exists()) {
                        String tempName = '/data/salve-estadual/temp/' + noArquivoAnexo
                        fileAnexo = new File(tempName)
                        if (!fileAnexo.exists()) {
                            tempName = '/data/salve-estadual/anexos/' + noArquivoAnexo
                            fileAnexo = new File(tempName)
                            if (!fileAnexo.exists()) {
                                tempName = '/data/salve-estadual/multimidia/' + noArquivoAnexo
                                fileAnexo = new File(tempName)
                            } else {
                                fileAnexo = null
                            }
                        }
                    }
                    if (noArquivoAnexo && !fileAnexo) {
                        uj.deErro = 'Arquivo ' + noArquivoAnexo + ' não encontrado'
                        uj.stEmailEnviado = false
                        uj.save(flush: true)
                    } else {
                        // zipar o arquivo antes de anexa-lo ao e-mail
                        String zipFilename = fileAnexo.getPath()
                        Map zipData = Util.zipFile(zipFilename)
                        if (zipData.zipFileName) {
                            zipFilename = zipData.zipFileName
                            if (!emailService.sendTo(uj.deEmail
                                , (uj?.deEmailAssunto ?: 'Sistema SALVE')
                                , uj.deMensagem
                                , '', [fullPath: zipFilename])) {
                                uj.deErro = 'Erro ao enviar e-mail'
                            } else {
                                uj.stEmailEnviado = true
                            }
                            // utilizar try catch porque o job pode ter sido deletado
                            try {uj.save(flush: true)} catch(Exception e ){}
                        }
                    }
                }
            } catch (Exception e ){
                Util.printLog('UserJobService.asyncEmail()','Erro ao enviar e-mail',e.getMessage() )
                uj.deErro = e.getMessage()
                uj.stEmailEnviado = false
                uj.save(flush: true)
            }
        }
    }
}
