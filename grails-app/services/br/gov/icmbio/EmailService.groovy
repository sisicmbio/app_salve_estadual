package br.gov.icmbio

import grails.util.Holders
import org.springframework.context.ApplicationContext

//import grails.transaction.Transactional
import org.springframework.web.multipart.commons.CommonsMultipartFile
import org.apache.commons.mail.Email
import org.apache.commons.mail.SimpleEmail
import org.apache.commons.mail.HtmlEmail
import org.apache.commons.mail.DefaultAuthenticator
import org.apache.commons.mail.EmailAttachment
import org.apache.commons.mail.MultiPartEmail

import javax.mail.internet.InternetAddress
import java.util.concurrent.ExecutionException

//@Transactional
class EmailService {

    def utilityService

    // Alternative ports: 8025, 587, 80 or 25. TLS is available on the same ports. SSL is available on ports 465, 8465 and 443.

    boolean sendTo(String strTo="",String strSubject="",String strMsg="", String emailFrom="", Map anexo = [:], String cc = '' )
    {
        ApplicationContext ctx = Holders.grailsApplication.mainContext
        boolean transactional   = false
        String host     = ctx.grailsApplication.config.email.host
        String username = ctx.grailsApplication.config.email.username
        String password = ctx.grailsApplication.config.email.password
        String from     = emailFrom?:ctx.grailsApplication.config.email.from
        String port     = ctx.grailsApplication.config.email.port
        ctx=null

        strTo = strTo?.replaceAll(/;/,',')

        if( from?.indexOf('>') > -1 )
        {
            from = from.substring( from.indexOf('<')+1,from.indexOf('>') )
        }

        if( !host || !strTo || !strMsg || !strSubject )
        {
            log.info( 'Host:'+host)
            log.info( 'StrTo:'+strTo)
            log.info( 'strMsg:'+strMsg)
            log.info( 'strSubject:'+strSubject)
            log.info( 'Email não enviado, parâmetros incompletos!')
            log.info('Email não enviado, parâmetros incompletos!')
            return false
        }
        try
        {
            strMsg = utilityService.str2html( strMsg.toString()  )
            strMsg = utilityService.trimEditor( strMsg.toString() )
            //strSubject = utilityService.str2html( strSubject )
            //strTo='luiseugeniobarbosa@gmail.com'
            /** /
            println ' '
            println '--------------------------------------------------------------------------------------'
            println 'Email formato TEXT selecionado'
            println 'host:' + host
            println 'username:' + username
            println 'password:' + password
            println 'from:' + from
            println 'to:' + strTo
            println 'port:' + port
            println 'Subject:' + strSubject
            //println 'Mensagem:' + strMsg
            println 'Formato HTML: ' + ( !! strMsg.find('</|/>') )
            println 'FileName: ' + (anexo ? anexo.fullPath:' sem anexo!')
            println '--------------------------------------------------------------------------------------'
            println ' '
             /**/

            // enviar no formato texto simples
            if( ! strMsg.find('</|/>') && ! anexo.fullPath ) {
                strMsg = strMsg.replaceAll(/<br ?\/?>/,"\n")
                //println 'Email formato TEXTO selecionado'
                log.info('Email formato TEXTO selecionado')
                SimpleEmail email = new SimpleEmail()
                email.setHostName( host )
                email.addTo( strTo )
                //email.setFrom(from,'ICMBio - Sistema Salve')
                email.setFrom( from )
                log.info('Enviando email para ' + strTo)
                if( cc ) {
                    List myCcList = InternetAddress.parse( cc )
                    email.setCc( myCcList )
                    log.info('Com copia para ' + myCcList.join(',') )
                }
                email.setSubject( strSubject)
                log.info('Assunto: ' + strSubject )
                log.info( ' ')
                email.setMsg( strMsg )
                if( username && password )
                {
                    email.setAuthentication( username, password )
                }
                //email.setSSLOnConnect(true)
                ///email.setSslSmtpPort( "465" )
                email.setSmtpPort( port.toInteger() )
                email.send()
            }
            else
            {
                log.info('Email formato HTML selecionado')

                strMsg = strMsg.replaceAll(/\n|\r|\t/,'').replaceAll(/>\s+</,'><')
                /*println '-------------  inicio mensagem EMAIL html -------------'
                println strMsg
                println '-------------  fim mensagem EMAIL html -------------'
                */
                //strMsg = strMsg.replaceAll(/\n/,"<br/>")
                // enviar no formato HTML
                HtmlEmail htmlEmail = new HtmlEmail()
                //htmlEmail.setSSLOnConnect(true)
                //htmlEmail.setSslSmtpPort( "465" )
                htmlEmail.setCharset('UTF-8')
                htmlEmail.setHostName( host )
                //htmlEmail.setAuthenticator( new DefaultAuthenticator( username , password ) );
                htmlEmail.setAuthentication(username,password)
                htmlEmail.setSmtpPort( port.toInteger() )
                // 1
                htmlEmail.setFrom( from ,'Sistema de Avaliação - SALVE','UTF-8') // assim funcionou a acentuação
                // 2
                //htmlEmail.setFrom( from , new String('Sistema de Avaliação - SALVE','UTF-8'))
                // 3
                //htmlEmail.setFrom( from , new String('Sistema de Avaliação - SALVE','ISO-8859-1'))
                log.info('Enviando email para ' + strTo)
                if( cc ) {
                    List myCcList = InternetAddress.parse( cc )
                    htmlEmail.setCc( myCcList )
                    log.info('Com copia para ' + myCcList.join(',') )
                }
                //htmlEmail.setFrom( from, 'ICMBio - Sistema Salve')
                htmlEmail.addTo( strTo )


                htmlEmail.setDebug(false)
                log.info('Assunto: ' + strSubject )
                log.info( ' ')
                htmlEmail.setSubject( strSubject  )

                /*StringBuilder builder = new StringBuilder();
                builder.append("<h1>Um titulo</h1>");
                builder.append("<p>Lorem ipsum dolor sit amet, <b>consectetur adipiscing elit</b>. Duis nec aliquam tortor. Sed dignissim dolor ac est consequat egestas. Praesent adipiscing dolor in consectetur fringilla.</p>");
                builder.append("<a href=\"http://wwww.botecodigital.info\">Boteco Digital</a> <br> ");
                builder.append("<img src=\"http://www.botecodigital.info/wp-content/themes/boteco/img/logo.png\">");
                */
                strMsg = strMsg.replaceAll(/<p>/ ,'<p style="line-height:20px;">')
                htmlEmail.setHtmlMsg('<html><body>'+strMsg.toString()+'</body></html>')

                // adicionar o anexo
                if( anexo.fullPath ) {
                    // Create the attachment
                    EmailAttachment attachment = new EmailAttachment()
                    attachment.setPath(anexo.fullPath)
                    // ou
                    //attachment.setURL(new URL("http://www.apache.org/images/asf_logo_wide.gif"));
                    attachment.setDisposition(EmailAttachment.ATTACHMENT)
                    attachment.setDescription("Arquivo anexo")
                    attachment.setName(anexo.fileName)
                    // add the attachment
                    htmlEmail.attach( attachment );
                }
                htmlEmail.send()
            }
            return true
        }
        catch(Exception e) {
            //e.printStackTrace()
            /*
            println 'Erro sendTo()'
            println e.getMessage()
            */
            log.error( e.getMessage() )
        }
        return false
    }

    Map saveTempFile( CommonsMultipartFile f )
    {
        Map res=[fullPath:'',fileName:'',extension:'', error:'']
        if( !f  )
        {
            return res
        }
        ApplicationContext ctx = Holders.grailsApplication.mainContext
        String tempDir = ctx.grailsApplication?.config.temp.dir
        File savePath  = new File( tempDir )
        if (! savePath.exists()) {
            if ( ! savePath.mkdirs()) {
                log.info( 'Diretório ' + tempDir +' não existe!')
                res.error = 'Diretório '+tempDir +' não existe';
                return res
            }
        }
        res.fileName = f.getOriginalFilename().replaceAll(' |,|\\(.+\\)', '_').replaceAll('_+','-').replaceAll('-\\.','.')
        res.fullPath = savePath.absolutePath + '/' + res.fileName
        res.extension = res.fileName.substring(res.fileName.lastIndexOf("."))
        if ( ! new File( res.fullPath ).exists()) {
            try {
                log.info('Salvando anexo em ' + res.fullPath)
                f.transferTo(new File(res.fullPath) )
                log.info( 'Anexo salvo')
            } catch ( Exception e ) {
                log.info( e.getMessage() )
                res.error = e.getMessage()
                res.fullPath=''
                res.fileName=''
                return res
            }
        }
        return res
    }
}
