package br.gov.icmbio

import com.vividsolutions.jts.geom.Geometry
import com.vividsolutions.jts.io.WKTReader
import grails.converters.JSON
import grails.transaction.Transactional

@Transactional
class CorpRegiaoService {

    def sqlService

    /**
     * método para gravar as informações da região no banco de dados
     * @param sqRegiao
     * @param noRegiao
     * @param wkt
     * @return
     */
    def save(Long sqRegiao = null, String noRegiao= '', Integer nuOrdem = null,  String wkt = '') {
        if( !noRegiao ){
            throw new Exception('Nome da região não informado')
        }

        Regiao regiao
        if( sqRegiao ){
            regiao = Regiao.get( sqRegiao )
            if( !regiao ){
                throw new Exception('Id da região inexistente')
            }
        } else {
            regiao = new Regiao()
        }

        if( wkt ) {
            WKTReader wktReader = new WKTReader();
            Geometry g = wktReader.read( wkt )
            regiao.geometry = g
            if (regiao.geometry) {
                regiao.geometry.SRID = 4674
            }
        } else {
            regiao.geometry=null
        }
        regiao.nuOrdem = nuOrdem
        regiao.noRegiao = noRegiao
        regiao.save()
    }


    /**
     * método para recuperar as informações da região para alteração
     * @param sqRegiao
     * @return
     */
    def edit(Long sqRegiao=0l) {
        if( !sqRegiao ){
            throw new Exception('Id da região não informado')
        }

        Regiao regiao
        regiao = Regiao.get( sqRegiao )
        if( !regiao ){
            throw new Exception('Id da região inexistente')
        }

        Map result = [ 'sqRegiao'    : regiao.id
                       , 'noRegiao'  : regiao.noRegiao
                       , 'nuOrdem'   : regiao.nuOrdem
                       , 'geojson'   : [ "type": "FeatureCollection"
                       , 'features'  : []
                       ]
        ]
        String cmdSql = """SELECT ST_AsGeoJSON( ge_regiao,13,8) AS geoJson 
                           FROM salve.regiao 
                           WHERE sq_regiao = :sqRegiao"""
        sqlService.execSql( cmdSql, [sqRegiao:sqRegiao] ).each { row ->
            if( row.geoJson ) {
                result.geojson.features.push( [ 'type': 'Feature'
                                               ,'properties': [],
                                                'geometry': JSON.parse( row.geoJson )

                ])
            }
        }
        return result
    }


    /**
     * método para excluir a região do banco de dados
     * @return
     */
    def delete(Long sqRegiao = 0l) {
        Regiao regiao = Regiao.get( sqRegiao )
        if( !regiao ) {
            throw new Exception('Id da região não informado')
        }
        regiao.delete()
    }

}
