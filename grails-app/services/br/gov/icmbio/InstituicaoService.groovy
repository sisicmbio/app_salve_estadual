package br.gov.icmbio

import grails.transaction.Transactional

@Transactional
class InstituicaoService {

    def estruturaOrg(Long idPai = null ) {
        List instituicoes = Instituicao.createCriteria().list {
            tipo {
                eq('codigoSistema','UNIDADE_ORGANIZACIONAL')
            }
            if( idPai ) {
                pai{
                    eq('id',idPai)
                }
            } else {
                isNull('pai')
            }
            pessoa {
                order('noPessoa')
            }
        }
        return instituicoes
    }
}
