package br.gov.icmbio

import com.vividsolutions.jts.geom.Geometry
import com.vividsolutions.jts.io.WKTReader
import grails.converters.JSON
import grails.transaction.Transactional

@Transactional
class CorpEstadoService {

    def sqlService

    /**
     * método para gravar as informações do Estado no banco de dados
     * @param sqEstado
     * @param noEstado
     * @param wkt
     * @return
     */
    def save(Long sqEstado = null, String noEstado= '', String sgEstado= '', Long sqRegiao = null,  String wkt = '') {
        if( !noEstado ){
            throw new Exception('Nome do Estado não informado')
        }
        if( !sgEstado ){
            throw new Exception('Sigla do Estado não informada')
        }

        Estado estado
        if( sqEstado ){
            estado = Estado.get( sqEstado )
            if( !estado ){
                throw new Exception('Id do Estado inexistente')
            }
        } else {
            estado = new Estado()
        }

        if( wkt ) {
            WKTReader wktReader = new WKTReader();
            Geometry g = wktReader.read( wkt )
            estado.geometry = g
            if (estado.geometry) {
                estado.geometry.SRID = 4674
            }
        } else {
            estado.geometry=null
        }
        Pais brasil = Pais.findByCoPais('BRA')
        if( !brasil ){
            throw new Exception('País Brasil não cadastrado')
        }
        estado.pais = brasil
        estado.sgEstado = sgEstado.toUpperCase()
        estado.noEstado = Util.capitalize(noEstado)
        estado.regiao = Regiao.get( sqRegiao )
        estado.save()
    }


    /**
     * método para recuperar as informações do Estado para alteração
     * @param sqEstado
     * @return
     */
    def edit(Long sqEstado=0l) {
        if( !sqEstado ){
            throw new Exception('Id do Estado não informado')
        }

        Estado estado
        estado = Estado.get( sqEstado )
        if( !estado ){
            throw new Exception('Id do Estado inexistente')
        }

        Map result = [ 'sqEstado'    : estado.id
                       , 'noEstado'  : estado.noEstado
                       , 'sgEstado'  : estado.sgEstado
                       , 'sqRegiao'  : estado?.regiao?.id
                       , 'noRegiao'  : estado?.regiao?.noRegiao
                       , 'geojson'   : [ "type": "FeatureCollection"
                       , 'features'  : []
                       ]
        ]
        String cmdSql = """SELECT ST_AsGeoJSON( ge_estado,13,8) AS geoJson 
                           FROM salve.estado 
                           WHERE sq_estado = :sqEstado"""
        sqlService.execSql( cmdSql, [sqEstado:sqEstado] ).each { row ->
            if( row.geoJson ) {
                result.geojson.features.push( [ 'type': 'Feature'
                                               ,'properties': [],
                                                'geometry': JSON.parse( row.geoJson )

                ])
            }
        }
        return result
    }


    /**
     * método para excluir a Estado do banco de dados
     * @return
     */
    def delete(Long sqEstado = 0l) {
        Estado estado = Estado.get( sqEstado )
        if( !estado ) {
            throw new Exception('Id do Estado não informado')
        }
        estado.delete()
    }

}
