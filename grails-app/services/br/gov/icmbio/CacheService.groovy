package br.gov.icmbio

import groovy.json.JsonBuilder
import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import grails.converters.JSON
import groovy.time.BaseDuration
import org.springframework.context.ApplicationContext
//import grails.converters.JSON
import grails.util.Holders

//import grails.transaction.Transactional
import org.codehaus.groovy.grails.web.servlet.mvc.GrailsParameterMap
import org.springframework.web.context.request.RequestContextHolder
import groovy.time.TimeCategory

import java.nio.file.Files
import java.nio.file.attribute.BasicFileAttributes

import static groovy.io.FileType.FILES

//@Transactional
class CacheService {
    static transactional = false // não abrir uma nova sessão
    private Integer defaultMinutes = 2i // tempo padrão de cache
    private String dateTimeFormat = "yyyy-MM-dd HH:mm:ss"
    private boolean enabled = false // habilitar/desabilitar o cache

    java.util.LinkedHashMap getCache( GrailsParameterMap params ) {
        return this.getCache( this.getKey( params ) )
    }
    java.util.LinkedHashMap getCache( String cacheKey ) {
        if( !enabled )
        {
            return [:]
        }

        def session = RequestContextHolder.currentRequestAttributes().getSession()
        if( !session.cache )
        {
            session.cache=[:]
        }
        else
        {
            // limpar os caches vencidos
            session.cache = session.cache.findAll {
                if( ! ( it.value['cacheInfo']['dateEnd'] >= new Date() ) )
                {
                    if( ! session.envProduction )
                    {
                        log.info( 'Cache [key ' + it.key + ' removido da memoria ]')
                    }
                }
                return (it.value['cacheInfo']['dateEnd'] > new Date() )
            }
        }
        if( session.cache[cacheKey] && enabled ) {
            if (!session.envProduction) {
                log.info( 'Lendo do cache em memoria. [key ' + cacheKey + ' - validade ' + session.cache[cacheKey]['cacheInfo']['dateEnd'].format('dd/MM/yyyy HH:mm:ss') + ']')
            }
            return session.cache[cacheKey]
        }
        /*
        // recuperar o cache do disco
        String tempDir = getTempDir()
        File file = new File(tempDir + cacheKey+'.cache')
        if( file.exists() )
        {
            log.info( 'Lendo do cache em disco. [key ' + cacheKey + ']')
            return (Map) (file.text as JSON)
        }
        */
        return [:]
    }
    void setCache( String cacheKey, String data, Integer maxAgeMinutes=1){
        if( data && data != 'null')
        {
            this.setCache(cacheKey, [data: data], maxAgeMinutes)
        }
    }
    void setCache( GrailsParameterMap params, String data, Integer maxAgeMinutes=defaultMinutes) {
        String cacheKey = this.getKey( params )
        if( data && data != 'null')
        {
            this.setCache(cacheKey, [data: data], maxAgeMinutes)
        }
        this.setCache( cacheKey, [data:data], maxAgeMinutes)
    }
    void setCache( String cacheKey, java.util.ArrayList data, Integer maxAgeMinutes=defaultMinutes){
        this.setCache(cacheKey,[data:data],maxAgeMinutes)
    }
    void setCache( GrailsParameterMap params, java.util.ArrayList data, Integer maxAgeMinutes=defaultMinutes) {
        this.setCache( this.getKey( params ), [data:data], maxAgeMinutes)
    }
    void setCache( GrailsParameterMap params, java.util.LinkedHashMap data, Integer maxAgeMinutes=defaultMinutes) {
        this.setCache( this.getKey( params ), data, maxAgeMinutes)
    }
    void setCache(String cacheKey, java.util.LinkedHashMap data, Integer maxAgeMinutes=defaultMinutes) {
        if( ! data || !enabled )
        {
            return
        }
        def session = RequestContextHolder.currentRequestAttributes().getSession()
        // controle de cache
        if( ! session.cache )
        {
            session.cache = [:]
        }
        Date dateEnd = new Date()
        use( TimeCategory ) {
            dateEnd = dateEnd + [maxAgeMinutes].minutes
        }
        // injetar a data de inicio e fim do cache
        data.cacheInfo=[date:new Date(),dateEnd:dateEnd]
        session.cache[cacheKey]=data

        // guardar o cache em disco
        //String tempDir = getTempDir()
        //new File(tempDir).write(new JsonBuilder( (data as JSON).toString() ).toPrettyString())
        //return
    }
    /**
     * retornar hash md5 dos parametros que será utilizado como chave do cache.
     * Se for passado o parâmetro "semCache" retorna uma chave inválida para que o cache não seja utilizado
     *
     * @param  params - mapa de parâmetros para gerar o hash
     * @return hash dos parâmetros
     */
    String getKey( GrailsParameterMap params )
    {
        //println 'cacheService()'
        //println params
        // se houver o parametro semCache retornar uma chave inválida
        if( params.semCache )
        {
            //println 'Cache desabilitado'
            //println ' '
            return Util.md5( new Date().toString() )
        }
        Map mapTemp = [:]
        params.each {
            // ignorar os seguintes parâmetros
            //if (! ( it.key =~/ajax|csv|container|count|callback/) )
            //{
                mapTemp[it.key] = it.value
            //}
        }
        try
        {
            mapTemp = mapTemp.sort()
        }
        catch(Exception e){}
        // colocar o nome do controller na frente da chave para implementar rotina de limpeza por controller
        String controller = params.controller ? params.controller+'-': ''
        controller = params.action ? controller+params.action + '-' : controller
        String key = controller+Util.md5(mapTemp.toString() )
        //println 'Key gerada: ' + key
        //println ' '

        return key
    }
    String getKey( String key = '' ){
        return Util.md5( key )
    }
    /**
     * limpar os caches da memória.
     * Opcionalmente pode ser passado o parametro key para limpeza específica
     *
     * @example clear('ficha-list')
     * @return
     */
    Integer  clear( String key = ""){
        Integer count = 0
        def session = RequestContextHolder.currentRequestAttributes().getSession()
        String cacheDir = getCacheDir()
        if( session.cache ) {
            if( key ) {
                if( session.cache && session.cache[key]){
                    count=1
                }
                session.cache = session.cache.findAll { !(it.key.matches( '^'+key+'.+' )) }
            }
            else {
                session.cache = [:]
            }
        }
        if( key ) {
            count = ( (new File( cacheDir + key + '.cache').delete()) || ( new File( cacheDir + key ).delete() ) ? 1 : 0 )
        } else {
            def dir = new File(cacheDir)
            def files = []
            dir.traverse(type: FILES, maxDepth: 0) { files.add(it) }
            files.each {
                if (it.name ==~ /.*cache$/) {
                    if( it.delete() ) {
                        count++
                    }
                }
            }
        }
        return count
    }
    /**
     * habilitar/desabilitar o servico de cache
     * @param newValue
     */
    void setEnabled( boolean newValue = true ){
        enabled=newValue
    }

    private String getTempDir(){
        // guardar o cache em disco
        ApplicationContext ctx = Holders.grailsApplication.mainContext
        return ctx.grailsApplication?.config?.temp?.dir
    }

    private String getCacheDir(){
        // guardar o cache em disco
        ApplicationContext ctx = Holders.grailsApplication.mainContext
        return ctx.grailsApplication?.config?.cache?.dir
    }

    // cache em disco

    /**
     * método para gravar HashMap em disco no diretório data/salve-estadual/cache
     * @param data
     * @param minutes
     * @param key
     * @return chave gerada para recuperacao do cache
     */
    String setToFileCache( String key='', java.util.ArrayList data, Integer minutes = 60 ) {
        setToFileCache(key, ['_cache_':data], minutes )
    }
    String setToFileCache(String key = '', java.util.LinkedHashMap data = [:], Integer minutes = 60 ) {
        Date expireDate = Util.agora()
        use( TimeCategory ) {
            expireDate = expireDate + ((minutes).minutes)
        }
        Map cacheObject = [ cache : data, expireDate : expireDate.format(dateTimeFormat) ]
        if( ! data ) {
            return
        }
        if( ! key ) {
            key = 'hash-'+cacheObject.hashCode()
        }
        String cacheFileName= getCacheDir()+key+'.cache'
        File file = new File( cacheFileName )
        def json_str = JsonOutput.toJson( cacheObject )
        file.write( json_str )
        return key
    }
    /**
     * método para recuperar HashMap do disco gravado no diretório data/salve-estadual/cache
     * @param key
     * @return
     */
    Map getFromFileCache( String key = '') {
        Map data = [cache: null]
        try {
            String cacheFileName = getCacheDir() + key.toString() + '.cache'
            File file = new File(cacheFileName)
            if (file.exists()) {
                def jsonSlurper = new JsonSlurper()
                data = jsonSlurper.parse(file)
                Date expireDate = Date.parse(dateTimeFormat, data.expireDate)
                if (expireDate < Util.agora()) {
                    file.delete()
                    data = [cache: null]
                }
            }
            if (data.cache && data.cache['_cache_']) {
                data = [cache: data.cache['_cache_']]
            }
        } catch ( Exception e ) {
            data = [cache: null]
        }
        return data
    }
}
