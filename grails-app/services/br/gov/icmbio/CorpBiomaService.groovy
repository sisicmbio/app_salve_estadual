package br.gov.icmbio

import com.vividsolutions.jts.geom.Geometry
import com.vividsolutions.jts.io.WKTReader
import grails.converters.JSON
import grails.transaction.Transactional

@Transactional
class CorpBiomaService {

    def sqlService

    /**
     * método para gravar as informações do bioma no banco de dados
     * @param sqBioma
     * @param noBioma
     * @param wkt
     * @return
     */
    def save(Long sqBioma = null, String noBioma= '', String wkt = '') {
        if( !noBioma ){
            throw new Exception('O nome do bioma não informado')
        }

        Bioma bioma
        if( sqBioma ){
            bioma = Bioma.get( sqBioma )
            if( !bioma ){
                throw new Exception('Id do bioma inexistente')
            }
        } else {
            bioma = new Bioma()
        }

        if( wkt ) {
            WKTReader wktReader = new WKTReader();
            Geometry g = wktReader.read( wkt )
            bioma.geometry = g
            if (bioma.geometry) {
                bioma.geometry.SRID = 4674
            }
        } else {
            bioma.geometry=null
        }
        bioma.noBioma = noBioma
        println ' '
        println 'SAVE BIOMA'
        bioma.save()

    }


    /**
     * método para recuperar as informações do bioma para alteração
     * @param sqBioma
     * @return
     */
    def edit(Long sqBioma=0l) {
        if( !sqBioma ){
            throw new Exception('Id do bioma não informado')
        }

        Bioma bioma
        bioma = Bioma.get( sqBioma )
        if( !bioma ){
            throw new Exception('Id do bioma inexistente')
        }

        Map result = [ 'sqBioma'    : bioma.id
                       , 'noBioma'  : bioma.noBioma
                       , 'geojson'   : [ "type": "FeatureCollection"
                       , 'features'  : []
                       ]
        ]
        String cmdSql = """SELECT ST_AsGeoJSON( ge_bioma,13,8) AS geoJson 
                           FROM salve.bioma 
                           WHERE sq_bioma = :sqBioma"""
        sqlService.execSql( cmdSql, [sqBioma:sqBioma] ).each { row ->
            if( row.geoJson ) {
                result.geojson.features.push( [ 'type': 'Feature'
                                               ,'properties': [],
                                                'geometry': JSON.parse( row.geoJson )

                ])
            }
        }
        return result
    }


    /**
     * método para excluir o bioma do banco de dados
     * @return
     */
    def delete(Long sqBioma = 0l) {
        Bioma bioma = Bioma.get( sqBioma )
        if( !bioma ) {
            throw new Exception('Id do bioma não informado')
        }
        bioma.delete()
    }
}
