package br.gov.icmbio

import grails.transaction.Transactional
//import org.apache.poi.xdgf.usermodel.section.geometry.NURBSTo

import java.nio.file.Files
import java.nio.file.attribute.BasicFileAttributes
import java.util.zip.ZipFile

import java.nio.file.*
import java.util.zip.ZipEntry
import java.io.File

@Transactional
class MainService {

    def sqlService
    def emailService
    def dadosApoioService
    def grailsApplication

    /**
     * método padrão para abrir uma planilha
     * @param fileName - arquivo xls ou xlsx
     * @param headerRows - quantidade de linhas compõem o cabeçalho da planilha
     * @param maxRowsAllowed - zero = sem limite
     * @return
     */
    protected PlanilhaParser _createPlanilhaParser(String fileName = '', Integer headerRows = 3, Integer maxRowsAllowed = 0 ){
        File file
        PlanilhaParser pp
        try {
            file = new File( fileName )
            pp = new PlanilhaParser(file)
        } catch( Exception e ) {
            throw new Exception('Erro ao abrir a planilha.')
        }
        if ( pp.getError()) {
            throw new Exception("Erro ao fazer a leitura da planilha. Erro: " + pp.getError())
        }

        // definir a partir de qual linha iniciam os dados
        pp.setHeaderRowNum(headerRows)

        // assumir que os dados estão na sheet 0
        pp.setSheet(0)

        // verificar se a planilha está vazia
        if( pp.getRows() < (headerRows + 1) ) {
            pp.close()
            throw new Exception("Panilha está vazia ou não contém dados na linha ${headerRows+1}")
        }

        // verificar se a planilha possui mais registros que o limite
        if( maxRowsAllowed && ( ( pp.getRows() - headerRows) > maxRowsAllowed ) ) {
            pp.close()
            throw new Exception("A planilha não deve ultrapassar o limite de ${maxRowsAllowed} registros!")
        }
        return pp
    }


    void processarPlanilhaAtualizarReferenciaOcorrencias(String fileName, Sicae user) {
        Util.printLog("INFO::SALVE::${this.getClass()}.processarPlanilhaAtualizarReferenciaOcorrencias");
        // iniciarl procssamento assincrono
        runAsync {
            try {
                List errorsFound = []
                int headerLines = 4
                int totalLines
                int currentLine
                String cmdSql

                // ler planilha
                File file
                PlanilhaParser pp = _createPlanilhaParser( fileName, headerLines )
                totalLines = pp.getRows()

                println 'planilha parser criada'
                println 'Panilha contem ' + ( totalLines-headerLines ) + ' linhas de dados'

                currentLine = headerLines + 1
                while ( currentLine <= totalLines ) {
                    List lineErrors = []

                    println 'Lendo linha ' + currentLine

                    List rowData = pp.getRowData( currentLine )

                    // se a primeira celula estiver vazia considerar fim dos dados
                    if( !rowData || rowData[0].toString().trim() == '' ) {
                        currentLine = totalLines+1
                        continue
                    }
                    // ler valores informados na planilha
                    String sqSalveText      = rowData[0]
                    String sqPublicacaoText = rowData[1] ? rowData[1].toString() : ''
                    String deTituloText     = rowData[2] ? rowData[2].toString() : ''
                    String nuDoiText        = rowData[3] ? rowData[3].toString() : ''
                    String deComPessText    = rowData[4] ? rowData[4].toString() : ''

                    // valores tratados para gravação
                    Long sqFichaOcorrencia = 0l
                    Long sqPublicacao = 0l
                    Integer nuAnoComPess = 0
                    String noAutorComPess = ""

                    // validar ID SALVE
                    if( !sqSalveText =~ /^SALVE: ?[0-9]{1,}$/ ){
                        lineErrors.push('ID Origem: '+ sqSalveText +' está inválido, dever ser informado no formato: SALVE:123456')
                    }
                    sqFichaOcorrencia = sqSalveText.replaceAll(/[^0-9]/,'').toLong()
                    cmdSql = """select fo.sq_ficha_ocorrencia, fo.sq_ficha, ficha.nm_cientifico, ficha.sq_unidade_org
                                        from salve.ficha_ocorrencia fo
                                        inner join salve.ficha on ficha.sq_ficha = fo.sq_ficha
                                        where sq_ficha_ocorrencia = :sqFichaOcorrencia"""
                    List rowsFichaOcorrencia = sqlService.execSql(cmdSql,[sqFichaOcorrencia:sqFichaOcorrencia])
                    if( !rowsFichaOcorrencia ){
                        lineErrors.push('ID Origem: '+ sqFichaOcorrencia +' não cadastrado')
                    }
                    if( rowsFichaOcorrencia[0].sq_unidade_org.toLong() != user.sqUnidadeOrg.toLong()){
                        lineErrors.push('Registro de Ocorrência '+ sqFichaOcorrencia +' pertence a outra unidade.')
                    }

                    // validar ID publicacao
                    if( sqPublicacaoText ) {
                        deComPessText = ''
                        nuDoiText     = ''
                        sqPublicacaoText =  sqPublicacaoText.replaceAll(/,/,'.')
                        try{
                            sqPublicacao = (Long) Double.parseDouble( sqPublicacaoText )
                            Publicacao p = Publicacao.get( sqPublicacao )
                            if( !p ){
                                lineErrors.push('ID Ref Bib SALVE: '+ sqPublicacao +' não cadastrado no banco de dados')
                            }
                        } catch(Exception e) {
                            lineErrors.push('ID Ref Bib SALVE: '+ sqPublicacaoText +' está inválido, dever ser informado no formato: 999999')
                        }
                    }

                    // deve ser infomado o id da publicacao, ou doi ou comunicacao pessoal
                    if( !sqPublicacao && !deComPessText && !nuDoiText ){
                         lineErrors.push('Necessário informar o Id Ref Bib ou DOI ou Comunicação Pessoal')
                    }

                    // validar DOI
                    if( nuDoiText ){
                        deComPessText=''
                        Publicacao p = Publicacao.findByDeDoi(nuDoiText)
                        if( !p ){
                            lineErrors.push('DOI: '+ nuDoiText +' não cadastrado nas referências bibliográficas')
                        }
                    }

                    // validar comunicacao pessoa informada
                    if( deComPessText ){
                        try {
                            if (!deComPessText =~ /^.{1,}, ?\d{4}$/) {
                                throw new Exception('')
                            }
                            nuAnoComPess = deComPessText.substring(deComPessText.length() - 4).toInteger()
                            // remover o ano do final do comPess
                            noAutorComPess = deComPessText.substring(0, deComPessText.length() - 4).trim().replaceAll(/,$/, '')
                        } catch( Exception e ){
                            lineErrors.push('Comunicação pessoal está no formato inválido: <b>' + deComPessText + '</b> Informe o nome e o ano separados por vírgula')
                        }
                    }
                    if( lineErrors ){
                       errorsFound.push('<br>Linha '+currentLine+'<hr>- ' + lineErrors.join('<br>- ' ) )
                       // ler proxima linha
                       currentLine++
                       continue
                    }


                    // localizar a referencia atual do registros
                    // se não existir criar e se existir alterar
                    Map sqlParams = [:]
                    sqlParams.sqFicha = rowsFichaOcorrencia[0].sq_ficha.toLong()
                    sqlParams.sqFichaOcorrencia = sqFichaOcorrencia

                    // localizar as referencias e comunicações pessoais da ocorrência
                    cmdSql = """select frb.sq_ficha_ref_bib from salve.ficha_ref_bib frb
                            where sq_ficha = :sqFicha
                            and sq_registro = :sqFichaOcorrencia
                            and no_tabela = 'ficha_ocorrencia'
                            and no_coluna = 'sq_ficha_ocorrencia'
                            """
                    List rowsFichaRefBib = sqlService.execSql(cmdSql, sqlParams)
                    if( rowsFichaRefBib ) {
                        FichaRefBib frb = FichaRefBib.get( rowsFichaRefBib[0].sq_ficha_ref_bib.toLong() )
                        if ( sqPublicacao ) {
                            frb.publicacao = Publicacao.get( sqPublicacao )
                            // limpar comunicacao pessoa
                            frb.noAutor = null
                            frb.nuAno = null
                        }
                        else if( nuAnoComPess ){
                            // limpar publicacao
                            frb.publicacao = null
                            frb.noAutor = noAutorComPess
                            frb.nuAno = nuAnoComPess
                        }
                        frb.save( flush:true )
                    } else {
                        FichaRefBib frb = new FichaRefBib()
                        frb.ficha = Ficha.get( rowsFichaOcorrencia[0].sq_ficha.toLong() )
                        frb.noTabela='ficha_ocorrencia'
                        frb.noColuna='sq_ficha_ocorrencia'
                        if ( sqPublicacao ) {
                            frb.publicacao = Publicacao.get( sqPublicacao )
                        }
                        else if( nuAnoComPess ){
                            frb.publicacao = null
                            frb.noAutor = noAutorComPess
                            frb.nuAno = nuAnoComPess
                        }
                        frb.save( flush:true )
                    }

                    // ler proxima linha da planilha
                    currentLine++
                }
                // fechar planilha
                pp.close()

                // enviar email com o resultado
                String emailAssunto = 'Sistema SALVE - Resultado Atualização Referências dos Registros'
                String emailMensagem

                if( errorsFound ){
                    emailMensagem = """<p>Segue abaixo os erros encontrados durante o processamento da planilha: ${fileName}<br>${errorsFound.join('<br>')}</p><br><p>Atenciosalmente,<br>Equipe SALVE</p>"""
                } else {
                    emailMensagem = """<p>Atualização das referências bibliográficas dos registros foi processada sem erros.</p><br><p>Atenciosalmente,<br>Equipe SALVE</p>"""
                }
                emailService.sendTo(user.email,emailAssunto, emailMensagem)
            } catch (Exception e) {
                println e.getMessage()
                String msgErro = """Erro no processamento da planilha de atualização das referências bibliográficas dos registros de ocorrência.<br>Planilha enviada: <b>${fileName}</b><br>Erro:<span style="color:#ff0000;">${e.getMessage()}</span>"""
                //emailService.sendTo(user.email, 'Sistema SALVE - Processamento planilha', '<p>' + msgErro + '</p>')
            }
        }
        return
    }

    /**
    * Processa requisição de upload em lote de Histórico de Avaliação (ESTADUAL)
    * @params String fileName - Arquivo da planilha
    * @params Long sqCicloAvaliacao - Ciclo da avaliação
    * @params String requestUrl - URL base
    * @params Sicae user - Usuario logado
    * @return Map res
    */
    private Map processaPlanilhaHistoricoAvaliacao(String fileName, Long sqCicloAvaliacao, String requestUrl, Sicae user) {
        Util.printLog("INFO::SALVE::${this.getClass()}.processaPlanilhaHistoricoAvaliacao");

        Map res = [status:0, msg: '', type:'error']

        DadosApoio tipoAbrangencia = dadosApoioService.getByCodigo('TB_TIPO_ABRANGENCIA','ESTADO')
        // busca o contexto estadual de TB_TIPO_AVALIACAO
        DadosApoio tipo_avaliacao_estadual = dadosApoioService.getByCodigo('TB_TIPO_AVALIACAO', 'ESTADUAL')

        Taxon taxon
        Long sqTaxon
        CicloAvaliacao ciclo
        Ficha ficha
        Estado estado
        DadosApoio categoria
        Integer nuAnoAvaliacao
        String deCategoriaOutra
        String dsCriterioAvalIucn
        String stPossivelmenteExtinta
        String txJustificativaAvaliacao

        String cmdSql
        List rowUF
        Map sqlParams = [:]
        List errors = []
        String msgError
        Long limiteLinhasProcessar = 500
        Long linhaInicioDadosProcessar = 4
        Long linhaFinalDadosProcessar = (linhaInicioDadosProcessar + limiteLinhasProcessar)

        if(!sqCicloAvaliacao){
            throw new Exception('Ops! Não foi possível obter informação do Ciclo de Avaliação.')
        }
        // Busca pelo ciclo
        try{
            ciclo = CicloAvaliacao.get(sqCicloAvaliacao.toInteger())
        }catch(Exception e){
            throw new Exception('Ops! Ocorreu um erro ao consultar o Ciclo de Avaliação.')
        }

        // leitura da planilha
        File file
        PlanilhaParser pp
        try{
            file = new File(fileName)
            pp = new PlanilhaParser(file)
        }catch(Exception e){
            throw new Exception('Ops! Não foi possível abrir a planilha para leitura.')
        }
        if ( pp.getError()) {
            throw new Exception("Ops! Ocorreu um erro ao ler planilha! Erro: " + pp.getError())
        }
        pp.setHeaderRowNum(3)
        pp.setSheet(0)

        // verificar se a planilha está vazia
        if( pp.getRows() < linhaInicioDadosProcessar ){
            throw new Exception("Ops! Panilha está vazia ou não contém dados na linha ${linhaInicioDadosProcessar}")
        }

        // verificar se a planilha possui mais registros que o limite
        if( pp.getRows() > ((linhaInicioDadosProcessar + limiteLinhasProcessar) ) ){
            throw new Exception("Ops! A planilha não deve ultrapassar o limite de ${limiteLinhasProcessar} registros!")
        }

        // variaveis de controle do loop/leitura do arquivo
        Boolean endFileContent = false
        Boolean isValidLine
        List recordSuccess = []
        List recordError = []
        String errorLine = ''
        String value = ''
        Integer lineEmpty

        for(int line in linhaInicioDadosProcessar..linhaFinalDadosProcessar) {
            isValidLine = true
            if(line > pp.getRows()) { // verifica se a linha atual é após as linhas com conteúdo
                isValidLine = false
                endFileContent = true // Identifica linha em branco ou fim dos dados/conteúdo preenchido
                lineEmpty = line
            }else{
                pp.getRowData(line).eachWithIndex
                { v,i ->
                    if(i == 0 && !v){
                        endFileContent = true // Identifica linha em branco. Fim dos daods
                        isValidLine = false // Identifica linha válida para salvar
                        lineEmpty = line
                    }
                    // Mensagem complementar da linha onde ocorreu o erro
                    errorLine = ' Erro na linha: ' + line + ' da planilha.'
                    value = v.toString().trim() // valor da celula
                    switch (i)
                    {
                        case 0: // Nome Científico
                            if(endFileContent){
                                break
                            }
                            try{
                                List rows = _buscaTaxon(value, sqCicloAvaliacao.toInteger())
                                sqTaxon = rows[0].sq_taxon
                                taxon = Taxon.get(sqTaxon)
                                // Busca pela ficha
                                ficha = Ficha.get(rows[0].sq_ficha.toLong())
                                if (!ficha) {
                                    if(!endFileContent){
                                        errors.push("Ficha do taxon: [${value}] não encontrada!" + errorLine)
                                    }
                                    isValidLine = false
                                }
                            }catch(Exception e){
                                if(!endFileContent){
                                    msgError = 'Não foi possível encontrar o taxon: [' + value + '].' + errorLine
                                    Util.printLog('ERRO: Nome Científico', msgError, e.getMessage() );
                                    errors.push(msgError)
                                }
                                isValidLine = false
                            }
                            break
                        case 1: // Estado/UF
                            if(value.length() == 2) { // verifica se é uma sigla do Estado
                                cmdSql = "SELECT sq_estado FROM corporativo.vw_estado WHERE sg_estado ilike :sigla_nome_estado LIMIT 1"
                            }else{ // busca pelo nome do estado
                                cmdSql = "SELECT sq_estado FROM corporativo.vw_estado WHERE fn_remove_acentuacao(no_estado) ilike :sigla_nome_estado LIMIT 1"
                            }
                            sqlParams.sigla_nome_estado = Util.removeAccents(value)
                            try{
                                rowUF = sqlService.execSql(cmdSql, sqlParams)
                                if(rowUF){
                                    estado = Estado.get( rowUF[0].sq_estado )
                                }else{
                                    if(!endFileContent){
                                        msgError = "Estado/UF não encontrado na base de dados! Estado informado: ${value}." + errorLine
                                        errors.push(msgError)
                                    }
                                    isValidLine = false
                                }
                            }catch(Exception e){
                                if(!endFileContent){
                                    msgError = "Estado/UF não encontrado na base de dados! Estado informado: ${value}." + errorLine
                                    errors.push(msgError)
                                }
                                isValidLine = false
                            }
                            if (!estado) {
                                if(!endFileContent){
                                    msgError = "Estado: [${value}] não encontraddo na base de dados!" + errorLine
                                    errors.push(msgError)
                                }
                                isValidLine = false
                            }
                            break
                        case 2: // ANO
                            try{
                                if (value != null && !value.isEmpty()) {
                                    nuAnoAvaliacao = (int)Double.parseDouble(value)
                                    if(nuAnoAvaliacao < 1500 || nuAnoAvaliacao > 2099 ) {
                                        if(!endFileContent){
                                            msgError = "Informe um ano válido. Ex.: 2021!" + errorLine
                                            errors.push(msgError)
                                        }
                                        isValidLine = false
                                    }
                                }else{
                                    if(!endFileContent){
                                        msgError = "Informe um ano válido. Ex.: 2021!" + errorLine
                                        errors.push(msgError)
                                    }
                                    isValidLine = false
                                }
                            }catch(Exception e){
                                if(!endFileContent){
                                    msgError = "Informe um ano válido. Ex.: 2021!" + errorLine
                                    errors.push(msgError)
                                }
                                isValidLine = false
                            }
                            break
                        case 3: // Categoria
                            try{
                                categoria = DadosApoioService.getByDescricaoSigla('TB_CATEGORIA_IUCN',value)
                            }catch(Exception e){
                                if(!endFileContent){
                                    msgError = "Ocorreu um erro ao consultar a categoria informada [${value}]." + errorLine
                                    errors.push(msgError)
                                }
                                isValidLine = false
                            }
                            if(!categoria){
                                if(!endFileContent){
                                    msgError = "A categoria informada: [${value}] não foi encontrada na base de dados." + errorLine
                                    errors.push(msgError)
                                }
                                isValidLine = false
                            }
                            break
                        case 4: // Outra Categoria
                            deCategoriaOutra = null
                            if(categoria && categoria.codigoSistema == 'OUTRA'){
                                deCategoriaOutra = value
                                if (!deCategoriaOutra) {
                                    if(!endFileContent){
                                        msgError = "Nome da outra categoria não informada." + errorLine
                                        errors.push(msgError)
                                    }
                                    isValidLine = false
                                }
                            }
                            break
                        case 5: // Critério
                            dsCriterioAvalIucn = null
                            if (value != null && !value.isEmpty()) {
                                dsCriterioAvalIucn = value
                            }
                            break
                        case 6: // Possivelmente Extinta?
                            stPossivelmenteExtinta = null
                            if(value.toUpperCase().indexOf('S') >= 0){
                                stPossivelmenteExtinta = 'S'
                            }else if(value.toUpperCase().indexOf('N') >= 0){
                                stPossivelmenteExtinta = 'N'
                            }
                            break
                        case 7: // Justificativa
                            txJustificativaAvaliacao = null
                            if (value != null && !value.isEmpty()) {
                                txJustificativaAvaliacao = Util.stripTagsFichaPdf(value)
                            }
                            break
                    } // END switch (i) {
                } // END pp.getRowData(line).eachWithIndex
            } // ELSE => if(line > pp.getRows())

            // se a linha da planilha não for válida para importar
            if(!isValidLine){
                if(line != lineEmpty){
                    recordError.push(line)
                }
                if(!endFileContent){
                    continue
                }
            }else{ // linha da planilha ok para importar
                recordSuccess.push(line)
            }

            // Se NÂO chegou no fim de leitura dos dados da planilha
            if(!endFileContent){
                // ...Continua processando os insert/update dos registros válidos até finalizar a planilha
                cmdSql = """
                        SELECT sq_taxon_historico_avaliacao FROM salve.taxon_historico_avaliacao
                        WHERE sq_tipo_avaliacao = :sq_tipo_avaliacao
                        AND sq_abrangencia = :sq_abrangencia
                        AND nu_ano_avaliacao = :nu_ano_avaliacao
                        AND sq_taxon = :sq_taxon
                        AND sq_categoria_iucn = :sq_categoria_iucn
                        AND tx_justificativa_avaliacao = :tx_justificativa_avaliacao
                        """
                sqlParams = [:]
                sqlParams.sq_tipo_avaliacao         = tipo_avaliacao_estadual.id // == 313 == ESTADUAL
                sqlParams.sq_abrangencia            = Abrangencia.findByEstado( estado ).id
                sqlParams.nu_ano_avaliacao          = nuAnoAvaliacao
                sqlParams.sq_taxon                  = sqTaxon
                sqlParams.sq_categoria_iucn         = categoria.id
                sqlParams.tx_justificativa_avaliacao= txJustificativaAvaliacao
                List rowsHist = sqlService.execSql(cmdSql, sqlParams)

                TaxonHistoricoAvaliacao reg
                if (rowsHist && rowsHist.size() > 0) {
                    // UPDATE
                    reg = TaxonHistoricoAvaliacao.get(rowsHist[0].sq_taxon_historico_avaliacao.toLong())
                }else{
                    // INSERT
                    reg = new TaxonHistoricoAvaliacao()
                    reg.taxon = taxon
                }
                reg.tipoAvaliacao = tipo_avaliacao_estadual
                reg.categoriaIucn = categoria
                // se não for ameacada nao tem criterio
                if ( !  (reg?.categoriaIucn?.codigoSistema ==~ /CR|VU|EN/ ) ) {
                    dsCriterioAvalIucn = null
                }
                if (!stPossivelmenteExtinta) {
                    stPossivelmenteExtinta = null
                }
                if (reg?.categoriaIucn?.codigoSistema != 'CR') {
                    stPossivelmenteExtinta = null
                }else {
                    if (!stPossivelmenteExtinta) {
                        stPossivelmenteExtinta = 'N'
                    }
                }
                reg.stPossivelmenteExtinta = stPossivelmenteExtinta ?: null
                reg.abrangencia = Abrangencia.findByEstado( estado )
                reg.noRegiaoOutra = null
                reg.nuAnoAvaliacao = nuAnoAvaliacao.toInteger()
                reg.deCategoriaOutra = deCategoriaOutra ?: null
                reg.deCriterioAvaliacaoIucn = dsCriterioAvalIucn ?: null
                reg.txJustificativaAvaliacao = txJustificativaAvaliacao ?: null
                try{
                    reg.save(flush: true)
                }catch(Exception e){
                    errors.push("Ocorreu um erro ao tentar salvar o registro. "+reg.getErrors().' ERRO: '+e.getMessage())
                    recordError.push(line)
                }

                try{// atualizar log da ficha, quem alterou
                    Ficha fichaLog = Ficha.get( ficha.id )
                    fichaLog.dtAlteracao = new Date()
                    fichaLog.save()
                }catch(Exception e){
                    Util.printLog("INFO::SALVE::${this.getClass()}.processaPlanilhaHistoricoAvaliacao"
                        , "Erro ao salvar data de alteração da ficha"
                        , e.getMessage() );
                }
            } else{
                // Se já chegou no fim dos dados da planilha...
                String msgImport = ''
                if(recordSuccess.size()){
                    msgImport += "${recordSuccess.size()} Registro(s) importado(s) com SUCESSO.\n"
                }
                if(recordError.size()){
                    msgImport += "<br>${recordError.size()} ERRO(S) encontrado(s).<br>Verifique seu email: [${user.email}] para obeter o log dos erros."
                }
                pp.close()
                // se houve algum envio com sucesso
                if(recordSuccess.size()){
                    res = [status:1, msg: msgImport, type:'success']
                }else{
                    res = [status:0, msg: msgImport, type:'error']
                }
                // envia o log APENAS se contém algum erro
                if(errors.size()){
                    _enviaEmailLogHistoricoAvaliacao( recordSuccess, recordError, errors, requestUrl, user)
                }
                return res
            }

        } // END for
        pp.close()
        return res
    }

    /**
    * Envia email com o log dos erros ocorridos na Importação de Histórico de Avaliação
    * @params List recordSuccess - Números das linhas da planilhas importadas com sucesso
    * @params List recordError - Números das linhas da planilhas com problemas
    * @params List errors - Lista dos problemas
    * @params String requestUrl - Base da url
    * @params Sicae user - Usuario logado
    * @return void
    */
    protected void _enviaEmailLogHistoricoAvaliacao(List recordSuccess, List recordError, List errors, String requestUrl, Sicae user){
        Util.printLog("INFO::SALVE::${this.getClass()}._enviaEmailLogHistoricoAvaliacao()" );

        String strDateTime  = new Date().format('dd-MM-yyyy-HH-mm-ss').toString()
        String fileName     = 'log-' + strDateTime + '.txt'
        String outputDir    = '/data/salve-estadual/temp/historico_avaliacao/'
        String assunto      = 'Log de Importação de Histórico de Avaliação - ' + new Date().format('dd/MM/yyyy HH:mm:ss').toString()
        String mensagem     = """<p style="font-size:18px;font-family:Times New Roman, Arial">Log de Importação de Histórico de Avaliação ${new Date().format('dd/MM/yyyy HH:mm:ss')}<br>
        <br>Para visualizar detalhes dos erros <a href="${requestUrl}download?fileName=${outputDir}${fileName}" target="_blank">clique aqui</a>.
        <br>Obs.: Este arquivo de log ficará disponível apenas por 05 dias.
        <br><br>Atenciosamente,<br>
        Equipe SALVE.
        </p>
        """
        String strLog       = """Log de Importação de Histórico de Avaliação

        Total de registros importados: ${recordSuccess.size()}
        Nº das linhas importadas: ${recordSuccess.join(', ').toString()}

        Total de linhas com problemas: ${recordError.size()}
        Nº das linhas com problemas: ${recordError.join(', ').toString()}

        Detalhes dos problemas:
        """
        List logs = [strLog]
        logs += errors
        try{
            new File(outputDir,fileName).withWriter('utf-8') {
                writer -> writer.write(logs.join('\n'))
            }
            emailService.sendTo(user.email, assunto, mensagem, grailsApplication.config.app.email )
        }catch(Exception e){
            msgError 'Ocorreu um erro ao enviar email: emailService.sendTo()'
            Util.printLog("ERRO::SALVE::${this.getClass()}._enviaEmailLogHistoricoAvaliacao()", msgError, e.getMessage() );
        }
    }

    /**
    * Processa requisição de upload em lote de mapas
    * @params String zipFileName, String dirMapsUserUpload, Long sqCicloAvaliacao, String requestUrl, Sicae user
    * @return Map res
    */
    private Map processaUploadMapas(String zipFileName, String dirMapsUserUpload, Long sqCicloAvaliacao, String requestUrl, Sicae user) {
        Util.printLog('INFO::SALVE::MainService.processaUploadMapas()');
        Map res     = [status:0, msg: '', type:'error']
        res = _descompactarMapas( zipFileName )
        if(res.outputDir && res.nomeArquivoPlanilha){
            res = _salvaMapasFichaAnexo(res.outputDir, res.nomeArquivoPlanilha, dirMapsUserUpload, sqCicloAvaliacao, requestUrl, user)
        }
        return res
    }
    /**
    * Descompactar os arquivos dos mapas(01 planilhas e N imagens)
    * @params String zipFileName
    * @return Map
    */
    protected Map  _descompactarMapas( String zipFileName ) {
        Util.printLog('INFO::SALVE::MainService._descompactarMapas()', 'zipFileName: '+zipFileName );

        List errors = []
        String msgError = ''
        String nomeArquivoPlanilha = ''
        String outputDir = ''
        Integer totalPlanilha = 0
        Integer totalImages = 0

        try {
            File file = new File( zipFileName )
            ZipFile zip = new ZipFile( file )
            outputDir = file.getAbsolutePath().replaceAll( file.getName(), '')
            String mapaName = file.getName().replaceAll(/\.zip$/, '')

            zip.entries().each {
                if ( ! it.isDirectory()) {
                    // valida extensoes | Permitir apenas XLSX, XLS, JPG, JPEG, PNG
                    String ext = Util.getFileExtension(it.name.toLowerCase())
                    String fileNameWithoutPath = it.name.replace(mapaName+'/','')
                    if (!['png', 'jpg', 'jpeg', 'bmp', 'xlsx', 'xls', 'zip'].contains(ext)) {
                        msgError = 'Extensão ' + ext + ' não permitida! Arquivos permitidos: .png .jpg .jpeg .bmp .xlsx .xls .zip Seu arquivo: ' + it.name
                        Util.printLog('ERRO::SALVE::MainService._descompactarMapas()', msgError );
                        errors.push(msgError)
                    }
                    // Verifica se tem imagens ou zip/shapefile
                    if (['png', 'jpg', 'jpeg', 'bmp', 'zip'].contains(ext)) {
                        totalImages++
                    }
                    // verifica se veio mais de UMA planilha no zip e guarda o nome
                    if (['xlsx', 'xls'].contains(ext)) {
                        nomeArquivoPlanilha = fileNameWithoutPath //it.name
                        totalPlanilha++
                    }
                    // valida tamanho do arquivo
                    if (it.size > (4 * 1048576)) { // 4Mb | 1048576 == 1Mb
                        msgError = 'Tamanho do arquvo ' + it.name + '(' + it.size + ' bytes)' + ' excede limite permitido! (4Mb)'
                        Util.printLog('ERRO::SALVE::MainService._descompactarMapas()', msgError );
                        errors.push(msgError)
                    }
                    def fOut = new File(outputDir + File.separator + fileNameWithoutPath)
                    //create output dir if not exists
                    new File(fOut.parent).mkdirs()
                    InputStream is = zip.getInputStream(it)
                    Files.copy(is, fOut.toPath(), StandardCopyOption.REPLACE_EXISTING)
                }
            }
            zip.close()
        }catch (Exception e) {
            msgError = 'Ocorreu um erro ao extrair os arquivos!'
            Util.printLog('ERRO::SALVE::MainService._descompactarMapas()', msgError, e.getMessage() );
            errors.push(msgError)
        }
         // verifica se veio mais de UMA planilha no zip e guarda o nome
        if (totalPlanilha > 1 ) {
            msgError = 'Existe mais de uma planilha no arquivo ZIP! O arquivo zip deve conter apenas uma planilha com a lista dos mapas que está sendo enviado.'
            Util.printLog('ERRO::SALVE::MainService._descompactarMapas()', msgError );
            errors.push(msgError)
        }
        // verifica se tinha imagem no zip
        if(totalImages == 0){
            msgError = 'Não foi encontrado nenhuma imagem(png,jpg,jpeg,bmp) ou shapefile(zip) de mapas no arquivo ZIP! Verifique se compactou(zip) todos os arquivos fora de pastas ou subpastas.'
            Util.printLog('ERRO::SALVE::MainService._descompactarMapas()', msgError );
            errors.push(msgError)
        }
        // se não enviou planilha mostra mensagem de erro
        if ( nomeArquivoPlanilha == '') {
            msgError = 'Falta a planilha (.xls ou .xlsx) no arquivo ZIP!'
            Util.printLog('ERRO::SALVE::MainService._descompactarMapas()', msgError );
            errors.push(msgError)
        }
        // verifica se houve erros
        Map res
        if (errors.size()) {
            res     = [status:0, msg:errors.join('<br>'), type:'error']
        }else{
            res     = [outputDir: outputDir, nomeArquivoPlanilha: nomeArquivoPlanilha]
        }
        return res
    }

    /**
    * Insere mapas oriundo de planilha/zip importados
    * @params String outputDir, String fileName, String dirMapsUserUpload, Long sqCicloAvaliacao, String requestUrl, Sicae user
    * @return Map
    */
    private Map  _salvaMapasFichaAnexo(String outputDir, String fileName, String dirMapsUserUpload, Long sqCicloAvaliacao, String requestUrl, Sicae user ) {
        Util.printLog('INFO::SALVE::MainService._salvaMapasFichaAnexo()' );

        Map res     = [status:0, msg: 'Foram encontrados alguns erros. Verifique seu e-mail para mais informações.', type:'error']
        Taxon taxon
        Ficha ficha
        Long sqTaxon
        Date dtAnexo
        String deLegenda
        String inPrincipal
        String noArquivo
        List errors = []
        String msgError
        Long limiteLinhasProcessar = 300
        Long linhaInicioDadosProcessar = 4
        Long linhaFinalDadosProcessar = (linhaInicioDadosProcessar + limiteLinhasProcessar)

        if(!sqCicloAvaliacao){
            msgError = 'Ops! Não foi possível obter informação do Ciclo de Avaliação.'
            Util.printLog('ERRO::SALVE::MainService._salvaMapasFichaAnexo()', msgError );
            throw new Exception(msgError)
        }
        // Busca pelo ciclo
        CicloAvaliacao ciclo
        try{
            Integer sq_ciclo = sqCicloAvaliacao.toInteger()
            ciclo = CicloAvaliacao.get(sq_ciclo)
        }catch(Exception e){
            msgError = 'Ops! Ocorreu um erro ao consultar o Ciclo de Avaliação.'
            Util.printLog('ERRO::SALVE::MainService._salvaMapasFichaAnexo()', msgError,  e.getMessage() );
            throw new Exception(msgError)
        }
        // Busca pelo contexto
        DadosApoio contexto
        try{
            contexto = dadosApoioService.getByCodigo('TB_CONTEXTO_ANEXO', 'MAPA_DISTRIBUICAO')
        }catch(Exception e){
            msgError = 'Ops! Não foi possível obter informação dos dados de apoio/mapa_distribuição.'
            Util.printLog('ERRO::SALVE::MainService._salvaMapasFichaAnexo()', msgError,  e.getMessage() );
            throw new Exception(msgError)
        }

        // leitura da planilha
        File file
        PlanilhaParser pp
        try{
            file = new File(outputDir + fileName)
            pp = new PlanilhaParser(file)
        }catch(Exception e){
            msgError = 'Ops! Não foi possível abrir a planilha para leitura.'
            Util.printLog('ERRO::SALVE::MainService._salvaMapasFichaAnexo()', msgError,  e.getMessage() );
            throw new Exception(msgError)
        }
        if ( pp.getError()) {
            msgError = "Ops! Ocorreu um erro ao ler planilha! Erro: " + pp.getError()
            Util.printLog('ERRO::SALVE::MainService._salvaMapasFichaAnexo()', msgError );
            throw new Exception(msgError)
        }
        pp.setHeaderRowNum(3)
        pp.setSheet(0)

        // verificar se a planilha está vazia
        if( pp.getRows() < linhaInicioDadosProcessar ){
            msgError = "Ops! Panilha está vazia!"
            Util.printLog('ERRO::SALVE::MainService._salvaMapasFichaAnexo()', msgError );
            throw new Exception(msgError)
        }

        // verificar se a planilha possui mais registros que o limite
        if( pp.getRows() > ((linhaInicioDadosProcessar + limiteLinhasProcessar) ) ){
            msgError = "Ops! A planilha não deve ultrapassar mais de ${limiteLinhasProcessar} registros!"
            Util.printLog('ERRO::SALVE::MainService._salvaMapasFichaAnexo()', msgError );
            throw new Exception(msgError)
        }

        Boolean endFileContent = false
        Boolean isValidLine
        List recordSuccess = []
        List recordError = []
        String errorLine = ''
        String value = ''
        Integer lineEmpty
        for(int line in 4..linhaFinalDadosProcessar) {
            isValidLine = true
            pp.getRowData(line).eachWithIndex { v,i ->
                if(i == 0 && !v){
                    endFileContent = true // Identifica linha em branco. Fim dos daods
                    isValidLine = false // Identifica linha válida para salvar
                    lineEmpty = line
                }
                // Mensagem complementar da linha onde ocorreu o erro
                errorLine = ' Erro na linha: ' + line + ' da planilha.'
                value = v.toString() // valor da celula
                switch (i) {
                    case 0: // Nome Científico
                        if(endFileContent){
                            break
                        }
                        try{
                            List rows = _buscaTaxon(value, sqCicloAvaliacao.toInteger())
                            sqTaxon = rows[0].sq_taxon
                            taxon = Taxon.get(sqTaxon)
                        }catch(Exception e){
                            if(!endFileContent){
                                msgError = 'Não foi possível encontrar o taxon: [' + value + '].' + errorLine
                                Util.printLog('ERRO::SALVE::MainService._salvaMapasFichaAnexo()', msgError,  e.getMessage() );
                                errors.push(msgError)
                            }
                            isValidLine = false
                        }
                        // Busca pela ficha
                        try{
                            ficha = Ficha.findByCicloAvaliacaoAndTaxon(ciclo, taxon)
                        }catch(Exception e){
                            if(!endFileContent){
                                msgError = "Ocorreu um erro ao consultar Ciclo: [${ciclo}] e Taxon: [${taxon}]." + errorLine
                                Util.printLog('ERRO::SALVE::MainService._salvaMapasFichaAnexo()', msgError,  e.getMessage() );
                                errors.push(msgError)
                            }
                            isValidLine = false
                        }
                        if (!ficha) {
                            if(!endFileContent){
                                msgError = "Ficha do taxon: [${value}] não encontrada!" + errorLine
                                errors.push(msgError)
                            }
                            isValidLine = false
                        }
                        break
                    case 1: // Legenda / observação
                        if(value.length() < 5) {
                            if(!endFileContent){
                                msgError = "Informação muito curta na legenda para o mapa!" + errorLine
                                errors.push(msgError)
                            }
                            isValidLine = false
                        }else if(value.length() > 200) {
                            if(!endFileContent){
                                msgError = "A legenda deve conter no máximo 200 caracteres!" + errorLine
                                errors.push(msgError)
                            }
                            isValidLine = false
                        }
                        deLegenda = value
                        break
                    case 2: // Data do arquivo
                        try {
                            dtAnexo = new Date().parse('dd/MM/yyyy', value )
                        }
                        catch (Exception e1) {
                            try {
                                dtAnexo = new Date().parse('dd-MM-yyyy', value )
                            }
                            catch (Exception e2) {
                                try{
                                    dtAnexo = new Date().parse('dd-MMM-yyyy', value )
                                }catch (Exception e3) {
                                    try{
                                        dtAnexo = new Date().parse('yyyy-MMM-dd', value )
                                    }catch(Exception e4){
                                        if(!endFileContent){
                                            msgError = "Informe uma data válida com formato dd/mm/aaaa. Data informada: [${value}]." + errorLine
                                            Util.printLog('ERRO::SALVE::MainService._salvaMapasFichaAnexo()', msgError,  e4.getMessage() );
                                            errors.push(msgError)
                                        }
                                        isValidLine = false
                                    }
                                }
                            }
                        }
                        break
                    case 3: // Imagem principal ou shape da distribuição geográfica Atual?
                        inPrincipal = 'N'
                        if(value.toUpperCase().indexOf('S') >= 0){
                            inPrincipal = 'S'
                        }
                        break
                    case 4: // Nome do arquivo
                        if(!value.length()) {
                            if(!endFileContent){
                                msgError = "Informe o nome da imagem!" + errorLine
                                errors.push(msgError)
                            }
                            isValidLine = false
                        }else if(value.length() > 255) {
                            if(!endFileContent){
                                msgError = "O nome do arquivo deve conter no máximo 255 caracteres!" + errorLine
                                errors.push(msgError)
                            }
                            isValidLine = false
                        }
                        noArquivo = value
                        String pathFileImage = outputDir + noArquivo
                        File fileImage
                        try{
                            fileImage = new File(pathFileImage)
                        }catch(Exception e){
                            if(!endFileContent){
                                msgError = "Ocorreu um erro tentar abrir o arquivo do mapa, Arquivo: [${pathFileImage}]." + errorLine
                                Util.printLog('ERRO::SALVE::MainService._salvaMapasFichaAnexo()', msgError,  e.getMessage() );
                                errors.push(msgError)
                            }
                            isValidLine = false
                        }
                        if (!fileImage.exists()) {
                            if(!endFileContent){
                                msgError = "Arquivo do mapa não existe no ZIP. Nome da imagem: ${value}." + errorLine
                                errors.push(msgError)
                            }
                            isValidLine = false
                        }
                        break
                }
            } // END pp.getRowData(line).eachWithIndex

            // se a linha da planilha não for válida para importar
            if(!isValidLine){
                if(line != lineEmpty){
                    recordError.push(line)
                }
                if(!endFileContent){
                    continue
                }
            }else{ // linha da planilha ok para importar
                recordSuccess.push(line)
            }

            // verifica se já chegou no fim dos dados da planilha
            if(endFileContent){
                String msgImport = ''
                if(recordSuccess.size()){
                    msgImport += "${recordSuccess.size()} mapa(s) importado(s) com sucesso.\n"
                }
                if(recordError.size()){
                    msgImport += "<br>${recordError.size()} ERRO(S) encontrado(s).<br>Verifique seu email: [${user.email}] para obeter o log dos erros."
                }
                // envia o log caso ocorra erro
                _enviaEmailLogProcessaUploadMapas( recordSuccess,  recordError,  errors, outputDir, dirMapsUserUpload, requestUrl, user)
                pp.close()

                if(recordSuccess.size()){
                    res.type = 'success'
                    res.msg = msgImport
                }
                return res
            }

            // salva os arquvos no diretório
            String pathFileImage = outputDir + noArquivo
            FileInputStream fis
            File fileImage
            try{
                fileImage = new File(pathFileImage)
                fis = new FileInputStream(fileImage)
            }catch(Exception e){
                msgError = "Ocorreu um erro ao tentar abrir arquivo do mapa para copiar. Arquivo [${pathFileImage}]." + errorLine
                Util.printLog('ERRO::SALVE::MainService._salvaMapasFichaAnexo()', msgError,  e.getMessage() );
            }
            String fileNameMapa = pathFileImage.replaceAll(' |, |\\(.+\\)', '_').replaceAll('_+', '-').replaceAll('-\\.', '.')
            String fileExtension = fileNameMapa.substring(fileNameMapa.lastIndexOf('.'))
            String hashFileName = fis.text.toString().encodeAsMD5() + fileExtension
            try{
                File savePath = new File( grailsApplication.config.anexos.dir)
                fileNameMapa = savePath.absolutePath + '/' + hashFileName
                if (!new File(fileNameMapa).exists()) {
                    fileImage.renameTo(new File(fileNameMapa))
                    ImageService.createThumb(fileNameMapa)
                }
            }catch(Exception e){
                msgError = "Ocorreu um erro ao tentar salvar arquivo do mapa." + errorLine
                Util.printLog('ERRO::SALVE::MainService._salvaMapasFichaAnexo()', msgError,  e.getMessage() );
            }
            // get mimeType
            Path path = new File(pathFileImage).toPath();
            String mimeType = Files.probeContentType(path);
            // Busca pela ficha/contexto/arquivo
            FichaAnexo fa = FichaAnexo.findByFichaAndContextoAndDeLocalArquivo(ficha, contexto, hashFileName)
            try{
                String typeSave = 'UPDATE'
                if (!fa) {
                    fa = new FichaAnexo()
                    fa.ficha = ficha
                    fa.contexto = contexto
                    fa.deLocalArquivo = hashFileName
                    typeSave = 'INSERT'
                }
                fa.deLegenda = deLegenda
                fa.noArquivo = noArquivo
                fa.deTipoConteudo = mimeType
                fa.inPrincipal = inPrincipal
                fa.dtAnexo = dtAnexo
                fa.save(flush: true)
                Util.printLog('INFO::SALVE::MainService._salvaMapasFichaAnexo()', 'FichaAnexo SALVO - '+typeSave );
            }catch(Exception e){
                msgError = 'Ocorreu um erro ao tentar salvar anexos da ficha'
                Util.printLog('ERRO::SALVE::MainService._salvaMapasFichaAnexo()', msgError,  e.getMessage() );
                throw new Exception(msgError)
            }
        }
        pp.close()
        return res
    }

    /**
    * Busca o taxon pelo nome
    * @params String noTaxon
    * $return List
    */
    protected List _buscaTaxon(String noTaxon, Integer sq_ciclo){
        String msgError
        String sql ="""
                    SELECT f.sq_taxon, f.sq_ficha
                    FROM salve.ficha f
                    INNER JOIN taxonomia.taxon AS t ON f.sq_taxon = t.sq_taxon
                    WHERE t.json_trilha->'especie'->>'no_taxon' = :taxon
                    AND f.sq_ciclo_avaliacao = :cilco
                    AND t.sq_nivel_taxonomico IN (7,8)
                    AND t.st_ativo = TRUE
                    LIMIT 1
                    """
        List row
        try{
            row = sqlService.execSql(sql, [
                'taxon' : noTaxon.trim().toLowerCase().capitalize()
                ,'cilco' : sq_ciclo
                ])
            if (!row) {
                msgError = 'Não foi possível encontrar o taxon: ' + noTaxon
                throw new Exception(msgError)
            }
            return row
        }catch(Exception e){
            msgError = 'Erro ao consultar taxon.'
            Util.printLog('ERRO::SALVE::MainService._buscaTaxon()', msgError,  e.getMessage() );
            throw new Exception(e.getMessage())
        }
    }
    /**
    * Envia email com o log dos erros ocorridos na importação
    * @params List recordSuccess, List recordError, List errors, String outputDir, String dirMapsUserUpload, String requestUrl, Sicae user
    * @return void
    */
    protected void _enviaEmailLogProcessaUploadMapas(List recordSuccess, List recordError, List errors, String outputDir, String dirMapsUserUpload, String requestUrl, Sicae user){
        Util.printLog('INFO::SALVE::MainService._enviaEmailLogProcessaUploadMapas()' );
        // Apenas envia se exitir pelo menos 1 erro na importação
        if(!errors.size()){
            return
        }
        String strLog =
"""
Total de registros importados: ${recordSuccess.size()}.
Nº das linhas importadas: ${recordSuccess.join(', ').toString()}.

Total de linhas com problemas: ${recordError.size()}.
Nº das linhas com problemas: ${recordError.join(', ').toString()}.

Detalhes dos problemas:
"""
        // salva o arquivo de log
        String strDateTime = new Date().format('dd-MM-yyyy-HH-mm-ss').toString()
        String fileName = 'log-' + strDateTime + '.txt'
        List logs = [strLog]
        logs += errors
        try{
            new File(outputDir,fileName).withWriter('utf-8') {
                writer -> writer.write(logs.join('\n'))
            }
        }catch(Exception e){
            msgError 'Erro ao gravar arquivo de log: ' + outputDir +  fileName
            Util.printLog('ERRO::SALVE::MainService._enviaEmailLogProcessaUploadMapas()', msgError,  e.getMessage() );
        }
        // envia o email com o link para o log
        String mensagem="""<p style="font-size:18px;font-family:Times New Roman, Arial">Log de importação de mapas em ${new Date().format('dd/MM/yyyy HH:mm:ss')}<br>
                <br>Para visualizar detalhes dos erros <a href="${requestUrl}download?fileName=/data/salve-estadual/temp/mapas/${dirMapsUserUpload}/${fileName}" target="_blank">clique aqui</a>.
                <br>Obs.: Este arquivo de log ficará disponível apenas por 05 dias.
                <br><br>Atenciosamente,<br>
                Equipe SALVE.
                </p>
                """
        String assunto = 'Log de importação de mapas - ' +  new Date().format('dd/MM/yyyy HH:mm:ss').toString()
        try{
            emailService.sendTo(user.email, assunto, mensagem, grailsApplication.config.app.email )
        }catch(Exception e){
            msgError 'Ocorreu um erro ao enviar email: emailService.sendTo()'
            Util.printLog('ERRO::SALVE::MainService._enviaEmailLogProcessaUploadMapas()', msgError,  e.getMessage() );
        }
    }

    /**
    * Processa requisição de upload em lote de fotos ou áudios
    * @params String zipFileName, Long sqCicloAvaliacao, String requestUrl, Sicae user
    * @return Map res
    */
    private Map processaUploadFotosAudios(String zipFileName, Long sqCicloAvaliacao, String requestUrl, Sicae user) {
        Util.printLog('INFO::SALVE::MainService.processaUploadFotosAudios()');
        Map res     = [status:0, msg: '', type:'error']
        res = _descompactarFotosAudios( zipFileName )
        if(res.outputDir && res.nomeArquivoPlanilha){
            res = _salvaFotosAudios(res.outputDir, res.nomeArquivoPlanilha, sqCicloAvaliacao, requestUrl, user)
        }
        return res
    }
    /**
    * Descompactar os arquivos de multimidia (01 planilhas e N imagens e N áudios)
    * @params String zipFileName
    * @return Map
    */
    protected Map  _descompactarFotosAudios( String zipFileName ) {
        Util.printLog('INFO::SALVE::MainService._descompactarFotosAudios()', 'zipFileName: '+zipFileName );

        List errors = []
        String msgError = ''
        String nomeArquivoPlanilha = ''
        String outputDir = ''
        Integer totalPlanilha = 0
        Integer totalArquivos = 0

        try {
            File file = new File( zipFileName )
            ZipFile zip = new ZipFile( file )
            outputDir = file.getAbsolutePath().replaceAll( file.getName(), '')

            zip.entries().each {
                if ( ! it.isDirectory()) {
                    // valida extensoes | Permitir apenas MP3, XLSX, XLS, JPG, JPEG, PNG
                    String ext = Util.getFileExtension(it.name.toLowerCase())
                    if (!['mp3','png','jpg','jpeg','bmp','xlsx','xls','zip'].contains(ext)) {
                        msgError = 'Extensão ' + ext + ' não permitida! Arquivos permitidos: .mp3 .png .jpg .jpeg .bmp .xlsx .xls .zip Seu arquivo: ' + it.name
                        Util.printLog('ERRO::SALVE::MainService._descompactarFotosAudios()', msgError );
                        errors.push(msgError)
                    }
                    // Verifica se tem imagens, mp3 ou zip
                    if (['mp3','png', 'jpg', 'jpeg', 'bmp', 'zip'].contains(ext)) {
                        totalArquivos++
                    }
                    // verifica se veio mais de UMA planilha no zip e guarda o nome
                    if (['xlsx', 'xls'].contains(ext)) {
                        nomeArquivoPlanilha = it.name
                        totalPlanilha++
                    }

                    //validar somente o tamanho das imagens
                    if (! ['xlsx', 'xls'].contains(ext)) {
                        // valida tamanho do arquivo
                        if (it.size > (10 * 1048576)) { // 10Mb | 1048576 == 1Mb
                            msgError = 'Tamanho do arquvo ' + it.name + '(' + it.size + ' bytes)' + ' excede limite permitido! (10Mb)'
                            Util.printLog('ERRO::SALVE::MainService._descompactarFotosAudios()', msgError);
                            errors.push(msgError)
                        }
                    }
                    def fOut = new File(outputDir + File.separator + it.name)
                    //create output dir if not exists
                    new File(fOut.parent).mkdirs()
                    InputStream is = zip.getInputStream(it)
                    Files.copy(is, fOut.toPath(), StandardCopyOption.REPLACE_EXISTING)
                }
            }
            zip.close()
        }catch (Exception e) {
            msgError = 'Ocorreu um erro ao extrair os arquivos!'
            Util.printLog('ERRO::SALVE::MainService._descompactarFotosAudios()', msgError, e.getMessage() );
            errors.push(msgError)
        }
         // verifica se veio mais de UMA planilha no zip e guarda o nome
        if (totalPlanilha > 1 ) {
            msgError = 'Existe mais de uma planilha no arquivo ZIP! O arquivo zip deve conter apenas uma planilha com a lista dos arquivos que está sendo enviado.'
            Util.printLog('ERRO::SALVE::MainService._descompactarFotosAudios()', msgError );
            errors.push(msgError)
        }
        // verifica se tinha imagem no zip
        if(totalArquivos == 0){
            msgError = 'Não foi encontrado nenhuma imagem(png,jpg,jpeg,bmp) ou áudio(mp3) de multimidia no arquivo ZIP! Verifique se compactou(zip) todos os arquivos fora de pastas ou subpastas.'
            Util.printLog('ERRO::SALVE::MainService._descompactarFotosAudios()', msgError );
            errors.push(msgError)
        }
        // se não enviou planilha mostra mensagem de erro
        if ( nomeArquivoPlanilha == '') {
            msgError = 'Falta a planilha (.xls ou .xlsx) no arquivo ZIP!'
            Util.printLog('ERRO::SALVE::MainService._descompactarFotosAudios()', msgError );
            errors.push(msgError)
        }
        // verifica se houve erros
        Map res
        if (errors.size()) {
            res     = [status:0, msg:errors.join('<br>'), type:'error']
        }else{
            res     = [outputDir: outputDir, nomeArquivoPlanilha: nomeArquivoPlanilha]
        }
        return res
    }
    /**
    * Insere aquivos multimidia oriundo de planilha/zip importados
    * @params String outputDir, String fileName, Long sqCicloAvaliacao, String requestUrl, Sicae user
    * @return Map
    */
    private Map  _salvaFotosAudios(String outputDir, String fileName, Long sqCicloAvaliacao, String requestUrl, Sicae user ) {
        Util.printLog('INFO::SALVE::MainService._salvaFotosAudios()' );

        Map res     = [status:0, msg: 'Foram encontrados alguns erros. Verifique seu e-mail para mais informações.', type:'error']

        DadosApoio tipo
        String noAutor
        String deEmailAutor
        String deLegenda
        String noArquivo
        Boolean inPrincipal
        Boolean inDestaque
        Date dtElaboracao
        String txMultimidia
        Taxon taxon
        Ficha ficha
        Long sqTaxon
        List errors = []
        String msgError
        Long limiteLinhasProcessar = 300
        Long linhaInicioDadosProcessar = 4
        Long linhaFinalDadosProcessar = (linhaInicioDadosProcessar + limiteLinhasProcessar)
        String zipDir = ''

        if(!sqCicloAvaliacao){
            msgError = 'Ops! Não foi possível obter informação do Ciclo de Avaliação.'
            Util.printLog('ERRO::SALVE::MainService._salvaFotosAudios()', msgError );
            throw new Exception(msgError)
        }
        // Busca pelo ciclo
        CicloAvaliacao ciclo
        try{
            Integer sq_ciclo = sqCicloAvaliacao.toInteger()
            ciclo = CicloAvaliacao.get(sq_ciclo)
        }catch(Exception e){
            msgError = 'Ops! Ocorreu um erro ao consultar o Ciclo de Avaliação.'
            Util.printLog('ERRO::SALVE::MainService._salvaFotosAudios()', msgError,  e.getMessage() );
            throw new Exception(msgError)
        }
        // leitura da planilha
        File file
        PlanilhaParser pp
        try{
            file = new File(outputDir + fileName)
            zipDir = file.getParent().toString()+'/'
            pp = new PlanilhaParser(file)
        }catch(Exception e){
            msgError = 'Ops! Não foi possível abrir a planilha para leitura.'
            Util.printLog('ERRO::SALVE::MainService._salvaFotosAudios()', msgError,  e.getMessage() );
            throw new Exception(msgError)
        }
        if ( pp.getError()) {
            msgError = "Ops! Ocorreu um erro ao ler planilha! Erro: " + pp.getError()
            Util.printLog('ERRO::SALVE::MainService._salvaFotosAudios()', msgError );
            throw new Exception(msgError)
        }
        pp.setHeaderRowNum(3)
        pp.setSheet(0)

        // verificar se a planilha está vazia
        if( pp.getRows() < linhaInicioDadosProcessar ){
            msgError = "Ops! Panilha está vazia!"
            Util.printLog('ERRO::SALVE::MainService._salvaFotosAudios()', msgError );
            throw new Exception(msgError)
        }

        // verificar se a planilha possui mais registros que o limite
        if( pp.getRows() > ((linhaInicioDadosProcessar + limiteLinhasProcessar) ) ){
            msgError = "Ops! A planilha não deve ultrapassar mais de ${limiteLinhasProcessar} registros!"
            Util.printLog('ERRO::SALVE::MainService._salvaFotosAudios()', msgError );
            throw new Exception(msgError)
        }

        Boolean endFileContent = false
        Boolean isValidLine
        List recordSuccess = []
        List recordError = []
        String errorLine = ''
        String value = ''
        Integer lineEmpty
        for(int line in 4..linhaFinalDadosProcessar) {
            isValidLine = true
            pp.getRowData(line).eachWithIndex { v,i ->
                if(i == 0 && !v){
                    endFileContent = true // Identifica linha em branco. Fim dos daods
                    isValidLine = false // Identifica linha válida para salvar
                    lineEmpty = line
                }
                // Mensagem complementar da linha onde ocorreu o erro
                errorLine = ' Erro na linha: ' + line + ' da planilha.'
                value = v.toString() // valor da celula
                switch (i) {
                    case 0: // Nome Científico
                        if(endFileContent){
                            break
                        }
                        try{
                            List rows = _buscaTaxon(value, sqCicloAvaliacao.toInteger())
                            sqTaxon = rows[0].sq_taxon
                            taxon = Taxon.get(sqTaxon)
                        }catch(Exception e){
                            if(!endFileContent){
                                msgError = 'Não foi possível encontrar o taxon: [' + value + '].' + errorLine
                                Util.printLog('ERRO::SALVE::MainService._salvaFotosAudios()', msgError,  e.getMessage() );
                                errors.push(msgError)
                            }
                            isValidLine = false
                        }
                        // Busca pela ficha
                        try{
                            ficha = Ficha.findByCicloAvaliacaoAndTaxon(ciclo, taxon)
                        }catch(Exception e){
                            if(!endFileContent){
                                msgError = "Ocorreu um erro ao consultar Ciclo: [${ciclo}] e Taxon: [${taxon}]." + errorLine
                                Util.printLog('ERRO::SALVE::MainService._salvaFotosAudios()', msgError,  e.getMessage() );
                                errors.push(msgError)
                            }
                            isValidLine = false
                        }
                        if (!ficha) {
                            if(!endFileContent){
                                msgError = "Ficha do taxon: [${value}] não encontrada!" + errorLine
                                errors.push(msgError)
                            }
                            isValidLine = false
                        }
                        break
                    case 1: // Tipo de Mídia * (imagem ou áudio)
                        if(value.toLowerCase().indexOf('image') >= 0 || value.toLowerCase().indexOf('foto') >= 0 ){
                            tipo = dadosApoioService.getByCodigo('TB_TIPO_MULTIMIDIA','IMAGEM')
                        }else if(value.toLowerCase().indexOf('audio') >= 0 || value.toLowerCase().indexOf('áudio') >= 0 || value.toLowerCase().indexOf('som') >= 0 ){
                            tipo = dadosApoioService.getByCodigo('TB_TIPO_MULTIMIDIA','SOM')
                        }else{
                            if(!endFileContent){
                                msgError = "Tipo de mídia não informado corretamente! Informe se é Imagem ou Áudio." + errorLine
                                errors.push(msgError)
                            }
                            isValidLine = false
                        }
                        break
                    case 2: // Nome do autor
                        if(value.length() < 5) {
                            if(!endFileContent){
                                msgError = "Informação muito curta no nome do autor!" + errorLine
                                errors.push(msgError)
                            }
                            isValidLine = false
                        }
                        noAutor = value
                        break
                    case 3: // Email do autor
                        if(value.length() > 0) {
                            if (!Validador.isEmail(value)) {
                                if(!endFileContent){
                                    msgError = "E-mail ["+value+"], não é um e-mail válido!" + errorLine
                                    errors.push(msgError)
                                }
                                isValidLine = false
                            }
                        }
                        deEmailAutor = value
                        break
                    case 4: // Legenda
                        if(value.length() <= 0) {
                            if(!endFileContent){
                                msgError = "A coluna legenda é uma informação obrigatória!" + errorLine
                                errors.push(msgError)
                            }
                            isValidLine = false
                        }
                        deLegenda = value
                        break
                    case 5: // Nome do arquivo
                        if(!value.length()) {
                            if(!endFileContent){
                                msgError = "Nome do arquivo não foi informado!" + errorLine
                                errors.push(msgError)
                            }
                            isValidLine = false
                        }
                        noArquivo = value
                        String pathFileMultimidia = (zipDir ? zipDir : outputDir) + noArquivo
                        File fileMultimidia
                        try{
                            fileMultimidia = new File(pathFileMultimidia)
                        }catch(Exception e){
                            if(!endFileContent){
                                msgError = "Ocorreu um erro tentar abrir o arquivo de multimidia, Arquivo: [${pathFileMultimidia}]." + errorLine
                                Util.printLog('ERRO::SALVE::MainService._salvaFotosAudios()', msgError,  e.getMessage() );
                                errors.push(msgError)
                            }
                            isValidLine = false
                        }
                        if (!fileMultimidia.exists()) {
                            if(!endFileContent){
                                msgError = "Arquivo da multimidia não existe no ZIP. Nome do arquivo: ${value}." + errorLine
                                errors.push(msgError)
                            }
                            isValidLine = false
                        }
                        break
                    case 6: // Imagem principal
                        inPrincipal = false
                        if(value.toUpperCase().indexOf('S') >= 0){
                            inPrincipal = true
                        }
                        break
                    case 7: // Imagem Destaque
                        inDestaque = false
                        if(value.toUpperCase().indexOf('S') >= 0){
                            inDestaque = true
                        }
                        break
                    case 8: // Data do arquivo
                        if(!value.length()) {
                            dtElaboracao = null
                        }else{
                            try {
                                dtElaboracao = new Date().parse('dd/MM/yyyy', value )
                            }
                            catch (Exception e1) {
                                try {
                                    dtElaboracao = new Date().parse('dd-MM-yyyy', value )
                                }
                                catch (Exception e2) {
                                    try{
                                        dtElaboracao = new Date().parse('dd-MMM-yyyy', value )
                                    }catch (Exception e3) {
                                        try{
                                            dtElaboracao = new Date().parse('yyyy-MMM-dd', value )
                                        }catch(Exception e4){
                                            if(!endFileContent){
                                                msgError = "Informe uma data do arquivo válida com formato dd/mm/aaaa. Data informada: [${value}]." + errorLine
                                                Util.printLog('ERRO::SALVE::MainService._salvaFotosAudios()', msgError,  e4.getMessage() );
                                                errors.push(msgError)
                                            }
                                            isValidLine = false
                                        }
                                    }
                                }
                            }
                        }
                        break
                    case 9: // Legenda
                        if(value.length()) {
                            txMultimidia = value
                        }else{
                            txMultimidia = null
                        }

                        break
                }
            } // END pp.getRowData(line).eachWithIndex

            // se a linha da planilha não for válida para importar
            if(!isValidLine){
                if(line != lineEmpty){
                    recordError.push(line)
                }
                if(!endFileContent){
                    continue
                }
            }else{ // linha da planilha ok para importar
                recordSuccess.push(line)
            }
            // verifica se já chegou no fim dos dados da planilha
            if(endFileContent){
                String msgImport = ''
                if(recordSuccess.size()){
                    msgImport += "${recordSuccess.size()} arquivo(s) importado(s) com sucesso.\n"
                }
                if(recordError.size()){
                    msgImport += "<br>${recordError.size()} ERRO(S) encontrado(s).<br>Verifique seu email: [${user.email}] para obeter o log dos erros."
                }
                // envia o log caso ocorra erro
                _enviaEmailLogProcessaUploadFotosAudios( recordSuccess,  recordError,  errors, outputDir, requestUrl, user)
                pp.close()
                if(recordSuccess.size()){
                    res.type = 'success'
                    res.msg = msgImport
                }
                return res
            }
            // salva os arquvos no diretório
            String pathFileMultimidia = (zipDir ?: outputDir) + noArquivo
            FileInputStream fis
            File fileImageAudio
            try{
                fileImageAudio = new File(pathFileMultimidia)
                fis = new FileInputStream(fileImageAudio)
            }catch(Exception e){
                msgError = "Ocorreu um erro ao tentar abrir arquivo para copiar. Arquivo [${pathFileMultimidia}]." + errorLine
                Util.printLog('ERRO::SALVE::MainService._salvaFotosAudios()', msgError,  e.getMessage() );
            }
            String fileNameImageAudio   = pathFileMultimidia.replaceAll(' |, |\\(.+\\)', '_').replaceAll('_+', '-').replaceAll('-\\.', '.')
            String fileExtension        = fileNameImageAudio.substring(fileNameImageAudio.lastIndexOf('.'))
            String hashFileName         = fis.text.toString().encodeAsMD5() + fileExtension
            File savePath               = new File(grailsApplication.config.multimidia.dir);
            try{
                if (!savePath.exists()) {
                    if (!savePath.mkdirs()) {
                        msgError = 'Não foi possivel criar o diretório ' + savePath.asString()
                        Util.printLog('ERRO::SALVE::MainService._salvaFotosAudios()', msgError,  e.getMessage() );
                    }
                }
                // cria o thumb e carousel da imagem
                if( noArquivo =~ /\.(jpe?g|png|bmp)|$/) {
                    String fileNameImage = savePath.absolutePath + '/' + hashFileName
                    if (!new File(fileNameImage).exists()) {
                        fileImageAudio.renameTo(new File(fileNameImage))
                        ImageService.createThumb(fileNameImage)
                        ImageService.createCarouselImage(fileNameImage)
                    }
                }
            }catch(Exception e){
                msgError = "Ocorreu um erro ao tentar salvar arquivo multimidia." + errorLine
                Util.printLog('ERRO::SALVE::MainService._salvaFotosAudios()', msgError,  e.getMessage() );
            }
            DadosApoio situacao = dadosApoioService.getByCodigo('TB_SITUACAO_MULTIMIDIA','APROVADA')
            FichaMultimidia fm  = FichaMultimidia.findByFichaAndNoArquivoDisco( ficha, hashFileName )
            if(!fm){
                fm = new FichaMultimidia()
            }
            try{
                fm.ficha =  ficha
                fm.noArquivoDisco   = hashFileName
                fm.noArquivo        = noArquivo
                fm.tipo             = tipo
                fm.situacao         = situacao
                fm.dtElaboracao     = dtElaboracao
                fm.noAutor          = noAutor
                fm.deEmailAutor     = deEmailAutor
                fm.deLegenda        = deLegenda?:null
                fm.txMultimidia     = txMultimidia?:null
                fm.txNaoAceito      = null
                fm.aprovadoPor      = Pessoa.get( user?.sqPessoa.toLong() )
                fm.dtAprovacao      = new Date()
                fm.reprovadoPor     = null
                fm.dtReprovacao     = null
                // somente imagens jpeg, png ou bmp podem ser destaque ou principal
                if( noArquivo =~ /\.(jpe?g|png|bmp)|$/) {
                    fm.inPrincipal  = (inPrincipal == true)
                    fm.inDestaque   = (inDestaque == true)
                } else {
                    fm.inPrincipal  = false
                    fm.inDestaque   = false
                }
                fm.save( flush:true )

                // ajusta o inPrincipal e inDestaque das imagens pois só pode existir um TRUE
                if( noArquivo =~ /\.(jpe?g|png|bmp)|$/ ) {
                    if(fm.inPrincipal){
                        FichaMultimidia.executeUpdate("UPDATE FichaMultimidia fm SET fm.inPrincipal=:inPrincipal "
                            + " WHERE fm.id <> :id "
                            + " AND fm.ficha.id = :sqFicha "
                            , [inPrincipal: false, id: fm.id, sqFicha: fm.ficha.id]
                        )
                    }
                    if(fm.inDestaque){
                        FichaMultimidia.executeUpdate("UPDATE FichaMultimidia fm SET fm.inDestaque=:inDestaque "
                            + " WHERE fm.id <> :id "
                            + " AND fm.ficha.id = :sqFicha "
                            , [inDestaque: false, id: fm.id, sqFicha: fm.ficha.id]
                        )
                    }
                }

                // atualizar log da ficha, quem alterou
                fm.ficha.dtAlteracao = new Date()
                fm.ficha.save()
            }catch(Exception e){
                msgError = 'Ocorreu um erro ao tentar salvar multimidia da ficha'
                Util.printLog('ERRO::SALVE::MainService._salvaFotosAudios()', msgError,  e.getMessage() );
                throw new Exception(msgError)
            }
        }
        pp.close()
        return res
    }
    /**
    * Envia email com o log dos erros ocorridos na importação
    * @params List recordSuccess, List recordError, List errors, String outputDir, String dirMapsUserUpload, String requestUrl, Sicae user
    * @return void
    */
    protected void _enviaEmailLogProcessaUploadFotosAudios(List recordSuccess, List recordError, List errors, String outputDir, String requestUrl, Sicae user){
        Util.printLog('INFO::SALVE::MainService._enviaEmailLogProcessaUploadFotosAudios()' );

        String dirFotosAudiosUpload = 'fotos_audios'
        // Apenas envia se exitir pelo menos 1 erro na importação
        if(!errors.size()){
            return
        }
        String strLog =
"""
Total de registros importados: ${recordSuccess.size()}.
Nº das linhas importadas: ${recordSuccess.join(', ').toString()}.

Total de linhas com problemas: ${recordError.size()}.
Nº das linhas com problemas: ${recordError.join(', ').toString()}.

Detalhes dos problemas:
"""
        // salva o arquivo de log
        String strDateTime = new Date().format('dd-MM-yyyy-HH-mm-ss').toString()
        String fileName = 'log-' + strDateTime + '.txt'
        List logs = [strLog]
        logs += errors
        try{
            new File(outputDir,fileName).withWriter('utf-8') {
                writer -> writer.write(logs.join('\n'))
            }
        }catch(Exception e){
            msgError 'Erro ao gravar arquivo de log: ' + outputDir +  fileName
            Util.printLog('ERRO::SALVE::MainService._enviaEmailLogProcessaUploadFotosAudios()', msgError,  e.getMessage() );
        }
        // envia o email com o link para o log
        String mensagem="""<p style="font-size:18px;font-family:Times New Roman, Arial">Log de importação de fotos/áudios em ${new Date().format('dd/MM/yyyy HH:mm:ss')}<br>
                <br>Para visualizar detalhes dos erros <a href="${requestUrl}download?fileName=/data/salve-estadual/temp/${dirFotosAudiosUpload}/${fileName}" target="_blank">clique aqui</a>.
                <br>Obs.: Este arquivo de log ficará disponível apenas por 05 dias.
                <br><br>Atenciosamente,<br>
                Equipe SALVE.
                </p>
                """
        String assunto = 'Log de importação de fotos/áudios - ' +  new Date().format('dd/MM/yyyy HH:mm:ss').toString()
        try{
            emailService.sendTo(user.email, assunto, mensagem, grailsApplication.config.app.email )
        }catch(Exception e){
            msgError 'Ocorreu um erro ao enviar email: emailService.sendTo()'
            Util.printLog('ERRO::SALVE::MainService._enviaEmailLogProcessaUploadFotosAudios()', msgError,  e.getMessage() );
        }
    }
}
