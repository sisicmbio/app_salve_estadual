package br.gov.icmbio

import grails.converters.JSON
import grails.transaction.Transactional
import org.codehaus.groovy.grails.web.json.JSONArray
import org.codehaus.groovy.grails.web.json.JSONObject

@Transactional
class TraducaoFichaService {

    def sqlService
    def fichaService
    def requestService
    def dadosApoioService
    def grailsApplication
    /**
     * método para retornar os dados para montagem do gride de fichas para tradução
     * @param params
     * @return
     */
    Map getDataGridTraducaoFicha(Map params = [:], Sicae user = [:]) {
        String sqlOrderBy = ''
        String cmdSql
        boolean calcularPercentuais = false
        Map sqlParams = [:]
        List whereFinal = []
        List situacoesValidas = ['FINALIZADA', 'PUBLICADA']

        if( params.sq_ficha ){
            whereFinal.push('ficha.sq_ficha = :sqFicha')
            sqlParams.sqFicha = params.sq_ficha.toLong()
        } else {

            calcularPercentuais = (params.inAndamentoTraducaoFiltro || params.inAndamentoRevisaoFiltro || params?.format == 'xls')

            if (!params.sortColumns) {
                params.sortColunms = 'ficha.nm_cientifico'
            }
            List sortColumns = params.sortColumns.split(':')
            sqlOrderBy = '\nORDER BY ' + sortColumns[0] + (sortColumns[1] ? ' ' + sortColumns[1] : '')

            // FILTRO PELO NOME CIENTIFICO
            /*
            if (params.nmCientificoFiltro) {
                whereFinal.push("ficha.nm_cientifico ilike :nmCientifico")
                sqlParams.nmCientifico = '%' + params.nmCientificoFiltro + '%'
            }*/
            // FILTRAR PELO NOME CIENTIFICO
            if (params.nmCientificoFiltro) {
                params.nmCientificoFiltro = params.nmCientificoFiltro.toString().replaceAll(/%/, '').replaceAll(/;/, ',')
                // verificar se foi passado lista de nomes ou apenas 1 nome
                List nomes = params.nmCientificoFiltro.split(',').collect { it.toString().trim() }
                if (nomes.size() == 1) {
                    // quando for informado somente 1 nome cientifico, fazer a busca pelos nomes antigos (sinonimias) também
                    whereFinal.push("""( ficha.nm_cientifico ilike :nmCientifico OR exists (
                                        select null from salve.ficha_sinonimia
                                        where no_sinonimia ILIKE :noSinonimia
                                        and ficha_sinonimia.sq_ficha = ficha.sq_ficha
                                        limit 1 ) )""")

                    params.nmCientificoFiltro = Util.clearCientificName(params.nmCientificoFiltro)
                    sqlParams.noSinonimia = nomes[0]
                    sqlParams.nmCientifico = '%' + nomes[0] + '%'
                } else {
                    List whereOr = []
                    List nomesEspecies = []
                    List nomesSubespecies = []
                    nomes.eachWithIndex { nome, index ->
                        // colocar o nome cientifico iniciado com letra maiuscula e o restante em minusculas
                        nome = nome.substring(0, 1).toUpperCase() + nome.substring(1)
                        // criar dinamicamente os parametros para cada nome informado
                        //String paramName = 'noTaxon' + index.toString()
                        //qryParams[paramName] = nome
                        // gerar cláusula OR do comando sql que será gerado
                        if (nome.split(' ').size() == 2) {
                            nomesEspecies.push( nome )
                        } else {
                            nomesSubespecies.push( nome )
                        }
            }
                    if( nomesEspecies ){
                        String nomesFormatados = "'"+nomesEspecies.join("','")+"'"
                        whereOr.push("taxon.json_trilha->'especie'->>'no_taxon'::text = any(array[${nomesFormatados}])")
                    } else if (nomesSubespecies) {
                        String nomesFormatados = "'"+nomesSubespecies.join("','")+"'"
                        whereOr.push("taxon.json_trilha->'subespecie'->>'no_taxon'::text = any(array[${nomesFormatados}])")
                    }
                    if (whereOr) {
                        whereFinal.push('(' + whereOr.join(' or ') + ')')
                    }
                    params.nmCientificoFiltro = '%'
                }
            }


            // FILTRO PELA UNIDADE ORGANIZACIONAL
            // se não for informada a unidade organizacional filtrar pela unidade do usuario logado quando ele nao for adm
            if (!params.sqUnidadeFiltro) {
                if (!user.isADM()) {
                    // ler as fichas da unidade de lotacao do usuario se ele tiver lotacao
                    if (user.sqUnidadeOrg) {
                        params.sqUnidadeFiltro = user.sqUnidadeOrg
                    } else {
                        // FILTRAR PELAS FICHAS ATRIBUIDAS AO PAPEL/FUNCAO INDEPENDENTE DA UNIDADE ORGANIZACIONAL
                        DadosApoio papel = dadosApoioService.getByCodigo('TB_PAPEL_FICHA', 'TRADUTOR')
                        whereFinal.push("exists ( select null from salve.ficha_pessoa fp where fp.sq_pessoa = :sqPessoa and fp.sq_papel = :sqPapel and in_ativo = 'S' and fp.sq_ficha = ficha.sq_ficha limit 1 )")
                        sqlParams.sqPessoa = user.sqPessoa.toLong()
                        sqlParams.sqPapel = papel?.id?.toLong()
                    }
                }
            }
            if (params.sgUnidadeFiltro) {
                whereFinal.push("unidade.sg_unidade_org ilike :sgUnidade")
                sqlParams.sgUnidade = '%' + params.sgUnidadeFiltro + '%'
            }
            if (params.sqUnidadeFiltro) {
                whereFinal.push("unidade.sq_pessoa = :sqUnidade")
                sqlParams.sqUnidade = params.sqUnidadeFiltro.toLong()
            }

            // FILTRAR PELA SITUACAO DA FICHA
            if (params.sqSituacaoFiltro) {
                whereFinal.push("ficha.sq_situacao_ficha = :sqSituacaoFicha")
                sqlParams.sqSituacaoFicha = params.sqSituacaoFiltro.toLong()
            } else {
                // FILTRAR AS SITUAÇÕES VÁLIDAS
                whereFinal.push("situacao.cd_sistema in (${situacoesValidas.collect { "'" + it + "'" }.join(',')})")
            }

            // FILTRAR PELO NOME COMUM
            if (params.nmComumFiltro) {
                whereFinal.push("EXISTS ( SELECT NULL from salve.ficha_nome_comum fnc where fnc.sq_ficha = ficha.sq_ficha and public.fn_remove_acentuacao(fnc.no_comum) ilike :nmComum limit 1 )")
                sqlParams.nmComum = '%' + Util.removeAccents(params.nmComumFiltro) + '%'

            }
            // FILTRO PELO NIVEL TAXONOMICO
            if (params.nivelFiltro && params.sqTaxonFiltro) {
                if (params.sqTaxonFiltro.toString().indexOf(',') > 0 ) {
                    whereFinal.push("(taxon.json_trilha->'${params.nivelFiltro.toLowerCase()}'->>'sq_taxon')::integer in ( ${params.sqTaxonFiltro})")
                } else {
                    whereFinal.push("(taxon.json_trilha->'${params.nivelFiltro.toLowerCase()}'->>'sq_taxon')::integer = :sqTaxon")
                    sqlParams.sqTaxon = params.sqTaxonFiltro.toLong()
                }
            }

            // FILTRAR PELO GRUPO AVALIADO
            if ( params.sqGrupoFiltro) {
                if (params.sqGrupoFiltro.toString().indexOf(',') > 0 ) {
                    whereFinal.push("ficha.sq_grupo in ( ${params.sqGrupoFiltro} )")
                } else {
                    whereFinal.push("ficha.sq_grupo = :sqGrupoFiltro")
                    sqlParams.sqGrupoFiltro = params.sqGrupoFiltro.toLong()
                }
                if (params.sqSubgrupoFiltro) {
                    if (params.sqSubgrupoFiltro.toString().indexOf(',') > 0 ) {
                        whereFinal.push("ficha.sq_subgrupo in ( ${params.sqSubgrupoFiltro} )")
                    } else {
                        whereFinal.push("ficha.sq_subgrupo = :sqSubgrupoFiltro")
                        sqlParams.sqSubgrupoFiltro = params.sqSubgrupoFiltro.toLong()
                    }
                }
            }

            // FILTRAR PELO TRADUTOR
            if( params.sqTradutorFiltro ){
                if (params.sqTradutorFiltro.toString().indexOf(',') > 0 ) {
                    whereFinal.push("exists ( select null from salve.traducao_revisao x where x.sq_registro = ficha.sq_ficha and dt_revisao is not null and x.sq_pessoa in (${params.sqTradutorFiltro}) )")
                } else {
                    whereFinal.push("exists ( select null from salve.traducao_revisao x where x.sq_registro = ficha.sq_ficha and dt_revisao is not null and x.sq_pessoa = ${params.sqTradutorFiltro} )")
                }
            }
        }

        cmdSql = """select ficha.sq_ficha,
                   ficha.nm_cientifico,
                   nivel.co_nivel_taxonomico,
                   situacao.ds_dados_apoio as ds_situacao_ficha,
                   situacao.cd_sistema as cd_situacao_ficha_sistema,
                   unidade.sg_unidade_org,
                   0 as percentual_traduzido,
                   0 as percentual_revisado,
                   revisores.no_revisor
                   -- coalesce(perdentuais.percentual_traduzido,0) as percentual_traduzido,
                   -- coalesce(perdentuais.percentual_revisado,0) as percentual_revisado
            from
                salve.ficha
                    inner join salve.dados_apoio situacao on situacao.sq_dados_apoio = ficha.sq_situacao_ficha
                    inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                    inner join taxonomia.nivel_taxonomico as nivel on nivel.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
                    inner join salve.vw_unidade_org as unidade on unidade.sq_pessoa = ficha.sq_unidade_org
                    -- ler o(s) revisor(es)
                    left join lateral (
                        with cte1 as (
                            select distinct tr.sq_pessoa
                            from salve.traducao_revisao tr
                                     inner join salve.traducao_tabela tt on tt.sq_traducao_tabela = tr.sq_traducao_tabela
                            where tr.sq_registro = ficha.sq_ficha
                              and tt.no_tabela = 'ficha'
                        )
                        select array_to_string( array_agg(pf.no_pessoa order by pf.no_pessoa),';') as no_revisor
                        from salve.vw_pessoa_fisica pf
                        inner join cte1 on cte1.sq_pessoa = pf.sq_pessoa
                        ) as revisores on true

                    ${whereFinal ? 'where ' + whereFinal.join('\nand ') : ''}
                    ${sqlOrderBy}
                    """
        /*
               -- calcular o percentual de revisao e traducao
                     left join lateral (
                        with passo1 as (
                            select tt.no_tabela, count(tt.*) as qtd_total
                            from salve.traducao_tabela tt
                            group by tt.no_tabela
                        ),
                             passo2 as (
                                 select tt.no_tabela
                                      , count(case when tr.tx_traduzido is not null then 1 else 0 end) as qtd_traduzido
                                      , sum(case when tr.dt_revisao is null then 0 else 1 end) as qtd_revisado
                                 from salve.traducao_revisao tr
                                          inner join salve.traducao_tabela tt on tt.sq_traducao_tabela = tr.sq_traducao_tabela
                                 where tr.sq_registro = ficha.sq_ficha
                                 group by tt.no_tabela
                             ),
                             passo3 as (
                                 select passo2.*,
                                        ((passo2.qtd_traduzido::decimal / passo1.qtd_total::decimal) * 100)::integer as percentual_traduzido
                                         ,
                                        ((passo2.qtd_revisado::decimal / passo1.qtd_total::decimal) * 100)::integer  as percentual_revisado
                                 from passo2
                                          inner join passo1 on passo1.no_tabela = passo2.no_tabela
                             )
                        select percentual_traduzido,
                        percentual_revisado
                        from passo3
                        ) as perdentuais on true

         */

        /** /
        //zzz
        println ' '
        println ' '
        println ' '
        println cmdSql
        /**/
        Map rows = sqlService.paginate(cmdSql, sqlParams, params, 100, 10, 0)

        // adicionar os percentuais no resultado para exportar para a planilha
        if( rows.size() > 1 && calcularPercentuais) {
        //if( rows.size() > 1 && params?.format == 'xls') {
            List percentuais = getPercentuaisPage( [ids:rows.rows.sq_ficha] ).rows
            rows.rows.each { row ->
                Map rowPercentual = percentuais.find { row.sq_ficha.toLong() == it.sqFicha.toLong() }
                if( rowPercentual ){
                    row.nu_percentual_traduzido = rowPercentual.nuPercentualTraduzido ?: '0'
                    row.nu_percentual_revisado = rowPercentual.nuPercentualRevisado   ?: '0'
                } else {
                    row.nu_percentual_traduzido = '0'
                    row.nu_percentual_revisado  = '0'
                }
            }

            // FILTRAR PELO PERCENTUAL TRADUZIDO e REVISADO
            if( params.inAndamentoTraducaoFiltro || params.inAndamentoRevisaoFiltro ) {
                List listAndamentos = []
                if( params.inAndamentoTraducaoFiltro){
                    listAndamentos.push( params.inAndamentoTraducaoFiltro )
                }
                if( params.inAndamentoRevisaoFiltro ){
                    listAndamentos.push( params.inAndamentoRevisaoFiltro )
                }
                List rowsValidas = rows.rows.findAll { row ->
                    boolean rowValida = true
                    listAndamentos.each {
                        if (rowValida && it == 'TRADUCAO_FINALIZADA' && row.nu_percentual_traduzido.toInteger() < 100) {
                            rowValida = false
                        } else if (rowValida && it == 'TRADUCAO_INICIADA' && (row.nu_percentual_traduzido.toInteger() < 1 || row.nu_percentual_traduzido.toInteger() > 99)) {
                            rowValida = false
                        } else if (rowValida && it == 'TRADUCAO_NAO_INICIADA' && row.nu_percentual_traduzido.toInteger() > 0) {
                            rowValida = false
                        } else if (rowValida && it == 'REVISAO_FINALIZADA' && row.nu_percentual_revisado.toInteger() < 100) {
                            rowValida = false
                        } else if (rowValida && it == 'REVISAO_INICIADA' && (row.nu_percentual_revisado.toInteger() < 1 || row.nu_percentual_revisado.toInteger() > 99)) {
                            rowValida = false
                        } else if (rowValida && it == 'REVISAO_NAO_INICIADA' && row.nu_percentual_revisado.toInteger() > 0) {
                            rowValida = false
                        }
                    }
                    return rowValida
                }
                rows.rows = rowsValidas
            }
        }
        return rows
    }

    /**
     * retornar os dados da ficha para a criação da tela de revisão (html)
     * @param sqFicha
     * @return
     */
    Map getFichaRevisao(Long sqFicha = 0) {

        //sqFicha=14019; /// debug Puma concolor

        Map fichaRevisao = [sqFicha: sqFicha
                            , error: '']
        try {
            // sql para recuperar os dados da ficha
            String cmdSql = """select ficha.sq_ficha
       ,ficha.ds_citacao
      ,ficha.nm_cientifico
      ,taxon.json_trilha->'reino'->>'no_taxon'::text as no_reino
      ,taxon.json_trilha->'filo'->>'no_taxon'::text as no_filo
      ,taxon.json_trilha->'classe'->>'no_taxon'::text as no_classe
      ,taxon.json_trilha->'ordem'->>'no_taxon'::text as no_ordem
      ,taxon.json_trilha->'familia'->>'no_taxon'::text as no_familia
      ,taxon.json_trilha->'genero'->>'no_taxon'::text as no_genero
      ,taxon.json_trilha->'especie'->>'no_taxon'::text as no_especie
      ,taxon.json_trilha->'subespecie'->>'no_taxon'::text as no_subespecie
      ,nome_comum.no_comum
      ,nome_antigo.no_antigo
      ,ficha.sq_categoria_final as sq_categoria_validada
      ,categoria_validada.sq_dados_apoio as sq_categoria_validada
      ,categoria_validada.ds_dados_apoio as ds_categoria_validada
      ,categoria_validada.cd_dados_apoio as cd_categoria_validada
      ,to_char( ficha.dt_aceite_validacao,'dd/MM/yyyy') as dt_validacao
      ,salve.snd( ficha.st_endemica_brasil) as ds_endemica_brasil
      ,salve.snd( ficha.st_migratoria) as ds_migratoria
      ,salve.snd( ficha.st_restrito_habitat_primario) as ds_restrito_habita_primario
      ,salve.snd( ficha.st_especialista_micro_habitat) as ds_especialista_micro_habitat
      ,ficha.vl_tempo_gestacao
      ,ficha.vl_tamanho_prole
      ,ficha.sq_unid_tempo_gestacao
      ,ficha.sq_medida_tempo_geracional
      ,tempo_gestacao.ds_dados_apoio as ds_tempo_gestacao
      ,tempo_geracional.ds_dados_apoio as ds_tempo_geracional
      ,ficha.vl_maturidade_sexual_macho
      ,ficha.vl_maturidade_sexual_femea
      ,ficha.sq_unid_maturidade_sexual_macho
      ,unid_mat_sexual_macho.ds_dados_apoio as ds_maturidade_sexual_macho
      ,ficha.sq_unid_maturidade_sexual_femea
      ,unid_mat_sexual_femea.ds_dados_apoio as ds_maturidade_sexual_femea
      ,ficha.vl_peso_macho
      ,ficha.vl_peso_femea
      ,ficha.sq_unidade_peso_macho
      ,ficha.sq_unidade_peso_femea
      ,unid_peso_macho.ds_dados_apoio as ds_unidade_peso_macho
      ,unid_peso_femea.ds_dados_apoio as ds_unidade_peso_femea

      ,ficha.vl_comprimento_macho_max
      ,ficha.vl_comprimento_femea_max
      ,unid_comp_max_macho.ds_dados_apoio as ds_unid_comp_max_macho
      ,unid_comp_max_femea.ds_dados_apoio as ds_unid_comp_max_femea
      ,ficha.sq_medida_comprimento_macho_max
      ,ficha.sq_medida_comprimento_femea_max

      ,ficha.vl_comprimento_macho
      ,ficha.vl_comprimento_femea
      ,unid_comp_macho.ds_dados_apoio as ds_unid_comp_macho
      ,unid_comp_femea.ds_dados_apoio as ds_unid_comp_femea
      ,ficha.sq_medida_comprimento_macho
      ,ficha.sq_medida_comprimento_femea

      ,ficha.vl_senilidade_reprodutiva_macho
      ,ficha.vl_senilidade_reprodutiva_femea
      ,ficha.sq_unidade_senilid_rep_macho
      ,ficha.sq_unidade_senilid_rep_femea
      ,unid_senilidade_macho.ds_dados_apoio as ds_unid_senilidade_macho
      ,unid_senilidade_femea.ds_dados_apoio as ds_unid_senilidade_femea

      ,ficha.vl_longevidade_macho
      ,ficha.vl_longevidade_femea
      ,ficha.sq_unidade_longevidade_macho
      ,ficha.sq_unidade_longevidade_femea
      ,unid_longevidade_macho.ds_dados_apoio as ds_unid_logevidade_macho
      ,unid_longevidade_femea.ds_dados_apoio as ds_unid_logevidade_femea

      ,ficha.sq_medida_tempo_geracional
      ,ficha.vl_tempo_geracional
      ,unid_tempo_geracional.ds_dados_apoio as ds_medida_tempo_geracional

      ,ficha.sq_tendencia_populacional
      ,tendencia_populacional.ds_dados_apoio as ds_tendencia_populacional

      ,salve.snd( ficha.st_presenca_lista_vigente ) as ds_presenca_lista_vigente

                from salve.ficha
                inner join salve.ciclo_avaliacao ciclo on ciclo.sq_ciclo_avaliacao = ficha.sq_ciclo_avaliacao
                inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                inner join salve.dados_apoio as categoria_validada on categoria_validada.sq_dados_apoio = ficha.sq_categoria_final
                left outer join salve.dados_apoio as tempo_gestacao on tempo_gestacao.sq_dados_apoio = ficha.sq_unid_tempo_gestacao
                left outer join salve.dados_apoio as tempo_geracional on tempo_geracional.sq_dados_apoio = ficha.sq_medida_tempo_geracional
                left outer join salve.dados_apoio as unid_mat_sexual_macho on unid_mat_sexual_macho.sq_dados_apoio = ficha.sq_unid_maturidade_sexual_macho
                left outer join salve.dados_apoio as unid_mat_sexual_femea on unid_mat_sexual_femea.sq_dados_apoio = ficha.sq_unid_maturidade_sexual_femea
                left outer join salve.dados_apoio as unid_peso_macho on unid_peso_macho.sq_dados_apoio = ficha.sq_unidade_peso_macho
                left outer join salve.dados_apoio as unid_peso_femea on unid_peso_femea.sq_dados_apoio = ficha.sq_unidade_peso_femea
                left outer join salve.dados_apoio as unid_comp_max_macho on unid_comp_max_macho.sq_dados_apoio = ficha.sq_medida_comprimento_macho_max
                left outer join salve.dados_apoio as unid_comp_max_femea on unid_comp_max_femea.sq_dados_apoio = ficha.sq_medida_comprimento_femea_max
                left outer join salve.dados_apoio as unid_comp_macho on unid_comp_macho.sq_dados_apoio = ficha.sq_medida_comprimento_macho
                left outer join salve.dados_apoio as unid_comp_femea on unid_comp_femea.sq_dados_apoio = ficha.sq_medida_comprimento_femea
                left outer join salve.dados_apoio as unid_senilidade_macho on unid_senilidade_macho.sq_dados_apoio = ficha.sq_unidade_senilid_rep_macho
                left outer join salve.dados_apoio as unid_senilidade_femea on unid_senilidade_femea.sq_dados_apoio = ficha.sq_unidade_senilid_rep_femea
                left outer join salve.dados_apoio as unid_longevidade_macho on unid_longevidade_macho.sq_dados_apoio = ficha.sq_unidade_longevidade_macho
                left outer join salve.dados_apoio as unid_longevidade_femea on unid_longevidade_femea.sq_dados_apoio = ficha.sq_unidade_longevidade_femea

                left outer join salve.dados_apoio as unid_tempo_geracional on unid_tempo_geracional.sq_dados_apoio = ficha.sq_medida_tempo_geracional
                left outer join salve.dados_apoio as tendencia_populacional on tendencia_populacional.sq_dados_apoio = ficha.sq_tendencia_populacional

                    -- nomes comuns
                left join lateral (
                    select x.sq_ficha, array_to_string( array_agg( x.no_comum ),'; ') as no_comum
                    from salve.ficha_nome_comum x
                    where x.sq_ficha = ficha.sq_ficha
                    group by x.sq_ficha
                    ) nome_comum on true
                    -- nomes antigos
                left join lateral (
                    select x.sq_ficha,array_to_string( array_agg( x.no_sinonimia ),'; ') as no_antigo
                    from salve.ficha_sinonimia x
                    where x.sq_ficha = ficha.sq_ficha
                    group by x.sq_ficha
                    ) nome_antigo on true
                where ficha.sq_ficha = :sqFicha
                """
/** /
 println ' '
 println cmdSql
 println fichaRevisao
 /**/

            List rows = sqlService.execSql(cmdSql, [sqFicha: fichaRevisao.sqFicha])
            if (!rows) {
                throw new Exception('Ficha inexistente ou ainda não validada.')
            }
            fichaRevisao = rows[0]
            // fazer tratamento do italico no nome cientifico
            fichaRevisao.nm_cientifico = Util.ncItalico(fichaRevisao.nm_cientifico)

            fichaRevisao.error = ''
            fichaRevisao.no_estado = ''
            List lista = fichaService.getUfsRegistro(fichaRevisao.sq_ficha)
            if (lista) {
                fichaRevisao.no_estado = lista.no_estado.join('; ')
            }
            fichaRevisao.no_bioma = ''
            lista = fichaService.getBiomasRegistro(fichaRevisao.sq_ficha)
            if (lista) {
                fichaRevisao.no_bioma = lista.no_bioma.join('; ')
            }
            fichaRevisao.no_bacia = ''
            lista = fichaService.getBaciasRegistro(fichaRevisao.sq_ficha)

            if (lista) {
                fichaRevisao.no_bacia = lista.no_bacia.join('; ')
            }
            fichaRevisao.areasRelevantes = _areasRelevantes(fichaRevisao.sq_ficha)
            fichaRevisao.habitosAlimentares = _habitosAlimentares(fichaRevisao.sq_ficha)
            fichaRevisao.interacoes = _interacoes(fichaRevisao.sq_ficha)
            fichaRevisao.ameacas = _ameacas(fichaRevisao.sq_ficha)
            fichaRevisao.usos = _usos(fichaRevisao.sq_ficha)
            fichaRevisao.historicoAvaliacoes = _historicoAvaliacoes(fichaRevisao.sq_ficha)
            fichaRevisao.convencoes = _convencoes(fichaRevisao.sq_ficha)
            fichaRevisao.acoes_conservacao = _acoes_conservacao(fichaRevisao.sq_ficha)
            fichaRevisao.mostrarDisclaimer = false
            // se a categoria validada for diferente da categoria oficial exibir o disclaimer
            if( fichaRevisao.historicoAvaliacoes ){
                Map ultimaAvaliacaoNacional = fichaRevisao.historicoAvaliacoes[0]
                //println ' '
                //println 'ULTIMA AVALIACAO NACIONAL'
                //println ultimaAvaliacaoNacional.ds_categoria
                if( ultimaAvaliacaoNacional.sq_categoria != fichaRevisao.sq_categoria_validada ){
                    fichaRevisao.mostrarDisclaimer = true
                }
            }
            //println 'Disclaimer:' + fichaRevisao.mostrarDisclaimer
            //println 'Cateogria validada:' + fichaRevisao.ds_categoria_validada

        } catch (Exception e) {
            fichaRevisao.error = e.getMessage()
        }
        return fichaRevisao
    }

    /**
     * retornar a lista de áreas relevantes da ficha
     * @param sqFicha
     * @return
     */
    protected List _areasRelevantes(Long sqFicha) {
        //TODO-passar essa query para uma funcao no banco de dados
        String cmdSql = """with passo1 as (
                select a.sq_ficha
                     , a.sq_ficha_area_relevancia
                     , a.sq_tipo_relevancia
                     , a.sq_uc_federal
                     , a.sq_uc_estadual
                     , a.sq_rppn
                     , tipo.ds_dados_apoio as ds_tipo_area
                     , uf.no_estado
                     , a.tx_local
                     , a.tx_relevancia
                from salve.ficha_area_relevancia a
                         inner join salve.dados_apoio as tipo on tipo.sq_dados_apoio = a.sq_tipo_relevancia
                         left join corporativo.vw_estado as uf on uf.sq_estado = a.sq_estado
                where a.sq_ficha = :sqFicha
            ), passo2 as (
                select passo1.sq_ficha_area_relevancia, array_to_string( array_agg(municipio.no_municipio),'; ') AS no_municipio
                from passo1
                left join salve.ficha_area_relevancia_municipio as arm on arm.sq_ficha_area_relevancia = passo1.sq_ficha_area_relevancia
                left outer join corporativo.vw_municipio municipio on municipio.sq_municipio = arm.sq_municipio
                group by passo1.sq_ficha_area_relevancia
            ), passo3 as (
                select ref.sq_registro as sq_ficha_area_relevancia, json_object_agg( ref.sq_registro, json_build_object(
                                'no_autor', coalesce( ref.no_autor, publicacao.no_autor ),
                                'nu_ano',coalesce( ref.nu_ano, publicacao.nu_ano_publicacao),
                                'de_titulo',publicacao.de_titulo ) ) as json_ref_bib

                from passo1
                inner join salve.ficha_ref_bib as ref on ref.sq_ficha = passo1.sq_ficha
                                                             and ref.sq_registro = passo1.sq_ficha_area_relevancia
                                                             and ref.no_tabela = 'ficha_area_relevancia'
                                                             and ref.no_coluna = 'sq_ficha_area_relevancia'
                left join taxonomia.publicacao on publicacao.sq_publicacao = ref.sq_publicacao
                group by ref.sq_registro
            ), passo4 as (
                select passo1.sq_ficha_area_relevancia, coalesce(coalesce( coalesce(uc_federal.sg_unidade_org,uc_estadual.nome_uc1), rppn.nome),passo1.tx_local) as no_local
                ,case when uc_federal.sq_pessoa is not null then 'salve.vw_pessoa|no_pessoa|'||uc_federal.sq_pessoa::text else
                    case when uc_estadual.gid is not null then 'geo.vw_mma_uc_nao_federais|nome_uc1|'||uc_estadual.gid::text else
                        case when rppn.ogc_fid is not null then 'geo.vw_rppn|nome|'||rppn.ogc_fid::text else '' end
                        end
                     end as no_tabela
                from passo1
                left outer join salve.vw_unidade_org uc_federal on uc_federal.sq_pessoa = passo1.sq_uc_federal
                left outer join geo.vw_mma_uc_nao_federais uc_estadual on uc_estadual.gid = passo1.sq_uc_estadual
                left outer join geo.vw_rppn as rppn on rppn.ogc_fid = passo1.sq_rppn
            )
            select
            passo1.ds_tipo_area as ds_tipo
            ,passo1.sq_tipo_relevancia as sq_tipo
            ,passo4.no_local
            ,passo1.no_estado
            ,passo2.no_municipio
            ,passo3.json_ref_bib
            ,passo4.no_tabela
            from passo1
            left join passo2 on passo2.sq_ficha_area_relevancia = passo1.sq_ficha_area_relevancia
            left join passo3 on passo3.sq_ficha_area_relevancia = passo1.sq_ficha_area_relevancia
            left join passo4 on passo4.sq_ficha_area_relevancia = passo1.sq_ficha_area_relevancia
            """
        return sqlService.execSql(cmdSql, [sqFicha: sqFicha])
    }


    /**
     * retornar a lista de habitos alimentares da ficha
     * @param sqFicha
     * @return
     */
    protected List _habitosAlimentares(Long sqFicha) {
        String cmdSql = """with passo1 as (
                    select a.sq_ficha
                         , a.sq_ficha_habito_alimentar
                         , a.sq_tipo_habito_alimentar
                         , tipo.ds_dados_apoio as ds_habito_alimentar
                    from salve.ficha_habito_alimentar a
                             inner join salve.dados_apoio as tipo on tipo.sq_dados_apoio = a.sq_tipo_habito_alimentar
                    where a.sq_ficha = :sqFicha
                ), passo2 as (
                         select ref.sq_registro                             as sq_ficha_area_relevancia,
                                json_object_agg(ref.sq_registro, json_build_object(
                                        'no_autor', coalesce(ref.no_autor, publicacao.no_autor),
                                        'nu_ano', coalesce(ref.nu_ano, publicacao.nu_ano_publicacao),
                                        'de_titulo', publicacao.de_titulo)) as json_ref_bib

                         from passo1
                                  inner join salve.ficha_ref_bib as ref on ref.sq_ficha = passo1.sq_ficha
                             and ref.sq_registro = passo1.sq_ficha_habito_alimentar
                             and ref.no_tabela = 'ficha_habito_alimentar'
                             and ref.no_coluna = 'sq_ficha_habito_alimentar'
                             left join taxonomia.publicacao on publicacao.sq_publicacao = ref.sq_publicacao
                         group by ref.sq_registro
                     )
                select passo1.ds_habito_alimentar as ds_tipo
                      , passo1.sq_tipo_habito_alimentar
                      , passo2.json_ref_bib
                from passo1
                         left join passo2 on passo2.sq_ficha_area_relevancia = passo1.sq_ficha_habito_alimentar
                         order by passo1.ds_habito_alimentar
                """
        return sqlService.execSql(cmdSql, [sqFicha: sqFicha])
    }

    /**
     * retornar a lista de interações da ficha com outras espécies
     * @param sqFicha
     * @return
     */
    protected List _interacoes(Long sqFicha) {
        String cmdSql = """with passo1 as (
                    select a.sq_ficha
                         , a.sq_ficha_interacao
                         , a.sq_categoria_iucn as sq_categoria
                         , a.sq_tipo_interacao
                         , tipo.ds_dados_apoio as ds_tipo_interacao
                         , taxon.no_taxon
                         , categoria.ds_dados_apoio as ds_categoria
                         , categoria.cd_dados_apoio as cd_categoria
                         , taxon.json_trilha->lower(nivel.co_nivel_taxonomico) ->>'no_taxon' as no_taxon
                    from salve.ficha_interacao a
                             inner join salve.dados_apoio as tipo on tipo.sq_dados_apoio = a.sq_tipo_interacao
                             inner join taxonomia.taxon on taxon.sq_taxon = a.sq_taxon
                             inner join taxonomia.nivel_taxonomico as nivel on nivel.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
                             left join salve.dados_apoio as categoria on categoria.sq_dados_apoio = a.sq_categoria_iucn
                    where a.sq_ficha = :sqFicha
                ), passo2 as (
                    select ref.sq_registro                             as sq_ficha_area_relevancia,
                           json_object_agg(ref.sq_registro, json_build_object(
                                   'no_autor', coalesce(ref.no_autor, publicacao.no_autor),
                                   'nu_ano', coalesce(ref.nu_ano, publicacao.nu_ano_publicacao),
                                   'de_titulo', publicacao.de_titulo)) as json_ref_bib

                    from passo1
                             inner join salve.ficha_ref_bib as ref on ref.sq_ficha = passo1.sq_ficha
                        and ref.sq_registro = passo1.sq_ficha_interacao
                        and ref.no_tabela = 'ficha_interacao'
                        and ref.no_coluna = 'sq_ficha_interacao'
                             left join taxonomia.publicacao on publicacao.sq_publicacao = ref.sq_publicacao
                    group by ref.sq_registro
                )
                select passo1.ds_tipo_interacao as ds_tipo
                     , passo1.sq_tipo_interacao
                     , passo2.json_ref_bib
                     , passo1.*
                from passo1
                         left join passo2 on passo2.sq_ficha_area_relevancia = passo1.sq_ficha_interacao
                order by passo1.ds_tipo_interacao
                """
        return sqlService.execSql(cmdSql, [sqFicha: sqFicha])
    }

    /**
     * retornar a lista de ameaças da ficha
     * @param sqFicha
     * @return
     */
    protected List _ameacas(Long sqFicha) {

        String cmdSql = """with passo1 as (
                    select a.sq_ficha
                          ,a.sq_ficha_ameaca
                         , a.sq_criterio_ameaca_iucn
                         , tipo.trilha_csv as trilha
                    from salve.ficha_ameaca a
                             inner join salve.vw_ameacas as tipo on tipo.id = a.sq_criterio_ameaca_iucn
                    where a.sq_ficha = :sqFicha
                    order by tipo.ordem
                ), passo2 as (
                    select ref.sq_registro                             as sq_ficha_ameaca,
                           json_object_agg(ref.sq_registro, json_build_object(
                                   'no_autor', coalesce(ref.no_autor, publicacao.no_autor),
                                   'nu_ano', coalesce(ref.nu_ano, publicacao.nu_ano_publicacao),
                                   'de_titulo', publicacao.de_titulo)) as json_ref_bib

                    from passo1
                    inner join salve.ficha_ref_bib as ref on ref.sq_ficha = passo1.sq_ficha
                      and ref.sq_registro = passo1.sq_ficha_ameaca
                      and ref.no_tabela = 'ficha_ameaca'
                      and ref.no_coluna = 'sq_ficha_ameaca'
                     left join taxonomia.publicacao on publicacao.sq_publicacao = ref.sq_publicacao
                    group by ref.sq_registro
                )
                select passo1.trilha as trilha
                     , passo1.sq_criterio_ameaca_iucn
                     , passo2.json_ref_bib
                from passo1
                left join passo2 on passo2.sq_ficha_ameaca = passo1.sq_ficha_ameaca
                """
        List result = []
        sqlService.execSql(cmdSql, [sqFicha: sqFicha]).each {
            it.trilha.split('\\|').each { linha ->
                // Ex: 2;Agropecuária e Aquacultura;698
                List itens = linha.split(';')
                if (itens.size() == 3) {
                    String jsonRefBib = it.sq_criterio_ameaca_iucn.toLong() == itens[2].toLong() ? it.json_ref_bib : ''
                    Map item = [codigo        : itens[0]
                                , descricao   : itens[1].toString()
                                , id          : itens[2].toLong()
                                , json_ref_bib: jsonRefBib]
                    if (!result.contains(item)) {
                        result.push(item)
                    }
                }
            }
        }
        return result
    }

    /**
     * retornar a lista de usos da ficha
     * @param sqFicha
     * @return
     */
    protected List _usos(Long sqFicha) {

        String cmdSql = """with passo1 as (
                select a.sq_ficha
                     ,a.sq_ficha_uso
                     , a.sq_uso
                     , tipo.trilha_csv as trilha
                from salve.ficha_uso a
                         inner join salve.vw_usos as tipo on tipo.id = a.sq_uso
                where a.sq_ficha = :sqFicha
                order by tipo.ordem
            ), passo2 as (
                select ref.sq_registro                             as sq_ficha_ameaca,
                       json_object_agg(ref.sq_registro, json_build_object(
                               'no_autor', coalesce(ref.no_autor, publicacao.no_autor),
                               'nu_ano', coalesce(ref.nu_ano, publicacao.nu_ano_publicacao),
                               'de_titulo', publicacao.de_titulo)) as json_ref_bib

                from passo1
                         inner join salve.ficha_ref_bib as ref on ref.sq_ficha = passo1.sq_ficha
                    and ref.sq_registro = passo1.sq_ficha_uso
                                                                      and ref.no_tabela = 'ficha_uso'
                    and ref.no_coluna = 'sq_ficha_uso'
                         left join taxonomia.publicacao on publicacao.sq_publicacao = ref.sq_publicacao
                group by ref.sq_registro
            )
            select passo1.trilha as trilha
                 , passo1.sq_uso
                 , passo2.json_ref_bib
            from passo1
                     left join passo2 on passo2.sq_ficha_ameaca = passo1.sq_ficha_uso
            """
        List result = []
        sqlService.execSql(cmdSql, [sqFicha: sqFicha]).each {
            it.trilha.split('\\|').each { linha ->
                // Ex: 1;Alimentação humana;2|1.2;Caça para comércio;80|1.2.2;Internacional;82
                List itens = linha.split(';')
                if (itens.size() == 3) {
                    String jsonRefBib = it.sq_uso.toLong() == itens[2].toLong() ? it.json_ref_bib : ''
                    Map item = [codigo        : itens[0]
                                , descricao   : itens[1].toString()
                                , id          : itens[2].toLong()
                                , json_ref_bib: jsonRefBib]
                    if (!result.contains(item)) {
                        result.push(item)
                    }
                }
            }
        }
        return result
    }

    /**
     * retornar a lista dos históricos das avaliaçãoes
     * @param sqFicha
     * @return
     */
    protected List _historicoAvaliacoes(Long sqFicha) {
        String cmdSql = """with passo1 as (
                select a.sq_ficha
                     , h.sq_taxon_historico_avaliacao
                     , h.sq_tipo_avaliacao as sq_tipo
                     , tipo.ds_dados_apoio as ds_tipo
                     , h.nu_ano_avaliacao as nu_ano
                     , abrangencia.sq_tipo as sq_tipo_abrangencia
                     , abrangencia.ds_tipo as ds_tipo_abrangencia
                     , abrangencia.de_abrangencia as ds_abrangencia
                     , h.sq_categoria_iucn as sq_categoria
                     , categoria.ds_dados_apoio as ds_categoria
                     , categoria.cd_dados_apoio as cd_categoria
                     , h.de_criterio_avaliacao_iucn as ds_criterio
                     --, h.tx_justificativa_avaliacao as tx_justificativa
                     , h.no_regiao_outra
                     , h.de_categoria_outra
                from salve.ficha  a
                inner join salve.ciclo_avaliacao as ciclo on ciclo.sq_ciclo_avaliacao = a.sq_ciclo_avaliacao
                inner join salve.taxon_historico_avaliacao h on h.sq_taxon = a.sq_taxon and h.nu_ano_avaliacao <= ciclo.nu_ano + 4
                inner join salve.dados_apoio as tipo on tipo.sq_dados_apoio = h.sq_tipo_avaliacao
                left outer join salve.vw_abrangencia as abrangencia on abrangencia.sq_abrangencia = h.sq_abrangencia
                left outer join salve.dados_apoio as categoria on categoria.sq_dados_apoio = h.sq_categoria_iucn
                where a.sq_ficha = :sqFicha
                order by tipo.de_dados_apoio_ordem,h.nu_ano_avaliacao desc
            ), passo2 as (
                select ref.sq_registro  as sq_taxon_historico_avaliacao,
                       json_object_agg(ref.sq_registro, json_build_object(
                               'no_autor', coalesce(ref.no_autor, publicacao.no_autor),
                               'nu_ano', coalesce(ref.nu_ano, publicacao.nu_ano_publicacao),
                               'de_titulo', publicacao.de_titulo)) as json_ref_bib

                from passo1
                    inner join salve.ficha_ref_bib as ref on ref.sq_ficha = passo1.sq_ficha
                    and ref.sq_registro = passo1.sq_taxon_historico_avaliacao
                    and ref.no_tabela = 'taxon_historico_avaliacao'
                    and ref.no_coluna = 'sq_taxon_historico_avaliacao'
                    left join taxonomia.publicacao on publicacao.sq_publicacao = ref.sq_publicacao
                group by ref.sq_registro
            )
            select passo1.*
                 , passo2.json_ref_bib
            from passo1
            left join passo2 on passo2.sq_taxon_historico_avaliacao = passo1.sq_taxon_historico_avaliacao
            """
        return sqlService.execSql(cmdSql, [sqFicha: sqFicha])
    }

    /**
     * retornar a lista de presenca em convencoes
     * @param sqFicha
     * @return
     */
    protected List _convencoes(Long sqFicha) {
        String cmdSql = """select convencao.sq_dados_apoio as sq_convencao
                     , convencao.ds_dados_apoio as ds_convencao
                     , lista.nu_ano
                        from salve.ficha_lista_convencao lista
                        inner join salve.dados_apoio as convencao on convencao.sq_dados_apoio = lista.sq_lista_convencao
                        where sq_ficha = :sqFicha
                        order by convencao.de_dados_apoio_ordem,convencao.ds_dados_apoio
                        """
        return sqlService.execSql(cmdSql, [sqFicha: sqFicha])
    }

    /**
     * retornar a lista de açoes de conservacao
     * @param sqFicha
     * @return
     */
    protected List _acoes_conservacao(Long sqFicha) {
        String cmdSql = """with passo1 as (
                select a.sq_ficha
                     ,a.sq_ficha_acao_conservacao
                     ,a.sq_acao_conservacao
                     ,tipo.trilha_csv as trilha
                     ,situacao.sq_dados_apoio as sq_situacao
                     ,situacao.ds_dados_apoio as ds_situacao
                     ,pan.sq_plano_acao
                     ,pan.sg_plano_acao
                     ,pan.tx_plano_acao
                from salve.ficha_acao_conservacao  a
                inner join salve.vw_acao_conservacao as tipo on tipo.id = a.sq_acao_conservacao
                inner join salve.dados_apoio as situacao on situacao.sq_dados_apoio = a.sq_situacao_acao_conservacao
                    left outer join salve.plano_acao as pan on pan.sq_plano_acao = a.sq_plano_acao
                where a.sq_ficha = :sqFicha
                order by tipo.ordem
            ), passo2 as (
                select ref.sq_registro  as sq_ficha_acao_conservacao,
                       json_object_agg(ref.sq_registro, json_build_object(
                               'no_autor', coalesce(ref.no_autor, publicacao.no_autor),
                               'nu_ano', coalesce(ref.nu_ano, publicacao.nu_ano_publicacao),
                               'de_titulo', publicacao.de_titulo)) as json_ref_bib

                from passo1
                    inner join salve.ficha_ref_bib as ref on ref.sq_ficha = passo1.sq_ficha
                    and ref.sq_registro = passo1.sq_ficha_acao_conservacao
                    and ref.no_tabela = 'ficha_acao_conservacao'
                    and ref.no_coluna = 'sq_ficha_acao_conservacao'
                    left join taxonomia.publicacao on publicacao.sq_publicacao = ref.sq_publicacao
                group by ref.sq_registro
            )
            select passo1.*
                 , passo2.json_ref_bib
            from passo1
            left join passo2 on passo2.sq_ficha_acao_conservacao = passo1.sq_ficha_acao_conservacao
            """

        List result = []
        sqlService.execSql(cmdSql, [sqFicha: sqFicha]).each {
            it.trilha.split('\\|').each { linha ->
                // Ex: 1;Proteção territorial;553|1.1;Proteção de locais/áreas;554|1.1.1;Identificação de novas áreas protegidas;555
                List itens = linha.split(';')
                if (itens.size() == 3) {
                    String jsonRefBib = ''
                    String txPan = ''
                    String sgPan = ''
                    Long sqSituacao = null
                    String dsSituacao = ''
                    Long sqPan = null
                    if (it.sq_acao_conservacao.toLong() == itens[2].toLong()) {
                        jsonRefBib = it.json_ref_bib
                        txPan = it.tx_plano_acao
                        sgPan = it.sg_plano_acao
                        sqPan = it.sq_plano_acao
                        sqSituacao = it.sq_situacao
                        dsSituacao = it.ds_situacao
                    }
                    Map item = [codigo         : itens[0]
                                , descricao    : itens[1].toString()
                                , id           : itens[2].toLong()
                                , sq_plano_acao: sqPan
                                , tx_plano_acao: txPan
                                , sg_plano_acao: sgPan
                                , sq_situacao  : sqSituacao
                                , ds_situacao  : dsSituacao
                                , json_ref_bib : jsonRefBib]
                    if (!result.contains(item)) {
                        result.push(item)
                    }
                }
            }
        }
        return result
    }

    /**
     * método para adicionar as fichas na fila para tradução assyncrona em lote
     * @param params
     * @return
     */
    void traduzirFichasEmLote(Map params = [:]) {
        String cmdSql
        Map sqlParams = [:]
        List whereFinal = []
        List situacoesValidas = ['FINALIZADA', 'PUBLICADA']

        // somente as fichas não traduzidas ou que ja finalizaram a tradução em 100% podem ser adicionadas à fila de tradução
        whereFinal.push('( fila.nu_percentual is null or fila.nu_percentual = 100 )')

        if (!params.ids && !params.all) {
            throw new Exception('Nenhuma ficha selecionada para tradução')
        }

        // FILTRO PELO IDS SELECIONADOS
        if (params.ids && params.all == 'N') {
            //whereFinal.push("ficha.sq_ficha in (${params.ids})")
            whereFinal.push("ficha.sq_ficha = any( array[${params.ids}] )")
        }

        // FILTRO PELO NOME CIENTIFICO
        if (params.nmCientificoFiltro) {
            whereFinal.push("ficha.nm_cientifico ilike :nmCientifico")
            sqlParams.nmCientifico = '%' + params.nmCientificoFiltro + '%'
        }

        // FILTRO PELA UNIDADE ORGANIZACIONAL
        if (params.sgUnidadeFiltro) {
            whereFinal.push("unidade.sg_unidade_org ilike :sgUnidade")
            sqlParams.sgUnidade = '%' + params.sgUnidadeFiltro + '%'
        }

        if (params.sqUnidadeFiltro) {
            whereFinal.push("unidade.sq_pessoa =  :sqUnidade")
            sqlParams.sqUnidade = params.sqUnidadeFiltro.toLong()
        }

        // FILTRAR PELA SITUACAO DA FICHA
        if (params.sqSituacaoFiltro) {
            whereFinal.push("ficha.sq_situacao_ficha = :sqSituacaoFicha")
            sqlParams.sqSituacaoFicha = params.sqSituacaoFiltro.toLong()
        } else {
            // FILTRAR AS SITUAÇÕES VÁLIDAS
            whereFinal.push("situacao.cd_sistema in (${situacoesValidas.collect { "'" + it + "'" }.join(',')})")
        }

        // FILTRAR PELO NOME COMUM
        if (params.nmComumFiltro) {
            whereFinal.push("EXISTS ( SELECT NULL from salve.ficha_nome_comum fnc where fnc.sq_ficha = ficha.sq_ficha and public.fn_remove_acentuacao(fnc.no_comum) ilike :nmComum limit 1 )")
            sqlParams.nmComum = '%' + Util.removeAccents(params.nmComumFiltro) + '%'

        }
        // FILTRO PELO NIVEL TAXONOMICO
        if (params.nivelFiltro && params.sqTaxonFiltro) {
            if (params.sqTaxonFiltro.toString().indexOf(',')) {
                whereFinal.push("(taxon.json_trilha->'${params.nivelFiltro.toLowerCase()}'->>'sq_taxon')::integer in ( ${params.sqTaxonFiltro})")
            } else {
                whereFinal.push("(taxon.json_trilha->'${params.nivelFiltro.toLowerCase()}'->>'sq_taxon')::integer = :sqTaxon")
                sqlParams.sqTaxon = params.sqTaxonFiltro.toLong()
            }
        }

        // FILTRAR PELO GRUPO AVALIADO
        if (params.sqGrupoFiltro) {
            if (params.sqGrupoFiltro.toString().indexOf(',')) {
                whereFinal.push("ficha.sq_grupo in ( ${params.sqGrupoFiltro} )")
            } else {
                whereFinal.push("ficha.sq_grupo = :sqGrupoFiltro")
                sqlParams.sqGrupoFiltro = params.sqGrupoFiltro.toLong()
            }
            if (params.sqSubgrupoFiltro) {
                if (params.sqSubgrupoFiltro.toString().indexOf(',')) {
                    whereFinal.push("ficha.sq_subgrupo in ( ${params.sqSubgrupoFiltro} )")
                } else {
                    whereFinal.push("ficha.sq_subgrupo = :sqSubgrupoFiltro")
                    sqlParams.sqSubgrupoFiltro = params.sqSubgrupoFiltro.toLong()
                }
            }
        }
        cmdSql = """select distinct ficha.sq_ficha, ficha.nm_cientifico
                    from salve.ficha
                    inner join salve.dados_apoio situacao on situacao.sq_dados_apoio = ficha.sq_situacao_ficha
                    inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                    inner join taxonomia.nivel_taxonomico as nivel on nivel.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
                    inner join salve.vw_unidade_org as unidade on unidade.sq_pessoa = ficha.sq_unidade_org
                    left outer join salve.traducao_fila as fila on fila.sq_ficha = ficha.sq_ficha
                    ${whereFinal ? 'where ' + whereFinal.join('\nand ') : ''}
                    order by ficha.nm_cientifico
                    """
        /*
        println ' '
        println 'Adicionar filhas na fila'
        println cmdSql
        println sqlParams
        */
        sqlService.execSql(cmdSql, sqlParams, null, 0).each { row ->
            Ficha ficha = Ficha.get(row.sq_ficha.toLong())
            if (ficha) {
                //println 'Add:' + ficha.nmCientifico
                try {
                    new TraducaoFila(ficha: ficha, nuPercentual: 0, stExecutando: false).save()
                    //println '  - ok '
                } catch (Exception e) {
                    println e.getMessage()
               }
            }
        }
    }

    /**
     * método para excluir a tradução das fichas em lote
     * @param params
     * @return
     */
    void excluirTraducaoFichasEmLote(Map params = [:]) {
        String cmdSql
        Map sqlParams = [:]
        List whereFinal = []

        if (!params.ids && !params.all) {
            throw new Exception('Nenhuma ficha selecionada para tradução')
        }

        // FILTRO PELO IDS SELECIONADOS
        if (params.ids && params.all == 'N') {
            //whereFinal.push("ficha.sq_ficha in (${params.ids})")
            whereFinal.push("ficha.sq_ficha = any( array[${params.ids}] )")
        }

        // FILTRO PELO NOME CIENTIFICO
        if (params.nmCientificoFiltro) {
            whereFinal.push("ficha.nm_cientifico ilike :nmCientifico")
            sqlParams.nmCientifico = '%' + params.nmCientificoFiltro + '%'
        }

        // FILTRO PELA UNIDADE ORGANIZACIONAL
        if (params.sgUnidadeFiltro) {
            whereFinal.push("unidade.sg_unidade_org ilike :sgUnidade")
            sqlParams.sgUnidade = '%' + params.sgUnidadeFiltro + '%'
        }

        if (params.sqUnidadeFiltro) {
            whereFinal.push("unidade.sq_pessoa =  :sqUnidade")
            sqlParams.sqUnidade = params.sqUnidadeFiltro.toLong()
        }

        // FILTRAR PELA SITUACAO DA FICHA
        if (params.sqSituacaoFiltro) {
            whereFinal.push("ficha.sq_situacao_ficha = :sqSituacaoFicha")
            sqlParams.sqSituacaoFicha = params.sqSituacaoFiltro.toLong()
        }

        // FILTRAR PELO NOME COMUM
        if (params.nmComumFiltro) {
            whereFinal.push("EXISTS ( SELECT NULL from salve.ficha_nome_comum fnc where fnc.sq_ficha = ficha.sq_ficha and public.fn_remove_acentuacao(fnc.no_comum) ilike :nmComum limit 1 )")
            sqlParams.nmComum = '%' + Util.removeAccents(params.nmComumFiltro) + '%'

        }
        // FILTRO PELO NIVEL TAXONOMICO
        if (params.nivelFiltro && params.sqTaxonFiltro) {
            if (params.sqTaxonFiltro.toString().indexOf(',')) {
                whereFinal.push("(taxon.json_trilha->'${params.nivelFiltro.toLowerCase()}'->>'sq_taxon')::integer in ( ${params.sqTaxonFiltro})")
            } else {
                whereFinal.push("(taxon.json_trilha->'${params.nivelFiltro.toLowerCase()}'->>'sq_taxon')::integer = :sqTaxon")
                sqlParams.sqTaxon = params.sqTaxonFiltro.toLong()
            }
        }

        // FILTRAR PELO GRUPO AVALIADO
        if (params.sqGrupoFiltro) {
            if (params.sqGrupoFiltro.toString().indexOf(',')) {
                whereFinal.push("ficha.sq_grupo in ( ${params.sqGrupoFiltro} )")
            } else {
                whereFinal.push("ficha.sq_grupo = :sqGrupoFiltro")
                sqlParams.sqGrupoFiltro = params.sqGrupoFiltro.toLong()
            }
            if (params.sqSubgrupoFiltro) {
                if (params.sqSubgrupoFiltro.toString().indexOf(',')) {
                    whereFinal.push("ficha.sq_subgrupo in ( ${params.sqSubgrupoFiltro} )")
                } else {
                    whereFinal.push("ficha.sq_subgrupo = :sqSubgrupoFiltro")
                    sqlParams.sqSubgrupoFiltro = params.sqSubgrupoFiltro.toLong()
                }
            }
        }
        // excluir da fila de tradução se houver
        cmdSql = """delete from salve.traducao_fila
                     where sq_ficha in (
                       select ficha.sq_ficha from salve.ficha
                       inner join salve.dados_apoio situacao on situacao.sq_dados_apoio = ficha.sq_situacao_ficha
                       inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                       inner join taxonomia.nivel_taxonomico as nivel on nivel.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
                       inner join salve.vw_unidade_org as unidade on unidade.sq_pessoa = ficha.sq_unidade_org
                       ${whereFinal ? 'where ' + whereFinal.join('\nand ') : ''}
                    )
        """
        sqlService.execSql(cmdSql, sqlParams, null, 0)


        // excluir as traduções
        cmdSql = """delete from salve.traducao_revisao where sq_traducao_revisao in (
                    select tr.sq_traducao_revisao
                    from salve.traducao_revisao tr
                    inner join salve.traducao_tabela tt on tt.sq_traducao_tabela = tr.sq_traducao_tabela
                    where tr.sq_registro in (
                       select ficha.sq_ficha from salve.ficha
                       inner join salve.dados_apoio situacao on situacao.sq_dados_apoio = ficha.sq_situacao_ficha
                       inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                       inner join taxonomia.nivel_taxonomico as nivel on nivel.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
                       inner join salve.vw_unidade_org as unidade on unidade.sq_pessoa = ficha.sq_unidade_org
                       ${whereFinal ? 'where ' + whereFinal.join('\nand ') : ''}
                    )
                    and tt.no_tabela = 'ficha'
                )
        """
        sqlService.execSql(cmdSql, sqlParams, null, 0)

    }

    /**
     * disparar o processamento assyncrono de tradução das fichas adicionadas na fila
     * @return
     */
    void runAsyncTranslate( Map params = [:] ) {
        if ( ! params.sqPessoa) {
            throw new Exception('Id do usuário não informado')
        }
        PessoaFisica usuario = PessoaFisica.get(params.sqPessoa.toLong())
        if ( ! usuario ) {
            throw new Exception('Id usuário inexistente')
        }
       runAsync {
            _asyncTranslate([usuario: usuario])
        }
    }

    void _asyncTranslate(Map data = [:]) {
        List rowsCamposFichaTraduzir = TraducaoTabela.findAllByNoTabela('ficha')
        List lista = TraducaoFila.findAllByNuPercentualLessThanAndStExecutando(100,false)
        String nomeUsuario = data?.usuario?.noPessoa ? ' - ' + data.usuario.noPessoa+ '  cpf: ' + data?.usuario?.nuCpf + ')': ''
        Util.printLog('TraducaoFichaService'
            ,'Iniciando traducao das fichas em lote de ' + lista.size()+' ficha(s)' + nomeUsuario,'')

        lista.eachWithIndex { traducaoFila, index->
            if( traducaoFila.nuPercentual == 0 ) {
                TraducaoFila.withNewTransaction {
                    TraducaoFila tf = TraducaoFila.get(traducaoFila.id)
                    tf.stExecutando = true
                    tf.save(flush: true)
                }

                Ficha ficha = traducaoFila.ficha
                Util.printLog('TraducaoFichaService', 'Traduzindo a ficha ' + (index + 1) + ' - ' + ficha.nmCientifico + nomeUsuario)
                int qtdCamposTraduzidos = 0
                // loop colunas a serem traduzidas
                rowsCamposFichaTraduzir.eachWithIndex { traducaoTabela, campoIndex ->
                    TraducaoRevisao traducaoRevisao = TraducaoRevisao.findByTraducaoTabelaAndSqRegistroAndTxTraduzidoIsNotNull(traducaoTabela, ficha.id)
                    // traduzir apenas o campos que não tiverem sido traduzido
                    if (!traducaoRevisao) {
                        String txOriginal = Util.stripTagsTraducao(ficha[Util.toCamelCase(traducaoTabela.noColuna)])
                        if (!txOriginal.isEmpty()) {
                            try {
                                // TODO - CHAMAR SERVICO DE TRADUCAO WEB
                                //String txTraduzido = _doRequestTranslateServices( txOriginal, 'libre')
                                String txTraduzido = requestService.doTranslate( txOriginal, 'libre')
                                if (txTraduzido) {
                                    qtdCamposTraduzidos++
                                    TraducaoRevisao.withNewTransaction {
                                        //println '  - traduzindo a coluna ' + Util.toCamelCase(traducaoTabela.noColuna)
                                        if (!traducaoRevisao) {
                                            traducaoRevisao = new TraducaoRevisao()
                                        }
                                        traducaoRevisao.txTraduzido = txTraduzido
                                        traducaoRevisao.sqRegistro = ficha.id.toLong()
                                        traducaoRevisao.traducaoTabela = traducaoTabela
                                        traducaoRevisao.dtRevisao = null
                                        traducaoRevisao.pessoa = data.usuario
                                        traducaoRevisao.save(flush: true)
                                    }
                                }
                            } catch (RuntimeException e) {
                                Util.printLog('TraducaoFichaService','Erro traducao da ficha ' + ficha.nmCientifico + nomeUsuario, e.getMessage() )
                            }
                        }
                    } else {
                        qtdCamposTraduzidos++
                    }
                    // atualizar o percentual da tradução na fila
                    int percent = Math.round((campoIndex + 1) / rowsCamposFichaTraduzir.size() * 100)
                    if (traducaoFila.nuPercentual != percent) {
                        TraducaoFila.withNewTransaction {
                            TraducaoFila tf = TraducaoFila.get(traducaoFila.id)
                            tf.nuPercentual = Math.min(100,percent)
                            tf.save(flush: true)
                        }
                    }
                }

                // alterar status executando para false
                TraducaoFila.withNewTransaction {
                    TraducaoFila tf = TraducaoFila.get(traducaoFila.id)
                    tf.stExecutando = false
                    tf.save(flush: true)
                }
            }
        }
        // excluir da fila as traduções completadas
        TraducaoFila.executeUpdate("delete TraducaoFila c where c.stExecutando=false ")
        Util.printLog('TraducaoFichaService'
            ,'Fim tradução da(s) ficha(s) em lote.\nA fila de tradução foi limpa')
    }

    /**
     * Fazer a requisição aos serviços de tradução on-line
     * @param txOriginal
     * @return
     */
    String _doRequestTranslateServices(String txOriginal = '',String serviceName = 'libre') {
        String resultado = ''
        Map requestParams = [:]
        String url = ''
        if (!txOriginal) {
            return null
        }
        // para evitar bloqueio dos serviços de tradução, simular tempo de 1 a 3 segundos entre as requisições
        if ( serviceName != 'libre' ) {
            int delayBetweenRequests = Math.abs(new Random().nextInt() % (3000 - 1000)) + 1000
            if ( delayBetweenRequests ) {
                sleep( delayBetweenRequests )
            }
        }

        try {
            // GOOGLE
            if( serviceName == 'google') {
                //requestParams = [client: 'gtx', sl: 'pt-br', 'tl': 'en', 'dt': 't', 'q': URLEncoder.encode(txOriginal, "UTF-8")]
                requestParams = [:]
                url = grailsApplication.config.traducao.urlGoogle + URLEncoder.encode(txOriginal, "UTF-8")
//println 'Traduzindo em:' + url

                resultado = requestService.doGet(url, requestParams, 'text')
                if (resultado) {
                    //println ' '
                    //println 'GOOGLE'
                    JSONArray ja = JSON.parse(resultado)
                    List paragrafos = []
                    if (ja[0][0][0]) {
                        ja[0].each { item ->
                            paragrafos.push(item[0])
                        }
                    }
                    resultado = paragrafos.join('<br>')
                } else {
                    resultado = _doRequestTranslateServices( txOriginal, 'libre')
                }
            } else if( serviceName == 'libre' ) {
                //println 'Utilizando LIBRETRANSLATE'
                url = grailsApplication.config.traducao.urlLibreTranslate
                //url = "http://144.126.147.57:5000/translate"
                String txOriginalTratado = Util.stripTagsTraducao( txOriginal )
                txOriginalTratado = txOriginalTratado.replaceAll(/\n/,'')
/*
println ' '
println ' '
println 'TEXTO PORTUGUES'
println txOriginalTratado
*/
//println 'Url servico traducao: [' + url+']'

                requestParams = [source: 'pt', target: 'en', format: 'html', q: txOriginalTratado]
                resultado = requestService.doPost(url, requestParams, 'text')
                if (resultado) {
                    //println ' '
                    //println 'LIBRETRANSLATE'
                    JSONArray jsonArray = JSON.parse('[' + resultado + ']')
                    resultado = jsonArray[0].translatedText
/*
println 'TEXTO TRADUZIDO'
println resultado
println '---------------------------------------------'
*/
                }
            }
        } catch (Exception e) {
            if( serviceName == 'google' ) {
                return _doRequestTranslateServices( txOriginal, 'libre')
            }
            Util.printLog('SALVE - TraducaoFichaService._doRequestTranslateServices()'
                , 'Erro ao traduzir texto: ' + txOriginal
                , e.getMessage())
            resultado = ''
        }
        /*if( resultado ) {
            println 'servico traducao utilizado: ' + serviceName + ':'+new Date().format('dd/MM/yyyy HH:mm:ss')
        }*/
        return resultado ?: null
    }

    /**
     * Ler os percentuais traduzidos das fichas para atualizar o gride de fichas à medida que as fichas vão sendo traduzidas
     * @param params
     * @return
     */
    Map getPercentuaisPage(Map params = [:]) {
        //println  ' '
        //println 'getPercnetuais()...'
        Map resultado = [finished: true, rows: []]
        if (params.ids) {
            String cmdSql = """select sq_ficha
                        , nu_percentual as andamento_traducao
                        from salve.traducao_fila
                        where traducao_fila.sq_ficha = any( array[${params.ids}])
            """
            sqlService.execSql(cmdSql).each { row ->
                resultado.rows.push([sqFicha                : row.sq_ficha
                                     , nuAndamentoTraducao: row.andamento_traducao
                ])
            }
            if (resultado.rows) {
                resultado.finished = false
            } else {
                resultado.finished = true
                cmdSql = """select * from salve.fn_traducao_calcular_percentuais('${params.ids}')"""
                sqlService.execSql(cmdSql).each { row ->

                    // se o percentual revisado for igual ao percentual traduzido então o percentual revisado é 100%
                    if( row.nu_percentual_traduzido > 0 &&  row.nu_percentual_revisado ==  row.nu_percentual_traduzido ){
                        row.nu_percentual_revisado = 100
                    }
                        resultado.rows.push([sqFicha                : row.sq_ficha
                                         , nuAndamentoTraducao  : 100
                                         , nuPercentualTraduzido: Math.min(100,row.nu_percentual_traduzido)
                                         , nuPercentualRevisado : Math.min(100,row.nu_percentual_revisado)
                    ])

                }
            }
        }
        return resultado
    }

    /**
     * metodo para gravacao da tradução ou revisão da tradução no banco de dados
     * @return
     */
    String saveTextoTraduzido( Map params = [:]) {
            PessoaFisica pessoaFisica = PessoaFisica.get( params.sqPessoa.toLong() )
            if( !pessoaFisica ){
                throw new Exception('Usuário não cadastrado')
            }

            if( !params.noTabela ){
                throw new Exception('Nome da tabela não informado')
            }
            if( !params.noColuna ){
                throw new Exception('Nome da coluna não informado')
            }
            if( !params.sqRegistro ){
                throw new Exception('Id do registro não informado')
            }

            params.noColuna = Util.toSnakeCase(params.noColuna)
            // verificar se a coluna está na lista de colunas que podem ser traduzidas
            TraducaoTabela traducaoTabela = TraducaoTabela.findByNoTabelaAndNoColuna(params.noTabela, params.noColuna)
            if( ! traducaoTabela ) {
                traducaoTabela = new TraducaoTabela()
                traducaoTabela.noTabela = params.noTabela
                traducaoTabela.noColuna = params.noColuna
                traducaoTabela.dsColuna = params.noTabela + '/' + Util.toCamelCase( params.noColuna )
                traducaoTabela.save(flush:true)
                //throw new Exception('Coluna não cadastrada para tradução')
            }
            // verificar se o registro já possui uma tradução e fazer a inclusão ou alteração
            TraducaoRevisao traducaoRevisao = TraducaoRevisao.findByTraducaoTabelaAndSqRegistro(traducaoTabela,params.sqRegistro.toLong())
            if( !traducaoRevisao ){
                traducaoRevisao = new TraducaoRevisao()
                traducaoRevisao.traducaoTabela = traducaoTabela
                traducaoRevisao.sqRegistro = params.sqRegistro.toLong()
            }
            // limpar o texto removendo tags html desnecessárias
            params.txTraduzido = Util.stripTagsTraducao( params.txTraduzido )
            traducaoRevisao.pessoa = pessoaFisica
            traducaoRevisao.txTraduzido = params.txTraduzido ?: null
            traducaoRevisao.dtRevisao   = null
            traducaoRevisao.save(flush:true)
            _saveLogTraducaoRevisao(traducaoRevisao)

            // retornar o texto traduzido com stripTag aplicado para o cliente
            return traducaoRevisao.txTraduzido
    }

    /**
     * metodo para marcar/desmarcar se a tradução está revisada
     * @return
     */
    Map saveRevisado( Map params = [:] ){
        Map res = [:]
        PessoaFisica pessoaFisica = PessoaFisica.get( params.sqPessoa )

        if( !pessoaFisica ){
            throw new Exception('Usuário não cadastrado')
        }

        if( !params.noTabela ){
            throw new Exception('Nome da tabela não informado')
        }

        if( !params.noColuna ){
            throw new Exception('Nome da coluna não informado')
        }

        if( !params.sqRegistro ){
            throw new Exception('Id do registro não informado')
        }

        params.noColuna = Util.toSnakeCase(params.noColuna)

        // verificar se a coluna está na lista de colunas que podem ser traduzidas
        TraducaoTabela traducaoTabela = TraducaoTabela.findByNoTabelaAndNoColuna(params.noTabela, params.noColuna)
        if( ! traducaoTabela ) {
            throw new Exception('Coluna '+params.noTabela+'/'+params.noColuna+' não cadastrada para tradução')
        }
        // verificar se o registro já possui uma tradução e fazer a inclusão ou alteração
        TraducaoRevisao traducaoRevisao = TraducaoRevisao.findByTraducaoTabelaAndSqRegistro(traducaoTabela,params.sqRegistro.toLong())
        if( !traducaoRevisao ){
            throw new Exception('Texto não foi traduzido')
        }
        if( params.inRevisado.toUpperCase() == 'S' ) {
            traducaoRevisao.dtRevisao = new Date()
            traducaoRevisao.pessoa = pessoaFisica
            res.noRevisor = ' por ' + Util.nomeAbreviado( Util.capitalize(pessoaFisica.noPessoa) )  + ' em '+ traducaoRevisao.dtRevisao.format('dd/MM/yyyy HH:mm')
        } else {
            traducaoRevisao.dtRevisao = null
            traducaoRevisao.pessoa = null
            res.noRevisor=''
        }
        traducaoRevisao.save(flush:true)

        // atualizar o percentual na ficha
        if( params.noTabela.toUpperCase() == 'FICHA' ) {
            sqlService.execSql("""select * from salve.fn_traducao_calcular_percentuais('${params.sqRegistro.toString()}')""").each { row ->
                TraducaoFichaPercentual traducaoFichaPercentual = TraducaoFichaPercentual.get(row.sq_ficha.toLong())
                if (!traducaoFichaPercentual) {
                    traducaoFichaPercentual = new TraducaoFichaPercentual(ficha : Ficha.get(params.sqRegistro.toLong())  )
                }
                traducaoFichaPercentual.nuPercentualTraduzido   = row.nu_percentual_traduzido
                traducaoFichaPercentual.nuPercentualRevisado    = row.nu_percentual_revisado
                traducaoFichaPercentual.save(flush:true)
            }
        }

        _saveLogTraducaoRevisao( traducaoRevisao )
        return res
    }

    /**
     * método para registrar as alterações realizadas na tabela de revisaão das fichas
     * @param traducaoRevisao
     */
    protected void _saveLogTraducaoRevisao( TraducaoRevisao traducaoRevisao ){
        // gravar historico das alterações
        TraducaoRevisaoHist traducaoRevisaoHist = TraducaoRevisaoHist.findByTraducaoRevisao(traducaoRevisao,[max:1, sort:'dtInclusao', order:'desc' ])
        if( ! traducaoRevisaoHist || traducaoRevisaoHist.pessoa != traducaoRevisao.pessoa ){
            //println 'NOVO REGISTRO HISTORICO'
            traducaoRevisaoHist = new TraducaoRevisaoHist()
            traducaoRevisaoHist.pessoa = traducaoRevisao.pessoa
            traducaoRevisaoHist.traducaoRevisao=traducaoRevisao

        }
        traducaoRevisaoHist.txTraduzido = traducaoRevisao.txTraduzido
        traducaoRevisaoHist.dtRevisao   = traducaoRevisao.dtRevisao
        traducaoRevisaoHist.dtInclusao  = new Date()
        traducaoRevisaoHist.save( flush:true )

    }
}
