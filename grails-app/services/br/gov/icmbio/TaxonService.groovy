package br.gov.icmbio

import grails.converters.JSON;
import grails.util.Environment;
import groovy.sql.Sql


class TaxonService {

	static transactional = false

	def dataSource
	def grailsApplication

	String importarEspecies(Integer sqPessoa, String historico = null ) {

        String delimitador = ';' // tem que ser ponto e virgula devido ao nome do autor conter virgula
        String fileName = '/data/salve-estadual/arquivos/planilha_importacao_taxon.csv'
        // ler arquivo csv
        File fcsv = new File(fileName);
        if (!fcsv.exists()) {
            log.info('Arquivo ' + fileName + ' não encontrado!')
            return 'Arquivo ' + fileName + ' não encontrado!'
        }

        if ( ! historico )
        {
            return '<h2>Sistema Salve</h2><hr><h3>Importação de planilha de taxon no formato csv</h3>' +
                    'url: <b>http://localhost:8080/salve-estadual/main/importarEspecies/texto para histórico</b>'
        }

        List itens = [];
        List niveisValidos = ["REINO", "FILO", "CLASSE", "ORDEM", "FAMILIA", "SUBFAMILIA", "TRIBO", "GENERO", "ESPECIE", "SUBESPECIE"]
        List existentes = [] // evitar pesquisar se o mesmo nivel existe duas vezes
        Boolean existe = false
        Boolean erro=false
        NivelTaxonomico nivelTaxonomico
        Taxon pai
        Taxon taxon
        String autor

        if (!sqPessoa) {
            log.info('Login SICAE não realizado ou sqPessoa não informado!')
            return 'Login SICAE não realizado ou sqPessoa não informado!'

        }
        Pessoa pessoa = Pessoa.get(sqPessoa)
        if ( ! pessoa) {
            log.info('Pessoa/Pessoa Fisica ' + sqPessoa + ' não cadastrada!')
            return 'Pessoa/Pessoa Fisica ' + sqPessoa + ' não cadastrada!'
        }
        TaxonGrupo grupo = TaxonGrupo.findByCoGrupo('SISTEMA');
        if (!grupo) {
            log.info('Grupo SISTEMA não cadastrado na tabela taxonomia.grupo')
            return 'Grupo SISTEMA não cadastrado na tabela taxonomia.grupo'
        }
        TaxonSituacao situacaoNome = TaxonSituacao.findByGrupoAndCoSituacao(grupo, 'INCLUSAO');
        if (!situacaoNome) {
            log.info('Situação nome INCLUSÃO do grupo SISTEMA não cadastrado na tabela taxonomia.situacao')
            return 'Situação nome INCLUSÃO do grupo SISTEMA não cadastrado na tabela taxonomia.situacao'
        }
        fcsv.eachLine { line, number ->

            // if( line.indexOf('californiensis') > -1)
            if ( true ) {
                //line = line.replaceAll(/"/, '')
                //log.info( line)
                //log.info( '<br>')
                // utilizar ponto e virgula porque nome do autor contem virgula
                if ( line.indexOf(delimitador) && line.indexOf('nm_') == -1) {
                    println ' '
                    println number - 1 + ') ' + line
                    itens = line.split(delimitador)
                    // a última coluna é sempre o autor
                    autor = itens[itens.size() - 1] ?: ''
                    autor = autor.replaceAll(/;/,',')
                    if (autor && (autor =~ /\d/)) {
                        // println 'Autor antes: ' + autor
                        // remover espaços duplos e virgulas duplas
                        autor = autor.replaceAll(/  {2,5}|/, '').replaceAll(/\,{1,5}/, ',').replaceAll(/ \,/, ',').replaceAll(/"/, '')
                        println 'Autor: ' + autor
                    }
                    pai = null

                    itens.eachWithIndex { nome, nivel ->
                        nome = nome.trim().replaceAll(/"/,'')

                        // nome não pode possuir numero
                        if (nome != '' && !(nome =~ /\d/)) {
                            List nomes = nome.split(' ')
                            // a espécie pode vir completa genero + especie
                            if( nomes.size() > 1 )
                            {
                                nome = nomes[1].toString().trim()
                            }

                            // ignorar nomes: reino, classe, classe1, ordem
                            if (niveisValidos.indexOf(nome.toUpperCase().replaceAll(/[0-9]/, '')) == -1) {

                                nivelTaxonomico = NivelTaxonomico.findByCoNivelTaxonomico(niveisValidos[nivel])
                                if (nivelTaxonomico) {
                                    // procurar o taxon
                                    erro = false
                                    try {
                                        taxon = Taxon.createCriteria().get {
                                            maxResults(1)
                                            if (pai) {
                                                eq('pai', pai)
                                            }
                                            eq('nivelTaxonomico', nivelTaxonomico)
                                            //sqlRestriction "upper(no_taxon)='"+nome.toUpperCase()+"'"
                                            or {
                                                eq('noTaxon', nome)
                                                eq('noTaxon', nome.toUpperCase())
                                            }
                                        }

                                        if (taxon) {
                                            println taxon.noCientifico + ' ja cadastrado!'
                                            if (autor && nivelTaxonomico.nuGrauTaxonomico.toInteger() >= grailsApplication.config.grau.especie.toInteger()) {
                                                if ( taxon.noAutor != autor) {
                                                    taxon.noAutor = autor
                                                    taxon.save()
                                                    gravarHistoricoTaxon(taxon, historico + ' Registro já existente, apenas atualizado Autor/ano.', pessoa)
                                                }
                                            }
                                            pai = taxon
                                        }else {
                                            taxon = new Taxon()
                                            if (pai) {
                                                taxon.pai = pai
                                            }
                                            taxon.nivelTaxonomico = nivelTaxonomico
                                            taxon.noTaxon = nome
                                            taxon.stAtivo = true
                                            taxon.inOcorreBrasil = true
                                            taxon.situacaoNome = situacaoNome
                                            if (autor && nivelTaxonomico.nuGrauTaxonomico.toInteger() >= grailsApplication.config.grau.especie.toInteger()) {
                                                taxon.noAutor = autor
                                            }
                                            taxon.validate()
                                            if (taxon.hasErrors()) {
                                                log.error('Erros taxon')
                                                log.error(taxon.getErrors())
                                            }else {
                                                taxon.save()
                                                gravarHistoricoTaxon(taxon, historico, pessoa)
                                                println taxon.noCientifico + ' cadastrado com sucesso!'
                                                //log.info( 'Gravado com sucesso!')
                                                pai = taxon
                                            }
                                        }
                                    }
                                    catch( Exception e)
                                    {
                                        erro = true
                                        println 'Erro get taxon: '+nome
                                        println 'Pai..: ' + ( pai?pai.noTaxon:'')
                                        println 'Nivel: ' + nivelTaxonomico.deNivelTaxonomico
                                        println e.getMessage();
                                        println "#"*100
                                        println ' '
                                        taxon=null
                                    }

                                } else {
                                    println 'Nivel taxonomico ' + nome + ' nao cadastrado!'
                                }
                            }
                        }
                    }
                }
                else if( line.indexOf('nm_') > -1 )
                {
                    List aTemp = []
                    if( line.indexOf(';') >- 0)
                    {
                        delimitador = ';'
                    }
                    if( line.indexOf(',') >- 0)
                    {
                        delimitador = ','
                    }
                    line.split(delimitador).each {
                        String nivel = it.replaceAll(/nm_/,'').toUpperCase().replaceAll(/"/,'')
                        if( niveisValidos.indexOf(nivel) >-1)
                        {
                            aTemp.push( nivel )
                        }
                    }
                    niveisValidos = aTemp
                }
            }
        }
        println 'Fim do Processamento ' + new Date()

        return 'Fim do Processamento ' + new Date()
    }

    void gravarHistoricoTaxon( Taxon taxon, String historico, Pessoa pessoa )
    {
        TaxonHist th = new TaxonHist()
        th.taxon           =taxon
        th.pessoa          =pessoa
        th.nivelTaxonomico =taxon.nivelTaxonomico
        th.situacao        =taxon.situacaoNome
        th.pai             =taxon.pai
        th.noTaxon         =taxon.noTaxon
        th.stAtivo         =taxon.stAtivo
        th.inOcorreBrasil  =taxon.inOcorreBrasil
        th.noAutor         =taxon.noAutor
        th.nuAno           =taxon.nuAno
        th.dtHistorico     = new Date()
        th.deHistorico     = ( historico ?: 'Importação planilha via SALVE.' )
        if( ! th.validate() ) {
            println th.getErrors()
        }
        else {
            log.info('historico gravado com SUCESSO!')
            th.save()
        }


        /*new TaxonHist(
                taxon           :taxon,
                pessoa          :pessoa,
                nivelTaxonomico :taxon.nivelTaxonomico,
                situacaoNome    :taxon.situacaoNome,
                pai             :taxon.pai,
                noTaxon         :taxon.noTaxon,
                stAtivo         :taxon.stAtivo,
                inOcorreBrasil  :taxon.inOcorreBrasil,
                noAutor         :taxon.noAutor,
                nuAno           :taxon.nuAno,
                dtHistorico     : new Date(),
                deHistorico     : ( historico ?: 'Importação planilha via SALVE.' ) ) .save(flush:true)
                 log.info( 'Historico gravado com sucesso!')
                               */
    }

    /**
    *    Verfica se uma espécie ou subespecie existe na tabela taxonomia.taxon
    */
    int taxonExists( String noCientifico = '',boolean caseSensitive = false,String nivel='')
    {
        if( ! noCientifico || noCientifico =='null' )
        {
            return 0
        }
        noCientifico = noCientifico.toString().trim().replaceAll(/[^a-zA-Z-]/,' ').replaceAll('/  /g',' ').trim()
        List aTemp = noCientifico.split(' ')
        if(aTemp.size() < 2  || aTemp.size > 3 || ( nivel.toString().toUpperCase()=='SUBESPECIE' && aTemp.size() != 3 ) )
        {
            return 0
        }
        List taxon = Taxon.createCriteria().list
            {
            order('id', 'desc')
            createAlias('pai', 'txPai')
            if( aTemp.size() == 2 ) // genero e especie
            {
                if( ! caseSensitive ) {
                    aTemp[0] = aTemp[0].trim().toLowerCase().capitalize()
                    aTemp[1] = aTemp[1].trim().toLowerCase()
                }
                and {
                    eq("nivelTaxonomico" , NivelTaxonomico.findByCoNivelTaxonomico('ESPECIE'))
                    eq('txPai.noTaxon'   , aTemp[0].trim() ) // genero
                    eq('noTaxon'      , aTemp[1].trim() ) // especie
                }
            }
            else if( aTemp.size() == 3 ) // subespecie
            {
                createAlias('txPai.pai', 'txAvo')
                and {

                    if( ! caseSensitive ) {
                        aTemp[0] = aTemp[0].toLowerCase().capitalize().trim()
                        aTemp[1] = aTemp[1].toLowerCase()
                        aTemp[2] = aTemp[2].toLowerCase()
                    }
                    eq("nivelTaxonomico", NivelTaxonomico.findByCoNivelTaxonomico('SUBESPECIE'))
                    eq('txAvo.noTaxon'  , aTemp[0] ) // genero
                    eq('txPai.noTaxon'  , aTemp[1] ) // especie
                    eq('noTaxon'        , aTemp[2] ) // subespecie
                }
            }
        }
        if( taxon )
        {
            return ( taxon[0]?.id ?: 0 )
        }
        return 0
    }
}
