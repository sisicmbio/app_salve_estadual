package br.gov.icmbio

//import grails.transaction.Transactional
import java.text.Normalizer
import java.text.SimpleDateFormat;


//@Transactional
class UtilityService {
	static transactional = false; // não abrir uma nova sessão
    private List letras = ["Ç|&Ccedil;","ç|&ccedil;","Á|&Aacute;","Â|&Acirc;","Ã|&Atilde;","É|&Eacute;","Ê|&Ecirc;","Í|&Iacute;","Ô|&Ocirc;","Õ|&Otilde;","Ó|&Oacute;","Ú|&Uacute;","á|&aacute;","â|&acirc;","ã|&atilde;","é|&eacute;","ê|&ecirc;","í|&iacute;","ô|&ocirc;","õ|&otilde;","ó|&oacute;","ú|&uacute;"]

    /**
	 * Método para remover os acentos para caractere sem acento correspondente.
	 * @param  String - palavra com acentos
	 * @return String - palaver sem acentos
	 */
    public String removeAccents(String str)
    {
        return Normalizer.normalize(str, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
    }
    /**
     * Método para validar cpf
     * @param cpf
     * @return
     */
    Boolean isCpf(String cpf)
    {
        if(!cpf )
        {
            return false;
        }

        try
        {
            cpf = cpf.replaceAll(/[^0-9]/,'')
            if( cpf.length() < 11 )
            {
                return false
            }
            return Validador.isValidCPF( cpf )
        } catch (Exception e){}
        return false;
    }

    boolean isDate( String date)
    {
        if( ! date )
        {
            return false;
        }
        try
        {
            if( new Date().parse('dd/MM/yyyy', date ) )
            {
                return true;
            }
        } catch (Exception e)
        {
        }
        return true
    }
    boolean isTime( String time)
    {
        if( ! time )
        {
            return false;
        }
        try
        {
            if( time.length() < 5 )
            {
                time +=':00'
            }
            SimpleDateFormat sdf = new SimpleDateFormat( "HH:mm:ss" );
            sdf.setLenient( false )
            sdf.parse( time );
            return true;
        } catch (Exception e)
        {
        }
        return true
    }

    boolean isDouble( String value )
    {
        if( !value)
        {
            return false;
        }
        try
        {
            value = value.replaceAll(',', '.');
            if( Double.parseDouble( value ) )
            {
                return true;
            }
        } catch (Exception e)
        {
        }
        return false;
    }

    String trimEditor( String texto = "" )
    {
        if( texto )
        {
            return texto.replaceAll(/<p>&nbsp;<\/p>/, '').trim()
        }
        return texto
    }

    String getRandomColor() {
        String[] letters = ["0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F"];
        String color = "#";
        for (int i = 0; i < 6; i++ ) {
            color += letters[ (int) Math.round(Math.random() * 15)];
        }
        return color;
    }

    /**
     * método para converter acentos no formato html ex: ç = &ccedil
     * @param  texto [description]
     * @return       [description]
     */
    String str2html( String texto = "" )
    {
        this.letras.each {
            List a = it.split(/\|/)
            texto = texto.replace(a[0],a[1])
        }
        return texto
    }

    String html2str( String texto = "" )
    {
        this.letras.each {
            List a = it.split(/\|/)
            texto = texto.replace(a[1],a[0])
        }
        return texto
    }

    Date str2Date( String text = '' )
    {
        if( ! text )
        {
            return null
        }
        try {
            text = text.replace(/-/,'/')
            if( text.indexOf('/') == -1 ) {
                return new Date(text.toLong() ).format('dd/MM/yyyy')
            }
            else
            {
                if( text.indexOf('/')==4) {
                    return  new Date().parse("yyyy/MM/dd", text)
                }
                else
                {
                    return  new Date().parse("dd/MM/yyyy", text)
                }
            }
        } catch( Exception e ) {
            println 'UtilityService.str2date('+ text + ') nao e uma data valida!'
        }
        return null
    }

}
