package br.gov.icmbio

import grails.transaction.Transactional

@Transactional
class ConsultaService {

    def sqlService
    def dadosApoioService
    /**
     *
     * Listar as consultas diretas e revisões que o usuário tem acesso
     * @param sqCicloAvaliacao
     * @param isAdm
     * @param sqUnidadeOrg
     * @param sqPessoa
     * @return
     */
    List getConsultasDiretasRevisoesUsuario( Long sqCicloAvaliacao, Sicae user) {
        List listConsultas
        Map pars = [sqCicloAvaliacao:sqCicloAvaliacao]
        if ( ! user.isADM() ) {
            if( user.sqUnidadeOrg ) {
                pars.sqUnidadeOrg = user.sqUnidadeOrg
            } else {
                if( user.isCT() ) {
                    DadosApoio papelCT = dadosApoioService.getByCodigo('TB_PAPEL_FICHA', 'COORDENADOR_TAXON')
                    pars.sqPapel = ( papelCT ? papelCT.id : 0 )
                }
                pars.sqPessoa = user.sqPessoa
            }
        }
        String cmdSql = """select distinct a.sq_ciclo_consulta
                ,a.sg_consulta
                ,a.sq_unidade_org
                ,a.dt_inicio
                ,a.dt_fim
                ,a.de_titulo_consulta
                ,d.sg_unidade_org
                ,b.ds_dados_apoio as ds_tipo_consulta
                ,b.cd_sistema as cd_tipo_consulta_sistema
        from salve.ciclo_consulta a
        inner join salve.dados_apoio as b on b.sq_dados_apoio = a.sq_tipo_consulta
        inner join salve.vw_unidade_org as d on d.sq_pessoa = a.sq_unidade_org
        ${pars.sqPessoa ? 'INNER JOIN salve.ciclo_consulta_ficha e on e.sq_ciclo_consulta = a.sq_ciclo_consulta':''}
        ${pars.sqPessoa ? 'INNER JOIN salve.ficha_pessoa f on f.sq_ficha = e.sq_ficha':''}
        where a.sq_ciclo_avaliacao  = :sqCicloAvaliacao
        and b.cd_sistema in ( 'REVISAO_POS_OFICINA','CONSULTA_DIRETA')
        ${pars.sqUnidadeOrg ? 'and a.sq_unidade_org = :sqUnidadeOrg':''}
        ${pars.sqPapel ? "and f.sq_papel = :sqPapel and f.in_ativo='S'":''}
        order by a.dt_inicio desc, a.sg_consulta
        """
        /** /
         println ' '
         println cmdSql
         println pars
         /**/

        listConsultas = sqlService.execSql(cmdSql,pars)

        /* USANDO GORM
        listConsultas = CicloConsulta.executeQuery("""
        select distinct new map( a.id as id
        ,a.dtInicio as dtInicio
        ,a.dtFim as dtFim
        ,a.deTituloConsulta as deTituloConsulta
        ,d.sgUnidade as sgUnidadeOrg
        ,b.descricao as dsTipoConsulta
        ,b.codigoSistema as codigoSistema)
        from CicloConsulta a
        inner join a.tipoConsulta b
        inner join a.unidade d
        left join a.fichas c
        where a.cicloAvaliacao.id = :sqCicloAvaliacao
        and a.tipoConsulta in (${consultaDireta.id},${consultaRevisao.id} )
        ${parametros.sqUnidadeOrg ? 'and a.unidade.id = :sqUnidadeOrg':''}
        ${parametros.inAtivo ? '\nand c.ficha.id in ( select fp.ficha.id from FichaPessoa fp where fp.pessoa.id = :sqPessoa and fp.papel.id = :sqPapel and fp.inAtivo = :inAtivo )' :''}
        order by a.dtInicio desc
        """, parametros)*/
        return listConsultas
    }

}
