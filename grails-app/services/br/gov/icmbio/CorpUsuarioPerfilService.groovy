package br.gov.icmbio


import grails.transaction.Transactional

@Transactional
class CorpUsuarioPerfilService {

    def sqlService

    /**
     * método para atribuir perfil(is) ao usuário
     * @param sqPessoa
     * @param sqPerfilGeral
     * @param sqPerfilFicha
     * @param sqPerfilInstituicao
     * @param sqInstituicao
     */
    void save(Long sqPessoa = null
              , String sqPerfilGeral = ''
              , String sqPerfilFicha = ''
              , String sqPerfilInstituicao = ''
              , Long sqInstituicao = 0l ) {
       if( !sqPessoa ){
            throw new Exception('Usuário não selecionado')
        }
        if( sqPerfilInstituicao && !sqInstituicao ) {
            throw new Exception('Instituição não selecionada')
        }
        Instituicao instituicao
        if( sqInstituicao ) {
            instituicao = Instituicao.get( sqInstituicao )
            if ( ! instituicao ) {
                throw new Exception('Instituição inexistente')
            }
        }
        Usuario usuario = Usuario.get( sqPessoa )
        List perfisGerais = Perfil.findAllByCdSistemaInList(['ADM','CONSULTA'])
        List perfisFichas = Perfil.findAllByCdSistemaInList(['COLABORADOR','COORDENADOR_TAXON','TRADUTOR','VALIDADOR'])
        List perfisInstituicao = Perfil.findAllByCdSistemaInList(['COLABORADOR','PONTO_FOCAL'])

        if( !usuario ){
            throw new Exception('Usuário não existe')
        }

        // POR UNIDADE - adicionar/remover o perfil de acesso por unidade de lotação
        List perfisSelecionados = sqPerfilInstituicao ? sqPerfilInstituicao.split(',').collect{it.toLong()} : []
        perfisInstituicao.each{perfil ->
            boolean incluir = perfisSelecionados.contains( perfil.id.toLong() )
            UsuarioPerfil usuarioPerfil = UsuarioPerfil.findByUsuarioAndPerfil(usuario,perfil)
            if( ! usuarioPerfil ) {
                if( incluir ) {
                    usuarioPerfil = new UsuarioPerfil(usuario: usuario, perfil: perfil)
                    usuarioPerfil.save(flush:true)
                    new UsuarioPerfilInstituicao(usuarioPerfil:usuarioPerfil,instituicao:instituicao).save()
                }
            } else {
                UsuarioPerfilInstituicao usuarioPerfilInstituicao = UsuarioPerfilInstituicao.findByUsuarioPerfilAndInstituicao(usuarioPerfil,instituicao)
                if( usuarioPerfilInstituicao ) {
                    if ( ! incluir ) {
                        if ( usuarioPerfilInstituicao ) {
                            usuarioPerfilInstituicao.delete()
                            if (perfil.cdSistema == 'PONTO_FOCAL') {
                               if ( ! UsuarioPerfilInstituicao.findByUsuarioPerfilAndInstituicaoNotEqual(usuarioPerfil, instituicao)) {
                                   println '4'
                                    usuarioPerfil.delete()
                                }
                            }
                        }
                    }
                } else {
                    if ( incluir ) {
                        new UsuarioPerfilInstituicao(usuarioPerfil: usuarioPerfil, instituicao: instituicao).save()
                    }
                }
            }
        }

        // GERAL - adicionar/remover o perfil geral do usuario
        perfisSelecionados = sqPerfilGeral ? sqPerfilGeral.split(',').collect{it.toLong()} : []
        perfisGerais.each{perfil ->
            boolean incluir = perfisSelecionados.contains( perfil.id.toLong() )
            UsuarioPerfil usuarioPerfil = UsuarioPerfil.findByUsuarioAndPerfil(usuario,perfil)
            if( ! usuarioPerfil ) {
                if( incluir ) {
                    usuarioPerfil = new UsuarioPerfil(usuario: usuario, perfil: perfil)
                    usuarioPerfil.save()
                }
            } else {
                if( ! incluir ) {
                    usuarioPerfil.delete()
                }
            }
        }
        // POR FICHA - adicionar/remover o perfil de acesso a fichas especificas
        perfisSelecionados = sqPerfilFicha ? sqPerfilFicha.split(',').collect{it.toLong()} : []
        perfisFichas.each{perfil ->
            boolean incluir = perfisSelecionados.contains( perfil.id.toLong() )
            UsuarioPerfil usuarioPerfil = UsuarioPerfil.findByUsuarioAndPerfil(usuario,perfil)
            if( ! usuarioPerfil ) {
                if( incluir ) {
                    usuarioPerfil = new UsuarioPerfil(usuario: usuario, perfil: perfil)
                    usuarioPerfil.save()
                }
            } else {
                if( ! incluir ) {
                    // o perfil colaborador serve tanto para interno quando para externo
                    // então para deletar o perfil COLABORADOR externo verificar se ele
                    // não está sendo utilizado como interno
                    UsuarioPerfilInstituicao usuarioPerfilInstituicao = UsuarioPerfilInstituicao.findByUsuarioPerfil( usuarioPerfil )
                    if( ! usuarioPerfilInstituicao ) {
                        usuarioPerfil.delete()
                    }
                }
            }
        }

        return
    }


    /**
     * método para retornas os perfis que o usuario tem na instituicao
     * @param sqPessoa
     * @return
     */
    List edit(Long sqPessoa = 0l, Long sqInstituiacao = 0l) {
        if( !sqPessoa ){
            throw new Exception('Usuário não selecionado')
        }
        if( !sqInstituiacao ){
            throw new Exception('Instituição não selecionado')
        }
        Usuario usuario  = Usuario.get( sqPessoa )
        Instituicao instituicao = Instituicao.get( sqInstituiacao )
        return UsuarioPerfilInstituicao.createCriteria().list {
            usuarioPerfil {
                eq('usuario',usuario)
            }
            eq('instituicao', instituicao)
        }?.usuarioPerfil?.perfil?.id
    }


    /**
     * método para excluir o perfil do usuário
     * @return
     */
    def delete(Long sqUsuarioPerfilInstituicao = 0l) {
        if (!sqUsuarioPerfilInstituicao) {
            throw new Exception('Id não informado')
        }
        UsuarioPerfilInstituicao usuarioPerfilInstituicao = UsuarioPerfilInstituicao.get(sqUsuarioPerfilInstituicao)
        if( !usuarioPerfilInstituicao ){
            throw new Exception('Id inexistente')
        }
        try {
            usuarioPerfilInstituicao.delete()
            return [sqUsuarioPerfilInstituicao: usuarioPerfilInstituicao.id
                    , sgInstituicao: usuarioPerfilInstituicao.instituicao.sgInstituicao
                    , noPerfil: usuarioPerfilInstituicao.usuarioPerfil.perfil.noPerfil]
        } catch( Exception e ){
            Util.printLog('SALVE - CorpUsuarioPerfilService.delete()','Erro ao exlcluir o perfil do usuário ',"Erro delete")
            println usuarioPerfilInstituicao.getErrors()
            throw new Exception(e.getMessage())
        }
    }
    /**
     * retornar a lista de perfils por instituicao do usuario
     * @param sqPessoa
     * @return
     */
    List listPerfilInstituicao( Long sqPessoa = 0 ) {
        if( !sqPessoa ){
            throw  new Exception('Id usuario não informado')
        }
        Usuario usuario = Usuario.get( sqPessoa )
        if( !usuario ){
            throw  new Exception('Id inexistente')
        }
        return usuario.perfilPorInstituicao
    }


    /**
     * criar listagem dos perfis do usuário
     * @param page
     * @param pageSize
     * @param q
     * @return
     */
    List list( Integer page=1, Integer pageSize=100, String q = '') {
        return []

        /*
        Integer skipRecords = ( pageSize * ( page - 1 ) )
        return Usuario.createCriteria().list {
            if( q ){
                pessoaFisica {
                    pessoa {
                        ilike('noPessoa', '%' + q.trim() + '%')
                    }
                }
            }
            maxResults( pageSize )
            firstResult( skipRecords )
            //order("pessoaFisica.pessoa.noPessoa", "asc")
        }*/
    }
}



