package br.gov.icmbio

import grails.converters.JSON
import grails.transaction.Transactional

@Transactional
class TraducaoService {

    def sqlService
    def requestService

    /**
     * método para retornar os dados para montagem do gride de textos fixos
     * @param params
     * @return
     */
    Map getDataGridTraducaoTextoFixo(Map params = [:] ) {
        String sqlOrderBy
        List sqlWhere = []

        Map sqlParams = [:]

        if( ! params.sortColumns ){
            params.sortColunms= 'tx_original'
        }
        if( params.txOriginal ) {
            sqlWhere.push('tx_original ilike :txOriginal')
            sqlParams.txOriginal = '%'+params.txOriginal+'%'
        }
        if( params.txTraduzido ) {
            sqlWhere.push('tx_traduzido ilike :txTraduzido')
            sqlParams.txTraduzido = '%'+params.txTraduzido+'%'
        }
        if( params.txRevisado ) {
            sqlWhere.push('tx_revisado ilike :txRevisado')
            sqlParams.txRevisado = '%'+params.txRevisado+'%'
        }
        List sortColumns = params.sortColumns.split(':')
        sqlOrderBy = '\nORDER BY '+sortColumns[0] + ( sortColumns[1] ? ' '+sortColumns[1] : '')

        Map rows = sqlService.paginate("""select * from salve.traducao_texto
            ${sqlWhere ? '\nwhere ' + sqlWhere.join(' and ') :''}
            ${sqlOrderBy}
        """,sqlParams, params,100,10,0)

        return  rows
    }

    /**
     * método para recuperar um registro para edição
     * @param params
     * @return
     */
    Map editTraducaoTextoFixo( Map params = [:] ) {
        Map res = [status: 0, msg: '', type: 'success', data: [:]]
        try {
            if (!params.sqTraducaoTexto) {
                throw new Exception('Id não informado.')
            }
            // verificar se o texto original já existe e gravar como alteracao
            List rows = sqlService.execSql("select * from salve.traducao_texto where sq_traducao_texto  = :sqTraducaoTexto limit 1",
                [sqTraducaoTexto: params.sqTraducaoTexto.toLong()])
            if (!rows) {
                throw new Exception('Id inexistente.')
            }
            res.data = rows[0]
        } catch (Exception e) {
            res.msg = e.getMessage()
            res.type = 'error'
            res.status = 1
        }
        return res
    }

    /**
     * método para fazer a exclusão física de um registro da tabela traducao_texto
     * @param params
     * @return
     */
    Map deleteTraducaoTextoFixo(Map params = [:] ) {
        Map res = [status:0,msg:'',type:'success',data:[:] ]
        try{
            if( ! params.sqTraducaoTexto ){
                throw new Exception('Id não informado.')
            }

            // verificar se o texto original já existe e gravar como alteracao
            List rows = sqlService.execSql("""select * from salve.traducao_texto
                 where sq_traducao_texto = :sqTraducaoTexto""",
                [sqTraducaoTexto: params.sqTraducaoTexto.toLong() ])

            if( ! rows ){
                throw new Exception('Id inexistente.')
            }
            sqlService.execSql("""delete from salve.traducao_texto
                    where sq_traducao_texto = :sqTraducaoTexto""",[sqTraducaoTexto:params.sqTraducaoTexto.toLong()])
            if( sqlService.lastError ){
                throw new Exception( sqlService.lastError )
            }
        } catch( Exception e ){
            res.status=1
            res.type='error'
            res.msg = e.getMessage()
        }
        return res
    }

    /**
     * Método para persistência ou atualização da tradução dos textos fixos
     * na tabela traducaoTexto
     * @param params
     * @return
     */
    Map saveTraducaoTextoFixo(Map params = [:] ){
        Map res = [ status:0, msg:'Dados gravados com SUCESSO.', type:'success', data: [:] ]
        try {
            // validar campos obrigatórios
            if( ! params.txOriginal ){
                throw new Exception('Texto original deve ser informado.')
            }
            if( ! params.txTraduzido ){
                throw new Exception('Texto traduzido deve ser informado.')
            }

            if( ! params.txRevisado ){
                throw new Exception('Tradução corrigida deve ser informada.')
            }

            // fazer a limpeza de tags de formatação de estilo css desnecessárias
            params.txTraduzido = Util.stripTagsTraducao(params.txTraduzido)
            params.txRevisado = Util.stripTagsTraducao(params.txRevisado)

            if( params.noTabelaTraducao.toString().toLowerCase() == 'apoio' ) {
                res = saveTraducaoTabelaApoio(params)
            } else if( params.noTabelaTraducao.toString().toLowerCase() == 'uso' ) {
               res = saveTraducaoUso( params )
            } else if( params.noTabelaTraducao.toString().toLowerCase() == 'pan' ) {
                res = saveTraducaoPan( params )
            } else {
                if (params.sqTraducaoTexto) {
                    // verificar se já existe
                    if (sqlService.execSql("""select sq_traducao_texto
                         from salve.traducao_texto
                         where tx_original = :txOriginal
                           and sq_traducao_texto <> :sqTraducaoTexto limit 1""",
                        [txOriginal: params.txOriginal, sqTraducaoTexto: params.sqTraducaoTexto.toLong()])) {
                        throw new Exception('Tradução já cadastrada.')
                    }
                    sqlService.execSql("""update salve.traducao_texto set tx_original = :txOriginal, tx_traduzido = :txTraduzido
                        , tx_revisado = :txRevisado
                        where sq_traducao_texto = :sqTraducaoTexto""",
                        [txOriginal: params.txOriginal, txTraduzido: params.txTraduzido
                            , txRevisado: params.txRevisado
                            , sqTraducaoTexto: params.sqTraducaoTexto.toLong()])
                } else {
                    // verificar se ja existe
                    List rows = sqlService.execSql("""select sq_traducao_texto
                         from salve.traducao_texto
                         where tx_original = :txOriginal limit 1""",
                        [txOriginal: params.txOriginal])
                    if ( rows ) {
                        sqlService.execSql("""update salve.traducao_texto set tx_traduzido = :txTraduzido
                            ,tx_revisado = :txRevisado
                            where sq_traducao_texto = :sqTraducaoTexto""",
                            [ txTraduzido: params.txTraduzido,txRevisado: params.txRevisado
                                 , sqTraducaoTexto: rows[0].sq_traducao_texto.toLong()])
                        // throw new Exception('Tradução já cadastrada.')
                    } else {
                        sqlService.execSql("""insert into salve.traducao_texto ( tx_original, tx_traduzido, tx_revisado)
                            values ( :txOriginal, :txTraduzido, :txRevisado)""",
                            [txOriginal: params.txOriginal, txTraduzido: params.txTraduzido
                             ,txRevisado: params.txRevisado ] )
                    }
                }
            }
            if( sqlService.lastError ){
                throw new Exception( sqlService.lastError )
            }
        } catch( Exception e ){
            res.msg     = e.getMessage()
            res.type    = 'error'
            res.status  = 1
        }
        return res
    }

    /**
     * Método para fazer a inclusão/alteracao da traducao da tabela de apoio (dados_apoio)
     * @param params
     * @return
     */
    Map saveTraducaoTabelaApoio( Map params = [:] ) {
        Map res = [status: 0, msg: '', type: 'success', data: [:] ]
        try {
            if( ! params.idRegistroTraducao ) {
                throw new Exception( 'Id do registro não informado.' )
            }
            if( ! params.txTraduzido ) {
                throw new Exception( 'Texto traduzido não informado.' )
            }
            if( ! params.txRevisado ) {
                throw new Exception( 'Tradução corrigida não informada.' )
            }
            // fazer a limpeza de tags de formatação de estilo css desnecessárias
            params.txTraduzido = Util.stripTagsTraducao(params.txTraduzido)
            params.txRevisado = Util.stripTagsTraducao(params.txRevisado)

            DadosApoio dadosApoio = DadosApoio.get( params.idRegistroTraducao.toLong() );
            if( dadosApoio ) {
                if( dadosApoio.traducaoDadosApoio[0] ) {
                    dadosApoio.traducaoDadosApoio[0].txTraduzido = params.txTraduzido
                    dadosApoio.traducaoDadosApoio[0].txRevisado = params.txRevisado
                    dadosApoio.traducaoDadosApoio[0].save()
                } else {
                    new TraducaoDadosApoio(dadosApoio:dadosApoio, txTraduzido: params.txTraduzido, txRevisado: params.txRevisado).save();
                }
            }
        }
        catch (Exception e) {
            res.msg = e.getMessage()
            res.type = 'error'
            res.status = 1
        }
        return res
    }

    /**
     * Método para fazer a inclusão/alteracao da traducao da tabela de usos
     * @param params
     * @return
     */
    Map saveTraducaoUso( Map params = [:] ) {
        Map res = [status: 0, msg: '', type: 'success', data: [:] ]
        try {
            if( ! params.idRegistroTraducao ) {
                throw new Exception( 'Id do registro não informado.' )
            }
            if( ! params.txTraduzido ) {
                throw new Exception( 'Texto traduzido não informado.' )
            }
            if( ! params.txRevisado ) {
                throw new Exception( 'Tradução corrigida não informada.' )
            }
            // fazer a limpeza de tags de formatação de estilo css desnecessárias
            params.txTraduzido = Util.stripTagsTraducao(params.txTraduzido)
            params.txRevisado = Util.stripTagsTraducao(params.txRevisado)
            Uso uso = Uso.get( params.idRegistroTraducao.toLong() )
            if( !uso ){
                throw new Exception( 'Id do uso inexistente.' )
            }
            TraducaoUso traducaoUso = TraducaoUso.findByUso( uso )
            if( traducaoUso ) {
                traducaoUso.txTraduzido = params.txTraduzido
                traducaoUso.txRevisado = params.txRevisado
                traducaoUso.save()
            } else {
                new TraducaoUso(uso: uso, txTraduzido: params.txTraduzido, txRevisado: params.txRevisado).save();
            }
        }
        catch (Exception e) {
            res.msg = e.getMessage()
            res.type = 'error'
            res.status = 1
        }
        return res
    }

    /**
     * Método para fazer a inclusão/alteracao da traducao da tabela de PANS
     * @param params
     * @return
     */
    Map saveTraducaoPan( Map params = [:] ) {
        Map res = [status: 0, msg: '', type: 'success', data: [:] ]
        try {
            if( ! params.idRegistroTraducao ) {
                throw new Exception( 'Id do registro não informado.' )
            }
            if( ! params.txTraduzido ) {
                throw new Exception( 'Texto traduzido não informado.' )
            }
            if( ! params.txRevisado ) {
                throw new Exception( 'Tradução corrigida não informada.' )
            }
            // fazer a limpeza de tags de formatação de estilo css desnecessárias
            params.txTraduzido = Util.stripTagsTraducao(params.txTraduzido)
            params.txRevisado = Util.stripTagsTraducao(params.txRevisado)
            PlanoAcao pan = PlanoAcao.get( params.idRegistroTraducao.toLong() )
            if( !pan){
                    throw new Exception( 'Id do PAN inexistente.' )
            }
            TraducaoPan traducaoPan = TraducaoPan.findByPlanoAcao( pan )
            if( traducaoPan ) {
                traducaoPan.txTraduzido = params.txTraduzido
                traducaoPan.txRevisado = params.txRevisado
                traducaoPan.save()
            } else {
                new TraducaoPan(planoAcao: pan, txTraduzido: params.txTraduzido, txRevisado: params.txRevisado).save();
            }
        }
        catch (Exception e) {
            res.msg = e.getMessage()
            res.type = 'error'
            res.status = 1
        }
        return res
    }

    /**
     * Método para fazer a tradução de textos ou palavras avulsas
     * Primeiro tenta o servico Libretranslate do ICMBio e se falhar
     * tenta utilizar o google translate
     * @param params
     * @return
     */
    Map traduzir( Map params = [:]) {
        Map res = [status: 0, msg: '', data:[traducao: '']]
        try {
            if ( params.texto ) {
                res.data.traducao = requestService.doTranslate(params.texto, 'libre')
            }
        } catch (Exception e) {
            res.msg = e.getMessage()
            res.status = 1
        }
        return res
    }
}
