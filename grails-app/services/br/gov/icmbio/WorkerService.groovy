package br.gov.icmbio

import org.codehaus.groovy.grails.web.servlet.mvc.GrailsHttpSession

//import grails.converters.JSON
//import org.codehaus.groovy.grails.plugins.DomainClassGrailsPlugin
//import grails.transaction.Transactional

//@Transactional
class WorkerService {

    def userJobService
    /**
     * registra na sessão a atividade que ficara executando em segundo plano
     * @param deRotulo - Imprimindo relatório x
     * @param vlMax - 1500
     * @param deCallback - ficha.updateGrid ou "window.open('xxxxx');"
     * @param idUpdatePercentual - id da tag html para informar o percentual executado
     * @return Worker
     */
    Map add(GrailsHttpSession selfSession, String deRotulo, Integer vlMax = 0i, String deCallback = '', String idUpdatePercentual = '') {
        if (!selfSession.sicae.user.sqPessoa) {
            return [:]
        }
        Map w = [:]
        w.sqPessoa = selfSession.sicae.user.sqPessoa
        w.id = w.sqPessoa.toString() + '-' + new Date().format('yyyyMMddHHmmssSSS')
        w.deRotulo = deRotulo
        w.vlMax = vlMax.toInteger()
        w.vlAtual = 0d
        w.vlControle = -1d // se em 1 hora não for alterado o worker será encerrado com erro
        w.dePercentual = '0%'
        w.deCallback = deCallback
        w.txError = ''
        w.txMensagem = ''
        w.dtInicio = null
        w.dtFim = null
        w.dtLastUpdate = new Date() // controlar se ainda está executando
        w.txResultado = ''
        w.idUpdatePercentual = idUpdatePercentual
        w.deTitle = '' // mensagem complementar atualizada dinamicamente durante o processamento
        sleep(100)

        if (!selfSession.workers) {
            //println 'Iniciado variavel workers na sessao!'
            selfSession.workers = [:]
        }
        selfSession.workers[w.id] = w
        return w
    }

    /**
     * atualiza a data de inicio da atividade
     * @param w
     */
    void start(Map w = [:]) {
        if (w.id) {
            w.dtFim = null
            w.dtInicio = new Date()
            w.dtLastUpdate = w.dtInicio
            //println "Worker start"
        }
    }

    /**
     * grava o erro ocorrido durante o processamento e encerra o worker
     * @param w
     */
    void setMax(Map w = [:], Integer max = 0i) {
        if (w.id) {
            w.vlMax = max
            //println "Worker setMax "+ max
        }
    }

    int getMax(Map w = [:]) {
        if (w.id) {
            return (int) w.max
        }
        return 0i
    }

    /**
     * atualizar o title
     * @param w
     */
    void setTitle(Map w = [:], String msg = '') {
        if (w.id) {
            w.deTitle = msg
        }
    }

    /**
     * grava o erro ocorrido durante o processamento e encerra o worker
     * @param w
     */
    void setError(Map w = [:], String error) {
        if (w.id) {
            w.dtFim = new Date() // encerra a atividade
            w.deCallback = ''
            w.txError = error
            w.dtLastUpdate = new Date()
        }
    }

    /**
     * atualiza a mensagem e encerra o worker
     * @param w
     */
    void setMessage(Map w = [:], String message) {
        if (w.id) {
            w.dtFim = new Date() // encerra a atividade
            w.txMensagem = message
            w.dtLastUpdate = new Date()
        }

    }

    /**
     * gravar a função javascript de callback
     * @param w
     * @param callback
     */
    void setCallback(Map w = [:], String callback = '') {
        if (w.id) {
            w.deCallback = callback
            w.dtLastUpdate = new Date()
        }
    }


    /**
     * incrementa o andamento da atividade
     * @param w
     */
    void stepIt(Map w = [:]) {
        /** /
         println ' '
         println 'Worker stepIt.'
         println w
         /**/
        if (w.id) {
            if (!w.dtFim) {
                w.dtLastUpdate = new Date()
                w.vlAtual = w.vlAtual + 1
                if (!w.dtInicio) {
                    w.dtInicio = new Date()
                }
                if (w.vlAtual >= w.vlMax) {
                    w.vlAtual = w.vlMax
                    w.dePercentual = '100%'
                    w.dtFim = new Date()
                    //println 'worker ' + w.id + ' finalizado'
                    //println ' '
                } else {
                    if (w.vlMax > 0) {
                        w.dePercentual = Util.formatNumber(w.vlAtual / w.vlMax * 100, '#0.0') + '%'
                    }
                }
            }
        }
    }

    /**
     * método chamado via ajax para rotornar da sessão, a lista de workers do usuário
     * @param id
     * @return
     */
    List getWorkers(GrailsHttpSession selfSession) {
        List workers = []
        if ( ! selfSession.sicae.user.sqPessoa ) {
            return workers
        }
        if ( selfSession.workers ) {
            workers = selfSession.workers.findAll {
                int idadeHoras = 0i
                if (it.value.dtInicio) {
                    idadeHoras = Util.dateDiff(it.value.dtInicio, new Date(), 'H')
                }
                it.value.type = 'session'
                /** /
                 println 'Idade worker '+idadeHoras
                 println 'Value ' + it.value
                 println "-"*100
                 println ' '
                 /**/

                if (idadeHoras > 1) {
                    if (it.value?.vlAtual == null || it.value.vlControle == it.value.vlAtual) {
                        if (it.value.dePercentual != '100%') {
                            println 'Erro execucao do worker em ' + new Date()
                            println 'Usuario...: ' + selfSession.sicae.user.noPessoa
                            println 'Parametros:'
                            it.value.each {
                                println it.key + '=' + it.value
                            }
                            println ' '
                            setError(it.value, 'Erro no processamento da tarefa ' + it.value.deRotulo)
                        }
                    }
                    it.value.vlControle = it.value.vlAtual
                }

                if (it.value?.vlMax || it.value?.txMensagem || it.value?.txError) {
                    it.value.sqPessoa == selfSession.sicae.user.sqPessoa
                }
            }.collect { it.value }
        }
        List userJobs = userJobService.getUserJobsAsList( selfSession.sicae.user.sqPessoa )?.findAll {
            return !it.stExecutado && !it.stCancelado && !it.deErro
        }
        if( userJobs ) {
            /*
            // disparar o envio do e-mail
            userJobs.each { job ->
                if( Math.round(job.vlPercentual) >= 100 && job.deEmail && job.noArquivoAnexo ){
                    println 'Enviar email ' + job.deEmail
                    userJobService.enviarEmail( job.id.toLong() )
                }
            }*/
            // adicionar os jobs no array
            workers += userJobs
        }
        return workers
    }

    /**
     * excluir atividade da lista
     * @param id
     * @return
     */
    Map delete(String id, GrailsHttpSession selfSession )    {
        Map res = [status:0,type:'success',msg:'']
        try {
            if( selfSession?.workers?.find { it ->
                return it.key==id
            }) {
                selfSession.workers.remove(id)
                res.msg='Tarefa finalizada com SUCESSO!'
            } else {
                res.status=1
                res.type='error'
                res.msg = 'Tarefa n° ' + id + ' inexistente'
            }
        }
        catch ( Exception e ) {
            res.msg = e.getMessage()
            res.type = 'error'
            res.status = 1
        }
        return res
    }

    /**
     * excluir todas as atividade da lista
     * @param selfSession
     * @return
     */
    Map deleteAll(GrailsHttpSession selfSession )    {
        Map res = [status:0,type:'success',msg:'']
        try {
            selfSession.workers = [:]
            res.msg='Tarefas removidas com SUCESSO!'
        } catch ( Exception e ) {
            res.msg = e.getMessage()
            res.type = 'error'
            res.status = 1
        }
        return res
    }

    /**
     * calcular o percentual executado do worker
      * @param worker
     * @return
     */
    String percentual( Map w = [:] )
    {
        if( w.vlMax )
        {
            return Util.formatNumber(w.vlAtual / w.vlMax * 100,'#0.0')+'%'
        }
        return ''
    }


    /**
     * rotorna da sessão, a lista de workers do usuário
     * @param id
     * @return
     */
    void clearWorkers( GrailsHttpSession selfSession) {
        List othersWorker = []
        if(! selfSession.sicae.user.sqPessoa)
        {
            return
        }
        if( ! selfSession.workers )
        {
            return
        }
        selfSession.workers = selfSession.workers.findAll {
            it.value.sqPessoa != selfSession.sicae.user.sqPessoa
        }
        /*
        selfSession.workers.each {
            if( it.value.sqPessoa != selfSession.sicae.user.sqPessoa )
            {
                othersWorker.push( it.value)
            }
        }
        selfSession.workers = othersWorker
        */
        return
    }
}
