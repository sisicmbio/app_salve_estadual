package br.gov.icmbio

import org.codehaus.groovy.grails.web.servlet.mvc.GrailsHttpSession
import com.vividsolutions.jts.geom.Coordinate
import com.vividsolutions.jts.geom.Geometry
import com.vividsolutions.jts.geom.GeometryFactory
// import grails.transaction.Transactional
import grails.util.Environment
import org.codehaus.groovy.grails.web.servlet.mvc.GrailsParameterMap
import org.springframework.web.context.request.RequestContextHolder
import org.springframework.context.ApplicationContext
import grails.util.Holders
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream
import grails.converters.JSON
import org.codehaus.groovy.grails.web.json.JSONObject

/**
 * Este SERVICE tem o objetivo de executar tarefas em segundo plano em conjunto com a servico workService.
 * Funcionamento: Assim que a tarefa é executada deve ser registrado um worker com a ação a ser
 * realizada. Quando for atingido 100% e durante a execução da tarefa ela fica atualizando o andamento
 * do worker
 */
// @Transactional
class AsyncService {
    static transactional = false
    def workerService
    def requestService
    def dadosApoioService
    def sqlService
    def geoService

    // dados da sessão ativa
    private GrailsHttpSession appSession

    private void getSession() {
        this.appSession = RequestContextHolder.currentRequestAttributes().getSession()
    }

    private boolean isDesenv() {
        return (Environment.current == Environment.DEVELOPMENT)
    }

    /**
     * metodo para criar log de depuracao
     * @param text
     */
    private void d( String text ){
        if( isDesenv() ) {
            if( text.trim() != '' ) {
                String h = new Date().format('HH:mm:ss')
                println h + ':' + text
            } else {
                println ' '
            }
        }
    }

    def atualizarEstadoBiomaUcFicha( Map data =[:], String jsCallback='', Boolean saveUserLog = true, String nmCientifico ='') {
        // NÃO ESTA SENDO UTILIZADO MAIS
        /*
        getSession()
        // iniciar a tarefa em segundo plano
        runAsync {
            _asyncAtualizarEstadoBiomaUcFicha( data, jsCallback, appSession, saveUserLog  )
        }*/
    }


    /**
     * metodo assyncrono para atualizar as colunas sq_estado, sq_municipio, sq_bioma, sq_uc_federal,
     * sq_uc_estadual e sq_rppn das tabelas ficha_ocorrencia ou ficha_ocorrencia_portalbio dependendo
     * do banco de dados do ponto
     * O parâmetro data deve ter um objeto map com as seguintes informações:
     * [id_ficha] [
     *      [ idOcorrencia:85044 - id da tabela ficha_ocorrencia ou ficha_ocorrencia_portalbio
     *      , bd:'portalbio'       - sigla do banco de dados SALVE ou PORTALBIO
     *      , situacao:'S'         - situção do registro S/N/E/R (sim/nao/excluido/restaurado)
     *      , ponto:geometry     - campo geometry do ponto ]
     *      ],
     *
     * @param data
     * @param jsCallBack
     */
    private void _asyncAtualizarEstadoBiomaUcFicha( Map data = [:],String jsCallBack='', GrailsHttpSession currentSession = null, Boolean saveUserLog = true ) {
        return // ROTINA NÃO UTILIZADA MAIS
        List erros = []
        List scriptsSql = []
        String cmdSql
        Ficha ficha
        VwFicha vwFicha
        FichaService fichaService = new FichaService(currentSession)
        fichaService.setDebug( isDesenv() )
        //Pais pais = Pais.findByNoPaisIlike('Brasil')
        Estado estado
        Municipio municipio
        Bioma bioma
        DadosApoio bacia
        DadosApoio biomaApoio
        DadosApoio contextoEstado       = dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA', 'ESTADO')
        DadosApoio contextoBioma        = dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA', 'BIOMA')
        DadosApoio contextoBacia        = dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA', 'BACIA_HIDROGRAFICA')
        DadosApoio contextoOcorrencia   = dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA', 'REGISTRO_OCORRENCIA')
        //DadosApoio tabelaBiomas         = DadosApoio.findByCodigoSistema('TB_BIOMA')

        // quando a ref aproximacao for ESTADO e met. Aproximação for CENTROIDE não tem o municipio, uc e bioma a ocorrencia
        //DadosApoio refAproxEstado       = dadosApoioService.getByCodigo('TB_REFERENCIA_APROXIMACAO', 'ESTADO')
        //DadosApoio metAproxCentroide    = dadosApoioService.getByCodigo('TB_METODO_APROXIMACAO', 'CENTROIDE')

        Uc uc
        Map worker
        Map estadosBiomasInsertedDeleted = [:]
        boolean isCentroideEstado=false
        boolean isRegistroSalve=true
        String table
        String tableColumnId
        Map pontoJaProcessado // evitar repetir a procura por estado, municipio e bioma se algum outro ponto ja tiver sido preenchido

        // adicionar as referencias bibliograficas dos pontos aos estados e biomas
        Map refBibsFinal=[:]

        /** /
         d('inicio atualizacao Estado, municipio, bioma e ucs da ficha')
         d(' - DATA');
         println data
        /**/

        try {
            // contar os pontos
            int totalPontos = 0
            data.eachWithIndex { sqFicha, listPontos, index ->
                totalPontos += listPontos.size();
            }
            // registrar na sessão o worker do processo
            /**/
            worker = workerService.add( currentSession
                    , 'Atualizando Estado, município, bioma, bacia e UC(s) da ficha ' + ( data?.nmCientifico?:'')+'\n'+totalPontos+' ponto(s)'
                    , totalPontos
                    , jsCallBack
            )
            // inicializar as variaveis do worker
            workerService.start( worker )
            /**/

            data.eachWithIndex { sqFicha, listPontos, index ->
                vwFicha = VwFicha.get( sqFicha.toLong() );
                // verificar se a ficha pode ser alterada pelo usuário
                if( ! fichaService.canModify( vwFicha.id.toLong() ) ) {
                    throw new Exception('Ficha ' + vwFicha.nmCientifico + ' não pode ser alterada.')
                }
                workerService.setTitle( worker, vwFicha.nmCientifico )
                // inicializar o array de ref bibs para a ficha
                if( ! refBibsFinal[ sqFicha.toString() ] ) {
                    refBibsFinal[ sqFicha.toString() ] = [:]
                    refBibsFinal[ sqFicha.toString() ]['estados'] = [:]
                    refBibsFinal[ sqFicha.toString() ]['biomas']  = [:]
                    refBibsFinal[ sqFicha.toString() ]['bacias']  = [:]
                    refBibsFinal[ sqFicha.toString() ]['ucs']  = [:]
                }
                // 1 - se o idOcorrencia existir atualizar sqEstado, sqMunicipio, sqBioma sqUcEstadual, sqUcFederal, sqRppn se estiverem vazios
                // processar a lista de pontos
                listPontos.each { pointData ->
                    // Estrutura do pointData [ idOcorrencia:fop.id, bd:item.bd, situacao:situacao, ponto:fop.geometry]

                    // limpar variaveis
                    estado = null
                    municipio = null
                    bioma = null
                    bacia = null
                    biomaApoio = null
                    uc = null

                    // TODO - VERIFICAR SE TEM COMO UTILIZAR ESTAS UCSNOPONTO JA CALCULADOS NO LOOP DA LINHA 450
                    // relação de UCS que o ponto atinge
                    Map ucsNoPonto = geoService.getUcsByCoord( pointData.ponto.y, pointData.ponto.x )
                    /*
                    * [ FEDERAIS:[ [id:3404, sigla:APA Planalto Central, nome:Área de Proteção Ambiental do Planalto Central] ], ESTADUAIS:[[id:6, sigla:APA DE CAFURINGA, nome:APA DE CAFURINGA]], RPPNS:[]]
                    *
                    * */
                    if( pointData.isCentroideEstado ) {
                        if( !pointData.sqBioma ) {
                            DadosApoio biomaSalve = geoService.getBiomaSalveByCoord( pointData.ponto.y, pointData.ponto.x )
                            pointData.sqBioma = biomaSalve?.id.toLong()
                            /*if( pointData.sqEstado ) {
                                Municipio municipioTemp = geoService.getMunicipioByCoord( pointData.ponto.y, pointData.ponto.x, Estado.get( pointData.sqEstado )  )
                                pointData.sqMunicipio = municipioTemp?.id?.toLong()
                            }*/
                        }
                    }

                    if( pointData.bd.toLowerCase() == 'portalbio' ) {
                        isRegistroSalve = false
                        table = 'salve.ficha_ocorrencia_portalbio'
                        tableColumnId = 'sq_ficha_ocorrencia_portalbio'
                    }
                    else {
                        isRegistroSalve = true
                        table = 'salve.ficha_ocorrencia';
                        tableColumnId = 'sq_ficha_ocorrencia';
                    }

                    // referencia bibliográfica do ponto
                    List rowRefBibPonto = pointData.rowRefBibPonto

                    if( !estadosBiomasInsertedDeleted[ sqFicha.toString() ] ) {
                        estadosBiomasInsertedDeleted[ sqFicha.toString() ] = [ //estados: [inserted: [ [ ['20':[ refBibs:[1,2,3] ] ] ] ], deleted: [] ],
                                                                               estados: [ inserted: [ ], deleted: [ ] ],
                                                                               biomas : [ inserted: [ ], deleted: [ ] ],
                                                                               bacias : [ inserted: [ ], deleted: [ ] ],
                                                                               ucs    : [ inserted: [ ], deleted: [ ], idsDeleted: [ ] ] ]
                    }

                    // se o ponto do SALVE foi excluido, excluir Estado, bioma, bacia e UC se nenhum outro ponto tiver nestas localizações
                    if( pointData.situacao == 'E' && pointData.bd.toLowerCase() == 'salve' ) {
                        /*[idOcorrencia:902301, bd:salve, situacao:E, ponto:[x:-40.12105181, y:-12.16285432], sqEstado:9, sqBioma:21, sqUcFederal:null, sqUcEstadual:null, sqRppn:null]*/

                        // agendar a exclusão do Estado se não tiver outro ponto no mesmo Estado
                        if( pointData.sqEstado ) {
                            cmdSql = """select sq_ficha_ocorrencia
                                from salve.ficha_ocorrencia
                                where sq_ficha = ${ sqFicha }
                                  and sq_ficha_ocorrencia != ${ pointData.idOcorrencia.toString() }
                                  and sq_contexto = 420
                                  and sq_estado=${ pointData.sqEstado.toString() }
                                  and ge_ocorrencia is NOT null
                                  and (in_utilizado_avaliacao = 'S' or in_utilizado_avaliacao is null ) limit 1;"""
                            List row = sqlService.execSql( cmdSql )
                            if( !row ) {
                                // verificar se tem alguma ocorrencia do PORTALBIO no mesmo estado e que esteja utilizado na avaliacao
                                cmdSql = """select sq_ficha_ocorrencia_portalbio
                                from salve.ficha_ocorrencia_portalbio
                                where sq_ficha = ${ sqFicha }
                                  and sq_estado = ${ pointData.sqEstado.toString() }
                                  and (in_utilizado_avaliacao = 'S' or in_utilizado_avaliacao is null ) limit 1;"""
                                row = sqlService.execSql( cmdSql )
                                if( !row ) {
                                    // agendar a exclusão do estado
                                    String situacao = 'deleted'
                                    if( !estadosBiomasInsertedDeleted[ sqFicha.toString() ].estados[ situacao ].contains( pointData.sqEstado ) ) {
                                        estadosBiomasInsertedDeleted[ sqFicha.toString() ].estados[ situacao ].push( pointData.sqEstado )
                                    }
                                    // adicionar/remover ref bib do estado ( somente vai existir rowRefBiPonto para pontos do SALVE )
                                    rowRefBibPonto.each { rowRefBib ->
                                        // d('    - Verificando se ja existe a ref bib agendada no ESTADO')
                                        if( !refBibsFinal[ sqFicha.toString() ].estados[ pointData.sqEstado.toString() ] ) {
                                            refBibsFinal[ sqFicha.toString() ].estados[ pointData.sqEstado.toString() ] = [ inserted: [ ], deleted: [ ] ]
                                        }
                                        if( !refBibsFinal[ sqFicha.toString() ].estados[ pointData.sqEstado.toString() ][ situacao ].contains( rowRefBib ) ) {
                                            // d('      - ref bib agendada no Estado');
                                            refBibsFinal[ sqFicha.toString() ].estados[ pointData.sqEstado.toString() ][ situacao ].push( rowRefBib )
                                        }
                                    }
                                }
                            }
                        }

                        // agendar a exclusão do bioma se não tiver outro ponto no mesmo bioma
                        if( pointData.sqBioma ) {
                            cmdSql = """select sq_ficha_ocorrencia
                                from salve.ficha_ocorrencia
                                where sq_ficha = ${ sqFicha }
                                  and sq_ficha_ocorrencia != ${ pointData.idOcorrencia.toString() }
                                  and sq_contexto = 420
                                  and ge_ocorrencia is NOT null
                                  and sq_bioma=${ pointData.sqBioma.toString() }
                                  and (in_utilizado_avaliacao = 'S' or in_utilizado_avaliacao is null ) limit 1;"""
                            List row = sqlService.execSql( cmdSql )
                            if( !row ) {
                                // verificar se tem alguma ocorrencia do PORTALBIO no mesmo bioma e que esteja utilizado na avaliacao
                                cmdSql = """select sq_ficha_ocorrencia_portalbio
                                from salve.ficha_ocorrencia_portalbio
                                where sq_ficha = ${ sqFicha }
                                  and sq_bioma = ${ pointData.sqBioma.toString() }
                                  and (in_utilizado_avaliacao = 'S' or in_utilizado_avaliacao is null ) limit 1;"""
                                row = sqlService.execSql( cmdSql )
                                if( !row ) {
                                    // agendar a exclusão do Bioma
                                    String situacao = 'deleted'
                                    if( !estadosBiomasInsertedDeleted[ sqFicha.toString() ].biomas[ situacao ].contains( pointData.sqBioma ) ) {
                                        estadosBiomasInsertedDeleted[ sqFicha.toString() ].biomas[ situacao ].push( pointData.sqBioma )
                                    }
                                    // adicionar/remover ref bib do bioma ( somente vai existir rowRefBiPonto para pontos do SALVE )
                                    rowRefBibPonto.each { rowRefBib ->
                                        // d('    - Verificando se ja existe a ref bib agendada no BIOMA')
                                        if( !refBibsFinal[ sqFicha.toString() ].biomas[ pointData.sqBioma.toString() ] ) {
                                            refBibsFinal[ sqFicha.toString() ].biomas[ pointData.sqBioma.toString() ] = [ inserted: [ ], deleted: [ ] ]
                                        }
                                        if( !refBibsFinal[ sqFicha.toString() ].biomas[ pointData.sqBioma.toString() ][ situacao ].contains( rowRefBib ) ) {
                                            // d('      - ref bib agendada no Bioma');
                                            refBibsFinal[ sqFicha.toString() ].biomas[ pointData.sqBioma.toString() ][ situacao ].push( rowRefBib )
                                        }
                                    }
                                }
                            }
                        }

                        // agendar a exclusão da bacia se não tiver outro ponto na mesma bacia
                        if( pointData.sqBacia ) {
                            cmdSql = """select sq_ficha_ocorrencia
                                from salve.ficha_ocorrencia
                                where sq_ficha = ${ sqFicha }
                                  and sq_ficha_ocorrencia != ${ pointData.idOcorrencia.toString() }
                                  and sq_contexto = 420
                                  and ge_ocorrencia is NOT null
                                  and sq_bacia_hidrografica=${ pointData.sqBacia.toString() }
                                  and (in_utilizado_avaliacao = 'S' or in_utilizado_avaliacao is null ) limit 1;"""
                            List row = sqlService.execSql( cmdSql )
                            if( !row ) {
                                // verificar se tem alguma ocorrencia do PORTALBIO na mema bacia e que esteja utilizado na avaliacao
                                cmdSql = """select sq_ficha_ocorrencia_portalbio
                                from salve.ficha_ocorrencia_portalbio
                                where sq_ficha = ${ sqFicha }
                                  and sq_bacia_hidrografica = ${ pointData.sqBacia.toString() }
                                  and (in_utilizado_avaliacao = 'S' or in_utilizado_avaliacao is null ) limit 1;"""
                                row = sqlService.execSql( cmdSql )
                                if( !row ) {
                                    // agendar a exclusão do Bioma
                                    String situacao = 'deleted'
                                    if( !estadosBiomasInsertedDeleted[ sqFicha.toString() ].bacias[ situacao ].contains( pointData.sqBacia ) ) {
                                        estadosBiomasInsertedDeleted[ sqFicha.toString() ].bacias[ situacao ].push( pointData.sqBacia )
                                    }
                                    // adicionar/remover ref bib da bacia
                                    rowRefBibPonto.each { rowRefBib ->
                                        // d('    - Verificando se ja existe a ref bib agendada na BACIA')
                                        if( !refBibsFinal[ sqFicha.toString() ].bacias[ pointData.sqBacia.toString() ] ) {
                                            refBibsFinal[ sqFicha.toString() ].bacias[ pointData.sqBacia.toString() ] = [ inserted: [ ], deleted: [ ] ]
                                        }
                                        if( !refBibsFinal[ sqFicha.toString() ].bacias[ pointData.sqBacia.toString() ][ situacao ].contains( rowRefBib ) ) {
                                            // d('      - ref bib agendada no Bacia');
                                            refBibsFinal[ sqFicha.toString() ].bacias[ pointData.sqBacia.toString() ][ situacao ].push( rowRefBib )
                                        }
                                    }
                                }
                            }
                        }

                        // agendar a exclusão das ucs

                        // FEDERAIS
                        ucsNoPonto.FEDERAIS.each { ucFederal ->
                            cmdSql = """select a.sq_ficha_ocorrencia from salve.ficha_ocorrencia a
                                    where a.sq_ficha = ${ sqFicha }
                                    and a.sq_contexto = 420
                                    and a.ge_ocorrencia is not null
                                    and a.sq_ficha_ocorrencia != ${ pointData.idOcorrencia.toString() }
                                    and (a.in_utilizado_avaliacao = 'S' or a.in_utilizado_avaliacao is null )
                                    and exists ( select null from salve.vw_unidade_org x where x.sq_pessoa = ${ ucFederal.id.toString() } and x.the_geom is not null and st_intersects( ('SRID=4674;'||st_asText( a.ge_ocorrencia))::geometry, ST_CollectionExtract( x.the_geom,3) ) )
                                    limit 1"""
                            List row = sqlService.execSql( cmdSql )
                            if( !row ) {
                                // verificar se tem alguma ocorrencia do PORTALBIO na mesma UC e que esteja utilizado na avaliacao
                                cmdSql = """select a.sq_ficha_ocorrencia_portalbio from salve.ficha_ocorrencia_portalbio a
                                    where a.sq_ficha = ${ sqFicha }
                                    and a.ge_ocorrencia is not null
                                    and (a.in_utilizado_avaliacao = 'S' or a.in_utilizado_avaliacao is null )
                                    and exists ( select null from salve.vw_unidade_org x where x.sq_pessoa = ${ ucFederal.id.toString() } and x.the_geom is not null and st_intersects( ('SRID=4674;'||st_asText( a.ge_ocorrencia))::geometry, ST_CollectionExtract( x.the_geom,3) ) )
                                    limit 1"""
                                row = sqlService.execSql( cmdSql )
                                if( !row ) {
                                    // agendar a exclusão da UC
                                    String situacao = 'deleted'
                                    Map ucData = ['id' : ucFederal.id , 'esfera':'F' ]
                                    if( !estadosBiomasInsertedDeleted[ sqFicha.toString() ].ucs[ situacao ].contains( ucData ) ) {
                                        estadosBiomasInsertedDeleted[ sqFicha.toString() ].ucs[ situacao ].push( ucData )
                                    }
                                    if( !estadosBiomasInsertedDeleted[ sqFicha.toString() ].ucs.idsDeleted.contains( pointData.idOcorrencia.toLong() ) ) {
                                        estadosBiomasInsertedDeleted[ sqFicha.toString() ].ucs.idsDeleted.push( pointData.idOcorrencia.toLong() )
                                    }
                                    // adicionar/remover ref bib da uc ( somente vai existir rowRefBiPonto para pontos do SALVE )
                                    rowRefBibPonto.each { rowRefBib ->
                                        // d('    - Verificando se ja existe a ref bib agendada na UC')
                                        String ucIdEsfera = ucData.id.toString()+'|'+ucData.esfera
                                        if( !refBibsFinal[ sqFicha.toString() ].ucs[ ucIdEsfera ] ) {
                                            refBibsFinal[ sqFicha.toString() ].ucs[ ucIdEsfera ] = [ inserted: [ ], deleted: [ ] ]
                                        }
                                        if( !refBibsFinal[ sqFicha.toString() ].ucs[ ucIdEsfera ][ situacao ].contains( rowRefBib ) ) {
                                            // d('      - ref bib agendada na UC');
                                            refBibsFinal[ sqFicha.toString() ].ucs[ ucIdEsfera ][ situacao ].push( rowRefBib )
                                        }
                                    }
                                }
                            }
                        }

                        // ESTADUAIS
                        ucsNoPonto.ESTADUAIS.each { ucEstadual ->
                            cmdSql = """select a.sq_ficha_ocorrencia from salve.ficha_ocorrencia a
                                    where a.sq_ficha = ${ sqFicha }
                                    and a.sq_contexto = 420
                                    and a.ge_ocorrencia is not null
                                    and a.sq_ficha_ocorrencia != ${ pointData.idOcorrencia.toString() }
                                    and (a.in_utilizado_avaliacao = 'S' or a.in_utilizado_avaliacao is null )
                                    and exists ( select null from geo.vw_uc_estadual_geo x where x.gid = ${ ucEstadual.id.toString() } and x.the_geom is not null and st_intersects( ('SRID=4674;'||st_asText( a.ge_ocorrencia))::geometry, ST_CollectionExtract( x.the_geom,3) ) )
                                    limit 1"""
                            List row = sqlService.execSql( cmdSql )
                            if( !row ) {
                                // verificar se tem alguma ocorrencia do PORTALBIO na mesma UC e que esteja utilizado na avaliacao
                                cmdSql = """select a.sq_ficha_ocorrencia_portalbio from salve.ficha_ocorrencia_portalbio a
                                    where a.sq_ficha = ${ sqFicha }
                                    and a.ge_ocorrencia is not null
                                    and (a.in_utilizado_avaliacao = 'S' or a.in_utilizado_avaliacao is null )
                                    and exists ( select null from geo.vw_uc_estadual_geo x where x.gid = ${ ucEstadual.id.toString() } and x.the_geom is not null and st_intersects( ('SRID=4674;'||st_asText( a.ge_ocorrencia))::geometry, ST_CollectionExtract( x.the_geom,3) ) )
                                    limit 1"""
                                row = sqlService.execSql( cmdSql )
                                if( !row ) {
                                    // agendar a exclusão da UC
                                    String situacao = 'deleted'
                                    Map ucData = ['id' : ucEstadual.id , 'esfera':'E' ]
                                    if( !estadosBiomasInsertedDeleted[ sqFicha.toString() ].ucs[ situacao ].contains( ucData ) ) {
                                        estadosBiomasInsertedDeleted[ sqFicha.toString() ].ucs[ situacao ].push( ucData )
                                    }
                                    if( !estadosBiomasInsertedDeleted[ sqFicha.toString() ].ucs.idsDeleted.contains( pointData.idOcorrencia.toLong() ) ) {
                                        estadosBiomasInsertedDeleted[ sqFicha.toString() ].ucs.idsDeleted.push( pointData.idOcorrencia.toLong() )
                                    }
                                    // adicionar/remover ref bib da uc ( somente vai existir rowRefBiPonto para pontos do SALVE )
                                    rowRefBibPonto.each { rowRefBib ->
                                        // d('    - Verificando se ja existe a ref bib agendada na UC')
                                        String ucIdEsfera = ucData.id.toString()+'|'+ucData.esfera
                                        if( !refBibsFinal[ sqFicha.toString() ].ucs[ ucIdEsfera ] ) {
                                            refBibsFinal[ sqFicha.toString() ].ucs[ ucIdEsfera ] = [ inserted: [ ], deleted: [ ] ]
                                        }
                                        if( !refBibsFinal[ sqFicha.toString() ].ucs[ ucIdEsfera ][ situacao ].contains( rowRefBib ) ) {
                                            // d('      - ref bib agendada na UC');
                                            refBibsFinal[ sqFicha.toString() ].ucs[ ucIdEsfera ][ situacao ].push( rowRefBib )
                                        }
                                    }
                                }
                            }
                        }

                        // RPPN
                        ucsNoPonto.RPPNS.each { ucRppn ->
                            cmdSql = """select a.sq_ficha_ocorrencia from salve.ficha_ocorrencia a
                                    where a.sq_ficha = ${ sqFicha }
                                    and a.sq_contexto = 420
                                    and a.ge_ocorrencia is not null
                                    and a.sq_ficha_ocorrencia != ${ pointData.idOcorrencia.toString() }
                                    and (a.in_utilizado_avaliacao = 'S' or a.in_utilizado_avaliacao is null )
                                    and exists ( select null from geo.vw_rppn_geo x where x.ogc_fid = ${ ucRppn.id.toString() } and x.the_geom is not null and st_intersects( ('SRID=4674;'||st_asText( a.ge_ocorrencia))::geometry, ST_CollectionExtract( x.the_geom,3) ) )
                                    limit 1"""
                            List row = sqlService.execSql( cmdSql )
                            if( !row ) {
                                // verificar se tem alguma ocorrencia do PORTALBIO na mesma UC e que esteja utilizado na avaliacao
                                cmdSql = """select a.sq_ficha_ocorrencia_portalbio from salve.ficha_ocorrencia_portalbio a
                                    where a.sq_ficha = ${ sqFicha }
                                    and a.ge_ocorrencia is not null
                                    and (a.in_utilizado_avaliacao = 'S' or a.in_utilizado_avaliacao is null )
                                    and exists ( select null from geo.vw_rppn_geo x where x.ogc_fid = ${ ucRppn.id.toString() } and x.the_geom is not null and st_intersects( ('SRID=4674;'||st_asText( a.ge_ocorrencia))::geometry, ST_CollectionExtract( x.the_geom,3) ) )
                                    limit 1"""
                                row = sqlService.execSql( cmdSql )
                                if( !row ) {
                                    // agendar a exclusão da UC
                                    String situacao = 'deleted'
                                    Map ucData = ['id' : ucRppn.id , 'esfera':'R' ]
                                    if( !estadosBiomasInsertedDeleted[ sqFicha.toString() ].ucs[ situacao ].contains( ucData ) ) {
                                        estadosBiomasInsertedDeleted[ sqFicha.toString() ].ucs[ situacao ].push( ucData )
                                    }
                                    if( !estadosBiomasInsertedDeleted[ sqFicha.toString() ].ucs.idsDeleted.contains( pointData.idOcorrencia.toLong() ) ) {
                                        estadosBiomasInsertedDeleted[ sqFicha.toString() ].ucs.idsDeleted.push( pointData.idOcorrencia.toLong() )
                                    }
                                    // adicionar/remover ref bib da uc ( somente vai existir rowRefBiPonto para pontos do SALVE )
                                    rowRefBibPonto.each { rowRefBib ->
                                        // d('    - Verificando se ja existe a ref bib agendada na UC')
                                        String ucIdEsfera = ucData.id.toString()+'|'+ucData.esfera
                                        if( !refBibsFinal[ sqFicha.toString() ].ucs[ ucIdEsfera ] ) {
                                            refBibsFinal[ sqFicha.toString() ].ucs[ ucIdEsfera ] = [ inserted: [ ], deleted: [ ] ]
                                        }
                                        if( !refBibsFinal[ sqFicha.toString() ].ucs[ ucIdEsfera ][ situacao ].contains( rowRefBib ) ) {
                                            // d('      - ref bib agendada na UC');
                                            refBibsFinal[ sqFicha.toString() ].ucs[ ucIdEsfera ][ situacao ].push( rowRefBib )
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else {
                        // situaçõe: S - utilizando na avaliação ou N-não utilizado na avaliação atualizar
                        // os campos pais, estado, bioma, bacia e uc do mesmo jeito
                        if( pointData.situacao == 'S' || pointData.situacao == 'N' || pointData.situacao == 'E' || pointData.situacao == 'R' ) {

                            boolean houveAlteracaoEstadoBiomaUc = false
                            // localizar o estado pela coordenada, sempre atualizar o estado e pais juntos
                            if( !pointData.sqEstado ) {
                                estado = geoService.getUfByCoord( pointData.ponto.y, pointData.ponto.x )
                                if( estado ) {
                                    // d('   - ' + estado.noEstado)
                                    pointData.sqEstado = estado.id
                                    houveAlteracaoEstadoBiomaUc = true
                                }
                                else {
                                    // d('   - Estado NÃO localizado no ponto - x:' + pointData.ponto.x + ' u:' + pointData.ponto.y)
                                }

                            }


                            // localizar o municipio pela coordenada
                            if( !pointData.isCentroideEstado ) {

                                // d(' ')
                                // d(' - verificando Municipio')
                                if( !pointData.sqMunicipio && pointData.sqEstado ) {
                                    municipio = geoService.getMunicipioByCoord( pointData.ponto.y, pointData.ponto.x, Estado.get( pointData.sqEstado.toInteger() ) )
                                    if( municipio ) {
                                        // d('   - ' + municipio.noMunicipio)
                                        pointData.sqMunicipio = municipio.id
                                        houveAlteracaoEstadoBiomaUc = true
                                    }
                                    else {
                                        // d('   - Municipio NÃO localizado no ponto - x:' + pointData.ponto.x + ' u:' + pointData.ponto.y)
                                    }
                                }
                            }
                            else {
                                // d('   - Municipio JA identificado')
                            }

                            // localizar o Bioma pela coordenada
                            DadosApoio biomaSalve
                            if( !pointData.isCentroideEstado ) {
                                if( !pointData.sqBioma || pointData.sqBioma < 10 ) {
                                    biomaSalve = geoService.getBiomaSalveByCoord( pointData.ponto.y, pointData.ponto.x )
                                    if( biomaSalve ) {
                                        pointData.sqBioma = biomaSalve.id
                                        houveAlteracaoEstadoBiomaUc = true
                                    }
                                }
                            }
                            if( pointData.sqBioma ){
                                biomaSalve = biomaSalve ?: DadosApoio.get( pointData.sqBioma.toLong() )
                                // se for Sistema Costeriro-Marinho, verificar se o grupo pode estar neste bioma
                                if( biomaSalve.codigoSistema == 'MARINHO_COSTEIRO' ) {
                                    String msgValidacaoBioma = geoService.validarBiomaGrupo(vwFicha.id.toLong(), pointData.ponto.y.toLong(), pointData.ponto.x.toLong() )
                                    if( msgValidacaoBioma ){
                                        pointData.sqBioma=null
                                        houveAlteracaoEstadoBiomaUc = true
                                    }
                                }
                            }

                            // localizar a Bacia pela coordenada
                            if( !pointData.isCentroideEstado ) {
                                if( !pointData.sqBacia ) {
                                    bacia = geoService.getBaciaByCoord( pointData.ponto.y, pointData.ponto.x )
                                    if( bacia ) {
                                        pointData.sqBacia = bacia.id
                                        houveAlteracaoEstadoBiomaUc = true
                                    }
                                }
                            }

                            // ajustar os ids das ucs
                            if( !pointData.isCentroideEstado ) {

                                if( !pointData.sqUcFederal && ucsNoPonto.FEDERAIS ) {
                                    pointData.sqUcFederal = ucsNoPonto.FEDERAIS[ 0 ].id
                                    houveAlteracaoEstadoBiomaUc = true
                                }
                                if( !pointData.sqUcEstadual && ucsNoPonto.ESTADUAIS ) {
                                    pointData.sqUcEstadual = ucsNoPonto.ESTADUAIS[ 0 ].id
                                    houveAlteracaoEstadoBiomaUc = true
                                }
                                if( !pointData.sqRppn && ucsNoPonto.RPPNS ) {
                                    pointData.sqRppn = ucsNoPonto.RPPNS[ 0 ].id
                                    houveAlteracaoEstadoBiomaUc = true
                                }
                            }

                            cmdSql = ''
                            if( pointData.isCentroideEstado ) {
                                if( pointData.sqUcFederal || pointData.sqUcEstadual || pointData.sqRppn ) {
                                    cmdSql = """update ${ table } set sq_uc_federal=null
                                    ,sq_uc_estadual=null
                                    ,sq_rppn=null
                                     where ${ tableColumnId }=${ pointData.idOcorrencia.toString() };"""
                                }
                            }
                            else {
                                if( houveAlteracaoEstadoBiomaUc ) {
                                    cmdSql = """update ${ table } set sq_estado=${ pointData.sqEstado ?: 'sq_estado' }
                                    ,sq_bioma=${ pointData.sqBioma ?: 'sq_bioma' }
                                    ,sq_bacia_hidrografica=${ pointData.sqBacia ?: 'sq_bacia_hidrografica' }
                                    ,sq_municipio=${ pointData.sqMunicipio ?: 'sq_municipio' }
                                    ,sq_uc_federal=${ pointData.sqUcFederal ?: 'sq_uc_federal' }
                                    ,sq_uc_estadual=${ pointData.sqUcEstadual ?: 'sq_uc_estadual' }
                                    ,sq_rppn=${ pointData.sqRppn ?: 'sq_rppn' }
                                     where ${ tableColumnId }=${ pointData.idOcorrencia.toString() };"""
                                }
                            }
                            if( cmdSql ) {
                                scriptsSql.push( cmdSql )
                            }
                            // adicionar Estado  o pos-processamento de Inclusão/Exclusão
                            // d(' ');
                            // d(' - agendar Inclusao/Exclusao do estado');
                            String situacao = ''
                            if( pointData.sqEstado ) {
                                if( pointData.situacao == 'N' || pointData.situacao == 'E' ) {
                                    // d('  - agendada exclusao do Estado: '+pointData.sqEstado+'  Situacao do ponto:  ' + pointData.situacao)
                                    situacao = 'deleted'
                                }
                                else {
                                    // d('  - agendada inclusao');
                                    situacao = 'inserted'
                                }
                                if( !estadosBiomasInsertedDeleted[ sqFicha.toString() ].estados[ situacao ].contains( pointData.sqEstado ) ) {
                                    estadosBiomasInsertedDeleted[ sqFicha.toString() ].estados[ situacao ].push( pointData.sqEstado )
                                }

                                // adicionar/remover ref bib do estado ( somente vai existir rowRefBiPonto para pontos do SALVE )
                                rowRefBibPonto.each { rowRefBib ->
                                    // d('    - Verificando se ja existe a ref bib agendada no ESTADO')
                                    if( !refBibsFinal[ sqFicha.toString() ].estados[ pointData.sqEstado.toString() ] ) {
                                        refBibsFinal[ sqFicha.toString() ].estados[ pointData.sqEstado.toString() ] = [ inserted: [ ], deleted: [ ] ]
                                    }
                                    if( !refBibsFinal[ sqFicha.toString() ].estados[ pointData.sqEstado.toString() ][ situacao ].contains( rowRefBib ) ) {
                                        // d('      - ref bib agendada no Estado');
                                        refBibsFinal[ sqFicha.toString() ].estados[ pointData.sqEstado.toString() ][ situacao ].push( rowRefBib )
                                    }
                                }
                            }


                            // adicionar BIOMA ao pos-processamento de Inclusão/Exclusão
                            // d(' ');
                            // d(' - agendar Inclusao/Exclusao do BIOMA');
                            //if (!isCentroideEstado) {
                            if( pointData.sqBioma ) {
                                if( pointData.situacao == 'N' || pointData.situacao == 'E' || pointData.isCentroideEstado ) {
                                    // d('  - agendada exclusao');
                                    situacao = 'deleted'
                                }
                                else {
                                    // d('  - agendada inclusao');
                                    situacao = 'inserted'
                                }
                                if( !estadosBiomasInsertedDeleted[ sqFicha.toString() ].biomas[ situacao ].contains( pointData.sqBioma ) ) {
                                    estadosBiomasInsertedDeleted[ sqFicha.toString() ].biomas[ situacao ].push( pointData.sqBioma )
                                }
                                // adicionar/remover ref bib do BIOMA ( somente vai existir rowRefBiPonto para pontos do SALVE )
                                rowRefBibPonto.each { rowRefBib ->
                                    // d('    - Verificando se ja existe a ref bib agendada no BIOMA')
                                    if( !refBibsFinal[ sqFicha.toString() ].biomas[ pointData.sqBioma.toString() ] ) {
                                        refBibsFinal[ sqFicha.toString() ].biomas[ pointData.sqBioma.toString() ] = [ inserted: [ ], deleted: [ ] ]
                                    }
                                    if( !refBibsFinal[ sqFicha.toString() ].biomas[ pointData.sqBioma.toString() ][ situacao ].contains( rowRefBib ) ) {
                                        refBibsFinal[ sqFicha.toString() ].biomas[ pointData.sqBioma.toString() ][ situacao ].push( rowRefBib )
                                        // d('      - ref bib agendada no BIOMA');
                                    }
                                }
                            }

                            // bacia
                            if( pointData.sqBacia ) {
                                if( pointData.situacao == 'N' || pointData.situacao == 'E' || pointData.isCentroideEstado ) {
                                    // d('  - agendada exclusao');
                                    situacao = 'deleted'
                                }
                                else {
                                    // d('  - agendada inclusao');
                                    situacao = 'inserted'
                                }
                                if( !estadosBiomasInsertedDeleted[ sqFicha.toString() ].bacias[ situacao ].contains( pointData.sqBacia ) ) {
                                    estadosBiomasInsertedDeleted[ sqFicha.toString() ].bacias[ situacao ].push( pointData.sqBacia )
                                }
                                // adicionar/remover ref bib do BACIA
                                rowRefBibPonto.each { rowRefBib ->
                                    // d('    - Verificando se ja existe a ref bib agendada no BIOMA')
                                    if( !refBibsFinal[ sqFicha.toString() ].bacias[ pointData.sqBacia.toString() ] ) {
                                        refBibsFinal[ sqFicha.toString() ].bacias[ pointData.sqBacia.toString() ] = [ inserted: [ ], deleted: [ ] ]
                                    }
                                    if( !refBibsFinal[ sqFicha.toString() ].bacias[ pointData.sqBacia.toString() ][ situacao ].contains( rowRefBib ) ) {
                                        refBibsFinal[ sqFicha.toString() ].bacias[ pointData.sqBacia.toString() ][ situacao ].push( rowRefBib )
                                        // d('      - ref bib agendada no BACIA');
                                    }
                                }
                            }
                        }



                        // adicionar as UCs ao pos-processamento de Inclusão/Exclusão
                        // d(' ');
                        // d(' - agendar Inclusao/Exclusao das UCs');
                        if( !isCentroideEstado ) {

                            // ler todas as UCs: Federais, Estaduais e RPPNS que o ponto pertence

                            // d('  - Ler todas as UCs abrangidas pelo ponto')
                            //Map mapUcs = geoService.getUcsByCoord(pointData.ponto.y, pointData.ponto.x)
                            String situacao
                            ucsNoPonto.each { key, ucs ->
                                // d('    - ' + key)
                                ucs.each {
                                    // d('      - ' + it.nome)
                                    String esfera = it.esfera
                                    /*if( key=='FEDERAL') {
                                        esfera ='F'
                                    } else if( key=='ESTADUAL' ){
                                        esfera='E'
                                    } else {
                                        esfera='R'
                                    }*/
                                    Map ucData = ['id' : it.id , 'esfera': esfera ]
                                    String ucIdEsfera = ucData.id.toString()+'|'+ucData.esfera
                                    if( pointData.situacao == 'N' || pointData.situacao == 'E' || pointData.isCentroideEstado ) {
                                        // d('  - agendada exclusao');
                                        situacao = 'deleted'
                                        if( !estadosBiomasInsertedDeleted[ sqFicha.toString() ].ucs.deleted.contains( ucData ) ) {
                                            estadosBiomasInsertedDeleted[ sqFicha.toString() ].ucs.deleted.push( ucData )
                                        }
                                        if( !estadosBiomasInsertedDeleted[ sqFicha.toString() ].ucs.idsDeleted.contains( pointData.idOcorrencia.toLong() ) ) {
                                            estadosBiomasInsertedDeleted[ sqFicha.toString() ].ucs.idsDeleted.push( pointData.idOcorrencia.toLong() )
                                        }
                                    }
                                    else {
                                        // d('  - agendada inclusao');
                                        situacao = 'inserted'
                                        // d('  - UC esta no estado ' + row.sq_estado)
                                        Map mapTemp = [ id: it.id.toInteger(), 'esfera':esfera, 'sqEstado': pointData.sqEstado, 'inPresencaAtual': pointData.inPresencaAtual ]
                                        if( ! estadosBiomasInsertedDeleted[ sqFicha.toString() ].ucs.inserted.contains( mapTemp ) ) {
                                            estadosBiomasInsertedDeleted[ sqFicha.toString() ].ucs.inserted.push( mapTemp )
                                        }
                                    }

                                    if( pointData.bd.toLowerCase() == 'salve' ) {
                                        // adicionar/remover ref bib da UC ( somente vai existir rowRefBiPonto para pontos do SALVE )
                                        rowRefBibPonto.each { rowRefBib ->
                                            // d('    - Verificando se ja existe a ref bib agendada a UC')
                                            if( !refBibsFinal[ sqFicha.toString() ].ucs[ ucIdEsfera ] ) {
                                                refBibsFinal[ sqFicha.toString() ].ucs[ ucIdEsfera ] = [ inserted: [ ], deleted: [ ] ]
                                            }
                                            if( !refBibsFinal[ sqFicha.toString() ].ucs[ ucIdEsfera ][ situacao ].contains( rowRefBib ) ) {
                                                // d('      - ref bib agendada para UC');
                                                refBibsFinal[ sqFicha.toString() ].ucs[ ucIdEsfera ][ situacao ].push( rowRefBib )
                                            }
                                        }
                                    }

                                }
                            }
                        }
                    }
                    workerService.stepIt( worker )
                } // fim each pontos
            } // end each data
            // fim loop pontos

            if( scriptsSql ) {
                // d(' - executando scripts gerados')
                // println scriptsSql.join(';\n')
                sqlService.execSql( scriptsSql.join(';\n') )
            }
            // remover ou adicionar os Estados, Biomas e UC referentes ao ponto
            //d(' ' )
            //d('Processar Estados Biomas Inserted Deleted')

            //println 'ESTADOS BIOMAS INSERTED DELETED'
            //println estadosBiomasInsertedDeleted
            //throw new Exception('FIM TESTE EXCLUSAO PONTO')

            estadosBiomasInsertedDeleted.each { sqFicha, item ->
                // ESTADOS
                // d(' ')
                // d(' - Inicio inclusao dos ESTADOS na ficha');
                item.estados.inserted.each { sqEstado ->
                    // se o estado tambem estiver na lista para exclusao então excluir da lista de exclusao
                    item.estados.deleted.removeAll { it.toLong() == sqEstado.toLong() }

                    // verificar se existe pelo menos 1 registro do contexto Estado no mesmo Estado
                    cmdSql = """select sq_ficha_ocorrencia from salve.ficha_ocorrencia
                                where sq_ficha = ${sqFicha.toString()}
                                and sq_contexto = ${contextoEstado.id.toString()}
                                and sq_estado = ${sqEstado.toString()}
                                limit 1
                                """
                    List res = sqlService.execSql(cmdSql)
                    Long idFichaOcorrencia = 0
                    if (!res) {
                        estado = Estado.get(sqEstado.toLong())
                        ficha = Ficha.get(sqFicha.toLong())
                        FichaOcorrencia fo = new FichaOcorrencia()
                        fo.ficha = ficha
                        fo.estado = estado
                        fo.contexto = contextoEstado
                        fo.save(flush: true)
                        idFichaOcorrencia = fo.id.toLong();
                        // d('  - Estado ' + estado.noEstado + ' adicionado na ficha ')
                    } else {
                        // d('  - Estado ' + sqEstado.toString() + ' ja existe na ficha');
                        idFichaOcorrencia = res[0].sq_ficha_ocorrencia.toLong()
                    }
                    // d(' ')
                    if (idFichaOcorrencia) {

                        if (refBibsFinal) {
                            // d(' ')
                            // d('  - Adicionar as Ref Bibs ao Estado relacionado a ocorrencia: ' + idFichaOcorrencia)
                            // a referencia bibliográfica é a mesma que estiver para o ponto
                            if (refBibsFinal[sqFicha.toString()].estados[sqEstado.toString()]?.inserted ) {
                                refBibsFinal[sqFicha.toString()].estados[sqEstado.toString()].inserted.each { refBib ->
                                    // se existir a mesma ref bib para exclusão, então remover da lista de exclusão
                                    refBibsFinal[sqFicha.toString()].estados[sqEstado.toString()].deleted.removeAll {
                                        it  == refBib
                                    }
                                    // verificar se já existe a ref bib no estado da ocorrencia
                                    cmdSql = """select sq_ficha_ref_bib from salve.ficha_ref_bib where sq_ficha = ${ sqFicha.toString() } and no_tabela = 'ficha_ocorrencia' and no_coluna = 'sq_estado' and sq_registro = ${idFichaOcorrencia.toString()}"""
                                    if ( refBib.sq_publicacao ) {
                                        cmdSql += """ and sq_publicacao = ${refBib.sq_publicacao.toString()};"""
                                    } else {
                                        cmdSql += """ and no_autor = '${refBib.no_autor}' and nu_ano = ${refBib.nu_ano.toString()};"""
                                    }


                                    if ( ! sqlService.execSql( cmdSql ) ) {
                                        if (!estado) {
                                            estado = Estado.get(sqEstado.toLong())
                                        }
                                        Map pars = [:]
                                        pars.sqFicha = sqFicha.toLong()
                                        pars.sqPublicacao = refBib.sq_publicacao ?: null
                                        pars.noTabela = 'ficha_ocorrencia'
                                        pars.sqRegistro = idFichaOcorrencia
                                        pars.noColuna = 'sq_estado'
                                        pars.deRotulo = 'Estado - ' + estado.noEstado
                                        pars.noAutor = refBib.no_autor ?: null
                                        pars.nuAno = refBib.nu_ano ?: null
                                        cmdSql = """INSERT INTO salve.ficha_ref_bib( sq_ficha, sq_publicacao, no_tabela, sq_registro, no_coluna, de_rotulo, no_autor, nu_ano)
                                        VALUES ( :sqFicha, :sqPublicacao, :noTabela, :sqRegistro, :noColuna, :deRotulo, :noAutor, :nuAno);"""
                                        sqlService.execSql(cmdSql, pars)
                                        // d('    - Referencia bibliografica adicionado ao Estado '+estado.noEstado)
                                    }
                                }
                            }
                        }
                    }
                }
                // d(' - Fim inclusao dos ESTADOS na ficha');


                // d(' ')
                // d(' - Inicio exclusao dos ESTADOS na ficha');
                item.estados.deleted.each { sqEstado ->
                    // d('   - verificando algum ponto do Salve no Estado');
                    cmdSql="""select o.sq_ficha_ocorrencia from salve.ficha_ocorrencia o
                                where o.sq_ficha = ${sqFicha.toString() }
                                and o.sq_contexto = ${contextoOcorrencia.id.toString() }
                                and o.ge_ocorrencia is not null
                                and ( o.sq_estado = ${sqEstado.toString()} OR exists (
                                    select null from corporativo.vw_estado uf
                                    where uf.sq_estado = ${sqEstado.toString()} and ST_Intersects(ST_GeographyFromText('SRID=4674;'||ST_AsText( o.ge_ocorrencia)), uf.the_geom::geometry )
                                    limit 1
                                ) )
                                and ( o.in_utilizado_avaliacao = 'S' or o.in_utilizado_avaliacao is null )
                                and ( o.in_presenca_atual is null or o.in_presenca_atual != 'N' )
                                limit 1
                             """
                    List res = sqlService.execSql(cmdSql)
                    if( ! res ) {
                        // excluir a referencia bibliográfica do estado
                        cmdSql = """select sq_ficha_ocorrencia from salve.ficha_ocorrencia
                                where sq_ficha = ${sqFicha.toString()}
                                and sq_contexto = ${contextoEstado.id.toString()}
                                and sq_estado = ${sqEstado.toString()}
                                """
                        List rowsTemp =  sqlService.execSql(cmdSql)
                        rowsTemp.each { row ->
                            // excluir ref_bib referentes ao estado
                            // d('     - Removendo as referências bibliograficas vinculadas ao Estado ' + sqEstado.toString() + ' da ficha')
                            cmdSql="""delete from salve.ficha_ref_bib where sq_ficha=${sqFicha.toString()}
                                          and no_tabela = 'ficha_ocorrencia'
                                          and sq_registro = ${row.sq_ficha_ocorrencia.toString()}
                                          and no_coluna = 'sq_estado'"""
                            sqlService.execSql(cmdSql)
                        }

                        // d('   - verificando algum ponto do Portalbio no Estado');
                        // verificar se tem algum ponto do portalbio utilizado na avaliacao do mesmo estado
                        cmdSql = """select o.sq_ficha_ocorrencia_portalbio
                                    from salve.ficha_ocorrencia_portalbio o
                                    where o.sq_ficha = ${sqFicha.toString()}
                                    and ( o.sq_estado = ${sqEstado.toString()} OR exists (
                                        select null from corporativo.vw_estado uf
                                        where uf.sq_estado = ${sqEstado.toString()} and ST_Intersects(ST_GeographyFromText('SRID=4674;'||ST_AsText( o.ge_ocorrencia)), uf.the_geom::geometry )
                                        limit 1
                                    ) )
                                    and ( o.in_utilizado_avaliacao = 'S')
                                    limit 1
                                """
                        res = sqlService.execSql(cmdSql)
                        if ( ! res ) {
                            // não existe ponto no Estado
                            rowsTemp.each { row ->
                                // d('   - Removendo Estado ' + sqEstado.toString() + ' da ficha')
                                cmdSql = """delete from salve.ficha_ocorrencia where sq_ficha_ocorrencia = ${row.sq_ficha_ocorrencia.toString() }"""
                                sqlService.execSql(cmdSql)
                                // d('     - Estado ' + sqEstado.toString() + ' removido da ficha ')
                            }
                        }
                    }
                }
                // d(' - Fim exclusao dos ESTADOS da ficha');

                // BIOMAS
                // d(' ')
                // d(' - Inicio inclusao dos BIOMAS na ficha');
                item.biomas.inserted.each { sqBioma ->
                    // se o bioma tambem estiver na lista para exclusao excluir da lista de exclusao
                    item.biomas.deleted.removeAll { it.toLong() == sqBioma.toLong() }

                    // verificar se existe pelo menos 1 registro do contexto Bioma no mesmo Bioma
                    cmdSql = """select sq_ficha_ocorrencia from salve.ficha_ocorrencia
                                where sq_ficha = ${sqFicha.toString()}
                                and sq_contexto = ${contextoBioma.id.toString()}
                                and sq_bioma = ${sqBioma.toString()}
                                limit 1
                                """
                    List res = sqlService.execSql(cmdSql)
                    Long idFichaOcorrencia = 0
                    if (!res) {
                        biomaApoio = DadosApoio.get(sqBioma.toLong())
                        ficha = Ficha.get(sqFicha.toLong())
                        FichaOcorrencia fo = new FichaOcorrencia()
                        fo.ficha = ficha
                        fo.bioma = biomaApoio
                        fo.contexto = contextoBioma
                        fo.save(flush: true)
                        idFichaOcorrencia = fo.id.toLong();
                        // d('  - Bioma ' + biomaApoio.descricao + ' adicionado na ficha ')
                    } else {
                        // d('  - Bioma ' + sqBioma.toString() + ' ja existe na ficha');
                        idFichaOcorrencia = res[0].sq_ficha_ocorrencia.toLong()
                    }
                    // d(' ')
                    if (idFichaOcorrencia) {
                        if (refBibsFinal) {
                            // d(' ')
                            // d('  - Adicionar as Ref Bibs ao BIOMA relacionado a ocorrencia: ' + idFichaOcorrencia)
                            // a referencia bibliográfica é a mesma que estiver para o ponto
                            if (refBibsFinal[sqFicha.toString()].biomas[sqBioma.toString()]?.inserted) {
                                refBibsFinal[sqFicha.toString()].biomas[sqBioma.toString()].inserted.each { refBib ->
                                    // se existir a mesma ref bib para exclusão, então remover da lista de exclusão
                                    refBibsFinal[sqFicha.toString()].biomas[sqBioma.toString()].deleted.removeAll {
                                        it == refBib
                                    }

                                    // verificar se já existe a ref bib no bioma da ocorrencia
                                    cmdSql = """select sq_ficha_ref_bib from salve.ficha_ref_bib where sq_ficha = ${ sqFicha.toString() } and no_tabela = 'ficha_ocorrencia' and no_coluna = 'sq_bioma' and sq_registro = ${idFichaOcorrencia.toString()}"""
                                    if (refBib.sq_publicacao) {
                                        cmdSql += """ and sq_publicacao = ${refBib.sq_publicacao.toString()};"""
                                    } else {
                                        cmdSql += """ and no_autor = '${refBib.no_autor}' and nu_ano = ${refBib.nu_ano.toString()};"""
                                    }
                                    if (!sqlService.execSql(cmdSql)) {
                                        if (!biomaApoio) {
                                            biomaApoio = DadosApoio.get(sqBioma.toLong())
                                        }
                                        Map pars = [:]
                                        pars.sqFicha = sqFicha.toLong()
                                        pars.sqPublicacao = refBib.sq_publicacao ?: null
                                        pars.noTabela = 'ficha_ocorrencia'
                                        pars.sqRegistro = idFichaOcorrencia
                                        pars.noColuna = 'sq_bioma'
                                        pars.deRotulo = 'Bioma - ' + biomaApoio.descricao
                                        pars.noAutor = refBib.no_autor ?: null
                                        pars.nuAno = refBib.nu_ano ?: null
                                        cmdSql = """INSERT INTO salve.ficha_ref_bib( sq_ficha, sq_publicacao, no_tabela, sq_registro, no_coluna, de_rotulo, no_autor, nu_ano)
                                        VALUES ( :sqFicha, :sqPublicacao, :noTabela, :sqRegistro, :noColuna, :deRotulo, :noAutor, :nuAno);"""
                                        sqlService.execSql(cmdSql, pars)
                                        // d('    - Referencia bibliografica adicionado ao Bioma ' + biomaApoio.descricao)
                                    }
                                }
                            }
                        }
                    }
                }
                // d(' - Fim inclusao dos BIOMAS na ficha');

                // d(' ')
                // d(' - Inicio exclusao dos BIOMAS na ficha');
                item.biomas.deleted.each { sqBioma ->
                    // d('   - verificando algum ponto do Salve no Bioma');
                    cmdSql = """select o.sq_ficha_ocorrencia from salve.ficha_ocorrencia o
                                where o.sq_ficha = ${sqFicha.toString()}
                                and o.sq_contexto = ${contextoOcorrencia.id.toString()}
                                and o.ge_ocorrencia is not null
                                and ( o.sq_bioma = ${sqBioma.toString()} or exists (
                                    select null from corporativo.vw_bioma bioma
                                    where bioma.sq_bioma = ${sqBioma.toString()} and ST_Intersects(ST_GeographyFromText('SRID=4674;'||ST_AsText( o.ge_ocorrencia)), bioma.the_geom::geometry )
                                    limit 1
                                ) )
                                and ( o.in_utilizado_avaliacao = 'S' or o.in_utilizado_avaliacao is null )
                                and ( o.in_presenca_atual is null or o.in_presenca_atual != 'N' )
                                limit 1"""
                    List res = sqlService.execSql(cmdSql)
                    if (!res) {
                        // não existe ponto no Bioma
                        // d('   - Removendo Bioma ' + sqBioma.toString() + ' da ficha')
                        cmdSql = """select sq_ficha_ocorrencia from salve.ficha_ocorrencia
                                where sq_ficha = ${sqFicha.toString()}
                                and sq_contexto = ${contextoBioma.id.toString()}
                                and sq_bioma = ${sqBioma.toString()}
                                """
                        List rowsTemp =  sqlService.execSql(cmdSql)
                        rowsTemp.each { row ->
                            // excluir ref_bib referentes ao bioma
                            // d('     - Removendo as referências bibliograficas vinculadas ao Bioma ' + sqBioma.toString() + ' da ficha')
                            cmdSql="""delete from salve.ficha_ref_bib where sq_ficha = ${sqFicha.toString()}
                                        and no_tabela = 'ficha_ocorrencia'
                                        and sq_registro = ${row.sq_ficha_ocorrencia.toString()}
                                        and no_coluna = 'sq_bioma'"""
                            sqlService.execSql(cmdSql)
                        }

                        // d('   - verificando algum ponto do Portalbio no Bioma');
                        // verificar se tem algum ponto do portalbio utilizado na avaliacao do mesmo estado
                        cmdSql = """select o.sq_ficha_ocorrencia_portalbio
                                    from salve.ficha_ocorrencia_portalbio o
                                    where o.sq_ficha = ${sqFicha.toString()}
                                    and ( o.sq_bioma = ${sqBioma.toString()} OR exists (
                                        select null from corporativo.vw_bioma bioma
                                        where bioma.sq_bioma = ${sqBioma.toString()} and ST_Intersects(ST_GeographyFromText('SRID=4674;'||ST_AsText( o.ge_ocorrencia)), bioma.the_geom::geometry )
                                        limit 1
                                    ) )
                                    and ( o.in_utilizado_avaliacao = 'S')
                                    limit 1
                                """
                        res = sqlService.execSql(cmdSql)
                        if ( ! res) {
                            rowsTemp.each { row ->
                                // d('   - Removendo Bioma ' + sqBioma.toString() + ' da ficha')
                                cmdSql = """delete from salve.ficha_ocorrencia where sq_ficha_ocorrencia = ${row.sq_ficha_ocorrencia.toString()}"""
                                sqlService.execSql(cmdSql)
                                // d('    - Bioma ' + sqBioma.toString() + ' removido da ficha ')
                            }
                        }
                    }
                }
                // d(' - Fim exclusao dos BIOMAS da ficha');

//--------------------------------------------------------------------
                // BACIAS
                // d(' ')
                // d(' - Inicio inclusao das BACIAS na ficha');
                item.bacias.inserted.each { sqBacia ->
                    // se a bacia tambem estiver na lista para exclusao excluir da lista de exclusao
                    item.bacias.deleted.removeAll { it.toLong() == sqBacia.toLong() }

                    // verificar se existe pelo menos 1 registro do contexto bacia_hidrografica na mesma bacia
                    cmdSql = """select sq_ficha_ocorrencia from salve.ficha_ocorrencia
                                where sq_ficha = ${sqFicha.toString()}
                                and sq_contexto = ${contextoBacia.id.toString()}
                                and sq_bacia_hidrografica = ${sqBacia.toString()}
                                limit 1
                                """
                    List res = sqlService.execSql(cmdSql)
                    Long idFichaOcorrencia = 0
                    if (!res) {
                        bacia = DadosApoio.get(sqBacia.toLong())
                        ficha = Ficha.get(sqFicha.toLong() )
                        FichaOcorrencia fo = new FichaOcorrencia()
                        fo.ficha = ficha
                        fo.baciaHidrografica = bacia
                        fo.contexto = contextoBacia
                        fo.save(flush: true)
                        idFichaOcorrencia = fo.id.toLong();
                        // d('  - Bacia ' + bacia.descricao + ' adicionado na ficha ')
                    } else {
                        // d('  - Bacia ' + sqBacia.toString() + ' ja existe na ficha');
                        idFichaOcorrencia = res[0].sq_ficha_ocorrencia.toLong()
                    }
                    // d(' ')
                    if (idFichaOcorrencia) {
                        if (refBibsFinal) {
                            // d(' ')
                            // d('  - Adicionar as Ref Bibs a BACIA relacionado a ocorrencia: ' + idFichaOcorrencia)
                            // a referencia bibliográfica é a mesma que estiver para o ponto
                            if (refBibsFinal[sqFicha.toString()].bacias[sqBacia.toString()]?.inserted) {
                                refBibsFinal[sqFicha.toString()].bacias[sqBacia.toString()].inserted.each { refBib ->
                                    // se existir a mesma ref bib para exclusão, então remover da lista de exclusão
                                    refBibsFinal[sqFicha.toString()].bacias[sqBacia.toString()].deleted.removeAll {
                                        it == refBib
                                    }

                                    // verificar se já existe a ref bib na bacia da ocorrencia
                                    cmdSql = """select sq_ficha_ref_bib from salve.ficha_ref_bib where sq_ficha = ${ sqFicha.toString() } and no_tabela = 'ficha_ocorrencia' and no_coluna = 'sq_bacia_hidrografica' and sq_registro = ${idFichaOcorrencia.toString()}"""
                                    if (refBib.sq_publicacao) {
                                        cmdSql += """ and sq_publicacao = ${refBib.sq_publicacao.toString()};"""
                                    } else {
                                        cmdSql += """ and no_autor = '${refBib.no_autor}' and nu_ano = ${refBib.nu_ano.toString()};"""
                                    }
                                    if (!sqlService.execSql(cmdSql)) {
                                        if (!bacia) {
                                            bacia = DadosApoio.get(sqBacia.toLong())
                                        }
                                        Map pars = [:]
                                        pars.sqFicha = sqFicha.toLong()
                                        pars.sqPublicacao = refBib.sq_publicacao ?: null
                                        pars.noTabela = 'ficha_ocorrencia'
                                        pars.sqRegistro = idFichaOcorrencia
                                        pars.noColuna = 'sq_bacia_hidrografica'
                                        pars.deRotulo = 'Bacia - ' + bacia.codigo + ' - ' + bacia.descricao
                                        pars.noAutor = refBib.no_autor ?: null
                                        pars.nuAno = refBib.nu_ano ?: null
                                        cmdSql = """INSERT INTO salve.ficha_ref_bib( sq_ficha, sq_publicacao, no_tabela, sq_registro, no_coluna, de_rotulo, no_autor, nu_ano)
                                        VALUES ( :sqFicha, :sqPublicacao, :noTabela, :sqRegistro, :noColuna, :deRotulo, :noAutor, :nuAno);"""
                                        sqlService.execSql(cmdSql, pars)
                                        // d('    - Referencia bibliografica adicionado a Bacia ' + bacia.descricao)
                                    }
                                }
                            }
                        }
                    }
                }
                // d(' - Fim inclusao das BACIAS na ficha');


                // d(' ')
                // d(' - Inicio exclusao dos BACIAS na ficha');
                item.bacias.deleted.each { sqBacia ->
                    // d('   - verificando algum ponto do Salve na Bacia');
                    cmdSql = """select o.sq_ficha_ocorrencia from salve.ficha_ocorrencia o
                                where o.sq_ficha = ${sqFicha.toString()}
                                and o.sq_contexto = ${contextoOcorrencia.id.toString()}
                                and o.ge_ocorrencia is not null
                                and ( o.sq_bacia_hidrografica = ${sqBacia.toString()} or exists (
                                    select null from salve.dados_apoio_geo as bacia
                                    where bacia.sq_dados_apoio = ${sqBacia.toString()} and ST_Intersects(ST_GeographyFromText('SRID=4674;'||ST_AsText( o.ge_ocorrencia)), bacia.ge_geo::geometry )
                                    limit 1
                                ) )
                                and ( o.in_utilizado_avaliacao = 'S' or o.in_utilizado_avaliacao is null )
                                and ( o.in_presenca_atual is null or o.in_presenca_atual != 'N' )
                                limit 1"""
                    List res = sqlService.execSql(cmdSql)
                    if (!res) {
                        // não existe ponto na Bacia
                        // d('   - Removendo Bacia ' + sqBacia.toString() + ' da ficha')
                        cmdSql = """select sq_ficha_ocorrencia from salve.ficha_ocorrencia
                                where sq_ficha = ${sqFicha.toString()}
                                and sq_contexto = ${contextoBacia.id.toString()}
                                and sq_bacia_hidrografica= ${sqBacia.toString()}
                                """
                        List rowsTemp =  sqlService.execSql(cmdSql)
                        rowsTemp.each { row ->
                            // excluir ref_bib referentes da bacia
                            // d('     - Removendo as referências bibliograficas vinculadas a Bacia ' + sqBacia.toString() + ' da ficha')
                            cmdSql="""delete from salve.ficha_ref_bib where sq_ficha = ${sqFicha.toString()}
                                        and no_tabela = 'ficha_ocorrencia'
                                        and sq_registro = ${row.sq_ficha_ocorrencia.toString()}
                                        and no_coluna = 'sq_bacia_hidrografica'"""
                            sqlService.execSql(cmdSql)
                        }

                        // d('   - verificando algum ponto do Portalbio na Bacia');
                        // verificar se tem algum ponto do portalbio utilizado na avaliacao na nemma bacia
                        cmdSql = """select o.sq_ficha_ocorrencia_portalbio
                                    from salve.ficha_ocorrencia_portalbio o
                                    where o.sq_ficha = ${sqFicha.toString()}
                                    and ( o.sq_bacia_hidrografica = ${sqBacia.toString()} OR exists (
                                        select null from salve.dados_apoio_geo bacia
                                        where bacia.sq_dados_apoio = ${sqBacia.toString()} and ST_Intersects(ST_GeographyFromText('SRID=4674;'||ST_AsText( o.ge_ocorrencia)), bacia.ge_geo::geometry )
                                        limit 1
                                    ) )
                                    and ( o.in_utilizado_avaliacao = 'S')
                                    limit 1
                                """
                        res = sqlService.execSql(cmdSql)
                        if ( ! res) {
                            rowsTemp.each { row ->
                                // d('   - Removendo Bacia ' + sqBacia.toString() + ' da ficha')
                                cmdSql = """delete from salve.ficha_ocorrencia where sq_ficha_ocorrencia = ${row.sq_ficha_ocorrencia.toString()}"""
                                sqlService.execSql(cmdSql)
                                // d('    - Bacia ' + sqBacia.toString() + ' removida da ficha ')
                            }
                        }
                    }
                }
                // d(' - Fim exclusao dos Bacias da ficha');

                //--------------------------------------------------------------------


                // UCS
                //d(' ')
                //d(' - Inicio inclusao das UCs na ficha');
                //println item.ucs
                item.ucs.inserted.each { mapData ->
                    Long sqUc               = mapData.id
                    Long sqEstado           = mapData?.sqEstado
                    String esfera           = mapData?.esfera ?: 'X'
                    Long sqFichaOcorrencia
                    Estado estadoUc                  = null
                    String inPresencaAtual  = mapData.inPresencaAtual?:null
                    if( sqEstado ) {
                        estadoUc = Estado.get(sqEstado)
                    }
                    // se a uc tambem estiver na lista para exclusao excluir da lista de exclusao
                    item.ucs.deleted.removeAll{ it.id.toLong() == sqUc.toLong() && it.esfera.toString() == esfera.toString() }
                    // ler a UC para pegar a Esfera
                    uc = Uc.findBySqUcAndInEsfera( sqUc, esfera )
                    String columnBdUc
                    String columnGormUc
                    if( uc ) {
                        if( uc.inEsfera=='F' ) {
                            columnBdUc = 'sq_uc_federal'
                            columnGormUc = 'sqUcFederal'
                        } else if( uc.inEsfera=='E' ) {
                            columnBdUc = 'sq_uc_estadual'
                            columnGormUc = 'sqUcEstadual'
                        } else if( uc.inEsfera=='R' ) {
                            columnBdUc = 'sq_rppn'
                            columnGormUc = 'sqRppn'
                        }

                        // verificar se existe pelo menos 1 registro do contexto OCORRENCIA na mesma Uc
                        cmdSql = """select sq_ficha_ocorrencia, in_utilizado_avaliacao from salve.ficha_ocorrencia
                                where sq_ficha = ${sqFicha.toString()}
                                and sq_contexto = ${contextoOcorrencia.id.toString()}
                                and ${columnBdUc} = ${sqUc.toString()}
                                limit 1
                                """
                        List res = sqlService.execSql(cmdSql)
                        if (!res) {
                            ficha = Ficha.get(sqFicha.toLong())
                            FichaOcorrencia fo = new FichaOcorrencia()
                            fo.geoValidate=false // não validar estado, municipio com a coodendada informada
                            fo.ficha        = ficha
                            fo.contexto     = contextoOcorrencia
                            fo.inUtilizadoAvaliacao ='S'
                            fo.inPresencaAtual = inPresencaAtual
                            fo[columnGormUc]= uc.sqUc // UC/RPPN
                            if( estadoUc ) {
                                fo.estado = estadoUc
                            }
                            fo.save( flush:true);
                            sqFichaOcorrencia = fo.id
                            // d('  - UC ' + uc.sgUnidade+ ' adicionado na ficha ')
                        } else {
                            sqFichaOcorrencia = res[0].sq_ficha_ocorrencia.toLong()
                            // d('  - Uc ' + uc.sgUnidade + ' ja existe na ficha');
                            // d('    - Registro atualizado para UTILIZADO NA AVALIACAO');
                            cmdSql="""update salve.ficha_ocorrencia set in_utilizado_avaliacao='S'
                                        ,in_presenca_atual = ${ inPresencaAtual ? "'"+inPresencaAtual+"'" : 'null'}
                                        ${ estadoUc ? ',sq_estado = '+estadoUc.id.toString() : '' }
                                        where sq_ficha = ${sqFicha.toString()}
                                        and sq_contexto = ${contextoOcorrencia.id.toString()}
                                        and ${columnBdUc} = ${sqUc.toString()}
                                        and ge_ocorrencia is null
                                        """
                            sqlService.execSql(cmdSql)
                        }

                        // inserir a ref bib da UC se não existir
                        // a referencia bibliográfica é a mesma que estiver para o ponto
                        if (refBibsFinal) {
                             /*d(' ')
                             d('  - Adicionar as Ref Bibs na UC relacionado a ocorrencia: ' + sqFichaOcorrencia)
                             println mapData
                             println 'REFBIBSFINAL'
                             println refBibsFinal*/

                            // a referencia bibliográfica é a mesma que estiver para o ponto
                            String ucIdEsfera = sqUc.toString()+'|'+esfera
                            if (refBibsFinal[sqFicha.toString()].ucs[ucIdEsfera]?.inserted) {
                                refBibsFinal[sqFicha.toString()].ucs[ucIdEsfera].inserted.each { refBib ->
                                    // se existir a mesma ref bib para exclusão, então remover da lista de exclusão
                                    refBibsFinal[sqFicha.toString()].ucs[ucIdEsfera].deleted.removeAll {
                                        it == refBib
                                    }
                                    // verificar se já existe a ref bib na uc da ocorrencia
                                    cmdSql = """select sq_ficha_ref_bib from salve.ficha_ref_bib where sq_ficha = ${ sqFicha.toString() } and no_tabela = 'ficha_ocorrencia' and no_coluna = 'sq_ficha_ocorrencia' and sq_registro = ${sqFichaOcorrencia.toString()}"""
                                   if (refBib.sq_publicacao) {
                                        cmdSql += """ and sq_publicacao = ${refBib.sq_publicacao.toString()};"""
                                    } else {
                                        cmdSql += """ and no_autor = '${refBib.no_autor}' and nu_ano = ${refBib.nu_ano.toString()};"""
                                    }
                                    if (!sqlService.execSql(cmdSql)) {
                                        Map pars = [:]
                                        pars.sqFicha = sqFicha.toLong()
                                        pars.sqPublicacao = refBib.sq_publicacao ?: null
                                        pars.noTabela = 'ficha_ocorrencia'
                                        pars.sqRegistro = sqFichaOcorrencia
                                        pars.noColuna = 'sq_ficha_ocorrencia'
                                        pars.deRotulo = ( uc.sgUnidade ?: uc.noUc )
                                        pars.noAutor = refBib.no_autor ?: null
                                        pars.nuAno = refBib.nu_ano ?: null
                                        cmdSql = """INSERT INTO salve.ficha_ref_bib( sq_ficha, sq_publicacao, no_tabela, sq_registro, no_coluna, de_rotulo, no_autor, nu_ano)
                                        VALUES ( :sqFicha, :sqPublicacao, :noTabela, :sqRegistro, :noColuna, :deRotulo, :noAutor, :nuAno);"""
                                        sqlService.execSql(cmdSql, pars)
                                        // d('    - Referencia bibliografica adicionado a UC ' + uc.sgUnidade)
                                    }
                                }
                            }
                        }
                        uc=null
                    }
                }
                // d(' - Fim inclusao das UCs na ficha');


                // d(' ')
                // d(' - Inicio exclusao dos UCs na ficha')
                /**
                 * a exclusão de UCs da ficha é válida somente para quando um
                 * ponto do PORTALBIO é excluído ou não utilizado porque os pontos
                 * do salve a uc fica no prório registro que já foi excluido ou inutilizado
                 */
                item.ucs.deleted.each { ucData ->
                    // ler a UC para pegar a Esfera
                    uc = Uc.findBySqUcAndInEsfera( ucData.id.toLong(), ucData.esfera )

                    if( uc ) {
                        // d('   - Verificando se aUC ' + uc.sgUnidade + ' devera ser excluida')
                        String view = 'salve.vw_unidade_org';
                        String colunaIdSalve = 'sq_uc_federal'
                        String colunaIdUc = 'uc.sq_pessoa'
                        if (uc.inEsfera == 'E') {
                            view = 'geo.vw_mma_uc_nao_federais'
                            colunaIdSalve = 'sq_uc_estadual'
                            colunaIdUc = 'uc.gid'
                        } else if (uc.inEsfera == 'R') {
                            view = 'geo.vw_icmbio_rppn'
                            colunaIdSalve = 'sq_rppn'
                            colunaIdUc = 'uc.ogc_fid'
                        }

                        // agora como um ponto do salve pode gerar mais de uma uc
                        // ( ex:serra dos orgaos) tem que verificar se tem algum outro ponto que
                        // está na mesma UC
                        cmdSql = """
                            select fo.sq_ficha_ocorrencia
                            from salve.ficha_ocorrencia fo
                            inner join ${view} as uc on ST_Intersects(ST_GeographyFromText('SRID=4674;'||ST_AsText(fo.ge_ocorrencia)), uc.the_geom::geometry )
                            where fo.sq_ficha = ${sqFicha.toString()}
                            and ${colunaIdUc} = ${ucData.id.toString()}
                            and ( fo.in_utilizado_avaliacao = 'S' or fo.in_utilizado_avaliacao is null )
                            and ( fo.in_presenca_atual is null or fo.in_presenca_atual != 'N' )
                            and fo.sq_ficha_ocorrencia not in ( ${item.ucs.idsDeleted.join(',')} )
                            limit 1
                            """
                        List rowsOcorrenciasSalveNaMesmaUc = sqlService.execSql(cmdSql)
                        if ( ! rowsOcorrenciasSalveNaMesmaUc  ) {
                            // excluir a ref bib da UC que o ponto adicionou
                            String ucIdEsfera = ucData.id.toString()+'|'+ucData.esfera
                            if( refBibsFinal[sqFicha.toString()].ucs[ucIdEsfera] ) {
                                refBibsFinal[sqFicha.toString()].ucs[ucIdEsfera].deleted.each { refBib ->

                                    // tem que ter a condicao and ge_ocorrencia is null senão mata as referencias bibliograficas do ponto
                                    cmdSql = """select sq_ficha_ocorrencia from salve.ficha_ocorrencia where sq_ficha = ${sqFicha.toString()}
                                    and sq_contexto = ${contextoOcorrencia.id.toString()}
                                    and ge_ocorrencia is null
                                    and ${colunaIdSalve} = ${ucData.id.toString()}"""

                                    sqlService.execSql(cmdSql).each { row ->
                                        cmdSql = """delete from salve.ficha_ref_bib where sq_ficha = ${ sqFicha.toString() } and no_tabela = 'ficha_ocorrencia' and no_coluna = 'sq_ficha_ocorrencia' and sq_registro = ${row.sq_ficha_ocorrencia.toString()}"""
                                        if (refBib.sq_publicacao) {
                                            cmdSql += """ and sq_publicacao = ${refBib.sq_publicacao.toString()};"""
                                        } else {
                                            cmdSql += """ and no_autor = '${refBib.no_autor}' and nu_ano = ${refBib.nu_ano.toString()};"""
                                        }
                                        sqlService.execSql(cmdSql)
                                    }
                                }
                            }
                            // verificar se existe alguma outro ponto do portalbio na mesma uc
                            // Regra: contar total de pontos e quanto pontos Utilizados e não utilizados do portalbio existem nesta uc
                            // se não tiver ponto, exluir a ocorrencia
                            // se tiver pelo menos um utilizado, atualizar a ocorrencia para utilizada na avaliacao
                            // senao atualizar a ocorrencia para não utilizada na avaliação
                            cmdSql = """select sum( case when coalesce( opb.in_utilizado_avaliacao,'N') = 'S' then 1 else 0 end ) as nu_utilizados, sum( case when coalesce( opb.in_utilizado_avaliacao,'N') <> 'S' then 1 else 0 end ) as nu_nao_utilizados, sum(1) as nu_pontos
                            from salve.ficha_ocorrencia_portalbio opb
                            inner join ${view} as uc on ST_Intersects(ST_GeographyFromText('SRID=4674;'||ST_AsText(opb.ge_ocorrencia)), uc.the_geom::geometry )
                            where opb.sq_ficha = ${sqFicha.toString()}
                            and ${colunaIdUc} = ${ucData.id.toString()}
                            and opb.sq_ficha_ocorrencia_portalbio not in ( ${item.ucs.idsDeleted.join(',')} )
                            """
                            // d(' ')
                            // d(' - Verificar quantos pontos do PORTALBIO existem na UC')
                            sqlService.execSql(cmdSql).each { row ->
                                if (!row.nu_pontos || row.nu_pontos.toInteger() == 0) {
                                    // d('   - nenhum ponto encontrado')
                                    cmdSql = """select sq_ficha_ocorrencia from salve.ficha_ocorrencia
                                    where sq_ficha = ${sqFicha.toString()}
                                    and sq_contexto = ${contextoOcorrencia.id.toString()}
                                    and ${colunaIdSalve} = ${ucData.id.toString()}
                                    and ge_ocorrencia is null limit 5
                                    """
                                    sqlService.execSql(cmdSql).each {
                                        // d('   - Excluindo UC ' + uc.sgUnidade)
                                        // excluir a ref bib primeiro
                                        cmdSql = """delete from salve.ficha_ref_bib where sq_ficha = ${ sqFicha.toString() } and no_tabela='ficha_ocorrencia'
                                                    and sq_registro = ${it.sq_ficha_ocorrencia.toString()}"""
                                        sqlService.execSql(cmdSql)
                                        // d('   - Ref Bibs foram excluidas do ponto')
                                        // excluir a ocorrencia
                                        cmdSql = """delete from salve.ficha_ocorrencia where sq_ficha_ocorrencia=${it.sq_ficha_ocorrencia.toString() + ' and ge_ocorrencia is null'}"""
                                        sqlService.execSql(cmdSql)
                                        // d('   - Uc foi excluida da ficha')
                                    }
                                } else {
                                    // d('   - ' + row.nu_pontos + ' encontrado(s)')
                                    // d('   - ' + row.nu_utilizados + ' utilizado(s)')
                                    // d('   - ' + row.nu_nao_utilizados + ' NAO utilizado(s)')
                                    String inUtilizado = 'S';
                                    if (!row.nu_utilizados || row.nu_utilizados.toInteger() == 0) {
                                        inUtilizado = 'N'
                                    }
                                    cmdSql = """update salve.ficha_ocorrencia set in_utilizado_avaliacao='${inUtilizado}'
                                            where sq_ficha = ${sqFicha.toString()}
                                            and sq_contexto = ${contextoOcorrencia.id.toString()}
                                            and ${colunaIdSalve} = ${ucData.id.toString()}
                                            and ge_ocorrencia is null
                                            """
                                    sqlService.execSql(cmdSql)
                                    // d('    - Registro atualizado para UTILIZADO NA AVALIACAO=' + inUtilizado);
                                }
                            }
                        }
//                        // remover a referencia bibliografica do ponto não utilizado da uc
//                        item.ucs.idsDeleted.each { id ->
//                            cmdSql = """delete from salve.ficha_ref_bib where no_tabela='ficha_ocorrencia'
//                                  and sq_registro=${id.toString()}"""
//                            sqlService.execSql(cmdSql)
//                        }

                    }
                } //ucData
                // d(' - Fim exclusao das UCs da ficha');
            }
            // d(' ')
            // atualizar o percentual preenchimento da ficha
            PessoaFisica pf = PessoaFisica.get( currentSession.sicae.user.sqPessoa )
            if( pf ) {
                List sqlUpdates=[]
                data.eachWithIndex { sqFicha, listPontos, index ->
                    // se a ficha tiver com o campo Observações Gerais sobre presença em UC preenchido na aba 7.4 e não tiver mais
                    // ucs nos pontos, então limpar o campo da observação.
                    //verificar se tem pelo menos um ponto com Presença Atual=SIM e com a coluna sq_uc_federal preenchida
                    cmdSql = """UPDATE salve.ficha set ds_presenca_uc = null where ficha.sq_ficha = ( SELECT a.sq_ficha
                                FROM salve.ficha_ocorrencia a
                                inner join salve.ficha on ficha.sq_ficha=a.sq_ficha
                                where a.sq_ficha = ${sqFicha}
                            and a.in_presenca_atual = 'S'
                            and ( a.sq_uc_federal is not null OR a.sq_uc_estadual is not null or a.sq_rppn is not null )
                            and ficha.ds_presenca_uc = 'Não foram encontradas informações para o táxon.'
                            limit 1)"""
                    sqlUpdates.push( cmdSql)

                    this.updatePercentualPreenchimento(sqFicha.toLong(),pf, saveUserLog )
                    // se for uma subespecie, atualizar o percentual da especie
                    this.updatePercentualEspecie(sqFicha.toLong(), pf, saveUserLog)
                }
                if( sqlUpdates ) {
                    sqlService.execSql(sqlUpdates.join(';\n') )
                }
            }

        } catch (Exception e) {
            println e.getMessage()
            // e.printStackTrace()
            erros.push( e.getMessage() )
        }
        //d('FIM atualizacao: Estados, municipios, biomas, bacias e Ucs.')
        try {
            currentSession.removeAttribute('gravarUtilizadoAvaliacao')
        } catch( Exception e ) {}

        if( erros ) {
            workerService.setError(worker, erros.join('<br>') )
        }
    } // session utilizado

       /**
     * data.copiarPendencias
     * data.ids
     * data.sqCicloDestino
     * data.dsCicloDestino
     * @param data
     */
    private void _asyncCopiarFichas(Map data = [:], GrailsHttpSession appSession ) {
        List erros = []
        List fichasNaoCopiadas = []
        Map worker
        boolean isPrimeiroCiclo=false

        println ' '
        println 'Copiar fichas para outro ciclo'
        println '  - _asyncCopiarFichas iniciado em ' + new Date().format('dd/MM/yyyy HH:mm:ss')
        println '  - usuario: ' + appSession.sicae.user.noUsuario

        try {
            // registrar na sessão o worker do processo
            worker = workerService.add(appSession
                , 'Copiando ' + data.ids.size() + ' ficha(s) para ' + data?.dsCicloDestino + '.'
                , data.ids.size() + 1 // add 1 para completar 100% só depois do passo de compactacao
                , "app.alertInfo('Copia da(s) ficha(s) finalizada!');")

            // inicializar as variaveis do worker
            workerService.start(worker)

            // declarar variáveis utilizadas
            CicloAvaliacao cicloDestino = CicloAvaliacao.get(data.sqCicloDestino)
            if (!cicloDestino) {
                throw new RuntimeException('Ciclo avaliação inexistente!')
            }
            PessoaFisica pessoaFisica = PessoaFisica.get(appSession.sicae.user.sqPessoa)
            if ( ! pessoaFisica ) {
                throw new RuntimeException('Pessoa Física não encontrada')
            }
            //FichaService fichaService =  new FichaService(appSession) // para atualizar o percentual de preenchimento da ficha apos a importação
            Ficha fichaOrigem
            Ficha fichaDestino
            FichaPendencia fichaPendenciaDestino
            FichaNomeComum fichaNomeComumDestino
            FichaSinonimia fichaSinonimaDestino
            FichaAnexo fichaAnexoDestino
            FichaOcorrencia fichaOcorrenciaDestino
            FichaOcorrenciaPortalbio fichaOcorrenciaPortalbioDestino
            FichaAmbienteRestrito fichaAmbienteRestritoDestino
            FichaAreaRelevancia fichaAreaRelevanciaDestino
            FichaAreaRelevanciaMunicipio fichaAreaRelevanciaMunicipioDestino
            FichaHabitoAlimentar fichaHabitoAlimentarDestino
            FichaHabitoAlimentarEsp fichaHabitoAlimentarEspDestino
            FichaHabitat fichaHabitatDestino
            FichaInteracao fichaInteracaoDestino
            FichaVariacaoSazonal fichaVariacaoSazonalDestino
            FichaPopulEstimadaLocal fichaPopulEstimadaLocalDestino
            FichaAreaVida fichaAreaVidaDestino
            FichaAmeaca fichaAmeacaDestino
            FichaAmeacaRegiao fichaAmeacaRegiaoDestino
            FichaAmeacaEfeito fichaAmeacaEfeitoDestino
            FichaUso fichaUsoDestino
            FichaUsoRegiao fichaUsoRegiaoDestino
            TaxonHistoricoAvaliacao taxonHistoricoAvaliacaoDestino
            FichaListaConvencao fichaListaConvencaoDestino
            FichaAcaoConservacao fichaAcaoConservacaoDestino
            FichaPesquisa fichaPesquisaDestino
            FichaDeclinioPopulacional fichaDeclinioPopulacionalDestino
            FichaMudancaCategoria fichaMudancaCategoriaDestino
            FichaAoo fichaAooDestino
            //-----------------
            FichaOcorrenciaPortalbioReg fichaOcorrenciaPortalbioRegDestino
            FichaOcorrenciaRegistro fichaOcorrenciaRegistroDestino
            FichaUf fichaUfDestino
            FichaBioma fichaBiomaDestino
            FichaBacia fichaBaciaDestino
            FichaHistSituacaoExcluida fichaHistSituacaoExcluidaDestino

            // módulo traducao
            TraducaoRevisao traducaoRevisaoDestino

            DadosApoio tipoAvaliacaoNacional    = dadosApoioService.getByCodigo('TB_TIPO_AVALIACAO', 'NACIONAL_BRASIL')
            DadosApoio tipoAvaliacaoEstadual    = dadosApoioService.getByCodigo('TB_TIPO_AVALIACAO', 'ESTADUAL')
            DadosApoio tipoAvaliacaoRegional    = dadosApoioService.getByCodigo('TB_TIPO_AVALIACAO', 'REGIONAL')
            DadosApoio situacaoCompilacao       = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'COMPILACAO')

            if (!tipoAvaliacaoNacional) {
                throw new RuntimeException('Tipo avaliação NACIONAL_BRASIL não cadastrada na tabela de apoio TB_TIPO_AVALIACAO!')
            }
            if (!tipoAvaliacaoEstadual) {
                throw new RuntimeException('Tipo avaliação ESTADUAL não cadastrada na tabela de apoio TB_TIPO_AVALIACAO!')
            }
            if (!tipoAvaliacaoRegional) {
                throw new RuntimeException('Tipo avaliação REGIONAL não cadastrada na tabela de apoio TB_TIPO_AVALIACAO!')
            }
            if (!situacaoCompilacao) {
                throw new RuntimeException('Situação da ficha COMPILACAO não cadastrada na tabela de apoio TB_SITUACAO_FICHA!')
            }

            // inicia o loop das fichas
            data.ids.each { idFicha ->
                try {
                    // inicio codigo de copia da ficha
                    fichaOrigem = Ficha.get(idFicha.toLong())

                    // não copiar as fichas excluidas
                    if( fichaOrigem && ! data.copiarExcluidas && fichaOrigem.situacaoFicha.codigoSistema == 'EXCLUIDA' ) {
                        fichaOrigem = null
                    }
                    if( fichaOrigem ) {
                        if (fichaOrigem.situacaoFicha.codigoSistema != 'EXCLUIDA') {
                            // no 1º ciclo nao tem a aba 11.6
                            if (fichaOrigem.cicloAvaliacao.nuAno < 2016) {
                                if (!fichaOrigem.categoriaFinal || fichaOrigem.categoriaFinal.codigoSistema == 'NE') {
                                    isPrimeiroCiclo = true
                                    fichasNaoCopiadas.push(fichaOrigem.nmCientifico)
                                    fichaOrigem = null
                                }
                            } else {
                                if (!fichaOrigem.categoriaIucn || !fichaOrigem.categoriaFinal
                                    || fichaOrigem.categoriaIucn.codigoSistema == 'NE'
                                    || fichaOrigem.categoriaFinal.codigoSistema == 'NE'
                                ) {
                                    fichasNaoCopiadas.push(fichaOrigem.nmCientifico)
                                    fichaOrigem = null
                                }
                            }
                        }
                    }

                    //println '1) lendo ficha origem ' + fichaOrigem.nmCientifico
                    if (fichaOrigem) {

                        // atualizar title do worker
                        workerService.setTitle(worker, fichaOrigem.nmCientifico )

                        // copiar somente se não existir a ficha no ciclo destino
                        fichaDestino = Ficha.findByTaxonAndCicloAvaliacao(fichaOrigem.taxon, cicloDestino)

                        if ( ! fichaDestino ) {
                            //println '2) criando ficha destino em branco'
                            fichaDestino = new Ficha(fichaOrigem.properties)
                            fichaDestino.fichaCicloAnterior = fichaOrigem
                            fichaDestino.pendencias = null
                            fichaDestino.cicloAvaliacao = cicloDestino
                            fichaDestino.usuario = pessoaFisica
                            fichaDestino.situacaoFicha = situacaoCompilacao

                            // limpar campos que não serão copiados
                            fichaDestino.categoriaFinal = null
                            fichaDestino.dsCriterioAvalIucnFinal = null
                            fichaDestino.dsJustificativaFinal = null
                            fichaDestino.stPossivelmenteExtintaFinal = null
                            fichaDestino.dtPublicacao = null
                            fichaDestino.publicadoPor = null
                            fichaDestino.dsDoi=null
                            fichaDestino.dsCitacao=null

                            //-------------------------------
                            // limpar dados das oficinas - aba 11.6
                            fichaDestino.categoriaIucn = null
                            fichaDestino.dsCriterioAvalIucn = null
                            fichaDestino.stPossivelmenteExtinta = null
                            fichaDestino.dsJustificativa = null
                            fichaDestino.stManterLc = null
                            fichaDestino.stTransferida = null
                            fichaDestino.vlPercentualPreenchimento = 0
                            fichaDestino.save()
                            //println '3) salvando ficha destino preenchida.'


                            // COPIAR REFERENCIAS BIBLIOGRÁFICAS
                            _copiarRefBib(fichaOrigem, fichaDestino, 'ficha', fichaOrigem.id, fichaDestino.id)

                            // COPIAR AS REF. BIBS. do HISTORICO DA AVALIAÇÕES DEVIDO AOS TAXONS SEREM OS MESMOS
                            FichaRefBib.executeQuery("select distinct new map(a.sqRegistro as sqRegistro) from FichaRefBib a where a.ficha.id = :sqFichaOrigem and a.noTabela='taxon_historico_avaliacao'"
                                    ,[sqFichaOrigem:fichaOrigem.id]).each {
                                _copiarRefBib( fichaOrigem, fichaDestino, 'taxon_historico_avaliacao', it.sqRegistro, it.sqRegistro )
                            }

                            /*FichaRefBib.findAllByFichaAndNoTabela(fichaOrigem, 'taxon_historico_avaliacao').each {
                                _copiarRefBib( fichaOrigem, fichaDestino, 'taxon_historico_avaliacao', it.sqRegistro, it.sqRegistro )
                            }
                            */

                            /*FichaRefBib.findByFichaAndNoTabela(fichaOrigem, 'taxon_historico_avaliacao').each {
                                println 'Copiar ficha_ref_bib ficha: '+ fichaOrigem.id + ' sq_registro: ' + it.sqRegistro
                                _copiarRefBib( fichaOrigem, fichaDestino, 'taxon_historico_avaliacao', it.sqRegistro, it.sqRegistro )
                            }
                             */


                            // 1) COPIAR AS PENDENCIAS
                            if (data.copiarPendencias) {
                                FichaPendencia.findAllByFicha(fichaOrigem).each {
                                    fichaPendenciaDestino = new FichaPendencia(it.properties)
                                    fichaPendenciaDestino.ficha = fichaDestino
                                    fichaPendenciaDestino.save()
                                } // fim each pendencias
                                //println '5) pendencias copiadas.'
                            }

                            // 2) NOMES COMUNS
                            FichaNomeComum.findAllByFicha(fichaOrigem).each {
                                fichaNomeComumDestino = new FichaNomeComum(it.properties)
                                fichaNomeComumDestino.ficha = fichaDestino
                                fichaNomeComumDestino.save()
                                _copiarRefBib(fichaOrigem, fichaDestino, 'ficha_nome_comum', it.id, fichaNomeComumDestino.id)
                            } // fim each nome comum
                            //println '6) nomes comuns copiados.'

                            /**/
                            // 3) SINONÍMIAS
                            FichaSinonimia.findAllByFicha(fichaOrigem).each {
                                fichaSinonimaDestino = new FichaSinonimia(it.properties)
                                fichaSinonimaDestino.ficha = fichaDestino
                                fichaSinonimaDestino.save()
                                _copiarRefBib(fichaOrigem, fichaDestino, 'ficha_sinonimia', it.id, fichaSinonimaDestino.id)
                            } // fim each nome comum
                            /**/
                            //println '7) sinonimias copiadas.'

                            /**/
                            // 4) Tabela de Anexos vai direto não importa o contexto
                            FichaAnexo.findAllByFicha(fichaOrigem).each {
                                // o mapa de distribuição não é copiado para o ciclo seguinte
                                if (it?.contexto?.codigoSistema != 'MAPA_DISTRIBUICAO' || it.inPrincipal != 'S') {
                                    fichaAnexoDestino = new FichaAnexo(it.properties)
                                    fichaAnexoDestino.ficha = fichaDestino
                                    fichaAnexoDestino.save()
                                }
                            }
                            /**/
                            //println '8) anexos copiados.'

                            /**/
                            // Tabela ficha_aoo
                            FichaAoo.findAllByFicha( fichaOrigem ).each {
                                fichaAooDestino = new FichaAoo( it.properties )
                                fichaAooDestino.ficha = fichaDestino
                                fichaAooDestino.save()
                            } // fim each ficha aoo

                            // 5) Tabela de Ocorrencias vai direto não importa o contexto
                            FichaOcorrencia.findAllByFicha(fichaOrigem).each {
                                fichaOcorrenciaDestino = new FichaOcorrencia(it.properties)
                                fichaOcorrenciaDestino.geoValidate = false
                                fichaOcorrenciaDestino.ficha = fichaDestino
                                fichaOcorrenciaDestino.save()
                                _copiarRefBib(fichaOrigem, fichaDestino, 'ficha_ocorrencia', it.id, fichaOcorrenciaDestino.id)

                                // copiar a tabela ficha_ocorrencia_registro
                                FichaOcorrenciaRegistro.findAllByFichaOcorrencia(it).each { it2 ->
                                    fichaOcorrenciaRegistroDestino = new FichaOcorrenciaRegistro(it2.properties)
                                    fichaOcorrenciaRegistroDestino.fichaOcorrencia = fichaOcorrenciaDestino
                                    fichaOcorrenciaRegistroDestino.save()
                                }
                            } // fim FichaOcorrencia

                            // copiar as ocorrencias do portalbio
                            FichaOcorrenciaPortalbio.findAllByFicha(fichaOrigem).each {
                                try {
                                    fichaOcorrenciaPortalbioDestino = new FichaOcorrenciaPortalbio(it.properties)
                                    fichaOcorrenciaPortalbioDestino.ficha = fichaDestino
                                    fichaOcorrenciaPortalbioDestino.save()
                                    if( fichaOcorrenciaPortalbioDestino.id) {
                                        _copiarRefBib(fichaOrigem, fichaDestino, 'ficha_ocorrencia_portalbio', it.id, fichaOcorrenciaPortalbioDestino.id)
                                        // copiar a tabela ficha_ocorrencia_portalbio_reg
                                        FichaOcorrenciaPortalbioReg.findAllByFichaOcorrenciaPortalbio(it).each { it2 ->
                                            fichaOcorrenciaPortalbioRegDestino = new FichaOcorrenciaPortalbioReg(it2.properties)
                                            fichaOcorrenciaPortalbioRegDestino.fichaOcorrenciaPortalbio = fichaOcorrenciaPortalbioDestino
                                            fichaOcorrenciaPortalbioRegDestino.save()
                                        }
                                    }
                                } catch( Exception ) {

                                }
                            } // fim FichaOcorrenciaPortalbio

                            // copiar a tabela fichaUf
                             FichaUf.findAllByFicha(fichaOrigem).each {
                                 fichaUfDestino = new FichaUf(it.properties)
                                 fichaUfDestino.ficha = fichaDestino
                                 fichaUfDestino.estado = it.estado
                                 fichaUfDestino.save()
                                 _copiarRefBib(fichaOrigem, fichaDestino, 'ficha_uf', it.id, fichaUfDestino.id )
                            } // fim each ficha uf

                            // copiar a tabela fichaBioma
                            FichaBioma.findAllByFicha(fichaOrigem).each {
                                fichaBiomaDestino = new FichaBioma(it.properties)
                                fichaBiomaDestino.ficha = fichaDestino
                                fichaBiomaDestino.save()
                                _copiarRefBib(fichaOrigem, fichaDestino, 'ficha_bioma', it.id, fichaBiomaDestino.id )
                            } // fim each ficha uf

                            // copiar a tabela fichaBacia
                            FichaBacia.findAllByFicha(fichaOrigem).each {
                                fichaBaciaDestino = new FichaBacia(it.properties)
                                fichaBaciaDestino.ficha = fichaDestino
                                fichaBaciaDestino.save()
                                _copiarRefBib(fichaOrigem, fichaDestino, 'ficha_bacia', it.id, fichaBaciaDestino.id )
                            } // fim each ficha uf


                            /**/
                            //println '9) ocorrencias copiadas.'

                            // ABA DISTRIBUICAO
                            /**/
                            //ambiente restrito
                            FichaAmbienteRestrito.findAllByFicha(fichaOrigem).each {
                                fichaAmbienteRestritoDestino = new FichaAmbienteRestrito(it.properties)
                                fichaAmbienteRestritoDestino.ficha = fichaDestino
                                //fichaAmbienteRestritoDestino.validate()
                                fichaAmbienteRestritoDestino.save()
                                _copiarRefBib(fichaOrigem, fichaDestino, 'ficha_ambiente_restrito', it.id, fichaAmbienteRestritoDestino.id)
                            }
                            /**/
                            //println '10) ambientes restritos copiados.'

                            //areas relevantes
                            /**/
                            FichaAreaRelevancia.findAllByFicha(fichaOrigem).each {
                                fichaAreaRelevanciaDestino = new FichaAreaRelevancia(it.properties)
                                fichaAreaRelevanciaDestino.ficha = fichaDestino
                                //fichaAreaRelevanciaDestino.validate()
                                fichaAreaRelevanciaDestino.save()
                                _copiarRefBib(fichaOrigem, fichaDestino, 'ficha_area_relevancia', it.id, fichaAreaRelevanciaDestino.id)
                                FichaAreaRelevanciaMunicipio.findAllByFichaAreaRelevancia(it).each { it2 ->
                                    fichaAreaRelevanciaMunicipioDestino = new FichaAreaRelevanciaMunicipio(it2.properties)
                                    fichaAreaRelevanciaMunicipioDestino.fichaAreaRelevancia = fichaAreaRelevanciaDestino
                                    fichaAreaRelevanciaMunicipioDestino.save()
                                }
                            }
                            /**/
                            //println '11) areas relevantes copiadas.'

                            // História Natural
                            /**/
                            FichaHabitoAlimentar.findAllByFicha(fichaOrigem).each {
                                fichaHabitoAlimentarDestino = new FichaHabitoAlimentar(it.properties)
                                fichaHabitoAlimentarDestino.ficha = fichaDestino
                                fichaHabitoAlimentarDestino.save()
                                _copiarRefBib(fichaOrigem, fichaDestino, 'ficha_habito_alimentar', it.id, fichaHabitoAlimentarDestino.id)
                            }
                            //println '12) habitos alimentares copiados.'

                            FichaHabitoAlimentarEsp.findAllByFicha(fichaOrigem).each {
                                fichaHabitoAlimentarEspDestino = new FichaHabitoAlimentarEsp(it.properties)
                                fichaHabitoAlimentarEspDestino.ficha = fichaDestino
                                //fichaHabitoAlimentarEspDestino.validate()
                                fichaHabitoAlimentarEspDestino.save()
                                _copiarRefBib(fichaOrigem, fichaDestino, 'ficha_habito_alimentar_esp', it.id, fichaHabitoAlimentarEspDestino.id)
                            }
                            /**/
                            //println '13) habitos alimentares especialistas copiados.'

                            // habitat
                            /**/
                            FichaHabitat.findAllByFicha(fichaOrigem).each {
                                fichaHabitatDestino = new FichaHabitat(it.properties)
                                fichaHabitatDestino.ficha = fichaDestino
                                //fichaHabitatDestino.validate()
                                fichaHabitatDestino.save()
                                _copiarRefBib(fichaOrigem, fichaDestino, 'ficha_habitat', it.id, fichaHabitatDestino.id)
                            }
                            /**/
                            //println '14) habitas copiados.'

                            // interação
                            /**/
                            FichaInteracao.findAllByFicha(fichaOrigem).each {
                                fichaInteracaoDestino = new FichaInteracao(it.properties)
                                fichaInteracaoDestino.ficha = fichaDestino
                                //fichaInteracaoDestino.validate()
                                fichaInteracaoDestino.save()
                                _copiarRefBib(fichaOrigem, fichaDestino, 'ficha_interacao', it.id, fichaInteracaoDestino.id)
                            }
                            /**/
                            //println '15) interacoes copiadas.'

                            // Sasonalidade
                            /**/
                            FichaVariacaoSazonal.findAllByFicha(fichaOrigem).each {
                                fichaVariacaoSazonalDestino = new FichaVariacaoSazonal(it.properties)
                                fichaVariacaoSazonalDestino.ficha = fichaDestino
                                //fichaVariacaoSazonalDestino.validate()
                                fichaVariacaoSazonalDestino.save()
                                _copiarRefBib(fichaOrigem, fichaDestino, 'ficha_variacao_sazonal', it.id, fichaVariacaoSazonalDestino.id)
                            }
                            /**/
                            //println '16) sasonalidades copiadas.'

                            // População Conhecida/Estimada
                            /**/
                            FichaPopulEstimadaLocal.findAllByFicha(fichaOrigem).each {
                                fichaPopulEstimadaLocalDestino = new FichaPopulEstimadaLocal(it.properties)
                                fichaPopulEstimadaLocalDestino.ficha = fichaDestino
                                //fichaPopulEstimadaLocalDestino.validate()
                                fichaPopulEstimadaLocalDestino.save()
                                _copiarRefBib(fichaOrigem, fichaDestino, 'ficha_popul_estimada_local', it.id, fichaPopulEstimadaLocalDestino.id)
                            }
                            /**/
                            //println '16) Populacoes copiadas.'

                            // Area de Vida
                            /**/
                            FichaAreaVida.findAllByFicha(fichaOrigem).each {
                                fichaAreaVidaDestino = new FichaAreaVida(it.properties)
                                fichaAreaVidaDestino.ficha = fichaDestino
                                //fichaAreaVidaDestino.validate()
                                fichaAreaVidaDestino.save()
                                _copiarRefBib(fichaOrigem, fichaDestino, 'ficha_area_vida', it.id, fichaAreaVidaDestino.id)
                            }
                            /**/
                            //println '17) Areas de vida copiadas.'

                            // Ameaças
                            /**/
                            FichaAmeaca.findAllByFicha(fichaOrigem).each {
                                fichaAmeacaDestino = new FichaAmeaca(it.properties)
                                fichaAmeacaDestino.ficha = fichaDestino

                                fichaAmeacaDestino.fichaAmeacaEfeitos=null
                                //fichaAmeacaDestino.validate()
                                fichaAmeacaDestino.save()
                                _copiarRefBib(fichaOrigem, fichaDestino, 'ficha_ameaca', it.id, fichaAmeacaDestino.id)
                                FichaAmeacaRegiao.findAllByFichaAmeaca(it).each { it2 ->
                                    // duplicar a tabela ficha_ocorrencia referente, aqui pega as regiões e os georeferenciamento
                                    fichaOcorrenciaDestino = new FichaOcorrencia(it2.fichaOcorrencia.properties)
                                    fichaOcorrenciaDestino.geoValidate = false
                                    fichaOcorrenciaDestino.ficha = fichaDestino
                                    fichaOcorrenciaDestino.save() // gerar nova ocorrencia
                                    fichaAmeacaRegiaoDestino = new FichaAmeacaRegiao(it2.properties)
                                    fichaAmeacaRegiaoDestino.fichaAmeaca = fichaAmeacaDestino
                                    fichaAmeacaRegiaoDestino.fichaOcorrencia = fichaOcorrenciaDestino
                                    fichaAmeacaRegiaoDestino.save()
                                }

                                // copiar efeitos da ameaça
                                FichaAmeacaEfeito.findAllByFichaAmeaca( it ).each { it2 ->
                                    fichaAmeacaEfeitoDestino = new FichaAmeacaEfeito()
                                    fichaAmeacaEfeitoDestino.fichaAmeaca = fichaAmeacaDestino
                                    fichaAmeacaEfeitoDestino.efeito = it2.efeito
                                    fichaAmeacaEfeitoDestino.nuPeso = it2.nuPeso
                                    fichaAmeacaEfeitoDestino.save()
                                }
                            }
                            /**/
                            //println '18 Ameacas copiadas.'

                            // Usos
                            /**/
                            FichaUso.findAllByFicha(fichaOrigem).each {
                                fichaUsoDestino = new FichaUso(it.properties)
                                fichaUsoDestino.ficha = fichaDestino
                                // fichaUsoDestino.validate()
                                fichaUsoDestino.save()
                                _copiarRefBib(fichaOrigem, fichaDestino, 'ficha_uso', it.id, fichaUsoDestino.id)
                                // copiar regionalização e georeferencia do uso
                                FichaUsoRegiao.findAllByFichaUso(it).each { it2 ->
                                    // duplicar a tabela ficha_ocorrencia referente, aqui pega as regiões e os georeferenciamento
                                    fichaOcorrenciaDestino = new FichaOcorrencia(it2.fichaOcorrencia.properties)
                                    fichaOcorrenciaDestino.ficha = fichaDestino
                                    fichaOcorrenciaDestino.geoValidate = false
                                    fichaOcorrenciaDestino.save() // gerar nova ocorrencia
                                    fichaUsoRegiaoDestino = new FichaUsoRegiao(it2.properties)
                                    fichaUsoRegiaoDestino.fichaUso = fichaUsoDestino
                                    fichaUsoRegiaoDestino.fichaOcorrencia = fichaOcorrenciaDestino
                                    fichaUsoRegiaoDestino.save()
                                }
                            }
                            /**/
                            //println '19) Usos copiadps.'

                            // CONSERVAÇÃO
                            // Histórico Avaliação
                            /* não precisa mais alimentar esta tabela. Agora o historico é do taxon
                         * e está sendo gravado na tabela TaxonHistoricoAvaliacao
                         */

                            /** /
                             FichaHistoricoAvaliacao.findAllByFicha(fichaOrigem).each {fichaHistoricoAvaliacaoDestino = new FichaHistoricoAvaliacao(it.properties)
                             fichaHistoricoAvaliacaoDestino.ficha = fichaDestino
                             // fichaHistoricoAvaliacaoDestino.validate()
                             // //println fichaHistoricoAvaliacaoDestino.getErrors()
                             fichaHistoricoAvaliacaoDestino.save()
                             _copiarRefBib(fichaOrigem, fichaDestino, 'ficha_historico_avaliacao', it.id, fichaHistoricoAvaliacaoDestino.id)
                             // //println '\n  - Historico Avaliacao copiado...' + it.id}/**/

                            // Listas e convenções
                            /**/
                            FichaListaConvencao.findAllByFicha(fichaOrigem).each {
                                fichaListaConvencaoDestino = new FichaListaConvencao(it.properties)
                                fichaListaConvencaoDestino.ficha = fichaDestino
                                //fichaListaConvencaoDestino.validate()
                                fichaListaConvencaoDestino.save()
                            }
                            /**/
                            //println '20) Listas e convencoes copiadas.'

                            // Listas e convenções
                            /**/
                            FichaAcaoConservacao.findAllByFicha(fichaOrigem).each {
                                fichaAcaoConservacaoDestino = new FichaAcaoConservacao(it.properties)
                                fichaAcaoConservacaoDestino.ficha = fichaDestino
                                //fichaAcaoConservacaoDestino.validate()
                                fichaAcaoConservacaoDestino.save()
                                _copiarRefBib(fichaOrigem, fichaDestino, 'ficha_acao_conservacao', it.id, fichaAcaoConservacaoDestino.id)
                            }
                            /**/

                            // PESQUISA
                            // Existentes / necessárias
                            /**/
                            FichaPesquisa.findAllByFicha(fichaOrigem).each {
                                fichaPesquisaDestino = new FichaPesquisa(it.properties)
                                fichaPesquisaDestino.ficha = fichaDestino
                                //fichaPesquisaDestino.validate()
                                fichaPesquisaDestino.save()
                                _copiarRefBib(fichaOrigem, fichaDestino, 'ficha_pesquisa', it.id, fichaPesquisaDestino.id)
                            }
                            /**/
                            //println '21) Pesquisas copiadas.'

                            // AVALIAÇÃO
                            // População / Declinios Populacionais
                            /**/
                            FichaDeclinioPopulacional.findAllByFicha(fichaOrigem).each {
                                fichaDeclinioPopulacionalDestino = new FichaDeclinioPopulacional(it.properties)
                                fichaDeclinioPopulacionalDestino.ficha = fichaDestino
                                //fichaDeclinioPopulacionalDestino.validate()
                                fichaDeclinioPopulacionalDestino.save()
                            }
                            /**/
                            //println '22) Declinio populacional copiados.'

                            // Motivos Mudança categoria
                            /**/
                            FichaMudancaCategoria.findAllByFicha(fichaOrigem).each {
                                fichaMudancaCategoriaDestino = new FichaMudancaCategoria(it.properties)
                                fichaMudancaCategoriaDestino.ficha = fichaDestino
                                //fichaMudancaCategoriaDestino.validate()
                            }
                            //println '23) Motivos mudanca copiados.'

                            // Avaliações Regionais
                            FichaAvaliacaoRegional.findAllByFicha(fichaOrigem).each {
                                // Avaliação Final se não existir
                                taxonHistoricoAvaliacaoDestino = new TaxonHistoricoAvaliacao()
                                taxonHistoricoAvaliacaoDestino.taxon = fichaDestino.taxon
                                taxonHistoricoAvaliacaoDestino.abrangencia = it.abrangencia
                                if (it?.abrangencia?.municipio) {
                                    taxonHistoricoAvaliacaoDestino.tipoAvaliacao = tipoAvaliacaoRegional
                                }else {
                                    taxonHistoricoAvaliacaoDestino.tipoAvaliacao = tipoAvaliacaoEstadual
                                }
                                taxonHistoricoAvaliacaoDestino.nuAnoAvaliacao = it.nuAnoAvaliacao
                                taxonHistoricoAvaliacaoDestino.categoriaIucn = it.categoriaIucn
                                taxonHistoricoAvaliacaoDestino.deCriterioAvaliacaoIucn = it.deCriterioAvaliacaoIucn
                                taxonHistoricoAvaliacaoDestino.txJustificativaAvaliacao = it.txJustificativaAvaliacao
                                if (!taxonHistoricoAvaliacaoDestino.save()) {
                                    //println taxonHistoricoAvaliacaoDestino.getErrors()
                                }
                            }
                            //println '24) Avaliacoes regionais copiadas.'


                            // 25 - HISTORICO DA SITUACAO EXCLUIDA
                            FichaHistSituacaoExcluida.findAllByFicha( fichaOrigem ).each {
                                fichaHistSituacaoExcluidaDestino = new FichaHistSituacaoExcluida( it.properties )
                                fichaHistSituacaoExcluidaDestino.ficha = fichaDestino
                                fichaHistSituacaoExcluidaDestino.save()
                            }

                            // 26 - COPIAR TRADUCAO
                            TraducaoRevisao.createCriteria().list{
                                traducaoTabela{
                                    eq('noTabela','ficha')
                                }
                                eq('sqRegistro',fichaOrigem.id.toLong())
                            }.each{
                                traducaoRevisaoDestino = new TraducaoRevisao(it.properties)
                                traducaoRevisaoDestino.sqRegistro = fichaDestino.id
                                traducaoRevisaoDestino.save()
                            }


                            /**/
                            // Avaliação Final existir criar o registro na tabela de historico
                            if ( fichaOrigem.categoriaFinal ) {

                                // só pode haver uma avaliação nacional por taxon
                                taxonHistoricoAvaliacaoDestino = TaxonHistoricoAvaliacao.createCriteria().get {
                                    eq('taxon.id', fichaDestino.taxon.id)
                                    eq('tipoAvaliacao.id', tipoAvaliacaoNacional.id)
                                    eq('nuAnoAvaliacao', fichaOrigem.cicloAvaliacao.nuAno.toInteger())
                                    maxResults(1)
                                }
                                if (!taxonHistoricoAvaliacaoDestino) {

                                    taxonHistoricoAvaliacaoDestino = new TaxonHistoricoAvaliacao()
                                    taxonHistoricoAvaliacaoDestino.taxon = fichaDestino.taxon
                                }
                                taxonHistoricoAvaliacaoDestino.tipoAvaliacao = tipoAvaliacaoNacional
                                taxonHistoricoAvaliacaoDestino.categoriaIucn = fichaOrigem.categoriaFinal
                                taxonHistoricoAvaliacaoDestino.nuAnoAvaliacao = fichaOrigem.cicloAvaliacao.nuAno
                                taxonHistoricoAvaliacaoDestino.deCriterioAvaliacaoIucn = fichaOrigem.dsCriterioAvalIucnFinal
                                taxonHistoricoAvaliacaoDestino.txJustificativaAvaliacao = Util.stripTagsFichaPdf( fichaOrigem.dsJustificativaFinal )
                                taxonHistoricoAvaliacaoDestino.stPossivelmenteExtinta = fichaOrigem.stPossivelmenteExtintaFinal
                                taxonHistoricoAvaliacaoDestino.save()
                                // //println '\n  - Avaliacao Final copiada...' + fichaHistoricoAvaliacaoDestino.id
                                //println '27) Avaliacao final copiada.'
                            }
                        }

                        // atualizar o percentual de preenchimento e gerar as pendências automáticas
                        updatePercentualPreenchimento(fichaDestino, pessoaFisica)

                        ////println 'Fim ) percentual atualizado.'
                        ////println fichaOrigem.taxon.noCientifico + ' percentual atualizado.'
                    }
                    // fim codigo de copia da ficha
                    // incrementa o andamento
                    workerService.stepIt(worker)

                    if( worker.vlAtual % 10 == 0)
                    {
                        //def session = sessionFactory.currentSession
                        //session.flush()
                        //session.clear()
                    }
                } catch (Exception e) {
                    println e.getMessage()
                    //e.printStackTrace()
                    erros.push(e.getMessage())
                }

            }// end loop each
        } catch ( Exception e ) {
            println e.getMessage(  )
            //e.printStackTrace()
            erros.push(e.getMessage() )
        }
        // adiciona os erros encontrados no resultado do worker
        if ( erros ) {
            workerService.setError(worker, erros.join('<br>'))
        }
        // mensagem fichas que não foram copiadas
        if ( fichasNaoCopiadas ) {
            if ( isPrimeiroCiclo ) {
                workerService.setMessage(worker, 'Cópia da(s) ficha(s) finalizada.<br><br> A(s) seguinte(s) ficha(s) NÃO fora(m) copiada(s) devido a aba 11.7 não estar prenchida: <br><div class="red">- ' + fichasNaoCopiadas.join('<br>- ') + '</div>')
            } else {
                workerService.setMessage(worker, 'Cópia da(s) ficha(s) finalizada.<br><br> A(s) seguinte(s) ficha(s) NÃO fora(m) copiada(s) devido as abas 11.6 e/ou 11.7 não estarem prenchidas: <br><div class="red">- ' + fichasNaoCopiadas.join('<br>- ') + '</div>')
            }
        }
        println '  - fim copia ficha para outro ciclo'
        println '  - excluir fichas da sessao'
        println data
        appSession.sicae.listFichasCopiadas -= data.ids
        println appSession.sicae.listFichasCopiadas
        // encerrar o worker
        workerService.stepIt(worker)
        //println '_asyncCopiarFichas() finalizado!'
    }

    /**
     * método para copiar as referencias bibliograficas da ficha de acordo com o contexto informado
     * chamado pelo método _asyncCopiarFichas()
     * @param fichaOrigem
     * @param fichaDestino
     * @param tabela
     * @param oldId
     * @param sqRegistro
     */
    private void _copiarRefBib(Ficha fichaOrigem, Ficha fichaDestino, String tabela, Long oldId, Long sqRegistro) {
        FichaRefBib fichaRefBibDestino
        FichaRefBibTag fichaRefBibTagDestino
        /** /
        println ' '
        println 'Copiando ref bib da tabela: ' + tabela
        println 'Ficha Destino:'+fichaDestino.id
        println 'sqRegistro:' + sqRegistro
        /**/
        if( sqRegistro ) {
            FichaRefBib.findAllByFichaAndNoTabelaAndSqRegistro(fichaOrigem, tabela, oldId).each {
                try {
                    fichaRefBibDestino = new FichaRefBib(it.properties)
                    fichaRefBibDestino.ficha = fichaDestino
                    fichaRefBibDestino.sqRegistro = sqRegistro
                    fichaRefBibDestino.save()
                    // println '    - Referencia bibliografica copiada..' + it.id
                    FichaRefBibTag.findAllByFichaRefBib(it).each {
                        fichaRefBibTagDestino = new FichaRefBibTag(it.properties)
                        //println 'id da ref bib destino: ' + fichaRefBibDestino.id
                        fichaRefBibTagDestino.fichaRefBib = fichaRefBibDestino
                        fichaRefBibTagDestino.save()
                        //println '      - Tag da Referencias bibliograficas copiadas...' + it.id
                    } // fim each tag
                } catch(Exception e ){

                }
            } // each ref bib
        }
    }

    /**
     * método executado em segundo plano que será responsável por gerar os arquivos rtf das fichas
     * @param data
     */
    private void _asyncZipFichas(Map data = [:], GrailsHttpSession currentSession = null ) {
        List erros = []
        String fileName = ''
        List fileNames = []
        String zipFileName = data.tempDir + 'fichas-' + data.contexto + '-' + new Date().format('dd-MMM-yyyy-hh-mm-ss') + '.zip'
        Ficha ficha
        /*println ' '
        println ' '
        println '_asyncZipFichas iniciado em ' + new Date().format('dd/MM/yyyy HH:mm:ss')
        println 'Criando arquivo ' + zipFileName
        println 'Unidade: ' + currentSession.sicae.user.sgUnidadeOrg
        println ' '
        println ' '*/


        // registrar na sessão o worker do processo
        Map worker = workerService.add(currentSession
            , 'Gerando zip de ' + data.ids.size() + ' ficha(s) selecionada(s) no formato doc.'
            , data.ids.size() + 1 // add 1 para completar 100% só depois do passo de compactacao
            , "window.open(app.url + 'main/download?fileName=" + zipFileName + "&delete=1', '_top');")

        // inicializar as variaveis do worker
        workerService.start(worker)

        // inicia o loop das fichas
        data.ids.each { idFicha ->
            ficha = Ficha.get(idFicha.toInteger())
            if (ficha) {
                RtfFicha rtf = new RtfFicha(ficha.id.toInteger(), data.tempDir, data)
                rtf.unidade = data.unidadeOrg
                rtf.ciclo = ficha?.cicloAvaliacao?.deCicloAvaliacao?.toString()
                // gravar rtf em disco
                //println 'Gerando doc para ' + ficha.nmCientifico
                fileName = rtf.run()
                if (!fileName) {
                    erros.push('Erro rtf ficha ' + ficha.nmCientifico)
                    println 'Erro rtf.run() ficha ' + ficha.nmCientifico
                }else {
                    fileNames.push(fileName)
                }
            }
            // incrementa o andamento
            workerService.stepIt(worker)
        }

        // adiciona os erros encontrados no resultado do worker
        if (erros) {
            workerService.setError(worker, erros.join('<br>'))
        }

        // zipar e excluir os arquivos do disco
        byte[] buffer = new byte[1024]
        // https://github.com/icsharpcode/SharpZipLib/wiki/Zip-Samples
        FileOutputStream fos = new FileOutputStream(zipFileName)
        ZipOutputStream zos = new ZipOutputStream(fos)
        try {
            fileNames.each { name ->
                String entryName = name.substring(data.tempDir.length(), name.length())
                File file = new File(name)
                if (file.exists()) {
                    ZipEntry ze = new ZipEntry(entryName)
                    //println entryName
                    try {
                        zos.putNextEntry(ze)
                        FileInputStream fis = new FileInputStream(file)
                        int len
                        while ((len = fis.read(buffer)) > 0) {
                            zos.write(buffer, 0, len)
                        }
                        zos.closeEntry()
                        fis.close()
                    } catch (Exception e) {
                        println 'Erro adicionando ficha no arquivo zip'
                        println e.getMessage();
                    }
                    file.delete()
                }
            }
        } catch (Exception e) {
            println 'Erro criacao do arquivo zip de fichas ' + e.getMessage()
        }
        // finalizar a adicção de conteudo no arquivo zip
        zos.close()
        // encerrar o worker
        workerService.stepIt(worker)
        println '_asyncZipFichas() finalizado!'
    }


    /**
     * método para copiar a ficha de um ciclo para outro
     * @param data
     * @return
     */
    def copiarFichas(Map data=[:] ) {

        if ( ! data.ids  ) {
            throw new RuntimeException('Nenhuma ficha selecionada!')
        }
        if (! data.sqCicloDestino ) {
            throw new RuntimeException('Ciclo destino não informado!')
        }

        // recuperar os dados da sessão e guardar na variavel privada appSession
        getSession()

        if ( ! appSession?.sicae) {
            throw new RuntimeException('Sessão encerrada!')
        }

        if ( ! appSession?.sicae?.user) {
            throw new RuntimeException('Usuário não autenticado!')
        }

        // adicionar os ids na sessão para evitar copiar 2x a mesma ficha
        if( ! appSession.sicae.listFichasCopiadas )
        {
            appSession.sicae.listFichasCopiadas = []
        }

        appSession.sicae.listFichasCopiadas=[];
        List idsValidos = data.ids.findAll {
            return ! appSession.sicae.listFichasCopiadas.contains( it )
        }
        //appSession.sicae.listFichasCopiadas += idsValidos
        data.ids = idsValidos
        if( ! data.ids )
        {
            throw new RuntimeException('Ficha(s) selecionada(s) já esta(ão) sendo conpiad(s) copiada(s)!')
        }

        // iniciar a tarefa em segundo plano
        runAsync {
            _asyncCopiarFichas(data, appSession)
            //appSession.sicae.listFichasCopiadas -= data.ids
            // se necessário, enviar email para usuário aqui com log da cópia
        }
    }

    /**
     * método para atualizar o percentual de preenchimento da ficha.
     * chamado pelo método: _asyncCopiarFichas()
     * @param ficha/sqFicha
     * @param pessoaFisica
     */
    def updatePercentualPreenchimento(Long sqFicha, PessoaFisica pessoaFisica = null, Boolean updateUserAndDateChanged = true){
        this.updatePercentualPreenchimento( Ficha.get( sqFicha), pessoaFisica, updateUserAndDateChanged )
    }
    def updatePercentualPreenchimento(Ficha ficha, PessoaFisica pessoaFisica = null, Boolean updateUserAndDateChanged = true ) {

        // inicializar com 100 preenchido e ir abatendo os campos monitorados que não foram preenchidos
        double percentualPrenchido = 100

        // itens monitorados e seus pesos
        Map pesosCamposMonitorados=[endemicaBrasil          :1
                                    ,estado                 :1
                                    ,bioma                  :1
                                    ,ocorrencia             :5
                                    ,historiaNatural        :1
                                    ,tendenciaPopulacional  :1
                                    ,populacao              :1
                                    ,ameacas                :1
                                    ,usos                   :1
                                    ,uc                     :1
                                    ,refBib                 :1
                                    ,pendencias             :1

        ]
        // calcular a média ponderada
        double totalPesos = 0
        pesosCamposMonitorados.each { key, value ->
            totalPesos += value.toDouble()
        }
        double valorPeso = 100 / totalPesos
        pesosCamposMonitorados.collect { it.value = it.value * valorPeso }
        totalPesos = 0d
        pesosCamposMonitorados.each { key, value ->
            totalPesos += value.toDouble()
        }
        List listaPendenciasPreenchimento = []
        if (ficha) {

            // 1) verificar se tem texto distribuicao global
            if (!ficha.stEndemicaBrasil || !Util.trimEditor(ficha.dsDistribuicaoGeoGlobal)) {
                listaPendenciasPreenchimento.push('Campo "<b>Endêmica do Brasil?</b>" e "<b>Distribuição Geográfica Global</b>" na aba 2 devem ser preenchidos!')
                percentualPrenchido -= pesosCamposMonitorados.endemicaBrasil
            }
            // 2) verificar se tem UF
            /*if( !ficha.stLocalidadeDesconhecida || ficha.stLocalidadeDesconhecida !='S' ) {
                if (!FichaOcorrencia.findByFichaAndContexto(ficha, dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA', 'ESTADO'))) {
                    if (!this.existeUfSubespecie(ficha)) {
                        listaPendenciasPreenchimento.push('Nenhuma "<b>UF</b>" informada na aba 2.1!')
                    }
                }
            }*/
            // 2) Verificar se tem Uf - NOVO-REGISTRO
            if ((!ficha.stLocalidadeDesconhecida || ficha.stLocalidadeDesconhecida != 'S') && (!ficha.stSemUf || ficha.stSemUf != 'S') ) {
                // println ' '
                // println 'CALCULANDO PERCENTUAL PREENCHIMENTO'
                // println 'VERIFICANDO FICHA UF '
                if (!FichaUf.findByFicha(ficha, [max: 1])) {
                    // println 'VERIFICANDO FICHA REGISTRO UF '
                    // verificar ocorrencias salve
                    if( sqlService.execSql("select count(*) as nu_estados from salve.fn_taxon_estados(:sqTaxon) where st_utilizado_avalicao = true",[sqTaxon:ficha.taxon.id.toLong()])[0].nu_estados == 0 ) {
                        listaPendenciasPreenchimento.push('Nenhuma "<b>UF</b>" informada na aba 2.1 ou registro de ocorrência em algum Estado ou marcado como Localidade Desconhecida ou marcado como Sem UF na aba 2.6')
                        percentualPrenchido -= pesosCamposMonitorados.estado
                    }
                    /*
                    if (!FichaOcorrenciaRegistro.executeQuery("""select re.estado from FichaOcorrenciaRegistro a, RegistroEstado re
                            where a.fichaOcorrencia.ficha.id = ${ficha.id} and a.registro = re.registro
                              and ( a.fichaOcorrencia.inUtilizadoAvaliacao is null or a.fichaOcorrencia.inUtilizadoAvaliacao='S' )""", [max: 1])) {
                        // verificar ocorrencias do portalbio
                        if (!FichaOcorrenciaPortalbioReg.executeQuery("""select re.estado from FichaOcorrenciaPortalbioReg a, RegistroEstado re
                                where a.registro = re.registro
                                  and a.fichaOcorrenciaPortalbio.ficha.id = ${ficha.id}
                                  and a.fichaOcorrenciaPortalbio.inUtilizadoAvaliacao = 'S'
                                """, [max: 1])) {
                            if (!this.existeUfSubespecie(ficha)) {
                                println 'Entrou aqui'
                                listaPendenciasPreenchimento.push('Nenhuma "<b>UF</b>" informada na aba 2.1 ou registro de ocorrência em algum Estado!')
                                percentualPrenchido -= pesosCamposMonitorados.estado
                            }
                        }
                    }
                    */
                }
            }


            if (!ficha.stLocalidadeDesconhecida || ficha.stLocalidadeDesconhecida != 'S') {

                // 3) verificar se tem BIOMA
                /*if ( ! FichaOcorrencia.findByFichaAndBiomaIsNotNull(ficha)) {
                    if ( ! this.existeBiomaSubespecie( ficha ) ) {
                        listaPendenciasPreenchimento.push('Nenhum "<b>BIOMA</b>" informado na aba 2.2!')
                    }
                }*/
                // 3) verificar se tem BIOMA - NOVO-REGISTRO
                if( sqlService.execSql("select count(*) as nu_biomas from salve.fn_taxon_biomas(:sqTaxon) where st_utilizado_avalicao = true",[sqTaxon:ficha.taxon.id.toLong()])[0].nu_biomas == 0 ) {
                    listaPendenciasPreenchimento.push('Nenhum "<b>Bioma</b>" informado na aba 2.2 ou registro de ocorrência em algum Bioma!')
                    percentualPrenchido -= pesosCamposMonitorados.bioma
                }

                /*
                if (!FichaBioma.findByFicha(ficha, [max: 1])) {
                    // verificar ocorrencias do SALVE
                    if (!FichaOcorrenciaRegistro.executeQuery("""select re.bioma from FichaOcorrenciaRegistro a, RegistroBioma re
                            where a.fichaOcorrencia.ficha.id = ${ficha.id} and a.registro = re.registro
                              and ( a.fichaOcorrencia.inUtilizadoAvaliacao is null or a.fichaOcorrencia.inUtilizadoAvaliacao='S' )""", [max: 1])) {
                        // verificar ocorrencias do portalbio
                        if (!FichaOcorrenciaPortalbioReg.executeQuery("""select re.bioma from FichaOcorrenciaPortalbioReg a, RegistroBioma re
                                where a.fichaOcorrenciaPortalbio.ficha.id = ${ficha.id} and a.registro = re.registro
                                  and a.fichaOcorrenciaPortalbio.inUtilizadoAvaliacao = 'S'
                                """, [max: 1])) {
                            if (!this.existeBiomaSubespecie(ficha)) {
                                listaPendenciasPreenchimento.push('Nenhum "<b>Bioma</b>" informado na aba 2.2 ou registro de ocorrência em algum Bioma!')
                                percentualPrenchido -= pesosCamposMonitorados.bioma
                            }
                        }
                    }
                }*/


                //4) verificar se possui alguma registro de ocorrencia
                if (!FichaOcorrenciaRegistro.executeQuery("""select a.id from FichaOcorrenciaRegistro a
                                where a.fichaOcorrencia.ficha.id = ${ficha.id}
                                  and a.fichaOcorrencia.inUtilizadoAvaliacao = 'S'
                                """, [max: 1])) {
                    if (!FichaOcorrenciaPortalbioReg.executeQuery("""select a.id from FichaOcorrenciaPortalbioReg a
                                where a.fichaOcorrenciaPortalbio.ficha.id = ${ficha.id}
                                  and a.fichaOcorrenciaPortalbio.inUtilizadoAvaliacao = 'S'
                                """, [max: 1])) {
                        if (!this.existeOcorrenciaSubespecie(ficha)) {
                            listaPendenciasPreenchimento.push('Nenhuma "<b>OCORRÊNCIA</b>" cadastrada na aba 2.6 ou do PortalBio / Consulta Ampla/Direta utilizada na avaliação.')
                            percentualPrenchido -= pesosCamposMonitorados.ocorrencia
                        }
                    }
                }
                /*
                // 4) possui algum registro de ocorrencia
                if (!FichaOcorrencia.findByFichaAndContexto(ficha, dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA', 'REGISTRO_OCORRENCIA'))) {
                    // verificar se tem alguma ocorrencia da consulta que foi aceita
                    if (!FichaOcorrencia.findByFichaAndContextoAndInUtilizadoAvaliacao(ficha, dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA', 'CONSULTA'), 'S')) {
                        // verificar se tem alguma ocorrência do portalbio utilizada na avaliação
                        if (!FichaOcorrenciaPortalbio.findByFichaAndInUtilizadoAvaliacao(ficha, 'S')) {
                            // verificar se tem alguma ocorrencia na subespecie
                            if (!this.existeOcorrenciaSubespecie(ficha)) {
                                listaPendenciasPreenchimento.push('Nenhuma "<b>OCORRÊNCIA</b>" cadastrada na aba 2.6 ou do PortalBio / Consulta Ampla/Direta utilizada na avaliação.')
                            }
                        }
                    }
                }*/
            }

            // 5) historia natural
            if (!Util.trimEditor(ficha.dsHistoriaNatural)) {
                listaPendenciasPreenchimento.push('Descrição de "<b>HISTÓRIA NATURAL</b>" não informada na aba 3!')
                percentualPrenchido -= pesosCamposMonitorados.historiaNatural
            }

            // 6) tendencia populacional
            if (!ficha.tendenciaPopulacional) {
                listaPendenciasPreenchimento.push('Campo "<b>Tendência Populacional</b>" não informado na aba 4!')
                percentualPrenchido -= pesosCamposMonitorados.tendenciaPopulacional
            }

            // 7) Obs sobre a população
            if (!Util.trimEditor(ficha.dsPopulacao)) {
                if (!ficha?.tendenciaPopulacional || ficha.tendenciaPopulacional.codigo != 'DESCONHECIDA') {
                    listaPendenciasPreenchimento.push('Campo "<b>Observação Sobre a População</b>" não informado na aba 4!')
                    percentualPrenchido -= pesosCamposMonitorados.populacao
                }

            }

            // 8) Obs sobre a ameaça
            if (!Util.trimEditor(ficha.dsAmeaca)) {
                listaPendenciasPreenchimento.push('Campo "<b>Observações Gerais Sobre as Ameaças</b>" não informado na aba 5!')
                percentualPrenchido -= pesosCamposMonitorados.ameacas
            }

            // 9) Obs sobre o Uso
            if (!Util.trimEditor(ficha.dsUso)) {
                listaPendenciasPreenchimento.push('Campo "<b>Observações Gerais Sobre os Usos</b>" não informado na aba 6!')
                percentualPrenchido -= pesosCamposMonitorados.usos
            }

            // 10) Obs sobre presença em uc ou UC informada contam 1 ponto
            /*if (!Util.trimEditor(ficha.dsPresencaUc) && !FichaOcorrencia.createCriteria().list {
                eq('ficha', ficha)
                eq('contexto', dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA', 'REGISTRO_OCORRENCIA'))
                and {
                    or {
                        isNotNull('sqUcFederal')
                        isNotNull('sqUcEstadual')
                        isNotNull('sqRppn')
                    }
                }
                maxResults(1)
            }) {
                if( !ficha.stLocalidadeDesconhecida || ficha.stLocalidadeDesconhecida !='S' ) {
                    if (!this.existeUCSubespecie(ficha)) {
                        listaPendenciasPreenchimento.push('Nenhuma ocorrência em "<b>UC</b>" informada na aba 2.6 ou campo "<b>Observações Gerais Sobre a Presença UC</b>" foi preenchido na aba 7.4!')
                    }
                }
            }*/
            // NOVO-REGISTRO
            if (!Util.trimEditor(ficha.dsPresencaUc)) {
                List res = sqlService.execSql("select sq_uc from salve.fn_ficha_ucs(:sqFicha) limit 1;", [sqFicha: ficha.id])
                if (!res) {
                    listaPendenciasPreenchimento.push('Nenhuma ocorrência em "<b>UC</b>" informada na aba 2.6 ou campo "<b>Observações Gerais Sobre a Presença UC</b>" foi preenchido na aba 7.4!')
                    percentualPrenchido -= pesosCamposMonitorados.uc
                }
            }

            // 11) ref. bibliográfica ou comunicacao pessoal
            if (!FichaRefBib.findByFicha(ficha)) {
                listaPendenciasPreenchimento.push('Nenhuma "<b>Referência Bibliográfica</b>" foi informada na ficha! (aba 9)')
                percentualPrenchido -= pesosCamposMonitorados.refBib
            }

            Integer qtdPendenciaPrenchimento = listaPendenciasPreenchimento.size()

            // 12) se hover pendencias adicionar 1 ponto
            List resultado = sqlService.execSql("""select count(*) as qtd from salve.ficha_pendencia
                    where sq_ficha = :sqFicha
                        and st_pendente='S'
                        and (de_assunto is null or de_assunto <> 'Campos obrigatórios')""", [sqFicha: ficha.id])
            if (resultado[0].qtd > 0 || qtdPendenciaPrenchimento > 0) {
                //listaPendenciasPreenchimento.push('Ficha possui ' + resultado[0].qtd + ' pendência(s) não resolvida(s)')
                percentualPrenchido -= pesosCamposMonitorados.pendencias
            }


            //println'\nPercentual ' + percentualPrenchido
            //println pesosCamposMonitorados

            // 13) possui mapa distribuição principal
            /*
                // reunião equipe dia 05/12/2018 solicitou que o mapa não seja computado no percentual
                if (!FichaAnexo.findByFichaAndContextoAndInPrincipal(ficha, dadosApoioService.getByCodigo('TB_CONTEXTO_ANEXO', 'MAPA_DISTRIBUICAO'), 'S')) {
                    listaPendenciasPreenchimento.push('Nenhuma "<b>Imagens de MAPA ou SHAPEFILE</b>" anexado na aba 2 como principal!')
                }
                */
            /*           if (qtdPendenciaPrenchimento > totalCamposMonitorados) {
                totalCamposMonitorados = qtdPendenciaPrenchimento
            }
*/
            FichaPendencia pendenciaCamposObrigatorios = FichaPendencia.findByFichaAndDeAssunto(ficha, 'Campos obrigatórios')

            if (listaPendenciasPreenchimento.size() == 0) {
                if (pendenciaCamposObrigatorios) {
                    pendenciaCamposObrigatorios.delete(flush: true)
                }
            } else {
                if (!pendenciaCamposObrigatorios) {
                    pendenciaCamposObrigatorios = new FichaPendencia()
                    pendenciaCamposObrigatorios.ficha = ficha
                    pendenciaCamposObrigatorios.deAssunto = 'Campos obrigatórios'
                    pendenciaCamposObrigatorios.usuario = pessoaFisica
                }

                if (pendenciaCamposObrigatorios.ficha) {
                    pendenciaCamposObrigatorios.contexto = dadosApoioService.getByCodigo('TB_CONTEXTO_PENDENCIA', 'SISTEMA')
                    pendenciaCamposObrigatorios.stPendente = 'S'
                    pendenciaCamposObrigatorios.dtPendencia = new Date()
                    pendenciaCamposObrigatorios.txPendencia = '<ol>' + listaPendenciasPreenchimento.collect {
                        '<li>' + it + '</li>'
                    }.join('') + '</ol>'
                    if (pessoaFisica && updateUserAndDateChanged) {
                        pendenciaCamposObrigatorios.usuario = pessoaFisica
                    }
                    pendenciaCamposObrigatorios.save(flush: true)
                }
            }
            //Long novoValor = (totalCamposMonitorados - qtdPendenciaPrenchimento) / totalCamposMonitorados * 100
            ficha.vlPercentualPreenchimento = percentualPrenchido.toLong()

            // se a ficha estiver na situacao VALIDADA e tiver pendencia, passar para VALIDADA_EM_REVISAO e virse-versa
            Integer qtdPendencia = 0
            if (ficha.situacaoFicha.codigoSistema ==~ /VALIDADA|VALIDADA_EM_REVISAO/) {

                qtdPendencia = listaPendenciasPreenchimento.size()
                if (qtdPendencia == 0) {
                    // verificar se tem pendencia informada pelos usuários
                    qtdPendencia = FichaPendencia.createCriteria().count {
                        eq('ficha.id', ficha.id)
                        eq('stPendente', 'S')
                    }
                }
                if (qtdPendencia > 0 || ! ficha.dsJustificativaFinal ) {
                    if (ficha.situacaoFicha.codigoSistema == 'VALIDADA') {
                        ficha.situacaoFicha = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'VALIDADA_EM_REVISAO')
                    }
                } else {
                    ficha.situacaoFicha = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'VALIDADA')
                }
            }
            if (pessoaFisica && updateUserAndDateChanged) {
                ficha.dtAlteracao = new Date()
                ficha.alteradoPor = pessoaFisica
            }
            ficha.save(flush: true, validate: false)
            /** /
             println ' '
             println ' '
             println 'Qtd pendencias encontradas:' + qtdPendenciaPrenchimento
             println 'Maximo: ' + totalCamposMonitorados
             println 'qtd pendencias cadastradas ' + resultado[0].qtd
             println 'Percentual calculado : ' + novoValor + ' ou ' + ficha.vlPercentualPreenchimento
             println '- '*50
             println ' '
             /**/
        }
    }

    /**
     * atualizar o percentual de preenchimento da ficha em função quando uma subespecie for alterada
     * @param ficha/sqFicha
     * @return
     */
    def updatePercentualEspecie( Long sqFicha, PessoaFisica pessoaFisica, Boolean updateUserAndDateChanged = true ) {
        updatePercentualEspecie(Ficha.get( sqFicha ), pessoaFisica, updateUserAndDateChanged  )
    }
    def updatePercentualEspecie( Ficha ficha, PessoaFisica pessoaFisica, Boolean updateUserAndDateChanged = true ) {
        if( ficha && ficha.taxon.nivelTaxonomico.coNivelTaxonomico == 'SUBESPECIE' ) {
            Taxon taxonPai = ficha.taxon.pai
            VwFicha fichaEspecie = VwFicha.findBySqTaxonAndSqCicloAvaliacao( taxonPai.id,ficha.cicloAvaliacao.id );
            if( fichaEspecie ) {
                this.updatePercentualPreenchimento( fichaEspecie.id, pessoaFisica, updateUserAndDateChanged )
            }
        }
    }
    def gerarZipFichas(GrailsParameterMap params) {

        // recuperar os dados da sessão e guardar na variavel privada appSession
        getSession()

        if (!appSession.sicae) {
            throw new RuntimeException('Sessão encerrada!')
        }

        // recuperar o contexto para ter acesso às variáveis de configuração do arquivo /data/salve-estadual/config.properties
        ApplicationContext ctx = Holders.grailsApplication.mainContext
        String tempDir = ctx.grailsApplication.config.temp.dir
        Map data = [contexto: 'salve']

        // CONTEXTO OFICINA - RETIRAR AS FICHAS EXCLUIDAS
        if (params.contexto) {
            if (params.contexto == 'oficina') {

                // verificar se a oficina foi selecionada
                if (!params.sqOficina) {
                    throw new RuntimeException('Oficina não foi informada!')
                }

                // se não passou a lista de ids, ler todas as fichas da oficina sem as excluídas
                if (!params.ids) {
                    List ids = []
                    OficinaFicha.createCriteria().list {
                        eq('oficina', Oficina.get(params.sqOficina.toInteger()))
                        vwFicha {
                            ne('sqSituacaoFicha', DadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'EXCLUIDA').id.toInteger())
                        }
                    }.each {
                        ids.push(it.ficha.id)
                    }
                    params.ids = ids.join(',')
                }

                if (!params.ids) {
                    throw new RuntimeException('Nenhuma ficha selecionada!')
                }

                // configurar os parâmetros para gerar o relatório
                data.adColaboracao = params.adColaboracao
                data.adValidacao = params.adValidacao
                data.contexto = 'oficina'
                data.tempDir = tempDir
                data.ids = params.ids.split(',')
                data.unidadeOrg = appSession.sicae.user.sgUnidadeOrg ?: 'Sistema Salve'
                /*
                println 'GerarZipFichas Params:'
                println data
                println "-"*100
                println ' '
                */

                // iniciar a tarefa em segundo plano
                runAsync {
                    _asyncZipFichas(data, appSession)
                }


                //throw new RuntimeException('Ops, tudo certo!');
            }

        }
    }

    /**
     * Ler ocorrencias do PortalBio e Sisbio e atualizar a tabela ficha_ocorrencia_portalbio
     * @exemple https://salve.icmbio.gov.br/salve-estadual/api/searchPortalbio/Accipiter%20bicolor
     * @param data
     * @return
     */
    def importarOcorrenciasPortalbio(Integer idFicha,String jsCallback='') {
        // recuperar os dados da sessão e guardar na variavel privada appSession
        getSession()

        // verificar se o usuário ainda está logado
        if (!appSession.sicae) {
            throw new RuntimeException('Sessão encerrada!')
        }

        // parametros para iniciar a tarefa
        Map data = [idFicha: idFicha, callback: jsCallback]

        // recuperar o contexto para ter acesso às variáveis de configuração do arquivo /data/salve-estadual/config.properties
        ApplicationContext ctx = Holders.grailsApplication.mainContext
        data.url = ctx.grailsApplication.config.url.search.portalbio

        VwFicha ficha = VwFicha.get( idFicha )

        if( !ficha ){
            return;
        }
        data.taxon = ficha?.taxon?.noCientifico
        data.sqTaxon = ficha?.taxon?.id


            /** /
            println ' '
            println ' '
            println 'runAsync: Importar as ocorrencias do PortalBio. Ficha (' + idFicha + ') ' + data.taxon + ' ' + new Date().format('dd/MM/yyyy HH:mm:ss')
            if (jsCallback) {
                println 'callback: ' + jsCallback
            }
            /**/

        runAsync {
            _impOcoPortalbio( data )
        }
    }

    /**
     * Executar o serviço assyncrono para ler e gravar as ocorrencias do PortalBio e Sisbio na tabela ficha_ocorrencia_portalbio
     * @exemple https://salve.icmbio.gov.br/salve-estadual/api/searchPortalbio/Accipiter%20bicolor
     * @param data
     * @return
     */
    def _impOcoPortalbio(Map data = [:] ) {
        Integer totalOcorrenciasSisbio
        Integer intervalo1Dia = ( 24i * 60i )
        Date dataAlteracao = new Date()
        String sql

        d(' ')
        d('Inicio importacao ocorrencias Portalbio/Sisbio');
        if( data ) {
            d(' - Ficha...: ' + data?.idFicha + ' - '+ data?.taxon)
            d(' - Taxon...: ' + data?.sqTaxon)
            d(' - Url     : ' + data?.url)
        }

        //Boolean isDesenv = (Environment.current == Environment.DEVELOPMENT)

        // verificar se algum usuário já fez a atulização quando for ambiente de Produção
        d(' - Lendo última vez...')
        // List regLastDate = sqlService.execSql('select max(dt_alteracao) as dt_alteracao from salve.ficha_ocorrencia_portalbio where sq_ficha = :sqFicha and no_base_dados is not null', [sqFicha: data.idFicha])
        DadosApoio tipoSincronismo = dadosApoioService.getByCodigo('TB_TIPO_SINCRONISMO','SINCRONISMO_REGISTRO_OCORRENCIA')
        if( !tipoSincronismo ){
            DadosApoio tbTipoSincronismo = new DadosApoio()
            tbTipoSincronismo.descricao = 'Tabela de tipos de sincronismos'
            tbTipoSincronismo.codigoSistema = 'TB_TIPO_SINCRONISMO'
            tbTipoSincronismo.codigo = 'TB_TIPO_SINCRONISMO'
            tbTipoSincronismo.save( flush:true )
            tipoSincronismo = new DadosApoio()
            tipoSincronismo.pai = tbTipoSincronismo
            tipoSincronismo.descricao = 'Sincronismo de Registros de Ocorrências'
            tipoSincronismo.codigoSistema = 'SINCRONISMO_REGISTRO_OCORRENCIA'
            tipoSincronismo.codigo = 'SINCRONISMO_REGISTRO_OCORRENCIA'
            tipoSincronismo.save( flush:true )
        }

        FichaSincronismo fichaSincronismo = FichaSincronismo.createCriteria().get {
            eq('ficha.id',data.idFicha.toLong())
            eq('tipo',tipoSincronismo)
        }
        // fazer o sincronismo com portalbio de 2 em 2 dias
        intervalo1Dia = intervalo1Dia * 2
        if( ! fichaSincronismo ) {
            fichaSincronismo = new FichaSincronismo()
            fichaSincronismo.tipo=tipoSincronismo
            fichaSincronismo.ficha = Ficha.get( data.idFicha.toLong())
            fichaSincronismo.nuMinutosIntervalo = intervalo1Dia
            fichaSincronismo.dtSincronismo = dataAlteracao - ( fichaSincronismo.nuMinutosIntervalo + 1 )
            fichaSincronismo.save( flush:true )
//println ' '
//println 'CRIANDO SINCRONISMO'
//            println fichaSincronismo.getErrors()
        }
            d(' - Ultima vez foi em ' + fichaSincronismo.dtSincronismo.format('dd/MM/yyyy HH:mm:ss') )
            Integer minutos = Util.dateDiff( fichaSincronismo.dtSincronismo, new Date(), 'M')
            d(' - ' + minutos + ' minutos atras')
            //Integer dias = Util.dateDiff(fs.dtSincronismo, new Date(), 'D');
            //d(' - ' + dias + ' dias atras')
            Integer intervaloMinimo  = fichaSincronismo.nuMinutosIntervalo ?: intervalo1Dia
            if ( minutos <= intervaloMinimo ) {
            //if ( dias <= regLastDate[0].nu_minutos_intervalo ) {
                d(' - Sincronismo portalbio foi a menos de ' + intervaloMinimo + ' minutos.')
                d(' - Sincronismo encerrado')
                d(' - Proximo sincronismo em ' + (intervaloMinimo - minutos) + ' minutos')
                // d(' - Sincronismo portalbio foi a menos de ' + intervaloDias + ' dia.')
                // d(' - Ultima vez foi em ' + fs.dtSincronismo.formst('dd/MM/yyyy HH:mm:ss') )
                // d(' - Proximo sincronismo em ' + (intervaloDias - dias) + ' dia')
                d('-' * 50)
                d(' ')
                //appSession.tarefas.remove(idTarefa)
                return
            }

        // evitar outro usuário fazer o sincronismo no mesmo dia
        fichaSincronismo.dtSincronismo = dataAlteracao;
        fichaSincronismo.save( flush:true )
        d(' - Data sincronismo foi atualizada.')

        JSONObject dados
        Date dt1500     = new Date().parse("dd/MM/yyyy", '01/01/1500')
        Date dtSearch   = null
        Ficha ficha
        List reg        = []
        FichaOcorrenciaPortalbio fop
        Map worker      = [:]

        // criar worker para atualizar o mapa ao terminar a tarefa
        if( data.callback ) {
            worker = workerService.add(appSession
                    , 'Lendo registros portalbio...'
                    , 0
                    , data.callback)
            // inicializar as variaveis do worker
            workerService.start(worker)
            // println 'worker criado na sessao!'
        }

        try {
            if( ! data.url.endsWith('/') )
            {
                data.url += '/'
            }
            String url = data.url + data.taxon.replaceAll(/\s/,'%20')

            // fazer o sincronismo com o portalbio somente se ainda não existir registros ainda
            dados = ['ocorrencias' : [] ]
            if( data.idFicha && ! sqlService.execSql("""select sq_ficha_ocorrencia_portalbio from salve.ficha_ocorrencia_portalbio where sq_ficha = :sqFicha AND NOT DE_UUID like 'ICMBIO/DIBIO/SISBIO/ID:%' limit 1""", [sqFicha:data.idFicha.toLong()] ) ) {
                d(' - Inicio request ocorrencias do Portalbio')
                dados = JSON.parse(requestService.doPost(url))
                d(' - Total ocorrencia PortalBio ' + dados.ocorrencias.size() )
                if( ! dados.ocorrencias ) {
                    dados = ['ocorrencias':[]]
                }
            } else {
                d(' - ocorrencia PortalBio ja importadas')
            }

            d(' - Ler ocorrencias NOVAS do SISBIO ( que não foram importadas para o SALVE ainda )')
            String cmd = """select sisbio.* from salve.vw_ocorrencias_sisbio as sisbio
                    where sisbio.sq_taxon = :sqTaxon
                      and sisbio.nu_latitude is not null
                      and sisbio.nu_longitude is not null
                      and not exists (
                            select null from salve.ficha_ocorrencia_portalbio x
                            where x.sq_ficha = :sqFicha
                            and x.de_uuid = concat( 'ICMBIO/DIBIO/SISBIO/ID:',sisbio.sq_ocorrencia)
                            offset 0 limit 1
                        )
                   """
            List dadosSisbio = sqlService.execSql( cmd, [sqTaxon:data.sqTaxon,sqFicha: (data.idFicha ? data.idFicha.toLong():0l) ])
            totalOcorrenciasSisbio = dadosSisbio ? dadosSisbio.size() : 0
            d(' - Total de ocorrências SISBIO ' + totalOcorrenciasSisbio.toString())
            if( dadosSisbio ) {
                if ( ( dados.ocorrencias.size() + dadosSisbio.size() ) > 0 ) {
                    // atualizar o valor max do worker
                    workerService.setMax(worker, dados.ocorrencias.size() + dadosSisbio.size())

                    ficha = Ficha.get( data.idFicha.toInteger() )

                    /***************************************
                     * processar as ocorrências do PORTALBIO
                     */
                    dados.ocorrencias.eachWithIndex { item, index ->
                        /** /
                         // depurar o primeiro registro retornado
                         if( isDesenv() ) {if (index == 0) {println ' '
                         println ' '
                         println ' '
                         println 'AsyncService()->_impOcoPortalbio()'
                         println item.toString().replaceAll(/","/, '"\n,"')
                         println ' '
                         println '-' * 100
                         println ' '
                         println ' '}}/**/

                        workerService.setTitle(worker, item.nomeCientifico)

                        // adicionar data da alteracao nos dados retornados do portalbio para controle
                        item.alteracao = dataAlteracao.format('dd/MM/yyyy HH:mm:ss')

                        // adicionar o nome da base de dados nos dados retornados do portalbio para controle
                        if (item.codigoColecao =~ /(?i)SISBIO/) {
                            item.nomeBaseDados = 'ICMBIO/Sisbio'
                        } else {
                            if (item.codigoColecao =~ /(?i)ARA -/) {
                                item.nomeBaseDados = 'ICMBIO/Ara'
                            } else if (item.codigoColecao =~ /(?i)SNA/) {
                                item.nomeBaseDados = 'ICMBIO/SNA'
                            } else if (item.codigoColecao =~ /(?i)SIMAM/) {
                                item.nomeBaseDados = 'ICMBIO/Simam'
                            } else if (item.codigoColecao =~ /(?i)SITAMAR/) {
                                item.nomeBaseDados = 'ICMBIO/Sitamar'
                            } else if (item.codigoColecao =~ /(?i)SISQUELONIOS/) {
                                item.nomeBaseDados = 'ICMBIO/Sisquelonios'
                            } else if (item.codigoColecao =~ /(?i)MONITORAMENTO/) {
                                item.nomeBaseDados = 'ICMBIO/Monitoramento'
                            } else {
                                item.nomeBaseDados = 'Portalbio'
                            }
                        }

                        try {
                            // campos obrigatórios para importação
                            if (!(item.latitude && item.longitude && item.uuid && item.autor)) {
                                throw new Exception('Registro inválido.')
                            }

                            // não importar as ocorrências do SISBIO
                            if ( (item.instituicao =~ /SISBIO/) || ( item.codigoColecao =~ /SISBIO/) ) {
                                /*if (isDesenv()) {
                                    println 'Desenv: Não importar ocorrencias do SISBIO diretamente do portalbio.'
                                }*/
                                throw new Exception('')
                            }

                            if (item.uuid) {

                                /*if( item.dataOcorrencia ) {
                                    println item.dataOcorrencia
                                    println item.dataOcorrencia.replaceAll(/(\/0{2,4})([1-9]{2})?/, '/19$2')
                                    println '-' * 70
                                }*/

                                // verificar se a ocorrencia foi transferida para outra ficha e não importa-la novamente
                                reg = FichaOcorrenciaPortalbio.executeQuery('select new map(fo.id as id )' +
                                    ' from FichaOcorrenciaPortalbio fo ' +
                                    ' join fo.ficha ficha where ficha.id <> :sqFicha ' +
                                    ' and fo.deUuid = :deUuid' +
                                    ' and fo.ficha.cicloAvaliacao.id = :sqCicloAvaliacao',
                                     [sqFicha: data.idFicha.toLong()
                                                                 , sqCicloAvaliacao: ficha.cicloAvaliacao.id
                                                                 , deUuid: item.uuid ] )
                                if ( reg.size() > 0 ) {
                                    //  registro foi transferido
                                     throw new Exception('')
                                }
                                reg = FichaOcorrenciaPortalbio.executeQuery('select new map(fo.id as id, fo.txOcorrencia as txOcorrencia, fo.dtOcorrencia as dtOcorrencia, fo.deUuid as deUuid )' +
                                    ' from FichaOcorrenciaPortalbio fo ' +
                                    ' join fo.ficha ficha where ficha.id = :sqFicha ' +
                                    ' and fo.deUuid = :deUuid', [sqFicha: data.idFicha.toLong(), deUuid: item.uuid])

                                // ja existe
                                if (reg.size() > 0) {
                                    reg.each {
                                        fop = FichaOcorrenciaPortalbio.get( it.id )
                                        if ( ! fop.dtOcorrencia || fop.dtOcorrencia == dt1500 ) {
                                            fop.dtOcorrencia = null
                                            fop.inDataDesconhecida = 'N'
                                        } else {
                                            fop.inDataDesconhecida = 'N'
                                        }
                                        fop.save(flush: true)
                                    }
                                    throw new Exception('')
                                }

                                // verificar se a coordenada já existe para o autor, data
                                Geometry geometry = null
                                if (item?.longitude && item?.latitude) {
                                    try {
                                        geometry = new GeometryFactory().createPoint(new Coordinate(item.longitude, item.latitude))
                                    } catch (Exception e) {
                                        geometry = null
                                    }
                                    if (!geometry) {
                                        throw new Exception('coordenadas invalidas ' + item.longitude + ', ' + item.latitude)
                                    }
                                    geometry.SRID = 4674
                                }

                                /*
                                // Esta verificação não é possível porque pode haver um registro identico em uma outra espécie
                                // verficar se tem mesmo Autor/Ano/Coordenadas/dataOcorrencia em outra ficha
                                String sql = 'select new map(fo.id as id) from FichaOcorrenciaPortalbio fo' +
                                    ' join fo.ficha ficha where ficha.id <> :sqFicha' +
                                    ' and ficha.cicloAvaliacao.id = :sqCicloAvaliacao'+
                                    ' and fo.noAutor = :noAutor'

                                if (item.dataOcorrencia) {
                                    sql += ' and fo.dtOcorrencia = :data'
                                    dtSearch = new Date().parse("dd/MM/yyyy", item.dataOcorrencia)
                                } else {
                                    dtSearch = dt1500
                                    sql += ' and ( fo.dtOcorrencia is null or fo.dtOcorrencia = :data )'
                                }
                                sql += ' and fo.geometry = :geometry'
                                reg = FichaOcorrenciaPortalbio.executeQuery(sql, [sqFicha: data.idFicha.toLong()
                                                                                  , sqCicloAvaliacao: ficha.cicloAvaliacao.id
                                                                                  , noAutor : item.autor
                                                                                  , data    : dtSearch
                                                                                  , geometry: geometry] )
                                if( reg.size() > 0 ){
                                    // println 'Registro '+reg[0].id +' foi transferido de ficha'
                                    throw new Exception('')
                                }
                                */


                                // verficar se tem mesmo Autor/Ano/Coordenadas/dataOcorrencia
                                sql = 'select new map(fo.id as id, fo.deUuid as deUuid ) from FichaOcorrenciaPortalbio fo' +
                                    ' join fo.ficha ficha where ficha.id = :sqFicha' +
                                    ' and fo.noAutor = :noAutor'

                                if (item.dataOcorrencia) {
                                    sql += ' and fo.dtOcorrencia = :data'
                                    dtSearch = new Date().parse("dd/MM/yyyy", item.dataOcorrencia)
                                } else {
                                    dtSearch = dt1500
                                    sql += ' and ( fo.dtOcorrencia is null or fo.dtOcorrencia = :data )'
                                }

                                sql += ' and fo.geometry = :geometry'
                                reg = FichaOcorrenciaPortalbio.executeQuery(sql, [sqFicha: data.idFicha.toLong(), noAutor: item.autor, data: dtSearch, geometry: geometry])

                                if (reg.size() > 0) {
                                    List regTemp = FichaOcorrenciaPortalbio.executeQuery('select new map( fop.id as id, fop.txOcorrencia as txOcorrencia ) from FichaOcorrenciaPortalbio fop where fop.id = :id', [id: reg[0].id.toLong()])
                                    if (regTemp) {

                                        // atualizar somente os metadados e a data da alteracao
                                        if (regTemp[0].txOcorrencia.toString() != item.toString()) {
                                            FichaOcorrenciaPortalbio f = FichaOcorrenciaPortalbio.get(regTemp[0].id.toLong())
                                            f.txOcorrencia = item.toString()
                                            f.dtAlteracao = dataAlteracao
                                            f.noBaseDados = item.nomeBaseDados
                                            if( ! f.dtOcorrencia || f.dtOcorrencia == dt1500 ) {
                                                f.dtOcorrencia=null
                                                f.inDataDesconhecida='S'
                                            } else {
                                                f.inDataDesconhecida='N'
                                            }
                                            f.save(flush: true)
                                        }
                                    }
                                    throw new Exception('')
                                } else {
                                    // gravar no banco de dados
                                    fop = new FichaOcorrenciaPortalbio()
                                    fop.ficha = ficha
                                    fop.deUuid = item.uuid
                                    fop.geometry = geometry
                                    fop.noAutor = item.autor
                                    fop.noCientifico = item.nomeCientifico
                                    fop.noLocal = item.local
                                    fop.noInstituicao = item.instituicao
                                    if( !dtSearch || dtSearch == dt1500 ){
                                        fop.inDataDesconhecida = 'S'
                                        fop.dtOcorrencia = null
                                    } else {
                                        fop.inDataDesconhecida = 'N'
                                        fop.dtOcorrencia = dtSearch
                                    }

                                    //(item.dataOcorrencia ? new Date().parse("dd/MM/yyyy", item.dataOcorrencia) : dt1500)
                                    fop.txOcorrencia = item.toString() // guardar o json com todos os dados retornados
                                    fop.noBaseDados = item.nomeBaseDados

                                    if (item.carencia) {
                                        fop.dtCarencia = new Date().parse("dd/MM/yyyy", item.carencia)
                                    }
                                    fop.deAmeaca = item.ameaca
                                    if (fop.validate()) {
                                        fop.dtAlteracao = dataAlteracao
                                        try {
                                            fop.save(flush: true)
                                        } catch (Exception e) {
                                        }
                                    }
                                }
                            } else {
                                d('Ocorrencisa do portalbio sem uuid.')
                            }
                        } catch (Exception e) { }
                        // incrementar o andamento
                        workerService.stepIt(worker)
                    } // end each ocorrencias portalbio

                    /************************************
                     * processar as ocorrências do SISBIO
                     */
                    Map mapDadosSisbio = [:]
                    List idsDuplicados = []
                    d(' - Inicio gravacao ocorrencias do SISBIO')
                    dadosSisbio.eachWithIndex { it, index ->
                        try {
                            mapDadosSisbio = [:]
                            mapDadosSisbio.uuid = "ICMBIO/DIBIO/SISBIO/ID:" + it.sq_ocorrencia
                            mapDadosSisbio.recordNumber = 'Nº Da Autorização/Licença Sisbio: ' + it.nu_autorizacao + '. Projeto: ' + it.no_projeto
                            mapDadosSisbio.nomeCientifico = it.no_cientifico
                            mapDadosSisbio.dataOcorrencia = it.dt_inicio.format('dd/MM/yyyy')
                            mapDadosSisbio.carencia = it.dt_carencia ? it.dt_carencia.format('dd/MM/yyyy') : ''
                            mapDadosSisbio.estado = it.sg_estado
                            mapDadosSisbio.municipio = it.no_municipio
                            mapDadosSisbio.latitude = it.nu_latitude
                            mapDadosSisbio.longitude = it.nu_longitude
                            mapDadosSisbio.datum = it.sg_datum
                            mapDadosSisbio.fonte = "Sistema de Autorização e Informação em Biodiversidade - Sisbio"
                            mapDadosSisbio.instituicao = "ICMBio"
                            mapDadosSisbio.codigoColecao = "SISBIO-DIBIO"
                            mapDadosSisbio.autor = it.no_autor
                            mapDadosSisbio.projeto = it.no_projeto?:''
                            mapDadosSisbio.local = it.de_local
                            mapDadosSisbio.precisaoCoord = it.de_precisao_coordenada
                            mapDadosSisbio.ameaca = ""
                            mapDadosSisbio.alteracao = dataAlteracao.format('dd/MM/yyyy HH:mm:ss')
                            mapDadosSisbio.nomeBaseDados = 'ICMBIO/Sisbio'


                            Geometry geometry = null
                            try {
                                geometry = new GeometryFactory().createPoint(new Coordinate(mapDadosSisbio.longitude, mapDadosSisbio.latitude))
                                if( geometry ){
                                    geometry.SRID = 4674
                                }
                            } catch (Exception e) {
                            }

                            if ( ! geometry  || geometry.x < -180 || geometry.x > 180 || geometry.y < -90 || geometry.y > 90) {
                                throw new Exception('coordenadas invalidas ' + mapDadosSisbio.uuid + ' lon: ' +
                                    mapDadosSisbio.longitude + ', lat: ' + mapDadosSisbio.latitude)
                            }


                            // verificar se a ocorrencia foi transferida para outra ficha
                            reg = FichaOcorrenciaPortalbio.executeQuery('select new map(fo.id as id )' +
                                ' from FichaOcorrenciaPortalbio fo ' +
                                ' join fo.ficha ficha ' +
                                ' where ficha.id <> :sqFicha ' +
                                ' and fo.ficha.cicloAvaliacao.id = :sqCicloAvaliacao' +
                                ' and fo.deUuid = :deUuid', [sqFicha: data.idFicha.toLong()
                                                             , sqCicloAvaliacao: ficha.cicloAvaliacao.id
                                                             , deUuid: mapDadosSisbio.uuid] )

                            if ( reg.size() > 0) {
                                throw new Exception('')
                            }
                            Date dtOcorrencia = Date.parse("yyyy-MM-dd", it.dt_inicio.format('yyyy-MM-dd'))

                           // verificar se a ocorrencia ja existe
                            FichaOcorrenciaPortalbio temp

                            fop = FichaOcorrenciaPortalbio.findByFichaAndDeUuid(ficha, mapDadosSisbio.uuid)
                            if ( !fop ) {
                                temp = null
                                // procurar mesmo autor,Data e coords
                                fop = FichaOcorrenciaPortalbio.findByFichaAndNoAutorAndDtOcorrenciaAndGeometry(ficha, it.no_autor, dtOcorrencia, geometry)
                                if (!fop) {
                                    fop = new FichaOcorrenciaPortalbio()
                                    fop.ficha = ficha
                                    fop.deUuid = mapDadosSisbio.uuid
                                }
                            } else {
                                temp = fop
                            }

                            fop.geometry = geometry
                            if( !dtOcorrencia || dtOcorrencia == dt1500 ){
                                fop.inDataDesconhecida='S'
                                fop.dtOcorrencia = null
                            } else {
                                fop.inDataDesconhecida='N'
                                fop.dtOcorrencia = dtOcorrencia
                            }
                            // println dtOcorrencia
                            // println fop.inDataDesconhecida
                            // println ' '

                            fop.noAutor = mapDadosSisbio.autor
                            fop.noCientifico = mapDadosSisbio.nomeCientifico
                            fop.noLocal = mapDadosSisbio.local
                            fop.noInstituicao = mapDadosSisbio.instituicao
                            fop.txOcorrencia = (mapDadosSisbio as JSON).toString()
                            fop.idOrigem = it.sq_ocorrencia
                            fop.dtAlteracao = dataAlteracao
                            fop.noBaseDados = mapDadosSisbio.nomeBaseDados

                            if (mapDadosSisbio.carencia) {
                                fop.dtCarencia = new Date().parse("dd/MM/yyyy", mapDadosSisbio.carencia)
                            }

                            if (!fop.validate()) {
                                /** /
                            println ' '
                            println ' '
                            println 'Dados incompletos ou duplicados'
                            println 'Importacao ocorrencia SISBIO ' + mapDadosSisbio.uuid
                            println fop.getErrors()
                            /**/
                                if (temp && (fop.getErrors().toString() =~ 'unique')) {
                                    if (!temp?.inUtilizadoAvaliacao) {
                                        idsDuplicados.push(temp.deUuid)
                                    }
                                }
                            } else {
                                fop.save()
                            }
                        } catch (Exception error) {
                            //println error.getMessage()
                        }

                        // incrementar o andamento
                        workerService.stepIt(worker)
                    } // end each ocorrencias sisbio
                    d(' - Fim gravacao ocorrencias SISBIO')

                    if (idsDuplicados) {
                        d(' - Excluindo ' + idsDuplicados.size() + ' ocorrencias SISBIO duplicadas')
                        sqlService.execSql("delete from salve.ficha_ocorrencia_portalbio where ( in_utilizado_avaliacao is null or in_utilizado_avaliacao <> 'S') and sq_ficha = " + ficha.id + " and no_instituicao ilike 'ICMBIO' and de_uuid in (" + idsDuplicados.collect { "'" + it + "'" }.join(',') + ")")
                    }
                } else {
                    // nenhuma ocorrência encontrada
                    if (isDesenv()) {
                        println 'Nenhuma ocorrência encontrada no PORTALBIO nem no SISBIO'
                    }
                    if (worker.id) {
                        workerService.delete(worker.id, appSession)
                    }
                }
            }

        } catch ( Exception e ) {
            //println e.getMessage()
            if( worker.id ) {
                workerService.delete( worker.id,appSession )
            }
            throw new Exception('Erro: ImpOcoPortalbio de ' + data.taxon + ' JSON.parse!')
        }
        // remover a tarefa da sessao
        //appSession.tarefas[idTarefa].fim = new Date()
    }

    /**
     * Verificar se existe Ocorrencia cadastrada na subespecie
     * @param ficha
     * @return
     */
    boolean existeOcorrenciaSubespecie(Ficha ficha )
    {
        String cmdSql
        if( ficha.taxon.nivelTaxonomico.coNivelTaxonomico != 'ESPECIE' )
        {
            return false;
        }
        DadosApoio contextoOcorrencia = dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA', 'REGISTRO_OCORRENCIA' )
        if( !contextoOcorrencia){
            println ' '
            println 'Erro SALVE - CONTEXTO REGISTRO_OCORRENCIA não cadastrado na tabela de apoio TB_CONTEXTO_OCORRENCIA'
            return false
        }
        cmdSql = """with cte as (
                    select ficha.sq_taxon, ficha.sq_ficha, ficha.sq_ciclo_avaliacao
                    from salve.ficha
                    where ficha.sq_ficha = :sqFicha
                ),
                     cte2 as (
                         select cte.sq_ficha, cte.sq_ciclo_avaliacao, taxon.sq_taxon, nivel.co_nivel_taxonomico
                         from cte
                                  inner join taxonomia.taxon on (taxon.sq_taxon = cte.sq_taxon or taxon.sq_taxon_pai = cte.sq_taxon)
                                  inner join taxonomia.nivel_taxonomico as nivel
                                             on nivel.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
                     )
                select null
                from salve.ficha_ocorrencia_registro a
                         inner join salve.ficha_ocorrencia c on c.sq_ficha_ocorrencia = a.sq_ficha_ocorrencia
                         inner join salve.ficha on ficha.sq_ficha = c.sq_ficha
                         inner join cte2 on cte2.sq_taxon = ficha.sq_taxon
                where ficha.sq_ciclo_avaliacao = cte2.sq_ciclo_avaliacao
                  and (c.in_presenca_atual is null or c.in_presenca_atual <> 'N')
                  and (c.in_utilizado_avaliacao = 'S' OR
                       (c.sq_contexto = ${contextoOcorrencia.id.toString()} and c.in_utilizado_avaliacao is null))
                union
                select null
                from salve.ficha_ocorrencia_portalbio_reg a
                         inner join salve.ficha_ocorrencia_portalbio c
                                    on c.sq_ficha_ocorrencia_portalbio = a.sq_ficha_ocorrencia_portalbio
                         inner join salve.ficha on ficha.sq_ficha = c.sq_ficha
                         inner join cte2 on cte2.sq_taxon = ficha.sq_taxon
                where ficha.sq_ciclo_avaliacao = cte2.sq_ciclo_avaliacao
                  and c.in_utilizado_avaliacao = 'S'
                limit 1
                """
        Map params = [ 'sqFicha':ficha.id ]
        List res = sqlService.execSql( cmdSql, params )
        return res.size() > 0

        /*
        //println ' '
        //println 'Verificando se a especia  '+ ficha.nmCientifico+' tem alguma subespecie com coordenadas.'
        String cmdSql = """select ficha_ocorrencia.sq_ficha_ocorrencia
                from salve.ficha_ocorrencia
                where ficha_ocorrencia.sq_contexto = ( select sq_dados_apoio from salve.dados_apoio where cd_sistema='REGISTRO_OCORRENCIA' and sq_dados_apoio_pai = ( \tselect sq_dados_apoio from salve.dados_apoio where cd_dados_apoio='TB_CONTEXTO_OCORRENCIA' limit 1) )
                  and coalesce(ficha_ocorrencia.in_utilizado_avaliacao,'S') = 'S'
                and ficha_ocorrencia.sq_ficha in (
                    select ficha.sq_ficha from salve.ficha
                    inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                    where ficha.sq_ciclo_avaliacao = :sqCicloAvaliacao
                    and taxon.sq_taxon_pai  = :sqTaxonEspecie
                    )
                limit 1 """

        Map params = [ 'sqCicloAvaliacao':ficha.cicloAvaliacao.id, 'sqTaxonEspecie': ficha.taxon.id ]
        List res = sqlService.execSql( cmdSql, params )
        //println 'Registros encontrados'
        //println res
        //println ' '
        //println ' '
        return res.size() > 0
        */
    }

    /**
     * Verificar se existe UF cadastrada na subespecie
     * @param ficha
     * @return
     */
    boolean existeUfSubespecie(Ficha ficha ) {
        if( ficha.taxon.nivelTaxonomico.coNivelTaxonomico != 'ESPECIE' )
        {
            return false;
        }
// println 'VERIFICANDO SUBESPECIE UF'
        // NOVO-REGISTRO
        List listFichasSub =  ficha.getFichasSubespecie()
        if( listFichasSub ) {
            List result = FichaUf.executeQuery("""select a.estado from FichaUf a where a.ficha.id in (${listFichasSub.sqFicha.join(',')})""", [max: 1])

            if (result.size() > 0) {
                return true
            }
            // procurar nos registros de ocorrencia
// println 'VERIFICANDO SUBESPECIE REGISTRO UF'

            result = FichaOcorrenciaRegistro.executeQuery("""select re.estado from FichaOcorrenciaRegistro a, RegistroEstado re
                            where a.fichaOcorrencia.ficha.id in ( ${listFichasSub.sqFicha.join(',')} ) and a.registro = re.registro
                              and ( a.fichaOcorrencia.inUtilizadoAvaliacao is null or a.fichaOcorrencia.inUtilizadoAvaliacao='S' )""", [max: 1])
            if (result.size() > 0) {
                return true
            }
            // procurar registros do portalbio
// println 'VERIFICANDO SUBESPECIE REGISTRO PORTALBIO UF'

            result = FichaOcorrenciaPortalbioReg.executeQuery("""select re.estado from FichaOcorrenciaPortalbioReg a, RegistroEstado re
                                where a.fichaOcorrenciaPortalbio.ficha.id in ( ${listFichasSub.sqFicha.join(',')} ) and a.registro = re.registro
                                  and a.fichaOcorrenciaPortalbio.inUtilizadoAvaliacao = 'S'
                                """, [max: 1])
            if (result.size() > 0) {
                return true
            }
        }
    }

    /**
     * Verificar se existe BIOMA cadastrado na subespecie
     * @param ficha
     * @return
     */
    boolean existeBiomaSubespecie(Ficha ficha ) {
        if( ficha.taxon.nivelTaxonomico.coNivelTaxonomico != 'ESPECIE' )
        {
            return false;
        }
// println 'VERIFICANDO SUBESPECIE BIOMA'

        List listFichasSub =  ficha.getFichasSubespecie()
        if( listFichasSub ) {
            List result = FichaUf.executeQuery("""select a.bioma from FichaBioma a where a.ficha.id in (${ listFichasSub.sqFicha.join(',') })""", [max: 1])
            if( result.size() > 0 ) {
                return true
            }
            // procurar registro ocorrencias SALVE
// println 'VERIFICANDO SUBESPECIE REGISTRO BIOMA'

            result = FichaOcorrenciaRegistro.executeQuery("""select re.bioma from FichaOcorrenciaRegistro a, RegistroBioma re
                            where a.fichaOcorrencia.ficha.id in ( ${listFichasSub.sqFicha.join(',')} ) and a.registro = re.registro
                              and ( a.fichaOcorrencia.inUtilizadoAvaliacao is null or a.fichaOcorrencia.inUtilizadoAvaliacao='S' )""",[max: 1])
            if( result.size() > 0 ) {
                return true
            }
            // procurar nos registros do portalbio
 // println 'VERIFICANDO SUBESPECIE REGISTRO PORTALBIO BIOMA'

            result = FichaOcorrenciaPortalbioReg.executeQuery("""select re.estado from FichaOcorrenciaPortalbioReg a, RegistroEstado re
                                where a.fichaOcorrenciaPortalbio.ficha.id in ( ${listFichasSub.sqFicha.join(',')} ) and a.registro = re.registro
                                  and a.fichaOcorrenciaPortalbio.inUtilizadoAvaliacao = 'S'
                                """,[max: 1])
            if( result.size() > 0 ) {
                return true
            }
        }
        return false
    }

    /**
     * Verificar se existe UC cadastrada na subespecie
     * @param ficha
     * @return
     */
    boolean existeUCSubespecie(Ficha ficha )
    {
        if( ficha.taxon.nivelTaxonomico.coNivelTaxonomico != 'ESPECIE' )
        {
            return false;
        }
        List res = sqlService.execSql("select sq_uc from salve.fn_ficha_ucs(:sqFicha) LIMIT 1;",[sqFicha:ficha.id] )

        //println ' '
        //println 'Verificando se a especia '+ ficha.nmCientifico+' tem alguma subespecie com UC.'
        /*String cmdSql = """select ficha_ocorrencia.sq_ficha_ocorrencia
                from salve.ficha_ocorrencia
                where ficha_ocorrencia.sq_contexto = ( select sq_dados_apoio from salve.dados_apoio where cd_sistema='REGISTRO_OCORRENCIA' and sq_dados_apoio_pai = ( select sq_dados_apoio from salve.dados_apoio where cd_dados_apoio='TB_CONTEXTO_OCORRENCIA' limit 1) )
                and coalesce(ficha_ocorrencia.in_utilizado_avaliacao,'S') = 'S'
                and not ( ficha_ocorrencia.sq_uc_federal is null and ficha_ocorrencia.sq_uc_estadual is null and ficha_ocorrencia.sq_rppn is null )
                 and ficha_ocorrencia.sq_ficha in (
                    select ficha.sq_ficha from salve.ficha
                    inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                    where ficha.sq_ciclo_avaliacao = :sqCicloAvaliacao
                    and taxon.sq_taxon_pai  = :sqTaxonEspecie
                    )
                limit 1 """
        Map params = [ 'sqCicloAvaliacao':ficha.cicloAvaliacao.id, 'sqTaxonEspecie': ficha.taxon.id ]
        List res = sqlService.execSql( cmdSql, params )
        */
        //println 'Registros encontrados'
        //println res
        //println ' '
        //println ' '
        return res.size() > 0
    }

    /**
     * método para fazer a verificação de uma espécie entrou ou saiu da lista cites
     * utilizando a API do speciesplus
     * @param sqFicha
     */
    def checkEntrouSaiuListaCites(Long sqFicha = 0, Integer intervaloDias = 30 ) {

        runAsync {
            Long intervaloEmMinutos = intervaloDias * 24 * 60
            String urlApi = 'https://api.speciesplus.net/api/v1/taxon_concepts'
            String accessToken = 'Zu1zfJiEAL79BkwptTaDcgtt'
            JSONObject resultado
            //println ' '
            //println ' '
            //println 'INICIANDO SINCRONISMO CITES'
            try {
                DadosApoio tipoSincronismo = dadosApoioService.getByCodigo('TB_TIPO_SINCRONISMO', 'SINCRONISMO_CITES')
                if (!tipoSincronismo) {
                    throw new Exception('Tipo de sincronismo SINCRONISMO_CITES não cadastrado na tabela TB_TIPO_SINCRONISMO')
                }

                // ler as 3 listas cites
                List tabelaConvencoes = dadosApoioService.getTable('TB_CONVENCOES').findAll {
                    it.codigoSistema =~ /^(CITES_ANEXO_I|CITES_ANEXO_II|CITES_ANEXO_III)$/
                }
                if (!tabelaConvencoes) {
                    throw new Exception('Tabela de lista e convenções CITES_ANEXO_I, CITES_ANEXO_II, CITES_ANEXO_III não cadastrados na TB_CONVENCOES')
                }

                // verificar se a ficha existe
                String cmdSql = """SELECT ficha.sq_ficha
                             , coalesce( taxon.json_trilha->'subespecie'->>'no_taxon', taxon.json_trilha->'especie'->>'no_taxon') as no_taxon
                             , sincronismo.dt_sincronismo
                             , sincronismo.sq_ficha_sincronismo
                             , coalesce(sincronismo.st_vencido,true)::boolean as st_vencido
                        FROM salve.ficha
                        inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                        left join lateral (
                                select fs.sq_ficha_sincronismo
                                , fs.dt_sincronismo
                                , (fs.dt_sincronismo + INTERVAL '${intervaloDias.toString()} days' ) < now() as st_vencido
                                from salve.ficha_sincronismo fs
                                inner join salve.dados_apoio as tipo on tipo.sq_dados_apoio = fs.sq_tipo_sincronismo
                                where fs.sq_ficha = ficha.sq_ficha
                                  and tipo.cd_sistema = 'SINCRONISMO_CITES') as sincronismo on true
                        where ficha.sq_ficha = :sqFicha
                        """
                List rows = sqlService.execSql(cmdSql, [sqFicha: sqFicha])
                if (!rows) {
                    throw new Exception('Ficha não encontrada')
                }
                //println '- Ficha: ' + rows[0].no_taxon

                // verificar se o último sincronismo está fora do intervalo
                //println '- Último sincronismo: ' + rows[0].dt_sincronismo
                //println '- Passados '+intervaloDias+' dias..: ' + (rows[0].st_vencido ? 'SIM' : 'NÃO')

                if (!rows[0].st_vencido) {
                    throw new Exception('') // encerrar
                }

                // consultar API
                String urlFinal = urlApi + "?per_page=1&name=${rows[0].no_taxon}"

                //println '-  Consultando API: ' + urlFinal

                resultado = requestService.doGetJson(urlFinal, ['X-Authentication-Token': accessToken])

                // TESTE COM RESULTADO VAZIO
                //JSONObject resultado = JSON.parse("""{"taxon_concepts": [],"pagination": {"per_page": 500,"total_entries": 0,"current_page": 1 }}""")

                // TESTE COM RESULTADO COM 1 LISTAS CITES
                //JSONObject resultado = JSON.parse("""{"pagination":{"per_page":500,"current_page":1,"total_entries":1},"taxon_concepts":[{"full_name":"Puma concolor","updated_at":"2022-04-14T14:45:47.638Z","author_year":"(Linnaeus, 1771)","name_status":"A","synonyms":[{"full_name":"Felis concolor","author_year":"Linnaeus, 1771","rank":"SPECIES","id":32243},{"full_name":"Puma concolor concolor","author_year":"Linnaeus, 1771","rank":"SUBSPECIES","id":98926}],"cites_listing":"I","rank":"SPECIES","active":true,"common_names":[{"name":"puma americká","language":"SK"},{"name":"puma","language":"SL"},{"name":"Puma-da-américa-central","language":"PT"},{"name":"Puma kostarykańska","language":"PL"},{"name":"Costa Ricaanse poema","language":"NL"},{"name":"Puma","language":"ES"},{"name":"León americano","language":"ES"},{"name":"Mitzli","language":"ES"},{"name":"León bayo","language":"ES"},{"name":"Onza bermeja","language":"ES"},{"name":"Puma centroamericano","language":"ES"},{"name":"puma","language":"SV"},{"name":"bergslejon","language":"SV"},{"name":"kuguar","language":"SV"},{"name":"centralamerikansk puma","language":"SV"},{"name":"Puma","language":"FR"},{"name":"Puma d'Amérique centrale","language":"FR"},{"name":"Kostarikanska puma","language":"HR"},{"name":"leone di montagna","language":"IT"},{"name":"Puma","language":"IT"},{"name":"coguaro","language":"IT"},{"name":"costa rica cougar","language":"DA"},{"name":"Costa-Rica-Puma","language":"DE"},{"name":"Red Tiger","language":"EN"},{"name":"Cougar","language":"EN"},{"name":"Deer Tiger","language":"EN"},{"name":"Mountain Lion","language":"EN"},{"name":"Puma","language":"EN"},{"name":"Costa Rican cougar","language":"EN"},{"name":"Костариканска пума","language":"BG"},{"name":"puma","language":"CS"},{"name":"Πούμα της Κεντρικής Αμερικής","language":"EL"},{"name":"Costa Rica puuma","language":"ET"},{"name":"Costa Rican cougar","language":"FI"},{"name":"Costa Rica-i puma","language":"HU"},{"name":"Vidusamerikas puma","language":"LV"},{"name":"Puma","language":"LT"},{"name":"Puma tal-Costa Rica","language":"MT"},{"name":"Pumă de America","language":"RO"},{"name":"Cúgar Chósta Ríce","language":"GA"}],"id":6330,"higher_taxa":{"phylum":"Chordata","family":"Felidae","kingdom":"Animalia","class":"Mammalia","order":"Carnivora"},"cites_listings":[{"annotation":"Only the populations of Costa Rica and Panama; all other populations are included in Appendix II","hash_annotation":null,"effective_at":"2019-11-26","appendix":"I","id":30233,"party":null}]}]}""")

                // TESTE COM RESULTADO COM 2 LISTAS CITES
                //JSONObject resultado = JSON.parse("""{"pagination":{"per_page":500,"current_page":1,"total_entries":1},"taxon_concepts":[{"full_name":"Puma concolor","updated_at":"2022-04-14T14:45:47.638Z","author_year":"(Linnaeus, 1771)","name_status":"A","synonyms":[{"full_name":"Felis concolor","author_year":"Linnaeus, 1771","rank":"SPECIES","id":32243},{"full_name":"Puma concolor concolor","author_year":"Linnaeus, 1771","rank":"SUBSPECIES","id":98926}],"cites_listing":"I/II","rank":"SPECIES","active":true,"common_names":[{"name":"puma americká","language":"SK"},{"name":"puma","language":"SL"},{"name":"Puma-da-américa-central","language":"PT"},{"name":"Puma kostarykańska","language":"PL"},{"name":"Costa Ricaanse poema","language":"NL"},{"name":"Puma","language":"ES"},{"name":"León americano","language":"ES"},{"name":"Mitzli","language":"ES"},{"name":"León bayo","language":"ES"},{"name":"Onza bermeja","language":"ES"},{"name":"Puma centroamericano","language":"ES"},{"name":"puma","language":"SV"},{"name":"bergslejon","language":"SV"},{"name":"kuguar","language":"SV"},{"name":"centralamerikansk puma","language":"SV"},{"name":"Puma","language":"FR"},{"name":"Puma d'Amérique centrale","language":"FR"},{"name":"Kostarikanska puma","language":"HR"},{"name":"leone di montagna","language":"IT"},{"name":"Puma","language":"IT"},{"name":"coguaro","language":"IT"},{"name":"costa rica cougar","language":"DA"},{"name":"Costa-Rica-Puma","language":"DE"},{"name":"Red Tiger","language":"EN"},{"name":"Cougar","language":"EN"},{"name":"Deer Tiger","language":"EN"},{"name":"Mountain Lion","language":"EN"},{"name":"Puma","language":"EN"},{"name":"Costa Rican cougar","language":"EN"},{"name":"Костариканска пума","language":"BG"},{"name":"puma","language":"CS"},{"name":"Πούμα της Κεντρικής Αμερικής","language":"EL"},{"name":"Costa Rica puuma","language":"ET"},{"name":"Costa Rican cougar","language":"FI"},{"name":"Costa Rica-i puma","language":"HU"},{"name":"Vidusamerikas puma","language":"LV"},{"name":"Puma","language":"LT"},{"name":"Puma tal-Costa Rica","language":"MT"},{"name":"Pumă de America","language":"RO"},{"name":"Cúgar Chósta Ríce","language":"GA"}],"id":6330,"higher_taxa":{"phylum":"Chordata","family":"Felidae","kingdom":"Animalia","class":"Mammalia","order":"Carnivora"},"cites_listings":[{"annotation":"Only the populations of Costa Rica and Panama; all other populations are included in Appendix II","hash_annotation":null,"effective_at":"2019-11-26","appendix":"I","id":30233,"party":null},{"annotation":"[FAMILY listing Felidae spp.] Included in Felidae spp. except the populations of Costa Rica and Panama, which are included in Appendix I. Specimens of the domesticated form are not subject to the provisions of the Convention.","hash_annotation":null,"effective_at":"2019-11-26","appendix":"II","id":30291,"party":null}]}]}""")


                if (!resultado) {
                    throw new Exception('Falha ao consultar a API')
                }

                /** /
                 println '-' * 50
                 println resultado.toString()
                 println '-' * 50
                 /**/

                //println '-  respondeu as ' + new Date().format('dd/MM/yyyy HH:mm:ss')
                //println '-  parsing....'

                List listas = []
                if (resultado.taxon_concepts) {
                    resultado.taxon_concepts.cites_listings[0].each { lista ->
                        Long sqLista = tabelaConvencoes.find {
                            (it.codigoSistema == 'CITES_ANEXO_' + lista.appendix)
                        }?.id?.toLong()

                        if (sqLista) {
                            Integer ano = (lista.effective_at.split('-')[0]).toInteger()
                            listas.push([appendix      : lista.appendix
                                         , annotation  : lista.annotation
                                         , effective_at: lista.effective_at
                                         , ano         : ano
                                         , sqLista     : sqLista
                            ])
                        } else {
                            throw new Exception('CITES_ANEXO_' + lista.appendix + ' não cadastrado na tabela TB_CONVENCOES')
                        }
                    }
                }

                //println '-  parsed.'
                //println '-  ' + listas.size() + ' Lista Encontrada'
                ////println listas


                // ATUALIZAR a tabela ficha_sincronismo
                FichaSincronismo fichaSincronismo
                if (rows[0].sq_ficha_sincronismo) {
                    fichaSincronismo = FichaSincronismo.get(rows[0].sq_ficha_sincronismo.toLong())
                } else {
                    fichaSincronismo = new FichaSincronismo(ficha: Ficha.get(sqFicha), tipo: tipoSincronismo, nuMinutosIntervalo: intervaloEmMinutos)
                }
                fichaSincronismo.dtSincronismo = new Date()
                fichaSincronismo.save(flush: true)
                //println '- data ultimo sincronismo atualizada'

                // atualizar a tabela ficha_lista_convencao
                List listFichaConvencao = FichaListaConvencao.createCriteria().list {
                    eq('ficha.id', sqFicha)
                    'in'('listaConvencao', tabelaConvencoes)
                }
                // se listas vazia a espécie não pertence a nenhuma lista
                if (!listas) {
                    // remover a ficha das listas CITES
                    //println '- removendo listas CITES incorretas, caso existam'
                    listFichaConvencao.each {
                        it.delete(flush: true)
                    }
                } else {
                    // remover as que não tem e adicionar as que existem
                    //println '- removendo listas CITES incorretas, caso existam'
                    listFichaConvencao.each {
                        if (!listas.sqLista.contains(it.listaConvencao.id)) {
                            it.delete(flush: true)
                        }
                    }
                    // adicionar as novas listas se não existir lista já cadastrada
                    if (!listFichaConvencao) {
                        //println '- adicionando lista CITES corretas, caso não exista'
                        listas.each {
                            if (!listFichaConvencao?.listaConvencao?.id?.contains(it.sqLista)) {
                                new FichaListaConvencao(
                                        ficha: fichaSincronismo.ficha
                                        , nuAno: it.ano
                                        , listaConvencao: DadosApoio.get(it.sqLista)).save(flush: true)
                            }
                        }
                        // gerar pendencia de ajuste se a especie estiver em mais de uma lista
                        if (listas.size() > 1) {
                            FichaPendencia fichaPendencia = FichaPendencia.findByFichaAndDeAssunto(fichaSincronismo.ficha, 'Duplicidade CITES')
                            if (!fichaPendencia) {
                                //println '- especie esta em mais de uma lista, gerando pendência de duplicidade cites...'
                                fichaPendencia = new FichaPendencia(ficha: fichaSincronismo.ficha
                                        , deAssunto: 'Duplicidade CITES'
                                )
                            }
                            fichaPendencia.usuario = PessoaFisica.get(fichaSincronismo.ficha.alteradoPor.id)
                            fichaPendencia.contexto = dadosApoioService.getByCodigo('TB_CONTEXTO_PENDENCIA', 'SISTEMA') // usuario não pode editar nem excluir
                            fichaPendencia.stPendente = 'S'
                            fichaPendencia.dtPendencia = new Date()
                            fichaPendencia.txPendencia = "<p>Corrigir a inclusão duplicada da CITES - Aba 7.2</p><p><b>Resultado da consulta ao speciesplus.net</b></p>"
                            listas.each { lista ->
                                fichaPendencia.txPendencia += '<p>Apendice ' + lista.appendix + ' - ' + lista.annotation + '<br>Effective date: ' + lista.effective_at + '</p>'
                            }
                            fichaPendencia.save(flush: true)
                            //println '- pendencia gravada com sucesso : ' + fichaPendencia.usuario.noPessoa
                        }
                    }
                }

                // verificar se existe pendencia de CITES incorreta e remove-la
                //println '- removendo pendencia cites incorreta caso exista'
                removerPendenciaDuplicidadeCites(sqFicha)

            } catch (Exception e) {
                //e.printStackTrace()
                if (e.getMessage()) {
                    Util.printLog('AsyncService.checkEntrouSaiuListaCites', 'Verificando ficha ' +
                            sqFicha, e.getMessage() +
                            (resultado ? "\nResultado API:" + resultado.toString() : '')
                    )
                }
            }
            // println ' '
            // println 'Fim - ficha:'+sqFicha+' - sincronismo CITES - ' + new Date().format('dd/MM/yyyy HH:mm:ss')
            // println ' '
        }
    }

    /**
     * método para fazer a atualização do historico de avaliação da espécie
     * utilizando a API do SALVE ICMBio
     * @param sqFicha
     */
    def updateHistoricoAvaliacao(Long sqFicha = 0, Integer intervaloDias = 30 ) {
        ApplicationContext ctx = Holders.grailsApplication.mainContext
        String tempDir = ctx.grailsApplication.config.temp.dir

        Ficha ficha = Ficha.get(sqFicha)

        if( !ficha ){
            return
        }

        Configuracao configuracao = Configuracao.findByEstadoIsNotNull()
        if( !configuracao ){
            return
        }

        // VERIFICAR INTERVALO DE VERIFICAÇÕES
        DadosApoio tipoSincronismo = dadosApoioService.getByCodigo('TB_TIPO_SINCRONISMO', 'SINCRONISMO_HISTORICO_AVALIACAO')
        FichaSincronismo fichaSincronismo = FichaSincronismo.findByFichaAndTipo(ficha,tipoSincronismo )
        if( fichaSincronismo ){
            if( Util.dateDiff(fichaSincronismo.dtSincronismo, new Date(),'D') <= intervaloDias ) {
                return
            }
        }

        runAsync {
            Long intervaloEmMinutos = intervaloDias * 24 * 60
            String sgEstado = configuracao.estado.sgEstado
            String urlApi = ctx.grailsApplication.config.url.salve.icmbio.api ?: 'https://salve.icmbio.gov.br/salve-api/'

            //urlApi='http://salve.dev.icmbio.gov.br:8090/salve-api/'

            JSONObject resultado
            try {
                String urlRequest = urlApi+'api/historicoAvaliacao?noCientifico='+ficha.taxon.noCientifico
                if (!tipoSincronismo) {
                    throw new Exception('Tipo de sincronismo SINCRONISMO_HISTORICO_AVALIACAO não cadastrado na tabela TB_TIPO_SINCRONISMO')
                }

                resultado = requestService.doGetJson(urlRequest)
                if( ! resultado.data ){
                    throw new Exception(resultado.msg)
                }
                resultado.data.each { row ->
                    DadosApoio tipoDaAvaliacao = dadosApoioService.getByCodigo('TB_TIPO_AVALIACAO',row.cd_tipo_sistema)
                    DadosApoio categoriaIucn = dadosApoioService.getByCodigo('TB_CATEGORIA_IUCN',row.cd_categoria_sistema)
                    // sincronizar com a tabela taxon_historico_avaliacao
                    // se for do mesmo Estado, ignorar
                    // se for de outro estado, localizar UF + ANO + ID ORIGEM
                    TaxonHistoricoAvaliacao th
                    if( row.cd_tipo_sistema == 'ESTADUAL') {
                        if ( row.sg_estado != sgEstado) {
                            DadosApoio tipoEstado = dadosApoioService.getByCodigo('TB_TIPO_ABRANGENCIA', 'ESTADO')
                            Estado estado = Estado.findBySgEstado(row.sg_estado)
                            Abrangencia abrangencia = Abrangencia.findByTipoAndEstado(tipoEstado, estado)
                            th = TaxonHistoricoAvaliacao.createCriteria().get {
                                eq('taxon', ficha.taxon)
                                eq('tipoAvaliacao', tipoDaAvaliacao)
                                eq('abrangencia', abrangencia)
                                eq('nuAnoAvaliacao', row.nu_ano_avaliacao.toInteger())
                                eq('idOrigem',row.id_origem)
                            }
                            if( !th ){
                                th = new TaxonHistoricoAvaliacao()
                                th.taxon = ficha.taxon
                                th.tipoAvaliacao = tipoDaAvaliacao
                                th.abrangencia = abrangencia
                                th.nuAnoAvaliacao = row.nu_ano_avaliacao
                                th.idOrigem = row.id_origem
                            }
                            // atualizar
                            th.categoriaIucn = categoriaIucn
                            th.deCriterioAvaliacaoIucn = row.ds_criterio?:null
                            th.stPossivelmenteExtinta = row.st_possivelmente_extinta?:null
                            th.txJustificativaAvaliacao = row.tx_justificativa?:null
                            th.stOficial = row.st_oficial
                            th.save()
                        }
                    } else if ( row.cd_tipo_sistema == 'NACIONAL_BRASIL') {

                        th = TaxonHistoricoAvaliacao.createCriteria().get {
                            eq('taxon', ficha.taxon)
                            eq('tipoAvaliacao', tipoDaAvaliacao)
                            eq('nuAnoAvaliacao', row.nu_ano_avaliacao)
                            eq('idOrigem',row.id_origem)
                        }
                        if( !th ){
                            th = new TaxonHistoricoAvaliacao()
                            th.taxon = ficha.taxon
                            th.tipoAvaliacao = tipoDaAvaliacao
                            th.nuAnoAvaliacao = row.nu_ano_avaliacao
                            th.idOrigem = row.id_origem
                        }
                        // atualizar
                        th.categoriaIucn = categoriaIucn
                        th.deCriterioAvaliacaoIucn = row.dsCriterio?:null
                        th.stPossivelmenteExtinta = row.st_possivelmente_extinta?:null
                        th.txJustificativaAvaliacao = row.tx_justificativa?:null
                        th.stOficial = row.st_oficial
                        th.idOrigem = row.id_origem
                        th.save()
                    }
                }

                // EXCLUIR REGISTROS QUE FORAM EXCLUIDOS NAS ORIGENS
                List idsRecebidos = resultado.data.id_origem
                if( idsRecebidos) {
                    TaxonHistoricoAvaliacao.createCriteria().list {
                        eq('taxon', ficha.taxon)
                        isNotNull('idOrigem')
                        not {
                            'in'('idOrigem', resultado.data.id_origem)
                        }
                    }.each {
                        it.delete()
                    }
                }

                // ATUALIZAR TABELA DE SINCRONISMOS REALIZADOS
                if ( ! fichaSincronismo ) {
                    fichaSincronismo = new FichaSincronismo(ficha: ficha, tipo: tipoSincronismo, nuMinutosIntervalo: intervaloEmMinutos)
                } else {
                    fichaSincronismo.dtSincronismo = new Date()
                }
                fichaSincronismo.save()

            } catch (Exception e) {
                //e.printStackTrace()
                if (e.getMessage()) {
                    Util.printLog('AsyncService.updateHistoricoAvaliacao', 'Verificando ficha ' +
                            sqFicha, e.getMessage() +
                            (resultado ? "\nResultado API:" + resultado.toString() : '')
                    )
                    println ' '
                    println ' '
                }
            }
            // println ' '
            // println 'Fim - ficha:'+sqFicha+' - sincronismo CITES - ' + new Date().format('dd/MM/yyyy HH:mm:ss')
            // println ' '
        }
    }



    /**
     * método para remover a pendência automática gerada na integração do SALVE
     * com a API do speciesplus quando a espécie estiver em mais de uma lista
     * cites e o usuário excluir a errada
     * @param sqFicha
     */
    void removerPendenciaDuplicidadeCites(Long sqFicha= 0 ){
        if( ! sqFicha ) {
            return
        }
        //println ' '
        //println 'Verificando se a ficha tem 2 CITES'
        Integer qtd = FichaListaConvencao.createCriteria().count{
            eq('ficha.id',sqFicha)
            //ne('id',reg.id)
            listaConvencao {
                like('codigoSistema', '%CITES%')
            }
        }
        //println 'Ficha tem '+ qtd+ ' cites'
        if( qtd < 2 ){
            //println 'Verificando se tem pendencia...'
            DadosApoio contextoSistema = dadosApoioService.getByCodigo('TB_CONTEXTO_PENDENCIA','SISTEMA');
            FichaPendencia.createCriteria().list {
                eq('ficha.id',sqFicha)
                eq('contexto',contextoSistema)
                eq('deAssunto','Duplicidade CITES')
                //maxResults(1)
            }.each {
                //println 'pendencia encontrada'
                it.delete()
                //println 'Excuir pendencia id ' + it.id
            }
        }
    }
}
