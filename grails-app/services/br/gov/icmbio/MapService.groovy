package br.gov.icmbio

// import grails.converters.JSON
// import grails.util.Environment

class MapService {
    static transactional = false
    def sqlService


    /**
     * método para selecionar os ids das fichas que são subespecies da ficha informada
     * @param sqFicha
     * @return
     */
    public List getIdsFichasSubespecies( Long sqFicha = 0  ) {
        List result = sqlService.execSql("""with cteFicha as (
                    select sq_ficha, sq_taxon, sq_ciclo_avaliacao
                    from salve.ficha where sq_ficha = :sqFicha
                )
                select sq_ficha from salve.ficha
                inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                where taxon.sq_taxon_pai = ( select cteFicha.sq_taxon from cteFicha )
                and ficha.sq_ciclo_avaliacao = ( select cteFicha.sq_ciclo_avaliacao from cteFicha )
                """,[sqFicha:sqFicha])
        return result ? result.sq_ficha : []
    }

    /**
     * método para construi o texto que será exibido no popup do mapa ao clicar em algum ponto
     * @param rowData
     * @return String
     */
    private  String _buildTextInfo( Map rowData = [:], Map params = [:] ) {
        List infoRows = []
        List columns = [
            [name: 'no_base_dados', label: '',showWhenCentroide:true]
            , [name: 'nm_cientifico', label: '',showWhenCentroide:true]
            , [name: 'no_local', label: 'Local',showWhenCentroide:false]
            , [name: 'no_estado', label: 'Estado',showWhenCentroide:true]
            , [name: 'no_municipio', label: 'Municipio',showWhenCentroide:false]
            , [name: 'nu_lat', label: '',showWhenCentroide:false]
            , [name: 'nu_lon', label: '',showWhenCentroide:false]
            , [name: 'ds_precisao', label: 'Precisão da coordenada',showWhenCentroide:false]
            , [name: 'no_responsavel', label: 'Responsável',showWhenCentroide:false]
            , [name: 'dt_ocorrencia', label: 'dt_ocorrencia',showWhenCentroide:true]
            , [name: 'ds_sensivel', label: 'Sensível',showWhenCentroide:false]
            , [name: 'ds_presenca_atual_coordenada', label: 'Presença atual na coordenada',showWhenCentroide:false]
            , [name: 'ds_tombamento', label: 'Nº tombamento',showWhenCentroide:false]
            , [name: 'dt_coleta', label: 'Coletado em',showWhenCentroide:true]
            , [name: 'no_autor', label: 'Autor',showWhenCentroide:true] // portalbio

            // não mostrar mais estes campos - reunião dia 01/0*/021
            //, [name: 'no_pessoa_inclusao', label: '',showWhenCentroide:true]
            //, [name: 'dt_inclusao', label: '',showWhenCentroide:true]

            , [name: 'ds_carencia', label: 'Data de carência',showWhenCentroide:false]
            , [name: 'ds_situacao_avaliacao', label: '',showWhenCentroide:false]
            , [name: 'json_ref_bib', label: 'Referência bibliográfica',showWhenCentroide:true]
            , [name: 'no_usuario_consulta', label: 'Colaborador(a)',showWhenCentroide:false]
            //, [name: 'ds_tipo_consulta'     , label: 'Tipo consulta',showWhenCentroide:false]
            , [name: 'no_consulta', label: '',showWhenCentroide:false]
            , [name: 'ds_periodo', label: 'Período',showWhenCentroide:false]
            , [name: 'tx_observacao_usuario_web', label: '',showWhenCentroide:true]
            , [name: 'tx_observacao', label: 'Observação',showWhenCentroide:true]
            // campos especificos do portabio
            , [name: 'ds_autorizacao_sisbio', label: '',showWhenCentroide:false] // portalbio
            , [name: 'ds_uuid', label: 'UUID',showWhenCentroide:true]
            , [name: 'id_origem', label: 'Id origem',showWhenCentroide:true]
        ]

        Boolean isCentroide = ( !rowData['in_centroide_estado'] ? false : true)

        // pre-processar par criar as colunas inexistentes
        columns.each { col ->
            if ( ! rowData.containsKey(col.name)) {
                rowData[col.name] = ''
            }
        }

        try {
            columns.each { col ->
                String value = ''
                String label = col.label
                String startBold = '<b>'
                String endBold = '</b>'

                if( ! isCentroide || col.showWhenCentroide ) {
                    value = rowData[col.name]

                    // nas ocorrencias do SALVE o autor é quem cadastrou a ocorrência
                    if (col.name == 'no_autor') {
                        if( ! value ){
                            value = Util.nomeAbreviado(rowData['no_pessoa_inclusao'])
                        }
                    }


                    if (value) {
                        // fazer tratamentos específicos
                        if (col.name =~ /in_subespecie|nu_lon|no_pessoa_inclusao|ds_motivo_nao_utilizado|tx_justificativa_nao_utilizado/) {
                            value = '' // colunas que serão impressas juntas com alguma outra
                        } else if (col.name == 'no_base_dados') {
                            value = '<span class="ol3-popup-base-dados">' + rowData.no_base_dados + '</span>'
                        } else if (col.name == 'nm_cientifico') {
                            value = '<span class="ol3-popup-nome-cientifico' + (rowData.in_subespecie == 'S' ? ' ol3-popup-subespecie' : '') +
                                '">' + Util.ncItalico(value, true, true) + '</span>'
                        } else if (col.name == 'nu_lat') {
                            value = 'Lat(y): <b>' + _roundCoord(value.toBigDecimal()).toString() + '</b>   Lon(x): <b>' + _roundCoord(rowData.nu_lon).toString() + '</b>'
                            // desabilitar o negrito da linha
                            startBold = ''
                            endBold = ''
                        } else if (col.name == 'dt_inclusao') {
                            value = 'Cadastrado em: <b>' + rowData[col.name] + '</b>' + (!rowData['no_pessoa_inclusao'] ? '' : '  por: <b>' + Util.nomeAbreviado(rowData['no_pessoa_inclusao']) + '</b>')
                            // desabilitar o negrito da linha
                            startBold = ''
                            endBold = ''
                        } else if (col.name == 'ds_situacao_avaliacao') {
                            String textUtilizadoNaoUtilizao = 'Registro ' + rowData.ds_situacao_avaliacao.toString().toLowerCase()
                            String textMotivoNaoUtilizado = ''
                            String textJustificativaNaoUtilizado = ''

                            if( rowData?.in_presenca_atual == 'N'){
                                textUtilizadoNaoUtilizao +='&nbsp;<span class="red">(histórico)</span>'
                            }
                            startBold = ''
                            endBold = ''
                            if (rowData.cd_situacao_avaliacao =~ /REGISTRO_NAO_UTILIZADO_AVALIACAO|REGISTRO_EXCLUIDO/) {
                                if (rowData.ds_motivo_nao_utilizado) {
                                    textMotivoNaoUtilizado = '<br>Motivo: <b>' + rowData.ds_motivo_nao_utilizado + '</b>'
                                }
                                if (rowData.tx_justificativa_nao_utilizado) {
                                    if (rowData.tx_justificativa_nao_utilizado =~ /(?i)<br\/?>/) {
                                        List lines = rowData.tx_justificativa_nao_utilizado.toString().replaceAll(/(?i)<br\/?>/, '<br>').split('<br>')
                                        rowData.tx_justificativa_nao_utilizado = lines[0]
                                        if (lines[1]) {
                                            rowData.tx_justificativa_nao_utilizado = '<i title="' + lines[1] + '" class="fa fa-user cursor-pointer green"></i>&nbsp;' +
                                                rowData.tx_justificativa_nao_utilizado
                                        }
                                    }
                                    textJustificativaNaoUtilizado = '<div class="alert alert-danger ol3-popup-text">' + rowData.tx_justificativa_nao_utilizado + '</div>'
                                }
                            }
                            value = '<span class="' +
                                (rowData.cd_situacao_avaliacao == 'REGISTRO_UTILIZADO_AVALIACAO' ? 'green-dark' : 'red-dark') + '">' + textUtilizadoNaoUtilizao + '</span>' +
                                textMotivoNaoUtilizado + textJustificativaNaoUtilizado
                        } else if (col.name == 'json_ref_bib') {
                            //label='';
                            startBold = ''
                            endBold = ''
                            value = '<div class="ol3-popup-ref-bib">' + Util.parseRefBib2Grid(rowData.json_ref_bib.toString()) + '</div>'
                        } else if (col.name == 'tx_observacao_usuario_web') {
                            //label='';
                            startBold = ''
                            endBold = ''
                            value = '<div class="alert alert-info ol3-popup-text">' + rowData.tx_observacao_usuario_web + '</div>'
                        } else if (col.name == 'no_usuario_consulta') {
                            value = Util.nomeAbreviado(value)
                        } else if (col.name == 'ds_tombamento') {
                            value += rowData.no_instituicao_tombamento ? ' / ' + rowData.no_instituicao_tombamento : ''
                        } else if (col.name == 'tx_observacao') {
                            //label='';
                            startBold = ''
                            endBold = ''
                            value = '<i class="fa fa-commenting blue cursor-pointer" title="' + Util.stripTagsKeepLineFeed(rowData.tx_observacao) + '"></i>'
                        } else if (col.name =~ /id_origem|ds_uuid/) {
                            startBold = ''
                            endBold = ''
                            value = '<span style="font-size:9px !important;color:gray;">' + label + ': ' + value + '</span>'
                            label = '';
                        } else if (col.name == 'no_estado') {
                            label = (isCentroide ? 'Centroide Estado':label)

                        } else if (col.name == 'no_autor' ) {
                            if ( params.isADM && rowData.ds_uuid ==~ /^ICMBIO\/DIBIO\/SISBIO\/ID: ?[0-9]{1,}$/) {
                                def matches = (rowData.ds_uuid =~ /[0-9]{1,}$/)
                                if (matches.size() == 1) {
                                    value += '&nbsp;<i data-id="' + matches[0] + '" class="cursor-pointer fa fa-users ol3-i-show-team" title="Visualiar equipe"></i>'
                                }
                            }
                        }
                    }
                }



                if (value) {
                    String newInfo = ''
                    if (label) {
                        newInfo = label + ': ' + startBold + value + endBold
                    } else {
                        newInfo = startBold + value + endBold
                    }
                    if( ! infoRows.contains(newInfo) ) {
                        infoRows.push(newInfo)
                    }
                }
            }
        } catch ( Exception e ){
            println e.getMessage()
            infoRows.push('Ocorreu um erro na leitura das informações')
        }

        return infoRows.join('<br>').replaceAll(/<\/div><br>/,'</div>')
    }

    /**
     * Arredondar as coordenadas para 6 casas decimais
     * @param value
     * @return
     */
    private BigDecimal _roundCoord( BigDecimal value = 0 ) {
        return Math.round( value * 100000000) / 100000000
    }

    /**
     * criar feature
     * @param row
     * @return
     */
    private Map _createFeature(String id ='', Map properties=[:], BigDecimal latY=0, BigDecimal lonX=0 ) {
        Map feature = [:]
        List removeProperties = ['nu_y', 'nu_x','x', 'y']

        try {
            // remover as propriedades para não retornar para o cliente
            removeProperties.each{
                properties.remove( it )
            }
            // padronizar as coordenadas para até 6 casas decimais
            lonX = Math.round( lonX * 100000000) / 100000000
            latY = Math.round( latY * 100000000) / 100000000
            feature = ["type"      : "Feature",
                       "id"        : id,
                       "properties": properties,
                       "geometry"  : [
                           "type"       : "Point",
                           "coordinates": [lonX, latY]
                       ]
            ]
        } catch( Exception e ){}

        /** /
        println ' '
        println '_createFeature()'
        println feature
        /**/
        return feature
    }

    /**
     * retornar geojson dos pontos SALVE para alimentar as camadas do mapa
     */
    def getOcorrenciasSalve( Map params = [:]) {
        /** /
         println ' '
         println 'MAPSERVICE - getOcorrenciasSalve()'
         println params
         /**/

        Map result = ["type"      : "FeatureCollection"
                      , "crs"     : ['type': 'name', 'properties': ['name': 'EPSG:4326']]
                      , "errors"  : []
                      , "features": []
        ]
        String ymd  = new Date().format('yyyy-MM-dd')
        String cor  = params.color ?: ''
        String icon = params.icon  ?: ''
        List sqlWhere = []
        Map sqlParams = [:]
        try {
            if (!params.sqFicha) {
                throw new Exception('Id da ficha deve ser informado')
            }

            if ( params.historico == 's' ) {
                sqlWhere.push("a.in_presenca_atual = 'N'" )
                sqlWhere.push("a.in_utilizado_avaliacao = 'S'")
            } else {
                if (params.registrosUtilizadosNaAvaliacao) {
                    sqlWhere.push("( a.in_presenca_atual is null or a.in_presenca_atual <> 'N')" )
                    sqlWhere.push("( a.in_utilizado_avaliacao = 'S' OR a.in_utilizado_avaliacao is null )")
                    cor = cor ?: "#00FF00" // VERDE - registros utilizados
                } else {
                    sqlWhere.push("a.in_utilizado_avaliacao = 'N'")
                    cor = cor ?: "#FF0000" // vermelho - registros não utilizados
                }
            }


            // se não for informada a cor das features, utilizar as cores padrão
            params.color = params.color ?: cor

            // ler os ids das fichas das subespecies
            List idsFichasSubespecies = getIdsFichasSubespecies(params.sqFicha.toLong())
/*
println ' '
println 'SUBESPCIES'
println idsFichasSubespecies
*/

            if (idsFichasSubespecies) {
                sqlWhere.push("a.sq_ficha in ( ${params.sqFicha},${idsFichasSubespecies.join(',')} )")
            } else {
                sqlWhere.push('a.sq_ficha = :sqFicha')
                sqlParams.sqFicha = params.sqFicha.toLong()
            }

            String cmdSql = """WITH CTE as ( SELECT a.sq_ficha_ocorrencia as id_registro
                       , a.sq_ficha
                       , st_x(a.ge_ocorrencia) AS nu_x
                       , st_y(a.ge_ocorrencia) AS nu_y
                       , 'salve' as no_base_dados
                       , 'salve' as no_origem
                       , salve.calc_carencia(a.dt_inclusao::date, prazo_carencia.cd_sistema) as dt_carencia
                       , upper( coalesce( a.in_utilizado_avaliacao,'S') ) as in_utilizado_avaliacao
                       , '${cor}' as no_color
                       , '${icon}' as no_icon
                from salve.ficha_ocorrencia as a
                left outer join salve.dados_apoio as prazo_carencia on prazo_carencia.sq_dados_apoio = a.sq_prazo_carencia
                where a.ge_ocorrencia is not null
                  and a.sq_contexto = 420
                  ${sqlWhere ? '\nand ' + sqlWhere.join(' and ') : ''}
                )
                select CTE.*
                , case when upper(nivel.co_nivel_taxonomico) = 'SUBESPECIE' then 'S' else 'N' end as in_subespecie
                from CTE
                inner join salve.ficha on ficha.sq_ficha = CTE.sq_ficha
                inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                inner join taxonomia.nivel_taxonomico as nivel on nivel.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
                ${!params.visualizarRegistrosEmCarencia ? "where (CTE.dt_carencia is null or CTE.dt_carencia < '${ymd}')" : ''}
                order by CTE.nu_y, CTE.nu_x;
                """
            /** /
             println ' '
             println 'sql ocorrencias SALVE'
             println cmdSql
             println sqlParams
             /**/

            // criar as features para cada ponto
            sqlService.execSql(cmdSql, sqlParams).each { row ->
                Map feature = _createFeature('salve-' + row.id_registro.toString(),
                    (Map) row,
                    (BigDecimal) row.nu_y,
                    (BigDecimal) row.nu_x)
                result.features.push(feature)
            }

            // ler as ocorrencias cadastradas nas consultas/revisoes que estão sendo utilizadas na avaliação
            if (params.registrosUtilizadosNaAvaliacao) {
                cmdSql = """WITH CTE as (
                    SELECT a.sq_ficha_ocorrencia as id_registro
                                   , a.sq_ficha
                                   , st_x(a.ge_ocorrencia) AS nu_x
                                   , st_y(a.ge_ocorrencia) AS nu_y
                                   , 'salve' as no_base_dados
                                   , 'consulta' as no_origem
                                   , salve.calc_carencia(a.dt_inclusao::date, prazo_carencia.cd_sistema) as dt_carencia
                                   , upper( coalesce( a.in_utilizado_avaliacao,'S') ) as in_utilizado_avaliacao
                                   , '${cor}' as no_color
                                   , '${icon}' as no_icon
                              from salve.ficha_ocorrencia as a
                              inner join salve.ficha_ocorrencia_consulta b on b.sq_ficha_ocorrencia = a.sq_ficha_ocorrencia
                              left outer join salve.dados_apoio as prazo_carencia on prazo_carencia.sq_dados_apoio = a.sq_prazo_carencia
                              where a.ge_ocorrencia is not null
                              and a.in_utilizado_avaliacao = 'S'
                              and a.sq_contexto = 1053
                              ${sqlWhere ? '\nand ' + sqlWhere.join(' and ') : ''}
                )
                select CTE.*
                     , case when upper(nivel.co_nivel_taxonomico) = 'SUBESPECIE' then 'S' else 'N' end as in_subespecie
                from CTE
                inner join salve.ficha on ficha.sq_ficha = CTE.sq_ficha
                inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                inner join taxonomia.nivel_taxonomico as nivel on nivel.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
                ${!params.visualizarRegistrosEmCarencia ? "where (CTE.dt_carencia is null or CTE.dt_carencia < '${ymd}')" : ''}
                order by CTE.nu_y, CTE.nu_x;
                """
                // criar as features para cada ponto
                sqlService.execSql(cmdSql, sqlParams).each { row ->
                    Map feature = _createFeature('salve-' + row.id_registro.toString(),
                        (Map) row,
                        (BigDecimal) row.nu_y,
                        (BigDecimal) row.nu_x)
                    result.features.push(feature)
                }
            }
        } catch (Exception e) {
            result.errors.push(e.getMessage())
        }
        return result
    }

    /**
     * retornar geojson dos pontos SALVE recebidos das consultas diretas, amplas ou revisão para alimentar as camada do mapa
     */
    def getOcorrenciasSalveConsulta( Map params = [:]){
        /** /
         println ' '
         println 'MAPSERVICE - getOcorrenciasSalveConsulta()'
         println params
         /**/
        Map result = ["type"      : "FeatureCollection"
                      , "crs"     : ['type': 'name', 'properties': ['name': 'EPSG:4326']]
                      , "errors"  : []
                      , "features": []
        ]
        String ymd = new Date().format('yyyy-MM-dd')
        String cor = "#0000ff" // azul - registros recebidos via consulta/revisao ainda não utilizados na avaliação
        List sqlWhere = []
        Map sqlParams = [:]
        try {
            if (!params.sqFicha) {
                throw new Exception('Id da ficha deve ser informado')
            }
            sqlWhere.push("(a.in_utilizado_avaliacao = 'N' or a.in_utilizado_avaliacao is null)")

            // se não for informada a cor das features, utilizar as cores padrão
            params.color = params.color ?: cor

            // ler os ids das fichas das subespecies
            List idsFichasSubespecies = getIdsFichasSubespecies(params.sqFicha.toLong())
            if (idsFichasSubespecies) {
                sqlWhere.push("a.sq_ficha in ( ${params.sqFicha},${idsFichasSubespecies.join(',')} )")
            } else {
                sqlWhere.push('a.sq_ficha = :sqFicha')
                sqlParams.sqFicha = params.sqFicha.toLong()
            }

            String cmdSql = """WITH CTE as (
                    SELECT a.sq_ficha_ocorrencia as id_registro
                                   , a.sq_ficha
                                   , st_x(a.ge_ocorrencia) AS nu_x
                                   , st_y(a.ge_ocorrencia) AS nu_y
                                   , 'salve' as no_base_dados
                                   , 'consulta' as no_origem
                                   , salve.calc_carencia(a.dt_inclusao::date, prazo_carencia.cd_sistema) as dt_carencia
                                   , upper( coalesce( a.in_utilizado_avaliacao,'N') ) as in_utilizado_avaliacao
                                   , '${cor}' as no_color
                              from salve.ficha_ocorrencia as a
                              inner join salve.ficha_ocorrencia_consulta b on b.sq_ficha_ocorrencia = a.sq_ficha_ocorrencia
                              left outer join salve.dados_apoio as prazo_carencia on prazo_carencia.sq_dados_apoio = a.sq_prazo_carencia
                              where a.ge_ocorrencia is not null
                              and ( a.in_utilizado_avaliacao = 'N' OR a.in_utilizado_avaliacao is null )
                              and a.sq_contexto = 1053
                              ${sqlWhere ? '\nand ' + sqlWhere.join(' and ') : ''}
                )
                select CTE.*
                     , case when upper(nivel.co_nivel_taxonomico) = 'SUBESPECIE' then 'S' else 'N' end as in_subespecie
                from CTE
                inner join salve.ficha on ficha.sq_ficha = CTE.sq_ficha
                inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                inner join taxonomia.nivel_taxonomico as nivel on nivel.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
                ${!params.visualizarRegistrosEmCarencia ? "where (CTE.dt_carencia is null or CTE.dt_carencia < '${ymd}')" : ''}
                order by CTE.nu_y, CTE.nu_x;
                """
                /** /
                println cmdSql
                println sqlParams
                /**/

            // criar as features para cada ponto
            sqlService.execSql(cmdSql, sqlParams).each { row ->
                Map feature = _createFeature('salve-' + row.id_registro.toString(),
                    (Map) row,
                    (BigDecimal) row.nu_y,
                    (BigDecimal) row.nu_x)
                result.features.push(feature)
            }

        } catch (Exception e) {
            result.errors.push(e.getMessage())
        }
        return result
    }

    /**
     * retornar geojson dos pontos do PORTABIO para alimentar a camada do mapa
     */
    def getOcorrenciasPortalbio( Map params = [:]){
        /** /
         println ' '
         println 'MAPSERVICE - getOcorrenciasSalve()'
         println params
         /**/
        Map result = ["type"      : "FeatureCollection"
                      , "crs"     : ['type': 'name', 'properties': ['name': 'EPSG:4326']]
                      , "errors"  : []
                      , "features": []
        ]

        String ymd = new Date().format('yyyy-MM-dd')
        String cor = "#00ff00" // verde - registros utilizados na avaliação
        List sqlWhere = []
        Map sqlParams = [:]
        try {
            if (!params.sqFicha) {
                throw new Exception('Id da ficha deve ser informado')
            }
            if ( params.avaliados == 'n' )  {
                sqlWhere.push("a.in_utilizado_avaliacao is null")
                cor = "#FF00FF" // rosa - registros não avaliados
            } else if ( params.registrosUtilizadosNaAvaliacao ) {
                sqlWhere.push("a.in_utilizado_avaliacao = 'S'")
                cor = "#FFFF00" // AMARELA - registros utilizados
            } else if( ! params.registrosUtilizadosNaAvaliacao )  {
                sqlWhere.push("a.in_utilizado_avaliacao in ('N','E')")
                cor = "#8A2BE2" // ROXO - registros não utilizados
            } else {
                throw  new Exception('Parãmetros incorretos')
            }
            // se não for informada a cor das features, utilizar as cores padrão
            params.color = params.color ?: cor

            // ler os ids das fichas das subespecies
            List idsFichasSubespecies = getIdsFichasSubespecies(params.sqFicha.toLong())
            if (idsFichasSubespecies) {
                sqlWhere.push("a.sq_ficha in ( ${params.sqFicha},${idsFichasSubespecies.join(',')} )")
            } else {
                sqlWhere.push('a.sq_ficha = :sqFicha')
                sqlParams.sqFicha = params.sqFicha.toLong()
            }

            String cmdSql = """WITH CTE as ( SELECT a.sq_ficha_ocorrencia_portalbio as id_registro
                   , a.sq_ficha
                   , st_x(a.ge_ocorrencia) AS nu_x
                   , st_y(a.ge_ocorrencia) AS nu_y
                   , 'portalbio' as no_base_dados
                   , 'portalbio' as no_origem
                   , a.dt_carencia
                   , upper( coalesce( a.in_utilizado_avaliacao,'S') ) as in_utilizado_avaliacao
                   , '${cor}' as no_color
              from salve.ficha_ocorrencia_portalbio as a
              where a.ge_ocorrencia is not null
              ${sqlWhere ? '\nand ' + sqlWhere.join(' and ') : ''}
              )
              select CTE.*
              , case when upper(nivel.co_nivel_taxonomico) = 'SUBESPECIE' then 'S' else 'N' end as in_subespecie
              from CTE
              inner join salve.ficha on ficha.sq_ficha = CTE.sq_ficha
              inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
              inner join taxonomia.nivel_taxonomico as nivel on nivel.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
              ${!params.visualizarRegistrosEmCarencia ? "where (CTE.dt_carencia is null or CTE.dt_carencia < '${ymd}')" : ''}
              order by CTE.nu_y, CTE.nu_x"""

              /** /
              println ' '
              println cmdSql
              println sqlParams
              /**/

            // criar as features para cada ponto
            sqlService.execSql(cmdSql, sqlParams).each { row ->
                Map feature = _createFeature('portalbio-' + row.id_registro.toString(),
                    (Map) row,
                    (BigDecimal) row.nu_y,
                    (BigDecimal) row.nu_x)
                result.features.push(feature)
            }

        } catch (Exception e) {
            result.errors.push(e.getMessage())
        }
        return result

    }

    /**
     * retornar informação do ponto clicado quando o usuário clica sobre um ponto no mapa
     */
    def getOcorrenciaInfo( Map params = [:] ) {
       Map result = [ text:'', msg:'' ]
       String cmdSql=''
        if( params.noBaseDados == 'salve') {
            cmdSql = "select * from salve.fn_info_registro_salve(:idRegistro)"
        } else if( params.noBaseDados == 'portalbio' ) {
            cmdSql = "select * from salve.fn_info_registro_portalbio(:idRegistro)"
        }
        if( cmdSql ) {
            sqlService.execSql( cmdSql,[idRegistro:params.idRegistro.toLong()]).each{ row ->
                result.text = _buildTextInfo( row, params );
            }
        }
       return result
    }
}
