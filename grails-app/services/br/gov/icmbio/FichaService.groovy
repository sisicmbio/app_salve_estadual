package br.gov.icmbio

import com.vividsolutions.jts.geom.Coordinate
import com.vividsolutions.jts.geom.Geometry
import com.vividsolutions.jts.geom.GeometryFactory
import grails.converters.JSON
import grails.util.Holders
import groovy.sql.GroovyRowResult
import groovy.sql.Sql
import groovy.time.TimeCategory
import groovy.time.TimeDuration
import org.codehaus.groovy.grails.web.json.JSONObject
import org.codehaus.groovy.grails.web.servlet.mvc.GrailsHttpSession
import org.codehaus.groovy.grails.web.servlet.mvc.GrailsParameterMap
import org.springframework.context.ApplicationContext
import org.springframework.web.context.request.RequestContextHolder
import java.text.DecimalFormat
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream

//@Transactional // não pode ser transactional
class FichaService  {

    static transactional = false

    private GrailsHttpSession appSession
    //def propertyInstanceMap = DomainClassGrailsPlugin.PROPERTY_INSTANCE_MAP

    def dadosApoioService
    def cacheService
    def workerService
    def requestService
    def dataSource
    def geoService
    def asyncService
    def sqlService
    def registroService

    private boolean debug = false // utilizado no metodo d() para logs

    //FichaService(){}
    FichaService( GrailsHttpSession currentSession = null ){
        if( currentSession != null ) {
            this.appSession = currentSession
        } else {
            this.getSession()
        }
    }

    void setDebug( boolean newValue = true) {
        this.debug = newValue;
    }

    //def sessionFactory
    //ssionFactory
    //def grailsApplication

    /*
    void cleanUpGorm() {
        def session = sessionFactory.currentSession
        session.flush()
        session.clear()
        propertyInstanceMap.get().clear()
    }
    */
    /**
     * metodo para criar log de depuracao
     * @param text
     */
    private void d(String text ){
        if( this.debug ) {
            if( text.trim() != '' ) {
                String h = new Date().format('HH:mm:ss')
                println h + ':' + text
            } else {
                println ' '
            }
        }
    }

    void setSession( GrailsHttpSession newValue=null ){
        this.appSession = newValue
    }

    void getSession() {
        try {
            // quando o fichaService é utilizado dentro de uma rotina assyncrona, não tem o RequestContextHolder
            if ( RequestContextHolder?.currentRequestAttributes()) {
                this.appSession = RequestContextHolder.currentRequestAttributes().getSession()
            }
        } catch( Exception e ) {
            // não pode zerar a appSession aqui senão da erro no asyncService que utiliza esta classe
            // sem ser por dependency injection
        }
    }


    // se a ficha for LC e passar para consolidade, o campo st_manter_lc da ficha deve ser marcado com true se tiver null
    void verificarStManterLc( List fichasIds ) {
        if( fichasIds.size() == 0 ) {
            return
        }
        Ficha ficha = Ficha.get( fichasIds[0].toLong() )
        if( !ficha ){
            return;
        }
        CicloAvaliacao cicloAvaliacao = ficha.cicloAvaliacao
        DadosApoio avaliacaoNacional = getDadosApoioByCodigo('TB_TIPO_AVALIACAO', 'NACIONAL_BRASIL')
        if( !avaliacaoNacional ) {
            return;
        }
        String cmdSql = """UPDATE salve.ficha
            SET st_manter_lc = TRUE
            WHERE sq_ficha IN
                ( SELECT ficha.sq_ficha
                 FROM salve.ficha
                 INNER JOIN LATERAL
                   (SELECT a.sq_taxon,
                           a.sq_taxon_historico_avaliacao ,
                           a.tx_justificativa_avaliacao AS tx_justificativa_aval_nacional ,
                           a.nu_ano_avaliacao AS nu_ano_aval_nacional ,
                           a.de_criterio_avaliacao_iucn AS de_criterio_aval_nacional ,
                           categoria.sq_dados_apoio AS sq_categoria_nacional ,
                           categoria.ds_dados_apoio AS ds_categoria_iucn_nacional ,
                           categoria.cd_dados_apoio AS cd_categoria_iucn_sistema_nacional,
                           categoria.cd_sistema AS cd_categoria_iucn_nacional ,
                           categoria.ds_dados_apoio AS de_ordem
                    FROM salve.taxon_historico_avaliacao a
                    INNER JOIN salve.dados_apoio categoria ON categoria.sq_dados_apoio = a.sq_categoria_iucn
                    WHERE a.sq_tipo_avaliacao = ${avaliacaoNacional.id.toString() }
                      AND categoria.cd_dados_apoio = 'LC'
                      AND a.sq_taxon = ficha.sq_taxon
                      order by a.nu_ano_avaliacao desc, a.sq_taxon_historico_avaliacao desc limit 1
                      ) hist ON TRUE
                 WHERE ficha.sq_ficha in( ${ fichasIds.join(',') } )
                 AND ficha.st_manter_lc IS NULL )"""
        sqlService.execSql( cmdSql )
    }

    Map searchPaginated( GrailsParameterMap params, Integer pageSize = 100) {
        Map res = [ data:[] , pagination : [ pageSize:pageSize ] ]
        if( ! params?.paginationPageNumber ) {
            res.pagination.pageNumber = params.paginationPageNumber = 1
        } else {
            res.pagination.pageNumber = params.paginationPageNumber.toInteger()
        }
        if( params?.paginationTotalRecords ) {
            res.pagination.totalRecords = params.paginationTotalRecords.toInteger()
        }
        List listTemp = search( params, res.pagination )
        return [  data:listTemp[0].data
                  , pagination:listTemp[0].pagination ]
    }


    /**
     * método geral de consulta às fichas utilizando os filtros padrão
     * definidos pelo /viws/templates/_filtrosFicha.gsp
     * @param params
     * @return List
     * Exemplo do sql Gerado:
     *      select vwFicha.*,tmp.*
     *      from (
     *        select vwFicha.sq_ficha as sq_ficha_tmp, vwFicha.cd_situacao_ficha_sistema
     *        from salve.vw_ficha vwFicha, salve.oficina_ficha oficinaFicha
     *        where vwFicha.sq_ciclo_avaliacao = 1
     *        and oficinaFicha.sq_ficha = vwFicha.sq_ficha
     *        and oficinaFicha.sq_oficina = 1
     *        group by vwFicha.sq_ficha, vwFicha.cd_situacao_ficha_sistema
     *      ) as tmp
     *      inner join salve.vw_ficha as vwFicha on vwFicha.sq_ficha = tmp.sq_ficha_tmp
     *      left outer join salve.dados_apoio as grupo on grupo.sq_dados_apoio = vwFicha.sq_grupo
     */

    List search( GrailsParameterMap params = null, Map pagination = null) {
        /**
         * params.sqFichaFiltro - lista de ids de fichas válidos
         * params.sqFichaNaoFiltro - lista de ids de fichas não válidos
         * params.sqSituacaoNaoFiltro - lista de ids de situacao fichas não válidos
         * params.colHistorico - adicionar colunas da ultima avaliacao nacional
         */
        List data = []
        Long SEM_CATEGORIA = -9l // utilizado o codigo -9 para filtrar as fichas sem categoria

        // inicializar propriedade appSession
        this.getSession()
        if ( ! this.appSession?.sicae?.user) {
            return data
        }

        if (!params.sqCicloAvaliacao) {
            println 'servico search chamado sem o parametro sqCicloAvaliacao'
            return data
        }

        CicloAvaliacao cicloAvaliacao = CicloAvaliacao.get(params.sqCicloAvaliacao.toLong())
        String sqlCmd = ''
        DadosApoio nacionalBrasil
        DadosApoio categoria
        //List categoriasAmeacada = []
        List listCategoriasFiltro = []
        List listCategoriasAvaliadasFiltro = []
        /*if( params.coCategoriaFiltro == 'AMEACADA')
        {
            categoriasAmeacada = DadosApoio.createCriteria().list {
                'in'('codigoSistema',['VU','EN','CR'])
            }
        }
        */

        // categoria ameacada = VU, EN e CR
        /*
        if (params.sqCategoriaFiltro) {
            if (params.sqCategoriaFiltro.toString().indexOf(',') == -1) {
                listCategoriasFiltro.push(params.sqCategoriaFiltro.toLong())
            } else {
                listCategoriasFiltro = params.sqCategoriaFiltro.split(',').collect { it.toLong() }
            }
        }


        if( params.sqCategoriaAvaliadaFiltro ) {
            if (params.sqCategoriaAvaliadaFiltro.toString().indexOf(',') == -1) {
                listCategoriasAvaliadasFiltro.push(params.sqCategoriaAvaliadaFiltro.toLong() )
            }
            else {
                listCategoriasAvaliadasFiltro = params.sqCategoriaAvaliadaFiltro.split(',').collect { it.toLong() }
            }
        }
        */

        Map qryParams = [:]
        Sql sql
        //try {
        String operador
        ///List selectColumns  = ['vwFicha.sq_ficha','vwFicha.sq_taxon','vwFicha.sq_ciclo_avaliacao','vwFicha.sq_unidade_org','vwFicha.sq_usuario_alteracao','vwFicha.sq_situacao_ficha','vwFicha.sq_grupo','vwFicha.sq_categoria_iucn','vwFicha.nm_cientifico','vwFicha.sg_unidade_org','vwFicha.no_usuario_alteracao','vwFicha.dt_alteracao','vwFicha.ds_situacao_ficha','vwFicha.vl_percentual_preenchimento','vwFicha.st_possivelmente_extinta','vwFicha.nu_pendencia','vwFicha.sq_categoria_final']
        List selectColumns = ['ficha.sq_ficha as sq_ficha_tmp,ficha.st_endemica_brasil']
        //println selectColumns.minus(['vwFicha.sq_categoria_final'])
        List groupByColumns = []
        List havingCount = []
        List fromTables = ['salve.ficha']
        List leftOuterTables = []
        List crossJoinTables = []
        List leftJoinLateralTables = []
        List where = [[condition: 'ficha.sq_ciclo_avaliacao = :sqCicloAvaliacao', paramName: 'sqCicloAvaliacao', value: params.sqCicloAvaliacao.toLong()]]
        List calcColumn = []
        List fakeColumn = []

        if( params.colPublicacao ) {
            DadosApoio tipoImagem = dadosApoioService.getByCodigo('TB_TIPO_MULTIMIDIA','IMAGEM')
            DadosApoio situacaoReprovada = dadosApoioService.getByCodigo('TB_SITUACAO_MULTIMIDIA','REPROVADA')
            selectColumns.push('ficha.sq_usuario_publicacao, ficha.dt_publicacao,count(fotos.sq_ficha_multimidia) as nu_fotos')
            groupByColumns.push('ficha.sq_ficha, ficha.sq_usuario_publicacao, ficha.dt_publicacao')
            leftOuterTables.push("salve.ficha_multimidia as fotos on fotos.sq_ficha = ficha.sq_ficha")
            where.push(['condition': "((fotos.sq_ficha is null ) or ( fotos.sq_tipo = ${tipoImagem.id.toString() } and fotos.sq_situacao <> ${situacaoReprovada.id.toString()}))", paramName: '', value: ''])

        }

        // filtrar pelo ID(s) da(s) ficha(s)
        if ( params.sqFichaFiltro ) {
            String strTemp = '-1'
            if( params.sqFichaFiltro instanceof List )
            {
                strTemp = params.sqFichaFiltro.join(',')
            }
            else {
                strTemp = params.sqFichaFiltro.toString().trim()
            }
            if (strTemp.indexOf(',') == -1) {
                where.push(['condition': "ficha.sq_ficha = :sqFicha", paramName: 'sqFicha', value: strTemp.toLong()])
            }else {
                where.push(['condition': "ficha.sq_ficha in (" + strTemp + ")", paramName: '', value: ''])
            }
        }
        // excluir as fichas da consulta
        if ( params.sqFichaNaoFiltro ) {
            String strTemp = '-1'
            if( params.sqFichaNaoFiltro instanceof List )
            {
                strTemp = params.sqFichaNaoFiltro.join(',')
            }
            else {
                strTemp = params.sqFichaNaoFiltro.toString().trim()
            }

            if (strTemp.indexOf(',') == -1) {
                where.push(['condition': "ficha.sq_ficha <> :sqFicha", paramName: 'sqFicha', value: strTemp.toLong() ])
            } else {
                where.push(['condition': "ficha.sq_ficha NOT IN (" + strTemp + ")", paramName: '', value: ''])
            }

        }

        // filtrar NOME CIENTIFICO
        if (params.nmCientificoFiltro) {
            //ands.push(['condition': "lower(ficha.nmCientifico) like :nmCientifico", paramName: 'nmCientifico', value: '%' + params.nmCientificoFiltro.toLowerCase() + '%'])
            String nmCientificoCleaned = params.nmCientificoFiltro.replaceAll(/(?i)[^a-zA-Z -]/,'').toLowerCase().trim()
            where.push(['condition': "lower( ficha.nm_cientifico) like :nmCientifico", paramName: 'nmCientifico', value: '%' + nmCientificoCleaned + '%'])
        }

        /*
        // filtrar pela categoria Avaliada ( aba 11.6 )
        if( listCategoriasAvaliadasFiltro.size() > 0 ) {
            if( listCategoriasAvaliadasFiltro.size() == 1 ) {
                where.push(['condition': "ficha.sq_categoria_iucn = :sqCategoriaAvaliada", paramName: 'sqCategoriaAvaliada', value: params.sqCategoriaAvaliadaFiltro.toLong()])
            } else {
                where.push(['condition': "ficha.sq_categoria_iucn in(${listCategoriasAvaliadasFiltro.join(',')})", paramName: '', value: ''])
            }
        }
       */


        if (params.nmComumFiltro) {
            //ands.push(['condition': "lower(ficha.nmCientifico) like :nmCientifico", paramName: 'nmCientifico', value: '%' + params.nmCientificoFiltro.toLowerCase() + '%'])
            //String nmComumCleaned = params.nmComumFiltro.replaceAll(/(?i)[^a-zA-Z -]/,'').toLowerCase().trim()
            String nmComumCleaned = params.nmComumFiltro.toLowerCase().trim()
            leftOuterTables.push('salve.ficha_nome_comum nome_comum on nome_comum.sq_ficha = ficha.sq_ficha')
            //where.push(['condition': "(nome_comum.no_comum ilike :nmComum or nome_comum.no_comum ilike :nmComum||' %' or nome_comum.no_comum ilike '% '||:nmComum or nome_comum.no_comum ilike '% '||:nmComum||' %' )", paramName: 'nmComum', value: nmComumCleaned])
            if( nmComumCleaned.indexOf('%') == - 1 ) {
                where.push(['condition': "( nome_comum.no_comum ilike :nmComum" +
                    " or nome_comum.no_comum ilike :nmComum||'% '" +
                    " or nome_comum.no_comum ilike :nmComum||'-% '" +
                    " or nome_comum.no_comum ilike '% '||:nmComum" +
                    " or nome_comum.no_comum ilike '%-'||:nmComum||' %'" +
                    " or nome_comum.no_comum ilike '% '||:nmComum||'-%'" +
                    " or nome_comum.no_comum ilike '% '||:nmComum||'%'" +
                    " or nome_comum.no_comum ilike '% '||:nmComum||' %'" +
                    " )", paramName: 'nmComum', value: nmComumCleaned])
            } else {
                where.push(['condition': "(nome_comum.no_comum ilike :nmComum or nome_comum.no_comum ilike :nmComum||' %' or nome_comum.no_comum ilike '% '||:nmComum or nome_comum.no_comum ilike :nmComum )", paramName: 'nmComum', value: nmComumCleaned])
            }
        }

        // filtrar SITUACAO
        if(  params.chkAvaliadaNoCicloFiltro != 'S') {
            if (params.sqSituacaoFiltro) {
                if (params.sqSituacaoFiltro.toString().indexOf(',') == -1) {
                    where.push(['condition': "ficha.sq_situacao_ficha = :idSituacao", paramName: 'idSituacao', value: params.sqSituacaoFiltro.toLong()])
                }else {
                    where.push(['condition': "ficha.sq_situacao_ficha in(${params.sqSituacaoFiltro})", paramName: '', value: ''])
                }
            }
            if (params.sqSituacaoNaoFiltro) {
                String strTemp = '-1'
                if (params.sqSituacaoNaoFiltro instanceof List) {
                    strTemp = params.sqSituacaoNaoFiltro.join(',')
                }else {
                    strTemp = params.sqSituacaoNaoFiltro.toString().trim()
                }
                if (strTemp.indexOf(',') == -1) {
                    where.push(['condition': "ficha.sq_situacao_ficha <> :idSituacaoNao", paramName: 'idSituacaoNao', value: strTemp.toLong()])
                }else {
                    where.push(['condition': "ficha.sq_situacao_ficha not in (${strTemp})", paramName: '', value: ''])
                }
            }

            // filtrar PELA LISTA DE SITUACAO
            if (params.sqListSituacaoFiltro) {
                if (!params.chkAvaliadaNoCicloFiltro) {
                    where.push(['condition': "ficha.sq_situacao_ficha in (" + params.sqListSituacaoFiltro.toString().replaceAll(/[\[\]]/, '') + ") ", paramName: '', value: ''])
                }
            }
        }
        else
        {
            if ( params.sqSituacaoFiltro || params.sqSituacaoNaoFiltro || params.sqListSituacaoFiltro ) {
                params.colOficinaFicha = true
            }
        }

        // filtrar MANTIDA / NÃO MANTIDA LC
        if (params.stManterLcFiltro != null) {
            if (params.stManterLcFiltro == true) {
                where.push(['condition': "ficha.st_manter_lc = true", paramName: '', value: ''])
            }else {
                //where.push(['condition': "(vwFicha.st_manter_lc = false or vwFicha.st_manter_lc is null)", paramName: '', value: ''])
                where.push(['condition': "ficha.st_manter_lc = false", paramName: '', value: ''])
            }
        } else if (params.stManterLcNullorFalseFiltro) {
            where.push(['condition': "(ficha.st_manter_lc = false or ficha.st_manter_lc is null)", paramName: '', value: ''])
        }

        if ( params.sqGrupoSalveFiltro ) {
            if( params.sqGrupoSalveFiltro.toString().indexOf(',') == -1 ) {
                where.push(['condition': "ficha.sq_grupo_salve = :idGrupoSalve", paramName: 'idGrupoSalve', value: params.sqGrupoSalveFiltro.toLong()])
            }
            else
            {
                where.push(['condition': "ficha.sq_grupo_salve in ( ${params.sqGrupoSalveFiltro})", paramName: '', value: ''])
            }
        }
        if ( params.sqGrupoFiltro ) {
            if( params.sqGrupoFiltro.toString().indexOf(',') == -1 ) {
                where.push(['condition': "ficha.sq_grupo = :idGrupo", paramName: 'idGrupo', value: params.sqGrupoFiltro.toLong()])
            }
            else
            {
                where.push(['condition': "ficha.sq_grupo in ( ${params.sqGrupoFiltro})", paramName: '', value: ''])
            }
        }
        if ( params.sqSubgrupoFiltro ) {
            if( params.sqSubgrupoFiltro.toString().indexOf(',') == -1 ) {
                where.push(['condition': "ficha.sq_subgrupo = :idSubgrupo", paramName: 'idSubgrupo', value: params.sqSubgrupoFiltro.toLong()])
            }
            else
            {
                where.push(['condition': "ficha.sq_subgrupo in ( ${params.sqSubgrupoFiltro})", paramName: '', value: ''])
            }
        }

        // filtrar pela UNIDADE ORGANIZACIONAL, quando não for ADM definir a unidade do usuário logado
        if ( ! params.sqUnidadeFiltro) {
            // se não houver a unidade organizacional, listar as fichas pelo papel atribuido
            if ( appSession.sicae.user.isCT() || appSession.sicae.user.isCE() ) {
                if (appSession.sicae.user.isCT()) {
                    params.sqUnidadeFiltro = null;
                    Pessoa pessoa = Pessoa.get(appSession.sicae.user.sqPessoa)
                    DadosApoio papel = getDadosApoioByCodigo('TB_PAPEL_FICHA', 'COORDENADOR_TAXON')
                    if (!(papel && pessoa)) {
                        return data
                    }
                    where.push(['condition': 'ficha.sq_ficha in ( ' +
                        "select fp.sq_ficha from salve.ficha_pessoa fp where fp.sq_pessoa = :sqPessoa and fp.sq_papel = :sqPapel and in_ativo='S'" + ')', paramName: '', value: ''])
                    qryParams.sqPessoa = appSession.sicae.user.sqPessoa.toInteger()
                    qryParams.sqPapel = papel.id

                } else if (appSession.sicae.user.isCE()) {
                    params.sqUnidadeFiltro = null;
                    Pessoa pessoa = Pessoa.get(appSession.sicae.user.sqPessoa)
                    DadosApoio papel = getDadosApoioByCodigo('TB_PAPEL_FICHA', 'COLABORADOR_EXTERNO')
                    if (!(papel && pessoa)) {
                        return data
                    }
                    where.push(['condition': 'ficha.sq_ficha in ( ' +
                        "select fp.sq_ficha from salve.ficha_pessoa fp where fp.sq_pessoa = :sqPessoa and fp.sq_papel = :sqPapel and in_ativo='S'" + ')', paramName: '', value: ''])
                    qryParams.sqPessoa = appSession.sicae.user.sqPessoa.toInteger()
                    qryParams.sqPapel = papel.id
                }
            }else {
                // administrador e consulta podem ver todas as fichas
                if (!appSession.sicae.user.isADM() && !appSession.sicae.user.isCO() ) {
                    params.sqUnidadeFiltro = null
                    if (appSession.sicae.user.sqUnidadeOrg) {
                        params.sqUnidadeFiltro = appSession.sicae.user.sqUnidadeOrg.toInteger()
                    }
                }
            }
        }

        if (params.sqUnidadeFiltro) {
            where.push(['condition': "ficha.sq_unidade_org = :idUnidade", paramName: 'idUnidade', value: params.sqUnidadeFiltro.toLong()])
        }

        // filtrar pela presença na lista oficial vigente
        if( params.stListaOficialFiltro ){
            if( params.stListaOficialFiltro.toUpperCase() == 'S'){
                where.push(['condition': "ficha.st_presenca_lista_vigente='S'", paramName: '', value:''])
            }
            else if( params.stListaOficialFiltro.toUpperCase() == 'N'){
                where.push(['condition': "( ficha.st_presenca_lista_vigente='N' or ficha.st_presenca_lista_vigente is null)", paramName: '', value:''])
            }

        }

        if (params.inValidadoresFiltro) {
            selectColumns.push('( SELECT COUNT(X.SQ_VALIDADOR_FICHA) FROM SALVE.VALIDADOR_FICHA X,SALVE.OFICINA_FICHA Y WHERE X.SQ_OFICINA_FICHA = Y.SQ_OFICINA_FICHA AND Y.SQ_FICHA = ficha.SQ_FICHA' +
                (params.sqOficinaFiltro ? ' AND Y.SQ_OFICINA = ' + params.sqOficinaFiltro : '') + ' ) AS nu_validadores')
        }

        // filtrar as fichas por consulta direta/ampla
        if ( params.sqCicloConsultaFiltro ) {
            where.push(['condition': "ficha.sq_ficha in ( select x.sq_ficha from salve.ciclo_consulta_ficha x where x.sq_ciclo_consulta= :sqCicloConsulta  )", paramName: 'sqCicloConsulta', value: params.sqCicloConsultaFiltro.toLong()])

        }

        // filtrar pela OFICINA e/ou AGRUPAMENTO das SALAS
        if (params.sqOficinaFichaFiltro || params.listOficinasFiltro || params.sqOficinaFiltro || params.dsAgrupamentoFiltro || params.colAgrupamento || params.colOficinaFicha || params.chkAvaliadaNoCicloFiltro == 'S' ) {

            params.colUltimaVersao = 'S' // adicionar id e numero da última versao da ficha no resultado da consulta
            String tipoOficina = 'OFICINA_AVALIACAO'
            Oficina oficina
            if (params.sqOficinaFiltro) {
                oficina = Oficina.get(params.sqOficinaFiltro.toInteger() )
                if( oficina )
                {
                    tipoOficina = oficina.tipoOficina.codigoSistema
                }
            }
            //fromTables.push('salve.oficina_ficha oficinaFicha')
            //where.push(['condition': "oficinaFicha.sq_ficha = vwFicha.sq_ficha", paramName: '', value: ''])
            //leftOuterTables.push('SALVE.oficina_ficha oficinaFicha on oficinaFicha.sq_ficha = vwFicha.sq_ficha')
            crossJoinTables.push("""LEFT JOIN LATERAL (
                        -- considerar a ultima oficina de avaliacao
                        select oficina_ficha.sq_ficha, oficina_ficha.sq_oficina_ficha, oficina_ficha.st_ct_convidado, oficina_ficha.st_pf_convidado,
                        oficina_ficha.sq_oficina, oficina_ficha.ds_agrupamento, oficina_ficha.sq_situacao_ficha,
                        oficina_ficha.sq_categoria_iucn, st_possivelmente_extinta
                        from salve.oficina_ficha, salve.oficina, salve.dados_apoio
                        where oficina.sq_oficina=oficina_ficha.sq_oficina
                        and oficina.sq_tipo_oficina = dados_apoio.sq_dados_apoio
                        and dados_apoio.cd_sistema = '${tipoOficina}'
                        and oficina_ficha.sq_ficha = ficha.sq_ficha"""+
                ( oficina ? '\nand oficina_ficha.sq_oficina='+oficina.id : '' )+
                """\norder by oficina.dt_fim desc limit 1
                        ) oficinaFicha ON TRUE
                        left join salve.oficina_ficha_versao on oficina_ficha_versao.sq_oficina_ficha = oficinaFicha.sq_oficina_ficha
                        left join salve.ficha_versao on ficha_versao.sq_ficha_versao = oficina_ficha_versao.sq_ficha_versao

                        """)

            if (params.sqOficinaFichaFiltro) {
                where.push(['condition': "oficinaFicha.sq_oficina_ficha = :sqOficinaFicha", paramName: 'sqOficinaFicha', value: params.sqOficinaFichaFiltro.toLong()])
            }
            selectColumns.push('oficinaFicha.sq_oficina_ficha,ficha_versao.sq_ficha_versao,ficha_versao.nu_versao')
            groupByColumns.push('oficinaFicha.sq_oficina_ficha, ficha_versao.sq_ficha_versao, ficha_versao.nu_versao')
            if (params.listOficinasFiltro || params.sqOficinaFiltro || params.dsAgrupamentoFiltro || params.colAgrupamento)
            {
                if (params.sqOficinaFiltro) {
                    where.push(['condition': "oficinaFicha.sq_oficina = :idOficina", paramName: 'idOficina', value: params.sqOficinaFiltro.toLong()])
                }else if (params.listOficinasFiltro) {
                    where.push(['condition': "oficinaFicha.sq_oficina in (" + params.listOficinasFiltro.join(',') + ")", paramName: '', value: ''])
                }
                if (params.dsAgrupamentoFiltro) {
                    where.push(['condition': "oficinaFicha.ds_agrupamento ilike :dsAgrupamento", paramName: 'dsAgrupamento', value: '%' + params.dsAgrupamentoFiltro + '%'])
                    selectColumns.push('oficinaFicha.ds_agrupamento')
                    groupByColumns.push('oficinaFicha.ds_agrupamento')
                }else if (params.colAgrupamento) {
                    selectColumns.push('oficinaFicha.ds_agrupamento')
                    groupByColumns.push('oficinaFicha.ds_agrupamento')
                }
            }
            // filtrar pela situacao da ficha na oficina
            if (params.sqSituacaoFiltro) {
                if (params.sqSituacaoFiltro.toString().indexOf(',') == -1) {
                    //where.push(['condition': "oficinaFicha.sq_situacao_ficha = :idSituacao", paramName: 'idSituacao', value: params.sqSituacaoFiltro.toLong()])
                    where.push(['condition': "ficha.sq_situacao_ficha = :idSituacao", paramName: 'idSituacao', value: params.sqSituacaoFiltro.toLong()])
                } else {
                    //where.push(['condition': "oficinaFicha.sq_situacao_ficha in (${params.sqSituacaoFiltro})", paramName: '', value: ''])
                    where.push(['condition': "ficha.sq_situacao_ficha in (${params.sqSituacaoFiltro})", paramName: '', value: ''])
                }
            }
            if (params.sqSituacaoNaoFiltro) {
                String strTemp = '-1'
                if (params.sqSituacaoNaoFiltro instanceof List) {
                    strTemp = params.sqSituacaoNaoFiltro.join(',')
                }else {
                    strTemp = params.sqSituacaoNaoFiltro.toString().trim()
                }
                if (strTemp.indexOf(',') == -1) {
                    //where.push(['condition': "oficinaFicha.sq_situacao_ficha <> :idSituacaoNao", paramName: 'idSituacaoNao', value: strTemp.toLong()])
                    where.push(['condition': "ficha.sq_situacao_ficha <> :idSituacaoNao", paramName: 'idSituacaoNao', value: strTemp.toLong()])
                }else {
                    //where.push(['condition': "oficinaFicha.sq_situacao_ficha not in (${strTemp})", paramName: '', value: ''])
                    where.push(['condition': "ficha.sq_situacao_ficha not in (${strTemp})", paramName: '', value: ''])
                }
            }
            // filtrar PELA LISTA DE SITUACAO
            if (params.sqListSituacaoFiltro) {
                if ( ! params.chkAvaliadaNoCicloFiltro ) {
                    //where.push(['condition': "oficinaFicha.sq_situacao_ficha in (" + params.sqListSituacaoFiltro.toString().replaceAll(/[\[\]]/, '') + ") ", paramName: '', value: ''])
                    where.push(['condition': "ficha.sq_situacao_ficha in (" + params.sqListSituacaoFiltro.toString().replaceAll(/[\[\]]/, '') + ") ", paramName: '', value: ''])
                }
            }
        }

        // filtrar COM / SEM COLABORACAO
        if (params.inColaboracaoFiltro || params.colColaboracao) {
            leftOuterTables.push('salve.ciclo_consulta_ficha cicloConsultaFicha on ficha.sq_ficha = cicloConsultaFicha.sq_ficha')
            leftOuterTables.push('salve.ficha_colaboracao fichaColaboracao on fichaColaboracao.sq_ciclo_consulta_ficha = cicloConsultaFicha.sq_ciclo_consulta_ficha')
            leftOuterTables.push('salve.ficha_ocorrencia_consulta fichaOcorrenciaConsulta on fichaOcorrenciaConsulta.sq_ciclo_consulta_ficha = cicloConsultaFicha.sq_ciclo_consulta_ficha')
            leftOuterTables.push('salve.ficha_consulta_anexo fichaConsultaAnexo on fichaConsultaAnexo.sq_ciclo_consulta_ficha = cicloConsultaFicha.sq_ciclo_consulta_ficha')

            crossJoinTables.push("""left join lateral ( select x.sq_ficha_ocorrencia_validacao
                             from salve.ficha_ocorrencia_validacao x
                             inner join salve.ficha_ocorrencia y on y.sq_ficha_ocorrencia = x.sq_ficha_ocorrencia
                             inner join salve.dados_apoio z on z.sq_dados_apoio = y.sq_contexto
                             where y.sq_ficha = ficha.sq_ficha and x.in_valido='N'
                             and z.cd_sistema in ('REGISTRO_OCORRENCIA', 'CONSULTAa')
                             limit 1
                             ) as validacao_salve on true""")

            crossJoinTables.push("""left join lateral ( select x.sq_ficha_ocorrencia_validacao
                            from salve.ficha_ocorrencia_validacao x
                            inner join salve.ficha_ocorrencia_portalbio y on y.sq_ficha_ocorrencia_portalbio = x.sq_ficha_ocorrencia_portalbio
                            where y.sq_ficha = ficha.sq_ficha and x.in_valido='N'
                            limit 1
                            ) as validacao_portalbio on true""")

            selectColumns.push('count(fichaColaboracao.sq_ficha_colaboracao) as nu_colaboracao')
            selectColumns.push('count(fichaOcorrenciaConsulta.sq_ficha_ocorrencia_consulta) as nu_colaboracao_ocorrencia')
            selectColumns.push('count(fichaConsultaAnexo.sq_ficha_consulta_anexo) as nu_colaboracao_planilha_ocorrencia')
            selectColumns.push('count(validacao_salve.sq_ficha_ocorrencia_validacao) as nu_validacao_salve')
            selectColumns.push('count(validacao_portalbio.sq_ficha_ocorrencia_validacao) as nu_validacao_portalbio')

            if (params.colColaboracao) {
                fakeColumn.push('null as colaboracoes') // coluna sera calculada depois da execução da query
            }
            if (params.inColaboracaoFiltro) {
                operador = '='
                if (params.inColaboracaoFiltro.toLowerCase() == 's') {
                    operador = '>'
                }
                havingCount.push("""having count(fichaColaboracao.sq_ficha_colaboracao ) +
                                        count( fichaOcorrenciaConsulta.sq_ficha_ocorrencia_consulta ) +
                                        count( fichaConsultaAnexo.sq_ficha_consulta_anexo ) +
                                        count( validacao_salve.sq_ficha_ocorrencia_validacao ) +
                                        count( validacao_portalbio.sq_ficha_ocorrencia_validacao ) ${operador} 0 """)
            }
        }

        // FILTRAR PELA CONSULTA
        if (params.sqCicloConsultaFiltro) {
            // adicionar a tabela ciclo_consulta_ficha se ainda não tiver sido adicionada anteriormente
            if (!params.inColaboracaoFiltro && !params.colColaboracao) {
                leftOuterTables.push('salve.ciclo_consulta_ficha cicloConsultaFicha on ficha.sq_ficha = cicloConsultaFicha.sq_ficha')
            }
            where.push(['condition': "cicloConsultaFicha.sq_ciclo_consulta = :sqCicloConsulta", paramName: 'sqCicloConsulta', value: params.sqCicloConsultaFiltro.toLong()])
        }

        // filtrar COM / SEM PENDENCIA
        /*if (params.inPendenciaFiltro) {
            operador = '='
            if (params.inPendenciaFiltro.toString() == '2') {
                operador = '>'
            }
            where.push(['condition': "ficha.nu_pendencia ${operador} 0", paramName: '', value: ''])
        }*/


        // filtrar CATEGORIA -  VIGENTE - ABA 7
        if ( params.sqCategoriaFiltro) {
            nacionalBrasil = nacionalBrasil ?: getDadosApoioByCodigo('TB_TIPO_AVALIACAO', 'NACIONAL_BRASIL')
            where.push(['condition': """exists ( select null from salve.taxon_historico_avaliacao h
                where h.sq_taxon = ficha.sq_taxon
                and h.sq_tipo_avaliacao = ${nacionalBrasil.id.toString()}
                and h.sq_categoria_iucn in (${params.sqCategoriaFiltro})
                ${params.stPexFiltro=~/S|N/ ? " and h.st_possivelmente_extinta='${params.stPexFiltro}'" :''}
                order by h.nu_ano_avaliacao desc, h.sq_taxon_historico_avaliacao desc
                offset 0 limit 1)""",paramsValue:'',value:''])
            //where.push(['condition': "ficha.sq_categoria_iucn in (${ params.sqCategoriaFiltro})", paramName: '', value:''])
        }

        // filtrar pela categoria - avaliada/validada - ABA 11.1 OU 11.2
        if( params.sqCategoriaAvaliacaoFiltro){
            String noColunaFiltrarCategoria
            String noColunaFiltrarPEX
            if( ! params.chkCategoriaValidadaFiltro){
                // ABA 11.1
                noColunaFiltrarCategoria = 'ficha.sq_categoria_iucn'
                noColunaFiltrarPEX = 'ficha.st_possivelmente_extinta'
            } else {
                // ABA 11.2
                noColunaFiltrarCategoria = 'ficha.sq_categoria_final'
                noColunaFiltrarPEX = 'ficha.st_possivelmente_extinta_final'
            }

            if (params.sqCategoriaAvaliacaoFiltro.toString().indexOf(',')) {
                where.push([condition:"${noColunaFiltrarCategoria} in (${params.sqCategoriaAvaliacaoFiltro})",paramsValue:'', value:''])
            } else {
                where.push([condition:"${noColunaFiltrarCategoria} = ${params.sqCategoriaAvaliacaoFiltro}",paramsValue:'', value:''])
            }
            if( params.stPexAvaliacaoFiltro =~ /S|N/){
                where.push([condition:"${noColunaFiltrarPEX} = ${params.stPexAvaliacaoFiltro}",paramsValue:'', value:''])
            }
        }

        /*if( params.colCategoria ){
            nacionalBrasil = nacionalBrasil ?: getDadosApoioByCodigo('TB_TIPO_AVALIACAO', 'NACIONAL_BRASIL')
            crossJoinTables.push("""LEFT JOIN LATERAL (
                select h.sq_categoria_iucn
                from salve.taxon_historico_avaliacao h
                where h.sq_taxon = ficha.sq_taxon
                and h.sq_tipo_avaliacao = ${nacionalBrasil.id.toString()}
                order by h.nu_ano_avaliacao desc, h.sq_taxon_historico_avaliacao desc
                offset 0 limit 1
                ) historico on true""")
            selectColumns.push('coalesce(ficha.sq_categoria_final,historico.sq_categoria_iucn) as sq_categoria_final')
            //groupByColumns.push('ficha.sq_categoria_final')
            groupByColumns.push('historico.sq_categoria_iucn')
        }*/


        /*
        if ( listCategoriasFiltro || params.colCategoria) {

            //if (params.chkAvaliadaNoCicloFiltro && params.coCategoriaFiltro) {
            if (params.chkAvaliadaNoCicloFiltro && listCategoriasFiltro ) {
                if ( listCategoriasFiltro.size() == 1 ) {
                    if( listCategoriasFiltro.contains(SEM_CATEGORIA) ) {
                        where.push(['condition': "oficinaFicha.sq_categoria_iucn is null ", paramName: '', value: ''])
                    } else {
                        where.push(['condition': "oficinaFicha.sq_categoria_iucn = :idCategoriaOficina", paramName: 'idCategoriaOficina', value: listCategoriasFiltro[0]])
                    }
                } else {
                    if( listCategoriasFiltro.contains(SEM_CATEGORIA) ) {
                        listCategoriasFiltro = listCategoriasFiltro.findAll { it > 0 }
                        where.push(['condition': "( oficinaFicha.sq_categoria_iucn in (" + listCategoriasFiltro.join(',') + ") or oficinaFicha.sq_categoria_iucn_final is null )", paramName: '', value: ''])
                    } else {
                        where.push(['condition': "oficinaFicha.sq_categoria_iucn in (" + listCategoriasFiltro.join(',') + ")", paramName: '', value: ''])
                    }
                }
                if( params.stPexFiltro ) {
                    where.push(['condition': "oficinaFicha.st_possivelmente_extinta = :stPex", paramName: 'stPex', value: params.stPexFiltro.toUpperCase() ])
                }

                // limpar lista para não filtrar novamente abaixo
                listCategoriasFiltro=[]

            } else {
                nacionalBrasil = nacionalBrasil ?: getDadosApoioByCodigo('TB_TIPO_AVALIACAO', 'NACIONAL_BRASIL')
                // remover a coluna sq_categoria_final original e adicionar uma calculada
                selectColumns.push('ficha.sq_taxon')
                groupByColumns.push('ficha.sq_taxon')
                selectColumns.removeAll { it == 'ficha.sq_categoria_final' }

                calcColumn.push("case when ficha.sq_categoria_final is not null then ficha.sq_categoria_final else (" +
                    " select a.sq_categoria_iucn" +
                    " from salve.taxon_historico_avaliacao a"+
                    " inner join salve.dados_apoio categoria on categoria.sq_dados_apoio = a.sq_categoria_iucn"+
                    " where a.sq_tipo_avaliacao = :idNacionalBrasil and a.sq_taxon = ficha.sq_taxon " +
                    " ORDER BY a.nu_ano_avaliacao desc, a.sq_taxon_historico_avaliacao desc limit 1"+
                    ") end as sq_categoria_final")
                if( params.stPexFiltro )
                {
                    calcColumn.push("case when ficha.sq_categoria_final is not null then ficha.st_possivelmente_extinta_final else ( select a.st_possivelmente_extinta " +
                        "from salve.taxon_historico_avaliacao a" +
                        " inner join salve.dados_apoio categoria on categoria.sq_dados_apoio = a.sq_categoria_iucn" +
                        " where a.sq_tipo_avaliacao = :idNacionalBrasil" +
                        " and a.sq_taxon = ficha.sq_taxon" +
                        " ORDER  BY s.nu_ano_avaliacao desc,  a.sq_taxon_historico_avaliacao desc limit 1) end as st_possivelmente_extinta_final")
                    groupByColumns.push('ficha.st_possivelmente_extinta_final')
                }
                // nacionalBrasil = nacionalBrasil ?: getDadosApoioByCodigo('TB_TIPO_AVALIACAO', 'NACIONAL_BRASIL')
                qryParams.idNacionalBrasil = nacionalBrasil.id
                qryParams.nuAnoFinalCicloAvaliacao = cicloAvaliacao.anoFinal

                groupByColumns.push('ficha.sq_categoria_final')

            }
        }*/

        // adicionar as colunas com os dados dos validadores
        if (params.colValidadores == true || params.noValidadorFiltro) {
            selectColumns.push('salve.json_validadores( oficinaFicha.sq_ficha, oficinaFicha.sq_oficina)::TEXT as json_validadores')
            /*selectColumns.push("json_object_agg( coalesce( pessoa.no_pessoa,''), json_build_object(" +
                    "'cd_situacao_convite',dados_apoio.cd_sistema" +
                    ",'in_comunicar_alteracao',case validador_ficha.in_comunicar_alteracao when true then 'S' else 'N' end" +
                    ",'dt_ultimo_comunicado',to_char( validador_ficha.dt_ultimo_comunicado,'dd/mm/yyyy HH24:MI:SS')"+
                    ",'in_respondido', case when validador_ficha.sq_resultado is not null then 'S' else 'N' end"+
                    ",'in_convite_vencido',case when ciclo_avaliacao_validador.dt_validade_convite > current_date then 'N' else 'S' end " +
                    ",'ds_resposta', coalesce(resultado.ds_dados_apoio,'')" +
                    ",'ds_categoria_sugerida', case when categoria_sugerida.sq_dados_apoio is null then '' else categoria_sugerida.ds_dados_apoio||' ('|| categoria_sugerida||')' end" +
                    ",'de_criterio_sugerido', case when validador_ficha.de_criterio_sugerido is null then '' else validador_ficha.de_criterio_sugerido end" +
                    ",'st_possivelmente_extinta', salve.snd(validador_ficha.st_possivelmente_extinta)" +
                    " ) )::text as json_validadores")
            */

            selectColumns.push("oficinaFicha.st_pf_convidado")
            selectColumns.push("oficinaFicha.st_ct_convidado")
            groupByColumns.push('oficinaFicha.st_pf_convidado')
            groupByColumns.push('oficinaFicha.st_ct_convidado')
            groupByColumns.push('oficinaFicha.sq_ficha')
            groupByColumns.push('oficinaFicha.sq_oficina')

            // adicionar a quantidade de chats no resultado
            selectColumns.push('chat.nu_chat');
            groupByColumns.push('chat.nu_chat')
            crossJoinTables.push("""LEFT JOIN LATERAL ( select count(x.sq_oficina_ficha_chat) as nu_chat from salve.oficina_ficha_chat x where x.sq_oficina_ficha = oficinaFicha.sq_oficina_ficha ) chat on true""")
            if( params.inComSemChatFiltro ) {
                where.push(['condition': "chat.nu_chat " + (params.inComSemChatFiltro == 'S' ? ' > 0' : ' = 0'), paramName: '', value: ''])
            }
        }
        if( params.colNomeComum )
        {
            selectColumns.push("array_to_string( array_agg(nomeComum.no_comum),', ') as no_comum")
            leftOuterTables.push('salve.ficha_nome_comum as nomeComum on nomeComum.sq_ficha = ficha.sq_ficha')
        }

        // FILTRAR PELO NÍVEL TAXONOMICO na coluna json_trilha da tabela taxonomia.taxon
        if (params.nivelFiltro && params.sqTaxonFiltro) {
            //and taxon.json_trilha -> 'reino'  ->>'no_taxon' = 'Animalia'
            //and taxon.json_trilha -> 'reino'  ->>'sq_taxon' = '1'
            //and taxon.json_trilha -> 'familia'  ->>'no_taxon' = 'Nymphalidae'
            //and taxon.json_trilha -> 'familia'  ->>'sq_taxon' = '123456'
            leftOuterTables.push('taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon')
            if( params.sqTaxonFiltro.trim().indexOf(',') == -1 ) {
                where.push(['condition': "taxon.json_trilha -> '${params.nivelFiltro.toLowerCase()}' ->> 'sq_taxon' = '${params.sqTaxonFiltro.toString()}'", paramName: '', value: ''])
            }
            else
            {
                where.push(['condition': "(taxon.json_trilha -> '${params.nivelFiltro.toLowerCase()}' ->> 'sq_taxon')::BIGINT in (${params.sqTaxonFiltro})", paramName: '', value: ''])
            }
        }

        // filtrar com / sem ponto focal
        if( params.inComSemPFFiltro )
        {
            DadosApoio pontoFocal = dadosApoioService.getByCodigo('TB_PAPEL_FICHA','PONTO_FOCAL')
            where.push(['condition':(params.inComSemPFFiltro=='SEM' ? 'NOT ':'') + 'EXISTS ( select null from salve.ficha_pessoa x where x.sq_ficha = ficha.sq_ficha AND x.sq_papel = ' + pontoFocal.id.toString() + ' limit 1)', paramName: '', value: ''])
        }

        // filtrar com / sem coordenador de taxon
        if( params.inComSemCTFiltro )
        {
            DadosApoio coordenadorTaxon = dadosApoioService.getByCodigo('TB_PAPEL_FICHA','COORDENADOR_TAXON')
            where.push(['condition':(params.inComSemCTFiltro=='SEM' ? 'NOT ':'') + 'EXISTS ( select null from salve.ficha_pessoa x where x.sq_ficha = ficha.sq_ficha AND x.sq_papel = ' + coordenadorTaxon.id.toString() + ' limit 1)', paramName: '', value: ''])
        }

        // filtrar com / sem imagem principal
        if( params.stImagemPrincipalFiltro ) {
            where.push(['condition':(params.stImagemPrincipalFiltro == 'SEM_IMAGEM' ? 'NOT' : '') + " EXISTS ( select y.cd_sistema from salve.ficha_multimidia x, salve.dados_apoio y where y.sq_dados_apoio = x.sq_tipo and y.cd_sistema = 'IMAGEM' and x.sq_ficha = ficha.sq_ficha and x.in_principal = true LIMIT 1)", paramName: '', value: ''])
        }

        // filtrar com / sem mapa distribuição
        if( params.stMapaDistribuicaoFiltro ) {
            where.push(['condition':(params.stMapaDistribuicaoFiltro == 'SEM_IMAGEM' ? 'NOT' : '') + " EXISTS ( select x.sq_ficha_anexo from salve.ficha_anexo x where x.sq_ficha = ficha.sq_ficha and x.in_principal='S' and x.de_tipo_conteudo ilike '%image%' LIMIT 1)", paramName: '', value: ''])
        }

        // filtrar cadastradas no ciclo atual
        if( params.chkIncluidasCicloFiltro == 'S' ) {
            crossJoinTables.push("""LEFT JOIN LATERAL ( select x.sq_ficha as sq_ficha_anterior
               from salve.ficha x
               inner join salve.ciclo_avaliacao y on y.sq_ciclo_avaliacao = x.sq_ciclo_avaliacao
               where x.sq_taxon = ficha.sq_taxon
               and x.sq_categoria_final is not null
               and y.nu_ano < ${cicloAvaliacao.nuAno}
               offset 0 limit 1) as ficha_anterior on true""")
            where.push([condition: 'ficha.sq_ficha_ciclo_anterior is null', value: ''])
            where.push([condition: 'ficha_anterior.sq_ficha_anterior is null', value: ''])
        }
        if( params.chkInexistenteProximoCicloFiltro == 'S' ) {
            crossJoinTables.push("""LEFT JOIN LATERAL ( select x.sq_ficha as sq_ficha_proximo_ciclo
               from salve.ficha x
               inner join salve.ciclo_avaliacao y on y.sq_ciclo_avaliacao = x.sq_ciclo_avaliacao
               where ( x.sq_taxon = ficha.sq_taxon or x.sq_ficha_ciclo_anterior = ficha.sq_ficha )
               and y.nu_ano > ${cicloAvaliacao.nuAno}
               offset 0 limit 1) as ficha_proximo_ciclo on true""")
            where.push([condition: 'ficha_proximo_ciclo.sq_ficha_proximo_ciclo is null', value: ''])
        }


        sql = new Sql(dataSource)

        // adicionar distinct se possuir somente a coluna sq_ficha na query motor
        if( selectColumns.size() ==1 ) {
            selectColumns[0] = 'distinct '+selectColumns[0]
        }

        sqlCmd = "select ${selectColumns.join(', ')}"
        if (fakeColumn) {
            sqlCmd += "\n,${fakeColumn.join(', ')}"
        }
        if (calcColumn) {
            sqlCmd += "\n,${calcColumn.join(', ')}"
        }

        sqlCmd += "\nfrom ${fromTables.join(', ')} ${(leftOuterTables ? 'left outer join ' : '') + leftOuterTables.join('\nleft outer join ')}"
        sqlCmd += ( crossJoinTables ? '\n'+crossJoinTables.join('\n') : '' )

        where.eachWithIndex { o, i ->
            sqlCmd += '\n' + (i == 0 ? 'where ' : 'and ') + o.condition
            if (o.paramName && o.value) {
                qryParams[o.paramName] = o.value
            }
        }
        if (groupByColumns || params.colNomeComum ) {
            groupByColumns += ['ficha.sq_ficha']
            sqlCmd += "\ngroup by ${groupByColumns.join(',')}"
        }
        if (havingCount) {
            sqlCmd += "\n${havingCount.join(' and ')}"
        }
        if( ! appSession.envProduction ) {
            /** /
             // zzz xxx
             println sqlCmd
             println qryParams
             println '-'*100
             println ' '
             /**/
        }

        // iniciar o sub select select tmp.* from (...)
        calcColumn = []
        selectColumns = []
        groupByColumns = []
        where = []
        leftOuterTables = []
        leftJoinLateralTables = []
        havingCount = []

        if( params.colCategoria ){
            nacionalBrasil = nacionalBrasil ?: getDadosApoioByCodigo('TB_TIPO_AVALIACAO', 'NACIONAL_BRASIL')
            leftJoinLateralTables.push("""( select h.sq_categoria_iucn
                from salve.taxon_historico_avaliacao h
                where h.sq_taxon = vwFicha.sq_taxon
                and h.sq_tipo_avaliacao = ${nacionalBrasil.id.toString()}
                order by h.nu_ano_avaliacao desc, h.sq_taxon_historico_avaliacao desc
                offset 0 limit 1
                ) historico on true""")
            selectColumns.push('coalesce(vwFicha.sq_categoria_final,historico.sq_categoria_iucn) as sq_categoria_final')
            selectColumns.push('cat.ds_dados_apoio as ds_categoria')
            selectColumns.push('cat.cd_sistema as cd_categoria_sistema')
            selectColumns.push('cat.cd_dados_apoio as cd_categoria')
            leftOuterTables.push('salve.dados_apoio cat on cat.sq_dados_apoio = sq_categoria_final')
        }


        // adicionar no resultado da consulta o ID e Numero da última versão da ficha
        if ( params.colUltimaVersao ){

            leftJoinLateralTables.push("""( select x.sq_ficha_versao, x.nu_versao
                            from salve.ficha_versao x
                            where x.sq_ficha = vwFicha.sq_ficha
                            order by x.nu_versao desc limit 1
                            ) as ficha_ultima_versao on true""")

            selectColumns.push('ficha_ultima_versao.sq_ficha_versao as sq_ultima_versao, ficha_ultima_versao.nu_versao as nu_ultima_versao')

        }




        // filtrar COM / SEM PENDENCIA
        if (params.inPendenciaFiltro) {
            operador = '='
            if (params.inPendenciaFiltro.toString() == '2') {
                operador = '>'
            }
            if( ! params.colPendenciaRevisao ) {
                where.push(['condition': "vwFicha.nu_pendencia ${operador} 0", paramName: '', value: ''])
            } else {
                if( operador == '=' ) {
                    where.push(['condition': "(pendencia_revisao.qtd_pendencia_revisao ${operador} 0 or pendencia_revisao.qtd_pendencia_revisao is null)", paramName: '', value: ''])
                } else {
                    where.push(['condition': "pendencia_revisao.qtd_pendencia_revisao ${operador} 0", paramName: '', value: ''])
                }
            }
        }
        // filtrar revisada ou não revisada
        if( params.colPendenciaRevisao ) {
            if (params.inRevisadasFiltro == 'S') {
                where.push(['condition': "pendencia_revisao.qtd_nao_revisada is not null AND pendencia_revisao.qtd_nao_revisada = 0", paramName: '', value: ''])

            } else if (params.inRevisadasFiltro == 'N') {
                where.push(['condition': "pendencia_revisao.qtd_nao_revisada is not null AND pendencia_revisao.qtd_nao_revisada > 0", paramName: '', value: ''])
            }
        }

        /*if ( params.colCategoria && params.chkAvaliadaNoCicloFiltro != 'S' ) {
            selectColumns.push('cat.ds_dados_apoio as ds_categoria')
            selectColumns.push('cat.cd_sistema as cd_categoria_sistema')
            selectColumns.push('cat.cd_dados_apoio as cd_categoria')
            leftOuterTables.push('salve.dados_apoio cat on cat.sq_dados_apoio = tmp.sq_categoria_final')
            if (params.coCategoriaFiltro) {
                if (params.coCategoriaFiltro == 'AMEACADA') {
                    where.push(['condition': "tmp.sq_categoria_final in (" + categoriasAmeacada.id.join(',') + ")", paramName: '', value: ''])
                }else {
                    where.push([condition: 'tmp.sq_categoria_final = :sqCategoriaFinal', paramName: '', value: ''])
                }
            }
        }
        */
        // adicionar a coluna json_trilha no resultado
        if( params.colJsonTrilha == true )
        {
            //selectColumns.push('cat.ds_dados_apoio as ds_categoria')
        }
        if ( params.inValidadoresFiltro ) {
            where.push([condition: 'tmp.nu_validadores '+( params.inValidadoresFiltro == 'S' ? '>' : '=' ) + ' 0', paramName: '', value: ''])
        }

        // filtrar pelo nome dos validadores
        if( params.noValidadorFiltro ) {
            // where.push(['condition': "lower(pessoa.no_pessoa) ilike :noValidador", paramName: 'noValidador', value: '%'+params.noValidadorFiltro.trim().toLowerCase()+'%'])
            //where.push(['condition': "lower(tmp.json_validadores) ilike :noValidador", paramName: 'noValidador', value: '%'+params.noValidadorFiltro.trim().toLowerCase()+'%'])
            where.push(['condition': "array( select json_object_keys( tmp.json_validadores::json) )::text ilike :noValidador", paramName: 'noValidador', value: '%'+params.noValidadorFiltro.trim().toLowerCase()+'%'])
        }

        // adicionar resulta da avaliação final gravados na tabela FICHA
        if( params.colAvaliacaoFinalFicha==true || params.inJustificativaPendenteFiltro )
        {
            selectColumns.push("ficha.sq_categoria_final")
            selectColumns.push("categoriaFinal.cd_dados_apoio as cd_categoria_final")
            selectColumns.push("categoriaFinal.ds_dados_apoio||' ('||categoriaFinal.cd_dados_apoio||')' as ds_categoria_final_completa")
            selectColumns.push("ficha.ds_justificativa_final")
            selectColumns.push("ficha.ds_criterio_aval_iucn_final")

            // campos da ficha que correspondem ao resultado da oficina
            selectColumns.push("ficha.sq_categoria_iucn as sq_categoria_oficina")
            selectColumns.push("categoriaOficina.cd_dados_apoio as cd_categoria_oficina")
            selectColumns.push("categoriaOficina.ds_dados_apoio||' ('||categoriaOficina.cd_dados_apoio||')' as ds_categoria_oficina")
            //selectColumns.push("ficha.ds_justificativa as ds_justificativa_oficina")
            selectColumns.push("ficha.ds_criterio_aval_iucn as ds_criterio_oficina")

            leftOuterTables.push('salve.ficha as ficha on ficha.sq_ficha =vwFicha.sq_ficha')
            leftOuterTables.push('salve.dados_apoio categoriaFinal on categoriaFinal.sq_dados_apoio = ficha.sq_categoria_final')
            leftOuterTables.push('salve.dados_apoio categoriaOficina on categoriaOficina.sq_dados_apoio = ficha.sq_categoria_iucn')

            if ( params.inJustificativaPendenteFiltro) {
                where.push(['condition': "ficha.ds_justificativa_final is null and ficha.sq_categoria_final is not null", paramName: '', value: ''])
            }

        }

        // adicionar colunas da tabela oficina_ficha no resultado da consulta sql
        if ( params.colOficinaFicha || params.sqOficinaFiltro) {
            leftOuterTables.push('salve.oficina_ficha oficina_ficha on oficina_ficha.sq_oficina_ficha = tmp.sq_oficina_ficha')
            leftOuterTables.push('salve.dados_apoio categoria_oficina on categoria_oficina.sq_dados_apoio = oficina_ficha.sq_categoria_iucn')
            leftOuterTables.push('salve.dados_apoio situacao_oficina on situacao_oficina.sq_dados_apoio = oficina_ficha.sq_situacao_ficha')

            selectColumns.push('oficina_ficha.ds_justificativa as ds_justificativa_oficina_ficha')
            selectColumns.push('oficina_ficha.ds_criterio_aval_iucn as ds_criterio_iucn_oficina_ficha')
            selectColumns.push('oficina_ficha.dt_avaliacao as dt_avaliacao_oficina_ficha, oficina_ficha.ds_agrupamento as ds_agrupamento_oficina_ficha')
            selectColumns.push('case when categoria_oficina.ds_dados_apoio is null then null else categoria_oficina.ds_dados_apoio||\' (\'||categoria_oficina.cd_dados_apoio||\')\' end as de_categoria_oficina_ficha')
            selectColumns.push('categoria_oficina.cd_dados_apoio as cd_categoria_oficina_ficha')
            selectColumns.push('categoria_oficina.cd_sistema as cd_categoria_sistema_oficina_ficha')
            selectColumns.push('categoria_oficina.de_dados_apoio_ordem as ds_ordem_categoria_oficina_ficha')
            selectColumns.push('situacao_oficina.cd_sistema as cd_situacao_oficina_ficha')
            selectColumns.push('situacao_oficina.ds_dados_apoio as ds_situacao_oficina_ficha')
            selectColumns.push('situacao_oficina.de_dados_apoio_ordem as ds_ordem_situacao_oficina_ficha')
            selectColumns.push('oficina_ficha.st_transferida as st_transferida_oficina_ficha')
            selectColumns.push('oficina_ficha.st_pf_convidado')
            selectColumns.push('oficina_ficha.st_ct_convidado')

        }


        // listar a situacao da ficha por padrao
        //leftOuterTables.push( 'salve.dados_apoio sit on sit.sq_dados_apoio = vwFicha.sq_situacao_ficha')
        //selectColumns.push('sit.cd_sistema as cd_situacao_ficha_sistema')
        if ( params.colCicloAvaliacao ) {
            leftOuterTables.push('salve.ciclo_avaliacao as ciclo on ciclo.sq_ciclo_avaliacao = vwFicha.sq_ciclo_avaliacao')
            selectColumns.push('ciclo.nu_ano as nu_ano_ciclo')
            selectColumns.push('ciclo.de_ciclo_avaliacao')
            selectColumns.push('ciclo.in_situacao as in_situacao_ciclo')
        }


        // para calcular o canModify precisa da situacao do ciclo
        if (params.colCanModify) {
            if (appSession?.sicae?.user.isADM()) {
                selectColumns.push('true as st_alterar')
            } else {
                if ( ! params.colCicloAvaliacao ) {
                    leftOuterTables.push('salve.ciclo_avaliacao as ciclo on ciclo.sq_ciclo_avaliacao = vwFicha.sq_ciclo_avaliacao')
                    selectColumns.push('ciclo.in_situacao as in_situacao_ciclo')
                }
                selectColumns.push("(vwFicha.sq_unidade_org = :unidadeOrgUsuario and ciclo.in_situacao <> 'F') as st_alterar")
                qryParams.unidadeOrgUsuario = appSession?.sicae?.user?.sqUnidadeOrg
            }
        }

        // adicionar a visao vw_pessoa para exibir o nome do usuario que publicou a ficha
        if( params.colPublicacao ) {
            leftOuterTables.push('salve.vw_pessoa as vwPessoa on vwPessoa.sq_pessoa = tmp.sq_usuario_publicacao')
            selectColumns.push('vwPessoa.no_pessoa as no_publicada_por,tmp.dt_publicacao ')
        }

        // adicinar a tabela taxonomia.taxon para ordenar pela json_trilha.especie e subespecie
        leftOuterTables.push('taxonomia.taxon as taxon on taxon.sq_taxon = vwFicha.sq_taxon')
        selectColumns.push('taxon.no_autor')
        // para calcular o nivel taxonomico precisa da Taxon
        if ( params.colNivelTaxonomico || params.colJsonTrilha ) {
            if ( params.colNivelTaxonomico ) {
                leftOuterTables.push('taxonomia.nivel_taxonomico as nivel_taxon on nivel_taxon.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico')
                selectColumns.push('nivel_taxon.co_nivel_taxonomico as co_nivel_taxonomico')
            }
            if ( params.colJsonTrilha ) {
                selectColumns.push('taxon.json_trilha')
            }
        }

        // adicionar colunas do grupo da especie
        if (params.colGrupoTaxonomico) {
            leftOuterTables.push('salve.dados_apoio as grupo on grupo.sq_dados_apoio = vwFicha.sq_grupo')
            selectColumns.push('grupo.ds_dados_apoio as ds_grupo, grupo.cd_sistema as cd_grupo_sistema')
            leftOuterTables.push('salve.dados_apoio as subgrupo on subgrupo.sq_dados_apoio = vwFicha.sq_subgrupo')
            selectColumns.push('subgrupo.ds_dados_apoio as ds_subgrupo, subgrupo.cd_sistema as cd_subgrupo_sistema')
            leftOuterTables.push('salve.dados_apoio as grupoSalve on grupoSalve.sq_dados_apoio = vwFicha.sq_grupo_salve')
            selectColumns.push('grupoSalve.ds_dados_apoio as ds_grupo_salve, grupoSalve.cd_sistema as cd_grupo_salve_sistema')
        }
/*
        if( listCategoriasFiltro )
        {
            if( listCategoriasFiltro.size() == 1 ) {
                // quando o codigo for -9 signica filtrar os registro sem categoria preenchida
                if( listCategoriasFiltro.contains(SEM_CATEGORIA) ) {
                    where.push(['condition': "( tmp.sq_categoria_final is null )", paramName: '', value:'' ] )
                } else {
                    where.push(['condition': "(tmp.sq_categoria_final = :sqCategoriaFinal )", paramName: 'sqCategoriaFinal', value: listCategoriasFiltro[0]])
                }
            }
            else
            {
                if( listCategoriasFiltro.contains( SEM_CATEGORIA ) ) {
                    listCategoriasFiltro = listCategoriasFiltro.findAll{ it > 0 }
                    where.push(['condition': "( tmp.sq_categoria_final in (" + listCategoriasFiltro.join(',') + ")  or tmp.sq_categoria_final is null )", paramName: '', value: ''])
                } else {
                    where.push(['condition': "(tmp.sq_categoria_final in (" + listCategoriasFiltro.join(',') + ") )", paramName: '', value: ''])
                }
            }
        }*/

        if( params.colPendenciaRevisao ) {
            selectColumns.push('pendencia_revisao.qtd_pendencia_revisao')
            selectColumns.push('pendencia_revisao.qtd_nao_resolvida')
            selectColumns.push('pendencia_revisao.qtd_nao_revisada')
            leftJoinLateralTables.push("""( select sum(case when x.st_pendente='S' then 1 else 1 end ) as qtd_pendencia_revisao
                        , sum( case when x.sq_usuario_resolveu is not null and x.sq_usuario_revisou is null then 1 else 0 end) as qtd_nao_revisada
                        , sum( case when x.sq_usuario_resolveu is null then 1 else 0 end) as qtd_nao_resolvida
                       from salve.ficha_pendencia x
                       inner join salve.dados_apoio as y on y.sq_dados_apoio = x.sq_contexto
                       where x.sq_ficha = vwFicha.sq_ficha
                         and y.cd_sistema = 'REVISAO'
                    ) as pendencia_revisao on true""")

        }


        if( params.colTotalCarencia ) {
            String dtFimCarencia = new Date().format('yyyy-MM-dd')
            selectColumns.push('carencia.qtd_carencia')
            leftJoinLateralTables.push("""( SELECT sum(tmp.qtd_carencia) AS qtd_carencia
                FROM ( SELECT sum(CASE
                    WHEN a.dt_carencia > '${dtFimCarencia}' THEN 1
                    ELSE 0
                    END) AS qtd_carencia
                FROM salve.vw_ficha_ocorrencia a
                INNER JOIN salve.ficha ON ficha.sq_ficha = a.sq_ficha
                INNER JOIN salve.dados_apoio situacao ON situacao.sq_dados_apoio = ficha.sq_situacao_ficha
                WHERE a.in_utilizado_avaliacao = 'S'
                AND situacao.cd_sistema <>'EXCLUIDA'
                AND a.sq_ficha = vwFicha.sq_ficha
                UNION ALL
                SELECT sum(CASE  WHEN a.dt_carencia > '${dtFimCarencia}' THEN 1
                                     ELSE 0
                                     END) AS qtd_carencia
                FROM salve.ficha_ocorrencia_portalbio a
                INNER JOIN salve.ficha ON ficha.sq_ficha = a.sq_ficha
                INNER JOIN salve.dados_apoio situacao ON situacao.sq_dados_apoio = ficha.sq_situacao_ficha
                WHERE situacao.cd_sistema <>'EXCLUIDA'
                AND a.tx_ocorrencia <> '{}'
                AND a.in_utilizado_avaliacao = 'S'
                AND a.sq_ficha = vwFicha.sq_ficha
                ) tmp
                ) carencia on true""")
        }

        // adicionar no retorno da query as colunas da tabela taxon_historico_avaliacao referentes a última avaliação nacional
        if( params.colHistorico )
        {
            nacionalBrasil = nacionalBrasil ?: getDadosApoioByCodigo('TB_TIPO_AVALIACAO', 'NACIONAL_BRASIL')
            selectColumns.push('hist.*')
            // subquery para ler as informaçõe da última avaliação nacional do taxon
            // aqui so funcionou com left join lateral
            leftJoinLateralTables.push("""( select a.sq_taxon, a.sq_taxon_historico_avaliacao
                        ,a.tx_justificativa_avaliacao as tx_justificativa_aval_nacional
                        ,a.nu_ano_avaliacao as nu_ano_aval_nacional
                        ,a.de_criterio_avaliacao_iucn as de_criterio_aval_nacional
                        ,categoria.sq_dados_apoio as sq_categoria_nacional
                        ,categoria.ds_dados_apoio as ds_categoria_iucn_nacional
                        ,categoria.cd_dados_apoio as cd_categoria_iucn_sistema_nacional
                        ,categoria.cd_sistema as cd_categoria_iucn_nacional
                        ,categoria.ds_dados_apoio as de_ordem
                        from salve.taxon_historico_avaliacao a
                        inner join salve.dados_apoio categoria on categoria.sq_dados_apoio = a.sq_categoria_iucn
                        where a.sq_tipo_avaliacao = :idNacionalBrasil
                        and a.sq_taxon = vwFicha.sq_taxon
                        order by a.nu_ano_avaliacao desc, a.sq_taxon_historico_avaliacao desc limit 1 ) hist on true""")
            qryParams.anoCicloAvaliacao = cicloAvaliacao.nuAno
            qryParams.idNacionalBrasil = nacionalBrasil.id
        }

        if( params.colJustificativaFicha )
        {
            leftOuterTables.push("""salve.ficha on ficha.sq_ficha = vwFicha.sq_ficha""")
            selectColumns.push('ficha.ds_justificativa as ds_justificativa_ficha')
        }

        if( params.colDOI) {

            if( ! leftOuterTables.contains( 'salve.ficha on ficha.sq_ficha = vwFicha.sq_ficha') ) {
                leftOuterTables.push("""salve.ficha on ficha.sq_ficha = vwFicha.sq_ficha""")
            }
            selectColumns.push('ficha.ds_doi')
        }

        if( params.stPexFiltro )
        {
            if( params.stPexFiltro == 'S') {
                where.push(['condition': "tmp.st_possivelmente_extinta_final = :stPex", paramName: 'stPex', value: params.stPexFiltro.toUpperCase()])
            }
            else
            {
                where.push(['condition': "( tmp.st_possivelmente_extinta_final = :stPex or vwFicha.st_possivelmente_extinta_final is null )", paramName: 'stPex', value: params.stPexFiltro.toUpperCase()])
            }
        }

        // construir o comando SQL final
        String sqlCmdFinal = "select vwFicha.*,tmp.*"
        if (selectColumns) {
            sqlCmdFinal += ',' + selectColumns.join(', ')
        }

        if (calcColumn) {
            sqlCmdFinal += "\n,${calcColumn.join(', ')}"
        }
        if (fakeColumn) {
            sqlCmdFinal += "\n,${fakeColumn.join(', ')}"
        }
        sqlCmd = sqlCmdFinal + "\nfrom ($sqlCmd) as tmp"

        sqlCmd += "\ninner join salve.vw_ficha as vwFicha on vwFicha.sq_ficha = tmp.sq_ficha_tmp"
        sqlCmd += (leftOuterTables ? '\nleft outer join ' : '') + leftOuterTables.join('\nleft outer join ')
        sqlCmd += (leftJoinLateralTables ? '\nleft join lateral ' : '') + leftJoinLateralTables.join('\nleft join lateral ')

        where.eachWithIndex { o, i ->
            sqlCmd += '\n' + (i == 0 ? 'where ' : 'and ') + o.condition
            if (o.paramName && o.value) {
                qryParams[o.paramName] = o.value
            }
        }

        if (groupByColumns) {
            sqlCmd += "\ngroup by ${groupByColumns.join(',')}"
        }
        if (havingCount) {
            sqlCmd += "\n${havingCount.join(' and ')}"
        }

        // ordem ESPECIE + SUBESPECIE de exibição final
        sqlCmd += "\norder by coalesce(taxon.json_trilha->'especie'->>'no_taxon',vwFicha.nm_cientifico),coalesce(taxon.json_trilha->'subespecie'->>'no_taxon','')"
        if( ! appSession.envProduction ) {
 /** /
 // zzz xxx
 println ' '
 println ' '
 println 'fichaService.search()'
 println '- '*50
 println sqlCmd
 println qryParams
 println '- '*50
 println ' '
 /**/
        }
        // se o parametro pagination for informado, fazer o count() e depois a query normal
        if( pagination ) {
            pagination.pageNumber = pagination.pageNumber ?: 1
            pagination.pageSize = pagination.pageSize ?: 100
            pagination.offset = 0

            if( pagination.pageSize > 0 ) {
                data=[]
                if( ! pagination?.totalRecords ) {
                    /*println ' '
                    println 'Iniciando select count() ' + new Date().format('HH:mm:ss')
                    data = sql.rows( "select count(countTemp.sq_ficha) as nu_total from ( ${ sqlCmd } ) as countTemp", qryParams )
                    pagination.totalRecords = data[ 0 ].nu_total.toLong()
                    println 'count() = ' + pagination.totalRecords + ' as ' + new Date().format('HH:mm:ss')
                     */
                    //println sqlCmd
                    //println qryParams
                    data = sql.rows(sqlCmd, qryParams)
                    pagination.totalRecords = data.size()
                    // recortar os registros da pagina 1
                    data = data.collate( pagination.pageSize )[0]
                }

                // calcular quantidade de paginas
                if( pagination.totalRecords > 0 ) {
                    pagination.totalPages = Math.max( 1, Math.ceil( pagination.totalRecords / pagination.pageSize ) )
                    // calcular o offset
                    pagination.offset = ( pagination.pageSize * ( pagination.pageNumber - 1 ) )
                    if( pagination.offset >= pagination.totalRecords ) {
                        pagination.offset = Math.max( 0, ( pagination.totalRecords - pagination.pageSize ) - 1 )
                    }
                } else {
                    pagination.totalPages = 0
                }
                if( !data ){
                    // adicionar offset e limit na query
                    data = sql.rows(sqlCmd + " offset ${pagination.offset} limit ${pagination.pageSize}", qryParams)
                }
            } else {
                data = sql.rows(sqlCmd, qryParams)
            }
            Map res = [data:data,pagination:pagination];
            return [res]
        } else {
            data = sql.rows(sqlCmd, qryParams)
        }
        sql.close()
        if ( data.size() > 0 ) {

            // FILTRAR PELO NIVEL TAXONÔMICO
            /*if (params.nivelFiltro && params.sqTaxonFiltro) {
                String coluna = 'id' + params.nivelFiltro.toLowerCase().capitalize()
                Integer idTaxon = params.sqTaxonFiltro.toInteger()
                data.removeAll { item ->
                    Taxon t = Taxon.get(item.sq_taxon.toInteger())
                    Map arvoreTaxon = t.getStructure()
                    return arvoreTaxon[coluna]?.toInteger() != idTaxon
                }
            }
            */
            // TODO - Criar função no banco de dados para retornar as colaborações e colaboradores das consulta amplas e direta
            // ADICIONAR AS COLABORAÇÕES
            qryParams = [:]
            if (params.colColaboracao) {
                // ler as colaborações textuais
                sqlCmd = "select b.sq_ficha\n" +
                    ",d.cd_sistema as cd_tipo_consulta_sistema\n" +
                    ",e.cd_sistema as cd_situacao_colaboracao_sistema\n" +
                    ",f.no_usuario \n" +
                    ",a.sq_web_usuario\n" +
                    ",situacao_ficha.cd_sistema as cd_situacao_ficha_sistema\n" +
                    "from salve.ficha_colaboracao a \n" +
                    "inner join salve.ciclo_consulta_ficha b on b.sq_ciclo_consulta_ficha = a.sq_ciclo_consulta_ficha\n" +
                    "inner join salve.ciclo_consulta c on c.sq_ciclo_consulta = b.sq_ciclo_consulta\n" +
                    "inner join salve.dados_apoio d on d.sq_dados_apoio = c.sq_tipo_consulta\n" +
                    "inner join salve.dados_apoio e on e.sq_dados_apoio = a.sq_situacao\n" +
                    "inner join salve.web_usuario f on f.sq_web_usuario = a.sq_web_usuario\n" +
                    "inner join salve.ficha on ficha.sq_ficha = b.sq_ficha\n"+
                    "inner join salve.dados_apoio situacao_ficha on situacao_ficha.sq_dados_apoio = ficha.sq_situacao_ficha\n" +
                    "where b.sq_ficha in (${data.sq_ficha.join(',')})\n" +
                    "and a.ds_colaboracao <> ''"

                if(params.sqCicloConsultaFiltro) {
                    sqlCmd += " and b.sq_ciclo_consulta = :sqCicloConsulta\n"
                    qryParams['sqCicloConsulta'] = params.sqCicloConsultaFiltro.toLong()
                }
                if (params.noColaboradorFiltro) {
                    sqlCmd += " and f.no_usuario ilike :noColaborador\n"
                    qryParams['noColaborador'] = '%' + params.noColaboradorFiltro + '%'
                }
                sqlCmd += "order by b.sq_ficha,f.no_usuario"

                /** /
                 println ' '
                 println 'SQL'
                 println sqlCmd
                 /**/

                GroovyRowResult dataRow
                sql.eachRow(sqlCmd, qryParams) { row ->
                    if (!dataRow || dataRow.sq_ficha != row.sq_ficha) {
                        dataRow = data.find { it.sq_ficha == row.sq_ficha }
                    }
                    if (dataRow) {
                        if (!dataRow.colaboracoes) {
                            dataRow.colaboracoes = [ampla: [], direta: [], revisao: [], planilha:[], registros:[]]
                        }
                        boolean stConcluida = false
                        if( row.cd_situacao_ficha_sistema =~ /^(EM_VALIDACAO|VALIDADA|VALIDADA_EM_REVISAO|FINALIZADA)$/ ) {
                            stConcluida = ! ( row.cd_situacao_colaboracao_sistema =~ /(NAO_AVALIADA|RESOLVER_OFICINA)$/ )
                        } else {
                            stConcluida = row.cd_situacao_colaboracao_sistema != 'NAO_AVALIADA'
                        }
                        Map dadosColaboracao = ['no_usuario': row.no_usuario, 'sq_web_usuario': row.sq_web_usuario, 'st_concluida': stConcluida]
                        if (row.cd_tipo_consulta_sistema == 'CONSULTA_AMPLA') {

                            Map colaboracaoTemp = dataRow.colaboracoes.ampla.find {
                                it.no_usuario == dadosColaboracao.no_usuario
                            }
                            if( colaboracaoTemp )
                            {
                                if( ! dadosColaboracao.st_concluida )
                                {
                                    colaboracaoTemp.st_concluida = false
                                }
                            }
                            else {
                                dataRow.colaboracoes.ampla.push(dadosColaboracao)
                            }
                            /*
                            // se ja tiver uma colaboraboracao não avaliada, não pricisa adicionar mais nenhuma
                            if (dadosColaboracao.st_concluida) {
                                if (dataRow.colaboracoes.ampla.size == 0) {
                                    dataRow.colaboracoes.ampla.push(dadosColaboracao)
                                }
                            } else {
                                if (dataRow.colaboracoes.ampla.size == 0) {
                                    dataRow.colaboracoes.ampla.push(dadosColaboracao)
                                } else {
                                    dataRow.colaboracoes.ampla[0].st_concluida = false
                                }
                            }
                            */
                        } else if (row.cd_tipo_consulta_sistema == 'CONSULTA_DIRETA') {
                            Map colaboracaoTemp = dataRow.colaboracoes.direta.find {
                                it.no_usuario == dadosColaboracao.no_usuario
                            }
                            if( colaboracaoTemp )
                            {
                                if( ! dadosColaboracao.st_concluida )
                                {
                                    colaboracaoTemp.st_concluida = false
                                }
                            }
                            else {
                                dataRow.colaboracoes.direta.push(dadosColaboracao)
                            }

                            /*
                            if (dadosColaboracao.st_concluida) {
                                if (dataRow.colaboracoes.direta.size == 0) {
                                    dataRow.colaboracoes.direta.push(dadosColaboracao)
                                }
                            } else {
                                if (dataRow.colaboracoes.direta.size == 0) {
                                    dataRow.colaboracoes.direta.push(dadosColaboracao)
                                } else {
                                    dataRow.colaboracoes.direta[0].st_concluida = false
                                }
                            }
                            */

                        } else if (row.cd_tipo_consulta_sistema == 'REVISAO_POS_OFICINA') {

                            Map colaboracaoTemp = dataRow.colaboracoes.revisao.find {
                                it.no_usuario == dadosColaboracao.no_usuario
                            }
                            if( colaboracaoTemp )
                            {
                                if( ! dadosColaboracao.st_concluida )
                                {
                                    colaboracaoTemp.st_concluida = false
                                }
                            }
                            else {
                                dataRow.colaboracoes.revisao.push(dadosColaboracao)
                            }
                            /*
                            if (dadosColaboracao.st_concluida) {
                                if (dataRow.colaboracoes.revisao.size == 0) {
                                    dataRow.colaboracoes.revisao.push(dadosColaboracao)
                                }
                            } else {
                                if (dataRow.colaboracoes.revisao.size == 0) {
                                    dataRow.colaboracoes.revisao.push(dadosColaboracao)
                                } else {
                                    dataRow.colaboracoes.revisao[0].st_concluida = false
                                }
                            }
                            */

                        }
                    }
                }
                dataRow = null
                sql.close()
                qryParams=[:]
                // ler as colaboracoes de ocorrencias ( pontos da consulta ampla/direta )
                sqlCmd = "select b.sq_ficha,f.no_usuario,f.sq_web_usuario,d.cd_sistema as cd_tipo_consulta_sistema, g.sq_situacao, h.cd_sistema as cd_situacao_colaboracao_sistema\n" +
                    "from salve.ficha_ocorrencia_consulta a\n" +
                    "inner join salve.ciclo_consulta_ficha b on b.sq_ciclo_consulta_ficha = a.sq_ciclo_consulta_ficha\n" +
                    "inner join salve.ciclo_consulta c on c.sq_ciclo_consulta = b.sq_ciclo_consulta\n" +
                    "inner join salve.dados_apoio d on d.sq_dados_apoio = c.sq_tipo_consulta\n" +
                    "inner join salve.web_usuario f on f.sq_web_usuario = a.sq_web_usuario\n" +
                    "inner join salve.ficha_ocorrencia g on g.sq_ficha_ocorrencia = a.sq_ficha_ocorrencia\n" +
                    "inner join salve.dados_apoio h on h.sq_dados_apoio = g.sq_situacao\n" +
                    "where b.sq_ficha in ( ${data.sq_ficha.join(',')} )\n"

                if(params.sqCicloConsultaFiltro) {
                    sqlCmd += " and b.sq_ciclo_consulta = :sqCicloConsulta\n"
                    qryParams['sqCicloConsulta'] = params.sqCicloConsultaFiltro.toLong()
                }
                if (params.noColaboradorFiltro) {
                    sqlCmd += "and f.no_usuario ilike :noColaborador\n"
                    qryParams['noColaborador'] = '%' + params.noColaboradorFiltro + '%'
                }
                /** /
                 println ' '
                 println sqlCmd
                 /**/
                sqlCmd += "order by b.sq_ficha,f.no_usuario"
                sql.eachRow(sqlCmd, qryParams) { row ->
                    if (!dataRow || dataRow.sq_ficha != row.sq_ficha) {
                        dataRow = data.find { it.sq_ficha == row.sq_ficha }
                    }
                    if (dataRow) {
                        if (!dataRow.colaboracoes) {
                            dataRow.colaboracoes = [ampla: [], direta: [], revisao: [], planilha:[], registros:[]]
                        }
                        Map dadosColaboracao = ['no_usuario': row.no_usuario, 'sq_web_usuario': row.sq_web_usuario, 'st_concluida': row.cd_situacao_colaboracao_sistema != 'NAO_AVALIADA']
                        if (row.cd_tipo_consulta_sistema == 'CONSULTA_AMPLA') {

                            Map colaboracaoTemp = dataRow.colaboracoes.ampla.find {
                                it.no_usuario == dadosColaboracao.no_usuario
                            }
                            if( colaboracaoTemp )
                            {
                                if( ! dadosColaboracao.st_concluida )
                                {
                                    colaboracaoTemp.st_concluida = false
                                }
                            }
                            else {
                                dataRow.colaboracoes.ampla.push(dadosColaboracao)
                            }
                        } else if (row.cd_tipo_consulta_sistema == 'CONSULTA_DIRETA') {
                            Map colaboracaoTemp = dataRow.colaboracoes.direta.find {
                                it.no_usuario == dadosColaboracao.no_usuario
                            }
                            if( colaboracaoTemp )
                            {
                                if( ! dadosColaboracao.st_concluida )
                                {
                                    colaboracaoTemp.st_concluida = false
                                }
                            }
                            else {
                                dataRow.colaboracoes.direta.push(dadosColaboracao)
                            }
                        } else if (row.cd_tipo_consulta_sistema == 'REVISAO_POS_OFICINA') {

                            Map colaboracaoTemp = dataRow.colaboracoes.revisao.find {
                                it.no_usuario == dadosColaboracao.no_usuario
                            }
                            if( colaboracaoTemp )
                            {
                                if( ! dadosColaboracao.st_concluida )
                                {
                                    colaboracaoTemp.st_concluida = false
                                }
                            }
                            else {
                                dataRow.colaboracoes.revisao.push(dadosColaboracao)
                            }
                        }
                    }
                }
                dataRow = null
                sql.close()
                qryParams = [:]
                // ler as colaborações feitas nos registros de ocorrencias quando marca sim/nao como valido
                sqlCmd = """select coalesce(b.sq_ficha,c.sq_ficha) as sq_ficha
                                ,a.sq_web_usuario
                                ,d.no_usuario
                                ,a.in_valido
                                ,a.tx_observacao
                                ,a.dt_inclusao
                                ,coalesce(e.cd_sistema,f.cd_sistema) as cd_situacao_sistema
                                from salve.ficha_ocorrencia_validacao a
                                left outer join salve.ficha_ocorrencia b on b.sq_ficha_ocorrencia = a.sq_ficha_ocorrencia
                                left outer join salve.ficha_ocorrencia_portalbio c on a.sq_ficha_ocorrencia_portalbio = c.sq_ficha_ocorrencia_portalbio
                                left outer join salve.web_usuario d on d.sq_web_usuario = a.sq_web_usuario
                                left outer join salve.dados_apoio e on e.sq_dados_apoio = b.sq_situacao
                                left outer join salve.dados_apoio f on f.sq_dados_apoio = c.sq_situacao
                                where a.in_valido is not null
                                  and a.in_valido = 'N'
                                  and ( b.sq_ficha in ( ${data.sq_ficha.join(',')} ) or c.sq_ficha in ( ${data.sq_ficha.join(',')} ) )
                                """

                if (params.noColaboradorFiltro) {
                    sqlCmd += "\nand d.no_usuario ilike :noColaborador\n"
                    qryParams['noColaborador'] = '%' + params.noColaboradorFiltro + '%'
                }
                sqlCmd += "\norder by 1,2,3"

                /** /
                 println ' '
                 println sqlCmd
                 /**/


                sql.eachRow(sqlCmd, qryParams) { row ->
                    if (!dataRow || dataRow.sq_ficha != row.sq_ficha) {
                        dataRow = data.find { it.sq_ficha == row.sq_ficha }
                    }
                    if ( dataRow ) {
                        if (!dataRow.colaboracoes) {
                            dataRow.colaboracoes = [ampla: [], direta: [], revisao: [], planilha:[] ,registros:[]]
                        }
                        Map dadosColaboracao = ['no_usuario': row.no_usuario
                                                ,'sq_web_usuario': row.sq_web_usuario]
                        dadosColaboracao.st_concluida =  (row.cd_situacao_sistema == 'ACEITA' )
                        if( params.contexto == 'consulta' ) {
                            dadosColaboracao.st_concluida =  ( (row.cd_situacao_sistema =~ /ACEITA|RESOLVER_OFICINA/) ? true : false )
                        }
                        Map colaboracaoTemp = dataRow.colaboracoes.ampla.find {
                            it.no_usuario == dadosColaboracao.no_usuario
                        }
                        if( ! colaboracaoTemp ) {
                            if( ! dataRow.colaboracoes.registros.contains( dadosColaboracao ) ) {
                                dataRow.colaboracoes.registros.push( dadosColaboracao )
                            }
                        }
                        /* por enquanto nao tem a consulta
                        if (row.cd_tipo_consulta_sistema == 'CONSULTA_AMPLA') {

                            Map colaboracaoTemp = dataRow.colaboracoes.ampla.find {
                                it.no_usuario == dadosColaboracao.no_usuario
                            }
                            if( colaboracaoTemp )
                            {
                                if( ! dadosColaboracao.st_concluida )
                                {
                                    colaboracaoTemp.st_concluida = false
                                }
                            }
                            else {
                                dataRow.colaboracoes.ampla.push(dadosColaboracao)
                            }
                        } else if (row.cd_tipo_consulta_sistema == 'CONSULTA_DIRETA') {
                            Map colaboracaoTemp = dataRow.colaboracoes.direta.find {
                                it.no_usuario == dadosColaboracao.no_usuario
                            }
                            if( colaboracaoTemp )
                            {
                                if( ! dadosColaboracao.st_concluida )
                                {
                                    colaboracaoTemp.st_concluida = false
                                }
                            }
                            else {
                                dataRow.colaboracoes.direta.push(dadosColaboracao)
                            }
                        } else if (row.cd_tipo_consulta_sistema == 'REVISAO_POS_OFICINA') {

                            Map colaboracaoTemp = dataRow.colaboracoes.revisao.find {
                                it.no_usuario == dadosColaboracao.no_usuario
                            }
                            if( colaboracaoTemp )
                            {
                                if( ! dadosColaboracao.st_concluida )
                                {
                                    colaboracaoTemp.st_concluida = false
                                }
                            }
                            else {
                                dataRow.colaboracoes.revisao.push(dadosColaboracao)
                            }
                        }*/
                    }
                }
                dataRow = null
                sql.close()
                qryParams=[:]
                // ler as colaboracoes de planilhas de registros de ocorrencias
                sqlCmd = """select a.sq_ficha_consulta_anexo, b.sq_ficha
                                ,c.no_usuario
                                ,c.sq_web_usuario
                                ,e.cd_sistema as cd_tipo_consulta_sistema
                                ,a.sq_situacao
                                ,f.cd_sistema as cd_situacao_colaboracao_sistema
                                ,a.no_arquivo
                                ,a.de_local_arquivo
                                ,a.dt_inclusao
                                from salve.ficha_consulta_anexo a
                                inner join salve.ciclo_consulta_ficha b on b.sq_ciclo_consulta_ficha = a.sq_ciclo_consulta_ficha
                                inner join salve.web_usuario c on c.sq_web_usuario = a.sq_web_usuario
                                inner join salve.ciclo_consulta d on d.sq_ciclo_consulta = b.sq_ciclo_consulta
                                inner join salve.dados_apoio e on e.sq_dados_apoio = d.sq_tipo_consulta
                                inner join salve.dados_apoio f on f.sq_dados_apoio = a.sq_situacao
                                where b.sq_ficha in ( ${data.sq_ficha.join(',')} )"""

                if (params.noColaboradorFiltro) {
                    sqlCmd += "and c.no_usuario ilike :noColaborador\n"
                    qryParams['noColaborador'] = '%' + params.noColaboradorFiltro + '%'
                }
                sqlCmd += "order by b.sq_ficha,c.no_usuario"

                /*
                    // anexos
                    println ' '
                    println 'SQL'
                    println sqlCmd
                */

                sql.eachRow(sqlCmd, qryParams) { row ->
                    if (!dataRow || dataRow.sq_ficha != row.sq_ficha) {
                        dataRow = data.find { it.sq_ficha == row.sq_ficha }
                    }
                    if (dataRow) {
                        if ( ! dataRow.colaboracoes ) {
                            dataRow.colaboracoes = [ampla: [], direta: [], revisao: [], planilha: [], registros:[] ]
                        }
                        Map dadosColaboracao = ['sq_ficha_consulta_anexo': row.sq_ficha_consulta_anexo
                                                ,'no_usuario'       :row.no_usuario
                                                ,'sq_web_usuario'   :row.sq_web_usuario
                                                ,'st_concluida'     :(row.cd_situacao_colaboracao_sistema == 'ACEITA')
                                                ,'no_arquivo'       :row.no_arquivo
                                                ,'de_local_arquivo' :row.de_local_arquivo
                                                ,'dt_inclusao'      :row?.dt_inclusao?.format('dd/MM/yyyy HH:mm:ss')
                                                ,'cd_tipo_consulta_sistema': row.cd_tipo_consulta_sistema
                        ]
                        Map colaboracaoTemp = dataRow.colaboracoes.planilha.find {
                            it.no_usuario == dadosColaboracao.no_usuario
                        }
                        if (colaboracaoTemp) {
                            if (!dadosColaboracao.st_concluida) {
                                colaboracaoTemp.st_concluida = false
                            }
                        } else {
                            dataRow.colaboracoes.planilha.push(dadosColaboracao)
                        }
                    }
                }

                // ordernar pelo nome do usuario e tipo da consulta
                if( dataRow && dataRow.colaboracoes ) {
                    dataRow.colaboracoes.planilha.sort { a, b ->
                        a.no_usuario + a.cd_tipo_consulta_sistema <=> b.no_usuario + b.cd_tipo_consulta_sistema
                    }
                }
                dataRow = null
                sql.close()


                //--------------------------------------------------------------


                // excluir as linhas que não tiverem colaboração do colaborador selecionado
                if (params.noColaboradorFiltro) {
                    data.removeAll { item ->
                        if (item.colaboracoes) {
                            !item.colaboracoes.find { k, colabs ->
                                !colabs.find {
                                    it.no_usuario.toLowerCase().indexOf(params.noColaboradorFiltro) == -1
                                }
                            }
                        } else {
                            true // deletar
                        }
                    }
                }
            }
        }
        sql = null
        /*
        } catch (Exception e) {
            if (sql) {
                sql.close()
                sql = null
            }
            println ' '
            println ' '
            println 'Erro em fichaService.search() ............................'
            println e.getMessage()
            println ' '
            println 'Sql gerado '
            println sqlCmd
            println ' '
            println 'Parametros recebidos'
            println qryParams
            println "-" * 100
            data = []
        }
*/
        // se passou o parametro pagination retornar como Map
        if( pagination ){
            Map res = [ data:data, pagination:pagination ]
            return res
        }


        // fazer a paginação do resultado
        // inicializar dados da paginação
        //params.paginationPageSize = params.paginationPageSize?:50
        params.paginationOffSet = 0 // fazer a numeração correta das linhas do gride
        // se passou page size ativar controle de paginacao
        if (params.paginationPageSize && data.size() > 0 ) {
            if (!params.paginationCurrentPage) {
                params.paginationCurrentPage = 1
            }
            params.paginationTotalRecords = data.size()
            if (params.paginationTotalRecords.toInteger() > params.paginationPageSize.toInteger()) {
                params.paginationTotalPages = Math.ceil(params.paginationTotalRecords.toInteger() / params.paginationPageSize.toInteger())
            } else {
                if (params.paginationTotalRecords.toInteger() > 0) {
                    params.paginationTotalPages = 1
                } else {
                    params.paginationTotalPages = 0
                }
            }
            params.paginationOffSet = ((params.paginationCurrentPage.toInteger() - 1) * params.paginationPageSize.toInteger())
            data = data.collate(params.paginationPageSize)[(params.paginationCurrentPage.toInteger() - 1)]
        }
        // adicionar campo canModify no resultado
        /*if( params.colCanModify )
        {
            data.each { it ->
                it.canModify = canModifyRow( it )
            }
        }
        */
        return data
    } // order

    //@Cacheable(value = 'list')
    List list(GrailsParameterMap params) {
        getSession()
        if( ! this.appSession?.sicae?.user )
        {
            return []
        }

        // def timeStart = new Date()
        // def timeStop  = new Date()
        // TimeDuration duration
        // println 'Inicio: ' + timeStart

        //Map limit = [:]
        Pessoa pessoa
        DadosApoio papel
        List listFichasPontoFocal = []
        //String cacheKey=''
        List fichas = []

        /*if( cacheService )
        {
            cacheKey = cacheService.getKey( params )
            fichas = cacheService.getCache( cacheKey )['data']
        }
        */

        if (!fichas) {
            // coordenador de taxon so visualiza suas fichas
            if (appSession.sicae.user.isCT()) {
                pessoa = Pessoa.get(appSession.sicae.user.sqPessoa)
                papel = getDadosApoioByCodigo('TB_PAPEL_FICHA', 'COORDENADOR_TAXON')
                if (!(papel && pessoa)) {
                    return null
                }
            } else if (appSession.sicae.user.isCE()) {
                pessoa = Pessoa.get(appSession.sicae.user.sqPessoa)
                papel = getDadosApoioByCodigo('TB_PAPEL_FICHA', 'COLABORADOR_EXTERNO')
                if (!(papel && pessoa)) {
                    return null
                }
            }

            listFichasPontoFocal = []
            if (params.sqPontoFocalFiltro) {
                listFichasPontoFocal = FichaPessoa.findAllByPessoaAndPapel(Pessoa.get(params.sqPontoFocalFiltro.toInteger()), dadosApoioService.getByCodigo("TB_PAPEL_FICHA", 'PONTO_FOCAL'))
            }
            fichas = VwFicha.createCriteria().list() {
                order("nmCientifico", "asc") // campo da ficha
                //cache(true)
                //maxResults(10)

                // filtrar pelo ciclo selecionado
                if (params.sqCicloAvaliacao) {
                    //println 'Filtrando pelo sqCicloAvaliacao ' + params.sqCicloAvaliacao
                    eq("sqCicloAvaliacao", params.sqCicloAvaliacao.toInteger())
                }

                // filtrar taxon específico
                if ((params.nivelFiltro == 'ESPECIE' || params.nivelFiltro == 'SUBESPECIE') && params.sqTaxonFiltro) {
                    //println 'Filtrando pelo sqTaxonFiltro ' + params.sqTaxonFiltro
                    eq('sqTaxon', params.sqTaxonFiltro.toInteger())
                    params.sqTaxonFiltro = ''
                    params.nivelFiltro = ''
                    params.noTaxonFiltro = ''
                }
                // filtrar pela unidade e se for Coordenador de Taxon ( CT ) somente as fichas atribuidas a ele
                if (appSession?.sicae?.user.isADM()) {
                    if (params?.sqUnidadeFiltro) {
                        //println 'Filtrando pela sqUnidadeFiltro ' + params.sqUnidadeFiltro
                        eq("sqUnidadeOrg", params.sqUnidadeFiltro.toInteger())
                        params.sqUnidadeFiltro = ''
                    }
                } else {

                    //println 'Filtrando pela sqUnidadeFiltro ' + params.sqUnidadeFiltro
                    // colaborador externo e coordenador de taxon so visualizam as fichas atribuidas a eles no módulo gerenciar função
                    if (appSession.sicae.user.isCT() || appSession.sicae.user.isCE()) {
                        List minhasFichas = FichaPessoa.findAllByPessoaAndPapel(pessoa, papel)
                        and { 'in'("id", minhasFichas?.ficha?.id ?: [(Long) 0]) }
                    } else {
                        // se não for adm ver somente as fichas da unidade
                        eq("sqUnidadeOrg", appSession?.sicae?.user?.sqUnidadeOrg?.toInteger())
                    }
                }
                // filtrar situacao
                if (params.sqSituacaoFiltro) {
                    //println 'Filtrando pela situacao '+params.sqSituacaoFiltro
                    eq('sqSituacaoFicha', params.sqSituacaoFiltro.toInteger())
                    params.sqSituacaoFiltro = ''
                }

                // filtrar pelo nome / parte nome cientifico
                if (params.nmCientificoFiltro) {

                    List aTemp = params.nmCientificoFiltro.trim().split(' ')
                    if (aTemp.size() == 1) {
                        //println 'Filtrando nmCientifico %' + params.nmCientificoFiltro.trim() + '%'
                        ilike('nmCientifico', '%' + params.nmCientificoFiltro.trim() + '%')
                    } else {
                        //println 'Filtrando nmCientifico ' + params.nmCientificoFiltro.trim()
                        ilike('nmCientifico', params.nmCientificoFiltro.trim() + '%')
                    }
                    params.nmCientificoFiltro = ''
                }

                // fichas do ponto focal
                if (listFichasPontoFocal) {
                    //println listFichasPontoFocal.ficha.id
                    and { 'in'("id", listFichasPontoFocal.vwFicha.id) }
                }

                // grupo
                if (params.sqGrupoSalveFiltro) {
                    eq('sqGrupoSalve', params.sqGrupoSalveFiltro.toInteger())
                    params.sqGrupoSalveFiltro = ''
                }

                if (params.sqGrupoFiltro) {
                    eq('sqGrupo', params.sqGrupoFiltro.toInteger())
                    params.sqGrupoFiltro = ''
                }

                // unidade
                if (params.sqUnidadeFiltro) {
                    eq('sqUnidadeOrg', params.sqUnidadeFiltro.toInteger())
                    params.sqUnidadeFiltro = ''
                }

                // situacao da ficha
                if (params.sqSituacaoFiltro) {
                    eq('sqSituacaoFicha', params.sqSituacaoFicha.toInteger())
                    params.sqSituacaoFicha = ''
                }

                // filtrar com / sem pendência
                if (params.inPendenciaFiltro == '1' || params.inPendenciaFiltro == '2') {

                    if (params.inPendenciaFiltro == '1') {
                        // Sem Pendencia
                        eq('nuPendencia', 0)

                    } else if (params.inPendenciaFiltro == '2') {
                        // com Pendencia
                        gt('nuPendencia', 0)
                    }
                    params.inPendenciaFiltro = ''
                }

            }

            /*
            fichas = Ficha.createCriteria().list()
                    {
                        order("nmCientifico", "asc") // campo da ficha
                        cache(true)
                        //firstResult(20)
                        //maxResults(10)

                        // filtrar pelo ciclo selecionado
                        if (params.sqCicloAvaliacao) {
                            //println 'Filtrando pelo sqCicloAvaliacao ' + params.sqCicloAvaliacao
                            eq("cicloAvaliacao", CicloAvaliacao.get(params.sqCicloAvaliacao))
                        }

                        // filtrar taxon específico
                        if (params.nivelFiltro == 'ESPECIE' || params.nivelFiltro == 'SUBESPECIE' && params.sqTaxonFiltro) {
                            // println 'Filtrando pelo sqTaxonFiltro ' + params.sqTaxonFiltro
                            eq('taxon', Taxon.get(params.sqTaxonFiltro.toInteger()))
                            params.sqTaxonFiltro = ''
                            params.nivelFiltro = ''
                            params.noTaxonFiltro = ''
                        }

                        // filtrar pela unidade e se for Coordenador de Taxon ( CT ) somente as fichas atribuidas a ele
                        if (session.sicae.user.isADM()) {
                            if (params?.sqUnidadeFiltro) {
                                eq("unidade", Unidade.get(params.sqUnidadeFiltro))
                                params.sqUnidadeFiltro = ''
                            }
                        } else {

                            //println 'Filtrando pela sqUnidadeFiltro '+params.sqUnidadeFiltro
                            // se não for adm ver somente as fichas da unidade
                            eq("unidade", Unidade.get(session.sicae.user.sqUnidadeOrg))
                            // coordenador de taxon so visualiza as fichas atribuidas a ele no módulo gerenciar função
                            if (session.sicae.user.isCT()) {
                                List minhasFichas = FichaPessoa.findAllByPessoaAndPapel(pessoa, papel)
                                and { 'in'("id", minhasFichas?.ficha?.id ?: [(Long) 0]) }
                            }
                        }
                        // filtrar situacao
                        if (params.sqSituacaoFiltro) {
                            //println 'Filtrando pela situacao '+params.sqSituacaoFiltro
                            eq('situacaoFicha', DadosApoio.get(params.sqSituacaoFiltro))
                            params.sqSituacaoFiltro = ''
                        }

                        // filtrar pelo nome / parte nome cientifico
                        if (params.nmCientificoFiltro) {

                            List aTemp = params.nmCientificoFiltro.trim().split(' ')
                            if (aTemp.size() == 1) {
                                ilike('nmCientifico', '%' + params.nmCientificoFiltro.trim() + '%')
                            } else if (aTemp.size() == 2) {
                                ilike('nmCientifico', params.nmCientificoFiltro.trim() + '%')
                            }
                            params.nmCientificoFiltro = ''
                        }

                        // fichas do ponto focal
                        if (listFichasPontoFocal) {
                            //println listFichasPontoFocal.ficha.id
                            and { 'in'("id", listFichasPontoFocal.ficha.id) }

                        }

                    }
                    */

        }

        // aplicar filtro pelo nivel taxonomico aqui
        /*if ( params.nivelFiltro && params.sqTaxonFiltro)
        {
            fichas.each {
                Map arvoreTaxon = it.taxon.structure
                println arvoreTaxon
                println "-"*100
                println ' '
            }
            params.nivelFiltro=''
            params.sqTaxonFiltro = ''
        }
        */

        //println fichas.size()+' Fichas Encontradas primeira etapa.'
        fichas = applyFilters(fichas, params)
        //println fichas.size() + ' Fichas filtradas segunda etapa'

        // inicializar dados da paginação
        if (params.paginationPageSize) // se passou page size ativar controle de paginacao
        {
            if (!params.paginationCurrentPage) {
                params.paginationCurrentPage = 1
            }
            //println 'Current page = 1'
            params.paginationTotalRecords = fichas.size()

            if (params.paginationTotalRecords.toInteger() > params.paginationPageSize.toInteger()) {
                params.paginationTotalPages = Math.ceil(params.paginationTotalRecords.toInteger() / params.paginationPageSize.toInteger())
            } else {
                if (params.paginationTotalRecords.toInteger() > 0) {
                    params.paginationTotalPages = 1
                } else {
                    params.paginationTotalPages = 0
                }
            }
            //println 'Ler a pagina ' + params.paginationCurrentPage +' com ' + params.paginationPageSize + ' registros'
            // criar array de 50 em 50 e pega a primeira posição
            fichas = fichas.collate(params.paginationPageSize)[(params.paginationCurrentPage.toInteger() - 1)]
            //println 'Pegar a página ' + ( params.paginationCurrentPage.toInteger() ) +' do array'
        }

        /*if( cacheService )
            {
                cacheService.setCache(params,[data:fichas])
                cacheService.setCache(cacheKey,[data:fichas])
                session.cache[cacheKey] = [data:fichas,date:new Date()]
            }
            */

        return fichas
    }

    /**
     * método para filtar as fichas de acordo com o formulário de filtragem de fichas ( template/_filtrosFicha.gsp )
     * @param lista
     * @param params
     * @return
     */
    List applyFilters(List lista, GrailsParameterMap params) {
        getSession()
        if( ! this.appSession?.sicae?.user )
        {
            return []
        }
        //appSession = RequestContextHolder.currentRequestAttributes().getSession()
        boolean temFiltro = false
        // não permitir que outros perfis vejam fichas de outras unidades
        if (!appSession?.sicae?.user?.isADM() && appSession.sicae.user.sqUnidadeOrg ) {
            params.sqUnidadeFiltro = appSession.sicae.user.sqUnidadeOrg.toInteger()
        }
        params.each {
            if (!temFiltro && it?.key?.indexOf('Filtro') > 0 && it.value) {
                temFiltro = true
            }
        }
        if (temFiltro && lista.size() > 0) {
            VwFicha vwFicha
            Oficina oficina
            // oficina
            if (params.sqOficinaFiltro) {
                oficina = Oficina.get(params.sqOficinaFiltro.toInteger())
            }
            lista = lista.findAll {
                vwFicha = null
                boolean result = true
                try {
                    if (it.hasProperty('nmCientifico')) // passou lista com objetos que possuem uma ficha
                    {
                        //if( ! it.ficha )
                        if (!it.hasProperty('ficha')) {
                            vwFicha = VwFicha.get(it.id)
                        } else {
                            vwFicha = it
                        }
                    } else if (it.hasProperty('ficha')) {
                        vwFicha = VwFicha.get(it?.ficha?.id)
                    }
                    if (vwFicha) {
                        // grupo
                        if (params.sqGrupoFiltro) {
                            result = (vwFicha.sqGrupo.toInteger() == params.sqGrupoFiltro.toInteger())
                        }
                        if (params.sqGrupoSalveFiltro) {
                            result = (vwFicha.sqGrupoSalve.toInteger() == params.sqGrupoSalveFiltro.toInteger())
                        }

                        // unidade
                        if (result && params.sqUnidadeFiltro) {
                            result = (vwFicha.sqUnidadeOrg.toInteger() == params.sqUnidadeFiltro.toInteger())
                        }

                        // situacao da ficha
                        if (result && params.sqSituacaoFiltro) {
                            result = (vwFicha.sqSituacaoFicha.toInteger() == params.sqSituacaoFiltro.toInteger())
                        }

                        // nome cientifico
                        if (result && params.nmCientificoFiltro) {
                            result = vwFicha.nmCientifico.toUpperCase().indexOf(params.nmCientificoFiltro.toUpperCase()) > -1
                        }

                        // categoria iucn
                        if (result && params.coCategoriaFiltro) {
                            if (vwFicha.sqCategoriaFinal) {
                                result = vwFicha.categoriaFinal.codigoSistema == params.coCategoriaFiltro
                            } else if (vwFicha.sqCategoriaIucn) {
                                result = vwFicha.sqCategoriaIucn.codigoSistema == params.coCategoriaFiltro
                            } else {
                                result = (vwFicha.codigoCategoriaUltimaAvaliacaoNacionalText == params.coCategoriaFiltro)
                            }
                        }
                        // categoria iucn
                        if (result && params.coCategoriaOficinaFiltro) {
                            if (params.coCategoriaOficinaFiltro.toInteger() == 0) {
                                result = (!vwFicha.sqCategoriaIucn)
                            } else {
                                result = (vwFicha.categoriaIucn?.codigo == params.coCategoriaOficinaFiltro)
                            }
                        }

                        // filtrar com / sem pendência
                        if (result && (params.inPendenciaFiltro == '1' || params.inPendenciaFiltro == '2')) {
                            //int qtdPendencias = FichaPendencia.countByFichaAndStPendente(ficha,'S');
                            if (params.inPendenciaFiltro == '1') {
                                // Sem Pendencia
                                result = (vwFicha.nuPendencia == 0)

                            } else if (params.inPendenciaFiltro == '2') {
                                // com Pendencia
                                result = (vwFicha.nuPendencia > 0)
                            }
                        }

                        // oficina
                        if (result && params.sqOficinaFiltro) {
                            result = (OficinaFicha.findByOficinaAndVwFicha(oficina, vwFicha) != null)
                        }

                        // nivel taxonomico
                        if (result && params.nivelFiltro && params.sqTaxonFiltro) {
                            if (params.nivelFiltro == 'ESPECIE' || params.nivelFiltro == 'SUBESPECIE' && params.sqTaxonFiltro) {
                                result = vwFicha.sqTaxon.toInteger() == params.sqTaxonFiltro.toInteger()
                            } else {
                                Map arvoreTaxon = vwFicha.taxon.structure
                                result = arvoreTaxon['id' + params.nivelFiltro.toLowerCase().capitalize()].toInteger() == params.sqTaxonFiltro.toInteger()
                            }
                        }
                    }
                } catch (Exception e) {
                    result = false
                    println e.getMessage()
                }
                return result // findAll
            }
        }
        return lista
    }

    /**
     * Remover espaços e parágrafos em branco no final do texto
     * @param value
     * @return
     */
    String trim(String value) {
        value = value.toString().trim().replaceAll(/<p>&nbsp;<\/>$/, '')
        if (value == 'null') {
            value = ''
        }
        return value
    }

    /**
     * Este método retorna todas as colaboracoes por ficha em todas as consultas cadasradas
     * @return [description]
     */
    String getColaboradoresHtml(Ficha ficha) {
        println 'metodo fichaService.getColaboradoresHtml() chamado com parametro Ficha nao e mais valido'
        return ''

        //return getColaboradoresHtml(ficha.id )
    }

    String getColaboradoresHtml(Long idFicha) {
        String html = ''
        Map colaboradoresDireta = [:]
        Map colaboradoresAmpla = [:]
        Map colaboradoresRevisao = [:]

        //CicloConsultaFicha.findAllByFicha( ficha ).unique { it.ficha }.each {
        VwFicha vwFicha = VwFicha.get(idFicha)
        CicloConsultaFicha.findAllByVwFicha(vwFicha).each {
            it.colaboracoes.each { colaboracao ->
                if (colaboracao.isDireta()) {
                    if (!colaboradoresDireta[colaboracao.usuario.username]) {
                        colaboradoresDireta[colaboracao.usuario.username] = [idUsuario: colaboracao.usuario.id, noUsuario: colaboracao.usuario.noUsuario, stConcluida: true]
                    }
                    if (colaboracao.situacao.codigoSistema == 'NAO_AVALIADA') {
                        colaboradoresDireta[colaboracao.usuario.username].stConcluida = false
                    }
                } else {
                    if (!colaboradoresAmpla[colaboracao.usuario.username]) {
                        colaboradoresAmpla[colaboracao.usuario.username] = [idUsuario: colaboracao.usuario.id, noUsuario: colaboracao.usuario.noUsuario, stConcluida: true]
                    }
                    if (colaboracao.situacao.codigoSistema == 'NAO_AVALIADA') {
                        colaboradoresAmpla[colaboracao.usuario.username].stConcluida = false
                    }
                }
            }

            // ocorrências
            it.colaboracoesOcorrencia.each { colaboracao ->
                if (colaboracao.isDireta()) {
                    if (!colaboradoresDireta[colaboracao.usuario.username]) {
                        colaboradoresDireta[colaboracao.usuario.username] = [idUsuario: colaboracao.usuario.id, noUsuario: colaboracao.usuario.noUsuario, stConcluida: true]
                    }
                    if (colaboracao.fichaOcorrencia.situacao.codigoSistema == 'NAO_AVALIADA') {
                        colaboradoresDireta[colaboracao.usuario.username].stConcluida = false
                    }
                } else {
                    if (!colaboradoresAmpla[colaboracao.usuario.username]) {
                        colaboradoresAmpla[colaboracao.usuario.username] = [idUsuario: colaboracao.usuario.id, noUsuario: colaboracao.usuario.noUsuario, stConcluida: true]
                    }
                    if (colaboracao.fichaOcorrencia.situacao.codigoSistema == 'NAO_AVALIADA') {
                        colaboradoresAmpla[colaboracao.usuario.username].stConcluida = false
                    }
                }
            }
        }
        List temp = []
        //boolean isLastCicle =ficha.cicloAvaliacao.isLastCicle()
        if (colaboradoresDireta) {
            html += '<h5 class="h-topic"><b>Direta</b></h5>'
            colaboradoresDireta.each {
                temp.push('<a class="' + (it.value.stConcluida ? 'green' : 'red') + '" href="javascript:void(0);" onClick="gerenciarConsulta.showColaboracoes(' + idFicha.toString() + ',' + it.value.idUsuario + ')">' + it.value.noUsuario + '</a>')
            }
            html += temp.join(', ')
        }
        if (colaboradoresAmpla) {
            if (html != '') {
                html += '<hr class="hr-separador"/>'
            }
            html += '<h5 class="h-topic"><b>Ampla</b></h5>'
            temp = []
            colaboradoresAmpla.each {
                temp.push('<a class="' + (it.value.stConcluida ? 'green' : 'red') + '" href="javascript:void(0);" onClick="gerenciarConsulta.showColaboracoes(' + idFicha.toString() + ',' + it.value.idUsuario + ')">' + it.value.noUsuario + '</a>')
            }
            html += temp.join(', ')
        }
        return html
    }

    /**
     * Retornar as contribuições relacionadas ao campo da ficha de todas as consulta da ficha
     * @param ficha
     * @return
     */
    List getColaboracoes(Ficha ficha, String campo = '', String situacao = '',Long sqFichaVersao = null) {
        // DEBUG SSC
        /** /
        if( campo == 'dsNotasTaxonomicas' ) {
            println ' '
            println ' '
            println 'fichaService.getColaboracao'
            println 'campo: ' + campo
            println 'situacao: ' + situacao
            println 'sqFicha: ' + ficha.id
            println 'sqFichaVersao: ' + sqFichaVersao
            println '--------------------------'
        }
         /**/
        List listColaboracoes
        listColaboracoes = FichaColaboracao.createCriteria().list {
            consultaFicha {
                eq('ficha.id', ficha.id)
            }
            if ( situacao != '') {
                // para a situação RESOLVER_OFICINA considerar as colaborações NAO_AVALIADA das revisões pos-oficina como pendencias tambem
                if( situacao == 'RESOLVER_OFICINA' ) {
                    or {
                        and {
                            eq('situacao', getDadosApoioByCodigo('TB_SITUACAO_COLABORACAO', 'NAO_AVALIADA'))
                            consultaFicha {
                                cicloConsulta {
                                    tipoConsulta {
                                        eq('codigoSistema', 'REVISAO_POS_OFICINA')
                                    }
                                }
                            }
                        }
                        eq('situacao', getDadosApoioByCodigo('TB_SITUACAO_COLABORACAO', situacao))
                    }
                } else {
                    eq('situacao', getDadosApoioByCodigo('TB_SITUACAO_COLABORACAO', situacao))
                }
            }
            if (campo != '') {
                eq('noCampoFicha', campo)
            }
        }.findAll {
            if ( ! sqFichaVersao ) {
                // SE NÃO INFORMOU UMA VERSAO, NÃO PODE EXIBIR AS COLABORAÇÕES VERSIONADAS
                return ! it?.consultaFicha?.cicloConsultaFichaVersao
            } else {
                // EXIBIR SOMENTE AS COLABORAÇÕES QUE HOUVERAM NA VERSÃO INFORMADA
                return it.consultaFicha?.cicloConsultaFichaVersao && it.consultaFicha.cicloConsultaFichaVersao.fichaVersao.id.toLong() == sqFichaVersao
            }
        }

        return listColaboracoes
    }

    /**
     * colaborações de pontos de ocorrência recebidos durante as consultas amplas ou diretas
     * @param ficha
     * @return
     */
    List getColaboracoesOcorrencia( Ficha ficha, String situacao) {
        List listColaboracoesOcorrencia
        //situacao = situacao ?: 'PENDENTE_APROVACAO'
        listColaboracoesOcorrencia = FichaOcorrenciaConsulta.createCriteria().list {
            cicloConsultaFicha {
                eq('ficha', ficha)
            }
            if( situacao ) {
                fichaOcorrencia {
                    eq('situacao', dadosApoioService.getByCodigo('TB_SITUACAO_MULTIMIDIA', situacao ) )
                }
            }

            if( situacao ) {
                // para a situação RESOLVER_OFICINA considerar as colaborações NAO_AVALIADA das revisões pos-oficina como pendencias tambem
                if (situacao == 'RESOLVER_OFICINA') {
                    or {
                        and {
                            fichaOcorrencia {
                                eq('situacao', dadosApoioService.getByCodigo('TB_SITUACAO_MULTIMIDIA', situacao ) )
                            }
                            cicloConsultaFicha {
                                cicloConsulta {
                                    tipoConsulta {
                                        eq('codigoSistema', 'REVISAO_POS_OFICINA')
                                    }
                                }
                            }
                        }
                        fichaOcorrencia {
                            eq('situacao', dadosApoioService.getByCodigo('TB_SITUACAO_MULTIMIDIA', situacao ) )
                        }
                    }
                } else {
                    fichaOcorrencia {
                        eq('situacao', dadosApoioService.getByCodigo('TB_SITUACAO_MULTIMIDIA', situacao ) )
                    }
                }
            }
        }
        return listColaboracoesOcorrencia
    }

    /**
     * Listar os nomes de todos os calaboradores das consultas amplas/diretas e dos avaliadores (funcao na oficina)
     * que tiveram alguma colaboração aceita
     * @param sqFicha
     * @return
     */
    /*
    List getNomesColaboradoresValidosAmplaDireta( Long sqFicha ){
        List colaboradores = Ficha.executeQuery("""select distinct new map( a.usuario.noUsuario as noUsuario) from FichaColaboracao a where a.consultaFicha.ficha.id = :sqFicha
            and a.situacao.codigoSistema='ACEITA'
            UNION ALL
            select distinct new map( a.usuario.noUsuario as noUsuario) from FichaConsultaAnexo a where a.consultaFicha.ficha.id = :sqFicha
            and a.situacao.codigoSistema='ACEITA'
            UNION ALL
            select distinct new map( a.usuario.noUsuario as noUsuario) from FichaOcorrenciaConsulta a where a.fichaOcorrencia.ficha.id = :sqFicha
            and a.fichaOcorrencia.inUtilizadoAvaliacao='S'
            """,[sqFicha:sqFicha]).each { row ->
            row.noUsuario = Util.capitalize(row.noUsuario)
        }

        // ler os nomes dos AVALIADORES da ficha
        List avaliadores = Ficha.executeQuery("""Select new map(a.oficinaParticipante.pessoa.noPessoa as noUsuario ) from OficinaFichaParticipante a
            where a.oficinaFicha.ficha.id = :sqFicha
            and a.papel.codigoSistema ='AVALIADOR'
            and a.oficinaParticipante.situacao.codigoSistema <> 'CONVITE_RECUSADO'
            """, [sqFicha:sqFicha]).each { row ->
            row.noUsuario = Util.capitalize(row.noUsuario)
        }
        // excluir os validadores dos colaboradores
        colaboradores -= avaliadores
        return colaboradores.sort{ it.noUsuario }
    }
    */

    /**
     * colaborações de pontos de ocorrência já existentes recebidas durante as consultas amplas ou diretas
     * @param ficha
     * @return
     */
    /*List getColaboracoesOcorrenciaExistente( Ficha ficha) {
        List listColaboracoesOcorrenciaExistentes
        listColaboracoesOcorrenciaExistentes = FichaOcorrenciaValidacao.createCriteria().list {
            eq('ficha', ficha)
        }
        return listColaboracoesOcorrenciaExistentes
    }*/

    /**
     * listar os estados de acordo com os registros de ocorrencias ordenados pela regiao + nome do estado
     * @param sqFicha
     * @return
     */
    List getUfsOcorrenciaPorRegiao( Long sqFicha  ) {
        if( !sqFicha ) {
            return []
        }
        try {
            // ler o contexto de ocorrencia de Estado
            DadosApoio contextoEstado = DadosApoioService.getByCodigo( 'TB_CONTEXTO_OCORRENCIA', 'ESTADO' )
            List lista = DadosApoio.executeQuery( """select new map(
                fo.id as sqFichaOcorrencia
                ,fo.ficha.id as sqFicha
                ,estado.noEstado as noEstado
                ,regiao.noRegiao as noRegiao
                ,case when lower(regiao.noRegiao) = 'norte' then 1 else
                        case when lower(regiao.noRegiao) = 'nordeste' then 2 else
                        case when lower(regiao.noRegiao) = 'centro-oeste' then 3 else
                        case when lower(regiao.noRegiao) = 'sudeste' then 4 else
                        case when lower(regiao.noRegiao) = 'sul' then 5 else 6
                        end end end end end as nuOrdemRegiao )
                from FichaOcorrencia as fo
                inner join fo.estado as estado
                inner join estado.regiao as regiao
                where fo.ficha.id = ${ sqFicha.toString() }
                and  fo.contexto.id = ${ contextoEstado.id.toString() }
                order by nuOrdemRegiao, estado.noEstado
                """ )
            return lista
        } catch( Exception e ) {
            return []
        }
    }
    List getUfsOcorrenciaPorRegiao( Ficha ficha  ) {
        if( ! ficha ) {
            return []
        }
        return getUfsOcorrenciaPorRegiao( ficha.id.toLong() )
    }


    /**
     * retorna os registros da tabela ficha_ocorrencia correspondente ao contexto ESTADO
     * da especie solicitada juntamente com as suas subespecies
     * @param sqFicha
     * @return
     */
    List getUfsOcorrencia( Ficha ficha ) {
        return getUfsOcorrencia( ficha.id )
    }
    List getUfsOcorrencia( Long sqFicha ){
        if( sqFicha ) {

            // ficha excluidas não entram na consulta
            //DadosApoio situacaoExcluida = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA','EXCLUIDA')

            // ler o contexto de ocorrencia de Estado
            DadosApoio contexto = DadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA', 'ESTADO')

            // ler id taxon da ficha
            List listTemp = Ficha.executeQuery( 'select new map( ficha.taxon.id as sqTaxon ) from Ficha ficha where ficha.id=:sqFicha',[ sqFicha:sqFicha ] )

            // ler os estados da espécie e subespecies
            VwFicha vwFicha = VwFicha.get( sqFicha )
            if( listTemp ) {
                // criar lista com ids da ficha e suas subespecies
                /*List fichasIds = Ficha.executeQuery("select new map( ficha.id as id ) from Ficha ficha " +
                        " inner join ficha.taxon taxon" +
                        " where ficha.id= :id or taxon.pai.id = :sqTaxon",
                        [id: vwFicha.id, sqTaxon: vwFicha.sqTaxon.toLong()])?.id
*/
                return FichaOcorrencia.executeQuery("""select new map( fo as fichaOcorrencia
                    , fo.id as sqFichaOcorrencia
                    , fo.ficha.id as sqFicha
                    , fo.estado as estado
                     )
                    from FichaOcorrencia fo
                    inner join fo.ficha ficha
                    where ( ficha.id = :sqFicha or ( ficha.taxon.pai.id = :sqTaxon and ficha.cicloAvaliacao.id = :sqCicloAvaliacao ) )
                    and fo.contexto = :contexto
                    order by fo.estado.noEstado
                    """, [sqFicha: sqFicha, sqTaxon:listTemp.first().sqTaxon.toLong()
                          , contexto: contexto
                          , sqCicloAvaliacao: vwFicha.sqCicloAvaliacao.toLong() ] ).unique { it.estado }
            }
        }
        return []
    }

    /**
     * retornar a lista de subespecies da ficha
     * @param sqFicha
     * @return List
     */
    List getSubespeciesFicha( Ficha ficha ) {
        List lista = []
        if( ficha && ficha.taxon.nivelTaxonomico.coNivelTaxonomico == 'ESPECIE') {
            Map pars =  [sqTaxonFicha: ficha.taxon.id, sqCicloAvaliacao: ficha.cicloAvaliacao.id]
            Ficha.executeQuery("""select new map(f.id as sqFicha) from Ficha f where f.taxon.pai.id = :sqTaxonFicha
                and f.cicloAvaliacao.id = :sqCicloAvaliacao""",pars).each {
                lista.push([sqFicha: it.sqFicha])
            }
        }
        return lista
    }
    List getSubespeciesFicha( Long sqFicha ) {
        return getSubespeciesFicha( Ficha.get( sqFicha ) )
    }

    //NOVO-REGISTRO
    /**
     * ler as ufs da nova tabela ficha_uf com as referencias no formato json
     * @param sqFicha
     * @return
     */
    List getUfsFicha( Long sqFicha ){
        if( ! sqFicha ){
            return []
        }
        String cmdSql = """select fn.sq_estado,
                   fn.no_contexto,
                   fn.sq_ficha_uf,
                   fn.no_estado,
                   fn.co_nivel_taxonomico,
                   fn.nu_ordem_regiao,
                   fn.st_utilizado_avalicao,
                   fn.js_ref_bibs::text as json_ref_bibs
                   from salve.fn_ficha_estados( :sqFicha ) as fn
                   order by fn.no_contexto, fn.no_estado;"""
        return executeSql(cmdSql, [sqFicha:sqFicha])
    }

    // NOVO-REGISTRO
    /**
     * ler as ufs da nova tabela registro_uf com as referencias no formato json
     * @param sqFicha
     * @return
     */
    List getUfsRegistro( Long sqFicha ){
        return getUfsFicha( sqFicha).findAll{ it.no_contexto == 'ocorrencia' }
    }
    /**
     * retornar os nomes dos estados da ficha, separados por virgula
     * @param sqFicha
     * @return
     */
    String getUfsText( Long sqFicha ) {
       return getUfsFicha( sqFicha ).unique{ it.no_estado }.sort {it.no_estado }.no_estado.join(', ')
    }
    String getUfsText( Ficha ficha ) {
        return getUfsText( ficha?.id )
    }
    /**
     * retornar os nomes dos estados da ficha, separados por virgula e ordenado por região (NORTE, NORDESTE ...)
     * @param sqFicha
     * @return
     */
    String getUfsNameByRegiaoText( Long sqFicha ) {
       String result = getUfsFicha( sqFicha ).unique{ it.no_estado }.sort {it.nu_ordem_regiao+it.no_estado }.no_estado.join(', ')
        return result ? result : ''
    }
    String getUfsNameByRegiaoText( Ficha ficha ) {
        return getUfsNameByRegiaoText( ficha.id.toLong() )
    }

    /**
    @deprecated
    */
    String getUfsNameText( Long sqFicha ) {
        if ( sqFicha ) {
            List lista = getUfsOcorrencia( sqFicha )
            return lista?.estado?.noEstado?.join(', ')// + ( lista ? '.' : '')
        }
        return ''
    }

    /**
     @deprecated
     */
    String getUfsNameText(Ficha ficha) {
        if (ficha) {
            List lista = getUfsOcorrencia( ficha )
            return lista?.estado?.noEstado?.join(', ') // + ( lista ? '.' : '')
        }
        return ''
    }

    //NOVO-REGISTRO
    /**
     * ler as ufs da nova tabela ficha_uf com as referencias no formato json
     * @param sqFicha
     * @return
     */
    List getBiomasFicha( Long sqFicha ){
        if( ! sqFicha ) {
            return []
        }
        String cmdSql = """select fn.no_contexto,
                       fn.sq_ficha_bioma,
                       fn.no_bioma,
                       fn.co_nivel_taxonomico,
                       fn.js_ref_bibs::text as json_ref_bibs
                from salve.fn_ficha_biomas(:sqFicha) as fn
                order by fn.no_contexto, fn.no_bioma;"""
        return executeSql(cmdSql, [sqFicha:sqFicha])
    }

    //NOVO-REGISTRO
    List getBiomasRegistro( Long sqFicha ){
        return getBiomasFicha( sqFicha ).findAll{it.no_contexto == 'ocorrencia' }
    }

    /**
     * retornar os biomas da ficha separados por virgula
     **/
    String getBiomasText(Long sqFicha ) {
        String result = getBiomasFicha(sqFicha).unique { it.no_bioma }.sort { it.no_bioma }.no_bioma.join(', ')
        return result ? result : ''
    }
    String getBiomasText(Ficha ficha) {
        return getBiomasText( ficha?.id )
    }

    /**
     * retorna os registros da tabela ficha_ocorrencia correspondente ao contexto BIOMA
     * da especie solicitada juntamente com as suas subespecies
     * @param sqFicha
     * @return
     * @deprecated
     */
    List getBiomasOcorrencia( Ficha ficha ) {
        return getBiomasOcorrencia( ficha.id )
    }
    List getBiomasOcorrencia( Long sqFicha ) {
        if( sqFicha ) {

            // fichas excluídas não entram no resultado
            // DadosApoio situacaoExcluida = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA','EXCLUIDA')

            // ler o contexto de ocorrencia de Estado
            DadosApoio contexto = DadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA', 'BIOMA')

            // ler id taxon da ficha
            List listTemp = Ficha.executeQuery( 'select new map( ficha.taxon.id as sqTaxon ) from Ficha ficha where ficha.id=:sqFicha',[ sqFicha:sqFicha ] )

            // ler os estados da espécie e subespecies
            VwFicha vwFicha = VwFicha.get( sqFicha )
            if( listTemp ) {
                // criar lista com ids da ficha e suas subespecies
                /*List fichasIds = Ficha.executeQuery("select new map( ficha.id as id ) from Ficha ficha " +
                        " inner join ficha.taxon taxon" +
                        " where ficha.id= :id or taxon.pai.id = :sqTaxon",
                        [id: vwFicha.id, sqTaxon: vwFicha.sqTaxon.toLong()])?.id
                */
                return FichaOcorrencia.executeQuery("""select new map( fo as fichaOcorrencia, fo.id as sqFichaOcorrencia
                    , fo.ficha.id as sqFicha
                    , fo.bioma as bioma
                     )
                    from FichaOcorrencia fo
                    inner join fo.ficha ficha
                    where ( ficha.id = :sqFicha or ( ficha.taxon.pai.id = :sqTaxon and ficha.cicloAvaliacao.id = :sqCicloAvaliacao ) )
                    and fo.contexto = :contexto
                    order by fo.bioma.descricao
                    """, [sqFicha: sqFicha, sqTaxon: listTemp.first().sqTaxon.toLong()
                          , contexto: contexto
                          , sqCicloAvaliacao: vwFicha.sqCicloAvaliacao.toLong() ]).unique { it.bioma }
            }
        }
        return []
    }


    /**
     * Listar as bacias hidrograficas da ficha e das subespecies da ficha
     * @param sqFicha ou Ficha
     * @return
     * @deprecated
     */
    List getBaciasFicha( Ficha ficha ) {
        return getBaciasFicha( ficha.id.toLong() )
    }
    List getBaciasFicha( Long sqFicha ) {
        List lista = []
        if (sqFicha) {
            List idsFichas = getSubespeciesFicha(sqFicha)
            idsFichas.push(['sqFicha': sqFicha])

            lista = FichaBacia.executeQuery("""select new map( a.bacia.id as sq_bacia ,a.bacia.descricao as no_bacia
            ,vb.codigo as cd_bacia
            ,vb.codigoSistema as cd_sistema
            ,vb.trilha as tx_trilha
            ,a.ficha.taxon.nivelTaxonomico.coNivelTaxonomico as co_nivel_taxonomico
            ,vb.ordem as de_bacia_ordem
            ) from FichaBacia a, VwBacia vb
            where a.ficha.id in (:idsFichas)
              and a.bacia.id = vb.id
            """, [idsFichas: idsFichas.sqFicha])
            lista = lista.unique { it.no_bacia }.sort { it.de_bacia_ordem }
        }
        return lista
    }

    /**
     * retorna os registros da tabela ficha_ocorrencia correspondente ao contexto BACIA HIDROGRAFICA
     * da especie solicitada juntamente com as suas subespecies
     * @param sqFicha
     * @return
     * @deprecated
     */
    List getBaciasOcorrencia( Ficha ficha ) {
        return getBaciasOcorrencia( ficha.id )
    }
    List getBaciasOcorrencia( Long sqFicha ) {
        if( sqFicha ) {
            // ler id taxon da ficha
            List listTemp = Ficha.executeQuery( 'select new map( ficha.taxon.id as sqTaxon ) from Ficha ficha where ficha.id=:sqFicha',[ sqFicha:sqFicha ] )

            // ler as bacias da espécie e subespecies
            VwFicha vwFicha = VwFicha.get( sqFicha )

            if( listTemp ) {
                DadosApoio contextoBacia = DadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA', "BACIA_HIDROGRAFICA")

                return FichaOcorrencia.executeQuery("""select new map(fo as fichaOcorrencia, fo.id as sqFichaOcorrencia, fo.ficha.id as sqFicha
                    , fo.baciaHidrografica as baciaHidrografica
                     )
                    from FichaOcorrencia fo
                    inner join fo.ficha ficha
                    where ( ficha.id = :sqFicha or ( ficha.taxon.pai.id = :sqTaxon and ficha.cicloAvaliacao.id = :sqCicloAvaliacao ) )
                    and fo.baciaHidrografica is not null
                    and fo.contexto.id = :sqContexto
                    order by fo.baciaHidrografica.ordem, fo.baciaHidrografica.descricao
                    """, [sqFicha : sqFicha,
                          sqTaxon : listTemp.first().sqTaxon.toLong(),
                          sqContexto : contextoBacia.id,
                          sqCicloAvaliacao : vwFicha.sqCicloAvaliacao.toLong()]).unique { it.baciaHidrografica }
            }
        }
        return []
    }

    //NOVO-REGISTRO
    /**
     * ler todas as bacias hidrograficas da ficha com as referencias no formato json
     * @param sqFicha
     * @return
     */
    List getBaciasHidrograficasFicha( Long sqFicha ){
        String cmdSql = """select fn.no_contexto,
                       fn.sq_ficha_bacia,
                       fn.no_bacia,
                       fn.cd_bacia,
                       fn.cd_sistema,
                       fn.tx_trilha,
                       fn.co_nivel_taxonomico,
                       fn.de_bacia_ordem,
                       fn.js_ref_bibs::text as json_ref_bibs
                from salve.fn_ficha_bacias(:sqFicha) as fn
                order by fn.no_contexto, fn.de_bacia_ordem;"""
        return executeSql(cmdSql, [sqFicha:sqFicha])

    }
    // NOVO-REGISTRO
    /**
     * ler apenas as bacias hidrograficas da ficha com as referencias no formato json
     * que possuem um registro de ocorrencia
     * @param sqFicha
     * @return
     */
    List getBaciasRegistro( Long sqFicha ){
        return getBaciasHidrograficasFicha(sqFicha).findAll{it.no_contexto == 'ocorrencia'}
    }

    /**
    * retornar as bacias hidrográficas separadas por virgula
    */
    String getBaciasText(Long sqFicha ) {
       String result = getBaciasHidrograficasFicha( sqFicha ).unique { it.no_bacia }.sort { it?.de_bacia_ordem }.no_bacia.join(', ')
       return result ? result  : ''
    }
    String getBaciasText(Ficha ficha) {
       return getBaciasText( ficha?.id )
    }

    //*******************************************************************************************

    /**
     * retorna os registros da tabela ficha_ocorrencia correspondente ao contexto UC
     * da especie solicitada juntamente com as suas subespecies
     * @param sqFicha
     * @return
     */
    List getUcsOcorrencia( Ficha ficha ) {
        return getUcsOcorrencia( ficha.id )
    }
    List getUcsOcorrencia( Long sqFicha ){
        // TODO - avalidar se tem como fazer com hql esta consulta

        try {
            DadosApoio contexto = DadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA', "REGISTRO_OCORRENCIA")

            // ler id taxon da ficha
            List listTemp = Ficha.executeQuery('select new map( ficha.taxon.id as sqTaxon ) from Ficha ficha where ficha.id=:sqFicha', [sqFicha: sqFicha])

            VwFicha vwFicha = VwFicha.get( sqFicha );
            if (listTemp) {
                return FichaOcorrencia.executeQuery("""select new map(
              fo as fichaOcorrencia
            , fo.id as sqFichaOcorrencia
            , fo.ficha.id as sqFicha
            , case when fo.sqUcFederal is not null then fo.sqUcFederal else
                   case when fo.sqUcEstadual is not null then fo.sqUcEstadual else fo.sqRppn end end as sqUc
            , ( select uc from Uc uc where uc.id =
                case when fo.sqUcFederal is not null then fo.sqUcFederal else
                   case when fo.sqUcEstadual is not null then fo.sqUcEstadual else fo.sqRppn end end
                and uc.esferaAdm.codigoSistema = case when fo.sqUcFederal is not null then 'FEDERAL' else
                   case when fo.sqUcEstadual is not null then 'ESTADUAL' else 'RPPN' end end
            ) as uc )
            from FichaOcorrencia fo
            inner join fo.ficha ficha
            where ( ficha.id = :sqFicha or ( ficha.taxon.pai.id = :sqTaxon and ficha.cicloAvaliacao.id = :sqCicloAvaliacao ) )
            and ( fo.inUtilizadoAvaliacao ='S' or fo.inUtilizadoAvaliacao is null)
            and fo.contexto = :contexto
            and (fo.sqUcFederal is not null or fo.sqUcEstadual is not null or fo.sqRppn is not null )
            """, [sqFicha: sqFicha, sqTaxon: listTemp.first().sqTaxon.toLong(), contexto: contexto, sqCicloAvaliacao: vwFicha.sqCicloAvaliacao.toLong()]).unique { it?.uc }.sort { it?.uc?.sgUnidade }

            }
        } catch( Exception e ) {
            println 'Erro getUcsOcorrencia sqFicha '+ sqFicha
            println e.getMessage()
        }
        return []
    }



/**
 * Método para retornar um Map com as UCs Federais, Estaduais e RPPNs cadastrdas nos registros de ocorrencias
 * do salve e portalbio e as referências bibliográficas/citação
 * Se o parametro withCitacao for true retorna a ref bib no formato de citação. Ex: Willmott, 2003 senão
 * retorna a referência bibliográfica no formato texto: Ex: Willmott, K.R., 2003. The Genus Adelpha: Its Systematics, Biology and Biogeography (Lepidoptera: Nymphalidae: Limenitidini), p.240. SCIENTIFIC PUBLISHERS (Id Salve:23809)
 *
 * @param sqFicha
 * @param withCitacao
 * @return
 */
    Map getUcsFromOcorrencia( Long sqFicha, Boolean withCitacao = true ) {
        Map resultado = [ : ]
        DadosApoio contexto = DadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA', "REGISTRO_OCORRENCIA")
        // ler id taxon e id ciclo da ficha
        Long sqTaxon
        Long sqCicloAvaliacao
        try {
            executeSql("select sq_ciclo_avaliacao, sq_taxon from salve.ficha where sq_ficha = " + sqFicha.toString()).each { row ->
                sqTaxon = row.sq_taxon
                sqCicloAvaliacao = row.sq_ciclo_avaliacao
            }

            String cmdSql = """WITH CTE AS (
                select uc_federal.sg_unidade_org as no_uc_federal
                     , uc_estadual.nome_uc1      as no_uc_estadual
                     , rppn.nome                 as no_rppn
                     , refs.sqs_ref_bibs
                     , concat( uf.no_estado,' - ',uf.sg_estado) as no_estado
                     , salve.snd( fo.in_presenca_atual) as de_presenca_atual
                from salve.ficha_ocorrencia fo
                         inner join salve.ficha on ficha.sq_ficha = fo.sq_ficha
                         inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                         left outer join salve.unidade_conservacao as uc_federal on uc_federal.sq_pessoa = fo.sq_uc_federal
                         left outer join salve.unidade_conservacao as uc_estadual on uc_estadual.sq_pessoa = fo.sq_uc_estadual
                         left outer join salve.unidade_conservacao as rppn on rppn.sq_pessoa = fo.sq_rppn
                         left outer join salve.vw_estado as uf on uf.sq_estado = fo.sq_estado
                         left join lateral (
                    select array_to_string(array_agg(x.sq_ficha_ref_bib), ',') AS sqs_ref_bibs
                    from salve.ficha_ref_bib x
                    where x.sq_ficha = ficha.sq_ficha
                      and x.no_tabela = 'ficha_ocorrencia'
                      and x.no_coluna = 'sq_ficha_ocorrencia'
                      and x.sq_registro = fo.sq_ficha_ocorrencia
                    ) as refs on true
                where (ficha.sq_ficha = ${ sqFicha.toString() } or (taxon.sq_taxon_pai = ${ sqTaxon.toString() }
                    and ficha.sq_ciclo_avaliacao = ${ sqCicloAvaliacao.toString() } ) )
                  and (fo.in_utilizado_avaliacao = 'S')
                  and fo.sq_contexto = ${ contexto.id.toString() }
                  and (fo.sq_uc_federal is not null or fo.sq_uc_estadual is not null or fo.sq_rppn is not null)

                UNION ALL

                select uc_federal.sg_unidade_org as no_uc_federal
                     , uc_estadual.nome_uc1      as no_uc_estadual
                     , rppn.nome                 as no_rppn
                     , refs.sqs_ref_bibs
                     , concat( uf.no_estado,' - ',uf.sg_estado) as no_estado
                     , null as de_presenca_atual
                from salve.ficha_ocorrencia_portalbio fo
                         inner join salve.ficha on ficha.sq_ficha = fo.sq_ficha
                         inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                         left outer join salve.unidade_conservacao as uc_federal on uc_federal.sq_pessoa = fo.sq_uc_federal
                         left outer join salve.unidade_conservacao as uc_estadual on uc_estadual.sq_pessoa = fo.sq_uc_estadual
                         left outer join salve.unidade_conservacao as rppn on rppn.sq_pessoa = fo.sq_rppn
                         left outer join salve.vw_estado as uf on uf.sq_estado = fo.sq_estado
                         left join lateral (
                    select array_to_string(array_agg(x.sq_ficha_ref_bib), ',') AS sqs_ref_bibs
                    from salve.ficha_ref_bib x
                    where x.sq_ficha = ficha.sq_ficha
                      and x.no_tabela = 'ficha_ocorrencia_portalbio'
                      and x.no_coluna = 'sq_ficha_ocorrencia_portalbio'
                      and x.sq_registro = fo.sq_ficha_ocorrencia_portalbio
                    ) as refs on true
                where (ficha.sq_ficha = ${ sqFicha } or (taxon.sq_taxon_pai = ${ sqTaxon.toString() }
                  and ficha.sq_ciclo_avaliacao = ${ sqCicloAvaliacao.toString() }))
                  and fo.in_utilizado_avaliacao = 'S'
                  and (fo.sq_uc_federal is not null or fo.sq_uc_estadual is not null or fo.sq_rppn is not null)
            )
            select distinct no_uc_federal
                , no_uc_estadual
                , no_rppn
                , sqs_ref_bibs
                , no_estado
                , de_presenca_atual
                , public.fn_remove_acentuacao(lower( concat( no_uc_federal, no_uc_estadual, no_rppn) )) as nomes
             from cte
            order by nomes
            """
/** /
 println ' '
 println 'UCS'
 println cmdSql
 /**/

            List listUcs = executeSql(cmdSql)
            listUcs.each { row ->
                List listRefs = [ ]
                if (row.sqs_ref_bibs) {
                    row.sqs_ref_bibs.split(',').collect { it.toLong() }.each { id ->
                        FichaRefBib frb = FichaRefBib.get(id)
                        if (withCitacao) {
                            if (!frb?.publicacao) {
                                listRefs.push(frb.referenciaHtml)
                            } else {
                                listRefs.push(frb?.publicacao?.citacao ?: '')
                            }
                        } else {
                            listRefs.push(frb.referenciaHtml)
                        }
                    }
                }
                String key
                if (row.no_uc_federal) {
                    key = row.no_uc_federal.toUpperCase()
                    if (!resultado[key]) {
                        resultado[key] = [ noEstado: row.no_estado, dePresencaAtual: row.de_presenca_atual, refs: [ ] ]
                    } else {
                        resultado[key].noEstado = (row.no_estado && row.no_estado.trim() != '-') ? row.no_estado : resultado[key].noEstado
                        resultado[key].dePresencaAtual = row.de_presenca_atual ?: resultado[key].dePresencaAtual
                    }
                    listRefs.each { refHtml ->
                        if (!resultado[key].refs.contains(refHtml)) {
                            resultado[key].refs.push(refHtml)
                        }
                    }
                }
                if (row.no_uc_estadual) {
                    key = row.no_uc_estadual.toUpperCase()
                    if (!resultado[key]) {
                        resultado[key] = [ noEstado: row.no_estado, dePresencaAtual: row.de_presenca_atual, refs: [ ] ]
                    } else {
                        resultado[key].noEstado = (row.no_estado && row.no_estado.trim() != '-') ? row.no_estado : resultado[key].noEstado
                        resultado[key].dePresencaAtual = row.de_presenca_atual ?: resultado[key].dePresencaAtual
                    }
                    listRefs.each { refHtml ->
                        if (!resultado[key].refs.contains(refHtml)) {
                            resultado[key].refs.push(refHtml)
                        }
                    }
                }
                if (row.no_rppn) {
                    key = row.no_rppn.toUpperCase()
                    if (!resultado[key]) {
                        resultado[key] = [ noEstado: row.no_estado, dePresencaAtual: row.de_presenca_atual, refs: [ ] ]
                    } else {
                        resultado[key].noEstado = (row.no_estado && row.no_estado.trim() != '-') ? row.no_estado : resultado[key].noEstado
                        resultado[key].dePresencaAtual = row.de_presenca_atual ?: resultado[key].dePresencaAtual
                    }
                    listRefs.each { refHtml ->
                        if (!resultado[key].refs.contains(refHtml)) {
                            resultado[key].refs.push(refHtml)
                        }
                    }
                }
            }
        } catch (Exception e) {}
        return resultado
    }

    // NOVO-REGISTRO - ler as ucs da ficha utilizando a nova estrutura de registro
    List getUcsRegistro( Long sqFicha ) {
        if (!sqFicha) {
            return []
        }
        List ucs = executeSql("select * from salve.fn_ficha_ucs(:sqFicha) order by nu_ordem, public.fn_remove_acentuacao(upper( no_uc ) );",[sqFicha:sqFicha] )
        List resultado = []
        // remover linhas duplicadas a concatenar as ref bibs em uma só uc
        ucs.each{ row ->
            Map rowExiste = resultado.find{ it.no_uc == row.no_uc }
            if( rowExiste ){
                def dadosJsonRepetido = JSON.parse(row.json_ref_bib)
                def dadosJsonExiste = JSON.parse(rowExiste.json_ref_bib)
                // se existir com presença atual, vale a presenca atual ou se estiver vazio e existir uma como desconhecido vale a desconhecido
                if( row.in_presenca_atual =='S'){
                    rowExiste.in_presenca_atual = row.in_presenca_atual
                    rowExiste.de_presenca_atual = row.de_presenca_atual
                }else if( row.in_presenca_atual =='D' && ! rowExiste.in_presenca_atual ){
                    rowExiste.in_presenca_atual = row.in_presenca_atual
                    rowExiste.de_presenca_atual = row.de_presenca_atual
                }
                if( row.no_estado != rowExiste.no_estado ) {
                    rowExiste.no_estado += ', ' + row.no_estado
                    rowExiste.sg_estado += ', ' + row.sg_estado
                }
                dadosJsonRepetido.each { key, value ->
                    if(! dadosJsonExiste.find {
                        it.value.no_autor == value.no_autor && it.value.de_titulo == value.de_titulo
                    }) {
                        if( value.no_autor ) {
                            dadosJsonExiste[key]=value
                            rowExiste.json_ref_bib = dadosJsonExiste.toString()
                        }
                    }
                }
            } else {
                resultado.push( row )
            }
        }
        return resultado
    }


    List getAreasRelevantesList(Ficha ficha) {
        return FichaAreaRelevancia.findAllByFicha(ficha)
    }

    String getAreasRelevantesText(Ficha ficha) {
        String html = '''<table class="table">
                <thead>
                    <tr>
                        <th>Tipo</th>
                        <th>Local</th>
                        <th>Estado</th>
                        <th>Municipio</th>
                        <th>Ref. Bibliográfica</th>
                    </tr>
                </thead>
                <tbody>
            '''
        FichaAreaRelevancia.findAllByFicha(ficha).each {
            html += '<tr>' +
                '<td>' + trim(it.tipoRelevancia.descricao) + '</td>' +
                '<td>' + trim(it.txLocal) + '</td>' +
                '<td>' + trim(it.estado.noEstado) + '</td>' +
                '<td>' + trim(it.municipiosHtml) + '</td>' +
                '<td>' + trim(it.refBibHtml) + '</td>' +
                '</tr>'

        }
        html += '</tbody></table>'
        return html
    }

    List getFichaNomesComuns(VwFicha vwFicha) {
        return getFichaNomesComuns(vwFicha?.id) // manter compatibilidade
    }

    List getFichaNomesComuns(Ficha ficha) {
        return getFichaNomesComuns(ficha?.id) // manter compatibilidade
    }

    List getFichaNomesComuns(Long idFicha = 0l) {
        VwFicha vwFicha
        if (idFicha) {
            vwFicha = VwFicha.get(idFicha)
            return FichaNomeComum.findAllByVwFicha(vwFicha)
        }
        return []
    }

    List getFichaSinonimias(Ficha ficha) {
        return FichaSinonimia.findAllByFicha(ficha)
    }

    String getFichaNomesComunsText(VwFicha vwFicha) // manter compatibilidade
    {
        getFichaNomesComunsText(vwFicha?.id) // manter compatibilidade
    }

    String getFichaNomesComunsText(Ficha ficha) {
        getFichaNomesComunsText(ficha?.id) // manter compatibilidade
    }

    String getFichaNomesComunsText(Long idFicha = 0l) {
        List data = []
        String lingua = ''
        getFichaNomesComuns(idFicha).each {
            lingua = (it.deRegiaoLingua ?: '')
            if (lingua && lingua.indexOf('(') == -1) {
                lingua = '(' + lingua + ')'
            }
            data.push(it.noComum +' '+ lingua )
        }
        return data.join(', ') //+ (data ? '.' : '')
    }

    String getFichaSinonimiasHtml(Ficha ficha) {
        List data = []
        String autor = ''
        getFichaSinonimias(ficha).each {
            autor = it.noAutor ?: ''
            /*if (autor.indexOf('(') == -1) {
                autor = '(' + autor + ')'
                autor = autor.replaceAll(/\)\)/, ')').replaceAll(/\(\(/, '(')
            }
            */
            data.push('<span><i>' + it.noSinonimia + '</i>  ' + autor + '</span>')
        }
        return data.join(', ') //+ (data ? '.' : '')
    }

    String getFichaDistribuicaoMapaPrincipal(Ficha ficha, String language = 'pt') {
        String localArquivo = ''
        File file
        DadosApoio contexto = getDadosApoioByCodigo('TB_CONTEXTO_ANEXO', 'MAPA_DISTRIBUICAO');
        ApplicationContext ctx = Holders.grailsApplication.mainContext
        FichaAnexo.findByFichaAndContextoAndInPrincipalAndDeTipoConteudoIlike(ficha, contexto, 'S','%image%').each {
            file = new File( it.deLocalArquivo.toString())
            if( ! file.exists() ){
                file = new File(ctx.grailsApplication?.config?.anexos?.dir +  it.deLocalArquivo.toString())
            }
        }
        if ( !file || !file.exists() ) {
            // INTEGRACAO SIS/IUCN
            if( language == 'en' ) {
                file = new File(ctx.grailsApplication?.config?.arquivos?.dir + 'sem-mapa-distribuicao-en.jpeg')
            } else {
                file = new File(ctx.grailsApplication?.config?.arquivos?.dir + 'sem-mapa-distribuicao.jpeg')
            }
            if (!file.exists()) {
                if( language=='en'){
                    file = new File(ctx.grailsApplication?.config?.arquivos?.dir + 'no-image-en.jpeg')
                } else {
                    file = new File(ctx.grailsApplication?.config?.arquivos?.dir + 'no-image.jpeg')
                }
            }
        }
        if ( file.exists() ) {
            localArquivo = file.getAbsolutePath()
        }
        return localArquivo
    }

    String getFichaImagemAmeaca(Ficha ficha) {
        String localArquivo = ''
        String strTemp = ''
        File file
        DadosApoio contexto = getDadosApoioByCodigo('TB_CONTEXTO_ANEXO', 'MAPA_AMEACA');
        FichaAnexo.findAllByFichaAndContexto(ficha, contexto,[sort: "dtAnexo",order:'desc']).each {
            if ( it.deTipoConteudo =~ /image/ && !localArquivo ) {
                strTemp = it.deLocalArquivo.toString()
                file = new File(strTemp)
                if (!file.exists()) {
                    ApplicationContext ctx = Holders.grailsApplication.mainContext
                    file = new File(ctx.grailsApplication?.config?.anexos?.dir + strTemp)
                    ctx = null
                }
                if (it.deLocalArquivo && file && file.exists()) {
                    localArquivo = file.getAbsolutePath()
                }
            }
        }
        if (localArquivo) {
            return localArquivo
        } else {
            return ''
        }
    }

    /**
     * ler a mais recente imagem principal do taxon independente do ciclo de avaliação
     * @param pFicha
     * @return
     */
    Map getFichaImagemPrincipal( Ficha pFicha = null, boolean publicada = false ) {
        Map data = [:]
        String localArquivo = ''
        String strTemp = ''
        File file
        if( ! dadosApoioService ) {
            dadosApoioService = new DadosApoioService();
        }
        FichaMultimidia.createCriteria().list {
            eq('inPrincipal',true)
            eq('situacao', dadosApoioService.getByCodigo('TB_SITUACAO_MULTIMIDIA','APROVADA') )
            ficha {
                eq('taxon', pFicha.taxon)
                if( publicada ) {
                    eq('situacaoFicha', dadosApoioService.getByCodigo('TB_SITUACAO_FICHA','PUBLICADA') )
                }
            }
            or {
                ilike('noArquivo', '%.jpeg')
                ilike('noArquivo', '%.jpg')
                ilike('noArquivo', '%.png')
                ilike('noArquivo', '%.bmp')
            }
            order('dtElaboracao', 'desc')
            maxResults(1)
        }.each {

            //FichaMultimidia.findAllByFichaAndInPrincipal(pFicha, true).each {
            ApplicationContext ctx = Holders.grailsApplication.mainContext
            file = new File(ctx.grailsApplication?.config?.multimidia?.dir + it.noArquivoDisco.toString())
            ctx = null
            if (file && file.exists()) {
                data.localArquivo = file.getAbsolutePath()
                data.legenda = it.deLegenda
                data.autor = it.noAutor
                data.id = it.id
            }
        }
        return data
    }

    List getFichaImages(Ficha ficha = null, String contexto = '') {
        List listArquivos = []
        String strTemp = ''
        File file
        DadosApoio apoioContexto = getDadosApoioByCodigo('TB_CONTEXTO_ANEXO', contexto)
        FichaAnexo.findAllByFichaAndContexto(ficha, apoioContexto).each {
            strTemp = it.deLocalArquivo.toString()
            if( strTemp =~ /(?i)\.(jpe?g|bmp|png|gif|tiff)$/  ) {
                file = new File(strTemp)
                if ( ! file.exists()) {
                    ApplicationContext ctx = Holders.grailsApplication.mainContext
                    file = new File( ctx.grailsApplication?.config?.anexos?.dir + strTemp)
                }
                if ( it.deLocalArquivo && file.exists()) {
                    listArquivos.push([localArquivo: file.getAbsolutePath(), legenda: it.deLegenda])
                }
            }
        }
        return listArquivos
    }



    /**
     * retornar os dados da ultima avaliacao nacional da tabela taxon_historico_avaliacao
     * @param idTaxon
     * @return
     */
    Map getUltimaAvaliacaoNacionalTaxon( Long sqTaxon = 0 ) {
        Map dados = [id: 0, sqCategoriaIucn: '', coCategoriaIucn: '', deCategoriaIucn: '', nuAnoAvaliacao: '', txJustificativaAvaliacao: '', deCriterioAvaliacaoIucn: '', stPossivelmenteExtinta: '', tipoAvaliacao: '', tipoAbrangencia: '']
        if ( sqTaxon ) {
            DadosApoio tipoNacionalBrasil = DadosApoio.findByCodigoSistema('NACIONAL_BRASIL')
            // primeiro ler a última avaliação nacional gravada no histórico
            TaxonHistoricoAvaliacao tha = TaxonHistoricoAvaliacao.createCriteria().get {
                eq('taxon.id', sqTaxon)
                eq('tipoAvaliacao', tipoNacionalBrasil)
                order('nuAnoAvaliacao', 'desc')
                order('id', 'desc')
                maxResults(1)
            }
            if (tha) {
                dados.id = tha.id ?: 0
                dados.sqCategoriaIucn = tha.categoriaIucn.id ?: ''
                dados.coCategoriaIucn = tha.categoriaIucn.codigo ?: ''
                dados.deCategoriaIucn = tha.categoriaIucn.descricaoCompleta ?: ''
                dados.nuAnoAvaliacao = tha.nuAnoAvaliacao ?: ''
                dados.txJustificativaAvaliacao = tha.txJustificativaAvaliacao ?: ''
                dados.deCriterioAvaliacaoIucn = tha.deCriterioAvaliacaoIucn ?: ''
                dados.stPossivelmenteExtinta = tha.stPossivelmenteExtinta ?: ''
                dados.tipoAvaliacao = tha.tipoAvaliacao?.descricao ?: ''
                dados.tipoAbrangencia = tha.tipoAbrangencia?.descricao ?: ''
                dados.mesAnoUltimaAvaliacao = tha.nuAnoAvaliacao ? tha.nuAnoAvaliacao.toString() : ''
            }
        }
        return dados
    }




    /**
     * retornar os dados da ultima avaliacao nacional da tabela taxon_historico_avaliacao
     * @param idFicha
     * @return
     */
    Map getUltimaAvaliacaoNacionalHist( Long idFicha = 0 ){
        Map dados = [id:0,sqCategoriaIucn: '',coCategoriaIucn: '',deCategoriaIucn: '',nuAnoAvaliacao: '', txJustificativaAvaliacao: '', deCriterioAvaliacaoIucn: '', stPossivelmenteExtinta: '', tipoAvaliacao:'', tipoAbrangencia:'' ]
        if( idFicha ) {
            VwFicha vwFicha = VwFicha.get( idFicha )
            if ( vwFicha ) {
                DadosApoio tipoNacionalBrasil = DadosApoio.findByCodigoSistema('NACIONAL_BRASIL')
                // primeiro ler a última avaliação nacional gravada no histórico
                TaxonHistoricoAvaliacao tha = TaxonHistoricoAvaliacao.createCriteria().get {
                    eq('taxon.id', vwFicha.sqTaxon.toLong())
                    eq('tipoAvaliacao', tipoNacionalBrasil)
                    //lt('nuAnoAvaliacao', vwFicha.cicloAvaliacao.nuAno) // salve sem ciclo não tem mais este limitador
                    order('nuAnoAvaliacao', 'desc')
                    order('id', 'desc')
                    maxResults(1)
                }
                if (tha) {
                    dados.id                        = tha.id ?:0
                    dados.sqCategoriaIucn           = tha.categoriaIucn.id ?: ''
                    dados.coCategoriaIucn           = tha.categoriaIucn.codigo ?: ''
                    dados.deCategoriaIucn           = tha.categoriaIucn.descricaoCompleta ?: ''
                    dados.nuAnoAvaliacao            = tha.nuAnoAvaliacao ?: ''
                    dados.txJustificativaAvaliacao  = tha.txJustificativaAvaliacao ?: ''
                    dados.deCriterioAvaliacaoIucn   = tha.deCriterioAvaliacaoIucn ?: ''
                    dados.stPossivelmenteExtinta    = tha.stPossivelmenteExtinta ?: ''
                    dados.tipoAvaliacao             = tha.tipoAvaliacao?.descricao ?: ''
                    dados.tipoAbrangencia           = tha.tipoAbrangencia?.descricao ?: ''
                    dados.mesAnoUltimaAvaliacao     = tha.nuAnoAvaliacao ? tha.nuAnoAvaliacao.toString() : ''
                }
            }
        }
        return dados
    }

    /**
     * Este método calcula a ultima avaliação nacional cadastrada na aba Conservação->Histórico Avaliação.
     * Caso a aba Avaliação Final estiver preenchida, esta prevalece sobre os dados do histórico
     * Caso não exista no histórico nem na Avaliação Final será retornado a categoria Não Avaliada (NE) com id=0
     * @param ficha Instância da classe Ficha
     * @return Mapa com resultados
     */
    Map getUltimaAvaliacaoNacional(Ficha ficha) { // evitar erro caso haja alguma chamada passando ficha ainda
        return getUltimaAvaliacaoNacional(ficha?.id ?: 0)
    }

    Map getUltimaAvaliacaoNacional(Long idFicha = 0) {
        VwFicha vwFicha = VwFicha.get(idFicha)
        Map resultado = [:]
        if (!vwFicha) {
            return resultado
        }

        DadosApoio tipoNacionalBrasil = DadosApoio.findByCodigoSistema('NACIONAL_BRASIL')

        if ( vwFicha.sqCategoriaFinal ) {
            resultado.id = 0
            resultado.tipoAvaliacao             = tipoNacionalBrasil.descricao
            resultado.tipoAbrangencia           = 'País'
            resultado.sqCategoriaIucn           = vwFicha.sqCategoriaFinal
            resultado.coCategoriaIucn           = vwFicha?.categoriaFinal?.codigo
            resultado.deCategoriaIucn           = vwFicha?.categoriaFinal?.descricaoCompleta
            resultado.stPossivelmenteExtinta    = vwFicha?.stPossivelmenteExtintaFinal
            resultado.txJustificativaAvaliacao  = ''
            resultado.nuAnoAvaliacao            = ''
            resultado.mesAnoUltimaAvaliacao     = ''
            resultado.deCriterioAvaliacaoIucn   = ''
            // recuperar campos textos longos da ficha
            String hql = """select new map(f.dsJustificativaFinal as dsJustificativaFinal
                          ,f.dsCriterioAvalIucnFinal as dsCriterioAvalIucnFinal
                          ,c.nuAno as nuAno
                          )
                  from Ficha f
                  join f.cicloAvaliacao c
                  where f.id = :id
                  """
            Ficha.executeQuery(hql, [id: idFicha], [max: 1, offset: 0]).each { linha ->
                resultado.txJustificativaAvaliacao = linha.dsJustificativaFinal ?: ''
                resultado.nuAnoAvaliacao = linha.nuAno ?: ''
                resultado.deCriterioAvaliacaoIucn = linha.dsCriterioAvalIucnFinal ?: ''
            }

            // ler o mes da última oficina de Avaliação da ficha
            String cmdSql ="""select to_char(oficina.dt_inicio, 'mm/yyyy') as de_mes_ano, oficina.dt_inicio
                            from salve.oficina_ficha, salve.ficha, salve.oficina, salve.dados_apoio
                            where oficina.sq_oficina = oficina_ficha.sq_oficina
                            and oficina_ficha.sq_ficha = ficha.sq_ficha
                            and oficina.sq_tipo_oficina = dados_apoio.sq_dados_apoio
                            and dados_apoio.cd_sistema = 'OFICINA_AVALIACAO'
                            and ficha.sq_ficha = ${idFicha.toString()}
                            order by oficina.dt_fim desc limit 1"""
            executeSql( cmdSql ).each{row ->
                //resultado.mesAnoUltimaAvaliacao = row.de_mes_ano
                resultado.mesAnoUltimaAvaliacao = row?.dt_inicio?.format('MMM/yyyy')?.toString()?.capitalize()
            }
        } else {
            // primeiro ler a última avaliação nacional gravada no histórico
            TaxonHistoricoAvaliacao tha = TaxonHistoricoAvaliacao.createCriteria().get {
                eq('taxon.id', vwFicha.sqTaxon.toLong())
                eq('tipoAvaliacao', tipoNacionalBrasil)
                lt('nuAnoAvaliacao', vwFicha.cicloAvaliacao.nuAno)
                order('nuAnoAvaliacao', 'desc')
                maxResults(1)
            }
            if (tha) {
                resultado.id = tha.id.toInteger()
                resultado.tipoAvaliacao = tha.tipoAvaliacao?.descricao ?: ''
                resultado.tipoAbrangencia = tha.tipoAbrangencia?.descricao ?: ''
                resultado.sqCategoriaIucn = tha.categoriaIucn?.id ?: ''
                resultado.coCategoriaIucn = tha.categoriaIucn?.codigo ?: ''
                resultado.deCategoriaIucn = tha.categoriaIucn?.descricaoCompleta ?: ''
                resultado.txJustificativaAvaliacao = tha.txJustificativaAvaliacao ?: ''
                resultado.nuAnoAvaliacao = tha.nuAnoAvaliacao ?: ''
                resultado.deCriterioAvaliacaoIucn = (tha.deCriterioAvaliacaoIucn ?: '')
                resultado.stPossivelmenteExtinta = tha?.stPossivelmenteExtinta ?: ''
                resultado.mesAnoUltimaAvaliacao = tha.nuAnoAvaliacao ? tha.nuAnoAvaliacao.toString() : ''
            } else {
                DadosApoio tipoNaoAvaliada = getDadosApoioByCodigo('TB_CATEGORIA_IUCN', 'NE')
                resultado.id = 0
                resultado.tipoAvaliacao = ''
                resultado.tipoAbrangencia = ''
                resultado.sqCategoriaIucn = tipoNaoAvaliada?.id
                resultado.coCategoriaIucn = tipoNaoAvaliada?.codigo
                resultado.deCategoriaIucn = tipoNaoAvaliada?.descricaoCompleta
                resultado.txJustificativaAvaliacao = ''
                resultado.nuAnoAvaliacao = ''
                resultado.deCriterioAvaliacaoIucn = ''
                resultado.stPossivelmenteExtinta = ''
                resultado.mesAnoUltimaAvaliacao = ''
            }
        }
        if (resultado.coCategoriaIucn == 'CR') {
            if (resultado.stPossivelmenteExtinta != 'S') {
                resultado.stPossivelmenteExtinta = 'N'
            }
        } else {
            resultado.stPossivelmenteExtinta = ''
        }
        return resultado
    }

    List getListSituacao() {
        return dadosApoioService.getTable('TB_SITUACAO_FICHA');
    }

    DadosApoio getDadosApoioByCodigo(String table, String codigo) {
        if (dadosApoioService) {
            return dadosApoioService.getByCodigo(table, codigo)
        } else {
            return DadosApoioService.getByCodigo(table, codigo)
        }
    }
//------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------
/*
    Integer sqCicloDestino
    , Lista listaIdsCopiar
    , Integer sqPessoa
    , boolean copiarPendencias
 */

    void asyncCopiarFichas( Map data=[:] ) {
        cacheService.clear() // limpar todos os caches
        asyncService.copiarFichas( data )
    }
//------------------------------------------------------------------------------------------------------
    boolean copiarFichas(Integer sqCicloDestino, Lista listaIdsCopiar, Integer sqPessoa, boolean copiarPendencias) {
        return false
        // não está mais sendo utilizada este método, passouo para asyncCopiarFichas
        try {
            CicloAvaliacao cicloDestino = CicloAvaliacao.get(sqCicloDestino)
            PessoaFisica usuario = PessoaFisica.get(sqPessoa)
            if (!usuario) {
                log.info( 'Pessoa Física ' + sqPessoa + ' não encontrada')
                return false
            }
            Ficha fichaOrigem
            Ficha fichaDestino
            FichaPendencia fichaPendenciaDestino
            FichaNomeComum fichaNomeComumDestino
            FichaSinonimia fichaSinonimaDestino
            FichaAnexo fichaAnexoDestino
            FichaOcorrencia fichaOcorrenciaDestino
            FichaAmbienteRestrito fichaAmbienteRestritoDestino
            FichaAreaRelevancia fichaAreaRelevanciaDestino
            FichaAreaRelevanciaMunicipio fichaAreaRelevanciaMunicipioDestino
            FichaHabitoAlimentar fichaHabitoAlimentarDestino
            FichaHabitoAlimentarEsp fichaHabitoAlimentarEspDestino
            FichaHabitat fichaHabitatDestino
            FichaInteracao fichaInteracaoDestino
            FichaVariacaoSazonal fichaVariacaoSazonalDestino
            FichaPopulEstimadaLocal fichaPopulEstimadaLocalDestino
            FichaAreaVida fichaAreaVidaDestino
            FichaAmeaca fichaAmeacaDestino
            FichaAmeacaRegiao fichaAmeacaRegiaoDestino
            FichaUso fichaUsoDestino
            FichaUsoRegiao fichaUsoRegiaoDestino
            //FichaHistoricoAvaliacao fichaHistoricoAvaliacaoDestino
            TaxonHistoricoAvaliacao taxonHistoricoAvaliacaoDestino
            FichaListaConvencao fichaListaConvencaoDestino
            FichaAcaoConservacao fichaAcaoConservacaoDestino
            FichaPesquisa fichaPesquisaDestino
            FichaDeclinioPopulacional fichaDeclinioPopulacionalDestino
            FichaMudancaCategoria fichaMudancaCategoriaDestino
            //FichaAvaliacaoRegional fichaAvaliacaoRegional
            DadosApoio tipoAvaliacaoNacional = getDadosApoioByCodigo('TB_TIPO_AVALIACAO', 'NACIONAL_BRASIL')
            DadosApoio tipoAvaliacaoEstadual = getDadosApoioByCodigo('TB_TIPO_AVALIACAO', 'ESTADUAL')
            DadosApoio tipoAvaliacaoRegional = getDadosApoioByCodigo('TB_TIPO_AVALIACAO', 'REGIONAL')
            DadosApoio situacaoCompilacao = getDadosApoioByCodigo('TB_SITUACAO_FICHA', 'COMPILACAO')
            cacheService.clear() // limpar todos os caches
            listaIdsCopiar.list.each
                {
                    // println ' '
                    // println ' '
                    // println ' '
                    // println "-"*100
                    // println 'Copiando ficha ' + it + '  para o ciclo ' + sqCicloDestino
                    fichaOrigem = Ficha.get(it)
                    if (fichaOrigem) {
                        // ABA TAXONOMIA
                        fichaDestino = Ficha.findByTaxonAndCicloAvaliacao(fichaOrigem.taxon, cicloDestino)

                        // copiar somente se não existir a especie no ciclo destino
                        if (!fichaDestino) {
                            fichaDestino = new Ficha(fichaOrigem.properties)
                            fichaDestino.fichaCicloAnterior = fichaOrigem
                            fichaDestino.pendencias = null
                            fichaDestino.cicloAvaliacao = cicloDestino
                            fichaDestino.usuario = usuario
                            fichaDestino.situacaoFicha = situacaoCompilacao

                            // limpar campos que não serão copiados
                            fichaDestino.categoriaFinal = null
                            fichaDestino.dsCriterioAvalIucnFinal = null
                            fichaDestino.dsJustificativaFinal = null
                            fichaDestino.stPossivelmenteExtintaFinal=null
                            fichaDestino.dtPublicacao = null
                            fichaDestino.publicadoPor = null
                            //-------------------------------
                            // limpar dados das oficinas - aba 11.6
                            fichaDestino.categoriaIucn = null
                            fichaDestino.dsCriterioAvalIucn = null
                            fichaDestino.stPossivelmenteExtinta = null
                            fichaDestino.dsJustificativa = null
                            fichaDestino.stManterLc = null
                            fichaDestino.stTransferida = null

                            fichaDestino.save()
                            // println 'Ficha copiada... '+fichaOrigem.id
                            // println 'ID Nova Ficha... ' + fichaDestino.id
                            _copiarRefBib(fichaOrigem, fichaDestino, 'ficha', fichaOrigem.id, fichaDestino.id)

                            // copiar as referencias referentes ao historico das avaliações
                            _copiarRefBib(fichaOrigem, fichaDestino, 'taxon_historico_avaliacao', fichaOrigem.id, fichaDestino.id)
                            // fim if ficha

                            // PENDENCIAS
                            if (copiarPendencias) {
                                FichaPendencia.findAllByFicha(fichaOrigem).each {

                                    fichaPendenciaDestino = new FichaPendencia(it.properties)
                                    fichaPendenciaDestino.ficha = fichaDestino
                                    fichaPendenciaDestino.save()
                                } // fim each pendencias
                            }

                            /**/
                            // 2) Nomes Comuns
                            FichaNomeComum.findAllByFicha(fichaOrigem).each {
                                fichaNomeComumDestino = new FichaNomeComum(it.properties)
                                fichaNomeComumDestino.ficha = fichaDestino
                                fichaNomeComumDestino.save()
                                // println '  - Nome Comum copiado...' + it.id
                                _copiarRefBib(fichaOrigem, fichaDestino, 'ficha_nome_comum', it.id, fichaNomeComumDestino.id)
                            } // fim each nome comum
                            /**/
                            /**/
                            // 3) Sinonímias
                            FichaSinonimia.findAllByFicha(fichaOrigem).each {
                                fichaSinonimaDestino = new FichaSinonimia(it.properties)
                                fichaSinonimaDestino.ficha = fichaDestino
                                fichaSinonimaDestino.save()
                                // println '\n  - Nome Sinonimia copiada...'+it.id
                                _copiarRefBib(fichaOrigem, fichaDestino, 'ficha_sinonimia', it.id, fichaSinonimaDestino.id)
                            } // fim each nome comum
                            /**/

                            /**/
                            // Tabela de Anexos vai direto não importa o contexto
                            FichaAnexo.findAllByFicha(fichaOrigem).each {
                                if( it?.contexto?.codigoSistema != 'MAPA_DISTRIBUICAO' || it.inPrincipal != 'S' ) {
                                    fichaAnexoDestino = new FichaAnexo(it.properties)
                                    fichaAnexoDestino.ficha = fichaDestino
                                    fichaAnexoDestino.save()
                                }
                                // println '\n  - Anexos da ficha foram copiados...'+it.id
                            }
                            /**/

                            /**/
                            // Tabela de Ocorrencias vai direto não importa o contexto
                            FichaOcorrencia.findAllByFicha(fichaOrigem).each {
                                fichaOcorrenciaDestino = new FichaOcorrencia(it.properties)
                                fichaOcorrenciaDestino.geoValidate = false
                                fichaOcorrenciaDestino.ficha = fichaDestino
                                fichaOcorrenciaDestino.save()
                                _copiarRefBib(fichaOrigem, fichaDestino, 'ficha_ocorrencia', it.id, fichaOcorrenciaDestino.id)
                                // println '\n  - Ocorrências da ficha foram copiadas...' + it.id
                            }
                            /**/

                            // ABA DISTRIBUICAO
                            /**/
                            //ambiente restrito
                            FichaAmbienteRestrito.findAllByFicha(fichaOrigem).each {
                                fichaAmbienteRestritoDestino = new FichaAmbienteRestrito(it.properties)
                                fichaAmbienteRestritoDestino.ficha = fichaDestino
                                fichaAmbienteRestritoDestino.validate()
                                // println fichaAmbienteRestritoDestino.getErrors()
                                fichaAmbienteRestritoDestino.save()
                                _copiarRefBib(fichaOrigem, fichaDestino, 'ficha_ambiente_restrito', it.id, fichaAmbienteRestritoDestino.id)
                                // println '\n  - Ambiente Restrito copiado...' + it.id
                            }
                            /**/

                            //areas relevantes
                            /**/
                            FichaAreaRelevancia.findAllByFicha(fichaOrigem).each {
                                fichaAreaRelevanciaDestino = new FichaAreaRelevancia(it.properties)
                                fichaAreaRelevanciaDestino.ficha = fichaDestino
                                fichaAreaRelevanciaDestino.validate()
                                // println fichaAreaRelevanciaDestino.getErrors()
                                fichaAreaRelevanciaDestino.save()
                                _copiarRefBib(fichaOrigem, fichaDestino, 'ficha_area_relevancia', it.id, fichaAreaRelevanciaDestino.id)
                                FichaAreaRelevanciaMunicipio.findAllByFichaAreaRelevancia(it).each { it2 ->
                                    fichaAreaRelevanciaMunicipioDestino = new FichaAreaRelevanciaMunicipio(it2.properties)
                                    fichaAreaRelevanciaMunicipioDestino.fichaAreaRelevancia = fichaAreaRelevanciaDestino
                                    fichaAreaRelevanciaMunicipioDestino.save()
                                    // println '      - Municipio copiado...' + it2.id
                                }
                                // println '\n  - Area Relevancia copiada...' + it.id
                            }
                            /**/

                            // História Natural
                            /**/

                            FichaHabitoAlimentar.findAllByFicha(fichaOrigem).each {
                                fichaHabitoAlimentarDestino = new FichaHabitoAlimentar(it.properties)
                                fichaHabitoAlimentarDestino.ficha = fichaDestino
                                fichaHabitoAlimentarDestino.validate()
                                // println fichaHabitoAlimentarDestino.getErrors()
                                fichaHabitoAlimentarDestino.save()
                                _copiarRefBib(fichaOrigem, fichaDestino, 'ficha_habito_alimentar', it.id, fichaHabitoAlimentarDestino.id)
                                // println '\n  - Habito Alimentar copiado...' + it.id
                            }

                            FichaHabitoAlimentarEsp.findAllByFicha(fichaOrigem).each {
                                fichaHabitoAlimentarEspDestino = new FichaHabitoAlimentarEsp(it.properties)
                                fichaHabitoAlimentarEspDestino.ficha = fichaDestino
                                fichaHabitoAlimentarEspDestino.validate()
                                // println fichaHabitoAlimentarEspDestino.getErrors()
                                fichaHabitoAlimentarEspDestino.save()
                                _copiarRefBib(fichaOrigem, fichaDestino, 'ficha_habito_alimentar_esp', it.id, fichaHabitoAlimentarEspDestino.id)
                                // println '\n  - Habito Alimentar Especialista copiado...' + it.id
                            }
                            /**/

                            // habitat
                            /**/
                            FichaHabitat.findAllByFicha(fichaOrigem).each {
                                fichaHabitatDestino = new FichaHabitat(it.properties)
                                fichaHabitatDestino.ficha = fichaDestino
                                fichaHabitatDestino.validate()
                                // println fichaHabitatDestino.getErrors()
                                fichaHabitatDestino.save()
                                _copiarRefBib(fichaOrigem, fichaDestino, 'ficha_habitat', it.id, fichaHabitatDestino.id)
                                // println '\n  - Habitats copiados...' + it.id
                            }
                            /**/

                            // interação
                            /**/
                            FichaInteracao.findAllByFicha(fichaOrigem).each {
                                fichaInteracaoDestino = new FichaInteracao(it.properties)
                                fichaInteracaoDestino.ficha = fichaDestino
                                fichaInteracaoDestino.validate()
                                // println fichaInteracaoDestino.getErrors()
                                fichaInteracaoDestino.save()
                                _copiarRefBib(fichaOrigem, fichaDestino, 'ficha_interacao', it.id, fichaInteracaoDestino.id)
                                // println '\n  - Interações copiados...' + it.id
                            }
                            /**/

                            // Sasonalidade
                            /**/
                            FichaVariacaoSazonal.findAllByFicha(fichaOrigem).each {
                                fichaVariacaoSazonalDestino = new FichaVariacaoSazonal(it.properties)
                                fichaVariacaoSazonalDestino.ficha = fichaDestino
                                fichaVariacaoSazonalDestino.validate()
                                // println fichaVariacaoSazonalDestino.getErrors()
                                fichaVariacaoSazonalDestino.save()
                                _copiarRefBib(fichaOrigem, fichaDestino, 'ficha_variacao_sazonal', it.id, fichaVariacaoSazonalDestino.id)
                                // println '\n  - Variacao Sazonal copiada...' + it.id
                            }
                            /**/

                            // População Conhecida/Estimada
                            /**/
                            FichaPopulEstimadaLocal.findAllByFicha(fichaOrigem).each {
                                fichaPopulEstimadaLocalDestino = new FichaPopulEstimadaLocal(it.properties)
                                fichaPopulEstimadaLocalDestino.ficha = fichaDestino
                                fichaPopulEstimadaLocalDestino.validate()
                                // println fichaPopulEstimadaLocalDestino.getErrors()
                                fichaPopulEstimadaLocalDestino.save()
                                _copiarRefBib(fichaOrigem, fichaDestino, 'ficha_popul_estimada_local', it.id, fichaPopulEstimadaLocalDestino.id)
                                // println '\n  - Populacao Conhecida / Estimada copiada...' + it.id
                            }
                            /**/

                            // Area de Vida
                            /**/
                            FichaAreaVida.findAllByFicha(fichaOrigem).each {
                                fichaAreaVidaDestino = new FichaAreaVida(it.properties)
                                fichaAreaVidaDestino.ficha = fichaDestino
                                fichaAreaVidaDestino.validate()
                                // println fichaAreaVidaDestino.getErrors()
                                fichaAreaVidaDestino.save()
                                _copiarRefBib(fichaOrigem, fichaDestino, 'ficha_area_vida', it.id, fichaAreaVidaDestino.id)
                                // println '\n  - Area de Vida copiada...' + it.id
                            }
                            /**/

                            // Ameaças
                            /**/
                            FichaAmeaca.findAllByFicha(fichaOrigem).each {
                                fichaAmeacaDestino = new FichaAmeaca(it.properties)
                                fichaAmeacaDestino.ficha = fichaDestino
                                fichaAmeacaDestino.validate()
                                // println fichaAmeacaDestino.getErrors()
                                fichaAmeacaDestino.save()
                                _copiarRefBib(fichaOrigem, fichaDestino, 'ficha_ameaca', it.id, fichaAmeacaDestino.id)
                                // println '    - Ameaça copiada...' + it.id
                                // copiar regionalização e georeferencia da ameaça
                                FichaAmeacaRegiao.findAllByFichaAmeaca(it).each { it2 ->
                                    // duplicar a tabela ficha_ocorrencia referente, aqui pega as regiões e os georeferenciamento
                                    fichaOcorrenciaDestino = new FichaOcorrencia(it2.fichaOcorrencia.properties)
                                    fichaOcorrenciaDestino.geoValidate = false
                                    fichaOcorrenciaDestino.ficha = fichaDestino
                                    fichaOcorrenciaDestino.save() // gerar nova ocorrencia
                                    // println '      - Ocorrência Criada...' + fichaOcorrenciaDestino.id

                                    fichaAmeacaRegiaoDestino = new FichaAmeacaRegiao(it2.properties)
                                    fichaAmeacaRegiaoDestino.fichaAmeaca = fichaAmeacaDestino
                                    fichaAmeacaRegiaoDestino.fichaOcorrencia = fichaOcorrenciaDestino
                                    fichaAmeacaRegiaoDestino.save()
                                    // println '      - Ameaça Região e Georeferencia copiada...' + it2.id
                                }
                                FichaAmeacaEfeito fichaAmeacaEfeitoDestino
                                println 'Ler efeito da ficha ameaca ' + it.id
                                FichaAmeacaEfeito.findAllByFichaAmeaca( it ).each { it2 ->
                                    fichaAmeacaEfeitoDestino = new FichaAmeacaEfeito()
                                    fichaAmeacaEfeitoDestino.fichaAmeaca = fichaAmeacaDestino.id
                                    fichaAmeacaEfeitoDestino.efeito = it2.efeito
                                    fichaAmeacaEfeitoDestino.nuPeso = it2.nuPeso
                                    fichaAmeacaEfeitoDestino.save()
                                }
                            }
                            /**/

                            // Usos
                            /**/
                            FichaUso.findAllByFicha(fichaOrigem).each {
                                fichaUsoDestino = new FichaUso(it.properties)
                                fichaUsoDestino.ficha = fichaDestino
                                fichaUsoDestino.validate()
                                // println fichaUsoDestino.getErrors()
                                fichaUsoDestino.save()
                                _copiarRefBib(fichaOrigem, fichaDestino, 'ficha_uso', it.id, fichaUsoDestino.id)
                                // println '    - Uso copiado...' + it.id
                                // copiar regionalização e georeferencia do uso
                                FichaUsoRegiao.findAllByFichaUso(it).each { it2 ->
                                    // duplicar a tabela ficha_ocorrencia referente, aqui pega as regiões e os georeferenciamento
                                    fichaOcorrenciaDestino = new FichaOcorrencia(it2.fichaOcorrencia.properties)
                                    fichaOcorrenciaDestino.ficha = fichaDestino
                                    fichaOcorrenciaDestino.geoValidate = false
                                    fichaOcorrenciaDestino.save() // gerar nova ocorrencia
                                    // println '      - Ocorrência Criada...' + fichaOcorrenciaDestino.id

                                    fichaUsoRegiaoDestino = new FichaUsoRegiao(it2.properties)
                                    fichaUsoRegiaoDestino.fichaUso = fichaUsoDestino
                                    fichaUsoRegiaoDestino.fichaOcorrencia = fichaOcorrenciaDestino
                                    fichaUsoRegiaoDestino.save()
                                    // println '      - Uso Região e Georeferencia copiada...' + it2.id
                                }
                            }
                            /**/

                            // CONSERVAÇÃO
                            // Histórico Avaliação
                            /* não precisa mais alimentar esta tabela. Agora o historico é do taxon
                             * e está sendo gravado na tabela TaxonHistoricoAvaliacao
                             */

                            /** /
                             FichaHistoricoAvaliacao.findAllByFicha(fichaOrigem).each {
                             fichaHistoricoAvaliacaoDestino = new FichaHistoricoAvaliacao(it.properties)
                             fichaHistoricoAvaliacaoDestino.ficha = fichaDestino
                             // fichaHistoricoAvaliacaoDestino.validate()
                             // println fichaHistoricoAvaliacaoDestino.getErrors()
                             fichaHistoricoAvaliacaoDestino.save()
                             _copiarRefBib(fichaOrigem, fichaDestino, 'ficha_historico_avaliacao', it.id, fichaHistoricoAvaliacaoDestino.id)
                             // println '\n  - Historico Avaliacao copiado...' + it.id
                             }
                             /**/

                            // Listas e convenções
                            /**/
                            FichaListaConvencao.findAllByFicha(fichaOrigem).each {
                                fichaListaConvencaoDestino = new FichaListaConvencao(it.properties)
                                fichaListaConvencaoDestino.ficha = fichaDestino
                                //fichaListaConvencaoDestino.validate()
                                // println fichaListaConvencaoDestino.getErrors()
                                fichaListaConvencaoDestino.save()
                                // println '\n  - Listas e Convencoes copiado...' + it.id
                            }
                            /**/

                            // Listas e convenções
                            /**/
                            FichaAcaoConservacao.findAllByFicha(fichaOrigem).each {
                                fichaAcaoConservacaoDestino = new FichaAcaoConservacao(it.properties)
                                fichaAcaoConservacaoDestino.ficha = fichaDestino
                                //fichaAcaoConservacaoDestino.validate()
                                // println fichaAcaoConservacaoDestino.getErrors()
                                fichaAcaoConservacaoDestino.save()
                                _copiarRefBib(fichaOrigem, fichaDestino, 'ficha_acao_conservacao', it.id, fichaAcaoConservacaoDestino.id)
                                // println '\n  - Acao Conservacao copiado...' + it.id
                            }
                            /**/

                            // PESQUISA
                            // Existentes / necessárias
                            /**/
                            FichaPesquisa.findAllByFicha(fichaOrigem).each {
                                fichaPesquisaDestino = new FichaPesquisa(it.properties)
                                fichaPesquisaDestino.ficha = fichaDestino
                                //fichaPesquisaDestino.validate()
                                // println fichaPesquisaDestino.getErrors()
                                fichaPesquisaDestino.save()
                                _copiarRefBib(fichaOrigem, fichaDestino, 'ficha_pesquisa', it.id, fichaPesquisaDestino.id)
                                // println '\n  - Pesquisas copiadas...' + it.id
                            }
                            /**/

                            // AVALIAÇÃO
                            // População / Declinios Populacionais
                            /**/
                            FichaDeclinioPopulacional.findAllByFicha(fichaOrigem).each {
                                fichaDeclinioPopulacionalDestino = new FichaDeclinioPopulacional(it.properties)
                                fichaDeclinioPopulacionalDestino.ficha = fichaDestino
                                //fichaDeclinioPopulacionalDestino.validate()
                                // println fichaDeclinioPopulacionalDestino.getErrors()
                                fichaDeclinioPopulacionalDestino.save()
                                // println '\n  - Declinio Populacional copiado...' + it.id
                            }
                            /**/

                            // Motivos Mudança categoria
                            /**/
                            FichaMudancaCategoria.findAllByFicha(fichaOrigem).each {
                                fichaMudancaCategoriaDestino = new FichaMudancaCategoria(it.properties)
                                fichaMudancaCategoriaDestino.ficha = fichaDestino
                                //fichaMudancaCategoriaDestino.validate()
                                // println fichaMudancaCategoriaDestino.getErrors()
                                fichaMudancaCategoriaDestino.save()
                                // println '\n  - Motivos mudanca categoria copiado...' + it.id
                            }

                            // Avaliações Regionais
                            FichaAvaliacaoRegional.findAllByFicha(fichaOrigem).each {
                                // Avaliação Final se não existir
                                taxonHistoricoAvaliacaoDestino = new TaxonHistoricoAvaliacao()
                                taxonHistoricoAvaliacaoDestino.taxon = fichaDestino.taxon
                                taxonHistoricoAvaliacaoDestino.abrangencia = it.abrangencia
                                if (it?.abrangencia?.municipio) {
                                    taxonHistoricoAvaliacaoDestino.tipoAvaliacao = tipoAvaliacaoRegional
                                } else {
                                    taxonHistoricoAvaliacaoDestino.tipoAvaliacao = tipoAvaliacaoEstadual
                                }
                                taxonHistoricoAvaliacaoDestino.nuAnoAvaliacao = it.nuAnoAvaliacao
                                taxonHistoricoAvaliacaoDestino.categoriaIucn = it.categoriaIucn
                                taxonHistoricoAvaliacaoDestino.deCriterioAvaliacaoIucn = it.deCriterioAvaliacaoIucn
                                taxonHistoricoAvaliacaoDestino.txJustificativaAvaliacao = it.txJustificativaAvaliacao
                                if (!taxonHistoricoAvaliacaoDestino.save()) {
                                    println taxonHistoricoAvaliacaoDestino.getErrors()
                                }
                            }

                            /**/
                            // Avaliação Final existir criar o registro na tabela de historico
                            if (fichaOrigem.categoriaFinal) {

                                // só pode haver uma avaliação nacional por taxon
                                taxonHistoricoAvaliacaoDestino = TaxonHistoricoAvaliacao.createCriteria().get {
                                    eq('taxon.id', fichaDestino.taxon.id)
                                    eq('tipoAvaliacao.id', tipoAvaliacaoNacional.id)
                                    eq('nuAnoAvaliacao', fichaOrigem.cicloAvaliacao.nuAno.toInteger())
                                    maxResults(1)
                                }
                                if (!taxonHistoricoAvaliacaoDestino) {
                                    taxonHistoricoAvaliacaoDestino = new TaxonHistoricoAvaliacao()
                                    taxonHistoricoAvaliacaoDestino.taxon = fichaDestino.taxon
                                }
                                taxonHistoricoAvaliacaoDestino.tipoAvaliacao = tipoAvaliacaoNacional
                                taxonHistoricoAvaliacaoDestino.categoriaIucn = fichaOrigem.categoriaFinal
                                taxonHistoricoAvaliacaoDestino.nuAnoAvaliacao = fichaOrigem.cicloAvaliacao.nuAno
                                taxonHistoricoAvaliacaoDestino.deCriterioAvaliacaoIucn = fichaOrigem.dsCriterioAvalIucnFinal
                                taxonHistoricoAvaliacaoDestino.txJustificativaAvaliacao = fichaOrigem.dsJustificativaFinal
                                taxonHistoricoAvaliacaoDestino.save()
                                // println '\n  - Avaliacao Final copiada...' + fichaHistoricoAvaliacaoDestino.id
                            }


                            // atualizar o percentual de preenchimento e gerar as pendências automáticas
                            updatePercentual(fichaDestino.id.toLong())
                            //println fichaOrigem.taxon.noCientifico + ' percentual atualizado.'
                        } else {
                            // println fichaOrigem.taxon.noCientifico + ' já cadastrado no ciclo selecionado!'
                        }
                    }
                    //this.cleanUpGorm()
                }
            return true
        }
        catch (Exception e) {
            println e.getMessage()
            log.info( e.getMessage() )
            return false
        }
    }

    private _copiarRefBib(Ficha fichaOrigem, Ficha fichaDestino, String tabela, Long oldId, Long sqRegistro) {
        FichaRefBib fichaRefBibDestino
        FichaRefBibTag fichaRefBibTagDestino
        FichaRefBib.findAllByFichaAndNoTabelaAndSqRegistro(fichaOrigem, tabela, oldId).each {
            fichaRefBibDestino = new FichaRefBib(it.properties)
            fichaRefBibDestino.ficha = fichaDestino
            fichaRefBibDestino.sqRegistro = sqRegistro
            fichaRefBibDestino.save()
            //println '    - Referencia bibliografica copiada..' + it.id
            FichaRefBibTag.findAllByFichaRefBib(it).each {
                fichaRefBibTagDestino = new FichaRefBibTag(it.properties)
                fichaRefBibTagDestino.fichaRefBib = fichaRefBibDestino
                fichaRefBibTagDestino.save()
                //println '      - Tag da Referencias bibliograficas copiadas...' + it.id
            } // fim each tag
        } // each ref bib
    }

    /**
     * novo metodo para gerar zip das fichas utilizando jobService
     */
    void gerarZipFichasAsync(List fichas, String tempDir = '/data/salve-estadual/temp/', String unidadeOrganizacional = 'Sistema Salve', Map config = [:], String urlSalve='', UserJobs job = null){

        runAsync {
            // definir a unidade organizacional padrão para SALVE webservice
            unidadeOrganizacional = unidadeOrganizacional ?: 'SALVE webservice'

            // definir o formato padrão para RTF
            config.formato = (config?.formato ?: 'rtf')

            // definir o contexto padrão para: exportacao-salve
            config.formato = (config?.contexto ?: 'exportacao-salve')

            // nome do arquivo zip final
            String zipFileName = tempDir + 'fichas-' + config.formato + '-' + config?.contexto + '-' + new Date().format('dd-MMM-yyyy-hh-mm-ss') + '.zip'

            // atualizar o job com o nome do arquivo e o link para download do mesmo
            if (job) {
                //String linkDownload='<a target="_self" href="'+urlSalve+'downloadWithProgress?fileName='+zipFileName+'"><b>AQUI</b></a>'
                job.deMensagem = ''//"Clique ${linkDownload} para baixar o arquivo"
                job.deAndamento = 'Gerando arquivo zip...'
                job.vlMax = fichas.size()
                job.deAndamento = '0 de ' + job.vlMax + ' ficha(s)' // adicionar %s para exibir o percentual executado
                job.vlAtual = 0
                job.noArquivoAnexo = zipFileName
                job.save(flush: true)
            }

            // criar array de erros
            List erros = []

            // iniciar o processo de criacao das fichas no formato especificado e compactação
            byte[] buffer = new byte[1024]
            FileOutputStream fos = new FileOutputStream(zipFileName)
            ZipOutputStream zos = new ZipOutputStream(fos)

            fichas.eachWithIndex { id, rowIndex ->
                // nome do arquivo pdf ou rtf gerado
                String tempFileName
                VwFicha ficha = VwFicha.get(id.toLong())

                try {
                    if (!ficha) {
                        throw new Exception('Id ficha ' + id + ' inexistente.')
                    }
                    if (config?.formato == 'pdf') {
                        PdfFicha pdf = new PdfFicha(ficha.id.toInteger(), tempDir)
                        pdf.setUnidade(unidadeOrganizacional)
                        pdf.setCiclo(ficha.cicloAvaliacao.deCicloAvaliacao.toString());
                        pdf.setRefBibs(getFichaRefBibs(ficha.id.toLong()))
                        pdf.setUfs(getUfsNameByRegiaoText(ficha.id.toLong()))
                        pdf.setBiomas(getBiomasText(ficha.id.toLong()))
                        pdf.setBacias(getBaciasText(ficha.id.toLong()))
                        tempFileName = pdf.run()
                    } else {
                        RtfFicha rtf = new RtfFicha(ficha.id.toInteger(), tempDir, config)
                        rtf.setUnidade(unidadeOrganizacional)
                        rtf.setCiclo(ficha.cicloAvaliacao.deCicloAvaliacao.toString())
                        rtf.setRefBibs(getFichaRefBibs(ficha.id.toLong()))
                        rtf.setUfs(getUfsNameByRegiaoText(ficha.id.toLong()))
                        rtf.setBiomas(getBiomasText(ficha.id.toLong()))
                        rtf.setBacias(getBaciasText(ficha.id.toLong()))
                        tempFileName = rtf.run()
                    }
                    if (!tempFileName) {
                        throw new Exception('Erro na criação do ' + config.formato + ' da ficha ' + ficha.nmCientifico)
                    }

                    // adicionar pdf/rtf ao zip e apaga-lo
                    String entryName = tempFileName.substring(tempDir.length(), tempFileName.length())
                    ZipEntry ze = new ZipEntry(entryName)

                    zos.putNextEntry(ze)
                    FileInputStream fis = new FileInputStream(tempFileName)
                    int len
                    while ((len = fis.read(buffer)) > 0) {
                        zos.write(buffer, 0, len)
                    }
                    fis.close()
                    new File(tempFileName).delete()
                    //Thread.sleep(300)
                    if (job) {
                        //job.stepIt(rowIndex + ' de ' + data.size() + ' (%s)')
                        job.stepIt(rowIndex + ' de ' + fichas.size())
                        if (job.stCancelado) {
                            throw new Exception('Cancelado pelo usuário')
                        }
                    }
                }
                catch (Exception e) {
                    Util.printLog('FichaService.gerarZipFichasAsync()', e.getMessage(), '')
                    erros.push(e.getMessage())
                    if (job && job.stCancelado) {
                        throw e
                    }
                }
            }
            zos.closeEntry()
            zos.close()
            Util.printLog('FichaService.gerarZipFichasAsync()', 'Fim criacao zip fichas', 'Arquivo zip gerado em: ' + zipFileName)

            // finalizar job
            if (job) {
                job.stepIt()
            }
        }
    }

    /**
     * Este método recebe um array de ids de fichas, gera os RTF em um unico arquivo zip
     * @return
     */
    void gerarZipFichas(List fichas, String tempDir = '/data/salve-estadual/temp/', String unidadeOrganizacional = 'Sistema Salve', Map config = [:], GrailsHttpSession selfSession = null) {
        Map resultado   = [zipFileName: '', erros: []]
        Map worker      = [:]
        config.formato = ( config?.formato ?: 'rtf' )
        resultado.zipFileName = tempDir + 'fichas-'+config.formato + '-' + config?.contexto + '-' + new Date().format('dd-MMM-yyyy-hh-mm-ss') + '.zip'
        // adicionar worker na sessão do usuário
        if( selfSession ) {
            worker = workerService.add(selfSession
                , 'Gerando zip de ' + fichas.size() + ' ficha(s) selecionada(s) no formato doc.'
                , fichas.size()
                , "window.open(app.url + 'main/download?fileName=" + resultado.zipFileName + "&delete=1', '_top');")
        }
        // javascript que será executado quando o worker terminar
        //workerService.setCallback(worker,"window.open(app.url + 'main/download?fileName=" + resultado.zipFileName + "&delete=1', '_top');")
        // iniciar arquivo zip
        byte[] buffer = new byte[1024]
        FileOutputStream fos = new FileOutputStream(resultado.zipFileName)
        ZipOutputStream zos = new ZipOutputStream(fos)
        if( worker != [:]  ) {
            workerService.start(worker)
        }
        fichas.each { id ->
            VwFicha ficha = VwFicha.get(id.toLong())
            try {
                if (!ficha) {
                    resultado.erros.push('Id ' + id + ' inválido.')
                } else {
                    if( worker != [:]  ) {
                        workerService.setTitle(worker, ficha.nmCientifico)
                    }

                    String fileName = ''
                    if( config?.formato == 'pdf' ) {
                        PdfFicha pdf = new PdfFicha(ficha.id.toInteger(), tempDir)
                        pdf.unidade = selfSession?.sicae?.user?.sgUnidadeOrg ?: 'SALVE WS'
                        pdf.ciclo = ficha.cicloAvaliacao.deCicloAvaliacao.toString();
                        pdf.setRefBibs(getFichaRefBibs( ficha.id.toLong() ) )
                        pdf.setUfs(getUfsNameByRegiaoText( ficha.id.toLong() ))
                        pdf.setBiomas(getBiomasText( ficha.id.toLong() ))
                        pdf.setBacias(getBaciasText( ficha.id.toLong() ))

                        fileName = pdf.run()
                    } else {
                        RtfFicha rtf = new RtfFicha(ficha.id.toInteger(), tempDir, config)
                        rtf.unidade = unidadeOrganizacional
                        rtf.ciclo = ficha?.cicloAvaliacao?.deCicloAvaliacao?.toString()
                        rtf.setRefBibs(getFichaRefBibs( ficha.id.toLong() ) )
                        rtf.setUfs(getUfsNameByRegiaoText( ficha.id.toLong() ))
                        rtf.setBiomas(getBiomasText( ficha.id.toLong() ))
                        rtf.setBacias(getBaciasText( ficha.id.toLong() ))
                        fileName = rtf.run()
                    }

                    if (!fileName) {
                        resultado.erros.push('Erro ficha ' + ficha.nmCientifico)
                    } else {
                        // adicionar ao zip e apagar
                        String entryName = fileName.substring(tempDir.length(), fileName.length())
                        ZipEntry ze = new ZipEntry(entryName)
                        try {
                            zos.putNextEntry(ze)
                            FileInputStream fis = new FileInputStream(fileName)
                            int len
                            while ((len = fis.read(buffer)) > 0) {
                                zos.write(buffer, 0, len)
                            }
                            fis.close()
                        } catch( Exception e ) {

                        }
                        new File(fileName).delete()
                        if( worker != [:]  ) {
                            workerService.stepIt(worker)
                        }
                        // sleep( 1000 * 120 )
                        // println 'Esperando 2 minutos'
                        Thread.sleep( 300 )
                    }
                }
            }
            catch (Exception e) {
                println e.getMessage();
                resultado.erros.push('Erro ficha id:' + id + ' - ' + ficha.nmCientifico + '<br/>' + e.getMessage())
            }
        }
        zos.closeEntry()
        zos.close()

        if( worker != [:]  ) {
            if (resultado.erros) {
                workerService.setError(worker, resultado.erros.join('<br>'))
            }
        }
    }

    /**
     * metodo para atualizar o percentual de preenchimento da ficha e gravar as pendências de preenchimento
     * @param sqFicha
     */
    void updatePercentual(long sqFicha = 0l) {
        updatePercentual( Ficha.get( sqFicha ) )
    }
    void updatePercentual( Ficha ficha) {
        getSession()
        if ( ! this.appSession?.sicae?.user) {
            println 'Update percentual: SEM SESSÃO DEFINIDA.'
            return
        }
        updatePercentual(ficha, PessoaFisica.get( appSession.sicae.user.sqPessoa ) )
    }

    void updatePercentual( Ficha ficha, PessoaFisica pessoaFisica ) {
        asyncService.updatePercentualPreenchimento(ficha,pessoaFisica)
    }
    /**
     * quando uma subespecie for alterada, atualizar o percentual da especie
     * @param ficha
     */
    void updatePercentualEspecie(long sqFicha = 0l) {
        updatePercentualEspecie( Ficha.get( sqFicha ) )
    }
    void updatePercentualEspecie( Ficha ficha )
    {
        //println ' '
        //println ' '
        //println 'updatePercentualEspecie() chamada'
        //println 'Atualizar percentual da Especie  da subespecie ' + ficha.nmCientifico
        if( ficha && ficha.taxon.nivelTaxonomico.coNivelTaxonomico == 'SUBESPECIE' ) {
            Taxon taxonPai = ficha.taxon.pai
            //println ' Taxon pai  ' + taxonPai.id
            VwFicha fichaEspecie = VwFicha.findBySqTaxonAndSqCicloAvaliacao( taxonPai.id,ficha.cicloAvaliacao.id );
            //println ' Especie ' + fichaEspecie.nmCientifico
            if( fichaEspecie ) {
                this.updatePercentual( fichaEspecie.id )
            }
        }
    }

    /**
     * Atualizar a situação da ficha para Validada ou Validada ( em revisão )
     * - se a ficha não tiver pendencias e com a avaliação final preenchida e na situacao "Validada (em revisao)", passar para Validada
     * - se a ficha tiver pendência, com avaliação final preenchida e na situacao "Validada", passar para Validada ( em revisão )
     * @param ficha
     */
    /*
    void updateSituacaoValidada( Ficha ficha ) {

        if (!ficha) {
            println 'voltou...1'
            return
        }

        if (!this.appSession?.sicae?.user) {
            println 'voltou...2'
            return
        }
        if (ficha.categoriaFinal && ficha.dsJustificativaFinal ) {
            VwFicha vf = VwFicha.get(ficha.id)
            if (ficha.situacaoFicha.codigoSistema == 'VALIDADA_EM_REVISAO') {
                if (vf.nuPendencia.toInteger() == 0i) {
                    sqlService.execSql("update salve.ficha set sq_situacao_ficha =:sqSituacaoFicha where sq_ficha = :sqFicha",[sqficha:ficha.id,sqSituacaoFicha:dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'VALIDADA').id])
                    println 'Ficha Alterada para VALIDADA'
                }
            } else if (ficha.situacaoFicha.codigoSistema == 'VALIDADA') {
                if (vf.nuPendencia.toInteger() > 0i) {
                    sqlService.execSql("update salve.ficha set sq_situacao_ficha =:sqSituacaoFicha where sq_ficha = :sqFicha",[sqficha:ficha.id,sqSituacaoFicha:dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'VALIDADA_EM_REVISAO').id])
                }
            }
        }
    }
    */
    /*
    Integer calcularPercentualPreenchimento( int sqFicha = 0i)
    {
        //def sessionSistema = RequestContextHolder.currentRequestAttributes().getSession()
        Integer result=0
        List listaPendencias=[]
        Ficha.withNewSession { session ->
           Ficha ficha = Ficha.get( sqFicha )
            if( ficha )
            {
                int totalCamposMonitorados = 12
                int qtdCampos  = 0

                // verificar se tem texto distribuicao global
                if( ficha.stEndemicaBrasil && Util.trimEditor( ficha.dsDistribuicaoGeoGlobal )  )
                {
                    qtdCampos++ //1
                }
                else
                {
                    listaPendencias.push('Campo "<b>Endêmica do Brasil?</b>" e "<b>Distribuição Geográfica Global</b>" na aba 2 devem ser preenchidos!' )
                }

                // possui mapa distribuição principal
                if( FichaAnexo.findByFichaAndContextoAndInPrincipal( ficha,dadosApoioService.getByCodigo('TB_CONTEXTO_ANEXO','MAPA_DISTRIBUICAO'),'S' ) )
                {
                    qtdCampos++ //5
                }
                else
                {
                    listaPendencias.push('Nenhuma "<b>Imagens de MAPA ou SHAPEFILE</b>" anexado na aba 2 como principal!' )
                }


                // verificar se tem UF
                if( FichaOcorrencia.findByFichaAndContexto( ficha, dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA','ESTADO') ) )
                {
                    qtdCampos++ //2
                }
                else
                {
                    listaPendencias.push('Nenhuma "<b>UF</b>" informada na aba 2.1!' )
                }


                // verificar se tem BIOMA
                if( FichaOcorrencia.findByFichaAndBiomaIsNotNull( ficha ) )
                {
                    qtdCampos++ //3
                }
                else
                {
                    listaPendencias.push('Nenhum "<b>BIOMA</b>" informado na aba 2.2!' )
                }

                // possui algum registro de ocorrencia
                if( FichaOcorrencia.findByFichaAndContexto( ficha,dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA','REGISTRO_OCORRENCIA') ) )
                {
                    qtdCampos++ //4
                }
                else
                {
                    listaPendencias.push('Nenhuma "<b>OCORRÊNCIA</b>" informada na aba 2.6!' )
                }

                // historia natural
                if( Util.trimEditor( ficha.dsHistoriaNatural ))
                {
                    qtdCampos++ //6
                }
                else
                {
                    listaPendencias.push('Descrição de "<b>HISTÓRIA NATUAL</b>" não informada na aba 3!' )
                }

                // tendencia populacional
                if( ficha.tendenciaPopulacional )
                {
                    qtdCampos++ //7
                }
                else
                {
                    listaPendencias.push('Campo "<b>Tendência Populacional</b>" não informado na aba 4!' )
                }


                // Obs sobre a população
                if( Util.trimEditor(ficha.dsPopulacao) )
                {
                    qtdCampos++ //8
                }
                else
                {
                    listaPendencias.push('Campo "<b>Observação Sobre a População</b>" não informado na aba 4!' )
                }


                // Obs sobre a ameaça
                if( Util.trimEditor( ficha.dsAmeaca ) )
                {
                    qtdCampos++ //9
                }
                else
                {
                    listaPendencias.push('Campo "<b>Observações Gerais Sobre as Ameaças</b>" não informado na aba 5!' )
                }

                // Obs sobre o Uso
                if( Util.trimEditor( ficha.dsUso ) )
                {
                    qtdCampos++ //10
                }
                else
                {
                    listaPendencias.push('Campo "<b>Observações Gerais Sobre os Usos</b>" não informado na aba 6!' )
                }


                // Obs sobre presença em uc ou UC informada contam 1 ponto
                if( Util.trimEditor( ficha.dsPresencaUc ) )
                {
                    qtdCampos++ //11
                }
                else
                {
                    if( FichaOcorrencia.createCriteria().list {
                           eq('ficha', ficha)
                           eq('contexto',dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA','REGISTRO_OCORRENCIA') )
                            and {
                                or {
                                    isNotNull('sqUcFederal')
                                    isNotNull('sqUcEstadual')
                                    isNotNull('sqRppn')
                                }
                            }
                            maxResults(1)
                        } )
                    {
                        qtdCampos++ //11
                    }
                    else
                    {
                        listaPendencias.push('Nenhuma ocorrência em "<b>UC</b>" informada na aba 2.6 ou campo "<b>Observações Gerais Sobre a Presença UC</b>" foi preenchido na aba 7.4!' )
                    }
                }

                // ref. bibliográfica ou comunicacao pessoal
                if( FichaRefBib.findByFicha(ficha) )
                {
                    qtdCampos ++ //12
                }
                else
                {
                    listaPendencias.push('Nenhuma "<b>Referência Bibliográfica</b>" foi informada na ficha!' )
                }
                FichaPendencia pendencia = FichaPendencia.findByFichaAndDeAssunto(ficha,'Campos obrigatórios')
                if( listaPendencias.size() == 0 && pendencia )
                {
                    pendencia.delete(flush:true)
                }
                else if( listaPendencias.size() > 0 )
                {
                    if( !pendencia )
                    {
                        pendencia = new FichaPendencia()
                        pendencia.ficha = ficha
                        pendencia.deAssunto = 'Campos obrigatórios'
                    }
                    pendencia.stPendente  ='S'
                    pendencia.dtPendencia = new Date();
                    pendencia.txPendencia = '<ol>' + listaPendencias.collect{'<li>'+it+'</li>'}.join('')+'</ol>'
                    pendencia.usuario     = Pessoa.get(sessionSistema.sicae.user.sqPessoa)
                    pendencia.save(flush:true)
                }
                if( qtdCampos > totalCamposMonitorados )
                {
                    qtdCampos = totalCamposMonitorados
                }
                result = qtdCampos / totalCamposMonitorados * 100
            }
        }
        return result
    }

    void atualizarPercentualPreenchimento( Ficha ficha )
    {
        Integer curValue = (ficha?.vlPercentualPreenchimento==null ? 0 : ficha.vlPercentualPreenchimento.toInteger() )
        Integer newValue = calcularPercentualPreenchimento( ficha.id.toInteger() )
        if( curValue != newValue || curValue < 0 )
        {
            ficha.vlPercentualPreenchimento = newValue
            ficha.save(flush:true)
        }
    }
    */


    /**
     * atualizar o campo inPresencaAtual da tabela fichaOcorrencia
     * @param ids
     * @param usuario
     */
    void marcarDesmarcarRegistroHistorico(List ids=[], String usuario='') {
        List listSqFichaOcorrencia = []
        List listSqFichaOcorrenciaPortalbio = []
        ids.eachWithIndex { item, i ->
            // registro de ocorrencia do SALVE
            if ( item.bd.toString().toLowerCase() =~ /salve-estadual/ ) {
                listSqFichaOcorrencia.push( item.id.toLong() )
            } else {
                listSqFichaOcorrenciaPortalbio.push( item.id.toLong() )
            }
        }
        String cmdSql
        DadosApoio situacaoUtilizado = dadosApoioService.getByCodigo('TB_SITUACAO_REGISTRO_OCORRENCIA','REGISTRO_UTILIZADO_AVALIACAO')
        DadosApoio situacaoAdicionadoApos = dadosApoioService.getByCodigo('TB_SITUACAO_REGISTRO_OCORRENCIA','REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')

        if( listSqFichaOcorrencia ) {
            cmdSql = """update salve.ficha_ocorrencia set in_presenca_atual=case when in_presenca_atual = 'N' and sq_situacao_avaliacao = ${situacaoUtilizado.id.toString() } then 'S' else 'N' end
            , tx_nao_utilizado_avaliacao=null
            , in_utilizado_avaliacao = 'S'
            , dt_invalidado = null
            , sq_situacao_avaliacao = ${situacaoUtilizado.id.toString() }
            WHERE sq_ficha_ocorrencia in (${listSqFichaOcorrencia.join(',')})
              and sq_situacao_avaliacao <> ${situacaoAdicionadoApos.id.toString()}"""
            sqlService.execSql( cmdSql );
        }
        if( listSqFichaOcorrenciaPortalbio ) {
            cmdSql = """update salve.ficha_ocorrencia_portalbio set in_presenca_atual = case when in_presenca_atual = 'N' and sq_situacao_avaliacao = ${situacaoUtilizado.id.toString() } then 'S' else 'N' end
            , tx_nao_utilizado_avaliacao=null
            , in_utilizado_avaliacao='S'
            , sq_situacao_avaliacao = ${situacaoUtilizado.id.toString() }
            WHERE sq_ficha_ocorrencia_portalbio in (${listSqFichaOcorrenciaPortalbio.join(',')})
              and sq_situacao_avaliacao <> ${situacaoAdicionadoApos.id.toString()}"""
            sqlService.execSql( cmdSql )
        }
    }

    /**
     * atualizar o campo inSensivel da tabela fichaOcorrencia
     * @param ids
     * @param usuario
     */
    void marcarDesmarcarRegistroSensivel(List ids=[], String usuario='') {
        List listSqFichaOcorrencia = []
        ids.eachWithIndex { item, i ->
            // registro de ocorrencia do SALVE
            if ( item.bd == 'salve') {
                listSqFichaOcorrencia.push( item.id.toLong() )
            }
        }
        if( listSqFichaOcorrencia ) {
            String cmdSql = """update salve.ficha_ocorrencia set in_sensivel = case when ( in_sensivel = 'N' or in_sensivel is null ) then 'S' else 'N' end WHERE sq_ficha_ocorrencia in (${listSqFichaOcorrencia.join(',')})"""
            sqlService.execSql( cmdSql )
        } else {
            throw new Exception('Nenhuma registro do SALVE foi selecionado')
        }
    }

    void gravarUtilizadoAvaliacao(String situacao='', List ids=[], String usuario='', String justificativa='', Long sqFicha = null, Long sqMotivoNaoUtilizadoAvaliacao = null, Boolean saveUserLog=true) {
        // se não informar a situção retornar erro
        if ( ! ( situacao =~ /^(S|N|R|E)$/ ) ) {
            /**
             * S=Sim
             * N=Nao
             * E=Excluido
             * R=Restaurado
             */
            throw new RuntimeException('Informe a S,N,R ou E!')
        }
        if ( situacao == 'N' && justificativa?.trim() == '' ) {
            throw new RuntimeException('Necessário informar a justificativa.');
        }

        if( ! sqFicha && ids ) {
            String bd = ids[0].bd
            Long id = ids[0].id.toLong()
            if ( bd == 'salve') {
                FichaOcorrencia fo = FichaOcorrencia.get( id )
                sqFicha = fo.vwFicha.id
            } else if ( bd == 'portalbio') {
                FichaOcorrenciaPortalbio fo = FichaOcorrenciaPortalbio.get( id )
                sqFicha = fo.vwFicha.id
            }
        }

        if( ! sqFicha){
            throw new RuntimeException('Id da ficha não informado.')
        }
        Ficha ficha = Ficha.executeQuery("select new map(f.id as id,f.nmCientifico as nmCientifico) from Ficha f where f.id = :id",[ id:sqFicha ])

        if( !ficha ){
            throw new RuntimeException('Id  da ficha '+ sqFicha + ' não encontrado.')
        }

        // verificar sessao
        getSession()
        if ( ! this.appSession?.sicae?.user ) {
            throw new RuntimeException('Sessão SICA-e expirada. Faça o login novamente.')
        }

        String dataHoraOperacao = new Date().format('yyyy-MM-dd HH:mm:ss')
        String sqPessoaRevisor     = this.appSession.sicae.user.sqPessoa.toString()
        boolean isCentroideEstado = false

        //throw new RuntimeException('Teste Parametros')

        // declaração das variáveis globais
        //FichaOcorrencia fo
        List rowOcorrencia = []
        FichaOcorrenciaPortalbio fop
        Map asyncUpdateFicha = [:] // calcular e atualizar os Estados, municipios, biomas e o percentual de preenchimento da ficha
        List sqlUpdates = []

        // depuração em desenv apenas
        //setDebug( (env=='desenv') )
        setDebug(false)

        d( ' ');
        d(' - Inicio gravacao Utilizado/Nao utilizado na avaliacao')

        // adicionar usuario, data e hora no final da justificativa
        String log = 'Cadastrado por ' + usuario + ' em ' + new Date().format('dd/MM/yyyy HH:mm:ss')
        DadosApoio aceita               = dadosApoioService.getByCodigo('TB_SITUACAO_COLABORACAO', 'ACEITA')

        // adicionar log padrão no final da justificativa
        if (situacao == 'N') {
            justificativa += ('<br/>' + log)
        } else if (situacao == 'S') {
            justificativa = 'Registro utilizado na avaliação.<br/>' + log
        } else if (situacao == 'E') {
            justificativa = 'Registro excluído.<br/>' + log;
        }
        if( justificativa ) {
            justificativa = justificativa.replaceAll(/'/, "")
        }
        String cmdSql
        List idsFichasUpdatePercentual= []

        // iniciar loop dos IDs
        ids.eachWithIndex { item, i ->

            if( sqlUpdates.size() > 100 ) {
                sqlService.execSql(sqlUpdates.join(';\n'));
                sqlUpdates=[]
            }

            // registro de ocorrencia do SALVE
            item.bd = item.bd.toLowerCase()

            if ( item.bd == 'salve') {

                if ( item.id ) {
                    rowOcorrencia = FichaOcorrencia.executeQuery("""select new map(a.id as id, ficha.id as sqFicha, situacaoAvaliacao.codigoSistema as cdSituacaoAvaliacao, a.geometry as geometry)
                        from FichaOcorrencia as a
                        inner join a.ficha as ficha
                        inner join a.situacaoAvaliacao as situacaoAvaliacao
                        where a.id = :id""",[id:item.id.toLong()])

                    if( rowOcorrencia ) {

                        if (!idsFichasUpdatePercentual.contains(rowOcorrencia[0].sqFicha.toLong())) {
                            idsFichasUpdatePercentual.push(rowOcorrencia[0].sqFicha.toLong())
                        }
                        // NOVO-REGISTRO - atualizar tabela registro - fazer o relacionamento do REGISTRO X FICHA_OCORRENCIA
                        if (situacao =~ /S|N/ ) {
                            FichaOcorrencia.withNewSession {
                                FichaOcorrencia.withNewTransaction {
                                    registroService.saveOcorrenciaSalve(rowOcorrencia[0])
                                }
                            }
                        }
                    }
                    if ( situacao == 'E' ) {
                        sqlUpdates.push("""delete from salve.ficha_ref_bib where sq_ficha = ${ficha.id.toString() } and no_tabela='ficha_ocorrencia' and sq_registro=${item.id.toString()}""")
                        sqlUpdates.push("""delete from salve.ficha_ocorrencia where sq_ficha_ocorrencia = ${item.id.toString() }""")
                        // e exclusao fisica será feita pelo processamento
                        //sqlUpdates.push("""update salve.ficha_ocorrencia set in_utilizado_avaliacao = 'E' where sq_ficha_ocorrencia = ${item.id.toString() }""")
                    } else {
                        // ocorrencias SALVE não tem opção de Restaurar
                        if( situacao != 'R' ) {
                            // se a ocorrencia estiver na situação NAO_AVALIADA, alterar para ACEITA
                            // e utilizada na avaliação para sim
                            DadosApoio situacaoAvaliacao
                            String sqlUpdateSituacaoAvaliacao = ''

                            if( situacao == 'S' ){
                                situacaoAvaliacao = dadosApoioService.getByCodigo('TB_SITUACAO_REGISTRO_OCORRENCIA','REGISTRO_UTILIZADO_AVALIACAO' )
                            } else if( situacao == 'N' ){
                                situacaoAvaliacao = dadosApoioService.getByCodigo('TB_SITUACAO_REGISTRO_OCORRENCIA','REGISTRO_NAO_UTILIZADO_AVALIACAO' )
                            }
                            if( situacaoAvaliacao ){
                                sqlUpdateSituacaoAvaliacao = ',sq_situacao_avaliacao = ' +situacaoAvaliacao.id.toString()
                            }
                            //if( fo.situacao && fo.situacao.codigoSistema == 'NAO_AVALIADA' ) {
                            if( rowOcorrencia && rowOcorrencia[0].cdSituacaoAvaliacao == 'NAO_AVALIADA' ) {
                                sqlUpdates.push( """update salve.ficha_ocorrencia set in_utilizado_avaliacao = 'S'
                                ,tx_nao_utilizado_avaliacao=NULL
                                ,st_adicionado_apos_avaliacao=false
                                ,sq_motivo_nao_utilizado_avaliacao=null
                                ,sq_situacao = ${ aceita.id.toString() }
                                ,sq_pessoa_revisor = ${ sqPessoaRevisor }
                                ,dt_revisao = '${ dataHoraOperacao }'
                                ${sqlUpdateSituacaoAvaliacao}
                                where (in_utilizado_avaliacao is null or in_utilizado_avaliacao <> 'S')
                                  AND sq_ficha_ocorrencia = ${ item.id.toString() }""" )
                            }
                            else {
                                sqlUpdates.push( """Update salve.ficha_ocorrencia set st_adicionado_apos_avaliacao=false
                                        , in_utilizado_avaliacao = ${ situacao ? "'"+situacao+"'" : 'null' }
                                        , tx_nao_utilizado_avaliacao = ${ justificativa ? "'" + justificativa + "'": 'null' }
                                        , sq_motivo_nao_utilizado_avaliacao = ${ sqMotivoNaoUtilizadoAvaliacao ?: 'null' }
                                        ${sqlUpdateSituacaoAvaliacao}
                                        where sq_ficha_ocorrencia=${ item.id.toString() }""" )
                            }
                        }
                    }
                }
            }
            else if ( item.bd == 'portalbio' ) {
                d(' - Ocorrencia: PORTALBIO')
                d(' - Nova situacao: '+situacao)
                if ( item.id ) {

                    // ler o registro de ocorrencia
                    fop = FichaOcorrenciaPortalbio.get( item.id.toInteger() )
                    if( ! idsFichasUpdatePercentual.contains(fop.ficha.id.toLong()) ) {
                        idsFichasUpdatePercentual.push(fop.ficha.id.toLong())
                    }

                    // NOVO-REGISTRO - atualizar tabela registro - fazer o relacionamento do REGISTRO X FICHA_OCORRENCIA
                    if( situacao =~ /S|N/ && fop) {
                        registroService.saveOcorrenciaPortalbio( fop )
                    }

                    List rowRefBibPonto
                    cmdSql = """select sq_publicacao, no_autor, nu_ano from salve.ficha_ref_bib
                              where no_tabela = 'ficha_ocorrencia_portalbio' and no_coluna = 'sq_ficha_ocorrencia_portalbio' and sq_registro = ${item.id.toString()}"""
                    rowRefBibPonto = sqlService.execSql(cmdSql)
                    if ( ! rowRefBibPonto ) {
                        // verificar se o registro já possui alguma ref bib
                        // e se não tiver ref bib criar a automatica
                        gerarReferenciaBibliografica( sqFicha.toLong(), fop.id.toLong(), fop.noBaseDados )
                        cmdSql = """select sq_publicacao, no_autor, nu_ano from salve.ficha_ref_bib
                                        where no_tabela = 'ficha_ocorrencia_portalbio' and no_coluna = 'sq_ficha_ocorrencia_portalbio' and sq_registro = ${item.id.toString()}"""
                        rowRefBibPonto = sqlService.execSql(cmdSql)
                    }
                    // Restaurar Registro Excluído
                    String inUtilizadoFinal     = situacao
                    String txNaoUtilizadoFinal  = justificativa ?: ''
                    if ( situacao == 'R') {
                        fop = FichaOcorrenciaPortalbio.get( item.id.toInteger() )
                        //Restaurar um registro excluido
                        //- se txNaoUtilizado contiver o texto "Registro excluído" então voltar ao estado original com o campo in_utilizado_avaliacao em branco
                        // senão colocar o registro como "Não utilizado na avaliação"
                        //if (fop.inUtilizadoAvaliacao == 'E') {
                        if ( fop?.situacaoAvaliacao?.codigoSistema == 'REGISTRO_EXCLUIDO') {
                            if ( fop.txNaoUtilizadoAvaliacao.toString() =~ /(?i).*Registro exclu.*/) {
                                txNaoUtilizadoFinal=''
                                inUtilizadoFinal = ''
                            }
                            if ( txNaoUtilizadoFinal ) {
                                inUtilizadoFinal = 'N'
                            }
                        }
                    }
                    DadosApoio situacaoAvaliacao = null
                    if( inUtilizadoFinal == 'S') {
                        situacaoAvaliacao = dadosApoioService.getByCodigo('TB_SITUACAO_REGISTRO_OCORRENCIA','REGISTRO_UTILIZADO_AVALIACAO' )
                    } else if( inUtilizadoFinal == 'N') {
                        situacaoAvaliacao = dadosApoioService.getByCodigo('TB_SITUACAO_REGISTRO_OCORRENCIA','REGISTRO_NAO_UTILIZADO_AVALIACAO' )
                    } else if( inUtilizadoFinal == 'E') {
                        situacaoAvaliacao = dadosApoioService.getByCodigo('TB_SITUACAO_REGISTRO_OCORRENCIA','REGISTRO_EXCLUIDO' )
                    } else {
                        situacaoAvaliacao = dadosApoioService.getByCodigo('TB_SITUACAO_REGISTRO_OCORRENCIA','REGISTRO_NAO_CONFIRMADO' )
                    }
                    String sqlUpdateSituacaoAvaliacao = ''
                    if( situacaoAvaliacao ){
                        sqlUpdateSituacaoAvaliacao = ',sq_situacao_avaliacao = ' +situacaoAvaliacao.id.toString()
                    }

                    sqlUpdates.push("""update salve.ficha_ocorrencia_portalbio set
                        in_utilizado_avaliacao = ${inUtilizadoFinal ? "'"+inUtilizadoFinal+"'" : 'null'},
                        tx_nao_utilizado_avaliacao = ${ txNaoUtilizadoFinal ? "'"+txNaoUtilizadoFinal+"'" : 'null'},
                        sq_motivo_nao_utilizado_avaliacao = ${ sqMotivoNaoUtilizadoAvaliacao ?: 'null'}
                        ${sqlUpdateSituacaoAvaliacao}
                        where sq_ficha_ocorrencia_portalbio = ${item.id.toString()}""")
                    d(' - Gravada situacao como: '  + fop.inUtilizadoAvaliacao )
                }
            }
        }
        if( sqlUpdates.size() > 0 ) {
            executeSql(sqlUpdates.join(';\n') )
            sqlUpdates=[]
        }

        // atualizar o percentual de preenchimento
        idsFichasUpdatePercentual.each{
            updatePercentual(it)
        }

    } // fim save in utilizado avaliacao


    /**
     * excluir a(s) referencia(s) bibliografica(s) relacionada(s) ao registro que foi excluído
     * @param noTabela
     * @param sqRegistro
     */
    void excluirRefBibRelacionada( String noTabela, Long sqRegistro )
    {
        // excluir a referencia bibliográfica referente ao registro - ESTADO
        String cmd = """delete from salve.ficha_ref_bib
            where no_tabela=:noTabela and sq_registro = :sqRegistro"""
        //println '-'*80
        //println '-'*80
        //println '-'*80
        //println 'Excluir FichaRefBib'
        //println 'noTabela  '+noTabela
        //println 'SqRegistro  '+sqRegistro
        sqlService.execSql(cmd, [ noTabela:noTabela, sqRegistro: sqRegistro ])
    }


    /**
     * Ler as ocorrências da espécie registradas no portalbio e gravar na tabela ficha_ocorrencia_portalbio de forma asyncrona
     * @param data. [idFicha:x,jsCallback:x}
     * @param urlBiocache
     */
    void asyncLoadPortalbio( Map data=[:] ) {

        if ( ! data.idFicha ) {
            return
        }
        VwFicha vwFicha = VwFicha.get(data.idFicha.toInteger())
        if (!vwFicha) {
            return
        }
        if (!vwFicha.cdSituacaoFichaSistema =~ /(COMPILACAO|CONSULTA|CONSULTA_FINALIZADA|CONSOLIDADA)$/) {
            return
        }

        try {
            Integer qtd = FichaOcorrenciaPortalbio.countByVwFicha( vwFicha )
            TimeDuration td = TimeCategory.minus(new Date(), vwFicha.dtAlteracao ?: new Date())
            //println 'Minutos:' + td.minutes
            if( qtd == 0 || !vwFicha.dtAlteracao || td.days > 1 ) {
                asyncService.importarOcorrenciasPortalbio(vwFicha.id.toInteger(), data.jsCallback)
            }
        } catch (Exception e) {
            println e.getMessage()
        }
    }

    void loadOcorrenciaPortalbio(Ficha ficha, String urlBiocache) {

        getSession()
        if( ! this.appSession?.sicae?.user )
        {
            return
        }

        //println 'loadOcorrenciaPortalbio: Ler registros do portalbio ' + ficha.taxon.noCientifico;
        //println 'Ulr:' + urlBiocache


        if (!ficha?.taxon?.noCientifico) {
            return
        }
        List nomesCientificos = [ficha.taxon.noCientifico]
        // adicionar os pontos dos nomes antigos
        // TODO-após adicionar tem que verificar se tem os pontos em algum ciclo anterior para gravar se foi utilizado/não utilizado ou excluído
        /*getFichaSinonimias(ficha).each {
            nomesCientificos.push( it.noSinonimia )
        }
        */
        nomesCientificos.each { nomeCientifico ->
            Boolean gravar = true
            String p = 'raw_taxon_name:"' + nomeCientifico + '"'
            try {

                /*if (!appSession.envProduction) {
                    println ' '
                    println 'FichaService/loadOcorrenciaPortalbio em ' + new Date().format('dd/MM/yyyy HH:mm:ss')
                    println 'Lendo ocorrencias do portalbio para ' + nomeCientifico
                    println 'Url:' + urlBiocache
                    println 'filtro: raw_taxon_name:"'+nomeCientifico+'"'
                    println '-' * 100
                }
                */

                /*
                // filtrar registros em carência
                if( ! session?.sicae  ) {
                    // TODO - testar filtro dos registros em carência se o usuário não etiver logado
                    Date currentDateTime = new Date().format("yyyy-MM-dd'T'HH:mm:ss'Z'")
                    p += " and access_rights:[* TO ${currentDateTime}]"
                }
                */

                def dados = requestService.doPost( urlBiocache, [q: p, pageSize: 10000, params: 'json'], "")
                if (!dados) {
                    return
                }
                def tmpJson = JSON.parse(dados)

                Date dataOcorrencia
                Date dataCarencia
                String strData
                String eventDate
                String carencia
                def local   // raw_locality
                def codigo  // raw_collectionCode
                String autor
                List occurrences
                FichaOcorrenciaPortalbio fop
                GeometryFactory gf = new GeometryFactory()
                if (tmpJson?.sr?.occurrences) {
                    occurrences = tmpJson?.sr?.occurrences
                } else {
                    occurrences = tmpJson.occurrences
                }

                // println 'Total de ' + occurrences.size() + ' ocorrencias de ' + nomeCientifico + ' no PortalBio. ' + new Date()

                occurrences.eachWithIndex { item, i ->

                    /** /
                     println "Ocorrencia PortalBio inválida: " + item.raw_institutionCode + " uuid:" + item.uuid.toString()
                     println 'eventDate: ' + item?.eventDate
                     println 'verbatinEventDate: ' + item?.verbatimEventDate
                     println 'carencia: ' + item?.carencia
                     println 'accessRights: ' + item?.accessRights
                     println 'raw_locality: ' + item?.raw_locality
                     println 'raw_collectionCode: ' + item?.raw_collectionCode
                     println 'scientificName: ' + item?.scientificName
                     println 'raw_scientificName: ' + item?.raw_scientificName
                     println 'raw_institutionCode: ' + item?.raw_institutionCode
                     println 'collector: ' + item?.collector
                     println 'ameaca_s: ' + item?.ameaca_s
                     println 'decimalLongitude: ' + item?.decimalLongitude
                     println 'decimalLatitude: ' + item?.decimalLatitude
                     println ' '
                     /**/

                    if (item?.decimalLatitude
                        && item?.decimalLongitude
                        && item?.decimalLatitude.toString() != 'null'
                        && item?.decimalLongitude.toString() != 'null'
                        && item?.collector
                        && item?.collector.toString() != 'null'
                        && item?.decimalLatitude.toString().indexOf('.') > 0
                        && item?.decimalLatitude.toString().indexOf('.') < 4
                        && item?.decimalLatitude.toString().indexOf('E') == -1
                        && item?.uuid
                    ) {
                        try {
                            eventDate = ''
                            autor = ''
                            // gravar os pontos do portalbio no salve
                            dataOcorrencia = null
                            try {
                                strData = item.verbatimEventDate
                                if (strData) {
                                    try {
                                        strData = strData.split(' ')[0]
                                        if (strData.length() == 10) {
                                            strData = strData.replaceAll(/-/, '/')
                                            //println 'Verbatin data: '+strData
                                            dataOcorrencia = new Date().parse("dd/MM/yyyy", strData)
                                        }
                                    } catch (Exception e) {
                                        dataOcorrencia = null
                                    }
                                }
                                if (!dataOcorrencia && item.eventDate) {
                                    //println 'Event data: '+ item.eventDate
                                    dataOcorrencia = new Date(item.eventDate)
                                }
                            } catch (Exception e) {
                            }

                            dataOcorrencia = dataOcorrencia ?: new Date().parse("dd/MM/yyyy", '01/01/1500')

                            if ( dataOcorrencia ) {
                                Geometry geometry = gf.createPoint(new Coordinate(item.decimalLongitude, item.decimalLatitude))
                                if (geometry) {
                                    geometry.SRID=4674
                                    autor = item.collector.trim()
                                    fop = FichaOcorrenciaPortalbio.findByFichaAndNoAutorAndDtOcorrenciaAndGeometry(ficha, autor, dataOcorrencia, geometry)
                                    if (!fop) {
                                        fop = new FichaOcorrenciaPortalbio()
                                        fop.ficha = ficha
                                        fop.noAutor = autor
                                        fop.dtOcorrencia = dataOcorrencia
                                        fop.geometry = geometry
                                        fop.deUuid = item.uuid.toString().trim()
                                        fop.txOcorrencia = '{}'
                                        gravar = true
                                    } else {
                                        // println 'ja cadastrada ' + item.uuid
                                        gravar = false
                                    }

                                    if (gravar) {

                                        eventDate = '<br/>Data: ' + dataOcorrencia.format("dd/MMM/yyyy");
                                        carencia = ''
                                        strData = ''
                                        dataCarencia = null
                                        try {
                                            if (item?.accessRights) {
                                                dataCarencia = new Date().parse("yyyy/MM/dd", item.accessRights.replace(/-/, '/'))
                                            }
                                        } catch (Exception e) {
                                        }
                                        if (dataCarencia) {
                                            carencia = '<br/>Carência: ' + dataCarencia.format('dd/MM/yyyy')
                                        }
                                        local = ''
                                        if (item?.raw_locality) {
                                            local = item.raw_locality
                                        }
                                        codigo = ''

                                        if (item.raw_collectionCode) {
                                            codigo = item.raw_collectionCode
                                        }
                                        if (item.scientificName) {
                                            fop.noCientifico = item.scientificName
                                        } else if (item?.raw_scientificName) {
                                            fop.noCientifico = item.raw_scientificName
                                        }
                                        fop.noLocal = local
                                        fop.noInstituicao = item.raw_institutionCode + '/' + codigo
                                        fop.deAmeaca = item.ameaca_s
                                        if (dataCarencia) {
                                            fop.dtCarencia = dataCarencia
                                        } else {
                                            fop.dtCarencia = null

                                        }
                                        if (!fop.save()) {
                                            println fop.getErrors()
                                        }
                                    }
                                } else {
                                    def f = new DecimalFormat('##################');
                                    println "Coordendada Invalida: " + item.raw_institutionCode + " uuid:" +
                                        item.uuid.toString() + ' Lon:' +
                                        f.format(item.decimalLongitude) +
                                        ', Lat:' + f.format(item.decimalLatitude); ;
                                }
                            }
                        }
                        catch (Exception e) {
                            println e.getMessage()
                            println "Ocorrencia PortalBio inválida: " + item.raw_institutionCode + " uuid:" + item.uuid.toString()
                            println 'eventDate: ' + item?.eventDate
                            println 'verbatinEventDate: ' + item?.verbatimEventDate
                            println 'carencia: ' + item?.carencia
                            println 'accessRights: ' + item?.accessRights
                            println 'raw_locality: ' + item?.raw_locality
                            println 'raw_collectionCode: ' + item?.raw_collectionCode
                            println 'scientificName: ' + item?.scientificName
                            println 'raw_scientificName: ' + item?.raw_scientificName
                            println 'raw_institutionCode: ' + item?.raw_institutionCode
                            println 'collector: ' + item?.collector
                            println 'ameaca_s: ' + item?.ameaca_s
                            println 'decimalLongitude: ' + item?.decimalLongitude
                            println 'decimalLatitude: ' + item?.decimalLatitude
                            println 'Continuando......'
                        }
                    }
                }
            } catch (Exception e) {
                println e.getMessage()
            }
        }
    } // fim loadPortalbio

    void setSituacaoById(Long idFicha, Long idSituacao) {

        String query = "update salve.ficha set sq_situacao_ficha = ${idSituacao} where sq_ficha=${idFicha}"
        Sql sql = new Sql(dataSource)
        try {
            sql.execute(query)
            sql.commit()
        } catch (Exception e) {
            //println e.getMessage()
        }
        sql.close()
    }

    void setSituacaoByIds(List<Long> idsFicha, Long idSituacao) {
        String query = "update salve.ficha set sq_situacao_ficha = ${idSituacao} where sq_ficha in (${idsFicha.join(',')})"
        Sql sql = new Sql(dataSource)
        try {
            sql.execute(query)
            sql.commit()
        } catch (Exception e) {
            //println e.getMessage()
        }
        sql.close()
    }

    List executeSql(String cmdSql, Map params = [:]) {
        Sql sql = new Sql(dataSource)
        List rows = []
        sql.withTransaction {

            try {
                if (cmdSql.trim() =~ /(?i)^(insert|delete|update)/) {
                    if (cmdSql =~ /(?i)\sRETURNING\s/) {
                        rows = sql.rows(cmdSql, params)
                    } else {
                        sql.executeUpdate(cmdSql, params)
                    }
                } else {
                    rows = sql.rows(cmdSql, params)
                }
            } catch (Exception e) {
                println ' '
                println 'Erro FichaService->execSql() em ' + new Date().format('dd/MM/yyyy HH:mm:ss')
                println cmdSql
                println params
                println ' '
                println e.getMessage()
                println ' '
            }
            if ( sql ) {
               // sql.close()
            }
        }
        return rows
        /*
        try {
            sql.execute(cmdSql)
            sql.commit()
        } catch (Exception e) {
            println e.getMessage()
        }
        sql.close()*/
    }


    /**
     * Recebe uma linha do resultado da query e calcula se a ficha pode ou não ser modificada
     * @param row
     * @return
     */
    Boolean canModifyRow(groovy.sql.GroovyRowResult row) {
        getSession()
        if( ! this.appSession?.sicae?.user )
        {
            return false
        }

        // Administrador sempre pode
        if (appSession?.sicae?.user.isADM()) {
            return true
        }

        if (row?.in_situacao_ciclo.toString().toUpperCase() == 'F') {
            return false
        }
        // usuario logado só pode alterar ficha da sua unidade
        return (row.sq_unidade_org == appSession?.sicae?.user?.sqUnidadeOrg)
    }

    /**
     * Localizar o Estado pelas coordenadas
     * @param lat
     * @param lon
     * @return
     */
    Estado getEstadoByCoord( Double lat, Double lon )
    {
        return geoService.getUfByCoord( lat.toBigDecimal(), lon.toBigDecimal() )
    }

    Estado getEstadoByCoord( String lat, String lon )
    {
        return getEstadoByCoord( Double.parseDouble( lat ), Double.parseDouble( lon ) )
    }

    Estado getEstadoByCoord( Geometry point )
    {
        return getEstadoByCoord( point.y, point.x )
    }

    /**
     * Localizar o Município pelas coordenadas
     * @param lat
     * @param lon
     * @return
     */
    Municipio getMunicipioByCoord( Double lat, Double lon, Estado uf = null )
    {
        //println 'getMunicipioByCoord() 1'
        return geoService.getMunicipioByCoord( lat.toBigDecimal(), lon.toBigDecimal(),uf )
    }
    Municipio getMunicipioByCoord( String lat, String lon, Estado uf = null)
    {
        //println 'getMunicipioByCoord() 2'
        return getMunicipioByCoord( Double.parseDouble( lat ), Double.parseDouble( lon ),uf )
    }
    Municipio getMunicipioByCoord( Geometry point, Estado uf = null)
    {
        //println 'getMunicipioByCoord() 3'
        return getMunicipioByCoord( point.y, point.x, uf )
    }


    /**
     * Localizar o Pais pelas coordenadas
     * @param lat
     * @param lon
     * @return
     */
    Pais getPaisByCoord( Double lat, Double lon )
    {
        return geoService.getPaisByCoord( lat.toBigDecimal(), lon.toBigDecimal() )
    }
    Pais getPaisByCoord( String lat, String lon)
    {
        return getPaisByCoord( Double.parseDouble( lat ), Double.parseDouble( lon ) )
    }
    Pais getPaisByCoord( Geometry point)
    {
        return getPaisByCoord( point.y, point.x )
    }


    /**
     * Localizar a UC pelas coordenadas
     * @param lat
     * @param lon
     * @return
     */
    Uc getUcByCoord( Double lat, Double lon )
    {
        return geoService.getUcByCoord( lat.toBigDecimal(), lon.toBigDecimal() )
    }

    Uc getUcByCoord( String lat, String lon)
    {
        return getUcByCoord( Double.parseDouble( lat ), Double.parseDouble( lon ) )
    }

    Uc getUcByCoord( Geometry point)
    {
        return getUcByCoord( point.y, point.x )
    }

    Bioma getBiomaByCoord( Geometry point)
    {
        return getBiomaByCoord( point.y, point.x )
    }

    DadosApoio getBiomaSalveByCoord( Double lat, Double lon) {
        return geoService.getBiomaSalveByCoord( lat.toBigDecimal(), lon.toBigDecimal() )
    }

    DadosApoio getBiomaSalveByCoord( Geometry point) {
        return getBiomaSalveByCoord( point.y, point.x )
    }

    /**
     * Verificar se o usuário pode modificar a ficha
     */
    boolean canModify( Long sqFicha ) {

        //d(' ')
        //d('fichaService.canModify()')

        if( !sqFicha || sqFicha == 0l ) {
            // d('   - ficha inexistente');
            return true
        }

        // inicializar a propriedade this.appSession
        getSession()
        if( ! this.appSession?.sicae?.user ) {
            //d('   - sessao expirada');
            return false
        }

        Sicae user = this.appSession.sicae.user
        if( ! user )
        {
            //d('   - sessao nao possui usuario definido.');
            return false
        }

        // usuário consulta
        if( user.isCO()  )
        {
            //d('   - Perfil CONSULTA nao pode alterar ficha');
            return false
        }

        // Administrador e colaborador externo podem porque independem da unidade de lotação
        if( user.isADM() || user.isCE() )
        {
            return true
        }
        VwFicha ficha = VwFicha.get( sqFicha )

        if( !ficha || ficha.cicloAvaliacao.inSituacao == 'F' )
        {
            //d('   - ciclo de avaliacao esta fechado.');
            return false
        }

        // quando for coordenador de taxon EXTERNO não tem unidade organizacional
        boolean resultado = ( ! sqFicha || ( ficha.unidade.id == user?.sqUnidadeOrg ) )
        if( sqFicha && user.isCT() && !user.sqUnidadeOrg )
        {
            DadosApoio ct = getDadosApoioByCodigo('TB_PAPEL_FICHA','COORDENADOR_TAXON')
            if( ct ) {
                resultado = ( FichaPessoa.createCriteria().count {
                    eq('vwFicha.id', sqFicha)
                    eq('pessoa.id', user.sqPessoa.toLong())
                    eq('inAtivo', 'S')
                    eq('papel.id', ct.id)
                } > 0 )
            }
        }
        if( ! resultado ) {
            //d('   - Usuario ou o perfil coordenador de taxon nao pode alterar esta ficha.');
        }
        return resultado
    }

    boolean canModify( Ficha ficha ) {
        return canModify( ficha.id.toLong() )
    }
    boolean canModify( VwFicha ficha) {
        return canModify( ficha.id.toLong() )
    }
    boolean canModify( Object sqFicha ) {
        if( !sqFicha ) {
            return true
        }
        return canModify( sqFicha.toLong() )
    }

    List canModifySituacao( Ficha ficha, DadosApoio novaSituacao ) {
        String msg = ''
        List erros = []
        /**
         * ficha não pode ser colocada em CONSOLIDADA SE:
         * a) a ficha estiver editavel
         * c) possuir alguma colaboração pendente de validação
         * d) estiver em alguma consulta ainda em aberto
         */
        if (!canModify(ficha)) {
            erros.push('A ficha não pode ser modificada!')
        }
        if( novaSituacao.codigoSistema == 'CONSOLIDADA') {
            // ficha não pode estar em consulta
            msg = ''
            List listConsultas = CicloConsultaFicha.createCriteria().list {
                eq('ficha', ficha)
                cicloConsulta {
                    ge('dtFim', Util.hoje())
                }
            }.eachWithIndex { it, index ->
                if( ! it.cicloConsultaFichaVersao ) {
                    if (index == 0) {
                        msg = 'Ficha está no período de consulta!'
                    }
                    msg += '<br/>' + it.cicloConsulta.tipoConsulta.descricao + ' -  ' + it.cicloConsulta.deTituloConsulta + '. Período: ' + it.cicloConsulta.periodoText
                }
            }
            if (msg) {
                erros.push(msg)
            }

            // não pode haver nenhuma colaboração com situação NAO_AVALIADA
            DadosApoio situacaoNaoAvaliada = dadosApoioService.getByCodigo('TB_SITUACAO_COLABORACAO', 'NAO_AVALIADA')
            DadosApoio situacaoPendenteAprovacao = dadosApoioService.getByCodigo('TB_SITUACAO_MULTIMIDIA', 'PENDENTE_APROVACAO')
            Integer qtdColaboracoesPendentes = FichaColaboracao.createCriteria().count {
                consultaFicha {
                    eq('ficha', ficha)
                    isNull('cicloConsultaFichaVersao')
                }
                eq('situacao', situacaoNaoAvaliada)
            }

            // verificar se tem algum ponto nao avaliado
            Integer qtdOcorrenciasPendentes = FichaOcorrenciaConsulta.createCriteria().count {
                cicloConsultaFicha {
                    eq('ficha', ficha)
                    isNull('cicloConsultaFichaVersao')
                }
                fichaOcorrencia {
                    eq('situacao', situacaoNaoAvaliada)
                }
            }

            Integer qtdMultimidiasPendentes=0
            /*
            // regra desabilitada - desconsiderar as fotos
            // verificar se existe foto de algum ponto nao avaliada
            Integer qtdMultimidiasPendentes = FichaConsultaMultimidia.createCriteria().count {
                fichaOcorrenciaConsulta {
                    cicloConsultaFicha {
                        eq('ficha', ficha)
                    }
                }
                fichaMultimidia {
                    eq('situacao', situacaoPendenteAprovacao)
                }
            }*/

            if (qtdColaboracoesPendentes + qtdOcorrenciasPendentes + qtdMultimidiasPendentes > 0) {
                erros.push('Para consolidar não pode haver colaboração não avaliada!')
            }
            if (qtdColaboracoesPendentes > 0) {
                erros.push(' - ' + qtdColaboracoesPendentes.toString() + ' colaboração(ões) não avaliada(s)!')
            }
            if (qtdOcorrenciasPendentes > 0) {
                erros.push(' - ' + qtdOcorrenciasPendentes + ' colaboração(ões) de registro de ocorrência não avaliada(s)!')
            }
            if (qtdMultimidiasPendentes > 0) {
                erros.push(' - ' + qtdMultimidiasPendentes + ' colaboração(ões) de fotos não avaliada(s)!')
            }
        } else if( novaSituacao.codigoSistema == 'POS_OFICINA') {

            msg = ''
            // não pode haver nenhuma colaboração com situação NAO_AVALIADA nem RESOLVER_OFICINA
            DadosApoio situacaoNaoAvaliada = dadosApoioService.getByCodigo('TB_SITUACAO_COLABORACAO', 'NAO_AVALIADA')
            DadosApoio situacaoResolverOficina = dadosApoioService.getByCodigo('TB_SITUACAO_COLABORACAO', 'RESOLVER_OFICINA')
            Map totais = [ qtdColaboracaoNaoAvaliada:0,qtdOcorrenciaNaoAvalida:0,qtdColaboracaoResolverOficina:0,qtdOcorrenciaResolverOficina:0 ]
            boolean existePendencia = false
            FichaColaboracao.createCriteria().list {
                consultaFicha {
                    eq('ficha', ficha)
                }
                'in'('situacao', [situacaoNaoAvaliada,situacaoResolverOficina])
            }.each {
                if( ! it.consultaFicha.cicloConsultaFichaVersao) {
                    if (it.situacao == situacaoNaoAvaliada) {
                        existePendencia = true
                        totais.qtdColaboracaoNaoAvaliada++
                    } else if (it.situacao == situacaoResolverOficina) {
                        existePendencia = true
                        totais.qtdColaboracaoResolverOficina++
                    }
                }
            }
            FichaOcorrenciaConsulta.createCriteria().list {
                cicloConsultaFicha {
                    eq('ficha', ficha)
                }
                fichaOcorrencia {
                    'in'('situacao', [situacaoNaoAvaliada,situacaoResolverOficina])
                }
            }.each {
                if( ! it.cicloConsultaFicha.cicloConsultaFichaVersao) {
                    if (it.fichaOcorrencia.situacao == situacaoNaoAvaliada) {
                        existePendencia = true
                        totais.qtdOcorrenciaNaoAvalida++
                    } else if (it.fichaOcorrencia.situacao == situacaoResolverOficina) {
                        existePendencia = true
                        totais.qtdOcorrenciaResolverOficina++
                    }
                }
            }
            if ( existePendencia )
            {
                erros.push('Pendências:')

                // NAO VALIDADAS
                if ( totais.qtdColaboracaoNaoAvaliada > 0) {
                    erros.push(' - '+ totais.qtdColaboracaoNaoAvaliada.toString() + ' colaboração(ões) não avaliada(s)!')
                }
                if ( totais.qtdOcorrenciaNaoAvaliada > 0) {
                    erros.push(' - ' + totais.qtdOcorrenciaNaoAvaliada.toString() + ' ocorrência(s) não avaliada(s)!')
                }
                // RESOLVER EM OFICINA
                if ( totais.qtdColaboracaoResolverOficina > 0) {
                    erros.push(' - ' + totais.qtdColaboracaoResolverOficina.toString() + ' colaboração(ões) para resolver em oficina!')
                }
                if ( totais.qtdOcorrenciaResolverOficina > 0) {
                    erros.push(' - ' + totais.qtdOcorrenciaResolverOficina.toString() + ' ocorrência(s) para resolver em oficina!')
                }

            }
        }
        return erros
    }

    List getTreeAmeacas(Ficha ficha) {
        List tree = []
        FichaAmeaca.findAllByFicha( ficha ).each {
            Map item = tree.find { it2 -> it.criterioAmeacaIucn.codigo + ' - ' + it.criterioAmeacaIucn.descricao == it2.descricao }
            if( ! item ) {
                String codigo = it.criterioAmeacaIucn.ordem.split('\\.').collect{it.toString().padLeft(3,'0')}.join('.')
                tree.push([ordem: codigo, descricao: it.criterioAmeacaIucn.codigo + ' - ' + it.criterioAmeacaIucn.descricao, peso: it.nuPeso])
            }
            else {
                item.peso = it.nuPeso
            }
            it.trilhaList.each { ameaca ->
                if ( ! tree.find { it2 ->
                    it2.descricao.toString().trim() == ameaca.toString().trim()
                }) {
                    Integer posicao = ameaca.toString().indexOf(' - ')
                    if( posicao > -1 ) {
                        String codigo = ameaca.toString().substring(0, posicao).split('\\.').collect {
                            it.toString().padLeft(3, '0')
                        }.join('.')
                        tree.push([ordem: codigo, descricao: ameaca.toString().trim(), peso: ''])
                    }
                }
            }
        }
        tree.sort{ it.ordem }
    }

    /**
     * Método para retornar a lista de nomes dos coordenadores de taxon de uma determinada ficha
     * @param sqFicha
     * @return
     */
    List getListaCoodenadorTaxonFicha( Ficha ficha ) {
        if( ficha ) {
            return getListaCoodenadorTaxonFicha( ficha.id )
        }
        return []
    }
    List getListaCoodenadorTaxonFicha( Long sqFicha ) {
        List resultado = []
        FichaPessoa.executeQuery("""select new map( fp.pessoa.id as sqPessoa, fp.pessoa.noPessoa as noPessoa ) from FichaPessoa fp
                where fp.ficha.id = :sqFicha
                and fp.inAtivo = 'S'
                and fp.papel.codigoSistema = 'COORDENADOR_TAXON'""",[sqFicha:sqFicha]).each {
            it.noPessoa = Util.capitalize( it.noPessoa )
            resultado.push( it )
        }
        return resultado
    }

    String getAutoria( Long sqFicha = null, Boolean updateFicha=false ) {

        String autoria = ''

        if ( ! sqFicha ) {
            throw new Exception( 'Ficha não informada')
        }

        Ficha ficha = Ficha.get( sqFicha )
        if ( ! ficha.categoriaIucn || ficha.categoriaIucn.codigoSistema == 'NE') {
            throw new Exception( 'Ficha precisa estar avaliada para gerar a autoria.')
        }

        // verificar se o Coordenador de Taxon está definido no módulo função
        DadosApoio papel = dadosApoioService.getByCodigo('TB_PAPEL_FICHA', 'COORDENADOR_TAXON')
        FichaPessoa ct = FichaPessoa.findByFichaAndPapelAndInAtivo(ficha, papel, 'S')
        if (!ct) {
            throw new Exception( 'Coordenador de taxon NÃO informado no módulo gerenciar função.')
        }

        // aba 11.6 deve estar preenchida
        List nomeCoordenadorTaxon = getListaCoodenadorTaxonFicha(sqFicha)?.noPessoa

        // ler os Avaliadores das oficinas de avaliação não versionadas que houveram
        DadosApoio papelAvaliador = dadosApoioService.getByCodigo( 'TB_PAPEL_OFICINA', 'AVALIADOR' )
        DadosApoio conviteRecusado = dadosApoioService.getByCodigo( 'TB_SITUACAO_CONVITE_PESSOA', 'CONVITE_RECUSADO' )
        List nomesAvaliadores = []
        String cmdSql ="""SELECT distinct pessoa.no_pessoa from salve.oficina_participante a
                inner join salve.oficina_ficha_participante b on b.sq_oficina_participante = a.sq_oficina_participante
                inner join salve.oficina_ficha c on c.sq_oficina_ficha = b.sq_oficina_ficha
                inner join salve.vw_pessoa pessoa on pessoa.sq_pessoa = a.sq_pessoa
                left join salve.oficina_ficha_versao ofv on ofv.sq_oficina_ficha = c.sq_oficina_ficha
                where b.sq_papel_participante = ${ papelAvaliador.id.toString() }
                and a.sq_situacao <> ${ conviteRecusado.id.toString() }
                and c.sq_ficha = ${ sqFicha.toString() }
                and ofv.sq_oficina_ficha is null
                """

        /** /
        println ' '
        println ' '
        println ' '
        println cmdSql
        /**/

        sqlService.execSql(cmdSql).each {
            String nomeTratado = Util.capitalize( it?.no_pessoa )
            if( !nomesAvaliadores.contains( nomeTratado ) && !nomeCoordenadorTaxon.contains( nomeTratado ) ) {
                nomesAvaliadores.push( nomeTratado )
            }
        }
        nomesAvaliadores = nomesAvaliadores.sort()
        List todoMundo = nomeCoordenadorTaxon + nomesAvaliadores
        todoMundo.collect { it ->
           it = it.toString().trim()
        }
        autoria = todoMundo.join('; ').trim() + ( todoMundo ? '.' : '' )
        if( updateFicha ) {
            ficha.dsCitacao = autoria
        }
        return autoria

    }

    /**
     * construir os nomes da equipe técnica
     * @param sqFicha
     * @param updateFicha
     * @return
     */
    String getApoioTecnico( Long sqFicha = null,Boolean updateFicha=false ) {
        List nomesIgnorar = getListaCoodenadorTaxonFicha(sqFicha)?.noPessoa

        // Os autores da ficha não podem estar como Equipe técnica
        Ficha.executeQuery("""select new map(a.dsCitacao as dsCitacao) from Ficha a where a.id = :sqFicha""",[sqFicha:sqFicha]).each{ row ->
            if( row.dsCitacao ){
                Util.stripTags(row.dsCitacao).split(';').collect{
                    String nomeSemHifen = Util.capitalize(it.trim()).replaceAll(/-/,' ')
                    if( ! nomesIgnorar.contains(nomeSemHifen) ){
                        nomesIgnorar.push( nomeSemHifen )
                    }
                }
            }
        }
        DadosApoio papelApoioTecnico = dadosApoioService.getByCodigo( 'TB_PAPEL_OFICINA', 'APOIO_TECNICO' )
        //DadosApoio papelRealtor = dadosApoioService.getByCodigo( 'TB_PAPEL_OFICINA', 'RELATOR' )
        //DadosApoio papelFacilitador = dadosApoioService.getByCodigo( 'TB_PAPEL_OFICINA', 'FACILITADOR' )
        DadosApoio conviteRecusado = dadosApoioService.getByCodigo( 'TB_SITUACAO_CONVITE_PESSOA', 'CONVITE_RECUSADO' )
        // List papeisValidos = [papelApoioTecnico.id, papelRealtor.id, papelFacilitador.id]
        List papeisValidos = [papelApoioTecnico.id]
        List nomes = [ ]
        sqlService.execSql( """SELECT distinct pessoa.no_pessoa from salve.oficina_participante a
                    inner join salve.oficina_ficha_participante b on b.sq_oficina_participante = a.sq_oficina_participante
                    inner join salve.oficina_ficha c on c.sq_oficina_ficha = b.sq_oficina_ficha
                    inner join salve.vw_pessoa pessoa on pessoa.sq_pessoa = a.sq_pessoa
                    where b.sq_papel_participante in ( ${ papeisValidos.join(',') } )
                    and a.sq_situacao <> ${ conviteRecusado.id.toString() }
                    and c.sq_ficha = ${ sqFicha.toString() }
                    and c.sq_oficina_ficha = ( select max(x.sq_oficina) from salve.oficina_ficha)
                    """ ).each {
            String nomesAutoria = Util.capitalize( it?.no_pessoa )
            String nomeSemHifen = nomesAutoria.replaceAll(/-/,' ')
            if( !nomes.contains( nomesAutoria ) && !nomesIgnorar.contains( nomeSemHifen ) ) {
                nomes.push( nomesAutoria )
            }
        }
        if( sqFicha && updateFicha ){
            Ficha ficha = Ficha.get( sqFicha )
            if( ficha ){
                ficha.dsEquipeTecnica =nomes.join('; ') + (nomes?'.':'')
                ficha.save()
            }
        }
        return nomes.join('; ') +  (nomes?'.':'')
    }

    String getColaboradores( Long sqFicha = null,Boolean updateFicha=false ) {
        Ficha ficha
        if (!sqFicha) {
            throw new Exception('Ficha não informada.')
        }

        if (updateFicha) {
            ficha = Ficha.get(sqFicha)
            if (!canModify(ficha.id)) {
                throw new Exception('Sem permissão para alterar a ficha.')
            }
        }
        // nome dos coordenadores de taxon não podem estar na lista de colaboradores
        List nomesIgnorar = getListaCoodenadorTaxonFicha(sqFicha)?.noPessoa?.collect { it.toLowerCase().replaceAll(/-/, ' ') }

        // Os autores da ficha não podem estar na lista de colaboradores
        Ficha.executeQuery("""select new map(a.dsCitacao as dsCitacao) from Ficha a where a.id = :sqFicha""", [sqFicha: sqFicha]).each { row ->
            if (row.dsCitacao) {
                Util.stripTags(row.dsCitacao).split(';').collect {
                    String nomeSemHifen = Util.capitalize(it.trim()).replaceAll(/-/, ' ').toLowerCase()
                    if (!nomesIgnorar.contains(nomeSemHifen)) {
                        nomesIgnorar.push(nomeSemHifen)
                    }
                }
            }
        }
        // ler nomes dos colaboradores das consulta Amplas/Diretas/Revioes nao versionadas
        String cmdSql = """with passo1 as (
                        select ficha.sq_ficha from salve.ficha where sq_ficha = :sqFicha
                    )
                    -- colaboradores com convidados para as oficinas e que aceitaram o convite
                    SELECT pessoa.no_pessoa
                    from passo1
                    inner join salve.oficina_ficha c on c.sq_ficha = passo1.sq_ficha
                    inner join salve.oficina_participante a on a.sq_oficina = c.sq_oficina
                    inner join salve.oficina_ficha_participante b on b.sq_oficina_participante = a.sq_oficina_participante
                    inner join salve.vw_pessoa pessoa on pessoa.sq_pessoa = a.sq_pessoa
                    inner join salve.dados_apoio as papel on papel.sq_dados_apoio = b.sq_papel_participante
                    inner join salve.dados_apoio as situacao on situacao.sq_dados_apoio = a.sq_situacao
                    left join salve.oficina_ficha_versao ofv on ofv.sq_oficina_ficha = c.sq_oficina_ficha
                    where papel.cd_sistema = 'COLABORADOR_AMPLA'
                      and situacao.cd_sistema <> 'CONVITE_RECUSADO'
                      and ofv.sq_oficina_ficha is null

                    union

                    -- ler as colaboracoes na ficha vindas das consultas diretas e amplas e revisao
                    select usr.no_usuario
                    from passo1
                    inner join salve.ciclo_consulta_ficha a on a.sq_ficha = passo1.sq_ficha
                    inner join salve.ficha_colaboracao as c on c.sq_ciclo_consulta_ficha = a.sq_ciclo_consulta_ficha
                    inner join salve.ciclo_consulta as ciclo_consulta on ciclo_consulta.sq_ciclo_consulta = a.sq_ciclo_consulta
                    left join salve.ciclo_consulta_ficha_versao ccfv on ccfv.sq_ciclo_consulta_ficha = a.sq_ciclo_consulta_ficha
                    left outer join salve.dados_apoio as situacao on situacao.sq_dados_apoio = c.sq_situacao
                    left outer join salve.dados_apoio as tipo on tipo.sq_dados_apoio = ciclo_consulta.sq_tipo_consulta
                    left outer join salve.web_usuario as usr on usr.sq_web_usuario = c.sq_web_usuario
                    where situacao.cd_sistema='ACEITA'
                      and ccfv.sq_ciclo_consulta_ficha is NULL

                    union -- ler a colaboracao de planilha de registro

                    select usr.no_usuario
                    from passo1
                    inner join salve.ciclo_consulta_ficha a on a.sq_ficha = passo1.sq_ficha
                    inner join salve.ficha_consulta_anexo anexo on anexo.sq_ciclo_consulta_ficha = a.sq_ciclo_consulta_ficha
                    left outer join salve.ciclo_consulta as ciclo_consulta on ciclo_consulta.sq_ciclo_consulta = a.sq_ciclo_consulta
                    left join salve.ciclo_consulta_ficha_versao ccfv on ccfv.sq_ciclo_consulta_ficha = a.sq_ciclo_consulta_ficha
                    left outer join salve.dados_apoio as tipo on tipo.sq_dados_apoio = ciclo_consulta.sq_tipo_consulta
                    left join salve.web_usuario as usr on usr.sq_web_usuario = anexo.sq_web_usuario
                    left join salve.dados_apoio as situacao on situacao.sq_dados_apoio = anexo.sq_situacao
                    where situacao.cd_sistema = 'ACEITA'

                    union -- colaboração novo ponto

                    select usr.no_usuario
                    from passo1
                             inner join salve.ficha_ocorrencia fo on fo.sq_ficha = passo1.sq_ficha and fo.sq_contexto = 1053
                             inner join salve.ficha_ocorrencia_consulta c on c.sq_ficha_ocorrencia = fo.sq_ficha_ocorrencia
                             left outer join salve.ciclo_consulta_ficha ccf on ccf.sq_ciclo_consulta_ficha = c.sq_ciclo_consulta_ficha
                             left outer join salve.ciclo_consulta_ficha_versao ccfv on ccfv.sq_ciclo_consulta_ficha = ccf.sq_ciclo_consulta_ficha
                             left outer join salve.ciclo_consulta as ciclo_consulta on ciclo_consulta.sq_ciclo_consulta = ccf.sq_ciclo_consulta
                             left outer join salve.dados_apoio as tipo on tipo.sq_dados_apoio = ciclo_consulta.sq_tipo_consulta
                             left outer join salve.dados_apoio as situacao on situacao.sq_dados_apoio = c.sq_situacao
                             left outer join salve.web_usuario as usr on usr.sq_web_usuario = c.sq_web_usuario
                    where ccfv.sq_ficha_versao is null

                    union -- validações concordo/discordo pontos existes

                    select usr.no_usuario

                    from passo1
                             inner join salve.ficha_ocorrencia fo on fo.sq_ficha = passo1.sq_ficha
                             inner join salve.ficha_ocorrencia_validacao fov on fov.sq_ficha_ocorrencia = fo.sq_ficha_ocorrencia
                             left outer join salve.ciclo_consulta_ficha a on a.sq_ciclo_consulta_ficha = fov.sq_ciclo_consulta_ficha
                             left join salve.ciclo_consulta_ficha_versao as ccfv on ccfv.sq_ciclo_consulta_ficha = a.sq_ciclo_consulta_ficha
                             inner join salve.ciclo_consulta as ciclo_consulta on ciclo_consulta.sq_ciclo_consulta = a.sq_ciclo_consulta
                             left outer join salve.dados_apoio as tipo on tipo.sq_dados_apoio = ciclo_consulta.sq_tipo_consulta
                             left outer join salve.web_usuario as usr on usr.sq_web_usuario = fov.sq_web_usuario
                             left outer join salve.dados_apoio as situacao on situacao.sq_dados_apoio = fov.sq_situacao
                    where fov.sq_ficha_ocorrencia is not null
                        and ccfv.sq_ficha_versao is null

                    union -- validações concordo/discordo pontos existes do portalbio

                    select usr.no_usuario
                    from passo1
                             inner join salve.ficha_ocorrencia_portalbio fo on fo.sq_ficha = passo1.sq_ficha
                             inner join salve.ficha_ocorrencia_validacao fov on fov.sq_ficha_ocorrencia_portalbio = fo.sq_ficha_ocorrencia_portalbio
                             left outer join salve.ciclo_consulta_ficha a on a.sq_ciclo_consulta_ficha = fov.sq_ciclo_consulta_ficha
                             left join salve.ciclo_consulta_ficha_versao as ccfv on ccfv.sq_ciclo_consulta_ficha = a.sq_ciclo_consulta_ficha
                             inner join salve.ciclo_consulta as ciclo_consulta on ciclo_consulta.sq_ciclo_consulta = a.sq_ciclo_consulta
                             left outer join salve.dados_apoio as tipo on tipo.sq_dados_apoio = ciclo_consulta.sq_tipo_consulta
                             left outer join salve.web_usuario as usr on usr.sq_web_usuario = fov.sq_web_usuario
                             left outer join salve.dados_apoio as situacao on situacao.sq_dados_apoio = fov.sq_situacao
                    where fov.sq_ficha_ocorrencia_portalbio is not null
                        and ccfv.sq_ficha_versao is null
                    """

        /** /
         println ' '
         println ' '
         println cmdSql
         /**/

        List nomes = []
        sqlService.execSql(cmdSql,[sqFicha:sqFicha]).each {
            String nomeTratado = Util.capitalize(it?.no_pessoa)
            String nomeSemHifen = nomeTratado.replaceAll(/-/, ' ').toLowerCase()
            if (!nomes.contains(nomeTratado) && !nomesIgnorar.contains(nomeSemHifen)) {
                nomes.push(nomeTratado)
            }
        }
        nomes = nomes ? nomes.sort() : []
        String colaboradores = nomes.join('; ') + (nomes ? '.' : '')
        if (ficha) {
            if (colaboradores) {
                ficha.dsColaboradores = colaboradores
            } else {
                ficha.dsColaboradores = null
            }
            ficha.save()
        }
        return colaboradores
    }

    /**
     * método para verificar se gerar a referencia bibliografica dos registros do portabio utilizados na avaliação quando não exisitir
     * @param sqFichaOcorrenciaPortalbio
     */
    void gerarReferenciaBibliografica( Long sqFicha = null, Long sqFichaOcorrenciaPortalbio = null, String noBaseDados = '' ) {
        String cmdSql
        List rowOcorrencia

        try {
            noBaseDados = noBaseDados ? noBaseDados.toUpperCase() : ''

            /** /
             println ' '
             println '- NOVA FUNCIONALIDADE - GERAR O REF BIB DO REGISTRO DE ACORDO COM A BASE DE DADOS E AUTOR'
             println '  Id Ficha: '+sqFicha.toString()
             println '  Bd: '+ noBaseDados
             println '  Id: '+sqFichaOcorrenciaPortalbio.toString()
             /**/

            // verificar se já possui ref bib ligada ao registro de ocorrencia
            cmdSql = """select sq_ficha_ref_bib from salve.ficha_ref_bib
            where sq_ficha = ${sqFicha.toString()}
            and no_tabela = 'ficha_ocorrencia_portalbio'
            and no_coluna = 'sq_ficha_ocorrencia_portalbio'
            and sq_registro = ${sqFichaOcorrenciaPortalbio.toString()}
            """

            List fichaRefBib = sqlService.execSql(cmdSql)
            if (fichaRefBib) {
// println 'JÁ EXISTE UMA REFERENCIA'
                return
            }

            // Titulo da base de dados corporativo
            String deTitulo = ''
            boolean gerarReferencia=true // se for do ARA não gerar as referências automáticas
            if(noBaseDados) {
                if (noBaseDados =~ /\/SISBIO/) {
                    deTitulo = 'Sistema de Autorização de Informação em Biodiversidade - SISBIO'
                } else if (noBaseDados =~ /\/ARA/) {
                    deTitulo = 'Atlas de Registros de Aves - ARA'
                    gerarReferencia=false
                } else if (noBaseDados =~ /\/SISQUELONIOS/) {
                    deTitulo = 'Sistema de Gestão e Informação dos Quelônios Amazônicos - SISQUELONIOS'
                } else if (noBaseDados =~ /\/SITAMAR/) {
                    deTitulo = 'Sistema de Informação sobre Tartarugas Marinhas – SITAMAR'
                } else if (noBaseDados =~ /\/SIMMMAM/) {
                    deTitulo = 'Sistema de Apoio ao Monitoramento de Mamíferos Marinhos– SIMMAM'
                } else if (noBaseDados =~ /\/SIMMMAM/) {
                    deTitulo = 'Sistema de Apoio ao Monitoramento de Mamíferos Marinhos– SIMMAM'
                } else if (noBaseDados =~ /\/SNA/) {
                    deTitulo = 'Sistema Nacional de Anilhamento - SNA'
                } else if (noBaseDados =~ /\/MONITORA/) {
                    deTitulo = 'Sistema de Gestão de Dados de Biodiversidade do Programa Nacional de Mnitoramento da Biodiversidade - SISMONITORA'
                }
            }

            if (!deTitulo) {
                // tentar encontrar o nome da base de dados do campo tx_ocorrencia
                cmdSql = """select tx_ocorrencia::jsonb->>'dataResourceName'::text as noColecao
                    ,tx_ocorrencia::jsonb->>'raw_collectionCode'::text as coColecao
                    from salve.ficha_ocorrencia_portalbio a
                    where a.sq_ficha_ocorrencia_portalbio = ${sqFichaOcorrenciaPortalbio}
                    """
                rowOcorrencia = sqlService.execSql(cmdSql)
                if( rowOcorrencia ) {
//println 'coColecao1: ' + rowOcorrencia[0].coColecao

                    if ((rowOcorrencia[0].noColecao =~ /(?i)Registro de Aves/) ||
                        (rowOcorrencia[0].coColecao =~ /(?i)Registros de Aves/)) {
                        deTitulo = 'Atlas de Registros de Aves - ARA'
                        gerarReferencia=false

                    } else if ((rowOcorrencia[0].noColecao =~ /(?i)Monitoramento da Biodiversidade/) ||
                        (rowOcorrencia[0].coColecao =~ /(?i)Monitoramento-DIBIO/)) {
                        deTitulo = 'Monitoramento da Biodiversidade em Unidades de Conservação Federais'

                    } else if ((rowOcorrencia[0].noColecao =~ /(?i)Anilhamento de Aves/) ||
                        (rowOcorrencia[0].coColecao =~ /(?i)SNA-CEMAVE/)) {
                        deTitulo = 'Sistema Nacional de Anilhamento de Aves Silvestres - SNA'

                    } else if ((rowOcorrencia[0].noColecao =~ /(?i)Sistema de Autorização/) ||
                        (rowOcorrencia[0].coColecao =~ /(?i)SISBIO/)) {
                        deTitulo = 'Sistema de Autorização de Informação em Biodiversidade - SISBIO'

                    } else if ((rowOcorrencia[0].noColecao =~ /(?i)Tartarugas Marinhas/) ||
                        (rowOcorrencia[0].coColecao =~ /(?i)SITAMAR/)) {
                        deTitulo = 'Sistema de Informação Sobre Tartarugas Marinhas - SITAMAR'

                    } else if ((rowOcorrencia[0].noColecao =~ /(?i)Mamíferos Marinhos/) ||
                        (rowOcorrencia.coColecao =~ /(?i)SIMMAM/)) {
                        deTitulo = 'Sistema de Apoio ao Monitoramento de Mamíferos Marinhos - SIMMAM'
                    }
                }
            }

            // localizar base de dados pelo campo no_instituicao
            if( !deTitulo ) {
                cmdSql = """select a.no_instituicao as noInstituicao
                    from salve.ficha_ocorrencia_portalbio a
                    where a.sq_ficha_ocorrencia_portalbio = ${sqFichaOcorrenciaPortalbio}
                    """
                rowOcorrencia = sqlService.execSql(cmdSql)
                if (rowOcorrencia) {
                    // println 'noInstituicao: ' + rowOcorrencia[0].noInstituicao

                    if (rowOcorrencia[0].noInstituicao =~ /\/SISBIO/) {
                        deTitulo = 'Sistema de Autorização de Informação em Biodiversidade - SISBIO'
                    } else if (rowOcorrencia[0].noInstituicao =~ /\/ARA/) {
                        deTitulo = 'Atlas de Registros de Aves - ARA'
                        gerarReferencia=false
                    } else if (rowOcorrencia[0].noInstituicao =~ /\/SISQUELONIOS/) {
                        deTitulo = 'Sistema de Gestão e Informação dos Quelônios Amazônicos - SISQUELONIOS'
                    } else if (rowOcorrencia[0].noInstituicao =~ /\/SITAMAR/) {
                        deTitulo = 'Sistema de Informação sobre Tartarugas Marinhas – SITAMAR'
                    } else if ((rowOcorrencia[0].noInstituicao =~ /(?i)Anilhamento de Aves/) ||
                        (rowOcorrencia[0].noInstituicao =~ /(?i)SNA-CEMAVE/)) {
                        deTitulo = 'Sistema Nacional de Anilhamento de Aves Silvestres - SNA'
                    } else if ((rowOcorrencia[0].noInstituicao =~ /(?i)Monitoramento da Biodiversidade/) ||
                        (rowOcorrencia[0].noInstituicao =~ /(?i)Monitoramento-DIBIO/)) {
                        deTitulo = 'Monitoramento da Biodiversidade em Unidades de Conservação Federais'
                    }
                }
            }

            if (!deTitulo) {
                // tentar encontrar o nome da base de dados do campo codigoColecao
                cmdSql = """select tx_ocorrencia::jsonb->>'codigoColecao'::text as coColecao
                    from salve.ficha_ocorrencia_portalbio a
                    where a.sq_ficha_ocorrencia_portalbio = ${sqFichaOcorrenciaPortalbio}
                    """
                rowOcorrencia = sqlService.execSql(cmdSql)
                if( rowOcorrencia ) {
                    // println 'Localizando pelo codigoColecao: ' + rowOcorrencia.coColecao[0]
//println 'coColecao2: ' + rowOcorrencia[0].coColecao
                    if ((rowOcorrencia[0].coColecao =~ /(?i)Registro de Aves/) ||
                        (rowOcorrencia[0].coColecao =~ /(?i)^ARA - /)) {
                        deTitulo = 'Atlas de Registros de Aves - ARA'
                        gerarReferencia=false

                    } else if ((rowOcorrencia[0].coColecao =~ /(?i)Monitoramento da Biodiversidade/) ||
                        (rowOcorrencia[0].coColecao =~ /(?i)Monitoramento-DIBIO/)) {
                        deTitulo = 'Monitoramento da Biodiversidade em Unidades de Conservação Federais'

                    } else if ((rowOcorrencia[0].coColecao =~ /(?i)Anilhamento de Aves/) ||
                        (rowOcorrencia[0].coColecao =~ /(?i)SNA-CEMAVE/)) {
                        deTitulo = 'Sistema Nacional de Anilhamento de Aves Silvestres - SNA'

                    } else if ((rowOcorrencia[0].coColecao =~ /(?i)Sistema de Autorização/) ||
                        (rowOcorrencia[0].coColecao =~ /(?i)SISBIO/)) {
                        deTitulo = 'Sistema de Autorização de Informação em Biodiversidade - SISBIO'

                    } else if ((rowOcorrencia[0].coColecao =~ /(?i)Tartarugas Marinhas/) ||
                        (rowOcorrencia[0].coColecao =~ /(?i)SITAMAR/)) {
                        deTitulo = 'Sistema de Informação Sobre Tartarugas Marinhas - SITAMAR'

                    } else if ((rowOcorrencia[0].coColecao =~ /(?i)Mamíferos Marinhos/) ||
                        (rowOcorrencia[0].coColecao =~ /(?i)SIMMAM/)) {
                        deTitulo = 'Sistema de Apoio ao Monitoramento de Mamíferos Marinhos - SIMMAM'
                    }
                }
            }

            if( ! deTitulo || !gerarReferencia  ){
                // println 'BASE DE DADOS NAO IDENTIFICADA'
                //println 'Titulo:' + deTitulo
                //println 'Gerar Referencia: ' + gerarReferencia
                return
            }

// println 'Identificado: ' + deTitulo
//return

            // Localizar a publicação
            TipoPublicacao tipoBd = TipoPublicacao.findByCoTipoPublicacao('BANCO_DADOS_INSTITUCIONAL')
            cmdSql = """select a.no_autor
                        , a.dt_ocorrencia
                        , a.no_base_dados
                        from salve.ficha_ocorrencia_portalbio a
                        where a.sq_ficha_ocorrencia_portalbio = ${sqFichaOcorrenciaPortalbio.toString()}"""
            List listOcorrencias = sqlService.execSql(cmdSql)

            listOcorrencias.each { row ->

                String noAutor = Util.nome2autor(row.no_autor)

                // println ' '
                // println 'Base de dados:' + deTitulo
                // println 'Autor:'+noAutor

                // le os nomes dos coletores no campo tx_ocorrencia
                cmdSql = """select tx_ocorrencia::jsonb->>'collector' as coletores
                    from salve.ficha_ocorrencia_portalbio a
                    where a.sq_ficha_ocorrencia_portalbio = ${sqFichaOcorrenciaPortalbio}
                    """
                // //println cmdSql
                rowOcorrencia = sqlService.execSql(cmdSql)

                if (rowOcorrencia && rowOcorrencia[0].coletores && rowOcorrencia[0].coletores.toString().toLowerCase() != 'null') {
                    noAutor = rowOcorrencia[0].coletores.toString()
                    if( ! ( noAutor =~ /;/) ) {
                        // nome único
                        noAutor = Util.nome2autor(noAutor)
                    } else {
                        // vários nomes
                        List nomes=[]
                        rowOcorrencia[0].coletores.toString().split(';').each {  autor ->
                            nomes.push( Util.nome2autor( autor ) )
                        }
                        noAutor = nomes.join('\n')
                    }
                }

                // evitar nome do autor com somente numeros
                if( noAutor.replaceAll(/[0-9]/,'').trim() == '' ) {
                    noAutor = deTitulo
                }

                if( row.dt_ocorrencia ) {
                    Integer anoPublicacao = row.dt_ocorrencia.format('yyyy').toInteger()
                    if (noAutor && anoPublicacao) {
                        /** /
                         println 'Titulo : ' + deTitulo
                         println 'Ano    : ' + anoPublicacao
                         println 'Autor  : ' + noAutor
                         println '-'*50
                         /**/

                        Publicacao publicacao = Publicacao.findByTipoPublicacaoAndDeTituloAndNuAnoPublicacaoAndNoAutor(tipoBd, deTitulo, anoPublicacao, noAutor)
                        if (!publicacao) {
                            publicacao = new Publicacao()
                            publicacao.tipoPublicacao = tipoBd
                            publicacao.nuAnoPublicacao = anoPublicacao
                            publicacao.deTitulo = deTitulo
                            publicacao.noAutor = noAutor
                            publicacao.inListaOficialVigente = 'N'
                            publicacao.save( flush: true )
                        }
                        if (publicacao) {
                            FichaRefBib frb = new FichaRefBib()
                            frb.ficha = Ficha.get(sqFicha)
                            frb.publicacao = publicacao
                            frb.noTabela = 'ficha_ocorrencia_portalbio'
                            frb.noColuna = 'sq_ficha_ocorrencia_portalbio'
                            frb.sqRegistro = sqFichaOcorrenciaPortalbio
                            frb.deRotulo = 'Ocorrência'
                            frb.save(flush: true)
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            println ' '
            println 'Erro FichaService - gerarReferenciaBibliografica()'
            println 'Id Ficha: ' + (sqFicha ?:'' )
            println 'Bd: '+ (noBaseDados?:'')
            println 'Id fichaOcorrenciaPortalbio: ' + ( sqFichaOcorrenciaPortalbio ?: '' )
            println e.getMessage()
        }
    }

    /**
     * método para gerar o "Como Citar" da ficha da espécie
     * @param ficha
     * @return
     */
    String comoCitarFicha( Long sqFicha=0, String  anoAvaliacao='',Boolean etAl = true ) {
        return comoCitarFicha( Ficha.get( sqFicha),anoAvaliacao, etAl )
    }
    String comoCitarFicha( Ficha ficha, String anoCitacao='', Boolean etAl = true ) {
        if( !ficha ) {
            return ''
        }
        String noCientifico = null
        String autores = null
        try {
            if (ficha.situacaoFicha.codigoSistema == 'PUBLICADA') {
                autores = ficha.dsCitacao.toString()
                noCientifico = ficha.taxon.noCientificoItalico
                try {
                    anoCitacao = ficha.dtPublicacao.format('yyyy')
                } catch( Exception e ) {
                    println e.getMessage()
                    anoCitacao=null
                }
                if( ! anoCitacao ){
                    Map dadosUltimaAvaliacao = getUltimaAvaliacaoNacional(ficha)
                    anoCitacao = dadosUltimaAvaliacao.mesAnoUltimaAvaliacao ? dadosUltimaAvaliacao.mesAnoUltimaAvaliacao.toString().reverse().substring(0,4).reverse() : ''
                }
          } else {
                anoCitacao = anoCitacao ?: new Date().format("yyyy")
            }
        } catch( Exception e ){
            println ' '
            println 'ERRO comoCitarFicha ' + ficha.id+' - ' + ficha.nmCientifico
            println e.getMessage()
        }
        return Util.getComoCitar(noCientifico, autores, anoCitacao, etAl)
    }

    /**
     * Listar as referencias bibliograficas utilizadas na ficha e nos registros de correncia
     * @param sqFicha
     * @return
     */
    List getFichaRefBibs(Ficha ficha){
        return getFichaRefBibs( ficha.id )
    }
    List getFichaRefBibs(Long sqFicha) {
        List rows = executeSql( "select * from salve.fn_ficha_ref_bibs(:sqFicha)",[sqFicha:sqFicha])
        // fazer processamento do resultado
        // 1) adicionar tags no ano
        rows.each {row->
            // ler as tags verificando se existe tag de uma letra
            String tag = ''
            if( row.no_tags ) {
                row.no_tags.split(',').sort().each {
                    if ((it =~ /^[a-z]{1}$/) && !tag) {
                        tag = it
                    }
                }
                // injetar a tag (a,b,c...) na frente do ano
                if( tag && row.de_ref_bibliografica ) {
                    row.de_ref_bibliografica = row.de_ref_bibliografica.toString().replaceFirst(/([0-9]{4})/, '$1' + tag)
                }
            }
        }
        return rows
    }

    void gravarHistoricoSituacaoExcluida( Long sqFicha , String dsJustificativa, Long sqUsuarioLogado ) {
        try {
            dsJustificativa = Util.stripTags( Util.trimEditor( dsJustificativa ) )
            // localizar o último histórico e verificar se houve alteração
            List rows = sqlService.execSql("select ds_justificativa from " +
                "salve.ficha_hist_situacao_excluida where sq_ficha = ${sqFicha.toString()} " +
                "order by dt_inclusao desc limit 1")
            if( !rows || rows[0].ds_justificativa != dsJustificativa ){
                sqlService.execSql("""insert into salve.ficha_hist_situacao_excluida (sq_ficha,sq_usuario_inclusao,ds_justificativa) values ( :sqFicha, :sqPessoa, :dsJustificativa )"""
                ,[  sqFicha         : sqFicha
                  , sqPessoa        : sqUsuarioLogado
                  , dsJustificativa : dsJustificativa
                ])
            }
        } catch( Exception e ){
            Util.printLog('SALVE - fichaService()','Erro ao gravar o historico da situacao excluida da ficha.',e.getMessage())
        }
    }

    void excluirHistoricoSituacaoExcluida( Long sqFicha , String dsJustificativa ) {
        try {
            dsJustificativa = Util.stripTags( Util.trimEditor( dsJustificativa ) )
            // localizar o último histórico e se for o da exclusao e a mesma justificativa então excluir o historico
            List rows = sqlService.execSql("select sq_ficha_hist_situacao_excluida, ds_justificativa from " +
                "salve.ficha_hist_situacao_excluida where sq_ficha = ${sqFicha.toString()} " +
                "order by dt_inclusao desc limit 1")
            if( rows && rows[0].ds_justificativa == dsJustificativa ){
                sqlService.execSql("delete from salve.ficha_hist_situacao_excluida where sq_ficha_hist_situacao_excluida = ${rows[0].sq_ficha_hist_situacao_excluida}")
            }
        } catch( Exception e ){
            Util.printLog('SALVE - fichaService()','Erro ao excluir  o historico da situacao excluida da ficha.',e.getMessage())
        }
    }

    /**
     * método para marcar/desmarca as fichas para exibição no módulo público
     * @param sqFichasCsv - ids das fichas separados por virgula
     * @param stExibir
     * @param sqPessoa
     */
    void marcarDesmarcarRegistroExibirModuloPublico( String sqFichasCsv, Boolean stExibir, Long sqPessoa) {
        //String cmdSql = """select sq_ficha from salve.ficha_modulo_publico where sq_ficha in (${sqFichasCsv})"""
        String cmdSql = """select sq_ficha from salve.ficha_modulo_publico where sq_ficha = any( array[ ${sqFichasCsv} ] )"""
        List rows = sqlService.execSql( cmdSql )
        List sqlsExecutar = []
        sqFichasCsv.split(',').each { sq_ficha ->
            if( ! rows.sq_ficha.contains( sq_ficha.toLong() ) ) {
                sqlsExecutar.push("insert into salve.ficha_modulo_publico (sq_ficha,sq_pessoa,st_exibir) values (${sq_ficha}, ${sqPessoa?:'null'}, ${stExibir})")
            } else {
                sqlsExecutar.push("update salve.ficha_modulo_publico set st_exibir = ${stExibir},sq_pessoa = ${sqPessoa?:'null'} where sq_ficha = ${sq_ficha} and st_exibir <> ${stExibir}")
            }
            if( sqlsExecutar.size()>100){
                sqlService.execSql( sqlsExecutar.join(';\n') )
                sqlsExecutar=[]
            }
        }
        if( sqlsExecutar ){
            sqlService.execSql( sqlsExecutar.join(';\n') )

        }
    }

/**
 * método para remover a pendência automática gerada na integração do SALVE
 * com a API do speciesplus quando a espécie estiver em mais de uma lista
 * cites e o usuário excluir a errada
 * @param sqFicha
 */
    void removerPendenciaDuplicidadeCites(Long sqFicha= 0 ){
        asyncService.removerPendenciaDuplicidadeCites(sqFicha)
    }

    /**
     * método para criar uma versão da ficha e o contexto do versionamento
     */
    void versionarFicha( Long sqFicha = 0l, Long sqPessoa = 0l, String contexto=''){
        if( !sqFicha || !contexto || !sqPessoa ){
            throw new Exception('Para realizar o versionamento da ficha é necessário informar o id, usuário e o contexto')
        }
        DadosApoio contextoVersao = dadosApoioService.getByCodigo('TB_CONTEXTO_VERSAO_FICHA',contexto)
        if( !contextoVersao ){
            throw new Exception('Contexto de versionamento ficha '+contexto+' não cadastrado na tabela TB_CONTEXTO_VERSAO_FICHA')
        }
        //println "select salve.fn_versionar_ficha(${sqFicha},${sqPessoa},${contextoVersao.id})::text"
        Map res = sqlService.execute("salve.fn_versionar_ficha(${sqFicha},${sqPessoa},${contextoVersao.id})")
        /** /
        println ' '
        println ' '
        println ' '
        println 'RESULTADO VERSIONAMENTO'
        println res
        /**/
        JSONObject json = JSON.parse( res.data )
        String logVersionamento = ''
        if( json.status == 1 ){
            logVersionamento = json.msg
            if( json.data.msg ){
                logVersionamento += '\n'+json.data.msg + '\n'
            }
            logVersionamento += 'LOG'
            logVersionamento += json.data.log
        } else {
            if( json.msg ){
                logVersionamento = '\n'+json.msg
            }
        }
        if( logVersionamento ) {
            // registrar o log do versionamento
            FichaVersionamento fv = FichaVersionamento.get(sqFicha)
            if (!fv) {
                fv = new FichaVersionamento(ficha: Ficha.get(sqFicha))
            }
            fv.inStatus = json.status.toInteger()
            fv.dsLog = Util.trimEditor(logVersionamento)
            fv.save(flush: true)
        }
    }


    /**
     * consulta para criação do gride da aba versões
     */

                /*
                -- colaboracoes nas consultas
                left join lateral (
                with colabs as (
                SELECT versao.sq_ficha_versao, usr.no_usuario, usr.de_email
                        FROM salve.ciclo_consulta_ficha_versao versao
                            INNER JOIN salve.ficha_colaboracao ON ficha_colaboracao.sq_ciclo_consulta_ficha = versao.sq_ciclo_consulta_ficha
                            INNER JOIN salve.ciclo_consulta_ficha ON ciclo_consulta_ficha.sq_ciclo_consulta_ficha = ficha_colaboracao.sq_ciclo_consulta_ficha
                            INNER JOIN salve.web_usuario usr ON usr.sq_web_usuario = ficha_colaboracao.sq_web_usuario
                            INNER JOIN salve.dados_apoio situacao ON situacao.sq_dados_apoio = ficha_colaboracao.sq_situacao
                        WHERE versao.sq_ficha_versao=fv.sq_ficha_versao
                          AND situacao.cd_sistema IN ('ACEITA', 'PARCIAL')
                        UNION
                        SELECT versao.sq_ficha_versao,usr.no_usuario, usr.de_email
                        FROM salve.ficha_ocorrencia_consulta
                                 INNER JOIN salve.ciclo_consulta_ficha ON ciclo_consulta_ficha.sq_ciclo_consulta_ficha = ficha_ocorrencia_consulta.sq_ciclo_consulta_ficha
                                 INNER JOIN salve.ficha_ocorrencia fo ON fo.sq_ficha_ocorrencia = ficha_ocorrencia_consulta.sq_ficha_ocorrencia
                                 INNER JOIN salve.dados_apoio situacao ON situacao.sq_dados_apoio = fo.sq_situacao
                                 INNER JOIN salve.web_usuario usr ON usr.sq_web_usuario = ficha_ocorrencia_consulta.sq_web_usuario
                                 INNER JOIN salve.ciclo_consulta_ficha_versao versao ON versao.sq_ciclo_consulta_ficha = ficha_ocorrencia_consulta.sq_ciclo_consulta_ficha
                        WHERE versao.sq_ficha_versao = fv.sq_ficha_versao
                          AND situacao.cd_sistema IN ( 'ACEITA', 'PARCIAL')
                )
                select json_agg( json_build_object('no_usuario',colabs.no_usuario,'de_email',colabs.de_email))::text as js_colaboracoes from colabs
                where colabs.sq_ficha_versao = fv.sq_ficha_versao
                ) as colaboracoes on true
                    */

    List getRowsGridVersoes(Long sqFicha  = 0l){
        String cmdSql = """select fv.sq_ficha_versao
                          ,fv.sq_ficha
                          ,fv.dt_inclusao
                          ,fv.nu_versao
                          ,fv.sq_contexto
                          ,to_char(fv.dt_inclusao, 'dd/mm/yyyy HH24:mi:ss') as dt_inclusao
                          ,fv.sq_ficha_versao
                          ,fv.sq_ficha
                          ,fv.dt_inclusao
                          ,fv.nu_versao
                          ,fv.sq_contexto
                          ,to_char(fv.dt_inclusao, 'dd/mm/yyyy HH24:mi:ss') as dt_inclusao
                          ,pf.no_pessoa
                          ,fv.st_publico
                          ,contexto.cd_sistema as cd_contexto
                          ,fv.js_ficha->>'sg_unidade_responsavel' as sg_unidade_responsavel
                          ,fv.js_ficha->>'funcoes' as js_funcoes
                          ,oficinas.js_oficinas
                          ,papeis.js_papeis
                          ,fv.js_ficha->'avaliacao'->'resultado_avaliacao'->>'ds_colaboradores' as ds_colaboradores
                    from salve.ficha_versao fv
                     inner join salve.vw_pessoa_fisica pf on pf.sq_pessoa = fv.sq_usuario_inclusao
                     inner join salve.dados_apoio as contexto on contexto.sq_dados_apoio = fv.sq_contexto
                        -- oficinas
                    left join lateral (
                        SELECT JSON_AGG(json_build_object(
                              'cd_tipo_sistema',tipo_oficina.cd_sistema
                            , 'ds_tipo', tipo_oficina.ds_dados_apoio
                            , 'no_oficina', oficina.no_oficina
                            , 'sg_oficina', oficina.sg_oficina
                            , 'de_local', oficina.de_local
                            , 'ds_periodo', CONCAT(TO_CHAR(oficina.dt_inicio, 'dd/mm/yyyy'), ' a ', TO_CHAR(oficina.dt_fim, 'dd/mm/yyyy'))
                            ))::text as js_oficinas
                        FROM salve.oficina_ficha_versao ofv
                                 LEFT OUTER JOIN salve.oficina_ficha ON ofv.sq_oficina_ficha = oficina_ficha.sq_oficina_ficha
                                 LEFT OUTER JOIN salve.oficina ON oficina.sq_oficina = oficina_ficha.sq_oficina
                                 LEFT OUTER JOIN salve.dados_apoio AS tipo_oficina ON tipo_oficina.sq_dados_apoio = oficina.sq_tipo_oficina
                        WHERE ofv.sq_ficha_versao = fv.sq_ficha_versao
                    ) as oficinas on true

                    left join lateral (
                    select distinct json_agg( json_build_object(
                        'no_pessoa',pf.no_pessoa,
                        'cd_papel_sistema',papel.cd_sistema,
                        'ds_papel',papel.ds_dados_apoio
                        ))::text as js_papeis
                        FROM salve.oficina_ficha_versao ofv
                         inner JOIN salve.oficina_ficha ON ofv.sq_oficina_ficha = oficina_ficha.sq_oficina_ficha
                         inner JOIN salve.oficina_participante op on op.sq_oficina = oficina_ficha.sq_oficina
                         inner join salve.vw_pessoa_fisica pf on pf.sq_pessoa = op.sq_pessoa
                         inner join salve.oficina_ficha_participante ofp on ofp.sq_oficina_participante = op.sq_oficina_participante
                         inner join salve.dados_apoio as papel on papel.sq_dados_apoio = ofp.sq_papel_participante
                         WHERE ofv.sq_ficha_versao = fv.sq_ficha_versao
                    ) as papeis on true
                    where fv.sq_ficha = :sqFicha;
            """

        /** /
        println ' '
        println ' '
        println cmdSql
        /**/

        List rows = sqlService.execSql(cmdSql,[sqFicha:sqFicha])
        return rows.sort{a,b -> b.nu_versao<=>a.nu_versao}
    }
}
