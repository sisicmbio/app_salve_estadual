package br.gov.icmbio

import com.vividsolutions.jts.geom.Geometry
import com.vividsolutions.jts.io.WKTReader
import grails.converters.JSON
import grails.transaction.Transactional
import org.eclipse.jdt.internal.compiler.env.INameEnvironment

@Transactional
class CorpEstruturaOrgService {

    def dadosApoioService

    /**
     * método para gravar as informações da Estrutura Organizacional no banco de dados
     * @return
     */
    Map save( Long sqPai = null, Long sqPessoa = null, String noPessoa = null, String sgInstituicao = null, Boolean stInterna = null, String noContato = null, String nuTelefone = null, String deEmail = null, String nuCnpj = null) {
        if( !noPessoa ){
            throw new Exception('Nome da Unidade organizacional não informado')
        }

        if( !sgInstituicao ){
            throw new Exception('Sigla da Unidade organizacional não informado')
        }

        if ( nuCnpj && ! Util.isCnpj(nuCnpj)){
            throw new Exception('Cnpj inválido')
        }

        Instituicao instituicao
        if( sqPessoa ){
            instituicao = Instituicao.get( sqPessoa )
            if( !instituicao ){
                throw new Exception('Id da Unidade organizacional inexistente')
            }
        }
        else
        {
            instituicao = new Instituicao()
            instituicao.pessoa = new Pessoa()
            DadosApoio tipoUnidadeOrg = dadosApoioService.getByCodigo('TB_TIPO_INSTITUICAO','UNIDADE_ORGANIZACIONAL')
            instituicao.tipo = tipoUnidadeOrg
        }
        instituicao.pessoa.noPessoa = noPessoa
        instituicao.pessoa.deEmail = deEmail ?: null
        instituicao.sgInstituicao = sgInstituicao ?: null
        instituicao.stInterna = stInterna
        instituicao.noContato = noContato
        instituicao.nuTelefone = nuTelefone
        instituicao.nuCnpj = nuCnpj ? nuCnpj.replaceAll(/[^0-9]/,'') : null
        if( sqPai ){
            instituicao.pai = Instituicao.get( sqPai )
            if( !instituicao.pai ){
                throw new Exception('Id da Unidade Superior inexistente')
            }
        } else {
            instituicao.pai = null
        }
        instituicao.pessoa.save(flush: true)
        instituicao.save(flush: true);
        return instituicao.asMap() // retornar os dados da unidade organizacional gravada para atualizar o gride
    }

    /**
     * método para recuperar as informações da Unidade organizacional para alteração
     * @param sqPessoa
     * @return
     */
    def edit(Long sqPessoa=0l) {
        if( !sqPessoa ){
            throw new Exception('Id da Unidade organizacional não informado')
        }

        Instituicao instituicao
        instituicao = Instituicao.get( sqPessoa )
        if( !instituicao ){
            throw new Exception('Id da Unidade organizacional inexistente')
        }
        return instituicao.asMap()
    }


    /**
     * método para excluir a Unidade Organizacional do banco de dados
     * @return
     */
    def delete(Long sqPessoa = 0l) {
        Instituicao instituicao = Instituicao.get( sqPessoa )
        if( !instituicao ) {
            throw new Exception('Id da Unidade organizacional não informado')
        }
        int qtdSubordinadas =  Instituicao.createCriteria().count{
            pai {
                eq('id',sqPessoa)
            }
        }
        if( qtdSubordinadas ) {
            throw new Exception("Unidade possui ${qtdSubordinadas} unidade(s) subordinada(s) a ela.")
        }
        if( instituicao.delete() ) {
            Pessoa.get( sqPessoa ).delete()
        }
    }

    /**
     * listagem da Estrutura organizacional
     * @param page
     * @param pageSize
     * @param q
     * @return
     */
    List list( Integer page=1, Integer pageSize=100, String q = '') {

        Integer skipRecords = ( pageSize * ( page - 1 ) )
        return Instituicao.createCriteria().list {
            if( q ){
                ilike('noInstituicao','%'+q.trim()+'%')
            }
            maxResults( pageSize )
            firstResult( skipRecords )
            order("noInstituicao", "asc")
        }
    }


    /**
     * método utilizado para montagem da treeview da estrutura organizacional
     * @param sqPai
     * @return
     */
    List treeview( Long sqPai = null ){
        List instituicoes
        if ( sqPai) {
            instituicoes = Instituicao.findAllByPai( Instituicao.get(sqPai) )
        } else {
            instituicoes = Instituicao.findAllByPaiIsNull()
        }
        List result = []
        instituicoes.sort{it.pessoa.noPessoa }.each {
            Map row = [  'key'          : it.id
                      , 'title'        : it.pessoa.noPessoa
                      , 'expanded'     : false
                      , 'folder'       : (it.pai ? false: true)
                      , 'lazy'         : (it.numFilhos > 0 ? true : false)
                      , 'sgInstituicao': it.sgInstituicao
                      , 'noContato'    : it.noContato ?: ''
                      , 'nuTelefone'   : it.nuTelefone ?: ''
                      , 'deEmail'      : it.pessoa.deEmail ?: ''
            ]
            result.push( row )
        }
        return result ?: null
    }

    int totalRecords(){

    }

}
