package br.gov.icmbio

import com.vividsolutions.jts.geom.Geometry
import com.vividsolutions.jts.io.WKTReader
import grails.converters.JSON
import grails.transaction.Transactional

@Transactional
class CorpPaisService {

    def sqlService

    /**
     * método para gravar as informações do país no banco de dados
     * @param sqPais
     * @param noPais
     * @param wkt
     * @return
     */
    def save(Long sqPais = null, String noPais= '', String coPais = '',  String wkt = '') {
        if( !noPais ){
            throw new Exception('Nome do país não informado')
        }
        if( !coPais ){
            throw new Exception('Código ISO do país não informado. Ex: BRA, ALA, ESP etc')
        }

        Pais pais
        if( sqPais ){
            pais = Pais.get( sqPais )
            if( !pais ){
                throw new Exception('Id do país inexistente')
            }
        } else {
            pais = new Pais()
        }

        if( wkt ) {
            WKTReader wktReader = new WKTReader();
            Geometry g = wktReader.read( wkt )
            pais.geometry = g
            if (pais.geometry) {
                pais.geometry.SRID = 4674
            }
        } else {
            pais.geometry=null
        }
        pais.coPais = coPais.toUpperCase()
        pais.noPais = noPais
        pais.save()
    }


    /**
     * método para recuperar as informações do país para alteração
     * @param sqPais
     * @return
     */
    def edit(Long sqPais=0l) {
        if( !sqPais ){
            throw new Exception('Id do país não informado')
        }

        Pais pais
        pais = Pais.get( sqPais )
        if( !pais ){
            throw new Exception('Id do país inexistente')
        }

        Map result = [ 'sqPais'    : pais.id
                       , 'noPais'  : pais.noPais
                       , 'coPais'   : pais.coPais
                       , 'geojson'   : [ "type": "FeatureCollection"
                       , 'features'  : []
                       ]
        ]
        String cmdSql = """SELECT ST_AsGeoJSON( ge_pais,13,8) AS geoJson 
                           FROM salve.pais 
                           WHERE sq_pais = :sqPais"""
        sqlService.execSql( cmdSql, [sqPais:sqPais] ).each { row ->
            if( row.geoJson ) {
                result.geojson.features.push( [ 'type': 'Feature'
                                               ,'properties': [],
                                                'geometry': JSON.parse( row.geoJson )

                ])
            }
        }
        return result
    }


    /**
     * método para excluir a região do banco de dados
     * @return
     */
    def delete(Long sqPais = 0l) {
        Pais pais = Pais.get( sqPais )
        if( !pais ) {
            throw new Exception('Id do país não informado')
        }
        pais.delete()
    }
}
