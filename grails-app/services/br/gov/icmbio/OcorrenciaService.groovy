package br.gov.icmbio

import grails.transaction.Transactional

@Transactional
class OcorrenciaService {

    /**
     * Definir a UC automaticamente
     * @return
     */
    FichaOcorrencia saveUc(FichaOcorrencia rec ) {
        return rec
    }

    /**
     * Definir a Uf automaticamente
     * @return
     */
    FichaOcorrencia saveUf( FichaOcorrencia rec ) {
        return rec
    }

    /**
     * Definir o Bioma automaticamente
     * @return
     */
    FichaOcorrencia saveBioma( FichaOcorrencia rec ) {
        return rec
    }

    /**
     * Verificar se o ponto está dentro da uc selecionada
     */
    boolean chkUc( FichaOcorrencia rec )
    {
        return true
    }

    /**
     * Verificar se a coordenada está dentro da UF selecionada
     */
    boolean chkUf( FichaOcorrencia rec)
    {
        return true
    }

    /**
     * Verificar se a coordenada está dentro do município selecionado
     */
    boolean chkMunicipio( FichaOcorrencia rec)
    {
        return true
    }

}
