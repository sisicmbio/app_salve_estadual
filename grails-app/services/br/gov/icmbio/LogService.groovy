package br.gov.icmbio

import grails.converters.JSON
import grails.transaction.Transactional
import grails.util.Environment
import groovy.json.JsonBuilder

@Transactional
class LogService {

    // gravar log formato texto
    void log(String controller, String action, String nome, String cpf, String ip, String log ) {
        if( ! log ) {
            return
        }
        // converter para formato json o texto do log
        if( ! ( log =~ /^\{/) ) {
            try {
                Map mapTemp = ['log':log]
                log = new JsonBuilder( mapTemp ).toString()
            } catch( Exception e ) {
            }
        }
        TrilhaAuditoria ta = new TrilhaAuditoria()
        ta.dtLog = new Date()
        ta.deIp = ip
        ta.deEnv = Environment.current.toString()
        ta.noPessoa = nome
        ta.nuCpf = cpf
        ta.noModulo = controller
        ta.noAcao = action
        ta.txParametros = log.trim()
        ta.save()
    }

    // gravar log de maps
    void log(String controller, String action, String nome, String cpf, String ip, Map log ) {
        String logStr = ''
        try {
            logStr = new JsonBuilder( log ).toString()
        } catch( Exception e1 ){
            try {
                logStr = new JsonBuilder(['log': log]).toString()
            } catch( Exception e2) {}
        }
        this.log( controller, action, nome, cpf , ip, logStr.toString() )
    }

}
