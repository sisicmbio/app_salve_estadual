package br.gov.icmbio

import grails.converters.JSON
import org.codehaus.groovy.grails.web.json.JSONArray

//import grails.transaction.Transactional
import org.codehaus.groovy.grails.web.json.JSONElement

// necessário para o trust manager - HTTPS
import groovyx.net.http.*

import javax.net.ssl.HostnameVerifier
import javax.net.ssl.HttpsURLConnection
import javax.net.ssl.SSLSession
import javax.net.ssl.TrustManager
import javax.net.ssl.SSLContext
import javax.net.ssl.X509TrustManager
import java.security.cert.X509Certificate
import org.apache.http.conn.ssl.SSLSocketFactory
import org.apache.http.conn.scheme.Scheme
//import br.gov.icmbio.Util;

//@Transactional
class RequestService {
    static transactional = false

    def grailsApplication;

    /**
    * Método utilizado para fazer requisições POST
    *  @params mixQueryParams - recebe os parametros de pesquisa nos formatos  [p1:1,p2:3]  ou  p1=2&p2=3
    *
    **/
    def doPost(String strUrl="", def mixQueryParams="", String strReturnContentType = "", Map mapCookies = [:] )
    {
        def sslContext    = null;
        def socketFactory = null;
        def httpsScheme   = null;
        ContentType contentType = null;

        if( strUrl.indexOf('?') > -1 )
        {
            Map getParams = urlParams2Map( strUrl);
            mixQueryParams = getParams;
            strUrl = strUrl.substring( 0, strUrl.indexOf('?' ) );
        }

        if( strUrl.isEmpty() )
        {
            return null;
        }

        //formato [p1:1,p2:3]
        if( mixQueryParams.getClass() == Map)
        {
                mixQueryParams = mixQueryParams.toString();
        }
        // formato p1=2&p2=3....
        if( mixQueryParams.getClass() == String )
        {
            mixQueryParams = mixQueryParams.split('&').inject([:] )
            { map, token ->
                try {

                    token.split('=').with {
                        if( map[ it[0] ]  != null )
                        {
                            map[ it[0] ].push(URLDecoder.decode(it[1],"UTF-8"))
                        }
                        else
                        {
                            map[ it[0] ] = [URLDecoder.decode(it[1],"UTF-8") ]
                        }
                    }
                } catch( Exception e) {}
                map
            }
        }
        if( strUrl.indexOf('https://') == 0 )
        {
            TrustManager[] trustAllCerts = [
              new X509TrustManager() {
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {return null}
                    public void checkClientTrusted(X509Certificate[] certs, String authType) {}
                    public void checkServerTrusted(X509Certificate[] certs, String authType) {}
                }
            ]
            sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());

            socketFactory = new SSLSocketFactory(sslContext, SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            httpsScheme = new Scheme("https", socketFactory, 443);
        }

        try
        {
            HTTPBuilder http = new HTTPBuilder( strUrl );
            if( httpsScheme != null )
            {
                http.client.connectionManager.schemeRegistry.register( httpsScheme );
            }
            contentType = ( strReturnContentType == 'image/jpg' ? ContentType.BINARY: ContentType.TEXT);
            //http://javadox.com/org.codehaus.groovy.modules.http-builder/http-builder/0.6/groovyx/net/http/ContentType.html
            return http.request( strUrl, Method.POST, contentType ) {
                // uri.path = strPath
                requestContentType = ContentType.URLENC
                body = mixQueryParams
                headers.'User-Agent' = "Mozilla/5.0 Firefox/3.0.4"
                if( mapCookies )
                {
                    headers['Cookie'] = mapCookies.collect { k,v -> "$k=$v" }.join(';');
                    //Util.d('Cookies: ' + headers['Cookie'] );
                }
                headers.Accept = '*/*,application/octet-stream,application/json,application/javascript,text/javascript,text/plain,application/x-www-form-urlencoded,application/xml,text/xml,application/xhtml+xml,application/atom+xml'
                contentType = contentType // */* application/octet-stream application/json application/javascript text/javascript text/plain application/x-www-form-urlencoded application/xml text/xml application/xhtml+xml application/atom+xml

                response.success = { resp, reader ->
                    if( contentType == ContentType.BINARY )
                    {
                        reader?.bytes
                    }
                    else
                    {
                        reader?.text
                    }
                }
                response.'404' = {
                    Util.d('404 - DoPost() não encontrou a url: ' + strUrl );
                }
                response.'503' = {
                    Util.d('503 - DoPost() servico temporariamente indisponível na url: ' + strUrl );
                }
                response.'301' = {
                    Util.d('301 - DoPost() URL has been permanently redirected to another URL. (' + strUrl+')' );
                }
                response.failure = { resp, reader ->
                	String erro
                    Util.d( 'Erro doPost() na url '+ strUrl + '\n' + reader?.text )
                    erro = resp?.statusLine+'\n'
                    erro += reader?.text
                    erro += "\n---------------------------------------\n"
                    erro += 'DoPost() falhou url: ' + strUrl+'\n'
                    erro += 'Parametros: ' + mixQueryParams+'\n'
                    erro += 'Cookies...: ' + mapCookies?.toString()+'\n'
                    erro += response?.toString() +'\n'
                    erro += '---------------------------------------'
                    Util.d( erro )
                }
            }

        }
        catch( Exception e ) {
            Util.d( 'ERROR - requestService() - Metodo doPost()')
            Util.d( 'URL: ' + strUrl )
            Util.d( 'Mensagem: ' + e.getMessage() )
            throw e
        }
    }
    /**
     * Método utilizado para fazer requisições GET que retornam JSON
     *
     * @param url
     * @return
     */
    JSONElement doGetJson(String url,Map headers=[:]) {
        url = url.replaceAll(' ','%20')
        int responseCode = 404
        try {
            SSLContext sslContext
            String result
            URL obj = new URL(url);

            // sumular certificado
            if( url.toString().trim() =~ /^https:/ ) {
                // println ' '
                // println 'doGetPost() - HTTPS SELECIONADO'
                TrustManager[] trustAllCerts = [
                        new X509TrustManager() {
                            public java.security.cert.X509Certificate[] getAcceptedIssuers() { return null }
                            public void checkClientTrusted(X509Certificate[] certs, String authType) {}
                            public void checkServerTrusted(X509Certificate[] certs, String authType) {}
                        }
                ]
                sslContext = SSLContext.getInstance("SSL");
                sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
                //HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());

                // Create all-trusting host name verifier
                HostnameVerifier allHostsValid = new HostnameVerifier() {
                    @Override
                    boolean verify(String hostname, SSLSession session) {
                        return true;
                    }
                };
                // Install the all-trusting host verifier
                //HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

                //-------------------------------------------------------------------------------

                HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
                con.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
                con.setDefaultHostnameVerifier(allHostsValid)
                con.setRequestMethod("GET")
                con.setConnectTimeout(15000); // 15 segundos
                con.setReadTimeout(15000);
                con.setRequestProperty("User-Agent", "Mozilla/5.0");
                //con.setRequestProperty("Accept", "*/*") //   '*/*,application/octet-stream,application/json,application/javascript,text/javascript,text/plain,application/x-www-form-urlencoded,application/xml,text/xml,application/xhtml+xml,application/atom+xml'


                headers.each { key, value ->
                    con.setRequestProperty(key, value)
                }
                responseCode = con.getResponseCode();

                if ( responseCode == HttpURLConnection.HTTP_OK) { // success
                    BufferedReader vin = new BufferedReader(new InputStreamReader(con.getInputStream()));
                    String inputLine;
                    StringBuffer response = new StringBuffer()
                    while ((inputLine = vin.readLine()) != null) {
                        response.append(inputLine);
                    }
                    vin.close();
                    result = response.toString()
                } else {
                    result= """{"message":"RequestService.getJson() - erro ${responseCode} ao acessar a url ${url}"}"""
                }
                con.disconnect()
            } else {
                // http
                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("GET");
                con.setRequestProperty("User-Agent", 'Mozilla/5.0"');
                headers.each { key, value ->
                    con.setRequestProperty(key, value)
                }
                responseCode = con.getResponseCode();
                if (responseCode == HttpURLConnection.HTTP_OK) { // success
                    BufferedReader vin = new BufferedReader(new InputStreamReader(con.getInputStream()));
                    String inputLine;
                    StringBuffer response = new StringBuffer()
                    while ((inputLine = vin.readLine()) != null) {
                        response.append(inputLine);
                    }
                    vin.close();
                    result = response.toString()
                } else {
                    result= """{"message":"RequestService.getJson() - erro ${responseCode} ao acessar a url ${url}"}"""
                }
                con.disconnect()
            }
            return JSON.parse(result)
        } catch (Exception e) {
            String message = e.getMessage()
            Util.d("doGetJson() não conseguiu buscar o JSON em (${url}). ${e.getClass()} ${message}, ${e}");
            if( responseCode == 404 ){
                message += '<br> 404 - Serviço não está disponível ou endereço '+url+' está incorreto.'
            }
            return JSON.parse('{"message":"'+message+'"}')
        }
    }

    /**
     * Método utilizado para fazer requisições GET que retornam JSON
     *
     * @param url
     * @return
     */
    JSONElement doGetJsonOld(String url,Map headers=[:]) {
        url = url.replaceAll(' ','+');
        def conn = new URL(url).openConnection()
        conn = this.setProxy( conn );
        try {
            conn.setConnectTimeout(5000);
            conn.setReadTimeout(5000);
            conn.setRequestProperty("User-Agent", "Mozilla/5.0");
            headers.each { key, value ->
                conn.setRequestProperty(key,value)
            }
            def json = conn.content.text
            return JSON.parse(json);
        } catch (Exception e) {
            Util.d("doGetJson() não conseguiu buscar o JSON em (${url}). ${e.getClass()} ${e.getMessage()}, ${e}");
            return JSON.parse('{"message":"'+e.getMessage()+'"}')
        }
    }
    /**
     * Método utilizado para fazer requisições GET que retornam texto puro
     *
     * @param url
     * @return
     */
    String doGet(String requestUrl='', Map requestParams=[:], String returnType='text', Map headers = [:] ) {
        String params = requestParams.collect { it }.join('&');
        requestUrl = requestUrl.replaceAll(' ','+');
        if( requestParams )
        {
            requestUrl = "${requestUrl}?${params}"
        }
        def conn = new URL(requestUrl).openConnection()
        conn = this.setProxy( conn );
        try {
            conn.setConnectTimeout(10000);
            conn.setReadTimeout(10000);
            conn.setRequestProperty("User-Agent", "Mozilla/5.0");
            headers.each { key, value ->
                conn.setRequestProperty(key,value)
            }
            String result = conn.content.text;
            if( returnType.toLowerCase() == 'json')
            {   if( result == 'null' )
                {
                    return ( [:] as JSON ).toString();
                } else {

                }
            }
            return result;
        } catch (Exception e) {
            Util.d("doGet() não conseguiu buscar dados na url (${requestUrl}). ${e.getClass()} ${e.getMessage()}, ${e}");
            if( returnType.toLowerCase() == 'json')
            {
                Map result = [error:e.getMessage()];
                return (result as JSON).toString();
            }
            return null
        }
    }
    /**
     * [setProxy description]
     * @param  conn [description]
     * @return      [description]
     */
    private HttpURLConnection setProxy( HttpURLConnection conn )
    {
        if( System.getProperty("encoded") )
        {
            Util.d('Requisicao via proxy autenticada (' + System.getProperty("encoded") + ')' )
            conn.setRequestProperty("Proxy-Authorization", "Basic " + System.getProperty("encoded") );
        }
        return conn
    }

    /**
     * Fazer a requisição aos serviços de tradução on-line libretranslate ou google tradutor
     * @param txOriginal
     * @return
     */
    String doTranslate( String txOriginal = '',String serviceName = 'libre', boolean lastTry=false) {
        String resultado = ''
        Map requestParams = [:]
        String url = ''
        if (!txOriginal) {
            return null
        }
        // para evitar bloqueio dos serviços de tradução, simular tempo de 1 a 3 segundos entre as requisições
        if ( serviceName != 'libre' ) {
            int delayBetweenRequests = Math.abs(new Random().nextInt() % (3000 - 1000)) + 1000
            if ( delayBetweenRequests ) {
                sleep( delayBetweenRequests )
            }
        }

        try {
            // GOOGLE
            if( serviceName == 'google') {
                //requestParams = [client: 'gtx', sl: 'pt-br', 'tl': 'en', 'dt': 't', 'q': URLEncoder.encode(txOriginal, "UTF-8")]
                requestParams = [:]
                url = grailsApplication.config.traducao.urlGoogle + URLEncoder.encode(txOriginal, "UTF-8")
//println 'Traduzindo em:' + url

                resultado = doGet(url, requestParams, 'text')
                if (resultado) {
                    //println ' '
                    //println 'GOOGLE'
                    JSONArray ja = JSON.parse(resultado)
                    List paragrafos = []
                    if (ja[0][0][0]) {
                        ja[0].each { item ->
                            paragrafos.push(item[0])
                        }
                    }
                    resultado = paragrafos.join('<br>')
                } else {
                    resultado = doTranslate( txOriginal, 'libre')
                }
            } else if( serviceName == 'libre' ) {
                //println 'Utilizando LIBRETRANSLATE'
                url = grailsApplication.config.traducao.urlLibreTranslate
                //url = "http://144.126.147.57:5000/translate"
                String txOriginalTratado = Util.stripTagsTraducao( txOriginal )
                txOriginalTratado = txOriginalTratado.replaceAll(/\n/,'')
/*
println ' '
println ' '
println 'TEXTO PORTUGUES'
println txOriginalTratado
*/
//println 'Url servico traducao: [' + url+']'

                requestParams = [source: 'pt', target: 'en', format: 'html', q: txOriginalTratado]
                resultado = doPost(url, requestParams, 'text')
                if (resultado) {
                    //println ' '
                    //println 'LIBRETRANSLATE'
                    JSONArray jsonArray = JSON.parse('[' + resultado + ']')
                    resultado = jsonArray[0].translatedText
/*
println 'TEXTO TRADUZIDO'
println resultado
println '---------------------------------------------'
*/
                }
            }
        } catch (Exception e) {
            if( ! lastTry ) {
                if (serviceName == 'google') {
                    return doTranslate(txOriginal, 'libre', true)
                } else if (serviceName == 'libre') {
                    return doTranslate(txOriginal, 'google', true)
                }
            }
            Util.printLog('SALVE - TraducaoFichaService._doRequestTranslateServices()'
                , 'Erro ao traduzir texto: ' + txOriginal
                , e.getMessage())
            resultado = ''
        }
        /*if( resultado ) {
            println 'servico traducao utilizado: ' + serviceName + ':'+new Date().format('dd/MM/yyyy HH:mm:ss')
        }*/
        return resultado ?: null
    }

    /**
     * método para transformar os parametros no format a=1&b=2 em ['a':1,'b':2]
     * @param strUrl
     * @return
     */
    Map urlParams2Map(String strUrl = '' ){
        URL url = new URL(strUrl)
        Map pars = [:]
        url.getQuery().split('&').each{p ->
            List l = p.split('=')
            pars[l[0]] = l[1] ?: null
        }
        return pars
    }

}
