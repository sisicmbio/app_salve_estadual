package br.gov.icmbio

// import org.codehaus.groovy.grails.web.servlet.mvc.GrailsParameterMap

//import grails.util.Holders
import org.springframework.web.context.request.RequestContextHolder
import org.codehaus.groovy.grails.web.servlet.mvc.GrailsParameterMap
import grails.transaction.Transactional

@Transactional
class OficinaService {

    def fichaService
    def sqlService

    /**
     * Listar as fichas da oficina
     * @param params
     * @return
     */
	List<OficinaFicha> getFichas(GrailsParameterMap params) {
        //def session = RequestContextHolder.currentRequestAttributes().getSession()
        if( ! params.sqOficinaSelecionada && ! params.sqOficina )
        {
            return []
        }
        if( params.sqOficinaSelecionada )
        {
            params.sqOficina = params.sqOficinaSelecionada
        }
        Oficina oficina = Oficina.get( params.sqOficina )
        List listOficinaFicha = OficinaFicha.createCriteria().list {
             eq('oficina',oficina)
            if( params.sqFicha )
            {
                vwFicha {
                    eq('id', params.sqFicha.toLong() )
                }
            }
             if( params.sqSituacaoFiltro )
             {
                vwFicha {
                    eq('sqSituacaoFicha', params.sqSituacaoFiltro.toInteger() )
                }
             }
        }
        listOficinaFicha = fichaService.applyFilters( listOficinaFicha , params )
        return listOficinaFicha.sort{ it.vwFicha.nmCientifico }
	}

    /**
     * listar os anexos da oficina
     * @param sqOficina
     * @return List
     */
    List getAnexos( Long sqOficina = 0 )
    {
        return sqlService.execSql("""select * from salve.vw_oficina_anexo where sq_oficina = :sqOficina order by no_arquivo""",[ sqOficina : sqOficina ])
    }

    /**
     * listar os anexos da reunião preparatória
     * @param sqOficinaMemoria
     * @return List
     */
    List getAnexosMemoria( Long sqOficinaMemoria = 0 )
    {
        return sqlService.execSql("""select * from salve.oficina_memoria_anexo where sq_oficina_memoria = :sqOficinaMemoria order by no_arquivo""",[ sqOficinaMemoria : sqOficinaMemoria ])
    }

    /**
     * metodo para verificar se o período da oficina está em choque com outra
     * @param oficina
     * @return
     */
    String periodoOficinaEmChoque(Oficina oficina ) {
        String resultado = ''
        if (oficina?.situacao?.codigoSistema == 'ABERTA') {
            List oficinas = Oficina.createCriteria().list {
                createAlias('tipoOficina', 'tipo')
                createAlias('situacao', 'sit')
                ne('id', oficina.id ?: 0l)
                eq('tipoOficina', oficina.tipoOficina)
                eq('sit.codigoSistema', 'ABERTA')
                or {
                    between('dtInicio', oficina.dtInicio, oficina.dtFim)
                    between('dtFim', oficina.dtInicio, oficina.dtFim)
                    and {
                        lt('dtInicio', oficina.dtInicio)
                        gt('dtFim', oficina.dtFim)
                    }
                }
            }
            if (oficinas.size() > 0) {
                resultado = "Periodo da oficina está em coflito com a <b>" +
                    oficinas[0].sgOficina + ' - ' +
                    oficinas[0].dtInicio.format('dd/MM/yyyy') + ' a ' +
                    oficinas[0].dtFim.format('dd/MM/yyyy') + '</b>'
            }

        }
        return resultado
    }

}
