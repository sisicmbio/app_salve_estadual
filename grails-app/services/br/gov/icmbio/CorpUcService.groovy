package br.gov.icmbio

import com.vividsolutions.jts.geom.Geometry
import com.vividsolutions.jts.io.WKTReader
import grails.converters.JSON
import grails.transaction.Transactional

@Transactional
class CorpUcService {

    def sqlService
    def dadosApoioService

    /**
     * método para gravar as informações da UC no banco de dados
     * @param sqPessoa
     * @param sqEsfera
     * @param sqTipo
     * @param noPessoa
     * @param sgInstituicao
     * @param nuCnpj
     * @param cdCnuc
     * @param wkt
     * @return
     */
    def save(Long sqPessoa = null
             , Long sqEsfera = null
             , Long sqTipo = null
             , String noPessoa = ''
             , String sgInstituicao = ''
             , String nuCnpj = ''
             , String cdCnuc = ''
             , String wkt = '') {

        UnidadeConservacao uc

        if (!noPessoa) {
            throw new Exception('Nome da UC não informado')
        }

        if (!sqEsfera) {
            throw new Exception('Esfera não informada')
        }

        if (!sqTipo) {
            throw new Exception('Tipo da UC não informado')
        }

        // VALIDAR CNPJ E NÃO PERMITIR DUPLICIDADE
        if (nuCnpj) {
            if (!Util.isCnpj(nuCnpj)) {
                throw new Exception('CNPJ ' + nuCnpj + ' inexistente')
            }
            // limpar cnpj, deixar somente numeros
            nuCnpj = nuCnpj.replaceAll(/[^0-9]/, '')
            // verificar se já existe o cnpj para outra pessoa
            int qtd = Instituicao.createCriteria().count {
                eq('nuCnpj', nuCnpj)
                if (sqPessoa) {
                    ne('id', sqPessoa)
                }
            }
            if (qtd > 0) {
                throw new Exception('CNPJ ' + Util.formatCnpj(nuCnpj) + ' já cadastrado.')
            }
        }

        // NÃO PERMITIR CODIGO CNUC DUPLICADO
        if (cdCnuc) {
            uc = UnidadeConservacao.createCriteria().get {
                eq('cdCnuc', cdCnuc)
                if (sqPessoa) {
                    ne('id', sqPessoa)
                }
            }
            if (uc) {
                throw new Exception('Código CNUC já cadastrado para ' + uc.instituicao.pessoa.noPessoa)
            }
        }

        if (sqPessoa) {
            uc = UnidadeConservacao.get(sqPessoa)
            if (!uc) {
                throw new Exception('Id da UC inexistente')
            }
        } else {
            Pessoa pessoa = new Pessoa()
            DadosApoio tipoUc = dadosApoioService.getByCodigo('TB_TIPO_INSTITUICAO', 'UNIDADE_CONSERVACAO')
            Instituicao instituicao = new Instituicao(pessoa: pessoa, tipo: tipoUc, sgInstituicao: sgInstituicao ?: noPessoa, nuCnpj: nuCnpj ?: null)
            uc = new UnidadeConservacao(instituicao: instituicao)
        }

        if (wkt) {
            WKTReader wktReader = new WKTReader();
            Geometry g = wktReader.read(wkt)
            uc.geometry = g
            if (uc.geometry) {
                uc.geometry.SRID = 4674
            }
        } else {
            uc.geometry = null
        }
        uc.instituicao.pessoa.noPessoa = noPessoa
        uc.instituicao.sgInstituicao = sgInstituicao
        uc.instituicao.nuCnpj = nuCnpj ?: null
        uc.cdCnuc = cdCnuc
        uc.esfera = DadosApoio.get(sqEsfera)
        uc.tipo = DadosApoio.get(sqTipo)

        // gravar tabela pessoa
        uc.instituicao.pessoa.save()
        if (uc.instituicao.pessoa.hasErrors()) {
            throw new Exception('Erro ao gravar pessoa\n' + uc.instituicao.pessoa.getErrors())
        }

        // gravar tabela instituicao
        uc.instituicao.save()
        if (uc.instituicao.hasErrors()) {
            throw new Exception('Erro ao gravar instituição\n' + instituicao.getErrors())
        }


        // gravar tabela unidade_conservacao
        uc.save()
        if( uc.hasErrors() ) {
            throw new Exception('Erro ao gravar UC\n'+uc.getErrors())
        }
    }


    /**
     * método para recuperar as informações da UC para alteração
     * @param sqPessoa
     * @return
     */
    def edit(Long sqPessoa=0l) {
        if( !sqPessoa ){
            throw new Exception('Id da UC não informado')
        }

        UnidadeConservacao uc
        uc = UnidadeConservacao.get( sqPessoa )
        if( !uc ){
            throw new Exception('Id da UC inexistente')
        }

        Map result = [ 'sqPessoa'           : uc.id
                       , 'noPessoa'         : uc.instituicao.pessoa.noPessoa
                       , 'sgInstituicao'    : uc.instituicao.sgInstituicao
                       , 'cdCnuc'           : uc.cdCnuc
                       , 'nuCnpj'           : uc.instituicao.nuCnpj
                       , 'nuCnpjFormatado'  : uc.instituicao.cnpjFormatado
                       , 'sqEsfera'         : uc.esfera ? uc.esfera.id : null
                       , 'noEsfera'         : uc.esfera ? uc.esfera.descricao : ''
                       , 'cdEsferaSistema'  : uc.esfera ? uc.esfera.codigoSistema : ''
                       , 'sqTipo'           : uc.tipo ? uc.tipo.id : null
                       , 'noTipo'           : uc.tipo ? uc.tipo.descricao : ''
                       , 'cdTipoSistema'    : uc.tipo ? uc.tipo.codigoSistema : ''
                       , 'geojson'          : [ "type": "FeatureCollection", 'features'     : []
                       ]
        ]
        String cmdSql = """SELECT ST_AsGeoJSON( ge_uc,13,8) AS geoJson 
                           FROM salve.unidade_conservacao 
                           WHERE sq_pessoa = :sqPessoa"""

        sqlService.execSql( cmdSql, [sqPessoa:sqPessoa] ).each { row ->
            if( row.geoJson ) {
                result.geojson.features.push( [ 'type': 'Feature'
                                               ,'properties': [],
                                                'geometry': JSON.parse( row.geoJson )

                ])
            }
        }
        return result
    }


    /**
     * método para excluir a UC do banco de dados
     * @return
     */
    def delete(Long sqPessoa = 0l) {
        UnidadeConservacao uc = UnidadeConservacao.get( sqPessoa )
        if( !uc ) {
            throw new Exception('Id da UC não informado')
        }
        uc.delete()
        uc.instituicao.delete()
        uc.instituicao.pessoa.delete()
    }

    /**
     * Listar as unidades de conservação
     * @param page
     * @param pageSize
     * @param q
     * @return
     */
    List list( Integer page=1, Integer pageSize=100, Map params = [:]) {
        Integer skipRecords = (pageSize * (page - 1))
        return UnidadeConservacao.createCriteria().list {
            if (params.noEsfera) {
                esfera {
                    ilike('descricao', '%' + params.noEsfera.trim() + '%')
                }
            }
            if (params.dsTipo) {
                tipo {
                    ilike('descricao', '%' + params.dsTipo.trim() + '%')
                }
            }
            if (params.cdCnuc) {
                params.cdCnuc = params.cdCnuc.toString().padLeft(10,'0')
                params.cdCnuc = Util.formatStr(params.cdCnuc,'####.##.####')
                eq('cdCnuc',params.cdCnuc)
            }

            if (params.nuCnpj) {
                instituicao {
                    eq('nuCnpj', params.nuCnpj)
                }
            }

            if (params.noPessoa) {
                instituicao {
                    or {
                        ilike('sgInstituicao', '%' + params.noPessoa.trim() + '%')
                        pessoa {
                            ilike('noPessoa', '%' + params.noPessoa.trim() + '%')
                        }
                    }
                }
            }
            maxResults(pageSize)
            firstResult(skipRecords)
            //order("instituicao.pessoa.noPessoa", "asc")
        }
    }

}
