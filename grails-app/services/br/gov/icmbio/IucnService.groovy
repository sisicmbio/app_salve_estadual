package br.gov.icmbio

import grails.converters.JSON
import grails.transaction.Transactional
import org.codehaus.groovy.grails.web.json.JSONObject
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.StandardCopyOption
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream

@Transactional
class IucnService {

    // injeção de dependências
    def sqlService
    def grailsApplication
    def emailService

    // lista de capos da ficha que podem ter tradução
    List camposFichaTraducao = []

    // lista dos arquivos que devem fazer parte do zip enviado para o sis/iucn
    List arquivosCsv = ['allfields.csv'
                        , 'assessments.csv'
                        , 'commonnames.csv'
                        , 'conservationneeded.csv'
                        , 'countries.csv'
                        , 'credits.csv'
                        , 'fao.csv'
                        , 'habitats.csv'
                        , 'lme.csv'
                        , 'plantspecific.csv'
                        , 'references.csv'
                        , 'researchneeded.csv'
                        , 'synonyms.csv'
                        , 'taxonomy.csv'
                        , 'threats.csv'
                        , 'usetrade.csv']

    /**
     * método para fazer a validação dos dados antes de adicionar a linha ao arquivo csv correspondente
     * @param colJson
     * @param jsonRow
     * @param problemasEncontrados
     * @return
     */
    protected boolean _validarRowJson( String colJson, JSONObject jsonRow, List problemasEncontrados){
        boolean result = true
        // regras para sinonímias ( synonyms )
        if( colJson == 'json_nomes_antigos' ){
            if( ( jsonRow.no_genero == '' ) || ( jsonRow.no_antigo =='')  )  {
                problemasEncontrados.push('Nome antigo "' + jsonRow.no_antigo + '" inválido')
                result=false
            }
            if( jsonRow.no_autor =='' )  {
                problemasEncontrados.push('Nome antigo "' + jsonRow.no_antigo + '" sem autor')
                result=false
            }
        }
        return result
    }

    /**
     * método para calcular o primeiro nome a partir do nome completo
     * @param name
     * @return
     */
    protected String _getFirstName(String name = '' ) {
        List parts = name.split(' ')
        if( parts.size() > 1 ) {
            parts.pop()
            name = parts.join(' ')
        }
        return name
    }

    /**
     * método para colcular o último nome a partir do nome completo
     * @param name
     * @return
     */
    protected String _getLastName(String name = '' ) {
        List parts = name.split(' ')
        if( parts.size() > 0 ) {
            name = parts[ parts.size() - 1 ]
        }
        return name
    }

    /**
     * método para colcular as iniciais do nome
     * @param name
     * @return
     */
    protected String _getInitials(String name = '' ) {
        List parts = name.split(' ')
        if( parts ) {
            name = parts.collect{ it.toString().substring(0,1)}.join('.')+'.'
        }
        return name
    }

    /**
     * método para receber uma lista de fichas e gerar os arquivos csv zipados
     * @param listFichas
     */
    def gerarZip(List listFichas = [], Long sqPessoa = 0l, String email = '') {
        Map res = [status: 0, msg: '', type: 'success', data: [:]]
        String diaHora = new Date().format('yyyy-MM-dd-HH-mm-ss')
        String nomeArquivoZip = grailsApplication.config.dir.temp ?: '/data/salve-estadual/temp/salve_fichas_' + diaHora + '.zip'
        try {
            // validar a lista de fichas, não pode estar vazia e todas as fichas devem estar publicadas
            validarFichas(listFichas)

            // validar se o id da pessoa informado é válido e retornar o e-mail corporativo ou o que foi recebido nos parâmetros
            email = validarPessoa(sqPessoa, email)

            // validar se o e-mail é valido
            validarEmail(email)

            res.data.nomeAquivoZip = nomeArquivoZip
            res.data.sqPessoa = sqPessoa
            res.data.email = email
            res.msg = """Gerando arquivo zip com a(s) ficha(s) selecionadas.
                Este procedimento levará alguns minutos e ao finalizar será eviado para o e-mail <b>${email}</b> o link para baixar o arquivo."""

            // alimentar a lista de campos da ficha
            camposFichaTraducao = sqlService.execSql("""select sq_traducao_tabela, no_coluna from salve.traducao_tabela where no_tabela = 'ficha'""")

            // inicar tarefa da criação dos arquivos csv e zipar todos os csvs gerados
            runAsync {
                _gerarCsv(listFichas, res)
            }
        } catch (Exception e) {
            res.status = 1
            res.type = 'error'
            res.msg = e.getMessage() + '<br>Solicitação foi cancelada.'
        }
        return res
    }

    /**
     * método para validar a lista de fichas que serão enviadas para o SIS.
     * 1) A lista não pode estar vazia
     * 2) Todas as fichas devem estar publicadas
     */
    protected void validarFichas(List listFichas = []) {

        // recusar lista vizia
        if (listFichas.size() == 0) {
            throw new Exception('Nenhuma ficha selecionada para transmissão.')
        }

        // recusoar se pelo menos uma ficha NÃO estiver publicada
        sqlService.execSql("""select count( ficha.sq_ficha ) as nu_fichas from salve.ficha
                inner join salve.dados_apoio as situacao on situacao.sq_dados_apoio = ficha.sq_situacao_ficha
                where situacao.cd_sistema <> 'PUBLICADA'
                and ficha.sq_ficha in ( ${listFichas.join(',')} )""").each { row ->
            if (row.nu_fichas > 0) {
                throw new Exception('Atenção, encontrada(s) ' + row.nu_fichas + ' ficha(s) não PUBLICADA(s).')
            }
        }
    }

    /**
     * método para validar se o id da pessoa informado é válido e retornar o e-mail corporativo
     */
    protected String validarPessoa(Long sqPessoa = null, String email = '') {
        if (!sqPessoa) {
            throw new Exception('Id do usuário não informado.')
        }
        List rows = sqlService.execSql("""select pf.no_pessoa, email.tx_email from salve.vw_pessoa_fisica pf
                left join corporativo.vw_email email on email.sq_pessoa = pf.sq_pessoa and email.sq_tipo_email = 2
                where pf.sq_pessoa = ${sqPessoa.toString()}
                limit 1
                """)
        if (rows.size() == 0) {
            throw new Exception('Id do usuário informado (' + sqPessoa.toString() + ') não cadastrado no ICMBio.')
        }
        return email ?: rows[0].tx_email
    }

    /**
     * método para validar se o e-mail informado é um e-mail válido ou está em branco
     * @param email
     */
    protected void validarEmail(String email = '') {
        email = email.trim().toLowerCase()
        if (email.isEmpty()) {
            throw new Exception('E-mail é obrigatório.')
        }
        if (!Validador.isEmail(email)) {
            throw new Exception('E-mail <b>' + email + '</b> é inválido.')
        }
    }

    /**
     * método para gerar os arquivos CSV com as informações das fichas selecionadas
     * @param listFichas
     * @param res
     */
    protected void _gerarCsv(List listFichas = [], Map res = [:]) {
        println ' '
        println 'Inicio Geração do arquivo zip com a(s) ficha(s) selecionadas ' + new Date().format('HH:mm:ss')

        //List campos = [colName:'RangeDocumentation.narrative',csvFile:'assessments.csv']
        // gerar 4 caracteres randomicos para prefixar os arquivos gerados e não gerar conflito entre usuários simultâneos
        String rndId = Util.randomString(4).toString().toLowerCase()

        //rndId = ''
        /**
         * rndId = '' para:
         * - não gravar na tabela iucn_transmissao
         * - nao gerar e nem enviar e-mail
         * - apenas gerar os arquivos CSVs em /data/salve-estadual/temp/
         */

        // excluir arquivos csv antigo se existirem
        new File('/data/salve-estadual/temp/salve_sis_' + rndId + '.zip').delete()
        arquivosCsv.each { csvName ->
            new File('/data/salve-estadual/temp/' + rndId + '_' + csvName).delete()
        }

        String cmdSql = """select coalesce(taxon.json_trilha->'subespecie'->>'no_taxon',taxon.json_trilha->'especie'->>'no_taxon') as nm_cientifico
                 ,taxon.sq_taxon
                 ,taxon.no_autor
                 ,ficha.sq_ficha
                 ,taxon.json_trilha->'reino'->>'no_taxon' as no_reino
                 ,taxon.json_trilha->'filo'->>'no_taxon' as no_filo
                 ,taxon.json_trilha->'classe'->>'no_taxon' as no_classe
                 ,taxon.json_trilha->'ordem'->>'no_taxon' as no_ordem
                 ,taxon.json_trilha->'familia'->>'no_taxon' as no_familia
                 ,taxon.json_trilha->'genero'->>'no_taxon' as no_genero
                 ,taxon.json_trilha->'especie'->>'no_taxon' as no_especie
                 ,taxon.json_trilha->'subespecie'->>'no_taxon' as no_subespecie
                 ,salve.remover_html_tags( salve.entity2char(coalesce( ficha.ds_notas_taxonomicas,'') ) ) as ds_notas_taxonomicas
                 ,salve.remover_html_tags( salve.entity2char(coalesce( ficha.ds_distribuicao_geo_global,'') ) )  as ds_distribuicao_geo_global
                 ,salve.remover_html_tags( salve.entity2char(coalesce( ficha.ds_distribuicao_geo_nacional,'') ) ) as ds_distribuicao_geo_nacional
                 ,ficha.vl_aoo
                 ,ficha.st_endemica_brasil
                 ,salve.remover_html_tags( salve.entity2char(coalesce( ficha.ds_justificativa_aoo_eoo,'') ) ) as ds_justificativa_aoo_eoo

                 , case when tendencia_aoo.cd_sistema = 'DECLINANDO' then '1' else
                    case when tendencia_aoo.cd_sistema = 'AUMENTANTO' then '0' else
                     case when tendencia_aoo.cd_sistema = 'DESCONHECIDO' then 'U' end end end as ds_tendencia_aoo

                 , case when ficha.st_flutuacao_aoo = 'S' then '1' else
                    case when ficha.st_flutuacao_aoo = 'N' then '0' else
                        case when ficha.st_flutuacao_aoo ='D' then 'U' end end end as ds_flutuacao_aoo

                 ,ficha.vl_eoo

                 , case when tendencia_eoo.cd_sistema ='DECLINANDO' then '1' else
                    case when tendencia_eoo.cd_sistema ='AUMENTANTO' then '0' else
                        case when tendencia_eoo.cd_sistema ='DESCONHECIDO' then 'U' end end end as ds_tendencia_eoo

                 , case when ficha.st_flutuacao_eoo = 'S' then '1' else
                     case when ficha.st_flutuacao_eoo ='N' then '0' else
                        case when ficha.st_flutuacao_eoo ='D' then 'U' end end end as ds_flutuacao_eoo

                  ,ficha.nu_localizacoes

                  -- ameacas
                  ,salve.remover_html_tags( salve.entity2char(coalesce( ficha.ds_ameaca,'') ) ) as ds_ameaca
                  ,case when ameacas.json_ameacas is not null then '1' else '0' end as st_ameaca
                  ,ameacas.json_ameacas


                  , case when tendencia_num_localizacoes.cd_sistema ='DECLINANDO' then '1' else
                     case when tendencia_num_localizacoes.cd_sistema in( 'ESTAVEL','AUMENTANTO') then '0' else
                       case when tendencia_num_localizacoes.cd_sistema ='DESCONHECIDO' then 'U' end end end as ds_tendencia_num_localizacoes

                  , case when ficha.st_flutuacao_num_localizacoes = 'S' then '1' else
                      case when ficha.st_flutuacao_num_localizacoes ='N' then '0' else
                        case when ficha.st_flutuacao_num_localizacoes ='D' then 'U' end end end as ds_flutuacao_num_localizacoes

                  ,ficha.nu_min_altitude
                  ,ficha.nu_max_altitude

                  ,ficha.nu_max_batimetria
                  ,ficha.nu_min_batimetria
                  ,'' as depth_zone
                  ,'Done' as map_status
                  ,salve.remover_html_tags( salve.entity2char(coalesce( ficha.ds_populacao,'') ) ) as ds_populacao
                  ,tendencia_populacional.ds_dados_apoio as ds_tendencia_populacional
                  ,tendencia_populacional.sq_dados_apoio as sq_tendencia_populacional
                  ,ficha.nu_individuo_maduro

                ,case when ficha.st_flut_extrema_indiv_maduro = 'S' then '1' else
                     case when ficha.st_flut_extrema_indiv_maduro ='N' then '0' else
                         case when ficha.st_flut_extrema_indiv_maduro ='D' then 'U' end end end as ds_flut_extrema_indiv_maduro

                ,case when ficha.st_populacao_fragmentada = 'S' then '1' else
                    case when ficha.st_populacao_fragmentada ='N' then '0' else
                        case when ficha.st_populacao_fragmentada ='D' then 'U' end end end as ds_populacao_fragmentada

                 ,case when tenden_num_individuo_maduro.cd_sistema ='DECLINANDO' then '1' else
                    case when tenden_num_individuo_maduro.cd_sistema in( 'ESTAVEL','AUMENTANTO') then '0' else
                        case when tenden_num_individuo_maduro.cd_sistema ='DESCONHECIDO' then 'U' end end end as ds_tenden_num_individuo_maduro

                ,perc_declinio_populacional.sq_dados_apoio as sq_perc_declinio_populacional
                ,case when perc_declinio_populacional.cd_sistema = '25P_3_ANOS_1_GERACAO' then perc_declinio_populacional.ds_dados_apoio end as ds_perc_declinio_populacional1
                ,case when perc_declinio_populacional.cd_sistema = '20P_5_ANOS_2_GERACAO' then perc_declinio_populacional.ds_dados_apoio end as ds_perc_declinio_populacional2
                ,case when perc_declinio_populacional.cd_sistema = '10P_10_ANO_3_GERACAO' then perc_declinio_populacional.ds_dados_apoio end as ds_perc_declinio_populacional3


                ,case when ficha.st_flut_extrema_sub_populacao = 'S' then '1' else
                    case when ficha.st_flut_extrema_sub_populacao ='N' then '0' else
                        case when ficha.st_flut_extrema_sub_populacao ='D' then 'U' end end end as ds_flut_extrema_sub_populacao


                , case when tendencia_num_subpopulacao.cd_sistema ='DECLINANDO' then '1' else
                    case when tendencia_num_subpopulacao.cd_sistema in( 'ESTAVEL','AUMENTANTO') then '0' else
                        case when tendencia_num_subpopulacao.cd_sistema ='DESCONHECIDO' then 'U' end end end as ds_tendencia_num_subpopulacao

                ,ficha.nu_indivi_subpopulacao_perc
                ,ficha.nu_indivi_subpopulacao_max
                ,ficha.nu_subpopulacao

                ,salve.remover_html_tags( salve.entity2char(coalesce( ficha.ds_metodo_calc_perc_extin_br,'') ) ) as ds_metodo_calc_perc_extin_br
                ,prob_extincao_brasil.sq_dados_apoio as sq_prob_extincao_brasil
                ,case when prob_extincao_brasil.cd_sistema = 'MAIOR_IGUAL_10P_100_ANOS' then prob_extincao_brasil.ds_dados_apoio end as ds_prob_extincao_brasil100
                ,case when prob_extincao_brasil.cd_sistema = 'MAIOR_IGUAL_20P_20_ANOS_5_GERACOES' then prob_extincao_brasil.ds_dados_apoio end as ds_prob_extincao_brasil5
                ,case when prob_extincao_brasil.cd_sistema = 'MAIOR_IGUAL_50P_10_ANOS_3_GERACOES' then prob_extincao_brasil.ds_dados_apoio end as ds_prob_extincao_brasil3


                -- população
                ,ficha.nu_reducao_popul_futura
                ,ficha.sq_tipo_redu_popu_futura
                ,unid_reducao_popul_futura.ds_dados_apoio as ds_unid_reducao_popul_futura

                ,ficha.nu_reducao_popul_pass_futura
                ,ficha.nu_reducao_popul_passada_rev


                -- habitats
                ,case when ficha.st_variacao_sazonal_habitat = 'S' then '1' else
                   case when ficha.st_variacao_sazonal_habitat ='N' then '0' else
                     case when ficha.st_variacao_sazonal_habitat ='D' then 'U' end end end as ds_variacao_sazonal_habitat
                ,salve.remover_html_tags( salve.entity2char(coalesce( ficha.ds_uso_habitat,'') ) ) as ds_uso_habitat
                ,habitats.json_habitats

                -- populacao
                ,salve.remover_html_tags( salve.entity2char(coalesce( ficha.ds_metod_calc_tempo_geracional,'') ) ) as ds_metod_calc_tempo_geracional
                ,ficha.vl_maturidade_sexual_femea
                ,unid_maturidade_sexual_femea.ds_dados_apoio as ds_unid_maturidade_sexual_femea
                ,ficha.vl_maturidade_sexual_macho
                ,unid_maturidade_sexual_macho.ds_dados_apoio as ds_unid_maturidade_sexual_macho

                -- comprimento femea em cm
                ,case when unid_comprimento_femea.cd_sistema = 'CM' then ficha.vl_comprimento_femea
                    else case when unid_comprimento_femea.cd_sistema = 'MM' then ficha.vl_comprimento_femea / 10
                        else case when unid_comprimento_femea.cd_sistema = 'M' then ficha.vl_comprimento_femea  * 100
                           else case when unid_comprimento_femea.cd_sistema = 'KM' then ficha.vl_comprimento_femea * 100000 end
                        end
                    end
                 end as vl_comprimento_femea

                -- comprimento maximo femea em cm
                ,case when unid_comprimento_femea_max.cd_sistema = 'CM' then ficha.vl_comprimento_femea_max
                    else case when unid_comprimento_femea_max.cd_sistema = 'MM' then ficha.vl_comprimento_femea_max / 10
                        else case when unid_comprimento_femea_max.cd_sistema = 'M' then ficha.vl_comprimento_femea_max  * 100
                           else case when unid_comprimento_femea_max.cd_sistema = 'KM' then ficha.vl_comprimento_femea_max * 100000 end
                        end
                    end
                 end as vl_comprimento_femea_max

                 -- comprimento macho em cm
                ,case when unid_comprimento_macho.cd_sistema = 'CM' then ficha.vl_comprimento_macho
                    else case when unid_comprimento_macho.cd_sistema = 'MM' then ficha.vl_comprimento_macho / 10
                        else case when unid_comprimento_macho.cd_sistema = 'M' then ficha.vl_comprimento_macho  * 100
                           else case when unid_comprimento_macho.cd_sistema = 'KM' then ficha.vl_comprimento_macho * 100000 end
                        end
                    end
                 end as vl_comprimento_macho

                -- comprimento maximo macho em cm
                ,case when unid_comprimento_macho_max.cd_sistema = 'CM' then ficha.vl_comprimento_macho_max
                    else case when unid_comprimento_macho_max.cd_sistema = 'MM' then ficha.vl_comprimento_macho_max / 10
                        else case when unid_comprimento_macho_max.cd_sistema = 'M' then ficha.vl_comprimento_macho_max  * 100
                           else case when unid_comprimento_macho_max.cd_sistema = 'KM' then ficha.vl_comprimento_macho_max * 100000 end
                        end
                    end
                 end as vl_comprimento_macho_max


                -- longevidade macho ou femea
                ,ficha.vl_longevidade_macho
                ,ficha.vl_longevidade_femea
                ,unid_longevidade_macho.cd_sistema     as ds_longevidade_macho
                ,unid_longevidade_femea.cd_sistema     as ds_longevidade_femea
                ,unid_longevidade_macho.sq_dados_apoio as sq_unid_longevidade_macho
                ,unid_longevidade_femea.sq_dados_apoio as sq_unid_longevidade_femea
                ,unid_longevidade_macho.cd_sistema as cd_sistema_unid_longevidade_macho
                ,unid_longevidade_femea.cd_sistema as cd_sistema_unid_longevidade_femea


                ,case when unid_comprimento_femea_max.cd_sistema = 'CM' then ficha.vl_comprimento_femea_max
                    else case when unid_comprimento_femea_max.cd_sistema = 'MM' then ficha.vl_comprimento_femea_max / 10
                        else case when unid_comprimento_femea_max.cd_sistema = 'M' then ficha.vl_comprimento_femea_max  * 100
                           else case when unid_comprimento_femea_max.cd_sistema = 'KM' then ficha.vl_comprimento_femea_max * 100000 end
                        end
                    end
                end as vl_comprimento_femea_max

                ,ficha.vl_tempo_gestacao
                ,unid_tempo_gestacao.ds_dados_apoio as ds_tempo_gestacao

                ,ficha.vl_tempo_gestacao
                ,unid_tempo_gestacao.ds_dados_apoio as ds_tempo_gestacao

                ,ficha.vl_intervalo_nascimento
                ,unid_intervalo_nascimento.ds_dados_apoio as ds_intervalo_nascimento

                ,ficha.vl_tamanho_prole

                ,case when modo_reproducao.cd_sistema = 'OVIPARIDADE' or modo_reproducao.cd_sistema = 'OVOVIVIPARIDADE' then '1' else case when modo_reproducao.cd_sistema is not null then '2' else '' end end as ds_modo_reproducao_oviparo
                ,case when modo_reproducao.cd_sistema = 'PARTENOGENESE' then '1' else case when modo_reproducao.cd_sistema is not null then '2' else '' end end as ds_modo_reproducao_partenogenese
                ,case when modo_reproducao.cd_sistema = 'VIVIPARIDADE' then '1' else case when modo_reproducao.cd_sistema is not null then '2' else '' end end as ds_modo_reproducao_viviparo

                -- usos
                ,salve.remover_html_tags( salve.entity2char(coalesce( ficha.ds_uso,'') ) ) as ds_uso
                ,case when usos.qtd_uso_subsistencia > 0 then 'Yes' else case when usos.json_usos is null then 'No' else 'Unknown' end end as st_uso_subsistencia
                ,usos.json_usos

                -- acoes de conservação
                ,salve.remover_html_tags( salve.entity2char( coalesce( ficha.ds_acao_conservacao,'') ) ) as ds_acao_conservacao
                ,acoes_conservacao.json_acoes_conservacao

                , salve.remover_html_tags( salve.entity2char(coalesce( ficha.ds_presenca_uc,'') ) ) as ds_presenca_uc

                , salve.remover_html_tags( salve.entity2char(coalesce( ficha.ds_pesquisa_exist_necessaria,'') ) ) as ds_pesquisa_exist_necessaria
                , pesquisas.json_pesquisas

                , categoria.cd_dados_apoio as cd_categoria
                , ficha.ds_criterio_aval_iucn_final as ds_criterio
                , case when ficha.st_possivelmente_extinta_final = 'S' then 'True' else 'False' end  as st_possivelmente_extinta
                , salve.remover_html_tags( salve.entity2char(coalesce( ficha.ds_justificativa_final,'') ) ) as ds_justificativa_final
                , motivos_mudanca.ds_motivo_mudanca
                , motivos_mudanca.cd_motivo_mudanca_sistema

                , salve.remover_html_tags( salve.entity2char(coalesce( ficha.ds_just_mudanca_categoria,'') ) ) as ds_just_mudanca_categoria
                , salve.remover_html_tags( salve.entity2char(coalesce( ficha.ds_diagnostico_morfologico,'') ) ) as ds_diagnostico_morfologico
                ,'Assessment' as ds_reference_type
                ,nomes_comuns.ds_nomes_comuns
                ,nomes_antigos.json_nomes_antigos


                -- Large Marine Ecosystemsl - falta fazer o mapeamento
                ,null as json_lme
                ,null as json_paises
                ,null as json_faos
                ,null as json_creditos
                ,null as json_plantas

                from salve.ficha
                inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                left outer join salve.dados_apoio as tendencia_aoo on tendencia_aoo.sq_dados_apoio = ficha.sq_tendencia_aoo
                left outer join salve.dados_apoio as tendencia_eoo on tendencia_eoo.sq_dados_apoio = ficha.sq_tendencia_aoo
                left outer join salve.dados_apoio as tendencia_num_localizacoes on tendencia_num_localizacoes.sq_dados_apoio = ficha.sq_tenden_num_localizacoes
                left outer join salve.dados_apoio as tendencia_populacional on tendencia_populacional.sq_dados_apoio = ficha.sq_tendencia_populacional
                left outer join salve.dados_apoio as tenden_num_individuo_maduro on tenden_num_individuo_maduro.sq_dados_apoio = ficha.sq_tenden_num_individuo_maduro
                left outer join salve.dados_apoio as perc_declinio_populacional on perc_declinio_populacional.sq_dados_apoio = ficha.sq_perc_declinio_populacional
                left outer join salve.dados_apoio as tendencia_num_subpopulacao on tendencia_num_subpopulacao.sq_dados_apoio = ficha.sq_tendencia_num_subpopulacao
                left outer join salve.dados_apoio as prob_extincao_brasil on prob_extincao_brasil.sq_dados_apoio = ficha.sq_prob_extincao_brasil
                left outer join salve.dados_apoio as unid_maturidade_sexual_femea on unid_maturidade_sexual_femea.sq_dados_apoio = ficha.sq_unid_maturidade_sexual_femea
                left outer join salve.dados_apoio as unid_maturidade_sexual_macho on unid_maturidade_sexual_macho.sq_dados_apoio = ficha.sq_unid_maturidade_sexual_macho

                left outer join salve.dados_apoio as unid_comprimento_femea on unid_comprimento_femea.sq_dados_apoio = ficha.sq_medida_comprimento_femea
                left outer join salve.dados_apoio as unid_comprimento_macho on unid_comprimento_macho.sq_dados_apoio = ficha.sq_medida_comprimento_macho

                left outer join salve.dados_apoio as unid_comprimento_femea_max on unid_comprimento_femea_max.sq_dados_apoio = ficha.sq_medida_comprimento_femea_max
                left outer join salve.dados_apoio as unid_comprimento_macho_max on unid_comprimento_macho_max.sq_dados_apoio = ficha.sq_medida_comprimento_macho_max

                left outer join salve.dados_apoio as unid_longevidade_macho on unid_longevidade_macho.sq_dados_apoio = ficha.sq_unidade_longevidade_macho
                left outer join salve.dados_apoio as unid_longevidade_femea on unid_longevidade_femea.sq_dados_apoio = ficha.sq_unidade_longevidade_femea

                left outer join salve.dados_apoio as unid_tempo_gestacao on unid_tempo_gestacao.sq_dados_apoio = ficha.sq_unid_tempo_gestacao
                left outer join salve.dados_apoio as unid_intervalo_nascimento on unid_intervalo_nascimento.sq_dados_apoio = ficha.sq_unid_intervalo_nascimento
                left outer join salve.dados_apoio as modo_reproducao on modo_reproducao.sq_dados_apoio = ficha.sq_modo_reproducao
                left join salve.dados_apoio as categoria on categoria.sq_dados_apoio = ficha.sq_categoria_final
                left join salve.dados_apoio as unid_reducao_popul_futura on unid_reducao_popul_futura.sq_dados_apoio = ficha.sq_tipo_redu_popu_futura

                -- motivos da mudanca de categoria
                 left join lateral (
                    select array_to_string(array_agg(distinct iucn.ds_iucn_motivo_mudanca), '|') as ds_motivo_mudanca
                    ,array_to_string(array_agg(distinct apoio.cd_sistema), '|') as cd_motivo_mudanca_sistema
                    from salve.ficha_mudanca_categoria
                    inner join salve.dados_apoio as apoio on apoio.sq_dados_apoio = ficha_mudanca_categoria.sq_motivo_mudanca
                    inner join salve.iucn_motivo_mudanca_apoio iucn_apoio on iucn_apoio.sq_dados_apoio = ficha_mudanca_categoria.sq_motivo_mudanca
                    inner join salve.iucn_motivo_mudanca iucn on iucn.sq_iucn_motivo_mudanca = iucn_apoio.sq_iucn_motivo_mudanca

            where ficha_mudanca_categoria.sq_ficha = ficha.sq_ficha
            ) as motivos_mudanca on true

                -- nomes comuns
               left join lateral(
                    select array_to_string( array_agg(ficha_nome_comum.no_comum order by sq_ficha_nome_comum ),'|') AS ds_nomes_comuns
                    from salve.ficha_nome_comum
                    where ficha_nome_comum.sq_ficha = ficha.sq_ficha
                      and  ( de_regiao_lingua ilike 'português' or de_regiao_lingua ilike 'brasil' )
                ) as nomes_comuns on true

                -- nomes antigos
                left join lateral(
                    select json_object_agg( ficha_sinonimia.sq_ficha_sinonimia::text, json_build_object('no_antigo',ficha_sinonimia.no_sinonimia
                    , 'no_autor', ficha_sinonimia.no_autor
                    , 'nu_ano',ficha_sinonimia.nu_ano
                    , 'no_genero',substr(ficha_sinonimia.no_sinonimia,0, strpos(ficha_sinonimia.no_sinonimia,' '))
                    , 'ds_iucn','-'
                    ) )::text as json_nomes_antigos
                   from salve.ficha_sinonimia where sq_ficha = ficha.sq_ficha
                ) as nomes_antigos on true

                -- selecionar os habitats
                left join lateral (
                select json_object_agg(ficha_habitat.sq_habitat::text, json_build_object('ds_dados_apoio',habitat.descricao
                    , 'ds_trilha_sistema', habitat.ds_trilha_sistema
                    , 'ds_iucn',coalesce(iucn_habitat.ds_iucn_habitat,'')
                    , 'cd_iucn',coalesce(iucn_habitat.cd_iucn_habitat,'') ) )::text as json_habitats
                from salve.ficha_habitat
                         inner join salve.fn_recursive_apoio('TB_HABITAT') as habitat on habitat.id = ficha_habitat.sq_habitat
                         left outer join salve.iucn_habitat_apoio on iucn_habitat_apoio.sq_dados_apoio = ficha_habitat.sq_habitat
                         left outer join salve.iucn_habitat  on iucn_habitat.sq_iucn_habitat = iucn_habitat_apoio.sq_iucn_habitat
                where ficha_habitat.sq_ficha = ficha.sq_ficha
                ) as habitats on true


                -- selecionar as ações de conservacao
                left join lateral (
                    select json_object_agg(ficha_acao_conservacao.sq_acao_conservacao::text,
                                           json_build_object('ds_dados_apoio', acao.ds_dados_apoio
                                               , 'cd_sistema', acao.cd_sistema
                                               , 'ds_iucn', coalesce(iucn_acao_conservacao.ds_iucn_acao_conservacao, '')
                                               , 'cd_iucn', coalesce(iucn_acao_conservacao.cd_iucn_acao_conservacao, '')
                                               , 'ds_notas', coalesce( pan.tx_plano_acao, coalesce(ordenamento.ds_dados_apoio,'')))
                        )::text as json_acoes_conservacao
                    from salve.ficha_acao_conservacao
                             inner join salve.dados_apoio as acao on acao.sq_dados_apoio = ficha_acao_conservacao.sq_acao_conservacao
                             left outer join salve.iucn_acao_conservacao_apoio on iucn_acao_conservacao_apoio.sq_dados_apoio = ficha_acao_conservacao.sq_acao_conservacao
                             left outer join salve.iucn_acao_conservacao on iucn_acao_conservacao.sq_iucn_acao_conservacao = iucn_acao_conservacao_apoio.sq_iucn_acao_conservacao
                             left outer join salve.dados_apoio as situacao on situacao.sq_dados_apoio = ficha_acao_conservacao.sq_situacao_acao_conservacao
                    left outer join salve.plano_acao as pan on ficha_acao_conservacao.sq_plano_acao = pan.sq_plano_acao
                    left outer join salve.dados_apoio as ordenamento on ordenamento.sq_dados_apoio = ficha_acao_conservacao.sq_tipo_ordenamento
                    where ficha_acao_conservacao.sq_ficha = ficha.sq_ficha
                             and situacao.cd_sistema in ( 'EM_IMPLEMENTACAO','EXISTENTE',  'NECESSARIA','PREVISTA' )
                    ) as acoes_conservacao on true

               -- selecionar as pesquisas
                     left join lateral (
                select json_object_agg(ficha_pesquisa.sq_tema_pesquisa::text, json_build_object('ds_dados_apoio',acao.ds_dados_apoio
                    , 'ds_iucn',coalesce(iucn_pesquisa.ds_iucn_pesquisa,'')
                    , 'cd_iucn',coalesce(iucn_pesquisa.cd_iucn_pesquisa,'')
                    , 'ds_notas',coalesce(traducao.tx_traduzido, coalesce( situacao.ds_dados_apoio,'') )
                    ) )::text as json_pesquisas
                from salve.ficha_pesquisa
                         inner join salve.dados_apoio as acao on acao.sq_dados_apoio = ficha_pesquisa.sq_tema_pesquisa
                         left outer join salve.iucn_pesquisa_apoio on iucn_pesquisa_apoio.sq_dados_apoio = ficha_pesquisa.sq_tema_pesquisa
                         left outer join salve.iucn_pesquisa on iucn_pesquisa.sq_iucn_pesquisa = iucn_pesquisa_apoio.sq_iucn_pesquisa
                         left outer join salve.dados_apoio as situacao on situacao.sq_dados_apoio = ficha_pesquisa.sq_situacao_pesquisa
                         left outer join salve.traducao_dados_apoio as traducao on traducao.sq_dados_apoio = situacao.sq_dados_apoio
                where ficha_pesquisa.sq_ficha = ficha.sq_ficha
                ) as pesquisas on true

                -- selecionar as ameacas
                     left join lateral (
                select json_object_agg(ficha_ameaca.sq_criterio_ameaca_iucn::text, json_build_object('ds_dados_apoio',acao.ds_dados_apoio
                    , 'ds_iucn',coalesce(iucn_ameaca.ds_iucn_ameaca,'')
                    , 'cd_iucn',coalesce(iucn_ameaca.cd_iucn_ameaca,'')
                    ) )::text as json_ameacas
                from salve.ficha_ameaca
                         inner join salve.dados_apoio as acao on acao.sq_dados_apoio = ficha_ameaca.sq_criterio_ameaca_iucn
                         left outer join salve.iucn_ameaca_apoio on iucn_ameaca_apoio.sq_dados_apoio = ficha_ameaca.sq_criterio_ameaca_iucn
                         left outer join salve.iucn_ameaca on iucn_ameaca.sq_iucn_ameaca = iucn_ameaca_apoio.sq_iucn_ameaca
                where ficha_ameaca.sq_ficha = ficha.sq_ficha
                ) as ameacas on true

                -- selecionar os usos
                left join lateral (
                select json_object_agg(ficha_uso.sq_uso::text, json_build_object('ds_uso',uso.ds_uso
                    , 'ds_iucn',coalesce(iucn_uso.ds_iucn_uso,'')
                    , 'cd_iucn',coalesce(iucn_uso.cd_iucn_uso,'')
                    , 'st_nacional', (case when uso.ds_uso ilike 'nacional%' or uso.ds_uso ilike '% nacional%' then 'TRUE' else 'FALSE' END)
                    , 'st_internacional', (case when uso.ds_uso ilike '%internacional%' then 'TRUE' else 'FALSE' END)
                    , 'st_subsistencia', (case when uso.ds_uso ilike '%subsistência%' then 'TRUE' else 'FALSE' END)
                   ) )::text as json_usos
                    , sum( case when uso.co_uso in ('1.1','1.5') then 1 else 0 end ) as qtd_uso_subsistencia
                    , count( uso.co_uso ) as qtd_total_uso
                from salve.ficha_uso
                    inner join salve.uso on uso.sq_uso = ficha_uso.sq_uso
                    left  join salve.iucn_uso on iucn_uso.sq_iucn_uso = uso.sq_iucn_uso
                    where ficha_uso.sq_ficha = ficha.sq_ficha
                ) as usos on true

                where ficha.sq_ficha in ( ${listFichas.join(',')} )
                order by nm_cientifico
               """
        // println cmdSql

        List problemasEncontrados = []
        println ' '
        sqlService.execSql(cmdSql).each { row ->

            println 'Ficha ' + row.nm_cientifico + ' ' + new Date().format('HH:mm:ss')

            // fazer tratamento e ajustes nos valores calculados ou que necessitam de alguma regra/formatação antes serem exportados
            _calcValues(row)

            // criar e popular os arquivos CSVs
            if ( true ) {
                _appendCredits(row, rndId, problemasEncontrados) // tem que ser chamado antes de _appendAssessments()
                _appendAllFields(row, rndId, problemasEncontrados)
                _appendHabitats(row, rndId, problemasEncontrados)
                _appendAssessments(row, rndId, problemasEncontrados)
                _appendCommonnames(row, rndId, problemasEncontrados)
                _appendConservationneeded(row, rndId, problemasEncontrados)
                _appendCountries(row, rndId, problemasEncontrados)  // faltam alguns campos obrigatorios
                _appendFao(row, rndId, problemasEncontrados)
                _appendLme(row, rndId, problemasEncontrados)
                _appendPlantspecific(row, rndId, problemasEncontrados) // Esse não se aplica a avalição da fauna
                _appendResearchneeded(row, rndId, problemasEncontrados)
                _appendSynonyms(row, rndId, problemasEncontrados)
                _appendTaxonomy(row, rndId, problemasEncontrados)
                _appendThreats(row, rndId, problemasEncontrados)
                _appendUseTrade(row, rndId, problemasEncontrados)
                _appendReferences(row, rndId, problemasEncontrados)


            }
            // testar individualmente
            //_appendAssessments(row, rndId, problemasEncontrados)
            //_appendHabitats(row,            rndId,  problemasEncontrados)
            //_appendConservationneeded(row,  rndId,  problemasEncontrados )
            //_appendResearchneeded(row,      rndId,  problemasEncontrados)
            //_appendThreats(row, rndId, problemasEncontrados)
            //_appendUseTrade(row, rndId, problemasEncontrados)
            //_appendLme(row, rndId, problemasEncontrados)
            //_appendCredits(row, rndId, problemasEncontrados)    // falta implementar
            //_appendFao(row, rndId, problemasEncontrados)        // falta implementar
            //_appendCountries(row, rndId, problemasEncontrados)  // faltam alguns campos obrigatorios
            // _appendSynonyms(row, rndId, problemasEncontrados)



        }

        boolean existeFichaSemRevisao = false
        boolean existeCampoObrigatoriaEmBranco = false
        if (problemasEncontrados) {
            println 'PROBLEMAS ENCONTRADOS'
            println '  - ' + problemasEncontrados.join('\n  - ')
            // se houver alguma ficha das selecionadas para o envio com alguma tradução não revisada ou algum campo obrigatório não preenchido, não gerar o aquivo zip
            List problemasCriticos = []
            if( problemasEncontrados.join(' ') =~ /não revisada\./){
                println '  - Arquivo ZIP não foi gerado devido a existencia de traducao nao revisada.'
                problemasCriticos.push('tradução não revisada')
                existeFichaSemRevisao = true
            }
            if( problemasEncontrados.join(' ') =~ /obrigatório\./){
                println '  - Arquivo ZIP não foi gerado devido a existencia de campos obrigatórios não preenchidos.'
                existeCampoObrigatoriaEmBranco = true
                problemasCriticos.push('campo obrigatório não preenchido')
            }
            if( problemasCriticos && rndId ){
                problemasEncontrados.push('<span style="color:red;">Arquivo ZIP não foi gerado devido a(s) seguinte(s) inconsistências(s): </span><b>' + problemasCriticos.join(', ')+'</b>.' )

                _enviarEmail(res.data.email, '', problemasEncontrados)
            }
            println '  '
        }

        // durante os testes nao existe o rndId
        if ( rndId ) {


            String zipFileName = 'não gerado'
            if( ! existeFichaSemRevisao && ! existeCampoObrigatoriaEmBranco ) {
                // zipar os arquivos csv gerados
                zipFileName = _gerarZip(rndId)
                println '  - NOME DO ARQUIVO ZIP'
                println '  - ' + zipFileName

                _enviarEmail(res.data.email, zipFileName, problemasEncontrados)
                println '  - ENVIAR EMAIL PARA ' + res.data.email

                // mover o arquivo zip para diretorio iucn
                File zipFile = new File(zipFileName)
                //println ' '
                //println 'MOVER'
                Path source = zipFile.toPath()
                Path target = new File(grailsApplication.config.iucn.dir + zipFile.getName()).toPath()
                //println 'de:' + source
                //println 'para:' + target
                Files.move(source, target, StandardCopyOption.REPLACE_EXISTING)
            }

            // gravar tranmissao
            IucnTransmissao iucnTransmissao = new IucnTransmissao()
            iucnTransmissao.dtTransmissao = new Date()
            iucnTransmissao.pessoa = PessoaFisica.get(res.data.sqPessoa.toLong())
            iucnTransmissao.noArquivoZip = zipFileName
            if (problemasEncontrados) {
                iucnTransmissao.txLog = problemasEncontrados.join('\n')
            }
            iucnTransmissao.validate()
            iucnTransmissao.save(flush: true)
            listFichas.each { sqFicha ->
                Ficha ficha = Ficha.get(sqFicha)
                new IucnTransmissaoFicha(ficha: ficha, iucnTransmissao: iucnTransmissao).save(flush: true)
            }

        }
        println 'Fim processamento da(s) ficha(s) selecionadas para SIS Connect/IUCN às ' + new Date().format('HH:mm:ss')
        println ' '

    }

    /**
     * método para adicionar uma linha no arquivo assessments.csv
     * @param row
     */
    protected void _appendAssessments(groovy.sql.GroovyRowResult row, String rndId, List problemasEncontrados) {
        List columns = [
            [name: 'BiogeographicRealm.realm', field: []]
            , [name: 'ConservationActionsDocumentation.narrative'               , field: ['ds_acao_conservacao']]
            , [name: 'HabitatDocumentation.narrative'                           , field: ['ds_uso_habitat'],required: true]
            , [name: 'MapStatus.status'                                         , field: ['map_status'],required: true]
            , [name: 'PopulationDocumentation.narrative'                        , field: ['ds_populacao'],required: true]
            , [name: 'PopulationTrend.value'                                    , field: ['ds_tendencia_populacional'],required: true]
            , [name: 'RangeDocumentation.narrative'                             , field: ['ds_distribuicao_geo_global', 'ds_distribuicao_geo_nacional'],required:true]
            , [name: 'RedListAssessmentDate.value'                              , field: ['dt_ultima_avaliacao'],required: true]
            , [name: 'RedListCriteria.critVersion'                              , field: [],default:'3.1',required: true]
            , [name: 'RedListCriteria.dataDeficientReason', field: []]
            , [name: 'RedListCriteria.isManual'                                 , field: [],default:'TRUE', required: true]
            , [name: 'RedListCriteria.manualCategory'                           , field: ['cd_categoria'], required: true]
            , [name: 'RedListCriteria.manualCriteria', field: []]
            , [name: 'RedListCriteria.manualCriteriaString'                     , field: ['ds_criterio']]
            , [name: 'RedListCriteria.possiblyExtinct'                          , field: ['st_possivelmente_extinta']]
            , [name: 'RedListCriteria.possiblyExtinctCandidate', field: []]
            , [name: 'RedListCriteria.yearLastSeen', field: []]
            , [name: 'RedListRationale.value'                                   , field: ['ds_justificativa_final'],required: true]
            , [name: 'RedListReasonsForChange.catCritChanges', field: []]

            , [name: 'RedListReasonsForChange.changeReasons'                    , field: ['ds_motivo_mudanca']]
            , [name: 'RedListReasonsForChange.otherReason', field: []]
            , [name: 'RedListReasonsForChange.timeframe'                , field: []]
            , [name: 'ReasonForChangeJustification.narrative'           , field: ['ds_just_mudanca_categoria']]
            , [name: 'RedListReasonsForChange.type'                     , field: ['st_motivo_mudanca_genuino']]

            , [name: 'System.value'                                     , field: ['ds_habitats'],required: true]
            , [name: 'ThreatsDocumentation.value'                       , field: ['ds_ameaca'],required: true]
            , [name: 'UseTradeDocumentation.value'                      , field: ['ds_uso']]
            , [name: 'language.value'                                   , field: [],default:'English',required: true]
            , [name: 'internal_taxon_id'                                , field: ['sq_taxon']]
        ]
        _createOrAppend('assessments.csv', columns, row, rndId, problemasEncontrados)
    }

    /**
     * método para adicionar uma linha no arquivo allfields.csv
     * @param row
     */
    protected void _appendAllFields(groovy.sql.GroovyRowResult row, String rndId, List problemasEncontrados) {
        List columns = [
            [name: 'AOO.justification', field: ['ds_justificativa_aoo_eoo']]
            , [name: 'AOO.range', field: ['vl_aoo']]
            , [name: 'AOOContinuingDecline.isInContinuingDecline', field: ['ds_tendencia_aoo']]
            , [name: 'AOOContinuingDecline.justification', field: []]
            , [name: 'AOOContinuingDecline.qualifier', field: []]
            , [name: 'AOODetails.breedingRange', field: []]
            , [name: 'AOODetails.breedingRangeJustification', field: []]
            , [name: 'AOODetails.nonbreedingRange', field: []]
            , [name: 'AOODetails.nonbreedingRangeJustification', field: []]
            , [name: 'AOOExtremeFluctuation.isFluctuating', field: ['ds_flutuacao_aoo']]
            , [name: 'AOOExtremeFluctuation.justification', field: []]
            , [name: 'AreaRestricted.isRestricted', field: []]
            , [name: 'AreaRestricted.justification', field: []]
            , [name: 'AvgAnnualFecundity.fecundity', field: ['vl_tamanho_prole']]
            , [name: 'AvgReproductiveAge.age', field: []]
            , [name: 'AvgReproductiveAge.units', field: []]
            , [name: 'BirthSize.size', field: []]
            , [name: 'Congregatory.value', field: []]
            , [name: 'CropWildRelative.isRelative', field: []]
            , [name: 'CurrentTrendDataDerivation.value', field: []]
            , [name: 'DepthLower.limit', field: ['nu_min_batimetria']]
            , [name: 'DepthUpper.limit', field: ['nu_max_batimetria']]
            , [name: 'DepthZone.depthZone', field: ['depth_zone']]
            , [name: 'EOO.justification', field: ['map_status']]
            , [name: 'EOO.range', field: ['vl_eoo']]
            , [name: 'EOOContinuingDecline.isContinuingDecline', field: ['ds_tendencia_eoo']]
            , [name: 'EOOContinuingDecline.justification', field: []]
            , [name: 'EOOContinuingDecline.qualifier', field: []]
            , [name: 'EOOExtremeFluctuation.isFluctuating', field: ['ds_flutuacao_eoo']]
            , [name: 'EOOExtremeFluctuation.justification', field: []]

            , [name: 'EggLaying.layEggs', field: ['ds_modo_reproducao_oviparo']]

            , [name: 'ElevationLower.limit', field: ['nu_min_altitude']]

            , [name: 'ElevationUpper.limit', field: ['nu_max_altitude']]

            , [name: 'ExtinctionProbabilityGenerations3.justification', field: ['ds_metodo_calc_perc_extin_br3']]
            , [name: 'ExtinctionProbabilityGenerations3.range', field: ['ds_prob_extincao_brasil3']]

            , [name: 'ExtinctionProbabilityGenerations5.justification', field: ['ds_metodo_calc_perc_extin_br5']]
            , [name: 'ExtinctionProbabilityGenerations5.range', field: ['ds_prob_extincao_brasil5']]

            , [name: 'ExtinctionProbabilityYears100.justification', field: ['ds_metodo_calc_perc_extin_br100']]
            , [name: 'ExtinctionProbabilityYears100.range', field: ['ds_prob_extincao_brasil100']]

            , [name: 'FemaleMaturityAge.age', field: ['vl_maturidade_sexual_femea']]
            , [name: 'FemaleMaturityAge.units', field: ['ds_unid_maturidade_sexual_femea']]

            , [name: 'FemaleMaturitySize.size', field: ['vl_comprimento_femea']] // centimetros

            , [name: 'FreeLivingLarvae.hasStage', field: []]
            , [name: 'GenerationLength.justification', field: ['ds_metod_calc_tempo_geracional']]
            , [name: 'GenerationLength.quality', field: []]
            , [name: 'GenerationLength.range', field: []]

            , [name: 'GestationTime.time', field: ['vl_tempo_gestacao']]
            , [name: 'GestationTime.units', field: ['ds_tempo_gestacao']]

            , [name: 'HabitatContinuingDecline.isDeclining', field: []]
            , [name: 'HabitatContinuingDecline.justification', field: []]
            , [name: 'HabitatContinuingDecline.qualifier', field: []]
            , [name: 'IdentificationDescription.text', field: ['ds_diagnostico_morfologico']]

            , [name: 'InPlaceEducationControlled.note', field: []]
            , [name: 'InPlaceEducationControlled.value', field: []]
            , [name: 'InPlaceEducationInternationalLegislation.note', field: []]
            , [name: 'InPlaceEducationInternationalLegislation.value', field: []]
            , [name: 'InPlaceEducationSubjectToPrograms.note', field: []]
            , [name: 'InPlaceEducationSubjectToPrograms.value', field: []]
            , [name: 'InPlaceLandWaterProtectionAreaPlanned.note', field: []]
            , [name: 'InPlaceLandWaterProtectionAreaPlanned.value', field: []]
            , [name: 'InPlaceLandWaterProtectionInPA.note', field: []]
            , [name: 'InPlaceLandWaterProtectionInPA.value'                         , field: ['st_presenca_uc']]
            , [name: 'InPlaceLandWaterProtectionInvasiveControl.note', field: []]
            , [name: 'InPlaceLandWaterProtectionInvasiveControl.value', field: []]
            , [name: 'InPlaceLandWaterProtectionPercentProtected.note', field: []]
            , [name: 'InPlaceLandWaterProtectionPercentProtected.value', field: []]
            , [name: 'InPlaceLandWaterProtectionSitesIdentified.note', field: []]
            , [name: 'InPlaceLandWaterProtectionSitesIdentified.value', field: []]
            , [name: 'InPlaceResearchMonitoringScheme.note', field: []]
            , [name: 'InPlaceResearchMonitoringScheme.value', field: []]
            , [name: 'InPlaceResearchRecoveryPlan.note'                                 , field: ['st_plano_acao']]
            , [name: 'InPlaceResearchRecoveryPlan.value', field: []]
            , [name: 'InPlaceSpeciesManagementExSitu.note', field: []]
            , [name: 'InPlaceSpeciesManagementExSitu.value', field: []]
            , [name: 'InPlaceSpeciesManagementHarvestPlan.note', field: []]
            , [name: 'InPlaceSpeciesManagementHarvestPlan.value', field: []]
            , [name: 'InPlaceSpeciesManagementReintroduced.note', field: []]
            , [name: 'InPlaceSpeciesManagementReintroduced.value', field: []]

            , [name: 'LiveBirth.liveBirth'                                              , field: ['ds_modo_reproducao_viviparo']]

            , [name: 'Livelihoods.noInfo', field: []]
            , [name: 'LocationContinuingDecline.inDecline', field: ['ds_tendencia_num_localizacoes']]
            , [name: 'LocationContinuingDecline.justification', field: []]
            , [name: 'LocationContinuingDecline.qualifier', field: []]
            , [name: 'LocationExtremeFluctuation.isFluctuating', field: ['ds_flutuacao_num_localizacoes']]
            , [name: 'LocationExtremeFluctuation.justification', field: []]
            , [name: 'LocationsNumber.justification', field: ['ds_ameaca']]
            , [name: 'LocationsNumber.range', field: ['nu_localizacoes']]

            , [name: 'Longevity.longevity', field: ['vl_longevidade']]
            , [name: 'Longevity.units', field: ['ds_longevidade']]

            , [name: 'MaleMaturityAge.age', field: ['vl_maturidade_sexual_macho']]
            , [name: 'MaleMaturityAge.units', field: ['ds_unid_maturidade_sexual_macho']]
            , [name: 'MaleMaturitySize.size', field: ['vl_comprimento_macho']] // em centimetros

            , [name: 'MatureIndividualsSubpopulation.value', field: []]
            , [name: 'MaxSize.size'                                         , field: ['vl_comprimento_max']] //  em centrimetros do que for maior entre o macho e femea
            , [name: 'MaxSubpopulationSize.range'                           , field: ['nu_indivi_subpopulacao_max']]
            , [name: 'MovementPatterns.pattern', field: []]
            , [name: 'NaturalMortality.value', field: []]
            , [name: 'NoThreats.noThreats'                                  , field: ['st_ameaca'],required: true]
            , [name: 'NonConsumptiveUse.isNonConsumptiveUse', field: []]
            , [name: 'NonConsumptiveUseDescription.narrative', field: []]
            , [name: 'NotUtilized.isNotUtilized'                            , field: ['st_uso']]
            , [name: 'Parthenogenesis.exhibitsParthenogenesis'              , field: ['ds_modo_reproducao_partenogenese']]

            , [name: 'PopulationContinuingDecline.isDeclining', field: ['ds_tenden_num_individuo_maduro']]
            , [name: 'PopulationContinuingDecline.justification', field: []]
            , [name: 'PopulationContinuingDecline.qualifier', field: []]
            , [name: 'PopulationDeclineGenerations1.justification', field: []]
            , [name: 'PopulationDeclineGenerations1.qualifier', field: []]
            , [name: 'PopulationDeclineGenerations1.range', field: ['ds_perc_declinio_populacional1']]
            , [name: 'PopulationDeclineGenerations2.justification', field: []]
            , [name: 'PopulationDeclineGenerations2.qualifier', field: []]
            , [name: 'PopulationDeclineGenerations2.range', field: ['ds_perc_declinio_populacional2']]

            , [name: 'PopulationDeclineGenerations3.justification', field: []]
            , [name: 'PopulationDeclineGenerations3.qualifier', field: []]
            , [name: 'PopulationDeclineGenerations3.range', field: ['ds_perc_declinio_populacional3']]

            , [name: 'PopulationExtremeFluctuation.isFluctuating', field: ['ds_flut_extrema_indiv_maduro']]
            , [name: 'PopulationExtremeFluctuation.justification', field: []]
            , [name: 'PopulationIncreaseRate.narrative', field: []]
            , [name: 'PopulationReductionFuture.direction', field: []]
            , [name: 'PopulationReductionFuture.justification', field: []]

            , [name: 'PopulationReductionFuture.qualifier'                      , field: ['ds_unid_reducao_popul_futura']]
            , [name: 'PopulationReductionFuture.range'                          , field: ['nu_reducao_popul_futura']]

            , [name: 'PopulationReductionFutureBasis.detail', field: []]
            , [name: 'PopulationReductionFutureBasis.value', field: []]
            , [name: 'PopulationReductionPast.direction', field: []]
            , [name: 'PopulationReductionPast.justification', field: []]
            , [name: 'PopulationReductionPast.qualifier', field: []]
            , [name: 'PopulationReductionPast.range'                        , field: ['nu_reducao_popul_passada_rev']]
            , [name: 'PopulationReductionPastBasis.detail', field: []]
            , [name: 'PopulationReductionPastBasis.value', field: []]

            , [name: 'PopulationReductionPastCeased.value'                  , field: ['st_population_reduction_past_ceased']]
            , [name: 'PopulationReductionPastReversible.value'              , field: ['st_population_reduction_past_reversible']]
            , [name: 'PopulationReductionPastUnderstood.value'              , field: ['st_population_reduction_past_understood']]

            , [name: 'PopulationReductionPastandFuture.direction', field: []]
            , [name: 'PopulationReductionPastandFuture.justification', field: []]
            , [name: 'PopulationReductionPastandFuture.numYears', field: []]
            , [name: 'PopulationReductionPastandFuture.qualifier', field: []]
            , [name: 'PopulationReductionPastandFuture.range', field: ['nu_reducao_popul_pass_futura']]
            , [name: 'PopulationReductionPastandFutureBasis.detail', field: []]
            , [name: 'PopulationReductionPastandFutureBasis.value', field: []]
            , [name: 'PopulationReductionPastandFutureCeased.value', field: []]
            , [name: 'PopulationReductionPastandFutureReversible.value', field: []]
            , [name: 'PopulationReductionPastandFutureUnderstood.value', field: []]
            , [name: 'PopulationSize.range', field: ['nu_individuo_maduro']]
            , [name: 'ReproductivePeriodicity.value', field: ['vl_intervalo_nascimento']]

            , [name: 'SevereFragmentation.isFragmented', field: ['ds_populacao_fragmentada']]
            , [name: 'SevereFragmentation.justification', field: []]
            , [name: 'SubpopulationContinuingDecline.isDeclining', field: ['ds_tendencia_num_subpopulacao']]
            , [name: 'SubpopulationContinuingDecline.justification', field: []]
            , [name: 'SubpopulationContinuingDecline.qualifier', field: []]
            , [name: 'SubpopulationExtremeFluctuation.isFluctuating', field: ['ds_flut_extrema_sub_populacao']]
            , [name: 'SubpopulationExtremeFluctuation.justification', field: []]
            , [name: 'SubpopulationNumber.justification', field: []]
            , [name: 'SubpopulationNumber.range', field: ['nu_subpopulacao']]
            , [name: 'SubpopulationSingle.value', field: ['nu_indivi_subpopulacao_perc']]
            , [name: 'ThreatsUnknown.value', field: ['st_ameaca_desconhecida'],required: true]
            , [name: 'TrendInDomesticOfftake.value', field: []]
            , [name: 'TrendInWildOfftake.value', field: []]
            , [name: 'UTCaptiveHarvest.value', field: []]
            , [name: 'UTHarvestTrendComments.narrative', field: []]
            , [name: 'UTIntlCommercialValue.value', field: []]
            , [name: 'UTLocalLivelihood.localcommercial', field: []]
            , [name: 'UTLocalLivelihood.localcommercialDetail', field: []]
            , [name: 'UTLocalLivelihood.subsistence', field: ['st_uso_subsistencia']]
            , [name: 'UTLocalLivelihood.subsistenceRationale', field: []]
            , [name: 'UTNatlCommercialValue.value', field: []]
            , [name: 'UseTradeNoInformation.value', field: ['ds_uso_nao_encontrado']]
            , [name: 'WaterBreeding.value', field: []]
            , [name: 'YearOfPopulationEstimate.value', field: []]
            , [name: 'internal_taxon_id', field: ['sq_taxon']]
        ]

        _createOrAppend('allfields.csv', columns, row, rndId, problemasEncontrados)
        /*
        if( !row ){
            return;
        }
        String fileName = '/data/salve-estadual/temp/'+rndId+'_allfields.csv'
        File file = new File( fileName )
        if( ! file.exists() ){
            file.write( columns.name.join(',')+'\n','UTF-8' )
        }

        List listValues = []
        columns.each { col ->
            try {
                String value = ''
                if (col.field) {
                    List fieldValues = []
                    col.field.each { field ->
                        fieldValues.push(row[field] ?: '')
                    }
                    value = fieldValues.join('\n')
                }
                listValues.push(_tratarValue(value))
            } catch (Exception e ){
                println ' '
                println 'ERRO - IucnService._appendAllFields'
                println e.getMessage()
            }
        }
        file.append( listValues.join(',') +'\n','UTF-8' )
         */
    }

    /**
     * método para adicionar uma linha no arquivo habitats.csv
     * @param row
     */
    protected void _appendHabitats(groovy.sql.GroovyRowResult row, String rndId, List problemasEncontrados) {
        List columns = [
            [name: 'GeneralHabitats.GeneralHabitatsSubfield.GeneralHabitatsLookup', field: ['cd_iucn'],required: true]
            , [name: 'GeneralHabitats.GeneralHabitatsSubfield.GeneralHabitatsName', field: ['ds_iucn']]
            , [name: 'GeneralHabitats.GeneralHabitatsSubfield.majorImportance', field: []]
            , [name: 'GeneralHabitats.GeneralHabitatsSubfield.season', field: []]
            , [name: 'GeneralHabitats.GeneralHabitatsSubfield.suitability', field: [], default: 'suitable',required: true]
            , [name: 'internal_taxon_id', field: ['sq_taxon']]
        ]
        _createOrAppendJson('habitats.csv', 'json_habitats', columns, row, rndId, problemasEncontrados)
        /*if( !row ){
            return
        }
        String fileName = '/data/salve-estadual/temp/'+rndId+'_habitats.csv'
        File file = new File( fileName )
        if( ! file.exists() ){
            file.write( columns.name.join(',')+'\n','UTF-8' )
        }
        if( ! row.json_habitats  ) {
            return
        }
        // parse json vindo do banco de dados
        JSONObject data
        try {
            data = JSON.parse( row.json_habitats )
        } catch(Exception e ){
            println e.getMessage()
            problemasEncontrados.push('Erro ao adicionar os habitats\n\n'+e.getMessage())
            return
        }
        data.each { id, rowJson ->
            rowJson.sq_taxon = row.sq_taxon
            // criticar se o registro da tabela de apoio não tiver na tabela iucn_dados_apoio
            if ( ! rowJson.ds_iucn ) {
                problemasEncontrados.push('Registro id '+ id + ' da tabela DADOS_APOIO não cadastrado na tabela IUCN_DADOS_APOIO')
            } else {
                List listValues = []
                columns.each { col ->
                    try {
                        String value = ''
                        if (col.field) {
                            List fieldValues = []
                            col.field.each { field ->
                                if (rowJson[field]) {
                                    fieldValues.push(rowJson[field] ?: '')
                                } else if (row[field]) {
                                    fieldValues.push(row[field] ?: '')
                                }
                            }
                            value = fieldValues.join('\n')
                        } else if (col.default) {
                            value = col.default
                        }
                        listValues.push(_tratarValue(value))
                    } catch (Exception e) {
                        println ' '
                        println 'ERRO - IucnService._appendHabitas'
                        println e.getMessage()
                    }
                }
                file.append(listValues.join(',') + '\n', 'UTF-8')
            }
        }

        */
    }

    /**
     * método para adicionar uma linha no arquivo threats.csv
     * @param row
     */
    protected void _appendThreats(groovy.sql.GroovyRowResult row, String rndId, List problemasEncontrados) {
        List columns = [
            [name: 'Threats.ThreatsSubfield.StressesSubfield.stress', field: []]
            , [name: 'Threats.ThreatsSubfield.StressesSubfield.stressdesc', field: []]
            , [name: 'Threats.ThreatsSubfield.ThreatsLookup', field: ['cd_iucn'],required: true]
            , [name: 'Threats.ThreatsSubfield.ThreatsName', field: ['ds_iucn']]
            , [name: 'Threats.ThreatsSubfield.ancestry', field: []]
            , [name: 'Threats.ThreatsSubfield.ias', field: []]
            , [name: 'Threats.ThreatsSubfield.internationalTrade', field: []]
            , [name: 'Threats.ThreatsSubfield.scope', field: []]
            , [name: 'Threats.ThreatsSubfield.severity', field: []]
            , [name: 'Threats.ThreatsSubfield.text', field: []]
            , [name: 'Threats.ThreatsSubfield.timing', field: [], default: 'Unknown',required: true]
            , [name: 'Threats.ThreatsSubfield.virus', field: []]
            , [name: 'internal_taxon_id', field: ['sq_taxon']]
        ]
        _createOrAppendJson('threats.csv', 'json_ameacas', columns, row, rndId, problemasEncontrados)
        /*
       if( !row ) {
            return
        }
        String fileName = '/data/salve-estadual/temp/'+rndId+'_threats.csv'
        File file = new File( fileName )
        if( ! file.exists() ){
            //println ' '
            //println 'Arquivo ' + fileName + ' criado'
            file.write( columns.name.join(',')+'\n','UTF-8' )
        }
        List listValues = []
        columns.each { col ->
            try {
                //println 'coluna..: ' + col.name
                //println 'campo(s): ' + col.field.join(', ')
                String value = ''
                if (col.field) {
                    List fieldValues = []
                    col.field.each { field ->
                        fieldValues.push(row[field] ?: '')
                    }
                    value = fieldValues.join('\n')
                }
                listValues.push(_tratarValue(value))
            } catch (Exception e ){
                println ' '
                println 'ERRO - IucnService._appendThreats'
                println e.getMessage()
            }
        }
        file.append( listValues.join(',') +'\n','UTF-8' )
         */
    }
    /**
     * método para adicionar uma linha no arquivo usetrade.csv
     * @param row
     */
    protected void _appendUseTrade(groovy.sql.GroovyRowResult row, String rndId, List problemasEncontrados) {
        List columns = [
            ['name': 'UTEndUse.UTEndUseSubfield.UTEndUseLookup', field: ['cd_iucn']]
            , ['name': 'UTEndUse.UTEndUseSubfield.UTEndUseName', field: ['ds_iucn']]
            , ['name': 'UTEndUse.UTEndUseSubfield.international', field: ['st_internacional']]
            , ['name': 'UTEndUse.UTEndUseSubfield.national', field: ['st_nacional']]
            , ['name': 'UTEndUse.UTEndUseSubfield.other', field: []]
            , ['name': 'UTEndUse.UTEndUseSubfield.subsistence', field: ['st_subsistencia']]
            , ['name': 'internal_taxon_id', field: ['sq_taxon']]
        ]
        _createOrAppendJson('usetrade.csv', 'json_usos', columns, row, rndId, problemasEncontrados)
    }

    /**
     * método para adicionar uma linha no arquivo conservationneeded.csv
     * @param row
     */
    protected void _appendConservationneeded(groovy.sql.GroovyRowResult row, String rndId, List problemasEncontrados) {
        List columns = [
            [name: 'ConservationActions.ConservationActionsSubfield.ConservationActionsLookup', field: ['cd_iucn']]
            , [name: 'ConservationActions.ConservationActionsSubfield.ConservationActionsName', field: ['ds_iucn']]
            , [name: 'ConservationActions.ConservationActionsSubfield.note', field: ['ds_notas']]
            , [name: 'internal_taxon_id', field: ['sq_taxon']]
        ]
        _createOrAppendJson('conservationneeded.csv', 'json_acoes_conservacao', columns, row, rndId, problemasEncontrados)
    }

    /**
     * método para adicionar uma linha no arquivo countries.csv
     * @param row
     */
    protected void _appendCountries(groovy.sql.GroovyRowResult row, String rndId, List problemasEncontrados) {
        List columns = [
             ['name':'CountryOccurrence.CountryOccurrenceSubfield.CountryOccurrenceLookup'  ,field:['cd_pais'],required: true]
            ,['name':'CountryOccurrence.CountryOccurrenceSubfield.CountryOccurrenceName'    ,field:['ds_pais']]
            ,['name':'CountryOccurrence.CountryOccurrenceSubfield.formerlyBred',field:[]]
            ,['name':'CountryOccurrence.CountryOccurrenceSubfield.origin'                   ,field:['ds_country_origin'],required: true]
            ,['name':'CountryOccurrence.CountryOccurrenceSubfield.presence'                 ,field:['st_presence_country'],required: true]
            ,['name':'CountryOccurrence.CountryOccurrenceSubfield.seasonality',field:[]]
            ,['name':'internal_taxon_id'                                                    ,field:['sq_taxon'],required: true]
        ]
        // Não tem campo correspondente no SALVE. Para espécies endêmicas, o código do Brasil no SIS é "BR" que pode ser eviado como defaut
        if( row.st_presence_country ) {
                Map paises = ['row_1': ['cd_pais'              : 'BR0'
                                        , 'ds_pais'            : 'Brazil1'
                                        , 'ds_iucn'            : 'Brazil2'
                                        , 'ds_country_origin'  : 'Native'
                                        , 'st_presence_country': row.st_presence_country
                                        ,]]
                row.json_paises = (paises as JSON).toString()
        } else {
            row.json_paises = ''
        }
        _createOrAppendJson('countries.csv', 'json_paises', columns, row, rndId, problemasEncontrados)
    }

    /**
     * método para adicionar uma linha no arquivo credits.csv
     * @param row
     */
    protected void _appendCredits(groovy.sql.GroovyRowResult row, String rndId, List problemasEncontrados) {
        List columns = [
             ['name':'Order'                , field:['nu_ordem'],required: true]
            ,['name':'affiliation'          , field:['no_instituicao']]
            ,['name':'credit_type'          , field:['ds_tipo_credito'],required: true]
            ,['name':'email'                , field:['de_email'],required: true]
            ,['name':'firstName'            , field:['no_primeiro'],required: true]
            ,['name':'initials'             , field:['no_iniciais']]
            ,['name':'internal_taxon_id'    , field:['sq_taxon']]
            ,['name':'lastName'             , field:['no_ultimo'],required: true]
            ,['name':'nickname'             , field:[]]
            ,['name':'user_id'              , field:['sq_pessoa'],required: true]
        ]
        // ler os participantes da AVALIACAO e os 2 validadores
        String cmdSql = """with passo1 as (
                    -- encontrar a ultima oficina de avaliacao
                    select oficina.sq_oficina, oficina.dt_fim from salve.oficina
                    inner join salve.oficina_ficha on oficina.sq_oficina = oficina_ficha.sq_oficina
                    inner join salve.dados_apoio as tipo on tipo.sq_dados_apoio = oficina.sq_tipo_oficina
                    and oficina_ficha.sq_ficha = :sqFicha
                    and tipo.cd_sistema = 'OFICINA_AVALIACAO'
                    order by dt_fim desc limit 1
                    )
                    -- ler os participantes
                    select distinct
                    case when papel.cd_sistema = 'AVALIADOR' then 'Contributor'  else
                        case when papel.cd_sistema = 'FACILITADOR' then 'Facilitator' else
                        case when papel.cd_sistema like 'COLABORADOR%' then 'Contributor' else '' end
                            end end as ds_tipo_credito
                    ,participante.no_pessoa
                    ,participante.no_instituicao
                    ,lower(participante.de_email) as de_email
                    ,participante.sq_pessoa
                    ,to_char(passo1.dt_fim,'DD/MM/YYYY') as dt_ultima_avaliacao
                    from salve.oficina_ficha_participante ofp
                    inner join salve.oficina_ficha oficina_ficha on oficina_ficha.sq_oficina_ficha = ofp.sq_oficina_ficha
                    inner join passo1 on passo1.sq_oficina = oficina_ficha.sq_oficina
                    inner join salve.dados_apoio papel on papel.sq_dados_apoio = ofp.sq_papel_participante
                    inner join salve.vw_oficina_participante participante on participante.sq_oficina_participante = ofp.sq_oficina_participante
                    order by 1,2"""


        Map sqlParams = [sqFicha:row.sq_ficha.toLong()]
        Map participantes = [:]
        Integer ordem = 1
        // colocar o ICMBIO como acessor
        participantes['row_avaliador_' + ordem ] = [
            no_pessoa           : 'Instituto Chico Mendes de Conservação da Biodiversidade',
            no_instituicao      : 'Instituto Chico Mendes de Conservação da Biodiversidade',
            dt_ultima_avaliacao :'',
            nu_ordem            :  ordem,
            sq_pessoa           :  3164,
            ds_tipo_credito     : 'Assessor',
            no_primeiro         : 'Instituto Chico Mendes de Conservação da Biodiversidade',
            no_ultimo           : 'ICMBio',
            de_email            : 'salve@icmbio.gov.br',
            no_iniciais         : 'ICMBio',
            ds_iucn             : '-' // evitar criticar com a tradução da tabela de apoio
        ]
        ordem++

        row.dt_ultima_avaliacao = ''
        sqlService.execSql(cmdSql, sqlParams ).eachWithIndex{ line, index ->
            // preencher a data da última avaliação
            row.dt_ultima_avaliacao = line?.dt_ultima_avaliacao ?: row.dt_ultima_avaliacao//no formato dd/mm/yyyyy
            if( line.ds_tipo_credito != '' ) {
                line.nu_ordem           = ordem
                line.sq_pessoa          = line.sq_pessoa
                line.ds_tipo_credito    = line.ds_tipo_credito ?: ''
                line.no_primeiro        = _getFirstName(line.no_pessoa)
                line.no_ultimo          = _getLastName(line.no_pessoa)
                line.de_email           =  line.de_email? line.de_email.toString().toLowerCase() : ''
                line.no_iniciais        = _getInitials(line.no_pessoa)
                line.ds_iucn            = '-' // evitar criticar com a tradução da tabela de apoio
                participantes['row_avaliador_' + ordem ] = line
                ordem++
            }
        }

        // ler os 2 VALIDADORES
        cmdSql = """select pf.sq_pessoa
                    , pf.no_pessoa
                    , a.de_email
                    ,'Reviewer' as ds_tipo_credito
                    ,'' as no_instituicao
                    from salve.ciclo_avaliacao_validador a
                    inner join salve.validador_ficha b on b.sq_ciclo_avaliacao_validador = a.sq_ciclo_avaliacao_validador
                    inner join salve.vw_pessoa_fisica as pf on pf.sq_pessoa = a.sq_validador
                    inner join salve.oficina_ficha c on c.sq_oficina_ficha = b.sq_oficina_ficha
                    inner join salve.oficina d on d.sq_oficina = c.sq_oficina
                    inner join salve.dados_apoio as tipo on tipo.sq_dados_apoio = d.sq_tipo_oficina
                    left  join salve.dados_apoio as situacao on situacao.sq_dados_apoio = a.sq_situacao_convite
                    where c.sq_ficha = :sqFicha
                    and tipo.cd_sistema = 'OFICINA_VALIDACAO'
                    and ( situacao.cd_sistema is null or situacao.cd_sistema <> 'CONVITE_RECUSADO')
                    order by pf.no_pessoa
                    """
        ordem=1
        sqlService.execSql(cmdSql, sqlParams ).eachWithIndex{ line, index ->
            if( line.ds_tipo_credito != '' ) {
                line.nu_ordem           = ordem
                line.sq_pessoa          = line.sq_pessoa
                line.ds_tipo_credito    = line.ds_tipo_credito ?: ''
                line.no_primeiro        = _getFirstName(line.no_pessoa)
                line.no_ultimo          = _getLastName(line.no_pessoa)
                line.de_email           =  line.de_email? line.de_email.toString().toLowerCase() : ''
                line.no_iniciais        = _getInitials(line.no_pessoa)
                line.ds_iucn            = '-' // evitar criticar com a tradução da tabela de apoio
                participantes['row_validador_' + ordem ] = line
                ordem++
            }
        }

        row.json_creditos = (participantes as JSON).toString()
        _createOrAppendJson('credits.csv', 'json_creditos', columns, row, rndId, problemasEncontrados)
    }

    /**
     * método para adicionar uma linha no arquivo fao.csv
     * @param row
     */
    protected void _appendFao(groovy.sql.GroovyRowResult row, String rndId, List problemasEncontrados) {
        List columns = [
           ['name':'FAOOccurrence.FAOOccurrenceSubfield.FAOOccurrenceLookup'        ,field:['cd_fao'],default:'41']
          ,['name':'FAOOccurrence.FAOOccurrenceSubfield.FAOOccurrenceName'          ,field:['ds_fao'],default:'Atlantic - southwest']
          ,['name':'FAOOccurrence.FAOOccurrenceSubfield.formerlyBred',field:[]]
          ,['name':'FAOOccurrence.FAOOccurrenceSubfield.origin',field:[]]
          ,['name':'FAOOccurrence.FAOOccurrenceSubfield.presence'                   ,field:[], default:'Extant']
          ,['name':'FAOOccurrence.FAOOccurrenceSubfield.seasonality',field:[]]
          ,['name':'internal_taxon_id'                                      ,field:['sq_taxon']]
        ]
        Map faos = ['row_1': ['cd_fao':'41', 'ds_fao':'Atlantic - southwest', 'ds_iucn':'Atlantic - southwest'] ]
        row.json_faos = (faos as JSON).toString()
        _createOrAppendJson('fao.csv', 'json_faos', columns, row, rndId, problemasEncontrados)
    }

    /**
     * método para adicionar uma linha no arquivo plantspecific.csv
     * @param row
     */
    protected void _appendPlantspecific(groovy.sql.GroovyRowResult row, String rndId, List problemasEncontrados) {
        List columns = [
             ['name':'PlantGrowthForms.PlantGrowthFormsSubfield.PlantGrowthFormsLookup' ,field:[], required: true]
            ,['name':'PlantGrowthForms.PlantGrowthFormsSubfield.PlantGrowthFormsName'   ,field:[] ]
            ,['name':'internal_taxon_id'                                                ,field:['sq_taxon'] ]
        ]
        _createOrAppendJson('plantspecific.csv', 'json_plantas', columns, row, rndId, problemasEncontrados)
    }

    /**
     * método para adicionar uma linha no arquivo researchneeded.csv
     * @param row
     */
    protected void _appendResearchneeded(groovy.sql.GroovyRowResult row, String rndId, List problemasEncontrados) {
        List columns = [
            [name: 'Research.ResearchSubfield.ResearchLookup', field: ['cd_iucn'],required: true]
            , [name: 'Research.ResearchSubfield.ResearchName', field: ['ds_iucn']]
            , [name: 'Research.ResearchSubfield.note', field: ['ds_notas']]
            , [name: 'internal_taxon_id', field: ['sq_taxon']]
        ]
        _createOrAppendJson('researchneeded.csv', 'json_pesquisas', columns, row, rndId, problemasEncontrados)
        /*if( !row ) {
            return
        }
        String fileName = '/data/salve-estadual/temp/'+rndId+'_researchneeded.csv'
        File file = new File( fileName )
        if( ! file.exists() ){
            //println ' '
            //println 'Arquivo ' + fileName + ' criado'
            file.write( columns.name.join(',')+'\n','UTF-8' )
        }
        List listValues = []
        columns.each { col ->
            try {
                //println 'coluna..: ' + col.name
                //println 'campo(s): ' + col.field.join(', ')
                String value = ''
                if (col.field) {
                    List fieldValues = []
                    col.field.each { field ->
                        fieldValues.push(row[field] ?: '')
                    }
                    value = fieldValues.join('\n')
                }
                listValues.push(_tratarValue(value))
            } catch (Exception e ){
                println ' '
                println 'ERRO - IucnService._appendResearchneeded'
                println e.getMessage()
            }
        }
        file.append( listValues.join(',') +'\n','UTF-8' )
        */
    }

    /**
     * método para adicionar uma linha no arquivo taxonomy.csv
     * @param row
     */
    protected void _appendTaxonomy(groovy.sql.GroovyRowResult row, String rndId, List problemasEncontrados) {
        List columns = [
            [name: 'Redlist_id', field: []]
            , [name: 'TaxonomicNotes.value', field: ['ds_notas_taxonomicas']]
            , [name: 'classname', field: ['no_classe'],required: true]
            , [name: 'family', field: ['no_familia'],required: true]
            , [name: 'genus', field: ['no_genero'],required: true]
            , [name: 'infraType', field: ['no_infra_type']]
            , [name: 'infra_authority', field: [],required: true]
            , [name: 'infra_name', field: ['no_subespecie']]
            , [name: 'internal_taxon_id', field: ['sq_taxon']]
            , [name: 'kingdom', field: ['no_reino'],required: true]
            , [name: 'ordername', field: ['no_ordem'],required: true]
            , [name: 'phylum', field: ['no_filo'],required: true]
            , [name: 'species', field: ['no_especie'],required: true]
            , [name: 'taxonomicAuthority', field: ['no_autor'],required: true]
        ]
        _createOrAppend('taxonomy.csv', columns, row, rndId, problemasEncontrados)
        /*if( !row ){
            return;
        }
        String fileName = '/data/salve-estadual/temp/'+rndId+'_taxonomy.csv'
        File file = new File( fileName )
        if( ! file.exists() ){
            //println ' '
            //println 'Arquivo ' + fileName + ' criado'
            file.write( columns.name.join(',')+'\n','UTF-8' )
        }
        List listValues = []
        columns.each { col ->
            try {
                //println 'coluna..: ' + col.name
                //println 'campo(s): ' + col.field.join(', ')
                String value = ''
                if (col.field) {
                    List fieldValues = []
                    col.field.each { field ->
                        fieldValues.push(row[field] ?: '')
                    }
                    value = fieldValues.join('\n')
                }
                listValues.push(_tratarValue(value))
            } catch (Exception e ){
                println ' '
                println 'ERRO - IucnService._appendTaxonomy'
                println e.getMessage()
            }
        }
        file.append( listValues.join(',') +'\n','UTF-8' )
         */
    }

    /**
     * método para adicionar uma linha no arquivo lme.csv
     * @param row
     */
    protected void _appendLme(groovy.sql.GroovyRowResult row, String rndId, List problemasEncontrados) {
        List columns = [
            ['name': 'LargeMarineEcosystems.LargeMarineEcosystemsSubfield.LargeMarineEcosystemsLookup', field: []]
            , ['name': 'LargeMarineEcosystems.LargeMarineEcosystemsSubfield.LargeMarineEcosystemsName', field: []]
            , ['name': 'LargeMarineEcosystems.LargeMarineEcosystemsSubfield.formerlyBred', field: []]
            , ['name': 'LargeMarineEcosystems.LargeMarineEcosystemsSubfield.origin', field: []]
            , ['name': 'LargeMarineEcosystems.LargeMarineEcosystemsSubfield.presence', field: []]
            , ['name': 'LargeMarineEcosystems.LargeMarineEcosystemsSubfield.seasonality', field: []]
            , ['name': 'internal_taxon_id', field: ['sq_taxon']]
        ]
        _createOrAppendJson('lme.csv', 'json_lme', columns, row, rndId, problemasEncontrados)

    }

    /**
     * método para adicionar uma linha no arquivo references.csv
     * @param row
     */
    protected void _appendReferences(groovy.sql.GroovyRowResult row, String rndId, List problemasEncontrados) {
        List columns = [
              [name: 'Reference_type'               , field: ['reference_type'],required: true]
            , [name: 'access_date'                  , field: ['dt_acesso_url']]
            , [name: 'alternate_title'              , field: []]
            , [name: 'author'                       , field: ['no_autor']]
            , [name: 'date'                         , field: ['dt_publicacao']]
            , [name: 'edition'                      , field: ['nu_edicao_livro']]
            , [name: 'externalBibCode'  , field: []]
            , [name: 'internal_taxon_id'            , field: ['sq_taxon']]
            , [name: 'isbnissn'                     , field: ['de_isbn_issn']]
            , [name: 'keywords' , field: []]
            , [name: 'number'   , field: []]
            , [name: 'number_of_volumes', field: []]
            , [name: 'pages'                        , field: ['de_paginas']]
            , [name: 'place_published'              , field: ['no_cidade']]
            , [name: 'publisher'                    , field: ['no_editora']]
            , [name: 'secondary_author'             , field: ['de_editores']]
            , [name: 'secondary_title'              , field: ['de_titulo_livro']]
            , [name: 'section' , field: []]
            , [name: 'short_title', field: [],required: true]
            , [name: 'submission_type', field: []]
            , [name: 'title'                        , field: ['de_titulo']]
            , [name: 'type'                         , field: ['ds_reference_type']]
            , [name: 'url'                          , field: ['de_url']]
            , [name: 'volume'                       , field: ['de_volume']]
            , [name: 'year'                         , field: ['nu_ano_publicacao'],required: true]
        ]

        Map tiposSalve = ['ARTIGO_CIENTIFICO'           :'other',
                          'LIVRO'                        :'book',
                          'CAPITULO_LIVRO'               :'book section',
                          'DISSERTACAO_MESTRADO'         :'other',
                          'SITE'                         :'other',
                          'TESE_DOUTORADO'               :'thesis',
                          'MIDIA_DIGITAL'                :'other',
                          'MUSEU'                        :'other',
                          'BANCO_DADOS_PESSOA'           :'rldb',
                          'BANCO_DADOS_INSTITUCIONAL'    :'rldb',
                          'RELATORIO_TECNICO'            :'report',
                          'PLANO_MANEJO_UC'              :'other',
                          'RESUMO_CONGRESSO'             :'other',
                          'ATO_OFICIAL'                  :'other',
                          'MONOGRAFIA'                   :'manuscript']

        String fileName = '/data/salve-estadual/temp/'+rndId+'_references.csv'
        File file = new File( fileName )
        if( ! file.exists() ){
            file.write( columns.name.join(',')+'\n','UTF-8' )
        }
        if( !row ){
            return;
        }
        String cmdSql = """select tipo.co_tipo_publicacao as tipo
            ,publicacao.*
            from salve.fn_ficha_ref_bibs(:sqFicha) as refbib
            inner join taxonomia.publicacao on publicacao.sq_publicacao = refbib.sq_publicacao
            inner join taxonomia.tipo_publicacao as tipo on tipo.sq_tipo_publicacao = publicacao.sq_tipo_publicacao
            """
        sqlService.execSql(cmdSql, [sqFicha:row.sq_ficha.toLong()]).each{ ref ->
            try {

                // pre-processamento
                ref.sq_taxon = row.sq_taxon
                ref.reference_type = 'Assessment'
                ref.ds_reference_type = tiposSalve[ref.tipo]
                ref.no_autor = ref.no_autor ? ref.no_autor.replaceAll('\n', ';') : ''
                ref.nu_ano_publicacao ? ref.nu_ano_publicacao.toString() : ''
                ref.dt_acesso_url = ref.dt_acesso_url ? ref.dt_acesso_url.format('yyyy/MM/dd') : ''
                ref.dt_publicacao = ref.dt_publicacao ? ref.dt_publicacao.format('yyyy/MM/dd') : ''
                ref.de_isbn_issn = (ref.de_isbn ?: (ref.de_issn ?: ''))

                List listValues = []
                columns.each { col ->
                    try {
                        String value = ''
                        if (col.field) {
                            List fieldValues = []
                            col.field.each { field ->
                                fieldValues.push(ref[field] ?: '')
                            }
                            value = fieldValues.join('\n')
                        }
                        listValues.push( _tratarValue( value ) )
                    } catch (Exception e ){
                        Util.printLog('ERRO - IucnService._appendReferences',e.getMessage(),'')
                    }
                }
                file.append( listValues.join(',') +'\n','UTF-8' )
            } catch( e1 ){
                Util.printLog('ERRO - IucnService._appendReferences',e1.getMessage(),'')
            }
        }
    }

    /**
     * método para adicionar uma linha no arquivo synonyms.csv
     * @param row
     */
    protected void _appendSynonyms(groovy.sql.GroovyRowResult row, String rndId, List problemasEncontrados) {
        List columns = [
            [name: 'infraType'              , field: []]
            , [name: 'infrarankAuthor'      , field: []]
            , [name: 'infrarankName'        , field: []]
            , [name: 'internal_taxon_id'                , field: ['sq_taxon']]
            , [name: 'name'                             , field: ['no_genero']]
            , [name: 'speciesAuthor'                    , field: ['no_autor'],required: true]
            , [name: 'speciesName'                      , field: ['no_antigo'],required: true]
        ]
        _createOrAppendJson('synonyms.csv', 'json_nomes_antigos',columns, row, rndId, problemasEncontrados)
    }

    /**
     * método para adicionar uma linha no arquivo commonnames.csv
     * @param row
     */
    protected void _appendCommonnames(groovy.sql.GroovyRowResult row, String rndId, List problemasEncontrados) {
        List columns = [
            [name: 'internal_taxon_id', field: ['sq_taxon']]
            , [name: 'language', field: ['no_language_nome_comum'],required: true]
            , [name: 'name', field: ['no_comum'],required: true]
            , [name: 'primary', field: ['st_primary_nome_comum']]
        ]
        if (!row) {
            return
        }
        String fileName = '/data/salve-estadual/temp/' + rndId + '_commonnames.csv'
        File file = new File(fileName)
        if (!file.exists()) {
            file.write(columns.name.join(',') + '\n', 'UTF-8')
        }
        // adicionar cada nome comum em uma linha
        if (!row.ds_nomes_comuns) {
            return
        }
        row.no_language_nome_comum = 'Portuguese'
        List listNomesComuns = row.ds_nomes_comuns.split('\\|')
        listNomesComuns.eachWithIndex { nomeComum, index ->
            row.no_comum = nomeComum
            row.st_primary_nome_comum = (index == 0 ? 'true' : 'false')
            List listValues = []
            columns.each { col ->
                try {
                    String value = ''
                    if (col.field) {
                        List fieldValues = []
                        col.field.each { field ->
                            fieldValues.push(row[field] ?: '')
                        }
                        value = fieldValues.join('\n')
                    }
                    listValues.push(_tratarValue(value))
                } catch (Exception e) {
                    println ' '
                    println 'ERRO - IucnService._appendCommonnames'
                    println e.getMessage()
                }
            }
            file.append(listValues.join(',') + '\n', 'UTF-8')
        }
    }

    /**
     * método para fazer o tratamento dos valores que serão inseridos na linha do arquivo csv
     * adicionando aspas duplas quando necessário
     * @param value
     * @return
     */
    protected String _tratarValue(String value = '') {
        value = value.trim()
        if (!value || value.isEmpty()) {
            return ''
        }
        if (value.isNumber() && !(value =~ /\./)) {
            return value
        }
        value = '"' + value.replaceAll(/"/, '\\\\"') + '"'
        return value
    }

    /**
     * método para gerar o arquivo zip com todos os arquivos csv gerados
     * @param rndId
     * @return
     */
    protected String _gerarZip(String rndId) {
        //List listFiles = ['assessments.csv','allfields.csv','threats.csv']
        String zipFileName = '/data/salve-estadual/temp/salve_sis_' + rndId + '.zip'
        byte[] buffer = new byte[1024]
        FileOutputStream fos = new FileOutputStream(zipFileName)
        ZipOutputStream zos = new ZipOutputStream(fos)
        arquivosCsv.each { entryName ->
            ZipEntry ze = new ZipEntry(entryName)
            String tempFileName = '/data/salve-estadual/temp/' + rndId + '_' + entryName
            //println '  - adicionando  ' + tempFileName + '  como ' + entryName+ ' em ' + zipFileName
            File file = new File(tempFileName)
            if (file.exists()) {
                try {
                    zos.putNextEntry(ze)
                    //FileInputStream fis = new FileInputStream(tempFileName)
                    FileInputStream fis = new FileInputStream(file)
                    int len
                    while ((len = fis.read(buffer)) > 0) {
                        zos.write(buffer, 0, len)
                    }
                    fis.close()
                } catch (Exception e) {
                    println e.getMessage()
                }
                // nao excluir o arquivo csv durante os testes quando o rndId for vazio
                if (rndId) {
                    file.delete()
                }
                //new File(tempFileName).delete()
            }
        }
        zos.closeEntry()
        zos.close()
        return zipFileName
    }

    /**
     * método para enviar o arquivo zip com todos os arquivos csv gerados para o e-mail do usuário
     */
    protected void _enviarEmail(String email, String zipFileName, List problemasEncontrados) {
        String dataEnvio = new Date().format('dd/MM/yyyy \'às\' HH:mm:ss')
        Map anexos = [:]
        String mensagem = ''
        if( zipFileName ) {
            mensagem = """Segue anexo o arquivo no formato zip contendo os arquivos no formato CSV das fichas selecionadas para envio ao SIS Connect / IUCN em ${dataEnvio}"""
            if (problemasEncontrados) {
                mensagem += '<p style="color:red;"><b>Problemas encontrados:</b></p><p>- ' + problemasEncontrados.join('<br>- ') + '</p>'
            }
            mapAnexos.fullPath = zipFileName
        } else {
            mensagem = "Segue a relação dos problemas encontrados nas fichas selecionadas para envio ao SIS Connect / IUCN em ${dataEnvio}"
            if (problemasEncontrados) {
                mensagem += '<p style="color:red;"><b>Problemas encontrados:</b></p><p>- ' + problemasEncontrados.join('<br>- ') + '</p>'
            }
        }
        emailService.sendTo(email, 'Transmissão SALVE para SIS Connect/IUCN', mensagem, 'salve@icmbio.gov.br', anexos)
    }

    /**
     * método para criação dos valores que precisam de algum tratamento antes de ser gravados
     * @param row
     * @return
     */
    protected void _calcValues(groovy.sql.GroovyRowResult row) {

        try {
            // coluna calculada descrição do Método de cálculo
            // (ExtinctionProbabilityGenerations3.justification, ExtinctionProbabilityGenerations5.justification e ExtinctionProbabilityYears100.justification )
            row.ds_metodo_calc_perc_extin_br100 = ''
            row.ds_metodo_calc_perc_extin_br3 = ''
            row.ds_metodo_calc_perc_extin_br5 = ''
            if (row.ds_prob_extincao_brasil100) {
                row.ds_metodo_calc_perc_extin_br100 = row.ds_metodo_calc_perc_extin_br
            } else if (row.ds_prob_extincao_brasil3) {
                row.ds_metodo_calc_perc_extin_br3 = row.ds_metodo_calc_perc_extin_br
            } else if (row.ds_prob_extincao_brasil5) {
                row.ds_metodo_calc_perc_extin_br5 = row.ds_metodo_calc_perc_extin_br
            }
            // adicionar a unidade no intervalo de nascimento
            if (row.ds_intervalo_nascimento && row.vl_intervalo_nascimento) {
                row.vl_intervalo_nascimento += ' ' + row.ds_intervalo_nascimento
            }

            // varificar se tem ou não uso
            row.st_uso = ''
            row.ds_uso_nao_encontrado = ''

            /**
             * isNotUtilized
             a) Se tiver um uso adicionado fica false
             b) Se não tiver uso adicionado e tiver marcado o CHECK fica false
             c) Se não tiver uso e não tiver marcado o CHECK ficha true
             */

            if( row.json_usos ){
                row.st_uso = 'false'
            } else {
                if (row.ds_uso == 'Não foram encontradas informações para o táxon.' ) {
                    row.st_uso = 'false'
                    row.ds_uso_nao_encontrado = row.ds_uso
                    row.ds_uso = ''
                } else {
                    row.st_uso = 'true'
                }
            }

            // ameaca desconhecida
            row.st_ameaca_desconhecida = '0'
            if (row.ds_ameaca == 'Não foram encontradas informações para o táxon.') {
                row.st_ameaca_desconhecida = '1'
            }

            // presenca em UC
            row.st_presenca_uc = 'No'
            if (row.ds_presenca_uc == 'Não foram encontradas informações para o táxon.') {
                row.st_presenca_uc = 'Unknown'
            } else if (row.ds_presenca_uc) {
                row.st_presenca_uc = 'Yes'
            }

            // subespecie
            row.no_infra_type = ''
            if (row.no_subespecie) {
                row.no_infra_type = 'subsp.'
            }

            // população
            row.st_population_reduction_past_reversible = ''
            row.st_population_reduction_past_understood = ''
            row.st_population_reduction_past_ceased     = ''
            if( row.nu_reducao_popul_passada_rev ){
                row.st_population_reduction_past_reversible = '1'
                row.st_population_reduction_past_understood = '1'
                row.st_population_reduction_past_ceased     = '1'
            }

            // considerar o maior valor entre o macho e femea
            row.vl_longevidade = ''
            row.ds_longevidade = ''
            row.sq_unid_longevidade = 0
            if( row.vl_longevidade_macho || row.vl_longevidade_femea  ) {
                row.vl_longevidade = Math.max(row.vl_longevidade_femea ?: 0.0, row.vl_longevidade_macho ?: 0.0).toString()
                row.ds_longevidade       =  (row.vl_longevidade_macho > row.vl_longevidade_femea ? row.ds_longevidade_mecho : row.ds_longevidade_femea)
                row.sq_unid_longevidade  =  (row.vl_longevidade_macho > row.vl_longevidade_femea ? row.sq_unid_longevidade_macho : row.sq_unid_longevidade_femea)
                row.cd_sistema_unid_longevidade =   (row.vl_longevidade_macho > row.vl_longevidade_femea ? row.cd_sistema_unid_longevidade_macho : row.cd_sistema_unid_longevidade_femea)
                if( row.cd_sistema_unid_longevidade =='HORAS'){
                    row.vl_longevidade = 1
                    row.ds_longevidade = 'Days'
                    row.sq_unid_longevidade = 0 // 0 = evitar a traducao
                } else if( row.cd_sistema_unid_longevidade =='DECADA'){
                    row.vl_longevidade = ( row.vl_longevidade.toString().toFloat() * 10).toString()
                    row.ds_longevidade = 'Years'
                    row.sq_unid_longevidade = 0 // 0 = evitar a traducao
                }
            }
            // comprimento macho e femea
            row.vl_comprimento_max = ''
            if( row.vl_comprimento_macho_max || row.vl_comprimento_femea_max  ) {
                row.vl_comprimento_max  = Math.max(row.vl_comprimento_macho_max ?: 0.0, row.vl_comprimento_femea_max ?: 0.0).toString()
            }

            // ação de conservação
            // InPlaceResearchRecoveryPlan.value = s                                    t_plano_ac'st_plano_acao'ao
            row.st_plano_acao = 'Unknown'
            if( row.json_acoes_conservacao ) {
               try {
                   JSONObject json = JSON.parse( row.json_acoes_conservacao )
                   // verificar se tem pan em alguma das ações
                   row.st_plano_acao = 'No'
                   json.each{ key,value ->
                       if( value.cd_sistema =~ /^(PAN|PRIM|PREC)$/ ){
                           row.st_plano_acao = 'Yes'
                       }
                   }
               } catch( Exception e2 ){
                   Util.printLog('SALVE IucnService.calcValues()',e2.getMessage(),'JSON.PARSE da coluna json_acoes_conservacao da ficha ' + row.nm_cientifico)
               }
            }

            // habitats
            row.ds_habitats=''
            if( row.json_habitats) {
               try {
                    JSONObject json = JSON.parse( row.json_habitats )
                    // verificar os ambientes dos habits para coluna ds_habitats (System.value)
                    List habitats = []
                    json.each{ key,value ->
                        if( value.ds_trilha_sistema =~ /AMBIENTES_TERRESTRES/ && !habitats.contains('Terrestrial') ){
                            habitats.push('Terrestrial')
                        }
                        if( value.ds_trilha_sistema =~ /AMBIENTES_MARINHO/ && !habitats.contains('Marine') ){
                            habitats.push('Marine')
                        }
                        if( value.ds_trilha_sistema =~ /AGUA_DOCE/ && !habitats.contains('Freshwater (=Inland waters)') ){
                            habitats.push('Freshwater (=Inland waters)')
                        }
                    }
                    row.ds_habitats = habitats.join('|')
                } catch( Exception e2 ){
                    Util.printLog('SALVE IucnService.calcValues()',e2.getMessage(),'JSON.PARSE da coluna json_habitas da ficha ' + row.nm_cientifico)
                }
            }

            // motivo mudanca categoria
            row.st_motivo_mudanca_genuino = 'No change'
            if( row.cd_motivo_mudanca_sistema ){
                row.st_motivo_mudanca_genuino = 'Nongenuine Change'
                if( row.cd_motivo_mudanca_sistema =~ /MUDANCA_GENUINA_ESTADO_CONSERVACAO/ ){
                    row.st_motivo_mudanca_genuino = 'Genuine Change'
                }
            }

            // paises
            row.st_presence_country = ''
            if( row.st_endemica_brasil=='S'){
                if( row.cd_categoria =='CR' && row.st_possivelmente_extinta.toLowerCase() == 'true') {
                    row.st_presence_country = 'Possibly Extinct'
                } else if( row.cd_categoria =='EX') {
                    row.st_presence_country = 'Extinct'
                } else {
                    row.st_presence_country = 'Extant'
                }
            }


        } catch (Exception e) {
            Util.printLog('SALVE IucnService.calcValues()',e.getMessage(),'Ficha ' + row.nm_cientifico )
        }
    }

    /**
     * método para ler a tradução da coluna de acordo com seu nome
     * @param row
     * @param noColuna
     * @return
     */
    protected String _traducao(groovy.sql.GroovyRowResult row, String noColuna) {
        String textoTraduzido = ''
        String textoOriginal = row[noColuna] ?: ''
        Long sqFicha = row.sq_ficha.toLong()
        String noTabela = 'ficha'
//println '_traduzir() ' + noColuna +' = ' + row[noColuna]

        if (textoOriginal == null || textoOriginal.isEmpty()) {
            return textoTraduzido
        }
        // colunas calculadas que devem utilizar a tradução da coluna original
        if (noColuna == 'ds_uso_nao_encontrado') {
            noColuna = 'ds_uso'
        } else if (noColuna =~ /^ds_perc_declinio_populacional/) {
            noTabela = 'apoio'
            noColuna = 'sq_perc_declinio_populacional'
        } else if (noColuna == 'ds_unid_reducao_popul_futura' ) {
            noTabela = 'apoio'
            noColuna = 'sq_tipo_redu_popu_futura'
        } else if (noColuna == 'ds_longevidade' ) {
            noTabela = 'apoio'
            noColuna = 'sq_unid_longevidade'
        } else if (noColuna == 'ds_tendencia_populacional' ) {
            noTabela = 'apoio'
            noColuna = 'sq_tendencia_populacional'
        }

        if (noTabela == 'ficha') {
            // procurar id da coluna
            Map regTraducaoTabela = camposFichaTraducao.find { it.no_coluna == noColuna }
            if (regTraducaoTabela) {
                List reg  = sqlService.execSql("""select tx_traduzido, dt_revisao
                    from salve.traducao_revisao
                    where sq_traducao_tabela = :sqTraducaoTabela
                      and sq_registro = :sqFicha""", [sqTraducaoTabela: regTraducaoTabela.sq_traducao_tabela, sqFicha: sqFicha])
                if( !reg || !reg[0].tx_traduzido ){
                    throw new Exception(sqFicha + '. Campo ' + noColuna + ' não traduzido.')
                } else if ( ! reg[0].dt_revisao ){
                    throw new Exception(sqFicha + '. Campo ' + noColuna + ' tradução não revisada.')
                }
                textoTraduzido = reg[0].tx_traduzido
            }
        } else {
            /** /
             println ' '
             println 'PROCURAR TABELAS AUXILIARES'
             println 'Coluna:' + noColuna
             println 'Tabela :' + noColuna
             println 'Texto original: ' + textoOriginal
             println '-' * 80
             /**/

            if (noTabela && noColuna) {
                if (noTabela == 'apoio') {
                    Long sqDadosApoio = row[noColuna].toLong()
                    if( sqDadosApoio == 0 ) {
                        return textoOriginal
                    }
                    //println 'id:' + sqDadosApoio
                    sqlService.execSql("""select tx_traduzido
                         from salve.traducao_dados_apoio where sq_dados_apoio = :sqDadosApoio""", [sqDadosApoio: sqDadosApoio]).each { reg ->
                        textoTraduzido = reg.tx_traduzido
                        //println 'Tradução:' + textoTraduzido
                        if( textoTraduzido =~ /Day\(s\)|Month\(s\)|Year\(s\)| / ){
                            // remover o (s) do final da unidade
                            textoTraduzido = textoTraduzido.toString().replaceAll(/\(s\)/,'s')
                        }
                    }
                    if (!textoTraduzido) {
                        List reg = sqlService.execSql('select ds_dados_apoio from salve.dados_apoio where sq_dados_apoio = :sqDadosApoio',[sqDadosApoio: sqDadosApoio])
                        String descricao = noColuna
                        if( reg ) {
                                noColuna = ' - <b>' + reg[0].ds_dados_apoio + '</b>, campo: ' + noColuna
                        }
                        throw new Exception(sqFicha + '. Tabela apoio registro id:' + sqDadosApoio + ' ' + noColuna + ' não está traduzido.')
                    }
                } else if (noTabela == 'pan') {
                    // TODO - tradução do PAN
                }
            }
        }

        // se não encontrar a tradução retornar o texto original
        textoTraduzido = textoTraduzido ?: textoOriginal

//println 'TRADUCAO:' + textoTraduzido

        return textoTraduzido
    }

    /**
     * Método para criar ou adicionar uma linha no formato CSV no arquivo informado
     * @param fileName
     * @param columns
     * @param row
     * @param rndId
     */
    protected void _createOrAppend(String fileName, List columns, groovy.sql.GroovyRowResult row, String rndId, List problemasEncontrados) {
        String tempFileName = '/data/salve-estadual/temp/' + rndId + '_' + fileName
        File file = new File(tempFileName)
        if (!file.exists()) {
            //println ' '
            //println 'Arquivo ' + fileName + ' criado'
            file.write(columns.name.join(',') + '\n', 'UTF-8')
        }
        if (!row) {
            return
        }
        List listValues = []
        columns.each { col ->
            try {
                String value = ''
                if (col.field) {
                    List fieldValues = []
                    col.field.each { field ->
                        // ler a tradução do campo da ficha
                        String traducao = ''
                        try {
                            //traducao = _traducao(row.sq_ficha.toLong(), field.toString(), ( row[field] ? row[field].toString() : ''))
                            traducao = _traducao(row, field.toString())
                        } catch (Exception eTraducao) {
                            String msgProblema = row.nm_cientifico + ': ' + eTraducao.getMessage() + ' (' + fileName + ')' // arquivo:
                            // não repetir a mensagem de erro
                            if ( ! problemasEncontrados.contains(msgProblema ) ) {
                                problemasEncontrados.push(msgProblema)
                            }
                        }
                        fieldValues.push(traducao)
                    }
                    value = fieldValues.join('\n')
                }
                if (!value && col.default) {
                    value = col.default
                }
                if( ! value && col?.required && col?.field ) {
                    String campo = col.field.join(', ')
                    if( col.field.size() == 1 ) {
                        problemasEncontrados.push("""${row.nm_cientifico}: ${row.sq_ficha}. O campo ${campo} é obrigatório.""")
                    } else {
                        problemasEncontrados.push("""${row.nm_cientifico}: ${row.sq_ficha}. Os campos ${campo} são obrigatórios.""")
                    }
                } else {
                    listValues.push(_tratarValue(value))
                }
            } catch (Exception e) {
                println ' '
                println 'ERRO - IucnService._append' + fileName.capitalize()
                println e.getMessage()
            }
        }
        file.append(listValues.join(',') + '\n', 'UTF-8')
    }

    /**
     * Método para criar ou adicionar uma ou mais linhas no formato CSV no arquivo informado
     * utilizado para as tabelas um para muitos. Ex: habitats, ação de conservação etc
     * @param fileName
     * @param colJson
     * @param columns
     * @param row
     * @param rndId
     */
    protected void _createOrAppendJson(String fileName, String colJson, List columns, groovy.sql.GroovyRowResult row, String rndId, List problemasEncontrados) {
        String tempFileName = '/data/salve-estadual/temp/' + rndId + '_' + fileName
        File file = new File(tempFileName)
        if (!file.exists()) {
            file.write(columns.name.join(',') + '\n', 'UTF-8')
        }
        if (!row) {
            return
        }

        if (!row[colJson]) {
            return
        }
        // parse json vindo do banco de dados
        JSONObject data
        try {
            data = JSON.parse(row[colJson])
        } catch (Exception erro ) {
          Util.printLog('SALVE - IucnService._createOrAppendJson()','Erro ao adicionar linha no arquivo '+fileName + '\n\nLinha: ' + row, erro.getMessage())
          problemasEncontrados.push('Erro ao adicionar linha em ' + fileName + '\n\nLinha: ' + row + '\n\n' + erro.getMessage())
          return
        }


        /** /
         println ' '
         println 'Arquivo:' + fileName
         println 'campo: ' + colJson
         println 'Data: ' + data
         /**/

        try {
            data.each { id, rowJson ->
                rowJson.sq_taxon = row.sq_taxon
                // criticar se o registro da tabela de apoio não tiver na tabela iucn_dados_apoio
                if (!rowJson.ds_iucn) {
                    // os usos possuem uma tabela separada da dados_apoio
                    String colJsonPrint = colJson.replaceAll(/json_/,'')
                    if (colJson == 'json_usos') {
                        problemasEncontrados.push('Registro id ' + id + ' - ' + rowJson.ds_uso + ' da tabela usos não relacionado com a tabela IUCN_USOS (' + colJsonPrint + ').')
                    } else {
                        problemasEncontrados.push('Registro id ' + id + ' - ' + rowJson.ds_dados_apoio + ' da tabela de apoio não relacionado com a tabela IUCN correspondente (' + colJsonPrint + ').')
                    }
                } else {
                    // fazer a validação da linha
                    boolean validRow = _validarRowJson( colJson, rowJson, problemasEncontrados)

                    if ( validRow ) {
                        List listValues = []
                        columns.each { col ->
                            try {
                                String value = ''
                                if (col.field) {
                                    List fieldValues = []
                                    col.field.each { field ->
                                        if (rowJson[field] != null) {
                                            fieldValues.push(rowJson[field] ?: '')
                                        } else if (row[field] != null) {
                                            fieldValues.push(row[field] ?: '')
                                        }
                                    }
                                    value = fieldValues.join('\n')
                                } else if (col.default) {
                                    value = col.default
                                }
                                listValues.push(_tratarValue(value))
                            } catch (Exception e) {
                                Util.printLog('SALVE - IucnService._createOrAppendJson()', 'Erro ao adicionar linha no arquivo ' + fileName +
                                    '\nColuna: ' + col +
                                    '\nLinha: ' +
                                    rowJson.toString(),
                                    e.getMessage())
                                problemasEncontrados.push('Erro ao adicionar linha em ' + fileName + 'Coluna: ' + col + '\n\n' + e.getMessage())
                            }
                        }
                        file.append(listValues.join(',') + '\n', 'UTF-8')
                    }
                }
            }
        } catch ( Exception erro ){
            Util.printLog('SALVE - IucnService._createOrAppendJson()','Erro ao adicionar linha no arquivo '+fileName,erro.getMessage())
        }
    }
}
