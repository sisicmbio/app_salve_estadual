package br.gov.icmbio

import grails.transaction.Transactional

@Transactional
class LoginService {

    def grailsApplication
    def emailService

    Sicae login(String email='', String senha='', Long sqPerfil = 0) {
        Sicae user = null
        Thread.sleep(1000) // exibir animação

        /** /
        println ' '
        println ' '
        println ' '
        println 'DEBUG LOGIN'
        println 'email:'+email
        println 'Senha:'+senha
        println 'Perfil:'+sqPerfil
        //throw  new Exception('Teste parametros')
        /**/


        if( email && senha) {
            email = email.toLowerCase()
            // entrar como ADMIN
            /*if( email =~ /^admin@salve/) {
                return _loginAdm( email, senha )
            }*/
            Usuario usuario = Usuario.createCriteria().get{
                pessoaFisica {
                    or {
                        pessoa {
                            eq('deEmail', email)
                        }
                    eq('nuCpf',email)
                    }
                }
            }

            if( !usuario ){
                throw  new Exception('Usuário não cadastrado')
            }

            if( !usuario.stAtivo ){
                throw  new Exception('Usuário está DESATIVADO no sistema')
            }

            if( usuario.deSenha != Util.md5( senha ) ) {
                throw  new Exception('Email, CPF ou senha incorretos')
            }
            // verificar se o usuario ja possui algum perfil atribuido
            List perfis = perfis( usuario )
            if( !perfis ){
                throw  new Exception('Usuário ainda não possui perfil de acesso')
            }
            user = new Sicae()
            user.sqPessoa       = usuario.id.toLong()
            user.nuCpf          = usuario.pessoaFisica.cpfFormatado
            user.sgSistema      = 'SALVE'
            user.noSistema      = 'Sistema de Avaliação do Risco de Extinção da Biodiversidade'
            user.noUsuario      = usuario.pessoaFisica.primeiroNome
            // se possuir apenas 1 perfil não precisa mostrar a tela de seleção de perfil
            if( perfis.size() == 1 ){
                user.sqPerfil = perfis[0].perfil.id
                user.cdPerfil = perfis[0].perfil.cdSistema
                user.noPerfil = perfis[0].perfil.noPerfil
            } else {
                user.sqPerfil = 0
                user.cdPerfil = ''
                user.noPerfil = ''
            }
            user.sgUnidadeOrg   = usuario.pessoaFisica?.instituicao?.sgInstituicao
            user.sqUnidadeOrg   = usuario.pessoaFisica?.instituicao?.id
            user.noUnidadeOrg   = usuario.pessoaFisica?.instituicao?.pessoa?.noPessoa
            user.dateTime       = new Date().format('dd/MM/yyyy HH:mm:ss')

            // atualizar ultimo acesso
            usuario.dtUltimoAcesso = new Date()
            usuario.save()


        } else if( sqPerfil ) {
            // validar perfil


        } else {
            throw  new Exception('Informações insuficientes ou incorretas')
        }
        return user
    }

    /**
     * método para gerar o hash e a mensgem de redefinição da senha
     * @param email
     * @return
     */
    String generateMessageResetPassword( String email = '') {

        if( !email ){
            throw new Exception('Email não informado')
        }
        // email deve estar em caixa baixa
        email = email.toLowerCase()

        // validar e mail
        Pessoa pessoa = Pessoa.findByDeEmail(email)
        if( !pessoa ){
            throw new Exception('Email não cadastrado')
        }
        Usuario usuario = Usuario.get( pessoa.id )
        if( !usuario ){
            throw new Exception('Email não pertence a um usuário')
        }
        if( !usuario.stAtivo ){
            if( usuario.dtUltimoAcesso ) {
                throw new Exception('Usuário está DESATIVADO')
            }
        }
        // gerar o hash
        usuario.deHash = Util.md5('hash' + usuario.id + new Date().toString() )
        usuario.save()
        String urlLink = grailsApplication.config.url.sistema + "login/changePasswordForm/"+usuario.deHash
        String emailFrom = grailsApplication.config.app.email
        String emailMessage = """<div>Olá ${pessoa.primeiroNome},
        Recebemos sua solicitação para recuperação/alteração de senha para a acesso ao Sistema SALVE associado ao e-mail <b>${pessoa.deEmail}</b>.
        Nenhuma alteração foi feita em sua conta ainda.
        <span class="red">Se você desconhece esta solicitação, favor ignorar esta mensagem.</span>
        Clique no link abaixo para redefinir sua senha:
        <a class="btn btn-block btn-link btn-sm fs20 bold" href="${urlLink}">REDEFINIR MINHA SENHA</a></div>"""
        String returnMessage
        if( emailService.sendTo(email,'Sistema SALVE - Recuperação de senha',emailMessage, emailFrom) ){
            returnMessage = "Email enviado com SUCESSO.<br>Acesso sua caixa de e-email <b>${email}</b> e siga a instruções para alteração de senha."
        } else {
            returnMessage = 'Ocorreu um erro ao enviar o email para a redefinição da senha. Tente mais tarde.'
        }
        return returnMessage
    }

    /**
     * método para alterar a senha do usuário
     * @param senha1
     * @param senha2
     * @param hash
     * @param sessionHash
     */
    void changePassword( String senha1='', String senha2='', String hash='', String sessionHash='' ) {
        if (!senha1 || !senha2 || !hash) {
            throw new Exception('Parâmetros insuficientes para alteração da senha')
        }
        if( senha1 != senha2 ){
            throw new Exception('Senhas informadas são diferentes')
        }
        if( hash != sessionHash ){
            throw new Exception('Hash inválido')
        }
        // localizar o usuario pelo hash
        Usuario usuario = Usuario.findByDeHash( hash )
        if( !usuario ) {
            throw new Exception('Hash inválido')
        }
        usuario.deHash = null
        senha1 = senha1.trim()
        usuario.deSenha= Util.md5(senha1)
        usuario.save()
    }

    /**
     * Login automático para testes como administrador
     * @param email
     * @param senha
     * @return
     */
    Sicae simularPerfilAdm(String email, String senha){
        Sicae user = new Sicae()
        Pessoa pessoa = Pessoa.findByDeEmail(email)
        if( ! pessoa ) {
            throw new Exception('Usuário ADM padrão não cadastrado')
        }

        Perfil perfilAdm = Perfil.findByCdSistema('ADM')
        if( !perfilAdm ){
            throw new Exception('Perfil ADM padrão não cadastrado')
        }

        Usuario usuario = Usuario.get( pessoa.id )
        if( !usuario ){
            throw new Exception('Usuário ADM padrão não cadastrado')
        }

        if( usuario.deSenha != Util.md5(senha) ){
            throw new Exception('Senha inválida')
        }
        user.sgSistema      = 'SALVE-Estadual'
        user.noSistema      = 'Sistema de Avaliação do Risco de Extinção da Biodiversidade'
        user.noUsuario      = 'Usuário'
        user.sqPerfil       = perfilAdm.id.toLong()
        user.cdPerfil       = perfilAdm.cdSistema
        user.noPerfil       = perfilAdm.noPerfil
        user.sgUnidadeOrg   = ''
        user.sqUnidadeOrg   = null
        user.noUnidadeOrg   = ''
        user.sqPessoa       = pessoa.id.toLong()
        user.nuCpf          = PessoaFisica.get(pessoa.id).cpfFormatado
        user.dateTime       = new Date().format('dd/MM/yyyy HH:mm:ss')
        return user
    }


    /**
     * retorna a lista de perfis do usuario
     * @param sqPessoa
     * @return
     */
    List perfis( Usuario usuario ) {
        List res = []
        if( !usuario ){
            return res
        }
        return UsuarioPerfil.findAllByUsuario(usuario)
    }

    /**
     * retorna a lista de instituições que o usuario possui com um determinado perfil
     * @param sqUsuarioPerfil
     * @return
     */
    Map selectPerfil(Long sqUsuarioPerfil = 0l){

        Map res = [instituicoes:[]]
        UsuarioPerfil usuarioPerfil = UsuarioPerfil.get( sqUsuarioPerfil )
        UsuarioPerfilInstituicao.findAllByUsuarioPerfil( usuarioPerfil).each {
            res.instituicoes.push([sqUsuarioPerfilInstituicao:it.id,
                    'noInstituicao':it.instituicao.pessoa.noPessoa,
                    'sgInstituicao':it.instituicao.sgInstituicao
            ])
        }
        return res
    }

    /**
     * realizar o login do usuario na instituição selecionada
     * @param sqUsuarioPerfil
     * @param sqUsuarioPerfilInstituicao
     */
    Sicae loginPerfilInstituicao(Long sqUsuarioPerfil =0 , Long sqUsuarioPerfilInstituicao = 0 ){
        Sicae user = null
        Pessoa pessoa
        Perfil perfil
        Usuario usuario
        Instituicao instituicao
        if( sqUsuarioPerfilInstituicao ) {
            UsuarioPerfilInstituicao usuarioPerfilInstituicao = UsuarioPerfilInstituicao.get( sqUsuarioPerfilInstituicao )
            instituicao = usuarioPerfilInstituicao.instituicao
            usuario = usuarioPerfilInstituicao.usuarioPerfil.usuario
            perfil = usuarioPerfilInstituicao.usuarioPerfil.perfil
            pessoa = usuario.pessoaFisica.pessoa
        } else if ( sqUsuarioPerfil ){
            UsuarioPerfil usuarioPerfil= UsuarioPerfil.get( sqUsuarioPerfil )
            perfil = usuarioPerfil.perfil
            usuario = usuarioPerfil.usuario
            pessoa = usuario.pessoaFisica.pessoa
        }
        if( !usuario ){
            throw new Exception('Usuário não cadastrado')
        }
        if( !pessoa ) {
            throw new Exception('Pessoa não cadastrada')
        }
        if( !perfil ){
            throw new Exception('Perfil não cadastrado')
        }
        user = new Sicae()
        user.sgSistema      = 'SALVE'
        user.noSistema      = 'Sistema de Avaliação do Risco de Extinção da Biodiversidade'
        user.noUsuario      = pessoa.noPessoa
        user.sqPerfil       = perfil.id.toLong()
        user.cdPerfil       = perfil.cdSistema
        user.noPerfil       = perfil.noPerfil
        if( instituicao ) {
            user.sgUnidadeOrg = instituicao.sgInstituicao
            user.sqUnidadeOrg = instituicao.id
            user.noUnidadeOrg = instituicao.pessoa.noPessoa
        }
        user.sqPessoa       = pessoa.id.toLong()
        user.nuCpf          = usuario.pessoaFisica.cpfFormatado
        user.dateTime       = new Date().format('dd/MM/yyyy HH:mm:ss')
        return user
    }
}
