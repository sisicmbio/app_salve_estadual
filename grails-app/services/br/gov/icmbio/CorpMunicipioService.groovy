package br.gov.icmbio

import com.vividsolutions.jts.geom.Geometry
import com.vividsolutions.jts.io.WKTReader
import grails.converters.JSON
import grails.transaction.Transactional

@Transactional
class CorpMunicipioService {

    def sqlService

    /**
     * método para gravar as informações do Municipio no banco de dados
     * @param sqMunicipio
     * @param noMunicipio
     * @param wkt
     * @return
     */
    Map save(Long sqMunicipio = null, String noMunicipio = '', Integer coIbge = null, Long sqEstado = null,  String wkt = '') {
        if( !noMunicipio ){
            throw new Exception('Nome do Municipio não informado')
        }

        if( !coIbge ){
            throw new Exception('Código IBGE não informado')
        }

        if( !sqEstado ){
            throw new Exception('Estado do município não informado')
        }

        Municipio municipio
        if( sqMunicipio ){
            municipio = Municipio.get( sqMunicipio )
            if( !municipio ){
                throw new Exception('Id do Municipio inexistente')
            }
        } else {
            municipio = new Municipio()
        }

        if( wkt ) {
            WKTReader wktReader = new WKTReader();
            Geometry g = wktReader.read( wkt )
            municipio.geometry = g
            if (municipio.geometry) {
                municipio.geometry.SRID = 4674
            }
        } else {
            municipio.geometry=null
        }
        Estado estado = Estado.get( sqEstado )
        if( !estado ){
            throw new Exception('Estado não cadastrado')
        }
        municipio.estado = estado
        municipio.coIbge = coIbge
        municipio.noMunicipio = Util.capitalize(noMunicipio).replaceAll(/'/,'`')
        municipio.save()
        return municipio.asMap() // retornar os dados do município gravado para atualizar o gride
    }

    /**
     * método para recuperar as informações do Municipio para alteração
     * @param sqMunicipio
     * @return
     */
    def edit(Long sqMunicipio=0l) {
        if( !sqMunicipio ){
            throw new Exception('Id do Municipio não informado')
        }

        Municipio municipio
        municipio = Municipio.get( sqMunicipio )
        if( !municipio ){
            throw new Exception('Id do Municipio inexistente')
        }

        Map result = [ 'sqMunicipio'    : municipio.id
                       , 'noMunicipio'  : municipio.noMunicipio
                       , 'coIbge'       : municipio.coIbge
                       , 'sqEstado'     : municipio?.estado?.id
                       , 'geojson'   : [ "type": "FeatureCollection"
                       , 'features'  : []
                       ]
        ]
        String cmdSql = """SELECT ST_AsGeoJSON( ge_municipio,13,8) AS geoJson 
                           FROM salve.municipio 
                           WHERE sq_municipio = :sqMunicipio"""
        sqlService.execSql( cmdSql, [sqMunicipio:sqMunicipio] ).each { row ->
            if( row.geoJson ) {
                result.geojson.features.push( [ 'type': 'Feature'
                                               ,'properties': [],
                                                'geometry': JSON.parse( row.geoJson )

                ])
            }
        }
        return result
    }


    /**
     * método para excluir a Municipio do banco de dados
     * @return
     */
    def delete(Long sqMunicipio = 0l) {
        Municipio municipio = Municipio.get( sqMunicipio )
        if( !municipio ) {
            throw new Exception('Id do Municipio não informado')
        }
        municipio.delete()
    }

    List list( Integer page=1, Integer pageSize=100, String q = '') {

        Integer skipRecords = ( pageSize * ( page - 1 ) )
        return Municipio.createCriteria().list {
            if( q ){
                ilike('noMunicipio','%'+q.trim()+'%')
            }
            maxResults( pageSize )
            firstResult( skipRecords )
            order("noMunicipio", "asc")
        }
    }

}
