package br.gov.icmbio

//import org.codehaus.groovy.grails.web.json.JSONElement
//import org.apache.catalina.connector.ResponseFacade
//import grails.orm.PagedResultList
//import grails.transaction.Transactional
import grails.converters.JSON
import groovy.sql.GroovyRowResult
import groovy.swing.factory.HStrutFactory
import org.codehaus.groovy.grails.web.servlet.mvc.GrailsHttpSession
import org.codehaus.groovy.grails.web.json.JSONObject
//import org.jsoup.Jsoup
//import org.jsoup.nodes.Document
//import org.jsoup.safety.Whitelist
import org.springframework.web.context.request.RequestContextHolder

import java.nio.charset.Charset
import java.nio.charset.StandardCharsets
import java.util.regex.Pattern
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream

//@Transactional
class ExportDataService {

    private appSession
    private boolean debug = false
    private int maxRegistrosPermitidos = 300000

    def dadosApoioService
    def htmlCleaner
    def grailsApplication
    def workerService
    def sqlService
    def emailService
    def fichaService
    def userJobService

    private void getSession() {
        this.appSession = RequestContextHolder.currentRequestAttributes().getSession()
    }
    /**
     * método para imprimir logs no terminal quando for passado o parametro debug=true
     * @param text
     */
    private void d(String text ){
        if( debug ) {
            if( text.trim() != '' ) {
                String h = new Date().format('HH:mm:ss')
                println h + ':' + text
            } else {
                println ' '
            }
        }
    }

    /**
     * metodo para converter os caracteres especiais html em caracteres da língua portuquesa
     * @param texto
     * @return
     */
    private String entity2Char(String texto = '') {
        Map entities = ['&Aacute;': 'Á', '&Egrave;': 'È', '&ocirc;': 'ô', '&Ccedil;': 'Ç', '&aacute;': 'á', '&egrave;': 'è', '&Ograve;': 'Ò', '&ccedil;': 'ç', '&Acirc;': 'Â', '&Euml;': 'Ë', '&ograve;': 'ò', '&acirc;': 'â',
                        '&euml;'  : 'ë', '&Oslash;': 'Ø', '&Ntilde;': 'Ñ', '&Agrave;': 'À', '&ETH;': 'Ð', '&oslash;': 'ø', '&ntilde;': 'ñ', '&agrave;': 'à', '&eth;': 'ð', '&Otilde;': 'Õ',
                        '&Aring;' : 'Å', '&otilde;': 'õ', '&Yacute;': 'Ý', '&aring;': 'å', '&Iacute;': 'Í', '&Ouml;': 'Ö', '&yacute;': 'ý', '&Atilde;': 'Ã', '&iacute;': 'í', '&ouml;': 'ö', '&atilde;': 'ã', '&Icirc;': 'Î',
                        '&quot;'  : '"', '&Auml;': 'Ä', '&icirc;': 'î', '&Uacute;': 'Ú', '&lt;': '<', '&auml;': 'ä', '&Igrave;': 'Ì', '&uacute;': 'ú', '&gt;': '>', '&AElig;': 'Æ', '&igrave;': 'ì', '&Ucirc;': 'Û',
                        '&amp;'   : '&', '&aelig;': 'æ', '&Iuml;': 'Ï', '&ucirc;': 'û', '&iuml;': 'ï', '&Ugrave;': 'Ù', '&reg;': '®', '&Eacute;': 'É', '&ugrave;': 'ù', '&copy;': '©', '&eacute;': 'é', '&Oacute;': 'Ó',
                        '&Uuml;'  : 'Ü', '&THORN;': 'Þ', '&Ecirc;': 'Ê', '&oacute;': 'ó', '&uuml;': 'ü', '&thorn;': 'þ', '&ecirc;': 'ê', '&Ocirc;': 'Ô', '&szlig;': 'ß',
                        '&#33;'   : '!', '&#65;': 'A', '&#97;': 'a', '&#163;': '£', '&#195;': 'Ã', '&#227;': 'ã', '&#34;': '"', '&#66;': 'B', '&#98;': 'b', '&#164;': '¤', '&#196;': 'Ä', '&#228;': 'ä', '&#35;': '#', '&#67;': 'C', '&#99;': 'c',
                        '&#165;'  : '¥', '&#197;': 'Å', '&#229;': 'å', '&#36;': '$', '&#68;': 'D', '&#100;': 'd', '&#166;': '¦', '&#198;': 'Æ', '&#230;': 'æ', '&#37;': '%', '&#69;': 'E', '&#101;': 'e', '&#167;': '§', '&#199;': 'Ç',
                        '&#231;'  : 'ç', '&#38;': '&', '&#70;': 'F', '&#102;': 'f', '&#168;': '¨', '&#200;': 'È', '&#232;': 'è', '&#39;': "'", '&#71;': 'G', '&#103;': 'g', '&#169;': '©', '&#201;': 'É', '&#233;': 'é', '&#40;': '(',
                        '&#72;'   : 'H', '&#104;': 'h', '&#170;': 'ª', '&#202;': 'Ê', '&#234;': 'ê', '&#41;': ')', '&#73;': 'I', '&#105;': 'i', '&#171;': '«', '&#203;': 'Ë', '&#235;': 'ë', '&#42;': '*', '&#74;': 'J', '&#106;': 'j',
                        '&#172;'  : '¬', '&#204;': 'Ì', '&#236;': 'ì', '&#43;': '+', '&#75;': 'K', '&#107;': 'k', '&#173;': '  ', '&#205;': 'Í', '&#237;': 'í', '&#44;': ',', '&#76;': 'L', '&#108;': 'l', '&#174;': '®', '&#206;': 'Î',
                        '&#238;'  : 'î', '&#45;': '-', '&#77;': 'M', '&#109;': 'm', '&#175;': '¯', '&#207;': 'Ï', '&#239;': 'ï', '&#46;': '.', '&#78;': 'N', '&#110;': 'n', '&#176;': '°', '&#208;': 'Ð', '&#240;': 'ð', '&#47;': '/',
                        '&#79;'   : 'O', '&#111;': 'o', '&#177;': '±', '&#209;': 'Ñ', '&#241;': 'ñ', '&#48;': '0', '&#80;': 'P', '&#112;': 'p', '&#178;': '²', '&#210;': 'Ò', '&#242;': 'ò', '&#49;': '1', '&#81;': 'Q', '&#113;': 'q',
                        '&#179;'  : '³', '&#211;': 'Ó', '&#243;': 'ó', '&#50;': '2', '&#82;': 'R', '&#114;': 'r', '&#180;': '´', '&#212;': 'Ô', '&#244;': 'ô', '&#51;': '3', '&#83;': 'S', '&#115;': 's', '&#181;': 'µ', '&#213;': 'Õ',
                        '&#245;'  : 'õ', '&#52;': '4', '&#84;': 'T', '&#116;': 't', '&#182;': '¶', '&#214;': 'Ö', '&#246;': 'ö', '&#53;': '5', '&#85;': 'U', '&#117;': 'u', '&#183;': '·', '&#215;': '×', '&#247;': '÷', '&#54;': '6',
                        '&#86;'   : 'V', '&#118;': 'v', '&#184;': '¸', '&#216;': 'Ø', '&#248;': 'ø', '&#55;': '7', '&#87;': 'W', '&#119;': 'w', '&#185;': '¹', '&#217;': 'Ù', '&#249;': 'ù', '&#56;': '8', '&#88;': 'X', '&#120;': 'x',
                        '&#186;'  : 'º', '&#218;': 'Ú', '&#250;': 'ú', '&#57;': '9', '&#89;': 'Y', '&#121;': 'y', '&#187;': '»', '&#219;': 'Û', '&#251;': 'û', '&#58;': ':', '&#90;': 'Z', '&#122;': 'z', '&#188;': '¼', '&#220;': 'Ü',
                        '&#252;'  : 'ü', '&#59;': ';', '&#91;': '[', '&#123;': '{', '&#189;': '½', '&#221;': 'Ý', '&#253;': 'ý', '&#60;': '<', '&#92;': '\\', '&#124;': '|', '&#190;': '¾', '&#222;': 'Þ', '&#254;': 'þ', '&#61;': '=',
                        '&#93;'   : ']', '&#125;': '}', '&#191;': '¿', '&#223;': 'ß', '&#255;': 'ÿ', '&#62;': '>', '&#94;': '^', '&#126;': '~', '&#192;': 'À', '&#224;': 'à', '&#256;': 'Ā', '&#63;': '?', '&#95;': '_', '&#161;': '¡',
                        '&#193;'  : 'Á', '&#225;': 'á', '&#64;': '@', '&#96;': '`', '&#162;': '¢', '&#194;': 'Â', '&#226;': 'â']
        entities.each { k, v ->
            texto = texto.replaceAll(k, v)
        }
        return texto
    }

    /**
     * método para remover a tag html <br> e &nbsp; dos textos
     * @param texto
     * @return
     */
    private String cleanHtml(String texto) {
        texto = texto ?:''
        texto = texto.replaceAll(/<br ?\/?>/, '. ')
        return htmlCleaner.cleanHtml(texto, 'none')
                .replaceAll(/&nbsp;/, ' ')
                .trim()
    }

    /**
     * método para trocar ponto e virgula por vírgula
     * @param texto
     * @return
     */
    private trocarPontoEvirgula(String texto = '') {
        texto = entity2Char(texto)
        return texto.replaceAll(/;/, ',')
    }

    /**
     * método para gerar arquivo CSV com os registros de ocorrências das fichas selecionadas.
     * O parâmetro idFichas deve ser os ids das fichas separados por virgula
     * @param response
     * @param idFichas
     * @return
     */
    void ocorrencias(GrailsHttpSession selfSession, Map workerData) {

        workerData.sqOcorrenciasSelecionadasSalve ?: ''
        workerData.sqOcorrenciasSelecionadasPortalbio ?:''
        if( workerData.sqOcorrenciasSelecionadasSalve || workerData.sqOcorrenciasSelecionadasPortalbio ) {

            // ajustar parametros de filtragem das ocorrencias selecionadas quando houver
            if (workerData.sqOcorrenciasSelecionadasSalve != '' && workerData.sqOcorrenciasSelecionadasPortalbio == '') {
                workerData.sqOcorrenciasSelecionadasPortalbio = '0'
            }
            else if (workerData.sqOcorrenciasSelecionadasPortalbio != '' && workerData.sqOcorrenciasSelecionadasSalve == '') {
                workerData.sqOcorrenciasSelecionadasSalve = '0'
            }
        }

        // perfil EXTERNO não pode visualizar registros em carência
        if( selfSession.sicae.user.isEX() )
        {
            workerData.excluirRegistrosEmCarencia = 'S'
        }


        /** /
        println ' '
        println ' '
        println ' '
        println 'Ocorrencias Salve '
        println workerData.sqOcorrenciasSelecionadasSalve
        println ' '
        println 'Ocorrencias Portalbio '
        println workerData.sqOcorrenciasSelecionadasPortalbio
        println ' '
        return
        /**/

        String noCientifico=''
        List headers = [ '"Reino"', '"Filo"', '"Classe"', '"Ordem"','"Família"','"Gênero"'
                         ,'"Nome Científico"'
                         ,'"Autor"'
                         , '"Latitude"'
                         , '"Longitude"'
                         , '"Usado na avaliação?"'
                         , '"Justificativa (quando não usado)"'
                         , '"Prazo de carência"'
                         , '"Datum"'
                         , '"Formato Original"'
                         , '"Precisão da Coordenada"'
                         , '"Referência de Aproximação"'
                         , '"Metodologia de Aproximação"'
                         , '"Descrição Metodo de aproximação"'
                         , '"Taxon Originalmente citado"'
                         , '"Presença atual "'
                         , '"Tipo de registro"'
                         , '"Dia"'
                         , '"Mês"'
                         , '"Ano"'
                         , '"Hora"'
                         , '"Dia fim"'
                         , '"Mês fim"'
                         , '"Ano fim"'
                         , '"Data e Ano desconhecidos"'
                         , '"Localidade"'
                         , '"Característica da localidade"'
                         , '"UC Federais"'
                         , '"UC Não Federais"'
                         , '"RPPN"'
                         , '"País"'
                         , '"Estado"'
                         , '"Município"'
                         , '"Ambiente"'
                         , '"Hábitat"'
                         , '"Elevação mínima"'
                         , '"Elevação máxima"'
                         , '"Profundidade mínima"'
                         , '"Profundidade máxima"'
                         , '"Identificador"'
                         , '"Data identificação"'
                         , '"Identificação incerta "'
                         , '"Tombamento"'
                         , '"Instituição tombamento"'
                         , '"Compilador"'
                         , '"Data compilação"'
                         , '"Revisor"'
                         , '"Data revisão"'
                         , '"Validador"'
                         , '"data validação"'
                         , '"Título"'
                         , '"Autor registro"'
                         , '"Ano registro"'
                         , '"Comunicação Pessoal"'
                         , '"Base de Dados"'
                         , '"Id Origem"'
                         , '"Observação"']

        String path = grailsApplication.config.temp.dir
        String hora = new Date().format('dd_MM_yyyy_hh_mm_ss')
        String fileName = path + 'salve_exportacao_registros_' + hora + '.csv'
        JSONObject jsonOcorrencia

        /** /
         println 'PONTO 1 OK'
         println 'Workdata Recebido'
         println workerData
         println 'Arquivo saida: ' + fileName
         println '-'*100
         println ' '
         /**/

        /*// adicionar os ids das subespecies
        List idSubespecies = []
        workerData.ids.each {
            Ficha ficha = Ficha.get( it.toInteger() )
            if( ficha.taxon.nivelTaxonomico.coNivelTaxonomico.toUpperCase() =='ESPECIE')
            {
                Ficha.createCriteria().list {
                    createAlias('taxon','t')
                    eq('t.pai',ficha.taxon)

                }.each {
                    idSubespecies.push( it.id )
                }
            }
        }
        // adicionar os ids das fichas das subespecies
        workerData.ids += idSubespecies
        */
/** /
 List lista = FichaOcorrencia.executeQuery("""
 Select new map( ficha.id as sq_ficha ) from Ficha ficha
 where ficha.unidade.id in( 3480,3485,3482 )
 and ficha.cicloAvaliacao.id = 2 """)
 workerData.ids =lista.sq_ficha
 println workerData
 /**/
        DadosApoio contexto1 = dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA','REGISTRO_OCORRENCIA')
        DadosApoio contexto2 = dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA','CONSULTA')
        String cmdSql = """select fo.*
                    ,coalesce( taxon.json_trilha->'subespecie'->>'no_taxon',taxon.json_trilha->'especie'->>'no_taxon') as nm_cientifico_taxon
                    ,taxon.no_autor as no_autor_taxon
                    from salve.vw_ficha_ocorrencia fo
                    inner join salve.vw_ficha on vw_ficha.sq_ficha = fo.sq_ficha
                    inner join taxonomia.taxon on taxon.sq_taxon = vw_ficha.sq_taxon
                    where fo.sq_ficha in ( ${workerData.ids.join(',')} )"""
        if( workerData.sqOcorrenciasSelecionadasSalve ) {
            cmdSql += """ and ( sq_ficha_ocorrencia in (${workerData.sqOcorrenciasSelecionadasSalve}) )"""
        }

        if (workerData.somenteUtilizadosAvaliacao == 'U') {
            cmdSql += """ and ( ds_utilizado_avaliacao <> 'Não' )"""
        } else if (workerData.somenteUtilizadosAvaliacao == 'NU') {
            cmdSql += """ and ( ds_utilizado_avaliacao = 'Não' )"""
        }

        List ocorrencias = []
        // ler somente as ocorrencias selecionadas no gride
        if( workerData.sqOcorrenciasSelecionadasSalve != '0') {
            ocorrencias = sqlService.execSql(cmdSql)
        }

        /** /
         println ' '
         println ' '
         println ' '
         println workerData
         println 'sql'
         println cmdSql
         println 'Ocorrencias encontradas'
         println ocorrencias.size()
        //return;

         /**/

        /*List ocorrencias = FichaOcorrencia.createCriteria().list {
            'in'('vwFicha', VwFicha.getAll( workerData.ids.toList() ) )
            'in'('contexto', [contexto1,contexto2])
            vwFicha {
                order("nmCientifico")
            }

        }
        */
        /*println ' '
        println 'Quantidades:'
        println ocorrencias1.size()
        println ocorrencias.size()
        println ' '
        */

        // ler somente as ocorrências do portalbio que foram selecionadas no gride
        List ocorrenciasPortalbio = []
        //if( workerData.sqOcorrenciasSelecionadasPortalbio != '0' ) {
            ocorrenciasPortalbio = FichaOcorrenciaPortalbio.createCriteria().list {
                'in'('vwFicha', Ficha.getAll(workerData.ids.toList()))
                // quando a exportaçao for do gride do modulo fichas exportar somente as ocorrencias utilizadas na avaliaçao
                //if( workerData.contexto == 'ficha') {
                if (workerData.somenteUtilizadosAvaliacao == 'U') {
                        'eq'('inUtilizadoAvaliacao', 'S')
                } else if (workerData.somenteUtilizadosAvaliacao == 'NU') {
                    or {
                        'eq'('inUtilizadoAvaliacao', 'N')
                        isNull('inUtilizadoAvaliacao')
                    }
                }
                //}
                if (workerData.sqOcorrenciasSelecionadasPortalbio) {
                    'in'('id', workerData.sqOcorrenciasSelecionadasPortalbio.split(',').collect { it.toLong() })
                }
                vwFicha {
                    order("nmCientifico")
                }
            }
        //}
        // criar worker na sessão
        // adicionar worker na sessão do usuário
        Map worker = workerService.add(selfSession
                , 'Exportando as ocorrências de ' + workerData.ids.size() + ' ficha(s).'
                , ocorrencias.size() + ocorrenciasPortalbio.size() + 1 // adicionar um passo a mais para a gravação dos dados em disco no final
        )

        if( ocorrencias.size() + ocorrenciasPortalbio.size()  > 0  ) {
            //File file =
            // file.append(headers.join(';') + '\n')
            StringBuilder sb = new StringBuilder()
            FileOutputStream file = new FileOutputStream( new File(fileName) )
            sb.append( headers.join(';') + '\n')
            //https://stackoverflow.com/questions/4576222/fastest-way-to-write-to-file

            workerService.setCallback(worker,"window.open(app.url + 'main/download?fileName=" + fileName + "&delete=1','_top');")
            workerService.start(worker)
            Date carencia = null

             /** /
             println 'Usuario: '+selfSession.sicae.user.noPessoa+'  em ' + new Date().format('dd/MM/yyyy HH:mm:ss')
             println ocorrencias.size()+' ocorrencias salve encontradas!'
             println ocorrenciasPortalbio.size()+' ocorrencias portalbio encontradas!'
             println 'Total: '+ocorrencias.size()+ocorrenciasPortalbio.size()
             println workerData.sqOcorrenciasSelecionadasPortalbio
             println 'Excluir Em Carencia? ' + workerData.excluirRegistrosEmCarencia

            println "-"*100
             println ' '
             /**/

            ocorrencias.sort{ it.nm_cientifico}.eachWithIndex { it,index ->
                //throw new RuntimeException('Teste de erro!')
                try {
                    if( it.nm_cientifico != noCientifico )
                    {
                        noCientifico = it.nm_cientifico
                        //log.info("Exportando ocorrecias de " + noCientifico)
                        workerService.setTitle( worker, it.nm_cientifico )
                    }

                    // ler a arvore taxonomica
                    VwFicha vw = VwFicha.get( it.sq_ficha.toLong() )
                    Map arvoreTaxonomica = vw.taxon.structure
//
//println arvoreTaxonomica
//println '- '*50



                    if ( workerData.excluirRegistrosEmCarencia == 'N' || !it.dt_carencia || it.dt_carencia < new Date() ) {
                        // adicionar linha no arquivo de saida
                        sb.append( ['"' + (arvoreTaxonomica.nmReino?:'') + '"'
                                    ,'"' + (arvoreTaxonomica.nmFilo?:'') + '"'
                                    ,'"' + (arvoreTaxonomica.nmClasse?:'') + '"'
                                    ,'"' + (arvoreTaxonomica.nmOrdem?:'') + '"'
                                    ,'"' + (arvoreTaxonomica.nmFamilia?:'') + '"'
                                    ,'"' + (arvoreTaxonomica.nmGenero?:'') + '"'
                                    , '"' + it.nm_cientifico_taxon + '"'
                                    , '"' + (it.no_autor_taxon ?: '')+ '"'
                                    , '"' + (it?.nu_y ? it?.nu_y.toString().replaceAll(/\./, ',') : '') + '"'
                                    , '"' + (it?.nu_x ? it?.nu_x.toString().replaceAll(/\./, ',') : '') + '"'
                                    , '"' + trocarPontoEvirgula(it.ds_utilizado_avaliacao ?: 'Não avaliado') + '"'
                                    , '"' + trocarPontoEvirgula(it.ds_nao_utilizado_avaliacao ?: '') + '"'
                                    , '"' + (it?.dt_carencia ? it.dt_carencia.format('dd/MM/yyyy') : '') + '"'
                                    , '"' + trocarPontoEvirgula(it?.no_datum ?: '') + '"'
                                    , '"' + trocarPontoEvirgula(it?.no_formato_original ?: '') + '"'
                                    , '"' + trocarPontoEvirgula(it?.no_precisao ?: '') + '"'
                                    , '"' + trocarPontoEvirgula(it?.no_referencia_aproximacao ?: '') + '"'
                                    , '"' + trocarPontoEvirgula(it?.no_metodologia_aproximacao ?: '') + '"'
                                    , '"' + trocarPontoEvirgula(it?.tx_metodologia_aproximacao ? cleanHtml(it.tx_metodologia_aproximacao.toString()) : '') + '"'
                                    , '"' + trocarPontoEvirgula(it?.no_taxon_citado ?: '') + '"'
                                    , '"' + trocarPontoEvirgula(it?.ds_presenca_atual ?: '') + '"'
                                    , '"' + trocarPontoEvirgula(it?.no_tipo_registro ?: '') + '"'
                                    , '"' + (it?.nu_dia_registro ?: '') + '"'
                                    , '"' + (it?.nu_mes_registro ?: '') + '"'
                                    , '"' + (it?.nu_ano_registro ?: '') + '"'
                                    , '"' + (it?.hr_registro ?: '') + '"'
                                    , '"' + (it?.nu_dia_registro_fim ?: '') + '"'
                                    , '"' + (it?.nu_mes_registro_fim ?: '') + '"'
                                    , '"' + (it?.nu_ano_registro_fim ?: '') + '"'
                                    , '"' + trocarPontoEvirgula(it?.ds_data_desconhecida ?: '') + '"'
                                    , '"' + trocarPontoEvirgula(it?.no_localidade ?: '') + '"'
                                    , '"' + trocarPontoEvirgula(it?.ds_caracteristica_localidade ? cleanHtml(it?.ds_caracteristica_localidade) : '') + '"'
                                    , '"' + trocarPontoEvirgula(it?.no_uc_federal ?: '') + '"'
                                    , '"' + trocarPontoEvirgula(it?.no_uc_estadual ?: '') + '"'
                                    , '"' + trocarPontoEvirgula(it?.no_rppn ?: '') + '"'
                                    , '"' + trocarPontoEvirgula(it?.no_pais ?: '') + '"'
                                    , '"' + trocarPontoEvirgula(it?.no_estado ?: '') + '"'
                                    , '"' + trocarPontoEvirgula(it?.no_municipio ?: '') + '"'
                                    , '"' + trocarPontoEvirgula(it?.no_ambiente ?: '') + '"'
                                    , '"' + trocarPontoEvirgula(it?.no_habitat ?: '') + '"'
                                    , '"' + (it?.vl_elevacao_min ?: '') + '"'
                                    , '"' + (it?.vl_elevacao_max ?: '') + '"'
                                    , '"' + (it?.vl_profundidade_min ?: '') + '"'
                                    , '"' + (it?.vl_profundidade_max ?: '') + '"'
                                    , '"' + (it?.de_identificador ?: '') + '"'
                                    , '"' + (it?.dt_identificacao ? it.dt_identificacao.format('dd/MM/yyyy') : '') + '"'
                                    , '"' + trocarPontoEvirgula(it?.no_identificacao_incerta ?: '') + '"'
                                    , '"' + trocarPontoEvirgula(it?.ds_tombamento ?: '') + '"'
                                    , '"' + trocarPontoEvirgula(it?.ds_instituicao_tombamento ?: '') + '"'
                                    , '"' + trocarPontoEvirgula(it?.no_compilador ?: '') + '"'
                                    , '"' + (it?.dt_compilacao ? it?.dt_compilacao.format('dd/MM/yyyy') : '') + '"'
                                    , '"' + trocarPontoEvirgula(it?.no_revisor ?: '') + '"'
                                    , '"' + (it?.dt_revisao ? it?.dt_revisao.format('dd/MM/yyyy') : '') + '"'
                                    , '"' + trocarPontoEvirgula(it?.no_validador ?: '') + '"'
                                    , '"' + (it?.dt_validacao ? it?.dt_validacao.format('dd/MM/yyyy') : '') + '"'
                                    , '"' + trocarPontoEvirgula(it?.de_titulo_publicacao ?: '') + '"'
                                    , '"' + trocarPontoEvirgula(it?.no_autor ? it?.no_autor.replaceAll(/\n/, '; ') : '') + '"'
                                    , '"' + (it?.nu_ano_publicacao ?: '') + '"'
                                    , '"' + trocarPontoEvirgula(it.de_com_pessoal ?: '') + '"'
                                    , '"SALVE"'
                                    , '"' + (it?.id_origem ?: '') + '"'
                                    , '"' + (it?.tx_observacao ?: '') + '"'
                        ].join(';') + "\n"
                        )

                        // gravar em disco a cada 1000
                        if ( ((index+1) % 1000 == 0) && sb.length() > 0) {
                            //println 'ExportDataService: gravando buffer (1000 linhas) - ' + new Date().format('dd/MM/yyyy HH:mm:ss')+' - '+selfSession.sicae.user.noPessoa

                            String s = new String( sb.toString().getBytes(), "ISO-8859-1")
                            file.write( s.toString().getBytes() )

                            //file.write(sb.toString().getBytes())
                            sb = new StringBuilder()
                        }
                    } /*else {
                        println "Registro em carencia ate " + it.dt_carencia.format('dd/mm/yyyy') + ' foi ignorado...'
                    }*/
                    // atualizar andamento do worker
                } catch( Exception e ) {
                    println ' '
                    println ' '
                    println ' '
                    println 'ExportDataService - Erro na ficha ' + it.sq_ficha
                    println e.getMessage()
                    //println e.printStackTrace()
                    println "-"*100
                    println ' '
                }
                workerService.stepIt(worker)
            }

            // adicionar as ocorrências do portalbio
            ocorrenciasPortalbio.eachWithIndex { it,index ->
                // ler a referencia bibliográfica
                carencia = it?.dtCarencia ? it.dtCarencia : null
                String noInstituicao = it?.noInstituicao ? it?.noInstituicao : ''
                if( it.noBaseDados )
                {
                    noInstituicao = it.noBaseDados
                }

                jsonOcorrencia = null
                if ( workerData.excluirRegistrosEmCarencia == 'N' || ! carencia || carencia < new Date() ) {
                    try {
                        jsonOcorrencia = JSON.parse( it.txOcorrencia.toString() )
                    } catch( Exception e){
                        jsonOcorrencia = null
                    }

                    // ler a arvore taxonomica
                    Map arvoreTaxonomica = it.vwFicha.taxon.structure
                    String noCientificoPortalbio = arvoreTaxonomica.nmSubespecie ? arvoreTaxonomica.nmSubespecie : arvoreTaxonomica.nmEspecie
                    String noAtorTaxon = it.vwFicha.taxon.noAutor ?: ''

                    sb.append(
                            //file.append(
                            [
                                    '"' + (arvoreTaxonomica.nmReino?:'') + '"'
                                    ,'"' + (arvoreTaxonomica.nmFilo?:'') + '"'
                                    ,'"' + (arvoreTaxonomica.nmClasse?:'') + '"'
                                    ,'"' + (arvoreTaxonomica.nmOrdem?:'') + '"'
                                    ,'"' + (arvoreTaxonomica.nmFamilia?:'') + '"'
                                    ,'"' + (arvoreTaxonomica.nmGenero?:'') + '"'
                                    ,'"' + (noCientificoPortalbio?:'') + '"'
                                    //,'"' + it.noCientifico + '"'
                                    //,'"' + it.vwFicha.nmCientifico + '"'
                                    ,'"' + noAtorTaxon +  '"'
                                    , '"' + (it?.latDecimal ? it?.latDecimal.toString().replaceAll(/\./, ',') : '') + '"'
                                    , '"' + (it?.lonDecimal ? it?.lonDecimal.toString().replaceAll(/\./, ',') : '') + '"'
                                    , '"' + trocarPontoEvirgula((it?.inUtilizadoAvaliacaoText ? it?.inUtilizadoAvaliacaoText : 'Não avaliado')) + '"'
                                    , '"' + trocarPontoEvirgula( ( it?.inUtilizadoAvaliacaoText.toLowerCase() == 'não' ? cleanHtml(it?.txNaoUtilizadoAvaliacao) : '') ) + '"'
                                    , '"' + (carencia ? carencia.format('dd/MM/yyyy') : '') + '"'//(it?.prazoCarencia?.descricao ? it?.prazoCarencia?.descricao : '') + '"'
                                    , '""' // datum
                                    , '""' // formato original da coordenada
                                    , '"' + ( jsonOcorrencia?.precisaoCoord ? ' (' + jsonOcorrencia.precisaoCoord + ')': '' )  + '"'
                                    , '""' // referencia de aproximação
                                    , '""' // metodoAproximacao
                                    , '""' // txMetodoAproximacao
                                    , '""' // taxon citado
                                    , '""' // inPresencaAtualText
                                    , '""' //+ trocarPontoEvirgula((it?.tipoRegistro?.descricao ? it?.tipoRegistro?.descricao : '')) + '"'
                                    , '""' //+ (it?.nuDiaRegistro ? it?.nuDiaRegistro : '') + '"'
                                    , '""' //+ (it?.nuMesRegistro ? it?.nuMesRegistro : '') + '"'
                                    , '""' //+ (it?.nuAnoRegistro ? it?.nuAnoRegistro : '') + '"'
                                    , '""' //+ (it?.hrRegistro ? it?.hrRegistro : '') + '"'
                                    , '""' //+ (it?.nuDiaRegistroFim ? it?.nuDiaRegistroFim : '') + '"'
                                    , '""' //+ (it?.nuMesRegistroFim ? it?.nuMesRegistroFim : '') + '"'
                                    , '""' //+ (it?.nuAnoRegistroFim ? it?.nuAnoRegistroFim : '') + '"'
                                    , '""' //+ trocarPontoEvirgula((it?.inDataDesconhecidaText ? it?.inDataDesconhecidaText : '')) + '"'
                                    , '"' + trocarPontoEvirgula((it?.noLocal ? it?.noLocal : '')) + '"'
                                    , '""' //+ trocarPontoEvirgula((it?.txLocal ? it?.txLocal : '')) + '"'
                                    , '""' //+ trocarPontoEvirgula((it?.noUcFederal ? it?.noUcFederal : '')) + '"'
                                    , '""' //+ trocarPontoEvirgula((it?.noUcEstadual ? it?.noUcEstadual : '')) + '"'
                                    , '""' //+ trocarPontoEvirgula((it?.noRppn ? it?.noRppn : '')) + '"'
                                    , '""' //+ trocarPontoEvirgula((it?.pais?.noPais ? it?.pais?.noPais : '')) + '"'
                                    , '""' //+ trocarPontoEvirgula((it?.estado?.noEstado ? it?.estado?.noEstado : '')) + '"'
                                    , '""' //+ trocarPontoEvirgula((it?.municipio?.noMunicipio ? it?.municipio?.noMunicipio : '')) + '"'
                                    , '""' //+ trocarPontoEvirgula((it?.habitat ? it?.ambiente?.descricao : '')) + '"'
                                    , '""' //+ trocarPontoEvirgula((it?.habitat ? it?.habitat?.descricao : '')) + '"'
                                    , '""' //+ (it?.vlElevacaoMin ? it?.vlElevacaoMin : '') + '"'
                                    , '""' //+ (it?.vlElevacaoMax ? it?.vlElevacaoMax : '') + '"'
                                    , '""' //+ (it?.vlProfundidadeMin ? it?.vlProfundidadeMin : '') + '"'
                                    , '""' //+ (it?.vlProfundidadeMax ? it?.vlProfundidadeMax : '') + '"'
                                    , '""' //+ (it?.deIdentificador ? it?.deIdentificador : '') + '"'
                                    , '""' //+ (it?.dtIdentificacao ? it.dtIdentificacao.format('dd/MM/yyyy') : '') + '"'
                                    , '""' //+ trocarPontoEvirgula((it?.qualificadorValTaxon ? it?.qualificadorValTaxon?.descricao : '')) + '"'
                                    , '""' //+ trocarPontoEvirgula((it?.txTombamento ? it?.txTombamento : '')) + '"'
                                    , '""' //+ trocarPontoEvirgula((it?.noInstituicao ? it?.noInstituicao : '')) + '"'
                                    , '""' //+ trocarPontoEvirgula((it?.pessoaCompilador ? it?.pessoaCompilador?.noPessoa + '-' + it?.pessoaCompilador?.nuCpfFormatado : '')) + '"'
                                    , '""' //+ (it?.dtCompilacao ? it?.dtCompilacao.format('dd/MM/yyyy') : '') + '"'
                                    , '"' + trocarPontoEvirgula((it?.pessoaRevisor ? it?.pessoaRevisor?.noPessoa + '-' + it?.pessoaRevisor.nuCpfFormatado : '')) + '"'
                                    , '"' + (it?.dtRevisao ? it?.dtRevisao.format('dd/MM/yyyy') : '') + '"'
                                    , '"' + trocarPontoEvirgula((it?.pessoaValidador ? it?.pessoaValidador?.noPessoa + '-' + it?.pessoaValidador.nuCpfFormatado : '')) + '"'
                                    , '"' + (it?.dtValidacao ? it?.dtValidacao.format('dd/MM/yyyy') : '') + '"'
                                    , '""' //+ trocarPontoEvirgula((refBib.titulo ? refBib.titulo : '')) + '"'
                                    , '""' //+ trocarPontoEvirgula((refBib.autor ? refBib.autor.replaceAll(/\n/, '; ') : '')) + '"'
                                    , '""' //+ (refBib.ano ? refBib.ano : '') + '"'
                                    , '""' //+ trocarPontoEvirgula((refBib.comPessoal ? refBib.comPessoal : '')) + '"'
                                    , '"'+ trocarPontoEvirgula( noInstituicao ) + '"'
                                    //, '"PORTALBIO'+( jsonOcorrencia?.codigoColecao ? '/'+jsonOcorrencia.codigoColecao : '' )+'"'
                                    , '""' // + (it?.idOrigemText ?: '') + '"'
                            ].join(';') + "\n")
                    // gravar em disco a cada 1000
                    if ( ((index+1) % 1000 == 0) && sb.length() > 0) {
                        //println 'ExportDataService: gravando buffer (1000 linhas) - ' + new Date().format('dd/MM/yyyy HH:mm:ss')+' - '+selfSession.sicae.user.noPessoa
                        //file.write(sb.toString().getBytes())

                        String s = new String( sb.toString().getBytes(), "ISO-8859-1")
                        file.write( s.toString().getBytes() )

                        sb = new StringBuilder()
                    }

                } /*else {
                    println "Registro em carencia ate " + carencia.format('dd/mm/yyyy') + ' foi ignorado...'
                }*/
                // atualizar andamento do worker
                workerService.stepIt(worker)
            }
            if ( sb.length() > 0 ) {
                //println 'ExportDataService: Finalizando exportacao - ' + new Date().format('dd/MM/yyyy HH:mm:ss')+' - '+selfSession.sicae.user.noPessoa
                String s = new String( sb.toString().getBytes(), "ISO-8859-1")
                file.write( s.toString().getBytes() )
                //file.write( sb.toString().getBytes() )
            }
            file.close()
            workerService.stepIt(worker)
        }
        else
        {
            workerService.setMessage(worker,"Nenhuma ocorrência encontrada para exportação.");
        }
    }

    void ocorrenciasPlanilha(GrailsHttpSession selfSession, Map workerData) {
        debug = true //grailsApplication.config.env == 'desenv'

        // variaveis
        List taxonsPai = []
        String noCientifico = ''
        String path = grailsApplication.config.temp.dir
        String horaArquivo = new Date().format('dd_MM_yyyy_HH_mm_ss')
        String horaInicio = new Date().format('dd/MM/yyyy HH:mm:ss')
        String fileName = path + 'salve_exportacao_registros_' + horaArquivo + '.xlsx'
        String urlSalve = grailsApplication.config.url.sistema ?:'https://salve.icmbio.gov.br/salve-estadual/'
        String linkDownload = '<a target="_blank" href="'+urlSalve+'download?fileName='+fileName+'"><b>AQUI</b></a>'
        JSONObject jsonOcorrencia
        PlanilhaExcel planilha
        String cmdSql
        String sqlPasso1 = ''
        List autores=[] // para gerar a citação
        String refBibs='' //  para gerar a coluna referencia bibliográfica
        Map worker
        List listData = []
        boolean shape = false

        if( ! workerData.nuAnoCicloAvaliacao ) {
            if( workerData.sqCicloAvaliacao ) {
                CicloAvaliacao cicloAvaliacao = CicloAvaliacao.get(workerData.sqCicloAvaliacao.toLong());
                workerData.nuAnoCicloAvaliacao = cicloAvaliacao.nuAno
            } else {
                // ler o ciclo atual
                CicloAvaliacao cicloAvaliacao = CicloAvaliacao.createCriteria().get{
                    lt('nuAno',new Date().format('yyyy').toInteger() )
                    eq('inSituacao','A')
                    maxResults(1)
                    order('nuAno','desc')
                }
                workerData.nuAnoCicloAvaliacao = cicloAvaliacao.nuAno
                //println 'Ano do ciclo calculado ' +  cicloAvaliacao.nuAno
            }
        }
        try {
            /** /
            println ' '
            println 'ExportDataService().ocorrenciasPlanilha()'
            println 'Parametros'
            println workerData
            // throw new Exception('Teste parametros.');
            /**/
            // flag na sessão para nao permitir mais de uma exportação por vez
            selfSession.exportingOccurrences = true
            workerData.sqOcorrenciasSelecionadasSalve ?: ''
            workerData.sqOcorrenciasSelecionadasPortalbio ?: ''
            if (workerData.sqOcorrenciasSelecionadasSalve || workerData.sqOcorrenciasSelecionadasPortalbio) {

                // ajustar parametros de filtragem das ocorrencias selecionadas quando houver
                if (workerData.sqOcorrenciasSelecionadasSalve != '' && workerData.sqOcorrenciasSelecionadasPortalbio == '') {
                    workerData.sqOcorrenciasSelecionadasPortalbio = '0'
                } else if (workerData.sqOcorrenciasSelecionadasPortalbio != '' && workerData.sqOcorrenciasSelecionadasSalve == '') {
                    workerData.sqOcorrenciasSelecionadasSalve = '0'
                }
            }
            // Por segurança, validar aqui tambem se o perfil pode exportar os registros em carência
            // perfil EXTERNO, Coordenador Taxon e Consulta não pode visualizar registros em carência
            if (!selfSession.sicae.user.canExportRegsCarencia()) {
                workerData.excluirRegistrosEmCarencia = 'S'
                workerData.excluirRegistrosSensiveis = 'S'
            }
            planilha = new PlanilhaExcel(fileName, 'Ocorrências', debug)
            planilha.addColumn(['title': 'Reino', 'column': 'no_reino', 'autoFit': true]) //  , color:'#ff0000', 'bgColor': '#ffffcc',align:'center'] )
            planilha.addColumn(['title': 'Filo', 'column': 'no_filo', 'autoFit': true ])
            planilha.addColumn(['title': 'Classe', 'column': 'no_classe', 'autoFit': true])
            planilha.addColumn(['title': 'Ordem', 'column': 'no_ordem', 'autoFit': true])
            planilha.addColumn(['title': 'Família', 'column': 'no_familia', 'autoFit': true])
            planilha.addColumn(['title': 'Gênero', 'column': 'no_genero', 'autoFit': true])
            planilha.addColumn(['title': 'Nome Científico', 'column': 'no_cientifico', 'autoFit': true])
            planilha.addColumn(['title': 'Autor', 'column': 'no_autor_taxon', 'autoFit': true])
            planilha.addColumn(['title': 'Latitude', 'column': 'nu_latitude', 'autoFit': true])
            planilha.addColumn(['title': 'Longitude', 'column': 'nu_longitude', 'autoFit': true])
            planilha.addColumn(['title': 'Endêmica Brasil', 'column': 'de_endemica_brasil', 'autoFit': true])
            planilha.addColumn(['title': 'Categoria Anterior', 'column': 'ds_categoria_anterior', 'autoFit': true])
            planilha.addColumn(['title': 'Categoria Avaliada', 'column': 'ds_categoria_avaliada', 'autoFit': true])
            planilha.addColumn(['title': 'Categoria Validada', 'column': 'ds_categoria_validada', 'autoFit': true])
            planilha.addColumn(['title': 'Plano de Ação Nacional', 'column': 'ds_pan', 'autoFit': true])
            planilha.addColumn(['title': 'Atitude (m)', 'column': 'nu_altitude', 'autoFit': true])
            planilha.addColumn(['title': 'Sensível?', 'column': 'ds_sensivel', 'autoFit': true])

            planilha.addColumn(['title': 'Situação do registro', 'column': 'ds_situacao_avaliacao', 'autoFit': true])
            planilha.addColumn(['title': 'Motivo (quando não utilizado)', 'column': 'ds_motivo_nao_utilizado', 'autoFit': true])
            planilha.addColumn(['title': 'Justificativa (quando não utilizado)', 'column': 'tx_justificativa_nao_usado_avaliacao', 'autoFit': true])

            planilha.addColumn(['title': 'Prazo de carência', 'column': 'de_prazo_carencia', 'autoFit': true])
            planilha.addColumn(['title': 'Datum', 'column': 'no_datum', 'autoFit': true])
            planilha.addColumn(['title': 'Formato Original', 'column': 'no_formato_original', 'autoFit': true])
            planilha.addColumn(['title': 'Precisão da Coordenada', 'column': 'no_precisao', 'autoFit': true])
            planilha.addColumn(['title': 'Referência de Aproximação', 'column': 'no_referencia_aproximacao', 'autoFit': true])
            planilha.addColumn(['title': 'Metodologia der Aproximação', 'column': 'no_metodologia_aproximacao', 'autoFit': true])
            planilha.addColumn(['title': 'Descrição Metodo de aproximação', 'column': 'tx_metodologia_aproximacao', 'autoFit': true])
            planilha.addColumn(['title': 'Taxon Originalmente citado', 'column': 'no_taxon_citado', 'autoFit': true])
            planilha.addColumn(['title': 'Presença atual ', 'column': 'ds_presenca_atual', 'autoFit': true])
            planilha.addColumn(['title': 'Tipo de registro', 'column': 'no_tipo_registro', 'autoFit': true])
            planilha.addColumn(['title': 'Dia', 'column': 'nu_dia_registro', 'autoFit': true])
            planilha.addColumn(['title': 'Mês', 'column': 'nu_mes_registro', 'autoFit': true])
            planilha.addColumn(['title': 'Ano', 'column': 'nu_ano_registro', 'autoFit': true])
            planilha.addColumn(['title': 'Hora', 'column': 'hr_registro', 'autoFit': true])
            planilha.addColumn(['title': 'Dia fim', 'column': 'nu_dia_registro_fim', 'autoFit': true])
            planilha.addColumn(['title': 'Mês fim', 'column': 'nu_mes_registro_fim', 'autoFit': true])
            planilha.addColumn(['title': 'Ano fim', 'column': 'nu_ano_registro_fim', 'autoFit': true])
            planilha.addColumn(['title': 'Data e Ano desconhecidos', 'column': 'ds_data_desconhecida', 'autoFit': true])
            planilha.addColumn(['title': 'Localidade', 'column': 'no_localidade', 'autoFit': true])
            planilha.addColumn(['title': 'Característica da localidade', 'column': 'ds_caracteristica_localidade', 'autoFit': true])
            planilha.addColumn(['title': 'UC Federais', 'column': 'no_uc_federal', 'autoFit': true])
            planilha.addColumn(['title': 'UC Não Federais', 'column': 'no_uc_estadual', 'autoFit': true])
            planilha.addColumn(['title': 'RPPN', 'column': 'no_rppn', 'autoFit': true])
            planilha.addColumn(['title': 'País', 'column': 'no_pais', 'autoFit': true])
            planilha.addColumn(['title': 'Estado', 'column': 'no_estado', 'autoFit': true])
            planilha.addColumn(['title': 'Município', 'column': 'no_municipio', 'autoFit': true])
            planilha.addColumn(['title': 'Ambiente', 'column': 'no_ambiente', 'autoFit': true])
            planilha.addColumn(['title': 'Bioma', 'column': 'no_bioma', 'autoFit': true, width:50])
            planilha.addColumn(['title': 'Hábitat', 'column': 'no_habitat', 'autoFit': true])
            planilha.addColumn(['title': 'Elevação mínima', 'column': 'vl_elevacao_min', 'autoFit': true])
            planilha.addColumn(['title': 'Elevação máxima', 'column': 'vl_elevacao_max', 'autoFit': true])
            planilha.addColumn(['title': 'Profundidade mínima', 'column': 'vl_profundidade_min', 'autoFit': true])
            planilha.addColumn(['title': 'Profundidade máxima', 'column': 'vl_profundidade_max', 'autoFit': true])
            planilha.addColumn(['title': 'Identificador', 'column': 'de_identificador', 'autoFit': true])
            planilha.addColumn(['title': 'Data identificação', 'column': 'dt_identificacao', 'autoFit': true])
            planilha.addColumn(['title': 'Identificação incerta', 'column': 'no_identificacao_incerta', 'autoFit': true])
            planilha.addColumn(['title': 'Tombamento', 'column': 'ds_tombamento', 'autoFit': true])
            planilha.addColumn(['title': 'Instituição tombamento', 'column': 'ds_instituicao_tombamento', 'autoFit': true])
            planilha.addColumn(['title': 'Compilador', 'column': 'no_compilador', 'autoFit': true])
            planilha.addColumn(['title': 'Data compilação', 'column': 'dt_compilacao', 'autoFit': true])
            planilha.addColumn(['title': 'Revisor', 'column': 'no_revisor', 'autoFit': true])
            planilha.addColumn(['title': 'Data revisão', 'column': 'dt_revisao', 'autoFit': true])
            planilha.addColumn(['title': 'Validador', 'column': 'no_validador', 'autoFit': true])
            planilha.addColumn(['title': 'data validação', 'column': 'dt_validacao', 'autoFit': true])
            planilha.addColumn(['title': 'Base de Dados', 'column': 'no_base_dados', 'autoFit': true])
            planilha.addColumn(['title': 'Id Origem', 'column': 'id_origem', 'autoFit': true])
            planilha.addColumn(['title': 'Nº Autorização SISBIO', 'column': 'nu_autorizacao', 'autoFit': true, width:40])
            planilha.addColumn(['title': 'Autor registro', 'column': 'no_autor_registro', 'autoFit': true])
            planilha.addColumn(['title': 'Projeto' , 'column': 'no_projeto', 'autoFit': true, width:80])
            planilha.addColumn(['title': 'Observação', 'column': 'tx_observacao', 'autoFit': true])
            planilha.addColumn(['title': 'Referência bibliográfica', 'column': 'tx_ref_bib', 'autoFit': true, width:80])

            // mostrar/esconder colunas selecionadas
            if( workerData.colsSelected ) {
                List colsSelected = workerData.colsSelected.split(',');
                planilha.getColumns().each{
                    it.visible = colsSelected.contains( it.column )
                }
            }

            // adicionar worker na sessão do usuário
            worker = workerService.add(selfSession
                , 'Exportando as ocorrências de ' + workerData.ids.size() + ' taxon(s).'
                , 1
            )

            println ' '
            println 'Exportacao de registros iniciada em ' + horaInicio
            println 'Usuario:' + selfSession.sicae.user.noPessoa + '   cpf: ' + selfSession.sicae.user.nuCpf + '   sqPessoa: ' + selfSession.sicae.user.sqPessoa
            println 'Quantidade de fichas: ' + workerData?.ids?.size()

            // categoria vigente
            DadosApoio tipoAvaliacaoNacional = dadosApoioService.getByCodigo('TB_TIPO_AVALIACAO', 'NACIONAL_BRASIL')
            DadosApoio contextoOcorrencia  = dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA','REGISTRO_OCORRENCIA')
            //DadosApoio contexto2 = dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA','CONSULTA')
            List sqlWherePasso1 = []
            List sqlWherePasso2 = []

            List ocorrencias = []
            List ocorrenciasPortalbio = []

            // if( workerData.ids ) {
            // se for passado mais de uma especie ler as ocorrencias da visão materializada senão ler on-line
            if( workerData.ids && ( workerData.ids.size() > 1 || workerData.useMV ) ) {

            // se o contexto form o relatorio analitico dos taxons então ler da visao materializada senão ler dados on-line
            //if( workerData.contexto && workerData.contexto =='relatorio_taxon_analitico') {
                workerService.setTitle(worker, 'Lendo as ocorrências (SALVE/PORTALBIO)')
                d(' - inciando consulta ocorrencias SALVE/PORTALBIO (mv_registros)');
                if (workerData.sqCicloAvaliacao) {
                    sqlWherePasso1.push('ficha.sq_ciclo_avaliacao = ' + workerData.sqCicloAvaliacao.toString())
                    sqlWherePasso2.push('mv.sq_ciclo_avaliacao = ' + workerData.sqCicloAvaliacao.toString())
                }
                if (workerData.sqOcorrenciasSelecionadasSalve) {
                    sqlWherePasso2.push("""mv.id_ocorrencia in (${workerData.sqOcorrenciasSelecionadasSalve}) and mv.bo_salve = true""")
                }
                if (workerData.somenteUtilizadosAvaliacao =~ /S|U/) {
                    sqlWherePasso2.push("mv.cd_situacao_avaliacao = 'REGISTRO_UTILIZADO_AVALIACAO'")
                } else if (workerData.utilizadosAvaliacao =~ /S|U/) {
                    sqlWherePasso2.push("mv.cd_situacao_avaliacao = 'REGISTRO_UTILIZADO_AVALIACAO'")
                } else if (workerData.somenteUtilizadosAvaliacao == 'NU') {
                    sqlWherePasso2.push("mv.cd_situacao_avaliacao == 'REGISTRO_NAO_UTILIZADO_AVALIACAO'")
                } else if (workerData.utilizadosAvaliacao =~ /N|NU/) {
                    sqlWherePasso2.push("mv.cd_situacao_avaliacao = 'REGISTRO_NAO_UTILIZADO_AVALIACAO'")
                } else if (workerData.utilizadosAvaliacao == 'AAA') {
                    sqlWherePasso2.push("mv.cd_situacao_avaliacao = 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO'")
                }
                if (workerData.excluirRegistrosSensiveis == 'S') {
                    sqlWherePasso2.push("mv.in_sensivel <> 'S'")
                }
                if (workerData.excluirRegistrosEmCarencia == 'S') {
                    sqlWherePasso2.push("( mv.dt_carencia is null or mv.dt_carencia < now() )")
                }
                cmdSql = """with passo0 as (
                    select tmp.id
                   from (values ( ${workerData.ids.join('),(')} ) ) as tmp (id))
                   , passo1 as (
                    select ficha.sq_taxon
                         , ficha.sq_ciclo_avaliacao
                    from salve.ficha
                    inner join passo0 on passo0.id = ficha.sq_ficha
                     ${sqlWherePasso1 ? "\nwhere " + sqlWherePasso1.join(' and ') : ''}
                ), taxons as (
                    select distinct ficha.sq_taxon
                    from salve.ficha
                    inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                    left join passo1 on passo1.sq_taxon = ficha.sq_taxon
                    where passo1.sq_taxon is not null or ( taxon.sq_taxon_pai in ( select passo1.sq_taxon from passo1 )
                      and ficha.sq_ciclo_avaliacao in ( select passo1.sq_ciclo_avaliacao from passo1) )
                )
                select mv.* from salve.mv_registros mv
                  inner join taxons on taxons.sq_taxon =  mv.sq_taxon
                  ${sqlWherePasso2 ? "\nwhere " + sqlWherePasso2.join(' and ') : ''}
                  """
 /** /
 // zzz-1
 // Ocorrências do mv_registros
 println ' '
 println 'OCORRENCIAS VISAO MATERIALIZADA MV_REGISTROS'
 println cmdSql
 /**/

                //List ocorrencias = []
                ocorrencias = sqlService.execSql(cmdSql)
                //println ' '
                //println '   - consulta a visao materializada finalizada. Total de ' + ocorrencias.size() + ' ocorrencias SALVE e PORTALBIO '
                d(' - consulta a visao materializada finalizada. Total de ' + ocorrencias.size() + ' ocorrencias SALVE e PORTALBIO');
            }
            else {


                if (workerData.sqCicloAvaliacao) {
                    sqlWherePasso1.push('ficha.sq_ciclo_avaliacao = ' + workerData.sqCicloAvaliacao.toString())
                    sqlWherePasso2.push('ficha.sq_ciclo_avaliacao = ' + workerData.sqCicloAvaliacao.toString())
                }
                if (workerData.sqOcorrenciasSelecionadasSalve) {
                    sqlWherePasso2.push("""fo.sq_ficha_ocorrencia in (${workerData.sqOcorrenciasSelecionadasSalve})""")
                }

                if (workerData.somenteUtilizadosAvaliacao =~ /S|U/) {
                    sqlWherePasso2.push("situacao_avaliacao.cd_sistema = 'REGISTRO_UTILIZADO_AVALIACAO'")
                } else if (workerData.utilizadosAvaliacao =~ /S|U/) {
                    sqlWherePasso2.push("situacao_avaliacao.cd_sistema = 'REGISTRO_UTILIZADO_AVALIACAO'")
                } else if (workerData.somenteUtilizadosAvaliacao == 'NU') {
                    sqlWherePasso2.push("situacao_avaliacao.cd_sistema = 'REGISTRO_NAO_UTILIZADO_AVALIACAO'")
                } else if (workerData.utilizadosAvaliacao =~ /N|NU/) {
                    sqlWherePasso2.push("situacao_avaliacao.cd_sistema = 'REGISTRO_NAO_UTILIZADO_AVALIACAO'")
                } else if (workerData.utilizadosAvaliacao == 'AAA') {
                    sqlWherePasso2.push("situacao_avaliacao.cd_sistema = 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO'")
                }

                if (workerData.excluirRegistrosSensiveis == 'S') {
                    sqlWherePasso2.push("(fo.in_sensivel <> 'S' or in_sensivel is null )")
                }
                if (workerData.excluirRegistrosEmCarencia == 'S') {
                    sqlWherePasso2.push("(salve.calc_carencia(fo.dt_inclusao::date, prazo_carencia.cd_sistema) is null or salve.calc_carencia(fo.dt_inclusao::date, prazo_carencia.cd_sistema) < now() )")
                }
                sqlWherePasso2.push("fo.sq_contexto = ${contextoOcorrencia.id.toString()}")
                sqlWherePasso2.push('fo.ge_ocorrencia is not null')


                cmdSql = """with passo0 as (
                    select tmp.id
                   from (values ( ${workerData.ids.join('),(')} ) ) as tmp (id))
                   , passo1 as (
                    select ficha.sq_taxon
                         , ficha.sq_ciclo_avaliacao
                    from salve.ficha
                    inner join passo0 on passo0.id = ficha.sq_ficha
                     ${sqlWherePasso1 ? "\nwhere " + sqlWherePasso1.join(' and ') : ''}
                ), taxons as (
                    select distinct ficha.sq_taxon
                    from salve.ficha
                    inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                    left join passo1 on passo1.sq_taxon = ficha.sq_taxon

                    where passo1.sq_taxon is not null or ( taxon.sq_taxon_pai in ( select passo1.sq_taxon from passo1 )
                      and ficha.sq_ciclo_avaliacao in ( select passo1.sq_ciclo_avaliacao from passo1) )
                ), registros as (
                    select fo.sq_ficha_ocorrencia
                     ,situacao_avaliacao.ds_dados_apoio    as ds_situacao_avaliacao
                     ,situacao_avaliacao.cd_sistema        as cd_situacao_avaliacao
                     ,fo.tx_nao_utilizado_avaliacao        as tx_justificativa_nao_usado_avaliacao
                     ,motivo.ds_dados_apoio                as ds_motivo_nao_utilizado
                    from salve.ficha_ocorrencia fo
                         inner join salve.ficha on ficha.sq_ficha = fo.sq_ficha
                         inner join taxons on taxons.sq_taxon = ficha.sq_taxon
                         inner join salve.dados_apoio as situacao_avaliacao on situacao_avaliacao.sq_dados_apoio = fo.sq_situacao_avaliacao
                         left  join salve.dados_apoio as motivo on motivo.sq_dados_apoio = fo.sq_motivo_nao_utilizado_avaliacao
                         left outer join salve.dados_apoio as prazo_carencia on prazo_carencia.sq_dados_apoio = fo.sq_prazo_carencia
                         ${sqlWherePasso2 ? "\nwhere " + sqlWherePasso2.join(' and ') : ''}
                )
                select ficha.nm_cientifico
                   , ficha.sq_categoria_iucn
                   , ficha.sq_categoria_final
                -- arvore taxonomica
                   , taxon.no_autor as no_autor_taxon
                   , taxon.json_trilha -> 'reino' ->> 'no_taxon' as no_reino
                   , taxon.json_trilha -> 'filo' ->> 'no_taxon' as no_filo
                   , taxon.json_trilha -> 'classe' ->> 'no_taxon' as no_classe
                   , taxon.json_trilha -> 'ordem' ->> 'no_taxon' as no_ordem
                   , taxon.json_trilha -> 'familia' ->> 'no_taxon' as no_familia
                   , taxon.json_trilha -> 'genero' ->> 'no_taxon' as no_genero
                   , categoria_anterior.ds_categoria_anterior
                   , case
                         when ficha.sq_categoria_iucn is not null then concat(cat_avaliada.ds_dados_apoio, ' (',
                                                                              cat_avaliada.cd_dados_apoio, ')')
                         else '' end as ds_categoria_avaliada
                   , case
                         when ficha.sq_categoria_final is not null then concat(cat_validada.ds_dados_apoio, ' (',
                                                                               cat_validada.cd_dados_apoio, ')')
                         else '' end as ds_categoria_validada
                   , pans.ds_pan
                -------------------------------------
                   ,st_x(fo.ge_ocorrencia) AS nu_longitude
                   ,st_y(fo.ge_ocorrencia) AS nu_latitude
                   ,fo.nu_altitude
                   ,salve.snd( ficha.st_endemica_brasil) as de_endemica_brasil
                   ,salve.snd( fo.in_sensivel ) as ds_sensivel
                   ,fo.st_adicionado_apos_avaliacao

                    ,registros.ds_situacao_avaliacao
                    ,registros.cd_situacao_avaliacao
                    ,registros.tx_justificativa_nao_usado_avaliacao
                    ,registros.ds_motivo_nao_utilizado

                   ,salve.calc_carencia(fo.dt_inclusao::date, prazo_carencia.cd_sistema) as dt_carencia
                   ,prazo_carencia.ds_dados_apoio as ds_prazo_carencia
                   ,datum.ds_dados_apoio as no_datum
                   ,formato_original.ds_dados_apoio as no_formato_original
                   ,precisao_coord.ds_dados_apoio as no_precisao
                   ,referencia_aprox.ds_dados_apoio as no_referencia_aproximacao
                   ,metodo_aprox.ds_dados_apoio as no_metodologia_aproximacao
                   ,fo.tx_metodo_aproximacao as tx_metodologia_aproximacao
                   ,taxon_citado.json_trilha -> 'especie'->>'no_taxon'::text as no_taxon_citado
                   ,salve.snd(fo.in_presenca_atual::text) AS ds_presenca_atual
                   ,tipo_registro.ds_dados_apoio as no_tipo_registro
                   ,fo.nu_dia_registro
                   ,fo.nu_mes_registro
                   ,fo.nu_ano_registro
                   ,fo.hr_registro
                   ,fo.nu_dia_registro_fim
                   ,fo.nu_mes_registro_fim
                   ,fo.nu_ano_registro_fim
                   ,salve.snd(fo.in_data_desconhecida::text) AS ds_data_desconhecida
                   ,fo.no_localidade
                   ,salve.remover_html_tags(fo.tx_local) as ds_caracteristica_localidade
                   ,uc_federal.no_uc_federal
                   ,uc_estadual.no_uc_estadual
                   ,initcap(rppn.sg_rppn::text) AS no_rppn
                   ,pais.no_pais
                   ,estado.no_estado
                   ,municipio.no_municipio
                   ,bioma.no_bioma
                   ,btrim(regexp_replace(habitat.ds_dados_apoio, '[0-9.]'::text, ''::text, 'g'::text)) AS no_ambiente
                   ,habitat.ds_dados_apoio AS no_habitat
                   ,fo.vl_elevacao_min
                   ,fo.vl_elevacao_max
                   ,fo.vl_profundidade_min
                   ,fo.vl_profundidade_max
                   ,fo.de_identificador
                   ,fo.dt_identificacao
                   ,qualificador.ds_dados_apoio AS no_identificacao_incerta
                   ,fo.tx_tombamento AS ds_tombamento
                   ,fo.tx_instituicao AS ds_instituicao_tombamento
                   ,compilador.no_pessoa AS no_compilador
                   ,fo.dt_compilacao
                   ,revisor.no_pessoa AS no_revisor
                   ,fo.dt_revisao
                   ,validador.no_pessoa AS no_validador
                   ,fo.dt_validacao
                   ,fo.in_utilizado_avaliacao
                   ,fo.in_sensivel
                   ,'salve' as no_base_dados
                   , CASE WHEN fo.id_origem IS NULL THEN concat('SALVE:', fo.sq_ficha_ocorrencia::text) ELSE fo.id_origem::text END AS id_origem
                   ,'' as nu_autorizacao
                   ,salve.remover_html_tags( fo.tx_observacao) as tx_observacao
                   ,refbibs.json_ref_bibs as tx_ref_bib
                   ,null as no_autor_registro
                   ,null as no_projeto

                --------------------------------------
                from registros
                inner join salve.ficha_ocorrencia fo on fo.sq_ficha_ocorrencia = registros.sq_ficha_ocorrencia
                inner join salve.ficha on ficha.sq_ficha = fo.sq_ficha
                inner join salve.ciclo_avaliacao as ciclo on ciclo.sq_ciclo_avaliacao = ficha.sq_ciclo_avaliacao
                inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                left join salve.dados_apoio as cat_avaliada on cat_avaliada.sq_dados_apoio = ficha.sq_categoria_iucn
                left join salve.dados_apoio as cat_validada on cat_validada.sq_dados_apoio = ficha.sq_categoria_final
                ------------------
                left outer join salve.dados_apoio as tipo_registro ON tipo_registro.sq_dados_apoio = fo.sq_tipo_registro
                left outer join salve.dados_apoio as prazo_carencia on prazo_carencia.sq_dados_apoio = fo.sq_prazo_carencia
                left outer join salve.dados_apoio as datum on datum.sq_dados_apoio = fo.sq_datum
                left outer join salve.dados_apoio as formato_original on formato_original.sq_dados_apoio = fo.sq_formato_coord_original
                left outer join salve.dados_apoio as precisao_coord on precisao_coord.sq_dados_apoio =fo.sq_precisao_coordenada
                left outer join salve.dados_apoio as referencia_aprox on referencia_aprox.sq_dados_apoio =fo.sq_ref_aproximacao
                left outer join salve.dados_apoio as metodo_aprox on metodo_aprox.sq_dados_apoio =fo.sq_metodo_aproximacao
                left outer join taxonomia.taxon as taxon_citado on taxon_citado.sq_taxon = fo.sq_taxon_citado
                left join salve.ficha_ocorrencia_registro freg on freg.sq_ficha_ocorrencia = fo.sq_ficha_ocorrencia
                -- categoria anterior
                left join lateral (
                    select h.sq_taxon, concat(cat_hist.ds_dados_apoio, ' (', cat_hist.cd_dados_apoio, ')') as ds_categoria_anterior
                    from salve.taxon_historico_avaliacao h
                             inner join salve.dados_apoio as cat_hist on cat_hist.sq_dados_apoio = h.sq_categoria_iucn
                    where h.sq_taxon = ficha.sq_taxon
                      and h.sq_tipo_avaliacao = ${tipoAvaliacaoNacional.id.toString()}
                      and h.nu_ano_avaliacao <= ciclo.nu_ano + 4
                    order by h.nu_ano_avaliacao
                    offset 0 limit 1) as categoria_anterior on true
                -- pans
                left join lateral (
                    SELECT acoes.sq_ficha,
                           array_to_string(
                                   array_agg(concat(plano_acao.tx_plano_acao, ' (', apoio_acao_situacao.ds_dados_apoio, ')')),', ') as ds_pan
                    FROM salve.ficha_acao_conservacao as acoes
                    inner join salve.plano_acao ON plano_acao.sq_plano_acao = acoes.sq_plano_acao
                    inner join salve.dados_apoio AS apoio_acao_situacao ON apoio_acao_situacao.sq_dados_apoio = acoes.sq_situacao_acao_conservacao
                    where acoes.sq_ficha = ficha.sq_ficha
                    group by acoes.sq_ficha
                ) as pans on true
                --------------
                left join salve.registro_rppn regrppn on regrppn.sq_registro = freg.sq_registro
                left join corporativo.vw_rppn rppn on rppn.sq_pessoa = regrppn.sq_rppn
                left join salve.registro_pais regpais on regpais.sq_registro = freg.sq_registro
                left join salve.pais pais on pais.sq_pais = regpais.sq_pais
                left join salve.registro_estado reguf on reguf.sq_registro = freg.sq_registro
                left join corporativo.vw_estado estado on estado.sq_estado = reguf.sq_estado
                left join salve.registro_municipio regmun on regmun.sq_registro = freg.sq_registro
                left join corporativo.vw_municipio municipio on municipio.sq_municipio = regmun.sq_municipio
                left join salve.dados_apoio habitat on habitat.sq_dados_apoio = fo.sq_habitat
                left join salve.dados_apoio ambiente on ambiente.sq_dados_apoio = habitat.sq_dados_apoio_pai
                left join salve.dados_apoio qualificador on qualificador.sq_dados_apoio = fo.sq_qualificador_val_taxon
                left join salve.vw_pessoa compilador on compilador.sq_pessoa = fo.sq_pessoa_compilador
                left join salve.vw_pessoa revisor on revisor.sq_pessoa = fo.sq_pessoa_revisor
                left join salve.vw_pessoa validador on validador.sq_pessoa = fo.sq_pessoa_validador
                LEFT JOIN salve.registro_bioma regbio ON regbio.sq_registro = freg.sq_registro
                LEFT JOIN corporativo.vw_bioma bioma ON bioma.sq_bioma = regbio.sq_bioma
                -- referencia bibliografica do registro
                left join lateral (
                    select json_object_agg( ficha_ref_bib.sq_ficha_ref_bib, json_build_object (
                            'de_ref_bib', salve.entity2char( publicacao.de_ref_bibliografica )
                        ,'no_autores_publicacao', REPLACE( publicacao.no_autor,E'
','; ' )
                        ,'nu_ano_publicacao', publicacao.nu_ano_publicacao
                        ,'no_autor', ficha_ref_bib.no_autor
                        ,'nu_ano', ficha_ref_bib.nu_ano
                        ) )::text AS json_ref_bibs
                    from salve.ficha_ref_bib
                             left outer join taxonomia.publicacao on publicacao.sq_publicacao = ficha_ref_bib.sq_publicacao
                    where no_tabela = 'ficha_ocorrencia'
                      and no_coluna = 'sq_ficha_ocorrencia'
                      and sq_registro = fo.sq_ficha_ocorrencia
                    ) as refbibs on true
                -- ucs federais
                left join lateral ( select array_to_string(
                                                   array_agg(coalesce(ucf.sg_unidade_org, ucf.no_pessoa::character varying)),
                                                   ', '::text) as no_uc_federal
                                    from salve.registro_uc_federal x
                                             join salve.vw_unidade_org ucf on ucf.sq_pessoa = x.sq_uc_federal
                                    where x.sq_registro = freg.sq_registro) uc_federal on true
                left join lateral ( select array_to_string(array_agg(uce.nome_uc1), ', '::text) as no_uc_estadual
                                    from salve.registro_uc_estadual x
                                             join geo.vw_uc_estadual uce on uce.gid = x.sq_uc_estadual
                                    where x.sq_registro = freg.sq_registro) uc_estadual on true
                """
                workerService.setTitle(worker, 'Lendo as ocorrências (SALVE)')
                d(' - inciando consulta ocorrencias SALVE');
/** /

 // zzz-2
 // Ocorrências do SALVE
 println ' '
 println 'OCORRENCIAS SALVE'
 println cmdSql
  /**/

                //List ocorrencias = []
                ocorrencias = sqlService.execSql(cmdSql);

                d(' - consulta SALVE finalizada. Total de ' + ocorrencias.size() + ' ocorrencias');


                // ler somente as ocorrências do portalbio que foram selecionadas no gride
                d(' - inciando consulta ocorrencias Portalbio');


                List idsContidosNoShp = []
                /* Exemplo como filtrar registro utilizando shp file
            if( shape ){
                idsContidosNoShp = sqlService.execSql("""select fo.sq_ficha_ocorrencia_portalbio
                from salve.ficha_ocorrencia_portalbio fo
                inner join salve.ficha on ficha.sq_ficha = fo.sq_ficha
                where ficha.sq_ciclo_avaliacao in ( 1, 2 )
                 and fo.sq_ficha_ocorrencia_portalbio in (
                    select x.sq_ficha_ocorrencia_portalbio x
                    from salve.ficha_ocorrencia_portalbio x
                    inner join public.bacia_purus purus on st_intersects( x.ge_ocorrencia, purus.geom)
                    where x.ge_ocorrencia is not null
                ) """)
            }*/


                /************************************************************************
                 * LER AS OCORRENCIAS DO PORTALBIO
                 ***********************************************************************/
                sqlWherePasso2 = []

                if (workerData.sqCicloAvaliacao) {
                    sqlWherePasso2.push('ficha.sq_ciclo_avaliacao = ' + workerData.sqCicloAvaliacao.toString())
                }

                // datum formato precisao referencia
                if (workerData.sqOcorrenciasSelecionadasPortalbio) {
                    sqlWherePasso2.push(" ( fo.sq_ficha_ocorrencia_portalbio in (${workerData.sqOcorrenciasSelecionadasPortalbio}) )")
                }
                if (workerData.somenteUtilizadosAvaliacao == 'U') {
                    sqlWherePasso2.push("( fo.in_utilizado_avaliacao = 'S' )")
                } else if (workerData.somenteUtilizadosAvaliacao == 'NU') {
                    sqlWherePasso2.push("( fo.in_utilizado_avaliacao = 'N' )")
                } else if (workerData.utilizadosAvaliacao == 'S') {
                    sqlWherePasso2.push("( fo.in_utilizado_avaliacao = 'S' )")
                } else if (workerData.utilizadosAvaliacao == 'N') {
                    sqlWherePasso2.push("( fo.in_utilizado_avaliacao = 'N' )")
                } else if (workerData.utilizadosAvaliacao == 'AAA') {
                    sqlWherePasso2.push("( fo.st_adicionado_apos_avaliacao = true )")
                }
                if (workerData.excluirRegistrosEmCarencia == 'S') {
                    sqlWherePasso2.push(" ( fo.dt_carencia is null or fo.dt_carencia < now() )")
                }

                sqlWherePasso2.push('fo.ge_ocorrencia is not null')
                sqlWherePasso2.push("situacao_avaliacao.cd_sistema <> 'REGISTRO_EXCLUIDO'")

                cmdSql = """with passo0 as (
                            select tmp.id
                            from (values (${workerData.ids.join('),(')})) as tmp (id))
                           , passo1 as (
                            select ficha.sq_taxon
                                 , ficha.sq_ciclo_avaliacao
                            from salve.ficha
                            inner join passo0 on passo0.id = ficha.sq_ficha
                            ${sqlWherePasso1 ? "\nwhere " + sqlWherePasso1.join(' and ') : ''}
                        )
                           , taxons as (
                            select distinct ficha.sq_taxon
                            from salve.ficha
                                     inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                                     left join passo1 on passo1.sq_taxon = ficha.sq_taxon
                            where passo1.sq_taxon is not null
                               or (taxon.sq_taxon_pai in (select passo1.sq_taxon from passo1)
                                and ficha.sq_ciclo_avaliacao in (select passo1.sq_ciclo_avaliacao from passo1))
                        )
                           , registros as (
                            select fo.sq_ficha_ocorrencia_portalbio
                             ,situacao_avaliacao.ds_dados_apoio    as ds_situacao_avaliacao
                             ,situacao_avaliacao.cd_sistema        as cd_situacao_avaliacao
                             ,fo.tx_nao_utilizado_avaliacao        as tx_justificativa_nao_usado_avaliacao
                             ,motivo.ds_dados_apoio                as ds_motivo_nao_utilizado
                            from salve.ficha_ocorrencia_portalbio fo
                                 inner join salve.ficha on ficha.sq_ficha = fo.sq_ficha
                                 inner join taxons on taxons.sq_taxon = ficha.sq_taxon
                                 inner join salve.dados_apoio as situacao_avaliacao on situacao_avaliacao.sq_dados_apoio = fo.sq_situacao_avaliacao
                                 left  join salve.dados_apoio as motivo on motivo.sq_dados_apoio = fo.sq_motivo_nao_utilizado_avaliacao
                                 ${sqlWherePasso2 ? "\nwhere  " + sqlWherePasso2.join(' and ') : ''}
                        )
                        select ficha.nm_cientifico
                             , ficha.sq_categoria_iucn
                             , ficha.sq_categoria_final

                        -- arvore taxonomica
                             , taxon.no_autor                                                                                        as no_autor_taxon
                             , taxon.json_trilha -> 'reino' ->> 'no_taxon'                                                           as no_reino
                             , taxon.json_trilha -> 'filo' ->> 'no_taxon'                                                            as no_filo
                             , taxon.json_trilha -> 'classe' ->> 'no_taxon'                                                          as no_classe
                             , taxon.json_trilha -> 'ordem' ->> 'no_taxon'                                                           as no_ordem
                             , taxon.json_trilha -> 'familia' ->> 'no_taxon'                                                         as no_familia
                             , taxon.json_trilha -> 'genero' ->> 'no_taxon'                                                          as no_genero

                             , categoria_anterior.ds_categoria_anterior
                             , case
                                   when ficha.sq_categoria_iucn is not null then concat(cat_avaliada.ds_dados_apoio, ' (',
                                                                                        cat_avaliada.cd_dados_apoio, ')')
                                   else '' end                                                                                       as ds_categoria_avaliada
                             , case
                                   when ficha.sq_categoria_final is not null then concat(cat_validada.ds_dados_apoio, ' (',
                                                                                         cat_validada.cd_dados_apoio, ')')
                                   else '' end                                                                                       as ds_categoria_validada
                             , pans.ds_pan
                        -------------------------------------
                             , st_x(fo.ge_ocorrencia)                                                                                AS nu_longitude
                             , st_y(fo.ge_ocorrencia)                                                                                AS nu_latitude
                             , null                                                                                                  as nu_altitude
                             , salve.snd(ficha.st_endemica_brasil)                                                                   as de_endemica_brasil
                             , null                                                                                                  as ds_sensivel

                             , fo.tx_nao_utilizado_avaliacao       as tx_justificativa_nao_usado_avaliacao
                             , registros.ds_situacao_avaliacao
                             , registros.cd_situacao_avaliacao
                             , registros.tx_justificativa_nao_usado_avaliacao
                             , registros.ds_motivo_nao_utilizado

                             , fo.dt_carencia
                             , null                                                                                                  as ds_prazo_carencia
                             , null                                                                                                  as no_datum
                             , null                                                                                                  as no_formato_original
                             , CASE
                                   WHEN left(fo.tx_ocorrencia, 1) = '{' THEN fo.tx_ocorrencia::json ->> 'precisaoCoord'
                                   ELSE '' END                                                                                       as no_precisao
                             , null                                                                                                  as no_referencia_aproximacao
                             , null                                                                                                  as no_metodologia_aproximacao
                             , null                                                                                                  as tx_metodologia_aproximacao
                             , null                                                                                                  as no_taxon_citado
                             , salve.snd(fo.in_presenca_atual)                                                                       ds_presenca_atual
                             , null                                                                                                  no_tipo_registro
                             , to_char(fo.dt_ocorrencia, 'DD')                                                                       as nu_dia_registro
                             , to_char(fo.dt_ocorrencia, 'MM')                                                                       as nu_mes_registro
                             , to_char(fo.dt_ocorrencia, 'yyyy')                                                                     as nu_ano_registro
                             , null                                                                                                  as hr_registro
                             , null                                                                                                  as nu_dia_registro_fim
                             , null                                                                                                  as nu_mes_registro_fim
                             , null                                                                                                  as nu_ano_registro_fim
                             , salve.snd(fo.in_data_desconhecida::text)                                                              AS ds_data_desconhecida
                             , fo.no_local                                                                                           as no_localidade
                             , null                                                                                                  as ds_caracteristica_localidade
                             , uc_federal.no_uc_federal
                             , uc_estadual.no_uc_estadual
                             , initcap(rppn.sg_rppn::text)                                                                           AS no_rppn
                             , pais.no_pais
                             , estado.no_estado
                             , municipio.no_municipio
                             , bioma.no_bioma
                             , null                                                                                                  as no_ambiente
                             , null                                                                                                  as no_habitat
                             , null                                                                                                  as vl_elevacao_min
                             , null                                                                                                  as vl_elevacao_max
                             , null                                                                                                  as vl_profundidade_min
                             , null                                                                                                  as vl_profundidade_max
                             , null                                                                                                  as de_identificador
                             , null                                                                                                  as dt_identificacao
                             , null                                                                                                  as no_identificacao_incerta
                             , null                                                                                                  as ds_tombamento
                             , null                                                                                                  as ds_instituicao_tombamento
                             , null                                                                                                  as no_compilador
                             , null                                                                                                  as dt_compilacao
                             , revisor.no_pessoa                                                                                     AS no_revisor
                             , fo.dt_revisao
                             , validador.no_pessoa                                                                                   AS no_validador
                             , fo.dt_validacao
                             , fo.in_utilizado_avaliacao
                             , null                                                                                                  as in_sensivel
                             , replace(CASE
                                           WHEN (fo.no_autor IS NULL) THEN
                                               CASE
                                                   WHEN ("left"(fo.tx_ocorrencia, 1) = '{'::text) THEN
                                                       CASE
                                                           WHEN (((fo.tx_ocorrencia)::json ->> 'autor'::text) IS NULL)
                                                               THEN ((fo.tx_ocorrencia)::json ->> 'collector'::text)
                                                           ELSE ((fo.tx_ocorrencia)::json ->> 'autor'::text) END
                                                   ELSE ''::text END
                                           ELSE fo.no_autor END, E'
', '; ') AS no_autor_registro
, fo.tx_ocorrencia::jsonb->>'projeto'::text as no_projeto
                             , fo.no_instituicao
                             , salve.calc_base_dados_portalbio(fo.tx_ocorrencia::text,
                                                               fo.de_uuid::text)                                                     as no_base_dados
                             , CASE
                                   WHEN (fo.id_origem IS NULL) THEN CASE
                                                                        WHEN ("left"(fo.tx_ocorrencia, 1) = '{'::text)
                                                                            THEN ((fo.tx_ocorrencia)::json ->> 'uuid'::text)
                                                                        ELSE fo.de_uuid END
                                   ELSE (fo.id_origem)::text END                                                                     AS id_origem
                             , CASE
                                   WHEN ("left"(fo.tx_ocorrencia, 1) = '{'::text) THEN ((fo.tx_ocorrencia)::json ->> 'recordNumber'::text)
                                   ELSE '' END                                                                                       as nu_autorizacao
                             , null                                                                                                  as tx_observacao
                             , refbibs.json_ref_bibs                                                                                 as tx_ref_bib


                        --------------------------------------
                        from registros
                                 inner join salve.ficha_ocorrencia_portalbio fo on fo.sq_ficha_ocorrencia_portalbio = registros.sq_ficha_ocorrencia_portalbio
                                 inner join salve.ficha on ficha.sq_ficha = fo.sq_ficha
                                 inner join salve.ciclo_avaliacao as ciclo on ciclo.sq_ciclo_avaliacao = ficha.sq_ciclo_avaliacao
                                 inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                                 left join salve.dados_apoio as cat_avaliada on cat_avaliada.sq_dados_apoio = ficha.sq_categoria_iucn
                                 left join salve.dados_apoio as cat_validada on cat_validada.sq_dados_apoio = ficha.sq_categoria_final
                        ------------------
                                 left join salve.ficha_ocorrencia_portalbio_reg as freg
                                           on freg.sq_ficha_ocorrencia_portalbio = fo.sq_ficha_ocorrencia_portalbio
                        -- categoria anterior
                                 left join lateral (
                            select h.sq_taxon, concat(cat_hist.ds_dados_apoio, ' (', cat_hist.cd_dados_apoio, ')') as ds_categoria_anterior
                            from salve.taxon_historico_avaliacao h
                                     inner join salve.dados_apoio as cat_hist on cat_hist.sq_dados_apoio = h.sq_categoria_iucn
                            where h.sq_taxon = ficha.sq_taxon
                              and h.sq_tipo_avaliacao = 311
                              and h.nu_ano_avaliacao <= ciclo.nu_ano + 4
                            order by h.nu_ano_avaliacao
                            offset 0 limit 1) as categoria_anterior on true

                        -- pans
                                 left join lateral (
                            SELECT acoes.sq_ficha,
                                   array_to_string(
                                           array_agg(concat(plano_acao.tx_plano_acao, ' (', apoio_acao_situacao.ds_dados_apoio, ')')),
                                           ', ') as ds_pan
                            FROM salve.ficha_acao_conservacao as acoes
                                     inner join salve.plano_acao ON plano_acao.sq_plano_acao = acoes.sq_plano_acao
                                     inner join salve.dados_apoio AS apoio_acao_situacao
                                                ON apoio_acao_situacao.sq_dados_apoio = acoes.sq_situacao_acao_conservacao
                            where acoes.sq_ficha = ficha.sq_ficha
                            group by acoes.sq_ficha
                            ) as pans on true
                        --------------
                                 left join salve.registro_rppn regrppn on regrppn.sq_registro = freg.sq_registro
                                 left join corporativo.vw_rppn rppn on rppn.sq_pessoa = regrppn.sq_rppn
                                 left join salve.registro_pais regpais on regpais.sq_registro = freg.sq_registro
                                 left join salve.pais pais on pais.sq_pais = regpais.sq_pais
                                 left join salve.registro_estado reguf on reguf.sq_registro = freg.sq_registro
                                 left join corporativo.vw_estado estado on estado.sq_estado = reguf.sq_estado
                                 left join salve.registro_municipio regmun on regmun.sq_registro = freg.sq_registro
                                 left join corporativo.vw_municipio municipio on municipio.sq_municipio = regmun.sq_municipio
                                 left join salve.vw_pessoa revisor on revisor.sq_pessoa = fo.sq_pessoa_revisor
                                 left join salve.vw_pessoa validador on validador.sq_pessoa = fo.sq_pessoa_validador
                                 LEFT JOIN salve.registro_bioma regbio ON regbio.sq_registro = freg.sq_registro
                                 LEFT JOIN corporativo.vw_bioma bioma ON bioma.sq_bioma = regbio.sq_bioma

                        -- referencia bibliografica do registro
                                 left join lateral (
                            select json_object_agg(ficha_ref_bib.sq_ficha_ref_bib, json_build_object(
                                    'de_ref_bib', salve.entity2char(publicacao.de_ref_bibliografica)
                                , 'no_autores_publicacao', REPLACE(publicacao.no_autor, E'
','; ')
                                , 'nu_ano_publicacao', publicacao.nu_ano_publicacao
                                , 'no_autor', ficha_ref_bib.no_autor
                                , 'nu_ano', ficha_ref_bib.nu_ano
                                ))::text AS json_ref_bibs
                            from salve.ficha_ref_bib
                                     left outer join taxonomia.publicacao on publicacao.sq_publicacao = ficha_ref_bib.sq_publicacao
                            where no_tabela = 'ficha_ocorrencia_portalbio'
                              and no_coluna = 'sq_ficha_ocorrencia_portalbio'
                              and sq_registro = fo.sq_ficha_ocorrencia_portalbio
                            ) as refbibs on true
                        -- ucs federais
                                 left join lateral ( select array_to_string(
                                                                    array_agg(coalesce(ucf.sg_unidade_org, ucf.no_pessoa::character varying)), ', ') as no_uc_federal
                                                     from salve.registro_uc_federal x
                                                              join salve.vw_unidade_org ucf on ucf.sq_pessoa = x.sq_uc_federal
                                                     where x.sq_registro = freg.sq_registro) uc_federal on true
                                 left join lateral ( select array_to_string(array_agg(uce.nome_uc1), ', '::text) as no_uc_estadual
                                                     from salve.registro_uc_estadual x
                                                              join geo.vw_uc_estadual uce on uce.gid = x.sq_uc_estadual
                                                     where x.sq_registro = freg.sq_registro) uc_estadual on true
                        """

/** /
 println ' '
 println ' '
 println ' '
 println 'OCORRENCIAS PORTALBIO'
 println cmdSql
 /**/


                // throw new Exception('testando')

                workerService.setTitle(worker, 'Lendo as ocorrências (PortalBio)')
                ocorrenciasPortalbio = sqlService.execSql(cmdSql)
                //ocorrenciasPortalbio = []
                d(' - consulta PORTALBIO finalizada. Total de ' + ocorrenciasPortalbio.size() + ' ocorrencias');
            }

            if (ocorrencias.size() + ocorrenciasPortalbio.size() == 0) {
                throw new Exception('Nenhuma ocorrencia encontrada.')
            }
            int totalRegistros = ocorrencias.size() + ocorrenciasPortalbio.size()
            if (totalRegistros > maxRegistrosPermitidos) {
                throw new Exception('O total de registros de ocorrências das fichas selecionadas é de ' + totalRegistros.toString() + '.<br>O limite máximo é de ' + maxRegistrosPermitidos + ' registros por vez.')
            }
            workerService.setCallback(worker, "window.open(app.url + 'main/download?fileName=" + fileName + "&delete=1','_top');")
            workerService.setTitle(worker, 'Iniciando a exportação de ' + totalRegistros.toString() + ' registros...')
            planilha.setWorker(worker, workerService)
            workerService.start(worker)
            Date carencia = null
            JSONObject jsonObj

            // loop ocorrencias do SALVE
            d(' - iniciando loop pre-processamento das ocorrencias do SALVE')
            int percent = 0
            ocorrencias.eachWithIndex { it, index ->

                percent = Math.round( (index+1) / ocorrencias.size() * 100 )
                if( index % 1000 == 0 ) {
                    d(' - ' + (index + 1) + ' / ' + ocorrencias.size() + ' (' + percent + '% )')
                }
                Map rowData = [:]
                if( it.cd_situacao_avaliacao == 'REGISTRO_UTILIZADO_AVALIACAO'){
                    it.tx_justificativa_nao_usado_avaliacao = ''
                    it.ds_motivo_nao_utilizado=''
                }
                it.tx_justificativa_nao_usado_avaliacao = it.tx_justificativa_nao_usado_avaliacao.toString().replaceAll(/<br\/?>/,'\n')

                try {
                    it.nm_cientifico_taxon = it.nm_cientifico
                    rowData['no_reino'] = it.no_reino
                    rowData['no_filo'] = it.no_filo
                    rowData['no_classe'] = it.no_classe
                    rowData['no_ordem'] = it.no_ordem
                    rowData['no_familia'] = it.no_familia
                    rowData['no_genero'] = it.no_genero

                    rowData['no_cientifico'] = Util.ncItalico( it.nm_cientifico_taxon, true )
                    rowData['no_autor_taxon'] = (it.no_autor_taxon ?: '')
                    rowData['nu_latitude'] = (it?.nu_latitude ? it?.nu_latitude.toString().replaceAll(/\./, ',') : '')
                    rowData['nu_longitude'] = (it?.nu_longitude ? it?.nu_longitude.toString().replaceAll(/\./, ',') : '')

                    rowData['ds_categoria_anterior'] = (it.ds_categoria_anterior ?: '')
                    rowData['ds_categoria_avaliada'] = (it.ds_categoria_avaliada ?: '')
                    rowData['ds_categoria_validada'] = (it.ds_categoria_validada ?: '')

                    rowData['ds_pan']       = (it.ds_pan ? it.ds_pan.replaceAll('Plano de Ação Nacional','PAN') : '')

                    rowData['de_endemica_brasil'] = (it.de_endemica_brasil ?: '')
                    rowData['nu_altitude'] = (it.nu_altitude ?: '')
                    rowData['ds_sensivel'] = it.ds_sensivel ?: '-'

                    rowData['ds_situacao_avaliacao'] = it.ds_situacao_avaliacao ?: ''
                    rowData['ds_motivo_nao_utilizado'] = it.ds_motivo_nao_utilizado ?: ''
                    rowData['tx_justificativa_nao_usado_avaliacao'] = it.tx_justificativa_nao_usado_avaliacao ?: ''


                    rowData['de_prazo_carencia'] = (it?.dt_carencia ? it.dt_carencia.format('dd/MM/yyyy') : '')
                    rowData['no_datum'] = it?.no_datum ?: ''
                    rowData['no_formato_original'] = it?.no_formato_original ?: ''
                    rowData['no_precisao'] = it?.no_precisao ?: ''
                    rowData['no_referencia_aproximacao'] = it?.no_referencia_aproximacao ?: ''
                    rowData['no_metodologia_aproximacao'] = it?.no_metodologia_aproximacao ?: ''
                    rowData['tx_metodologia_aproximacao'] = it?.tx_metodologia_aproximacao ? cleanHtml(it.tx_metodologia_aproximacao.toString()) : ''
                    rowData['no_taxon_citado'] = it?.no_taxon_citado ?: ''
                    rowData['ds_presenca_atual'] = it?.ds_presenca_atual ?: ''
                    rowData['no_tipo_registro'] = it?.no_tipo_registro ?: ''
                    rowData['nu_dia_registro'] = (it?.nu_dia_registro ?: '')
                    rowData['nu_mes_registro'] = (it?.nu_mes_registro ?: '')
                    rowData['nu_ano_registro'] = (it?.nu_ano_registro ?: '')
                    rowData['hr_registro'] = (it?.hr_registro ?: '')
                    rowData['nu_dia_registro_fim'] = (it?.nu_dia_registro_fim ?: '')
                    rowData['nu_mes_registro_fim'] = (it?.nu_mes_registro_fim ?: '')
                    rowData['nu_ano_registro_fim'] = (it?.nu_ano_registro_fim ?: '')
                    rowData['ds_data_desconhecida'] = it?.ds_data_desconhecida ?: ''
                    rowData['no_localidade'] = it?.no_localidade ?: ''
                    rowData['ds_caracteristica_localidade'] = it?.ds_caracteristica_localidade ? cleanHtml(it?.ds_caracteristica_localidade) : ''
                    rowData['no_uc_federal'] = it?.no_uc_federal ?: ''
                    rowData['no_uc_estadual'] = it?.no_uc_estadual ?: ''
                    rowData['no_rppn'] = it?.no_rppn ?: ''
                    rowData['no_pais'] = it?.no_pais ?: ''
                    rowData['no_estado'] = it?.no_estado ?: ''
                    rowData['no_municipio'] = it?.no_municipio ?: ''
                    rowData['no_bioma'] = (it?.no_bioma ?:'')
                    rowData['no_ambiente'] = it?.no_ambiente ?: ''
                    rowData['no_habitat'] = it?.no_habitat ?: ''
                    rowData['vl_elevacao_min'] = (it?.vl_elevacao_min ?: '')
                    rowData['vl_elevacao_max'] = (it?.vl_elevacao_max ?: '')
                    rowData['vl_profundidade_min'] = (it?.vl_profundidade_min ?: '')
                    rowData['vl_profundidade_max'] = (it?.vl_profundidade_max ?: '')
                    rowData['de_identificador'] = (it?.de_identificador ?: '')
                    rowData['dt_identificacao'] = (it?.dt_identificacao ? it.dt_identificacao.format('dd/MM/yyyy') : '')
                    rowData['no_identificacao_incerta'] = it?.no_identificacao_incerta ?: ''
                    rowData['ds_tombamento'] = it?.ds_tombamento ?: ''
                    rowData['ds_instituicao_tombamento'] = it?.ds_instituicao_tombamento ?: ''
                    rowData['no_compilador'] = it?.no_compilador ?: ''
                    rowData['dt_compilacao'] = (it?.dt_compilacao ? it?.dt_compilacao.format('dd/MM/yyyy') : '')
                    rowData['no_revisor'] = it?.no_revisor ?: ''
                    rowData['dt_revisao'] = (it?.dt_revisao ? it?.dt_revisao.format('dd/MM/yyyy') : '')
                    rowData['no_validador'] = it?.no_validador ?: ''
                    rowData['dt_validacao'] = (it?.dt_validacao ? it?.dt_validacao.format('dd/MM/yyyy') : '')
                    rowData['no_base_dados'] = it.no_base_dados ?:''
                    rowData['id_origem'] = (it?.id_origem ?: '')
                    rowData['nu_autorizacao'] = (it?.nu_autorizacao ?: '')
                    rowData['no_autor_registro'] = '' // registros do salve não possui esse campo
                    rowData['no_projeto'] = // '' // registros do salve não possui esse campo
                    rowData['tx_observacao'] = (it?.tx_observacao ?: '')
                    rowData['tx_ref_bib'] = _parseRefBibPlanilha( it.tx_ref_bib );
                    listData.push(rowData)

                    // atualizar andamento do worker
                } catch (Exception e) {
                    println ' '
                    //println 'ExportDataService.ocorrenciasPlanilha - Erro na ficha ' + it?.sq_ficha
                    //println e.getMessage()
                    println e.printStackTrace()
                    println "-" * 100
                    println ' '
                }
            }
            d(' - iniciando loop pre-processamento das ocorrencias do PORTALBIO')

            // loop ocorrências do portalbio
            percent=0
            ocorrenciasPortalbio.eachWithIndex { it, index ->

                percent = Math.round( (index+1) / ocorrenciasPortalbio.size() * 100 )
                if( index % 1000 == 0 ) {
                    d(' - ' + (index + 1) + ' / ' + ocorrenciasPortalbio.size() + ' (' + percent + '% )')
                }
                if( it.no_base_dados ){
                    it.no_instituicao = it.no_base_dados
                }

                if( it.cd_situacao_avaliacao == 'REGISTRO_UTILIZADO_AVALIACAO'){
                    it.tx_justificativa_nao_usado_avaliacao = ''
                    it.ds_motivo_nao_utilizado=''
                }
                it.tx_justificativa_nao_usado_avaliacao = it.tx_justificativa_nao_usado_avaliacao.toString().replaceAll(/<br\/?>/,'\n')


                Map rowData = [:]
                carencia = it?.dt_carencia ? it.dt_carencia : null
                it.nm_cientifico_taxon = it.nm_cientifico
                rowData['no_reino'] = it.no_reino
                rowData['no_filo'] = it.no_filo
                rowData['no_classe'] = it.no_classe
                rowData['no_ordem'] = it.no_ordem
                rowData['no_familia'] = it.no_familia
                rowData['no_genero'] = it.no_genero
                rowData['no_cientifico'] = Util.ncItalico(it.nm_cientifico_taxon,true)
                rowData['no_autor_taxon'] = (it.no_autor_taxon ?: '')
                rowData['nu_latitude'] = (it?.nu_latitude ? it?.nu_latitude.toString().replaceAll(/\./, ',') : '')
                rowData['nu_longitude'] = (it?.nu_longitude ? it?.nu_longitude.toString().replaceAll(/\./, ',') : '')

                rowData['ds_categoria_anterior'] = (it.ds_categoria_anterior ?: '')
                rowData['ds_categoria_avaliada'] = (it.ds_categoria_avaliada ?: '')
                rowData['ds_categoria_validada'] = (it.ds_categoria_validada ?: '')

                rowData['ds_pan']       = (it.ds_pan ? it.ds_pan.replaceAll('Plano de Ação Nacional','PAN') : '')

                rowData['nu_altitude'] = (it.nu_altitude ?: '')
                rowData['ds_sensivel'] = it.ds_sensivel ?: '-'

                rowData['ds_situacao_avaliacao'] = it.ds_situacao_avaliacao ?: ''
                rowData['ds_motivo_nao_utilizado'] = it.ds_motivo_nao_utilizado ?: ''
                rowData['tx_justificativa_nao_usado_avaliacao'] = it.tx_justificativa_nao_usado_avaliacao ?: ''

                rowData['de_prazo_carencia'] = (it?.dt_carencia ? it.dt_carencia.format('dd/MM/yyyy') : '')
                rowData['no_datum'] = it?.no_datum ?: ''
                rowData['no_formato_original'] = it?.no_formato_original ?: ''
                rowData['no_precisao'] = it?.no_precisao ?: ''
                rowData['no_referencia_aproximacao'] = it?.no_referencia_aproximacao ?: ''
                rowData['no_metodologia_aproximacao'] = it?.no_metodologia_aproximacao ?: ''
                rowData['tx_metodologia_aproximacao'] = it?.tx_metodologia_aproximacao ? cleanHtml(it.tx_metodologia_aproximacao.toString()) : ''
                rowData['no_taxon_citado'] = it?.no_taxon_citado ?: ''
                rowData['ds_presenca_atual'] = it?.ds_presenca_atual ?: ''
                rowData['no_tipo_registro'] = it?.no_tipo_registro ?: ''
                rowData['nu_dia_registro'] = (it?.nu_dia_registro ?: '')
                rowData['nu_mes_registro'] = (it?.nu_mes_registro ?: '')
                rowData['nu_ano_registro'] = (it?.nu_ano_registro ?: '')
                rowData['hr_registro'] = (it?.hr_registro ?: '')
                rowData['nu_dia_registro_fim'] = (it?.nu_dia_registro_fim ?: '')
                rowData['nu_mes_registro_fim'] = (it?.nu_mes_registro_fim ?: '')
                rowData['nu_ano_registro_fim'] = (it?.nu_ano_registro_fim ?: '')
                rowData['ds_data_desconhecida'] = it?.ds_data_desconhecida ?: ''
                rowData['no_localidade'] = it?.no_localidade ?: ''
                rowData['ds_caracteristica_localidade'] = it?.ds_caracteristica_localidade ? cleanHtml(it?.ds_caracteristica_localidade) : ''
                rowData['no_uc_federal'] = it?.no_uc_federal ?: ''
                rowData['no_uc_estadual'] = it?.no_uc_estadual ?: ''
                rowData['no_rppn'] = it?.no_rppn ?: ''
                rowData['no_pais'] = it?.no_pais ?: ''
                rowData['no_estado'] = it?.no_estado ?: ''
                rowData['no_municipio'] = it?.no_municipio ?: ''
                rowData['no_bioma'] = (it?.no_bioma ?:'')
                rowData['no_ambiente'] = it?.no_ambiente ?: ''
                rowData['no_habitat'] = it?.no_habitat ?: ''
                rowData['vl_elevacao_min'] = (it?.vl_elevacao_min ?: '')
                rowData['vl_elevacao_max'] = (it?.vl_elevacao_max ?: '')
                rowData['vl_profundidade_min'] = (it?.vl_profundidade_min ?: '')
                rowData['vl_profundidade_max'] = (it?.vl_profundidade_max ?: '')
                rowData['de_identificador'] = (it?.de_identificador ?: '')
                rowData['dt_identificacao'] = (it?.dt_identificacao ? it.dt_identificacao.format('dd/MM/yyyy') : '')
                rowData['no_identificacao_incerta'] = it?.no_identificacao_incerta ?: ''
                rowData['ds_tombamento'] = it?.ds_tombamento ?: ''
                rowData['ds_instituicao_tombamento'] = it?.ds_instituicao_tombamento ?: ''
                rowData['no_compilador'] = it?.no_compilador ?: ''
                rowData['dt_compilacao'] = (it?.dt_compilacao ? it?.dt_compilacao.format('dd/MM/yyyy') : '')
                rowData['no_revisor'] = it?.no_revisor ?: ''
                rowData['dt_revisao'] = (it?.dt_revisao ? it?.dt_revisao.format('dd/MM/yyyy') : '')
                rowData['no_validador'] = it?.no_validador ?: ''
                rowData['dt_validacao'] = (it?.dt_validacao ? it?.dt_validacao.format('dd/MM/yyyy') : '')
                rowData['no_base_dados'] = it.no_base_dados ?:''
                rowData['id_origem'] = (it?.id_origem ?: '')
                rowData['nu_autorizacao'] = (it?.nu_autorizacao ?: '')
                rowData['no_autor_registro'] = (it?.no_autor_registro ?: '')
                rowData['no_projeto'] = (it?.no_projeto ?: '')
                rowData['tx_observacao'] = (it?.tx_observacao ?: '')
                rowData['tx_ref_bib'] = _parseRefBibPlanilha(it.tx_ref_bib )
                listData.push(rowData)

            }

            d(' - colocando em ordem alfabetica (listData)')
            // colocar em ordem alfabetica os nomes cientificos
            listData.sort{it.no_cientifico}

            planilha.setTitle(' ')
            planilha.setData(listData)
            planilha.setCitation(autores.join('; ') )

            if ( ! planilha.run() ) {
                throw new Exception(planilha.getError())
            }
            workerService.stepIt(worker)
            if (workerData.email) {
                // zipar o arquivo antes de anexa-lo ao e-mail
                Map zipData = Util.zipFile(fileName)
                if (zipData.zipFileName) {
                    fileName = zipData.zipFileName
                }
                emailService.sendTo(workerData.email.toString(), 'SALVE - Exportação de registros'
                    , """<p>Exportação dos registros de ocorrências realizada em ${horaInicio} foi finalizada.</p>
                                <p>clique ${linkDownload} para baixar a planilha. Este link ficará disponível até o dia ${(new Date() + 5).format('dd/MM/yyyy')}).</p>
                                <p>Atenciosamente,<br>
                                <b>Equipe SALVE</b></p>"""
                    , '', [fullPath: fileName])
            }
        } catch (Exception e) {
            println e.getMessage()
            //workerService.setError(worker, e.getMessage())
            workerService.setMessage(worker, e.getMessage())
            workerService.setCallback(worker, '')
            //e.printStackTrace();
        }
    }


    /**
     * método para exportação das fichas
     * O parâmetro workerData.fichas deve conter a lista de fichas selecionadas pelo fichaService.search()
     * @param response
     * @param idFichas
     * @return
     */
    void fichas(GrailsHttpSession selfSession, Map workerData ) {

        String noCientifico = ''

        //  new String(originalCsv.getBytes('UTF-8'), Charset.forName("ISO-8859-1"))
        /*public byte[] convertToISO8859(byte[] stringBytes) {
            ByteBuffer inputBuffer = ByteBuffer.wrap(stringBytes);
            CharBuffer charBuffer = Charset.forName("UTF-8").decode(inputBuffer) // decode UTF-8
            ByteBuffer outputBuffer = Charset.forName("ISO-8859-15").encode(charBuffer) // encode ISO-8559-15
            return outputBuffer.array()
        }
        */


        // colunas da tabela que será gerada
        List headers = [
                'Ciclo Avaliação'
                , 'Situação da Ficha' //new String('Situação da Ficha', "UTF-8")
                , 'Filo'
                , 'Classe'
                , 'Ordem'
                , 'Família'
                , 'Nome Científico'
                , 'Nome Comum'
                , 'Unidade Responsável'


                , 'Ano Última Avaliação Nacional'
                , 'Categoria Última Avaliação Nacional'
                , 'Critério Última Avaliação Nacional'

                , 'Categoria Avaliação do Ciclo'
                , 'Critério Avaliação do Ciclo'
                //, 'Justificativa Avaliação do Ciclo'

                , 'Categoria Validação do Ciclo'
                , 'Critério Validação do Ciclo'
                , 'Justificativa Validação do Ciclo'
                , 'PEX Final do Ciclo'

        ]

        // variávies
        String path = grailsApplication.config.temp.dir
        String hora = new Date().format('dd_MM_yyyy_hh_mm_ss')
        String fileName = path + 'salve_exportacao_ficha_' + hora + '.csv'
        JSONObject jsonTrilha

        // adicion worker na sessão do usuário
        Map worker = workerService.add(selfSession
                , 'Exportando ' + workerData.fichas.size() + ' ficha(s) para csv.'
                , workerData.fichas.size() + 1 // adicionar um passo a mais para a gravação dos dados em disco no final
        )

        if (workerData.fichas.size() > 0) {
            //File file =
            // file.append(headers.join(';') + '\n')
            StringBuilder sb = new StringBuilder()
            FileOutputStream file = new FileOutputStream( new File(fileName) )
            sb.append( headers.join(';') + '\n' )
            //https://stackoverflow.com/questions/4576222/fastest-way-to-write-to-file

            workerService.setCallback(worker, "window.open(app.url + 'main/download?fileName=" + fileName + "&delete=1','_top');")
            workerService.start(worker)

            /*println 'Usuario: ' + selfSession.sicae.user.noPessoa + '  em ' + new Date().format('dd/MM/yyyy HH:mm:ss')
            println workerData.fichas.size() + ' fichas encontradas!'
            println 'Filename: '+ fileName
            println "-" * 100
            println ' '
            */
            workerData.fichas.eachWithIndex { it, index ->
                //throw new RuntimeException('Teste de erro!')
                /*if( index == 0 )
                {
                    println it
                }
                */
                try {
                    if (it.nm_cientifico != noCientifico) {
                        noCientifico = it.nm_cientifico
                        //println noCientifico
                        workerService.setTitle(worker, it.nm_cientifico)
                    }

                    try {
                        jsonTrilha = JSON.parse(it.json_trilha.toString())
                    } catch( Exception e )
                    {
                        println e.getMessage()
                        jsonTrilha=[:]
                    }


                    // adicionar linha no arquivo de saida
                    sb.append([
                            '"'+it.de_ciclo_avaliacao+' de ' + it.nu_ano_ciclo +' a ' + (it.nu_ano_ciclo.toInteger()+4i)+'"'
                            , '"'+it.ds_situacao_ficha+'"'
                            , '"'+jsonTrilha?.filo?.no_taxon+'"'
                            , '"'+jsonTrilha?.classe?.no_taxon+'"'
                            , '"'+jsonTrilha?.ordem?.no_taxon+'"'
                            , '"'+jsonTrilha?.familia?.no_taxon+'"'

                            , '"'+it.nm_cientifico+'"'
                            , '"'+it.no_comum+'"'
                            , '"'+it.sg_unidade_org+'"'


                            // dados ultima avaliação nacional
                            , '"'+(it.nu_ano_aval_nacional?:'')+'"'
                            , '"'+(it.ds_categoria_iucn_nacional? it.ds_categoria_iucn_nacional+' ('+it.cd_categoria_iucn_nacional+')':'')+'"'
                            , '"'+(it.de_criterio_aval_nacional?:'')+'"'

                            // dados da oficina
                            , '"'+(it.ds_categoria_oficina?:'')+'"'
                            , '"'+(it.ds_criterio_oficina?:'')+'"'
                            //, '"'+(it.ds_justificativa_oficina?:'')+'"'

                            // dados da avaliacao final
                            , '"'+(it.ds_categoria_final_completa?:'')+'"'
                            , '"'+(it.ds_criterio_aval_iucn_final?:'')+'"'
                            , '"'+(it.ds_justificativa_final ? Util.stripTags( it.ds_justificativa_final ):'')+'"'
                            , '"'+Util.snd(it.st_possivelmente_extinta)+'"'




                    ].join(';') + "\n"
                    )
                    // gravar em disco a cada 1000
                    if (((index + 1) % 1000 == 0) && sb.length() > 0) {
                        //println 'ExportDataService: gravando buffer (1000 linhas) - ' + new Date().format('dd/MM/yyyy HH:mm:ss') + ' - ' + selfSession.sicae.user.noPessoa

                        String s = new String( sb.toString().getBytes(), "ISO-8859-1")
                        file.write( s.toString().getBytes() )


                        //file.write( sb.toString().getBytes())
                        sb = new StringBuilder()
                    }

                    // atualizar andamento do worker
                } catch (Exception e) {
                    println ' '
                    println ' '
                    println ' '
                    println 'Erro na ficha ' + it?.sq_ficha
                    println e.getMessage()
                    //println e.printStackTrace()
                    println "-" * 100
                    println ' '
                }
                workerService.stepIt(worker)
            }
            if ( sb.length() > 0 ) {
                println 'ExportDataService: Finalizando exportacao - ' + new Date().format('dd/MM/yyyy HH:mm:ss')+' - '+selfSession.sicae.user.noPessoa
                file.write(sb.toString().getBytes())
            }
            file.close()
            workerService.stepIt(worker)
        }
        else
        {
            workerService.setMessage(worker,"Nenhuma ficha encontrada para exportação.")
        }
    }


    /**
     * Metodo para exportar as fichas para formato ZIP recebendo array de ids de fichas
     * @param selfSession
     * @param workerData
     */
    void gridFichasZip( Map exportParams ) {

        runAsync {
            String horaInicio = new Date().format('dd/MM/yyyy HH:mm:ss')
            String pathTempDir = grailsApplication.config.temp.dir
            String hora = new Date().format('dd_MM_yyyy_hh_mm_ss')
            String zipFileName = pathTempDir + 'salve_exportacao_ficha_' + exportParams?.format?.toLowerCase() + '_' + hora + '.zip'
            boolean imprimirPendencias = (exportParams.pendencias && exportParams.pendencias.toString().toUpperCase() == 'S')

            // CRIAR O JOB NO BANCO DE DADOS
            UserJobs job
            byte[] buffer = new byte[1024]
            FileOutputStream fos = new FileOutputStream(zipFileName)
            ZipOutputStream zos = new ZipOutputStream(fos)

            try {
                // 1º) CRIAR JOB
                Map jobParams = [usuario      : PessoaFisica.get(exportParams.usuario.sqPessoa)
                                 , deRotulo   : exportParams.jobRotulo ?: 'Exportação fichas no formato ' + exportParams.format
                                 , deEmail    : exportParams.email ?: null
                                 , deMensagem : ''
                                 , vlMax      : exportParams.idsFichas.size()
                                 , vlAtual    : 0
                                 , deAndamento: '0 de ' + exportParams.idsFichas.size() + ' (%s)'
                ]
                jobParams.noArquivoAnexo = zipFileName

                // enviar o email
                if (jobParams.deEmail ) {
                    jobParams.deMensagem = """<p>Segue anexo a planilha com as fichas exportadas em ${horaInicio}.</p>
                            <p>Caso não tenha recebido o arquivo em anexo, clique <a target="_blank" href="https://salve.icmbio.gov.br/salve-estadual/download?fileName=${zipFileName}">AQUI</a> para baixar a planilha. Este link ficará disponível até o dia ${(new Date() + 5).format('dd/MM/yyyy')}).</p>
                            <p>Atenciosamente,<br>
                            <b>Equipe SALVE</b></p>"""
                }

                job = userJobService.createJob(jobParams).save(flush: true)

                // consultar as fichas
                if ( ! exportParams.idsFichas && exportParams.cmdSql) {
                    List rows = sqlService.execSql(exportParams.cmdSql + ' OFFSET 0 LIMIT ' + (exportParams.maxFichasExportarPdf + 1), exportParams.sqlParams)
                    // pegar somente a coluna sq_ficha do resultado
                    if (!rows) {
                        throw new Exception('Nenhuma ficha econtrada')
                    }
                    exportParams.idsFichas = rows?.sq_ficha
                }

            job.vlAtual=0
            job.vlMax = exportParams.idsFichas.size()
            job.save( flush:true)

            // variáveis gerar o arquivo zip
            exportParams.idsFichas.eachWithIndex { sqFicha, rowIndex ->
                    sqlService.execSql("""select nm_cientifico, ciclo.de_ciclo_avaliacao from salve.ficha
                                    inner join salve.ciclo_avaliacao ciclo on ciclo.sq_ciclo_avaliacao = ficha.sq_ciclo_avaliacao
                                    where ficha.sq_ficha  = :sqFicha""", [sqFicha: sqFicha.toLong()]).each { row ->

                        // criar arquivo temporario da ficha
                        String tempFileName = ''
                        if (exportParams?.format == 'PDF') {
                            PdfFicha pdf = new PdfFicha(sqFicha.toInteger(), pathTempDir)

                            // integração SIS/IUCN - definir a linguagem de impressão do pdf pt ou en
                            exportParams.language = exportParams.language ?: 'pt'
                            if (exportParams.language == 'en') {
                                pdf.setTranslateToEnglish(true)
                            }
                            pdf.setRefBibs(fichaService.getFichaRefBibs(sqFicha.toLong()))
                            pdf.setUfs(fichaService.getUfsNameByRegiaoText(sqFicha.toLong()))
                            pdf.setBiomas(fichaService.getBiomasText(sqFicha.toLong()))
                            pdf.setBacias(fichaService.getBaciasText(sqFicha.toLong()))
                            pdf.setUcs(fichaService.getUcsRegistro(sqFicha.toLong()))
                            pdf.unidade = exportParams?.usuario?.sgUnidadeOrg ?: 'SALVE WS'
                            pdf.ciclo = row.de_ciclo_avaliacao
                            pdf.setImprimirPendencias(imprimirPendencias)
                            tempFileName = pdf.run()
                        } else {
                            Map config = [adAvaliacao: 'N', adColaboracao: 'N', adValidacao: 'N', sqOficina: 0]
                            RtfFicha rtf = new RtfFicha(sqFicha.toInteger(), pathTempDir, config)
                            rtf.setRefBibs(fichaService.getFichaRefBibs(sqFicha.toLong()))
                            rtf.setUfs(fichaService.getUfsNameByRegiaoText(sqFicha.toLong()))
                            rtf.setBiomas(fichaService.getBiomasText(sqFicha.toLong()))
                            rtf.setBacias(fichaService.getBaciasText(sqFicha.toLong()))
                            rtf.setUcs(fichaService.getUcsRegistro(sqFicha.toLong()))
                            rtf.unidade = exportParams?.usuario?.sgUnidadeOrg ?: 'SALVE'
                            rtf.ciclo = row.de_ciclo_avaliacao
                            tempFileName = rtf.run()
                        }

                        if (tempFileName) {
                            String entryName = tempFileName.substring(pathTempDir.length(), tempFileName.length())
                            ZipEntry ze = new ZipEntry(entryName)
                            try {
                                zos.putNextEntry(ze)
                                FileInputStream fis = new FileInputStream(tempFileName)
                                int len
                                while ((len = fis.read(buffer)) > 0) {
                                    zos.write(buffer, 0, len)
                                }
                                fis.close()
                            } catch (Exception e) {
                                println e.getMessage()
                            }
                            new File(tempFileName).delete()
                            if( rowIndex > 0 ) {
                                job.stepIt('Exportando fichas ' + rowIndex + ' de ' + exportParams.idsFichas.size() )
                                if( job.stCancelado ) {
                                    throw new Exception ('Cancelada pelo usuário')
                                }
                            }
                        }
                    } // fim each ids
                }
                zos.closeEntry()
                zos.close()
                job.stepIt('Exportação de '+exportParams.idsFichas.size() + ' ficha(s) finalizada.')
            } catch (Exception error) {
                String usuario = exportParams.usuario ? '\n' +
                    exportParams?.usuario?.noPessoa +' - CPF: '+ Util.formatCpf(exportParams?.usuario?.nuCpf) + ' - ID: ' +exportParams?.usuario?.sqPessoa : ''
                Util.printLog('SALVE - ExportDataService.gridFichasZip()','Erro ao exportar fichas no formato '+exportParams?.format + usuario
                    ,error.getMessage())
                // encerrar o job
                userJobService.error( job.id, error.getMessage() )
                try {
                    zos.closeEntry()
                    zos.close()
                } catch( Exception e ){}
            }
        }
    }

    /**
     * Metodo para exportar fichas para planilha recebendo array de ids de fichas
     * @param selfSession
     * @param workerData
     */
    void gridFichasPlanilha( Map exportParams ) {
        runAsync {
            String horaInicio = new Date().format('dd/MM/yyyy HH:mm:ss')
            String urlSalve = grailsApplication.config.url.sistema ?: 'https://salve.icmbio.gov.br/salve-estadual/'
            String path = grailsApplication.config.temp.dir
            String hora = new Date().format('dd_MM_yyyy_hh_mm_ss')
            String tempFileName = path + 'salve_exportacao_ficha_' + hora + '.xlsx'
            String linkDownload = '<a target="_blank" href="' + urlSalve + 'download?fileName=' + tempFileName + '"><b>AQUI</b></a>'

            UserJobs job
            try {
                if( ! exportParams.idsFichas ) {
                    throw new Exception('Nenhuma ficha para exportação')
                }
                // o codigo da avaliação nacional na tabela de apoio
                Long sqAvaliacaoNacional = 0L
                sqlService.execSql("""select sq_dados_apoio from salve.dados_apoio
                                where cd_sistema = 'NACIONAL_BRASIL'
                                  and sq_dados_apoio_pai = ( select x.sq_dados_apoio from salve.dados_apoio x
                                  where x.cd_sistema = 'TB_TIPO_AVALIACAO' and x.sq_dados_apoio_pai is null offset 0 limit 1)   """).each { row ->
                    sqAvaliacaoNacional = row.sq_dados_apoio.toLong()
                }
                // Ler as fichas selecionada com as colunas que serão exportadas para planilha
                // o nome da coluna deve ser a mesmo que for informado nas colunas da planilha
                String cmdSql = """select concat(ciclo.de_ciclo_avaliacao,' de ', ciclo.nu_ano, ' a ' , (ciclo.nu_ano + 4 ) ) as de_ciclo_avaliacao
                    ,situacao.ds_dados_apoio as ds_situacao_ficha
                    ,taxon.json_trilha->'filo'->>'no_taxon' as no_filo
                    ,taxon.json_trilha->'classe'->>'no_taxon' as no_classe
                    ,taxon.json_trilha->'ordem'->>'no_taxon' as no_ordem
                    ,taxon.json_trilha->'familia'->>'no_taxon' as no_familia
                    ,coalesce(taxon.json_trilha->'subespecie'->>'no_taxon',taxon.json_trilha->'especie'->>'no_taxon') as nm_cientifico
                    ,taxon.no_autor
                    ,nome_comum.no_comum
                    ,uo.sg_unidade_org
                    ,grupo.ds_dados_apoio as no_grupo
                    ,grupo_salve.ds_dados_apoio as no_grupo_salve
                    ,avaliacao_nacional.nu_ano_ultima_avaliacao_nacional
                    ,avaliacao_nacional.de_categoria_ultima_avaliacao_nacional
                    ,avaliacao_nacional.de_criterio_ultima_avaliacao_nacional
                    ,case when ficha.sq_categoria_iucn is not null and categoria_avaliada.cd_sistema <> 'NE' then concat( categoria_avaliada.ds_dados_apoio,' (',categoria_avaliada.cd_dados_apoio,')') else '' end as de_categoria_avaliacao
                    ,ficha.ds_criterio_aval_iucn as de_criterio_avaliacao
                    ,case when ficha.sq_categoria_final is not null and categoria_validada.cd_sistema <> 'NE' then concat( categoria_validada.ds_dados_apoio,' (',categoria_validada.cd_dados_apoio,')') else '' end as de_categoria_validacao
                    ,ficha.ds_criterio_aval_iucn_final as de_criterio_validacao
                    ,salve.remover_html_tags( salve.entity2char(ficha.ds_justificativa)) as tx_justificativa_avaliacao
                    ,salve.remover_html_tags( salve.entity2char(ficha.ds_justificativa_final)) as tx_justificativa_validacao
                    ,salve.snd(ficha.st_possivelmente_extinta_final) as de_pex_validacao
                    ,ficha.ds_citacao as no_autores
    -- campos abertos aba 1
    , salve.remover_html_tags( salve.entity2char(ficha.ds_notas_taxonomicas ) ) as ds_notas_taxonomicas
    , salve.remover_html_tags( salve.entity2char(ficha.ds_diagnostico_morfologico ) ) as ds_diagnostico_morfologico
    -- campos abertos aba 2
    , salve.snd(ficha.st_endemica_brasil) as st_endemica_brasil
    , salve.remover_html_tags( salve.entity2char(ficha.ds_distribuicao_geo_global ) ) as ds_distribuicao_geo_global
    , salve.remover_html_tags( salve.entity2char(ficha.ds_distribuicao_geo_nacional ) ) as ds_distribuicao_geo_nacional
    -- campos abertos aba 3
    , salve.snd( ficha.st_migratoria) as st_migratoria
    , salve.remover_html_tags( salve.entity2char(ficha.ds_historia_natural ) ) as ds_historia_natural
    -- campos aberto aba 4
    , salve.remover_html_tags( salve.entity2char(ficha.ds_populacao ) ) as ds_populacao
    -- campos abertos aba 5 - ameaças
    ,replace(rel.no_ameacas,'|',chr(13)) as no_ameacas
    ,salve.remover_html_tags( salve.entity2char( ficha.ds_ameaca ) ) as ds_ameaca
    -- campos abertos aba 6 - usos
    ,replace( rel.no_usos, '|', chr(13)) as no_usos
    ,salve.remover_html_tags( salve.entity2char(ficha.ds_uso ) ) as ds_uso
    -- campos abertos aba 7
    ,salve.remover_html_tags( salve.entity2char(ficha.ds_historico_avaliacao ) ) as ds_historico_avaliacao
    -- campos abertos aba 8
    ,salve.remover_html_tags( salve.entity2char(ficha.ds_pesquisa_exist_necessaria ) ) as ds_pesquisa_exist_necessaria
    -- campos abertos aba 11
    ,salve.remover_html_tags( salve.entity2char(ficha.ds_justificativa_aoo_eoo ) ) as ds_justificativa_aoo_eoo
    -- ESTADOS
    ,rel.no_estados
    -- BIOMAS
    ,rel.no_biomas
    -- UCS
    ,replace(rel.no_ucs,';',chr(13)) as no_ucs
    -- PANS
    ,replace(rel.no_pans,';',chr(13)) as no_pans

    from salve.ficha as ficha
                    inner join salve.vw_unidade_org as uo on uo.sq_pessoa = ficha.sq_unidade_org
                    inner join salve.dados_apoio as situacao on situacao.sq_dados_apoio = ficha.sq_situacao_ficha
                    left  join salve.dados_apoio as grupo on grupo.sq_dados_apoio = ficha.sq_grupo
                    left  join salve.dados_apoio as grupo_salve on grupo_salve.sq_dados_apoio = ficha.sq_grupo_salve
                    inner join salve.vw_pessoa as pessoa on pessoa.sq_pessoa = ficha.sq_usuario_alteracao
                    inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                    inner join taxonomia.nivel_taxonomico as nivel on nivel.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
                    inner join salve.ciclo_avaliacao as ciclo on ciclo.sq_ciclo_avaliacao = ficha.sq_ciclo_avaliacao
                    -- ultima avaliacao
                    left join salve.dados_apoio as categoria_avaliada on categoria_avaliada.sq_dados_apoio = ficha.sq_categoria_iucn
                    -- ultima validacao
                    left join salve.dados_apoio as categoria_validada on categoria_validada.sq_dados_apoio = ficha.sq_categoria_final
                    -- nomes comuns
                    left join lateral (
                       select array_to_string( array_agg(distinct x.no_comum order by x.no_comum),', ') as no_comum
                       from salve.ficha_nome_comum x
                       where x.sq_ficha = ficha.sq_ficha
                    ) as nome_comum on true
                    -- ultima avaliacao nacional
                    left join lateral (
                       select h.nu_ano_avaliacao as nu_ano_ultima_avaliacao_nacional
                       ,concat( categoria_1.ds_dados_apoio,' (',categoria_1.cd_dados_apoio,')') as de_categoria_ultima_avaliacao_nacional
                       ,de_criterio_avaliacao_iucn as de_criterio_ultima_avaliacao_nacional
                       from salve.taxon_historico_avaliacao h
                       inner join salve.dados_apoio categoria_1 on categoria_1.sq_dados_apoio = h.sq_categoria_iucn
                       where h.sq_taxon = ficha.sq_taxon
                       and h.sq_tipo_avaliacao = ${sqAvaliacaoNacional.toString()}
                       and h.nu_ano_avaliacao <= ciclo.nu_ano + 4
                       order by h.nu_ano_avaliacao desc
                       offset 0 limit 1
                    ) as avaliacao_nacional on true

                    left outer join salve.mv_relatorio_taxon_analitico rel on rel.sq_ficha = ficha.sq_ficha
                    where ficha.sq_ficha in ( ${exportParams.idsFichas.join(',')} )
            """

                /** /
                println ' '
                println ' '
                println 'EXPORTACAO FICHAS GRIDE '
                println cmdSql
                 /**/

                List rows = sqlService.execSql(cmdSql)
                rows.sort{it.nm_cientifico}

                // fazer ajustes nos dados antes de enviar para a planilha
                rows.each { row ->
                    // fazer tratamento do nome cientifico para colocar itálico
                    row.nm_cientifico = Util.ncItalico(row.nm_cientifico, true, true)
                    // tratamento da coluna uso para não repetir codigo

                    if( row.no_usos ) {
                        List usos = []
                        row.no_usos.split(';').collect { uso ->
                            uso.split('\\|').collect{
                                if( !usos.contains(it)){
                                    usos.push( it )
                                }
                            }
                        }
                        row.no_usos = usos.join('\n')
                    }
                    if( row.no_ameacas ) {
                        List usos = []
                        row.no_ameacas.split(';').collect { uso ->
                            uso.split('\\|').collect{
                                if( !usos.contains(it)){
                                    usos.push( it )
                                }
                            }
                        }
                        row.no_ameacas = usos.join('\n')
                    }
                }

                // 1º) CRIAR PARAMETROS DO JOB
                Map jobParams = [usuario      : PessoaFisica.get(exportParams.usuario.sqPessoa)
                                 , deRotulo   : exportParams.jobRotulo ?: 'Exportação fichas'
                                 , deEmail    : exportParams.email ?: null
                                 , deMensagem : ''
                                 , vlMax      : rows.size()
                                 , vlAtual    : 0
                                 , deAndamento: '0 de ' + rows.size() + ' (%s)' // %s é onde será exibido o percentual executado
                ]
                if (jobParams.deEmail) {
                    jobParams.deEmailAssunto = exportParams.emailAssunto ?: 'SALVE - Exportação fichas finalizada'
                    jobParams.deMensagem = """<p>Segue o link para baixar a planilha com os dados exportados em ${horaInicio}.</p>
                                <p>Clique ${linkDownload} para baixar o arquivo. Este link ficará disponível até o dia ${(new Date() + 5).format('dd/MM/yyyy')}).</p>
                                <p>Atenciosamente,<br>
                                <b>Equipe SALVE</b></p>"""
                }


                // CRIAR O JOB NO BANCO DE DADOS
                jobParams.noArquivoAnexo = tempFileName
                job = userJobService.createJob(jobParams).save(flush: true)


                // criar a planilha
                PlanilhaExcel planilha = new PlanilhaExcel(tempFileName, 'Fichas Salve', true)
                planilha.setTitle(' ') // não adicionar titulo na planilha
                planilha.setData(rows)
                planilha.addColumn(['title': 'Ciclo Avaliação', 'column': 'de_ciclo_avaliacao', 'autoFit': true,'width':30])
                planilha.addColumn(['title': 'Situação', 'column': 'ds_situacao_ficha', 'autoFit': true, 'bgColor':'#ffffff', 'align':'center'])
                planilha.addColumn(['title': 'Filo', 'column': 'no_filo', 'autoFit': true,'align':'center'])
                planilha.addColumn(['title': 'Classe', 'column': 'no_classe', 'autoFit': true,'align':'center'])
                planilha.addColumn(['title': 'Ordem', 'column': 'no_ordem', 'autoFit': true,'align':'center'])
                planilha.addColumn(['title': 'Família', 'column': 'no_familia', 'autoFit': true,'align':'center'])
                planilha.addColumn(['title': 'Nome científico', 'column': 'nm_cientifico', 'autoFit': true, 'width':25,'align':'center'])
                planilha.addColumn(['title': 'Autor', 'column': 'no_autor', 'autoFit': true, 'width':25,'align':'center'])
                planilha.addColumn(['title': 'Nome comum', 'column': 'no_comum', 'autoFit': true, 'width':25])
                planilha.addColumn(['title': 'Unidade', 'column': 'sg_unidade_org', 'autoFit': true])
                planilha.addColumn(['title': 'Grupo Avaliado', 'column': 'no_grupo', 'autoFit': true,'align':'center'])
                planilha.addColumn(['title': 'Grupo', 'column': 'no_grupo_salve', 'autoFit': true,'align':'center' ,'width':20])
                planilha.addColumn(['title': 'Ano Última Avaliação Nacional', 'column': 'nu_ano_ultima_avaliacao_nacional', 'autoFit': true, 'align':'center'])
                planilha.addColumn(['title': 'Categoria Última Avaliação Nacional', 'column': 'de_categoria_ultima_avaliacao_nacional', 'autoFit': true,'align':'center'])
                planilha.addColumn(['title': 'Critério Última Avaliação Nacional', 'column': 'de_criterio_ultima_avaliacao_nacional', 'autoFit': true,'align':'center'])
                planilha.addColumn(['title': 'Categoria Avaliação do Ciclo', 'column': 'de_categoria_avaliacao', 'autoFit': true,'align':'center'])
                planilha.addColumn(['title': 'Critério Avaliação do Ciclo', 'column': 'de_criterio_avaliacao', 'autoFit': true,'align':'center'])
                planilha.addColumn(['title': 'Justificativa Avaliação do Ciclo', 'column': 'tx_justificativa_avaliacao', 'autoFit': true, 'width':50])
                planilha.addColumn(['title': 'Categoria Validação no Ciclo', 'column': 'de_categoria_validacao', 'autoFit': true,'align':'center'])
                planilha.addColumn(['title': 'Critério Validação no Ciclo', 'column': 'de_criterio_validacao', 'autoFit': true,'align':'center'])
                planilha.addColumn(['title': 'Justificativa Validação do Ciclo', 'column': 'tx_justificativa_validacao', 'autoFit': true,'width':50])
                planilha.addColumn(['title': 'PEX Final do Ciclo', 'column': 'de_pex_validacao', 'autoFit': true,'align':'center'])
                planilha.addColumn(['title': 'Autores da ficha', 'column': 'no_autores', 'autoFit': true, width: 50])
                // campos abertos aba 1
                String cor1='#fffff1'
                String cor2='#fffff1'
                planilha.addColumn(['title': 'Notas taxonômicas', 'column': 'ds_notas_taxonomicas', 'autoFit': true, width: 40, 'bgColor': cor1, 'color': '#000000'])
                planilha.addColumn(['title': 'Notas Morfológicas', 'column': 'ds_diagnostico_morfologico', 'autoFit': true, width: 40, 'bgColor': cor1, 'color': '#000000'])
                // campos abertos aba 2
                planilha.addColumn(['title': 'Endêmica Brasil?', 'column': 'st_endemica_brasil', 'autoFit': true, 'bgColor': cor2, 'color': '#000000', 'align':'center'])
                planilha.addColumn(['title': 'Distribuição Geográfica Global', 'column': 'ds_distribuicao_geo_global', 'autoFit': true, width: 40, 'bgColor': cor2, 'color': '#000000'])
                planilha.addColumn(['title': 'Distribuição Geográfica Nacional', 'column': 'ds_distribuicao_geo_nacional', 'autoFit': true, width: 40, 'bgColor': cor2, 'color': '#000000'])
                // campos abertos aba 3
                planilha.addColumn(['title': 'Migratória?', 'column': 'st_migratoria', 'autoFit': true, 'bgColor':cor1, 'color': '#000000', 'align':'center'])
                planilha.addColumn(['title': 'História Natural', 'column': 'ds_historia_natural', 'autoFit': true, width: 40,'bgColor':cor1, 'color': '#000000'])
                // campos abertos aba 4
                planilha.addColumn(['title': 'Observação População', 'column': 'ds_populacao', 'autoFit': true, width: 40,'bgColor':cor2, 'color': '#000000'])
                // campos abertos aba 5
                planilha.addColumn(['title': 'Ameaças', 'column': 'no_ameacas', 'autoFit': true, width: 50,'bgColor':cor1, 'color': '#000000'])
                planilha.addColumn(['title': 'Observação Ameaça', 'column': 'ds_ameaca', 'autoFit': true, width: 40,'bgColor':cor1, 'color': '#000000'])
                // campos abertos aba 6
                // usos
                planilha.addColumn(['title': 'Usos', 'column': 'no_usos', 'autoFit': true, width: 50,'bgColor':cor2, 'color': '#000000'])
                planilha.addColumn(['title': 'Observação Uso', 'column': 'ds_uso', 'autoFit': true, width: 40,'bgColor':cor2, 'color': '#000000'])
                // campos abertos aba 7
                planilha.addColumn(['title': 'Observações gerais sobre o histórico de avaliação', 'column': 'ds_historico_avaliacao', 'autoFit': true, width: 40,'bgColor':cor1, 'color': '#000000'])
                 // campos abertos aba 8
                planilha.addColumn(['title': 'Observações Gerais Sobre as Pesquisas Existentes/Necessárias', 'column': 'ds_pesquisa_exist_necessaria', 'autoFit': true,'bgColor':cor2, 'color': '#000000'])
                 // campos abertos aba 11
                planilha.addColumn(['title': 'Explicacao (EOO/AOO)', 'column': 'ds_justificativa_aoo_eoo', 'autoFit': true, width: 40,'bgColor':cor1, 'color': '#000000'])
                // estados
                planilha.addColumn(['title': 'Estados', 'column': 'no_estados', 'autoFit': true, width: 40])
                // biomas
                planilha.addColumn(['title': 'Biomas', 'column': 'no_biomas', 'autoFit': true, width: 40])
                // ucs
                planilha.addColumn(['title': 'Unidades de Conservação', 'column': 'no_ucs', 'autoFit': true, width: 50])
                // PANS
                planilha.addColumn(['title': 'PANs', 'column': 'no_pans', 'autoFit': true, width: 50])



                if (!planilha.run(job)) {
                    throw new Exception(planilha.getError())
                }
            } catch (Exception error) {
                String usuario = exportParams.usuario ? '\n' +
                    exportParams?.usuario?.noPessoa + ' - CPF: ' + Util.formatCpf(exportParams?.usuario?.nuCpf) + ' - ID: ' + exportParams?.usuario?.sqPessoa : ''
                Util.printLog('SALVE - ExportDataService.gridFichasPlanilha', 'Erro ao exportar fichas' + usuario
                    , error.getMessage())
                if (job) {
                    // encerrar o job
                    userJobService.error(job.id, error.getMessage())
                }
            }
        }
    }

    /**
     * Exportar gride de oficinas de avaliação para planilha
     * @param exportParams
     */
    void exportOficinasAvaliacaoPlanilha( Map exportParams ){
        runAsync {
            String horaInicio = new Date().format('dd/MM/yyyy HH:mm:ss')
            String urlSalve = grailsApplication.config.url.sistema ?: 'https://salve.icmbio.gov.br/salve-estadual/'
            String path = grailsApplication.config.temp.dir
            String hora = new Date().format('dd_MM_yyyy_hh_mm_ss')
            String tempFileName = path + 'salve_exportacao_oficina_avaliacao_' + hora + '.xlsx'
            String linkDownload = '<a target="_blank" href="' + urlSalve + 'download?fileName=' + tempFileName + '"><b>AQUI</b></a>'

            UserJobs job
            try {

                if( ! exportParams.rows ) {
                    throw new Exception('Nenhuma oficina para exportação')
                }

                // fazer ajustes nos dados antes de enviar para a planilha
                exportParams.rows.each { row ->
                    row.dt_inicio = row.dt_inicio.format('dd/MM/yyyy')
                    row.dt_fim = row.dt_fim.format('dd/MM/yyyy')
                }

                // 1º) CRIAR PARAMETROS DO JOB
                Map jobParams = [usuario      : PessoaFisica.get(exportParams.usuario.sqPessoa)
                             , deRotulo   : exportParams.jobRotulo ?: 'Exportação oficinas'
                             , deEmail    : exportParams.email ?: null
                             , deMensagem : ''
                             , vlMax      : exportParams.rows.size()
                             , vlAtual    : 0
                             , deAndamento: '0 de ' + exportParams.rows.size() + ' (%s)' // %s é onde será exibido o percentual executado
                ]
                if (jobParams.deEmail) {
                    jobParams.deEmailAssunto = exportParams.emailAssunto ?: 'SALVE - Exportação fichas finalizada'
                    jobParams.deMensagem = """<p>Segue o link para baixar a planilha com os dados exportados em ${horaInicio}.</p>
                                <p>Clique ${linkDownload} para baixar o arquivo. Este link ficará disponível até o dia ${(new Date() + 5).format('dd/MM/yyyy')}).</p>
                                <p>Atenciosamente,<br>
                                <b>Equipe SALVE</b></p>"""
                }


                // CRIAR O JOB NO BANCO DE DADOS
                jobParams.noArquivoAnexo = tempFileName
                job = userJobService.createJob(jobParams).save(flush: true)


                // criar a planilha
                PlanilhaExcel planilha = new PlanilhaExcel(tempFileName, 'Oficinas avaliação', true)
                //planilha.setTitle('SALVE - Exportação da Oficinas de Avaliação e Reuniões Preparatórias')
                planilha.setTitle(' ') // para não imprimir o titulo padrão
                planilha.setData(exportParams.rows)
                planilha.addColumn(['title': 'Oficina'  , 'column': 'no_oficina', 'autoFit': true,'width':60])
                planilha.addColumn(['title': 'Sigla'    , 'column': 'sg_oficina', 'autoFit': true,'width':20])
                planilha.addColumn(['title': 'Local'    , 'column': 'de_local'  , 'autoFit': true,'align':'center'])
                planilha.addColumn(['title': 'Início'   , 'column': 'dt_inicio' , 'autoFit': true,'align':'right'])
                planilha.addColumn(['title': 'Término'  , 'column': 'dt_fim'    , 'autoFit': true,'align':'center'])
                planilha.addColumn(['title': 'Tipo'     , 'column': 'ds_tipo'   , 'autoFit': true])
                planilha.addColumn(['title': 'Unidade'  , 'column': 'sg_unidade_org', 'autoFit': true, 'width':40])
                planilha.addColumn(['title': 'Situação' , 'column': 'ds_situacao', 'autoFit': true])
                planilha.addColumn(['title': 'Fonte de Recursos', 'column': 'no_fonte_recurso', 'autoFit': true])
                if (!planilha.run(job)) {
                    throw new Exception(planilha.getError())
                }
            } catch (Exception error) {
                String usuario = exportParams.usuario ? '\n' +
                    exportParams?.usuario?.noPessoa + ' - CPF: ' + Util.formatCpf(exportParams?.usuario?.nuCpf) + ' - ID: ' + exportParams?.usuario?.sqPessoa : ''
                Util.printLog('SALVE - ExportDataService.gridFichasPlanilha', 'Erro ao exportar fichas' + usuario
                    , error.getMessage())
                if (job) {
                    // encerrar o job
                    userJobService.error(job.id, error.getMessage())
                }
            }
        }
    }
    /**
     * Exportar gride de oficinas de Validação para planilha
     * @param exportParams
     */
    void exportOficinasValidacaoPlanilha( Map exportParams ){
        runAsync {
            String horaInicio = new Date().format('dd/MM/yyyy HH:mm:ss')
            String urlSalve = grailsApplication.config.url.sistema ?: 'https://salve.icmbio.gov.br/salve-estadual/'
            String path = grailsApplication.config.temp.dir
            String hora = new Date().format('dd_MM_yyyy_hh_mm_ss')
            String tempFileName = path + 'salve_exportacao_oficina_validacao_' + hora + '.xlsx'
            String linkDownload = '<a target="_blank" href="' + urlSalve + 'download?fileName=' + tempFileName + '"><b>AQUI</b></a>'

            UserJobs job
            try {

                if( ! exportParams.rows ) {
                    throw new Exception('Nenhuma oficina para exportação')
                }

                // fazer ajustes nos dados antes de enviar para a planilha
                exportParams.rows.each { row ->
                    row.dt_inicio = row.dt_inicio.format('dd/MM/yyyy')
                    row.dt_fim = row.dt_fim.format('dd/MM/yyyy')
                }

                // 1º) CRIAR PARAMETROS DO JOB
                Map jobParams = [usuario      : PessoaFisica.get(exportParams.usuario.sqPessoa)
                             , deRotulo   : exportParams.jobRotulo ?: 'Exportação oficinas'
                             , deEmail    : exportParams.email ?: null
                             , deMensagem : ''
                             , vlMax      : exportParams.rows.size()
                             , vlAtual    : 0
                             , deAndamento: '0 de ' + exportParams.rows.size() + ' (%s)' // %s é onde será exibido o percentual executado
                ]
                if (jobParams.deEmail) {
                    jobParams.deEmailAssunto = exportParams.emailAssunto ?: 'SALVE - Exportação fichas finalizada'
                    jobParams.deMensagem = """<p>Segue o link para baixar a planilha com os dados exportados em ${horaInicio}.</p>
                                <p>Clique ${linkDownload} para baixar o arquivo. Este link ficará disponível até o dia ${(new Date() + 5).format('dd/MM/yyyy')}).</p>
                                <p>Atenciosamente,<br>
                                <b>Equipe SALVE</b></p>"""
                }


                // CRIAR O JOB NO BANCO DE DADOS
                jobParams.noArquivoAnexo = tempFileName
                job = userJobService.createJob(jobParams).save(flush: true)


                // criar a planilha
                PlanilhaExcel planilha = new PlanilhaExcel(tempFileName, 'Oficinas avaliação', true)
                //planilha.setTitle('SALVE - Exportação da Oficinas de Avaliação e Reuniões Preparatórias')
                planilha.setTitle(' ') // para não imprimir o titulo padrão
                planilha.setData(exportParams.rows)
                planilha.addColumn(['title': 'Oficina'  , 'column': 'no_oficina', 'autoFit': true,'width':60])
                planilha.addColumn(['title': 'Sigla'    , 'column': 'sg_oficina', 'autoFit': true,'width':20])
                planilha.addColumn(['title': 'Local'    , 'column': 'de_local'  , 'autoFit': true,'align':'center'])
                planilha.addColumn(['title': 'Início'   , 'column': 'dt_inicio' , 'autoFit': true,'align':'right'])
                planilha.addColumn(['title': 'Término'  , 'column': 'dt_fim'    , 'autoFit': true,'align':'center'])
                planilha.addColumn(['title': 'Situação' , 'column': 'ds_situacao', 'autoFit': true])
                planilha.addColumn(['title': 'Quantidade de Fichas', 'column': 'nu_fichas', 'autoFit': true,'align':'right'])
                if (!planilha.run(job)) {
                    throw new Exception(planilha.getError())
                }
            } catch (Exception error) {
                String usuario = exportParams.usuario ? '\n' +
                    exportParams?.usuario?.noPessoa + ' - CPF: ' + Util.formatCpf(exportParams?.usuario?.nuCpf) + ' - ID: ' + exportParams?.usuario?.sqPessoa : ''
                Util.printLog('SALVE - ExportDataService.gridFichasPlanilha', 'Erro ao exportar fichas' + usuario
                    , error.getMessage())
                if (job) {
                    // encerrar o job
                    userJobService.error(job.id, error.getMessage())
                }
            }
        }
    }

    /**
     * Exportar os arquivos zip dos shapefiles das fichas para um arquivo zip unico
     * @param selfSession
     * @param workerData
     */
    void asyncExportShapefileFicha( Map exportParams ) {
        runAsync {
            String urlSalve = grailsApplication.config.url.sistema ?: 'https://salve.icmbio.gov.br/salve-estadual/'
            String horaInicio = new Date().format('dd/MM/yyyy HH:mm:ss')
            String pathTempDir = grailsApplication.config.temp.dir
            String anexosDir = grailsApplication.config.anexos.dir
            String hora = new Date().format('dd_MM_yyyy_hh_mm_ss')
            String zipFileName = pathTempDir + 'salve_exportacao_shapes_ficha_' + hora + '.zip'
            String linkDownload = '<a target="_blank" href="' + urlSalve + 'download?fileName=' + zipFileName + '"><b>AQUI</b></a>'
            UserJobs job
            if( ! exportParams.idsfichas && exportParams.cmdSql ) {
                List rows = sqlService.execSql(exportParams.cmdSql + ' OFFSET 0 LIMIT ' + (exportParams.maxFichas + 1), exportParams.sqlParams)
                // ler somente a coluna sq_ficha do resultado
                if ( rows ) {
                    exportParams.idsFichas = rows.sq_ficha
                }
            }

            // 1º) CRIAR PARAMETROS DO JOB
            Map jobParams = [usuario      : PessoaFisica.get(exportParams.usuario.sqPessoa)
                             , deRotulo   : exportParams.jobRotulo ?: 'Exportação dos shapefiles das fichas'
                             , deEmail    : exportParams.email ?: null
                             , deMensagem : ''
                             , vlAtual    : 0

            ]

            if (jobParams.deEmail) {
                jobParams.deEmailAssunto = exportParams.emailAssunto ?: 'SALVE - Exportação shapefiles fichas'
                jobParams.deMensagem = """<p>Segue anexo o arquivo ZIP contendo o(s) shapefile(s) da(s) ficha(s) selecionada(s) em ${horaInicio}.</p>
                                <p>Caso não tenha recebido o arquivo em anexo, clique <a target="_blank" href="https://salve.icmbio.gov.br/salve-estadual/download?fileName=${zipFileName}">AQUI</a> para baixar a planilha. Este link ficará disponível até o dia ${(new Date() + 5).format('dd/MM/yyyy')}).</p>
                                <p>Atenciosamente,<br>
                                <b>Equipe SALVE</b></p>"""
            }

            // CRIAR O JOB NO BANCO DE DADOS
            jobParams.noArquivoAnexo = zipFileName
            job = userJobService.createJob(jobParams).save(flush: true)

            // variáveis utilizadas para gerar o arquivo zip
            byte[] buffer = new byte[1024]
            FileOutputStream fos = new FileOutputStream(zipFileName)
            ZipOutputStream zos = new ZipOutputStream(fos)

            try {
                if (exportParams.idsFichas.size() == 0) {
                    throw new Exception('Nenhuma ficha encontrada.')
                }
                DadosApoio contextoMapa = dadosApoioService.getByCodigo('TB_CONTEXTO_ANEXO', 'MAPA_DISTRIBUICAO')
                String cmdSql = """select ficha.nm_cientifico,ficha_anexo.sq_ficha_anexo, ficha_anexo.no_arquivo, ficha_anexo.de_local_arquivo
                        from salve.ficha_anexo
                        inner join salve.ficha on ficha.sq_ficha = ficha_anexo.sq_ficha
                        where ficha_anexo.sq_contexto = ${contextoMapa.id.toString()}
                        and ficha_anexo.no_arquivo ilike '%.zip'
                        """
                cmdSql += " and ficha_anexo.sq_ficha = any( array[${exportParams.idsFichas.join(',')}]) "
                if (exportParams.shapefiles == 'A') {
                    cmdSql += " and ficha_anexo.in_principal='S'"
                }

                List rows = sqlService.execSql(cmdSql)
                if (!rows) {
                    throw new Exception("Nenhum shapefile cadastrado")
                }

                job.vlMax           = rows.size()
                job.deAndamento     = '0 de '+job.vlMax // adicionar %s para exibir o percentual executado
                job.vlAtual         = 0
                job.save(flush:true)

                rows.eachWithIndex { row,rowIndex ->
                    // criar arquivo temporario da ficha
                    String arquivoAnexo = anexosDir + row.de_local_arquivo
                    if (arquivoAnexo) {
                        File arquivo = new File(arquivoAnexo)
                        if (arquivo.exists()) {
                            String entryName = Util.ncItalico(row.nm_cientifico, true, false) - row.no_arquivo //arquivoAnexo.substring(pathTempDir.length(), arquivoAnexo.length())
                            entryName = entryName.replaceAll(/ /, '_') + '_' + row.sq_ficha_anexo.toString() + '.zip'
                            //println 'Adding  '+entryName+ '  to  '+zipFileName
                            ZipEntry ze = new ZipEntry(entryName)
                            try {
                                zos.putNextEntry(ze)
                                FileInputStream fis = new FileInputStream(arquivoAnexo)
                                int len
                                while ((len = fis.read(buffer)) > 0) {
                                    zos.write(buffer, 0, len)
                                }
                                fis.close()
                            } catch (Exception e) {
                                println e.getMessage()
                            }
                        } else {
                            println ' '
                            println 'Erro - SALVE - exportacao shapefiles fichas - ' + new Date().format('dd/MM/yyyy HH:mm:ss')
                            println 'Arquivo ' + arquivoAnexo + ' não existe - sq_ficha_anexo:' + row.sq_ficha_anexo
                        }
                        if ( job ) {
                            job.stepIt(rowIndex + ' de ' +  job.vlMax + ' (%s)')
                            if( job.stCancelado ) {
                                throw new Exception ('Cancelado pelo usuário')
                            }
                        }

                        //Thread.sleep( 300 )
                    }
                }
            } catch (Exception error) {
                String usuario = exportParams.usuario ? '\n' +
                    exportParams?.usuario?.noPessoa + ' - CPF: ' + Util.formatCpf(exportParams?.usuario?.nuCpf) + ' - ID: ' + exportParams?.usuario?.sqPessoa : ''
                Util.printLog('SALVE - ExportDataService.asyncExportShapefileFicha()', 'Erro ao exportar shapefiles das fichas' + usuario
                    , error.getMessage())
                if ( job ) {
                    // encerrar o job
                    userJobService.error(job.id, error.getMessage() )
                }


            } finally {
                zos.closeEntry()
                zos.close()
                workerService.stepIt(worker)
            }
        }
    }

    /**
     * Depreciado. utilizar o novo metodo gridFichasPlanilha()
     */
    void fichasPlanilha( GrailsHttpSession selfSession, Map workerData ) {
        // variávies
        String noCientifico = ''
        String noAutorTaxon = ''
        String path = grailsApplication.config.temp.dir
        String hora = new Date().format('dd_MM_yyyy_hh_mm_ss')
        String fileName = path + 'salve_exportacao_ficha_' + hora + '.xlsx'
        JSONObject jsonTrilha
        Map worker = [:]
        List listData = []
        // criar array de dados com as colunas da planilha
        try {
            // adicionar worker na sessão do usuário
            worker = workerService.add(selfSession
                    , 'Exportando ' + workerData.fichas.size() + ' ficha(s) para planilha.'
                    , 1
            )
            workerService.setCallback(worker, "window.open(app.url + 'main/download?fileName=" + fileName + "&delete=1','_top');")
            workerService.start(worker)

            if( workerData.fichas.size() == 0 ) {
                throw new Exception('Nenhuma ficha encontrada.')
            }

            // não permitir se já existir uma exportação em andamento
            if( selfSession.exportingData == true ) {
                throw new Exception('Já existe uma exportação em andamento. Aguarde a finalização.');
            }
            selfSession.exportingData=true

            // loop alimentação listData
            workerData.fichas.eachWithIndex { it, index ->
                Map rowData = [:]
                try {
                    jsonTrilha = JSON.parse(it.json_trilha.toString())
                } catch (Exception e) {
                    println e.getMessage()
                    jsonTrilha = [:]
                }

                if (it.nm_cientifico != noCientifico) {
                    // remover o autor do nome cientifico
                    noCientifico = Util.ncItalico(it.nm_cientifico, true, true)
                    noAutorTaxon = it?.no_autor ?: ''
                    workerService.setTitle(worker, it.nm_cientifico)
                }


                rowData['de_ciclo_avaliacao'] = it.de_ciclo_avaliacao + ' de ' + it.nu_ano_ciclo + ' a ' + (it.nu_ano_ciclo.toInteger() + 4i)
                rowData['ds_situacao_ficha']            = it.ds_situacao_ficha ?: ''
                rowData['no_filo']                      = jsonTrilha?.filo?.no_taxon
                rowData['no_classe']                    = jsonTrilha?.classe?.no_taxon
                rowData['no_ordem']                     = jsonTrilha?.ordem?.no_taxon
                rowData['no_familia']                   = jsonTrilha?.familia?.no_taxon
                rowData['nm_cientifico']                = noCientifico
                rowData['no_autor']                     = noAutorTaxon
                rowData['no_comum']                     = it.no_comum ?:''
                rowData['sg_unidade_org']               = it.sg_unidade_org ?:''
                rowData['no_grupo']                     = it?.ds_grupo ?:''
                rowData['no_grupo_salve']               = it?.ds_grupo_salve ?:''
                rowData['nu_ano_ultima_avaliacao_nacional'] = it.nu_ano_aval_nacional ?:''
                rowData['de_categoria_ultima_avaliacao_nacional'] = ( it.ds_categoria_iucn_nacional ? it.ds_categoria_iucn_nacional +' ( ' +it.cd_categoria_iucn_nacional +') ' :'' )
                rowData['de_criterio_ultima_avaliacao_nacional'] = (it.de_criterio_aval_nacional ?:'')
                // dados da oficina dde avaliacao
                rowData['de_categoria_avaliacao']       = (it.ds_categoria_oficina ?:'')
                rowData['de_criterio_avaliacao']        = (it.ds_criterio_oficina ?:'')
                //rowData['tx_justificativa_validacao']       = (it.ds_justificativa_oficina?:'')
                // dados da validacao final
                rowData['de_categoria_validacao']       = (it.ds_categoria_final_completa ?:'')
                rowData['de_criterio_validacao']        = (it.ds_criterio_aval_iucn_final ?:'')
                rowData['tx_justificativa_validacao']   = (it.ds_justificativa_final ? Util.stripTags( it.ds_justificativa_final): '')
                rowData['de_pex_validacao']             = Util.snd(it.st_possivelmente_extinta)
                listData.push( rowData )
            }
            // fim loop

            // criar a planilha
            PlanilhaExcel planilha = new PlanilhaExcel(fileName, 'Fichas Salve', true)
            planilha.setTitle(' ')
            planilha.setData(listData)
            planilha.addColumn(['title': 'Ciclo Avaliação'  , 'column': 'de_ciclo_avaliacao', 'autoFit': true])
            planilha.addColumn(['title': 'Situação'         , 'column': 'ds_situacao_ficha', 'autoFit': true])
            planilha.addColumn(['title': 'Filo'             , 'column': 'no_filo', 'autoFit': true])
            planilha.addColumn(['title': 'Classe'           , 'column': 'no_classe', 'autoFit': true])
            planilha.addColumn(['title': 'Ordem'            , 'column': 'no_ordem', 'autoFit': true])
            planilha.addColumn(['title': 'Família'          , 'column': 'no_familia', 'autoFit': true])
            planilha.addColumn(['title': 'Nome científico'  , 'column': 'nm_cientifico', 'autoFit': true])
            planilha.addColumn(['title': 'Autor'            , 'column': 'no_autor', 'autoFit': true])
            planilha.addColumn(['title': 'Nome comum'       , 'column': 'no_comum', 'autoFit': true])
            planilha.addColumn(['title': 'Unidade'          , 'column': 'sg_unidade_org', 'autoFit': true])
            planilha.addColumn(['title': 'Grupo Avaliado'   , 'column': 'no_grupo', 'autoFit': true])
            planilha.addColumn(['title': 'Grupo'            , 'column': 'no_grupo_salve', 'autoFit': true])
            planilha.addColumn(['title': 'Ano Última Avaliação Nacional'        , 'column': 'nu_ano_ultima_avaliacao_nacional', 'autoFit': true])
            planilha.addColumn(['title': 'Categoria Última Avaliação Nacional'  , 'column': 'de_categoria_ultima_avaliacao_nacional', 'autoFit': true])
            planilha.addColumn(['title': 'Critério Última Avaliação Nacional'   , 'column': 'de_criterio_ultima_avaliacao_nacional', 'autoFit': true])
            planilha.addColumn(['title': 'Categoria Avaliação do Ciclo'         , 'column': 'de_categoria_avaliacao', 'autoFit': true])
            planilha.addColumn(['title': 'Critério Avaliação do Ciclo'          , 'column': 'de_criterio_avaliacao', 'autoFit': true])
            planilha.addColumn(['title': 'Categoria Validação no Ciclo'         , 'column': 'de_categoria_validacao', 'autoFit': true])
            planilha.addColumn(['title': 'Critério Validação no Ciclo'          , 'column': 'de_criterio_validacao', 'autoFit': true])
            planilha.addColumn(['title': 'Justificativa Validação do Ciclo'     , 'column': 'tx_justificativa_validacao', 'autoFit': true])
            planilha.addColumn(['title': 'PEX Final do Ciclo'                   , 'column': 'de_pex_validacao', 'autoFit': true])
            planilha.setWorker( worker, workerService )
            if ( ! planilha.run()) {
                throw new Exception(planilha.getError())
            }
            workerService.stepIt(worker)
        } catch (Exception e ) {
            workerService.setMessage(worker,e.getMessage())
            workerService.setCallback(worker,'')
        }
        // se ainda existir a sessão do usuário
        try {
            selfSession.exportingData = false
        } catch( Exception e) {}
    }

    /**
     * Exportar as pendências das fichas para planilha
     * @param selfSession
     * @param workerData
     */
    void asyncExportPendenciaFicha( Map exportParams ) {
        runAsync {
            String noCientifico = ''
            String noAutorTaxon = ''
            String horaInicio = new Date().format('dd/MM/yyyy HH:mm:ss')
            String urlSalve = grailsApplication.config.url.sistema ?: 'https://salve.icmbio.gov.br/salve-estadual/'
            String path = grailsApplication.config.temp.dir
            String hora = new Date().format('dd_MM_yyyy_hh_mm_ss')
            String tempFileName = path + 'salve_exportacao_pendencias_ficha_' + hora + '.xlsx'
            String linkDownload = '<a target="_blank" href="' + urlSalve + 'download?fileName=' + tempFileName + '"><b>AQUI</b></a>'

            JSONObject jsonTrilha
            List listData = []
            UserJobs job
            if( ! exportParams.idsfichas && exportParams.cmdSql ){
                List rows = sqlService.execSql(exportParams.cmdSql + ' OFFSET 0 LIMIT ' + (exportParams.maxFichas + 1), exportParams.sqlParams)
                // ler somente a coluna sq_ficha do resultado
                if ( rows ) {
                    exportParams.idsFichas = rows.sq_ficha
                }
            }

            // 1º) CRIAR PARAMETROS DO JOB
            Map jobParams = [usuario      : PessoaFisica.get(exportParams.usuario.sqPessoa)
                             , deRotulo   : exportParams.jobRotulo ?: 'Exportação das pendências das fichas'
                             , deEmail    : exportParams.email ?: null
                             , deMensagem : ''
                             , vlAtual    : 0

            ]

            if (jobParams.deEmail) {
                jobParams.deEmailAssunto = exportParams.emailAssunto ?: 'SALVE - Exportação pendências fichas'
                jobParams.deMensagem = """<p>Segue o link para baixar a planilha com os dados exportados em ${horaInicio}.</p>
                                <p>Clique ${linkDownload} para baixar o arquivo. Este link ficará disponível até o dia ${(new Date() + 5).format('dd/MM/yyyy')}).</p>
                                <p>Atenciosamente,<br>
                                <b>Equipe SALVE</b></p>"""
            }

            // CRIAR O JOB NO BANCO DE DADOS
            jobParams.noArquivoAnexo = tempFileName
            job = userJobService.createJob(jobParams).save(flush: true)

            // criar array de dados com as colunas da planilha
            try {
                if (exportParams.idsFichas.size() == 0) {
                    throw new Exception('Nenhuma ficha encontrada.')
                }

                // query
                String cmdSql = """select taxon.json_trilha,taxon.no_autor, ficha.nm_cientifico, ficha_pendencia.dt_pendencia,
                    ficha_pendencia.tx_pendencia, vw_pessoa.no_pessoa
                    from salve.ficha_pendencia
                    inner join salve.ficha on ficha.sq_ficha = ficha_pendencia.sq_ficha
                    inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                    left outer join salve.vw_pessoa on vw_pessoa.sq_pessoa = ficha_pendencia.sq_usuario
                    where ficha.sq_ficha = any( array[${exportParams.idsFichas.join(',')}] )
                    and ficha_pendencia.st_pendente='S'
                    order by ficha.nm_cientifico, ficha_pendencia.dt_pendencia, vw_pessoa.no_pessoa"""

                // loop alimentação listData
                sqlService.execSql(cmdSql).eachWithIndex { row, index ->
                    Map rowData = [:]
                    try {
                        jsonTrilha = JSON.parse(row.json_trilha.toString())
                    } catch (Exception e) {
                        println e.getMessage()
                        jsonTrilha = [:]
                    }

                    if (row.nm_cientifico != noCientifico) {
                        // remover o autor do nome cientifico
                        noCientifico = Util.ncItalico(row.nm_cientifico, true, true)
                        noAutorTaxon = row?.no_autor ?: ''
                    }
                    rowData['no_filo'] = jsonTrilha?.filo?.no_taxon
                    rowData['no_classe'] = jsonTrilha?.classe?.no_taxon
                    rowData['no_ordem'] = jsonTrilha?.ordem?.no_taxon
                    rowData['no_familia'] = jsonTrilha?.familia?.no_taxon
                    rowData['nm_cientifico'] = noCientifico
                    rowData['no_autor_taxon'] = noAutorTaxon
                    rowData['dt_pendencia'] = row.dt_pendencia ? row.dt_pendencia.format('dd/MM/yyyy') : ''
                    rowData['no_pessoa'] = row.no_pessoa ?: ''

                    // trocar <li> para \n para manter a quebra de linha
                    rowData['tx_pendencia'] = row.tx_pendencia.replaceAll(/<li>/, '\n');
                    rowData['tx_pendencia'] = Util.stripTagsKeepLineFeed(rowData['tx_pendencia'])
                    listData.push(rowData)
                }
                // fim loop

                // criar a planilha
                PlanilhaExcel planilha = new PlanilhaExcel(tempFileName, 'Pendências Fichas', true)
                planilha.setTitle('Exportação de Pendências Fichas - SALVE')
                planilha.setData(listData)
                planilha.addColumn(['title': 'Filo', 'column': 'no_filo', 'autoFit': true])
                planilha.addColumn(['title': 'Classe', 'column': 'no_classe', 'autoFit': true])
                planilha.addColumn(['title': 'Ordem', 'column': 'no_ordem', 'autoFit': true])
                planilha.addColumn(['title': 'Família', 'column': 'no_familia', 'autoFit': true])
                planilha.addColumn(['title': 'Nome científico', 'column': 'nm_cientifico', 'autoFit': true])
                planilha.addColumn(['title': 'Autor', 'column': 'no_autor_taxon', 'autoFit': true])
                planilha.addColumn(['title': 'Data', 'column': 'dt_pendencia', 'autoFit': true])
                planilha.addColumn(['title': 'Registrada por', 'column': 'no_pessoa', 'autoFit': true])
                planilha.addColumn(['title': 'Pendência', 'column': 'tx_pendencia', 'autoFit': true])
                if ( !planilha.run( job ) ) {
                    throw new Exception(planilha.getError())
                }
            } catch (Exception error ) {
                String usuario = exportParams.usuario ? '\n' +
                    exportParams?.usuario?.noPessoa + ' - CPF: ' + Util.formatCpf(exportParams?.usuario?.nuCpf) + ' - ID: ' + exportParams?.usuario?.sqPessoa : ''
                Util.printLog('SALVE - ExportDataService.fichasPendenciasPlanilha()', 'Erro ao exportar pendências das fichas' + usuario
                    , error.getMessage())
                if ( job ) {
                    // encerrar o job
                    userJobService.error(job.id, error.getMessage() )
                }
            }
        }
    }

    /**
     * INTEGRAÇÃO SIS/IUCN
     * Exportar o gride de tradução de fichas para planilha
     * @param selfSession
     * @param workerData
     */
    void gridTraducaoFicha( GrailsHttpSession selfSession, Map workerData ){
        String horaInicio= new Date().format('dd/MM/yyyy HH:mm:ss')
        String path = grailsApplication.config.temp.dir
        String hora = new Date().format('dd_MM_yyyy_hh_mm_ss')
        String fileName = path + 'salve_exportacao_traducao_ficha_' + hora + '.xlsx'
        //JSONObject jsonTrilha
        Map worker = [:]
        List listData = []

        // não permitir se já existir uma exportação em andamento
        /*if( selfSession.exportingData == true ) {
            throw new Exception('Já existe uma exportação em andamento. Aguarde a finalização.');
        }
        selfSession.exportingData=true
         */

        debug=false
        d('Inicio exportacao gride traducao fichas')
        d(' - arquivo:' + fileName );

        // criar array de dados com as colunas da planilha
        try {
            if( workerData.rows.size() == 0 ) {
                throw new Exception('Nenhuma ficha encontrada.')
            }
            // adicionar worker na sessão do usuário
            worker = workerService.add(selfSession
                , 'Exportando gride tradução de ' + workerData.rows.size() + ' ficha(s) para planilha.'
                , 1
            )
            workerService.setCallback(worker, "window.open(app.url + 'main/download?fileName=" + fileName + "&delete=1','_top');")
            workerService.start(worker)

            // loop alimentação listData
            workerData.rows.each { row ->
                Map rowData = [:]
                // remover o autor do nome cientifico
                rowData['nm_cientifico']             = Util.ncItalico(row?.nm_cientifico, true, true)
                rowData['nu_percentual_traduzido']   = row.nu_percentual_traduzido ?: ''
                rowData['nu_percentual_revisado']    = row.nu_percentual_revisado ?: ''
                rowData['no_revisor']                = row?.no_revisor?.split(';')?.collect{ Util.nomeAbreviado( it ) }?.join("\n")
                rowData['no_unidade_org']            = row?.sg_unidade_org ?:''
                rowData['ds_situacao_ficha']         = row?.ds_situacao_ficha ?:''
                listData.push( rowData )
            }
            // fim loop

            // criar a planilha
            PlanilhaExcel planilha = new PlanilhaExcel(fileName, 'Pendências Fichas', true)
            planilha.setTitle('Exportação Tradução Fichas - SALVE')
            planilha.setData(listData)
            planilha.addColumn(['title': 'Nome científico'  , 'column': 'nm_cientifico', 'autoFit': true])
            planilha.addColumn(['title': 'Traduzido (%)'    , 'column': 'nu_percentual_traduzido', 'autoFit': true, 'type':'integer'])
            planilha.addColumn(['title': 'Revisado (%)'     , 'column': 'nu_percentual_revisado', 'autoFit': true])
            planilha.addColumn(['title': 'Revisor'          , 'column': 'no_revisor', 'autoFit': true, width:35])
            planilha.addColumn(['title': 'Situação da Ficha', 'column': 'ds_situacao_ficha', 'autoFit': true])
            if (!planilha.run()) {
                throw new Exception(planilha.getError())
            }
            workerService.stepIt(worker)

            // enviar email
            if( workerData.email) {
                // zipar o arquivo antes de anexa-lo ao e-mail
                Map zipData = Util.zipFile( fileName )
                if( zipData.zipFileName ){
                    fileName = zipData.zipFileName
                }
                emailService.sendTo( workerData.email.toString(),'SALVE - Resultado da exportação de dados'
                    ,"""<p>Segue anexo a planilha com resultado da exportação realizada em ${horaInicio}.</p>
                                <p>Caso não tenha recebido o arquivo em anexo, clique <a target="_blank" href="https://salve.icmbio.gov.br/salve-estadual/download?fileName=${fileName}">AQUI</a> para baixar a planilha. Este link ficará disponível até o dia ${(new Date()+5).format('dd/MM/yyyy')}).</p>
                                <p>Atenciosamente,<br>
                                <b>Equipe SALVE</b></p>"""
                    ,'',[fullPath:fileName])
            }

        } catch (Exception e ) {
            Util.printLog('SALVE - exportacao gride fichas traduzidas','Erro ao exportar o gride de fichas traduzidas',e.getMessage())
            //println e.getMessage()
            workerService.setCallback(worker,'')
            workerService.setMessage(worker,e.getMessage())
        }
        // se ainda existir a sessão do usuário
        try {
            //selfSession.exportingData = false
        } catch( Exception e) {}
    }

    /**
     * método genérico para para exportar dados para planilha
     * Recebe as rows e as cols e cria a planilha
     * @exemplo de utilização:
         if( outputFormat == 'CSV' ) {
             // criar variavel local selfSession para utilzar workers
             GrailsHttpSession selfSession = session
             Map workerData = [sqPessoa   : session?.sicae?.user?.sqPessoa
             , email    : params.email
             , rows     : dadosGride
             , columns   :[
             [title:'Nome Científico'       ,fieldName:'nm_cientifico',autoFit:true],
             [title:'Grupo'                 ,fieldName:'ds_grupo',autoFit:true],
             [title:'Categoria Avaliada'    ,fieldName:'cd_categoria_avaliada',autoFit:true],
             [title:'Critério avaliada'     ,fieldName:'ds_criterio_avaliado',autoFit:true],
             [title:'Validadores'           ,fieldName:'no_validadores',autoFit:true],
             [title:'Respostas'             ,fieldName:'ds_respostas',autoFit:true],
             [title:'Situação ficha'        ,fieldName:'ds_situacao_ficha',autoFit:true],
             [title:'Categoria validada'    ,fieldName:'cd_categoria_final',autoFit:true],
             [title:'Critério validado'     ,fieldName:'ds_criterio_aval_iucn_final',autoFit:true],
             [title:'Justificativa final'   ,fieldName:'ds_justificativa_final',autoFit:false],
             [title:'Data validação'        ,fieldName:'dt_aceite_validacao_formatada',autoFit:true],
             [title:'Unidade responsável'   ,fieldName:'sg_unidade_org',autoFit:true],
             ]

             ]
             result.msg = 'A exportação está em andamento.<br><br>Ao finalizar será ' +
             (params.email ? 'enviado para o email <b>' + params.email + '</b> o link para baixar a planilha.' : 'exibida a tela para baixar a planilha.') +
             '<br><br>Dependendo da quantidade de registros este procedimento poderá levar alguns minutos.'
             runAsync {
                 exportDataService.exportToPlanilha(selfSession, workerData)
             }
         }
     */
    void exportToPlanilha( GrailsHttpSession selfSession, Map workerData ) {

        // variávies
        String horaInicio= new Date().format('dd/MM/yyyy HH:mm:ss')
        String path = grailsApplication.config.temp.dir
        String hora = new Date().format('dd_MM_yyyy_hh_mm_ss')
        String fileName = path + 'salve_exportacao_' + hora + '.xlsx'
        Map worker = [:]
        List listData = []
        try {
            // adicionar worker na sessão do usuário
            worker = workerService.add(selfSession
                , 'Exportando ' + workerData.rows.size() + ' registros para planilha.'
                , 1
            )
            workerService.setCallback(worker, "window.open(app.url + 'main/download?fileName=" + fileName + "&delete=1','_top');")
            workerService.start(worker)

            if( workerData.rows.size() == 0 ) {
                throw new Exception('Nenhuma registro para ser exportado.')
            }

            // não permitir se já existir uma exportação em andamento
            if( selfSession.exportingData == true ) {
                throw new Exception('Já existe uma exportação em andamento. Aguarde a finalização.');
            }
            selfSession.exportingData=true
            if( ! workerData.columns ) {
                workerData.columns = []
                listData[0].keySet().each{ fieldName ->
                    workerData.columns.push(['title':fieldName,'fileName':fileName])
                }
            }

            // criar a planilha
            PlanilhaExcel planilha = new PlanilhaExcel(fileName, 'Exportação', true)
            planilha.setTitle(' ')
            planilha.setData(workerData.rows)
            workerData.rows=[] // liberar memoria
            // adicionar as colunas
            workerData.columns.each{ column ->
                boolean autoFit = column?.autoFit ? true : false;
                planilha.addColumn(['title': column.title  , 'column': column.fieldName, 'autoFit': autoFit])
            }
            planilha.setWorker( worker, workerService )
            if ( ! planilha.run()) {
                throw new Exception(planilha.getError())
            }
            workerService.stepIt(worker)

            if( workerData.email) {
                // zipar o arquivo antes de anexa-lo ao e-mail
                Map zipData = Util.zipFile(fileName)
                if(  zipData.zipFileName ){
                    fileName = zipData.zipFileName
                }
                emailService.sendTo( workerData.email.toString(),'SALVE - Resultado da exportação de dados'
                    ,"""<p>Segue anexo a planilha com resultado da exportação realizada em ${horaInicio}.</p>
                                <p>Caso não tenha recebido o arquivo em anexo, clique <a target="_blank" href="https://salve.icmbio.gov.br/salve-estadual/download?fileName=${fileName}">AQUI</a> para baixar a planilha. Este link ficará disponível até o dia ${(new Date()+5).format('dd/MM/yyyy')}).</p>
                                <p>Atenciosamente,<br>
                                <b>Equipe SALVE</b></p>"""
                    ,'',[fullPath:fileName])
            }


        } catch (Exception e ) {
            workerService.setMessage(worker,e.getMessage())
            workerService.setCallback(worker,'')
        }
        // se ainda existir a sessão do usuário
        try {
            selfSession.exportingData = false
        } catch( Exception e) {}
    }

    /**
     * criar a lista de referencias bibliograficas a partir do array retornado pelo postgres
     * @Example: "{ ""679102"" : {""de_ref_bib"" : ""Rocha, C.F.D.; Vrcibradic, D.; Menezes, V.A. & Ariani, C.V., 2009. Ecology and natural history of the easternmost native lizard species in South America, <i>Trachylepis atlantica</i> (Scincidae), from the Fernando de Noronha Archipelago. Journal of Herpetology, 43 (3): p.450-459."", ""no_autores_publicacao"" : ""Rocha, C.F.D.; Vrcibradic, D.; Menezes, V.A.; Ariani, C.V."", ""nu_ano_publicacao"" : 2009, ""no_autor"" : null, ""nu_ano"" : null} }"
     *           "{ ""1953946"" : {""de_ref_bib"" : null, ""no_autores_publicacao"" : null, ""nu_ano_publicacao"" : null, ""no_autor"" : ""Barbosa, Luis."", ""nu_ano"" : 2021}, ""1076908"" : {""de_ref_bib"" : ""Lyra, M.L. & Vences, M., 2018. Preliminary assessment of mitochondrial variation in the insular endemic, biogeographically enigmatic Noronha skink, Trachylepis atlantica (Squamata: Scincidae). Salamandra, 54 (3): p.229-232."", ""no_autores_publicacao"" : ""Lyra, M.L.; Vences, M."", ""nu_ano_publicacao"" : 2018, ""no_autor"" : null, ""nu_ano"" : null} }"
     * @param jsonRefBibs
     * @return
     */
    protected _parseRefBibPlanilha(String jsonRefBibs = '' ){
        if( !jsonRefBibs ){
            return ''
        }
        List refBibs = []
        try {
            def dadosJson = JSON.parse(jsonRefBibs)
            dadosJson.each { key, value ->
                if( value.de_ref_bib ){
                    refBibs.push( value.de_ref_bib )
                }
                // comunicacao pessoal
                if( value.nu_ano && value.no_autor ){
                    refBibs.push( Util.formatarComunicacaoPessoal( value.no_autor, value.nu_ano ) )
                }
            }
            refBibs.sort{ Util.removeAccents(it).toLowerCase() }
            return refBibs.join("\n")
        } catch( Exception e ){
            println ' '
            println 'ERRO SALVE - exportDataService._parseRefbibPlanilha - '+ new Date().format('dd/MM/yyyy HH:mm:ss')
            println e.getMessage()
            println 'jsonRefBib Recebido'
            println jsonRefBibs

        }

    }


    /**
     * NOVO método assincrono para exportação dos registro de ocorrencias
     * utilizando a tabela user_jobs
     *
     * @param config
     */
    void asyncExportarOcorrencias( Map exportParams ) {
        runAsync {
            Map emailVars =[ data        : new Date().format('dd/MM/yyyy HH:mm:ss')
                            ,dataLimite  : (new Date()+5).format('dd/MM/yyyy')
                            ,linkDownload: '' ]
            String cmdSql
            UserJobs job
            List autores = []
            Long cacheSeconds = 240l
            PlanilhaExcel planilha
            boolean acessoExterno = ( ! exportParams?.usuario ) // se não tiver usuario logado assumir que o acesso é usuario externo
            String path = grailsApplication.config.temp.dir
            String horaArquivo = new Date().format('dd_MM_yyyy_HH_mm_ss')
            String tempFilename = path + 'salve_exportacao_registros_' + horaArquivo + '.xlsx'
            String urlSalve = grailsApplication.config.url.sistema ?:'https://salve.icmbio.gov.br/salve-estadual/'

            try {

                // 1º) CRIAR PARAMETROS DO JOB
                Map jobParams = [ usuario  : PessoaFisica.get( exportParams.usuario.sqPessoa )
                                 ,deRotulo : exportParams.jobRotulo ?: 'Exportação de registros relatório táxon analítico'
                                 ,deEmail  : exportParams.email ?: null
                                 ,deEmailAssunto : exportParams.emailAssunto ?: 'SALVE - Exportação de registros finalizada'
                                 ,deMensagem : ''

                ]
                // SE FOR INFORMADO E-MAIL FORMATAR AS MENSAGENS
                if( jobParams.deEmail ) {
                    String mensagemEmailPadrao = """<p>SALVE - Exportação de registros</p>
                                       <p>Segue anexo a planilha com os registros de ocorrências solicitados em {data}.</p>
                                       <p>Caso não tenha recebido o arquivo em anexo, clique {linkDownload} para baixá-lo.</p>
                                       <p><span style="color:#ff0000;">Este link ficará disponível até o dia {dataLimite}.</span></p>
                                       <p>Atenciosamente,<br>
                                       <b>Equipe SALVE</b></p>"""
                    jobParams.deMensagem = exportParams.emailMensagem ?: mensagemEmailPadrao
                    emailVars.linkDownload = """<a target="_blank" href="https://salve.icmbio.gov.br/salve-estadual/download?fileName=${tempFilename}">AQUI</a>"""
                } else {
                    //String mensagemEmailPadrao = ''//'Clique {linkDownload} para baixar o arquivo'
                    //jobParams.deMensagem = exportParams.emailMensagem ?: mensagemEmailPadrao
                    //emailVars.linkDownload ='<a target="_self" href="'+urlSalve+'downloadWithProgress?fileName='+tempFilename+'"><b>AQUI</b></a>'
                    jobParams.deMensagem = '' // o download será automático ao terminar o job
                }
                if( jobParams.deMensagem) {
                    emailVars.each { key, value ->
                        Pattern pattern = Pattern.compile('\\{' + key + '\\}')
                        jobParams.deMensagem = jobParams.deMensagem.toString().replaceAll(pattern, value)
                    }
                }

                // CRIAR O JOB NO BANCO DE DADOS
                jobParams.noArquivoAnexo = tempFilename
                job = userJobService.createJob( jobParams ).save( flush:true )

                // 2º) EXECUTAR SQL PARA RECUPERAR OS IDS DAS FICHAS
                List idsFichas = []
                if( !exportParams.ids && exportParams.cmdSql && exportParams.maxFichas ) {
                    exportParams.cmdSql += ' OFFSET 0 LIMIT ' + (exportParams.maxFichas + 1)
                    job.deAndamento ='Consultando fichas...'
                    job.save(flush:true)
                    idsFichas = sqlService.execSqlCache(exportParams.cmdSql, exportParams.sqlParams,cacheSeconds)?.sq_ficha
                } else {
                    idsFichas = exportParams.ids.split(',')
                }
                if (!idsFichas) {
                    throw new Exception('Nenhuma ficha encontrada')
                }

                if (idsFichas.size() > exportParams.maxFichas) {
                    throw new Exception('A quantidade de fichas para exportação dos registros excedeu o limite de ' + exportParams.maxFichas.toString() + ' fichas')
                }


                // 3º) SELECIONAR OS REGISTROS DE OCORRENCIA
                List sqlWhere = []
                if( exportParams.utilizadoAvaliacao == 'S' ) {
                    sqlWhere.push("mv.cd_situacao_avaliacao = 'REGISTRO_UTILIZADO_AVALIACAO'")
                } else if( exportParams.utilizadoAvaliacao == 'N' ) {
                    sqlWhere.push("mv.cd_situacao_avaliacao = 'REGISTRO_NAO_UTILIZADO_AVALIACAO'")
                } else if( exportParams.adicionadosAposAvaliacao ) {
                    sqlWhere.push("mv.cd_situacao_avaliacao = 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO'")
                }
                if( exportParams.excluirSensiveis == 'S' || acessoExterno ) {
                    sqlWhere.push("mv.in_sensivel <> 'S'")
                }
                if( exportParams.excluirCarencia == 'S' || acessoExterno ) {
                    sqlWhere.push("( mv.dt_carencia is null or mv.dt_carencia < now() )")
                }
                if(  idsFichas.size() > 1 ) {
                    // para varias fichas ler da visão meterializada
                    cmdSql = """with taxons as (WITH especies AS (SELECT ficha.sq_taxon, ficha.sq_ciclo_avaliacao
                            FROM salve.ficha
                            WHERE ficha.sq_ficha = ANY (ARRAY [${idsFichas.join(',')}])),
                            subespecie AS (
                                 SELECT taxon.sq_taxon, especies.sq_ciclo_avaliacao
                                   FROM taxonomia.taxon
                                   INNER JOIN especies ON especies.sq_taxon = taxon.sq_taxon_pai
                            )
                            SELECT sq_taxon, sq_ciclo_avaliacao
                            FROM especies
                            UNION
                            SELECT sq_taxon, sq_ciclo_avaliacao
                            FROM subespecie
                )
                select mv.* from salve.mv_registros as mv
                inner join taxons on taxons.sq_taxon = mv.sq_taxon
                where mv.sq_ciclo_avaliacao = taxons.sq_ciclo_avaliacao
                ${sqlWhere ? ' and ' + sqlWhere.join('\nand ') : ''}
                """
                }
                else {
                    // para apenas 1 ficha não utilizar o cache e ler os dados on-line
                    cacheSeconds=0
                    cmdSql = """select mv.* from salve.fn_ficha_registros(${idsFichas[0]}) as mv
                    ${sqlWhere ? ' where ' + sqlWhere.join('\nand ') : ''}
                    """
                }
                job.deAndamento ='Lendo registros de ' + idsFichas.size() + ' ficha(s)'
                job.save(flush:true)
/** /
println ' '
println 'EXPORTAR REGISTROS'
println 'Cache seconds:' +cacheSeconds
println cmdSql
/**/

                List rows = sqlService.execSqlCache(cmdSql,[:], cacheSeconds)
                if ( !rows || rows.size() == 0 ) {
                    throw new Exception('Nenhuma ocorrencia encontrada.')
                }
                job.vlMax           = rows.size()
                job.deAndamento     = '0 de '+job.vlMax+' registro(s)' // adicionar %s para exibir o percentual executado
                job.vlAtual         = 0
                job.save(flush:true)

                // 4º FAZER PRE-PROCESSAMENTO PARA AJUSTAR O RESULTADO DE ALGUMAS COLUNAS ANTES DE ENVIAR PARA A PLANILHA
                rows.eachWithIndex {  row, int rowNum ->
                    row.no_taxon = Util.ncItalico((row?.no_subespecie ?: row.no_especie), true)
                    if (row.cd_situacao_avaliacao == 'REGISTRO_UTILIZADO_AVALIACAO') {
                        row.tx_justificativa_nao_usado_avaliacao = ''
                        row.ds_motivo_nao_utilizado = ''
                    }
                    row.tx_justificativa_nao_usado_avaliacao = row.tx_justificativa_nao_usado_avaliacao.toString().replaceAll(/<br\/?>/, '\n')
                    row['no_cientifico'] = row.no_taxon
                    row['nu_latitude'] = (row?.nu_latitude ? row.nu_latitude.toString().replaceAll(/\./, ',') : '')
                    row['nu_longitude'] = (row?.nu_longitude ? row.nu_longitude.toString().replaceAll(/\./, ',') : '')
                    row['ds_pan'] = (row.ds_pan ? row.ds_pan.replaceAll('Plano de Ação Nacional', 'PAN') : '')
                    row['de_prazo_carencia'] = (row?.dt_carencia ? row.dt_carencia.format('dd/MM/yyyy') : '')
                    row['tx_metodologia_aproximacao'] = row?.tx_metodologia_aproximacao ? cleanHtml(row.tx_metodologia_aproximacao.toString()) : ''
                    row['ds_caracteristica_localidade'] = row?.ds_caracteristica_localidade ? cleanHtml(row?.ds_caracteristica_localidade) : ''
                    row['dt_identificacao'] = (row?.dt_identificacao ? row.dt_identificacao.format('dd/MM/yyyy') : '')
                    row['dt_compilacao'] = (row?.dt_compilacao ? row?.dt_compilacao.format('dd/MM/yyyy') : '')
                    row['dt_revisao'] = (row?.dt_revisao ? row?.dt_revisao.format('dd/MM/yyyy') : '')
                    row['dt_validacao'] = (row?.dt_validacao ? row?.dt_validacao.format('dd/MM/yyyy') : '')
                    row['tx_ref_bib'] = _parseRefBibPlanilha(row.tx_ref_bib);
                }

                // 5º CRIAR INSTANCIA DA PLANILHA E DEFINIR AS COLUNAS
                planilha = new PlanilhaExcel(tempFilename, 'Ocorrências',true)
                planilha.addColumn(['title': 'Reino', 'column': 'no_reino', 'autoFit': true]) //  , color:'#ff0000', 'bgColor': '#ffffcc',align:'center'] )
                planilha.addColumn(['title': 'Filo', 'column': 'no_filo', 'autoFit': true ])
                planilha.addColumn(['title': 'Classe', 'column': 'no_classe', 'autoFit': true])
                planilha.addColumn(['title': 'Ordem', 'column': 'no_ordem', 'autoFit': true])
                planilha.addColumn(['title': 'Família', 'column': 'no_familia', 'autoFit': true])
                planilha.addColumn(['title': 'Gênero', 'column': 'no_genero', 'autoFit': true])
                planilha.addColumn(['title': 'Nome Científico', 'column': 'no_cientifico', 'autoFit': true])
                planilha.addColumn(['title': 'Autor', 'column': 'no_autor_taxon', 'autoFit': true])
                planilha.addColumn(['title': 'Latitude', 'column': 'nu_latitude', 'autoFit': true])
                planilha.addColumn(['title': 'Longitude', 'column': 'nu_longitude', 'autoFit': true])
                planilha.addColumn(['title': 'Endêmica Brasil', 'column': 'de_endemica_brasil', 'autoFit': true])
                planilha.addColumn(['title': 'Categoria Anterior', 'column': 'ds_categoria_anterior', 'autoFit': true])
                planilha.addColumn(['title': 'Categoria Avaliada', 'column': 'ds_categoria_avaliada', 'autoFit': true])
                planilha.addColumn(['title': 'Categoria Validada', 'column': 'ds_categoria_validada', 'autoFit': true])
                planilha.addColumn(['title': 'Plano de Ação Nacional', 'column': 'ds_pan', 'autoFit': true])
                planilha.addColumn(['title': 'Atitude (m)', 'column': 'nu_altitude', 'autoFit': true])
                planilha.addColumn(['title': 'Sensível?', 'column': 'ds_sensivel', 'autoFit': true])
                planilha.addColumn(['title': 'Situação do registro', 'column': 'ds_situacao_avaliacao', 'autoFit': true])
                planilha.addColumn(['title': 'Motivo (quando não utilizado)', 'column': 'ds_motivo_nao_utilizado', 'autoFit': true])
                planilha.addColumn(['title': 'Justificativa (quando não utilizado)', 'column': 'tx_justificativa_nao_usado_avaliacao', 'autoFit': true])
                planilha.addColumn(['title': 'Prazo de carência', 'column': 'de_prazo_carencia', 'autoFit': true])
                planilha.addColumn(['title': 'Datum', 'column': 'no_datum', 'autoFit': true])
                planilha.addColumn(['title': 'Formato Original', 'column': 'no_formato_original', 'autoFit': true])
                planilha.addColumn(['title': 'Precisão da Coordenada', 'column': 'no_precisao', 'autoFit': true])
                planilha.addColumn(['title': 'Referência de Aproximação', 'column': 'no_referencia_aproximacao', 'autoFit': true])
                planilha.addColumn(['title': 'Metodologia der Aproximação', 'column': 'no_metodologia_aproximacao', 'autoFit': true])
                planilha.addColumn(['title': 'Descrição Metodo de aproximação', 'column': 'tx_metodologia_aproximacao', 'autoFit': true])
                planilha.addColumn(['title': 'Taxon Originalmente citado', 'column': 'no_taxon_citado', 'autoFit': true])
                planilha.addColumn(['title': 'Presença atual ', 'column': 'ds_presenca_atual', 'autoFit': true])
                planilha.addColumn(['title': 'Tipo de registro', 'column': 'no_tipo_registro', 'autoFit': true])
                planilha.addColumn(['title': 'Dia', 'column': 'nu_dia_registro', 'autoFit': true])
                planilha.addColumn(['title': 'Mês', 'column': 'nu_mes_registro', 'autoFit': true])
                planilha.addColumn(['title': 'Ano', 'column': 'nu_ano_registro', 'autoFit': true])
                planilha.addColumn(['title': 'Hora', 'column': 'hr_registro', 'autoFit': true])
                planilha.addColumn(['title': 'Dia fim', 'column': 'nu_dia_registro_fim', 'autoFit': true])
                planilha.addColumn(['title': 'Mês fim', 'column': 'nu_mes_registro_fim', 'autoFit': true])
                planilha.addColumn(['title': 'Ano fim', 'column': 'nu_ano_registro_fim', 'autoFit': true])
                planilha.addColumn(['title': 'Data e Ano desconhecidos', 'column': 'ds_data_desconhecida', 'autoFit': true])
                planilha.addColumn(['title': 'Localidade', 'column': 'no_localidade', 'autoFit': true])
                planilha.addColumn(['title': 'Característica da localidade', 'column': 'ds_caracteristica_localidade', 'autoFit': true])
                planilha.addColumn(['title': 'UC Federais', 'column': 'no_uc_federal', 'autoFit': true])
                planilha.addColumn(['title': 'UC Não Federais', 'column': 'no_uc_estadual', 'autoFit': true])
                planilha.addColumn(['title': 'RPPN', 'column': 'no_rppn', 'autoFit': true])
                planilha.addColumn(['title': 'País', 'column': 'no_pais', 'autoFit': true])
                planilha.addColumn(['title': 'Estado', 'column': 'no_estado', 'autoFit': true])
                planilha.addColumn(['title': 'Município', 'column': 'no_municipio', 'autoFit': true])
                planilha.addColumn(['title': 'Ambiente', 'column': 'no_ambiente', 'autoFit': true])
                planilha.addColumn(['title': 'Bioma', 'column': 'no_bioma', 'autoFit': true, width:50])
                planilha.addColumn(['title': 'Hábitat', 'column': 'no_habitat', 'autoFit': true])
                planilha.addColumn(['title': 'Elevação mínima', 'column': 'vl_elevacao_min', 'autoFit': true])
                planilha.addColumn(['title': 'Elevação máxima', 'column': 'vl_elevacao_max', 'autoFit': true])
                planilha.addColumn(['title': 'Profundidade mínima', 'column': 'vl_profundidade_min', 'autoFit': true])
                planilha.addColumn(['title': 'Profundidade máxima', 'column': 'vl_profundidade_max', 'autoFit': true])
                planilha.addColumn(['title': 'Identificador', 'column': 'de_identificador', 'autoFit': true])
                planilha.addColumn(['title': 'Data identificação', 'column': 'dt_identificacao', 'autoFit': true])
                planilha.addColumn(['title': 'Identificação incerta', 'column': 'no_identificacao_incerta', 'autoFit': true])
                planilha.addColumn(['title': 'Tombamento', 'column': 'ds_tombamento', 'autoFit': true])
                planilha.addColumn(['title': 'Instituição tombamento', 'column': 'ds_instituicao_tombamento', 'autoFit': true])
                planilha.addColumn(['title': 'Compilador', 'column': 'no_compilador', 'autoFit': true])
                planilha.addColumn(['title': 'Data compilação', 'column': 'dt_compilacao', 'autoFit': true])
                planilha.addColumn(['title': 'Revisor', 'column': 'no_revisor', 'autoFit': true])
                planilha.addColumn(['title': 'Data revisão', 'column': 'dt_revisao', 'autoFit': true])
                planilha.addColumn(['title': 'Validador', 'column': 'no_validador', 'autoFit': true])
                planilha.addColumn(['title': 'data validação', 'column': 'dt_validacao', 'autoFit': true])
                planilha.addColumn(['title': 'Base de Dados', 'column': 'no_base_dados', 'autoFit': true])
                planilha.addColumn(['title': 'Id Origem', 'column': 'id_origem', 'autoFit': true])
                planilha.addColumn(['title': 'Nº Autorização SISBIO', 'column': 'nu_autorizacao', 'autoFit': true, width:40])
                planilha.addColumn(['title': 'Autor registro', 'column': 'no_autor_registro', 'autoFit': true])
                planilha.addColumn(['title': 'Projeto' , 'column': 'no_projeto', 'autoFit': true, width:80])
                planilha.addColumn(['title': 'Observação', 'column': 'tx_observacao', 'autoFit': true])
                planilha.addColumn(['title': 'Referência bibliográfica', 'column': 'tx_ref_bib', 'autoFit': true, width:80])

                // EXPORTAR SOMENTE AS COLUNAS SELECIONADAS
                if( exportParams.colunasSelecionadas ) {
                    List colsSelected = exportParams.colunasSelecionadas.split(',');
                    planilha.getColumns().each{
                        it.visible = colsSelected.contains( it.column )
                    }
                }

                planilha.setTitle(' ') // não imprimir titulo
                planilha.setData( rows )
                // TODO - LER NOMES DOS AUTORES VINDOS NO CAMPO tx_ref_bib.no_autores_publicacao para criar a citação
                planilha.setCitation(autores.join('; ') )
                if ( ! planilha.run(job) ) {
                    throw new Exception(planilha.getError() )
                }
            } catch (Exception error) {
                String usuario = exportParams.usuario ? '\n' +
                    exportParams?.usuario?.noPessoa +' - CPF: '+ Util.formatCpf(exportParams?.usuario?.nuCpf) + ' - ID: ' +exportParams?.usuario?.sqPessoa : ''
                Util.printLog('SALVE - ExportDataService.asyncExportarOcorrencias()','Erro ao exportar registros' + usuario
                    ,error.getMessage())
                if( job ) {
                    // encerrar o job
                    userJobService.error( job.id, error.getMessage() )
                }
                //error.printStackTrace()
            }
        }
    }



}
