package br.gov.icmbio

//import grails.transaction.Transactional

//@Transactional
class DadosApoioService {

 	public static List getTable( String table_name )
    {
        if( table_name ) {
            table_name = table_name.toUpperCase()
            if (table_name == 'TB_SUBGRUPO') {
                List itensGrupos = DadosApoio.findAllByPai(DadosApoio.findByCodigoSistema('TB_GRUPO'))
                if( itensGrupos ) {
                    return  DadosApoio.createCriteria().list{
                        'in'('pai',itensGrupos)
                        order( 'ordem', 'asc')
                        order( 'descricao', 'asc')
                    }
                }
            }

            List pai = DadosApoio.findAllByCodigoSistema(table_name);
            if (pai) {
                return  DadosApoio.createCriteria().list{
                    eq('pai',pai[0])
                    order( 'ordem', 'asc')
                    order( 'descricao', 'asc')
                }
            }
        }
		return []
    }

	static DadosApoio getByCodigo( String tableName=null, String codigoSistema = null )
    {
		DadosApoio pai = DadosApoio.findByCodigoSistema(tableName);
		if( pai )
		{
            if( codigoSistema ) {
                return DadosApoio.findByPaiAndCodigoSistema( pai, codigoSistema )
            }
            return pai
		}
		return null
    }

    public static DadosApoio getByDescricao( String tableName, String descricao)
    {
        List pai = DadosApoio.findAllByCodigoSistema(tableName);
        if( pai )
        {
            return DadosApoio.findByPaiAndDescricaoIlike(pai,descricao)
        }
        return null
    }

    public static DadosApoio getByDescricaoSigla( String tableName, String descricao)
    {
        List pai = DadosApoio.findAllByCodigoSistema(tableName);
        if( pai )
        {
            return DadosApoio.createCriteria().get{
                or
                {
                    ilike("descricao",descricao)
                    ilike("codigoSistema",descricao)
                }
            }
        }
        return null
    }

    public static List getNodeTree(DadosApoio node)
    {
        List data = [];
        node.itens.each {
            data.push([id:it.id,title:it.descricao,icon:false,foder:false])
        }
        return data;
    }

    public static List getCentros() {
        return Unidade.createCriteria().list {
            or {
                pessoa {
                    ilike('noPessoa', 'Centro %')
                    order('noPessoa', 'asc')
                }
                //ilike('sgUnidade','BAV/%')
                eq('sgInstituicao','ICMBio')
            }

        }
    }

    public static List getCategoriasAmeacadas()
    {
        return  DadosApoio.createCriteria().list {
            pai {
                eq('codigoSistema','TB_CATEGORIA_IUCN')
            }
            'in'('codigoSistema',['VU','EN','CR'])
        }
    }
}
