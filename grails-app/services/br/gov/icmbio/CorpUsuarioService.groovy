package br.gov.icmbio

import com.vividsolutions.jts.geom.Geometry
import com.vividsolutions.jts.io.WKTReader
import grails.converters.JSON
import grails.transaction.Transactional

@Transactional
class CorpUsuarioService {

    //def sqlService

    /**
     * método para gravar as informações do usuário no banco de dados
     * @param sqPessoa
     * @param noPessoa
     * @param wkt
     * @return
     */
    Map save(Long sqPessoa = null, String nuCpf = '', String deEmail = '', String noPessoa= '', Boolean stAtivo, String deSenha = '',Long sqInstituicao = null) {
       if( !noPessoa ){
            throw new Exception('Nome do usuário não informado')
        }
        if( !nuCpf && !deEmail ){
            throw new Exception('CPF ou e-mail do usuário deve ser informado')
        }

        // validar cpf
        if( nuCpf && ! Util.isCpf(nuCpf) ) {
            throw new Exception('CPF inexistente')
        }

        // remover formatacao do CPF
        nuCpf = nuCpf.replaceAll(/[^0-9]/,'')

        // validar email
        if( deEmail && ! Util.isEmail(deEmail) ) {
            throw new Exception('E-mail inválido')
        }
        Instituicao instituicao
        if( sqInstituicao ) {
            instituicao = Instituicao.get( sqInstituicao )
        }
        Pessoa pessoa

        // localizar a pessoa pelo CPF informado
        PessoaFisica pessoaFisica = PessoaFisica.findByNuCpf(nuCpf)
        if( pessoaFisica ){
            sqPessoa = pessoaFisica.id
        }

        Usuario usuario

        if( sqPessoa ){
            pessoa = Pessoa.get( sqPessoa )
            if( !pessoa ){
                throw new Exception('Id do usuário inexistente')
            }
            pessoaFisica = PessoaFisica.get( sqPessoa )
            usuario = Usuario.get( sqPessoa )
        } else {
            pessoa = new Pessoa()
            pessoaFisica = new PessoaFisica()
            usuario = new Usuario()
        }

        // criar o usuário se ainda não existir
        if( !usuario ){
            usuario = new Usuario()
        }

        pessoa.noPessoa = noPessoa
        pessoa.deEmail = deEmail ?: null
        pessoa.save()

        pessoaFisica.pessoa = pessoa
        pessoaFisica.nuCpf = nuCpf ?:null
        pessoaFisica.instituicao = ( instituicao ?: pessoaFisica.instituicao )
        pessoaFisica.save()

        usuario.stAtivo = stAtivo.toBoolean()
        usuario.pessoaFisica = pessoaFisica

        if( deSenha ){
            usuario.deSenha = Util.md5( deSenha )
        }
        usuario.save()

        return usuario.asMap()
    }


    /**
     * método para recuperar as informações do usuário para alteração
     * @param sqPessoa
     * @return
     */
    def edit(Long sqPessoa=0l) {
        if( !sqPessoa ){
            throw new Exception('Id do usuário não informado')
        }

        Usuario usuario = Usuario.get( sqPessoa )

        if( !usuario ){
            throw new Exception('Id do usuário inexistente')
        }

        Map result = usuario.asMap()

        return result
    }


    /**
     * método para excluir o usuário do banco de dados
     * @return
     */
    def delete(Long sqPessoa = 0l) {
        Usuario usuario = Usuario.get( sqPessoa )
        if( !usuario ) {
            throw new Exception('Id do usuário não encontrado')
        }
        usuario.delete()
    }

    /**
     * criar listagem paginada dos usuários
     * @param page
     * @param pageSize
     * @param q
     * @return
     */
    List list( Integer page=1, Integer pageSize=100, String q = '') {
        Integer skipRecords = ( pageSize * ( page - 1 ) )
        return Usuario.createCriteria().list {
            if( q ){
                pessoaFisica {
                    pessoa {
                        ilike('noPessoa', '%' + q.trim() + '%')
                    }
                }
            }
            maxResults( pageSize )
            firstResult( skipRecords )

        }.sort{it.pessoaFisica.pessoa.noPessoa}
    }

    /**
     * consultar usuario pelo CPF
     * @param nuCpf
     * @return
     */
    Map consultarCpf(String nuCpf = '' ) {
        nuCpf = nuCpf.replaceAll(/[^0-9]/, '')
        if (!nuCpf) {
            return [:]
        }
        Usuario usuario = Usuario.createCriteria().get {
            pessoaFisica {
                eq('nuCpf', nuCpf)
            }
        }
        if (!usuario) {
            return [:]
        }
        return usuario.asMap()
    }

    /**
     * consultar usuario pelo E-MAIL
     * @param deEmail
     * @return
     */
    Map consultarEmail(String deEmail = '' ){
        deEmail = deEmail.replaceAll(/\s/, '').toLowerCase()

        if( !deEmail || !Util.isEmail(deEmail) ){
            return [:]
        }
        Usuario usuario = Usuario.createCriteria().get{
            pessoaFisica {
                pessoa {
                    eq('deEmail', deEmail )
                }
            }
        }
        if( !usuario ){
            return [:]
        }
        return usuario.asMap()
    }


    /**
     * retornar lista com os perfis do usuário por instituicao
     */
    List perfilPorInstituicao(Long sqPessoa = 0l){
        Usuario u = Usuario.get( sqPessoa )
        if( u ) {
            return u.perfilPorInstituicao
        }
        return []
    }

}
