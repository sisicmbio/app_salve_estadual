package br.gov.icmbio

import grails.transaction.Transactional

@Transactional
class PublicacaoService {

    def sqlService

    List search( String q = null, String contexto = null, Boolean inListaOficialVigente = null, String refBibSearchFor = null ) {
        boolean informouAno = false
		q = q.trim()
        if (! q )
        {
           return []
        }
        // verificar se o parametro q termina com 4 numero que significar que foi informado o ano
        informouAno =  ( q =~ /.+[0-9]{4}$/ )


        // verificar se informou autor e ano
        List qs = q.split(' ')
        List lista = Publicacao.createCriteria().list {
            // verficar padrão id: 99999
            if( q.toString() =~ /(?i)^id:(\s{1,})?[0-9]{1,}$/ ) {
                eq('id', q.replaceAll(/[^0-9]/,'').toLong() )
            } else if( ! refBibSearchFor ) {
                qs.each { palavra ->
                    palavra = URLDecoder.decode(palavra.toString(), "UTF-8")
                    if ( informouAno && palavra.isInteger() && palavra.toInteger() > 1500) {
                        and {
                            eq('nuAnoPublicacao', palavra.toInteger())
                        }
                    } else {
                        and {
                            or {
                                ilike("noAutor", '%' + palavra + '%')
                                ilike('deTitulo', '%' + palavra + '%')
                            }
                        }
                    }
                }
            }
            else
            {
                if( refBibSearchFor == 'autor')
                {   and {
                        ilike("noAutor", '%' + qs[0] + '%')
                    }
                }
                else if( refBibSearchFor == 'titulo')
                {
                    and {
                        ilike("deTitulo", '%' + qs[0] + '%')
                    }
                }
                else if( refBibSearchFor == 'ano')
                {
                    and {
                        eq('nuAnoPublicacao', q.toInteger() )
                    }
                }
            }
            if ( inListaOficialVigente  != null ) {
                and {
                    eq('inListaOficialVigente', inListaOficialVigente )
                }
            }

            if (!contexto || contexto != 'ocorrencia') {
                List tiposInvalidos = TipoPublicacao.findAllByCoTipoPublicacaoOrCoTipoPublicacao('MUSEU', 'BANCO_DADOS');
                if( tiposInvalidos )
                {
                    and {
                        not { 'in'("tipoPublicacao", tiposInvalidos) }
                    }
                }
            }
        }
        return lista
    }
    //----------------------------------------------
    /**
     * Pesquisar referencia bibliográfica pelo autor e titulo ignorando acentuação
     * @param q
     * @param contexto
     * @param inListaOficialVigente
     * @param refBibSearchFor
     * @return
     */
    List searchAcento( String q = null, String contexto = null, Boolean inListaOficialVigente = null, String refBibSearchFor = null ) {
        boolean informouAno = false
        String cmdSql = """"""
        List sqlWhere = []
        q = q.trim()
        if (!q) {
            return []
        }
        // verificar se o parametro q termina com 4 numero que significar que foi informado o ano
        informouAno = (q =~ /.+[0-9]{4}$/)

        // remover acentos, cedilhas etc
        q = Util.removeAccents(q)
        // verificar se informou autor e ano
        List qs = q.split(' ')

        // verficar padrão id: 99999
        if( q.toString() =~ /(?i)^id:(\s{1,})?[0-9]{1,}$/ ) {
            sqlWhere.push('sq_publicacao = ' + q.replaceAll(/[^0-9]/,'') )
        } else if ( ! refBibSearchFor) {
            qs.each { palavra ->
                palavra = URLDecoder.decode(palavra.toString(), "UTF-8")
                if (informouAno && palavra.isInteger() && palavra.toInteger() > 1500) {
                    sqlWhere.push('nu_ano_publicacao = ' + palavra)
                } else {
                    sqlWhere.push("(public.fn_remove_acentuacao( no_autor ) ilike '%" + palavra + "%' or public.fn_remove_acentuacao(de_titulo) ilike '%" + palavra + "%')")
                }
            }
        } else {
            if (refBibSearchFor == 'autor') {
                sqlWhere.push("public.fn_remove_acentuacao(no_autor) ilike '%" + qs[0] + "%'")
            } else if (refBibSearchFor == 'titulo') {
                sqlWhere.push("public.fn_remove_acentuacao(de_titulo) ilike '%" + qs[0] + "%'")
            } else if (refBibSearchFor == 'ano') {
                sqlWhere.push("nu_ano_publicacao = " + q)
            }
        }
        if (inListaOficialVigente != null) {
            sqlWhere.push("in_lista_oficial_vigente = '" + inListaOficialVigente + "'")
        }

        if (!contexto || contexto != 'ocorrencia') {
            List tiposInvalidos = TipoPublicacao.findAllByCoTipoPublicacaoOrCoTipoPublicacao('MUSEU', 'BANCO_DADOS');
            if (tiposInvalidos) {
                sqlWhere.push(" sq_tipo_publicacao not in (" + tiposInvalidos.id.join(',') + ")")
            }
        }
        cmdSql = """select sq_publicacao as id
                    ,de_titulo as deTitulo
                    ,no_autor as noAutor
                    ,nu_ano_publicacao as nuAnoPublicacao
                    ,concat( de_titulo
                        ,case when no_autor is null then '' else ' / ' || no_autor end
                    ,case when publicacao.nu_ano_publicacao is null then
                    case when dt_publicacao is null then '' else ' / ' || to_char(dt_publicacao,'dd/mm/yyyy') end
                    else ' / '::text || nu_ano_publicacao::text end
                    ) as tituloAutorAno
                    ,coalesce(de_ref_bibliografica,de_titulo) as referenciaHtml
                    from taxonomia.publicacao
                    where ${sqlWhere.join(' and ')}
                    """
        //println cmdSql
        return sqlService.execSql( cmdSql )
    }
}
