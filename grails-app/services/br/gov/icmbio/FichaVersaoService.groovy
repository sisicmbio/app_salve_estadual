package br.gov.icmbio

import grails.converters.JSON
import grails.transaction.Transactional
import groovy.json.JsonSlurper
import org.codehaus.groovy.grails.web.json.JSONObject

@Transactional
class FichaVersaoService {

    def sqlService

    FichaVersao findFichaVersao( Map params = [:] ) {

        FichaVersao fichaVersao
        // encontrar a versão da ficha baseado nos parametros recebidos
        if ( params.sqCicloConsultaFichaVersao ) {
            CicloConsultaFichaVersao ccv = CicloConsultaFichaVersao.get(params.sqCicloConsultaFichaVersao.toLong())
            if (! ccv ) {
                throw new Exception('Id '+params.sqCicloConsultaFichaVersao+' inexistente. (cicloConsultaFichaVersao)')
            }
            fichaVersao = ccv.fichaVersao
        } else if ( params.sqFichaVersao ) {
            fichaVersao = FichaVersao.get(params.sqFichaVersao.toLong())
            if (! fichaVersao ) {
                throw new Exception('Id '+params.sqFichaVersao+' inexistente. (fichaVersao)')
            }
        }

        if( ! fichaVersao ){
            throw new Exception('Versão da ficha não encontrada')
        }
        return fichaVersao
    }


    JSONObject loadJsonVersao(FichaVersao fichaVersao ) {
        JSONObject fichaJson = JSON.parse( fichaVersao.jsFicha )

        // campos calculados a partir do json da versao
        fichaJson.classificacao_taxonomica.nomes_comuns_formatados = Util.formatarNomesComuns(fichaJson.classificacao_taxonomica.nomes_comuns)
        fichaJson.classificacao_taxonomica.nomes_antigos_formatados = Util.formatarNomesAntigos(fichaJson.classificacao_taxonomica.nomes_antigos)

        // criar o "Como citar"
        String noCientificoSemAutor = fichaJson?.taxon_trilha?.subespecie?.no_taxon ?: fichaJson?.taxon_trilha?.especie?.no_taxon
        String anoAvaliacao = fichaJson?.avaliacao?.resultado_avaliacao?.nu_mes_ano_ultima_avaliacao?.toString()?.reverse()?.substring(0,4)?.reverse()
        fichaJson.comoCitar = Util.getComoCitar(noCientificoSemAutor, fichaJson?.avaliacao?.resultado_avaliacao?.ds_citacao,anoAvaliacao,false)
        return fichaJson
    }

    /**
     * método para criação do gride de registros de ocorrencia
     */
    Map getOcorrencias(Long sqFichaVersao = 0){
        if( !sqFichaVersao ){
            return [rows:[], pagination:[:]]
        }
        FichaVersao fichaVersao = FichaVersao.get( sqFichaVersao )
        if( !fichaVersao ){
            return ''
        }
        JSONObject registrosJson = JSON.parse( fichaVersao.jsRegistros )
        List rows = []
        registrosJson.ocorrencias_salve.each { row ->
            Map rowData = [
                id               : row.sq_ficha_ocorrencia,
                no_base_dados    : 'SALVE',
                de_tipo_registro : row.georeferenciamento.sq_tipo_registro,
                de_periodo       : _formatarPeriodoOcorrencia(row.georeferenciamento),
                de_presenca_atual: Util.snd(row.georeferenciamento.in_presenca_atual),
                nu_altitude      : row.localidade.nu_altitude,
                de_sensivel      : Util.snd(row.georeferenciamento.in_sensivel),
                de_carencia      : row.georeferenciamento.sq_prazo_carencia,
                dt_carencia      : '',
                de_datum         : row.georeferenciamento.sq_datum,
                nu_latitude      : row.georeferenciamento.nu_lat,
                nu_longitude     : row.georeferenciamento.nu_lon,
                de_formato_original   : row.georeferenciamento.sq_formato_coord_original,
                de_precisao           : row.georeferenciamento.sq_precisao_coordenada,
                tx_metodo_aproximacao : row.georeferenciamento.tx_metodo_aproximacao,
                de_situacao_avaliacao : row.georeferenciamento.sq_situacao_avaliacao,
                de_motivo_nao_utilizado : row.georeferenciamento.sq_motivo_nao_utilizado_avaliacao,
                tx_nao_utilizado        : row.georeferenciamento.tx_nao_utilizado_avaliacao,
                id_origem               : row.georeferenciamento.id_origem,
                tx_observacao           : row.georeferenciamento.tx_observacao,
            ]
            rows.push( rowData )
        }
        Map pagination = [pageSize: 100, offset: 0, rowNum: 1, buttons: 10
                          , gridId:'GridOcorrenciasVersao', pageNumber: 1
                          , totalRecords: rows.size()]
        return [rows:rows, pagination:pagination]
    }

    //-----------------------------------------

    /**
     * mostrar a fundamentação e o chat da ficha na validação
     * @param sqOficinaFicha
     * @return
     */
    Map dadosFundamentacao( Long sqOficinaFicha = 0l ) {
        if( !sqOficinaFicha ){
            throw  new Exception('Id não informado')
        }
        OficinaFicha oficinaFicha = OficinaFicha.get( sqOficinaFicha.toLong() )
        if( !oficinaFicha ){
            throw  new Exception('Id informado não encontrado')
        }
        OficinaFichaVersao oficinaFichaVersao = OficinaFichaVersao.get( oficinaFicha.id )
        if( !oficinaFichaVersao ){
            throw  new Exception('Ficha não versionada')
        }
        return [nuVersao:oficinaFichaVersao.fichaVersao.nuVersao]
    }

    //-----------------------------------------

    void updateJson( Long sqFichaVersao = 0l, String key = '', String value ='',String table='', String column=''){
        if( !sqFichaVersao ){
            throw  new Exception('Id da versão não informado')
        }
        if( !key ){
            throw  new Exception('Valor da chave não informado')
        }

        FichaVersao fichaVersao = FichaVersao.get( sqFichaVersao )
        if( !fichaVersao ){
            throw  new Exception('Versão inexistente')
        }
        value = Util.stripTagsFichaPdf( value )

        /*
        if( !value.isNumber() ){
        }
        */

        String valueToSave = value.replaceAll(/"/,'\\\\"').replaceAll(/\n|\r/,'')  //.replaceAll( /\// , '\\\\/')

        String cmdSql = """update salve.ficha_versao set js_ficha = jsonb_set(js_ficha,'{${key.split('\\.').join(',')}}','"${valueToSave}"',false)
            where sq_ficha_versao = :sqFichaVersao;
            """
        /** /
        println ' '
        println 'cmdSql'
        println cmdSql
        /**/

        // fazer a atualização da chave da coluna jsonb
        sqlService.execSql(cmdSql,[sqFichaVersao:sqFichaVersao])
    }
    //-----------------------------------------
    //-----------------------------------------
    String _formatarPeriodoOcorrencia(Map data = [:] ) {
        String inicio = ''
        String fim = ''
        if (data.nu_dia_registro && data.nu_mes_registro && data.nu_ano_registro) {
            inicio = data.nu_dia_registro + '/' + data.nu_mes_registro + '/' + data.nu_ano_registro
        } else if (data.nu_mes_registro && data.nu_ano_registro) {
            inicio = data.nu_mes_registro + '/' + data.nu_ano_registro
        } else if (data.nu_ano_registro) {
            inicio = data.nu_ano_registro
        }
        if (data.nu_dia_registro_fim && data.nu_mes_registro_fim && data.nu_ano_registro_fim) {
            fim = data.nu_dia_registro_fim + '/' + data.nu_mes_registro_fim + '/' + data.nu_ano_registro_fim
        } else if (data.nu_mes_registro_fim && data.nu_ano_registro_fim) {
            fim = data.nu_mes_registro_fim + '/' + data.nu_ano_registro_fim
        } else if (data.nu_ano_registro_fim) {
            fim = data.nu_ano_registro_fim
        }
        if (inicio && fim) {
            return inicio + ' a ' + fim
        } else if (inicio) {
            return inicio
        } else if (fim) {
            return fim
        }
    }

}
