package br.gov.icmbio

import grails.transaction.Transactional
import grails.util.Holders
import org.springframework.context.ApplicationContext

@Transactional
class AssinaturaEletronicaService {

    /**
     * verificar se o documento ja foi assinado e caso constrário
     * realizar a assinatura utilizando o método Util.assinarPdf()
     * @example http://localhost:8080/salve-estadual/api/assinarDocFinalOficina/c1a20be7762728d20b62da7a6e94981fe737ed0c
     * @param hashRegistro
     * @return
     */
    boolean assinarRelatorioFinal(String hashRegistro = '' )
    {
        // gerar a data e hora da assinatura
        String dataHora = new Date().format("dd/MM/yyyy', às' HH:mm")

        /**
         *
         * String fileName = "/data/salve-estadual/temp/rel_final_oficina_assinar.pdf"
         */
        // validar se o hash foi enviado
        if( ! hashRegistro ){
            throw new Exception('Hash do registro não foi informado')
        }

        // ler o registro do banco de dados
        OficinaAnexoAssinatura reg =  OficinaAnexoAssinatura.findByDeHash( hashRegistro )
        if( ! reg )
        {
            throw new Exception('Documento não encontrado!<br>hash:['+hashRegistro+']')
        }

        // validar se o documento já foi assinado
        if( reg.dtAssinatura )
        {
            throw new Exception('Assinatura já registrada!')
        }

        // ler o nome completo do participante
        String nomeParticipante = Util.capitalize(reg.oficinaParticipante.pessoa.noPessoa)

        // gerar o texto da assinatura eletrônica
        // TODO - tornar este texto configurável pelo adminstrador do salve
        String textoAssinatura = "Documento assinado eletronicamente por ${nomeParticipante}, em ${dataHora}, conforme horário oficial de Brasilia, com fundamento no artigo 10º da Instrução Normativa ICMBio nº 9 de 11/08/2020."

        // verificar se o documento já está assinado
        if( reg.dtAssinatura )
        {
            throw new Exception('Documento já foi assinado em ' + reg.dtAssinatura.format('dd/MM/yyyy HH:mm:ss') )
        }
        // nome da unidade organizacional para o cabecalho das paginas
        String unidadeOrg = ''
        Oficina oficina = reg?.oficinaAnexo?.oficina
        if( oficina.unidade ) {
            //unidadeOrg = oficina.unidade.noPessoa + (oficina.unidade.sgUnidade ? ' - ' + oficina.unidade.sgUnidade : '')
            unidadeOrg = oficina.unidade.noPessoa
        }

        // montar nome completo do arquivo no diretório anexos
        ApplicationContext ctx = Holders.grailsApplication.mainContext
        String dirAnexos = ctx.grailsApplication?.config?.anexos?.dir+'/'
        dirAnexos = dirAnexos.replaceAll(/\/anexos\/\//,'/anexos/')
        String pdfFileName = dirAnexos+reg.oficinaAnexo.deLocalArquivo

        //textoAssinatura = 'Para validar a autencidade deste documento, acesse o site:\nhttps://salve.icmbio.gov.br/salve-estadual/validar-doc\ne informe o código verificador: '+hashRegistro.substring(0,6)+'.'
        Map res = Util.assinarPdf( pdfFileName,textoAssinatura,'','', unidadeOrg )
        if( res.error )
        {
            throw new Exception( res.msg )
        }

        // gravar a data e hora da assinatura
        reg.dtAssinatura = new Date()
        reg.save(flush:true)
        return true
    }
}
