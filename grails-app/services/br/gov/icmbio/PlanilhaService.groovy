package br.gov.icmbio
// import grails.transaction.Transactional

import com.vividsolutions.jts.geom.Coordinate
import com.vividsolutions.jts.geom.GeometryFactory
import grails.converters.JSON
import org.codehaus.groovy.grails.plugins.DomainClassGrailsPlugin
import org.springframework.web.multipart.commons.CommonsMultipartFile
import org.codehaus.groovy.grails.plugins.web.taglib.ValidationTagLib

//Exemplo: throw new RuntimeException("rollback!")

//@Transactional
class PlanilhaService
{
    static transactional = false

    def sessionFactory
    def propertyInstanceMap = DomainClassGrailsPlugin.PROPERTY_INSTANCE_MAP
    def utilityService
    def dadosApoioService
    def taxonService
    def geoService
    def emailService
    def messageSource
    def fichaService
    def sqlService
    def registroService

    Map importarPlanilha(CommonsMultipartFile file
                         , Integer sqCicloAvaliacao
                         , Integer sqPessoa
                         , Integer sqUnidade
                         , String deComentario
                         , String saveToPath
                         , String contexto
                         , boolean isAdm
                         , String emailUsuario =  ''
    )
    {

        Map result = [startTime:System.currentTimeMillis()];
        PessoaFisica pessoaFisica = PessoaFisica.get(sqPessoa);
        Unidade unidade = Unidade.get(sqUnidade);
        CicloAvaliacao cicloAvaliacao
        Planilha tbPlanilha = null

        contexto = contexto.toUpperCase()

        if (!pessoaFisica)
        {
            result.msg = 'Pessoa Física não encontrada. (' + sqPessoa + ')';
            result.type = 'error';
            result.status = 1;
            return result;
        }

        if ( ! unidade )
        {
            result.msg = 'Usuário não vinculado a uma Unidade do ICMBio. (Perfil Externo)';
            result.type = 'error';
            result.status = 1;
            return result;
        }
        if ( ! sqCicloAvaliacao && ( contexto =='FICHA' || contexto=='OCORRENCIA') )
        {
            result.msg = 'Ciclo Avaliação não informado!';
            result.type = 'error';
            result.status = 1;
            return result;
        }
        else if( sqCicloAvaliacao )
        {
            cicloAvaliacao = CicloAvaliacao.get(sqCicloAvaliacao);
            if ( ! cicloAvaliacao )
            {
                result.msg = 'Ciclo Avaliação não encontrado. (' + sqCicloAvaliacao + ')';
                result.type = 'error';
                result.status = 1;
                return result;
            }
        }

        if (!contexto)
        {
            result.msg = 'Contexto não informado! Ex: FICHA,OCORRENCIA ou REFBIB';
            result.type = 'error';
            result.status = 1;
            return result;
        } else if( contexto !='FICHA' && contexto !='OCORRENCIA' && contexto != 'REFBIB')
        {
            result.msg = 'Contexto ('+contexto+') informado inválido! Ex: FICHA,OCORRENCIA ou REFBIB';
            result.type = 'error';
            result.status = 1;
            return result;
        }

        try
        {
            PlanilhaParser pp = new PlanilhaParser(file);
            pp.setSheet(0); // selecionar a primeira aba da planilha
            if( pp.getError())
            {
                pp.close()
                result.msg = pp.getError()
                result.status = 1;
                result.type = 'error';
                return result;
            }

            // gravar o arquivo no iretorio
            if (saveToPath)
            {
                pp.saveAs(saveToPath + file.getOriginalFilename())
                if( pp.getError())
                {
                    pp.close()
                    result.msg = pp.getError()
                    result.status = 1
                    result.type = 'error'
                    return result
                }
            }

            // dados para retorno
            //result.firstVisibleTab = workbook.getFirstVisibleTab();
            result.sheetName = pp.getSheetName()
            result.linhas = pp.getRows()
            result.fileName = file.getOriginalFilename()
            result.fileSize = file.getSize();
            result.tmpPath = file.getStorageDescription();
            result.contentType = file.getContentType();

            //result.msg = 'Arquivo:' + file.getOriginalFilename() + ' Tamanho: ' + (file.getSize() / 1024 / 1024).toString() + 'Mb';
            //result.msg = 'Arquivo:' + file.getOriginalFilename() + ' Tamanho: ' + (file.getSize() / 1024 / 1024).toString() + 'Mb';
            result.type = 'success';
            result.status = 0;
            result.qtdLinhasComErro = 0;
            result.colsNotFound = []; // lista de colunas não foram encontradas na planilha

            // gravar planilha no banco de dados
            tbPlanilha = Planilha.findByUnidadeAndNoArquivoAndNoContexto(unidade, result.fileName,contexto)
            if (!tbPlanilha) {
                tbPlanilha = new Planilha()
                tbPlanilha.unidade = unidade
                tbPlanilha.noContexto = contexto
                tbPlanilha.noArquivo = result.fileName
            }
            if( tbPlanilha.inProcessando )
            {
                result.msg = 'Planilha está sendo processada. Andamento '+tbPlanilha.nuPercentual+'%.<br><br>Para reiniciar importação exclua a planilha do histórico abaixo!'
                result.status = 1
                result.type = 'info'
                tbPlanilha = null
                pp.close()
                return result
            }
            tbPlanilha.pessoaFisica = pessoaFisica
            if( cicloAvaliacao )
            {
                tbPlanilha.cicloAvaliacao = cicloAvaliacao
            }
            tbPlanilha.deComentario = deComentario ? deComentario : 'Salve - Importação Planilha';
            tbPlanilha.deCaminho = saveToPath;
            tbPlanilha.txLog = null
            tbPlanilha.nuPercentual = 0
            tbPlanilha.inProcessando = true
            // gerar o id da planilha
            tbPlanilha.save(flush: true)
            println 'Percentual e log zerados'

            // limpar importação anterior;
            tbPlanilha.clearLines();
            result.idPlanilha = tbPlanilha.id;
            result.seconds = 0;
            /** /
            result.msg = 'Até aqui tudo bem. Planilha Salva.... contexto ' + contexto
            result.status = 1;
            result.type = 'error';
            return result;
            /**/

            String assunto = ''
            if (contexto == 'FICHA')
            {
                pp.setHeaderRowNum(3) // linha dos titulos das colunas
                importarFicha(tbPlanilha, pp, result, isAdm)
                assunto = 'Importação Fichas'
            } else if (contexto == 'OCORRENCIA')
            {
                pp.setHeaderRowNum(3) // linha dos titulos das colunas
                importarOcorrencia(tbPlanilha, pp, result,isAdm)
                assunto = 'Importação Registros'
                result.linhas-=3
            } else if (contexto == 'REFBIB')
            {
                pp.setHeaderRowNum(1) // linha dos titulos das colunas
                importarRefBib(tbPlanilha, pp, result,isAdm)
                assunto = 'Importação de Referência Bibliográfica'
                //emailUsuario='' // por enquanto não enviar email
            }

            if( emailUsuario )
            {
                if (emailService.sendTo(emailUsuario, assunto
                        , 'A importação da planilha <b>' + tbPlanilha.noArquivo + '</b> foi finalizada.<br/>' +
                           'Segue abaixo o resultado:<br/>' + tbPlanilha.getLineErrors()+'<br/>' + tbPlanilha.txLog ) )
                {
                    println 'Email enviado com sucesso para ' + emailUsuario
                }
            }
            else
            {
                println 'Usuário sem email para envio do log.'
                println '------------------------------------'
            }
        }
        catch (Exception e)
        {

            result.type = 'error';
            result.msg = e.getMessage();
            result.status = 1;

            //e.printStackTrace()
            tbPlanilha.inProcessando=false
            tbPlanilha.txLog + (tbPlanilha.txLog !='' ? '<br>':'')+e.getMessage()
            try{ tbPlanilha.save(flush:true) } catch( Exception e2 ){}
            println 'Erro: ' + e.getMessage();

        }
        return result;
    }

    void importarFicha(Planilha tbPlanilha, PlanilhaParser pp, Map result, boolean isAdm) {
        println 'Iniciando importacao de fichas ' + new Date().format('dd/MM/yyyy HH:mm:ss')

        Integer curLin = 0  // numero da linha atual do loop de linhas
        List planTitles = [] // lista com os titulos das colunas extraido da planilha
        List planilhaLog = []
        Unidade unidadePlanilha
        Map fichaColumns = getValidFichaColumns()
        def g = new ValidationTagLib()
        // 1-Ler os títulos da colunas, transformar em caixa alta e sem acentos
        planTitles = pp.getRowData(3)
        if( ! planTitles)
        {
            result.msg = 'Não consegui ler os títulos da colunas,certifique que estejam na linha 3 da planilha'
            result.status = 1;
            result.type = 'error';
            return

        }
        planTitles.eachWithIndex{v,i ->
            planTitles[i] = utilityService.removeAccents( v.toString().toUpperCase() )
        }
        curLin = 3;
        Integer contador = 0
        Integer progressTotal = ( result.linhas.toInteger() - 3) * 2
        Integer progressPosition = 0
        String valor     = ''
        while ( curLin++ < result.linhas)
        {
            // ler os valores da linha curLin
            List data = pp.getRowData(curLin)
            if( data )
            {
                // criar a linha em branco na tabela
                PlanilhaFicha tbPlanilhaFicha = new PlanilhaFicha(nuLinha:curLin, planilha:tbPlanilha);
                tbPlanilhaFicha.isAdm = isAdm // permitir cadastro de fichas de outro centro se for perfil adm
                tbPlanilhaFicha.isRegistro = false;
                // gravar cada valor na sua coluna correspondente
                planTitles.eachWithIndex { String colName,int colIndex ->

                    Map colData = fichaColumns[colName];
                    /*if( colName == 'NOME CIENTIFICO')
                    {
                        println 'Linha: ' + curLin + ' ' + data[colIndex]
                        log.info( 'Linha: ' + curLin + ' ' + data[colIndex] )
                    }
                    */
                    if( colData )
                    {
                        valor = data[ colIndex ].toString().trim()
                        if( !valor || valor == 'null ')
                        {
                            valor=''
                        }
                        if (colData.required && ! valor  )
                        {
                           // lineErrors.push('Erro: Coluna ' + colName + ' não pode ser vazia!');
                            tbPlanilhaFicha.erros.add( 'Erro: Coluna ' + colName + ' não pode ser vazia!')
                        }
                        else
                        {
                            if(  valor ) {
                                // validar sigla do centro
                                if( colData.colPlanilhaFicha=='txCentro')
                                {
                                    if( !unidadePlanilha || unidadePlanilha.sgUnidade != valor )
                                    {
                                        unidadePlanilha = Unidade.findByNoPessoaOrSgUnidade(valor,valor)
                                    }
                                    if( ! unidadePlanilha )
                                    {
                                        println  'Erro gravando linha ' + curLin +' coluna ' + colData.colPlanilhaFicha + ' = ' + valor +'  inexistente no ICMBio'
                                        tbPlanilhaFicha.erros.add('Erro gravando linha ' + curLin +' coluna ' + colData.colPlanilhaFicha + ' = ' + valor +'  inexistente no ICMBio' );
                                    }
                                }

                                // tratamento especifico das colunas antes de gravar na tabela
                                if (colData.colPlanilhaFicha == 'txCpfPf' || colData.colPlanilhaFicha == 'txCpfCt')
                                {
                                    valor = tratarCpf( valor)
                                    if (!utilityService.isCpf(valor))
                                    {
                                        //tbPlanilhaFicha.erros.add('Erro: Coluna ' + colName + ' valor ' + valor + ' está inválido!');
                                        tbPlanilhaFicha.erros.add('Erro: Coluna ' + colName + ' cpf ' + valor + ' está inválido!');
                                    }
                                }
                                // validar Estado
                                if (colData.colPlanilhaFicha == 'txEstadoAvaliacao' )
                                {
                                    // estado da abrangencia
                                    if( ! dadosApoioService.getByDescricao('TB_ESTADO', valor ) )
                                    {
                                        tbPlanilhaFicha.erros.add('Erro: Coluna ' + colName + ' valor ' + valor + ' não existe na tabela de apoio TB_ESTADO!');
                                    }
                                }

                                if (colData.colPlanilhaFicha == 'txNomeCientifico' )
                                {
                                    valor = valor.replaceAll(/[^a-zA-Z-]/,' ').replaceAll('/  /g',' ').trim()
                                }

                                if (colData.colPlanilhaFicha == 'txAutorAno' )
                                {
                                    valor = ( valor.toString() == 'null' ? '' : valor )
                                }

                                // campos validados na tabela de apoio
                                if ( colData.tbApoio )
                                {
                                    // carregar dados apoio
                                    tbPlanilhaFicha.dadosApoio[ colData.colFicha ] = dadosApoioService.getByDescricao(colData.tbApoio, valor);
                                    if (! tbPlanilhaFicha.dadosApoio[ colData.colFicha ]  )
                                    {
                                        tbPlanilhaFicha.erros.add('Erro: Coluna '+colName+' valor '+valor+' não cadastrado na tabela ' + colData.tbApoio )
                                    }
                                }

                                // gravar o dados da celula da planilha no campo correspondente da tabela
                                try
                                {
                                    // joão-do-norte/Portugues;Amazonian Spinetail/Ingles;Pijuí de Cabanis/Espanhol
                                    // Synallaxis cabanisi/Berlepsch & Leverkuhn, 1890
                                    //print colData.colPlanilhaFicha+':'+valor+', '
                                    tbPlanilhaFicha[ colData.colPlanilhaFicha ] = valor;
                                } catch (Exception e)
                                {
                                    tbPlanilhaFicha.erros.add('- Erro: ' + e.getMessage() )
                                }
                            }
                        }
                    }
                }
                if( tbPlanilhaFicha.validate() )
                {
                    tbPlanilhaFicha.save();
                    tbPlanilha.addToLinhas(tbPlanilhaFicha);
                    result.qtdLinhasComErro += tbPlanilhaFicha.erros.size()
                    contador++
                }
                else
                {
                    tbPlanilhaFicha.errors.allErrors.each {
                        planilhaLog.push('<p>Erro: linha '+curLin+'<br>'+g.message(error: it)+'<p>')
                    }
                }
                // 100 em 100 gravar o buffer
                if (contador % 100 == 0)
                {
                    //println 'Flushing 100 fichas ' + new Date()
                    cleanUpGorm();
                    //println 'Flushing finished'
                    contador = 0;
                }
            }
            progressPosition++
            updatePercentual( tbPlanilha, ( progressPosition / progressTotal * 100) )
        }
        //println 'Finalizada leitura das linhas'
        // verificar se saiu com o buffer carregado e descarrega-lo
        if ( contador > 0 )
        {
            //println 'Flushing ' + contador + ' fichas finais ' + new Date()
            cleanUpGorm();
            contador=0
        }

        List listIdsFichas = [] // fichas para atualizar o percentual após a importação

        // criar as fichas das linhas sem erros
        //if ( true  ||result.qtdLinhasComErro == 0)
        if ( tbPlanilha.linhas ) {
            boolean isNew = true
            DadosApoio situacaoFicha    = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'COMPILACAO')
            DadosApoio tbTipoAvaliacao  = DadosApoio.findByCodigo('TB_TIPO_AVALIACAO')
            DadosApoio tbTipoAbrangencia= DadosApoio.findByCodigo('TB_TIPO_ABRANGENCIA')
            DadosApoio categoriaNE      = dadosApoioService.getByCodigo('TB_CATEGORIA_IUCN', 'NE')

            Abrangencia abrangencia
            DadosApoio categoriaIucn // Extinta, Vulneravel ..
            DadosApoio tipoAbrangencia
            DadosApoio tipoAvaliacao
            DadosApoio papel
            Unidade unidadeOrg = null
            Pessoa pessoaTemp
            String catTemp
            Integer qtd
            Integer ano
            List listTemp
            tbPlanilha.linhas.sort{ it.nuLinha }.each { lin ->
                //println 'Importando ficha linha '+lin.nuLinha+') ' + ( lin?.taxon ? lin.taxon.noCientifico:' especie nao cadastrada')
                if ( lin.erros.size() == 0 && lin.taxon )
                {
                    if( !isAdm  )
                    {
                        if( ! unidadeOrg )
                        {
                            unidadeOrg = tbPlanilha.unidade
                        }
                    }
                    else
                    {
                        if( unidadeOrg==null || unidadeOrg?.sgUnidade != lin.txCentro.trim() )
                        {
                            unidadeOrg = Unidade.findByNoPessoaOrSgUnidade(lin.txCentro.trim(),lin.txCentro.trim())
                        }
                    }
                    try {
                        // criar ou editar a ficha se não existirr
                        /** /
                        println 'procurar ficha:'
                        println 'Ciclo:' + tbPlanilha.cicloAvaliacao
                        println 'Unidade:' + unidadeOrg
                        println 'Taxon:' + lin.taxon
                        /**/

                        Ficha ficha = Ficha.findByCicloAvaliacaoAndUnidadeAndTaxon(tbPlanilha.cicloAvaliacao, unidadeOrg, lin.taxon)
                        if (!ficha) {
                            isNew = true
                            ficha = new Ficha()
                            ficha.cicloAvaliacao = tbPlanilha.cicloAvaliacao
                            ficha.unidade = tbPlanilha.unidade
                            ficha.taxon = lin.taxon
                            ficha.situacaoFicha = situacaoFicha
                            ficha.usuario = tbPlanilha.pessoaFisica
                            ficha.categoriaIucn = categoriaNE
                            ficha.vlPercentualPreenchimento = 0
                        } else {
                            isNew = false
                            // atualizar nome cientifico + autor
                            if (lin['txNomeCientifico'].toString().trim() != '') {
                                ficha.nmCientifico = lin['txNomeCientifico'].toString().trim() +
                                    (lin['txAutorAno'].toString().trim() == 'null' ? '' : ' ' + lin['txAutorAno'].toString().trim())
                                ficha.save(flush: true)
                            }
                        }

                        if( true ){
                        //if (ficha.vlPercentualPreenchimento.toInteger() == 0i) {
                            // gravar os campos que não precisam de tratamento e não são da tabela de apoio
                            fichaColumns.each { colName, colData ->
                                valor = '';
                                if (lin[colData.colPlanilhaFicha] != null) {
                                    valor = lin[colData.colPlanilhaFicha].toString().trim()
                                    if (colData.colPlanilhaFicha == 'txNomeCientifico' && valor != '') {
                                        valor += (lin['txAutorAno'].toString().trim() != 'null' ? ' ' + lin['txAutorAno'].toString().trim() : '')
                                    }
                                }
                                if (colData.colFicha && !colData.tbApoio) {
                                    if (colData.colFicha.indexOf('st') == 0 && valor) {
                                        valor = valor.toString().substring(0, 1).toUpperCase();
                                    }
                                    try {
                                        if (valor) {
                                            // se comecar por numero tratar ponto e virgula
                                            if( valor =~ /^[-0-9]/ && ! (valor =~ /\s/) && ! (valor =~ /[0-9]-/) && !( valor =~ /(?i)[a-z+*%#@!]/) ) {
                                                valor = valor.replaceAll(',', '.');
                                                if (colData.colFicha == 'nuMaxAltitude'
                                                    || colData.colFicha == 'nuMinAltitude'
                                                    || colData.colFicha == 'nuMaxBatimetria'
                                                    || colData.colFicha == 'nuMinBatimetria') {

                                                    if( isNew || ficha[colData.colFicha] == null || ficha[colData.colFicha].toString() == '' ) {
                                                        ficha[colData.colFicha] = Double.parseDouble(valor).intValue();
                                                    }
                                                } else {
                                                    if( isNew || ficha[colData.colFicha] == null || ficha[colData.colFicha].toString()== '' ) {
                                                        ficha[colData.colFicha] = Double.parseDouble(valor);
                                                    }
                                                }
                                            } else {
                                                if( isNew || ficha[colData.colFicha] == null || ficha[colData.colFicha].toString() == '' ) {
                                                    ficha[colData.colFicha] = valor;
                                                }
                                            }
                                        }
                                    }
                                    catch (Exception e) {
                                        planilhaLog.push('Erro gravando linha ' + lin.nuLinha + ' coluna ' + colData.colFicha + ' = ' + valor + '  Motivo:<br>' + e.getMessage());
                                    }
                                } else {
                                    if (valor) {
                                        if (colData.colPlanilhaFicha == 'txCentro') {
                                            if (unidadePlanilha || valor != unidadePlanilha?.sgUnidade) {
                                                unidadePlanilha = Unidade.findByNoPessoaOrSgUnidade(valor, valor)
                                            }
                                            if (!unidadePlanilha) {
                                                ficha.unidade = null // evitar que aficha seja gravada
                                                planilhaLog.push('Erro gravando linha ' + lin.nuLinha + ' coluna ' + colData.colPlanilhaFicha + ' = ' + valor + '  inexistente no ICMBio');
                                            } else {
                                                if (isAdm) {
                                                    ficha.unidade = unidadePlanilha
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            // gravar os campos que precisam de tratamento
                            lin.dadosApoio.each { key, value ->
                                try {
                                    ficha[key] = value
                                } catch (Exception e) {
                                    //println e.getMessage();
                                    planilhaLog.push('Erro gravando linha ' + lin.nuLinha + ' coluna: ' + key + ' = ' + value.descricao);
                                }
                            }
                            ficha.validate()
                            if (ficha.getErrors().errorCount > 0) {
                                //def errMsgList = ficha.errors.allErrors.collect{g.message([error : it])}
                                println 'Linha ' + lin.nuLinha + ' tem ' + ficha.getErrors().errorCount + ' erros';
                                ficha.errors.allErrors.each {
                                    planilhaLog.push('<p>Erro: linha ' + lin.nuLinha + '<br>' + g.message(error: it) + '<p>')
                                }
                            } else {
                                ficha.save()
                            }
                        } else {
                            planilhaLog.push('Aviso: a ficha <b>' + lin['txNomeCientifico'] + '</b> não foi atualizada porque o percentual de preenchimanto não está 0%')
                        }



                        // gravar tabelas Ns ligadas a ficha. Sinonimia, nome comum, historico avaliação, pendencias etc..
                        if (ficha.id) {
                            // registrar o id para calcular o percentual de preenchimento no final
                            if( listIdsFichas.indexOf( ficha.id) == -1 ) {
                                listIdsFichas.push(ficha.id)
                            }
                            if( lin['txNomeCientifico'].toString() && lin['txAutorAno'].toString() && lin['txAutorAno'].toString() != 'null' )
                            {
                                if( ! ficha.taxon.noAutor )
                                {
                                    ficha.taxon.noAutor = lin['txAutorAno'].toString()
                                    ficha.taxon.save()
                                }
                            }
                            // adicionar função ponto focal para a ficha ( papel )
                            if( lin['txCpfPf'] != '' )
                            {
                                // se for adm o usuario da ficha será o ponto focal
                                pessoaTemp=null
                                if(  ficha.usuario.nuCpf != lin['txCpfPf']  && isAdm && lin['txCpfPf'] )
                                {
                                    pessoaTemp = PessoaFisica.findByNuCpf( lin['txCpfPf'])
                                    if( pessoaTemp && pessoaTemp.nuCpf )
                                    {
                                        ficha.usuario = pessoaTemp
                                    }
                                }
                                if( !pessoaTemp && lin['txCpfPf'] )
                                {
                                    pessoaTemp = (Pessoa) PessoaFisica.findByNuCpf( lin['txCpfPf'] )
                                }
                                papel = dadosApoioService.getByCodigo('TB_PAPEL_FICHA','PONTO_FOCAL')
                                if( pessoaTemp && papel && pessoaTemp.nuCpf )
                                {
                                    FichaPessoa fichaPessoa = new FichaPessoa()
                                    fichaPessoa.ficha = ficha
                                    fichaPessoa.pessoa = pessoaTemp
                                    fichaPessoa.papel = papel
                                    // verificar se o ponto focal já está cadastrado
                                    if( FichaPessoa.countByFichaAndPessoaAndPapel(ficha,pessoaTemp,papel) == 0 )
                                    {
                                        // só pode ter 1 pf ativo por ficha
                                        if ( FichaPessoa.countByFichaAndPapelAndInAtivo( ficha,papel,'S' ) == 0 )
                                        {
                                            fichaPessoa.inAtivo = 'S'
                                        }
                                        else
                                        {
                                            fichaPessoa.inAtivo = 'N'
                                        }
                                        fichaPessoa.validate()
                                        fichaPessoa.save()
                                    }
                                }
                            }
                            if (lin.txNomeComum)
                            {
                                lin.txNomeComum.toString().split(';').each{ nomeComum ->
                                    listTemp = nomeComum.toString().split('/')
                                    qtd = 0;
                                    if (listTemp[1])
                                    {
                                        qtd = FichaNomeComum.countByFichaAndNoComumAndDeRegiaoLingua(ficha, listTemp[0], listTemp[1]);
                                    }
                                    else if ( listTemp[0] )
                                    {
                                        qtd = FichaNomeComum.countByFichaAndNoComum(ficha, listTemp[0]);
                                    }
                                    if (!qtd)
                                    {
                                        new FichaNomeComum(ficha: ficha, noComum: listTemp[0], deRegiaoLingua: listTemp[1]).save();
                                    }
                                }
                            }
                           if (lin.txSinonimia)
                           {
                                List ignoreValues = ['','null','não há','não há sinonímia','desconhecida','desconhecidas']
                                lin.txSinonimia.toString().split(';').each{ sinonimia ->
                                    if( ignoreValues.indexOf(sinonimia.toString().trim().toLowerCase().replaceAll(/\./,'') ) == -1 )
                                    {
                                        listTemp = sinonimia.toString().split('/')
                                        qtd = 0
                                        if (listTemp[1])
                                        {
                                            qtd = FichaSinonimia.countByFichaAndNoSinonimiaAndNoAutor(ficha, listTemp[0], listTemp[1]);
                                        } else if (listTemp[0])
                                        {
                                            qtd = FichaSinonimia.countByFichaAndNoSinonimia(ficha, listTemp[0]);
                                        }
                                        if (!qtd)
                                        {
                                            if( !listTemp[1] )
                                            {
                                                planilhaLog.push('<p>Aviso: linha ' +lin.nuLinha+'<br>Falta nome do autor da sinonímia '+listTemp[0]+'</p>' )
                                            }
                                            else
                                            {
                                                new FichaSinonimia(ficha: ficha, noSinonimia: listTemp[0], noAutor: listTemp[1]).save();
                                            }
                                        }
                                    }
                                }
                            }

                            // adicionar Histórico de avalição
                            if (lin.txTipoAvaliacao && lin.txAnoAvaliacao)
                            {
                                try
                                {
                                    ano = Double.parseDouble(lin.txAnoAvaliacao).intValue()
                                } catch (Exception e)
                                {
                                    ano = 0
                                }
                                if (ano > 1500)
                                {
                                    if (tbTipoAbrangencia)
                                    {
                                        if (tbTipoAvaliacao)
                                        {
                                            // tipo da avaliação
                                            //tipoAvaliacao = DadosApoio.findByPaiAndDescricao(tbTipoAvaliacao, lin.txTipoAvaliacao);
                                            tipoAvaliacao = dadosApoioService.getByDescricao('TB_TIPO_AVALIACAO', lin.txTipoAvaliacao)
                                            if (tipoAvaliacao)
                                            {
                                                // abrangencia
                                                abrangencia=null
                                                if (lin.txEstadoAvaliacao)
                                                {
                                                    Estado estado = Estado.findByNoEstado( lin.txEstadoAvaliacao)
                                                    if( estado ) {
                                                        abrangencia = Abrangencia.findByEstado( estado )
                                                    }
                                                }

                                                // categoria ex: Vulnerável (VU)
                                                if( lin.txCategoriaAvaliacao )
                                                {
                                                    listTemp = lin.txCategoriaAvaliacao.toString().split('\\('); // remover a sigla (VU)
                                                    catTemp = listTemp[0].toString().trim();
                                                    categoriaIucn = dadosApoioService.getByDescricao('TB_CATEGORIA_IUCN', catTemp);
                                                    if (categoriaIucn?.codigoSistema)
                                                    {
                                                        //No primeiro ciclo, a última avaliação nacional deve ir para o "Resultado Final"
                                                        if (tipoAvaliacao.codigoSistema == 'NACIONAL_BRASIL' && ano >= 2012 && ano <= 2014)
                                                        {
                                                            ficha.categoriaFinal            = categoriaIucn
                                                            ficha.dsJustificativaFinal      = lin.txJustificativaAvaliacao
                                                            ficha.dsCriterioAvalIucnFinal   = lin.txCriterioAvaliacao
                                                            ficha.save()
                                                        } else
                                                        {
                                                            if (abrangencia) {
                                                                qtd = FichaHistoricoAvaliacao.countByFichaAndTipoAvaliacaoAndAbrangenciaAndCategoriaIucnAndNuAnoAvaliacao(ficha, tipoAvaliacao, abrangencia, categoriaIucn, ano)
                                                            } else
                                                            {
                                                                qtd = FichaHistoricoAvaliacao.countByFichaAndTipoAvaliacaoAndCategoriaIucnAndNuAnoAvaliacao(ficha, tipoAvaliacao, categoriaIucn, ano)
                                                            }
                                                            if (qtd == 0)
                                                            {
                                                                new FichaHistoricoAvaliacao(ficha: ficha
                                                                        , tipoAvaliacao: tipoAvaliacao
                                                                        //, tipoAbrangencia: tipoAbrangencia
                                                                        , abrangencia: abrangencia
                                                                        , categoriaIucn: categoriaIucn
                                                                        , nuAnoAvaliacao: ano
                                                                        , deCriterioAvaliacaoIucn: lin.txCriterioAvaliacao
                                                                        , txJustificativaAvaliacao: lin.txJustificativaAvaliacao).save()
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        planilhaLog.push('Erro: linha ' + lin.nuLinha + ' Categoria Avaliação ' + catTemp + ' inválida!')
                                                    }
                                                }
                                                else
                                                {
                                                    planilhaLog.push('Erro: linha ' + lin.nuLinha + ' Categoria Avaliação não informada!')
                                                }
                                            } else
                                            {
                                                planilhaLog.push('Erro: linha ' + lin.nuLinha + ' Tipo Avaliação ' + lin.txTipoAvaliacao + ' inválida!')
                                            }
                                        }
                                    }
                                } else
                                {
                                    planilhaLog.push('Erro: linha ' + lin.nuLinha+' Ano avaliação ' + lin.txAnoAvaliacao + ' inválido!')
                                }
                            }
                            if ( lin.txPendencias )
                            {
                                if( lin.txCpfPf )
                                {
                                    PessoaFisica pf = PessoaFisica.findByNuCpf( lin.txCpfPf )
                                    if( pf )
                                    {
                                        try
                                        {
                                            if (FichaPendencia.createCriteria().count {
                                                eq('ficha', ficha)
                                                eq('txPendencia', lin.txPendencias.trim()) } == 0)
                                            {
                                                FichaPendencia fp = new FichaPendencia(ficha: ficha
                                                        , usuario: pf
                                                        , deAssunto: 'Pendência'
                                                        , txPendencia: lin.txPendencias.trim()
                                                        , dtPendencia: new Date()
                                                        , stPendente: 'S')
                                                if (!fp.validate())
                                                {
                                                    println fp.getErrors()
                                                    planilhaLog.push('Aviso: linha ' + lin.nuLinha + ' Pendências não foram gravadas devido ao erro ' + fp.getErrors())
                                                } else
                                                {
                                                    fp.save(flush: true)
                                                }
                                            }
                                        }
                                        catch( Exception e )
                                            {
                                                println e.getMessage()
                                                planilhaLog.push('Aviso: linha ' + lin.nuLinha+' Pendências não foram gravadas devido ao erro ' + e.getMessage() )
                                            }
                                        }
                                    else
                                    {
                                        println ('Aviso: linha ' + lin.nuLinha+' Pendências não foram gravadas devido ao CPF ' + lin.txCpfPf+' não estar cadastrado no SICA-E.')
                                        planilhaLog.push('Aviso: linha ' + lin.nuLinha+' Pendências não foram gravadas devido ao CPF ' + lin.txCpfPf+' não estar cadastrado no SICA-E.')
                                    }
                                }
                                else
                                {
                                    println ('Aviso: linha ' + lin.nuLinha+' Pendências não foram gravadas devido a coluna Ponto Focal estar em branco.')
                                    planilhaLog.push('Aviso: linha ' + lin.nuLinha+' Pendências não foram gravadas devido a coluna Ponto Focal estar em branco.')
                                }
                            }
                            // gravar a ficha na linha
                            lin.ficha = ficha

                            // flush session
                            contador ++
                            if ((contador) % 500 == 0)
                            {
                                cleanUpGorm()
                                contador = 0
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        println '- Erro criando a ficha da linha ' + lin.nuLinha+'. Motivo: ' + e.getMessage()
                        planilhaLog.push('Erro criando a ficha da linha ' + lin.nuLinha+'. Motivo:<br>' + e.getMessage());
                    }
                }
                progressPosition++
                updatePercentual( tbPlanilha, ( progressPosition / progressTotal * 100) )
            }
            // salvar o buffer se saiu com dados
            if (contador > 0 )
            {
                cleanUpGorm()
                contador=0
            }

        }
        else {
            planilhaLog.push('Planilha está vazia.');
        }
        tbPlanilha.txLog = planilhaLog.join('<br />');
        tbPlanilha.nuPercentual  = 100
        tbPlanilha.inProcessando = false
        tbPlanilha.save( flush:true)
        def batchEnded = System.currentTimeMillis()
        if( result.linhas > 3 )
        {
            result.linhas -= 3; // as três primeiras linhas não contam como ficha
        }
        result.seconds = (batchEnded - result.startTime) / 1000
        listIdsFichas.each {
            fichaService.updatePercentual( it.toLong() )
           // println 'Calculando percentual id ' + it
        }
    }

    /* ******************************************************************************************************************* */

    void importarOcorrencia(Planilha tbPlanilha, PlanilhaParser pp, Map result, Boolean isAdm) {
        Integer curLin = 0  // numero da linha atual do loop de linhas
        List planTitles = [] // lista com os titulos das colunas extraido da planilha
        List planilhaLog = [];
        GeometryFactory gf = new GeometryFactory();
        Estado estadoTemp // para validar o municipio
        Integer intTemp   // para validar dia, mes e ano
        Map ocorrenciaColumns = getValidOcorrenciaColumns();
        def g = new ValidationTagLib()
        Uc ucTemp
        boolean alteracao = false
        boolean possuiErro = false
        FichaOcorrencia fichaOcorrencia

        // 1-Ler os títulos da colunas, transformar em caixa alta e sem acentos
        planTitles = pp.getRowData(3)
        if (!planTitles) {
            result.msg = 'Não consegui ler os títulos da colunas,certifique que estejam na linha 3 da planilha'
            result.status = 1;
            result.type = 'error';
            return
        }
        if (planTitles[0].toUpperCase() != 'NOME CIENTÍFICO') {
            result.msg = 'Planilha não está no formato correto. Os títulos devem estar na linha 3.'
            result.status = 1;
            result.type = 'error';
            return

        }
        planTitles.eachWithIndex { v, i ->
            planTitles[i] = utilityService.removeAccents(v.toString().toUpperCase())
        }

        curLin = 3;
        Integer contador = 0
        Integer progressTotal = (result.linhas.toInteger() - 3)
        Integer progressPosition = 0

        //println '  - Inciando loop '+ result.linhas

        while (curLin++ < result.linhas) {
            // atualizar percentual processamento
            estadoTemp = null

            // ler linha da planilha
            List data = pp.getRowData(curLin)
            if (data) {
                PlanilhaOcorrencia tbPlanilhaOcorrencia = new PlanilhaOcorrencia(nuLinha: curLin, planilha: tbPlanilha);
                tbPlanilhaOcorrencia.isAdm = isAdm
                tbPlanilhaOcorrencia.isRegistro = true // procurar a especie nas fichas e não na base taxonomica
                // gravar cada valor na sua coluna correspondente
                planTitles.eachWithIndex { String colName, int colIndex ->
                    // remover espaços não unicos do título das colunas
                    colName = colName.replaceAll(/\s{2,}/, ' ');
                    // compatibilidade planilha antiga
                    if (colName == 'USADO NA AVALIACAO?') {
                        colName = 'USADO AVALIACAO?'
                    }

                    // ler o atributos/regras da coluna
                    Map colData = ocorrenciaColumns[colName];
                    if (colData) {
                        if (colData.required && !data[colIndex]) {
                            tbPlanilhaOcorrencia.erros.push('Erro: Coluna ' + colName + ' não pode ser vazia!');
                            /*if( colName=='ANO') {
                                tbPlanilhaOcorrencia.erros.push('Erro: Coluna ' + colName + ' não pode ser vazia! Necessário informar no mínimo o ano do registro.');
                            } else {
                                tbPlanilhaOcorrencia.erros.push('Erro: Coluna ' + colName + ' não pode ser vazia!');
                            }*/
                        } else {
                            // ler o valor da celula referente a coluna colIndex
                            if (data[colIndex]) {
                                // remover os espaços e caracteres especiais
                                data[colIndex] = data[colIndex].toString().trim().replaceAll(//, '')

                                // tratamento especifico das colunas antes de gravar na tabela
                                if (colData.colPlanilhaOcorrencia == 'txCompilador' || colData.colPlanilhaOcorrencia == 'txRevisor' || colData.colPlanilhaOcorrencia == 'txValidador') {
                                    // tratar o valor do cpf por vir da planilha
                                    data[colIndex] = tratarCpf(data[colIndex])
                                    if (!utilityService.isCpf(data[colIndex].toString())) {
                                        tbPlanilhaOcorrencia.erros.add('Erro: Coluna ' + colName + ' cpf ' + data[colIndex] + ' está inválido.');
                                    } else {
                                        if (!PessoaFisica.findByNuCpf(data[colIndex])) {
                                            tbPlanilhaOcorrencia.erros.add('Erro: Coluna ' + colName + ' cpf ' + data[colIndex] + ' não cadastrado na tabela corporativo.pessoa_fisica.')
                                        }
                                    }
                                }

                                /*
                                if ( colData.colPlanilhaOcorrencia == 'txUsadoAvaliacao') {
                                    if (data[colIndex]) {
                                        data[colIndex] = ((data[colIndex].toString().toUpperCase() =~ /^S/) ? 'S' : 'N')
                                    }
                                }*/

                                // corrigir o numero do tombamento
                                if (colData.colPlanilhaOcorrencia == 'txTombamento') {
                                    // remover .0; e .0 do final do numero do tombamento
                                    data[colIndex] = data[colIndex].toString().replaceAll(/\.0;/, ';')
                                    data[colIndex] = data[colIndex].toString().replaceAll(/\.0$/, '')
                                }

                                // validar datas
                                //if (colData.colPlanilhaOcorrencia == 'txDataCompilacao' || colData.colPlanilhaOcorrencia == 'txDataRevisao' || colData.colPlanilhaOcorrencia == 'txDataValidacao' || colData.colPlanilhaOcorrencia == 'txDataIdentificacao' )
                                if (colData.colFichaOcorrencia && colData.colFichaOcorrencia.indexOf('dt') == 0) {
                                    if (!utilityService.isDate(data[colIndex])) {
                                        tbPlanilhaOcorrencia.erros.add('Erro: Coluna ' + colName + ' valor ' + data[colIndex] + ' está inválida.');
                                    }
                                }
                                // validar hora
                                if (colData.colFichaOcorrencia && colData.colFichaOcorrencia.indexOf('hr') == 0) {
                                    if (!utilityService.isTime(data[colIndex])) {
                                        tbPlanilhaOcorrencia.erros.add('Erro: Coluna ' + colName + ' valor ' + data[colIndex] + ' está inválida.');
                                    }
                                }
                                // validar campos extrangeiros que não sejam da tabela de apoio
                                if (colData.colPlanilhaOcorrencia == 'txPais') {
                                    if (data[colIndex] == 'Brazil') {
                                        data[colIndex] = 'Brasil'
                                    }
                                    if (!Pais.findByNoPaisOrNoPais(data[colIndex], Util.removeAccents(data[colIndex]))) {
                                        tbPlanilhaOcorrencia.erros.add('Erro: Coluna ' + colName + ' valor ' + data[colIndex] + ' não cadastrado na tabela corporativo.Pais.')
                                    }
                                }
                                if (colData.colPlanilhaOcorrencia == 'txEstado') {
                                    estadoTemp = Estado.findByNoEstadoOrSgEstadoOrNoEstado(data[colIndex], data[colIndex], Util.removeAccents(data[colIndex]))
                                    if (!estadoTemp) {
                                        tbPlanilhaOcorrencia.erros.add('Erro: Coluna ' + colName + ' valor ' + data[colIndex] + ' não cadastrado na tabela corporativo.Estado.')
                                    }
                                }
                                if (colData.colPlanilhaOcorrencia == 'txMunicipio') {
                                    String noMunicipio = data[colIndex].replaceAll(/(?i)DOeste/,"D'Oeste").replaceAll(/(?i)DÁgua/,"D'Água")
                                    if (estadoTemp) {
                                        Integer qtd = Municipio.createCriteria().count {
                                            eq('estado', estadoTemp)
                                            or {
                                                eq('noMunicipio', noMunicipio)
                                                eq('noMunicipio', Util.removeAccents(noMunicipio))
                                                ilike('noMunicipio', noMunicipio)
                                                ilike('noMunicipio', Util.removeAccents(noMunicipio))
                                            }
                                        }
                                        if (qtd == 0)
                                        //if (!Municipio.findByEstadoAndNoMunicipio(estadoTemp, data[colIndex],Util.removeAccents(data[colIndex]) ) )
                                        {
                                            tbPlanilhaOcorrencia.erros.add('Erro: Coluna ' + colName + ' valor ' + noMunicipio+ ' do Estado ' + estadoTemp.noEstado + ' não cadastrado na tabela corporativo.Municipio.')
                                        }
                                    } else {
                                        if ( ! Municipio.findByNoMunicipioIlike(noMunicipio) && !Municipio.findByNoMunicipioIlike(Util.removeAccents(noMunicipio))) {
                                            tbPlanilhaOcorrencia.erros.add('Erro: Coluna ' + colName + ' valor ' + noMunicipio + ' não cadastrado na tabela corporativo.Municipio.')
                                        }
                                    }
                                }
                                // validar campos decimais
                                if (colData.colPlanilhaOcorrencia == 'txElevacaoMinima' ||
                                    colData.colPlanilhaOcorrencia == 'txElevacaoMaxima' ||
                                    colData.colPlanilhaOcorrencia == 'txProfundidadeMinima' ||
                                    colData.colPlanilhaOcorrencia == 'txProfundidadeMaxima') {
                                    if (!utilityService.isDouble(data[colIndex]) && data[colIndex].toString() != '0.0') {
                                        tbPlanilhaOcorrencia.erros.add('Erro: Coluna ' + colName + ' valor ' + data[colIndex] + ' inválido.')
                                    }
                                }
                                // validar ucFederal,Estadual e rppn
                                if (colData.colPlanilhaOcorrencia == 'txUcFederal') {
                                    if (!Uc.findByNoUcIlike(data[colIndex]) && !Uc.findBySgUnidade(data[colIndex])) {
                                        tbPlanilhaOcorrencia.erros.add('Erro: Coluna ' + colName + ' valor ' + data[colIndex] + ' não cadastrada.')
                                    }
                                }
                                if (colData.colPlanilhaOcorrencia == 'txUcEstadual') {
                                    if (!Uc.findByNoUcIlike(data[colIndex]) && !Uc.findBySgUnidade(data[colIndex])) {
                                        tbPlanilhaOcorrencia.erros.add('Erro: Coluna ' + colName + ' valor ' + data[colIndex] + ' não cadastrada.')
                                    }
                                }
                                if (colData.colPlanilhaOcorrencia == 'txRppn') {
                                    if (!Uc.findByNoUcIlike(data[colIndex]) && !Uc.findBySgUnidade(data[colIndex])) {
                                        tbPlanilhaOcorrencia.erros.add('Erro: Coluna ' + colName + ' valor ' + data[colIndex] + ' não cadastrada.')
                                    }
                                }
                                if (colData.colFichaOcorrencia && colData.colFichaOcorrencia == 'txTaxonOriginalCitado') {
                                    if (!taxonService.taxonExists(data[colIndex])) {
                                        tbPlanilhaOcorrencia.erros.add('Erro: Coluna ' + colName + ' valor ' + data[colIndex] + ' não cadastrado na tabela Taxon.')
                                    }
                                }
                                // validar dia / mes / ano
                                if (colData.colFichaOcorrencia && colData.colFichaOcorrencia == 'txDia') {
                                    try {
                                        intTemp = Integer.parseInt(data[colIndex])
                                    } catch (Exception e) {
                                        intTemp = -1
                                    }
                                    if (intTemp < 1 || intTemp > 31) {
                                        tbPlanilhaOcorrencia.erros.add('Erro: Coluna ' + colName + ' valor ' + data[colIndex] + ' deve estar entre 1 e 31.')
                                    }
                                } else if (colData.colFichaOcorrencia && colData.colFichaOcorrencia == 'txMes') {
                                    try {
                                        intTemp = Integer.parseInt(data[colIndex])
                                    } catch (Exception e) {
                                        intTemp = -1
                                    }
                                    if (intTemp < 1 || intTemp > 12) {
                                        tbPlanilhaOcorrencia.erros.add('Erro: Coluna ' + colName + ' valor ' + data[colIndex] + ' deve estar entre 1 e 12.')
                                    }
                                } else if (colData.colFichaOcorrencia && colData.colFichaOcorrencia == 'txAno') {
                                    try {
                                        intTemp = Double.parseDouble(data[colIndex]).intValue()
                                    } catch (Exception e) {
                                        intTemp = -1
                                    }
                                    if (intTemp < 1500 || intTemp > 9999) {
                                        tbPlanilhaOcorrencia.erros.add('Erro: Coluna ' + colName + ' valor ' + data[colIndex] + ' deve estar entre 1500 e 9999.')
                                    } else {
                                        if (tbPlanilhaOcorrencia.txDia && tbPlanilhaOcorrencia.txMes && data[colIndex]) {
                                            if (!utilityService.isDate(tbPlanilhaOcorrencia.txDia + '/' + tbPlanilhaOcorrencia.txMes + '/' + data[colIndex])) {
                                                tbPlanilhaOcorrencia.erros.add('Erro: Colunas Dia/Mes/Ano formam uma data inválida!')
                                            }
                                        }
                                    }
                                } else if (colData.colFichaOcorrencia && colData.colFichaOcorrencia == 'idOrigem' && data[colIndex] != '') {
                                    try {
                                        //intTemp = Double.parseDouble(data[colIndex]).intValue()
                                        //data[ colIndex ] = intTemp.toString()
                                        data[colIndex] = data[colIndex]
                                    }
                                    catch (Exception e1) {
                                        tbPlanilhaOcorrencia.erros.add('Erro: Coluna ' + colName + ' valor ' + data[colIndex] + ' deve ser um número válido.')
                                    }
                                }
                                // validar id ref bib
                                else if (data[colIndex] && colData.colPlanilhaOcorrencia == 'idRefBibSalve') {
                                    try {
                                        intTemp = Double.parseDouble(data[colIndex]).intValue()
                                        if (intTemp > 0) {
                                            data[colIndex] = intTemp
                                            Publicacao publicacao = Publicacao.get(intTemp);
                                            if (!publicacao) {
                                                tbPlanilhaOcorrencia.erros.add('Erro: Coluna ' + colName + ' valor <b>' + data[colIndex] + '</b> não é de uma referência cadastrada no SALVE.')
                                            }
                                        }
                                    }
                                    catch (Exception e1) {
                                        tbPlanilhaOcorrencia.erros.add('Erro: Coluna ' + colName + ' valor ' + data[colIndex] + ' deve ser um número válido.')
                                    }
                                }

                                // validar numero do DOI - deDoi
                                else if (data[colIndex] && colData.colPlanilhaOcorrencia == 'deDoi') {
                                    try {
                                        Publicacao publicacao = Publicacao.findByDeDoi(data[colIndex])
                                        if (!publicacao) {
                                            tbPlanilhaOcorrencia.erros.add('Erro: Coluna ' + colName + ' valor <b>' + data[colIndex] + '</b> não cadastrado.')
                                        }
                                    }
                                    catch (Exception e1) {
                                        tbPlanilhaOcorrencia.erros.add('Erro: Coluna ' + colName + ' valor <b>' + data[colIndex] + '</b> é inválido.')
                                    }
                                }
                                // campos tabela de apoio
                                if (colData.tbApoio) {
                                    tbPlanilhaOcorrencia.dadosApoio[colData.colFichaOcorrencia] = dadosApoioService.getByDescricao(colData.tbApoio, data[colIndex]);
                                    if (!tbPlanilhaOcorrencia.dadosApoio[colData.colFichaOcorrencia]) {
                                        if (colData.tbApoio == 'TB_HABITAT') {
                                            List arrTemp = data[colIndex].split(' - ')
                                            String strTemp = '';
                                            if (arrTemp.size() > 1) {
                                                strTemp = arrTemp[arrTemp.size() - 1]
                                            } else {
                                                strTemp = data[colIndex]
                                            }
                                            // localizar pelo formato "1.1. descricao"
                                            strTemp = '[1-9]\\.[1-9]? ' + this.scape2Regex(strTemp) + '$'
                                            //println ' '
                                            //println ' '
                                            //println 'Localizar HABITAT: '+ strTemp
                                            String cmd = "select sq_dados_apoio from salve.dados_apoio where ds_dados_apoio ~ :ds"
                                            //println 'cmd SQL: '+cmd;
                                            //println strTemp
                                            //println '-----------------'

                                            List reg = sqlService.execSql(cmd, [ds: strTemp]);
                                            //println 'Registro encontrado'
                                            //println reg;
                                            //println 'sq_dados_apoio:' + reg[0].sq_dados_apoio.toString()
                                            tbPlanilhaOcorrencia.dadosApoio[colData.colFichaOcorrencia] = null
                                            if (reg && reg[0].sq_dados_apoio > 0) {
                                                tbPlanilhaOcorrencia.dadosApoio[colData.colFichaOcorrencia] = DadosApoio.get(reg[0].sq_dados_apoio)
                                                //println 'Dados Apoio'
                                                //println tbPlanilhaOcorrencia.dadosApoio[colData.colFichaOcorrencia]
                                                //println ' '
                                            }
                                        }
                                        else if( colData.tbApoio == 'TB_SITUACAO_REGISTRO_OCORRENCIA') {
                                            String cdSistema = 'REGISTRO_UTILIZADO_AVALIACAO' // valor padrão caso não tenha sido informado
                                            if( data[colIndex].toString().toUpperCase() =~ /N.O UTILIZADO/ ) {
                                               cdSistema = 'REGISTRO_NAO_UTILIZADO_AVALIACAO'
                                            } else if( data[colIndex].toString().toUpperCase() =~ /UTILIZADO/){
                                                cdSistema = 'REGISTRO_UTILIZADO_AVALIACAO'
                                            } else if( data[colIndex].toString().toUpperCase() =~ /N.O CONFIRMADO/){
                                                cdSistema = 'REGISTRO_NAO_CONFIRMADO'
                                            } else if( data[colIndex].toString().toUpperCase() =~ /ADICIONADO_AP.S/ ) {
                                               cdSistema = 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO'
                                            }

                                           tbPlanilhaOcorrencia.dadosApoio[colData.colFichaOcorrencia] = dadosApoioService.getByCodigo(colData.tbApoio, cdSistema);

                                        }
                                        if (!tbPlanilhaOcorrencia.dadosApoio[colData.colFichaOcorrencia]) {
                                            tbPlanilhaOcorrencia.erros.add('Aviso: Coluna ' + colName + ' valor <b>' + data[colIndex] + '</b> não cadastrado na tabela ' + colData.tbApoio)
                                        }
                                    }
                                }
                                // gravar o dados da celula da planilha no campo correspondente da tabela
                                try {
                                    tbPlanilhaOcorrencia[colData.colPlanilhaOcorrencia] = data[colIndex]
                                } catch (Exception e) {
                                    tbPlanilhaOcorrencia.erros.push('Erro: ' + e.getMessage())
                                }
                            } else {
                                tbPlanilhaOcorrencia[colData.colPlanilhaOcorrencia] = ''
                            }
                        }
                    }
                }

                if (tbPlanilhaOcorrencia?.txPrecisaoCoordenada?.toUpperCase() == 'APROXIMADA') {
                    if (!tbPlanilhaOcorrencia.txMetodoAproximacao) {
                        tbPlanilhaOcorrencia.erros.add('Erro: para a Precisão Aproximada deve ser informada a Metodologia de Aproximação!')
                    }
                }

                // validar se foi informada a referencia bibliográfica
                if (!tbPlanilhaOcorrencia.txTituloRefBib && !tbPlanilhaOcorrencia.idRefBibSalve && !tbPlanilhaOcorrencia.txComunicacaoPessoal && !tbPlanilhaOcorrencia.deDoi) {
                    tbPlanilhaOcorrencia.erros.add('Erro: necessário informar a referência bibliográfica/DOI ou a comunicação pessoal!')
                }

                if (tbPlanilhaOcorrencia.validate()) {
                    tbPlanilhaOcorrencia.save()
                    tbPlanilha.addToLinhas(tbPlanilhaOcorrencia);
                    result.qtdLinhasComErro += tbPlanilhaOcorrencia.erros.size()
                    contador++
                } else {
                    tbPlanilhaOcorrencia.errors.allErrors.each {
                        planilhaLog.push('<p>Erro: linha ' + curLin + '<br>' + g.message(error: it) + '<p>')
                    }
                }
                // 100 em 100 gravar o buffer
                if (contador % 100 == 0) {
                    cleanUpGorm()
                    contador = 0
                }
            }
            //progressPosition++
            updatePercentual(tbPlanilha, (progressPosition / progressTotal * 100))
        }

        // fechar workbook
        pp.close()

        // verificar se saiu com o buffer carregado e descarrega-lo
        if (contador > 0) {
            cleanUpGorm();
            contador = 0
        }

        // Lista de fichas para atualização do percentual
        List listIdsFichas = []

        // criar registros tbFichaOcorrencia
        if (true) {
//            DadosApoio situacaoFicha    = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'COMPILACAO');
//            DadosApoio tbTipoAvaliacao  = DadosApoio.findByCodigo('TB_TIPO_AVALIACAO');
//            DadosApoio tbTipoAbrangencia= DadosApoio.findByCodigo('TB_TIPO_ABRANGENCIA')
            // gravar o contexto
            DadosApoio contextoOcorrencia = dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA', 'REGISTRO_OCORRENCIA')
            DadosApoio contextoEstado = dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA', 'ESTADO')
            DadosApoio contextoBioma = dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA', 'BIOMA')
            DadosApoio contextoBacia = dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA', 'BACIA_HIDROGRAFICA')
            DadosApoio contextoRegistroOcorrencia = dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA', 'REGISTRO_OCORRENCIA')
            DadosApoio situacaoUtilizado = dadosApoioService.getByCodigo('TB_SITUACAO_REGISTRO_OCORRENCIA', 'REGISTRO_UTILIZADO_AVALIACAO')
            tbPlanilha.linhas.sort { it.nuLinha }.each { lin ->
                if (lin.erros.size() == 0 && lin.txLatitude && lin.txLatitude) {
                    try {
                        Ficha ficha
                        if (isAdm) // para administrador não precisa criticar a unidade
                        {
                            // campos para verificar se já foi cadastrado o ponto
                            ficha = Ficha.findByCicloAvaliacaoAndTaxon(tbPlanilha.cicloAvaliacao, lin.taxon)
                        } else {
                            ficha = Ficha.findByCicloAvaliacaoAndUnidadeAndTaxon(tbPlanilha.cicloAvaliacao, tbPlanilha.unidade, lin.taxon)
                        }
                        if (!ficha) {
                            planilhaLog.push('Erro gravando linha ' + lin.nuLinha + ' falta cadastro da ficha ' + lin.taxon.noCientifico);
                        } else {

                            String lon = lin.txLongitude.replaceAll(/,/, '.')
                            String lat = lin.txLatitude.replaceAll(/,/, '.')
                            def geometry = gf.createPoint(new Coordinate(Double.parseDouble(lon), Double.parseDouble(lat)))
                            geometry.SRID = 4674
                            if (!geometry || geometry.x < -180 || geometry.x > 180 || geometry.y < -90 || geometry.y > 90) {
                                planilhaLog.push('Erro linha ' + lin.nuLinha + ': Coordenada inválida Lon(x):' + geometry.x + 'e Lat(y):' + geometry.y)
                            } else {
                                // verificar se a ocorrencia já existe
                                /*FichaOcorrencia fichaOcorrencia = FichaOcorrencia.findByFichaAndGeometryAndPrazoCarenciaAndDatumAndFormatoCoordOriginal(
                                      ficha
                                    , geometry
                                    , lin.dadosApoio.prazoCarencia
                                    , lin.dadosApoio.datum
                                    , lin.dadosApoio.formatoCoordOriginal
                                    )
                                    */

                                List listTemp = FichaOcorrencia.createCriteria().list {
                                    eq('ficha.id', ficha.id)
                                    eq('geometry', geometry)
                                    if (lin.dadosApoio.prazoCarencia) {
                                        eq('prazoCarencia', lin.dadosApoio.prazoCarencia)
//println 'Tem carencia: ' + lin.dadosApoio.prazoCarencia
                                    }
                                    if (lin.dadosApoio.datum) {
                                        eq('datum', lin.dadosApoio.datum)
//println 'Tem datum: ' + lin.dadosApoio.datum
                                    }
                                    if (lin.idOrigem) {
                                        //lin.idOrigem = Double.parseDouble( lin.idOrigem ).intValue().toString()
                                        eq('idOrigem', lin.idOrigem)
//println 'Tem idOrigem: '+ lin.idOrigem
                                    }
                                    if (lin.txTombamento) {
                                        eq('txTombamento', lin.txTombamento)
//println 'Tem txTombamento: ' + lin.txTombamento
                                    }

                                    if (lin.dadosApoio.formatoCoordOriginal) {
                                        eq('formatoCoordOriginal', lin.dadosApoio.formatoCoordOriginal)
//println 'Tem formatoCoordOriginal: ' + lin.dadosApoio.formatoCoordOriginal
                                    }
                                    // adicionar a data da ocorrencia

                                    if (lin.txAno) {
                                        eq('nuAnoRegistro', Double.parseDouble(lin.txAno).toInteger())
//println 'Tem nuAnoRegistro ' +  lin.txAno
                                    }
                                    if (lin.txMes) {
                                        eq('nuMesRegistro', Double.parseDouble(lin.txMes).toInteger())
//println 'Tem nuMesRegistro' + lin.txMes
                                    }

                                    if (lin.txDia) {
                                        eq('nuDiaRegistro', Double.parseDouble(lin.txDia).toInteger())
//println 'Tem nuDiaRegistro: '+ lin.txDia
                                    }

                                    if (lin.txHora) {
                                        eq('hrRegistro', lin.txHora)
//println 'Tem hrRegistro: ' + lin.txHora
                                    }


                                }
                                fichaOcorrencia = null
                                if (listTemp.size() > 0) {
                                    fichaOcorrencia = listTemp[0]
                                }
                                listTemp = []
                                possuiErro = false
                                alteracao = true

                                List autores = []
                                Publicacao publicacao
                                String comPessoal
                                String anoComPessoal

                                if (!fichaOcorrencia) {
                                    alteracao = false
                                    fichaOcorrencia = new FichaOcorrencia()
                                    fichaOcorrencia.ficha = ficha
                                    fichaOcorrencia.geometry = geometry
                                    fichaOcorrencia.contexto = contextoOcorrencia
                                    fichaOcorrencia.pessoaInclusao = tbPlanilha.pessoaFisica
                                    fichaOcorrencia.dtInclusao = new Date()
                                }

                                // gravar os campos que são da tabela de apoio
                                lin.dadosApoio.each { key, value ->
                                    try {
                                        fichaOcorrencia[key] = value
                                    } catch (Exception e) {
                                        planilhaLog.push('Erro gravando linha ' + lin.nuLinha + ' coluna: ' + key + ' = ' + value.descricao);
                                        possuiErro = true
                                    }
                                }
                                ucTemp = null
                                ocorrenciaColumns.each { colName, colData ->
                                    String valor = lin[colData.colPlanilhaOcorrencia].toString().trim()
                                    if (valor && valor.toLowerCase() != 'null') {
                                        // gravar colunas que não são relacionadas a tabela de apoio
                                        if (colData.colFichaOcorrencia && !colData.tbApoio) {
                                            // valor Sim / Não ler a primeira letra
                                            if (colData.colFichaOcorrencia.indexOf('in') == 0 && valor) {
                                                valor = valor.substring(0, 1).toUpperCase();
                                            }
                                            try {
                                                // println 'Coluna: ' +  colData.colFichaOcorrencia
                                                // println 'Valor :' + valor
                                                if (colData.colFichaOcorrencia.indexOf('dt') == 0) {
                                                    fichaOcorrencia[colData.colFichaOcorrencia] = new Date().parse('dd/MM/yyyy', valor)
                                                } else {
                                                    if (valor ==~ /[0-9].*/) {
                                                        valor = valor.replaceAll(',', '.');
                                                        if (colData.colFichaOcorrencia.indexOf('vl') == 0) {
                                                            fichaOcorrencia[colData.colFichaOcorrencia] = Double.parseDouble(valor)
                                                        } else if (colData.colFichaOcorrencia.indexOf('nu') == 0) {
                                                            fichaOcorrencia[colData.colFichaOcorrencia] = Double.parseDouble(valor).intValue()
                                                        } else if (colData.colFichaOcorrencia.indexOf('tx') == 0) {
                                                            fichaOcorrencia[colData.colFichaOcorrencia] = valor.replaceAll(/\./, '')
                                                        } else if (colData.colFichaOcorrencia.indexOf('hr') == 0) {
                                                            fichaOcorrencia[colData.colFichaOcorrencia] = valor
                                                        } else if (colData.colFichaOcorrencia.indexOf('id') == 0) {
                                                            fichaOcorrencia[colData.colFichaOcorrencia] = Double.parseDouble(valor).intValue()
                                                        }
                                                    } else {
                                                        // gravar as UCs
                                                        if (colData.colPlanilhaOcorrencia == 'txUcFederal' ||
                                                            colData.colPlanilhaOcorrencia == 'txUcEstadual' ||
                                                            colData.colPlanilhaOcorrencia == 'txRppn') {
                                                            // NOV-REGISTRO
                                                            // não é mais necessario atualizar Ucs, Estado, Bioma, Bacia...
                                                            /*
                                                                ucTemp = Uc.findBySgUnidade(valor)
                                                                if (!ucTemp) {
                                                                    ucTemp = Uc.findByNoUc(valor)
                                                                }

                                                                if (ucTemp) {
                                                                    fichaOcorrencia.ucControle = ucTemp
                                                                }
                                                             */
                                                        } else {
                                                            fichaOcorrencia[colData.colFichaOcorrencia] = valor
                                                        }
                                                    }
                                                }
                                            } catch (Exception e) {
                                                possuiErro = true
                                                println 'Erro gravando linha ' + lin.nuLinha + ' coluna ' + colData.colFichaOcorrencia + ' = ' + valor + '<br>Motivo:<br>' + e.getMessage()
                                                planilhaLog.push('Erro gravando linha ' + lin.nuLinha + ' coluna ' + colData.colFichaOcorrencia + ' = ' + valor + '<br>Motivo:<br>' + e.getMessage())
                                            }
                                        } else {
                                            // colunas que devem ser lidas da tabela de apoio
                                            if (colData.colPlanilhaOcorrencia == 'txRevisor') {
                                                fichaOcorrencia.pessoaRevisor = PessoaFisica.findByNuCpf(valor)
                                            } else if (colData.colPlanilhaOcorrencia == 'txValidador') {
                                                fichaOcorrencia.pessoaValidador = PessoaFisica.findByNuCpf(valor)
                                            } else if (colData.colPlanilhaOcorrencia == 'txCompilador') {
                                                fichaOcorrencia.pessoaCompilador = PessoaFisica.findByNuCpf(valor)
                                            } else if (colData.colPlanilhaOcorrencia == 'txTaxonOriginalCitado') {
                                                fichaOcorrencia.taxonCitado = Taxon.get(taxonService.taxonExists(valor))
                                            } else if (colData.colPlanilhaOcorrencia == 'txPais') {
                                                fichaOcorrencia.pais = Pais.findByNoPais(valor)
                                            } else if (colData.colPlanilhaOcorrencia == 'txEstado') {
                                                fichaOcorrencia.estado = Estado.findByNoEstadoOrSgEstado(valor, valor)
                                            } else if (colData.colPlanilhaOcorrencia == 'txMunicipio') {
                                                if (fichaOcorrencia.estado) {
                                                    fichaOcorrencia.municipio = Municipio.findByNoMunicipioAndEstado(valor, fichaOcorrencia.estado)
                                                } else {
                                                    fichaOcorrencia.municipio = Municipio.findByNoMunicipio(valor)
                                                    fichaOcorrencia.estado = fichaOcorrencia?.municipio?.estado
                                                }
                                            }
                                        }
                                    } else {
                                        // limpar as colunas sem valor menos as que iniciarem com sq_ para
                                        // não limpar sq_uc_federal, sq_uc_estadual, sq_rppn que são calculadas
                                        // pela coordenada
                                        if (colData.colFichaOcorrencia) {
                                            try {
                                                if (!(colData.colFichaOcorrencia =~ /^sq_/)) {
                                                    fichaOcorrencia[colData.colFichaOcorrencia] = null
                                                }
                                            } catch (Exception e) {
                                            }
                                        }
                                    }
                                }

                                // verificar se a referência bibliográfica informada e valida
                                try {
                                    if (lin.idRefBibSalve || lin.txTituloRefBib || lin.deDoi) {
                                        if (lin.idRefBibSalve) {
                                            publicacao = Publicacao.get(lin.idRefBibSalve.toLong())
                                        } else if (lin.deDoi) {
                                            publicacao = Publicacao.findByDeDoi(lin.deDoi)
                                        } else if (lin.txTituloRefBib && lin.txAutorRefBib && lin.txAnoRefBib) {
                                            lin.txAnoRefBib = Double.parseDouble(lin.txAnoRefBib).intValue().toString()
                                            // colocar no padrão do campo com nomes separados por "; "
                                            autores = lin.txAutorRefBib.replaceAll(/ ?\n+ ?| ?;+ ?|&/, ';').split(';')
                                            // localizar a referência bibliográfica
                                            publicacao = null
                                            Publicacao.createCriteria().list {
                                                eq('nuAnoPublicacao', lin.txAnoRefBib.toInteger())
                                                and {
                                                    autores.each { autor ->
                                                        ilike('noAutor', '%' + autor.trim() + '%')
                                                    }
                                                }
                                            }.each {
                                                String tituloA = Util.stripTags(it.deTitulo).trim().toUpperCase().replaceAll(/[^a-zA-Z0-9]/, '')
                                                String tituloB = Util.stripTags(lin.txTituloRefBib).trim().toUpperCase().replaceAll(/[^a-zA-Z0-9]/, '')
                                                if (tituloA.trim() == tituloB.trim()) {
                                                    //println 'Publicacao encontrada:' + it
                                                    publicacao = it
                                                }
                                            }
                                        }
                                        if (!publicacao) {
                                            throw new Exception('<p>Erro: linha ' + lin.nuLinha + '<br>Referência bibliográfica informada não cadastrada no SALVE.<br>' +
                                                '&nbsp;- ' + lin.txTituloRefBib +
                                                '<br>&nbsp;- Ano: ' + lin.txAnoRefBib.toString() +
                                                '<br>&nbsp;- Autor: ' + autores.join('; '))
                                        }
                                    } else if (lin.txComunicacaoPessoal) {
                                        comPessoal = lin.txComunicacaoPessoal?.trim()
                                        // comunicação pessoal deve terminar com 4 numeros
                                        if (comPessoal =~ /\d{4}$/) {
                                            anoComPessoal = comPessoal.substring(comPessoal.length() - 4)
                                            // remover o ano do final do comPess
                                            comPessoal = comPessoal.substring(0, comPessoal.length() - 4).trim().replaceAll(/,$/, '')
                                        } else {
                                            throw new Exception('<p>Erro: linha ' + lin.nuLinha + '<br>Comunicação pessoal deve terminar com o ano. Ex: ' + comPessoal + ',2020')
                                        }
                                    }
                                } catch (Exception e) {
                                    // se for alteracao nao precisa ter uma referencia bibliografica
                                    if (!alteracao) {
                                        possuiErro = true
                                        throw new Exception('<p>Aviso: linha ' + lin.nuLinha + '<br>Referência bibliográfica ou Com Pess.informada está inválida.<br>Erro:' + e.getMessage())
                                    }
                                }
                                if (!possuiErro) {
                                    if (listIdsFichas.indexOf(ficha.id) == -1) {
                                        listIdsFichas.push(ficha.id)
                                    }
                                    // quando for alteração não reprocessar a parte geo
                                    if (!alteracao) {
                                        //println 'Inicio GEO em ' + new Date()
                                        if (fichaOcorrencia.estado) {
                                            // verificar se o ponto está no Estado
                                            if (!geoService.chkUf(fichaOcorrencia.estado, geometry)) {
                                                possuiErro = true
                                                planilhaLog.push('Erro linha ' + lin.nuLinha + '. Motivo: Ponto fora do Estado ' + fichaOcorrencia.estado.sgEstado)
                                            }
                                        } else {
                                            // NOV-REGISTRO
                                            // não é mais necessario atualizar Estado, Bioma, Bacia...
                                            /*
                                                // pegar o Estado pela coordenada
                                                fichaOcorrencia.estado = geoService.getUfByCoord(geometry.y, geometry.x)
                                                // se tiver Estado, definir o Pais como Brasil
                                                if (fichaOcorrencia.estado) {
                                                    fichaOcorrencia.pais = dadosApoioService.getByCodigo('TB_PAIS', 'BRASIL')
                                                } else {
                                                    fichaOcorrencia.pais = null;
                                                }*/
                                        }

                                        // validar se ponto está no Pais
                                        if (fichaOcorrencia.pais) {
                                            if (!geoService.chkPais(fichaOcorrencia.pais, geometry)) {
                                                possuiErro = true
                                                planilhaLog.push('Erro linha ' + lin.nuLinha + '. Motivo: Ponto fora do Pais ' + fichaOcorrencia.pais.noPais)
                                            }
                                        } else {
                                            // NOV-REGISTRO
                                            // não é mais necessario atualizar Paix, Ucs, Estado, Bioma, Bacia...
                                            /*
                                                fichaOcorrencia.pais = geoService.getPaisByCoord(geometry.y, geometry.x)
                                                 */
                                        }

                                        // validar se o ponto esta dentro da uc
                                        if (fichaOcorrencia.ucControle) {
                                            if (!geoService.chkUc(fichaOcorrencia.ucControle, geometry)) {
                                                possuiErro = true
                                                planilhaLog.push('Erro linha ' + lin.nuLinha + '. Motivo: Ponto fora da UC ' + fichaOcorrencia.ucControle.sgUnidade)
                                            }
                                        } else {
                                            // NOVO-REGISTRO
                                            // não é mais necessario atualizar Paix, Ucs, Estado, Bioma, Bacia...
                                            /*
                                        fichaOcorrencia.ucControle = geoService.getUcByCoord(geometry.y, geometry.x)
                                         */
                                        }

                                        // verificar se o ponto está no Municipio
                                        if (fichaOcorrencia.municipio) {
                                            // verificar se o ponto está no Municipio
                                            if (!geoService.chkMunicipio(fichaOcorrencia.municipio, geometry)) {
                                                possuiErro = true
                                                planilhaLog.push('Erro linha ' + lin.nuLinha + '. Motivo: Ponto fora do Município ' + fichaOcorrencia.municipio.noMunicipio)
                                            }
                                        } else {
                                            // pegar o Municipio pela coordenada
                                            // NOV-REGISTRO
                                            // não é mais necessario atualizar Paix, Ucs, Estado, Bioma, Bacia...
                                            /*
                                                fichaOcorrencia.municipio = geoService.getMunicipioByCoord(geometry.y, geometry.x, fichaOcorrencia.estado)
                                                */
                                        }
                                        // gravar o bioma
                                        // NOV-REGISTRO
                                        //  não é mais necessario atualizar Paix, Ucs, Estado, Bioma, Bacia...
                                        /*
                                            if (!fichaOcorrencia.bioma) {
                                                fichaOcorrencia.bioma = geoService.getBiomaSalveByCoord(geometry.y, geometry.x)
                                            }
                                            */
                                        /*
                                            // validar grupo especie com bioma MARINHO
                                            if( fichaOcorrencia.ficha.grupo && fichaOcorrencia.bioma && fichaOcorrencia.bioma.codigoSistema=='MARINHO_COSTEIRO'){
                                                if( fichaOcorrencia.ficha.grupo.codigoSistema =~ /^(COLEMBOLAS|ANFIBIOS|EFEMEROPTERAS|ODONATAS|ABELHAS|ARACNIDEOS|COLEMBOLAS|CEFALOPODAS|FORMIGAS|INVERTEBRADOS_TERRESTRES|BORBOLETAS|MARIPOSAS|MIRIAPODAS|OLIGOQUETAS|ONICOFORAS|MOLUSCOS_TERRESTRES|CARNIVOROS_TERRESTRES|CERVIDEOS|MARSUPIAIS|MORCEGOS|PERISSODACTILAS|PRIMATAS|ROEDORES|TAIASSUIDAES|XENARTRAS|CROCODILIANOS|LAGARTOS|ANFISBENAS|SERPENTES|LAGOMORFAS)$/) {
                                                    planilhaLog.push('Inconsistência linha ' + lin.nuLinha + '. O grupo ' + fichaOcorrencia.ficha.grupo.descricao + ' não pertence ao bioma Marinho.')
                                                    fichaOcorrencia.bioma=null
                                                }
                                            }
                                            // gravar a bacia hidrográfica
                                            if (!fichaOcorrencia.baciaHidrografica) {
                                                fichaOcorrencia.baciaHidrografica = geoService.getBaciaByCoord(geometry.y, geometry.x)
                                            }
                                            */
                                        //println '   Fim GEO em ' + new Date()
                                    }
                                    if (!possuiErro) {
                                        // atualizar a observação
                                        if (lin.txObservacao) {
                                            fichaOcorrencia.txObservacao = lin.txObservacao
                                        }

                                        // atualizar utilizado avaliação
                                        /*if (lin.txUsadoAvaliacao && lin.txUsadoAvaliacao == 'N') {
                                            fichaOcorrencia.inUtilizadoAvaliacao = 'N'
                                        } else {
                                            fichaOcorrencia.inUtilizadoAvaliacao = 'S'
                                        }*/

                                        // A situação padrão do registro é sempre utilizado na avaliação
                                        if( !fichaOcorrencia?.situacaoAvaliacao?.codigoSistema){
                                            fichaOcorrencia?.situacaoAvaliacao = situacaoUtilizado
                                        }

                                        if( fichaOcorrencia?.situacaoAvaliacao?.codigoSistema =~ /REGISTRO_UTILIZADO_AVALIACAO|REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO/){
                                            fichaOcorrencia.inUtilizadoAvaliacao = 'S'
                                            fichaOcorrencia.txNaoUtilizadoAvaliacao = null
                                            fichaOcorrencia.dtInvalidado = null
                                        } else {
                                            fichaOcorrencia.inUtilizadoAvaliacao = 'N'
                                        }

                                        if (lin.txTombamento) {
                                           fichaOcorrencia.txTombamento = lin.txTombamento
                                        }

                                        fichaOcorrencia.geoValidate = false
                                        // não precisa validar se o estado e municipio batem com o ponto novamente
                                        fichaOcorrencia.validate()
                                        if (fichaOcorrencia.getErrors().errorCount > 0) {
                                            //println fichaOcorrencia.getErrors();
                                            //println "-" * 100
                                            fichaOcorrencia.errors.allErrors.each {
                                                planilhaLog.push('<p>Erro: linha ' + lin.nuLinha + '<br>' + g.message(error: it) + '<p>')
                                            }
                                        } else {
                                            fichaOcorrencia.save(flush:true)
                                            (lin as PlanilhaOcorrencia).ocorrencia = fichaOcorrencia
                                            // NOV-REGISTRO
                                            // não é mais necessario atualizar Estado, Bioma, Bacia...
                                            // gravar o estado e bioma e bacias nas abas 2.1 e 2.2 e 2.3 respectivamente
                                            /*
                                            if( fichaOcorrencia.estado ) {
                                                if (!FichaOcorrencia.findByFichaAndContextoAndEstado(fichaOcorrencia.ficha, contextoEstado, fichaOcorrencia.estado)) {
                                                    new FichaOcorrencia(ficha: fichaOcorrencia.ficha, contexto: contextoEstado, estado: fichaOcorrencia.estado).save()
                                                }
                                            }
                                            if( fichaOcorrencia.bioma ) {
                                                if ( !FichaOcorrencia.findByFichaAndContextoAndBioma(fichaOcorrencia.ficha, contextoBioma, fichaOcorrencia.bioma)) {
                                                    new FichaOcorrencia(ficha: fichaOcorrencia.ficha, contexto: contextoBioma, bioma: fichaOcorrencia.bioma).save()
                                                }
                                            }
                                            if( fichaOcorrencia.baciaHidrografica ) {
                                                if (!FichaOcorrencia.findByFichaAndContextoAndBaciaHidrografica(fichaOcorrencia.ficha, contextoBacia, fichaOcorrencia.baciaHidrografica)) {
                                                    new FichaOcorrencia(ficha: fichaOcorrencia.ficha, contexto: contextoBacia, baciaHidrografica: fichaOcorrencia.baciaHidrografica).save()
                                                }
                                            }
                                            */
                                            if (publicacao) {
                                                // verificar se a referencia bibliográfica já está cadastrada
                                                FichaRefBib refBibOcorrencia = FichaRefBib.findByFichaAndSqRegistroAndNoTabelaAndNoColuna(ficha
                                                    , fichaOcorrencia.id.toInteger()
                                                    , 'ficha_ocorrencia'
                                                    , 'sq_ficha_ocorrencia')
                                                if (!refBibOcorrencia) {
                                                    refBibOcorrencia = new FichaRefBib()
                                                    refBibOcorrencia.ficha = ficha
                                                    refBibOcorrencia.sqRegistro = fichaOcorrencia.id.toInteger()
                                                    refBibOcorrencia.noTabela = 'ficha_ocorrencia'
                                                    refBibOcorrencia.noColuna = 'sq_ficha_ocorrencia'
                                                    refBibOcorrencia.deRotulo = 'ocorrência'
                                                }
                                                refBibOcorrencia.publicacao = publicacao
                                                refBibOcorrencia.nuAno = null
                                                refBibOcorrencia.noAutor = null
                                                refBibOcorrencia.save(flush:true)
                                            } else if (comPessoal && anoComPessoal.isNumber()) {
                                                // verificar se a comunicacao pessoal já está cadastrada
                                                FichaRefBib refBibOcorrencia = FichaRefBib.findByFichaAndSqRegistroAndNoTabelaAndNoColuna(ficha
                                                    , fichaOcorrencia.id.toInteger()
                                                    , 'ficha_ocorrencia'
                                                    , 'sq_ficha_ocorrencia')
                                                if (!refBibOcorrencia) {
                                                    refBibOcorrencia = new FichaRefBib()
                                                    refBibOcorrencia.ficha = ficha
                                                    refBibOcorrencia.sqRegistro = fichaOcorrencia.id.toInteger()
                                                    refBibOcorrencia.noTabela = 'ficha_ocorrencia'
                                                    refBibOcorrencia.noColuna = 'sq_ficha_ocorrencia'
                                                    refBibOcorrencia.deRotulo = 'ocorrência'
                                                }
                                                refBibOcorrencia.publicacao = null
                                                refBibOcorrencia.nuAno = anoComPessoal.toInteger()
                                                refBibOcorrencia.noAutor = comPessoal
                                                refBibOcorrencia.save(flush:true)
                                            }
/*
                                            List autores = []
                                            try {
                                                // gravar a referência bibliográfica
                                                if ( lin.idRefBibSalve || lin.txTituloRefBib || lin.deDoi) {
                                                    if (lin.idRefBibSalve) {
                                                        pub = Publicacao.get(lin.idRefBibSalve.toLong())
                                                    } else if (lin.deDoi) {
                                                        pub = Publicacao.findByDeDoi(lin.deDoi)
                                                    } else if (lin.txTituloRefBib && lin.txAutorRefBib && lin.txAnoRefBib) {
                                                        lin.txAnoRefBib = Double.parseDouble(lin.txAnoRefBib).intValue().toString()
                                                        // colocar no padrão do campo com nomes separados por "; "
                                                        autores = lin.txAutorRefBib.replaceAll(/ ?\n+ ?| ?;+ ?|&/, ';').split(';')

                                                        // localizar a referência bibliográfica
                                                        pub = null
                                                        Publicacao.createCriteria().list {
                                                            eq('nuAnoPublicacao', lin.txAnoRefBib.toInteger())
                                                            and {
                                                                autores.each { autor ->
                                                                    ilike('noAutor', '%' + autor.trim() + '%')
                                                                }
                                                            }
                                                        }.each {
                                                            String tituloA = Util.stripTags(it.deTitulo).trim().toUpperCase().replaceAll(/[^a-zA-Z0-9]/, '')
                                                            String tituloB = Util.stripTags(lin.txTituloRefBib).trim().toUpperCase().replaceAll(/[^a-zA-Z0-9]/, '')
                                                            if (tituloA.trim() == tituloB.trim()) {
                                                                pub = it
                                                            }
                                                        }
                                                    }
                                                    if ( ! pub ) {
                                                        planilhaLog.push('<p>Aviso: linha ' + lin.nuLinha + '<br>Referência bibliográfica não cadastrada no SALVE.<br>' +
                                                            '&nbsp;- ' + lin.txTituloRefBib +
                                                            '<br>&nbsp;- Ano: ' + lin.txAnoRefBib.toString() +
                                                            '<br>&nbsp;- Autor: ' + autores.join('; '))

                                                    } else {
                                                        // verificar se a referencia bibliográfica já está cadastrada
                                                        FichaRefBib refBibOcorrencia = FichaRefBib.findByFichaAndSqRegistroAndNoTabelaAndNoColuna(ficha
                                                            , fichaOcorrencia.id.toInteger()
                                                            , 'ficha_ocorrencia'
                                                            , 'sq_ficha_ocorrencia')
                                                        if (!refBibOcorrencia) {
                                                            refBibOcorrencia = new FichaRefBib()
                                                            refBibOcorrencia.ficha = ficha
                                                            refBibOcorrencia.sqRegistro = fichaOcorrencia.id.toInteger()
                                                            refBibOcorrencia.noTabela = 'ficha_ocorrencia'
                                                            refBibOcorrencia.noColuna = 'sq_ficha_ocorrencia'
                                                            refBibOcorrencia.deRotulo = 'ocorrência'
                                                        }
                                                        refBibOcorrencia.publicacao = pub
                                                        refBibOcorrencia.save()
                                                    }
                                                } else if (lin.txComunicacaoPessoal) {
                                                    String comPess = lin.txComunicacaoPessoal?.trim()
                                                    String ano = ''
                                                    // comunicação pessoal deve terminar com 4 numeros
                                                    if (comPess =~ /\d{4}$/) {
                                                        ano = comPess.substring(comPess.length() - 4)
                                                        // remover o ano do final do comPess
                                                        comPess = comPess.substring(0, comPess.length() - 4).trim().replaceAll(/,$/, '')
                                                        // verificar se a comunicacao pessoal já está cadastrada
                                                        FichaRefBib refBibOcorrencia = FichaRefBib.findByFichaAndSqRegistroAndNoTabelaAndNoColuna(ficha
                                                            , fichaOcorrencia.id.toInteger()
                                                            , 'ficha_ocorrencia'
                                                            , 'sq_ficha_ocorrencia')
                                                        if (!refBibOcorrencia) {
                                                            refBibOcorrencia = new FichaRefBib()
                                                            refBibOcorrencia.ficha = ficha
                                                            refBibOcorrencia.sqRegistro = fichaOcorrencia.id.toInteger()
                                                            refBibOcorrencia.noTabela = 'ficha_ocorrencia'
                                                            refBibOcorrencia.noColuna = 'sq_ficha_ocorrencia'
                                                            refBibOcorrencia.deRotulo = 'ocorrência'
                                                        }
                                                        refBibOcorrencia.publicacao = null
                                                        if (ano.isNumber()) {
                                                            refBibOcorrencia.nuAno = ano.toInteger()
                                                            refBibOcorrencia.noAutor = comPess
                                                            refBibOcorrencia.save()
                                                        }
                                                    } else {
                                                        planilhaLog.push('<p>Erro: linha ' + lin.nuLinha + '<br>Comunicação pessoal deve terminar com o ano. Ex: ' + comPess + ',2020')
                                                    }
                                                }
                                            } catch (Exception e) {
                                                planilhaLog.push('<p>Aviso: linha ' + lin.nuLinha + '<br>Referência bibliográfica ou Com Pess.informada está inválida.<br>Erro:' + e.getMessage())
                                            }
                                             */

                                            // Adicionar o ponto aos registros
                                            /** /
                                             println ' '
                                             println 'PONTO GRAVADO NA TABELA FICHA OCORRENCIA '
                                             println ' - iniciando gravacao da tabela REGISTRO'
                                             println '   - sqFichaOcorrencia:' + fichaOcorrencia.id.toString()
                                             /**/
                                            // NOVO-REGISTRO
                                            registroService.saveOcorrenciaSalve(fichaOcorrencia)
                                            //println ' - fim  gravacao da tabela REGISTRO'

                                            //planilhaLog.push('Ocorrência linha ' + lin.nuLinha + ' - <b>' + lin.taxon.noCientifico + '</b> gravada com <span class="green">SUCESSO!</span>')
                                            if ((++contador) % 500 == 0) {
                                                cleanUpGorm()
                                                contador = 0
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        println '-- Erro criando a Ocorrencia da linha ' + lin.nuLinha + '. Motivo:<br>' + e.getMessage()
                        planilhaLog.push('Erro criando a Ocorrência da linha ' + lin.nuLinha + '. Motivo:<br>' + e.getMessage())
                        //e.printStackTrace()
                    }
                }
                progressPosition++
                updatePercentual(tbPlanilha, (progressPosition / progressTotal * 100))
            }
            if (contador > 0) {
                cleanUpGorm()
                contador = 0
            }
        }
        tbPlanilha.txLog = planilhaLog.join('<br />');
        tbPlanilha.nuPercentual = 100
        tbPlanilha.inProcessando = false
        tbPlanilha.save(flush: true)
        // atualizar percentual de preenchimento da ficha
        listIdsFichas.each {
            fichaService.updatePercentual(it.toLong())
        }

        def batchEnded = System.currentTimeMillis()
        result.seconds = (batchEnded - result.startTime) / 1000
    }// fim importarOcorrencia .save

    /**
     * Importar planilha padrão de referências bibliográficas
     * @param tbPlanilha
     * @param pp
     * @param result
     * @param isAdm
     */
    void importarRefBib(Planilha tbPlanilha, PlanilhaParser pp, Map result, boolean isAdm)
    {
        Integer curLin = 0  // numero da linha atual do loop de linhas
        List planTitles = [] // lista com os titulos das colunas extraido da planilha
        List planilhaLog = []
        Integer anoAtual = new Date().format('yyyy').toInteger()
        Integer qtdAbas = pp.getSheetCount()
        Integer qtdLinhas = 0
        TipoPublicacao tipoPublicacao
        String locale = Locale.getDefault()
        result.errors=[]
        result.linhas = 0
        Integer contador = 0

        //pp.setHeaderRowNum(2) // linha dos titulos das colunas

        // ler aba por aba
        List sheets = pp.getSheetNames()
        sheets.eachWithIndex { nomeAba, abaNum ->
            pp.setSheet(abaNum) // selecionar a aba da planilha
            //println 'Processando aba ' + nomeAba
            if (planilhaLog.size() > 0)
            {
                planilhaLog.push('<br/>')
            }

            // lista mapeamento nome coluna x campo tabela taxonomia.Publicacao
            Map refBibMetadados = getRefBibMetadados(nomeAba)
            Map colNames = refBibMetadados.cols

            if (!colNames)
            {
                planilhaLog.push('Nome aba <span style="color:red">' + nomeAba + '</span> inválido.')
            } else
            {
                tipoPublicacao = TipoPublicacao.findByCoTipoPublicacao(refBibMetadados.tipo)
                if (!tipoPublicacao)
                {
                    planilhaLog.push('Tipo <span style="color:red">' + refBibMetadados.tipo + '</span> não cadastradado na tabela taxonomia.tipo_publicacao.')
                }
                else
                {
                    qtdLinhas = pp.getRows()
                    // 1-Ler os títulos da colunas, transformar em caixa alta e sem acentos
                    planTitles = pp.getTitles()
                    if (!planTitles)
                    {
                        planilhaLog.push('<br/>Erro lendo cabeçalho planilha ' + nomeAba + '.')
                    } else
                    {
                    planTitles.eachWithIndex { v, i ->
                            planTitles[i] = this.tratarNome(v.toString())
                        }
                        // ler as linhas
                        curLin = 3// dados começam na linha 3
                        String valor = ''
                        contador = 0
                        if (curLin > qtdLinhas)
                        {
                            planilhaLog.push('<br/>Aba ' + nomeAba + '<span class="red"> vazia!</span>')
                        } else
                        {
                            result.linhas += qtdLinhas
                            List linErros = []
                            while (curLin <= qtdLinhas)
                            {
                                // ler os valores da linha curLin
                                List data = pp.getRowData(curLin++)
                                if (data)
                                {
                                    // validar campos obrigatórios
                                    linErros = [] // zerar os erros de linha
                                    Map saveData = [:]
                                    colNames.eachWithIndex { mapCol, colIndex ->

                                        int posicao = planTitles.indexOf( mapCol.key )
                                        if( posicao == -1 )
                                        {
                                            linErros.push('coluna <b>' + mapCol.key + '</b> <span style="color:red">não identificada.</span>')
                                        }
                                        else
                                        {
                                            /**/
                                            String colValue = data[posicao].toString().trim()
                                            colValue = (colValue == 'null' ? '' : colValue)
                                            if (mapCol.value?.required && !colValue)
                                            {
                                                linErros.push('coluna <b>' + mapCol.value.label + '</b> <span style="color:red">está vazia.</span>')
                                            } else
                                            {
                                                // aqui fazer tratamento das colunas numero, data, valor etc
                                                if (colValue)
                                                {
                                                    if (mapCol.value.colName == 'nuAnoPublicacao')
                                                    {
                                                        Integer anoInformado = Double.parseDouble(colValue).intValue()
                                                        colValue = anoInformado.toString()
                                                        if (anoInformado < 1500i || anoInformado > (anoAtual+10))
                                                        {
                                                            linErros.push('coluna <b>' + mapCol.value.label + '</b> (' + colValue + ') deve estar entre <span style="color:red">1500 e ' + (anoAtual+10) + '</span>')
                                                            colValue = ''
                                                        }
                                                    } else if (mapCol.value.colName == 'noAutor')
                                                    {
                                                        String autores = colValue
                                                        colValue = ''
                                                        autores.replaceAll(/&/, ';').split(/;/).each {
                                                            colValue += it.trim().replaceAll(/\{|\}/,'') + "\n"
                                                        }
                                                    } else if( mapCol.value.colName.indexOf('dt') == 0)
                                                    {
                                                        if( ! utilityService.isDate(colValue) )
                                                        {
                                                            linErros.push('coluna <b>' + mapCol.value.label + '</b> (' + colValue + ') <span style="color:red">data inválida!</span>')
                                                            colValue = ''

                                                        }
                                                    }
                                                    if (colValue)
                                                    {
                                                        //println mapCol.value.colName+'='+colValue
                                                        saveData[mapCol.value.colName] = colValue
                                                    }
                                                }
                                            }
                                        }
                                        /**/
                                    }
                                    // se não deu nenhum erro na linha gravar na tabela
                                    String html = ''
                                    String curTableCol=''
                                    //if ( false )
                                    if ( linErros.size() == 0)
                                    {
                                        try
                                        {
                                            /**/
                                            // gravar registro
                                            Publicacao publicacao = new Publicacao()
                                            publicacao.tipoPublicacao = tipoPublicacao
                                            publicacao.inListaOficialVigente=false
                                            saveData.each {
                                                curTableCol = it.key // se der erro mostrar qual foi a coluna
                                                if( it.key.indexOf('dt') == 0 && it.value )
                                                {
                                                   publicacao[it.key] = new Date().parse('dd/MM/yyyy', it.value)
                                                }
                                                else if( it.key == 'nuAnoPublicacao' && it.value )
                                                {
                                                    publicacao[it.key] = it.value.toInteger()
                                                }
                                                else if( it.value.isNumber() )
                                                {
                                                    publicacao[it.key] = Double.parseDouble(it.value).intValue().toString();
                                                }
                                                else
                                                {
                                                    publicacao[it.key] = ( it.value == '' ? null : it.value )
                                                }
                                            }
                                            if (!publicacao.validate())
                                            {
                                                linErros.push('<br>Problema de inconsistência ao salvar registro:')
                                                publicacao.errors.allErrors.each {
                                                    linErros.push('<br>' + messageSource.getMessage(it, locale))
                                                }
                                            } else
                                            {
                                                // verificar duplicidade
                                                Publicacao publicacaoDuplicada =  Publicacao.findByTipoPublicacaoAndDeTitulo(publicacao.tipoPublicacao,publicacao.deTitulo)
                                                if( ! publicacaoDuplicada )
                                                {
                                                    publicacao.save()
                                                }
                                                else {
                                                    // atualizar somente  ano
                                                    publicacaoDuplicada.nuAnoPublicacao = publicacao.nuAnoPublicacao
                                                    publicacaoDuplicada.save()
                                                }
                                                contador++
                                                if (contador % 500 == 0)
                                                {
                                                    cleanUpGorm()
                                                    contador=0
                                                }
                                            }
                                            /**/
                                        } catch (Exception e)
                                        {
                                            linErros.push('<span style="color:red">coluna '+curTableCol+', ' + e.getMessage() + '</span>')
                                            println e.getMessage()
                                        }
                                    }
                                    if (linErros.size() > 0)
                                    {
                                        html='<br/><span style="font-size:13px; color:blue;">Erro linha: ' + (curLin - 1) + '</span>'
                                        html += '<ul>'
                                        linErros.each {
                                            html += '<li>' + it.toString() + '</li>'
                                        }
                                        html += '</ul>'
                                    }
                                    planilhaLog.push('<br/>Aba ' + nomeAba + ': <span class="blue">' + (qtdLinhas-2) + ' linha(s) / ' + linErros.size() + ' erro(s)</span>')
                                    if (html)
                                    {
                                        planilhaLog.push(html)
                                    }
                                }
                            } // end while linhas
                            if ( contador > 0 )
                            {
                                cleanUpGorm()
                                contador = 0
                            }
                        } // end if qtd linhas
                    } // end if planTitles
                } // end if tipo invalido
            } // fim if col names
            // atualizar percentual
            updatePercentual(tbPlanilha, ((abaNum + 1) / qtdAbas * 100))
        }// fim each sheets
        tbPlanilha.txLog = planilhaLog.join('')
        tbPlanilha.nuPercentual  = 100
        tbPlanilha.inProcessando = false
        tbPlanilha.save( flush:true)
        def batchEnded = System.currentTimeMillis()
        if( result.linhas > 2 )
        {
            result.linhas -= 2; // as três primeiras linhas não contam como ficha
        }
        result.seconds = (batchEnded - result.startTime) / 1000
    }

    /**
     * Importar arquivos .bib extraido do mendeley
     * @param noArquivo
     * @param conteudo
     * @param deComentario
     * @param sqUnidadeOrg
     * @param sqPessoaFisica
     * @param emailUsuario
     * @param dirPlanilhas
     * @return
     */
     Map importArquivoBib(String contexto
                         , String noArquivo
                         , String vlTamanhoArquivo
                         , String conteudo
                         , String deComentario
                         , Integer sqUnidadeOrg
                         , Integer sqPessoaFisica
                         , String emailUsuario
                         , String dirPlanilhas
    )
    {
        Map result = [status:1,msg:'',type:'error',errors:[],startTime:System.currentTimeMillis() ]
        String codigoSistema = ''
        String locale = Locale.getDefault()
        Planilha planilha
        String titulo
        Integer anoPublicacao=0

        def jsonData
        Map mapFields = ['author'     :'noAutor'
                         ,'title'      :'deTitulo'
                         ,'year'       :'nuAnoPublicacao'
                         ,'journal'    :'noRevistaCientifica'
                         ,'volume'     :'deVolume'
                         ,'number'     :'deIssue'
                         ,'doi'        :'deDoi'
                         ,'issn'       :'deIssn'
                         ,'isbn'       :'deIsbn'
                         ,'booktitle'  :'deTituloLivro'
                         ,'editor'     :'deEditores'
                         ,'publisher'  :'noEditora'
                         ,'address'    :'noCidade'
                         ,'edition'    :'nuEdicaoLivro'
                         ,'url'        :'deUrl'
                         ,'urldate'    :'dtAcessoUrl'
                         ,'school'     :'noUniversidade'
        ]
        Map mapDocs = ['article'        :'ARTIGO_CIENTIFICO'
                       ,'book'          :'LIVRO'
                       ,'incollection'  :'CAPITULO_LIVRO'
                       ,'inproceedings' :'RESUMO_CONGRESSO'
                       ,'phdthesis'     :'DISSERTACAO_MESTRADO'
                       ,'techreport'    :'RELATORIO_TECNICO'
                       ,'unpublished'   :'RELATORIO_TECNICO'
                       ,'misc'          :'SITE']
        try
        {
            jsonData = JSON.parse(conteudo)
        } catch (Exception e)
        {
            result.msg='Erro ao converter para JSON'
            return result
        }
        contexto = contexto.toUpperCase()
        planilha = Planilha.findByNoArquivoAndNoContexto( noArquivo,contexto)
        if( !planilha )
        {
            planilha = new Planilha()
            planilha.noContexto     = contexto
            planilha.noArquivo      = noArquivo
            planilha.deCaminho      = dirPlanilhas
        }
        if( planilha.inProcessando )
        {
            result.msg ='Arquivo já esta sendo processado. Andamento: '+planilha.nuPercentual.toString()+'%'
            return result
        }

        //result.msg = 'Arquivo:' + file.getOriginalFilename() + ' Tamanho: ' + (file.getSize() / 1024 / 1024).toString() + 'Mb';
        //result.msg = 'Arquivo:' + file.getOriginalFilename() + ' Tamanho: ' + (file.getSize() / 1024 / 1024).toString() + 'Mb';
        result.type = 'success'
        result.status = 0;
        result.qtdLinhasComErro = 0;
        result.colsNotFound = []; // lista de colunas não foram encontradas na planilha
        // gravar na tabela planilha
        planilha.unidade = Unidade.get( sqUnidadeOrg )
        planilha.pessoaFisica   = PessoaFisica.get( sqPessoaFisica )
        planilha.deComentario   = deComentario
        planilha.dtEnvio        = new Date()
        planilha.deCaminho      = '.'
        planilha.nuPercentual   = 0
        planilha.inProcessando  =true
        planilha.txLog          = ''
        Publicacao publicacao

        if( !planilha.save( flush:true ) )
        {
            println planilha.getErrors()
            println '-'*100
            planilha.errors.allErrors.each {
                result.errors.push( messageSource.getMessage(it, locale ) )
            }
            return result
        }
        int contador = 0
        int qtdLinhas = jsonData.size()
        Map logs =[erros:0]
        Integer anoLimite = new Date().format('yyyy').toInteger()
        jsonData.eachWithIndex {doc, linha ->
            try {
                titulo=''
                if (doc && doc[0]['entryTags']) {
                    updatePercentual(planilha, (linha / qtdLinhas * 100))
                    // validar tipo: article, book etc
                    String tipo = doc[0]['entryType'].toLowerCase()
                    if (!mapDocs[tipo]) {
                        logs.erros++
                        result.errors.push('Titulo:' + titulo + '. <span class="red">Tipo ' + tipo + ' não mapeado.</span>')
                    } else {

                        // quando for dissertaca_mestrado analisar o type
                        if (tipo == 'phdthesis') {
                            if (doc[0]['entryTags']['type']) {
                                if (doc[0]['entryTags']['type'].toLowerCase().indexOf('tese ') == 0 || doc[0]['entryTags']['type'].toLowerCase().indexOf('doutorado') > -1) {
                                    tipo = 'TESE_DOUTORADO'
                                }else if (doc[0]['entryTags']['type'].toLowerCase().indexOf('disserta') > -1 || doc[0]['entryTags']['type'].toLowerCase().indexOf('mestrado') > -1) {
                                    tipo = 'DISSERTACAO_MESTRADO'
                                }else {
                                    tipo = 'MONOGRAFIA'
                                }
                            }
                        }
                        if (mapDocs[tipo]) {
                            TipoPublicacao tipoPublicacao = TipoPublicacao.findByCoTipoPublicacao(mapDocs[tipo])
                            if (!tipoPublicacao) {
                                logs.erros++
                                result.errors.push('Titulo:' + titulo + '. <span class="red">tipo ' + mapDocs[tipo] + ' não cadastradado na tabela taxonomia.tipo_publicacao.</span>')
                            }else {
                                titulo = doc[0]['entryTags'].title?.toString()?.replaceAll(/\{|\}/, '')
                                anoPublicacao = 0
                                if (doc[0]['entryTags'].year) {
                                    try {
                                        anoPublicacao = doc[0]['entryTags'].year.toInteger()
                                    } catch (Exception e) {
                                    }
                                }
                                //println 'Titulo: '+titulo+' Ano: '+anoPublicacao
                                // verificar se já existe o titulo
                                if (anoPublicacao > 0) {
                                    publicacao = Publicacao.findByDeTituloAndTipoPublicacaoAndNuAnoPublicacao(titulo, tipoPublicacao, anoPublicacao)
                                    if (!publicacao) {
                                        try {
                                            publicacao = new Publicacao()
                                            publicacao.tipoPublicacao = tipoPublicacao
                                            publicacao.inListaOficialVigente = false
                                            mapFields.each { it ->
                                                try {
                                                    if (doc[0]['entryTags'][it.key]) {
                                                        if (it.value == 'nuAnoPublicacao') {
                                                            try {
                                                                publicacao[it.value] = doc[0]['entryTags'][it.key].toInteger()
                                                            } catch (Exception e) {
                                                                result.errors.push(tipo + ' - ' + titulo + '. <span class="red">Year ' + it.value + ' inválido.</span>')
                                                            }
                                                        }else if (it.value == 'noAutor') {
                                                            List autores = []
                                                            doc[0]['entryTags'][it.key].toString()?.replaceAll(/&/, ',')?.split(/,/)?.each {
                                                                autores.push(it.value.toString().trim())
                                                            }
                                                            publicacao[it.value] = autores.join("\n")
                                                        }else if (it.value == 'dtAcessoUrl') {
                                                            String data = doc[0]['entryTags'][it.key].toString()?.replaceAll(/\//, '-')
                                                            try {
                                                                publicacao[it.value] = new Date().parse('yyyy-MM-dd', data)
                                                            }
                                                            catch (Exception e1) {
                                                                try {
                                                                    publicacao[it.value] = new Date().parse('dd-MM-yyyy', data)
                                                                }
                                                                catch (Exception e2) {
                                                                    result.errors.push(tipo + ' - ' + titulo + '.<span class="red"> data ' + data + ' inválida.</span>')
                                                                }
                                                            }
                                                        }else if (it.value == 'deTitulo') {
                                                            publicacao[it.value] = titulo
                                                        }else {
                                                            publicacao[it.value] = doc[0]['entryTags'][it.key].toString()?.replaceAll(/\{|\}/, '')
                                                        }
                                                    }else {
                                                        publicacao[it.value] = null
                                                    }
                                                    //println it.value+' = '+doc[0]['ntryTags][it.key]
                                                } catch (Exception e) {
                                                    println e.getMessage()
                                                    logs.erros++
                                                    result.errors.push(tipo + ' - ' + titulo + '. <span class="red">campo ' + it.value + ' não existe na tabela taxonomia.tipo_publicacao.</span>')
                                                }
                                            }
                                            publicacao.nuAnoPublicacao = anoPublicacao
                                            if (publicacao.nuAnoPublicacao > (anoLimite + 1)) {
                                                logs.erros++
                                                result.errors.push(tipo + ' - ' + titulo + '. <span class="red">year ' + publicacao.nuAnoPublicacao + ' inválido.</span>')
                                            }else if (!publicacao.deTitulo) {
                                                logs.erros++
                                                result.errors.push(tipo + ' - ' + titulo + '. <span class="red">title não informado.</span>')

                                            }else if (!publicacao.noAutor) {
                                                logs.erros++
                                                result.errors.push(tipo + ' - ' + titulo + '. <span class="red">author não informado.</span>')

                                            }else {

                                                if (!publicacao.save()) {
                                                    logs.erros++
                                                    publicacao?.errors?.allErrors?.each {
                                                        println it
                                                    }
                                                }else {
                                                    if (!logs[tipo]) {
                                                        logs[tipo] = 0
                                                    }
                                                    logs[tipo]++
                                                    if ((++contador) % 100 == 0) {
                                                        //println 'Gravados 100 registros...'
                                                        cleanUpGorm()
                                                        contador = 0
                                                    }
                                                }

                                            }
                                        }
                                        catch (Exception e) {
                                            if( titulo ) {
                                                println e.getMessage()
                                                logs.erros++
                                                result.errors.push(tipo + ' - ' + titulo + '.<span class="red">-- Erro: ' + e.getMessage() + '</span>')
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch(Exception e)
            {
                if( titulo ) {
                    logs.erros++
                    result.errors.push('Titulo:' + titulo + '.<span class="red">Erro: ' + e.getMessage() + '</span>')
                    println e.getMessage()
                }
            }
        }
        if ( contador > 0 )
        {
            cleanUpGorm()
            contador=0
        }
        planilha.inProcessando=false
        planilha.nuPercentual=100
        logs.each {
            planilha.txLog += ( it.value +' ' + it.key +' encontrados(s).<br/>' )
        }
        if( result.errors )
        {
            planilha.txLog += '<br/>' + result.errors.join('<br/>')
        }
        planilha.save(flush:true)
        result.sheetName = ''
        result.linhas = jsonData.size()
        result.fileName = noArquivo
        result.fileSize = vlTamanhoArquivo
        result.tmpPath = '.'
        result.contentType = 'json'
        def batchEnded = System.currentTimeMillis()
        result.seconds = (batchEnded - result.startTime) / 1000

        if( emailUsuario )
        {
            /*if ( emailService.sendTo( emailUsuario, 'Importação de Referência Bibliográfica'
                    , 'A importação do arquivo <b>' + planilha.noArquivo + '</b> foi finalizada.<br/>' +
                    'Segue abaixo o resultado:<br/>' +planilha.txLog ) )
            {
                println 'Email enviado com sucesso para ' + emailUsuario
            }
            */
        }
        return result
    }

    protected Map getValidFichaColumns()
    {

    Map cols = [ 'NOME CIENTIFICO'                      :[colPlanilhaFicha:'txNomeCientifico'           ,'colFicha':'nmCientifico'              ,required:true ,tbApoio:null]
                 ,'AUTOR E ANO'                         :[colPlanilhaFicha:'txAutorAno'                 ,'colFicha':null                        ,required:false ,tbApoio:null]
                 ,'GRUPO'                               :[colPlanilhaFicha:'txGrupo'                    ,'colFicha':'grupo'                     ,required:false ,tbApoio:'TB_GRUPO']
                 ,'SUBGRUPO'                            :[colPlanilhaFicha:'txSubgrupo'                ,'colFicha':'subgrupo'                   ,required:false ,tbApoio:'TB_SUBGRUPO']
                 ,'CENTRO (SIGLA)'                      :[colPlanilhaFicha:'txCentro'                   ,'colFicha':null                        ,required:true  ,tbApoio:null]
                 ,'PONTO FOCAL (CPF)'                   :[colPlanilhaFicha:'txCpfPf'                    ,'colFicha':null                        ,requred:true   ,tbApoio:null]
                 ,'CT (CPF)'                            :[colPlanilhaFicha:'txCpfCt'                    ,'colFicha':null                        ,requred:false  ,tbApoio:null]
                 ,'NOME COMUM'                          :[colPlanilhaFicha:'txNomeComum'                ,'colFicha':null                        ,requred:false  ,tbApoio:null]
                 ,'SINONIMIA'                           :[colPlanilhaFicha:'txSinonimia'                ,'colFicha':null                        ,requred:false  ,tbApoio:null]
                 ,'NOTAS TAXONOMICAS'                   :[colPlanilhaFicha:'txNotasTaxonomicas'         ,'colFicha':'dsNotasTaxonomicas'        ,requred:false  ,tbApoio:null]
                 ,'NOTAS MORFOLOGICAS'                  :[colPlanilhaFicha:'txNotasMorfologicas'        ,'colFicha':'dsDiagnosticoMorfologico'  ,requred:false  ,tbApoio:null]
                 ,'ENDEMICA DO BRASIL'                  :[colPlanilhaFicha:'txEndemicaBrasil'           ,'colFicha':'stEndemicaBrasil'          ,requred:false  ,tbApoio:null]
                 ,'DISTRIBUICAO GLOBAL'                 :[colPlanilhaFicha:'txDistribuicaoGlobal'       ,'colFicha':'dsDistribuicaoGeoGlobal'   ,requred:false  ,tbApoio:null]
                 ,'DISTRIBUICAO NACIONAL'               :[colPlanilhaFicha:'txDistribuicaoNacional'     ,'colFicha':'dsDistribuicaoGeoNacional' ,requred:false  ,tbApoio:null]
                 ,'ALTITUDE MAXIMA'                     :[colPlanilhaFicha:'txAltitudeMaxima'           ,'colFicha':'nuMaxAltitude'             ,requred:false  ,tbApoio:null]
                 ,'ALTITUDE MINIMA'                     :[colPlanilhaFicha:'txAltitudeMinima'           ,'colFicha':'nuMinAltitude'             ,requred:false  ,tbApoio:null]
                 ,'BATIMETRIA MAXIMA'                   :[colPlanilhaFicha:'txBatimetriaMaxima'         ,'colFicha':'nuMaxBatimetria'        ,requred:false  ,tbApoio:null]
                 ,'BATIMETRIA MINIMA'                   :[colPlanilhaFicha:'txBatimetriaMinima'         ,'colFicha':'nuMinBatimetria'           ,requred:false  ,tbApoio:null]
                 ,'HISTORIA NATURAL'                    :[colPlanilhaFicha:'txHistoriaNatural'          ,'colFicha':'dsHistoriaNatural'         ,requred:false  ,tbApoio:null]
                 ,'ESPECIE MIGRATORIA'                  :[colPlanilhaFicha:'txEspecieMigratoria'        ,'colFicha':'stMigratoria'              ,requred:false  ,tbApoio:null]
                 ,'PADRAO DE DESLOCAMENTO'              :[colPlanilhaFicha:'txPadraoDeslocamento'       ,'colFicha':'padraoDeslocamento'        ,requred:false  ,tbApoio:'TB_PADRAO_DESLOCAMENTO']
                 ,'TENDENCIA POPULACIONAL'              :[colPlanilhaFicha:'txTendenciaPopulacional'    ,'colFicha':'tendenciaPopulacional'     ,requred:false  ,tbApoio:'TB_TENDENCIA']
                 ,'POPULACAO'                           :[colPlanilhaFicha:'txPopulacao'                ,'colFicha':'dsPopulacao'               ,requred:false  ,tbApoio:null]
                 ,'AMEACAS'                             :[colPlanilhaFicha:'txAmeaca'                   ,'colFicha':'dsAmeaca'                  ,requred:false  ,tbApoio:null]
                 ,'USOS'                                :[colPlanilhaFicha:'txUso'                      ,'colFicha':'dsUso'                     ,requred:false  ,tbApoio:null]
                 ,'PRESENCA EM LISTA OFICIAL VIGENTE'   :[colPlanilhaFicha:'txPresencaListaOficial'     ,'colFicha':'stPresencaListaVigente'    ,requred:false  ,tbApoio:null]
                 ,'ACOES DE CONSERVACAO'                :[colPlanilhaFicha:'txAcaoConservacao'          ,'colFicha':'dsAcaoConservacao'         ,requred:false  ,tbApoio:null]
                 ,'PRESENCA EM UC'                      :[colPlanilhaFicha:'txPresencaUc'               ,'colFicha':'dsPresencaUc'              ,requred:false  ,tbApoio:null]
                 ,'PESQUISAS'                           :[colPlanilhaFicha:'txPesquisa'                 ,'colFicha':'dsPesquisaExistNecessaria' ,requred:false  ,tbApoio:null]
                 ,'TIPO AVALIACAO'                      :[colPlanilhaFicha:'txTipoAvaliacao'            ,'colFicha':null                        ,requred:false  ,tbApoio:null]
                 ,'ANO'                                 :[colPlanilhaFicha:'txAnoAvaliacao'             ,'colFicha':null                        ,requred:false  ,tbApoio:null]
                 ,'ESTADO'                              :[colPlanilhaFicha:'txEstadoAvaliacao'          ,'colFicha':null                        ,requred:false  ,tbApoio:null]
                 ,'CATEGORIA'                           :[colPlanilhaFicha:'txCategoriaAvaliacao'       ,'colFicha':null                        ,requred:false  ,tbApoio:null]
                 ,'CRITERIO'                            :[colPlanilhaFicha:'txCriterioAvaliacao'        ,'colFicha':null                        ,requred:false  ,tbApoio:null]
                 ,'JUSTIFICATIVA'                       :[colPlanilhaFicha:'txJustificativaAvaliacao'   ,'colFicha':null                        ,requred:false  ,tbApoio:null]
                 ,'PENDENCIAS'                          :[colPlanilhaFicha:'txPendencias'               ,'colFicha':null                        ,requred:false  ,tbApoio:null]
                 ,'POSSIVELMENTE EXTINTA'               :[colPlanilhaFicha:'txPossivelmenteExtinta'     ,'colFicha':'stPossivelmenteExtinta'    ,requred:false  ,tbApoio:null]

        ]
        return cols;
    }

    protected Map getValidOcorrenciaColumns()
    {
        Map cols = [ 'NOME CIENTIFICO'                  :[colPlanilhaOcorrencia:'txNomeCientifico'              ,'colFichaOcorrencia':null                      ,required:true ,tbApoio:null]
                    ,'LATITUDE'                         :[colPlanilhaOcorrencia:'txLatitude'                    ,'colFichaOcorrencia':null                      ,required:true ,tbApoio:null]
                    ,'LONGITUDE'                        :[colPlanilhaOcorrencia:'txLongitude'                   ,'colFichaOcorrencia':null                      ,required:true ,tbApoio:null]
                    ,'PRAZO DE CARENCIA'                :[colPlanilhaOcorrencia:'txPrazoCarencia'               ,'colFichaOcorrencia':'prazoCarencia'           ,required:true ,tbApoio:'TB_PRAZO_CARENCIA']
                    ,'DATUM'                            :[colPlanilhaOcorrencia:'txDatum'                       ,'colFichaOcorrencia':'datum'                   ,required:true ,tbApoio:'TB_DATUM']
                    ,'FORMATO ORIGINAL'                 :[colPlanilhaOcorrencia:'txFormatoOriginal'             ,'colFichaOcorrencia':'formatoCoordOriginal'    ,required:false,tbApoio:'TB_FORMATO_COORDENADA_ORIGINAL']
                    ,'PRECISAO DA COORDENADA'           :[colPlanilhaOcorrencia:'txPrecisaoCoordenada'          ,'colFichaOcorrencia':'precisaoCoordenada'      ,required:true ,tbApoio:'TB_PRECISAO_COORDENADA']
                    ,'METODOLOGIA DE APROXIMACAO'       :[colPlanilhaOcorrencia:'txMetodoAproximacao'           ,'colFichaOcorrencia':'metodoAproximacao'       ,required:false ,tbApoio:'TB_METODO_APROXIMACAO']
                    ,'DESCRICAO METODO DE APROXIMACAO'  :[colPlanilhaOcorrencia:'txDescricaoMetodoAproximacao'  ,'colFichaOcorrencia':'txMetodoAproximacao'     ,required:false ,tbApoio:null]
                    ,'REFERENCIA DE APROXIMACAO'        :[colPlanilhaOcorrencia:'txReferenciaAproximacao'       ,'colFichaOcorrencia':'refAproximacao'          ,required:false ,tbApoio:'TB_REFERENCIA_APROXIMACAO']
                    ,'TAXON ORIGINALMENTE CITADO'       :[colPlanilhaOcorrencia:'txTaxonOriginalCitado'         ,'colFichaOcorrencia':null                      ,required:false ,tbApoio:null]
                    ,'PRESENCA ATUAL'                   :[colPlanilhaOcorrencia:'txPresencaAtual'               ,'colFichaOcorrencia':'inPresencaAtual'         ,required:false ,tbApoio:null]
                    ,'TIPO DE REGISTRO'                 :[colPlanilhaOcorrencia:'txTipoRegistro'                ,'colFichaOcorrencia':'tipoRegistro'            ,required:false ,tbApoio:'TB_TIPO_REGISTRO_OCORRENCIA']
                    ,'DIA'                              :[colPlanilhaOcorrencia:'txDia'                         ,'colFichaOcorrencia':'nuDiaRegistro'           ,required:false ,tbApoio:null]
                    ,'MES'                              :[colPlanilhaOcorrencia:'txMes'                         ,'colFichaOcorrencia':'nuMesRegistro'           ,required:false ,tbApoio:null]
                    ,'ANO'                              :[colPlanilhaOcorrencia:'txAno'                         ,'colFichaOcorrencia':'nuAnoRegistro'           ,required:false ,tbApoio:null]
                    ,'HORA'                             :[colPlanilhaOcorrencia:'txHora'                        ,'colFichaOcorrencia':'hrRegistro'              ,required:false ,tbApoio:null]
                    ,'DATA E ANO DESCONHECIDOS'         :[colPlanilhaOcorrencia:'txDataAnoDesconhecidos'        ,'colFichaOcorrencia':'inDataDesconhecida'      ,required:false ,tbApoio:null]
                    ,'LOCALIDADE'                       :[colPlanilhaOcorrencia:'txLocalidade'                  ,'colFichaOcorrencia':'noLocalidade'            ,required:false ,tbApoio:null]
                    ,'CARACTERISTICA DA LOCALIDADE'     :[colPlanilhaOcorrencia:'txCaracteristicaLocalidade'    ,'colFichaOcorrencia':'txLocal'                 ,required:false ,tbApoio:null]
                    ,'UC FEDERAIS'                      :[colPlanilhaOcorrencia:'txUcFederal'                   ,'colFichaOcorrencia':'sq_uc_federal'           ,required:false ,tbApoio:null]
                    ,'UC NAO FEDERAIS'                  :[colPlanilhaOcorrencia:'txUcEstadual'                  ,'colFichaOcorrencia':'sq_uc_estadual'          ,required:false ,tbApoio:null]
                    ,'RPPN'                             :[colPlanilhaOcorrencia:'txRppn'                        ,'colFichaOcorrencia':'sq_rppn'                 ,required:false ,tbApoio:null]
                    ,'PAIS'                             :[colPlanilhaOcorrencia:'txPais'                        ,'colFichaOcorrencia':null                      ,required:false ,tbApoio:null]
                    ,'ESTADO'                           :[colPlanilhaOcorrencia:'txEstado'                      ,'colFichaOcorrencia':null                      ,required:false ,tbApoio:null]
                    ,'MUNICIPIO'                        :[colPlanilhaOcorrencia:'txMunicipio'                   ,'colFichaOcorrencia':null                      ,required:false ,tbApoio:null]
                    ,'AMBIENTE'                         :[colPlanilhaOcorrencia:'txAmbiente'                    ,'colFichaOcorrencia':null                      ,required:false ,tbApoio:null]
                    ,'HABITAT'                          :[colPlanilhaOcorrencia:'txHabitat'                     ,'colFichaOcorrencia' :'habitat'                ,required:false ,tbApoio:'TB_HABITAT']
                    ,'ELEVACAO MINIMA'                  :[colPlanilhaOcorrencia:'txElevacaoMinima'              ,'colFichaOcorrencia' :'vlElevacaoMin'          ,required:false ,tbApoio:null]
                    ,'ELEVACAO MAXIMA'                  :[colPlanilhaOcorrencia:'txElevacaoMaxima'              ,'colFichaOcorrencia' :'vlElevacaoMax'          ,required:false ,tbApoio:null]
                    ,'PROFUNDIDADE MINIMA'              :[colPlanilhaOcorrencia:'txProfundidadeMinima'          ,'colFichaOcorrencia' :'vlProfundidadeMin'      ,required:false ,tbApoio:null]
                    ,'PROFUNDIDADE MAXIMA'              :[colPlanilhaOcorrencia:'txProfundidadeMaxima'          ,'colFichaOcorrencia' :'vlProfundidadeMax'      ,required:false ,tbApoio:null]
                    ,'IDENTIFICADOR'                    :[colPlanilhaOcorrencia:'txIdentificador'               ,'colFichaOcorrencia' :'deIdentificador'        ,required:false ,tbApoio:null]
                    ,'DATA IDENTIFICACAO'               :[colPlanilhaOcorrencia:'txDataIdentificacao'           ,'colFichaOcorrencia' :'dtIdentificacao'        ,required:false ,tbApoio:null]
                    ,'IDENTIFICACAO INCERTA'            :[colPlanilhaOcorrencia:'txIndicacaoIncerta'            ,'colFichaOcorrencia' :'qualificadorValTaxon'   ,required:false ,tbApoio:'TB_QUALIFICADOR_TAXON']
                    ,'TOMBAMENTO'                       :[colPlanilhaOcorrencia:'txTombamento'                  ,'colFichaOcorrencia' :'txTombamento'           ,required:false ,tbApoio:null]
                    ,'INSTITUICAO TOMBAMENTO'           :[colPlanilhaOcorrencia:'txInstituicaoTombamento'       ,'colFichaOcorrencia' :'txInstituicao'          ,required:false ,tbApoio:null]
                    ,'COMPILADOR'                       :[colPlanilhaOcorrencia:'txCompilador'                  ,'colFichaOcorrencia' :null                     ,required:false ,tbApoio:null]
                    ,'DATA COMPILACAO'                  :[colPlanilhaOcorrencia:'txDataCompilacao'              ,'colFichaOcorrencia' :'dtCompilacao'           ,required:false ,tbApoio:null]
                    ,'REVISOR'                          :[colPlanilhaOcorrencia:'txRevisor'                     ,'colFichaOcorrencia' :null                     ,required:false ,tbApoio:null]
                    ,'DATA REVISAO'                     :[colPlanilhaOcorrencia:'txDataRevisao'                 ,'colFichaOcorrencia' :'dtRevisao'              ,required:false ,tbApoio:null]
                    ,'VALIDADOR'                        :[colPlanilhaOcorrencia:'txValidador'                   ,'colFichaOcorrencia' :null                     ,required:false ,tbApoio:null]
                    ,'DATA VALIDACAO'                   :[colPlanilhaOcorrencia:'txDataValidacao'               ,'colFichaOcorrencia' :'dtValidacao'            ,required:false ,tbApoio:null]
                    //,'USADO AVALIACAO?'                 :[colPlanilhaOcorrencia:'txUsadoAvaliacao'              ,'colFichaOcorrencia' :'inUtilizadoAvaliacao'   ,required:false ,tbApoio:null]
                    ,'SITUACAO DO REGISTRO'             :[colPlanilhaOcorrencia:'txUsadoAvaliacao'              ,'colFichaOcorrencia' :'situacaoAvaliacao'      ,required:false ,tbApoio:'TB_SITUACAO_REGISTRO_OCORRENCIA']
                    ,'JUSTIFICATIVA (QUANDO NAO USADO)' :[colPlanilhaOcorrencia:'txJustificativaNaoUso'         ,'colFichaOcorrencia' :'txNaoUtilizadoAvaliacao'  ,required:false ,tbApoio:null]
                    ,'ID ORIGEM'                        :[colPlanilhaOcorrencia:'idOrigem'                      ,'colFichaOcorrencia' :'idOrigem'               ,required:false ,tbApoio:null]
                    ,'ID REF BIB SALVE'                 :[colPlanilhaOcorrencia:'idRefBibSalve'                 ,'colFichaOcorrencia' :null                     ,required:false ,tbApoio:null]
                    ,'DOI'                              :[colPlanilhaOcorrencia:'deDoi'                         ,'colFichaOcorrencia' :null                     ,required:false ,tbApoio:null]
                    ,'TITULO REF BIB'                   :[colPlanilhaOcorrencia:'txTituloRefBib'                ,'colFichaOcorrencia' :null                     ,required:false ,tbApoio:null]
                    ,'AUTOR REF BIB'                    :[colPlanilhaOcorrencia:'txAutorRefBib'                 ,'colFichaOcorrencia' :null                     ,required:false ,tbApoio:null]
                    ,'ANO REF BIB'                      :[colPlanilhaOcorrencia:'txAnoRefBib'                   ,'colFichaOcorrencia' :null                     ,required:false ,tbApoio:null]
                    ,'COMUNICACAO PESSOAL'              :[colPlanilhaOcorrencia:'txComunicacaoPessoal'          ,'colFichaOcorrencia' :null                     ,required:false ,tbApoio:null]
                    ,'OBSERVACAO'                       :[colPlanilhaOcorrencia:'txObservacao'                  ,'colFichaOcorrencia' :'txObservacao'           ,required:false ,tbApoio:null]
        ]
        return cols;
    }

    protected Map getRefBibMetadados( String nomeAba = '' )
    {
        Map cols = ['artigocientifico':[tipo:'ARTIGO_CIENTIFICO', cols:[
             ['Autor(es)'            :['colName':'noAutor','required':true] ]
            ,['Título'              :['colName':'deTitulo','required':true] ]
            ,['Ano Publicação'      :['colName':'nuAnoPublicacao','required':true] ]
            ,['Revista Científica'  :['colName':'noRevistaCientifica']]
            ,['Volume'              :['colName':'deVolume']]
            ,['Página(s)'          :['colName':'dePaginas']]
            ,['Nº DOI'              :['colName':'deDoi']]
            ,['Nº ISSN'             :['colName':'deIssn']]
            ]],
            'atooficial':[tipo:'ATO_OFICIAL', cols:[
             ['Autor(es)':['colName':'noAutor','required':true] ]
            ,['Título'              :['colName':'deTitulo','required':true] ]
            ,['Ano Publicação'      :['colName':'nuAnoPublicacao','required':true] ]
            ,['Revista Científica'  :['colName':'noRevistaCientifica']]
            ,['Volume'              :['colName':'deVolume']]
            ,['Nº Páginas'          :['colName':'dePaginas']]
            ,['Url de Acesso'       :['colName':'deUrl']]
            ,['Data de Acesso'      :['colName':'dtAcessoUrl']]
            ,['Nº DOI'              :['colName':'deDoi']]
            ,['Nº ISSN'             :['colName':'deIssn']]
            ]],
            'bancodedadosinstitucional':[tipo:'BANCO_DADOS_INSTITUCIONAL', cols:[
             ['Autor(es)':['colName':'noAutor','required':true] ]
            ,['Título'              :['colName':'deTitulo','required':true] ]
            ,['Ano Publicação'      :['colName':'nuAnoPublicacao','required':true] ]
            ,['Url de Acesso'       :['colName':'deUrl']]
            ,['Data de Acesso'      :['colName':'dtAcessoUrl']]
            ]],
            'capitulodelivro':[tipo:'CAPITULO_LIVRO', cols:[
             ['Autor(es)'           :['colName':'noAutor','required':true] ]
            ,['Título'              :['colName':'deTitulo','required':true] ]
            ,['Ano Publicação'      :['colName':'nuAnoPublicacao','required':true] ]
            ,['Nº Páginas'          :['colName':'dePaginas']]
            ,['Título do Livro'     :['colName':'deTituloLivro']]
            ,['Editor(es)'          :['colName':'deEditores','required':true]]
            ,['Editora'             :['colName':'noEditora']]
            ,['Cidade'              :['colName':'noCidade']]
            ,['Nº Edição do Livro'  :['colName':'nuEdicaoLivro']]
            ,['Nº DOI'              :['colName':'deDoi']]
            ,['Nº ISBN'             :['colName':'deIsbn']]
            ]],
            'dissertacaodemestrado':[tipo:"DISSERTACAO_MESTRADO", cols:[
             ['Autor(es)'           :['colName':'noAutor','required':true] ]
            ,['Título'              :['colName':'deTitulo','required':true] ]
            ,['Ano Publicação'      :['colName':'nuAnoPublicacao','required':true] ]
            ,['Nº Páginas'          :['colName':'dePaginas']]
            ,['Cidade'              :['colName':'noCidade']]
            ,['Universidade'        :['colName':'noUniversidade']]
            ]],
            'livro':[tipo:'LIVRO', cols:[
             ['Autor(es)'           :['colName':'noAutor','required':true] ]
            ,['Título'              :['colName':'deTitulo','required':true] ]
            ,['Ano Publicação'      :['colName':'nuAnoPublicacao','required':true] ]
            ,['Nº Páginas'          :['colName':'dePaginas']]
            ,['Editora'             :['colName':'noEditora']]
            ,['Cidade'              :['colName':'noCidade']]
            ,['Nº Edição do Livro'  :['colName':'nuEdicaoLivro']]
            ,['Nº DOI'              :['colName':'deDoi']]
            ,['Nº ISBN'             :['colName':'deIsbn']]
            ]],
            'midiadigital':[tipo:'MIDIA_DIGITAL', cols:[
             ['Autor(es)'           :['colName':'noAutor','required':true] ]
            ,['Título'              :['colName':'deTitulo','required':true] ]
            ,['Ano Publicação'      :['colName':'nuAnoPublicacao','required':true] ]
            ,['Volume'              :['colName':'deVolume']]
            ,['Nº Páginas'          :['colName':'dePaginas']]
            ,['Editora'             :['colName':'noEditora']]
            ,['Cidade'              :['colName':'noCidade']]
            ,['Url de Acesso'       :['colName':'deUrl']]
            ,['Data de Acesso'      :['colName':'dtAcessoUrl']]
            ,['Nº DOI'              :['colName':'deDoi']]
            ,['Nº ISSN'             :['colName':'deIssn']]
            ]],
            'monografia':[tipo:'MONOGRAFIA', cols:[
             ['Autor(es)'           :['colName':'noAutor','required':true] ]
            ,['Título'              :['colName':'deTitulo','required':true] ]
            ,['Ano Publicação'      :['colName':'nuAnoPublicacao','required':true] ]
            ,['Nº Páginas'          :['colName':'dePaginas']]
            ,['Cidade'              :['colName':'noCidade']]
            ,['Universidade'        :['colName':'noUniversidade']]
            ]],
            'relatoriotecnico':[tipo:"RELATORIO_TECNICO", cols:[
             ['Autor(es)'           :['colName':'noAutor','required':true] ]
            ,['Título'              :['colName':'deTitulo','required':true] ]
            ,['Ano Publicação'      :['colName':'nuAnoPublicacao','required':true] ]
            ,['Nº Páginas'          :['colName':'dePaginas']]
            ,['Editora'             :['colName':'noEditora']]
            ,['Cidade'              :['colName':'noCidade']]
            ,['Url de Acesso'       :['colName':'deUrl']]
            ,['Data de Acesso'      :['colName':'dtAcessoUrl']]
            ,['Nº DOI'              :['colName':'deDoi']]
            ,['Nº ISBN'             :['colName':'deIsbn']]
            ]],
            'resumosdecongresso':[tipo:"RESUMO_CONGRESSO", cols:[
             ['Autor(es)'           :['colName':'noAutor','required':true] ]
            ,['Título'              :['colName':'deTitulo','required':true] ]
            ,['Ano Publicação'      :['colName':'nuAnoPublicacao','required':true] ]
            ,['Nº Páginas'          :['colName':'dePaginas']]
            ,['Título do Congresso' :['colName':'deTituloLivro']]
            ,['Editor(es)'          :['colName':'deEditores','required':true]]
            ,['Editora'             :['colName':'noEditora']]
            ,['Cidade'              :['colName':'noCidade']]
            ,['Url de Acesso'       :['colName':'deUrl']]
            ,['Data de Acesso'      :['colName':'dtAcessoUrl']]
            ,['Nº DOI'              :['colName':'deDoi']]
            ,['Nº ISBN'             :['colName':'deIsbn']]
            ]],
            'sitioeletronico':[tipo:"SITE", cols:[
             ['Autor(es)'           :['colName':'noAutor','required':true] ]
            ,['Título'              :['colName':'deTitulo','required':true] ]
            ,['Ano Publicação'      :['colName':'nuAnoPublicacao','required':true] ]
            ,['Título da Publicação':['colName':'deTituloLivro']]
            ,['Url de Acesso'       :['colName':'deUrl']]
            ,['Data de Acesso'      :['colName':'dtAcessoUrl']]
            ,['Nº DOI'              :['colName':'deDoi']]
            ,['Nº ISSN'             :['colName':'deIssn']]
            ]],
            'tesededoutorado' :[tipo:"TESE_DOUTORADO", cols:[
             ['Autor(es)'           :['colName':'noAutor','required':true] ]
            ,['Título'              :['colName':'deTitulo','required':true] ]
            ,['Ano Publicação'      :['colName':'nuAnoPublicacao','required':true] ]
            ,['Nº Páginas'          :['colName':'dePaginas']]
            ,['Cidade'              :['colName':'noCidade']]
            ,['Universidade'        :['colName':'noUniversidade']]
            ]]
        ]
        nomeAba = this.tratarNome( nomeAba )
        if( !cols[ nomeAba ])
        {
            return [:]
        }
        Map metadados = [tipo : cols[ nomeAba ].tipo, cols:[:]]
        cols[ nomeAba ].cols.each {
            it.each {
                it.value.label = it.key // guardar o titulo acentuado
                metadados.cols.put(this.tratarNome(it.key), it.value)
            }
        }
        return metadados
    }

    void cleanUpGorm() {
        def session = sessionFactory.currentSession
        session.flush()
        session.clear()
        propertyInstanceMap.get().clear()
    }

    /*
        metodo para corrigir o cpf vindo da planilha como decimal em notação científica '2.97567838E10'
    */
    String tratarCpf( String cpf)
    {
        String cpfTemp
        if( cpf.indexOf('E') > -1 )
        {
           // completar com zeros à direita
           cpfTemp = cpf.replaceAll(/\./,'').split('E')[0].padRight(11, '0')
           if( !utilityService.isCpf( cpfTemp ) )
           {
               // completar com zeros à esquerda
               cpfTemp = cpf.replaceAll(/\./,'').split('E')[0].padLeft(11, '0')
               if( utilityService.isCpf( cpfTemp ) )
                {
                   cpf = cpfTemp
                }
           }
           else
           {
                cpf = cpfTemp
           }
        }
        cpf = cpf.replaceAll(/[^0-9]/,'')
        return cpf
    }

    void updatePercentual( Planilha planilha, BigDecimal percentual )
    {
        /*if( planilha )
        {
            if( planilha.inProcessando )
            {
                Integer intPercentual = percentual
                if( intPercentual != planilha.nuPercentual )
                {
                    //println '  - Atualizdo percentual de '+planilha.nuPercentual+' para '+ intPercentual
                    //println ' '
                    planilha.nuPercentual  = intPercentual
                    planilha.inProcessando = ( intPercentual < 100)
                    planilha.save( flush : true )
                }
            }
        }
        */

        Planilha.withNewSession { session ->
            Planilha p =Planilha.get( planilha.id )
            if( p )
            {
                if( p.inProcessando )
                {
                    Integer intPercentual = percentual
                    if( intPercentual != p.nuPercentual )
                    {
                        p.nuPercentual  = intPercentual
                        p.inProcessando = ( intPercentual < 100)
                        p.save( flush : true )
                        //print ' ' + p.nuPercentual + '%'
                    }
                }
            }
            p=null
        }
    }

    /**
     * Remover caracteres irregulares, remover acentos e conveter para minusculas
     * @param nome
     * @return
     */
    String tratarNome( String nome )
    {
        // cortar a partir do parenteses
        nome = nome.split('\\(')[0].trim()
        nome = nome.replaceAll(' |\\(|\\)|-|º|ª|\\.|,','')
        return utilityService.removeAccents(nome.toLowerCase())
    }

    String scape2Regex(String texto)
    {
        String specialCharRegex = /[\W_&&[^\s]().]/;
        return texto.replaceAll(specialCharRegex, /\\$0/)
    }
}
