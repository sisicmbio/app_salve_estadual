package br.gov.icmbio

import grails.transaction.Transactional
import groovy.json.internal.LazyMap

import java.nio.charset.StandardCharsets
import java.nio.file.Files
import groovy.json.JsonSlurper

// necessário para o trust manager - HTTPS
import groovyx.net.http.*
import javax.net.ssl.TrustManager
import javax.net.ssl.SSLContext
import javax.net.ssl.X509TrustManager
import java.security.cert.X509Certificate
import org.apache.http.conn.ssl.SSLSocketFactory
import org.apache.http.conn.scheme.Scheme

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;


/**
* Classe de integração com DOI CrossRef
* https://www.crossref.org/
*/

@Transactional
class DoiService {

    def grailsApplication
    def dadosApoioService
    def sqlService
    def idsFicha
    def tipoIdentificacaoDoi
    String msgError = ''
    String email = 'carlosrar@gmail.com'
    //String email = 'luiseugeniobarbosa@gmail.com'

    /**
    * Envia o DOI/XML para o CrossRef
    */
    def Map enviarDoiFicha(String ids = '') {
        Map res = [ status: 0, msg: '', type: 'success']

        idsFicha = ids
        tipoIdentificacaoDoi = 'ficha' //Ex. da estrutura DOI modelo: 10.37002/salve.{ficha}.123456 OU 10.37002/salve.{documentox}.123456

        def error = _doPostDoi()
        if(error){
            res.status  = 1
            res.msg     = error
            res.type    = 'error'
        }
        return res
    }

    /**
    * Realiza o post dos dados
    */
    protected String _doPostDoi(){
        //////////////////////////////// Para teste: Tricorythodes mirca
        URLConnection connection    = null
        String operation            = grailsApplication.config.doi.crossref.operation
        String login_id             = grailsApplication.config.doi.crossref.login_id
        String login_passwd         = grailsApplication.config.doi.crossref.login_passwd
        String url                  = grailsApplication.config.doi.crossref.url
        List listFichasAtualizarDoi = []
        def sslContext              = null;
        def socketFactory           = null;
        def httpsScheme             = null;

        try {
            msgError = ''

            // cria e salva o conteúdo no arquivo
            String fileName = "/data/salve-estadual/temp/dataset-${tipoIdentificacaoDoi}-${new Date().format('dd-MM-yyyy-HH:mm:ss')}.xml"

            def contentXml = _templateDoiXml( listFichasAtualizarDoi )

            if( contentXml == '' ){
                msgError = 'Não foi possível gerar conteúdo para o arquivo XML do DOI/Crossref! Verifique se o conteúdo da(s) ficha(s) atendem os requisitos (publicada, citação e data da publicação).'
                Util.printLog('ERRO::SALVE::DoiService._doPostDoi()', msgError);
                throw new Exception( msgError )
            }

            // força validação de certificado
            if( url.indexOf('https://') == 0 )
            {
                TrustManager[] trustAllCerts = [
                new X509TrustManager() {
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {return null}
                        public void checkClientTrusted(X509Certificate[] certs, String authType) {}
                        public void checkServerTrusted(X509Certificate[] certs, String authType) {}
                    }
                ]
                sslContext = SSLContext.getInstance("SSL");
                sslContext.init(null, trustAllCerts, new java.security.SecureRandom());

                socketFactory = new SSLSocketFactory(sslContext, SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
                httpsScheme = new Scheme("https", socketFactory, 443);

                HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
                // Create all-trusting host name verifier
                HostnameVerifier allHostsValid = new HostnameVerifier() {
                    @Override
                    public boolean verify(String hostname, SSLSession session) {
                        return true;
                    }
                };
                // Install the all-trusting host verifier
                HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
            }

            connection = new URL(url).openConnection();

            new File(fileName).withWriter('utf-8') {
                writer -> writer.writeLine contentXml
            }
            File textFile = new File( fileName );
            String boundary = Long.toHexString(System.currentTimeMillis());
            String CRLF = "\r\n"; // Line separator required by multipart/form-data.

            connection.setRequestMethod("POST");
            connection.setDoOutput(true); // indicates POST method
            connection.setDoInput(true);
            connection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);

            OutputStream output = connection.getOutputStream();
            PrintWriter writer = new PrintWriter(new OutputStreamWriter(output, StandardCharsets.UTF_8), true);

            // campo operation.
            writer.append("--" + boundary).append(CRLF);
            writer.append("Content-Disposition: form-data; name=\"operation\"").append(CRLF);
            writer.append("Content-Type: text/plain; charset=" + StandardCharsets.UTF_8).append(CRLF);
            writer.append(CRLF).append(operation).append(CRLF).flush();
            // campo login_id.
            writer.append("--" + boundary).append(CRLF);
            writer.append("Content-Disposition: form-data; name=\"login_id\"").append(CRLF);
            writer.append("Content-Type: text/plain; charset=" + StandardCharsets.UTF_8).append(CRLF);
            writer.append(CRLF).append(login_id).append(CRLF).flush();
            // campo login_passwd.
            writer.append("--" + boundary).append(CRLF);
            writer.append("Content-Disposition: form-data; name=\"login_passwd\"").append(CRLF);
            writer.append("Content-Type: text/plain; charset=" + StandardCharsets.UTF_8).append(CRLF);
            writer.append(CRLF).append(login_passwd).append(CRLF).flush();
            // campo login_passwd.1673
            writer.append("--" + boundary).append(CRLF);
            writer.append("Content-Disposition: form-data; name=\"login_passwd\"").append(CRLF);
            writer.append("Content-Type: text/plain; charset=" + StandardCharsets.UTF_8).append(CRLF);
            writer.append(CRLF).append(login_passwd).append(CRLF).flush();
            // campo file fname
            writer.append("--" + boundary).append(CRLF);
            writer.append("Content-Disposition: form-data; name=\"fname\"; filename=\"" + textFile.getName() + "\"").append(CRLF);
            writer.append("Content-Type: text/plain; charset=" +  StandardCharsets.UTF_8).append(CRLF); // Text file itself must be saved in this charset!
            writer.append(CRLF).flush();
            Files.copy(textFile.toPath(), output); output.flush()
            writer.append(CRLF).flush(); // CRLF is important! It indicates end of boundary.

            // End of multipart/form-data.
            writer.append("--" + boundary + "--").append(CRLF).flush();

            def postRC = connection.getResponseCode()
            //println 'postRC::::::::::::::::: '+postRC;

            String resp = ''
            if (postRC.equals(200)) {
                resp = connection.getInputStream().getText()
                //println 'RESP::::::::::::: '+resp
                if ( resp.contains('SUCCESS') ){
                    /*
                    // atualizar as campo ds_dos das fichas no banco de dados
                    // não precisa isso está sendo feito pelo DoiJob diariamente
                    List listSqlUpdates = []
                    listFichasAtualizarDoi.each{
                        listSqlUpdates.push("""update salve.ficha set ds_doi ='${it.dsDoi}' where sq_ficha=${it.sqFicha};""")
                    }
                    sqlService.execSql( listSqlUpdates.join('\n') )
                    */
                    msgError = ''
                }else{
                    msgError = 'O arquivo XML contendo o dataset não foi validado pelo crossref.org!'
                    Util.printLog('ERRO::SALVE::DoiService._doPostDoi()', msgError + ' COD: ' + postRC.toString() + connection.getResponseMessage().toString() + resp.toString()  );
                }
            }else{
                msgError = 'Erro na conexão/envio do dataset para crossref.org!'
                Util.printLog('ERRO::SALVE::DoiService._doPostDoi()', msgError + ' COD: ' + connection.getResponseCode().toString() + connection.getResponseMessage().toString() + resp.toString() );
            }

        } catch( Exception e ) {
            msgError = 'Ocorreu um erro inesperado ao gerar/enviar DOI! '+e.getMessage()
            Util.printLog('ERRO::SALVE::DoiService._doPostDoi()', msgError, e.getMessage() );
        }
        // fechar a conexão http
        if( connection ){
            connection.disconnect();
        }
        // fim envio xml para crossref
        return msgError
    }

    /**
    * Retorna o template XML do DOI
    */
    protected String _templateDoiXml( List listFichasAtualizarDoi ){
        String recordsDoiXml = _templateRecordDoiXml( listFichasAtualizarDoi )
        if(recordsDoiXml == ''){
            return '';
        }

        // TODO substituir o email ${grailsApplication.config.app.email}
        String doiXml = """<?xml version="1.0" encoding="UTF-8"?>
        <doi_batch xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.crossref.org/schema/5.3.0 https://www.crossref.org/schemas/crossref5.3.0.xsd" xmlns="http://www.crossref.org/schema/5.3.0" xmlns:jats="http://www.ncbi.nlm.nih.gov/JATS1" xmlns:fr="http://www.crossref.org/fundref.xsd" xmlns:mml="http://www.w3.org/1998/Math/MathML" version="5.3.0">
            <head>
                <doi_batch_id>${new Date().format('dd-MM-yyyy-HH:mm:ss')}</doi_batch_id>
                <timestamp>${new Date().getTime()}</timestamp>
                <depositor>
                    <depositor_name>Sistema de Avaliação do Risco de Extinção da Biodiversidade - SALVE</depositor_name>
                    <email_address>${email}</email_address>
                </depositor>
                <registrant>Instituto Chico Mendes de Conservação da Biodiversidade - ICMBio</registrant>
            </head>
            <body>
                <database>
                    <database_metadata language="pt">
                        <titles>
                            <title>Datasets - Sistema SALVE - ICMBio</title>
                        </titles>
                        <institution>
                            <institution_name>Instituto Chico Mendes de Conservação da Biodiversidade - ICMBio</institution_name>
                        </institution>
                    </database_metadata>
                    ${recordsDoiXml}
                </database>
            </body>
        </doi_batch>
        """
        return doiXml
    }

    /**
    * Retorna o template do bloco record XML do DOI
    */
    protected String _templateRecordDoiXml(List listFichasAtualizarDoi ) {
        String prefixICMBio = grailsApplication.config.doi.crossref.prefix_icmbio

        List sqlWherePasso1 = []
        Map sqlParams = [:]
        DadosApoio situacaoPublicada = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'PUBLICADA')

        // filtrar pelos ids selecionados
        if( idsFicha ) {
            if( idsFicha.indexOf(',') == -1 ) {
                sqlWherePasso1.push("ficha.sq_ficha = :sqFicha")
                sqlParams.sqFicha = idsFicha.toLong()
            } else {
                sqlWherePasso1.push("ficha.sq_ficha = any( array[${idsFicha}] )")
            }
        }
        // fim construcao dos filtros

        String cmdSql="""SELECT ficha.sq_ficha
                , coalesce(taxon.json_trilha -> 'subespecie' ->> 'no_taxon'
                , taxon.json_trilha -> 'especie' ->> 'no_taxon') AS nm_cientifico
                , ficha.ds_criterio_aval_iucn_final as de_criterio
                , ficha.ds_doi
                , ficha.dt_publicacao
                , ficha.ds_equipe_tecnica
                , ficha.ds_colaboradores
                , ficha.ds_citacao
            FROM salve.ficha
            INNER JOIN taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
            WHERE ficha.ds_citacao IS NOT NULL
                -- AND ficha.ds_doi IS NULL          -- # PARA PEMITIR FOÇAR ATUALIZAR DOI
                AND ficha.dt_publicacao IS NOT NULL
                AND ficha.sq_situacao_ficha = ${situacaoPublicada.id.toString()}
                ${sqlWherePasso1 ? '\nAND ' + sqlWherePasso1.join('\nand ') : ''}
            """
        /** /
        println cmdSql
        println sqlParams
        /**/

        String doiRecordXml = ''
        String xmlAutores
        def identificacao_doi
        def url_ficha_pdf
        def possuiAutores = false
        List lista = sqlService.execSql( cmdSql, sqlParams ).each{ row ->

            // gerar o numero do DOI
            identificacao_doi   = "${prefixICMBio}/salve.${tipoIdentificacaoDoi}.${row.sq_ficha}"

            // gera o UUID da ficha/public
            String uuid_ficha   = Util.encryptStr(row.sq_ficha.toString())
            url_ficha_pdf       = 'https://salve.icmbio.gov.br/salve-estadual/api/fichaHash/' + uuid_ficha
            xmlAutores          = ''
            // pegar os autores
            if( row.ds_citacao.indexOf(';') > 0){
                def strAutores = row.ds_citacao.split(';');
                for( String nome : strAutores ){
                    xmlAutores += """
                        <person_name sequence="first" contributor_role="author">
                            <surname>${nome}</surname>
                        </person_name>
                    """
                    possuiAutores = true
                    //<given_name></given_name> => Não está sendo utilizado
                }
            }else if(row.ds_citacao){
                def ds_citacao = row.ds_citacao
                if(ds_citacao.length() > 59){
                    ds_citacao = ds_citacao.substring(0,59)
                }
                xmlAutores += """
                        <person_name sequence="first" contributor_role="author">
                            <surname>${ds_citacao}</surname>
                        </person_name>
                    """
                possuiAutores = true
            }

            // se não encontrou autores, atribui ICMBio
            if(possuiAutores == false){
                xmlAutores += """
                        <person_name sequence="first" contributor_role="author">
                            <surname>ICMBio</surname>
                        </person_name>
                    """
            }

            // pegar dia, mes e ano
            String[] parts = row.dt_publicacao.toString().split(" ");
            parts = parts[0].split('-');
            def ano = parts[0]
            def mes = parts[1]
            def dia = parts[2]

            doiRecordXml += """
                <dataset dataset_type="record">
                    <contributors>
                        ${xmlAutores}
                    </contributors>
                    <titles>
                        <title>Ficha de ${row.nm_cientifico}</title>
                    </titles>
                    <database_date>
                        <creation_date>
                            <month>${mes}</month>
                            <day>${dia}</day>
                            <year>${ano}</year>
                        </creation_date>
                    </database_date>
                    <publisher_item>
                        <item_number>${row.sq_ficha}</item_number>
                    </publisher_item>
                    <description language="pt">Publicação da ficha no sistema SALVE da espécie ${row.nm_cientifico} - https://salve.icmbio.gov.br</description>
                    <doi_data>
                        <doi>${identificacao_doi}</doi>
                        <timestamp>${new Date().getTime()}</timestamp>
                        <resource>${url_ficha_pdf}</resource>
                    </doi_data>
                </dataset>
            """

            // adicionar ficha e o numero do seu DOI na lista
            listFichasAtualizarDoi.push( [ sqFicha:row.sq_ficha, dsDoi: identificacao_doi] )
        }

        if(possuiAutores == false){
            msgError = 'Ficha sem autores! Ficha: '+idsFicha
            Util.printLog('ERRO::SALVE::DoiService._templateRecordDoiXml()', msgError );
        }
        if(lista.size() == 0){
            msgError = 'Nenhuma ficha atende os requisotos para geração do DOI! idsFicha: '+idsFicha
            Util.printLog('ERRO::SALVE::DoiService._templateRecordDoiXml()', msgError );
        }
        return doiRecordXml
    }

    /**
    * Verifica se a ficha possui um DOI publicado na CrossRef
    * Se encontrar atualiza o campo salve.ficha.ds_doi com o endereço DOI caso este ainda não esteja preenchido na tabela
    */
    def verificaDoiCrossref(String sq_ficha){
        Map res = [ status: 0, msg: '', type: 'success' ]
        String url = ''
        String prefixICMBio             = grailsApplication.config.doi.crossref.prefix_icmbio
        String tipoIdentificacaoDoi     = 'ficha'
        DadosApoio situacaoPublicada    = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'PUBLICADA')

        List sqlWherePasso1 = []
        Map sqlParams = [:]
        Map resSqFicha = [:]

        def sslContext    = null
        def socketFactory = null
        def httpsScheme   = null

        // filtrar pelos ids selecionados
        if( sq_ficha ) {
            if( sq_ficha.indexOf(',') == -1 ) {
                sqlWherePasso1.push("sq_ficha = :sqFicha")
                sqlParams.sqFicha = sq_ficha.toLong()
            } else {
                sqlWherePasso1.push("sq_ficha = any( array[${sq_ficha}] )")
            }
        }

        String cmdSql="""SELECT sq_ficha, ds_doi
        FROM salve.ficha
        WHERE sq_situacao_ficha = ${situacaoPublicada.id.toString()}
        ${sqlWherePasso1 ? '\nAND ' + sqlWherePasso1.join('\nand ') : ''}
        """

        /** /
        println cmdSql
        println sqlParams
        /**/

        List lista = sqlService.execSql( cmdSql, sqlParams ).each{ row ->
            // consulta API crossref
            String path_doi = row.ds_doi ?: "${prefixICMBio}/salve.${tipoIdentificacaoDoi}.${row.sq_ficha}"
            String urlCrossrefWorks = grailsApplication.config.doi.crossref.works.url ?: 'https://api.crossref.org/works/'
            // EX Para teste: path_doi = "${prefixICMBio}/issn.2236-2886.2019.n.2.3-18"
            url = urlCrossrefWorks + URLEncoder.encode(path_doi,'UTF-8')
            LazyMap parsedResponse = null
            try{
                // força validação de certificado
                if( url.indexOf('https://') == 0 )
                {
                    TrustManager[] trustAllCerts = [
                    new X509TrustManager() {
                            public java.security.cert.X509Certificate[] getAcceptedIssuers() {return null}
                            public void checkClientTrusted(X509Certificate[] certs, String authType) {}
                            public void checkServerTrusted(X509Certificate[] certs, String authType) {}
                        }
                    ]
                    sslContext = SSLContext.getInstance("SSL");
                    sslContext.init(null, trustAllCerts, new java.security.SecureRandom());

                    socketFactory = new SSLSocketFactory(sslContext, SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
                    httpsScheme = new Scheme("https", socketFactory, 443);

                    HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
                    // Create all-trusting host name verifier
                    HostnameVerifier allHostsValid = new HostnameVerifier() {
                        @Override
                        public boolean verify(String hostname, SSLSession session) {
                            return true;
                        }
                    };
                    // Install the all-trusting host verifier
                    HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
                }

                // fazer a requisição e transformar o resultado em JSON
                parsedResponse = new JsonSlurper().parse( new URL(url) )
                if( parsedResponse &&  parsedResponse.get( 'status' ).toString().toLowerCase() == 'ok' ){
                    if( parsedResponse["message"]["DOI"].toString() == path_doi.toString() ){
                        // update set DOI se a ficha ainda estiver sem o doi
                        if( ! row.ds_doi ) {
                            cmdSql = "UPDATE salve.ficha SET ds_doi='${path_doi}' WHERE ds_doi IS NULL AND sq_ficha = ${row.sq_ficha}"
                            sqlService.execSql(cmdSql)
                        }
                        res.status = 0
                        res.msg = "DOI com status publicado! Endereço DOI: <a href='https://doi.org/${path_doi}' target='_blank'>https://doi.org/${path_doi}</a>"
                        res.type = 'success'
                        resSqFicha.put(row.sq_ficha, "<a href='https://doi.org/${path_doi}' target='_blank'>${path_doi}</a>")
                    }else{
                        res.status = 0
                        res.msg = 'Este DOI continua em analise pela crossref.org'
                        res.type = 'success'
                        resSqFicha.put(row.sq_ficha, "Em Analise")
                    }
                }
            }catch(Exception e){
                println e.getMessage()
                res.status = 1
                res.msg = 'Não foi possível obter informação do DOI! Possivel causa: Situação em analise.'
                res.type = 'error'
                resSqFicha.put(row.sq_ficha, "Ops! Tente mais tarde.<br><small><i>(em validação ou possui erros)</i><small>")
            }
        }
        if( !lista ) {
            res.status = 1
            res.msg = 'Ficha não encontrada ou ainda não publicada.'
            res.type = 'info'
        }
        if( res.status == 1 && url) {
            res.msg += '<br>Endereço utilizado: ' + url
        }
        if(resSqFicha.size() > 1 ){ // Mensagem generia para caso de consulta de mais de uma ficha
            res.status = 0
            res.msg = "Consulta no crossref.org concluída com sucesso!"
            res.type = 'success'
        }
        res.data = resSqFicha
        return res
    }
}
