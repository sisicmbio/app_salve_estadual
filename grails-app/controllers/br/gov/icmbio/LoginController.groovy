package br.gov.icmbio

import grails.converters.JSON

class LoginController {

    def loginService

    def index() {

        // verificar se as configurações iniciais foram cadastradas
        Configuracao configuracao = Configuracao.findByEstadoIsNotNull()
        if( ! configuracao ) {
            redirect(controller:"configuracao", action: 'index')
            return;
        }
        /** /
        println ' '
        println 'ENTRANDO LOGIN'
        println 'SQ_PERFIL...: ' + session.sicae.user.sqPerfil
        println 'CD_PERFIL...: ' + session.sicae.user.cdPerfil
        /**/

        // gravar na sessao do usuario os dados da instituicao executora da SALVE
        session.setAttribute('config',[
                noInstituicao:configuracao.noInstituicao ?:'',
                sgInstituicao:configuracao.sgInstituicao?:'',
                sqEstado:configuracao.estado.id?:0,
                sgEstado:configuracao.estado.sgEstado?:'',
                dsLink:configuracao.dsLink ?:'',
                noArquivoBrasao:configuracao.noArquivoBrasao?:''

        ] )

        /** /
        // simular perfil testar o sistema como adminstrador -- REMOVER DEPOIS
        if( !session?.sicae?.user ){
            session.sicae = [:]
            session.sicae.user = loginService.simularPerfilAdm('admin@salve-estadual','123456')
            CicloAvaliacao ciclo = CicloAvaliacao.get(2)
            if( !ciclo ){
                throw new Exception('Ciclo de Avaliação 2 não cadastrado');
            }
            session.setAttribute('ciclo', ciclo )
            redirect(uri:'/')
        }
        // fim simulação do perfil administrador -- REMOVER ATÉ AQUI
        /**/

        List listPerfis = []
        if( session?.sicae?.user && !session.sicae.user.sqPerfil ){
            listPerfis = loginService.perfis( Usuario.get(session.sicae.user.sqPessoa ) )
        }

        // se for o perfil validador redirecionar para a tela de validação
        if( session?.sicae?.user?.isVL() ){
            forward(controller: "gerenciarValidacao", action: "indexValidador")
        } else {
            render('view':'index',model:[listPerfis:listPerfis,configuracao:configuracao])
        }
    }


    def login(){
        Map res = [status:0,msg:'',type:'success']
        try {
            // se passar email e senha - validar email e senha
            // se passar email e perfil - validar email e perfil
            session.sicae = [:]
            session.sicae.user = loginService.login( params.email ?: session?.sicae?.user?.deEmail, params.senha?:'', params.sqPerfil ? params.sqPerfil.toLong() : 0 )
            CicloAvaliacao ciclo = CicloAvaliacao.get(2)
            if( !ciclo ){
                throw new Exception('Ciclo de Avaliação 2 não cadastrado');
            }
            session.setAttribute('ciclo', ciclo )
        } catch( Exception e ){
            res.status = 1
            res.msg = e.getMessage()
        }
        render res as JSON
    }

    def selectPerfil(){
        Map res = [status:0,msg:'',type:'success',data:[:]]
        try {
            res.data = loginService.selectPerfil( params.sqUsuarioPerfil ? params.sqUsuarioPerfil.toLong() : 0 )
        } catch( Exception e ){
            res.status = 1
            res.msg = e.getMessage()
        }
        render res as JSON
    }

    def selectPerfilInstituicao(){
        Map res = [status:0,msg:'',type:'success']
        try {
            session.sicae = [:]
            session.sicae.user = loginService.loginPerfilInstituicao(
                                                params.sqUsuarioPerfil ? params.sqUsuarioPerfil.toLong() : 0l,
                                                params.sqUsuarioPerfilInstituicao ? params.sqUsuarioPerfilInstituicao.toLong():0l)
        } catch( Exception e ){
            e.printStackTrace()
            res.status = 1
            res.msg = e.getMessage()
        }
        render res as JSON
    }

    def logout(){
        session.removeAttribute('sicae')
        redirect(uri:'/')
    }

    def forgotPasswordForm(){
        render( view:'forgotPasswordForm')
    }

    def changePasswordForm(){
        session.setAttribute('user-hash',params.hash)
        render( view:'changePasswordForm',model:[hash:params.hash])
    }

    def changePassword(){
        Map res = [status:0,msg:'Senha alterada com SUCESSO.',type:'success']
        try {
            loginService.changePassword( params.senha1, params.senha2, params.hash, session.getAttribute('user-hash') )
            session.removeAttribute('user-hash')
        } catch( Exception e ){
            res.status = 1
            res.type='error'
            res.msg = e.getMessage()
            println e.getMessage()
        }
        render res as JSON
    }

    /**
     * gerar o hash de reset da senha e enviar o email para o usuário
     * @return
     */
    def sendResetPasswordMail(){
        Map res = [status:0,msg:'',type:'success']
        try {
            res.msg = loginService.generateMessageResetPassword(params.email)
        } catch( Exception e ){
            println e.getMessage()
            res.status=1
            res.msg = e.getMessage()
            res.type='info'
        }
        render res as JSON
    }

}
