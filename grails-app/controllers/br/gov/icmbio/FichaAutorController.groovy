package br.gov.icmbio
import grails.converters.JSON;

class FichaAutorController extends FichaBaseController {

    def getForm()
    {
        if( ! params.sqFicha )
        {
            render "Id da Ficha não informado!"
            return;
        }

        render(template: "/ficha/autor/formSelAutor", model: [sqFicha:params?.sqFicha,noCientifico:params?.noCientifico]);
    }

     def save()
     {

        if( ! params.sqFicha )
        {
            return this._returnAjax(1, "Id da Ficha não informado!", "error")
        }
        if( ! params.noAutor && ! params.sqAutor )
        {
            return this._returnAjax(1, "Nenhum autor informado!", "error")
        }
        if( ! params.noCitacao && ! params.sqAutor  )
        {
            return this._returnAjax(1, "Nome para citação é obrigatório!", "error")
        }
        Ficha ficha = Ficha.get( params.sqFicha);
         DadosApoio autor;
         if( !params.sqAutor )
         {
             DadosApoio pai = DadosApoio.findByCodigo('TB_AUTOR');
             //encontrar o autor
             autor = DadosApoio.findByPaiAndDescricao(pai, params.noAutor);
             if (!autor)
             {
                 // verificar se o nome já existe
                 //Integer qtd = DadosApoio.countByPaiAndIdNotEqualAndDescricao(pai,reg.id, params.noAutor);
                 autor = new DadosApoio(codigo: params.noCitacao, descricao: params.noAutor);
                 autor.save(flush: true);
             }
         }
         else {
             autor = DadosApoio.get(params.sqAutor);
         }

        // verificar se o autor já existe na ficha
        Integer qtd = FichaAutor.countByFichaAndAutor(ficha,autor);
        FichaAutor reg;
        if( qtd > 0 )
        {
            //return this._returnAjax(1, "Autor já cadastrado na ficha!", "error")
        }

        if( params.sqFichaAutor )
        {
            reg = FichaAutor.get(params.sqFichaAutor)
        }
        else
        {
            reg = new FichaAutor();
            reg.ficha = ficha;
        }
        reg.autor = autor;
        reg.save( flush:true);
        return this._returnAjax(0, getMsg(1), "success");
    }

    def getGrid() {
        List listAutores = FichaAutor.findAllByFicha( Ficha.get( params.sqFicha ) );
        render(template:'/ficha/autor/divGridModalAutores',model:[listAutores:listAutores]);
    }

    def edit()
    {
        FichaAutor reg = FichaAutor.get(params.id);
        if( reg?.id )
        {
            render reg.asJson();
            return;
        }
        render [:] as JSON;
    }

    def delete()
    {
        FichaAutor reg = FichaAutor.get(params.id);
        if( reg )
        {
            reg.delete(flush:true);
            return this._returnAjax(0, getMsg(4), "success")
        }
        else
        {
            return this._returnAjax(1, getMsg(51), "error")
        }
        render '';
    }

    def select2Autor()
    {
        Map data = ['items': []];

        if (!params?.q?.trim()) {
            render '';
            return;
        }

        String q = '%' + params.q.trim() + '%';

        List lista = DadosApoio.findAllByDescricaoIlikeAndPai(q, DadosApoio.findByCodigoSistema('TB_AUTOR'));
        if (lista) {
            lista.each {
                data.items.push(['id': it.id, 'descricao': it.descricao,'codigo':it.codigo]);
            }
            render data as JSON
            return;
        }
        render '';




















    }
}
