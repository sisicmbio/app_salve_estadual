package br.gov.icmbio

import grails.converters.JSON

class TraducaoController extends BaseController {

    def traducaoService

    def getGridTraducaoTextoFixo() {
        Map data = traducaoService.getDataGridTraducaoTextoFixo( params )
        render(template: "gridTraducaoTextoFixo", model: [
              rows          : data.rows
            , gridId        : params.gridId
            , pagination    : data.pagination
            ])
    }

    /**
     * fazer a inclusão/alteração da tradução dos textos fixos no banco de dados
     * @return
     */
    def save() {
        render traducaoService.saveTraducaoTextoFixo(params) as JSON
    }

    /**
     * retornar os dados do item selecionado para tela de edição do registro
     * @return
     */
    def edit() {
        render traducaoService.editTraducaoTextoFixo(params) as JSON
    }

    /**
     * fazer a exclusão da trudução
     * @return
     */
    def delete() {
        render traducaoService.deleteTraducaoTextoFixo(params) as JSON
    }

    /**
     * fazer a tradução de um texto recebido
     */
    def traduzir() {
        Map res = traducaoService.traduzir( [texto : params?.texto ?: ''])
        render res as JSON
    }
}
