package br.gov.icmbio

import grails.converters.JSON
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.safety.Whitelist

class InformativoController extends BaseController{
    def sqlService

    def save() {

        List erros = []
        if( ! params.nuInformativo ) {
            erros.push('O número do informativo é obrigatório.')
        }
        if( ! params.dtInformativo ) {
            erros.push('A data do informativo é obrigatória.')
        }

        if( ! params.deTema )  {
            erros.push('O tema é obrigatório.')
        }

        /*if( ! params.fldAnexo && !params.sqInformativo )  {
            erros.push('O documento anexo deve ser informado.')
        }*/
        Date dtInformativo = null
        //Date dtInicioAlerta = null
        //Date dtFimAlerta = null
        dtInformativo   = new Date().parse('dd/MM/yyyy', params.dtInformativo )
        //dtInicioAlerta  = params.dtInicioAlerta ? new Date().parse('dd/MM/yyyy', params.dtInicioAlerta ) : null
        //dtFimAlerta     = params.dtFimAlerta ? new Date().parse('dd/MM/yyyy', params.dtFimAlerta ) : null
        /*if( dtInicioAlerta && dtFimAlerta &&
                dtInicioAlerta > dtFimAlerta ) {
            erros.push('A data inicial do periodo de alerta deve ser MENOR que a data final.')
        }
        if( dtFimAlerta && dtFimAlerta < dtInformativo ) {
            erros.push('A data final do periodo de alerta deve ser MAIOR ou IGUAL a data do informativo.')

        }*/

        if( erros ) {
            return _returnAjax(1,erros.join('<br>'),'error')
        }

        Informativo informativo
        if( params.sqInformativo ) {
            informativo = Informativo.get( params.sqInformativo.toLong() )
        } else {
            informativo = new Informativo()
        }
        informativo.nuInformativo   = params.nuInformativo.toInteger()
        informativo.dtInformativo   = dtInformativo
        informativo.deTema          = params.deTema
        //informativo.txInformativo   = params.txInformativo?:null
        //informativo.txInformativo   = Jsoup.clean( params.txInformativo, Whitelist.none().addTags('br','b') ).trim()
        //informativo.txInformativo   = Jsoup.clean( params.txInformativo, "", Whitelist.none().addTags('b','i') , new Document.OutputSettings().prettyPrint(false) )

        //informativo.dtInicioAlerta  = dtInicioAlerta
        //informativo.dtFimAlerta     = dtFimAlerta

        if( params.inPublicado ) {
            informativo.inPublicado = 'S'
        } else {
            informativo.inPublicado = 'N'
        }
        if( params.fldAnexo ) {
            def f = request.getFile('fldAnexo')
            if ( f && !f.empty) {
                d(' ')
                d('Inicio gravacao do anexo')

                String fileName = f.getOriginalFilename().replaceAll(' |,|\\(.+\\)', '_').replaceAll('_+','-').replaceAll('-\\.','.')
                d('FileName: ' + fileName)
                String fileExtension = fileName.substring(fileName.lastIndexOf("."))
                d('FileExtension: ' + fileExtension )
                String hashFileName = f.inputStream.text.toString().encodeAsMD5() + fileExtension
                d('HashFileName: ' + hashFileName )
                File savePath = new File( grailsApplication.config.anexos.dir )
                fileName = savePath.absolutePath + '/' + hashFileName
                d('FileName com hash: ' + hashFileName )
                if ( ! new File( fileName).exists() ) {
                    f.transferTo(new File(fileName));
                    ImageService.createThumb(fileName)
                }
                informativo.deLocalArquivo = hashFileName
                informativo.noArquivo = f.getOriginalFilename()
                informativo.deTipoConteudo = f.getContentType()
            }
        }
        informativo.save( flush:true )
        Map data=[sqInformativo:informativo.id]
        //Map data = JSON.parse( informativo.asJson().toString() )
        return _returnAjax(0,'Informativo gravado com SUCESSO!','success', data )
    }

    def saveAlerta() {

        List erros = []

        if( ! params.dtFimAlerta )  {
            erros.push('Data final do alerta deve ser informada.')
        }
        if( ! params.txAlerta )  {
            erros.push('A mensagem do alerta deve ser informada.')
        }
        Date dtInformativo = Util.hoje()
        Date dtInicioAlerta  = params.dtInicioAlerta ? new Date().parse('dd/MM/yyyy', params.dtInicioAlerta ) : null
        Date dtFimAlerta     = params.dtFimAlerta ? new Date().parse('dd/MM/yyyy', params.dtFimAlerta ) : null
        if( dtInicioAlerta && dtFimAlerta &&
                dtInicioAlerta > dtFimAlerta ) {
            erros.push('A data inicial do periodo de alerta deve ser MENOR ou IGUAL que a data final.')
        }
        if( dtFimAlerta && dtFimAlerta < dtInformativo ) {
            erros.push('A data final do periodo de alerta deve ser MAIOR ou IGUAL a data de hoje.')

        }
        if( erros ) {
            return _returnAjax(1,erros.join('<br>'),'error')
        }

        Informativo informativoAlerta
        if( params.sqAlerta ) {
            informativoAlerta = Informativo.get( params.sqAlerta.toLong() )
        } else {
            informativoAlerta = new Informativo()
        }
        informativoAlerta.dtInformativo   = informativoAlerta.dtInformativo ?: dtInformativo
        informativoAlerta.deTema          = null
        informativoAlerta.txInformativo   = Jsoup.clean( params.txAlerta, "", Whitelist.none().addTags('b','i') , new Document.OutputSettings().prettyPrint(false) )
        informativoAlerta.dtInicioAlerta  = dtInicioAlerta
        informativoAlerta.dtFimAlerta     = dtFimAlerta

        if( params.inPublicado ) {
            informativoAlerta.inPublicado = 'S'
        } else {
            informativoAlerta.inPublicado = 'N'
        }
        informativoAlerta.save( flush:true )
        Map data=[sqAlerta:informativoAlerta.id]
        //Map data = JSON.parse( informativo.asJson().toString() )
        return _returnAjax(0,'Dados gravados com SUCESSO!','success', data )
    }

    def getGrid() {
        List listInformativos = Informativo.createCriteria().list{
            if( params.sqInformativo ) {
                eq('id',params.sqInformativo.toLong())
            }
            isNotNull('nuInformativo')
            isNull('dtFimAlerta')
            order('nuInformativo','desc')
        }
        render(template: 'divGridInformativos', model: ['listInformativos': listInformativos ]);
    }

    def getGridAlerta() {
        List listInformativos = Informativo.createCriteria().list{
            if( params.sqInformativo ) {
                eq('id',params.sqInformativo.toLong())
            }
            isNull('nuInformativo')
            isNotNull('dtFimAlerta')
            order('dtInicioAlerta','desc')
        }
        render(template: 'divGridAlertas', model: ['listInformativos': listInformativos ]);
    }

    def edit() {
        Informativo informativo
        if( params.sqInformativo ) {
            informativo = Informativo.get( params.sqInformativo.toLong() )
        }
        if( informativo ) {
            render informativo.asJson();
        } else {
            render [:] as JSON
        }
    }

    def editAlerta() {
        Informativo informativoAlerta
        if( params.sqInformativo ) {
            informativoAlerta = Informativo.get( params.sqInformativo.toLong() )
        }
        if( informativoAlerta ) {
            render informativoAlerta.asJsonAlerta()
        } else {
            render [:] as JSON
        }
    }

    def delete() {
        Informativo informativo
        if( params.sqInformativo ) {
            informativo = Informativo.get( params.sqInformativo.toLong() )
        }
        if( informativo ) {
            informativo.delete(flush:true)
        }
        return _returnAjax(1,'Informativo excluído com SUCESSO!','success')
    }

    def deleteAlerta() {
        Informativo informativoAlerta
        if( params.sqAlerta ) {
            informativoAlerta = Informativo.get( params.sqAlerta.toLong() )
        }
        if( informativoAlerta ) {
            informativoAlerta.delete(flush:true)
        }
        return _returnAjax(1,'Registro excluído com SUCESSO!','success')
    }

    def calcularProximoNumero() {
        Integer proximoNumero = Informativo.createCriteria().get {
            projections {
                max "nuInformativo"
            }
        } as Integer
        proximoNumero = ( proximoNumero ?: 0 ) + 1
        return _returnAjax(0,'','success', [proximoNumero:proximoNumero])
    }

    /*def show() {
        List listInformativos = Informativo.createCriteria().list{
            if( params.nuInformativo ) {
                eq('nuInformativo',params.nuInformativo.toInteger())
            }
            eq('inPublicado','S')
            // ler o último informativo apenas
            order('nuInformativo','desc')
            maxResults(1)
        }
        if( ! listInformativos ){
            render '<h3>Nenhum informativo encontrado!</h3>'
            return;
        }
        Informativo informativo = listInformativos[0]
        Integer nuInformativoAtual = 1
        if( informativo ) {
            nuInformativoAtual = informativo.nuInformativo
        }

        // ler o maior e o menor informativo publicado
        Integer numeroProximo  = 0
        Integer numeroAnterior = 0
        String cmdSql="""select tmp.no_sentido, tmp.nu_informativo from (
                       select 'ANTERIOR' as no_sentido, max(nu_informativo) as nu_informativo from salve.informativo
                       where nu_informativo < :nuInformativoAtual and in_publicado='S'
                    union all
                       select 'PROXIMO', min(nu_informativo) from salve.informativo
                       where nu_informativo > :nuInformativoAtual and in_publicado='S'
                    ) tmp
                    order by tmp.nu_informativo"""
        sqlService.execSql( cmdSql,[nuInformativoAtual:nuInformativoAtual.toInteger()] ).each{ row ->
            if( row.no_sentido == 'PROXIMO' ) {
                numeroProximo = row.nu_informativo ?: 0
            } else {
                numeroAnterior = row.nu_informativo ?: 0
            }
        }

        // controle de leitura marcar como lido
        if( params.marcarComoLido == 'S' ) {
            Map pars = [sqPessoa: session.sicae.user.sqPessoa, sqInformativo: informativo.id];
            List rows = sqlService.execSql("""select sq_informativo_pessoa from salve.informativo_pessoa
                where sq_informativo = :sqInformativo and sq_pessoa = :sqPessoa limit 1""", pars)
            if (rows.size() == 0) {
                sqlService.execSql("""INSERT INTO salve.informativo_pessoa( sq_informativo, sq_pessoa ) VALUES ( :sqInformativo, :sqPessoa );""", pars)
            }
        }
        render(template: 'showInformativo', model: [ 'informativo':informativo
                                                     ,numeroAnterior:numeroAnterior
                                                     ,numeroProximo:numeroProximo ] )
    }
    */

    /**
     * verificar qual informatio o usuário ainda não viu e retornar o numero
     * @return
     */
    def getInformativoAlerta() {
        Integer nuInformativo = 0
        Integer sqInformativo = 0
        String deTema = ''
        String txInformativo = '';
        String dataAtual = new Date().format('yyyy-MM-dd')
        String deLocalArquivo = ''
        String interromper = 'N'
        Map pars
        String cmdSql
        try {

            if (session && session?.sicae?.user?.sqPessoa) {

                // não exibir alertas para validadores
                if ( session?.sicae?.user?.isVL()) {
                    interromper = 'S'
                }

                if (interromper == 'N') {
                    // ler o proximo da lista que não foi lido ainda
                    if (!params.sqInformativo) {
                        cmdSql = """select sq_informativo, nu_informativo, de_tema, tx_informativo, de_local_arquivo from salve.informativo
                                where informativo.in_publicado='S'
                                AND  ( informativo.dt_inicio_alerta is null or informativo.dt_inicio_alerta <= '${dataAtual}')
                                and ( informativo.dt_fim_alerta is not null and informativo.dt_fim_alerta >= '${dataAtual}' )
                                and not exists ( select null from salve.informativo_pessoa x where x.sq_informativo = informativo.sq_informativo
                                        and x.sq_pessoa = :sqPessoa limit 1 )
                                order by informativo.dt_informativo
                                limit 1"""
                        pars = [sqPessoa: session.sicae.user.sqPessoa]
                    } else {
                        cmdSql = """select sq_informativo, nu_informativo, de_tema, tx_informativo, de_local_arquivo from salve.informativo where sq_informativo = :sqInformativo"""
                        pars = [sqInformativo: params.sqInformativo.toLong()]
                    }
                    List rows = sqlService.execSql(cmdSql, pars)
                    if (rows) {
                        sqInformativo = rows[0].sq_informativo
                        nuInformativo = rows[0].nu_informativo
                        deTema = rows[0].de_tema ?: ''
                        txInformativo = rows[0].tx_informativo ?: ''
                        deLocalArquivo = rows[0].de_local_arquivo ?: ''
                    }
                }
            }

        } catch( Exception e ) {
            interromper = 'S'
        }

        _returnAjax(0,'','success',[
                    interromper     : interromper
                    ,sqInformativo  : sqInformativo
                    ,nuInformativo  : nuInformativo
                    ,deTema         : deTema
                    ,txInformativo  : txInformativo
                    ,deLocalArquivo : deLocalArquivo])
    }

    /**
     * marcar o informativo como lido pelo usuário
     */
    def setRead() {
        if( ! params.sqInformativo || ! session?.sicae?.user?.sqPessoa ) {
            render [:] as JSON
            return
        }
        // apenas marcar como lido os informativos que possuirem período de alerta definido
        Informativo informativo = Informativo.get( params.sqInformativo.toLong() )
        if( informativo && ( informativo.dtInicioAlerta || informativo.dtFimAlerta ) ) {
            Map pars = [sqPessoa: session.sicae.user.sqPessoa, sqInformativo: params.sqInformativo.toInteger()];
            List rows = sqlService.execSql("""select sq_informativo_pessoa from salve.informativo_pessoa
                where sq_informativo = :sqInformativo and sq_pessoa = :sqPessoa limit 1""", pars)
            if (rows.size() == 0) {
                sqlService.execSql("""INSERT INTO salve.informativo_pessoa( sq_informativo, sq_pessoa ) VALUES ( :sqInformativo, :sqPessoa );""", pars)
            }
        }
        render '';
    }

    /**
     * Exibir os usuários que já viram o informativo
     */
    def showPessoas() {
        Map data = [pessoas:[]]
        String cmdSql = """select pf.no_pessoa, to_char(informativo_pessoa.dt_visualizacao,'dd/MM/yyyy HH:mm:ss') as dt_visualizacao
        from salve.informativo_pessoa
        inner join salve.vw_pessoa_fisica pf on pf.sq_pessoa = informativo_pessoa.sq_pessoa
        where informativo_pessoa.sq_informativo = :sqInformativo
        order by pf.no_pessoa"""
        data.pessoas = sqlService.execSql( cmdSql,[sqInformativo: params.sqInformativo.toLong()]);
        _returnAjax(0,'','success',data)
    }
}
