package br.gov.icmbio

import grails.converters.JSON
import org.apache.catalina.startup.Tomcat

class OficinaController extends BaseController {
    def dadosApoioService
    def fichaService
    def emailService
    def oficinaService
    def sqlService
    def exportDataService

    private Map lerPapeis( Oficina oficina ) {
        Map mapPapeis = [ : ]
        //List papeisNaoImprimir = [ 'COLABORADOR','COMPILADOR', 'COLABORADOR_DIRETA','COLABORADOR_AMPLA' ]
        // 07/10/2020 - Arthur detectou que ele era um colaborador da oficina Aves LC e NA do Pampa e não saiu na listagem da ficha e pediu para alterar
        // para que os colaboradores também saíssem na ficha
        List papeisNaoImprimir = [ 'COMPILADOR' ]
        OficinaFichaParticipante.createCriteria().list {
            oficinaFicha {
                eq( 'oficina', oficina )
            }
            papel {
                not {
                    'in'( "codigoSistema", papeisNaoImprimir )
                }
                order( 'ordem' )
            }
            oficinaParticipante {
                pessoa {
                    order( 'noPessoa' )
                }
            }
        }.each {
            if( !mapPapeis[ it.papel.descricao ] ) {
                mapPapeis[ it.papel.descricao ] = [ ]
            }
            if( mapPapeis[ it.papel.descricao ].indexOf( it.oficinaParticipante.pessoa.noPessoaCap ) == -1 ) {
                mapPapeis[ it.papel.descricao ].push( it.oficinaParticipante.pessoa.noPessoaCap )
            }
        }
        return mapPapeis
    }

    /**
     * [index description]
     * @return [description]
     */
    def index() {

        if( ! session?.sicae?.user ) {
            render '<h3>Sessão expirada!</h3>'
            return
        }

        if( !session?.sicae?.user?.menuOficina() ) {
            render '<h3>Acesso não autorizado!</h3>'
            return
        }

        Oficina oficina = new Oficina()

        // listar os ciclos abertos
        List listCiclosAvaliacao = [ session.getAttribute('ciclo') ] //CicloAvaliacao.findAllByInOficina( 'S', [ sort: 'nuAno', order: 'desc' ] )

        List listTipoOficina = dadosApoioService.getTable( 'TB_TIPO_OFICINA' ).findAll {
            return ( ( it.codigoSistema ==~ /OFICINA_AVALIACAO|REUNIAO_PREPARATORIA/ ) )
        }
        List listSituacaoOficina = dadosApoioService.getTable( 'TB_SITUACAO_OFICINA' )

        List listFonteRecurso = dadosApoioService.getTable( 'TB_FONTE_RECURSO' )

        // filtrar a localidade da oficina para perfil não adm
        List listUnidade = dadosApoioService.getCentros().findAll {
            return session.sicae.user.isADM() || it.id.toInteger() == session.sicae.user.sqUnidadeOrg.toInteger()
        }

        render( view: 'index', model: [ oficina              : oficina
                                        , listCicloAvaliacao : listCiclosAvaliacao
                                        , listTipoOficina    : listTipoOficina
                                        , listUnidade        : listUnidade
                                        , listSituacaoOficina: listSituacaoOficina
                                        , listFonteRecurso: listFonteRecurso] )
    }


    /**
     * metodo para listar as oficinas de acordo com perfil do usuário logado
     * @param sqCicloAvaliacao
     * @return
     */
    protected Map _getOficinasUsuario( Long sqCicloAvaliacao = 0
                    , String nmCientificoFiltro = ''
                    , Long sqSituacaoFiltro = null
                    , String sgUnidadeFiltro = null
                    , Long sqTipoOficinaFiltro = null
                    , String noOficinaFiltro = null
                    , String deLocalFiltro = null
    ) {
        // listar as oficinas de VALIDACAO do ciclo
        CicloAvaliacao cicloAvaliacao = CicloAvaliacao.get( sqCicloAvaliacao )
        Map pars = [:]
        if ( ! session.sicae.user.isADM() ) {
            if( session.sicae.user.sqUnidadeOrg ) {
                pars.sqUnidadeOrg = session.sicae.user.sqUnidadeOrg
            } else {
                if( session.sicae.user.isCT() ) {
                    DadosApoio papelCT = dadosApoioService.getByCodigo('TB_PAPEL_FICHA', 'COORDENADOR_TAXON')
                    pars.sqPapel = ( papelCT ? papelCT.id : 0 )
                }
                pars.sqPessoa = session.sicae.user.sqPessoa
            }
        }
        if( sqSituacaoFiltro ){
            pars.sqSituacao = sqSituacaoFiltro
        }
        if( sqTipoOficinaFiltro ){
            pars.sqTipoOficina = sqTipoOficinaFiltro
        }

        String cmdSql = """SELECT DISTINCT a.sq_oficina
            , a.no_oficina
            , a.sg_oficina
            , a.sq_situacao
            , a.de_local
            , a.dt_inicio
            , a.dt_fim
            , tipo.ds_dados_apoio as ds_tipo
            , tipo.cd_sistema as cd_tipo_sistema
            ,concat( to_char(a.dt_inicio,'dd/mm/yyyy'),' a ', to_char(a.dt_fim,'dd/mm/yyyy') ) as de_periodo
            ,und.sg_unidade_org
            ,fonte.ds_dados_apoio as no_fonte_recurso
            ,situacao.ds_dados_apoio as ds_situacao
            ,situacao.cd_sistema as cd_situacao_sistema
            FROM salve.oficina a
            INNER JOIN salve.dados_apoio as tipo on tipo.sq_dados_apoio = a.sq_tipo_oficina
            LEFT JOIN salve.oficina_ficha b on b.sq_oficina = a.sq_oficina
            LEFT JOIN salve.ficha c on c.sq_ficha = b.sq_ficha
            INNER JOIN salve.vw_unidade_org as und on und.sq_pessoa = a.sq_unidade_org
            INNER JOIN salve.dados_apoio as situacao on situacao.sq_dados_apoio = a.sq_situacao
            LEFT JOIN salve.dados_apoio as fonte on fonte.sq_dados_apoio = a.sq_fonte_recurso
            ${pars.sqPessoa ? 'INNER JOIN salve.ficha_pessoa d on d.sq_ficha = b.sq_ficha' : '' }
            WHERE a.sq_ciclo_avaliacao = :sqCicloAvaliacao
            AND tipo.cd_sistema in ('OFICINA_AVALIACAO','REUNIAO_PREPARATORIA')
            ${ pars.sqUnidadeOrg ? 'AND a.sq_unidade_org = :sqUnidadeOrg' :''}
            ${ pars.sqPessoa ? "AND d.sq_pessoa = :sqPessoa AND d.in_ativo='S'" :''}
            ${ pars.sqPapel ? "AND d.sq_papel = :sqPapel" :''}
            ${ pars.sqSituacao ? "AND situacao.sq_dados_apoio = :sqSituacao" :''}
            ${ pars.sqTipoOficina ? "AND tipo.sq_dados_apoio = :sqTipoOficina" :''}
            ${ noOficinaFiltro? "AND (a.no_oficina ilike '%${noOficinaFiltro}%' or a.sg_oficina ilike '%${noOficinaFiltro}%')" :''}
            ${ deLocalFiltro ? "AND a.de_local ilike '%${deLocalFiltro}%'" :''}
            ${ sgUnidadeFiltro ? "AND und.no_pessoa ilike '%${sgUnidadeFiltro}%'" :''}
            ${ nmCientificoFiltro ? "AND c.nm_cientifico ilike '%${nmCientificoFiltro}%'" :''}
            """
        pars.sqCicloAvaliacao = cicloAvaliacao.id
        cmdSql+='\nORDER BY a.dt_inicio desc, a.sg_oficina'
         /** /
         println ' '
         println '_getOficinasUsuario()'
         println cmdSql
         println pars
         println ' '
         /**/
        return sqlService.paginate(cmdSql,pars,params,50,10,0)
        //return sqlService.execSql( cmdSql, pars )
    }

    def getGridOficinas() {
        if( !params.sqCicloAvaliacao ) {
            render ''
            return
        }
        List listSituacaoOficina = dadosApoioService.getTable( 'TB_SITUACAO_OFICINA' )

        // quando informar o nome da especie remover o filtro aberta/encerrada
        if( params.noCientificoFiltrarOficina ){
            params.sqSituacaoFiltro=0l
        }

        Map paginate = _getOficinasUsuario( params.sqCicloAvaliacao.toLong()
                                        , params.noCientificoFiltrarOficina
                                        , params.sqSituacaoFiltro ? params.sqSituacaoFiltro.toLong() : 0l
                                        , params.sgUnidadeFiltro
                                        , params.sqTipoOficinaFiltro ? params.sqTipoOficinaFiltro.toLong() : 0l
            , params.noOficinaFiltro
            , params.deLocalFiltro
        )

        List listOficinas = paginate.rows
        if( params.format == 'xls' ) {
            Map res=[status:0,msg:'Exportar para planilha em andamento...']
            // exportacao das oficinas para planilha
            Map exportParams = [usuario:session.sicae.user, rows:paginate.rows]
            exportDataService.exportOficinasAvaliacaoPlanilha( exportParams )
            render res as JSON
            return
        }
        render( template: 'divGridOficinas', model: [ listOficina           : listOficinas
                                                     ,listSituacaoOficina   : listSituacaoOficina
                                                     ,pagination            : paginate.pagination ] )
    }

    /**
     * Selelcionar somente os pontos focais definidos no modulo gerenciar função*/
    def select2PF() {
        Map data = [ 'items': [ ] ]
        if( !params.q || !params.sqCicloAvaliacao ) {
            render data as JSON
            return
        }
        String q = params.q.trim().toLowerCase()

        FichaPessoa.executeQuery( """
        select distinct new map( p.id as sqPessoa
        ,p.noPessoa as noPessoa
        ,pf.nuCpf   as nuCpf
        ,e.txEmail as txEmail
        ,e.tipo as sqTipoEmail)
        from FichaPessoa a, DadosApoio d, Ficha f, Pessoa p, PessoaFisica pf, Email e
        where a.inAtivo = 'S'
          and d.codigoSistema='PONTO_FOCAL'
          and a.papel.id = d.id
          and a.ficha.id = f.id
          and a.pessoa.id = p.id
          and a.pessoa.id = pf.id
          and a.pessoa.id = e.pessoa.id
          and f.cicloAvaliacao.id = :sqCicloAvaliacao
          and lower( p.noPessoa ) like :q
          order by p.noPessoa, e.tipo desc
        """, [ sqCicloAvaliacao: params.sqCicloAvaliacao.toLong(), q: '%' + q + '%' ] ).each {

            Map existe = data.items.find { it2 -> it2.id == it.sqPessoa
            }
            // se já existir dar preferencia pelo email instituicional
            if( existe ) {
                if( it.txEmail.indexOf( 'icmbio.gov.br' ) > 0 ) {
                    existe.email = it.txEmail
                }
            }
            else {
                String cpfFormatado = Util.formatCpf( it.nuCpf )
                data.items.push( [ 'id'       : it.sqPessoa,
                                   'descricao': Util.capitalize( it.noPessoa ) + ' - ' + cpfFormatado,
                                   'cpf'      : cpfFormatado,
                                   'email'    : it.txEmail.toLowerCase() ] )
            }
        }
        render data as JSON
    }

    /**
     * [edit description]
     * @return [description]
     */
    def editOficina() {
        if( params.sqOficina ) {
            Oficina reg = Oficina.get( params.sqOficina )
            if( reg ) {
                render reg.asJson()
                return
            }
        }
        render '{}'

    }

    /**
     * [save description]
     * @return [description]
     */
    def saveOficina() {

        List errors = [ ]

        // validar periodo
        if( !params.sqCicloAvaliacao ) {
            errors.push( 'Ciclo de Avaliação não Selecionado!' )
        }
        if( !params.sqTipoOficina ) {
            errors.push( 'Tipo de oficina não especificado!' )
        }
        if( params.dtInicio ) {
            params.dtInicio = new Date().parse( 'dd/MM/yyyy', params.dtInicio )
        }
        else {
            errors.push( 'Data início deve ser informada!' )
        }
        if( params.dtFim ) {
            params.dtFim = new Date().parse( 'dd/MM/yyyy', params.dtFim )
        }
        else {
            errors.push( 'Data final deve ser informada!' )
        }

        if( params.dtInicio > params.dtFim ) {
            errors.push( 'Data final deve ser MAIOR ou IGUAL a inicial!' )
        }
        if( !params.sqSituacao && !params.sqOficina ) {
            //errors.push('Situação deve ser informada!')
            DadosApoio situacaoAberta = dadosApoioService.getByCodigo( 'TB_SITUACAO_OFICINA', 'ABERTA' )
            if( !situacaoAberta ) {
                errors.push( 'Situação ABERTA não cadastrada na tabela de apoio TB_SITUACAO_OFICINA' )
            }
            else {

                params.sqSituacao = situacaoAberta.id
            }
        }
        DadosApoio fonteRecurso
        if( params.sqFonteRecurso ){
            fonteRecurso = DadosApoio.get( params.sqFonteRecurso.toLong() )
        }

        if( errors ) {
            this._returnAjax( 1, "Erros no preenchimento!<br>" + errors.join( '<br>' ), "error" )
        }
        DadosApoio tipoOficina = DadosApoio.get( params.int( 'sqTipoOficina' ) )
        if( tipoOficina.codigoSistema == 'OFICINA_VALIDACAO' ) {
            params.sqUnidadeOrg = null
            params.sqPontoFocal = null
        } else if( ! params.sqOficina ) {
            // PARA CADASTRAR UMA OFICINA DE AVALIAÇÃO DEVE FORNECER A UNIDADE ORGANIZACIONAL
            if( !params.sqUnidadeOrg ) {
                params.sqUnidadeOrg = session.sicae.user.sqUnidadeOrg
            }
            if( ! params.sqUnidadeOrg ) {
                return this._returnAjax( 1,'Unidade organizacional inexistente. Efetue login novamente','error' )
            }
        }

        Oficina oficina

        if( params.sqOficina ) {
            oficina = Oficina.get( params.int( 'sqOficina' ) )
        }
        else {
            oficina = new Oficina()
        }
        oficina.properties = params
        oficina.fonteRecurso = fonteRecurso
        oficina.cicloAvaliacao = CicloAvaliacao.get( params.int( 'sqCicloAvaliacao' ) )
        oficina.tipoOficina = tipoOficina
        if( params.sqUnidadeOrg ) {
            oficina.unidade = Unidade.get( params.int( 'sqUnidadeOrg' ) )
        }
        if( params.sqPontoFocal ) {
            oficina.pontoFocal = Pessoa.get( params.int( 'sqPontoFocal' ) )
        }
        if( params.sqSituacao) {
            oficina.situacao = DadosApoio.get(params.sqSituacao)
        }

        // SALVE SEM CICLO
        // se a oficina estiver aberta verificar se não possui choque de data com outra oficina aberta
        String oficinaChoque = oficinaService.periodoOficinaEmChoque(oficina)
        if( oficinaChoque ){
            return this._returnAjax( 1, oficinaChoque,'error' )
        }

        if( !oficina.save() ) {
            oficina.errors.allErrors.each {
                errors.push( 'Campo ' + it.getField() + ' não informado' )
            }
            this._returnAjax( 1, "Erro de inconsistência!<br>" + errors.join( '<br>' ), "error" )
            return
        }
        this._returnAjax( 0, "Registro salvo com sucesso!", "success" )
    }

    /**
     * alterar a situação da oficina somente*/
    def changeSituacao() {
        Map res = [ msg: '', status: 1, type: 'error' ]
        if( !params.sqOficina ) {
            res.msg = 'Id da oficina não informado!'
            render res as JSON
            return
        }
        if( !params.sqSituacao ) {
            res.msg = 'Id da situação não informado!'
            render res as JSON
            return
        }
        Oficina oficina = Oficina.get( params.sqOficina )
        if( oficina ) {
            res.msg = ''
            oficina.situacao = DadosApoio.get( params.sqSituacao )
            String erroChoque = oficinaService.periodoOficinaEmChoque( oficina )
            if( erroChoque ){
                oficina.discard()
                res.status=1
                res.type='error'
                res.msg=erroChoque
                render res as JSON
                return
            }

            //if( oficina.situacao.codigoSistema == 'ENCERRADA' ) {
                // não é para alterar a data final da oficina. Decidido em reunião 18/11/2020
                /*if( oficina.dtFim > Util.hoje() ) {
                    oficina.dtFim = Util.hoje()
                    res.msg = 'Data final da oficina ajustada para ' + oficina.dtFim.format( 'dd/MM/yyyy' )
                }*/
            //}
            oficina.save( flush: true )
            // atualizar a fichas EM_VALIACAO para AVALIADA_REVISADA
            if( oficina.tipoOficina.codigoSistema == 'OFICINA_VALIDACAO' ) {
                String cmdSql = ""
                if( oficina.situacao.codigoSistema == 'ENCERRADA' ) {
                    // se for oficina de VALIDAÇÃO, NÃO voltar as fichas EM_VALIDACAO para AVALIADA_REVISADA automaticamente,
                    // este procedimento deverá ser feito pelo PF ficha a ficha
                    /*DadosApoio situacaoAvaliadaRevisada = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'AVALIADA_REVISADA')
                    DadosApoio situacaoEmValiacao = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'EM_VALIDACAO')
                    cmdSql = """update salve.ficha set sq_situacao_ficha = ${ situacaoAvaliadaRevisada.id.toString() }
                        where sq_situacao_ficha = ${situacaoEmValiacao.id.toString()}
                        and sq_ficha in ( select x.sq_ficha from salve.oficina_ficha x where x.sq_oficina = ${oficina.id.toString()} );
                    """*/
                }
                else
                    if( oficina.situacao.codigoSistema == 'ABERTA' ) {
                        // se a ficha estiver na situacao AVALIDA_REVISADA, voltar as fichas em_validacao para EM_VALIDACAO
                        DadosApoio situacaoAvaliadaRevisada = dadosApoioService.getByCodigo( 'TB_SITUACAO_FICHA', 'AVALIADA_REVISADA' )
                        DadosApoio situacaoEmValiacao = dadosApoioService.getByCodigo( 'TB_SITUACAO_FICHA', 'EM_VALIDACAO' )
                        cmdSql = """update salve.ficha set sq_situacao_ficha = ${ situacaoEmValiacao.id.toString() }
                        where sq_situacao_ficha = ${ situacaoAvaliadaRevisada.id.toString() }
                        and sq_ficha in ( select x.sq_ficha from salve.oficina_ficha x where x.sq_oficina = ${
                            oficina.id.toString() } );
                    """
                    }
                sqlService.execSql( cmdSql )
            }
            res.status = 0
            res.type = 'success'
        }
        render res as JSON
    }


    /**
     * Excluir Oficina
     * @return [description]
     */
    def delete() {
        String cmdSql
        //def Oficina = Oficina.get(params.id)
        if( params.id ) {
            try {
                Oficina oficina = Oficina.get( params.id.toLong() )
                if( !oficina ) {
                    throw new Exception( 'Oficina iválida!' );
                }

                // SALVE SEM CICLO
                // se o oficina possuir alguma ficha versionada ela não pode ser excluída
                Integer nuFichasVersionadas = oficina.nuFichasVersionadas()
                if( nuFichasVersionadas > 0 ){
                    throw new Exception( 'Oficina não pode ser excluída porque possui ' + nuFichasVersionadas + ' ficha(s) versionada(s)' )
                }

                if( oficina.tipoOficina.codigoSistema == 'OFICINA_VALIDACAO' ) {
                    // se for oficina de VALIDAÇÃO, voltar as fichas em_validacao para AVALIADA_REVISADA
                    DadosApoio situacaoAvaliadaRevisada = dadosApoioService.getByCodigo( 'TB_SITUACAO_FICHA', 'AVALIADA_REVISADA' )
                    DadosApoio situacaoEmValiacao = dadosApoioService.getByCodigo( 'TB_SITUACAO_FICHA', 'EM_VALIDACAO' )
                    cmdSql = """update salve.ficha set sq_situacao_ficha = ${
                        situacaoAvaliadaRevisada.id.toString() }
                        where sq_situacao_ficha = ${ situacaoEmValiacao.id.toString() }
                        and sq_ficha in ( select x.sq_ficha from salve.oficina_ficha x where x.sq_oficina = ${
                        oficina.id.toString() } );
                    """
                    sqlService.execSql( cmdSql )
                }
                sqlService.execSql( "delete from salve.oficina where sq_oficina = ${ oficina.id.toString() };" )
                this._returnAjax( 0, "Registro excluido com sucesso!", "success" )
            }
            catch( Exception e ) {
                this._returnAjax( 1, e.getMessage(), "error" )
            }
        }
    }


    def getFormSelecionarFicha() {
        if( !params.sqOficinaSelecionada ) {
            render '<h4>Nenhuma oficina selecionada!</h4>'
            return;
        }
        Oficina oficina = Oficina.get( params.int( 'sqOficinaSelecionada' ) )
        if( !oficina ) {
            render '<h4>Oficina inválida!</h4>'
            return;
        }

        /* List listSituacaoFicha = []
        // oficina de validação já será filtrada pela situação AVALIADA_REVISADA
        if( oficina.tipoOficina.codigoSistema != 'OFICINA_VALIDACAO') {
            listSituacaoFicha = dadosApoioService.getTable('TB_SITUACAO_FICHA')
        }
        List listNivelTaxonomico = NivelTaxonomico.list()
        */
        boolean canModify = oficina.canModify()
        render( template: 'formSelecionarFicha', model: [ oficina    : oficina
                                                          //,listSituacaoFicha:listSituacaoFicha
                                                          //,listNivelTaxonomico:listNivelTaxonomico
                                                          , oficina  : oficina
                                                          , canModify: canModify ] )
    }

    /**
     * Construir gride FICHAS que podem ser adicionadas na oficina
     * */
    def getGridFicha1() {

        List listFichas = []
        boolean canModify
        Map pagination = [:]

        if (!params.sqCicloAvaliacao) {
            render '<h4>Ciclo não informado</h4>'
            return
        }
        if (!params.sqOficina) {
            render '<h4>Oficina não selecionada!</h4>'
            return
        }


        List listSituacaoFicha = [ ]

        // ler o ponto focal da oficina
        Oficina oficina = Oficina.get( params.sqOficina.toLong() )
        boolean isPrimeiroCiclo = (oficina.cicloAvaliacao.nuAno == 2010)
        canModify = oficina.canModify()

        if( oficina.tipoOficina.codigoSistema != 'OFICINA_VALIDACAO' ) {
            //params.sqPontoFocalFiltro = oficina?.pontoFocal?.id
            // ler a situações válidas para as fichas
            //List listSituacaoFicha = fichaService.getListSituacao()

            // somente as fichas consolidadas podem ser adicionadas na oficina
            DadosApoio situacaoConsolidada = dadosApoioService.getByCodigo( 'TB_SITUACAO_FICHA', 'CONSOLIDADA' )
            params.sqSituacaoFiltro = situacaoConsolidada.id

            // para o primeiro permitir as fichas validadas para poder fazer o cadastramento do histórico da oficina
            // das oficinas já validadas
            if( isPrimeiroCiclo ) {
                DadosApoio situacaoValidada = dadosApoioService.getByCodigo( 'TB_SITUACAO_FICHA', 'VALIDADA' )
                params.sqSituacaoFiltro=situacaoConsolidada.id
                params.sqSituacaoFiltro += ',' + situacaoValidada.id
            }
        }
        else {
            DadosApoio situacaoAvaliadaRevisada = dadosApoioService.getByCodigo( 'TB_SITUACAO_FICHA', 'AVALIADA_REVISADA' )
            params.sqSituacaoFiltro = situacaoAvaliadaRevisada.id
        }


        // adicionar colunas referente ao grupo taxonomico no resultado do search
        params.colGrupoTaxonomico = true


        if( !params.gridHash ) {

            // ler as fichas já adicionadas na oficina
            String hql = "select new map(f.id as id) from OficinaFicha a join a.vwFicha f join a.oficina o where o.id = " + oficina.id.toString()

            //List fichasSelecionadas = OficinaFicha.executeQuery( hql )
            List fichaNaoFiltro = OficinaFicha.executeQuery(hql)?.id

            // selecionar as fichas
            params.sqFichaNaoFiltro = fichaNaoFiltro
            listFichas = fichaService.search(params)

            // criar as páginas no disco
            pagination = Util.paginateArrayToDisk(listFichas
                , 100
                , params.gridPage ? params.gridPage.toInteger() : 1
            )
            // retornar a pagina 1
            listFichas = pagination.rows
        }
        else  {
            listFichas = Util.paginateArrayGetPageData(params.gridHash, params.gridPage.toInteger())

        }
        render( template: "divGridSelFicha"
            , model: [ listFichas         : listFichas
                       , listSituacaoFicha: listSituacaoFicha
                       , oficina          : oficina
                       , canModify        : canModify
                       , isPrimeiroCiclo  : isPrimeiroCiclo
                       , pagination       : pagination
                ])
    }

    /*
    * Construir o gride com as fichas já selecioandas na oficina
    */

    def getGridFicha2() {
        List listFichas
        boolean canModify
        Map pagination = [:]

        if( !params.sqOficina ) {
            render '<h4>Oficina não informada</h4>'
            return
        }

        canModify = Oficina.get( params.sqOficina.toInteger() ).canModify()

        if( !params.gridHash ) {
            // criar o parametro de filtro pela oficina para o metodo search
            params.sqOficinaFiltro = params.sqOficina

            // adicionar colunas referente ao grupo taxonomico no resultado do search
            params.colGrupoTaxonomico = true
            params.colAgrupamento = true // adicionar a coluna ds_agrupamento no resultado do sql

            // executar a consulta das fichas
            listFichas = fichaService.search(params)

            // criar as páginas no disco
            pagination = Util.paginateArrayToDisk(listFichas
                , 100
                , params.gridPage ? params.gridPage.toInteger() : 1
            )
            // retornar a pagina selecionada
            listFichas = pagination.rows

            // numerar corretamente as linhas do gride de acordo com a página selecionada
            params.gridRowNum = pagination.rowNum

        } else {
            listFichas = Util.paginateArrayGetPageData(params.gridHash, params.gridPage.toInteger())
        }

        render( template: "divGridSelFichaSelecionada", model: [ listFichas     : listFichas
                                                                 ,pagination    : pagination
                                                                 ,canModify     : canModify ] )
    }

    /**
     * Gravar as fichas selecionadas para a oficina
     * @return
     */
    def saveFicha() {
        Map res = [ status: 1, type: 'error', msg: '', errors: [ ] ]
        List idsAlterar = [ ]
        DadosApoio emValidacao = dadosApoioService.getByCodigo( 'TB_SITUACAO_FICHA', 'EM_VALIDACAO' )

        if( !params.sqOficina ) {
            res.errors.push( 'Oficina não informada' )
        }
        else
            if( !emValidacao ) {
                res.errors.push( 'Situação EM_VALIDACAO não cadastradas na TB_SITUACAO_FICHA!' )
            }
            else
                if( !params[ 'ids' ] ) {
                    res.errors.push( 'Nenhuma ficha selecionada!' )
                }
                else {
                    List ids = [ ]
                    params.ids.split( ',' ).each {
                        ids.push( it.toInteger() )
                    }
                    List novos = ids
                    OficinaFicha oficinaFicha
                    Oficina oficina = Oficina.get( params.sqOficina.toInteger() )
                    if( oficina ) {
                        try {
                            List fichasAtuais = OficinaFicha.findAllByOficina( oficina ).each {
                                if( !ids.find { obj -> return obj == it.ficha.id
                                } ) {
                                    it.deleted = true
                                }
                                else {
                                    novos.remove( novos.indexOf( ( Integer ) it.ficha.id ) )
                                }
                            }
                            novos.each {
                                Ficha ficha = Ficha.get( it.toLong() )
                                if( ficha ) {
                                    oficinaFicha = new OficinaFicha( oficina: oficina, ficha: ficha, situacaoFicha: ficha.situacaoFicha ).save( flush: true )
                                    if( oficina.tipoOficina.codigoSistema == 'OFICINA_VALIDACAO' ) {
                                        oficinaFicha.situacaoFicha = emValidacao
                                        oficinaFicha.categoriaIucn = ficha.categoriaIucn
                                        oficinaFicha.dsCriterioAvalIucn = ficha.dsCriterioAvalIucn
                                        oficinaFicha.stPossivelmenteExtinta = ficha.stPossivelmenteExtinta
                                        oficinaFicha.dsJustificativa = ficha.dsJustificativa
                                        oficinaFicha.save( flush: true )
                                        idsAlterar.push( ficha.id.toLong() )
                                    }
                                }
                            }

                            if( idsAlterar ) {
                                // alterar a situação das fichas
                                fichaService.setSituacaoByIds( idsAlterar, emValidacao.id.toLong() )
                                // as fichas que entrarem em oficina de validação que estiverem em
                                // outra oficina de validação e os validadores não responderam ainda
                                // devem ser removidas dos validadores
                                String cmd = """delete from salve.validador_ficha where sq_validador_ficha in ( select validador_ficha.sq_validador_ficha
                            from salve.validador_ficha
                            inner join salve.oficina_ficha on oficina_ficha.sq_oficina_ficha = validador_ficha.sq_oficina_ficha
                            where validador_ficha.sq_resultado is null
                            and oficina_ficha.sq_ficha in ( ${ idsAlterar.join( ',' ) } )
                            and oficina_ficha.sq_oficina_ficha <> ${ oficinaFicha.id.toString() }
                        )"""
                                sqlService.execSql( cmd )
                            }

                            // quando a ficha entrar em uma oficina de avaliação, remover a situação transferida das fichas que foram transferidas da oficina anterior
                            // a situação transferida ficará registrada na tabela oficina_ficha apenas
                            if( oficina.tipoOficina.codigoSistema == 'OFICINA_AVALIACAO' ) {
                                String cmd = "update salve.ficha set st_transferida = null where sq_ficha in (${ novos.join( ',' ) }) and st_transferida=true"
                                sqlService.execSql( cmd )
                            }

                            res.status = 0
                            res.msg = ''
                            res.type = 'success'
                        }
                        catch( Exception e ) {
                            println e.getMessage()
                            res.status = 1
                            res.errors.push( 'Erro de processamento!' )
                        }
                    }
                }
        if( res.errors ) {
            res.error = res.errors.join( '<br>' )
            res.errors = [ ]
        }
        render res as JSON
    }

    /*
    * Excluir as fichas selecionadas da oficina
    */

    def deleteFicha() {
        Map res = [ status: 0, type: 'success', msg: '', errors: [ ] ]
        DadosApoio situacaoVoltar
        OficinaFicha oficinaFicha

        if( !params.sqOficina ) {
            res.errors.push( 'Id da oficina não informado!' )
        }
        else
            if( !params[ 'ids' ] ) {
                res.errors.push( 'Nenhuma ficha selecionada!' )
            }
            else {

                try {
                    List idsAlterarSituacao = [ ]
                    params.ids.split( ',' ).each {
                        oficinaFicha = OficinaFicha.get( it.toInteger() )
                        if( oficinaFicha ) {
                            idsAlterarSituacao.push( oficinaFicha.vwFicha.id.toLong() )
                            oficinaFicha.delete( flush: true )
                        }
                    }
                    if( idsAlterarSituacao ) {
                        Oficina oficina = Oficina.get( params.sqOficina.toLong() )

                        // se a ficha for removida de uma oficina de avaliação
                        // voltar a situação para CONSOLIDADA
                        if( oficina.tipoOficina.codigoSistema == 'OFICINA_AVALIACAO' ) {
                            situacaoVoltar = dadosApoioService.getByCodigo( 'TB_SITUACAO_FICHA', 'CONSOLIDADA' )
                        }
                        else {
                            situacaoVoltar = dadosApoioService.getByCodigo( 'TB_SITUACAO_FICHA', 'AVALIADA_REVISADA' )
                        }
                        fichaService.setSituacaoByIds( idsAlterarSituacao, situacaoVoltar.id.toLong() )
                    }
                    idsAlterarSituacao = [ ]
                }
                catch( Exception e ) {
                    println e.getMessage()
                    res.status = 1
                    res.errors.push( 'Erro na exclusão!' )
                }
            }
        render res as JSON
    }

    def saveAgrupamento() {
        Map res = [ status: 0, type: 'success', msg: '', errors: [ ] ]
        if( !params[ 'ids' ] ) {
            res.errors.push( 'Nenhuma ficha selecionada!' )
        }
        else {
            OficinaFicha oficinaFicha
            try {
                params.ids.split( ',' ).each {
                    oficinaFicha = OficinaFicha.get( it.toInteger() )
                    if( OficinaFicha ) {
                        if( params.dsAgrupamento ) {
                            oficinaFicha.dsAgrupamento = params.dsAgrupamento
                        }
                        else {
                            oficinaFicha.dsAgrupamento = null
                        }
                        oficinaFicha.save( flush: true )
                    }
                }
            }
            catch( Exception e ) {
                println e.getMessage()
                res.status = 1
                res.errors.push( 'Erro na exclusão!' )
            }
        }
        render res as JSON
    }

    /**
     * ABA PARTICIPANTES*/

    def getFormParticipante() {
        if( params.sqOficinaSelecionada ) {
            List listSitParticipante = dadosApoioService.getTable( 'TB_SITUACAO_PARTICIPANTE' )
            String emailPadrao = session.sicae.user.email
            Oficina oficina = Oficina.get( params.sqOficinaSelecionada.toInteger() )
            boolean canModify = oficina.canModify()
            render( template: "formCadParticipante", model: [ emailPadrao          : emailPadrao
                                                              , oficina            : oficina
                                                              , listSitParticipante: listSitParticipante
                                                              , canModify          : canModify ] )
        }
        else {
            render '<h4>Nenhuma oficina selecionada'
        }
    }

    def saveParticipante() {
        Map res = [ status: 0, type: 'success', msg: '', errors: [ ] ]
        DadosApoio situacaoInicial = dadosApoioService.getByCodigo( 'TB_SITUACAO_CONVITE_PESSOA', 'EMAIL_NAO_ENVIADO' )
        if( !situacaoInicial ) {
            res.msg = 'Código EMAIL_NAO_ENVIADO não cadastrado na tabela de apoio TB_SITUACAO_CONVITE_PESSOA'
        }
        else
            if( !params.sqOficina ) {
                res.status = 1
                res.type = 'error'
                res.msg = 'Oficina não inforamada!'
            }
            else {
                OficinaParticipante op = new OficinaParticipante()
                op.oficina = Oficina.get( params.int( 'sqOficina' ) )
                op.situacao = situacaoInicial
                op.instituicao = Instituicao.get( params.int( 'sqInstituicao' ) )
                op.pessoa = Pessoa.get( params.int( 'sqParticipante' ) )
                op.properties = params
                res.status = 0
                res.type = 'success'
                res.msg = 'Participante adicionado com SUCESSO!'
                if( !op.save( flush: true ) ) {
                    op.errors.allErrors.each {
                        res.errors.push( messageSource.getMessage( it, locale ) );
                        // locale=Locale.getDefault() no baseController
                    }
                    res.status = 1
                    res.type = 'error'
                    res.msg = ''
                }
            }
        render res as JSON
    }

    def deleteParticipante() {
        Map res = [ status: 0, type: 'success', msg: '', errors: [ ] ]

        if( !params[ 'ids' ] ) {
            res.errors.push( 'Nenhuma ficha selecionada!' )
        }
        else {
            List ids = [ ]
            params.ids.split( ',' ).each {
                ids.push( it.toInteger() )
            }
            try {
                params.ids.split( ',' ).each {
                    OficinaParticipante.get( it.toInteger() ).delete( flush: true )
                }
            }
            catch( Exception e ) {
                println e.getMessage()
                res.status = 1
                res.msg.push( 'Erro na exclusão!' )
            }
        }
        render res as JSON
    }


    def getGridParticipante() {
        if( !params.sqOficina ) {
            render '<h4>Oficina não informada!'
            return
        }

        Oficina oficina = Oficina.get( params.sqOficina.toLong() )
        boolean canModify = oficina.canModify()
        boolean hasDocFinal = oficina.hasDocFinal()

        //List listOficinaParticipante = VwOficinaParticipante.findAllBySqOficina( oficina.id, [sort:'noPessoa'] )

        List listOficinaParticipante = VwOficinaParticipante.createCriteria().list {
            eq( 'sqOficina', oficina.id.toInteger() )
            if( params.id ) {
                eq( 'id', params.id.toLong() )
            }
            order( 'noPessoa' )
        }

        List listSituacaoConvite = dadosApoioService.getTable( 'TB_SITUACAO_CONVITE_PESSOA' )
        render( template: 'divGridParticipante', model: [ listOficinaParticipante: listOficinaParticipante
                                                          , listSituacaoConvite  : listSituacaoConvite
                                                          , canModify            : canModify
                                                          , hasDocFinal          : hasDocFinal ] )
    }

    def getTextLastEmail() {
        if( !params.sqOficina ) {
            render ''
        }
        render Oficina.get( params.sqOficina.toInteger() )?.txEmailParticipantes ?: ''
    }

    def enviarEmailParticipantes() {
        Map res = [ status: 0, type: 'success', msg: '', errors: [ ] ]
        if( !params.ids ) {
            res.msg = 'Nenhum participante selecionado!'
            res.status = 1
            res.type = 'error'
        }
        else {
            List ids = [ ]
            params.ids.split( ',' ).each {
                ids.push( it.toInteger() )
            }
            try {
                PessoaFisica pf = PessoaFisica.get( session.sicae.user.sqPessoa )
                String emailFrom = pf.email
                OficinaParticipante op
                String textoEmailCompleto
                DadosApoio situacaoEnviado = dadosApoioService.getByCodigo( 'TB_SITUACAO_CONVITE_PESSOA', 'EMAIL_ENVIADO' );
                res.status = 0
                res.type = 'success'
                res.msg = 'Email(s) enviado(s) com SUCESSO!'

                // anexo
                Map anexo = emailService.saveTempFile( request.getFile( 'fldAnexo' ) )
                if( anexo.error ) {
                    res.status  = 1
                    res.type    ='error'
                    res.msg     = anexo.error
                    render res as JSON
                    return;
                }

                params.ids.split( ',' ).each {
                    op = OficinaParticipante.get( it.toInteger() )
                    if( op ) {

                        textoEmailCompleto = params.txEmail
                        textoEmailCompleto = textoEmailCompleto.replaceAll( /\{nome\}/, op.pessoa.noPessoa )
                        textoEmailCompleto = textoEmailCompleto.replaceAll( /\{oficina\}/, op.oficina.noOficina )
                        textoEmailCompleto = textoEmailCompleto.replaceAll( /\{periodo\}/, op.oficina.periodo )
                        textoEmailCompleto = textoEmailCompleto.replaceAll( /\{linkSalve\}/, '<a target="_blank" href="' + this.baseUrl + '">Clique aqui para acessar o SALVE</a>' )
                        if( emailService.sendTo( op.email, params.deAssuntoEmail, textoEmailCompleto, emailFrom, anexo ) ) {
                            op.deEmail = op.email
                            op.dtEmail = new Date()
                            op.situacao = situacaoEnviado
                            op.save( flush: true );
                            if( params.deEmailCopia ) {
                                params.deEmailCopia.split( ',' ).each {
                                    emailService.sendTo( it.trim(), 'Cópia - ' + params.deAssuntoEmail, textoEmailCompleto, '', anexo )
                                }
                            }
                        }
                        else {
                            println 'Erro envio de email para ' + op.email
                            res.errors.push( 'Erro envio de email para ' + op.email )
                            res.status = 1
                            res.msg = ''
                        }
                    }
                }
                // excluir o arquivo anexo do disco
                if( anexo.fullPath ) {
                    try {
                        new File( anexo.fullPath ).delete()
                    }
                    catch( Exception e ) {
                    }
                }
                // guardar dados do último email enviado para reaproveitar nos próximo
                if( params.sqOficina ) {
                    Oficina oficina = Oficina.get( params.sqOficina.toInteger() )
                    if( oficina ) {
                        oficina.txEmailParticipantes = 'Assunto:' + params.deAssuntoEmail + "\nCc:" + params.deEmailCopia + "\n\n" + params.txEmail.trim()
                        oficina.save( flush: true )
                    }
                }
            }
            catch( Exception e ) {
                println e.getMessage()
                res.status = 1
                res.msg = 'Erro na exclusão!'
            }

        }
        render res as JSON
    }

    def changeSituacaoConvite() {
        if( params.id && params.sqSituacao ) {
            OficinaParticipante reg = OficinaParticipante.get( params.id.toInteger() )
            if( reg ) {
                reg.situacao = DadosApoio.get( params.sqSituacao.toInteger() )
                reg.save( flush: true )
            }
        }
        render ''
    }

    def saveLinkSei() {
        Map res = [ status: 1, type: 'error', msg: '' ]
        if( params.sqOficina ) {
            Oficina oficina = Oficina.get( params.sqOficina.toInteger() )
            if( oficina ) {
                oficina.deLinkSei = params.deLinkSei ?: null
                oficina.save( flush: true )
                res.status = 0
                res.type = 'success'
                res.msg = 'Link Sei gravado com SUCESSO!'
                render res as JSON
                return
            }
            res.msg = 'Id Inválido'
            render res as JSON
            return;
        }
        msg = 'Id da oficina não informado!'
        render res as JSON
    }

    def getGridImportarParticipante(){
        if( !params.sqCicloConsulta ) {
            render ''
            return
        }

        List listParticipantes = sqlService.execSql("""select ccp.sq_ciclo_consulta_pessoa
            , ccp.sq_pessoa
            , pf.no_pessoa
            , ccp.de_email
            , inst.sg_instituicao
            from salve.ciclo_consulta_pessoa ccp
            inner join salve.vw_pessoa_fisica pf on pf.sq_pessoa = ccp.sq_pessoa
            inner join salve.web_instituicao inst on inst.sq_web_instituicao = ccp.sq_instituicao
            where ccp.sq_ciclo_consulta = :sqCicloConsulta
            and ccp.de_email is not null
            order by lower( public.fn_remove_acentuacao( pf.no_pessoa ) )
            """,[sqCicloConsulta:params.sqCicloConsulta.toLong()])

        render( template: 'divGridImportarParticipantes', model: [listParticipantes: listParticipantes ] )
    }

    /**
     * adicionar na oficina os participantes da consulta/revisao selecionados
     * @return
     */
    def saveImportacaoParticipantes() {
        Map res = [ status:0, type:'success',msg:'']
        try {
            if( !params.ids ){
                throw new Exception('Nenhum participante selecionado!')
            }

            if( !params.sqCicloConsultaImportar ) {
                throw new Exception('Consulta/revisão não informada!')
            }

            if( !params.sqOficina ) {
                throw new Exception('Avaliação não informada!')
            }
            Oficina oficina = Oficina.get( params.sqOficina )
            if( !oficina ) {
                throw new Exception('Oficina inválida!')
            }


            /*if( !oficina.canModify() ) {
                throw new Exception('Oficina não pode ser alterada!')
            }*/

            // ler os participantes selecionados
            String cmdSql = """select ccp.sq_ciclo_consulta, ccp.sq_pessoa, ccp.de_email, ccp.sq_instituicao
            from salve.ciclo_consulta_pessoa ccp
            where ccp.sq_ciclo_consulta = :sqCicloConsulta
              and ccp.sq_ciclo_consulta_pessoa in ( ${params.ids} )
            """
            List listParticipantes = sqlService.execSql(cmdSql, [sqCicloConsulta: params.sqCicloConsultaImportar.toLong()])

            DadosApoio situacaoInicial = dadosApoioService.getByCodigo( 'TB_SITUACAO_CONVITE_PESSOA', 'EMAIL_NAO_ENVIADO' )
            if( ! situacaoInicial ) {
                throw new Exception('Código EMAIL_NAO_ENVIADO não cadastrado na tabela de apoio TB_SITUACAO_CONVITE_PESSOA')
            }
            listParticipantes.each { row ->
                Pessoa pessoa = Pessoa.get(row.sq_pessoa)
                if( ! OficinaParticipante.findByOficinaAndPessoa(oficina, pessoa) ) {
                    OficinaParticipante op = new OficinaParticipante()
                    op.oficina = oficina
                    op.pessoa = pessoa
                    op.instituicao = WebInstituicao.get(row.sq_instituicao.toLong())
                    op.situacao = situacaoInicial
                    op.deEmail = row.de_email.toLowerCase()
                    if (op.validate()) {
                        op.save()
                    } else {
                        println op.getErrors()
                    }
                }
            }
        } catch ( Exception e ) {
            res.type= 'error'
            res.msg=e.getMessage()
        }
        render res as JSON
    }


    /**
     * ABA MEMORIA*/

    def getFormMemoria() {
        if( params.sqOficinaSelecionada ) {

            Oficina oficina = Oficina.get( params.sqOficinaSelecionada.toInteger() )
            OficinaMemoria oficinaMemoria = OficinaMemoria.findByOficina( oficina )
            List listPapeis = dadosApoioService.getTable( 'TB_PAPEL_OFICINA' ).findAll {
                return it.codigoSistema != 'VALIDADOR' && it.codigoSistema != 'COLABORADOR_AMPLA'
            }.sort {
                it.descricao
            }

            boolean canModify = oficina.canModify()
            render( template: "formCadMemoria", model: [ oficina         : oficina
                                                         , oficinaMemoria: oficinaMemoria
                                                         , listPapeis    : listPapeis
                                                         , canModify     : canModify ] )
        }
        else {
            render '<h4>Nenhuma oficina selecionada'
        }
    }

    def saveFormMemoria() {
        Map res = [ status: 0, type: 'success', msg: '', errors: [ ] ]

        if( !params.sqOficina ) {
            res.errors.push( 'Oficina não informada.' )
        }

        if( res.errors.size() > 0 ) {
            res.status = 1
            res.type = 'error'
            render res as JSON
            return;
        }
        try {

            OficinaMemoria oficinaMemoria = null;
            if( !params.sqOficinaMemoria ) {
                Oficina oficina = Oficina.get( params.sqOficina.toLong() )
                oficinaMemoria = new OficinaMemoria()
                oficinaMemoria.oficina = oficina
            }
            else {
                oficinaMemoria = OficinaMemoria.get( params.sqOficinaMemoria.toLong() )
            }
            oficinaMemoria.dtInicioOficina = new Date().parse( 'dd/MM/yyyy', params.dtInicioOficina )
            oficinaMemoria.dtFimOficina = new Date().parse( 'dd/MM/yyyy', params.dtFimOficina )
            oficinaMemoria.deSalas = params.deSalas
            oficinaMemoria.txMemoria = params.txMemoria
            if( oficinaMemoria.dtInicioOficina > oficinaMemoria.dtFimOficina ) {
                throw new Exception( 'Data inicial deve ser MENOR ou IGUAL a data final.' )
            }
            oficinaMemoria.deLocalOficina = params.deLocalOficina // local onde ocorrerá a oficina
            if( params.txMemoria ) {
                oficinaMemoria.txMemoria = params.txMemoria
            }
            if( !oficinaMemoria.save( flush: true ) ) {
                oficinaMemoria.errors.allErrors.each {
                    res.errors.push( messageSource.getMessage( it, locale ) );
                }
                res.status = 1
                res.type = 'error'
                res.msg = 'Ocorreu um erro na gravação.'
                println res.errors
                render res as JSON
                return
            }

            // retornar no id da oficinaMemoria
            res.data = [ sqOficinaMemoria: oficinaMemoria.id ]

            if( params.txConvidado ) {
                DadosApoio papel = DadosApoio.get( params.sqPapel.toInteger() )
                List participantes = params.txConvidado.split( "\n" ).collect {
                    Util.capitalize( it.trim() )
                }
                participantes.each {
                    if( it ) {
                        OficinaMemoriaParticipante omp = OficinaMemoriaParticipante.findByOficinaMemoriaAndPapelAndNoParticipante( oficinaMemoria, papel, it );
                        if( !omp ) {
                            omp = new OficinaMemoriaParticipante()
                        }
                        omp.oficinaMemoria = oficinaMemoria
                        omp.papel = papel
                        omp.noParticipante = it
                        omp.save( flush: true )
                    }
                }
            }
            res.status = 0;
            res.msg = 'Dados gravados com SUCESSO!'
        }
        catch( Exception e ) {
            res.status = 1
            res.msg = e.getMessage()
            println res.msg;
        }
        render res as JSON
    }

    def getGridParticipanteMemoria() {
        if( !params.sqOficina ) {
            render ''
            return;
        }
        Oficina oficina = Oficina.get( params.sqOficina.toInteger() )
        OficinaMemoria oficinaMemoria = OficinaMemoria.findByOficina( oficina )
        boolean canModify = oficina.canModify()
        List listParticipantes = OficinaMemoriaParticipante.findAllByOficinaMemoria( oficinaMemoria, [ 'sort': 'noParticipante' ] )
        render( template: 'divGridParticipanteMemoria', model: [ listParticipantes: listParticipantes
                                                                 , canModify      : canModify ] )
    }

    def deleteParticipantesMemoria() {
        if( params.ids ) {
            try {
                OficinaMemoriaParticipante.executeUpdate( "delete OficinaMemoriaParticipante c where c.id in (" + params.ids + ")" )
                return this._returnAjax( 0, "Exclusão realizada com SUCESSO!", "success" )
            }
            catch( Exception e ) {
                println e.getMessage()
                return this._returnAjax( 1, "Não foi possível excluir o registro!", "error" )
            }
        }
        return this._returnAjax( 1, "Nenhum participante selecionado!", "info" )
    }

    def getGridAnexoMemoria() {
        if( !params.sqOficinaMemoria ) {
            render '<h2>Id reunião não informada!</h2>'
            return
        }
        //List listOficinaAnexo = sqlService.execSql("""select * from salve.vw_oficina_anexo where sq_oficina = :sqOficina""",[ sqOficina : params.sqOficina.toLong() ])
        render( template: 'divGridOficinaMemoriaAnexo', model: [ listAnexos: oficinaService.getAnexosMemoria( params.sqOficinaMemoria.toLong() ) ] )

    }

    def saveAnexoMemoria() {
        //return this._returnAjax(1, 'Verificar parametros!', 'error');
        if( !session?.sicae?.user?.sqPessoa ) {
            return this._returnAjax( 1, 'Necessário fazer login para salvar anexo!.', "error" )
        }
        String anexosDir = ( grailsApplication.config.anexos.dir + '/' ).replaceAll( /anexos\/\//, 'anexos/' )
        // gravar o anexo
        def f = request.getFile( 'fldOficinaAnexo' )
        if( f && !f.empty ) {
            File savePath = new File( anexosDir )
            if( !savePath.exists() ) {
                if( !savePath.mkdirs() ) {
                    return this._returnAjax( 1, 'Não foi possivel criar o diretório ' + savePath.asString(), "error" );
                }
            }
            OficinaMemoria oficinaMemoria = OficinaMemoria.get( params.sqOficinaMemoria.toLong() )
            if( oficinaMemoria ) {
                String fileName = f.getOriginalFilename().replaceAll( ' |,|\\(.+\\)', '_' ).replaceAll( '_+', '-' ).replaceAll( '-\\.', '.' )
                String fileExtension = fileName.substring( fileName.lastIndexOf( "." ) )
                String hashFileName = f.inputStream.text.toString().encodeAsMD5() + fileExtension
                fileName = savePath.absolutePath + '/' + hashFileName
                if( !new File( fileName ).exists() ) {
                    f.transferTo( new File( fileName ) );
                    ImageService.createThumb( fileName )
                }
                OficinaMemoriaAnexo oficinaMemoriaAnexoNovo = OficinaMemoriaAnexo.findByOficinaMemoriaAndDeLocalArquivo( oficinaMemoria, hashFileName )
                if( !oficinaMemoriaAnexoNovo ) {
                    oficinaMemoriaAnexoNovo = new OficinaMemoriaAnexo()
                    oficinaMemoriaAnexoNovo.oficinaMemoria = oficinaMemoria
                    oficinaMemoriaAnexoNovo.deLocalArquivo = hashFileName
                }
                oficinaMemoriaAnexoNovo.pessoa = PessoaFisica.get( session.sicae.user.sqPessoa )
                oficinaMemoriaAnexoNovo.deLegenda = params.deLegendaOficinaAnexo
                oficinaMemoriaAnexoNovo.noArquivo = f.getOriginalFilename()
                oficinaMemoriaAnexoNovo.deTipoConteudo = f.getContentType()
                if( !oficinaMemoriaAnexoNovo.save() ) {
                    println oficinaMemoriaAnexoNovo.getErrors()
                    println "-" * 100
                    return this._returnAjax( 1, 'Erroa ao salvar o anexo!', 'error' )
                }
                return this._returnAjax( 0, 'Anexo gravado com SUCESSO!', 'success' )
            }
        }
        return this._returnAjax( 1, 'Não foi possível gravar o anexo!', 'info' );

    }

    def gerarDocMemoria() {
        sleep( 500 )

        // flag utilizado no método getGridExecucaoTotais()
        params.relatorio = true

        if( !params.sqOficinaMemoria ) {
            return this._returnAjax( 1, 'Parâmetros insuficientes!', 'error' );
        }
        OficinaMemoria oficinaMemoria = OficinaMemoria.get( params.sqOficinaMemoria.toLong() )

        if( oficinaMemoria ) {
            String unidade = oficinaMemoria.oficina.unidade.noPessoa ? oficinaMemoria.oficina.unidade.noPessoa.toUpperCase() : 'SALVE'
            String unidadeSigla = oficinaMemoria.oficina.unidade.sgUnidade ?: 'Sistema de Avaliação'

            RtfOficinaMemoria rtf = new RtfOficinaMemoria( params.sqOficinaMemoria.toLong()
                , unidade
                , unidadeSigla
                , grailsApplication.config.temp.dir.toString() )
            /*
            String dia      = params.dtInicio
            String periodo  = ''
            if( oficina.dtInicio && oficina.dtFim )
            {
                if( oficina.dtInicio == oficina.dtFim)
                {
                    periodo = oficina.dtInicio.format('dd/MM/yyyy')
                }
                else if( oficina.dtInicio.format('MM/yyyy') == oficina.dtFim.format('MM/yyyy') )
                {

                    periodo = oficina.dtInicio.format('dd') + ' a ' + oficina.dtFim.format('dd/MM/yyyy')
                }
                else
                {
                    periodo = oficina.dtInicio.format('dd/MM/yyyy') + ' a ' + oficina.dtFim.format('dd/MM/yyyy')
                }
            }*/
            String fileName = rtf.run()
            if( !fileName ) {
                return this._returnAjax( 1, 'Ocorreu um erro na criação do arquivo.', 'warining', [ fileName: '' ] )
            }
            return this._returnAjax( 0, '', 'success', [ fileName: fileName ] )
        }
        else {
            return this._returnAjax( 1, 'Oficina inválida.', 'error' )
        }
    }


    def saveNomeParticipanteMemoriaReuniao() {

        Map res = [ status: 0, msg: '', type: 'success', errors: [ ] ]
        try {
            if( !params.noParticipante ) {
                throw new Exception( 'Nome deve ser informado!' )
            }

            if( !params.sqOficinaMemoriaParticipante ) {
                throw new Exception( 'Id do participante deve ser informado!' )
            }
            OficinaMemoriaParticipante op = OficinaMemoriaParticipante.get( params.sqOficinaMemoriaParticipante.toLong() )
            if( op ) {
                op.noParticipante = params.noParticipante
                op.save( flush: true )
                res.msg = 'Nome do participante alterado com SUCESSO!'
            }
            else {
                throw new Exception( 'Id inválido!' )
            }
        }
        catch( Exception e ) {
            res.status = 1
            res.msg = e.getMessage()
        }
        render res as JSON

    }

    def saveFuncaoParticipanteMemoriaReuniao() {
        Map res = [ status: 0, msg: '', type: 'success', errors: [ ] ]
        try {
            if( !params.sqPapel ) {
                throw new Exception( 'Função deve ser informada!' )
            }

            if( !params.sqOficinaMemoriaParticipante ) {
                throw new Exception( 'Id do participante deve ser informado!' )
            }
            OficinaMemoriaParticipante op = OficinaMemoriaParticipante.get( params.sqOficinaMemoriaParticipante.toLong() )
            if( op ) {
                op.papel = DadosApoio.get( params.sqPapel.toInteger() )
                op.save( flush: true )
                res.msg = 'Função do participante alterada com SUCESSO!'
            }
            else {
                throw new Exception( 'Id inválido!' )
            }
        }
        catch( Exception e ) {
            res.status = 1
            res.msg = e.getMessage()
        }
        render res as JSON
    }

    //*********************************************************************************
    //*********************************************************************************
    //*********************************************************************************
    //*********************************************************************************

    // ABA PAPEL/FUNÇÃO

    def getFormPapel() {
        if( params.sqOficinaSelecionada ) {
            Oficina oficina = Oficina.get( params.sqOficinaSelecionada.toInteger() )
            List listPapeis = dadosApoioService.getTable( 'TB_PAPEL_OFICINA' ).findAll {
                //return true
                return it.codigoSistema != 'VALIDADOR' //|| oficina.tipoOficina.codigoSistema == 'OFICINA_VALIDACAO'
            }.sort {
                it.descricao
            }
            List listParticipantes = OficinaParticipante.findAllByOficina( oficina, [ sort: "pessoa.noPessoa" ] )
            //List listFichas        = OficinaFicha.findAllByOficina(oficina)
            boolean canModify = oficina.canModify()
            //String emailPadrao = session.sicae.user.email
            render( template: "formCadPapel", model: [ listPapeis         : listPapeis
                                                       , oficina          : oficina
                                                       , listParticipantes: listParticipantes
                                                       //,listFichas:listFichas
                                                       , canModify        : canModify ] )
        }
        else {
            render '<h4>Nenhuma oficina selecionada'
        }
    }

    def getFichasParticipante() {
        sleep( 500 )
        String sql = """select new map( c.nmCientifico as nmCientifico , e.coNivelTaxonomico as coNivelTaxonomico, fv.id as sqFichaVersao, fv.nuVersao as nuVersao )
            from OficinaFichaParticipante a
            join a.oficinaParticipante b
            join a.oficinaFicha.ficha c
            left join a.oficinaFicha.oficinaFichaVersao v
            left join v.fichaVersao fv
            join c.taxon d
            join d.nivelTaxonomico e
            where b.id = :sqOficinaParticipante and a.papel.id = :sqPapel
            order by a.oficinaFicha.ficha.nmCientifico
            """
        render OficinaParticipante.executeQuery( sql, [ sqOficinaParticipante: params.sqOficinaParticipante.toLong(), sqPapel: params.sqPapel.toLong() ] ).collect {
            Util.ncItalico( it.nmCientifico, true, true, it.coNivelTaxonomico) + ( it.nuVersao ? '&nbsp;V.' + it.nuVersao : '' )
        }.join( ', ' )
    }


    def getGridPapelParticipanteFicha() {
        if( !params.sqOficina ) {
            render ''
            return
        }
        Oficina oficina = Oficina.get( params.sqOficina.toInteger() )
        boolean canModify = oficina.canModify()
        boolean hasDocFinal = oficina.hasDocFinal()

        Map mapColaboradoresAmpla = [ : ]
        Map mapColaboradoresDireta = [ : ]
        DadosApoio consultaDireta = dadosApoioService.getByCodigo( 'TB_TIPO_CONSULTA_FICHA', 'CONSULTA_DIRETA' )
        DadosApoio consultaAmpla = dadosApoioService.getByCodigo( 'TB_TIPO_CONSULTA_FICHA', 'CONSULTA_AMPLA' )
        //List listOficinaParticipantes = OficinaParticipante.findAllByOficina( oficina, [sort:"pessoa.noPessoa"] )

        /*String sql = """select new map(b.id as id
                       , b.pessoa.noPessoa as noPessoa
                       , a.papel.descricao as dsPapel
                       , a.papel.id as sqPapel
                       , count( a.oficinaFicha.ficha) as nuFicha)
        from OficinaFichaParticipante a
        join a.oficinaParticipante b
        where b.oficina.id = :sqOficina
        group by b.id, a.papel.id, b.pessoa.noPessoa, a.papel.descricao
        order by b.pessoa.noPessoa, a.papel.descricao """
        List listOficinaParticipantes = OficinaFichaParticipante.executeQuery(sql,[sqOficina:oficina.id])
*/
        Map qryParams = [ sqOficina: oficina.id ]
        String sql = """select participante.sq_oficina_participante as "id",
                participante.no_pessoa as "noPessoa",
                ofp.sq_papel_participante as "sqPapel",
                papel.ds_dados_apoio as "dsPapel",
                participante.dt_email_assinatura as "dtEmailAssinatura",
                participante.dt_assinatura as "dtAssinatura",
                participante.de_hash_assinatura as "deHashAssinatura",
                count(*) as "nuFicha"
                from salve.oficina_ficha_participante ofp
                inner join salve.oficina_ficha oficina_ficha on oficina_ficha.sq_oficina_ficha = ofp.sq_oficina_ficha
                inner join salve.dados_apoio papel on papel.sq_dados_apoio = ofp.sq_papel_participante
                INNER JOIN salve.vw_oficina_participante participante on participante.sq_oficina_participante = ofp.sq_oficina_participante
                where oficina_ficha.sq_oficina = :sqOficina"""
        if( params.sqOficinaParticipante ) {
            sql += """\nand participante.sq_oficina_participante = :sqOficinaParticipante"""
            qryParams.sqOficinaParticipante = params.sqOficinaParticipante.toLong()
        }
        sql += """\ngroup by participante.sq_oficina_participante, ofp.sq_papel_participante, papel.ds_dados_apoio, participante.no_pessoa, participante.dt_email_assinatura, participante.dt_assinatura, participante.de_hash_assinatura
                          order by participante.no_pessoa"""

        List listOficinaParticipantes = sqlService.execSql( sql, qryParams )
        List oficinaFichas
        // quando for apenas a atulizaçao de 1 linha do gride não precisa dados dos demais participante das consultas amplas e diretas
        if( !params.sqOficinaParticipante ) {
            oficinaFichas = OficinaFicha.findAllByOficina( oficina ).vwFicha
            if( oficinaFichas ) {
                // colaborações da consulta Ampla para exibir na parte de baixo do gride alem dos participantes selecionados na oficina
                FichaColaboracao.createCriteria().list {
                    consultaFicha {
                        cicloConsulta {
                            eq( 'cicloAvaliacao', oficina.cicloAvaliacao )
                            eq( 'tipoConsulta', consultaAmpla )
                        }
                        //if( oficinaFichas.size() > 0 )
                        'in'( 'vwFicha', oficinaFichas )
                    }
                    'in'( 'situacao', [ dadosApoioService.getByCodigo( 'TB_SITUACAO_COLABORACAO', 'ACEITA'),
                                        dadosApoioService.getByCodigo( 'TB_SITUACAO_COLABORACAO', 'PARCIAL')
                                      ])
                }.each {
                    String noUsuario = Util.capitalize( it.usuario.noUsuario )
                    String noCientifico = Util.ncItalico( it.consultaFicha.vwFicha.nmCientifico )
                    if( !mapColaboradoresAmpla[ noUsuario ] ) {
                        mapColaboradoresAmpla[ noUsuario ] = [ ]
                    }
                    if( mapColaboradoresAmpla[ noUsuario ].indexOf( noCientifico ) == -1 ) {
                        mapColaboradoresAmpla[ noUsuario ].push( noCientifico )
                    }
                }
            }

            // colaborações da consulta direta
            if( oficinaFichas ) {
                FichaColaboracao.createCriteria().list {
                    consultaFicha {
                        cicloConsulta {
                            eq( 'cicloAvaliacao', oficina.cicloAvaliacao )
                            eq( 'tipoConsulta', consultaDireta )
                        }
                        //if( oficinaFichas)
                        'in'( 'vwFicha', oficinaFichas )
                    }
                    and {
                        or {
                            eq( 'situacao', dadosApoioService.getByCodigo( 'TB_SITUACAO_COLABORACAO', 'ACEITA' ) )
                            eq( 'situacao', dadosApoioService.getByCodigo( 'TB_SITUACAO_COLABORACAO', 'PARCIAL' ) )
                        }
                    }
                }.each {
                    String noUsuario = Util.capitalize( it.usuario.noUsuario )

                    String noCientifico = Util.ncItalico( it.consultaFicha.vwFicha.nmCientifico )
                    if( !mapColaboradoresAmpla[ noUsuario ] ) {
                        mapColaboradoresAmpla[ noUsuario ] = [ ]
                        mapColaboradoresAmpla[ noUsuario ] = [ ]
                    }
                    if( mapColaboradoresAmpla[ noUsuario ].indexOf( noCientifico ) == -1 ) {
                        mapColaboradoresAmpla[ noUsuario ].push( noCientifico )
                    }

                    /*if( !mapColaboradoresDireta[noUsuario])
            {
                mapColaboradoresDireta[noUsuario] = []
                mapColaboradoresAmpla[noUsuario] = []
            }
            if( mapColaboradoresDireta[noUsuario].indexOf( it.consultaFicha.ficha.noCientificoItalico) == -1 )
            {
                mapColaboradoresDireta[noUsuario].push(it.consultaFicha.ficha.noCientificoItalico)
            }
            */
                }
            }
            mapColaboradoresAmpla = mapColaboradoresAmpla.sort()
        }
        render( template: 'divGridPapelParticipanteFicha', model: [ listOficinaParticipantes: listOficinaParticipantes
                                                                    , mapColaboradoresAmpla : mapColaboradoresAmpla
                                                                    , mapColaboradoresDireta: mapColaboradoresDireta
                                                                    , oficina               : oficina
                                                                    , hasDocFinal           : hasDocFinal
                                                                    , canModify             : canModify ] )
    }

    def saveEmailParticipante() {

        Map res = [ status: 0, msg: '', type: 'success', errors: [ ] ]
        try {
            if( !params.deEmail ) {
                throw new Exception( 'Email deve ser informado!' )
            }

            if( !params.sqOficinaParticipante ) {
                throw new Exception( 'Id do participante deve ser informado!' )
            }
            OficinaParticipante op = OficinaParticipante.get( params.sqOficinaParticipante.toLong() )
            if( op ) {
                op.deEmail = params.deEmail
                op.save( flush: true )
                res.msg = 'Email alterado com SUCESSO!'
            }
            else {
                throw new Exception( 'Id inválido!' )
            }
        }
        catch( Exception e ) {
            res.status = 1
            res.msg = e.getMessage()
        }
        render res as JSON
    }

    // gride com as fichas da oficina
    def getGridFichasPapel() {
        if( !params.sqOficina ) {
            render ''
            return
        }
        Oficina oficina = Oficina.get( params.sqOficina.toInteger() )
        boolean canModify = oficina.canModify()

        /*List listFichas = OficinaFicha.createCriteria().list {
            eq('oficina',oficina )
            if( params.dsAgrupamentoFiltro )
            {
                ilike('dsAgrupamento',"%"+params.dsAgrupamentoFiltro+"%" )
            }
        }.sort { it.vwFicha.nmCientifico }
        */
        // criar o parametro de filtro pela oficina para o metodo search
        params.sqOficinaFiltro = params.sqOficina

        // para utilizar o metodo search tem que passar o ciclo de avaliaçao
        params.sqCicloAvaliacao = oficina.cicloAvaliacao.id

        // adicionar a coluna ds_agrupamento no resultado do sql
        params.colAgrupamento = true
        List listFichas = fichaService.search( params )

        render( template: 'divGridPapelSelFicha', model: [ listFichas : listFichas
                                                           , canModify: canModify ] )
    }

    def savePapel() {
        Map res = [ status: 0, msg: '', type: 'success', errors: [ ] ]
        try {
            List idsOficinaParticipantes = params.idsOficinaParticipantes.split( ',' )
            List idsFichas = []
            Oficina oficina = Oficina.get( params.sqOficina.toLong() )
            if( !oficina ) {
                res.msg = 'Oficina inválida!'
                res.type = 'error'
                render res as JSON
                return
            }
            if( oficina.tipoOficina.codigoSistema != 'OFICINA_VALIDACAO' ) {
                if( !params.idsFichas ) {
                    res.msg = 'Nenhuma ficha selecionada!'
                    res.type = 'error'
                    render res as JSON
                    return
                }
            }
            else {
                // TODO - corrigir modelo. Criar tabelas oficina_participante_papel e oficina_participante_papel_ficha
                // selecionar uma ficha da tabela oficina_ficha para manter a integridade, ja que a oficina de validação não precisa informar ficha
                OficinaFicha oficinaFicha = OficinaFicha.findByOficina( oficina )
                params.idsFichas = oficinaFicha.vwFicha.id.toString()
            }

            if( params.idsFichas ) {
                params.idsFichas.split( ',' ).each {
                    idsFichas.push( it.toInteger() )
                }
            }

            OficinaParticipante oficinaParticipante

            // papel
            DadosApoio papel = DadosApoio.get( params.sqPapelParticipante.toInteger() )

            // loop participantes selecionados
            idsOficinaParticipantes.each { idOficinaParticipante ->

                // oficina participante
                oficinaParticipante = OficinaParticipante.get( idOficinaParticipante )
                List novos = params.idsFichas.split(',').collect{ it.toInteger() }
                if( oficinaParticipante ) {
                    // ler as fichas atuais do participante e excluir as fichas que ele possuir e que não foram selecionadas no gride
                    OficinaFichaParticipante.findAllByOficinaParticipanteAndPapel( oficinaParticipante, papel ).each { oficinaFichaParticipante ->
                        if( !idsFichas.find { sqFicha ->
                            return sqFicha.toInteger() == oficinaFichaParticipante?.oficinaFicha?.ficha?.id.toInteger()
                        } ) {
                            oficinaFichaParticipante.delete( flush: true )
                        }
                        else {
                            novos.remove( novos.indexOf( oficinaFichaParticipante.oficinaFicha.ficha.id.toInteger() ) )
                        }
                    }
                    // adicionar fichas restantes ao participante
                    novos.each {
                        OficinaFicha oficinaFicha = OficinaFicha.findByOficinaAndVwFicha( oficina, VwFicha.get( it ) )
                        new OficinaFichaParticipante( oficinaFicha: oficinaFicha
                            , oficinaParticipante: oficinaParticipante
                            , papel: papel ).save( flush: true )
                    }
                }
            }
            res.status = 0
            res.msg = getMsg( 1 )
            res.type = 'success'
            res.erros = [ ]
        }
        catch( Exception e ) {
            println e.getMessage()
            res.status = 1
            res.msg = 'Erro Gravação!'
        }
        render res as JSON
    }

    def editPapel() {
        if( !params.sqPapelParticipante || !params.id ) {
            render '{}'
            return
        }
        DadosApoio papel = DadosApoio.get( params.sqPapelParticipante.toInteger() )
        Map data = [ sqOficinaParticipante: params.id, sqPapelParticipante: params.sqPapelParticipante, sqFicha: [ ] ]
        OficinaFichaParticipante.findAllByOficinaParticipanteAndPapel( OficinaParticipante.get( params.id.toInteger() ), papel ).each {
            data.sqFicha.push( it.oficinaFicha.vwFicha.id )
        }
        render data as JSON
    }

    def deletePapel() {
        if( !params.sqPapelParticipante || !params.id ) {
            render ''
            return
        }

        DadosApoio papel = DadosApoio.get( params.sqPapelParticipante.toInteger() )
        OficinaFichaParticipante.findAllByOficinaParticipanteAndPapel( OficinaParticipante.get( params.id.toInteger() ), papel ).each {
            it.delete( flush: true )
        }
        render ''

    }

    // ABA EXECUCAO
    // -------------------------------------------------------------------------------

    def getFormExecucao() {
        // dados para o formulário de filtro das fichas
        List listNivelTaxonomico = NivelTaxonomico.list()
        List listSituacaoFicha = fichaService.getListSituacao()
        DadosApoio invertebradosTerrestres = DadosApoioService.getByCodigo( 'TB_GRUPO', 'INVERTEBRADOS_TERRESTRES' )

        // verificar se tem ordem insecta e exibir o botão Avaliação Expedita
        boolean avaliacaoExpedita = false
        Oficina oficina = Oficina.get( params.sqOficinaSelecionada.toInteger() )
        boolean canModify = oficina.canModify()
        int qtd = OficinaFicha.createCriteria().count {
            eq( 'oficina', oficina )
            vwFicha {
                eq( 'sqGrupoSalve', invertebradosTerrestres.id.toInteger() )
            }
        }
        avaliacaoExpedita = ( qtd > 0 )
        OficinaFicha.findAllByOficina( oficina ).each {
            if( !avaliacaoExpedita ) {
                if( it?.vwFicha?.sqGrupo?.toInteger() == invertebradosTerrestres.id.toInteger() ) {
                    avaliacaoExpedita = true
                }
            }
        }
        render( template: 'formExecucao', model: [ oficina              : oficina
                                                   , listNivelTaxonomico: listNivelTaxonomico
                                                   , listSituacaoFicha  : listSituacaoFicha
                                                   , avaliacaoExpedita  : avaliacaoExpedita
                                                   , canModify          : canModify ] )
    }

    def getGridExecucaoTotais() {

        if( !params.sqOficina ) {
            if( params.relatorio ) {
                return [ : ]
            }
            render 'Oficina não selecionada!'
            return
        }

        DadosApoio categoria
        Oficina oficina = Oficina.get( params.sqOficina.toInteger() )
        DadosApoio situacaoAvaliada = dadosApoioService.getByCodigo( 'TB_SITUACAO_FICHA', 'POS_OFICINA' )
        DadosApoio situacaoValidada = dadosApoioService.getByCodigo( 'TB_SITUACAO_FICHA', 'VALIDADA' )
        //DadosApoio situacaoTransferida      = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'TRANSFERIDA')
        DadosApoio situacaoCompilacao = dadosApoioService.getByCodigo( 'TB_SITUACAO_FICHA', 'COMPILACAO' )
        DadosApoio situacaoExcluida = dadosApoioService.getByCodigo( 'TB_SITUACAO_FICHA', 'EXCLUIDA' )
        DadosApoio situacaoAvaliadaRevisada = dadosApoioService.getByCodigo( 'TB_SITUACAO_FICHA', 'AVALIADA_REVISADA' )

        if( !( situacaoAvaliada && situacaoValidada && situacaoCompilacao && situacaoExcluida && situacaoAvaliadaRevisada ) ) {
            render ''
            return
        }


        Map data = [ qtdLc: 0i, qtdNaoLc: 0i, qtdAvaliada: 0i, qtdNaoAvaliada: 0i, qtdTransferida: 0i, qtdExcluida: 0i, qtdCategoria: [ : ] ]
        int totalGeralLc = 0i
        boolean contabilizarCategoria = false

        // adicionar a categoria no retorno da consulta
        params.colCategoria = true
        params.sqCicloAvaliacao = oficina.cicloAvaliacao.id.toInteger()

        // Map dadosUltimaAvaliacao
        oficinaService.getFichas( params ).sort {
            it?.vwFicha?.categoriaIucn?.ordem
        }.each {
            if( it?.situacaoFicha ) {
                categoria = null
                if( it?.situacaoFicha == situacaoCompilacao ) {
                    data.qtdTransferida++
                }
                else
                    if( it?.situacaoFicha == situacaoExcluida ) {
                        data.qtdExcluida++
                    }
                    else {
                        if( it?.situacaoFicha == situacaoAvaliada || it?.situacaoFicha == situacaoValidada || it?.situacaoFicha == situacaoAvaliadaRevisada ) {
                            // primeiro le a categoria definida na oficina e se não houver ler a da ficha
                            if( it?.categoriaIucn ) {
                                categoria = it.categoriaIucn
                            } else if( it?.vwFicha?.sqCategoriaIucn ) {
                                categoria = it.vwFicha.categoriaIucn
                            }

                            if( categoria ) {
                                if( categoria.codigoSistema == 'LC' ) {
                                    if( it?.situacaoFicha == situacaoAvaliada || it?.situacaoFicha == situacaoAvaliadaRevisada )
                                    //if( it.vwFicha.codigoCategoriaUltimaAvaliacaoNacionalText == 'LC' )
                                    {
                                        if( it.vwFicha.stManterLc == true ) {
                                            data.qtdLc++ // manteve a categoria LC
                                        }
                                        else {
                                            data.qtdAvaliada++
                                        }
                                    }
                                    else
                                        if( it?.situacaoFicha == situacaoValidada ) {
                                            data.qtdAvaliada++
                                        }
                                        else {
                                            data.qtdNaoAvaliada++
                                        }
                                }
                                else {
                                    data.qtdAvaliada++
                                }
                            }
                        }
                        else {
                            data.qtdNaoAvaliada++
                        }
                    }

                if( categoria ) {
                    totalGeralLc++
                    if( data.qtdCategoria[ categoria.descricaoCompleta ] ) {
                        data.qtdCategoria[ categoria.descricaoCompleta ]++
                    }
                    else {
                        data.qtdCategoria[ categoria.descricaoCompleta ] = 1i
                    }
                }
            }
        }

        // adicionar total transferidas e excluidas no gride de total da direta

        if( data.qtdTransferida > 0 ) {
            data.qtdCategoria[ 'Transferidas' ] = data.qtdTransferida
        }
        if( data.qtdExcluida > 0 ) {
            data.qtdCategoria[ 'Excluídas' ] = data.qtdExcluida
        }
        data.qtdCategoria[ 'Total' ] = totalGeralLc + data.qtdTransferida + data.qtdExcluida
        if( params?.relatorio ) {
            return data
        }
        render( template: "divGridExeTotais", model: [ data: data ] )
    }


    def getGridExecucaoFichaLc() {
        Map sqlParams = [:]
        String cmdSql
        List sqlWhere = []
        List listFichas
        Integer contadorLinha = 1

        if( params.sqOficinaSelecionada ) {
            params.sqOficina = params.sqOficinaSelecionada
        }

        if( !params.sqOficina ) {
            render 'Oficina não selecionada!'
            return
        }

        DadosApoio categoriaLc = DadosApoioService.getByCodigo( 'TB_CATEGORIA_IUCN', 'LC' )
        //DadosApoio situacaoAvaliada = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'POS_OFICINA')

        Oficina oficina = Oficina.get( params.sqOficina.toInteger() )
        boolean canModify = oficina.canModify()

        if( false ) {
            // adicionar a categoria no retorno da consulta
            params.colCategoria = true
            params.colOficinaFicha = true // adicionar colunas da oficina_ficha
            params.colHistorico = true // adicionar colunas da ultima avaliacao nacional da tabela ficha_historico_avaliacao
            params.sqCicloAvaliacao = oficina.cicloAvaliacao.id.toInteger()
            params.sqOficinaFiltro = params.sqOficina
            //params.colJustificativaFicha = true
            params.stManterLcFiltro = true

            // para atulizar somente a linha especifica no gride
            if (params.sqFicha) {
                params.sqFichaFiltro = params.sqFicha
            }

            // consultar banco de dados
            listFichas = fichaService.search(params)
        } else {

            // FILTROS QUE SERÃO FEITOS PELA FN_FICHAS_SELECT()
            Map filtrosFicha = ['sq_ciclo_avaliacao':oficina.cicloAvaliacao.id,'st_manter_lc':true]

            // FILTRO PELA UNIDADE OU PELO PERFIL
            if(params.sqUnidadeFiltro){
                filtrosFicha.sq_unidade_org = params.sqUnidadeFiltro.toLong()
            } else {
                // filtrar somente as fichas que o usuário tem acesso
                // Se for Coordenador de taxon ou Colaborador Externo ler as fichas pela função
                // quaquer outro perfil que não seja o Administrador ou Consulta tem que ter a unidade
                if (session.sicae.user.isCT() || session.sicae.user.isCE()) {
                    filtrosFicha.sq_pessoa = session.sicae.user.sqPessoa
                } else {
                    if ( ! session.sicae.user.isADM() && ! session.sicae.user.isCO() ) {
                        filtrosFicha.sq_unidade_org = session.sicae.user.sqUnidadeOrg ? session.sicae.user.sqUnidadeOrg.toLong() : -1;
                    }
                }
            }

            // FILTRO PELO NOME CIENTIFICO
            if( params.nmCientificoFiltro ) {
                filtrosFicha.nm_cientifico = params.nmCientificoFiltro.toString()
            }

            // FILTRO PELO NOME COMUM
            if( params.nmComumFiltro ) {
                filtrosFicha.no_comum = params.nmComumFiltro.toString()
            }

            // FILTRO PELO NIVEL TAXONOMICO
            if( params.nivelFiltro && params.sqTaxonFiltro){
                filtrosFicha.sq_taxon = params.sqTaxonFiltro.toString()
                filtrosFicha.co_nivel_taxonomico = params.nivelFiltro.toString()
            }

            // FILTRO COM / SEM PENDENCIA
            if( params.inPendenciaFiltro ){
                filtrosFicha.st_com_pendencia = (params.inPendenciaFiltro.toInteger() == 1)
            }

            // FILTRO PELA SITUACAO DA FICHA
            if( params.sqSituacaoFiltro ){
                filtrosFicha.sq_situacao_ficha = params.sqSituacaoFiltro.toString()
            }

            // FILTRO PELA CATEGORIA
            if( params.sqCategoriaFiltro ){
                if( params.chkAvaliadaNoCicloFiltro == 'S' ) {
                    filtrosFicha.sq_categoria_ficha = params.sqCategoriaFiltro.toString()
                } else {
                    filtrosFicha.sq_categoria_final_ou_historico = params.sqCategoriaFiltro.toString()
                }
            }

            // FILTRO PELO GRUPO e SUBGRUPO AVALIADO
            if( params.sqGrupoFiltro ){
                filtrosFicha.sq_grupo = params.sqGrupoFiltro.toString()
                if( params.sqSubgrupoFiltro ) {
                    filtrosFicha.sq_subgrupo = params.sqSubgrupoFiltro.toString()
                }
            }

            //****************************************************************/
            // FILTROS APLICADOS NA QUERY LOCAL E NÃO PELA FN_FICHAS_SELECT() /
            //****************************************************************/

            // FILTRO PELA FICHA
            if( params.sqFicha  ) {
                sqlWhere.push('oficina_ficha.sq_ficha = :sqFicha')
                sqlParams.sqFicha = params.sqFicha.toLong()
                // passar tambem para a fn_select_ficha para melhorar a performance
                filtrosFicha.sq_ficha = params.sqFicha.toLong()
            }

            // FILTRO PELA CATEGORIA DA OFICINA DE AVALIAÇÃO
            if( params.sqOficina  ){
                sqlWhere.push('oficina_ficha.sq_oficina = :sqOficina')
                sqlParams.sqOficina = params.sqOficina.toLong()
                if( params.coCategoriaOficinaFiltro ) {
                    DadosApoio categoriaOficina = dadosApoioService.getByCodigo('TB_CATEGORIA_IUCN', params.coCategoriaOficinaFiltro.toString().toUpperCase())
                    sqlWhere.push('oficina_ficha.sq_categoria_iucn = :sqCategoriaOficina')
                    sqlParams.sqCategoriaOficina = categoriaOficina.id
                }
                if( params.dsAgrupamentoFiltro ) {
                    sqlWhere.push('oficina_ficha.ds_agrupamento ILIKE :dsAgrupamento')
                    sqlParams.dsAgrupamento = '%'+params.dsAgrupamentoFiltro.toString().replaceAll(/%/,'').trim()+'%'
                }
            }

            cmdSql = """select fs.sq_ficha
                       ,fs.nm_cientifico
                       ,ficha.st_endemica_brasil
                       ,nivel.co_nivel_taxonomico
                       ,oficina_ficha.sq_oficina_ficha
                       ,oficina.sg_oficina
                       ,situacao.ds_dados_apoio as ds_situacao_oficina_ficha
                       ,situacao.cd_sistema as cd_situacao_oficina_ficha
                       ,categoria_avaliada.cd_dados_apoio as cd_categoria_oficina_ficha
                       ,categoria_avaliada.cd_sistema as cd_categoria_sistema_oficina_ficha
                       ,case when oficina_ficha.sq_categoria_iucn is null then null else concat( categoria_avaliada.ds_dados_apoio,' (',categoria_avaliada.cd_dados_apoio,')') end as de_categoria_oficina_ficha
                       ,oficina_ficha.ds_justificativa as ds_justificativa_oficina_ficha
                       ,oficina_ficha.dt_avaliacao AS dt_avaliacao_oficina_ficha
                       -- dados do historico
                       ,taxon_historico.sq_taxon_historico_avaliacao
                       ,taxon_historico.cd_categoria_iucn_nacional
                       ,taxon_historico.ds_categoria_iucn_nacional
                       ,taxon_historico.tx_justificativa_aval_nacional
                       -- versao
                       ,ficha_versao.sq_ficha_versao
                       ,ficha_versao.nu_versao
                       from salve.oficina_ficha
                      inner join salve.fn_ficha_select('${filtrosFicha as JSON}') fs on fs.sq_ficha = oficina_ficha.sq_ficha
                      inner join salve.oficina on oficina.sq_oficina = oficina_ficha.sq_oficina
                      inner join salve.ficha on ficha.sq_ficha = oficina_ficha.sq_ficha
                      left outer join salve.dados_apoio as categoria_avaliada on categoria_avaliada.sq_dados_apoio = oficina_ficha.sq_categoria_iucn
                      left outer join salve.dados_apoio as situacao on situacao.sq_dados_apoio = oficina_ficha.sq_situacao_ficha
                      inner join taxonomia.taxon on taxon.sq_taxon = fs.sq_taxon
                      inner join taxonomia.nivel_taxonomico as nivel on nivel.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
                      -- versao
                      left join salve.oficina_ficha_versao on oficina_ficha_versao.sq_oficina_ficha = oficina_ficha.sq_oficina_ficha
                      left join salve.ficha_versao on ficha_versao.sq_ficha_versao = oficina_ficha_versao.sq_ficha_versao

                      -- ler os dados do historico
                      left join lateral (
                            SELECT th.sq_taxon_historico_avaliacao,
                            categoria_th.cd_dados_apoio as cd_categoria_iucn_nacional,
                            case when th.sq_categoria_iucn is null then null else concat( categoria_th.ds_dados_apoio,' (',categoria_th.cd_dados_apoio,')') end as ds_categoria_iucn_nacional,
                            th.tx_justificativa_avaliacao as tx_justificativa_aval_nacional
                            FROM salve.taxon_historico_avaliacao th
                            INNER JOIN salve.dados_apoio AS tipo_th ON tipo_th.sq_dados_apoio = th.sq_tipo_avaliacao
                            INNER JOIN salve.dados_apoio AS categoria_th ON categoria_th.sq_dados_apoio = th.sq_categoria_iucn
                            WHERE th.sq_taxon = fs.sq_taxon
                            AND tipo_th.cd_sistema = 'NACIONAL_BRASIL'
                            ORDER BY th.nu_ano_avaliacao DESC LIMIT 1
                      ) as taxon_historico on true
                      where fs.st_manter_lc = true
                      ${sqlWhere ? 'AND ' + sqlWhere.join(' AND ') : '' }
                      """
             /** /
             println ' '
             println ' '
             println cmdSql
             println sqlParams
             /**/
            listFichas = sqlService.execSql(cmdSql, sqlParams).sort{it.nm_cientifico}


        }

        // simular uma paginação para somente exibir no cabecalho o total de registros
        Map pagination = [pageSize:100000000, gridId:'GridExeFichaLc', pageNumber:1, offset:0, totalRecords:listFichas.size(), totalPages:1]


        render( template: "divGridExeFichasLc", model: [ gridId:'GridExeFichaLc', pagination: pagination, listFichas: listFichas, canModify: canModify, categoriaLc: categoriaLc, contadorLinha: contadorLinha ] )
    }

    def getGridExecucaoFichaNaoLc() {
        Map sqlParams = [:]
        String cmdSql
        List sqlWhere = []
        List listFichas = []
        Integer contadorLinha = 1

        if( !params.sqOficina ) {
            render 'Oficina não selecionada!'
            return
        }

        if( params.sqFicha ) {
            params.sqFichaFiltro = params.sqFicha
        }

        if( params.sqOficinaSelecionada ) {
            params.sqOficina = params.sqOficinaSelecionada
        }

        // DadosApoio situacaoAvaliada = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'POS_OFICINA')
        // DadosApoio situacaoValidada = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'VALIDADA')
        DadosApoio categoriaLc = DadosApoioService.getByCodigo( 'TB_CATEGORIA_IUCN', 'LC' )
        Oficina oficina = Oficina.get( params.sqOficina.toInteger() )
        boolean canModify = oficina.canModify()


        // metodo antigo utilizando fichaService.search()
        if( params.dsAgrupamentoFiltro && false) {

            params.colCategoria = true
            params.colOficinaFicha = true
            params.sqCicloAvaliacao = oficina.cicloAvaliacao.id.toInteger()
            params.sqOficinaFiltro = params.sqOficina
            //params.colJustificativaFicha=true // retornar a coluna ds_justificativa da ficha
            listFichas = fichaService.search(params)

            // Excluir as fichas marcadas como MANTER_LC
            listFichas = listFichas.findAll {
                return (it.st_manter_lc != true)
            }
        } else  {

            // metodo novo utilizando salve.fn_fichas_select()

            // FILTROS QUE SERÃO FEITOS PELA FN_FICHAS_SELECT()
            Map filtrosFicha = ['sq_ciclo_avaliacao':oficina.cicloAvaliacao.id,'st_manter_lc':false]

            // FILTRO PELA UNIDADE OU PELO PERFIL
            if(params.sqUnidadeFiltro){
                filtrosFicha.sq_unidade_org = params.sqUnidadeFiltro.toLong()
            } else {
                // filtrar somente as fichas que o usuário tem acesso
                // Se for Coordenador de taxon ou Colaborador Externo ler as fichas pela função
                // quaquer outro perfil que não seja o Administrador ou Consulta tem que ter a unidade
                if (session.sicae.user.isCT() || session.sicae.user.isCE()) {
                    filtrosFicha.sq_pessoa = session.sicae.user.sqPessoa
                } else {
                    if ( ! session.sicae.user.isADM() && ! session.sicae.user.isCO() ) {
                        filtrosFicha.sq_unidade_org = session.sicae.user.sqUnidadeOrg ? session.sicae.user.sqUnidadeOrg.toLong() : -1;
                    }
                }
            }

            // FILTRO PELO NOME CIENTIFICO
            if( params.nmCientificoFiltro ) {
                filtrosFicha.nm_cientifico = params.nmCientificoFiltro.toString()
            }

            // FILTRO PELO NOME COMUM
            if( params.nmComumFiltro ) {
                filtrosFicha.no_comum = params.nmComumFiltro.toString()
            }

            // FILTRO PELO NIVEL TAXONOMICO
            if( params.nivelFiltro && params.sqTaxonFiltro){
                filtrosFicha.sq_taxon = params.sqTaxonFiltro.toString()
                filtrosFicha.co_nivel_taxonomico = params.nivelFiltro.toString()
            }

            // FILTRO COM / SEM PENDENCIA
            if( params.inPendenciaFiltro ){
                filtrosFicha.st_com_pendencia = (params.inPendenciaFiltro.toInteger() == 1)
            }

            // FILTRO PELA SITUACAO DA FICHA
            if( params.sqSituacaoFiltro ){
                filtrosFicha.sq_situacao_ficha = params.sqSituacaoFiltro.toString()
            }

            // FILTRO PELA CATEGORIA
            if( params.sqCategoriaFiltro ){
                if( params.chkAvaliadaNoCicloFiltro == 'S' ) {
                    filtrosFicha.sq_categoria_ficha = params.sqCategoriaFiltro.toString()
                } else {
                    filtrosFicha.sq_categoria_final_ou_historico = params.sqCategoriaFiltro.toString()
                }
            }

            // FILTRO PELO GRUPO e SUBGRUPO AVALIADO
            if( params.sqGrupoFiltro ){
                filtrosFicha.sq_grupo = params.sqGrupoFiltro.toString()
                if( params.sqSubgrupoFiltro ) {
                    filtrosFicha.sq_subgrupo = params.sqSubgrupoFiltro.toString()
                }
            }

            //****************************************************************/
            // FILTROS APLICADOS NA QUERY LOCAL E NÃO PELA FN_FICHAS_SELECT() /
            //****************************************************************/

            // FILTRO PELA FICHA
            if( params.sqFicha  ) {
                sqlWhere.push('oficina_ficha.sq_ficha = :sqFicha')
                sqlParams.sqFicha = params.sqFicha.toLong()
                // passar tambem para a fn_select_ficha para melhorar a performance
                filtrosFicha.sq_ficha = params.sqFicha.toLong()
            }

                // FILTRO PELA CATEGORIA DA OFICINA DE AVALIAÇÃO
            if( params.sqOficina  ){
                sqlWhere.push('oficina_ficha.sq_oficina = :sqOficina')
                sqlParams.sqOficina = params.sqOficina.toLong()
                if( params.coCategoriaOficinaFiltro ) {
                    DadosApoio categoriaOficina = dadosApoioService.getByCodigo('TB_CATEGORIA_IUCN', params.coCategoriaOficinaFiltro.toString().toUpperCase())
                    sqlWhere.push('oficina_ficha.sq_categoria_iucn = :sqCategoriaOficina')
                    sqlParams.sqCategoriaOficina = categoriaOficina.id
                }
                if( params.dsAgrupamentoFiltro ) {
                    sqlWhere.push('oficina_ficha.ds_agrupamento ILIKE :dsAgrupamento')
                    sqlParams.dsAgrupamento = '%'+params.dsAgrupamentoFiltro.toString().replaceAll(/%/,'').trim()+'%'
                }
            }

            cmdSql = """select fs.sq_ficha
                    ,fs.nm_cientifico
                    ,fs.st_transferida
                    ,nivel.co_nivel_taxonomico
                    ,oficina_ficha.sq_oficina_ficha
                    ,oficina.sg_oficina
                    ,situacao.ds_dados_apoio as ds_situacao_oficina_ficha
                    ,situacao.cd_sistema as cd_situacao_oficina_ficha
                    ,categoria_avaliada.cd_dados_apoio as cd_categoria_oficina_ficha
                    ,categoria_avaliada.cd_sistema as cd_categoria_sistema_oficina_ficha
                    ,oficina_ficha.dt_avaliacao AS dt_avaliacao_oficina_ficha
                    ,case when oficina_ficha.sq_categoria_iucn is null then null else concat( categoria_avaliada.ds_dados_apoio,' (',categoria_avaliada.cd_dados_apoio,')') end as de_categoria_oficina_ficha
                    ,oficina_ficha.ds_criterio_aval_iucn as ds_criterio_iucn_oficina_ficha
                    ,oficina_ficha.ds_justificativa as ds_justificativa_oficina_ficha
                    ,oficina_ficha.st_possivelmente_extinta
                    ,oficina_ficha.st_transferida as st_transferida_oficina_ficha
                    ,oficina_ficha.ds_agrupamento as ds_agrupamento_oficina_ficha
                    -- versao
                    ,ficha_versao.sq_ficha_versao
                    ,ficha_versao.nu_versao

                    from salve.oficina_ficha
                    inner join salve.fn_ficha_select('${filtrosFicha as JSON}') fs on fs.sq_ficha = oficina_ficha.sq_ficha
                    inner join salve.oficina on oficina.sq_oficina = oficina_ficha.sq_oficina
                    left outer join salve.dados_apoio as categoria_avaliada on categoria_avaliada.sq_dados_apoio = oficina_ficha.sq_categoria_iucn
                    left outer join salve.dados_apoio as situacao on situacao.sq_dados_apoio = oficina_ficha.sq_situacao_ficha
                    inner join taxonomia.taxon on taxon.sq_taxon = fs.sq_taxon
                    inner join taxonomia.nivel_taxonomico as nivel on nivel.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
                    -- versao
                    left join salve.oficina_ficha_versao on oficina_ficha_versao.sq_oficina_ficha = oficina_ficha.sq_oficina_ficha
                    left join salve.ficha_versao on ficha_versao.sq_ficha_versao = oficina_ficha_versao.sq_ficha_versao

                    where (fs.st_manter_lc is null or fs.st_manter_lc = false)
                    ${sqlWhere ? 'AND ' + sqlWhere.join(' AND ') : '' }
                    """
             /** /
             println ' '
             println ' '
             println cmdSql
             println sqlParams
             /**/
            listFichas = sqlService.execSql(cmdSql, sqlParams).sort{it.nm_cientifico}

        }

        // simular uma paginação para somente exibir no cabecalho o total de registros
        Map pagination = [  pageSize:100000000,
                            gridId:'GridExeFichaNaoLc',
                            pageNumber:1,
                            offset:0,
                            totalRecords:listFichas.size(),
                            totalPages:1]

        render( template: "divGridExeFichasNaoLc",
            model: [gridId:'GridExeFichaNaoLc',
                    pagination: pagination,
                    listFichas: listFichas,
                    canModify: canModify,
                    categoriaLc: categoriaLc,
                    contadorLinha: contadorLinha] )

        /* Metodo antigo
        render( template: "divGridExeFichasNaoLc", model: [
            pagination : [:],
            listFichas: listFichas,
            canModify: canModify,
            categoriaLc: categoriaLc, contadorLinha: contadorLinha ] )
        */

    }

    // conformar/desconfirmar LC
    def confirmarLc() {
        Map res = [ status: 1, msg: '', type: 'error' ]
        DadosApoio novaSituacao
        DadosApoio situacaoConsolidada
        DadosApoio categoriaLC = dadosApoioService.getByCodigo( 'TB_CATEGORIA_IUCN', 'LC' )
        DadosApoio categoriaNE = dadosApoioService.getByCodigo( 'TB_CATEGORIA_IUCN', 'NE' )
        OficinaFicha oficinaFicha = OficinaFicha.get( params?.sqOficinaFicha?.toLong() )
        if( oficinaFicha ) {
            // Alterar a situação da ficha para Avaliada ou voltar para Consolidada
            novaSituacao = dadosApoioService.getByCodigo( 'TB_SITUACAO_FICHA', 'POS_OFICINA' ) // avaliada
            situacaoConsolidada = dadosApoioService.getByCodigo( 'TB_SITUACAO_FICHA', 'CONSOLIDADA' )
            if( !novaSituacao ) {
                res.msg = 'Código VALIDADA não cadastrado na TB_SITUACAO_FICHA.'
            }
            else
                if( !situacaoConsolidada ) {
                    res.msg = 'Código CONSOLIDADA não cadastrado na TB_SITUACAO_FICHA.'
                }
                else {
                    // se for confirmada a ação do botão vai passar para nao_confirmar
                    // e a nova situação para consolidada
                    if( params.situacao == 'confirmada' ) {
                        novaSituacao = situacaoConsolidada
                    }

// println ' '
// println 'NOVA SITUACAO  ' +  novaSituacao.codigoSistema

                    // se a situação for avaliada (POS_OFICINA) a ficha não pode ter pendencia nem item para resolver em oficina
                    if( novaSituacao.codigoSistema == 'POS_OFICINA' /*AVALIADA*/ ) {
                        List erros = fichaService.canModifySituacao( oficinaFicha.ficha, novaSituacao )
                        if( erros ) {
                            res.msg = erros.join( '<br>' )
                            render res as JSON
                            return
                        }
                        oficinaFicha.situacaoFicha = novaSituacao
                        oficinaFicha.dtAvaliacao = new Date()

                        if( !oficinaFicha.dsJustificativa ) // não sobrescrever a justificativa se já tiver alterado a justificativa original
                        {
                            // ler a justificativa, criterio etc primeiro do historico de avaliação senão encontrar
                            // ler a que tiver na aba 11.6 da ficha
                            Map dadosUltimaAvaliacaoNacional = oficinaFicha.ficha.ultimaAvaliacaoNacionalHist;
                            if( dadosUltimaAvaliacaoNacional.txJustificativaAvaliacao ) {
                                oficinaFicha.dsJustificativa = dadosUltimaAvaliacaoNacional.txJustificativaAvaliacao ?: null
                                oficinaFicha.categoriaIucn = DadosApoio.get( dadosUltimaAvaliacaoNacional.sqCategoriaIucn.toLong() )
                                oficinaFicha.dsCriterioAvalIucn = dadosUltimaAvaliacaoNacional.deCriterioAvaliacaoIucn ?: null
                                oficinaFicha.stPossivelmenteExtinta = dadosUltimaAvaliacaoNacional.stPossivelmenteExtinta ?: null
                            }
                            else {
                                if( oficinaFicha.ficha.categoriaIucn.codigoSistema != 'NE' ) {
                                    oficinaFicha.dsJustificativa = oficinaFicha.ficha.dsJustificativa
                                    oficinaFicha.categoriaIucn = oficinaFicha.ficha.categoriaIucn
                                    oficinaFicha.dsCriterioAvalIucn = oficinaFicha.ficha.dsCriterioAvalIucn
                                    oficinaFicha.stPossivelmenteExtinta = oficinaFicha.ficha.stPossivelmenteExtinta
                                }
                            }
                        }

                        if( params.situacao == 'nao_confirmada' ) {
                                oficinaFicha.categoriaIucn = categoriaLC
                         } else {
                            oficinaFicha.categoriaIucn = null;
                        }

                        // atualizar a aba 11.6 da ficha
                        oficinaFicha.ficha.situacaoFicha = novaSituacao
                        oficinaFicha.ficha.categoriaIucn = categoriaLC
                        oficinaFicha.ficha.dsJustificativa = oficinaFicha.dsJustificativa
                        // (params.situacao == 'nao_confirmada' ? oficinaFicha.dsJustificativa : null)
                        oficinaFicha.ficha.dsCriterioAvalIucn = oficinaFicha.dsCriterioAvalIucn
                        oficinaFicha.ficha.stPossivelmenteExtinta = oficinaFicha.stPossivelmenteExtinta
                        // se a situação atual for não_confirmada então é porque está confirmando, senão está voltando para a situalão nula
                        // este campo so ficará false se for clicado no botao avaliar para a ficha passar para o gride de baixo
                        oficinaFicha.ficha.stManterLc = true // (params.situacao == 'nao_confirmada' ? true : null)

                        // preencher ajuste regional com os valores padrão
                        if( params.preencherAjusteRegional == 'S' ) {
                            DadosApoio tipoConectividadeDesconhecida = dadosApoioService.getByCodigo('TB_CONECTIVIDADE','DESCONHECIDA')
                            DadosApoio tipoTendencia = dadosApoioService.getByCodigo('TB_TENDENCIA','DESCONHECIDA')
                            oficinaFicha.ficha.tipoConectividade            = tipoConectividadeDesconhecida
                            oficinaFicha.ficha.tendenciaImigracao           = tipoTendencia
                            oficinaFicha.ficha.stDeclinioBrPopulExterior    = 'D'
                            oficinaFicha.ficha.dsConectividadePopExterior   = 'Não foram encontradas informações para o táxon.'
                        }
                    } else {
                        // limpar resultado da ofician de avaliacao
                        oficinaFicha.situacaoFicha = situacaoConsolidada
                        oficinaFicha.dtAvaliacao = null
                        oficinaFicha.categoriaIucn = null
                        oficinaFicha.dsJustificativa = null
                        oficinaFicha.dsCriterioAvalIucn = null
                        oficinaFicha.stPossivelmenteExtinta = null

                        // limpar avaliacao da FICHA  ( aba 11.6 )
                        oficinaFicha.ficha.situacaoFicha = novaSituacao
                        oficinaFicha.ficha.categoriaIucn = categoriaNE
                        oficinaFicha.ficha.dsJustificativa = null
                        oficinaFicha.ficha.dsCriterioAvalIucn = null
                        oficinaFicha.ficha.stPossivelmenteExtinta = null
                        oficinaFicha.ficha.stManterLc = true

                        // limpar os dados do ajuste regional
                        if( params.stEndemicaBrasil == 'S' ) {
                            if( oficinaFicha.ficha.dsConectividadePopExterior == 'Não foram encontradas informações para o táxon.') {
                                oficinaFicha.ficha.tipoConectividade = null
                                oficinaFicha.ficha.tendenciaImigracao = null
                                oficinaFicha.ficha.stDeclinioBrPopulExterior = null
                                oficinaFicha.ficha.dsConectividadePopExterior = null
                            }
                        }
                    }
                    // salvar a ficha
                    oficinaFicha.ficha.save( flush: true )

                    // salvar a oficina/ficha
                    oficinaFicha.save( flush: true )
                    res.status = 0
                    res.type = 'success'
                    res.msg = getMsg( 1 )
                }
        }
        else {
            res.msg = 'Id ficha inválido'
        }
        render res as JSON
    }

    def avaliarLc() {
        Map res = [ status: 1, msg: '', type: 'error' ]
        OficinaFicha oficinaFicha = OficinaFicha.get( params?.sqOficinaFicha?.toLong() )
        if( oficinaFicha ) {
            // Alterar a situação da ficha para consolidada e limpar categoria e justificativa
            oficinaFicha.ficha.situacaoFicha = dadosApoioService.getByCodigo( 'TB_SITUACAO_FICHA', 'CONSOLIDADA' )
            oficinaFicha.ficha.categoriaIucn = dadosApoioService.getByCodigo( 'TB_CATEGORIA_IUCN', 'NE' )
            oficinaFicha.ficha.dsCriterioAvalIucn = null
            oficinaFicha.ficha.dsJustificativa = null
            oficinaFicha.ficha.categoriaFinal = null
            oficinaFicha.ficha.dsJustificativaFinal = null
            oficinaFicha.ficha.dsCriterioAvalIucnFinal = null
            oficinaFicha.ficha.stPossivelmenteExtintaFinal = null
            oficinaFicha.ficha.stManterLc = false
            oficinaFicha.ficha.save( flush: true )
            // atualizar dados da oficina
            oficinaFicha.categoriaIucn = null
            oficinaFicha.dsJustificativa = null
            oficinaFicha.dsCriterioAvalIucn = null
            oficinaFicha.dtAvaliacao = null
            oficinaFicha.stPossivelmenteExtinta = null
            oficinaFicha.situacaoFicha = oficinaFicha.ficha.situacaoFicha
            oficinaFicha.save( flush: true )
            res.status = 0
            res.msg = ''
        } else {
            res.msg = 'Id inválido '
        }
        render res as JSON
    }


    def salvarJustificativa() {
        Map res = [ status: 1, msg: '', type: 'error' ]
        if( !params.id ) {
            res.msg = 'Id inválido!'
        }
        OficinaFicha of = OficinaFicha.get( params.id.toInteger() );
        if( of ) {
            of.ficha.dsJustificativa = params.dsJustificativa
            of.ficha.save( flush: true )
            // atualizar resultado da oficina
            of.dsJustificativa = params.dsJustificativa
            of.dtAvaliacao = new Date()
            res.msg = ''
            res.status = 0
        }
        render res as JSON
    }


    def transferirRetornarFicha() {
        Map res = [ status: 1, msg: '', type: 'error' ]
        DadosApoio novaSituacao
        String codigoSistema
        OficinaFicha oficinaFicha = OficinaFicha.get( params?.sqOficinaFicha?.toLong() )
        if( oficinaFicha ) {
            // se a ficha já estiver em uma situação apos a oficina não permitir transferir/excluir/restaurar etc.
            DadosApoio situacaoAvaliadaRevisada = dadosApoioService.getByCodigo( 'TB_SITUACAO_FICHA', 'AVALIADA_REVISADA')
            if( oficinaFicha.ficha.situacaoFicha.ordem > situacaoAvaliadaRevisada.ordem ) {
                res.msg     = 'Ficha já está ' + oficinaFicha.ficha.situacaoFicha.descricao
                res.type    = 'modal'
                res.status  = 1
                render res as JSON
                return;
            }
            if( oficinaFicha?.situacaoFicha?.codigoSistema == 'COMPILACAO' ) {
                codigoSistema = 'CONSOLIDADA'
            }
            else {
                codigoSistema = 'COMPILACAO'
            }
            novaSituacao = dadosApoioService.getByCodigo( 'TB_SITUACAO_FICHA', codigoSistema )
            if( !novaSituacao ) {
                res.msg = 'Situação ' + codigoSistema + ' não cadastrado na tabela de apoio TB_SITUACAO_FICHA'
            }
            else {
                // atualizar campos da ficha
                oficinaFicha.ficha.situacaoFicha = novaSituacao
                oficinaFicha.ficha.categoriaIucn = null
                oficinaFicha.ficha.dsJustificativa = null
                oficinaFicha.ficha.dsCriterioAvalIucn = null
                oficinaFicha.ficha.stPossivelmenteExtinta = null
                oficinaFicha.ficha.stTransferida = ( codigoSistema == 'COMPILACAO' )
                oficinaFicha.ficha.save( flush: true )

                // atualizar dados gerados durante da oficina na tabela oficina_ficha
                oficinaFicha.categoriaIucn = null
                oficinaFicha.dsJustificativa = null
                oficinaFicha.dsCriterioAvalIucn = null
                oficinaFicha.stPossivelmenteExtinta = null
                oficinaFicha.dtAvaliacao = null
                oficinaFicha.stTransferida = oficinaFicha.ficha.stTransferida
                oficinaFicha.situacaoFicha = oficinaFicha.ficha.situacaoFicha
                oficinaFicha.save( flush: true )
                res.status = 0
                res.msg = ''
            }
        }
        else {
            res.msg = 'Id inválido '
        }
        render res as JSON
    }

    def excluirRestaurarFicha() {
        Map res = [ status: 1, msg: '', type: 'error' ]
        DadosApoio novaSituacao
        String codigoSistema
        String dsJustificativaExclusao = ''
        OficinaFicha oficinaFicha = OficinaFicha.get( params?.sqOficinaFicha?.toLong() )
        if( oficinaFicha ) {
            if( oficinaFicha.situacaoFicha.codigoSistema == 'EXCLUIDA' ) {
                codigoSistema = 'CONSOLIDADA'
                dsJustificativaExclusao = oficinaFicha.dsJustificativa // para poder localizar o historico na tabela ficha_hist_situacao_excluida
            }
            else {
                codigoSistema = 'EXCLUIDA'
            }

            novaSituacao = dadosApoioService.getByCodigo( 'TB_SITUACAO_FICHA', codigoSistema )
            if( !novaSituacao ) {
                res.msg = 'Situação ' + codigoSistema + ' não cadastrado na tabela de apoio TB_SITUACAO_FICHA'
            }
            else {
                // atualizar campos da ficha
                oficinaFicha.ficha.situacaoFicha = novaSituacao
                oficinaFicha.ficha.categoriaIucn = null
                oficinaFicha.ficha.dsCriterioAvalIucn = null
                oficinaFicha.ficha.dsJustificativa = ( codigoSistema == 'EXCLUIDA' ? 'Excluída por não ocorrer no Brasil' : null )
                oficinaFicha.ficha.stPossivelmenteExtinta = null
                oficinaFicha.ficha.stTransferida = false
                oficinaFicha.ficha.save( flush: true )
                // atualizar dados da oficina
                oficinaFicha.categoriaIucn = null
                oficinaFicha.dsCriterioAvalIucn = null
                oficinaFicha.dsJustificativa = oficinaFicha.ficha.dsJustificativa
                oficinaFicha.stPossivelmenteExtinta = null
                oficinaFicha.stTransferida = oficinaFicha.ficha.stTransferida
                oficinaFicha.dtAvaliacao = null
                if( oficinaFicha.dsJustificativa && codigoSistema != 'EXCLUIDA' ) {
                    oficinaFicha.dtAvaliacao = new Date()
                }
                oficinaFicha.situacaoFicha = oficinaFicha.ficha.situacaoFicha
                oficinaFicha.save( flush: true )
                res.status = 0
                res.msg = ''
                // registrar log de exclusao
                if( codigoSistema == 'EXCLUIDA' ) {
                    fichaService.gravarHistoricoSituacaoExcluida( oficinaFicha.ficha.id.toLong()
                        , oficinaFicha.ficha.dsJustificativa, session.sicae.user.sqPessoa.toLong() )
                    // versionar a ficha
                    Long sqPessoa = session.sicae.user.sqPessoa
                    fichaService.versionarFicha(oficinaFicha.ficha.id.toLong(), sqPessoa,'EXCLUSAO_OFICINA')
                    //sqlVersionar.push("select salve.fn_versionar_ficha(${it},${sqPessoa},true)::text")
                } else {
                    // excluir Versão se a ultima versao for uma versão do contexto EXCLUSAO_OFICINA
                    FichaVersao fichaVersao = FichaVersao.createCriteria().get{
                        eq('ficha',oficinaFicha.ficha )
                        maxResults(1)
                        order( 'id','desc')
                    }

                    if( fichaVersao && fichaVersao.contexto.codigoSistema =='EXCLUSAO_OFICINA'){
                        fichaVersao.delete(flush:true)
                    }

                    // apagar o registro do historico das exclusões
                    if( dsJustificativaExclusao ){
                        fichaService.excluirHistoricoSituacaoExcluida( oficinaFicha.ficha.id.toLong(), dsJustificativaExclusao )
                    }
                }
            }
        }
        else {
            res.msg = 'Id inválido '
        }
        render res as JSON
    }

    def getFormAvaliacaoExpedita() {
        DadosApoio categoriaLC = DadosApoioService.getByCodigo( 'TB_CATEGORIA_IUCN', 'LC' )
        DadosApoio categoriaDD = DadosApoioService.getByCodigo( 'TB_CATEGORIA_IUCN', 'DD' )
        DadosApoio invertebradosTerrestres = DadosApoioService.getByCodigo( 'TB_GRUPO', 'INVERTEBRADOS_TERRESTRES' )
        //List listFichas = []
        Oficina oficina = Oficina.get( params.sqOficina.toInteger() )
        boolean canModify = oficina.canModify()
        /*OficinaFicha.findAllByOficina(oficina).each {


            Map structure = it.ficha.taxon.structure
            if (structure.nmClasse.toUpperCase() == 'INSECTA')
            {
                listFichas.push(it.ficha)

            }
        }
        */
        List listFichas = OficinaFicha.createCriteria().list {
            eq( 'oficina', oficina )
            ficha {
                eq( 'grupoSalve', invertebradosTerrestres )
            }
        }.ficha
        render( template: '/oficina/formAvaliacaoExpedita', model: [ listFichas   : listFichas
                                                                     , categoriaLC: categoriaLC
                                                                     , categoriaDD: categoriaDD
                                                                     , canModify  : canModify ] )
    }

    def saveAvaliacaoExpedita() {
        Map res = [ status: 1, msg: 'Ver parametros', type: 'error' ]
        if( !params.id ) {
            res.msg = 'Id ficha não informado'
        }
        else {
            Ficha ficha = Ficha.get( params.id.toInteger() )
            if( !ficha ) {
                res.msg = 'Id ficha inválido!'
            }
            else {
                ficha.stTemRegistroAreasAmplas = null
                ficha.stPossuiAmplaDistGeografica = null
                ficha.stFrequenteInventarioEoo = null
                ficha.properties = params
                ficha.dsJustificativa = null
                ficha.dsCriterioAvalIucn = null
                if( params.sqCategoriaIucn ) {
                    ficha.categoriaIucn = DadosApoio.get( params.sqCategoriaIucn.toInteger() )
                    ficha.situacaoFicha = dadosApoioService.getByCodigo( 'TB_SITUACAO_FICHA', 'POS_OFICINA' )
                    ficha.dsJustificativa = '<span style="color:red;font-size:14px;">Avaliação expressa – falta justificativa</span>'
                }
                else {
                    ficha.categoriaIucn = dadosApoioService.getByCodigo( 'TB_CATEGORIA_IUCN', 'NE' ) // NAO AVALIADA
                    ficha.situacaoFicha = dadosApoioService.getByCodigo( 'TB_SITUACAO_FICHA', 'CONSULTA' )
                }

                ficha.save( flush: true )
                // atualizar dados da oficina
                OficinaFicha.findAllByFicha( ficha ).each {
                    it.categoriaIucn = ( ficha.categoriaIucn && ficha?.categoriaIucn?.codigoSistema != 'NE' ? ficha.categoriaIucn : null )
                    it.dsJustificativa = ficha.dsJustificativa
                    it.dsCriterioAvalIucn = ficha.dsCriterioAvalIucn
                    it.dtAvaliacao = new Date()
                    it.save( flush: true )
                }

                // limpar ameaça se houver
                FichaAmeaca.findAllByFicha( ficha ).each {
                    it.delete( flush: true );
                }
                res.status = 0
                res.type = 'success'
                res.msg = getMsg( 1 )
            }
        }
        render res as JSON
    }

    def gerarMemoria() {
        sleep( 500 )
        if( !params.sqOficina ) {
            return this._returnAjax( 1, 'Parâmetros insuficientes!', 'error' );
        }
        Oficina oficina = Oficina.get( params.sqOficina.toInteger() )

        if( oficina ) {
            Map mapTotais = getGridExecucaoTotais()

            // ler papeis
            Map mapPapeis = lerPapeis( oficina )
            oficina.txEncaminhamento = params.txEncaminhamento
            oficina.save( flush: true )
            String unidade = oficina.unidade.noPessoa.toUpperCase()
            //session?.sicae?.user?.noUnidadeOrg ?: 'Sistema de Avaliação'
            String unidadeSigla = oficina.unidade.sgUnidade
            //session?.sicae?.user?.sgUnidadeOrg ?: 'SALVE'
            RtfOficina rtf = new RtfOficina( params.sqOficina.toInteger(), grailsApplication.config.temp.dir.toString(), unidade, unidadeSigla )
            String periodo = ''
            if( oficina.dtInicio && oficina.dtFim ) {
                if( oficina.dtInicio == oficina.dtFim ) {
                    periodo = oficina.dtInicio
                }
                else
                    if( oficina.dtInicio.format( 'MM/yyyy' ) == oficina.dtFim.format( 'MM/yyyy' ) ) {

                        periodo = oficina.dtInicio.format( 'dd' ) + ' a ' + oficina.dtFim.format( 'dd/MM/yyyy' )
                    }
                    else {
                        periodo = oficina.dtInicio.format( 'dd/MM/yyyy' ) + ' a ' + oficina.dtFim.format( 'dd/MM/yyyy' )
                    }
            }
            String fileName = rtf.runMemoriaOficina( periodo, mapTotais, mapPapeis )
            if( !fileName ) {
                return this._returnAjax( 1, 'Ocorreu um erro na criação do arquivo.', 'warining', [ fileName: '' ] )
            }
            return this._returnAjax( 0, '', 'success', [ fileName: fileName ] )
        }
        else {
            return this._returnAjax( 1, 'Oficina inválida.', 'error' )
        }
    }

    def gerarRelatorioLc() {
        sleep( 500 )
        if( !params.sqOficina ) {
            return this._returnAjax( 1, 'Parâmetros insuficientes!', 'error' );
        }
        Oficina oficina = Oficina.get( params.sqOficina.toInteger() )

        if( !oficina ) {
            return this._returnAjax( 1, 'Oficina inválida!', 'error' );
        }
        String unidade = oficina.unidade.noPessoa ? oficina.unidade.noPessoa.toUpperCase() : 'SALVE'
        String unidadeSigla = oficina.unidade.sgUnidade ?: 'Sistema de Avaliação'
        RtfOficina rtf = new RtfOficina( params.sqOficina.toInteger(), grailsApplication.config.temp.dir.toString(), unidade, unidadeSigla )
        params.imprimirJustificativaOficina = ( params.imprimirJustificativaOficina == null ) ? 'P' : params.imprimirJustificativaOficina // P = relatorio padrão
        String fileName = rtf.runDocLC( [ ids: params.ids,imprimirJustificativaOficina:params.imprimirJustificativaOficina ] )
        if( !fileName ) {
            return this._returnAjax( 1, 'Ocorreu um erro na criação do arquivo.', 'warining', [ fileName: '' ] )
        }
        return this._returnAjax( 0, '', 'success', [ fileName: fileName ] )
    }

    /**
     * Verificar se tem função atribuida aos participantes da oficina e exibir
     * alerta antes da impressao do relatório final
     */
    def existeFuncaoAtribuida(){
        Map res = [msg:'',status:0, type:'']
        String cmdSql = """select count( ofp.*) as total
            from salve.oficina_ficha_participante ofp
            inner join salve.oficina_ficha oficina_ficha on oficina_ficha.sq_oficina_ficha = ofp.sq_oficina_ficha
            where oficina_ficha.sq_oficina = :sqOficina
            """
        Map sqlParams = [sqOficina:params.sqOficina.toLong() ]
        List rows = sqlService.execSql( cmdSql, sqlParams)
        if( rows[0].total < 1 ) {
            res.msg = 'Atenção, antes da impressão do relatório final para assinatura lembre-se de cadastrar as funções dos participantes na aba Função.'
        }
        render res as JSON
    }

    def gerarDocFinal() {
        // flag utilizado no método getGridExecucaoTotais()
        params.relatorio = true

        if( !params.sqOficina ) {
            return this._returnAjax( 1, 'Parâmetros insuficientes!', 'error' );
        }

        Oficina oficina = Oficina.get( params.sqOficina.toInteger() )

        if( oficina ) {
            // ler papeis
            Map mapPapeis = lerPapeis( oficina )
            Map mapTotais = getGridExecucaoTotais()

            oficina.txEncaminhamento = params.txEncaminhamento
            oficina.save( flush: true )

            // String unidade  = session?.sicae?.user?.noUnidadeOrg ?: 'SALVE'
            // String unidadeSigla  = session?.sicae?.user?.sgUnidadeOrg ?: 'Sistema de Avaliação'
            String unidade = oficina.unidade.noPessoa ? oficina.unidade.noPessoa.toUpperCase() : 'SALVE'
            String unidadeSigla = oficina.unidade.sgUnidade ?: 'Sistema de Avaliação'
            RtfOficina rtf = new RtfOficina( params.sqOficina.toInteger(), grailsApplication.config.temp.dir.toString(), unidade, unidadeSigla, mapTotais )
            String dia = params.dtInicio
            String periodo = ''
            if( oficina.dtInicio && oficina.dtFim ) {
                if( oficina.dtInicio == oficina.dtFim ) {
                    periodo = oficina.dtInicio.format( 'dd/MM/yyyy' )
                }
                else
                    if( oficina.dtInicio.format( 'MM/yyyy' ) == oficina.dtFim.format( 'MM/yyyy' ) ) {

                        periodo = oficina.dtInicio.format( 'dd' ) + ' a ' + oficina.dtFim.format( 'dd/MM/yyyy' )
                    }
                    else {
                        periodo = oficina.dtInicio.format( 'dd/MM/yyyy' ) + ' a ' + oficina.dtFim.format( 'dd/MM/yyyy' )
                    }
            }
            // gerar o RTF
            String fileName = rtf.runDocFinal( dia, periodo, mapPapeis
                , params.tipo
                , params.chkConsiderarUAN
                , params.chkOrdenadoPorOrdemFamilia)

            if( ! fileName ) {
                return this._returnAjax( 1, 'Ocorreu um erro na criação do arquivo.', 'warining', [ fileName: '' ] )
            }
            return this._returnAjax( 0, '', 'success', [ fileName: fileName ] )
        }
        else {
            return this._returnAjax( 1, 'Oficina inválida.', 'error' )
        }
    }

    def getFormAnexo() {
        if( !params.sqOficina ) {
            render '<b>Id oficina faltando</b>'
            return
        }
        render( template: 'formAnexo' )
    }

    def saveAnexo() {
        if( !params.sqOficina ) {
            return this._returnAjax( 1, 'ID da oficina não informado.', "error" )
        }
        if( !session?.sicae?.user?.sqPessoa ) {
            return this._returnAjax( 1, 'Necessário fazer login para salvar anexo!.', "error" )
        }
        Map data = [ qtdParticipantes: 0i ]
        String anexosDir = ( grailsApplication.config.anexos.dir + '/' ).replaceAll( /anexos\/\//, 'anexos/' )
        //String urlSistema =(session.url+'/').replaceAll(/salve\/\//,'salve/')
        String urlSistema = ( grailsApplication.config.url.sistema + '/' ).replaceAll( /salve\/\//, 'salve/' )

        boolean isPdf = false
        // gravar o anexo
        def f = request.getFile( 'fldOficinaAnexo' )
        if( f && !f.empty ) {
            File savePath = new File( anexosDir )
            if( !savePath.exists() ) {
                if( !savePath.mkdirs() ) {
                    return this._returnAjax( 1, 'Não foi possivel criar o diretório ' + savePath.asString(), "error" );
                }
            }
            Oficina oficina = Oficina.get( params.sqOficina )
            if( oficina ) {
                String fileName = f.getOriginalFilename().replaceAll( ' |,|\\(.+\\)', '_' ).replaceAll( '_+', '-' ).replaceAll( '-\\.', '.' )
                String fileExtension = fileName.substring( fileName.lastIndexOf( "." ) )
                String hashFileName = f.inputStream.text.toString().encodeAsMD5() + fileExtension
                fileName = savePath.absolutePath + '/' + hashFileName
                isPdf = ( fileName ==~ /(i?).*\.pdf$/ ) // verificar se o arquivo termina com .pdf
                if( !new File( fileName ).exists() ) {
                    f.transferTo( new File( fileName ) );
                    ImageService.createThumb( fileName )
                }
                OficinaAnexo oficinaAnexoNovo = OficinaAnexo.findByOficinaAndDeLocalArquivo( oficina, hashFileName )
                if( !oficinaAnexoNovo ) {
                    oficinaAnexoNovo = new OficinaAnexo()
                    oficinaAnexoNovo.oficina = oficina
                    oficinaAnexoNovo.deLocalArquivo = hashFileName

                    // se estiver adicionando um pdf como documento final, verificar se já tem outro
                    if( isPdf && params.inRelatorioFinal ) {

                        Integer qtd = OficinaAnexo.createCriteria().count {
                            eq( 'oficina', oficina )
                            eq( 'inDocFinal', true )
                        }

                        /*qtd = OficinaAnexoAssinatura.createCriteria().count {
                        oficinaAnexo {
                            eq('oficina',oficina)
                        }*/

                        if( qtd > 0 ) {
                            return this._returnAjax( 1, 'Oficina já possui documento final anexado!<br>Caso queira substituí-lo, faça a exclusão do mesmo no gride de anexos da oficina.', 'error' )
                        }
                    }
                }
                else {
                    if( isPdf ) {
                        // se for adicionado um pdf e existir assinatura, não permitir gravar
                        Integer qtd = OficinaAnexoAssinatura.createCriteria().count {
                            eq( 'oficinaAnexo', oficinaAnexoNovo )
                            isNotNull( 'dtAssinatura' )
                        }
                        if( qtd > 0 ) {
                            return this._returnAjax( 1, 'Documento já possui ' + qtd.toString() + ' assinatura(s)!<br>Para regravá-lo faça a exclusão do mesmo no gride de anexos da oficina.', 'error' )
                        }
                        if( params.inRelatorioFinal ) {

                            qtd = OficinaAnexo.createCriteria().count {
                                eq( 'oficina', oficina )
                                eq( 'inDocFinal', true )
                                ne( 'id', oficinaAnexoNovo.id )
                            }


                            /*qtd = OficinaAnexoAssinatura.createCriteria().count {
                                oficinaAnexo {
                                    eq('oficina', oficina)
                                    ne('id', oficinaAnexoNovo.id)
                                }
                            }

                             */
                            if( qtd > 0 ) {
                                return this._returnAjax( 1, 'Oficina já possui documento final anexado!<br>Caso queira substituí-lo, faça a exclusão do mesmo no gride de anexos da oficina.', 'error' )
                            }
                        }
                        //
                    }
                }
                oficinaAnexoNovo.pessoa = PessoaFisica.get( session.sicae.user.sqPessoa )
                oficinaAnexoNovo.deLegenda = params.deLegendaOficinaAnexo
                oficinaAnexoNovo.noArquivo = f.getOriginalFilename()
                oficinaAnexoNovo.deTipoConteudo = f.getContentType()
                if( isPdf && params.inRelatorioFinal ) {
                    oficinaAnexoNovo.inDocFinal = true
                }
                if( !oficinaAnexoNovo.save() ) {
                    println oficinaAnexoNovo.getErrors()
                    println "-" * 100
                    return this._returnAjax( 1, 'Erroa ao salvar o anexo!', 'error' )
                }

                // quando for o documento final alimentar a tabela oficina_anexo_assinatura com os participantes
                // que possuirem funçao na oficina
                if( ( oficinaAnexoNovo.noArquivo ==~ /(i?).*\.pdf$/ ) ) {
                    OficinaAnexoAssinatura.executeUpdate( "delete OficinaAnexoAssinatura oficinaAnexoAssinatura where oficinaAnexoAssinatura.oficinaAnexo.id = :id ", [ id: oficinaAnexoNovo.id ] )
                    if( params.inRelatorioFinal ) {
                        // excluir as assinaturas do pdf antigo se existir
                        OficinaAnexoAssinatura reg = OficinaAnexoAssinatura.createCriteria().get {
                            maxResults( 1 )
                            oficinaAnexo {
                                eq( 'oficina', oficina )
                            }
                        }
                        if( reg ) {
                            // deletar o pdf e as assinaturas
                            OficinaAnexo.executeUpdate( "delete OficinaAnexo oficinaAnexo where oficinaAnexo.id = :id and oficinaAnexo.id <> :idNovo ", [ id: reg.oficinaAnexo.id, idNovo: oficinaAnexoNovo.id ] )
                            OficinaAnexoAssinatura.executeUpdate( "delete OficinaAnexoAssinatura oficinaAnexoAssinatura where oficinaAnexoAssinatura.oficinaAnexo.id = :id ", [ id: reg.oficinaAnexo.id ] )
                        }
                        // ler os participantes com funçao na oficina
                        // e criar as novas assinaturas eletrônicas
                        String sql = """select distinct participante.sq_oficina_participante
                        from salve.oficina_ficha_participante ofp
                        inner join salve.oficina_ficha oficina_ficha on oficina_ficha.sq_oficina_ficha = ofp.sq_oficina_ficha
                        inner join salve.vw_oficina_participante as participante on participante.sq_oficina_participante = ofp.sq_oficina_participante
                        where oficina_ficha.sq_oficina = :sqOficina"""
                        List listParticipantesComFuncao = sqlService.execSql( sql, [ sqOficina: oficina.id ] )
                        listParticipantesComFuncao.each {
                            OficinaAnexoAssinatura novo = new OficinaAnexoAssinatura()
                            novo.oficinaAnexo = oficinaAnexoNovo
                            novo.oficinaParticipante = OficinaParticipante.get( it.sq_oficina_participante )
                            novo.save()
                            data.qtdParticipantes++
                        }
                        // nome da unidade organizacional para o cabecalho das paginas
                        String codigoArquivo = oficinaAnexoNovo.deLocalArquivo.substring( 0, 6 )
                        String unidadeOrg = ''
                        if( oficina.unidade ) {
                            //unidadeOrg = oficina.unidade.noPessoa + (oficina.unidade.sgUnidade ? ' - ' + oficina.unidade.sgUnidade : '')
                            unidadeOrg = oficina.unidade.noPessoa
                        }

                        // adicionar o qr code no final documento anexado
                        Util.assinarPdf( anexosDir + oficinaAnexoNovo.deLocalArquivo
                            , 'Para consultar o documento on-line, acesse o endereço eletrônico abaixo:\n' + urlSistema + 'validarDoc\ne informe o código de acesso: ' + codigoArquivo
                            , urlSistema + 'validarDoc/' + codigoArquivo, '', unidadeOrg )
                    }
                }
                return this._returnAjax( 0, 'Anexo Gravado com SUCESSO!', 'success', data )
            }
            return this._returnAjax( 1, 'Id oficina inválido!', 'error' )
        }
        return this._returnAjax( 1, 'Arquivo inválido!', 'error' );
    }

    def getGridAnexo() {
        if( !params.sqOficina ) {
            render '<h2>Id oficina não informado!</h2>'
            return
        }
        //List listOficinaAnexo = sqlService.execSql("""select * from salve.vw_oficina_anexo where sq_oficina = :sqOficina""",[ sqOficina : params.sqOficina.toLong() ])
        render( template: 'divGridOficinaAnexo', model: [ listOficinaAnexo: oficinaService.getAnexos( params.sqOficina.toLong() ) ] )
    }

    def deleteAnexo() {
        if( !params.id ) {
            return this._returnAjax( 1, 'Id não informado', 'error' );
        }
        OficinaAnexo fa = OficinaAnexo.get( params.id.toInteger() )
        if( !fa ) {
            return this._returnAjax( 1, 'Id não encontrado!', 'error' );
        }
        String deLocalArquivo = fa.deLocalArquivo
        String deLocalThumb = ''
        //Integer qtd = OficinaAnexo.countByDeLocalArquivo(deLocalArquivo)
        // apagar do banco de dados
        fa.delete( flash: true )

        // ler somente o nome do arquivo sem o caminho completo
        Integer posicao = deLocalArquivo.lastIndexOf( '/' )
        if( posicao > 0 ) {
            deLocalArquivo = deLocalArquivo.substring( posicao + 1 )
        }

        // verificar se o anexo está em uso
        if( OficinaAnexo.countByDeLocalArquivoAndIdNotEqual( deLocalArquivo, fa.id ) == 0 ) {
            deLocalThumb = grailsApplication.config.anexos.dir + 'thumbnail.' + deLocalArquivo
            deLocalArquivo = grailsApplication.config.anexos.dir + '/' + deLocalArquivo
            // excluir o anexo do disco se não tiver sendo usado por outro registro
            File file = new File( deLocalArquivo )
            if( file.exists() ) {
                // apagar do disco o arquivo
                file.delete()
            }
            file = new File( deLocalThumb )
            if( file.exists() ) {
                // apagar do disco a imagem thumb
                file.delete()
            }
        }
        return this._returnAjax( 0 )
    }

    //************************************************************************************

    def getAnexoMemoria() {

        if( !params.id ) {
            response.sendError( 404 );
            return;
        }
        OficinaMemoriaAnexo pa = OficinaMemoriaAnexo.get( params.id );
        if( !pa ) {
            response.sendError( 404 );
            return;
        }

        String hashFileName = pa.deLocalArquivo
        Integer posicao = hashFileName.lastIndexOf( '/' )
        if( posicao > 0 ) {
            hashFileName = hashFileName.substring( posicao + 1 )
            pa.deLocalArquivo = hashFileName
            pa.save( flush: true )
        }
        File file
        if( params.thumb ) {
            file = new File( grailsApplication.config.anexos.dir + 'thumbnail.' + hashFileName );
            if( file.exists() ) {
                hashFileName = grailsApplication.config.anexos.dir + 'thumbnail.' + hashFileName
            }
            else {
                file = null
            }
        }
        if( !file ) {
            file = new File( grailsApplication.config.anexos.dir + hashFileName )
        }
        if( !file || !file.exists() ) {
            file = new File( grailsApplication.config.arquivos.dir + 'arquivo-nao-encontrado.pdf' )
            if( !file.exists() ) {
                response.sendError( 400 )
                return
            }
        }
        String fileType = pa.deTipoConteudo ? pa.deTipoConteudo : 'application/octet-stream';
        String fileName = pa.noArquivo.replaceAll( ' |,|\\(.+\\)', '_' ).replaceAll( '_+', '-' ).replaceAll( '-\\.', '.' )
        if( fileType ) {
            render( file: file, fileName: fileName, contentType: fileType )
        }
        else {
            render( file: file, fileName: fileName )
        }
    }

    //************************************************************************************

    def deleteAnexoMemoria() {
        if( !params.id ) {
            return this._returnAjax( 1, 'Id não informado', 'error' );
        }
        OficinaMemoriaAnexo fa = OficinaMemoriaAnexo.get( params.id.toLong() )
        if( !fa ) {
            return this._returnAjax( 1, 'Id não encontrado!', 'error' );
        }
        String deLocalArquivo = fa.deLocalArquivo
        String deLocalThumb = ''
        //Integer qtd = OficinaAnexo.countByDeLocalArquivo(deLocalArquivo)
        // apagar do banco de dados
        fa.delete( flash: true )

        // ler somente o nome do arquivo sem o caminho completo
        Integer posicao = deLocalArquivo.lastIndexOf( '/' )
        if( posicao > 0 ) {
            deLocalArquivo = deLocalArquivo.substring( posicao + 1 )
        }

        // verificar se o anexo está em uso por outro registro
        if( OficinaMemoriaAnexo.countByDeLocalArquivoAndIdNotEqual( deLocalArquivo, fa.id ) == 0 ) {
            deLocalThumb = grailsApplication.config.anexos.dir + 'thumbnail.' + deLocalArquivo
            deLocalArquivo = grailsApplication.config.anexos.dir + '/' + deLocalArquivo
            // excluir o anexo do disco se não tiver sendo usado por outro registro
            File file = new File( deLocalArquivo )
            if( file.exists() ) {
                // apagar do disco o arquivo
                file.delete()
            }
            file = new File( deLocalThumb )
            if( file.exists() ) {
                // apagar do disco a imagem thumb
                file.delete()
            }
        }
        return this._returnAjax( 0 )
    }

    //************************************************************************************

    def getAnexo() {
        if( !params.id ) {
            response.sendError( 404 );
            return;
        }
        OficinaAnexo pa = OficinaAnexo.get( params.id );
        if( !pa ) {
            response.sendError( 404 );
            return;
        }
        String hashFileName = pa.deLocalArquivo
        Integer posicao = hashFileName.lastIndexOf( '/' )
        if( posicao > 0 ) {
            hashFileName = hashFileName.substring( posicao + 1 )
            pa.deLocalArquivo = hashFileName
            pa.save( flush: true )
        }
        File file
        if( params.thumb ) {
            file = new File( grailsApplication.config.anexos.dir + 'thumbnail.' + hashFileName );
            if( file.exists() ) {
                hashFileName = grailsApplication.config.anexos.dir + 'thumbnail.' + hashFileName
            }
            else {
                file = null
            }
        }
        if( !file ) {

            file = new File( grailsApplication.config.anexos.dir + hashFileName )
        }

        if( !file || !file.exists() ) {
            file = new File( grailsApplication.config.arquivos.dir + 'arquivo-nao-encontrado.pdf' )
            if( !file.exists() ) {
                response.sendError( 400 )
                return
            }
        }


        String fileType = pa.deTipoConteudo ? pa.deTipoConteudo : 'application/octet-stream';
        String fileName = pa.noArquivo.replaceAll( ' |,|\\(.+\\)', '_' ).replaceAll( '_+', '-' ).replaceAll( '-\\.', '.' )
        if( fileType ) {
            render( file: file, fileName: fileName, contentType: fileType )
        }
        else {
            render( file: file, fileName: fileName )
        }
    }

    def encerrarOficina() {
        Map res = [ : ]
        res.msg = ''
        res.type = 'error'
        res.status = 1
        Oficina oficina
        DadosApoio encerrada
        if( !params.sqOficina ) {
            res.msg = 'Id da oficina não informado!'
        }
        else {
            oficina = Oficina.get( params.sqOficina.toInteger() )
            if( !oficina ) {
                res.msg = 'Oficina Inválida!'
            }
            else {
                encerrada = dadosApoioService.getByCodigo( 'TB_SITUACAO_OFICINA', 'ENCERRADA' )
                if( !encerrada ) {
                    res.msg = 'Situação ENCERRADA não cadastrada na tabela de apoio TB_SITUACAO_OFICINA'
                }
                else {
                    // verificar se a oficina está aberta
                    if( oficina.situacao != dadosApoioService.getByCodigo( 'TB_SITUACAO_OFICINA', 'ABERTA' ) ) {
                        res.msg = 'Oficina não está ABERTA!'
                    }
                    else {
                        // verificar se todas as fichas estão avaliadas.
                        List situacoesValidas = [ ]
                        DadosApoio.findAllByCodigoSistemaInList( [ 'AVALIADA', 'AVALIADA_REVISADA', 'POS_OFICINA', 'EXCLUIDA', 'VALIDADA', 'COMPILACAO' ] ).each {
                            situacoesValidas.push( it.id.toInteger() )
                        }
                        Integer qtd = OficinaFicha.createCriteria().count {
                            eq( 'oficina', oficina )
                            eq('stTransferida',false)
                            vwFicha {
                                not {
                                    'in'( "sqSituacaoFicha", situacoesValidas )
                                }
                            }
                        }
                        if( qtd > 0 ) {
                            res.msg = 'Existe(m) ' + qtd.toString() + ' ficha(s) não avaliada(s).'
                        }
                        else {
                            oficina.situacao = encerrada
                            if( !oficina.dtFim || oficina.dtFim > Util.hoje() ) {
                                oficina.dtFim = Util.hoje()
                            }
                            oficina.save( flush: true )
                            res.msg = 'Oficina encerrada com SUCESSO!'
                            res.type = 'success'
                            res.status = 0
                        }
                    }
                }
            }
        }
        render res as JSON
    }

    def enviarEmailAssinaturaEletronica() {
        Map res = [ status: 1, msg: '', type: 'error', erros: [ ] ]
        int qtdEnvios = 0;

        // TODO - tornar texto do email configurável pelo usuário
        String msgEmail = """<p>Prezado(a) {nome},<br>Clique no link abaixo para visualizar e assinar eletronicamente o documento final da {oficina} realizada em {local} no período de {periodo}.</p><p>{link}</p>"""

        String urlSalve = grailsApplication.config.url.sistema
        String linkSalveAssinar = '<a href="' + urlSalve + '/api/assinarDocFinalOficina/{hash}" style="color:green!important" target="_blank"><b>Visualizar documento</b></a>'
        linkSalveAssinar = linkSalveAssinar.replaceAll( /\/\/api\//, '/api/' )
        try {
            if( !urlSalve ) {
                throw new Exception( 'Url sistema não registrado na sessão. Efetue login novamente!' )
            }

            if( params.sqOficinaParticipante ) {
                OficinaParticipante oficinaParticipante = OficinaParticipante.get( params.sqOficinaParticipante.toLong() )
                if( !oficinaParticipante ) {
                    throw new Exception( 'Id do participante inexistente!' )
                }
                params.sqOficina = oficinaParticipante.oficina.id
                // localizar o doc final nos anexos
                OficinaAnexo oficinaAnexo = OficinaAnexo.findByOficinaAndInDocFinal( oficinaParticipante.oficina, true )
                if( !oficinaAnexo ) {
                    throw new Exception( 'Documento final da oficina não anexado!' )
                }
                // verificar se o participante já tem hash da assinatura
                Integer qtd = OficinaAnexoAssinatura.countByOficinaAnexoAndOficinaParticipante( oficinaAnexo, oficinaParticipante )
                if( qtd == 0 ) {
                    OficinaAnexoAssinatura novo = new OficinaAnexoAssinatura()
                    novo.oficinaAnexo = oficinaAnexo
                    novo.oficinaParticipante = oficinaParticipante
                    novo.save( flush: true )
                    params.hash = novo.deHash
                }

            }

            if( !params.sqOficina ) {
                throw new Exception( 'ID da oficina não informado!' )
            }
            Oficina oficina = Oficina.get( params.sqOficina.toLong() )
            if( !oficina ) {
                throw new Exception( 'ID da oficina inválido!' )
            }
            OficinaAnexoAssinatura.createCriteria().list {
                oficinaAnexo {
                    eq( 'oficina', oficina )
                }
                isNull( 'dtAssinatura' )
                if( params.sqOficinaParticipante ) {
                    eq( 'oficinaParticipante.id', params.sqOficinaParticipante.toLong() )
                }
                if( params.hash ) {
                    eq( 'deHash', params.hash )
                }
            }.eachWithIndex { it, index ->

                // enviar somente para quem ainda não assinou
                if( !it.dtAssinatura ) {
                    String nomeParticipante = it.oficinaParticipante.pessoa.noPessoa
                    String emailParticipante = it.oficinaParticipante.deEmail
                    String siglaInstituicaoParticipante = it.oficinaParticipante.instituicao.sgInstituicao
                    String nomeInstituicaoParticipante = it.oficinaParticipante.instituicao.noInstituicao
                    String linkTemp = linkSalveAssinar.replaceAll( /\{hash\}/, it.deHash )
                    String nomeOficina = oficina.noOficina
                    String periodoOficina = oficina.periodo
                    String msg = msgEmail.replaceAll( /\{nome\}/, Util.capitalize( nomeParticipante ) )
                        .replaceAll( /\{link\}/, linkTemp )
                        .replaceAll( /\{oficina\}/, nomeOficina )
                        .replaceAll( /\{periodo\}/, periodoOficina )
                        .replaceAll( /\{local\}/, oficina.deLocal )

                    //if( emailService.sendTo( 'eugeniob@tba.com.br','Assinatura Eletrônica - SALVE', msg ) ){

                    // quando for localhost não enviar
                    if( ( session.url ==~ /.*localhost.*/ ) || emailService.sendTo( emailParticipante, 'Assinatura Eletrônica - SALVE', msg ) ) {
                        it.dtEmail = new Date()
                        it.save( flush: true )
                        qtdEnvios++
                    }
                    else {
                        res.erros.push( ' - ' + emailParticipante + ' não enviado.' )
                    }
                }
            }
            if( qtdEnvios > 0 ) {
                res.msg = qtdEnvios + ' email(s) enviado(s) com SUCESSO!'
            }
            else {
                res.msg = 'Nenhum email enviado! Todos os participantes já assinaram.'
            }
            res.status = 0
            res.type = 'success'
        }
        catch( Exception e ) {
            res.type = 'error'
            res.error = e.getMessage()
        }
        if( res.erros ) {
            res.msg = 'Erro<br>' + res.erros.join( '<br>' )
            res.erros = [ ]
        }
        render res as JSON
    }

    /**
     * método para retornar os grupos das fichas que fazem parte da oficina
     */
    def getGruposFichasOficina(){
        Map res = [ status:0, msg: '', type: 'success', data:[] ]
        try {
            Oficina oficina = Oficina.get( params.sqOficina.toLong() )
            if( oficina ){
                res.data = sqlService.execSqlCache("""select distinct grupo.sq_dados_apoio as sq_grupo from salve.oficina_ficha
                    inner join salve.oficina on oficina.sq_oficina = oficina_ficha.sq_oficina
                    inner join salve.ficha on ficha.sq_ficha = oficina_ficha.sq_ficha
                    inner join salve.dados_apoio as grupo on grupo.sq_dados_apoio = ficha.sq_grupo
                    where oficina.sq_oficina = :sqOficina
                    """,[sqOficina:oficina.id],5)
            }
        } catch( Exception e ) {
            res.status=1
            res.type='error'
            res.msg = e.getMessage()
        }
        render res as JSON
    }
}
