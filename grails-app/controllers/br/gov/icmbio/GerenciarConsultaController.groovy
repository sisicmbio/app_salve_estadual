package br.gov.icmbio
import grails.converters.JSON
//import groovy.json.JsonBuilder
import groovy.sql.Sql

class GerenciarConsultaController extends BaseController {
    def fichaService
    def dadosApoioService
    def emailService
    def cacheService
    def sqlService
    def logService

    private String getTextoEmailPadrao() {
        return """<div style="border: 1px solid gray;padding:5px;line-height:normal;text-align: justify;font-size:18px;font-family:Times New Roman,Arial;">
                <p>Prezado(a) {nome},</p>
                <p>O Instituto Chico Mendes de Conserva&ccedil;&atilde;o da Biodiversidade &eacute; respons&aacute;vel pelo Processo de Avalia&ccedil;&atilde;o do Estado de Conserva&ccedil;&atilde;o da Fauna Brasileira, cujos resultados subsidiam o Minist&eacute;rio do Meio Ambiente na publica&ccedil;&atilde;o da Lista Nacional Oficial de Esp&eacute;cies Amea&ccedil;adas de Extin&ccedil;&atilde;o.</p>
                <p>Estamos organizando avalia&ccedil;&atilde;o dos XXXXXXXXXX e, considerando seu conhecimento sobre o grupo, solicitamos revis&atilde;o das informa&ccedil;&otilde;es e complementa&ccedil;&otilde;es para as seguintes esp&eacute;cies:</p>
                <p>{fichas}</p>
                <p>As fichas de informa&ccedil;&otilde;es dessas esp&eacute;cies podem ser acessadas no m&oacute;dulo de consulta do Sistema de Avalia&ccedil;&atilde;o do Estado de Conserva&ccedil;&atilde;o da Biodiversidade &ndash; SALVE. {linkSalveConsulta}.</p>
                <p>Para visualizar as fichas das esp&eacute;cies e enviar suas contribui&ccedil;&otilde;es, &eacute; necess&aacute;rio cadastrar-se no Sistema e efetuar login. Aten&ccedil;&atilde;o: ao efetuar seu cadastro, utilize o mesmo e-mail do seu cadastro no ICMBio {emailConvidado}.</p>
                <p>Acesse o site, fa&ccedil;a seu cadastro e login e envie suas contribui&ccedil;&otilde;es at&eacute; o dia {dataFinal}. Os devidos cr&eacute;ditos ser&atilde;o dados ao longo do processo de avalia&ccedil;&atilde;o e na publica&ccedil;&atilde;o das fichas a todos os colaboradores.</p>
                <p>Contamos com a sua colabora&ccedil;&atilde;o!</p>
                <p>Atenciosamente,<br>{usuario}<br>{unidade}</p>
                </div>
                """
    }

    /**
     * Resolvido em reuinião com equipe no dia 01/07/2020 que somente as fichas na situção
     * compilação, consulta finalizada e consolidada poderão entrar em consulta e que
     * a situação da ficha só poderá ser retroagida pelos administradores e que se a ficha
     * tiver sido avaliada ou validada o salve deverá emitir alerta informando que estas
     * informações das abas 11.6 e 11.7 serão zeradas
     *
     */
    private void _alterarSituacaoFichas(Long sqCicloConsulta = null) {
        if (!sqCicloConsulta) {
            return
        }

        d(' ')
        d('ALTERAR SITUACAO FICHAS')
        d('sq_cicloConsulta:' + sqCicloConsulta)

        // CATEGORIAS
        DadosApoio categoriaNE = dadosApoioService.getByCodigo('TB_CATEGORIA_IUCN', 'NE')

        // SITUACOES POSSIVEIS DE ALTERACAO CASO ALTERE A DATA DA CONSULTA/REVISAO
        DadosApoio compilacao = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'COMPILACAO')
        DadosApoio consulta = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'CONSULTA')
        DadosApoio consultaFinalizada = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'CONSULTA_FINALIZADA')
        DadosApoio consolidada = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'CONSOLIDADA')
        DadosApoio avaliada = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'POS_OFICINA')
        DadosApoio avaliadaRevisao = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'AVALIADA_EM_REVISAO')

        DadosApoio validacao = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'EM_VALIDACAO')
        DadosApoio validada = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'VALIDADA')
        DadosApoio validadaRevisao = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'VALIDADA_EM_REVISAO')

        DadosApoio colaboracaoNaoAvaliada = dadosApoioService.getByCodigo('TB_SITUACAO_COLABORACAO', 'NAO_AVALIADA')
        //DadosApoio resolverEmOficina = dadosApoioService.getByCodigo('TB_SITUACAO_COLABORACAO','RESOLVER_OFICINA')

        //println 'SqCicloAvaliacao: ' + sqCicloAvaliacao
        //List idsSituacoesValidas = [ compilacao.id, consulta.id, consultaFinalizada.id,  avaliada.id, avaliadaRevisao.id]
        List idsSituacoesValidas = [compilacao.id, consulta.id, consultaFinalizada.id, consolidada.id, avaliada.id, avaliadaRevisao.id, validada.id, validacao.id, validadaRevisao.id]

        Map alterarSituacaoPara = ['CONSULTA': [], 'CONSULTA_FINALIZADA': [], 'CONSOLIDADA': [], 'AVALIADA': [], 'AVALIADA_EM_REVISAO': [], 'VALIDADA': [], 'VALIDADA_EM_REVISAO': []]

        // ler as fichas da consulta informada
        String cmdSql = """select ficha.sq_ficha,
                    situacao_ficha.cd_sistema as cd_situacao_ficha,
                    tipo_consulta.cd_sistema as cd_tipo_consulta,
                    pendencia.nu_pendencia,
                    ciclo_consulta.dt_fim
                    from salve.ciclo_consulta
                    inner join salve.ciclo_consulta_ficha on ciclo_consulta_ficha.sq_ciclo_consulta = ciclo_consulta.sq_ciclo_consulta
                    inner join salve.ficha on ficha.sq_ficha = ciclo_consulta_ficha.sq_ficha
                    inner join salve.dados_apoio situacao_ficha on situacao_ficha.sq_dados_apoio = ficha.sq_situacao_ficha
                    inner join salve.dados_apoio tipo_consulta on tipo_consulta.sq_dados_apoio = ciclo_consulta.sq_tipo_consulta
                    left join lateral(
                        select count( ficha_pendencia.sq_ficha ) as nu_pendencia
                        from salve.ficha_pendencia
                        where ficha_pendencia.sq_ficha = ficha.sq_ficha
                        and ficha_pendencia.st_pendente = 'S'
                        and ficha_pendencia.de_assunto = 'Campos obrigatórios'
                    ) as pendencia on true
                    where ciclo_consulta.sq_ciclo_consulta = ${sqCicloConsulta}
                    """
        d('Iniciando loop fichas');
        sqlService.execSql(cmdSql).each { row ->
            if (row.sq_ficha) {
                // CONSULTA ESTA ABERTA
                if (row.dt_fim >= Util.hoje()) {
                    d('Entrou consulta aberta')
                    d('Situacao da ficha:' + row.cd_situacao_ficha)
                    d('Tipo consulta: ' + row.cd_tipo_consulta)
                    if (row.cd_tipo_consulta == 'REVISAO_POS_OFICINA') {
                        /* ficha pode ser colocada em revisao a qualquer momento e não pode ser alterada a situação da ficha
                        // ficha esta EM_VALIDACAO e foi colocada em uma REVISÃO POS OFICINA
                        if (row.cd_situacao_ficha == 'EM_VALIDACAO') {
                            // NÃO ALTERA NADA
                        } else if (row.cd_situacao_ficha == 'VALIDADA') {
                            d('Passar para VALIDADA EM REVISAO')
                            // ficha esta VALIDADA e foi colocada em Revisão
                            alterarSituacaoPara.VALIDADA_EM_REVISAO.push(row.sq_ficha)
                        } else if (row.cd_situacao_ficha == 'VALIDADA') {
                            // ficha esta VALIDADA_EM_REVISAO e foi colocada em Revisão então NÃO ALTERA NADA
                        } else if ( ! ( row.cd_situacao_ficha =~ /^(AVALIADA_EM_REVISAO|VALIDADA_EM_REVISAO)$/) ) {
                            // ficha foi colocada em revisão
                            alterarSituacaoPara.AVALIADA_EM_REVISAO.push(row.sq_ficha)
                        }*/
                    } else {
                        // AVALIADAS COLOCADAS EM CONSULTA
                        if (row.cd_situacao_ficha == 'POS_OFICINA' /* AVALIADA*/) {
                            alterarSituacaoPara.AVALIADA_EM_REVISAO.push(row.sq_ficha)
                        } else if (row.cd_situacao_ficha =~ /^(COMPILACAO|CONSULTA_FINALIZADA|CONSOLIDADA)$/) {
                            alterarSituacaoPara.CONSULTA.push(row.sq_ficha)
                        }
                    }
                } else {
                    // CONSULTA / REVISÃO ESTÁ ENCERRADA
                    d('Entrou consulta encerrada')
                    d('Situacao da ficha:' + row.cd_situacao_ficha)
                    d('Tipo consulta: ' + row.cd_tipo_consulta)

                    if (row.cd_situacao_ficha =~ /^(CONSULTA)$/) {
                        Integer qtdColaboracoesPendentes = 0
                        // se não tiver pendencia, vericar se tem colaboracao pendente
                        if (row.nu_pendencia == 0) {
                            // se a ficha não tiver ALGUMA colaboração passar para CONSOLIDADA
                            qtdColaboracoesPendentes = FichaColaboracao.createCriteria().count {
                                'in'('situacao', [colaboracaoNaoAvaliada])
                                consultaFicha {
                                    eq('ficha.id', row.sq_ficha.toLong())
                                }
                            }
                            // verificar se houve colaboração de registros de ocorrencias nao aceitos
                            if (qtdColaboracoesPendentes == 0) {
                                qtdColaboracoesPendentes = FichaOcorrenciaConsulta.createCriteria().count {
                                    fichaOcorrencia {
                                        'in'('situacao', [colaboracaoNaoAvaliada])
                                    }
                                    cicloConsultaFicha {
                                        eq('ficha.id', row.sq_ficha.toLong())
                                    }
                                }
                            }
                        }
                        if (qtdColaboracoesPendentes == 0 && row.nu_pendencia == 0) {
                            alterarSituacaoPara.CONSOLIDADA.push(row.sq_ficha)
                        } else {
                            alterarSituacaoPara.CONSULTA_FINALIZADA.push(row.sq_ficha)
                        }
                    } else if (row.cd_situacao_ficha == 'AVALIADA_EM_REVISAO') {
                        // se a ficha estiver em AVALIADA_EM_REVISAO ela deve sempre ir para CONSULTA_FINALIZADA ao terminar a consulta
                        alterarSituacaoPara.CONSULTA_FINALIZADA.push(row.sq_ficha)
                    }
                    // se a ficha estiver VALIDADA ou VALIDADA_EM_REVISAO NÃO ALTERAR A SITUACAO
                }
            }
        }
        //d(alterarSituacaoPara.toString())

        logService.log('GerenciarConsulta'
                , 'saveConsulta +  _alterarSituacaoFichas()'
                , session.sicae.user.noUsuario
                , session.sicae.user.nuCpf
                , Util.getRequestIp(request)
                , alterarSituacaoPara)

        if (alterarSituacaoPara.CONSULTA) {
            sqlService.execSql("""update salve.ficha set sq_situacao_Ficha = ${consulta.id.toString()}
                    ,sq_categoria_iucn = ${categoriaNE.id.toString()}
                    ,st_manter_lc=null
                    ,sq_categoria_final=null
                    ,ds_justificativa=null
                    ,ds_justificativa_final=null
                    ,ds_criterio_aval_iucn=null
                    ,ds_criterio_aval_iucn_final=null
                    ,st_possivelmente_extinta=null
                    ,st_possivelmente_extinta_final=null
                    ,sq_tipo_conectividade=null
                    ,sq_tendencia_imigracao=null
                    ,st_declinio_br_popul_exterior=null
                    ,ds_conectividade_pop_exterior=null
                    where sq_ficha in ( ${alterarSituacaoPara.CONSULTA.join(',')} )""")

            // excluir motivo mudanca categoria se houver
            sqlService.execSql("""delete from salve.ficha_mudanca_categoria where sq_ficha in (${alterarSituacaoPara.CONSULTA.join(',')});""")
        }
        if (alterarSituacaoPara.CONSOLIDADA) {
            sqlService.execSql("""update salve.ficha set sq_situacao_ficha = ${consolidada.id.toString()} where sq_ficha in (${alterarSituacaoPara.CONSOLIDADA.join(',')});""")
            //Ficha.executeUpdate("update Ficha ficha set ficha.situacaoFicha = :situacao where ficha.id in(:ids)", [ids: alterarSituacaoPara.CONSOLIDADA, situacao: consolidada])
            // se a ficha for LC e passar para consolidade, o campo st_manter_lc da ficha deve ser marcado com true se tiver null
            fichaService.verificarStManterLc(alterarSituacaoPara.CONSOLIDADA)
        }
        if (alterarSituacaoPara.CONSULTA_FINALIZADA) {
            //Ficha.executeUpdate("update Ficha ficha set ficha.situacaoFicha = :situacao where ficha.id in(:ids)", [ids: alterarSituacaoPara.CONSULTA_FINALIZADA, situacao: consultaFinalizada ])
            sqlService.execSql("""update salve.ficha set sq_situacao_ficha = ${consultaFinalizada.id.toString()} where sq_ficha in (${alterarSituacaoPara.CONSULTA_FINALIZADA.join(',')});""")
        }
        if (alterarSituacaoPara.AVALIADA) {
            //Ficha.executeUpdate("update Ficha ficha set ficha.situacaoFicha = :situacao where ficha.id in(:ids)", [ids: alterarSituacaoPara.AVALIADA, situacao: avaliada])
            sqlService.execSql("""update salve.ficha set sq_situacao_ficha = ${avaliada.id.toString()} where sq_ficha in (${alterarSituacaoPara.AVALIADA.join(',')});""")
        }
        if (alterarSituacaoPara.AVALIADA_EM_REVISAO) {
            //Ficha.executeUpdate("update Ficha ficha set ficha.situacaoFicha = :situacao where ficha.id in(:ids)", [ids: alterarSituacaoPara.AVALIADA_EM_REVISAO, situacao: avaliadaRevisao])
            sqlService.execSql("""update salve.ficha set sq_situacao_ficha = ${avaliadaRevisao.id.toString()} where sq_ficha in (${alterarSituacaoPara.AVALIADA_EM_REVISAO.join(',')});""")
        }
        if (alterarSituacaoPara.VALIDADA) {
            //Ficha.executeUpdate("update Ficha ficha set ficha.situacaoFicha = :situacao where ficha.id in(:ids)", [ids: alterarSituacaoPara.VALIDADA, situacao: validada])
            sqlService.execSql("""update salve.ficha set sq_situacao_ficha = ${validada.id.toString()} where sq_ficha in (${alterarSituacaoPara.VALIDADA.join(',')});""")
        }
        if (alterarSituacaoPara.VALIDADA_EM_REVISAO) {
            //Ficha.executeUpdate("update Ficha ficha set ficha.situacaoFicha = :situacao where ficha.id in(:ids)", [ids: alterarSituacaoPara.VALIDADA_EM_REVISAO, situacao: validadaRevisao])
            sqlService.execSql("""update salve.ficha set sq_situacao_ficha = ${validadaRevisao.id.toString()} where sq_ficha in (${alterarSituacaoPara.VALIDADA_EM_REVISAO.join(',')});""")
        }

        /* NÃO PRECISOU MAIS
        // voltar as fichas para compilação/avaliada que estão em CONSULTA/AVALIADA_EM_REVISAO/CONSULTA_FINALIZADA sem ter uma consulta vinculada
        cmdSql = """update salve.ficha set sq_situacao_ficha = case when sq_situacao_ficha = ${avaliadaRevisao.id.toString()} then ${avaliada.id.toString()} else ${compilacao.id.toString() } end
        where sq_ficha in ( select tmp.sq_ficha from ( select a.sq_ficha, c.cd_sistema, count(b.sq_ciclo_consulta_ficha) as nu_consultas from salve.ficha a
        left outer join salve.ciclo_consulta_ficha b on b.sq_ficha = a.sq_ficha left outer join salve.dados_apoio c on c.sq_dados_apoio = a.sq_situacao_ficha
        where c.cd_sistema in ('CONSULTA','CONSULTA_FINALIZADA','AVALIADA_EM_REVISAO') and a.sq_ficha in ( ${ ids.join(',') } )
        group by a.sq_ficha, c.cd_sistema
        having count(b.sq_ciclo_consulta_ficha) = 0) tmp )"""
        sqlService.execSql( query )*/

    }

    // NAO UTILIZADO MAIS
    private void _corrigirSituacaoFichas(List<Long> ids = [], Long sqCicloAvaliacao = 0l) {

        return // não será mais utilizado


        if (!ids) {
            return
        }

        d(' ')
        d('CORRIGIR SITUACAO FICHAS')
        d(ids.join(','))


        // se não passar o ciclo, pegar da primeira ficha informada
        if (sqCicloAvaliacao == 0l) {
            sqCicloAvaliacao = VwFicha.get(ids[0]).sqCicloAvaliacao.toLong()
        }
        if (!sqCicloAvaliacao) {
            return
        }
        // CATEGORIAS
        DadosApoio categoriaNE = dadosApoioService.getByCodigo('TB_CATEGORIA_IUCN', 'NE')

        // SITUACOES POSSIVEIS DE ALTERACAO CASO ALTERE A DATA DAS CONSULTAS
        DadosApoio compilacao = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'COMPILACAO')
        DadosApoio consulta = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'CONSULTA')
        DadosApoio consultaFinalizada = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'CONSULTA_FINALIZADA')
        DadosApoio consolidada = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'CONSOLIDADA')
        DadosApoio avaliada = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'POS_OFICINA')
        DadosApoio avaliadaRevisao = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'AVALIADA_EM_REVISAO')

        DadosApoio validacao = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'EM_VALIDACAO')
        DadosApoio validada = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'VALIDADA')
        DadosApoio validadaRevisao = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'VALIDADA_EM_REVISAO')

        DadosApoio colaboracaoNaoAvaliada = dadosApoioService.getByCodigo('TB_SITUACAO_COLABORACAO', 'NAO_AVALIADA')
        //DadosApoio resolverEmOficina = dadosApoioService.getByCodigo('TB_SITUACAO_COLABORACAO','RESOLVER_OFICINA')

        //println 'SqCicloAvaliacao: ' + sqCicloAvaliacao
        //List idsSituacoesValidas = [ compilacao.id, consulta.id, consultaFinalizada.id,  avaliada.id, avaliadaRevisao.id]
        List idsSituacoesValidas = [compilacao.id, consulta.id, consultaFinalizada.id, consolidada.id, avaliada.id, avaliadaRevisao.id, validada.id, validacao.id, validadaRevisao.id]
        Map voltarPara = ['CONSULTA': [], 'CONSULTA_FINALIZADA': [], 'CONSOLIDADA': [], 'AVALIADA': [], 'AVALIADA_EM_REVISAO': [], 'VALIDADA': [], 'VALIDADA_EM_REVISAO': []]
        String query = """select new map( a.ficha.id as sqFicha, a.ficha.situacaoFicha.codigoSistema as cdSituacaoFichaSistema
                    , c.codigoSistema as cdTipoConsulta
                    , max( b.dtFim ) as dtFim)
                    from CicloConsultaFicha a
                    inner join a.cicloConsulta b
                    inner join b.tipoConsulta c
                    where b.cicloAvaliacao.id = ${sqCicloAvaliacao.toString()}
                    and a.ficha.id in (${ids.join(',')} )
                    and a.ficha.situacaoFicha.id IN (${idsSituacoesValidas.join(',')})
                    group by a.ficha.id, a.ficha.situacaoFicha.codigoSistema,c.codigoSistema
                    """
        d('Iniciando loop fichas');
        CicloConsultaFicha.executeQuery(query).each {
            if (it?.sqFicha) {
                VwFicha vwFicha = VwFicha.get(it.sqFicha.toLong())

                // QUANDO A CONSULTA ESTA ABERTA

                if (it.dtFim >= Util.hoje()) {
                    d('Entrou consulta aberta')
                    d('Situacao da ficha:' + it.cdSituacaoFichaSistema)
                    d('Tipo consulta: ' + it.cdTipoConsulta)
                    if (it.cdTipoConsulta == 'REVISAO_POS_OFICINA') {
                        // ficha esta EM_VALIDACAO e foi colocada em uma REVISÃO POS OFICINA
                        if (vwFicha.cdSituacaoFichaSistema == 'EM_VALIDACAO') {
                            // NÃO ALTERA NADA
                        } else if (vwFicha.cdSituacaoFichaSistema == 'VALIDADA') {
                            // ficha esta VALIDADA e foi colocada em Revisão
                            voltarPara.VALIDADA_EM_REVISAO.push(it.sqFicha)
                        } else if (vwFicha.cdSituacaoFichaSistema == 'VALIDADA') {
                            // ficha esta VALIDADA_EM_REVISAO e foi colocada em Revisão
                            // NÃO ALTERA NADA
                        } else if (it.cdSituacaoFichaSistema != 'AVALIADA_EM_REVISAO') {
                            // ficha foi colocada em revisão
                            voltarPara.AVALIADA_EM_REVISAO.push(it.sqFicha)
                        }
                    } else {
                        if (it.cdSituacaoFichaSistema != 'POS_OFICINA' /* AVALIADA*/) {
                            voltarPara.CONSULTA.push(it.sqFicha)
                        } else {
                            // AVALIADAS
                            voltarPara.AVALIADA_EM_REVISAO.push(it.sqFicha)
                        }
                    }
                } else {
                    d('Entrou consulta encerrada')
                    d('Situacao da ficha:' + it.cdSituacaoFichaSistema)
                    d('Tipo consulta: ' + it.cdTipoConsulta)
                    // ENTRA AQUI QUANDO A CONSULTA ESTA FINALIZADA

                    if (it.cdSituacaoFichaSistema =~ /^(CONSULTA|COMPILACAO)$/) {
                        Integer qtdColaboracoesPendentes = 0
                        // se não tiver pendencia, vericar se tem colaboracao pendente
                        if (vwFicha.nuPendencia == 0) {
                            // se a ficha não tiver ALGUMA colaboração passar para CONSOLIDADA
                            qtdColaboracoesPendentes = FichaColaboracao.createCriteria().count {
                                'in'('situacao', [colaboracaoNaoAvaliada])
                                consultaFicha {
                                    eq('ficha.id', vwFicha.id.toLong())
                                }
                            }
                            // verificar se houve colaboração de registros de ocorrencias nao aceitos
                            if (qtdColaboracoesPendentes == 0) {
                                qtdColaboracoesPendentes = FichaOcorrenciaConsulta.createCriteria().count {
                                    fichaOcorrencia {
                                        'in'('situacao', [colaboracaoNaoAvaliada])
                                    }
                                    cicloConsultaFicha {
                                        eq('ficha.id', vwFicha.id.toLong())
                                    }
                                }
                            }
                        }
                        if (qtdColaboracoesPendentes == 0 && vwFicha.nuPendencia == 0) {
                            voltarPara.CONSOLIDADA.push(vwFicha.id)
                        } else {
                            voltarPara.CONSULTA_FINALIZADA.push(vwFicha.id)
                        }
                    } else if (it.cdSituacaoFichaSistema == 'AVALIADA_EM_REVISAO') {
                        // se a ficha estiver em AVALIADA_EM_REVISAO ela deve sempre ir para CONSULTA_FINALIZADA ao terminar a consulta
                        voltarPara.CONSULTA_FINALIZADA.push(vwFicha.id)
                    }
                    // se a ficha estiver VALIDADA ou VALIDADA_EM_REVISAO NÃO ALTERAR A SITUACAO
                }
            }
        }
        d(voltarPara.toString())

        logService.log('GerenciarConsulta'
                , 'saveConsulta +  _corrigirSituacaoFichas()'
                , session.sicae.user.noUsuario
                , session.sicae.user.nuCpf
                , Util.getRequestIp(request)
                , voltarPara)

        if (voltarPara.CONSULTA) {
            Ficha.executeUpdate("""update Ficha ficha
                    set ficha.situacaoFicha = :situacao
                    ,ficha.categoriaIucn = :categoriaNE
                    ,ficha.stManterLc=null
                    ,ficha.categoriaFinal=null
                    ,ficha.dsJustificativa=null
                    ,ficha.dsJustificativaFinal=null
                    ,ficha.dsCriterioAvalIucn=null
                    ,ficha.dsCriterioAvalIucnFinal=null
                    ,ficha.stPossivelmenteExtinta=null
                    ,ficha.stPossivelmenteExtintaFinal=null
                    ,ficha.tipoConectividade=null
                    ,ficha.tendenciaImigracao=null
                    ,ficha.stDeclinioBrPopulExterior=null
                    ,ficha.dsConectividadePopExterior=null
                    where ficha.id in(:ids)""", [ids: voltarPara.CONSULTA, situacao: consulta, categoriaNE: categoriaNE])
            // excluir motivo mudanca categoria se houver
            sqlService.execSql("""delete from salve.ficha_mudanca_categoria where sq_ficha in (${voltarPara.CONSULTA.join(',')})""")
        }
        if (voltarPara.CONSOLIDADA) {
            Ficha.executeUpdate("update Ficha ficha set ficha.situacaoFicha = :situacao where ficha.id in(:ids)", [ids: voltarPara.CONSOLIDADA, situacao: consolidada])
            // se a ficha for LC e passar para consolidade, o campo st_manter_lc da ficha deve ser marcado com true se tiver null
            fichaService.verificarStManterLc(voltarPara.CONSOLIDADA)
        }
        if (voltarPara.CONSULTA_FINALIZADA) {
            Ficha.executeUpdate("update Ficha ficha set ficha.situacaoFicha = :situacao where ficha.id in(:ids)", [ids: voltarPara.CONSULTA_FINALIZADA, situacao: consultaFinalizada])
        }
        if (voltarPara.AVALIADA) {
            Ficha.executeUpdate("update Ficha ficha set ficha.situacaoFicha = :situacao where ficha.id in(:ids)", [ids: voltarPara.AVALIADA, situacao: avaliada])
        }
        if (voltarPara.AVALIADA_EM_REVISAO) {
            Ficha.executeUpdate("update Ficha ficha set ficha.situacaoFicha = :situacao where ficha.id in(:ids)", [ids: voltarPara.AVALIADA_EM_REVISAO, situacao: avaliadaRevisao])
        }
        if (voltarPara.VALIDADA) {
            Ficha.executeUpdate("update Ficha ficha set ficha.situacaoFicha = :situacao where ficha.id in(:ids)", [ids: voltarPara.VALIDADA, situacao: validada])
        }
        if (voltarPara.VALIDADA_EM_REVISAO) {
            Ficha.executeUpdate("update Ficha ficha set ficha.situacaoFicha = :situacao where ficha.id in(:ids)", [ids: voltarPara.VALIDADA_EM_REVISAO, situacao: validadaRevisao])
        }
        // voltar as fichas para compilação/avaliada que estão em CONSULTA/AVALIADA_EM_REVISAO/CONSULTA_FINALIZADA sem ter uma consulta vinculada
        query = """update salve.ficha set sq_situacao_ficha = case when sq_situacao_ficha = ${avaliadaRevisao.id.toString()} then ${avaliada.id.toString()} else ${compilacao.id.toString()} end
        where sq_ficha in ( select tmp.sq_ficha from ( select a.sq_ficha, c.cd_sistema, count(b.sq_ciclo_consulta_ficha) as nu_consultas from salve.ficha a
        left outer join salve.ciclo_consulta_ficha b on b.sq_ficha = a.sq_ficha left outer join salve.dados_apoio c on c.sq_dados_apoio = a.sq_situacao_ficha
        where c.cd_sistema in ('CONSULTA','CONSULTA_FINALIZADA','AVALIADA_EM_REVISAO') and a.sq_ciclo_avaliacao = ${sqCicloAvaliacao.toString()} and a.sq_ficha in ( ${ids.join(',')} )
        group by a.sq_ficha, c.cd_sistema
        having count(b.sq_ciclo_consulta_ficha) = 0) tmp )"""
        sqlService.execSql(query)
    }


    def index() {
        List listCiclosAvaliacao = [session.getAttribute('ciclo')]
        List listNivelTaxonomico = NivelTaxonomico.list()
        List listSituacaoFicha = fichaService.getListSituacao()
        List listTipoConsultaFicha = dadosApoioService.getTable('TB_TIPO_CONSULTA_FICHA')
        render('view': 'index', model: [listCiclosAvaliacao    : listCiclosAvaliacao
                                        , listNivelTaxonomico  : listNivelTaxonomico
                                        , listTipoConsultaFicha: listTipoConsultaFicha
                                        , listSituacaoFicha    : listSituacaoFicha
        ])
    }





    def getGridConsultas() {
        List qryWhere = []
        Map qryParams = [:]
        String qryOrderBy = 'ciclo_consulta.dt_inicio desc '
        String dtHoje = new Date().format('yyyy-MM-dd')

        //String hoje = new Date().format('yyyy-MM-dd');
        if (!params.sqCicloAvaliacao) {
            render 'Id do ciclo de avaliação não foi informado'
            return
        }
        // lista consultas do ciclo
        //List listConsultas = CicloConsulta.findAllByCicloAvaliacao(CicloAvaliacao.get(params.sqCicloAvaliacao),['sort':'tipoConsulta.descricao'])
        params.canModify = ( session.sicae.user.canConsolidarConsulta() )

        if (!session.sicae.user.isADM()) {
            DadosApoio papel
            //if( ( session.sicae.user.isCT() || session.sicae.user.isCE() ) && ! session?.sicae?.user?.sqUnidadeOrg  )
            // SE O USUÁRIO NÃO TIVER UNIDADE, FAZER FILTRO PELA FUNÇÃO ATRIBUIDA NAS FICHAS
            if (!session?.sicae?.user?.sqUnidadeOrg) {
                params.canModify = false
                papel = dadosApoioService.getByCodigo('TB_PAPEL_FICHA', (session.sicae.user.isCT() ? 'COORDENADOR_TAXON' : 'COLABORADOR_EXTERNO'))
                if (!papel) {
                    render 'Perfil ' + (session.sicae.user.isCT() ? 'COORDENADOR_TAXON' : 'COLABORADOR_EXTERNO') + ' não cadastrado na tabela TB_PAPEL_FICHA'
                    return;
                }
                params.inAtivo = 'S'
                params.sqPessoa = session?.sicae?.user?.sqPessoa?.toLong()
                params.sqPapel = papel?.id?.toLong()
                qryParams.sqPessoa = params.sqPessoa.toLong()
                qryParams.inAtivo = params.inAtivo.toString()
                qryParams.sqPapel = params.sqPapel.toLong()
            } else if (session?.sicae?.user?.sqUnidadeOrg) {
                params.sqUnidadeOrg = session.sicae.user.sqUnidadeOrg.toLong()
            } else {
                render 'Sem acesso a estas informações'
                return
            }
        }

        // filtros
        qryWhere.push('ciclo_consulta.sq_ciclo_avaliacao = :sqCicloAvaliacao')
        qryParams.sqCicloAvaliacao = params.sqCicloAvaliacao.toLong()
        // UNIDADE ORGANIZACIONAL
        if (params.sqUnidadeOrg) {
            qryWhere.push('ciclo_consulta.sq_unidade_org = :sqUnidadeOrg')
            qryParams.sqUnidadeOrg = params.sqUnidadeOrg.toLong()
        }
        // TIPO DE CONSULTA
        if (params.sqTipoConsulta) {
            qryWhere.push('ciclo_consulta.sq_tipo_consulta = :sqTipoConsulta')
            qryParams.sqTipoConsulta = params.sqTipoConsulta.toLong()
        }
        if (params.coSituacaoConsulta) {
            if (params.coSituacaoConsulta.toUpperCase() == 'ABERTA') {
                qryWhere.push("ciclo_consulta.dt_fim >= '" + dtHoje + "'")
            }
            if (params.coSituacaoConsulta.toUpperCase() == 'ENCERRADA') {
                qryWhere.push("ciclo_consulta.dt_fim < '" + dtHoje + "'")
            }
        }
        if (params.deTituloConsulta) {
            qryWhere.push("concat(ciclo_consulta.de_titulo_consulta,' ',ciclo_consulta.sg_consulta) ilike :deTituloConsulta")
            qryParams.deTituloConsulta = '%' + params.deTituloConsulta + '%'
        }
        // SIGLA DA UNIDADE ORG
        if (params.sgUnidadeOrg) {
            qryWhere.push('unidade.sg_unidade_org ilike :sgUnidadeOrg')
            qryParams.sgUnidadeOrg = '%' + params.sgUnidadeOrg + '%'
        }

        // CONSULTAS DE UMA DETERMINADA ESPÉCIE
        params.noCientifico = params.noCientificoFiltrarConsulta ?: params.noCientifico
        if ( params.noCientifico || params.sqPessoa ) {
            qryWhere.push('fichas.nu_fichas > 0')
           if( params.noCientifico ) {
                qryParams.noCientifico = '%' + params.noCientifico + '%'
            }
        }
        if (params.sortColumns) {
            List sortParams = params.sortColumns.split(':')
            qryOrderBy = sortParams[0] + ' ' + (sortParams[1] ?: '')
        }
        String cmdSql = """select ciclo_consulta.sq_ciclo_consulta
                            , ciclo_consulta.dt_inicio
                            , ciclo_consulta.dt_fim
                            , ciclo_consulta.de_titulo_consulta
                            , ciclo_consulta.sg_consulta
                            , unidade.sg_unidade_org
                            , tipo.ds_dados_apoio as ds_tipo_consulta
                            , case when dt_fim >= '${dtHoje}' then true else false end as st_aberta
                            , fichas.nu_fichas
                            from salve.ciclo_consulta
                            inner join salve.ciclo_avaliacao on ciclo_avaliacao.sq_ciclo_avaliacao = ciclo_consulta.sq_ciclo_avaliacao
                            inner join salve.vw_unidade_org as unidade on unidade.sq_pessoa = ciclo_consulta.sq_unidade_org
                            inner join salve.dados_apoio as tipo on tipo.sq_dados_apoio = ciclo_consulta.sq_tipo_consulta
                            left join lateral (
                               select count(*) as nu_fichas
                               from salve.ciclo_consulta_ficha ccf
                               ${params.noCientifico ? 'INNER JOIN salve.ficha on ficha.sq_ficha = ccf.sq_ficha' : ''}
                               ${params.sqPessoa ? 'INNER JOIN salve.ficha_pessoa on ficha_pessoa.sq_ficha = ccf.sq_ficha' : ''}
                               where ccf.sq_ciclo_consulta = ciclo_consulta.sq_ciclo_consulta
                               ${params.noCientifico ? '\nand ficha.nm_cientifico ilike :noCientifico' : ''}
                               ${params.sqPessoa ? '\nand ficha_pessoa.sq_pessoa = :sqPessoa and ficha_pessoa.in_ativo = :inAtivo and ficha_pessoa.sq_papel = :sqPapel': ''}
                            ) as fichas on true
                            ${qryWhere ? '\nwhere ' + qryWhere.join(' and ') : ''}
                            ${qryOrderBy ? '\norder by ' + qryOrderBy : ''}"""
        //println cmdSql
        Map data = sqlService.paginate(cmdSql, qryParams, params)
        render(template: "divGridConsultas", model: [listConsultas: data.rows, canModify: params.canModify, pagination: data.pagination])
    }

    def getGridFichas() {
        if (!params.sqCicloAvaliacao) {
            render ''
            return
        }
        String msgEmpty = 'Nenhuma ficha encontrada!'
        boolean canModify = this.isLastCicle(params.sqCicloAvaliacao.toInteger())

        // exibir somente as fichas de acordo com o tipo da consulta
        DadosApoio tipoConsulta

        if (params.sqTipoConsulta && ! params.sqSituacaoFiltro ) {
            tipoConsulta = DadosApoio.get( params.sqTipoConsulta.toLong() )
            if (tipoConsulta?.codigoSistema == 'REVISAO_POS_OFICINA') {
                //params.sqSituacaoFiltro = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA','POS_OFICINA')?.id
                params.sqSituacaoFiltro = dadosApoioService.getTable('TB_SITUACAO_FICHA').findAll {
                    return true // revisão aceita qualquer ficha agora
                    //it.codigoSistema =~ /^(CONSOLIDADA|POS_OFICINA|EM_VALIDACAO|VALIDADA|VALIDADA_EM_REVISAO)$/
                }?.id?.join(',')
                //println params.sqSituacaoFiltro
                //msgEmpty = 'Nenhuma ficha CONSOLIDADA, AVALIADA, EM VALIDAÇÃO, VALIDADA ou VALIDADA EM REVISÃO encontrada!'
                msgEmpty = 'Nenhuma ficha encontrada!'
            }
        }

         // filtrar somente as fichas de acordo com o tipo da consulta
        if ( ! params.sqSituacaoFiltro) {
            DadosApoio tabelaSituacaoFicha = DadosApoio.findByCodigo('TB_SITUACAO_FICHA')

            // SE A FICHA ESTIVER EM CONSULTA AMPLA ELA PODE SER ADICIONADA EM UMA CONSULTA DIRETA E VIRSE-VERSA
            List listSituacoesFiltrar = []
            if (tipoConsulta?.codigoSistema == 'CONSULTA_DIRETA') {
                listSituacoesFiltrar.push('CONSULTA');
            } else if ( tipoConsulta?.codigoSistema == 'CONSULTA_AMPLA') {
                listSituacoesFiltrar.push('CONSULTA');
            }
            listSituacoesFiltrar += ['COMPILACAO', 'CONSULTA_FINALIZADA', 'CONSOLIDADA']
            params.sqSituacaoFiltro = DadosApoio.createCriteria().list {
                eq('pai', tabelaSituacaoFicha)
                'in'('codigoSistema', listSituacoesFiltrar)
            }?.id?.join(',')
            msgEmpty = 'Nenhuma ficha em ' + listSituacoesFiltrar.join(', ').replaceAll(/_/, ' ') + ' foi encontrada!'
        }
        List listFichas = fichaService.search(params)
        render(template: "divGridFicha", model: [listFichas: listFichas, canModify: canModify, msgEmpty: msgEmpty])
    }

    def saveConsulta() {
        Long sqCicloConsultaAlterando = 0l
        CicloAvaliacao cicloAvaliacao
        CicloConsulta cicloConsulta
        DadosApoio tipoConsulta
        //List<Long> idsEmConsulta
        List listaFichasEmConsulta
        List listaFichasExistentesNaConsulta
        List<Long> idsPostados = []
        boolean naoRetroativo = true // cadastro com data retroativa não deve alterar a situação das fichas
        DadosApoio situacaoConsulta
        DadosApoio situacaoValidada
        DadosApoio situacaoValidadaEmRevisao
        DadosApoio situacaoEmValidacao
        DadosApoio situacaoAvaliadaEmRevisao
        Date dtInicioAtual = null
        Date dtFimAtual = null

        List msgAlertas = []

        Map res = [status: 1, msg: '', type: 'error', errors: []]
        // validar ciclo avaliação
        if (!params.sqCicloAvaliacao) {
            res.errors.push('Ciclo Avaliação não selecionado!')
        }

        // validar tipo
        if (!params.sqTipoConsulta) {
            res.errors.push('Tipo da consulta deve ser informado!')
        }
        // validar titulo
        if (!params.deTituloConsulta) {
            res.errors.push('Titulo da consulta deve ser informado!')
        }
        if (!params.sgConsulta) {
            res.errors.push('Nome módulo consulta deve ser informado!')
        }

        // validar periodo
        if (params.dtInicio) {
            params.dtInicio = new Date().parse('dd/MM/yyyy', params.dtInicio)
        } else {
            res.errors.push('Data início deve ser informada!')
        }
        if (params.dtFim) {
            params.dtFim = new Date().parse('dd/MM/yyyy', params.dtFim)
        } else {
            res.errors.push('Data final deve ser informada!')
        }

        if (params.dtInicio > params.dtFim) {
            res.errors.push('Data final deve ser MAIOR ou IGUAL a inicial!')
        }
        if (!params['ids'] && !params.sqCicloConsulta) {
            res.errors.push('Nenhuma ficha selecionada!')
        }
        if ( ! res.errors ) {
            naoRetroativo = (params.dtFim >= Util.hoje() || params.sqCicloConsulta)
            tipoConsulta = DadosApoio.get(params.sqTipoConsulta)
            cicloAvaliacao = CicloAvaliacao.get(params.sqCicloAvaliacao)

            if (params.sqCicloConsulta) {
                cicloConsulta = CicloConsulta.get(params.sqCicloConsulta)
                sqCicloConsultaAlterando = cicloConsulta.id.toLong()
                // na alteração se der inconsistência voltar o periodo anterior
                dtInicioAtual = cicloConsulta.dtInicio
                dtFimAtual = cicloConsulta.dtFim
            } else {
                cicloConsulta = CicloConsulta.findByCicloAvaliacaoAndTipoConsultaAndDeTituloConsultaAndDtInicioAndDtFim(cicloAvaliacao, tipoConsulta, params.deTituloConsulta, params.dtInicio, params.dtFim)
                if (!cicloConsulta) {
                    cicloConsulta = new CicloConsulta()
                    cicloConsulta.cicloAvaliacao = cicloAvaliacao


                    // TODO-SALVE
                    // simular unidade para ADM - REMOVER DEPOIS
                    // ANTIGO: cicloConsulta.unidade = Unidade.get(session.sicae.user.sqUnidadeOrg.toLong())
                    session.sicae.user.sqUnidadeOrg = 4
                    cicloConsulta.unidade = VwUnidadeOrg.findById(session.sicae.user.sqUnidadeOrg)
                    // TODO-SALVE FIM

                } else {
                    // na alteração se der inconsistência voltar o periodo anterior
                    dtInicioAtual = cicloConsulta.dtInicio
                    dtFimAtual = cicloConsulta.dtFim
                }
            }
            cicloConsulta.tipoConsulta = tipoConsulta
            cicloConsulta.dtInicio = params.dtInicio
            cicloConsulta.dtFim = params.dtFim
            cicloConsulta.deTituloConsulta = params.deTituloConsulta
            cicloConsulta.sgConsulta = params.sgConsulta
            cicloConsulta.validate()

            if ( ! cicloConsulta.hasErrors()) {
                cicloConsulta.save(flush: true)

                // se a consulta for do tipo REVISÃO POS OFICINA, pode estar
                // sendo adicionada fichas que estão na Avaliação ou na Validação
                if (tipoConsulta.codigoSistema == 'REVISAO_POS_OFICINA') {
                    // situações utilizadas para as fichas que estão na VALIDAÇÃO e que irão para ou voltarão da REVISÃO POS OFICINA
                    situacaoValidada = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'VALIDADA')
                    situacaoValidadaEmRevisao = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'VALIDADA_EM_REVISAO')
                    situacaoEmValidacao = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'EM_VALIDACAO')
                }

                if (params['ids']) {
                    params.ids.split(',').each {
                        idsPostados.push(it.toLong())
                    }
                }
                // criar a lista com as fichas que já estão cadastradas na consulta para não duplicar
                listaFichasExistentesNaConsulta = CicloConsultaFicha.executeQuery("""
                        select new map( a.ficha.id as sqFicha, a.ficha.situacaoFicha.codigoSistema as cdSituacaoFichaSistema )
                        from CicloConsultaFicha a
                        where a.cicloConsulta.id = :sqCicloConsulta
                        """, [sqCicloConsulta: cicloConsulta.id])
                if( listaFichasExistentesNaConsulta ) {
                    List idsJaCadastrados = listaFichasExistentesNaConsulta.sqFicha
                    idsJaCadastrados -= idsPostados
                    idsPostados += idsJaCadastrados
                }
                // se for uma inclusão com data retroativa, não alterar a situação das fichas
                if (naoRetroativo) {
                    // adicionar as fichas no ciclo da consulta
                    if (tipoConsulta.codigoSistema == 'REVISAO_POS_OFICINA') {
                        situacaoConsulta = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'AVALIADA_EM_REVISAO')
                    } else {
                        situacaoConsulta = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'CONSULTA')
                    }
                    // gerar lista critica para verificar se as fichas selecionadas já estão ou não em alguma consulta aberta
                    listaFichasEmConsulta = CicloConsultaFicha.executeQuery("""
                        select new map( a.ficha.id as sqFicha
                        , a.ficha.nmCientifico as nmCientifico
                        , a.ficha.situacaoFicha.codigoSistema as cdSituacaoFichaSistema
                        , c.codigoSistema as cdTipoConsultaSistema
                        , max( b.dtFim ) as dtFim)
                        from CicloConsultaFicha a
                        inner join a.cicloConsulta b
                        inner join b.tipoConsulta c
                        where b.cicloAvaliacao.id = :sqCicloAvaliacao
                        and a.ficha.id in (:ids)
                        and a.cicloConsulta.id <> :sqCicloConsulta
                        and b.dtFim >= :hoje
                        group by a.ficha.id, a.ficha.nmCientifico, a.ficha.situacaoFicha.codigoSistema, c.codigoSistema
                        """, [sqCicloConsulta: sqCicloConsultaAlterando, ids: idsPostados, sqCicloAvaliacao: cicloAvaliacao.id, hoje: Util.hoje()])

                    idsPostados.each { idFicha ->
                        try {
                            // criticar se já esta em consulta somente se a data da consulta for >= hoje
                            if (cicloConsulta.dtFim >= Util.hoje()) {
                                Map itemCritico = listaFichasEmConsulta.find { it.sqFicha.toLong() == idFicha.toLong() && it.cdTipoConsultaSistema == cicloConsulta.tipoConsulta.codigoSistema }
                                if (itemCritico) {
                                    // se a ficha estiver em consulta não pode ser incluida em revisao e virse-versa
                                    if (tipoConsulta.codigoSistema == 'REVISAO_POS_OFICINA') {
                                        if (itemCritico.cdTipoConsultaSistema == cicloConsulta.tipoConsulta.codigoSistema || itemCritico.cdTipoConsultaSistema == 'CONSULTA_AMPLA' || itemCritico.cdTipoConsultaSistema == 'CONSULTA_DIRETA') {
                                            // permitir que a ficha possa estar em mais de uma revisao ao mesmo tempo.
                                            // Necessidade devido ao CBC ter aberto revisão para a Conabio e os centros não puderam colocar suas fichas que estavam na cobabio em suas próprias revisões
                                            // throw new RuntimeException(' - ' + itemCritico.nmCientifico + ' já está em ' + cicloConsulta.tipoConsulta.descricao + '!')
                                        }
                                    } else if (itemCritico.cdTipoConsultaSistema == cicloConsulta.tipoConsulta.codigoSistema || itemCritico.cdTipoConsultaSistema == 'REVISAO_POS_OFICINA') {
                                        throw new RuntimeException(' - ' + itemCritico.nmCientifico + ' já está em ' + cicloConsulta.tipoConsulta.descricao + '!')
                                    }
                                }
                            }
                            if (!listaFichasExistentesNaConsulta.find { it.sqFicha.toLong() == idFicha.toLong() }) {
                                new CicloConsultaFicha(ficha: Ficha.get(idFicha.toInteger()), cicloConsulta: cicloConsulta).save(flush: true)
                            }
                        } catch (Exception e) {
                            msgAlertas.push(e.getMessage())
                        }
                    }
                    _alterarSituacaoFichas(cicloConsulta.id.toLong())
                } else {
                    // consulta criada com data retroativa - adicionar as fichas somente
                    idsPostados.each { idFicha ->
                        if (!listaFichasExistentesNaConsulta.find { it.sqFicha.toLong() == idFicha.toLong() }) {
                            new CicloConsultaFicha(ficha: Ficha.get(idFicha.toInteger()), cicloConsulta: cicloConsulta).save(flush: true)
                        }
                    }
                }
                res.status = 0
                if ( msgAlertas ) {
                    if( dtFimAtual ) {
                        cicloConsulta.dtInicio = dtInicioAtual
                        cicloConsulta.dtFim = dtFimAtual
                        cicloConsulta.save(flush:true);
                        res.type = 'modal'
                        res.msg = 'Inconsistência<br>'+ msgAlertas.join('<br>')

                    } else {
                        res.type = 'info'
                        res.msg = 'Consulta gravada com ALERTA!<br><br>Ficha(s) NÃO adicionada(s)<br>' + msgAlertas.join('<br>')
                    }
                } else {
                    res.type = 'success'
                    res.msg = 'Consulta gravada com SUCESSO!'
                }
            }
        } else {
            res.status = 1
            res.type = 'error'
            res.msg = 'Erro de gravação!'
        }
        render res as JSON
    }
    def editConsulta(){
        Map res = [status: 1, msg: '', style: 'error']
        if (!params.sqCicloConsulta)
        {
            res.msg = 'Ciclo Consulta não selecionado!'
            render res as JSON
            return
        } else
        {
            CicloConsulta cicloConsulta = CicloConsulta.get(params.sqCicloConsulta)
            if (cicloConsulta)
            {
                render cicloConsulta.asJson()
                return
            }
            render [:] as JSON
        }
    }


    def deleteConsulta() {
        List listaTemp
        Map res = [status: 1, msg: '', type: 'error']
        if (!params.sqCicloConsulta) {
            res.msg = 'Ciclo Consulta não selecionado'
            render res as JSON
            return
        }

        CicloConsulta cicloConsulta = CicloConsulta.get( params.sqCicloConsulta.toLong() )
        if (!cicloConsulta) {
            res.msg = 'Ciclo Consulta inexistente'
            render res as JSON
            return
        }

        // SALVE SEM CICLO
        // se o consulta possuir alguma ficha versionada ela não pode ser excluída
        Integer nuFichasVersionadas = cicloConsulta.nuFichasVersionadas()

        if( nuFichasVersionadas > 0 ){
            res.msg = 'Exclusão nao permitida porque o registro possui ' + nuFichasVersionadas + ' ficha(s) versionada(s)'
            render res as JSON
            return
        }

        // guardar ids das fichas para ajustar a situação de após a exclusão
        List listaCadastrados = CicloConsultaFicha.executeQuery("""
                    select new map( a.ficha.id as sqFicha, c.nuVersao as nuVersao )
                    from CicloConsultaFicha a
                    left join a.cicloConsultaFichaVersao b
                    left join b.fichaVersao c
                    where a.cicloConsulta.id = :sqCicloConsulta
                    """, [sqCicloConsulta: params.sqCicloConsulta.toLong()])


        // 1) excluir da tabela CICLO_CONSULTA_PESSOA_FICHA
           // já é cascade
        // 2) excluir da tabela CICLO_CONSULTA_PESSOA
           // ja é cascade
        // 3) excluir da tabela FICHA_OCORRENCIA_VALIDACAO
            // ja é cascade
        // 4) exlcuir da tabela FICHA_OCORRENCIA
        // 5) não permitir excluir a consulta se ela possuir ficha versionada

        listaTemp = FichaOcorrenciaConsulta.executeQuery("""
            select new map( a.fichaOcorrencia.id as sqFichaOcorrencia)
              from FichaOcorrenciaConsulta a
              inner join a.cicloConsultaFicha b
              where b.cicloConsulta.id = :sqCicloConsulta
            """, [sqCicloConsulta: params.sqCicloConsulta.toLong()] )


        // se a consulta excluida estiver abera, fichar com a data de ontem e atualizar as fichas antes de excluir
        if( cicloConsulta.dtFim >= Util.hoje() ){
            DadosApoio compilacao   = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA','COMPILACAO')
            DadosApoio consulta     = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA','CONSULTA')
            DadosApoio avaliada     = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA','POS_OFICINA')
            DadosApoio avaliadaRevisao = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA','AVALIADA_EM_REVISAO')
            // voltar a ficha para a situacao anterior
            String cmdSql = """update salve.ficha set sq_situacao_ficha = case when sq_situacao_ficha = ${avaliadaRevisao.id.toString()} then ${avaliada.id.toString()} else
                        CASE WHEN sq_situacao_ficha= ${consulta.id.toString()} then ${compilacao.id.toString()} ELSE sq_situacao_ficha end end where sq_ficha in (${ listaCadastrados.sqFicha.join(',')})"""
            sqlService.execSql( cmdSql)
        }

        if( listaTemp.size() > 0 )
        {
            FichaOcorrencia.executeUpdate("delete from FichaOcorrencia a where a.id in (:ids)",[ids : listaTemp.sqFichaOcorrencia ] )
        }
        // 5) excluir da tabela FICHA_OCORRENCIA_CONSULTA
            // já é cascade
        // 6) excluir da tabela FICHA_COLABORACAO
            // já é cascade
        // 7) excluir da tabela CICLO_CONSULTA_FICHA
            // já é cascade
        // 8) excluir da tabela CICLO_CONSULTA
        CicloConsulta.executeUpdate("delete from CicloConsulta a where a.id = :id",[ id : cicloConsulta.id ] )
        res.status = 0
        res.type   = 'success'
        res.msg    = 'Registro excluído com SUCESSO!'
        render res as JSON
    }
    def deleteConsultaOld(){
        Map res = [status: 1, msg: '', style: 'error']
        if (!params.sqCicloConsulta)
        {
            res.msg = 'Ciclo Consulta não selecionado!'
            render res as JSON
            return
        } else
        {
            // alterar a situação das fichas apos excluir uma consulta
            DadosApoio situacaoCompilacao = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'COMPILACAO')
            DadosApoio situacaoConsulta = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'CONSULTA')
            DadosApoio situacaoConsultaFinalizada = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'CONSULTA_FINALIZADA')
            CicloConsulta cicloConsulta = CicloConsulta.get(params.sqCicloConsulta)

            // guardar as fichas
            /*List fichasDoCicloExcluido = CicloConsultaFicha.createCriteria().list {
                eq('cicloConsulta', cicloConsulta)
            }.vwFicha?.id
            */

            List fichasDoCicloExcluido = []
            CicloConsultaFicha.createCriteria().list {
                eq('cicloConsulta', cicloConsulta)
            }.each {
                fichasDoCicloExcluido.push(it.vwFicha.id.toLong())
                // println 'Colaboraces excluidas'
                // excluir as colaboracoes de ocorrências
                FichaOcorrenciaConsulta.findAllByCicloConsultaFicha(it).each { oco ->

                    FichaOcorrencia fo = oco.fichaOcorrencia
                    // println 'delete ocorrencia colaboracao ' + oco.id
                    oco.delete(flush: true)
                    // println 'ocorrencia colaboracao excluida'
                    // println 'delete ocorrencia' + fo.id
                    fo.delete(flush: true)
                    // println 'ocorrencia excluida'
                    // println '/' * 100
                    // println ' '
                }

                // excluir ciclo_consulta_ficha
                it.delete(flush: true)
            }
            cicloConsulta.delete(flush:true)


            // alterar a situação das fichas que estavam em consulta
            if( fichasDoCicloExcluido ) {
                // excluir as colaborações ( ficha_colaboracao )
                // FichaColaboracao.findAllByConsultaFicha()

                // excluir as ocorrências colaboradas ( ficha_ocorrencia_consulta )

                // excluir as fichas da consulta
                // fichaService.executeSql("delete from salve.ciclo_consulta_ficha where sq_ciclo_consulta=${cicloConsulta.id.toString()} and sq_ficha in (${fichasDoCicloExcluido.join(',')})")
                // excluir a consulta
                //cicloConsulta.delete(flush: true)
                // atualizar a situação das fichas
                fichasDoCicloExcluido.each { idFicha ->
                    VwFicha vwFicha = VwFicha.get(idFicha)
                    if (vwFicha.sqSituacaoFicha == situacaoConsulta.id) {
                        // colocar em compilacao, consulta ou consulta finalizada caso alguma ainda esteja em outra consulta
                        Date maxDate = CicloConsultaFicha.createCriteria().get {
                            createAlias('cicloConsulta', 'cc')
                            eq('vwFicha', vwFicha)
                            projections {
                                max('cc.dtFim')
                            }
                        }
                        if (!maxDate) {
                            fichaService.setSituacaoById(idFicha, situacaoCompilacao.id)
                        }else if (maxDate <= Util.hoje()) {
                            fichaService.setSituacaoById(idFicha, situacaoConsultaFinalizada.id)
                        }else {
                            fichaService.setSituacaoById(idFicha, situacaoConsulta.id)
                        }
                    }
                }
            }
        }
        render res as JSON
    }

    /**
     * ABA CONVIDAR
     *
     */

    def convidar() {
        // ler consultas diretas cadastradas
        //CicloAvaliacao cicloAvaliacao = CicloAvaliacao.get(params?.sqCicloAvaliacao)
        DadosApoio consultaDireta = dadosApoioService.getByCodigo('TB_TIPO_CONSULTA_FICHA', 'CONSULTA_DIRETA')
        DadosApoio consultaRevisao= dadosApoioService.getByCodigo('TB_TIPO_CONSULTA_FICHA', 'REVISAO_POS_OFICINA')
        //boolean canModify = this.isLastCicle(cicloAvaliacao.id.toInteger() )
        boolean canModify = true

        // lista consultas do ciclo
        Map parametros = [sqCicloAvaliacao:params.sqCicloAvaliacao.toLong()]
        if( ! session.sicae.user.isADM() )
        {
            DadosApoio papel
            // SE O USUÁRIO NÃO TIVER UNIDADE, FAZER FILTRO PELA FUNÇÃO ATRIBUIDA NAS FICHAS
            if( ! session?.sicae?.user?.sqUnidadeOrg  )
            {
                papel = dadosApoioService.getByCodigo('TB_PAPEL_FICHA',( session.sicae.user.isCT() ? 'COORDENADOR_TAXON' : 'COLABORADOR_EXTERNO') )
                if( !papel )
                {
                    println 'Perfil ' + ( session.sicae.user.isCT() ? 'COORDENADOR_TAXON' : 'COLABORADOR_EXTERNO')+ ' não cadastrado na tabela TB_PAPEL_FICHA'
                }
                parametros.inAtivo  = 'S'
                parametros.sqPessoa = session?.sicae?.user?.sqPessoa?.toLong()
                parametros.sqPapel  = papel?.id?.toLong()

            }
            else if( session?.sicae?.user?.sqUnidadeOrg ) {
                parametros.sqUnidadeOrg = session.sicae.user.sqUnidadeOrg.toLong()
            }
            else
            {
                render ''
                return
            }
        }
        if( params.sqUnidadeOrg )
        {
            parametros.sqUnidadeOrg = params.sqUnidadeOrg.toLong()
        }
        List listConsultasDiretas = CicloConsulta.executeQuery("""
        select distinct new map( a.id as id
        ,a.dtInicio as dtInicio
        ,a.dtFim as dtFim
        ,a.deTituloConsulta as deTituloConsulta
        ,d.sgUnidade as sgUnidadeOrg
        ,b.descricao as dsTipoConsulta
        ,b.codigoSistema as codigoSistema)
        from CicloConsulta a
        inner join a.tipoConsulta b
        inner join a.unidade d
        left join a.fichas c
        where a.cicloAvaliacao.id = :sqCicloAvaliacao
        and a.tipoConsulta in (${consultaDireta.id},${consultaRevisao.id} )
        ${parametros.sqUnidadeOrg ? 'and a.unidade.id = :sqUnidadeOrg':''}
        ${parametros.inAtivo ? '\nand c.ficha.id in ( select fp.ficha.id from FichaPessoa fp where fp.pessoa.id = :sqPessoa and fp.papel.id = :sqPapel and fp.inAtivo = :inAtivo )' :''}
        order by a.dtInicio desc
        """, parametros)

        List listNivelTaxonomico  = NivelTaxonomico.list()

        render('template': 'formConvidar', model: [listConsultasDiretas : listConsultasDiretas
                                                   , listNivelTaxonomico: listNivelTaxonomico
                                                   , emailUsuario       : session.sicae.user.email
                                                   , textoEmailPadrao   : getTextoEmailPadrao()
                                                   , canModify:canModify])
    }

    def getGridFichasConvidar(){
        if (params.sqCicloConsulta && params.sqCicloAvaliacao)
        {
            // filtrar a fichas
            params.sqCicloConsultaFiltro = params.sqCicloConsulta
            params.count = params.count ?:0
            List fichas = fichaService.search(params)
            if( ! fichas && params.count.toInteger() > 0 )
            {
               render '<h3>Nenhuma ficha encontrada!</h3>'
               return
            }
            // filtrar as fichas que estão no ciclo consulta selecionado
            List listCicloConsultaFichas = CicloConsultaFicha.createCriteria().list {
                eq('cicloConsulta.id',params.sqCicloConsulta.toLong() )

                if( fichas )
                {
                    'in'('ficha.id', fichas.collect{ it.sq_ficha.toLong() } )
                }
                ficha {
                    order('nmCientifico')
                }
            }
            render(template: "divGridFichaConvidar", model: [listCicloConsultaFichas: listCicloConsultaFichas])
        }
        render ''
    }

    def saveConvidado()
    {
        Map res = [status: 1, msg: '', type: 'error']
        List ids = params.ids.split(',')
        DadosApoio situacaoInicial = dadosApoioService.getByCodigo('TB_SITUACAO_CONVITE_PESSOA', 'EMAIL_NAO_ENVIADO')
        if (!situacaoInicial)
        {
            res.msg = 'Código EMAIL_NAO_ENVIADO não cadastrado na tabela de apoio TB_SITUACAO_CONVITE_PESSOA'
        } else
        {
            CicloConsulta cicloConsulta = CicloConsulta.get(params.sqCicloConsultaConvidar)
            if (cicloConsulta)
            {
                CicloConsultaPessoa cicloConsultaPessoa
                CicloConsultaFicha cicloConsultaFicha
                CicloConsultaPessoaFicha cicloConsultaPessoaFicha
                PessoaFisica pessoa = PessoaFisica.get(params.sqPessoaConvidada)
                if (pessoa) {
                    cicloConsultaPessoa = CicloConsultaPessoa.findByCicloConsultaAndPessoa(cicloConsulta, pessoa)

                    if (!cicloConsultaPessoa)
                    {
                        cicloConsultaPessoa = new CicloConsultaPessoa()
                        cicloConsultaPessoa.cicloConsulta = cicloConsulta
                        cicloConsultaPessoa.pessoa = pessoa
                        cicloConsultaPessoa.situacao = situacaoInicial
                    }
                    cicloConsultaPessoa.deEmail = params.deEmail
                    cicloConsultaPessoa.deObservacao = params.deObservacao

                    if( params.sqInstituicao ) {
                        if( ! cicloConsultaPessoa.instituicao || cicloConsultaPessoa.instituicao.id.toLong() != params.sqInstituicao.toLong() ){
                            cicloConsultaPessoa.instituicao = Instituicao.get( params.sqInstituicao.toLong() )
                            // atualizar instituição da pessoa em todas as outras consultas/revisões que estiverem sem preenchimento
                            sqlService.execSql("""update salve.ciclo_consulta_pessoa set sq_instituicao = ${params.sqInstituicao.toString() } where sq_pessoa = ${pessoa.id.toString() } and sq_instituicao is null""")
                        }
                    } else {
                        cicloConsultaPessoa.instituicao = null
                    }

                    if (cicloConsultaPessoa.save(flush: true))
                    {
                        res.msg = getMsg(1)
                        // gravar as fichas
                        List fichasAtuais = CicloConsultaPessoaFicha.findAllByCicloConsultaPessoa(cicloConsultaPessoa)
                        ids.each {
                            cicloConsultaFicha = CicloConsultaFicha.get(it)
                            if ( ! fichasAtuais.find {
                                if (it.cicloConsultaFicha == cicloConsultaFicha)
                                {
                                    return true
                                }
                            })
                            {
                                cicloConsultaPessoaFicha = new CicloConsultaPessoaFicha()
                                cicloConsultaPessoaFicha.cicloConsultaPessoa = cicloConsultaPessoa
                                cicloConsultaPessoaFicha.cicloConsultaFicha = cicloConsultaFicha
                                cicloConsultaPessoaFicha.save(flush: true)
                            }
                        }

                        /*List fichasAtuais = CicloConsultaPessoaFicha.findAllByCicloConsultaPessoa(cicloConsultaPessoa)
                        fichasAtuais.each { it.deleted = true } // marcar todas como excluídas
                        ids.each {
                            cicloConsultaFicha = CicloConsultaFicha.get(it)
                            if (!fichasAtuais.find {
                                if (it.cicloConsultaFicha == cicloConsultaFicha)
                                {
                                    it.deleted = false; return true
                                }
                            })
                            {
                                cicloConsultaPessoaFicha = new CicloConsultaPessoaFicha()
                                cicloConsultaPessoaFicha.cicloConsultaPessoa = cicloConsultaPessoa
                                cicloConsultaPessoaFicha.cicloConsultaFicha = cicloConsultaFicha
                                cicloConsultaPessoaFicha.save(flush: true)
                            }
                        }
                        // excluir as fichas que ficaram marcadas como excluidas
                        fichasAtuais.each {
                            if (it.deleted)
                            {
                                it.delete(flush: true)
                            }
                        }*/
                        res.type = 'success'
                        res.status = 0
                        res.msg = 'Convite gravado com SUCESSO!'
                    } else
                    {
                        println cicloConsultaPessoa.getErrors()
                        res.msg = 'Erro de inconsistênia encontrado'
                    }
                } else
                {
                    res.msg = 'Pessoa não encontrada!'
                }
            }
        }
        render res as JSON
    }

    def enviarEmailConvidados()
    {
        if (!params.txEmail)
        {
            render 'Texto email não informado'
            return
        }
        if (!params.deAssuntoEmail)
        {
            render 'Assunto email não informado'
            return
        }
        PessoaFisica pf = PessoaFisica.get(session.sicae.user.sqPessoa)
        String emailFrom = pf.email
        if (!emailFrom)
        {
            render 'Atenção,<br><b>' + pf.noPessoa + '</b> não possui email cadastrado no SICA-E.<br>Necessário atualizar o cadastro no ICMBio!'
            return
        }
        DadosApoio situacaoEnviado = dadosApoioService.getByCodigo('TB_SITUACAO_CONVITE_PESSOA', 'EMAIL_ENVIADO')
        if (!situacaoEnviado)
        {
            render 'Situção EMAIL_ENVIADO não cadastrada na tabela de apoio TB_SITUACAO_CONVITE_PESSOA'
            return
        }

        String dataFinal = ''

        // atualizar campo email da tabela ciclo_consulta
        if (params.sqCicloConsultaConvidar)
        {
            CicloConsulta cicloConsulta = CicloConsulta.get(params.sqCicloConsultaConvidar.toInteger())
            if (cicloConsulta)
            {
                cicloConsulta.txEmail = 'Assunto:' + params.deAssuntoEmail + "\nCc:" + params.deEmailCopia + "\n\n" + params.txEmail.trim()
                cicloConsulta.save(flush: true)
                dataFinal = cicloConsulta?.dtFim?.format('dd/MMM/yyyy')
            }
        }

        List ids = params.ids.split(',')
        CicloConsultaPessoa cicloConsultaPessoa
        String email
        String textoEmailCompleto
        List erros = []
        ids.each {
            cicloConsultaPessoa = CicloConsultaPessoa.get(it)
            if (cicloConsultaPessoa)
            {
                email = cicloConsultaPessoa.deEmail
                if (email)
                {
                    textoEmailCompleto = params.txEmail
                    if (textoEmailCompleto.indexOf('{fichas}') == -1)
                    {
                        textoEmailCompleto += '<br>{fichas}'
                    }
                    textoEmailCompleto = textoEmailCompleto.replaceAll(/\{nome\}/, cicloConsultaPessoa.pessoa.noPessoa)
                    textoEmailCompleto = textoEmailCompleto.replaceAll(/\{fichas\}/, cicloConsultaPessoa.fichasEmail)
                    textoEmailCompleto = textoEmailCompleto.replaceAll(/\{linkSalveConsulta\}/, '<a target="_blank" href="'+grailsApplication.config.url.salve.consulta+'">Clique aqui para acessar a consulta</a>')
                    textoEmailCompleto = textoEmailCompleto.replaceAll(/\{dataFinal\}/, dataFinal)
                    textoEmailCompleto = textoEmailCompleto.replaceAll(/\{emailConvidado\}/, email)
                    textoEmailCompleto = textoEmailCompleto.replaceAll(/\{usuario\}/, session?.sicae?.user?.noUsuario)
                    textoEmailCompleto = textoEmailCompleto.replaceAll(/\{unidade\}/, session?.sicae?.user?.noUnidadeOrg)
                    params.deEmailCopia = params.deEmailCopia ?: ''
                    if (emailService.sendTo(email, params.deAssuntoEmail, textoEmailCompleto, emailFrom, [:], params.deEmailCopia ) )
                    {
                        cicloConsultaPessoa.situacao = situacaoEnviado
                        /*if (params.deEmailCopia)
                        {
                            params.deEmailCopia.split(',').each {
                                emailService.sendTo(it.trim(), 'Cópia - ' + params.deAssuntoEmail, textoEmailCompleto)
                            }
                        }
                        */
                    } else
                    {
                        println 'Erro envio de email para ' + email
                        erros.push('Erro envio de email para ' + email)
                    }
                    cicloConsultaPessoa.dtEnvio = new Date()
                    // adicionar as espécies no texto do email
                    cicloConsultaPessoa.deEmail = email
                    if (!cicloConsultaPessoa.save(flush: true))
                    {
                        erros.push('Erro inconsisteência ao salvar ' + email)
                        cicloConsultaPessoa.errors.allErrors.each {
                            erros.push(messageSource.getMessage(it, locale))
                        }
                    }
                } else
                {
                    println 'Email de ' + cicloConsultaPessoa.pessoa.noPessoa + ' nao informado'
                    erros.push('Email de ' + cicloConsultaPessoa.pessoa.noPessoa + ' nao informado')
                }
            } else
            {
                println 'Ciclo consulta ' + it + ' não cadastrado!'
                erros.push('Ciclo consulta ' + it + ' não cadastrado!')
            }
        }
        String plural = (ids.size() > 1 ? 's' : '')
        if (erros.size() == 0)
        {
            //render 'Email' + plural + ' enviado' + plural + ' com SUCESSO!'
            render ''
        } else
        {
            render erros.join('<br>')
        }
    }

    def getGridConvidadosConvidar()
    {
        if (params.sqCicloConsulta)
        {
            CicloConsulta cicloConsulta = CicloConsulta.get( params.sqCicloConsulta.toInteger() )
            boolean  canModify = ( this.isLastCicle( cicloConsulta?.cicloAvaliacao?.id?.toInteger() )
                    && cicloConsulta.dtFim >= Util.hoje() )

            List listCicloConsultaPessoas = CicloConsultaPessoa.findAllByCicloConsulta(cicloConsulta
                ,[sort:'pessoa.pessoa.noPessoa'] // Alias: pessoa.pessoa.noPessoa == pessoaFisica.pessoa.noPessoa
            )
            List listSituacoesConvite = dadosApoioService.getTable('TB_SITUACAO_CONVITE_PESSOA').findAll{
                it.codigoSistema ==~ /EMAIL_ENVIADO|EMAIL_NAO_ENVIADO/
            }

            // colaboradores ou usuários externos não podem convidar
            if( session?.sicae?.user?.isCL() || ! session?.sicae?.user?.sqUnidadeOrg )
            {
                canModify=false
            }
            render(template: "divGridConvidadosConvidar", model: [listCicloConsultaPessoas: listCicloConsultaPessoas
                                                                  , listSituacoesConvite  : listSituacoesConvite
                                                                  ,canModify:canModify])
        }
        render ''
    }

    /**
     * listar as fichas atribuidas ao convidado da consulta direta
     * @return
     */
    def getListaFichasConvidado()
    {
        if( !params.id || !params.rowNum )
        {
            render 'Parâmetros insuficientes.'
            return
        }
        boolean canModify=true
        List lista = CicloConsultaPessoaFicha.executeQuery("""
        select new map( a.id as id, ficha.nmCientifico as descricao)
        from CicloConsultaPessoaFicha a
        inner join a.cicloConsultaFicha b
        inner join b.vwFicha ficha
        where a.cicloConsultaPessoa.id = :sqCicloConsultaPessoa
        order by ficha.nmCientifico
        """,[sqCicloConsultaPessoa:params.id.toLong() ]).each {
            it.descricao = Util.ncItalico( it.descricao,true )
        }
        render(template: "/templates/tableDeleteItem", model: [lista: lista, id:params.id, rowNum:params.rowNum, callback:'gerenciarConsulta.deleteFichaConvidado',canModify:canModify] )
    }

    def deleteFichaConsultaConvidado()
    {
        Map res = [status: 1, msg: '', errors: [], type: 'error']
        if (params.values) {
            //List ids = params.values.split(',').collect { it.toLong() }
            CicloConsultaPessoaFicha.executeUpdate("delete CicloConsultaPessoaFicha a where a.id in (${params.values})")
            res.status=0
            res.msg=''
        }
        render res as JSON
    }

    def getTextLastEmail()
    {
        if (params.sqCicloConsulta)
        {
            render CicloConsulta.get(params.sqCicloConsulta.toInteger())?.txEmail ?: ''
        } else
        {
            render ''
        }
    }

    def editConvidado()
    {
        if (params.sqCicloConsultaPessoa)
        {
            render CicloConsultaPessoa.get(params.sqCicloConsultaPessoa.toInteger()).asJson()
        }
        render [:] as JSON
    }

    def deleteConvidado()
    {
        if (params.id)
        {
            CicloConsultaPessoa.get(params.id.toInteger()).delete(flush: true)
        }
        render ''
    }

    def changeSituacaoConvite()
    {
        if (params.id && params.sqSituacao)
        {
            CicloConsultaPessoa reg = CicloConsultaPessoa.get(params.id.toInteger())
            if (reg)
            {
                reg.situacao = DadosApoio.get(params.sqSituacao.toInteger())
                reg.save(flush: true)
            }
        }
        render ''
    }

    /**
     * ABA CONSOLIDAR
     *
     */

    def consolidar() {
        // consultas
        CicloAvaliacao cicloAvaliacao = CicloAvaliacao.get( params.sqCicloAvaliacao.toInteger() )
        List listSituacaoFicha = fichaService.getListSituacao()

        /*
        String cmdSql ="""SELECT DISTINCT ciclo_consulta.sq_ciclo_consulta
                    ,ciclo_consulta.de_titulo_consulta
                    ,tipo.ds_dados_apoio as de_tipo_consulta
                    ,tipo.cd_sistema
                    ,ciclo_consulta.dt_inicio
                    ,ciclo_consulta.dt_fim
                    """
        if( session.sicae.user.isADM()) {
            cmdSql += """\nFROM salve.ciclo_consulta
                    inner join salve.dados_apoio as tipo on tipo.sq_dados_apoio = ciclo_consulta.sq_tipo_consulta
                    where ciclo_consulta.sq_ciclo_avaliacao = ${params.sqCicloAvaliacao}
                    """
            //listCicloConsulta = CicloConsulta.findAllByCicloAvaliacao(cicloAvaliacao).sort { a, b -> b.dtInicio <=> a.dtInicio }
        } else {
            if( session.sicae.user.sqUnidadeOrg ) {
                //Unidade unidade = Unidade.get(session.sicae.user.sqUnidadeOrg.toLong())
                //listCicloConsulta = CicloConsulta.findAllByCicloAvaliacaoAndUnidade(cicloAvaliacao, unidade).sort { a, b -> b.dtInicio <=> a.dtInicio }
                cmdSql += """\nFROM salve.ciclo_consulta
                    inner join salve.dados_apoio as tipo on tipo.sq_dados_apoio = ciclo_consulta.sq_tipo_consulta
                    where ciclo_consulta.sq_ciclo_avaliacao = ${params.sqCicloAvaliacao}
                    and ciclo_consulta.sq_unidade_org = ${session.sicae.user.sqUnidadeOrg.toString()}
                    """
            } else {
                // encontrar as oficinas pelo sq_pessoa
                DadosApoio papelCT = dadosApoioService.getByCodigo('TB_PAPEL_FICHA','COORDENADOR_TAXON')
                cmdSql += """\nFROM salve.ciclo_consulta_ficha
                            INNER JOIN salve.ciclo_consulta ON ciclo_consulta.sq_ciclo_consulta = ciclo_consulta_ficha.sq_ciclo_consulta
                            inner join salve.ficha_pessoa on ficha_pessoa.sq_ficha = ciclo_consulta_ficha.sq_ficha
                            inner join salve.ficha on ficha.sq_ficha = ciclo_consulta_ficha.sq_ficha
                            inner join salve.dados_apoio as tipo on tipo.sq_dados_apoio = ciclo_consulta.sq_tipo_consulta
                            where ficha_pessoa.sq_papel = ${papelCT.id.toString()}
                            and ficha_pessoa.sq_pessoa =  ${session.sicae.user.sqPessoa.toString()}
                            AND ficha.sq_ciclo_avaliacao = ${params.sqCicloAvaliacao.toString()}
                            """
            }
        }
        cmdSql +="\nORDER BY ciclo_consulta.dt_inicio DESC, tipo.ds_dados_apoio"
        List listCicloConsulta = sqlService.execSql(cmdSql)
        */

        // lista consultas do ciclo
        Map parametros = [sqCicloAvaliacao:params.sqCicloAvaliacao.toLong()]
        if( ! session.sicae.user.isADM() )
        {
            DadosApoio papel
            // SE O USUÁRIO NÃO TIVER UNIDADE, FAZER FILTRO PELA FUNÇÃO ATRIBUIDA NAS FICHAS
            if( ! session?.sicae?.user?.sqUnidadeOrg  )
            {
                papel = dadosApoioService.getByCodigo('TB_PAPEL_FICHA',( session.sicae.user.isCT() ? 'COORDENADOR_TAXON' : 'COLABORADOR_EXTERNO') )
                if( !papel )
                {
                    println 'Perfil ' + ( session.sicae.user.isCT() ? 'COORDENADOR_TAXON' : 'COLABORADOR_EXTERNO')+ ' não cadastrado na tabela TB_PAPEL_FICHA'
                }
                parametros.inAtivo  = 'S'
                parametros.sqPessoa = session?.sicae?.user?.sqPessoa?.toLong()
                parametros.sqPapel  = papel?.id?.toLong()

            }
            else if( session?.sicae?.user?.sqUnidadeOrg ) {
                parametros.sqUnidadeOrg = session.sicae.user.sqUnidadeOrg.toLong()
            }
            else
            {
                render ''
                return
            }
        }
        if( params.sqUnidadeOrg )
        {
            parametros.sqUnidadeOrg = params.sqUnidadeOrg.toLong()
        }
        List listCicloConsulta = CicloConsulta.executeQuery("""
        select distinct new map( a.id as id
        ,a.dtInicio as dtInicio
        ,a.dtFim as dtFim
        ,a.deTituloConsulta as deTituloConsulta
        ,d.sgUnidade as sgUnidadeOrg
        ,b.descricao as dsTipoConsulta
        ,b.codigoSistema as codigoSistema)
        from CicloConsulta a
        inner join a.tipoConsulta b
        inner join a.unidade d
        left join a.fichas c
        where a.cicloAvaliacao.id = :sqCicloAvaliacao
        ${parametros.sqUnidadeOrg ? 'and a.unidade.id = :sqUnidadeOrg':''}
        ${parametros.inAtivo ? '\nand c.ficha.id in ( select fp.ficha.id from FichaPessoa fp where fp.pessoa.id = :sqPessoa and fp.papel.id = :sqPapel and fp.inAtivo = :inAtivo )' :''}
        order by a.dtInicio desc
        """, parametros)
        List listNivelTaxonomico = NivelTaxonomico.list()
        List listCategorias = dadosApoioService.getTable('TB_CATEGORIA_IUCN').findAll{ it.codigoSistema != 'OUTRA' }.sort{ it.ordem }
        List listGrupos             = dadosApoioService.getTable('TB_GRUPO')
        List listSubgrupos          = dadosApoioService.getTable('TB_SUBGRUPO')
        List listConsultasAbertas = listCicloConsulta.findAll{
                return it.dtFim >= Util.hoje()
        }

        render(template: 'formConsolidar', model: [listCicloConsulta: listCicloConsulta
                                                   , listNivelTaxonomico: listNivelTaxonomico
                                                   , listCategorias: listCategorias
                                                   , listGrupos            : listGrupos
                                                   , listSubgrupos         : listSubgrupos
                                                   , listConsultasAbertas:listConsultasAbertas
                                                   , listSituacaoFicha: listSituacaoFicha])
    }

    def getGridConsolidar() {
        try {
            List wherePasso1 = []
            List whereTodosPassos = []
            List whereFinal = []
            List innerJoinsPasso1 = []
            Map sqlParams = [:]
            String qryOrderBy = 'passo1.nm_cientifico '

            // para criar o gride da consolidação das fichas é necessário informar uma consulta/revisão ou uma espécie
            if (!params.sqCicloConsultaFiltro && !params.nmCientificoFiltro) {
                throw new Exception('Necessário informar uma consulta/revisão ou o nome de uma espécie')
            }
            // necessário informa o ciclo de avalição
            if (!params.sqCicloAvaliacao) {
                throw new Exception('Necessário informar o ciclo de avaliação')
            }

            // filtrar pela unidade do usuário
            if( !params.sqUnidadeFiltro && ! session.sicae.user.isADM() ) {
                if (session?.sicae?.user?.sqUnidadeOrg) {
                    params.sqUnidadeFiltro = session.sicae.user.sqUnidadeOrg.toLong()
                } else {
                    // coordenador de taxon só pode visualizar as fichas atribuidas a ele
                    if( session.sicae.user.isCT() ){
                        wherePasso1.push("""ficha.sq_ficha in (select a.sq_ficha from salve.ficha_pessoa a
                                inner join salve.dados_apoio as papel on papel.sq_dados_apoio = a.sq_papel
                                where a.sq_pessoa = ${session.sicae.user.sqPessoa}
                                  and papel.cd_sistema = 'COORDENADOR_TAXON'
                                  and a.in_ativo = 'S'
                                )""")
                    } else {
                        render 'Sem acesso a este módulo'
                        return
                    }
                }
            }

            // FILTRO POR UNIDADE ORGANIZACIONAL
            if( params.sqUnidadeFiltro ) {
                wherePasso1.push('ficha.sq_unidade_org = :sqUnidadeOrg')
                sqlParams.sqUnidadeOrg = params.sqUnidadeFiltro.toLong()
            }

            wherePasso1.push('ficha.sq_ciclo_avaliacao=:sqCicloAvaliacao')
            sqlParams.sqCicloAvaliacao = params.sqCicloAvaliacao.toLong()

            // filtrar pela consulta/revisao
            if (params.sqCicloConsultaFiltro) {
                wherePasso1.push('ciclo_consulta.sq_ciclo_consulta = :sqCicloConsulta')
                sqlParams.sqCicloConsulta = params.sqCicloConsultaFiltro.toLong()
                // Quando for selecionada uma REVISAO, mostrar somente as colaboracoes dela feitas na revisao
                // Quando for Ampla ou Direta, mostrar todas as colaborações juntas
                CicloConsulta.executeQuery("""select new map( tipo.codigoSistema as codigoSistema) from CicloConsulta cc
                    inner join cc.tipoConsulta as tipo
                    where cc.id = :sqCicloConsulta""",[sqCicloConsulta:sqlParams.sqCicloConsulta]).each {
                    if( it.codigoSistema =~ /REVISAO/ ) {
                        whereTodosPassos.push('ciclo_consulta.sq_ciclo_consulta = :sqCicloConsulta')
                    } else {
                        // SALVE SEM CICLO
                        whereTodosPassos.add("""(ccfv.sq_ficha_versao = passo1.sq_ficha_versao or (ccfv.sq_ficha_versao is null and passo1.sq_ficha_versao is null))""") // SALVE SEM CICLO
                    }
                }
            }

            // FILTROS
            if (params.sqFicha) {
                // filtrar pelo id da ficha e ignorar os demais filtros
                wherePasso1.push('ficha.sq_ficha = :sqFicha')
                sqlParams.sqFicha = params.sqFicha.toLong()
            } else if( params.nmCientificoFiltro ) {
                // filtrar pelo nome da especie e ignorar todos os demais filtro
                wherePasso1.push('ficha.nm_cientifico ilike :nmCientifico')
                sqlParams.nmCientifico=params.nmCientificoFiltro+'%'
            } else {
                // outros filtros que possam existir
                if( params.nmComumFiltro ) {

                    wherePasso1.push("""exists ( select x.sq_ficha_nome_comum from salve.ficha_nome_comum x where x.sq_ficha = ficha.sq_ficha and public.fn_remove_acentuacao(x.no_comum) ilike :noComum offset 0 limit 1)""")
                    sqlParams.noComum = '%'+Util.removeAccents(params.nmComumFiltro)+'%'
                }

                // NIVEL TAXONOMICO
                if( params.sqTaxonFiltro && params.nivelFiltro){
                    // nivelFiltro = REINO
                    // sqTaxonFiltro = 1,2
                    innerJoinsPasso1.push("""inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon AND ( taxon.json_trilha -> '${params.nivelFiltro.toLowerCase()}' ->> 'sq_taxon')::integer in (${params.sqTaxonFiltro})""")
                }

                // COM / SEM PENDENCIA
                if( params.inPendenciaFiltro ){
                    whereTodosPassos.push("""situacao.cd_sistema = 'NAO_AVALIADA'""")
                    // COM PENDENCIA
                    if( params.inPendenciaFiltro.toInteger() == 2 ) {
                        whereFinal.push("""( passo3.json_colaboracoes is NOT null OR passo4.json_planilha is NOT null OR passo6.json_ocorrencias is NOT null )""")
                    }
                    // SEM  PENDENCIA
                    else if( params.inPendenciaFiltro.toInteger() == 1 ) {
                        whereFinal.push("""( passo3.json_colaboracoes is null AND passo4.json_planilha is null AND passo6.json_ocorrencias is null )""")
                    }
                }

                // SITUACAO DA FICHA
                if( params.sqSituacaoFiltro ){
                    //sqSituacaoFiltro = 3,1009
                    wherePasso1.push("""ficha.sq_situacao_ficha in (${params.sqSituacaoFiltro})""")
                }

                // CATEGORIA FICHA
                if( params.sqCategoriaFiltro ){
                    //sqSituacaoFiltro = 3,1009
                    //wherePasso1.push("""ficha.sq_situacao_ficha in (${params.sqSituacaoFiltro})""")
                    whereFinal.push("""categoria.sq_categoria in (${params.sqCategoriaFiltro})""")
                }

                // GRUPOS AVALIADOS
                if( params.sqGrupoFiltro ){
                    //sqGrupoFiltro = 1205,1206
                    wherePasso1.push("""ficha.sq_grupo in (${params.sqGrupoFiltro})""")
                    if( params.sqSubgrupoFiltro ) {
                        //sqSubgrupoFiltro = 1248,1247
                        wherePasso1.push("""ficha.sq_subgrupo in (${params.sqSubgrupoFiltro})""")
                    }
                }

                // NOME DO COLABORADOR
                if( params.noColaboradorFiltro ){
                    whereTodosPassos.add("""public.fn_remove_acentuacao(usr.no_pessoa) ilike :noColaborador""")
                    sqlParams.noColaborador = '%'+Util.removeAccents(params.noColaboradorFiltro)+'%'
                    whereFinal.push("""( passo3.json_colaboracoes is NOT null OR passo4.json_planilha is NOT null OR passo6.json_ocorrencias is NOT null )""")
                }

                // COM / SEM COLABORACAO
                if( params.inColaboracaoFiltro ){
                    if( params.inColaboracaoFiltro.toUpperCase() == 'S') {
                        whereFinal.push("""( passo3.json_colaboracoes is NOT null OR passo4.json_planilha is NOT null OR passo6.json_ocorrencias is NOT null )""")
                    }else if( params.inColaboracaoFiltro.toUpperCase() == 'N') {
                        whereFinal.push("""( passo3.json_colaboracoes is null and passo4.json_planilha is null AND passo6.json_ocorrencias is null )""")
                    }
                }
            }
            List listSituacaoFicha = fichaService.getListSituacao()
            CicloAvaliacao cicloAvaliacao = CicloAvaliacao.get(params.sqCicloAvaliacao)
            boolean canModify = true

            if (params.sortColumns) {
                List sortParams = params.sortColumns.split(':')
                qryOrderBy = sortParams[0] + ' ' + (sortParams[1] ?: '')
            }
            // REMOVER DUPLICIDADE DE FILTROS
            wherePasso1       = wherePasso1.unique{it.toLowerCase().replaceAll(/ /,'')}
            whereTodosPassos  = whereTodosPassos.unique{it.toLowerCase().replaceAll(/ /,'')}
            whereFinal        = whereFinal.unique{it.toLowerCase().replaceAll(/ /,'')}

            String cmdSql ="""with passo1 as (
                    -- todas as fichas que estão em consulta/revisao
                    select distinct a.sq_ficha
                    , ficha.nm_cientifico
                    , ficha.sq_situacao_ficha
                    , ficha.sq_taxon
                    , ciclo.nu_ano
                    , coalesce(ficha_versao.js_ficha->>'st_manter_lc'::text, ficha.st_manter_lc::text)::boolean as st_manter_lc
                    , ccfv.sq_ciclo_consulta_ficha
                    , ficha_versao.nu_versao
                    , ficha_versao.sq_ficha_versao
                    from salve.ciclo_consulta_ficha a
                    inner join salve.ciclo_consulta ciclo_consulta on ciclo_consulta.sq_ciclo_consulta = a.sq_ciclo_consulta
                    inner join salve.ficha on ficha.sq_ficha = a.sq_ficha
                    inner join salve.ciclo_avaliacao ciclo on ciclo.sq_ciclo_avaliacao = ficha.sq_ciclo_avaliacao
                    left join salve.ciclo_consulta_ficha_versao ccfv on ccfv.sq_ciclo_consulta_ficha = a.sq_ciclo_consulta_ficha
                    left join salve.ficha_versao on ficha_versao.sq_ficha_versao = ccfv.sq_ficha_versao
                    ${innerJoinsPasso1 ? innerJoinsPasso1.join('\n'):''}
                    where ${wherePasso1.join(' and ')}
                ),  passo2 as (
                    -- ler as colaboracoes na ficha vindas das consultas diretas e amplas e revisao
                    select passo1.sq_ficha
                         ,usr.no_pessoa as no_usuario
                         ,usr.sq_pessoa as sq_web_usuario
                         ,tipo.ds_dados_apoio as ds_tipo
                         ,sum(case when situacao.cd_sistema='NAO_AVALIADA' then 1 else 0 end) as nu_nao_avaliada
                    from passo1
                        left join salve.ciclo_consulta_ficha a on a.sq_ficha = passo1.sq_ficha
                        inner join salve.ficha_colaboracao as c on c.sq_ciclo_consulta_ficha = a.sq_ciclo_consulta_ficha
                        inner join salve.ciclo_consulta as ciclo_consulta on ciclo_consulta.sq_ciclo_consulta = a.sq_ciclo_consulta
                        left join salve.ciclo_consulta_ficha_versao ccfv on ccfv.sq_ciclo_consulta_ficha = a.sq_ciclo_consulta_ficha
                        left outer join salve.dados_apoio as situacao on situacao.sq_dados_apoio = c.sq_situacao
                        left outer join salve.dados_apoio as tipo on tipo.sq_dados_apoio = ciclo_consulta.sq_tipo_consulta
                        left outer join salve.pessoa as usr on usr.sq_pessoa = c.sq_web_usuario
                        ${whereTodosPassos ? 'where '+whereTodosPassos.join(' and '):''}
                    group by passo1.sq_ficha, usr.sq_pessoa, usr.no_pessoa, tipo.ds_dados_apoio
                ), passo3 as (
                    -- agregar dados do passo 2 por tipo de consulta
                    select passo2.sq_ficha
                         , json_object_agg( concat('rnd-',round(random()*10000000)::text), json_build_object('ds_tipo', passo2.ds_tipo
                        , 'no_usuario', passo2.no_usuario
                        , 'sq_web_usuario', passo2.sq_web_usuario
                        , 'nu_nao_avaliada', passo2.nu_nao_avaliada))::text as json_colaboracoes
                    from passo2
                    group by passo2.sq_ficha
                ), passo4 as (
                -- ler a colaboracao de planilha de registro
                    select passo1.sq_ficha, json_object_agg( concat('rnd-',round(random()*10000000)::text)
                                               , json_build_object('ds_tipo', tipo.ds_dados_apoio
                                               , 'sq_ficha_consulta_anexo', anexo.sq_ficha_consulta_anexo
                                               , 'no_arquivo', anexo.no_arquivo
                                               , 'de_local_arquivo', anexo.de_local_arquivo
                                               , 'no_usuario', usr.no_pessoa
                                               , 'nu_nao_avaliada', case
                                                                        when situacao.cd_sistema in( 'NAO_AVALIADA','NAO_ACEITA') then 1
                                                                        else 0 end))::text as json_planilha
                    from passo1
                             left join salve.ciclo_consulta_ficha a on a.sq_ficha = passo1.sq_ficha
                             inner join salve.ficha_consulta_anexo anexo on anexo.sq_ciclo_consulta_ficha = a.sq_ciclo_consulta_ficha
                             left outer join salve.ciclo_consulta as ciclo_consulta on ciclo_consulta.sq_ciclo_consulta = a.sq_ciclo_consulta
                             left join salve.ciclo_consulta_ficha_versao ccfv on ccfv.sq_ciclo_consulta_ficha = a.sq_ciclo_consulta_ficha
                             left outer join salve.dados_apoio as tipo on tipo.sq_dados_apoio = ciclo_consulta.sq_tipo_consulta
                             left join salve.pessoa as usr on usr.sq_pessoa = anexo.sq_web_usuario
                             left join salve.dados_apoio as situacao on situacao.sq_dados_apoio = anexo.sq_situacao
                             ${whereTodosPassos ? 'where '+whereTodosPassos.join(' and '):''}
                    group by passo1.sq_ficha
                ), passo5 as (
                    -- ler as colaboracoes de registros de ocorrencia
                    select passo1.sq_ficha
                         , usr.no_pessoa as no_usuario
                         , situacao.cd_sistema
                    from passo1
                         inner join salve.ficha_ocorrencia fo on fo.sq_ficha = passo1.sq_ficha and fo.sq_contexto = 1053
                         inner join salve.ficha_ocorrencia_consulta c on c.sq_ficha_ocorrencia = fo.sq_ficha_ocorrencia
                         left outer join salve.ciclo_consulta_ficha ccf on ccf.sq_ciclo_consulta_ficha = c.sq_ciclo_consulta_ficha
                         left outer join salve.ciclo_consulta_ficha_versao ccfv on ccfv.sq_ciclo_consulta_ficha = ccf.sq_ciclo_consulta_ficha
                         left outer join salve.ciclo_consulta as ciclo_consulta on ciclo_consulta.sq_ciclo_consulta = ccf.sq_ciclo_consulta
                         left outer join salve.dados_apoio as tipo on tipo.sq_dados_apoio = ciclo_consulta.sq_tipo_consulta
                         left outer join salve.dados_apoio as situacao on situacao.sq_dados_apoio = c.sq_situacao
                         left outer join salve.pessoa as usr on usr.sq_pessoa = c.sq_web_usuario
                    where (ccfv.sq_ficha_versao = passo1.sq_ficha_versao or (ccfv.sq_ficha_versao is null and passo1.sq_ficha_versao is null))

                    union all -- validações concordo/discordo

                    select fo.sq_ficha
                          ,usr.no_pessoa as no_usuario
                         , situacao.cd_sistema
                    from passo1
                             inner join salve.ficha_ocorrencia fo on fo.sq_ficha = passo1.sq_ficha
                             inner join salve.ficha_ocorrencia_validacao fov on fov.sq_ficha_ocorrencia = fo.sq_ficha_ocorrencia
                             left outer join salve.ciclo_consulta_ficha a on a.sq_ciclo_consulta_ficha = fov.sq_ciclo_consulta_ficha
                             left join salve.ciclo_consulta_ficha_versao as ccfv on ccfv.sq_ciclo_consulta_ficha = a.sq_ciclo_consulta_ficha
                             inner join salve.ciclo_consulta as ciclo_consulta on ciclo_consulta.sq_ciclo_consulta = a.sq_ciclo_consulta
                             left outer join salve.dados_apoio as tipo on tipo.sq_dados_apoio = ciclo_consulta.sq_tipo_consulta
                             left outer join salve.pessoa as usr on usr.sq_pessoa = fov.sq_web_usuario
                             left outer join salve.dados_apoio as situacao on situacao.sq_dados_apoio = fov.sq_situacao
                      where (ccfv.sq_ficha_versao = passo1.sq_ficha_versao or (ccfv.sq_ficha_versao is null and passo1.sq_ficha_versao is null))
                      and fov.sq_ficha_ocorrencia is not null

                    union all

                    select fo.sq_ficha
                         , usr.no_pessoa as no_usuario
                         -- tipo.ds_dados_apoio as ds_tipo
                         , situacao.cd_sistema
                    from passo1
                             inner join salve.ficha_ocorrencia_portalbio fo on fo.sq_ficha = passo1.sq_ficha
                             inner join salve.ficha_ocorrencia_validacao fov on fov.sq_ficha_ocorrencia_portalbio = fo.sq_ficha_ocorrencia_portalbio
                             left outer join salve.ciclo_consulta_ficha a on a.sq_ciclo_consulta_ficha = fov.sq_ciclo_consulta_ficha
                             left join salve.ciclo_consulta_ficha_versao as ccfv on ccfv.sq_ciclo_consulta_ficha = a.sq_ciclo_consulta_ficha
                             inner join salve.ciclo_consulta as ciclo_consulta on ciclo_consulta.sq_ciclo_consulta = a.sq_ciclo_consulta
                             left outer join salve.dados_apoio as tipo on tipo.sq_dados_apoio = ciclo_consulta.sq_tipo_consulta
                             left outer join salve.pessoa as usr on usr.sq_pessoa = fov.sq_web_usuario
                             left outer join salve.dados_apoio as situacao on situacao.sq_dados_apoio = fov.sq_situacao
                      where (ccfv.sq_ficha_versao = passo1.sq_ficha_versao or (ccfv.sq_ficha_versao is null and passo1.sq_ficha_versao is null))
                      and fov.sq_ficha_ocorrencia_portalbio is not null
                ), passo51 as (
                    -- agrupar e totalizar o passo2
                    select passo5.sq_ficha
                         ,passo5.no_usuario
                         ,sum(case when passo5.cd_sistema = 'NAO_AVALIADA' then 1 else 0 end)  as nu_nao_avaliada
                         ,sum(case when passo5.cd_sistema <> 'NAO_AVALIADA' then 1 else 0 end) as nu_avaliada
                    from passo5
                    group by passo5.sq_ficha, passo5.no_usuario
                ), passo6 as (
                -- agregar passo 51
                    select passo51.sq_ficha, json_object_agg(concat('rnd-',round(random()*10000000)::text),
                                                            json_build_object(
                                                                 'no_usuario',passo51.no_usuario
                                                                , 'nu_nao_avaliada',passo51.nu_nao_avaliada
                                                                , 'nu_avaliada', passo51.nu_avaliada) )::text as json_ocorrencias
                    from passo51
                    group by passo51.sq_ficha
                )
                -- montar a query final
                select passo1.sq_ficha
                     , passo1.sq_situacao_ficha
                     , passo1.sq_ficha_versao
                     , passo1.sq_ciclo_consulta_ficha
                     , passo1.nu_versao
                     , passo1.nm_cientifico
                     , passo1.st_manter_lc
                     , passo3.json_colaboracoes
                     , passo4.json_planilha
                     , passo6.json_ocorrencias
                     , categoria.cd_categoria_sistema
                     , categoria.ds_categoria_vigente
                     , situacao_ficha.cd_sistema as cd_situacao_ficha_sistema
                     , situacao_ficha.ds_dados_apoio as ds_situacao_ficha
                     , case when passo3.json_colaboracoes is not null or
                            passo4.json_planilha is not null or
                            passo6.json_ocorrencias is not null then true else false end as in_colaboracao
                from passo1
                inner join salve.dados_apoio as situacao_ficha on situacao_ficha.sq_dados_apoio = passo1.sq_situacao_ficha
                left join passo3 on passo3.sq_ficha = passo1.sq_ficha
                left join passo4 on passo4.sq_ficha = passo1.sq_ficha
                left join passo6 on passo6.sq_ficha = passo1.sq_ficha
                -- CATEGORIA HIST
                left join lateral (
                    select categoria.sq_dados_apoio as sq_categoria
                         , categoria.cd_sistema as cd_categoria_sistema
                         , concat(categoria.ds_dados_apoio, ' (', categoria.cd_dados_apoio, ')') as ds_categoria_vigente
                    from salve.taxon_historico_avaliacao a
                    inner join salve.dados_apoio categoria on categoria.sq_dados_apoio = a.sq_categoria_iucn
                    inner join salve.dados_apoio as tipo on tipo.sq_dados_apoio = a.sq_tipo_avaliacao
                    left join salve.taxon_historico_avaliacao_versao thv on thv.sq_taxon_historico_avaliacao = a.sq_taxon_historico_avaliacao
                    where a.sq_taxon = passo1.sq_taxon
                      and tipo.cd_sistema = 'NACIONAL_BRASIL'
                      and (thv.sq_ficha_versao = passo1.sq_ficha_versao or (thv.sq_ficha_versao is null and passo1.sq_ficha_versao is null))
                    ORDER BY a.nu_ano_avaliacao desc, a.sq_taxon_historico_avaliacao desc
                    OFFSET 0 LIMIT 1
                    ) as categoria on true
                    ${whereFinal ? 'where ' + whereFinal.join('\nand ') : ''}
                    order by ${qryOrderBy}"""
            /** /
            println ' '
            println ' '
            println ' '
            println cmdSql
            println sqlParams
            /**/
            Map paginate = sqlService.paginate( cmdSql, sqlParams, params,100,10,0)
            List listRows = paginate.rows
            Map pagination = paginate.pagination

            render(template: 'divGridConsolidar', model: [cicloAvaliacao:cicloAvaliacao
                                                          , listRows  : listRows
                                                          , pagination : pagination
                                                          , canModify : canModify
                                                          , listSituacaoFicha:listSituacaoFicha

            ] )

        } catch ( Exception e ){
            render e.getMessage()
        }

    }

    /*
    def getGridConsolidarOld() {
        params.contexto = 'consulta'
        if( !params.sqCicloAvaliacao )
        {
            render ''
            return
        }

        if( !params.sqCicloConsultaFiltro && !params.nmCientificoFiltro ){
            render ''
            return;
        }

        List listSituacaoFicha = fichaService.getListSituacao()
        CicloAvaliacao cicloAvaliacao = CicloAvaliacao.get(params.sqCicloAvaliacao)
        boolean canModify = this.isLastCicle( cicloAvaliacao.id.toInteger() )
        params.colColaboracao=true // adicionar as colaborações na lista de fichas
        params.colCategoria=true // adicionar os dados da categoria atual na lista de fichas
        params.paginationPageSize = 50 // fazer a paginação dos registros

        // SITUAÇÕES DAS FICHAS QUE NÃO DEVEM APARECER NO GRIDE CONSOLIDAR
        params.sqSituacaoNaoFiltro = listSituacaoFicha.findAll{
            it.codigoSistema ==~ /EXCLUIDA/
        }?.id

        // atualizar somente a linha editada
        if( params.sqFicha )
        {
            params.sqFichaFiltro = params.sqFicha
        }
        else if( params.idFicha )
        {
            params.sqFichaFiltro = params.idFicha
            params.paginationTotalRecords=null
            params.paginationCurrentPage=null
            params.paginationPageSize=null
            params.paginationTotalPages=null
        }
        List listRows = fichaService.search(params)

        render(template: 'divGridConsolidar', model: [cicloAvaliacao:cicloAvaliacao
                                                      , listRows: listRows
                                                      , canModify: canModify
                                                      , listSituacaoFicha:listSituacaoFicha
                                                      ] )
    }*/

    def getGridColaboracoes() {
        // ler a colaborações do usuario na ficha no cicloConsulta
        Usuario usr = Usuario.get(params.idUsuario)
        Ficha ficha = Ficha.get(params.idFicha)
        List listFichaColaboracao = FichaColaboracao.createCriteria().list {
            eq('usuario',usr)
            consultaFicha{
                eq('ficha',ficha)
            }
        }.findAll { row->
            return ( ! row.consultaFicha?.cicloConsultaFichaVersao?.id || row?.consultaFicha?.cicloConsultaFichaVersao?.fichaVersao?.id == params.sqFichaVersao.toLong() )
        }

        // colaboracao com registros de ocorrencias
        String cmdSql = """select count(c.sq_ciclo_consulta_ficha)::integer as nu_colaboracoes
                    from salve.ficha_ocorrencia_consulta c
                    inner join salve.ficha_ocorrencia fo on fo.sq_ficha_ocorrencia=c.sq_ficha_ocorrencia
                    left outer join salve.ciclo_consulta_ficha ccf on ccf.sq_ciclo_consulta_ficha = c.sq_ciclo_consulta_ficha
                    left outer join salve.ciclo_consulta_ficha_versao ccfv on ccfv.sq_ciclo_consulta_ficha = ccf.sq_ciclo_consulta_ficha
                    where fo.sq_ficha = ${params.idFicha}
                    and c.sq_web_usuario = ${usr.id}
                    and (ccfv.sq_ciclo_consulta_ficha is null ${params.sqFichaVersao ? ' or ccfv.sq_ficha_versao='+params.sqFichaVersao: ''})
                    """
        Integer qtdColaboracaoOcorrencia = 0
        sqlService.execSql(cmdSql).each{row->
            qtdColaboracaoOcorrencia = row.nu_colaboracoes.toInteger()
        }

        // colaboracao feita no campo select s/n do gride para dizer se o registro é válido ou não
        Integer qtdColaboracaoRegistros = 0

        cmdSql = """select count(a.*)::integer as nu_colaboracoes
                        from salve.ficha_ocorrencia_validacao a
                        left join salve.ficha_ocorrencia fo on fo.sq_ficha_ocorrencia=a.sq_ficha_ocorrencia
                        left join salve.ficha_ocorrencia_portalbio fo2 on fo2.sq_ficha_ocorrencia_portalbio = a.sq_ficha_ocorrencia_portalbio
                        left join salve.ficha_ocorrencia_validacao_versao versao on versao.sq_ficha_ocorrencia_validacao = a.sq_ficha_ocorrencia_validacao
                        left join salve.ciclo_consulta_ficha_versao ccfv on ccfv.sq_ciclo_consulta_ficha = a.sq_ciclo_consulta_ficha
                        where a.sq_web_usuario = ${usr.id}
                        and ( fo.sq_ficha = ${ficha.id} or fo2.sq_ficha=${ficha.id} )
                        and (ccfv.sq_ciclo_consulta_ficha is null ${params.sqFichaVersao ? ' or ccfv.sq_ficha_versao='+params.sqFichaVersao: ''})
                        """

        sqlService.execSql(cmdSql).each{ row->
            qtdColaboracaoRegistros = row.nu_colaboracoes.toInteger()
        }

        List listSituacaoColaboracao = dadosApoioService.getTable('TB_SITUACAO_COLABORACAO')
        //boolean canModify = ficha.cicloAvaliacao.isLastCicle()
        boolean canModify = true
        if( params.readonly == true ) {
            canModify = false
        }
        if( canModify && ! ( ficha.situacaoFicha.codigoSistema =~ /CONSULTA/) ){
            canModify = false
        }

        /** /
        println ' '
        println 'Usuario..: ' + usr.noUsuario
        println 'Situacao ficha:' + ficha.situacaoFicha.codigoSistema
        println 'CanModify: ' + ( canModify ? 'sim' : 'nao')
        /**/

        render(template: 'divGridColaboracao', model: [ficha  : ficha
                                                       , canModify : canModify
                                                       , usuario                : usr
                                                       , listFichaColaboracao   : listFichaColaboracao
                                                       , listSituacaoColaboracao: listSituacaoColaboracao
                                                       , qtdColaboracaoOcorrencia: qtdColaboracaoOcorrencia + qtdColaboracaoRegistros
                                                       ])

    }

    def changeManterLc() {
        if( ! params?.sqFicha )
        {
            render ''
            return
        }

        Ficha ficha = Ficha.get( params.sqFicha.toInteger() )
        if ( ficha )
        {
            // a ficha deve estar em consuta para poder mudar a situação manter/não manter LC
            if( ! (ficha.situacaoFicha.codigoSistema ==~ /CONSOLIDADA|CONSULTA|CONSULTA_FINALIZADA|AVALIADA_EM_REVISAO/) )
            {
                render 'Para alterar este campo a ficha deve está em Consulta ou Consulta finalizada ou Avaliada em revisão!'
                return
            }

            DadosApoio consulta = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'CONSULTA')
            if( params.stManterLc == 'S' ) {
                // se não tiver preenchida a categoria ou for NE ler do historico
                /*if( ! ficha.categoriaIucn || ficha.categoriaIucn.codigoSistema=='NE') {
                    // ler dados da última oficina e se não encontrar ler do histórico
                    List tipoOficinas = dadosApoioService.getTable('TB_TIPO_OFICINA').findAll{
                        return it.codigoSistema ==~ /OFICINA_VALIDACAO|OFICINA_AVALIACAO/
                    }
                    OficinaFicha oficinaFicha = OficinaFicha.createCriteria().get{
                        maxResults(1)
                        eq('vwFicha.id', ficha.id)
                        isNotNull('categoriaIucn')
                        oficina {
                            order('dtFim','desc')
                            'in'('tipoOficina', tipoOficinas)
                        }
                    }
                    Map dadosUltimaAvaliacaoNacional = fichaService.getUltimaAvaliacaoNacional(ficha.id)
                    if( oficinaFicha && oficinaFicha.categoriaIucn )
                    {
                        ficha.categoriaIucn          = oficinaFicha.categoriaIucn
                        ficha.dsJustificativa        = oficinaFicha.dsJustificativa ?: dadosUltimaAvaliacaoNacional.txJustificativaAvaliacao
                        ficha.dsCriterioAvalIucn     = oficinaFicha.dsCriterioAvalIucn ?: dadosUltimaAvaliacaoNacional.deCriterioAvaliacaoIucn
                        ficha.stPossivelmenteExtinta = oficinaFicha.stPossivelmenteExtinta ?: dadosUltimaAvaliacaoNacional.stPossivelmenteExtinta
                    }
                    else {
                        // definir/manter a categoria para LC e a justificativa da aba 10.6-Resultado
                        DadosApoio categoriaLc = dadosApoioService.getByCodigo('TB_CATEGORIA_IUCN', 'LC')
                        ficha.categoriaIucn = categoriaLc
                        if ( dadosUltimaAvaliacaoNacional.id ) {
                            ficha.dsJustificativa = dadosUltimaAvaliacaoNacional.txJustificativaAvaliacao
                        } else {
                            ficha.dsJustificativa = ''
                        }
                        ficha.stPossivelmenteExtinta = null
                        ficha.dsCriterioAvalIucn = null
                        //ficha.situacaoFicha = consulta // não precisa mais alterar a situação
                    }
                }*/
                ficha.stManterLc=true
                ficha.save(flush:true)
            } else  {
                // NAO MANTER LC
                // se a ficha estiver na situacao AVALIADA_EM_REVISAO não pode apagar a aba 11.6
                if( ficha.situacaoFicha.codigoSistema=='CONSULTA_FINALIZADA') {
                    // limpar aba 11.6-Avaliação
                    DadosApoio categoriaNE = dadosApoioService.getByCodigo('TB_CATEGORIA_IUCN', 'NE')
                    ficha.situacaoFicha = consulta
                    ficha.dsJustificativa = null
                    ficha.categoriaIucn = categoriaNE // NÃO AVALIADA
                    ficha.stPossivelmenteExtinta = null
                    ficha.dsCriterioAvalIucn = null
                }
                ficha.stManterLc = false
                // este campo é preenchido com false ao selecionar a ficha LC para Avaliar na oficina no gride da Avaliação
                ficha.save(flush:true)
            }
        }
        render ''
    }

    def updateFicha()
    {

        Map res = [status: 1, msg: '', errors: [], type: 'error']

        /*res.msg = 'ver parametros!'
        render res as JSON
        return
        */
        if (params.sqFicha && params.campoFicha)
        {
            Ficha ficha = Ficha.get(params.sqFicha)
            if (ficha)
            {
                try
                {
                    ficha[params.campoFicha] = params.dsTextoAlterado
                    ficha.save(flush: true)
                    res.status = 0
                    res.type = 'success'
                    res.msg = 'Gravado com SUCESSO!'
                } catch (Exception e)
                {
                    println e.getMessage()
                    res.status = 1
                    res.msg = 'Campo ' + params.campoFicha + ' inválido!'
                }
            }
        } else
        {

            if (!params.sqFichaColaboracao)
            {
                res.msg = 'Id não informado!'
                render res as JSON
                return
            }

            if (!params.dsTextoAlterado)
            {
                res.msg = 'Texto não informado!'
                render res as JSON
                return
            }


            FichaColaboracao fc = FichaColaboracao.get(params.sqFichaColaboracao.toInteger())
            if (!fc)
            {
                res.msg = 'Id inválido!'
                render res as JSON
                return

            }
            if (!session?.sicae?.user?.isADM() && !session?.sicae?.user?.isPF()){
                res.msg = 'Operação não permitida!'
            } else
            {
                res.status = 0
                res.type = 'success'
                res.msg = 'Dados gravados com SUCESSO!'
                try
                {
                    fc.consultaFicha.ficha[fc.noCampoFicha] = params.dsTextoAlterado
                    fc.consultaFicha.ficha.save(flush: true)
                }
                catch (Exception e)
                {
                    res.status = 1
                    res.type = 'error'
                    res.msg = 'Campo ' + fc.noCampoFicha + ' não existe!'
                }

            }
        }
        render res as JSON
    }

    def changeSituacaoColaboracaoPlanilha() {
        Map res = [status:1, msg:'',type:'error']
        try {
            if (!params.sqFichaConsultaAnexo) {
                render 'Id não informado!'
                return
            }
            DadosApoio situacao
            if( params.stRecebido=='S') {
                situacao = dadosApoioService.getByCodigo('TB_SITUACAO_COLABORACAO','ACEITA')
            } else {
                situacao = dadosApoioService.getByCodigo('TB_SITUACAO_COLABORACAO','NAO_ACEITA')
            }
            FichaConsultaAnexo fca = FichaConsultaAnexo.get( params.sqFichaConsultaAnexo.toLong() )
            if( fca ) {
                fca.situacao = situacao
                fca.save( flush:true )
            }
            res.status=0;
            res.msg='Gravação realizada com SUCESSO!'
            res.type='success'
        } catch (Exception e ) {
            res.msg = e.getMessage()
        }
        render res as JSON
    }


    def changeSituacaoColaboracao() {
        if (!params.sqFichaColaboracao && !params.sqFichaColaboracaoOcorrencia)
        {
            render 'Id não informado!'
            return
        }
        if (!params.sqSituacao)
        {
            render 'Id da situação não informado!'
            return
        }
        if (params.sqFichaColaboracao)
        {
            FichaColaboracao fc = FichaColaboracao.get(params.sqFichaColaboracao.toInteger())
            if (!fc)
            {
                render 'Id inválido!'
                return
            }
            fc.situacao = DadosApoio.get(params.sqSituacao.toInteger())
            fc.save(flush: true)
        } else if (params.sqFichaOcorrenciaConsulta)
        {
            FichaOcorrenciaConsulta foc = FichaOcorrenciaConsulta.get(params.sqFichaOcorrenciaConsulta.toInteger())
            if (!foc)
            {
                render 'Id inválido!'
                return
            }
            foc.fichaOcorrencia.situacao = DadosApoio.get(params.sqSituacao.toInteger())
            foc.fichaOcorrencia.save(flush: true)
        }
        render ''
    }

    def updateSituacaoFicha()
    {
        if( params.sqFicha && params.sqSituacao)
        {
            Ficha ficha = Ficha.get( params.sqFicha.toInteger())
            if( ficha )
            {
                DadosApoio novaSituacao = DadosApoio.get( params.sqSituacao.toInteger() )
                List erros = fichaService.canModifySituacao(ficha,novaSituacao )
                int qtdColaboracaoNaoAvaliada = 0
                if( !erros ) {
                    // situações que a ficha não pode ter colaboração não avaliada
                    if( novaSituacao.codigoSistema =~ /^(VALIDADA|FINALIZADA)$/ ) {
                        /*
                        DadosApoio situacaoNaoAvaliada = dadosApoioService.getByCodigo('TB_SITUACAO_COLABORACAO','NAO_AVALIADA')
                        DadosApoio situacaoResolverOficina = dadosApoioService.getByCodigo('TB_SITUACAO_COLABORACAO','RESOLVER_OFICINA')
                        */
                        // a aba 11.7 deve estar preenchida
                        if( !ficha.categoriaFinal || ficha.categoriaFinal.codigoSistema=='NE')
                        {
                            render'Resultado da validação não informado. (aba 11.7)'
                            return;
                        }
                        /*

                        // ESTA VALIDAÇÃO JÁ FOI FEITA ACIMA EM fichaService.canModifySituacao()

                        // aqui não pode ter resolver em oficina
                        qtdColaboracaoNaoAvaliada += FichaColaboracao.createCriteria().count {
                            consultaFicha{
                                eq('ficha',ficha)
                                isNull('cicloConsultaFichaVersao')
                            }
                            'in'('situacao',[situacaoNaoAvaliada,situacaoResolverOficina])
                        }
                        qtdColaboracaoNaoAvaliada += FichaOcorrenciaConsulta.createCriteria().count() {
                            cicloConsultaFicha{
                                eq('ficha',ficha)
                                isNull('cicloConsultaFichaVersao')
                            }
                            fichaOcorrencia {
                                'in'('situacao', [situacaoNaoAvaliada, situacaoResolverOficina])
                            }
                        }*/
                    } else if( novaSituacao.codigoSistema =~ /^CONSOLIDADA$/ ) {
                        /*

                        // ESTA VALIDAÇÃO JÁ FOI FEITA ACIMA EM fichaService.canModifySituacao()

                        // aqui pode ter resolver em oficina
                        DadosApoio situacaoNaoAvaliada = dadosApoioService.getByCodigo('TB_SITUACAO_COLABORACAO','NAO_AVALIADA')
                        //DadosApoio situacaoResolverOficina = dadosApoioService.getByCodigo('TB_SITUACAO_COLABORACAO','RESOLVER_OFICINA')
                        // aqui não pode ter não avaliada
                        qtdColaboracaoNaoAvaliada += FichaColaboracao.createCriteria().count {
                            consultaFicha{
                                eq('ficha',ficha)
                                isNull('cicloConsultaFichaVersao')

                            }
                            'in'('situacao',[situacaoNaoAvaliada])
                        }
                        qtdColaboracaoNaoAvaliada += FichaOcorrenciaConsulta.createCriteria().count() {
                            cicloConsultaFicha{
                                eq('ficha',ficha)
                                isNull('cicloConsultaFichaVersao')
                            }
                            fichaOcorrencia {
                                eq('situacao',situacaoNaoAvaliada)

                            }
                        }*/
                    }

                    if( qtdColaboracaoNaoAvaliada > 0 ) {
                        if( qtdColaboracaoNaoAvaliada == 1 ) {
                            render 'Ficha possui ' + qtdColaboracaoNaoAvaliada + ' colaboração não avalida.';
                        } else {
                            render 'Ficha possui ' + qtdColaboracaoNaoAvaliada + ' colaborações não avalidas';
                        }
                        return
                    } else {
                        ficha.situacaoFicha = novaSituacao
                        if (params.stManterLc == 'S') {
                            ficha.stManterLc = true
                        }
                        ficha.save(flush: true)
                        cacheService.clear('gerenciarConsulta');
                    }
                }
                else {
                    render erros.join('<br>')
                }
            }
            else
            {
                render 'Ficha não encontrada.'
            }
        }
        render ''
    }

    def getListaFichas()
    {
        //render CicloConsulta.get( params.id.toInteger() ).fichasText
        boolean canModify=true
        Map parametros = [sqCicloConsulta:params.id.toLong()]
        if( ! session.sicae.user.isADM() )
        {
            DadosApoio papel
            //if( ( session.sicae.user.isCT() || session.sicae.user.isCE() ) && ! session?.sicae?.user?.sqUnidadeOrg  )
            // SE O USUÁRIO NÃO TIVER UNIDADE, FAZER FILTRO PELA FUNÇÃO ATRIBUIDA NAS FICHAS
            if( ! session?.sicae?.user?.sqUnidadeOrg  )
            {
                canModify=false
                papel = dadosApoioService.getByCodigo('TB_PAPEL_FICHA',( session.sicae.user.isCT() ? 'COORDENADOR_TAXON':'COLABORADOR_EXTERNO') )
                parametros.inAtivo='S'
                parametros.sqPessoa = session?.sicae?.user?.sqPessoa?.toLong()
                parametros.sqPapel = papel.id.toLong()
            }
        }

        List lista = CicloConsultaFicha.executeQuery("""
        select new map( a.id as id, b.nmCientifico as descricao, d.coNivelTaxonomico as nivelTaxonomico
        ,f.nuVersao as nuVersao, f.id as sqFichaVersao)
        from CicloConsultaFicha a
        inner join a.ficha b
        inner join b.taxon c
        inner join c.nivelTaxonomico d
        left join a.cicloConsultaFichaVersao e
        left join e.fichaVersao f
        where a.cicloConsulta.id = :sqCicloConsulta
        ${parametros.inAtivo ? '\nand b.id in ( select fp.ficha.id from FichaPessoa fp where fp.pessoa.id = :sqPessoa and fp.papel.id = :sqPapel and fp.inAtivo = :inAtivo )' : ''}
        order by b.nmCientifico
        """,parametros).each {
            it.descricao = Util.ncItalico( it.descricao,true, true, it.nivelTaxonomico )
        }
        render(template: "/templates/tableDeleteItem", model: [lista: lista, id:params.id, rowNum:params.rowNum, callback:'gerenciarConsulta.deleteFicha',canModify:canModify] )
    }

    def deleteFichaConsulta()
    {
        Map res = [status: 1, msg: '', errors: [], type: 'error']
        if (params.values) {
            List ids = params.values.split(',').collect { it.toLong() }
            // ler o ciclo_consulta
            CicloConsulta cicloConsulta = CicloConsultaFicha.get( ids[0] ).cicloConsulta;
            List idsFichas = CicloConsultaFicha.executeQuery("select new map( a.ficha.id as sqFicha ) from CicloConsultaFicha a where a.id in (:ids)",[ ids:ids ]).sqFicha
            if (ids.size() > 0) {
                try {
                    if( cicloConsulta.dtFim >= Util.hoje() ) {
                        // fichas foram excluidas de uma consulta/revisao aberta
                        DadosApoio compilacao   = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA','COMPILACAO')
                        DadosApoio consulta     = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA','CONSULTA')
                        DadosApoio avaliada     = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA','POS_OFICINA')
                        DadosApoio avaliadaRevisao = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA','AVALIADA_EM_REVISAO')
                        String cmdSql = """update salve.ficha set sq_situacao_ficha = case when sq_situacao_ficha = ${avaliadaRevisao.id.toString()} then ${avaliada.id.toString()} else
                        CASE WHEN sq_situacao_ficha= ${consulta.id.toString()} then ${compilacao.id.toString()} ELSE sq_situacao_ficha end end where sq_ficha in (${idsFichas.join(',')})"""
                        sqlService.execSql( cmdSql)
                    }
                    CicloConsultaFicha.executeUpdate('delete from CicloConsultaFicha a where a.id in (:ids)', [ids: ids])
                    res.status = 0
                    res.type = 'success'
                    res.msg = 'Exclusão realizada com SUCESSO!'
                } catch (Exception e) {
                    res.msg = e.getMessage()
                }
            }
        }
        render res as JSON
    }
}
