/**
 * Este controlador é resposável por receber as ações referentes ao cadastro aba Pesquisas da ficha de espécies
 * Regras:
 *
 *
 *
 *
 *
 */

package br.gov.icmbio
import grails.converters.JSON;

class FichaPesquisaController extends FichaBaseController {

	def dadosApoioService
	def messageSource;

	def index()
	{
		if( !params.sqFicha )
		{
			render 'Id da ficha deve ser informado!'
			return;
		}
		Ficha ficha 			= Ficha.get(params.sqFicha)
		List listTema 			= dadosApoioService.getTable('TB_TEMA_PESQUISA').sort{ it.ordem }
		List listSituacao		= dadosApoioService.getTable('TB_SITUACAO_PESQUISA').sort{ it.ordem }
		render(template:"/ficha/pesquisa/index",model:['ficha':ficha,'listTema':listTema,'listSituacao':listSituacao]);
	}

	def saveFrmPesquisa()
	{
		if( !params.sqFicha )
		{
			return this._returnAjax(1, 'ID da ficha não informado.', "error")
		}
		Ficha ficha = Ficha.get(params.sqFicha)
		ficha.properties = params
        try {
            ficha.save(flush:true)
            this._returnAjax(0, getMsg(1), "success")
        } catch(Exception e) {
            println e.getMessage()
            this._returnAjax(1, getMsg(2), "error")
        }
	}


	def saveFrmPesquisaDetalhe()
	{

//
//		println params
//		println '-'*100
//		println ' '
//		return this._returnAjax(1, 'OK', "error")
//
//
		if( !params.sqFicha )
		{
			return this._returnAjax(1, 'ID da ficha não informado.', "error")
		}
		if( !params.sqTemaPesquisa )
		{
			return this._returnAjax(1, 'Nenhum tema selecionado.', "error")
		}
		if( !params.sqSituacaoPesquisa )
		{
			return this._returnAjax(1, 'Situação não informada.', "error")
		}
		Ficha ficha			 = Ficha.get( params.sqFicha.toInteger() )
		DadosApoio situacao  = DadosApoio.get( params.sqSituacaoPesquisa.toInteger() )
		DadosApoio tema
		FichaPesquisa reg

		params.sqTemaPesquisa.split(',').each { idTema ->

			tema = DadosApoio.get( idTema.toInteger() )
			reg = FichaPesquisa.findByFichaAndTema(ficha,tema)

			// se existir o tema, apenas alterar a situação
			if( ! reg ) {
				reg = new FichaPesquisa()
				reg.ficha = ficha
				reg.tema = tema
				reg.situacao = situacao
			}
			else
			{
				reg.situacao = situacao
			}
			reg.save(flush: true)
		}

		// atualizar log da ficha, quem alterou
		ficha.dtAlteracao = new Date()
		ficha.save()

		this._returnAjax(0, getMsg(1), "success")
		/*
		if( params.sqFichaPesquisa )
		{
			reg = FichaPesquisa.get( params.sqFichaPesquisa )
		}
		else
		{
			reg = new FichaPesquisa();
			reg.ficha = Ficha.get(params.sqFicha)
		}
		reg.properties 	= params
		reg.tema 		= DadosApoio.get(params.sqTemaPesquisa)
		reg.situacao 	= DadosApoio.get(params.sqSituacaoPesquisa)
        try {
            reg.save(flush:true)
            this._returnAjax(0, getMsg(1), "success")
        } catch(Exception e) {
            println e.getMessage()
            this._returnAjax(1, getMsg(2), "error")
        }
        */
	}

	def getGridPesquisa()
	{
		if( !params.sqFicha)
		{
			render ''
			return
		}
		List listPesquisa = FichaPesquisa.findAllByFicha(Ficha.get(params.sqFicha)).sort { it.tema.descricao }
		render( template:'/ficha/pesquisa/divGridPesquisa',model:['listPesquisa':listPesquisa])
	}

	def editPesquisa()
	{
		FichaPesquisa reg = FichaPesquisa.get(params.id);
		if( reg?.id )
		{
			render reg.asJson();
			return;
		}
		render [:] as JSON;
	}

	def deletePesquisa()
    {
    	FichaPesquisa reg = FichaPesquisa.get(params.id);
		if( reg )
		{
			Ficha ficha = reg.ficha
			reg.delete(flush:true)

			// atualizar log da ficha, quem alterou
			ficha.dtAlteracao = new Date()
			ficha.save()


			return this._returnAjax(0, getMsg(4), "success")
		}
		else
		{
			return this._returnAjax(1, getMsg(51), "error")
		}
		render '';
    }
}