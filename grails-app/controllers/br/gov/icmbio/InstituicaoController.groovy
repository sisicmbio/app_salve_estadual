package br.gov.icmbio
import grails.converters.JSON

class InstituicaoController extends BaseController {
    def index() { 
    	render ''
    }

    def getGrid() {
    	if( ! params.q )
    	{
    		render '<h2>Filtro não informado</h2>'
    		return
    	}
    	String q='%'+params.q+'%'
    	List listWebInstituicao = WebInstituicao.createCriteria().list {
    		or {
    			ilike('noInstituicao',q)
    			ilike('sgInstituicao',q)
    		}
    	}
	   	render( template:'/instituicao/divGridModalCadInstituicao', model:[listWebInstituicao:listWebInstituicao] )
    }

    def save()  {
    	Map res = [status:1,msg:'',type:"error", errors:[]]

    	if( ! params.noInstituicao )
    	{
    		res.errors.push('Nome da instituição não informado!')
    	}
		if( ! params.sgInstituicao )
    	{
    		res.errors.push('Sigla da instituição não informada!')
    	}
    	if( res.errors )
    	{
    		res.msg=''
    	}
    	else
    	{
    		WebInstituicao webInstituicao;
    		if( params.sqWebInstituicao )
    		{
    			webInstituicao = WebInstituicao.get( params.sqWebInstituicao.toInteger() )
    		}
    		else
    		{
    			webInstituicao = new WebInstituicao()
    		}

    		webInstituicao.noInstituicao = params.noInstituicao
            webInstituicao.sgInstituicao = params.sgInstituicao?:null

    		// verificar duplicidade
    		if( WebInstituicao.createCriteria().count {
    			ilike('noInstituicao', webInstituicao.noInstituicao)
    			if( params.sqWebInstituicao ){
	    			ne('id', params.sqWebInstituicao.toLong() )
    			}
    		} > 1 )	{
    			res.errors.push('Nome já cadastrado!')
    			render res as JSON
    			return;
    		}

    		if( webInstituicao.save( ) )	{
    			res.status=0
    			res.type='success'
    			res.msg=getMsg(1)
    		}
    		else
    		{
    			res.msg = 'Erro ao salvar instituição!'
    		}
    	}
    	render res as JSON
    }

    def edit()
    {

        WebInstituicao reg = WebInstituicao.get(params.id);
        if( reg?.id )
        {
            render reg.asJson();
            return;
        }
        render [:] as JSON;
    }

    def delete()
    {

        WebInstituicao reg = WebInstituicao.get(params.id);
        if( reg?.id )
        {
            try {
                reg.delete( flush:true )
            }
            catch( Exception e )
            {
                println e.getMessage()
                render 'Não foi possível excluir este registro. Possivemente está sendo utilizado em outras tabelas.'
            }
            
        }
        render ''
    }

}
