package br.gov.icmbio
import grails.converters.JSON

class AcaoConservacaoController extends BaseController {

    def dadosApoioService
    def sqlService

    protected List _getData(Long idPai = null ){
        String cmdSql = """select distinct a.sq_dados_apoio
                    , a.sq_dados_apoio_pai
                    , a.cd_dados_apoio
                    , a.ds_dados_apoio
                    , a.de_dados_apoio_ordem
                    , c.cd_iucn_acao_conservacao
                    , c.ds_iucn_acao_conservacao
                    , filhos.nu_filhos
                    from salve.dados_apoio a
                    left join salve.iucn_acao_conservacao_apoio b on b.sq_dados_apoio = a.sq_dados_apoio
                    left join salve.iucn_acao_conservacao c on c.sq_iucn_acao_conservacao = b.sq_iucn_acao_conservacao
                    left join lateral (
                        select count(*) as nu_filhos from salve.dados_apoio x where x.sq_dados_apoio_pai = a.sq_dados_apoio
                    ) as filhos on true
                    where a.sq_dados_apoio_pai = :sqPai
                    order by a.de_dados_apoio_ordem"""
        Map sqlParams = [:]
        if( ! idPai )
        {
            DadosApoio pai = dadosApoioService.getByCodigo('TB_ACAO_CONSERVACAO')
            sqlParams.sqPai = pai.id
        } else {
            sqlParams.sqPai = idPai
        }
        return sqlService.execSql(cmdSql, sqlParams)
    }

    def index() {
        List data = _getData()
        Map pagination = ['totalRecords': data.size(), 'pageCurrent': 1,'pageSize':50]
        this._pagination(pagination)
        render (view: 'index', model:['data': data, 'pagination': pagination])
    }


    def getTreeView() {
        response.setHeader("Content-Type", "application/json")
        response.setHeader("Access-Control-Allow-Origin","*" )
        List data = _getData( params.key ? params.key.toLong() : null )
        Map result=['status':'ok','result':[]];
        data.each {
            String codigoApoio = it.cd_dados_apoio && (it.cd_dados_apoio.toString() =~ /^[0-9]/) ? it.cd_dados_apoio : ''
            result['result'].push([
                'key'         : it.sq_dados_apoio
                ,'pai'        : (it.sq_dados_apoio_pai ?: 0 )
                ,'title'      :  ( codigoApoio ? codigoApoio + ' ' : '' ) + it.ds_dados_apoio
                ,'iucnAcaoConservacao' : ( it.cd_iucn_acao_conservacao ?  it.cd_iucn_acao_conservacao + ' '+ it.ds_iucn_acao_conservacao : '' )
                // campos controle da fancytree
                ,'select'   : false
                ,'expanded' : false
                ,'folder'   : (it?.nu_filhos > 0)
                ,'lazy'     : (it?.nu_filhos > 0)
            ] );
        }

            sleep(500)
        render result as JSON;
    }

    def getForm()
    {
        DadosApoio acaoConservacao
        IucnAcaoConservacaoApoio iucnAcaoConservacaoApoio

        if( params.id && params.operacao == 'acaoConservacao.edit' )
        {
            acaoConservacao = DadosApoio.get( params.id)
            iucnAcaoConservacaoApoio = IucnAcaoConservacaoApoio.findByAcaoConservacao( acaoConservacao )
        }
        else
        {
            acaoConservacao = new DadosApoio()
            iucnAcaoConservacaoApoio = new IucnAcaoConservacaoApoio()
        }
        List listAcaoConservacao = dadosApoioService.getTable('TB_ACAO_CONSERVACAO').sort{it.ordem }
        List listIucnAcaoConservacao = IucnAcaoConservacao.findAll().sort{it.dsIucnAcaoConservacaoOrdem}
        render( template:'frmAcaoConservacao',model:[ acaoConservacao:acaoConservacao
                                             ,listAcaoConservacao:listAcaoConservacao
                                             ,listIucnAcaoConservacao:listIucnAcaoConservacao
                                             ,iucnAcaoConservacaoApoio:iucnAcaoConservacaoApoio]);
    }

    def save(){

        DadosApoio dadosApoio
        if( params.operacao == 'acaoConservacao.edit')
        {
            dadosApoio = DadosApoio.get(params.id);
        }
        else if( params.operacao == 'acaoConservacao.add')
        {
            dadosApoio = new DadosApoio()
            if( ! params.id ) {
                dadosApoio.pai = dadosApoioService.getByCodigo('TB_ACAO_CONSERVACAO')
            } else {
                dadosApoio.pai = DadosApoio.get( params.id)
            }
        }
        String codigoSistema = params.codigoSistema ?: null
        if( codigoSistema ) {
            codigoSistema = Util.calcularCodigoSistema( codigoSistema )
        } else {
            codigoSistema = Util.calcularCodigoSistema( params.descricao )
        }
        dadosApoio.codigo             = params.codigo
        dadosApoio.codigoSistema      = codigoSistema
        dadosApoio.ordem              = params.ordem ?: Util.formatarCodigoOrdem(params.codigo)
        dadosApoio.descricao          = params.descricao
        dadosApoio.save(flush:true)

        // adicionar registro na tabela iucn_acao_conservacao_apoio
        IucnAcaoConservacaoApoio iucnAcaoConservacaoApoio = IucnAcaoConservacaoApoio.findByAcaoConservacao( dadosApoio )
        if( params.sqIucnAcaoConservacao) {
            //acaoConservacao.iucnUso = IucnUso.get( params.sqIucnUso.toLong() )
            IucnAcaoConservacao iucnAcaoConservacao = IucnAcaoConservacao.get( params.sqIucnAcaoConservacao )
            if( ! iucnAcaoConservacaoApoio ){
                iucnAcaoConservacaoApoio = new IucnAcaoConservacaoApoio()
                iucnAcaoConservacaoApoio.acaoConservacao =  dadosApoio
            }
            iucnAcaoConservacaoApoio.iucnAcaoConservacao = iucnAcaoConservacao
            iucnAcaoConservacaoApoio.save( flush:true )
        } else if( iucnAcaoConservacaoApoio )  {
            iucnAcaoConservacaoApoio.delete(flush: true)
        }
        return this._returnAjax(0,null,null,[id:dadosApoio.id
                                             , codigoSistema:dadosApoio.codigoSistema
                                             , ordem:dadosApoio.ordem ]);
    }

    def delete()
    {
        if( params.id)
        {
            DadosApoio  dadosApoio  = DadosApoio.get(params.id)
            if( dadosApoio ) {
                IucnAcaoConservacaoApoio iucnAcaoConservacaoApoio = IucnAcaoConservacaoApoio.findByAcaoConservacao( dadosApoio )
                if( iucnAcaoConservacaoApoio ) {
                    iucnAcaoConservacaoApoio.delete(flush:true)
                }
                dadosApoio.delete( flush:true )

            }
            return this._returnAjax(0);
        }
        return this._returnAjax(1, 'Id não informado.', "error")
    }

}
