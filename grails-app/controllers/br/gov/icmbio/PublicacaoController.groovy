package br.gov.icmbio

import grails.converters.JSON
import org.codehaus.groovy.grails.web.json.JSONObject

class PublicacaoController extends BaseController {

    def dadosApoioService
    def fichaService
    def sqlService
    def doiService
    def iucnService


    /**
     * exibir a tela in        def http = new HTTPBuilder('https://test.crossref.org/servlet/deposit')
        println httpicial do módulo de publicação
     * @return
     */
    def index() {

        // listar os ciclos de avaliação
        List listCiclosAvaliacao = [ session.getAttribute('ciclo') ]//CicloAvaliacao.list( sort: 'nuAno', order: 'desc' );
        render view: 'index', model: [ listCiclosAvaliacao: listCiclosAvaliacao ]
    }

    /**
     * renderizar o gride com as fichas publicadas
     * @return
     */
    def getGridFichasPublicadas() {
        List qryInnerJoins = []
        List sqlWherePasso1 = []
        List sqlWherePasso2 = []

        Map sqlParams = [:]
        DadosApoio situacaoPublicada = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'PUBLICADA')
        DadosApoio tipoImagem = dadosApoioService.getByCodigo('TB_TIPO_MULTIMIDIA','IMAGEM')
        DadosApoio situacaoReprovada = dadosApoioService.getByCodigo('TB_SITUACAO_MULTIMIDIA','REPROVADA')

        if( !situacaoPublicada ) {
            render 'Situação Publicada não cadastrada na tabela de apoio TB_SITUACAO_FICHA!'
            return
        }

        // filtro pelo ciclo de avaliacao
        if( params.sqCicloAvaliacao ){
            sqlWherePasso1.push('ficha.sq_ciclo_avaliacao = :sqCicloAvaliacao')
            sqlParams.sqCicloAvaliacao = params.sqCicloAvaliacao.toLong()
        }

        // filtro pela unidade responsavel
        if( params.sqUnidadeFiltro ){
            sqlWherePasso1.push('ficha.sq_unidade_org = :sqUnidadeOrg')
            sqlParams.sqUnidadeOrg = params.sqUnidadeFiltro.toLong()
        }

        // filtro pelo nome cientifico
        if( params.nmCientificoFiltro ){
            params.nmCientificoFiltro = params.nmCientificoFiltro.toString().replaceAll(/%/, '').replaceAll(/;/, ',')
            // verificar se foi passado lista de nomes ou apenas 1 nome
            List nomes = params.nmCientificoFiltro.split(',').collect { it.toString().trim() }
            if (nomes.size() == 1) {
                // quando for informado somente 1 nome cientifico, fazer a busca pelos nomes antigos (sinonimias) também
                sqlWherePasso1.push("""( ficha.nm_cientifico ilike :nmCientifico OR exists (
                                        select null from salve.ficha_sinonimia
                                        where no_sinonimia ILIKE :noSinonimia
                                        and ficha_sinonimia.sq_ficha = ficha.sq_ficha
                                        limit 1 ) )""")

                params.nmCientificoFiltro = Util.clearCientificName(params.nmCientificoFiltro)
                sqlParams.noSinonimia = nomes[0]
                sqlParams.nmCientifico = '%' + nomes[0] + '%'
            } else {
                List whereOr = []
                qryInnerJoins.push('inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon')
                List nomesEspecies = []
                List nomesSubespecies = []
                nomes.eachWithIndex { nome, index ->
                    // colocar o nome cientifico iniciado com letra maiuscula e o restante em minusculas
                    nome = nome.substring(0, 1).toUpperCase() + nome.substring(1)
                    // criar dinamicamente os parametros para cada nome informado
                    //String paramName = 'noTaxon' + index.toString()
                    //qryParams[paramName] = nome
                    // gerar cláusula OR do comando sql que será gerado
                    if (nome.split(' ').size() == 2) {
                        nomesEspecies.push( nome )
                    } else {
                        nomesSubespecies.push( nome )
                    }
                }
                if( nomesEspecies ){
                    String nomesFormatados = "'"+nomesEspecies.join("','")+"'"
                    whereOr.push("taxon.json_trilha->'especie'->>'no_taxon'::text = any(array[${nomesFormatados}])")
                } else if (nomesSubespecies) {
                    String nomesFormatados = "'"+nomesSubespecies.join("','")+"'"
                    whereOr.push("taxon.json_trilha->'subespecie'->>'no_taxon'::text = any(array[${nomesFormatados}])")
                }
                if (whereOr) {
                    sqlWherePasso1.push('(' + whereOr.join(' or ') + ')')
        }
                params.nmCientificoFiltro = '%'
            }
        }

        // filtro pelo percentual de revisao
        if( params.inAndamentoRevisaoFiltro ) {
            qryInnerJoins.push('left join salve.traducao_ficha_percentual on traducao_ficha_percentual.sq_ficha = ficha.sq_ficha')
            if( params.inAndamentoRevisaoFiltro == 'REVISAO_NAO_INICIADA' ){
                sqlWherePasso1.push('(traducao_ficha_percentual.nu_percentual_revisado is null or traducao_ficha_percentual.nu_percentual_revisado = 0)')
            } else if( params.inAndamentoRevisaoFiltro == 'REVISAO_INICIADA' ){
                sqlWherePasso1.push('traducao_ficha_percentual.nu_percentual_revisado > 0 and traducao_ficha_percentual.nu_percentual_revisado < 100')
            } else if( params.inAndamentoRevisaoFiltro == 'REVISAO_FINALIZADA' ){
                sqlWherePasso1.push('traducao_ficha_percentual.nu_percentual_revisado > 99')
            }
        }


        // filtro pelo nome comum
        if( params.nmComumFiltro ){
            /*sqlWherePasso1.push('nome_comum.no_comum ilike :nmComum')
            params.nmComumFiltro =Util.removeAccents(params.nmComumFiltro.replaceAll(/[^a-zA-Z0-9_-]/,''))
            sqlParams.nmComum = '%' + params.nmComumFiltro + '%'*/
            sqlWherePasso1.push("EXISTS ( SELECT NULL from salve.ficha_nome_comum fnc where fnc.sq_ficha = ficha.sq_ficha and public.fn_remove_acentuacao(fnc.no_comum) ilike :nmComum limit 1 )")
            sqlParams.nmComum = '%' + Util.removeAccents(params.nmComumFiltro) + '%'

        }
        // filtro pelo nivel taxonomico
        if (params.nivelFiltro && params.sqTaxonFiltro) {
            qryInnerJoins.push('inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon')
            if (params.sqTaxonFiltro.toString().indexOf(',') > -1) {
                sqlWherePasso1.push("(taxon.json_trilha->'${params.nivelFiltro.toLowerCase()}'->>'sq_taxon')::integer in (${params.sqTaxonFiltro})")
            } else {
                sqlWherePasso1.push("(taxon.json_trilha->'${params.nivelFiltro.toLowerCase()}'->>'sq_taxon')::integer = :sqTaxon")
                sqlParams.sqTaxon = params.sqTaxonFiltro.toLong()
            }
        }

        // filtro pela categoria
        if (params.sqCategoriaFiltro ) {
            if( params.sqCategoriaFiltro.indexOf(',') == -1 ) {
                sqlWherePasso1.push("ficha.sq_categoria_final = :sqCategoriaFinal")
                sqlParams.sqCategoriaFinal = params.sqCategoriaFiltro.toLong()
            } else {
                sqlWherePasso1.push("ficha.sq_categoria_final in (${params.sqCategoriaFiltro})")
            }
        }

        // filtro pelo grupo avaliado
        if (params.sqGrupoFiltro ) {
            if( params.sqGrupoFiltro.indexOf(',') == -1 ) {
                sqlWherePasso1.push("ficha.sq_grupo = :sqGrupoAvaliado")
                sqlParams.sqGrupoAvaliado = params.sqGrupoFiltro.toLong()
            } else {
                sqlWherePasso1.push("ficha.sq_grupo in (${params.sqGrupoFiltro})")
            }
        }

        // filtro pelo subgrupo
        if (params.sqSubgrupoFiltro ) {
            if( params.sqSubgrupoFiltro.indexOf(',') == -1 ) {
                sqlWherePasso1.push("ficha.sq_subgrupo = :sqSubgrupoFiltro")
                sqlParams.sqSubgrupoFiltro = params.sqSubgrupoFiltro.toLong()
            } else {
                sqlWherePasso1.push("ficha.sq_subgrupo in (${params.sqSubgrupoFiltro})")
            }
        }

        // filtrar pelos ids selecionados
        if( params.ids ) {
            if( params.ids.indexOf(',') == -1 ) {
                sqlWherePasso1.push("ficha.sq_ficha = :sqFicha")
                sqlParams.sqFicha = params.ids.toLong()
            } else {
                sqlWherePasso1.push("ficha.sq_ficha = any( array[${params.ids}] )")
            }
        }

        // filtro fichas enviadas/não enviadas para iucn
        if( params.inEnvioIucnFiltro ) {
            sqlWherePasso2.push("iucn_transmissao.dt_transmissao is ${params.inEnvioIucnFiltro == 'S' ? 'NOT': ''} null")
        }

        // FILTRAR EXIBIDAS/NAO EXIBIDAS MÓDULO PUBLICO
        if( params.stExibirModuloPublicoFiltro){
            qryInnerJoins.push('left outer join salve.ficha_modulo_publico fmp on fmp.sq_ficha = ficha.sq_ficha')
            if( params.stExibirModuloPublicoFiltro.toLowerCase() == 's') {
                sqlWherePasso1.push('fmp.st_exibir=true')
            } else {
                sqlWherePasso1.push('(fmp.st_exibir=false or fmp.st_exibir is null)')
            }
        }

        // FILTRAR FICHAS COM/SEM DOI
        if( params.inComSemDOIFiltro ) {
            if( params.inComSemDOIFiltro.toLowerCase() =='s') {
                sqlWherePasso1.push('ficha.ds_doi IS NOT null')
            } else if ( params.inComSemDOIFiltro.toLowerCase() =='n') {
                sqlWherePasso1.push('ficha.ds_doi IS null')
            }
        }

        // remover itens duplicados
        qryInnerJoins.unique()


        // fim construcao dos filtros
        String cmdSql= """with fichasValidas  as (
                            select ficha_versao.sq_ficha
                            , json_agg(json_build_object('nu_versao'
                            , ficha_versao.nu_versao
                            , 'sq_ficha_versao',ficha_versao.sq_ficha_versao
                            , 'st_publico',ficha_versao.st_publico
                            , 'dt_versao'
                            , to_char(ficha_versao.dt_inclusao,'dd/mm/yyyy'))) as json_versoes
                            from salve.ficha_versao
                            inner join salve.ficha on ficha.sq_ficha = ficha_versao.sq_ficha
                                ${qryInnerJoins ? qryInnerJoins.join('\n') : ''}
                            ${sqlWherePasso1 ? '\nwhere ' + sqlWherePasso1.join('\nand ') : ''}
                            group by ficha_versao.sq_ficha
                        ),
                         ocorrencias as (
                            SELECT a.sq_ficha, count( * ) as qtd_carencia
                            FROM fichasValidas
                                     inner join salve.mv_registros as a on fichasValidas.sq_ficha = a.sq_ficha
                            where a.cd_situacao_avaliacao = 'REGISTRO_UTILIZADO_AVALIACAO'
                              and a.dt_carencia > current_date
                            group by a.sq_ficha
                        ), fotos as (
                            select x.sq_ficha, count(*) as nu_fotos
                            from fichasValidas
                            inner join salve.ficha_multimidia x on  x.sq_ficha = fichasValidas.sq_ficha
                            where x.sq_situacao <> ${situacaoReprovada.id.toString()} and x.sq_tipo = ${tipoImagem.id.toString()}
                            group by x.sq_ficha
                        ), ultimaTransmissao as (
                            select fichasValidas.sq_ficha, max( x.sq_iucn_transmissao_ficha ) as sq_iucn_transmissao_ficha
                            from fichasValidas
                                     inner join salve.iucn_transmissao_ficha x on x.sq_ficha = fichasValidas.sq_ficha
                            group by fichasValidas.sq_ficha
                        )
                        select ficha.sq_ficha
                             , coalesce(taxon.json_trilha -> 'subespecie' ->> 'no_taxon',
                                        taxon.json_trilha -> 'especie' ->> 'no_taxon')              as nm_cientifico
                             , ficha.nm_cientifico as no_cientifico_com_autor
                             , concat(categoria.ds_dados_apoio, ' (', categoria.cd_dados_apoio, ')')as de_categoria
                             , ficha.ds_criterio_aval_iucn_final as de_criterio
                             , unidade.sg_unidade_org
                             , nivel.co_nivel_taxonomico
                             , ficha.ds_doi
                             , ficha.dt_publicacao
                             , pf.no_pessoa
                             , fotos.nu_fotos
                             , coalesce( ocorrencias.qtd_carencia,0)  as qtd_carencia
                             , iucn_transmissao.dt_transmissao as dt_ultima_transmissao
                             , iucn_transmissao.sq_iucn_transmissao
                             , strpos(iucn_transmissao.tx_log,concat(': ',fichasValidas.sq_ficha::text,'.') ) > 0 as st_erro
                             , fichasValidas.json_versoes::text
                        from fichasValidas
                                 inner join salve.ficha on ficha.sq_ficha = fichasValidas.sq_ficha
                                 inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                                 inner join taxonomia.nivel_taxonomico as nivel on nivel.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
                                 inner join salve.dados_apoio as categoria on categoria.sq_dados_apoio = ficha.sq_categoria_final
                                 inner join salve.vw_unidade_org as unidade on unidade.sq_pessoa = ficha.sq_unidade_org
                                 left join salve.pessoa as pf on pf.sq_pessoa = ficha.sq_usuario_publicacao
                                 left outer join ocorrencias on ocorrencias.sq_ficha = fichasValidas.sq_ficha
                                 left outer join fotos on fotos.sq_ficha = fichasValidas.sq_ficha
                                 left outer join ultimaTransmissao on fichasValidas.sq_ficha = ultimaTransmissao.sq_ficha
                                 left outer join salve.iucn_transmissao_ficha as transmissao_ficha on transmissao_ficha.sq_iucn_transmissao_ficha = ultimaTransmissao.sq_iucn_transmissao_ficha
                                 left outer join salve.iucn_transmissao on iucn_transmissao.sq_iucn_transmissao = transmissao_ficha.sq_iucn_transmissao
                        ${sqlWherePasso2 ? '\nand ' + sqlWherePasso2.join('\nand ') : ''}
                        """
        /** /
         println ' '
         println ' '
         println ' '
         println ' '
        println cmdSql
        println sqlParams
        /**/
        //List lista = []
        List lista = sqlService.execSql( cmdSql, sqlParams )
        lista.sort{it.nm_cientifico}
        lista.each{
            // colocar as versões em ordem decrescente
            it.versoes = JSON.parse(it.json_versoes ).sort{  a,b-> b.nu_versao <=> a.nu_versao }
        }
        // fazer o cancelamento da publicação das fichas selecionadas
        if( params.cancelarPublicacao == 'S' ){
            params.ids = lista.sq_ficha.join(',')
            params.mensagemErro = _cancelar()
            params.remove( 'cancelarPublicacao')
            params.remove( 'ids')
            getGridFichasPublicadas()
            return
        }

        // fazer a transmissão das fichas para o SIS/IUCN
        if( params.transmitirIucn == 'S' ){
            params.ids = lista.sq_ficha.join(',')
            params.mensagemErro = _transmitirIucn()
            params.remove( 'transmitirIucn')
            params.remove( 'ids')
            if( params.mensagemErro ){
                render params.mensagemErro
            } else {
                render 'O arquivo zip contendo as informações da(s) ficha(s) selecionada(s) para transmissão está sendo gerado, ao finalizar serã enviado para o seu e-mail'
            }
            //getGridFichasPublicadas()
            return
        }
        // renderizar o gride
        render template: 'divGridFichasPublicadas', model: [ listFicha   : lista ]
    }

    /**
     * criar o gride das fichas não publicadas
     * @return
     */
    def getGridFichasNaoPublicadas(){
        List qryInnerJoins = []
        List sqlWherePasso1 = []
        List sqlWherePasso2 = []
        String orderBy = ''
        Map sqlParams = [:]
        DadosApoio contextoRevisao = dadosApoioService.getByCodigo('TB_CONTEXTO_PENDENCIA', 'REVISAO')
        DadosApoio tipoImagem = dadosApoioService.getByCodigo('TB_TIPO_MULTIMIDIA','IMAGEM')
        DadosApoio situacaoAprovada = dadosApoioService.getByCodigo('TB_SITUACAO_MULTIMIDIA','APROVADA')

        if( !params.sqCicloAvaliacao ) {
            render 'id ciclo avaliação não informado.'
            return
        }

        CicloAvaliacao cicloAvaliacao = CicloAvaliacao.get( params.sqCicloAvaliacao.toInteger() )
        if( !cicloAvaliacao ) {
            render 'Id ciclo avaliação inválido'
            return
        }

        // situacao finalizada
        DadosApoio situacaoFinalizada = dadosApoioService.getByCodigo( 'TB_SITUACAO_FICHA', 'FINALIZADA' )
        if( !situacaoFinalizada ) {
            render 'Situação Finalizada não cadastrada na tabela de apoio TB_SITUACAO_FICHA!'
            return
        }

        // filtro pelo ciclo de avaliacao
        if( params.sqCicloAvaliacao ){
            sqlWherePasso1.push('ficha.sq_ciclo_avaliacao = :sqCicloAvaliacao')
            sqlParams.sqCicloAvaliacao = params.sqCicloAvaliacao.toLong()
        }
        // filtro pela unidade responsavel
        if( params.sqUnidadeFiltro ){
            sqlWherePasso1.push('ficha.sq_unidade_org = :sqUnidadeOrg')
            sqlParams.sqUnidadeOrg = params.sqUnidadeFiltro.toLong()
        }
        // filtro pelo nome cientifico
        /*if( params.nmCientificoFiltro ){
            sqlWherePasso1.push('ficha.nm_cientifico ilike :nmCientifico')
            params.nmCientificoFiltro = params.nmCientificoFiltro.replaceAll(/[^\sa-zA-Z-]/,'')
            sqlParams.nmCientifico= '%' + params.nmCientificoFiltro + '%'
        }*/

        if (params.nmCientificoFiltro) {
            params.nmCientificoFiltro = params.nmCientificoFiltro.toString().replaceAll(/%/, '').replaceAll(/;/, ',')
            // verificar se foi passado lista de nomes ou apenas 1 nome
            List nomes = params.nmCientificoFiltro.split(',').collect { it.toString().trim() }
            if (nomes.size() == 1) {
                // quando for informado somente 1 nome cientifico, fazer a busca pelos nomes antigos (sinonimias) também
                sqlWherePasso1.push("""( ficha.nm_cientifico ilike :nmCientifico OR exists (
                                        select null from salve.ficha_sinonimia
                                        where no_sinonimia ILIKE :noSinonimia
                                        and ficha_sinonimia.sq_ficha = ficha.sq_ficha
                                        limit 1 ) )""")

                params.nmCientificoFiltro = Util.clearCientificName(params.nmCientificoFiltro)
                sqlParams.noSinonimia = nomes[0]
                sqlParams.nmCientifico = '%' + nomes[0] + '%'
            } else {
                List whereOr = []
                qryInnerJoins.push("""inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon""")
                List nomesEspecies = []
                List nomesSubespecies = []
                nomes.eachWithIndex { nome, index ->
                    // colocar o nome cientifico iniciado com letra maiuscula e o restante em minusculas
                    nome = nome.substring(0, 1).toUpperCase() + nome.substring(1)
                    // criar dinamicamente os parametros para cada nome informado
                    //String paramName = 'noTaxon' + index.toString()
                    //qryParams[paramName] = nome
                    // gerar cláusula OR do comando sql que será gerado
                    if (nome.split(' ').size() == 2) {
                        nomesEspecies.push( nome )
                    } else {
                        nomesSubespecies.push( nome )
                    }
        }
                if( nomesEspecies ){
                    String nomesFormatados = "'"+nomesEspecies.join("','")+"'"
                    whereOr.push("taxon.json_trilha->'especie'->>'no_taxon'::text = any(array[${nomesFormatados}])")
                } else if (nomesSubespecies) {
                    String nomesFormatados = "'"+nomesSubespecies.join("','")+"'"
                    whereOr.push("taxon.json_trilha->'subespecie'->>'no_taxon'::text = any(array[${nomesFormatados}])")
                }
                if (whereOr) {
                    sqlWherePasso1.push('(' + whereOr.join(' or ') + ')')
                }
                params.nmCientificoFiltro = '%'
            }
        }


        // filtro pelo nome comum
        if( params.nmComumFiltro ){
            /*
            sqlWherePasso1.push('nome_comum.no_comum ilike :nmComum')
            params.nmComumFiltro =Util.removeAccents(params.nmComumFiltro.replaceAll(/[^a-zA-Z0-9_-]/,''))
            sqlParams.nmComum = '%' + params.nmComumFiltro + '%'*/
            sqlWherePasso1.push("EXISTS ( SELECT NULL from salve.ficha_nome_comum fnc where fnc.sq_ficha = ficha.sq_ficha and public.fn_remove_acentuacao(fnc.no_comum) ilike :nmComum limit 1 )")
            sqlParams.nmComum = '%' + Util.removeAccents(params.nmComumFiltro) + '%'

        }
        // filtro pelo nivel taxonomico
        if ( params.nivelFiltro && params.sqTaxonFiltro) {
            qryInnerJoins.push('inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon')
            if (params.sqTaxonFiltro.toString().indexOf(',') > -1) {
                sqlWherePasso1.push("(taxon.json_trilha->'${params.nivelFiltro.toLowerCase()}'->>'sq_taxon')::integer in (${params.sqTaxonFiltro})")
            } else {
                sqlWherePasso1.push("(taxon.json_trilha->'${params.nivelFiltro.toLowerCase()}'->>'sq_taxon')::integer = :sqTaxon")
                sqlParams.sqTaxon = params.sqTaxonFiltro.toLong()
            }
        }

        // filtro pelas pendencias
        if ( params.inPendenciaFiltro ) {
            String operador=''
            if( params.inPendenciaFiltro == '1'){
                // SEM PENDENCIA TIPO REVISAO
                operador='NOT'
            }
            sqlWherePasso1.push("""${operador} EXISTS ( SELECT NULL from salve.ficha_pendencia pendencia where pendencia.sq_ficha = ficha.sq_ficha and pendencia.sq_contexto = ${contextoRevisao.id.toString()} LIMIT 1 )""")
        }

        // filtro pelas revisadas
        if ( params.inRevisadasFiltro ) {
            String operador=''
            if( params.inRevisadasFiltro == 'N' ) {
                // NÃO REVISADAS
                operador='NOT'
            }
            sqlWherePasso1.push("""EXISTS ( select null from salve.ficha_pendencia x where x.sq_ficha = ficha.sq_ficha and x.sq_usuario_resolveu is not null and x.sq_usuario_revisou is ${operador} null and x.sq_contexto = ${contextoRevisao.id.toString()} LIMIT 1 )""")
        }
        // filtro pela categoria
        if (params.sqCategoriaFiltro ) {
            if( params.sqCategoriaFiltro.indexOf(',') == -1 ) {
                sqlWherePasso1.push("ficha.sq_categoria_final = :sqCategoriaFinal")
                sqlParams.sqCategoriaFinal = params.sqCategoriaFiltro.toLong()
            } else {
                sqlWherePasso1.push("ficha.sq_categoria_final in (${params.sqCategoriaFiltro})")
            }
        }

        // filtro pelo grupo avaliado
        if (params.sqGrupoFiltro ) {
            if( params.sqGrupoFiltro.indexOf(',') == -1 ) {
                sqlWherePasso1.push("ficha.sq_grupo = :sqGrupoAvaliado")
                sqlParams.sqGrupoAvaliado = params.sqGrupoFiltro.toLong()
            } else {
                sqlWherePasso1.push("ficha.sq_grupo in (${params.sqGrupoFiltro})")
            }
        }

        // filtro pelo subgrupo
        if (params.sqSubgrupoFiltro ) {
            if( params.sqSubgrupoFiltro.indexOf(',') == -1 ) {
                sqlWherePasso1.push("ficha.sq_subgrupo = :sqSubgrupoFiltro")
                sqlParams.sqSubgrupoFiltro = params.sqSubgrupoFiltro.toLong()
            } else {
                sqlWherePasso1.push("ficha.sq_subgrupo in (${params.sqSubgrupoFiltro})")
            }
        }


        // filtrar pelos ids selecionados
        if( params.ids ) {
            if( params.ids.indexOf(',') == -1 ) {
                sqlWherePasso1.push("ficha.sq_ficha = :sqFicha")
                sqlParams.sqFicha = params.ids.toLong()
            } else {
                sqlWherePasso1.push("ficha.sq_ficha = any( array[${params.ids}] )")
            }
        }


        // FILTRAR EXIBIDAS/NAO EXIBIDAS MÓDULO PUBLICO
        if( params.stExibirModuloPublicoFiltro){
            if( params.stExibirModuloPublicoFiltro.toLowerCase() == 's') {
                sqlWherePasso1.push('fmp.st_exibir=true')
            } else {
                sqlWherePasso1.push('(fmp.st_exibir=false or fmp.st_exibir is null)')
            }
        }

        // remover duplicidade se houver
        qryInnerJoins.unique()

        // fim construcao dos filtros

        String cmdSql = """with fichasValidas  as (
                            select ficha.sq_ficha
                                 ,ficha.sq_taxon
                                 ,ficha.nm_cientifico as no_cientifico_com_autor
                                 ,ficha.ds_criterio_aval_iucn_final as de_criterio
                                 ,ficha.ds_doi
                                 ,ficha.dt_publicacao
                                 ,ficha.sq_categoria_final
                                 ,ficha.sq_unidade_org
                                 ,ficha.sq_usuario_publicacao
                            from salve.ficha
                            ${qryInnerJoins ? qryInnerJoins.join('\n') : ''}
                            ${ params.nivelFiltro ? 'inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon' : ''}
                            where ficha.sq_situacao_ficha = ${situacaoFinalizada.id.toString() }
                            ${sqlWherePasso1 ? '\nand ' + sqlWherePasso1.join('\nand ') : ''}
                        ), ocorrencias as (
                            SELECT a.sq_ficha, count( * ) as qtd_carencia
                            FROM fichasValidas
                            inner join salve.mv_registros as a on fichasValidas.sq_ficha = a.sq_ficha
                            where a.cd_situacao_avaliacao = 'REGISTRO_UTILIZADO_AVALIACAO'
                              and a.dt_carencia  > current_date
                            group by a.sq_ficha
                        ), pendencia_revisao as (
                           select fichasValidas.sq_ficha, sum(case when x.st_pendente = 'S' then 1 else 1 end) as qtd_pendencia_revisao
                           ,sum(case when x.sq_usuario_resolveu is not null and x.sq_usuario_revisou is null then 1 else 0 end) as qtd_nao_revisada
                           ,sum(case when x.sq_usuario_resolveu is null then 1 else 0 end) as qtd_nao_resolvida
                           from fichasValidas
                           inner join salve.ficha_pendencia x on x.sq_ficha = fichasValidas.sq_ficha
                           where x.sq_contexto = ${ contextoRevisao.id.toString() }
                           group by fichasValidas.sq_ficha
                        ), versoes as (
                            select fichasValidas.sq_ficha, json_agg(json_build_object('sq_ficha_versao',ficha_versao.sq_ficha_versao,'st_publico',ficha_versao.st_publico,'nu_versao', ficha_versao.nu_versao,'dt_versao', to_char(ficha_versao.dt_inclusao,'dd/mm/yyyy'))) as json_versoes
                            from fichasValidas
                            inner join salve.ficha_versao on ficha_versao.sq_ficha = fichasValidas.sq_ficha
                            group by fichasValidas.sq_ficha
                        )
                        select fichasValidas.sq_ficha
                             , coalesce(taxon.json_trilha -> 'subespecie' ->> 'no_taxon',
                                        taxon.json_trilha -> 'especie' ->> 'no_taxon')               as nm_cientifico
                             , fichasValidas.no_cientifico_com_autor
                             , nivel.co_nivel_taxonomico
                             , concat(categoria.ds_dados_apoio, ' (', categoria.cd_dados_apoio, ')') as de_categoria
                             , fichasValidas.de_criterio
                             , unidade.sg_unidade_org
                            , coalesce( ocorrencias.qtd_carencia,0)  as qtd_carencia
                            ,coalesce(pendencia_revisao.qtd_pendencia_revisao, 0) as qtd_pendencia_revisao
                            ,coalesce(pendencia_revisao.qtd_nao_resolvida, 0)     as qtd_nao_resolvida
                            ,coalesce(pendencia_revisao.qtd_nao_revisada, 0)      as qtd_nao_revisada
                            ,case when pendencia_revisao.qtd_nao_revisada > 0 then 'bgRed' else
                                case when pendencia_revisao.qtd_nao_resolvida > 0 then 'bgOrange' else 'bgGreen' end
                                    end as de_cor_pendencia
                            , fotos.nu_fotos
                            , versoes.json_versoes::text
                            , coalesce(fmp.st_exibir,true) as st_exibir_modulo_publico
                        from fichasValidas
                        inner join taxonomia.taxon on taxon.sq_taxon = fichasValidas.sq_taxon
                        inner join taxonomia.nivel_taxonomico as nivel on nivel.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
                        inner join salve.dados_apoio as categoria on categoria.sq_dados_apoio = fichasValidas.sq_categoria_final
                        inner join salve.vw_unidade_org as unidade on unidade.sq_pessoa = fichasValidas.sq_unidade_org
                        left outer join salve.ficha_modulo_publico as fmp on fmp.sq_ficha = fichasValidas.sq_ficha
                        left outer join ocorrencias on ocorrencias.sq_ficha = fichasValidas.sq_ficha
                        left outer join pendencia_revisao on pendencia_revisao.sq_ficha = fichasValidas.sq_ficha
                        left join lateral (
                            select count(sq_ficha) as nu_fotos
                            from salve.ficha_multimidia x
                            where x.sq_ficha = fichasValidas.sq_ficha
                            and x.sq_situacao = ${situacaoAprovada.id.toString()}
                            and x.sq_tipo = ${tipoImagem.id.toString()}
                            ) as fotos on true
                        left outer join versoes on versoes.sq_ficha = fichasValidas.sq_ficha
                        ${sqlWherePasso2 ? '\nwhere ' + sqlWherePasso2.join('\nand ') : ''}
                        """
        /** /
         // st_exibir_modulo_publico
        println ' '
        println cmdSql
        println params
        /**/

        List lista  = sqlService.execSql(cmdSql, sqlParams)
        lista.sort{it.nm_cientifico }

        // fazer a publicacação das fichas selecionadas
        if( params.publicar == 'S' ){
            params.ids = lista.sq_ficha.join(',')
            params.mensagemErro = _publicar()
            params.remove( 'publicar')
            params.remove( 'ids')
            getGridFichasNaoPublicadas()
            return
        }
        lista.each{
            // colocar as versões em ordem decrescente
            //it.versoes = [:]
            it.versoes = it.json_versoes ? JSON.parse(it.json_versoes ).sort{  a,b-> b.nu_versao <=> a.nu_versao } : [:]
        }

        // renderizar o gride
        render template: 'divGridFichasNaoPublicadas', model: [ listFicha   : lista]
    }

    /**
     * publicar fichas selecionadas
     * @return
     */
    protected String _publicar() {
        //Map res = [ msg: '', type: 'error', status: 1 ]
        if( !params.ids ) {
            //res.msg = 'Nenhuma ficha selecionada!'
            //render res as JSON
            return 'Nenhuma ficha selecionada!'
        }

        if( !params.sqCicloAvaliacao ) {
            //res.msg = 'Ciclo de avaliação não informado!'
            //render res as JSON
            return 'Ciclo de avaliação não informado!'
        }

        if( !session?.sicae?.user?.pessoa ) {
            // res.msg = 'Sessão expirada!'
            // render res as JSON
            return 'Sessão expirada!'
        }

        // situacao publicada
        DadosApoio situacaoPublicada = dadosApoioService.getByCodigo( 'TB_SITUACAO_FICHA', 'PUBLICADA' )
        if( !situacaoPublicada ) {
            //res.msg = 'Situação Publicada não cadastrada na tabela de apoio TB_SITUACAO_FICHA!'
            //render res as JSON
            return 'Situação Publicada não cadastrada na tabela de apoio TB_SITUACAO_FICHA!'
        }
        // situacao finalizada
        DadosApoio situacaoFinalizada = dadosApoioService.getByCodigo( 'TB_SITUACAO_FICHA', 'FINALIZADA' )
        if( !situacaoFinalizada ) {
            //res.msg = 'Situação Publicada não cadastrada na tabela de apoio TB_SITUACAO_FICHA!'
            //render res as JSON
            return 'Situação Finalizada não cadastrada na tabela de apoio TB_SITUACAO_FICHA!'
        }

        // situacao finalizada
        DadosApoio situacaoCompilacao = dadosApoioService.getByCodigo( 'TB_SITUACAO_FICHA', 'COMPILACAO' )
        if( !situacaoCompilacao ) {
            return 'Situação Compilação não cadastrada na tabela de apoio TB_SITUACAO_FICHA!'
        }


        // atualizar a Autoria das fichas
        // criar lista das fichas sem Autoria/citacao
        Ficha.createCriteria().list {
            'in'('id', params.ids.split(',').collect{ it.toLong() })
            or {
                isNull('dsCitacao')
                eq('dsCitacao','')
            }
        }.each {
            String autoria = fichaService.getAutoria(it.id.toLong())
            Long sqFicha = it.id
            Ficha.withNewTransaction {
                Ficha.withNewSession {
                    Ficha ficha = Ficha.get(sqFicha)
                    ficha.dsCitacao = autoria
                    ficha.save(flush: true)
                }
            }
        }

        DadosApoio contextoVersao = dadosApoioService.getByCodigo('TB_CONTEXTO_VERSAO_FICHA','VERSAO_PUBLICACAO')
        if( !contextoVersao ){
            throw new Exception('Contexto de versionamento ficha PUBLICACAO não cadastrado na tabela TB_CONTEXTO_VERSAO_FICHA')
        }

        String cmdSql = """update salve.ficha set dt_publicacao = '${ new Date().format( 'yyyy/MM/dd HH:mm:ss' ) }'
            ,sq_usuario_publicacao = :sqUsuarioPublicacao
            ,sq_situacao_ficha = :sqSituacaoCompilacao"""

        Map sqlParams = [ : ]
        sqlParams.sqSituacaoFinalizada = situacaoFinalizada.id.toLong()
        sqlParams.sqSituacaoCompilacao = situacaoCompilacao.id.toLong()
        sqlParams.sqUsuarioPublicacao = session.sicae.user.sqPessoa.toLong()
        cmdSql += "\nwhere ficha.sq_situacao_ficha = :sqSituacaoFinalizada and ficha.sq_ficha = any( array[ ${ params.ids } ] )"
        sqlService.execSql( cmdSql, sqlParams )

        // versionar as fichas
        //List sqlVersionar =[]
        List sqlVersionar =[]
        String sqPessoa = session.sicae.user.sqPessoa.toString()
        params.ids.split(',').each { sqFicha ->
            //sqlVersionar.push("select salve.fn_versionar_ficha(${it},${sqPessoa},${contextoVersao.id} )::text")
            cmdSql = "select salve.fn_versionar_ficha(${sqFicha},${sqPessoa},${contextoVersao.id} )::text"
            Map res = sqlService.execute(cmdSql)
            JSONObject json = JSON.parse(res.data)
            String mensagemRetorno = ''
            if ( json.status == 1) {
                mensagemRetorno = json.msg
                if (json.data.msg) {
                    mensagemRetorno += '\n' + json.data.msg + '\n'
                }
                mensagemRetorno += 'LOG' + json.data.log
            } else {
                if (json.msg) {
                    mensagemRetorno = '\n' + json.msg
                }
            }

            // gravar o resultado do versionamento na tabela ficha_versionamento
            FichaVersionamento.withNewTransaction {
                FichaVersionamento.withNewSession {
                    FichaVersionamento fv = FichaVersionamento.get(sqFicha.toLong())
                    if (!fv) {
                        fv = new FichaVersionamento(ficha: Ficha.get(sqFicha.toLong()))
                    }
                    fv.inStatus = json.status.toInteger()
                    fv.dsLog = Util.trimEditor(mensagemRetorno)
                    fv.save(flush: true)
                }
            }
        }
        //sqlService.execSql( sqlVersionar.join('\nUNION\n') )
        return ''
    }

    /**
     * cancelar a publicação
     *
     * @return
     */
    protected String _cancelar() {
        //Map res = [ msg: '', type: 'error', status: 1 ]
        if( !params.ids ) {
            return 'Nenhuma ficha selecionada!'
            /*res.msg = 'Nenhuma ficha selecionada!'
            render res as JSON
            return*/
        }

        if( !params.sqCicloAvaliacao ) {
            return  'Ciclo de avaliação não informado!'
            /*res.msg = 'Ciclo de avaliação não informado!'
            render res as JSON
            return*/
        }

        if( !session?.sicae?.user?.pessoa ) {
            return 'Sessão expirada!'
            /*res.msg = 'Sessão expirada!'
            render res as JSON
            return*/
        }

        // situacao publicada
        DadosApoio situacaoPublicada = dadosApoioService.getByCodigo( 'TB_SITUACAO_FICHA', 'PUBLICADA' )
        if( !situacaoPublicada ) {
            return 'Situação Publicada não cadastrada na tabela de apoio TB_SITUACAO_FICHA!'
            /*res.msg = 'Situação Publicada não cadastrada na tabela de apoio TB_SITUACAO_FICHA!'
            render res as JSON
            return*/
        }

        // situacao finalizada
        DadosApoio situacaoFinalizada = dadosApoioService.getByCodigo( 'TB_SITUACAO_FICHA', 'FINALIZADA' )
        if( !situacaoPublicada ) {
            return 'Situação Finalizada não cadastrada na tabela de apoio TB_SITUACAO_FICHA!'
            /*res.msg = 'Situação Finalizada não cadastrada na tabela de apoio TB_SITUACAO_FICHA!'
            render res as JSON
            return*/
        }
        // construir sql de update
        String cmdSql = """update salve.ficha set dt_publicacao = null
            ,sq_usuario_publicacao = null
            ,sq_situacao_ficha = :sqSituacaoFinalizada"""

        Map sqlParams = [ : ]
        sqlParams.sqSituacaoFinalizada = situacaoFinalizada.id.toLong()

        //cmdSql += "\nwhere sq_ficha in ( ${ params.ids } )"
        cmdSql += "\nwhere sq_ficha = any( array[ ${ params.ids } ] )"

        /*
    else
        if( params.all == 'S' ) {
            cmdSql += "\nwhere sq_ciclo_avaliacao = :sqCicloAvaliacao and sq_situacao_ficha = :sqSituacaoPublicada"
            sqlParams.sqCicloAvaliacao = params.sqCicloAvaliacao.toLong()
            sqlParams.sqSituacaoPublicada = situacaoPublicada.id.toLong()
        }*/

        // executar sql de update
        //println cmdSql
        sqlService.execSql( cmdSql, sqlParams );
        /*res.msg = 'Publicação realizada com SUCESSO!'
        res.type = 'success'
        res.status = 0
        render res as JSON*/
        return ''
    }

    def selectImages() {
        if( !params.sqFicha ) {
            render '<h3>Ficha não informada</h3>'
            return;
        }
        Ficha fichaSelecionada = Ficha.get( params.sqFicha.toLong() )
        DadosApoio tipoImagem = dadosApoioService.getByCodigo( 'TB_TIPO_MULTIMIDIA', 'IMAGEM' )
        DadosApoio situacaoAprovada = dadosApoioService.getByCodigo( 'TB_SITUACAO_MULTIMIDIA', 'APROVADA' )
        List listImages = FichaMultimidia.createCriteria().list {
            ficha {
                eq( 'taxon', fichaSelecionada.taxon )
            }
            eq( 'tipo', tipoImagem )
            eq( 'situacao', situacaoAprovada )
            order( 'dtElaboracao', 'desc' )
        }
        if( !listImages ) {
            render '<div style="margin:10px;" class="alert alert-danger"><h3>Nenhuma imagem cadastrada na aba Multimidia</div>';
            return;
        }
        render template: 'selectImages', model: [ listImages: listImages ]
    }

    /**
     * atualizar a situacao da imagem para destaque ou não
     * @return
     */
    def updateDestaque() {
        Map res = [ status: 0, msg: '', type: 'success' ]
        try {
            if( !session?.sicae?.user?.pessoa ) {
                throw new Exception( 'Sessão expirada.' )
            }

            if( !params.sqFichaMultimidia ) {
                throw new Exception( 'Id da imagem não informado.' )
            }
            FichaMultimidia fichaMultimidia = FichaMultimidia.get( params.sqFichaMultimidia.toLong() )
            if( !fichaMultimidia ) {
                throw new Exception( 'Id inválido.' )
            }
            fichaMultimidia.inDestaque = ( params.inDestaque == 'S' )
            DadosApoio situacaoPendenteAprovacao = dadosApoioService.getByCodigo( 'TB_SITUACAO_MULTIMIDIA', 'PENDENTE_APROVACAO' )

            if( fichaMultimidia.situacao == situacaoPendenteAprovacao ) {
                DadosApoio situacaoAprovada = dadosApoioService.getByCodigo( 'TB_SITUACAO_MULTIMIDIA', 'APROVADA' )
                fichaMultimidia.situacao = situacaoAprovada
                fichaMultimidia.aprovadoPor = Pessoa.get( session.sicae.user.sqPessoa.toLong() )
            }
            fichaMultimidia.save( flush: true )
            res.msg = 'Atualização realizada com SUCESSO!'
        }
        catch( Exception e ) {
            res.msg = e.getMessage()
            res.type = 'error'
            res.status = 1
        }
        render res as JSON
    }


    /**
     * alterar/gravar o numero do DOI da ficha
     * @return
     */
    def saveDoi() {
        /**
        * OBS: Metodo antigo que alterava o DOI mannualmente na base de daos
        * Substituido pela integração com a crossref gerarAtualizarDoi()
        */
        /*
        Map res = [ status: 0, msg: '', type: 'success' ]

        try {

            if( ! params.sqFicha ) {
                throw new Exception('Ficha não informada')
            }
            Ficha ficha = Ficha.get( params.sqFicha.toLong() )
            if( !ficha ) {
                throw new Exception('Ficha não cadastrada')
            }

            // verificar se já existe o DOI para não duplicar
            if(  params.dsDoi ) {

                Integer qtd = Ficha.createCriteria().count {
                    ne('id', params.sqFicha.toLong())
                    eq('cicloAvaliacao', ficha.cicloAvaliacao )
                    ilike('dsDoi',params.dsDoi)
                }
                if( qtd > 0 ) {
                    throw new Exception('DOI já cadastrado')
                }
            }
            ficha.dsDoi = params.dsDoi
            ficha.save()
        } catch( Exception e) {
            res.type='error'
            res.status=1
            res.msg=e.getMessage()
        }
        render res as JSON
        */
    }

    /**
    * Envia o DOI XML para a CrossRef uma ou mais ficha, para cadastrar ou atualizar
    */
    def gerarAtualizarDoi(){
        Map res = [ status: 0, msg: '', type: 'success' ]
        if( !params.ids )
        {
            res.status = 1
            res.msg = 'Nenhuma ficha selecionada!'
            res.type = 'error'
        }
        if( !session?.sicae?.user?.pessoa )
        {
            res.status = 1
            res.msg = 'Sessão expirada!'
            res.type = 'error'
        }
        if(res.type == 'success')
        {
            res = doiService.enviarDoiFicha(params.ids.toString())
        }
        render res as JSON
    }

    /**
    * Controller que verifica se o DOI está publicado no crossref
    */
    def verificaDoiCrossref(){
        Map res = [ status: 0, msg: '', type: 'success' ]
        if( !params.ids )
        {
            res.status = 1
            res.msg = 'Nenhuma ficha selecionada!'
            res.type = 'error'
        }
        if( !session?.sicae?.user?.pessoa )
        {
            res.status = 1
            res.msg = 'Sessão expirada!'
            res.type = 'error'
        }
        if(res.type == 'success')
        {
            res = doiService.verificaDoiCrossref(params.ids.toString())
        }
        render res as JSON
    }


    /**
     * remover as pendencias de revisao das fichas selecionadas
     */
    def deletePendenciaFichas(){

        Map res = [ status: 0, msg: '', type: 'success' ]

        try {
            if( ! params.ids ) {
                throw new Exception('Nenhuma ficha selecionada')
            }
            DadosApoio contextoRevisao = dadosApoioService.getByCodigo('TB_CONTEXTO_PENDENCIA','REVISAO')
            if( ! contextoRevisao ) {
                throw new Exception('Contexto REVISAO não cadastrado na tabela TB_CONTEXTO_PENDENCIA')
            }
            FichaPendencia.executeUpdate("""delete from FichaPendencia a where a.ficha.id in (${params.ids}) and a.contexto.id=${contextoRevisao.id.toString()}""")
            res.msg = 'Dados gravados com SUCESSO!'
        } catch( Exception e) {
            res.type='error'
            res.status=1
            res.msg=e.getMessage()
            println e.getMessage();
        }
        render res as JSON

    }

    /**
     * excluir uma pendencia especifica
     */
    def deletePendencia() {

        Map res = [ status: 0, msg: '', type: 'success' ]

        try {
            if( ! params.sqFichaPendencia ) {
                throw new Exception('Pendência não informada')
            }
            FichaPendencia pendencia = FichaPendencia.get( params.sqFichaPendencia.toLong())
            if( pendencia ){
                pendencia.delete()
            }
            res.msg = 'Pendência excluída com SUCESSO!'
        } catch( Exception e) {
            res.type='error'
            res.status=1
            res.msg=e.getMessage()
            println e.getMessage();
        }
        render res as JSON
    }


    /**
     * Marcar todas as pendnecias de revisão que estiverem resolvidas como revisadas
     */
    def revisarPendencias(){
        Map res = [ status: 0, msg: '', type: 'success' ]
        try {
            if( ! params.ids ) {
                throw new Exception('Nenhuma ficha selecionada')
            }
            DadosApoio contextoRevisao = dadosApoioService.getByCodigo('TB_CONTEXTO_PENDENCIA','REVISAO')
            if( ! contextoRevisao ) {
                throw new Exception('Contexto REVISAO não cadastrado na tabela TB_CONTEXTO_PENDENCIA')
            }
            PessoaFisica pessoa = PessoaFisica.get( session.sicae.user.sqPessoa )
            FichaPendencia.executeUpdate("""update FichaPendencia a
                set usuarioRevisou = :pessoa, dtRevisao = :dtRevisao
                where a.ficha.id in (${params.ids})
                  and dtResolvida is not null
                and a.contexto.id=${contextoRevisao.id.toString()}""",[pessoa:pessoa,dtRevisao:new Date()])
            res.msg = 'Dados gravados com SUCESSO!'
        } catch( Exception e) {
            res.type='error'
            res.status=1
            res.msg=e.getMessage()
            println e.getMessage();
        }
        render res as JSON
    }


    /**
     * adicionar pendencia de revisao das fichas selecionadas
     */
    def savePendenciaFichas(){
        Map res = [ status: 0, msg: '', type: 'success' ]

        try {
            if( ! params.ids ) {
                throw new Exception('Nenhuma ficha selecionada')
            }
            if( ! params.text ) {
                throw new Exception('Texto da pendência não informado')
            }
            DadosApoio pendenciaRevisao = dadosApoioService.getByCodigo('TB_CONTEXTO_PENDENCIA','REVISAO')
            if( !pendenciaRevisao ) {
                throw new Exception('Contexto REVISAO não cadastrado na tabela TB_CONTEXTO_PENDENCIA')
            }

            params.ids.split(',').each{ sqFicha ->

                FichaPendencia fichaPendencia = null
                Ficha ficha = Ficha.get( sqFicha.toLong() )
                /*
                // cada ficha so pode ter 1 pendencia de revisao
                FichaPendencia fichaPendencia = FichaPendencia.createCriteria().get {
                    eq('contexto',pendenciaRevisao)
                    eq('ficha',ficha )
                }*/
                // se a pendencia existir ignorar
                if( ! fichaPendencia ){
                    fichaPendencia             = new FichaPendencia()
                    fichaPendencia.contexto    = pendenciaRevisao
                    fichaPendencia.ficha       = ficha
                    fichaPendencia.usuario     = PessoaFisica.get( session.sicae.user.sqPessoa.toLong() )
                    fichaPendencia.stPendente  = 'S'
                    fichaPendencia.dtPendencia = new Date()
                    fichaPendencia.txPendencia = params.text
                    fichaPendencia.save()
                }
            }
            res.msg = 'Dados gravados com SUCESSO!'
        } catch( Exception e) {
            res.type='error'
            res.status=1
            res.msg=e.getMessage()
            println e.getMessage();
        }
        render res as JSON
    }

    /**
     * Marcar a pendencia como revisada ou não, se ja tiver revisada remove a revisao senão grava a revisao
     * @return
     */
    def updateSituacaoRevisao(){
        Map res = [status:0,msg:'Dados gravados com SUCESSO!',type:'success', data:[stRevisado:'N', log:''] ]
        try{

            if( !params.sqFicha ){
                throw new Exception('Ficha não informada')
            }
            if( !params.sqFichaPendencia ){
                throw new Exception('Pendência não informada')
            }
            //def ficha = Ficha.executeQuery("select new map(f.id as sqFicha) from Ficha f where f.id = :id",[id:params.sqFicha.toLong()])
            FichaPendencia fichaPendencia = FichaPendencia.createCriteria().get {
                createAlias('vwFicha', 'vw')
                eq('vw.id', params.sqFicha.toLong() )
                eq('id',params.sqFichaPendencia.toLong() )
            }
            if( !fichaPendencia ){
                throw new Exception('Pendência não encontrada')
            }

            if( fichaPendencia.dtRevisao ){
                fichaPendencia.usuarioRevisou = null
                fichaPendencia.dtRevisao = null
                res.data.stRevisado='N'
            } else {
                fichaPendencia.usuarioRevisou = PessoaFisica.get( session.sicae.user.sqPessoa.toLong() )
                fichaPendencia.dtRevisao = new Date()
                res.data.stRevisado='S'
            }
            fichaPendencia.save()
            res.data.log        = fichaPendencia.logRevisao()
        } catch( Exception e ){
            res.status  = 1
            res.type    ='error'
            res.msg     = e.getMessage()
            println e.getMessage()
        }
        render res as JSON
    }


    // integração SIS/IUCN
    def verLogTransmissao(){
        Map res = [ status: 0, msg: '', type: 'success', data:[:] ]
        try {
            if( !session?.sicae?.user?.pessoa ) {
                throw new Exception( 'Sessão expirada.' )
            }

            if( !params.sqIucnTransmissao ) {
                throw new Exception( 'Id da transmissao não informado.' )
            }

            if( !params.sqFicha ) {
                throw new Exception( 'Id da ficha não informado.' )
            }

            /*Ficha ficha = Ficha.get( params.sqFicha.toLong() )
            if( !ficha ){
                throw new Exception( 'Id ficha inválido.' )
            }*/


            IucnTransmissao transmissao = IucnTransmissao.get( params.sqIucnTransmissao.toLong() )

            if( ! transmissao ) {
                throw new Exception( 'Id transmissão inválido.' )
            }

            List linhas = transmissao.txLog.split(/\n/).findAll{
                String key = ': '+params.sqFicha.toString()+'.'
                return it.toString().indexOf(key) > 0
            }
            res.msg = ''
            // destacar na cor vermelha o nome das colunas com problema
            res.data.log = '<div style="margin:10px;margin-top:10px;">' + linhas.collect {
                it = it.replaceAll(/Campo ([a-z]{2}_[a-z_]{1,})/,'Campo <span style="color: #e60000;">$1</span>')
            }.join('<br>')+'<div>'
        }
        catch( Exception e ) {
            res.msg = e.getMessage()
            res.type = 'error'
            res.status = 1
        }
        render res as JSON
    }

    /**
     * transmitir as fichas selecionadas para o SIS/IUCN
     */
    protected String _transmitirIucn() {

        if( !session?.sicae?.user?.pessoa ) {
            return 'Sessão expirada.'
        }

        Long sqPessoa = session.sicae.user.sqPessoa.toLong()
        String eMail = session.sicae.user.email

        if( ! eMail ) {
            return 'E-mail não encontrado.'
        }

        if( ! params.ids ) {
            return 'Nenhuma ficha selecionada.'
        }

        /** /
        println ' '
        println 'TRANSMITIR SIS/IUCN'
        println 'IDS:'
        println params.ids.getClass()
        println params.ids
        println 'Pessoa:' + sqPessoa
        println 'email:' + eMail
        return ''
        /**/

        List<Long> listIds = params.ids.split(',').collect{ it.toLong() }
        Map res = iucnService.gerarZip( listIds, sqPessoa, eMail )
        return ( res.status == 1 ? res.msg : '' )
    }




}
