package br.gov.icmbio

class FichaRefBibController extends FichaBaseController {

    def index() {
		Ficha ficha = Ficha.get(params.sqFicha)
		render(template:"/ficha/refBib/index", model:['ficha': ficha]);
    }
}
