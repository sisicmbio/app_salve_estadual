package br.gov.icmbio

import grails.converters.JSON

class CorpEstruturaOrgController extends BaseController {

    def corpEstruturaOrgService
    def instituicaoService

    /**
     * Exibir o formulário de manutenção da Estrutura Organizacional
     * @return
     */
    def index() {
        render(view:'index',model:[ instituicoes: instituicaoService.estruturaOrg() ] )
    }

    /**
     * Gravar/atualizar a Estrutura Organizacional no banco de dados
      * @return
     */
    def save(){
        Map res = [status:0, type:'success', msg:'Unidade gravada com SUCESSO', data:[:] ]

        try {
           res.data = corpEstruturaOrgService.save(   params.sqPai ? params.sqPai.toLong() : null
                                   , params.sqPessoa ? params.sqPessoa.toLong() : null
                                   , params.noPessoa      ?:null
                                   , params.sgInstituicao ?:null
                                   , params.stInterna.toBoolean()
                                   , params.noContato     ?:null
                                   , params.nuTelefone    ?:null
                                   , params.deEmail       ?:null
                                   , params.nuCnpj        ?:null
           )

        } catch( Exception e ){
            res.status = 1
            res.type = 'error'
            res.msg = e.getMessage()
            println res.msg
        }
        render res as JSON
    }

    /**
     * retornar os dados da Estrutura Organizacional para edição
     * @return
     */
    def edit(){
        Map res = [status:0, type:'success', msg:'', data:[:] ]
        try {
            res.data = corpEstruturaOrgService.edit(  params.sqPessoa ? params.sqPessoa.toLong() : 0l)
        } catch( Exception e ){
            res.status = 1
            res.type = 'error'
            res.msg = e.getMessage()
        }
        render res as JSON
    }

    /**
     * excluir a Estrutura Organizacional do banco de dados
     * @return
     */
    def delete(){
        Map res = [status:0, type:'success', msg:'Unidade organizacional excluída com SUCESSO', data:[:] ]
        try {
            res.data = corpEstruturaOrgService.delete(  params.sqPessoa ? params.sqPessoa.toLong() : 0l)
        } catch( Exception e ){
            res.status = 1
            res.type = 'error'
            res.msg = e.getMessage()
        }
        render res as JSON
    }

    /**
     * criar o gride da esttutura organizacional
     * @return
     */
    def treeview() {
        try {
            List result = corpEstruturaOrgService.treeview( params.id ? params.id.toLong() : null )
            render result as JSON
        } catch (Exception e) {
            render e.getMessage()
        }
    }
}
