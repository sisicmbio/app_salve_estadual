package br.gov.icmbio

import grails.converters.JSON

class CorpEstadoController extends BaseController {

    def corpEstadoService

    /**
     * Exibir o formulário de manutenção dos Estados
     * @return
     */
    def index() {
        render(view:'index',model:[regioes:Regiao.list() ] )
    }

    /**
     * Gravar/atualizar o Estado no banco de dados
      * @return
     */
    def save(){
        Map res = [status:0, type:'success', msg:'Estado gravado com SUCESSO', data:[:] ]
        try {
            corpEstadoService.save(  params.sqEstado ? params.sqEstado.toLong() : null
                                   , params.noEstado ?:''
                                   , params.sgEstado ?:''
                                   , params.sqRegiao ? params.sqRegiao.toLong(): null
                                   , params.wkt      ?:'')

        } catch( Exception e ){
            res.status = 1
            res.type = 'error'
            res.msg = e.getMessage()
        }
        render res as JSON
    }

    /**
     * retornar os dados do Estado paro edição
     * @return
     */
    def edit(){
        Map res = [status:0, type:'success', msg:'', data:[:] ]
        try {
            res.data = corpEstadoService.edit(  params.sqEstado ? params.sqEstado.toLong() : 0l)
        } catch( Exception e ){
            res.status = 1
            res.type = 'error'
            res.msg = e.getMessage()
        }
        render res as JSON
    }

    /**
     * excluir o Estado do banco de dados
     * @return
     */
    def delete(){
        Map res = [status:0, type:'success', msg:'Estado excluído com SUCESSO', data:[:] ]
        try {
            res.data = corpEstadoService.delete(  params.sqEstado ? params.sqEstado.toLong() : 0l)
        } catch( Exception e ){
            res.status = 1
            res.type = 'error'
            res.msg = e.getMessage()
        }
        render res as JSON
    }

    /**
     * criar o gride dos Estados
     * @return
     */
    def grid(){
        try {
            render(template: 'gridEstado', model: [estados: Estado.list()])
        } catch( Exception e ) {
            render e.getMessage()
        }
    }
}
