/**
 * Este controlador é resposável por receber as ações referentes ao cadastro aba Conservação da ficha de espécies
 * Regras:
 *
 *
 *
 *
 *
 */

package br.gov.icmbio
import grails.converters.JSON

class FichaConservacaoController extends FichaBaseController {

	def dadosApoioService
	def messageSource
    def fichaService

	def index() {

		if( !params.sqFicha )
		{
			render 'id da ficha não informado.'
			return
		}
		// dados da aba 7.1
		Ficha ficha 				= Ficha.get(params.sqFicha.toLong())
		List listTipoAvaliacao 		= dadosApoioService.getTable('TB_TIPO_AVALIACAO').sort{ it.ordem }
		List listTipoAbrangencia 	= dadosApoioService.getTable('TB_TIPO_ABRANGENCIA').sort{ it.ordem }

		//List listCategoriaIucn	= dadosApoioService.getTable('TB_CATEGORIA_IUCN').sort{ it.ordem }
		// Solicitado em reunião de 04/05/2022 a não exibição de: AMEACADA|NE|POSSIVELMENTE_EXTINTA PEX
		List listCategoriaIucn		= dadosApoioService.getTable('TB_CATEGORIA_IUCN').findAll{
			it.codigoSistema != 'AMEACADA' && it.codigoSistema != 'NE' && it.codigoSistema != 'POSSIVELMENTE_EXTINTA'
		}.sort { it.ordem }

		render(template:"/ficha/conservacao/index",model:['ficha':ficha,
							'listTipoAvaliacao':listTipoAvaliacao,
							'listTipoAbrangencia':listTipoAbrangencia,
							'listCategoriaIucn':listCategoriaIucn]);
	}

	def getGridHistoricoAvaliacao() {
		if (!params.sqFicha) {
			render ''
			return
		}
		VwFicha vf = VwFicha.get(params.sqFicha.toLong())
		if (!vf) {
			render 'Id da ficha inválido'
			return
		}
		DadosApoio nacionalBrasil = dadosApoioService.getByCodigo('TB_TIPO_AVALIACAO','NACIONAL_BRASIL')

		//List listFichaHistoricoAvaliacao = FichaHistoricoAvaliacao.findAllByFicha(Ficha.get(params.sqFicha)).sort{it.nuAnoAvaliacao.toString()+it.tipoAvaliacao.ordem}
		List listHistorico = TaxonHistoricoAvaliacao.createCriteria().list {
			eq('taxon.id', vf.sqTaxon.toLong())

            // mostrar todas as avaliações do taxon independentes do ciclo selecionado
            /*or {
                le('nuAnoAvaliacao', (vf.cicloAvaliacao.nuAno + 4))
                ne('tipoAvaliacao', nacionalBrasil)
            }*/
/*
			or {
				le('nuAnoAvaliacao', (vf.cicloAvaliacao.nuAno+4) )
				and {
					le('nuAnoAvaliacao', (vf.cicloAvaliacao.nuAno + 4))
					ne('tipoAvaliacao', nacionalBrasil)
				}
			}
			*/
			tipoAvaliacao {
				order('ordem')
			}
			order('nuAnoAvaliacao', 'desc')

		}
		//findAllByTaxon(Ficha.get(params.sqFicha)).sort { it.nuAnoAvaliacao.toString() + it.tipoAvaliacao.ordem }
		render(template: '/ficha/conservacao/historicoAvaliacao/divGridHistoricoAvaliacao', model: [vwFicha:vf,'listHistorico': listHistorico])
	}

/*
	def select2Abrangencia()
	{
		Map data = ['items':[]];
		Map  mapOptions=[:]
		if( params.sqTipoAbrangencia)
		{
			if( params.coTipoAbrangencia == 'BACIA_HIDROGRAFICA' )
			{
				mapOptions.sort= 'nuOrdem';
			}
			else
			{
				mapOptions.sort='deAbrangencia';
			}
			List lista = Abrangencia.findAllByTipoAbrangencia(DadosApoio.get(params.sqTipoAbrangencia),mapOptions);
			lista.each {
				if( params.coTipoAbrangencia == 'BACIA_HIDROGRAFICA')
				{
					data.items.push( 'id':it.id,'descricao': (it.coAbrangencia?it.coAbrangencia+'-':'')+it.deAbrangencia)
				}
				else
				{
					data.items.push( 'id':it.id,'descricao': it.deAbrangencia )
				}
				//data.items.push( 'id':it.id,'descricao': (it.coAbrangencia?it.coAbrangencia+'-':'')+it.deAbrangencia)
			}
		}
		render data as JSON;
	}
*/
	def saveFrmConHistorico() {
		DadosApoio tipoAbrangencia
		DadosApoio tipoAvaliacao
		DadosApoio categoria
		VwFicha vwFicha

		if (!params.sqFicha) {
			this._returnAjax(1, 'ID da ficha não informado.', "error")
			return
		}

		if (!params.sqTipoAvaliacao) {
			this._returnAjax(1, 'Tipo da avaliação não informado.', "error")
			return
		}

        // this._returnAjax(1, 'Teste parametros.', "info")

		tipoAvaliacao = DadosApoio.get(params.sqTipoAvaliacao.toLong())

		vwFicha = VwFicha.get(params.sqFicha.toLong())

		// não pode haver duas avaliações nacionais para o mesmo taxon
		if( tipoAvaliacao.codigoSistema=='NACIONAL_BRASIL')
		{
			Integer qtd = TaxonHistoricoAvaliacao.createCriteria().count {
				eq('taxon.id', vwFicha.sqTaxon.toLong())
				eq('tipoAvaliacao', tipoAvaliacao)
				eq('nuAnoAvaliacao', params.nuAnoAvaliacao.toInteger() )
				if( params.sqTaxonHistoricoAvaliacao ) {
					ne('id', params.sqTaxonHistoricoAvaliacao.toLong())
				}
			}
			if( qtd > 0 )
			{
				this._returnAjax(1, 'Avaliação Nacional de ' + params.nuAnoAvaliacao + ' já cadastrada.', "error")
				return
			}
            if( ! params.sqTaxonHistoricoAvaliacao) {
                // avaliação nacional só pode ser adicionada se for anterior ao ciclo atual
                if( params.nuAnoAvaliacao.toInteger() >= vwFicha.cicloAvaliacao.nuAno.toInteger() ) {
                    this._returnAjax( 1, 'Ano informado deve ser menor que o início do ciclo atual (' + ( vwFicha.cicloAvaliacao.nuAno.toInteger() ) + ').', "error" )
                    return
                }
            }
		}

		if (!params.sqCategoriaIucn) {
			this._returnAjax(1, 'Categoria não informada.', "error")
			return
		}
		categoria = DadosApoio.get(params.sqCategoriaIucn.toLong())

		if ( categoria.codigoSistema == 'OUTRA') {
			if (!params.deCategoriaOutra) {
				this._returnAjax(1, 'Nome da outra categoria não informado.', "error")
				return
			}
		} else {
			params.deCategoriaOutra = null
		}

		TaxonHistoricoAvaliacao reg

		if (params.sqTaxonHistoricoAvaliacao) {
			reg = TaxonHistoricoAvaliacao.get(params.sqTaxonHistoricoAvaliacao.toLong())
		}else {
			reg = new TaxonHistoricoAvaliacao()
			reg.taxon = vwFicha.taxon
		}

		reg.tipoAvaliacao = tipoAvaliacao
		reg.categoriaIucn = categoria

		if (!params.stPossivelmenteExtinta) {
			params.stPossivelmenteExtinta = null
		}
		if (reg?.categoriaIucn?.codigoSistema != 'CR') {
			params.stPossivelmenteExtinta = null
		}else {
			if (!params.stPossivelmenteExtinta) {
				params.stPossivelmenteExtinta = 'N'
			}
		}

		// se não for ameacada nao tem criterio
		if ( !  (reg?.categoriaIucn?.codigoSistema ==~ /CR|VU|EN/ ) ) {
			params.dsCriterioAvalIucn = null
		}
		reg.stPossivelmenteExtinta = params.stPossivelmenteExtinta ?: null
		reg.stOficial = params.stOficial ? params.stOficial=='true': false
		reg.abrangencia = null
		reg.noRegiaoOutra = null
		if (params.sqTipoAbrangencia) {
			tipoAbrangencia = DadosApoio.get(params.sqTipoAbrangencia.toLong())
			if (tipoAbrangencia?.codigoSistema != 'OUTRA') {
                if( params.sqAbrangencia ) {
                    reg.abrangencia = Abrangencia.get(params.sqAbrangencia.toLong())
                }
			}else {
				reg.noRegiaoOutra = params.noRegiaoOutra
			}
		}
		reg.nuAnoAvaliacao = params.nuAnoAvaliacao.toInteger()
		reg.deCategoriaOutra = params.deCategoriaOutra ?: null
		reg.deCriterioAvaliacaoIucn = params.dsCriterioAvalIucn ?: null
		reg.txJustificativaAvaliacao = params.txJustificativaAvaliacao ?: null
		if (!reg.save(flush: true)) {
			println reg.getErrors()
			println '-' * 100
			println ' '
			this._returnAjax(1, 'Erro ao gravar', "error")
		}

		// atualizar log da ficha, quem alterou
		Ficha ficha = Ficha.get( params.sqFicha.toLong() )
		ficha.dtAlteracao = new Date()
		ficha.save()

		this._returnAjax(0, getMsg(1), "success")
	}

	def editHistoricoAvaliacao(){
    	TaxonHistoricoAvaliacao reg = TaxonHistoricoAvaliacao.get(params.id.toLong() )
		if( ! reg )
		{
			render [:] as JSON
			return
		}
		render reg.asJson()
	}

	def deleteHistoricoAvaliacao(){
		TaxonHistoricoAvaliacao reg = TaxonHistoricoAvaliacao.get(params.id.toLong() )
		if( ! reg )
		{
			this._returnAjax(1, getMsg(51), "error")
			return
		}

        // excluir a referencia bibliografica
        FichaRefBib.createCriteria().list {
            eq('noTabela'   , 'taxon_historico_avaliacao')
            eq('noColuna'   , 'sq_taxon_historico_avaliacao')
            eq('sqRegistro' , reg.id.toInteger())
        }.each {
            it.delete()
        }
		reg.delete()

		// atualizar log da ficha, quem alterou
		Ficha ficha = Ficha.get( params.sqFicha.toLong() )
		ficha.dtAlteracao = new Date()
		ficha.save(flush:true)

		this._returnAjax(0, getMsg(4), "success")
	}

    def saveFrmConHistoricoObs() {
        VwFicha vwFicha
        if (!params.sqFicha) {
            this._returnAjax(1, 'ID da ficha não informado.', "error")
            return
        }
        Ficha ficha = Ficha.get(params.sqFicha.toLong());
        if( ficha ) {
            ficha.dsHistoricoAvaliacao = Util.trimEditor( params.dsHistoricoAvaliacao )
            ficha.save( flush:true )
            this._returnAjax(0, 'Dados gravados com SUCESSO!', "success")
        }
        this._returnAjax(1, 'ID da ficha não existe.', "error")
    }

    def savePresencaListaVigente() {
        if( !params.sqFicha ){
            this._returnAjax(1, 'ID da ficha não informado.', "error")
            return
        }
        if( !params.stPresencaListaVigente ){
            this._returnAjax(1, 'Parâmetros incorretos.', "error")
            return
        }
        Ficha ficha = Ficha.get( params.sqFicha.toLong() )
        ficha.stPresencaListaVigente = params.stPresencaListaVigente
        ficha.save()
        this._returnAjax(0, 'Ficha atualizada com SUCESSO.', "success")
    }


	def saveOficial(){
		params.format = params.format ?: 'pdf'
		if( !params.sqFicha ){
			return this._returnAjax(1, 'ID da ficha não informado.', "error")
		}
		if( !params.id ){
			return this._returnAjax(1, 'ID do histórico não informado.', "error")
		}

		Ficha ficha = Ficha.get( params.sqFicha.toLong())
		TaxonHistoricoAvaliacao th = TaxonHistoricoAvaliacao.findByTaxonAndId(ficha.taxon,params.id.toLong())
		if( !th ){
			return this._returnAjax(1, 'Parâmetros incorretos.', "error")
		}
		th.stOficial = ! th.stOficial
		th.save()
		return this._returnAjax(0, 'Dados gravados com SUCESSO', "success",[stOficial:th.stOficial])
	}


	def visualizarFicha(){
		params.format = params.format ?: 'pdf'
		if( !params.sqFicha ){
			return this._returnAjax(1, 'ID da ficha não informado.', "error")
		}
		if( !params.id ){
			return this._returnAjax(1, 'ID do histórico não informado.', "error")
		}

		if( !params.idOrigem ){
			return this._returnAjax(1, 'ID origem não informado.', "error")
		}
		TaxonHistoricoAvaliacao th = TaxonHistoricoAvaliacao.get( params.id.toLong() )
		if( ! th || th.idOrigem.toLong() != params.idOrigem.toLong() ) {
			return this._returnAjax(1, 'ID origem inválido.', "error")
		}

		Ficha ficha = Ficha.get( params.sqFicha.toLong())
		if( !ficha ){
			return this._returnAjax(1, 'ID inválido.', "error")
		}
		String urlApiSalve = grailsApplication.config.url.salve.icmbio.api ?: 'https://salve.icmbio.gov.br/salve-api/'
		urlApiSalve += 'fichaEspecie/'+ficha.taxon.noCientifico.replaceAll(/\s/,'%20')+'?type='+params.format
		Map data=[urlDownload:urlApiSalve  ]
		return this._returnAjax(0, '','success',data)
	}

	//---------------------------------------------------------------------------------------------------

	def tabConListaConvencao(){
		if( !params.sqFicha )
		{
			render 'Id da ficha deve ser informado!'
			return;
		}
		Ficha ficha 			= Ficha.get(params.sqFicha)

		//List listListasConvencoes = dadosApoioService.getTable('TB_CONVENCOES').sort{ it.ordem }
		/***********
		* Regra alterada mediante solicitação do Coordenador juntamente com equipe em 03/Jun/2022
		***********/
		List listListasConvencoes = dadosApoioService.getTable('TB_CONVENCOES').findAll {
            // não listar as opções das CITES porque já está sendo adidionado automaticamente pela api do speciesplus
            return ! (it.codigoSistema =~ /CITES/ ) && ( session.sicae.user.isADM() || (it.codigoSistema != 'LISTA_OFICIAL_AMEACADA_2014' && it.codigoSistema != 'LISTA_OFICIAL_AMEACADA_2022') )
        }.sort {
            it.ordem
        };

        // ler última verificação das listas CITES no speciesplus
        DadosApoio tipoCites = dadosApoioService.getByCodigo('TB_TIPO_SINCRONISMO','SINCRONISMO_CITES')
        FichaSincronismo fichaSincronismoCites = FichaSincronismo.findByFichaAndTipo(ficha,tipoCites )

   		FichaRefBib atoOficial = FichaRefBib.findByFichaAndNoTabelaAndNoColunaAndSqRegistro(ficha,'ficha','st_presenca_lista_vigente',ficha.id);
		render(template:"/ficha/conservacao/listaConvencao/formListaConvencao",model:[ficha:ficha
                                                                                      ,listListasConvencoes:listListasConvencoes
                                                                                      ,fichaSincronismoCites:fichaSincronismoCites
                                                                                      ,atoOficial:atoOficial]);
	}

    /*
	def saveFrmConListaConvencao(){
		if( !params.sqFicha )
		{
			this._returnAjax(1, 'ID da ficha não informado.', "error")
			return
		}
//		if( ! params.sqListaConvencao )
//		{
//			this._returnAjax(1, 'Selecione uma Lista/Convenção.', "error")
//			return
//		}
//
		Ficha ficha = Ficha.get(params.sqFicha)
		ficha.properties = params
        try {
            ficha.save(flush:true)
       		FichaRefBib frb = FichaRefBib.findByFichaAndNoTabelaAndNoColunaAndSqRegistro(ficha,'ficha','st_presenca_lista_vigente',ficha.id);
            if( ficha.stPresencaListaVigente != 'S' ) {
                params.sqAtoOficial=null
            }

            // não existe mais o vinculo o campo Presença em Lista Oficial com o cadastro de listas e convenções
//            if( ficha.stPresencaListaVigente != 'S' ) {
//            	FichaListaConvencao.findAllByFicha( ficha ).each
//            	{
//            		it.delete(flush:true, failOnError:true)
//            	}
//            	if( frb )
//            	{
//            		frb.delete(flush:true);
//            	}
//            }
//            else
//            {
            	// gravar a ref bibliográfica - ato oficial
            	if( params.sqAtoOficial)
            	{
	            	if( ! frb )
	            	{
    	        		frb =  new FichaRefBib();
    	        		frb.ficha = ficha;
    	        		frb.noTabela='ficha';
    	        		frb.noColuna='st_presenca_lista_vigente';
    	        		frb.deRotulo='Presença em Lista Oficial Vigente';
    	        		frb.sqRegistro=ficha.id;
        	    	}
        	    	if( frb?.publicacao?.id != params.sqAtoOficial )
        	    	{
        	   			frb.publicacao = Publicacao.get( params.sqAtoOficial);
			    		frb.save(flush:true);
			    	}
            	}
            	else
            	{
	            	if( frb )
	            	{
	            		frb.delete(flush:true);
	            	}
            	}
            //}

            this._returnAjax(0, getMsg(1), "success")
        } catch(Exception e) {
            println e.getMessage()
            this._returnAjax(1, getMsg(2), "error")
        }
	}
*/

	def saveFrmConListaConvencaoDetalhe()
	{
		if( !params.sqFicha )
		{
			this._returnAjax(1, 'ID da ficha não informado.', "error")
			return
		}
		Ficha ficha = Ficha.get(params.sqFicha)
        DadosApoio documento = DadosApoio.get(params.sqListaConvencao)
        FichaListaConvencao reg = FichaListaConvencao.createCriteria().get{
            eq('ficha',ficha)
            eq('listaConvencao',documento)
            if( params.nuAno){
                eq('nuAno', params.nuAno.toInteger())
            } else {
                isNull( 'nuAno')
            }
        }
        if( ! reg ) {
            reg = new FichaListaConvencao();
        }
		reg.properties = params
		reg.ficha = ficha
		reg.listaConvencao = documento

        try {
            reg.save(flush:true)
            // nao tem mais o vinculo da presença com a lista oficial e o gride de Listas e conveções
			/*if( ficha.stPresencaListaVigente != 'S' )
            {
	            ficha.stPresencaListaVigente = 'S'
        	}*/
			// atualizar log da ficha, quem alterou
			ficha.dtAlteracao = new Date()
            ficha.alteradoPor = PessoaFisica.get( session.sicae.user.sqPessoa)
			ficha.save(flush:true)

			this._returnAjax(0, getMsg(1), "success")
        } catch(Exception e) {
            println e.getMessage()
            this._returnAjax(1, getMsg(2), "error")
        }
	}

	def getGridListaConvencao()
	{
		if( !params.sqFicha )
		{
			render 'Id da ficha deve ser informado!'
			return;
		}
	    List listFichaListaConvencao = FichaListaConvencao.findAllByFicha(Ficha.get(params.sqFicha))
		render(template:"/ficha/conservacao/listaConvencao/divGridListaConvencao",model:['listFichaListaConvencao':listFichaListaConvencao])
	}

	def deleteListaConvencao()
	{
		FichaListaConvencao reg = FichaListaConvencao.get(params.id);
		if( reg )
		{
			Ficha ficha = reg.ficha
            boolean  isCites = ( reg.listaConvencao.codigoSistema =~ /CITES/ )
			reg.delete(flush:true);

			// atualizar log da ficha, quem alterou
			ficha.dtAlteracao = new Date()
			ficha.save()

            // quando for exclusão de uma CITES, verificar e retirar a pendencia se houver
            if( isCites ){
                fichaService.removerPendenciaDuplicidadeCites( ficha.id )
            }

			this._returnAjax(0, getMsg(4), "success")
			return
		}
		else
		{
			this._returnAjax(1, getMsg(51), "error")
			return
		}
		render '';
	}

//---------------------------------------------------------------------------------------------------

	def tabConAcaoConservacao()
	{
		if( !params.sqFicha )
		{
			render 'Id da ficha deve ser informado!'
			return;
		}
		Ficha ficha 			= Ficha.get(params.sqFicha)
		List listSituacaoAcaoConservacao 	= dadosApoioService.getTable('TB_STATUS_ACAO_CONSERVACAO').sort{ it.ordem }
		List listTipoOrdenamento 			= dadosApoioService.getTable('TB_ORDENAMENTO_PESCA').sort{ it.ordem }
		List listAcaoConservacao 			= dadosApoioService.getTable('TB_ACAO_CONSERVACAO').sort{ it.ordem };

		render(template:"/ficha/conservacao/acaoConservacao/formAcaoConservacao",model:['ficha':ficha
				,'listSituacaoAcaoConservacao':listSituacaoAcaoConservacao
				,'listTipoOrdenamento':listTipoOrdenamento
				,'listAcaoConservacao':listAcaoConservacao]);
	}

	def saveFrmConAcaoConservacao()
	{

		if( !params.sqFicha )
		{
			this._returnAjax(1, 'ID da ficha não informado.', "error")
			return
		}
		Ficha ficha = Ficha.get(params.sqFicha)
		ficha.properties = params
        try {
            ficha.save(flush:true)
            this._returnAjax(0, getMsg(1), "success")
        } catch(Exception e) {
            println e.getMessage()
            this._returnAjax(1, getMsg(2), "error")
        }
	}

	def select2PlanoAcao()
	{
		String q = params.q
		Map data = ['items':[] ]
		List lista = PlanoAcao.findAllBySgPlanoAcaoIlikeAndSgPlanoAcaoLike('%'+q+'%','%PAN %')
        lista.each {
			data.items.push(['id':it.id,'descricao':it.sgPlanoAcao])
        }
        render data as JSON
	}
	def select2AcaoConservacao()
	{
		String q = params.q
		Map data = ['items':[] ]
		List lista 			= dadosApoioService.getTable('TB_ACAO_CONSERVACAO').sort{ it.ordem }

		/*List lista = AcaoConservacao.findAllByDescricaoIlike('%'+q+'%')

		// ordenar codigos: 1.1, 10.1, 1.1.1
        lista.sort { a,b ->
            if( a.codigo && b.codigo )
            {
                String cA,cB;
                Integer pA  = a.codigo.indexOf('.')
                Integer pB  = b.codigo.indexOf('.')
                if( pA > 0)
                {
                    cA = a.codigo.substring(0,pA)
                    cA = cA.padLeft(3,'0')+a.codigo.substring(pA)
                }
                if( pB > 0)
                {
                    cB = b.codigo.substring(0,pB)
                    cB = cB.padLeft(3,'0')+b.codigo.substring(pB)

                }
                if( cA && cB)
                {
                    return cA <=> cB
                }
            }
            return a.codigo <=> b.codigo
        }
        */

        lista.each {
			data.items.push(['id':it.id,'descricao':it.descricaoCompleta,'codigo':it.codigo,'codigoSistema':it.codigoSistema])
        }
        render data as JSON
	}

	def saveFrmConAcaoConservacaoDetalhe() {
		if( !params.sqFicha )
		{
			this._returnAjax(1, 'ID da ficha não informado.', "error")
			return
		}
		FichaAcaoConservacao reg;
		if( params.sqFichaAcaoConservacao )
		{
			reg = FichaAcaoConservacao.get(params.sqFichaAcaoConservacao)
		}
		else
		{
			reg 		= new FichaAcaoConservacao()
			reg.ficha 	= Ficha.get(params.sqFicha)
		}
		reg.properties = params
		reg.acaoConservacao 		= DadosApoio.get(params.sqAcaoConservacao)
		reg.situacaoAcaoConservacao = DadosApoio.get( params.sqSituacaoAcaoConservacao)
		reg.planoAcao=null;
		reg.tipoOrdenamento=null;
		if( params.sqPlanoAcao) {
			reg.planoAcao 				= PlanoAcao.get( params.sqPlanoAcao )
		}
		if( params.sqTipoOrdenamento ) {
			reg.tipoOrdenamento 		= DadosApoio.get( params.sqTipoOrdenamento )
		}
        try {
            reg.save(flush:true)

			// atualizar log da ficha, quem alterou
			reg.ficha.dtAlteracao = new Date()
			reg.ficha.save()


			this._returnAjax(0, getMsg(1), "success")
        } catch(Exception e) {
            println e.getMessage()
            this._returnAjax(1, getMsg(2), "error")
        }

	}

	def getGridAcaoConservacao()
	{
		if( !params.sqFicha)
		{
			render ''
			return
		}
		List listFichaAcaoConservacao = FichaAcaoConservacao.findAllByFicha(Ficha.get(params.sqFicha))
		render( template:'/ficha/conservacao/acaoConservacao/divGridAcaoConservacao',model:['listFichaAcaoConservacao':listFichaAcaoConservacao])
	}

	def editAcaoConservacao()
	{
    	FichaAcaoConservacao reg = FichaAcaoConservacao.get(params.id);
		if( reg?.id )
		{
			render reg.asJson();
			return;
		}
		render [:] as JSON;
	}

	def deleteAcaoConservacao()
	{
    	FichaAcaoConservacao reg = FichaAcaoConservacao.get(params.id);
		if( reg )
		{
			Ficha ficha = reg.ficha
			reg.delete(flush:true)

			// atualizar log da ficha, quem alterou
			reg.ficha.dtAlteracao = new Date()
			reg.ficha.save()

			this._returnAjax(0, getMsg(4), "success")
			return
		}
		else
		{
			this._returnAjax(1, getMsg(51), "error")
			return
		}
		render '';
	}

//---------------------------------------------------------------------------------------------------

	def tabConPresencaUC()
	{
		if( !params.sqFicha )
		{
			render 'ID da ficha não informado.'
			return;
		}
		Ficha ficha = Ficha.get(params.sqFicha)
		render(template:"/ficha/conservacao/presencaUC/formPresencaUC",model:['ficha':ficha]);
	}

    def recalcularUcs() {
        Map res = [:]
        res.msg=''
        res.status=0
        res.type='success'
        List listHashes = []
        List idsOcorrencias= []
        // ler todas as ocorrencias utilizadas na avaliacao
        try {
            if( !session?.sicae?.user?.noPessoa ){
                throw new Exception('Sessão expirada.');
            }
            if( session.getAttribute('gravarUtilizadoAvaliacao') ) {
                throw new Exception('Já existe um processamento de registros em execução. Aguarde a finalização para solicitar outro.')
            }
            DadosApoio contextoOcorrencia = dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA', 'REGISTRO_OCORRENCIA')

            VwFicha ficha = VwFicha.get( params.sqFicha.toLong() )
            if( !ficha ){
                throw new Exception('Id da ficha inválido.');
            }
            FichaOcorrencia.executeQuery("""select new map(fo.id as sqFichaOcorrencia, fo.geometry as geometry) from FichaOcorrencia fo
                        where fo.contexto.id = :sqContexto and fo.geometry is not null
                        and fo.inUtilizadoAvaliacao = 'S'
                        and fo.ficha.id = :sqFicha""", [sqFicha: ficha.id.toLong(), sqContexto: contextoOcorrencia.id]).each {

                try {
                    float x = it.geometry.x
                    float y = it.geometry.y
                    x = Math.round(x * 100000000) / 100000000
                    y = Math.round(y * 100000000) / 100000000
                    String xyHash = (x.toString() + '/' + y.toString()).encodeAsMD5()
                    if (!listHashes.contains(xyHash)) {
                        listHashes.push(xyHash)
                        idsOcorrencias.push(['bd': 'salve', id: it.sqFichaOcorrencia])
                    }
                } catch (Exception e) {
                }
            }
            // ocorrencias do portalbio
            FichaOcorrenciaPortalbio.executeQuery("""select new map(fo.id as sqFichaOcorrenciaPortalbio, fo.geometry as geometry) from FichaOcorrenciaPortalbio fo
                        where fo.geometry is not null
                        and fo.inUtilizadoAvaliacao = 'S'
                        and fo.ficha.id = :sqFicha""", [sqFicha: ficha.id.toLong()]).each {
                try {
                    float x = it.geometry.x
                    float y = it.geometry.y
                    x = Math.round(x * 100000000) / 100000000
                    y = Math.round(y * 100000000) / 100000000
                    String xyHash = (x.toString() + '/' + y.toString()).encodeAsMD5()
                    if (!listHashes.contains(xyHash)) {
                        listHashes.push(xyHash)
                        idsOcorrencias.push(['bd': 'portalbio', id: it.sqFichaOcorrenciaPortalbio])
                    }
                } catch (Exception e) {
                }
            }
            if (idsOcorrencias) {
                res.msg = 'Total de <b>'+idsOcorrencias.size()+'</b> registro(s) encontrado(s).<br><br>' +
                    'Ao final do processamento será exibido um alerta na parte superior da tela e em seguida basta clicar na aba 7.4 para atualizar o gride.<br><br>Clique no botão fechar para continuar os trabalhos.'
                    // 'Aguarde a mensagem de aviso do término do processanto e clique na aba 7.4 novamente para atualizar o gride.'
                res.type ='modal'
                fichaService.gravarUtilizadoAvaliacao('S', idsOcorrencias, session?.sicae?.user?.noPessoa, '', ficha.id.toLong(), null, true)
            } else {
                res.msg = 'Nenhum registro encontrado'
            }
        } catch (Exception e) {
            res.status =1
            res.msg    = e.getMessage()
            res.type   ='error'
        }
        render res as JSON
    }

	def saveFrmConPresencaUC( params )
	{
		if( !params.sqFicha )
		{
			this._returnAjax(1, 'ID da ficha não informado.', "error")
			return
		}

		Ficha ficha = Ficha.get(params.sqFicha)
		ficha.properties = params
        try {
            ficha.save(flush:true)
			fichaService.updatePercentual( ficha )
            //fichaService.atualizarPercentualPreenchimento( ficha )
            this._returnAjax(0, getMsg(1), "success")
        } catch(Exception e) {
            println e.getMessage()
            this._returnAjax(1, getMsg(2), "error")
        }
	}

	def getGridPresencaUc() {
		if( !params.sqFicha)
		{
			render ''
			return
		}
        List listUcs = []

        // NOVO-REGISTRO
        List listUcsRegistros = fichaService.getUcsRegistro( params.sqFicha.toLong() )
		render( template:'/ficha/conservacao/presencaUC/divGridPresencaUC',model:['listUcsRegistros':listUcsRegistros])
	}

	// ############################################################################################################################################################################################# //
	// 	MODULO FICHA/CONSERVACAO/SITUACAO REGIONAL
	// ############################################################################################################################################################################################# //

	def tabSituacaoRegional()
	{
		if( !params.sqFicha )
		{
			render 'Ficha não informada.'
			return
		}
		Ficha ficha = Ficha.get( params.sqFicha.toInteger() )
		List listFichaAvaliacaoRegional = FichaAvaliacaoRegional.findAllByFicha(ficha)
		render( template:'/fichaCompleta/divGridAvaliacoesRegionais',model:[listFichaAvaliacaoRegional:listFichaAvaliacaoRegional,canModify:false])
	}

}
