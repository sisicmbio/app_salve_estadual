package br.gov.icmbio

import grails.converters.JSON

class CorpBiomaController extends BaseController {

    def corpBiomaService

    /**
     * Exibir o formulário de manutenção dos biomas
     * @return
     */
    def index() {
        render(view:'index',model:[biomas:Bioma.list() ] )
    }

    /**
     * Gravar/atualizar o bioma no banco de dados
      * @return
     */
    def save(){
        Map res = [status:0, type:'success', msg:'Bioma gravado com SUCESSO', data:[:] ]
        try {
            corpBiomaService.save(  params.sqBioma ? params.sqBioma.toLong() : null
                                   , params.noBioma ?:''
                                   , params.wkt     ?:'')

        } catch( Exception e ){
            res.status = 1
            res.type = 'error'
            res.msg = e.getMessage()
        }
        render res as JSON
    }

    /**
     * retornar os dados do bioma para edição
     * @return
     */
    def edit(){
        Map res = [status:0, type:'success', msg:'', data:[:] ]
        try {
            res.data = corpBiomaService.edit(  params.sqBioma ? params.sqBioma.toLong() : 0l)
        } catch( Exception e ){
            res.status = 1
            res.type = 'error'
            res.msg = e.getMessage()
        }
        render res as JSON
    }

    /**
     * excluir o bioma do banco de dados
     * @return
     */
    def delete(){
        Map res = [status:0, type:'success', msg:'Bioma excluído com SUCESSO', data:[:] ]
        try {
            res.data = corpBiomaService.delete(  params.sqBioma ? params.sqBioma.toLong() : 0l)
        } catch( Exception e ){
            res.status = 1
            res.type = 'error'
            res.msg = e.getMessage()
        }
        render res as JSON
    }

    /**
     * criar o gride dos biomas
     * @return
     */
    def grid(){
        try {
            render(template: 'gridBioma', model: [biomas: Bioma.list()])
        } catch( Exception e ) {
            render e.getMessage()
        }
    }
}
