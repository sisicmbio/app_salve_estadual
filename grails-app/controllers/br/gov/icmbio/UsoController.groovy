package br.gov.icmbio
import grails.converters.JSON
// import groovy.sql.Sql

class UsoController extends BaseController {

    def dataSource
    def dadosApoioService

    def index() {

        List data = Uso.findAll();
        //List data = Uso.findAllByPaiIsNull()
        Map pagination = ['totalRecords': data.size(), 'pageCurrent': 1,'pageSize':50]
        this._pagination(pagination)
        render (view: 'index', model:['data': data, 'pagination': pagination])
    }


    def getTreeView() {
        response.setHeader("Content-Type", "application/json")
        response.setHeader("Access-Control-Allow-Origin","*" )
        List data=[]
        if( ! params.key )
        {
            data = Uso.findAllByPaiIsNull([sort: "ordem"]);
        }
        else
        {
            data = Uso.findAllByPai(Uso.get( params.key ),[sort: "ordem"] );
        }
        Map result=['status':'ok','result':[]];
        data.each {
                result['result'].push([
                    'key'       : it.id
                    ,'pai'      : (it.pai ? it.pai.id : 0)
                    ,'title'      : it.codigo+' '+it.descricao
                    ,'iucnAmeaca' : it.criterioAmeacaIucn ? it.criterioAmeacaIucn.descricaoCompleta : ''
                    ,'iucnUso'    : it?.iucnUso ? it.iucnUso.descricaoCompleta : ''
                    // campos controle da fancytree
                    ,'select'   : false
                    ,'expanded' : false
                    ,'folder'   : (it.itens.size()>0)
                    ,'lazy'     : (it.itens.size()>0)
                    ] );
            }
            sleep(500)
        render result as JSON;
    }

    def getForm()
    {
        Uso uso;
        if( params.id && params.operacao == 'uso.edit' )
        {
            uso = Uso.get( params.id)
        }
        else
        {
            uso = new Uso();
        }
        List listCriteriosAmeaca = dadosApoioService.getTable('TB_CRITERIO_AMEACA_IUCN').sort{it.ordem }
        List listIucnUso = IucnUso.findAll()
        render( template:'frmUso',model:[ uso:uso
                                          ,listCriteriosAmeaca:listCriteriosAmeaca
                                          ,listIucnUso:listIucnUso]);
    }

    def save()
    {

        Uso uso;
        if( params.operacao == 'uso.edit')
        {
            uso = Uso.get(params.id);
        }
        else if( params.operacao == 'uso.add')
        {
           uso = new Uso()
           uso.pai = Uso.get( params.id);
        }
        uso.codigo              = params.codigo
        uso.descricao           = params.descricao
        uso.ordem               = params.ordem ?: Util.formatarCodigoOrdem(params.codigo)
        uso.criterioAmeacaIucn  = null;
        uso.iucnUso             = null
        if( params.sqCriterioAmeacaIucn)
        {
            uso.criterioAmeacaIucn = DadosApoio.get( params.sqCriterioAmeacaIucn);
        }
        if( params.sqIucnUso)
        {
            uso.iucnUso = IucnUso.get( params.sqIucnUso.toLong() )
        }
        uso.save( flush:true);
        return this._returnAjax(0,null,null,[id:uso.id]);
    }

    def delete()
    {
        if( params.id)
        {
            Uso.get(params.id).delete( flush:true);
            return this._returnAjax(0);
        }
        return this._returnAjax(1, 'Id não informado.', "error")
    }

}
