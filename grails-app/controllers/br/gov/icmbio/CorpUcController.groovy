package br.gov.icmbio

import grails.converters.JSON

class CorpUcController extends BaseController {

    def corpUcService
    def dadosApoioService


    /**
     * Exibir o formulário de manutenção das UC
     * @return
     */
    def index() {
        render(view:'index',model:[
                esferas:dadosApoioService.getTable('TB_ESFERA_ADMINISTRATIVA'),
                tipos:dadosApoioService.getTable('TB_TIPO_UC')
        ] )
    }

    /**
     * Gravar/atualizar a UC no banco de dados
      * @return
     */
    def save(){

        Map res = [status:0, type:'success', msg:'UC gravada com SUCESSO', data:[:] ]
        try {
            corpUcService.save(  params.sqPessoa ? params.sqPessoa.toLong() : null
                                   , params.sqEsfera ? params.sqEsfera.toLong() : null
                                   , params.sqTipo   ? params.sqTipo.toLong() : null
                                   , params.noPessoa ?:''
                                   , params.sgInstituicao ?:''
                                   , params.nuCnpj ?:''
                                   , params.cdCnuc ?:''
                                   , params.wkt    ?:'')

        } catch( Exception e ){
            res.status = 1
            res.type = 'error'
            res.msg = e.getMessage()
            println e.getMessage();
        }
        render res as JSON
    }

    /**
     * retornar os dados da UC para edição
     * @return
     */
    def edit(){

        Map res = [status:0, type:'success', msg:'', data:[:] ]

        try {
            res.data = corpUcService.edit(  params.sqPessoa ? params.sqPessoa.toLong() : 0l)
        } catch( Exception e ){
            res.status = 1
            res.type = 'error'
            res.msg = e.getMessage()
        }
        render res as JSON
    }

    /**
     * excluir a UC do banco de dados
     * @return
     */
    def delete(){
        Map res = [status:0, type:'success', msg:'UC excluída com SUCESSO', data:[:] ]

        try {
            res.data = corpUcService.delete(  params.sqPessoa ? params.sqPessoa.toLong() : 0l)
        } catch( Exception e ){
            res.status = 1
            res.type = 'error'
            res.msg = e.getMessage()
        }
        render res as JSON
    }

    /**
     * criar o gride das ucs
     * @return
     */
    def grid(){

        try {
            Integer pageSize = 20
            Integer totalRecords = params.totalRecords ?: 0
            Integer totalPages
            Integer page

                if( !params.totalRecords ) {
                    totalRecords = UnidadeConservacao.createCriteria().count{}
                }
                totalPages = Math.ceil( totalRecords / pageSize )
                page = params.page ? params.page.toInteger() : 1
                page = Math.min(totalPages,page)
                List unidades = corpUcService.list( page, pageSize, params )

                render(template: 'gridUc', model: [unidades :unidades /*UnidadeConservacao.list()*/
                                                   , page   :page
                                                   , firstRowNum : ( pageSize * ( page-1 ) )
                                                   , totalRecords:totalRecords
                                                   , totalPages:totalPages
                ])
        } catch( Exception e ) {
            render e.getMessage()
        }
        render ''
    }
}
