package br.gov.icmbio

class FichaBaseController extends BaseController {

    /**
    *   Definir método que irá iterceptar todas as ações submetidas
    */
    def beforeInterceptor = [action:this.&auth, except: 'semPermissao']

    Boolean auth()
    {

    	if( ! super.auth() )
    	{
    		return false
    	}

        if( ! params &&  request.getMethod().toUpperCase() != 'POST' )
        {
              return false
        }

    	//println 'Sem permissão';
    	//println '........................................................';
    	//render 'Sem permissão de Acesso';
        //redirect(controller:"ficha", action: 'semPermissao')
        //
        /*
        println '.'*200;
        println "FICHA";
        println "Controlador: "+this.controller;
        println "Acao.......: "+this.action;
        println "SqUnidade..: "+session?.sicae?.user?.sqUnidadeOrg;
        println "SqFicha....: "+params?.sqFicha;
        println "isAjax.....: "+this.isAjax;
        */
        if( this.controller && this.action )
        {
            if( this.controller =='ficha' )
            {
                if( !this.action || this.action == 'list')
                {
                    session['sqFicha']=null;
                }
            }
        }

		if( params.sqFicha && this.action )
    	{
            if( this.action ==~ /^(save|del).*/  )
            {
                // validador pode inserir pendência na ficha
                if( this.action == 'saveFrmPendencia' )
                {
                    return true
                }
                VwFicha f = VwFicha.get( params?.sqFicha?.toInteger() )
                if( ! f.canModify( session?.sicae?.user ) )
                {
                    render this._returnAjax(1, getMsg(60), 'error')
                    return false
                }
            }
        }
    	return true
    }
}


