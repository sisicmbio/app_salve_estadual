package br.gov.icmbio

import com.vividsolutions.jts.geom.Geometry
import com.vividsolutions.jts.io.WKTReader
import grails.converters.JSON;

class DadosApoioController extends BaseController{

    def messageSource
    def sqlService

	/**
	 * Método para mostrar lista de tabelas auxiliares e grid com registros cadastrados no banco
	 * @return Apoio
	 */
    def index()
    {
        /*Integer regPorPagina = 20;
        List data = DadosApoio.findAllByPaiIsNull([max:regPorPagina])
        Map pagination = ['totalRecords': DadosApoio.findAllByPaiIsNull().size(), 'pageCurrent': 1]
        this._pagination(pagination)
        render (view: 'index', model:['data': data, 'pagination': pagination])
        */
    	render (view: 'index')
    }

    /**
     * Método que renderiza formulário para adicionar registro
     * @return [description]
     */
    // def add()
    // {
    //     def data = new DadosApoio()
    //     render(template:'form', model:['data':data])
    // }

    /**
     * Método para buscar os registros relacionados a uma tabela de apoio
     * @return Apoio
     */
    // def listParents()
    // {
    //     List data;
    //     Integer regPorPagina = 20;
    //     params.pageCurrent = ( params.pageCurrent ? params.pageCurrent : '1' );
    //     Map pagination = ['totalRecords': 0, 'pageCurrent': params?.pageCurrent.toInteger()]

    //     if(params.pai){
    //         data = DadosApoio.findAllByPai(DadosApoio.get(params.pai),[max: regPorPagina, offset: (regPorPagina * (params?.pageCurrent.toInteger() - 1))])
    //         pagination.totalRecords = DadosApoio.findAllByPai(DadosApoio.get(params.pai)).size()
    //     } else {
    //         data = DadosApoio.findAllByPaiIsNull([max: regPorPagina, offset: (regPorPagina * (params?.pageCurrent.toInteger() - 1))])
    //         pagination.totalRecords = DadosApoio.findAllByPaiIsNull().size()
    //     }

    //     this._pagination(pagination)
    //     render(template: 'list', model:['data':data, 'pagination': pagination])
    // }

    /**
     * Método para buscar apenas um item do banco pelo id do registro
     * @return Apoio
     */
    // def edit()
    // {
    //     def data = DadosApoio.get( params.id )
    //     render(template: 'form', model:['data':data])
    // }


    /**
     * Método para salvar novo registro no banco.
     * Se houver id_apoio, cadastra um novo filho,
     * Senão, cadastra um novo pai
     * @return apoio
     */
    def save()    {

        def apoio;
        if( params.id && params.id.toInteger() > 0 )
        {
            apoio = DadosApoio.get( params.id )
        }
        else {

            params.codigo = params.codigo.toUpperCase();
            // verificar se a tabela já existe
            if( params.codigo && DadosApoio.findAllByCodigo( params.codigo ).size() > 0 )
            {
                return this._returnAjax(1, "Tabela "+params.codigo+" já existe!", "error")
            }
            apoio = new DadosApoio();
        }

        apoio.codigo    = params?.codigo
        apoio.descricao = params?.descricao
        apoio.ordem     = (params?.ordem?:null)

        if( apoio.descricao =~ /<span/){
            apoio.descricao = apoio.descricao.split('<span')[0]
        }
        if( params.pai && params.pai.toInteger() > 0) {
            DadosApoio aPai = DadosApoio.get(params.pai);
            apoio.pai = aPai
        }

        List errors=[];
        if( ! apoio.validate() )
        {
            println apoio.getErrors()
            /*apoio.errors.allErrors.each {
                errors.push(messageSource.getMessage( it, null ) );
            }*/
            return this._returnAjax(1, 'Erro ao gravar', "error");
        }

        if( errors )
        {
            return this._returnAjax(1, errors.join('<br/>'), "error");
        }
        try {
            apoio.save(flush:true);
            this._returnAjax(0,null,null,[id:apoio.id,codigo:apoio.codigo])
        }
        catch(Exception e) {
            println e.getMessage()
            this._returnAjax(1, "Não foi possível gravar o registro!", "error")
        }
    }

    /**
     * [delete description]
     * @return [description]
     */
    def delete()
    {
        DadosApoio apoio = DadosApoio.get(params.id)
        if( apoio?.itens?.size() > 0)
        {
            return this._returnAjax(1, "Registro possui subitens!", "error");
        }
        if( apoio ) {
            try {
                apoio.delete( flush:true )
                this._returnAjax(0)
            }
            catch(Exception e) {
                String msg = e.getMessage()
                if( msg =~ /(referenced from table|violates foreign key constraint)/ ) {
                    msg ='Registro está sendo utilizado em outra(s) tabela(s) do SALVE.'
                } else {
                    println msg
                    msg="Registro não pode ser excluído."
                }
                this._returnAjax(1, msg, "error")
            }
        }
    }

    /**
     * Método para retornar a lista de tabelas pais do sistema para preencher as combos
     * @return Apoio
     */
    /*def listParentsJSON()
    {
        List data = DadosApoio.findAllByPaiIsNull();
        render data as JSON;
    }
*/

    def getTreeView()
    {

        //https://github.com/mar10/fancytree/wiki/TutorialLoadData
        response.setHeader("Content-Type", "application/json")
        response.setHeader( "Access-Control-Allow-Origin", "*" )
        //response.setHeader( "Access-Control-Allow-Credentials", "true" )
        //response.setHeader( "Access-Control-Allow-Methods", "PUT" )
        //response.setHeader( "Access-Control-Max-Age", "3600" )
        List data=[]
        List tabelasTraduzir = ['TB_CRITERIO_AMEACA_IUCN'
                                ,'TB_UNIDADE_TEMPO'
                                ,'TB_UNIDADE_PESO'
                                ,'TB_TIPO_AVALIACAO'
                                ,'TB_TIPO_REDUCAO'
                                ,'TB_CONVENCOES'
                                ,'TB_ACAO_CONSERVACAO'
                                ,'TB_SITUACAO_PESQUISA'
                                ,'TB_TEMA_PESQUISA'
                                ,'TB_CATEGORIA_IUCN'
                                ,'TB_PROB_EXTINCAO_BRASIL'
                                ,'TB_PERC_DECLINIO_POPULACIONAL'
                                ,'TB_STATUS_ACAO_CONSERVACAO'
                                ,'TB_ORDENAMENTO_PESCA'
                                ,'TB_MUDANCA_CATEGORIA'
                                ,'TB_TENDENCIA'
        ]

        boolean habilitarTraducao = false
        if( ! params.key )
        {
            //data = DadosApoio.findAllByPaiIsNull([sort: "ordem"]);
            data = DadosApoio.createCriteria().list{
                isNull('pai')
                // não exibir as tabelas gerenciadas separadamente por ter uma relação com as tabelas do SIS/IUCN
                not {
                    'in'('codigoSistema',['TB_HABITAT','TB_CRITERIO_AMEACA_IUCN','TB_ACAO_CONSERVACAO','TB_TEMA_PESQUISA'])
                }
                order( "ordem")
            }
        }
        else
        {
            data = DadosApoio.findAllByPai(DadosApoio.get( params.key ),[sort: "ordem"] );
            habilitarTraducao=true
        }

        data = data.sort{  (it.ordem ? it.ordem : '')+it.descricao }

        /*
        the following attributes are available as 'node.PROPERTY':
            expanded, extraClasses, folder, hideCheckbox, key, lazy, selected, title, tooltip, unselectable.
        All other fields are considered custom and will be added to the nodes data object as 'node.data.PROPERTY' (e.g. 'node.data.myOwnAttr').
        */

        /*
            Pode ser retornado um array com resultados ou um map com result e status para ser tratado no evento postProcess()
        */
        Map result=['status':'ok','result':[]];
        boolean isBaciaHidrografica = false
        data.each {

                    if( !it.pai ) {
                        habilitarTraducao = tabelasTraduzir.contains(it.codigoSistema)
                        /*
                        if (habilitarTraducao) {
                            println 'Tabela:' + it.codigoSistema + ' : Traduzir: ' + habilitarTraducao
                        }*/

                        isBaciaHidrografica = (it.codigoSistema == 'TB_BACIA_HIDROGRAFICA')
                    }

                result['result'].push([
                    'key'       : it.id

                    // Alteracoes integração com SIS/IUCN
                    ,'habilitarTraducao':habilitarTraducao

                    // habilitar upload do shapefile da bacia
                    ,'isBaciaHidrografica':isBaciaHidrografica
                        
                    // adicionar à frente do texto original o span para inserir o a traducao
                    ,'title'    : it.descricao + ( habilitarTraducao ? '<span class="treeInput-text-traduzido" style="display:block;" id="traducaoApoio-'+it.id+'">' + ( it?.traducaoDadosApoio[0]?.txRevisado ?:'' ) +'</span>' :'')
                    ,'text'     : it.descricao
                    ,'traducao' : ( habilitarTraducao && it?.traducaoDadosApoio ? it?.traducaoDadosApoio[0]?.txRevisado : '')
                    ,'ordem'    : it.ordem
                    ,'codigo'   : it.codigo
                    ,'select'   : false
                    ,'expanded' : false
                    ,'folder'   : (it.itens.size()>0)
                    ,'lazy'     : (it.itens.size()>0)
                    ,'pai'      : (it.pai ? it.pai.id : 0)
                    ] );
            }
            sleep(500)
        render result as JSON;
    }


    def getGeoData(){
        Map res = [msg:'',status:0,type:'success',data:[:]]
        DadosApoioGeo dadosApoioGeo
        if( params.id ){
            dadosApoioGeo = DadosApoioGeo.findByDadosApoio( DadosApoio.get(params.id.toLong() ) )
        }
        res.data.id = params.id
        res.data.geojson = [ "type": "FeatureCollection", 'features'  : [] ]
        if( dadosApoioGeo ){
            res.data.txGeo = dadosApoioGeo.txGeo
            res.data.cdGeo = dadosApoioGeo.cdGeo
            String cmdSql = """SELECT ST_AsGeoJSON( ge_geo,13,8) AS geoJson 
                           FROM salve.dados_apoio_geo 
                           WHERE sq_dados_apoio = :sqDadosApoio"""
            sqlService.execSql( cmdSql, [sqDadosApoio:params.id.toLong()] ).each { row ->
                if( row.geoJson ) {
                    res.data.geojson.features.push( [ 'type': 'Feature'
                                                    ,'properties': [],
                                                    'geometry': JSON.parse( row.geoJson )

                    ])
                }
            }
        } else {
            res.data.txGeo = params.text ?:''
            res.data.cdGeo = params.codigo ?:''
        }
        println res.data
        render res as JSON
    }

    /**
     * Gravar/atualizar a tabela dados_apoio_geo
     * @return
     */
    def saveGeo(){
        Map res = [status:0, type:'success', msg:'Dados gravados com SUCESSO', data:[:] ]
        try {

            if (!params.txGeo) {
                throw new Exception('Nome deve ser informado')
            }
            if (!params.wkt) {
                throw new Exception('Shape deve ser informado')
            }
            DadosApoio dadosApoio = DadosApoio.get(params.sqDadosApoio.toLong())
            if (!dadosApoio) {
                throw new Exception('Id inexistente')
            }
            DadosApoioGeo dadosApoioGeo = DadosApoioGeo.findByDadosApoio(dadosApoio)
            if (!dadosApoioGeo) {
                dadosApoioGeo = new DadosApoioGeo(dadosApoio: dadosApoio)
            }
            if (params.wkt) {
                WKTReader wktReader = new WKTReader();
                Geometry g = wktReader.read(params.wkt)
                dadosApoioGeo.geGeo = g
                if (dadosApoioGeo.geGeo) {
                    dadosApoioGeo.geGeo.SRID = 4674
                }
            } else {
                dadosApoioGeo.geGeo = null
            }
            dadosApoioGeo.txGeo = params.txGeo ?: null
            dadosApoioGeo.cdGeo = params.cdGeo ?: null
            dadosApoioGeo.save()
        } catch( Exception e ){
            res.status = 1
            res.type = 'error'
            res.msg = e.getMessage()
        }
        render res as JSON
    }

}
