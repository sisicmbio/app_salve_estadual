/**
 * Este controlador é resposável por receber as ações comuns a todas as abas referentes ao cadastro da ficha de especies.
 * Regras:
 * 1) Listar apenas as fichas da unidade do usuário logado.
 * 2) após salvar a espécie/subespécie a ficha será criada, não será permitido alterar a espécie/subespécie selecionada da ficha
 *
 */
package br.gov.icmbio

import asset.pipeline.JsAssetFile
import com.vividsolutions.jts.geom.Coordinate
import com.vividsolutions.jts.geom.GeometryFactory
import grails.converters.JSON
import org.codehaus.groovy.grails.web.json.JSONElement
import org.codehaus.groovy.grails.web.json.JSONObject
import org.codehaus.groovy.grails.web.servlet.mvc.GrailsHttpSession

import java.util.regex.Pattern


class FichaController extends FichaBaseController {

    int maxFichasExportar = 15000
    int maxFichasExportarPdf = 1000
    int maxFichasExportarShp = 5000
    int maxFichasExportarOcorrencias = 200

    def requestService
    def dadosApoioService
    def publicacaoService
    def messageSource
    def fichaService
    def exportDataService
    def geoService
    def asyncService
    def sqlService
    def logService
    def userJobService

    /*
    // http://mrhaki.blogspot.com.br/2015/05/groovy-goodness-share-data-in.html
    def downloadData(requestUrl, requestParams) {
        final params = requestParams.collect { it }.join('&')
        final url = "${requestUrl}?${params}"

        println 'Download from ' + url;

        final response = new JsonSlurper().parseText( url.toURL().text );
        return response
    }
    */
    def semPermissao() {
        println new Date()
        Thread.sleep(2000)
        println new Date()
        render 'Sem permissão pera executar esta ação'
    }

    def index() {
        List listCiclosAvaliacao = [session.getAttribute('ciclo')]

        // tipo de publicação ( para cadastro on-line de ref bib.)
        List listTipoPublicacao = TipoPublicacao.list(sort: 'deTipoPublicacao')
        List listSituacaoFicha = fichaService.getListSituacao().findAll {
            // excluir CONSULTA,AVALIADA_EM_REVISAO
            //return ! (it.codigoSistema =~/^(CONSULTA|AVALIADA_EM_REVISAO)$/)

            // Solicitado exibir também "CONSULTA" em reunião de 04/05/2022 por Carlos A. R.
            return !(it.codigoSistema =~ /^(AVALIADA_EM_REVISAO)$/)
        }

        // mostrar/esconder coluna de pesquisa pela Unidade Responsável no gride
        boolean grideFiltroUnidade = true
        if (session.sicae.user.sqUnidadeOrg && !session.sicae.user.isADM() && !session.sicae.user.isCO()) {
            grideFiltroUnidade = false
        }
        // grupos para ações em lote
        /* // serão preenchido via ajax
        List listGruposSalve        = dadosApoioService.getTable('TB_GRUPO_SALVE')
        List listGrupos             = dadosApoioService.getTable('TB_GRUPO')
        List listSubgrupos          = dadosApoioService.getTable('TB_SUBGRUPO')
        */

        // para cadastrar uma ficha é preciso tem pelo menos uma unidade interna cadastrada
        Instituicao instituicao = Instituicao.findByStInterna(true)
        if (!instituicao) {
            render """<div class="alert alert-info">Para realizar o cadastro das fichas é necessário cadastrar pelo menos uma unidade organizacional responsável.<br>
                      Clique <a href="javascript:void(0)" onClick="app.loadModule('corpEstruturaOrg',{title:'Manutenção das instituições / Unidades'})">AQUI</a> para fazer o cadastro</div>"""
            return
        }

        // relação de unidades que já possuem ficha cadastrada para filtrar o gride
        //DetachedCriteria query = Ficha.where {}.projections { distinct 'unidade' }
        //List listUnidades = query.list().sort { it.noPessoa } // sgUnidade
        render(view: 'index', model: ['listCiclosAvaliacao' : listCiclosAvaliacao
                                      //, 'listUnidades'    : listUnidades
                                      , 'listSituacaoFicha' : listSituacaoFicha
                                      , 'listTipoPublicacao': listTipoPublicacao
                                      , 'grideFiltroUnidade': grideFiltroUnidade
        ])
    }

    /**
     * Método para excluir a ficha do processo de avaliação com uma justificativa e
     * alterar a situação da ficha para exlcuida
     */
    def alterarSituacaoFichaParaExcluida() {
        Map res = [status:0, msg:'', type:'',data:[:] ]
        Boolean fichaJaEstavaExcluida = false
        try {
            if( !params.sqFicha ){
                throw new Exception('Ficha não informada')
            }
            Ficha ficha = Ficha.get( params.sqFicha.toLong() )
            if( !ficha ) {
                throw new Exception('Ficha inválida')
            }
            if( ! params.dsJustificativaExclusao ) {
                throw new Exception('Necessário informar a justificativa')
            }
            params.dsJustificativaExclusao = Util.trimEditor(params.dsJustificativaExclusao)
            if(  ( ficha.categoriaFinal && ficha.categoriaFinal.codigoSistema != 'NE') ||
                 ( ficha.categoriaIucn  && ficha.categoriaIucn.codigoSistema != 'NE' ) ) {
                throw new Exception('Ficha está avaliada. Para excluir a ficha da processo de avaliação as abas 11.6 e 11.7 devem estar em branco')
            }
            DadosApoio situacaoExcluida = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA','EXCLUIDA')
            ficha.dsJustificativa = params.dsJustificativaExclusao
            fichaJaEstavaExcluida = ficha.situacaoFicha.codigoSistema=='EXCLUIDA'
            ficha.situacaoFicha = situacaoExcluida
            ficha.save(flush:true)
            res.msg='Alteração realizada com SUCESSO.'
            res.data.dsSituacao = situacaoExcluida.descricao // atualizar a tela
            res.data.cdSituacao = situacaoExcluida.codigoSistema // atualizar a tela
            res.type='success'
            if( ! fichaJaEstavaExcluida ) {
                fichaService.gravarHistoricoSituacaoExcluida(ficha.id.toLong()
                    , params.dsJustificativaExclusao
                    , session.sicae.user.sqPessoa.toLong())

                // se for exclusao da ficha do 2º ciclo tem que desmarcar das fichas do 1º e 2º ciclo para não exibir no modulo publico
                fichaService.marcarDesmarcarRegistroExibirModuloPublico(ficha.id.toString(), false, session.sicae.user.sqPessoa)
                if (ficha.fichaCicloAnterior) {
                    fichaService.marcarDesmarcarRegistroExibirModuloPublico(ficha.fichaCicloAnterior.id.toString(), false, session.sicae.user.sqPessoa)
                }

                // SALVE SEM CICLO
                // versionar a ficha
                fichaService.versionarFicha(ficha.id.toString(),session.sicae.user.sqPessoa,'EXCLUSAO_FICHA')
                // DESMARCAR versão publicada
                sqlService.execSql("update salve.ficha_versao set st_publico=false where sq_ficha = ${ficha.id.toString()} and st_publico=true")
            } else {
                // atualizar a justificativa no historico tambem
                FichaHistSituacaoExcluida.createCriteria().list{
                    eq('ficha',ficha)
                    order('id','desc')
                    maxResults(1)
                }.each {
                    it.dsJustificativa = params.dsJustificativaExclusao
                    it.save()
                }
            }

        } catch ( Exception e ) {
            res.msg = e.getMessage()
            res.status=1
            res.type='error'
        }
        render res as JSON
    }

    /**
     * Método para ler a justificativa da exclusao da ficha
     * do processo de avlaiação
     * @return
     */
    def getJustificativaExcluida() {
        Map res = [status:0, msg:'', type:'',data:[:] ]
        try {
            if( !params.sqFicha ){
                throw new Exception('Ficha não informada')
            }
            Ficha ficha = Ficha.get( params.sqFicha.toLong() )
            if( !ficha ) {
                throw new Exception('Ficha inválida')
            }

            // ler a última justificativa do histórico
            FichaHistSituacaoExcluida fhs = FichaHistSituacaoExcluida.createCriteria().get{
                eq('ficha',ficha)
                order('dtInclusao','desc')
                maxResults(1)
            }

            res.msg=''
            res.data.dsJustificativa = Util.stripTags( fhs ? fhs.dsJustificativa : ficha.dsJustificativa )
            res.type='success'
        } catch ( Exception e ) {
            res.msg = e.getMessage()
            res.status=1
            res.type='error'
        }
        render res as JSON
    }

    /**
     * método para alterar o taxon da ficha
     * @return
     */
    def alterarTaxonFicha() {
        if (!params.sqFicha) {
            return this._returnAjax(1, 'Nenhuma ficha Selecionada!', 'error')
        }

        if (!params.sqTaxonNovo) {
            return this._returnAjax(1, 'Taxon não selecionado!', 'error')
        }

        Ficha ficha
        ficha = Ficha.get(params.sqFicha.toInteger())
        if (!ficha) {
            return this._returnAjax(1, 'Ficha não encontrada!', 'error')
        }

        Taxon taxonAntigo = ficha.taxon
        Taxon taxon = Taxon.get(params.sqTaxonNovo.toInteger())
        if (!taxon) {
            return this._returnAjax(1, 'Taxon não encontrado!', 'error')
        }
        // verificar se o taxon já possui ficha
        Integer qtd = Ficha.countByTaxonAndCicloAvaliacaoAndIdNotEqual(taxon, ficha.cicloAvaliacao,ficha.id)
        if (qtd > 0) {
            return this._returnAjax(1, '<b>' + taxon.noCientifico + '</b> já possui ficha cadastrada neste ciclo!', 'error')
        }
        ficha.nmCientifico = taxon.noCientificoCompleto
        ficha.taxon = taxon
        ficha.save()
        if( taxonAntigo.id != taxon.id ) {
            // atualizar tabela taxon_historico_avaliacao
            TaxonHistoricoAvaliacao.createCriteria().list {
                eq('taxon', taxonAntigo)
                lt('nuAnoAvaliacao', ficha.cicloAvaliacao.nuAno)
            }.each {
                if (!TaxonHistoricoAvaliacao.findByTaxonAndNuAnoAvaliacaoAndtipoAvaliacaoAndcategoriaIucn(taxon, it.nuAnoAvaliacao, it.tipoAvaliacao, it.categoriaIucn)) {
                    TaxonHistoricoAvaliacao novo = new TaxonHistoricoAvaliacao()
                    novo.taxon = taxon
                    novo.nuAnoAvaliacao = it.nuAnoAvaliacao
                    novo.tipoAvaliacao = it.tipoAvaliacao
                    novo.categoriaIucn = it.categoriaIucn
                    novo.deCategoriaOutra = it.deCategoriaOutra
                    novo.noRegiaoOutra = it.noRegiaoOutra
                    novo.abrangencia = it.abrangencia
                    novo.deCriterioAvaliacaoIucn = it.deCriterioAvaliacaoIucn
                    novo.txJustificativaAvaliacao = it.txJustificativaAvaliacao
                    novo.stPossivelmenteExtinta = it.stPossivelmenteExtinta
                    novo.save()
                    // gravar as referências bibliográficas
                    Integer idRegistro = it.id.toInteger()
                    FichaRefBib.createCriteria().list {
                        eq('ficha', ficha)
                        eq('noTabela', 'taxon_historico_avaliacao')
                        eq('noColuna', 'sq_taxon_historico_avaliacao')
                        eq('sqRegistro', idRegistro)
                    }.each {
                        if (!FichaRefBib.findByFichaAndPublicacaoAndNoTabelaAndNoColunaAndSqRegistro(it.ficha, it.publicacao, it.noTabela, it.noColuna, novo.id.toInteger())) {
                            FichaRefBib frb = new FichaRefBib()
                            frb.ficha = it.ficha
                            frb.publicacao = it.publicacao
                            frb.noTabela = it.noTabela
                            frb.noColuna = it.noColuna
                            frb.deRotulo = it.deRotulo
                            frb.sqRegistro = novo.id
                            frb.nuAno = it.nuAno
                            frb.noAutor = it.noAutor
                            frb.save()
                        }
                    }
                }
            }
        }
        return this._returnAjax(0, 'Taxon alterado com SUCESSO!', 'success', [novoNoCientifico: Util.ncItalico(taxon.noCientificoCompleto)])
    }

    /**
     * método para alterar o taxon da ficha
     * @return
     */
    def alterarArvoreTaxonomica() {
        Taxon taxon
        Taxon taxonPaiNovo
        Taxon taxonPaiAntigo
        try {
            if (!params.sqFicha) {
                throw new Exception('Ficha não informada!')
            }
            if (!params.sqTaxon) {
                throw new Exception('Taxon não informado!')
            }
            if (!params.sqTaxonPaiAntigo) {
                throw new Exception('Taxon pai antigo não informado.')
            }

            if (!params.sqTaxonPaiNovo) {
                throw new Exception('Taxon pai novo não informado!')
            }

            if (!params.txHistorico) {
                throw new Exception('Motivo da mundança não informado!')
            }

            Ficha ficha = Ficha.get(params.sqFicha.toLong())
            if (!ficha) {
                throw new Exception('Ficha inválida!')
            }
            if (!ficha.taxon.jsonTrilha) {
                sqlService.execSql("update taxonomia.taxon set json_trilha=taxonomia.criar_trilha(sq_taxon) where sq_taxon = ${ficha.taxon.id.toString()}")
                List row = sqlService.execSql("select json_trilha from taxonomia.taxon where sq_taxon = ${ficha.taxon.id.toString()}")
                ficha.taxon.jsonTrilha = row[0].json_trilha
            }
            if (!ficha.taxon.jsonTrilha) {
                throw new Exception('A trilha do taxon ' + ficha.taxon.noCientifico + ' não está preenchida informe ao administrador do SINTAX!')
            }

            /**
             * Regras:
             * 1) Quando for alterado o genero de uma Especie ou uma Especie de uma SubEspecie:
             * a) no caso de uma especie, todas as fichas que tiverem o mesmo genero devem ser alterado o campo nm_cientifico
             * b) no caso de uma subespecie, todas as fichas que tiverem a mesma espcecia devem ser alterado o campo nm_cientifico
             *
             * 2) Ao alterar qualquer nivel, verificar se a nova arvore já existe:
             * a) se existir, alterar somente o taxon da ficha e o nm_cientifico
             * b( se não existir, alterar o taxon.pai do taxon e alterar as fichas conforme regra numero 1
             */

            List idsArvoreAntiga = []
            // o taxon alterado é nivel abaixo do selecionado. Se troquei o reino o alterado é o filo
            taxon = Taxon.get(params.sqTaxon.toLong())
            if (!taxon) {
                throw new Exception('Taxon inexistente.')
            }
            Integer nuGrauAlterado = taxon.nivelTaxonomico.nuGrauTaxonomico.toInteger()
            d 'Taxon alterado: ' + taxon.noTaxon
            d 'Grau Alterado:' + nuGrauAlterado + ' ' + taxon.nivelTaxonomico.deNivelTaxonomico

            taxonPaiAntigo = Taxon.get(params.sqTaxonPaiAntigo.toLong())
            if (!taxonPaiAntigo) {
                throw new Exception('Taxon pai antigo inexistente.')
            }
            d 'Pai antigo:' + taxonPaiAntigo.noTaxon

            taxonPaiNovo = Taxon.get(params.sqTaxonPaiNovo.toLong())
            if (!taxonPaiNovo) {
                throw new Exception('Taxon pai novo inexistente.')
            }
            d 'Pai novo:' + taxonPaiNovo.noTaxon
            // verificar se já existe a nova árvore informada
            Taxon taxonExiste = Taxon.createCriteria().get {
                eq('id', taxon.id)
                eq('pai.id', taxonPaiNovo.id)
            }
            d 'Verificando se ja existe ' + taxonPaiNovo.noTaxon + '/' + taxon.noTaxon
            if (taxonExiste) {
                throw new Exception('<b>' + taxonPaiNovo.noTaxon + ' > ' + taxon.noTaxon + '</b> já existe no SINTAX.')
                // atualizar taxon da ficha apenas


            } else {
                // atualizar TAXON No SINTAX
                //taxon.pai = taxonPaiNovo
                //taxon.save( flush:true );
                d 'Taxon ' + taxon.id + ' teve o pai alterado de ' + taxonPaiAntigo.id + ' para ' + taxonPaiNovo.id
                String cmdSql = """update taxonomia.taxon set json_trilha=taxonomia.criar_trilha(sq_taxon)
                    where json_trilha->'${taxon.nivelTaxonomico.coNivelTaxonomico.toLowerCase()}'->>'sq_taxon' = '${taxonPaiAntigo.id.toString()}"""
                d ' '
                d 'Sql atualizar trilha'
                d cmdSql
                //sqlService.execSql(cmdSql)

                // se o nivel for Genero ou Especie, atulizar nomes cientificos das fichas
                if( taxon.nivelTaxonomico.coNivelTaxonomico == 'GENERO' || taxon.nivelTaxonomico.coNivelTaxonomico == 'ESPECIE' ) {

                }
            }

        } catch (Exception e) {
            return this._returnAjax(1, e.getMessage(), 'error')
        }


        return this._returnAjax(0, 'Árvore taxonômica atualizada com SUCESSO!'
                , 'success'
                , [sqNovoTaxon  : taxonPaiNovo.id.toString()
                   , noNovoTaxon: taxonPaiNovo.noCientifico])
    }

    /**
     * exibir as 3 ultimas fichas alteradas para o usuário
     * @return
     */
    def getLastChanged() {
        if( ! params.sqCicloAvaliacao )
        {
            render ''
            return
        }
        if( !session?.sicae?.user )
        {
            return ''
        }
        PessoaFisica pf = PessoaFisica.get(session?.sicae?.user?.sqPessoa)
        DadosApoio papel = null
        String nomePapel = ''
        if ( session.sicae.user.isCT() ) {
            nomePapel = 'COORDENADOR_TAXON'
        }
        else if ( session.sicae.user.isCE() ) {
            nomePapel = 'COLABORADOR_EXTERNO'
        }
        if( nomePapel ) {
            papel = dadosApoioService.getByCodigo('TB_PAPEL_FICHA', nomePapel )
            if( !papel )
            {
                render 'Papel '+nomePapel+' não cadastrado na tabela de apoio TB_PAPEL_FICHA'
                return
            }
        }

        List result = []
        if( pf ) {
            Ficha.createCriteria().list {
                eq('cicloAvaliacao', CicloAvaliacao.get(params.sqCicloAvaliacao.toInteger()))
                eq('alteradoPor', pf)
                isNotNull('dtAlteracao')
                isNotNull('alteradoPor')
                // quando não for ADM mostrar 5 ultimas alterações em qualquer unidade

                if ( papel ){
                    List minhasFichas = FichaPessoa.findAllByPessoaAndPapel(pf, papel )
                    'in'("id", minhasFichas?.vwFicha?.id ?: [(Long) 0])
                } else {
                    if( ! session.sicae.user.isADM() && session?.sicae?.user?.sqUnidadeOrg ) {
                        eq('unidade',Unidade.get(session.sicae.user.sqUnidadeOrg.toLong()) )
                    }
                }
                order('dtAlteracao', 'desc')
                maxResults(5)
            }.each {
                result.push('<a href="javascript:void(0);" data-action="ficha.edit" data-no-cientifico="'+
                         it.nmCientifico + '" data-sq-ficha="' + it.id + '" title="Alterada em '+
                         it.dtAlteracao.format('dd/MM/yyyy HH:mm:ss') + '">' + it.taxon.noCientificoItalico + '</a>')
            }
        }

        if( result.size() > 0 )
        {
            render result.size()+' últimas alterações: '+result.join('&nbsp;&nbsp;/&nbsp;&nbsp;')
        }
        else
        {
            render ''
        }
    }

    def getFormFiltroFicha() {
        Map excluirFiltro = [:]
        CicloAvaliacao cicloAvaliacao
        if( params.sqCicloAvaliacao )
        {
            cicloAvaliacao = CicloAvaliacao.get( params.sqCicloAvaliacao.toInteger() )
        }

        // alguns campos do formulário de filtros deverão ser exibidos sob demanda pelo parametro mostrarFiltros
        // ex: mostrarFiltros='comSemPapel,grupo
        Map listMostrarFiltros = [:]
        if( params.mostrarFiltros )
        {
            params.mostrarFiltros.split(',').each {
                listMostrarFiltros[it.trim()]=true
            }
        }

        // array de campos para excluir dos filtros. Ex: hideFilters='oficina,grupo'
        List hf = []
        if( params.hideFilters )
        {
            hf = params.hideFilters.split(',')
        }
        params.hideFilters = (List) hf

        // listas para alimentarem os combos do form filtro
        List listNivelTaxonomico    = NivelTaxonomico.list()
        List listSituacaoFicha      = []
        if( params.hideFilters.indexOf('situacao') == -1 ) {
            listSituacaoFicha = fichaService.getListSituacao()
        }
        // filtro que não são alimentados por tabela
        if( params.hideFilters.indexOf('pendencia') >-1 ){
            excluirFiltro.pendencia=true
        }
        if( params.hideFilters.indexOf('nomeCientifico') >-1 ){
            excluirFiltro.nomeCientifico=true
        }
        if( params.hideFilters.indexOf('revisada') >-1 ){
            excluirFiltro.revisada=true
        }

        if( params.hideFilters =~ /categoria,|categoria$/  ){
            excluirFiltro.categoria=true
        }
        if( params.hideFilters =~ /categoriaAvaliacao/  ){
            excluirFiltro.categoriaAvaliacao=true
        }

        if( params.hideFilters.indexOf('envioIucn') >-1 ){
            excluirFiltro.envioIucn=true
        }

        if( params.hideFilters.indexOf('andamentoRevisao') >-1 ){
            excluirFiltro.andamentoRevisao=true
        }

        List listOficinas = []
        if( cicloAvaliacao && params.hideFilters.indexOf('oficina') == -1 )
        {
            // nao exibir as reunioes preparatorias quando for consulta/revisao
            /*
            if ( params.contexto == 'consulta' ) {
                DadosApoio revisaoPreparatoria = dadosApoioService.getByDescricao('TB_TIPO_OFICINA','REUNIAO_PREPARATORIA')
                listOficinas = Oficina.findAllByCicloAvaliacaoAndTipoOficinaNotEqual( cicloAvaliacao, revisaoPreparatoria )
            } else {
                listOficinas = Oficina.findAllByCicloAvaliacao(cicloAvaliacao)
            }*/
            DadosApoio reuniaoPreparatoria = dadosApoioService.getByCodigo('TB_TIPO_OFICINA', 'REUNIAO_PREPARATORIA')
            if( session?.sicae?.user?.isADM() ) {
                listOficinas = Oficina.findAllByCicloAvaliacaoAndTipoOficinaNotEqual(cicloAvaliacao, reuniaoPreparatoria)
            } else {
                if( session?.sicae?.user?.sqUnidadeOrg ) {
                    Unidade unidade = Unidade.findById(session?.sicae?.user?.sqUnidadeOrg.toLong())
                    listOficinas = Oficina.findAllByCicloAvaliacaoAndTipoOficinaNotEqualAndUnidade(cicloAvaliacao, reuniaoPreparatoria, unidade)
                }
            }
        }

        // listas de consultas Amplas e Diretas realizadas no ciclo
        List listConsultas = []
        if ( listMostrarFiltros.consultas ) {
            listConsultas = CicloConsulta.findAllByCicloAvaliacao( cicloAvaliacao,[ sort: "dtInicio", order:"desc"] )
        }

        // filtrar os grupos por unidade
        List listGruposSalve = []
        List listGrupos      = []
        List gruposUnidade = []
        List gruposOficina = []
        Long sqUnidadeUsuario = null

        if( !session?.sicae?.user?.isADM() && session?.sicae?.user?.sqUnidadeOrg ) {
            // gruposUnidade = sqlService.execSql("""select distinct sq_grupo from salve.ficha where sq_unidade_org = :sqUnidadeOrg and sq_grupo is not null""",[sqUnidadeOrg : session.sicae.user.sqUnidadeOrg ])
            sqUnidadeUsuario = session?.sicae?.user?.sqUnidadeOrg?.toLong()
        }

        // listar somente os grupos das fichas que estão na oficina selecionada
        if( params.sqOficina ){
            String cmdSql = """select distinct grupo.sq_dados_apoio as sq_grupo from salve.oficina_ficha
                    inner join salve.oficina on oficina.sq_oficina = oficina_ficha.sq_oficina
                    inner join salve.ficha on ficha.sq_ficha = oficina_ficha.sq_ficha
                    inner join salve.dados_apoio as grupo on grupo.sq_dados_apoio = ficha.sq_grupo
                    where oficina.sq_oficina = :sqOficina
                    ${ sqUnidadeUsuario  ? ' and ficha.sq_unidade_org = '+sqUnidadeUsuario.toString():''}
                    """
            gruposOficina = sqlService.execSqlCache(cmdSql,[sqOficina:params.sqOficina.toLong()],5)?.sq_grupo?.collect{it.toString()}

        } else if( sqUnidadeUsuario ) {
            // ler somente os grupos das fichas da unidade do usuário
            gruposUnidade = sqlService.execSql("""select distinct sq_grupo from salve.ficha where sq_unidade_org = :sqUnidadeOrg and sq_grupo is not null""",[sqUnidadeOrg : sqUnidadeUsuario ])?.sq_grupo?.collect{it.toString()}
        }

        // criar a lista de grupos
        listGrupos = dadosApoioService.getTable('TB_GRUPO').findAll {
            if (  ( !gruposUnidade && !gruposOficina) || gruposUnidade.indexOf( it.id.toString() ) > -1
                  || gruposOficina.indexOf( it.id.toString() ) > -1  )  {
                return true
            } else  {
              return false
            }
        }
        listGruposSalve = dadosApoioService.getTable('TB_GRUPO_SALVE')
        List listSubgrupos          = dadosApoioService.getTable('TB_SUBGRUPO')
        List listCategorias         = dadosApoioService.getTable('TB_CATEGORIA_IUCN').findAll{ !(it.codigoSistema =~ /OUTRA|POSSIVELMENTE_EXTINTA/) }.sort{ it.ordem }
        List listEstados            = Estado.executeQuery( "select new map( e.id as id, e.noEstado as descricao) from Estado as e order by e.noEstado" );


        // listas de tradutores das fichas
        List listTradutores = []
        if ( listMostrarFiltros.tradutores ) {

            listTradutores = sqlService.execSql("""select pf.sq_pessoa, pf.no_pessoa from salve.ficha_pessoa a
                inner join salve.dados_apoio as papel on papel.sq_dados_apoio = a.sq_papel
                inner join salve.ficha on ficha.sq_ficha = a.sq_ficha
                inner join salve.vw_pessoa_fisica as pf on pf.sq_pessoa = a.sq_pessoa
                where ficha.sq_ciclo_avaliacao = 2
                and papel.cd_sistema = 'TRADUTOR'
                union
                -- ler nomes dos revisores
                select pf.sq_pessoa, pf.no_pessoa from salve.traducao_revisao as tr
                inner join salve.vw_pessoa_fisica as pf on pf.sq_pessoa = tr.sq_pessoa
                inner join salve.traducao_tabela as tt on tt.sq_traducao_tabela = tr.sq_traducao_tabela
                inner join salve.ficha on ficha.sq_ficha = tr.sq_registro
                where tr.dt_revisao is not null
                and tt.no_tabela = 'ficha'
                order by 2
                """)
        }


        // permitir ou não aplicar o filtro sem selecionar alguma coisa
        boolean isRequired =   params?.required?.toBoolean()  ? true : false
        boolean isCollapsed =  params?.collapsed?.toBoolean() == false  ? false : true

        //model="[callback: 'ficha.updateGridFichas', listSituacaoFicha: listSituacaoFicha]"></g:render>
        render( template:"/templates/filtrosFicha", model:[callback:params?.callback
            ,label: params.label?:'Filtrar fichas'
            ,listNivelTaxonomico:listNivelTaxonomico
            ,listSituacaoFicha:listSituacaoFicha
            ,listOficinas:listOficinas
            ,listGruposSalve:listGruposSalve
            ,listGrupos:listGrupos
            ,listSubgrupos:listSubgrupos
            ,outrosCampos: params?.outrosCampos
            ,listCategorias:listCategorias
            ,isRequired : isRequired
            ,isCollapsed : isCollapsed
            ,listMostrarFiltros:listMostrarFiltros
            ,listConsultas:listConsultas
            ,excluirFiltro:excluirFiltro
            ,listEstados:listEstados
            ,listTradutores:listTradutores
            ,gridId : params.gridId ?: ''
        ])
    } // categoria

    /**
    * responder a chamada ajax de seleção das subespecies da ficha selecionada
    */
    def getSubespecies() {
        List lista = []
        if( !params.sqFicha )
        {
            render lista as JSON
        }
        Ficha ficha = Ficha.get( params.sqFicha )
        if( ficha.taxon.nivelTaxonomico.coNivelTaxonomico == 'SUBESPECIE')
        {
            Ficha fichaEspecie
            // ler a espécie
            Ficha.createCriteria().list{

                createAlias('taxon','t')
                eq('cicloAvaliacao', ficha.cicloAvaliacao)
                eq('taxon',ficha.taxon.pai)
                eq('t.pai', ficha.taxon.pai.pai)
            } each {

                fichaEspecie = it

                lista.push( [sqFicha:it.id, noCientifico:it.noCientificoItalico, coNivel:'ESPECIE'] )
            }
            ficha = fichaEspecie
            //Ficha.findByCicloAvaliacaoAndTaxon( ficha.cicloAvaliacao, ficha.taxon.pai ).each {}
        }
        if ( ficha )
        {
            NivelTaxonomico subespecie
            subespecie = NivelTaxonomico.findByCoNivelTaxonomico('SUBESPECIE')
            Ficha.createCriteria().list{
                 createAlias('taxon','t')
                 createAlias('taxon.pai','p')
                 eq('cicloAvaliacao',ficha.cicloAvaliacao)
                 eq('t.nivelTaxonomico', subespecie)
                 eq('t.pai',ficha.taxon)
                 eq('p.pai',ficha.taxon.pai)
             }.each{
                if( it.taxon.pai.noCientifico.indexOf(ficha.taxon.pai.noCientifico) == 0 && it.id != params.sqFicha.toInteger() )
                {
                    lista.push( [sqFicha:it.id, noCientifico: Util.ncItalico(it.taxon.noCientificoItalico,true),coNivel:'SUBESPECIE'] )
                }
            }
        }
        render lista as JSON
    }

    def copiarFicha() {
        Map res = [status:0, msg:'', type:'']
        Map data = [:]
        try {
            if( !params.sqCicloDestino )
            {
                throw new RuntimeException('Ciclo destino não selecionado!')
            }
            if( !params.ids )
            {
                throw new RuntimeException('Nenhuma ficha selecionada!')
            }
            data.copiarPendencias   = (params?.copiarPendencias == 'S')
            data.copiarExcluidas    = (params?.copiarExcluidas == 'S')
            data.ids                = params.ids.split(',')
            data.sqCicloDestino     = params.sqCicloDestino.toLong()
            data.dsCicloDestino     = params.dsCicloDestino.trim()
            fichaService.asyncCopiarFichas(data)
        } catch(Exception e){
            res.status=1
            res.msg=e.getMessage()
            res.type='error'
        }
        render res as JSON
    }

    def copiarFichaOld() {
        Map res = [status:0, msg:'Fichas copiadas com SUCESSO!', type:'success']

        if( ! session?.sicae?.user?.sqPessoa )
        {
            res.status=1
            res.msg='Sessão Expirada!!'
            res.type='error'
        }
        else if( !params.sqCicloDestino )
        {
            res.status=1
            res.msg='Ciclo não selecionado!'
            res.type='error'
        }
        else
        {
            Lista listaIdsCopiar = new Lista()
            boolean copiarPendencias = (params?.copiarPendencias == 'S')
            listaIdsCopiar.list = params.ids.split(',')

            if( !fichaService.copiarFichas( params.sqCicloDestino.toInteger(), listaIdsCopiar,session.sicae.user.sqPessoa, copiarPendencias ) )
            {
                res.status=1
                res.msg = 'Erro ao copiar!'
                res.type='error'
            }

        }
        render res as JSON
    }


    /**
     * Método para criação do gride de fichas e exportação para planilha, pdf, doc e dos registros de ocorrências
     * @return
     */
    def getGridFicha() {
        String errorMessage = ''
        String cmdSql
        List passos = []
        List qryWhere = []
        List qryWhereFinal = []
        List qryInnerJoins = []
        Map qryParams = [:]
        Map data = [rows: [], pagination: [:]]
        Map result = [:]
        String outputFormat = (params.format ? params.format.toUpperCase() : 'TABLE') // TABLE/CSV/PDF/DOC
        String qryOrderBy = 'no_especie, nu_grau_taxonomico, no_subespecie'
        String msgAvisoSessao = '<br><br>Dependendo da quantidade de fichas este procedimento poderá demorar. <span class="red">(Não encerre a sua sessão)</span>'
        String dadosUsuario = session.sicae.user.noPessoa +' - CPF: '+ Util.formatCpf(session.sicae.user.nuCpf) + ' - ID: ' +session.sicae.user.sqPessoa

        try {

            // throw new Exception('Teste parametros')

            if (!session?.sicae?.user) {
                throw new Exception('Sessão expirada. Faça o login novamente!')
            }

            if (!params.sqCicloAvaliacao) {
                throw new Exception('Id do ciclo não informado.')
            }

            // Regra para não permitir exportação de registros em carências
            if( ! session.sicae.user.canExportRegsCarencia() ) {
                params.excluirRegistrosEmCarencia = 'S'
            }

            // não permitir duas exportações simultâneas
            //session.exportingData = false // para testar apenas
            if (outputFormat != 'TABLE') {
                if ( session.exportingData == true ) {
                    throw new Exception('Já existe uma exportação em andamento<br><br>Aguarde a finalização.')
                }
            }

            CicloAvaliacao cicloAvaliacao = CicloAvaliacao.get(params.sqCicloAvaliacao.toInteger())
            if (!cicloAvaliacao) {
                throw new Exception('Ciclo avaliacao inválido.')
            }

            // filtros
            // filtro por palavra chave da aba
            if( params.nuAbaFiltro && params.dsPalavraChaveFiltro){
                // fazer o texto a ser pesquisado ficar no padrão da fn_limpar_texto() do banco de dados
                String palavraChave = Util.removeAccents(params.dsPalavraChaveFiltro.replaceAll(/%/,'')).replaceAll(/[^a-zA-Z-]/,' ').replaceAll(/\s{2,}/,' ').trim()
                if( palavraChave ) {
                    if( palavraChave.length() < 3 ) {
                        throw new Exception('A palavra chave para pesquisar nas abas deve ter no mínimo 3 letras.')
                    }
                    Map camposAbas = [
                        '1': ['ds_notas_taxonomicas', 'ds_diagnostico_morfologico'],
                        '2': ['ds_distribuicao_geo_global', 'ds_distribuicao_geo_nacional'],
                        '3': ['ds_historia_natural', 'ds_habito_alimentar','ds_uso_habitat','ds_interacao','ds_reproducao'],
                        '4': ['ds_populacao','ds_metod_calc_tempo_geracional','ds_caracteristica_genetica'],
                        '5': ['ds_ameaca'],
                        '6': ['ds_uso'],
                        '7': ['ds_historico_avaliacao','ds_acao_conservacao','ds_presenca_uc'],
                        '8': ['ds_pesquisa_exist_necessaria'],
                        '11': ['ds_justificativa','ds_justificativa_final','ds_conectividade_pop_exterior','ds_justificativa_aoo_eoo'],

                    ]
                    List whereOr = []
                    camposAbas[params.nuAbaFiltro.toString()].each {
                        // procurar por palavras completas
                        whereOr.push("salve.fn_contem_palavra('"+palavraChave+"',ficha." + it + ") = TRUE")
                        /*whereOr.push('salve.fn_limpar_texto(ficha.' + it + ") ~* '^"+
                            palavraChave +" | "+palavraChave+" | "+palavraChave+"\$|^"+palavraChave+"\$'")

                         */
                    }
                    qryWhere.push('(' + whereOr.join(' OR ') +')')
                }
            }

            // CICLO DE AVALIACAO
            qryWhere.push('ficha.sq_ciclo_avaliacao = :sqCicloAvaliacao')
            qryParams.sqCicloAvaliacao = params.sqCicloAvaliacao.toLong()

            // FILTRAR PELO NOME CIENTIFICO
            if (params.nmCientificoFiltro) {
                params.nmCientificoFiltro = params.nmCientificoFiltro.toString().replaceAll(/%/, '').replaceAll(/;/, ',')
                // verificar se foi passado lista de nomes ou apenas 1 nome
                List nomes = params.nmCientificoFiltro.split(',').collect { it.toString().trim() }
                if (nomes.size() == 1) {
                    // quando for informado somente 1 nome cientifico, fazer a busca pelos nomes antigos (sinonimias) também
                    qryWhere.push("""( ficha.nm_cientifico ilike :nmCientifico OR exists (
                                        select null from salve.ficha_sinonimia
                                        where no_sinonimia ILIKE :noSinonimia
                                        and ficha_sinonimia.sq_ficha = ficha.sq_ficha
                                        limit 1 ) )""")

                    params.nmCientificoFiltro = Util.clearCientificName(params.nmCientificoFiltro)
                    qryParams.noSinonimia = nomes[0]
                    qryParams.nmCientifico = '%' + nomes[0] + '%'
                } else {
                    List whereOr = []
                    List nomesEspecies = []
                    List nomesSubespecies = []
                    nomes.eachWithIndex { nome, index ->
                        // colocar o nome cientifico iniciado com letra maiuscula e o restante em minusculas
                        nome = nome.substring(0, 1).toUpperCase() + nome.substring(1)
                        // criar dinamicamente os parametros para cada nome informado
                        //String paramName = 'noTaxon' + index.toString()
                        //qryParams[paramName] = nome
                        // gerar cláusula OR do comando sql que será gerado
                        if (nome.split(' ').size() == 2) {
                            nomesEspecies.push( nome )
                        } else {
                           nomesSubespecies.push( nome )
                        }
                    }
                    if( nomesEspecies ){
                        String nomesFormatados = "'"+nomesEspecies.join("','")+"'"
                        whereOr.push("taxon.json_trilha->'especie'->>'no_taxon'::text = any(array[${nomesFormatados}])")
                  } else if (nomesSubespecies) {
                        String nomesFormatados = "'"+nomesSubespecies.join("','")+"'"
                        whereOr.push("taxon.json_trilha->'subespecie'->>'no_taxon'::text = any(array[${nomesFormatados}])")
                    }
                    if (whereOr) {
                        qryWhere.push('(' + whereOr.join(' or ') + ')')
                    }
                    params.nmCientificoFiltro = '%'
                }
            }

            // FILTRAR ESPECIE MIGRAGRATORIA
            if (params.stMigratoriaFiltro) {
               qryWhere.push('ficha.st_migratoria = :stMigratoria')
               qryParams.stMigratoria = params.stMigratoriaFiltro.toUpperCase()
            }

            // FILTRAR PELA SIGLA DA UNIDADE ORGANIZACIONAL
            if (params.sgUnidadeFiltro) {
                qryWhereFinal.push('uo.sg_unidade_org ilike :sgUnidadeOrg')
                qryParams.sgUnidadeOrg = '%' + params.sgUnidadeFiltro + '%'
            }

            // FILTRAR PELO LOG DA ALTERAÇÃO
            if (params.logAlteracaoFiltro) {
                qryWhereFinal.push("concat( to_char( ficha.dt_alteracao ,'dd/mm/yyyy HH24:mi:ss'),'-',pessoa.no_pessoa) ilike :logAlteracao")
                qryParams.logAlteracao = '%' + params.logAlteracaoFiltro + '%'
            }

            // FILTRAR COM/SEM PENDENCIA
            if (params.inPendenciaFiltro) {
                qryWhereFinal.push("pendencias.nu_pendencia " + (params.inPendenciaFiltro.toInteger() == 1i ? '= 0' : '> 0'))
            }

            // FILTRAR PELA SITUACAO DA FICHA
            if (params.sqSituacaoFiltro) {
                if (params.sqSituacaoFiltro.toString().indexOf(',') > -1) {
                    qryWhere.push("ficha.sq_situacao_ficha in ( ${params.sqSituacaoFiltro} )")
                } else {
                    qryWhere.push("ficha.sq_situacao_ficha = :sqSituacaoFicha")
                    qryParams.sqSituacaoFicha = params.sqSituacaoFiltro.toLong()

                }
            }

            // se for informado um nome nome cientifico apenas, desconsiderar se está excluida ou não
            if( params.nmCientificoFiltro && !( params.nmCientificoFiltro =~/;/ ) ){
                params.chkExcluidasFiltro = 'S'
            }

            // EXIBIR OU NÃO AS FICHAS EXCLUÍDAS
            if( params?.chkExcluidasFiltro != 'S') {
                DadosApoio situacaoExcluida = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA','EXCLUIDA')
                if( !params.sqSituacaoFiltro || !params.sqSituacaoFiltro.split(',').contains( situacaoExcluida.id.toString() ) ) {
                    qryWhere.push("ficha.sq_situacao_ficha <> ${situacaoExcluida.id.toString()}")
                }
            }

            // FILTRO PELO NOME COMUM
            if (params.nmComumFiltro) {
                qryWhere.push("EXISTS ( SELECT NULL from salve.ficha_nome_comum fnc where fnc.sq_ficha = ficha.sq_ficha and public.fn_remove_acentuacao(fnc.no_comum) ilike :nmComum limit 1 )")
                qryParams.nmComum = '%' + Util.removeAccents(params.nmComumFiltro) + '%'
            }

            // FILTRO PELO NIVEL TAXONOMICO
            if (params.nivelFiltro && params.sqTaxonFiltro) {
                if (params.sqTaxonFiltro.toString().indexOf(',')) {
                    qryWhereFinal.push("(taxon.json_trilha->'${params.nivelFiltro.toLowerCase()}'->>'sq_taxon')::integer in ( ${params.sqTaxonFiltro})")
                } else {
                    qryWhereFinal.push("(taxon.json_trilha->'${params.nivelFiltro.toLowerCase()}'->>'sq_taxon')::integer = :sqTaxon")
                    qryParams.sqTaxon = params.sqTaxonFiltro.toLong()
                }
            }

            // FILTRAR PELA CATEGORIA - HISTORICO - ABA 7
            if (params.sqCategoriaFiltro) {
                qryWhereFinal.push("""exists (
                            select null
                            from salve.taxon_historico_avaliacao h
                            where h.sq_taxon = ficha.sq_taxon
                            and h.sq_tipo_avaliacao = :sqTipoAvaliacao
                            and h.sq_categoria_iucn in (${params.sqCategoriaFiltro})
                            ${params.stPexFiltro=~/S|N/ ? " and h.st_possivelmente_extinta='${params.stPexFiltro}'" :''}
                            order by h.nu_ano_avaliacao desc, h.sq_taxon_historico_avaliacao desc
                            offset 0 limit 1
                            )"""
                )
                DadosApoio tipoAvaliacaoNacional = dadosApoioService.getByCodigo('TB_TIPO_AVALIACAO', 'NACIONAL_BRASIL')
                qryParams.sqTipoAvaliacao = tipoAvaliacaoNacional.id.toLong()
            }

            // FILTRAR PELA CATEGORIA - AVALIADA OU VALIDADA - ABA 11.1, 11.2
            if( params.sqCategoriaAvaliacaoFiltro){
                String noColunaFiltrarCategoria
                String noColunaFiltrarPEX
                if( ! params.chkCategoriaValidadaFiltro){
                    // ABA 11.1
                    noColunaFiltrarCategoria = 'ficha.sq_categoria_iucn'
                    noColunaFiltrarPEX = 'ficha.st_possivelmente_extinta'
                } else {
                    // ABA 11.2
                    noColunaFiltrarCategoria = 'ficha.sq_categoria_final'
                    noColunaFiltrarPEX = 'ficha.st_possivelmente_extinta_final'
                }

                if (params.sqCategoriaAvaliacaoFiltro.toString().indexOf(',')) {
                    qryWhere.push("${noColunaFiltrarCategoria} in (${params.sqCategoriaAvaliacaoFiltro})")
                } else {
                    qryWhere.push("${noColunaFiltrarCategoria} = :sqCategoria")
                    qryParams.sqCategoria = params.sqCategoriaAvaliacaoFiltro.toLong()
                }
                if( params.stPexAvaliacaoFiltro =~ /S|N/){
                    qryWhere.push("${noColunaFiltrarPEX}='${params.stPexAvaliacaoFiltro}'")
                }
            }





                /*
                if (params.chkAvaliadaNoCicloFiltro == 'S') {
                    if (params.sqCategoriaFiltro.toString().indexOf(',')) {
                        qryWhere.push("ficha.sq_categoria_iucn in (${params.sqCategoriaFiltro})")
                    } else {
                        qryWhere.push("ficha.sq_categoria_iucn = :sqCategoria")
                        qryParams.sqCategoria = params.sqCategoriaFiltro.toLong()
                    }
                    if( params.stPexFiltro == 'S'){
                        qryWhere.push("ficha.st_possivelmente_extinta='S'")
                    } else if( params.stPexFiltro =='N'){
                        qryWhere.push("ficha.st_possivelmente_extinta='N'")
                    }
                } else {
                    qryWhereFinal.push("""( ficha.sq_categoria_final in (${params.sqCategoriaFiltro}) or  exists (
                            with cte_historico as (
                                select h.sq_categoria_iucn, h.st_possivelmente_extinta
                                from salve.taxon_historico_avaliacao h
                                where h.sq_taxon = ficha.sq_taxon
                                and h.sq_tipo_avaliacao = :sqTipoAvaliacao
                                and h.nu_ano_avaliacao <= ciclo.nu_ano + 4
                                order by h.nu_ano_avaliacao desc
                                offset 0
                                limit 1 )
                                select null from cte_historico where cte_historico.sq_categoria_iucn in (${params.sqCategoriaFiltro})
                                ${params.stPexFiltro=~/S|N/ ? " and cte_historico.st_possivelmente_extinta='${params.stPexFiltro}'" :''}
                            )
                        )""")
                    DadosApoio tipoAvaliacaoNacional = dadosApoioService.getByCodigo('TB_TIPO_AVALIACAO', 'NACIONAL_BRASIL')
                    qryParams.sqTipoAvaliacao = tipoAvaliacaoNacional.id.toLong()
                }
            }
            */
            // FILTRAR PELO GRUPO AVALIADO
            if (params.sqGrupoFiltro) {
                if (params.sqGrupoFiltro.toString().indexOf(',')) {
                    qryWhere.push("ficha.sq_grupo in ( ${params.sqGrupoFiltro} )")
                } else {
                    qryWhere.push("ficha.sq_grupo = :sqGrupoFiltro")
                    qryParams.sqGrupoFiltro = params.sqGrupoFiltro.toLong()
                }
                if (params.sqSubgrupoFiltro) {
                    if (params.sqSubgrupoFiltro.toString().indexOf(',')) {
                        qryWhere.push("ficha.sq_subgrupo in ( ${params.sqSubgrupoFiltro} )")
                    } else {
                        qryWhere.push("ficha.sq_subgrupo = :sqSubgrupoFiltro")
                        qryParams.sqSubgrupoFiltro = params.sqSubgrupoFiltro.toLong()
                    }
                }
            }

            // FILTRAR PELO GRUPO SALVE
            if ( params.sqGrupoSalveFiltro) {

                if ( params.sqGrupoSalveFiltro.toString().indexOf(',')) {
                    if( params.sqGrupoSalveFiltro.toString().indexOf('-1') > -1 ) {
                        qryWhere.push("( ficha.sq_grupo_salve in ( ${params.sqGrupoSalveFiltro}) or ficha.sq_grupo_salve is null )")
                    } else {
                        qryWhere.push("ficha.sq_grupo_salve in ( ${params.sqGrupoSalveFiltro})")
                    }
                } else {
                    if( params.sqGrupoSalveFiltro.toString() ==  '-1' ) {
                        qryWhere.push("ficha.sq_grupo_salve is null")
                     } else {
                        qryWhere.push("ficha.sq_grupo_salve = :sqGrupoSalveFiltro")
                        qryParams.sqGrupoSalveFiltro = params.sqGrupoSalveFiltro.toLong()
                    }

                }
            }

            // FILTRAR FICHAS COM/SEM FOTO PRINCIPAL CONSIDERANDO CICLO ANTERIOR
            if (params.stImagemPrincipalFiltro) {
                DadosApoio tipoImagem = dadosApoioService.getByCodigo('TB_TIPO_MULTIMIDIA','IMAGEM')
                qryParams.sqTipoImagem = tipoImagem.id
                // qryWhere.push("${params.stImagemPrincipalFiltro == 'COM_IMAGEM' ? '' : 'NOT'} EXISTS ( select null from salve.ficha_multimidia x, salve.dados_apoio y where y.sq_dados_apoio = x.sq_tipo and y.cd_sistema = 'IMAGEM' and x.sq_ficha = ficha.sq_ficha and x.in_principal = true OFFSET 0 LIMIT 1)")
                if( params.stImagemPrincipalFiltro=='COM_IMAGEM') {
                    //qryInnerJoins.push("""inner join salve.ficha_multimidia m on m.sq_ficha = ficha.sq_ficha and m.in_principal=true and m.sq_tipo = :sqTipoImagem""")
                    qryWhere.push("""exists ( select null from salve.ficha_multimidia fm
                                        inner join salve.ficha as ficha_1 on ficha_1.sq_ficha= fm.sq_ficha
                                        where ( ficha_1.sq_ficha in ( ficha.sq_ficha, ficha.sq_ficha_ciclo_anterior )
                                               OR ficha_1.sq_taxon = ficha.sq_taxon)
                                        and fm.in_principal = true
                                        and fm.sq_tipo = :sqTipoImagem
                                        limit 1
                                        )""")

                } else {
                    //qryInnerJoins.push("""left join salve.ficha_multimidia m on m.sq_ficha = ficha.sq_ficha and m.in_principal=true and m.sq_tipo = :sqTipoImagem""")
                    //qryWhere.push("""m.sq_ficha_multimidia is null""")
                    qryWhere.push("""NOT exists ( select null from salve.ficha_multimidia fm
                                        inner join salve.ficha as ficha_1 on ficha_1.sq_ficha= fm.sq_ficha
                                        where ( ficha_1.sq_ficha in ( ficha.sq_ficha, ficha.sq_ficha_ciclo_anterior )
                                               OR ficha_1.sq_taxon = ficha.sq_taxon )
                                        and fm.in_principal = true
                                        and fm.sq_tipo = :sqTipoImagem
                                        limit 1
                                        )""")
                }

            }

            // FILTRAR FICHAS COM/SEM MAPA DE DISTRIBUIÇAO INFORMADO
            if (params.stMapaDistribuicaoFiltro) {
               // qryWhere.push("${params.stMapaDistribuicaoFiltro == 'COM_IMAGEM' ? '' : 'NOT'} EXISTS ( select x.sq_ficha_anexo from salve.ficha_anexo x where x.sq_ficha = ficha.sq_ficha and x.in_principal='S' and x.de_tipo_conteudo ilike '%image%' OFFSET 0 LIMIT 1)")
                if( params.stMapaDistribuicaoFiltro == 'COM_IMAGEM' ) {
                    qryInnerJoins.push("""inner join salve.ficha_anexo anexo on anexo.sq_ficha = ficha.sq_ficha and anexo.in_principal = 'S' and anexo.de_tipo_conteudo ilike '%image%'""")
                } else {
                    qryInnerJoins.push("""left join salve.ficha_anexo anexo on anexo.sq_ficha = ficha.sq_ficha and anexo.in_principal = 'S' and anexo.de_tipo_conteudo ilike '%image%'""")
                    qryWhere.push("""anexo.sq_ficha_anexo is null""")
                }
            }

            // FILTRAR FICHAS PELA OFICINA
            if (params.sqOficinaFiltro) {
                //qryWhere.push('EXISTS ( select null from salve.oficina_ficha x where x.sq_oficina = :sqOficina and x.sq_ficha = ficha.sq_ficha offset 0 limit 1)')
                qryInnerJoins.push("inner join salve.oficina_ficha ofi on ofi.sq_ficha = ficha.sq_ficha and ofi.sq_oficina = :sqOficina")
                qryParams.sqOficina = params.sqOficinaFiltro.toLong()
            }
            if (params.sqOficinaMultiFiltro) {
                //qryWhere.push("EXISTS ( select null from salve.oficina_ficha x where x.sq_oficina in ( ${params.sqOficinaMultiFiltro}) and x.sq_ficha = ficha.sq_ficha offset 0 limit 1)")
                qryInnerJoins.push("inner join salve.oficina_ficha ofi on ofi.sq_ficha = ficha.sq_ficha and ofi.sq_oficina in(${params.sqOficinaMultiFiltro})")
                //qryParams.sqOficina = params.sqOficinaFiltro.toLong()
            }

            if (params.sqEstadoFiltro) {
                qryWhere.push("exists ( select null from salve.fn_taxon_estados(ficha.sq_taxon) x where x.sq_estado in (${params.sqEstadoFiltro.toString()}) limit 1)")
                /*
                DadosApoio contextoOcorrencia   = dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA', 'REGISTRO_OCORRENCIA')
                DadosApoio contextoEstado       = dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA', 'ESTADO')
                passos.push("""select x.sq_ficha
                from salve.ficha_uf x
                inner join motor on motor.sq_ficha = x.sq_ficha
                where x.sq_estado in (${params.sqEstadoFiltro.toString() })
                UNION
                select z.sq_ficha
                from salve.registro_estado x
                inner join salve.ficha_ocorrencia_registro y on y.sq_registro = x.sq_registro
                inner join salve.ficha_ocorrencia z on z.sq_ficha_ocorrencia = y.sq_ficha_ocorrencia
                inner join motor on motor.sq_ficha = z.sq_ficha
                where x.sq_estado in (${params.sqEstadoFiltro.toString() })
                and ( z.in_presenca_atual is null or z.in_presenca_atual <> 'N' )
                and (z.in_utilizado_avaliacao = 'S' or (z.sq_contexto = ${contextoOcorrencia.id.toString() } and z.in_utilizado_avaliacao is null))
                UNION
                select z.sq_ficha
                from salve.registro_estado x
                inner join salve.ficha_ocorrencia_portalbio_reg y on y.sq_registro = x.sq_registro
                inner join salve.ficha_ocorrencia_portalbio z
                on z.sq_ficha_ocorrencia_portalbio = y.sq_ficha_ocorrencia_portalbio
                inner join motor on motor.sq_ficha = z.sq_ficha
                where x.sq_estado in (${params.sqEstadoFiltro.toString() })
                and z.in_utilizado_avaliacao = 'S'
                """)*/
            }

            // PRESENÇA LISTA OFICIAL
            if( params.stListaOficialFiltro ) {
                String whereListaOficial
                if( params.stListaOficialFiltro == 'S') {
                    whereListaOficial = "ficha.st_presenca_lista_vigente='S'"
                } else {
                    whereListaOficial = "( ficha.st_presenca_lista_vigente is null or ficha.st_presenca_lista_vigente = 'N' )"
                }
                qryWhere.push( whereListaOficial )
            }

            //------------------------------------
            // UNIDADE ORGANIZACIONAL USUARIOS EXTERNOS
            if ( ! session.sicae.user.isADM() ) {
                DadosApoio papel
               //  se for perfil consulta pode visualizar todas as fichas sem modificar
               if( session?.sicae?.user?.isCO() ) {
                    params.canModify = false
                } else {
                    //if( ( session.sicae.user.isCT() || session.sicae.user.isCE() ) && ! session?.sicae?.user?.sqUnidadeOrg  )
                    // SE O USUÁRIO NÃO TIVER UNIDADE, FAZER FILTRO PELA FUNÇÃO ATRIBUIDA NAS FICHAS
                    if (!session?.sicae?.user?.sqUnidadeOrg) {
                        papel = dadosApoioService.getByCodigo('TB_PAPEL_FICHA', (session.sicae.user.isCT() ? 'COORDENADOR_TAXON' : 'COLABORADOR_EXTERNO'))
                        if (!papel) {
                            throw new Exception('Perfil ' + (session.sicae.user.isCT() ? 'COORDENADOR_TAXON' : 'COLABORADOR_EXTERNO') + ' não cadastrado na tabela TB_PAPEL_FICHA')
                        }
                        // FILTRAR PELAS FICHAS ATRIBUIDAS AO PAPEL/FUNCAO
                        qryWhere.push("exists ( select null from salve.ficha_pessoa fp where fp.sq_pessoa = :sqPessoa and fp.sq_papel = :sqPapel and in_ativo = 'S' and fp.sq_ficha = ficha.sq_ficha limit 1 )")
                        qryParams.sqPessoa = session?.sicae?.user?.sqPessoa?.toLong()
                        qryParams.sqPapel = papel?.id?.toLong()

                    } else if (session?.sicae?.user?.sqUnidadeOrg) {
                        params.sqUnidadeFiltro = session.sicae.user.sqUnidadeOrg.toLong()
                    } else {
                        throw new Exception('Sem acesso a estas informações')
                    }
                }
            }

            // FILTRAR PELA UNIDADE ORGANIZACIONAL
            if (params.sqUnidadeFiltro) {
                qryWhere.push('ficha.sq_unidade_org= :sqUnidadeOrg')
                qryParams.sqUnidadeOrg = params.sqUnidadeFiltro.toLong()
            }

            // FILTRAR INCLUIDAS NO CICLO ATUAL, SEM RESULTADO NO CICLO ANTERIOR
            if (params.chkIncluidasCicloFiltro == 'S') {
                CicloAvaliacao cicloAnterior = CicloAvaliacao.createCriteria().get {
                    lt('nuAno', cicloAvaliacao.nuAno)
                    order('nuAno', 'desc')
                    maxResults(1)
                }
                if( cicloAnterior ) {
                    qryWhere.push("""not exists ( select null from salve.ficha ficha_1
                        where ficha_1.sq_ciclo_avaliacao = ${cicloAnterior.id.toString()}
                          and ficha_1.sq_categoria_iucn_sugestao is null and ficha_1.sq_taxon = ficha.sq_taxon offset 0 limit 1 )""")
                }
            }

            // FILTRAR EXIBIDAS/NAO EXIBIDAS MÓDULO PUBLICO
            if( params.stExibirModuloPublicoFiltro){
                qryInnerJoins.push('left outer join salve.ficha_modulo_publico fmp on fmp.sq_ficha = ficha.sq_ficha')
                if( params.stExibirModuloPublicoFiltro.toLowerCase() == 's') {
                    qryWhere.push('fmp.st_exibir=true')
                } else {
                    qryWhere.push('(fmp.st_exibir=false or fmp.st_exibir is null)')
                }
            }

            // COLUNA ORDER BY
            if (params.sortColumns) {
                List sortParams = params.sortColumns.split(':')
                if( sortParams[0] == 'ficha.nm_cientifico' ) {
                    sortParams[0] = "no_especie ${ sortParams[1] ? sortParams[1] : ''}, nu_grau_taxonomico, no_subespecie"
                }
                qryOrderBy = sortParams[0] + ' ' + (sortParams[1] ?: '')
            }

            // TABLE - SAIDA SERA O GRIDE DE FICHAS
            //String ultimoPasso = 'motor'
            cmdSql = ''
            /*cmdSql = """with motor as (
                            select distinct ficha.sq_ficha
                            from salve.ficha
                            ${qryInnerJoins ? qryInnerJoins.join('\n') :''}
                            ${qryWhere ? '\nwhere ' + qryWhere.join(' and ') : ''}
                       )"""
            List listPassos = []
            passos.eachWithIndex{ sql, index ->
                ultimoPasso = 'passo' + index.toString()
                listPassos.push( ultimoPasso +' as ('+ sql + ')')
            }
            //cmdSql += listPassos ? ', ' + listPassos.join('\n,'):''
            //cmdSql += "\n"*/
            if (outputFormat == 'TABLE') {
                cmdSql += """select ficha.sq_ficha
                    , ficha.sq_ciclo_avaliacao
                    , ficha.sq_unidade_org
                    , ficha.sq_situacao_ficha
                    , ficha.nm_cientifico
                    , uo.sg_unidade_org
                    , ficha.dt_alteracao
                    , pessoa.no_pessoa as no_usuario_alteracao
                    , situacao.ds_dados_apoio as ds_situacao_ficha
                    , situacao.cd_sistema as cd_situacao_ficha_sistema
                    , ficha.vl_percentual_preenchimento
                    , pendencias.nu_pendencia
                    , nivel.co_nivel_taxonomico
                    , case when ciclo.in_situacao = 'F' then false else true end as st_alterar
                    , versoes.js_versoes::text
                    , taxon.json_trilha->'especie'->>'no_taxon' as no_especie
                    , taxon.json_trilha->'subespecie'->>'no_taxon' as no_subespecie

                    """
            } else if( outputFormat=='CSV') {
                // CSV - EXPORTAR FICHAS PARA PLANILHA
                cmdSql += """select ficha.sq_ficha"""
            } else if( outputFormat=='PDF') {
                // PDF - EXPORTAR PDF DAS FICHAS EM UM ARQUIVO ZIP
                cmdSql += """select ficha.sq_ficha"""
            } else if( outputFormat=='RTF') {
                // RTF - EXPORTAR DOCF DAS FICHAS EM UM ARQUIVO ZIP
                cmdSql += """select ficha.sq_ficha"""
            } else if( outputFormat=='REG') {
                // REG - EXPORTAR OS REGISTROS DE OCORRENCIAS PARA PLANILHA
                cmdSql += """select ficha.sq_ficha"""
            } else if( outputFormat=='PEN') {
                // REG - EXPORTAR AS PENDENCIAS
                cmdSql += """select ficha.sq_ficha"""
            } else if( outputFormat=='SHP') {
                // SHP - EXPORTAR OS ARQUIVO SHAPEFILES DAS FICHAS
                cmdSql += """select ficha.sq_ficha"""
            } else {
                throw new Exception('Problema na exportação. Formato '+outputFormat+' incorreto')
            }
//            inner join ${ultimoPasso} on ${ultimoPasso}.sq_ficha = ficha.sq_ficha

            cmdSql += """\nfrom salve.ficha
                    inner join salve.ciclo_avaliacao as ciclo on ciclo.sq_ciclo_avaliacao = ficha.sq_ciclo_avaliacao
                    inner join salve.vw_unidade_org as uo on uo.sq_pessoa = ficha.sq_unidade_org
                    inner join salve.dados_apoio as situacao on situacao.sq_dados_apoio = ficha.sq_situacao_ficha
                    inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                    inner join taxonomia.nivel_taxonomico as nivel on nivel.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
                    ${qryInnerJoins ? qryInnerJoins.join('\\n') : ''}
                    left outer join salve.vw_usuario as pessoa on pessoa.sq_pessoa = ficha.sq_usuario_alteracao
                    left join lateral (
                            select json_agg( json_build_object('sq_ficha_versao',ficha_versao.sq_ficha_versao,
                                'nu_versao',ficha_versao.nu_versao,
                                'cd_contexto_sistema',contexto.cd_sistema,
                                'dt_versao',to_char(ficha_versao.dt_inclusao,'dd/mm/yyyy')
                                )) as js_versoes
                            from salve.ficha_versao
                            inner join salve.dados_apoio as contexto on contexto.sq_dados_apoio = ficha_versao.sq_contexto
                            where ficha_versao.sq_ficha = ficha.sq_ficha
                        ) as versoes on true

                    left join lateral (
                       select count(pendencias.sq_ficha_pendencia ) as nu_pendencia
                       from salve.ficha_pendencia as pendencias
                       where pendencias.sq_ficha = ficha.sq_ficha
                       and pendencias.st_pendente = 'S'
                    ) as pendencias on true
                    ${qryWhere ? 'where ' + qryWhere.join('\nand ') : ''}
                    ${qryWhereFinal ? '\nand ' + qryWhereFinal.join('\nand ') : ''}
                    ${qryOrderBy ? '\norder by ' + qryOrderBy : ''}
            """
/** /
// zzz
println cmdSql
println qryParams
/**/
            if (outputFormat == 'TABLE') {
                data = sqlService.paginate(cmdSql, qryParams, params,100,10,0)
            }
            else if (outputFormat == 'CSV' ) {
                List idsFichas = []
                // usuário pode ter selecionado algumas fichas no gride
                if (params.exportIds) {
                    idsFichas = params.exportIds.split(',')
                } else {
                    List rows = sqlService.execSql(cmdSql + ' OFFSET 0 LIMIT ' + (this.maxFichasExportar + 1), qryParams)
                    // pegar somente a coluna sq_ficha do resultado
                    if (rows) {
                        idsFichas = rows.sq_ficha
                    }
                }
                if (idsFichas && idsFichas.size() > this.maxFichasExportar) {
                    throw new Exception('Limite máximo para esta solicitação é de ' + this.maxFichasExportar.toString() + ' fichas.')
                }

                // criar variavel local selfSession para utilzar workers
                Map exportParams = [usuario     : session?.sicae?.user
                                  , email       : params.email
                                  , idsFichas   : idsFichas
                ]


                result.msg = 'A exportação de <b>' + idsFichas.size().toString() + '</b> ficha(s) para planilha está em andamento.<br>Ao finalizar será '+
                    (params.email ? 'enviado para o email <b>'+params.email+'</b> o link para baixar a planilha.' : 'exibida a tela para baixar a planilha.')

                if( ! exportParams.idsFichas ) {
                     throw new Exception('Nenhuma ficha encontrada. Verifique se existe algum campo dos filtros preenchido incorretamente.')
                }
                registrarTrilhaAuditoria('exportar_fichas', 'ficha',exportParams)
                exportDataService.gridFichasPlanilha( exportParams )

            }
            else if ( outputFormat =~ /PDF|RTF/ ) {
                List idsFichas = []
                // usuário pode ter selecionado algumas fichas no gride
                if (params.exportIds) {
                    idsFichas = params.exportIds.split(',')
                }/* else {
                    //println cmdSql
                    List rows = sqlService.execSql(cmdSql + ' OFFSET 0 LIMIT ' + (this.maxFichasExportar + 1), qryParams)
                    // pegar somente a coluna sq_ficha do resultado
                    if (rows) {
                        idsFichas = rows.sq_ficha
                    }
                }
                if (idsFichas && idsFichas.size() > this.maxFichasExportarPdf) {
                    throw new Exception('Limite máximo para esta solicitação é de ' + this.maxFichasExportarPdf.toString() + ' fichas.')
                }*/

                // criar variavel local selfSession para utilzar workers
                Map exportParams = [ usuario      : session?.sicae?.user
                                    , email       : params.email
                                    , format      : outputFormat
                                    , idsFichas   : idsFichas
                                    , pendencias  : params.pendencias ?: 'N'
                                    , maxFichasExportarPdf : maxFichasExportarPdf
                                    , cmdSql      : idsFichas ? '': cmdSql
                                    , sqlParams   : idsFichas ? [:] : qryParams
                                    ]

                result.msg = 'A exportação da(s) ficha(s) para o formato '+outputFormat+' está em andamento.<br>Ao finalizar será '+
                    (params.email ? 'enviado para o email <b>'+params.email+'</b> o link para baixar a planilha.' : 'exibida a tela para baixar a planilha.')

                registrarTrilhaAuditoria('exportar_fichas_'+outputFormat.toLowerCase(), 'ficha', exportParams)

                exportDataService.gridFichasZip(exportParams)

                /*
                // criar variavel local selfSession para utilzar workers
                GrailsHttpSession selfSession = session
                Map workerData = [sqPessoa   : session?.sicae?.user?.sqPessoa
                                  , contexto    : 'ficha'
                                  , email       : params.email
                                  , pendencias  : params.pendencias ?: 'N'
                                  , format      : outputFormat
                                  , idsFichas   : idsFichas
                ]
                result.msg = 'A exportação de <b>' + idsFichas.size().toString() + ' ficha(s)</b> está em andamento.<br><br>Ao finalizar será '+
                    (params.email ? 'enviado para o email <b>'+params.email+'</b> o link para baixar o arquivo.' : 'exibida a tela para baixar o arquivo.')+
                    '<br><br>Dependendo da quantidade de fichas este procedimento poderá demorar. <span class="red">(Não encerre a sua sessão)</span>'


                registrarTrilhaAuditoria('exportar_fichas_'+outputFormat.toLowerCase(), workerData.contexto, workerData)
                runAsync {
                    exportDataService.gridFichasZip(selfSession, workerData)
                }*/
            }
            else if (outputFormat == 'REG' ) {
                Map exportParams = [
                     ids                 : params.exportIds // exportar somente os registros das fichas selecionadas no gride
                    ,maxFichas           : maxFichasExportarOcorrencias
                    ,usuario             : session?.sicae?.user
                    ,email               : params.email ?: ( params?.enviarEmail?.toUpperCase() == 'S' && session.sicae.user.email ? session.sicae.user.email : '' )
                    ,excluirSensiveis    : params.excluirRegistrosSensiveis
                    ,excluirCarencia     : params.excluirRegistrosEmCarencia
                    ,utilizadoAvaliacao  : params?.utilizadosAvaliacao == 'AAA' ? '' : params.utilizadosAvaliacao
                    ,adicionadosAposAvaliacao : (params?.utilizadosAvaliacao.toUpperCase() == 'AAA' )
                    ,colunasSelecionadas : params.colsSelected ?: ''
                    ,emailMessage        : ''
                    ,cmdSql              : cmdSql // sql para selecionar as fichas para exportação dos registros
                    ,sqlParams           : qryParams // parametros para execução do comando sql

                ]
                registrarTrilhaAuditoria('exportar_ocorrencia','ficha',exportParams )
                exportDataService.asyncExportarOcorrencias( exportParams )
                result.msg = 'A exportação dos registros de ocorrência da(s) ficha(s) está em andamento.<br><br>Ao finalizar será '+
                    (params.email ? 'enviado para o email <b>'+params.email+'</b> o link para baixar a planilha.' : 'exibida a tela para baixar a planilha.')+msgAvisoSessao
            }
            else if (outputFormat == 'PEN' ) {

                Map exportParams = [
                    idsFichas            : params.exportIds ? params.exportIds.split(',') : ''
                    ,cmdSql              : params.exportIds ? ''  : cmdSql
                    ,sqlParams           : params.exportIds ? [:] : qryParams
                    ,maxFichas           : maxFichasExportar
                    ,usuario             : session?.sicae?.user
                    ,email               : params.email
                    ,emailMessage        : ''
                ]

                registrarTrilhaAuditoria('exportar_pendencia','ficha', exportParams)
                exportDataService.asyncExportPendenciaFicha( exportParams )

                result.msg = 'A exportação das pendências das fichas está em andamento.<br><br>Ao finalizar será '+
                    (params.email ? 'enviado para o email <b>'+params.email+'</b> o link para baixar a planilha.' : 'exibida a tela para baixar a planilha.')
            }
            else if ( outputFormat =~ /SHP/ ) {


                Map exportParams = [
                    idsFichas            : params.exportIds ? params.exportIds.split(',') : ''
                    ,cmdSql              : params.exportIds ? ''  : cmdSql
                    ,sqlParams           : params.exportIds ? [:] : qryParams
                    ,maxFichas           : maxFichasExportar
                    ,usuario             : session?.sicae?.user
                    ,email               : params.email
                    ,emailMessage        : ''
                    ,shapefiles          : params.shapefiles ?: 'A' // padrão é exportar somente os Atuais
                ]

                registrarTrilhaAuditoria('exportar_fichas_shp','ficha', exportParams)
                exportDataService.asyncExportShapefileFicha( exportParams )

                result.msg = 'A exportação das pendências das fichas está em andamento.<br><br>Ao finalizar será '+
                    (params.email ? 'enviado para o email <b>'+params.email+'</b> o link para baixar a planilha.' : 'exibida a tela para baixar a planilha.')

            }
        } catch (Exception e) {
            errorMessage = e.getMessage()
            result.msg = errorMessage
            //e.printStackTrace()
        }
        // quando for exportacao de dados retornar o json do resultado
        if ( outputFormat != 'TABLE' ) {
            result.status=0
            if (errorMessage) {
                result.status=1
                result = [msg: errorMessage, status: 1, type: 'error']
                println result
            }
            render result as JSON
            return
        }
        data.rows.each{
            // colocar as versões em ordem decrescente
            //it.versoes = [:]
            it.versoes = it.js_versoes ? JSON.parse(it.js_versoes ).sort{  a,b-> b.nu_versao <=> a.nu_versao } : [:]
        }


        // renderizar o gride
        render(template: "divGridFichas", model: [listFichas    : data.rows
                                                  , canModify   : params.canModify
                                                  , pagination  : data.pagination
                                                  , errorMessage: errorMessage])
    }


    /**
     * chamadas para este método devem ser substituidas pelo método getGridFicha()
     * @deprecated
    */
    def list() {
        GrailsHttpSession selfSession
        Integer registrosPorPagina = 100
        Map res     = [:]
        Map filtros = [:]
        List ids    = []

        try {
            if (!session?.sicae?.user) {
                throw new Exception('Sessão expirada. Faça o login novamente!')
            }

            if (!params.sqCicloAvaliacao) {
                throw new Exception('Id do ciclo não informado.')
            }

            CicloAvaliacao cicloAvaliacao = CicloAvaliacao.get(params.sqCicloAvaliacao.toInteger())
            if (!cicloAvaliacao) {
                throw new Exception('Ciclo avaliacao inválido.')
            }

            // exportar registros de ocorrencias
            if (params.exportarOcorrencias == 'S') {
                if (session.exportingOccurrences) {
                    throw new Exception( 'Já existe uma exportação de registros em andamento. Aguarde o término.')
                }
                ids = params.ids ? params.ids.split(',') : []
                registrosPorPagina = -1 // cancelar a paginacao
            } else {
                // não paginar quando for exportação das fichas para planilha
                if (params.csv && params.csv == 'S') {
                    // ler a fichas
                    //params.paginationPageSize=100 // exibir apenas 100 registros por página
                    registrosPorPagina = -1 // cancelar a paginacao
                }

                // gravar a pagina atual na sessao para voltar na pagina correta apos a edição da ficha
                if (!params?.voltar) {
                    session['listFichaPagination'] = [:]
                    if (params.paginationTotalRecords) {
                        params.each { key, value ->
                            if (key =~ /^pagination/) {
                                session['listFichaPagination'][key] = value
                            }
                        }
                    }
                } else {
                    params.remove('voltar')
                    session['listFichaPagination'].each { key, value ->
                        params[key] = value
                    }
                    session['listFichaPagination'] = [:]
                }
                params.colCanModify = true // adicionar coluna canModify no resultado do search
                params.colNivelTaxonomico = true // adicionar coluna co_nivel_taxonico no resultado do search
                // guardar os filtros

                params.each {
                    if (it.key =~ /Filtro$/) {
                        filtros[it.key] = it.value
                    }
                }
                filtros.sqCicloAvaliacao = cicloAvaliacao.id
            }

            if (!ids) {
                // executar a consulta  das fichas
                res = fichaService.searchPaginated(params, registrosPorPagina)
                ids = res.data.sq_ficha
            }

            if (params.exportarOcorrencias == 'S') {

                /*
                // revalidar o parametro excluirRegistrosEmCarencia
                // revalidar o parametro excluirRegistrosEmCarencia
                if( ! session.sicae.user.canExportRegsCarencia() ) {
                    params.excluirRegistrosEmCarencia = 'S'
                }

                res = [status: 0, type: 'success', msg: '']
                if (ids.size() > maxFichasExportarOcorrencias) {
                    throw new Exception('Para exportação dos registros de ocorrencia o limite MÁXIMO é de <b>' + maxFichasExportarOcorrencias.toString() + '</b> táxons.')
                }
                String email = (params?.enviarEmail == 'S' ? session?.sicae?.user?.email : '')
                Map workerData = [sqPessoa                    : session?.sicae?.user?.sqPessoa
                                  , ids                       : ids
                                  , idsType                   : params.idsType
                                  , sqCicloAvaliacao          : params.sqCicloAvaliacao
                                  , nuAnoCicloAvaliacao       : cicloAvaliacao.nuAno
                                  , email                     : email
                                  , excluirRegistrosSensiveis : params.excluirRegistrosSensiveis
                                  , excluirRegistrosEmCarencia: params.excluirRegistrosEmCarencia
                                  , utilizadosAvaliacao       : params.utilizadosAvaliacao
                                  , contexto                  : params.contexto ?: 'ficha'
                ]
                selfSession = session
                // gravar log
                registrarTrilhaAuditoria('exportar_ocorrencia',workerData.contexto.toString(), (Map) workerData )
                runAsync {
                    try {
                        exportDataService.ocorrenciasPlanilha(selfSession, workerData)
                    }
                    catch (Exception e) {
                        throw e
                    }
                }*/
                render res as JSON
                return

            } else {
                render(template: 'divGridFicha', model: [cicloAvaliacao      : cicloAvaliacao
                                                         , fichas            : res.data
                                                         , pagination        : res.pagination
                                                         , filtros           : filtros as JSON
                                                         , unidadeSelecionada: params.int('sqUnidade')])
                return
            }
        } catch( Exception e ){
            println ' '
            println 'fichaController - list()'
            println e.getMessage()
            res.msg = e.getMessage()
            res.status = 1
            res.type = 'error'
            render res as JSON
        }
        render ''
    }

    def edit() {
        Ficha ficha = null
        List listPontoFocal = null
        String comoCitarFicha=''

        // se for uma inclusão, verificar se o ciclo selecionado está aberto
        if ( ! params?.sqFicha ) {

            if( !session?.sicae?.user?.sqPessoa )
            {
                render '<h3>Usuário não identificado pelo CPF.<h3>'
                return
            }

            if (!params['sqCicloAvaliacao']) {
                render '<h3>Para cadastrar uma ficha é necessário informar o ciclo de avaliação<h3>'
                return
            }
            // carregar o ciclo de avaliação selecionado.
            def cicloAvaliacao = CicloAvaliacao.get(params['sqCicloAvaliacao'])
            if (!cicloAvaliacao) {
                render '<h3>Ciclo de Avaliação selecionado não existe!<h3>'
                return
            }

            if (cicloAvaliacao.inSituacao != "A") {
                render '<h3>Ciclo de Avaliação selecionado não está aberto!<h3>'
                return
            }

            // simular unidade para ADM - REMOVER DEPOIS
            //session.sicae.user.sqUnidadeOrg = 4
            //session.sicae.user.sgUnidadeOrg = 'ICMBio'

            /*if ( ! session?.sicae?.user?.sqUnidadeOrg) {
                render '<h3>Usuário sem lotação<h3>'
                return
            }*/

            // somente ADM pode incluir ficha no 1º ciclo - 20-10-2020
            if( cicloAvaliacao.id.toInteger() == 1 && ! session?.sicae?.user?.isADM() ) {
                render '<h3>Inclusão de ficha no 1º ciclo não permitida.<h3>'
                return
            }

            ficha = new Ficha()
            ficha.cicloAvaliacao = cicloAvaliacao
            ficha.taxon = new Taxon()
            ficha.grupo = new DadosApoio()
            ficha.grupoSalve = new DadosApoio()
            ficha.situacaoFicha = new DadosApoio()
            ficha.usuario = PessoaFisica.get(session.sicae.user.sqPessoa) // pessoa logada
            ficha.unidade = Unidade.get(session?.sicae?.user?.sqUnidadeOrg) // unidade organizacional do usuário logado

            // encontrar o ponto focal no modulo gerenciarPapel
            DadosApoio papel = dadosApoioService.getByCodigo('TB_PAPEL_FICHA','PONTO_FOCAL')

            /*
            println 'Selecionar ponto focal:'
            println 'Ciclo:' + cicloAvaliacao.id.toString()
            println 'Unidade:' + ficha.unidade.id.toString()
            println 'Papel: ' + papel.id.toString()
            println "-"*100
            println ' '
            */

            /* // não precisa mais da lista de pontos focais no cadatro de uma nova ficha
            listPontoFocal= FichaPessoa.createCriteria().list {
                //maxResults(1)
                pessoa {
                    order('noPessoa')
                }
                eq('papel',papel)
                vwFicha {
                    eq('sqCicloAvaliacao',cicloAvaliacao.id.toInteger())
                    if( ! session.sicae.user.isADM() ) {
                        eq('sqUnidadeOrg', ficha.unidade.id.toInteger())
                    }
                }
            }.pessoa.unique{ [it.id] }*/
        } else {
            ficha = Ficha.get(params['sqFicha'])
        }

        List gruposSalve = dadosApoioService.getTable('TB_GRUPO_SALVE')
        List grupos     = dadosApoioService.getTable('TB_GRUPO')
        List subgrupos  = dadosApoioService.getTable('TB_SUBGRUPO')

        // tags para referencia bibliográfica
        List listTags = dadosApoioService.getTable('TB_TAG')
        if(! ficha )
        {
            render '<h3>Não foi possivel criar ficha.<h3>'
            return
        }

        Boolean canModify = ficha.canModify( session?.sicae?.user )

        params.canModify = canModify

        // importar os pontos do portalbio
        if( canModify && ficha.id )
        {
            // fazer a inetgração das listas CITES com a API do speciesplus
            asyncService.checkEntrouSaiuListaCites( ficha.id.toInteger() )

            // fazer a inetgração das SALVE ICMBIo para atualizar o historico das avaliações
            asyncService.updateHistoricoAvaliacao( ficha.id.toInteger() )

        }
        comoCitarFicha = fichaService.comoCitarFicha( ficha )
        render(template: 'ficha', model: ['ficha': ficha
                                          , 'canModify': canModify
                                          , 'listTaxonHierarchy': ficha?.taxon?.hierarchy
                                          , 'listTags'  : listTags
                                          , 'gruposSalve'  : gruposSalve
                                          , 'grupos'    : grupos
                                          , 'subgrupos' : subgrupos
                                          , 'listPontoFocal':listPontoFocal
                                          , comoCitarFicha : comoCitarFicha
                                          ])
    } // fim edit ficha

    def delete() {
        Ficha ficha = Ficha.get(params.sqFicha)
        if (ficha) {
            try {
                // somente ADM pode excluir ficha no 1º ciclo
                if( ficha.cicloAvaliacao.id == 1 && !session?.sicae?.user?.isADM() ) {
                    return this._returnAjax(1, "Exclusão não permitida no 1º ciclo.", "info")
                }
                if( ficha.situacaoFicha.codigoSistema !='EXCLUIDA'){
                    return this._returnAjax(1, "Erro: Para excluir a ficha do banco de dados, ela precisa estar na situação EXCLUÍDA.", "modal")
                }
                ficha.delete()
                return this._returnAjax(0, "Ficha excluída com SUCESSO!", "success")
            }
            catch (Exception e) {
                return this._returnAjax(1, "Não foi possivel excluir a ficha!", "error")
            }
        }
        return this._returnAjax(1, "Ficha não encontrada!", "warning")
    }

    /**
     * metodo para criação dos grides da ficha
     * @return string html do gride
     */
    def getGrid() {
        String strController
        String strAction

        //sleep(3000);
        switch (params?.gridName) {
        //-----------------------------------------------------------
            case "nomeComum":
                strController = 'fichaTaxonomia'
                strAction = 'getGridNomeComum'
                break
        //-----------------------------------------------------------
            case "sinonimia":
                strController = 'fichaTaxonomia'
                strAction = 'getGridSinonimia'
                break
        //-----------------------------------------------------------
            case "distUf":
                strController = 'fichaDistribuicao'
                strAction = 'getGridDistUf'
                break
        //-----------------------------------------------------------
            case "distAnexo":
                strController = 'fichaDistribuicao'
                strAction = 'getGridAnexo'
                break
        //-----------------------------------------------------------
            case "distBioma":
                strController = 'fichaDistribuicao'
                strAction = 'getGridDistBioma'
                break
        //-----------------------------------------------------------
            case "distBaciaHidrografica":
                strController = 'fichaDistribuicao'
                strAction = 'getGridDistBaciaHidrografica'
                break
        //-----------------------------------------------------------
            case "distAmbienteRestrito":
                strController = 'fichaDistribuicao'
                strAction = 'getGridDistAmbienteRestrito'
                break
        //-----------------------------------------------------------
            case "distAmbienteRestrito":
                strController = 'fichaDistribuicao'
                strAction = 'getGridFichaAreaRelevancia'
                break
        // ----------------------------------------------------------
            case "distAreaRelevante":
                strController = 'fichaDistribuicao'
                strAction = 'getGridDistAreaRelevante'
                break
        //-----------------------------------------------------------
            case "municipiosAreaRelevante":
                strController = 'fichaDistribuicao'
                strAction = 'getGridMunicipiosAreaRelevante'
                break
        //-----------------------------------------------------------

            case "ocorrencia":
                strController = 'fichaDisOcorrencia'
                strAction = 'getGridOcorrencia'
                break
        // ----------------------------------------------------------
            case 'distOcoRefBib':
                strController = 'fichaDisOcorrencia'
                strAction = 'getGridRefBib'
                break
        // ----------------------------------------------------------

            case "habitoAlimentar":
                strController = 'fichaHistoriaNatural'
                strAction = 'getGridHabitoAlimentar'
                break
        // ----------------------------------------------------------
            case "habitoAlimentarEsp":
                strController = 'fichaHistoriaNatural'
                strAction = 'getGridHabitoAlimentarEsp'
                break
        // ----------------------------------------------------------
            case "habitat":
                strController = 'fichaHistoriaNatural'
                strAction = 'getGridHabitat'
                break
        // ----------------------------------------------------------
            case "interacao":
                strController = 'fichaHistoriaNatural'
                strAction = 'getGridInteracao'
                break
        // ----------------------------------------------------------
            case "variacaoSazonal":
                strController = 'fichaHistoriaNatural'
                strAction = 'getGridVariacaoSazonal'
                break
        // ----------------------------------------------------------
            case "populGraficos":
                strController = 'fichaPopulacao'
                strAction = 'getGridPopulGraficos'
                break
        // ----------------------------------------------------------
            case "populEstimada":
                strController = 'fichaPopulacao'
                strAction = 'getGridPopulEstimada'
                break
        // ----------------------------------------------------------
            case "popAreaVida":
                strController = 'fichaPopulacao'
                strAction = 'getGridPopAreaVida'
                break
        // ----------------------------------------------------------
            case "ameaca":
                strController = 'fichaAmeaca'
                strAction = 'getGridAmeaca'
                break
        // ----------------------------------------------------------
            case "ameacaAnexo":
                strController = 'fichaAmeaca'
                strAction = 'getGridAnexo'
                break

        // ----------------------------------------------------------
            case "uso":
                strController = 'fichaUso'
                strAction = 'getGridUso'
                break
        // ----------------------------------------------------------
            case "usoAnexo":
                strController = 'fichaUso'
                strAction = 'getGridAnexo'
                break

        // ----------------------------------------------------------
            case "selRegiao":
                strController = 'ficha'
                strAction = 'getGridSelRegiao'
                break

        // ----------------------------------------------------------
            case "selGeo":
                strController = 'ficha'
                strAction = 'getGridSelGeo'
                break

        // ----------------------------------------------------------
            case "historicoAvaliacao":
                strController = 'fichaConservacao'
                strAction = 'getGridHistoricoAvaliacao'
                break
        // ----------------------------------------------------------
            case "listaConvencao":
                strController = 'fichaConservacao'
                strAction = 'getGridListaConvencao'
                break
        // ----------------------------------------------------------
            case "presencaUc":
                strController = 'fichaConservacao'
                strAction = 'getGridPresencaUc'
                break
        // ----------------------------------------------------------
            case "acaoConservacao":
                strController = 'fichaConservacao'
                strAction = 'getGridAcaoConservacao'
                break
        // ----------------------------------------------------------
            case "pesquisa":
                strController = 'fichaPesquisa'
                strAction = 'getGridPesquisa'
                break

        // ----------------------------------------------------------
            case "declinioPopulacional":
                strController = 'fichaAvaliacao'
                strAction = 'getGridDeclinioPopulacional'
                break

        // ----------------------------------------------------------
            case "motivoMudancaCategoria":
                strController = 'fichaAvaliacao'
                strAction = 'getGridMotivoMudancaCategoria'
                break
        // ----------------------------------------------------------
            case "pendencia":
                strController = 'fichaPendencia'
                params.callback='ficha.updateGridPendencia'
                strAction = 'getGridPendencia'
                break
        // ----------------------------------------------------------
            case "refBib":
                strController = 'ficha'
                strAction = 'getGridRefBib'
                break
        // ----------------------------------------------------------
            case "allRefBib":
                strController = 'ficha'
                strAction = 'getGridAllRefBib'
                break
        // ----------------------------------------------------------
            case "cadRefBib":
                strController = 'ficha'
                strAction = 'getGridCadRefBib'
                break
        // ----------------------------------------------------------
            case "cadRefBibAnexos":
                strController = 'ficha'
                strAction = 'getGridCadRefBibAnexos'
                break
        // ----------------------------------------------------------
            case "modalAutores":
                strController = 'fichaAutor'
                strAction = 'getGrid'
                break
        // ----------------------------------------------------------
            case "pendencia":
                strController = 'fichaPendencia'
                params.onClose = 'ficha.updateGridPendencia'
                strAction = 'getGridPendencia'
                break
        // ----------------------------------------------------------
            default:
                render ''
                return
        //-----------------------------------------------------------
        }
        //Util.d('getGrid para ' + strController+'/'+strAction)
        forward controller: strController, action: strAction, params: params
    }

    /**
     * metodo para criação das janelas modais com ficha selecionada, precisa do sqFicha
     * @return string html da janela
     */
    def getModal() {
        if ( ! params.sqFicha && params.codigoCategoria != '' ) {
            render '<h3>Id da ficha não informado</h3>'
            return
        }

        String strController
        String strAction
        Ficha ficha
        Boolean canModify = true
        if( params.sqFicha )
        {
            ficha = Ficha.get( params.sqFicha.toLong() )
        }
        if( ficha ) {
            canModify = ficha.canModify(session?.sicae?.user)
        }
        Map model = [canModify: canModify]
        sleep(300)
        switch (params?.modalName) {
            case "selecionarCaverna":
                model.listEstados = Estado.list().sort{it.noEstado}
                break;
            case 'tranferirRegistroEspecie':
                break;
            case "selecionarRefBib":
                List listTags = dadosApoioService.getTable('TB_TAG')
                model.listTags = listTags
//                if( params.noTabela == 'taxon_historico_avaliacao' )
//                {
//                    FichaRefBib frb = FichaRefBib.createCriteria().get{
//                        eq('noTabela',params.noTabela)
//                        eq('noColuna',params.noColuna)
//                        eq('sqRegistro',params.sqRegistro.toInteger())
//                    }
//                    if( frb )
//                    {
//                        params.sqFicha = frb.ficha.id
//                    }
//                }
                break
        //----------------------------------------------------------------------
            case "selecionarRefBibTemp":
                break
        //----------------------------------------------------------------------
            case "selecionarRegiao":
                model.listAbrangencia = []
                dadosApoioService.getTable('TB_TIPO_ABRANGENCIA').sort { it.descricao }.each {
                    if (it.codigoSistema != 'PAIS') {
                        model.listAbrangencia.push(it)
                    }
                }
                break
        //----------------------------------------------------------------------
            case "selecionarGeo":
                model.listDatum = dadosApoioService.getTable('TB_DATUM')
                break
        //----------------------------------------------------------------------
            case "selCriterioAvaliacao":

                /*[reload:true, modalName:selCriterioAvaliacao, action:getModal, fieldCategoria:sqCategoriaIucn, sqFicha:26881, con
                 texto:frmResultadoOficina, field:dsCriterioAvalIucn, codigoCategoria:VU, container:, ajax:true, controller:ficha,
                 canModify:true]
                 */


                //model.listDatum = dadosApoioService.getTable('TB_DATUM')
                break
        //----------------------------------------------------------------------
            case "fichaAnexo":
                if (params.contexto) {
                    DadosApoio contexto = dadosApoioService.getByCodigo('TB_CONTEXTO_ANEXO', params.contexto)
                    model.listFichaAnexo = FichaAnexo.findAllByFichaAndContexto(ficha, contexto)
                } else {
                    render '<h3>Necessário informar o contexto</h3>'
                }
                break
        //----------------------------------------------------------------------
            case "cadComentarioValidador":
                model.id = params.id
                break
        //----------------------------------------------------------------------
            case "selAmeaca":
                break
        //----------------------------------------------------------------------
            case "selecionarAmeaca":
                model.referenciasTemporais = dadosApoioService.getTable('TB_REFERENCIA_TEMPORAL_AMEACA')
                break
        //----------------------------------------------------------------------
            case "selUso":
                break
        //----------------------------------------------------------------------
            case "selEstado":
                break

        //----------------------------------------------------------------------
            case "selBioma":
                break
        //----------------------------------------------------------------------
            case "selBacias":
                break
        //----------------------------------------------------------------------
            case "cadMunicipioFicha":
                break
        }
        render(template: "/modals/" + params.modalName, model: model)
    }

    def abaDistribuicao() {
        render(template: "distribuicao/index")
    }

    /**
     * Selecionar ficha para pelo nome cientifico e alimentar o campo select
     * @return
     */
    def select2Ficha() {
        Map data = ['items': []]

        if ( ! params?.sqCicloAvaliacao ) {
            render ''
            return
        }

        if ( ! params?.q ) {
            render ''
            return
        }

        if( ! session.sicae.user.sqUnidadeOrg ){
            render ''
            return
        }
        List where = []
        Long sqUnidadeOrg = session.sicae.user.sqUnidadeOrg

        if( ! session.sicae.user.isADM() ) {
            where.push('ficha.sq_unidade_org = '+sqUnidadeOrg.toString())
        }
        String q = '%'+params.q.trim() + '%'
        String cmdSql = """select ficha.sq_ficha, ficha.nm_cientifico
            from salve.ficha
            where ficha.sq_ciclo_avaliacao = :sqCicloAvaliacao
            and ficha.nm_cientifico ilike :q
            ${ where ? ' and ' + where.join('\nand ') :'' }
            order by ficha.nm_cientifico"""
        sqlService.execSql( cmdSql,
            [sqCicloAvaliacao:params.sqCicloAvaliacao.toLong(),q:q]).each {
            data.items.push(['id': it.sq_ficha, 'descricao': it.nm_cientifico])
        }
       render data as JSON
    }

    def select2FichaCicloAnterior() {
        Map data = ['items': []]
        if ( ! params?.sqCicloAvaliacao ) {
            render ''
            return
        }
        // ler o ciclo anterior
        List cicloAnterior = CicloAvaliacao.createCriteria().list {
            lt('id',params.sqCicloAvaliacao.toLong())
            order('id','desc')
            maxResults(1)
        }
        String q = params.q.trim() + '%'
        Ficha.executeQuery("""select new map(ficha.id as sqFicha, ficha.nmCientifico as nmCientifico)
            from Ficha as ficha
            where ficha.cicloAvaliacao.id = :sqCicloAvaliacao and ficha.nmCientifico like :q
            order by ficha.nmCientifico""",
            [sqCicloAvaliacao: ( cicloAnterior ? cicloAnterior.id : 0 ),q:q]).each {
            data.items.push(['id': it.sqFicha, 'descricao': it.nmCientifico])
        }
       render data as JSON
    }

    def saveFichaCicloAnterior(){
        Map res=[type:'success',status:0,msg:'',error:'', sqFichaSinonimia:0 ]
        try {
            if( ! params.sqFicha ) {
                throw  new Exception('Ficha não informada')
            }
            if( ! params.sqFichaCicloAnterior ) {
                throw  new Exception('Ficha ciclo anterior não informada')
            }
            Ficha ficha = Ficha.get(params.sqFicha.toLong() )
            if( ! ficha ) {
                throw new Exception('Ficha inválida')
            }
            Ficha fichaCicloAnterior = Ficha.get(params.sqFichaCicloAnterior.toLong() )
            if( ! fichaCicloAnterior ) {
                throw new Exception('Ficha ciclo anterior inválida')
            }
            ficha.fichaCicloAnterior = fichaCicloAnterior
            ficha.save()
            String noAntigo  = fichaCicloAnterior.taxon.noCientifico
            String noAutor   = fichaCicloAnterior.taxon.noAutor//.replaceAll(/[0-9,]/,'')
            if( noAutor ) {
                Integer nuAno = fichaCicloAnterior.taxon.nuAno
                if( nuAno && noAutor.indexOf(nuAno.toString() ) == -1) {
                    noAutor = noAutor+', '+nuAno.toString()
                }
                // gravar na tabela sinonimia
                if (!FichaSinonimia.findByFichaAndNoSinonimia(ficha, noAntigo)) {
                    FichaSinonimia fichaSinonimia = new FichaSinonimia()
                    fichaSinonimia.ficha = ficha
                    fichaSinonimia.noSinonimia = noAntigo
                    fichaSinonimia.noAutor = noAutor
                    fichaSinonimia.nuAno = nuAno ?: null
                    fichaSinonimia.validate()
                    fichaSinonimia.save(flush:true)
                    res.sqFichaSinonimia = fichaSinonimia.id
                }
            }
            res.msg = 'Dados gravados com SUCESSO!'
        } catch(Exception e ) {
            res.status=1
            res.msg = e.getMessage()
            res.type='error'
        }
        render res as JSON
    }

    def select2Subgrupo() {
        Map data = ['items': []]
        if ( ! params?.sqGrupo ) {
            render data as JSON
            return
        }
        String q = '%' + params.q.trim() + '%'
        DadosApoio grupo = DadosApoio.get( params.sqGrupo.toInteger() )
        if( grupo ) {
            List lista = DadosApoio.findAllByDescricaoIlikeAndPai(q, grupo)
            if (lista) {
                lista.each {
                    data.items.push(['id': it.id, 'descricao': it.descricao])
                }
            }
        }
        render data as JSON
    }

    def select2RefBib() {
        Map data = ['items': []]
        params.contexto = params.contexto ?: ''
        params.refBibSearchFor = params.refBibSearchFor ?: ''
        params.q = params.q ? params.q.trim() : ''
        if( ! params.q )
        {
            render 'sem parâmetro para pesquisar'
            return
        }
        params.inListaOficialVigente = ( params.inListaOficialVigente == null ? null : params.inListaOficialVigente.toBoolean() )
        //List lista = publicacaoService.search(params.q, params.contexto, params.inListaOficialVigente, params.refBibSearchFor );
        // List lista = publicacaoService.search( params.q,params.contexto, params.inListaOficialVigente, params.refBibSearchFor )
        List lista = publicacaoService.searchAcento( params.q,params.contexto, params.inListaOficialVigente, params.refBibSearchFor )

        if (lista) {
            lista.each {
                data.items.push(['id'       : it.id,
                                 'titulo'   : it.deTitulo,
                                 'autor'    : it?.noAutor ?:'',
                                 'ano'      : it?.nuAnoPublicacao?:'',
                                 'citacao'  : it.referenciaHtml,
                                 'descricao': it.tituloAutorAno
                ])

            }
        }
        render data as JSON
    }

    def saveFrmRefBib() {
        if (!params.sqFichaRefBib) {
            if (!params.sqFicha) {
                return this._returnAjax(1, "ID da ficha não informado", "error")
            }
            if (!params.noAutorRef && !params.nuAnoRef) {
                params.noAutorRef = null
                params.nuAnoRef = null
                if (!params.sqPublicacao) {
                    return this._returnAjax(1, "Referência Bibliográfica não selecionada!", "error")
                }
            } else {
                params.sqPublicacao = null
                if (!params.noAutorRef) {
                    return this._returnAjax(1, "Necessário informar o nome do autor!", "error")
                }
                if (!params.nuAnoRef) {
                    return this._returnAjax(1, "Necessário informar o ano!", "error")
                }
                params.nuAno = params.nuAnoRef
                params.noAutor = params.noAutorRef
            }
            if (!params.noTabela) {
                return this._returnAjax(1, "Necessário informar o nome da tabela!", "error")
            }
            if (!params.noColuna) {
                return this._returnAjax(1, "Necessário informar o nome da coluna da tabela!", "error")
            }
            if (!params.sqRegistro) {
                return this._returnAjax(1, "Necessário informar o id do registro da tabela!", "error")
            }
            if (!params.deRotulo) {
                return this._returnAjax(1, "Necessário informar a descrição do rótulo!", "error")
            }
        }

        // alerar o nome do parametro sqRegistro para sqRegistros para poder utilzar reg.properties = params
        //println params.sqRegistro.split(',').toList()
        params.sqRegistros = params.sqRegistro.split(',').toList()
        params.remove( 'sqRegistro')

        //println params
        //return this._returnAjax(1, "Verificar parametros!", "error")
        FichaRefBib reg = null
        Publicacao publicacao = null
        Ficha ficha = Ficha.get(params.sqFicha)
        if( params.sqPublicacao ) {
            publicacao = Publicacao.get( params.sqPublicacao.toLong() )
        }
        try {
            params.sqRegistros.each { sqRegistro ->
                if ( params.sqFichaRefBib ) {
                    reg = FichaRefBib.get(params.sqFichaRefBib)
                    // excluir as tags
                    FichaRefBibTag.findAllByFichaRefBib(reg).each { it.delete(flush: true, failOnError: true) }
                } else {
                    reg = FichaRefBib.findByFichaAndPublicacaoAndNoTabelaAndSqRegistroAndNoColuna(ficha,publicacao,params.noTabela, sqRegistro.toLong(),params.noColuna )
                    if( !reg ) {
                        reg = new FichaRefBib()
                        reg.ficha = ficha
                    }
                }
                reg.properties = params
                if ( params.sqPublicacao ) {
                    reg.publicacao = publicacao
                }
                reg.sqRegistro = sqRegistro.toLong()
                reg.save(flush: true)

                // adicionar as tags
                if ( params['deTagsIds[]'] ) {
                    DadosApoio pai = DadosApoio.findByCodigoSistema('TB_TAG')
                    List tags = []
                    if (params['deTagsIds[]'] instanceof String) {
                        tags.push(params['deTagsIds[]'])
                    } else {
                        tags = params['deTagsIds[]']
                    }
                    tags.each { tagId ->
                        //it = 'Teste Tag String';
                        if ( tagId.isInteger() ) {
                            new FichaRefBibTag(fichaRefBib: reg, tag: DadosApoio.get( tagId )).save(flush: true)
                        } else {
                            //String codigo = utilityService.removeAccents( tagId.toUpperCase().replaceAll(/ /,'_') );
                            // DadosApoio tag = new DadosApoio(descricao:tagId.toString(),codigo:codigo,pai: pai ).save(flush:true);
                            DadosApoio tag = new DadosApoio(descricao: tagId.toString(), pai: pai).save(flush: true)
                            new FichaRefBibTag(fichaRefBib: reg, tag: tag,).save(flush: true)
                        }
                    }
                }
            }
            // atualizar data alteracao e usuario alteracao nos registros da tabela ficha_ocorrencia quando mexer na ref bib do registro
            if( params.noTabela=='ficha_ocorrencia' && params.noColuna == 'sq_ficha_ocorrencia') {
                String dataAlteracao = new Date().format('yyyy-MM-dd HH:mm:ss')
                String sqPessoaAlteracao = session.sicae.user.sqPessoa.toString()
                sqlService.execSql("""update salve.ficha_ocorrencia
                    set dt_alteracao = '${dataAlteracao}',
                    sq_pessoa_alteracao = ${sqPessoaAlteracao}
                    where sq_ficha_ocorrencia in (${ params.sqRegistros.join(',') })""")
            }
            //fichaService.atualizarPercentualPreenchimento(reg.ficha)
            fichaService.updatePercentual( ficha )
            return this._returnAjax(0, "Referência gravada com SUCESSO!", "success")
        }
        catch (Exception e) {
            if (e.getMessage().toString().indexOf('unique') > -1) {
                return this._returnAjax(1, "Referência já cadastrada para este contexto!", "error")
            }
            println e.getMessage()
            return this._returnAjax(1, "Não foi possivel gravar a referência!", "error")
        }
        // atualizar data alteração e usuario
        ficha.dtAlteracao = new Date()
        ficha.alteradoPor = PessoaFisica.get(session.sicae.user.sqPessoa)
        ficha.save(flush:true)
    }

    def deleteFichaPublicacao() {
        if ( ! params.sqPublicacao) {
            return this._returnAjax(1, "Id não informado", "error")
        }
        if ( ! params.sqFicha) {
            return this._returnAjax(1, "Id ficha não informado", "error")
        }
        try {
            Ficha ficha = Ficha.get(params.sqFicha.toLong())
            if ( ! ficha) {
                return this._returnAjax(1, "Id ficha ficha inválido", "error")
            }
            Publicacao publicacao = Publicacao.get( params.sqPublicacao.toLong() )
            if (!publicacao) {
                return this._returnAjax(1, "id referência inválido", "error")
            }
            boolean canDelete = session.sicae.user.isADM() || session.sicae.user.isPF()
            if ( ! canDelete ) {
                return this._returnAjax(1, "Perfil não autorizado a fazer esta exclusão",'error')
            }
            FichaRefBib.findAllByFichaAndPublicacao(ficha, publicacao).each { row ->
                // Nao excluir as referencias das ocorrencias (pontos no mapa) e do histórico de avaliações aba 7.1
                if( ! ( row.noColuna =~ /sq_ficha_ocorrencia|sq_ficha_ocorrencia_portalbio|sq_taxon_historico_avaliacao/) ) {
                    FichaRefBibTag.findAllByFichaRefBib(row).each { it.delete() }
                    row.delete(flush: true)
                }
            }
            fichaService.updatePercentual(ficha.id.toLong())
            return this._returnAjax(0, "Referência Excluída com SUCESSO", "success")
        }
        catch (Exception e) {
            println e.getMessage()
            return this._returnAjax(1, "Erro ao excluir a Referência Bibliográfica", "error")
        }
    }

    def deleteFichaRefBib() {
        if ( ! params.sqFichaRefBib ) {
            return this._returnAjax(1, "Id não informado", "error")
        }
        try {
            FichaRefBib reg = FichaRefBib.get( params.sqFichaRefBib.toLong() )
            if (reg) {
                FichaRefBibTag.findAllByFichaRefBib(reg).each { it.delete() }
                reg.delete(flush: true)
                fichaService.updatePercentual(reg.ficha.id.toLong())
                return this._returnAjax(0, "Referência Excluída com SUCESSO", "success")
            }
            else
            {
                return this._returnAjax(1, "id inválido", "error")
            }
        }
        catch (Exception e) {
            println e.getMessage()
            return this._returnAjax(1, "Erro ao excluir a Referência Bibliográfica", "error")
        }
    }

    /**
     * consultar onde uma publicação está sendo referenciada na ficha
     * @return
     */
    def getReferenciaUtilizacao() {
        Map res = [status: 0, msg: '', data: [rotulos: []]]
        if ( ! params.sqPublicacao ) {
            render res as JSON
            return;
        }
        try {
            FichaRefBib reg = FichaRefBib.get( params.sqPublicacao.toLong() )
            if (reg) {
                sqlService.execSql("""select distinct de_rotulo, no_coluna from salve.ficha_ref_bib
                where sq_ficha = :sqFicha
                and sq_publicacao = :sqPublicacao
                and no_coluna <> 'ficha'
                order by de_rotulo
                """, [sqFicha: reg.ficha.id, sqPublicacao: reg.publicacao.id]).each { row ->
                        res.data.rotulos.push( Util.capitalize( row.de_rotulo ) )
                    }
            }
        } catch( Exception e ){
            res.status=1
            res.error = e.getMessage()
        }
       render res as JSON
    }

    def editRefBib() {
        if (!params?.id) {
            return [:] as JSON
        }

        FichaRefBib reg = FichaRefBib.get(params.id)
        if (reg?.id) {
            render reg.asJson()
            return
        }
        render [:] as JSON
    }

    def getGridImportacaoFichas() {
        if (! params.sqCicloAvaliacao ) {
            render 'Falta parametros ciclo avaliacao'
            return
        }
        if (! params.contexto ) {
            render 'Falta contexto da importação!'
            return
        }
        Unidade unidade = Unidade.get( session.sicae.user.sqUnidadeOrg)
        if( !unidade )
        {
            render 'Unidade Organizaciona não encontrada! ('+session.sicae.user.sqUnidadeOrg +')'
            return

        }

        List listPlanilhas = Planilha.findAllByUnidadeAndCicloAvaliacaoAndNoContexto(unidade, CicloAvaliacao.get( params.sqCicloAvaliacao), params.contexto, [sort:'dtEnvio',order:'desc'] )

        render(template: 'divGridImportacaoFicha', model: ['listPlanilhas': listPlanilhas ])
    }

    def getGridRefBib() {
        if (!params.sqFicha || !params.noTabela || !params.noColuna || !params.sqRegistro) {
            /*render 'Falta parametros para consulta!<br>'+
            'SqFicha: '+params?.sqFicha+'<br>NoTabela: '+params?.noTabela?.toLowerCase()+'<br>NoColuna: '+params?.noColuna?.toLowerCase()+'<br>SqRegistro:'+params?.sqRegistro;
            */
            render ''
            return
        }
        List listFichaRefBib
        listFichaRefBib = FichaRefBib.findAllByFichaAndNoTabelaAndNoColunaAndSqRegistro(Ficha.get(params.sqFicha), params.noTabela.toLowerCase(), params.noColuna.toLowerCase(), params.sqRegistro)
        listFichaRefBib.sort {
            it?.publicacao ? it.publicacao?.noAutor: it?.noAutor
        }


        render(template: 'divGridRefBib', model: ['listFichaRefBib': listFichaRefBib])
    }

    def getGridAllRefBibOld() {
        if (!params.sqFicha) {
            render ''
            return
        }


        // ler as referencias dos registros utilizados na avaliacao
        List refsValidas = sqlService.execSql("""select a.sq_ficha_ref_bib
                           from salve.ficha_ref_bib a
                    inner join salve.ficha_ocorrencia b on b.sq_ficha_ocorrencia = a.sq_registro
                    inner join taxonomia.publicacao p on p.sq_publicacao = a.sq_publicacao
                           where a.sq_ficha = ${params.sqFicha.toString()}
                                 and a.no_tabela = 'ficha_ocorrencia'
                                 and a.no_coluna = 'sq_ficha_ocorrencia'
                    and ( b.in_utilizado_avaliacao = 'S' or b.in_utilizado_avaliacao is null )

                    UNION ALL

                    select a.sq_ficha_ref_bib
                           from salve.ficha_ref_bib a
                    inner join salve.ficha_ocorrencia_portalbio b on b.sq_ficha_ocorrencia_portalbio = a.sq_registro
                    inner join taxonomia.publicacao p on p.sq_publicacao = a.sq_publicacao
                           where a.sq_ficha = ${params.sqFicha.toString()}
                                 and a.no_tabela = 'ficha_ocorrencia_portalbio'
                                 and a.no_coluna = 'sq_ficha_ocorrencia_portalbio'
                        and b.in_utilizado_avaliacao = 'S'
                """)

        Ficha ficha = Ficha.get(params.sqFicha.toLong())
        /*
        //DadosApoio situacaoExcluida = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA','EXCLUIDA')
        List listFichaRefBib = []
        List listFichaRefBibPortalbio = []
        FichaRefBib.createCriteria().list() {
            createAlias('ficha', 'fi')
            createAlias('fi.situacaoFicha', 'situacao')
            createAlias('fi.taxon', 'tx')
            ne('situacao.codigoSistema', 'EXCLUIDA')
            and {
                or {
                    eq('ficha', ficha)
                    eq('tx.pai', ficha.taxon)
                }
                ne('noTabela', 'ficha_ocorrencia')
                ne('noTabela', 'ficha_ocorrencia_portalbio')
            }
            isNotNull('publicacao') // não incluir a comunicação pessoal
        }.sort { Util.removeAccents( it.publicacao?.noAutor) }.each {

             // Quando houver a mesma referência na ficha e no registro de ocorrencia exibir somente a da ficha
             //Discutido em reunião dia : 24/06/2021 devido reclamação Cadu email 23/06/2021

            // prencher a referencia bibliográfica se ainda não estiver preenchida
            if( ! it.publicacao.deRefBibliografica){
                it.publicacao.deRefBibliografica = it.referenciaHtml
                it.publicacao.save(flush:true);
            }
            //String refBib = it.publicacao.deRefBibliografica ? it.publicacao.deRefBibliografica : it.publicacao.deTitulo
            if ( ( it.noTabela =~ /^ficha_ocorrencia/ )  ) {
                if( ! listFichaRefBibPortalbio.find{ row -> row.publicacao.id == it.publicacao.id} )  {
                    listFichaRefBibPortalbio.push(it)
                    //if( listFichaRefBib.find{ row->row.publicacao.id== it.publicacao.id } ) {
                    //    listFichaRefBib = listFichaRefBib.findAll{ row->row.publicacao.id != it.publicacao.id }
                    //}
                }
            } else {
                //if( ! listFichaRefBib.find{ row -> row.publicacao.id == it.publicacao.id } && ! listFichaRefBibPortalbio.find{row-> row.publicacao.id == it.publicacao.id} ) {
                if( ! listFichaRefBib.find{ row -> row.publicacao.id == it.publicacao.id } ) {
                    listFichaRefBib.push(it)
                }
            }
            // se a referencia estiver duplicada na ficha e nos registros, exibir somente a da ficha
            listFichaRefBibPortalbio = listFichaRefBibPortalbio.findAll{ it1 ->
                return  ! listFichaRefBib.find{ it2 -> it2.publicacao.id == it1.publicacao.id }
            }
        }*/
        if( refsValidas ) {
            // fim ref bib ficha
            // referencias das ocorrencias
            FichaRefBib.createCriteria().list() {
                createAlias('ficha', 'fi')
                createAlias('fi.situacaoFicha', 'situacao')
                createAlias('fi.taxon', 'tx')
                ne('situacao.codigoSistema', 'EXCLUIDA')
                and {
                    or {
                        eq('ficha', ficha)
                        eq('tx.pai', ficha.taxon)
                    }
                }
                isNotNull('publicacao') // não incluir a comunicação pessoal
                'in'('id', refsValidas?.sq_ficha_ref_bib?.collect { it.toLong() })
            }.sort { Util.removeAccents(it.publicacao?.noAutor) }.each {
                /**
                 * Quando houver a mesma referência na ficha e no registro de ocorrencia exibir somente a da ficha
                 * Discutido em reunião dia : 24/06/2021 devido reclamação Cadu email 23/06/2021
                 */
                // prencher a referencia bibliográfica se ainda não estiver preenchida
                if (!it.publicacao.deRefBibliografica) {
                    it.publicacao.deRefBibliografica = it.referenciaHtml
                    it.publicacao.save(flush: true);
                }
                //String refBib = it.publicacao.deRefBibliografica ? it.publicacao.deRefBibliografica : it.publicacao.deTitulo
                if ((it.noTabela =~ /^ficha_ocorrencia/)) {
                    if (!listFichaRefBibPortalbio.find { row -> row.publicacao.id == it.publicacao.id }) {
                        listFichaRefBibPortalbio.push(it)
                        /*if( listFichaRefBib.find{ row->row.publicacao.id== it.publicacao.id } ) {
                        listFichaRefBib = listFichaRefBib.findAll{ row->row.publicacao.id != it.publicacao.id }
                    }*/
                    }
                } else {
                    //if( ! listFichaRefBib.find{ row -> row.publicacao.id == it.publicacao.id } && ! listFichaRefBibPortalbio.find{row-> row.publicacao.id == it.publicacao.id} ) {
                    if (!listFichaRefBib.find { row -> row.publicacao.id == it.publicacao.id }) {
                        listFichaRefBib.push(it)
                    }
                }
                // se a referencia estiver duplicada na ficha e nos registros, exibir somente a da ficha
                listFichaRefBibPortalbio = listFichaRefBibPortalbio.findAll { it1 ->
                    return !listFichaRefBib.find { it2 -> it2.publicacao.id == it1.publicacao.id }
                }
            }
        }
        // canDelete
        // fim ref bibs ocorrencias
        // se o taxon não for o mesmo é porque a ref bib é de uma subespecie
        render(template: 'divGridAllRefBib', model: ['listFichaRefBib': listFichaRefBib,
                                                     'listFichaRefBibPortalbio': listFichaRefBibPortalbio,
                                                      taxon:ficha.taxon])
    }
    def getGridAllRefBibOld2() {
        if ( ! params.sqFicha ) {
            render ''
            return
        }
        // criar as listas de referencias da ficha e dos registros
        List listFichaRefBib = []
        List listFichaRefBibPortalbio=[]
        fichaService.getFichaRefBibs(params.sqFicha.toLong()).each{row ->
            JSONObject jsonAnexos=null
            row.anexos=[]
            row.tags=''
            if( row.json_anexos ) {
                jsonAnexos = JSON.parse( row.json_anexos )
                jsonAnexos.keys().findAll {
                    row.anexos.push([noAnexo:jsonAnexos[it].no_arquivo,sqAnexo:jsonAnexos[it].sq_publicacao_anexo])
                }
            }

            if( row.no_tags ){
                row.tags = row.no_tags.replaceAll(/,/, '<br>')
            }
            if( row.no_origem == 'ficha'){
                listFichaRefBib.push( row )
            } else {
                listFichaRefBibPortalbio.push( row )
            }
        }
        render(template: 'divGridAllRefBib', model: ['listFichaRefBib': listFichaRefBib,
                                                     'listFichaRefBibPortalbio': listFichaRefBibPortalbio])
    }


    def getGridAllRefBib() {
        if ( ! params.sqFicha ) {
            render ''
            return
        }
        // criar as listas de referencias da ficha e dos registros
        List listFichaRefBib = []
        List listFichaRefBibPortalbio=[]
        VwFicha ficha = VwFicha.get( params.sqFicha.toLong() )
        fichaService.getFichaRefBibs(params.sqFicha.toLong()).each{row ->
            JSONObject jsonAnexos=null
            row.anexos=[]
            row.tags =''
            if( row.json_anexos ) {
                jsonAnexos = JSON.parse( row.json_anexos )
                jsonAnexos.keys().findAll {
                    row.anexos.push([noAnexo:jsonAnexos[it].no_arquivo,sqAnexo:jsonAnexos[it].sq_publicacao_anexo])
                }
            }
            if( row.no_tags ){
                row.tags = row.no_tags.split(',').findAll{it.size()>1}.join('<br>')
            }

            // evitar duplicidade fazer tratamento dos dados antes de inserir na lista
            Map rowData = [sq_publicacao:row.sq_publicacao
                ,de_ref_bibliografica:row.de_ref_bibliografica
                ,can_delete : ( row.no_tabela == 'ficha' && row.no_coluna == 'ficha' )
                ,tags               : row.tags
                ,anexos             :row.anexos
                ,co_nivel_taxonomico:row?.co_nivel_taxonomico
            ]
            // PONTOS FOCAIS, ADMS pode excluir qualquer referência na aba 9
            if( session.sicae.user.isADM() || session.sicae.user.isPF() || session.sicae.user.isCI()) {
                rowData.can_delete = true
            }

            // não permitir excluir referencia da subespecie no gride da espécie
            if( row?.co_nivel_taxonomico?.toString()?.toLowerCase() != ficha.coNivelTaxonomico.toString().toLowerCase() ){
                rowData.can_delete=false
            }

            // se existir referencias duplicadas, habilitar o botao de excluir se uma delas pertencer a
            // propria aba 9 (no_tabela=ficha e no_coluna=ficha)
            if( row.no_origem == 'ficha'){

                if( ! listFichaRefBib.contains(rowData)) {
                    Map itemTemp = listFichaRefBib.find {
                        it.sq_publicacao.toLong() == rowData.sq_publicacao.toLong()
                    }
                    if ( itemTemp) {
                        // se as descrições forem diferentes, vale a mais completa
                        if( rowData.de_ref_bibliografica.size() >itemTemp.de_ref_bibliografica.size() ){
                            itemTemp.de_ref_bibliografica = rowData.de_ref_bibliografica
                        }
                        // acumular as tags
                        if (rowData.tags && itemTemp.tags.indexOf(rowData.tags) == -1) {
                           itemTemp.tags += (itemTemp.tags ? '<br>' : '') + rowData.tags
                        }
                        if ( rowData.can_delete ) {
                             itemTemp.can_delete = true
                         }
                    } else {
                        listFichaRefBib.push(rowData)
                    }
                }
            } else {
                if (!listFichaRefBibPortalbio.contains(rowData)) {
                    listFichaRefBibPortalbio.push(rowData)
                }
            }
        }

        render(template: 'divGridAllRefBib', model: ['listFichaRefBib': listFichaRefBib,
                            isCI : session.sicae.user.isCI(),
                            'listFichaRefBibPortalbio': listFichaRefBibPortalbio])
    }


    def select2Tags() {
        Map data = ['items': []]

        if (!params?.q?.trim()) {
            render ''
            return
        }

        String q = '%' + params.q.trim() + '%'

        List lista = DadosApoio.findAllByDescricaoIlikeAndPai(q, DadosApoio.findByCodigoSistema('TB_TAG'))
        if (lista) {
            lista.each {
                data.items.push(['id': it.id, 'descricao': it.descricao])
            }
            render data as JSON
            return
        }
        render ''
    }

    def select2Municipio() {
        List lista
        Map data = ['items': []]
        if (!params?.q?.trim()) {
            render ''
            return
        }
        String q = params.q.trim() + '%'
        if (params.sqEstado) {
            lista = Municipio.findAllByNoMunicipioIlikeAndEstado(q, Estado.get(params.sqEstado.toLong() ) )
        } else {
            lista = Municipio.findAllByNoMunicipioIlike(q)
        }
        if (lista) {
            lista.each {
                data.items.push(['id'       : it.id,
                                 'descricao': it.noMunicipio
                ])
            }
            render data as JSON
            return
        }
        render ''
    }

    def select2PessoaFisica() {
        // TODO - Implementar paginação igual select2taxon()
        List lista
        Map data = ['items': []]
        if (!params?.q?.trim()) {
            render 'sem parametros'
            return
        }
        boolean onlyEmployees = params?.onlyEmployees == true ? true : false
        boolean isCpf = false
        String q = params.q.toString().trim().toLowerCase()

        // verificar se passou o cpf
        if( q.replaceAll(/[^0-9]/,'') =~ /^[0-9]{11}/ ){
            q=q.replaceAll(/[^0-9]/,'')
            isCpf=true
        } else {
            // eliminar: de da(s) do(s)
            q = q.replaceAll(/ d[a-u]s? /,' ')
        }
        List whereNomes = []
        List nomesComAcentoPesquisarPessoa = []
        List nomesSemAcentoPesquisarPessoa = []
        /* REGRA REMOVIDA PARA SALVE-ESTADUAL
        List nomesComAcentoPesquisarExterno = []
        List nomesSemAcentoPesquisarExterno = []
        */
        Map pars = [:]
        if( ! isCpf ) {
            q.split(' ').eachWithIndex { it, index ->
                if( it.toString().trim() != '' ) {
                    String noPessoa = 'noPessoa' + index.toString()
                    String noPessoaSemAcento = 'noPessoaSemAcento' + index.toString()
                    String nomeComAcento = "pf.no_pessoa ilike :${noPessoa}"
                    String nomeSemAcento = "pf.no_pessoa ilike :${noPessoaSemAcento}"
                    /* REGRA REMOVIDA PARA SALVE-ESTADUAL
                    if( ! onlyEmployees ) {
                        String nomeComAcentoExterno = "ue.no_usuario_externo ilike :${noPessoa}"
                        String nomeSemAcentoExterno = "ue.no_usuario_externo ilike :${noPessoaSemAcento}"
                        nomesComAcentoPesquisarExterno.push(nomeComAcentoExterno)
                        nomesSemAcentoPesquisarExterno.push(nomeSemAcentoExterno)
                    }*/
                    nomesComAcentoPesquisarPessoa.push(nomeComAcento)
                    pars[noPessoa] = "%" + it + "%"
                    nomesSemAcentoPesquisarPessoa.push(nomeSemAcento)
                    pars[noPessoaSemAcento] = "%" + Util.removeAccents(it) + "%"
                }
            }
            if (nomesComAcentoPesquisarPessoa) {
                whereNomes.push('(' + nomesComAcentoPesquisarPessoa.join(' and ') + ')')
            }
            /* REGRA REMOVIDA PARA SALVE-ESTADUAL
            if (nomesComAcentoPesquisarExterno) {
                whereNomes.push('(' + nomesComAcentoPesquisarExterno.join(' and ') + ')')
            }*/
            if (nomesSemAcentoPesquisarPessoa) {
                whereNomes.push('(' + nomesSemAcentoPesquisarPessoa.join(' and ') + ')')
            }
            /* REGRA REMOVIDA PARA SALVE-ESTADUAL
            if (nomesSemAcentoPesquisarExterno) {
                whereNomes.push('(' + nomesSemAcentoPesquisarExterno.join(' and ') + ')')
            }*/
        } else {
            whereNomes.push('pf.nu_cpf = :nuCpf')
            pars.nuCpf = q
        }
        //Map pars = [noPessoa:'%'+q+'%',noPessoaSemAcento:'%'+Util.removeAccents(q)+'%']
        String sql

        sql =
        """
        SELECT pf.sq_pessoa AS "id"
            , pf.no_pessoa  AS "noPessoa"
            , pf.nu_cpf     AS "nuCpfFormatado"
            , 'E'::text     AS "tipo",
            p.de_email      AS email
        FROM salve.vw_pessoa_fisica pf
        LEFT JOIN salve.pessoa p ON pf.sq_pessoa = p.sq_pessoa
        WHERE ${ whereNomes.join('\nor ') }
        """
        /* REGRA REMOVIDA PARA SALVE-ESTADUAL
        if( ! onlyEmployees ) {
            sql = """select pf.sq_pessoa as "id"
                    , pf.no_pessoa as "noPessoa"
                    , pf.nu_cpf as "nuCpfFormatado"
                    , 'E'::text as "tipo"
                    , coalesce( coalesce(email.tx_email, ue.tx_email ),'sem email') as "email"
                    from salve.vw_pessoa_fisica pf
                    left outer join salve.vw_usuario_externo ue on ue.sq_pessoa = pf.sq_pessoa
                    left join lateral (
                       select emailTemp.tx_email from (
                          select tx_email, 2 as relevancia from salve.vw_email email where email.tx_email is not null and email.sq_tipo_email = 2
                          and email.sq_pessoa = pf.sq_pessoa
                          union all
                          select tx_email, 1 as relevancia from salve.vw_email email where email.tx_email is not null and email.sq_tipo_email <> 2
                             and email.sq_pessoa = pf.sq_pessoa
                       ) as emailTemp
                       order by relevancia desc
                       limit 1
                    ) as email on true
                    where ${ whereNomes.join('\nor ') }
                """
        } else {
            sql = """
            select distinct tmp.sq_pessoa as "id"
            , tmp.no_pessoa as "noPessoa"
            , coalesce( tmp.tx_email, 'sem email' ) as "email"
            , tmp.nu_cpf as "nuCpfFormatado"
            , tmp.tipo from
            (
                select pf.sq_pessoa, pf.no_pessoa
                ,email.tx_email
                ,pf.nu_cpf, 'I'::text as tipo
                from salve.vw_pessoa_fisica pf
                inner join salve.vw_email email on email.sq_pessoa = pf.sq_pessoa and email.sq_tipo_email = 2
                where ${ whereNomes.join('\nor ') }
            ) tmp order by tmp.no_pessoa,tmp.tipo desc
            """
        }*/

        /** /
        println ' '
        println sql
        println pars
        /**/
        lista = sqlService.execSql( sql, pars, null, 10)
        if (lista) {
            lista.each { it ->
                if( !params.comEmail || (params.comEmail == 'S' && !(it?.email =~ /sem email/) ) ) {
                    data.items.push(['id'       : it.id,
                                     'descricao': Util.capitalize(it.noPessoa) + (it.nuCpfFormatado ? ' - ' + Util.formatCpf(it.nuCpfFormatado) : ''),
                                     'cpf'      : Util.formatCpf(it.nuCpfFormatado),
                                     'email'    : it?.email?.toLowerCase(),
                                     'txHint'   : it?.email?.toLowerCase() + ' (' + ((it?.tipo == 'I' || it?.email.indexOf('@icmbio') > 0) ? 'INTERNO' : 'EXTERNO') + ')'
                    ])
                }
            }
            render data as JSON
            return
        }

        render ''
    }

    def select2UsuarioInterno() {
        params.onlyEmployees = true
        return select2PessoaFisica()
    }

    /**
     * pesquisa taxon para campo select2
     * @return
     */
    def select2TaxonNovo() {
        return select2Taxon()
/*
        String searchText
        // estrutura de dados de retorno
        Map data = [page        : params?.page ? params?.page.toInteger() + 1 : 1,
                    totalRecords: params.totalRecords ? params.totalRecords.toInteger() : 0i,
                    items       : []]
        try {

            // definir valor padrão para a string de pesquisa
            searchText = params.q ?:''

            // limpar texto só letras e traço
            searchText = searchText.trim().replaceAll(/[^a-zA-Z-]/, ' ').replaceAll('/  /g', ' ')

            // tem pque informar o nivel de pesquisa; Reino, filo, classe...
            if ( ! params.sqNivel ) {
                throw new RuntimeException('Nível taxonômido não informado.')
            }

            // validar nivel taxonômico de pesquisa informado
            NivelTaxonomico nivel = NivelTaxonomico.get(params.sqNivel.toLong())
            if (!nivel) {
                throw new RuntimeException('Nível taxonômido inválido.')
            }

            // para os niveis Reino, Filo e Class não precisa palavra chave para pesquisar
            if ( searchText.length() < 3 ) {
                //  exigir mínimo de 3 caracteres para os níveis abaixo de família
                if ( nivel.nuGrauTaxonomico > grailsApplication.config.grau.familia ) {
                    throw new RuntimeException('Informe pelo menos 3 letras.')
                }
            }


            // comando sql para pesquisar a tabela taxonomia.taxon
            String sqlCmd = """
                select taxon.sq_taxon
                , (json_trilha->:noNivel->>'no_taxon')::TEXT AS no_taxon
                , json_trilha
                , nivel_taxonomico.co_nivel_taxonomico
                from taxonomia.taxon
                inner join taxonomia.nivel_taxonomico on nivel_taxonomico.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
                where taxon.sq_nivel_taxonomico = :sqNivel
                and json_trilha->:noNivel->>'no_taxon' ilike :q
                and taxon.st_ativo=true
                order by json_trilha->:noNivel->>'no_taxon'
                """
            // parametros para a execução da query
            Map qryParams = [q        : '%' + searchText + '%'
                             , sqNivel: nivel.id
                             , noNivel: nivel.coNivelTaxonomico.toLowerCase()]
            // ler os niveis taxonomicos para montar o array de retorno
            List niveis = NivelTaxonomico.list().sort{it.nuGrauTaxonomico}

            sqlService.execSql(sqlCmd, qryParams).each {
                try {

                    List breadCrumb = []
                    JSONObject jsonTrilha = JSON.parse(it.json_trilha.toString() )
                    Map mapTemp=[id                 : it.sq_taxon
                                 , descricao        : it.no_taxon
                                 , coNivelTaxonomico: it.co_nivel_taxonomico
                                 , nmCientifico      : it.no_taxon
                                 , txNomes : ''
                                ]
                    niveis.each {
                        mapTemp['id' + Util.capitalize(it.coNivelTaxonomico)] = jsonTrilha[it.coNivelTaxonomico.toLowerCase()] ? jsonTrilha[it.coNivelTaxonomico.toLowerCase()]?.sq_taxon : ''
                        mapTemp['nm' + Util.capitalize(it.coNivelTaxonomico)] = jsonTrilha[it.coNivelTaxonomico.toLowerCase()] ? jsonTrilha[it.coNivelTaxonomico.toLowerCase()]?.no_taxon : ''
                        if (jsonTrilha[it.coNivelTaxonomico.toLowerCase()]) {
                            if( it.nuGrauTaxonomico.toInteger() >= grailsApplication.config.grau.especie.toInteger() ) {
                                breadCrumb.push( Util.ncItalico( mapTemp['nm' + Util.capitalize(it.coNivelTaxonomico)],true,false ) )
                            }
                            else
                            {
                                breadCrumb.push(mapTemp['nm' + Util.capitalize(it.coNivelTaxonomico)])
                            }
                        }
                    }
                    mapTemp.txHint=breadCrumb.join(' > ')
                    data.items.push(mapTemp)
                } catch (Exception e) {
                    println e.getMessage()
                }
            }
        }
        catch (Exception e) {
            println e.getMessage()
        }
        render data as JSON
        */

    }

    def select2Taxon() {

        //return select2TaxonOld()
        boolean caseSensitive = params.w_diferenciar_letra ? true  : false
        String cmdSql
        String where = ''
        String wLike = params.w_like ?:'iniciado_por' //'contem_texto'
        String searchText
        String whereNivelSuperior = ''
        String whereAtivos = ''
        Integer pageSize    = params.pageSize ? params.pageSize.toInteger() : 100
        Integer page        = params.page ? params.page.toInteger() : 1
        Integer offset      = params.offset ? params.offset.toInteger() : 0
        Integer totalRecords = params.totalRecords ? params.totalRecords.toInteger() : 0
        String saltar       = 'limit '+pageSize+' offset ' + offset
        if( params?.ativos && params.ativos.toBoolean() == true ) {
            whereAtivos = ' and taxon.st_ativo=true'
        }

        String q = Util.clearCientificName(params.q)

        // estrutura de dados de retorno
        Map data = [page        : page,
                    totalRecords: totalRecords,
                    message     : '',
                    error       : '',
                    items       : []]

        // ajustar parametros para pesquisa ESPECIE E SUBESPECIE
        /*if( params.nivel == 'SUBESPECIE'){
            if( q.toString().findAll( { it -> it == " "}).size() == 1 )
            {
                params.nivel='ESPECIE'
            }
        } else */
        if( params.cdNivelPesquisar ){
            params.nivel = params.cdNivelPesquisar.toString().toUpperCase()
        }

        if( params.nivel =~ /ESPECIE/) {
            if (q.split(' ').size() == 1) {
                if (!params.wLike) {
                    wLike = 'contem_texto'
                }
            }
        }
        try {

            // ler a lista de níveis em ordem hierarquica
            List listNiveis = NivelTaxonomico.list().sort { it.nuGrauTaxonomico }
            if( !listNiveis ){
                throw new Exception('A tabela taxonomia.nivel_taxonomico está vazia')
            }

            // localizar o nivel de pesquisa
            NivelTaxonomico nivelInformado
            if( params.sqNivel ) {
                nivelInformado = listNiveis.find { it.id.toLong() == params.sqNivel.toLong() }
            }
            else
            {
                nivelInformado = listNiveis.find { it.coNivelTaxonomico == params.nivel.toUpperCase() }
            }

            // definir valor padrão para a string de pesquisa
            searchText = q ?: ''

            // limpar texto da procura, deixar somente letras e traços
            searchText = searchText.trim().replaceAll(/[^a-zA-Z-]/, ' ').replaceAll('/  /g', ' ')

            // para os niveis Reino, Filo e Class não precisa palavra chave para pesquisar
            if (searchText.length() < 3) {
                //  exigir mínimo de 3 caracteres para os níveis abaixo de família
                if (nivelInformado.nuGrauTaxonomico.toInteger() > grailsApplication.config.grau.familia.toInteger()) {
                    //throw new RuntimeException('Informe pelo menos 3 letras.')
                    throw new Exception('')
                }
            }

            // verificar se está selecionado o nivel superior
            listNiveis.each {
                String pName = 'w_id' + it.coNivelTaxonomico.toLowerCase().capitalize()
                if (params[pName]) {
                    whereNivelSuperior = """and json_trilha->'${it.coNivelTaxonomico.toLowerCase()}'->>'sq_taxon' = '${ params[pName] }'"""
                }
            }

            // construir condição de pesquisa de acordo com parâmetros recebido
            if (searchText) {
                where = """and json_trilha->'${nivelInformado.coNivelTaxonomico.toLowerCase()}'->>'no_taxon'"""
                if (wLike == 'iniciado_por') {
                    where += (caseSensitive ? " like '" : " ilike '") + searchText + "%'"
                }else if (wLike == 'contem_texto') {
                    where += (caseSensitive ? " like '" : " ilike '") + '%' + searchText + "%'"
                }else if (wLike == 'palavra_exata') {
                    where += ' ' + (caseSensitive ? "like " : "ilike ") + " any (array['${searchText} %', '% ${searchText}', '% ${searchText} %','${searchText}'] )"
                }
            }

            // se for a primeira pagina, calcular o total de registros para a paginação
            if( data.page == 1 ) {
                cmdSql = """select count( taxon.sq_taxon ) as nu_total
                from taxonomia.taxon
                inner join taxonomia.nivel_taxonomico on nivel_taxonomico.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
                where taxon.sq_nivel_taxonomico = :sqNivel
                $where
                $whereAtivos
                $whereNivelSuperior
                """
                sqlService.execSql(cmdSql, [noNivel: nivelInformado.coNivelTaxonomico.toLowerCase(), sqNivel: nivelInformado.id],null,60).each {
                    data.totalRecords = it.nu_total
                }
                //println ' '
                //println 'TOTAL CALCULADO ' + data.totalRecords
            }
            // comando sql para pesquisar a tabela taxonomia.taxon
            cmdSql = """
                select taxon.sq_taxon
                , (json_trilha->:noNivel->>'no_taxon')::TEXT AS no_taxon
                , json_trilha
                , nivel_taxonomico.co_nivel_taxonomico
                from taxonomia.taxon
                inner join taxonomia.nivel_taxonomico on nivel_taxonomico.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
                where taxon.sq_nivel_taxonomico = :sqNivel
                $where
                $whereAtivos
                $whereNivelSuperior
                order by json_trilha->:noNivel->>'no_taxon'
                $saltar
                """

            Map sqlParams = [noNivel: nivelInformado.coNivelTaxonomico.toLowerCase(), sqNivel: nivelInformado.id]

            /** /
            println ' '
            println 'SALVE - fichaController->selectTaxonNovo() - ' + new Date().format('dd/MM/yyyy HH:mm:ss')
            println cmdSql
            println sqlParams
            /**/

            sqlService.execSql(cmdSql, sqlParams,null,10).each {
                try {
                    List breadCrumb = []
                    JSONObject jsonTrilha = JSON.parse(it.json_trilha.toString())
                    Map mapTemp = [id                 : it.sq_taxon
                                   , descricao        : it.no_taxon
                                   , coNivelTaxonomico: it.co_nivel_taxonomico
                                   , nmCientifico     : it.no_taxon
                                   , txNomes          : ''
                    ]
                    listNiveis.each {
                        mapTemp['id' + Util.capitalize(it.coNivelTaxonomico)] = jsonTrilha[it.coNivelTaxonomico.toLowerCase()] ? jsonTrilha[it.coNivelTaxonomico.toLowerCase()]?.sq_taxon : ''
                        mapTemp['nm' + Util.capitalize(it.coNivelTaxonomico)] = jsonTrilha[it.coNivelTaxonomico.toLowerCase()] ? jsonTrilha[it.coNivelTaxonomico.toLowerCase()]?.no_taxon : ''
                        if (jsonTrilha[it.coNivelTaxonomico.toLowerCase()]) {
                            if (it.nuGrauTaxonomico.toInteger() >= grailsApplication.config.grau.especie.toInteger()) {
                                breadCrumb.push(Util.ncItalico(mapTemp['nm' + Util.capitalize(it.coNivelTaxonomico)], true, false))
                            }else {
                                breadCrumb.push(mapTemp['nm' + Util.capitalize(it.coNivelTaxonomico)])
                            }
                        }
                    }
                    mapTemp.txHint = breadCrumb.join(' > ')
                    if (data.items.size() < pageSize) {
                        data.items.push(mapTemp)
                    }
                } catch (Exception e) {
                    println e.getMessage()
                }
            }
            render data as JSON
        } catch( Exception e ) {
            if( e.getMessage() ) {
                println e.getMessage()
                data.error = e.getMessage()
            }
            render data as JSON
        }
    } // count q

    /**
     * alimentar select da alteração do nivel taxonomico para outro do mesmo nivel
     */
    def select2TaxonAlteracao() {
        String cmdSql
        Map data = [message     : '',
                    error       : '',
                    items       : [] ]

        List niveis = NivelTaxonomico.list([order:'nuGrauTaxonomico'])
        try {
            params.q = params.q.replaceAll(/[^a-z A-Z]/,'').trim()
            if( !params.sqTaxonAntigo ) {
                throw new Exception('Taxon ANTIGO não informado.')
            }
            // ler o taxon pai do nivel sendo alterado
            cmdSql="select sq_taxon_pai from taxonomia.taxon where sq_taxon = :sqTaxonAntigo"
            sqlService.execSql(cmdSql,[sqTaxonAntigo:params.sqTaxonAntigo.toLong()]).each {
                // ler todos os taxons irmãos excluindo o atual
                cmdSql = """select sq_taxon, no_taxon, json_trilha::text from taxonomia.taxon
                where sq_taxon <> :sqTaxonAntigo
                and ${it.sq_taxon_pai ? ' sq_taxon_pai = ' + it.sq_taxon_pai.toString() : ' sq_taxon_pai is null'}
                and no_taxon ilike '%${params.q}%'
                and st_ativo = true
                order by no_taxon"""
                sqlService.execSql(cmdSql, [sqTaxonAntigo: params.sqTaxonAntigo.toLong()]).each { row ->
                    List hints = []
                    niveis.each { nivel ->
                        String codigo = nivel.coNivelTaxonomico.toLowerCase()
                        JSONObject jsonTrilha = JSON.parse( row.json_trilha )
                        if ( jsonTrilha[codigo] ) {
                            hints.push( jsonTrilha[codigo].no_taxon )
                        }
                    }
                    data.items.push( [id       : row.sq_taxon,
                                      descricao: row.no_taxon,
                                      txHint     :hints.join(' &gt; ') ])
                }
            }
        } catch(Exception e) {
            data.message=e.getMessage()
            data.error=e.getMessage()
        }
        render data as JSON
    }

    def select2Ucs() {
        List lista
        Map data = ['items': []]
        if (!params?.q?.trim()) {
            render ''
            return
        }
        String q = '%' + Util.removeAccents(params.q.trim())+ '%'
        //lista = Uc.findAllByNoUcIlikeOrSgUnidadeIlike(q,q)
        sqlService.execSql("""select uc.sq_controle as id, uc.sq_uc as "sqUc" ,uc.in_esfera as "inEsfera"
            ,case when uc.sg_unidade is null or uc.sg_unidade = '' then uc.no_uc else uc.sg_unidade end as sigla
            from salve.vw_uc uc
            where public.fn_remove_acentuacao(concat(uc.no_uc,' ', uc.sg_unidade)) ilike :q
            """,[q:q] ).each {
                data.items.push(['id'        : it.id   // sq_controle
                                 ,'sqUc'     : it.sqUc
                                 ,'esfera'   : it.inEsfera
                                 ,'descricao': (Util.capitalize( it.sigla ) )
                ])
            }
           render data as JSON
    }

    def select2UcsEsfera() {
        List lista
        Map data = ['items': []]
        if (!params?.q?.trim()) {
            render ''
            return
        }
        String q = '%' + Util.removeAccents(params.q.trim())+ '%'
        //lista = Uc.findAllByNoUcIlikeOrSgUnidadeIlike(q,q)
        sqlService.execSql("""select uc.sq_controle as id, uc.sq_uc as "sqUc" ,uc.in_esfera as "inEsfera"
            ,case when uc.sg_unidade is null or uc.sg_unidade = '' then uc.no_uc else uc.sg_unidade end as sigla
            from salve.vw_uc uc
            where public.fn_remove_acentuacao(concat(uc.no_uc,' ', uc.sg_unidade)) ilike :q
            """,[q:q] ).each {
                data.items.push(['id'        : it.inEsfera+it.sqUc
                                 ,'idControle': it.id
                                 ,'esfera'   : it.inEsfera
                                 ,'descricao': (Util.capitalize( it.sigla ) )
                ])
            }
           render data as JSON
    }

    def select2InstituicaoFiltro() {

        Map data = ['items': []]
        if( ! params.sqCicloAvaliacao )
        {
            render ''
            return
        }
        List unidadesValidas = []
        if( params.sqOficinaValidacao )
        {
            unidadesValidas = OficinaFicha.executeQuery("select distinct ficha.unidade.id from OficinaFicha a join a.ficha ficha where a.oficina.id="+params.sqOficinaValidacao.toString())
        }

        List lista = VwFicha.createCriteria().list() {
            eq('sqCicloAvaliacao', params.sqCicloAvaliacao.toInteger() )
            if( params.q )
            {
                ilike('sgUnidadeOrg', '%' + params.q.trim() + '%')
            }
            if( unidadesValidas )
            {
                'in'('sqUnidadeOrg', unidadesValidas.collect{ it.toInteger() } )
            }
          }.unique { it.sqUnidadeOrg }.sort { it.sgUnidadeOrg }

        if ( lista ) {
            lista.each {
                data.items.push(['id': it.sqUnidadeOrg, 'descricao': it.sgUnidadeOrg])
            }
            render data as JSON
            return
        }
        render ''
    }

    /**
     * método para recuperar as abrangências da tabela abrangencia de acordo com o tipo informado
     * @return [description]
     */
    def getAbrangencias()
    {
        Map data = ['items': []]
        Map map
        String codigo
        String ident = ''
        if( !params.sqTipoAbrangencia )
        {
            render data as JSON
            return
        }
        DadosApoio tipo = DadosApoio.get( params.sqTipoAbrangencia.toInteger() )
        params.q = params.q ? params.q.toUpperCase() : ''
        // procurar primeiro nas tabelas corporativas
        Abrangencia.findAllByTipoAndSalveIsNull( tipo ).sort{it.ordem}.each { item ->
            if ( item.descricao.toUpperCase().indexOf(params.q ) > -1 )
            {
                ident=''
                if (item.pai ) {
                    ident = '&nbsp;' * 4
                    if (data.items.count { it.id == item.pai.id } == 0) {
                        if (item.pai.pai) {
                            ident = '&nbsp;' * 4
                            if (data.items.count { it.id == item.pai.pai.id } == 0) {
                                if (!(codigo.toString() ==~ /[0-9].*/))
                                    if (!codigo ==~ /[0-9].*/) {
                                        codigo = ''
                                    }

                                map = [id: item.pai.pai.id, descricao: codigo + Util.capitalize(item.pai.pai.descricao), ordem: item.pai.pai.ordem + item.pai.pai.descricao]
                                if (item.pai.pai.salve) {
                                    map.codigoSistema = item.pai.pai.salve.codigoSistema
                                }
                                if (!data.items.contains(map)) {
                                    data.items.push(map)
                                }
                            }
                        }

                        codigo = (item.pai.codigo ? item.pai.codigo + '-' : '')
                        if (!(codigo.toString() ==~ /[0-9].*/)) {
                            codigo = ''
                        }
                        map = [id: item.pai.id, descricao: codigo + Util.capitalize(item.pai.descricao), ordem: item.pai.ordem+item.pai.descricao]
                        if (item.pai.salve) {
                            map.codigoSistema = item.pai.salve.codigoSistema
                        }
                        if (!data.items.contains(map)) {
                            data.items.push(map)
                        }
                    }
                }
                codigo = (item.codigo ? item.codigo + '-' : '')

                if( ! (  codigo.toString() ==~ /[0-9].*/) )
                {
                    codigo =''
                }

                map = [id: item.id, descricao: ident + codigo + Util.capitalize(item.descricao),ordem:item.ordem + item.descricao ]
                if (item.salve)
                {
                    map.codigoSistema = item.salve.codigoSistema
                }
                if( ! data.items.contains( map ) ) {
                    data.items.push(map)
                }
            }
        }
        // salve
        if( data.items.size()== 0 ) {
            Abrangencia.findAllByTipoAndSalveIsNotNull(tipo, [max: 1000]).sort { it.ordem }.each { item ->
                if (item.descricao.toUpperCase().indexOf(params.q) > -1) {
                    ident = ''
                    if (item.pai) {
                        ident = '&nbsp;' * 4
                        if (data.items.count { it.id == item.pai.id } == 0) {
                            if (item.pai.pai) {
                                ident = '&nbsp;' * 4
                                if (data.items.count { it.id == item.pai.pai.id } == 0) {
                                    if (!(codigo.toString() ==~ /[0-9].*/))
                                        if (! (codigo ==~ /[0-9].*/) ) {
                                            codigo = ''
                                        }

                                    map = [id: item.pai.pai.id, descricao: codigo + Util.capitalize(item.pai.pai.descricao), ordem: item.pai.pai.ordem]
                                    if (item.pai.pai.salve) {
                                        map.codigoSistema = item.pai.pai.salve.codigoSistema
                                    }
                                    if (!data.items.contains(map)) {
                                        data.items.push(map)
                                    }
                                }
                            }

                            codigo = (item.pai.codigo ? item.pai.codigo + '-' : '')
                            if (!(codigo.toString() ==~ /[0-9].*/)) {
                                codigo = ''
                            }
                            map = [id: item.pai.id, descricao: codigo + Util.capitalize(item.pai.descricao), ordem: item.pai.ordem]
                            if (item.pai.salve) {
                                map.codigoSistema = item.pai.salve.codigoSistema
                            }
                            if (!data.items.contains(map)) {
                                data.items.push(map)
                            }
                        }
                    }
                    codigo = (item.codigo ? item.codigo + '-' : '')

                    if (!(codigo.toString() ==~ /[0-9].*/)) {
                        codigo = ''
                    }

                    map = [id: item.id, descricao: ident + codigo + Util.capitalize(item.descricao), ordem: item.ordem]
                    if (item.salve) {
                        map.codigoSistema = item.salve.codigoSistema
                    }
                    if (!data.items.contains(map)) {
                        data.items.push(map)
                    }
                }
            }
        }
        /*Abrangencia.findAllByTipoAndPaiIsNull( DadosApoio.get( params.sqTipoAbrangencia.toInteger() ) ).each { pai ->
            if( pai.descricao.toUpperCase().indexOf( params.q ) > -1 )
            {
                map = [id: pai.id, descricao: (pai.codigo ? pai.codigo + '-' : '') + pai.descricao]
                if (pai.salve)
                {
                    map.codigoSistema = pai.salve.codigoSistema
                }

                data.items.push(map)
                // filhos
                Abrangencia.findAllByPai(pai).each { filho ->
                    map = [id: filho.id, descricao: ('&nbsp;' * 4) + (filho.codigo ? filho.codigo + '-' : '') + filho.descricao]
                    if (filho.salve)
                    {
                        map.codigoSistema = filho.salve.codigoSistema
                    }
                    data.items.push(map)
                    // netos
                    Abrangencia.findAllByPai(filho).each { neto ->
                        map = [id: neto.id, descricao: ('&nbsp;' * 8) + (neto.codigo ? neto.codigo + '-' : '') + neto.descricao]
                        if (neto.salve)
                        {
                            map.codigoSistema = neto.salve.codigoSistema
                        }
                        data.items.push(map)
                    }
                }
            }
        }
        */
       data.items = data.items.sort{it.ordem}
       render data as JSON
    }

    def getAbrangencias_old()
    {
        List itens=[]
        if( !params.sqTipoAbrangencia )
        {
            render itens as JSON
            return
        }
        Map data
        Abrangencia.findAllByTipoAndPaiIsNull( DadosApoio.get( params.sqTipoAbrangencia.toInteger() ) ).each { pai ->
            data=[id:pai.id, descricao:(pai.codigo ? pai.codigo+'-':'')+pai.descricao]
            if( pai.salve )
            {
                data.codigoSistema = pai.salve.codigoSistema
            }

            itens.push( data )
            // filhos
            Abrangencia.findAllByPai( pai ).each { filho ->
                data=[id:filho.id,descricao:('&nbsp;'*4) + (filho.codigo ? filho.codigo+'-':'')+filho.descricao]
                if( filho.salve )
                {
                    data.codigoSistema = filho.salve.codigoSistema
                }
                itens.push( data )
                // netos
                Abrangencia.findAllByPai( filho ).each { neto ->
                    data=[id:neto.id,descricao:('&nbsp;'*8) + (neto.codigo ? neto.codigo+'-':'')+neto.descricao]
                    if( neto.salve )
                    {
                        data.codigoSistema = neto.salve.codigoSistema
                    }
                    itens.push( data )
                }
            }

        }
        render itens as JSON
    }

    def getTipoAbrangencia() {
        List itens = []
        List subItens
        List dados
        if (params.isOcorrencia) {
            dados = dadosApoioService.getTable('TB_' + params.codigo)
            if (dados.size() > 0) {
                if( params.codigo =='BIOMA')
                {
                    dados = dados.findAll { it.codigoSistema != 'DESCONHECIDO'}
                }
                dados = dados.sort { it.ordem + it.descricao }
                dados.each {

                    itens.push([codigo: it.id, descricao: (it.codigo.isNumber() ? it.codigo + ' - ' : '') + it.descricao])
                    subItens = it.itens.sort { it3 -> it3.ordem + it3.descricao }
                    subItens.each { it2 ->
                        itens.push([codigo: it2.id, descricao: (it2.codigo.isNumber() ? it2.codigo + ' - ' : '') + it.descricao + ' / ' + it2.descricao])
                    }
                }
            } else {
                switch (params?.codigo)
                {
                    case 'ESTADO':
                        if (params.noContexto == 'AMEACA_REGIAO' || params.noContexto == 'USO_REGIAO' )
                        {
                            FichaOcorrencia.findAllByFichaAndContexto(Ficha.get(params.sqFicha), dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA','ESTADO')).each{
                                itens.push([codigo:it.estado.id,descricao:it.estado.noEstado + '-' + it.estado.sgEstado])
                            }
                        }
                        else
                        {
                            Estado.list().each {
                                itens.push([codigo: it.id, descricao: it.noEstado + '-' + it.sgEstado])
                            }
                        }
                        break

                    case 'PAIS':
                        Pais.list().each {
                            itens.push([codigo: it.id, descricao: it.noPais])
                        }
                        break

                    case 'UC':
                        Uc.list().each {
                            itens.push([codigo: it.id, descricao: it.sigla])
                        }
                        break

                }
            }
        } else {
            // ler os estados cadastrados na Aba Distribuição -> UF
            List dominio = []
            if ( params?.form == 'frmConHistorico' && params.codigo == 'ESTADO' )
            {
                FichaOcorrencia.findAllByFichaAndContexto(Ficha.get(params.sqFicha), dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA','ESTADO')).each{
                    dominio.push( it.estado.sgEstado )
                }
            }
            dados = dadosApoioService.getTable('TB_' + params.codigo)
            if (dados.size() > 0)
            {
                dados = dados.sort { it.ordem + it.descricao }
                if (params.codigo == 'BIOMA')
                {
                    dados = dados.findAll { it.codigoSistema != 'DESCONHECIDO' }
                }
                dados.each {

                    if( ! dominio || dominio.indexOf( it.codigoSistema) > -1 )
                    {
                        itens.push([codigo: it.id, descricao: (it?.codigo?.isNumber() ? it.codigo + ' - ' : '') + it.descricao])
                        subItens = it.itens.sort { it3 -> it3?.ordem + it3.descricao }
                        subItens.each { it2 ->
                            itens.push([codigo: it2.id, descricao: (it2?.codigo?.isNumber() ? it2.codigo + ' - ' : '') + it.descricao + ' / ' + it2.descricao])
                        }

                    }
                }
            }
        }
        render itens as JSON
    }

    def getHabitat() {
        List dados = []
        if (params.sqHabitatPai) {
            // lista de habitats já cadastrados para a ficha
            //List fichaHabitat = FichaHabitat.findAllByFicha(Ficha.get(params.sqFicha))
            // carregar somente os habitats adicionados
            List listHabitat = DadosApoio.createCriteria().list()
                    {
                        eq('pai', DadosApoio.get(params.sqHabitatPai))
                        /*if (fichaHabitat) {
                            not {
                                'in'('id', fichaHabitat.habitat.id)
                            }
                        }
                        */
                    }
            listHabitat.sort { it.ordem + it.descricao }.each {
                dados.push([id: it.id, descricao: it.descricao, codigo: it.codigo])
                it.itens.sort { it.ordem + it.descricao }.each { filho ->
                    dados.push([id: filho.id, descricao: '&nbsp;&nbsp;' + filho.descricao, codigo: filho.codigo])
                    filho.itens.sort { it.ordem + it.descricao }.each { neto ->
                        dados.push([id: neto.id, descricao: '&nbsp;&nbsp;&nbsp;&nbsp;' + neto.descricao, codigo: neto.codigo])
                    }
                }
            }
            render dados as JSON
        } else {
            render[] as JSON
        }
    }

    /** **********************************************************************************************************/
    /* Regionalizacao
/************************************************************************************************************/
    def saveRegionalizacao() {
        if (!params.noDominio || !params.noCampo || !params.noPropriedade || !params.noContexto) {
            return this._returnAjax(1, 'Parâmetros insuficientes!', "error")
        }
//return this._returnAjax(1, 'Ver Parametros', "error");

        Ficha ficha
        FichaOcorrencia regFichaOcorrencia
        FichaAmeacaRegiao regAmeaca
        FichaUsoRegiao regUso
        if (params.noContexto == 'AMEACA_REGIAO') {
            if (params.id) {
                regAmeaca = FichaAmeacaRegiao.get(params.id)
            } else {
                regAmeaca = new FichaAmeacaRegiao()
                regAmeaca.fichaAmeaca = FichaAmeaca.get(params.sqRegistro)
            }
            regFichaOcorrencia = regAmeaca.fichaOcorrencia
            ficha = regAmeaca.fichaAmeaca.ficha
        } else if (params.noContexto == 'USO_REGIAO') {
            if (params.id) {
                regUso = FichaUsoRegiao.get(params.id)
            } else {
                regUso = new FichaUsoRegiao()
                regUso.fichaUso = FichaUso.get(params.sqRegistro)
            }
            regFichaOcorrencia = regUso.fichaOcorrencia
            ficha = regUso.fichaUso.ficha

        }
        if (!regAmeaca && !regUso) {
            return this._returnAjax(1, 'Contexto ' + params.noContexto + ' não permitido!', "error")
        }

        // criar registro FichaOcorrencia
        if (!regFichaOcorrencia) {
            regFichaOcorrencia = new FichaOcorrencia()
            regFichaOcorrencia.ficha = ficha
        }
        regFichaOcorrencia.tipoAbrangencia = DadosApoio.get(params.sqTipoAbrangencia)
        //regFichaOcorrencia.abrangencia = DadosApoio.get( params[params.noCampo] )
        regFichaOcorrencia.abrangencia = Abrangencia.get( params[params.noCampo] )

        regFichaOcorrencia.contexto = dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA', params.noContexto)
        regFichaOcorrencia.txLocal = (params.txLocal ? params.txLocal : null)
        regFichaOcorrencia.noLocalidade = (params.noRegiaoOutra ? params.noRegiaoOutra : null)

        try
        {
            def domain = "br.gov.icmbio." + params.noDominio
            def classDominio
            try
            {
                classDominio = new GroovyClassLoader().loadClass(domain)
            }
            catch (Exception e)
            {
                domain = "br.gov.icmbio.DadosApoio"
                classDominio = new GroovyClassLoader().loadClass(domain)
            }
            if (params.noPropriedade.toUpperCase() != 'OUTRA' && params.noPropriedade.toUpperCase() != 'OUTRO')
            {
                regFichaOcorrencia[params.noPropriedade] = classDominio.get(params[params.noCampo])
            }
        } catch (Exception e ){ println e.getMessage()}

        // gravar de acordo com o tipo estado, pais, bacia etc...
        regFichaOcorrencia.save(flush: true)

        // return this._returnAjax(1, 'oK '+ domain, "success");
        if (regAmeaca) {
            regAmeaca.fichaOcorrencia = regFichaOcorrencia
            regAmeaca.save(flush: true)
        } else if (regUso) {
            regUso.fichaOcorrencia = regFichaOcorrencia
            regUso.save(flush: true)
        }
        return this._returnAjax(0, getMsg(2), "success")
    }

    def editRegionalizacao() {

        FichaAmeacaRegiao regAmeaca
        FichaUsoRegiao regUso
        if (params.noContexto == 'AMEACA_REGIAO') {
            regAmeaca = FichaAmeacaRegiao.get(params.id)
            if (regAmeaca?.id) {
                render regAmeaca.asJson()
                return
            }
        } else if (params.noContexto == 'USO_REGIAO') {
            regUso = FichaUsoRegiao.get(params.id)
            if (regUso?.id) {
                render regUso.asJson()
                return
            }

        }
        render [:] as JSON

    }

    def deleteRegionalizacao() {

        FichaOcorrencia fichaOcorrencia
        FichaAmeacaRegiao regAmeaca
        FichaUsoRegiao regUso
        if (params.noContexto == 'AMEACA_REGIAO') {
            regAmeaca = FichaAmeacaRegiao.get(params.id)
            fichaOcorrencia = regAmeaca.fichaOcorrencia
            regAmeaca.delete(flush: true)
        } else if (params.noContexto == 'USO_REGIAO') {

            regUso = FichaUsoRegiao.get(params.id)
            fichaOcorrencia =  regUso.fichaOcorrencia
            regUso.delete(flush: true)
        }
        if( fichaOcorrencia)
        {
            fichaOcorrencia.delete( flush:true )
        }
        return this._returnAjax(0)
    }

    def getGridSelRegiao() {
        if (!params.sqRegistro) {
            render 'sqRegistro não informado'
            return
        }
        List regioes
        DadosApoio contexto = dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA', params.noContexto)
        if (params.noContexto == 'AMEACA_REGIAO') {
            //regioes = FichaAmeacaRegiao.findAllByFichaAmeaca(FichaAmeaca.get(params.sqRegistro) )
            regioes = FichaAmeacaRegiao.createCriteria().list {
                createAlias('fichaOcorrencia', 'b')
                eq("fichaAmeaca", FichaAmeaca.get(params.sqRegistro))
                eq('b.contexto', contexto)
            }
        } else if (params.noContexto == 'USO_REGIAO') {
            //regioes = FichaUsoRegiao.findAllByFichaUso(FichaUso.get(params.sqRegistro) )
            regioes = FichaUsoRegiao.createCriteria().list {
                createAlias('fichaOcorrencia', 'b')
                eq("fichaUso", FichaUso.get(params.sqRegistro))
                eq('b.contexto', contexto)
            }
        }
        if (regioes) {
            render(template: '/ficha/divGridSelRegiao', model: ['listRegioes': regioes])
            return
        }
        render ''
    }

    /** **********************************************************************************************************/
    /* Georeferencia da Ameaça
    /************************************************************************************************************/
    def saveGeoreferencia() {
        if (!params.noContexto) {
            return this._returnAjax(1, 'Necessário informar o contexto Ameaca ou Uso?', "error")
        }
        if (!params.fldLatitude || !params.fldLongitude) {
            return this._returnAjax(1, 'Coordenadas não informadas', "error")
        }

        FichaOcorrencia regFichaOcorrencia
        FichaAmeacaRegiao regAmeaca
        FichaUsoRegiao regUso
        Ficha ficha
        if (params.noContexto == 'AMEACA_GEO') {
            if (params.id) {
                regAmeaca = FichaAmeacaRegiao.get(params.id)
            } else {
                regAmeaca = new FichaAmeacaRegiao()
                regAmeaca.fichaAmeaca = FichaAmeaca.get(params.sqRegistro)
            }
            regFichaOcorrencia = regAmeaca.fichaOcorrencia
            ficha = regAmeaca.fichaAmeaca.ficha
        } else if (params.noContexto == 'USO_GEO') {
            if (params.id) {
                regUso = FichaUsoRegiao.get(params.id)
            } else {
                regUso = new FichaUsoRegiao()
                regUso.fichaUso = FichaUso.get(params.sqRegistro)
            }
            regFichaOcorrencia = regUso.fichaOcorrencia
            ficha = regUso.fichaUso.ficha
        }
        if (!regAmeaca && !regUso) {
            return this._returnAjax(1, 'Contexto ' + params.noContexto + ' não permitido!', "error")
        }

        // criar registro FichaOcorrencia
        if (!regFichaOcorrencia) {
            regFichaOcorrencia = new FichaOcorrencia()
            regFichaOcorrencia.ficha = ficha
        }
        regFichaOcorrencia.tipoAbrangencia = DadosApoio.get(params.sqTipoAbrangencia)
        regFichaOcorrencia.contexto = dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA', params.noContexto)
        if( !regFichaOcorrencia.contexto )
        {
            return this._returnAjax(1, 'Contexto '+params.noContexto+' não encontrado na TB_CONTEXTO_OCORRENCIA!', "error")
        }
        regFichaOcorrencia.txLocal = (params.txLocal ? params.txLocal : null)
        regFichaOcorrencia.datum = DadosApoio.get(params.sqDatum)
        // gravar a coordenada
        GeometryFactory gf = new GeometryFactory()
        regFichaOcorrencia.geometry = gf.createPoint(new Coordinate(Double.parseDouble(params.fldLongitude), Double.parseDouble(params.fldLatitude)))
        if( regFichaOcorrencia.geometry ) {
            regFichaOcorrencia.geometry.SRID = 4674
        }
        regFichaOcorrencia.save(flush: true)
        // return this._returnAjax(1, 'oK '+ domain, "success");
        if (regAmeaca) {
            regAmeaca.fichaOcorrencia = regFichaOcorrencia
            regAmeaca.save(flush: true)
        } else if (regUso) {
            regUso.fichaOcorrencia = regFichaOcorrencia
            regUso.save(flush: true)
        }
        return this._returnAjax(0, getMsg(1), "success")
    }

    def getGridSelGeo() {
        if (!params.sqRegistro) {
            render 'sqRegistro não informado'
            return
        }
        List coordenadas
        DadosApoio contexto = dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA', params.noContexto)
        if (params.noContexto == 'AMEACA_GEO') {
            coordenadas = FichaAmeacaRegiao.createCriteria().list {
                createAlias('fichaOcorrencia', 'b')
                eq("fichaAmeaca", FichaAmeaca.get(params.sqRegistro))
                eq('b.contexto', contexto)
            }
        } else if (params.noContexto == 'USO_GEO') {
            coordenadas = FichaUsoRegiao.createCriteria().list {
                createAlias('fichaOcorrencia', 'b')
                eq("fichaUso", FichaUso.get(params.sqRegistro))
                eq('b.contexto', contexto)
            }

        }
        if (coordenadas) {
            render(template: '/ficha/divGridSelGeo', model: ['listCoordenadas': coordenadas])
            return
        }
        render ''
    }

    def editGeoreferencia() {

        if (params.noContexto == 'AMEACA_GEO') {
            FichaAmeacaRegiao regAmeaca = FichaAmeacaRegiao.get(params.id)
            render regAmeaca.asJson()
        } else if (params.noContexto == 'USO_GEO') {
            FichaUsoRegiao regUso = FichaUsoRegiao.get(params.id)
            render regUso.asJson()
        }
        else
        {
            render '{}'
        }
    }

    def deleteGeoreferencia() {

        if (params.noContexto == 'AMEACA_GEO') {
            FichaAmeacaRegiao regAmeaca = FichaAmeacaRegiao.get(params.id)
            FichaOcorrencia ocoAmeaca = regAmeaca.fichaOcorrencia
            regAmeaca.delete(flush: true)
            ocoAmeaca.delete(flush: true)
        } else if (params.noContexto == 'USO_GEO') {
            FichaUsoRegiao regUso = FichaUsoRegiao.get(params.id)
            FichaOcorrencia ocoUso =  regUso.fichaOcorrencia
            regUso.delete(flush: true)
            ocoUso.delete(flush: true)
        }
        return this._returnAjax(0)
    }
    /** **********************************************************************************************************/

    def saveAnexo() {
        String msgType='success'
        Date dtAnexo = new Date()
        if (!params.sqFicha) {
            return this._returnAjax(1, 'ID da ficha não informado.', "error")
        }

        // gravar o anexo
        def f = request.getFile('fldFichaAnexo')
        if (f && !f.empty) {
            if( params.dtAnexo ) {
                try {
                    dtAnexo = new Date().parse('dd/MM/yyyy', params.dtAnexo )
                } catch( Exception e ){
                    return this._returnAjax(1, 'Data '+params.dtAnexo + ' inexistente', "error" )
                }
            }
            DadosApoio contexto = dadosApoioService.getByCodigo('TB_CONTEXTO_ANEXO', params.contexto)
            if (!contexto) {
                return this._returnAjax(1, 'Contexto <b>' + params.contexto + '</b> não cadastrado na TB_CONTEXTO_ANEXO', "error")
            }
            File savePath = new File( grailsApplication.config.anexos.dir)
            if (!savePath.exists()) {
                if (!savePath.mkdirs()) {
                    return this._returnAjax(1, 'Não foi possivel criar o diretório ' + savePath.asString(), "error")
                }
            }
            Ficha ficha = Ficha.get(params.sqFicha)
            String fileName = f.getOriginalFilename().replaceAll(' |,|\\(.+\\)', '_').replaceAll('_+','-').replaceAll('-\\.','.')
            String fileExtension = fileName.substring(fileName.lastIndexOf("."))
            String hashFileName = f.inputStream.text.toString().encodeAsMD5() + fileExtension
            fileName = savePath.absolutePath + '/' + hashFileName
            if (!new File(fileName).exists()) {
                f.transferTo(new File(fileName))
                ImageService.createThumb(fileName)
            }
            FichaAnexo fa = FichaAnexo.findByFichaAndContextoAndDeLocalArquivo(ficha, contexto, hashFileName)
            if (!fa) {
                fa = new FichaAnexo()
                fa.ficha = ficha
                fa.contexto = contexto
                fa.deLocalArquivo = hashFileName
            }
            fa.deLegenda = params.deLegendaFichaAnexo
            fa.noArquivo = f.getOriginalFilename()
            fa.deTipoConteudo = f.getContentType()
            fa.inPrincipal = 'N'
            fa.dtAnexo = dtAnexo
            String mensagem = ''
            if( fa.noArquivo.toLowerCase() =~ /\.(jpe?g|bmp|png)$/ && params.inFichaAnexoPrincipal == 'S' ) {
                fa.inPrincipal = 'S'
            }
            fa.save(flush: true)
            fichaService.updatePercentual( ficha )
            return this._returnAjax(0, 'Arquivo gravado com SUCESSO!'+mensagem, msgType )
        }
    }

    def deleteAnexo() {
        if (!params.id) {
            return this._returnAjax(1, 'Id não informado', 'error')
        }
        FichaAnexo fa = FichaAnexo.get(params.id)
        if( !fa ) {
            return this._returnAjax(0)
        }
        String deLocalArquivo = fa.deLocalArquivo
        String deLocalThumb  = ''

        /*
        REGRAS PARA EXCLUSÃO DE ANEXO
        A) Se for exclusão da imagem PRINCIAL do mapa de distribuição a ficha não pode estar FINALIZADA em diante
        */
        if( fa.contexto.codigoSistema=='MAPA_DISTRIBUICAO' && fa.inPrincipal=='S'){
            if( (fa.ficha.situacaoFicha.codigoSistema =~ /FINALIZADA|PUBLICADA/) ) {
                return this._returnAjax(1, 'A ficha já está <b>' + fa.ficha.situacaoFicha.descricao.toLowerCase() + '</b>. Mapa não pode ser excluído.<br>Para alterar o mapa atual, cadastre um novo mapa como principal e exclua o anterior.', 'modal')
            }
        }

        // apagar do banco de dados
        Integer id = fa.ficha.id.toInteger()
        fa.delete(flush: true)
        fichaService.updatePercentual( fa.ficha.id )

        // ler somente o nome do arquivo sem o caminho completo
        Integer posicao = deLocalArquivo.lastIndexOf('/')
        if( posicao > 0 )
        {
            deLocalArquivo = deLocalArquivo.substring(posicao+1)
        }
        if ( FichaAnexo.countByDeLocalArquivoAndIdNotEqual(deLocalArquivo,fa.id) == 0 )
        {
            deLocalThumb = grailsApplication.config.anexos.dir + 'thumbnail.' + deLocalArquivo
            deLocalArquivo = grailsApplication.config.anexos.dir+'/'+deLocalArquivo
            // excluir o anexo do disco se não tiver sendo usado por outro registro

            File file = new File(deLocalArquivo)
            if (file.exists()) {
                // apagar do disco
                file.delete()
            }

            file = new File(deLocalThumb)
            if (file.exists())
            {
                // apagar do disco a imagem thumb
                file.delete()
            }
        }
        return this._returnAjax(0)
    }

    def getAnexo() {
        if (!params.id) {
            response.sendError(404)
            return
        }
        // deixar somente numeros no id porque poder ser postado 1245.zip no caso dos shp
        params.id = params.id.toString().replaceAll(/[^0-9]/,'')
        FichaAnexo pa = FichaAnexo.get(params.id.toLong())
        if (!pa ) {
            response.sendError(404)
            return
        }
        String hashFileName = pa.deLocalArquivo
        Integer posicao = hashFileName.lastIndexOf('/')
        if( posicao > 0 )
        {
            hashFileName = hashFileName.substring(posicao+1)
            pa.deLocalArquivo = hashFileName
            pa.save(flush:true)
        }
        File file
        if( params.thumb )
        {
            file = new File(grailsApplication.config.anexos.dir+'thumbnail.'+hashFileName)
            if ( file.exists() )
            {
                hashFileName = grailsApplication.config.anexos.dir+'thumbnail.'+hashFileName
            }
            else
            {
                // exibir imagem thumbnail padrao
                String fotoPadrao = grailsApplication.config.arquivos.dir+'thumbnail.no-image.jpeg'
                file = new File( fotoPadrao )
            }
        }
        if( ! file )
        {
            file = new File( grailsApplication.config.anexos.dir+hashFileName )
        }
        if ( ! file.exists() )
        {
            // exibir imagem padrao
            String fotoPadrao = grailsApplication.config.arquivos.dir+'no-image.jpeg'
            file = new File( fotoPadrao )
            if ( ! file.exists() ) {
                response.sendError(400)
                return
            }
        }
        String fileType = pa.deTipoConteudo ? pa.deTipoConteudo : 'application/octet-stream'
        String fileName = pa.noArquivo.replaceAll(' |,|\\(.+\\)', '_').replaceAll('_+','-').replaceAll('-\\.','.')
        if( fileType )
        {
            render(file: file, fileName: fileName, contentType: fileType)
        }
        else
        {
            render(file: file, fileName: fileName)
        }
    }

    def toggleImagemPrincipal(){
        try {
            if( session.sicae.user.isCO() ) {
                throw new Exception('Ação não autorizada')
            }
            if (params.sqFichaAnexo) {
                FichaAnexo fichaAnexo = FichaAnexo.get(params.sqFichaAnexo.toLong())
                if (!fichaAnexo) {
                    throw new Exception('Id anexo inválido')
                }
                boolean isShp = (fichaAnexo.noArquivo =~ /\.zip$/)
                DadosApoio contextoMapa = dadosApoioService.getByCodigo('TB_CONTEXTO_ANEXO','MAPA_DISTRIBUICAO')
                FichaAnexo.findAllByFichaAndContexto(fichaAnexo.ficha, contextoMapa).each{
                    if( ( isShp && ( it.noArquivo =~ /\.zip$/ ) ) || ( !isShp && !( it.noArquivo =~ /\.zip$/ ) ) ) {
                        if (it.id.toLong() == params.sqFichaAnexo.toLong()) {
                            it.inPrincipal = (it.inPrincipal == 'N' ? 'S' : 'N')
                        } else {
                            it.inPrincipal = 'N'
                        }
                        it.save()
                    }
                }
            }
        } catch( Exception e ){
            return this._returnAjax(1, e.getMessage(), 'error')
        }
        return this._returnAjax(0, '', 'success')
    }

    def alterarSituacaoEmLote() {
        if ( ! session?.sicae?.user?.sqPessoa ) {
            return this._returnAjax(1, 'Sessão expirada.', 'error')
        }
        if (!params['ids']) {
            return this._returnAjax(1, 'Nenhuma ficha Selecionada', 'error')
        }
        // se for ciclo de avaliacao 2010 - 1º ciclo, nao validar se a aba 11.6 esta vazia
        CicloAvaliacao cicloAvaliacao = CicloAvaliacao.get(params.sqCicloAvaliacao.toLong() )

        boolean isAdm = session.sicae.user.isADM()
        DadosApoio situacao = DadosApoio.get(params.sqNovaSituacao)
        if (!situacao) {
            return this._returnAjax(1, 'Situação Inválida!', 'error')
        }
        if( params.dsJustificativa) {
            params.dsJustificativa = Util.trimEditor( params.dsJustificativa ).replaceAll(/'/,"''")
        }

        if( situacao.codigoSistema=='EXCLUIDA' && !params.dsJustificativa ) {
            return this._returnAjax(1, 'Necessário informa a justificativa', 'error')
        }

        DadosApoio situacaoNaoAvaliada = dadosApoioService.getByCodigo('TB_SITUACAO_COLABORACAO', 'NAO_AVALIADA')
        if (!situacaoNaoAvaliada) {
            return this._returnAjax(1, 'Situação NAO_AVALIADA não cadastrada!', 'error')
        }
        DadosApoio situacaoResolverOficina = dadosApoioService.getByCodigo('TB_SITUACAO_COLABORACAO', 'RESOLVER_OFICINA')
        if (!situacaoResolverOficina) {
            return this._returnAjax(1, 'Situação RESOLVER_OFICINA não cadastrada!', 'error')
        }
        DadosApoio categoriaNE = dadosApoioService.getByCodigo('TB_CATEGORIA_IUCN', 'NE')
        if (!situacaoResolverOficina) {
            return this._returnAjax(1, 'Categoria IUCN (NE) não cadastrada!', 'error')
        }

        DadosApoio situacaoOficinaAberta = dadosApoioService.getByCodigo('TB_SITUACAO_OFICINA', 'ABERTA')
        if (!situacaoOficinaAberta) {
            return this._returnAjax(1, 'Situação da oficina ABERTA não cadastrada!', 'error')
        }

        DadosApoio situacaoFichaExcluida = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'EXCLUIDA')
        if ( !situacaoFichaExcluida ) {
            return this._returnAjax(1, 'Situação ficha EXCLUIDA não cadastrada!', 'error')
        }

        DadosApoio oficinaAvaliacao = dadosApoioService.getByCodigo('TB_TIPO_OFICINA', 'OFICINA_AVALIACAO')
        if (!oficinaAvaliacao) {
            return this._returnAjax(1, 'Tipo de oficina OFICINA_AVALIACAO não cadastrada!', 'error')
        }

        DadosApoio oficinaValidacao = dadosApoioService.getByCodigo('TB_TIPO_OFICINA', 'OFICINA_VALIDACAO')
        if (!oficinaValidacao) {
            return this._returnAjax(1, 'Tipo de oficina OFICINA_VALIDACAO não cadastrada!', 'error')
        }

        DadosApoio revisaoPosOficina = dadosApoioService.getByCodigo('TB_TIPO_CONSULTA_FICHA', 'REVISAO_POS_OFICINA')
        if ( ! revisaoPosOficina) {
            return this._returnAjax(1, 'Tipo de consulta REVISAO_POS_OFICINA não cadastrada!', 'error')
        }

        DadosApoio contextoMapaDistribuicao = dadosApoioService.getByCodigo('TB_CONTEXTO_ANEXO', 'MAPA_DISTRIBUICAO')
        if ( ! contextoMapaDistribuicao) {
            return this._returnAjax(1, 'Contexto MAPA_DISTRIBUICAO não cadastrada!', 'error')
        }

        String tokenAutorizacao = params?.tokenAutorizacao ?: ''
        //return this._returnAjax(0, 'Teste session ' + session['tokenAutorizacaoAcaoLote'], 'success')

        if (tokenAutorizacao && (!session['tokenAutorizacaoAcaoLote'] || session['tokenAutorizacaoAcaoLote'] != tokenAutorizacao)) {
            tokenAutorizacao = ''
        }

        //d('Token ativo:' + tokenAutorizacao)

        // dados para log
        String cpf = session.sicae.user.nuCpf
        String nome = session.sicae.user.noPessoa
        String ip = Util.getRequestIp(request)
        String controller = params.controller
        String action = params.action

        List listLateralJoins = []

        // ler todas as fichas que serão atualizadas
        String cmdSql = """select ficha.sq_ficha
                ,ficha.nm_cientifico
                ,ficha.sq_categoria_iucn
                ,ficha.sq_categoria_final
                ,ficha.sq_unidade_org
                ,ficha.st_localidade_desconhecida
                ,situacao.ds_dados_apoio as de_situacao_ficha
                ,situacao.cd_sistema as co_situacao_sistema
                ,situacao.de_dados_apoio_ordem as de_situacao_ficha_ordem
                ,ciclo_avaliacao.in_situacao as de_situacao_ciclo
                ,categoria.cd_sistema as co_categoria_sistema
                ,categoria_final.cd_sistema as co_categoria_final_sistema
                ,unidade.sg_unidade_org
                """


        //if ( ! ( situacao.codigoSistema =~ /^(CONSOLIDADA|POS_OFICINA|AVALIADA|AVALIADA_EM_REVISAO|AVALIADA_REVISADA)$/) ) {
            // ler as oficinas abertas, qualquer mudança de situação deve verificar se tem oficina aberta
            cmdSql += "\n,oficinas_abertas.de_oficinas_abertas"
            cmdSql += "\n,oficinas_abertas.nu_avaliacao_abertas"
            cmdSql += "\n,oficinas_abertas.nu_validacao_abertas"
            listLateralJoins.push("""left join lateral (
                            select sum( case when oficina.sq_tipo_oficina = ${oficinaAvaliacao.id.toString()} then 1 else 0 end ) as nu_avaliacao_abertas
                            ,sum( case when oficina.sq_tipo_oficina = ${oficinaValidacao.id.toString()} then 1 else 0 end) as nu_validacao_abertas
                            ,array_to_string( array_agg( concat(oficina.sg_oficina,' de ',to_char(dt_inicio,'dd/MM/yyyy'),' a ',to_char(dt_fim,'dd/MM/yyyy') ) ), ', ')  as de_oficinas_abertas
                            from salve.oficina_ficha
                            inner join salve.oficina on oficina.sq_oficina = oficina_ficha.sq_oficina
                            left join salve.oficina_ficha_versao ofv on ofv.sq_oficina_ficha = oficina_ficha.sq_oficina_ficha
                            where oficina_ficha.sq_ficha = ficha.sq_ficha
                            and oficina_ficha.st_transferida = false
                            and oficina.sq_tipo_oficina in (${ oficinaAvaliacao.id.toString()+','+oficinaValidacao.id.toString()})
                            and oficina.sq_situacao = ${situacaoOficinaAberta.id.toString()}
                            and ofv.sq_ficha_versao is null
                      ) as oficinas_abertas on true
                      """)

        /*} else {
            cmdSql += "\n,'' as de_oficinas_abertas"
            cmdSql += "\n,'' as nu_avaliacao_abertas"
            cmdSql += "\n,'' as nu_validacao_abertas"
        }*/


        // computar se tem consulta aberta para todas as situações menos consulta
        if ( ! ( situacao.codigoSistema =~ /^(CONSULTA)$/)) {
            cmdSql += "\n,consultas_abertas.de_consultas_abertas"
            listLateralJoins.push("""left join lateral (
                       select array_to_string( array_agg( concat(de_titulo_consulta,' de ',to_char(dt_inicio,'dd/MM/yyyy'),' a ',to_char(dt_fim,'dd/MM/yyyy') ) ) ,' e ') as de_consultas_abertas
                       from salve.ciclo_consulta_ficha
                       inner join salve.ciclo_consulta on ciclo_consulta.sq_ciclo_consulta = ciclo_consulta_ficha.sq_ciclo_consulta
                       left outer join salve.ciclo_consulta_ficha_versao versao on versao.sq_ciclo_consulta_ficha = ciclo_consulta_ficha.sq_ciclo_consulta_ficha
                       where ciclo_consulta_ficha.sq_ficha = ficha.sq_ficha
                       and ciclo_consulta.sq_tipo_consulta <> ${ revisaoPosOficina.id.toString() }
                       and ciclo_consulta.dt_fim >= '${new Date().format('yyyy-MM-dd')}'
                       and versao.sq_ficha_versao is null
                       limit 2
                    ) as consultas_abertas on true
                    """)
        } else {
            cmdSql += "\n,'' as de_consultas_abertas"
        }

        // computar colaborações pendentes
        if (situacao.codigoSistema =~ /^(CONSOLIDADA|POS_OFICINA|VALIDADA|FINALIZADA|PUBLICADA)$/) {
            cmdSql += "\n,coalesce(colaboracao_pendente.nu_colaboracao_pendente,0) as nu_colaboracao_pendente"
            cmdSql += "\n,coalesce(colaboracao_pendente.nu_resolver_oficina,0) as nu_resolver_oficina"
            listLateralJoins.push("""left join lateral (\nselect sum( case when ficha_colaboracao.sq_situacao = ${situacaoNaoAvaliada.id.toString()} then 1 else 0 end ) as nu_colaboracao_pendente
                ,sum( case when ficha_colaboracao.sq_situacao = ${situacaoResolverOficina.id.toString()} then 1 else 0 end ) as nu_resolver_oficina
                FROM salve.ficha_colaboracao
                INNER JOIN salve.ciclo_consulta_ficha ON ciclo_consulta_ficha.sq_ciclo_consulta_ficha = ficha_colaboracao.sq_ciclo_consulta_ficha
                where ciclo_consulta_ficha.sq_ficha = ficha.sq_ficha
                and ficha.sq_situacao_ficha <> ${situacaoFichaExcluida.id.toString()}
                and ficha_colaboracao.sq_situacao in ( ${situacaoNaoAvaliada.id.toString() + ',' + situacaoResolverOficina.id.toString()} )
                ) as colaboracao_pendente on true""")
        } else {
            cmdSql += ",0 as nu_colaboracao_pendente, 0 as nu_resolver_oficina"
        }

        // computar pendências da ficha
        if (situacao.codigoSistema =~ /PUBLICADA|FINALIZADA/ ) {
            cmdSql += "\n,coalesce(pendencia.nu_pendencia,0) as nu_pendencia"
            listLateralJoins.push("""left join lateral (
                        select count( sq_ficha_pendencia) as nu_pendencia from salve.ficha_pendencia
                        where sq_ficha = ficha.sq_ficha
                        and st_pendente = 'S'
                        ) as pendencia on true""")
        } else {
            cmdSql += "\n,0 as nu_pendencia"
        }

        // computar mapa distribuição informado
        if (situacao.codigoSistema == 'FINALIZADA') {
            cmdSql += "\n,mapa_distribuicao.sq_ficha_anexo as sq_mapa_distribuicao"
            listLateralJoins.push("""left join lateral (
                       select sq_ficha_anexo
                       from salve.ficha_anexo
                       where sq_ficha = ficha.sq_ficha
                       and in_principal = 'S'
                       and sq_contexto  = ${contextoMapaDistribuicao.id.toString()}
                       limit 1
                      ) as mapa_distribuicao on true""")
        } else {
            cmdSql += "\n,null as sq_mapa_distribuicao"
        }

        cmdSql += """\nfrom salve.ficha
            inner join salve.dados_apoio situacao on situacao.sq_dados_apoio = ficha.sq_situacao_ficha
            LEFT  join salve.dados_apoio categoria on categoria.sq_dados_apoio = ficha.sq_categoria_iucn
            LEFT  join salve.dados_apoio categoria_final on categoria_final.sq_dados_apoio = ficha.sq_categoria_final
            inner join salve.ciclo_avaliacao on ciclo_avaliacao.sq_ciclo_avaliacao = ficha.sq_ciclo_avaliacao
            inner join salve.vw_unidade_org unidade on unidade.sq_pessoa = ficha.sq_unidade_org"""
        if (listLateralJoins) {
            cmdSql += '\n' + listLateralJoins.join('\n')
        }
        cmdSql += """\nwhere ficha.sq_ficha in ( ${params['ids']} )
              and ficha.sq_situacao_ficha <> ${situacao.id.toString()}
            order by ficha.nm_cientifico
            """
        List listIdsAprovados = []
        List erros = []
        List listSqls = []
        List listExcluidas = []

        int qtdFichasAvaliadas = 0
        int qtdFichasValidadas = 0
        String nmCientifico = ''

        /** /
        println '-'*80
        println '-'*80
        println '-'*80
        println  cmdSql
        /**/

        // criar a lista de fichas
        List listFichas = sqlService.execSql(cmdSql)

        //return this._returnAjax(1, 'Teste sql', 'success')

        // verificar se o ciclo de avaliação está aberto
        if (listFichas) {
            // Ciclo deve estar aberto
            if (!listFichas[0].de_situacao_ciclo == 'A') {
                return this._returnAjax(1, 'Ciclo de Avaliação NÃO ESTÁ ABERTO!', 'error')
            }
        } else {
            return this._returnAjax(0, '<b>Nenhuma</b> ficha alterada!', 'info')
        }

        listFichas.each { row ->
            List listErros = []
            boolean abaAvaliacaoPreenchida = false
            boolean abaValidacaoPreenchida = false
            nmCientifico = '<b>' + Util.ncItalico(row.nm_cientifico, true) + '</b>'
            boolean rowValida = true
            // computar a quantidade de fichas AVALIADAS e VALIDADAS
            if (row.co_categoria_sistema && row.co_categoria_sistema != 'NE') {
                qtdFichasAvaliadas++
                abaAvaliacaoPreenchida = true
            }
            if (row.co_categoria_final_sistema && row.co_categoria_final_sistema != 'NE') {
                qtdFichasValidadas++
                abaValidacaoPreenchida = true
            }
            // 1º ciclo não tem aba 11.6
            if( cicloAvaliacao.nuAno == 2010 ) {
                abaAvaliacaoPreenchida = true
            }

            try {

                // 1) ficha dever ser da mesma unidade do usuário
                if (!isAdm && row.sq_unidade_org.toLong() != session.sicae.user.sqUnidadeOrg.toLong()) {
                    listErros.push('pertence a unidade <b>' + row.sg_unidade_org + '</b>')
                } else

                // 2) se a ficha estiver em uma consulta aberta não pode mudar de situação
                if (row.de_consultas_abertas) {
                    listErros.push('situação não pode ser alterada em lote durante a consulta/Revisão em andamento: ' + row.de_consultas_abertas)
                }

                // 2.1) se a ficha não estiver finalizada ou publicada e stiver em uma oficina aberta ( validação por exemplo )
                // a situação só pode ser alterada pela oficina com exceção PARA FINALIZADA E PUBLICADA
                if( ! ( row.co_situacao_sistema =~ '^(FINALIZADA|PUBLICADA)$') ) {
                    if (row.de_oficinas_abertas && !(situacao.codigoSistema =~ '^(EM_VALIDACAO|FINALIZADA|PUBLICADA)$')) {
                        listErros.push('situação não pode ser alterada em lote enquanto a oficina estiver aberta: ' + row.de_oficinas_abertas)
                    }
                }

                // 2.2 se a ficha ja estiver excluida limpar a justificativa
                if( row.co_situacao_sistema == 'EXCLUIDA') {
                    listSqls.push('update salve.ficha set ds_justificativa = null where sq_ficha = '+row.sq_ficha)
                }

                // 3) Regras para passar para compilação
                // se tiver dados nas abas 11.6 e/ou 11.7 não permitir e se for ADM solicitar confirmação
                if (situacao.codigoSistema == 'COMPILACAO') {
                    if (!isAdm) {
                        if (abaAvaliacaoPreenchida && abaValidacaoPreenchida) {
                            listErros.push('abas 11.6 e 11.7 preenchidas.')
                        } else {
                            if (abaAvaliacaoPreenchida) {
                                listErros.push('aba 11.6 preenchida.')
                            } else if (abaValidacaoPreenchida) {
                                listErros.push('aba 11.7 preenchida.')
                            }
                        }
                    }
                } else

                // 3.1) Regras para passar para excluida
                // se tiver dados nas abas 11.6 e/ou 11.7 não permitir e se for ADM solicitar confirmação

                if (situacao.codigoSistema == 'EXCLUIDA') {
                    if ( !isAdm ) {
                        if (abaAvaliacaoPreenchida && abaValidacaoPreenchida) {
                            listErros.push('abas 11.6 e 11.7 preenchidas.')
                        } else {
                            if (abaAvaliacaoPreenchida) {
                                listErros.push('aba 11.6 preenchida.')
                            } else if (abaValidacaoPreenchida) {
                                listErros.push('aba 11.7 preenchida.')
                            }
                        }
                    }
                } else

                // 4) Regras para passar para consulta ( não pode )
                // SOMENTE ATRAVÉS DA ABERTURA DE UMA CONSULTA AMPLA OU DIRETA
                /*// a ficha deve estar em COMPILACAO, CONSULTA_FINALIZADA, CONSOLIDADA
                if (situacao.codigoSistema == 'CONSULTA') {
                    if (!(row.co_situacao_sistema =~ /^(COMPILACAO|CONSULTA_FINALIZADA|CONSOLIDADA)$/)) {
                        listErros.push('está na situação ' + row.de_situacao_ficha)
                    }
                } else
                */


                // 5) Regras para passar para CONSULTA FINALIZADA
                // a ficha deve estar em COMPILACAO, CONSULTA OU CONSOLIDADA
                if (situacao.codigoSistema == 'CONSULTA_FINALIZADA') {
                    /*if (!(row.co_situacao_sistema =~ /^(COMPILACAO|CONSULTA|CONSOLIDADA|POS_OFICINA|PUBLICADA)$/)) {
                        listErros.push('está na situação ' + row.de_situacao_ficha)
                    }*/
                } else

                // 6) Regras para passar para CONSOLIDADA
                // a ficha deve estar em COMPILACAO,CONSULTA,CONSULTA_FINALIZADA
                // e não pode ter colaboracao pendente de avaliacao
                if (situacao.codigoSistema == 'CONSOLIDADA') {
                    /*if (!(row.co_situacao_sistema =~ /^(COMPILACAO|CONSULTA|CONSULTA_FINALIZADA|POS_OFICINA|PUBLICADA)$/)) {
                        listErros.push('está na situação ' + row.de_situacao_ficha)
                    }*/
                    if (row.nu_colaboracao_pendente > 0) {
                        listErros.push(row.nu_colaboracao_pendente.toString() + (row.nu_colaboracao_pendente == 1 ? ' colaboração não avaliada.' : ' colaborações não avaliadas.'))
                    }
                } else

                // 7) Regras para passar para AVALIADA_EM_REVISAO
                // a ficha estar em COMPILACAO, CONSULTA_FINALIZADA, CONSOLIDADA OU POS_OFICINA (AVALIADA)
                // tem que estar com aba 11.6 preenchida
                if (situacao.codigoSistema == 'AVALIADA_EM_REVISAO') {
                    /*if (!(row.co_situacao_sistema =~ /^(COMPILACAO|CONSULTA_FINALIZADA|CONSOLIDADA|POS_OFICINA|EM_VALIDACAO|PUBLICADA)$/)) {
                        listErros.push('está na situação ' + row.de_situacao_ficha)
                    }*/
                    if (!abaAvaliacaoPreenchida) {
                        listErros.push('aba 11.6 em branco.')
                    }
                } else

                // 8) Regras para a ficha passar para AVALIADA (POS_OFICINA)
                // a ficha deve estar em COMPILACAO, CONSULTA_FINALIZADA,CONSOLIDADA,AVALIADA_EM_REVISAO
                // tem que estar com a aba 11.6 preenchida
                // nao pode ter colaboracoes pendentes de avaliação
                if (situacao.codigoSistema == 'POS_OFICINA') { // AVALIADA
                    /*if (!(row.co_situacao_sistema =~ /^(COMPILACAO|CONSULTA_FINALIZADA|CONSOLIDADA|AVALIADA_REVISADA|AVALIADA_EM_REVISAO|PUBLICADA)$/)) {
                        listErros.push('está na situação ' + row.de_situacao_ficha)
                    }*/
                    if ( ! abaAvaliacaoPreenchida ) {
                        listErros.push('aba 11.6 em branco.')
                    }
                    if (row.nu_colaboracao_pendente.toInteger() + row.nu_resolver_oficina.toInteger() > 0) {
                        if (row.nu_colaboracao_pendente.toInteger() > 0) {
                            listErros.push(row.nu_colaboracao_pendente.toString() + (row.nu_colaboracao_pendente.toInteger() == 1 ? ' colaboração não avaliada.' : ' colaborações não avaliadas.'))
                        }
                        if (row.nu_resolver_oficina.toInteger() > 0) {
                            listErros.push(row.nu_resolver_oficina.toString() + (row.nu_resolver_oficina.toInteger() == 1 ? ' colaboração para resolver em oficina.' : ' colaborações para resolver em oficina'))
                        }
                    }
                } else

                // 9) Regras para a ficha passar para AVALIADA_REVISADA
                // a ficha deve estar em COMPILACAO, CONSULTA_FINALIZADA,CONSOLIDADA,AVALIADA,AVALIADA_EM_REVISAO
                // tem que estar com a aba 11.6 preenchida
                // nao pode ter colaboracoes pendentes de avaliação nem resolver em oficina
                if (situacao.codigoSistema == 'AVALIADA_REVISADA') {
                    /*if (!(row.co_situacao_sistema =~ /^(COMPILACAO|CONSULTA_FINALIZADA|CONSOLIDADA|POS_OFICINA|AVALIADA_EM_REVISAO|VALIDADA|VALIDADA_EM_REVISAO|PUBLICADA)$/)) {
                        listErros.push('está na situação ' + row.de_situacao_ficha)
                    }*/
                    if (!abaAvaliacaoPreenchida) {
                        listErros.push('aba 11.6 em branco.')
                    }
                    /*if( ! row.nu_pendencia.toInteger() > 0 ) {
                        listErros.push( row.nu_pendencia.toString() + ( row.nu_pendencia.toInteger() == 1 ? ' pendência.':' pendências.') )
                    }*/
                    if (row.nu_colaboracao_pendente.toInteger() + row.nu_resolver_oficina.toInteger() > 0) {
                        if (row.nu_colaboracao_pendente.toInteger() > 0) {
                            listErros.push(row.nu_colaboracao_pendente.toString() + (row.nu_colaboracao_pendente.toInteger() == 1 ? ' colaboração não avaliada.' : ' colaborações não avaliadas.'))
                        }
                        if (row.nu_resolver_oficina.toInteger() > 0) {
                            listErros.push(row.nu_resolver_oficina.toString() + (row.nu_resolver_oficina.toInteger() == 1 ? ' colaboração para resolver em oficina.' : ' colaborações para resolver em oficina'))
                        }
                    }
                } else

                    // tem que ter uma oficina de VALIDACAO aberta
                    if( situacao.codigoSistema=='EM_VALIDACAO') {
                        if ( ! row.nu_validacao_abertas ) {
                            listErros.push('nenhuma oficina de VALIDAÇÃO aberta com a ficha')
                        }
                    } else

                // 10) Regras para a ficha passar para VALIDADA
                // a ficha deve ter o resultado da VALIDACAO ( aba 11.7 ) preenchido
                // nao pode ter colaboracao pendente de avaliação
                if (situacao.codigoSistema == 'VALIDADA') {
                    if (!abaAvaliacaoPreenchida && !abaValidacaoPreenchida) {
                        listErros.push('abas 11.6 e 11.7 em branco.')
                    } else {
                        if (!abaAvaliacaoPreenchida) {
                            listErros.push('abas 11.6 em branco.')
                        } else if (!abaValidacaoPreenchida) {
                            listErros.push('abas 11.7 em branco.')
                        }
                    }
                    if (row.nu_colaboracao_pendente.toInteger() + row.nu_resolver_oficina.toInteger() > 0) {
                        if (row.nu_colaboracao_pendente.toInteger() > 0) {
                            listErros.push(row.nu_colaboracao_pendente.toString() + (row.nu_colaboracao_pendente.toInteger() == 1 ? ' colaboração não avaliada.' : ' colaborações não avaliadas.'))
                        }
                        if (row.nu_resolver_oficina.toInteger() > 0) {
                            listErros.push(row.nu_resolver_oficina.toString() + (row.nu_resolver_oficina.toInteger() == 1 ? ' colaboração para resolver em oficina.' : ' colaborações para resolver em oficina.'))
                        }
                    }
                } else

                // 11) Regras para a ficha passar para VALIDADA_EM_REVISAO
                // a ficha deve ter o resultado da VALIDACAO ( aba 11.7 ) preenchido
                if (situacao.codigoSistema == 'VALIDADA_EM_REVISAO') {
                    if (!abaAvaliacaoPreenchida && !abaValidacaoPreenchida) {
                        listErros.push('abas 11.6 e 11.7 em branco.')
                    } else {
                        if (!abaAvaliacaoPreenchida) {
                            listErros.push('abas 11.6 em branco.')
                        } else if (!abaValidacaoPreenchida) {
                            listErros.push('abas 11.7 em branco.')
                        }
                    }
                } else

                // 12) Regras para a ficha passar para FINALIZADA
                // a ficha deve ter o resultado da AVALIAÇÃO E DA VALIDACAO ( abas 11.6 e 11.7 ) preenchidas
                // a ficha nao pode ter pendencia
                // a ficha deve ter o Mapa de Distribuiçao cadastrado e marcado como prinicipal na aba 2
                if (situacao.codigoSistema == 'FINALIZADA') {
                    // abas 11.6 e 11.7 preenchidas
                    if (!abaAvaliacaoPreenchida && !abaValidacaoPreenchida) {
                        listErros.push('abas 11.6 e 11.7 em branco.')
                    } else {
                        if (!abaAvaliacaoPreenchida) {
                            listErros.push('abas 11.6 em branco.')
                        } else if (!abaValidacaoPreenchida) {
                            listErros.push('abas 11.7 em branco.')
                        }
                    }
                    if( ! row.sq_mapa_distribuicao ) {
                        if( row.st_localidade_desconhecida.toString().toUpperCase() != 'S' ) {
                            listErros.push('Mapa de distribuição não cadastrado ou localidade desconhecida não marcada.')
                        }
                    }
                    if (row.nu_pendencia) {
                        listErros.push(row.nu_pendencia.toString() + (row.nu_pendencia == 1 ? ' pendência.' : ' pendências.'))
                    }
                    if (row.nu_colaboracao_pendente + row.nu_resolver_oficina > 0) {
                        if (row.nu_colaboracao_pendente > 0) {
                            listErros.push(row.nu_colaboracao_pendente.toString() + (row.nu_colaboracao_pendente == 1 ? ' colaboração não avaliada' : ' colaborações não avaliadas'))
                        }
                        if (row.nu_resolver_oficina.toInteger() > 0) {
                            listErros.push(row.nu_resolver_oficina.toString() + (row.nu_resolver_oficina == 1 ? ' colaboração para resolver em oficina.' : ' colaborações para resolver em oficina'))
                        }
                    }
                } else

                // 13) Regras para a ficha passar para PUBLICADA
                // a ficha deve ter o resultado da AVALIAÇÃO E DA VALIDACAO ( abas 11.6 e 11.7 ) preenchidas
                // não ter pendencia e nem colaboraçao pendente
                if (situacao.codigoSistema == 'PUBLICADA') {
                    // abas 11.6 e 11.7 preenchidas
                    if (!abaAvaliacaoPreenchida && !abaValidacaoPreenchida) {
                        listErros.push('abas 11.6 e 11.7 em branco.')
                    } else {
                        if (!abaAvaliacaoPreenchida) {
                            listErros.push('abas 11.6 em branco.')
                        } else if (!abaValidacaoPreenchida) {
                            listErros.push('abas 11.7 em branco.')
                        }
                    }
                    if (row.nu_colaboracao_pendente.toInteger() + row.nu_resolver_oficina.toInteger() > 0) {

                        if (row.nu_colaboracao_pendente.toInteger() > 0) {
                            listErros.push(row.nu_colaboracao_pendente.toString() + (row.nu_colaboracao_pendente.toInteger() == 1 ? ' colaboração não avaliada.' : ' colaborações não avaliadas.'))
                        }
                        if (row.nu_resolver_oficina.toInteger() > 0) {
                            listErros.push(row.nu_resolver_oficina.toString() + (row.nu_resolver_oficina.toInteger() == 1 ? ' colaboração para resolver em oficina.' : ' colaborações para resolver em oficina.'))
                        }
                    }
                    // não pode ter pendencia não resolvida
                    if (row.nu_pendencia) {
                        listErros.push(row.nu_pendencia.toString() + (row.nu_pendencia == 1 ? ' pendência.' : ' pendências.'))
                    }
                }


                if (listErros) {
                    throw new Exception(nmCientifico + '<br>- ' + listErros.join('<br>- '))
                }

            } catch (Exception e) {
                rowValida = false
                //println e.getMessage()
                //e.printStackTrace()
                erros.push( e.getMessage() )
            }
            if (rowValida) {
                listIdsAprovados.push(row.sq_ficha)
            }
        }
        if (listIdsAprovados.size() > 0) {
            List logMessages = []
            if (situacao.codigoSistema =~ /^(COMPILACAO|CONSULTA|CONSULTA_FINALIZADA|CONSOLIDADA|EXCLUIDA)$/) {
                // limpar Avaliaçao, Validação, Mudança categoria e Ajuste Regional
                if (qtdFichasAvaliadas + qtdFichasValidadas == 0 || tokenAutorizacao) {
                    session['tokenAutorizacaoAcaoLote'] = null
                    logMessages.push("Alterou a situação das fichas (${listIdsAprovados.join(',')}) para " + situacao.codigoSistema + ' e as informações das abas 11.6, 11.7, ajuste regional e mudança de categoria foram limpas.')
                    listSqls.push("""update salve.ficha set sq_situacao_ficha=${situacao.id.toString()}
                            ,st_manter_lc=null
                            ,sq_categoria_iucn=${categoriaNE.id.toString()}
                            ,sq_categoria_final=null
                            ,ds_justificativa=${params.dsJustificativa ? "'" + params.dsJustificativa + "'" : null }
                            ,ds_justificativa_final=null
                            ,ds_criterio_aval_iucn=null
                            ,ds_criterio_aval_iucn_final=null
                            ,st_possivelmente_extinta=null
                            ,st_possivelmente_extinta_final=null
                            ,sq_tipo_conectividade=null
                            ,sq_tendencia_imigracao=null
                            ,st_declinio_br_popul_exterior=null
                            ,ds_conectividade_pop_exterior=null
                            ,dt_alteracao = '${ new Date().format('yyyy-MM-dd HH:mm:ss')}'
                            ,sq_usuario_alteracao = ${ session.sicae.user.sqPessoa.toString() }
                            where ficha.sq_ficha in ( ${listIdsAprovados.join(',')} );""")
                    listSqls.push("delete from salve.ficha_mudanca_categoria where sq_ficha in (${listIdsAprovados.join(',')});")
                    if( situacao.codigoSistema == 'EXCLUIDA' ) {
                        listExcluidas = listIdsAprovados
                    }
                } else {
                    session['tokenAutorizacaoAcaoLote'] = Util.md5(session.getId() + Util.randomString(6))
                }
            } else if (situacao.codigoSistema =~ '^(POS_OFICINA|AVALIADA_EM_REVISAO|AVALIADA_REVISADA)$') {
                // limpar a aba 11.7 - VALIDAÇÃO
                qtdFichasAvaliadas = 0 // não limpar a aba 11.6
                if (qtdFichasValidadas == 0 || tokenAutorizacao) {
                    session['tokenAutorizacaoAcaoLote'] = null
                    listSqls.push("""update salve.ficha set sq_situacao_ficha=${situacao.id.toString()}
                            ,sq_categoria_final=null
                            ,ds_justificativa_final=null
                            ,ds_criterio_aval_iucn_final=null
                            ,st_possivelmente_extinta_final=null
                            ,dt_alteracao = '${ new Date().format('yyyy-MM-dd HH:mm:ss')}'
                            ,sq_usuario_alteracao = ${ session.sicae.user.sqPessoa.toString() }
                            where ficha.sq_ficha in ( ${listIdsAprovados.join(',')} )""")
                    logMessages.push("Alterou a situação das fichas (${listIdsAprovados.join(',')}) para " + situacao.codigoSistema + ' e as informações da aba 11.7 foram limpas.')
                } else {
                    session['tokenAutorizacaoAcaoLote'] = Util.md5(session.getId() + Util.randomString(6))
                }
            } else {

                String updatePublicacao = ""
                if( situacao.codigoSistema == 'PUBLICADA' ) {
                    String dataPublicacao = new Date().format('yyyy-MM-dd HH:mm:ss')
                    updatePublicacao = ",sq_usuario_publicacao = ${ session.sicae.user.sqPessoa.toString() }, dt_publicacao = '${dataPublicacao}' "
                }else if( situacao.codigoSistema == 'FINALIZADA' ) {
                    updatePublicacao = ",sq_usuario_publicacao = null, dt_publicacao = null "
                }

                session['tokenAutorizacaoAcaoLote']=null
                qtdFichasAvaliadas = 0 // não limpar a aba 11.6
                qtdFichasValidadas = 0 // não limpar a aba 11.7
                listSqls.push("""update salve.ficha set
                             dt_alteracao = '${ new Date().format('yyyy-MM-dd HH:mm:ss')}'
                            ,sq_usuario_alteracao = ${ session.sicae.user.sqPessoa.toString() }
                            ,sq_situacao_ficha=${situacao.id.toString()}
                            ${updatePublicacao}
                            ${situacao.codigoSistema == 'EXCLUIDA' ? ",ds_justificativa='${ params.dsJustificativa }'" : ''}
                            where ficha.sq_ficha in ( ${listIdsAprovados.join(',')} )""")
                logMessages.push("Alterou a situação das fichas (${listIdsAprovados.join(',')}) para " + situacao.codigoSistema + '.')
            }

            /** /
            listSqls.each { sql ->
                println sql
            }
            /**/
            // executar os scripts de atualização do banco de dados
            if (listSqls) {
                //    println 'sqlService 2'
                sqlService.execSql(listSqls.join(';\n'))
            }

            // gravar logs na tabela thrila_auditoria
            logMessages.each { message ->
                logService.log(controller, action, nome, cpf, ip, message)
            }

            // se a situação for PUBLICADA, atualizar a Autoria/citação das fichas sem Autoria/Citação
            if( situacao.codigoSistema == 'PUBLICADA' ) {
                DadosApoio situacaoPublicada = dadosApoioService.getByCodigo( 'TB_SITUACAO_FICHA', 'PUBLICADA' )
                Ficha.createCriteria().list {
                    isNull('dsCitacao')
                    eq('situacaoFicha', situacaoPublicada )
                   'in'('id', listIdsAprovados.collect{ it.toLong() } )
                }.each {
                    it.dsCitacao = fichaService.getAutoria( it.id.toLong() )
                }
            }
            // gravar o historico da exclusao
            if( listExcluidas ) {
                listExcluidas = listIdsAprovados
                //DadosApoio contextoVersao = dadosApoioService.getByCodigo('TB_CONTEXTO_VERSAO_FICHA','EXCLUSAO_LOTE');
                listExcluidas.each {
                    fichaService.gravarHistoricoSituacaoExcluida(it.toLong(), params.dsJustificativa, session.sicae.user.sqPessoa.toLong())

                    // DISCUTIDO EM REUNIÃO SALVE-SEM-CICLO QUE NÃO É NECESSÁRIO VERSIONAR FICHA ALTERADA PARA SITUAÇÃO EXCLUIDA
                    // gerar versão da ficha excluida
                    //fichaService.versionarFicha(it.toLong(), session.sicae.user.sqPessoa.toLong(), 'EXCLUSAO_LOTE')
                    //sqlService.execSql("select salve.fn_versionar_ficha(${it.toString()},${session.sicae.user.sqPessoa},${contextoVersao.id} )::text")
                }
                // SALVE SEM CICLO
                // quando a ficha for excluida desmarcar a versão publicada para que a ficha não seja exibida no módulo público
                cmdSql = """UPDATE salve.ficha_versao set st_publico = false
                             WHERE st_publico = true
                               AND sq_ficha = any( array[${listExcluidas.join(',')}] )
               """
                sqlService.execSql(cmdSql)
            }
        } else {
            logService.log(controller, action, nome, cpf, ip, 'Nenhuma ficha foi alterada')
        }
        if (erros.size() > 0) {
            return this._returnAjax(1, '<p class="red">' + erros.size().toString() + ' Ficha(s) <b>NÃO</b> Atualizada(s) para <b>' + situacao.descricao + '</b></p>' + erros.join('<br>'), 'modal')
        }
        if (session['tokenAutorizacaoAcaoLote']) {
            Map data = [qtdFichasAvaliadas: qtdFichasAvaliadas, qtdFichasValidadas: qtdFichasValidadas, tokenAutorizacao: session['tokenAutorizacaoAcaoLote']]
            return this._returnAjax(0, 'Autorização solicitada', 'success', data)
        } else {
            return this._returnAjax(0, 'Operação executada com SUCESSO!', 'success')
        }
    }

    def excluirFichaEmLote() {
        // somente ponto focal ou adm
        if( ! session.sicae.user.isPF() && !session.sicae.user.isADM() )
        {
            return this._returnAjax(1,'Sem permissão para excluir ficha')
        }
        List fichasExcluir = []
        Ficha ficha
        params.ids.split(',').each {
            if ( it.isInteger() )
            {
                ficha = Ficha.get( it.toInteger() )
                if( ficha ) {
                    if (ficha.situacaoFicha.codigoSistema == 'EXCLUIDA') {
                        if (ficha.canModify(session?.sicae?.user)) {
                            fichasExcluir.push(it.toInteger())
                        }
                    }
                }
            }
        }
        if( fichasExcluir.size == 0  )
        {
            return this._returnAjax(1, 'Nenhuma ficha Selecionada pode ser excluída!<br><br>Obs: para que a ficha seja excluída ela deve estar na situação EXCLUÍDA.', 'error')
        }
        Ficha.executeUpdate("delete Ficha f where f.id in (${fichasExcluir.join(',')})" )
        return this._returnAjax(0)
    }

    def alterarUnidadeLote() {
        if( !params.sqUnidadeDestino )
        {
            return this._returnAjax(1,'Unidade destino não informada!')
        }

        // somente ponto focal ou adm
        if( ! session.sicae.user.isPF() && !session.sicae.user.isADM() )
        {
            return this._returnAjax(1,'Sem permissão para transferir ficha(s)')
        }
        Unidade unidadeDestino = Unidade.get( params.sqUnidadeDestino)
        DadosApoio pontoFocal = dadosApoioService.getByCodigo('TB_PAPEL_FICHA','PONTO_FOCAL')
        if( unidadeDestino ) {
            List fichasAlterar = []
            Ficha ficha
            params.ids.split(',').each {
                ficha = Ficha.get(it.toInteger())
                if ( ficha.canModify( session?.sicae?.user ) && ficha.unidade.id.toLong() != unidadeDestino.id.toLong() )
                {
                    fichasAlterar.push(it.toInteger())
                }
            }
            if (fichasAlterar.size == 0)
            {
                return this._returnAjax(1, 'Nenhuma ficha Selecionada pode ser transferida!', 'error')
            }
            Ficha.executeUpdate("update Ficha f set f.unidade=:unidadeDestino where f.id in ( ${fichasAlterar.join(',')})", [unidadeDestino:unidadeDestino])
            // remover ponto focal da(s) ficha(s)
            sqlService.execSql("""UPDATE salve.ficha_pessoa SET IN_ATIVO = 'N' where sq_ficha IN ( ${fichasAlterar.join(',')} ) and sq_papel=${pontoFocal.id.toString()} and in_ativo = 'S';""")
        }
        return this._returnAjax(0)

    }

    def alterarGrupoSubgrupoLote() {
        // somente adm
        if ( !  session.sicae.user.isADM()) {
            return this._returnAjax(1, 'Sem permissão para alterar Grupo/Subgrupo de ficha(s).')
        }
        if ( ! params.ids ) {
            return this._returnAjax(1, 'Nenhuma ficha selecionada.')
        }
        String cmdSql = """update salve.ficha set sq_grupo = ${params.sqGrupoAvaliado ? params.sqGrupoAvaliado.toString():'null'}
            , sq_subgrupo = ${params.sqSubgrupo?params.sqSubgrupo.toString():'null'} where sq_ficha in (${params.ids})"""
        sqlService.execSql( cmdSql )
        return this._returnAjax(0,'Gravação realizada com SUCESSO!','success')
    }

    def alterarGrupoSalveLote() {
        // somente adm
        if ( !  session.sicae.user.isADM()) {
            return this._returnAjax(1, 'Sem permissão para alterar Grupo/Subgrupo de ficha(s).')
        }
        if ( ! params.ids ) {
            return this._returnAjax(1, 'Nenhuma ficha selecionada.')
        }
        String cmdSql = """update salve.ficha set sq_grupo_salve = ${params.sqGrupoSalve ? params.sqGrupoSalve.toString():'null'} where sq_ficha in (${params.ids})"""
        sqlService.execSql( cmdSql )
        return this._returnAjax(0,'Gravação realizada com SUCESSO!','success')
    }

    def alterarAutoriaLote() {
        /*if ( !  session.sicae.user.isADM()) {
            return this._returnAjax(1, 'Sem permissão para alterar Grupo/Subgrupo de ficha(s).')
        }*/
        if ( ! params.ids ) {
            return this._returnAjax(1, 'Nenhuma ficha selecionada.')
        }
        if( params.preechimentoAutomatico == 'S' ) {
            //List sqls = []
            params.ids.split(',').each{
                fichaService.getAutoria( it.toLong(),true )
                /*if( autoria ) {
                    sqls.push( sprintf( "update salve.ficha set ds_citacao = '%s' where sq_ficha = %s", [ autoria, it.toString() ] ) )
                }
                if( sqls.size(  ) > 300 ) {
                    sqlService.execSql( sqls.join(';') )
                    sqls = []
                }*/
            }
            /*
            if( sqls.size(  ) > 0 ) {
                sqlService.execSql( sqls.join(';') )
            }*/
        }
        else {
            //return this._returnAjax(1, 'Os nomes dos autores não foram inforamados.')
            DadosApoio categoriaNE = dadosApoioService.getByCodigo('TB_CATEGORIA_IUCN','NE')
            String cmdSql
            Map sqlParams = [:]
            if ( params.txAutoria ) {
                String txAutoria = params.txAutoria.replaceAll(/\n/,'; ') // alterar quebra de linha por ponto-e-virgula
                txAutoria = txAutoria.replaceAll(/;/,'; ') // adicionar espaco entre os nomes
                txAutoria = txAutoria.replaceAll(/\s{2,}/,' ') // remover espacos duplos
                txAutoria = Util.stripTags( txAutoria )
                cmdSql = """update salve.ficha set ds_citacao = :txAutoria where sq_ficha in (${params.ids}) and ficha.sq_categoria_iucn is not null and ficha.sq_categoria_iucn <> ${categoriaNE.id.toString()}"""
                sqlParams = [ txAutoria: txAutoria ]
            } else  {
                cmdSql = """update salve.ficha set ds_citacao = null where sq_ficha in (${params.ids}) and ficha.sq_categoria_iucn is not null and ficha.sq_categoria_iucn <> ${categoriaNE.id.toString()}"""
            }
            sqlService.execSql( cmdSql, sqlParams )
        }
        return this._returnAjax(0,'Gravação realizada com SUCESSO!','success')
    }

    def alterarEquipeTecnicaLote() {
        if ( ! params.ids ) {
            return this._returnAjax(1, 'Nenhuma ficha selecionada.')
        }
        if( params.preechimentoAutomatico == 'S' ) {
            //List sqls = []
            params.ids.split(',').each{
                fichaService.getApoioTecnico( it.toLong(),true )
            }
        }
        else  {
            //return this._returnAjax(1, 'Os nomes dos autores não foram inforamados.')
            DadosApoio categoriaNE = dadosApoioService.getByCodigo('TB_CATEGORIA_IUCN','NE')
            String cmdSql
            Map sqlParams = [:]
            if ( params.txEquipeTecnica ) {
                String txEquipeTecnica = params.txEquipeTecnica.replaceAll(/\n/,'; ') // alterar quebra de linha por ponto-e-virgula
                txEquipeTecnica = txEquipeTecnica.replaceAll(/,/,'; ') // alterar virgula por ponto-e-virgula
                txEquipeTecnica = txEquipeTecnica.replaceAll(/;/,'; ') // adicionar espaco entre os nomes
                txEquipeTecnica = txEquipeTecnica.replaceAll(/\s{2,}/,' ') // remover espacos duplos
                txEquipeTecnica = Util.stripTags( txEquipeTecnica )
                //cmdSql = """update salve.ficha set ds_equipe_tecnica = :txEquipeTecnica where sq_ficha in (${params.ids}) and ficha.sq_categoria_iucn is not null and ficha.sq_categoria_iucn <> ${categoriaNE.id.toString()}"""
                cmdSql = """update salve.ficha set ds_equipe_tecnica = :txEquipeTecnica where sq_ficha in (${params.ids})"""
                sqlParams = [ txEquipeTecnica: txEquipeTecnica ]
            } else {
                cmdSql = """update salve.ficha set ds_equipe_tecnica = null where sq_ficha in (${params.ids})"""
            }
            sqlService.execSql( cmdSql, sqlParams )
        }
        return this._returnAjax(0,'Gravação realizada com SUCESSO!','success')
    }

    def alterarColaboradoresLote() {
        if ( ! params.ids ) {
            return this._returnAjax(1, 'Nenhuma ficha selecionada.')
        }
        if( params.preechimentoAutomatico == 'S' ) {
            //List sqls = []
            params.ids.split(',').each{
                fichaService.getColaboradores( it.toLong(),true )
            }
        }
        else if ( params.txColaboradores ) {
            //return this._returnAjax(1, 'Os nomes dos autores não foram inforamados.')
            DadosApoio categoriaNE = dadosApoioService.getByCodigo('TB_CATEGORIA_IUCN','NE')
            String txColaboradores = params.txColaboradores.replaceAll(/\n/,'; ')
            txColaboradores = txColaboradores.replaceAll(/,/,'; ') // alterar virgula por ponto-e-virgula
            txColaboradores = txColaboradores.replaceAll(/;/,'; ') // adicionar espaco entre os nomes
            txColaboradores = txColaboradores.replaceAll(/\s{2,}/,' ') // remover espacos duplos
            txColaboradores = Util.stripTags( txColaboradores )
            String cmdSql = """update salve.ficha set ds_colaboradores = :txColaboradores where sq_ficha in (${ params.ids }) and ficha.sq_categoria_iucn is not null and ficha.sq_categoria_iucn <> ${categoriaNE.id.toString()}"""
            sqlService.execSql( cmdSql, [ txColaboradores: txColaboradores ] )
        } else {
            return this._returnAjax(1,'Nenhum parâmetro fornecido!','info')
        }
        return this._returnAjax(0,'Gravação realizada com SUCESSO!','success')
    }

    def getCategoriaUltimaAvaliacaoNacional() {
        Map resultado=[:]
        if( params.sqTaxon && params.reino && params.noCientifico )
        {
            // recuperar a ultima avaliação nacional
            if( params.reino == 'ANIMALIA')
            {
                DadosApoio tipo = dadosApoioService.getByCodigo('TB_TIPO_AVALIACAO', 'NACIONAL_BRASIL')
                List listAvaliacao = FichaHistoricoAvaliacao.createCriteria().list() {
                    createAlias('ficha', 'f')
                    eq('f.taxon', Taxon.get(params.sqTaxon))
                    eq('tipoAvaliacao', tipo)
                    order('nuAnoAvaliacao', 'desc')
                    maxResults(1)
                }
                if (listAvaliacao.size() == 1)
                {
                    resultado.sqCategoriaIucn = listAvaliacao[0].categoriaIucn.id
                    resultado.coCategoriaIucn = listAvaliacao[0].categoriaIucn.codigo
                    resultado.deCategoriaIucn = listAvaliacao[0].categoriaIucn.descricao
                }
            }
            else if( params.reino == 'PLANTAE')
            {
                // pesquisar no ws do CNCFLORA http://cncflora.jbrj.gov.br/services/index.html
                if( params.noCientifico )
                {
                    //JSONElement dados = requestService.doGet('http://cncflora.jbrj.gov.br/services/search/all?q=' + java.net.URLEncoder.encode(params.noCientifico, "UTF-8") + '&type=taxon');
                    JSONElement dados = this.getCategoriaCncflora( params.noCientifico )
                    if (dados.size() > 0)
                    {
                        if ( dados.error )
                        {
                            resultado = dados
                        }
                        else
                        {
                            if( dados.taxon.current.scientificName.toUpperCase().indexOf( params.noCientifico.toUpperCase()) == 0 )
                            {
                                if ( dados.category )
                                {
                                    DadosApoio categoria = dadosApoioService.getByCodigo('TB_CATEGORIA_IUCN', dados.category)
                                    if (categoria)
                                    {
                                        resultado.sqCategoriaIucn = categoria.id
                                        resultado.coCategoriaIucn = categoria.codigo
                                        resultado.deCategoriaIucn = categoria.descricao
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        render resultado as JSON
    }

    def getCategoriaCncflora(String noCientifico) {
        try
        {
            if( grailsApplication?.config?.url?.cncfloraWsTaxon )
            {
                //println 'Consultando CNCFLORA....'
                noCientifico = noCientifico.replaceAll(' ','+')
                String url =  grailsApplication.config.url.cncfloraWsTaxon.toString()+noCientifico
                //String resultado = requestService.doGet( url,null,'json');
                def res = requestService.doGetJson( url )
                if( ! res )
                {
                    Map data = [:]
                    data.error = '<b>' + params.noCientifico+'</b><br>Categoria NÃO encontrada no site do CNCFLORA!'
                    res = JSON.parse( (data as JSON).toString() )
                }
                return res

                /*if( resultado == '{}' )
                {
                    Map data = [:];
                    data.error = '<b>' + params.noCientifico+'</b><br>Categoria NÃO encontrada no site do CNCFLORA!';
                    resultado = JSON.parse( (data as JSON).toString() );
                }
                return JSON.parse( resultado );
                */
            }
        }
        catch( Exception e)
        {
            Map result = [error:e.getMessage()]
            return JSON.parse( (result as JSON).toString() )
        }
    }

    def gerarPdf() {
        sleep(500)
        if( ! params.sqFicha )
        {
            return this._returnAjax(1, 'Parâmetros insuficientes!', 'error')
        }
        Ficha ficha = Ficha.get(params.sqFicha )
        if( !ficha )
        {
            return this._returnAjax(1, 'Ficha Não Encontrada!', 'error')
        }

        boolean imprimirPendencias = ( params.pendencias && params.pendencias.toUpperCase() == 'S' ? true : false)
        PdfFicha pdf = new PdfFicha( ficha.id.toInteger(), grailsApplication.config.temp.dir.toString())
        // integração SYS/IUCN
        if( params?.language && params.language == 'en') {
            pdf.setTranslateToEnglish(true)
        }
        pdf.setTitulo(grailsApplication.config.app.instituicao.nome)
        pdf.setSubTitulo(grailsApplication.config.app.title)
        pdf.setLogo(grailsApplication.config.app.instituicao.logo)
        pdf.setRefBibs(fichaService.getFichaRefBibs(params.sqFicha.toLong() ) )
        pdf.setUfs(fichaService.getUfsNameByRegiaoText( params.sqFicha.toLong() ) )
        pdf.setBiomas(fichaService.getBiomasText( params.sqFicha.toLong() ))
        pdf.setBacias(fichaService.getBaciasText( params.sqFicha.toLong() ))
        pdf.setUcs(fichaService.getUcsRegistro(params.sqFicha.toLong() ) )
        pdf.unidade = session?.sicae?.user?.sgUnidadeOrg ?: grailsApplication.config.app.sigla
        pdf.ciclo = ficha.cicloAvaliacao.deCicloAvaliacao.toString()
        pdf.setImprimirPendencias(imprimirPendencias)
        String fileName = pdf.run()
        return this._returnAjax(0, '', 'success',[fileName:fileName])
    }

    def gerarRtf(){
        sleep(500)
        if( ! params.sqFicha )
        {
            return this._returnAjax(1, 'Parâmetros insuficientes!', 'error')
        }
        Ficha ficha = Ficha.get(params.sqFicha )
        if( !ficha )
        {
            return this._returnAjax(1, 'Ficha Não Encontrada!', 'error')
        }

        // parametros de controle do relatório
        Map config = [ adColaboracao:params?.adColaboracao
                      ,adAvaliacao:params?.adAvaliacao
                      ,adValidacao:params?.adValidacao ]

        RtfFicha rtf = new RtfFicha( ficha.id.toInteger() , grailsApplication.config.temp.dir.toString(), config )
        rtf.unidade = session?.sicae?.user?.sgUnidadeOrg ?: 'SALVE WS'
        rtf.ciclo = ficha.cicloAvaliacao.deCicloAvaliacao.toString()
        rtf.setRefBibs(fichaService.getFichaRefBibs( ficha.id.toLong() ) )
        rtf.setBiomas(fichaService.getBiomasText( params.sqFicha.toLong() ))
        rtf.setBacias(fichaService.getBaciasText( params.sqFicha.toLong() ))
        rtf.setUfs(fichaService.getUfsNameByRegiaoText( ficha.id.toLong() ) )
        rtf.setUcs(fichaService.getUcsRegistro(params.sqFicha.toLong() ) )
        String fileName = rtf.run()
        if( !fileName )
        {
            return this._returnAjax(1, 'Ocorreu um erro na criação do arquivo.', 'warining',[fileName:''])
        }
        return this._returnAjax(0, '', 'success',[fileName:fileName])
    }

    def gerarZipFichas() {
        Map res=[type:'success',status:0,msg:'',error:'']
        Map config=[contexto:'salve',sqPessoa:session?.sicae?.user?.sqPessoa]
        GrailsHttpSession selfSession = session

        if( ! params.ids && !params.todasPaginas )
        {
            params.todasPaginas='S'
        }
        params.formato = params.formato ?: 'rtf' // rtf ou pdf
        config.formato=params.formato

        /** /
        println( params )
        /**/

        /** /
        println ' '
        println 'Zips recebidos'
        println params.ids
        println "-"*100
        println ' '
        /**/

        if( params.todasPaginas=='S') {
            params.ids = ''
            if (params.contexto == 'ficha') {
                params.ids = fichaService.search(params).collect { it.sq_ficha }.join(',')
            } else if (params.contexto == 'oficina') {
                // fichas da oficina
                if (!params.ids) {
                    // gerar ids das fichas da oficina
                    if (!params.sqOficina) {
                        res.msg = 'Id da oficina não foi informado!'
                        res.type = 'error'
                        res.status = 1
                        render res as JSON
                        return
                    }
                    List ids = []
                    OficinaFicha.createCriteria().list {
                        eq('oficina', Oficina.get(params.sqOficina.toInteger()))
                        vwFicha {
                            ne('sqSituacaoFicha', DadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'EXCLUIDA').id.toInteger())
                        }
                    }.each {
                        ids.push(it.ficha.id)
                    }
                    params.ids = ids.join(',')
                }
                config.adColaboracao = params.adColaboracao
                config.adValidacao   = params.adValidacao
                config.adAvaliacao   = params.adAvaliacao
                config.contexto      = 'oficina'
            }
        }

        if (params.contexto == 'oficina-avaliacao') {
            // fichas da oficina de avaliacao
            if (!params.ids) {
                // gerar ids das fichas da oficina
                if (!params.sqOficina) {
                    res.msg = 'Id da oficina não foi informado!'
                    res.type = 'error'
                    res.status = 1
                    render res as JSON
                    return
                }
                List ids = []
                OficinaFicha.createCriteria().list {
                    eq('oficina', Oficina.get(params.sqOficina.toInteger()))
                }.each {
                    ids.push(it.ficha.id)
                }
                params.ids = ids.join(',')
            }
            config.adColaboracao = params.adColaboracao
            config.adValidacao   = params.adValidacao
            config.adAvaliacao   = params.adAvaliacao
            config.contexto      = 'oficina-avaliacao'
            config.sqOficina     = params.sqOficina
        }

        if(  ! params.ids )
        {
            res.msg='Fichas não informadas'
            res.type='error'
            res.status=1
            render  res as JSON
        }
        else {
            List ids = params.ids.split(',')
            String unidade = (session?.sicae?.user?.sgUnidadeOrg ?: 'Sistema Salve')
            String tempDir = grailsApplication.config.temp.dir
            if( ids.size() > this.maxFichasExportar ) {
                res.msg='Limite máximo para esta solicitação é de ' + this.maxFichasExportar.toString()+' fichas.'
                res.type='error'
                res.status=1
                render  res as JSON
                return
            }
            String urlSalve = grailsApplication.config.url.sistema ?: 'https://salve.icmbio.gov.br/salve-estadual/'

            // criar o job para acompanhamento pelo usuário
            UserJobs job

            // CRIAR PARAMETROS DO JOB
            Map jobParams = [  usuario  : PessoaFisica.get( session.sicae.user.sqPessoa )
                              ,deRotulo : 'Exportação fichas para aqruivo zip']

            // CRIAR O JOB NO BANCO DE DADOS
            job = userJobService.createJob( jobParams ).save( flush:true )

            // DISPARAR O PROCESSO DE CRIAÇÃO DO ARQUIVO ZIP
            fichaService.gerarZipFichasAsync(ids, tempDir, unidade, config, urlSalve, job);
        }
        render res as JSON
    }

    def exportCsvOld( Lista fichas ) {
        sleep(1000)
        String fileName =''
        Integer status  = 0
        String msg      =''
        String type     = 'success'

        try
        {
            fileName = exportDataService.fichas( fichas )
        }
        catch (Exception e)
        {
            msg    = e.getMessage()
            status = 1
            type   = 'error'
        }
        return this._returnAjax(status, msg, type,[fileName:fileName])
    }

    def exportCsv() {
        Map res = [status: 0, msg: '', type: 'success', error: []]
        res.msg = 'Método desativado. (ficha.exportCsv)'
        res.status = 1
        render res as JSON
        return



        /*
        // criar variavel local selfSession para utilzar workers
        GrailsHttpSession selfSession = session

        Map res = [status: 0, msg: '', type: 'success', error: []]
        if( session.exportingData == true ) {
            //session.exportingOccurrences = false
            res.status=1
            res.msg = 'Já existe uma exportação em andamento. Aguarde a finalização.'
            render res as JSON
            return
        }


        Map workerData = [sqPessoa: session?.sicae?.user?.sqPessoa
                          , contexto : 'ficha'
                          , fichas   : []
        ]
        if (!params.sqCicloAvaliacao) {
            res.msg = 'Ciclo de avaliação não informado!'
            res.type = 'error'
            res.status = 1
        } else {
            params.colJsonTrilha = true // adicionar a coluna json_trilha da tabela taxonomia.taxon no resultado da consulta
            params.colNomeComum  = true // adicionar columa no_comum no resultado da consulta
            params.colAvaliacaoFinalFicha = true // adicionar colunas com as categorias da oficina e avaliação final
            params.colHistorico = true // adicionar colunas com a ultima avaliação nacional
            params.colCicloAvaliacao = true //  adicionar informações do ciclo de avaliação no resultado da consulta
            params.colGrupoTaxonomico = true //  adicionar informações dos grupos
            workerData.fichas = fichaService.search(params)
            if( workerData.fichas.size() > this.maxFichasExportar ) {
                res.msg='Limite máximo para esta solicitação é de ' + this.maxFichasExportar.toString()+' fichas.'
                res.type='error'
                res.status=1
                render  res as JSON
                return
                }
            registrarTrilhaAuditoria('exportar_fichas',workerData.contexto, workerData)
            runAsync {
                try {
                    exportDataService.fichasPlanilha(selfSession, workerData)
                } catch (Exception e) {
                    println 'Ocorreu um erro.'
                    println e.getMessage()
                }
            }
        }
        render res as JSON
         */
    }

    def exportPendencia() {
        // criar variavel local selfSession para utilzar workers
        GrailsHttpSession selfSession = session

        Map res = [status: 0, msg: '', type: 'success', error: []]

        Map workerData = [sqPessoa: session?.sicae?.user?.sqPessoa
                          , sqFichas   : []
        ]
        if (!params.sqCicloAvaliacao) {
            res.msg = 'Ciclo de avaliação não informado!'
            res.type = 'error'
            res.status = 1
        } else {
            // selecionar as fichas
            workerData.sqFichas = fichaService.search(params)?.sq_ficha
            runAsync {
                try {
                    exportDataService.fichasPendenciasPlanilha(selfSession, workerData)
                } catch (Exception e) {
                    println 'Ocorreu um erro.'
                    println e.getMessage()
                }
            }
        }

        render res as JSON
    }

    def getCentroideEstado() {
        if( !params.lat || !params.lon )
        {
            return this._returnAjax(1,'Coordenadas inválidas!','error')
        }
        Map data = geoService.getCentroideEstado( params.lat.toBigDecimal(), params.lon.toBigDecimal(), params.sqEstado ? params.sqEstado.toLong() : 0l )
        if( data.noEstado ) {
            return this._returnAjax(0,'Centróide do Estado <b>' + data.noEstado+'</b> definido!','success', data)
        }
        return this._returnAjax(1,'Centróide não encontrado!','errror', data)
    }

    def getCentroideMunicipio() {
        if( !params.lat || !params.lon )
        {
            return this._returnAjax(1,'Coordenadas inválidas!','error')
        }
        Map data = geoService.getCentroideMunicipio( params.lat.toBigDecimal(), params.lon.toBigDecimal(), params.sqMunicipio ? params.sqMunicipio.toLong() : 0l )
        if( data.noMunicipio )
        {
            return this._returnAjax(0,'Centróide <b>' + data.noMunicipio+'</b> definido!','success', data)
        }
        return this._returnAjax(1,'Centróide não encontrado!','errror', data)
    }

    def getCentroide()  {

        if( params.sqUc )
        {
            try {
                Uc uc = Uc.get( params.sqUc.toInteger() )
                Map centroide = uc.centroide
                return this._returnAjax(0,null,null, centroide )
            }
            catch(Exception e) {
                println e.getMessage()
                return this._returnAjax(1)
            }
        }
        return this._returnAjax(1)
    }

    def getUcByCoord() {
        Map data = [:]
        if( !params.lat || !params.lon )
        {
            render data as JSON
            return
        }

        Uc uc = geoService.getUcByCoord( params.lat.toBigDecimal(), params.lon.toBigDecimal() )
        if( uc ) {
            data.sqControle = uc.id
            data.sqUc = uc.sqUc
            data.noUc = (uc?.sgUnidade ?: uc.noUc )
        }

        // verificar se o bioma é compativel com a a espécie
        if( params.sqFicha ) {
            data.msgAlertaBioma = geoService.validarBiomaGrupo( params.sqFicha.toLong(), params.lat.toBigDecimal(), params.lon.toBigDecimal() )
        }

        /*
        // localicar Estado
        Estado uf = geoService.getUfByCoord( params.lat.toBigDecimal(), params.lon.toBigDecimal() )
        if( uf )
        {
            data.sqPais = uf?.pais?.id
            data.sqEstado = uf.id
            data.noEstado = uf.noEstado
            data.sgEstado = uf.sgEstado

            Municipio municipio = geoService.getMunicipioByCoord(params.lat.toBigDecimal(), params.lon.toBigDecimal(),uf)
            if ( municipio )
            {
                data.sqMunicipio = municipio.id
                data.noMunicipio = municipio.noMunicipio
            }

            Bioma bioma = geoService.getBiomaByCoord(params.lat.toBigDecimal(), params.lon.toBigDecimal())
            if( bioma )
            {
                DadosApoio biomaApoio = dadosApoioService.getByDescricao('TB_BIOMA', bioma.noBioma )
                if( biomaApoio ) {
                    data.sqBioma = biomaApoio.id;
                    data.noBioma = biomaApoio.descricao
                }
            }
        }*/
        render data as JSON
    }

    def getAndamentoImportacaoPlanilhas() {
        Map res = [data:[]]
        Planilha.createCriteria().list{
            cache:false
            eq('inProcessando',true)
           }.each{
            res.data.push([id:it.id,percentual:it.nuPercentual,situacao: (it.inProcessando?'Processando':'Finalizada')])
           }
        render res as JSON
    }

    /**
     * Retorna o stream da imagem para ser utilizado nas tags <img src=""> nas páginas
     * @return
     */
    def showImagem() {
        String path
        if( !params.sqFicha || !params.tipo )
        {
            render '<h3>Necessário informar parâmetros: sqFicha e tipo</h3>'
            return
        }

        String noImage = grailsApplication.config.arquivos.dir+'/no-image.jpeg'
        if( params.tipo == 'mapa-distribuicao')
        {
            path = fichaService.getFichaDistribuicaoMapaPrincipal(Ficha.get(params.sqFicha))
            if( !path )
            {
                path = noImage
            }

        } else if( params.tipo == 'mapa-ameaca') {
            path = fichaService.getFichaImagemAmeaca( Ficha.get( params.sqFicha ) )
            if( !path )
            {
                path = noImage
            }

        }
        String extensao = Util.getFileExtension(path)
        if (path)
        {
            File f = new File(path)
            if( ! f.exists() )
            {
                f = new File(noImage )
                extensao='jpeg'
            }
            if( f.exists() ) {
                response.contentType = 'image/' + extensao
                response.outputStream << f.getBytes()
                response.outputStream.flush()
                return
            }
            render ''
        } else {
            render ''
        }
    }

    /**
     * Retorna json com os pontos do portalbio e cadastrados no salve e colaborações
     * @return
     */
    def getPontos() {

        // o retorno será JSON
        response.setHeader("Content-Type", "application/json")

        // cabeçalho padrão do GeoJson da resposta
        Map geoJson = ["type": "FeatureCollection", 'crs': ['type': 'name', 'properties': ['name': 'EPSG:4326']], "features": []]
        String url  = grailsApplication.config.url.biocache
        String noCientifico
        Ficha ficha
        DadosApoio contextoOcorrencia = dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA', 'REGISTRO_OCORRENCIA')

        List fixedColors = ['#ff0000','#00ff00','#0000ff','#808080','#C0C0C0','#2F4F4F','#00008B','#483D8B','#4B0082','#9370DB','#4682B4','#483D8B','#B0C4DE','#800080','#FF00FF','#FF1493','#FF69B4','#DB7093','#DC143C','#FF0000','#800000','#B22222','#FFB6C1','#FA8072','#FF6347','#FF7F50','#D2691E','#CD853F','#BC8F8F','#DEB887','#FF4500','#FF8C00','#006400','#556B2F','#808000','#00FF00','#9ACD32','#ADFF2F','#7FFF00','#00FF7F','#20B2AA','#00FFFF','#00CED1','#1E90FF','#00BFFF','#008B8B','#7FFFD4','#98FB98','#DAA520','#FFFF00','#FFD700','#F0E68C']
        Map cores =[:]

        def feature

        if( !grailsApplication?.config?.url?.biocache )
        {
            render geoJson as JSON
            println 'Constante url.biocache não definido no arquivo de confirações'
            return
        }

        if( !grailsApplication?.config?.url?.solr )
        {
            render geoJson as JSON
            println 'Constante url.solr não definido no arquivo de confirações'
            return
        }

        if(!params.sqFicha)
        {
            render geoJson as JSON
        }
        else
        {
            ficha = Ficha.get( params.sqFicha.toInteger() )
            if( !ficha )
            {
                render geoJson as JSON
                return
            }
            // ler os pontos cadastrados no ciclo
            noCientifico = ficha.taxon.noCientifico
            if( ! noCientifico )
            {
                render geoJson as JSON
                return
            }

            //  PORTALBIO
            //
            // ler os pontos do partalbio e se não contrar no banco de dados ler do portalbio
            List listOcorrenciaPortalbio = FichaOcorrenciaPortalbio.findAllByFicha(ficha)
            if( ! listOcorrenciaPortalbio )
            {
                listOcorrenciaPortalbio = lerPontosPortalbio(noCientifico)
                gravarPontosPortalbio(ficha,listOcorrenciaPortalbio)
            }
            listOcorrenciaPortalbio.each {
                // definir a cor do ponto para os registro do portalbio
                if( !cores[it.noInstituicao] )
                {
                    if( cores.size() >= fixedColors.size() )
                    {
                        cores[it.noInstituicao]=Util.getRandomColor()
                    }
                    else
                    {
                        cores[it.noInstituicao] = fixedColors[ cores.size() ]
                    }
                }

                feature = ["type"      : "Feature",
                           "id"        : it.id,
                           "properties": [
                                   "text"    : '<b>Portalbio/'+it.noInstituicao + '</b>'+
                                                '<br/>'+it.noLocal +
                                                '<br/>'+it.noCientifico +
                                                '<br/>Responsável:' + it.noAutor+
                                               (it.dtOcorrencia ? '<br/>Data Ocorrência: '+it.dtOcorrencia.format("dd/MMM/yyyy") : '') +
                                               (it.dtCarencia ? '<br/>Carência: '+it.dtCarencia.format("dd/MMM/yyyy") : '')
                                                //'<br><a title="Abrir página do Portal da Biodiversidade" target="_blank" href="https://portaldabiodiversidade.icmbio.gov.br/portal/occurrences/'+it.deUuid+'">Abrir página do PortalBio</a>'
                                                ,"bd"      : 'portalbio',
                                   "color"   : cores[it.noInstituicao], //color,
                                   "legend"  : it.noInstituicao,//(color=="#ff000"?'Espécie Ameaçada':'Espécie Não Ameaçada'),
                                   "ameaca_s": it.deAmeaca,
                                   "utilizadoAvaliacao": it.inUtilizadoAvaliacao,
                                   "txNaoUtilizadoAvaliacao": it.txNaoUtilizadoAvaliacao?:''
                                   // TODO - adicionar mais atributtos aqui
                           ],
                           "geometry"  : [
                                   "type"       : "Point",
                                   "coordinates": [it.geometry.x, it.geometry.y]
                           ]
                ]
                geoJson.features.push(feature)
            }

            // SALVE
            FichaOcorrencia.findAllByFichaAndContexto(ficha, dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA', 'REGISTRO_OCORRENCIA')).each
            {
                if (it.geometry)
                {

                    if( !cores['salve'] )
                    {
                        if( cores.size() >= fixedColors.size() )
                        {
                            cores['salve']=Util.getRandomColor()
                        }
                        else
                        {
                            cores['salve'] = fixedColors[ cores.size() ]
                        }
                    }
                   feature = ["type"      : "Feature",
                               "id"        : it.id,
                               "properties": [
                                       "text": '<b>SALVE<br>Espécie</b><br>'+
                                       it.ficha.noCientifico+'<br>'+
                                       '<span class="nowrap">Presença Atual na Coordenada: <b>'+it.inPresencaAtualText+'</b></span>',
                                       "bd"  : 'salve',
                                       "color": cores['salve'],
                                       "legend":'Salve',
                                       "utilizadoAvaliacao": it.inUtilizadoAvaliacao,
                                       "txNaoUtilizadoAvaliacao": it.txNaoUtilizadoAvaliacao?:''

                                       // TODO - adicionar mais atributtos aqui
                               ],
                               "geometry"  : [
                                       "type"       : "Point",
                                       "coordinates": [it?.geometry?.x, it?.geometry?.y]
                               ]
                    ]
                    geoJson.features.push( feature )
                }
            }


             // ler os pontos das subespecies
            if( ficha.taxon.nivelTaxonomico.coNivelTaxonomico=='ESPECIE')
            {
                FichaOcorrencia.createCriteria().list {
                    createAlias('ficha','f')
                    createAlias('ficha.taxon','taxon')
                    eq('taxon.pai', ficha.taxon)
                    eq('contexto',contextoOcorrencia )
                }.each { it ->
                    if (it.geometry)
                    {
                        if( !cores[it.ficha.nmCientifico] )
                        {

                          if( cores.size() > fixedColors.size() )
                            {
                                cores[it.ficha.nmCientifico]=Util.getRandomColor()
                            }
                            else
                            {
                                cores[it.ficha.nmCientifico] = fixedColors[ cores.size() ]
                            }
                        }

                        feature = ["type"      : "Feature",
                                   "id"        : it.id,
                                   "properties": [
                                           "text": '<b>SALVE<br>Subespecie</b><br>'+
                                                   it.ficha.nmCientifico+'<br>'+
                                                   '<span class="nowrap">Presença Atual na Coordenada: <b>' + it.inPresencaAtualText +'</b></span>',
                                           "bd"  : 'salve',
                                           "color": cores['salve'], //color,
                                           "legend":'Salve' ,
                                           "utilizadoAvaliacao": it.inUtilizadoAvaliacao,
                                           "txNaoUtilizadoAvaliacao": it.txNaoUtilizadoAvaliacao?:''

                                           // TODO - adicionar mais atributtos aqui

                                   ],
                                   "geometry"  : [
                                           "type"       : "Point",
                                           "coordinates": [it?.geometry?.x, it?.geometry?.y]
                                   ]
                        ]
                        geoJson.features.push(feature)
                    }
                }
            }


            // COLABORAÇÕES
            FichaOcorrenciaConsulta.createCriteria().list{
                cicloConsultaFicha {
                    eq('ficha', ficha)
                }
                order('id')
            }.each {
                if (it.fichaOcorrencia.geometry)
                {

                    if (!cores['salve-consulta'])
                    {
                        if (cores.size() >= fixedColors.size())
                        {
                            cores['salve-consulta'] = Util.getRandomColor()
                        } else
                        {
                            cores['salve-consulta'] = fixedColors[cores.size()]
                        }
                    }
                    feature = ["type"      : "Feature",
                               "id"        : it.id,
                               "properties": [
                                       "bd"    : 'salve-consulta',
                                       //"id"  : it.id,
                                       "text"  : '<b>Salve-Consulta</b><br>' +
                                               it.fichaOcorrencia.noLocalidade +
                                               '<br>Situação: ' + it.cicloConsultaFicha.ficha.situacaoFicha.descricao +
                                               '<br>Data do Registro: ' + it.fichaOcorrencia.dataHora +
                                               '<br>Cadastrado por: ' + it.usuario.noUsuario +
                                               '<br>Cadastrado em: ' + it.fichaOcorrencia?.dtInclusao.format('dd/MM/yyyy HH:mm:ss') +
                                               '<br>Última alteração: ' + it.fichaOcorrencia?.dtAlteracao.format('dd/MM/yyyy HH:mm:ss') +
                                               '<br>Observações: ' + it.txObservacao,
                                       "utilizadoAvaliacao": it.fichaOcorrencia.inUtilizadoAvaliacao,
                                       "txNaoUtilizadoAvaliacao": it.fichaOcorrencia.txNaoUtilizadoAvaliacao?:'',
                                       "color" : cores['salve-consulta'], //color,
                                       "legend": 'Salve Consulta',

                               ],
                               "geometry"  : [
                                       "type"       : "Point",
                                       "coordinates": [it.fichaOcorrencia.geometry?.x, it.fichaOcorrencia.geometry?.y]
                               ]
                    ]
                    geoJson.features.push(feature)
                }

            }

            // ler pontos colaboradores
            /*FichaColaboracaoOcorrencia.createCriteria().list{
                cicloConsultaFicha{ eq('ficha',ficha) } }.each {
                if (it.geometry)
                {

                    if( !cores['salve-consulta'] )
                        {

                          if( cores.size() > fixedColors.size() )
                            {
                                cores['salve-consulta']=Util.getRandomColor()
                            }
                            else
                            {
                                cores['salve-consulta'] = fixedColors[ cores.size() ]
                            }
                    }

                    feature = ["type"      : "Feature",
                               "id"        : it.id,
                               "properties": [
                                       "bd"  : 'salve-consulta',
                                       //"id"  : it.id,
                                       "text": '<b>Salve-Consulta</b><br>'+
                                                it.noLocalidade+
                                               '<br>Situação: ' + it.cicloConsultaFicha.ficha.situacaoFicha.descricao+
                                               '<br>Data do Registro: '+it.dataHora+
                                               '<br>Cadastrado por: '+it.usuario.noUsuario+
                                               '<br>Cadastrado em: '+it.dtInclusao.format('dd/MM/yyyy HH:mm:ss')+
                                               '<br>Última alteração: '+it.dtAlteracao.format('dd/MM/yyyy HH:mm:ss')+
                                               '<br>Observações: '+it.txObservacao,
                                        "color": cores['salve-consulta'], //color,
                                        "legend":'Salve Consulta' ,

                               ],
                               "geometry"  : [
                                       "type"       : "Point",
                                       "coordinates": [it?.geometry?.x, it?.geometry?.y]
                               ]
                    ]
                    geoJson.features.push(feature);
                }
                */
            // ser pontos
            render geoJson as JSON
        }
    }

    private List lerPontosPortalbio(String noCientifico = '' ) {
        List resultado = []
        String p = 'raw_taxon_name:"' + noCientifico + '"'
        String url  = grailsApplication.config.url.biocache
        String strData = ''
        def dados = requestService.doPost(url, [q: p, pageSize: 10000], "")
        if (!dados)
        {
            return dados
        }

        try{

            def tmp = JSON.parse(dados)
            Date data
            Date eventDate
            Date carencia
            def local   // raw_locality
            def codigo  // raw_collectionCode
            List occurrences = []
            if (tmp?.sr?.occurrences)
            {
                occurrences = tmp?.sr?.occurrences
            } else
            {
                occurrences = tmp.occurrences
            }
            occurrences.eachWithIndex { item, i ->
                if (item.decimalLatitude.toString().indexOf('.') > 0
                    && item.decimalLatitude.toString().indexOf('.') < 4
                    && item.decimalLatitude.toString().indexOf('E') == - 1
                    )
                {
                    eventDate=null
                    data=null
                    try {
                        strData = item.verbatimEventDate
                        if( strData )
                        {
                            try {
                                strData = strData.split(' ')[0]
                                if (strData.length() == 10) {
                                    strData = strData.replaceAll(/-/, '/')
                                    data = new Date().parse("dd/MM/yyyy", strData)
                                }
                            } catch ( Exception e ){ data = null }
                        }
                        if ( !data && item.eventDate )
                        {
                            data = new Date(item.eventDate).format("dd/MMM/yyyy")
                        }
                    } catch( Exception e ) {}
                    if( data )
                    {
                        eventDate = data
                    }
                    carencia = null
                    try {
                        if ( item.carencia  )
                        {
                            data = new Date(item.accessRights).format("dd/MMM/yyyy")
                        }
                    } catch( Exception e ) {}
                    if( data )
                    {
                        carencia  = data
                    }
                    local =''
                    if( item.raw_locality)
                    {
                        local = item.raw_locality
                    }
                    codigo =''
                    if( item.raw_collectionCode)
                    {
                        codigo = item.raw_collectionCode
                    }
                    //GeometryFactory gf = new GeometryFactory()
                    Map linha = ["noInstituicao"      : item.raw_institutionCode,
                               "deUuid"             : item.uuid.toString(),
                               "noLocal"            : local,
                               "noCientifico"       : item.scientificName,
                               "noAutor"            : item.collector,
                               "dtOcorrencia"       : eventDate,
                               "dtCarencia"         : carencia,
                               "deAmeaca"           : item.ameaca_s,
                               //"geometry"           : gf.createPoint( new Coordinate( Double.parseDouble( item.decimalLongitude.toString()  ), Double.parseDouble( item.decimalLatitude.toString()) ) ),
                               "txOcorrencia"       : item.toString()
                              ]
                    resultado.push( linha )
                }
            }
       }
       catch( e ) {
            println e.getMessage()
       }
       return resultado
    }

    private void gravarPontosPortalbio( Ficha ficha, List pontos )
    {
        FichaOcorrenciaPortalbio fop
        pontos.each { item ->

            if (item.geometry )
            {
                item.geometry.SRID = 4674
                fop = FichaOcorrenciaPortalbio.findByFichaAndGeometryAndNoAutor(ficha, item.geometry, item.noAutor.trim())
                if (!fop)
                {
                    fop = new FichaOcorrenciaPortalbio()
                    fop.ficha = ficha
                    fop.geometry = item.geometry
                }
                fop.deUuid = item.deUuid.toString().trim()
                if (item.noCientifico )
                {
                    fop.noCientifico    = item.noCientifico
                    fop.noLocal         = item.noLocal
                    fop.noInstituicao   = item.noInstituicao
                    fop.noAutor         = item.noAutor
                    fop.deAmeaca        = item.deAmeaca
                    if (item.dtOcorrencia)
                    {
                        fop.dtOcorrencia = item.dtOcorrencia
                    }
                    if (item.dtCarencia)
                    {
                        fop.dtCarencia = item.dtCarencia
                    }
                    fop.txOcorrencia = item.txOcorrencia
                    if (!fop.save())
                    {
                        println fop.getErrors()
                    }
                }
            }
        }
    }

    /**
     * exibir as pendências da ficha na janela modal
     */
    def getGridVisualizarPendencias() {

        if( !params.sqFicha ) {
            render 'Ficha não informada'
            return
        }
        List listPendencias = FichaPendencia.createCriteria().list {
            eq('ficha.id',params.sqFicha.toLong())
            eq('stPendente', 'S')
            order('dtPendencia')
        }
        if( listPendencias ) {
            String nmCientifico = Util.ncItalico( listPendencias[0].vwFicha.nmCientifico, true)
            String arvoreTaxonomica = listPendencias[0].vwFicha.taxon.hierarchy.join(' &rarr; ')
            render(template: "/ficha/divGridVisualizarPendencias", model: [listPendencias: listPendencias
                                        ,arvoreTaxonomica:arvoreTaxonomica
                                        , nmCientifico: nmCientifico])
        } else {
            render '<h3>Não existe pendência</h3>'
        }

    }
    /**
     * Alterar a situção de uma pendencia para S ou N, se tiver S ficha N, se tiver N fica S
     */
    def updateSituacaoPendencia(){
        Map res = [status:0,msg:'Dados gravados com SUCESSO!',type:'success', data:[stPendente:'S',log : '' ] ]
        try{

            if( !params.sqFicha ){
                throw new Exception('Ficha não informada')
            }
            if( !params.sqFichaPendencia ){
                throw new Exception('Pendência não informada')
            }
            //def ficha = Ficha.executeQuery("select new map(f.id as sqFicha) from Ficha f where f.id = :id",[id:params.sqFicha.toLong()])
            FichaPendencia fichaPendencia = FichaPendencia.createCriteria().get {
                createAlias('vwFicha', 'vw')
                eq('vw.id', params.sqFicha.toLong() )
                eq('id',params.sqFichaPendencia.toLong() )
            }
            if( !fichaPendencia ){
                throw new Exception('Pendência não encontrada')
            }
            if( fichaPendencia.dtRevisao ){
                throw new Exception('Pendência já está revisada!')
            }
            fichaPendencia.stPendente = fichaPendencia.stPendente == 'S' ? 'N' : 'S'
            if( fichaPendencia.stPendente == 'N' ) {
                fichaPendencia.usuarioResolveu = PessoaFisica.get( session.sicae.user.sqPessoa.toLong() )
                fichaPendencia.dtResolvida = new Date()
            } else {
                fichaPendencia.usuarioResolveu = null
                fichaPendencia.dtResolvida = null
            }
            fichaPendencia.usuarioRevisou = null
            fichaPendencia.dtRevisao = null
            fichaPendencia.save()
            res.data.stPendente = fichaPendencia.stPendente
            res.data.log        = fichaPendencia.logRevisao()

            /*res.data.resolvidaPor = ( fichaPendencia.usuarioResolveu ? 'Resolvida por ' + Util.nomeAbreviado(fichaPendencia.usuarioResolveu.noPessoa)+
                ' em ' + fichaPendencia.dtResolvida.format('dd/MM/yyyy') : '')
            res.data.revisadaPor = ( fichaPendencia.usuarioRevisou ? 'Resolvida por ' + Util.nomeAbreviado(fichaPendencia.usuarioRevisou.noPessoa)+
                ' em ' + fichaPendencia.dtRevisao.format('dd/MM/yyyy') : '')
*/
        } catch( Exception e ){
            res.status  = 1
            res.type    ='error'
            res.msg     = e.getMessage()
            println e.getMessage()
        }
        render res as JSON
    }

    /**
     * receber uma lista de ids de fichas e verificar se existe pelo menos um registro de ocorrencia sensivel
     */
    def hasSensitiveOccurrence() {
        Map res = [status:1,msg:'',type:'error',data:[result:false]]
        String cmdSql
        // TODO - adicionar o ciclo de avaliação nesta requisição para utilizar o index da tabela
        //String whereCicloAvaliacao = ( params.sqCicloAvaliacao ? ' and ficha.sq_ciclo_avaliacao = '+params.sqCicloAvaliacao :'')
        DadosApoio contextoOcorrencia = dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA','REGISTRO_OCORRENCIA')
        try {
            if( ! params.ids && !params.dataTempFile ) {
                throw  new Exception('Lista de fichas não foi informado.')
            }
            if( params.ids ) {
                if( params.idsType == 'ficha' ) {
                    cmdSql = """select sq_ficha_ocorrencia from salve.ficha_ocorrencia where sq_contexto = ${contextoOcorrencia.id.toString()} and in_sensivel = 'S' AND ficha_ocorrencia.sq_ficha in (${params.ids}) limit 1"""
                }
            } else if( params.dataTempFile ) {
                String tempDir = grailsApplication.config.temp.dir
                String fileName = tempDir+'/'+params.dataTempFile
                fileName = fileName.replaceAll(/\/\/rel/,'/rel')
                File file = new File( fileName )
                if( !file.exists() ) {
                    throw  new Exception('Arquivo temporário não existe: ' + params.dataTempFile )
                }
                Map relData = JSON.parse( file.text )
                if( relData?.ids?.size() == 0 ){
                    throw  new Exception('Lista de ids não encontrada no arquivo temporário: ' + params.dataTempFile )
                }
                if( params.idsType == 'taxon' ) {
                    cmdSql = """select a.sq_ficha_ocorrencia from salve.ficha_ocorrencia a inner join salve.ficha on ficha.sq_ficha = a.sq_ficha where a.sq_contexto = ${contextoOcorrencia.id.toString()} and a.in_sensivel = 'S' AND ficha.sq_taxon in (${relData.ids.join(',')}) limit 1"""
                } else {
                    cmdSql = """select a.sq_ficha_ocorrencia from salve.ficha_ocorrencia a where a.sq_contexto = ${contextoOcorrencia.id.toString()} and a.in_sensivel = 'S' AND a.sq_ficha in (${relData.ids.join(',')}) limit 1"""
                }
            }
            List rows = []
            if( cmdSql ) {
                rows = sqlService.execSql(cmdSql)
            }
            res.status = 0
            res.type='success'
            res.data.result =  ( rows && rows.size() == 1 )
        } catch( Exception e ) {
            res.msg = e.getMessage(  )
        }
        render res as JSON
    }

    /**
     * receber uma lista de ids de ocorrencias e transferir para a ficha selecionada
     */
    def transferirRegistro() {
        Map res = [status:1,msg:'',type:'error',data:[result:false]]
        String noCientificoDestino
        String setUtilizado = ''
        List msgAvisos      = []

        try {
            // session.setAttribute( 'gravarUtilizadoAvaliacao', 'FICHA TESTE' )
            // session.removeAttribute( 'gravarUtilizadoAvaliacao')

            /*if( session.getAttribute( 'gravarUtilizadoAvaliacao' ) ) {
                throw new Exception('Estados, municípios, bacias e biomas da ficha <b>' +
                    session.getAttribute( 'gravarUtilizadoAvaliacao' ) +
                    '</b> estão sendo atualizados.<br><br>Aguarde o término para realizar a transferência.')
            }*/

            if ( ! params.idRegistros) {
                throw new Exception('Nenhum registro informado')

            }
            if ( ! params.sqFichaOrigem ) {
                throw new Exception('Ficha de origem não informada')
            }
            if ( ! params.sqFichaOrigem ) {
                throw new Exception('Ficha de destino não informada')
            }
            DadosApoio situacaoAvaliacao = dadosApoioService.getByCodigo('TB_SITUACAO_REGISTRO_OCORRENCIA','REGISTRO_UTILIZADO_AVALIACAO')
            if( params.inMarcarUtilizado == 'S' ){
                setUtilizado = ", in_utilizado_avaliacao='S', sq_situacao_avaliacao = ${situacaoAvaliacao.id.toString()}"
            }
            Ficha fichaOrigem = Ficha.get( params.sqFichaOrigem.toLong() )
            Ficha fichaDestino = Ficha.get( params.sqFichaDestino.toLong() )
            if( !fichaOrigem.canModify( session.sicae.user) ) {
                throw new Exception('Ficha '+fichaOrigem.nmCientifico+' não pode ser alterada')
            }
            if( !fichaDestino.canModify( session.sicae.user) ) {
                throw new Exception('Ficha '+fichaOrigem.nmCientifico+' não pode ser alterada')
            }
            noCientificoDestino = fichaDestino.taxon.noCientifico

            List dados = JSON.parse(params.idRegistros) as List
            List idsSalve = []
            List idsPortalbio= []
            dados.each{ item ->
                if( item.bd.toLowerCase()=='salve'){
                    idsSalve.push( item.id.toLong() )
                } else {
                    idsPortalbio.push( item.id.toLong() )
                }
            }

            String cmdSql
            // String txTransferencia = '\n--\nRegistro transferido da ficha '+fichaDestino.nmCientifico
            if( idsSalve ) {

                // ler os pontos que são centroide e não permitir se a ficha destino já possuir um centroide no mesmo Estado
                cmdSql = """with passo1 as (
                            select fo.sq_ficha_ocorrencia, fo.sq_estado, fo.sq_metodo_aproximacao, fo.sq_ref_aproximacao
                            from salve.ficha_ocorrencia fo
                                     inner join salve.dados_apoio as refAprox on refAprox.sq_dados_apoio = fo.sq_ref_aproximacao
                                     inner join salve.dados_apoio as metodo on metodo.sq_dados_apoio = fo.sq_metodo_aproximacao
                            where fo.sq_ficha_ocorrencia in (${idsSalve.join(',')})
                              and refAprox.cd_sistema = 'ESTADO'
                              and metodo.cd_sistema = 'CENTROIDE'
                        )
                        select sq_ficha_ocorrencia, uf.no_estado
                        from passo1
                        inner join salve.vw_estado uf on uf.sq_estado = passo1.sq_estado
                        where exists ( select null from salve.ficha_ocorrencia fo2
                            where fo2.sq_ficha = ${fichaDestino.id.toString()}
                            and fo2.sq_estado = passo1.sq_estado
                            and fo2.sq_metodo_aproximacao = passo1.sq_metodo_aproximacao
                            and fo2.sq_ref_aproximacao = passo1.sq_ref_aproximacao
                            limit 1
                            )
                        """
// estado
                sqlService.execSql(cmdSql).each { row ->
                    idsSalve -= row.sq_ficha_ocorrencia.toLong()
                    msgAvisos.push('Ficha destino já possui centroide no Estado ' + row.no_estado +'.')
                }

                if (idsSalve) {
                    cmdSql = """update salve.ficha_ocorrencia set sq_ficha = ${fichaDestino.id.toString()}
                ${setUtilizado}
                where sq_ficha = ${fichaOrigem.id.toString()}
                  -- and ( in_utilizado_avaliacao is null or in_utilizado_avaliacao = 'N' )
                  and sq_ficha_ocorrencia in (${idsSalve.join(',')})
                  """
/** /
 println ' '
 println 'TRANSFERINDO REGISTROS DO SALVE'
 println cmdSql
 /**/
                    sqlService.execSql(cmdSql)
                    // atualizar as referencias
                    cmdSql = """update salve.ficha_ref_bib set sq_ficha = ${fichaDestino.id.toString()} where sq_ficha = ${fichaOrigem.id.toString()}
                    and no_tabela = 'ficha_ocorrencia'
                    and no_coluna = 'sq_ficha_ocorrencia'
                    and sq_registro in (${idsSalve.join(',')})
                    and exists (
                        select null from salve.ficha_ocorrencia x
                        where x.sq_ficha = ${fichaDestino.id.toString()}
                        and x.sq_ficha_ocorrencia = ficha_ref_bib.sq_registro
                        limit 1
                    )"""
                    sqlService.execSql(cmdSql)
                }
            }

            if( idsPortalbio ) {
                //println 'ids portalbio:' + idsPortalbio.join(',')
                    // excluir os registros da origem que já existirem no destino
                    cmdSql = """select sq_ficha_ocorrencia_portalbio, de_uuid from salve.ficha_ocorrencia_portalbio
                      where sq_ficha = ${fichaOrigem.id.toString()}
                      and sq_ficha_ocorrencia_portalbio in (${idsPortalbio.join(',')})
                      and ( EXISTS (
                              select null
                              from salve.ficha_ocorrencia_portalbio x
                              where x.sq_ficha = ${fichaDestino.id.toString()}
                                and x.de_uuid = ficha_ocorrencia_portalbio.de_uuid
                              limit 1 )
                              OR EXISTS (
                              select null from salve.ficha_ocorrencia_portalbio x
                              where x.sq_ficha = ${fichaDestino.id.toString()}
                              and x.no_autor = ficha_ocorrencia_portalbio.no_autor
                              and x.dt_ocorrencia = ficha_ocorrencia_portalbio.dt_ocorrencia
                              and x.ge_ocorrencia = ficha_ocorrencia_portalbio.ge_ocorrencia
                              limit 1)
                          )
                        """
                /*println ' '
                println 'TRANSFERINDO REGISTROS DO PORTALBIOE'
                println 'QUERY DOS REGISTRO QUE JA EXISTEM NA FICHA DESTINO'
                println cmdSql*/


                    List rows = sqlService.execSql(cmdSql)
                    rows.each {
                        idsPortalbio = [] // cancelar a transferência
                        FichaOcorrenciaPortalbio fo = FichaOcorrenciaPortalbio.get(it.sq_ficha_ocorrencia_portalbio.toLong() )
                        if (fo) {

                            // ATUALIZAR O DE_UUID da ocorrencia que já existe na ficha destino se não estiver no formado: ICMBIO/SIBIO/SISBIO/ID:xxxxxx
                            // porque ainda pode estar no formato uuid original do portalbio:xya0q-aapqpaaçaçpa-qla
                            FichaOcorrenciaPortalbio.executeQuery("""from FichaOcorrenciaPortalbio fo where fo.ficha.id=:sqFicha
                                and fo.noAutor = :noAutor
                                and fo.dtOcorrencia = :dtOcorrencia
                                and fo.geometry = :geometry
                                and not fo.deUuid like 'ICMBIO/DIBIO/SISBIO/ID%'""",[sqFicha:fichaDestino.id, noAutor:fo.noAutor, dtOcorrencia:fo.dtOcorrencia, geometry:fo.geometry],[max:1]).each{ rowFo ->
                                //println 'ALTERAR UUID DE ' + rowFo.id + ' '+rowFo.deUuid
                                rowFo.deUuid = it.de_uuid
                                rowFo.save()
                            }
                            fo.delete(flush: true)
                            /** /
                            println it.de_uuid
                            println it.sq_ficha_ocorrencia_portalbio + ' EXCLUIDO'
                            println '-----------------------'
                            /**/
                        }
                        //throw new Exception( 'Registro já existe na ficha destino.<br> UUID: ' + rows[0].de_uuid)
                    }
                // ,tx_nao_utilizado_avaliacao = case when position('${txTransferencia}' in tx_nao_utilizado_avaliacao) > 0 then tx_nao_utilizado_avaliacao else trim( concat( tx_nao_utilizado_avaliacao,'${txTransferencia}') ) end
                // ,tx_nao_utilizado_avaliacao = trim( concat( tx_nao_utilizado_avaliacao,'${txTransferencia}') )

                /*
                // criar lista dos registros que serão transferidos para depois recria-los como transferido na ficha origem
                cmdSql = """select sq_ficha_ocorrencia_portalbio, sq_ficha from salve.ficha_ocorrencia_portalbio
                    where sq_ficha = ${fichaOrigem.id.toString()}
                      and ( in_utilizado_avaliacao is null or in_utilizado_avaliacao = 'N' )
                      and sq_ficha_ocorrencia_portalbio in (${idsPortalbio.join(',')})
                      and de_uuid not in (
                          select x.de_uuid
                          from salve.ficha_ocorrencia_portalbio x
                          where x.sq_ficha = ${fichaDestino.id.toString()}
                          limit 1)
                      and not exists (
                          select null from salve.ficha_ocorrencia_portalbio x
                          where x.sq_ficha = ${fichaDestino.id.toString()}
                          and x.no_autor = ficha_ocorrencia_portalbio.no_autor
                          and x.dt_ocorrencia = ficha_ocorrencia_portalbio.dt_ocorrencia
                          and x.ge_ocorrencia = ficha_ocorrencia_portalbio.ge_ocorrencia
                          limit 1)"""

                List registrosTransferidos= sqlService.execSql(cmdSql);
                println ' '
                println 'LISTA DE REGISTROS TRANSFERIDOS'
                println cmdSql
                registrosTransferidos.each {
                    println it.sq_ficha_ocorrencia_portalbio
                }*/
                if( idsPortalbio ) {
                    cmdSql = """update salve.ficha_ocorrencia_portalbio set sq_ficha = ${fichaDestino.id.toString()}
                    ${setUtilizado}
                    , no_cientifico='${noCientificoDestino}'
                    where sq_ficha = ${fichaOrigem.id.toString()}
                      -- and ( in_utilizado_avaliacao is null or in_utilizado_avaliacao = 'N' )
                      and sq_ficha_ocorrencia_portalbio in (${idsPortalbio.join(',')})
                      and de_uuid not in (
                          select x.de_uuid
                          from salve.ficha_ocorrencia_portalbio x
                          where x.sq_ficha = ${fichaDestino.id.toString()}
                          limit 1)
                      and not exists (
                          select null from salve.ficha_ocorrencia_portalbio x
                          where x.sq_ficha = ${fichaDestino.id.toString()}
                          and x.no_autor = ficha_ocorrencia_portalbio.no_autor
                          and x.dt_ocorrencia = ficha_ocorrencia_portalbio.dt_ocorrencia
                          and x.ge_ocorrencia = ficha_ocorrencia_portalbio.ge_ocorrencia
                          limit 1)"""

                    if (sqlService.execSql(cmdSql) == null) {
                        throw new Exception('Ocorreu um erro na operação.')
                    }
                    cmdSql = """update salve.ficha_ref_bib set sq_ficha = ${fichaDestino.id.toString()} where sq_ficha = ${fichaOrigem.id.toString()}
                    and no_tabela = 'ficha_ocorrencia_portalbio'
                    and no_coluna = 'sq_ficha_ocorrencia_portalbio'
                    and sq_registro in (${idsPortalbio.join(',')})
                    and exists (
                        select null from salve.ficha_ocorrencia_portalbio x
                        where x.sq_ficha = ${fichaDestino.id.toString()}
                        and x.sq_ficha_ocorrencia_portalbio = ficha_ref_bib.sq_registro
                        limit 1
                    )"""

                    if (sqlService.execSql(cmdSql) == null) {
                        throw new Exception('Ocorreu um erro na operação.')
                    }
                }
            }

            res.status = 0
            res.type='success'
            res.msg = 'Registro(s) transferído(s) com SUCESSO'
            if( msgAvisos ) {
                if( dados.size() == 1 ) {
                    res.msg     = msgAvisos.join('<br>')
                    res.type    = 'error'
                    res.status = 1
                } else {
                    res.type='modal'
                    res.msg = 'Registros transferídos com SUCESSO com a(s) seguinte(s) restrição(ões):<br><span class="red">'+msgAvisos.join('<br>') + '</span>'
                }
            }
        } catch( Exception e ){
            res.msg = e.getMessage()
            println e.getMessage()
        }
        render res as JSON
    }

    def creditos() {
        if( !params.sqFicha ){
            render 'Id da ficha não informado'
            return;
        }
        List rows = Ficha.executeQuery("""select new map(dsCitacao as dsAutoria, dsColaboradores as dsColaboradores, dsEquipeTecnica as dsEquipeTecnica) from Ficha f where f.id = :sqFicha""",[sqFicha:params.sqFicha.toLong()])
        render( template:"fichaCreditos/index", model:[row:rows[0] ])
    }

    def getLocaisRefBibUtilizada (){
        Map res = [ status:1, msg:'',type:'error',data:[]]
        try {
            if (! params.sqFicha) {
                throw  new Exception( 'Id da ficha não informado')
                return;
            }

            if (! params.sqPublicacao) {
                throw  new Exception( 'Id da referência bibliográfica não informado')
                return;
            }

            String cmdSql = """select distinct de_rotulo from salve.ficha_ref_bib
            where sq_ficha = :sqFicha
              and sq_publicacao = :sqPublicacao
              and de_rotulo is not null
              and lower(de_rotulo) not in('ficha','ocorrencia','ocorrência')
              order by de_rotulo"""
              res.data = sqlService.execSql(cmdSql, [sqFicha:params.sqFicha.toLong(), sqPublicacao:params.sqPublicacao.toLong()])
              res.status=0;
        } catch ( Exception e ){
            res.status=1
            res.msg = e.getMessage()

        }
        render res as JSON
    }

    def getUltimasReferencias() {

        Map res = [status: 1, msg: '', type: 'error', data: []]
        try {
            if (!params.sqFicha) {
                throw new Exception('Id da ficha não informado')
                return;
            }
            String cmdSql = """with cte as (
                    select sq_ficha_ref_bib, sq_publicacao
                    from salve.ficha_ref_bib
                    where sq_ficha = :sqFicha
                      and sq_publicacao is not null
                    order by sq_ficha_ref_bib desc
                    limit 30
                )
                select distinct publicacao.sq_publicacao, salve.remover_html_tags(concat(publicacao.de_titulo,
                    case when publicacao.no_autor is null then '' else ' / ' || publicacao.no_autor end,
                    case when publicacao.nu_ano_publicacao is null then '' else ' / ' || publicacao.nu_ano_publicacao end
                    ))
                as de_titulo_autor_ano
                from cte
                inner join taxonomia.publicacao on publicacao.sq_publicacao = cte.sq_publicacao
                limit 5
                """
            List rows = sqlService.execSql( cmdSql,[ sqFicha:params.sqFicha.toLong() ])
            res.status=0
            res.data=rows
        } catch (Exception e) {
            res.status = 1
            res.msg = e.getMessage()

        }
        render res as JSON
    }


    def marcarDesmarcarModuloPublico(){

        Map res = [status: 1, msg: '', type: 'error']

        try {
            Long sqPessoa = session.sicae.user.sqPessoa.toLong()
            boolean stExibir = params.stExibir.toBoolean() ? true : false
            if ( ! params.sqFicha ) {
                throw new Exception('Id da ficha não informado')
                return;
            }
            fichaService.marcarDesmarcarRegistroExibirModuloPublico(params.sqFicha, stExibir, sqPessoa)
            /*
            String cmdSql = """select sq_ficha from salve.ficha_modulo_publico where sq_ficha in (${params.sqFicha})"""
            List rows = sqlService.execSql( cmdSql )
            List sqlsExecutar = []
            params.sqFicha.split(',').each { sq_ficha ->
                if( ! rows.sq_ficha.contains( sq_ficha.toLong() ) ) {
                  sqlsExecutar.push("insert into salve.ficha_modulo_publico (sq_ficha,sq_pessoa,st_exibir) values (${sq_ficha}, ${sq_pessoa},true)")
                } else {
                  sqlsExecutar.push("update salve.ficha_modulo_publico set st_exibir = ${stExibir},sq_pessoa = ${sq_pessoa} where sq_ficha = ${sq_ficha}")
                }
            }
            if( sqlsExecutar ){
                sqlService.execSql( sqlsExecutar.join(';\n') )
            }*/
            res.status=0
        } catch (Exception e) {
            res.status = 1
            res.msg = e.getMessage()
        }
        render res as JSON
    }

    /**
     * criar o gride com as versões da ficha
     * @return
     */
    def versoes() {
        if( !params.sqFicha ){
            render 'Id da ficha não informado'
            return;
        }
        List rows = fichaService.getRowsGridVersoes(params.sqFicha.toLong())
        Boolean canModify = session.sicae.user.isADM()
        render( template:"versoes/index", model:[ rows:rows,canModify:canModify ])
    }

    /**
     * metodo para tornar uma versão da ficha json ( versionada ) para o módulo público
     * @return
     */
    def publicarVersao(){
        Map res = [status:0,msg:'',type:'success', data:[sqFichaVersao:0,stPublico:false]]
        try {

            if( ! session.sicae.user.isADM() ){
                throw new Exception('Sem permissão')
            }

            FichaVersao fv  = FichaVersao.get(params.sqFichaJson.toLong() )
            if( !fv ){
                throw new Exception('Id versão da ficha nao encontrado')
            }
            fv.stPublico = !fv.stPublico
            fv.save()
            if( fv.stPublico ){
                res.msg='Versão publicada com SUCESSO'
            } else {
                res.msg='Publicação REMOVIDA com SUCESSO'
            }
            res.data.stPublico = fv.stPublico
            res.data.sqFichaVersao = fv.id

            // remover a publicacao anterior caso uma nova versão fique publicada
            if( fv.stPublico ){
                FichaVersao.executeUpdate("""update FichaVersao fv set fv.stPublico = false where fv.ficha.id = :sqFicha and fv.id <> :id""",[sqFicha:fv.ficha.id,id:fv.id])
            }
            /*
            List rows = sqlService.execSql("select sq_ficha from salve.ficha_versao where sq_ficha_versao = :sqFichaJson",[sqFichaJson:params.sqFichaJson.toLong()])
            if( ! rows ){
                throw new Exception('Id versão da ficha nao encontrado')
            }
            Long sqFicha = rows[0].sq_ficha.toLong()
            sqlService.execSql("""update salve.ficha_versao set st_publico = (sq_ficha_versao = ${params.sqFichaJson})::boolean
                    where sq_ficha = :sqFicha
                      and st_publico::boolean <> (sq_ficha_versao = ${params.sqFichaJson})::boolean
                      """, [sqFicha:sqFicha] )*/


        } catch( Exception e ){
            res.status = 1
            res.type   = 'error'
            res.msg    = e.getMessage()
        }
        render res as JSON
    }
}
