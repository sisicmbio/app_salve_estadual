package br.gov.icmbio
import grails.converters.JSON

class HabitatController extends BaseController {

    def dadosApoioService
    def sqlService

    protected List _getData(Long idPai = null ){
        String cmdSql = """select distinct a.sq_dados_apoio
                    , a.sq_dados_apoio_pai
                    , a.cd_dados_apoio
                    , a.ds_dados_apoio
                    , a.de_dados_apoio_ordem
                    , c.cd_iucn_habitat
                    , c.ds_iucn_habitat
                    , filhos.nu_filhos
                    from salve.dados_apoio a
                    left join salve.iucn_habitat_apoio b on b.sq_dados_apoio = a.sq_dados_apoio
                    left join salve.iucn_habitat c on c.sq_iucn_habitat = b.sq_iucn_habitat
                    left join lateral (
                        select count(*) as nu_filhos from salve.dados_apoio x where x.sq_dados_apoio_pai = a.sq_dados_apoio
                    ) as filhos on true
                    where a.sq_dados_apoio_pai = :sqPai
                    order by a.de_dados_apoio_ordem, a.ds_dados_apoio"""
        Map sqlParams = [:]
        if( ! idPai )
        {
            DadosApoio pai = dadosApoioService.getByCodigo('TB_HABITAT')
            sqlParams.sqPai = pai.id
        } else {
            sqlParams.sqPai = idPai
        }
        return sqlService.execSql(cmdSql, sqlParams)
    }

    def index() {
        List data = _getData()
        Map pagination = ['totalRecords': data.size(), 'pageCurrent': 1,'pageSize':50]
        this._pagination(pagination)
        render (view: 'index', model:['data': data, 'pagination': pagination])
    }


    def getTreeView() {
        response.setHeader("Content-Type", "application/json")
        response.setHeader("Access-Control-Allow-Origin","*" )
        List data = _getData( params.key ? params.key.toLong() : null )
        Map result=['status':'ok','result':[]];
        data.each {
            String codigoApoio = it.cd_dados_apoio && (it.cd_dados_apoio.toString() =~ /^[0-9]/) ? it.cd_dados_apoio : ''
                result['result'].push([
                    'key'         : it.sq_dados_apoio
                    ,'pai'        : (it.sq_dados_apoio_pai ?: 0 )
                    ,'title'      :  ( codigoApoio ? codigoApoio + ' ' : '' ) + it.ds_dados_apoio
                    ,'iucnHabitat' : ( it.cd_iucn_habitat ?  it.cd_iucn_habitat + ' '+ it.ds_iucn_habitat : '' )
                    // campos controle da fancytree
                    ,'select'   : false
                    ,'expanded' : false
                    ,'folder'   : (it?.nu_filhos > 0)
                    ,'lazy'     : (it?.nu_filhos > 0)
                    ] );
            }
            sleep(500)
        render result as JSON;
    }

    def getForm()
    {
        DadosApoio habitat
        IucnHabitatApoio iucnHabitatApoio
        if( params.id && params.operacao == 'habitat.edit' )
        {
            habitat = DadosApoio.get( params.id)
            iucnHabitatApoio = IucnHabitatApoio.findByHabitat( habitat )
        }
        else
        {
            habitat = new DadosApoio();
            iucnHabitatApoio = new IucnHabitatApoio()
        }
        List listCriteriosHabitat = dadosApoioService.getTable('TB_HABITAT').sort{it.ordem }
        List listIucnHabitat = IucnHabitat.findAll()

        render( template:'frmHabitat',model:[ habitat:habitat
                                             ,listCriteriosHabitat:listCriteriosHabitat
                                             ,listIucnHabitat:listIucnHabitat
                                             ,iucnHabitatApoio:iucnHabitatApoio]);
    }

    def save(){
        DadosApoio dadosApoio
        if( params.operacao == 'habitat.edit')
        {
            dadosApoio = DadosApoio.get(params.id);
        }
        else if( params.operacao == 'habitat.add')
        {
            dadosApoio = new DadosApoio()
            if( ! params.id ) {
                dadosApoio.pai = dadosApoioService.getByCodigo('TB_HABITAT')
            } else {
                dadosApoio.pai = DadosApoio.get( params.id)
            }
        }

        String codigoSistema = params.codigoSistema ?:null
        if( codigoSistema ){
            codigoSistema = Util.calcularCodigoSistema( codigoSistema )
        } else {
            codigoSistema = Util.calcularCodigoSistema( params.descricao )
        }
        dadosApoio.codigo             = params.codigo
        dadosApoio.codigoSistema      = codigoSistema
        dadosApoio.ordem              = Util.formatarCodigoOrdem(params.codigo)
        dadosApoio.descricao          = params.descricao
        dadosApoio.save( flush:true )

        // adicionar registro na tabela iucn_habitat_apoio
        IucnHabitatApoio iucnHabitatApoio = IucnHabitatApoio.findByHabitat( dadosApoio )
        if( params.sqIucnHabitat) {
            //habitat.iucnUso = IucnUso.get( params.sqIucnUso.toLong() )
            IucnHabitat iucnHabitat = IucnHabitat.get( params.sqIucnHabitat )
            if( ! iucnHabitatApoio ){
                iucnHabitatApoio = new IucnHabitatApoio()
                iucnHabitatApoio.habitat =  dadosApoio
            }
            iucnHabitatApoio.iucnHabitat = iucnHabitat
            iucnHabitatApoio.save( flush:true )
        } else if( iucnHabitatApoio )  {
            iucnHabitatApoio.delete(flush: true)
        }
        return this._returnAjax(0,null,null,[id:dadosApoio.id
                                             , codigoSistema:dadosApoio.codigoSistema
                                             , ordem:dadosApoio.ordem ]);
    }

    def delete()
    {
        if( params.id)
        {
            DadosApoio  dadosApoio  = DadosApoio.get(params.id)
            if( dadosApoio ) {
                IucnHabitatApoio iucnHabitatApoio = IucnHabitatApoio.findByHabitat( dadosApoio )
                if( iucnHabitatApoio ) {
                    iucnHabitatApoio.delete(flush:true)
                }
                dadosApoio.delete( flush:true )
            }
            return this._returnAjax(0);
        }
        return this._returnAjax(1, 'Id não informado.', "error")
    }

}
