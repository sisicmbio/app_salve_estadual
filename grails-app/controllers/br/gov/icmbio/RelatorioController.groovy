package br.gov.icmbio

import grails.converters.JSON
import groovy.sql.Sql
import org.codehaus.groovy.grails.web.json.JSONObject

import java.util.regex.Pattern


class RelatorioController extends BaseController {

    int maxFichasExportarOcorrencias = 5000
    int maxFichasExportarPlanilha    = 30000
    def dadosApoioService
    def dataSource
    def sqlService
    def exportDataService
    def workerService
    def emailService
    def fichaService
    def userJobService

    // todas as consultas que demorarem mais de 40s o sql será enviado para o email abaixo
    Map debugSql = [email:'luis.barbosa.terceirizado@icmbio.gov.br',seconds:40]

    /**
     * método padrão para exportar relatórios no formato csv
     * @param fichas
     * @return
     */
    private String exportCsv(List columns, List rows, String name = 'rel') {

        String path = grailsApplication.config.temp.dir
        List valores = []
        List lines = [columns.title.join(';')]
        rows.each { row ->
            valores = []
            columns.each { column ->
                valores.push(row[column.fieldName])
            }
            lines.push(valores.join(';'))
        }
        String hora = new Date().format('dd_MM_yyyy_hh_mm_ss')
        String fileName = path + 'salve_' + name + '_' + hora + '.csv'
        File file = new File(fileName)
        lines.each { String line ->
            file.append("${line}\n")
        }
        return fileName
    }

    private String _gridLongText( String texto = '',String delimiter='', Boolean justify=true ) {
        String novoTexto = Util.html2str( texto )
        String className = (justify ? 'grid-long-text-justify' : 'grid-long-text')
        if( ! novoTexto  ) {
            return novoTexto
        }
        if( delimiter ) {
            novoTexto = novoTexto.split(delimiter).collect{it.trim().replaceAll(/^- ?/,'')  }.join('<br>')
        }

        if( texto && texto.trim().size() > 40 ) {
            novoTexto = '<div class="'+className+'">'+novoTexto+'</div>'
        }
        return novoTexto
    }

    def select2Subgrupo() {
        Map data = ['items': []];
        if ( ! params['sqGrupoFiltro'] ) {
            render '';
            return;
        } else {
            params.sqGrupoFiltro = params['sqGrupoFiltro'].split(',').collect { it.toLong() }
        }

        String q = '%' + params.q.trim() + '%';
        List lista = DadosApoio.createCriteria().list {
            pai {
                'in'('id', params.sqGrupoFiltro )
                order('descricao')
            }
            ilike('descricao',q)
            order('descricao')
        }
        if (lista) {
            lista.each {
                data.items.push(['id': it.id, 'descricao': it.descricao,'txHint':it.pai.descricao]);
            }
            render data as JSON
            return;
        }
        render '';
    }

    def index() {}
    //****************************************************************************************
    //****************************************************************************************
    // métodos gerais

    def select2Oficina() {
        Map data = ['items': []]
        Oficina.createCriteria().list {
            if (params.sqCicloAvaliacaoFiltro) {
                eq('cicloAvaliacao.id', params.sqCicloAvaliacaoFiltro.toLong())
            }
            order('sgOficina')
        }.each {
            data.items.push(['id'       : it.id,
                             'txHint'   : it.situacao.descricao,
                             'descricao': Util.capitalize(it.sgOficina) + ' ' + it.periodo
            ])
        }
        render data as JSON
    }

    def select2UnidadeOrg() {

        Map data = ['items': []]
        List lista = VwFicha.createCriteria().list() {
            //eq('sqCicloAvaliacao', params.sqCicloAvaliacao.toInteger() )
            if( params.q )
            {
                ilike('sgUnidadeOrg', '%' + params.q.trim() + '%')
            }
        }.unique { it.sqUnidadeOrg }.sort { it.sgUnidadeOrg }
        if ( lista ) {
            lista.each {
                data.items.push(['id': it.sqUnidadeOrg, 'descricao': it.sgUnidadeOrg])
            }
            render data as JSON
            return
        }
        render ''
    }

    /**
     * metodo para retornar os campos que poderão ser utilizados para filtrar o relatório
     * @return
     */
    def getCamposFiltro() {
        if (!params.sqCicloAvaliacao) {
            render ''
            return
        }
        params.idRel = params.idRel ?: 'rel001';
        /** /
         println ' '
         println 'GET CAMPOS FILTRO'
         println params
         /**/

        CicloAvaliacao ciclo = CicloAvaliacao.get(params.sqCicloAvaliacao.toLong())
        List listNivelTaxonomico = []
        List listOficinasValidacao = []
        List listOficinasAvaliacao = []
        List listSituacaoFicha = []
        List listGrupos = []
        if (params.idRel =~ /001/) {
            listNivelTaxonomico = NivelTaxonomico.list()
            DadosApoio oficinaAvaliacao = dadosApoioService.getByCodigo('TB_TIPO_OFICINA', 'OFICINA_AVALIACAO')
            listOficinasValidacao = Oficina.findAllByCicloAvaliacaoAndTipoOficina(ciclo, oficinaAvaliacao)
        } else if (params.idRel =~ /006/) {
            DadosApoio oficinaValidacao = dadosApoioService.getByCodigo('TB_TIPO_OFICINA', 'OFICINA_VALIDACAO')
            DadosApoio oficinaAvaliacao = dadosApoioService.getByCodigo('TB_TIPO_OFICINA', 'OFICINA_AVALIACAO')
            listOficinasValidacao = Oficina.findAllByCicloAvaliacaoAndTipoOficina(ciclo, oficinaValidacao)
            listOficinasAvaliacao = Oficina.findAllByCicloAvaliacaoAndTipoOficina(ciclo, oficinaAvaliacao)
            listGrupos = dadosApoioService.getTable('TB_GRUPO')
            listSituacaoFicha = dadosApoioService.getTable('TB_SITUACAO_FICHA')
                .findAll { it.codigoSistema =~ /EM_VALIDACAO|VALIDADA|VALIDADA_EM_REVISAO/ }.sort { it.ordem }
        }

        render('template': 'filtros', model: [
            idRel: params.idRel,
            listNivelTaxonomico: listNivelTaxonomico,
            listOficinasValidacao: listOficinasValidacao,
            listOficinasAvaliacao: listOficinasAvaliacao,
            listGrupos:listGrupos,
            listSituacaoFicha:listSituacaoFicha
        ])
    }

    /**
     * relatório de pontos focais e coordenadores de taxon
     * @return
     */
    def rel001() {
        Map resultado = [:]

        // retornar o formulário se não for informado o ciclo de avaliação
        if (!params.sqCicloAvaliacao) {

            List listCicloAvaliacao = CicloAvaliacao.list(sort: 'nuAno', order: 'desc');
            //listOficinas = Oficina.findAllByCicloAvaliacao(ciclo)
            render('view': 'rel001', model: ['listCicloAvaliacao': listCicloAvaliacao])

        } else {
            CicloAvaliacao ciclo = CicloAvaliacao.get(params.sqCicloAvaliacao.toInteger())
            Map parametrosConsulta = [sqCicloAvaliacao: ciclo.id.toInteger()]

            // criar comando SQL
            String query = """select tmp.no_pessoa
                , sg_instituicao_pessoa
                , no_papel
                , case when in_ativo = 'S' then 'Ativo' ELSE 'Inativo' END as in_ativo
                , no_grupo
                , sg_instituicao_ficha
                , tx_emails
                , count(*) as nu_fichas
                , array_to_string( array_agg(tmp.nm_cientifico),'<br>') AS nm_cientificos
                from (
                  select
                  pessoa.no_pessoa
                , coalesce( web_instituicao.sg_instituicao, unidade_org.sg_unidade_org) as sg_instituicao_pessoa
                , papel.ds_dados_apoio as no_papel
                , ficha_pessoa.in_ativo
                , grupo.ds_dados_apoio as no_grupo
                , unidade_org.sg_unidade_org as sg_instituicao_ficha
                , coalesce( email.tx_emails, ue.tx_email) as tx_emails
                , coalesce( taxon.json_trilha->'subespecie'->>'no_taxon', taxon.json_trilha->'especie'->>'no_taxon') as nm_cientifico
                from salve.ficha_pessoa
                inner join salve.ficha on ficha.sq_ficha = ficha_pessoa.sq_ficha
                inner join salve.vw_pessoa as pessoa on pessoa.sq_pessoa = ficha_pessoa.sq_pessoa
                inner join salve.vw_unidade_org as unidade_org on unidade_org.sq_pessoa = ficha.sq_unidade_org
                inner join salve.dados_apoio as papel on papel.sq_dados_apoio = ficha_pessoa.sq_papel
                inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                left join salve.web_instituicao on web_instituicao.sq_web_instituicao = ficha_pessoa.sq_web_instituicao
                left outer join salve.dados_apoio as grupo on grupo.sq_dados_apoio = ficha.sq_grupo
                left outer join sicae.vw_usuario_externo ue on ue.sq_usuario_externo = pessoa.sq_pessoa
                left join lateral ( select
                     array_to_string( array_agg( DISTINCT x.tx_email),'<br>') AS tx_emails
                     from salve.vw_email x where x.sq_pessoa = ficha_pessoa.sq_pessoa
                ) email on true
                """

            // se for passado o filtro pelo nivel taxonomico acrescentar join com a tabela taxon
            if (params.nivelFiltro && params.sqTaxonFiltro) {
                query += """\ninner join taxonomia.taxon as taxon on taxon.sq_taxon = ficha.sq_taxon"""
            }

            query += """\nwhere ficha.sq_ciclo_avaliacao = :sqCicloAvaliacao"""

            // filtrar pela função/papel
            if (params.sqPapelFiltro) {

                query += """\nand ficha_pessoa.sq_papel = :sqPapel"""
                parametrosConsulta.sqPapel = params.sqPapelFiltro.toLong()
            }

            // filtrar pela situação Ativo/Inativo
            if (params.sqSituacaoFiltro == 'S' || params.sqSituacaoFiltro == 'N') {
                query += """\nand ficha_pessoa.in_ativo = :inAtivo"""
                parametrosConsulta.inAtivo = params.sqSituacaoFiltro
            }

            // filtrar pela unidade selecionada
            if (params.sqUnidadeFiltro) {
                query += """\nand ficha.sq_unidade_org = :sgUnidadeOrg"""
                parametrosConsulta.sgUnidadeOrg = params.sqUnidadeFiltro.toLong()
            }

            // filtrar pela oficina selecionada
            if (params.sqOficinaFiltro) {

                // criar lista com os IDs das fichas da oficina selecionada
                List idsFichas = OficinaFicha.findAllByOficina(Oficina.get(params.sqOficinaFiltro.toInteger()))?.vwFicha?.id
                // evitar erro na clausula IN ( 0 )
                if (idsFichas.size() == 0) {
                    idsFichas.push(0l)
                }
                query += """\nand ficha.sq_ficha in ( ${idsFichas.join(',')} )"""

            }

            // filtrar pelo nivel taxonomico
            if (params.nivelFiltro && params.sqTaxonFiltro) {
                query += """\nand taxon.json_trilha->'${params.nivelFiltro.toLowerCase()}'->>'sq_taxon'='${params.sqTaxonFiltro}'"""
            }
            query += """\norder by pessoa.no_pessoa, ficha.nm_cientifico)
                tmp group by tmp.no_pessoa
                , tmp.sg_instituicao_pessoa
                , tmp.no_papel
                , tmp.in_ativo
                , tmp.no_grupo
                , tmp.sg_instituicao_ficha
                , tmp.tx_emails
                order by tmp.no_pessoa
                """
            /** /
             println ' '
             println ' '
             println query
             println ' '
             println ' '
             println ' '
             /**/

            List rows = sqlService.execSql(query, parametrosConsulta)
            rows.each {
                it.no_pessoa = Util.capitalize(it.no_pessoa)
                it.nm_cientificos = it.nm_cientificos ? it.nm_cientificos.split('<br>').collect { Util.ncItalico(it, true) }.join('<br>') : ''
            }
            if (params.formato == 'html') {
                render(template: 'grideRel001', model: [rows: rows])
            }
        }
    }

    //*******************************************************************************
    //*******************************************************************************
    //*******************************************************************************
    //*******************************************************************************
    /**
     * relatório de OFICINAS - rel002
     * @return
     */
    def rel002() {
        Map resultado = [:]
        Map qryParams = [:]
        List erros = []
        List rows = []

        // retornar o formulário se não for informado o ciclo de avaliação
        if (!params.print) {

            List listCicloAvaliacao = CicloAvaliacao.list(sort: 'nuAno', order: 'desc');
            //listOficinas = Oficina.findAllByCicloAvaliacao(ciclo)
            render('view': 'rel002', model: ['listCicloAvaliacao': listCicloAvaliacao])
            return
        }
        // validar filtros
        if (params.dtInicioFiltro) {
            try {
                qryParams.dtInicio = new Date().parse('dd/MM/yyyy', params.dtInicioFiltro).format('yyyy-MM-dd')
            } catch (Exception e) {
                erros.push('Data inicial inválida!')
            }
        }
        if (params.dtFimFiltro) {
            try {
                qryParams.dtFim = new Date().parse('dd/MM/yyyy', params.dtFimFiltro).format('yyyy-MM-dd')
            } catch (Exception e) {
                erros.push('Data final inválida!')
            }
        }
        if (qryParams.dtInicio && qryParams.dtFim && qryParams.dtInicio > qryParams.dtFim) {
            String dataInicio = qryParams.dtInicio
            qryParams.dtInicio = qryParams.dtFim
            qryParams.dtFim = dataInicio
        }
        // filtrar as oficinas da unidade do usuário logado
        if (!session?.sicae?.user?.isADM()) {
            qryParams.sqUnidadeOrg = session.sicae.user.sqUnidadeOrg.toLong()
        }

        if (!erros) {

            List where = []
            String sqlCmd = ''

            where = []
            // oficinas que ocorreram no período de...
            if (params.sqCicloAvaliacaoFiltro) {
                where.push("oficina.sq_ciclo_avaliacao = :sqCicloAvaliacao")
                qryParams.sqCicloAvaliacao = params.sqCicloAvaliacaoFiltro.toLong()

                // oficinas que ocorreram no período de...
                if (qryParams.dtInicio && qryParams.dtFim) {
                    where.push("( (oficina.dt_inicio between '" + qryParams.dtInicio + "' and '" + qryParams.dtFim + "') or ( oficina.dt_fim between '" + qryParams.dtInicio + "' and '" + qryParams.dtFim + "') )")
                    qryParams.remove('dtInicio')
                    qryParams.remove('dtFim')
                }
                // oficinas que iniciaram após...
                if (qryParams.dtInicio) {
                    where.push("oficina.dt_inicio >= '" + qryParams.dtInicio + "'")
                    qryParams.remove('dtInicio')
                }
                // oficinas que terminaram até ...
                if (qryParams.dtFim) {
                    where.push("oficina.dt_fim <= '" + qryParams.dtFim + "'")
                    qryParams.remove('dtFim')
                }
                // filtrar as oficinas da unidade organizacional do usuario logado

                if (qryParams.sqUnidadeOrg) {
                    where.push("oficina.sq_unidade_org = :sqUnidadeOrg")
                }


                if (params.formato == 'html') {
                    sqlCmd = """select tmp2.sq_oficina
            ,tmp2.sg_oficina
            --,tmp2.de_periodo
            ,tmp2.dt_inicio
            ,tmp2.dt_fim
            ,tmp2.de_fonte_recurso
            ,tmp2.de_local
            ,coalesce(tmp2.sg_unidade_org, tmp2.de_local ) as sg_unidade_org
            ,tmp2.ds_tipo_oficina
            ,tmp2.de_ciclo_avaliacao
            ,tmp2.json_participantes||'' as json_participantes
            ,tmp2.ds_situacao
            ,json_object_agg( COALESCE(grupo.ds_dados_apoio,''), json_build_object('cd_grupo_sistema',grupo.cd_sistema) )||'' as json_grupos
            --,json_object_agg( COALESCE(ficha.nm_cientifico,''), json_build_object('sq_ficha',ficha.sq_ficha, 'no_cientifico',ficha.nm_cientifico, 'cd_situacao_sistema',situacao_ficha_oficina.cd_sistema, 'no_grupo',grupo.ds_dados_apoio) )||'' as json_fichas
            ,json_object_agg( COALESCE(  coalesce( taxon.json_trilha->'subespecie'->>'no_taxon', taxon.json_trilha->'especie'->>'no_taxon'),''), json_build_object('sq_ficha',ficha.sq_ficha, 'no_cientifico', ficha.nm_cientifico, 'cd_situacao_sistema',situacao_ficha_oficina.cd_sistema, 'no_grupo',grupo.ds_dados_apoio) )||'' as json_fichas

            -- ler os participantes
             from (select tmp.sq_oficina
            ,tmp.sg_oficina
            ,to_char( tmp.dt_inicio,'dd/MM/yyyy') as dt_inicio
            ,to_char(tmp.dt_fim,'dd/MM/yyyy') as dt_fim
            ,tmp.de_fonte_recurso
            ,tmp.de_local
            ,tmp.sg_unidade_org
            ,tmp.ds_tipo_oficina
            ,tmp.de_ciclo_avaliacao
            ,tmp.ds_situacao
            ,json_object_agg( COALESCE(pessoa.no_pessoa,''), json_build_object('sq_pessoa',pessoa.sq_pessoa,'no_pessoa',pessoa.no_pessoa) ) as json_participantes
             -- motor
            from ( select coalesce(unidade_org.sg_unidade_org,centro.no_pessoa) as sg_unidade_org
            ,tipo.ds_dados_apoio as ds_tipo_oficina
            ,ciclo.de_ciclo_avaliacao
            ,situacao.ds_dados_apoio as ds_situacao
            ,fonteRecurso.ds_dados_apoio as de_fonte_recurso
            ,oficina.*
            from salve.oficina
            left join salve.vw_pessoa as centro on centro.sq_pessoa = oficina.sq_unidade_org
            left join salve.vw_unidade_org as unidade_org on unidade_org.sq_pessoa = oficina.sq_unidade_org
            inner join salve.dados_apoio as tipo on tipo.sq_dados_apoio = oficina.sq_tipo_oficina
            inner join salve.dados_apoio as situacao on situacao.sq_dados_apoio = oficina.sq_situacao
            left  join salve.dados_apoio as fonteRecurso on fonteRecurso.sq_dados_apoio = oficina.sq_fonte_recurso
            inner join salve.ciclo_avaliacao as ciclo on ciclo.sq_ciclo_avaliacao = oficina.sq_ciclo_avaliacao"""
                    if (where) {
                        sqlCmd += "\nwhere " + where.join(' and ')
                    }
                    sqlCmd += """ ) tmp
            left outer join salve.oficina_participante as participante on participante.sq_oficina = tmp.sq_oficina
            left outer join salve.vw_pessoa as pessoa on pessoa.sq_pessoa = participante.sq_pessoa
            left outer join salve.dados_apoio as situacao on situacao.sq_dados_apoio = participante.sq_situacao
            group by tmp.sq_oficina
            ,tmp.sg_oficina
            ,tmp.ds_situacao
            ,tmp.dt_inicio
            ,tmp.dt_fim
            ,tmp.de_fonte_recurso
            ,tmp.de_local
            ,tmp.sg_unidade_org
            ,tmp.ds_tipo_oficina
            ,tmp.de_ciclo_avaliacao) tmp2
            left outer join salve.oficina_ficha on oficina_ficha.sq_oficina = tmp2.sq_oficina
            left outer join salve.ficha on ficha.sq_ficha = oficina_ficha.sq_ficha
            left outer join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
            left outer join salve.dados_apoio as grupo on grupo.sq_dados_apoio = ficha.sq_grupo
            left outer join salve.dados_apoio as situacao_ficha_oficina on situacao_ficha_oficina.sq_dados_apoio = oficina_ficha.sq_situacao_ficha
            where ( situacao_ficha_oficina.cd_sistema is null or situacao_ficha_oficina.cd_sistema not in ('EXCLUIDA','COMPILACAO') )
            and oficina_ficha.st_transferida = false
            group by tmp2.sq_oficina
            ,tmp2.sg_oficina
            ,tmp2.ds_situacao
            ,tmp2.dt_inicio
            ,tmp2.dt_fim
            ,tmp2.de_fonte_recurso
            ,tmp2.de_local
            ,tmp2.sg_unidade_org
            ,tmp2.ds_tipo_oficina
            ,tmp2.de_ciclo_avaliacao
            ,tmp2.json_participantes||''
            order by tmp2.sg_oficina
            """
                    Sql sql = new Sql(dataSource)

                    try {

                        rows = sql.rows(sqlCmd, qryParams)
                        rows.each { row ->

                            // https://grails.github.io/grails2-doc/2.1.2/api/org/codehaus/groovy/grails/web/json/JSONObject.html
                            JSONObject fichas = JSON.parse(row.json_fichas)
                            JSONObject participantes = JSON.parse(row.json_participantes)
                            JSONObject grupos = JSON.parse(row.json_grupos)

                            // aplicar o itálico nos nomes científicos
                            List nomesCientificos = fichas?.names()?.collect {
                                return Util.ncItalico(it, true)
                            }

                            // colocar os nomes cientificos em ordem alfabética

                            nomesCientificos.sort()

                            // campos calculados
                            row.de_fichas = nomesCientificos?.join(', ')
                            row.nu_fichas = nomesCientificos.size()
                            if (row.nu_fichas.toInteger() == 1i) {
                                if (row.de_fichas.trim() == '') {
                                    row.nu_fichas = 0
                                }

                            }

                            row.de_participantes = participantes?.names()?.collect { it = Util.capitalize(it) }.join(', ')
                            row.nu_participantes = row.de_participantes.split(',').size()
                            row.de_grupos = grupos.keys().findAll { it != '' }.join(', ')
                        }
                    } catch (Exception e) {
                        println e.getMessage()
                    }
                    sql.close()
                } else if (params.formato == 'participantes') {

                    sqlCmd = """select op.sq_oficina
                             , coalesce( oficina.sg_oficina, oficina.no_oficina) as no_oficina
                             , oficina.de_local
                             , oficina.dt_inicio
                             , oficina.dt_fim
                             , tipo.ds_dados_apoio as ds_tipo
                             , situacao.ds_dados_apoio as ds_situacao
                             , pf.no_pessoa
                             , op.de_email
                             , instituicao.no_instituicao
                             , instituicao.sg_instituicao
                             , papeis.papeis
                        from salve.oficina_participante op
                                 inner join salve.vw_pessoa_fisica as pf on pf.sq_pessoa = op.sq_pessoa
                                 inner join salve.web_instituicao as instituicao on instituicao.sq_web_instituicao = op.sq_instituicao
                                 inner join salve.oficina on oficina.sq_oficina = op.sq_oficina
                                 inner join salve.dados_apoio as situacao on situacao.sq_dados_apoio = oficina.sq_situacao
                                 inner join salve.dados_apoio as tipo on tipo.sq_dados_apoio = oficina.sq_tipo_oficina
                        left join lateral (
                                select array_to_string(array_agg( distinct  papel.ds_dados_apoio),', ') as papeis
                                    from salve.oficina_ficha_participante as ofp
                                    inner join salve.dados_apoio as papel on papel.sq_dados_apoio = ofp.sq_papel_participante
                                    where ofp.sq_oficina_participante = op.sq_oficina_participante
                        ) as papeis on true
                        ${where ? 'where ' + where.join('\nand ') : ''}

                    """
                    Sql sql = new Sql(dataSource)
                    Map res = [status: 0, type: 'success', msg: 'Planilha participantes gerada com sucesso', data:[:] ]
                    try {
                        /** /
                        println ' '
                        println sqlCmd
                        println qryParams
                        /**/
                        rows = sql.rows(sqlCmd, qryParams)

                        // colocar em ordem alfabetica por oficina + nome do participante
                        rows.sort { Util.removeAccents( (it.no_oficina + it.no_pessoa).toLowerCase()) }

                        if( rows ) {
                            res.data = _exportRel002Participantes(rows)
                        } else {
                            res.msg = 'Nenhum registro encontrado'
                        }
                    } catch (Exception e) {
                        res.status = 1
                        res.msg = e.getMessage()
                        res.type = 'error'
                    }
                    sql.close()
                    render res as JSON
                    return
                }
            }
            render('template': 'grideRel002', model: [rows: rows, erros: erros])
        }
    }

    //***************************************************************

    /**
     * relatório de TAXONS - Analítico - rel003
     * @return
     */
    def rel003() {
        String errorMessage = ''
        Map sqlParams = [:]
        List whereOr = []
        List whereMv = []
        String cmdSql
        boolean isAdm = session?.sicae?.user?.isADM()

        // retirar o cache para o perfil adm para que a coluna "Exibir módulo público" seja atualizada on-line
        Integer cacheMinutes  =  isAdm ? 0 : 10

        // se não for impressão ou exportacao renderizar a tela
        //if ( !params.starting && !params.print && !params.exportarOcorrencias ) {
        if (!params.gridId && !params.exportAs) {
            _rel003View()
            return
        }

        //render 'passou REL 003'
        //return;

        /***********************************/
        /**      inicio dos filtros        */
        /***********************************/

        // CICLO DE AVALIACAO
        if (params.sqCicloAvaliacaoFiltro) {
            //wherePasso1.push("ficha.sq_ciclo_avaliacao = :sqCicloAvaliacao")
            //whereMv.push("mv.sq_ciclo_avaliacao = :sqCicloAvaliacao")
            //sqlParams.sqCicloAvaliacao = params.sqCicloAvaliacaoFiltro.toLong()


            // FILTRAR INCLUIDAS NO CICLO ATUAL, SEM RESULTADO NO CICLO ANTERIOR

            if (params.chkIncluidasCicloFiltro == 'S') {
                CicloAvaliacao cicloAvaliacao = CicloAvaliacao.get(params.sqCicloAvaliacaoFiltro.toLong() )
                CicloAvaliacao cicloAnterior = CicloAvaliacao.createCriteria().get {
                    lt('nuAno', cicloAvaliacao.nuAno)
                    order('nuAno', 'desc')
                    maxResults(1)
                }
                if (cicloAnterior) {
                    whereMv.push("""not exists ( select null from salve.ficha ficha_1
                        where ficha_1.sq_ciclo_avaliacao = ${cicloAnterior.id.toString()}
                          and ficha_1.sq_categoria_iucn_sugestao is null and ficha_1.sq_taxon = coalesce(mv.sq_subespecie,mv.sq_especie) offset 0 limit 1 )""")
                }
            }
        }

        // NOME CIENTIFICO
        if (params.nmCientificoFiltro) {
            params.nmCientificoFiltro = params.nmCientificoFiltro.toString().replaceAll(/%/, '').replaceAll(/;/,',')
            //wherePasso1.push("ficha.nm_cientifico ilike :nmCientifico")
            // verificar se foi passado lista de nomes ou apenas 1 nome
            List nomes = params.nmCientificoFiltro.split(',').collect{ it.toString().trim() }
            if( nomes.size() == 1 ) {
                whereMv.push("(mv.no_taxon ilike :noTaxon or mv.no_taxon_anterior ilike :noTaxon)")
                sqlParams.noTaxon = nomes[0] + '%'
            } else {
                List nomesTaxons = []
                //whereOr = []
                nomes.eachWithIndex { nome, index ->
                    // colocar o nome cientifico iniciado com letra maiuscula e o restante em minusculas
                    nome =  nome.substring(0, 1).toUpperCase() + nome.substring(1)
                    // criar dinamicamente os parametros para cada nome informado
                    //String paramName = 'noTaxon'+index.toString()
                    //sqlParams[paramName] = nome
                    // gerar cláusula OR do comando sql que será gerado
                    //whereOr.push("mv.no_taxon = :" + paramName)
                    //whereOr.push("mv.no_taxon ='" + nome + "'")
                    nomesTaxons.push( nome )
                }
                if( nomesTaxons ) {
                    String nomesFormatados = "'" + nomesTaxons.join("','") + "'"
                    whereMv.push("( mv.no_taxon::text = any(array[${nomesFormatados}]) or mv.no_taxon_anterior::text = any(array[${nomesFormatados}]) )")
                }
                /*if( whereOr ){
                    whereMv.push( '('  + whereOr.join(' or ') + ')' )
                }*/
                params.nmCientificoFiltro = '%'
            }
        }

        // NOME COMUM
        if (params.nmComumFiltro) {
            //wherePasso1.push("exists ( select null from salve.ficha_nome_comum as x where x.sq_ficha = ficha.sq_ficha and public.fn_remove_acentuacao(x.no_comum) ilike :nmComum limit 1)")
            whereMv.push("( mv.no_comum ilike :noComum or mv.no_comum ilike :noComumSemAcento )")
            params.nmComumFiltro = params.nmComumFiltro.toString().replaceAll(/%/, '')
            sqlParams.noComum = '%' + params.nmComumFiltro + '%'
            sqlParams.noComumSemAcento = '%' + Util.removeAccents(params.nmComumFiltro) + '%'
        }

        // SITUACAO DA FICHA
        // se não houver filtro pela situação, eliminar as fichas excluidas por padrão
        if (params.sqSituacaoFiltro) {
            //wherePasso1.push("ficha.sq_situacao_ficha in ( ${params.sqSituacaoFiltro} )")
            whereMv.push("mv.sq_situacao_ficha in ( ${params.sqSituacaoFiltro} )")
        } else {
            // se não for informado o nome cientifico adicionar o filtro das fichas excluidas automaticamente
            // se não for solicitadas as excluidas
            if( ! params.nmCientificoFiltro && ! params.chkExcluidasFiltro ) {
                DadosApoio situacaoExcluida = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'EXCLUIDA')
                if ( situacaoExcluida ) {
                    ///wherePasso1.push("ficha.sq_situacao_ficha <> ${situacaoExcluida.id.toString()} ")
                    whereMv.push("mv.sq_situacao_ficha <> ${situacaoExcluida.id.toString()} ")
                }
            }
        }

        // ENDEMICA DO BRASIL
        if (params.stEndemicaFiltro =~ /S|N|D/) {
            //wherePasso1.push('ficha.st_endemica_brasil = :stEndemica')
            whereMv.push('mv.st_endemica_brasil = :stEndemica')
            sqlParams.stEndemica = params.stEndemicaFiltro.toUpperCase()
        }

        // ARVORE TAXONOMICA / GRUPO TAXONOMICO
        if ( params.nivelFiltro && params.sqTaxonFiltro) {
            if( params.nivelFiltro.isNumber()){
                NivelTaxonomico nivelTaxonomico = NivelTaxonomico.get(params.nivelFiltro.toLong())
                params.nivelFiltro = nivelTaxonomico.coNivelTaxonomico
            }
            // wherePasso2.push("""exists ( select null from taxonomia.taxon x where x.sq_taxon = passo1.sq_taxon and (x.json_trilha-> :deNivelTaxonomico ->>'sq_taxon')::bigint in (${params.sqTaxonFiltro}) limit 1)""")
            // sqlParams.deNivelTaxonomico = Util.removeAccents(params.nivelFiltro).toLowerCase()
            String nivelTaxon = Util.removeAccents(params.nivelFiltro).toLowerCase()
            if( ! (params.sqTaxonFiltro =~ /\,/) ) {
                whereMv.push("mv.sq_${nivelTaxon} = :sqTaxonFiltro")
                sqlParams.sqTaxonFiltro = params.sqTaxonFiltro.toLong()
            } else {
                whereMv.push("sq_${nivelTaxon} in (${params.sqTaxonFiltro})")
            }

        }

        // UNIDADE ORGANIZACIONAL
        if (params.sqUnidadeFiltro) {
            //wherePasso1.push("ficha.sq_unidade_org in (${params.sqUnidadeFiltro})")
            if( ! ( params.sqUnidadeFiltro =~ /\,/) ) {
                whereMv.push("mv.sq_unidade_org = :sqUnidadeFiltro")
                sqlParams.sqUnidadeFiltro = params.sqUnidadeFiltro.toLong()
            } else {
                whereMv.push("mv.sq_unidade_org in (${params.sqUnidadeFiltro})")
            }
        }

        //CATEGORIA E CRITERIO HISTORICO
        //sqCategoriaHistFiltro = 1002,131
        if (params.sqCategoriaHistFiltro) {
            //wherePasso3.push("""historico.sq_categoria_anterior in (${params.sqCategoriaHistFiltro})""")
            if (!(params.sqCategoriaHistFiltro =~ /\,/)) {
                whereMv.push("""mv.sq_categoria_anterior = :sqCcategoriaAnterior""")
                sqlParams.sqCcategoriaAnterior = params.sqCategoriaHistFiltro.toLong()
            } else {
                whereMv.push("""mv.sq_categoria_anterior in (${params.sqCategoriaHistFiltro})""")
            }
        }
        if (params.dsCriterioHistExatoFiltro) {
            //wherePasso3.push("historico.de_criterio_anterior = :dsCriterioHistExatoFiltro")
            whereMv.push("mv.de_criterio_anterior = :dsCriterioHistExatoFiltro")
            sqlParams.dsCriterioHistExatoFiltro = params.dsCriterioHistExatoFiltro
        } else if (params.dsCriterioHistFiltro) {
            //wherePasso3.push("(" + params.dsCriterioHistFiltro.split(',').collect { "historico.de_criterio_anterior like  '%" + it + "%'" }.join(' or ') + ')')
            if( ! (params.dsCriterioHistFiltro =~ /\,/) ) {
                whereMv.push("mv.de_criterio_anterior ilike :dsCriterioHistFiltro")
                sqlParams.dsCriterioHistFiltro = '%' + params.dsCriterioHistFiltro + '%'
            } else {
                whereMv.push("(" + params.dsCriterioHistFiltro.split(',').collect { "mv.de_criterio_anterior like '%" + it + "%'" }.join(' or ') + ')')
            }
        }


        // CATEGORIA E CRITERIO AVALIADO
        // sqCategoriaOficinaFiltro = 131,132
        // dsCriterioOficinaFiltro = B,C
        if (params.sqCategoriaOficinaFiltro) {
            if (!(params.sqCategoriaOficinaFiltro =~ /\,/)) {
                whereMv.push("""mv.sq_categoria_avaliada = :sqCategoriaAvaliada""")
                sqlParams.sqCategoriaAvaliada = params.sqCategoriaOficinaFiltro.toLong()
            } else {
                whereMv.push("""mv.sq_categoria_avaliada in (${params.sqCategoriaOficinaFiltro})""")
            }
        }
        if (params.dsCriterioOficinaExatoFiltro) {
            whereMv.push("mv.de_criterio_avaliado = :dsCriterioAvaliado")
            sqlParams.dsCriterioAvaliado = params.dsCriterioOficinaExatoFiltro
        } else if (params.dsCriterioOficinaFiltro) {
            if( ! (params.dsCriterioOficinaFiltro =~ /\,/) ) {
                whereMv.push("mv.de_criterio_avaliado Like :dsCriterioAvaliado")
                sqlParams.dsCriterioAvaliado = '%' + params.dsCriterioOficinaFiltro + '%'
            } else {
                whereMv.push("(" + params.dsCriterioOficinaFiltro.split(',').collect { "mv.de_criterio_avaliado like '%" + it + "%'" }.join(' or ') + ')')
            }
        }
        /*
        if (params.sqCategoriaOficinaFiltro) {
            whereFinal.push("passoFinal.sq_categoria_avaliada in (${params.sqCategoriaOficinaFiltro})")
        }
        if (params.dsCriterioOficinaExatoFiltro) {
            whereFinal.push("passoFinal.de_criterio_avaliado = :dsCriterioAvaliadoExato")
            sqlParams.dsCriterioAvaliadoExato = params.dsCriterioOficinaExatoFiltro
        } else if (params.dsCriterioOficinaFiltro) {
            whereFinal.push("(" + params.dsCriterioOficinaFiltro.split(',').collect { "passoFinal.de_criterio_avaliado like '%" + it + "%'" }.join(' or ') + ')')
        }*/


        // CATEGORIA E CRITERIO VALIDADO
        // sqCategoriaFinalFiltro = 1002,132
        // dsCriterioFinalFiltro = A
        // dsCriterioFinalExatoFiltro = A1a+2a
        if (params.sqCategoriaFinalFiltro) {
            if (!(params.sqCategoriaFinalFiltro =~ /\,/)) {
                whereMv.push("""mv.sq_categoria_validada = :sqCategoriaValidada""")
                sqlParams.sqCategoriaValidada = params.sqCategoriaFinalFiltro.toLong()
            } else {
                whereMv.push("""mv.sq_categoria_validada in (${params.sqCategoriaFinalFiltro})""")
            }
        }
        if (params.dsCriterioFinalExatoFiltro) {
            whereMv.push("mv.de_criterio_validado = :dsCriterioValidado")
            sqlParams.dsCriterioValidado = params.dsCriterioFinalExatoFiltro
        } else if (params.dsCriterioFinalExatoFiltro) {
            if( ! (params.dsCriterioFinalExatoFiltro =~ /\,/) ) {
                whereMv.push("mv.de_criterio_validado ilike :dsCriterioValidado")
                sqlParams.dsCriterioValidado = '%' + params.dsCriterioFinalExatoFiltro + '%'
            } else {
                whereMv.push("(" + params.dsCriterioFinalExatoFiltro.split(',').collect { "mv.de_criterio_validado like '%" + it + "%'" }.join(' or ') + ')')
            }
        }
        /*if (params.sqCategoriaFinalFiltro) {
            //wherePasso1.push("ficha.sq_categoria_final in (${params.sqCategoriaFinalFiltro})")
            whereFinal.push("passoFinal.sq_categoria_validada in (${params.sqCategoriaFinalFiltro})")
        }
        if (params.dsCriterioFinalExatoFiltro) {

            whereFinal.push("passoFinal.de_criterio_validado = :dsCriterioFinalExato")
            sqlParams.dsCriterioFinalExato = params.dsCriterioFinalExatoFiltro
        } else if (params.dsCriterioFinalFiltro) {
            whereFinal.push("(" + params.dsCriterioFinalFiltro.split(',').collect { "passoFinal.de_criterio_validado like '%" + it + "%'" }.join(' or ') + ')')
        }*/

        // PERIODO DA VALIDACAO
        // dtInicioValidacaoFiltro = 01/01/2021
        // dtFimValidacaoFiltro = 05/01/2021
        if (params.dtInicioValidacaoFiltro) {
            try {
                String dtInicioValidacao = new Date().parse('dd/MM/yyyy', params.dtInicioValidacaoFiltro).format('yyyy-MM-dd')
                //wherePasso1.push("ficha.dt_aceite_validacao >= '${dtInicioValidacao}'")
                whereMv.push("mv.dt_aceite_validacao >= '${dtInicioValidacao}'")
            } catch (Exception e) {
                //erros.push('Data inicial inválida')
            }
        }
        if (params.dtFimValidacaoFiltro) {
            try {
                String dtFimValidacao = new Date().parse('dd/MM/yyyy', params.dtFimValidacaoFiltro).format('yyyy-MM-dd')
                //wherePasso1.push("ficha.dt_aceite_validacao <= '${dtFimValidacao}'")
                whereMv.push("mv.dt_aceite_validacao <= '${dtFimValidacao} 23:59:59'")
            } catch (Exception e) {
                //erros.push('Data final inválida!')
            }
        }

        // COMPARATIVO ENTRE AS AVALIAÇÕES
        //deComparativoFiltro = PIOR,NAO_APLICA
        //deComparativoRelacaoFiltro = HO --HISTORIXO X OFICINA, HF- Hstorico x Final, OF-Oficina x Final
        whereOr = []
        if (params.deComparativoFiltro) {
            String coluna = ''
            //String sinal = ''
            // Definir as colunas que serão utilizadas para comparacao se melhorou, piorou, igual ou NA
            // HO - comparar historico avaliação com resultado da oficina de avaliacao ( aba 11.6 )
            // HF - comparar historico avaliacao com resultado da validacao (aba 11.7)
            // OF - comparar resultadao a valiacao (aba 11.6) com resultado da validacao (aba 11.7)
            if (params.deComparativoRelacaoFiltro == 'HO') {
                coluna = 'mv.de_comparativo_categoria_anterior_com_avaliada'
            } else if (params.deComparativoRelacaoFiltro == 'HF') {
                coluna = 'mv.de_comparativo_categoria_anterior_com_validada'
            } else if (params.deComparativoRelacaoFiltro == 'OF') {
                coluna = 'mv.de_comparativo_categoria_avaliada_com_validada'
            }
            if( ! (params.deComparativoFiltro =~ /,/)) {
                whereMv.push(coluna + ' = :comparativo')
                String strTemp = Util.capitalize(params.deComparativoFiltro.toString())
                if( params.deComparativoFiltro == 'NAO_APLICA') {
                    strTemp = 'Não se aplica'
                }
                sqlParams.comparativo = strTemp
            } else {
                whereMv.push("(" + params.deComparativoFiltro.split(',').collect {
                    String strTemp = Util.capitalize( it.toString() )
                    if( it == 'NAO_APLICA') {
                        strTemp = 'Não se aplica'
                    }
                    coluna + " = '" + strTemp + "'"
                }.join(' or ') + ')')
            }
        }

        // GRUPO
        //sqGrupoFiltro = 1205,1206
        if (params.sqGrupoFiltro) {
            //wherePasso1.push("ficha.sq_grupo in (${params.sqGrupoFiltro})")
            if( ! ( params.sqGrupoFiltro =~ /,/ ) ) {
                whereMv.push("mv.sq_grupo_avaliado = :sqGrupoAvaliado")
                sqlParams.sqGrupoAvaliado = params.sqGrupoFiltro.toLong()
            } else {
                whereMv.push("mv.sq_grupo_avaliado in (${params.sqGrupoFiltro})")
            }
        }

        //SUBGRUPO
        // sqSubgrupoFiltro = 1193,1191
        if (params.sqSubgrupoFiltro) {
            //wherePasso1.push("ficha.sq_subgrupo in (${params.sqSubgrupoFiltro})")
            if( ! ( params.sqSubgrupoFiltro =~ /,/ ) ) {
                whereMv.push("mv.sq_subgrupo_avaliado = :sqSubgrupoAvaliado")
                sqlParams.sqSubgrupoAvaliado = params.sqSubgrupoFiltro.toLong()
            } else {
                whereMv.push("mv.sq_subgrupo_avaliado in (${params.sqSubgrupoFiltro})")
            }
        }


        // BIOMA
        //chkEndemicaBioma = S
        //sqBiomaFiltro = 24,25
        /*
        if (params.sqBiomaFiltro) {
            // como os biomas estão na base corporativa e na tabela de apoio porque no salve pode haver o bioma Indeterminado
            // a comparacao deve ser feita pelos nomes dos biomas
            List nomesBiomas = []
            DadosApoio.findAllByIdInList(params.sqBiomaFiltro.split(',').collect { it.toLong() }).each {
                nomesBiomas.push( Util.removeAccents(it.descricao).toUpperCase() )
            }
            if (params.chkEndemicaBioma && params.chkEndemicaBioma == 'S') {
                whereOr = []
                nomesBiomas.each {
                   whereOr.push("mv.no_biomas_array = '{${it}}'")
                }
                whereMv.push( '('  + whereOr.join(' or ') + ')' )
            } else {
                whereMv.push("mv.no_biomas_array && '{${nomesBiomas.join(',')}}'")
            }
        }
        */
        if (params.sqBiomaFiltro) {
            // como os biomas estão na base corporativa e na tabela de apoio porque no salve pode haver o bioma Indeterminado
            // a comparacao deve ser feita pelos nomes dos biomas
            List nomesBiomas = []
            DadosApoio.findAllByIdInList(params.sqBiomaFiltro.split(',').collect { it.toLong() }).each {
                nomesBiomas.push(Util.removeAccents(it.descricao).toUpperCase())
            }
            if (params.radANDORBiomas == 'and') {
                if (!params.chkSomenteBiomas) {
                    //ocorre nos dois ao mesmo tempo mas pode ocorrer em um outro ( Especies qu ocorrem em todos estes Biomas A,B,C ao mesmo tempo )
                    whereMv.push("mv.no_biomas_array @> '{${nomesBiomas.join(',')}}'")
                } else {
                    //ocorre em todas as UFs ao mesmo tempo e somente nelas
                    whereMv.push("'{${nomesBiomas.join(',')}}' <@ mv.no_biomas_array")
                    whereMv.push("array_length(mv.no_biomas_array,1) = array_length( ARRAY[${nomesBiomas.collect { "'" + it + "'" }.join(',')}],1)")
                }
            } else {
                if (!params.chkSomenteBiomas) {
                    // ocorrem em pelo menos uma das UFs selecionadas e tambem em alguma outra
                    whereMv.push("mv.no_biomas_array && '{${nomesBiomas.join(',')}}'")
                } else {
                    // ocorre exclusivamente em uma ou na outra UF (Especies endemicas das UFs A,B,C)
                    whereMv.push("mv.no_biomas_array <@ '{${nomesBiomas.join(',')}}'")
                }
            }
        }


        // ESTADO
        /*
        //chkEndemicaEstado = S
        //sqEstadoFiltro = 26,4,22
        if (params.sqEstadoFiltro) {
            // colocando apenas no final o custo fica bem mais baixo
            if (params.chkEndemicaEstado && params.chkEndemicaEstado == 'S') {
                whereOr = []
                params.sqEstadoFiltro.split(',').each {
                    whereOr.push("mv.sq_estados_array = '{${it}}'")
                }
               whereMv.push('(' + whereOr.join(' or ') + ')')
            }
            else {
                whereMv.push("mv.sq_estados_array && '{${params.sqEstadoFiltro}}'")
            }
        }*/
        if( params.sqEstadoFiltro ) {
            if (params.radANDOREstados == 'and') {
                if (!params.chkSomenteEstados) {
                    //ocorre nos dois ao mesmo tempo mas pode ocorrer em um outro ( Especies qu ocorrem em todos estas UFs A,B,C ao mesmo tempo )
                    whereMv.push("mv.sq_estados_array @> '{${params.sqEstadoFiltro}}'")
                } else {
                    //ocorre em todas as UFs ao mesmo tempo e somente nelas
                    whereMv.push("'{${params.sqEstadoFiltro}}' <@ mv.sq_estados_array")
                    whereMv.push("array_length(mv.sq_estados_array,1) = array_length( ARRAY[${params.sqEstadoFiltro.split(',').collect { "'" + it + "'" }.join(',')}],1)")

                }
            } else {
                if (!params.chkSomenteEstados) {
                    // ocorrem em pelo menos uma das UFs selecionadas e tambem em alguma outra
                    whereMv.push("mv.sq_estados_array && '{${params.sqEstadoFiltro}}'")
                } else {
                    // ocorre exclusivamente em uma ou na outra UF (Especies endemicas das UFs A,B,C)
                    whereMv.push("mv.sq_estados_array <@ '{${params.sqEstadoFiltro}}'")
                }
            }
        }

        // UCS COM ESFERA + ID COMO IDS
        /*
        // UCS
        if (params.sqUcFiltro) {
            // os codigos das ucs deve ter a efera na frente do id são pesquisados como string no array
            List codigosUc = params.sqUcFiltro.split(',').collect {
                '"' + it + '"'
            }
            if (codigosUc) {
                if( params.radANDORUcs == 'and' ){
                    if( ! params.chkSomenteUcs ){
                        //ocorre nas duas ao mesmo tempo mas pode ocorrer em uma outra ( Especies qu ocorrem em todas estas UCs A,B,C ao mesmo tempo )
                        whereMv.push("mv.sq_ucs_array @> '{${codigosUc.join(',')}}'")
                    } else {
                        //ocorre nas duas ao mesmo tempo e somente nas duas
                        whereMv.push("'{${codigosUc.join(',')}}' <@ mv.sq_ucs_array")
                        whereMv.push("array_length(mv.sq_ucs_array,1) = array_length( ARRAY[${codigosUc.collect{"'"+it+"'"}.join(',')}],1)")
                    }
                } else {
                    if( ! params.chkSomenteUcs ){
                        // ocorrem em pelo menos uma das UCS selecionadas e tambem em alguma outra
                        whereMv.push("mv.sq_ucs_array && '{${codigosUc.join(',')}}'")
                    } else {
                        // ocorre exclusivamente em uma ou na outra (Especies endemicas das UCs A,B,C)
                        whereMv.push("mv.sq_ucs_array <@ '{${codigosUc.join(',')}}'")
                    }
                }
            }
        }
        */

        // UCS - PELO SQ_CONTROLE
        // sqUcFiltro = 2,3382,177 - sq_controle
        if (params.sqUcFiltro) {
            // criar os codigos no padrão id+esfera para pesquisar na visão materializada
            List codigosUc = []
            Uc.createCriteria().list {
                'in'('id', params.sqUcFiltro.split(',').collect { it.toLong() })
            }.each {
                // o array na visão materializada não possui a esfera: Ex: {3300,3404,3646,3646,325717,3273,1078,1085,1017,880,858,330873,4965,4874}
                codigosUc.push(it.sqUc.toString() )
            }
            if (codigosUc) {
                if( params.radANDORUcs == 'and' ){
                    if( ! params.chkSomenteUcs ){
                        //ocorre nas duas ao mesmo tempo mas pode ocorrer em uma outra ( Especies qu ocorrem em todas estas UCs A,B,C ao mesmo tempo )
                        whereMv.push("mv.sq_ucs_array @> '{${codigosUc.join(',')}}'")
                    } else {
                        //ocorre nas duas ao mesmo tempo e somente nas duas
                        whereMv.push("'{${codigosUc.join(',')}}' <@ mv.sq_ucs_array")
                        whereMv.push("array_length(mv.sq_ucs_array,1) = array_length( ARRAY[${codigosUc.collect{"'"+it+"'"}.join(',')}],1)")

                    }
                } else {
                    if( ! params.chkSomenteUcs ){
                        // ocorrem em pelo menos uma das UCS selecionadas e tambem em alguma outra
                        whereMv.push("mv.sq_ucs_array && '{${codigosUc.join(',')}}'")
                    } else {
                        // ocorre exclusivamente em uma ou na outra (Especies endemicas das UCs A,B,C)
                        whereMv.push("mv.sq_ucs_array <@ '{${codigosUc.join(',')}}'")
                    }
                }
            }
        }

        // OCORRE EM UC
        //stOcorreEmUc = S
        if (params.stOcorreEmUcFiltro) {
            if (params.stOcorreEmUcFiltro == 'S') {
                whereMv.push("mv.no_ucs is not null")
            } else {
                whereMv.push("mv.no_ucs is null")
            }
        }

        // ESPECIE MIGRATORIA
        // stMigratoriaFiltro = S,N
        if (params.stMigratoriaFiltro =~ /S|N|D/) {
            if( ! ( params.stMigratoriaFiltro =~ /,/ ) ) {
                whereMv.push("mv.st_migratoria = :stMigratoria")
                sqlParams.stMigratoria =  params.stMigratoriaFiltro
            } else {
                whereMv.push("mv.st_migratoria in (${params.stMigratoriaFiltro.split(',').collect { "'${it}'" }.join(',')})")
            }
        }

        // TENDENCIA POPULACIONAL
        // sqTendenciaPopulacionalFiltro = 246,248
        if (params.sqTendenciaPopulacionalFiltro) {
            if( ! ( params.sqTendenciaPopulacionalFiltro =~ /,/ ) ) {
                whereMv.push("mv.sq_tendencia_populacional = :sqTendenciaPopulacional")
                sqlParams.sqTendenciaPopulacional = params.sqTendenciaPopulacionalFiltro.toLong()
            } else {
                whereMv.push("mv.sq_tendencia_populacional in (${params.sqTendenciaPopulacionalFiltro})")
            }
        }

        // BACIAS HIDROGRAFICAS
        // sqBaciaHidrograficaFiltro = 434,427
        if ( params.sqBaciaHidrograficaFiltro ) {
            List idsBaciasValidas = []
            if( ! ( params.sqBaciaHidrograficaFiltro =~ /,/ ) ) {
                // ler os ids das subbacias para pesquisar toda a arvore a partir da bacia selecionada
                idsBaciasValidas = sqlService.execSql( "select id from salve.fn_recursive_apoio_item(${params.sqBaciaHidrograficaFiltro})")?.id
            } else {
                params.sqBaciaHidrograficaFiltro.split(',').each {
                    idsBaciasValidas += sqlService.execSql("select id from salve.fn_recursive_apoio_item(${it})")?.id
                }
            }
            // ordenar e eliminar os duplicados
            idsBaciasValidas.unique().sort()
            whereMv.push("mv.sq_bacias_array && '{${idsBaciasValidas.join(',')}}'")

        }

        // AMEACAS
        // sqAmeacaFiltro = 699,700
      /*  if (params.sqAmeacaFiltro) {
            List idsValidos = []
            if( ! ( params.sqAmeacaFiltro =~ /,/ ) ) {
                // ler os ids para pesquisar toda a arvore a partir do item selecionado
                idsValidos = sqlService.execSql( "select id from salve.fn_recursive_apoio_item(${params.sqAmeacaFiltro})")?.id
            } else {
                params.sqAmeacaFiltro.split(',').each {
                    idsValidos += sqlService.execSql("select id from salve.fn_recursive_apoio_item(${it})")?.id
                }
            }
            // ordenar e eliminar os duplicados
            idsValidos.unique().sort()
            if( idsValidos == 1 ) {
                whereMv.push("mv.sq_ameacas_array = :sqAmeaca")
                sqlParams.sqAmeaca = idsValidos[0].toLong()
            } else {
                whereMv.push("mv.sq_ameacas_array && '{${idsValidos.join(',')}}'")
            }
        }
        */

        if( params.sqAmeacaFiltro ) {
            if (params.radANDORAmeacas == 'and') {
                if (!params.chkSomenteAmeacas) {
                    //possui as duas ameaças ao mesmo tempo mas pode ter outras
                    whereMv.push("mv.sq_ameacas_array @> '{${params.sqAmeacaFiltro}}'")
                } else {
                    //possui todas as AMEACAS ao mesmo tempo e somente elas
                    whereMv.push("'{${params.sqAmeacaFiltro}}' <@ mv.sq_ameacas_array")
                    whereMv.push("array_length(mv.sq_ameacas_array,1) = array_length( ARRAY[${params.sqAmeacaFiltro.split(',').collect { "'" + it + "'" }.join(',')}],1)")
                }
            } else {
                if (!params.chkSomenteAmeacas) {
                    // possui pelo menos uma das AMEAÇAS selecionadas e tambem alguma outra
                    whereMv.push("mv.sq_ameacas_array && '{${params.sqAmeacaFiltro}}'")
                } else {
                    // possui exclusivamente uma ou a outra AMEAÇA
                    whereMv.push("mv.sq_ameacas_array <@ '{${params.sqAmeacaFiltro}}'")
                }
            }
        }

        // USOS
        // sqUsoFiltro = 6,83
        /*
        if (params.sqUsoFiltro) {
            // ler os ids recursivamente para que os usos sejam filtradas utilizando a hierarquia entre eles
            List idsValidos = _recursiveIdsUso(params.sqUsoFiltro)?.split(',')
            // ordenar e eliminar os duplicados
            idsValidos.unique().sort()
            if( idsValidos == 1 ) {
                whereMv.push("mv.sq_usos_array = :sqUso")
                sqlParams.sqUso = idsValidos[0].toLong()
            } else {
                whereMv.push("mv.sq_usos_array && '{${idsValidos.join(',')}}'")
            }
        }*/

        if (params.sqUsoFiltro) {
            if (params.radANDORUsos == 'and') {
                if (!params.chkSomenteUsos) {
                    // possui os dois ao mesmo tempo mas pode possuir algum outro
                    whereMv.push("mv.sq_usos_array @> '{${params.sqUsoFiltro}}'")
                } else {
                    // possui todos os USOS ao mesmo tempo e somente eles
                    whereMv.push("'{${params.sqUsoFiltro}}' <@ mv.sq_usos_array")
                    whereMv.push("array_length(mv.sq_usos_array,1) = array_length( ARRAY[${params.sqUsoFiltro.split(',').collect { "'" + it + "'" }.join(',')}],1)")
                }
            } else {
                if (!params.chkSomenteUsos) {
                    // pussui pelo menos um dos USOS selecionados e tambem algum outro
                    whereMv.push("mv.sq_usos_array && '{${params.sqUsoFiltro}}'")
                } else {
                    // possui exclusivamente um ou o outro USO
                    whereMv.push("mv.sq_usos_array <@ '{${params.sqUsoFiltro}}'")
                }
            }
        }

        // LISTAS E CONVENÇOES
        // sqListaFiltro = 1056,1057
        if (params.sqListaFiltro) {
            whereMv.push("mv.sq_listas_convencoes_array && '{${params.sqListaFiltro}}'")
        }

        // PRESENÇA EM LISTA OFICIAL
        // stListaOficial = S
        if (params.stListaOficialFiltro =~ /S|N/) {
            whereMv.push("mv.st_presenca_lista_vigente = :stListaOficial")
            sqlParams.stListaOficial = params.stListaOficialFiltro.toUpperCase()
        }

        // AÇÃO DE CONSERVAÇAO
        // sqAcaoFiltro = 558,560
        if (params.sqAcaoFiltro) {
            List idsValidos = _recursiveIdsApoio( params.sqAcaoFiltro)?.split(',')
            // ordenar e eliminar os duplicados
            idsValidos.unique().sort()
            whereMv.push("mv.sq_acoes_conservacao_array && '{${idsValidos.join(',')}}'")
        }

        // MULTIMIDIA
        // = COM_IMAGEM
        if (params.stMultimidiaFiltro) {
            String whereMultimidia = ''
            if (params.stMultimidiaFiltro == 'COM_IMAGEM') {
                whereMv.push("mv.st_com_imagem = 'S'")
            } else if (params.stMultimidiaFiltro == 'COM_AUDIO') {
                whereMv.push("mv.st_com_audio = 'S'")
            } else if (params.stMultimidiaFiltro == 'COM_IMAGEM_OU_AUDIO') {
                whereMv.push("( mv.st_com_imagem = 'S' or mv.st_com_audio = 'S')")
            } else if (params.stMultimidiaFiltro == 'COM_IMAGEM_E_AUDIO') {
                whereMv.push("( mv.st_com_imagem = 'S' and  mv.st_com_audio = 'S')")
            } else if (params.stMultimidiaFiltro == 'SEM_IMAGEM') {
                whereMv.push("mv.st_com_imagem = 'N'")
            } else if (params.stMultimidiaFiltro == 'SEM_AUDIO') {
                whereMv.push("mv.st_com_audio = 'N'")
            } else if (params.stMultimidiaFiltro == 'SEM_IMAGEM_OU_AUDIO') {
                whereMv.push("( mv.st_com_audio = 'N' or mv.st_com_imagem = 'N')")
            } else if (params.stMultimidiaFiltro == 'SEM_IMAGEM_E_AUDIO') {
                whereMv.push("( mv.st_com_audio = 'N' and mv.st_com_imagem = 'N')")
            }
        }

        // MAPA DE DISTRIBUICAO
        //stMapaDistribuicao = COM_IMAGEM
        if (params.stMapaDistribuicaoFiltro) {
            if (params.stMapaDistribuicaoFiltro == 'COM_IMAGEM') {
                whereMv.push(" mv.st_com_mapa = 'S'")
            } else if (params.stMapaDistribuicaoFiltro == 'SEM_IMAGEM') {
                whereMv.push(" mv.st_com_mapa = 'N'")
            }
        }

        // EXIBIR MODULO PUBLICO
        if( params.stExibirModuloPublico ) {
            if ( params.stExibirModuloPublico.toLowerCase() == 's' ) {
                whereMv.push("fmp.st_exibir=true")
            } else {
                whereMv.push("( fmp.st_exibir is null or fmp.st_exibir = false )")
            }
        }


        String sqlOrderBy=''
        if( params.sortColumns ){
            List sortColumns = params.sortColumns.split(':')
            sqlOrderBy = '\nORDER BY '+sortColumns[0] + ( sortColumns[1] ? ' '+sortColumns[1] : '')
            if( ! ( params.sortColumns.toString() =~ /no_taxon/) ) {
                sqlOrderBy += ',no_taxon'
            }
        }

        // Select da visão materializada
        cmdSql ="""select mv.sq_ficha
             ,mv.no_taxon
             ,mv.no_comum
             ,mv.no_familia
             ,mv.no_classe
             ,mv.no_ordem
             ,mv.no_autor_taxon
             ,mv.nu_ano_taxon
             ,mv.no_taxon_anterior
             ,mv.sg_unidade_org
             ,mv.de_situacao_ficha
             ,mv.de_endemica_brasil
             ,mv.st_presenca_lista_vigente
             ,mv.de_protegida_legislacao
             ,mv.de_categoria_anterior
             ,mv.de_criterio_anterior
             ,mv.de_categoria_avaliada
             ,mv.de_criterio_avaliado
             ,mv.tx_justificativa_avaliada
             ,mv.de_periodo_avaliacao
             ,mv.de_periodo_validacao
             ,mv.de_categoria_validada
             ,mv.de_criterio_validado
             ,mv.tx_justificativa_validada
             ,mv.dt_aceite_validacao
             ,mv.de_nivel_categoria_anterior
             ,mv.de_nivel_categoria_avaliada
             ,mv.de_nivel_categoria_validada
             ,mv.de_comparativo_categoria
             ,mv.de_comparativo_categoria_anterior_com_avaliada
             ,mv.de_comparativo_categoria_anterior_com_validada
             ,mv.de_comparativo_categoria_avaliada_com_validada
             ,mv.de_motivo_mudanca
             ,mv.no_grupo_avaliado
             ,mv.no_subgrupo_avaliado
             ,mv.no_grupo_recorte
             ,mv.st_migratoria
             ,mv.de_migratoria
             ,mv.de_tendencia_populacional
             ,mv.no_biomas
             ,mv.no_estados
             ,mv.no_bacias
             ,mv.no_ucs
             ,mv.co_cnucs
             ,mv.no_ameacas
             ,mv.no_usos
             ,mv.no_listas_convencoes
             ,mv.no_pans
             ,mv.no_acoes_conservacao
             ,mv.st_com_imagem
             ,mv.st_com_audio
             ,mv.st_com_mapa
             ,${ isAdm || params.stExibirModuloPublico ? 'fmp.st_exibir': 'null as st_exibir'}
              from salve.mv_relatorio_taxon_analitico as mv
              ${ isAdm || params.stExibirModuloPublico ? 'left join salve.ficha_modulo_publico as fmp on fmp.sq_ficha = mv.sq_ficha':''}
              ${whereMv ? 'where ' + whereMv.join('\nand '):''}
              """
        cmdSql += sqlOrderBy

        // simular pagina 2
        /*
        params.paginationTotalRecords =15115
        params.paginationTotalPages = 152
        params.paginationPageNumber = 2
        */
/** /
 //zzz
 if( session.envProduction == false ) {
     println ' '
     println ' '
     println ' '
     println ' '
     println cmdSql
     println sqlParams
     println ' '
     }
 /**/
        if ( params.exportAs == 'ROW' ) {
            Map result = [status:0, msg:'',type:'modal']

            try {

                // parâmetros para execução assyncrona
                Map exportParams = [
                    ids                  : null
                    ,maxFichas           : maxFichasExportarPlanilha
                    ,colunasSelecionadas : params.colsSelected ?: ''
                    ,usuario             : session?.sicae?.user
                    ,email               : params.email
                    ,emailMessage        : ''
                    ,cmdSql              : cmdSql // sql para selecionar as fichas para exportação dos registros
                    ,sqlParams           : sqlParams // parametros para execução do comando sql

                ]
                registrarTrilhaAuditoria('exportar_relatorio_taxon_analitico', 'relatorio_taxon_analitico', exportParams)
                _exportRel003(exportParams)

                result.msg= 'A exportação da(s) ficha(s) para planilha está em andamento.<br><br>Ao finalizar será '+
                    (params.email ? 'enviado para o email <b>'+params.email+'</b> o link para baixar a planilha.' : 'exibida a tela para baixar a planilha.')

            } catch(Exception e ) {
                result.status = 1
                result.msg    = e.getMessage()
            }
            render result as JSON
            return

        }
        else if (params.exportAs == 'REG') {
            Map result = [status:0, msg:'',type:'info']
            try {
                // parâmetros para execução assyncrona
                Map exportParams = [
                    ids                  : params.exportIds // exportar somente os registros das fichas selecionadas no gride
                    ,maxFichas           : maxFichasExportarOcorrencias
                    ,usuario             : session?.sicae?.user
                    ,email               : params.email ?: ( params?.enviarEmail?.toUpperCase() == 'S' && session.sicae.user.email ? session.sicae.user.email : '' )
                    ,excluirSensiveis    : params.excluirRegistrosSensiveis
                    ,excluirCarencia     : params.excluirRegistrosEmCarencia
                    ,utilizadoAvaliacao  : params?.utilizadosAvaliacao == 'AAA' ? '' : params.utilizadosAvaliacao
                    ,adicionadosAposAvaliacao : (params?.utilizadosAvaliacao.toUpperCase() == 'AAA' )
                    ,colunasSelecionadas : params.colsSelected ?: ''
                    ,emailMessage        : ''
                    ,cmdSql              : cmdSql // sql para selecionar as fichas para exportação dos registros
                    ,sqlParams           : sqlParams // parametros para execução do comando sql

                ]

                exportDataService.asyncExportarOcorrencias( exportParams )
                result.msg = 'A exportação dos registros de ocorrência da(s) ficha(s) está em andamento.<br><br>Ao finalizar será '+
                    (exportParams.email ? 'enviado para o email <b>'+exportParams.email+'</b> o link para baixar a planilha.' : 'exibida a tela para baixar a planilha.')

            } catch(Exception e ) {
                result.status = 1
                result.msg    = e.getMessage()
            }

            render result as JSON
            return
        }
        else {
            //println 'Inicio consulta relatorio taxon analitico em '+ new Date().format('dd/MM/yyyy HH:mm:ss')
            if( params.marcarDesmarcarModuloPublico ){
                boolean stExibir = params.marcarDesmarcarModuloPublico.toLowerCase() == 's' ? true : false
                Long sqPessoa = session.sicae.user.sqPessoa.toLong()

                String cmdSqlUpdate = """select mv.sq_ficha
                     from salve.mv_relatorio_taxon_analitico as mv
                     left join salve.ficha_modulo_publico as fmp on fmp.sq_ficha = mv.sq_ficha
                     ${whereMv ? 'where ' + whereMv.join('\nand ') : ''}"""
                List rows = sqlService.execSql(cmdSqlUpdate, sqlParams)
                if( rows ) {

                    // registrar trilha auditoria
                    registrarTrilhaAuditoria('marcar_desmarcar_registro_exibir_modulo_publico','relatorio_taxon_analitico',
                        [sqFichas: rows.sq_ficha.join(','),stExibir:stExibir] )
                    fichaService.marcarDesmarcarRegistroExibirModuloPublico( rows.sq_ficha.join(','), stExibir, sqPessoa)
                }
            }
            Map mapResultado = sqlService.paginate(cmdSql, sqlParams, params, 100, 10, cacheMinutes, debugSql )

            // renderizar o gride
            render(template: "grideRel003", model: [rows          : mapResultado.rows
                                                    , pagination  : mapResultado.pagination
                                                    , errorMessage: errorMessage])
        }

    } // fim rel003

    /**
     * Método para exibir a tela do relatório Taxon Analitico - rel003
     * @return
     */
    protected void _rel003View() {
        String rndId = 'rnd'+Util.randomString(8)
        List listNivelTaxonomico = NivelTaxonomico.list()
        List listCicloAvaliacao = CicloAvaliacao.list(sort: 'nuAno', order: 'desc');
        List listCategoria = dadosApoioService.getTable('TB_CATEGORIA_IUCN').findAll { !(it.codigoSistema ==~ /(OUTRA|AMEACADA|POSSIVELMENTE_EXTINTA)/) }.sort { it.ordem }
        List listGrupo = dadosApoioService.getTable('TB_GRUPO')
        List listSubgrupo = dadosApoioService.getTable('TB_SUBGRUPO')
        List listBioma = dadosApoioService.getTable('TB_BIOMA')
        List listAmeaca = sqlService.ameacas
        //List listAmeaca        = dadosApoioService.getTable('TB_CRITERIO_AMEACA_IUCN')
        List listLista = sqlService.convencoes
        List listAcao = sqlService.acoesConservacao// dadosApoioService.getTable('TB_ACAO_CONSERVACAO')
        List listUso = sqlService.usos // Uso.list()
        List listEstado = Estado.list()
        List listSituacaoFicha = dadosApoioService.getTable('TB_SITUACAO_FICHA')
        List listTendenciaPopulacional = dadosApoioService.getTable('TB_TENDENCIA')
        List listBaciaHidrografica = sqlService.getRecursiveApoio('TB_BACIA_HIDROGRAFICA')
        List columns = ['Taxon', 'Família', 'Classe', 'Ordem', 'Nome comum']
        render('view': 'rel003', model: ['listCicloAvaliacao'         : listCicloAvaliacao
                                         , 'listNivelTaxonomico'      : listNivelTaxonomico
                                         , 'listSituacaoFicha'        : listSituacaoFicha
                                         , 'listCategoria'            : listCategoria
                                         , 'listGrupo'                : listGrupo
                                         , 'listSubgrupo'             : listSubgrupo
                                         , 'listBioma'                : listBioma
                                         , 'listEstado'               : listEstado
                                         , 'listAmeaca'               : listAmeaca
                                         , 'listUso'                  : listUso
                                         , 'listLista'                : listLista
                                         , 'listAcao'                 : listAcao
                                         , 'listBaciaHidrografica'    : listBaciaHidrografica
                                         , 'listTendenciaPopulacional': listTendenciaPopulacional
                                         , rndId                      : rndId
                                         , isAdm                      : session.sicae.user.isADM()
        ])
    }

//    def rel003Old() {
//        GrailsHttpSession selfSession
//        Map resultado = [data:[], recordsTotal:0, recordsFiltered:0, errors:[]]
//        Map qryParams = [:]
//        // criar lista com os taxons para utilizar na exportação dos registros de ocorrencias
//        List idsTaxonsEncontrados = []
//        List where    = []
//        boolean rowValida = true
//
//
//        String whereEstado =''
//        String whereHist  =''
//        String whereBioma  =''
//
//        String whereUcFederal   = ''
//        String whereUcEstadual  = ''
//        String whereUcRppn      = ''
//        String whereAmeaca      = ''
//        String whereUso         = ''
//        String whereLista       = ''
//        String whereAcao        = ''
//        String wherePexHist     = ''
//
//        List erros = []
//        List rows = []
//
//        // retornar o formulário se não for informado o ciclo de avaliação
//        if( params.exportarOcorrencias == 'S' ) {
//            Map res = [status:0, msg:'',type:'info']
//            /*
//            if( session.exportingOccurrences == true ){
//                //session.exportingOccurrences = false
//                res.status=1
//                res.msg = 'Já existe uma exportação de registros sendo executada. Aguarde a finalização.'
//                render res as JSON
//                return
//            }*/
//
//            String tempDir = grailsApplication.config.temp.dir
//            //String fileName = tempDir+'/rel_taxon_analitico_'+params.relId+'.json'
//            String fileName = tempDir + '/' + params.dataTempFile
//            fileName = fileName.replaceAll(/\/\/rel/,'/rel')
//            String email = ( params?.enviarEmail == 'S' ? session?.sicae?.user?.email :'')
//            File file = new File( fileName )
//            if( ! file.exists() ) {
//                res.status=1
//                res.msg = 'Arquivo não encontrado '+fileName
//                render res as JSON
//                return
//            }
//            Map relData = JSON.parse( file.text )
//            if( relData.ids.size() > maxFichasExportarOcorrencias ){
//                res.status=1
//                res.type='error'
//                res.msg = 'Para exportação dos registros de ocorrencia o limite máximo é de ' + maxFichasExportarOcorrencias.toString()+' fichas.';
//                render res as JSON
//                return
//            }
//            Map workerData = [ sqPessoa                             : session?.sicae?.user?.sqPessoa
//                               , ids                                : relData.ids
//                               , idsType                             : 'taxon'
//                               , sqCicloAvaliacao                   : relData.sqCicloAvaliacao
//                               , email                              : email
//                               , excluirRegistrosSensiveis          : params.excluirRegistrosSensiveis
//                               , excluirRegistrosEmCarencia         : params.excluirRegistrosEmCarencia
//                               , utilizadosAvaliacao                : params.utilizadosAvaliacao
//                               , contexto                           : 'relatorioTaxonAnalitico'
//                               , colsSelected                       : params.colsSelected
//                            ]
//            selfSession = session
//            // gravar log
//            registrarTrilhaAuditoria('exportar_ocorrencia',workerData.contexto, workerData);
//            runAsync {
//                try {
//                    exportDataService.ocorrenciasPlanilha( selfSession, workerData )
//                }
//                catch( Exception e ) {
//                    println 'Ocorreu um erro.'
//                    println e.getMessage()
//                    res.msg = e.getMessage()
//                    res.status = 1
//                    res.type = 'error'
//                }
//            }
//            render res as JSON
//            return;
//        }
//        else if ( ! params.print ) {
//            List listNivelTaxonomico    = NivelTaxonomico.list()
//            List listCicloAvaliacao = CicloAvaliacao.list(sort: 'nuAno', order: 'desc');
//            List listCategoria     = dadosApoioService.getTable('TB_CATEGORIA_IUCN').findAll{ ! (it.codigoSistema ==~ /(OUTRA|AMEACADA)/) }.sort{ it.ordem }
//            List listGrupo         = dadosApoioService.getTable('TB_GRUPO')
//            List listSubgrupo      = dadosApoioService.getTable('TB_SUBGRUPO')
//
//            List listBioma         = dadosApoioService.getTable('TB_BIOMA')
//            List listAmeaca        = sqlService.ameacas
//            //List listAmeaca        = dadosApoioService.getTable('TB_CRITERIO_AMEACA_IUCN')
//            List listLista         = sqlService.convencoes
//            List listAcao          = sqlService.acoesConservacao// dadosApoioService.getTable('TB_ACAO_CONSERVACAO')
//            List listUso           = sqlService.usos // Uso.list()
//            List listEstado        = Estado.list()
//            List listSituacaoFicha = dadosApoioService.getTable('TB_SITUACAO_FICHA')
//            List listTendenciaPopulacional = dadosApoioService.getTable('TB_TENDENCIA')
//            List listBaciaHidrografica = sqlService.getRecursiveApoio('TB_BACIA_HIDROGRAFICA')
//
//            List columns = [ 'Taxon', 'Família', 'Classe', 'Ordem','Nome comum']
//
//
//            render('view': 'rel003', model: ['listCicloAvaliacao'    : listCicloAvaliacao
//                                             ,'listNivelTaxonomico'  : listNivelTaxonomico
//                                             ,'listSituacaoFicha'    : listSituacaoFicha
//                                             ,'listCategoria':listCategoria
//                                             ,'listGrupo'   :listGrupo
//                                             ,'listSubgrupo':listSubgrupo
//                                             ,'listBioma'   :listBioma
//                                             ,'listEstado'  :listEstado
//                                             ,'listAmeaca'  :listAmeaca
//                                             ,'listUso'     :listUso
//                                             ,'listLista'   :listLista
//                                             ,'listAcao'    :listAcao
//                                             ,'listBaciaHidrografica'     : listBaciaHidrografica
//                                             ,'listTendenciaPopulacional' : listTendenciaPopulacional
//            ])
//            return
//        }
//
//        if( params.starting == 'S' ) {
//            render resultado as JSON
//            return
//        }
//
//        // validar filtros
//        if (params.dtInicioValidacaoFiltro) {
//            try {
//                qryParams.dtInicioValidacao = new Date().parse('dd/MM/yyyy', params.dtInicioValidacaoFiltro).format('yyyy-MM-dd')
//            } catch (Exception e) {
//                erros.push('Data inicial inválida')
//            }
//        }
//        if (params.dtFimValidacaoFiltro) {
//            try {
//                qryParams.dtFimValidacao = new Date().parse('dd/MM/yyyy', params.dtFimValidacaoFiltro).format('yyyy-MM-dd')
//            } catch (Exception e) {
//                erros.push('Data final inválida!')
//            }
//        }
//        if ( qryParams.dtInicioValidacao && qryParams.dtFimValidacao && qryParams.dtInicioValidacao > qryParams.dtFimValidacao) {
//            String dataInicioValidacao = qryParams.dtInicioValidacao
//            qryParams.dtInicioValidacao = qryParams.dtFimValidacao
//            qryParams.dtFimValidacao = dataInicioValidacao
//        }
//        if( qryParams.dtFimValidacao ) {
//            qryParams.dtFimValidacao += ' 23:59:59'
//        }
//
//        DadosApoio tipoAvaliacaoNacional= dadosApoioService.getByCodigo('TB_TIPO_AVALIACAO', 'NACIONAL_BRASIL')
//        DadosApoio contextoEstado       = dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA','ESTADO' )
//        DadosApoio contextoBioma        = dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA','BIOMA' )
//        DadosApoio contextoBacia        = dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA','BACIA_HIDROGRAFICA' )
//        DadosApoio contextoOcorrencia   = dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA','REGISTRO_OCORRENCIA' )
//        DadosApoio categoriaCR          = dadosApoioService.getByCodigo('TB_CATEGORIA_IUCN', 'CR')
//
//        qryParams.sqAvaliacaoNacional = tipoAvaliacaoNacional.id
//        qryParams.sqContextoEstado    = contextoEstado.id
//        qryParams.sqContextoBioma     = contextoBioma.id
//        qryParams.sqContextoBacia     = contextoBacia.id
//        qryParams.sqContextoOcorrencia = contextoOcorrencia.id
//
//// teste
////params.sqCicloAvaliacaoFiltro = 5
//
//        if( ! params.start )
//        {
//            params.start = 0l
//        }
//        if( !params.length)
//        {
//            params.length = 0l
//        }
//        if( params.length.toLong() > 0l  ) {
//            qryParams.limit = params.length.toLong()
//        }
//        if( params.start > 0  ) {
//            qryParams.offset = params.start.toLong()
//        }
//
//        // filtro nome cientifico
//        if( params.nmCientificoFiltro )
//        {
//            String nmCientificoCleaned = Util.removeAccents(params.nmCientificoFiltro).replaceAll(/(?i)[^a-zA-Z -]/,'').toLowerCase().trim()
//            where.push(  "ficha.nm_cientifico ilike '%${nmCientificoCleaned}%'")
//        }
//        if( params.nmComumFiltro )
//        {
//            String nmComumCleaned = Util.removeAccents(params.nmComumFiltro).replaceAll(/(?i)[^a-zA-Z -]/,'').toLowerCase().trim()
//            where.push(  "nome_comum.no_comum ilike '%${nmComumCleaned}%'")
//        }
//
//        if( params.nivelFiltro && params.sqTaxonFiltro)
//        {
//            NivelTaxonomico nivel = NivelTaxonomico.get( params.nivelFiltro.toLong())
//            if( nivel )
//            {
//                if( params.sqTaxonFiltro.toString().indexOf(',') == -1 ) {
//                    where.push("taxon.json_trilha->'${nivel.coNivelTaxonomico.toLowerCase()}'->>'sq_taxon'='${params.sqTaxonFiltro}'")
//                }
//                else
//                {
//                    where.push("taxon.json_trilha->'${nivel.coNivelTaxonomico.toLowerCase()}'->>'sq_taxon' in (${params.sqTaxonFiltro.split(',').collect{"'"+it+"'"}.join(',')        })")
//                }
//            }
//        }
//
//        // Unidade Responsável
//        if( params.sqUnidadeFiltro )
//        {
//            String whereUnidade = "ficha.sq_unidade_org"
//            if( params.sqUnidadeFiltro.indexOf(',') > -1 )
//            {
//                whereUnidade += ' in ('+params.sqUnidadeFiltro+')'
//            }
//            else
//            {
//                whereUnidade += ' = '+params.sqUnidadeFiltro
//            }
//            where.push( whereUnidade )
//
//        }
//
//        // SITUACAO FICHA
//        if( params.sqSituacaoFiltro ) {
//            String whereSituacao = "ficha.sq_situacao_ficha"
//            if( params.sqSituacaoFiltro.indexOf(',') > -1 )
//            {
//                whereSituacao += ' in ('+params.sqSituacaoFiltro+')'
//            }
//            else
//            {
//                whereSituacao += ' = '+params.sqSituacaoFiltro
//            }
//            where.push( whereSituacao )
//        }
//
//        // OFICINA
//        if( params.sqOficinaFiltro )
//        {
//            String whereOficina = "ficha.sq_ficha in ( select x.sq_ficha from salve.oficina_ficha x where x.sq_oficina"
//            if( params.sqOficinaFiltro.indexOf(',') > -1 )
//            {
//                whereOficina += ' in ('+params.sqOficinaFiltro+') )'
//            }
//            else
//            {
//                whereOficina += ' = '+params.sqOficinaFiltro + ')'
//            }
//            where.push( whereOficina )
//        }
//
//        if( params.deComparativoFiltro && ! params.deComparativoRelacaoFiltro ) {
//            params.deComparativoRelacaoFiltro = 'HF'
//        }
//
//        // comparativo das categorias
//        if( params.deComparativoFiltro && params.deComparativoRelacaoFiltro )
//        {
//            String whereComparativo = ''
//            if( params.deComparativoRelacaoFiltro=='HFO') {
//                whereComparativo = "historico is not null and coalesce( categoria_final.cd_dados_apoio_nivel, categoria_oficina.cd_dados_apoio_nivel ) is not null"
//            }
//            else if( params.deComparativoRelacaoFiltro=='HF') {
//                whereComparativo = "historico is not null and categoria_final.cd_dados_apoio_nivel is not null"
//            }
//            else if( params.deComparativoRelacaoFiltro=='HO') {
//                whereComparativo = "historico is not null and categoria_oficina.cd_dados_apoio_nivel is not null"
//            }
//            else if( params.deComparativoRelacaoFiltro=='OF') {
//                whereComparativo = "categoria_oficina.cd_dados_apoio_nivel is not null and categoria_final.cd_dados_apoio_nivel is not null"
//            }
//           where.push(whereComparativo)
//        }
//
//        // parametros do usuário
//        if( params.sqCicloAvaliacaoFiltro ) {
//            //whereCicloAvaliacao = 'AND ficha.sq_ciclo_avaliacao = '+params.sqCicloAvaliacaoFiltro
//            where.push('ficha.sq_ciclo_avaliacao = :sqCicloAvaliacao')
//            qryParams.sqCicloAvaliacao= params.sqCicloAvaliacaoFiltro.toLong()
//        } else {
//            where.push('ficha.sq_ciclo_avaliacao <> 3')
//        }
//
//        // FILTRAR PELA CATEGORIA ( FINAL, OFICINA E HISTORICO )
//
//        if( params.sqCategoriaHistFiltro )
//        {
//            whereHist = ' and a.sq_categoria_iucn '
//            if( params.sqCategoriaHistFiltro.indexOf(',') > -1 )
//            {
//                whereHist += ' in ('+params.sqCategoriaHistFiltro+')'
//            }
//            else
//            {
//                whereHist += ' = '+params.sqCategoriaHistFiltro
//            }
//            where.push( 'historico.json_historico is not null')
//        }
//
//        // CATEGORIA OFICINA
//
//        if( params.sqCategoriaOficinaFiltro )
//        {
//            //String whereCategoria = 'categoria_oficina.sq_dados_apoio'
//            String whereCategoria = 'oficina_ficha.sq_categoria_iucn'
//            if( params.sqCategoriaOficinaFiltro.indexOf(',') > -1 )
//            {
//                whereCategoria += ' in ('+params.sqCategoriaOficinaFiltro+')'
//            }
//            else
//            {
//                whereCategoria += ' = '+params.sqCategoriaOficinaFiltro
//            }
//            where.push( whereCategoria )
//        }
//
//        if( params.sqCategoriaFinalFiltro )
//        {
//            String whereCategoria = 'categoria_final.sq_dados_apoio'
//            if( params.sqCategoriaFinalFiltro.indexOf(',') > -1 )
//            {
//                whereCategoria += ' in ('+params.sqCategoriaFinalFiltro+')'
//            }
//            else
//            {
//                whereCategoria += ' = '+params.sqCategoriaFinalFiltro
//            }
//            where.push( whereCategoria )
//        }
//
//        // FILTRAR PELO CRITERIO ( FINAL, OFICINA E HISTORICO )
//        if( params.dsCriterioHistExatoFiltro )
//        {
//            whereHist += " and a.de_criterio_avaliacao_iucn = '" + params.dsCriterioHistExatoFiltro + "'"
//            if( ! where.contains( 'historico.json_historico is not null') ) {
//                where.push('historico.json_historico is not null')
//            }
//        } else if( params.dsCriterioHistFiltro ) {
//            List whereOr = []
//            params.dsCriterioHistFiltro.split(',').each{ it ->
//                whereOr.push( "a.de_criterio_avaliacao_iucn like '%${it}%'")
//            }
//            whereHist += " and ( ${whereOr.join(' OR ')} )"
//            if( ! where.contains( 'historico.json_historico is not null') ) {
//                where.push('historico.json_historico is not null')
//            }
//        }
//
//        if( params.dsCriterioOficinaExatoFiltro )
//        {
//            where.push( "ficha.ds_criterio_aval_iucn = '"+params.dsCriterioOficinaExatoFiltro+"'")
//        } else if( params.dsCriterioOficinaFiltro ) {
//            List whereOr = []
//            params.dsCriterioOficinaFiltro.split(',').each{ it ->
//                whereOr.push( "ficha.ds_criterio_aval_iucn like '%${it}%'")
//            }
//            where.push("( ${whereOr.join(' OR ')} )")
//        }
//
//        if( params.dsCriterioFinalExatoFiltro )
//        {
//            where.push ( "ficha.ds_criterio_aval_iucn_final = '"+params.dsCriterioFinalExatoFiltro+"'")
//        } else if( params.dsCriterioFinalFiltro ) {
//            List whereOr = []
//            params.dsCriterioFinalFiltro.split(',').each{ it ->
//                whereOr.push( "ficha.ds_criterio_aval_iucn_final like '%${it}%'")
//            }
//            where.push("( ${whereOr.join(' OR ')} )")
//        }
//
//
//        // filtrar PEX oficina e PEX final e PEX Histórico
//        if( params.inPexOficina )
//        {
//            if( !params.sqCategoriaOficinaFiltro ) {
//                where.push("ficha.st_possivelmente_extinta = 'S'")
//            }
//            else
//            {
//                where.push( "( ( ficha.st_possivelmente_extinta = '" + params.inPexOficina.toUpperCase() + "' and ficha.sq_categoria_iucn = "+categoriaCR.id.toString()+") or ficha.sq_categoria_iucn <> "+categoriaCR.id.toString() + ")" )
//            }
//        }
//
//        if( params.inPexFinal )
//        {
//           if( ! params.sqCategoriaFinalFiltro ) {
//               where.push("ficha.st_possivelmente_extinta_final = 'S'")
//           }
//           else
//           {
//               where.push( "( ( ficha.st_possivelmente_extinta_final = '" + params.inPexFinal.toUpperCase() + "' and ficha.sq_categoria_final = "+categoriaCR.id.toString()+") or ficha.sq_categoria_final <> "+categoriaCR.id.toString() + ")" )
//           }
//        }
//
//        // filtrar pelo periodo da validacao
//        if( qryParams.dtInicioValidacao ) {
//            where.push( "ficha.dt_aceite_validacao >= '${qryParams.dtInicioValidacao}'" )
//            qryParams.remove('dtInicioValidacao')
//        }
//        if( qryParams.dtFimValidacao ) {
//            where.push( "ficha.dt_aceite_validacao <= '${qryParams.dtFimValidacao}'" )
//            qryParams.remove('dtFimValidacao')
//        }
//
//        if( params.inPexHistFiltro )
//        {
//            if( !params.sqCategoriaHistFiltro ) {
//                where.push("historico is not null")
//                wherePexHist = " and a.st_possivelmente_extinta = '" + params.inPexHistFiltro.toUpperCase() + "'"
//            }
//            else
//            {
//                wherePexHist = " and ( ( a.st_possivelmente_extinta = '" + params.inPexHistFiltro.toUpperCase() + "' and a.sq_categoria_iucn = "+categoriaCR.id.toString()+") or a.sq_categoria_iucn <> "+categoriaCR.id.toString() + ")"
//            }
//        }
//
//        // GRUPO SALVE
//        if( params.sqGrupoSalveFiltro  )
//        {
//
//            String whereGrupo = ' ficha.sq_grupo_salve '
//            if( params.sqGrupoSalveFiltro.indexOf(',') > -1 )
//            {
//                whereGrupo += ' in ('+params.sqGrupoSalveFiltro+')'
//            }
//            else
//            {
//                whereGrupo += ' = '+params.sqGrupoSalveFiltro
//            }
//            where.push( whereGrupo )
//        }
//
//        // GRUPO
//        if( params.sqGrupoFiltro  )
//        {
//
//            String whereGrupo = ' ficha.sq_grupo '
//            if( params.sqGrupoFiltro.indexOf(',') > -1 )
//            {
//                whereGrupo += ' in ('+params.sqGrupoFiltro+')'
//            }
//            else
//            {
//                whereGrupo += ' = '+params.sqGrupoFiltro
//            }
//            where.push( whereGrupo )
//        }
//
//        // SUBGRUPO
//        if( params.sqSubgrupoFiltro  )
//        {
//
//            String whereGrupo = ' ficha.sq_subgrupo '
//            if( params.sqSubgrupoFiltro.indexOf(',') > -1 )
//            {
//                whereGrupo += ' in ('+params.sqSubgrupoFiltro+')'
//            }
//            else
//            {
//                whereGrupo += ' = '+params.sqSubgrupoFiltro
//            }
//            where.push( whereGrupo )
//        }
//
//        // BIOMA ( especie + subespecie )
//        if( params.sqBiomaFiltro  ) {
//            /*where.push( 'bioma.json_bioma is not null')
//            whereBioma = ' and ocorrencia_bioma.sq_bioma'
//            if( params.sqBiomaFiltro.indexOf(',') > -1 )
//            {
//                whereBioma += ' in ('+params.sqBiomaFiltro+')'
//            }
//            else
//            {
//                whereBioma += ' = ' + params.sqBiomaFiltro
//            }
//            */
//
//            // a consulta sql não pode ter ponto de interrogação
//            /*if( params.sqBiomaFiltro.indexOf(',') > -1 )
//            {
//                whereBioma += ' ?| array[' + params.sqBiomaFiltro +']'
//            }
//            else
//            {
//                whereBioma = " and bioma.json_bioma ?? '20'"
//            }
//            */
//                String strSqlOr = ''
//                if( params.sqBiomaFiltro.split(',').contains('0') ) {
//                    strSqlOr = ' OR NOT EXISTS ( select null FROM salve.ficha_ocorrencia ocorrencia_bioma where ocorrencia_bioma.sq_ficha = ficha.sq_ficha and ocorrencia_bioma.sq_contexto = :sqContextoBioma )'
//                }
//                where.add("""( EXISTS ( select null FROM salve.ficha_ocorrencia ocorrencia_bioma
//                INNER JOIN salve.ficha as ficha_1 ON ficha_1.sq_ficha = ocorrencia_bioma.sq_ficha
//                INNER JOIN salve.ciclo_avaliacao as ciclo on ciclo.sq_ciclo_avaliacao = ficha_1.sq_ciclo_avaliacao
//                INNER JOIN taxonomia.taxon ON taxon.sq_taxon = ficha_1.sq_taxon
//                WHERE ( taxon.sq_taxon = ficha.sq_taxon OR taxon.sq_taxon_pai = ficha.sq_taxon)
//                and ficha_1.sq_ciclo_avaliacao = ficha.sq_ciclo_avaliacao
//                and ocorrencia_bioma.sq_contexto = :sqContextoBioma
//                and ocorrencia_bioma.sq_bioma in (${ params.sqBiomaFiltro }) )${strSqlOr})""")
//        }
//
//        // BACIA HIDROGRÁFICA ( especie + subespecie )
//        if( params.sqBaciaHidrograficaFiltro  ){
//                String strSqlOr = ''
//                if( params.sqBaciaHidrograficaFiltro.split(',').contains('0') ) {
//                    strSqlOr = ' OR NOT EXISTS ( select null FROM salve.ficha_ocorrencia ocorrencia_bacia'
//                    + ' where ocorrencia_bacia.sq_ficha = ficha.sq_ficha and ocorrencia_bacia.sq_contexto = :sqContextoBacia )'
//                }
//                where.add("""( EXISTS ( select null FROM salve.ficha_ocorrencia ocorrencia_bacia
//                INNER JOIN salve.ficha as ficha_1 ON ficha_1.sq_ficha = ocorrencia_bacia.sq_ficha
//                INNER JOIN salve.ciclo_avaliacao as ciclo on ciclo.sq_ciclo_avaliacao = ficha_1.sq_ciclo_avaliacao
//                INNER JOIN taxonomia.taxon ON taxon.sq_taxon = ficha_1.sq_taxon
//                WHERE ( taxon.sq_taxon = ficha.sq_taxon OR taxon.sq_taxon_pai = ficha.sq_taxon)
//                and ficha_1.sq_ciclo_avaliacao = ficha.sq_ciclo_avaliacao
//                and ocorrencia_bacia.sq_contexto = :sqContextoBacia
//                and ocorrencia_bacia.sq_bacia_hidrografica in (${ params.sqBaciaHidrograficaFiltro }) )${strSqlOr})""")
//        }
//
//        // ESTADO ( especie + subespecie )
//        if( params.sqEstadoFiltro  ) {
//            /*where.push( 'estado.json_estado is not null')
//            whereEstado = ' and ficha_oco_estado.sq_estado'
//            if( params.sqEstadoFiltro.indexOf(',') > -1 )
//            {
//                whereEstado += ' in ('+params.sqEstadoFiltro+')'
//            }
//            else
//            {
//                whereEstado += ' = ' + params.sqEstadoFiltro
//            }
//            */
//            // este filtro no objeto json não funciona por causa do ponto de interrogação na clausula where que dá conflito
//            //where.push("estado.json_estado::jsonb ?| array[${params.sqEstadoFiltro.split(',').collect{"'"+it+"'"}.join(',' ) } ]")
//            where.push("""EXISTS ( SELECT NULL FROM salve.ficha_ocorrencia ficha_oco_estado
//                INNER JOIN salve.ficha as ficha_1 ON ficha_1.sq_ficha = ficha_oco_estado.sq_ficha
//                INNER JOIN salve.ciclo_avaliacao as ciclo on ciclo.sq_ciclo_avaliacao = ficha_1.sq_ciclo_avaliacao
//                INNER JOIN taxonomia.taxon ON taxon.sq_taxon = ficha_1.sq_taxon
//                WHERE ( taxon.sq_taxon = ficha.sq_taxon OR taxon.sq_taxon_pai = ficha.sq_taxon)
//                  AND ficha_1.sq_ciclo_avaliacao = ficha.sq_ciclo_avaliacao
//                  AND ficha_oco_estado.sq_contexto = :sqContextoEstado
//                  AND ficha_oco_estado.sq_estado in (${params.sqEstadoFiltro}) limit 1 )""")
//        }
//
//        // UC ( especie + subespecie )
//        if( params.sqUcFiltro  ) {
//
//            /*
//            where.push( 'uc.json_uc is not null')
//            if( params.sqUcFiltro.indexOf(',') > -1 )
//            {
//                Map mapTemp = [E:[],F:[],R:[]]
//                params.sqUcFiltro.split(',').each{
//                    if( it =~/F$/ )
//                    {
//                        mapTemp.F.push( it.replaceAll('F','') )
//                    }
//                    else if( it =~/E$/ )
//                    {
//                        mapTemp.E.push( it.replaceAll('E','') )
//                    }
//                    else if( it =~/R$/)
//                    {
//                        mapTemp.R.push( it.replaceAll('R','') )
//                    }
//                }
//                if( mapTemp.F )
//                {
//                    whereUcFederal = "and uo.sq_pessoa in (" + mapTemp.F.join(',') + ')'
//                }
//                if( mapTemp.E )
//                {
//                    whereUcEstadual = "and uc.gid in (" + mapTemp.E.join(',') + ')'
//                }
//                if( mapTemp.R )
//                {
//                    whereUcRppn = "and uc.ogc_fid in (" + mapTemp.R.join(',') + ')'
//                }
//            }
//            else
//            {
//                if( params.sqUcFiltro =~/F$/)
//                {
//                    whereUcFederal = "and uo.sq_pessoa = "  + params.sqUcFiltro.replaceAll('F','')
//                }
//                else if( params.sqUcFiltro =~/E$/)
//                {
//                    whereUcEstadual = "and uc.gid = "  + params.sqUcFiltro.replaceAll('E','')
//                }
//                else if( params.sqUcFiltro =~/R$/)
//                {
//                    whereUcRppn = "and uc.ogc_fid = "  + params.sqUcFiltro.replaceAll('R','')
//                }
//            }
//            */
//            //params.sqUcFiltro = '325717F,3400F,3367F,3379F,3396F,3382F,3401F,3387F,3404F,3375F,3372F,3367F,3349F,3401F'
//            Map mapTemp = [E:[],F:[],R:[]]
//            if( params.sqUcFiltro.indexOf(',') > -1 )
//            {
//                params.sqUcFiltro.split(',').each{
//                    if( it =~/F$/ )
//                    {
//                        mapTemp.F.push( it.replaceAll('F','') )
//                    }
//                    else if( it =~/E$/ )
//                    {
//                        mapTemp.E.push( it.replaceAll('E','') )
//                    }
//                    else if( it =~/R$/)
//                    {
//                        mapTemp.R.push( it.replaceAll('R','') )
//                    }
//                }
//            }
//            else
//            {
//                if( params.sqUcFiltro =~/F$/)
//                {
//                    mapTemp.F.push(params.sqUcFiltro.replaceAll('F','') )
//                }
//                else if( params.sqUcFiltro =~/E$/)
//                {
//                    mapTemp.E.push( params.sqUcFiltro.replaceAll('E','') )
//                }
//                else if( params.sqUcFiltro =~/R$/)
//                {
//                    mapTemp.R.push( params.sqUcFiltro.replaceAll('R','') )
//                }
//            }
//
//            if( mapTemp.F )
//            {
//                where.push("""EXISTS (SELECT null FROM salve.ficha_ocorrencia ficha_oco_uc
//                            INNER JOIN salve.ficha as ficha_1 ON ficha_1.sq_ficha = ficha_oco_uc.sq_ficha
//                            INNER JOIN salve.ciclo_avaliacao as ciclo on ciclo.sq_ciclo_avaliacao = ficha_1.sq_ciclo_avaliacao
//                            INNER JOIN taxonomia.taxon ON taxon.sq_taxon = ficha_1.sq_taxon
//                            WHERE ( taxon.sq_taxon = ficha.sq_taxon OR taxon.sq_taxon_pai = ficha.sq_taxon)
//                            and  ficha_1.sq_ciclo_avaliacao = ficha.sq_ciclo_avaliacao
//                            and ficha_oco_uc.in_utilizado_avaliacao = 'S'
//                            and ficha_oco_uc.sq_contexto =:sqContextoOcorrencia and ficha_oco_uc.sq_uc_federal in (${mapTemp.F.join(',')}) LIMIT 1 )""")
//            }
//            if( mapTemp.E )
//            {
//               // whereUcEstadual = "and uc.gid in (" + mapTemp.E.join(',') + ')'
//                where .push("""EXISTS (SELECT NULL FROM salve.ficha_ocorrencia ficha_oco_uc
//                            INNER JOIN salve.ficha as ficha_1 ON ficha_1.sq_ficha = ficha_oco_uc.sq_ficha
//                            INNER JOIN salve.ciclo_avaliacao as ciclo on ciclo.sq_ciclo_avaliacao = ficha_1.sq_ciclo_avaliacao
//                            INNER JOIN taxonomia.taxon ON taxon.sq_taxon = ficha_1.sq_taxon
//                            WHERE ( taxon.sq_taxon = ficha.sq_taxon OR taxon.sq_taxon_pai = ficha.sq_taxon)
//                            and  ficha_1.sq_ciclo_avaliacao = ficha.sq_ciclo_avaliacao
//                            and ficha_oco_uc.in_utilizado_avaliacao = 'S'
//                            and ficha_oco_uc.sq_contexto = :sqContextoOcorrencia AND ficha_oco_uc.sq_uc_estadual IN (${mapTemp.E.join(',')}) LIMIT 1)""")
//            }
//            if( mapTemp.R )
//            {
//                //whereUcRppn = "and uc.ogc_fid in (" + mapTemp.R.join(',') + ')'
//                where.push("""EXISTS (SELECT NULL FROM salve.ficha_ocorrencia ficha_oco_uc
//                            INNER JOIN salve.ficha as ficha_1 ON ficha_1.sq_ficha = ficha_oco_uc.sq_ficha
//                            INNER JOIN salve.ciclo_avaliacao as ciclo on ciclo.sq_ciclo_avaliacao = ficha_1.sq_ciclo_avaliacao
//                            INNER JOIN taxonomia.taxon ON taxon.sq_taxon = ficha_1.sq_taxon
//                            WHERE ( taxon.sq_taxon = ficha.sq_taxon OR taxon.sq_taxon_pai = ficha.sq_taxon)
//                            and ficha_1.sq_ciclo_avaliacao = ficha.sq_ciclo_avaliacao
//                            and ficha_oco_uc.in_utilizado_avaliacao = 'S'
//                            and ficha_oco_uc.sq_contexto = :sqContextoOcorrencia AND ficha_oco_uc.sq_rppn IN (${mapTemp.R.join(',')}) LIMIT 1)""")
//            }
//            /*where ficha_oco_uc.sq_ficha = ficha.sq_ficha and ficha_oco_uc.sq_uc_federal is not null
//            $whereUcFederal
//            AND ficha_oco_uc.sq_contexto =:sqContextoOcorrencia
//            */
//        }
//
//        // ESPECIE MIGRATORIA
//        if( params.stMigratoriaFiltro  ){
//            String whereMigratoria = ' ficha.st_migratoria '
//            if( params.stMigratoriaFiltro.indexOf(',') > -1 )
//            {
//                whereMigratoria += ' in ('+params.stMigratoriaFiltro.split(',').collect{ "'" + it + "'"}.join(',')  +')'
//            }
//            else
//            {
//                whereMigratoria += " = '"+params.stMigratoriaFiltro + "'"
//            }
//            where.push( whereMigratoria )
//        }
//
//        // TENDENCIA POPULACIONAL
//        if( params.sqTendenciaPopulacionalFiltro  ){
//            String whereTendenciaPopulacional = ' ficha.sq_tendencia_populacional '
//            if( params.sqTendenciaPopulacionalFiltro.indexOf(',') > -1 )
//            {
//                whereTendenciaPopulacional += ' in ('+params.sqTendenciaPopulacionalFiltro+')'
//            }
//            else
//            {
//                whereTendenciaPopulacional += ' = '+params.sqTendenciaPopulacionalFiltro
//            }
//            where.push( whereTendenciaPopulacional )
//        }
//
//        // MULTIMIDIA
//        if( params.stMultimidiaFiltro ) {
//            String whereMultimidia = ''
//            if (params.stMultimidiaFiltro == 'COM_IMAGEM') {
//                whereMultimidia = "EXISTS ( select dados_apoio.cd_sistema from salve.ficha_multimidia, salve.dados_apoio where dados_apoio.sq_dados_apoio = ficha_multimidia.sq_tipo and dados_apoio.cd_sistema = 'IMAGEM' AND SQ_FICHA = ficha.sq_ficha LIMIT 1)"
//            } else if (params.stMultimidiaFiltro == 'COM_AUDIO') {
//                whereMultimidia = "EXISTS ( select dados_apoio.cd_sistema from salve.ficha_multimidia, salve.dados_apoio where dados_apoio.sq_dados_apoio = ficha_multimidia.sq_tipo and dados_apoio.cd_sistema = 'SOM' AND SQ_FICHA = ficha.sq_ficha LIMIT 1)"
//            } else if (params.stMultimidiaFiltro == 'COM_IMAGEM_OU_AUDIO') {
//                whereMultimidia = "EXISTS ( select dados_apoio.cd_sistema from salve.ficha_multimidia, salve.dados_apoio where dados_apoio.sq_dados_apoio = ficha_multimidia.sq_tipo and dados_apoio.cd_sistema IN ('IMAGEM','SOM') AND SQ_FICHA = ficha.sq_ficha LIMIT 1)"
//            } else if (params.stMultimidiaFiltro == 'COM_IMAGEM_E_AUDIO') {
//                whereMultimidia = "EXISTS ( select dados_apoio.cd_sistema from salve.ficha_multimidia, salve.dados_apoio where dados_apoio.sq_dados_apoio = ficha_multimidia.sq_tipo and dados_apoio.cd_sistema = 'IMAGEM' AND SQ_FICHA = ficha.sq_ficha LIMIT 1)"
//                whereMultimidia += " AND EXISTS ( select dados_apoio.cd_sistema from salve.ficha_multimidia, salve.dados_apoio where dados_apoio.sq_dados_apoio = ficha_multimidia.sq_tipo and dados_apoio.cd_sistema = 'SOM' AND SQ_FICHA = ficha.sq_ficha LIMIT 1)"
//            } else if (params.stMultimidiaFiltro == 'SEM_IMAGEM') {
//                whereMultimidia = "NOT EXISTS ( select dados_apoio.cd_sistema from salve.ficha_multimidia, salve.dados_apoio where dados_apoio.sq_dados_apoio = ficha_multimidia.sq_tipo and dados_apoio.cd_sistema = 'IMAGEM' AND SQ_FICHA = ficha.sq_ficha LIMIT 1)"
//            } else if (params.stMultimidiaFiltro == 'SEM_AUDIO') {
//                whereMultimidia = "NOT EXISTS ( select dados_apoio.cd_sistema from salve.ficha_multimidia, salve.dados_apoio where dados_apoio.sq_dados_apoio = ficha_multimidia.sq_tipo and dados_apoio.cd_sistema = 'SOM' AND SQ_FICHA = ficha.sq_ficha LIMIT 1)"
//            } else if (params.stMultimidiaFiltro == 'SEM_IMAGEM_OU_AUDIO') {
//                whereMultimidia = "( NOT EXISTS ( select dados_apoio.cd_sistema from salve.ficha_multimidia, salve.dados_apoio where dados_apoio.sq_dados_apoio = ficha_multimidia.sq_tipo and dados_apoio.cd_sistema = 'IMAGEM' AND SQ_FICHA = ficha.sq_ficha LIMIT 1) "
//                whereMultimidia += " OR NOT EXISTS ( select dados_apoio.cd_sistema from salve.ficha_multimidia, salve.dados_apoio where dados_apoio.sq_dados_apoio = ficha_multimidia.sq_tipo and dados_apoio.cd_sistema = 'SOM' AND SQ_FICHA = ficha.sq_ficha LIMIT 1) ) "
//            } else if (params.stMultimidiaFiltro == 'SEM_IMAGEM_E_AUDIO') {
//                whereMultimidia = "NOT EXISTS ( select dados_apoio.cd_sistema from salve.ficha_multimidia, salve.dados_apoio where dados_apoio.sq_dados_apoio = ficha_multimidia.sq_tipo and dados_apoio.cd_sistema IN ('IMAGEM','SOM') AND SQ_FICHA = ficha.sq_ficha LIMIT 1)"
//            }
//            if (whereMultimidia) {
//                where.push(whereMultimidia)
//            }
//        }
//
//        // MAPA DE DISTRIBUICAO
//        if( params.stMapaDistribuicao ) {
//            String whereMapaDistribuicao = ''
//            if (params.stMapaDistribuicao == 'COM_IMAGEM') {
//                whereMapaDistribuicao = "EXISTS ( select x.sq_ficha_anexo from salve.ficha_anexo x where x.sq_ficha = ficha.sq_ficha and x.de_tipo_conteudo ilike '%image%' LIMIT 1)"
//            }
//            else if (params.stMapaDistribuicao == 'SEM_IMAGEM') {
//                whereMapaDistribuicao = "NOT EXISTS ( select x.sq_ficha_anexo from salve.ficha_anexo x where x.sq_ficha = ficha.sq_ficha and x.de_tipo_conteudo ilike '%image%' LIMIT 1)"
//            }
//            if (whereMapaDistribuicao) {
//                where.push(whereMapaDistribuicao)
//            }
//        }
//
//        // OCORRENCIA EM UC
//        if( params.stOcorreEmUc ) {
//            if( params.stOcorreEmUc == 'S' )
//            {
//                where.push("uc.json_uc is NOT null")
//            }
//            else if( params.stOcorreEmUc == 'N' )
//            {
//                where.push("uc.json_uc is null")
//            }
//        }
//
//        // AMEACA  ( especie + subespecie )
//        if( params.sqAmeacaFiltro  ) {
//            // pesquisar pela hierarquia 1, 1.1, 1.1.1, se selecionar o item 1, ler todos os filhos
//            List listConditions = []
//            DadosApoio.createCriteria().list{
//                'in'('id', params.sqAmeacaFiltro.split(',').collect{ it.toLong() } )
//            }.each {
//                listConditions.push( "de_dados_apoio_ordem = '${it.ordem}' or de_dados_apoio_ordem like '${it.ordem}.%'")
//            }
//            // considerar as ameaças da especie e subespecies
//            String sqlIdsValidos="select sq_dados_apoio from salve.dados_apoio where "+listConditions.join(' or ')
//            where.push( """EXISTS ( select NULL from salve.ficha_ameaca
//                                INNER JOIN salve.ficha as ficha_1 ON ficha_1.sq_ficha = ficha_ameaca.sq_ficha
//                                INNER JOIN salve.ciclo_avaliacao as ciclo on ciclo.sq_ciclo_avaliacao = ficha_1.sq_ciclo_avaliacao
//                                INNER JOIN taxonomia.taxon ON taxon.sq_taxon = ficha_1.sq_taxon
//                                WHERE ( taxon.sq_taxon = ficha.sq_taxon OR taxon.sq_taxon_pai = ficha.sq_taxon)
//                                AND ficha_1.sq_ciclo_avaliacao = ficha.sq_ciclo_avaliacao
//                                AND ficha_ameaca.sq_criterio_ameaca_iucn in ( ${sqlIdsValidos} ) LIMIT 1 )""")
//            //where.push( """EXISTS ( select NULL from salve.ficha_ameaca WHERE ficha_ameaca.sq_ficha = ficha.sq_ficha AND ficha_ameaca.sq_criterio_ameaca_iucn in ( ${params.sqAmeacaFiltro} ) LIMIT 1 )""")
//        }
//
//        // USO ( especie + subespecie )
//        if( params.sqUsoFiltro  ) {
//            // pesquisar pela hierarquia 1, 1.1, 1.1.1, se selecionar o item 1, ler todos os filhos
//            List listConditions = []
//            Uso.createCriteria().list{
//                'in'('id', params.sqUsoFiltro.split(',').collect{ it.toLong() } )
//            }.each {
//                listConditions.push( "nu_ordem = '${it.ordem}' or nu_ordem like '${it.ordem}.%'")
//            }
//            // considerar os usos da Espécie e subespecies
//            String sqlIdsValidos="select sq_uso from salve.uso where "+listConditions.join(' or ')
//            where.push("""EXISTS ( SELECT null from salve.ficha_uso
//                                INNER JOIN salve.ficha as ficha_1 ON ficha_1.sq_ficha = ficha_uso.sq_ficha
//                                INNER JOIN salve.ciclo_avaliacao as ciclo on ciclo.sq_ciclo_avaliacao = ficha_1.sq_ciclo_avaliacao
//                                INNER JOIN taxonomia.taxon ON taxon.sq_taxon = ficha_1.sq_taxon
//                                WHERE ( taxon.sq_taxon = ficha.sq_taxon OR taxon.sq_taxon_pai = ficha.sq_taxon)
//                                AND ficha_1.sq_ciclo_avaliacao = ficha.sq_ciclo_avaliacao
//                                AND ficha_uso.sq_uso in (${sqlIdsValidos}) LIMIT 1 )""")
//            //where.push("""EXISTS ( SELECT null from salve.ficha_uso where ficha_uso.sq_ficha = ficha.sq_ficha and ficha_uso.sq_uso in (${params.sqUsoFiltro}) LIMIT 1 )""")
//        }
//
//        // LISTAS E CONVENÇÕES
//        if( params.sqListaFiltro  ){
//
//            /*where.push( 'lista_convencao.json_lista_convencao is not null')
//            whereLista = ' and ficha_lista_convencao.sq_lista_convencao'
//            if( params.sqListaFiltro.indexOf(',') > -1 )
//            {
//                whereLista += ' in ('+params.sqListaFiltro+')'
//            }
//            else
//            {
//                whereLista += ' = ' + params.sqListaFiltro
//            }
//            */
//            where.push("""EXISTS ( select null from salve.ficha_lista_convencao where ficha_lista_convencao.sq_ficha = ficha.sq_ficha and ficha_lista_convencao.sq_lista_convencao in (${params.sqListaFiltro}) LIMIT 1 )""")
//
//        }
//
//        // PRESENÇA LISTA OFICIAL
//        if( params.stListaOficial ) {
//            String whereListaOficial
//            if( params.stListaOficial == 'S') {
//                whereListaOficial = "ficha.st_presenca_lista_vigente='S'"
//            } else {
//                whereListaOficial = "( ficha.st_presenca_lista_vigente is null or ficha.st_presenca_lista_vigente = 'N' )"
//            }
//            where.push( whereListaOficial )
//        }
//
//        // AÇÕES DE CONSERVAÇÃO / INSTRUMENTO DE GESTÃO
//        if( params.sqAcaoFiltro  ) {
//            // pesquisar pela hierarquia 1, 1.1, 1.1.1, se selecionar o item 1, ler todos os filhos
//            List listConditions = []
//            DadosApoio.createCriteria().list{
//                'in'('id', params.sqAcaoFiltro.split(',').collect{ it.toLong() } )
//            }.each {
//                listConditions.push( "de_dados_apoio_ordem = '${it.ordem}' or de_dados_apoio_ordem like '${it.ordem}.%'")
//            }
//            String sqlIdsValidos="select sq_dados_apoio from salve.dados_apoio where "+listConditions.join(' or ')
//            where.push("""EXISTS ( select null from salve.ficha_acao_conservacao acoes where acoes.sq_ficha = ficha.sq_ficha and acoes.sq_acao_conservacao in (${sqlIdsValidos}) LIMIT 1 )""")
//            //where.push("""EXISTS ( select null from salve.ficha_acao_conservacao acoes where acoes.sq_ficha = ficha.sq_ficha and acoes.sq_acao_conservacao in (${params.sqAcaoFiltro}) LIMIT 1 )""")
//        }
//
//        // ENDEMICA
//        if( params.stEndemicaFiltro  ) {
//            where.push('ficha.st_endemica_brasil = :stEndemica')
//            qryParams.stEndemica = params.stEndemicaFiltro
//        }
//
//        List sqlUcs = []
//
//        String sqlCmd="""
//            SELECT
//              ciclo_avaliacao.nu_ano as nu_ano_ciclo_inicio
//            , (ciclo_avaliacao.nu_ano::integer + 4::integer) as nu_ano_ciclo_fim
//            , ciclo_avaliacao.de_ciclo_avaliacao
//            , coalesce( taxon.json_trilha->'subespecie'->>'no_taxon', taxon.json_trilha->'especie'->>'no_taxon') as nm_cientifico
//            , taxon.no_autor
//            , taxon.nu_ano as nu_ano_taxon
//
//            -- nome especie ciclo anterior
//            , case when ficha_ciclo_anterior.sq_taxon is not null and ficha_ciclo_anterior.sq_taxon = ficha.sq_taxon then '' else coalesce( taxon_ciclo_anterior.json_trilha->'subespecie'->>'no_taxon', taxon_ciclo_anterior.json_trilha->'especie'->>'no_taxon') end as nm_cientifico_anterior
//            , case when ficha_ciclo_anterior.sq_taxon is not null and ficha_ciclo_anterior.sq_taxon = ficha.sq_taxon then '' else taxon_ciclo_anterior.no_autor end as no_autor_anterior
//            , case when ficha_ciclo_anterior.sq_taxon is not null and ficha_ciclo_anterior.sq_taxon = ficha.sq_taxon then null else taxon_ciclo_anterior.nu_ano end as nu_ano_anterior
//
//            -- situacao da ficha
//            , situacao_ficha.ds_dados_apoio as de_situacao_ficha
//
//            -- grupo
//            , grupo.ds_dados_apoio as de_grupo
//            , grupo.cd_sistema as cd_grupo_sistema
//
//            -- subgrupo
//            , subgrupo.ds_dados_apoio as de_subgrupo
//            , subgrupo.cd_sistema as cd_subgrupo_sistema
//
//            -- grupo salve
//            , grupoSalve.ds_dados_apoio as de_grupo_salve
//            , grupoSalve.cd_sistema as cd_grupo_salve_sistema
//
//            -- historia natural - Migratoria S/N
//            , salve.snd( ficha.st_migratoria ) as ds_migratoria
//
//            -- tendencia populacional
//            , tendenciaPopulacional.sq_dados_apoio as sq_tendencia_populacional
//            , tendenciaPopulacional.ds_dados_apoio as ds_tendencia_populacional
//
//            -- endemica
//            , ficha.st_endemica_brasil
//            , salve.snd(ficha.st_endemica_brasil) as de_endemica_brasil
//
//            -- presenca em lista vigente - Protegido Legislação
//            , ficha.st_presenca_lista_vigente
//            , salve.snd(ficha.st_presenca_lista_vigente) as de_presenca_lista_vigente
//
//            -- avaliacao oficina - quando nao houver dados na oficina e tiver na ficha, considerar a ficha
//            ,coalesce( oficina_ficha.cd_categoria_oficina, categoria_ficha.cd_dados_apoio ) cd_categoria_oficina
//            ,coalesce( oficina_ficha.de_categoria_oficina, categoria_ficha.ds_dados_apoio ) as de_categoria_oficina
//            ,coalesce( oficina_ficha.de_nivel_categoria_oficina, categoria_ficha.cd_dados_apoio_nivel ) as de_nivel_categoria_oficina
//            ,coalesce( oficina_ficha.de_criterio_oficina, ficha.ds_criterio_aval_iucn ) as de_criterio_oficina
//            ,coalesce( oficina_ficha.st_possivelmente_extinta_oficina, ficha.st_possivelmente_extinta ) as st_possivelmente_extinta_oficina
//            ,coalesce( oficina_ficha.ds_justificativa_oficina, ficha.ds_justificativa) as ds_justificativa_oficina
//            ,coalesce( oficina_ficha.cd_categoria_oficina_sistema, categoria_ficha.cd_sistema ) as cd_categoria_oficina_sistema
//            ,coalesce( oficina_ficha.cd_categoria_oficina_sistema, categoria_ficha.cd_sistema ) as cd_categoria_oficina_sistema
//            ,case when oficina_ficha.dt_inicio is null then '' else
//                    concat( to_char( oficina_ficha.dt_inicio,'dd/mm/yyyy'),' a ', to_char( oficina_ficha.dt_fim,'dd/mm/yyyy') )
//            end as de_periodo_avaliacao
//
//            -- avaliacao final
//            , categoria_final.ds_dados_apoio as de_categoria_final
//            , categoria_final.cd_dados_apoio as cd_categoria_final
//            , categoria_final.cd_dados_apoio_nivel as de_nivel_categoria_final
//            , ficha.ds_criterio_aval_iucn_final    as de_criterio_final
//            , ficha.st_possivelmente_extinta_final as st_possivelmente_extinta_final
//            , salve.remover_html_tags(ficha.ds_justificativa_final) as ds_justificativa_final
//            , categoria_final.cd_sistema           as cd_categoria_final_sistema
//            , case when oficina_validacao.dt_inicio is null then '' else
//                    concat( to_char( oficina_validacao.dt_inicio,'dd/mm/yyyy'),' a ', to_char( oficina_validacao.dt_fim,'dd/mm/yyyy') )
//            end as de_periodo_validacao
//
//
//            -- colunas json (n)
//            , nomeComum.no_comum
//            , motivoMudanca.de_motivo_mudanca
//            , historico.json_historico
//            , taxon.json_trilha
//            , bioma.json_bioma
//            , bacia.json_bacia
//            , ameaca.json_ameaca
//            , uso.json_uso
//            , acao_conservacao.json_acao_conservacao
//            , plano_acao.json_plano_acao
//            , lista_convencao.json_lista_convencao
//            , uc.json_uc
//            , estado.json_estado
//
//            -- unidade responsavel
//            , unidadeOrg.sg_unidade_org
//            , ficha.dt_aceite_validacao
//
//            FROM salve.ficha
//            LEFT OUTER JOIN salve.dados_apoio as categoria_ficha on categoria_ficha.sq_dados_apoio = ficha.sq_categoria_iucn
//
//            -- nomes comuns
//            CROSS JOIN LATERAL (
//                select array_to_string( array_agg( ficha_nome_comum.no_comum ),', ') as no_comum
//                from salve.ficha_nome_comum
//                where sq_ficha = ficha.sq_ficha
//            ) nomeComum
//
//            -- motivos da mudança de categoria
//            CROSS JOIN LATERAL
//            (
//                select array_to_string( array_agg(motivo_mudanca.ds_dados_apoio),', ') as de_motivo_mudanca
//                from salve.ficha_mudanca_categoria
//                inner join salve.dados_apoio as motivo_mudanca on motivo_mudanca.sq_dados_apoio = ficha_mudanca_categoria.sq_motivo_mudanca
//                where sq_ficha = ficha.sq_ficha
//            ) motivoMudanca
//
//            CROSS JOIN LATERAL (
//             select json_object_agg( apoio_bioma.sq_dados_apoio, json_build_object('de_bioma', apoio_bioma.ds_dados_apoio)) AS json_bioma
//                        FROM salve.ficha_ocorrencia AS fo
//                        INNER JOIN salve.ficha as ficha_1 ON ficha_1.sq_ficha = fo.sq_ficha
//                        INNER JOIN salve.ciclo_avaliacao as ciclo on ciclo.sq_ciclo_avaliacao = ficha_1.sq_ciclo_avaliacao
//                        INNER JOIN taxonomia.taxon ON taxon.sq_taxon = ficha_1.sq_taxon
//                        INNER JOIN salve.dados_apoio apoio_bioma ON apoio_bioma.sq_dados_apoio = fo.sq_bioma
//                        WHERE ( taxon.sq_taxon = ficha.sq_taxon OR taxon.sq_taxon_pai = ficha.sq_taxon)
//                          and ficha_1.sq_ciclo_avaliacao = ficha.sq_ciclo_avaliacao
//                          AND fo.sq_contexto  = :sqContextoBioma
//            ) bioma
//
//            CROSS JOIN LATERAL (
//               select json_object_agg( bacia.id, json_build_object('de_bacia_hidrografica', bacia.descricao,'json_trilha',bacia.trilha,'ordem',bacia.ordem)) AS json_bacia
//                FROM salve.ficha_ocorrencia AS fo
//                INNER JOIN salve.ficha as ficha_1 ON ficha_1.sq_ficha = fo.sq_ficha
//                INNER JOIN salve.ciclo_avaliacao as ciclo on ciclo.sq_ciclo_avaliacao = ficha_1.sq_ciclo_avaliacao
//                INNER JOIN taxonomia.taxon ON taxon.sq_taxon = ficha_1.sq_taxon
//                INNER JOIN salve.vw_bacia_hidrografica bacia ON bacia.id = fo.sq_bacia_hidrografica
//                WHERE ( taxon.sq_taxon = ficha.sq_taxon OR taxon.sq_taxon_pai = ficha.sq_taxon)
//                  and ficha_1.sq_ciclo_avaliacao = ficha.sq_ciclo_avaliacao
//                  AND fo.sq_contexto  = :sqContextoBacia
//            ) bacia
//
//
//            CROSS JOIN LATERAL
//            (  select json_object_agg( apoio_ameaca.sq_dados_apoio, json_build_object( 'de_ameaca', apoio_ameaca.ds_dados_apoio, 'cd_dados_apoio',apoio_ameaca.cd_dados_apoio)) AS json_ameaca
//               from salve.ficha_ameaca
//               INNER JOIN salve.ficha as ficha_1 ON ficha_1.sq_ficha = ficha_ameaca.sq_ficha
//               INNER JOIN salve.ciclo_avaliacao as ciclo on ciclo.sq_ciclo_avaliacao = ficha_1.sq_ciclo_avaliacao
//               INNER JOIN taxonomia.taxon ON taxon.sq_taxon = ficha_1.sq_taxon
//               LEFT OUTER JOIN salve.dados_apoio as apoio_ameaca on apoio_ameaca.sq_dados_apoio = ficha_ameaca.sq_criterio_ameaca_iucn
//               WHERE ( taxon.sq_taxon = ficha.sq_taxon OR taxon.sq_taxon_pai = ficha.sq_taxon)
//               AND ficha_1.sq_ciclo_avaliacao = ficha.sq_ciclo_avaliacao
//            ) ameaca
//
//            CROSS JOIN LATERAL
//            (      select json_object_agg( uso.sq_uso, json_build_object('sq_uso', uso.sq_uso, 'de_uso', uso.ds_uso)) AS json_uso
//                   from salve.ficha_uso
//                   INNER JOIN salve.ficha as ficha_1 ON ficha_1.sq_ficha = ficha_uso.sq_ficha
//                   INNER JOIN salve.ciclo_avaliacao as ciclo on ciclo.sq_ciclo_avaliacao = ficha_1.sq_ciclo_avaliacao
//                   INNER JOIN taxonomia.taxon ON taxon.sq_taxon = ficha_1.sq_taxon
//                   LEFT OUTER JOIN salve.uso on uso.sq_uso = ficha_uso.sq_uso
//                   WHERE ( taxon.sq_taxon = ficha.sq_taxon OR taxon.sq_taxon_pai = ficha.sq_taxon)
//                   AND ficha_1.sq_ciclo_avaliacao = ficha.sq_ciclo_avaliacao
//            ) uso
//
//            CROSS JOIN LATERAL
//            (  select json_object_agg( apoio_acao.sq_dados_apoio, json_build_object(
//                    'de_acao', coalesce(apoio_acao.ds_dados_apoio,'')
//                  , 'sq_situacao',acoes.sq_situacao_acao_conservacao
//                  , 'ds_situacao',coalesce(apoio_acao_situacao.ds_dados_apoio,'')
//                  , 'sq_tipo_ordenamento', acoes.sq_tipo_ordenamento
//                  , 'ds_tipo_ordenamento', coalesce(apoio_tipo_ordenamento.ds_dados_apoio,'')
//                  )) AS json_acao_conservacao
//                  FROM salve.ficha_acao_conservacao acoes
//                  left outer join salve.dados_apoio as apoio_acao on apoio_acao.sq_dados_apoio = acoes.sq_acao_conservacao
//                  left outer join salve.dados_apoio as apoio_acao_situacao on apoio_acao_situacao.sq_dados_apoio = acoes.sq_situacao_acao_conservacao
//                  left outer join salve.dados_apoio as apoio_tipo_ordenamento on apoio_tipo_ordenamento.sq_dados_apoio = acoes.sq_tipo_ordenamento
//                  WHERE acoes.sq_ficha = ficha.sq_ficha
//             ) acao_conservacao
//
//             CROSS JOIN LATERAL
//                  (SELECT json_object_agg(plano_acao.sq_plano_acao, json_build_object('de_plano_acao',coalesce(plano_acao.sg_plano_acao,plano_acao.tx_plano_acao), 'ds_situacao',coalesce(apoio_acao_situacao.ds_dados_apoio,'') ) ) AS json_plano_acao
//                   FROM salve.ficha_acao_conservacao acoes
//                   inner join salve.plano_acao ON plano_acao.sq_plano_acao = acoes.sq_plano_acao
//                   inner join salve.dados_apoio AS apoio_acao_situacao ON apoio_acao_situacao.sq_dados_apoio =acoes.sq_situacao_acao_conservacao
//                   WHERE acoes.sq_ficha = ficha.sq_ficha ) plano_acao
//
//            cross join lateral
//            (  select json_object_agg( apoio_lista_convencao.sq_dados_apoio, json_build_object(
//               'de_lista_convencao', coalesce(apoio_lista_convencao.ds_dados_apoio,'')
//                , 'nu_ano',ficha_lista_convencao.nu_ano
//                )) AS json_lista_convencao
//                from salve.ficha_lista_convencao
//                left outer join salve.dados_apoio as apoio_lista_convencao on apoio_lista_convencao.sq_dados_apoio = ficha_lista_convencao.sq_lista_convencao
//                where ficha_lista_convencao.sq_ficha = ficha.sq_ficha
//            ) lista_convencao
//
//            CROSS JOIN LATERAL
//            (  select json_object_agg( ucs.sq_uc, json_build_object('no_uc', ucs.no_uc,'sg_uc',ucs.sg_uc, 'in_esfera', ucs.in_esfera)) AS json_uc
//               from
//               (
//               """
//               if( whereUcFederal || !(whereUcEstadual+whereUcRppn) ) {
//                sqlUcs.push("""
//                --uc federal
//                select uo.sq_pessoa as sq_uc, pessoa.no_pessoa as no_uc, uo.sg_unidade_org as sg_uc, 'F' AS in_esfera
//                FROM salve.ficha_ocorrencia ficha_oco_uc
//                inner JOIN salve.vw_unidade_org uo ON uo.sq_pessoa = ficha_oco_uc.sq_uc_federal
//                      INNER JOIN salve.vw_pessoa pessoa on pessoa.sq_pessoa = uo.sq_pessoa
//                      INNER JOIN salve.ficha as ficha_1 ON ficha_1.sq_ficha = ficha_oco_uc.sq_ficha
//                      INNER JOIN salve.ciclo_avaliacao as ciclo on ciclo.sq_ciclo_avaliacao = ficha_1.sq_ciclo_avaliacao
//                      INNER JOIN taxonomia.taxon ON taxon.sq_taxon = ficha_1.sq_taxon
//                      WHERE ( taxon.sq_taxon = ficha.sq_taxon OR taxon.sq_taxon_pai = ficha.sq_taxon )
//                      and ficha_1.sq_ciclo_avaliacao = ficha.sq_ciclo_avaliacao
//                      and ficha_oco_uc.sq_uc_federal is not null
//                      and ficha_oco_uc.in_utilizado_avaliacao = 'S'
//                      and ficha_oco_uc.sq_contexto =:sqContextoOcorrencia
//                """)
//                }
//
//                if( whereUcEstadual || !(whereUcFederal+whereUcRppn) ) {
//                sqlUcs.push("""
//                  -- uc estadual
//                  select uc.gid, '', initcap( uc.nome_uc1 ), 'E'
//                  FROM salve.ficha_ocorrencia ficha_oco_uc
//                  inner JOIN geo.vw_uc_estadual uc ON uc.gid = ficha_oco_uc.sq_uc_estadual
//                      INNER JOIN salve.ficha as ficha_1 ON ficha_1.sq_ficha = ficha_oco_uc.sq_ficha
//                      INNER JOIN salve.ciclo_avaliacao as ciclo on ciclo.sq_ciclo_avaliacao = ficha_1.sq_ciclo_avaliacao
//                      INNER JOIN taxonomia.taxon ON taxon.sq_taxon = ficha_1.sq_taxon
//                      WHERE ( taxon.sq_taxon = ficha.sq_taxon OR taxon.sq_taxon_pai = ficha.sq_taxon )
//                      and ficha_1.sq_ciclo_avaliacao = ficha.sq_ciclo_avaliacao
//                      and ficha_oco_uc.sq_uc_estadual is not null
//                      and ficha_oco_uc.in_utilizado_avaliacao = 'S'
//                      and ficha_oco_uc.sq_contexto = :sqContextoOcorrencia
//                """)
//                }
//
//                if( whereUcRppn || !(whereUcEstadual+whereUcFederal) ) {
//                sqlUcs.push("""
//                  -- rppn
//                  select uc.ogc_fid, '', initcap( uc.nome ), 'R'
//                  FROM salve.ficha_ocorrencia ficha_oco_uc
//                  inner JOIN geo.vw_rppn uc ON uc.ogc_fid = ficha_oco_uc.sq_rppn
//                  INNER JOIN salve.ficha as ficha_1 ON ficha_1.sq_ficha = ficha_oco_uc.sq_ficha
//                  INNER JOIN salve.ciclo_avaliacao as ciclo on ciclo.sq_ciclo_avaliacao = ficha_1.sq_ciclo_avaliacao
//                  INNER JOIN taxonomia.taxon ON taxon.sq_taxon = ficha_1.sq_taxon
//                  WHERE ( taxon.sq_taxon = ficha.sq_taxon OR taxon.sq_taxon_pai = ficha.sq_taxon )
//                  and ficha_1.sq_ciclo_avaliacao = ficha.sq_ciclo_avaliacao
//                  and ficha_oco_uc.sq_rppn is not null
//                  and ficha_oco_uc.in_utilizado_avaliacao = 'S'
//                  AND ficha_oco_uc.sq_contexto = :sqContextoOcorrencia""")
//                }
//            sqlCmd +='\n'+sqlUcs.join('\nUNION ALL\n')+'\t\t\n) ucs'
//            sqlCmd += """\t\n) uc
//            CROSS JOIN LATERAL (
//               SELECT json_object_agg( estado.sq_estado, json_build_object(
//                 'no_regiao', estado.no_regiao
//                ,'no_estado', estado.no_estado
//                ,'sg_estado', estado.sg_estado
//               )) AS json_estado
//               FROM (
//               SELECT distinct vw_estado.sq_estado
//                , vw_estado.no_estado
//                , vw_estado.sg_estado
//                , vw_regiao.sq_regiao
//                , vw_regiao.no_regiao
//               FROM salve.ficha_ocorrencia ficha_oco_estado
//                INNER JOIN salve.ficha as ficha_1 ON ficha_1.sq_ficha = ficha_oco_estado.sq_ficha
//                INNER JOIN salve.ciclo_avaliacao as ciclo on ciclo.sq_ciclo_avaliacao = ficha_1.sq_ciclo_avaliacao
//                INNER JOIN taxonomia.taxon ON taxon.sq_taxon = ficha_1.sq_taxon
//                INNER JOIN salve.vw_estado ON vw_estado.sq_estado = ficha_oco_estado.sq_estado
//                LEFT join salve.vw_regiao on vw_regiao.sq_regiao = vw_estado.sq_regiao
//                WHERE ( taxon.sq_taxon = ficha.sq_taxon OR taxon.sq_taxon_pai = ficha.sq_taxon)
//                 AND ficha_1.sq_ciclo_avaliacao = ficha.sq_ciclo_avaliacao
//                 AND ficha_oco_estado.sq_estado is not null
//                 AND ficha_oco_estado.sq_contexto = :sqContextoEstado
//               ) estado
//            ) estado
//
//            CROSS JOIN LATERAL (
//                SELECT json_object_agg( hist.sq_taxon_historico_avaliacao,json_build_object(
//                 'sq_taxon_historico_avaliacao', hist.sq_taxon_historico_avaliacao
//                ,'nu_ano'              ,hist.nu_ano
//                ,'de_criterio'         ,hist.de_criterio
//                ,'sq_categoria'        ,hist.sq_categoria
//                ,'de_categoria'        ,hist.de_categoria
//                ,'cd_categoria'        ,hist.cd_categoria
//                ,'st_possivelmente_extinta' ,hist.st_possivelmente_extinta
//                ,'cd_categoria_sistema',hist.cd_categoria_sistema
//                ,'de_nivel'            ,hist.de_nivel
//                --,'tx_justificativa'    ,hist.tx_justificativa_avaliacao
//                ) ) as json_historico
//                from ( select a.sq_taxon_historico_avaliacao as sq_taxon_historico_avaliacao
//                  ,a.nu_ano_avaliacao as nu_ano
//                  ,a.de_criterio_avaliacao_iucn as de_criterio
//                  ,categoria.sq_dados_apoio as sq_categoria
//                  ,categoria.ds_dados_apoio as de_categoria
//                  ,categoria.cd_dados_apoio as cd_categoria
//                  ,a.st_possivelmente_extinta as st_possivelmente_extinta
//                  ,categoria.cd_sistema as cd_categoria_sistema
//                  ,categoria.cd_dados_apoio_nivel as de_nivel
//                --,'tx_justificativa'    ,a.tx_justificativa_avaliacao
//                  from salve.taxon_historico_avaliacao a
//                  left join salve.dados_apoio categoria on categoria.sq_dados_apoio = a.sq_categoria_iucn
//                  where a.sq_taxon = ficha.sq_taxon
//                    and a.sq_tipo_avaliacao = :sqAvaliacaoNacional
//                    $whereHist
//                    $wherePexHist
//                    and a.nu_ano_avaliacao = (
//                    select max( tmp1.nu_ano_avaliacao ) from salve.taxon_historico_avaliacao tmp1
//                     where tmp1.sq_taxon =   ficha.sq_taxon
//                       and tmp1.sq_tipo_avaliacao = :sqAvaliacaoNacional
//                       and tmp1.nu_ano_avaliacao < ( select nu_ano from salve.ciclo_avaliacao x where x.sq_ciclo_avaliacao = ficha.sq_ciclo_avaliacao )
//                  ) limit 1
//                ) hist
//            ) historico
//            INNER JOIN salve.ciclo_avaliacao on ciclo_avaliacao.sq_ciclo_avaliacao = ficha.sq_ciclo_avaliacao
//            INNER JOIN salve.vw_unidade_org unidadeOrg on unidadeOrg.sq_pessoa = ficha.sq_unidade_org
//            INNER JOIN salve.dados_apoio situacao_ficha on situacao_ficha.sq_dados_apoio = ficha.sq_situacao_ficha
//            ${params.nmComumFiltro ? 'INNER JOIN salve.ficha_nome_comum nome_comum on nome_comum.sq_ficha = ficha.sq_ficha':''}
//            LEFT JOIN salve.ficha as ficha_ciclo_anterior on ficha_ciclo_anterior.sq_ficha = ficha.sq_ficha_ciclo_anterior
//            LEFT JOIN taxonomia.taxon as taxon_ciclo_anterior on taxon_ciclo_anterior.sq_taxon = ficha_ciclo_anterior.sq_taxon
//            LEFT JOIN taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
//            LEFT JOIN salve.dados_apoio grupo on grupo.sq_dados_apoio = ficha.sq_grupo
//            LEFT JOIN salve.dados_apoio subgrupo on subgrupo.sq_dados_apoio = ficha.sq_subgrupo
//            LEFT JOIN salve.dados_apoio grupoSalve on grupoSalve.sq_dados_apoio = ficha.sq_grupo_salve
//            LEFT JOIN salve.dados_apoio tendenciaPopulacional on tendenciaPopulacional.sq_dados_apoio = ficha.sq_tendencia_populacional
//            LEFT JOIN salve.dados_apoio categoria_final on categoria_final.sq_dados_apoio = ficha.sq_categoria_final
//            --LEFT JOIN salve.dados_apoio categoria_oficina on categoria_oficina.sq_dados_apoio = ficha.sq_categoria_iucn
//            LEFT JOIN LATERAL (
//                        -- considerar a ultima oficina de avaliacao
//                        select oficina_ficha.sq_ficha
//                        ,oficina_ficha.sq_oficina_ficha
//                        ,oficina_ficha.st_ct_convidado
//                        ,oficina_ficha.st_pf_convidado
//                        ,oficina_ficha.sq_oficina
//                        ,oficina_ficha.ds_agrupamento
//                        ,oficina_ficha.sq_situacao_ficha
//                        ,oficina_ficha.sq_categoria_iucn
//                        ,oficina.dt_inicio
//                        ,oficina.dt_fim
//                        ,categoria.cd_dados_apoio as cd_categoria_oficina
//                        ,categoria.cd_sistema as cd_categoria_oficina_sistema
//                        ,categoria.ds_dados_apoio as de_categoria_oficina
//                        ,categoria.cd_dados_apoio_nivel as de_nivel_categoria_oficina
//                        ,oficina_ficha.ds_criterio_aval_iucn as de_criterio_oficina
//                        ,oficina_ficha.st_possivelmente_extinta as st_possivelmente_extinta_oficina
//                        ,salve.remover_html_tags( oficina_ficha.ds_justificativa ) as ds_justificativa_oficina
//                        from salve.oficina_ficha, salve.oficina, salve.dados_apoio, salve.dados_apoio as categoria
//                        where oficina.sq_oficina=oficina_ficha.sq_oficina
//                        and oficina_ficha.sq_categoria_iucn = categoria.sq_dados_apoio
//                        and oficina.sq_tipo_oficina = dados_apoio.sq_dados_apoio
//                        and dados_apoio.cd_sistema = 'OFICINA_AVALIACAO'
//                        and oficina_ficha.sq_ficha = ficha.sq_ficha
//                        order by oficina.dt_fim desc limit 1
//                        ) oficina_ficha ON oficina_ficha.sq_ficha = ficha.sq_ficha
//
//            -- oficina de validacao - ler o periodo apenas
//            LEFT JOIN LATERAL (
//                -- considerar a ultima oficina de VALIDACAO
//                select oficina_ficha.sq_ficha
//                ,oficina.dt_inicio
//                ,oficina.dt_fim
//                from salve.oficina_ficha, salve.oficina, salve.dados_apoio
//                where oficina.sq_oficina=oficina_ficha.sq_oficina
//                and oficina.sq_tipo_oficina = dados_apoio.sq_dados_apoio
//                and dados_apoio.cd_sistema = 'OFICINA_VALIDACAO'
//                and oficina_ficha.sq_ficha = ficha.sq_ficha
//                order by oficina.dt_fim desc limit 1
//                ) oficina_validacao ON oficina_validacao.sq_ficha = ficha.sq_ficha
//            WHERE ficha.sq_ficha is not null
//            ${ where ? ' and '+ where.join('\nand ') : '' }
//            ORDER BY ficha.nm_cientifico, ciclo_avaliacao.nu_ano
//            """
//
//if( session.envProduction == false ) {
//
///** /
// /// xxx zzz
// println ' '
// println ' '
// println ' '
// println sqlCmd
// println qryParams
// println "-"*100
// /**/
//}
//        Sql sql = new Sql(dataSource)
//        try {
//
//        // tratamento dos dados antes da impressão
//            rows = sql.rows(sqlCmd, qryParams)
//            resultado.recordsTotal= rows.size()
//            resultado.recordsFiltered = rows.size()
//
//            // criar array com as colunas do reltório
//            rows.eachWithIndex { row, index ->
//
//                rowValida=true
//
//                /**
//                if( index == 0 )
//                {
//                    println ' '
//                    println ' '
//                    println ' '
//                    println row.json_acao_conservacao
//                    println ' '
//                    println ' '
//                    println ' '
//                }
//                /**/
//                JSONObject trilha=null
//                try {
//                    trilha = JSON.parse(row.json_trilha.toString())
//                } catch( Exception e ) {
//                    trilha = JSON.parse('{}')
//                }
//
//                // criar a lista dos taxons do relatório para utilizar na exportação dos registros
//                if( trilha ) {
//                    idsTaxonsEncontrados.push( trilha?.subespecie?.sq_taxon ? trilha?.subespecie?.sq_taxon?.toLong() : trilha?.especie?.sq_taxon?.toLong())
//                }
//                /*println trilha?.especie?.sq_taxon + ' / '+trilha?.subespecie?.sq_taxon
//                println trilha.filo.no_taxon
//                println trilha.classe.no_taxon
//                println ' '*/
//
//
//                //if (resultado.data.size() < 1000) {
//                row.nm_cientifico = Util.ncItalico(row.nm_cientifico, true)
//                // https://grails.github.io/grails2-doc/2.1.2/api/org/codehaus/groovy/grails/web/json/JSONObject.html
//                JSONObject estados = JSON.parse(row.json_estado.toString())
//                JSONObject biomas = JSON.parse(row.json_bioma.toString())
//                JSONObject bacias = JSON.parse(row.json_bacia.toString())
//                JSONObject ucs = JSON.parse(row.json_uc.toString())
//                JSONObject ameacas = JSON.parse(row.json_ameaca.toString())
//                JSONObject usos = JSON.parse(row.json_uso.toString())
//                JSONObject listas = JSON.parse(row.json_lista_convencao.toString())
//                JSONObject acoes = JSON.parse(row.json_acao_conservacao.toString())
//                JSONObject pans = JSON.parse(row.json_plano_acao.toString())
//                JSONObject historico = JSON.parse(row.json_historico.toString())
//
//                /*if( index == 0 )
//                {
//                    println bacias
//                    println '--------------------'
//                    println biomas
//                }*/
//
//                row.de_comparativo = ''
//                row.de_categoria_anterior = ''
//                row.cd_categoria_anterior = ''
//                row.de_criterio_anterior = ''
//                row.de_nivel_categoria_anterior = ''
//                row.de_nivel_categoria_oficina = row.de_nivel_categoria_oficina ?: ''
//                row.de_nivel_categoria_final   = row.de_nivel_categoria_final ?: ''
//
//                row.de_ucs = ''
//                row.de_ameacas = ''
//                row.de_usos = ''
//                row.de_listas = ''
//                row.de_acoes = ''
//
//                // ESTADOS
//                row.de_estados = estados?.names()?.findAll { estados[it].sg_estado }.collect {
//                    return estados[it].sg_estado ?: ''
//                }.sort().join(', ')
//
//                // FILTRAR ESPÉCIES ENDÊMICAS NO ESTADO
//                if( rowValida && params.chkEndemicaEstado == 'S' && params.sqEstadoFiltro  )
//                {
//                    Integer qtdEstadosFiltro = params.sqEstadoFiltro.split(',').size()
//                    if( estados?.names()?.size().toInteger() == qtdEstadosFiltro )
//                    {
//                        params.sqEstadoFiltro.split(',').each{
//                            if( rowValida )
//                            {
//                                rowValida = estados?.containsKey( it.toString() )
//                            }
//                        }
//                    }
//                    else
//                    {
//                        rowValida = false
//                    }
//                }
//
//                // BIOMAS
//                row.de_biomas = biomas?.names()?.findAll { biomas[it].de_bioma }.collect {
//                    return biomas[it].de_bioma ?: ''
//                }.sort().join(', ')
//
//                // BACIAS
//                row.de_bacias = bacias?.names()?.findAll { bacias[it].de_bacia_hidrografica }.collect {
//                    return bacias[it].de_bacia_hidrografica ?: ''
//                }.sort().join(', ')
//
//                // FILTRAR ESPÉCIES ENDÊMICAS NO BIOMA
//                if( rowValida && params.chkEndemicaBioma == 'S' && params.sqBiomaFiltro  )
//                {
//                    Integer qtdBiomasFiltro = params.sqBiomaFiltro.split(',').size()
//                    if( biomas?.names()?.size().toInteger() == qtdBiomasFiltro )
//                    {
//                        params.sqBiomaFiltro.split(',').each{
//                            if( rowValida )
//                            {
//                                rowValida = biomas?.containsKey( it.toString() )
//                            }
//                        }
//                    }
//                    else
//                    {
//                        rowValida=false
//                    }
//                }
//                row.de_ucs = ucs?.names()?.findAll { ucs[it].sg_uc || ucs[it].no_uc }.collect {
//                    return Util.capitalize( ucs[it].sg_uc ?: ucs[it].no_uc) ?: ''
//                }.sort().join(', ')
//
//                row.de_ameacas = ameacas?.names()?.findAll { ameacas[it].de_ameaca != null }.collect {
//                    return ameacas[it].de_ameaca ?: ''
//                }.sort().join(', ')
//
//                row.de_usos = usos?.names()?.findAll { usos[it].de_uso != null }.collect {
//                    return usos[it].de_uso ?: ''
//                }.sort().join(', ')
//
//                row.de_listas = listas?.names()?.findAll { listas[it].de_lista_convencao != null }.collect {
//                    return listas[it].de_lista_convencao ? listas[it].de_lista_convencao + ' (' + listas[it].nu_ano + ')' : ''
//                }.sort().join(', ')
//
//                row.de_acoes = acoes?.names()?.findAll { acoes[it].de_acao != null }.collect {
//                    return acoes[it].de_acao ?: ''
//                }.sort().join(', ')
//
//                row.de_pans = pans?.names()?.findAll { pans[it].de_plano_acao != null }.collect {
//                    return (pans[it].de_plano_acao ? pans[it].de_plano_acao + ' ('+pans[it].ds_situacao?.toLowerCase()+')' : '')
//                }.sort().join(', ')
//
//                row.de_categoria_final_completa = ''
//                if( row.de_categoria_final ) {
//                    row.de_categoria_final_completa = row.de_categoria_final + ' (' + row.cd_categoria_final + ')'
//                    if( row.cd_categoria_final_sistema == 'CR' && row.st_possivelmente_extinta_final == 'S' )
//                    {
//                        row.de_categoria_final_completa += '<br><span class="blue">(PEX)</span>'
//                    }
//                }
//                row.de_categoria_oficina_completa = ''
//                if( row.de_categoria_oficina ) {
//                    row.de_categoria_oficina_completa = row.de_categoria_oficina + ' (' + row.cd_categoria_oficina + ')'
//                    if( row.cd_categoria_oficina_sistema == 'CR' && row.st_possivelmente_extinta_oficina == 'S' )
//                    {
//                        row.de_categoria_oficina_completa += '<br><span class="blue">(PEX)</span>'
//                    }
//                }
//
//                // calcular a coluna Comparativo Categoria
//                if (historico && rowValida) {
//                    def hist = historico.values()[0]
//                    // a partir do segundo ciclo não considerar como ciclo anterior ciclos com ano < 2008
//                    if( hist.nu_ano < 2008 && row.nu_ano_ciclo_inicio.toInteger() > 2010 ) {
//                        row.de_categoria_anterior = ''
//                        row.de_criterio_anterior = ''
//                        row.cd_categoria_anterior = ''
//                        row.de_nivel_categoria_anterior = ''
//
//                    } else {
//
//                        row.de_categoria_anterior = ( hist.de_categoria ? hist.de_categoria + ' (' + hist.cd_categoria + ')' + ( hist.st_possivelmente_extinta == 'S' ? '<br><span class="blue">(PEX)</span>' : '' ) : '' )
//                        row.de_criterio_anterior = ( hist.de_criterio ?: '' )
//                        row.cd_categoria_anterior = ( hist.cd_categoria_sistema ? hist.cd_categoria_sistema : '' )
//                        row.de_nivel_categoria_anterior = hist.de_nivel ?: ''
//                    }
//                    if (row.de_nivel_categoria_anterior) {
//                        String deNivelOficinaOuFinal = ''
//                        String cdCategoriaOficinaOuFinal = ''
//
//                        if (row.de_nivel_categoria_final) {
//                            deNivelOficinaOuFinal = row.de_nivel_categoria_final
//                            cdCategoriaOficinaOuFinal = row.cd_categoria_final_sistema
//                        } else if (row.de_nivel_categoria_oficina) {
//                            deNivelOficinaOuFinal = row.de_nivel_categoria_oficina
//                            cdCategoriaOficinaOuFinal = row.cd_categoria_oficina_sistema
//                        }
//                        if (deNivelOficinaOuFinal) {
//                            if (deNivelOficinaOuFinal.toUpperCase() > row.de_nivel_categoria_anterior.toUpperCase()) {
//                                row.de_comparativo = 'Melhor'
//                            } else if (deNivelOficinaOuFinal.toUpperCase() < row.de_nivel_categoria_anterior.toUpperCase()) {
//                                row.de_comparativo = 'Pior'
//                            } else if (deNivelOficinaOuFinal.toUpperCase() == row.de_nivel_categoria_anterior.toUpperCase()) {
//                                row.de_comparativo = 'Igual'
//                            }
//
//                            // se a categoria FINAL for NA o comparativo fica Não se aplica
//                            if ( row.cd_categoria_anterior != 'NA' && row.cd_categoria_final_sistema == 'NA') {
//                                row.de_comparativo = 'Não se aplica'
//                            } else if (row.cd_categoria_anterior == 'NE' || row.cd_categoria_final_sistema == 'NE') {
//                                row.de_comparativo = 'Não se aplica'
//                            }
//
//                            // exceção
//                            // - de VU, EN e CR para DD = Não Aplica
//                            if ((row.cd_categoria_anterior =~ /VU|EN|CR/) && cdCategoriaOficinaOuFinal == 'DD') {
//                                row.de_comparativo = 'Não se aplica'
//                            }
//                        }
//                    }
//                }
//
//                // filtrar pelo comparativo entre as categorias avaliadas ( melhor, pior ou igual )
//                row.de_nivel_categoria_anterior = row.de_nivel_categoria_anterior.toUpperCase()
//                row.de_nivel_categoria_oficina = row.de_nivel_categoria_oficina.toUpperCase()
//                row.de_nivel_categoria_final = row.de_nivel_categoria_final.toUpperCase()
//                if( params.deComparativoRelacaoFiltro && params.deComparativoFiltro && rowValida ) {
//                    rowValida = false
//                    params.deComparativoFiltro.split(',').each {
//                        if ( ! rowValida ) {
//
//                            String cdCategoriaA  = ''
//                            String cdCategoriaB  = ''
//                            String deNivelA      = ''
//                            String deNivelB      = ''
//
//                            /*if (params.deComparativoRelacaoFiltro == 'HFO') {
//                                if( row.de_nivel_categoria_anterior && (row.de_nivel_categoria_final ?: row.de_nivel_categoria_oficina) ) {
//                                    if (it == 'IGUAL') {
//                                        rowValida = ( row.de_nivel_categoria_anterior == row.de_nivel_categoria_final || row.de_nivel_categoria_anterior == row.de_nivel_categoria_oficina )
//                                    } else if (it == 'PIOR') {
//                                        rowValida = row.de_nivel_categoria_anterior > row.de_nivel_categoria_final || row.de_nivel_categoria_anterior > row.de_nivel_categoria_oficina
//                                    } else if (it == 'MELHOR') {
//                                        rowValida = row.de_nivel_categoria_anterior < row.de_nivel_categoria_final || row.de_nivel_categoria_anterior < row.de_nivel_categoria_oficina
//                                    }
//                                }
//                            }
//                            else*/
//                            if (params.deComparativoRelacaoFiltro == 'HF') {
//                                if( row.de_nivel_categoria_anterior && row.de_nivel_categoria_final ) {
//
//                                    cdCategoriaA = row.cd_categoria_anterior
//                                    cdCategoriaB = row.cd_categoria_final_sistema
//                                    deNivelA     = row.de_nivel_categoria_anterior
//                                    deNivelB     = row.de_nivel_categoria_final
//                                }
//                            }
//                            else if (params.deComparativoRelacaoFiltro == 'HO') {
//                                if( row.de_nivel_categoria_anterior && row.de_nivel_categoria_oficina ) {
//                                    cdCategoriaA = row.cd_categoria_anterior
//                                    cdCategoriaB = row.cd_categoria_oficina_sistema
//                                    deNivelA     = row.de_nivel_categoria_anterior
//                                    deNivelB     = row.de_nivel_categoria_oficina
//                                }
//                            }
//                            else if (params.deComparativoRelacaoFiltro == 'OF') {
//                                if( row.de_nivel_categoria_oficina  && row.de_nivel_categoria_final ) {
//                                    cdCategoriaA = row.cd_categoria_oficina_sistema
//                                    cdCategoriaB = row.cd_categoria_final_sistema
//                                    deNivelA     = row.de_nivel_categoria_oficina
//                                    deNivelB     = row.de_nivel_categoria_final
//                                }
//                            }
//                            // - quando a categoria for LC sempre será melhor
//
//                            if (it == 'IGUAL') {
//                                rowValida = cdCategoriaA == cdCategoriaB
//                            } else if (it == 'PIOR') {
//                                rowValida = deNivelA > deNivelB
//                            } else if (it == 'MELHOR') {
//                                rowValida = deNivelA < deNivelB
//                            }
//                            if( cdCategoriaA != 'NA' && cdCategoriaB == 'NA' && it == 'NAO_APLICA')
//                            {
//                                rowValida = true
//                            }
//                            else if( ( cdCategoriaA =~ /VU|EN|CR/ ) && cdCategoriaB == 'DD' )
//                            // exceções
//                            // - de VU, EN e CR para DD = Não Aplica
//                            {
//                                rowValida = false
//                                if (it == 'NAO_APLICA') {
//                                    rowValida = true
//                                }
//                            }
//                        }
//                    }
//                }
//                // ------------------------------------------------------------------------------------
//                if( rowValida ) {
//
//                    List rowData = [ (String) (index + 1)
//
//                                         , row.nm_cientifico
//                                         , trilha?.familia?.no_taxon ?:''
//                                         , trilha?.classe?.no_taxon  ?:''
//                                         , trilha?.ordem?.no_taxon   ?:''
//
//                                         , _gridLongText( row.no_comum, ',', false )
//                                         , row.no_autor ?:''
//                                         , row.nu_ano_taxon ? row.nu_ano_taxon.toString() : ''
//
//                                         , row?.nm_cientifico_anterior ?:''
//                                         //, row.no_autor_anterior
//                                         //, row.nu_ano_taxon_anterior
//
//                                         , row.de_ciclo_avaliacao ? row.de_ciclo_avaliacao + ' (' + row.nu_ano_ciclo_inicio + ' a ' + row.nu_ano_ciclo_fim + ')' : ''
//                                         , row.sg_unidade_org ?: ''
//                                         , row.de_situacao_ficha ?: ''
//
//                                         , row.de_endemica_brasil ?: ''
//                                         , row.de_presenca_lista_vigente ?: ''
//
//                                         , row.de_categoria_anterior ?: ''
//                                         , row.de_criterio_anterior ?: ''
//
//                                         , row.de_periodo_avaliacao ?:''
//                                         , row.de_categoria_oficina_completa
//                                         , row.de_criterio_oficina ?: ''
//                                         , _gridLongText( row.ds_justificativa_oficina )
//                                         //, row.st_possivelmente_extinta_oficina == 'S' ? 'Sim' : 'Não'
//
//                                         , row.de_periodo_validacao ?:'']
//                                        if( session.sicae.user.isADM() ) {
//                                            rowData += [row.dt_aceite_validacao ? row.dt_aceite_validacao.format('dd/MM/yyyy') : '']
//                                        }
//                                        rowData += [
//                                           row.de_categoria_final_completa
//                                         , row.de_criterio_final ?: ''
//                                         , _gridLongText( row.ds_justificativa_final )
//                                         //, row.st_possivelmente_extinta_final == 'S' ? 'Sim' : 'Não'
//
//                                         , row.de_comparativo ?: ''
//                                         , row.de_motivo_mudanca ? row.de_motivo_mudanca.replaceAll(', ', '<br>') : ''
//
//                                         , row.de_grupo ?: ''
//                                         , row.de_subgrupo ?: ''
//                                         , row.de_grupo_salve ?: ''
//                                         , row.ds_migratoria ?: ''
//                                         , row.ds_tendencia_populacional ?: ''
//
//                                         , _gridLongText( row.de_biomas, ',', false )
//                                         , _gridLongText( row.de_estados ? row.de_estados.replaceAll(/\, ?/, ', ') : '' )
//                                         , _gridLongText( row.de_bacias, ',', false )
//                                         , _gridLongText( row.de_ucs, ',', false )
//                                         , _gridLongText( row.de_ameacas, ',', false )
//                                         , _gridLongText( row.de_usos, ',', false )
//                                         , _gridLongText( row.de_listas, ',', false )
//                                         , _gridLongText( row.de_acoes, ',', false )
//                                         , _gridLongText( row.de_pans, ',', false )
//                    ]
//
//                    // remover coluna que só o adm pode visualizar
//                    resultado.data.push( rowData )
//                }
//                else {
//                    resultado.recordsFiltered--
//                }
//            }
//            // guardar os ids dos taxons no disco caso seja clicado no botão Exportar Ocorrencias
//            if( params.relId ) {
//                Map relData = [sqCicloAvaliacao: params.sqCicloAvaliacaoFiltro,ids:idsTaxonsEncontrados]
//                String fileName = '/data/salve-estadual/temp/rel_taxon_analitico_'+params.relId+'.json'
//                new File(fileName).write( (relData as JSON).toString() )
//            }
//
//        } catch (Exception e) {
//            println e.getMessage()
//            // e.printStackTrace()
//            resultado.errors.push( e.getMessage() )
//        }
//        sql.close()
//        render resultado as JSON
//        //render('template': 'grideRel003', model: [rows: rows, erros: erros])
//    } // bioma
//// no_taxon

    def rel004() {
        Map resultado = [data:[], recordsTotal:0, recordsFiltered:0, errors:[]]
        Map parametrosConsulta = [:]
        List where = []
        String query = ''
        boolean rowValida = true

        // retornar o formulário se não for informado o ciclo de avaliação
        if ( ! params.print ) {
            List listNivelTaxonomico = NivelTaxonomico.list()
            List listCicloAvaliacao  = CicloAvaliacao.list(sort: 'nuAno', order: 'desc')
            List listGrupo           = dadosApoioService.getTable('TB_GRUPO')
            List listSubgrupo        = dadosApoioService.getTable('TB_SUBGRUPO')

            render('view': 'rel004', model: ['listCicloAvaliacao'   : listCicloAvaliacao
                                            ,'listNivelTaxonomico'  : listNivelTaxonomico
                                            ,'listGrupo'            : listGrupo
                                            ,'listSubgrupo'         : listSubgrupo ])
        } else {

            if( params.starting == 'S' )
            {
                render resultado as JSON
                return
            }
            // criar comando SQL COM AS COLUNAS DO RELATORIO
            query ="""
            select ficha.sq_ficha,ficha.sq_taxon,
                coalesce( taxon.json_trilha->'subespecie'->>'no_taxon', taxon.json_trilha->'especie'->>'no_taxon') as nm_cientifico,
                grupo.ds_dados_apoio as no_grupo,
                subgrupo.ds_dados_apoio as no_subgrupo,
                ficha.sg_unidade_org,
                ficha.ds_situacao_ficha,

                -- AVALIACAO
                oficina_avaliacao.no_oficina_avaliacao,
                -- periodo da avaliacao
                oficina_avaliacao.de_periodo_avaliacao,

                -- VALIDACAO
                oficina_validacao.no_oficina_validacao,
                 -- periodo da validacao
                oficina_validacao.de_periodo_validacao,

                -- ponto(s) foca(is)
                array_to_string( array_agg( DISTINCT initcap( ponto_focal.no_pessoa )  ),', ') as no_ponto_focal,
                -- coordenador(es) de taxon
                array_to_string( array_agg( DISTINCT initcap( coordenador.no_pessoa )  ),', ') as no_coordenador_taxon

                FROM salve.vw_ficha as ficha
                INNER JOIN taxonomia.taxon as taxon on taxon.sq_taxon = ficha.sq_taxon
                LEFT JOIN salve.dados_apoio as grupo on grupo.sq_dados_apoio = ficha.sq_grupo
                LEFT JOIN salve.dados_apoio as subgrupo on subgrupo.sq_dados_apoio = ficha.sq_subgrupo
                LEFT JOIN salve.ficha_pessoa as ficha_pessoa_ponto_focal ON ficha_pessoa_ponto_focal.sq_ficha = ficha.sq_ficha and ficha_pessoa_ponto_focal.sq_papel = 856 and ficha_pessoa_ponto_focal.in_ativo = 'S'
                LEFT JOIN salve.ficha_pessoa as ficha_pessoa_coordenador ON ficha_pessoa_coordenador.sq_ficha = ficha.sq_ficha and ficha_pessoa_coordenador.sq_papel = 855 and ficha_pessoa_coordenador.in_ativo = 'S'
                LEFT JOIN salve.vw_pessoa as ponto_focal on ponto_focal.sq_pessoa = ficha_pessoa_ponto_focal.sq_pessoa
                LEFT JOIN salve.vw_pessoa as coordenador on coordenador.sq_pessoa = ficha_pessoa_coordenador.sq_pessoa

                -- oficina de AVALIACAO
                LEFT JOIN lateral (
                -- considerar a ultima oficina de AVALIACAO
                select oficina_ficha.sq_ficha
                ,concat( to_char( oficina.dt_inicio,'dd/mm/yyyy'),' a ', to_char( oficina.dt_fim,'dd/mm/yyyy') ) as de_periodo_avaliacao
                ,oficina.no_oficina as no_oficina_avaliacao
                from salve.oficina_ficha, salve.oficina, salve.dados_apoio
                where oficina.sq_oficina=oficina_ficha.sq_oficina
                and oficina.sq_tipo_oficina = dados_apoio.sq_dados_apoio
                and dados_apoio.cd_sistema = 'OFICINA_AVALIACAO'
                and oficina_ficha.sq_ficha = ficha.sq_ficha
                order by oficina.dt_fim desc limit 1
                ) oficina_avaliacao ON true

                -- oficina de VALIDACAO
                LEFT JOIN lateral (
                -- considerar a ultima oficina de VALIDACAO
                select oficina_ficha.sq_ficha
                ,concat( to_char( oficina.dt_inicio,'dd/mm/yyyy'),' a ', to_char( oficina.dt_fim,'dd/mm/yyyy') ) as de_periodo_validacao
                ,oficina.no_oficina as no_oficina_validacao
                from salve.oficina_ficha, salve.oficina, salve.dados_apoio
                where oficina.sq_oficina=oficina_ficha.sq_oficina
                and oficina.sq_tipo_oficina = dados_apoio.sq_dados_apoio
                and dados_apoio.cd_sistema = 'OFICINA_VALIDACAO'
                and oficina_ficha.sq_ficha = ficha.sq_ficha
                order by oficina.dt_fim desc limit 1
                ) oficina_validacao ON true
                """
            // se for passado o filtro pelo nivel taxonomico acrescentar join com a tabela taxon
            /*if (params.nivelFiltro && params.sqTaxonFiltro)
            {
                query +="""\nINNER JOIN taxonomia.taxon as taxon on taxon.sq_taxon = ficha.sq_taxon"""
            }*/

            CicloAvaliacao ciclo = CicloAvaliacao.get( params.sqCicloAvaliacaoFiltro.toInteger() )
            parametrosConsulta.sqCicloAvaliacao = ciclo.id.toInteger()
            query+="""\nwhere ficha.sq_ciclo_avaliacao = :sqCicloAvaliacao"""

            // filtro nome cientifico
            if( params.nmCientificoFiltro )
            {
                String nmCientificoCleaned = params.nmCientificoFiltro.replaceAll(/(?i)[^a-zA-Z -]/,'').toLowerCase().trim()
                where.push(  "ficha.nm_cientifico ilike '%${nmCientificoCleaned}%'")
            }

            // filtro nivel taxonomico
            if( params.nivelFiltro && params.sqTaxonFiltro)
            {
                NivelTaxonomico nivel = NivelTaxonomico.get( params.nivelFiltro.toLong())
                if( nivel )
                {
                    if( params.sqTaxonFiltro.toString().indexOf(',') == -1 ) {
                        where.push("taxon.json_trilha->'${nivel.coNivelTaxonomico.toLowerCase()}'->>'sq_taxon'='${params.sqTaxonFiltro}'")
                    }
                    else
                    {
                        where.push("taxon.json_trilha->'${nivel.coNivelTaxonomico.toLowerCase()}'->>'sq_taxon' in (${params.sqTaxonFiltro.split(',').collect{"'"+it+"'"}.join(',')        })")
                    }
                }
            }

            if ( params.sqGrupoSalveFiltro ) {
                where.push( 'ficha.sq_grupo_grupo in ('+params.sqGrupoSalveFiltro+')' )
            }

            if ( params.sqGrupoFiltro ) {
                where.push( 'ficha.sq_grupo in ('+params.sqGrupoFiltro+')' )
            }
            if ( params.sqSubgrupoFiltro ) {
                where.push( 'ficha.sq_subgrupo in ('+params.sqSubgrupoFiltro+')' )
            }

            if ( params.sqUnidadeFiltro ) {
                where.push( 'ficha.sq_unidade_org in ('+params.sqUnidadeFiltro+')' )
            }

            if ( where ) {
                query += "\nand " + where.join(' and ')
            }

            query +="""\ngroup by ficha.sq_ficha, ficha.sq_taxon, ficha.nm_cientifico,  grupo.ds_dados_apoio
                        ,subgrupo.ds_dados_apoio, ficha.sg_unidade_org, ficha.ds_situacao_ficha
                        ,oficina_avaliacao.no_oficina_avaliacao, oficina_avaliacao.de_periodo_avaliacao
                        ,oficina_validacao.no_oficina_validacao, oficina_validacao.de_periodo_validacao
                        ,taxon.json_trilha->'especie'->>'no_taxon', taxon.json_trilha->'subespecie'->>'no_taxon'
                        """

             /** /
             println ' '
             println ' PARAMETROS CONSULTA'
             println parametrosConsulta
             println query
             println ' '
             println ' '
             println ' '
             /**/

            List rows = sqlService.execSql( query,parametrosConsulta )

            resultado.recordsTotal= rows.size()
            resultado.recordsFiltered = rows.size()
            resultado.data = []

            rows.eachWithIndex { row, index ->
                rowValida = true
                resultado.data.push(
                        [ (String) (index + 1)
                         ,row.nm_cientifico ? Util.ncItalico( row.nm_cientifico, true) :''
                         ,row.no_grupo?:''
                         ,row.no_subgrupo?:''
                         ,row.sg_unidade_org?:''
                         ,row.ds_situacao_ficha
                         ,row.no_ponto_focal ? '- ' + row.no_ponto_focal.replaceAll(', ','<br>- ') : ''
                         ,row.no_coordenador_taxon ? '- ' + row.no_coordenador_taxon.replaceAll(', ','<br>- ') :''
                         // AVALIACAO
                         ,row.no_oficina_avaliacao?:''
                         ,row.de_periodo_avaliacao?:''
                         // VALIDACAO
                         ,row.no_oficina_validacao?:''
                         ,row.de_periodo_validacao?:''
                        ]
                )
            }
            render resultado as JSON
        }
    }

    /**
     * relatório gerencial do control das pendências das avaliações
     * @return
     */
    def rel005() {
        Map resultado = [:]
        Map parametrosConsulta = [:]
        String cmdSql
        List where = []
        List whereFinal = []
        List innerJoins = []

        // iniciar filtrando somente as oficinas com alguma pendencia
        params.comSemPendenciaFiltro = (params.comSemPendenciaFiltro==null ? 'S' : params.comSemPendenciaFiltro)

        // retornar o formulário se não for informado o ciclo de avaliação
        if ( ! params.sqCicloAvaliacao && !params.sqOficina && !params.formato ) {
            List listCicloAvaliacao = CicloAvaliacao.list(sort: 'nuAno', order: 'desc');
            render('view': 'rel005', model: ['listCicloAvaliacao': listCicloAvaliacao])
        } else if( params.formato=="grideEspeciesPendencia" ) {

            Oficina oficina = Oficina.get( params.sqOficina.toLong() )
            Unidade unidade = Unidade.get( params.sqUnidadeOrg.toLong() )

            cmdSql = """with cte as (
                    select ficha.sq_categoria_final
                         , ficha.nm_cientifico
                         , grupo.ds_dados_apoio as no_grupo
                         , case
                               when ficha.ds_justificativa_final is null and ficha.sq_categoria_final is not null then 1
                               else 0 end                                                                                     as nu_pendente
                         , array_agg(coalesce(resposta.cd_sistema, ''))::text                                                 as tx_respostas
                    from salve.oficina_ficha
                             inner join salve.ficha on ficha.sq_ficha = oficina_ficha.sq_ficha
                             inner join salve.vw_unidade_org as u on u.sq_pessoa = ficha.sq_unidade_org
                             inner join salve.oficina on oficina.sq_oficina = oficina_ficha.sq_oficina
                        left outer join salve.dados_apoio grupo on grupo.sq_dados_apoio = ficha.sq_grupo
                             left join salve.validador_ficha on validador_ficha.sq_oficina_ficha = oficina_ficha.sq_oficina_ficha
                             left join salve.dados_apoio resposta on resposta.sq_dados_apoio = validador_ficha.sq_resultado
                    where oficina.sq_oficina = :sqOficina
                    and ficha.sq_unidade_org = :sqUnidadeOrg
                    group by ficha.nm_cientifico
                           , ficha.sq_categoria_final
                           , ficha.ds_justificativa_final
                           , grupo.ds_dados_apoio
                ), cte2 as (
                    select cte.nm_cientifico
                         , cte.no_grupo
                         , sum(case when position('RESULTADO_C' in cte.tx_respostas) > 0 and cte.sq_categoria_final is null then 1 else 0 end) as nu_em_validacao
                         , sum(cte.nu_pendente) as nu_pendente
                    from cte
                    group by cte.nm_cientifico, cte.no_grupo
                )
                select cte2.nm_cientifico, cte2.no_grupo
                     ,case when nu_em_validacao>0 then 'Pendente' else 'Ok' end as in_validacao
                     ,case when cte2.nu_pendente > 0 then 'Pendente' else 'Ok' end as in_pendente
                from cte2
                WHERE cte2.nu_em_validacao + cte2.nu_pendente > 0
                order by cte2.nm_cientifico
            """
            parametrosConsulta.sqOficina = params.sqOficina.toLong()
            parametrosConsulta.sqUnidadeOrg = params.sqUnidadeOrg.toLong()
            List rows = sqlService.execSql(cmdSql, parametrosConsulta)
            render(template: 'grideRel005Pendencias', model: [rows: rows
                                                              , sgUnidade: unidade.sgUnidade
                                                              , noOficina: oficina.noOficina
                                                              , winId    : params.winId
            ])

        } else {

            CicloAvaliacao ciclo = CicloAvaliacao.get(params.sqCicloAvaliacao.toInteger())
            parametrosConsulta.sqCicloAvaliacao = ciclo.id.toInteger()

            DadosApoio oficinaValidacao = dadosApoioService.getByCodigo('TB_TIPO_OFICINA', 'OFICINA_VALIDACAO')
            parametrosConsulta.sqOficinaValidacao = oficinaValidacao.id

            // oficinas que ocorreram no período de...
            if (params.sqOficinaFiltro) {
                where.push("oficina.sq_oficina = :sqOficina")
                parametrosConsulta.sqOficina = params.sqOficinaFiltro.toLong()
            }

            // filtrar pelo nivel taxonomico
            if( params.nivelFiltro && params.sqTaxonFiltro ) {
                innerJoins.push("""inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon and taxon.json_trilha->'${params.nivelFiltro.toLowerCase()}'->>'sq_taxon' = '${params.sqTaxonFiltro}'""")
            }

            // filtro com / sem pendencia
            if( params.comSemPendenciaFiltro ){
                if( params.comSemPendenciaFiltro == 'S') {
                    whereFinal.push("nu_em_validacao + nu_pendente > 0")
                } else if ( params.comSemPendenciaFiltro == 'N') {
                    whereFinal.push("nu_em_validacao + nu_pendente = 0")
                }
            }

            // filtrar pela unidade do usuário
            if( ! session.sicae.user.isADM()  ) {
                where.push("""ficha.sq_unidade_org = :sqUnidadeOrg""")
                parametrosConsulta.sqUnidadeOrg = session.sicae.user.sqUnidadeOrg.toLong()
            }

            // criar comando SQL
            cmdSql = """with cte as (
                    select ficha.nm_cientifico
                         , ficha.sq_categoria_final
                         , u.sg_unidade_org  as no_unidade
                         , oficina.sg_oficina as no_oficina
                         , oficina.sq_oficina
                         , ficha.sq_unidade_org
                         , case
                               when ficha.ds_justificativa_final is null and ficha.sq_categoria_final is not null then 1
                               else 0 end as nu_pendente
                         , array_agg(coalesce(resposta.cd_sistema, ''))::text  as tx_respostas
                         , oficina.dt_inicio
                    from salve.oficina_ficha
                             inner join salve.ficha on ficha.sq_ficha = oficina_ficha.sq_ficha
                             inner join salve.vw_unidade_org as u on u.sq_pessoa = ficha.sq_unidade_org
                             inner join salve.oficina on oficina.sq_oficina = oficina_ficha.sq_oficina
                             left join salve.validador_ficha on validador_ficha.sq_oficina_ficha = oficina_ficha.sq_oficina_ficha
                             left join salve.dados_apoio resposta on resposta.sq_dados_apoio = validador_ficha.sq_resultado
                             ${innerJoins ? innerJoins.join('\n') : ''}
                    where ficha.sq_ciclo_avaliacao = :sqCicloAvaliacao
                      and oficina.sq_tipo_oficina = :sqOficinaValidacao
                      ${ where ? 'and '+where.join(' and ') : ''}
                    group by ficha.nm_cientifico
                           , ficha.sq_categoria_final
                           , u.sg_unidade_org
                           , oficina.sq_oficina
                           , oficina.sg_oficina
                           , ficha.ds_justificativa_final
                           , ficha.sq_unidade_org
                           , oficina.dt_inicio
                ), cte2 as (
                         select cte.no_unidade
                              , cte.no_oficina
                              , cte.sq_oficina
                              , cte.sq_unidade_org
                              , cte.dt_inicio
                              , sum(case when cte.sq_categoria_final is null then 0 else 1 end) as nu_validada
                              , sum(case when position('RESULTADO_C' in cte.tx_respostas) > 0 and cte.sq_categoria_final is null then 1 else 0 end) as nu_em_validacao
                              , sum(cte.nu_pendente) as nu_pendente
                         from cte
                         group by cte.no_unidade, cte.no_oficina, cte.sq_oficina, cte.sq_unidade_org, cte.dt_inicio
                       )
                select * from cte2
                ${ whereFinal ? 'WHERE ' + whereFinal.join(' AND ') :'' }
                order by no_unidade, dt_inicio desc, no_oficina
                """

            /** /
             // zzz
             println ' '
             println ' '
             println cmdSql
             println parametrosConsulta
             println ' '
             println ' '
             println ' '
             /**/

            List rows = sqlService.execSql(cmdSql, parametrosConsulta)

            if (params.formato == 'html') {
                render(template: 'grideRel005', model: [rows: rows])
            }
        }
    }


    /**
     * relatório analitico da validação
     * @return
     */
    def rel006Old() {
        Map resultado = [data:[], recordsTotal:0, recordsFiltered:0, errors:[]]
        Map parametrosConsulta = [:]
        CicloAvaliacao ciclo
        List listOficinas = []
        List listGrupos = []
        String cmdSql
        List where = []
        List wherePasso1 = []
        List wherePasso2 = []


        if( params.sqCicloAvaliacaoFiltro ) {
            ciclo = CicloAvaliacao.get(params.sqCicloAvaliacaoFiltro.toInteger())
        }
        DadosApoio oficinaValidacao = dadosApoioService.getByCodigo('TB_TIPO_OFICINA', 'OFICINA_VALIDACAO')
        DadosApoio oficinaAvaliacao = dadosApoioService.getByCodigo('TB_TIPO_OFICINA', 'OFICINA_AVALIACAO')


        // retornar o formulário se não for informado o ciclo de avaliação
        if ( ! params.print ) {
            List listCiclosAvaliacao = CicloAvaliacao.list(sort: 'nuAno', order: 'desc');
            listGrupos = dadosApoioService.getTable('TB_GRUPO')
            render('view': 'rel006', model: [ ciclo : ciclo
                                             ,'listCiclosAvaliacao': listCiclosAvaliacao
                                             , listGrupos: listGrupos

                 ])
            return
        }


        if( !params.sqCicloAvaliacaoFiltro ) {
            resultado.errors.push('Ciclo não informado')
            render resultado as JSON
            return
        }

        if( params.starting == 'S' ) {
            render resultado as JSON
            return
        }

//params.sqOficinaValidacaoFiltro = 217

        // filtrar pela oficina
        if( params.sqOficinaValidacaoFiltro){
            wherePasso1.push("o.sq_oficina = :sqOficina")
            parametrosConsulta.sqOficina = params.sqOficinaValidacaoFiltro.toLong()
        }

        // filtrar pelos grupos selecionados
        if( params.sqGrupoFiltro ){
            wherePasso2.push("ficha.sq_grupo in (${params.sqGrupoFiltro})")
        }

        parametrosConsulta.sqCicloAvaliacao = ciclo.id.toInteger()

        // filtrar pela unidade do usuário
        if ( ! session.sicae.user.isADM() ) {
            params.sqUnidadeOrg = session.sicae.user.sqUnidadeOrg.toLong()
        }

        if( params.sqUnidadeFiltro ) {
            wherePasso2.push("ficha.sq_unidade_org in (${ params.sqUnidadeFiltro })")
        }

        // criar comando SQL
        cmdSql = """with passo1 as (
                    select o.sq_oficina as sq_oficina_validacao
                         , o.sg_oficina as sg_oficina_validacao
                         , o.no_oficina as no_oficina_validacao
                         , DATE_TRUNC('day',o.dt_inicio) as dt_inicio_validacao
                    from salve.oficina o
                    where sq_ciclo_avaliacao = :sqCicloAvaliacao
                      and o.sq_tipo_oficina = ${oficinaValidacao.id.toString()}
                      ${ (wherePasso1 ? '\n and ' + wherePasso1.join('\nand ') :'') }
                ),
                     -- ler as especies da oficina de validação
                     passo2 as (
                         select oficina_ficha.sq_ficha
                              , passo1.sq_oficina_validacao
                              , coalesce(passo1.sg_oficina_validacao, passo1.no_oficina_validacao) as no_oficina_validacao
                              , passo1.dt_inicio_validacao
                              , ficha.nm_cientifico
                              , u.sg_unidade_org
                              , grupo.ds_dados_apoio                                               as no_grupo
                              , situacao_validacao.ds_dados_apoio                                  as ds_situacao_validacao
                              , ficha.dt_aceite_validacao                                          as dt_validacao
                              , case when categoria_validada.ds_dados_apoio is null then '' else concat(categoria_validada.ds_dados_apoio, ' (', categoria_validada.cd_dados_apoio,
                                       ')') end                                                        as ds_categoria_validacao
                              , oficina_ficha.ds_criterio_aval_iucn                                as ds_criterio_validacao
                         from salve.oficina_ficha
                                  inner join passo1 on passo1.sq_oficina_validacao = oficina_ficha.sq_oficina
                                  inner join salve.ficha on ficha.sq_ficha = oficina_ficha.sq_ficha
                                  inner join salve.vw_unidade_org u on u.sq_pessoa = ficha.sq_unidade_org
                                  left outer join salve.dados_apoio as grupo on grupo.sq_dados_apoio = ficha.sq_grupo
                                  left outer join salve.dados_apoio as situacao_validacao
                                                  on situacao_validacao.sq_dados_apoio = oficina_ficha.sq_situacao_ficha
                                  left outer join salve.dados_apoio as categoria_validada
                                                  on categoria_validada.sq_dados_apoio = oficina_ficha.sq_categoria_iucn
                                ${wherePasso2 ? ' where '+wherePasso2.join('\nand ') : ''}

                     ), passo3 as (
                    -- adicionar os validadores
                    select passo2.*
                         , avaliacao.*
                         , salve.json_validadores(passo2.sq_ficha, passo2.sq_oficina_validacao)::text AS json_validadores
                    from passo2
                             -- ler a última oficina de avaliação
                             left join lateral (
                        select coalesce(oficina.sg_oficina, oficina.no_oficina)                                        as no_oficina_avaliacao
                             , case when categoria_avaliada.ds_dados_apoio is null then '' else concat( categoria_avaliada.ds_dados_apoio, ' (', categoria_avaliada.cd_dados_apoio,
                                      ')') end                                                                             as ds_categoria_avaliacao
                             , oficina_ficha.ds_criterio_aval_iucn                                                     as ds_criterio_avaliacao


                                 inner join salve.oficina on oficina.sq_oficina = oficina_ficha.sq_oficina
                                 left outer join salve.dados_apoio as categoria_avaliada
                                                 on categoria_avaliada.sq_dados_apoio = oficina_ficha.sq_categoria
                        where oficina_ficha.sq_ficha = passo2.sq_ficha
                          and oficina.sq_tipo_oficina = ${oficinaAvaliacao.id.toString()}
                          and DATE_TRUNC('day', oficina.dt_fim) <= DATE_TRUNC('day', passo2.dt_inicio_validacao)
                        order by oficina.dt_fim desc

                        ) as avaliacao on true
                ), passo4 as (
                    -- adicionar o ponto focal
                    select passo3.*, ponto_focal.no_ponto_focal
                    ,coordenador_taxon.no_coordenador_taxon
                    from passo3
                    left join lateral(
                        select array_to_string( array_agg( pf.no_pessoa order by lower(pf.no_pessoa) ),'|') as no_ponto_focal
                        from salve.ficha_pessoa
                       left outer join salve.dados_apoio as papel on papel.sq_dados_apoio = ficha_pessoa.sq_papel
                        left outer join salve.vw_pessoa_fisica pf on pf.sq_pessoa = ficha_pessoa.sq_pessoa
                        where ficha_pessoa.in_ativo = 'S'
                          and papel.cd_sistema ='PONTO_FOCAL'
                          and ficha_pessoa.sq_ficha = passo3.sq_ficha
                        ) as ponto_focal on true
                        -- adicionar o coordenador taxon
                    left join lateral(
                        select array_to_string( array_agg( pf.no_pessoa order by lower(pf.no_pessoa) ),'|') as no_coordenador_taxon
                        from salve.ficha_pessoa
                                 left outer join salve.dados_apoio as papel on papel.sq_dados_apoio = ficha_pessoa.sq_papel
                        left outer join salve.vw_pessoa_fisica pf on pf.sq_pessoa = ficha_pessoa.sq_pessoa
                        where ficha_pessoa.in_ativo = 'S'
                          and papel.cd_sistema ='COORDENADOR_TAXON'
                          and ficha_pessoa.sq_ficha = passo3.sq_ficha
                        ) as coordenador_taxon on true
                )
                select passo4.* from passo4
                """

        /** /
         // zzz
         println ' '
         println ' '
         println cmdSql
         println parametrosConsulta
         println ' '
         println ' '
         println ' '
         /**/

        List rows = sqlService.execSql(cmdSql, parametrosConsulta)

        resultado.data = []

        rows.eachWithIndex { row, lin ->
            JSONObject jsonValidadores = null
            List nomesValidadores  = []

           i( !isADM ) {
               List respostas = []
               List respostasRevisao = []
           }
            try {
                jsonValidadores = JSON.parse(row.json_validadores)
            } catch( Exception e) {}
            if( jsonValidadores ){
                nomesValidadores = jsonValidadores.names()
                nomesValidadores.eachWithIndex{ nome,i ->
                    String resposta = jsonValidadores[nomesValidadores[i]]?.de_resposta ?: ''
                    String respostaRevisao = jsonValidadores[nomesValidadores[i]]?.de_resposta_revisao ?: ''
                    if( respostaRevisao ){
                        resposta = resposta + '*<br>Revisão da coordenação<br>*' + respostaRevisao
                    }
                    respostas.push( resposta )

                }
            }

            row.no_validador_1          = nomesValidadores[0] ?: ''
            row.ds_resposta_validador_1 = respostas[0] ?: ''
            row.no_validador_2          = nomesValidadores[1] ?: ''
            row.ds_resposta_validador_2 =  respostas[1] ?: ''
            row.no_validador_3          = nomesValidadores[2] ?: ''
            row.ds_resposta_validador_3 =  respostas[2] ?: ''


            resultado.data.push(
                [ (String) (lin + 1)
                  ,row.nm_cientifico ? Util.ncItalico( row.nm_cientifico, true) :''
                  ,row.no_grupo?:''
                  ,row.sg_unidade_org?:''
                  ,row.no_oficina_avaliacao?:''
                  ,row.ds_categoria_avaliacao?:''
                  ,row.ds_criterio_avaliacao?:''

                  ,row.no_oficina_validacao?:''
                  ,row.ds_situacao_validacao?:''
                  ,row.dt_validacao ? row.dt_validacao.format('dd/MM/yyyy'):''
                  ,row.ds_categoria_validacao?:''
                  ,row.ds_criterio_validacao?:''

                  ,row.no_validador_1
                  ,row.ds_resposta_validador_1
                  ,row.no_validador_2
                  ,row.ds_resposta_validador_2
                  ,row.no_validador_3
                  ,row.ds_resposta_validador_3

                  ,row.no_ponto_focal ? row.no_ponto_focal.replaceAll(/\|/,'<br>') : ''
                  ,row.no_coordenador_taxon ? row.no_coordenador_taxon.replaceAll(/\|/,'<br>') :''
                ]
            )
        }
        render resultado as JSON
    }

    /**
     * Retornar os ids filhos da tabela de apoio,  seperados por virgula
     * por examplo a tabela de Bacias, Usos, Ameaças
     * @param idsApioio - lista dos ids pais serparados por virgula
     */
    protected _recursiveIdsApoio( String idsPaisApoio = '' ){
        if( ! idsPaisApoio ) {
            return ''
        }
        List rows = sqlService.execSql("""WITH RECURSIVE arvore(sq_dados_apoio, sq_dados_apoio_pai) AS (
                        SELECT da.sq_dados_apoio,
                               da.sq_dados_apoio_pai
                        FROM salve.dados_apoio da
                        WHERE da.sq_dados_apoio in (${idsPaisApoio})
                        UNION
                        SELECT dc.sq_dados_apoio,
                               dc.sq_dados_apoio_pai
                        FROM arvore arvore_1
                             JOIN salve.dados_apoio dc ON dc.sq_dados_apoio_pai = arvore_1.sq_dados_apoio
                    )
                    SELECT distinct arvore.sq_dados_apoio AS id
                    FROM arvore
                    where arvore.sq_dados_apoio_pai is not null;""")
        return rows.id.join(',')
    }

    /**
     * Retornar os ids filhos da tabela de Usos,  seperados por virgula
     * @param idsUso - lista dos ids pais serparados por virgula
     */
    protected _recursiveIdsUso( String idsPaisUso = '' ){
        if( ! idsPaisUso ) {
            return ''
        }
        List rows = sqlService.execSql("""WITH RECURSIVE arvore(sq_uso, sq_uso_pai) AS (
                SELECT da.sq_uso,
                       da.sq_uso_pai
                FROM salve.uso da
                WHERE da.sq_uso in (${idsPaisUso})
                UNION
                SELECT dc.sq_uso,
                   dc.sq_uso_pai
                FROM arvore arvore_1
                JOIN salve.uso dc ON dc.sq_uso_pai = arvore_1.sq_uso
            )
            SELECT distinct arvore.sq_uso AS id
            FROM arvore
            """)
        return rows.id.join(',')
    }

    /**
     * metodo para gerar a planilha de participantes da oficina ( rel002 participantes )
     */
    protected Map _exportRel002Participantes( List rows = [] ) {
        String horaInicio= new Date().format('dd/MM/yyyy HH:mm:ss')
        String path = grailsApplication.config.temp.dir
        String urlSalve = grailsApplication.config.url.sistema ?:'https://salve.icmbio.gov.br/salve-estadual/'
        String hora = new Date().format('dd_MM_yyyy_hh_mm_ss')
        String fileName = path + 'salve_exportacao_participantes_oficinas_' + hora + '.xlsx'
        String linkDownload = '<a target="_blank" href="'+urlSalve+'download?fileName='+fileName+'"><b>AQUI</b></a>'

        // criar a planilha
        PlanilhaExcel planilha = new PlanilhaExcel(fileName, 'Participantes das Oficinas', false)
        planilha.setTitle(' ')
        planilha.addColumn(['title': 'Oficina'                  , 'column': 'no_oficina', 'autoFit': true       , 'width':40, alwaysVisible:true])  // width é a quantidade de caracteres e não pixels
        planilha.addColumn(['title': 'Local'                    , 'column': 'de_local', 'autoFit': true        , 'width':40, 'align': 'center'])
        planilha.addColumn(['title': 'Data início'              , 'column': 'dt_inicio', 'autoFit': true       , 'width':10, 'align': 'center'])
        planilha.addColumn(['title': 'Data fim'                 , 'column': 'dt_fim', 'autoFit': true          , 'width':10, 'align': 'center'])
        planilha.addColumn(['title': 'Tipo'                     , 'column': 'ds_tipo', 'autoFit': true         , 'width':15, 'align': 'center'])
        //planilha.addColumn(['title': 'Grupo'                    , 'column': 'no_grupo', 'autoFit': true       , 'width':30])
        planilha.addColumn(['title': 'Situação'                 , 'column': 'ds_situacao', 'autoFit': true     , 'width':10, 'align': 'center'])

        planilha.addColumn(['title': 'Participante'             , 'column': 'no_pessoa', 'autoFit': true        , 'width':40])
        planilha.addColumn(['title': 'E-mail'                   , 'column': 'de_email', 'autoFit': true         , 'width':40])
        planilha.addColumn(['title': 'Função'                   , 'column': 'papeis', 'autoFit': true           , 'width':30])
        planilha.addColumn(['title': 'Institução'               , 'column': 'no_instituicao', 'autoFit': true   , 'width':60])

        planilha.setData( rows )
        if ( ! planilha.run() ) {
            throw new Exception(planilha.getError())
        }
        Map res = [fileName:fileName,linkDownload:linkDownload]
        return res
    }


    /**
     * metodo assincrono para gerar a planilha do relatório taxon analitico rel003
     */
    void _exportRel003( Map exportParams ) {
        runAsync {

            boolean isADM = exportParams?.usuario && exportParams?.usuario?.isADM()
            boolean isPF = exportParams?.usuario && exportParams?.usuario?.isPF()
            UserJobs job
            List rows
            Map emailVars =[ data        : new Date().format('dd/MM/yyyy HH:mm:ss')
                             ,dataLimite  : (new Date()+5).format('dd/MM/yyyy')
                             ,linkDownload: '' ]


            String path = grailsApplication.config.temp.dir
            String urlSalve = grailsApplication.config.url.sistema ?: 'https://salve.icmbio.gov.br/salve-estadual/'
            String hora = new Date().format('dd_MM_yyyy_hh_mm_ss')
            String tempFileName = path + 'salve_exportacao_taxon_analitico_' + hora + '.xlsx'

            // criar as colunas dos Estados
            List estadosValidos = []
            // criar as colunas dos biomas
            List biomasValidos = []

            try {

                // 1º) CRIAR PARAMETROS DO JOB
                Map jobParams = [ usuario  : PessoaFisica.get( exportParams.usuario.sqPessoa )
                                  ,deRotulo : exportParams.jobRotulo ?: 'Exportação resultado relatório taxon analitico'
                                  ,deEmail  : exportParams.email ?: null
                                  ,deEmailAssunto : exportParams.emailAssunto ?: 'SALVE - Exportação Táxon Analítico finalizada'
                                  ,deMensagem : ''

                ]
                if ( jobParams.deEmail ) {
                    String mensagemEmailPadrao = """<p>Segue em anexo a planilha com o resultado da exportação do relatório Táxon Analítico realizada em {data}.</p>
                                <p>Caso não tenha recebido o arquivo em anexo, clique {linkDownload} para baixá-lo.</p>
                                <p><span style="color:#ff0000;">Este link ficará disponível até o dia {dataLimite}.</span></p>
                                <p>Atenciosamente,<br>
                                <b>Equipe SALVE</b></p>"""
                    jobParams.deMensagem = exportParams.emailMensagem ?: mensagemEmailPadrao
                    jobParams.jsExecutar = ''
                    emailVars.linkDownload = """<a target="_blank" href="https://salve.icmbio.gov.br/salve-estadual/download?fileName=${tempFileName}">AQUI</a>"""
                } else {
                    String mensagemEmailPadrao = 'Clique {linkDownload} para baixar o arquivo'
                    jobParams.deMensagem = exportParams.emailMensagem ?: mensagemEmailPadrao
                    emailVars.linkDownload ='<a target="_self" href="'+urlSalve+'downloadWithProgress?fileName='+tempFileName+'"><b>AQUI</b></a>'
                }

                if( jobParams.deMensagem) {
                    emailVars.each { key, value ->
                        Pattern pattern = Pattern.compile('\\{' + key + '\\}')
                        jobParams.deMensagem = jobParams.deMensagem.toString().replaceAll(pattern, value)
                    }
                }


                // CRIAR O JOB NO BANCO DE DADOS
                jobParams.noArquivoAnexo = tempFileName
                job = userJobService.createJob( jobParams ).save( flush:true )

                // 2º) EXECUTAR SQL PARA RECUPERAR OS IDS DAS FICHAS
                if( !isADM ) {
                    exportParams.cmdSql += ' OFFSET 0 LIMIT ' + (exportParams.maxFichas + 1)
                }
                job.deAndamento ='Consultando fichas...'
                job.save(flush:true)

                rows = sqlService.execSqlCache(exportParams.cmdSql, exportParams.sqlParams,240l)
                if (!rows) {
                    throw new Exception('Nenhuma ficha encontrada')
                }
                if (rows.size() > exportParams.maxFichas) {
                    throw new Exception('A quantidade de fichas para exportação excedeu o limite de ' + exportParams.maxFichas.toString() + ' fichas')
                }
                job.vlMax           = rows.size()
                job.deAndamento     = '0 de '+job.vlMax+' ficha(s)' // adicionar %s para exibir o percentual executado
                job.vlAtual         = 0
                job.save(flush:true)

                // 4º FAZER PRE-PROCESSAMENTO PARA AJUSTAR O RESULTADO DE ALGUMAS COLUNAS ANTES DE ENVIAR PARA A PLANILHA
                rows.each { row ->
                    // remover autor e ano do nome cientifico
                    row.no_taxon = Util.ncItalico(row.no_taxon, true, true)
                    row.no_taxon_anterior = Util.ncItalico(row.no_taxon_anterior, true, true)

                    // criar as colunas dos Estados onde ocorre
                    if (row.no_estados) {
                        row.no_estados.split(',').each { estado ->
                            String estadoTrim = estado.trim()
                            if (!estadosValidos.contains(estadoTrim)) {
                                estadosValidos.push(estadoTrim)
                            }
                            row[estadoTrim] = 'X'
                        }
                    }
                    // criar as colunass dos biomas onde ocorre
                    if (row?.no_biomas) {
                        row.no_biomas.split(',').each { bioma ->
                            String biomaTrim = 'Bioma ' + bioma.trim()
                            if (!biomasValidos.contains(biomaTrim)) {
                                biomasValidos.push(biomaTrim)
                            }
                            row[biomaTrim] = 'X'
                        }
                    }

                    if( row.no_usos ) {
                        List usos = []
                        row.no_usos.split(';').collect { uso ->
                            uso.split('\\|').collect{
                                if( !usos.contains(it)){
                                    usos.push( it )
                                }
                            }
                        }
                        row.no_usos = usos.join('\n')
                    }
                }
                estadosValidos.sort()
                biomasValidos.sort()

                // criar a planilha
                PlanilhaExcel planilha = new PlanilhaExcel(tempFileName, 'Exportação Relatório Táxon Analítico', true)
                planilha.setTitle(' ') // não imprimir o cabecalho na planilha
                planilha.setData( rows )
                List colsSelected = []
                if( exportParams.colunasSelecionadas ){
                    colsSelected = exportParams.colunasSelecionadas.split(',');
                }
                planilha.addColumn(['title': 'Táxon', 'column': 'no_taxon', 'autoFit': true, 'width': 30, alwaysVisible: true])  // width é a quantidade de caracteres e não pixels
                planilha.addColumn(['title': 'Família', 'column': 'no_familia', 'autoFit': true])
                planilha.addColumn(['title': 'Classe', 'column': 'no_classe', 'autoFit': true])
                planilha.addColumn(['title': 'Ordem', 'column': 'no_ordem', 'autoFit': true])
                planilha.addColumn(['title': 'Nome comum', 'column': 'no_comum', 'autoFit': true, 'width': 30])
                planilha.addColumn(['title': 'Autor', 'column': 'no_autor_taxon', 'autoFit': true])
                planilha.addColumn(['title': 'Ano', 'column': 'nu_ano_taxon', 'autoFit': true])
                planilha.addColumn(['title': 'Nome taxon ciclo anterior', 'column': 'no_taxon_anterior', 'autoFit': true])
                //planilha.addColumn(['title': 'Ciclo avaliação', 'column': 'de_ciclo_avaliacao', 'autoFit': true])
                planilha.addColumn(['title': 'Unidade', 'column': 'sg_unidade_org', 'autoFit': true])
                planilha.addColumn(['title': 'Situação ficha', 'column': 'de_situacao_ficha', 'autoFit': true])
                planilha.addColumn(['title': 'Espécie endêmica?', 'column': 'de_endemica_brasil', 'autoFit': true])
                planilha.addColumn(['title': 'Protegida legislação', 'column': 'de_protegida_legislacao', 'autoFit': true])
                planilha.addColumn(['title': 'Categoria anterior', 'column': 'de_categoria_anterior', 'autoFit': true])
                planilha.addColumn(['title': 'Critério anterior', 'column': 'de_criterio_anterior', 'autoFit': true])

                planilha.addColumn(['title': 'Período avaliação', 'column': 'de_periodo_avaliacao', 'autoFit': true])
                planilha.addColumn(['title': 'Categoria avaliação', 'column': 'de_categoria_avaliada', 'autoFit': true])
                planilha.addColumn(['title': 'Critério avaliação', 'column': 'de_criterio_avaliado', 'autoFit': true])
                planilha.addColumn(['title': 'Justificativa avaliação', 'column': 'tx_justificativa_avaliada', 'autoFit': true])

                planilha.addColumn(['title': 'Período validação', 'column': 'de_periodo_validacao', 'autoFit': true])

                if (isADM || isPF) {
                    planilha.addColumn(['title': 'Data validação', 'column': 'dt_aceite_validacao', 'autoFit': true])
                }
                planilha.addColumn(['title': 'Categoria validação', 'column': 'de_categoria_validada', 'autoFit': true])
                planilha.addColumn(['title': 'Critério validação', 'column': 'de_criterio_validado', 'autoFit': true])
                planilha.addColumn(['title': 'Justificativa validação', 'column': 'tx_justificativa_validada', 'autoFit': true])

                // type=csv, qubrar o conteudo pelo ponto-e-virgula
                planilha.addColumn(['title': 'Motivo mudança', 'column': 'de_motivo_mudanca', 'autoFit': true, 'type': 'csv', 'width': 30])

                planilha.addColumn(['title': 'Grupo avaliado', 'column': 'no_grupo_avaliado', 'autoFit': true])
                planilha.addColumn(['title': 'Subgrupo avaliado', 'column': 'no_subgrupo_avaliado', 'autoFit': true])
                planilha.addColumn(['title': 'Recorte módulo público', 'column': 'no_grupo_recorte', 'autoFit': true])
                planilha.addColumn(['title': 'Espécie migratória?', 'column': 'de_migratoria', 'autoFit': true])
                planilha.addColumn(['title': 'Tendência populacional', 'column': 'de_tendencia_populacional', 'autoFit': true])

                if (colsSelected.contains('no_biomas')) {
                    // coluna dos Biomas separados por vírgula
                    planilha.addColumn(['title'    : 'Biomas', 'column': 'no_biomas', 'autoFit': true, 'width': 20, 'align': 'center'
                                        , 'bgColor': '#CAECF2', 'color': '#000000'])
                    if (biomasValidos) {
                        // colunas do bioma separado
                        biomasValidos.each { bioma ->
                            planilha.addColumn(['title'    : bioma, 'column': bioma, 'autoFit': true, 'width': 20, 'align': 'center'
                                                , 'bgColor': '#E4F3F8', 'color': '#000000', alwaysVisible: true])
                        }
                    }
                }

                // coluna dos Estados separados por vírgula
                if (colsSelected.contains('no_estados')) {
                    planilha.addColumn(['title'    : 'Estados', 'column': 'no_estados', 'autoFit': true, 'width': 20, 'align': 'center'
                                        , 'bgColor': '#f0f0c2', 'color': '#000000']);

                    if (estadosValidos) {
                        // coluna do Estado separado
                        estadosValidos.each { estado ->
                            planilha.addColumn(['title'    : estado, 'column': estado, 'autoFit': true, 'width': 20, 'align': 'center'
                                                , 'bgColor': '#F8F8E4', 'color': '#000000', alwaysVisible: true])
                        }
                    }
                }

                planilha.addColumn(['title': 'Bacia', 'column': 'no_bacias', 'autoFit': true, 'type': 'tree', 'width': 50])
                planilha.addColumn(['title': 'Unidade de conservação', 'column': 'no_ucs', 'autoFit': true, 'type': 'csv', 'width': 40])
                planilha.addColumn(['title': 'Código CNUC', 'column': 'co_cnucs', 'autoFit': true, 'width': 40])
                planilha.addColumn(['title': 'Ameaça', 'column': 'no_ameacas', 'autoFit': true, 'type': 'tree', 'width': 30])
                planilha.addColumn(['title': 'Uso', 'column': 'no_usos', 'autoFit': true, 'type': 'tree', 'width': 40])
                planilha.addColumn(['title': 'Listas e convenções', 'column': 'no_listas_convencoes', 'autoFit': true, 'type': 'tree', 'width': 40])
                planilha.addColumn(['title': 'Ação de conservação', 'column': 'no_acoes_conservacao', 'autoFit': true, 'type': 'tree', 'width': 40])
                planilha.addColumn(['title': 'Planos de ação', 'column': 'no_pans', 'autoFit': true, 'type': 'csv', 'width': 40])

                // EXPORTAR SOMENTE AS COLUNAS SELECIONADAS
                if( colsSelected ) {
                    planilha.getColumns().each{
                        it.visible = colsSelected.contains( it.column ) || it.alwaysVisible
                    }
                }
                // NÃO IMPRIMIR O TÍTULO POR SOLICITAÇÃO DOS USUÁRIOS
                planilha.setTitle(' ')

                // GERAR O ARQUIVO XLSX
                if ( ! planilha.run( job ) ) {
                    throw new Exception(planilha.getError() )
                }
            } catch (Exception error) {
                String usuario = exportParams.usuario ? '\n' +
                    exportParams?.usuario?.noPessoa +' - CPF: '+ Util.formatCpf(exportParams?.usuario?.nuCpf) + ' - ID: ' +exportParams?.usuario?.sqPessoa : ''
                Util.printLog('SALVE - RelatorioController._exportRel003()','Erro ao exportar resultado ' + usuario
                    ,error.getMessage())
                if( job ) {
                    // encerrar o job
                    userJobService.error( job.id, error.getMessage() )
                }
            }
        }
    }

    /**
     * metodo assincrono para gerar a planilha do relatório validacao analitico rel006
     */
    void _exportRel006(Map exportParams ) {

        runAsync {
            //boolean isADM = exportParams?.usuario && exportParams?.usuario?.isADM()
            UserJobs job
            List rows
            Map emailVars =[ data        : new Date().format('dd/MM/yyyy HH:mm:ss')
                             ,dataLimite  : (new Date()+5).format('dd/MM/yyyy')
                             ,linkDownload: '' ]


            String path = grailsApplication.config.temp.dir
            String urlSalve = grailsApplication.config.url.sistema ?: 'https://salve.icmbio.gov.br/salve-estadual/'
            String hora = new Date().format('dd_MM_yyyy_hh_mm_ss')
            String tempFileName = path + 'salve_exportacao_validacao_analitico_' + hora + '.xlsx'
            String horaInicio = new Date().format('dd/MM/yyyy HH:mm:ss')
            String linkDownload = '<a target="_blank" href="' + urlSalve + 'download?fileName=' + tempFileName + '"><b>AQUI</b></a>'

            try {

                // 1º) CRIAR PARAMETROS DO JOB
                Map jobParams = [ usuario  : PessoaFisica.get( exportParams.usuario.sqPessoa )
                                  ,deRotulo : exportParams.jobRotulo ?: 'Exportação relatório validação analitico'
                                  ,deEmail  : exportParams.email ?: null
                                  ,deMensagem : ''
                                  ,vlMax: exportParams.rows.size()
                                  ,vlAtual : 0
                                  ,deAndamento : '0 de ' + exportParams.rows.size() +' (%s)' // %s é onde será exibido o percentual executado
                ]
                if( jobParams.deEmail ) {
                    jobParams.deEmailAssunto = exportParams.emailAssunto ?: 'SALVE - Exportação relatório validação analítico finalizada'
                    jobParams.deMensagem= """<p>Segue o link para baixar a planilha com os dados exportados em ${horaInicio}.</p>
                                <p>Clique ${linkDownload} para baixar o arquivo. Este link ficará disponível até o dia ${(new Date() + 5).format('dd/MM/yyyy')}).</p>
                                <p>Atenciosamente,<br>
                                <b>Equipe SALVE</b></p>"""
                }


                // CRIAR O JOB NO BANCO DE DADOS
                jobParams.noArquivoAnexo = tempFileName
                job = userJobService.createJob( jobParams ).save( flush:true )

                // criar a planilha
                PlanilhaExcel planilha = new PlanilhaExcel(tempFileName, 'Validação Analítico', false)
                planilha.setTitle(' ') // não imprimir o titulo

                // pre-processamento
                exportParams.rows.each { row ->
                    row.ds_resposta_validador_1 = row.ds_resposta_validador_1 ? row.ds_resposta_validador_1.replaceAll(/<br>/, "\n") : ''
                    row.ds_resposta_validador_2 = row.ds_resposta_validador_2 ? row.ds_resposta_validador_2.replaceAll(/<br>/, "\n") : ''
                    row.ds_resposta_validador_3 = row.ds_resposta_validador_3 ? row.ds_resposta_validador_3.replaceAll(/<br>/, "\n") : ''
                    row.no_ponto_focal = row.no_ponto_focal ? row.no_ponto_focal.replaceAll(/<br>/, "\n") : ''
                    row.no_coordenador_taxon = row.no_coordenador_taxon ? row.no_coordenador_taxon.replaceAll(/<br>/, "\n") : ''
                }

                planilha.setData(exportParams.rows)
                planilha.addColumn(['title': 'Táxon', 'column': 'nm_cientifico', 'autoFit': true, 'width': 30])  // width é a quantidade de caracteres e não pixels
                planilha.addColumn(['title': 'Grupo avaliado', 'column': 'no_grupo', 'autoFit': true, 'width': 20, 'align': 'center'])
                planilha.addColumn(['title': 'Unidade responsável', 'column': 'sg_unidade_org', 'autoFit': true, 'width': 20, 'align': 'center'])

                planilha.addColumn(['title': 'Oficina de avaliação', 'column': 'no_oficina_avaliacao', 'autoFit': true, 'width': 50, 'bgColor': '#FFFACD', 'color': '#000000'])
                planilha.addColumn(['title': 'Categoria da avaliação', 'column': 'ds_categoria_avaliacao', 'autoFit': true, 'width': 30, 'align': 'center', 'bgColor': '#FFFACD', 'color': '#000000'])
                planilha.addColumn(['title': 'Critério da avaliação', 'column': 'ds_criterio_avaliacao', 'autoFit': true, 'width': 20, 'align': 'center', 'bgColor': '#FFFACD', 'color': '#000000'])

                planilha.addColumn(['title': 'Oficina de validacao', 'column': 'no_oficina_validacao', 'autoFit': true, 'width': 50, 'bgColor': '#DCECFA', 'color': '#000000'])
                planilha.addColumn(['title': 'Situação na validação', 'column': 'ds_situacao_validacao', 'autoFit': true, 'width': 20, 'align': 'center', 'bgColor': '#DCECFA', 'color': '#000000'])
                planilha.addColumn(['title': 'Data da valiação', 'column': 'dt_validacao', 'autoFit': true, 'width': 15, 'align': 'center', 'bgColor': '#DCECFA', 'color': '#000000'])
                planilha.addColumn(['title': 'Categoria da validacao', 'column': 'ds_categoria_validacao', 'autoFit': true, 'width': 30, 'align': 'center', 'bgColor': '#DCECFA', 'color': '#000000'])
                planilha.addColumn(['title': 'Critério da validacao', 'column': 'ds_criterio_validacao', 'autoFit': true, 'width': 20, 'align': 'center', 'bgColor': '#DCECFA', 'color': '#000000'])

                planilha.addColumn(['title': 'Validador 1', 'column': 'no_validador_1', 'autoFit': true, 'width': 30, 'bgColor': '#E1E8E2', 'color': '#000000'])
                planilha.addColumn(['title': 'Resposta validador 1', 'column': 'ds_resposta_validador_1', 'autoFit': true, 'width': 40, 'bgColor': '#E1E8E2', 'color': '#000000'])

                planilha.addColumn(['title': 'Validador 2', 'column': 'no_validador_2', 'autoFit': true, 'width': 30, 'bgColor': '#ECEBCA', 'color': '#000000'])
                planilha.addColumn(['title': 'Resposta validador 2', 'column': 'ds_resposta_validador_2', 'autoFit': true, 'width': 40, 'bgColor': '#ECEBCA', 'color': '#000000'])

                planilha.addColumn(['title': 'Validador 3', 'column': 'no_validador_3', 'autoFit': true, 'width': 30, 'bgColor': '#F9E6D2', 'color': '#000000'])
                planilha.addColumn(['title': 'Resposta validador 3', 'column': 'ds_resposta_validador_3', 'autoFit': true, 'width': 40, 'bgColor': '#F9E6D2', 'color': '#000000'])

                planilha.addColumn(['title': 'Ponto focal', 'column': 'no_ponto_focal', 'autoFit': true, 'width': 40])
                planilha.addColumn(['title': 'Coordenador taxon', 'column': 'no_coordenador_taxon', 'autoFit': true, 'width': 40])

                if (!planilha.run(job)) {
                    throw new Exception(planilha.getError())
                }
            } catch (Exception error) {
                String usuario = exportParams.usuario ? '\n' +
                    exportParams?.usuario?.noPessoa +' - CPF: '+ Util.formatCpf(exportParams?.usuario?.nuCpf) + ' - ID: ' +exportParams?.usuario?.sqPessoa : ''
                Util.printLog('SALVE - RelatorioController._exportRel006()','Erro ao exportar resultado ' + usuario
                    ,error.getMessage())
                if( job ) {
                    // encerrar o job
                    userJobService.error( job.id, error.getMessage() )
                }
            }
        }
    }

    def getOficinaValidacao(){
        Map result = [status:0, msg:'',type:'success', data:[]]
        try {
            if( !params.sqCicloAvaliacao ) {
                throw new Exception('Ciclo de avaliação não informado')
            }
            DadosApoio oficinaValidacao = dadosApoioService.getByCodigo('TB_TIPO_OFICINA', 'OFICINA_VALIDACAO')
            CicloAvaliacao ciclo = CicloAvaliacao.get( params.sqCicloAvaliacao.toLong() )
            Oficina.createCriteria().list {
                eq('cicloAvaliacao',ciclo)
                eq('tipoOficina', oficinaValidacao)
                order('dtFim','desc')
            }.each {
                result.data.push( id:it.id, descricao:( it.noOficina ?: it.sgOficina ) )
            }
        } catch( Exception e ){
            result.status = 1
            result.type='error'
            result.msg = e.getMessage()
        }
        render result as JSON
    }


    /**
     * relatório de pontos focais e coordenadores de taxon
     * @return
     */
    def rel006() {

        String errorMessage = ''
        Map sqlPars = [:]
        CicloAvaliacao ciclo
        String cmdSql
        List wherePasso1 = []
        List wherePasso2 = []
        List wherePasso3 = []
        List wherePasso4 = []

        if (!params.gridId && !params.exportAs) {
            List listCicloAvaliacao = CicloAvaliacao.list(sort: 'nuAno', order: 'desc');
            render('view': 'rel006', model: ['listCicloAvaliacao': listCicloAvaliacao])
            return
        }

        DadosApoio oficinaValidacao = dadosApoioService.getByCodigo('TB_TIPO_OFICINA', 'OFICINA_VALIDACAO')
        DadosApoio oficinaAvaliacao = dadosApoioService.getByCodigo('TB_TIPO_OFICINA', 'OFICINA_AVALIACAO')

        ciclo = CicloAvaliacao.get(params.sqCicloAvaliacao.toLong() )
        sqlPars.sqCicloAvaliacao = ciclo.id.toInteger()

        // filtrar pela unidade do usuário logado quando não for ADM
        if ( ! session.sicae.user.isADM() ) {
            params.sqUnidadeOrg = session.sicae.user.sqUnidadeOrg.toLong()
        }

        // FILTRO PELA UNIDADE
        if( params.sqUnidadeFiltro ) {
            wherePasso2.push("ficha.sq_unidade_org in (${ params.sqUnidadeFiltro })")
        }


        // FILTRO PELA OFICINA DE VALIDAÇÃO
        if( params.sqOficinaValidacaoFiltro){
            wherePasso1.push("o.sq_oficina = :sqOficinaValidacao")
            sqlPars.sqOficinaValidacao = params.sqOficinaValidacaoFiltro.toLong()
        }

        // FILTRO PELA OFICINA DE AVALIAÇÃO
        if( params.sqOficinaAvaliacaoFiltro){
            wherePasso4.push("passo4.sq_oficina_avaliacao = :sqOficinaAvaliacao ")
            sqlPars.sqOficinaAvaliacao = params.sqOficinaAvaliacaoFiltro.toLong()
        }

        // filtrar pelos grupos selecionados
        if( params.sqGrupoFiltro ){
            wherePasso2.push("ficha.sq_grupo in (${params.sqGrupoFiltro})")
        }

        // FILTRAR PELA SITUAÇÃO DA FICHA NA VALIDAÇÃO
        if( params.sqSituacaoFiltro ){
            wherePasso2.push("oficina_ficha.sq_situacao_ficha in (${params.sqSituacaoFiltro})")
        }

        // FILTRAR PELO PERIODO
        if (params.dtInicioValidacaoFiltro) {
            try {
                sqlPars.dtInicio = new Date().parse('dd/MM/yyyy', params.dtInicioValidacaoFiltro).format('yyyy-MM-dd')
            } catch (Exception e) {
                errorMessage='Data inicial inválida!'
            }
        }
        if (params.dtFimValidacaoFiltro) {
            try {
                sqlPars.dtFim = new Date().parse('dd/MM/yyyy', params.dtFimValidacaoFiltro).format('yyyy-MM-dd')
                sqlPars.dtFim +=' 23:59:59'
            } catch (Exception e) {
                errorMessage = 'Data final inválida!'
            }
        }
        if (sqlPars.dtInicio && sqlPars.dtFim && sqlPars.dtInicio > sqlPars.dtFim) {
            String dataInicio = sqlPars.dtInicio
            sqlPars.dtInicio = sqlPars.dtFim
            sqlPars.dtFim = dataInicio
        }
        // fichas validadas  no período de...
        if (sqlPars.dtInicio && sqlPars.dtFim) {
            wherePasso2.push("ficha.dt_aceite_validacao between '" + sqlPars.dtInicio + "' and '" + sqlPars.dtFim + "'")
            sqlPars.remove('dtInicio')
            sqlPars.remove('dtFim')
        }
        // validacoes que iniciaram após...
        if (sqlPars.dtInicio) {
            wherePasso2.push("ficha.dt_aceite_validacao >= '" + sqlPars.dtInicio + "'")
            sqlPars.remove('dtInicio')
        }
        // oficinas que terminaram até ...
        if (sqlPars.dtFim) {
            wherePasso2.push("ficha.dt_aceite_validacao <= '" + sqlPars.dtFim + "'")
            sqlPars.remove('dtFim')
        }
        // FILTRAR PELO NOME DOS VALIDADORES
        if( params.noValidadorFiltro){
            params.noValidadorFiltro = params.noValidadorFiltro.replaceAll(/%/,'')
            List whereOr = []
            params.noValidadorFiltro.split(',').each {
                whereOr.push( "public.fn_remove_acentuacao( passo3.json_validadores::text ) ilike '%${Util.removeAccents(it.trim())}%'")
            }
            wherePasso3.push('(' + whereOr.join(' OR ') + ')')
        }


        String sqlOrderBy=''
        if( params.sortColumns ){
            List sortColumns = params.sortColumns.split(':')
            sqlOrderBy = '\nORDER BY '+sortColumns[0] + ( sortColumns[1] ? ' '+sortColumns[1] : '')
            if( ! ( params.sortColumns.toString() =~ /nm_cientifico/) ) {
                sqlOrderBy += ',nm_cientifico'
            }
        }

        // criar comando SQL
        cmdSql = """with passo1 as (
                    select o.sq_oficina as sq_oficina_validacao
                         , o.sg_oficina as sg_oficina_validacao
                         , o.no_oficina as no_oficina_validacao
                         , DATE_TRUNC('day',o.dt_inicio) as dt_inicio_validacao
                    from salve.oficina o
                    where sq_ciclo_avaliacao = :sqCicloAvaliacao
                      and o.sq_tipo_oficina = ${oficinaValidacao.id.toString()}
                      ${ (wherePasso1 ? '\n and ' + wherePasso1.join('\nand ') :'') }
                ),
                     -- ler as especies da oficina de validação
                     passo2 as (
                         select oficina_ficha.sq_ficha
                              , passo1.sq_oficina_validacao
                              , coalesce(passo1.sg_oficina_validacao, passo1.no_oficina_validacao) as no_oficina_validacao
                              , passo1.dt_inicio_validacao
                              , ficha.nm_cientifico
                              , u.sg_unidade_org
                              , grupo.ds_dados_apoio                                               as no_grupo
                              , situacao_validacao.ds_dados_apoio                                  as ds_situacao_validacao
                              , ficha.dt_aceite_validacao                                          as dt_validacao
                              , case when categoria_validada.ds_dados_apoio is null then '' else concat(categoria_validada.ds_dados_apoio, ' (', categoria_validada.cd_dados_apoio,
                                       ')') end                                                        as ds_categoria_validacao
                              , oficina_ficha.ds_criterio_aval_iucn                                as ds_criterio_validacao
                         from salve.oficina_ficha
                                  inner join passo1 on passo1.sq_oficina_validacao = oficina_ficha.sq_oficina
                                  inner join salve.ficha on ficha.sq_ficha = oficina_ficha.sq_ficha
                                  inner join salve.vw_unidade_org u on u.sq_pessoa = ficha.sq_unidade_org
                                  left outer join salve.dados_apoio as grupo on grupo.sq_dados_apoio = ficha.sq_grupo
                                  left outer join salve.dados_apoio as situacao_validacao
                                                  on situacao_validacao.sq_dados_apoio = oficina_ficha.sq_situacao_ficha
                                  left outer join salve.dados_apoio as categoria_validada
                                                  on categoria_validada.sq_dados_apoio = oficina_ficha.sq_categoria_iucn
                                ${wherePasso2 ? ' where '+wherePasso2.join('\nand ') : ''}

                     ), passo3 as (
                    -- adicionar os validadores
                    select passo2.*
                         , avaliacao.*
                         , salve.json_validadores(passo2.sq_ficha, passo2.sq_oficina_validacao)::text AS json_validadores
                    from passo2
                             -- ler a última oficina de avaliação
                             left join lateral (
                        select oficina.sq_oficina as sq_oficina_avaliacao
                             , coalesce(oficina.sg_oficina, oficina.no_oficina)                                        as no_oficina_avaliacao
                             , case when categoria_avaliada.ds_dados_apoio is null then '' else concat( categoria_avaliada.ds_dados_apoio, ' (', categoria_avaliada.cd_dados_apoio,
                                      ')') end                                                                             as ds_categoria_avaliacao
                             , oficina_ficha.ds_criterio_aval_iucn                                                     as ds_criterio_avaliacao

                        from salve.oficina_ficha
                                 inner join salve.oficina on oficina.sq_oficina = oficina_ficha.sq_oficina
                                 left outer join salve.dados_apoio as categoria_avaliada
                                                 on categoria_avaliada.sq_dados_apoio = oficina_ficha.sq_categoria_iucn
                        where oficina_ficha.sq_ficha = passo2.sq_ficha
                          and oficina.sq_tipo_oficina = ${oficinaAvaliacao.id.toString()}
                          and DATE_TRUNC('day', oficina.dt_fim) <= DATE_TRUNC('day', passo2.dt_inicio_validacao)
                        order by oficina.dt_fim desc
                        limit 1
                        ) as avaliacao on true
                ), passo4 as (
                    -- adicionar o ponto focal
                    select passo3.*, ponto_focal.no_ponto_focal
                    ,coordenador_taxon.no_coordenador_taxon
                    from passo3
                    left join lateral(
                        select array_to_string( array_agg( pf.no_pessoa order by lower(pf.no_pessoa) ),'|') as no_ponto_focal
                        from salve.ficha_pessoa
                        left outer join salve.dados_apoio as papel on papel.sq_dados_apoio = ficha_pessoa.sq_papel
                        left outer join salve.vw_pessoa_fisica pf on pf.sq_pessoa = ficha_pessoa.sq_pessoa
                        where ficha_pessoa.in_ativo = 'S'
                          and papel.cd_sistema ='PONTO_FOCAL'
                          and ficha_pessoa.sq_ficha = passo3.sq_ficha
                        ) as ponto_focal on true
                        -- adicionar o coordenador taxon
                    left join lateral(
                        select array_to_string( array_agg( pf.no_pessoa order by lower(pf.no_pessoa) ),'|') as no_coordenador_taxon
                        from salve.ficha_pessoa
                                 left outer join salve.dados_apoio as papel on papel.sq_dados_apoio = ficha_pessoa.sq_papel
                        left outer join salve.vw_pessoa_fisica pf on pf.sq_pessoa = ficha_pessoa.sq_pessoa
                        where ficha_pessoa.in_ativo = 'S'
                          and papel.cd_sistema ='COORDENADOR_TAXON'
                          and ficha_pessoa.sq_ficha = passo3.sq_ficha
                        ) as coordenador_taxon on true
                        ${(wherePasso3 ? '\n where ' + wherePasso3.join('\nand ') : '')}
                )
                select passo4.*
                from passo4
                ${(wherePasso4 ? '\n where ' + wherePasso4.join('\nand ') : '')}
                ${sqlOrderBy}
                """
        /** /
         // zzz
         println ' '
         println ' '
         println cmdSql
         println sqlPars
         println ' '
         println ' '
         println ' '
         /**/
        Map mapResultado = [rows:0]
        //List rows = sqlService.execSql(cmdSql, parametrosConsulta)
        if ( params.exportAs == 'ROW' ) {
            mapResultado.rows = sqlService.execSqlCache(cmdSql, sqlPars)
        } else {
            mapResultado = sqlService.paginate(cmdSql, sqlPars, params, 100, 10, 1, debugSql)
        }

        // fazer pre-processamento dos dados retornados do banco de dados
        mapResultado.rows.eachWithIndex { row, lin ->
            JSONObject jsonValidadores = null
            List nomesValidadores  = []
            List respostas         = []
            List respostasRevisao  = []
            // ler os validadores do json retornado do banco
            try {
                jsonValidadores = JSON.parse(row.json_validadores)
            } catch( Exception e) {}
            if( jsonValidadores ){
                nomesValidadores = jsonValidadores.names()
                nomesValidadores.eachWithIndex{ nome,i ->
                    String resposta = jsonValidadores[nomesValidadores[i]]?.de_resposta ?: ''
                    String respostaRevisao = jsonValidadores[nomesValidadores[i]]?.de_resposta_revisao ?: ''
                    if( respostaRevisao ){
                        resposta = '<span class="text-strike">'+resposta+'</span><br><span class="red">Revisão da coordenação</span><br>' + respostaRevisao
                    }
                    respostas.push( resposta )

                }
            }

            // colocar negrito e itálico e remover o autor do nome cientifico
            row.nm_cientifico = row.nm_cientifico ? Util.ncItalico( row.nm_cientifico, true) : ''

            // exibir a data da validação no formato dd/mm/yyyy
            row.dt_validacao = row.dt_validacao ? row.dt_validacao.format('dd/MM/yyyy'):''

            // adicionar quebra de linha nos nomes e capitalizar
            row.no_ponto_focal = row.no_ponto_focal ? row.no_ponto_focal.replaceAll(/\|/,'<br>') : ''
            if( row.no_ponto_focal ) {
                row.no_ponto_focal = row.no_ponto_focal.split(/\|/).collect{ Util.capitalize(it) }.join('<br>')
            }

            // adicionar quebra de linha nos nomes e capitalizar
            if( row.no_coordenador_taxon ) {
                row.no_coordenador_taxon = row.no_coordenador_taxon.split(/\|/).collect{ Util.capitalize(it) }.join('<br>')
            }
            row.no_validador_1          = nomesValidadores[0] ? Util.capitalize(nomesValidadores[0] ) : ''
            row.ds_resposta_validador_1 = respostas[0] ?: ''
            row.no_validador_2          = nomesValidadores[1] ?  Util.capitalize(nomesValidadores[1]): ''
            row.ds_resposta_validador_2 =  respostas[1] ?: ''
            row.no_validador_3          = nomesValidadores[2] ?  Util.capitalize(nomesValidadores[2]): ''
            row.ds_resposta_validador_3 =  respostas[2] ?: ''
        }


        // exportar para planilha o resultado
        if ( params.exportAs == 'ROW' ) {
            Map result = [status:0, msg:'',type:'modal']
            try {
                // parâmetros para execução assyncrona
                Map exportParams = [
                     rows                : mapResultado.rows
                    ,usuario             : session?.sicae?.user
                    ,email               : params.email
                    ,emailMessage        : ''
                ]

                registrarTrilhaAuditoria('exportar_relatorio_validacao_analitico', 'relatorio_validacao_analitico', exportParams)
                _exportRel006(exportParams)

                result.msg= 'A exportação está em andamento.<br><br>Ao finalizar será '+
                    (params.email ? 'enviado para o email <b>'+params.email+'</b> o link para baixar a planilha.' : 'exibida a tela para baixar a planilha.')

            } catch(Exception e ) {
                result.status = 1
                result.msg    = e.getMessage()
            }
            render result as JSON
            return
        }

        // renderizar o gride
        render(template: "grideRel006", model: [rows          : mapResultado.rows
                                                , pagination  : mapResultado.pagination
                                                , errorMessage: errorMessage
                                                ])

    }


}
