package br.gov.icmbio

import grails.converters.JSON
import org.codehaus.groovy.grails.web.servlet.mvc.GrailsHttpSession

class TraducaoFichaController extends BaseController {
    int maxFichasExportarPdf = 1000

    // injeção das dependências
    def traducaoFichaService
    def dadosApoioService
    def exportDataService

    /**
     * exibir a tela inicial do módulo de tradução das fichas
     * @return
     */
    def index() {
        // listar os ciclos de avaliação
        List listCiclosAvaliacao = [ session.getAttribute('ciclo') ] //CicloAvaliacao.list( sort: 'nuAno', order: 'desc' );
        List listSituacaoFicha = dadosApoioService.getTable('TB_SITUACAO_FICHA').findAll{
            it.codigoSistema =~ /^(FINALIZADA|PUBLICADA)$/
        }
        boolean grideFiltroUnidade = true
        render view: 'index', model: [ listCiclosAvaliacao: listCiclosAvaliacao
            ,listSituacaoFicha:listSituacaoFicha
            ,grideFiltroUnidade:grideFiltroUnidade
        ]
    }

    /**
     * criação do gride de fichas para tradução
     */
    def getGridFichaTraducao(){
        Map data = traducaoFichaService.getDataGridTraducaoFicha( params, session.sicae.user )
        if( params?.format?.toString()?.toLowerCase() =='xls'){
            return _exportarPlanilha( data.rows )
        }
        render(template: "gridTraducaoFicha", model: [
            rows          : data.rows
            , gridId        : params.gridId
            , pagination    : data.pagination
        ])
    }

    /**
     * realizar a tradução de uma ou mais fichas
     * @return
     */
    def traduzirFichasEmLote(){
        Map res=[status:0,msg:'Ficha(s) adicionada(s) à fila de tradução com SUCESSO!',type:'success']
        try {
           params.sqPessoa =  session?.sicae?.user?.sqPessoa?.toLong()
           traducaoFichaService.traduzirFichasEmLote( params )

        } catch( Exception e ){
            res.type='modal'
            res.status = 1;
            res.msg=e.getMessage()
        }
        render res as JSON
    }

    /**
     * excluir a tradução das fichas selecionadas
     * @return
     */
    def excluirTraducaoFichasEmLote(){
        Map res=[status:0,msg:'Ficha(s) adicionada(s) à fila de tradução com SUCESSO!',type:'success']
        try {
            params.sqPessoa =  session?.sicae?.user?.sqPessoa?.toLong()
            traducaoFichaService.excluirTraducaoFichasEmLote( params )

        } catch( Exception e ){
            res.type='modal'
            res.status = 1;
            res.msg=e.getMessage()
        }
        render res as JSON
    }

    /**
     * disparar o processamento assyncrono de tradução das fichas adicionadas na fila
     * @return
     */
    def runAsyncTranslate(){
        Map res=[status:0,msg:'Tradução iniciada com SUCESSO!',type:'success']
        try {
            params.sqPessoa =  session?.sicae?.user?.sqPessoa?.toLong()
            traducaoFichaService.runAsyncTranslate( params )

        } catch( Exception e ){
            res.type='modal'
            res.status = 1;
            res.msg=e.getMessage()
        }
        render res as JSON
    }

    /**
     * exibir a ficha traduzida no formato html para fazer ou revisar a tradução
     * tópico a tópico
     */
    def fichaRevisao(){
        Map fichaRevisao = traducaoFichaService.getFichaRevisao(params.sqFicha.toLong())
        render(template: "fichaRevisao", model: [fichaRevisao:fichaRevisao])
    }

    /**
     * metodo para gravacao da tradução ou revisão da tradução no banco de dados
     * @return
     */
    def saveTextoTraduzido() {
        Map res = [status:0,msg:'Dados gravados com SUCESSO', type:'success',data:[:]]
        try {
            if( ! session?.sicae?.user ){
                throw new Exception('Sessão expirada.')
            }
            params.sqPessoa = session.sicae.user?.sqPessoa?.toLong()
            // retornar o texto traduzido com stripTag aplicado para o cliente
            res.data.txTraduzido =  traducaoFichaService.saveTextoTraduzido( params )
        } catch( Exception e ){
            res.type    = 'error'
            res.msg     = e.getMessage()
            res.status  = 1
        }
        render res as JSON
    }

    /**
     * metodo para marcar/desmarcar se a tradução está revisada
     * @return
     */
    def saveRevisado() {

        Map res = [status:0,msg:'Dados gravados com SUCESSO', type:'success',data:[:]]
        try {
            if( ! session?.sicae?.user ){
                throw new Exception('Sessão expirada.')
            }
            params.sqPessoa = session.sicae.user?.sqPessoa?.toLong()
            res.data = traducaoFichaService.saveRevisado(params)
        } catch( Exception e ){
            res.type    = 'error'
            res.msg     = e.getMessage()
            res.status  = 1
        }
        render res as JSON
    }

    /**
     * ler o andamento dos percentuais das fichas da pagina atual do gride de tradução
     * @return
     */
    def getPercentuaisPage(){
        Map res=[status:0,msg:'',type:'success', data:[] ]
        try {
            res.data = traducaoFichaService.getPercentuaisPage( params )
        } catch( Exception e ){
            res.type='modal'
            res.status = 1;
            res.msg=e.getMessage()
        }
        render res as JSON
    }

    /**
     * gerar arquivo zip dos pdfs das fichas em segundo plano
     * @return
     */
    def gerarPdfFichas() {
        Map res = [msg:'', status:0, type:'success']
        List idsFichas = params.ids.split(',')
        try {
            if (idsFichas && idsFichas.size() > this.maxFichasExportarPdf) {
                throw new Exception('Limite máximo para esta solicitação é de ' + this.maxFichasExportarPdf.toString() + ' fichas.')
            }
            // criar variavel local selfSession para utilzar workers
            GrailsHttpSession selfSession = session
            Map workerData = [sqPessoa    : session?.sicae?.user?.sqPessoa
                              , contexto  : 'traducao_ficha'
                              , email     : params.email
                              , pendencias: params.pendencias ?: 'N'
                              , format    : 'PDF'
                              , language  : 'en'
                              , idsFichas : idsFichas
            ]
            res.msg = 'A exportação de <b>' + idsFichas.size().toString() + ' ficha(s)</b> está em andamento.<br><br>Ao finalizar será ' +
                (params.email ? 'enviado para o email <b>' + params.email + '</b> o link para baixar o arquivo.' : 'exibida a tela para baixar o arquivo.') +
                '<br><br>Dependendo da quantidade de fichas este procedimento poderá demorar. <span class="red">(Não encerre a sua sessão)</span>'


            registrarTrilhaAuditoria('exportar_fichas_pdf_en', workerData.contexto, workerData)
            runAsync {
                exportDataService.gridFichasZip(selfSession, workerData)
            }
        } catch( Exception e ){
            res.type='error'
            res.msg = e.getMessage()
            res.status = 1
        }
        render res as JSON
    }

    /**
     * exportar o gride atual para planilha
     * @return
     */
    protected void _exportarPlanilha( List rows = []) {
        Map res = [msg:'', status:0, type:'success']
        try {
            if (rows.size() < 1 ) {
                throw new Exception('Nenhum registro encontrado')
            }
            // criar variavel local selfSession para utilzar workers
            GrailsHttpSession selfSession = session
            Map workerData = [sqPessoa    : session?.sicae?.user?.sqPessoa
                              , contexto  : 'traducao_ficha'
                              , email     : params.email
                              , rows      : rows
            ]
            res.msg = 'A exportação de <b>' + rows.size().toString() + ' ficha(s)</b> para planilha está em andamento.<br><br>Ao finalizar será ' +
                (params.email ? 'enviado para o email <b>' + params.email + '</b> o link para baixar o arquivo.' : 'exibida a tela para baixar o arquivo.')
                //'<br><br>Dependendo da quantidade de fichas este procedimento poderá demorar. <span class="red">(Não encerre a sua sessão)</span>'

            runAsync {
                exportDataService.gridTraducaoFicha(selfSession, workerData)
            }
        } catch( Exception e ){
            res.type='error'
            res.msg = e.getMessage()
            res.status = 1
        }
        render res as JSON
    }

}
