/**
 * Este controlador é resposável por receber as ações referentes ao cadastro aba Populção da ficha de espécies
 * Regras:
 *
 *
 *
 *
 *
 */

package br.gov.icmbio
import grails.converters.JSON;

import br.gov.icmbio.DadosApoioService

class FichaPopulacaoController extends FichaBaseController {

	def dadosApoioService
	def messageSource
    def fichaService

	def index() {

		if( !params.sqFicha )
		{
			render 'Id da ficha deve ser informado!'
			return;
		}
		Ficha ficha 			= Ficha.get(params.sqFicha)
		List listTendencia 		= dadosApoioService.getTable('TB_TENDENCIA')
		List listUnidadeTempo 	= dadosApoioService.getTable('TB_UNIDADE_TEMPO')
        //listTendencia.find { it.codigoSistema ==''}
        DadosApoio tendenciaDesconhecida = listTendencia.find {
             it.codigoSistema == 'DESCONHECIDO'
        }


		render(template:"/ficha/populacao/index",
			'model':['ficha'			    :ficha,
					'listTendencia' 	    :listTendencia,
					'listUnidadeTempo' 	    :listUnidadeTempo,
                    'tendenciaDesconhecida' :tendenciaDesconhecida
					]
				)
	}

	def saveFrmPopulacao()
	{
		if( !params.sqFicha )
		{
			return this._returnAjax(1, 'ID da ficha não informado.', "error")
		}
		Ficha ficha = Ficha.get(params.sqFicha)
		ficha.properties = params
		ficha.tendenciaPopulacional = DadosApoio.get(params.sqTendenciaPopulacional);
		ficha.periodoTaxaCrescPopul = DadosApoio.get(params.sqPeriodoTaxaCrescPopul);
        ficha.medidaTempoGeracional = DadosApoio.get(params.sqMedidaTempoGeracional)
        ficha.tipoReduPopuPassRev 	= DadosApoio.get(params.sqTipoReduPopuPassRev)

        try {
            ficha.save(flush:true)
			fichaService.updatePercentual( ficha )
            //fichaService.atualizarPercentualPreenchimento( ficha )
            return this._returnAjax(0, getMsg(1), "success")
        } catch(Exception e) {
            println e.getMessage()
            return this._returnAjax(1, getMsg(2), "error")
        }
	}

/*
	def saveFrmPopulacaoGrafico()
	{
		if( !params.sqFicha )
		{
			return this._returnAjax(1, 'ID da ficha não informado.', "error")
		}

		// gravar o anexo
		def f = request.getFile('fldPopulacaoAnexoGrafico')
    	if (f && ! f.empty)
    	{
    		DadosApoio contexto =dadosApoioService.getByCodigo('TB_CONTEXTO_ANEXO','GRAFICO_POPULACAO');
    		if( !contexto )
    		{
			  return this._returnAjax(1, 'Contexto GRAFICO_POPULACAO não cadastrado na TB_CONTEXTO_ANEXO', "error")
    		}
			File  savePath = new File( grailsApplication.config.anexos.dir );
			if( ! savePath.exists() )
			{
				if( ! savePath.mkdirs() )
				{
					return this._returnAjax(1, 'Não foi possivel criar o diretório ' + savePath.asString(), "error");
				}
			}
			Ficha ficha = Ficha.get( params.sqFicha );
			String fileName = f.getOriginalFilename();
			String fileExtension = fileName.substring(fileName.lastIndexOf("."));
    		fileName=savePath.absolutePath+'/'+f.inputStream.text.toString().encodeAsMD5()+fileExtension;
			if( ! new File(fileName).exists() )
			{
				f.transferTo( new File(fileName) );
			}
			FichaAnexo fa = FichaAnexo.findByFichaAndDeLocalArquivo(ficha,fileName);
			if( ! fa ){
				fa = new FichaAnexo();
				fa.ficha 			= ficha;
				fa.contexto 		= contexto;
				fa.deLocalArquivo 	= fileName;
			}
			fa.deLegenda            = params.deLegendaPopulacaoGrafico
			fa.noArquivo			= f.getOriginalFilename();
			fa.deTipoConteudo 		= f.getContentType();
			fa.save( flush:true );
    		return this._returnAjax(0);
    	}
	}
*/
	def getGridPopulGraficos()
	{
		if( !params.sqFicha )
		{
			render  'ID da ficha não informado.';
			return;
		}
    	DadosApoio contexto = dadosApoioService.getByCodigo('TB_CONTEXTO_ANEXO','GRAFICO_POPULACAO');
    	List dados = FichaAnexo.findAllByFichaAndContexto(Ficha.get( params.sqFicha),contexto );
		render( template:'/ficha/populacao/divGridPopulGraficos',model:[listFichaAnexo:dados]);
	}
/*
	def deletePopulGrafico()
	{
		if( !params.id )
		{
			return this._returnAjax(1,'Id não informado','error');
		}
		FichaAnexo fa = FichaAnexo.get(params.id);
		String deLocalArquivo = fa.deLocalArquivo;
		File file = new File( deLocalArquivo);
	 	if( file.exists() )
	 	{
	 		// apagar do disco
	 		file.delete();
	 	}
	 	// apagar do banco de dados
		fa.delete( flash:true);
		return this._returnAjax(0);
	}

	def getGrafico()
	{
		if( !params.id)
		{
			response.sendError(404);
			return;
		}
		FichaAnexo pa = FichaAnexo.get( params.id);
		if( ! pa )
		{
			response.sendError(404);
		}
		File file = new File(pa.deLocalArquivo);
		if( ! file.exists())
		{
			response.sendError(400);
		}
		String fileType = pa.deTipoConteudo ? pa.deTipoConteudo : 'application/octet-stream';
		String fileName = pa.noArquivo.replaceAll(' ','_');
  		render(file: file, fileName: fileName, contentType:fileType);
	}
*/

	// aba População Conhecida/Estimada
	def tabPopulEstimada()
	{
		List listUnidadeAbundancia 	= dadosApoioService.getTable('TB_UNID_ABUNDANCIA_POPULACAO').sort{ it.descricao }
		List listTipoAbrangencia = dadosApoioService.getTable('TB_TIPO_ABRANGENCIA').sort{ it.descricao }
		render(template:"/ficha/populacao/formPopulEstimada",'model':['listUnidadeAbundancia':listUnidadeAbundancia
			,listTipoAbrangencia:listTipoAbrangencia]);
	}

	def saveFrmPopulEstimada()
	{
		if( !params.sqFicha )
		{
			return this._returnAjax(1, 'ID da ficha não informado.', "error")
		}

		FichaPopulEstimadaLocal reg

		if( params.sqFichaPopulEstimadaLocal )
		{
			reg = FichaPopulEstimadaLocal.get( params.sqFichaPopulEstimadaLocal)
		}
		else
		{
			reg = new FichaPopulEstimadaLocal()
			reg.ficha = Ficha.get( params.sqFicha )
		}
		reg.properties = params
		reg.unidAbundanciaPopulacao = DadosApoio.get(params.sqUnidAbundanciaPopulacao)

        if( params.sqAbrangencia )
        {
            reg.abrangencia = Abrangencia.get( params.sqAbrangencia.toInteger() )
        }
        else
        {
            reg.abrangencia=null
        }
	    try {
            reg.validate()
            println reg.getErrors()

            reg.save(flush:true)

			// atualizar log da ficha, quem alterou
			reg.ficha.dtAlteracao = new Date()
			reg.ficha.save()

			this._returnAjax(0, getMsg(1), "success")
        } catch(Exception e) {
            println e.getMessage()
            this._returnAjax(1, getMsg(2), "error")
        }
	}
	def getGridPopulEstimada()
	{
		if( !params.sqFicha)
		{
			render ''
			return
		}
		List listFichaPopulEstimada = FichaPopulEstimadaLocal.findAllByFicha(Ficha.get(params.sqFicha))
		render( template:'/ficha/populacao/divGridPopulEstimada',model:['listFichaPopulEstimada':listFichaPopulEstimada])
	}

	def editPopulEstimada()
	{
		if( ! params.id )
		{
			render [:] as JSON;
			return
		}
		FichaPopulEstimadaLocal reg = FichaPopulEstimadaLocal.get(params.id);
		if( reg?.id )
		{
			render reg.asJson();
			return;
		}
		render [:] as JSON;
	}

	def deletePopulEstimada()
	{

		FichaPopulEstimadaLocal reg = FichaPopulEstimadaLocal.get(params.id);
		if( reg )
		{
			Ficha ficha = reg.ficha
			reg.delete(flush:true)

			// atualizar log da ficha, quem alterou
			ficha.dtAlteracao = new Date()
			ficha.save()

			return this._returnAjax(0, getMsg(4), "success")
		}
		else
		{
			return this._returnAjax(1, getMsg(51), "error")
		}
		render '';
	}



	// aba População Area de Vida

	def tabPopAreaVida()
	{
		List listUnidadeArea 	= dadosApoioService.getTable('TB_UNIDADE_AREA').sort{ it.descricao }
		render(template:"/ficha/populacao/formAreaVida",model:['listUnidadeArea':listUnidadeArea]);
	}

	def getGridPopAreaVida()
	{
		if( !params.sqFicha)
		{
			render ''
			return
		}
		List listFichaPopAreaVida = FichaAreaVida.findAllByFicha(Ficha.get(params.sqFicha))
		render( template:'/ficha/populacao/divGridPopAreaVida',model:['listFichaPopAreaVida':listFichaPopAreaVida])
	}

	def saveFrmPopAreaVida()
	{
		if( !params.sqFicha )
		{
			return this._returnAjax(1, 'ID da ficha não informado.', "error")
		}

		FichaAreaVida reg

		if( params.sqFichaAreaVida )
		{
			reg = FichaAreaVida.get( params.sqFichaAreaVida)
		}
		else
		{
			reg = new FichaAreaVida()
			reg.ficha = Ficha.get( params.sqFicha )
		}
		reg.properties = params
		reg.unidadeArea = DadosApoio.get(params.sqUnidadeArea)

        try {
            reg.save(flush:true)

			// atualizar log da ficha, quem alterou
			reg.ficha.dtAlteracao = new Date()
			reg.ficha.save()

			this._returnAjax(0, getMsg(1), "success")
        } catch(Exception e) {
            println e.getMessage()
            this._returnAjax(1, getMsg(2), "error")
        }
	}

	def editPopAreaVida()
	{
		FichaAreaVida reg = FichaAreaVida.get(params.id);
		if( reg?.id )
		{
			render reg.asJson();
			return;
		}
		render [:] as JSON;
	}

	def deletePopAreaVida()
	{

		FichaAreaVida reg = FichaAreaVida.get(params.id);
		if( reg )
		{
			Ficha ficha = reg.ficha
			reg.delete(flush:true)

			// atualizar log da ficha, quem alterou
			ficha.dtAlteracao = new Date()
			ficha.save()

			return this._returnAjax(0, getMsg(4), "success")
		}
		else
		{
			return this._returnAjax(1, getMsg(51), "error")
		}
		render ''
	}

}
