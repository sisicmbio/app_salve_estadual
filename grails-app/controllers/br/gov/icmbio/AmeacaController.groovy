package br.gov.icmbio
import grails.converters.JSON

class AmeacaController extends BaseController {

    def dadosApoioService
    def sqlService

    protected List _getData(Long idPai = null ){
        String cmdSql = """select distinct a.sq_dados_apoio
                    , a.sq_dados_apoio_pai
                    , a.cd_dados_apoio
                    , a.ds_dados_apoio
                    , a.de_dados_apoio_ordem
                    , c.cd_iucn_ameaca
                    , c.ds_iucn_ameaca
                    , filhos.nu_filhos
                    from salve.dados_apoio a
                    left join salve.iucn_ameaca_apoio b on b.sq_dados_apoio = a.sq_dados_apoio
                    left join salve.iucn_ameaca c on c.sq_iucn_ameaca = b.sq_iucn_ameaca
                    left join lateral (
                        select count(*) as nu_filhos from salve.dados_apoio x where x.sq_dados_apoio_pai = a.sq_dados_apoio
                    ) as filhos on true
                    where a.sq_dados_apoio_pai = :sqPai
                    order by a.de_dados_apoio_ordem, a.ds_dados_apoio"""
        Map sqlParams = [:]
        if( ! idPai )
        {
            DadosApoio pai = dadosApoioService.getByCodigo('TB_CRITERIO_AMEACA_IUCN')
            sqlParams.sqPai = pai.id
        } else {
            sqlParams.sqPai = idPai
        }
        return sqlService.execSql(cmdSql, sqlParams)
    }

    def index() {
        List data = _getData()
        Map pagination = ['totalRecords': data.size(), 'pageCurrent': 1,'pageSize':50]
        this._pagination(pagination)
        render (view: 'index', model:['data': data, 'pagination': pagination])
    }


    def getTreeView() {
        response.setHeader("Content-Type", "application/json")
        response.setHeader("Access-Control-Allow-Origin","*" )
        List data = _getData( params.key ? params.key.toLong() : null )
        Map result=['status':'ok','result':[]];
        data.each {
            String codigoApoio = it.cd_dados_apoio && (it.cd_dados_apoio.toString() =~ /^[0-9]/) ? it.cd_dados_apoio : ''
            result['result'].push([
                'key'         : it.sq_dados_apoio
                ,'pai'        : (it.sq_dados_apoio_pai ?: 0 )
                ,'title'      :  ( codigoApoio ? codigoApoio + ' ' : '' ) + it.ds_dados_apoio
                ,'iucnAmeaca' : ( it.cd_iucn_ameaca ?  it.cd_iucn_ameaca + ' '+ it.ds_iucn_ameaca : '' )
                // campos controle da fancytree
                ,'select'   : false
                ,'expanded' : false
                ,'folder'   : (it?.nu_filhos > 0)
                ,'lazy'     : (it?.nu_filhos > 0)
            ] );
        }
            sleep(500)
        render result as JSON;
    }

    def getForm()
    {
        DadosApoio ameaca
        IucnAmeacaApoio iucnAmeacaApoio
        if( params.id && params.operacao == 'ameaca.edit' )
        {
            ameaca = DadosApoio.get( params.id)
            iucnAmeacaApoio = IucnAmeacaApoio.findByAmeaca( ameaca )
        }
        else
        {
            ameaca = new DadosApoio();
            iucnAmeacaApoio = new IucnAmeacaApoio()
        }
        List listCriteriosAmeaca = dadosApoioService.getTable('TB_CRITERIO_AMEACA_IUCN').sort{it.ordem }
        List listIucnAmeaca = IucnAmeaca.findAll()

        render( template:'frmAmeaca',model:[ ameaca:ameaca
                                             ,listCriteriosAmeaca:listCriteriosAmeaca
                                             ,listIucnAmeaca:listIucnAmeaca
                                             ,iucnAmeacaApoio:iucnAmeacaApoio]);
    }

    def save(){

        DadosApoio dadosApoio
        if( params.operacao == 'ameaca.edit')
        {
            dadosApoio = DadosApoio.get(params.id);
        }
        else if( params.operacao == 'ameaca.add')
        {
            dadosApoio = new DadosApoio()
            if( ! params.id ) {
                dadosApoio.pai = dadosApoioService.getByCodigo('TB_CRITERIO_AMEACA_IUCN')
            } else {
                dadosApoio.pai = DadosApoio.get( params.id)
            }
        }
        String codigoSistema = params.codigoSistema ?:null
        if( codigoSistema ){
            codigoSistema = Util.calcularCodigoSistema( codigoSistema )
        } else {
            codigoSistema = Util.calcularCodigoSistema( params.descricao )
        }
        dadosApoio.codigo             = params.codigo
        dadosApoio.codigoSistema      = codigoSistema
        dadosApoio.ordem              = params.ordem
        dadosApoio.descricao          = params.descricao
        dadosApoio.save( flush:true);

        // adicionar registro na tabela iucn_ameaca_apoio
        IucnAmeacaApoio iucnAmeacaApoio = IucnAmeacaApoio.findByAmeaca( dadosApoio )
        if( params.sqIucnAmeaca) {
            //ameaca.iucnUso = IucnUso.get( params.sqIucnUso.toLong() )
            IucnAmeaca iucnAmeaca = IucnAmeaca.get( params.sqIucnAmeaca )
            if( ! iucnAmeacaApoio ){
                iucnAmeacaApoio = new IucnAmeacaApoio()
                iucnAmeacaApoio.ameaca =  dadosApoio
            }
            iucnAmeacaApoio.iucnAmeaca = iucnAmeaca
            iucnAmeacaApoio.save( flush:true )
        } else if( iucnAmeacaApoio )  {
            iucnAmeacaApoio.delete(flush: true)
        }

        return this._returnAjax(0,null,null,[id:dadosApoio.id
                                             , codigoSistema:dadosApoio.codigoSistema
                                             , ordem:dadosApoio.ordem ])
    }

    def delete()
    {
        if( params.id)
        {
            DadosApoio  dadosApoio  = DadosApoio.get(params.id)
            if( dadosApoio ) {
                IucnAmeacaApoio iucnAmeacaApoio = IucnAmeacaApoio.findByAmeaca( dadosApoio )
                if( iucnAmeacaApoio ) {
                    iucnAmeacaApoio.delete(flush:true)
                }
                dadosApoio.delete( flush:true )
            }
            return this._returnAjax(0);
        }
        return this._returnAjax(1, 'Id não informado.', "error")
    }

}
