package br.gov.icmbio

import grails.converters.JSON

class FichaMultimidiaController extends BaseController {
    def dadosApoioService

    def index() {
        List listTipoMultimidia = dadosApoioService.getTable('TB_TIPO_MULTIMIDIA').sort{ it.ordem }
        List listSituacao       = dadosApoioService.getTable('TB_SITUACAO_MULTIMIDIA').sort{ it.ordem }
        render(template:"/ficha/multimidia/index",model:[
            listTipoMultimidia:listTipoMultimidia
            ,listSituacao:listSituacao
        ])
    }

    def save() {

        /*println params
        println "- " + 50
        return this._returnAjax(1, 'Teste parametros!', 'error')
        */

        FichaMultimidia fm
        FichaOcorrenciaMultimidia fom
        String mmDir = grailsApplication.config.multimidia.dir
        File savePath = new File(mmDir);
        if (!savePath.exists()) {
            if (!savePath.mkdirs()) {
                return this._returnAjax(1, 'Não foi possivel criar o diretório ' + savePath.asString(), "error");
            }
        }

        if( params.sqFichaOcorrenciaMultimidia )
        {
            fom = FichaOcorrenciaMultimidia.get( params.sqFichaOcorrenciaMultimidia.toLong() )
            params.sqFichaMultimidia = fom.fichaMultimidia.id
        }

        if( params.sqFichaMultimidia )
        {
            fm = FichaMultimidia.get( params.sqFichaMultimidia.toLong() )
        }
        else
        {
            fm = new FichaMultimidia()
            fm.ficha =  Ficha.get(params.sqFicha.toInteger() )
        }

        DadosApoio situacao = DadosApoio.get( params?.sqSituacao?.toInteger() )
        if( situacao.codigoSistema == 'REPROVADA' && !params.txNaoAceito ) {
            return this._returnAjax(1, 'Necessário informar o motivo da reprovação!', "error")
        }

        def f = request.getFile('fldAnexo')
        if (f && !f.empty)
        {
            String fileName = f.getOriginalFilename()
            String fileExtension = Util.getFileExtension(fileName)
            String fileNameMd5 = f.inputStream.text.toString().encodeAsMD5() +'.'+fileExtension
            fileName = savePath.absolutePath + '/' + fileNameMd5
            if (!new File(fileName).exists()) {
                f.transferTo(new File(fileName) )
            }

            if( FichaMultimidia.findByFichaAndNoArquivoDiscoAndIdNotEqual( fm.ficha, fileNameMd5, fm.id?:0 ) )
            {
                return this._returnAjax(1, 'Arquivo já cadastrado!', 'error')
            }
            fm.noArquivoDisco=fileNameMd5
            fm.noArquivo = f.getOriginalFilename()
        }
        else if( !params.sqFichaMultimidia )
        {
            return this._returnAjax(1, 'Nenhum arquivo selecionado!', 'error')
        }
        fm.tipo = DadosApoio.get( params?.sqTipo?.toInteger() )
        fm.situacao = situacao
        if( params.dtElaboracao ) {
            fm.dtElaboracao = new Date().parse('dd/MM/yyyy', params.dtElaboracao)
        } else {
            fm.dtElaboracao = null
        }
        // somente imagens jpeg, png ou bmp podem ser destaque ou principal
        if( params.noArquivo =~ /\.(jpe?g|png|bmp)|$/) {
            fm.inPrincipal = (params.inPrincipal == 'S')
            fm.inDestaque = (params.inDestaque == 'S')
        } else {
            fm.inPrincipal = false
            fm.inDestaque = false
        }
        fm.noAutor = params.noAutor
        fm.deEmailAutor = params.deEmailAutor
        fm.deLegenda = params.deLegenda?:null
        fm.txMultimidia = params.txMultimidia?:null
        fm.txNaoAceito = params.txNaoAceito?:null
        if( fm.situacao.codigoSistema =='APROVADA' && !fm.aprovadoPor )
        {
            fm.aprovadoPor = Pessoa.get( session.sicae.user.sqPessoa.toLong() )
            fm.dtAprovacao = new Date()
            fm.reprovadoPor= null
            fm.dtReprovacao= null
        }

        if( fm.situacao.codigoSistema =='REPROVADA' && !fm.reprovadoPor )
        {
            fm.reprovadoPor = Pessoa.get( session.sicae.user.sqPessoa.toLong() )
            fm.dtReprovacao = new Date()
            fm.aprovadoPor = null
            fm.dtAprovacao = null
        }
        fm.validate()
        if( fm.hasErrors() )
        {
            return this._returnAjax(1, 'Formulário possui erros!', 'erro');
        }

        // vaidar se a imagem está ok
        // gerar o thumnail e a imagem para o carousel
        String fileName = savePath.absolutePath + '/' + fm.noArquivoDisco
        File file = new File(fileName)
        if ( file.exists() ) {
            if( ! ImageService.createThumb( fileName ) ){
                return this._returnAjax(1, 'Imagem com problema. Edite-a e selve novamente em um dos formatos JPEG ou PNG.', 'erro');
            }
            if( fm.inDestaque ){
                if( !ImageService.createCarouselImage(fileName )){
                    return this._returnAjax(1, 'Imagem com problema. Edite-a e selve novamente em um dos formatos JPEG ou PNG.', 'erro');
                }
            }
        }
        fm.save( flush:true )

        // só pode haver uma principal e uma em destaque por taxon
        if( fm.inPrincipal || fm.inDestaque ) {
            FichaMultimidia.createCriteria().list{
                ficha {
                    eq('taxon', fm.ficha.taxon )
                }
                ne('id',fm.id)
            }.each{
                it.inPrincipal = false
                it.inDestaque = false
                it.save(flush:true)
            }
        }

        // gravar a tabela ficha ocorrencia multimidia quando a midia for ligada a um registro de ocorrencia
        if( params.sqFichaOcorrencia )
        {
            if (! fom )
            {
                fom = new FichaOcorrenciaMultimidia()
                fom.fichaOcorrencia = FichaOcorrencia.get( params.sqFichaOcorrencia.toLong() )
                fom.fichaMultimidia = fm
                fom.save( flush:true)
            }
        }

        // atualizar log da ficha, quem alterou
        fm.ficha.dtAlteracao = new Date()
        fm.ficha.save()
        return this._returnAjax(0,getMsg(0), 'success')
    }

    def getGrid() {
        if( !params.sqFicha )
        {
            render 'Id da ficha não informado!'
            return
        }

        Ficha fcha = Ficha.get( params.sqFicha.toInteger() )

        // Busca FichaConsultaFoto da ficha corrente
        List listFichaConsultaFoto = FichaConsultaFoto.createCriteria().list{
            fichaMultimidia {
                eq('ficha', fcha)
            }
        }

        // Fotos do Salve que não estão nas fotos dos colaboradores
        List listFichaMultimidia = FichaMultimidia.createCriteria().list{
            ficha {
                eq('taxon',fcha.taxon )
            }
            if(listFichaConsultaFoto?.fichaMultimidia?.id)
            {
                and { // com exceção das fotos colaboradas
                    not{ 'in'("id", listFichaConsultaFoto?.fichaMultimidia?.id) }
                }
            }
            order('dtElaboracao','desc')
        }



        render(template:'/ficha/multimidia/divGridMultimidia', model:[
            listFichaMultimidia    : listFichaMultimidia
            , listFichaConsultaFoto: listFichaConsultaFoto] )
    }

    def getGridMultimidiaOcorrencia()
    {
        if( !params.sqFichaOcorrencia )
        {
            render 'Id da ocorrência não informado!'
            return
        }
        List listFichaOcorrenciaMultimidia = FichaOcorrenciaMultimidia.createCriteria().list{
            fichaOcorrencia {
                eq('id',params.sqFichaOcorrencia.toLong() )
            }
            fichaMultimidia {
                order('dtElaboracao', 'desc')
            }
        }
        render(template:'/ficha/multimidia/divGridOcorrenciaMultimidia', model:[listFichaOcorrenciaMultimidia:listFichaOcorrenciaMultimidia] )
    }


    def getMultimidia() {

        if (!params.id) {
            response.sendError(404);
            return;
        }
        FichaMultimidia reg = FichaMultimidia.get(params.id);
        if (! reg ) {
            response.sendError(404);
            return;
        }
        String mmDir = grailsApplication.config.multimidia.dir
        String hashFileName = reg.noArquivoDisco
        Integer posicao = hashFileName.lastIndexOf('/')
        if( posicao > 0 )
        {
            hashFileName = hashFileName.substring(posicao+1)
            reg.noArquivoDisco = hashFileName
            reg.save(flush:true)
        }
        File file
        if( params.thumb )
        {
            file = new File(mmDir+'thumbnail.'+hashFileName);
            if ( file.exists() )
            {
                hashFileName = mmDir+'thumbnail.'+hashFileName
            }
            else
            {
                file=null
            }
        }
        if( ! file )
        {
            file = new File( mmDir+hashFileName )
        }
        if ( ! file.exists() )
        {
            response.sendError(400)
            return
        }
        String fileName = reg.noArquivo.replaceAll(' |,|\\(.+\\)', '_').replaceAll('_+','-').replaceAll('-\\.','.')
        String fileExtension = Util.getFileExtension(fileName)

        // fazer download com o nome da especie + autor
        fileName = reg.ficha.taxon.noCientifico + ( reg.noAutor? '_'+reg.noAutor : '') +'.'+fileExtension
        fileName = fileName.replaceAll(' |,|\\(.+\\)', '_').replaceAll('_+','-').replaceAll('-\\.','.')

        String fileType = 'application/octet-stream';
        if( reg.tipo.codigoSistema=='IMAGEM')
        {
            fileType = 'image/'+fileExtension
        }
        else if( reg.tipo.codigoSistema=='SOM')
        {
            fileType = 'audio/mp3'
        }
        if( fileType )
        {
            render(file: file, fileName: fileName, contentType: fileType)
        }
        else
        {
            render(file: file, fileName: fileName)
        }
    }

    def edit() {
        FichaMultimidia reg = FichaMultimidia.get( params.id.toInteger())
        if( reg?.id )
        {
            render reg.asJson()
            return
        }
        render [:] as JSON
    }

    def delete()
    {
        try {

            if( params.sqFichaOcorrenciaMultimidia )
            {
                FichaOcorrenciaMultimidia.get( params.sqFichaOcorrenciaMultimidia.toLong() ).delete( flush:true )
            }
            FichaMultimidia reg = FichaMultimidia.get( params.id.toInteger())
            if( reg )
            {
                String noArquivoDisco = reg.noArquivoDisco
                Ficha ficha = reg.ficha
                reg.delete( flush:true )
                if( FichaMultimidia.countByNoArquivoDisco( noArquivoDisco ) == 0 )
                {
                    String mmDir = grailsApplication.config.multimidia.dir
                    String fileName = mmDir + noArquivoDisco
                    String fileNameThumb = mmDir + 'thumbnail.'+noArquivoDisco
                    new File( fileName).delete()
                    new File( fileNameThumb).delete()
                }
                // atualizar log da ficha, quem alterou
                ficha.dtAlteracao = new Date()
                ficha.save()
                render ''
            }
            else
            {
                render 'Id  Inválido'
            }
        } catch(Exception e ) {
            render e.getMessage();
        }
    }

    /**
     * marcar/desmarcar a foto como principal para impressão no pdf
     * @return
     */
    def toggleImagemPrincipal(){
        try {
            if ( params.sqFichaMultimidia ) {
                FichaMultimidia fichaMultimidia = FichaMultimidia.get(params.sqFichaMultimidia.toLong() )
                if (!fichaMultimidia ) {
                    throw new Exception('Id inválido')
                }
                boolean isImg = (fichaMultimidia.noArquivo =~ /\.(jpe?g|png|bmp)|$/)
                if( isImg ) {
                    FichaMultimidia.createCriteria().list {
                        ficha {
                            eq('taxon', Ficha.get(params.sqFicha.toInteger()).taxon)
                        }
                    }.each { row ->
                        row.inPrincipal = row.id.toLong() == params.sqFichaMultimidia.toLong() && params.checked == 'S'
                        row.save()

                        fichaMultimidia.ficha.dtAlteracao = new Date()
                        fichaMultimidia.ficha.save()
                        String mmDir = grailsApplication.config.multimidia.dir
                        File savePath = new File(mmDir);
                        String fileName = savePath.absolutePath + '/' + row.noArquivoDisco
                        File file = new File(fileName)
                        if (file.exists()) {
                            try{ ImageService.createThumb(fileName) }catch( Exception e ){}
                            if (row.inDestaque) {
                                try{ ImageService.createCarouselImage(fileName) }catch( Exception e ){}
                            }
                        }
                    }
                }
            }
        } catch( Exception e ){
            println e.getMessage()
            return this._returnAjax(1, e.getMessage(), 'error')
        }
        return this._returnAjax(0, '', 'success')
    }

    /**
     * marcar/desmarcar a foto em destaque para exibir no carrossel do modulo publico
     * @return
     */
    def toggleImagemDestaque(){
        try {
            if ( params.sqFichaMultimidia ) {
                FichaMultimidia fichaMultimidia = FichaMultimidia.get(params.sqFichaMultimidia.toLong() )
                if (!fichaMultimidia ) {
                    throw new Exception('Id inválido')
                }
                boolean isImg = (fichaMultimidia.noArquivo =~ /\.(jpe?g|png|bmp)|$/)
                if( isImg ){
                    FichaMultimidia.createCriteria().list{
                        ficha {
                            eq('taxon',Ficha.get( params.sqFicha.toInteger() ).taxon )
                        }
                    }.each{ row->
                        row.inDestaque = row.id.toLong() == params.sqFichaMultimidia.toLong() && params.checked == 'S'
                        row.save()
                    }
                }
                fichaMultimidia.ficha.dtAlteracao = new Date()
                fichaMultimidia.ficha.save()
            }
        } catch( Exception e ){
            println e.getMessage()
            return this._returnAjax(1, e.getMessage(), 'error')
        }
        return this._returnAjax(0, '', 'success')
    }
}
