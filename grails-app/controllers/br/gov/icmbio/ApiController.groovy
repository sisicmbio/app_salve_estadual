package br.gov.icmbio

import com.vividsolutions.jts.geom.Coordinate
import com.vividsolutions.jts.geom.Geometry
import com.vividsolutions.jts.geom.GeometryFactory
import grails.converters.JSON
import grails.util.Environment

class ApiController {
    def dadosApoioService
    def emailService
    def utilityService
    def requestService
    def publicacaoService
    def assinaturaEletronicaService
    def sqlService
    def mapService
    def fichaService


    def index() {
        render(view: 'index', model: [:])
    }

    private boolean isContextoPublico=false // restringir a exibição de pontos no mapa de acordo com o contexto Publico ou Salve

    private boolean isDesenv() {
        return  ( Environment.current.toString() == 'DEVELOPMENT')
    }

    private boolean isLogged() {
        return  session?.sicae?.user?.sqPessoa ? true : false
    }

    private void enableCors( /*org.apache.catalina.connector.ResponseFacade response*/) {
        String domain = request.getServerName() ? request.getServerName() : request.getHeader("Origin")
        if( ! domain || domain =~ /\.gov\.br|localhost|10\.|192\./ ){
            response.setHeader('Access-Control-Allow-Origin', "*")
            response.setHeader('Access-Control-Allow-Methods', 'POST, PUT, GET, OPTIONS, PATCH')
            response.setHeader('Access-Control-Allow-Headers', 'X-Additional-Headers-Example')
            response.setHeader('Access-Control-Allow-Credentials', 'true')
            response.setHeader('Access-Control-Max-Age', '1728000')
        } else {
            Util.printLog('SALVE - ApiController.enableCors()', 'Acesso NAO permitido para: ' + domain, '')
            println 'GETHEADER: ' + request?.getHeader("Origin")
            println 'GETATTRIBUTE: ' + request.getAttribute('Origin')
            println 'GETCONTEXTPATH: ' + request.getContextPath()
            println 'GETPATHINFO: ' + request.getPathInfo()
            println 'GETREQUESTURI: ' + request.getRequestURI()
            println 'GETREQUESTURL: ' + request.getRequestURL()
            println 'GETPARTS: ' + request.getParts()
            println 'GETSERVERNAME: ' + request.getServerName()
            println '-' * 80
        }
    }

    /**
     * metodo para encriptar
     * @param value
     * @return
     */
    private String encryptStr( String value ){
        value = value ?: ''
        return AESCryption.encrypt( value, new Date().format('ddMMyyyy') )
        //return Util.encryptStr( value )
    }

    /**
     * metodo para desencriptar
     * @param value
     * @return
     */
    private String decryptStr( String value ){
        value = value ?: ''
        return AESCryption.decrypt(value, new Date().format('ddMMyyyy') )
    }

    /**
     * método utilizado pelas requisições de baixar arquivo
     * para ler o conteúdo do arquivo e enviar o conteudo (stream) para o browser
     * @param fileName
     */
    private void _downloadFile(String fileName = null, String attatchFileName = '' ) {
        String tempDir  = grailsApplication?.config?.temp?.dir ?: '/data/salve/temp/'
        if (!fileName) {
            throw new RuntimeException('Arquivo não informado')
        }
        if ( params.fileName ) {
            fileName = URLDecoder.decode(params.fileName, "UTF-8")
        }
        /** /
         println ' '
         println 'SALVE-ESTADUAL'
         println 'Download File - ' + new Date().format('dd/MM/yyyy HH:mm:ss')
         println 'Name...............: ' + fileName
         println 'TempDir............: ' + tempDir
         /**/
        File file = new File(fileName)
        if ( ! file.exists()) {
            file = new File(tempDir + fileName)
            if (!file.exists()) {
                throw new RuntimeException('Arquivo ' + fileName + ' inexistente')
            }
            fileName = tempDir + fileName
        }
        String fileExtension = Util.getFileExtension(fileName)
        String contentDisposition = "attachment;filename=${attatchFileName?:file.name}"
        String contentType
        switch (fileExtension) {
            case 'zip':
                contentType = 'application/zip'
                break;
            case 'json':
                contentType = 'application/json'
                break;
            case 'rtf':
                contentType = 'text/rtf'
                break;
            case 'pdf':
                contentType = 'application/pdf'
                break;
            case 'xlsx':
                contentType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet\n'
                break;
            default:
                if (fileName =~ /(?i)\.(log|out|xml|properties)$/) {
                    contentType = 'text/plain'
                    //contentDisposition = "filename=${file.name}"
                } else if (fileName =~ /(?i)\.(jpe?g|bmp|png|gif|tiff)$/) {
                    contentType = 'application/image/' + fileExtension
                    contentDisposition = "filename=${file.name}"
                } else {
                    //contentType = 'pplication/octet-stream'
                    render(status: 403, text: 'Tipo de arquivo não permitido')
                    return;
                }
                break;
        }

        /*
        println "Content-Type.......: " +contentType
        println "Content-Disposition: " + contentDisposition
        println "Content-Length.....: " + file.size().toString()
        println '-'*50
        */

        response.setHeader("Content-Type", contentType)
        response.setHeader("Content-Disposition", contentDisposition)
        response.setHeader("Content-Length", file.size().toString())
        response.setContentType(contentType)

        FileInputStream fileInputStream = new FileInputStream(file);
        byte[] buf = new byte[1024];
        int bytesread, bytesBuffered = 0;
        while ((bytesread = fileInputStream.read(buf)) > -1) {
            response.outputStream.write(buf, 0, bytesread);
            bytesBuffered += bytesread;
            if (bytesBuffered > 1024 * 1024) { //flush after 1MB
                bytesBuffered = 0
                response.outputStream.flush()
            }
        }
        if ( bytesBuffered > 0 || response.outputStream != null ) {
            response.outputStream.flush();
        }
    }

    /**
     * metodo para retornar o multipolygon da AOO da ficha
     * no formato geojson
     * @example: http://salve.publico.test:8080/salve-estadual/api/layer/aooFicha/10448
     * @param sqFicha
     */
    def aooFicha( Long sqFicha ) {
        //enableCors()
        //response.setHeader('content-type', 'application/geo+json')
        //FichaAoo fichaAoo = FichaAoo.findByFicha( Ficha.get( sqFicha ));
        Map result =  ["type": "FeatureCollection"
                   //, 'crs': ['type': 'name', 'properties': ['name': 'EPSG:4326']]
                   , "features": []
        ]

        String cmdSql = """SELECT ST_AsGeoJSON( ge_aoo,13,8) as geoJson from salve.ficha_aoo where sq_ficha = ${ sqFicha.toString() }"""
        sqlService.execSql( cmdSql ).each { row ->
            if( row.geoJson ) {
                result.features.push( [ type: 'Feature'
                                       ,'properties': [
                                            'id'    :'calc-aoo' // define o style do desenho no mapa
                                            ,'name' :'AOO'
                                            ,'sqFicha':sqFicha.toString()
                                       ]
                                       ,geometry: JSON.parse( row.geoJson )
                ])
            }
        }
        //render teste as JSON
        render result as JSON
    }

   /**
     * metodo para retornar o multipolygon da EOO da ficha
     * no formato geojson
     * @example: http://salve.publico.test:8080/salve-estadual/api/layer/eooFicha/10448
     * @param sqFicha
     */
    def eooFicha( Long sqFicha ) {
        enableCors();
        Map result =  ["type": "FeatureCollection"
                   //, 'crs': ['type': 'name', 'properties': ['name': 'EPSG:4326']]
                   , "features": []
        ]
        String cmdSql = """SELECT ST_AsGeoJSON( ge_eoo,13,8) as geoJson, nu_hydroshed_level from salve.ficha_eoo where sq_ficha = ${ sqFicha.toString() }"""
            sqlService.execSql( cmdSql ).each { row ->
                if( row.geoJson ) {
                    result.features.push( [ type: 'Feature'
                                           ,'properties': [
                                                'id'    :'calc-eoo' // define o style do desenho no mapa
                                                ,'name' :'EOO'
                                                ,'nuHydroshedLevel':row.nu_hydroshed_level
                                                ,'sqFicha':sqFicha.toString()
                                           ]
                                           ,geometry: JSON.parse( row.geoJson )
                    ])
                }
            }
        render result as JSON
    }

    def validarDoc(){
        Map res=[status:'1', msg:'Código acesso inválido.',fileName:'']
        Boolean isPost = request.getMethod().toUpperCase() == 'POST'
        if( params.codigoAcesso && isPost )
        {
            if( ! params.token )
            {
                res.msg='Captcha não informado!'
                render res as JSON
                return
            }

            try {
                // dados.biodiversidade@gmail.com
                // https://www.google.com/recaptcha/
                if( isPost ) {
                    String secretKey = "6Lc76qQUAAAAAKUDWLXTZEAmsIuAyjf-5rrJTvf0"
                    //String siteKey = "6Lc76qQUAAAAAFWXugs0BTxi5pLvPS2MbhQZsZYy"
                    String urlCaptcha = "https://www.google.com/recaptcha/api/siteverify"
                    URL url = new URL(urlCaptcha + '?secret=' + secretKey + '&response=' + params.token)
                    def dadosJson = JSON.parse(url.getText())
                    if (!dadosJson.success) {
                        res.msg = 'Token inválido!'
                        render res as JSON
                        return
                    }
                    if (!dadosJson.score || dadosJson.score.toFloat() < 0.5f) {
                        res.msg = 'Página está protegida contra robôs!'
                        render res as JSON
                        return
                    }
                }
            } catch( Exception e )
            {
                res.msg = e.getMessage()
                render res as JSON
                return;
            }

            if( params.codigoAcesso.toString().size() > 5 ) {
                // procurar o anexo iniciado com o codigo acesso enviado
                OficinaAnexo anexo = OficinaAnexo.createCriteria().get {
                    maxResults(1)
                    ilike('deLocalArquivo', (params.codigoAcesso + '%').toString().toLowerCase())
                }
                if (anexo) {
                    res.fileName = anexo.deLocalArquivo
                    res.status = 0
                    res.msg = ''
                }
            }
            render res as JSON
        }
        else {
            render(view: 'validarDoc', model: [codigoAcesso : params.codigoAcesso ?: ''])
        }
    }

    /**
     * Método para assinatura eletrônica do documento final da oficina
     *
     */
    def assinarDocFinalOficina() {

        String dirAnexos = (grailsApplication.config.anexos.dir+'/').replaceAll(/anexos\/\//,'anexos/')
        String mensagem = '';

        /**
         * se passoui o parametro assinar=S fazer a assinatura e retornar
         */
        if( params.assinar == 'S')
        {
            Map res=[status:0, msg:'']
            if( ! params.hash ) {
                res.status=1
                res.msg = "Dados inválidos para assinatura eletrônica!"
            }
            try {
                if( assinaturaEletronicaService.assinarRelatorioFinal( params.hash ) ) {
                    res.status = 0
                    res.msg = 'Documento assinado com SUCESSO!'

                }
            } catch( Exception e )
            {
                res.status=1
                res.msg = e.getMessage()
            }
            render res as JSON
            return
        }

        OficinaAnexoAssinatura reg
        if( !params.hash )
        {
            mensagem = "Dados inválidos para assinatura eletrônica!"
        }
        else {
            // verificar se o hash existe
            reg = OficinaAnexoAssinatura.findByDeHash(params.hash)
            if (!reg) {
                mensagem = '<h3>Hash informado está inválido!</h3><br>[' + params.hash + ']'
                params.hash = ''
            } else {
                String fileName = dirAnexos + reg.oficinaAnexo.deLocalArquivo
                File file = new File(fileName)
                if (!file.exists()) {
                    mensagem = 'Arquivo ' + reg.oficinaAnexo.noArquivo + ' não encontrado!'
                    params.hash = ''
                }
            }
        }
        render ( view:'assinarDocFinalOficina', model:[ mensagem:mensagem
                                                        , reg:reg
                                                        , hash:params?.hash
                                                        , fileName:reg?.oficinaAnexo?.deLocalArquivo] )
    }

    def shapefile() {
        enableCors()
        String path
        if (!params.name) {
            render ''
        }
        // se não tiver ponto no nome retornar o arquivo padrão salve xxxxx.shp.zip do shapefile
        if( params.name.indexOf('.') == -1 ) {
            path = "/data/salve-estadual/arquivos/shapefiles/"+params.name+".shp.zip"
        }
        else {
            path = "/data/salve-estadual/arquivos/shapefiles/"+params.name
        }
        File f = new File(path)
        if( ! f.exists() ) {
            path = "/data/salve-estadual/arquivos/shapefiles/" + params.name.replaceAll(/\.[a-z]{1,}$/,'') + '/'+params.name
            f = new File(path)
            // se o arquivo cfg não existir criar um vazio para evitar erro
            if( ! f.exists() && ( path =~ /\.cpg$/) ){
                f.write('')
            }
        }
        if( f.exists() ) {
            response.contentType = 'application/octet-stream'
            response.setHeader("Content-Disposition", 'inline')
            response.setHeader("Content-Length", f.size().toString())
            int bytesread , bytesBuffered;
            byte[] buf
            try {
                FileInputStream fileInputStream  = new FileInputStream(f);
                buf=new byte[1024]
                while( (bytesread = fileInputStream.read( buf )) > -1 ) {
                    response.outputStream.write( buf, 0, bytesread );
                    bytesBuffered += bytesread;
                    if (bytesBuffered > (1024*1024) ) { //flush after 1MB
                        bytesBuffered = 0
                        response.outputStream.flush()
                    }
                }
            } catch( Exception e ){
                println ' '
                println 'SALVE - ERRO - apiController.shapefile - ' + new Date().format('dd/MM/yyyy HH:mm:ss')
                println 'Arquivo: ' + path
                println e.getMessage()
            }
            finally {

                if ( bytesBuffered && bytesBuffered > 0 && response.outputStream != null ) {
                   try{ response.outputStream.flush();} catch( Exception err ){}
                }
            }
        } else {
            println 'SALVE - apiController().shapefile NAO ENCONTRADO ' + new Date().format('dd/MM/yyyy HH:mm:ss')
            println 'Arquivo:'+path
            render ''
        }
    }

    def respostaConvite() {
        String template
        String msg
        String codigoResposta
        DadosApoio novaSituacao
        if( params.resposta && params.hash )
        {
            CicloAvaliacaoValidador cav = CicloAvaliacaoValidador.findByDeHash( params.hash )
            if( cav )
            {
                if( cav.dtValidadeConvite > Util.hoje() )
                {
                    if (params.resposta.toUpperCase() == 'S')
                    {
                        codigoResposta = 'CONVITE_ACEITO'
                    } else if (params.resposta.toUpperCase() == 'N')
                    {
                        codigoResposta = 'CONVITE_RECUSADO'
                    }
                    novaSituacao = dadosApoioService.getByCodigo('TB_SITUACAO_CONVITE_PESSOA', codigoResposta)
                    if (novaSituacao)
                    {
                        cav.situacaoConvite = novaSituacao
                        cav.dtResposta = new Date()
                        cav.save(flush: true)
                        msg = 'Sua resposta foi recebida com SUCESSO!'
                        String emailSalve = grailsApplication.config.app.email
                        // enviar email para salve
                        emailService.sendTo(emailSalve
                                ,'Resposta convite validador'
                                ,'<h3>Validador ' + cav.validador.noPessoa+' '+( params.resposta.toUpperCase()=='S'?'aceitou':'recusou')+' o convite!</h3>',emailSalve)
                       novaSituacao = null
                       if( codigoResposta == 'CONVITE_ACEITO')
                        {
                            novaSituacao = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'EM_VALIDACAO')
                            msg +='<p style="color:#00a900">Você ACEITOU o convite!</p>'
                        }
                        else {
                           msg +='<p style="color:#f00">Você NÃO ACEITOU o convite!</p>'
                           // se todos os validadores recusarem o convite voltar a situação da ficha para AVALIADA/REVISADA
                           DadosApoio conviteAceito = dadosApoioService.getByCodigo('TB_SITUACAO_CONVITE_PESSOA', 'CONVITE_ACEITO')
                           VwFicha ficha = cav.validadorFicha[0].oficinaFicha.vwFicha
                           Integer qtd = ValidadorFicha.createCriteria().count() {
                               oficinaFicha {
                                   eq('vwFicha.id',ficha.id)
                               }
                               cicloAvaliacaoValidador{
                                   eq('situacaoConvite',conviteAceito)
                               }
                           }
                           if( qtd == 0 )
                           {
                               novaSituacao = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'AVALIADA_REVISADA')
                           }
                       }
                       if( novaSituacao != null )
                       {
                           cav.validadorFicha.each{
                               it.oficinaFicha.ficha.situacaoFicha = novaSituacao
                               it.oficinaFicha.ficha.save( flush:true)
                           }
                       }
                    } else
                    {
                        msg = 'Situação ' + codigoResposta + ' não cadastrada!'
                    }
                }
                else {
                    msg="Prazo de validade do convite venceu em "+cav.dtValidadeConvite.format('dd/MM/yyyy')+'.'
                }
            }
            else
            {
                msg="Erro ao processar solicitação<br><br>Hash:"+params.hash+'<br>Resposta:'+params.resposta
             }
        }
        render ( template:'respostaConviteValidador', model:[msg:msg] )
    }

    /**
     * Retornar o json ou o link para download da ficha em pdf, rtf ou json
     * @example: http://localhost:8080/salve-estadual/api/fichaEspecie/Aburria cujubi
     * @return
     */
    def fichaEspecie(){
        enableCors()
        Map mapFicha = [status: 400, msg: 'Serviço ainda não disponível!']
        String tempDir = grailsApplication.config.temp.dir ?: '/data/salve-estadual/temp/'
        params.formato = params.formato ?: 'PDF'
        try {
            Configuracao configuracao = Configuracao.findByEstadoIsNotNull()
            if( !configuracao ){
                throw new RuntimeException("Dados iniciais de configuração da aplicação não foi preenchido")
            }

            if ( ! params.noCientifico ) {
                throw new RuntimeException("Nome científico não informado")
            }
            if( params.noCientifico.split(' ').size() < 2 ){
                throw new RuntimeException("Nome científico inválido. Informe uma espécie ou uma subespécie")
            }

            // verificar se é espécie ou subespécie
            boolean isEspecie  = params.noCientifico.split(' ').size() == 2
            String noNivelTaxonomico = isEspecie ? 'especie' : 'subespecie'
            //NivelTaxonomico nivelTaxonomico = NivelTaxonomico.findByCoNivelTaxonomico( isEspecie ? 'especie' : 'subespecie' )

            // localizar o taxon pelo nome científico
            String cmdSql = """select ficha.sq_ficha, ficha_versao.sq_ficha_versao, ficha_versao.js_ficha
                        from taxonomia.taxon
                        inner join salve.ficha on ficha.sq_taxon = taxon.sq_taxon
                        inner join salve.ficha_versao on ficha_versao.sq_ficha = ficha.sq_ficha
                        where json_trilha->:nivelTaxonico->>'no_taxon'= :noCientifico
                        and ficha_versao.st_publico=true
                        and ficha_versao.st_publicada=true
                        and ficha.sq_ciclo_avaliacao = 2
                    """
            Map sqlParams =[noCientifico:"'"+params.noCientifico+"'", nivelTaxonico:"'"+noNivelTaxonomico+"'"]
            List rows = sqlService.execSql(cmdSql, sqlParams)
            if( ! rows ) {
                throw new RuntimeException("Nenhuma versão pública da ficha de " + params.noCientifico)
            }
            Integer sqFicha = rows[0].sq_ficha.toInteger()
            //Integer sqFicha=1
            String urlSalve = grailsApplication.config.url.sistema ?: 'http://localhost:8080/salve-estadual/'
            if( params.formato && params.formato.toString().toUpperCase() == 'PDF') {

                PdfFicha pdf = new PdfFicha(sqFicha, tempDir)
                // integração SYS/IUCN
                if (params?.language && params.language == 'en') {
                    pdf.setTranslateToEnglish(true)
                }
                pdf.setTitulo(configuracao.noInstituicao)
                pdf.setSubTitulo(grailsApplication.config.app.title)
                pdf.setLogo(configuracao.noArquivoBrasao ?: '')
                pdf.setRefBibs(fichaService.getFichaRefBibs(sqFicha.toLong()))
                pdf.setUfs(fichaService.getUfsNameByRegiaoText(sqFicha.toLong()))
                pdf.setBiomas(fichaService.getBiomasText(sqFicha.toLong()))
                pdf.setBacias(fichaService.getBaciasText(sqFicha.toLong()))
                pdf.setUcs(fichaService.getUcsRegistro(sqFicha.toLong()))
                pdf.setUnidade(configuracao.sgInstituicao)
                pdf.setCiclo('')
                pdf.setImprimirPendencias(false)
                mapFicha.fileName= pdf.run()
                return _downloadFile(mapFicha.fileName)
                //mapFicha.link=  urlSalve + 'download?fileName='+pdf.run()
            } else if( params.formato && params.formato =~ /(?i)RTF|DOC/) {
                RtfFicha rtf = new RtfFicha( sqFicha, tempDir)
                //rtf.setTitulo(configuracao.noInstituicao)
                //rtf.setSubTitulo(grailsApplication.config.app.title)
                //rtf.setLogo(configuracao.noArquivoBrasao?:'')
                rtf.setRefBibs(fichaService.getFichaRefBibs(sqFicha.toLong() ) )
                rtf.setUfs(fichaService.getUfsNameByRegiaoText( sqFicha.toLong() ) )
                rtf.setBiomas(fichaService.getBiomasText( sqFicha.toLong() ))
                rtf.setBacias(fichaService.getBaciasText( sqFicha.toLong() ))
                rtf.setUcs(fichaService.getUcsRegistro(sqFicha.toLong() ) )
                rtf.setUnidade(configuracao.sgInstituicao)
                rtf.setCiclo('')
                mapFicha.fileName= rtf.run()
                return _downloadFile(mapFicha.fileName)
                //mapFicha.link=  urlSalve + 'download?fileName='+rtf.run()
            } else {
                mapFicha.jsonFicha = rows[0].js_ficha
            }
            mapFicha.msg=''
            mapFicha.status=200
        } catch ( Exception e ) {
            mapFicha.msg = e.getMessage()
            println e.getMessage()
        }
        render mapFicha as JSON
    }

    /**
     * retornar a ficha em pdf via sq_ficha em formato de hash
     * @example: http://localhost:8080/salve-estadual/api/fichaHash/395455527765536a68504e5a4f324a716659696a31773d3d
     * @return
     */
    def fichaHash(){
        enableCors()
        //https://blog.bitexpert.de/blog/creating-a-grails-backend-for-an-angularjs-app/
        Boolean pdf = true
        Map mapFicha = [status: 1, msg: 'Serviço ainda não disponível!']
        Integer sq_ficha
        String noCientifico
        String msgError
        try {
            if (!params.hash) {
                throw new RuntimeException("O hash não foi informado!")
            }
            sq_ficha = Util.decryptStr(params.hash).toInteger()
            String sql ="""
                    SELECT nm_cientifico, co_nivel_taxonomico
                    FROM salve.vw_ficha
                    WHERE sq_ficha = :sq_ficha
                    """

            List row
            row = sqlService.execSql(sql, ['sq_ficha' : sq_ficha])
            if (!row) {
                msgError = 'Não foi possível encontrar a ficha: ' + sq_ficha
                throw new Exception(msgError)
            }

            noCientifico = Util.ncItalico(row.nm_cientifico[0].toString(), true, false)

            // verificar se é uma espécie ou subespecie
            // se tiver 2 nomes e uma espécie
            // se tiver 3 nomes e uma subespece
            boolean isEspecie  = noCientifico.split(' ').size() == 2
            NivelTaxonomico nivelTaxonomico = NivelTaxonomico.findByCoNivelTaxonomico( isEspecie ? 'ESPECIE' : 'SUBESPECIE' )

            DadosApoio situacaoPublicada = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA','PUBLICADA')
            if( !situacaoPublicada ) {
                throw new RuntimeException("Situação PUBLICADA não cadastrada.")
            }

            Ficha ficha
            // procurar a última publicação do taxon
            String cmd = """SELECT ficha.sq_ficha
                    FROM salve.ficha
                    INNER JOIN taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                    WHERE taxon.sq_nivel_taxonomico = ${ nivelTaxonomico.id }
                    AND ficha.sq_ficha = ${ sq_ficha }
                    -- ler a ultima ficha publicada do taxon
                    AND ficha.sq_ficha = (
                        SELECT ficha2.sq_ficha from salve.ficha ficha2
                        INNER JOIN salve.ciclo_avaliacao ON ciclo_avaliacao.sq_ciclo_avaliacao = ficha2.sq_ciclo_avaliacao
                        WHERE ficha2.sq_taxon = ficha.sq_taxon
                        ORDER BY ciclo_avaliacao.nu_ano DESC
                        LIMIT 1
                    )"""

            sqlService.execSql( cmd ).each{
                ficha = Ficha.get( it.sq_ficha.toLong() )
            }

            if ( ! ficha) {
                throw new RuntimeException("Ficha de " + noCientifico + " não encontrada.")
            }

            if ( ! isDesenv() && ! ficha.dtPublicacao ) {
                throw new RuntimeException("Ficha da espécie " + noCientifico + " não está publicada!")
            }
            if( pdf ) {
                try {
                    PdfFicha oPdf = new PdfFicha(ficha.id.toInteger(), grailsApplication.config.temp.dir.toString())
                    oPdf.unidade = 'SALVE WS'
                    oPdf.ciclo = ficha.cicloAvaliacao.deCicloAvaliacao.toString()
                    String fileName = oPdf.run()
                    render(file: new File(fileName), fileName: "ficha_especie_" + (noCientifico.replaceAll(/ /, '_')) + '.pdf')
                } catch( Exception e ) {
                    render e.getMessage()
                }
                return
            }
            mapFicha.msg=''
            mapFicha.status=0
        } catch ( Exception e ) {
            mapFicha.msg = e.getMessage()
        }
        render mapFicha as JSON
    }

    /**
     * Retorna o stream da imagem
     * @return
     */
    def imagem(){
        String path
        if( !params.sqFicha || !params.tipo )
        {
            render  ''
        }
        Ficha ficha = Ficha.get( params.sqFicha.toInteger() )
        if( ficha ) {
            if( params.tipo == 'mapa-distribuicao') {
                path = ficha.imagemPrincipalDistribuicao
            }
            String extensao = Util.getFileExtension(path)
            if ( path ) {
                File f = new File(path)
                if( f.exists() ) {
                    response.contentType = 'image/' + extensao
                    response.outputStream << f.getBytes()
                    response.outputStream.flush()
                    return
                }
            }
            render(status: 404, text: 'Imagem não cadastrada.')
            return
        }
        render(status: 404, text: 'Ficha inexistente.')
    }

    /**
     * Retornar o registros de ocorrências da base SALVE e Portalbio
     *
     * @param webUserId- id da sessão do usuário para controle da exibição dos registros em carência
     * @param sqFicha - id da ficha para selecionar os pontos
     * @param formato - formato de retorno sendo geojson, json
     * @param bd - nome do banco de dados. Ex: salve ou portalbio
     * @param cor - cor dos pontos no mapa
     * @param utilizadas - s/n para filtrar os registros que foram ou não utilizadas na avaliação
     * @param excluidas - s/n para filtrar os registros que foram marcados como excluídos na avaliação
     * @param layerId - id do layer
     * @exemple http://localhost:8090/salve-estadual/api/ocorrencias/2/geojson/salve-estadual/red/n
     * @return
     */
    def ocorrenciasModuloPublico( String webUserId, String sqFicha, String formato, String bd, String cor, String utilizadas, String excluidas, String layerId ) {
        try {
            Long id = decryptStr(params.sqFicha.replaceAll(' ', '+')).toLong();
            this.isContextoPublico=true // não mostrar dados sensiveis
            this.ocorrencias(webUserId, id, formato, bd, cor, utilizadas, excluidas, layerId)
            this.isContextoPublico=false
        } catch( Exception e ) {
            println e.getMessage();
            render [:] as JSON
        }
    }

    // http://salve.test:8080/salve-estadual/api/ocorrencias/7/34259/geojson/portalbio/%23FFFF00/s
    def ocorrencias( String webUserId, Long sqFicha, String formato, String bd, String cor, String utilizadas, String excluidas, String layerId ) {
        String cmdSql
        enableCors()

        //String origin = ( ! request.getHeader("Origin") ? '*' : request.getHeader("Origin") )
        //println 'ApiController.ocorrencias() origin: '+ origin

        boolean contextoExterno = false
        if ( ! isLogged() || this.isContextoPublico ) {
            contextoExterno = true
        }

        /** /
        println ' '
        println '------------ REQUISICAO DO MAPA -------------------- ' + new Date().format('dd/MM/yyyy HH:mm:ss')
        println 'Origin: ' + request.getHeader("Origin")
        println 'SqFicha: ' +sqFicha
        println 'Bd:'+bd
        println 'Cor:'+cor
        println 'Utilizadas:'+utilizadas
        println 'Excluidas:'+excluidas
        println 'LayerId:'+layerId
        println 'ContextoExterno:'+(contextoExterno ? 'SIM':'NAO')
        println '-'*100
        /**/

        // filtrar ficha excluidas para não entrar no resultado
        //DadosApoio situacaoExcluida = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA','EXCLUIDA')

        Date date1500 = new Date().parse("dd/MM/yyyy", '01/01/1500')
        Map result =[ ocorrencias:[], errors:[]]

        // definir valores padrão
        formato     = formato ?: 'geojson'
        bd          = bd?:''
        cor         = cor ?: utilityService.getRandomColor()
        utilizadas  = utilizadas?:'t'
        excluidas   = excluidas ?:'t'
        layerId     = layerId   ?:''
        params.webUserId = params.webUserId ? params.webUserId.toInteger() : 0i

        // padronizer case
        bd         = bd.toLowerCase()
        utilizadas = utilizadas.toUpperCase()
        excluidas  = excluidas.toUpperCase()

        /** /
        render 'Formato:' + formato + '<br>'
        render 'SqFicha:' + sqFicha.toString() + '<br>'
        render 'Bd:' + bd + '<br>'
        render 'Cor:' + cor + '<br>'
        render 'Utilizados:' + utilizadas + '<br>'
        render 'Excluidos:' + excluidas + '<br>'
        return
        /**/

        if( formato == 'geojson') {
            result =  ["type": "FeatureCollection"
                       , 'crs': ['type': 'name', 'properties': ['name': 'EPSG:4326']]
                       , "features": []
                       , "errors":[]
                       , "ocorrencias":[] // campo temporário no caso de features
                       ]
        }

        if( ! sqFicha )
        {
             result.errors.push('Id ficha não informado!')
             render result as JSON
             return
        }
        VwFicha ficha = VwFicha.get( sqFicha )
        if( !ficha )
        {
            result.errors.push('Id ficha '+sqFicha.toString() + ' inválido!')
            render result as JSON
            return
        }

        //--------------------------------------------------------------------------------
        // Ocorrencias do portalbio importadas para o salve
        // --------------------------------------------------------------------------------

        Map feature = [:]
        String noCientificoPortalbio=''
        if( !bd || bd =='portalbio') {
            List listWhere = ['a.sq_ficha='+sqFicha.toString()]
            listWhere.push('a.ge_ocorrencia is not null')
            if ( excluidas == 'S' || utilizadas == 'E') {
                listWhere.push("a.in_utilizado_avaliacao='E'")
            } else if (utilizadas == 'S') {
                listWhere.push("a.in_utilizado_avaliacao='${utilizadas.toUpperCase()}'")
            } else if (utilizadas == 'N') {
                listWhere.push("a.in_utilizado_avaliacao in ('N','E')")
            } else if (utilizadas == 'V') {
                listWhere.push("( a.in_utilizado_avaliacao is null or a.in_utilizado_avaliacao= 'R')")
            }

            // não exibir registros em carência
            if ( ! isLogged() || this.isContextoPublico || ! session.sicae.user.canViewRegsCarencia() ) {
                listWhere.push("( a.dt_carencia is null or a.dt_carencia < '${new Date().format('yyyy-MM-dd')}')")
            }

            cmdSql = """select a.sq_ficha_ocorrencia_portalbio
                    , a.no_instituicao
                    , ficha.nm_cientifico as no_cientifico
                    , case when ficha.nm_cientifico ilike concat(a.no_cientifico,'%') then null else a.no_cientifico end as no_cientifico_portalbio
                    , ( case when position('{' in tx_ocorrencia) < 1 then '{}' else tx_ocorrencia end ::jsonb)->>'codigoColecao' as co_colecao
                    , case when a.de_uuid ilike '%SISBIO%' then 'ICMBIO/SISBIO'
                         else concat('Portalbio/',coalesce( ( case when position('{' in tx_ocorrencia) < 1 then '{}' else tx_ocorrencia end ::jsonb)->>'codigoColecao',a.no_instituicao )::text ) END AS de_legenda
                    , a.no_local
                    , coalesce( estado.sg_estado, ( case when position('{' in tx_ocorrencia) < 1 then '{}' else tx_ocorrencia end ::jsonb)->>'estado'::text) AS sg_estado
                    , coalesce( municipio.no_municipio, ( case when position('{' in tx_ocorrencia) < 1 then '{}' else tx_ocorrencia end ::jsonb)->>'municipio'::text) AS no_municipio
                    , st_x( a.ge_ocorrencia) as nu_x
                    , st_y( a.ge_ocorrencia) as nu_y
                    , ( ( case when position('{' in tx_ocorrencia) < 1 then '{}' else tx_ocorrencia end ::jsonb)->>'precisaoCoord'::text) as de_precisao_coordenada
                    , a.no_autor
                    , coalesce(a.dt_ocorrencia,'1500-01-01') as dt_ocorrencia
                    , a.dt_carencia
                    , a.in_utilizado_avaliacao
                    , a.tx_nao_utilizado_avaliacao
                    , a.sq_motivo_nao_utilizado_avaliacao
                    , motivoNaoUtilizado.ds_dados_apoio as ds_motivo_nao_utilizado_avaliacao
                    , ( ( case when position('{' in tx_ocorrencia) < 1 then '{}' else tx_ocorrencia end ::jsonb)->>'recordNumber'::text) as de_record_number
                    , a.de_uuid
                    , a.de_ameaca
                    , 'N' as in_sensivel
                    , nivel.co_nivel_taxonomico
                    from salve.ficha_ocorrencia_portalbio a
                    inner join salve.ficha on ficha.sq_ficha = a.sq_ficha
                    inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                    inner join taxonomia.nivel_taxonomico nivel on nivel.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
                    left join salve.dados_apoio as motivoNaoUtilizado on motivoNaoUtilizado.sq_dados_apoio = a.sq_motivo_nao_utilizado_avaliacao
                    left join salve.estado estado on estado.sq_estado = a.sq_estado
                    left join salve.municipio municipio on municipio.sq_municipio = a.sq_municipio
                    where ${listWhere.join('\nand ')}
                    """ // dt_
            sqlService.execSql(cmdSql).each{row ->
                String legenda = 'PortalBio/' + row.no_instituicao
                String texto = "<div style=\"max-width:500px;\"><b>${row.de_legenda}</b>"
                texto += '<h4><b>' + Util.ncItalico(row.no_cientifico) + '</b></h4>' +
                    (row.no_cientifico_portalbio ? Util.ncItalico( row.no_cientifico_portalbio ) + ' <i class="fa fa-level-up" aria-hidden="true" title="Nome anterior"></i><br>' : '') +
                    (row.no_local ? 'Local:<b> ' + Util.capitalize( row.no_local ) + '</b><br>' : '') +
                    (row.sg_estado ? 'Estado:<b>' + row.sg_estado + '</b><br>' : '') +
                    (row.no_municipio ? 'Municipio:<b>' + row.no_municipio + '</b><br>' : '') +
                    'Lat: <b>' + row.nu_y.toString() + '</b>&nbsp;&nbsp;&nbsp;Lon: <b>' + row.nu_x.toString() + '</b>' +
                    (row.de_precisao_coordenada ? ' (' + row.de_precisao_coordenada + ')' : '') +
                    '<br>Responsável:<b> ' + Util.capitalize( row.no_autor ) +'</b>' +
                    (row.dt_ocorrencia && row.dt_ocorrencia != date1500 ? '<br>Data Ocorrência: <b>' + row.dt_ocorrencia.format("dd/MM/yyyy") : '') +'</b>'+
                    (row.dt_carencia ? '<br>Carência: <b>' + row.dt_carencia.format("dd/MM/yyyy") : '') +'</b>'+
                    '<br>' + formatUtilizadoAvaliacao(row.in_utilizado_avaliacao, 'portalbio', row.tx_nao_utilizado_avaliacao, row.ds_motivo_nao_utilizado_avaliacao).replaceAll(/<br\/?>/, '\n') +
                    (row.de_record_number ? '<br>' + row.de_record_number : '') +
                    '<span class="hidden">Id Origem: ' + (row.de_uuid ?: 'Não informado') + '</span></div>'

                if (formato == 'geojson') {
                    feature = createFeature('portalbio', row.sq_ficha_ocorrencia_portalbio.toString(), row.nu_x, row.nu_y, layerId);
                    feature.properties['text'] = texto
                    feature.properties['uuid'] = row.de_uuid
                    feature.properties['color'] = cor
                    feature.properties['legend'] = legenda
                    feature.properties['ameaca_s'] = row.de_ameaca
                    feature.properties['utilizadoAvaliacao'] = Util.snd( row.in_utilizado_avaliacao)
                    feature.properties['txNaoUtilizadoAvaliacao'] = (row.tx_nao_utilizado_avaliacao ?: '')
                    feature.properties['dsMotivoNaoUtilizadoAvaliacao'] = (row.ds_motivo_nao_utilizado_avaliacao ?: '')
                    feature.properties['sqMotivoNaoUtilizadoAvaliacao'] = (row.sq_motivo_nao_utilizado_avaliacao ?: '')
                    feature.properties['idOrigem'] = (row.de_uuid ?: 'Não informado')
                    feature.properties['isSubespecie'] = (row.co_nivel_taxonomico == 'SUBESPECIE')
                    feature.properties['isSensivel'] = (row.in_sensivel=='S')
                    result.features.push(feature)

                } else {
                    result.ocorrencias.push([bd                       : 'portalbio'
                                             , id                     : row.sq_ficha_ocorrencia_portalbio.toString()
                                             , x                      : row.nu_x
                                             , y                      : row.nu_y
                                             , text                   : texto
                                             , uuid                   : row.de_uuid
                                             , color                  : cor
                                             , legend                 : legenda
                                             , ameaca_s               : row.de_ameaca
                                             , utilizadoAvaliacao     : Util.snd(row.in_utilizado_avaliacao)
                                             , txNaoUtilizadoAvaliacao: row.tx_nao_utilizado_avaliacao ?: ''
                                             , dsMotivoNaoUtilizadoAvaliacao : row.ds_motivo_nao_utilizado_avaliacao ?: ''
                                             , sqMotivoNaoUtilizadoAvaliacao : row.sq_motivo_nao_utilizado_avaliacao ?: ''
                                             , idOrigem               : row.de_uuid ?: 'Não informado'
                                             , recordNumber           : row.de_record_number ?: ''
                                             , isSubespecie           : (row.co_nivel_taxonomico == 'SUBESPECIE')
                                             , isSensivel             : (row.in_sensivel == 'S')
                    ])
                }
            }

        }

        //--------------------------------------------------------------------------------
        // Ocorrências criadas no SALVE e SALVE-CONSULTA
        //--------------------------------------------------------------------------------

        if( !bd || bd =='salve') {
            List idsFichas=[]
            List listWhere=[]
            DadosApoio contextoOcorrencia = dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA', 'REGISTRO_OCORRENCIA')

            // condiçoes where
            if( this.isContextoPublico ){
                listWhere.push("a.in_sensivel='N'")
            }
            if (excluidas == 'S' || utilizadas=='E') {
                listWhere.push("a.in_utilizado_avaliacao='E'")
            }
            else if (utilizadas == 'S') {
                listWhere.push("(a.in_presenca_atual is null or a.in_presenca_atual <> 'N' )")
                listWhere.push("(a.in_utilizado_avaliacao is null or a.in_utilizado_avaliacao = 'S' or a.in_utilizado_avaliacao = '')")
            }
            else if ( utilizadas == 'SH' ) {
                listWhere.push("(a.in_presenca_atual = 'N' )")
                listWhere.push("(a.in_utilizado_avaliacao is null or a.in_utilizado_avaliacao = 'S' or a.in_utilizado_avaliacao = '')")
            }
            else if (utilizadas == 'N') {
                listWhere.push("a.in_utilizado_avaliacao='N'")
            }
            cmdSql = """WITH CTE AS ( select ficha.sq_ficha from salve.ficha
                    inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                    where ficha.sq_ciclo_avaliacao = ${ficha.sqCicloAvaliacao.toString()}
                    and ( taxon.sq_taxon_pai = ${ficha.sqTaxon.toString() } or ficha.sq_ficha = ${ficha.id.toString()})
                )
                SELECT motor.sq_ficha_ocorrencia,
                   motor.sq_taxon,
                   motor.sq_ficha,
                   motor.co_nivel_taxonomico,
                   motor.nm_cientifico,
                   motor.nu_x,
                   motor.nu_y,
                   motor.dt_carencia,
                   motor.ds_prazo_carencia,
                   estado.no_estado,
                   municipio.no_municipio,
                   ucFederal.sg_unidade_org sg_uc_federal,
                   ucEstadual.nome_uc1 as sg_uc_estadual,
                   ucRppn.nome as sg_rppn,
                   motor.in_sensivel,
                   salve.snd( motor.in_sensivel ) as ds_sensivel,
                   precisao.ds_dados_apoio as ds_precisao_coordenada,
                   tipoRegistro.ds_dados_apoio as ds_tipo_registro,
                   motor.in_presenca_atual,
                   salve.snd( motor.in_presenca_atual) as ds_presenca_atual,
                   refBib.json_ref_bib,
                   motor.nu_dia_registro,
                   motor.nu_mes_registro,
                   motor.nu_ano_registro,
                   motor.dt_inclusao,
                   pf.no_pessoa AS no_pessoa_inclusao,
                   motor.in_utilizado_avaliacao,
                   salve.snd( motor.in_utilizado_avaliacao) as ds_utilizado_avaliacao,
                   motor.tx_nao_utilizado_avaliacao,
                   motor.ds_motivo_nao_utilizado_avaliacao,
                   motor.sq_motivo_nao_utilizado_avaliacao,
                   motor.id_origem,
                   motor.in_observacao,
                   motor.tx_tombamento,
                   motor.tx_instituicao
            FROM
              (SELECT st_x(a.ge_ocorrencia) AS nu_x,
                      st_y(a.ge_ocorrencia) AS nu_y,
                      CASE
                          WHEN a.sq_prazo_carencia IS NOT NULL
                               AND prazo_carencia.cd_sistema <> 'SEM_CARENCIA'::text
                               AND a.dt_inclusao IS NOT NULL THEN CASE
                                                                      WHEN prazo_carencia.cd_sistema = '1_ANO'::text THEN a.dt_inclusao + '1 year'::interval
                                                                      ELSE CASE
                                                                               WHEN prazo_carencia.cd_sistema = '2_ANOS'::text THEN a.dt_inclusao + '2 years'::interval
                                                                               ELSE CASE
                                                                                        WHEN prazo_carencia.cd_sistema = '3_ANOS'::text THEN a.dt_inclusao + '3 years'::interval
                                                                                        ELSE NULL::TIMESTAMP WITHOUT TIME ZONE
                                                                                    END
                                                                           END
                                                                  END
                          ELSE NULL::TIMESTAMP WITHOUT TIME ZONE
                      END AS dt_carencia,
                      prazo_carencia.ds_dados_apoio as ds_prazo_carencia,
                      a.sq_estado,
                      a.sq_municipio,
                      a.sq_uc_federal,
                      a.sq_uc_estadual,
                      a.sq_rppn,
                      a.sq_precisao_coordenada,
                      a.sq_tipo_registro,
                      a.sq_pessoa_inclusao,
                      nivelt.co_nivel_taxonomico,
                      ficha.nm_cientifico,
                      a.in_sensivel,
                      a.in_presenca_atual,
                      a.nu_dia_registro,
                      a.nu_mes_registro,
                      a.nu_ano_registro,
                      a.dt_inclusao,
                      a.in_utilizado_avaliacao,
                      a.tx_nao_utilizado_avaliacao,
                      a.sq_motivo_nao_utilizado_avaliacao,
                      motivoNaoUtilizado.ds_dados_apoio  as ds_motivo_nao_utilizado_avaliacao,
                      a.id_origem,
                      a.sq_ficha_ocorrencia,
                      ficha.sq_taxon,
                      ficha.sq_ficha,
                      case when tx_observacao is null then false else true end as in_observacao,
                      a.tx_tombamento,
                      a.tx_instituicao
               FROM salve.ficha_ocorrencia a
               INNER JOIN CTE ON CTE.sq_ficha = a.sq_ficha
               INNER JOIN salve.ficha ON ficha.sq_ficha = a.sq_ficha
               INNER JOIN taxonomia.taxon ON taxon.sq_taxon = ficha.sq_taxon
               INNER JOIN taxonomia.nivel_taxonomico AS nivelt ON nivelt.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
               LEFT OUTER JOIN salve.dados_apoio AS prazo_carencia ON prazo_carencia.sq_dados_apoio = a.sq_prazo_carencia
               LEFT OUTER JOIN salve.dados_apoio as motivoNaoUtilizado on motivoNaoUtilizado.sq_dados_apoio = a.sq_motivo_nao_utilizado_avaliacao
               WHERE a.sq_contexto = ${contextoOcorrencia.id.toString() }
                 AND a.ge_ocorrencia IS NOT NULL
                 AND ( st_x(a.ge_ocorrencia) between -180 and 180 )
                 AND ( st_y(a.ge_ocorrencia) between -90 and 90 )
                 AND ${ listWhere.join('\nAND ')}
                 ) motor

            LEFT JOIN salve.estado estado ON estado.sq_estado = motor.sq_estado
            LEFT JOIN salve.municipio municipio ON municipio.sq_municipio = motor.sq_municipio
            left join salve.unidade_conservacao as ucFederal  on ucFederal.sq_pessoa=motor.sq_uc_federal
            left join salve.unidade_conservacao as ucEstadual on ucEstadual.gid = motor.sq_uc_estadual
            left join salve.unidade_conservacao as ucRppn     on ucRppn.ogc_fid = motor.sq_rppn
            left join salve.dados_apoio as precisao on precisao.sq_dados_apoio = motor.sq_precisao_coordenada
            left join salve.dados_apoio as tipoRegistro  on tipoRegistro.sq_dados_apoio = motor.sq_tipo_registro
            left join salve.pessoa_fisica pf on pf.sq_pessoa = motor.sq_pessoa_inclusao
            left join lateral (
                  --select refbib.de_titulo,ref_bib.no_autor, ref_bib.nu_ano_publicacao
                   select json_object_agg( x.sq_ficha_ref_bib,
                  json_build_object('de_titulo', coalesce( publicacao.de_titulo,'')
                  , 'no_autor', coalesce( publicacao.no_autor,x.no_autor)
                  , 'nu_ano', coalesce( publicacao.nu_ano_publicacao, x.nu_ano ) ) ) as json_ref_bib
                  from salve.ficha_ref_bib x
                  left join taxonomia.publicacao on publicacao.sq_publicacao = x.sq_publicacao
                  where x.sq_ficha = motor.sq_ficha
                  and x.no_tabela = 'ficha_ocorrencia'
                  and x.sq_registro = motor.sq_ficha_ocorrencia
            ) refBib on true"""
    // println ' '
    // println cmdSql
            sqlService.execSql(cmdSql).each {row ->

                // não exibir registros em carência
                Boolean emCarencia = false
                if ( row.dt_carencia && ( ! isLogged() || this.isContextoPublico || ! session.sicae.user.canViewRegsCarencia() ) ) {
                        if (row.dt_carencia > new Date()) {
                            emCarencia = true
                        }
                    }
                    String legenda          = 'Salve'
                    String texto            = '<b>SALVE</b><br>'
                    String estadoMunicipio  = ''
                    List ucs = []
                    if( row.no_estado ) {
                        estadoMunicipio += 'Estado: <b>' + row.no_estado + '</b>'
                    }
                    if( row.no_municipio ) {
                        estadoMunicipio += '<br>Município: <b>' + row.no_municipio + '</b>'
                    }
                    if( row.sg_uc_federal ) {
                        ucs.push( row.sg_uc_federal )
                    }
                    if( row.sg_uc_estadual ) {
                        ucs.push( row.sg_uc_estadual )
                    }
                    if( row.sg_rppn ) {
                        ucs.push( row.sg_rppn )
                    }
                    // referencia bibliografica
                    List refBibs = []
                    if( row.json_ref_bib ){
                        def dadosJson = JSON.parse( row.json_ref_bib.toString() )
                        //{ "643187" : {"de_titulo" : "Guia dos Sphingidae da Serra dos Órgãos, Sudeste do Brasil.", "no_autor" : "Martin, A.\nSoares, A.\nBizarro, J.", "nu_ano" : 2012} }
                        dadosJson.each{ key, value ->
                            String citacao = Util.getCitacao( value.no_autor, value.nu_ano )
                            if( ! refBibs.contains( citacao ) ) {
                                refBibs.push( citacao )
                            }
                        }
                    }

                    // dataHora
                    List dmy = []
                    if( row.nu_dia_registro ) {
                        dmy.push( row.nu_dia_registro.toString())
                    }
                    if( row.nu_mes_registro ) {
                        dmy.push( row.nu_mes_registro.toString())
                    }
                    if( row.nu_ano_registro ) {
                        dmy.push( row.nu_ano_registro.toString() )
                    }
                    String dataHora = dmy.join('/')

                    if ( ! emCarencia) {
                        if (row.co_nivel_taxonomico == 'SUBESPECIE') {
                            texto = '<b>SALVE (subespécie)</b><br>'
                        }

                        texto += '<h4> <b>' + Util.ncItalico(row.nm_cientifico) + '</b></h4>' +
                            estadoMunicipio +
                            (ucs ? '<br>UC: <b>' + ucs.join(', ') + '</b>' : '') +
                            '<br>Lat: <b>' + row.nu_y + '</b>&nbsp;&nbsp;&nbsp;Lon: <b>' + row.nu_x + '</b>' +
                            '<br>Sensível: <b>' + (row.in_sensivel == 'S' ? 'Sim' : 'Não') + '</b>' +
                            (row.ds_precisao_coordenada ? '<br>Precisão da coordenada: <b>' + row.ds_precisao_coordenada + '</b>' : '') +
                            (row.ds_tipo_registro ? '<br>Tipo de registro: <b>' + row.ds_tipo_registro + '</b>' : '') +
                            '<br><span class="nowrap">Presença atual na coordenada: <b>' + row.ds_presenca_atual + '</b></span>' +
                            (refBibs ? '<br>Ref. Bib: <b>' + refBibs.join(', ') + '</b>' : '') +
                            (row.tx_tombamento ? '<br>Nº tombamento: <b>' + row.tx_tombamento + '</b>' : '') +
                            (row.tx_instituicao ? '<br>Instituição tombamento: <b>' + row.tx_instituicao + '</b>' : '') +
                            (dataHora ? '<br>Registrado em: <b>' + dataHora + '</b>' : '') +
                            '<br>Incluído em: <b>' + (row.dt_inclusao ? row.dt_inclusao.format('dd/MM/yyyy HH:mm:ss') + '-' + row.no_pessoa_inclusao : '') + '</b>' +
                            '<br>Carência: <b>' + row.ds_prazo_carencia + (row.dt_carencia ? ' (' + row.dt_carencia.format('dd/MM/yyyy') + ')' : '') + '</b>' +
                            '<br>' + formatUtilizadoAvaliacao(row.in_utilizado_avaliacao, 'salve', row.tx_nao_utilizado_avaliacao, row.ds_motivo_nao_utilizado_avaliacao).replaceAll(/<br\/?>/, '\n') +
                            ( contextoExterno || ! row.in_observacao ? '' : '<br>Observação:<i data-table="ficha_ocorrencia" data-id="'+row.sq_ficha_ocorrencia+'" data-column="tx_observacao" data-title="Observação sobre o registro" onClick="app.showText(\'\',this,event)" class="fa fa-commenting-o" aria-hidden="true" style="cursor: pointer;"></i>')+
                            '<span class="hidden">Id Origem: ' + (row.id_origem ?: row.sq_ficha_ocorrencia) + '</span>'

                        if (formato == 'geojson') {

                            feature = createFeature('salve', row.sq_ficha_ocorrencia.toString(), row.nu_x, row.nu_y, layerId)
                            feature.properties['text'] = texto
                            feature.properties['uuid'] = ''// it.id.toString()
                            //cor = '#12b612'; // verde escuro
                            //cor = '#00ff00'; // verde

                            if (row.in_utilizado_avaliacao != 'N') {
                                if (row.in_presenca_atual == 'N') {
                                    feature.properties['color'] = cor;
                                    if (cor.indexOf('png') > 0) {
                                        String icon = '/markers/' + cor
                                        if (asset.assetPathExists(src: icon)) {
                                            feature.properties.icon = assetPath(src: icon)
                                        }
                                    }
                                } else {
                                    feature.properties['color'] = cor
                                    /*if( row.in_sensivel == 'S' ) {
                                        feature.properties['color'] = '#b5f7b5'
                                    }*/
                                }
                            } else {
                                feature.properties['color'] = cor
                            }
                            feature.properties['legend'] = legenda
                            feature.properties['ameaca_s'] = ''
                            feature.properties['utilizadoAvaliacao'] = row.ds_utilizado_avaliacao
                            feature.properties['txNaoUtilizadoAvaliacao'] = row.tx_nao_utilizado_avaliacao ?: ''
                            feature.properties['sqMotivoNaoUtilizadoAvaliacao'] = row.sq_motivo_nao_utilizado_avaliacao ?: ''
                            feature.properties['dsMotivoNaoUtilizadoAvaliacao'] = row.ds_motivo_nao_utilizado_avaliacao ?: ''
                            feature.properties['idOrigem'] = (row.id_origem ?: row.sq_ficha_ocorrencia)
                            feature.properties['isSubespecie'] = (row.co_nivel_taxonomico == 'SUBESPECIE' ? true : false)
                            feature.properties['isSensivel'] = (row.in_sensivel == 'S')
                            result.features.push(feature)
                        } else {

                            result.ocorrencias.push([bd                       : 'salve'
                                                     , id                     : row.sq_ficha_ocorrencia.toString()
                                                     , x                      : row.nu_x
                                                     , y                      : row.nu_y
                                                     , text                   : texto
                                                     , uuid                   : row.sq_ficha_ocorrencia.toString()
                                                     , color                  : cor
                                                     , legend                 : legenda
                                                     , ameaca_s               : ''
                                                     , utilizadoAvaliacao     : row.ds_utilizado_avaliacao
                                                     , txNaoUtilizadoAvaliacao: row.tx_nao_utilizado_avaliacao ?: ''
                                                     , dsMotivoNaoUtilizadoAvaliacao : row.ds_motivo_nao_utilizado_avaliacao ?: ''
                                                     , sqMotivoNaoUtilizadoAvaliacao : row.sq_motivo_nao_utilizado_avaliacao ?: ''
                                                     , idOrigem               : row.id_origem ?: row.sq_ficha_ocorrencia
                                                     , recordNumber           : ''
                                                     , isSubespecie           : (row.co_nivel_taxonomico == 'SUBESPECIE' ? true : false)
                                                     , isSensivel             : (row.in_sensivel == 'S')

                            ])
                        }
                    }
            }

            // ler as ocorrências das colaborações que foram aceitas na avaliação
            if (utilizadas == 'S' || utilizadas == 'N') {
                FichaOcorrenciaConsulta.createCriteria().list {
                    fichaOcorrencia {
                        eq('vwFicha.id', ficha.id.toLong())
                        eq('inUtilizadoAvaliacao', utilizadas)
                        isNotNull('geometry')
                        if( this.isContextoPublico ){
                            eq('inSensivel','N')
                        }
                    }
                }.each {

                    if (it.fichaOcorrencia.geometry && it.fichaOcorrencia.geometry.x <= 180 ) {

                        String legenda = 'Salve'
                        String texto = '<b>SALVE</b>&nbsp;(Salve-consulta)<br>'

                        String estadoMunicipio  = ''
                        if( it.fichaOcorrencia.estado ) {
                            estadoMunicipio += '<b>Estado</b>: '+it.fichaOcorrencia.estado.noEstado
                        }
                        if( it.fichaOcorrencia.municipio ) {
                            estadoMunicipio += '<br><b>Município</b>: '+it.fichaOcorrencia.municipio.noMunicipio
                        }

                        // não exibir registros em carência
                        Boolean emCarencia = false

                        if ( ( this.isContextoPublico || ! isLogged() || ! session.sicae.user.canViewRegsCarencia() ) && params?.webUserId?.toInteger() != it?.usuario?.id?.toInteger()) {
                            Date dtCarencia = it.fichaOcorrencia.carencia
                            if (dtCarencia && dtCarencia > new Date()) {
                                emCarencia = true
                            }
                        }
                        if ( ! emCarencia ) {
                            if (it.fichaOcorrencia.vwFicha.taxon.pai == ficha.taxon) {
                                texto = '<b>SALVE Consulta (subespécie)</b><br>'
                            }
                            texto += '<h4><b>' + Util.linkSearchPortalbio( it.fichaOcorrencia.vwFicha.nmCientifico ) + '</b></h4>' +
                                    estadoMunicipio+
                                    '<br>Lat: <b>' + it?.fichaOcorrencia?.geometry?.y + '</b>&nbsp;&nbsp;&nbsp;Lon:<b>' + it?.fichaOcorrencia?.geometry?.x +'</b>'+
                                    '<br>Sensível: <b>' + ( it.fichaOcorrencia.inSensivel == 'S' ? 'Sim' : 'Não') +'</b>'+
                                    (it.fichaOcorrencia.refBibHtml ? '<br>Ref. Bib: <b>' + Util.stripTags(it.fichaOcorrencia.refBibHtml)+'</b>' : '') +
                                    (it.fichaOcorrencia.precisaoCoordenada ? '<br>Precisão da coordenada: <b>' + it.fichaOcorrencia.precisaoCoordenada.descricao+'</b>' : '' ) +
                                    (it.fichaOcorrencia.tipoRegistro ? '<br>Tipo de registro: <b>' + it.fichaOcorrencia.tipoRegistro.descricao+'</b>' : '' ) +
                                    '<br><span class="nowrap">Presença Atual na Coordenada: <b>' + it.fichaOcorrencia.inPresencaAtualText + '</b></span>' +
                                    //'<br>Região:<span style="white-space:wrap;">' + it.fichaOcorrencia.noRegiao + '</span>' +
                                    //'<br>Registrado em:' + it.fichaOcorrencia.dataHora +
                                    (it.fichaOcorrencia.dataHora.toLowerCase() != 'desconhecida' ? '<br>Registrado em: <b>' + it.fichaOcorrencia.dataHora+'</b>' : '') +
                                    '<br>Incluído em:<b>' + (it.fichaOcorrencia.dtInclusao ? it.fichaOcorrencia.dtInclusao.format('dd/MM/yyyy') + '-' + it?.usuario?.noUsuario : '') +'</b>'+
                                    '<br>Carência:<b>' + it.fichaOcorrencia.dataFimCarencia +'</b>'+
                                    '<br>' + formatUtilizadoAvaliacao(it.fichaOcorrencia.inUtilizadoAvaliacao, 'salve-consulta', it.fichaOcorrencia.txNaoUtilizadoAvaliacao, it.fichaOcorrencia?.motivoNaoUtilizadoAvaliacao?.descricao).replaceAll(/<br\/?>/, '\n') +
                                    '<span class="hidden">id Origem: ' + (it.fichaOcorrencia.idOrigem ?: it.fichaOcorrencia.id) + '</span>'

                            if (formato == 'geojson') {

                                feature = createFeature('salve', it.fichaOcorrencia.id.toString(), it.fichaOcorrencia.geometry.x, it.fichaOcorrencia.geometry.y, layerId)
                                feature.properties['text'] = texto
                                feature.properties['uuid'] = it.fichaOcorrencia.id.toString()
                                feature.properties['color'] = cor
                                /*if( it.fichaOcorrencia.inUtilizadoAvaliacao ) {
                                    if( it.fichaOcorrencia.inPresencaAtual != 'S') {
                                        feature.properties['color'] =  cor
                                    } else {
                                        feature.properties['color'] = cor
                                    }
                                } else {
                                    feature.properties['color'] = cor
                                }*/
                                feature.properties['legend'] = legenda
                                feature.properties['ameaca_s'] = ''
                                feature.properties['utilizadoAvaliacao'] = it.fichaOcorrencia.inUtilizadoAvaliacaoText
                                feature.properties['txNaoUtilizadoAvaliacao'] = it.fichaOcorrencia.txNaoUtilizadoAvaliacao ?: ''
                                feature.properties['dsMotivoNaoUtilizadoAvaliacao'] = it.fichaOcorrencia?.motivoNaoUtilizadoAvaliacao?.descricao ?: ''
                                feature.properties['sqMotivoNaoUtilizadoAvaliacao'] = it.fichaOcorrencia?.motivoNaoUtilizadoAvaliacao?.id ?: ''
                                feature.properties['idOrigem'] = (it.fichaOcorrencia?.idOrigem ?: it.fichaOcorrencia.id)
                                feature.properties['isSubespecie'] = (it.fichaOcorrencia.vwFicha.taxon.nivelTaxonomico.coNivelTaxonomico.toUpperCase() == 'SUBESPECIE' ? true : false)
                                feature.properties['isSensivel'] = ( it.fichaOcorrencia.inSensivel != 'N' )
                                result.features.push(feature)
                            } else {
                                result.ocorrencias.push([bd                       : 'salve'
                                                         , id                     : it.fichaOcorrencia.id.toString()
                                                         , x                      : it.fichaOcorrencia.geometry.x
                                                         , y                      : it.fichaOcorrencia.geometry.y
                                                         , text                   : texto
                                                         , uuid                   : it.fichaOcorrencia.id.toString()
                                                         , color                  : cor
                                                         , legend                 : legenda
                                                         , ameaca_s               : ''
                                                         , utilizadoAvaliacao     : it.fichaOcorrencia.inUtilizadoAvaliacaoText
                                                         , txNaoUtilizadoAvaliacao: it.fichaOcorrencia.txNaoUtilizadoAvaliacao ?: ''
                                                         , dsMotivoNaoUtilizadoAvaliacao: it.fichaOcorrencia?.motivoNaoUtilizadoAvaliacao?.descricao ?: ''
                                                         , sqMotivoNaoUtilizadoAvaliacao: it.fichaOcorrencia?.motivoNaoUtilizadoAvaliacao?.id ?: ''
                                                         , idOrigem               : (it.fichaOcorrencia?.idOrigem ?: it.fichaOcorrencia.id)
                                                         , recordNumber           : ''
                                                         , isSubespecie           : (it.fichaOcorrencia.vwFicha.taxon.nivelTaxonomico.coNivelTaxonomico.toUpperCase() == 'SUBESPECIE' ? true : false)
                                                         , isSensivel             : (it.fichaOcorrencia.inSensivel == 'S')

                                ])
                            }
                        }
                    } else {
                        println ' '
                        println '-'*80
                        println 'Error'
                        println 'Coordenada cadastrada na consulta direta/ampla ou revisao pos-oficina'
                        println 'Tabela: ficha_ocorrencia_consulta invalida!'
                        println 'sq_ficha_ocrrencia_consulta:' + it.id
                        println 'sq_ficha_ocrrencia:' + it.fichaOcorrencia.id
                        println 'Lat(y): ' + it?.fichaOcorrencia?.geometry?.y
                        println 'Lon(x): ' + it?.fichaOcorrencia?.geometry?.x
                        println 'Localidade: ' + it.fichaOcorrencia?.noLocalidade
                        println 'Autor: ' + it?.usuario?.noUsuario
                        println '-'*80
                    }
                }
            }
            // fim each ficha salve
        }

        if( !bd || bd =='salve-consulta')  {
            DadosApoio naoAvaliada = dadosApoioService.getByCodigo('TB_SITUACAO_COLABORACAO', 'NAO_AVALIADA')
            DadosApoio resolverOficina = dadosApoioService.getByCodigo('TB_SITUACAO_COLABORACAO', 'RESOLVER_OFICINA')
            FichaOcorrenciaConsulta.createCriteria().list {
                fichaOcorrencia {
                    //ne('inUtilizadoAvaliacao', 'S')
                    eq('vwFicha.id', ficha.id.toLong())
                    'in'('situacao', [naoAvaliada,resolverOficina] )
                    isNotNull('geometry')
                }
            }.each {
                String legenda = 'Salve Consulta'
                String texto = '<b>SALVE Consulta</b><br>'
                String estadoMunicipio  = ''
                if( it.fichaOcorrencia.estado ) {
                    estadoMunicipio += '<b>Estado</b>: '+it.fichaOcorrencia.estado.noEstado
                }
                if( it.fichaOcorrencia.municipio ) {
                    estadoMunicipio += '<br><b>Município</b>: '+it.fichaOcorrencia.municipio.noMunicipio
                }

                // não exibir registros em carência
                Boolean emCarencia = false
                if( ( ! isLogged() || ! session.sicae.user.canViewRegsCarencia() ) && params?.webUserId?.toInteger() != it?.usuario?.id?.toInteger()  )
                {
                    Date dtCarencia = it.fichaOcorrencia.carencia
                    if( dtCarencia && dtCarencia > new Date() )
                    {
                        emCarencia = true
                    }
                }
                if( !emCarencia ) {
                    if (it.fichaOcorrencia.vwFicha.taxon.pai == ficha.taxon) {
                        texto = '<b>SALVE Consulta (subespécie)</b><br>'
                    }
                    texto += '<h4><b>'+Util.ncItalico( it.fichaOcorrencia.vwFicha.nmCientifico) + '</b></h4>'+
                            estadoMunicipio+
                            '<br>Lat: <b>'+it?.fichaOcorrencia?.geometry?.y+'</b>&nbsp;&nbsp;&nbsp;Lon: <b>'+it?.fichaOcorrencia?.geometry?.x+'</b>'+
                            (it.fichaOcorrencia.refBibHtml?'<br>Ref. Bib: <b>' + Util.stripTags( it.fichaOcorrencia.refBibHtml) +'</b>': '' ) +
                            (it.fichaOcorrencia.precisaoCoordenada ? '<br>Precisão da coordendada: <b>' + it.fichaOcorrencia.precisaoCoordenada.descricao+'</b>' : '' ) +
                            (it.fichaOcorrencia.tipoRegistro ? '<br>Tipo de registro: <b>' + it.fichaOcorrencia.tipoRegistro.descricao+'</b>' : '' ) +
                            '<br><span class="nowrap">Presença Atual na Coordenada: <b>' + it.fichaOcorrencia.inPresencaAtualText + '</b></span>' +
                            //'<br>Região:<span style="white-space:wrap;">' + it.fichaOcorrencia.noRegiao + '</span>' +
                            //'<br>Registrado em:' + it.fichaOcorrencia.dataHora +
                            (it.fichaOcorrencia.dataHora.toLowerCase() != 'desconhecida'? '<br>Registrado em: <b>' + it.fichaOcorrencia.dataHora+'</b>' :'') +
                            //'<br>Região:<span style="white-space:wrap;">' + it.fichaOcorrencia.noRegiao +'</span>' +
                            '<br>Incluído em: <b>' + (it.fichaOcorrencia.dtInclusao ? it.fichaOcorrencia.dtInclusao.format('dd/MM/yyyy') + '-' + it?.usuario?.noUsuario : '')+'</b>'+
                            '<br>Carência: <b>' + it.fichaOcorrencia.dataFimCarencia +'</b>'+
                            '<br>'+formatUtilizadoAvaliacao(it.fichaOcorrencia.inUtilizadoAvaliacao,'salve-consulta',it.fichaOcorrencia.txNaoUtilizadoAvaliacao,it.fichaOcorrencia?.motivoNaoUtilizadoAvaliacao?.descricao).replaceAll(/<br\/?>/,'\n')+
                            '<span class="hidden">id Origem: '+ ( it.fichaOcorrencia.idOrigem ?: it.fichaOcorrencia.id )+'</span>'

                    if (formato == 'geojson') {

                        feature = createFeature('salve', it.fichaOcorrencia.id.toString(), it.fichaOcorrencia.geometry.x, it.fichaOcorrencia.geometry.y, layerId)
                        feature.properties['text'] = texto
                        feature.properties['uuid'] = it.fichaOcorrencia.id.toString()
                        feature.properties['color'] = cor
                        feature.properties['legend'] = legenda
                        feature.properties['ameaca_s'] = ''
                        feature.properties['utilizadoAvaliacao'] = it.fichaOcorrencia.inUtilizadoAvaliacaoText
                        feature.properties['txNaoUtilizadoAvaliacao'] = it.fichaOcorrencia.txNaoUtilizadoAvaliacao ?: ''
                        feature.properties['dsMotivoNaoUtilizadoAvaliacao'] = it.fichaOcorrencia?.motivoNaoUtilizadoAvaliacao?.descricao ?: ''
                        feature.properties['sqMotivoNaoUtilizadoAvaliacao'] = it.fichaOcorrencia?.motivoNaoUtilizadoAvaliacao?.id ?: ''
                        feature.properties['idOrigem'] = ( it.fichaOcorrencia?.idOrigem ?: it.fichaOcorrencia.id )
                        feature.properties['isSubespecie'] = ( it.fichaOcorrencia.vwFicha.taxon.nivelTaxonomico.coNivelTaxonomico.toUpperCase() == 'SUBESPECIE' ? true:false )

                        result.features.push(feature)
                    } else {
                        result.ocorrencias.push([bd                       : 'salve'
                                                 , id                     : it.fichaOcorrencia.id.toString()
                                                 , x                      : it.fichaOcorrencia.geometry.x
                                                 , y                      : it.fichaOcorrencia.geometry.y
                                                 , text                   : texto
                                                 , uuid                   : it.fichaOcorrencia.id.toString()
                                                 , color                  : cor
                                                 , legend                 : legenda
                                                 , ameaca_s               : ''
                                                 , utilizadoAvaliacao     : it.fichaOcorrencia.inUtilizadoAvaliacaoText
                                                 , txNaoUtilizadoAvaliacao: it.fichaOcorrencia.txNaoUtilizadoAvaliacao ?: ''
                                                 , dsMotivoNaoUtilizadoAvaliacao: it.fichaOcorrencia?.motivoNaoUtilizadoAvaliacao?.descricao ?: ''
                                                 , sqMotivoNaoUtilizadoAvaliacao: it.fichaOcorrencia?.motivoNaoUtilizadoAvaliacao?.id ?: ''
                                                 , idOrigem               : ( it.fichaOcorrencia?.idOrigem ?: it.fichaOcorrencia.id )
                                                 , recordNumber           : jsonOcorrencia?.recordNumber ?:''
                                                 , isSubespecie           : ( it.fichaOcorrencia.vwFicha.taxon.nivelTaxonomico.coNivelTaxonomico.toUpperCase() == 'SUBESPECIE' ? true:false )

                        ])
                    }
                }
            }
        }

        // retorno final
        render result as JSON
    }

    /**
     * Método privado para gerar a estrutura básica de uma feature no formato geojson
     * @param bd - nome do banco de dados de origem da ocorrencia
     * @param id - id da ocorrência
     * @param x - coordenada X
     * @param y - coordeanda Y
     * @return
     */
    private Map createFeature( String bd, String id, Double x, Double y, layerId) {
        // padronizar as coordenadas com no máximo 8 casas decimais
        Map coords = Util.roundCoords(y,x)
        return  ["type"      : "Feature",
                   "id"        : id+bd.toLowerCase(), // id tem que concatenar com o bd para evitar duplicidade já que tem id de duas tabelas
                   "properties": [id:id, bd:bd, legendId:layerId],
                   "geometry"  : [
                      "type"       : "Point",
                      "coordinates": [coords.lonX,coords.latY]
                   ]
                ]
    }

    /**
     * formatar o texto utilizada/não utilizada/excluida no processo de avaliação
     * @param situacao
     * @param bd
     * @return
     */
    private String formatUtilizadoAvaliacao( String situacao, String bd, String txNaoUtilizado, String dsMotivoNaoUtilizadoAvaliacao = '')
    {
        String deUtilizadoOuNao
        String cor = '#258e25';
        situacao = situacao ? situacao.toString().toUpperCase() : ''

        if( situacao == 'E')
        {
            return '<span style="color:#ff0000">Não utilizado na avaliação (Excluído)</span>'
        }
        if( bd == 'salve')  {
            if( situacao == 'S' || !situacao || situacao == 'NULL' )
            {
                return '<span style="color:'+cor+'">Utilizado na avaliação</span>'
            }
            else {

                if (situacao == 'N') {
                    deUtilizadoOuNao = 'Não utilizado'
                    if( dsMotivoNaoUtilizadoAvaliacao ) {
                        deUtilizadoOuNao += ': ' + ( dsMotivoNaoUtilizadoAvaliacao?.toUpperCase() == 'OUTRO' ? '' : dsMotivoNaoUtilizadoAvaliacao )
                    } else {
                        deUtilizadoOuNao+= ' na avaliação'
                    }
                    // + ( ( dsMotivoNaoUtilizadoAvaliacao?.toUpperCase() == 'OUTRO') ? '' :dsMotivoNaoUtilizadoAvaliacao )
                } else {
                    txNaoUtilizado = ''
                    deUtilizadoOuNao = 'Não avaliado'
                }
                return '<span style="color:#FF0000">' + deUtilizadoOuNao + '</span>' +
                        (txNaoUtilizado ? '&nbsp;<i title="' + txNaoUtilizado + '" class="cursor-pointer glyphicon glyphicon-comment"></i>' : '')

            }

        }
        else if( bd == 'salve-consulta'){
            if (situacao == 'N') {
                deUtilizadoOuNao = 'Não utilizado'
                if( dsMotivoNaoUtilizadoAvaliacao ) {
                    deUtilizadoOuNao += ': ' + ( dsMotivoNaoUtilizadoAvaliacao?.toUpperCase() == 'OUTRO' ? '' : dsMotivoNaoUtilizadoAvaliacao )
                } else {
                    deUtilizadoOuNao+= ' na avaliação'
                }
                //+ ( (dsMotivoNaoUtilizadoAvaliacao?.toUpperCase() == 'OUTRO') ? '' :dsMotivoNaoUtilizadoAvaliacao )
                cor="#ff0000";
            } else {
                txNaoUtilizado = ''
                if( situacao =='S' ) {
                    deUtilizadoOuNao = 'Utilizado na avaliação'
                } else {
                    deUtilizadoOuNao = 'Não avaliado'
                    cor="#000000"
                }

            }
            return '<span style="color:'+cor+'">' + deUtilizadoOuNao + '</span>' +
                    (txNaoUtilizado ? '&nbsp;<i title="' + txNaoUtilizado + '" class="cursor-pointer glyphicon glyphicon-comment"></i>' : '')

        }
        else if( bd == 'portalbio') {
            if (situacao == 'S') {
                return '<span style="color:#258e25">Utilizado na avaliação</span>'
            } else {
                deUtilizadoOuNao = 'Não avaliado'
                if( situacao =='S' ) {
                    deUtilizadoOuNao = 'Utilizado na avaliação'
                    txNaoUtilizado = ''
                } else if( situacao =='N' ) {
                    deUtilizadoOuNao = 'Não utilizado'// + ( (dsMotivoNaoUtilizadoAvaliacao?.toUpperCase() == 'OUTRO') ? '' : dsMotivoNaoUtilizadoAvaliacao )
                    if( dsMotivoNaoUtilizadoAvaliacao ) {
                        deUtilizadoOuNao += ': ' + ( dsMotivoNaoUtilizadoAvaliacao?.toUpperCase() == 'OUTRO' ? '' : dsMotivoNaoUtilizadoAvaliacao )
                    } else {
                        deUtilizadoOuNao+= ' na avaliação'
                    }
                } else {
                    txNaoUtilizado = ''
                }
                return '<span style="color:#FF0000">'+deUtilizadoOuNao+'</span>' +
                        (txNaoUtilizado ? '&nbsp;<i title="' + txNaoUtilizado + '" class="cursor-pointer glyphicon glyphicon-comment"></i>' : '')
            }
        }
        return ''
    }


    /**
    * Exportar dados para portalbio
     */
    def exportOccurrencesDwca() {

        try {


            if (!params.sqCicloAvaliacao) {
                render '<h2>Ciclo avaliação não informado!</h2>'
                return
            }
            CicloAvaliacao cicloAvaliacao = CicloAvaliacao.get(params.sqCicloAvaliacao.toInteger())
            if (!cicloAvaliacao) {
                render '<h2>ID inválido!</h2>'
                return

            }

            // gerar arquivo csv e zipa-lo
            String path = grailsApplication.config.temp.dir
            String hora = new Date().format('dd_MM_yyyy_hh_mm_ss')
            String fileName = path + 'salve_dwca_' + hora + '.csv'

            /** /
            println ' '
            println 'Exportando ocorrencias SALVE para DWCA '+ new Date().format('dd/MM/yyyy HH:mm:ss')
            println 'Ciclo...: ' + cicloAvaliacao.deCicloAvaliacao
            println 'Arquivo.:' + fileName
            println "-"*100
            println ' '
            /**/


            DadosApoio contexto = dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA', 'REGISTRO_OCORRENCIA')
            List headers = ['"idOrigem"','"scientificName"','"originalNameUsage"','"decimalLatitude"','"decimalLongitude"','"accessRights"','"recordedBy"','"geodeticDatum"','"georeferenceRemarks"','"samplingProtocol"','"eventDate"','"locality"','"locationRemarks"','"country"','"stateProvince"','"municipality"','"minimumElevationInMeters"','"maximumElevationInMeters"','"minimumDepthInMeters"','"maximumDepthInMeters"','"habitat"','"identifiedBy"','"dateIdentified"','"identificationRemarks"','"otherCatalogNumbers"','"ownerInstitutionCode"','"catalogNumber"']
            def lines = [headers.join(';')] as List<String>
            lines += FichaOcorrencia.createCriteria().list {
                eq('contexto.id', contexto.id)
                vwFicha {
                    eq('sqCicloAvaliacao', cicloAvaliacao.id.toInteger() )
                }
            }.findAll {
                    // considerar somente os registrox com Referência bibliográfica
                    return it.refBibDWCA
                }.collect {
                    [
                            // idSalve
                            Util.quote(it.id.toString())

                            // scientificName
                            , Util.quote(it.ficha.taxon.noCientifico)

                            // originalNameUsage
                            , Util.quote(it.taxonCitado ? it.taxonCitado.noCientifico : '')

                            // decimalLatitude
                            , Util.quote(it.geometry.y.toString().replaceAll(/\./,','))

                            // decimalLongitude
                            , Util.quote(it.geometry.x.toString().replaceAll(/\./,','))

                            // accessRights
                            , Util.quote(it.carencia ? it.carencia.format('dd/MM/yyyy') : '' + (it.dtInclusao ? '|' + it.dtInclusao.format('dd/MM/yyyy') : ''))

                            // recordedBy
                            , Util.quote( it.refBibDWCA )

                            // geodeticDatum
                            , Util.quote(it.datum.descricao)

                            // georeferenceRemarks
                            , Util.quote((it.precisaoCoordenada ? 'Precisao da coordenada: ' + it.precisaoCoordenada.descricao : '') +
                            (it.refAproximacao ? '<br>Referência de aproximação: ' + it.refAproximacao.descricao : '') +
                            (it.metodoAproximacao ? '<br>Metodologia de aproximação: ' + it.metodoAproximacao.descricao : '') +
                            (it.txMetodoAproximacao ? '<br>' + it.txMetodoAproximacao : ''))

                            // samplingProtocol
                            , Util.quote(it?.tipoRegistro?.descricao ?: '')

                            // eventDate
                            , Util.quote(it.getDataHora())

                            // locality
                            , Util.quote((it.uc ? it.uc.sgUnidade : '') + (it?.noLocalidade ? '/' + it.noLocalidade : '') ?: '')

                            // locationRemarks
                            , Util.quote((it.txLocal ?: ''))

                            // country
                            , Util.quote((it.pais ? it.pais.noPais : ''))

                            // stateProvince
                            , Util.quote((it.estado ? it.estado.noEstado : ''))

                            // municipality
                            , Util.quote((it.municipio ? it.municipio.noMunicipio : '') )

                            // minimumElevationInMeters
                            , Util.quote(( it.vlElevacaoMin ? it.vlElevacaoMin.toString() : ''))

                            // maximumElevationInMeters
                            , Util.quote((it.vlElevacaoMax ? it.vlElevacaoMax.toString() : ''))

                            // minimumDepthInMeters
                            , Util.quote((it.vlProfundidadeMin ? it.vlProfundidadeMin.toString() : ''))

                            // maximumDepthInMeters
                            , Util.quote((it.vlProfundidadeMax ? it.vlProfundidadeMax.toString() : ''))

                            // habitat
                            , Util.quote((it.habitat ? it.habitat.descricao : ''))

                            // identifiedBy
                            , Util.quote((it.deIdentificador ?: ''))

                            // dateIdentified
                            , Util.quote((it.dtIdentificacao ? it.dtIdentificacao.format('dd/MM/yyyy') : ''))

                            // identificationRemarks
                            , Util.quote((it.qualificadorValTaxon ? it.qualificadorValTaxon.descricao : ''))

                            // otherCatalogNumbers
                            , Util.quote((it.txTombamento ?: ''))

                            // ownerInstitutionCode
                            , Util.quote((it.txInstituicao ?: ''))

                            // catalogNumber
                            , Util.quote( ( it.idOrigem ? it.idOrigem.toString() : '') )

                    ].join(';')
            } as List<String>


            File file = new File(fileName)
            lines.each { String line ->
                file.append("${line}\n")
            }
            render fileName
        }
        catch( Exception e) {
            println e.getMessage()
            render 'erro: ' + e.getMessage()
        }
    }


    /**
     * Pesquisar ocorrências no portalbio pelo nome científico utilizando biocacheService do portalbio
     * mas não atualizar o banco de dados
     * @param noCientifico
     * @return json
     */
    def searchPortalbio( String noCientifico ){
        enableCors()
        Map resultado = [data:new Date().format('dd/MM/yyyy HH:mm:ss'),total:0,ocorrencias:[]]
        Date date1500 = new Date().parse("dd/MM/yyyy", '01/01/1500')
        String urlBiocache = grailsApplication.config.url.biocache

        if( ! noCientifico )
        {
            render resultado as JSON
        }
        noCientifico = noCientifico.replaceAll(/%20/,' ')
        noCientifico = Util.ncItalico( noCientifico,true,false )
        String p = 'raw_taxon_name:"' + noCientifico + '"'

        // filtrar registros em carência
        if( ! isLogged()  ) {
            // TODO - testar filtro dos registros em carência se o usuário não etiver logado
            String currentDateTime = new Date().format("yyyy-MM-dd'T'HH:mm:ss'Z'")
            p += " and access_rights:[* TO ${currentDateTime}]"
        }
            /** /
            if ( session && ! session.envProduction ) {
                println ' '
                println ' '
                println 'ApiController/searchPortalbio em ' + new Date().format('dd/MM/yyyy HH:mm:ss')
                println 'Lendo ocorrencias do portalbio para ' + noCientifico
                println 'Url:' + urlBiocache
                println 'filtro: ' + p
                println '-' * 100
            }
            /**/

            def dados = requestService.doPost(urlBiocache, [q: p, pageSize: 10000, params: 'json'], "")
        try {

            if (!dados) {
                render resultado as JSON
                return
            }
            def tmpJson = JSON.parse(dados)
            Date dataOcorrencia
            Date dataCarencia
            String strData
            String eventDate
            String local   // raw_locality
            String autor
            List occurrences
            GeometryFactory gf = new GeometryFactory()
            if (tmpJson?.sr?.occurrences) {
                occurrences = tmpJson?.sr?.occurrences
            } else {
                occurrences = tmpJson.occurrences
            }
            //println 'Total de ' + occurrences.size() + ' ocorrencias no PortalBio.'
            occurrences.eachWithIndex { item, i ->

            if (item?.decimalLatitude
                        && item?.decimalLongitude
                        && item?.decimalLatitude.toString() != 'null'
                        && item?.decimalLongitude.toString() != 'null'
                        && item?.collector
                        && item?.collector.toString() != 'null'
                        && item?.decimalLatitude.toString().indexOf('.') > 0
                        && item?.decimalLatitude.toString().indexOf('.') < 4
                        && item?.decimalLatitude.toString().indexOf('E') == -1
                        && item?.uuid
                ) {
                    try {
                        eventDate = ''
                        autor = ''
                        // gravar os pontos do portalbio no salve
                        dataOcorrencia = null
                        try {
                            strData = item.verbatimEventDate
                            if (strData) {
                                try {
                                    strData = strData.split(' ')[0]
                                    if (strData.length() == 10) {
                                        strData = strData.replaceAll(/-/, '/')
                                        //println 'Verbatin data: '+strData
                                        dataOcorrencia = new Date().parse("dd/MM/yyyy", strData)
                                    }
                                } catch (Exception e) {
                                    dataOcorrencia = null
                                }
                            }
                            if (!dataOcorrencia && item.eventDate) {
                                //println 'Event data: '+ item.eventDate
                                dataOcorrencia = new Date(item.eventDate)
                            }
                        } catch (Exception e) {
                        }

                        dataOcorrencia = dataOcorrencia ?: date1500
                        if ( dataOcorrencia ) {
                            Geometry geometry = gf.createPoint(new Coordinate(item.decimalLongitude, item.decimalLatitude))
                            if (geometry) {
                                geometry.SRID = 4674
                                Map data = [:]
                                data.uuid           = item.uuid.toString().trim()
                                data.nomeCientifico = ''
                                data.autor          = item.collector.trim()
                                data.latitude       = geometry.y
                                data.longitude      = geometry.x
                                data.dataOcorrencia = ( dataOcorrencia == date1500 ? '' : dataOcorrencia.format("dd/MM/yyyy") )
                                data.carencia       = ''
                                data.codigoColecao  = ''
                                data.local          = ''
                                data.estado         = ''
                                data.municipio      = ''
                                data.fonte          = ''
                                data.precisaoCoord  = ''
                                data.recordNumber   = (item.recordNumber ? item.recordNumber.toString().trim():'') // neste campo tem a autorização do sisbio
                                try {
                                    if (item?.accessRights) {
                                        data.carencia = new Date().parse("yyyy/MM/dd", item.accessRights.replace(/-/, '/'))
                                    }
                                } catch (Exception e) {
                                }

                                if (dataCarencia) {
                                    data.carencia = dataCarencia.format('dd/MM/yyyy')
                                }

                                if (item?.raw_locality) {

                                    data.local = item.raw_locality
                                    //println 'Local ' + data.local
                                }

                                if (item.raw_collectionCode) {
                                    data.codigoColecao = item.raw_collectionCode
                                }

                                if (item.scientificName) {
                                    data.nomeCientifico = item.scientificName
                                }else if (item?.raw_scientificName) {
                                    data.nomeCientifico = item.raw_scientificName
                                }

                                if (item.stateProvince) {
                                    data.estado = item.stateProvince
                                }

                                if (item.dataResourceName) {
                                    data.fonte = item.dataResourceName
                                }

                                if (item.municipality) {
                                    data.municipio = Util.capitalize(item.municipality)
                                }
                                if (item.georeference_remarks) {
                                    data.precisaoCoord = item.georeference_remarks
                                }
                                data.instituicao = item.raw_institutionCode
                                data.ameaca = item.ameaca_s
                                resultado.ocorrencias.push(data)
                            }
                        }
                    }
                    catch (Exception e) {
                    }
                }
            }
        } catch( Exception e){}
        resultado.total = resultado.ocorrencias.size()
        render resultado as JSON
    } // fim loadPortalbio


    /**
    * método para pesquisar referência bibliográfica
     * @param q - texto a ser pesquisado
     * @param inListaOficialVigente - filtrar somente a referências marcas como sendo uma lista oficial vigente
     * @param refBibSearchFor - filtrar somente em um dos campos: autor, titulo ou ano
     * @exemple
     *  localizar referencias que possuam a palavra paulo no autor ou titulo: paulo
     *  localizar referencias que possuam a palavra paulono autor ou titulo: paulo publicada em 2014: paulo 2014
     *  http://localhost:8080/salve-estadual/api/searchRefBib/luis/2008
     * @return
    */
    def searchRefBib( String p, String ano, String campo ) {

        Map data = [ total:0, errors:[],items: []]

        if ( ! params.p ) {
            data.errors.push('Mínimo 3 caracteres para pesquisar')
            data.errors.push('Exemplo 1: https://salve.icmbio.gov.br/salve-estadual/api/searchRefBib/The Birds South America 1989')
            data.errors.push('Exemplo 2: https://salve.icmbio.gov.br/salve-estadual/api/searchRefBib/Ridgely 1989')
        }
        else
        {
            params.p = params?.p?.trim()

            // validar ano
            if (params.ano) {
                params.ano = params.ano.trim()
                if (!params.ano.isInteger() || params.int('ano') < 1500 || params.int('ano') > 2999) {
                    data.errors.push('Ano inválido deve estar entre 1500 e 2999.')
                }
                params.p += ' ' + params.ano
            }

            // validar campó
            if (params.campo && !(params.campo.toUpperCase() ==~ /AUTOR|TITULO|ANO/) ) {
                data.errors.push('Campos válidos: autor, titulo ou ano.')
            }
        }

        if( data.errors )
        {
            render data as JSON
            return
        }

        List lista = publicacaoService.search( params.p, null, null, params.campo )

        if (lista) {
            lista.each {
                data.items.push(['id'       : it.id,
                                 'titulo'   : it.deTitulo,
                                 'autor'    : it.noAutor,
                                 'ano'      : it.nuAnoPublicacao,
                                 'citacao'  : it.referenciaHtml,
                                 'descricao': it.tituloAutorAno,
                                 'doi'      : it.deDoi,
                                 'issn'     : it.deIssn,
                                 'isbn'     : it.deIsbn
                ])

            }
        }
        data.total = data.items.size()
        render data as JSON
    }

    /**
     * metodo para recuperar dados das tabelas de apoio e preencher
     * selects via ajax
     */
    def getTableApoio() {
        Map res = [status:0,msg:'',data:[] ]
        try {
            if( !params.tableName ) {
                throw new Exception('Código da tabela não informado');
            }
            res.data = dadosApoioService.getTable(params.tableName)
        } catch (Exception e ) {
            res.status=1;
            res.msg=e.getMessage()
        }
        render res as JSON
    }


    /**
     * método para retornar as camadas utilizadas no mapa utiliznao o salve como proxy para acessar o serviço de mapa
     * devido ao problema do https://
     * String pUrl = "http://mapas.icmbio.gov.br/cgi-bin/mapserv?map=/geodados/icmbio/mapfile/biomas.map&version=1.1.1&service=WMS&request=GetLegendGraphic&format=image/png&SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&FORMAT=image%2Fpng&TRANSPARENT=true&LAYERS=biomas&WIDTH=256&HEIGHT=256&CRS=EPSG%3A3857&STYLES=&BBOX=-7505654.01421598%2C-2501884.6714053266%2C-5003769.342810653%2C0'"
     * pUrl = java.net.URLDecoder.decode(pUrl, StandardCharsets.UTF_8.name())
     * @return
     */
    def getLayer(){
        enableCors()

        String hashName
        String extension
        String dirLayer
        File file
        params.cache = !params.cache || params.cache.toString() == 'true'
        params.debug = params.debug && params.debug.toString() == 'true'

        // params.cache=true;

        if( params.debug ) {
            println ' '
            println 'API-getLayer() ' + new Date().format('hh:mm:ss')
            println 'Params:'
            params.each {
                println it.key + '=' + it.value
            }
            println '-' * 80
        }
        String layerUrl=''
        switch (params.layer.toLowerCase() ) {
            case 'biomas':
                layerUrl = "http://mapas.icmbio.gov.br/cgi-bin/mapserv?map=/geodados/icmbio/mapfile/biomas.map" +
                    "&version=${params.VERSION  ?:'1.1.1'}" +
                    "&service=${params.SERVICE  ?:'WMS'}" +
                    "&request=GetLegendGraphic" +
                    "&format=${params.FORMAT    ?:'image/png'}" +
                    "&SERVICE=${params.SERVICE  ?:'WMS'}" +
                    "&VERSION=${params.VERSION  ?:'1.3.0'}" +
                    "&REQUEST=${params.REQUEST  ?:'GetMap'}" +
                    "&FORMAT=${params.FORMAT    ?:'image/png'}" +
                    "&TRANSPARENT=${params.TRANSPARENT?:'true'}" +
                    "&LAYERS=${params.layer}" +
                    "&WIDTH=${params.WIDTH      ?:'256'}" +
                    "&HEIGHT=${params.height    ?:'256'}" +
                    "&CRS=${params.CRS          ?:'EPSG:3857'}" +
                    "&STYLES=" +
                    "&BBOX=${params.BBOX}"
                break
            case 'uc':
                // http://mapas.icmbio.gov.br/mapproxy/service?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&FORMAT=image/png&TRANSPARENT=true&LAYERS=uc&STYLES=&WIDTH=256&HEIGHT=256&CRS=EPSG%3A3857&BBOX=-7505654.01421598%2C-5003769.342810653%2C-5003769.342810653%2C-2501884.6714053266
                layerUrl = "http://mapas.icmbio.gov.br/mapproxy/service?" +
                    "SERVICE=${params.SERVICE  ?:'WMS'}" +
                    "&VERSION=${params.VERSION  ?:'1.3.0'}" +
                    "&REQUEST=${params.REQUEST  ?:'GetMap'}" +
                    "&FORMAT=${params.FORMAT    ?:'image/png'}" +
                    "&TRANSPARENT=${params.TRANSPARENT?:'true'}" +
                    "&LAYERS=${params.layer}" +
                    "&STYLES=" +
                    "&WIDTH=${params.WIDTH      ?:'256'}" +
                    "&HEIGHT=${params.height    ?:'256'}" +
                    "&CRS=${params.CRS          ?:'EPSG:3857'}" +
                    "&BBOX=${params.BBOX}"
                break;
        }
        // definir o tipo do conteudo retornado
        response.contentType = params.format ?: 'image/png'

        if( params.debug ) {
            println 'Url: ' + layerUrl
            println 'ContentType:' + response.contentType
            println 'Usar cache: ' + params.cache
        }

        // sempre gravar em cache as imagens
        extension = ( params.FORMAT =~ /(?i)png$/) ? '.png':''
        dirLayer = '/data/salve-estadual/cache/layers/' + params.layer.toLowerCase().replaceAll(/\s/,'_')
        file = new File( dirLayer )
        if( file.exists() ){
            file.mkdirs()
        }

        hashName = dirLayer + '/' + layerUrl.toString().encodeAsMD5() + extension
        if( params.debug ) {
            println 'hashName: ' + hashName
        }

        if( params.cache ) {
            // verficar se já existe a imagem no cache de layer
            file = new File( hashName )
            if( file.exists() ) {
                try {
                    if( params.debug ) {
                        println 'cache existe.'
                    }
                    /*response.setHeader("Content-Length", file.size().toString())
                    if( params.debug ) {
                        println 'content-Length:' + file.size().toString()
                    }*/
                    FileInputStream fileInputStream = new FileInputStream(file);
                    byte[] buf = new byte[1024]
                    int bytesread, bytesBuffered
                    while ((bytesread = fileInputStream.read(buf)) > -1) {
                        response.outputStream.write(buf, 0, bytesread);
                        bytesBuffered += bytesread;
                        if (bytesBuffered > 1024 * 1024) { //flush after 1MB
                            bytesBuffered = 0
                            response.outputStream.flush()
                        }
                    }
                }
                finally {
                    if (response.outputStream != null) {
                        if( params.debug ) {
                            println 'outputStream.flush(): ok from cache'
                        }
                        response.outputStream.flush()
                    }
                }
                return
            }
        }
        URL url = new URL( layerUrl ); // creating a url object
        HttpURLConnection urlConnection =  (HttpURLConnection) url.openConnection(); // creating a urlconnection object
        Integer responseCode = urlConnection.getResponseCode();
        if( responseCode == HttpURLConnection.HTTP_OK ){
            if( params.debug ) {
                println 'responseCode: 200'
            }
            InputStream inputStream = urlConnection.getInputStream()
            //response.outputStream << inputStream
            try {
                if (hashName) {
                    FileOutputStream outputStream = new FileOutputStream(hashName)
                    int bytesRead = -1
                    byte[] buffer = new byte[1024]
                    while ((bytesRead = inputStream.read(buffer)) != -1) {
                        response.outputStream.write(buffer, 0, bytesRead);
                        outputStream.write(buffer, 0, bytesRead)
                    }
                    outputStream.close()
                    if (params.debug) {
                        println 'imagem colocada no cache'
                    }
                }
            } catch( Exception e ){}
            response.outputStream.flush()
            if( params.debug ) {
                println 'outputStream.flush(): ok sem cache'
            }
            inputStream.close()
        }
    }

    def getLayerOcorrenciasSalve(){
        // definir valores padrão

        // pontos utilizados ou não na avaliacao. Padrão: sim
        params.utilizados = ( params?.utilizados?.toLowerCase() == 'n' ? 'n' : 's' )
        params.historico  = ( params?.historico?.toLowerCase() == 's' ? 's' : 'n' )

        // parametros passados para o serviço
        Map data = [ registrosUtilizadosNaAvaliacao : params.utilizados == 's' ]
        data.sqFicha = params.sqFicha ?: 0
        data.color   = params.color ?: ''
        data.icon    = params.icon  ?: ''
        data.historico = params.historico ?: ''
        data.visualizarRegistrosEmCarencia = false

        if( data.historico == 's' ) {
            data.icon = '/markers/' + ( data.icon ?: 'point_green_t_10x10.png' )
            if ( asset.assetPathExists(src: data.icon)) {
                data.icon = assetPath(src: data.icon)
            }
        }

        if( isLogged() ) {
            data.visualizarRegistrosEmCarencia = session.sicae.user.canViewRegsCarencia()
        }
        render mapService.getOcorrenciasSalve( data ) as JSON
    }


    def getLayerOcorrenciasSalveConsulta(){
        // definir valores padrão
        Map data = [ : ]
        data.sqFicha = params.sqFicha ?: 0
        data.color   = params.color ?: ''
        data.visualizarRegistrosEmCarencia = false
        if( isLogged() ) {
            data.visualizarRegistrosEmCarencia = session.sicae.user.canViewRegsCarencia()
        }
        render mapService.getOcorrenciasSalveConsulta( data ) as JSON
    }


    def getLayerOcorrenciasPortalbio(){
        // definir valores padrão

        // pontos utilizados ou não na avaliacao. Padrão: sim
        params.avaliados = ( params.avaliados && params?.avaliados?.toLowerCase() == 'n' ? 'n' : 's' )
        params.utilizados = ( params?.utilizados?.toLowerCase() == 'n' ? 'n' : 's')

        // parametros passados para o serviço
        Map data = [ avaliados: params.avaliados
                     ,registrosUtilizadosNaAvaliacao : params.utilizados == 's' ]
        data.sqFicha = params.sqFicha ?: 0
        data.color   = params.color ?: ''
        data.visualizarRegistrosEmCarencia = false
        if( isLogged() ) {
            data.visualizarRegistrosEmCarencia = session.sicae.user.canViewRegsCarencia()
        }
        render mapService.getOcorrenciasPortalbio( data ) as JSON
    }

    /**
     * metodo para retornar as informações do ponto clicado para ser exibido no popup do mapa
     * @return
     */
    def getInfoOcorrencia(){
        enableCors();
        Map data = [ idRegistro : params.idRegistro ?: 0 ]
        data.noBaseDados = params.noBaseDados ?: ''
        data.logged= false
        data.isADM = false
        if( session?.sicae?.user ) {
            data.logged = true
            data.isADM = session?.sicae.user.isADM()
        }
        render mapService.getOcorrenciaInfo( data ) as JSON
    }

    /**
     * retornar geojson com os registros de ocorrência de uma especie
     * utilizando o novo padrão de cores Verde, Amarela ou Vermelho
     * no modulo publico
     */
    def camadasOcorrenciasModuloPublico() {
        if ( ! params.sqFicha ) {
            throw new Exception('Id da ficha não informado')
        }
        params.sqFicha  = decryptStr(params.sqFicha.replaceAll(' ', '+')).toLong();
        params.salvePublico = true;
        this.camadasOcorrencias();
    }
    /**
     * retornar geojson com os registros de ocorrência de uma especie
     * utilizando o novo padrão de cores Verde, Amarela ou Vermelho
     */

    def camadasOcorrencias() {
        enableCors();
        response.contentType = 'application/json'
        // criar os filtros sql dinamicos de acordo com os parametros recebidos
        List sqlWhereSalve = []
        List sqlWherePortal = []
        Map sqlParamsSalve = [:]
        Map sqlParamsPortal = [:]
        List sqlInnerJoins = []

        // ler as informações da institucao do salve-estadual
        Configuracao configuracao = Configuracao.findByEstadoIsNotNull()

        Map result =  ["type": "FeatureCollection"
                       , 'crs': ['type': 'name', 'properties': ['name': 'EPSG:4326']]
                       , "features": []
                       , "errors":[]
                       , "ocorrencias":[] // campo temporário no caso de features
            ]

        try {

            //throw new Exception('Teste de erro')
            if ( ! params.sqFicha ) {
                throw new Exception('Id da ficha não informado')
            }

            boolean contextoExterno = false
            if ( ! isLogged() || params.salvePublico || params.salveConsulta ) {
                contextoExterno = true
            }
            if( params.contexto && params.contexto=='validador' ){
                contextoExterno=true
            }

            // não mostrar os registros em carência e sensíveis no contexto PUBLICO e CONSULTA e no contexto do validador
            if( contextoExterno ) {
                sqlWhereSalve.push('( salve.calc_carencia(fo.dt_inclusao::date, prazo_carencia.cd_sistema) is null or ' +
                    'salve.calc_carencia(fo.dt_inclusao::date, prazo_carencia.cd_sistema) < now() )')
                sqlWherePortal.push('( fo.dt_carencia is null or fo.dt_carencia < now() )')
                sqlWhereSalve.push("""( fo.in_sensivel IS NULL or fo.in_sensivel = 'N' )""")

                // filtrar pela situação do registro
                if( params.salvePublico ) {
                    sqlWhereSalve.push("situacao_avaliacao.cd_sistema in ('REGISTRO_UTILIZADO_AVALIACAO')")
                    sqlWherePortal.push("situacao_avaliacao.cd_sistema in ('REGISTRO_UTILIZADO_AVALIACAO')")

                } else {
                    sqlWhereSalve.push("situacao_avaliacao.cd_sistema in ('REGISTRO_NAO_CONFIRMADO','REGISTRO_NAO_UTILIZADO_AVALIACAO','REGISTRO_UTILIZADO_AVALIACAO')")
                    sqlWherePortal.push("situacao_avaliacao.cd_sistema in ('REGISTRO_NAO_CONFIRMADO','REGISTRO_NAO_UTILIZADO_AVALIACAO','REGISTRO_UTILIZADO_AVALIACAO')")
                }

            }

            // exibir as ocorrencias da especie e de suas subespecies
            List idsFichasSubespecies = mapService.getIdsFichasSubespecies( params.sqFicha.toLong() )
            if ( idsFichasSubespecies ) {
                sqlWhereSalve.push("fo.sq_ficha in ( ${params.sqFicha} , ${idsFichasSubespecies.join(',') } )")
                sqlWherePortal.push("fo.sq_ficha in ( ${params.sqFicha} , ${idsFichasSubespecies.join(',') } )")
            } else {
                sqlWhereSalve.push('fo.sq_ficha = :sqFicha')
                sqlWherePortal.push('fo.sq_ficha = :sqFicha')
                sqlParamsSalve.sqFicha = params.sqFicha.toLong()
                sqlParamsPortal.sqFicha = params.sqFicha.toLong()
            }
        String cmdSql = """with oSalve as (
                    select distinct fo.sq_ficha
                                  , fo.sq_ficha_ocorrencia as sq_ocorrencia
                                  , 'salve' as  no_origem
                                  , concat(case when fo.sq_contexto = 420 then 'salve' else
                                    case when fo.sq_contexto = 1053 then 'salve-consulta' else '' end end ,'-','${configuracao.estado.sgEstado}') as no_sistema
                                  , st_x( coalesce(reg.ge_registro,fo.ge_ocorrencia)) as nu_x
                                  , st_y( coalesce(reg.ge_registro,fo.ge_ocorrencia)) as nu_y
                                  , salve.calc_carencia(fo.dt_inclusao::date, prazo_carencia.cd_sistema) as dt_carencia
                                  , uf.sg_estado
                                  , ( ref_aproximacao.cd_sistema='ESTADO' AND aproximacao.cd_sistema = 'CENTROIDE')::boolean as st_centroide
                                  , fo.sq_situacao_avaliacao
                                  , fo.in_presenca_atual = 'N' as st_registro_historico
                                  , '${configuracao.noInstituicao}' as no_instituicao_origem  
                                  , '${configuracao.sgInstituicao}' as sg_instituicao_origem 
                                  , '${configuracao.estado.sgEstado}' as sg_estado_origem
                            from salve.ficha_ocorrencia fo
                             left join salve.ficha_ocorrencia_registro freg on freg.sq_ficha_ocorrencia = fo.sq_ficha_ocorrencia --freg.sq_registro = reg.sq_registro
                             left join salve.registro as reg on reg.sq_registro = freg.sq_registro
                             left outer join salve.dados_apoio as prazo_carencia on prazo_carencia.sq_dados_apoio = fo.sq_prazo_carencia
                             left outer join salve.dados_apoio as aproximacao on aproximacao.sq_dados_apoio = fo.sq_metodo_aproximacao
                             left outer join salve.dados_apoio as ref_aproximacao on ref_aproximacao.sq_dados_apoio = fo.sq_ref_aproximacao
                             left outer join salve.dados_apoio as situacao_avaliacao on situacao_avaliacao.sq_dados_apoio = fo.sq_situacao_avaliacao
                             left outer join salve.estado uf on uf.sq_estado = fo.sq_estado
                             where fo.ge_ocorrencia is not null
                             ${sqlWhereSalve ? ' and ' + sqlWhereSalve.join('\nand ') : ''}

                )
                select oSalve.*, (nivel_taxonomico.co_nivel_taxonomico = 'SUBESPECIE')::bool as st_subespecie
                ,situacao_avaliacao.ds_dados_apoio as ds_situacao_avaliacao
                ,situacao_avaliacao.cd_sistema as cd_situacao_avaliacao
                , case when situacao_avaliacao.cd_sistema = 'REGISTRO_UTILIZADO_AVALIACAO' then
                    case when oSalve.st_registro_historico then 'regUtilizadoHistorico' else 'regUtilizados' end
                  else
                    case when situacao_avaliacao.cd_sistema = 'REGISTRO_NAO_UTILIZADO_AVALIACAO' then 'regNaoUtilizados' else
                       case when situacao_avaliacao.cd_sistema = 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO' then 'regConfirmadoAdicionadoAposAvaliacao' else 'regNaoConfirmados' end
                    end
                 end as no_layer
                from oSalve
                         inner join salve.ficha on ficha.sq_ficha = oSalve.sq_ficha
                         inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                         inner join taxonomia.nivel_taxonomico on nivel_taxonomico.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
                         inner join salve.dados_apoio as situacao_avaliacao on situacao_avaliacao.sq_dados_apoio = oSalve.sq_situacao_avaliacao
                order by nu_y, nu_x
                """

            /** /
            println ' '
            println params
            println cmdSql
            println sqlParamsSalve
            /**/
            sqlService.execSql(cmdSql, sqlParamsSalve).each { row ->

                Map feature = createFeature(row.no_origem.toString()
                    , row.sq_ocorrencia.toString()
                    , row.nu_x, row.nu_y, 'registros')

                // criar o icone de acordo com a situação e tipo do registro
                String icon = ''
                if( row.no_layer == 'regUtilizados' ) {
                    icon = 'ponto_verde_12x12.png'
                    if( row.st_subespecie ){
                        icon = 'ponto_verde_12x12_sub.png'
                    }
                } else if( row.no_layer == 'regNaoUtilizados' ) {
                    icon = 'ponto_vermelho_12x12.png'
                    if(row.st_subespecie  ){
                        icon = 'ponto_vermelho_12x12_sub.png'
                    }
                } else if( row.no_layer == 'regConfirmadoAdicionadoAposAvaliacao' ) {
                    icon = 'ponto_verde_12x12_X.png'
                    if( row.st_subespecie ){
                        icon = 'ponto_verde_12x12_X_sub.png'
                    }
                } else if( row.no_layer == 'regUtilizadoHistorico' ) {
                    icon = 'ponto_verde_12x12_T.png'
                    if( row.st_subespecie  ){
                        icon = 'ponto_verde_12x12_T_sub.png'
                    }
                } else if( row.no_layer == 'regNaoConfirmados' ) {
                    icon = 'ponto_amarelo_12x12.png'
                    if( row.st_subespecie ){
                        icon = 'ponto_amarelo_12x12_sub.png'
                    }
                }

                row.em_carencia = ( row.dt_carencia && row.dt_carencia > Util.hoje()) ? 'S' :'N'

                // se o ponto for centroide do Estado e não tiver sendo utilizado na avaliação, desconsiderar o centroide
                // para não hachurar o Estado
                if( row.st_centroide && row.cd_situacao_avaliacao == 'REGISTRO_NAO_UTILIZADO_AVALIACAO' ) {
                    row.st_centroide = false
                }

                // adicionar propriedades em cada ponto de acordo com o que se deseja aplicar filtros no mapa
                feature.properties += [sqFicha       : row.sq_ficha
                                       , origem      : row.no_origem
                                       , sistema     : row.no_sistema
                                       , sqOcorrencia: row.sq_ocorrencia
                                       , noLayer     : row.no_layer
                                       , isSubespecie: row.st_subespecie
                                       , emCarencia  : row.em_carencia
                                       , sgEstado    : row.sg_estado
                                       , isCentroide : row.st_centroide
                                       , isHistorico : row.st_registro_historico
                                       , cdSituacao  : row.cd_situacao_avaliacao
                                       , inUtilizadoAvaliacao  : (row.cd_situacao_avaliacao == 'REGISTRO_UTILIZADO_AVALIACAO' ? 's' : 'n')
                                       , icon         :icon
                        ,sgOrigem: row.sg_instituicao_origem
                        ,noOrigem: row.no_instituicao_origem
                        ,sgEstadoOrigem : row.sg_estado_origem
                ]
                if (feature) {
                    result.features.push(feature)
                }
            }
        } catch( Exception e ){
            result.errors.push( e.getMessage() )
            if( ! session?.envProduction ) {
                println ' '
                println e.getMessage()
            }
        }
        /*
        println 'RESULTADO'
        println result
        */
        //Thread.sleep(1000 );
        render result as JSON
    }


    /**
     * metodo para retornar os poligonos dos estados para
     * exibição no mapa
     * @return
     */
    def estadosCentroide(){
        enableCors()
        response.contentType = 'application/json'
        Map result =  ["type": "FeatureCollection"
                       , 'crs': ['type': 'name', 'properties': ['name': 'EPSG:4326']]
                       , "features": []
                       , "errors":[]
        ]
        String siglasEstados = ''
        // pode ser enviado as siglas seperadas por virgula ou somente uma
        if( params.siglas ) {
            siglasEstados = params.siglas.split(',').collect { "'" + it + "'" }.join(',');
        } else if( params.sigla ) {
            siglasEstados = "'"+params.sigla+"'"
        }
        if( siglasEstados ) {
            //siglasEstados = Estado.list().sgEstado.collect { "'" + it + "'" }.join(',');
            //println ' '
            //println Estado.list().sgEstado.collect { "'" + it + "'" }.join(',');

            List lista = sqlService.execSql("""select ST_AsGeoJSON( uf.ge_estado,13,8) as feature, sg_estado, no_estado
                 from salve.estado as uf where sg_estado in (${siglasEstados}) and uf.ge_estado is not null""")

            // criar o texto que será exibido no popup ao clicar no mapa
            Map info = [:]
            if( params.origem && params.sqOcorrencia ){
                    Map data = [ idRegistro : params.sqOcorrencia, noBaseDados: params.origem,centroide:true]
                    //JSONObject res = JSON.parse( mapService.getOcorrenciaInfo( data ) )
                    info = mapService.getOcorrenciaInfo( data )
            }

            lista.each { row ->

                Map feature = ["type"      : "Feature",
                               "id"        : 'centroideEstado-'+row.sg_estado,
                               "properties": [  'id'            :'centroideEstado-'+row.sg_estado
                                              , 'sistema'       :params?.sistema
                                              , 'origem'        :params?.origem
                                              , 'sqOcorrencia'  :params?.sqOcorrencia
                                              , 'type'          :'centroide'
                                              , 'text'          : info?.text ?: ''
                                              , 'sigla'         : row.sg_estado
                                              , 'nome'          : row.no_estado],
                               "geometry"  : JSON.parse(row.feature)
                ]
                result.features.push( feature )
           }
        }
        render result as JSON
    }

    /**
     * método para fazer a tradução de textos utilizando o libreTranslate ou google translate
     * @param textoOriginal
     * @param tradutorPadrao
     * @return JSON
     */
    def translate() {
        enableCors()
        response.contentType = 'application/json'

        /** /
        println ' '
        println 'API-TRANSLATE'
        println 'Parametros:'
        println  params
        /**/

        // utilizar o serviço libretranslate como padrão
        String tradutorPadrao = params.tradutorPadrao ?: 'libre'
        String textoOriginal  = params.texto ?: ''

        Map result = [msg   : '',
                      type  : 'success',
                      status: 0,
                      data  : ['traducao': '']
        ]

        if( ! textoOriginal){
            render result as JSON
            return
        }

        try {

            if( !isLogged() ){
                throw  new Exception('Usuário não autenticado')
            }

            if( ! tradutorPadrao =~ /^(libre|google)$/ ) {
                tradutorPadrao = 'libre'
            }
           // TODO - CHAMAR SERVICO DE TRADUCAO WEB
            result.data.traducao= requestService.doTranslate( textoOriginal, tradutorPadrao.toLowerCase() )
        } catch (Exception e) {
            result.type='error'
            result.status=1
            result.msg = e.getMessage()
        }
        render result as JSON
    }

    /**
     * método para mostrar os participantes da equipe da autorização SISBIO
     */
    def equipeAutorizacaoSisbio(){
        enableCors();
        response.contentType = 'application/json'
        Map res = [status:0, msg:'', type:'success',data:[] ]
        // TODO - CRIAR ENDPOINT API DE COMUNICAÇÃO COM SALVE PARA PEGAR ESTAS INFORMAÇÕES
        render res as JSON
    }

    def getLayerBiomas(){
        enableCors()
        String cmdSql = """SELECT sq_bioma,no_bioma, ST_AsGeoJSON( bioma.ge_bioma,13,8) as geoJson from salve.bioma order by bioma.no_bioma"""
        Map result =  ["type": "FeatureCollection"
                       //, 'crs': ['type': 'name', 'properties': ['name': 'EPSG:4326']]
                       , "features": []
        ]
        Map coresBiomas = ['AMAZONIA':'#b0fd19',
                            'CAATINGA':'#fdfdb0',
                            'CERRADO':'#fdc3b0',
                            'SISTEMA_COSTEIRO_MARINHO':'#6d8aee',
                            'MATA_ATLANTICA':'#d7fdb0',
                            'PAMPA':'#fdf0d6',
                            'PANTANAL':'#fde5fd']

        sqlService.execSql( cmdSql,[:],[:],60 ).each { row ->
            if( row.geojson ) {
                String codigoSistema = Util.calcularCodigoSistema( row.no_bioma )
                result.features.push( [
                    type: 'Feature'
                    ,'properties': [
                        'id'    : 'bioma-'+row.sq_bioma
                       ,'name'  : row.no_bioma
                       ,'code'  : codigoSistema
                       ,'color' : coresBiomas[codigoSistema ]
                       ,'sqBioma':row.sq_bioma.toString()
                   ]
                   ,geometry: JSON.parse( row.geojson )
                ])
            }
        }
        render result as JSON
    }

    /**
     * Retorna o histórico de avaliação de uma espécie
     * @example http://salve.dev.icmbio.gov.br:8080/salve-estadual/api/historicoAvaliacao?noCientifico=Puma concolor
     * @return
     */
    def getHistoricoAvaliacao(){
        enableCors()
        Map res = [status: 404, msg: '',data:[]]
        try {
            if ( ! params.noCientifico ) {
                throw new RuntimeException("Nome científico não informado")
            }
            if( params.noCientifico.split(' ').size() < 2 ){
                throw new RuntimeException("Nome científico inválido. Informe uma espécie ou uma subespécie")
            }
            Configuracao configuracao = Configuracao.findByEstadoIsNotNull()
            if( !configuracao ){
                throw new RuntimeException("Dados iniciais de configuração da aplicação não foi preenchido")
            }

            // verificar se é espécie ou subespécie
            boolean isEspecie  = params.noCientifico.split(' ').size() == 2
            String noNivelTaxonomico = isEspecie ? 'especie' : 'subespecie'
            //NivelTaxonomico nivelTaxonomico = NivelTaxonomico.findByCoNivelTaxonomico( isEspecie ? 'especie' : 'subespecie' )

            // localizar o taxon pelo nome científico
            String cmdSql = """select hist.sq_taxon_historico_avaliacao
                             , coalesce(hist.id_origem,hist.sq_taxon_historico_avaliacao) as id_origem
                             , json_trilha->'reino'->>'no_taxon' as no_reino
                             , json_trilha->'filo'->>'no_taxon' as no_filo
                             , json_trilha->'classe'->>'no_taxon' as no_classe
                             , json_trilha->'ordem'->>'no_taxon' as no_ordem
                             , json_trilha->'familia'->>'no_taxon' as no_familia
                             , json_trilha->'genero'->>'no_taxon' as no_genero
                             , json_trilha->'especie'->>'no_taxon' as no_especie
                             , json_trilha->'subespecie'->>'no_taxon' as no_subepecie
                             , tipo.ds_dados_apoio as ds_tipo
                             , tipo.cd_sistema as cd_tipo_sistema
                             , hist.nu_ano_avaliacao
                             , hist.st_possivelmente_extinta
                             , categoria.cd_sistema as cd_categoria_sistema
                             , categoria.cd_dados_apoio as cd_categoria_iucn
                             , categoria.cd_dados_apoio_nivel as cd_categoria_nivel
                             , hist.de_criterio_avaliacao_iucn as ds_criterio
                             , hist.tx_justificativa_avaliacao as tx_justificativa
                             from salve.taxon_historico_avaliacao hist
                             inner join taxonomia.taxon on taxon.sq_taxon = hist.sq_taxon
                             inner join salve.dados_apoio as categoria on categoria.sq_dados_apoio = hist.sq_categoria_iucn
                             inner join salve.dados_apoio as tipo on tipo.sq_dados_apoio = hist.sq_tipo_avaliacao
                             left join salve.abrangencia on abrangencia.sq_abrangencia = hist.sq_abrangencia
                             left join salve.estado as uf on uf.sq_estado = abrangencia.sq_estado
                        where tipo.cd_sistema = 'ESTADUAL'
                          and json_trilha->'${noNivelTaxonomico}'->>'no_taxon'= '${params.noCientifico}'
                          and uf.sg_estado = '${configuracao.estado.sgEstado}'
                          order by hist.nu_ano_avaliacao desc, hist.sq_taxon_historico_avaliacao desc;
                    """
            res.data = [origem:configuracao.asMap(),itens:sqlService.execSql(cmdSql)]
            res.msg=''
            res.status=200

        } catch (Exception e) {
            res.msg = e.getMessage()
        }
        render res as JSON
    }

    /**
     * Disponibilizar os registros de ocorrência do salve estadual no formato GeoJson para exibição no mapa
     * @return
     */
    def layerOcorrencias() {
        enableCors()
        Map res = [status: 404, msg: '', data: []]
        try {

            if ( ! params.noCientifico ) {
                throw new RuntimeException("Nome científico não informado")
            }
            if( params.noCientifico.split(' ').size() < 2 ){
                throw new RuntimeException("Nome científico inválido. Informe uma espécie ou uma subespécie")
            }
            boolean isEspecie  = params.noCientifico.split(' ').size() == 2
            String noNivelTaxonomico = isEspecie ? 'especie' : 'subespecie'

            // localizar o taxon pelo nome científico
            String cmdSql = """select ficha.sq_ficha
                        from taxonomia.taxon
                        inner join salve.ficha on ficha.sq_taxon = taxon.sq_taxon
                        inner join taxonomia.nivel_taxonomico nt on nt.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
                        where json_trilha->'${noNivelTaxonomico}'->>'no_taxon' = '${params.noCientifico}'
                        and ficha.sq_ciclo_avaliacao = 2
                        and nt.co_nivel_taxonomico = '${noNivelTaxonomico.toUpperCase()}';
                    """
            List rows = sqlService.execSql(cmdSql,[:])
            if( ! rows ) {
                throw new RuntimeException("Táxon não cadastrado na base taxonômica")
            }
            params.sqFicha  =  rows[0].sq_ficha
            if ( ! params.sqFicha) {
                throw new Exception('Nome científico não informado')
            }
            params.sqFicha  = encryptStr(params.sqFicha.toString())
            return camadasOcorrenciasModuloPublico()
        } catch (Exception e) {
            res.msg = e.getMessage()
        }
        render res as JSON
    }
}
