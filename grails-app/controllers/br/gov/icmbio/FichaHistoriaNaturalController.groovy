/**
 * Este controlador é resposável por receber as ações referentes ao cadastro aba História Natural da ficha de espécies
 * Regras:
 *
 *
 *
 *
 *
 */

package br.gov.icmbio
import grails.converters.JSON
import br.gov.icmbio.DadosApoioService

class FichaHistoriaNaturalController extends FichaBaseController {

	def dadosApoioService
	def messageSource
    def fichaService

	def index() {
		Ficha ficha = Ficha.get(params.sqFicha)
        List listPadraoDeslocamento	= dadosApoioService.getTable('TB_PADRAO_DESLOCAMENTO')
        render(template:"/ficha/historiaNatural/index",model:['ficha':ficha
                                                              ,'listPadraoDeslocamento':listPadraoDeslocamento]);
	}

	def saveFrmHistoriaNatural()
	{
		Ficha ficha = Ficha.get(params.sqFicha)
		ficha.properties = params
        ficha.padraoDeslocamento    = null;
        if( params.sqPadraoDeslocamento )
        {
            ficha.padraoDeslocamento = DadosApoio.get(params.sqPadraoDeslocamento);
        }
        try {
            ficha.save(flush:true)
			fichaService.updatePercentual( ficha )
            //fichaService.atualizarPercentualPreenchimento( ficha )
            this._returnAjax(0, getMsg(1), "success")
        } catch(Exception e) {
            println e.getMessage()
            this._returnAjax(1, getMsg(2), "error")
        }
	}

	//--------------------------------------------------------------------------------------------------------
	// 3.1 - Aba Hábito Alimentar
	//--------------------------------------------------------------------------------------------------------

	def tabHisNatHabitoAlimentar()
	{
		if( !params.sqFicha )
		{
			render 'Id da ficha deve ser informado!'
			return;
		}
		// ler posições tróficas validas
		List listPosicaoTrofica = dadosApoioService.getTable('TB_POSICAO_TROFICA').sort{ it.ordem }
		List listHabitoAlimentar = dadosApoioService.getTable('TB_HABITO_ALIMENTAR').sort{ it.ordem }
		//List listNivelTaxonomico = NivelTaxonomico.list();
		List listNivelTaxonomico = NivelTaxonomico.findAllByNuGrauTaxonomicoGreaterThanEquals(grailsApplication.config.grau.familia);
		List listCategoriaIucn	 = dadosApoioService.getTable('TB_CATEGORIA_IUCN').findAll{ it.codigoSistema != 'OUTRA' && it.codigo != 'AMEACADA'}.sort{ it.ordem };
		Ficha ficha = Ficha.get(params.sqFicha)
		render(template:"/ficha/historiaNatural/formHabitoAlimentar",
				model:['ficha':ficha,
						'listPosicaoTrofica':listPosicaoTrofica,
						'listHabitoAlimentar':listHabitoAlimentar,
						'listNivelTaxonomico':listNivelTaxonomico,
						'listCategoriaIucn'  :listCategoriaIucn
					])
	}

	def saveFrmHisNatPosicaoTrofica()
	{
		if( !params.sqFicha )
		{
			render 'Id da ficha deve ser informado!'
			return;
		}
		/*
		// carlos pediu para retirar este campo
		if( ! params.sqPosicaoTrofica )
		{
			return this._returnAjax(1, 'Informe a posição trófica!', "error")
		}
		*/
		Ficha ficha = Ficha.get(params.sqFicha);
		//ficha.posicaoTrofica = DadosApoio.get(params.sqPosicaoTrofica);
		ficha.stHabitoAlimentEspecialista = params.stHabitoAlimentEspecialista;
        try {
            ficha.save(flush:true)
            if( ficha.stHabitoAlimentEspecialista == 'N')
            {
				FichaHabitoAlimentarEsp.findAllByFicha( ficha ).each{
					it.delete(flush:true);
				};
			}


            this._returnAjax(0, getMsg(1), "success")
        } catch(Exception e) {
            println e.getMessage()
            this._returnAjax(1, getMsg(2), "error")
        }
		return this._returnAjax(0, 'ok', "info")
	}

	/*
		METODOS DO HABITO ALIMENTAR
	*/
	def saveFrmHisNatHabitoAlimentar()
	{
		FichaHabitoAlimentar fha;
		if( !params.sqFicha )
		{
			render 'Id da ficha deve ser informado!'
			return;
		}
		if( ! params.sqTipoHabitoAlimentar )
		{
			return this._returnAjax(1, 'Informe o hábito alimentar!', "error")
		}
		if( params.sqFichaHabitoAlimentar )
		{
			fha = FichaHabitoAlimentar.get( params.sqFichaHabitoAlimentar );
		}
		else
		{
			fha = new FichaHabitoAlimentar();
		}

		fha.ficha 					= Ficha.get(params.sqFicha);
		fha.tipoHabitoAlimentar 	= DadosApoio.get( params.sqTipoHabitoAlimentar );
		fha.dsFichaHabitoAlimentar 	= params.dsFichaHabitoAlimentar;

		List errors=[];
		if( !fha.validate() )
        {
            fha.errors.allErrors.each {
                errors.push(messageSource.getMessage(it, null, null) );
            }
        }
        if( errors )
        {
        	return this._returnAjax(1, errors.join('<br/>'), "error");
        }
	    try {
            fha.save(flush:true)

			// atualizar log da ficha, quem alterou
			fha.ficha.dtAlteracao = new Date()
			fha.ficha.save()

			this._returnAjax(0, getMsg(1), "success")
        } catch(Exception e) {
            println e.getMessage()
            this._returnAjax(1, getMsg(2), "error")
        }
		return this._returnAjax(0, 'ok', "info")
	}

	def getGridHabitoAlimentar()
	{
		if( !params.sqFicha)
		{
			render '';
			return;
		}
		List listHabitoAlimentar = FichaHabitoAlimentar.findAllByFicha(Ficha.get(params.sqFicha));
		render( template:'/ficha/historiaNatural/divGridHabitoAlimentar',model:['listHabitoAlimentar':listHabitoAlimentar]);
	}

	def editHabitoAlimentar()
	{
		FichaHabitoAlimentar reg = FichaHabitoAlimentar.get(params.id);
		if( reg?.id )
		{
			render reg.asJson();
			return;
		}
		render [:] as JSON;

	}

	def deleteHabitoAlimentar()
	{
		FichaHabitoAlimentar reg = FichaHabitoAlimentar.get(params.id);
		if( reg )
		{
			Ficha ficha  = reg.ficha
			reg.delete(flush:true);

			// atualizar log da ficha, quem alterou
			ficha.dtAlteracao = new Date()
			ficha.save()

			return this._returnAjax(0, getMsg(4), "success")
		}
		else
		{
			return this._returnAjax(1, getMsg(51), "error")
		}
		render '';
	}

	def saveFrmObsHabitoAlimentar()
	{
		if( !params.sqFicha )
		{

			return this._returnAjax(1, 'Id da ficha deve ser informado!', "error")
		}
		Ficha ficha = Ficha.get(params.sqFicha);
		ficha.dsHabitoAlimentar = params.dsHabitoAlimentar;
		ficha.save(flush:true);
        this._returnAjax(0, getMsg(1), "success")
	}

	/*
		MÉTODOS DO MODULO HABITO ALIMENTAR ESPECIALISTA
	*/
	def saveFrmHisNatHabitoAlimentarEsp()
	{
		FichaHabitoAlimentarEsp fha;
		if( !params.sqFicha )
		{
			render 'Id da ficha deve ser informado!'
			return;
		}
		if( ! params.sqTaxon )
		{
			return this._returnAjax(1, 'Informe o Taxon!', "error")
		}
		if( params.sqFichaHabitoAlimentarEsp )
		{
			fha = FichaHabitoAlimentarEsp.get( params.sqFichaHabitoAlimentarEsp );
		}
		else
		{
			fha = new FichaHabitoAlimentarEsp();
		}

		fha.ficha 						= Ficha.get(params.sqFicha);
		fha.taxon 						= Taxon.get( params.sqTaxon );
		fha.categoriaIucn				= DadosApoio.get(params.sqCategoriaIucn);
		fha.dsFichaHabitoAlimentarEsp 	= params.dsFichaHabitoAlimentarEsp;
		if( !fha.validate() )
        {
            fha.errors.allErrors.each {
                errors.push(messageSource.getMessage(it, null, null) );
            }
        }
        if( errors )
        {
        	return this._returnAjax(1, errors.join('<br/>'), "error");
        }
	    try {
            fha.save(flush:true)
            if( fha.ficha.stHabitoAlimentEspecialista != 'S')
            {
            	fha.ficha.stHabitoAlimentEspecialista = 'S';
            	fha.ficha.save( flush:true);
        	}
            this._returnAjax(0, getMsg(1), "success")
        } catch(Exception e) {
            println e.getMessage()
            this._returnAjax(1, getMsg(2), "error")
        }
		return this._returnAjax(0, 'ok', "info")
	}

	def getGridHabitoAlimentarEsp()
	{
		if( !params.sqFicha)
		{
			render '';
			return;
		}
		List listHabitoAlimentarEsp = FichaHabitoAlimentarEsp.findAllByFicha(Ficha.get(params.sqFicha.toLong()));
		render( template:'/ficha/historiaNatural/divGridHabitoAlimentarEspecialista',model:['listHabitoAlimentarEsp':listHabitoAlimentarEsp]);
	}

	def editHabitoAlimentarEsp()
	{
		FichaHabitoAlimentarEsp reg = FichaHabitoAlimentarEsp.get(params.id);
		if( reg?.id )
		{
			render reg.asJson();
			return;
		}
		render [:] as JSON;
	}

	def deleteHabitoAlimentarEsp()
	{
		FichaHabitoAlimentarEsp reg = FichaHabitoAlimentarEsp.get(params.id);
		if( reg )
		{
			reg.delete(flush:true);
			return this._returnAjax(0, getMsg(4), "success")
		}
		else
	{
			return this._returnAjax(1, getMsg(51), "error")
		}
		render '';
	}

	//--------------------------------------------------------------------------------------------------------
	// Aba Habitat
	//--------------------------------------------------------------------------------------------------------

	def tabHisNatHabitat()
	{
		if( !params.sqFicha)
		{
			render 'Id da ficha não encontrado!'
			return;
		}
		// carregar habitas pais
		List listHabitat = dadosApoioService.getTable('TB_HABITAT').sort{ it.ordem }
		Ficha ficha 	= Ficha.get(params.sqFicha);
		render(template:"/ficha/historiaNatural/formHabitat",
			model:[ficha:ficha,listHabitat:listHabitat]);
	}

	def saveFrmHisNatHabitat()
	{
		if( !params.sqFicha )
		{
			return this._returnAjax(1, 'ID ficha não encontrado!', "error")
		}
		if( ! params.sqHabitatPai )
		{
			return this._returnAjax(1, 'Selecione o Habitat!', "error")
		}
		DadosApoio habitat;
		if( params.sqHabitat)
		{
			habitat = DadosApoio.get(params.sqHabitat);
		}
		else
		{
			habitat = DadosApoio.get(params.sqHabitatPai);
		}
		Ficha ficha = Ficha.get(params.sqFicha);

		// verificar se já foi cadastrado
		if ( FichaHabitat.findByFichaAndHabitat(ficha,habitat) )
		{
			this._returnAjax(1, getMsg(3), "error")
			return;
		}
		FichaHabitat fh = new FichaHabitat();
		fh.ficha 		= ficha
		fh.habitat 		= habitat
        try {
            fh.save(flush:true)

			// atualizar log da ficha, quem alterou
			ficha.dtAlteracao = new Date()
			ficha.save()

			this._returnAjax(0, getMsg(1), "success")
        } catch(Exception e) {
            println e.getMessage()
            this._returnAjax(1, getMsg(2), "error")
        }
		return this._returnAjax(0, 'ok', "info")
	}
	def getGridHabitat()
	{
		if( !params.sqFicha)
		{
			render '';
			return;
		}
		List listHabitat = FichaHabitat.findAllByFicha(Ficha.get(params.sqFicha)).sort{ it.descricao.toUpperCase() }
		render( template:'/ficha/historiaNatural/divGridHabitat',model:['listHabitat':listHabitat]);
	}
	def deleteHabitat()
	{
		FichaHabitat reg = FichaHabitat.get(params.id);
		if( reg )
		{
			Ficha ficha = reg.ficha
			reg.delete(flush:true)

			// atualizar log da ficha, quem alterou
			reg.ficha.dtAlteracao = new Date()
			reg.ficha.save()

			return this._returnAjax(0, getMsg(4), "success")
		}
		else
		{
			return this._returnAjax(1, getMsg(51), "error")
		}
		render '';
	}
	def saveFrmHisNatHabitatDetalhe()
	{

		if( !params.sqFicha )
		{
			return this._returnAjax(1, 'ID ficha não encontrado!', "error")
		}

		if( params.stEspecialistaMicroHabitat != 'S')
		{
			params.dsEspecialistaMicroHabitat = null
		}
		if( params.stDiferMachoFemeaHabitat != 'S')
		{
			params.dsDiferencaMachoFemea = null
		}

		Ficha ficha = Ficha.get(params.sqFicha)
		ficha.properties = params
		ficha.save( flush:true)
		return this._returnAjax(0, getMsg(1), "success")
	}

	//--------------------------------------------------------------------------------------------------------
	// Aba Interação
	//--------------------------------------------------------------------------------------------------------
	def tabHisNatInteracao()
	{
		if( !params.sqFicha )
		{
			render 'Id da ficha não informado!'
			return;
		}
		Ficha ficha = Ficha.get(params.sqFicha);
		List listTipoInteracao 		= dadosApoioService.getTable('TB_TIPO_INTERACAO').sort{ it.ordem }
		List listClassificacao 		= dadosApoioService.getTable('TB_CLASSIFICACAO_INTERACAO').sort{ it.ordem }
		List listCategoriaIucn		= dadosApoioService.getTable('TB_CATEGORIA_IUCN').findAll{ it.codigoSistema != 'OUTRA' && it.codigo != 'AMEACADA'}.sort{ it.ordem };
		List listNivelTaxonomico 	= NivelTaxonomico.findAllByNuGrauTaxonomicoGreaterThanEquals(grailsApplication.config.grau.familia);

		render(template:"/ficha/historiaNatural/formInteracao",
			model:['ficha':ficha,
			'listTipoInteracao':listTipoInteracao,
			'listClassificacao':listClassificacao,
			'listCategoriaIucn':listCategoriaIucn,
					'listNivelTaxonomico'	: listNivelTaxonomico
				  ]);
	}
	def saveFrmHisNatInteracao()
	{
		if( !params.sqFicha )
		{
			return this._returnAjax(1, 'ID ficha não encontrado!', "error")
		}
		Ficha ficha = Ficha.get(params.sqFicha);
		ficha.properties = params;
		ficha.save( flush:true);
		return this._returnAjax(0, getMsg(1), "success");
	}

	def saveFrmHisNatInteracaoDetalhe()
	{
		if( !params.sqFicha )
		{
			return this._returnAjax(1, 'ID ficha não encontrado!', "error")
		}

		FichaInteracao fi;
		if( params.sqFichaInteracao )
		{
			fi = FichaInteracao.get(params.sqFichaInteracao);

		}
		else
		{
			fi = new FichaInteracao();

		}
		fi.properties    = params;
		fi.ficha 		 = Ficha.get( params.sqFicha)
		fi.tipoInteracao = DadosApoio.get(params.sqTipoInteracao)
		fi.classificacao = null;
		fi.categoriaIucn = null;
		if( params.sqClassificacao )
		{
			fi.classificacao = DadosApoio.get(params.sqClassificacao)
		}
		if( params.sqCategoriaIucn )
		{
			fi.categoriaIucn = DadosApoio.get(params.sqCategoriaIucn)
		}
        if( params.sqTaxonInteracao )
        {
            fi.taxon = Taxon.get(params.sqTaxonInteracao);
            if (fi.taxon.nivelTaxonomico.nuGrauTaxonomico >= grailsApplication.config.grau.especie.toInteger() && !fi.categoriaIucn)
            {
                return this._returnAjax(1, 'Necessário informar a Categoria.', "error");
            }
        }
		List errors = []
		if( !fi.validate() )
        {
            fi.errors.allErrors.each {
            	errors.push(it.field+ ' campo Obrigatório!');
                //errors.push(messageSource.getMessage(it, null, it.field+' campo obrigatório') );
            }
        }
        if( errors )
        {
        	return this._returnAjax(1, errors.join('<br/>'), "error");
        }
		fi.save( flush:true)

		// atualizar log da ficha, quem alterou
		fi.ficha.dtAlteracao = new Date()
		fi.ficha.save()


		return this._returnAjax(0, getMsg(1), "success");
	}

	def getGridInteracao()
	{
		if( !params.sqFicha)
		{
			render '';
			return;
		}
        List listInteracaoSubespecies = []
        Ficha ficha = Ficha.get(params.sqFicha.toInteger() )
        if( ficha.taxon.nivelTaxonomico.coNivelTaxonomico=='ESPECIE')
        {
            List fichasSubepecies = Ficha.createCriteria().list {
                taxon {
                    eq('pai',ficha.taxon)
                }
            }
            listInteracaoSubespecies = FichaInteracao.findAllByFichaInList( fichasSubepecies )
        }
		List listInteracao = FichaInteracao.findAllByFicha(ficha)
        listInteracao += listInteracaoSubespecies
		render( template:'/ficha/historiaNatural/divGridInteracao',model:['ficha':ficha,'listInteracao':listInteracao,temSubespecie:listInteracaoSubespecies.size()>0])
	}


	def editInteracao()
	{
		FichaInteracao reg = FichaInteracao.get(params.id);
		if( reg?.id )
		{
			render reg.asJson();
			return;
		}
		render [:] as JSON;
	}

	def deleteInteracao()
	{
		FichaInteracao reg = FichaInteracao.get(params.id);
		if( reg )
		{
			Ficha ficha = reg.ficha
			reg.delete(flush:true)

			// atualizar log da ficha, quem alterou
			ficha.dtAlteracao = new Date()
			ficha.save()

			return this._returnAjax(0, getMsg(4), "success")
		}
		else
		{
			return this._returnAjax(1, getMsg(51), "error")
		}
		render '';
	}

	def lerObsInteracaoSubespecie()
	{
		if( ! params.sqFicha )
		{
			render ''
			return
		}
		Ficha ficha = Ficha.get(params.sqFicha.toInteger() )
		List fichas = Ficha.createCriteria().list {
        	taxon {
            	eq('pai',ficha.taxon)
        	}
        	order('dtAlteracao','desc')
        	maxResults(1)
    	}
    	if( fichas.size() > 0 )
    	{
    		render fichas.dsInteracao.join('<br/>')
    	}
    	render ''
	}

	//--------------------------------------------------------------------------------------------------------
	// Aba Reprodução
	//--------------------------------------------------------------------------------------------------------
	def tabHisNatReproducao()
	{
		if( !params.sqFicha )
		{
			render 'Id da ficha não informado!'
			return
		}
		Ficha ficha = Ficha.get(params.sqFicha)
		List listModoReproducao			= dadosApoioService.getTable('TB_MODO_REPRODUCAO').sort{ it.ordem }
		List listSistemaAcasalamento	= dadosApoioService.getTable('TB_SISTEMA_ACASALAMENTO').sort{ it.ordem }
		List listUnidadeTempo			= dadosApoioService.getTable('TB_UNIDADE_TEMPO').sort{ it.ordem }
		List listUnidadeMedida			= dadosApoioService.getTable('TB_UNIDADE_METRICA').sort{ it.ordem }
		List listUnidadePeso			= dadosApoioService.getTable('TB_UNIDADE_PESO').sort{ it.ordem }
		render(template:"/ficha/historiaNatural/formReproducao",model:
				['ficha' 					: ficha,
				 'listModoReproducao'		: listModoReproducao,
				 'listSistemaAcasalamento'	: listSistemaAcasalamento,
				 'listUnidadeTempo'			: listUnidadeTempo,
				 'listUnidadeMedida'		: listUnidadeMedida,
				 'listUnidadePeso'			: listUnidadePeso
				]);
	}

	def saveFrmHisNatReproducao()
	{
		if( !params.sqFicha )
		{
			return this._returnAjax(1, 'ID ficha não encontrado!', "error")
		}

		Ficha ficha  =  Ficha.get( params.sqFicha)
		ficha.properties = params;

		ficha.modoReproducao 			= DadosApoio.get(params.sqModoReproducao)
		ficha.sistemaAcasalamento 		= DadosApoio.get(params.sqSistemaAcasalamento)
		ficha.unidIntervaloNascimento 	= DadosApoio.get(params.sqUnidIntervaloNascimento)
		ficha.unidTempoGestacao 		= DadosApoio.get(params.sqUnidTempoGestacao)

		ficha.unidMaturidadeSexualMacho 	= DadosApoio.get(params.sqUnidMaturidadeSexualMacho)
		ficha.unidMaturidadeSexualFemea 	= DadosApoio.get(params.sqUnidMaturidadeSexualFemea)

		ficha.unidadePesoMacho 			= DadosApoio.get(params.sqUnidadePesoMacho)
		ficha.unidadePesoFemea 			= DadosApoio.get(params.sqUnidadePesoFemea)

		ficha.medidaComprimentoMacho 	= DadosApoio.get(params.sqMedidaComprimentoMacho)
		ficha.medidaComprimentoFemea 	= DadosApoio.get(params.sqMedidaComprimentoFemea)

		ficha.medidaComprimentoMachoMax	= DadosApoio.get(params.sqMedidaComprimentoMachoMax)
		ficha.medidaComprimentoFemeaMax	= DadosApoio.get(params.sqMedidaComprimentoFemeaMax)

		ficha.unidadeSenilidRepMacho 		= DadosApoio.get(params.sqUnidadeSenilidRepMacho)
		ficha.unidadeSenilidRepFemea 		= DadosApoio.get(params.sqUnidadeSenilidRepFemea)

		ficha.unidadeLongevidadeMacho 		= DadosApoio.get(params.sqUnidadeLongevidadeMacho)
		ficha.unidadeLongevidadeFemea 		= DadosApoio.get(params.sqUnidadeLongevidadeFemea)
		List errors = []
		if( !ficha.validate() )
        {
            ficha.errors.allErrors.each {
            	errors.push(it.field+ ' campo Obrigatório!');
                //errors.push(messageSource.getMessage(it, null, it.field+' campo obrigatório') );
            }
        }
        if( errors )
        {
        	return this._returnAjax(1, errors.join('<br/>'), "error");
        }
		ficha.save( flush:true);
		return this._returnAjax(0, getMsg(1), "success");
	}

	//--------------------------------------------------------------------------------------------------------
	// Aba Sazonalidade
	//--------------------------------------------------------------------------------------------------------
	def tabHisNatSazonalidade()
	{

		if( !params.sqFicha )
		{
			render 'Id da ficha não informado!'
			return
		}
		List listFaseVida 	= dadosApoioService.getTable('TB_FASE_VIDA').sort{ it.ordem }
		List listEpoca 		= dadosApoioService.getTable('TB_EPOCA').sort{ a,b ->
			 a.ordem+a.descricao <=> b.ordem+b.descricao
		}
		List listHabitat	= dadosApoioService.getTable('TB_HABITAT').sort{ it.ordem }
		render(template:"/ficha/historiaNatural/formSazonalidade",
			'model':['listFaseVida'	: listFaseVida,
					'listEpoca'		: listEpoca,
					'listHabitat'	: listHabitat,
					]);
	}

	def saveFrmHisNatSazonalidade()
	{

		if( !params.sqFicha )
		{
			return this._returnAjax(1, 'ID ficha não informado!', "error");
		}
		FichaVariacaoSazonal fvs;
		if( params.sqFichaVariacaoSazonal )
		{
			fvs = FichaVariacaoSazonal.get( params.sqFichaVariacaoSazonal)
		}
		else
		{
			fvs = new FichaVariacaoSazonal();
		}
		fvs.dsFichaVariacaoSazonal  = params.dsFichaVariacaoSazonal
		fvs.ficha 					= Ficha.get( params.sqFicha )
		fvs.faseVida 				= DadosApoio.get( params.sqFaseVida)
		fvs.epoca 					= DadosApoio.get( params.sqEpoca)
		fvs.habitat 				= DadosApoio.get( params.sqHabitat)
		List errors = []
		if( !fvs.validate() )
        {
            fvs.errors.allErrors.each {
            	errors.push(it.field+ ' campo Obrigatório!');
                //errors.push(messageSource.getMessage(it, null, it.field+' campo obrigatório') );
            }
        }
        if( errors )
		{
        	return this._returnAjax(1, errors.join('<br/>'), "error");
        }
		fvs.save( flush:true)

		// atualizar log da ficha, quem alterou
		fvs.ficha.dtAlteracao = new Date()
		fvs.ficha.save()

		return this._returnAjax(0, getMsg(1), "success");
	}

	def getGridVariacaoSazonal()
	{
		if( !params.sqFicha)
		{
			render '';
			return;
		}
		List listVariacaoSazonal = FichaVariacaoSazonal.findAllByFicha(Ficha.get(params.sqFicha));
		render( template:'/ficha/historiaNatural/divGridVariacaoSazonal',model:['listVariacaoSazonal':listVariacaoSazonal]);
	}

	def editVariacaoSazonal()
	{
		FichaVariacaoSazonal reg = FichaVariacaoSazonal.get(params.id);
		if( reg?.id )
		{
			render reg.asJson();
			return;
		}
		render [:] as JSON;
	}

	def deleteVariacaoSazonal()
	{
		FichaVariacaoSazonal reg = FichaVariacaoSazonal.get(params.id);
		if( reg )
		{
			Ficha ficha = reg.ficha
			reg.delete(flush:true)

			// atualizar log da ficha, quem alterou
			ficha.dtAlteracao = new Date()
			ficha.save()

			return this._returnAjax(0, getMsg(4), "success")
		}
		else
	{
			return this._returnAjax(1, getMsg(51), "error")
		}
		render ''
	}

}
