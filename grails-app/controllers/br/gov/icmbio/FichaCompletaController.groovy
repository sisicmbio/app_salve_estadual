package br.gov.icmbio

import com.vividsolutions.jts.geom.Geometry
import com.vividsolutions.jts.geom.GeometryFactory
import com.vividsolutions.jts.io.WKTReader

//import com.vividsolutions.jts.geom.Coordinate
//import com.vividsolutions.jts.geom.Geometry
//import com.vividsolutions.jts.geom.GeometryFactory
import grails.converters.JSON
import org.codehaus.groovy.grails.web.json.JSONObject
import org.hibernate.internal.jaxb.mapping.orm.JaxbOneToMany

//import com.vividsolutions.jts.io.WKTReader

class FichaCompletaController extends BaseController {

    def dadosApoioService
    def fichaService
    def cacheService
    def emailService
    def sqlService

    def index() {

        List listMudancaCategoria
        List listResultados
        List listCategoriaIucn = dadosApoioService.getTable('TB_CATEGORIA_IUCN').findAll { it.codigoSistema != 'OUTRA' && it.codigoSistema != 'AMEACADA' && it.codigoSistema != 'POSSIVELMENTE_EXTINTA' }.sort { it.ordem };
        List listTipoAbrangencia = dadosApoioService.getTable('TB_TIPO_ABRANGENCIA')
        List listConectividade = dadosApoioService.getTable('TB_CONECTIVIDADE')
        List listTendencia = dadosApoioService.getTable('TB_TENDENCIA').sort { it.ordem }

        ValidadorFicha validadorFicha1, validadorFicha2
        List listOutrosValidadores = []
        Map validadoresPosicoes = [:]
        int validadorNumero = 1

        PessoaFisica validadorLogado
        boolean flagEditCategoria = true
        boolean canModify
        boolean fichaVersionada = false
        Ficha ficha
        OficinaFicha oficinaFicha
        Map taxonStructure
        Map dadosUltimaAvaliacao
        Map dadosImagemPrincipal
        List listSituacaoColaboracao

        if (!params.sqFicha) {
            render '<h3>Nenhuma ficha Selecionada</h3>'
            return
        }
        if (!session?.sicae?.user?.sqPessoa) {
            render '<h3>Necessário fazer login novamente.</h3>'
            return

        }
        // a ficha completa pode ser aberta a partir dos contextos Oficina, consulta e validação
        if (params.contexto == null || !params.contexto) {
            params.contexto = 'consulta'
        }

        if (params.canModify == null || params.canModify.toString() == 'S' || params.canModify.toString() == 'true') {
            params.canModify = true
            canModify = true
        } else {
            params.canModify = false
            canModify = false
        }

        if (params.sqOficinaFicha) {
            oficinaFicha = OficinaFicha.get(params.sqOficinaFicha.toLong())
            canModify = oficinaFicha.oficina.situacao.codigoSistema == 'ABERTA'
        }

        if (params.contexto == 'oficina' && !oficinaFicha) {
            render '<h3>Id da OficinaFicha inválido!</h3>'
            return
        }

        ficha = Ficha.get(params.sqFicha.toInteger())
        if (!ficha) {
            render '<h3>Id Ficha inválido!</h3>'
            return
        }

        if (params.contexto == 'oficina') {
            params.verColaboracoes = params.canModify
            listMudancaCategoria = dadosApoioService.getTable('TB_MUDANCA_CATEGORIA').sort { it.ordem }
            listTendencia = dadosApoioService.getTable('TB_TENDENCIA').sort { it.ordem }
            //listCategoriaIucn = dadosApoioService.getTable('TB_CATEGORIA_IUCN').findAll { it.codigoSistema != 'NE' && it.codigoSistema != 'OUTRA' && it.codigoSistema != 'AMEACADA' && it.codigoSistema != 'POSSIVELMENTE_EXTINTA' }.sort { it.ordem };

        } else if (params.contexto == 'validacao') {
            listCategoriaIucn = dadosApoioService.getTable('TB_CATEGORIA_IUCN').findAll { it.codigoSistema != 'NE' && it.codigoSistema != 'OUTRA' && it.codigoSistema != 'AMEACADA' && it.codigoSistema != 'POSSIVELMENTE_EXTINTA' }.sort { it.ordem };
        } else if (params.contexto == 'publicacao') {
            params.verColaboracoes = false
        } else if (params.contexto == 'validador') {
            if (!params.sqOficina) {
                render '<h3>Id da Oficina inválido!</h3>'
                return
            }
            oficinaFicha = OficinaFicha.findByOficinaAndVwFicha(
                Oficina.get(params.sqOficina.toLong()), VwFicha.get(ficha.id))
            params.verColaboracoes = false
            params.sqOficinaFicha = oficinaFicha.id

            listCategoriaIucn = dadosApoioService.getTable('TB_CATEGORIA_IUCN').findAll {
                it.codigoSistema != 'NE' && it.codigoSistema != 'OUTRA' && it.codigoSistema != 'AMEACADA' && it.codigoSistema != 'POSSIVELMENTE_EXTINTA' }.sort { it.ordem };
            listResultados = dadosApoioService.getTable('TB_RESULTADO_VALIDADOR')
            validadorLogado = PessoaFisica.get(session?.sicae?.user?.sqPessoa?.toInteger())
            validadorFicha1 = this.getValidadorFicha(validadorLogado, oficinaFicha.id, false) // resultado do validador logado
            validadorFicha2 = this.getValidadorFicha(validadorLogado, oficinaFicha.id, true) // resultado do outro validador
            listOutrosValidadores = this.getOutrosValidadoresFicha(validadorLogado, oficinaFicha.id)

            // calcular a posição do validador entre os validadores cadastrados
            if (validadorFicha1) {
                listOutrosValidadores.eachWithIndex { it, index ->
                    if (it.id < validadorFicha1.id) {
                        validadorNumero++
                        validadoresPosicoes[it.id.toString()] = index + 1
                    } else {
                        validadoresPosicoes[it.id.toString()] = index + 2
                    }
                }
                validadoresPosicoes[validadorFicha1.id.toString()] = validadorNumero
            }
        }
        // ler a estrutura taxonômica
        taxonStructure = ficha.taxon.getStructure()

        if (params.verColaboracoes) {
            listSituacaoColaboracao = dadosApoioService.getTable('TB_SITUACAO_COLABORACAO')
        }

        // recuperar os dados da última avaliacao nacional da tabela taxon_hist
        dadosUltimaAvaliacao = ficha.ultimaAvaliacaoNacionalHist

        if (dadosUltimaAvaliacao.id) {
            dadosUltimaAvaliacao.txJustificativaAvaliacao = Util.stripTags(dadosUltimaAvaliacao.txJustificativaAvaliacao)
        }

        // ler registros portalbio - esta travando a avaliação nas espécies com milhares de registro
        /*if( canModify ) {
            fichaService.asyncLoadPortalbio([idFicha: ficha.id.toInteger()])
        }*/

        // ler imagens grafico populacao
        // DadosApoio contexto = dadosApoioService.getByCodigo('TB_CONTEXTO_ANEXO', 'GRAFICO_POPULACAO')
        // List listGraficosPopulacao = FichaAnexo.findAllByFichaAndContexto(ficha, contexto)
        List listGraficosPopulacao = []

        // imagem principal se houver
        dadosImagemPrincipal = fichaService.getFichaImagemPrincipal(ficha)

        // dados para o grafico de tempo geracional
        Map dadosGraficoTempoGeracional = [anoAvaliacao : new Date().format( 'yyyy' ).toInteger(),
            anoInicial:0,
            anoFinal:0,
            menosAnos:0,
            maisAnos:0
        ]

        if( ficha.vlTempoGeracional ){
            dadosGraficoTempoGeracional.valor = ficha.vlTempoGeracional
            if( ficha.medidaTempoGeracional.codigoSistema == 'DECADA'){
                dadosGraficoTempoGeracional.valor /= 10
            } else if( ficha.medidaTempoGeracional.codigoSistema == 'DIA'){
                dadosGraficoTempoGeracional.valor /= 365
            } else if( ficha.medidaTempoGeracional.codigoSistema == 'MES'){
                dadosGraficoTempoGeracional.valor /= 365
            } else if( ficha.medidaTempoGeracional.codigoSistema == 'HORA'){
                dadosGraficoTempoGeracional.valor /= 365
            }
            dadosGraficoTempoGeracional.valor *= 3

            dadosGraficoTempoGeracional.valor = dadosGraficoTempoGeracional.valor.round().toInteger()
            if ( dadosGraficoTempoGeracional.valor < 10) {
                dadosGraficoTempoGeracional.valor = 10;
            }
            dadosGraficoTempoGeracional.anoInicial = dadosGraficoTempoGeracional.anoAvaliacao - dadosGraficoTempoGeracional.valor
            dadosGraficoTempoGeracional.anoFinal   = dadosGraficoTempoGeracional.anoAvaliacao + dadosGraficoTempoGeracional.valor
            dadosGraficoTempoGeracional.menosAnos  =  dadosGraficoTempoGeracional.valor * -1
            dadosGraficoTempoGeracional.maisAnos  =  dadosGraficoTempoGeracional.valor
        }

        // se a ficha da oficina estiver versionada não permitir a edição
        fichaVersionada = oficinaFicha?.oficinaFichaVersao ? true : false

        //FichaVersao fichaVersao
        if( params.sqFichaVersao && params.sqFichaVersao.toInteger() > 0i ) {
            //fichaVersao = FichaVersao.get( params.sqFichaVersao.toLong() )
            fichaVersionada=true
            canModify = false
        }
        if( ficha.situacaoFicha.codigoSistema=='EXCLUIDA'){
            canModify = false
        }

        if( params.contexto == 'consulta'){
            if( !( ficha.situacaoFicha.codigoSistema =~ /CONSULTA|CONSULTA_FINALIZADA/) ){
                canModify = false
            }
        }
        /** /
            println ' '
            println 'Situacao ficha ' + ficha.situacaoFicha.codigoSistema
            println 'can modify ' + canModify
            println 'Versionada:'+fichaVersionada
            println 'id Versionada:' + params.sqFichaVersao ? params.sqFichaVersao.toLong(): 0l
        /**/
        render(view: 'index', model: [ficha                    : ficha
                                      , oficinaFicha           : oficinaFicha
                                      , canModify              : canModify
                                      , sqFichaVersao          : params.sqFichaVersao ? params.sqFichaVersao.toLong(): 0l
                                      , fichaVersionada        : fichaVersionada
                                      , dadosUltimaAvaliacao   : dadosUltimaAvaliacao
                                      , taxonStructure         : taxonStructure
                                      , verColaboracoes        : (params.boolean('verColaboracoes') ? 'true' : 'false')
                                      , contexto               : params.contexto
                                      , listSituacaoColaboracao: listSituacaoColaboracao
                                      , listCategoriaIucn      : listCategoriaIucn
                                      , listMudancaCategoria   : listMudancaCategoria
                                      , flagEditCategoria      : flagEditCategoria
                                      , listResultados         : listResultados
                                      , validadorFicha1        : validadorFicha1
                                      , validadorFicha2        : validadorFicha2
                                      , listOutrosValidadores  : listOutrosValidadores
                                      , validadorNumero        : validadorNumero
                                      , validadoresPosicoes    : validadoresPosicoes
                                      , dadosImagemPrincipal   : dadosImagemPrincipal
                                      , listConectividade      : listConectividade
                                      , listTendencia          : listTendencia
                                      , convidadoChat          : true // exibir o botão de enviar no chat no _formChat
                                      , listTipoAbrangencia    : listTipoAbrangencia
                                      , listGraficosPopulacao  : listGraficosPopulacao
                                      , dadosGraficoTempoGeracional:dadosGraficoTempoGeracional
        ])

    }

    /**
     *  Criar gride com as ocorrencias do portalbio, salve e colaborações
     **/
    def getGridOcorrencias() {
    /** /
    println ' '
    println ' '
    println ' '
    println 'FichaCompletaController().getGridOcorrencias()'
    println params
    println ' '
    /**/
            List qryWhereSalve = []
            List qryWhereSugestoes = []
            List qryWherePortalbio = []
            Integer paginationPageSize = 100


            if( !params.filtroSelecionado ) {
                if (params.contexto == 'consulta') {
                    params.filtroSelecionado = 'CC' // Com Colaboração
                } else if (params.contexto == 'oficina') {
                    params.filtroSelecionado = 'T' // T-Todos RO - Resolver Oficina
                } else {
                    params.filtroSelecionado = 'T' // todos
                }
            }

            if(!params?.sqFicha) {
                render 'Parâmetro Ficha não informada!'
                return;
            }

            // ler as colaborações do usuario na ficha no cicloConsulta
            // WebUsuario usr = WebUsuario.get(params.idUsuario)
            Ficha ficha = Ficha.get(params.sqFicha.toInteger())

            List subespecies = fichaService.getSubespeciesFicha(ficha)
            List sqFichasEspecie_e_subespecies = [ficha.id] + ( subespecies ? subespecies.sqFicha : [] )

            // contexto representa o módulo que chamou a ficha completa: Ex: consulta, oficina
            params.contexto = params.contexto ?: 'consulta'
            if (!ficha) {
                render 'Ficha inválida!'
                return;
            }

            params.sqFichaVersao = params.sqFichaVersao ? params.sqFichaVersao.toLong() : 0l

            // Tratar a versão da ficha
            if( params.sqFichaVersao > 0 ) {
                qryWhereSalve.push("( contexto.cd_sistema <> 'CONSULTA' OR ( ccfv.sq_ciclo_consulta_ficha IS NOT NULL AND ccfv.sq_ficha_versao = ${params.sqFichaVersao} ) )")
            } else {
                qryWhereSalve.push("( contexto.cd_sistema <> 'CONSULTA' OR  ccfv.sq_ciclo_consulta_ficha IS NULL OR situacao_avaliacao.cd_sistema IN ('REGISTRO_UTILIZADO_AVALIACAO', 'REGISTRO_NAO_UTILIZADO_AVALIACAO', 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO') )")
            }

            qryWhereSalve.push("o.sq_ficha in ( ${sqFichasEspecie_e_subespecies.join(',')} )")
            qryWherePortalbio.push("o.sq_ficha in ( ${sqFichasEspecie_e_subespecies.join(',')} )")

            if (params.contexto == 'validacao') {
                qryWhereSalve.push("(o.in_utilizado_avaliacao = 'S' OR o.in_utilizado_avaliacao is null)")
                qryWherePortalbio.push("o.in_utilizado_avaliacao = 'S'")
            }


            // considerar como "com colaboração" os registros com "Concordo: SIM/NÃO" e os novos registros adicionados pelos colaboradores
            if (params.filtroSelecionado == 'CC' ) {
                qryWhereSalve.push("(sugestoes.json_sugestoes is not null or (contexto.cd_sistema = 'CONSULTA' and ccfv.sq_ficha_versao ${ params.sqFichaVersao>0 ? "="+params.sqFichaVersao : "is null"} ))")
                qryWherePortalbio.push("(sugestoes.json_sugestoes is not null )")
                //qryWhereSalve.push("(sugestoes.json_sugestoes is not null)")
                //qryWherePortalbio.push("(sugestoes.json_sugestoes is not null)")
            } // Todos

            String cmdSql // Com Colaboração
            String sqlOrderBy = "order by col_ordem, no_base_dados, nu_lat, nu_lon"
            if( params.bd ) {
                cmdSql = "select motor.*, row_number() over ( ${sqlOrderBy} ) as row_number from (\n"
            }
            else {
                cmdSql = "select motor.* from (\n"
            }

            // criar where das sugestões
            if( params.contexto == 'oficina'){
                if( params.filtroSelecionado == 'RO'){
                    qryWhereSugestoes.push("situacao.cd_sistema='RESOLVER_OFICINA'")
                }
            }

            if (params.sqFichaVersao > 0 ) {
                qryWhereSugestoes.push("ccfv.sq_ficha_versao = " + params.sqFichaVersao)
            } else {
                qryWhereSugestoes.push("ccfv.sq_ficha_versao is null")
            }

            // ler as ocorrencias do Salve, Salve Consulta e do Portalbio
            cmdSql += """select 'A' as col_ordem, coalesce(oc.sq_ficha_ocorrencia_consulta, o.sq_ficha_ocorrencia)                         as id
                 , o.sq_ficha_ocorrencia                                                                    as id_ocorrencia
                 , 'salve' as no_fonte
                 , case when oc.sq_ficha_ocorrencia_consulta is null then 'Salve' else 'Salve Consulta' end as no_base_dados
                 , st_y(o.ge_ocorrencia)                                                                    as nu_lat
                 , st_x(o.ge_ocorrencia)                                                                    as nu_lon
                 , datum.ds_dados_apoio                                                                     as no_datum
                 , estado.sg_estado                                                                         as no_estado
                 , municipio.no_municipio                                                                   as no_municipio
                 , o.no_localidade                                                                          as no_localidade
                 , o.tx_local                                                                               as tx_local
                 , precisao.ds_dados_apoio                                                                  as no_precisao
                 , autor.no_pessoa                                                                         as no_autor
                 , case
                       when o.nu_dia_registro is not null then concat(o.nu_dia_registro::text, '/', o.nu_mes_registro::text, '/',
                                                                      o.nu_ano_registro::text)
                       else
                           case
                               when o.nu_mes_registro is not null then concat(o.nu_mes_registro, '/', o.nu_ano_registro::text)
                               else
                                   case when o.nu_ano_registro is not null then o.nu_ano_registro::text else '' end
                               end
                end                             as dt_ocorrencia
                 , o.hr_registro                as hr_registro
                 , o.in_utilizado_avaliacao     as in_utilizado_avaliacao
                 , o.tx_nao_utilizado_avaliacao as tx_nao_utilizado_avaliacao
                 , o.tx_nao_aceita              as tx_nao_aceita
                 -- situacao da colaboração quando o registro vier de uma consulta
                 , situacao.cd_sistema          as cd_situacao_sistema
                 , situacao.ds_dados_apoio      as ds_situacao
                 -- situacao do registro no SALVE
                 , situacao_avaliacao.cd_sistema          as cd_situacao_avaliacao_sistema
                 , situacao_avaliacao.ds_dados_apoio      as ds_situacao_avaliacao
                 , revisor.no_pessoa            as no_revisor
                 , sugestoes.json_sugestoes     as json_sugestoes
                 , refBib.json_ref_bib          as json_ref_bib
                 , multimidia.json_multimidia   as json_multimidia
                 , ccfv.sq_ficha_versao         as sq_ficha_versao
            from salve.ficha_ocorrencia o
                     inner join salve.dados_apoio as contexto on contexto.sq_dados_apoio = o.sq_contexto
                     left join salve.ficha_ocorrencia_consulta as oc on oc.sq_ficha_ocorrencia = o.sq_ficha_ocorrencia
                     left outer join salve.ciclo_consulta_ficha ccf on ccf.sq_ciclo_consulta_ficha = oc.sq_ciclo_consulta_ficha
                     left outer join salve.ciclo_consulta_ficha_versao ccfv on ccfv.sq_ciclo_consulta_ficha = ccf.sq_ciclo_consulta_ficha
                     left outer join salve.pessoa as autor on autor.sq_pessoa = oc.sq_web_usuario
                     left outer join salve.dados_apoio as situacao_avaliacao on situacao_avaliacao.sq_dados_apoio = o.sq_situacao_avaliacao
                     left outer join salve.dados_apoio as situacao on situacao.sq_dados_apoio = oc.sq_situacao
                     left outer join salve.dados_apoio as datum on datum.sq_dados_apoio = o.sq_datum
                     left outer join salve.dados_apoio as precisao on precisao.sq_dados_apoio = o.sq_precisao_coordenada
                     left outer join salve.estado as estado on estado.sq_estado = o.sq_estado
                     left outer join salve.municipio as municipio on municipio.sq_municipio = o.sq_municipio
                     left outer join salve.pessoa as revisor on revisor.sq_pessoa = o.sq_pessoa_revisor

                     -- REF BIB
                     left join lateral (
                            select json_object_agg(x.sq_ficha_ref_bib,
                                                   json_build_object('de_titulo', coalesce(publicacao.de_titulo, '')
                                                       , 'no_autor', coalesce(publicacao.no_autor, x.no_autor)
                                                       , 'nu_ano', coalesce(publicacao.nu_ano_publicacao, x.nu_ano))) as json_ref_bib
                            from salve.ficha_ref_bib x
                                     left join taxonomia.publicacao on publicacao.sq_publicacao = x.sq_publicacao
                            where x.sq_ficha = o.sq_ficha
                              and x.no_tabela = 'ficha_ocorrencia'
                              and x.sq_registro = o.sq_ficha_ocorrencia
                            ) refBib on true


                     -- FOTOS
                     left join lateral (
                         select json_object_agg(m2.sq_ficha_multimidia,
                                       json_build_object('no_arquivo', m2.no_arquivo
                                           , 'no_autor', m2.no_autor
                                           , 'cd_situacao_sistema', situacao_multimidia.cd_sistema
                                           , 'tx_nao_aceito', m2.tx_nao_aceito
                                           , 'dt_multimidia', to_char(m2.dt_elaboracao,'dd/MM/yyyy')
                                           )) as json_multimidia
                         from salve.ficha_consulta_multimidia m
                                 inner join salve.ficha_ocorrencia_consulta m1 on m1.sq_ficha_ocorrencia_consulta = m.sq_ficha_ocorrencia_consulta
                                 inner join salve.ficha_multimidia m2 on m2.sq_ficha_multimidia = m.sq_ficha_multimidia
                                 left outer join salve.dados_apoio as situacao_multimidia on situacao_multimidia.sq_dados_apoio = m2.sq_situacao
                                 inner join salve.ciclo_consulta_ficha ccf on ccf.sq_ciclo_consulta_ficha = m1.sq_ciclo_consulta_ficha
                                 left join salve.ciclo_consulta_ficha_versao ccfv on ccfv.sq_ciclo_consulta_ficha = ccf.sq_ciclo_consulta_ficha
                        where m1.sq_ficha_ocorrencia = o.sq_ficha_ocorrencia
                        AND ${ params.sqFichaVersao > 0 ? "ccfv.sq_ficha_versao = "+params.sqFichaVersao : "ccfv.sq_ficha_versao is null" }
                        ) as multimidia on true

                     -- SUGESTOES
                    left join lateral (
                        select json_object_agg(va.sq_ficha_ocorrencia_validacao,
                               json_build_object( 'sq_ficha_ocorrencia_validacao', va.sq_ficha_ocorrencia_validacao
                                   , 'in_valido', va.in_valido
                                   , 'no_usuario', usuario.no_pessoa
                                   , 'tx_observacao', va.tx_observacao
                                   , 'cd_situacao_sistema',situacao.cd_sistema
                                   , 'ds_situacao',situacao.ds_dados_apoio
                                   )) as json_sugestoes
                        from salve.ficha_ocorrencia_validacao va
                        inner join salve.pessoa as usuario on usuario.sq_pessoa = va.sq_web_usuario
                        inner join salve.ciclo_consulta_ficha ccf on ccf.sq_ciclo_consulta_ficha =va.sq_ciclo_consulta_ficha
                        inner join salve.ciclo_consulta cc on cc.sq_ciclo_consulta = ccf.sq_ciclo_consulta
                        left join salve.ciclo_consulta_ficha_versao ccfv on ccfv.sq_ciclo_consulta_ficha = va.sq_ciclo_consulta_ficha
                        left join salve.dados_apoio as situacao on situacao.sq_dados_apoio = va.sq_situacao
                        where va.sq_ficha_ocorrencia = o.sq_ficha_ocorrencia
                        ${qryWhereSugestoes ? 'AND '+qryWhereSugestoes.join(' AND '):''}
                        ) as sugestoes on true

            where o.ge_ocorrencia is not null
              and contexto.cd_sistema in ('REGISTRO_OCORRENCIA', 'CONSULTA')
              ${ params.filtroSelecionado =='RO' ? " AND (situacao.cd_sistema = 'RESOLVER_OFICINA' or sugestoes.json_sugestoes is not null)":""}
              ${qryWhereSalve ? ' and ' + qryWhereSalve.join(' and ') : ''}

            UNION ALL

            select 'B' as col_ordem, o.sq_ficha_ocorrencia_portalbio                                                                            as id
                 , o.sq_ficha_ocorrencia_portalbio                                                                            as id_ocorrencia
                 , 'portalbio'                                                                                                as no_fonte
                 , 'Portalbio'                                                                                                as no_base_dados
                 , st_x(o.ge_ocorrencia)                                                                                      as st_x
                 , st_y(o.ge_ocorrencia)                                                                                      as st_y
                 , null                                                                                                       as no_datum
                 , case
                       when left(o.tx_ocorrencia, 1) = '{' and o.tx_ocorrencia::jsonb ->> 'stateProvince'::text <> ''
                           then o.tx_ocorrencia::jsonb ->> 'stateProvince'::text
                       else uf.sg_estado end                                                                                  as no_estado
                 , case
                       when left(o.tx_ocorrencia, 1) = '{' and o.tx_ocorrencia::jsonb ->> 'municipality'::text <> ''
                           then o.tx_ocorrencia::jsonb ->> 'municipality'::text
                       else municipio.no_municipio end                                                                        as no_municipio
                 , o.no_local                                                                                                 as no_localidade
                 , ''                                                                                                         as tx_local
                 , case
                       when left(o.tx_ocorrencia, 1) = '{' then o.tx_ocorrencia::jsonb ->> 'precisaoCoord'::text
                       else '' end                                                                                            as no_precisao
                 , o.no_autor                                                                                                 as no_autor
                 , to_char(o.dt_ocorrencia,'dd/mm/yyyy')                                                                                            as dt_ocorrencia
                 , ''                                                                                                         as hr_registro
                 , o.in_utilizado_avaliacao                                                                                   as in_utilizado_avaliacao
                 , o.tx_nao_utilizado_avaliacao                                                                               as tx_nao_utilizado_avaliacao
                 , o.tx_nao_aceita                                                                                            as tx_nao_aceita
                 , situacao.cd_sistema                                                                                        as cd_situacao_sistema
                 , situacao.ds_dados_apoio                                                                                    as ds_situacao
                 , situacao_avaliacao.cd_sistema          as cd_situacao_avaliacao_sistema
                 , situacao_avaliacao.ds_dados_apoio      as ds_situacao_avaliacao
                 , revisor.no_pessoa                                                                                          as no_revisor
                 , sugestoes.json_sugestoes                                                                                   as json_sugestoes
                 , refBib.json_ref_bib
                 , null                                                                                                       as json_multimidia
                 , null as sq_ficha_versao
            from salve.ficha_ocorrencia_portalbio o
                     left join salve.estado as uf on uf.sq_estado = o.sq_estado
                     left join salve.municipio as municipio on municipio.sq_municipio = o.sq_municipio
                     left outer join salve.dados_apoio as situacao_avaliacao on situacao_avaliacao.sq_dados_apoio = o.sq_situacao_avaliacao
                     left outer join salve.dados_apoio as situacao on situacao.sq_dados_apoio = o.sq_situacao
                     left outer join salve.pessoa as revisor on revisor.sq_pessoa = o.sq_pessoa_revisor
                     left join lateral (
                select json_object_agg(x.sq_ficha_ref_bib,
                                       json_build_object('de_titulo', coalesce(publicacao.de_titulo, '')
                                           , 'no_autor', coalesce(publicacao.no_autor, x.no_autor)
                                           , 'nu_ano', coalesce(publicacao.nu_ano_publicacao, x.nu_ano))) as json_ref_bib
                from salve.ficha_ref_bib x
                         left join taxonomia.publicacao on publicacao.sq_publicacao = x.sq_publicacao
                where x.sq_ficha = o.sq_ficha
                  and x.no_tabela = 'ficha_ocorrencia_portalbio'
                  and x.sq_registro = o.sq_ficha_ocorrencia_portalbio
                ) refBib on true
                left join lateral (
                     select json_object_agg(va.sq_ficha_ocorrencia_validacao,
                           json_build_object('sq_ficha_ocorrencia_validacao', va.sq_ficha_ocorrencia_validacao
                               , 'in_valido', va.in_valido
                               , 'no_usuario', usuario.no_pessoa
                               , 'tx_observacao', va.tx_observacao
                               , 'cd_situacao_sistema',situacao.cd_sistema
                               , 'ds_situacao',situacao.ds_dados_apoio
                               )) as json_sugestoes
                       from salve.ficha_ocorrencia_validacao va
                       inner join salve.pessoa as usuario on usuario.sq_pessoa = va.sq_web_usuario
                       inner join salve.ciclo_consulta_ficha ccf on ccf.sq_ciclo_consulta_ficha =va.sq_ciclo_consulta_ficha
                       inner join salve.ciclo_consulta cc on cc.sq_ciclo_consulta = ccf.sq_ciclo_consulta
                       left join salve.ciclo_consulta_ficha_versao ccfv on ccfv.sq_ciclo_consulta_ficha = va.sq_ciclo_consulta_ficha
                       left join salve.dados_apoio as situacao on situacao.sq_dados_apoio = va.sq_situacao
                       where va.sq_ficha_ocorrencia_portalbio = o.sq_ficha_ocorrencia_portalbio
                         and ( ccfv.sq_ciclo_consulta_ficha is null ${params.sqFichaVersao > 0 ? ' or ccfv.sq_ficha_versao='+params.sqFichaVersao : ''})
                         ${ params?.contexto == 'oficina'? " and situacao.cd_sistema='RESOLVER_OFICINA'": ""}
                       ) as sugestoes on true
            where o.ge_ocorrencia is not null
              ${qryWherePortalbio ? ' and ' + qryWherePortalbio.join(' and ') : ''}
            """
            cmdSql += ") as motor\n"

            /**
             * Quando utilizar a opção "Localizar no Gride" do mapa, é
             * preciso localizar a página que o registro está
             */
            if( params.scroll && params.id && params.bd && paginationPageSize ) {
                List rows = sqlService.execSql('select tmp.row_number from (' + cmdSql + ') as tmp where no_fonte = \'' + params.bd.toLowerCase() + '\' and id_ocorrencia = ' + params.id, [:])
                if ( rows ) {
                    params.paginationPageNumber = Math.max(1, Math.ceil(rows[0].row_number / paginationPageSize)).toInteger()
                }
            }

        /** /
        println ' '
        println ' '
        println ' '
        println cmdSql
        /**/


            Map paginate = sqlService.paginate(cmdSql,[:],params, paginationPageSize)
            List listOcorrencias = paginate.rows
            Map pagination = paginate.pagination
            pagination.totalPages = pagination.totalPages ?: 1 // evitar erro no gride


            //adicionar avaliado e value para verificar se os itens estão aceitos para controle da cor de exibição no gride
            listOcorrencias.each {
                it.avaliado = false
                it.value = ''
                if (it.cd_situacao_sistema == 'RESOLVER_OFICINA') {
                    it.value = 'RESOLVER_OFICINA'
                    if (params.contexto == 'consulta') {
                        it.avaliado = true
                    }
                } else if (it.cd_situacao_sistema == 'ACEITA') {
                    it.avaliado = true
                }

                if (it.no_revisor) {
                    it.avaliado = true
                    it.value = it.no_revisor
                }
                if (it.value == '' && params.contexto == 'oficina') {
                    if (!it.cd_situacao_sistema || it.cd_situacao_sistema != 'NAO_AVALIADA') {
                        it.avaliado = true
                    }
                }
                // adicionar o link para indentificar a coordenada no mapa ao clicar
                it.coordenadas = 'Lat:&nbsp;' + it.nu_lat + '<br>Lon:&nbsp;' + it.nu_lon +
                    '&nbsp;<a href="javascript:void(0);" data-action="identificarPonto" data-fonte="' + it.no_fonte + '" data-ponto-id="' + it.id_ocorrencia + '"><img class="marker-pin" src="/salve-estadual/assets/marker_pin_red.png" title="Identificar no mapa!"></a>'

                it.refBib = Util.parseRefBib2Grid(it.json_ref_bib.toString())

                it.sugestoes=[]
                it.qtd_sim = 0
                it.qtd_nao = 0
                if( it.json_sugestoes ) {
                    try {
                        def dadosJson = JSON.parse( it.json_sugestoes.toString() )
                        dadosJson.each { key, value ->
                            /*
                            if( value.in_valido=='S'){
                                it.qtd_sim++
                                value.de_valido = 'Sim'
                            }else if( value.in_valido=='N'){
                                it.qtd_nao++
                                value.de_valido = 'Não'
                            }
                            */
                            // AJUSTE SALVE SEM CICLO
                            if( value.in_valido=='S'){
                                it.qtd_sim++
                                value.de_valido = 'Sim'
                            } else if( value.in_valido == 'N'){
                                //if( value.cd_situacao_sistema == 'NAO_AVALIADA') {
                                    it.qtd_nao++
                                //}
                                value.de_valido = 'Não'
                            }

                            it.sugestoes.push ( value )
                        }
                    } catch (Exception e) {
                    }
                }

                it.multimidias=[]
                if( it.json_multimidia ) {
                    try {
                        def dadosJson = JSON.parse( it.json_multimidia.toString() )
                        dadosJson.each { key, value ->
                            value.id = key
                            it.multimidias.push ( value )
                        }
                    } catch (Exception e) {
                        println 'ERROR'
                        println e.getMessage();
                    }
                }
            } // fim each ocorrencias


            List listSituacaoColaboracao = dadosApoioService.getTable('TB_SITUACAO_COLABORACAO').findAll {
                return it.codigoSistema ==~ /NAO_AVALIADA|ACEITA|NAO_ACEITA|RESOLVER_OFICINA/
            }
            List listSituacaoMultimidia = dadosApoioService.getTable('TB_SITUACAO_MULTIMIDIA').sort { it.ordem }

            render(template: 'divGridOcorrencias', model: [ficha                    : ficha
                                                           , listOcorrencias        : listOcorrencias
                                                           , listSituacaoColaboracao: listSituacaoColaboracao
                                                           , contexto               : params.contexto
                                                           , pagination             : pagination
                                                           , listSituacaoMultimidia : listSituacaoMultimidia
                                                           , sqFichaVersao          : params.sqFichaVersao])
    }



    def repetirUltimaAvaliacao()
    {
        render fichaService.getUltimaAvaliacaoNacionalHist( params?.sqFicha?.toLong() ) as JSON
        /*
        // ler apenas do histórico das avaliações
        Map dados = [sqCategoriaIucn:'',txJustificativaAvaliacao:'',deCriterioAvaliacaoIucn:'', stPossivelmenteExtinta:'']
        if( params.sqFicha )
        {
            VwFicha vwFicha = VwFicha.get( params.sqFicha.toInteger() )
            if( vwFicha ) {
                DadosApoio tipoNacionalBrasil = DadosApoio.findByCodigoSistema('NACIONAL_BRASIL')

                // primeiro ler a última avaliação nacional gravada no histórico
                TaxonHistoricoAvaliacao tha = TaxonHistoricoAvaliacao.createCriteria().get {
                    eq('taxon.id', vwFicha.sqTaxon.toLong())
                    eq('tipoAvaliacao', tipoNacionalBrasil)
                    lt('nuAnoAvaliacao', vwFicha.cicloAvaliacao.nuAno)
                    order('nuAnoAvaliacao', 'desc')
                    maxResults(1)
                }

                if( tha )
                {
                    dados.sqCategoriaIucn           = tha.categoriaIucn.id ?:''
                    dados.txJustificativaAvaliacao  = tha.txJustificativaAvaliacao?: ''
                    dados.deCriterioAvaliacaoIucn   = tha.deCriterioAvaliacaoIucn ?: ''
                    dados.stPossivelmenteExtinta    = tha.stPossivelmenteExtinta ?: ''
                }
            }
        }
        render dados as JSON
        */
    }

    def updateSituacaoMultimidia()
    {
        if ( ! params.sqFichaMultimidia )
        {
            render 'Id não informado!'
            return
        }
        if( !params.sqSituacao )
        {
            render 'Situação não informada!'
            return
        }
        DadosApoio situacao = DadosApoio.get( params.sqSituacao.toLong() )

        if( situacao.codigoSistema == 'REPROVADA'  )
        {
            if( ! params.txNaoAceito ) {
                render 'Necessário informar o motivo da desaprovação!'
                return
            }
        }
        else
        {
            params.txNaoAceito = null
        }

        FichaMultimidia fichaMultimidia = FichaMultimidia.get( params.sqFichaMultimidia.toLong() )
        fichaMultimidia.situacao = situacao
        fichaMultimidia.txNaoAceito =  params.txNaoAceito
        fichaMultimidia.aprovadoPor  = null
        fichaMultimidia.dtAprovacao  = null
        fichaMultimidia.reprovadoPor = null
        fichaMultimidia.dtAprovacao  = null
        if( situacao.codigoSistema == 'REPROVADA'  )
        {
            fichaMultimidia.reprovadoPor = Pessoa.get( session.sicae.user.sqPessoa.toLong() )
            fichaMultimidia.dtReprovacao = new Date()
        }
        else if( situacao.codigoSistema == 'APROVADA' )
        {
            fichaMultimidia.aprovadoPor = Pessoa.get( session.sicae.user.sqPessoa.toLong() )
            fichaMultimidia.dtAprovacao  = new Date()
        }
        fichaMultimidia.save()
        render ''
    }


	/**
	* Alterar a situação da colaboração diretamente no gride de colaborações
	**/
	def updateSituacaoColaboracao()
	{
        if ( ! params.id )
        {
            render 'Id não informado!'
            return
        }
        if ( ! params.fonte )
        {
            render 'fonte não informada!'
            return
        }
        if (!params.sqSituacao)
        {
            render 'Id da situação não informado!'
            return
        }

        DadosApoio situacao = DadosApoio.get(params.sqSituacao.toInteger() )
        if( !situacao )
        {
            render 'Id situação inválido!'
            return
        }
        if( params.fonte == 'portalbio')
        {
            FichaOcorrenciaPortalbio fop = FichaOcorrenciaPortalbio.get( params.id.toInteger() )
            if (!fop)
            {
                render 'Id inválido!'
                return
            }
            fop.situacao = situacao
            fop.pessoaRevisor = null
            fop.dtRevisao = null
            fop.inUtilizadoAvaliacao = 'N'
            if( situacao.codigoSistema==~ /ACEITA|NAO_ACEITA/) {
                fop.pessoaRevisor = PessoaFisica.get(session.sicae.user.sqPessoa)
                fop.dtRevisao = new Date()
            }
            if( situacao.codigoSistema == 'ACEITA') {
                fop.inUtilizadoAvaliacao = 'S'
                fop.txNaoUtilizadoAvaliacao=null
                fop.txNaoAceita = null
            } else {
                fop.txNaoAceita = params.txNaoAceita
                fop.txNaoUtilizadoAvaliacao = params.txNaoAceita
            }
            fop.save(flush: true)
        }
        else if( params.fonte == 'consulta')
        {
            if( situacao.codigoSistema == 'NAO_ACEITA' && !params.txNaoAceita )
            {
                render 'Necessário informar o motivo da não aceitação!'
                return
            }

            // colaboração de registro de ocorrencia
            if( params.campoFicha == 'mapa-ocorrencias') {
                FichaOcorrenciaConsulta fc = FichaOcorrenciaConsulta.get(params.id.toInteger())
                if (!fc) {
                    render 'Id inválido!'
                    return
                }
                fc.fichaOcorrencia.situacao = situacao
                fc.fichaOcorrencia.pessoaRevisor = null
                fc.fichaOcorrencia.dtRevisao = null
                fc.fichaOcorrencia.inUtilizadoAvaliacao = null
                if (situacao.codigoSistema ==~ /ACEITA|NAO_ACEITA/) {
                    fc.fichaOcorrencia.pessoaRevisor = PessoaFisica.get(session.sicae.user.sqPessoa)
                    fc.fichaOcorrencia.dtRevisao = new Date()
                }
                if (situacao.codigoSistema == 'ACEITA') {
                    fc.fichaOcorrencia.inUtilizadoAvaliacao = 'S'
                    fc.fichaOcorrencia.txNaoAceita = null
                } else if (situacao.codigoSistema == 'NAO_ACEITA') {
                    fc.fichaOcorrencia.inUtilizadoAvaliacao = 'N'
                    fc.fichaOcorrencia.txNaoAceita = Util.stripTags(params.txNaoAceita)
                }
                fc.fichaOcorrencia.save(flush: true)

                // REPROVAR AS FOTOS QUANDO O REGISTRO FOR NÃO ACEITO
                if ( situacao.codigoSistema == 'NAO_ACEITA') {
                    DadosApoio situacaoReprovado = dadosApoioService.getByCodigo('TB_SITUACAO_MULTIMIDIA', 'REPROVADA')
                    fc.multimidias.each { foto ->
                        if( foto.fichaMultimidia.situacao != situacaoReprovado ) {
                            foto.fichaMultimidia.situacao = situacaoReprovado
                            foto.fichaMultimidia.txNaoAceito = 'Foto REPROVADA porque o registro da ocorrência não foi aceito na consolidação da consulta.'
                            foto.fichaMultimidia.aprovadoPor = null
                            foto.fichaMultimidia.dtAprovacao = null
                            foto.fichaMultimidia.reprovadoPor = Pessoa.get(session.sicae.user.sqPessoa.toLong())
                            foto.fichaMultimidia.dtReprovacao = new Date()
                            foto.fichaMultimidia.save(flush:true)
                        }
                    }
                }
            }
            else {
                // colaboração na ficha
                FichaColaboracao fc = FichaColaboracao.get(params.id.toInteger())
                if (!fc) {
                    render 'Id inválido!'
                    return
                }
                fc.situacao = situacao
                if( ! fc.dsColaboracao )
                {
                    fc.dsColaboracao=''
                }
                if( situacao.codigoSistema == 'ACEITA')
                {
                    fc.txNaoAceita = null
                }
                else {
                    fc.txNaoAceita = Util.stripTags(params.txNaoAceita)
                }
                fc.save(flush: true)
            }
        }
        else if( params.fonte=='salve')
        {

            FichaOcorrencia fo = FichaOcorrencia.get(params.id.toInteger())
            if (!fo)
            {
                render 'Id inválido!'
                return
            }
            fo.situacao = situacao
            fo.pessoaRevisor = null
            fo.dtRevisao = null
            fo.inUtilizadoAvaliacao = 'N'
            if( situacao.codigoSistema==~ /ACEITA|NAO_ACEITA/) {
                fo.pessoaRevisor = PessoaFisica.get(session.sicae.user.sqPessoa)
                fo.dtRevisao = new Date()
            }
            if( situacao.codigoSistema == 'ACEITA')
            {
                fo.inUtilizadoAvaliacao = 'S'
            }
            fo.save(flush: true)
        }
        else
        {
            render 'Fonte '+params.fonte+' inválida!'
            return
        }
        // limpar cache
        if( cacheService && params.campoFicha )
        {
            cacheService.clear(params.campoFicha.split('-')[0])
        }
        render ''
    }

    /**
     * alterar a situação dos registros vindos das consulta para Aceito, Nao avaliado ou resolver em oficina
     */
    def updateSituacaoPontoConsulta(){
        Map res=[status:0,msg:'',type:'success', data:[:] ]
        try {
            if ( ! params.sqFichaOcorrenciaConsulta) {
                throw  new Exception('Id não informado!')
            }
            FichaOcorrenciaConsulta foc = FichaOcorrenciaConsulta.get(params.sqFichaOcorrenciaConsulta.toLong())
            if (!foc) {
                throw  new Exception('Id inválido')
            }
            // se a ficha estiver versionada na consulta/revisão não permitir alteracao
            if( foc.cicloConsultaFicha.cicloConsultaFichaVersao ){
                throw  new Exception('Alteração não permitida devido ao versionamento da ficha.')
            }

            DadosApoio novaSituacao = dadosApoioService.getByCodigo('TB_SITUACAO_COLABORACAO',params.value?:'NAO_AVALIADA')
            if( !novaSituacao ){
                throw  new Exception('Situação ' + params.value + ' inválida')
            }
            // por enquanto atualizar as duas situações e depois remove a coluna sq_situacao da tabela ficha_ocorrencia
            foc.fichaOcorrencia.situacao = novaSituacao
            foc.fichaOcorrencia.save()
            foc.situacao = novaSituacao
            foc.save()

        } catch( Exception e ){
            res.type='error'
            res.status=1
            res.msg = e.getMessage()
        }
        render res as JSON

    }

    /**
     * alterar a situacao da colaboração feita nos registros de ocorrencia se Concorda ou Discorda
     * com a utilização do registro na avaliação
     */
    def updateSituacaoSugestao(){

        Map res=[status:0,msg:'',type:'success', data:[:] ]
        try {

            if ( ! params.sqFichaOcorrenciaValidacao) {
                throw  new Exception('Id não informado!')
            }

            FichaOcorrenciaValidacao fov = FichaOcorrenciaValidacao.get(params.sqFichaOcorrenciaValidacao.toLong())
            if (!fov) {
                throw  new Exception('Id inválido')
            }

            // se a ficha estiver versionada na consulta/revisão não permitir alteracao
            if( fov.cicloConsultaFicha.cicloConsultaFichaVersao ){
                throw  new Exception('Alteração não permitida devido ao versionamento da ficha.')
            }

            DadosApoio novaSituacao = dadosApoioService.getByCodigo('TB_SITUACAO_COLABORACAO',params.value?:'NAO_AVALIADA')
            if( !novaSituacao ){
                throw  new Exception('Situação ' + params.value + ' inválida')
            }
            fov.situacao = novaSituacao
            fov.save()

        } catch( Exception e ){
            res.type='error'
            res.status=1
            res.msg = e.getMessage()
        }
        render res as JSON

    }

    /**
     * altera a situação ou gravar o revisor no registro de ocorrencia
     * @return [description]
     */
    def saveRevisor() {
        Map res=[status:1,msg:'',type:'error']

        if( ! params.id )
        {
            res.msg = 'Id não informado!'
            render res as JSON
            return
        }

        if( !params.fonte) // salve, portalbio
        {
            res.msg='Fonte não informada!'
            render res as JSON
            return
        }
        DadosApoio situacaoAceita = dadosApoioService.getByCodigo('TB_SITUACAO_COLABORACAO','ACEITA')
        DadosApoio situacaoNaoAvaliada = dadosApoioService.getByCodigo('TB_SITUACAO_COLABORACAO','NAO_AVALIADA')
        DadosApoio situacaoResolverOficina = dadosApoioService.getByCodigo('TB_SITUACAO_COLABORACAO','RESOLVER_OFICINA')

        FichaOcorrenciaValidacao fov
        FichaOcorrencia fo
        // foi selecionado o sim ou não de algum colaborador
        if( params.sqFichaOcorrenciaValidacao ){
            fov = FichaOcorrenciaValidacao.get(params.sqFichaOcorrenciaValidacao.toInteger() )
        }
        params.fonte = params.fonte.toLowerCase()
        params.id    = params.id.toInteger()
        if( params.fonte == 'portalbio') {
            FichaOcorrenciaPortalbio fop = FichaOcorrenciaPortalbio.get( params.id.toInteger() )
            fop.pessoaRevisor        = null
            fop.dtRevisao            = null
            if( params.value=="RESOLVER_OFICINA") {
                fop.situacao = situacaoResolverOficina
            }  else {
                if( params.value == "ACEITA") {
                    fop.situacao        = situacaoAceita
                    fop.pessoaRevisor   = PessoaFisica.get( session.sicae.user.sqPessoa )
                    fop.dtRevisao       = new Date()
                } else {
                    fop.situacao = situacaoNaoAvaliada
                }

                /*
                fop.inUtilizadoAvaliacao = null
                fop.pessoaRevisor = null
                fop.dtRevisao = null

                if( fov ) {
                    fop.situacao = situacaoAceita

                    // gravar o revisor aqui
                    if( fov.inValido=='S')
                    {
                        fop.inUtilizadoAvaliacao ='S'
                    }
                    else
                    {
                        fop.inUtilizadoAvaliacao ='N'
                    }
                    // gravar o revisor aqui
                    Email email = Email.findByTxEmail( fov.usuario.username )
                    if( email  )
                    {
                        // limpar o revisor e voltar a situação para não validada
                        fop.pessoaRevisor = email.pessoa
                        fop.dtRevisao = new Date()
                        println 'Email encontrado: ' +fov.usuario.username
                    }
                    else
                    {
                        println 'Email não encontrado: ' +fov.usuario.username
                    }
                }*/
            }
            fop.save( flush:true)
        }
        else if( params.contexto == 'consulta' || params.fonte == 'salve'){
            if( params.id != params.idOcorrencia ) {
                try {
                    fo = FichaOcorrenciaConsulta.get(params.id).fichaOcorrencia
                } catch( Exception ) {
                    fo = FichaOcorrencia.get( params.id )
                }
            }
            else if( params.fonte == 'salve') {
               fo = FichaOcorrencia.get( params.id )
            }
            if( fo ) {
                fo.pessoaRevisor = null
                fo.dtRevisao = null
                //fo.inUtilizadoAvaliacao = null
                if( params.value == "ACEITA") {
                    fo.situacao        = situacaoAceita
                    fo.pessoaRevisor   = PessoaFisica.get( session.sicae.user.sqPessoa )
                    fo.dtRevisao       = new Date()
                } else {
                    if( params.value == "RESOLVER_OFICINA") {
                        fo.situacao = situacaoResolverOficina
                    } else {
                        fo.situacao = situacaoNaoAvaliada
                    }
                }

                /*
                fo.situacao = situacaoNaoAvaliada
                if (params.value == "RESOLVER_OFICINA") {
                    fo.situacao = situacaoResolverOficina
                } else if (fov) {
                    fo.situacao = situacaoAceita
                    if (fov.inValido == 'S') {
                        fo.inUtilizadoAvaliacao = 'S'
                    } else {
                        fo.inUtilizadoAvaliacao = 'N'
                    }
                    // gravar o revisor aqui
                    Email email = Email.findByTxEmail(fov.usuario.username)
                    if (email) {
                        // limpar o revisor e voltar a situação para não validada
                        fo.pessoaRevisor = email.pessoa
                        fo.dtRevisao = new Date()
                    }
                }*/
                fo.save(flush: true)
            }
        }
        render res as JSON
    }

    /**
     *  Criar gride com os habitos alimentares
     **/
    def getGridHabitoAlimentar(){
        Ficha ficha = Ficha.get( params.sqFicha )
        if ( ficha )
        {
            List listFichaHabitoAlimentar = FichaHabitoAlimentar.findAllByFicha( ficha )
            List listFichaHabitoAlimentarEsp
            if( ficha.stHabitoAlimentEspecialista=='S')
            {
                listFichaHabitoAlimentarEsp = FichaHabitoAlimentarEsp.findAllByFicha( ficha )
            }
            render(template: 'divGridHabitoAlimentar', model: [ficha:ficha
            													, listFichaHabitoAlimentar: listFichaHabitoAlimentar
                                                             	, listFichaHabitoAlimentarEsp : listFichaHabitoAlimentarEsp
                                                             	])

        }
        else
        {
            render ''
        }
    }

    /**
     *  Criar gride com os habitats
     **/
    def getGridHabitat()
    {
        Ficha ficha = Ficha.get( params.sqFicha )
        if ( ficha )
        {
            List listFichaHabitats = FichaHabitat.findAllByFicha( ficha)
            render(template: 'divGridHabitats', model: [ficha:ficha
                 , listFichaHabitats: listFichaHabitats
                                                        ])
        }
    }

    /**
     *  Criar gride com as iterações
     **/
    def getGridInteracao()
    {
        Ficha ficha = Ficha.get( params.sqFicha )
        if ( ficha )
        {
            List listFichaIteracao = FichaInteracao.findAllByFicha( ficha)
            render(template: 'divGridInteracao', model: [ficha:ficha
                                                       ,listFichaIteracao: listFichaIteracao
                                                           ])
        }
    }
    /**
     *  Criar gride com as ameaças
     **/
    def getGridAmeaca()
    {
        Ficha ficha = Ficha.get( params.sqFicha )
        if ( ficha )
        {
            List listFichaAmeaca = FichaAmeaca.findAllByFicha( ficha )
            render(template: 'divGridAmeaca', model: [ficha:ficha
                                                       ,listFichaAmeaca: listFichaAmeaca
                                                           ])
        }
    }

    def getGridAnexosAmeaca()
    {
        Ficha ficha = Ficha.get( params.sqFicha )
        if ( ficha )
        {
            DadosApoio contexto = dadosApoioService.getByCodigo('TB_CONTEXTO_ANEXO','MAPA_AMEACA');
            List listAnexos = FichaAnexo.findAllByFichaAndContexto(ficha,contexto ,[sort: "dtAnexo",order:'desc']);
            render(template: 'divGridAnexosAmeaca'
                , model     : [ficha:ficha
                , contexto  : 'fichaCompleta'
                , listAnexos: listAnexos
            ])
        }
    }

    def getGridAnexosUso()
    {
        Ficha ficha = Ficha.get( params.sqFicha )
        if ( ficha )
        {
            DadosApoio contexto = dadosApoioService.getByCodigo('TB_CONTEXTO_ANEXO','MAPA_USO');
            List listAnexos = FichaAnexo.findAllByFichaAndContexto(ficha,contexto ,[sort: "dtAnexo",order:'desc']);
            render(template: 'divGridAnexosUso'
                , model     : [ficha:ficha
                , contexto  : 'fichaCompleta'
                , listAnexos: listAnexos
            ])
        }
    }

    def getGridAnexosPopulacao()
    {
        Ficha ficha = Ficha.get( params.sqFicha )
        if ( ficha )
        {
            DadosApoio contexto = dadosApoioService.getByCodigo('TB_CONTEXTO_ANEXO','GRAFICO_POPULACAO');
            List listAnexos = FichaAnexo.findAllByFichaAndContexto(ficha,contexto ,[sort: "dtAnexo",order:'desc']);
            render(template: 'divGridAnexosPopulacao'
                , model     : [ficha:ficha
                , contexto  : 'fichaCompleta'
                , listAnexos: listAnexos
            ])
        }
    }

    /**
     *  Criar gride com as Usos
     **/
    def getGridUso()
    {
        Ficha ficha = Ficha.get( params.sqFicha )
        if ( ficha )
        {
            List listFichaUso = FichaUso.findAllByFicha( ficha )
            render(template: 'divGridUso', model: [ficha:ficha
                                                       ,listFichaUso: listFichaUso
                                                           ])
        }
    }

    /**
     * Criar o gride de histórico de avaliações
     */
    def getHistoricoAvaliacoes()
    {
        Map res = [ultimaAvaliacaoNacional:null,
                   gridHistorico:null,
                   gridAcoes:null,
                   gridUcs:null,
                   gridPesquisas:null,
                   gridAvaliacoes:null ]
        DadosApoio nacionalBrasil = dadosApoioService.getByCodigo('TB_TIPO_AVALIACAO', 'NACIONAL_BRASIL')

        List listAvaliacoes
        // Encontrar a categoria da última avaliação nacional da especie
        VwFicha vwFicha = VwFicha.get( params.sqFicha.toLong() )
        if ( vwFicha )
        {
            res.ultimaAvaliacaoNacional = fichaService.getUltimaAvaliacaoNacionalHist( vwFicha.id.toLong() )

            // ler a avaliação nacional da aba 11.7 se houver
            /*if( ! res.ultimaAvaliacaoNacional.sqCategoriaIucn && vwFicha.sqCategoriaFinal ) {
                Ficha ficha = Ficha.get( params.sqFicha.toLong() )
                res.ultimaAvaliacaoNacional.sqCategoriaIucn = ficha.categoriaFinal.id
                res.ultimaAvaliacaoNacional.coCategoriaIucn = ficha.categoriaFinal.codigoSistema
                res.ultimaAvaliacaoNacional.deCategoriaIucn = ficha.categoriaFinal.descricao
                res.ultimaAvaliacaoNacional.nuAnoAvaliacao =  ficha.cicloAvaliacao.nuAno
                res.ultimaAvaliacaoNacional.txJustificativaAvaliacao = ficha.dsJustificativaFinal
                res.ultimaAvaliacaoNacional.deCriterioAvaliacaoIucn = ficha.dsCriterioAvalIucnFinal
            }*/

            /*listAvaliacoes = FichaHistoricoAvaliacao.createCriteria().list() {
                createAlias('vwFicha', 'f' )
                eq('f.sqTaxon', vwFicha?.sqTaxon?.toInteger() )
                ne('id', res.ultimaAvaliacaoNacional.id.toLong())
                order('nuAnoAvaliacao', 'desc')
                order('id', 'desc')
            }
            */
            //List listFichaHistoricoAvaliacao = FichaHistoricoAvaliacao.findAllByFicha(Ficha.get(params.sqFicha)).sort{it.nuAnoAvaliacao.toString()+it.tipoAvaliacao.ordem}
            listAvaliacoes = TaxonHistoricoAvaliacao.createCriteria().list {
                eq('taxon.id', vwFicha.sqTaxon.toLong())
                if( res.ultimaAvaliacaoNacional) {
                    ne('id', res.ultimaAvaliacaoNacional.id.toLong())
                }
                or {
                    lt('nuAnoAvaliacao', (vwFicha.cicloAvaliacao.nuAno))
                    and {
                        le('nuAnoAvaliacao', (vwFicha.cicloAvaliacao.nuAno + 4))
                        ne('tipoAvaliacao', nacionalBrasil)
                    }
                }
                tipoAvaliacao {
                    order('ordem')
                }
                order('nuAnoAvaliacao')
            }
        }

        // historico das avaliações
        res.gridHistorico =  g.render(template:'divGridHistoricoAvaliacao', model:[sqFicha:params.sqFicha, listAvaliacoes : listAvaliacoes])

        List lista
        // presença em convenções
        lista = FichaListaConvencao.findAllByVwFicha(vwFicha)
        res.gridConvencoes =  g.render(template:'divGridConvencoes', model:[sqFicha:params.sqFicha, listFichaListaConvencao : lista])

        // ações de conservação
        lista = FichaAcaoConservacao.findAllByVwFicha(vwFicha)
        res.gridAcoes =  g.render(template:'divGridAcoesConservacao', model:[listFichaAcaoConservacao: lista])

        // presença em UC
        lista = fichaService.getUcsOcorrencia(vwFicha.id.toLong())
        /*lista = FichaOcorrencia.createCriteria().list
        {
            eq('ficha.id', ficha.id)
            eq('contexto', DadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA', 'REGISTRO_OCORRENCIA'))
            and {
                or {
                    isNotNull('sqUcFederal')
                    isNotNull('sqUcEstadual')
                    isNotNull('sqRppn')
                }
            }
        }.unique { it?.ucHtml }
        */

        res.gridUcs =  g.render(template:'divGridUcs', model:[listFichaOcorrencia: lista])
        render res as JSON
    }

    def getGridPesquisa()
    {
        Ficha ficha = Ficha.get( params.sqFicha )
        if ( ficha )
        {

            List lista = FichaPesquisa.findAllByFicha(ficha)
            render(template: 'divGridPesquisas', model: [ficha:ficha, listFichaPesquisa: lista])
            return
        }
        render ''
    }

    def getRefBibs()
    {
        Ficha ficha = Ficha.get( params.sqFicha )
        List listFichaRefBib = []
        List listFichaRefBibPortalbio = []

        if ( ficha )
        {
            // Referencias Bibliograficas
            List lista = FichaRefBib.createCriteria().list() {
                eq('ficha', ficha)
                isNotNull('publicacao') // não incluir a comunicação pessoal
            }.unique { it.publicacao?.noAutor }.each {
                if( it.noTabela == 'ficha_ocorrencia_portalbio') {
                    listFichaRefBibPortalbio.push(it.referenciaHtml)
                } else {
                    listFichaRefBib.push( it.referenciaHtml )
                }
            }
            // colocar as referencias em ordem alfabética
            listFichaRefBib.sort{ stu1, stu2 -> Util.removeAccents(stu1).compareToIgnoreCase( Util.removeAccents( stu2 ) ) }
            listFichaRefBibPortalbio.sort{ stu1, stu2 -> Util.removeAccents(stu1).compareToIgnoreCase( Util.removeAccents( stu2) ) }
            render(template: 'divGridRefBibs', model: [ficha:ficha,
                                                       listFichaRefBib: listFichaRefBib,
                                                       'listFichaRefBibPortalbio': listFichaRefBibPortalbio ])
            return
        }
        render ''
    }

    def getPendencias()
    {
        Ficha ficha = Ficha.get( params.sqFicha )
        if ( ficha )
        {
            // Referencias Bibliograficas
            List lista = FichaPendencia.createCriteria().list() {
                eq('ficha', ficha)
                eq('stPendente','S')
            }.sort { it.dtPendencia }
            render(template: '/ficha/pendencia/divGridPendencias', model: [listPendencias: lista])
            return
        }
        render ''
    }

    def consolidarFicha()
    {
        Map res=[status:1,msg:'',type:'error',descricao:'']
        DadosApoio novaSituacao
        Ficha ficha = Ficha.get( params.sqFicha )
        if( ficha )
        {
            if( ficha.situacaoFicha.codigoSistema == 'AVALIADA_EM_REVISAO')
            {
                novaSituacao = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'AVALIADA_REVISADA')
                res.msg='Ficha avalidada e revisada com SUCESSO!'
                if( ! novaSituacao )
                {
                    res.msg='Situação AVALIADA_REVISADA não cadastraa na tabela de apoio TB_FICHA_SITUACAO!'
                    render res as JSON
                    return
                }
            }
            else {
                novaSituacao = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'CONSOLIDADA')
                res.msg='Ficha consolidada com SUCESSO!'
                if( ! novaSituacao )
                {
                    res.msg='Situação CONSOLIDADA não cadastraa na tabela de apoio TB_FICHA_SITUACAO!'
                    render res as JSON
                    return
                }
            }
            List erros = fichaService.canModifySituacao(ficha, novaSituacao )
            if( ! erros )
            {
                ficha.situacaoFicha = novaSituacao
                ficha.save( flus:true)
                res.type='success'
                res.descricao = novaSituacao.descricao
                res.status=0
                cacheService.clear('gerenciarConsulta-getGridConsolidar')
            }
            else {
                res.msg = erros.join('<br>')
            }

            /*
            // ficha não pode estar em consulta
            List listConsultas = CicloConsultaFicha.createCriteria().list{
                eq('ficha',ficha)
                cicloConsulta {
                    ge('dtFim', Util.hoje())
                }
            }
            if( listConsultas.size() > 0 )
            {
                res.msg = 'Ficha está no período de consulta!'
                listConsultas.each{
                    res.msg += '<br/>' + it.cicloConsulta.tipoConsulta.descricao+' - '+it.cicloConsulta.deTituloConsulta +'. Período: '+ it.cicloConsulta.periodoText
                }
                render res as JSON
                return
            }

            // não pode haver nenhuma colaboração com situação NAO_AVALIADA
            DadosApoio situacaoNaoAvaliada = dadosApoioService.getByCodigo('TB_SITUACAO_COLABORACAO', 'NAO_AVALIADA')
            //DadosApoio situacaoResolver    = dadosApoioService.getByCodigo('TB_SITUACAO_COLABORACAO', 'RESOLVER_OFICINA')
            Integer qtdColaboracoesPendentes = FichaColaboracao.createCriteria().count {
                consultaFicha {
                    eq('ficha',ficha)
                }
                eq('situacao', situacaoNaoAvaliada)
            }
            Integer qtdOcorrenciasPendentes = FichaOcorrenciaConsulta.createCriteria().count {
                cicloConsultaFicha {
                    eq('ficha',ficha)
                }
                fichaOcorrencia {
                    eq('situacao', situacaoNaoAvaliada)
                }
            }
            List msg=[]
            if( qtdColaboracoesPendentes > 0 )
            {
                msg.push('Total de ' + qtdColaboracoesPendentes+' colaborações não avaliadas!')
            }
            if( qtdOcorrenciasPendentes > 0 )
            {
                msg.push('Total de ' + qtdOcorrenciasPendentes+' colaborações de registro de ocorrência não avaliadas!')
            }

            if( msg )
            {
                res.msg = 'Para consolidar a ficha não pode haver colaboração não avaliada!<br>'+msg.join('<br/>')
            }
            else
            {
                DadosApoio novaSituacao
                if( ficha.situacaoFicha.codigoSistema == 'AVALIADA_EM_REVISAO')
                {
                    novaSituacao = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'AVALIADA_REVISADA')
                    res.msg='Ficha avalidada e revisada com SUCESSO!'
                }
                else {
                    novaSituacao = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'CONSOLIDADA')
                    res.msg='Ficha consolidada com SUCESSO!'
                }
                if( novaSituacao )
                {
                    ficha.situacaoFicha = novaSituacao
                    ficha.save( flus:true)
                    res.type='success'
                    res.descricao = novaSituacao.descricao
                    res.status=0
                    cacheService.clear('gerenciarConsulta-getGridConsolidar');
                }
                else
                {
                    res.msg='Situação CONSOLIDADA não cadastraa na tabela de apoio TB_FICHA_SITUACAO!'
                }


            }
             */
        }
        render res as JSON
    }

    /**
     * quando a oficina estiver encerrada os dados do ajuste regional poderão
     * ser alterados sozinhos
     */
    def saveAjusteRegional() {
        if ( ! params.sqFicha )
        {
            return this._returnAjax(1, 'ID da ficha não informado.', "error")
        }
        if ( ! params.sqOficinaFicha )
        {
            return this._returnAjax(1, 'ID da Oficinaficha não informado.', "error")
        }

        Ficha ficha = Ficha.get( params.sqFicha.toLong() )
        if( !ficha )
        {
            return this._returnAjax(1, 'Ficha inválida.', "error")
        }

        if ( ! ficha.canModify(session.sicae.user))
        {
            return this._returnAjax(1, 'Alteração não permitida.', "error")
        }

        OficinaFicha oficinaFicha = OficinaFicha.get( params.sqOficinaFicha.toLong() )
        if( ! oficinaFicha )
        {
            return this._returnAjax(1, 'OficinaFicha inválida.', "error")
        }

        // gravar campos do form ajuste regional
        if( params.sqTipoConectividade )
        {
            ficha.tipoConectividade = DadosApoio.get( params.sqTipoConectividade.toLong() )
        }
        else {
            ficha.tipoConectividade = null
        }
        // tendencia Imigração
        if( params.sqTendenciaImigracao )
        {
            ficha.tendenciaImigracao = DadosApoio.get( params.sqTendenciaImigracao.toLong() )
        }
        else {
            ficha.tendenciaImigracao = null
        }
        // Populacao Brasileira pode declinar
        ficha.stDeclinioBrPopulExterior = params.stDeclinioBrPopulExterior?:null
        ficha.dsConectividadePopExterior = params.dsConectividadePopExterior?:null
        ficha.save( flush:true )

        return this._returnAjax(0, 'Dados gravados com SUCESSO!.', "success")


    }

    /**
     * Salvar apenas a justificativa durante a oficina de avaliação
     */
    def saveJustificativaOficina(){
        Map res = [msg:'',status:0, type:'success']
        try {
            res.msg='Dados gravados com SUCESSO'
            if( params.sqOficinaFicha ) {
                OficinaFicha oficinaFicha = OficinaFicha.get( params.sqOficinaFicha.toLong())
                if( ! oficinaFicha || oficinaFicha.vwFicha.id.toLong() != params.sqFicha.toLong() ){
                    throw new Exception('Oficina inválida');
                }
                oficinaFicha.dsJustificativa = params.dsJustificativa
                oficinaFicha.save()
            }
        } catch( Exception e ){
            res.status=1
            res.msg=e.getMessage()
            res.type='error'
        }
        render res as JSON
    }
    /**
     * Salvar o resultado da oficina quando a ficha completa estiver sendo visualizada no módulo Oficina->Execução
     * @return
     */
    def saveResultadoAvaliacao() {
        String cmdSql

        if ( ! session?.sicae?.user) {
            return this._returnAjax(1, 'Sessão expirada. Efetue login novamente.', "info")
        }

        if ( ! params.sqFicha )
        {
            return this._returnAjax(1, 'ID da ficha não informado.', "error")
        }
        if ( ! params.sqOficinaFicha )
        {
            return this._returnAjax(1, 'ID da Oficinaficha não informado.', "error")
        }
        DadosApoio categoria
        String novaSituacao = ''
        Ficha ficha = Ficha.get( params.sqFicha.toLong() )
        if( !ficha )
        {
            return this._returnAjax(1, 'Ficha inválida.', "error")
        }

        if ( ! ficha.canModify(session.sicae.user)) {
            return this._returnAjax(1, 'Alteração não permitida.', "error")
        }

        OficinaFicha oficinaFicha = OficinaFicha.get( params.sqOficinaFicha.toLong() )
        if( ! oficinaFicha )
        {
            return this._returnAjax(1, 'OficinaFicha inválida.', "error")
        }

        if( oficinaFicha.oficinaFichaVersao ){
            return this._returnAjax(1, 'Ficha está vesionada nesta oficina.', "error")
        }
        // validar se não existem colaboracoes pendentes de aceitacao na ficha
        Integer qtdPendencias = 0
        cmdSql ="""with passo1 as (
                select situacao.cd_sistema
                    from salve.ciclo_consulta_ficha ccf
                    inner join salve.ficha_colaboracao fc on fc.sq_ciclo_consulta_ficha = ccf.sq_ciclo_consulta_ficha
                    inner join salve.dados_apoio as situacao on situacao.sq_dados_apoio = fc.sq_situacao
                    left outer join salve.ciclo_consulta_ficha_versao ccfv on ccfv.sq_ciclo_consulta_ficha = ccf.sq_ciclo_consulta_ficha
                    where ccf.sq_ficha = ${params.sqFicha}
                    and ccfv.sq_ciclo_consulta_ficha is null
                    and situacao.cd_sistema in ( 'NAO_AVALIADA','RESOLVER_OFICINA')
                UNION

                SELECT situacao.cd_sistema
                FROM salve.ficha_ocorrencia_consulta foc
                         INNER JOIN salve.ficha_ocorrencia fo ON fo.sq_ficha_ocorrencia = foc.sq_ficha_ocorrencia
                         INNER JOIN salve.dados_apoio AS situacao ON situacao.sq_dados_apoio = foc.sq_situacao
                         LEFT OUTER JOIN salve.ciclo_consulta_ficha_versao ccfv ON ccfv.sq_ciclo_consulta_ficha = foc.sq_ciclo_consulta_ficha
                WHERE fo.sq_ficha = ${params.sqFicha}
                  AND ccfv.sq_ciclo_consulta_ficha IS NULL
                  and situacao.cd_sistema in ( 'NAO_AVALIADA','RESOLVER_OFICINA')

                UNION

                SELECT situacao.cd_sistema
                FROM salve.ficha_ocorrencia_validacao fov
                         INNER JOIN salve.ciclo_consulta_ficha ccf ON ccf.sq_ciclo_consulta_ficha = fov.sq_ciclo_consulta_ficha
                         INNER JOIN salve.dados_apoio AS situacao ON situacao.sq_dados_apoio = fov.sq_situacao
                         LEFT OUTER JOIN salve.ficha_ocorrencia_validacao_versao fovv ON fovv.sq_ficha_ocorrencia_validacao = fov.sq_ficha_ocorrencia_validacao
                WHERE ccf.sq_ficha = ${params.sqFicha}
                  AND fovv.sq_ficha_ocorrencia_validacao IS NULL
                  and situacao.cd_sistema in ( 'NAO_AVALIADA','RESOLVER_OFICINA')
                )
                select count(*) as nu_pendencia from passo1
        """

        /** /
        println ' '
        println ' '
        println ' '
        println cmdSql
         /**/

        sqlService.execSql(cmdSql).each{row->
            qtdPendencias += row.nu_pendencia
        }
        if( qtdPendencias ){
            String mensagem = qtdPendencias == 1 ? 'Existe 1 colaboração não resolvida.':'Existem ' + qtdPendencias.toString() + ' colaborações não resolvidas.'
            return this._returnAjax(1, mensagem, "error")
        }

        if( params.sqCategoriaIucn )
        {
            if ( ! params.stPossivelmenteExtinta )
            {
                params.stPossivelmenteExtinta = null
            }
            categoria = DadosApoio.get( params.sqCategoriaIucn.toInteger() )
            if (categoria.codigoSistema == 'VU' || categoria.codigoSistema == 'CR' || categoria.codigoSistema == 'EN')
            {
                if ( ! params.dsCriterioAvalIucn )
                {
                    return this._returnAjax(1, 'Informe o critério', "error")
                }
            } else
            {
                params.dsCriterioAvalIucn = null
            }

            if ( categoria.codigoSistema != 'CR' )
            {
                params.stPossivelmenteExtinta = null
            }
            else
            {
                if( ! params.stPossivelmenteExtinta ) {
                    params.stPossivelmenteExtinta = 'N'
                }
            }

            if( params.avaliadaRevisada == "S" )
            {
                novaSituacao  = 'AVALIADA_REVISADA'
            }
            else
            {
                novaSituacao = 'POS_OFICINA'
            }
            params.situacaoFicha = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', novaSituacao)
            if( !params.situacaoFicha )
            {
                return this._returnAjax(1, 'Situação '+novaSituacao+' não cadastrada na tabela de apoio TB_SITUACAO_FICHA.', "error")
            }
        }
        else {
            // se não informou a categoria é porque limpou o resultado da oficina
            params.dsJustificativa = null
            params.categoriaIucn   = null
            params.dsCriterioAvalIucn = null
            params.dsJustMudancaCategoria = null
            params.stPossivelmenteExtinta = null
            params.situacaoFicha = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'CONSOLIDADA')
        }

        // validar mudanca de categoria
        Map dadosUltimaAvaliacao = ficha.ultimaAvaliacaoNacionalHist
        if ( categoria
                && categoria.codigoSistema != 'NE'
                && dadosUltimaAvaliacao.coCategoriaIucn != 'NE'
                && dadosUltimaAvaliacao.sqCategoriaIucn
                && dadosUltimaAvaliacao.sqCategoriaIucn.toLong() != categoria.id.toLong() )
        {
            if( FichaMudancaCategoria.countByFicha( ficha ) == 0 )
            {
                return this._returnAjax(1, 'Motivo da mudança de categoria deve ser informado.', "error")
            }
            ficha.dsJustMudancaCategoria = params.dsJustMudancaCategoria
        }
        else
        {
            // verificar se houve mudanca de categoria na Validação, se não houver pode excluir os motivos
            if( ! categoria || categoria.codigoSistema == 'NE' || ! ficha.categoriaFinal || ! dadosUltimaAvaliacao.sqCategoriaIucn || dadosUltimaAvaliacao.sqCategoriaIucn.toLong() == ficha.categoriaFinal.id.toLong() ) {
                ficha.dsJustMudancaCategoria = null
                FichaMudancaCategoria.findAllByFicha(ficha).each { it.delete() }
            }
        }

        // gravar resultado da avaliacao ( oficina )
        ficha.categoriaIucn 		= categoria
        ficha.dsJustificativa 		= params.dsJustificativa
        ficha.dsCriterioAvalIucn	= params.dsCriterioAvalIucn
        ficha.stPossivelmenteExtinta= params.stPossivelmenteExtinta
        ficha.situacaoFicha         = params.situacaoFicha

        // campos do form ajuste regional
        if( params.sqTipoConectividade )
        {
            ficha.tipoConectividade = DadosApoio.get( params.sqTipoConectividade.toLong() )
        }
        else {
            ficha.tipoConectividade = null
        }
        // tendencia Imigração
        if( params.sqTendenciaImigracao )
        {
            ficha.tendenciaImigracao = DadosApoio.get( params.sqTendenciaImigracao.toLong() )
        }
        else {
            ficha.tendenciaImigracao = null
        }
        // Populacao Brasileira pode declinar
        ficha.stDeclinioBrPopulExterior = params.stDeclinioBrPopulExterior?:null
        ficha.dsConectividadePopExterior = params.dsConectividadePopExterior?:null

        // gravar  copia do resultado na tabela oficina ficha
        oficinaFicha.situacaoFicha = ficha.situacaoFicha
        oficinaFicha.categoriaIucn = ficha.categoriaIucn
        oficinaFicha.dsJustificativa= ficha.dsJustificativa
        oficinaFicha.dsCriterioAvalIucn = ficha.dsCriterioAvalIucn
        oficinaFicha.stPossivelmenteExtinta = ficha.stPossivelmenteExtinta
        if( ficha.categoriaIucn )
        {
            oficinaFicha.dtAvaliacao = new Date()
        }
        else
        {
            oficinaFicha.dtAvaliacao = null
        }

        oficinaFicha.save( flush:true)
        return this._returnAjax(0, getMsg(1), "success")
    }


    /**
     * Adicionar motivo da mudança quando a ficha completa estiver sendo visualizada no módulo oficina->execucao
     * @return
     */
    def addMotivoMudanca()
    {
        if( !params.sqFicha )
        {
            return this._returnAjax(1, 'ID da ficha não informado.', "error")
        }

        if( !params.sqMotivoMudanca )
        {
            return this._returnAjax(1, 'Selecione um motivo de mudança de categoria!', "error")
        }
        FichaMudancaCategoria reg
        DadosApoio motivoMudanca = DadosApoio.get(params.sqMotivoMudanca)
        if( params.sqFichaMudancaCategoria )
        {
            reg = FichaMudancaCategoria.get( params.sqFichaMudancaCategoria)
        }
        else
        {
            reg = new FichaMudancaCategoria()
            reg.ficha 			= Ficha.get( params.sqFicha )
            reg.motivoMudanca   = new DadosApoio()
        }
        reg.properties 		= params
        // validar duplicidade
        Integer qtd = FichaMudancaCategoria.countByFichaAndMotivoMudanca(reg.ficha,motivoMudanca);
        if( qtd > 0 && motivoMudanca != reg.motivoMudanca)
        {
            this._returnAjax(1, getMsg(3), "error")
            return
        }
        reg.motivoMudanca = motivoMudanca
        try {
            reg.save(flush:true)
            this._returnAjax(0, getMsg(1), "success")
        } catch(Exception e) {
            println e.getMessage()
            this._returnAjax(1, getMsg(2), "error")
        }
    }

    /**
     * Atualizar o gride com os motivos da mudança de categoria quando a ficha completa estiver sendo visualizada no módulo oficina->execucao
     */
    def getGridMotivoMudanca()
    {
        if( params.canModify == null )
        {
            params.canMofidy = 'false'
        }
        if( params.canModify.toString() == 'false' || ! params.canModify )
        {
            params.canModify=false;
        }
        else
        {
            params.canModify=true;
        }
        if( !params.sqFicha )
        {
            render 'ID da ficha não informado.'
        }
        Ficha ficha = Ficha.get( params.sqFicha.toLong() )
        List listaMotivoMudanca = FichaMudancaCategoria.findAllByFicha( ficha )
        render(template:"/oficina/divGridMotivoMudanca",
                model:['listaMotivoMudanca':listaMotivoMudanca
                       ,ficha:ficha
                       ,canModify:params.canModify])
    }

    /**
     * Editar o gride com os motivos da mudança de categoria quando a ficha completa estiver sendo visualizada no módulo oficina->execucao
     * @return
     */
    def editMotivoMudanca()
    {
        FichaMudancaCategoria reg = FichaMudancaCategoria.get(params.id)
        if( reg?.id )
        {
            render reg.asJson()
            return
        }
        render [:] as JSON
    }

    /**
     * Excluir motivo da mudança de categoria quando a ficha completa estiver sendo visualizada no módulo oficina->execucao
     * @return
     */
    def deleteMotivoMudanca() {
        Map res = [status: 1, msg: '', type: 'error']
        if (!params.id) {
            res.msg = 'id inválido!'
            render res as JSON
            return
        }
        if (params?.container ==~ /.?tabAvaResultado.*/ && !session.sicae.user.isADM()) {
            res.msg = 'Operação não permitida para seu perfil!'
            render res as JSON
            return
        }

        try {
            FichaMudancaCategoria reg = FichaMudancaCategoria.get(params.id.toLong())
            if (reg) {
                if (reg.ficha.situacaoFicha.codigoSistema == 'PUBLICADA') {
                    int qtd = FichaMudancaCategoria.createCriteria().count {
                        eq('ficha', reg.ficha)
                    }
                    if (qtd < 2) {
                        res.status = 1
                        res.msg = 'Ficha está PUBLICADA!'
                        res.type = 'error'
                        reg = null;
                    }
                }
                if (reg) {
                    reg.delete(flush: true)
                    res.status = 0
                    res.msg = 'Registro excluído com SUCESSO!'
                    res.type = 'success'
                }
            }
        } catch (Exception e) {
        }
        render res as JSON
    }

    /**
    * Atualizar o campo texto da ficha
    */
    def updateTextoFicha()
    {
        Map res = [status: 1, msg: '', errors: [], type: 'error']
        /*res.msg = 'ver parametros!'
        render res as JSON
        return
        */
        if (params.sqFicha && params.campoFicha)
        {
            Ficha ficha = Ficha.get(params.sqFicha)
            if (ficha)
            {
                try
                {
                    // SALVE SEM CICLO
                    if( params.campoFicha =='dsCitacao'){
                        params.dsTextoAlterado = params.dsTextoAlterado.replaceAll(/<\/p>/,'</p>#CRLF#')
                        params.dsTextoAlterado = Util.stripTags(params.dsTextoAlterado)
                        params.dsTextoAlterado = params.dsTextoAlterado.replaceAll(/#CRLF# ?/,'\n')

                    }
                    ficha[params.campoFicha] = params.dsTextoAlterado
                    ficha.save(flush: true)
                    res.status = 0
                    res.type = 'success'
                    res.msg = 'Gravado com SUCESSO!'
                } catch (Exception e) {
                    println e.getMessage()
                    res.status = 1
                    res.msg = 'Campo ' + params.campoFicha + ' inválido!'
                }
            }
        } else
        {

            if (!params.sqFichaColaboracao)
            {
                res.msg = 'Id não informado!'
                render res as JSON
                return
            }

            if (!params.dsTextoAlterado)
            {
                res.msg = 'Texto não informado!'
                render res as JSON
                return
            }


            FichaColaboracao fc = FichaColaboracao.get(params.sqFichaColaboracao.toInteger())
            if (!fc)
            {
                res.msg = 'Id inválido!'
                render res as JSON
                return

            }
            if (!session?.sicae?.user?.isADM() && !session?.sicae?.user?.isPF())
            {
                res.msg = 'Operação não permitida!'
            } else
            {
                res.status = 0
                res.type = 'success'
                res.msg = 'Dados gravados com SUCESSO!'
                try
                {
                    fc.consultaFicha.ficha[fc.noCampoFicha] = params.dsTextoAlterado
                    fc.consultaFicha.ficha.save(flush: true)
                }
                catch (Exception e) {
                    res.status = 1
                    res.type = 'error'
                    res.msg = 'Campo ' + fc.noCampoFicha + ' não existe!'
                }
            }
        }
        render res as JSON
    }

    def getFormPendencia() {
        FichaPendencia fichaPendencia

        // no contexto "validador" só pode ter uma pendencia por validador em cada ficha
        /*if( params.contexto && params.contexto.toLowerCase() == 'validador' )
        {
            fichaPendencia = FichaPendencia.findByVwFichaAndUsuarioAndContexto(
              VwFicha.get( params.sqFicha )
              ,PessoaFisica.get( session.sicae.user.sqPessoa )
              ,dadosApoioService.getByCodigo('TB_CONTEXTO_PENDENCIA','VALIDACAO') )
        }
        */

        if( params.sqFichaPendencia ){
            params.id = params.sqFichaPendencia
        }

        if ( params.id ) {
            fichaPendencia = FichaPendencia.get(params.id.toInteger())
        }else {
            fichaPendencia = new FichaPendencia()
        }

        render( template:'formPendencia',model:[fichaPendencia:fichaPendencia,sqFichas:params.sqFichas, contexto:params.contexto])
    }



    /**
     * Validação
     */
    def saveResultadoValidador() {
        Map res=[status:1,msg:'',type:'error',errors:[]]
        DadosApoio resultado = null
        DadosApoio categoriaSugerida = null
        PessoaFisica validador = null
        Ficha ficha

        if( ! session?.sicae?.user?.sqPessoa ){
            res.msg = 'Sessão expirada. Efetue login novamente!'
            render res as JSON
            return
        }

        if( !params.sqFicha )
        {
            res.errors.push('Id da ficha não informado!')
        }
        else if( !params.sqOficinaFicha )
        {
            res.errors.push('Id da Ofician/ficha não informado!')
        }

        ficha = Ficha.get( params.sqFicha.toInteger() )
        if ( ! ficha ) {
            res.msg = 'Ficha não cadastrada'
            render res as JSON
            return
        }

        if ( ficha.situacaoFicha.codigoSistema == 'PUBLICADA' ) {
            res.msg = 'Operação não permitida. A ficha já está publicada.'
            render res as JSON
            return
        }

        if ( ficha.situacaoFicha.codigoSistema == 'FINALIZADA' ) {
            res.msg = 'Operação não permitida. A ficha já está finalizada.'
            render res as JSON
            return
        }

        // verificar se oficina ainda está aberta
        OficinaFicha oficinaFicha = OficinaFicha.get( params.sqOficinaFicha.toLong() )
        if( !oficinaFicha )
        {
            res.errors.push('Oficina não encontrada!')
        }
        if( oficinaFicha.oficina.situacao.codigoSistema != 'ABERTA' ) {
            res.errors.push('Oficina está ' + oficinaFicha.oficina.situacao.codigoSistema)
        }

        /*
        // se a ficha tiver sido validada devido aos validadores responderem iguais, então permitir alteração por eles
        else if ( ficha.situacaoFicha.codigoSistema == 'VALIDADA' )
        {
            res.errors.push('Ficha está validada!')
        }
        */
        if( ! res.errors )
        {
            // validar campos em função do resultado selecionado
            if ( params.sqResultado )
            {
                resultado = DadosApoio.get(params.sqResultado.toInteger() )
            }
            if (!resultado && params.sqResultado)
            {
                res.errors.push('Resultado inválido!')
            }
            else
            {
                if( resultado )
                {
                    if (resultado.codigoSistema == 'RESULTADO_A')
                    {
                        params.sqCategoriaSugerida = null
                        params.deCriterioSugerido = null
                        //params.dsJustificativa = null
                    } else if (resultado.codigoSistema == 'RESULTADO_B') {
                        params.sqCategoriaSugerida = null
                        params.deCriterioSugerido = null
                        if (!params.dsJustificativa)
                        {
                            res.errors.push('Necessário informar a fundamentação!')
                        }
                    } else if (resultado.codigoSistema == 'RESULTADO_C') {
                        if (!params.sqCategoriaSugerida)
                        {
                            res.errors.push('Necessário informar a categoria!')
                        } else
                        {
                            categoriaSugerida = DadosApoio.get(params.sqCategoriaSugerida.toInteger())
                            if (!categoriaSugerida)
                            {
                                res.errors.add('Categoria não cadastrada!');
                            }
                        }


                        if ( ! params.deCriterioSugerido && ("EN|VU|CR").indexOf(categoriaSugerida.codigoSistema) > -1 )
                        {
                            res.errors.push('Necessário informar a critério!')
                        }

                        if( categoriaSugerida.codigoSistema != 'CR' || ! params?.stPossivelmventeExtinta )
                        {
                            params.stPossivelmventeExtinta=null
                        }
                        if( categoriaSugerida.codigoSistema == 'CR' && params?.stPossivelmventeExtinta != 'S' )
                        {
                            params.stPossivelmventeExtinta='N'
                        }

                        if (!params.dsJustificativa)
                        {
                            res.errors.push('Necessário informar a fundamentação!')
                        }
                    }
                }
                if(res.errors.size() == 0 )
                {
                    validador = PessoaFisica.get( session?.sicae?.user?.sqPessoa?.toInteger())
                    if ( ! validador ) {
                        res.errors.push('Validador não cadastrado no ICMBIO!')
                    } else  {
                        // gravar
                        ValidadorFicha validadorFicha = this.getValidadorFicha(validador, params.sqOficinaFicha.toLong() )
                        if ( ! validadorFicha )
                        {
                            res.errors.push('Ficha não cadastrada para o validador logado!')
                        } else {
                            if( resultado )
                            {
                                validadorFicha.resultado = resultado
                                validadorFicha.categoriaSugerida = categoriaSugerida
                                validadorFicha.deCriterioSugerido = params.deCriterioSugerido
                                validadorFicha.txJustificativa = params.dsJustificativa
                                validadorFicha.stPossivelmenteExtinta=params.stPossivelmenteExtinta
                            }
                            else {
                                validadorFicha.resultado = null
                                validadorFicha.categoriaSugerida = null
                                validadorFicha.deCriterioSugerido = null
                                validadorFicha.txJustificativa = null
                                validadorFicha.stPossivelmenteExtinta=null
                            }
                            if( validadorFicha.save() ) {
                                res.status=0
                                res.msg=getMsg(1)
                                res.type='success'
                                // atualizar o resultado final se for A-A ou B-A
                                Map resultados = validadorFicha.getResultadosFicha()

                                if( resultados.qtdValidadores > 1 )
                                {

                                    if( resultados.qtdA > 1 ) // os dois foram A -  avaliação automática
                                    {
                                        // finalizar valiação gravando o resultado da aba 11.6 na aba 11.7
                                        ficha.categoriaFinal            = ficha.categoriaIucn
                                        ficha.dsCriterioAvalIucnFinal   = ficha.dsCriterioAvalIucn
                                        ficha.dsJustificativaFinal      = ficha.dsJustificativa
                                        ficha.stPossivelmenteExtintaFinal = ficha.stPossivelmenteExtinta
                                        // se a ficha tiver pendência passar para VALIDADA_EM_REVISAO
                                        VwFicha vwFicha = VwFicha.get( ficha.id )
                                        if( vwFicha.nuPendencia == 0 ) {
                                            ficha.situacaoFicha = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'VALIDADA')
                                        }
                                        else
                                        {
                                            ficha.situacaoFicha = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'VALIDADA_EM_REVISAO')
                                        }
                                        ficha.dtAceiteValidacao    = new Date()
                                        oficinaFicha.dtAvaliacao   = ficha.dtAceiteValidacao
                                        oficinaFicha.situacaoFicha = ficha.situacaoFicha
                                        oficinaFicha.categoriaIucn = ficha.categoriaFinal
                                        oficinaFicha.dsCriterioAvalIucn = ficha.dsCriterioAvalIucnFinal
                                        oficinaFicha.dsJustificativa = ficha.dsJustificativaFinal
                                        oficinaFicha.stPossivelmenteExtinta = ficha.stPossivelmenteExtintaFinal

                                        oficinaFicha.save()
                                        ficha.save(flush:true)
                                    }
                                    else if( resultados.qtdB > 1 || (resultados.qtdB==1 && resultados.qtdA == 1 ) ) // foi BB ou AB
                                    {
                                        // grava categoria e critério mas não finaliza
                                        ficha.categoriaFinal            = ficha.categoriaIucn
                                        ficha.dsCriterioAvalIucnFinal   = ficha.dsCriterioAvalIucn
                                        ficha.stPossivelmenteExtintaFinal = ficha.stPossivelmenteExtinta
                                        ficha.dsJustificativaFinal      = null
                                        ficha.situacaoFicha             = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'VALIDADA_EM_REVISAO')
                                        ficha.dtAceiteValidacao         = new Date() // x104851x
                                        oficinaFicha.dtAvaliacao        = ficha.dtAceiteValidacao
                                        oficinaFicha.situacaoFicha      = ficha.situacaoFicha
                                        oficinaFicha.categoriaIucn      = ficha.categoriaFinal
                                        oficinaFicha.dsCriterioAvalIucn = ficha.dsCriterioAvalIucnFinal
                                        oficinaFicha.dsJustificativa        = ficha.dsJustificativaFinal
                                        oficinaFicha.stPossivelmenteExtinta = ficha.stPossivelmenteExtintaFinal
                                        oficinaFicha.save()
                                        ficha.save(flush:true)
                                    }
                                    // sempre que um validador salvar tem que limpar a validação final se tiver preenchida
                                    else //if( resultados.qtdC > 0 ) // pelo menos 1 foi C
                                    {
                                        ficha.categoriaFinal                = null
                                        ficha.dsCriterioAvalIucnFinal       = null
                                        ficha.dsJustificativaFinal          = null
                                        ficha.stPossivelmenteExtintaFinal   = null
                                        ficha.situacaoFicha                 = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'EM_VALIDACAO')
                                        ficha.dtAceiteValidacao             = null
                                        oficinaFicha.dtAvaliacao            = null
                                        oficinaFicha.situacaoFicha          = ficha.situacaoFicha
                                        oficinaFicha.categoriaIucn          = ficha.categoriaFinal
                                        oficinaFicha.dsCriterioAvalIucn     = ficha.dsCriterioAvalIucnFinal
                                        oficinaFicha.dsJustificativa        = ficha.dsJustificativaFinal
                                        oficinaFicha.stPossivelmenteExtinta = ficha.stPossivelmenteExtintaFinal

                                        // se 2 responderam C e houve consenso na categoria e criterio, gravar a data da validacao na ficha
                                        /*
                                        if( resultados.qtdC > 1  ){
                                            int qtd = ValidadorFicha.createCriteria().count{
                                                eq('oficinaFicha.id', oficinaFicha.id )
                                                cicloAvaliacaoValidador{
                                                    ne('validador', validador )
                                                }
                                                if( validadorFicha.categoriaSugerida ) {
                                                    eq('categoriaSugerida', validadorFicha.categoriaSugerida)
                                                } else {
                                                    isNull( 'categoriaSugerida');
                                                }
                                                if( validadorFicha.deCriterioSugerido ) {
                                                    eq('deCriterioSugerido', validadorFicha.deCriterioSugerido)
                                                } else {
                                                    or {
                                                        isNull('deCriterioSugerido')
                                                        eq('deCriterioSugerido','')
                                                    }
                                                }
                                            }
                                            if( qtd > 0 ) {
                                                oficinaFicha.dtAvaliacao = new Date()
                                            }
                                        }
                                        */
                                        oficinaFicha.save()
                                        ficha.save(flush: true)
                                    }
                                }
                                // enviar os resultados outros validadores de volta para exibir para o validador
                                res.data = [ resultados:[] ]
                                ValidadorFicha.createCriteria().list {
                                    eq('oficinaFicha.id', oficinaFicha.id )
                                    cicloAvaliacaoValidador{
                                        ne('validador',validador)
                                    }
                                }.each{ it ->
                                    res.data.resultados.push( [ sqValidadorFicha:it.id
                                        ,'dsResultado'              :(validadorFicha.resultado ? (it.resultado?.descricao ?: '') :'')
                                        ,'dsCategoriaSugerida'      :(validadorFicha.resultado ? (it.categoriaSugerida?.descricao ?:'') :'')
                                        ,'deCriterioSugerido'       :(validadorFicha.resultado ? (it.deCriterioSugerido ?:'') :'')
                                        ,'stPossivelmenteExtinta'   :(validadorFicha.resultado ? (it.stPossivelmenteExtinta ?: ''):'')
                                        ,'dePossivelmenteExtinta'   :(validadorFicha.resultado ? (it.stPossivelmenteExtinta ? Util.snd(it.stPossivelmenteExtinta) : ''):'')
                                        ,'txJustificativa'          :(validadorFicha.resultado ? (it.txJustificativa ?:'') :'')
                                    ])
                                }
                            }
                            else
                            {
                                validadorFicha.errors.allErrors.each {
                                    res.errors.push( messageSource.getMessage(it, locale ) );
                                }
                            }
                        }
                        // atualizar a situacao da ficha na oficina
                        oficinaFicha.situacaoFicha = ficha.situacaoFicha
                        oficinaFicha.save(flush:true)
                        /*println '-'*80
                        println '-'*80
                        println '-'*80
                        println 'OFICINA FICHA'
                        println 'Situacao ficha:' + oficinaFicha.situacaoFicha.descricao*/


                        // se a ficha tiver sido validada verificar regra para exibição no modulo público
                        // sem histórico ou histórico = LC, DD, NA ou NT + aba 11.7 LC, DD, NA ou NT
                        if( ficha.dtAceiteValidacao ){
                            // ler categoria da última avaliação nacional ( histórico )
                            TaxonHistoricoAvaliacao hist = TaxonHistoricoAvaliacao.createCriteria().get {
                                eq('taxon.id',ficha.taxon.id)
                                eq('tipoAvaliacao.id',311l)
                                order('nuAnoAvaliacao','desc')
                                maxResults(1)
                            }
                            if( ( ! hist || (hist.categoriaIucn.codigoSistema =~ /LC|DD|NA|NT/ ) )
                                && ficha.categoriaFinal
                                && (ficha.categoriaFinal.codigoSistema =~ /LC|DD|NA|NT/) ) {
                                fichaService.marcarDesmarcarRegistroExibirModuloPublico(ficha.id.toString(),true ,0 )
                            }
                        } else {
                            // se o validador alterou a resposta e a ficha deixou de estar validada, remover da tabela ficha_modulo_publico
                            // o registro que foi criado automaticamente pela validação dos validadores
                           sqlService.execSql("delete from salve.ficha_modulo_publico where sq_ficha = ${ficha.id.toString() } and sq_pessoa is null and st_exibir=true")
                        }

                    }
                }
            }
        }
        else
        {
            res.msg = res.errors.join('<br/>')
        }
        render res as JSON
    }

    def saveChatValidacao() {
       Map res = [status:1,msg:'',type:'error',lastUpdate:'']

/*
        println ' '
        println 'SALVE CHAT'
        println 'Usuario:'+session.sicae.user.noPessoa
        println 'sqPessoa:'+session.sicae.user.sqPessoa
        res.msg ='Testando CHAT'
        render res as JSON
        return;
*/

       /** /
       println params
       render res as JSON
       return
       /**/
       //List listValidadores = []
       //ValidadorFicha validadorFicha
       OficinaFichaChat oficinaFichaChat

       if( ! params.sqOficinaFicha )
       {
            res.msg='Oficina/Ficha não informada!'
       }
       else
       {
           if( params.txChat.trim() )
           {
               PessoaFisica usuario  = PessoaFisica.get(session?.sicae?.user?.sqPessoa?.toInteger())
               if (! usuario )
               {
                    res.msg='usuario não cadastrado no ICMBIO ou Sessão expirada!'
               }
               else
               {
                   oficinaFichaChat = new OficinaFichaChat()
                   oficinaFichaChat.oficinaFicha = OficinaFicha.get( params.sqOficinaFicha.toLong() )
                   oficinaFichaChat.pessoa = usuario
                   oficinaFichaChat.dtMensagem = new Date()
                   oficinaFichaChat.deMensagem = params.txChat.trim()
                   if( session?.sicae?.user?.isPF() ) {
                       oficinaFichaChat.sgPerfil = 'PF'
                   }
                   else if( session?.sicae?.user?.isVL() ) {
                       oficinaFichaChat.sgPerfil = 'VL'
                   }
                   else if( session?.sicae?.user?.isADM() ) {
                       oficinaFichaChat.sgPerfil = 'AD'
                   }
                   else
                   {
                       oficinaFichaChat.sgPerfil = '--'
                   }
                   if( !oficinaFichaChat.save(flush:true))
                   {
                       println oficinaFichaChat.getErrors()
                       res.msg='Erro ao salvar chat'
                   }
                   else
                   {
                       res.type='success'
                       res.msg=''
                       res.status=0
                   }
                   sleep(1000) // para o worker não repetir o chat na tela
                   res.lastUpdate = new Date().format('dd/MM/yyyy HH:mm:ss')

                   //Ficha ficha  = Ficha.get( params.sqFicha.toInteger() )
                    /*if( params.contexto =='validador')
                    {
                        listValidadores.push( this.getValidadorFicha( usuario , params.sqOficinaFicha.toLong()) )
                    }
                    else if( params.contexto == 'validacao' )
                    {
                        listValidadores.push( ValidadorFicha.findByOficinaFicha( OficinaFicha.get( params.sqOficinaFicha.toLong() ) ) )

//                        listValidadores = ValidadorFicha.createCriteria().list {
//                            cicloAvaliacaoValidador{
//                                eq('situacaoConvite', dadosApoioService.getByCodigo('TB_SITUACAO_CONVITE_PESSOA','CONVITE_ACEITO') )
//                            }
//                            eq('oficinaFicha.id', params.sqOficinaFicha.toLong() )
//                        }
                    }
                    */

                    /*listValidadores.each
                    {
                        ValidadorFichaChat chat = new ValidadorFichaChat()
                        chat.validadorFicha = it
                        chat.pessoa = usuario
                        chat.deMensagem = params.txChat
                        chat.dtMensagem = new Date()
                        if( session?.sicae?.user?.isPF() ) {
                            chat.sgPerfil = 'PF'
                        }
                        else if( session?.sicae?.user?.isVL() ) {
                            chat.sgPerfil = 'VL'
                        }
                        else if( session?.sicae?.user?.isADM() ) {
                            chat.sgPerfil = 'AD'
                        }
                        else
                        {
                            chat.sgPerfil = '--'
                        }
                        chat.validate()
                        chat.save( flush:true )
                        res.status=0
                        res.msg=''
                        res.type='success'
                        sleep(1000) // para o worker não repetir o chat na tela
                        res.lastUpdate = new Date().format('dd/MM/yyyy HH:mm:ss')

                        // resetar necessidade de comunicação
                        if( params.contexto == 'validador' )
                        {
                            it.dtUltimoComunicado=null
                            it.inComunicarAlteracao=false
                            it.save( flush:true )
                        }
                        else if( params.contexto == 'validacao')
                        {
                            it.dtUltimoComunicado = null
                            it.inComunicarAlteracao = true
                            it.save( flush:true)
                        }
                    }
                    */
                }

            }
       }
       render res as JSON
    }

    /**
     * Habilitar/Convidar o ponto focal ou coordenador de taxon a participar do chat com os validadores
     * @return
     */
    def convidarChat()
    {
        Map res = [msg:'',type:'error',status:1]
        DadosApoio papel
        ValidadorFicha vf = ValidadorFicha.get( params.sqValidadorFicha.toInteger() )
        VwFicha vwFicha
        //String emailFrom = grailsApplication.config.email.from
        try {
            vwFicha = VwFicha.get( params.sqFicha.toLong() )
            if (vwFicha && vf && params.codigo == 'CT') {
                papel = dadosApoioService.getByCodigo('TB_PAPEL_FICHA','COORDENADOR_TAXON')
            }else if (vwFicha && vf && params.codigo == 'PF') {
                papel = dadosApoioService.getByCodigo('TB_PAPEL_FICHA','PONTO_FOCAL')
            }
            else
            {
                throw new RuntimeException('Código inválido!')
            }
            // enviar email para o ct/pf
            FichaPessoa fp = FichaPessoa.createCriteria().get {
                eq('inAtivo','S')
                eq('papel', papel)
                eq('vwFicha.id',vwFicha.id )
                maxResults(1)
            }
            if( !fp )
            {
                throw new RuntimeException('Ficha não esta com '+ papel.descricao +' atribuido e ativo.')
            }

            res.status=0
            res.msg='Convite enviado com SUCESSO!'
            String taxon = Util.ncItalico(vwFicha.nmCientifico)
            String email = fp.pessoa.email
            String nome  = Util.capitalize( fp.pessoa.noPessoa )

            //email = 'salve@icmbio.gov.br'
            //email = 'eugeniob@tba.com.br'

            if( taxon && email )
            {
                String mensagem="""<p style="font-size:18px;font-family:Times New Roman, Arial">Prezado(a) <b>${nome}</b>,<br>
                Você está convidado(a) a participar do bate-papo referente a validação do taxon <b>${taxon}</b>.
                <br>Para isso, acesse o <a href="${this.baseUrl}" target="_blank">SALVE</a> no menu "Gerenciar", opção "Validação".
                <br><br>Atenciosamente,<br>
                Equipe SALVE.
                </p>
                """
                if( emailService.sendTo(nome+' <'+email+'>','Convite bate-papo com validadores',mensagem) )
                {
                    if (params.codigo == 'CT') {
                        vf.oficinaFicha.stCtConvidado = true
                        vf.oficinaFicha.save(flush: true)
                    }else if (params.codigo == 'PF') {
                        vf.oficinaFicha.stPfConvidado = true
                        vf.oficinaFicha.save(flush: true)
                    }
                    else
                    {
                        throw new RuntimeException('Código inválido!')
                    }
                    res.type='success'
                    res.status=0
                }
                else
                {
                    throw new RuntimeException('Erro enviando email!')
                }
            }
        } catch( Exception e)
        {
            res.status=1
            res.type='error'
            res.error = e.getMessage()
        }
        render res as JSON
    }

    def getGridChats()
    {
        String dateTimeFormat = "dd/MM/yyyy HH:mm:ss"
        Long lastId = 0;
        Map resultado = [htmlChat:''
                         , cdSituacaoFichaSistema:''
                         , sugestoes:[]
                         , ctConvidado:true
                         , pfConvidado:true
                         , exibirBotaoEnviar:true
                         , lastUpdate:new Date().format(dateTimeFormat)
                         , lastId : lastId
                         , error  :'']

        if( !params.sqOficinaFicha )
        {
            resultado.error = 'Oficina Ficha não informada'
            render resultado as JSON
            return
        }

        // continuar a partir do último id ja enviado
        if( params.lastId ) {
            lastId = params.lastId.toLong()
        }

        // map de retorno com o html do chat e os resultados dos validadores
        resultado.lastId = lastId

        VwFicha vwFicha  = VwFicha.get( params.sqFicha.toInteger() )

        // se a ficha tiver VALIDADA não permitir alteração do resultado final
        resultado.cdSituacaoFichaSistema = vwFicha.cdSituacaoFichaSistema
        PessoaFisica usuario = PessoaFisica.get( session?.sicae?.user?.sqPessoa?.toInteger() )
        List listChats = OficinaFichaChat.createCriteria().list {
            eq('oficinaFicha.id', params.sqOficinaFicha.toLong() )
            gt('id',lastId.toLong())
            order('id')
        }

        if( listChats.size() > 0 ) {
            resultado.lastId = listChats.last().id
        }

        // ler os resultados para atualizar online as respostas dos validadores na tela do chat
        Map nomesValidadores = [:]
        if( ! params.contexto || params.contexto != 'validador' ) {
            ValidadorFicha.createCriteria().list {
                eq('oficinaFicha.id',params.sqOficinaFicha.toLong() )
                isNotNull('resultado')
                cicloAvaliacaoValidador {
                    order('id', 'asc')
                }
                /*if( params.contexto =='validador' ) {
                    cicloAvaliacaoValidador {
                        ne('validador.id',session.sicae.user.sqPessoa.toLong() )
                    }
                }*/
            }.eachWithIndex { it, index ->
                resultado.sugestoes.push(
                        [  sqValidadorFicha        : it.id
                         , sqResultado           : it?.resultado?.id
                         , coResultado           : it?.resultado?.codigoSistema
                         , dsResultado           : it?.resultado?.descricao ?: ''
                         , sqCategoriaSugerida   : it?.categoriaSugerida?.id ?: ''
                         , dsCategoriaSugerida   : it?.categoriaSugerida?.descricaoCompleta ?: ''
                         , dsCriterioSugerido    : it?.deCriterioSugerido ?: ''
                         , stPossivelmenteExtinta: it?.stPossivelmenteExtinta == 'S' ? '(PEX)' : ''
                         , txJustificativa       : it?.txJustificativa ?: ''])

                // não exibir o nome dos validadores na tela
                nomesValidadores[it.cicloAvaliacaoValidador.validador.id.toString()] = [ nome: Util.nomeAbreviado( it.cicloAvaliacaoValidador.validador.noPessoa ), numero: ( index + 1 ) +'º' ]
            }
        } else if( params.contexto =='validador'){
            //println ' '
            //println 'CHAT CONTEXTO VALIDADOR ' +  session?.sicae?.user?.sqPessoa.toInteger()
        }

        // hashcode das respostas dos Validadores para evitar enviar os texto em todas as requisições
        resultado.hashRespostas = 'hash-respostas' + resultado.sugestoes.hashCode()
        if( params.hashRespostas && resultado.hashRespostas == params.hashRespostas ){
            resultado.sugestoes=[]
        }

        // Equipe salve pediu para liberar o chat para PF E CT em 08/11/2018
        resultado.pfConvidado = true
        resultado.ctConvidado = true

        resultado.htmlChat = g.render(template :'/fichaCompleta/divGridChats'
                        , model:[ listChats:listChats
                        //, nomesValidadores: nomesValidadores // mostrar os nomes nos chats
                        , usuario:usuario
                        , contexto:params.contexto ?:''
                    ])
        // limpar espaços e comentários do HTML
        resultado.htmlChat = resultado.htmlChat.trim().replaceAll(/<!--(.+)-->/,'').replaceAll(/>\s+</,'><').replaceAll(/\n/,'').trim()
        //resultado.lastUpdate = new Date().format(dateTimeFormat)
        render resultado as JSON
    }

    def deleteChat()
    {
        Map res=[lastUpdate:'', type:'success', msg:'', status:0 ]
        if( !params.sqChat)
        {
            render res as JSON
            return
        }

        ValidadorFichaChat reg = ValidadorFichaChat.get( params.sqChat.toInteger())
        if( reg )
        {
            if( reg.pessoa.id.toInteger() == session?.sicae?.user?.sqPessoa.toInteger() )
            {
                reg.delete( flush:true)
                sleep(1000) // para o worker não repetir o chat na tela
                res.lastUpdate = new Date().format('dd/MM/yyyy HH:mm:ss')
            }
            else
            {
                res.msg = '<h3>Mensagem pertence a '+reg.pessoa.noPessoa+'!</h3>'
                res.type = 'error'
            }
        }
        render res as JSON
    }


    /**
     * Enviar um email para todos os participantes da validação solicitando a contribuição no bate-papo da validação da ficha
     * @return
     */
    def chamarAtencaoTodosChat() {
        Map res = [msg: 'ver paremetros. Testando', type: 'error', status: 1]
        VwFicha vwFicha
        List listWarning = []
        List listEmail = []
        List listEnvioOk = []
        List listEnvioErro = []
        String email
        params.listCts = params.listCts ?: ''
        params.listVls = params.listVls ? params.listVls.split(',').collect{ it.toLong() } : []
        try {
            if( ! session?.sicae?.user?.sqPessoa )
            {
                throw new RuntimeException('Sessão expirada, faça login novamente.')
            }
            if( !params.sqFicha )
            {
                throw new RuntimeException('Ficha não informada.')
            }
            vwFicha = VwFicha.get( params.sqFicha.toLong() )
            if( !vwFicha )
            {
                throw new RuntimeException('Ficha inválida.')
            }

            // enviar email para o usuário logado
            listEmail.push([ email: session.sicae.user.email
                             , nome: Util.capitalize( session.sicae.user.noPessoa ) + ' (<b>'+session.sicae.user.email + '</b>)'
                             , nomeCompleto: Util.capitalize( session.sicae.user.noPessoa )
                             , papel:''  ] )

            log.info(' ')
            log.info('Iniciando envio de email para PF,CT e Validadores da especie '+ vwFicha.nmCientifico);
            log.info('  - sqOficinaFicha:' + params.sqOficinaFicha.toLong())

            boolean enviadoparaValidador=false
            boolean enviadoparaCoordenadorTaxon=false
            boolean enviadoparaPontoFocal=false

                // encontrar email dos validadores
            log.info('  - Encontrando validadores...')
            ValidadorFicha.executeQuery("""select new map(b.deEmail as deEmail, c.noPessoa as noPessoa, c.id as sqPessoa ) from ValidadorFicha a
                join a.cicloAvaliacaoValidador b
                join b.validador c
                where a.oficinaFicha.id = :sqOficinaFicha
                and b.situacaoConvite.codigoSistema = :coSituacaoConvite
                """, [sqOficinaFicha: params.sqOficinaFicha.toLong(), coSituacaoConvite: 'CONVITE_ACEITO'])
                .each {
                if( it.deEmail ) {
                    email = it.deEmail.toLowerCase()
                     if( !params.listVls || params.listVls.contains(it.sqPessoa.toLong()) ) {
                         if (!listEmail.find { it.email == email }) {
                             listEmail.push([email         : email
                                             , nome        : Util.capitalize(it.noPessoa) + ' (<b>' + email + '</b>)'
                                             , nomeCompleto: Util.capitalize(it.noPessoa)
                                             , papel       : 'VL'])
                             enviadoparaValidador = true // encontrou pelo menos um validador
                             log.info('  - Email validador: ' + email)
                         }
                     }
                } else {
                    if( session.sicae.user.isADM() ) {
                        listWarning.push('Validador ' + Util.capitalize(it.noPessoa) + ' está sem email!')
                        log.info('  -  Validador '+ it.pessoa.noPessoa + ' está sem email!' )
                    }
                    else
                    {
                        listWarning.push('Validador está sem email!')
                        log.info('  - Validador está sem email!')
                    }
                }
            }

            // encontrar o CT
            log.info('  - Encontrando o Coordenador de Taxon')
            DadosApoio papelCT = dadosApoioService.getByCodigo('TB_PAPEL_FICHA', 'COORDENADOR_TAXON')
            FichaPessoa.createCriteria().list {
                eq('inAtivo', 'S')
                eq('papel', papelCT)
                eq('vwFicha.id', vwFicha.id)
                if( params.listCts ){
                    'in'('pessoa.id', params.listCts.split(',').collect{it.toLong() } )
                }
            }.each {
                // evitar envio para a mesma pessoa logada
                //if( it.pessoa.id != session.sicae.user.sqPessoa ) {
                if( true ) {
                    if (it?.pessoa?.email) {
                        email = it.pessoa.email.toLowerCase()
                        if( ! listEmail.find { it.email == email } ) {
                            listEmail.push([  email: email
                                            , nome: Util.capitalize(it.pessoa.noPessoa) + ' (<b>'+email+'</b>)'
                                            , nomeCompleto: Util.capitalize(it.pessoa.noPessoa)
                                            , papel:'CT' ])
                            enviadoparaCoordenadorTaxon=true // encontrou um CT
                        }
                        log.info('    - Email CT: ' + email)
                    }else {
                        if( session?.sicae?.user?.isADM() ) {
                            listWarning.push(Util.capitalize('CT ' + it.pessoa.noPessoa) + ' está sem email!')
                            log.info('    - CT '+ it.pessoa.noPessoa + ' está sem email!' )
                        }
                        else
                        {
                            listWarning.push('Coordenador de taxon está sem email!')
                            log.info('    - Coordenador de taxon está sem email!' )
                        }
                    }
                }
            }

            // encontrar o PF
            log.info('  - encontrando Ponto Focal')
            DadosApoio papelPF = dadosApoioService.getByCodigo('TB_PAPEL_FICHA', 'PONTO_FOCAL')
            FichaPessoa.createCriteria().get {
                eq('inAtivo', 'S')
                eq('papel', papelPF)
                eq('vwFicha.id', vwFicha.id)
                maxResults(1)
            }.each {
                // evitar envio para a mesma pessoa logada
                //if( it?.pessoa?.id != session.sicae.user.sqPessoa ) {
                if( true ) {
                    if ( it?.pessoa?.email) {
                        email = it.pessoa.email.toLowerCase()
                        if( ! listEmail.find { it.email == email } ) {
                            listEmail.push( [email: email
                                             , nome: Util.capitalize(it.pessoa.noPessoa) + ' (<b>' + email + '</b>)'
                                             , nomeCompleto: Util.capitalize(it.pessoa.noPessoa)
                                             , papel:'PF'] )
                            enviadoparaPontoFocal=true // encontrou o ponto focal
                            log.info('    - ' + email)
                        }
                    } else {
                        if( session?.sicae?.user?.isADM() ) {
                            listWarning.push(Util.capitalize('PF ' + it.pessoa.noPessoa) + ' está sem email!')
                            log.info('    - PF: ' + Util.capitalize('PF ' + it.pessoa.noPessoa) + ' está sem email!')
                        }
                        else
                        {
                            listWarning.push( 'Ponto focal está sem email!')
                            log.info('     - Ponto focal está sem email!')
                        }
                    }
                }
            }


            /*println listEmail
            render res as JSON
            return;*/
            String nomeCientifico = vwFicha.taxon.noCientifico
            if( params.txEmail ) {
                params.txEmail = '<p>Espécie: <b><i>' + nomeCientifico + '</i></b></p><p>' + params.txEmail + '</p>'
            } else {
                params.txEmail = '<p>Prezado(a) {nome},<br>houve alteração no bate-papo da validação do táxon {taxon}.</p><p>Estamos solicitando sua contribuição.</p>'
            }
            /*params.txEmail = params.txEmail
                ? '<p>' + params.txEmail + '</p>'
                : '<p>Prezado(a) {nome},<br>houve alteração no bate-papo da validação do táxon {taxon}.</p><p>Estamos solicitando sua contribuição.</p>'
            */
            params.txEmail = '<p style="font-size:18px;font-family:Times New Roman, Arial">' + params.txEmail + '</p>'

            listEmail.eachWithIndex { it, i ->
                String nome = it.nome
                String taxon = Util.ncItalico(vwFicha.nmCientifico,false)
                String mensagem = params.txEmail.replaceAll(/\{nome\}/, '<b>'+nome+'</b>')
                mensagem = mensagem.replaceAll(/\n/, '<br>')
                mensagem = mensagem.replaceAll(/\{taxon\}/, '<b>'+taxon+'</b>')
                mensagem += """<p>Servidores do ICMBio clique <a href="${this.urlSicae}" target="_blank">AQUI</a> para acessar.<p>Demais usuários externos clique <a href="${this.urlSicaeExterno}" target="_blank">AQUI</a> para acessar.<br><br><br>Atenciosamente,<br>Equipe SALVE.</p>"""

                if ( ! emailService.sendTo( it.nome + ' <' + it.email + '>', 'Convite bate-papo validação ' + nomeCientifico, mensagem) ) {
                    //listWarning.push('Erro enviando email para ' + it.nome )
                    if( session.sicae.user.isADM() ) {
                        listEnvioErro.push(it.nome)
                    }
                    else
                    {
                        listEnvioErro.push((i+1)+'ª pessoa.')
                    }
                }
                else {
                    //listWarning.push('Email enviado com sucesso para ' + it.nome)
                    listEnvioOk.push(it.nome)
                }
            }
            res.status = 0
            res.type='modal'
            res.msg=''
            if( listWarning )
            {
                res.msg='Avisos<br>'+listWarning.join('<br>')
            }
            else
            {
                if( listEnvioOk ) {
                    res.msg = "<b>Comunicar todos finalizado.</b><br>Notificação automática enviada via e-mail para:"
                    if( params.contexto == 'validador') {
                        if( listEnvioOk.size() > 0 ) {
                            res.msg += """
                                - Validador: ${enviadoparaValidador ? '<b>Sim</b>' : 'Não'}
                                - Coordenador de Taxon: ${enviadoparaCoordenadorTaxon ? '<b>Sim</b>':'Não'}
                                - Ponto Focal: ${enviadoparaPontoFocal ? '<b>Sim</b>':'Não'}
                            """
                        }
                    } else {
                        listEmail.each {
                            res.msg += '<br>- ' + ( it.papel ? it.papel + ' - ' : '') + Util.nomeAbreviado( it.nomeCompleto )+' (<b>' + it.email + '</b>)'
                        }
                    }
                }
                if( listEnvioErro ) {
                    res.msg += (res.msg?'<br><br>':'')+'<span class="red">ERRO no envio para:<br>' + listEnvioErro.join('<br>- ')+'</span>'
                }
            }
        } catch( Exception e ) {
            println e.getMessage()
            res.msg = e.getMessage()
        }
        log.info('Fim comunicar todos (chat)');
        log.info(' ')
        render res as JSON
    }

    /**
     * Enviar um email para o outro validador solicitando a contribuição no bate-papo da validação da ficha
     * @return
     */

    def chamarAtencaoChat() {
        Map res = [msg: '', type: 'error', status: 1]
        VwFicha vwFicha
        String taxon = ''
        String email = ''
        String nome  = ''

        try {
            vwFicha = VwFicha.get(params.sqFicha.toLong())
            List regs = []
            if (params.sqValidador.toLong() > 0l) {
                // encontrar o outro validador
                regs = ValidadorFicha.executeQuery("""select new map(b.deEmail as deEmail, c.noPessoa as noPessoa ) from ValidadorFicha a
                join a.cicloAvaliacaoValidador b
                join b.validador c
                where a.id <> :id and a.oficinaFicha.id = :sqOficinaFicha
                """, [id              : params.sqValidadorFicha.toLong()
                      , sqOficinaFicha: params.sqOficinaFicha.toLong()])
            } else {
                regs = ValidadorFicha.executeQuery("""select new map(b.deEmail as deEmail, c.noPessoa as noPessoa ) from ValidadorFicha a
                join a.cicloAvaliacaoValidador b
                join b.validador c
                where a.oficinaFicha.id = :sqOficinaFicha
                """, [sqOficinaFicha: params.sqOficinaFicha.toLong()])
            }

            if (!regs) {
                throw new RuntimeException('Validador não encontrado.')
            }
            taxon = Util.ncItalico(vwFicha.nmCientifico)
            regs.each {
                email = it.deEmail.toLowerCase()
                nome = Util.capitalize(it.noPessoa)
                if (taxon && email) {
                    String mensagem = """<p style="font-size:18px;font-family:Times New Roman, Arial">Um dos validadores solicita sua contribuição no bate-papo da espécie ${taxon}.<br><br>Clique <a href="${this.baseUrl}" target="_blank">AQUI</a> para acessar o SALVE".
                            <br><br>Atenciosamente,<br>
                            Equipe SALVE.
                            </p>
                            """
                    if (emailService.sendTo(nome + ' <' + email + '>', 'Convite bate-papo validação espécie', mensagem)) {
                        res.type = 'success'
                        res.msg = 'Email enviado com SUCESSO!'
                        res.status = 0
                    } else {
                        throw new RuntimeException('Erro enviando email!')
                    }
                }
            }
        } catch (Exception e) {
            res.error = e.getMessage()
        }
        render res as JSON
    }

    // MODULO DE AVALIAÇÃO REGIONAL
    def saveAvaliacaoRegional()
    {
        Map res = [status:1,msg:'Teste',type:'error',errors:[]]
        //render res as JSON
        //return
        if( !params.sqFicha )
        {
            res.errors.push('Id da ficha não informado!')
        }
        if( !params.sqCategoriaIucn)
        {
            res.errors.push('Categoria não informada!')
        }
        if( !params.nuAnoAvaliacao)
        {
            params.nuAnoAvaliacao = new Date().format('YYYY').toString()
            //res.errors.push('Ano da avaliação não informado!')
        }

        if( params.sqTipoAbrangencia && !params.sqAbrangencia)
        {
            DadosApoio tipoAbrangencia = DadosApoio.get( params.sqTipoAbrangencia.toLong() )
            if( tipoAbrangencia.codigoSistema != 'OUTRA' ) {
                res.errors.push('Abrangência não selecionada!')
            }
        }
        DadosApoio categoria = DadosApoio.get(params.sqCategoriaIucn.toInteger() )

        if (categoria?.codigoSistema == 'VU' || categoria?.codigoSistema == 'CR' || categoria?.codigoSistema == 'EN')
        {
            if (!params.dsCriterioAvalIucn)
            {
                res.errors.push('Critério avaliação não informado!')
            }
        }
        if ( categoria?.codigoSistema != 'CR' )
        {
            params.stPossivelmenteExtinta = null
        }
        else
        {
            params.stPossivelmenteExtinta = ( params.stPossivelmenteExtinta == 'S' ? 'S' : 'N' )
        }

        FichaAvaliacaoRegional reg
        Ficha ficha = Ficha.get( params.sqFicha.toInteger() )
        if( res.errors.size() == 0 )
        {
            if (params.sqFichaAvaliacaoRegional)
            {
                reg = FichaAvaliacaoRegional.get(params.sqFichaAvaliacaoRegional.toInteger())
            } else
            {
                reg = new FichaAvaliacaoRegional()
                reg.ficha = ficha
            }
            if (params.sqAbrangencia)
            {
                reg.abrangencia = Abrangencia.get(params.sqAbrangencia.toInteger())
            } else
            {
                reg.abrangencia = null
            }
            reg.nuAnoAvaliacao = params.nuAnoAvaliacao.toInteger()
            reg.txJustificativaAvaliacao = params.dsJustificativa  ?: null
            reg.deCriterioAvaliacaoIucn  = params.dsCriterioAvalIucn        ?: null
            //reg.deCriterioAvaliacaoRegional = params.deCriterioAvalRegional ?: null
            reg.stPossivelmenteExtinta   = params.stPossivelmenteExtinta    ?: null
            reg.categoriaIucn = categoria
            if (reg.save(flush: true))
            {
                res.status = 0
                res.msg = getMsg(1)
                res.type = 'success'
            } else
            {
                reg.errors.allErrors.each {
                    res.errors.push(messageSource.getMessage(it, locale));  // locale=Locale.getDefault() no baseController
                }
            }
        }
        if( res.errors )
        {
            res.msg = res.errors.join('<br/>')
        }
        render res as JSON
    }

    def getGridAvaliacaoRegional()
    {
        if( !params.sqFicha )
        {
            render ''
        }
        //params.canModify = params.canModify ? params.canModify.asBoolean() : params.canModify
        params.canModify = true; // alterado para que os dados da avaliação regional possa ser alterado a qualquer momento
        Ficha ficha = Ficha.get( params.sqFicha.toInteger() )
        List listFichaAvaliacaoRegional = FichaAvaliacaoRegional.findAllByFicha(ficha)
        render( template:'/fichaCompleta/divGridAvaliacoesRegionais',model:[listFichaAvaliacaoRegional:listFichaAvaliacaoRegional,canModify:params.canModify])
    }

    def editAvaliacaoRegional()
    {
        if (!params?.id) {
            return [:] as JSON
        }
        FichaAvaliacaoRegional reg = FichaAvaliacaoRegional.get( params.id.toInteger() )
        if (reg?.id) {
            render reg.asJson()
            return
        }
        render [:] as JSON
    }

    def deleteAvaliacaoRegional()
    {
        if( params.id )
        {
            FichaAvaliacaoRegional.get( params.id.toInteger()).delete( flush:true )
        }
        render ''
    }

    def getCitacao() {
        if( ! params.sqFicha ) {
            render 'Parâmetros insufientes para gerar a citação'
            return
        }
        OficinaFicha oficinaFicha=null
        if( ! params.sqOficinaFicha ) {

            /*select oficina.* from salve.oficina_ficha
            inner join salve.oficina on oficina.sq_oficina = oficina_ficha.sq_oficina
            where oficina_ficha.sq_ficha = 36967 and oficina.sq_tipo_oficina = 613
            order by oficina.dt_fim desc
            limit 1*/
            oficinaFicha = OficinaFicha.createCriteria().get {
                eq('ficha.id',params.sqFicha.toLong() )
                oficina {
                    eq('tipoOficina', dadosApoioService.getByCodigo('TB_TIPO_OFICINA','OFICINA_AVALIACAO') )
                    order('dtFim','desc')
                }
                maxResults(1)
            }
        } else {
            oficinaFicha = OficinaFicha.get( params.sqOficinaFicha.toLong() )
        }

        if( ! oficinaFicha ) {
            render 'Avaliação não encontrada!'
            return
        }

        Ficha ficha = oficinaFicha.ficha // Ficha.get( params.sqFicha.toLong() )
        DadosApoio papel = dadosApoioService.getByCodigo('TB_PAPEL_FICHA','COORDENADOR_TAXON')
        FichaPessoa ct = FichaPessoa.findByFichaAndPapelAndInAtivo( ficha, papel, 'S')
        if( !ct )
        {
            render 'Coordenador de taxon não definido no módulo gerenciar função.'
            return;
        }

        List nomeCoordenadorTaxon = []
        nomeCoordenadorTaxon.push( nome2citacao( ct?.pessoa?.noPessoa.toString() ) )

        // ler os Avaliadores
        DadosApoio papelAvaliador  = dadosApoioService.getByCodigo('TB_PAPEL_OFICINA','AVALIADOR')
        DadosApoio conviteRecusado = dadosApoioService.getByCodigo('TB_SITUACAO_CONVITE_PESSOA','CONVITE_RECUSADO')
        List nomesAvaliadores = []
        sqlService.execSql("""SELECT distinct pessoa.no_pessoa from salve.oficina_participante a
                    inner join salve.oficina_ficha_participante b on b.sq_oficina_participante = a.sq_oficina_participante
                    inner join salve.oficina_ficha c on c.sq_oficina_ficha = b.sq_oficina_ficha
                    inner join salve.pessoa as pessoa on pessoa.sq_pessoa = a.sq_pessoa
                    where b.sq_papel_participante = ${papelAvaliador.id.toString()}
                    and a.sq_situacao  <> ${conviteRecusado.id.toString() }
                    and c.sq_ficha = ${params.sqFicha.toString()}
                    """).each {
            String nomeCitacao = nome2citacao( it?.no_pessoa )
            if( ! nomesAvaliadores.contains(nomeCitacao) && ! nomeCoordenadorTaxon.contains( nomeCitacao ) ) {
                nomesAvaliadores.push( nomeCitacao )
            }
        }
        nomesAvaliadores = nomesAvaliadores.sort()
        List allNomes = nomeCoordenadorTaxon + nomesAvaliadores

        /*
        List listOficinaParticipante = oficinaParticipante.createCriteria().list {

            oficinaFichaParticipante {
                eq('papel',papel)

                oficinaFicha {
                    eq('ficha', ficha)
                }
            }
        }
        */

        /*
            List listOficinaFichaParticipante = OficinaFichaParticipante.createCriteria().list {
            eq('papel', papel )
            eq('oficinaFicha',oficinaFicha)
            oficinaParticipante{
                pessoa {
                    order('noPessoa')
                }
            }
        }
        listOficinaFichaParticipante.each {
            nomes.push( nome2citacao( it?.oficinaParticipante?.pessoa?.noPessoa ) )
        }*/

        String citacao = allNomes.join('; ')
        if( ficha && ! ficha.dsCitacao ) {
            ficha.dsCitacao = citacao;
            ficha.save()
        }


        render citacao.trim()+' '+new Date().format('yyyy')+' '+ficha.noCientifico
    }

    def getAutoria() {
        Map res = [ status:0,msg:'', type:'success', autoria:'']
        try {
            Boolean updateFicha = params.updateFicha ? true : false
            res.autoria = fichaService.getAutoria( params?.sqFicha?.toLong(), updateFicha )
        } catch ( Exception e ){
            res.status=1
            res.msg=e.getMessage()
            res.type='error'
        }
        render res as JSON
    }

    def getEquipeTecnica() {
        if( ! params.sqFicha ) {
            render 'Parâmetros insufientes para gerar a equipe técnica'
            return
        }
        render fichaService.getApoioTecnico( params.sqFicha.toLong() )
    }

    def getColaboradores() {
        Map res = [ status:0,msg:'', type:'success', colaboradores:'']
        try {
            res.colaboradores = fichaService.getColaboradores( params?.sqFicha?.toLong() )
        } catch ( Exception e ){
            res.status=1
            res.msg=e.getMessage()
            res.type='error'
        }
        render res as JSON
    }

    def getGridDeclinioPopulacional()
    {
        if( !params.sqFicha )
        {
            render 'ID da ficha não informado.'
        }
        List listaDeclinios = FichaDeclinioPopulacional.findAllByFicha(Ficha.get(params.sqFicha));
        render(template:"/ficha/avaliacao/divGridDeclinioPopulacional",
                model:['listaDeclinios':listaDeclinios])
    }


    /**
     * método para gravar a justificativa de não utilização do registro de ocorrencia no processo de avaliação
     * nas ocorrências marcadas no gride de ocorrências
     */
    /*def saveJustificativaNaoUtilizarOcorrencia(){
        if( ! params.txJustificativa )
        {
            render 'Informe a justificativa!'
            return
        }
        List ids    = params.ids.split(',')
        List tables = params.tables.split(',')

        FichaOcorrencia fo
        FichaOcorrenciaPortalbio fop
        ids.eachWithIndex { id , i ->
            if( tables[i] == 'salve')
            {
                fo  = FichaOcorrencia.get( id.toInteger() )
                if( fo )
                {
                    fo.geoValidate =false // não fazer localização geo do estado, municipio, bioma ao salvar
                    fo.inUtilizadoAvaliacao = 'N'
                    fo.txNaoUtilizadoAvaliacao = params.txJustificativa
                    fo.save(flush:true)
                }
            }
            else if( tables[i] == 'portalbio')
            {
                fop  = FichaOcorrenciaPortalbio.get( id.toInteger() )
                if( fop )
                {
                    fop.inUtilizadoAvaliacao = 'N'
                    fop.txNaoUtilizadoAvaliacao = params.txJustificativa
                    fop.save(flush:true)
                }
            }
        }
        render ''
    }
    */

    /**
     * método para atualizar os campos inUtilizadoAvaliacao e a justificativa da tabela ficha_ocorrencia ao selecionar as ocorrências no gride[
     */
    def saveInUtilizadoAvaliacao(){
        Map res = [ status:0, msg:'', type:'success' ]

        if( session.getAttribute( 'gravarUtilizadoAvaliacao' ) ) {
            res.status = 0
            res.msg = 'Estados, municípios, bacias e biomas da ficha <b>' + session.getAttribute( 'gravarUtilizadoAvaliacao' ) + '</b> estão sendo atualizados.<br>Aguarde o término.'
            res.type = 'info'
            render res as JSON
            return;
        }

        try {
            JSONObject data = JSON.parse( params.data )
            if( data.sqFicha ) {
                VwFicha vwFicha = VwFicha.get(data.sqFicha.toLong());
                // NOVO-REGISTRO
                // não precisa mais desse controle de sessão
                //session.setAttribute('gravarUtilizadoAvaliacao', vwFicha.nmCientifico)
            }
            fichaService.gravarUtilizadoAvaliacao( data.sn,data.ids, session.sicae.user.noPessoa, data.txJustificativa, data?.sqFicha?.toLong(), (data.sqMotivoNaoUtilizado ? data.sqMotivoNaoUtilizado.toLong() : null) )

            // CODIGO SALVE SEM CICLO
            // marcar as colaborações como resolvida se for no contexto da consulta
            if( data.fonte && data.contexto && data.contexto == 'consulta' && data.idOcorrencia ) {
                String where = ' and ' + (data.fonte = 'salve' ? 'va.sq_ficha_ocorrencia' : 'va.sq_ficha_ocorrencia_portalbio') + ' = ' + data.idOcorrencia
                DadosApoio situacaoAceita = dadosApoioService.getByCodigo('TB_SITUACAO_COLABORACAO', data.value ?: 'ACEITA')
                sqlService.execSql("""update salve.ficha_ocorrencia_validacao set sq_situacao = ${situacaoAceita.id}
                             from salve.ficha_ocorrencia_validacao va
                            inner join salve.pessoa as usuario on usuario.sq_pessoa = va.sq_web_usuario
                            inner join salve.ciclo_consulta_ficha ccf on ccf.sq_ciclo_consulta_ficha = va.sq_ciclo_consulta_ficha
                            inner join salve.ciclo_consulta cc on cc.sq_ciclo_consulta = ccf.sq_ciclo_consulta
                            left join salve.ciclo_consulta_ficha_versao ccfv on ccfv.sq_ciclo_consulta_ficha = va.sq_ciclo_consulta_ficha
                            left join salve.dados_apoio as situacao on situacao.sq_dados_apoio = va.sq_situacao
                        where ccfv.sq_ciclo_consulta_ficha is null
                          and situacao.cd_sistema = 'NAO_AVALIADA'
                          $where
                          and va.sq_ficha_ocorrencia_validacao = ficha_ocorrencia_validacao.sq_ficha_ocorrencia_validacao
                        """)

                sqlService.execSql("""update salve.ficha_ocorrencia_consulta set sq_situacao = ${situacaoAceita.id}
                             from salve.ficha_ocorrencia_consulta va
                            inner join salve.pessoa as usuario on usuario.sq_pessoa = va.sq_web_usuario
                            inner join salve.ciclo_consulta_ficha ccf on ccf.sq_ciclo_consulta_ficha = va.sq_ciclo_consulta_ficha
                            inner join salve.ciclo_consulta cc on cc.sq_ciclo_consulta = ccf.sq_ciclo_consulta
                            left join salve.ciclo_consulta_ficha_versao ccfv on ccfv.sq_ciclo_consulta_ficha = va.sq_ciclo_consulta_ficha
                            left join salve.dados_apoio as situacao on situacao.sq_dados_apoio = va.sq_situacao
                        where ccfv.sq_ciclo_consulta_ficha is null
                          and situacao.cd_sistema = 'NAO_AVALIADA'
                          and va.sq_ficha_ocorrencia = ${data.idOcorrencia}
                          and va.sq_ficha_ocorrencia_consulta = ficha_ocorrencia_consulta.sq_ficha_ocorrencia_consulta
                        """)
            }
        }
        catch( Exception e ) {
            /*
            println ' '
            println 'ERRO - SALVE  - saveInUtilizadoAvaliacao()'
            println e.getMessage()
            e.printStackTrace()
             */
            res.msg = e.getMessage();
            res.status=1
            res.type='error'
            try{session.removeAttribute('gravarUtilizadoAvaliacao')} catch( Exception e2 ){}
        }
        render res as JSON
    }
    /**
     * método para recuperar as justificativas das colaborações das consultas amplas, diretas e revisões
     * informadas quando usuário não concorda com o registro de ocorrência
     */
    def getJustificativasColaboracoes(){
        Map res = [status:0,msg:'',sqMotivo:'', txJustificativas:'']
        List colaboracoes=[]
        try {
            if ( ! params.idOcorrencia) {
                throw new Exception('Id da ocorrência não informado!')
            }
            if ( ! params.fonte) {
                throw new Exception('Fonte do registro não informada!')
            }
            switch (params.fonte) {
                case 'salve':
                    break
                case 'portalbio':
                    List ocorrencia = FichaOcorrenciaPortalbio.executeQuery("""select new map(inUtilizadoAvaliacao as inUtilizadoAvaliacao
                        ,txNaoUtilizadoAvaliacao as txNaoUtilizadoAvaliacao
                        ,motivoNaoUtilizadoAvaliacao.id as sqMotivo )
                        from FichaOcorrenciaPortalbio o where o.id=${params.idOcorrencia.toString()}""")
                    if( ocorrencia ) {
                        res.txJustificativas = ( ocorrencia[0].inUtilizadoAvaliacao=='N' ? ocorrencia[0].txNaoUtilizadoAvaliacao : '')
                        res.sqMotivo = ocorrencia[0]?.sqMotivo
                        FichaOcorrenciaValidacao.createCriteria().list {
                            eq('fichaOcorrenciaPortalbio.id', params.idOcorrencia.toLong())
                            eq('inValido', 'N')
                            isNotNull('txObservacao')
                        }.each {
                            colaboracoes.push([noUsuario     : it.usuario.noUsuario
                                           , txObservacao: it.txObservacao])
                        }
                    }
                    break;
                case 'consulta':
                    break;
            }
        } catch( Exception e ){
            res.status  = 1
            res.type    = 'info'
            res.msg     = e.getMessage()
        }
        if( res.txJustificativas ){
            // excluir as informações de log: nome do usuário e data que a observacao foi gravada
            res.txJustificativas = res.txJustificativas.replaceAll(/\.<br\/?>.*/ ,'')
        }
        if( colaboracoes ) {
            colaboracoes.each {
                String justificativa = '<br>'+it.noUsuario+'<br>'+Util.trimEditor( it.txObservacao ) + '<br>'
                if( res.txJustificativas.toString().indexOf( justificativa ) == -1 ){
                    res.txJustificativas += justificativa
                }
            }
        }
        // ler o motivo OUTRO como padrão
        if( ! res.sqMotivo ){
            DadosApoio motivoOutro = dadosApoioService.getByCodigo('TB_MOTIVO_NAO_UTILIZACAO_REGISTRO_OCORRENCIA','OUTRO')
            res.sqMotivo = motivoOutro ? motivoOutro.id : 0
        }
        render res as JSON
    }

    /**
     * método para processamento das ações realizadas no mapa.
     * data = {"action":"naoUtilizarNaAvaliacao","sn":"N","ids":[{"id":"589785","bd":"salve"}],"txJustificativa":"Registro não utilizado na avaliação.","sqMotivo":"1298"}*
     * data = {"action":"utilizarNaAvaliacao","sn":"S","ids":[{"id":"589785","bd":"salve"}]}
     * @return
     */
    def mapActions() {

        // deixar liberado por enquanto
        session.removeAttribute( 'gravarUtilizadoAvaliacao')

        Map res = [status:1,msg:'',type:'error', data:params ]
        try {
            JSONObject data = JSON.parse( params.data )
            data.motivoNaoUtilizadoAvaliacao=null

            switch( data.action ) {
                case 'deleteAOO':
                    Ficha ficha = Ficha.get(data.sqFicha.toLong() )
                    FichaAoo fichaAoo = FichaAoo.findByFicha( ficha )
                    if( fichaAoo ) {
                        fichaAoo.geometry = null
                        ficha.vlAoo = null
                        ficha.save()
                        fichaAoo.save(flush: true)
                        res.status=0;
                        res.message='AOO EXCLUÍDA com SUCESSO'
                        res.type='success'
                        res.data=''
                    }
                    break;

                case 'deleteEOO':
                    Ficha ficha = Ficha.get(data.sqFicha.toLong() )
                    FichaEoo fichaEoo = FichaEoo.findByFicha( ficha )
                    if( fichaEoo ) {
                        fichaEoo.geometry = null
                        fichaEoo.nuHydroshedLevel=null
                        ficha.vlEoo = null
                        ficha.save()
                        fichaEoo.save(flush: true)
                        res.status=0
                        res.message='EOO EXCLUÍDA com SUCESSO'
                        res.type='success'
                        res.data=''
                    }
                    break

                case 'saveEOO':
                    Ficha ficha = Ficha.get(data.sqFicha.toLong() )
                    FichaEoo fichaEoo = FichaEoo.findByFicha( ficha )
                    if( ! fichaEoo ) {
                        fichaEoo = new FichaEoo()
                        fichaEoo.ficha = ficha
                    }
                    if( data.wkt ) {
                        WKTReader wkt = new WKTReader();
                        Geometry g = wkt.read( data.wkt )
                        fichaEoo.geometry = g
                        if( data.nuHydroshedLevel ) {
                            fichaEoo.nuHydroshedLevel = data.nuHydroshedLevel.toInteger()
                        } else {
                            fichaEoo.nuHydroshedLevel = null
                        }
                        if (fichaEoo.geometry) {
                            fichaEoo.geometry.SRID = 4674
                        }
                        Long vlEoo =  Long.parseLong( data.vlEoo.toString().replaceAll(/[^0-9]/,'') )
                        if( vlEoo > 40000 ) {
                           throw new Exception('Valor máximo para EOO é de 40.000 km²')
                        }
                        ficha.vlEoo = vlEoo
                    } else {
                        fichaEoo.geometry=null
                        fichaEoo.nuHydroshedLevel=null
                        ficha.vlEoo=null
                    }
                    try {
                        ficha.save()
                        fichaEoo.save(flush: true)
                        res.status  = 0
                        res.message = 'EOO gravada com SUCESSO'
                        res.type    = 'success'
                        res.data    = ''
                    } catch( Exception e ) {
                        res.data    = ''
                        res.status  = 1
                        res.msg     = e.getMessage()
                        if( ( res.msg =~ /numeric field overflow/) ){
                            println 'AJUSTAR PARA ZERO'
                            res.msg = 'Valor da EOO '+ Util.formatInt(data.vlEoo) +'km² muito alto.'
                            ficha.vlEoo=0
                            ficha.save()
                        }
                        println ' '
                        println 'Erro SALVE saveEoo'
                        println e.getMessage()
                    }
                    break;

                break;case 'saveAOO':
                    Ficha ficha = Ficha.get(data.sqFicha.toLong() )
                    FichaAoo fichaAoo = FichaAoo.findByFicha( ficha )
                    if( ! fichaAoo ) {
                        fichaAoo = new FichaAoo()
                        fichaAoo.ficha = ficha
                    }
                    if( data.wkt ) {
                        WKTReader wkt = new WKTReader();
                        Geometry g = wkt.read( data.wkt )
                        fichaAoo.geometry = g
                        if (fichaAoo.geometry) {
                            fichaAoo.geometry.SRID = 4674
                        }
                        if( data.vlAoo ) {
                            ficha.vlAoo = Double.parseDouble( data.vlAoo.toString().replaceAll(/\,/,'.') )
                        }
                    } else {
                        fichaAoo.geometry=null
                        ficha.vlAoo=null
                    }
                    ficha.save()
                    fichaAoo.save( flush: true )
                    /*
                    if( data.wkt ) {
                        String cmdSql = """update salve.ficha_aoo set ge_aoo = 'SRID=4326;${data.wkt}' where sq_ficha = ${ficha.id.toString()}"""
                        sqlService.execSql( cmdSql )
                        //String cmdSql = """update salve.ficha_aoo set ge_aoo = :wkt where sq_ficha = :sqFicha"""
                        //Map pars = [sqFicha:ficha.id, wkt: "SRID=4326;${data.wkt}"]
                        //println ' '
                        //println cmdSql
                        //println 'PARS'
                        //println pars
                        //sqlService.execSql( cmdSql, pars)

                    } else {
                        fichaAoo.geometry=null;
                        fichaAoo.save( flush:true);
                    }
                     */
                    res.status=0;
                    res.message='Aoo gravada com SUCESSO'
                    res.type='success'
                    res.data=''
                    break;

                case 'marcarDesmarcarRegistroHistorico':
                    fichaService.marcarDesmarcarRegistroHistorico(data?.ids, session?.sicae?.user?.noPessoa);
                    res.status = 0
                    res.msg = ''
                    res.type = 'success'
                break;

                case 'marcarDesmarcarRegistroSensivel':
                    fichaService.marcarDesmarcarRegistroSensivel(data?.ids, session?.sicae?.user?.noPessoa);
                    res.status = 0
                    res.msg = ''
                    res.type = 'success'
                    break

                case 'naoUtilizarNaAvaliacao':
                    DadosApoio motivoNaoUtilizadoAvaliacao
                    if( data.sqMotivo ) {
                        motivoNaoUtilizadoAvaliacao = DadosApoio.get( data.sqMotivo.toInteger() )
                        data.motivoNaoUtilizadoAvaliacao = motivoNaoUtilizadoAvaliacao
                    }
                    if( ! data.txJustificativa ) {
                        if( motivoNaoUtilizadoAvaliacao && motivoNaoUtilizadoAvaliacao.codigoSistema == 'OUTRO') {
                            res.msg = 'Necessário informar a justificativa'
                            break
                        }
                        data.txJustificativa = motivoNaoUtilizadoAvaliacao.descricao
                    }

                case 'utilizarNaAvaliacao':
                case 'excluir':
                case 'restaurar':
                    // todos os cases caem aqui
                    //session.removeAttribute( 'gravarUtilizadoAvaliacao')

                    if( ! data.ids )
                    {
                        res.msg = 'Nenhum registro selecionado!'
                        break
                    }
                    else {

                        /*if( session.getAttribute( 'gravarUtilizadoAvaliacao' ) ) {
                            res.status = 1
                            res.msg = 'Estados, municípios, bacias e biomas da ficha <b>' + session.getAttribute( 'gravarUtilizadoAvaliacao' ) + '</b> estão sendo atualizados.<br><br>Aguarde o término.'
                            res.type = 'info'
                            break;
                        }*/


                        if( data.ids ) {
                            // ler o nome da especie a partir do registro da ocorrencia
                            Long sqFichaOcorrencia = data.ids[0].id.toLong()
                            String bd = data.ids[0].bd
                            VwFicha vwFicha = null
                            if (bd.toLowerCase() == 'salve') {
                                FichaOcorrencia fo = FichaOcorrencia.get(sqFichaOcorrencia)
                                vwFicha = fo.vwFicha
                            } else if (bd.toLowerCase() == 'portalbio') {
                                FichaOcorrenciaPortalbio fo = FichaOcorrenciaPortalbio.get(sqFichaOcorrencia)
                                vwFicha = fo.vwFicha
                            }
                            if (vwFicha) {
                                fichaService.gravarUtilizadoAvaliacao(data?.sn, data?.ids, session?.sicae?.user?.noPessoa, data?.txJustificativa, vwFicha.id, data?.motivoNaoUtilizadoAvaliacao?.id, true )
                                res.status = 0
                                res.msg = ''
                                res.type = 'success'
                            } else {
                                res.status = 1
                                res.msg = 'Ficha não encontrada.'
                                res.type = 'info'
                            }
                        } else {
                            res.status = 1
                            res.msg = 'Nenhum registro selecionado'
                            res.type = 'info'
                        }
                        break
                    }
            }
        } catch ( Exception e) {
            session.removeAttribute( 'gravarUtilizadoAvaliacao')
            res.msg = e.getMessage()
            res.data = ''
            println e.getMessage()
        }
        render res as JSON
    }

    def getInfoFundamentacao() {

        Long sqUsuarioLogado

        if( !params.sqOficinaFicha ) {
            render 'Id da oficina-ficha não informado'
            return;
        }
        if( !params.sqFicha ) {
            render 'Id da ficha não informado'
            return;
        }

        if( session?.sicae?.user?.sqPessoa ) {
            sqUsuarioLogado = session.sicae.user.sqPessoa
        } else {
            render 'Sessão expridada. Efetue login novamente.'
            return;
        }

        VwFicha vwFicha = VwFicha.get(params.sqFicha.toLong())
        if ( ! vwFicha ) {
            render '<h3>Ficha inválida</h3>'
            return
        }

        OficinaFicha oficinaFicha = OficinaFicha.get( params.sqOficinaFicha.toLong())
        if ( ! oficinaFicha ) {
            render '<h3>Oficina-Ficha inválida</h3>'
            return
        }

        DadosApoio recusado = dadosApoioService.getByCodigo('TB_SITUACAO_CONVITE_PESSOA', 'CONVITE_RECUSADO')
        DadosApoio naoEnviado = dadosApoioService.getByCodigo('TB_SITUACAO_CONVITE_PESSOA', 'EMAIL_NAO_ENVIADO')

        // ler o chat
        List listChats = OficinaFichaChat.findAllByOficinaFicha( oficinaFicha, [sort: "dtMensagem"])

        // ler as respostas dos validadores
        List listValidadorFicha = ValidadorFicha.createCriteria().list {
            eq('oficinaFicha', oficinaFicha)
            // não considerar convites recusados ou vencidos
            cicloAvaliacaoValidador {
                order('id', 'asc')
                /*validador {
                    order('noPessoa', 'asc')
                }*/
                or {
                    ne('situacaoConvite', recusado)
                    and {
                        eq('situacaoConvite', naoEnviado)
                        ge('dtValidadeConvite', new Date())
                    }

                }
            }
        }.unique { it.cicloAvaliacaoValidador.validador }

        // não exibir o nome dos validadores na tela
        Map nomesValidadores = [:]
        listValidadorFicha.eachWithIndex { it, index ->
            nomesValidadores[it.cicloAvaliacaoValidador.validador.id.toString()] = [ nome: Util.nomeAbreviado( it.cicloAvaliacaoValidador.validador.noPessoa ), numero: ( index + 1 ) +'º' ]
        }

        render(template: 'infoFundamentacao'
            , model: [ listValidadorFicha: listValidadorFicha
                        , listChats:listChats
                        , sqUsuarioLogado : sqUsuarioLogado
                        , nomesValidadores : nomesValidadores
        ])
    }

    /**
     * retorar a lista de motivos para não utilização do ponto no processo de avaliação
     * @return
     */
    def getOpcoesNaoUtilizacaoOcorrencia() {
        Map res = [status:0,msg:11,data:[]]
        dadosApoioService.getTable('TB_MOTIVO_NAO_UTILIZACAO_REGISTRO_OCORRENCIA').each {
            res.data.push([id:it.id, descricao:it.descricao, codigoSistema:it.codigoSistema])
        }
        render res as JSON
    }


    /**
    *  métodos privados
    */



    /**
     * transformar nomes copletos no format de citação. Ex: João da Silva -> Silva J.
     * @param  nome [description]
     * @return      [description]
     */
    private String nome2citacao( String nome = '')
    {
        List partes = Util.capitalize( nome.toString() ).split(' ')
        String resultado = partes.pop() + ' ' // ultimo  nome
        partes.each {
            if(  ! (it.toLowerCase() ==~ /^d.s?$/ || it.trim() == '' ) )
            {
                resultado += it.substring(0,1)+'.'
            }
        }
        return resultado
    }


    /**
     * a ficha pode estar em mais de uma oficina para o mesmo validador, então
     * considerar a mais recente.
     * Tambem está sendo utilizado o parametro outroValidador para pegar o resultado do outro validador quando for true
     */
    private ValidadorFicha getValidadorFicha(PessoaFisica validador, Long sqOficinaFicha = 0l, boolean outroValidador = false )
    {

        List listValidadorFichas = ValidadorFicha.createCriteria().list{
            cicloAvaliacaoValidador {
                if( ! outroValidador )
                {
                    eq('validador', validador )
                }
                else
                {
                    ne('validador', validador )
                }
            }
            eq('oficinaFicha.id', sqOficinaFicha)
            /*oficinaFicha {
                eq('ficha.id', ficha.id)
                oficina {
                    order('dtInicio', 'desc')
                }
            }
            */
        }

        if( listValidadorFichas.size() > 0 )
        {
            return listValidadorFichas[0]
        }
        return null
    }

    /**
     * retornar a lista com os outros validadores da ficha
     * @param validador
     * @param sqOficinaFicha
     * @return
     */
    private List getOutrosValidadoresFicha(PessoaFisica validador, Long sqOficinaFicha = 0l ) {
        return ValidadorFicha.createCriteria().list{
            eq('oficinaFicha.id', sqOficinaFicha)
                cicloAvaliacaoValidador {
                ne('validador', validador )
            }
        }
    }

    def getAnexosFicha() {
        String appUrl = grailsApplication.config.url.sistema ?: '/salve/'
        Ficha ficha = Ficha.get( params.sqFicha )
        if ( ficha ) {
            //DadosApoio contexto = dadosApoioService.getByCodigo('TB_CONTEXTO_ANEXO','MAPA_DISTRIBUICAO');
            List dados = FichaAnexo.createCriteria().list{
                eq('ficha',ficha)
                contexto {
                    order('descricao')
                }
                order('dtAnexo')
            }
            render( template:'/fichaCompleta/divGridAnexosFicha',model:[listFichaAnexo:dados, appUrl:appUrl]);
        }
        render ''
    }

}
