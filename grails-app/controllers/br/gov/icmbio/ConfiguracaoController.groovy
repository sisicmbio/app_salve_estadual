package br.gov.icmbio

import grails.converters.JSON

class ConfiguracaoController {

    def index() {
        Configuracao configuracao = Configuracao.findByEstadoIsNotNull()
        if( ! configuracao ) {
            configuracao = new Configuracao()
        }
        List estados = Estado.list().sort{ it.noEstado }
        render('view':'index',model:[estados:estados,configuracao:configuracao])
    }

    def save(){
        Map res = [status:0,msg:'',type:'success']
        try {

             if( !params.sqEstado ){
                 throw new Exception('Estado não informado');
             }
             if( !params.noInstituicao ){
                 throw new Exception('Instituição não informada');
             }
             if( !params.sgInstituicao ){
                 throw new Exception('Sigla da instituição não informada');
             }
            Estado estado = Estado.get(params.sqEstado.toLong())
            if( !estado ){
                throw new Exception('Estado inválido');
            }
            String brasaoFileName = params.brasaoCurrentFileName ?: ''
            if( params.noArquivoBrasao) {
                def f = request.getFile('noArquivoBrasao')

                if (f && !f.empty) {
                    File savePath = new File(grailsApplication.config.arquivos.dir + '/brasoes-estados')
                    String originalFileName = f.getOriginalFilename()
                    brasaoFileName = 'brasao-' + estado.sgEstado.toLowerCase() + '.' + Util.getFileExtension(originalFileName)
                    if (!savePath.exists()) {
                        if (!savePath.mkdirs()) {
                            return this._returnAjax(1, 'Não foi possivel criar o diretório ' + savePath.asString(), "error")
                        }
                    }
                    String fullFileName = savePath.absolutePath + '/' + brasaoFileName
                    f.transferTo(new File(fullFileName))
                }
            }
            Configuracao.findAllByEstadoNotEqual(estado).each {
                it.delete()
            }
            Configuracao configuracao = Configuracao.findByEstado(estado)
            if( !configuracao ) {
                configuracao = new Configuracao()
                configuracao.estado = estado
            }
            configuracao.dsLink = params.dsLink
            configuracao.noInstituicao = params.noInstituicao
            configuracao.sgInstituicao = params.sgInstituicao
            configuracao.noArquivoBrasao = brasaoFileName
            configuracao.save(flush:true)
        } catch( Exception e) {
            res.type='error'
            res.status=1
            res.msg = e.getMessage()
        }


        println res

        render res as JSON
    }
}
