/**
 * Este controlador é resposável por receber as ações referentes ao cadastro aba Avaliação da ficha de espécies
 * Regras:
 * -------------------------------------------------------------------------------------
 * 1) 10.6 - Resultado - Ao selecionar a categoria IUCN, verificar se está igual a registrada na última avaliação nacional ( aba - 7.1 - Conservação)
 * caso seja diferente exibir opção de registrar os motivos da mudanca( gride ) e a justificativa da mudança.
 * --------------------------------------------------------------------------------------
 *
 *
 *
 **/

package br.gov.icmbio

import grails.converters.JSON;

class FichaAvaliacaoController extends FichaBaseController {

    def dadosApoioService
    def fichaService

    def index() {
        if( !params.sqFicha ) {
            render( 'ID da ficha não informado.' )
            return
        }
        Ficha ficha = Ficha.get( params.sqFicha.toInteger() )
        render( template: "/ficha/avaliacao/index", model: [ 'ficha': ficha ] )
    }

    /*def saveFrmAvaliacao()
    {
        if( !params.sqFicha )
        {
            return this._returnAjax(1, 'ID da ficha não informado.', "error")
        }
        Ficha ficha = Ficha.get(params.sqFicha)
        ficha.properties = params
        try {
            ficha.save(flush:true)
            this._returnAjax(0, getMsg(1), "success")
        } catch(Exception e) {
            println e.getMessage()
            this._returnAjax(1, getMsg(2), "error")
        }
    }
    */

    // aba 11.1 - Distribuição
    def tabAvaDistribuicao() {
        if( !params.sqFicha ) {
            render( 'ID da ficha não informado.' )
            return
        }
        Ficha ficha = Ficha.get( params.sqFicha )
        List listTendencia = dadosApoioService.getTable( 'TB_TENDENCIA' ).sort {
            it.ordem
        }
        String hydroshedLevel = ''
        if( ficha.vlEoo > 0 ) {
            FichaEoo fichaEoo = FichaEoo.findByFicha( ficha )
            if( fichaEoo.nuHydroshedLevel){
                hydroshedLevel = 'Hydrosheds level ' + fichaEoo.nuHydroshedLevel.toString()
            } else {
                hydroshedLevel = 'Registros de ocorrência'
            }
        }
        render( template: "/ficha/avaliacao/formDistribuicao", model: [ 'ficha': ficha
                                                                        , hydroshedLevel:hydroshedLevel
                                                                        , 'listTendencia': listTendencia ] )
    }

    def saveFrmDistribuicao() {
        if( !params.sqFicha ) {
            return this._returnAjax( 1, 'ID da ficha não informado.', "error" )
        }
        /*if( params.vlEoo ) {
            params.vlEoo = params.vlEoo.toString().replaceAll(/\./,'').toLong()
        }*/
        Ficha ficha = Ficha.get( params.sqFicha )
        ficha.properties = params
        ficha.tendenciaEoo = DadosApoio.get( params.sqTendenciaEoo )
        ficha.tendenciaAoo = DadosApoio.get( params.sqTendenciaAoo )
        try {
            ficha.save( flush: true )
            this._returnAjax( 0, getMsg( 1 ), "success" )
        }
        catch( Exception e ) {
            println e.getMessage()
            this._returnAjax( 1, getMsg( 2 ), "error" )
        }
    }

    // ----------------------------------------------------------------------

    def tabAvaPopulacao() {
        if( !params.sqFicha ) {
            render( 'ID da ficha não informado.' )
            return;
        }
        Ficha ficha = Ficha.get( params.sqFicha )
        List listMedidaTempo = dadosApoioService.getTable( 'TB_UNIDADE_TEMPO' ).sort {
            it.ordem
        }
        List listTipoReducao = dadosApoioService.getTable( 'TB_TIPO_REDUCAO' ).sort {
            it.ordem
        }
        List listTipoDeclinio = dadosApoioService.getTable( 'TB_DECLINIO_POPULACIONAL' ).sort {
            it.ordem
        }
        List listTendencia = dadosApoioService.getTable( 'TB_TENDENCIA' ).sort {
            it.ordem
        }
        List listPercentuais = dadosApoioService.getTable( 'TB_PERC_DECLINIO_POPULACIONAL' ).sort {
            it.ordem
        }

        render( template: "/ficha/avaliacao/formPopulacao",
            model: [ 'ficha'           : ficha,
                     'listMedidaTempo' : listMedidaTempo,
                     'listTipoReducao' : listTipoReducao,
                     'listTipoDeclinio': listTipoDeclinio,
                     'listTendencia'   : listTendencia,
                     'listPercentuais' : listPercentuais, ] )
    }

    def saveFrmPopulacao() {
        if( !params.sqFicha ) {
            return this._returnAjax( 1, 'ID da ficha não informado.', "error" )
        }
        Ficha ficha = Ficha.get( params.sqFicha )
        ficha.properties = params
        //ficha.medidaTempoGeracional 	= DadosApoio.get(params.sqMedidaTempoGeracional)

        ficha.tipoReduPopuPassRev 		= DadosApoio.get(params.sqTipoReduPopuPassRev)
        ficha.tipoReduPopuPass = DadosApoio.get( params.sqTipoReduPopuPass )
        ficha.tipoReduPopuFutura = DadosApoio.get( params.sqTipoReduPopuFutura )
        ficha.tipoReduPopuPassFutura = DadosApoio.get( params.sqTipoReduPopuPassFutura )
        ficha.declinioPopulacional = DadosApoio.get( params.sqDeclinioPopulacional )
        ficha.tendenNumIndividuoMaduro = DadosApoio.get( params.sqTendenNumIndividuoMaduro )
        ficha.percDeclinioPopulacional = DadosApoio.get( params.sqPercDeclinioPopulacional )
        ficha.tendenciaNumSubpopulacao = DadosApoio.get( params.sqTendenciaNumSubpopulacao )
        try {
            ficha.save( flush: true )
            this._returnAjax( 0, getMsg( 1 ), "success" )
        }
        catch( Exception e ) {
            println e.getMessage()
            this._returnAjax( 1, getMsg( 2 ), "error" )
        }
    }

    def addDeclinioPopulacional() {
        if( !params.sqFicha ) {
            this._returnAjax( 1, 'ID da Ficha não informado!', "error" )

        }

        if( !params.sqDeclinioPopulacional ) {
            this._returnAjax( 1, 'Declínio Populacional não informado!', "error" )
        }

        FichaDeclinioPopulacional reg = new FichaDeclinioPopulacional();
        reg.ficha = Ficha.get( params.sqFicha );
        reg.declinioPopulacional = DadosApoio.get( params.sqDeclinioPopulacional );
        if( FichaDeclinioPopulacional.countByFichaAndDeclinioPopulacional( reg.ficha, reg.declinioPopulacional ) > 0 ) {
            reg = null;
            return this._returnAjax( 1, getMsg( 3 ), 'info' );
        }

        reg.save( flush: true );
        return this._returnAjax( 0 );
    }

    def getGridDeclinioPopulacional() {
        if( !params.sqFicha ) {
            render 'ID da ficha não informado.'
        }
        List listaDeclinios = FichaDeclinioPopulacional.findAllByFicha( Ficha.get( params.sqFicha ) );
        render( template: "/ficha/avaliacao/divGridDeclinioPopulacional",
            model: [ 'listaDeclinios': listaDeclinios ] )
    }

    def deleteDeclinioPopulacional() {
        if( !params.id ) {
            render '';
            return;
        }
        FichaDeclinioPopulacional.get( params.id )?.delete( flush: true )
        return this._returnAjax( 0 );
    }


    // ----------------------------------------------------------------------------

    def tabAvaAmeaca() {
        if( !params.sqFicha ) {
            render( 'ID da ficha não informado.' )
            return;
        }
        Ficha ficha = Ficha.get( params.sqFicha )
        List listTendencia = dadosApoioService.getTable( 'TB_TENDENCIA' ).sort {
            it.ordem
        }
        render( template: "/ficha/avaliacao/formAmeaca",
            model: [ 'ficha'        : ficha,
                     'listTendencia': listTendencia, ] );
    }

    def saveFrmAmeacas() {
        if( !params.sqFicha ) {
            return this._returnAjax( 1, 'ID da ficha não informado.', "error" )
        }
        Ficha ficha = Ficha.get( params.sqFicha )
        ficha.properties = params
        ficha.tendenNumLocalizacoes = DadosApoio.get( params.sqTendenNumLocalizacoes )
        ficha.tendenQualidadeHabitat = DadosApoio.get( params.sqTendenQualidadeHabitat )
        try {
            ficha.save( flush: true )
            this._returnAjax( 0, getMsg( 1 ), "success" )
        }
        catch( Exception e ) {
            println e.getMessage()
            this._returnAjax( 1, getMsg( 2 ), "error" )
        }
    }

    //-----------------------------------------------------------------------------

    def tabAvaAnaliseQuantitativa() {
        if( !params.sqFicha ) {
            render( 'ID da ficha não informado.' )
            return;
        }
        Ficha ficha = Ficha.get( params.sqFicha )
        List listTendencia = dadosApoioService.getTable( 'TB_TENDENCIA' ).sort {
            it.ordem
        }
        List listProbExtincao = dadosApoioService.getTable( 'TB_PROB_EXTINCAO_BRASIL' ).sort {
            it.ordem
        }
        render( template: "/ficha/avaliacao/formAnaliseQuantitativa", model: [ 'ficha': ficha, 'listProbExtincao': listProbExtincao, 'listTendencia': listTendencia ] );
    }

    def saveFrmAnaliseQuantitativa() {
        if( !params.sqFicha ) {
            return this._returnAjax( 1, 'ID da ficha não informado.', "error" )
        }
        Ficha ficha = Ficha.get( params.sqFicha )
        ficha.properties = params
        ficha.probExtincaoBrasil = DadosApoio.get( params.sqProbExtincaoBrasil )
        ficha.tendenNumLocalizacoes = DadosApoio.get( params.sqTendenNumLocalizacoes )
        ficha.tendenQualidadeHabitat = DadosApoio.get( params.sqTendenQualidadeHabitat )
        try {
            ficha.save( flush: true )
            this._returnAjax( 0, getMsg( 1 ), "success" )
        }
        catch( Exception e ) {
            println e.getMessage()
            this._returnAjax( 1, getMsg( 2 ), "error" )
        }
    }

    //-----------------------------------------------------------------------------
    def tabAvaAjusteRegional() {
        if( !params.sqFicha ) {
            render( 'ID da ficha não informado.' )
            return
        }
        Ficha ficha = Ficha.get( params.sqFicha )
        render( template: "/ficha/avaliacao/formAjusteRegionalPreview",
            model: [ 'ficha': ficha ] )
    }

    def saveFrmAjusteRegional() {
        if( !params.sqFicha ) {
            return this._returnAjax( 1, 'ID da ficha não informado.', "error" )
        }
        Ficha ficha = Ficha.get( params.sqFicha )
        ficha.properties = params
        ficha.tipoConectividade = DadosApoio.get( params.sqTipoConectividade )
        ficha.tendenciaImigracao = DadosApoio.get( params.sqTendenciaImigracao )
        try {
            ficha.save( flush: true )
            this._returnAjax( 0, getMsg( 1 ), "success" )
        }
        catch( Exception e ) {
            println e.getMessage()
            this._returnAjax( 1, getMsg( 2 ), "error" )
        }
    }

    //-----------------------------------------------------------------------------
    // Avaliação Expressa / Expedita
    //-----------------------------------------------------------------------------
    def tabAvaExpedita() {
        if( !params.sqFicha ) {
            render( 'ID da ficha não informado.' )
            return;
        }
        Ficha ficha = Ficha.get( params.sqFicha )
        // renderizar a página
        render( template: "/ficha/avaliacao/formAvaliacaoExpedita",
            model: [ 'ficha': ficha ] );
    }

    def saveFrmAvaExpedita() {
        String p1, p2, p3, p4
        if( !params.sqFicha ) {
            return this._returnAjax( 1, 'ID da ficha não informado.', "error" )
        }
        p1 = params.stFavorecidoConversaoHabitats
        p2 = params.stTemRegistroAreasAmplas
        p3 = params.stPossuiAmplaDistGeografica
        p4 = params.stFrequenteInventarioEoo


        // regras da validação expedita
        if( p1 == 'S' || p2 == 'S' || ( p3 == 'S' && p4 == 'S' ) ) {
            params.categoriaIucn = dadosApoioService.getByCodigo( 'TB_CATEGORIA_IUCN', 'LC' );
        }
        else
            if( p1 == 'N' && p2 == 'D' && p3 == 'N' && p4 == 'D' ) {
                params.categoriaIucn = dadosApoioService.getByCodigo( 'TB_CATEGORIA_IUCN', 'DD' );
            }
            else
                if( p1 == 'N' && p2 == 'D' && p3 == 'D' && p4 == 'N' ) {
                    params.categoriaIucn = dadosApoioService.getByCodigo( 'TB_CATEGORIA_IUCN', 'DD' );
                }
                else
                    if( p1 == 'N' && p2 == 'D' && p3 == 'D' && p4 == 'D' ) {
                        params.categoriaIucn = dadosApoioService.getByCodigo( 'TB_CATEGORIA_IUCN', 'DD' );
                    }

        // se p1 = s zera todas as outras
        if( !p1 || p1 == 'S' ) {
            params.stTemRegistroAreasAmplas = null;
            params.stPossuiAmplaDistGeografica = null;
            params.stFrequenteInventarioEoo = null;
        }

        if( p1 != 'S' ) {
            if( !p2 || p2 == 'S' ) {
                params.stPossuiAmplaDistGeografica = null;
                params.stFrequenteInventarioEoo = null;
            }
            else {
                if( ( !p3 || !p4 ) && ( p1 == 'D' && p3 == 'D' ) ) {
                    return this._returnAjax( 1, 'Preencha todos os campos.', "error" )
                }
            }
        }

        Ficha ficha = Ficha.get( params.sqFicha )
        ficha.properties = params
        try {
            ficha.save( flush: true )
            this._returnAjax( 0, getMsg( 1 ), "success" )
        }
        catch( Exception e ) {
            println e.getMessage()
            this._returnAjax( 1, getMsg( 2 ), "error" )
        }
    }

    //-----------------------------------------------------------------------------

    def tabAvaResultado() {
        if( !params.sqFicha ) {
            render( 'ID da ficha não informado.' )
            return
        }
        Ficha ficha = Ficha.get( params.sqFicha.toLong() )
        List listCategoriaIucn = dadosApoioService.getTable( 'TB_CATEGORIA_IUCN' ).findAll {
            it.codigoSistema != 'OUTRA' && it.codigoSistema != 'AMEACADA' && it.codigoSistema != 'POSSIVELMENTE_EXTINTA'
        }.sort {
            it.ordem
        };
        List listMudancaCategoria = dadosApoioService.getTable( 'TB_MUDANCA_CATEGORIA' ).sort {
            it.ordem
        }
        //List listTipoAbrangencia = dadosApoioService.getTable('TB_TIPO_ABRANGENCIA')
        List listConectividade = dadosApoioService.getTable( 'TB_CONECTIVIDADE' ).sort {
            it.ordem
        }
        List listTendencia = dadosApoioService.getTable( 'TB_TENDENCIA' ).sort {
            it.ordem
        }

        // recuperar a ultima avaliação nacional
        Map dadosUltimaAvaliacao = ficha.ultimaAvaliacaoNacionalHist

        // verificar se pode alterar a categoria em função da avaliação expedita
        Boolean flagEditCategoria = true
        if( ficha.stFavorecidoConversaoHabitats == 'S' || ficha.stTemRegistroAreasAmplas == 'S' || ( ficha.stPossuiAmplaDistGeografica == 'S' && ficha.stFrequenteInventarioEoo == 'S' ) ) {
            flagEditCategoria = false
        }
        else
            if( ficha.stFavorecidoConversaoHabitats == 'N' || ficha.stTemRegistroAreasAmplas == 'D' || ( ficha.stPossuiAmplaDistGeografica == 'D' && ficha.stFrequenteInventarioEoo == 'N' ) ) {
                flagEditCategoria = false
            }
            else
                if( ficha.stFavorecidoConversaoHabitats == 'N' || ficha.stTemRegistroAreasAmplas == 'D' || ( ficha.stPossuiAmplaDistGeografica == 'D' && ficha.stFrequenteInventarioEoo == 'D' ) ) {
                    flagEditCategoria = false
                }

        // esta view é chamada no módulo ficha e no grupo de validação da fichaCompleta
        params.contexto = params.contexto ?: 'ficha'

        // somente ADMs E PFs podem editar o resultado da oficias na aba 11.6 sendo que PF não pode alterar a categoria
        boolean canModify = session.sicae.user.isADM() || session.sicae.user.isPF()
        boolean canModifyCategory = session.sicae.user.isADM()
        boolean canModifyEquipes = session.sicae.user.isADM() || ( ficha.categoriaIucn && ficha.categoriaIucn.codigoSistema != 'NE' ) // se ficha já estiver validada pode alterar

        // se a ficha estiver finalizada ou publicada NÃO poderá ser alterada pelo PF
        if( canModify && session.sicae.user.isPF() && ficha.situacaoFicha.codigoSistema =~ /^(FINALIZADA|PUBLICADA)$/) {
           canModify=false
        }

        // se por algum motivo a ficha voltou para compilacao ea aba 11.6 ficou preenchida o PF pode limpa-la
        if( canModify && session.sicae.user.isPF() && ficha.situacaoFicha.codigoSistema =~ /^(COMPILACAO)$/) {
            canModifyCategory = true
        }

        // no primeiro ciclo nao tem restrição de alteração
        if( ficha.cicloAvaliacao.nuAno <= 2010 && ficha.cicloAvaliacao.inSituacao != 'F' ) {
            canModify = true
            canModifyCategory = true
        }

        render( template: "/ficha/avaliacao/formResultadoOficina",
            model: [ 'ficha'                 : ficha
                     , 'canModify'           : canModify
                     , 'canModifyCategory'   : canModifyCategory
                     , 'canModifyEquipes'    : canModifyEquipes
                     , 'listCategoriaIucn'   : listCategoriaIucn
                     , 'listMudancaCategoria': listMudancaCategoria
                     , 'dadosUltimaAvaliacao': dadosUltimaAvaliacao
                     // campos formulario ajuste regional
                     , 'listConectividade'   : listConectividade
                     , 'listTendencia'       : listTendencia ] )
        /*
        // renderizar a página
        render(template:"/fichaCompleta/formResultadoOficina",
            model:['ficha'			: ficha,
                    'canModify'     : canModify,
            'listCategoriaIucn'		: listCategoriaIucn,
            'listMudancaCategoria'	: listMudancaCategoria,
            'listTipoAbrangencia'	: listTipoAbrangencia,
            'dadosUltimaAvaliacao'  : dadosUltimaAvaliacao,
            'flagEditCategoria'     : flagEditCategoria,
             'listConectividade'		: listConectividade,
               'listTendencia'			: listTendencia,
            ])
            */
    }

    /**
     * gravar Resultado da Avaliação ( Aba 11.6 )
     * @return
     */
    def saveResultadoAvaliacao() {

        if( !params.sqFicha ) {
            return this._returnAjax( 1, 'ID da ficha não informado.', "error" )
        }

        Ficha ficha = Ficha.get( params.sqFicha.toLong() )
        if( !ficha ) {
            return this._returnAjax( 1, 'Ficha inválida.', "error" )
        }

        if( !ficha.canModify( session.sicae.user ) ) {
            return this._returnAjax( 1, 'Alteração não permitida.', "error" )

        }

        if( !params.sqCategoriaIucn && params.dsJustificativa ) {
            return this._returnAjax( 1, 'Categoria não informada.', "error" )
        }

        DadosApoio categoria = null
        if( params.sqCategoriaIucn ) {
            categoria = DadosApoio.get( params.sqCategoriaIucn.toLong() )
            if( !categoria ) {
                return this._returnAjax( 1, 'Categoria inválida.', "error" )
            }
        }
        if( ficha.situacaoFicha.codigoSistema == 'PUBLICADA' ) {
            if( !categoria || categoria.codigoSistema == 'NE' ) {
                return this._returnAjax( 1, 'Ficha publicada deve ter o resultado da avaliação.', "error" )
            }
        }

        if( categoria ) {

            if( !params.dsJustificativa && categoria.codigoSistema != 'NE' ) {
                return this._returnAjax( 1, 'Jutificativa da avaliação é obrigatória.', "error" )
            }

            if( categoria.codigoSistema ==~ /VU|CR|EN/ ) {
                if( !params.dsCriterioAvalIucn ) {
                    return this._returnAjax( 1, 'Critério não informado.', "error" )
                }
            }
            else {
                params.dsCriterioAvalIucn = null;
            }

            // possivelmente extinta
            if( categoria.codigoSistema == 'CR' ) {
                if( params.stPossivelmenteExtinta != 'S' ) {
                    params.stPossivelmenteExtinta = 'N'
                }
            }
            else {
                params.stPossivelmenteExtinta = null
            }

            // validar mudanca de categoria
            Map dadosUltimaAvaliacao = fichaService.getUltimaAvaliacaoNacionalTaxon(ficha.taxon.id)
            if( categoria.codigoSistema != 'NE' && dadosUltimaAvaliacao.sqCategoriaIucn && dadosUltimaAvaliacao.sqCategoriaIucn.toLong() != categoria.id.toLong() ) {
                if( FichaMudancaCategoria.countByFicha( ficha ) == 0 ) {
                    return this._returnAjax( 1, 'Motivo da mudança de categoria deve ser informado.', "error" )
                }
                ficha.dsJustMudancaCategoria = params.dsJustMudancaCategoria
            }
            else {
                // verificar se houve mudanca de categoria na Validação, se não houver pode excluir os motivos
                if( categoria.codigoSistema == 'NE' || !ficha.categoriaFinal || !dadosUltimaAvaliacao.sqCategoriaIucn || dadosUltimaAvaliacao.sqCategoriaIucn.toLong() == ficha.categoriaFinal.id.toLong() ) {
                    ficha.dsJustMudancaCategoria = null
                    FichaMudancaCategoria.findAllByFicha( ficha ).each {
                        it.delete()
                    }
                }
            }

            // gravar resultado da avaliacao ( oficina )
            ficha.categoriaIucn = categoria
            ficha.dsJustificativa = Util.corrigirCategoriasTexto(params.dsJustificativa)
            ficha.dsCriterioAvalIucn = params.dsCriterioAvalIucn
            ficha.stPossivelmenteExtinta = params.stPossivelmenteExtinta
            if( params?.dsCitacao != null ) {
                ficha.dsCitacao = Util.trimEditor( params.dsCitacao )
                ficha.dsCitacao = (ficha.dsCitacao == '' ? null : ficha.dsCitacao)
            }
            if( params?.dsEquipeTecnica != null ) {
                ficha.dsEquipeTecnica = Util.trimEditor( params.dsEquipeTecnica )
                ficha.dsEquipeTecnica = (ficha.dsEquipeTecnica == '' ? null : ficha.dsEquipeTecnica)
            }
            if( params?.dsColaboradores != null ) {
                ficha.dsColaboradores = Util.trimEditor( params.dsColaboradores )
                ficha.dsColaboradores = (ficha.dsColaboradores == '' ? null : ficha.dsColaboradores)
            }

            // campos do form ajuste regional
            if( params.sqTipoConectividade ) {
                ficha.tipoConectividade = DadosApoio.get( params.sqTipoConectividade.toLong() )
            }
            else {
                ficha.tipoConectividade = null
            }

            // tendencia Imigração
            if( params.sqTendenciaImigracao ) {
                ficha.tendenciaImigracao = DadosApoio.get( params.sqTendenciaImigracao.toLong() )
            }
            else {
                ficha.tendenciaImigracao = null
            }
            // Populacao Brasileira pode declinar
            ficha.stDeclinioBrPopulExterior = params.stDeclinioBrPopulExterior ?: null
            ficha.dsConectividadePopExterior = params.dsConectividadePopExterior ?: null
        }
        else {
            ficha.categoriaIucn = null
            ficha.dsJustificativa = null
            ficha.dsCriterioAvalIucn = null
            ficha.stPossivelmenteExtinta = null
            ficha.tipoConectividade = null
            ficha.tendenciaImigracao = null
            ficha.stDeclinioBrPopulExterior = null
            ficha.dsConectividadePopExterior = null

            // as informações sobre a mudança de categoria são preenchidas nas abas 11.6 e 11.7.
            // Assim se a aba 11.7 não tiver a categoria preenchida a mudaçã de categoria pode ser
            // excluída aqui
            if( ! ficha.categoriaFinal ) {
                ficha.dsJustMudancaCategoria = null
                FichaMudancaCategoria.findAllByFicha( ficha ).each {
                    it.delete()
                }
            }
        }
        ficha.save( flush: true )
        return this._returnAjax( 0, getMsg( 1 ), "success" )
    }

    def saveAutoria(){
        Map res = [status:0,msg:'',type:'success']
        try {
            if (! params.sqFicha ) {
                throw new Exception('Id da ficha não informado')
            }

            Ficha ficha = Ficha.get( params.sqFicha.toLong() )
            if( ! ficha ){
                throw new Exception('Ficha inexistente')
            }

            println ' '
            println ' '
            println 'ADM '
            println session.sicae.user.isADM()
            if( ! ficha.canModify(session.sicae.user) ) {
                throw new Exception('Sem permissão')
            }
            ficha.dsCitacao = Util.stripTags( params.dsCitacao )

            // não permitir gravar a citação/autoria em branco se a ficha estiver publicada
            if( !ficha.dsCitacao && ficha.situacaoFicha.codigoSistema=='PUBLICADA') {
                throw new Exception('Autoriá deve ser informada quando a ficha estiver publicada')
            }

            ficha.save()
            res.msg = 'Autoria gravada com SUCESSO'
        } catch( Exception e ){
            res.msg      = e.getMessage();
            res.status   = 1
            res.type     = 'error'
        }
        render res as JSON
    }

    def saveEquipeTecnica(){
        Map res = [status:0,msg:'',type:'success']
        try {
            if (! params.sqFicha ) {
                throw new Exception('Id da ficha não informado')
            }

            Ficha ficha = Ficha.get( params.sqFicha.toLong() )
            if( ! ficha ){
                throw new Exception('Ficha inexistente')
            }

            if( ! ficha.canModify(session.sicae.user) ) {
                throw new Exception('Sem permissão')
            }

            ficha.dsEquipeTecnica = Util.stripTags(params.dsEquipeTecnica)
            ficha.save()
            res.msg = 'Equipe técnica gravada com SUCESSO'
        } catch( Exception e ){
            res.msg      = e.getMessage();
            res.status   = 1
            res.type     = 'error'
        }
        render res as JSON
    }

    def saveColaboradores(){
        Map res = [status:0,msg:'',type:'success']
        try {
            if (! params.sqFicha ) {
                throw new Exception('Id da ficha não informado')
            }

            Ficha ficha = Ficha.get( params.sqFicha.toLong() )
            if( ! ficha ){
                throw new Exception('Ficha inexistente')
            }

            if( ! ficha.canModify(session.sicae.user) ) {
                throw new Exception('Sem permissão')
            }

            ficha.dsColaboradores = Util.stripTags(params.dsColaboradores)
            ficha.save()
            res.msg = 'Colaboradores gravados com SUCESSO'
        } catch( Exception e ){
            res.msg      = e.getMessage();
            res.status   = 1
            res.type     = 'error'
        }
        render res as JSON
    }

    //-----------------------------------------------------------------------------

    def tabAvaResultadoFinal() {
        if( !params.sqFicha ) {
            render( 'ID da ficha não informado.' )
            return
        }
        Ficha ficha = Ficha.get( params.sqFicha )
        List listCategoriaIucn = dadosApoioService.getTable( 'TB_CATEGORIA_IUCN' ).findAll {
            it.codigoSistema != 'OUTRA' && it.codigoSistema != 'AMEACADA' && it.codigoSistema != 'POSSIVELMENTE_EXTINTA'
        }.sort {
            it.ordem
        };
        List listMudancaCategoria = dadosApoioService.getTable( 'TB_MUDANCA_CATEGORIA' ).sort {
            it.ordem
        }

        // recuperar a ultima avaliação nacional
        Map dadosUltimaAvaliacao = ficha.ultimaAvaliacaoNacionalHist

        // esta view é chamada no módulo ficha e no grupo de validação da fichaCompleta
        params.contexto = params.contexto ?: 'ficha'

        boolean canModify = false
        boolean canModifyCategory = false

        // no primeiro ciclo a alteração é permitida
        if( ficha.cicloAvaliacao.nuAno <= 2010 && ficha.cicloAvaliacao.inSituacao != 'F' ) {
            canModify = true
            canModifyCategory = true
        }
        else {
            // somente ADMs podem editar o resultado da validação na aba 11.7
            // 10-07-2020 - Pontos focais podem alterar se a ficha estiver nas seguintes situações: Em Validação, Validada, Validada em Revisão
            // com a categoria definada
            canModify = session.sicae.user.isADM() || ( ficha.categoriaFinal && session.sicae.user.isPF() && ( ficha.situacaoFicha.codigoSistema =~ /^(EM_VALIDACAO|VALIDADA|VALIDADA_EM_REVISAO)$/ ) )
            // PF pode alterar a categoria se a ficha estiver na situacao anterior a da Validação
            canModifyCategory = session.sicae.user.isADM() || ( session.sicae.user.isPF() && ! (ficha.situacaoFicha.codigoSistema =~ /^(EM_VALIDACAO|VALIDADA|VALIDADA_EM_REVISAO|FINALIZADA|PUBLICADA)$/ ) )
        }

        render( template: "/ficha/avaliacao/formResultadoFinal",
            model: [ 'ficha'                 : ficha
                     , 'canModify'           : canModify
                     , 'canModifyCategory'   : canModifyCategory
                     , 'listCategoriaIucn'   : listCategoriaIucn
                     , 'listMudancaCategoria': listMudancaCategoria
                     , 'dadosUltimaAvaliacao': dadosUltimaAvaliacao ] )
    }


    def saveResultadoValidacao() {

        if( !params.sqFicha ) {
            return this._returnAjax( 1, 'ID da ficha não informado.', "error" )
        }

        Ficha ficha = Ficha.get( params.sqFicha.toLong() )
        if( !ficha ) {
            return this._returnAjax( 1, 'Ficha inválida.', "error" )
        }

        if( !ficha.canModify( session.sicae.user ) ) {
            return this._returnAjax( 1, 'Alteração não permitida.', "error" )

        }

        if( !params.sqCategoriaIucn && params.dsJustificativa ) {
            return this._returnAjax( 1, 'Categoria não informada.', "error" )
        }

        if( !params.dsJustificativa && params.sqCategoriaIucn && params.cdCategoriaIucn != 'NE' ) {
            return this._returnAjax( 1, 'Jutificativa da avaliação é obrigatória.', "error" )
        }

        DadosApoio categoria = null
        if( params.sqCategoriaIucn ) {
            categoria = DadosApoio.get( params.sqCategoriaIucn.toLong() )
            if( !categoria ) {
                return this._returnAjax( 1, 'Categoria inválida.', "error" )
            }
        }

        if( ficha.situacaoFicha.codigoSistema == 'PUBLICADA' ) {
            if( !categoria || categoria.codigoSistema == 'NE' ) {
                return this._returnAjax( 1, 'Ficha publicada deve ter o resultado da validação.', "error" )
            }
        }

        // SE A FICHA ESTIVER EM VALIDACAO OU VALIDADA EM REVISAO E A ULTIMA OFICINA AINDA ESTIVER ABERTA NÃO PERMITIR
        // ALTERAR PELA ABA 11.7
        List nomesOficinasValidacaoAbertas = []
        if ( ficha.situacaoFicha.codigoSistema =~ /^(EM_VALIDACAO|VALIDADA_EM_REVISAO)$/) {
            OficinaFicha.createCriteria().list{
                eq('ficha', ficha)
                oficina {
                    tipoOficina {
                        eq('codigoSistema', 'OFICINA_VALIDACAO')
                    }
                    situacao {
                         eq('codigoSistema', 'ABERTA')
                    }
                }
            }.each {
                nomesOficinasValidacaoAbertas.push(it.oficina.noOficina)
            }
            if( nomesOficinasValidacaoAbertas ){
                return this._returnAjax( 1, 'Gravação NÃO FOI realizada devido a ficha estar sendo validada na oficina:<br>'+nomesOficinasValidacaoAbertas.join('<br>'), "error" )
            }
        }


            if( categoria ) {
            if( categoria.codigoSistema ==~ /VU|CR|EN/ ) {
                if( !params.dsCriterioAvalIucn ) {
                    return this._returnAjax( 1, 'Critério não informado.', "error" )
                }
            }
            else {
                params.dsCriterioAvalIucn = null
            }

            // possivelmente extinta
            if( categoria.codigoSistema == 'CR' ) {
                if( params.stPossivelmenteExtinta != 'S' ) {
                    params.stPossivelmenteExtinta = 'N'
                }
            }
            else {
                params.stPossivelmenteExtinta = null
            }


            // validar mudanca de categoria
            Map dadosUltimaAvaliacao = ficha.ultimaAvaliacaoNacionalHist
            if( categoria.codigoSistema != 'NE' && dadosUltimaAvaliacao.sqCategoriaIucn && dadosUltimaAvaliacao.sqCategoriaIucn.toLong() != categoria.id.toLong() ) {
                if( FichaMudancaCategoria.countByFicha( ficha ) == 0 ) {
                    return this._returnAjax( 1, 'Motivo da mudança de categoria deve ser informado.', "error" )
                }
                ficha.dsJustMudancaCategoria = params.dsJustMudancaCategoria
            }
            else {
                // verificar se houve mudanca de categoria na AVALIACAO, se não houve pode excluir os motivos
                if( !ficha.categoriaIucn || !dadosUltimaAvaliacao.sqCategoriaIucn || dadosUltimaAvaliacao.sqCategoriaIucn.toLong() == ficha.categoriaIucn.id.toLong() ) {
                    ficha.dsJustMudancaCategoria = null
                    FichaMudancaCategoria.findAllByFicha( ficha ).each {
                        it.delete()
                    }
                }

            }

            // gravar resultado da validacao
            ficha.categoriaFinal = categoria
            ficha.dsJustificativaFinal = params.dsJustificativa
            ficha.dsCriterioAvalIucnFinal = params.dsCriterioAvalIucn
            ficha.stPossivelmenteExtintaFinal = params.stPossivelmenteExtinta
        }
        else {
            ficha.categoriaFinal = null
            ficha.dsJustificativaFinal = null
            ficha.dsCriterioAvalIucnFinal = null
            ficha.stPossivelmenteExtintaFinal = null
            // verificar se houve mudanca de categoria na AVALIACAO, se não houve pode excluir os motivos
            if( !ficha.categoriaIucn ) {
                ficha.dsJustMudancaCategoria = null
                FichaMudancaCategoria.findAllByFicha( ficha ).each {
                    it.delete()
                }
            }

        }
        ficha.save( flush: true )


        /**
         * REGRA DESATIVADA POR DECISÃO DA EQUIPE SALVE - COORDENACAO EM REUINÃO
        // SE A FICHA ESTIVER EM VALIDAÇÃO, SINCRONIZAR O RESULTADO DA OFICINA COM
        // O QUE ESTA SENDO GRAVADO NA ABA 11.7
        if( ficha.categoriaFinal && Util.stripTags(ficha.dsJustificativaFinal) !='' ) {
            // se a ficha estiver em validação, atualizar o resultado da oficina
            if (ficha.situacaoFicha.codigoSistema =~ /^(EM_VALIDACAO|VALIDADA|VALIDADA_EM_REVISAO|FINALIZADA)$/) {
                DadosApoio oficinaValidacao = dadosApoioService.getByCodigo('TB_TIPO_OFICINA', 'OFICINA_VALIDACAO')
                OficinaFicha oficinaFicha = OficinaFicha.createCriteria().get {
                    createAlias('oficina', 'o')
                    eq('ficha', ficha)
                    eq('o.tipoOficina', oficinaValidacao)
                    order('o.dtFim', 'desc')
                    maxResults(1)
                }
                if ( oficinaFicha ) {
                    // SE O OFICINA AINDA ESTIVER ABERTA E JÁ POSSUIR UMA CATEGORIA
                    // ENTÃO PODE SER ALTERADA PORQUE OS VALIDADORES JÁ ENTRARAM EM CONSENSO
                    if( oficinaFicha.categoriaIucn && oficinaFicha.oficina.situacao.codigoSistema == 'ABERTA') {
                        oficinaFicha.categoriaIucn = ficha.categoriaFinal
                        oficinaFicha.dsJustificativa = ficha.dsJustificativaFinal
                        oficinaFicha.dsCriterioAvalIucn = ficha.dsCriterioAvalIucnFinal
                        oficinaFicha.stPossivelmenteExtinta = ficha.stPossivelmenteExtintaFinal
                        oficinaFicha.save(flush: true);
                    }
                }
            }
        }
         */
        return this._returnAjax( 0, getMsg( 1 ), "success" )
    }

    //-----------------------------------------------------------------------------

    def tabAvaValidacao() {
        if( !params.sqFicha ) {
            render( 'ID da ficha não informado.' )
            return;
        }
        Ficha ficha = Ficha.get( params.sqFicha )
        List listCategoriaIucn = dadosApoioService.getTable( 'TB_CATEGORIA_IUCN' ).findAll {
            it.codigoSistema != 'OUTRA' && it.codigoSistema != 'AMEACADA' && it.codigoSistema != 'POSSIVELMENTE_EXTINTA'
        }.sort {
            it.ordem
        };
        render( template: "/ficha/avaliacao/formValidacao",
            model: [ 'ficha'            : ficha,
                     'listCategoriaIucn': listCategoriaIucn ] );
    }

    def saveFrmValidacao() {
        if( !params.sqFicha ) {
            return this._returnAjax( 1, 'ID da ficha não informado.', "error" )
        }

        String categoriaIucn = '';
        if( params.stCategoriaOk == 'N' ) {
            if( !params.sqCategoriaIucnSugestao ) {
                return this._returnAjax( 1, 'Informe a categoria sugerida', "error" )
            }
            params.categoriaIucnSugestao = DadosApoio.get( params.sqCategoriaIucnSugestao )
            categoriaIucn = params.categoriaIucnSugestao.codigoSistema
        }

        if( params.stCategoriaOk == 'S' || params.stCriterioAvalIucnOk == 'S' ) {
            params.dsCriterioAvalIucnSugerido = null;
        }
        else {

            if( categoriaIucn == 'VU' || categoriaIucn == 'CR' || categoriaIucn == 'EN' ) {
                if( !params.dsCriterioAvalIucnSugerido ) {
                    return this._returnAjax( 1, 'Informe o critério sugerido', "error" )
                }
            }
            else {
                params.dsCriterioAvalIucnSugerido = null;
            }
        }
        Ficha ficha = Ficha.get( params.sqFicha )
        ficha.properties = params
        def now = new Date();
        String data = now.format( "dd/MM/yyyy HH:mm:ss", TimeZone.getTimeZone( 'UTC' ) )
        String nome = session?.sicae?.user.noUsuario;
        if( params.dsJustificativaValidacaoNova ) {
            ficha.dsJustificativaValidacao = ( ficha.dsJustificativaValidacao ? '<hr>' + ficha.dsJustificativaValidacao : '' ) + '<b>' + nome + ' - ' + data + '</b>' + params.dsJustificativaValidacaoNova.trim();
        }
        //ficha.dsJustificativaValidacao=null;
        try {
            ficha.save( flush: true )
            this._returnAjax( 0, getMsg( 1 ), "success", [ dsJustificativaValidacao: ficha.dsJustificativaValidacao ] )
        }
        catch( Exception e ) {
            println e.getMessage()
            this._returnAjax( 1, getMsg( 2 ), "error" )
        }
    }

    //-----------------------------------------------------------------------------

    def saveFrmResultadoFinal() {
        if( !params.sqFicha ) {
            return this._returnAjax( 1, 'ID da ficha não informado.', "error" )
        }
        Ficha ficha = Ficha.get( params.sqFicha )
        /*if( ficha.situacaoFicha.codigoSistema == 'COMPILACAO')
        {
            return this._returnAjax(1, 'Ficha em COMPILAÇÃO, resultado final não pode ser gravado.', "error")
        }
        */
        if( !params.stPossivelmenteExtintaFinal ) {
            params.stPossivelmenteExtintaFinal = null
        }
        params.categoriaFinal = null
        if( params.sqCategoriaFinal ) {
            params.categoriaFinal = DadosApoio.get( params.sqCategoriaFinal )
            String categoriaIucn = params.categoriaFinal.codigoSistema
            if( categoriaIucn != 'CR' ) {
                params.stPossivelmenteExtintaFinal = null
            }
            else {
                if( !params.stPossivelmenteExtintaFinal ) {
                    params.stPossivelmenteExtintaFinal = 'N'
                }
            }
            if( categoriaIucn == 'VU' || categoriaIucn == 'CR' || categoriaIucn == 'EN' ) {
                if( !params.dsCriterioAvalIucnFinal ) {
                    return this._returnAjax( 1, 'Informe o Critério', "error" )
                }
            }
            else {
                params.dsCriterioAvalIucnFinal = null
                params.stPossivelmenteExtintaFinal = null
            }
        }
        else {
            params.dsCriterioAvalIucnFinal = null
            params.stPossivelmenteExtintaFinal = null
            params.dsJustificativaFinal = null
        }

        ficha.properties = params
        try {
            ficha.save( flush: true )
            this._returnAjax( 0, getMsg( 1 ), "success" )
        }
        catch( Exception e ) {
            println e.getMessage()
            this._returnAjax( 1, getMsg( 2 ), "error" )
        }
    }


}
