/**
 * Módulo padrão para manutenção das tabelas auxiliares do SIS/IUCN
 * 
 */
package br.gov.icmbio

class IucnTabelasController extends BaseController {
    def sqlService
    protected List _getData( String tableName='',Long id = 0l ) {
        List data = []
        if( tableName ) {
            String table = Util.toSnakeCase(tableName)
            String sqlWhere = ''
            if( id ){
                sqlWhere = """where sq_${table} = ${id.toString()}"""
            }
            String cmdSql = """select sq_${table} as id, cd_${table} as codigo, ds_${table} as descricao, ds_${table}_ordem as ordem from salve.${table} ${sqlWhere} order by ds_${table}_ordem"""
            Map sqlParams = [:]
            data = sqlService.execSql(cmdSql, sqlParams)
        }
        return data
    }

    /**
     * método para exibição da tela inicial
     * @return
     */
    def index() {
        if( ! params.tableName ){
            render 'Parâmetros insuficientes.<br>- Necessário informar o parametro tableName'
            return
        }
        render (view: 'index')
    }

    /**
     * método para criação do gride de registros da tabela selecionada
     * @return
     */
    def getGrid() {
        List rows = _getData(params.tableName)
        render(template: "gridIucnTabelas", model: [rows : rows ])
    }

    /**
     * método para criação do formulário de edição dos registros
     * @return
     */
    def getForm() {
        Map reg = [id:'',codigo:'', descricao:'', ordem:'']
        if( params.id ){
            List rows = _getData(params.tableName, params.id.toLong() )
            reg = rows[0]
        }
        render( template:'frmIucnTabelas',model:[reg:reg]);
    }

    /**
     * método para gravação do registro no banco de dados
     * @return
     */
    def save(){
        String table = Util.toSnakeCase(params.tableName)
        String cmdSql
        Map sqlParams = [:]
        sqlParams.codigo    = params.codigo
        sqlParams.descricao = params.descricao
        sqlParams.ordem     = params.ordem
        if( params.id ) {
            cmdSql = """update salve.${table} set cd_${table} = :codigo, ds_${table} = :descricao, ds_${table}_ordem = :ordem where sq_${table} = :id"""
            sqlParams.id = params.id.toLong()
        } else {
            cmdSql = """insert into salve.${table} (cd_${table}, ds_${table}, ds_${table}_ordem) values (:codigo, :descricao, :ordem) returning sq_${table} as id"""
        }
        List row = sqlService.execSql( cmdSql, sqlParams )
        return this._returnAjax(0,'Dados gravados com SUCESSO!','success',[id:params.id ?: row.id ]);
    }

    /**
     * método para exclusão do registro do banco de dados
     * @return
     */
    def delete() {
        if( params.id && params.tableName) {
            String table = Util.toSnakeCase(params.tableName)
            sqlService.execSql("""delete from salve.${table} where sq_${table} = ${params.id};""")
            return this._returnAjax(0,'Exclusão realizada com SUCESSO!', 'success');
        }
        return this._returnAjax(1, 'Id não informado.', "error")
    }
}
