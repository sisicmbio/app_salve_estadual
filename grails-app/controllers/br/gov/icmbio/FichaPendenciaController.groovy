package br.gov.icmbio
import grails.converters.JSON

class FichaPendenciaController extends FichaBaseController{

	def dadosApoioService
	def fichaService

    def index() {
		render ''
     }

     def saveFrmPendencia() {

		 if (!session?.sicae?.user?.sqPessoa) {
			 return this._returnAjax(1, 'Usuário não definido!', "error")
		 }
		 if ( params.contexto == 'validador' ) {
			 params.contexto = dadosApoioService.getByCodigo('TB_CONTEXTO_PENDENCIA', 'VALIDACAO')
		 } else if( params.contexto == 'revisao' ) {
			 return _savePendenciaRevisao()
		 } else  {
			 params.contexto = dadosApoioService.getByCodigo('TB_CONTEXTO_PENDENCIA', 'FICHA')
		 }

		 FichaPendencia reg;
		 if (params.sqFichaPendencia) {
			 reg = FichaPendencia.get(params.sqFichaPendencia)
		 } else {
			 reg = new FichaPendencia()
			 reg.usuario = PessoaFisica.get(session?.sicae?.user?.sqPessoa) // pessoa logada
			 reg.ficha = Ficha.get(params.sqFicha)
			 reg.dtPendencia = new Date()
		 }
         // se a ficha já possuir o contexto então não sobrescrever
         if( reg.contexto ){
             params.remove( 'contexto');
         }
		 reg.properties = params
         if( reg.stPendente=='S') {
             reg.usuarioResolveu=null
             reg.dtResolvida=null
             reg.usuarioRevisou=null
             reg.dtRevisao=null
         }

		 if (!reg.txPendencia && reg.id) {
			 reg.delete(flush: true);
			 this._returnAjax(0, 'Pendência excluída com SUCESSO!', "success")
			 return
		 }

		 if (reg.validate()) {
			 try {
				 reg.save(flush: true)
				 fichaService.updatePercentual(reg.ficha.id)
                 // se a ficha estivar na situação finalizada e for adicionada uma pendência,
                 // retornar para a situação validada ( Em revisão )
                 String mensagemComplementar = ''
                 Map data=[:]
                 if( reg.ficha.situacaoFicha.codigoSistema =='FINALIZADA' ){
                     VwFicha vwFicha = VwFicha.get( reg.ficha.id)
                     if( vwFicha.nuPendencia > 0 ) {
                         DadosApoio situacaoValidadaEmRevisao = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA','VALIDADA_EM_REVISAO')
                         if( situacaoValidadaEmRevisao ) {
                             reg.ficha.situacaoFicha = situacaoValidadaEmRevisao
                             reg.ficha.save( flush:true )
                             mensagemComplementar ='<br>Situação da ficha for alterada para <b>'+situacaoValidadaEmRevisao.descricao+'</b>'

                             // retornar nova situacao para alterar a situacao exibida na parte de cima da tela da ficha
                             data.deSituacaoFicha = situacaoValidadaEmRevisao.descricao
                             data.sqSituacaoFicha = situacaoValidadaEmRevisao.id
                         }
                     }
                 }
				 this._returnAjax(0, getMsg(1)+mensagemComplementar, "success", data )
			 } catch (Exception e) {
				 println e.getMessage()
				 this._returnAjax(1, getMsg(2), "error")
			 }
		 }
		 this._returnAjax(1, getMsg(2), "error")
	 }

    /**
     * adicioanr pendencia de revisao nas fichas
     * @return
     */
     protected _savePendenciaRevisao() {
         Map res = [ status: 0, msg: '', type: 'success' ]
         FichaPendencia fichaPendencia
         try {
             if( ! params.txPendencia ) {
                 throw new Exception('Texto da pendência não informado')
             }
             if( params.sqFichaPendencia ){
                 fichaPendencia = FichaPendencia.get(params.sqFichaPendencia)
                 fichaPendencia.stPendente = params.stPendente
                 fichaPendencia.txPendencia = params.txPendencia
                 if( fichaPendencia.stPendente == 'S') {
                     fichaPendencia.usuarioResolveu=null
                     fichaPendencia.dtResolvida=null
                     fichaPendencia.usuarioRevisou=null
                     fichaPendencia.dtRevisao=null
                 }
                 fichaPendencia.save( flush:true )
                 _savePendenciaRevisaoEmail( fichaPendencia)
             } else {
                 if( ! params.sqFichas ) {
                     throw new Exception('Nenhuma ficha selecionada')
                 }
                 DadosApoio pendenciaRevisao = dadosApoioService.getByCodigo('TB_CONTEXTO_PENDENCIA','REVISAO')
                 if( !pendenciaRevisao ) {
                     throw new Exception('Contexto REVISAO não cadastrado na tabela TB_CONTEXTO_PENDENCIA')
                 }
                 params.sqFichas.split(',').each { sqFicha ->

                     fichaPendencia = null
                     Ficha ficha = Ficha.get(sqFicha.toLong())

                     // se a pendencia já existir ignorar
                     if (!fichaPendencia) {
                         fichaPendencia = new FichaPendencia()
                         fichaPendencia.contexto = pendenciaRevisao
                         fichaPendencia.ficha = ficha
                         fichaPendencia.usuario = PessoaFisica.get(session.sicae.user.sqPessoa.toLong())
                         fichaPendencia.stPendente = 'S'
                         fichaPendencia.dtPendencia = new Date()
                         fichaPendencia.txPendencia = params.txPendencia
                         fichaPendencia.save()
                         _savePendenciaRevisaoEmail( fichaPendencia )
                     }
                 }
             }
             res.msg = 'Dados gravados com SUCESSO!'
         } catch( Exception e) {
             res.type='error'
             res.status=1
             res.msg=e.getMessage()
             println e.getMessage();
         }
         render res as JSON
     }

    /**
     * registrar o envio do email para o ponto focal se ainda não existier
     * @param fichaPendencia
     */
    protected void _savePendenciaRevisaoEmail( FichaPendencia fichaPendencia ) {
        // gravar o envio de email para o ponto focal
        if( fichaPendencia.stPendente == 'S') {
            // verifica se ja tem email agendado
            int qtd = FichaPendenciaEmail.createCriteria().count{
                eq('fichaPendencia',fichaPendencia)
                isNull('dtEnvio')
            }
            if( qtd == 0 ){
                FichaPendenciaEmail fichaPendenciaEmail = new FichaPendenciaEmail()
                fichaPendenciaEmail.fichaPendencia = fichaPendencia
                fichaPendenciaEmail.save()
            }
        } else {
            // excluir o envio do email se a pendencia estiver resolvida
            FichaPendenciaEmail.createCriteria().list {
                eq('fichaPendencia',fichaPendencia)
                isNull('dtEnvio')
            }.each{
                it.delete(flush:true)
            }
        }
    }

     def getGridPendencia() {
		 Ficha ficha
		 List listPendencias
		 if ( params.sqFicha) {

			 // clicou na aba pendencia da ficha
			 if (params.container == 'tabPendencia') {
				 params.onClose = 'ficha.updateGridPendencia'
			 }
			 ficha = Ficha.get(params.sqFicha.toLong() )
			 listPendencias = FichaPendencia.findAllByFicha(ficha, ['sort': 'dtPendencia', 'order': 'desc'])
		 }
         render(template:"/ficha/pendencia/divGridPendencias", model:['ficha': ficha,listPendencias:listPendencias])
    }

	def editPendencia()
	{
    	FichaPendencia reg = FichaPendencia.get(params.id);
		if( reg?.id )
		{
			render reg.asJson();
            return;
		}
		render [:] as JSON;
	}

	def deletePendencia()
	{
		FichaPendencia reg = FichaPendencia.get(params.id);
		if( reg )
		{
			reg.delete(flush:true);
			fichaService.updatePercentual(reg.ficha.id)
			return this._returnAjax(0, getMsg(4), "success")
		}
		else
		{
			return this._returnAjax(1, getMsg(51), "error")
		}
		render ''
	}
}
