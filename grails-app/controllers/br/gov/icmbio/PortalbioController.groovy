package br.gov.icmbio
import grails.converters.JSON;
class PortalbioController {

    def requestService

    def index() { }

    def getOcorrencias()
    {

        // Metodo subustituido pelo FichaDistribuicao.getPoints()
        // Teste: http://localhost:8090/salve-estadual/fichaDistribuicao/getPoints?sqFicha=25&bd=portalbio
        //
            /*String fileName = '/home/luis/projetos/grails/sae/docs/retorno_search_portalbio.txt';
            File  file = new File( fileName );
            if( ! file.exists() )
            {
                render 'Arquivo coords.txt não existe';
                return;
            }
            response.setHeader("Content-Type", "application/json");
            //render(file: file, fileName: 'coordenadas.txt', contentType:'application/json');
            render file.text;
            return;
            */
		// http://10.197.94.48:8080/solr/biocache/select?q=*%3A*&fq=Puma+concolor&wt=json&indent=true
		// http://10.197.94.48:8080/solr/biocache/select?q=taxon_name%3A%22Puma+Concolor%22&wt=json&indent=true
		// http://10.197.94.48:8080/solr/biocache/select?q=*%3A*&fq=Puma+Concolor&start=0&rows=10000000&fl=institution_code%2Ccollection_code%2Ccatalogue_number%2Clatitude%2Clongitude%2Cdata_provider%2Cdata_resource%2Ccommon_name%2Crecord_number%2Clast_load_date%2Ccollector%2Craw_locality%2CSensivel_s%2CAmeaca_s%2CcategoriaAmeaca_s%2Cmunicipality%2Cdispositio&wt=json&indent=true


/*
        if( !grailsApplication?.config?.url?.solr )
        {
            render '{}'
            println 'Constante url.solr não definido no arquivo de confirações'
            return;
        }
    	response.setHeader("Content-Type", "application/json");
        String url=grailsApplication.config.url.solr+"/biocache/select";
        if(!params.noCientifico)
        {
            render '{}'
        }
        else
        {
            String par = 'q=raw_taxon_name:"'+params.noCientifico+'"&start=0&rows=1000000000&fl=institution_code,collection_code,catalogue_number,latitude,longitude,data_provider,data_resource,common_name,record_number,last_load_date,collector,raw_locality,Sensivel_s,Ameaca_s,categoriaAmeaca_s,municipality,disposition&wt=json';
            def dados = requestService.doPost( url,par,"" );
            if( ! dados )
            {
                render '{}';
                return;
            }
            render dados;
        }
        */
    }
}
