package br.gov.icmbio

import grails.converters.JSON

class CorpRegiaoController extends BaseController {

    def corpRegiaoService

    /**
     * Exibir o formulário de manutenção das Regiões
     * @return
     */
    def index() {
        render(view:'index',model:[regioes:Regiao.list() ] )
    }

    /**
     * Gravar/atualizar a Região no banco de dados
      * @return
     */
    def save(){
        Map res = [status:0, type:'success', msg:'Região gravada com SUCESSO', data:[:] ]
        try {
            corpRegiaoService.save(  params.sqRegiao ? params.sqRegiao.toLong() : null
                                   , params.noRegiao ?:''
                                   , params.nuOrdem  ? params.nuOrdem.toInteger(): null
                                   , params.wkt      ?:'')

        } catch( Exception e ){
            res.status = 1
            res.type = 'error'
            res.msg = e.getMessage()
        }
        render res as JSON
    }

    /**
     * retornar os dados da região para edição
     * @return
     */
    def edit(){
        Map res = [status:0, type:'success', msg:'', data:[:] ]
        try {
            res.data = corpRegiaoService.edit(  params.sqRegiao ? params.sqRegiao.toLong() : 0l)
        } catch( Exception e ){
            res.status = 1
            res.type = 'error'
            res.msg = e.getMessage()
        }
        render res as JSON
    }

    /**
     * excluir a região do banco de dados
     * @return
     */
    def delete(){
        Map res = [status:0, type:'success', msg:'Região excluída com SUCESSO', data:[:] ]
        try {
            res.data = corpRegiaoService.delete(  params.sqRegiao ? params.sqRegiao.toLong() : 0l)
        } catch( Exception e ){
            res.status = 1
            res.type = 'error'
            res.msg = e.getMessage()
        }
        render res as JSON
    }

    /**
     * criar o gride das regioes
     * @return
     */
    def grid(){
        try {
            render(template: 'gridRegiao', model: [regioes: Regiao.list()])
        } catch( Exception e ) {
            render e.getMessage()
        }
    }
}
