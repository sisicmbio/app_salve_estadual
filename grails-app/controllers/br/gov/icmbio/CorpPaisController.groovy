package br.gov.icmbio

import grails.converters.JSON

class CorpPaisController extends BaseController {

    def corpPaisService

    /**
     * Exibir o formulário de manutenção dos Países
     * @return
     */
    def index() {
        render(view:'index',model:[paises:Pais.list() ] )
    }

    /**
     * Gravar/atualizar a País no banco de dados
      * @return
     */
    def save(){
        Map res = [status:0, type:'success', msg:'País gravado com SUCESSO', data:[:] ]
        try {
            corpPaisService.save(  params.sqPais ? params.sqPais.toLong() : null
                                   , params.noPais ?:''
                                   , params.coPais ?:''
                                   , params.wkt    ?:'')

        } catch( Exception e ){
            res.status = 1
            res.type = 'error'
            res.msg = e.getMessage()
        }
        render res as JSON
    }

    /**
     * retornar os dados da país para edição
     * @return
     */
    def edit(){
        Map res = [status:0, type:'success', msg:'', data:[:] ]
        try {
            res.data = corpPaisService.edit(  params.sqPais ? params.sqPais.toLong() : 0l)
        } catch( Exception e ){
            res.status = 1
            res.type = 'error'
            res.msg = e.getMessage()
        }
        render res as JSON
    }

    /**
     * excluir a país do banco de dados
     * @return
     */
    def delete(){
        Map res = [status:0, type:'success', msg:'País excluído com SUCESSO', data:[:] ]
        try {
            res.data = corpPaisService.delete(  params.sqPais ? params.sqPais.toLong() : 0l)
        } catch( Exception e ){
            res.status = 1
            res.type = 'error'
            res.msg = e.getMessage()
        }
        render res as JSON
    }

    /**
     * criar o gride das regioes
     * @return
     */
    def grid(){
        try {
            render(template: 'gridPais', model: [paises: Pais.list()])
        } catch( Exception e ) {
            render e.getMessage()
        }
    }


}
