package br.gov.icmbio

import grails.converters.JSON
import org.codehaus.groovy.grails.web.json.JSONObject

class FichaVersaoController {

    def fichaVersaoService

    def index() {
        FichaVersao fichaVersao
        JSONObject jsonFicha
        FichaHistSituacaoExcluidaVersao fichaHistSituacaoExcluidaVersao
        try {
            fichaVersao = fichaVersaoService.findFichaVersao( params )
            jsonFicha = fichaVersaoService.loadJsonVersao( fichaVersao )

            if( fichaVersao.contexto.codigoSistema != 'PUBLICACAO' ){
                fichaHistSituacaoExcluidaVersao = FichaHistSituacaoExcluidaVersao.findByFichaVersao( fichaVersao )
            }

            render( 'view':'index','model':[ fichaVersao : fichaVersao
                                            , jsonFicha  : jsonFicha
                                            , fichaHistSituacaoExcluidaVersao : fichaHistSituacaoExcluidaVersao
                                            ])
        } catch ( Exception e ){
            render '<div class="mt20 alert alert-danger">'+ e.getMessage()+'</div>'
        }
    }

    /**
     * método para listar os registros de ocorrência da ficha versionada
     */
    def getGridOcorrencias(){
        Map data = [rows:[],pagination: [:]]
        try {
            data = fichaVersaoService.getOcorrencias(params.sqFichaVersao ? params.sqFichaVersao.toLong() : 0l)
        } catch( Exception e ){
            println e.getMessage()
        }
        render(template: "divGridOcorrenciasVersao", model: [gridId:'GridOcorrenciasVersao',rows:data.rows,pagination:data.pagination])
        //render(template: "divGridOcorrenciasVersao", model: [gridId:'GridOcorrenciasVersao',rows:[],pagination:[:]])
    }

    /**
     * método para exibir a fudamentacao e o chat da validacao da ficha versionada
     */
    def fundamentacaoVersao(){
        try {
            Map dadosFundamentacao = fichaVersaoService.dadosFundamentacao( params.sqOficinaFicha ? params.sqOficinaFicha.toLong() : 0l )
            render( 'view':'fundamentacaoVersao','model':[dadosFundamentacao:dadosFundamentacao])
        } catch ( Exception e ){
            render '<div class="mt20 alert alert-danger">'+ e.getMessage()+'</div>'
        }
    }

    /**
     * método para atualizar o JSON da ficha versionada
     */
    def updateJson(){

        /** /
        println ' '
        println 'params'
        println params
         /**/
        Map res = [status:0, msg:'',type:'success']
        try {
            fichaVersaoService.updateJson(
                                 params.sqFichaVersao ? params.sqFichaVersao.toLong():0l
                                ,params.key
                                ,params.value
                                ,params.table
                                ,params.column)
        } catch(Exception e ){
            res.status=1
            res.type='error'
            res.msg = e.getMessage()
        }
        render res as JSON
    }
}
