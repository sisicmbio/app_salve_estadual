/**
 * Este controlador é resposável por receber as ações referentes ao cadastro aba Distribuição da ficha de espécies
 * https://www.dataiku.com/learn/guide/other/geo/convert-coordinates-with-PostGIS.html
 * Regras:
 * - Quando a precisão da coordenada for APROXIMADA, o campo referencia da aproximação é obrigatório
 *
 *
 **/

package br.gov.icmbio
import com.vividsolutions.jts.geom.GeometryFactory
import com.vividsolutions.jts.geom.Coordinate
import groovy.sql.Sql
import net.sf.ehcache.search.parser.MOrderBy
import org.codehaus.groovy.grails.web.servlet.mvc.GrailsHttpSession
import grails.converters.JSON
import org.codehaus.groovy.grails.web.json.JSONObject
class FichaDisOcorrenciaController extends FichaBaseController {

    def dadosApoioService
    def messageSource
    def exportDataService
    def geoService
    def fichaService
    def dataSource
    def asyncService
    def sqlService
    def registroService

    def index() {
        render(template: "/ficha/distribuicao/ocorrencia/index");
    }

    def getGridOcorrencia() {
        String cmdSql
        List whereGeral = []

        List rows
        Integer paginationPageSize = 100

/** /
println ' '
println 'GETGRIDOCORRENCIA()'
println params
/**/

        // ordenacao padrão do resultado
        String sqlOrderBy = "ORDER BY motor.no_bd desc"
        // ordenar pela coluna clicada
        if( params.sortColumns ){
            List sortColumns = params.sortColumns.split(':')
            if( sortColumns[0] == 'no_localidade') {
                sortColumns[0] = 'public.fn_remove_acentuacao( trim(lower(motor.no_localidade)) )'
            } else if( sortColumns[0] == 'bd' ) {
                sortColumns[0] = 'no_bd'
            }

            sqlOrderBy = '\nORDER BY ' + sortColumns[0]
            // adicionar DESC / ASC
            if( sortColumns[1] && sortColumns[1].toString().toUpperCase() == 'DESC' ) {
                sqlOrderBy += ' DESC NULLS LAST'
            }

            if( sortColumns[0].toString().toLowerCase() != 'no_bd') {
                sqlOrderBy += ',no_bd DESC';
            }
        }

        if ( ! params.sqFicha) {
            render 'Id da ficha não informado!'
            return;
        }

        VwFicha vwFicha = VwFicha.get(params.sqFicha.toLong())
        // encontrar os ids das subespecies da ficha selecionada
        /*List idsSubespecies = []
        //DadosApoio situacaoExcluida = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA','EXCLUIDA')
        sqlService.execSql("""select sq_ficha from salve.ficha
                inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                where taxon.sq_taxon_pai = ${vwFicha.sqTaxon.toString()}
                and ficha.sq_ciclo_avaliacao = ${vwFicha.sqCicloAvaliacao.toString()}
                """).each {
            idsSubespecies.push(it.sq_ficha)
        }

        boolean incluirOcorrenciasPortalbio = true*/

        Map pars = [:]
        // filtrosSalve
        List whereSalve = []
        // filtrosPortalbio
        List wherePortalbio = []

        // se for informado o ID não aplicar filtros
        if ( ! params.idFiltro && ! params.rowId) {

            if ( params.bdFiltro ) {
                pars.bd = '%' + params.bdFiltro + '%'
                whereGeral.push("motor.no_fonte ilike :bd")
            }

            if (params.txCoordenadaFiltro) {
                if (params.txCoordenadaFiltro.toString().replaceAll(/,/, '.').isNumber()) {
                    pars.txCoordenada = Double.parseDouble(params.txCoordenadaFiltro.replaceAll(/,/, '.'))
                    whereGeral.push("( motor.nu_x = :txCoordenada or motor.nu_y = :txCoordenada)")
                }
            }

            if (params.noMunicipioFiltro) {
                pars.noMunicipio = '%' + Util.removeAccents( params.noMunicipioFiltro ) + '%'
                whereGeral.push("public.fn_remove_acentuacao( motor.no_municipio ) ilike :noMunicipio")
            }

            if (params.sgEstadoFiltro) {
                pars.sgEstado = params.sgEstadoFiltro
                whereGeral.push("motor.no_estado ilike :sgEstado")
            }

            if (params.noLocalidadeFiltro) {
                pars.noLocalidade = '%' + Util.removeAccents(params.noLocalidadeFiltro) + '%'
                whereGeral.push("public.fn_remove_acentuacao( motor.no_localidade ) ilike :noLocalidade" )
            }
        }

        // bd recebido do mapa função "Localiazar no Gride"
        if( params.bd ) {
            sqlOrderBy +=  (sqlOrderBy ? ',':'') + 'id' // adicionar o id na clausula order by para funcionar a localizacao do registro no gride
            cmdSql = "select motor.*, row_number() over ( ORDER BY NULL ) as row_number from (\n select * from salve.fn_ficha_grid_registros( :sqFicha ) ${sqlOrderBy} ) as motor"
        }
        else {
            cmdSql = "select motor.* from salve.fn_ficha_grid_registros( :sqFicha ) as motor"
        }
        pars.sqFicha = params.sqFicha.toLong()

        // adicionar os filtros no resultado dos UNION ALL da query
        if ( params.dtOrdenarFiltro &&  params.dtOrdenarFiltro ) {
            params.dtOrdenarFiltro = params.dtOrdenarFiltro.replaceAll(/\//, '-')
            params.dtOrdenarFiltro = params.dtOrdenarFiltro.split('-').reverse().join('-')
            whereGeral.push("motor.dt_ordenar ilike :dtOrdenar")
            pars.dtOrdenar = '%' + params.dtOrdenarFiltro + '%'
        }


        if( params.rowId ) {
            whereGeral.push("motor.id_row = :rowId")
            pars.rowId = params.rowId.toString();
        }
        if ( ! params.idFiltro && !params.rowId) {
            if (params.refBibFiltro) {
                pars.refBib = '%' + Util.removeAccents(params.refBibFiltro) + '%'
                whereGeral.push("motor.json_ref_bib is not null and public.fn_remove_acentuacao(motor.json_ref_bib::text) ilike :refBib")
            }
        }

        // não exibir os registros em carência
        if( ! session.sicae.user.canViewRegsCarencia() ) {
            whereGeral.push("( motor.dt_carencia is null or motor.dt_carencia < '${new Date().format('yyyy-MM-dd')}')")
        }

        if ( whereGeral ) {
            cmdSql += '\nWHERE ' + whereGeral.join('\nand ')
        }

        // adicionar order by
        cmdSql += '\n'+sqlOrderBy + ', motor.nu_y, motor.nu_x'

        /**
         * Quando utilizar a opção "Localizar no Gride" do mapa, é
         * preciso localizar a página que o registro está
         */
        if( params.idFiltro && params.bd && paginationPageSize ) {
            rows = sqlService.execSql('select tmp.row_number from (' + cmdSql + ') as tmp where tmp.no_bd = \'' + params.bd.toLowerCase() + '\' and tmp.id = ' + params.idFiltro, pars)
            //println 'Encontrar em qual página está a linha do pontos selecionado no mapa
            //println  'select tmp.row_number from (' + cmdSql + ') as tmp where tmp.no_bd = \'' + params.bd.toLowerCase() + '\' and tmp.id = ' + params.idFiltro
            //println pars
            if ( rows ) {
                params.paginationPageNumber = Math.max(1, Math.ceil(rows[0].row_number / paginationPageSize)).toInteger()
                //println 'Pagina encontrada: ' + params.paginationPageNumber
            }
        }

         /** /
         // zzz
         println ' '
         println cmdSql
         println pars
         println whereGeral

        println ' '
        println ' '
        println ' '
        println ' '
        println '='*50
        render ''
        /**/

        Map paginate = sqlService.paginate( cmdSql, pars, params, paginationPageSize )
        rows = paginate.rows
        Map pagination = paginate.pagination
        if( paginate.error ){
            render paginate.error
            return
        }

        // se for a ficha de uma especie, não permitir alterar referencia bibliografica nas subespecies exibidas no gride
        boolean isFichaSubespecie = vwFicha.coNivelTaxonomico =='SUBESPECIE'

        // fazer tratamento dos nomes dos autores e imprimir somente o primeiro com .et al. no final
        // Publicacao publicacao = new Publicacao()
        // criar os dados computados para exibição no gride
        rows.each { row ->
            // colunas do gride
            row.rowId = row.id_row ?: ''
            row.noCientificoCompletoItalico = Util.ncItalico(row.nm_cientifico, false, true, row?.co_nivel_taxonomico)
            row.coordenadas = 'Lat:&nbsp;' + row?.nu_y + '<br>Lon:&nbsp;' + row?.nu_x + '<br>Datum:&nbsp;' + (row.no_datum ?: '')
            if( row?.st_centroide ){
                row.coordenadas +='<br><span class="blue">Centroide</span>'
            }

            row.precisaoCoord = row.ds_precisao_coord ?: ''
            row.altitude = (row.nu_altitude ? '<br>Altitude: ' + row.nu_altitude + 'm' : '')

            if (row.dt_carencia) {
                if (row.dt_carencia > Util.hoje()) {
                    row.carencia = (row.no_prazo_carencia ? row.no_prazo_carencia + '<br>' : '') +
                        row.dt_carencia.format('dd/MM/yyyy')
                } else {
                    row.carencia = (row.no_prazo_carencia ? row.no_prazo_carencia + '<br>' : '') +
                        'Encerrada<br>' +
                        row.dt_carencia.format('dd/MM/yyyy')
                }
            } else {
                row.carencia = 'Sem carência'
            }

            row.localidade      = row.no_localidade ?: ''
            row.noInstituicao   = 'ICMBio'
            row.estado          = row.no_estado ?: ''
            row.municipio       = row.no_municipio ?: ''
            row.presencaAtual   = row.ds_presenca_atual ?: ''
            row.refBib          = Util.parseRefBib2Grid(row.json_ref_bib.toString())
            row.log             = ''

            /*if (row?.dt_inclusao ) {
                row.log = row.dt_inclusao.format('dd/MM/yyyy HH:mm:ss').replaceAll(' 00:00:00', '')
            }*/
            if (row?.dt_alteracao) {
                row.log = row.dt_alteracao.format('dd/MM/yyyy HH:mm:ss').replaceAll(' 00:00:00', '')
            }
            if( row.no_usuario_alteracao ){
                row.log +='<br>' + Util.nomeAbreviado( row.no_usuario_alteracao )
            }
            row.cdSituacaoAvaliacao     = row.cd_situacao_avaliacao ?:''
            row.dsSituacaoAvaliacao     = row.ds_situacao_avaliacao ?:''
            row.dsMotivoNaoUtilizado    = row.ds_motivo_nao_utilizado ?:''
            row.txJustificativaNaoUtilizado = row.tx_justificativa_nao_utilizado?:''
            if( row.tx_justificativa_nao_utilizado) {
                row.txJustificativaNaoUtilizado = row.txJustificativaNaoUtilizado.replaceAll('Cadastrado por ','<br>Cadastrado por ')
            }

            //row.idOrigem                    = ( row.id_origem ?: '' )
            row.idOrigem = (row.id_origem ? ((row.id_origem =~ /^SALVE:/) ? row.id : row.id_origem) : row.id)
            row.subespecie  = (row.co_nivel_taxonomico == 'SUBESPECIE')
            row.canEdit     = ! session.sicae.user.isCO() // PERFIL CONSULTA NÃO PODE EDITAR
            if( row.subespecie && ! isFichaSubespecie ){
                row.canEdit = false;
            }
            row.observacao  = row.tx_observacao
            row.dataHora    = row.ds_data_hora
            row.dataHora    = (row.dataHora.toString() =~'1500') ? '' : row.dataHora
            row.isSensivel  = row.in_sensivel == 'S'


            // adicionar o link para indentificar a coordenada no mapa ao clicar
            row.coordenadas += '&nbsp;<a href="javascript:void(0);" data-action="distribuicao.identificarPonto" data-bd="' + row.no_bd + '" data-ponto-id="' + row.id + '"><img class="marker-pin" src="/salve-estadual/assets/marker_pin_red.png" title="Identificar no mapa!"></a>'
            row.coordenadas += row?.nu_altitude ?: ''
            if (row.tx_observacao) {
                row.coordenadas += '<br>Observação <i title="clique para visualizar." data-table="ficha_ocorrencia" data-id="' + row.id + '" data-column="tx_observacao" data-title="Observação" data-action="app.showText" class="fa fa-commenting-o" aria-hidden="true"></i>'
            }
            row.no_autor   = row.no_autor ?: ''
            row.no_projeto = row.no_projeto ?: ''

            // definir a cor do texto da situação do registro na avaliação
            switch ( row.cd_situacao_avaliacao ){
                case 'REGISTRO_UTILIZADO_AVALIACAO':
                case 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO':
                    row.corSituacaoAvaliacao = 'green'
                    break
                case 'REGISTRO_NAO_UTILIZADO_AVALIACAO':
                    row.corSituacaoAvaliacao = 'red'
                    break;
                case 'REGISTRO_EXCLUIDO':
                    row.corSituacaoAvaliacao = 'red'
                    break;
                default:
                    row.corSituacaoAvaliacao = 'yellow'
            }

        }

        // rowId
        render(template: "/ficha/distribuicao/divGridOcorrencia",
            model: [ 'registros'    : rows
                    , gridId        : params.gridId
                    , pagination    : pagination
                    , hasSubespecies: true // não utilizado por enquanto
                   ])
    }


    def deleteDisOcorrencia() {
        FichaOcorrencia fichaOcorrencia = FichaOcorrencia.get(params.sqFichaOcorrencia)
        if (fichaOcorrencia) {
            if (fichaOcorrencia.ficha != Ficha.get(params.sqFicha)) {
                return this._returnAjax(1, 'Registro não pertence a Ficha', "error");
            }
            try {
                fichaOcorrencia.delete(flush: true)
                fichaService.updatePercentual(fichaOcorrencia.ficha)

                // se for uma subespecie, atualizar o percentual da especie
                fichaService.updatePercentualEspecie(fichaOcorrencia.ficha)

                //fichaService.atualizarPercentualPreenchimento(fichaOcorrencia.ficha)
                this._returnAjax(0, getMsg(4), "success")
            }
            catch (Exception e) {
                println e.getMessage()
                this._returnAjax(1, getMsg(51), "error")
            }
        } else {
            this._returnAjax(1, getMsg(6), "error")
        }
    }

    // #####################################################################################################################################//
    // 	MODULO FICHA/DISTRIBUICAO/OCORRENCIA/GEOREFERENCIA 																					//
    // #####################################################################################################################################//
    def tabGeoReferencia() {
        FichaOcorrencia fichaOcorrencia = null
        String stLocalidadeDesconhecida = ''
        String stSemUf = ''
        if (!params.sqFicha) {
            render 'ID ficha não informado'
            return;
        }
        if (params.sqFichaOcorrencia) {
            fichaOcorrencia = FichaOcorrencia.get(params.sqFichaOcorrencia)
        }
        Ficha ficha
        if (!fichaOcorrencia) {
            ficha = Ficha.get(params.sqFicha.toLong())
            stLocalidadeDesconhecida = ficha.stLocalidadeDesconhecida ?: ''
            stSemUf = ficha.stSemUf ?: ''
        } else {
            stLocalidadeDesconhecida = fichaOcorrencia.ficha.stLocalidadeDesconhecida ?: ''
            stSemUf = fichaOcorrencia.ficha.stSemUf ?: ''
        }

        List prazoCarencia = dadosApoioService.getTable('TB_PRAZO_CARENCIA')
        List datum = dadosApoioService.getTable('TB_DATUM')
        List formatoOriginal = dadosApoioService.getTable('TB_FORMATO_COORDENADA_ORIGINAL')
        List precisaoCoord = dadosApoioService.getTable('TB_PRECISAO_COORDENADA')
        List refAproximacao = dadosApoioService.getTable('TB_REFERENCIA_APROXIMACAO')
        List metodAproximacao = dadosApoioService.getTable('TB_METODO_APROXIMACAO')
        List motivoNaoUtilizado = dadosApoioService.getTable('TB_MOTIVO_NAO_UTILIZACAO_REGISTRO_OCORRENCIA')
        List situacaoRegistro = dadosApoioService.getTable('TB_SITUACAO_REGISTRO_OCORRENCIA').findAll {
            return it.codigoSistema != 'REGISTRO_EXCLUIDO'
        }
        List tipoRegistro = dadosApoioService.getTable('TB_TIPO_REGISTRO_OCORRENCIA').sort {
            it.descricao
        }

        // verificar se a ficha possui pelo menos um registro e não possui nenhuma UF identificada
        Integer nuEstados = 0
        if (ficha) {
            sqlService.execSql("select count(sg_estado) as nu_estados from salve.fn_ficha_estados(:sqFicha)", [sqFicha: ficha?.id?.toLong()]).each { row ->
                nuEstados = row.nu_estados
            }
        }

        FichaRefBib fichaRefBib
        if (params.sqFichaOcorrencia) {
            /*
            ficha = Ficha.get(params.sqFicha.toLong())
            // cada registro de ocorrencia so pode ter uma referencia bibliográfica
            // excluir as demais que forem encontradas e selecionar a última
            FichaRefBib.findAllByFichaAndSqRegistroAndNoTabelaAndNoColuna(ficha
                , fichaOcorrencia.id
                , 'ficha_ocorrencia'
                , 'sq_ficha_ocorrencia').each {
                if (fichaRefBib) {
                    fichaRefBib.delete(flush: true)
                }
                fichaRefBib = it
            }*/

            // ler a primeira ref cadastrada
			fichaRefBib = FichaRefBib.createCriteria().get {
				eq('sqRegistro', params.sqFichaOcorrencia.toInteger() )
				eq('noColuna', 'sq_ficha_ocorrencia')
				eq('noTabela', 'ficha_ocorrencia')
                order('id','asc')
                maxResults(1)
			}
        }
        render(template: "/ficha/distribuicao/ocorrencia/formGeoreferencia", model: ['prazoCarencia'           : prazoCarencia,
                                                                                     'datum'                   : datum,
                                                                                     'formatoOriginal'         : formatoOriginal,
                                                                                     'precisaoCoord'           : precisaoCoord,
                                                                                     'refAproximacao'          : refAproximacao,
                                                                                     'metodAproximacao'        : metodAproximacao,
                                                                                     'fichaOcorrencia'         : fichaOcorrencia,
                                                                                     'fichaRefBib'             : fichaRefBib,
                                                                                     'tipoRegistro'            : tipoRegistro,
                                                                                     'situacaoRegistro'        : situacaoRegistro,
                                                                                     'motivoNaoUtilizado'      : motivoNaoUtilizado,
                                                                                     'stLocalidadeDesconhecida': stLocalidadeDesconhecida,
                                                                                     'stSemUf'                 : stSemUf,
                                                                                     'nuEstados'               : nuEstados])
    } // id

    def saveLocalidadeDesconhecida() {
        Ficha ficha
        if (!params.sqFicha) {
            return this._returnAjax(1, 'ID da ficha não informado!', 'error');
        }
        ficha = Ficha.get(params.sqFicha.toLong())
        if (ficha) {
            ficha.stLocalidadeDesconhecida = (params.stLocalidadeDesconhecida == 'S' ? 'S' : 'N')
            ficha.save(flush: true)
            //fichaService.atualizarPercentualPreenchimento( fichaOcorrencia.ficha )
            fichaService.updatePercentual(ficha.id)
            // se for uma subespecie, atualizar o percentual da especie
            fichaService.updatePercentualEspecie(ficha)
            return this._returnAjax(0, 'Gravação realizada com SUCESSO!', 'success');
        }
        return this._returnAjax(1, 'Ficha inválida!', 'info');
    }

    def saveSemUf() {
        Ficha ficha
        if (!params.sqFicha) {
            return this._returnAjax(1, 'ID da ficha não informado!', 'error');
        }
        ficha = Ficha.get(params.sqFicha.toLong())
        if (ficha) {
            ficha.stSemUf = (params.stSemUf == 'S' ? 'S' : 'N')
            ficha.save(flush: true)
            fichaService.updatePercentual(ficha.id)
            // se for uma subespecie, atualizar o percentual da especie
            fichaService.updatePercentualEspecie(ficha)
            return this._returnAjax(0, 'Gravação realizada com SUCESSO!', 'success');
        }
        return this._returnAjax(1, 'Ficha inválida!', 'info');
    }

    /**
     * Gravar formulário aba 2.6.1 - Georeferenciamento
     * @return
     */
    def saveFrmDisOcoGeoreferencia() {

        if (!params.sqFicha) {
            return this._returnAjax(1, 'ID da ficha não informado!', 'error');
        }

        if (!params.fldLatitude || !params.fldLongitude) {
            return this._returnAjax(1, 'Coordenada inválida!', 'error');
        }
        if (!params.sqFichaOcorrencia && !params.sqPublicacaoTemp && !params.nuAnoRefTemp && !params.noAutorRefTemp) {
            return this._returnAjax(1, 'Necessário informar uma Referência Bibliográfica ou Comunicação Pessoa!', 'error');
        }

        if( !params.sqSituacaoAvaliacao){
            return this._returnAjax(1, 'Situação do registro não informada!', 'error');

        }
        DadosApoio situacaoAvaliacao = DadosApoio.get( params.sqSituacaoAvaliacao.toLong() )

        if( situacaoAvaliacao.codigoSistema == 'REGISTRO_NAO_UTILIZADO_AVALIACAO' ) {
            if( ! params.sqMotivoNaoUtilizadoAvaliacao ) {
                return this._returnAjax(1, 'Necessário informar o motivo da não utilização do registro na avalição!', "info")
            }

            if( ! params.txNaoUtilizadoAvaliacao ) {
                return this._returnAjax(1, 'Necessário informar a justificativa da não utilização do registro!', "info")
            }
            params.inUtilizadoAvaliacao='N'
        } else if( situacaoAvaliacao.codigoSistema == 'REGISTRO_UTILIZADO_AVALIACAO' )  {
            params.inUtilizadoAvaliacao = 'S'
            params.txNaoUtilizadoAvaliacao = null
            params.sqMotivoNaoUtilizadoAvaliacao = null
        } else if( situacaoAvaliacao.codigoSistema == 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO' )  {
            params.inUtilizadoAvaliacao = 'S'
            params.txNaoUtilizadoAvaliacao = null
            params.sqMotivoNaoUtilizadoAvaliacao = null
        } else {
            params.inUtilizadoAvaliacao = 'N'
            params.txNaoUtilizadoAvaliacao = null
            params.sqMotivoNaoUtilizadoAvaliacao = null
        }


        // NOVO-REGISTRO
        // nao precisa mais
        //Map asyncUpdateFicha = [:]
        //asyncUpdateFicha[params.sqFicha.toString()] = []

        if (params?.cdReferenciaAproximacao && params?.cdMetodologiaAproximacao && params.cdReferenciaAproximacao == 'ESTADO' && params.cdMetodologiaAproximacao == 'CENTROIDE') {
            params.sqMunicipio = null
            params.sqBioma = null
            params.sqBacia = null
            params.ucControle = null
            params.sqPaisUc = null
            params.sqMunicipioUc = null
            params.sqBiomaUc = null
            params.sqControle = null
        }

        if (params.sqTipoRegistro) {
            params.tipoRegistro = DadosApoio.get(params.sqTipoRegistro.toLong())
        }
        if (params.dtRegistro) {
            params.dtRegistro = new Date().parse('dd/MM/yyyy', params.dtRegistro)
        }
        // fim validação da data e ano

        if (params.idOrigem) {
            /*
            if( ! params.idOrigem.trim().isLong() )
            {
                return  this._returnAjax(1, 'Id da origem deve ser um número válido!','error')
            }
            params.idOrigem = params.long('idOrigem')
             */

            // verificar se idOrigem já existe
            Integer qtd = FichaOcorrencia.createCriteria().count {
                if (params.sqFichaOcorrencia) {
                    ne('id', params.sqFichaOcorrencia.toLong())
                }
                eq('idOrigem', params.idOrigem)
                eq('ficha.id', params.sqFicha.toLong())
            }
            if (qtd > 0) {
                return this._returnAjax(1, 'Id da origem ' + params.idOrigem + ' já cadastrado!', 'error')
            }
        }

        //return  this._returnAjax(1, 'VER PARAMETROS!','error');

        FichaOcorrencia fichaOcorrencia
        GeometryFactory gf = new GeometryFactory();
        List errors = []

        if (params.sqFichaOcorrencia) {
            fichaOcorrencia = FichaOcorrencia.get(params.sqFichaOcorrencia);
            // verificar se salvou um ponto que foi excluido pelo mapa e o id está inválido
            if( fichaOcorrencia ) {
                /*
                boolean isCentroideEstado
                if (fichaOcorrencia?.metodoAproximacao && fichaOcorrencia.refAproximacao.codigoSistema == 'ESTADO' && fichaOcorrencia.metodoAproximacao.codigoSistema == 'CENTROIDE') {
                    isCentroideEstado = true
                } else {
                    isCentroideEstado = false
                }*/

                if( params.txNaoUtilizadoAvaliacao ) {
                    if (fichaOcorrencia.txNaoUtilizadoAvaliacaoSemUsuario.toString().trim() != params.txNaoUtilizadoAvaliacao.toString().trim()) {
                        params.txNaoUtilizadoAvaliacao = params.txNaoUtilizadoAvaliacao + '<br>Cadastrado por ' + session.sicae.user.noPessoa +
                            ' em ' + new Date().format('dd/MM/yyyy HH:mm:ss')
                    }
                }
                fichaOcorrencia.properties = params;
            } else {
                params.sqFichaOcorrencia=null
            }
        }
        // se não existe a ocorrencia, criar uma nova
        if( !fichaOcorrencia ) {
            fichaOcorrencia = new FichaOcorrencia(params);
            fichaOcorrencia.ficha = Ficha.get(params.sqFicha);
        }
        if (params.fldLatitude && params.fldLongitude) {
            fichaOcorrencia.geometry = gf.createPoint(new Coordinate(Double.parseDouble(params.fldLongitude), Double.parseDouble(params.fldLatitude)));
            if (fichaOcorrencia.geometry) {
                fichaOcorrencia.geometry.SRID = 4674
            }
        }
        // adicionar o nome do usuario que cadastrou a jutificativa da não utilização
        if( fichaOcorrencia.txNaoUtilizadoAvaliacao ){
            if( !fichaOcorrencia.usuarioDataTxNaoUtilizadoAvaliacao?.nome ){
                 fichaOcorrencia.txNaoUtilizadoAvaliacao = fichaOcorrencia.txNaoUtilizadoAvaliacao + '<br>Cadastrado por ' + session.sicae.user.noPessoa + ' em ' +
                        new Date().format('dd/MM/yyyy HH:mm:ss')

            }
        }

        // estes campos sempre serão recalculados ao salvar a ocorrencia
        fichaOcorrencia.ucControle = null
        fichaOcorrencia.sqUcFederal = null
        fichaOcorrencia.sqUcEstadual = null
        fichaOcorrencia.sqRppn = null
        fichaOcorrencia.estado = null
        fichaOcorrencia.municipio = null
        fichaOcorrencia.bioma = null
        fichaOcorrencia.baciaHidrografica = null

        // marcar o registro como utilizado na avaliação se o campo não estiver preenchido
        /*if (!fichaOcorrencia.inUtilizadoAvaliacao) {
            fichaOcorrencia.inUtilizadoAvaliacao = 'S'
            fichaOcorrencia.motivoNaoUtilizadoAvaliacao=null
            fichaOcorrencia.stAdicionadoAposAvaliacao=false
            fichaOcorrencia.txNaoUtilizadoAvaliacao = null
        }*/

        if (fichaOcorrencia.ucControle && !params.inPresencaAtual) {
            return this._returnAjax(1, 'Campo Presença Atual na Coordenada deve ser informado quando informada a UC!', "info")
        }
        // não permitir adicionar 2 registros centoide no mesmo Estado
        if ( params.sqApoioMetodoAproximacao && params.sqApoioRefAproximacao ) {
            DadosApoio metodoAproximacao = DadosApoio.get(params.sqApoioMetodoAproximacao.toLong()) //dadosApoioService.getByCodigo('TB_METODO_APROXIMACAO','CENTROIDE')
            DadosApoio refAproximacao = DadosApoio.get(params.sqApoioRefAproximacao.toLong())//dadosApoioService.getByCodigo('TB_REFERENCIA_APROXIMACAO','ESTADO')
            if (metodoAproximacao.codigoSistema == 'CENTROIDE' && refAproximacao.codigoSistema == 'ESTADO') {

               // encontrar o Estado que o registro está
                Estado estado = geoService.getUfByCoord(  fichaOcorrencia.geometry.y,  fichaOcorrencia.geometry.x)
                if( estado ) {
                    List reg = sqlService.execSql("""select fo.sq_ficha_ocorrencia from salve.ficha_ocorrencia fo
                    where fo.sq_contexto in ( 420, 418 )
                    and sq_ficha = :sqFicha
                    and fo.sq_metodo_aproximacao = :sqMetodoAproximacao
                    and fo.sq_ref_aproximacao = :sqRefAproximacao
                    and fo.ge_ocorrencia is not null
                    and fo.sq_ficha_ocorrencia <> :sqFichaOcorrencia
                    and fo.sq_estado = :sqEstado
                    limit 1""", [sqFicha              : params.sqFicha.toLong()
                                 , sqMetodoAproximacao: metodoAproximacao.id.toLong()
                                 , sqRefAproximacao   : refAproximacao.id.toLong()
                                 , sqFichaOcorrencia  : (params.sqFichaOcorrencia ? params.sqFichaOcorrencia.toLong() : 0l)
                                 , sqEstado           : estado.id.toLong()
                    ])
                    if (reg) {
                        return this._returnAjax(1, 'Já existe um centroide cadastrado neste Estado ('+estado.sgEstado+').' , "error")
                    }
                }
            }
        }

        // gravar dados apoio
        params.each {
            if (it.key.indexOf('sqApoio') == 0) {
                String key = it.key.replaceAll('sqApoio', '');
                key = key[0].toLowerCase() + key.substring(1);
                fichaOcorrencia[key] = DadosApoio.get(it.value);
            }
        }

        // gravar o contexto
        fichaOcorrencia.contexto = dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA', 'REGISTRO_OCORRENCIA');
        fichaOcorrencia.inPresencaAtual = params.inPresencaAtual

        // alterar o valor do campo data desconhecida
        if (fichaOcorrencia.nuAnoRegistro) {
            fichaOcorrencia.inDataDesconhecida = 'N'
        } else {
            fichaOcorrencia.inDataDesconhecida = 'S'
        }

        def locale = Locale.getDefault()
        if (!fichaOcorrencia.validate()) {
            fichaOcorrencia.errors.allErrors.each {
                errors.add(messageSource.getMessage(it, locale));
            }
        }
        if (errors) {
            return this._returnAjax(1, errors.join('<br>'), "error");
        }


        // atualizar campos logs
        fichaOcorrencia.pessoaAlteracao = PessoaFisica.get(session.sicae.user.sqPessoa)
        fichaOcorrencia.pessoaInclusao = fichaOcorrencia.pessoaInclusao ?: fichaOcorrencia.pessoaAlteracao

        fichaOcorrencia.nuAltitude = (params.nuAltitude ? params.nuAltitude.toInteger() : null)
        fichaOcorrencia.inSensivel = (params.inSensivel ? params.inSensivel : 'N')
        fichaOcorrencia.geoValidate = false // não precisa de validar Uf e Municipio novamente
        fichaOcorrencia.idOrigem = params.idOrigem ?: null
        fichaOcorrencia.txObservacao = params.txObservacao ?: null

        fichaOcorrencia.ficha.stLocalidadeDesconhecida = 'N'
        /*
        if( params.inSituacaoRegistro == 'CONFIRMADO_ADICIONADO_APOS_AVALIACAO' ) {
            fichaOcorrencia.stAdicionadoAposAvaliacao = true
        } else {
            fichaOcorrencia.stAdicionadoAposAvaliacao = false
        }*/

        fichaOcorrencia.situacaoAvaliacao = situacaoAvaliacao

        // gravar/limpar o motivo da não utilização do registro na avaliação
        if( params.sqMotivoNaoUtilizadoAvaliacao  ){
            fichaOcorrencia.motivoNaoUtilizadoAvaliacao = DadosApoio.get( params.sqMotivoNaoUtilizadoAvaliacao.toInteger() )
        } else {
            fichaOcorrencia.motivoNaoUtilizadoAvaliacao = null
        }


        fichaOcorrencia.ficha.save(flush: true)
        if (fichaOcorrencia.save(flush: true)) {

            // verificar se a ref bib informada já existe para o registro
            String cmdSql = """select sq_publicacao, no_autor, nu_ano from salve.ficha_ref_bib
                              where sq_ficha = ${params.sqFicha.toString() }
                                and no_tabela = 'ficha_ocorrencia'
                                and no_coluna = 'sq_ficha_ocorrencia'
                                and sq_registro = ${fichaOcorrencia.id.toString()}"""
            if ( params.sqPublicacaoTemp ) {
                cmdSql+= "\nand sq_publicacao = ${params.sqPublicacaoTemp.toString()}"
            } else if( params.noAutorRefTemp && params.nuAnoRefTemp ){
                cmdSql+= "\nand no_autor = '${params.noAutorRefTemp.toString()}' and nu_ano='${params.nuAnoRefTemp}'"
            }

            // se não existir a referencia no ponto, adiciona-la
            List rowRefTemp = sqlService.execSql( cmdSql )
            FichaRefBib refBibOcorrencia
            if( ! rowRefTemp ) {
                // se o ponto já tiver referencia(s) cadastrada(s) substituir a primeira pela
                // que foi informada na alteração do registro
                refBibOcorrencia = FichaRefBib.createCriteria().get {
                    eq('sqRegistro', fichaOcorrencia.id.toInteger() )
                    eq('noColuna', 'sq_ficha_ocorrencia')
                    eq('noTabela', 'ficha_ocorrencia')
                    order('id','asc')
                    maxResults(1)
                }
                if( ! refBibOcorrencia ) {
                    refBibOcorrencia = new FichaRefBib()
                    refBibOcorrencia.ficha = fichaOcorrencia.ficha
                    refBibOcorrencia.sqRegistro = fichaOcorrencia.id.toInteger()
                    refBibOcorrencia.noTabela = 'ficha_ocorrencia'
                    refBibOcorrencia.noColuna = 'sq_ficha_ocorrencia'
                    refBibOcorrencia.deRotulo = 'ocorrência'
                }

                if (params.sqPublicacaoTemp) {
                    // publicação
                    refBibOcorrencia.publicacao = Publicacao.get(params.sqPublicacaoTemp);
                    refBibOcorrencia.noAutor = null
                    refBibOcorrencia.nuAno = null
                } else if (params.noAutorRefTemp) {
                    // comunicação pessoal
                    refBibOcorrencia.publicacao = null
                    refBibOcorrencia.noAutor = params.noAutorRefTemp
                    if (params.nuAnoRefTemp) {
                        refBibOcorrencia.nuAno = params.nuAnoRefTemp.toInteger()
                    }
                }
                refBibOcorrencia.save(flush: true)
                /*
                // atualizar a lista de referencias do ponto
                cmdSql = """select sq_publicacao, no_autor, nu_ano from salve.ficha_ref_bib
                            where sq_ficha = ${params.sqFicha.toString() }
                              and no_tabela = 'ficha_ocorrencia'
                              and no_coluna = 'sq_ficha_ocorrencia'
                              and sq_registro = ${fichaOcorrencia.id.toString()}"""
                listRefBibsPonto = sqlService.execSql(cmdSql)
                */
            }

            // NOVO-REGISTRO Não é necessário mais isso
            /*
            // SE NÃO TIVER PRESENÇA NA COORDENADA NÃO PRECISA CALCULAR UC,ESTADO BIOMA ETC...
            if( fichaOcorrencia.inPresencaAtual != 'N') {
                asyncUpdateFicha[fichaOcorrencia.ficha.id.toString()].push([idOcorrencia       : fichaOcorrencia.id.toLong()
                                                                            , bd               : 'salve'
                                                                            , situacao         : fichaOcorrencia.inUtilizadoAvaliacao
                                                                            , ponto            : [x: fichaOcorrencia.geometry.x, y: fichaOcorrencia.geometry.y]
                                                                            , sqEstado         : fichaOcorrencia?.estado?.id
                                                                            , sqMunicipio      : fichaOcorrencia?.municipio?.id
                                                                            , sqBioma          : fichaOcorrencia?.bioma?.id
                                                                            , sqBacia          : fichaOcorrencia?.baciaHidrografica?.id
                                                                            , sqUcFederal      : fichaOcorrencia?.sqUcFederal
                                                                            , sqUcEstadual     : fichaOcorrencia?.sqUcEstadual
                                                                            , sqRppn           : fichaOcorrencia?.sqRppn
                                                                            , isCentroideEstado: centroideEstado
                                                                            , inPresencaAtual  : fichaOcorrencia.inPresencaAtual?:''
                                                                            , rowRefBibPonto   : listRefBibsPonto])
                if (params.sqFichaOcorrencia) {
                    Map oldData = asyncUpdateFicha[fichaOcorrencia.ficha.id.toString()][0]
                    Map newData = asyncUpdateFicha[fichaOcorrencia.ficha.id.toString()][1]
                    if ( (oldData.ponto == newData.ponto)
                        && ( oldData.sqEstado == newData.sqEstado )
                        && (oldData.sqBioma == newData.sqBioma)
                        && (oldData.sqBacia == newData.sqBacia)
                        && (oldData.sqUcFederal == newData.sqUcFederal)
                        && (oldData.sqRppn == newData.sqRppn)
                        && (oldData.rowRefBibPonto == newData.rowRefBibPonto)
                        && (oldData.inPresencaAtual == newData.inPresencaAtual)
                    ) {
                        // NÃO HOUVE ALTERAÇÃO NO REGISTRO REFERENTE A ESTADO, MUNICIPIO, BIOMA, BACIA, UC OU REF BIB
                        asyncUpdateFicha[fichaOcorrencia.ficha.id.toString()] = []
                    }
                }
            }
            */


            // NOVO-REGISTRO Não é necessário mais isso
            /*
            if (asyncUpdateFicha[fichaOcorrencia.ficha.id.toString()].size() > 0 ) {
                session.removeAttribute('gravarUtilizadoAvaliacao')
                asyncService.atualizarEstadoBiomaUcFicha( asyncUpdateFicha, '')
            }
            */

            String msgBiomaGrupoErrado = geoService.validarBiomaGrupo( fichaOcorrencia.ficha.id.toLong()
                                , fichaOcorrencia.geometry.y.toBigDecimal()
                                , fichaOcorrencia.geometry.x.toBigDecimal() )


            // NOVO-REGISTRO - atualizar tabela registro - fazer o relacionamento do REGISTRO X FICHA_OCORRENCIA
            registroService.saveOcorrenciaSalve( fichaOcorrencia )


            // atualizar o percentual de preenchimento da ficha
            fichaService.updatePercentual( fichaOcorrencia.ficha.id.toLong() )

            this._returnAjax(0, getMsg(1), "success", ['sqFichaOcorrencia': fichaOcorrencia.id, msgBiomaGrupoErrado:msgBiomaGrupoErrado])
        } else {
            fichaOcorrencia.errors.allErrors.each {
                this._returnAjax(1, getMsg(2), "error")
            }
        }
    } // FIM SAVE PONTO

    // #####################################################################################################################################//
    // 	MODULO FICHA/DISTRIBUICAO/OCORRENCIA/DADOS DO REGISTRO																				//
    // #####################################################################################################################################//

    def tabDisOcoDadosRegistro() {

        if (!params.sqFichaOcorrencia) {
            render 'Id da Ocorrência não informado!';
            return;
        }
        FichaOcorrencia fichaOcorrencia = FichaOcorrencia.get(params.sqFichaOcorrencia)
        //List tipoRegistro = dadosApoioService.getTable('TB_TIPO_REGISTRO_OCORRENCIA').sort{ it.descricao }
        //List listNiveisTaxonomicos = NivelTaxonomico.list();
        if (!grailsApplication?.config?.grau?.genero) {
            render 'Constante grau.genero não definido no arquivo /data/salve-estadual/config.properties';
            return
        }
        List listNiveisTaxonomicos = NivelTaxonomico.findAllByNuGrauTaxonomicoGreaterThanEquals(grailsApplication?.config?.grau?.genero);
        //render(template:"/ficha/distribuicao/ocorrencia/formDadosRegistro", model:['tipoRegistro': tipoRegistro, 'listNiveisTaxonomicos': listNiveisTaxonomicos, 'fichaOcorrencia': fichaOcorrencia]);
        render(template: "/ficha/distribuicao/ocorrencia/formDadosRegistro", model: ['listNiveisTaxonomicos': listNiveisTaxonomicos, 'fichaOcorrencia': fichaOcorrencia]);
    }

    def saveFrmDisOcoRegistro() {

        if (!params.sqFicha) {
            return this._returnAjax(1, 'ID da ficha não informado!', "error");
        }
        if (params.nuAnoRegistro) {
            params.inDataDesconhecida = 'N';
        } else {
            params.inDataDesconhecida = 'S';
            params.nuDiaRegistro = null;
            params.nuMesRegistro = null;
            params.nuAnoRegistro = null;
            params.hrRegistro = null;
            // data fim
            params.nuDiaRegistroFim = null;
            params.nuMesRegistroFim = null;
            params.nuAnoRegistroFim = null;

        }
        FichaOcorrencia fichaOcorrencia;
        if (params.sqTaxonCitado) {
            params.taxonCitado = Taxon.get(params.sqTaxonCitado)
        }
        if (params.sqTipoRegistro) {
            params.tipoRegistro = DadosApoio.get(params.sqTipoRegistro)
        }
        if (params.dtRegistro) {
            params.dtRegistro = new Date().parse('dd/MM/yyyy', params.dtRegistro)
        }
        if (params.sqFichaOcorrencia) {
            fichaOcorrencia = FichaOcorrencia.get(params.sqFichaOcorrencia)
        } else {
            fichaOcorrencia = new FichaOcorrencia()
        }
        params.ficha = Ficha.get(params.sqFicha);
        fichaOcorrencia.geoValidate = false;
        fichaOcorrencia.properties = params
        fichaOcorrencia.save(flush: true);
        // atualizar log da ficha, quem alterou
        fichaOcorrencia.ficha.dtAlteracao = new Date()
        fichaOcorrencia.ficha.save()
        return this._returnAjax(0, getMsg(1), "success")
    }


    // #####################################################################################################################################//
    // 	MODULO FICHA/DISTRIBUICAO/OCORRENCIA/LOCALIDADE 2.6.3
    // #####################################################################################################################################//
    def tabDisOcoLocalidade() {
        if (!params.sqFichaOcorrencia) {
            render 'Id da Ocorrência não informado!';
            return;
        }
        FichaOcorrencia fichaOcorrencia = FichaOcorrencia.get(params.sqFichaOcorrencia);
        List listContinentes = dadosApoioService.getTable('TB_CONTINENTE').sort {
            it.ordem
        }
        List listPaises = Pais.list([order: 'descricao']);
        List listEstados = Estado.list([order: 'noEstado']);
        List listHabitats = dadosApoioService.getTable('TB_HABITAT').sort {
            it.ordem
        }
        render(template: "/ficha/distribuicao/ocorrencia/formLocalidade",
            model: ['fichaOcorrencia': fichaOcorrencia,
                    'listContinentes': listContinentes,
                    'listPaises'     : listPaises,
                    'listEstados'    : listEstados,
                    'listHabitats'   : listHabitats,]);
    }

    def saveFrmDisOcoLocalidade() {
        if (!params.sqFichaOcorrencia) {
            return this._returnAjax(1, 'Id da Ocorrência não informado!', "error");
        }
        FichaOcorrencia fichaOcorrencia = FichaOcorrencia.get(params.sqFichaOcorrencia.toLong())
        fichaOcorrencia.properties = params
        fichaOcorrencia.continente = null
        fichaOcorrencia.habitat = null
        if (!params.sqControle) {
            fichaOcorrencia.pais = null;
            fichaOcorrencia.estado = null;
            fichaOcorrencia.municipio = null;
        }


        if (params.sqContinente) {
            fichaOcorrencia.continente = DadosApoio.get(params.sqContinente);
        }
        if (params.sqPais) {
            fichaOcorrencia.pais = Pais.get(params.sqPais);
        }
        if (params.sqEstado) {
            fichaOcorrencia.estado = Estado.get(params.sqEstado);
        }
        if (params.sqMunicipio) {
            fichaOcorrencia.municipio = Municipio.get(params.sqMunicipio);
        }
        if (params.sqHabitat) {
            fichaOcorrencia.habitat = DadosApoio.get(params.sqHabitat);
        } else if (params.sqHabitatPai) {
            fichaOcorrencia.habitat = DadosApoio.get(params.sqHabitatPai);
        }

        // if( params.sqControle )
        // {
        //           // basta informar a ucControle que a classe grava no campo correto no caso de ser uma estadual ou rppn
        // 	fichaOcorrencia.ucControle = Uc.get( params.sqControle.toInteger() );
        //           // para gravar uc o campo inPresencaAtual
        //           if( ! fichaOcorrencia?.inPresencaAtual?.trim()  )
        //           {
        //               return this._returnAjax(1,'Campo Presença Atual na Coordenada da aba anterior não informado ou a aba não está salva!' , "info")
        //           }

        // }

        def locale = Locale.getDefault()
        List errors = []
        if (!fichaOcorrencia.validate()) {
            fichaOcorrencia.errors.allErrors.each {
                errors.add(messageSource.getMessage(it, locale));
            }
        }
        if (errors) {
            return this._returnAjax(1, errors.join('<br>'), "error");
        }
        fichaOcorrencia.geoValidate = false;
        fichaOcorrencia.save(flush: true)

        // atualizar log da ficha, quem alterou
        fichaOcorrencia.ficha.dtAlteracao = new Date()
        fichaOcorrencia.ficha.save()

        return this._returnAjax(0, getMsg(1), "success")
    }


    // #####################################################################################################################################//
    // 	MODULO FICHA/DISTRIBUICAO/QUALIFICADOR 2.6.4
    // #####################################################################################################################################//
    def tabDisOcoQualificador() {
        if (!params.sqFichaOcorrencia) {
            render 'Id da Ocorrência não informado!';
            return;
        }
        FichaOcorrencia fichaOcorrencia = FichaOcorrencia.get(params.sqFichaOcorrencia);
        List listQualificadores = dadosApoioService.getTable('TB_QUALIFICADOR_TAXON').sort {
            it.descricao
        }
        render(template: "/ficha/distribuicao/ocorrencia/formQualificador",
            model: [fichaOcorrencia   : fichaOcorrencia,
                    listQualificadores: listQualificadores]);
    }

    def saveFrmDisOcoQualificador() {
        if (!params.sqFichaOcorrencia) {
            return this._returnAjax(1, 'Id da Ocorrência não informado!', "error");
        }
        FichaOcorrencia fichaOcorrencia = FichaOcorrencia.get(params.sqFichaOcorrencia);
        if (params.dtIdentificacao) {
            params.dtIdentificacao = new Date().parse('dd/MM/yyyy', params.dtIdentificacao)
        }
        fichaOcorrencia.geoValidate = false;
        fichaOcorrencia.properties = params;
        fichaOcorrencia.qualificadorValTaxon = DadosApoio.get(params.sqQualificadorValTaxon);
        if (fichaOcorrencia.save(flush: true)) {

            // atualizar log da ficha, quem alterou
            fichaOcorrencia.ficha.dtAlteracao = new Date()
            fichaOcorrencia.ficha.save()

            this._returnAjax(0, getMsg(1), "success")
        } else {
            if (!fichaOcorrencia.validate()) {
                fichaOcorrencia.errors.allErrors.each {
                    println it
                    this._returnAjax(1, getMsg(2), "error")
                }
            }
        }
    }

    // #####################################################################################################################################//
    // 	MODULO FICHA/DISTRIBUICAO/QUALIFICADOR 2.6.5
    // #####################################################################################################################################//
    def tabDisOcoResponsavel() {
        if (!params.sqFichaOcorrencia) {
            render 'Id da Ocorrência não informado!';
        }
        FichaOcorrencia fichaOcorrencia = FichaOcorrencia.get(params.sqFichaOcorrencia);
        // não sugerir o nome do compilador
        //if (fichaOcorrencia && !fichaOcorrencia?.pessoaCompilador && session?.sicae?.user?.sqPessoa) {
        //    fichaOcorrencia.pessoaCompilador = PessoaFisica.get(session.sicae.user.sqPessoa);
        //}
        render(template: "/ficha/distribuicao/ocorrencia/formResponsavel", model: [fichaOcorrencia: fichaOcorrencia]);
    }

    def saveFrmDisOcoResponsavel() {
        if (!params.sqFichaOcorrencia) {
            return this._returnAjax(1, 'Id da Ocorrência não informado!', "error");
        }
        FichaOcorrencia fichaOcorrencia = FichaOcorrencia.get(params.sqFichaOcorrencia);

        /*if (!params.sqPessoaValidador && !params.sqPessoaRevisor && !params.sqPessoaCompilador) {
            return this._returnAjax(1, 'Dados Incompletos para gravação!', 'error');
        }*/

        fichaOcorrencia.pessoaCompilador = null
        fichaOcorrencia.pessoaRevisor = null
        fichaOcorrencia.pessoaValidador = null
        if (params.dtCompilacao) {
            fichaOcorrencia.dtCompilacao = new Date().parse('dd/MM/yyyy', params.dtCompilacao)
        } else {
            fichaOcorrencia.dtCompilacao = null
        }

        if (params.dtValidacao) {
            fichaOcorrencia.dtValidacao = new Date().parse('dd/MM/yyyy', params.dtValidacao)
        } else {
            fichaOcorrencia.dtValidacao = null
        }

        if (params.dtRevisao) {
            fichaOcorrencia.dtRevisao = new Date().parse('dd/MM/yyyy', params.dtRevisao)
        } else {
            fichaOcorrencia.dtRevisao = null
        }

        if (params.dtInvalidado) {
            fichaOcorrencia.dtInvalidado = new Date().parse('dd/MM/yyyy', params.dtInvalidado)
        } else {
            fichaOcorrencia.dtInvalidado = null;
        }

        // campo passou para aba 2.6.1
        //fichaOcorrencia.inUtilizadoAvaliacao = params.inUtilizadoAvaliacao


        if (params.sqPessoaCompilador) {
            if (!params.dtCompilacao) {
                return this._returnAjax(1, 'Informe a data compilação!', 'error');
            }
            fichaOcorrencia.pessoaCompilador = PessoaFisica.get(params.sqPessoaCompilador);
        } else {
            fichaOcorrencia.dtCompilacao = null;
        }

        if (params.sqPessoaRevisor) {
            if (!params.dtRevisao) {
                return this._returnAjax(1, 'Informe a data da revisão!', 'error');
            }
            fichaOcorrencia.pessoaRevisor = PessoaFisica.get(params.sqPessoaRevisor);

        } else {
            fichaOcorrencia.dtRevisao = null;
        }

        if (params.sqPessoaValidador) {
            if (!params.dtValidacao) {
                return this._returnAjax(1, 'Informe a data da validação!', 'error');
            }
            fichaOcorrencia.pessoaValidador = PessoaFisica.get(params.sqPessoaValidador);
        } else {
            fichaOcorrencia.dtValidacao = null;
        }
        fichaOcorrencia.geoValidate = false;
        fichaOcorrencia.save(flush: true);

        // atualizar log da ficha, quem alterou
        fichaOcorrencia.ficha.dtAlteracao = new Date()
        fichaOcorrencia.ficha.save()

        return this._returnAjax(0, getMsg(1), "success")
    }


    // #####################################################################################################################################//
    // 	MODULO FICHA/DISTRIBUICAO/QUALIFICADOR 2.6.6
    // #####################################################################################################################################//

    def tabDisOcoRefBib() {
        if (!params.sqFichaOcorrencia) {
            render 'Id da Ocorrência não informado!';
        }
        render(template: "/ficha/distribuicao/ocorrencia/formRefBib", model: [sqFichaOcorrenciad: params.sqFichaOcorrencia]);
    }

    def saveFrmDisOcoRefBib() {
        if (!params.sqFicha) {
            return this._returnAjax(1, 'Id da Ficha não informado!', "error");
        }
        if (!params.sqFichaOcorrencia) {
            return this._returnAjax(1, 'Id da Ocorrência não informado!', "error");
        }
        if (!params.sqPublicacao) {
            return this._returnAjax(1, 'Selecione uma referência bibliográfica!', "error");
        }

        FichaRefBib reg = new FichaRefBib();
        reg.ficha = Ficha.get(params.sqFicha);
        reg.publicacao = Publicacao.get(params.sqPublicacao);
        reg.noTabela = 'ficha_ocorrencia';
        reg.noColuna = 'sq_ficha_ocorrencia';
        reg.sqRegistro = params.sqFichaOcorrencia.toInteger();
        reg.deRotulo = 'Registro de Ocorrência';

        // verificar se já existe
        if (FichaRefBib.countByFichaAndPublicacaoAndNoTabelaAndSqRegistro(reg.ficha, reg.publicacao, reg.noTabela, reg.sqRegistro) > 0) {
            return this._returnAjax(1, 'Referência ja cadastrada!', "error");
        }
        try {
            reg.save(flush: true)

            // atualizar log da ficha, quem alterou
            reg.ficha.dtAlteracao = new Date()
            reg.ficha.save()

            return this._returnAjax(0, getMsg(1), "success");
        }
        catch (Exception e) {
            return this._returnAjax(1, getMsg(51), "error");
        }
    }

    def deleteRefBib() {
        FichaRefBib reg = FichaRefBib.get(params.id);
        if (reg) {
            Ficha ficha = reg.ficha
            reg.delete(flush: true)

            // atualizar log da ficha, quem alterou
            ficha.dtAlteracao = new Date()
            ficha.save()

            return this._returnAjax(0, getMsg(4), "success")
        } else {
            return this._returnAjax(1, getMsg(51), "error")
        }
        render '';
    }

    def getGridRefBib() {

        if (!params.sqFichaOcorrencia) {
            render 'Id do registro não informado!'
            return;
        }
        List lista = FichaRefBib.findAllByFichaAndNoTabelaAndSqRegistroAndNoColuna(Ficha.get(params.sqFicha), 'ficha_ocorrencia', params.sqFichaOcorrencia, 'sq_ficha_ocorrencia');
        render(template: "/ficha/distribuicao/ocorrencia/divGridRefBib", model: ['lista': lista])
    }


    /**
     * Aba multimidia
     * @return
     */

    def tabDisOcoMultimidia() {
        if (!params.sqFichaOcorrencia) {
            render 'Id da Ocorrência não informado!';
        }
        List listTipoMultimidia = dadosApoioService.getTable('TB_TIPO_MULTIMIDIA').sort {
            it.ordem
        }
        List listSituacao = dadosApoioService.getTable('TB_SITUACAO_MULTIMIDIA').sort {
            it.ordem
        }

        render(template: "/ficha/distribuicao/ocorrencia/formMultimidia", model: [sqFichaOcorrenciad  : params.sqFichaOcorrencia
                                                                                  , listTipoMultimidia: listTipoMultimidia
                                                                                  , listSituacao      : listSituacao]);
    }
}
