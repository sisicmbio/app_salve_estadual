package br.gov.icmbio

import grails.converters.JSON
import grails.util.Environment
import groovy.sql.Sql
import org.apache.catalina.core.ApplicationHttpRequest
import org.codehaus.groovy.grails.web.json.JSONObject
import org.springframework.web.context.request.ServletRequestAttributes
import org.springframework.web.context.request.RequestContextHolder

import org.jsoup.Jsoup
import org.jsoup.safety.Whitelist
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.multipart.commons.CommonsMultipartFile

import java.nio.file.attribute.BasicFileAttributes
import java.util.zip.ZipFile

import java.nio.file.*
import static groovy.io.FileType.FILES

class MainController {

    def planilhaService
    def requestService
    def taxonService
    def cacheService
    def workerService
    def dataSource
    def dadosApoioService
    def fichaService
    def sqlService
    def consultaService
    def mainService
    def userJobService


    private void enableCors( /*org.apache.catalina.connector.ResponseFacade response*/)
    {
        String origin = ( ! request.getHeader("Origin") ? '*' : request.getHeader("Origin") )
        if( origin =~ /\.gov\.br/){
            origin = '*'
        }
        //println 'ApiController.enableCors() origin: ' + origin
        response.setHeader('Access-Control-Allow-Origin', origin )
        response.setHeader('Access-Control-Allow-Methods', 'POST, PUT, GET, OPTIONS, PATCH')
        response.setHeader('Access-Control-Allow-Headers', 'X-Additional-Headers-Example')
        response.setHeader('Access-Control-Allow-Credentials', 'true')
        response.setHeader('Access-Control-Allow-Private-Network', 'true')
        response.setHeader('Access-Control-Request-Private-Network', 'true')
        response.setHeader('Access-Control-Max-Age', '1728000')
    }

     def index() {
        if (params.ajax) {
            render 'Ação não mapeada!' + (params?.url ? ' - url: ' + params.url : '')
            return
        }
        redirect(uri: '/')
    }

    private void  _descompactarShapeFile( String zipFileName ) {
        println ' '
        println'Gravarndo SHAPEFILE ' + new Date().format('dd/MM/yyyy HH:mm:ss')
        println 'Arquivo: ' + zipFileName
        File file = new File( zipFileName )
        ZipFile zip = new ZipFile( file )
        String outputDir = file.getAbsolutePath().replaceAll( file.getName(),'')
        String shapeName = file.getName().replaceAll(/\.zip$/,'')
        shapeName = shapeName.split(/\./)[0]
        println 'ShapeName: ' + shapeName
        println 'Diretorio: ' + outputDir
        zip.entries().each{
            if ( ! it.isDirectory()){
                String fileExtension = Util.getFileExtension( it.name )
                String newFileName = shapeName+'.'+fileExtension
                println ' - ' + newFileName
                def fOut = new File( outputDir +  newFileName )
                //create output dir if not exists
                new File(fOut.parent).mkdirs()
                def fos = new FileOutputStream(fOut)
                //println "   - name: ${it.name}, size:${it.size}"
                def buf = new byte[it.size]
                def len = zip.getInputStream(it).read(buf) //println zip.getInputStream(it).text
                fos.write(buf, 0, len)
                fos.close()
            }
        }
        zip.close()
    }


    private println2(String value = '', String sep = '') {
        if (value) {
            log.info(value)
        }
        if (sep) {
            log.info(sep * 50)
        }
    }

    def ping() {
        this.enableCors()
        if (!session?.sicae?.user) {
            render 'Sessão Encerrada. Efetue login novamente!'
        }else {
            render ''
        }
    }

    def saveWkt() {
        Map res = [status:0,msg:'',type:'sucess']
        /*
        String cmdSql   = ''
        List row        = []
        Map pars        = [:]
        if( params.table && params.PS1_NM && params.PS1_CD && params.wkt ) {
            if ( params.PS1_CD.toLong() == 842 ) {
                DadosApoioGeo geo = new DadosApoioGeo()
                WKTReader wkt = new WKTReader();
                Geometry g = wkt.read( params.wkt)
                geo.geometry = g
                geo.idGeo = params.PS1_CD.toLong()
                geo.deGeo = params.PS1_NM
                geo.dadosApoio = DadosApoio.get( 476 )
                geo.save(flush:true)
                if( ! geo.getErrors() ) {
                    res.msg ='Dados gravados com SUCESSO!'
                    res.type='success'
                } else {
                    println geo.getErrors()
                }
            }
        }

       */
        render res as JSON
    }

    def getLog() {
        params.qtd = (params?.qtd ?: 1000)
        enableCors()
        String fileName
        File file
        fileName = params?.file ?: grailsApplication.config.tomcat.dir + 'logs/stacktrace.log'
        file = new File(fileName)
        if (!file.exists()) {
            fileName = '/opt/tomcat/logs/catalina.out'
        }
        file = new File(fileName)
        if (!file.exists()) {
            fileName = 'target/stacktrace.log'
            file = new File(fileName)
            if (!file.exists()) {
                render 'a) Arquivo <b>/opt/apache-tomcat-7.0.68/logs/catalina.out</b> não encontrado!<br>'
                render 'b) Arquivo <b>/opt/tomcat/logs/catalina.out</b> não encontrado!<br>'
                render 'c) Arquivo <b>target/stacktrace.log</b> não encontrado!<br>'
                render 'Exemplo: http://172.17.0.2:8080/salve-estadual/getLog/100?file=/opt/apache-tomcat-8.5.15/logs/catalina.out<br>'
                return
            }
        }
        file = null
        Integer qtdLinhas = params.qtd.toInteger()
        render "Arquivo: <b>" + fileName + '</b>'
        render "<br>Ultimas <b>" + qtdLinhas + '</b> linhas.'
        render '<div id="div-log" style="background-color:#000;color:#01D925;margin-top:5px;font-family:courier;font-size:14px;border:1px solid #000;width:100%;height:90%;overflow:auto;white-space: nowrap">'
        List linhas = []
        try {
            ReverseFileReader reader = new ReverseFileReader(fileName, "UTF-8")
            String line
            Integer lin = 0
            while ((line = reader.readLine()) != null && lin++ < qtdLinhas) {
                linhas.push(line + "<br/>")
            }
        } catch (Exception e) {
            linhas.push(e.getMessage())
        }
        render linhas.reverse().join('') + '</div><script>var objDiv=document.getElementById("div-log");objDiv.scrollTop=objDiv.scrollHeight;</script>'
    }

    def menu() {
        // não tem menu para o perfil validador
        if (session?.sicae?.user && !session?.sicae?.user?.isVL()) {
            render(template: 'menu')
        }
        render ''
    }

    // método par fazer o carregamento dos arquivos js remotamente nas páginas
    def javascript() {
        enableCors();
        if (!params.module) {
            render null
            return
        }
        String pathSeparator = '/'
        // tratamento especial modulos da ficha para encontrar o arquivo js das abas
        if (params.module.startsWith('ficha') && params.module.size() > 5) {
            params.module = params.module.substring(5, 6).toLowerCase() + params.module.substring(6)
            if (params.submodule && params.submodule.substring(0, 3) == 'tab') {
                params.submodule = params.submodule.substring(3, 4).toLowerCase() + params.submodule.substring(4)
                params.module = 'ficha/' + params.module + '/' + params.submodule
            }else {
                params.submodule = params.module
                params.module = 'ficha/' + params.submodule
            }
        }
        String realPath = request.getSession().getServletContext().getRealPath('/')
        String fileJs
        File file
        if (realPath.indexOf(pathSeparator) == -1) {
            pathSeparator = '\\'
        }
        if (Environment.current.toString() != 'PRODUCTION') {
            realPath = realPath.replace('web-app', 'grails-app')
            if (new File(realPath + 'WEB-INF').exists()) {
                log.info('run-war: true')
                // quando estiver executando com grains run-war deverá cair aqui
                realPath += 'WEB-INF' + pathSeparator + 'grails-app' + pathSeparator

            }
            realPath += 'views' + pathSeparator
        }else {
            realPath += '/WEB-INF/grails-app/views/'
        }
        fileJs = realPath + params.module + pathSeparator + (params?.submodule ? params.submodule : params.module)
        if (fileJs.indexOf('.js') < 0) {
            fileJs += '.js'
        }
        file = new File(fileJs)
        if (file.exists()) {
            render(file: file, encoding: 'UTF-8', contentType: 'text/javascript')
        }else {
            String assetFile = fileJs.replaceAll(/\/views\//, '/assets/javascripts/')
            file = new File(assetFile)
            if (file.exists()) {
                //println 'Encontrado.'
                render(file: file, encoding: 'UTF-8', contentType: 'text/javascript')
            }else {
                //println 'Não encontrado'
                // procurar pelo formato xxx.js.gsp
                fileJs = fileJs.replaceAll(/\.js/, '.gsp')
                //println 'Tentando como ' + fileJs
                file = new File(fileJs)
                if (file.exists()) {
                    render(file: file, encoding: 'UTF-8', contentType: 'text/javascript')
                }else {
                    fileJs = fileJs.replace('\\', '/')
                    render 'null'
                }
            }
        }
    }

    /**
     * metodo para criação das janelas modais
     * @return string html da janela
     */
    def getModal() {

        if (!session?.sicae?.user) {
            render '<h3>Sessão Está Encerrada!</h3>'
            return
        }

        if (!params?.modalName) {
            render '<h3>Parâmetro modalName não informado!</h3>'
            return
        }
        Map model = params
        sleep(100)
        switch (params?.modalName) {
           // integração SIS/IUCN
            case 'traducaoTextoFixo':
                if( params?.noTabelaTraducao && params.idRegistroTraducao ) {
                    if( params.noTabelaTraducao == 'apoio'){
                        DadosApoio dadosApoio = DadosApoio.get( params.idRegistroTraducao.toLong() )
                        if( dadosApoio ){
                            params.txOriginal= dadosApoio.descricao ?:''
                            params.txTraduzido = dadosApoio?.traducaoDadosApoio?.txTraduzido ? dadosApoio?.traducaoDadosApoio[0]?.txTraduzido : ''
                            params.txRevisado = dadosApoio?.traducaoDadosApoio?.txRevisado ? dadosApoio?.traducaoDadosApoio[0]?.txRevisado : ''
                        }
                    }
                }
                break;

            case 'comunicarTodosChatValidacao':
                model.listCts = []
                model.listValidadores = []
                // Exibir lista de CTS para os Pontos Focais e Admins
                if( params.sqFicha && session?.sicae && ( session.sicae.user.isPF() || session.sicae.user.isADM() ) ) {
                    DadosApoio papelCT = dadosApoioService.getByCodigo('TB_PAPEL_FICHA', 'COORDENADOR_TAXON')
                    FichaPessoa.createCriteria().list {
                        eq('inAtivo', 'S')
                        eq('papel', papelCT)
                        eq('vwFicha.id', params.sqFicha.toLong())
                    }.each {
                        if (it?.pessoa?.email) {
                            String email = it.pessoa.email.toLowerCase()
                            if (!model.listCts.find { it.email == email }) {
                                model.listCts.push([email         : email
                                                    , sqPessoa    : it.pessoa.id
                                                    , nome        : Util.capitalize(it.pessoa.noPessoa) + ' (<b>' + email + '</b>)'
                                                    , nomeCompleto: Util.capitalize(it.pessoa.noPessoa)
                                                    , papel       : 'CT'])
                            }
                        }
                    }

                    // ler os validadores
                    ValidadorFicha.executeQuery("""select new map(b.deEmail as deEmail, c.noPessoa as noPessoa, c.id as sqPessoa ) from ValidadorFicha a
                        join a.cicloAvaliacaoValidador b
                        join b.validador c
                        where a.oficinaFicha.id = :sqOficinaFicha
                        and b.situacaoConvite.codigoSistema = :coSituacaoConvite
                        """, [sqOficinaFicha: params.sqOficinaFicha.toLong(), coSituacaoConvite: 'CONVITE_ACEITO'])
                        .each {
                            if (it.deEmail) {
                                String email = it.deEmail.toLowerCase()
                                if (!model.listValidadores.find { it.email == email }) {
                                    model.listValidadores.push([email         : email
                                                                , sqPessoa    : it.sqPessoa
                                                                , nome        : Util.capitalize(it.noPessoa) + ' (<b>' + email + '</b>)'
                                                                , nomeCompleto: Util.capitalize(it.noPessoa)
                                                                , papel       : 'VL'])
                                }
                            }
                        }
                }
                break

            case 'selecionarColunasExportacaoRegistros':
                model.listColunas = [
                    ['title': 'Reino', 'column': 'no_reino']
                    ,['title': 'Filo', 'column': 'no_filo']
                    ,['title': 'Classe', 'column': 'no_classe']
                    ,['title': 'Ordem', 'column': 'no_ordem']
                    ,['title': 'Família', 'column': 'no_familia']
                    ,['title': 'Gênero', 'column': 'no_genero']
                    //,['title': 'Nome Científico', 'column': 'no_cientifico'] // coluna obrigatoria
                    ,['title': 'Autor', 'column': 'no_autor_taxon']
                    //,['title': 'Latitude', 'column': 'nu_latitude'] // coluna obrigatoria
                    //,['title': 'Longitude', 'column': 'nu_longitude'] // coluna obrigatoria
                    ,['title': 'Categoria Anterior', 'column': 'ds_categoria_anterior']
                    ,['title': 'Categoria Avaliada', 'column': 'ds_categoria_avaliada']
                    ,['title': 'Categoria Validada', 'column': 'ds_categoria_validada']
                    ,['title': 'Plano de Ação Nacional', 'column': 'ds_pan']
                    ,['title': 'Endêmica Brasil?', 'column': 'de_endemica_brasil']
                    ,['title': 'Atitude (m)', 'column': 'nu_altitude']
                    ,['title': 'Sensível?', 'column': 'ds_sensivel']
                    ,['title': 'Situacao avaliação', 'column': 'ds_situacao_avaliacao']
                    ,['title': 'Justificativa (quando não utilizado)', 'column': 'tx_justificativa_nao_usado_avaliacao']
                    ,['title': 'Motivo (quando não utilizado)', 'column': 'ds_motivo_nao_utilizado']
                    ,['title': 'Prazo de carência', 'column': 'de_prazo_carencia']
                    ,['title': 'Datum', 'column': 'no_datum']
                    ,['title': 'Formato Original', 'column': 'no_formato_original']
                    ,['title': 'Precisão da Coordenada', 'column': 'no_precisao']
                    ,['title': 'Referência de Aproximação', 'column': 'no_referencia_aproximacao']
                    ,['title': 'Metodologia de Aproximação', 'column': 'no_metodologia_aproximacao']
                    ,['title': 'Descrição Metodo de aproximação', 'column': 'tx_metodologia_aproximacao']
                    ,['title': 'Taxon Originalmente citado', 'column': 'no_taxon_citado']
                    ,['title': 'Presença atual ', 'column': 'ds_presenca_atual']
                    ,['title': 'Tipo de registro', 'column': 'no_tipo_registro']
                    ,['title': 'Dia', 'column': 'nu_dia_registro']
                    ,['title': 'Mês', 'column': 'nu_mes_registro']
                    ,['title': 'Ano', 'column': 'nu_ano_registro']
                    ,['title': 'Hora', 'column': 'hr_registro']
                    ,['title': 'Dia fim', 'column': 'nu_dia_registro_fim']
                    ,['title': 'Mês fim', 'column': 'nu_mes_registro_fim']
                    ,['title': 'Ano fim', 'column': 'nu_ano_registro_fim']
                    ,['title': 'Data e Ano desconhecidos', 'column': 'ds_data_desconhecida']
                    ,['title': 'Localidade', 'column': 'no_localidade']
                    ,['title': 'Característica da localidade', 'column': 'ds_caracteristica_localidade']
                    ,['title': 'UC Federais', 'column': 'no_uc_federal']
                    ,['title': 'UC Não Federais', 'column': 'no_uc_estadual']
                    ,['title': 'RPPN', 'column': 'no_rppn']
                    ,['title': 'País', 'column': 'no_pais']
                    ,['title': 'Estado', 'column': 'no_estado']
                    ,['title': 'Município', 'column': 'no_municipio']
                    ,['title': 'Ambiente', 'column': 'no_ambiente']
                    ,['title': 'Hábitat', 'column': 'no_habitat']
                    ,['title': 'Elevação mínima', 'column': 'vl_elevacao_min']
                    ,['title': 'Elevação máxima', 'column': 'vl_elevacao_max']
                    ,['title': 'Profundidade mínima', 'column': 'vl_profundidade_min']
                    ,['title': 'Profundidade máxima', 'column': 'vl_profundidade_max']
                    ,['title': 'Identificador', 'column': 'de_identificador']
                    ,['title': 'Data identificação', 'column': 'dt_identificacao']
                    ,['title': 'Identificação incerta', 'column': 'no_identificacao_incerta']
                    ,['title': 'Tombamento', 'column': 'ds_tombamento']
                    ,['title': 'Instituição tombamento', 'column': 'ds_instituicao_tombamento']
                    ,['title': 'Compilador', 'column': 'no_compilador']
                    ,['title': 'Data compilação', 'column': 'dt_compilacao']
                    ,['title': 'Revisor', 'column': 'no_revisor']
                    ,['title': 'Data revisão', 'column': 'dt_revisao']
                    ,['title': 'Validador', 'column': 'no_validador']
                    ,['title': 'data validação', 'column': 'dt_validacao']
                    // não precisa mais estas colunas, já tem a coluna ref. bib.
                    //,['title': 'Título', 'column': 'de_titulo_publicacao']
                    //,['title': 'Autor registro', 'column': 'no_autor']
                    //,['title': 'Ano registro', 'column': 'nu_ano_publicacao']
                    ,['title': 'Base de Dados', 'column': 'no_base_dados']
                    ,['title': 'Id Origem', 'column': 'id_origem']
                    ,['title': 'Observação', 'column': 'tx_observacao']
                    ,['title': 'Referência bibliográfica', 'column': 'tx_ref_bib']
                    ,['title': 'Bioma', 'column': 'no_bioma']
                    ]
                model.listColunas.sort{ Util.removeAccents( it.title.toUpperCase() ) }
                break

            case 'selecionarColunasExportacaoTaxons':
                model.listColunas = [
                    ['title': 'Família'                  , 'column': 'no_familia'],
                    ['title': 'Classe'                   , 'column': 'no_classe'],
                    ['title': 'Ordem'                    , 'column': 'no_ordem'],
                    ['title': 'Nome comum'               , 'column': 'no_comum'],
                    ['title': 'Autor'                    , 'column': 'no_autor_taxon'],
                    ['title': 'Ano'                      , 'column': 'nu_ano_taxon'],
                    ['title': 'Nome taxon ciclo anterior', 'column': 'no_taxon_anterior'],
                    ['title': 'Ciclo avaliação'          , 'column': 'de_ciclo_avaliacao'],
                    ['title': 'Unidade'                  , 'column': 'sg_unidade_org'],
                    ['title': 'Situação ficha'           , 'column': 'de_situacao_ficha'],
                    ['title': 'Espécie endêmica?'        , 'column': 'de_endemica_brasil'],
                    ['title': 'Protegida legislação'     , 'column': 'de_protegida_legislacao'],
                    ['title': 'Categoria anterior'       , 'column': 'de_categoria_anterior'],
                    ['title': 'Critério anterior'        , 'column': 'de_criterio_anterior'],
                    ['title': 'Período avaliação'        , 'column': 'de_periodo_avaliacao'],
                    ['title': 'Categoria avaliação'      , 'column': 'de_categoria_avaliada'],
                    ['title': 'Critério avaliação'       , 'column': 'de_criterio_avaliado'],
                    ['title': 'Justificativa avaliação'  , 'column': 'tx_justificativa_avaliada'],
                    ['title': 'Período validação'        , 'column': 'de_periodo_validacao'],
                    ['title': 'Categoria validação'      , 'column': 'de_categoria_validada'],
                    ['title': 'Critério validação'       , 'column': 'de_criterio_validado'],
                    ['title': 'Justificativa validação'  , 'column': 'tx_justificativa_validada'],
                    ['title': 'Motivo mudança'           , 'column': 'de_motivo_mudanca'],
                    ['title': 'Grupo avaliado'           , 'column': 'no_grupo_avaliado'],
                    ['title': 'Subgrupo avaliado'        , 'column': 'no_subgrupo_avaliado'],
                    ['title': 'Recorte módulo público'   , 'column': 'no_grupo_recorte'],
                    ['title': 'Espécie migratória?'      , 'column': 'de_migratoria'],
                    ['title': 'Tendência populacional'   , 'column': 'de_tendencia_populacional'],
                    ['title': 'Biomas'                   , 'column': 'no_biomas'],
                    ['title': 'Estados'                  , 'column': 'no_estados'],
                    ['title': 'Bacia'                    , 'column': 'no_bacias'],
                    ['title': 'Unidade de conservação'   , 'column': 'no_ucs'],
                    ['title': 'Código CNUC'              , 'column': 'co_cnucs'],
                    ['title': 'Ameaça'                   , 'column': 'no_ameacas'],
                    ['title': 'Uso'                      , 'column': 'no_usos'],
                    ['title': 'Listas e convenções'      , 'column': 'no_listas_convencoes'],
                    ['title': 'Ação de conservação'      , 'column': 'no_acoes_conservacao'],
                    ['title': 'Planos de ação'           , 'column': 'no_pans']
                ]

                // regra removida por solicitação da equipe
                //if( session?.sicae?.user?.isADM() ) {
                    model.listColunas.push(['title': 'Data validação'           , 'column': 'dt_aceite_validacao'])
                //}
                model.listColunas.sort{ Util.removeAccents( it.title.toUpperCase() ) }
                break;

            case 'selecionarFichaCicloAnterior':
                // selecionar o ciclo anterior
                CicloAvaliacao ciclo = CicloAvaliacao.get( params.sqCicloAvaliacao )
                model.cicloAnterior = CicloAvaliacao.findByNuAnoLessThan( ciclo.nuAno,[sort:'nuAno',order:'desc',max:1] )
                break

            case 'cadastrarInformativo':
                model.proximoNumero = Informativo.createCriteria().get {
                    projections {
                        max "nuInformativo"
                    }
                } as Integer
                model.proximoNumero = ( model.proximoNumero ?: 0 ) + 1
                break;

            case 'modalInformativosPublicados':
                model.listInformativosPublicados = Informativo.findAllByInPublicadoAndNuInformativoIsNotNull('S',[sort: "nuInformativo",'order':'desc']);
                break;

            case "cadastrarRefBib":
                model.listTipoPublicacao = TipoPublicacao.findAllByCoTipoPublicacaoNotInList(['MUSEU', 'BANCO_DADOS_PESSOA', 'PLANO_MANEJO_UC'])
                break
            //----------------------------------------------------------------------
            case "importacaoFicha":
                // ler lista de planilhas ja importadas pelo usuario
                break

            case "exportarOcorrenciaDwca":
                // ler lista de planilhas ja importadas pelo usuario
                model.listCicloAvaliacao = CicloAvaliacao.list().sort{ it.nuAno }
                break

            case "alterarTaxonFicha":
                // recuperar a arvore taxonomica da ficha selecionada
                Ficha ficha = Ficha.get( params.sqFicha.toInteger() )
                if( ! ficha.canModify(session?.sicae?.user) )
                {
                    render '<h3>Edição não permitida!</h3>'
                    return
                }
                model.structure = ficha.taxon.getStructure()
                model.especie = ( ficha.taxon.nivelTaxonomico.nuGrauTaxonomico.toInteger() == grailsApplication.config.grau.especie.toInteger() )
                break

            case "alterarArvoreTaxonomica":
                // recuperar a arvore taxonomica da ficha selecionada
                Ficha ficha = Ficha.get( params.sqFicha.toInteger() )
                if( ! ficha.canModify(session?.sicae?.user) )
                {
                    render '<h3>Edição não permitida!</h3>'
                    return
                }
                // criar lista da árvore taxonomica atual da espécie até nivel genero apenas
                model.listArvoreAtual = [];
                model.structure = ficha.taxon.getStructure()
                Long sqTaxonFilho=null
                NivelTaxonomico.list(sort:'nuGrauTaxonomico',order:'desc').each {

                        String noNivel = Util.capitalize(it.coNivelTaxonomico)
                        if (model.structure['id' + noNivel]) {
                            model.listArvoreAtual.push([sqTaxon         : model.structure['id' + noNivel]
                                                        , noTaxon       : model.structure['nm' + noNivel]
                                                        , noNivel       : noNivel
                                                        , coTaxon       : it.coNivelTaxonomico
                                                        , nuGrau        : it.nuGrauTaxonomico
                                                        , sqTaxonFilho  : sqTaxonFilho]
                            )
                            sqTaxonFilho = model.structure['id' + noNivel].toLong()
                        }

                }
                model.listArvoreAtual.sort{it.nuGrau}
                break

            case "alterarPublicacaoFicha":
                model.publicacao = Publicacao.get( params.sqPublicacao.toLong() )
                break

            case 'modalOficinaImportarParticipante':
                model.listConsultas = consultaService.getConsultasDiretasRevisoesUsuario( params.sqCicloAvaliacao.toLong(), session.sicae.user )
                model.sqOficina = params.sqOficina
                model.sqCicloAvaliacao = params.sqCicloAvaliacao
                break;

            case 'visualizarPendenciaRevisaoFicha':
                model.fichaRevisao = null
                if( params.sqFicha ) {
                    DadosApoio contextoRevisao = dadosApoioService.getByCodigo('TB_CONTEXTO_PENDENCIA', 'REVISAO')
                    Ficha ficha = Ficha.get(params.sqFicha.toLong())
                    List pendencias = FichaPendencia.findAllByFichaAndContexto(ficha, contextoRevisao)
                    model.pendencias = pendencias
                }
                break;

            case 'modalHistoricoSituacaoExcluida':

                // ler o  historico da situacao excluida
                model.listHistorico = sqlService.execSql("""select fhs.dt_inclusao
                        , p.no_pessoa
                        , to_char(fhs.dt_inclusao,'DD/MM/YYYY hh:mi:ss') as dt_inclusao
                        , fhs.ds_justificativa
                        from salve.ficha_hist_situacao_excluida fhs
                        inner join salve.pessoa p on p.sq_pessoa = fhs.sq_usuario_inclusao
                        where fhs.sq_ficha = :sqFicha
                        order by fhs.dt_inclusao desc
                        """,[sqFicha:params.sqFicha.toLong()])

                break

            case 'modalUserJobs':
                model.jobs = UserJobs.findAllByUsuario( PessoaFisica.get(session.sicae.user.sqPessoa)
                    ,[sort: "dtInclusao", 'order':'desc'] )

        }

        if (!model.fileName) {
            model.fileName = ''
        }
        if (!model.title) {
            model.title = ''
        }
        if (!model.maxSize) {
            model.maxSize = 1000
        }
        if (!model.accept) {
            model.accept = '*'
        }
        if (!model.extension) {
            model.extension = 'pdf,zip,png,jpg.doc,txt,csv,doc,odt,ods,shp'
        }

        List extensions = []
        model.extension.split(',').each {
            extensions.push('"' + it + '"')
        }
        model.extension = '[' + extensions.join(',') + ']'
        try {
            render(template: "/modals/" + params.modalName, model: model)
        } catch( Exception e ) {
            render 'Janela modal ' + (params.modalName ?: '') + ' não mapeada ou com erro.<hr><p class="red text-left">'+e.getMessage()+'</p>';
        }
    }

    /**
     * metodo para criação dos grides da ficha
     * @return string html do gride
     */
    def getGrid() {
        if (!session?.sicae?.user) {
            render '<h3>Sessão Está Encerrada!</h3>'
            return
        }
        String strController
        String strAction
        sleep(300)
        switch (params?.gridName) {
        // ----------------------------------------------------------
            case "modalCadRefBib":
                strController = 'referenciaBibliografica'
                strAction = 'getGrid'
                break
        // ----------------------------------------------------------
            case "modalCadInstituicao":
                strController = 'instituicao'
                strAction = 'getGrid'
                break
        // ----------------------------------------------------------
            case "modalCadRefBibAnexos":
                strController = 'referenciaBibliografica'
                strAction = 'getGridAnexos'
                break
        // ----------------------------------------------------------
            case "modalImportacaoFichas":
                strController = 'ficha'
                strAction = 'getGridImportacaoFichas'
                break
        }
        forward controller: strController, action: strAction, params: params
    }

    def savePlanilhaFicha() {
        Map result = [:]
        if (!session?.sicae?.user) {
            result.status = 1
            result.type = 'error'
            result.msg = 'Sessão expirada. Efetue login novamente!'
            render result as JSON
            return
        }
        if (!params?.sqCicloAvaliacao) {
            result.status = 1
            result.type = 'error'
            result.msg = '<h3>Id do Ciclo de Avaliação não informado!</h3>'
            render result as JSON
            return
        }
        if (!params?.contexto) {
            result.status = 1
            result.type = 'error'
            result.msg = '<h3>Falta informar o contexto!</h3>'
            render result as JSON
            return
        }

        MultipartHttpServletRequest mpr = (MultipartHttpServletRequest) request
        CommonsMultipartFile file = (CommonsMultipartFile) mpr.getFile("file")
        if (!file || file.isEmpty()) {
            result.msg = 'Não consegui abrir o arquivo!'
            result.status = 1
            result.type = 'error'
            return result
        }

        // receber a planilha e grava nas tabela PLANILHA E PLANILHA_FICHA
        result = planilhaService.importarPlanilha(file
                , params.sqCicloAvaliacao.toInteger()
                , session.sicae.user.sqPessoa
                , session.sicae.user.sqUnidadeOrg
                , params.deComentario.toString()
                , grailsApplication.config.planilhas.dir
                , params.contexto
                , session.sicae.user.isADM()
                , session.sicae.user.email
        )
        render result as JSON
    }


    def deletePlanilha() {
        if (params.id) {
            Planilha p = Planilha.get(params.id.toLong() )
            if(p) {
                sqlService.execSql("delete from salve.planilha where sq_planilha = :id",[id:params.id.toLong()] )
            }
        }
        render ''
    }

    /**
     * Importar ref. bibliográficas no formato de planilha ou do arquivo .bib extraido do mendeley
     * @return
     */
    def savePlanilhaRefBib() {

        Map result = [:]
        result.status = 1
        result.type = 'error'

        if (!session?.sicae?.user) {
            result.msg = 'Sessão expirada. Efetue login novamente!'
            render result as JSON
            return
        }
        if (!params?.contexto) {
            result.msg = '<h3>Falta informar o contexto!</h3>'
            render result as JSON
            return
        }
        MultipartHttpServletRequest mpr = (MultipartHttpServletRequest) request
        CommonsMultipartFile file = (CommonsMultipartFile) mpr.getFile("file")
        if (!file || file.isEmpty()) {
            result.msg = 'Não consegui abrir o arquivo!'
            result.status = 1
            result.type = 'error'
            return result
        }

        // se tiver o fileName é o formato .bib
        if (params.fileName) {
            File fileJson = new File(grailsApplication.config.planilhas.dir + params.fileName + '.json')
            try {
                fileJson.write params.conteudo
            } catch (Exception e) {

                println2 'Erro gerando arquivo'
                println2 grailsApplication.config.planilhas.dir + params.fileName + '.json'
                println2 e.getMessage().substring(0, 500)
                println2 '-' * 100
            }
            result = planilhaService.importArquivoBib(params.contexto
                    , params.fileName
                    , params.fileSize
                    , params.conteudo
                    , params.deComentario
                    , session.sicae.user.sqUnidadeOrg
                    , session.sicae.user.sqPessoa
                    , session.sicae.user.email
                    , grailsApplication.config.planilhas.dir
            )
        }else {
            // receber a planilha e grava nas tabela PLANILHA E PLANILHA_FICHA
            result = planilhaService.importarPlanilha(file
                    , 0
                    , session.sicae.user.sqPessoa
                    , session.sicae.user.sqUnidadeOrg
                    , params.deComentario.toString()
                    , grailsApplication.config.planilhas.dir
                    , params.contexto
                    , session.sicae.user.isADM()
                    , session.sicae.user.email
            )
        }
        render result as JSON
    }


    /**
     * Método responsavel por disponibilizar ao usuário para download um arquivo desejado
     * @return [description]
     */
    def download() {
        //log.info('Main Controller Download')
        //log.info(params)
        String downloadName = Util.getFileNameOnly(params.downloadName?: params.fileName)
        File f
        String dirArquivos
        String dirAnexos
        String dirTemp
        String contentType
        String contentDisposition
        try {
            dirArquivos = grailsApplication.config.arquivos.dir
            dirAnexos   = grailsApplication.config.anexos.dir
            dirTemp     = grailsApplication.config.temp.dir
            contentType = "application/octet-stream"
            contentDisposition = ""
            if (params.fileName) {
                params.fileName = URLDecoder.decode(params.fileName, "UTF-8")
            }

            switch (params.fileName) {
                case 'planilha_atualizar_referencias_registros_salve':
                    params.fileName = 'planilha_atualizar_referencias_registros_salve.zip'
                    f = new File(dirArquivos + params.fileName)
                    if (f) {
                        contentDisposition = "filename=${params.fileName}"
                    }
                    break
                case 'planilha_modelo_importacao_registro':
                    params.fileName = 'planilha_modelo_importacao_registros_salve.zip'
                    f = new File(dirArquivos + params.fileName)
                    if (f) {
                        contentDisposition = "filename=${params.fileName}"
                    }
                    break
                case 'planilha_modelo_importacao_ficha':
                    params.fileName = 'planilha_modelo_importacao_fichas_salve.zip'
                    f = new File(dirArquivos + params.fileName)
                    if (f) {
                        contentDisposition = "filename=${params.fileName}"
                    }
                    break
                case 'planilha_modelo_importacao_ref_bib':
                    params.fileName = 'planilha_modelo_importacao_ref_bib.zip'
                    f = new File(dirArquivos + params.fileName)
                    if (f) {
                        contentDisposition = "filename=${params.fileName}"
                    }
                    break

                case 'manual_salve':
                    params.fileName = 'manual_salve.pdf'
                    contentType = 'application/pdf'
                    f = new File(dirArquivos + params.fileName)
                    if (f) {
                        contentDisposition = "filename=${params.fileName}"
                    }
                    break
                case 'politica_dados_avaliacao':
                    params.fileName = 'politica_dados_avaliacao.pdf'
                    contentType = 'application/pdf'
                    f = new File(dirArquivos + params.fileName)
                    if (f) {
                        contentDisposition = "filename=${params.fileName}"
                    }
                    break
                default:
                    f = new File(params.fileName)
                    if (!f || !f.exists()) {
                        f = new File(dirAnexos + params.fileName)
                        if (!f || !f.exists()) {
                            f = new File(dirArquivos + params.fileName)
                            if (!f || !f.exists()) {
                                f = new File(dirTemp + params.fileName)
                                if (!f || !f.exists()) {
                                    f = new File(dirArquivos + 'arquivo-nao-encontrado.pdf')
                                    if (!f.exists()) {
                                        throw new Exception('Arquivo inexitente: ' + params.fileName)
                                    }
                                }
                            }
                        }
                    }

                    contentDisposition = "attachment;filename=\"${downloadName}\""
                    if (params.fileName.indexOf('.pdf') > -1) {
                        contentType = 'application/pdf'
                        //contentDisposition = "filename=${f.name}"
                    } else if (params.fileName =~ /(?i)\.(txt|properties|log|sql|config|csv)$/) {
                        contentType = 'text/plain'
                        contentDisposition = "attachment;filename=\"${downloadName}\""
                    } else if (params.fileName =~ /(?i)\.(jpe?g|bmp|png|gif|tiff)$/) {
                        contentType = 'image/' + Util.getFileExtension(f.getName()).toLowerCase()
                        contentDisposition = "filename=${downloadName}"
                        /*println 'Download de imagem'
                    println 'contentType ' + contentType
                    println "-"*100
                    println ' '
                    */
                    } else if (params.fileName.indexOf('.zip') > -1) {
                        contentType = 'application/zip'
                    }

            } // end switch

            if (params.disposition) {
                if (params.disposition == 'filename') {
                    contentDisposition = "filename=${downloadName}"
                } else if (params.disposition == 'attachment') {
                    contentDisposition = "attachment;filename=\"${downloadName}\""
                }
            }

            // se não existir o arquivo, devolver arquivo padrão arquivo-nao-encontrado.pdf
            if (!f || !f.exists()) {
                f = new File(grailsApplication.config.arquivos.dir + 'arquivo-nao-encontrado.pdf')
                contentDisposition = "attachment;filename=\"${downloadName}\""
                contentType = 'application/pdf'
            }

            if (f && f.exists()) {
                response.setHeader("Content-Type", contentType)
                response.setHeader("Content-Disposition", contentDisposition)
                response.setHeader("Content-Length", f.size().toString())
                response.setContentType(contentType)
                /*response.outputStream << f.bytes
                response.outputStream.flush()*/
                try {
                    FileInputStream fileInputStream = new FileInputStream(f);
                    byte[] buf = new byte[4196];
                    int bytesread, bytesBuffered
                    while ((bytesread = fileInputStream.read(buf)) > -1) {
                        response.outputStream.write(buf, 0, bytesread);
                        bytesBuffered += bytesread;
                        if (bytesBuffered > 1024 * 1024) { //flush after 1MB
                            bytesBuffered = 0
                            response.outputStream.flush()
                        }
                    }
                    // excluir o arquivo temporario
                    // não pode ser excluido aqui senão o timer do download não encontra o arquivo
                    /*if (params.delete && params.delete.toInteger() == 1) {
                        f.delete()
                    }*/
                }
                finally {
                    if (response.outputStream != null) {
                        response.outputStream.flush();
                    }
                }
                return
            } else {
                //render '<html><body><br><center><h3>Arquivo <i>' + params.fileName + '</i> não encontrado!</h3><h5><a href="javascript:history.back();">Voltar</a></h5></center></body></html>'
            }
        } catch( Exception e) {}
        render(status: 503, text: 'Arquivo inexitente: ' + params.fileName)
    }

    /*
    Metodo utilizado para dar carga de taxons do arquivo localizado em : /data/salve-estadual/arquivos/planilha_importacao_taxon.xls
    Exemplo das Colunas da planilha:
    // http://localhost:8080/salve-estadual/main/importarEspecies/<HISTORICO>
    // https://salve.icmbio.gov.br/salve-estadual/main/importarEspecies/Importacao taxon para criacao de fichas de odonata
    "nm_reino";"nm_filo";"nm_classe";"nm_ordem";"nm_familia";"nm_genero";"nm_subfamilia";"nm_tribo";"nm_especie";"nm_subespecie";"no_autor"
    "Animalia";"Annelida";"Polychaeta";"Phyllodocida";"Glyceridae";"Hemipodia";"californiensis";;"(Hartman, 1938)"
    */

    def importarEspecies() {

        if (!session?.sicae?.user?.sqPessoa) {
            render 'Login SICAE não realizado'
            return
        }
        if (!session?.sicae?.user?.isADM()) {
            render 'Acesso restrito a Administradores!'
            return
        }
        /*render 'importar especies desativada'
        return
        */
        //redirect(uri:'/')
        //return
        render taxonService.importarEspecies( session.sicae.user.sqPessoa.toInteger(), params.historico)

        // atualizar coluna json_trilha da tabela taxonomia
        sqlService.execSql("update taxonomia.taxon set json_trilha = taxonomia.criar_trilha( sq_taxon ) where json_trilha is null")

        return
    }

    def updateArquivoSistema() {
        Map res = [status: 0, msg: 'Nenhum arquivo foi enviado', type: 'info']
        String originalFileName
        String acao = ''
        String directory = grailsApplication.config.arquivos.dir ?: '/data/salve-estadual/arquivos/'
        CommonsMultipartFile f = request.getFile('fldArquivoSistema')
        String dirMapsUserUpload // nome da pasta no temp criada para upload do mapas pelo usuário
        if (f && !f.empty) {
            originalFileName = f.getProperties().fileItem.name
            params.fileName  = params.fileName ?: originalFileName
            // sempre que for um shp o nome do arquivo será o mesmo do arquivo original
            if( params.fileName.toLowerCase() == 'shapefile' ) {
                params.fileName = originalFileName
                directory +='shapefiles/'
            } else if ( params.fileName.toLowerCase() == 'mapas' ) { // para mapas enviados pelo usuários
                directory = grailsApplication.config.temp.dir ?: '/data/salve-estadual/temp/'
                params.fileName = originalFileName
                dirMapsUserUpload = session.sicae.user.nuCpf + '-' + new Date().format('HH-mm-ss').toString()
                directory += 'mapas/' + dirMapsUserUpload
            } else if ( params.fileName.toLowerCase() == 'historico_avaliacao' ) { // para planilha historico avaliacao enviados pelo usuários
                directory = grailsApplication.config.temp.dir ?: '/data/salve-estadual/temp/'
                params.fileName = originalFileName
                directory += 'historico_avaliacao/'
            } else if ( params.fileName.toLowerCase() == 'referencias_ocorrencias' ) { // para atualizar as referencias bibliograficas dos registros de ocorrencias do salve
                acao = 'atualizar_referencias_ocorrencias'
                directory = grailsApplication.config.temp.dir ?: '/data/salve-estadual/temp/'
                params.fileName = originalFileName
            } else if ( params.fileName.toLowerCase() == 'fotos_audios' ) { // para envio de fotos ou áudios (Aba 10 - Multimida) em lote
                directory = grailsApplication.config.temp.dir ?: '/data/salve-estadual/temp/'
                params.fileName = originalFileName
                directory += 'fotos_audios/'
            }

                // extensoes proibidas
            String ext = Util.getFileExtension(params.fileName)
            if (['bat', 'sh', 'run', 'bin', 'exe', 'deb', 'rpm'].findAll { it == ext }) {
                res.msg = 'Extensão ' + ext + ' não permitida!'
                render res as JSON
                return
            }

            File savePath = new File( directory )
            if (!savePath.exists()) {
                if (!savePath.mkdirs()) {
                    res.msg = 'Não foi possível criar o diretório ' + savePath.asString()
                    res.status = 1
                    res.type = 'error'
                    render res as JSON
                    return
                }
            }

            String fileName = savePath.absolutePath + '/' + params.fileName
                .replaceAll(/\/|\\|\+|\?|\^|\*| /, '_') // trocar os caracteres "/\+?*^" para sublinhado
                .replaceAll(/_{2,}/, '_') // trocar 2 ou mais sublinhados seguidos para um unico sublinhado
                .replaceAll(/^_/,'') // remover sublinhado do início do nome
            try {
                f.transferTo(new File(fileName))
                res.status = 0
                res.msg = 'Arquivo atualizado com SUCESSO!'
                res.type = 'success'
                if( directory =~ /shapefiles/) {
                    _descompactarShapeFile( fileName )
                } else if ( directory =~ /mapas/) { // Mapas das Fichas / Upload ZIP
                        res = mainService.processaUploadMapas(fileName
                            , dirMapsUserUpload
                            , params.sqCicloAvaliacao.toLong()
                            , Util.getBaseUrl(request.getRequestURL().toString())
                            , session.sicae.user)
                } else if ( directory =~ /historico_avaliacao/) { // Planilha Historioco de Avaliação
                    res = mainService.processaPlanilhaHistoricoAvaliacao(fileName
                        , params.sqCicloAvaliacao.toLong()
                        , Util.getBaseUrl(request.getRequestURL().toString())
                        , session.sicae.user)
                } else if ( directory =~ /fotos_audios/) { // Fotos ou áudios (Aba 10 - Multimida)
                        res = mainService.processaUploadFotosAudios(fileName
                            , params.sqCicloAvaliacao.toLong()
                            , Util.getBaseUrl(request.getRequestURL().toString())
                            , session.sicae.user)
                } else if( acao == 'atualizar_referencias_ocorrencias'){
                    mainService.processarPlanilhaAtualizarReferenciaOcorrencias(fileName,session.sicae.user)
                    res.msg = 'Planilha recebida com SUCESSO<br>O resultado do processamento será enviado para <b>'+session.sicae.user.email+'</b>.'
                    res.type='modal'
                }
            }
            catch (Exception e) {
                res.status = 1
                res.msg = e.getMessage()
                res.type = 'error'
            }

        }
        render res as JSON
    }

    def select2Unidade() {
        List lista
        Map data = ['items': []]
        if (!params?.q?.trim()) {
            render ''
            return
        }

        String q = '%' + params.q.trim() + '%'
        lista = Unidade.findAllByNoPessoaIlikeOrSgUnidadeIlike(q, q)
        if (lista) {
            lista.each {
                data.items.push(['id'       : it.id,
                                 'descricao': it.sgUnidade
                ])
            }
            render data as JSON
            return
        }
        render ''
    }

    def select2Categoria() {

        List lista
        Map data = ['items': []]
        if( params.q )
        {
            params.q = '%' + params.q.trim() + '%'
        }
       lista = DadosApoio.createCriteria().list {
            createAlias('pai','p')
            if( params.q ) {
                ilike('descricao', params.q)
            }
            eq('p.codigoSistema','TB_CATEGORIA_IUCN')
            order('ordem')
        }
        if (lista) {
            lista.each {
                data.items.push(['id'       : it.id,
                                 'descricao': it.descricao+' ('+it.codigo+')'
                ])
            }
            render data as JSON
            return
        }
        render ''
    }

    def select2Instituicao() {
        Map data = ['items': []]
        Instituicao.createCriteria().list() {
            pessoa{
                order('noPessoa', 'asc')
            }
            or {
                pessoa{
                    ilike('noPessoa', '%' + params.q + '%')
                }
                ilike('sgInstituicao', '%' + params.q + '%')
            }
        }.each {
            data.items.push(['id': it.id, 'descricao': it.sgInstituicao ?: it.pessoa.noPessoa])
        }
        render data as JSON
    }

    /**
     * selecionar as instituições cadastradas no próprio SALVE pelo módulo de consulta web
     * @return
     */
    def select2WebInstituicao() {

        Map data = ['items': []]
        if (!params.q || params.q.length() < 3) {
            return ''
        }
        List lista = br.gov.icmbio.WebInstituicao.createCriteria().list() {
            if (params.q) {
                or {
                    ilike('sgInstituicao', '%' + params.q.trim() + '%')
                    ilike('noInstituicao', '%' + params.q.trim() + '%')
                }
            }
        }.sort { it.noInstituicao }

        if (lista) {
            lista.each {
                data.items.push(['id': it.id, 'descricao': it.sgInstituicao ? Util.capitalize(it.noInstituicao) : it.sgInstituicao])
            }
            render data as JSON
            return
        }
        render ''
    }

    def getAndamentoImportacaoPlanilhas() {
        Map res = [data: []]
        Planilha.createCriteria().list {
            cache: false
            eq('inProcessando', true)
        }.each {
            res.data.push([id: it.id, percentual: it.nuPercentual, situacao: (it.inProcessando ? 'Processando' : 'Finalizada')])
        }
        render res as JSON
    }

    def dashboard() {
        String cacheName = 'dashboard-tela-inicial'
        Integer cacheDurationInMinutes = 60
        DadosApoio categoriaNE = dadosApoioService.getByCodigo('TB_CATEGORIA_IUCN','NE')
        //DadosApoio situacaoPubalicada = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA','PUBLICADA')
        try {

            if (!session?.sicae?.user) {
                render ''
                return
            }

            // não exibir dashboard para perfil validador
            if (session?.sicae?.user?.isVL() ) {
                render ''
                return
            }

            // ler o ultimo ciclo aberto, se não encontrar, ler o mais recente
            //List listCiclos =  CicloAvaliacao.findAllByInSituacaoAndInOficina('A', 'S', [max: 2, sort: 'nuAno', order: 'desc'])
            List listCiclos = [ session.getAttribute('ciclo') ] //CicloAvaliacao.findAllByInOficina( 'S', [ sort: 'nuAno', order: 'desc' ] )
            if ( ! listCiclos ) {
                render ''
                return
            }

            // salve sem ciclo utilizar o último ciclo cadastrado
            //session.setAttribute('ciclo',listCiclos[0])

            List resultado = cacheService.getFromFileCache( cacheName ).cache
            if( ! resultado ) {
                resultado = []
                if (!listCiclos) {
                    listCiclos = CicloAvaliacao.findAllByInSituacaoAndInOficina('F', 'S', [max: 1, sort: 'nuAno', order: 'desc'])
                    if (!listCiclos) {
                        render ''
                        return
                    }
                }
                // calcular Unidade x Ficha
                //
                //def list = OficinaFicha.executeQuery("select ficha, count(*) from OficinaFicha group by ficha")
                // println OficinaFicha
                // DadosApoio excluida = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA','EXCLUIDA')
                listCiclos.each { ciclo ->
                    Map dados = [tituloCiclo        : ciclo.deCicloAvaliacao + ' - ' + ciclo.nuAno + ' a ' + ciclo.anoFinal
                                 , qtdFichas        : 0
                                 , qtdFichaValidadas: 0
                                 , qtdFichaAvaliadas: 0
                                 , qtdFichaPublicadas: 0
                                 , centros          : [:]]
                    VwFicha.createCriteria().list {
                        eq('sqCicloAvaliacao', ciclo.id.toInteger())
                        ne('cdSituacaoFichaSistema','EXCLUIDA')
                    }.each {
                        dados.qtdFichas++
                        Unidade unidade = it.unidade
                        if (!dados.centros[unidade.sgUnidade]) {
                            dados.centros[unidade.sgUnidade] = [somaFichas: 0, fichas: [], oficinas: [], somaPercentual: 0, qtdFichaAvaliadas: 0, qtdFichaValidadas: 0, qtdFichaPublicadas: 0]
                        }
                        // as fichas do primeiro ciclo não tiveram oficina então contabilisar como
                        // avaliada as que possuirem resultado final
                        if( ciclo.nuAno == 2010 ) {
                            // considerar a Validacao para o 1º cilo
                            if ( it.sqCategoriaFinal && it.sqCategoriaFinal != categoriaNE.id) {
                              dados.centros[unidade.sgUnidade].qtdFichaAvaliadas = dados.centros[unidade.sgUnidade].qtdFichaAvaliadas + 1
                            }
                        } else {
                            // considerar a oficina de avaliação
                            if ( it.sqCategoriaIucn && it.sqCategoriaIucn != categoriaNE.id ) {
                            //if ( it.sqCategoriaFinal ) {
                                dados.centros[unidade.sgUnidade].qtdFichaAvaliadas = dados.centros[unidade.sgUnidade].qtdFichaAvaliadas + 1
                            }
                        }

                        // acumular fichas PUBLICADAS
                        if ( it.cdSituacaoFichaSistema == 'PUBLICADA') {
                            dados.centros[unidade.sgUnidade].qtdFichaPublicadas = dados.centros[unidade.sgUnidade].qtdFichaPublicadas + 1
                        }

                        // acumular percentual das fichas
                        dados.centros[unidade.sgUnidade].somaPercentual = dados.centros[unidade.sgUnidade].somaPercentual + it.vlPercentualPreenchimento
                        dados.centros[unidade.sgUnidade].somaFichas = dados.centros[unidade.sgUnidade].somaFichas + 1
                    }
                    DadosApoio oficinaAvaliacao = dadosApoioService.getByCodigo('TB_TIPO_OFICINA', 'OFICINA_AVALIACAO')
                    List oficinasFichas = OficinaFicha.createCriteria().list {
                        oficina {
                            eq('cicloAvaliacao', ciclo)
                            eq('tipoOficina', oficinaAvaliacao)
                        }
                    }.each {
                        String sigla = it?.oficina?.unidade?.sgUnidade?.toString()
                        if (dados.centros[sigla]) {

                            if (dados.centros[sigla].fichas.indexOf(it.vwFicha.id.toInteger()) == -1) {
                                dados.centros[sigla].fichas.push(it.vwFicha.id.toInteger())
                            }

                            if (dados.centros[sigla].oficinas.indexOf(it.oficina.id.toInteger()) == -1) {
                                dados.centros[sigla].oficinas.push(it.oficina.id.toInteger())
                            }
                        }
                    }

                    // ler dados das consultas
                    dados.consultas=[]
                    CicloConsulta.createCriteria().list {
                        eq('cicloAvaliacao', ciclo)
                        tipoConsulta {
                            order('codigoSistema')
                        }
                        tipoConsulta {
                            order('descricao', 'asc')
                        }
                        order('dtInicio','desc')
                    }.each {
                        dados.consultas.push([dtFim:it.dtFim
                                               ,tipoConsulta:it.tipoConsulta.descricao
                                               ,deTituloConsulta:it.deTituloConsulta
                                               ,periodoText:it.periodoText
                                               ,stAberta: (it.dtFim >= new Date())
                                               ,corTexto: (it.dtFim >= new Date() ? '#369736' : '#000000')
                        ])
                    }

                    // ler dados das oficinas
                    dados.oficinas = []
                    Oficina.createCriteria().list {
                        eq('cicloAvaliacao', ciclo)
                        order('dtInicio','desc')
                    }.each {
                        dados.oficinas.push([
                                corTexto  : (it?.situacao?.codigoSistema != 'ENCERRADA' ? '#369736' : '#000000')
                                , stAberta: (it?.situacao?.codigoSistema != 'ENCERRADA' ? true : false)
                                , sgOficina : it?.sgOficina
                                , deLocal: it.deLocal
                                , sgUnidade : it?.unidade?.sgUnidade
                                , dePeriodo: it.periodo
                                , deSituacao: it?.situacao?.descricao
                        ])
                    }

                    // adicionar os dados estatisticos no array resultado
                    resultado.push(dados)
                }
                cacheService.setToFileCache(cacheName,resultado,cacheDurationInMinutes)
            }
            //cacheService.setEnabled(false)
            render(template: 'dashboard', model: [resultado: resultado])
        } catch (Exception e) {
            //e.printStackTrace()
            println 'Erro dashboard: ' + e.getMessage()
            render ''
        }

    }
    /**
     * limpar os caches da memória.
     * Opcionalmente pode ser passado o parametro key para limpeza específica
     *
     * @example /clearCache?key=ficha-list
     * @return
     */
    def clearCache() {
        render '<pre>'
        render '<h3>Cache limpo as ' + new Date().format('dd/MM/yyyy HH:mm:ss') + '</h3>'
        render '<div style="font-weight:bold;"><ul>'
        int qtd = cacheService.clear( params.key )
        if (params.key) {
            render '<li>Chave: ' + params.key+'</li>'
        }
        render '<li>cache/arquivos removidos: ' + qtd +'</li>'
        render '</ul></div>'
        render '</pre>'
    }
    /**
     * lista as chaves dos caches armazenamdos
     * @return
     */
    def listCache() {
        List dados = []
        render '<pre><h2>SALVE - CACHE SERVICE</h2><h3>Listagem dos caches</h3>'
        render '<hr>'
        dados.push( '<u>Cache SESSÃO</u>')
        if ( session.cache ) {
            session.cache.each {
                dados.push( '<a title="Excluir" class="cursor-pointer" target="_blank" href="/salve-estadual/main/clearCache/' + it.key + '">'+it.key+'</a>  /  Válido até: <b>' + it.value.cacheInfo.dateEnd.format('dd/MM/yyyy HH:mm:ss') + '</b>')
            }
        }

        // cache em disco
        String cacheDir = grailsApplication?.config?.cache?.dir
        if( cacheDir ) {
            dados.push( '<u>Cache DISCO</u>')
            def dir = new File(cacheDir)
            def files = []
            dir.traverse(type: FILES, maxDepth: 0) { files.add(it) }
            files.each {
                if (it.name ==~ /.*cache$/) {
                    dados.push( '<a title="Excluir" class="cursor-pointer" target="_blank" href="/salve-estadual/main/clearCache/' + it.name + '">'+it.name + '</a>' )
                }
            }
        }

        if( ! dados )
        {
            render '<h3>Nenhum cache armazenado.</h3>'
        } else {
            render '<ul>'+dados.collect{'<li>'+it+'</li>'}.join(' ')+'</ul>'
        }
        render '</pre>'
    }



    /**
     * verificar andamento das atividades
     * @return List
     */
    def getWorkers() {
        try {
            if (!session?.sicae?.user?.sqPessoa) {
                throw new Exception('Sessão expirada.')
            }
            List jobs = userJobService.getUserJobsAsList( session.sicae.user.sqPessoa)?.findAll {
                return !it.stExecutado && !it.stCancelado
            }
            render jobs as JSON
        }
        catch (Exception e) {
            Map error = ['error': e.getMessage()]
            render error as JSON
        }
    }


    /**
     * retornar as tarefas em execução do usuário
     * @return
     */
    def getUserJobs() {
        Map res = [ status:0, msg:'', data: [ jobs:[] ] ]
        try {
            if (!session?.sicae?.user?.sqPessoa ) {
                throw new RuntimeException('Sessão expirada!')
            }
            res.data.jobs = userJobService.getUserJobsAsList( session?.sicae?.user?.sqPessoa )
        }
        catch (Exception e) {
            res.status = 1
            res.msg = e.getMessage()
        }
        render res as JSON
    }

    /**
     * excluir tarefa da tabela user_jobs
     * @param sqUserJob
     * @return
     */
    def cancelUserJob(){
        Map res = [status:0,type:'success',msg:'', data:[:] ]
        try {
            if( ! session?.sicae?.user ) {
                throw new Exception('Sessão expirada, necessário fazer login novamente')
            }
            if( !params.sqUserJob ){
                throw new Exception('Id da tarefa não informado')
            }
            if( params.sqUserJob.toLong() > 0l ) {
                res.data.sqUserJob = userJobService.cancelUserJob( params.sqUserJob.toLong(), session.sicae.user.sqPessoa.toLong() )
            } else {
                userJobService.cancelAllUserJobs(session.sicae.user.sqPessoa.toLong() )
            }
        } catch(Exception e ){
            res.status  = 1
            res.type    = 'error'
            res.msg     = e.getMessage()
        }
        render res as JSON
    }

    /**
     * atualizar dados tabela user_jobs
     */
    def updateUserJob(){
        Map res = [status:0,type:'success',msg:'']
        try {
            if( params.sqUserJob && session?.sicae?.user?.sqPessoa ) {
                PessoaFisica pf = PessoaFisica.get( session.sicae.user.sqPessoa )
                UserJobs uj = UserJobs.get( params.sqUserJob.toLong())
                if( uj && uj.usuario == pf ) {
                    if( params.stExecutado != null ) {
                        uj.stExecutado = params.stExecutado.toBoolean()
                        uj.save( flush:true )
                    }
                    userJobService.asyncMail(uj)
                }
            }
        } catch( Exception e ){
            println e.getMessage()
        }
        render res as JSON

    }

    /**
     * excluir atividade da lista
     * @param id
     * @return
     */
    def deleteWorker(String id ) {
        println 'DeleteWorker '+ id
        Map res = [status:0,type:'success',msg:'']
        try {
            if (!session?.sicae?.user?.sqPessoa) {
                throw new RuntimeException('Sessão expirada!')
            }
            res = workerService.delete(id, session)
        }
        catch (Exception e) {
            // Map error = ['error': e.getMessage()]
            //render error as JSON
        }
        render res as JSON
    }
    /**
     * excluir todas a atividade do usuário que está executando em segundo plano
     * @return
     */
    def deleteAllUserWorkers() {
        Map res = [status:0,type:'success',msg:'']
        try {
            if (!session?.sicae?.user?.sqPessoa) {
                throw new RuntimeException('Sessão expirada!')
            }
            println 'deleteAllUserWorkers ' + session.sicae.user.noPessoa
            res = workerService.deleteAll(session)
        }
        catch (Exception e) {
             res.error = e.getMessage()
             res.status=1
        }
        render res as JSON
    }

    def gc()
    {
        // limpar todos os caches do servico de cache
        cacheService.clear()
        try {
            render '<h1>Executando Garbage Collect</h1>'
            render '<h2>Antes</h2>'
            mem()
            System.gc();
            Thread.sleep(100);
            System.runFinalization();
            Thread.sleep(100);
            System.gc();
            Thread.sleep(100);
            System.runFinalization();
            Thread.sleep(100);
            render '<h2>Depois</h2>'
            mem()
        } catch ( InterruptedException ex ) {
            //ex.printStackTrace();
            render ex.getMessage()
            return
        }
        render '<br>Garbage collect executado!'
    }

    def mem()
    {
        try {
            long timeout   = session.getMaxInactiveInterval()
            long maxMemory = Runtime.getRuntime().maxMemory()
            long totalMemory = Runtime.getRuntime().totalMemory()
            long freeMemory = Runtime.getRuntime().freeMemory()
            render '<pre>'
            render '<br>Informações da Memória'
            render '<br>Maxima..........: ' + (maxMemory/1024/1024).toInteger().toString() + ' MB'
            render '<br>Total...........: ' + (totalMemory/1024/1024).toInteger().toString() + ' MB'
            render '<br>Livre...........: ' + (freeMemory/1024/1024).toInteger().toString() + ' MB'
            render '<br>Utilizada.......: ' + ( (totalMemory - freeMemory)/1024/1024).toInteger().toString() + ' MB'
            render '<br>Processadores...: ' + Runtime.getRuntime().availableProcessors()
            render '<br>Session timeout.: ' + (timeout / 60) + ' minutos'

        } catch ( InterruptedException ex ) {
            //ex.printStackTrace();
            render ex.getMessage()
        }
    }

    /**
     * recuperar a coluna text de uma tabela
     * @return
     */
    def getText()
    {
        sleep(1000)// terminar de montar a tela e exibir o spinner
        //String texto = '<h3>texto não encontrado.</h3>'
        String texto = ''
        if ( ! session.sicae ) {
            render '<h3>Sessão expirada.</h3>'
            return
        }
        try {
            String cmdSql = "select a.${params.column.trim()} as texto from salve.${params.table.toLowerCase() } a where a.sq_${params.table.toLowerCase()} = :id"
            Sql sql = new Sql( dataSource )
            try
            {
                def data = sql.rows( cmdSql,[[id:params.id.toLong()]] )
                if( data.size() > 0 ) {
                    if( ! data[0].texto ) {
                        if ( params.column.toLowerCase().trim() == 'ds_justificativa') {
                            if (params.table.toLowerCase().trim() == 'oficina_ficha') {
                                OficinaFicha oficinaFicha = OficinaFicha.get( params.id.toLong() )
                                if( oficinaFicha ) {
                                    if( oficinaFicha.oficina.tipoOficina.codigoSistema == 'OFICINA_AVALIACAO' )
                                    {
                                        // texto da aba 11.6
                                        texto = oficinaFicha.ficha.dsJustificativa ?: ''
                                    } else if( oficinaFicha.oficina.tipoOficina.codigoSistema == 'OFICINA_VALIDACAO' )
                                    {
                                        // texto da aba 11.7
                                        texto = oficinaFicha.ficha.dsJustificativaFinal ?: ''
                                    }
                                    // ler do historico
                                    if( !texto  ) {
                                        params.sqCicloAvaliacao = oficinaFicha.vwFicha.sqCicloAvaliacao
                                        params.colHistorico = true
                                        params.sqFichaFiltro = oficinaFicha.vwFicha.id
                                        data = fichaService.search(params)
                                        if (data && data[0].tx_justificativa_aval_nacional) {
                                            texto = '<h4>Justificativa da última avaliação nacional</h4>' + data[0].tx_justificativa_aval_nacional
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                       texto = data[0].texto
                    }
                }
            } catch(Exception e ) {
                println2 e.getMessage()
            }
            sql.close()
        }
        catch( Exception e ) {
            render e.getMessage()
        }
        if( texto ) {
            texto = texto.replaceAll(/&nbsp;/, ' ')
            texto = Util.stripTagsFichaPdf(texto)
        }
        render  raw(  texto ?: '' )
    }

    /**
     * método para remover formatacao html dos texto colados nos campos texto do editor rico
     * @site: https://jsoup.org/cookbook/cleaning-html/whitelist-sanitizer
     * @param html
     * @return
     */
    def htmlSanitizer()
    {
        // https://jsoup.org/apidocs/org/jsoup/safety/Whitelist.html#removeAttributes-java.lang.String-java.lang.String...-
        //sleep(3000);
        // remover <font color -> html = html.replaceAll("<\\/{0,1}font.*?>","");
        // cleanstr = cleanstr.replaceAll("(?i)<\\/?font[^>]*>", "");
        /*
        //remove style attribute
            Elements elms = doc.select("*").not("img");
            for (Element e : elms) {
                String attr = e.attr("style");
                if(!"".equals(attr) || null!=attr){
                    e.attr("style", "");
            }
        }
         */
        params.html = params.html ?:''
        Whitelist allowedTags = Whitelist.simpleText(); // This whitelist allows only simple text formatting: b, em, i, strong, u. All other HTML (tags and attributes) will be removed.
        allowedTags.addTags("br", "cite", "em", "i", "p", "strong", "img", "li", "ul", "ol", "sup", "sub", "s");
        allowedTags.addAttributes("p", "style");
        allowedTags.addAttributes("img", "src", "style","class");

                //String resultado = params.html

        //String resultado = Jsoup.clean( params.html, Whitelist.simpleText() ).trim()
        //String resultado = Jsoup.clean( params.html, Whitelist.basic() ).trim()
        //String resultado = Jsoup.clean( params.html, Whitelist.relaxed() ).trim()
        String resultado = Jsoup.clean( params.html, allowedTags )
        render resultado

    }

    /**
     * método para limpar o diretorio /data/salve-estadual/temp
     * Ex: https://salve.icmbio.gov.br/salve-estadual/limparTempDir/4 para limpar os arquivos com 4 dias de vida
     * @return
     */
    def limparTempDir()
    {
        if( ! session.sicae.user.isADM() )
        {
            render '<br>Acesso restrito a administradores'
            return
        }
        if( params.dias ) {
            def dir = new File("/data/salve-estadual/temp");
            def files = [];
            dir.traverse(type: FILES, maxDepth: 0) { files.add(it) }
            files.each {
                BasicFileAttributes attr = Files.readAttributes(it.toPath(), BasicFileAttributes.class);
                Date dataCriacao = new Date(attr.creationTime().toMillis())
                Date hoje = new Date()
                render '<b>' + it.toString() + '</b>'
                render ' - idade ' + ( hoje - dataCriacao )
                if (((hoje - dataCriacao)) >= params.dias.toInteger()) {
                    if( it.delete() ) {
                        render ' - <span style="color:#f00;">excluído</span><br>';
                    }
                    else
                    {
                        render ' - erro ao excluir.'
                    }
                }
                else
                {
                    render '<br>'
                }

            }
        }
        else
        {render '<br>Exclusão de arquivos temporários<br>Informe o parametro /5 para 5 dias';
        }
    }

    private String getIp(ApplicationHttpRequest request = null ) {
        String ipAddress = ''
        if ( request ) {
            ipAddress = request.getHeader("Client-IP")
            if (!ipAddress) {
                ipAddress = request.getHeader("X-Forwarded-For")
            } else if (!ipAddress) {
                ipAddress = request.remoteAddr
            } else if (!ipAddress) {
                ipAddress = request.getRemoteAddr();
            }
        }
        if( ! ipAddress ) {
            ipAddress = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getRemoteAddr();
        }
        return (ipAddress == '0:0:0:0:0:0:0:1') ? 'localhost' : ipAddress
    }

    private void registrarLogAcesso() {
        try {
            if( ! (grailsApplication?.config?.dataSource?.url =~ /.*32\.11(0|4).*/ ) ) {
                if (session?.sicae?.user) {
                    println2 'Registrando acesso ...'
                    TrilhaAuditoria ta = new TrilhaAuditoria()
                    ta.deEnv = Environment.current.toString()
                    ta.noAcao = 'login'
                    ta.noModulo = 'main'
                    ta.deIp = getIp(request)
                    ta.dtLog = new Date()
                    ta.nuCpf = session.sicae.user.nuCpf
                    ta.noPessoa = session.sicae.user.noUsuario
                    ta.txParametros = session.sicae.user.asJson().toString()
                    ta.save(flush: true)
                    println2 'Acesso registrado com sucesso!'
                }
            }
        } catch( Exception e ) {
            log.info('Erro registrando acesso na tabela de auditoria (' + grailsApplication?.config?.dataSource?.url+')')
            log.error(e.getMessage())
        }
    }

    def cancelWorker() {

        if( !params.idTarefa ) {
            render '<h3>SALVE - Tarefas sendo executadas</h3>'
            render '<pre>'
            session.workers.each {
                render '<b>' + it.key + '</b> - <a title="Clique para cancelar a execução." href="/salve-estadual/cancelarTarefa/'+it.key+'">Cancelar</a>'
                render '<br>'
                render it.value.deRotulo
                render '<hr>';
            }
            render '</pre>'
        } else {
            session.workers.each {
                if( it.key==params.idTarefa ) {
                    workerService.setError( it.value, 'Tarefa ' + it.value.deRotulo +' foi cancelada!');
                }
            }
            redirect(uri: '/');
        }
    }

    def restoreSession(){
        String cookie = params.cookie ?: ''

        // limpar sessao do usuario
        session.sicae = [:]
        session.sicae.dateTime = new Date().getDateTimeString()
        session.sicae.user = new Sicae(cookie: params.cookie)
        session.sicae.user.sqUnidadeOrg = 0
        session.sicae.user.sgUnidadeOrg = ''
        JSONObject data =  _getSicaeSessionData( cookie )
        if( data.status == 0 ) {
            if (data.sqUnidadeOrg) {
                if (data.nuCpf) {
                    session.sicae.user.properties = data
                    session.nuCpfLogado = data.nuCpf
                }
            } else {
                if (data.sqUsuario) {
                    if (data.nuCpf) {
                        session.sicae.user.properties = data
                    }
                }
            }
            registrarLogAcesso()
        } else {
            session.sicae = null        }
        render data
    }

    // novo
    protected JSONObject _getSicaeSessionData(String cookie = '' ){
        JSONObject sessionData = JSON.parse('{}')
        String urlSessionPhp
        try {
            sessionData.status=0
            sessionData.msg=''
            urlSessionPhp = grailsApplication.config.url.sicae + '/session.php'
            urlSessionPhp = urlSessionPhp.replaceAll(/\/\/session.php/,'/session.php')
            String dadosSessaoSicae = requestService.doPost( urlSessionPhp, "", "text/plain", ['sisicmbio': cookie])
            sessionData = JSON.parse( dadosSessaoSicae )
            if( ! sessionData.sqSistema || sessionData.sqSistema != 24 ) {
                sessionData = JSON.parse('{}')
                sessionData.status=1
                sessionData.msg = 'Sessão expirada. Efetue login novamente.'
            } else {
                // se tiver a unidadeOrg é um usuário interno
                if (sessionData.sqUnidadeOrg) {
                    Pessoa p = Pessoa.get(sessionData.sqUnidadeOrg.toInteger())
                    if (!p || p.noPessoa.toUpperCase() != sessionData.noUnidadeOrg.toString().trim().toUpperCase()) {
                        p = Pessoa.findByNoPessoaIlike(sessionData.noUnidadeOrg.toString().trim())
                        if (p) {
                            sessionData.sqUnidadeOrg = p.id
                        }
                    }
                    if (sessionData.nuCpf) {
                        PessoaFisica pf = PessoaFisica.findByNuCpf(sessionData.nuCpf)
                        if (pf) {
                            sessionData.sqPessoa = pf.id
                        }
                    }
                    if (sessionData.nuCpf) {
                        sessionData.nuCpfLogado = sessionData.nuCpf
                    } else {
                        sessionData.msg = 'Login externo ainda não implementado!'
                        sessionData.status=1
                    }
                } else {
                    // usuário externo
                    if (!sessionData.sqUsuario) {
                        sessionData.msg = 'Login para usuário EXTERNO ainda não implementado!'
                        sessionData.status=1
                    } else {
                        UsuarioExterno usr = UsuarioExterno.get(sessionData.sqUsuario.toInteger())
                        Pessoa pessoa
                        if (usr) {
                            pessoa = Pessoa.get(usr.id.toInteger())
                            sessionData.noUsuario = Util.capitalize(pessoa.noPessoa)
                            sessionData.sqPessoa = pessoa.id
                            PessoaFisica pf = PessoaFisica.get(pessoa.id)
                            if (pf) {
                                sessionData.nuCpf = pf.nuCpf
                            } else {
                                sessionData.msg = 'Pessoa Física não cadastrada no banco de dados.'
                                sessionData.status=1
                            }
                        } else {
                            sessionData.msg = 'Usuário ' + sessionData.sqUsuario + ' nao cadastrado no banco de dados.'
                            sessionData.status=1
                        }
                    }

                }
            }
        } catch(Exception e ) {
            sessionData.status=1
            sessionData.error = e.getMessage()
        }
        return sessionData
    }


    /**
     * método para gravar os textos editados diretamente nos grides - inline edits
     * @return
     */
    def saveEditableLabels(){
        Map res = [status:0,msg:'',type:'success']
        try {
            if ( ! session.sicae.user ) {
                throw new Exception('Sessão expirada')
            }
            if( params.contexto == 'ficha-anexo'){
                FichaAnexo fichaAnexo  = FichaAnexo.get( params.id.toLong() )
                if( !fichaAnexo ){
                    throw new Exception('ID inválido')
                }
                fichaAnexo.deLegenda = params.newText
                fichaAnexo.save()
            }
        } catch( Exception e ){
            res.type='error'
            res.status = 1
            res.msg = e.getMessage()
        }
        render res as JSON
    }

}
