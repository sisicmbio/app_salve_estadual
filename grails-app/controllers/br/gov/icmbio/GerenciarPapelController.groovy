package br.gov.icmbio
import grails.converters.JSON

class GerenciarPapelController extends FichaBaseController {
    def dadosApoioService
    def fichaService
    def sqlService

    def index()    {
        List listCiclosAvaliacao = [ session.getAttribute('ciclo') ] //CicloAvaliacao.list(sort: 'nuAno', order: 'desc')
        List listPapeis = dadosApoioService.getTable('TB_PAPEL_FICHA').sort { it.descricao }
        List listGrupo  = dadosApoioService.getTable('TB_GRUPO').sort { it.descricao }
        List listNivelTaxonomico = NivelTaxonomico.list();
        List listSituacaoFicha = fichaService.getListSituacao()

        render(view: 'index', model: ['listCiclosAvaliacao': listCiclosAvaliacao
            , 'listPapeis': listPapeis
            , 'listNivelTaxonomico': listNivelTaxonomico
            , 'listSituacaoFicha' : listSituacaoFicha
            , 'listGrupo' : listGrupo
            ])
    }

    def getGridFichas() {
        if( !params.sqCicloAvaliacao )
        {
            render ''
            return
        }
        List listFichas = fichaService.search( params )
        render(template:"divGridFicha",model:[listFichas:listFichas])
    }

    // pessoas
    def getGrid() {
        if( params.sqCicloAvaliacao ) {
            String query = """
            select tmp.*, count(*) as nu_fichas from (
            select ficha_pessoa.sq_pessoa
            , pessoa.no_pessoa
            , ficha_pessoa.sq_papel
            , papel.ds_dados_apoio as no_papel
            , ficha.sq_unidade_org as sq_unidade_ficha
            , vw_unidade_org.sg_unidade_org as sg_unidade_ficha
            , ficha_pessoa.sq_web_instituicao as sq_unidade_org
            --, web_instituicao.sg_instituicao as sg_unidade_org
            --, coalesce( ficha_pessoa.sq_web_instituicao, ficha.sq_unidade_org ) as sq_unidade_org
            , coalesce( web_instituicao.sg_instituicao, vw_unidade_org.sg_unidade_org)  as sg_unidade_org
            , case when web_instituicao.sg_instituicao is null then 'INTERNA' else 'EXTERNA' END as in_tipo_unidade
            , ficha_pessoa.in_ativo
            , grupo.sq_dados_apoio as sq_grupo
            , grupo.ds_dados_apoio as ds_grupo
            from salve.ficha_pessoa
            inner join salve.vw_ficha as ficha on ficha.sq_ficha = ficha_pessoa.sq_ficha
            INNER JOIN salve.vw_pessoa pessoa on pessoa.sq_pessoa = ficha_pessoa.sq_pessoa
            inner join salve.dados_apoio papel on papel.sq_dados_apoio = ficha_pessoa.sq_papel
            inner join salve.vw_unidade_org on vw_unidade_org.sq_pessoa = ficha.sq_unidade_org
            left  join salve.dados_apoio grupo on grupo.sq_dados_apoio = ficha_pessoa.sq_grupo
            left outer join salve.web_instituicao on web_instituicao.sq_web_instituicao = ficha_pessoa.sq_web_instituicao
            where ficha.sq_ciclo_avaliacao = :sqCicloAvaliacao
            ${params.noCientificoFiltro ? "and ficha.nm_cientifico ilike :noCientificoFiltro": ""}
            ${params.sqPessoa ? ' AND ficha_pessoa.sq_pessoa = ' + params.sqPessoa : '' }
            ) tmp
            group by tmp.sq_pessoa, tmp.sq_unidade_ficha, tmp.in_tipo_unidade, tmp.sg_unidade_ficha
            , tmp.no_pessoa, tmp.sq_papel, tmp.no_papel, tmp.sq_unidade_org
            , tmp.sg_unidade_org,tmp.in_ativo, tmp.sq_grupo, tmp.ds_grupo
            order by tmp.no_pessoa, tmp.sg_unidade_org, tmp.no_papel, tmp.ds_grupo, tmp.sg_unidade_ficha, tmp.in_ativo
            """

            /** /
            println ' '
            println ' '
            println ' '
            println query
            println params

            /**/
            List rows = sqlService.execSql(query,[ 'sqCicloAvaliacao' : params.sqCicloAvaliacao.toLong()
                                                   , 'noCientificoFiltro' : params.noCientificoFiltro+'%' ])

            // criar a estrutura de dados para exibir na página
            List nomesFiltro = []
            List papeisFiltro = []
            List gruposFiltro = []
            List unidadesFiltro = []
            List instituicoesFiltro = []

            List dataGrid = []
            rows.each { row ->
                String nomePessoaCapitalizado = Util.capitalize( row.no_pessoa )

                Map pessoa = [ sq_ciclo_avaliacao:params.sqCicloAvaliacao
                              ,sq_pessoa: row.sq_pessoa
                              ,no_pessoa: nomePessoaCapitalizado,
                              sg_unidade_org:row.sg_unidade_org, // não utilizar SQ_UNIDADE_ORG
                              in_tipo_unidade:row.in_tipo_unidade,
                              papeis: [] ]
                pessoa.rowKey = pessoa.encodeAsMD5()

                Map papel = [sq_papel: row.sq_papel, in_ativo:row.in_ativo, no_papel: row.no_papel, nu_fichas:row.nu_fichas,
                             sq_unidade_ficha   : row.sq_unidade_ficha,
                             sq_unidade_usuario : row.sq_unidade_org,
                             sg_unidade_ficha   : row.sg_unidade_ficha,
                             sg_unidade_usuario : row.sg_unidade_org,
                             in_tipo_unidade    : row.in_tipo_unidade,
                             sq_grupo           : row.sq_grupo,
                             ds_grupo           : row.ds_grupo ]

                // localizar se a pessoa+unidade já existe
                Map pessoaFind = dataGrid.find {
                    it.sq_pessoa == row.sq_pessoa && it.sg_unidade_org == row.sg_unidade_org
                }
                if ( ! pessoaFind ) {
                    if( ! nomesFiltro.contains( nomePessoaCapitalizado ) )
                    {
                        nomesFiltro.push( nomePessoaCapitalizado )
                    }
                    if( row.sg_unidade_org ) {
                        String nome = row.sg_unidade_org
                        if( ! instituicoesFiltro.contains( nome ) ) {
                            instituicoesFiltro.push(nome)
                        }
                    }
                    dataGrid.push(pessoa)
                    pessoaFind = pessoa
                }
                Map papelExistente = pessoaFind.papeis.find {
                            return it.sq_papel == papel.sq_papel && it.sq_grupo == papel.sq_grupo && it.sq_unidade_ficha == papel.sq_unidade_ficha
                };
                if ( ! papelExistente ) {
                    pessoaFind.papeis.push(papel)
                    // lista de papeis para filtrar o gride
                    if( ! papeisFiltro.contains(papel.no_papel) )
                    {
                        papeisFiltro.push(papel.no_papel)
                    }
                    // lista de grupos taxonomicos para filtrar o gride
                    if( ! gruposFiltro.contains(papel.ds_grupo) )
                    {
                        gruposFiltro.push(papel.ds_grupo)
                    }
                    // lista de unidades organizacionais para filtrar o gride
                    if( ! unidadesFiltro.contains(papel.sg_unidade_ficha) )
                    {
                        unidadesFiltro.push(papel.sg_unidade_ficha )
                    }
                } else {
                    if( papel.in_ativo=='S' ) {
                        papelExistente.in_ativo= 'S'
                    }
                }
            }

            // println dataGrid
           render( template:'divGridPessoa', model:[sqCicloAvaliacao:params.sqCicloAvaliacao
                                                    ,dataGrid:dataGrid
                                                    ,nomesFiltro:nomesFiltro
                                                    ,papeisFiltro:papeisFiltro
                                                    ,gruposFiltro:gruposFiltro
                                                    ,unidadesFiltro:unidadesFiltro
                                                    ,instituicoesFiltro:instituicoesFiltro
           ])
        }
        else
        {
            render '<b>Nenhum registro encontrado</b>';
        }
    }

    def getFichasPessoaFuncao() {
        if( !params.sqCicloAvaliacao )
        {
            render '<b>Ciclo avaliação não informado</b>'
            return
        }
        String query="""select ficha_pessoa.sq_ficha_pessoa, ficha.nm_cientifico, ficha_pessoa.in_ativo, ficha.co_nivel_taxonomico
            from salve.ficha_pessoa
            inner join salve.vw_ficha as ficha on ficha.sq_ficha = ficha_pessoa.sq_ficha
            INNER JOIN salve.vw_pessoa pessoa on pessoa.sq_pessoa = ficha_pessoa.sq_pessoa
            inner join salve.dados_apoio papel on papel.sq_dados_apoio = ficha_pessoa.sq_papel
            inner join salve.vw_unidade_org on vw_unidade_org.sq_pessoa = ficha.sq_unidade_org
            left outer join salve.dados_apoio grupo on grupo.sq_dados_apoio = ficha_pessoa.sq_grupo
            where ficha.sq_ciclo_avaliacao = :sqCicloAvaliacao
            and ficha_pessoa.sq_papel = :sqPapel
            and ficha.sq_unidade_org = :sqUnidadeFicha"""
        if( params.inTipoUnidade == 'INTERNA') {
            query += "\nand ficha_pessoa.sq_web_instituicao is null"
        } else {
            query += "\nand ficha_pessoa.sq_web_instituicao = :sqUnidadeOrg"
        }
        if( params.sqGrupo )
        {
            query += "\nand ficha_pessoa.sq_grupo = :sqGrupo"
        } else {
            query += "\nand ficha_pessoa.sq_grupo is null"
        }

        if( params.inAtivo )
        {
            query += "\nand ficha_pessoa.in_ativo = :inAtivo"
        }
        query += "\nand ficha_pessoa.sq_pessoa = :sqPessoa order by ficha.nm_cientifico"

        /** /
        println ' '
        println ' '
        println ' '
        println query
        /**/

        List listFichas = sqlService.execSql(query,[ sqCicloAvaliacao:params.sqCicloAvaliacao.toLong()
        ,sqPapel        : params.sqPapel.toLong()
        ,sqUnidadeFicha : params.sqUnidadeFicha.toLong()
        ,sqUnidadeOrg   : params.sqUnidadeOrg ? params.sqUnidadeOrg.toLong() : 0l
        ,sqGrupo        : params.sqGrupo ? params.sqGrupo.toLong() : 0l
        ,sqPessoa       : params.sqPessoa.toLong()
        ,inAtivo        : params.inAtivo
        ])

        /*List listFichas = FichaPessoa.createCriteria().list {
            vwFicha {
                eq('sqCicloAvaliacao', params.sqCicloAvaliacao.toInteger() )
                if( params.in_tipo_unidade=='INTERNA') {
                    eq('sqUnidadeOrg', params.sqUnidadeOrg.toInteger())
                }
                if( params.sqGrupo )
                {
                    eq('sqGrupo', params.sqGrupo.toInteger())
                }
                else
                {
                    isNull( 'sqGrupo')
                }
                order('nmCientifico')
            }
            if( params.in_tipo_unidade=='EXTERNA') {
                eq('webInstituicao.id', params.sqUnidadeOrg.toInteger() )
            } else {
                isNull( 'webInstituicao')
            }
            eq('inAtivo','S' )
            eq('pessoa', Pessoa.get( params.sqPessoa.toInteger() ) )
            eq('papel', DadosApoio.get( params.sqPapel.toInteger() ) )
        }
        println listFichas.size();
        println listFichas;
         */
        sleep(500);
        render( template:'tableFichasPessoaFuncao', model:[listFichas:listFichas
                                                           ,sqCicloAvaliacao: params.sqCicloAvaliacao
                                                           ,sqPessoa        : params.sqPessoa
                                                           ,sqPapel         : params.sqPapel
                                                           ,sqGrupo         : params.sqGrupo
                                                           ,sqUnidadeFicha  : params.sqUnidadeFicha
                                                           ,sqUnidadeOrg    : params.sqUnidadeOrg
                                                           ,inTipoUnidade   : params.inTipoUnidade


        ])
    }

    def save()  {
        if( params['sqFicha[]'] && params['sqFicha[]'].getClass() == String )
        {
            params['sqFicha[]'] = [ params['sqFicha[]']  ];
        }
        if( !params['sqFicha[]'] )
        {
            return this._returnAjax(1, 'Nenhuma ficha selecionada!', "error")
        }
        if( !params.sqPessoa )
        {
            return this._returnAjax(1, 'Nenhuma pessoa selecionada!', "error")
        }
        if( !params.sqPapel )
        {
            return this._returnAjax(1, 'Nenhum papel selecionado!', "error")
        }

        FichaPessoa reg
        //VwFicha vwFicha
        DadosApoio papel = DadosApoio.get( params.sqPapel.toInteger() )
        DadosApoio grupo = null
        DadosApoio grupoGravacao = null
        // se for coordenador de taxon ou colaborador externo que ter a instituição
        if( papel.codigoSistema == 'COORDENADOR_TAXON' || papel.codigoSistema == 'COLABORADOR_EXTERNO') {
            if( ! params.sqWebInstituicao ) {
                return this._returnAjax(1, 'Instituição do CT não informada!', "error")
            }
        } else {
            // ponto focal tem que ser um usuario interno então não tem web_instituicao
            params.sqWebInstituicao=null
        }
        // Coodenador de taxon não tem grupo, pegar os grupos das fichas atribuidas a ele
        if( papel.codigoSistema == 'COORDENADOR_TAXON' ) {
            params.sqGrupo=null
        }

        // ler o grupo
        if ( params.sqGrupo ) {
            grupo  = DadosApoio.get( params.sqGrupo.toInteger() )
        } else {
            if( papel.codigoSistema != 'COORDENADOR_TAXON' ) {
                return this._returnAjax(1, 'Grupo não informado!', "error")
            }
        }
        Pessoa pessoa = Pessoa.get( params.sqPessoa.toLong() )
        WebInstituicao webInstituicao
        if( params.sqWebInstituicao ) {
            webInstituicao = WebInstituicao.get( params.sqWebInstituicao.toInteger() )
        }
        List errors=[]

        VwFicha.createCriteria().list {
            'in'('id',params['sqFicha[]'].collect{it.toLong()})
        }.each { vwFicha ->

            // se for informado o grupo, a ficha tem que ser do mesmo grupo
            if ( grupo && vwFicha.sqGrupo.toLong() != grupo.id.toLong() ) {
                errors.push('<b>'+vwFicha.nmCientifico + '</b> <span class="red">NÃO ESTÁ</span> no grupo <b>' + grupo.descricao + '</b>' )
            } else {

                if ( grupo ) {
                    grupoGravacao = grupo
                } else {
                    if ( vwFicha.sqGrupo ) {
                        grupoGravacao = DadosApoio.get(vwFicha.sqGrupo.toLong())
                    }
                }
                // para atribui a ficha a alguem tem que ter o grupo
                if( ! grupoGravacao ) {
                    errors.push('<b>' + Util.ncItalico(vwFicha.nmCientifico, true) + '</b> está <span class="red">SEM GRUPO</span>')
                } else {
                    //d('  - ' + vwFicha.nmCientifico)
                    //d('  - ' + pessoa.noPessoa)
                    //d('  - ' + papel.codigoSistema)

                    // só pode haver 1 ponto focal ativo por ficha
                    reg=null
                    if( papel.codigoSistema=='PONTO_FOCAL') {
                        reg = FichaPessoa.findByVwFichaAndPessoaNotEqualAndPapelAndInAtivo(vwFicha, pessoa, papel,'S')
                    }
                    if ( reg ) {
                        //d('  - ' + reg.pessoa.noPessoa + ' ja e ponto focal da especie')
                        //errors.push('Ficha ' + Util.ncItalico(vwFicha.nmCientifico, true) + ' já possui PF ATIVO! (' + reg.pessoa.noPessoa + ')')
                        errors.push( '<b>' + Util.ncItalico(vwFicha.nmCientifico, true) + '</b><span class="red">JÁ POSSUI PF ATIVO</span> <b>' + reg.pessoa.noPessoa + '</b>');

                    } else {
                        //d('  - Atribuindo papel')
                        // verificar se a pessoa já possui registro
                        reg = FichaPessoa.createCriteria().get {
                            eq('vwFicha', vwFicha)
                            eq('pessoa', pessoa)
                            eq('papel', papel)
                            eq('grupo',grupoGravacao)
                        }
                        // novo registro
                        if ( ! reg ) {
                            //d('  - Novo papel')
                            reg = new FichaPessoa()
                            reg.ficha = vwFicha.ficha
                            reg.pessoa = pessoa
                            reg.papel = papel
                            reg.inAtivo = 'S'
                            reg.grupo = grupoGravacao
                        } else {
                            //d('  - Pessoa já possui o papel sqFichaPessoa:' +reg.id)
                            reg.inAtivo = 'S'
                        }
                        if ( webInstituicao ) {
                            //d('  - Gravando a instituicao')
                            reg.webInstituicao = webInstituicao
                        } else {
                            reg.webInstituicao = null
                        }
                        reg.save()
                    }
                }
            }
        }


        /*if( params['sqFicha[]'].size() == 1 && errors)
        {
            return this._returnAjax(1, errors.join('<br>'), "error")
        }*/
        if( errors ) {
            errors.push('<hr>') // para que a mensagen sejam exibidas em janela pop-up
            return this._returnAjax(0, 'Operação realizada com as seguintes inconsistências:'+( errors?'<br><br>'+errors.join('<br>'):''), "info")
        }
        return this._returnAjax(0, getMsg(1)+( errors?'<br><br>Inconsistências:<br>'+errors.join('<br>'):''), "success")
    }

    def deleteAllFichas() {

        Map pars = [ sqPessoa:params.sqPessoa.toLong()
                     ,sqPapel            :params.sqPapel.toLong()
                     ,sqUnidadeFicha     :params.sqUnidadeFicha.toLong()
                     ,sqCicloAvaliacao   :params.sqCicloAvaliacao.toLong()
        ]

        String query = """delete from salve.ficha_pessoa where sq_ficha_pessoa in (
            select ficha_pessoa.sq_ficha_pessoa from salve.ficha_pessoa
                inner join salve.ficha on ficha.sq_ficha = ficha_pessoa.sq_ficha
                where ficha_pessoa.sq_pessoa = :sqPessoa
                and ficha_pessoa.sq_papel = :sqPapel
                and ficha.sq_unidade_org = :sqUnidadeFicha
                and ficha.sq_ciclo_avaliacao = :sqCicloAvaliacao"""
                if( params.sqGrupo ) {
                    query +="""\nand ficha_pessoa.sq_grupo = :sqGrupo"""
                    pars.sqGrupo = params.sqGrupo.toLong()
                } else {
                    query +="""\nand ficha_pessoa.sq_grupo is null"""
                }

        if (params.inTipoUnidade == 'INTERNA') {
            query += "\nand ficha_pessoa.sq_web_instituicao is null )"
        } else {
            query += "\nand ficha_pessoa.sq_web_instituicao = :sqUnidadeOrg)"
            pars.sqUnidadeOrg = params.sqUnidadeOrg.toLong()
        }

        sqlService.execSql(query,pars)
        return this._returnAjax(0, getMsg(4), "success")
    }

    def delete() {
        if( params.id )
        {
            FichaPessoa.get( params.id ).delete(flush:true);
            return this._returnAjax(0, getMsg(4), "success")
        }
        else {
            return this._returnAjax(1, 'Id não informado', "error")
        }
    }

    def setSituacao() {
        List mensagens = []

        if (!params.id && !params.sqPessoa)
        {
            return this._returnAjax(1, 'Id não informado!', "success")
        }
        if( !params.sn )
        {
            return this._returnAjax(1, 'Valor S/N não informado!', "success")
        }

        if( !params.sqUnidadeFicha && !params.sqUnidadeOrg )
        {
            return this._returnAjax(1, 'Unidade não informada!', "success")
        }
        //return this._returnAjax(1, 'Ver parâmetros', "error")
        if( params.id ) {

            FichaPessoa reg = FichaPessoa.get(params.id);
            if (reg) {
                if (params.sn.toUpperCase() == 'S') {
                    if (reg.papel.codigoSistema == 'PONTO_FOCAL'
                            && FichaPessoa.countByFichaAndPapelAndInAtivo(reg.ficha, reg.papel, 'S') > 0) {
                        return this._returnAjax(1, 'Ficha ' + Util.ncItalico(reg.ficha.taxon.noCientifico,true) + ' já possui PF ATIVO.', "error")
                    }
                }
                reg.inAtivo = params.sn.toUpperCase();
                reg.save(flush: true)
            }
        }
        else if( params.sqPessoa && params.sqCicloAvaliacao && params.sqPapel )
        {
            DadosApoio papel = DadosApoio.get( params.sqPapel.toInteger() )
            FichaPessoa.createCriteria().list{
                vwFicha {
                    eq('sqCicloAvaliacao',params.sqCicloAvaliacao.toInteger())
                    eq('sqUnidadeOrg', params.sqUnidadeFicha.toInteger() );
                }
                eq('pessoa',Pessoa.get( params.sqPessoa.toLong() ) )
                eq('papel', papel)
                if( params.sqUnidadeOrg ) {
                    println ' '
                    println 'FILTRA PELA INSTITUICAO ' + params.sqUnidadeOrg
                    webInstituicao {
                        eq('id', params.sqUnidadeOrg.toLong() )
                    }
                }
                if( params.sqGrupo ) {
                    eq('grupo',DadosApoio.get(params.sqGrupo.toLong() ) )
                } else {
                    isNull('grupo')
                }
            }.eachWithIndex{ reg,i ->
                boolean valido = true
                if ( params.sn.toUpperCase() == 'S' ) {
                    if ( papel.codigoSistema == 'PONTO_FOCAL' ) {
                        FichaPessoa fichaPessoaTemp = FichaPessoa.findByVwFichaAndPapelAndPessoaNotEqualAndInAtivo( reg.vwFicha, reg.papel, reg.pessoa, 'S')
                        if( fichaPessoaTemp ) {
                            mensagens.push( '<span class="font-size:12px;">- ' + reg.vwFicha.nmCientifico + ' <span class="red">JÁ POSSUI PF ATIVO</span> <b>' + fichaPessoaTemp.pessoa.noPessoa + '</b>.</span>');
                            valido = false
                        }
                    }
                }


                if( valido && params.sqUnidadeOrg && reg.webInstituicao && reg.webInstituicao.id.toInteger() == params.sqUnidadeOrg.toInteger() )
                {
                    reg.inAtivo = params.sn.toUpperCase();
                    reg.save()
                }
                else if( valido && params.sqUnidadeFicha && reg.vwFicha.sqUnidadeOrg.toInteger() == params.sqUnidadeFicha.toInteger() )
                {
                    reg.inAtivo = params.sn.toUpperCase();
                    reg.save()
                }
            }
        }
        return this._returnAjax(0, getMsg(1)+(mensagens.size()>0?'<br><br><span class="red bold blink-me">Ficha(s) que NÃO foram alteradas(s)</span><hr>'+mensagens.join('<br>'):''), (mensagens.size()>0 ? "info":"success"))
    }

    def select2PessoaFisica() {
        DadosApoio papel
        if( params.sqPapel )
        {
            papel = DadosApoio.get( params.sqPapel.toLong() )
        }
        FichaController fc = new FichaController()
        if( papel && papel.codigoSistema == 'PONTO_FOCAL')
        {
            return fc.select2UsuarioInterno()
        }
        else {
            // se for ADM dar acesso a base corporativa de pessoas fisicas
            // senão pesquisar os coordenadores de taxon já vinculados nas fichas da unidade do usuário logado
            if( session.sicae.user.isADM() ) {
                return fc.select2PessoaFisica()
            } else {
                Map data = ['items': []]
                List whereSql = []
                Map sqlPars = [sqUnidadeOrg:session.sicae.user.sqUnidadeOrg.toLong()]
                if( params.q ){
                    whereSql.push("public.fn_remove_acentuacao(pf.no_pessoa) ilike :noPessoa")
                    String q = params.q.trim().replaceAll(/[=\*\.<>]/, '')
                    sqlPars.noPessoa='%' + Util.removeAccents(q)+'%'
                }
                String cmdSql = """select distinct pf.sq_pessoa
                                 , pf.no_pessoa
                                 , pf.nu_cpf
                            from salve.oficina_ficha
                                     inner join salve.ficha_pessoa on ficha_pessoa.sq_ficha = oficina_ficha.sq_ficha
                                     inner join salve.dados_apoio papel on papel.sq_dados_apoio = ficha_pessoa.sq_papel
                                     inner join salve.dados_apoio grupo on grupo.sq_dados_apoio = ficha_pessoa.sq_grupo
                                     inner join salve.vw_pessoa_fisica pf on pf.sq_pessoa = ficha_pessoa.sq_pessoa
                                     inner join salve.ficha on ficha.sq_ficha = ficha_pessoa.sq_ficha
                            where ficha.sq_unidade_org = :sqUnidadeOrg
                              and papel.cd_sistema = 'COORDENADOR_TAXON'
                              and ficha_pessoa.in_ativo = 'S'
                              ${ whereSql ? '\nand '+ whereSql.join('\n and ') : ''}
                            order by pf.no_pessoa
                            """
                        /** /
                        println ' '
                        println cmdSql
                        println sqlPars
                        */


                List lista = sqlService.execSql( cmdSql, sqlPars, null, 5)
                if (lista) {
                    lista.each { it ->
                        data.items.push([ 'id'       : it.sq_pessoa,
                                          'descricao': Util.capitalize(it.no_pessoa) +' ('+ Util.formatCpf(it.nu_cpf)+')'
                        ])
                    }
                    render data as JSON
                    return
                }

            }


            return null
        }
    }
}
