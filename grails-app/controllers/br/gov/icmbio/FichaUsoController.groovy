/**
 * Este controlador é resposável por receber as ações referentes ao cadastro aba Usos da ficha de espécies
 * Regras:
 *
 *
 *
 *
 *
 */

package br.gov.icmbio
import grails.converters.JSON;

class FichaUsoController extends FichaBaseController {

    def dadosApoioService
    def fichaService

	def index() {

		if( !params.sqFicha )
		{
			render 'ID da ficha não informado.'
			return;
		}
		Ficha ficha = Ficha.get(params.sqFicha)
        List listUsos = Uso.list().sort{it.codigo}
		render(template:"/ficha/uso/index",model:['ficha':ficha,listUsos:listUsos])
	}

    def getTreeRecursiveUsos( Uso uso, Lista listExistentes )
    {
        boolean existe = listExistentes.list.find{ FichaUso fichaUso -> fichaUso.uso.id == uso.id };
        Map node = [id:uso.id,title:uso.descricaoCompleta,icon:false,folder:false,selected:existe];
        if( uso.itens.size() > 0)
        {
            node.children = []
            uso.itens.sort{it.ordem}.each {
                node.children.push( getTreeRecursiveUsos(it,listExistentes) )
            }
        }
        return node;
    }


    def getTreeUsos()
    {

        List data =[]

        if( !params.sqFicha )
        {
            render data as JSON
            return;
        }
        // selecionar os usos já cadatrados para marcar os checks
        List listUsos = Uso.findAllByPaiIsNull()
        Lista existentes = new Lista(FichaUso.findAllByFicha( Ficha.get(params.sqFicha) ) )
        listUsos.each {
            data.push( getTreeRecursiveUsos( it , existentes ) )
        }
        render data as JSON
    }

    def saveTreeUso() {
        Map data = [limparDescricao: false]
        if (!params.sqFicha) {
            return this._returnAjax(1, 'ID da ficha não informado.', "error")
        }

        Ficha ficha = Ficha.get(params.sqFicha.toLong())
        if (!ficha) {
            return this._returnAjax(1, 'ID da ficha inválido.', "error", data)
        }
        Boolean updated = false

        List idsSelecionados = []
        List idsValidos = []
        if (params.ids) {
            idsSelecionados = params.ids.split(',');
        }
        // criar a lista das usos ordenada pelo codigo na ordem descendente para não incluir um codigo pai se o filho ja existir
        String ultimoCodigoProcessado = ''
        if (idsSelecionados) {
            Uso.createCriteria().list {
                'in'('id', idsSelecionados.collect { it.toLong() })
                order('ordem', 'desc')
            }.each {
                if (!ultimoCodigoProcessado || ultimoCodigoProcessado.indexOf(it.codigo + '.') != 0) {
                    ultimoCodigoProcessado = it.codigo
                    Integer index = idsSelecionados.findIndexOf { item -> item.toLong() == it.id.toLong() }
                    idsValidos.push(idsSelecionados[index])
                }
            }
        }
        //return this._returnAjax(0, getMsg(1), 'success', data)

        /** novo*/
        // 1) excluir os usos existentes e que não foram mantidas
        // 2) atualizar/adicionar os novos usos
        if ( idsValidos ) {
            FichaUso.executeUpdate("delete FichaUso fu where fu.ficha.id = :sqFicha and fu.uso.id NOT in ( ${idsValidos.join(',')} )", [sqFicha: ficha.id])
            idsValidos.each {
                Uso uso = Uso.get(it.toLong())
                if (uso) {
                    FichaUso fu = FichaUso.findByFichaAndUso(ficha, uso)
                    if (fu) {
                        fu.save(flush: true)
                        updated = true
                    } else {
                        // verificar se ja tem codigo filho cadastrado, se tiver não permitir cadastrar o pai
                        List filho = FichaUso.createCriteria().list {
                            createAlias('uso', 'u')
                            eq('ficha', ficha)
                            like('u.codigo', uso.codigo + '.%')
                            maxResults(1)
                        }
                        if ( ! filho ) {
                            fu = new FichaUso(ficha: ficha, uso: uso)
                            fu.save(flush: true)
                            updated = true
                        }
                    }
                }
            }
        } else {
            // nenhum uso selecionado
            FichaUso.executeUpdate("delete FichaUso fu where fu.ficha.id = :sqFicha", [sqFicha: ficha.id])
            if (Util.stripTags(ficha.dsUso) == "Não foram encontradas informações para o táxon.") {
                data.limparDescricao = true
                ficha.dsUso = null
                ficha.save()
                updated = true
            }
        }
        if( updated ) {
            ficha.dtAlteracao = new Date()
            ficha.save()
        }
        /** fim novo*/

        /*
        Ficha ficha = Ficha.get( params.sqFicha.toLong() )
        FichaUso.findAllByFicha(ficha).each { fichaUso ->
            if (!idsValidos.find { it.toInteger() == fichaUso.uso.id.toInteger() })
            {
                fichaUso.delete(flush: true);
            } else
            {
                idsValidos.removeAll{ it.toInteger()== fichaUso.uso.id.toInteger() }
            }
        }
        // gravar os novos ids
        idsValidos.each {
            new FichaUso(ficha: ficha, uso: Uso.get(it.toInteger())).save(flush: true)
        }
        if( FichaUso.countByFicha( ficha ) > 0  )
        {
            if(  Util.stripTags(ficha.dsUso) == "Não foram encontradas informações para o taxon.")
            {
                data.limparDescricao=true
                ficha.dsUso = null
                ficha.save()
            }
        }
        // atualizar log da ficha, quem alterou
        ficha.dtAlteracao = new Date()
        ficha.save()*/

        return this._returnAjax(0, getMsg(1), 'success');
    }

def saveFrmUso()
    {
		if( !params.sqFicha )
		{
			return this._returnAjax(1, 'ID da ficha não informado.', "error")
		}
		if( !params.sqUso)
		{
			return this._returnAjax(1, 'Selecione um uso!', "error")
		}

		FichaUso reg

		if( params.sqFichaUso )
		{
			reg = FichaUso.get( params.sqFichaUso)
		}
		else
		{
			reg = new FichaUso()
			reg.ficha = Ficha.get( params.sqFicha )
		}
		reg.properties = params
		reg.uso = Uso.get(params.sqUso)
        try {
            reg.save(flush:true)
            this._returnAjax(0, getMsg(1), "success")
        } catch(Exception e) {
            println e.getMessage()
            this._returnAjax(1, getMsg(2), "error")
        }
    }

    def getGridUso()
    {

		if( !params.sqFicha)
		{
			render ''
			return
		}
		List listFichaUso = FichaUso.findAllByFicha(Ficha.get(params.sqFicha),[sort:'uso.codigo'],[sort:'uso.descricao'] )
		render( template:'/ficha/uso/divGridUso',model:['listFichaUso':listFichaUso])

    }

    def getGridAnexo()
    {
        if( !params.sqFicha )
        {
            render  'ID da ficha não informado.';
            return;
        }
        DadosApoio contexto = dadosApoioService.getByCodigo('TB_CONTEXTO_ANEXO','MAPA_USO');
        List dados = FichaAnexo.findAllByFichaAndContexto(Ficha.get( params.sqFicha),contexto );
        render( template:'/ficha/uso/divGridAnexos',model:[listFichaAnexo:dados]);
    }

    def editUso()
    {
        FichaUso reg = FichaUso.get(params.id);
        if( reg?.id )
        {
            render reg.asJson();
            return;
        }
        render [:] as JSON;
    }


    def deleteUso()
    {
    	FichaUso reg = FichaUso.get(params.id);
		if( reg )
		{
			reg.delete(flush:true);
			return this._returnAjax(0, getMsg(4), "success")
		}
		else
		{
			return this._returnAjax(1, getMsg(51), "error")
		}
		render '';
    }

    def saveFrmUsoFicha()
    {
		if( !params.sqFicha )
		{
			return this._returnAjax(1, 'ID da ficha não informado.', "error")
		}
		Ficha ficha = Ficha.get( params.sqFicha )
		ficha.dsUso = params.dsUso
        try {
            ficha.save(flush:true)
            fichaService.updatePercentual( ficha )
            //fichaService.atualizarPercentualPreenchimento( ficha )
            this._returnAjax(0, getMsg(1), "success")
        } catch(Exception e) {
            println e.getMessage()
            this._returnAjax(1, getMsg(2), "error")
        }
    }

    def getGridUsoRegiao()
    {
        if( !params.sqFichaUso)
        {
            render 'sqFichaUso não informado'
            return
        }
        List listFichaUsoRegiao = FichaUsoRegiao.findAllByFichaUso(FichaUso.get(params.sqFichaUso ) )
        render( template:'/ficha/uso/divGridUsoRegiao',model:['listFichaUsoRegiao':listFichaUsoRegiao])
    }
}
