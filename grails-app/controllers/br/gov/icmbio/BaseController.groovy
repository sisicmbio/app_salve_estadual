package br.gov.icmbio

import grails.converters.JSON
import org.codehaus.groovy.grails.web.json.JSONObject
import grails.util.Environment
import org.springframework.web.context.request.ServletRequestAttributes
import org.springframework.web.context.request.RequestContextHolder
import org.codehaus.groovy.grails.web.servlet.mvc.GrailsParameterMap
import org.springframework.web.multipart.commons.CommonsMultipartFile
class BaseController {

    // informações da requisição preenchias a cada request pelo metodo auth()
    protected Map requestInfo = [url: '', protocol: '', baseUrl: '', port: '', ip: '', remoteHost: '', controller: '', action: '']

    def grailsApplication
    def messageSource
    def requestService

    String action
    String controller
    Locale locale = Locale.getDefault()
    // Locale locale = new Locale("pt", "BR");
    Boolean isAjax
    String baseUrl
    String urlSicae
    String urlSicaeExterno

    def index() {}

    /**
    *   Definir método que irá iterceptar todas as ações submetidas aos controllers
    *   def beforeInterceptor = [action:this.&auth,except:['/controller/action','/controller/action'] ]
    */
    def beforeInterceptor = [action:this.&auth, except: 'login']

    Boolean auth() {
        /*
        def ANSI_BLUE = "\u001B[0;34m"
        def ANSI_WHITE = "\u001B[0;37m"
        def ANSI_YELLOW = "\033[1;33m" // bold
        def ANSI_GREEN = "\033[0;32m"
        def ANSI_GREEN_BRIGHT = "\033[0;92m"
        */
        def ANSI_BLUE = ""
        def ANSI_WHITE = ""
        def ANSI_YELLOW = "" // bold
        def ANSI_GREEN = ""
        def ANSI_GREEN_BRIGHT = ""

        try {

            this.action = '';
            this.controller = '';
            this.isAjax = params?.ajax  || request?.xhr

            // remover os espaços em brando do início e fim dos parâmetros recebidos
            trimParams()

            // alimentar requestInfo
            requestInfo.dateTime = new Date()
            requestInfo.controller = params?.controller ?: ''
            requestInfo.action = params?.action ?: ''
            if( request ) {
                this.isAjax = params?.ajax || request?.xhr
                this.baseUrl = Util.getBaseUrl( request.getRequestURL().toString() )
                try {
                    URL url = new URL(request.requestURL ? request.requestURL.toString() : '')
                    requestInfo.url = url.toString().replaceAll(/\.dispatch/,'')
                    requestInfo.port = url.getPort() ? url.getPort().toString() : url.getDefaultPort().toString()
                    requestInfo.protocol = url.getProtocol() ? url.getProtocol().toString() : 'https'
                    requestInfo.path = (url.getPath() ? url.getPath().toString() : '/').replaceAll(/\.dispatch/,'')
                    requestInfo.remoteHost = url.getHost() ? url.getHost().toString() : ''
                    requestInfo.remoteIp = request.getRemoteAddr() ? request.getRemoteAddr().toString() : request.getRemoteHost().toString()
                    requestInfo.remoteIp2 = getRequestIp()
                } catch (Exception e ) {}
            }

            // ler a url

            this.urlSicae = grailsApplication?.config?.url?.sicae ?: 'https://sicae.sisicmbio.icmbio.gov.br'
            this.urlSicaeExterno = grailsApplication?.config?.url?.sicae ? grailsApplication?.config?.url?.sicae+'usuario-externo/login' : 'https://sicae.sisicmbio.icmbio.gov.br/usuario-externo/login'


            // modo consulta não pode alterar
            if( session?.sicae?.user && session?.sicae?.user?.isCO() ) {
                if (params.action && params.action ==~ /^save.*/) {
                    if (this.isAjax) {
                        this._returnAjax(1, 'Operação não permitida no modo consulta.', 'danger')
                    }
                    return false
                }
            }

            // formatar os parametros
            String paramValues  = ''
            if( request && params && Environment.current.toString() == 'DEVELOPMENT') {
                params.each {
                    if (it?.key == 'controller') {
                        this.controller = it?.value
                    } else if (it?.key == 'action') {
                        this.action = it?.value
                    } else {
                        if (it.value && (it.value.getClass() == CommonsMultipartFile)) {
                            try {
                                CommonsMultipartFile f = request.getFile(it.key)
                                if (f && !f.empty) {
                                    paramValues += '   ' + it?.key + ' = ' + ANSI_YELLOW + f.getOriginalFilename() + ANSI_WHITE + '\n'
                                    paramValues += '   - Size: ' + f.getSize() + '\n'
                                    paramValues += '   - Type: ' + f.getContentType() + '\n'
                                }
                            } catch (Exception e) {
                            }
                        } else {
                            paramValues += '   ' + it?.key + ' = ' + ANSI_YELLOW + it?.value + ANSI_WHITE + '\n'
                        }
                    }
                }
                paramValues = '\na) Arquivo fonte: ' + ANSI_GREEN + this.controller + 'Controller.groovy' + ANSI_WHITE +

                        //'\nb) Controller = '+  ANSI_GREEN_BRIGHT + this.getClass().toString().replace('class br.gov.icmbio.','')+ ANSI_WHITE
                        '\nb) Metodo (action) = ' + ANSI_GREEN + this.action + '()' + ANSI_WHITE +
                        '\nc) Parametros Recebidos:\n' + paramValues

                // Util.d('\n'+('*'*80)+"\n## Interceptor - " + Environment.current.toString()+" ##${paramValues}"+('*'*80));
                if (params?.noLog == true || !params?.noLog) {
                    String msg = '\n' + ('*' * 84) + "\n## Request: " + new Date().getDateTimeString() +
                            " ip:" + this.getRequestIp() + ' ' + Environment.current.toString() +
                            " User: " + (session?.sicae?.user ? session?.sicae?.user?.noUsuario : 'Nao logado') +
                            " ##${paramValues}" + ('*' * 84)

                    if( this.action != 'getGridChats') {
                        log.info(msg)
                    }
                    //log.info( msg.replaceAll("[^\\x00-\\x7F]", "") )
                }
            }

            // criar o parametro canModify se não for passado
            params.canModify = ( params?.canModify?.toString() == 'false'  ? false : true)

            // não executar a ação se não estiver logado no sicae
            if( ! session?.sicae?.user )
            {
               if( this.isAjax )
               {
                   return this._returnAjax(1,'Necessário efetuar login novamente','danger'
                       ,[restoreSession:true])
               }
               redirect(controller:"login", action: 'index')
               return false
            }
        }
        catch( Exception e ) {
            log.info( 'Problema ocorrido no metodo baseController/auth em ' + new Date() )
            log.info( e.getMessage() )
            log.info( ' ')
            return true
        }
        return true
    }

// request.
    /**
     * metodo para criar log de depuracao
     * @param text
     */
    private void d( String text ){
        if( Environment.current.toString() == 'DEVELOPMENT' ) {
            if( text.trim() != '' ) {
                String h = new Date().format('HH:mm:ss')
                println h + ':' + text
            } else {
                println ' '
            }
        }
    }

    /*
    Método para remover os espaços do início e fim dos parâmetros postados
    */
    private Map trimParams() {
        if( params )
        {
            params?.each
            {
                try {
                    if( it?.getClass() == String )
                    {
                        it = it?.toString()?.trim()
                        it = it.replaceAll(/<p>&nbsp;<\/p>$/,'')
                    }
                    else if( it?.value?.getClass() == String )
                    {
                        it.value = it.value.toString().trim()
                        it.value = it.value.replaceAll(/<p>&nbsp;<\/p>$/,'')

                    }
                } catch(Exception e) {}
            }
        }
    }

    /*
    Método pra =  (P)repare (R)eturn (A)jax
    */
    private JSONObject pra(String msg,Map data,Integer status)
    {
        Map res = [message:msg,data:data,status:status]
        render res as JSON
    }

        // verificar se a sessão do sicae ainda está ativa
    Boolean verifySessionSicae()
    {

        if ( session?.sicae?.user )
        {
            return true;
        }
        return false;



        Map sicae = session?.sicae?.user;
        String cookie;

        if( ! sicae ) // não está logado
        {
            return false;
        }

        if( !sicae?.dateTime )
        {
            return false;
        }

        if( !sicae?.cookie )
        {
            return false;
        }

        // tempo para validar o cookie no SICAE é 5 em 5 minutos
        if( sicae?.noUsuario && Util.dateDiff( sicae.dateTime, new Date(), 'M' ) < 5 )
        {
            //return true;
        }

        if( ! grailsApplication.config.url.sicae )
        {
            return false;
        }
        cookie = sicae.cookie; // guardar o cookie da sessão do sicae e limpar os dados atuais
        def dadosLoginSicae = requestService.doPost( grailsApplication.config.url.sicae + '/session.php',"","",['sisicmbio':cookie] );
        if( dadosLoginSicae )
        {
            dadosLoginSicae = dadosLoginSicae.replaceAll(/null/,'""');
            try {
                JSON.parse(dadosLoginSicae).each {
                    if( it.value )
                    {
                        //println it.key.toString().trim()+'='+it.value.toString().trim();
                        sicae[it.key.toString().trim()]=it.value.toString().trim();
                    }
                }
                // reiniciar tempo da sessão
                session.sicae.user.dateTime = new Date().getDateTimeString();
                return true;
            }
            catch(Exception e) {
                Util.d( 'Nao foi possivel tratar os Dados retornados pelo sicae.');
                Util.d( 'Tipo da variavel: ' + dadosLoginSicae.getClass())
                Util.d( dadosLoginSicae);
                Util.d( 'Erro: ' + e.getMessage() );
                return false;
            }
        }
        return false;
    }

     /**
     * [_returnAjax description]
     * @param  0   [description]
     * @param  ''  [description]
     * @param  ''  [description]
     * @param  [:] [description]
     * @return     [description]
     */
    private _returnAjax(Integer status = 0, String msg = '', String type = '', Map data = [:])
    {

        Map mapReturn = [
                            'status': status,
                            'msg'   : msg,
                            'type'  : type ?: 'info',
                            'data'  : data
                        ]

        render mapReturn as JSON
    }

    private Map _pagination(Map data) {
        List pages = []
        int i, count = 0
        int pageSize = 10
        if( data.pageSize )
        {
            pageSize = data.pageSize;
        }
        Map pagination = ['pageCount': 0, 'pageNumber': []]
        if( data && data.totalRecords > 1 ) {
            pagination.pageCount = Math.round(Math.ceil(data.totalRecords / pageSize))
            for( i = data.pageCurrent - 5; i <= pagination.pageCount; i++ ) {
                if( i > 0 ) {
                    count++
                    if( i <= pagination.pageCount ) {
                        pages.push( i )
                    } else {
                        if( (pages.min() - 1) > 0 ) {
                            pages.push( pages.min() -1 )
                        }
                    }
                    if( count >= 10 ) {
                        break
                    }
                }
            }
            pagination.pageNumber = pages.sort()
            data.pageNumber = pagination.pageNumber
            data.pageCount = pagination.pageCount
        } else {
            data.pagination = [:]
        }
        return data
    }
    /**
     * Ler a mensagem do arquivo message_pt_BR.properties informando somente o número
     * @example getMsg(10)
     * @param  num [description]
     * @return     [description]
     */
    String getMsg(num) {
        //Locale locale = Locale.getDefault()
        //Locale locale = new Locale("pt", "BR");

        String msg  = 'msg' +num.toString().replaceAll('[^0-9]','').padLeft(3,'0')
        try{
            return messageSource.getMessage( msg,null,locale)
            //grailsApplication.mainContext.getMessage(msg, locale ,null);
         } catch( Exception e) {
            return 'Comando executado com SUCESSO!'
         }

         // não funcionou com messageSource.getMessage()
         //return messageSource.getMessage(msg, 'Mensagem ' + msg + ' não definida', null)
    }

    /*
    Identificar o endereço IP do computador que está acessando o sistema
     */
    String getRequestIp() {
        String ipAddress='';
        if( request ) {
            ipAddress = ( (ServletRequestAttributes)RequestContextHolder.currentRequestAttributes()).getRequest().getRemoteAddr();
            ipAddress = ipAddress == '0:0:0:0:0:0:0:1' ? '127.0.0.1' : ipAddress
            ipAddress = ipAddress == 'localhost' ? '127.0.0.1' : ipAddress

            /*
            ipAddress = request.getHeader("Client-IP")
            if (!ipAddress)
            {
                ipAddress = request.getHeader("X-Forwarded-For")
            }
            else if (!ipAddress)
            {
                ipAddress = request.remoteAddr
            }
            else if (!ipAddress)
            {
                ipAddress = request.getRemoteAddr();
            }*/
        }
        return ipAddress
    }

    /**
     * Somente as informações que estiverem no último ciclo poderão ser alteradas
     * @param sqCicloAvaliacao
     * @return
     */
    boolean isLastCicle( Integer sqCicloAvaliacao )
    {
        return  CicloAvaliacao.get(sqCicloAvaliacao)?.isLastCicle()
    }


    protected boolean hasFilterParams(  GrailsParameterMap params )
    {
        boolean result = false
        params.each {
            if( it.key.indexOf('Filtro') > 0 && it.value && ! result )
            {
                result = true
            }
        }
        return result
    }

    protected void registrarTrilhaAuditoria( String acao='', String modulo='', Map data = [:] ) {

        if ( session?.sicae?.user) {
            d('Registrando Trilha auditoria ')
            d('  - modulo.: ' + modulo)
            d('  - acao...: '+acao)
            d('  - usuario: '+ session?.sicae?.user.noPessoa )
            TrilhaAuditoria ta = new TrilhaAuditoria()
            ta.deEnv = Environment.current.toString()
            ta.noAcao = acao ?:null
            ta.noModulo = modulo ?:null
            try {
                ta.deIp = this.getRequestIp()
            } catch( Exception  e) { }
            ta.dtLog = new Date()
            ta.nuCpf = session.sicae.user.nuCpf
            ta.noPessoa = session.sicae.user.noUsuario
            ta.txParametros = (data as JSON).toString().trim()

            d('Trilha registrada com SUCESSO!')
        }
    }

}
