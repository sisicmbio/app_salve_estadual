/**
 * Este controlador é responsável por receber as ações referentes ao cadastro da ficha de especies da aba taxonomia.
 * Regras:
 *
 * 1) para cadastrar uma ficha é necessario selecionar um ciclo de avaliação com status aberto
 * 2) para cadastrar uma ficha é necessário selecionar uma especie ou subespecie
 * 3) para cadastrar uma ficha é necessário verificar se já existe a mesma espécie em outra unidade organizacional, se existir não permitir adicionar
 *
 */
package br.gov.icmbio
import grails.converters.JSON;

class FichaTaxonomiaController extends FichaBaseController {

	def dadosApoioService
	def dataSource
	def fichaService
    def emailService

	def save() {

		DadosApoio papel
		Pessoa pontoFocal

		if (!session?.sicae?.user?.sqPessoa) {
			return this._returnAjax(1, grailsApplication.config.msg050, "error")
		}

		Ficha ficha
		List errors = []
		Map res = [:]
        boolean enviarEmail = false

		if (params['sqFicha']) {
			// alteracao
			ficha = Ficha.get(params['sqFicha'])
			ficha.properties = params

            // grupo recorte modulo publico somente os adm podem cadastrar ou retirar
            if( session.sicae.user.isADM() ) {
                if (params.sqGrupoSalve) {
                    ficha.grupoSalve = DadosApoio.get(params.sqGrupoSalve.toLong())
                } else {
                    ficha.grupoSalve = null;
                }
            }

			if (params.sqGrupo ) {
				ficha.grupo = DadosApoio.get( params.sqGrupo.toLong() )
			} else {
				ficha.grupo=null;
				ficha.subgrupo=null;
			}

			if( params.sqSubgrupo ) {
				ficha.subgrupo = DadosApoio.get( params.sqSubgrupo.toLong() )
			} else {
				ficha.subgrupo=null;
			}
		} else {

            CicloAvaliacao cicloAvaliacao = session.getAttribute('ciclo')

            // para cadastrar uma subespecie tem que ter a ficha da espécie
            if( params.w_idSubespecie && params.w_idEspecie ){
                Ficha fichaEspecie = Ficha.findByCicloAvaliacaoAndTaxon( cicloAvaliacao, Taxon.get( params.w_idEspecie.toLong() ) )
                if( !fichaEspecie ) {
                    return this._returnAjax(1, 'Ficha da espécie ainda não foi cadastrada.<br>Para cadastrar a ficha da subespécies, cadastre primeiro a ficha da espécie.', 'error');
                }
                // AO CADASTRAR UMA SUBESPECIE E A ESPÉCIE ESTIVER AMEAÇADA, EXIBIR CAMPO PARA JUTIFICATIVA E ENVIAR PARA EQUIPE SALVE
                Map ultimaAvaliacao = fichaService.getUltimaAvaliacaoNacional( fichaEspecie.id );
                if( ultimaAvaliacao.coCategoriaIucn =~ /CR|VU|EN/ ) {
                    enviarEmail = true
                    if( ! params.dsJustificativa ) {
                        return this._returnAjax(0, '', 'info', [stInformarJustificativa: true]);
                    }
                }
            }
			if( !session?.sicae?.user?.sqUnidadeOrg && !params.sqUnidadeOrg ) {
				return this._returnAjax(1, 'Unidade responsável é obrigatória', 'info');
			}
			Unidade unidade
			if( params.sqUnidadeOrg ){
				unidade = Unidade.get( params.sqUnidadeOrg.toLong())
			} else {
				unidade = Unidade.get( session?.sicae?.user?.sqUnidadeOrg )
			}
			if( !unidade ) {
				return this._returnAjax(1, 'Unidade responsável inválida', 'info');
			}

			// inclusao
			ficha = new Ficha()
			ficha.properties = params

			// nova ficha, valores padrão
			ficha.situacaoFicha = DadosApoio.findByCodigoSistema('COMPILACAO')

			ficha.cicloAvaliacao = cicloAvaliacao
			ficha.usuario = PessoaFisica.get(session?.sicae?.user?.sqPessoa) // pessoa logada

			ficha.unidade = unidade // unidade organizacional responsável pela ficha
			ficha.taxon = Taxon.get(params.sqTaxon)
			ficha.nmCientifico = ficha.taxon.noCientificoCompleto ?: ficha.taxon.noCientifico

            // ficha ja deve nascer na categoria NE-não avaliada - 17-06-2020
            ficha.categoriaIucn = dadosApoioService.getByCodigo('TB_CATEGORIA_IUCN','NE')

            if( params.sqGrupo ) {
                ficha.grupo = DadosApoio.get( params.sqGrupo.toLong() )
            }
            if( params.sqSubgrupo ) {
                ficha.subgrupo = DadosApoio.get( params.sqSubgrupo.toLong() )
            }
			// verificar se existe a mesma especie em um outro centro
			Ficha fichaTemp = Ficha.findByTaxonAndCicloAvaliacao(ficha.taxon, ficha.cicloAvaliacao)
			if ( fichaTemp ) {
				ficha.discard()
				return this._returnAjax(1, 'Taxon já cadastrado por:<br>Unidade:<b>' + fichaTemp?.unidade?.sgInstituicao + 
				'</b><br>Responsável: <b>' + fichaTemp?.usuario?.noPessoa + '</b>.', 'error');
			}

			// verificar se a espécie já foi cadastrada no mesmo ciclo
			/*fichaTemp = Ficha.findByTaxonAndCicloAvaliacao(ficha.taxon, ficha.cicloAvaliacao)
			if (fichaTemp) {
				return this._returnAjax(1, 'Expécie já cadastrada neste ciclo de avaliação!', 'error');
			}*/

			// verificar nome cientifico
			/*fichaTemp = Ficha.findByCicloAvaliacaoAndNmCientifico(ficha.cicloAvaliacao, ficha.taxon.noCientificoCompleto)
			if (fichaTemp) {
				return this._returnAjax(1, ficha.taxon.noCientifico + ' já cadastrado neste ciclo de avaliação.<br>Unidade responsável: ' + ficha.unidade.sgUnidade, 'error')
			}
			fichaTemp = Ficha.findByCicloAvaliacaoAndNmCientifico(ficha.cicloAvaliacao, ficha.taxon.noCientifico)
			if (fichaTemp) {
				return this._returnAjax(1, ficha.taxon.noCientifico + ' já cadastrado neste ciclo de avaliação.<br>Unidade responsável: ' + ficha.unidade.sgUnidade, 'error')
			}*/

		}

		if (!ficha.validate()) {
			//println ficha.getErrors()
			ficha.errors.allErrors.each {
				errors.push(messageSource.getMessage(it, locale));
			}
		}

		if (errors) {
			return this._returnAjax(1, errors.join('<br/>'), "error");
		}

		try {

			ficha.save(flush: true)
			fichaService.updatePercentual( ficha )

			//fichaService.atualizarPercentualPreenchimento( ficha )
			// gerar as pendencias dos campos obrigatórios
			//fichaService.calcularPercentualPreenchimento( ficha.id.toInteger() )

			res['sqFicha'] = ficha.id
			res['noCientifico'] = ficha.taxon.noCientificoItalico
			String msgPontoFocal = ''
			String msgCoordenadorTaxon = ''

			// gravar o ponto focal
			if (params.sqPontoFocal) {
				// gravar somente se a ficha não possuir um ponto focal atribuido
				if (FichaPessoa.countByFichaAndPapelAndInAtivo(ficha, papel, 'S') == 0) {
					papel = dadosApoioService.getByCodigo('TB_PAPEL_FICHA', 'PONTO_FOCAL')
					pontoFocal = Pessoa.get(params.sqPontoFocal.toInteger())
					if (!FichaPessoa.findByFichaAndPessoaAndPapelAndInAtivo(ficha, pontoFocal, papel, 'S')) {

						// alterar a unidade organizaciona da ficha para a do ponto focal
						// encontrar a unidade do ponto focal por alguma ficha já atribuida a ele
						FichaPessoa fp = FichaPessoa.findByPessoaAndPapelAndInAtivo(pontoFocal, papel, 'S')
						if (fp && ficha.unidade != fp.ficha.unidade) {
							ficha.unidade = fp.ficha.unidade
							ficha.save(flush: true)
						}
						FichaPessoa reg = new FichaPessoa()
						reg.ficha = ficha
						reg.pessoa = pontoFocal
						reg.papel = papel
						reg.inAtivo = 'S'
                        reg.grupo = ficha.grupo
						reg.save(flush: true)
						msgPontoFocal = '<br>Ponto focal atribuido com SUCCESSO!'
					}
				} else {
					msgPontoFocal = '<br>Ficha já possui um Ponto focal ativo!'
				}
			}

			// GRAVAR O COORDENADOR DE TAXON
			if ( params.sqCoordenadorTaxon ) {
				// gravar somente se a ficha não possuir um ponto focal atribuido
				DadosApoio papelCT = dadosApoioService.getByCodigo('TB_PAPEL_FICHA', 'COORDENADOR_TAXON')
				Pessoa pessoa = Pessoa.get( params.sqCoordenadorTaxon.toLong() )
				if( pessoa && papelCT ) {
                    WebInstituicao webInstituicao
                    if( params.sqWebInstituicaoCT ) {
                        webInstituicao = WebInstituicao.get( params.sqWebInstituicaoCT.toLong() )
                    } else {
                        FichaPessoa fichaPessoaTemp = FichaPessoa.findByPessoaAndPapelAndWebInstituicaoIsNotNull( pessoa, papelCT,[max:1] )
                        if( fichaPessoaTemp ){
                            webInstituicao = fichaPessoaTemp.webInstituicao
                        }
                    }
                    if( webInstituicao ) {
                        FichaPessoa fichaPessoa = FichaPessoa.findByFichaAndPessoaAndPapel(ficha, pessoa, papelCT)
                        if (!fichaPessoa) {
                            fichaPessoa = new FichaPessoa()
                            fichaPessoa.ficha = ficha
                            fichaPessoa.pessoa = pessoa
                            fichaPessoa.papel = papelCT
                            fichaPessoa.webInstituicao = webInstituicao
                        }
                        fichaPessoa.inAtivo = 'S'
                        fichaPessoa.grupo = ficha.grupo
                        fichaPessoa.save(flush: true)
                        msgCoordenadorTaxon = '<br>Coodenador de Taxon atribuido com SUCCESSO!'
                    } else {
                        msgCoordenadorTaxon = '<br>Coodenador de Taxon SEM instituição!'
                    }
				}
				else {
					msgCoordenadorTaxon = '<br>Coodenador de Taxon já atribuido na ficha!'
				}
			}

            if( enviarEmail ){
                String emailFrom = session.sicae.user.email
                String emailSalve = grailsApplication.config.app.email ?: 'salve@icmbio.gov.br'
                String mensagemEmail = '<p>Subespecie cadastrada: <b>' +
                    ficha.taxon.noCientificoItalico+'</b></p>'+
                    '<p>Usuário: <b>'+session.sicae.user.noPessoa+'</b></p>'+
                    '<p><b>Justificativa</b>: ' + Util.stripTags(params.dsJustificativa)+'</p>'
                emailService.sendTo(emailSalve,'Cadastro de subespecie de espécie ameaçada',mensagemEmail,emailFrom)
            }
			return this._returnAjax(0, getMsg(1) + msgPontoFocal + msgCoordenadorTaxon, "success", res)
		}
		catch (Exception e) {
			println e.getMessage();
			return this._returnAjax(1, getMsg(2), "error")
		}
		this._returnAjax(1, "Erro!", "error")
	}

	def saveNomeComum()	{
		if( !session?.sicae?.user?.sqPessoa)
		{
			return this._returnAjax(1, grailsApplication.config.msg050, "error")
		}
		if( !params.sqFicha)
		{
			return this._returnAjax(1, 'Ficha não selecionada', "error")
		}
		if( !params.noComum)
		{
			return this._returnAjax(1, 'Nome comum deve ser informado', "error")
		}
		FichaNomeComum nc;
		if( params.sqFichaNomeComum )
		{
				nc = FichaNomeComum.get( params.sqFichaNomeComum);
		}
		else
		{
			 nc = new FichaNomeComum();
			 // adicionar a ficha
			 nc.ficha = Ficha.get(params.sqFicha);
		}
		List errors=[]
		Map res = [:]
		// atribuir os valores postados ao objeto
        nc.properties = params
        //println 'IndexOf:' + params.noComum.indexOf(',')
        params.noComum.split(',').each {
            //println 'Adicionando ' + it
            //println '.......................................'
            nc.noComum = it.toString().trim()
            try
            {
                nc.save(flush: true)
                if ( params.noComum.indexOf(',') > -1)
                {   params.sqFichaNomeComun=null
                    nc = new FichaNomeComum()
                    // adicionar a ficha
                    nc.ficha = Ficha.get(params.sqFicha)
                    nc.properties = params
                }
            } catch ( Exception e)
            {
                errors.push( 'Erro salvando nome comum!')
                println e.getMessage()
            }
        }

		// atualizar log da ficha, quem alterou
		nc.ficha.dtAlteracao = new Date()
		nc.ficha.save()

        /*
		// validar campos
		if(!nc.validate())
        {
            nc.errors.allErrors.each {
                errors.push(messageSource.getMessage(it,locale) );
            }
        }
        */
        if( errors )
        {
        	return this._returnAjax(0, errors.join('<br/>'), "error")
        }
        return this._returnAjax(0, getMsg(1), "success")

		/*try
		{
            //nc.save(flush:true)
            //res['sqFichaNomeComum']= nc.id;
            return this._returnAjax(0, getMsg(1), "success",res)
        }
        catch(Exception e)
        {
        	println e.getMessage();
            return this._returnAjax(1,getMsg(2) , "error")
        }
		return this._returnAjax(1, "Erro!", "error")
		*/
	}

	def saveSinonimia()	{
		if( !session?.sicae?.user?.sqPessoa)
		{
			return this._returnAjax(1, grailsApplication.config.msg050, "error")
		}
		if( !params.sqFicha)
		{
			return this._returnAjax(1, 'Ficha não selecionada', "error")
		}
		if( !params.noSinonimia)
		{
			return this._returnAjax(1, 'Sinonímia não informada!', "error")
		}
		if( !params.noAutor)
		{
			return this._returnAjax(1, 'Nome do Autor não informada!', "error")
		}
		/*if( !params.nuAno)
		{
			return this._returnAjax(1, 'Ano não informada!', "error")
		}
		*/

		FichaSinonimia sinonimia;
		if( params.sqFichaSinonimia )
		{
			sinonimia = FichaSinonimia.get(params.sqFichaSinonimia);
		}
		else
		{
			sinonimia = new FichaSinonimia();
			sinonimia.ficha = Ficha.get(params.sqFicha); // adicionar a ficha
		}

		List errors=[]
		Map res = [:]

		// atribuir os valores postados ao objeto
		sinonimia.properties = params;

		// validar campos
		if(!sinonimia.validate())
        {
            sinonimia.errors.allErrors.each {
                errors.push(messageSource.getMessage(it,locale) );
            }
        }
        if( errors )
        {
        	return this._returnAjax(0, errors.join('<br/>'), "error");
        }
		try
		{
            sinonimia.save(flush:true)
            res['sqFichaSinonimia']= sinonimia.id;

			// atualizar log da ficha, quem alterou
			sinonimia.ficha.dtAlteracao = new Date()
			sinonimia.ficha.save()

            return this._returnAjax(0, getMsg(1), "success",res)
        }
        catch(Exception e)
        {
        	println e.getMessage();
            return this._returnAjax(1,getMsg(2) , "error")
        }
		return this._returnAjax(1, "Erro!", "error")
	}

	def getGridNomeComum() {
		if( !params.sqFicha)
		{
			render '';
			return;
		}
        Ficha ficha  = Ficha.get(params.sqFicha)
		List lista = FichaNomeComum.findAllByFicha( ficha )
		render( template:'/ficha/taxonomia/divGridNomeComum',model:['lista':lista]);
	}

	def getGridSinonimia() {
		if( !params.sqFicha)
		{
			render '';
			return;
		}
        Ficha ficha  = Ficha.get(params.sqFicha)
        List lista = FichaSinonimia.findAllByFicha( ficha )
        Ficha fichaCicloAnterior = ficha.fichaCicloAnterior
        String nomeAntigo = ''
        if( fichaCicloAnterior ) {
            nomeAntigo = fichaCicloAnterior.taxon.noCientifico
        }
		render( template:'/ficha/distribuicao/ocorrencia/divGridSinonimia',model:['listaFichaSinonimia':lista,nomeAntigo:nomeAntigo]);
	}

	def editNomeComum()	{
    	FichaNomeComum reg = FichaNomeComum.get(params.id);
		if( reg?.id )
		{
			render reg.asJson();
            return;
		}
		render [:] as JSON;
	}

	def editSinonimia()	{
    	FichaSinonimia reg = FichaSinonimia.get(params.id);
		if( reg?.id )
		{
			render reg.asJson();
            return;
		}
		render [:] as JSON;
	}

	def deleteNomeComum() {
		FichaNomeComum nomeComum = FichaNomeComum.get(params.id);
		if( nomeComum )
		{
			nomeComum.delete(flush:true);

			// atualizar log da ficha, quem alterou
			nomeComum.ficha.dtAlteracao = new Date()
			nomeComum.ficha.save()
			return this._returnAjax(0, getMsg(4), "success")
		}
		else
		{
			return this._returnAjax(1, getMsg(51), "error")
		}
		render '';
	}

	def deleteSinonimia() {

		FichaSinonimia sinonimia = FichaSinonimia.get(params.id);
		if( sinonimia )
		{
			sinonimia.delete(flush:true)

			// atualizar log da ficha, quem alterou
			sinonimia.ficha.dtAlteracao = new Date()
			sinonimia.ficha.save()

			return this._returnAjax(0, getMsg(4), "success")
		}
		else
		{
			return this._returnAjax(1, getMsg(51), "error")
		}
		render '';
	}

    def findPontoFocal() {
        if( !params.sqCicloAvaliacao ) {
            return this._returnAjax(1, 'Ciclo Avaliação não informado',"error")
        }
        if( !params.sqGrupo ) {
            return this._returnAjax(1, 'Grupo não informado',"error")
        }
        DadosApoio papelPontoFocal = dadosApoioService.getByCodigo('TB_PAPEL_FICHA','PONTO_FOCAL')
        FichaPessoa fichaPessoa = FichaPessoa.createCriteria().get {
            eq('inAtivo','S')
            eq('papel', papelPontoFocal)
            grupo {
                eq('id',params.sqGrupo.toLong())
            }
            ficha {
                cicloAvaliacao {
                    eq('id',params.sqCicloAvaliacao.toLong())
                }
            }
            maxResults(1)
        }
        return this._returnAjax(0, '',"success",[ sqPessoa: ( fichaPessoa ? fichaPessoa.pessoa.id : '' ),noPessoa: ( fichaPessoa ? fichaPessoa.pessoa.noPessoa : '' ) ] )
    }

    def findCoordenadorTaxon() {
        if( !params.sqCicloAvaliacao ) {
            return this._returnAjax(1, 'Ciclo Avaliação não informado',"error")
        }
        if( !params.sqGrupo ) {
            return this._returnAjax(1, 'Grupo não informado',"error")
        }
        DadosApoio papelCT = dadosApoioService.getByCodigo('TB_PAPEL_FICHA','COORDENADOR_TAXON')
        FichaPessoa fichaPessoa = FichaPessoa.createCriteria().get {
            eq('inAtivo','S')
            eq('papel', papelCT)
            grupo {
                eq('id',params.sqGrupo.toLong())
            }
            ficha {
                cicloAvaliacao {
                    eq('id',params.sqCicloAvaliacao.toLong())
                }
            }
            isNotNull('webInstituicao')
            maxResults(1)
        }
        return this._returnAjax(0, '',"success",[
            sqPessoa: ( fichaPessoa ? fichaPessoa.pessoa.id : '' ),
            noPessoa: ( fichaPessoa ? fichaPessoa.pessoa.noPessoa : '' )
        ] )
    }

    def findInstituicaoCT() {
        if( !params.sqCicloAvaliacao ) {
            return this._returnAjax(1, 'Ciclo Avaliação não informado',"error")
        }
        if( !params.sqPessoa ) {
            return this._returnAjax(1, 'CT não selecionado',"error")
        }
        DadosApoio papelCT = dadosApoioService.getByCodigo('TB_PAPEL_FICHA','COORDENADOR_TAXON');
        if( ! papelCT ) {
            return this._returnAjax(1, 'Papel COORDENADOR_TAXON não cadadastrado',"error")
        }
        FichaPessoa fichaPessoa = FichaPessoa.createCriteria().get {
            eq('inAtivo','S')
            eq('papel', papelCT)
            eq('pessoa.id',params.sqPessoa.toLong() )
            isNotNull('webInstituicao')
            ficha {
                eq('cicloAvaliacao.id', params.sqCicloAvaliacao.toLong())
            }
            maxResults(1)
        }
        return this._returnAjax(0, '',"success",[sqWebInstituicao:( fichaPessoa ? fichaPessoa.webInstituicao.id : '' )
                                                 ,noWebInstituicao:( fichaPessoa ? fichaPessoa.webInstituicao.noInstituicao : '' )
         ] )
    }

}
