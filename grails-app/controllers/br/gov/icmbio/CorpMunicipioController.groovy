package br.gov.icmbio

import grails.converters.JSON

class CorpMunicipioController extends BaseController {

    def corpMunicipioService

    /**
     * Exibir o formulário de manutenção dos Municipios
     * @return
     */
    def index() {
        render(view:'index',model:[estados:Estado.list() ] )
    }

    /**
     * Gravar/atualizar a Municipio no banco de dados
      * @return
     */
    def save(){
        Map res = [status:0, type:'success', msg:'Municipio gravado com SUCESSO', data:[:] ]
        try {
            res.data = corpMunicipioService.save(  params.sqMunicipio ? params.sqMunicipio.toLong() : null
                                   , params.noMunicipio ?: ''
                                   , params.coIbge ? params.coIbge.toInteger(): null
                                   , params.sqEstado ? params.sqEstado.toLong(): null
                                   , params.wkt      ?: '' )

        } catch( Exception e ){
            res.status = 1
            res.type = 'error'
            res.msg = e.getMessage()
        }
        render res as JSON
    }

    /**
     * retornar os dados da município para edição
     * @return
     */
    def edit(){
        Map res = [status:0, type:'success', msg:'', data:[:] ]
        try {
            res.data = corpMunicipioService.edit(  params.sqMunicipio ? params.sqMunicipio.toLong() : 0l)
        } catch( Exception e ){
            res.status = 1
            res.type = 'error'
            res.msg = e.getMessage()
        }
        render res as JSON
    }

    /**
     * excluir a município do banco de dados
     * @return
     */
    def delete(){
        Map res = [status:0, type:'success', msg:'Municipio excluído com SUCESSO', data:[:] ]
        try {
            res.data = corpMunicipioService.delete(  params.sqMunicipio ? params.sqMunicipio.toLong() : 0l)
        } catch( Exception e ){
            res.status = 1
            res.type = 'error'
            res.msg = e.getMessage()
        }
        render res as JSON
    }

    /**
     * criar o gride dos municpípios
     * @return
     */
    def grid(){
        Integer pageSize = 100
        Integer totalRecords = params.totalRecords ?: 0
        Integer totalPages
        Integer page
        try {
            if( !params.totalRecords ) {
                totalRecords = Municipio.createCriteria().count{}
            }
            totalPages = Math.ceil( totalRecords / pageSize )
            page = params.page ? params.page.toInteger() : 1
            page = Math.min(totalPages,page)
            List municipios = corpMunicipioService.list( page, pageSize, params.q )
            render(template: 'gridMunicipio', model: [municipios: municipios
                                                      , page:page
                                                      , totalRecords:totalRecords
                                                      , totalPages:totalPages])
        } catch( Exception e ) {
            render e.getMessage()
        }
    }
}
