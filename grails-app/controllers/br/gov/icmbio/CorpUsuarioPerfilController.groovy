package br.gov.icmbio

import grails.converters.JSON

class CorpUsuarioPerfilController extends BaseController {

    def corpUsuarioPerfilService
    def dadosApoioService

    /**
     * Exibir o formulário de manutenção do Perfil do Usuário
     * @return
     */
    def index() {
        params.sqPessoa =  params.sqPessoa ?: 0l
        Usuario usuario = Usuario.get( params.sqPessoa.toLong() )
        List tiposInstituicao = dadosApoioService.getTable('TB_TIPO_INSTITUICAO')
        List perfisGeral = []
        List perfisFicha = []
        List perfisInstituicao = []
        Perfil.list().sort{it.noPerfil}.each {
            if (it.cdSistema =~ /ADM|CONSULTA/) {
                perfisGeral.push([sqPerfil: it.id, noPerfil: it.noPerfil, cdSistema: it.cdSistema])
            } else if (it.cdSistema =~ /TRADUTOR|VALIDADOR|COORDENADOR_TAXON|COLABORADOR/) {
                perfisFicha.push([sqPerfil: it.id, noPerfil: it.noPerfil, cdSistema: it.cdSistema])
            }
            if (it.cdSistema =~ /COLABORADOR|PONTO_FOCAL/) {
                perfisInstituicao.push([sqPerfil: it.id, noPerfil: it.noPerfil, cdSistema: it.cdSistema])
            }
        }
        render(view:'index',model:[usuario:usuario
                                   ,perfisGeral:perfisGeral
                                   ,perfisFicha:perfisFicha
                                   ,perfisInstituicao:perfisInstituicao
                                   ,tiposInstituicao:tiposInstituicao] )
    }

    /**
     * Gravar/atualizar o Perfil do Usuário no banco de dados
      * @return
     */
    def save(){
        Map res = [status:0, type:'success', msg:'Perfil gravado com SUCESSO', data:[:] ]
        try {

             corpUsuarioPerfilService.save( params.sqPessoa ? params.sqPessoa.toLong() : 0l
                                          , params.sqPerfilGeral ?: ''
                                          , params.sqPerfilFicha ?: ''
                                          , params.sqPerfilInstituicao ?: ''
                                          , params.sqInstituicao ? params.sqInstituicao.toLong() : 0l

             )
        } catch( Exception e ){
            res.status = 1
            res.type = 'error'
            res.msg = e.getMessage()
        }
        render res as JSON
    }

    /**
     * retornar os dados do perfil do usuario
     * @return
     */
    def edit(){
        Map res = [status:0, type:'success', msg:'', data:[] ]
        try {
            res.data = corpUsuarioPerfilService.edit(params.sqPessoa ? params.sqPessoa.toLong() : 0l
                                                 ,params.sqInstituicao ? params.sqInstituicao.toLong() : 0l)
        } catch( Exception e ){
            res.status = 1
            res.type = 'error'
            res.msg = e.getMessage()
        }
        render res as JSON
    }

    /**
     * excluir o perfil do usuário
     * @return
     */
    def delete(){
        Map res = [status:0, type:'success', msg:'Perfil excluído com SUCESSO', data:[:] ]
        try {
            res.data = corpUsuarioPerfilService.delete(params.sqUsuarioPerfilInstituicao ? params.sqUsuarioPerfilInstituicao.toLong() : 0l )
        } catch( Exception e ){
            res.status = 1
            res.type = 'error'
            res.msg = e.getMessage()
        }
        render res as JSON
    }

    /**
     * criar a lista de perfils por instuição do usuário
     * @return
     */
    def ulPerfilInstituicao(){
        Map res = [status:0, type:'success', msg:'', html:"" ]
        try {
            List list = corpUsuarioPerfilService.listPerfilInstituicao(params.sqPessoa ? params.sqPessoa.toLong() : 0l )
            res.html = g.render(template:'ulPerfilInstituicao', model:[perfisInstituicao:list])
        } catch( Exception e ) {
            res.status = 1
            res.type = 'error'
            res.msg = e.getMessage()
        }
        render res as JSON
    }


    /**
     * selecionar a instituição para atribuir o perfil ao usuário
     * @return
     */
    def select2Instituicao() {
        Map data = ['items': []]
        DadosApoio tipoInstituicao


        /*
        if (!params.sqTipoInstituicao) {
            println 'PARAMETROS INSUFICIENTES'
            render ''
            return
        }*/

        if ( params.sqTipoInstituicao) {
            tipoInstituicao = DadosApoio.get( params.sqTipoInstituicao.toLong())
        }

        Instituicao.createCriteria().list {
            if( tipoInstituicao ) {
                eq('tipo', tipoInstituicao)
            }
            if( params.stInterna ) {
                eq('stInterna',true)
            }
            or {
                ilike('sgInstituicao','%' + params.q + '%')
                pessoa {
                    ilike('noPessoa', '%' + params.q + '%')
                }
            }
        }.each {
            data.items.push(['id': it.id
                             , 'title': it.pessoa.noPessoa
                             ,'descricao':it.sgInstituicao ?: it.pessoa.noPessoa ])
        }
        render data as JSON
    }
}
