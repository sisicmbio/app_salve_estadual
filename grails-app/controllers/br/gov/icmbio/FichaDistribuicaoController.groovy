package br.gov.icmbio
import grails.converters.JSON
import org.codehaus.groovy.grails.web.json.JSONElement

import java.text.DecimalFormat;
import com.vividsolutions.jts.geom.Geometry
import com.vividsolutions.jts.geom.GeometryFactory
import com.vividsolutions.jts.geom.Coordinate

class FichaDistribuicaoController extends FichaBaseController {

	def dadosApoioService
    def requestService
    def fichaService
    def utilityService
    def sqlService
    //def asyncService
    // def messsageSource

	// ############################################################################################################################################################################################# //
	// 	MODULO FICHA/DISTRIBUICAO 																																									 //
	// ############################################################################################################################################################################################# //

	/**
	 * Método responsável por exibir template de Distribuição.
	 * Tela: 2 - Distribuicao.
	 * Menu: Espécie -> Ficha de Espécie
	 * @return [description]
	 */
	def index() {

        if( !params.sqFicha )
        {
            render 'id da ficha não informado.'
            return
        }

		Ficha ficha = Ficha.get( params.sqFicha.toInteger() )
        //
        //fichaService.asyncLoadPortalbio([ idFicha: ficha.id.toInteger(), jsCallback:'distribuicao.oMap.refresh();'] )

        /*
        // ler as ocorrências do portalbio e gravar na tabela ficha_ocorrencia_portalbio
        if ( ficha && ficha.situacaoFicha.codigo =~ /(COMPILACAO|CONSULTA|CONSULTA_FINALIZADA|CONSOLIDADA)$/  )
        {
            try {
                Integer qtd = FichaOcorrenciaPortalbio.countByFicha( ficha )
                TimeDuration td = TimeCategory.minus(  new Date(),ficha.dtAlteracao?:new Date() )
                //println 'Minutos:' + td.minutes
                if( qtd == 0 || !ficha.dtAlteracao || td.days > 1 )
                //if( true )
                {
                    asyncService.importarOcorrenciasPortalbio( params.sqFicha.toInteger(),'distribuicao.oMap.refresh();' )
                    //if( ficha.dtAlteracao && ( Util.hoje() - ficha.dtAlteracao) > 0 ) {
                    //println ficha.nmCientifico + ' - atualizando registros do portalbio ' + new Date().format('dd/MM/yyyy HH:mm:ss')
                    //fichaService.loadOcorrenciaPortalbio(ficha, grailsApplication?.config?.url?.biocache)
                }
            } catch( Exception e) {
                println e.getMessage()
            }
        }
        */

		render(template:"/ficha/distribuicao/index", model:['ficha': ficha]);
	}

	/**
	 * Método responsável por gravar informações da distribuição da espécie.
	 * Tela: 2 - Distribuicao.
	 * Menu: Espécie -> Ficha de Espécie
	 * @return [description]
	 */
	def saveFrmDistribuicao() {
		Ficha ficha = Ficha.get(params.sqFicha)
		ficha.properties = params
        try {
            ficha.save(flush:true)
            fichaService.updatePercentual( ficha )
            //fichaService.atualizarPercentualPreenchimento( ficha )
            this._returnAjax(0, getMsg(1), "success")
        } catch(Exception e) {
            println e.getMessage()
            this._returnAjax(1, getMsg(2), "error")
        }
	}

    def getPoints(){
    	// o retorno será JSON
        response.setHeader("Content-Type", "application/json");
        // cabeçalho padrão do GeoJson da resposta
        Map geoJson = ["type": "FeatureCollection", 'crs': ['type': 'name', 'properties': ['name': 'EPSG:4326']], "features": []];
        String url  = grailsApplication.config.url.biocache;
		String noCientifico;
		Ficha ficha
        String color
        List fixedColors = ['#ff0000','#0000ff','#00ff00','#808080','#C0C0C0','#2F4F4F','#00008B','#483D8B','#4B0082','#9370DB','#4682B4','#483D8B','#B0C4DE','#800080','#FF00FF','#FF1493','#FF69B4','#DB7093','#DC143C','#FF0000','#800000','#B22222','#FFB6C1','#FA8072','#FF6347','#FF7F50','#D2691E','#CD853F','#BC8F8F','#DEB887','#FF4500','#FF8C00','#006400','#556B2F','#808000','#00FF00','#9ACD32','#ADFF2F','#7FFF00','#00FF7F','#20B2AA','#00FFFF','#00CED1','#1E90FF','#00BFFF','#008B8B','#7FFFD4','#98FB98','#DAA520','#FFFF00','#FFD700','#F0E68C']
        Map cores =[:]
        def feature

        if(!params.sqFicha)
        {
            render geoJson as JSON;
        }
        else
        {
            ficha = Ficha.get( params.sqFicha );
            if( !ficha )
            {
                render '{}';
                return;
            }

            // ler os pontos cadastrados no ciclo
            noCientifico = ficha.taxon.noCientifico
            if( ! noCientifico )
            {
                render '{}'
                return;
            }
            if( params.bd == 'portalbio')
            {
                //https://portaldabiodiversidade.icmbio.gov.br/biocache-service/occurrences/search?q=kingdom:Animalia
                //https://portaldabiodiversidade.icmbio.gov.br/biocache-service/occurrences/search?q=raw_taxon_name:"Puma concolor"
                //String par = 'q=raw_taxon_name:"'+params.noCientifico+'"&start=0&rows=1000000000&fl=institution_code,collection_code,catalogue_number,latitude,longitude,data_provider,data_resource,common_name,record_number,last_load_date,collector,raw_locality,Sensivel_s,Ameaca_s,categoriaAmeaca_s,municipality,disposition&wt=json';
                //String par = 'https://portaldabiodiversidade.icmbio.gov.br/biocache-service/occurrences/search';
                //String p='raw_taxon_name:"' + URLEncoder.encode( params.noCientifico, "UTF-8")+'"';
                String p = 'raw_taxon_name:"' + noCientifico + '"'
                // fazer a importação do portalbio somente se a ficha estiver na situação compilaçao
                if (ficha.situacaoFicha.codigoSistema == 'COMPILACAO')
                {
                    if (!grailsApplication?.config?.url?.biocache)
                    {
                        render geoJson as JSON;
                        println 'Constante url.biocache não definido no arquivo de confirações'
                        return;
                    }

                    if (!grailsApplication?.config?.url?.solr)
                    {
                        render geoJson as JSON;
                        println 'Constante url.solr não definido no arquivo de confirações'
                        return;
                    }

                    try
                    {
                        //println 'Lendo registros de ocorrencias de:'
                        //println url
                        //println '-'*100
                        //println ' '
                        def dados = requestService.doPost(url, [q: p, pageSize: 10000, params: 'json'], "")
                        // println '-----------------------------------------------------------'
                        if (!dados)
                        {
                            render '{}'
                            return
                        }

                        def tmp = JSON.parse(dados)
                        String strData
                        Date dataOcorrencia
                        Date dataCarencia
                        def data;
                        def eventDate
                        def carencia
                        def local   // raw_locality
                        def codigo  // raw_collectionCode
                        List occurrences
                        FichaOcorrenciaPortalbio fop
                        GeometryFactory gf = new GeometryFactory()
                        if (tmp?.sr?.occurrences)
                        {
                            occurrences = tmp?.sr?.occurrences
                        } else
                        {
                            occurrences = tmp.occurrences
                        }
                        //println 'Ocorrencias: ' + occurrences.size();
                        occurrences.eachWithIndex { item, i ->
                            try
                            {
                                if (item?.decimalLatitude
                                        && item?.decimalLatitude.toString() != 'null'
                                        && item?.collector.toString() != 'null'
                                        && item?.decimalLatitude.toString().indexOf('.') > 0
                                        && item?.decimalLatitude.toString().indexOf('.') < 4
                                        && item?.decimalLatitude.toString().indexOf('E') == -1
                                        && item?.uuid
                                )
                                {
                                    try
                                    {
                                        eventDate = '';
                                        // gravar os pontos do portalbio no salve

                                        Geometry geometry = gf.createPoint(new Coordinate(item.decimalLongitude, item.decimalLatitude))
                                        if (geometry) {
                                            geometry.SRID = 4674
                                            fop = FichaOcorrenciaPortalbio.findByFichaAndGeometryAndNoAutor(ficha, geometry, item.collector.trim())
                                            if (!fop)
                                            {
                                                fop = new FichaOcorrenciaPortalbio()
                                                fop.ficha = ficha
                                                fop.geometry = geometry
                                            }
                                            fop.deUuid = item.uuid.toString().trim()

                                            dataOcorrencia = null
                                            strData = item.verbatimEventDate
                                            if( strData ) {
                                                strData = strData.split(' ')[0]
                                                if (strData.length() == 10) {
                                                    dataOcorrencia = utilityService.str2Date(strData)
                                                }
                                            }
                                            if ( !dataOcorrencia && item.eventDate )
                                            {
                                                dataOcorrencia = utilityService.str2Date( item.eventDate )
                                            }

                                            if ( dataOcorrencia )
                                            {
                                                eventDate = '<br/>Data: ' + dataOcorrencia.format("dd/MMM/yyyy");
                                            }
                                            carencia = ''
                                            data = ''
                                            dataCarencia=null
                                            if (item?.accessRights)
                                            {
                                                dataCarencia = utilityService.str2Date( item.accessRights )
                                            }
                                            if ( dataCarencia )
                                            {
                                                carencia = '<br/>Carência: ' + dataCarencia.format('dd/MM/yyyy')
                                            }

                                            local = ''
                                            if (item?.raw_locality)
                                            {
                                                local = item.raw_locality
                                            }
                                            codigo = ''
                                            if (item.raw_collectionCode)
                                            {
                                                codigo = item.raw_collectionCode
                                            }
                                            if (item.scientificName)
                                            {
                                                fop.noCientifico = item.scientificName
                                            } else if (item?.raw_scientificName)
                                            {
                                                fop.noCientifico = item.raw_scientificName
                                            }
                                            fop.noLocal = local
                                            fop.noInstituicao = item.raw_institutionCode + '/' + codigo
                                            fop.noAutor = item.collector.trim()
                                            fop.deAmeaca = item.ameaca_s

                                            if ( dataOcorrencia )
                                            {
                                                try {
                                                    fop.dtOcorrencia = dataOcorrencia
                                                } catch(Exception e ) {}
                                            }
                                            if ( dataCarencia )
                                            {
                                                try {
                                                    fop.dtCarencia = dataCarencia
                                                } catch ( Exception e ) {}
                                            }
                                            fop.txOcorrencia = item.toString()

                                            if (!fop.save())
                                            {
                                                println fop.getErrors()
                                            }
                                        }
                                    }
                                    catch (Exception e) {
                                        println e.getMessage()
                                        println "Registro inválido: " + item.raw_institutionCode + " uuid:" + item.uuid.toString()
                                        println 'eventDate: ' + item?.eventDate
                                        println 'carencia: ' + item?.carencia
                                        println 'accessRights: ' + item?.accessRights
                                        println 'raw_locality: ' + item?.raw_locality
                                        println 'raw_collectionCode: ' + item?.raw_collectionCode
                                        println 'scientificName: ' + item?.scientificName
                                        println 'raw_scientificName: ' + item?.raw_scientificName
                                        println 'raw_institutionCode: ' + item?.raw_institutionCode
                                        println 'collector: ' + item?.collector
                                        println 'ameaca_s: ' + item?.ameaca_s
                                        println 'decimalLongitude: ' + item?.decimalLongitude
                                        println 'decimalLatitude: ' + item?.decimalLatitude
                                        println 'Continuando......'
                                        println "-" * 100
                                    }
                                } else
                                {
                                    def f = new DecimalFormat('##################');
                                    println "Coordendada Invalida: " + item.raw_institutionCode + " uuid:" +
                                            item.uuid.toString() + ' Lon:' +
                                            f.format(item.decimalLongitude) +
                                            ', Lat:' + f.format(item.decimalLatitude); ;
                                }
                            } catch (Exception e) {
                                //println "Registro inválido: " + item.raw_institutionCode + " uuid:" + item.uuid.toString()
                            }
                        }
                    }
                    catch (Exception e) {
                        println e.getMessage()
                    }
                }
                cores['portalbio'] = fixedColors[ 0 ]
                FichaOcorrenciaPortalbio.findAllByFicha(ficha).each {
                    feature = ["type"      : "Feature",
                               "id"        : it.id,
                               "properties": [
                                       "text"    : '<b>Portalbio/' + it.noInstituicao + '</b>' +
                                               '<br/>' + it.noLocal +
                                               '<br/>' + it.noCientifico +
                                               '<br/>Responsável: ' + it.noAutor +
                                               (it.dtOcorrencia ? '<br/>Data Ocorrência: '+it.dtOcorrencia.format("dd/MMM/yyyy") : '') +
                                               (it.dtCarencia ? '<br/>Carência: '+it.dtCarencia.format("dd/MMM/yyyy") : '')
                                               /*+'<br>'+
                                               '<a href="#trFichaDistportalbio'+it.id+'" title="Identificar no gride">Gride</a>'*/
                                               ,
                                       //'<br><a title="Abrir página do Portal da Biodiversidade" target="_blank" href="https://portaldabiodiversidade.icmbio.gov.br/portal/occurrences/' + it.deUuid + '">Abrir página do PortalBio</a>',
                                       "uuid"    :it.deUuid,
                                       "bd"      : 'portalbio',
                                       "color"   : cores['portalbio'], //color,
                                       "legend"  : 'PortalBio',//it.noInstituicao,//(color=="#ff000"?'Espécie Ameaçada':'Espécie Não Ameaçada'),
                                       "ameaca_s": it.deAmeaca,
                                       "utilizadoAvaliacao": it.inUtilizadoAvaliacao,
                                       "txNaoUtilizadoAvaliacao": it.txNaoUtilizadoAvaliacao?:''
                                       // TODO - adicionar mais atributtos aqui
                               ],
                               "geometry"  : [
                                       "type"       : "Point",
                                       "coordinates": [it.geometry.x, it.geometry.y]
                               ]
                    ];
                    geoJson.features.push(feature)
                }
            }
            else if( params.bd == 'avaliacao' )
            {
                // ler os pontos adicionados no processo de avaliação
                DadosApoio contextoOcorrencia = dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA', 'REGISTRO_OCORRENCIA')

                cores['salve'] = fixedColors[ 1 ]
                FichaOcorrencia.findAllByFichaAndContexto(ficha, contextoOcorrencia ).each {
                    if (it.geometry)
                    {
                        feature = ["type"      : "Feature",
                                   "id"        : it.id,
                                   "properties": [
                                           "text": '<b>SALVE<br>Espécie</b><br>'+
                                           it.ficha.nmCientifico+'<br>'+
                                           '<span class="nowrap">Presença Atual na Coordenada: <b>'+
                                                   it.inPresencaAtualText + '</b></span>',
                                           "bd"  : 'salve',
                                           "color": cores['salve'], //color,
                                           "legend":'Salve', //it.ficha.nmCientifico,
                                           "utilizadoAvaliacao": it.inUtilizadoAvaliacao,
                                           "txNaoUtilizadoAvaliacao": it.txNaoUtilizadoAvaliacao?:''

                                           // TODO - adicionar mais atributtos aqui
                                   ],
                                   "geometry"  : [
                                           "type"       : "Point",
                                           "coordinates": [it?.geometry?.x, it?.geometry?.y]
                                   ]
                        ];
                        geoJson.features.push(feature);
                    }
                }
                // ler os pontos das subespecies
                if( ficha.taxon.nivelTaxonomico.coNivelTaxonomico=='ESPECIE')
                {
                    cores['salve'] = fixedColors[ 1 ]
                    FichaOcorrencia.createCriteria().list {
                        createAlias('ficha','f')
                        createAlias('ficha.taxon','taxon')
                        eq('taxon.pai', ficha.taxon)
                        eq('contexto',contextoOcorrencia )
                    }.each {
                        if (it.geometry)
                        {
                            feature = ["type"      : "Feature",
                                       "id"        : it.id,
                                       "properties": [
                                               "text": '<b>SALVE<br>Subespecie</b><br>'+
                                                       it.ficha.nmCientifico+'<br>'+
                                                       '<span class="nowrap">Presença Atual na Coordenada: <b>'+
                                                       it.inPresencaAtualText + '</b></span>',
                                               "bd"  : 'salve',
                                               "color": cores['salve'], //color,
                                               "legend":'Salve' ,
                                               "utilizadoAvaliacao": it.inUtilizadoAvaliacao,
                                               "txNaoUtilizadoAvaliacao": it.txNaoUtilizadoAvaliacao?:''

                                               // TODO - adicionar mais atributtos aqui
                                       ],
                                       "geometry"  : [
                                               "type"       : "Point",
                                               "coordinates": [it?.geometry?.x, it?.geometry?.y]
                                       ]
                            ];
                            geoJson.features.push(feature);
                        }
                    }
                }
            }
//            println 'bd:' + params.bd
//            println 'Quantidade de pontos:' + geoJson.size();
//            println '--------------------------'
//            println ' '
            render geoJson as JSON;
        }
    }

	def getGridAnexo()
	{
		if( !params.sqFicha )
		{
			render  'ID da ficha não informado.'
			return
		}
    	DadosApoio contexto = dadosApoioService.getByCodigo('TB_CONTEXTO_ANEXO','MAPA_DISTRIBUICAO');
    	List dados = FichaAnexo.findAllByFichaAndContexto(Ficha.get( params.sqFicha),contexto );
        String appUrl = grailsApplication.config.url.sistema ?: '/salve-estadual/'

        boolean canModify = ! session.sicae.user.isCO()
        render( template:'/ficha/distribuicao/divGridAnexos',model:[listFichaAnexo:dados, appUrl:appUrl, canModify:canModify ]);
	}

	// ############################################################################################################################################################################################# //
	// 	MODULO FICHA/DISTRIBUICAO/UF 																																								 //
	// ############################################################################################################################################################################################# //

	/**
	 * Método responsável por exibir template de UF.
	 * Tela: 2.1 - UF.
	 * Aba: 2 - Distribuicao.
	 * Menu: Espécie -> Ficha de Espécie
	 * Regras:
	 * 		1. Enviar a lista de UF's e as UF's cadastradas na ficha
	 * @return [description]
	 */
	def tabUf() {
        List estados=[]
		if( !params.sqFicha )
		{
			render 'Id da ficha não informado!';
			return;
		}
		//List estados = Estado.list().sort { it.noEstado }
		//List listUfs = FichaOcorrencia.findAllByEstadoIsNotNullAndNoLocalidadeIsNullAndFicha(Ficha.get(params.sqFicha),[sort: "estado.noEstado"])
		//List listUfs = FichaOcorrencia.findAllByFichaAndContexto(Ficha.get(params.sqFicha), dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA','ESTADO'));

        /*List listUfs = FichaUf.findAllByFicha( Ficha.get(params.sqFicha) );
		// listar somente os estados ainda não adicionados na ocorrencia uf
        estados=Estado.createCriteria().list() {
            if(listUfs) {
                not {
                    'in'('id',listUfs.estado.id)
                }
            }
            order( "noEstado")
        }*/
		render(template:"/ficha/distribuicao/formUF", model:['estados': estados]);
	}

    def getTreeUfs(){
        List data =[]
        if( !params.sqFicha )
        {
            render data as JSON
            return
        }
        sqlService.execSql("""select  uf.sq_estado
                                  , uf.no_estado
                                  , case when fu.sq_ficha_uf is null then false  else true end as st_existe
                            from salve.estado as uf
                            left outer join salve.ficha_uf fu on fu.sq_ficha = :sqFicha and fu.sq_estado = uf.sq_estado""",
                            [sqFicha:params.sqFicha.toLong()]).each { row->
           data.push( [id:row.sq_estado,title:row.no_estado,icon:false,folder:false,selected:( row.st_existe ? 'existe' : '' )])
        }
        render data as JSON
    }

    def saveTreeUf() {
        if (!params.sqFicha)
        {
            return this._returnAjax(1, 'ID da ficha não informado.', "error")
        }
        Ficha ficha 		= Ficha.get(params.sqFicha.toLong())
        // NOVO-REGISTRO
        List idsExcluidos
        if( params.ids ) {
            // ler os ids excluidos para excluir as referencias bibliográficas relacionadas ao Estado
            idsExcluidos = FichaUf.executeQuery("select new map( a.id as id) from FichaUf a where a.ficha.id = ${params.sqFicha} and a.estado.id not in (${params.ids})").id
            // exlcuir Estados desmarcados
            FichaUf.executeUpdate("""delete from FichaUf where ficha.id = ${params.sqFicha} and estado.id not in (${params.ids})""")
            // adicionar novos Estados
            Estado.executeQuery("from Estado a where a.id in (${params.ids} ) and not exists ( from FichaUf x where x.ficha.id=${params.sqFicha} and x.estado.id = a.id )").each { estado ->
                FichaUf fu = new FichaUf()
                fu.ficha = ficha
                fu.estado = estado
                fu.save()
            }
        } else {
            idsExcluidos = FichaUf.executeQuery("select new map( a.id as id) from FichaUf a where a.ficha.id = ${params.sqFicha}").id
            FichaUf.executeUpdate("""delete from FichaUf where ficha.id = ${params.sqFicha}""")
        }
        if( idsExcluidos ) {
            FichaRefBib.executeUpdate("delete from FichaRefBib where ficha.id=${ficha.id.toString()} and noTabela='ficha_uf' and noColuna='sq_ficha_uf' and sqRegistro in (${idsExcluidos.join(',')})")
        }
        //fichaService.atualizarPercentualPreenchimento( ficha )
        fichaService.updatePercentual( ficha )
        return this._returnAjax(0, getMsg(1), 'success');
    }


	/**
	 * Método responsável por gravar uma UF.
	 * Tela: 2.1 - UF.
	 * Aba: 2 - Distribuicao.
	 * Menu: Espécie -> Ficha de Espécie
	 * @return JSON
	 */
	/*

	não precisa mais com a seleção via treeview
	def saveFrmDisUf()
	{
		if( ! params.sqFicha )
		{
			return this._returnAjax(1, 'ID da ficha não informado', "error")
		}
		if( ! params['sqEstado[]'] )
		{
			return this._returnAjax(1, 'Estado não selecionado', "error");
		}
		List ufs=[];
		Ficha ficha 		= Ficha.get(params.sqFicha)
		DadosApoio contexto = dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA','ESTADO');
		if( params['sqEstado[]'].getClass() == String)
		{
			ufs.push( params['sqEstado[]'] )
		}
		else
		{
			ufs=params['sqEstado[]'];
		}
		if( ufs.size() == 0)
		{
			return this._returnAjax(1, 'Nenhum Estado Selecionado', "error");
		}
		params.ficha = ficha
		params.contexto = contexto

		def  fichaOcExists;
		ufs.each{
			params.estado = Estado.get( it )
			fichaOcExists = FichaOcorrencia.countByFichaAndEstadoAndContexto(params.ficha,params.estado,params.contexto);
			if( ! fichaOcExists ) {
				new FichaOcorrencia(params).save(flush:true);
			}
		}
		return this._returnAjax(0);
	}
	*/
	/**
	 * Método responsável por excluir uma UF.
	 * Tela: 2.1 - UF.
	 * Aba: 2 - Distribuicao.
	 * Menu: Espécie -> Ficha de Espécie
	 * Regras:
	 * 		1. Se existir mais de um registro com o mesmo estado, considera-se que há ocorrência registrada com o estado, portanto, não
	 *   		pode haver a exclusão do estado.
	 * @return JSON
	 */
	def deleteDistUf(){
		try{
			//FichaOcorrencia.get(params.sqFichaOcorrencia).delete(flush:true);
			FichaUf.get(params.sqFichaOcorrencia).delete(flush:true);
			return this._returnAjax(0, getMsg(4), "success")
		}
		catch(Exception e) {
			return this._returnAjax(1, e.getMessage(), "error");
		}
	}

	/**
	 * [selAllUf description]
	 * @return [description]
	 */
	def selAllUf(){
		Ficha ficha 		= Ficha.get(params.sqFicha)
		DadosApoio contexto = dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA','ESTADO');
		// selecionar todos os estados que não estiverm já incluídos
		//List ufsExistentes = FichaOcorrencia.findAllByFichaAndContexto(ficha,contexto);
		List ufsExistentes = FichaUf.findAllByFicha(ficha);
		params['sqEstado[]']=[];
		Estado.createCriteria().list
        {
			not {
			 	'in'('id',ufsExistentes.estado.id)
			}

		}.each {
			params['sqEstado[]'].push( it.id )
		}
		if( params['sqEstado[]'].size() > 0 ) {
			return this.saveFrmDisUf();
		}
		return this._returnAjax(1, 'Nenhum Estado Selecionado', "error");
	}

	/**
	 * [getGridDistUf description]
	 * @return [description]
	 */
	def getGridDistUf() {
        boolean  isSubespecie=false
		if( ! params.sqFicha ) {
			render ''
			return
		}
        // List listUfs = []
        // List listUfs = fichaService.getUfsOcorrencia( params.sqFicha.toLong() )
        // listUfs = FichaUf.findAllByFicha( Ficha.get( params.sqFicha.toLong() ) )

        List result = Ficha.executeQuery("""select new map(f.taxon.nivelTaxonomico.coNivelTaxonomico as coNivelTaxonomico) from Ficha f where f.id=:sqFicha""",[sqFicha:params.sqFicha.toLong()])
        isSubespecie = result && result[0].coNivelTaxonomico=='SUBESPECIE'

        //NOVO-REGISTRO

        // exibir cada Estado com suas referencias sem duplicar os Estados
        List listUfsFichaTodas=[]
        fichaService.getUfsFicha(params.sqFicha.toLong()).each {row ->
            //String refBib = Util.parseRefBib2Grid( row?.json_ref_bibs?.toString())
            Map item = listUfsFichaTodas.find{
                it.no_estado == row.no_estado && it.no_contexto == row.no_contexto
            }
            if( ! item ){
                item = [no_contexto:row.no_contexto, sq_ficha_uf:row.sq_ficha_uf, no_estado:row.no_estado, co_nivel_taxonomico:row.co_nivel_taxonomico, json_ref_bibs:[] ]
                listUfsFichaTodas.push(item )
            }
            if( row.json_ref_bibs ) {
                List listRefs = JSON.parse( row.json_ref_bibs )
                listRefs.each { ref ->
                    if( ! item.json_ref_bibs.contains( ref ) ) {
                        item.json_ref_bibs.push( ref )
                    }
                }
            }
        }

        List listUfsFicha = listUfsFichaTodas.findAll { it.no_contexto =='ficha' }
        List listUfsRegistros = listUfsFichaTodas.findAll { it.no_contexto =='ocorrencia' }

        //NOVO-REGISTRO
        //List listUfsRegistros = fichaService.getUfsRegistro(params.sqFicha.toLong())

		render(template:'/ficha/distribuicao/divGridDistUf', model:['listUfsFicha':listUfsFicha,'listUfsRegistros':listUfsRegistros,isSubespecie:isSubespecie])
	}

	// ############################################################################################################################################################################################# //
	// 	MODULO FICHA/DISTRIBUICAO/BIOMA																																								 //
	// ############################################################################################################################################################################################# //

	def tabBioma() {
        List biomas = []
        List fichaOcorrenciaBioma=[]
		if( !params.sqFicha )
		{
			render 'Id da ficha não informado!';
			return;
		}
		//def biomas = dadosApoioService.getTable('TB_BIOMA')
		//List fichaOcorrenciaBioma = FichaOcorrencia.findAllByBiomaIsNotNullAndFicha(Ficha.get(params.sqFicha))

		/*List fichaOcorrenciaBioma = FichaOcorrencia.findAllByFichaAndContexto( Ficha.get(params.sqFicha), dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA','BIOMA'));

		// carregar somente os biomas que ainda não foram adicionados
		biomas=DadosApoio.createCriteria().list()
		{
            eq('pai', DadosApoio.findAllByCodigo('TB_BIOMA')[0] )
        	if(fichaOcorrenciaBioma)
        	{
                not {
                    'in'('id',fichaOcorrenciaBioma.bioma.id)
                }
            }
            order("ordem")
        }*/
		render(template:"/ficha/distribuicao/formBioma", model:['biomas': biomas, 'fichaOcorrenciaBioma': fichaOcorrenciaBioma]);
	}

	def getTreeRecursiveBioma(DadosApoio apoio, Lista listExistentes ) {
        boolean existe = listExistentes.list.find {
            FichaBioma fichaBioma -> fichaBioma?.bioma?.id == apoio.id
        };
        Map node = [id:apoio.id,title:apoio.descricao,icon:false,folder:false,selected:existe];
        return node;
    }

    def getTreeBiomas() {
        List data =[]

        if( !params.sqFicha )
        {
            render data as JSON
            return;
        }
        // selecionar os usos já cadatrados para marcar os checks
		Ficha ficha 		= Ficha.get(params.sqFicha)
		//DadosApoio contexto = dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA','BIOMA' )
		//Lista listExistentes = new Lista( FichaOcorrencia.findAllByFichaAndContexto( ficha,contexto ) )
		Lista listExistentes = new Lista( FichaBioma.findAllByFicha( ficha ) )
        List listBiomas = dadosApoioService.getTable('TB_BIOMA');
        listBiomas.each {
            data.push( getTreeRecursiveBioma( it , listExistentes ) )
        }
        render data as JSON
    }

    def saveTreeBioma() {
        if (!params.sqFicha)
        {
            return this._returnAjax(1, 'ID da ficha não informado.', "error")
        }
        Ficha ficha = Ficha.get( params.sqFicha.toLong() )
        /*
        List idsValidos = []
        if ( params.ids )
        {
            idsValidos = params.ids.split(',');
        }
		DadosApoio contexto = dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA','BIOMA' )
        List idsOcorrenciasExcluidas=[]
		FichaOcorrencia.findAllByFichaAndContexto( ficha, contexto ).each
		{ fichaOcorrencia ->
			if (!idsValidos.find { it.toInteger() == fichaOcorrencia.bioma.id.toInteger() })
            {
                try {
                    idsOcorrenciasExcluidas.push( fichaOcorrencia.id )
                    fichaOcorrencia.delete(flush: true)
                } catch( Exception e ) {}
            } else
            {
                idsValidos.removeAll{ it.toInteger() == fichaOcorrencia.bioma.id.toInteger() }
            }
		}
        if( idsOcorrenciasExcluidas ) {
            FichaRefBib.executeUpdate("delete from FichaRefBib where ficha.id=${ficha.id.toString()} and noTabela='ficha_ocorrencia' and noColuna='sq_bioma' and sqRegistro in (${idsOcorrenciasExcluidas.join(',')})")
        }
        // gravar os novos ids
        idsValidos.each {
            new FichaOcorrencia(ficha: ficha, contexto:contexto, bioma: DadosApoio.get(it.toInteger())).save()
        }*/

        // NOVO-REGISTRO
        List idsExcluidos
        if( params.ids ) {
            // ler os ids excluidos para excluir as referencias bibliográficas relacionadas ao Bioma
            idsExcluidos = FichaBioma.executeQuery("select new map( a.id as id) from FichaBioma a where a.ficha.id = ${params.sqFicha} and a.bioma.id not in (${params.ids})").id
            // exlcuir Biomas desmarcados na tela
            FichaBioma.executeUpdate("""delete from FichaBioma where ficha.id = ${params.sqFicha} and bioma.id not in (${params.ids})""")
            // adicionar novos Estados
            DadosApoio.executeQuery("from DadosApoio a where a.id in (${params.ids} ) and not exists ( from FichaBioma x where x.ficha.id=${params.sqFicha} and x.bioma.id = a.id )").each { bioma ->
                FichaBioma fb = new FichaBioma()
                fb.ficha = ficha
                fb.bioma = bioma
                fb.save()
            }
        } else {
            idsExcluidos = FichaBioma.executeQuery("select new map( a.id as id) from FichaBioma a where a.ficha.id = ${params.sqFicha}").id
            FichaBioma.executeUpdate("""delete from FichaBioma where ficha.id = ${params.sqFicha}""")
        }
        if( idsExcluidos ) {
            FichaRefBib.executeUpdate("delete from FichaRefBib where ficha.id=${ficha.id.toString()} and noTabela='ficha_bioma' and noColuna='sq_ficha_bioma' and sqRegistro in (${idsExcluidos.join(',')})")
        }

        //fichaService.atualizarPercentualPreenchimento( ficha )
        fichaService.updatePercentual(ficha)


        // Exibor alerta quando o Sistema Costeriro-Marinho for adiciona a um grupo não permitido
        def result = FichaBioma.executeQuery("""select count(bioma) from FichaBioma a where a.ficha.id=${params.sqFicha} and a.bioma.codigoSistema = 'MARINHO_COSTEIRO'""")
        boolean temBiomaMarinho = result[0] > 0

        /*
        FichaOcorrencia.findAllByFichaAndContexto(ficha, contexto).each { row ->
            if (row.bioma.codigoSistema == 'MARINHO_COSTEIRO') {
                temBiomaMarinho = true
            }
        }*/
        String msgBiomaMarinho=''
        if( temBiomaMarinho ) {
            if( ficha.grupo.codigoSistema =~ /^(COLEMBOLAS|ANFIBIOS|EFEMEROPTERAS|ODONATAS|ABELHAS|ARACNIDEOS|COLEMBOLAS|CEFALOPODAS|FORMIGAS|INVERTEBRADOS_TERRESTRES|BORBOLETAS|MARIPOSAS|MIRIAPODAS|OLIGOQUETAS|ONICOFORAS|MOLUSCOS_TERRESTRES|CARNIVOROS_TERRESTRES|CERVIDEOS|MARSUPIAIS|MORCEGOS|PERISSODACTILAS|PRIMATAS|ROEDORES|TAIASSUIDAES|XENARTRAS|CROCODILIANOS|LAGARTOS|ANFISBENAS|SERPENTES|LAGOMORFAS)$/){
                msgBiomaMarinho = 'O <b>Sistema Costeriro-Marinho</b> foi adicionado mas NÃO É COMPATÍVEL com espécies do grupo <b>'+ficha.grupo.descricao+'</b>.'
            }
        }
        return this._returnAjax(0, getMsg(1), 'success',[msgBiomaMarinho:msgBiomaMarinho])
    }

    def deleteDistBioma() {
		FichaBioma fichaBioma = FichaBioma.get(params.sqFichaOcorrencia)
		if( fichaBioma )
		{
			if( fichaBioma.ficha != Ficha.get( params.sqFicha ) )
			{
				return this._returnAjax(1, 'Registro não pertence a Ficha', "error");
			}
			try {
				fichaBioma.delete(flush:true)
				this._returnAjax(0, getMsg(4), "success")
			}
			catch(Exception e) {
				println e.getMessage()
				this._returnAjax(1, getMsg(51), "error")
			}
		} else {
			this._returnAjax(1, getMsg(6), "error")
		}
	}

	def selAllBioma() {
		params['sqBioma[]']=[]
		Ficha ficha 		= Ficha.get(params.sqFicha)
		DadosApoio contexto = dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA','BIOMA');
		// selecionar todos os biomas que não estiverm já incluídos
		//List biomasExistentes = FichaOcorrencia.findAllByFichaAndContexto(ficha,contexto);
		List biomasExistentes = FichaBioma.findAllByFicha( ficha );
		// carregar somente os biomas que ainda não foram adicionados
		DadosApoio.createCriteria().list()
		{
            eq('pai', DadosApoio.findAllByCodigo('TB_BIOMA')[0] )
        	if(biomasExistentes)
        	{
                not {
                    'in'('id',biomasExistentes.bioma.id)
                }
            }
        }.each{
			params['sqBioma[]'].push(it.id);
		}

		if( params['sqBioma[]'].size() > 0)
		{
			return this.saveFrmDisBioma();
		}
		return this._returnAjax(1, 'Nenhum Bioma Selecionado', "error");
	}

	def getGridDistBioma() {
		if( ! params.sqFicha ) {
			render ''
			return
		}
        // List listBiomas = fichaService.getBiomasOcorrencia( params.sqFicha.toLong() )

        List result = Ficha.executeQuery("""select new map(f.taxon.nivelTaxonomico.coNivelTaxonomico as coNivelTaxonomico) from Ficha f where f.id=:sqFicha""",[sqFicha:params.sqFicha.toLong()])
        boolean isSubespecie = result && result[0].coNivelTaxonomico=='SUBESPECIE'


        //NOVO-REGISTRO
        // exibir cada bioma com suas referencias sem duplicar os biomas
        List listBiomasFichaTodos = []
        fichaService.getBiomasFicha(params.sqFicha.toLong() ).each{ row ->
            Map item = listBiomasFichaTodos.find{
                it.no_bioma == row.no_bioma && it.no_contexto == row.no_contexto
            }
            if( ! item ){
                item = [no_contexto:row.no_contexto,sq_ficha_bioma:row.sq_ficha_bioma, no_bioma:row.no_bioma, co_nivel_taxonomico:row.co_nivel_taxonomico, json_ref_bibs:[] ]
                listBiomasFichaTodos.push(item )
            }
            if( row.json_ref_bibs ) {
                List listRefs = JSON.parse( row.json_ref_bibs )
                listRefs.each { ref ->
                    if( ! item.json_ref_bibs.contains( ref ) ) {
                        item.json_ref_bibs.push( ref )
                    }
                }
            }
        }
        List listBiomasFicha = listBiomasFichaTodos?.findAll{it.no_contexto=='ficha'}

        //NOVO-REGISTRO
        List listBiomasRegistros = listBiomasFichaTodos?.findAll{it.no_contexto=='ocorrencia'}
        render(template:'/ficha/distribuicao/divGridDistBioma', model:['listBiomasFicha':listBiomasFicha, listBiomasRegistros:listBiomasRegistros,isSubespecie: isSubespecie] )
	}

	// ############################################################################################################################################################################################# //
	// 	MODULO FICHA/DISTRIBUICAO/BACIA HIDROGRÁFICA																																				 //
	// ############################################################################################################################################################################################# //

	def tabBaciaHidrografica(){
		//List baciasHidrograficas = Abrangencia.findAllByTipoAbrangencia( dadosApoioService.getByCodigo('TB_TIPO_ABRANGENCIA','BACIA_HIDROGRAFICA'),['sort':'nuOrdem']);
		//List fichaOcorrenciaBacia = FichaOcorrencia.findAllByFichaAndContexto( Ficha.get(params.sqFicha), dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA','BACIA_HIDROGRAFICA'));

		// carregar somente os biomas que ainda não foram adicionados
		/*List bacias=DadosApoio.createCriteria().list() {
            eq('pai', DadosApoio.findAllByCodigo('TB_BACIA_HIDROGRAFICA')[0] )
        	if(fichaOcorrenciaBacia)
        	{
                not {
                    'in'('id',fichaOcorrenciaBacia.baciaHidrografica.id)
                }
            }
            order("ordem");
        }
        */

		//List baciasHidrograficas = Abrangencia.findAllByTipoAbrangencia(tipoAbrangencia);
		//List baciasHidrograficas = FichaOcorrencia.findAllByContexto( dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA','BACIA_HIDROGRAFICA'));
		render(template:"/ficha/distribuicao/formBaciaHidrografica", model:[] ) ;
	}

	def getTreeRecursiveBacia(DadosApoio apoio, Lista listExistentes, Lista baciasComShape ) {
        boolean existe = listExistentes.list.find{ FichaBacia fichaBacia -> fichaBacia?.bacia?.id == apoio.id };
        Map node = [id:apoio.id,title:apoio.descricaoCompleta,icon:false,folder:false,selected:existe];
        if( apoio.itens.size() > 0)
        {
            node.children = []
            apoio.itens.each {
                if( !baciasComShape || baciasComShape.list.contains( it.id) ) {
                    node.children.push( getTreeRecursiveBacia( it, listExistentes, baciasComShape ) )
                }
            }
        }
        return node;
   }

    def getTreeBacias() {

        List data =[]

        if( !params.sqFicha )
        {
            render data as JSON
            return;
        }

        // selecionar os usos já cadatrados para marcar os checks
		Ficha ficha 		= Ficha.get(params.sqFicha)
		//DadosApoio contexto = dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA','BACIA_HIDROGRAFICA' )
		//Lista listExistentes = new Lista( FichaOcorrencia.findAllByFichaAndContexto( ficha,contexto ) )
		Lista listExistentes = new Lista( FichaBacia.findAllByFicha( ficha ) )

        //DadosApoio apoioTbBacias = dadosApoioService.getByCodigo('TB_BACIA_HIDROGRAFICA')
        // listar as bacias que possuem shapefiles
        Lista baciasComShape = new Lista( DadosApoioGeo.findAll()?.dadosApoio?.id )

        List listBacias = dadosApoioService.getTable('TB_BACIA_HIDROGRAFICA')

        listBacias.each {
            data.push( getTreeRecursiveBacia( it , listExistentes, baciasComShape ) )
        }

        render data as JSON
    }

    def saveTreeBacia() {
        if (!params.sqFicha)
        {
            return this._returnAjax(1, 'ID da ficha não informado.', "error")
        }
        Ficha ficha 		= Ficha.get(params.sqFicha)
        /*
        List idsValidos = []
        if ( params.ids )
        {
            idsValidos = params.ids.split(',');
        }
		DadosApoio contexto = dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA','BACIA_HIDROGRAFICA' )
        List idsOcorrenciasExcluidas=[]
		FichaOcorrencia.findAllByFichaAndContexto( ficha, contexto ).each
		{ fichaOcorrencia ->
			if (!idsValidos.find { it.toInteger() == fichaOcorrencia.baciaHidrografica.id.toInteger() })
            {
                idsOcorrenciasExcluidas.push( fichaOcorrencia.id )
                fichaOcorrencia.delete(flush: true);
            } else
            {
                idsValidos.removeAll{ it.toInteger() == fichaOcorrencia.baciaHidrografica.id.toInteger() }
            }

		}
        if( idsOcorrenciasExcluidas ) {
            FichaRefBib.executeUpdate("delete from FichaRefBib where ficha.id=${ficha.id.toString()} and noTabela='ficha_ocorrencia' and noColuna='sq_bacia_hidrografica' and sqRegistro in (${idsOcorrenciasExcluidas.join(',')})")
        }

        // gravar os novos ids
        idsValidos.each {
           new FichaOcorrencia(ficha: ficha, contexto:contexto, baciaHidrografica: DadosApoio.get(it.toInteger())).save(flush: true)
        }
         */
        // NOVO-REGISTRO
        List idsExcluidos
        if( params.ids ) {
            // ler os ids excluidos para excluir as referencias bibliográficas relacionadas a Bacia
            idsExcluidos = FichaBacia.executeQuery("select new map( a.id as id) from FichaBacia a where a.ficha.id = ${params.sqFicha} and a.bacia.id not in (${params.ids})").id
            // exlcuir Bacias desmarcados na tela
            FichaBacia.executeUpdate("""delete from FichaBacia where ficha.id = ${params.sqFicha} and bacia.id not in (${params.ids})""")
            // adicionar novos Estados
            DadosApoio.executeQuery("from DadosApoio a where a.id in (${params.ids} ) and not exists ( from FichaBacia x where x.ficha.id=${params.sqFicha} and x.bacia.id = a.id )").each { bacia ->
                FichaBacia fb = new FichaBacia()
                fb.ficha = ficha
                fb.bacia = bacia
                fb.save()
            }
        } else {
            idsExcluidos = FichaBacia.executeQuery("select new map( a.id as id) from FichaBacia a where a.ficha.id = ${params.sqFicha}").id
            FichaBacia.executeUpdate("""delete from FichaBacia where ficha.id = ${params.sqFicha}""")
        }
        if( idsExcluidos ) {
            FichaRefBib.executeUpdate("delete from FichaRefBib where ficha.id=${ficha.id.toString()} and noTabela='ficha_bacia' and noColuna='sq_ficha_bacia' and sqRegistro in (${idsExcluidos.join(',')})")
        }


        // atualizar log da ficha, quem alterou
        ficha.dtAlteracao = new Date()
        ficha.save()

        return this._returnAjax(0, getMsg(1), 'success');
    }


	/*def getSubBacia() {
		BaciaHidrografica bacia = BaciaHidrografica.get(params.sqBacia)
		//List subBacias = BaciaHidrografica.findAllByPai(bacia);

		// ler as subbacias já cadastradas
  		List subBaciasCadastradas = FichaOcorrencia.findAllBySqBaciaHidrograficaIsNotNullAndSqFicha(Ficha.get(params.sqFicha))
		// listar somente as subacias que ainda não foram selecionadas
        List subBacias = BaciaHidrografica.createCriteria().list() {
            eq('pai',bacia)
            if(subBaciasCadastradas) {
                not {
	                    'in'('id',subBaciasCadastradas.sqBaciaHidrografica.id)
                }
            }
            order( "nome")
        }

		render subBacias as JSON
	}
	*/
	def saveFrmDisBaciaHidrografica() {
        // metodo não esta sendo utilizado, bacias estão sendo gravadas com saveTreeBacia()
        /*
		if( ! params.sqFicha )
		{
			return this._returnAjax(1, 'ID da ficha não informado', "error")
		}
		if( ! params['sqBacia[]'] )
		{
			return this._returnAjax(1, 'Bacia não selecionada', "error");
		}
		Ficha ficha = Ficha.get(params.sqFicha)
		DadosApoio contexto = dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA','BACIA_HIDROGRAFICA');
		List bacias=[];
		if( params['sqBacia[]'].getClass() == String)
		{
			bacias.push( params['sqBacia[]'] )
		}
		else
		{
			bacias=params['sqBacia[]'];
		}
		if( bacias.size() == 0)
		{
			return this._returnAjax(1, 'Nenhuma Bacia Selecionada', "error");
		}
		params.ficha = ficha
		params.contexto = contexto
		def  fichaOcExists;
		bacias.each{
			params.baciaHidrografica = DadosApoio.get( it )
			fichaOcExists = FichaOcorrencia.countByFichaAndBaciaHidrograficaAndContexto(params.ficha,params.baciaHidrografica,params.contexto);
			if( ! fichaOcExists ) {
				new FichaOcorrencia(params).save(flush:true);
			}
		}
		*/
		return this._returnAjax(0);
	}

	def deleteDistBaciaHidrografica() {
		FichaBacia fichaBacia = FichaBacia.get(params.sqFichaOcorrencia)
		if( fichaBacia ) {
			try {
				fichaBacia.delete(flush:true)
				this._returnAjax(0, getMsg(4), "success")
			}
			catch(Exception e) {
				println e.getMessage()
				this._returnAjax(1, getMsg(51), "error")
			}
		} else {
			this._returnAjax(1, getMsg(6), "error")
		}
	}

	def selAllBacia() {
		params['sqBacia[]']=[]
		Ficha ficha 		= Ficha.get(params.sqFicha)
		DadosApoio contexto = dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA','BACIA_HIDROGRAFICA');
		// selecionar todos as bacias que não estiverm já incluídos
		//List baciasExistentes = FichaOcorrencia.findAllByFichaAndContexto(ficha,contexto);
		List baciasExistentes = FichaBacia.findAllByFicha( ficha );
		// carregar somente as bacias que ainda não foram adicionados
		DadosApoio.createCriteria().list()
		{
            eq('pai', DadosApoio.findAllByCodigo('TB_BACIA_HIDROGRAFICA')[0] )
        	if(baciasExistentes)
        	{
                not {
                    'in'('id',baciasExistentes.bacia.id)
                }
            }
            order( "ordem")
        }.each{
			params['sqBacia[]'].push(it.id);
		}

		if( params['sqBacia[]'].size() > 0)
		{
			return this.saveFrmDisBaciaHidrografica();
		}
		return this._returnAjax(1, 'Nenhum Bacia Selecionado', "error");
	}

	def getGridDistBaciaHidrografica() {
		if( ! params.sqFicha ) {
			render ''
			return
		}

        /*// lista as ufs da espécie e subespecies
        VwFicha vwFicha = VwFicha.get( params.sqFicha.toLong() )
        List fichasIds = Ficha.executeQuery("select new map( ficha.id as id ) from Ficha ficha " +
                " inner join ficha.taxon taxon"+
                " where ficha.id= :id or taxon.pai.id = :sqTaxon",
                [ id:vwFicha.id, sqTaxon:vwFicha.sqTaxon.toLong() ] )?.id
        DadosApoio contexto = dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA','BIOMA');
        List listBacias = FichaOcorrencia.createCriteria().list {
            'in'('vwFicha.id', fichasIds)
            isNotNull('baciaHidrografica' )
        }.unique{ it.baciaHidrografica }
        */
        //List listBacias = fichaService.getBaciasOcorrencia( params.sqFicha.toLong() )
        /*List listBacias = fichaService.getBaciasFicha( params.sqFicha.toLong() )
        listBacias.each {
            it.html_ref_bib = Util.parseRefBib2Grid( it.json_ref_bib.toString() )
        }*/
        List result = Ficha.executeQuery("""select new map(f.taxon.nivelTaxonomico.coNivelTaxonomico as coNivelTaxonomico) from Ficha f where f.id=:sqFicha""",[sqFicha:params.sqFicha.toLong()])
        boolean isSubespecie = result && result[0].coNivelTaxonomico=='SUBESPECIE'

        //NOVO-REGISTRO


        // exibir cada bacia com suas referencias sem duplicar a bacia
        List listBaciasFichaTodas=[]
        fichaService.getBaciasHidrograficasFicha(params.sqFicha.toLong()).each {row ->
            //String refBib = Util.parseRefBib2Grid( row?.json_ref_bibs?.toString())
            Map item = listBaciasFichaTodas.find{
                it.no_bacia == row.no_bacia && it.no_contexto == row.no_contexto
            }
            if( ! item ){
                item = [no_contexto:row.no_contexto, sq_ficha_bacia:row.sq_ficha_bacia, no_bacia:row.no_bacia, cd_bacia:row.cd_bacia, co_nivel_taxonomico:row.co_nivel_taxonomico, tx_trilha:row.tx_trilha, json_ref_bibs:[] ]
                listBaciasFichaTodas.push(item )
            }
            if( row.json_ref_bibs ) {
                List listRefs = JSON.parse( row.json_ref_bibs )
                listRefs.each { ref ->
                    if( ! item.json_ref_bibs.contains( ref ) ) {
                        item.json_ref_bibs.push( ref )
                    }
                }
            }
        }

        List listBaciasFicha = listBaciasFichaTodas.findAll{ it.no_contexto=='ficha'}
        List listBaciasRegistros = listBaciasFichaTodas.findAll{ it.no_contexto=='ocorrencia'}

        // NOVO-REGISTRO
        //List listBaciasRegistros = fichaService.getBaciasRegistro(params.sqFicha.toLong())
        //List listBacias = FichaOcorrencia.findAllByFichaAndBaciaHidrograficaIsNotNull(Ficha.get(params.sqFicha),[sort: "baciaHidrografica.ordem"])
		render(template:'/ficha/distribuicao/divGridDistBaciaHidrografica', model:['listBaciasFicha':listBaciasFicha, listBaciasRegistros:listBaciasRegistros,isSubespecie: isSubespecie]);
	}

	// ############################################################################################################################################################################################# //
	// 	MODULO FICHA/DISTRIBUICAO/AMBIENTE RESTRITO 																																				 //
	// ############################################################################################################################################################################################# //

	def tabAmbienteRestrito()
	{
		if( !params.sqFicha )
		{
			render 'Id da ficha não informado!';
			return;
		}
        List listHabitats = dadosApoioService.getTable('TB_RESTRICAO_HABITAT').sort{it.descricao }
        render(template:"/ficha/distribuicao/formAmbienteRestrito", model:['listHabitats': listHabitats]);
	}

    def saveFrmDisAmbienteRestrito() {
    	 //println params;
		 //return this._returnAjax(1, 'ver parametros....', "success");

		if( !params.sqFicha )
		{
			return this._returnAjax(1, 'ID ficha não informado!', "error");
		}

		if( !params.sqRestricaoHabitat )
		{
			return this._returnAjax(1, 'Ambiente restrito não selecionado!', "error");
		}

		Ficha ficha = Ficha.get(params.sqFicha)
        DadosApoio resHab = DadosApoio.get(params.sqRestricaoHabitat)
		FichaAmbienteRestrito fichaAmbienteRestrito;

		if( params.sqFichaAmbienteRestrito )
		{
			fichaAmbienteRestrito = FichaAmbienteRestrito.get( params.sqFichaAmbienteRestrito );
		}
		else
		{
			params.sqFichaAmbienteRestrito = 0;
			fichaAmbienteRestrito = new FichaAmbienteRestrito();
			fichaAmbienteRestrito.ficha	= ficha;
		}

		Integer fichaAmbResExists = FichaAmbienteRestrito.countByFichaAndRestricaoHabitatAndIdNotEqual(ficha, resHab, params.sqFichaAmbienteRestrito );
		if( fichaAmbResExists == 0 ) {
            fichaAmbienteRestrito.restricaoHabitat       = resHab;
            fichaAmbienteRestrito.txFichaRestricaoHabitat= params.txFichaRestricaoHabitat;
            fichaAmbienteRestrito.deRestricaoHabitatOutro= params.deRestricaoHabitatOutro;
            fichaAmbienteRestrito.save(flush: true)

            // atualizar log da ficha, quem alterou
            ficha.dtAlteracao = new Date()
            ficha.save()

            return this._returnAjax(0, getMsg(1), "success")
		}
		else
		{
			return this._returnAjax(1, 'Ambiente Já Cadastrado!', "error")
		}
	}

	def editDisAmbienteRestrito()
	{

    	FichaAmbienteRestrito reg = FichaAmbienteRestrito.get(params.id);
		if( reg?.id )
		{
			render reg.asJson();
            return;
		}
		render [:] as JSON;
	}

	def deleteDisAmbienteRestrito() {
		FichaAmbienteRestrito fichaAmbRes = FichaAmbienteRestrito.get(params.sqFichaAmbienteRestrito)
		if( fichaAmbRes ) {
			try {
                Ficha ficha = fichaAmbRes.ficha
				fichaAmbRes.delete(flush:true)

                // atualizar log da ficha, quem alterou
                ficha.dtAlteracao = new Date()
                ficha.save()

                this._returnAjax(0, getMsg(4), "success")
			}
			catch( Exception e ) {
				println e.getMessage()
				this._returnAjax(1, getMsg(51), "error")
			}
		} else {
			this._returnAjax(1, getMsg(6), "error")
		}
	}

	def getGridDistAmbienteRestrito() {
		if( ! params.sqFicha ) {
			render ''
			return
		}
		List listAmbientes = FichaAmbienteRestrito.findAllByFicha(Ficha.get(params.sqFicha))
        listAmbientes.sort{ it.restricaoHabitat.ordem}
        render(template:'/ficha/distribuicao/divGridDistAmbienteRestrito', model:['listAmbientes':listAmbientes] );
	}

    def getGridSelecionarCaverna(){
        // https://salve.icmbio.gov.br/salve-api/api/searchCaverna?sgEstado=GO
        String urlApiSalve = 'https://salve.icmbio.gov.br/salve-api/api/searchCaverna/'
        List urlParams=[]
        if( params.sqEstadoFiltro ){
            Estado estado = Estado.get( params.sqEstadoFiltro.toLong() )
            urlParams.push('sgEstado='+estado.sgEstado)
        }
        if( params.noCavernaFiltro ){
            urlParams.push('noCaverna='+params.noCavernaFiltro)
        }

        if( ! urlParams ){
            render 'Informe pelo menos um parâmetro para pesquisar'
            return;
        }
        urlApiSalve += '?'+urlParams.join('&')
        JSONElement resposta = requestService.doGetJson(urlApiSalve)

        // atualizar a tabela local com as cavernas
        _updateLocalTableCavernas(resposta.data )

        render(template:'/ficha/distribuicao/divGridSelecionarCaverna', model:['listCavernas':resposta.data] );
    }

    def saveCavernas(){
        Map res = [ status:0, msg:'', type:'success', erros:[] ]
        try {
            if( !params.sqFicha ){
                res.erros.push('Id da ficha não informado')
            }
            if( !params.sqFichaAmbienteRestrito ){
                res.erros.push('Id do ambiente não informado')
            }
            if( !params.idsCavernas ){
                res.erros.push('Nenhuma caverna selecionada')
            }
            if( res.erros ){
                throw new Exception( res.erros.join('<br>') )
            }
            FichaAmbienteRestrito far = FichaAmbienteRestrito.get(params.sqFichaAmbienteRestrito.toLong() )
            if( !far ){
                throw new Exception( 'Ambiente restrito informado não existe' )
            }
            if( far.ficha.id.toLong() != params.sqFicha.toLong() ) {
                throw new Exception( 'Id da ficha incorreto' )
            }
            List idsExistentes = far?.cavernas?.caverna?.id
            params.idsCavernas.split(',').each{ sqCaverna ->
                if( !idsExistentes.contains(sqCaverna.toLong() ) ){
                    Caverna caverna = Caverna.get( sqCaverna.toLong() )
                    if( caverna ){
                        new FichaAmbRestritoCaverna(fichaAmbienteRestrito: far, caverna: caverna).save()
                    }
                }
            }
            res.msg='Gravação realizada com SUCESSO!'
        } catch( Exception e ){
            res.msg = e.getMessage()
            res.status = 1
            res.type='error'
            res.errors =[]
            println e.getMessage()
        }
        render res as JSON
    }

    def deleteCaverna(){
        Map res = [ status:0, msg:'', type:'success', erros:[] ]
        try {
            if( !params.sqFicha ){
                res.erros.push('Id da ficha não informado')
            }

            if( !params.sqFichaAmbRestritoCaverna ){
                res.erros.push('Id caverna não informado')
            }

            if( res.erros ){
                throw new Exception( res.erros.join('<br>') )
            }

            FichaAmbRestritoCaverna farc = FichaAmbRestritoCaverna.get(params.sqFichaAmbRestritoCaverna.toLong() )
            if( !farc ){
                throw new Exception( 'Caverna informada não existe' )
            }

            if( farc.fichaAmbienteRestrito.ficha.id.toLong() != params.sqFicha.toLong() ) {
                throw new Exception( 'Id da ficha incorreto' )
            }
            farc.delete()
            res.msg='Exclusão realizada com SUCESSO!'
        } catch( Exception e ){
            res.msg = e.getMessage()
            res.status = 1
            res.type='error'
            res.errors =[]
            println e.getMessage()
        }
        render res as JSON
    }


	// ############################################################################################################################################################################################# //
	// 	MODULO FICHA/DISTRIBUICAO/AREA RELEVANTE																																					 //
	// ############################################################################################################################################################################################# //

	def tabAreaRelevante()
	{
		List arRels = dadosApoioService.getTable('TB_AREAS_RELEVANTES').sort{ it.descricao }
		List estados = []
		// exibir somente os estados da aba 2.1 - UF
        List ufsFicha = fichaService.getUfsFicha( params.sqFicha.toLong() )
        ufsFicha.each{ row ->
            if( row.st_utilizado_avalicao ){
                estados.push([ id:row.sq_estado, noEstado:row.no_estado] )
            }
        }
        estados.sort{ it.noEstado };
		/*FichaOcorrencia.findAllByFichaAndContexto(Ficha.get(params.sqFicha), dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA','ESTADO')).each{
			estados.push( it.estado);
		}*/

		List fichaArRelMuns = FichaAreaRelevanciaMunicipio.createCriteria().list() {
			fichaAreaRelevancia {
				eq('ficha', Ficha.get(params.sqFicha))
			}
		}.sort{it.municipio.noMunicipio}
		render(template:"/ficha/distribuicao/formAreaRelevante",
			model:[
					'arRels'			: arRels,
					'estados'			: estados,
					'fichaArRelMuns' 	: fichaArRelMuns
				]);
	}

	def editAreaRelevante()
	{
    	FichaAreaRelevancia reg = FichaAreaRelevancia.get(params.id);
		if( reg?.id )
		{
			render reg.asJson();
            return;
		}
		render [:] as JSON;
	}

	def saveFrmDisAreaRelevante() {
		if( !params.sqFicha )
		{
			return this._returnAjax(1, 'Id da ficha não informado!', "error");
		}
		if( !params.sqEstado )
		{
			return this._returnAjax(1, 'Estado não selecionado!', "error");
		}
		if( !params.sqTipoRelevancia )
		{
			return this._returnAjax(1, 'Área Relevante não selecionada!', "error");
		}
		Ficha ficha 				= Ficha.get(params.sqFicha)
		Estado estado 				= Estado.get(params.sqEstado)
		DadosApoio tipoRelevancia 	= DadosApoio.get(params.sqTipoRelevancia)
		FichaAreaRelevancia fichaAreaRelevancia;
        if( ficha && estado && tipoRelevancia ) {
			params.ficha = ficha
			params.estado = estado
			params.tipoRelevancia = tipoRelevancia
		}
		if( params.sqFichaAreaRelevante)
		{
			fichaAreaRelevancia = FichaAreaRelevancia.get( params.sqFichaAreaRelevante )
		}
		else
		{
			fichaAreaRelevancia = new FichaAreaRelevancia()
		}

        fichaAreaRelevancia.properties = params;

        // sempre será postado o campo ucControle e a gravação nos campos ucFederal, ucEstadual e rppn está sendo fienta na model (FichaAreaRelevante.groovy)
        if( params.sqControle )
        {
            fichaAreaRelevancia.ucControle = Uc.get(params.sqControle.toInteger());
            fichaAreaRelevancia.txLocal =null;
        }



		// validar campo obrigatorio
		List errors = [];
		if( ! fichaAreaRelevancia.validate() ) {
			fichaOcorrencia.errors.allErrors.each {
				errors.add( it.arguments[3] )
			}
		}
		if ( errors ) {
			return this._returnAjax(1, errors.join('<br>'), "error");
		}

		fichaAreaRelevancia.save(flush: true);

        // atualizar log da ficha, quem alterou
        ficha.dtAlteracao = new Date()
        ficha.save()

        return this._returnAjax(0, getMsg(1), "success");
	}

	def deleteDisAreaRelevante() {

		if( !params.sqFichaAreaRelevancia)
		{
			return this._returnAjax(1, 'Id não informado', "error")
		}
		FichaAreaRelevancia fichaAreaRelevancia = FichaAreaRelevancia.get(params.sqFichaAreaRelevancia)
		if( fichaAreaRelevancia ) {
			try {
				List fichaAreaRelevanciaMunicipio = FichaAreaRelevanciaMunicipio.findAllByFichaAreaRelevancia(fichaAreaRelevancia)
				if( fichaAreaRelevanciaMunicipio ) {
					try {
						fichaAreaRelevanciaMunicipio.each{
							it.delete(flush: true)
						}
					}
					catch(Exception e) {
						println e.getMessage()
						this._returnAjax(1, getMsg(51), "error")
					}
				}

                Ficha ficha = fichaAreaRelevancia.ficha

				fichaAreaRelevancia.delete(flush: true)
                // atualizar log da ficha, quem alterou
                ficha.dtAlteracao = new Date()
                ficha.save()
				this._returnAjax(0, getMsg(4), "success")
			}
			catch(Exception e) {
				println e.getMessage()
				this._returnAjax(1, getMsg(51), "error")
			}
		} else {
			this._returnAjax(1, getMsg(6), "error")
		}
	}

	def getGridDistAreaRelevante() {
		if( ! params.sqFicha ) {
			render ''
			return
		}
		List listAreas = FichaAreaRelevancia.findAllByFicha(Ficha.get(params.sqFicha));
		render(template:'/ficha/distribuicao/divGridDistAreaRelevante', model: ['listAreas': listAreas]);
	}

	// municípios
	//
	/*def openModalSelMunicipio() {
		FichaAreaRelevancia fichaAreaRelevancia = FichaAreaRelevancia.get(params.sqFichaAreaRelevancia)
		render(template:'/ficha/distribuicao/modal/modalSelMunicipio', model:['fichaArRel': fichaArRel])
	}
	*/
	def saveFrmMunicipio() {
		//println params;
		//return 	this._returnAjax(1, 'Verificar parametros...', "success");

		if( ! params.sqMunicipio )
		{
			return 	this._returnAjax(1, 'Município não selecionado!', "error");
		}
		if( ! params.sqFichaAreaRelevancia )
		{
			return 	this._returnAjax(1, 'Id da área de relevância não informado!', "error");
		}

		FichaAreaRelevancia fichaAreaRelevancia = FichaAreaRelevancia.get(params.sqFichaAreaRelevancia);
		Municipio municipio = Municipio.get(params.sqMunicipio)
		if( fichaAreaRelevancia && municipio ) {
			params.fichaAreaRelevancia = fichaAreaRelevancia
			params.municipio = municipio
		}
		Integer fichaArRelMunExist = FichaAreaRelevanciaMunicipio.countByFichaAreaRelevanciaAndMunicipio(params.fichaAreaRelevancia, params.municipio);
		if( ! fichaArRelMunExist ) {
			FichaAreaRelevanciaMunicipio fichaArRemMun = new FichaAreaRelevanciaMunicipio(params)
			if( fichaArRemMun.save(flush: true) ) {
				this._returnAjax(0, getMsg(1), "success")
			} else {
				if( ! fichaArRemMun.validate() ) {
					fichaArRemMun.errors.allErrors.each {
						println it
						this._returnAjax(1, getMsg(2), "error")
					}
				}
			}
		} else {
			this._returnAjax(1, getMsg(3), "error")
		}
	}

	def deleteMunicipioAreaRelevante() {
		FichaAreaRelevanciaMunicipio fichaArRelMun = FichaAreaRelevanciaMunicipio.get(params.sqFichaAreaRelevanciaMunicipio)
		if( fichaArRelMun ) {
			try {
				fichaArRelMun.delete(flush:true)
				this._returnAjax(0, getMsg(4), "success")
			}
			catch(Exception e) {
				println e.getMessage()
				this._returnAjax(1, getMsg(51), "error")
			}
		} else {
			this._returnAjax(1, getMsg(6), "error")
		}
	}

	def getGridMunicipiosAreaRelevante() {
		if( ! params.sqFichaAreaRelevancia ) {
			render 'id da area de relevância não informado!'
			return
		}
		List listMunicipios = FichaAreaRelevanciaMunicipio.findAllByFichaAreaRelevancia(FichaAreaRelevancia.get(params.sqFichaAreaRelevancia))
		render(template:'/ficha/distribuicao/divGridMunicipiosAreaRelevante', model:['listMunicipios':listMunicipios])
	}

    // ############################################################################################################################################################################################# //
    // 	MODULO FICHA/DISTRIBUICAO/EOO / AOO
    // ############################################################################################################################################################################################# //

    def tabEooAoo()
    {
        if( ! params.sqFicha ) {
            render 'id da ficha não informado!'
            return
        }
        Ficha ficha = Ficha.get(params.sqFicha.toLong() )
        render(template:"/ficha/distribuicao/formEooAoo",
            model: [
                ficha:ficha
            ]);
    }

    def saveFrmDisEooAoo() {
        if( ! params.sqFicha ) {
            return 	this._returnAjax(1, 'Ficha não informada!', "error");
        }

        Ficha ficha = Ficha.get(params.sqFicha.toLong() )
        params.dsJustificativaEoo = Util.trimEditor(params.dsJustificativaEoo)
        params.dsJustificativaAoo = Util.trimEditor(params.dsJustificativaAoo)
        ficha.properties = params


        // atualizar log da ficha, quem alterou
        ficha.dtAlteracao = new Date()
        ficha.save()

        return this._returnAjax(0, getMsg(1), "success");
    }

    /**
     * Atualizar a tabela Cavernas com a base de dados do CANIE
     * com o resultado da pesquisa
     * @param cavernas
     * @return
     */
    private _updateLocalTableCavernas(List cavernas= [] ) {
        runAsync {
            Caverna cav
            cavernas.each { row ->
                cav = Caverna.get( row.sq_caverna.toLong() )
                if ( ! cav ) {
                    cav = new Caverna()
                    cav.id = row.sq_caverna.toLong()
                }
                cav.noCaverna = row.no_caverna
                cav.noEstado = row.no_estado
                cav.noMunicipio = row.no_municipio
                cav.noLocalidade = row.no_localidade ?: row.no_municipio
                cav.coRegistroNacional = row.co_registro_nacional?:null
                cav.save()
            }
       }
    }
}
