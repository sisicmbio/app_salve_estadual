package br.gov.icmbio
import grails.converters.JSON

import static groovy.io.FileType.FILES;

class CicloAvaliacaoController extends BaseController {

    private String _cacheDir() {
        return grailsApplication?.config?.cache?.dir ?:'/data/salve-estadual/temp/'
    }
    private void _clearCache() {
        def dir = new File( _cacheDir() )
        def files = []
        if( dir.isDirectory() ) {
            dir.traverse(type: FILES, maxDepth: 0) { files.add(it) }
            files.each {
                if (it.toString() =~ /public-.+\.cache$/) {
                    if (!it.delete()) {
                        println 'erro ao excluir o arquivo ' + it.toString()
                    }
                }
            }
            println 'Cache publico limpo!'
        }
        
    }

    // exibir o formulário inicial com a lista de registros em forma de gride
    def index() {
    	render (view:'index');
    }

    def getGrid()
    {
        List listCiclos = CicloAvaliacao.list(sort:'nuAno',order:'desc');
        render( template:'gride',model:[listCiclos:listCiclos])
    }

    def save() {
        def cicloAvaliacao;
        Integer qtdAbertos=0;
        /*
        if(params.id) {
            cicloAvaliacao = CicloAvaliacao.get(params.id)
             // na edição, não pode alterar a situação para Aberto se existir outro em aberto
            if( params?.situacao == 'A')
            {
                qtdAbertos = CicloAvaliacao.countBySituacaoAndIdNotEqual('A',cicloAvaliacao.id);
                if( qtdAbertos > 0)
                {
                    return this._returnAjax(1, "Existe um outro ciclo em aberto!", "error")
                }
            }
        } else {
            cicloAvaliacao = new CicloAvaliacao()
            // só pode haver um ciclo aberto ao mesmo tempo
            if( params?.situacao == 'A')
            {
                qtdAbertos = CicloAvaliacao.countBySituacao('A')
                if( qtdAbertos > 0)
                {
                    return this._returnAjax(1, "Existe ciclo em aberto!", "error")
                }
            }
            // o ano deve ser maior que o ultimo cadastrado + 5 anos
            List ultimoAno = CicloAvaliacao.withCriteria {
            projections{
                max 'ano'
                }
            }
            if( ultimoAno[0] && params.int('ano') < ultimoAno[0] + 5 )
            {
                return this._returnAjax(1, "Ano deve ser maior ou igual a " + (ultimoAno[0]+5).toString(), "error")
            }
        }
        */

        //não permitir gravar se o periodo do ciclo estiver em choque com outro
        /*regras:
        - o periódo deve ser de 4 anos
        - a ano inicial não pode estar em choque com outro ciclo
        - a nao final não pode estar em choque com outro ciclo
        */
        String cicloEmChoque=''
        CicloAvaliacao.createCriteria().list {
            if( params.sqCicloAvaliacao )
            {
                ne('id', params.sqCicloAvaliacao.toLong())
            }
        }.each{
            if( !cicloEmChoque && params.nuAno.toInteger()>= it.nuAno.toInteger() && params.nuAno.toInteger() <= it.nuAno.toInteger() + 4 )
            {
               cicloEmChoque = 'O periódo está em choque com o ciclo '+it.nuAno.toString() + ' a '+ (it.nuAno.toInteger() + 4)+'.'
            }
        }
        if( cicloEmChoque )
        {
            return this._returnAjax(1, cicloEmChoque, "error")
        }

        if(params.sqCicloAvaliacao)
        {
            cicloAvaliacao = CicloAvaliacao.get(params.sqCicloAvaliacao)
        } else {
            cicloAvaliacao = new CicloAvaliacao()
        }

        cicloAvaliacao.nuAno            = params.int('nuAno')
        cicloAvaliacao.deCicloAvaliacao = params.deCicloAvaliacao
        cicloAvaliacao.inSituacao       = params.inSituacao
        cicloAvaliacao.inOficina        = params.inOficina
        cicloAvaliacao.inPublico        = params.inPublico ?: 'N'
        try {
                cicloAvaliacao.save(flush:true)
                _clearCache() // limpar o cache do módulo público
                this._returnAjax(0, "Registro salvo com sucesso!", "success")
            }
            catch(Exception e) {
                this._returnAjax(1, "Não foi possível gravar o registro!", "error")
            }
    }

    def edit()
    {
        if( !params.id)
        {
                return this._returnAjax(1, "ID ciclo não informado!", "error")
        }
        def reg = CicloAvaliacao.get(params.id)
        if( reg?.id )
        {
            render reg.asJson();
            return;
        }
        render [:] as JSON;
    }

    def delete()
    {
        def cicloAvaliacao = CicloAvaliacao.get(params.id)
        if(cicloAvaliacao) {
            try {
                cicloAvaliacao.delete(flush:true)
                this._returnAjax(0, "Registro excluído com sucesso!", "success")
            }
            catch(Exception e) {
                this._returnAjax(0, "Não foi possível excluir o registro!", "error")
            }
        }
    }
}
