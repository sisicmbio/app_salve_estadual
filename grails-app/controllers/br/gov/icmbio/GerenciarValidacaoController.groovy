package br.gov.icmbio

import grails.converters.JSON
import org.codehaus.groovy.grails.web.json.JSONObject
import org.codehaus.groovy.grails.web.servlet.mvc.GrailsHttpSession

class GerenciarValidacaoController extends BaseController {

    def dadosApoioService
    def fichaService
    def oficinaService
    def emailService
    def sqlService
    def exportDataService

    def index() {

        //List listCiclosAvaliacao = CicloAvaliacao.list(sort: 'nuAno', order: 'desc')
        List listCiclosAvaliacao = [ session.getAttribute('ciclo') ]//CicloAvaliacao.findAllByInOficina('S', [sort: 'nuAno', order: 'desc'])
        List listTipoOficina    = dadosApoioService.getTable('TB_TIPO_OFICINA').findAll{
            return ( ( it.codigoSistema ==~ /OFICINA_VALIDACAO/ ) )
        }
        // filtrar a localidade da oficina para perfil não adm
        List listUnidade = []
        if( session.sicae.user.sqUnidadeOrg ) {
            listUnidade = dadosApoioService.getCentros().findAll {
                return session.sicae.user.isADM() || it.id.toInteger() == session.sicae.user.sqUnidadeOrg.toInteger()
            }
        }
        List listFonteRecurso = dadosApoioService.getTable( 'TB_FONTE_RECURSO' )
        List listSituacaoOficina= dadosApoioService.getTable('TB_SITUACAO_OFICINA')

        render('view': 'index', model: [listCiclosAvaliacao: listCiclosAvaliacao
                                        , listTipoOficina:listTipoOficina
                                        , listUnidade:listUnidade
                                        , listSituacaoOficina:listSituacaoOficina
                                        , listFonteRecurso: listFonteRecurso
        ])
    }


    /**
     * metodo para listar as oficinas de acordo com perfil do usuário logado
     * @param sqCicloAvaliacao
     * @return
     */
    protected Map _getOficinasUsuario( Long sqCicloAvaliacao = 0
                                    , String nmCientificoFiltro = ''
                                    , Long sqSituacaoFiltro = null
                                    , String noOficinaFiltro = ''
                                    , String deLocalFiltro = ''

    ) {
        // listar as oficinas de VALIDACAO do ciclo
        DadosApoio tipoOficina = dadosApoioService.getByCodigo('TB_TIPO_OFICINA', 'OFICINA_VALIDACAO')
        Map pars = [:]
        if ( ! session.sicae.user.isADM() ) {
            if( session.sicae.user.sqUnidadeOrg ) {
                pars.sqUnidadeOrg = session.sicae.user.sqUnidadeOrg
            } else {
                if( session.sicae.user.isCT() ) {
                    DadosApoio papelCT = dadosApoioService.getByCodigo('TB_PAPEL_FICHA', 'COORDENADOR_TAXON')
                    pars.sqPapel = ( papelCT ? papelCT.id : 0 )
                }
                pars.sqPessoa = session.sicae.user.sqPessoa
            }
        }
        if( sqSituacaoFiltro ){
            pars.sqSituacao = sqSituacaoFiltro
        }

        pars.sqTipoOficina      = tipoOficina.id
        pars.sqCicloAvaliacao   = sqCicloAvaliacao

        String cmdSql = """SELECT a.sq_oficina
                         , a.no_oficina
                         , a.sg_oficina
                         , a.dt_inicio
                         , a.dt_fim
                         , a.de_local
                         , a.sq_situacao
                         , situacao.cd_sistema as cd_situacao_sistema
                         , situacao.ds_dados_apoio as ds_situacao
                         , concat( to_char(a.dt_inicio,'dd/mm/yyyy'),' a ', to_char(a.dt_fim,'dd/mm/yyyy') ) as de_periodo
                         , fichas.nu_fichas
                    FROM salve.oficina a
                    INNER JOIN salve.dados_apoio as situacao on situacao.sq_dados_apoio = a.sq_situacao
                    LEFT JOIN LATERAL (
                        select count(x.sq_oficina_ficha) as nu_fichas
                        ${nmCientificoFiltro ? ", sum( case when ficha.nm_cientifico ilike '%${nmCientificoFiltro}%' then 1 else 0 end) as nu_fichas_filtro":''}
                        ${pars.sqUnidadeOrg  ? ", sum( case when ficha.sq_unidade_org = :sqUnidadeOrg then 1 else 0 end) as nu_fichas_unidade_filtro":''}
                        ${pars.sqPapel       ? ", sum( case when ficha_pessoa.sq_papel = :sqPapel then 1 else 0 end) as as nu_fichas_papel_filtro":''}
                        from salve.oficina_ficha x
                        ${nmCientificoFiltro ? 'inner join salve.ficha on ficha.sq_ficha = x.sq_ficha':''}
                        where x.sq_oficina = a.sq_oficina
                        ) AS fichas on true
                    where a.sq_tipo_oficina = :sqTipoOficina
                      and a.sq_ciclo_avaliacao = :sqCicloAvaliacao
                      ${ pars.sqSituacao   ? "AND a.sq_situacao = :sqSituacao" :''}
                      ${nmCientificoFiltro ? 'AND fichas.nu_fichas_filtro > 0 ':''}
                      ${pars.sqUnidadeOrg  ? 'AND fichas.nu_fichas_unidade_filtro > 0':''}
                      ${pars.sqPapel       ? 'AND fichas.nu_fichas_papel_filtro > 0':''}
                      ${ noOficinaFiltro   ? "AND (a.no_oficina ilike '%${noOficinaFiltro}%' or a.sg_oficina ilike '%${noOficinaFiltro}%')" :''}
                      ${ deLocalFiltro     ? "AND a.de_local ilike '%${deLocalFiltro}%'" :''}
                    """
        cmdSql+='\nORDER BY a.dt_inicio desc'
        /** /
        println ' '
        println ' '
        println cmdSql
        println pars
        /**/
        return sqlService.paginate(cmdSql,pars,params,5,10,0)
    }

    /**
     * metodo para listar as oficinas de acordo com perfil do usuário logado
     * @param sqCicloAvaliacao
     * @return
     */
    /*protected List _getOficinasUsuario( Long sqCicloAvaliacao = 0,String nmCientificoFiltro = '' ) {
        // listar as oficinas de VALIDACAO do ciclo
        CicloAvaliacao cicloAvaliacao = CicloAvaliacao.get( sqCicloAvaliacao )
        DadosApoio tipoOficina = dadosApoioService.getByCodigo('TB_TIPO_OFICINA', 'OFICINA_VALIDACAO')
        Map pars = [:]
        if ( ! session.sicae.user.isADM() ) {
            if( session.sicae.user.sqUnidadeOrg ) {
                pars.sqUnidadeOrg = session.sicae.user.sqUnidadeOrg
            } else {
                if( session.sicae.user.isCT() ) {
                    DadosApoio papelCT = dadosApoioService.getByCodigo('TB_PAPEL_FICHA', 'COORDENADOR_TAXON')
                    pars.sqPapel = ( papelCT ? papelCT.id : 0 )
                }
                pars.sqPessoa = session.sicae.user.sqPessoa
            }
        }
        String cmdSql = """SELECT DISTINCT a.sq_oficina, a.sg_oficina, a.dt_inicio
                  ,situacao.cd_sistema as cd_situacao_sistema
                  ,situacao.ds_dados_apoio as ds_situacao
                  ,concat( to_char(a.dt_inicio,'dd/mm/yyyy'),' a ', to_char(a.dt_fim,'dd/mm/yyyy') ) as de_periodo
                  FROM salve.oficina a
                  INNER JOIN salve.dados_apoio as situacao on situacao.sq_dados_apoio = a.sq_situacao
                  LEFT JOIN salve.oficina_ficha b on b.sq_oficina = a.sq_oficina
                  LEFT JOIN salve.ficha c on c.sq_ficha = b.sq_ficha

                  ${pars.sqPessoa ? 'INNER JOIN salve.ficha_pessoa d on d.sq_ficha = b.sq_ficha' : '' }
                  WHERE a.sq_ciclo_avaliacao = :sqCicloAvaliacao
                    AND a.sq_tipo_oficina = :sqTipoOficina
                    ${ pars.sqUnidadeOrg ? 'AND c.sq_unidade_org = :sqUnidadeOrg' :''}
                    ${ pars.sqPessoa ? "AND d.sq_pessoa = :sqPessoa AND d.in_ativo='S'" :''}
                    ${ pars.sqPapel ? "AND d.sq_papel = :sqPapel" :''}
                    ${ nmCientificoFiltro ? "AND c.nm_cientifico ilike '%${nmCientificoFiltro}%'" :''}
                """
        pars.sqTipoOficina = tipoOficina.id
        pars.sqCicloAvaliacao = cicloAvaliacao.id
        cmdSql+='\nORDER BY a.dt_inicio desc'
     return sqlService.execSql( cmdSql, pars )
    }*/


    /**
     * metodo para validar se uma ficha pode ser atribuida a um determinado validador
     * @param sqValidadores
     * @param sqFichas
     * @return Map [fichasValidadorInvalidas : [:], papeis:[] ]
     */
    protected Map _validarFichasValidador( List sqPessoas, List sqOficinaFichas ) {
        Map result = [fichasValidadorInvalidas : [:], papeis:[] ]
        // ler os CTs, PFs e Facilitadores das fichas selecionadas
        DadosApoio oficinaTipoAvaliacao = dadosApoioService.getByCodigo('TB_TIPO_OFICINA', 'OFICINA_AVALIACAO')
        String cmdSql = """select oficina_ficha.sq_ficha
                             , pf.sq_pessoa
                             , pf.no_pessoa
                             , ficha.nm_cientifico
                             , papel.cd_sistema     as cd_papel_sistema
                             , papel.ds_dados_apoio as ds_papel
                             , grupo.ds_dados_apoio as ds_grupo

                        from salve.oficina_ficha
                                 inner join salve.ficha_pessoa on ficha_pessoa.sq_ficha = oficina_ficha.sq_ficha
                                 inner join salve.dados_apoio papel on papel.sq_dados_apoio = ficha_pessoa.sq_papel
                                 inner join salve.dados_apoio grupo on grupo.sq_dados_apoio = ficha_pessoa.sq_grupo
                                 inner join salve.vw_pessoa_fisica pf on pf.sq_pessoa = ficha_pessoa.sq_pessoa
                                 inner join salve.ficha on ficha.sq_ficha = ficha_pessoa.sq_ficha
                        where oficina_ficha.sq_oficina_ficha in (${sqOficinaFichas.join(',')})
                          and ficha_pessoa.sq_pessoa in (${sqPessoas.join(',')})
                          and papel.cd_sistema = 'PONTO_FOCAL'
                          and ficha_pessoa.in_ativo = 'S'
                        UNION ALL (
                            with passo1 as (
                                select oficina_ficha.sq_ficha, ficha.nm_cientifico, ficha.sq_grupo
                                from salve.oficina_ficha
                                inner join salve.ficha on ficha.sq_ficha = oficina_ficha.sq_ficha
                                where oficina_ficha.sq_oficina_ficha in ( ${sqOficinaFichas.join(',')} )
                            ), passo2 as (
                                -- LER A ULTIMA OFICINA DE AVALIACAO DE CADA FICHA
                                select passo1.sq_ficha, passo1.nm_cientifico, passo1.sq_grupo, validacao.sq_oficina_ficha
                                from passo1
                                         inner join lateral (
                                    select oficina_ficha.sq_oficina_ficha
                                    from salve.oficina_ficha
                                    inner join salve.oficina on oficina.sq_oficina = oficina_ficha.sq_oficina
                                    where oficina.sq_tipo_oficina = ${oficinaTipoAvaliacao.id.toString()}
                                      and oficina_ficha.sq_ficha = passo1.sq_ficha
                                    order by oficina.dt_fim desc
                                    limit 1
                                    ) as validacao on true
                            )
                            -- LER OS FACILITADORES DA FICHA
                            select passo2.sq_ficha
                                 , op.sq_pessoa
                                 , pf.no_pessoa
                                 , passo2.nm_cientifico
                                 , papel.cd_sistema as cd_papel_sistema
                                 , papel.ds_dados_apoio as ds_papel
                                 , grupo.ds_dados_apoio as ds_grupo
                             from passo2
                             inner join salve.oficina_ficha_participante ofp on ofp.sq_oficina_ficha = passo2.sq_oficina_ficha
                             inner join salve.oficina_participante op on op.sq_oficina_participante = ofp.sq_oficina_participante
                             inner join salve.dados_apoio as papel on papel.sq_dados_apoio = ofp.sq_papel_participante
                             inner join salve.dados_apoio as grupo on grupo.sq_dados_apoio = passo2.sq_grupo
                             inner join salve.vw_pessoa_fisica pf on pf.sq_pessoa = op.sq_pessoa
                             where papel.cd_sistema = 'FACILITADOR'
                               and op.sq_pessoa in (${sqPessoas.join(',')})
                             )
                             order by no_pessoa, ds_papel
                        """
        result.papeis = sqlService.execSql( cmdSql )
        result.papeis.each { row ->
            if (!result.fichasValidadorInvalidas[row.sq_pessoa]) {
                result.fichasValidadorInvalidas[row.sq_pessoa] = [no_pessoa: row.no_pessoa, papeis: [:], fichas:[]]
            }
            if (!result.fichasValidadorInvalidas[row.sq_pessoa].papeis[row.cd_papel_sistema]) {
                result.fichasValidadorInvalidas[row.sq_pessoa].papeis[row.cd_papel_sistema] = [ds_papel:row.ds_papel,fichas: []]
            }
            result.fichasValidadorInvalidas[row.sq_pessoa].papeis[row.cd_papel_sistema]['fichas'].push(Util.ncItalico(row.nm_cientifico, true) + (row.ds_grupo ? ' (' + row.ds_grupo + ')' : ''))
        }
        return result
    }

    /**
     * retornar as oficinas de validação para preencher os campos selects
     */
    def selectOficinas() {
        List res = []
        if( ! params.sqCicloAvaliacao )
        {
            render res as JSON
            return
        }
        _getOficinasUsuario(params.sqCicloAvaliacao.toLong(),params.noCientificoFiltrarOficina).each { row ->
            res.push([id: row.sq_oficina, sgOficina: row.sg_oficina, periodo: row.de_periodo])
        }
        render res as JSON
    }

    def getGridOficinas() {
        if( ! params.sqCicloAvaliacao )
        {
            render ''
            return
        }
        Map paginate = _getOficinasUsuario( params.sqCicloAvaliacao.toLong()
                            , params.noCientificoFiltrarOficina
                            , params.sqSituacaoFiltro ? params.sqSituacaoFiltro.toLong() : 0l
                            , params.noOficinaFiltro
                            , params.deLocalFiltro
        )

        List listOficinas = paginate.rows
        if( params.format == 'xls' ) {
            Map res=[status:0,msg:'Exportar para planilha em andamento...']
            // exportacao das oficinas para planilha
            Map exportParams = [usuario:session.sicae.user, rows:paginate.rows]
            exportDataService.exportOficinasValidacaoPlanilha( exportParams )
            render res as JSON
            return
        }
        List listSituacaoOficina= dadosApoioService.getTable('TB_SITUACAO_OFICINA')
        render( template:'divGridOficinas'  ,model:[listOficinas         : listOficinas
                                                    ,listSituacaoOficina : listSituacaoOficina
                                                    ,pagination          : paginate.pagination
                        ])
    }

    def saveOficina()
    {
        List errors = []
        // validar periodo
        if (! params.sqCicloAvaliacao)
        {
            errors.push('Ciclo de Avaliação não Selecionado!')
        }

        if (params.dtInicio)
        {
            params.dtInicio = new Date().parse('dd/MM/yyyy', params.dtInicio)
        } else
        {
            errors.push('Data início deve ser informada!')
        }

        if (params.dtFim)
        {
            params.dtFim = new Date().parse('dd/MM/yyyy', params.dtFim)
        } else
        {
            errors.push('Data final deve ser informada!')
        }

        if (params.dtInicio > params.dtFim)
        {
            errors.push('Data final deve ser MAIOR ou IGUAL a inicial!')
        }

        DadosApoio situacaoEmValidacao = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA','EM_VALIDACAO')
        if ( ! situacaoEmValidacao )
        {
            errors.push('Situação da ficha EM_VALIDACAO não cadastrastrada na tabela de apoiO TB_SITUACAO_FICHA!')
        }

        /*DadosApoio situacaoAvaliadaRevisada = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA','VALIDADA_REVISADA')
        if ( ! situacaoAvaliadaRevisada )
        {
            errors.push('Situação da ficha AVALIADA_REVISADA não cadastrastrada na tabela de apoiO TB_SITUACAO_FICHA!')
        }*/

        DadosApoio fonteRecurso
        if( params.sqFonteRecurso ){
            fonteRecurso = DadosApoio.get( params.sqFonteRecurso.toLong() )
        }

        if( errors )
        {
            this._returnAjax(1, "Erros no preenchimento!<br>"+errors.join('<br>'), "error")
            return
        }

        DadosApoio oficinaValidacao = dadosApoioService.getByCodigo('TB_TIPO_OFICINA','OFICINA_VALIDACAO')
        params.sqUnidadeOrg=null
        params.sqPontoFocal=null

        Oficina oficina
        //CicloAvaliacao cicloAvaliacao
        DadosApoio situacaoAberta       = dadosApoioService.getByCodigo('TB_SITUACAO_OFICINA','ABERTA')
        DadosApoio situacaoEncerrada    = dadosApoioService.getByCodigo('TB_SITUACAO_OFICINA','ENCERRADA')
        if( params.sqOficina )
        {
            oficina = Oficina.get(params.int('sqOficina') )
        }
        else
        {
            oficina = new Oficina()
        }
        oficina.properties      = params
        oficina.cicloAvaliacao  = CicloAvaliacao.get( params.int('sqCicloAvaliacao') )
        oficina.tipoOficina     = oficinaValidacao
        oficina.fonteRecurso = fonteRecurso
        if( ! oficina.situacao ) {
            if (oficina.dtFim < Util.hoje()) {
                oficina.situacao = situacaoEncerrada
            } else {
                oficina.situacao = situacaoAberta
            }
        }

        // SALVE SEM CICLO
        String erroChoque = oficinaService.periodoOficinaEmChoque( oficina )
        if( erroChoque ){
            oficina.discard()
            return this._returnAjax(1, erroChoque, "error")
        }

        if( ! oficina.save() ){
            oficina.errors.allErrors.each {
                errors.push('Campo ' + it.getField() + ' não informado')
            }
            this._returnAjax(1, "Erro de inconsistência!<br>"+errors.join('<br>'), "error")
            return
        }
        // GRAVAR AS FICHAS
        List fichasJaCadastradas = OficinaFicha.createCriteria().list {
            eq('oficina', oficina)
        }?.ficha?.id

        if( params.ids ) {
            params.ids.split(',').each {
                OficinaFicha oficinaFicha = null
                Ficha ficha = Ficha.get( it.toLong() )
                if( ficha ) {
                    if (!fichasJaCadastradas.contains(it.toLong())) {
                        oficinaFicha = new OficinaFicha()
                        oficinaFicha.ficha = ficha
                        oficinaFicha.oficina = oficina
                        oficinaFicha.situacaoFicha = situacaoEmValidacao
                        //oficinaFicha.situacaoFicha = oficinaFicha.ficha.situacaoFicha // situacaoAvaliadaRevisada
                        /*oficinaFicha.stPossivelmenteExtinta = oficinaFicha.ficha.stPossivelmenteExtinta
                        oficinaFicha.categoriaIucn          = oficinaFicha.ficha.categoriaIucn
                        oficinaFicha.dsCriterioAvalIucn     = oficinaFicha.ficha.dsCriterioAvalIucn
                        oficinaFicha.dsJustificativa        = oficinaFicha.ficha.dsJustificativa
                        */
                        oficinaFicha.save()
                    } else {
                        oficinaFicha = OficinaFicha.findByFichaAndOficina(ficha, oficina)
                    }
                    if( oficinaFicha ) {
                        if (oficinaFicha.oficina.situacao == situacaoAberta) {
                            if (oficinaFicha.ficha?.situacaoFicha?.codigoSistema == 'AVALIADA_REVISADA') {
                                oficinaFicha.ficha.situacaoFicha = situacaoEmValidacao
                                oficinaFicha.situacaoFicha = situacaoEmValidacao
                                oficinaFicha.ficha.save()
                                oficinaFicha.save()
                            }
                        }
                        // se a ficha estiver EM_VALIDACAO não pode volvar para AVALIADA_REVISADA automaticamente
                    }
                }
            }
        }
        this._returnAjax(0, "Registro gravado com SUCESSO!", "success")
    }

    /**
     * retornar a lista de fichas adicionadas na oficina
     */
    def getListaFichasOficina()
    {
        boolean canModify=true
        if( !params.sqOficina )
        {
            render 'id da oficina não informado'
            return
        }

        List lista = OficinaFicha.executeQuery("""
        select new map( a.id as id, b.nmCientifico as descricao, b.id as sqFicha, d.coNivelTaxonomico as coNivelTaxonomico)
        from OficinaFicha a
        inner join a.ficha b
        inner join b.taxon c
        inner join c.nivelTaxonomico d
        where a.oficina.id = :sqOficina
        order by b.nmCientifico
        """,[sqOficina:params.sqOficina.toLong()]).each {
            it.descricao = Util.ncItalico( it.descricao,true,true,it.coNivelTaxonomico )
        }

        render(template: "/templates/tableDeleteItem", model: [lista: lista
                                                               , sqOficina:params.sqOficina
                                                               , rowNum:params.rowNum
                                                               , callback:'gerenciarValidacao.deleteFichaOficina'
                                                               , canModify:canModify] )
    }

    /**
     * excluir a(s) ficha(s) da oficina
     * @return
     */
    def deleteFichaOficina()
    {
        Map res = [status: 1, msg: '', errors: [], type: 'error']
        if (params.values) {
            List ids = params.values.split(',').collect { it.toLong() }
            try {
                // ler a oficina
                OficinaFicha oficinaFicha = OficinaFicha.get( ids[0] );
                // se for oficina de VALIDACAO e a ficha estiver EM_VALIDACAO, voltar para AVALIADA_REVISADA
                if( oficinaFicha.oficina.tipoOficina.codigoSistema == 'OFICINA_VALIDACAO') {
                    // ler os ids da fichas
                    DadosApoio situacaoAvaliadaRevisada = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'AVALIADA_REVISADA')
                    DadosApoio situacaoEmValiacao = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'EM_VALIDACAO')
                    String cmdSql = """update salve.ficha set sq_situacao_ficha = ${ situacaoAvaliadaRevisada.id.toString() }
                        where sq_situacao_ficha = ${situacaoEmValiacao.id.toString()}
                        and sq_ficha in ( select x.sq_ficha from salve.oficina_ficha x where x.sq_oficina_ficha in ( ${ids.join(',')} ) );
                    """
                    sqlService.execSql(cmdSql)
                }
                OficinaFicha.executeUpdate('delete from OficinaFicha a where a.id in (:ids)', [ids: ids])
                res.status = 0
                res.type = 'success'
                res.msg = 'Exclusão realizada com SUCESSO!'
              } catch (Exception e) {
                res.msg = e.getMessage()
            }
        }
        render res as JSON
    }

    private List getFichas(Integer sqCicloAvaliacao) {
        CicloAvaliacao cicloAvaliacao = CicloAvaliacao.get(sqCicloAvaliacao)
        List oficinaFicha = []

        if (!cicloAvaliacao) {
            render 'Id Ciclo Avaliação inválido.'
            return []
        }

        if (!params.sqOficina && !params.sqOficinaFiltro && !params.listOficinasFiltro && !params.sqFichaFiltro) {
            //render 'Id Ciclo Avaliação inválido.'
            return []
        }
        // FILTRAR PELAS SITUAÇÕES DAS FICHAS VÁLIDAS
        // List listSituacoesValidas = DadosApoio.findAllByPaiAndCodigoSistemaInList( DadosApoio.findByCodigoSistema('TB_SITUACAO_FICHA'),['AVALIADA_REVISADA','EM_VALIDACAO','VALIDADA'])
        // params.sqListSituacaoFiltro = listSituacoesValidas.collect { it.id.toInteger() }

        // adicionar as colunas da tabela oficina_ficha
        params.colOficinaFicha = true

        // CONSULTAR AS FICHAS VÁLIDAS
        List listIdsOficinaFicha = (Long[]) fichaService.search(params)?.sq_oficina_ficha
        //List listIdsFicha = (Long[]) fichaService.search(params)?.sq_oficina_ficha

        if (listIdsOficinaFicha) {
            // LER OS REGISTROS DA TABELA OFICINA_FICHA COM A LISTA DE FICHAS VALIDAS
            oficinaFicha = OficinaFicha.createCriteria().list {
                'in'('id', listIdsOficinaFicha)
                /*vwFicha {
                    'in'('id', listIdsFicha)
                    order('nmCientifico')
                }
                */
            }
        }
        return oficinaFicha
    }

    private List getConvites(Integer sqCicloAvaliacao, Integer sqOficina) {
        CicloAvaliacao cicloAvaliacao = CicloAvaliacao.get(sqCicloAvaliacao)
        if (!cicloAvaliacao) {
            render 'Id Ciclo Avaliação inválido.'
            return []
        }
        return CicloAvaliacaoValidador.createCriteria().list {
            eq('cicloAvaliacao.id', cicloAvaliacao.id.toLong())
            validadorFicha {
                oficinaFicha {
                    eq('oficina.id', sqOficina.toLong())
                }
            }
        }.unique { it.id }.sort { it.validador.noPessoa }
    }

    // SELECIONAR FICHAS
    def getFormSelecionarFicha() {
        if (!params.sqCicloAvaliacao) {
            render '<h3>Id do ciclo não informado!</h3>'
            return
        }
        CicloAvaliacao cicloAvaliacao = CicloAvaliacao.get(params.sqCicloAvaliacao.toInteger())
        DadosApoio tipoOficina = dadosApoioService.getByCodigo('TB_TIPO_OFICINA', 'OFICINA_VALIDACAO')
        List listTodasOficinas = Oficina.createCriteria().list {
            eq('cicloAvaliacao', cicloAvaliacao)
            eq('tipoOficina', tipoOficina)
        }.sort { [it.dtFim, it.noOficina] }
        render(template: 'formSelecionarFichas', model: [listTodasOficinas: listTodasOficinas])
    }


    // CONVIDAR VALIDADORES
    def getFormPessoa() {
        if (!params.sqCicloAvaliacao) {
            render 'Id do ciclo de avaliação não informado!'
            return;
        }

        CicloAvaliacao cicloAvaliacao = CicloAvaliacao.get( params.sqCicloAvaliacao.toInteger() )

        // listar as oficinas de VALIDACAO do ciclo
        DadosApoio tipoOficina = dadosApoioService.getByCodigo('TB_TIPO_OFICINA', 'OFICINA_VALIDACAO')
        List listTodasOficinas = Oficina.createCriteria().list {
            eq('cicloAvaliacao', cicloAvaliacao)
            eq('tipoOficina', tipoOficina)
            order( 'dtInicio', 'desc')
        }

        if( ! listTodasOficinas )
        {
            render '<h3>Nenhuma validação cadastrada!</h3>'
            return
        }

        List listNivelTaxonomico = NivelTaxonomico.list()
        List listCategorias = dadosApoioService.getTable('TB_CATEGORIA_IUCN').findAll{ it.codigoSistema != 'OUTRA' }.sort{ it.ordem }
        List listGrupos = dadosApoioService.getTable('TB_GRUPO').sort{ it.descricao }
        List listSubgrupos          = dadosApoioService.getTable('TB_SUBGRUPO')
        List listSituacaoFicha = [] // apenas fichas na situação AVALIADA_REVISADA

        // criar a lista dos validadore já convidados para auxiliar na seleção
        List listValidadores = sqlService.execSql("""select DISTINCT cav.sq_validador, concat( initcap(pf.no_pessoa),' (',cav.de_email,')') as no_pessoa,cav.de_email
                                        from salve.ciclo_avaliacao_validador cav
                                        inner join salve.vw_pessoa_fisica pf on pf.sq_pessoa = cav.sq_validador
                                        order by no_pessoa
                                        """)


        boolean canModify=true
        render(template: 'formCadValidadores', model: [
                  listSituacaoFicha    : listSituacaoFicha
                , listNivelTaxonomico: listNivelTaxonomico
                , listGrupos         : listGrupos
                , listSubgrupos         : listSubgrupos
                , listCategorias     : listCategorias
                , listTodasOficinas  : listTodasOficinas
                , listValidadores    : listValidadores
                , canModify          : canModify
        ])
    }

    def saveFrmValidadores() {
        Map res = [status: 1, msg: '', type: 'error', errors: []]
        if (!params.sqCicloAvaliacao) {
            res.errors.push('Ciclo de avaliação não informado!')
        }
        if (params.sqPessoa1 == params.sqPessoa2) {
            res.errors.push('Validadores devem ser diferentes!');
        }
        if (params.deEmail1 == params.deEmail2) {
            res.errors.push('Email dos validadores devem ser diferentes!')
        }
        if (!params.sqPessoa1) {
            res.errors.push('Validador deve ser selecionado!')
        }
        if (!params.ids) {
            res.errors.push('Nenhuma ficha foi selecionada!')
        }

        // necessário informar a oficina de validação
        if (!params.sqOficina) {
            res.errors.push('Oficina não foi selecionada!')
        }

        // array de ids da tabela oficina_ficha
        List listIdsOficinasFicha = params.ids.split(',').collect { it.toLong() }
        if (!listIdsOficinasFicha) {
            res.error.push('Nenhuma ficha selecionada!')
        }

        if (res.errors.size() > 0) {
            res.msg = res.errors.join('<br>')
            res.errors = []
            render res as JSON
            return
        }

        // fazer a validações:
        // 1) O ponto focal não pode ser validador da ficha que ele for o próprio ponto focal

//        res.status=0
//        res.type='success'
//        res.msg = 'VER PARAMETROS'
//        render res as JSON
//        return

        DadosApoio papelAvaliador = dadosApoioService.getByCodigo('TB_PAPEL_OFICINA', 'AVALIADOR')
        DadosApoio situacaoRecusado = dadosApoioService.getByCodigo('TB_SITUACAO_CONVITE_PESSOA', 'CONVITE_RECUSADO')
        DadosApoio situacaoEmailNaoEnviado = dadosApoioService.getByCodigo('TB_SITUACAO_CONVITE_PESSOA', 'EMAIL_NAO_ENVIADO')
        List avisos = []

        // ler a quantidade de convites existentes em cada ficha selecionada
        // só pode hever 2 validadores por sq_oficina_ficha
        // Desconsiderar oficinas fechadas, convites recusados e convtes vencidos na contagem
        List lista = CicloAvaliacaoValidador.executeQuery("""
            select new map( a.id as sqOficinaFicha, c.validador.id as sqValidador )
            from OficinaFicha a, ValidadorFicha b, CicloAvaliacaoValidador  c, Oficina d
            where a.id = b.oficinaFicha.id and  a.id in ( :ids )
            and b.cicloAvaliacaoValidador.id = c.id
            and d.id = a.oficina.id
            and c.situacaoConvite.id <> :recusado
            """, [ids: listIdsOficinasFicha, recusado: situacaoRecusado.id.toLong()])


        List sqPessoas = []
        if (params.sqPessoa1) {
            sqPessoas.push(params.sqPessoa1.toLong())
        }
        if (params.sqPessoa2) {
            sqPessoas.push(params.sqPessoa2.toLong())
        }
        Map resultadoValidacao = [:]
        if ( params.saveType=='validate' || params.saveType == 'saveValids' ) {
            List sqOficinaFichas = params.ids.split(',').collect{ it.toLong() }
            resultadoValidacao = _validarFichasValidador(sqPessoas, sqOficinaFichas )

            if( params.saveType == 'validate' && resultadoValidacao.fichasValidadorInvalidas ) {
                res.msg = ''
                res.errors = []
                res.type = 'info'
                res.status = 0
                res.fichasValidadorInvalidas = resultadoValidacao.fichasValidadorInvalidas
                render res as JSON
                return
            }
        }

        //fazer a inclusão das fichas selecionadas
        Long sqPessoa = 0l
        listIdsOficinasFicha.each { sqOficinaFicha ->
            2.times { i ->
                i++
                try {
                    if (params['sqPessoa' + i.toString()] && params['deEmail' + i.toString()]) {
                        sqPessoa = params['sqPessoa' + i.toString()].toLong()
                        if (sqPessoa > 0) {
                            if (lista.count { it.sqOficinaFicha.toLong() == sqOficinaFicha } < 2
                                    && lista.count { it.sqOficinaFicha.toLong() == sqOficinaFicha && it.sqValidador == sqPessoa } == 0) {

                                // Validador convidado não pode ter papel de Avaliador na oficina
                                try {
                                    OficinaFicha oficinaFicha = OficinaFicha.get(sqOficinaFicha)
                                    int qtd = OficinaFichaParticipante.createCriteria().count {
                                        eq('oficinaFicha.id', sqOficinaFicha)
                                        eq('papel.id', papelAvaliador.id)
                                        oficinaParticipante {
                                            eq('pessoa.id', sqPessoa)
                                        }
                                    }
                                    if( qtd > 0 ){
                                        throw new Exception( Pessoa.get(sqPessoa).noPessoa + ' foi AVALIADOR da ficha ' + oficinaFicha.vwFicha.nmCientifico + '.')
                                    }

                                    // se for para gravar somente as fichas validas para o validador, fazer a verificação se está valido
                                    if( params.saveType == 'saveValids' && resultadoValidacao.papeis ) {
                                        // validar se a pessoa é ponto focal do grupo da ficha
                                        Map papelNaoPermitido = resultadoValidacao.papeis.find { it.sq_ficha.toString() == oficinaFicha.ficha.id.toString() && it.sq_pessoa.toString() == sqPessoa.toString() }
                                        if ( papelNaoPermitido ) {
                                            throw new Exception('') // não fazer a gravação
                                        }
                                    }

                                    // verificar se o validador já existe na tabela ciclo_avaliacao_validador
                                    CicloAvaliacaoValidador cav = CicloAvaliacaoValidador.createCriteria().get {
                                        eq('oficina.id', params.sqOficina.toLong())
                                        eq('validador.id', sqPessoa)
                                    }

                                    // não gravar por enquanto
                                    /**/
                                    if (!cav) {
                                        cav = new CicloAvaliacaoValidador()
                                        cav.oficina = Oficina.get(params.sqOficina.toInteger())
                                        cav.cicloAvaliacao = CicloAvaliacao.get(params.sqCicloAvaliacao.toInteger())
                                        cav.validador = PessoaFisica.get(sqPessoa)
                                        cav.situacaoConvite = situacaoEmailNaoEnviado
                                    }
                                    cav.deEmail = params['deEmail' + i.toString()]

                                    if (cav.save(flush: true)) {
                                        // inserir a ficha
                                        ValidadorFicha vf = new ValidadorFicha()
                                        vf.cicloAvaliacaoValidador = cav
                                        vf.oficinaFicha = OficinaFicha.get(sqOficinaFicha)
                                        vf.save(flush: true)
                                    }
                                    /**/

                                } catch( Exception e ) {
                                    if( e.getMessage() ) {
                                        res.errors.push( e.getMessage() )
                                    }
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    res.errors.push(e.getMessage())
                    println e.getMessage()
                }
            }
            if (res.errors.size() > 0) {
                res.msg = res.errors.join('<br>')
                res.errors = []
                res.type = 'error'
                res.status = 1
                render res as JSON
                return
            }
        }

        res.msg = getMsg(1) + (avisos.size() > 0 ? '<br><br>AVISO<br>' + avisos.join('<br>') : '')
        res.type = (avisos.size() == 0 ? 'success' : 'info')
        res.status = 0
        render res as JSON
    }

    /**
     * alimentar o gride de fichas da aba Fichas ao selecionar a oficina
     */
    def getGridFichasCadastro() {
        String cmdSql
        Map sqlParams = [:]
        List sqlWhere   = []
        List listFichas
        boolean canModify = true

        if( ! params.sqCicloAvaliacao ) {
            render ''
            return
        }

        // primeiro ciclo não tem filtro pela situação para possibilitar cadastrar as oficinas passadas
        params.sqSituacaoFiltro = null
        if( params.sqCicloAvaliacao.toInteger()>1) {
            // listar somente as fichas na situacao avaliada revisada quando for 2º ciclo
            DadosApoio avaliadaRevisada = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'AVALIADA_REVISADA');
            params.sqSituacaoFiltro = avaliadaRevisada.id
        }

        //*************************************************
        // FILTROS QUE SERÃO FEITOS PELA FN_FICHAS_SELECT()
        //*************************************************
        Map filtrosFicha = ['sq_ciclo_avaliacao':params.sqCicloAvaliacao.toLong(),'sq_situacao_ficha': params.sqSituacaoFiltro]

        // FILTRO PELA UNIDADE OU PELO PERFIL
        if (params.sqUnidadeFiltro) {
            filtrosFicha.sq_unidade_org = params.sqUnidadeFiltro.toLong()
        } else {
            // filtrar somente as fichas que o usuário tem acesso
            // Se for Coordenador de taxon ou Colaborador Externo ler as fichas pela função
            // quaquer outro perfil que não seja o Administrador ou Consulta tem que ter a unidade
            if (session.sicae.user.isCT() || session.sicae.user.isCE()) {
                filtrosFicha.sq_pessoa = session.sicae.user.sqPessoa
            } else {
                if (!session.sicae.user.isADM() && !session.sicae.user.isCO()) {
                    filtrosFicha.sq_unidade_org = session.sicae.user.sqUnidadeOrg ? session.sicae.user.sqUnidadeOrg.toLong() : -1;
                }
            }
        }

        // FILTRO PELO NOME CIENTIFICO
        if (params.nmCientificoFiltro) {
            filtrosFicha.nm_cientifico = params.nmCientificoFiltro.toString()
        }

        // FILTRO PELO NOME COMUM
        if (params.nmComumFiltro) {
            filtrosFicha.no_comum = params.nmComumFiltro.toString()
        }

        // FILTRO PELO NIVEL TAXONOMICO
        if (params.nivelFiltro && params.sqTaxonFiltro) {
            filtrosFicha.sq_taxon = params.sqTaxonFiltro.toString()
            filtrosFicha.co_nivel_taxonomico = params.nivelFiltro.toString()
        }

        // FILTRO COM / SEM PENDENCIA
        if (params.inPendenciaFiltro) {
            filtrosFicha.st_com_pendencia = (params.inPendenciaFiltro.toInteger() == 1)
        }

        // FILTRO PELA SITUACAO DA FICHA
        if (params.sqSituacaoFiltro) {
            filtrosFicha.sq_situacao_ficha = params.sqSituacaoFiltro.toString()
        }

        // FILTRO PELA CATEGORIA
        if (params.sqCategoriaFiltro) {
            if (params.chkAvaliadaNoCicloFiltro == 'S') {
                filtrosFicha.sq_categoria_ficha = params.sqCategoriaFiltro.toString()
            } else {
                filtrosFicha.sq_categoria_final_ou_historico = params.sqCategoriaFiltro.toString()
            }
        }

        // FILTRO PELO GRUPO e SUBGRUPO AVALIADO
        if (params.sqGrupoFiltro) {
            filtrosFicha.sq_grupo = params.sqGrupoFiltro.toString()
            if (params.sqSubgrupoFiltro) {
                filtrosFicha.sq_subgrupo = params.sqSubgrupoFiltro.toString()
            }
        }

/*


        List categorias = dadosApoioService.getTable('TB_CATEGORIA_IUCN')
        // FILTRO PELAS AMEAÇADAS
        if( params?.fldAmeacadasFiltro == 'AMEACADAS'){
            // filtrar as categorias EX, EW, RE, CR, EN, VU e NT e retornar apenas os ids separados por virgula
            filtrosFicha.sq_situacao_ficha = categorias.findAll{ it.codigoSistema =~ /EX|EW|RE|CR|EN|VU|NT/}.id.join(',')
            println ' '
            println filtrosFicha.sq_situacao_ficha
        }
*/

        //****************************************************************/
        // FILTROS APLICADOS NA QUERY LOCAL E NÃO PELA FN_FICHAS_SELECT() /
        //****************************************************************/

        List categorias = dadosApoioService.getTable('TB_CATEGORIA_IUCN')
        // FILTRO PELAS AMEAÇADAS
        if( params?.fldAmeacadasFiltro == 'AMEACADAS'){
            // AMEAÇADAS: EX, EW, RE, CR, EN, VU, NT da aba 11.6 ou as LC. DD. NA que estão como CR, EN e VU na aba7
            String idsCategoriasEX = categorias.findAll{ it.codigoSistema =~ /EX|EW|RE|CR|EN|VU|NT/}.id.join(',')
            String idsCategoriasLC = categorias.findAll{ it.codigoSistema =~ /LC|DD|NA/}.id.join(',')
            String idsCategoriasCR = categorias.findAll{ it.codigoSistema =~ /CR|EN|VU/}.id.join(',')
            sqlWhere.push("( ficha.sq_categoria_iucn in (${idsCategoriasEX}) " +
                " OR (ficha.sq_categoria_iucn in (${idsCategoriasLC}) " +
                " AND taxon_historico.sq_categoria_iucn_nacional in (${idsCategoriasCR}) ) )")
        } else if( params?.fldAmeacadasFiltro == 'NAO_AMEACADAS') {
            // FILTRO PELAS NÃO AMEAÇADAS
            // NÃO AMEAÇADAS: LC, NA e DD na aba 11.6 e que NÃO estão ameaçadas CR, EN e VU na aba 7 ou não tem registro na aba 7.
            String idsCategoriasLC = categorias.findAll { it.codigoSistema =~ /LC|DD|NA/ }.id.join(',')
            String idsCategoriasCR = categorias.findAll { it.codigoSistema =~ /CR|EN|VU/ }.id.join(',')
            sqlWhere.push("ficha.sq_categoria_iucn in (${idsCategoriasLC}) " +
                "AND ( taxon_historico.sq_categoria_iucn_nacional IS NULL OR taxon_historico.sq_categoria_iucn_nacional NOT IN (${idsCategoriasCR}) ) ")
        }


            // FILTRO PELA FICHA
        /*if (params.sqFicha) {
            sqlWhere.push('oficina_ficha.sq_ficha = :sqFicha')
            sqlParams.sqFicha = params.sqFicha.toLong()
            // passar tambem para a fn_select_ficha para melhorar a performance
            filtrosFicha.sq_ficha = params.sqFicha.toLong()
        }*/


        cmdSql = """select fs.sq_ficha
                     ,fs.nm_cientifico
                     ,fs.st_transferida
                     ,categoria_avaliada_ficha.ds_dados_apoio as ds_categoria_avaliada_ficha
                     ,categoria_avaliada_ficha.cd_dados_apoio as cd_categoria_avaliada_ficha
                     ,unidade_org.sg_unidade_org
                     ,situacao_ficha.ds_dados_apoio as ds_situacao_ficha
                     ,situacao_ficha.cd_sistema as cd_situacao_ficha_sistema
                     -- dados do historico
                     ,taxon_historico.cd_categoria_iucn_nacional
                     ,taxon_historico.ds_categoria_iucn_nacional
                from salve.fn_ficha_select('${filtrosFicha as JSON}') fs
                inner join salve.ficha on ficha.sq_ficha = fs.sq_ficha
                inner join salve.dados_apoio as categoria_avaliada_ficha on categoria_avaliada_ficha.sq_dados_apoio = ficha.sq_categoria_iucn
                inner join salve.dados_apoio as situacao_ficha on situacao_ficha.sq_dados_apoio = ficha.sq_situacao_ficha
                inner join salve.vw_unidade_org as unidade_org on unidade_org.sq_pessoa = ficha.sq_unidade_org
                -- ler os dados do historico
                LEFT JOIN LATERAL (
                    SELECT th.sq_taxon_historico_avaliacao,
                           th.sq_categoria_iucn as sq_categoria_iucn_nacional,
                           categoria_th.cd_dados_apoio as cd_categoria_iucn_nacional,
                           case when th.sq_categoria_iucn is null then null else concat( categoria_th.ds_dados_apoio,' (',categoria_th.cd_dados_apoio,')') end as ds_categoria_iucn_nacional,
                           th.tx_justificativa_avaliacao as tx_justificativa_aval_nacional
                    FROM salve.taxon_historico_avaliacao th
                             INNER JOIN salve.dados_apoio AS tipo_th ON tipo_th.sq_dados_apoio = th.sq_tipo_avaliacao
                             INNER JOIN salve.dados_apoio AS categoria_th ON categoria_th.sq_dados_apoio = th.sq_categoria_iucn
                    WHERE th.sq_taxon = fs.sq_taxon
                      AND tipo_th.cd_sistema = 'NACIONAL_BRASIL'
                    ORDER BY th.nu_ano_avaliacao DESC, th.sq_taxon_historico_avaliacao desc LIMIT 1
                    ) as taxon_historico on true
                    ${sqlWhere ? 'WHERE ' + sqlWhere.join(' AND ') : ''}
                """
        /** /
         println ' '
         println ' '
         println cmdSql
         println sqlParams
         /**/
        listFichas = sqlService.execSql(cmdSql, sqlParams).sort { it.nm_cientifico }
        render(template: "divGridFichasCadastro", model: [listFichas: listFichas,canModify:canModify] )





        /*
        params.colHistorico=true
        List listFichas = fichaService.search( params )
        render(template: "divGridFichasCadastro", model: [listFichas: listFichas,canModify:canModify] )
        */
    }

    def getGridFichasOficina() {
        List listFichas = []
        if (!params.sqOficinaFiltro) {
            render '<h3>Oficina não informada!</h3>'
            return
        }
        // criar array de ids das fichas da oficina
        List fichasFiltradas = fichaService.search(params).collect { it.sq_ficha.toLong() }
        if (fichasFiltradas) {
            listFichas = OficinaFicha.createCriteria().list {
                eq('oficina', Oficina.get(params.sqOficinaFiltro.toLong()))
                vwFicha {
                    'in'('id', fichasFiltradas)
                    order('nmCientifico')
                }
            }
        }
        render(template: 'divGridFichas', model: [listOficinaFicha: listFichas])
    }

    def getGridConvites() {
        if (!params.sqCicloAvaliacao) {
            render 'Id do ciclo de avaliação não informado!'
            return;
        }
        if (!params.sqOficina) {
            render 'Oficina não selecionada!'
            return;
        }
        List listConvites = CicloAvaliacaoValidador.executeQuery("""
            select new map( a.id as sqCicloAvaliacaoValidador
            ,p.noPessoa as noValidador
            ,a.deEmail as deEmail
            ,a.dtEmail as dtEnviadoEm
            ,a.dtValidadeConvite as dtValidadeConvite
            ,e.descricao as dsSituacaoConvite
            ,e.codigoSistema as dsSituacaoConviteCodigo
            ,a.noUsuario as noEnviadoPor
            ,count( d.nmCientifico ) as nuFichas )
            from CicloAvaliacaoValidador a, ValidadorFicha b, OficinaFicha c, Ficha d, Pessoa p, DadosApoio e
            where a.id = b.cicloAvaliacaoValidador.id
              and c.id = b.oficinaFicha.id
              and d.id = c.ficha.id
              and p.id = a.validador.id
              and e.id = a.situacaoConvite.id
              and a.oficina.id = :sqOficina
            group by a.id,p.noPessoa
             ,a.deEmail
             ,a.dtEmail
             ,a.dtValidadeConvite
             ,e.descricao
             ,e.codigoSistema
             ,a.noUsuario
             order by p.noPessoa
            """, [sqOficina: params.sqOficina.toLong()])
        render(template: 'divGridConvites', model: [canModify: true, listConvites: listConvites])
        //render(template: 'divGridConvites', model: [canModify:true, listConvites: getConvites(params.sqCicloAvaliacao.toInteger(),params.sqOficina.toInteger())])
    }

    def getFichasValidador() {
        if (!params.id) {
            render ''
            return
        }
        List fichas = ValidadorFicha.executeQuery("""select new map( c.nmCientifico as nmCientifico, e.coNivelTaxonomico as coNivelTaxonomico ) from ValidadorFicha a
            join a.oficinaFicha b
            join b.ficha c
            join c.taxon d
            join d.nivelTaxonomico e
            where a.cicloAvaliacaoValidador.id = :id
            order by c.nmCientifico
            """, [id: params.id.toLong()])
        render fichas.collect { Util.ncItalico(it.nmCientifico, true,true, it.coNivelTaxonomico ) }.join(', ')
    }

    def sendEmailValidadores() {
        Map res = [status: 1, msg: '', type: 'error', errors: []]
        String hash
        Integer qtdEnvios = 0
        if (!params.ids) {
            res.msg = 'Nenhum validador selecionado.'
        }else if (!params.txEmail) {
            res.msg = 'Texto email não informado.'
        }else if (!params.deAssuntoEmail) {
            res.msg = 'Assunto email não informado.'
        }else if (!params.dtValidadeConvite) {
            res.msg = 'Data de validade do convite deve ser informada.'
        }else {
            Date dataValidade = new Date().parse('dd/MM/yyyy', params.dtValidadeConvite)
            if (dataValidade < Util.hoje()) {
                res.msg = 'Data de validade do(s) convite(s), ' + params.dtValidadeConvite + ', deve ser maior que hoje.'
            }else {
                String enviadoPor = PessoaFisica.get(session.sicae.user.sqPessoa).noPessoa
                DadosApoio situacaoEnviado = dadosApoioService.getByCodigo('TB_SITUACAO_CONVITE_PESSOA', 'EMAIL_ENVIADO')
                String htmlFichas = ''
                if (!situacaoEnviado) {
                    res.msg = 'Situção EMAIL_ENVIADO não cadastrada na tabela de apoio TB_SITUACAO_CONVITE_PESSOA'
                }else {
                    List ids = params.ids.split(',')
                    ids.each {
                        CicloAvaliacaoValidador cicloAvaliacaoValidador = CicloAvaliacaoValidador.get(it)
                        if (cicloAvaliacaoValidador) {
                            String email = cicloAvaliacaoValidador.deEmail
                            if (email) {

                                hash = Util.sha1(it.toString() + email + Util.agora())
                                String textoEmailCompleto = params.txEmail
                                String urlSalve = Util.getBaseUrl( request.getRequestURL().toString() )

                                // utilizar a url oficial quando estiver em producao
                                if( session.getAttribute('envProduction') ) {
                                    urlSalve = grailsApplication.config.url.sistema ?: urlSalve
                                }
                                String urlSalveExterno = grailsApplication.config.url.sicae
                                urlSalveExterno = ((urlSalveExterno ?: 'https://sicae.sisicmbio.icmbio.gov.br') + '/usuario-externo/login').replaceAll('//usuario-externo/','/usuario-externo/')

                                // adicionar link das respostas Sim e Não
                                String linkRespostas = '<p><a _target="_blank" href="' + urlSalve + 'api/respostaConvite/' + hash + '/s"><b style="color:green;">Aceitar o Convite</b></a>' +
                                                      '  ou   <a _target="_blank" href="' + urlSalve + 'api/respostaConvite/' + hash + '/n"><b style="color:red;">Recusar o Convite</b></a></p><br>'
                                // calcular quantidade por grupo
                                Map grupos = [:]
                                cicloAvaliacaoValidador.validadorFicha?.oficinaFicha?.vwFicha.each {

                                    String grupo = it.grupo ? it.grupo.descricao : 'Sem grupo';

                                    if( ! grupos[ grupo ] )
                                    {
                                        grupos[grupo] = 0i
                                    }
                                    grupos[grupo]++
                                }
                                String htmlGrupos=''
                                if( grupos ) {
                                    htmlGrupos = "<div><h3 style=\"margin:0px;\">Grupo${grupos.size() == 1 ? '' : 's'}</h3><ul>"
                                    grupos.each { k, v ->
                                        htmlGrupos += '<li>' + v + '&nbsp;' + k + '</li>'
                                    }
                                    htmlGrupos += "</ul></div>"
                                }
                                textoEmailCompleto = textoEmailCompleto.replaceAll(/\{nome\}/, cicloAvaliacaoValidador.validador.noPessoa)
                                htmlFichas = g.render(template: '/templates/email/tabelaFichas', model: [grupos:grupos,listFichas: cicloAvaliacaoValidador.validadorFicha?.oficinaFicha?.vwFicha])
                                textoEmailCompleto = textoEmailCompleto.replaceAll(/\{fichas\}/, htmlGrupos+linkRespostas+htmlFichas)
                                //'<ol>' + cicloAvaliacaoValidador.validadorFicha?.oficinaFicha?.ficha?.noCientificoItalico.sort().collect{'<li>'+it+'</li>'}.join('')+'</ol>')
                                textoEmailCompleto = textoEmailCompleto.replaceAll(/\{linkSalve\}/, '<a target="_blank" href="' + urlSalve + '">Clique aqui para acessar o Sistema SALVE.</a>')
                                textoEmailCompleto = textoEmailCompleto.replaceAll(/\{linkSalveExterno\}/, '<a target="_blank" href="' + urlSalveExterno + '">Clique aqui para acessar o Sistema SALVE - EXTERNO.</a>')
                                textoEmailCompleto = textoEmailCompleto.replaceAll(/\{prazo\}/, params.dtValidadeConvite)
                                textoEmailCompleto = Util.trimEditor(textoEmailCompleto.replaceAll(/\{emailConvidado\}/, email))

                                params.deEmailCopia = params.deEmailCopia ?: ''
                                if (emailService.sendTo(email, params.deAssuntoEmail, textoEmailCompleto, grailsApplication.config.app.email,[:],params.deEmailCopia)) {
                                    qtdEnvios++
                                    cicloAvaliacaoValidador.dtEmail = new Date()
                                    cicloAvaliacaoValidador.situacaoConvite = situacaoEnviado
                                    cicloAvaliacaoValidador.dtResposta = null
                                    cicloAvaliacaoValidador.noUsuario = enviadoPor
                                    cicloAvaliacaoValidador.dtValidadeConvite = dataValidade
                                    cicloAvaliacaoValidador.deHash = hash
                                    /*if (params.deEmailCopia) {
                                        params.deEmailCopia.split(',').each {
                                            emailService.sendTo(it.trim(), 'Cópia - ' + params.deAssuntoEmail, textoEmailCompleto)
                                        }
                                    }
                                     */
                                }else {
                                    println 'Erro envio de email para ' + email
                                    res.errors.push('Erro envio de email para ' + email)
                                }

                                if (!cicloAvaliacaoValidador.save(flush: true)) {
                                    res.errors.push('Erro inconsisteência ao atualizar envio para ' + email)
                                    cicloAvaliacaoValidador.errors.allErrors.each {
                                        res.errors.push(messageSource.getMessage(it, locale))
                                    }
                                }
                            }else {
                                println 'Email de ' + cicloAvaliacaoValidador.validador.noPessoa + ' nao encontrado'
                                res.errors.push('Email de ' + cicloAvaliacaoValidador.validador.noPessoa + ' nao encontrado')
                            }
                        }else {
                            println 'Ciclo consulta validador' + it + ' não cadastrado!'
                            res.errors.push('Ciclo consulta validador ' + it + ' não cadastrado!')
                        }
                    }
                }
            }
        }
        if (res.errors.size() == 0) {
            res.status = 0
            res.msg = qtdEnvios.toString() + ' Email' + (qtdEnvios > 1 ? '(s)' : '') + ' enviado' + (qtdEnvios > 1 ? '(s)' : '') + ' com SUCESSO!'
            res.type = 'success'
        }
        render res as JSON
    }

    def deleteConvite() {
        if (!params.id) {
            render ''
        }
        CicloAvaliacaoValidador.get(params.id).delete(flush: true);
        render ''
    }

    /**
     * ABA ACOMPANHAR
     */

    def getTelaAcompanhamento() {
        if (!params.sqCicloAvaliacao) {
            render '<h3>Id do ciclo não informado!</h3>'
            return
        }
        List listRespostas=[  ['codigo':'RESULTADO_A|RESULTADO_A','descricao':'a-a']
                             ,['codigo':'RESULTADO_A|RESULTADO_B','descricao':'a-b']
                             ,['codigo':'RESULTADO_A|RESULTADO_C','descricao':'a-c']
                             ,['codigo':'RESULTADO_B|RESULTADO_B','descricao':'b-b']
                             ,['codigo':'RESULTADO_B|RESULTADO_C','descricao':'b-c']
                             ,['codigo':'RESULTADO_C|RESULTADO_C','descricao':'c-c']
        ]

        params.format = 'all' // não paginar e retornar todas as oficinas
        Map paginate = _getOficinasUsuario( params.sqCicloAvaliacao.toLong() )
        List listTodasOficinas = paginate.rows
        if( ! listTodasOficinas ) {
            render '<h3>Nenhuma validação cadastrada ou nenhuma ficha em validação!</h3>'
            return
        }

        //List listSituacaoFicha = fichaService.getListSituacao()
        List listNivelTaxonomico = NivelTaxonomico.list()
        List listGrupos = dadosApoioService.getTable('TB_GRUPO').sort{ it.descricao }
        List listSubgrupos = dadosApoioService.getTable('TB_SUBGRUPO')
        render(template: 'telaAcompanhamento', model: [listTodasOficinas    : listTodasOficinas
                                                       , listNivelTaxonomico: listNivelTaxonomico
                                                       , listGrupos:listGrupos
                                                       , listSubgrupos:listSubgrupos
                                                       , listRespostas: listRespostas
                                                       , canModify : params.canModify])
    }

    def getGridAcompanhamento() {
        Map result = [:]
        List sqlWhere = []
        String errorMessage = ''
        String outputFormat = params.format ? params.format.toUpperCase() : 'TABLE';
        String sqlOrderBy = 'nm_cientifico'
        Map sqlParams = [:]
        List dadosGride = []
        Map data = [:]

        try {
            if (!params.sqCicloAvaliacao) {
                throw new Exception('<h3>Id do ciclo não informado!</h3>')
            }
            if (!session?.sicae?.user) {
                throw new Exception('<h3>Sessão expirada!</h3>')
            }

            if (params.sqFicha) {
                params.sqFichaFiltro = params.sqFicha
            } else {
                if (!params.sqOficinaFiltro) {
                    throw new Exception('Oficina não selecionada!')
                }
            }

            // SE NÃO FOR ADMINISTRADOR AS FICHAS DEVEM SER FILTRADAS DE ACORDO COM A UNIDADE OU A FUNÇÃO
            String sqlFiltrarFichasPessoa
            if (!session.sicae.user.isADM()) {
                DadosApoio papel
                // SE O USUÁRIO NÃO TIVER UNIDADE, FAZER FILTRO PELA FUNÇÃO ATRIBUIDA NAS FICHAS
                if (!session?.sicae?.user?.sqUnidadeOrg) {
                    papel = dadosApoioService.getByCodigo('TB_PAPEL_FICHA', (session.sicae.user.isCT() ? 'COORDENADOR_TAXON' : 'COLABORADOR_EXTERNO'))
                    if (!papel) {
                        render 'Perfil ' + (session.sicae.user.isCT() ? 'COORDENADOR_TAXON' : 'COLABORADOR_EXTERNO') + ' não cadastrado na tabela TB_PAPEL_FICHA'
                        return;
                    }
                    params.sqPessoaFiltro = session?.sicae?.user?.sqPessoa?.toLong()
                    params.sqPapelFiltro = papel?.id?.toLong()
                    sqlParams.sqPessoa = params.sqPessoaFiltro.toLong()
                    sqlParams.sqPapel = params.sqPapelFiltro.toLong()
                    sqlFiltrarFichasPessoa = """INNER JOIN salve.ficha_pessoa on ficha_pessoa.sq_ficha = oficina_ficha.sq_ficha
                    and ficha_pessoa.sq_pessoa = :sqPessoa
                    and ficha_pessoa.in_ativo = 'S'
                    and ficha_pessoa.sq_papel = :sqPapel"""
                } else {
                    params.sqUnidadeFiltro = session.sicae.user.sqUnidadeOrg.toLong()
                }
            }

            // FILTRAR PELA FICHA
            if (params.sqFichaFiltro) {
                sqlWhere.push('oficina_ficha.sq_ficha = :sqFicha')
                sqlParams.sqFicha = params.sqFichaFiltro.toLong()
            }

            // FILTRAR PELA OFICINA
            if (params.sqOficinaFiltro) {
                sqlWhere.push('oficina_ficha.sq_oficina = :sqOficina')
                sqlParams.sqOficina = params.sqOficinaFiltro.toLong()
            }

            // filtrar as fichas da unidade organizacional
            if (params.sqUnidadeFiltro) {
                sqlWhere.push('ficha.sq_unidade_org = :sqUnidadeOrg')
                sqlParams.sqUnidadeOrg = params.sqUnidadeFiltro.toLong()
            }
            // FILTRAR PELO NOME CIENTIFICO
            if (params.nmCientificoFiltro) {
                sqlWhere.push('ficha.nm_cientifico ilike :nmCientifico')
                sqlParams.nmCientifico = '%' + params.nmCientificoFiltro + '%'
            }
            // FILTRAR PELO NOME COMUM
            if (params.nmComumFiltro) {
                sqlParams.noComum = '%' + Util.removeAccents(params.nmComumFiltro) + '%'
            }
            // FILTRAR PELO(s) GRUPO(s)
            if (params.sqGrupoFiltro) {
                sqlWhere.push('ficha.sq_grupo in (' + params.sqGrupoFiltro + ')')
            }
            // FILTRAR PELO NOME DO GRUPO
            if (params.noGrupoFiltro) {
                sqlWhere.push('grupo.ds_dados_apoio ilike :noGrupo')
                sqlParams.noGrupo = '%' + params.noGrupoFiltro + '%'
            }

            // FILTRAR PELO SUBGRUPO
            if (params.sqSubgrupoFiltro) {
                sqlWhere.push('ficha.sq_subgrupo in (' + params.sqSubgrupoFiltro + ')')
            }
            String sqlFiltrarNomeValidador
            if (params.noValidadorFiltro) {
                sqlFiltrarNomeValidador = """INNER join LATERAL(
                     select sq_validador_ficha
                     from salve.validador_ficha
                     inner join salve.ciclo_avaliacao_validador on ciclo_avaliacao_validador.sq_ciclo_avaliacao_validador = validador_ficha.sq_ciclo_avaliacao_validador
                     inner join salve.vw_pessoa on vw_pessoa.sq_pessoa = ciclo_avaliacao_validador.sq_validador
                     inner join salve.vw_pessoa as pessoa on pessoa.sq_pessoa = ciclo_avaliacao_validador.sq_validador
                     where validador_ficha.sq_oficina_ficha = oficina_ficha.sq_oficina_ficha
                     and public.fn_remove_acentuacao( pessoa.no_pessoa ) ilike :noValidador
                     limit 1
                     ) as validador_ficha on true"""
                sqlParams.noValidador = '%' + Util.removeAccents(params.noValidadorFiltro) + '%'
            }

            // FILTRAR PELA COMBINAÇÃO DAS RESPOSTAS: A-A, B-B, C-C ...
            String sqlFiltrarRespostasValidadores
            if (params.deRespostasFiltro) {
                String respostas = params.deRespostasFiltro.split(',').collect{ "'"+it+"'"}.join(',')
                sqlFiltrarRespostasValidadores = """INNER JOIN LATERAL
                        ( WITH cte_respostas AS
                            ( SELECT array_to_string(array_agg(resultado.cd_sistema
                                ORDER BY resultado.cd_sistema),'|') AS tx_respostas
                                FROM salve.validador_ficha
                                INNER JOIN salve.dados_apoio AS resultado ON resultado.sq_dados_apoio = validador_ficha.sq_resultado
                                WHERE validador_ficha.sq_oficina_ficha = oficina_ficha.sq_oficina_ficha
                                LIMIT 2 )
                        SELECT tx_respostas FROM cte_respostas
                        WHERE tx_respostas in( ${respostas} ) ) AS respostas_validadores ON TRUE"""
                sqlParams.deRespostasFiltro = params.deRespostasFiltro
            }

            // FILTRAR COM/SEM JUSTIFICATIVA PENDENTE
            if (params.inJustificativaPendenteFiltro) {
                if (params.inJustificativaPendenteFiltro == 'S') {
                    sqlWhere.push("ficha.ds_justificativa_final IS NULL AND ficha.sq_categoria_final IS NOT NULL")
                } else {
                    sqlWhere.push("ficha.ds_justificativa_final IS NOT NULL")
                }
            }

            // FILTRAR COM/SEM CHAT
            if ( params.inComSemChatFiltro) {
                //sqlWhere.push("""${ params.inComSemChatFiltro == 'N' ? 'NOT' :''} exists ( select null from salve.oficina_ficha_chat x where x.sq_oficina_ficha = oficina_ficha.sq_oficina_ficha limit 1 )""")
                sqlWhere.push("""chat.st_possui_chat is ${ params.inComSemChatFiltro == 'S' ? 'not' :'' } null""")
            }

            // FILTRAR PELO(S) CODIGO(S) DA(S) CATEGORIA(S) AVALIADA(S) EX: DD,EN,LC
            if (params.coCategoriaAvaliadaFiltro) {
                params.coCategoriaAvaliadaFiltro = params.coCategoriaAvaliadaFiltro.toString().toUpperCase().replaceAll(/[^A-Za-z,]/, '')
                if (params.coCategoriaAvaliadaFiltro) {
                    String itens = params.coCategoriaAvaliadaFiltro.split(',').collect { "'" + it.toString().toUpperCase() + "'" }.join(',')
                    sqlWhere.push('categoria_avaliada.cd_dados_apoio in (' + itens + ')')
                }
            }

            // FILTRAR PELO(S) CODIGO(S) DA(S) CATEGORIA(S) VALIDADA(S) EX: DD,EN,LC
            if (params.coCategoriaValidadaFiltro) {
                params.coCategoriaValidadaFiltro = params.coCategoriaValidadaFiltro.toString().toUpperCase().replaceAll(/[^A-Za-z,]/, '')
                if (params.coCategoriaValidadaFiltro) {
                    String itens = params.coCategoriaValidadaFiltro.split(',').collect { "'" + it.toString().toUpperCase() + "'" }.join(',')
                    sqlWhere.push('categoria_final.cd_dados_apoio in (' + itens + ')')
                }
            }

            // FILTRAR PELO(S) CRITERIOS AVALIADOS EX: A1b
            if (params.dsCriterioAvaliadoFiltro) {
                params.dsCriterioAvaliadoFiltro = params.dsCriterioAvaliadoFiltro.toString().toUpperCase().replaceAll(/[^A-Za-z0-9\(\),]/, '')
                if (params.dsCriterioAvaliadoFiltro) {
                    //String itens = params.dsCriterioAvaliadoFiltro.split(';').collect { "'" + it.toString().toUpperCase() + "'" }.join(',')
                    sqlWhere.push('ficha.ds_criterio_aval_iucn ilike :dsCriterioAvaliado')
                    sqlParams.dsCriterioAvaliado = params.dsCriterioAvaliadoFiltro + '%'
                }
            }

            // FILTRAR PELO(S) CRITERIOS VALIDADOS EX: B1ab(i,ii,iii,v)
            if (params.dsCriterioValidadoFiltro) {
                params.dsCriterioValidadoFiltro = params.dsCriterioValidadoFiltro.toString().toUpperCase().replaceAll(/[^A-Za-z0-9\(\),]/, '')
                if (params.dsCriterioValidadoFiltro) {
                    //String itens = params.dsCriterioValidadoFiltro.split(';').collect { "'" + it.toString().toUpperCase() + "'" }.join(',')
                    sqlWhere.push('ficha.ds_criterio_aval_iucn_final ilike :dsCriterioValidado')
                    sqlParams.dsCriterioValidado = params.dsCriterioValidadoFiltro + '%'
                }
            }

            // FILTRAR PELA SIGLA DA UNIDADE ORGANIZACIONAL
            if (params.sgUnidadeOrgFiltro) {
                sqlWhere.push('unidade.sg_unidade_org ilike :sgUnidadeOrg')
                sqlParams.sgUnidadeOrg = params.sgUnidadeOrgFiltro + '%'

            }

            // FILTRAR PELA SITUACAO DA FICHA NA OFICINA
            if (params.dsSituacaoFichaValidacaoFiltro ) {
                sqlWhere.push('sfo.ds_dados_apoio ilike :dsSituacaoFichaValidacao')
                sqlParams.dsSituacaoFichaValidacao = '%' + params.dsSituacaoFichaValidacaoFiltro + '%'

            }

            if ( params.sortColumns) {
                List sortColumns = params.sortColumns.split(':')
                sqlOrderBy = '\nORDER BY ' + sortColumns[0] + (sortColumns[1] ? ' ' + sortColumns[1] : '')
                if( ! ( params.sortColumns.toString() =~ /nm_cientifico/) ) {
                    sqlOrderBy += ',nm_cientifico'
                }
            }

            String cmdSql = """WITH cte AS ( SELECT ficha.sq_taxon ,
                           oficina_ficha.sq_ficha ,
                           oficina_ficha.sq_oficina,
                           oficina_ficha.sq_oficina_ficha ,
                           ficha.nm_cientifico ,
                           ficha.st_possivelmente_extinta,
                           ficha_versao.sq_ficha_versao,
                           ficha_versao.nu_versao,

                           grupo.ds_dados_apoio AS ds_grupo ,
                           categoria_avaliada.cd_dados_apoio AS cd_categoria_avaliada ,
                           categoria_avaliada.ds_dados_apoio AS ds_categoria_avaliada ,
                           ficha.ds_criterio_aval_iucn AS ds_criterio_avaliado ,
                           oficina_ficha.ds_criterio_aval_iucn as ds_criterio_avaliado_oficina,
                           sfo.ds_dados_apoio as ds_situacao_ficha_oficina,
                           sfo.cd_sistema as cd_situacao_ficha_oficina_sistema,
                           situacao.ds_dados_apoio AS ds_situacao_ficha,
                           situacao.cd_sistema as cd_situacao_ficha_sistema,
                           sfo.cd_sistema as cd_situacao_ficha_validacao_sistema,
                           sfo.ds_dados_apoio as ds_situacao_ficha_validacao,

                           -- coalesce(categoria_final.cd_dados_apoio, categoria_validada.cd_dados_apoio ) as cd_categoria_final,
                           -- case when categoria_final.ds_dados_apoio is not null then concat( categoria_final.ds_dados_apoio,' (', categoria_final.cd_dados_apoio,')') else concat( categoria_validada.ds_dados_apoio,' (', categoria_validada.cd_dados_apoio,')') end as ds_categoria_final_completa,

                           case when oficina_ficha.sq_categoria_iucn is not null then categoria_validada.cd_dados_apoio else '' end as cd_categoria_final,
                           case when oficina_ficha.sq_categoria_iucn is not null then concat( categoria_validada.ds_dados_apoio,' (', categoria_validada.cd_dados_apoio,')') else '' end as ds_categoria_final_completa,

                           coalesce( ficha.st_possivelmente_extinta_final, oficina_ficha.st_possivelmente_extinta) as st_possivelmente_extinta_final,
                           coalesce( ficha.ds_criterio_aval_iucn_final, oficina_ficha.ds_criterio_aval_iucn) as ds_criterio_aval_iucn_final,
                           coalesce(ficha.ds_justificativa_final, oficina_ficha.ds_justificativa) as ds_justificativa_final,

                           case when ficha.ds_justificativa_final is null and ficha.sq_categoria_final is not null then true else false end st_pendente,
                           unidade.sg_unidade_org,
                           ficha.dt_aceite_validacao,
                           oficina_ficha.dt_avaliacao as dt_validada,
                           oficina_ficha.st_pf_convidado,
                           oficina_ficha.st_ct_convidado,
                           coalesce(chat.st_possui_chat,false) as st_possui_chat
                   FROM salve.oficina_ficha
                   INNER JOIN salve.ficha ON ficha.sq_ficha = oficina_ficha.sq_ficha
                   LEFT OUTER JOIN salve.dados_apoio AS grupo ON grupo.sq_dados_apoio = ficha.sq_grupo
                   LEFT OUTER JOIN salve.dados_apoio AS categoria_avaliada ON categoria_avaliada.sq_dados_apoio = ficha.sq_categoria_iucn
                   LEFT OUTER JOIN salve.dados_apoio AS categoria_validada ON categoria_validada.sq_dados_apoio = oficina_ficha.sq_categoria_iucn
                   LEFT OUTER JOIN salve.dados_apoio AS categoria_final  ON categoria_final.sq_dados_apoio = ficha.sq_categoria_final
                   LEFT OUTER JOIN salve.oficina_ficha_versao on oficina_ficha_versao.sq_oficina_ficha = oficina_ficha.sq_oficina_ficha
                   LEFT OUTER JOIN salve.ficha_versao on ficha_versao.sq_ficha_versao = oficina_ficha_versao.sq_ficha_versao

                   LEFT OUTER JOIN salve.dados_apoio as sfo on sfo.sq_dados_apoio = oficina_ficha.sq_situacao_ficha
                   inner join salve.dados_apoio AS situacao ON situacao.sq_dados_apoio = ficha.sq_situacao_ficha
                   inner join salve.vw_unidade_org as unidade on unidade.sq_pessoa = ficha.sq_unidade_org
                   -- possui/não possui chat
                   left join lateral (
                      select true as st_possui_chat
                      from salve.oficina_ficha_chat x
                      where x.sq_oficina_ficha = oficina_ficha.sq_oficina_ficha
                      offset 0 limit 1
                   ) as chat on true
                   ${params.nmComumFiltro ? "inner join lateral ( select fnc.sq_ficha from salve.ficha_nome_comum fnc where fnc.sq_ficha = ficha.sq_ficha and public.fn_remove_acentuacao( fnc.no_comum ) ilike :noComum limit 1 ) as nome_comum on true" : ''}
                   ${params.sqTaxonFiltro ? "inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon and (taxon.json_trilha->'" + params.nivelFiltro.toLowerCase() + "'->>'sq_taxon')::integer in( " + params.sqTaxonFiltro + " )" : ''}
                   ${sqlFiltrarNomeValidador ?: ''}
                   ${sqlFiltrarFichasPessoa ?: ''}
                   ${sqlFiltrarRespostasValidadores ?: ''}
                   WHERE ${sqlWhere.join('\nAND ')}
                   )
                SELECT cte.*
                       ,nivel.co_nivel_taxonomico
                       ,salve.json_validadores( cte.sq_ficha, cte.sq_oficina ) AS json_validadores
                FROM cte
                inner join taxonomia.taxon on taxon.sq_taxon = cte.sq_taxon
                inner join taxonomia.nivel_taxonomico as nivel on nivel.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
                ${sqlOrderBy}"""

            /** /
             println ' '
             println cmdSql
             println sqlParams
             /**/

            if (outputFormat == 'TABLE') {
                data = sqlService.paginate(cmdSql, sqlParams, params, 50, 12)
                dadosGride = data.rows

            } else if (outputFormat == 'CSV' ) {
                // sem paginação para exportacao de dados
                dadosGride = sqlService.execSql(cmdSql, sqlParams)
            }

            // tratar o retorno json_validadores para renderização na view
            int contador = 0
            dadosGride.each {
                def dadosJson = JSON.parse(it.json_validadores)
                it.validadores = []
                it.comunicarAlteracao = false
                it.dtUltimoComunicado = ''
                contador = 0
                for (item in dadosJson) {
                    contador++
                    boolean valido = false
                    if (item.value.cd_situacao_convite == 'EMAIL_ENVIADO' && item.value.in_convite_vencido == 'N') {
                        valido = true
                    } else if (item.value.cd_situacao_convite == 'CONVITE_ACEITO') {
                        valido = true
                    } else if (item.value.cd_situacao_convite == 'EMAIL_NAO_ENVIADO') {
                        valido = true
                    }
                    it.validadores.push([nome                  : item.key
                                         , situacaoConvite     : item.value.cd_situacao_convite
                                         , valido              : valido
                                         // colocar em negrito as respostas: a) , b)  c)
                                         , resposta            : item.value.de_resposta.replaceAll(/([abc]\))/, '<b>$1</b>')
                                         , categoria_sugerida  : item.value.de_categoria_sugerida
                                         , criterio_sugerido   : item.value.de_criterio_sugerido
                                         , respondido          : item.value.in_respondido == 'S'
                                         , possivelmenteExtinta: item.value.st_possivelmente_extinta == 'S' ? '(PEX)' : ''
                                         , contador_chat       : item.value.nu_chats
                                        // revisao coordenacao
                                         , sq_revisao          : item.value.sq_revisao ?: ''
                                         , resposta_revisao    : item.value.de_resposta_revisao.replaceAll(/([abc]\))/, '<b>$1</b>')
                                         , categoria_revisao   : item.value.de_categoria_revisao
                                         , criterio_revisao    : item.value.de_criterio_revisao
                                         , possivelmenteExtinta_revisao: item.value.st_possivelmente_extinta_revisao == 'S' ? '(PEX)' : ''
                                         , ordem               : item.value.sq_validador_ficha
                                         // colocar em ordem alfabetica somente os 2 primeiros validadores
                                         // e caso exista o terceiro validador este deverá ficar sempre por último
                                         , ordem               : (contador < 3 ? '0' : contador.toString() ) + Util.removeAccents(item.key).toUpperCase()


                    ])
                    if (item.value.in_comunicar_alteracao == 'S' && !it.comunicarAlteracao) {
                        it.comunicarAlteracao = true
                        it.dtUltimoComunicado = item.value.dt_ultimo_comunicado
                    }
                }

                // colocar em ordem alfabetica os validadores
                it.validadores.sort{it.ordem}

                if( outputFormat=='CSV'){
                    List nomes = []
                    List respostas = []
                    it.validadores.sort{ it.nome }.eachWithIndex{ validadorFicha, index ->
                        if( validadorFicha.situacaoConvite =='CONVITE_ACEITO' && validadorFicha.valido ){
                            if( session.sicae.user.isADM() || session.sicae.user.isPF() ) {
                                nomes.push( (index+1)+'. '+br.gov.icmbio.Util.nomeAbreviado( validadorFicha.nome ) )
                            } else {
                                nomes.push( (index+1)+'. validador(a)')
                            }
                        }
                        if( validadorFicha.situacaoConvite =='CONVITE_ACEITO' && validadorFicha.valido ) {
                            if( validadorFicha.resposta_revisao ) {
                                respostas.push((index + 1).toString() + '. ' + Util.stripTags(validadorFicha.resposta_revisao) + ' (revisão)' )
                            } else {
                                respostas.push((index + 1).toString() + '. ' + (validadorFicha.resposta ? Util.stripTags(validadorFicha.resposta) : 'não respondeu.'))
                            }
                        }
                    }
                    // respostas
                    it.no_validadores = nomes.join('\n')
                    it.ds_respostas = respostas.join('\n')
                }
                // adicionar PEX na categoria final
                if( it?.st_possivelmente_extinta_final == 'S') {
                    it.cd_categoria_final + ' (PEX)'
                }
                // data da validação
                it.dt_aceite_validacao_formatada=''
                if( it.dt_aceite_validacao) {
                    it.dt_aceite_validacao_formatada = it.dt_aceite_validacao.format('dd/MM/yyyy HH:mm:ss')
                }
                // justificativa final
                if( ! it.ds_justificativa_final && it.ds_categoria_final_completa ) {
                    it.ds_justificativa_final = null
                }
                it.ds_justificativa_final = Util.html2str( Util.stripTags( it.ds_justificativa_final ) )
            }

            if( outputFormat == 'CSV' ) {
                // criar variavel local selfSession para utilzar workers
                GrailsHttpSession selfSession = session
                Map workerData = [sqPessoa   : session?.sicae?.user?.sqPessoa
                                  , email    : params.email
                                  , rows     : dadosGride
                                  , columns   :[
                                         [title:'Nome Científico'       ,fieldName:'nm_cientifico',autoFit:true],
                                         //[title:'Situação ficha'        ,fieldName:'ds_situacao_ficha',autoFit:true],
                                         [title:'Grupo'                 ,fieldName:'ds_grupo',autoFit:true],
                                         [title:'Categoria Avaliada'    ,fieldName:'cd_categoria_avaliada',autoFit:true],
                                         [title:'Critério avaliada'     ,fieldName:'ds_criterio_avaliado',autoFit:true],
                                         [title:'Validadores'           ,fieldName:'no_validadores',autoFit:true],
                                         [title:'Respostas'             ,fieldName:'ds_respostas',autoFit:true],
                                         [title:'Situação ficha na validação', fieldName:'ds_situacao_ficha_validacao',autoFit:true],
                                         //[title:'Situação ficha'        ,fieldName:'ds_situacao_ficha',autoFit:true],
                                         [title:'Situação ficha na avaliação'        ,fieldName:'ds_situacao_ficha_oficina',autoFit:true],
                                         [title:'Categoria validada'    ,fieldName:'cd_categoria_final',autoFit:true],
                                         [title:'Critério validado'     ,fieldName:'ds_criterio_aval_iucn_final',autoFit:true],
                                         [title:'Justificativa final'   ,fieldName:'ds_justificativa_final',autoFit:false],
                                         [title:'Data validação'        ,fieldName:'dt_aceite_validacao_formatada',autoFit:true],
                                         [title:'Unidade responsável'   ,fieldName:'sg_unidade_org',autoFit:true],
                                    ]

                ]
                result.msg = 'A exportação está em andamento.<br><br>Ao finalizar será ' +
                    (params.email ? 'enviado para o email <b>' + params.email + '</b> o link para baixar a planilha.' : 'exibida a tela para baixar a planilha.') +
                    '<br><br>Dependendo da quantidade de registros este procedimento poderá levar alguns minutos.'

                runAsync {
                    exportDataService.exportToPlanilha(selfSession, workerData)
                }
            }

        } catch (Exception e) {
            errorMessage = e.getMessage()
            e.printStackTrace()
        }
        // quando for exportacao de dados retornar o json do resultado
        if ( outputFormat != 'TABLE') {
            if (errorMessage) {
                result = [msg: errorMessage, status: 1, type: 'error']
            }
            render result as JSON
            return
        }
        render(template: 'divGridFichasAcomp', model: [pagination : data.pagination,
                                                       dadosGride : dadosGride
                                                       , canModify: params?.canModify
                                                       , isAdm    : session.sicae.user.isADM()
                                                       , isPF     : session.sicae.user.isPF()
        ])

    }

    def getTabelaTotais(){

        DadosApoio situacaoExcluida = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA','EXCLUIDA')

        String query="""
            select oficina_ficha.sq_ficha, categoria_final.de_dados_apoio_ordem as ordem
            ,categoria_oficina.cd_dados_apoio as cd_categoria_oficina
            ,categoria_oficina.ds_dados_apoio||' ('||categoria_oficina.cd_dados_apoio||')' as ds_categoria_oficina_completa
            ,categoria_final.cd_dados_apoio as cd_categoria_final
            ,categoria_final.ds_dados_apoio||' ('||categoria_final.cd_dados_apoio||')' as ds_categoria_final_completa
            ,array_agg( coalesce(resposta.cd_sistema,'') ) as respostas
            from salve.oficina_ficha
            inner join salve.ficha on ficha.sq_ficha=oficina_ficha.sq_ficha
            left join salve.validador_ficha on validador_ficha.sq_oficina_ficha = oficina_ficha.sq_oficina_ficha
            left JOIN salve.dados_apoio categoria_oficina on categoria_oficina.sq_dados_apoio = ficha.sq_categoria_iucn
            left JOIN salve.dados_apoio categoria_final   on categoria_final.sq_dados_apoio = ficha.sq_categoria_final
            left JOIN salve.dados_apoio resposta          on resposta.sq_dados_apoio = validador_ficha.sq_resultado
            where oficina_ficha.sq_oficina = :sqOficina
            and ficha.sq_situacao_ficha <> ${ situacaoExcluida.id.toString() }
            group by oficina_ficha.sq_ficha, categoria_oficina.cd_dados_apoio
            ,categoria_final.cd_dados_apoio
            ,categoria_oficina.ds_dados_apoio
            ,categoria_final.ds_dados_apoio
            ,categoria_final.de_dados_apoio_ordem
            order by categoria_final.de_dados_apoio_ordem
            """
        List rows = sqlService.execSql(query,[sqOficina:params.sqOficina.toLong()])
        Map resultado = [total:0, validadas:0,em_validacao:0,a_validar:0,mantidas:0,alteradas:0,categorias:[:]]
        rows.each {
            // validadas
            if( it.cd_categoria_final ) {
                resultado.validadas++
                // por categoria
                if( ! resultado.categorias[it.ds_categoria_final_completa] )
                {
                    resultado.categorias[it.ds_categoria_final_completa] = 0
                }
                resultado.categorias[it.ds_categoria_final_completa]++

                // manteve a categoria ?
                if( it.cd_categoria_oficina == it.cd_categoria_final )
                {
                    resultado.mantidas++
                }
                else
                {
                    resultado.alteradas++
                }
            }
            // em validacao
            else if( it.respostas.toString().indexOf('RESULTADO_C') > -1 )
            {
                resultado.em_validacao++
            }
            else
            {
                resultado.a_validar++
            }
            // total
            resultado.total ++
        }

        DadosApoio situacaoAceito = dadosApoioService.getByCodigo('TB_SITUACAO_CONVITE_PESSOA', 'CONVITE_ACEITO')
        // calcular o percentual dos validadores
        query = """select e.no_pessoa, count( a.sq_validador_ficha ) as qtd_fichas
                ,sum( case when a.sq_resultado is not null then 1 else 0 end ) as qtd_respondidas
                from salve.validador_ficha a, salve.oficina_ficha b, salve.ficha c, salve.ciclo_avaliacao_validador d, salve.vw_pessoa e
                where a.sq_oficina_ficha = b.sq_oficina_ficha
                and c.sq_ficha = b.sq_ficha
                and d.sq_ciclo_avaliacao_validador = a.sq_ciclo_avaliacao_validador
                and e.sq_pessoa = d.sq_validador
                and d.sq_situacao_convite = :sqConviteAceito
                and b.sq_oficina = :sqOficina
                group by e.no_pessoa
                order by e.no_pessoa"""

        rows = sqlService.execSql(query,[sqOficina:params.sqOficina.toLong(),sqConviteAceito:situacaoAceito.id])
        List dadosDesenpenho = []
        rows.each {

            Integer qtdFichas       = it.qtd_fichas.toInteger()
            if( qtdFichas > 0 ) {
                Integer qtdRespondidas = it.qtd_respondidas.toInteger()
                Double percentual = qtdRespondidas / qtdFichas * 100
                dadosDesenpenho.push([nome            : Util.nomeAbreviado( Util.capitalize( it.no_pessoa ) )
                                      , qtdFichas     : qtdFichas
                                      , qtdRespondidas: qtdRespondidas
                                      , percentual    : Util.formatNumber(percentual.round(0), '##0') + ' %'])
            }
        }
        if( params.sortDesempenho ) {
            if( params.sortOrder == 'desc'){
                dadosDesenpenho.sort { a,b -> b[params.sortDesempenho] <=> a[params.sortDesempenho] }
            } else {
                dadosDesenpenho.sort { it[params.sortDesempenho] }
            }
        }
        render(template: 'tabelaTotais', model: [resultado:resultado,dadosDesempenho:dadosDesenpenho])
    }

    //----------------------------------

    def getGridFichasLc() {

        if (!params.sqCicloAvaliacao) {
            render 'Ciclo de avaliação não informado'
            return
        }
        if (params.sqFicha) {
            params.sqFichaFiltro = params.sqFicha
        }else {
            if (!params.sqOficinaFiltro) {
                render 'Oficina não selecionada!';
                return
            }
        }
        List listFichas = []
        params.colCategoria = true
        params.colOficinaFicha = true
        params.stManterLcFiltro = true
        listFichas = fichaService.search(params)/*.each {
           if( it.cd_categoria_sistema == 'LC' && it.sq_categoria_iucn && it.sq_categoria_final && it.sq_categoria_final.toInteger() == it.sq_categoria_iucn.toInteger() )
           {
               it.nm_cientifico = Util.ncItalico( it.nm_cientifico )
               listFichas.push( it )
           }
           */
        /*if( dadosUltimaAvaliacao?.coCategoriaIucn == 'LC' && it.vwFicha?.categoriaIucn?.codigoSistema == 'LC' )
            {
                listFichas.push( it )
            }
           */
        //}

        /*OficinaFicha.createCriteria().list {
            oficina {
                eq('cicloAvaliacao', CicloAvaliacao.get( params.sqCicloAvaliacao.toInteger() ) )
            }
        }.each {
            Map dadosUltimaAvaliacao = fichaService.getUltimaAvaliacaoNacional( it.vwFicha.id )
                if( dadosUltimaAvaliacao?.coCategoriaIucn == 'LC' && it.vwFicha?.categoriaIucn?.codigoSistema == 'LC' )
                {
                   listFichas.push( it )
                }
        }
        */

        // filtrar fichas
        //listFichas = fichaService.applyFilters(listFichas,params)
        render(template: 'divGridFichasLc', model: [listFichas: listFichas])
    }

    /**
     * Marcar/Desmacar para envio de email comunicando o validador para acessar o modulo de validação e verificar o chat
     * @return [description]
     */
    def marcarComunicarAlteracao() {
        if (params.sqFicha && params.selecionado) {
            DadosApoio situacaoAceito = dadosApoioService.getByCodigo('TB_SITUACAO_CONVITE_PESSOA', 'CONVITE_ACEITO')
            ValidadorFicha.createCriteria().list {
                oficinaFicha {
                    eq('ficha', Ficha.get(params.sqFicha.toInteger()))
                }
                cicloAvaliacaoValidador {
                    eq('situacaoConvite', situacaoAceito)
                }
            }.each {
                if (params.selecionado == 'S') {
                    it.inComunicarAlteracao = true
                    //it.dtUltimoComunicado=null
                }else {
                    it.inComunicarAlteracao = false
                }
                it.save(flush: true)
            }
        }
        render ''
    }

    /**
     * enviar email aos validadores para acessarem o salve e verificar o chat das fichas em validação
     * @return [description]
     */
    def enviarEmailPendenciaValidacao() {
        Map res = [status: 0, msg: '', type: 'success', errors: []]
        if (!params.sqFichas) {
            res.msg = 'Nenhuma ficha selecionada!'
            render res as JSON
            return;
        }
        String emailFrom = grailsApplication.config.app.email
        String emailMsg = '<p style="line-height:20px;">Prezado(a) <b>{nome}</b>,' +
                ' informamos que houve atualização na discussão da validação de:<br/>{fichas}<br/>' +
                '{linkSalve}<br/>' +
                'Obrigado!'

        String emailMsgFinal = ''
        String urlSalve = Util.getBaseUrl(request.getRequestURL().toString())
        String linkSalve = '<br/><a target="_blank" href="' + urlSalve + '">Clique aqui para acessar as suas espécies em validação.</a>'

        List ids = []
        params.sqFichas.split(',').each {
            ids.push(it.toLong())
        }
        Map validadores = [:]
        String noCientifico = ''
        ValidadorFicha validadorFicha
        DadosApoio situacaoAceito = dadosApoioService.getByCodigo('TB_SITUACAO_CONVITE_PESSOA', 'CONVITE_ACEITO')
        ValidadorFicha.createCriteria().list {
            oficinaFicha {
                'in'('ficha.id', ids)
            }
            cicloAvaliacaoValidador {
                eq('situacaoConvite', situacaoAceito)
            }
        }.each {
            if (!validadores[it.cicloAvaliacaoValidador.validador.email]) {
                validadores[it.cicloAvaliacaoValidador.validador.email] = [
                        noPessoa: it.cicloAvaliacaoValidador.validador.noPessoa
                        , fichas: []
                        , ids   : []
                ]
            }

            noCientifico = it.oficinaFicha.ficha.noCientifico
            if (validadores[it.cicloAvaliacaoValidador.validador.email].fichas.indexOf(noCientifico) == -1) {
                validadores[it.cicloAvaliacaoValidador.validador.email].fichas.push(noCientifico)
                validadores[it.cicloAvaliacaoValidador.validador.email].ids.push(it.id)
            }
        }

        validadores.each {

            //println emailMsg.replaceAll('{nome}',it.value.noPessoa);
            emailMsgFinal = emailMsg.replaceAll(/\{nome\}/, it.value.noPessoa).replaceAll(/\{fichas\}/, it.value.fichas.join('<br/>')).replaceAll(/\{linkSalve\}/, linkSalve)
            if (emailService.sendTo(it.key, 'Sistema de Avaliação - SALVE', emailMsgFinal, emailFrom))
            //if( true )
            {
                //println "Email enviado para: " + it.key
                it.value.ids.each { sqValidadorFicha ->
                    //println "Desmarcar "+ sqValidadorFicha
                    validadorFicha = ValidadorFicha.get(sqValidadorFicha.toInteger())
                    validadorFicha.inComunicarAlteracao = false
                    validadorFicha.dtUltimoComunicado = new Date();
                    validadorFicha.save(flush: true)
                }
            }else {
                //println 'Erro enviando email para '+it.key
                res.errors.push('Erro email para ' + it.key)
            }
        }

        //println validadores
        if (res.errors) {
            res.type = 'error'
            res.msg = 'Resultado envio emails:</br>' + res.errors.join('</br>')
        }else {
            res.msg = 'Email(s) enviado(s) com SUCESSSO!'
        }
        render res as JSON
    }

    /**
     * MODULO VALIDADORES
     */
    def indexValidador() {
        List listNivelTaxonomico = [ ]
        List listOficinas = [ ]

        // listas para popular os selects dos filtros no cabecalho da tabela
        List listGrupos = [ ]
        List listRespostas = [ ]
        List listCategoriasAvaliadas = [ ]
        List listCriteriosAvaliados = [ ]
        List listCategoriasValidadas = [ ]
        List listCriteriosValidados = [ ]

        try {

            // validar se a sessão está ok
            PessoaFisica validador = PessoaFisica.get(session?.sicae?.user?.sqPessoa.toLong())
            if (!validador) {
                throw new Exception('Validador não cadastrado')
            }

            // ler o niveis taxomomicos para filtragem das fichas
            listNivelTaxonomico = NivelTaxonomico.list()

            // ler tabela apoio oficina de validação
            DadosApoio oficinaValidacao = dadosApoioService.getByCodigo('TB_TIPO_OFICINA', 'OFICINA_VALIDACAO')
            // ler tabela apoio situacao convite aceito
            DadosApoio situacaoAceito = dadosApoioService.getByCodigo('TB_SITUACAO_CONVITE_PESSOA', 'CONVITE_ACEITO')

            // ler as oficinas que o validador já participou para filtragem das fichas
            if (session.listasValidador) {
                //listOficinas = session.listOficinas
            }
            // criar a lista de oficinas do usuário
            if (!listOficinas) {
                Map pars = [ sqValidador       : validador.id
                             , sqTipoOficina   : oficinaValidacao.id
                             , sqSituacaoAceito: situacaoAceito.id
                ]

                /** /
                println ' '
                println 'LER OFICINAS DO VALIDADOR'
                println pars
                /**/

                sqlService.execSql("""select distinct
                          oficina.sq_oficina
                        , oficina.dt_inicio
                        , concat( to_char(oficina.dt_inicio,'dd/MM/yy'),' a ',to_char(oficina.dt_fim,'dd/MM/yy'),' - ',oficina.sg_oficina,' (',situacao.ds_dados_apoio,')' ) as sg_oficina
                        , situacao.cd_dados_apoio as cd_situacao_sistema_oficina
                        , grupo.sq_dados_apoio as sq_grupo
                        , grupo.ds_dados_apoio as ds_grupo
                        , resultado.sq_dados_apoio          as sq_resposta
                        , resultado.ds_dados_apoio          as ds_resposta
                        , categoria_oficina.sq_dados_apoio  as sq_categoria_oficina
                        , case when categoria_oficina.sq_dados_apoio is not null then concat( categoria_oficina.ds_dados_apoio, ' (', categoria_oficina.cd_dados_apoio ,')') else '' end as ds_categoria_oficina
                        , ficha.ds_criterio_aval_iucn       as ds_criterio_oficina
                        , categoria_final.sq_dados_apoio    as sq_categoria_final
                        , case when categoria_final.sq_dados_apoio is not null then concat( categoria_final.ds_dados_apoio, ' (',categoria_final.cd_dados_apoio,')') else '' end as ds_categoria_final
                        , ficha.ds_criterio_aval_iucn_final as ds_criterio_final
                        -- respostas do validador na ficha
                        from salve.validador_ficha vf
                        -- convite
                        inner join salve.ciclo_avaliacao_validador cav on cav.sq_ciclo_avaliacao_validador = vf.sq_ciclo_avaliacao_validador
                        -- ciclo avaliacao
                        inner join salve.ciclo_avaliacao on ciclo_avaliacao.sq_ciclo_avaliacao = cav.sq_ciclo_avaliacao
                        -- oficina
                        inner join salve.oficina_ficha on oficina_ficha.sq_oficina_ficha = vf.sq_oficina_ficha
                        INNER JOIN salve.oficina on oficina.sq_oficina = oficina_ficha.sq_oficina
                        -- ficha
                        inner join salve.ficha on ficha.sq_ficha = oficina_ficha.sq_ficha

                        -- tabelas de apoio

                        -- situacao da oficina
                        inner join salve.dados_apoio as situacao on situacao.sq_dados_apoio = oficina.sq_situacao
                        -- resultado
                        left outer join salve.dados_apoio as resultado on resultado.sq_dados_apoio = vf.sq_resultado
                        -- categoria sugerida pelo validador
                        left outer join salve.dados_apoio as categoria_sugerida on categoria_sugerida.sq_dados_apoio = vf.sq_categoria_sugerida
                        -- categoria da oficina de avaliacao aba 11.6 (avaliada)
                        inner join salve.dados_apoio as categoria_oficina on categoria_oficina.sq_dados_apoio = ficha.sq_categoria_iucn
                        -- categoria validada 11.7 (final)
                        left outer join salve.dados_apoio as categoria_final on categoria_final.sq_dados_apoio = ficha.sq_categoria_final
                        -- grupo avaliado
                        left outer join salve.dados_apoio as grupo on grupo.sq_dados_apoio = ficha.sq_grupo

                        WHERE cav.sq_validador = :sqValidador
                        -- condicoes basicas ----
                        -- ciclo de avaliacao tem que estar habilitado para oficinas
                        and ciclo_avaliacao.in_oficina = 'S'
                        -- convite tem que estar aceito
                        and cav.sq_situacao_convite = :sqSituacaoAceito
                        -- oficina tem que ser de validacao
                        and oficina.sq_tipo_oficina = :sqTipoOficina""", pars).each { row ->

                    // preencher as listas

                    // oficinas
                    Map mapTemp = [ id         : row.sq_oficina
                                    , descricao: row.sg_oficina
                                    , situacao : row.cd_situacao_sistema_oficina
                    ]
                    if (!listOficinas.contains(mapTemp)) {
                        listOficinas.push(mapTemp)
                    }

                    // grupos
                    mapTemp = [ id: row.sq_grupo ?:0, descricao: row.ds_grupo ?:'Em branco']
                    if (!listGrupos.contains(mapTemp)) {
                        listGrupos.push(mapTemp)
                    }
                    listGrupos.sort{it.descricao}

                    // respostas
                    mapTemp = [ id: row.sq_resposta ?:0, descricao: row.ds_resposta ?:'Em branco' ]
                    if (!listRespostas.contains(mapTemp)) {
                        listRespostas.push(mapTemp)
                    }

                    // categorias AVALIADAS
                    mapTemp = [ id         : row.sq_categoria_oficina ?:0
                                , descricao: row.ds_categoria_oficina ?:'Em branco'
                    ]
                    if (!listCategoriasAvaliadas.contains(mapTemp)) {
                        listCategoriasAvaliadas.push(mapTemp)
                    }

                    // criterios  AVALIADAS
                    mapTemp = [ id: row.ds_criterio_oficina ?: '0', descricao: row.ds_criterio_oficina ?: 'Em branco' ]
                    if (!listCriteriosAvaliados.contains(mapTemp)) {
                        listCriteriosAvaliados.push(mapTemp)
                    }

                    // categorias VALIDADAS
                    mapTemp = [ id         : row.sq_categoria_final ?:0
                                , descricao: row.ds_categoria_final ?:'Em branco'
                    ]
                    if (!listCategoriasValidadas.contains(mapTemp)) {
                        listCategoriasValidadas.push(mapTemp)
                    }

                    // criterios  VALIDADAS
                    mapTemp = [ id: row.ds_criterio_final ?: '0', descricao: row.ds_criterio_final ?:'Em branco' ]
                    if (!listCriteriosValidados.contains(mapTemp)) {
                        listCriteriosValidados.push(mapTemp)
                    }
                }
            }
        } catch (Exception e) {
            println e.getMessage()
            render e.getMessage()

            flash.error = e.getMessage()
        }

        //flash.error = 'teste mensagem de erro'
        render(view: 'indexValidador', model: [ listNivelTaxonomico      : listNivelTaxonomico
                                                , listOficinas           : listOficinas
                                                , listGrupos             : listGrupos
                                                , listRespostas          : listRespostas
                                                , listCategoriasAvaliadas: listCategoriasAvaliadas
                                                , listCategoriasValidadas: listCategoriasValidadas
                                                , listCriteriosAvaliados : listCriteriosAvaliados
                                                , listCriteriosValidados : listCriteriosValidados
        ])
    }

    def getGridFichasValidador() {
        if (!session?.sicae?.user?.sqPessoa) {
            render '<div class="alert alert-danger"><b>Id do validador inválido!</b><br/>Efetue o login novamente!</div>'
            return
        }
        /** /
        println ' '
        println 'MONTAR GRIDE FICHAS VALIDADOR'
        println 'Validador: ' + session?.sicae?.user?.sqPessoa
        println params
        /**/
        //render '<div class="alert alert-danger"><b>Id do validador inválido!</b><br/>Efetue o login novamente!</div>'
        //return;

        PessoaFisica validador = PessoaFisica.get(session?.sicae?.user?.sqPessoa.toInteger())
        if (!validador) {
            render '<div class="alert alert-danger"><b>Validador não cadastrado no ICMBIO!</div>'
            return
        }

        // filtro padrão inicial
        params.cdSituacaoFiltro = params.cdSituacaoFiltro ?: 'PENDENCIAS'

        // carregar as fichas do validador
        DadosApoio situacaoAceito = dadosApoioService.getByCodigo('TB_SITUACAO_CONVITE_PESSOA', 'CONVITE_ACEITO')
        //DadosApoio situacaoValidada = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'VALIDADA')
        //DadosApoio situacaoValidadaRevisao = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'VALIDADA_EM_REVISAO')
        DadosApoio situacaoAberta = dadosApoioService.getByCodigo('TB_SITUACAO_OFICINA', 'ABERTA')
        DadosApoio oficinaValidacao = dadosApoioService.getByCodigo('TB_TIPO_OFICINA', 'OFICINA_VALIDACAO')
        Map qryParams = [sqValidador        : validador.id
                    , sqTipoOficina    : oficinaValidacao.id
                    , sqSituacaoAceito : situacaoAceito.id
                    , sqSituacaoAberta : situacaoAberta.id
        ]

        String cmdSql ="""select
                          ficha.nm_cientifico
                        , ficha.sq_ficha
                        , grupo.sq_dados_apoio as sq_grupo
                        , grupo.ds_dados_apoio as ds_grupo
                        , nome_comum.no_comum

                        , resultado.ds_dados_apoio          as ds_resposta
                        , categoria_sugerida.ds_dados_apoio as ds_categoria_sugerida
                        , categoria_sugerida.cd_dados_apoio as cd_categoria_sugerida
                        , vf.de_criterio_sugerido
                        , salve.snd(vf.st_possivelmente_extinta) as ds_possivelmente_extinta

                        , categoria_oficina.ds_dados_apoio  as ds_categoria_oficina
                        , categoria_oficina.cd_dados_apoio  as cd_categoria_oficina
                        , categoria_oficina.cd_sistema      as cd_categoria_sistema_oficina
                        , ficha.ds_criterio_aval_iucn       as ds_criterio_oficina
                        , salve.snd( ficha.st_possivelmente_extinta ) as st_possivelmente_extinta_oficina

                        , categoria_final.ds_dados_apoio    as ds_categoria_final
                        , categoria_final.cd_dados_apoio    as cd_categoria_final
                        , ficha.ds_criterio_aval_iucn_final as ds_criterio_final
                        , ficha.ds_justificativa_final      as ds_justificativa_final
                        , salve.snd( ficha.st_possivelmente_extinta_final ) as st_possivelmente_extinta_final
                        , chat.nu_chat

                        , oficina.sq_oficina
                        , oficina.sg_oficina||' - '||to_char(oficina.dt_inicio,'dd/MM/yy')||' a '||to_char(oficina.dt_fim,'dd/MM/yy') as sg_oficina
                        , oficina_ficha.sq_oficina_ficha
                        , nivel.co_nivel_taxonomico
                        , ficha_versao.sq_ficha_versao
                        , ficha_versao.nu_versao

                        -- respostas do validador na ficha
                        from salve.validador_ficha vf

                        -- convite
                        inner join salve.ciclo_avaliacao_validador cav on cav.sq_ciclo_avaliacao_validador = vf.sq_ciclo_avaliacao_validador

                        -- ciclo avaliacao
                        inner join salve.ciclo_avaliacao on ciclo_avaliacao.sq_ciclo_avaliacao = cav.sq_ciclo_avaliacao

                        -- oficina
                        inner join salve.oficina_ficha on oficina_ficha.sq_oficina_ficha = vf.sq_oficina_ficha
                        inner join salve.oficina on oficina.sq_oficina = oficina_ficha.sq_oficina
                        -- ficha versao
                        left outer join salve.oficina_ficha_versao ofv on ofv.sq_oficina_ficha = oficina_ficha.sq_oficina_ficha
                        left outer join salve.ficha_versao on ficha_versao.sq_ficha_versao = ofv.sq_ficha_versao


                        -- ficha e taxon
                        inner join salve.ficha on ficha.sq_ficha = oficina_ficha.sq_ficha
                        inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                        inner join taxonomia.nivel_taxonomico nivel on nivel.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico


                        -- tabelas de apoio

                        -- resultado
                        left outer join salve.dados_apoio as resultado on resultado.sq_dados_apoio = vf.sq_resultado

                        -- categoria sugerida pelo validador
                        left outer join salve.dados_apoio as categoria_sugerida on categoria_sugerida.sq_dados_apoio = vf.sq_categoria_sugerida

                        -- categoria da oficina de avaliacao aba 11.6 (avaliada)
                        inner join salve.dados_apoio as categoria_oficina on categoria_oficina.sq_dados_apoio = ficha.sq_categoria_iucn

                        -- categoria validada 11.7 (final)
                        left outer join salve.dados_apoio as categoria_final on categoria_final.sq_dados_apoio = ficha.sq_categoria_final

                        -- grupo avaliado
                        left outer join salve.dados_apoio as grupo on grupo.sq_dados_apoio = ficha.sq_grupo

                        -- ler os nomes comuns
                        left join lateral (
                           select array_to_string( array_agg( ficha_nome_comum.no_comum),', ') as no_comum
                              from salve.ficha_nome_comum
                              where ficha_nome_comum.sq_ficha = oficina_ficha.sq_ficha
                        ) as nome_comum on true

                        -- ler chats
                         LEFT JOIN lateral (
                                select count( chat.sq_oficina_ficha_chat ) as nu_chat
                                  from salve.oficina_ficha_chat chat
                                 where chat.sq_oficina_ficha = oficina_ficha.sq_oficina_ficha
                                   and chat.sq_pessoa = cav.sq_validador
                        ) chat on true

                        WHERE cav.sq_validador = :sqValidador

                        -- condicoes basicas ----

                        -- não exibir as fichas versionadas
                        and ofv.sq_ficha_versao is null

                        -- ciclo de avaliacao tem que estar habilitado para oficinas
                        and ciclo_avaliacao.in_oficina = 'S'

                        -- convite tem que estar aceito
                        and cav.sq_situacao_convite = :sqSituacaoAceito

                        -- oficina tem que estar aberta
                        and oficina.sq_situacao = :sqSituacaoAberta

                        -- oficina tem que ser de validacao
                        and oficina.sq_tipo_oficina = :sqTipoOficina
                        """

        // aplicar filtro pelas condição selecionada: Minhas Pendências, Minhas Validações, Validadas ou Não validadas
        if ( params.sqFicha ) {
            cmdSql += "\nand ficha.sq_ficha = " + params.sqFicha.toString()
        } else {
            if( params.cdSituacaoFiltro == 'VALIDADAS' ) {
              cmdSql += "\nand ficha.sq_categoria_final is not null"
            } else if( params.cdSituacaoFiltro == 'NAO_VALIDADAS' ) {
              cmdSql += "\nand ficha.sq_categoria_final is null"
            } else if( params.cdSituacaoFiltro == 'PENDENCIAS' ) {
              cmdSql += "\nand vf.sq_resultado is null"
            } else if( params.cdSituacaoFiltro == 'VALIDACOES' ) {
              cmdSql += "\nand vf.sq_resultado is not null"
            }

            //filtrar pelo nome cientifico
            if( params.no_cientifico) {
                cmdSql += "\nand ficha.nm_cientifico ilike :noCientifico"
                qryParams.noCientifico = '%' + params.no_cientifico + '%'
            }

            //filtrar pelo nome comum
            if( params.no_comum) {
                cmdSql += "\nand nome_comum.no_comum ilike :noComum"
                qryParams.noComum = '%' + params.no_comum + '%'
            }

            // filtrar pela oficina
            if (params.sqOficinaFiltro) {
                cmdSql += "\nand oficina.sq_oficina = :sqOficina"
                qryParams.sqOficina = params.sqOficinaFiltro.toLong()
            }

            // filtar pelo grupo
            if( params.sq_grupo ) {
                if( params.sq_grupo.toInteger() > 0 ) {
                    cmdSql += "\nand ficha.sq_grupo = :sqGrupo"
                    qryParams.sqGrupo = params.sq_grupo.toLong()
                } else {
                    cmdSql += "\nand ficha.sq_grupo is null"
                }
            }

            // filtar pelo resposta a, b ou c
            if( params.sq_resposta ) {
                if( params.sq_resposta.toInteger() > 0 ) {
                    cmdSql += "\nand vf.sq_resultado = :sqResposta"
                    qryParams.sqResposta = params.sq_resposta.toLong()
                } else {
                    cmdSql += "\nand vf.sq_resultado is null"
                }
            }

            // filtrar pela categoria avaliada
            if( params.sq_categoria_avaliada ){
                if( params.sq_categoria_avaliada.toInteger() > 0 ) {
                    cmdSql += "\nand ficha.sq_categoria_iucn = :sqCategoriaAvaliada"
                    qryParams.sqCategoriaAvaliada = params.sq_categoria_avaliada.toLong()
                } else {

                }
            }

            // filtrar pelo criterio avaliado
            if( params.ds_criterio_avaliado ){
                if( params.ds_criterio_avaliado.toString() != '0') {
                    cmdSql += "\nand ficha.ds_criterio_aval_iucn = :dsCriterioAvaliado"
                    qryParams.dsCriterioAvaliado = params.ds_criterio_avaliado
                } else {
                    cmdSql += "\nand ficha.ds_criterio_aval_iucn is null"
                }
            }

            // filtrar pela categoria validada
            if( params.sq_categoria_validada ){
                if( params.sq_categoria_validada.toInteger() > 0 ) {
                    cmdSql += "\nand ficha.sq_categoria_final = :sqCategoriaValidada"
                    qryParams.sqCategoriaValidada = params.sq_categoria_validada.toLong()
                } else {

                }
            }

            // filtrar pelo criterio Validado
            if( params.ds_criterio_validado ){
                if( params.ds_criterio_validado.toString() != '0') {
                    cmdSql += "\nand ficha.ds_criterio_aval_iucn_final = :dsCriterioValidado"
                    qryParams.dsCriterioValidado = params.ds_criterio_validado
                } else {
                    cmdSql += "\nand ficha.ds_criterio_aval_iucn_final is null"
                }
            }
        }

        // filtrar fichas Com / Sem chat
        if( params.inComSemChatFiltro ) {
            if( params.inComSemChatFiltro == 'S' ) {
                cmdSql += "\nand chat.nu_chat > 0";
            } else if( params.inComSemChatFiltro == 'N' ) {
                cmdSql += "\nand chat.nu_chat = 0";
            }
        }

        // filtrar pelo nivel taxonômico selecionado
        if( params.sqNivelFiltro && params.sqTaxonFiltro ) {
            cmdSql += "\nand json_trilha->'${params?.sqNivelFiltro?.toLowerCase()}'->>'sq_taxon' = '${params.sqTaxonFiltro.toString()}' ";
        }

        // ordenar pelo nome cientifico
        cmdSql += "\norder by ficha.nm_cientifico"

// params.sqFicha
 /** /
 println ' '
 println cmdSql
 println qryParams
 /**/

        Map data = sqlService.paginate(cmdSql,qryParams,params);
        //List rows = sqlService.execSql(cmdSql, pars)

/*
        // calcular a paginação
        if( ! pagination.totalRecords )  {
            pagination.totalRecords = rows.size()
            pagination.totalPages = Math.max(1, Math.ceil(pagination.totalRecords / pagination.pageSize))
            pagination.pageNumber = 1
            rows = rows.collate( pagination.pageSize )[0]
        }
*/
        // se não encontrar nenhuma ficha, mostrar os convites enviados que ainda estejam na validade
        List listCicloAvaliacaoValidador = []
        if( ! data.rows ) {
            listCicloAvaliacaoValidador = CicloAvaliacaoValidador.createCriteria().list {
                eq('validador.id', validador.id)
                ge('dtValidadeConvite', new Date())
            }//.sort { a,b -> b.cicloAvaliacao.anoFinal<=>a.cicloAvaliacao.anoFinal}*/
        }

        render(template: 'divGridFichasValidador'
                , model: [canModify                    : true
                          , validador                  : validador
                          , rows                       : data.rows
                          , pagination                 : data.pagination
                          , listCicloAvaliacaoValidador: listCicloAvaliacaoValidador])
    }

    def saveEmailParticipante() {

        Map res = [status: 0, msg: '', type: 'success', errors: []]
        try {

            if( !params.deEmail )
            {
                throw new Exception('Email deve ser informado!')
            }

            if( ! params.sqCicloAvaliacaoValidador )
            {
                throw new Exception('Id do validador deve ser informado!')
            }

            CicloAvaliacaoValidador reg  = CicloAvaliacaoValidador.get( params.sqCicloAvaliacaoValidador.toLong() )
            if( reg ) {
                reg.deEmail = params.deEmail
                reg.save(flush: true)
                res.msg = 'Email alterado com SUCESSO!'
            } else {
                throw new Exception('Id inválido!')
            }
        } catch( Exception e ) {
            res.status = 1
            res.msg = e.getMessage()
        }
        render res as JSON
    }



    /**
     * MODULO FUNDAMENTAÇÃO
     */
    def getFormFundamentacao() {
        boolean convidadoChat = false

        if( ! session?.sicae?.user ) {
            render 'Sessão expirada. Efetue login novamente.'
            return;
        }

        OficinaFicha oficinaFicha

        Ficha ficha = Ficha.get(params.sqFicha.toInteger())
        if (!ficha) {
            render '<h3>Ficha inválida</h3>'
            return
        }

        // inicializar parametro canModify
        if( params.canModify == null || params.canModify.toString() == 'true' || params.canModify.toString() == 'S' )
        {
            params.canModify = ficha.canModify( session.sicae.user )
        }
        else
        {
            params.canModify = false
        }

        if( session.sicae.user.isADM() )
        {
             params.canModify = true
        }
        boolean canModify = params.canModify

        // situação da ficha em que a funamentação não pode mais ser alterada
        if( ficha.situacaoFicha.codigoSistema == 'FINALIZADA' ){
            canModify = false
            params.canModify=false

        }

        if( params.sqOficinaFicha )
        {
            oficinaFicha = OficinaFicha.get( params.sqOficinaFicha.toLong() )
        }

        DadosApoio recusado = dadosApoioService.getByCodigo('TB_SITUACAO_CONVITE_PESSOA', 'CONVITE_RECUSADO')
        DadosApoio naoEnviado = dadosApoioService.getByCodigo('TB_SITUACAO_CONVITE_PESSOA', 'EMAIL_NAO_ENVIADO')
        if (!recusado) {
            render '<h3>Situação CONVITE_RECUSADO não cadastrada na tabela de apoio TB_SITUACAO_CONVITE_PESSOA</h3>'
            return
        }
        if (!naoEnviado) {
            render '<h3>Situação EMAIL_NAO_ENVIADO não cadastrada na tabela de apoio TB_SITUACAO_CONVITE_PESSOA</h3>'
            return
        }
        // ler respostas dos validadores
        List listValidadorFicha = ValidadorFicha.createCriteria().list {
            eq('oficinaFicha.id', params.sqOficinaFicha.toLong())
            // não considerar convites recusados ou vencidos
            cicloAvaliacaoValidador {
                order('id', 'asc')
                or {
                    ne('situacaoConvite', recusado)
                    and {
                        eq('situacaoConvite', naoEnviado)
                        ge('dtValidadeConvite', new Date())
                    }
                }
            }
        }.unique { it.cicloAvaliacaoValidador.validador }

        List listCategoriaIucn = dadosApoioService.getTable('TB_CATEGORIA_IUCN').findAll{ !(it.codigoSistema =~ /NE|OUTRA|AMEACADA|POSSIVELMENTE_EXTINTA/) }.sort{ it.ordem }
        List listMudancaCategoria = dadosApoioService.getTable('TB_MUDANCA_CATEGORIA').sort { it.ordem }

        /**
         * Equipe salve solicitou que o CT e PF possa participar sempre do CHAT em 08/11/2018
         * deve ser alterado tambem no modulo fichaCompletaController.getGridChats()
         */
        convidadoChat = true
        Map taxonStructure = ficha.taxon.getStructure()

        boolean exibirNomeValidador = session.sicae.user.isADM() || session.sicae.user.isPF()
        Map dadosUltimaAvaliacao = ficha.ultimaAvaliacaoNacionalHist
        render(template: '/gerenciarValidacao/formCadFundamentacao', model: [
                  ficha             : ficha
                , oficinaFicha      : oficinaFicha
                , canModify         : canModify
                , listValidadorFicha: listValidadorFicha
                , listCategoriaIucn : listCategoriaIucn
                , convidadoChat     : convidadoChat
                , listMudancaCategoria : listMudancaCategoria
                , dadosUltimaAvaliacao : dadosUltimaAvaliacao
                , taxonStructure : taxonStructure
                , exibirNomeValidador : exibirNomeValidador
                ,contexto             :params.contexto?:''

                //,dadosUltimaAvaliacaoNacional:dadosUltimaAvaliacaoNacional
        ])
    }

    def saveFundamentacao() {
        Map res = [status: 1, msg: '', type: 'modal', errors: []]

        DadosApoio avaliadaRevisada = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'AVALIADA_REVISADA');
        DadosApoio validada = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'VALIDADA');
        DadosApoio emValidacao = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'EM_VALIDACAO');
        DadosApoio validadaEmRevisao = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'VALIDADA_EM_REVISAO');
        DadosApoio conviteRecusado  = dadosApoioService.getByCodigo('TB_SITUACAO_CONVITE_PESSOA', 'CONVITE_RECUSADO');
        DadosApoio categoriaFinalFichaAtual // se não alterar a categoria manter a data da validação

        if (!params.sqFicha) {
            res.msg = 'Ficha não informada.'
            render res as JSON
            return
        }

        // regras para quando houverem apenas 2 validadores
        //int qtdValidadores = ValidadorFicha.countByOficinaFicha(OficinaFicha.get( params.sqOficinaFicha.toLong())).toInteger()
        int qtdValidadores = ValidadorFicha.createCriteria().count{
            eq('oficinaFicha',OficinaFicha.get( params.sqOficinaFicha.toLong() ) )
            cicloAvaliacaoValidador {
                ne('situacaoConvite',conviteRecusado)
            }
        }
        if( qtdValidadores > 1 ) {
            res.msg = 'Não será possível salvar a validação enquanto não houver consenso ' +
                'sobre a <b>categoria</b> e o <b>critério</b> entre os validadores/revisões e o responsável pelo táxon!';

            int qtdRespostaA = 0
            int qtdRespostaB = 0
            int qtdRespostaC = 0
            qtdRespostaA += ( params?.coResultado1 == 'RESULTADO_A' ? 1 : 0);
            qtdRespostaA += ( params?.coResultado2 == 'RESULTADO_A' ? 1 : 0);
            qtdRespostaA += ( params?.coResultado3 == 'RESULTADO_A' ? 1 : 0);
            qtdRespostaB += ( params?.coResultado1 == 'RESULTADO_B' ? 1 : 0);
            qtdRespostaB += ( params?.coResultado2 == 'RESULTADO_B' ? 1 : 0);
            qtdRespostaB += ( params?.coResultado3 == 'RESULTADO_B' ? 1 : 0);
            qtdRespostaC += ( params?.coResultado1 == 'RESULTADO_C' ? 1 : 0);
            qtdRespostaC += ( params?.coResultado2 == 'RESULTADO_C' ? 1 : 0);
            qtdRespostaC += ( params?.coResultado3 == 'RESULTADO_C' ? 1 : 0);
            Integer categoriaSugerida = 0
            String  criterioSugerido  = ''
            categoriaSugerida = params.sqCategoriaSugerida1 == params.sqCategoriaSugerida2 ? ( params.sqCategoriaSugerida1 ? params.sqCategoriaSugerida1.toInteger() :0 ) : categoriaSugerida;
            categoriaSugerida = params.sqCategoriaSugerida1 == params.sqCategoriaSugerida3 ? ( params.sqCategoriaSugerida1 ? params.sqCategoriaSugerida1.toInteger() :0 ) : categoriaSugerida;
            categoriaSugerida = params.sqCategoriaSugerida2 == params.sqCategoriaSugerida3 ? ( params.sqCategoriaSugerida2 ? params.sqCategoriaSugerida2.toInteger() :0 ) : categoriaSugerida;
            criterioSugerido = params.dsCriterioSugerido1 == params.dsCriterioSugerido2 ? params.dsCriterioSugerido1 ?:'' : criterioSugerido;
            criterioSugerido = params.dsCriterioSugerido1 == params.dsCriterioSugerido3 ? params.dsCriterioSugerido1 ?:'' : criterioSugerido;
            criterioSugerido = params.dsCriterioSugerido2 == params.dsCriterioSugerido3 ? params.dsCriterioSugerido2 ?:'' : criterioSugerido;

            // tem que haver consenso entre pelo menos 2 validadores
            if( qtdRespostaC > 1 ) {
                // se houveram 2 respostas c e todos sugeriram a mesma categoria e criterios então aceitar se o PF acatar a sugestão
                if ( categoriaSugerida != params.sqCategoriaIucn.toInteger() || criterioSugerido != params.dsCriterioAvalIucn ) {
                    res.msg = 'A categoria e critério devem ser iguais aos sugeriodos pelos validadores ou pela revsião da coordenação.'
                    render res as JSON
                    return;
                }
            } else if( qtdRespostaA < 2 && qtdRespostaB < 2 ) {
                // Respostas A e B - so pode gravar se o PF informar a mesma caregoria e criterio
                if( qtdRespostaA > 0 && qtdRespostaB > 0) {
                    if ( params.sqCategoriaOficina != params.sqCategoriaIucn || params.dsCriterioAvalIucnOficina != params.dsCriterioAvalIucn ) {
                        res.msg = 'A categoria e critério devem ser os aceitos pelos validadores.'
                        render res as JSON
                        return
                    }
                } else {
                    render res as JSON
                    return;
                }
            } else {
                // se houverem 2 resposta A ou B então o pf só pode gravar se for a mesma categoria e criterio da avaliação
                String resultadoAouB = (qtdRespostaA > qtdRespostaB ? 'A' : 'B');
                if( params.sqCategoriaOficina != params.sqCategoriaIucn ){
                    res.msg = 'Houveram <b>' + qtdRespostaA + ' resultados ' + resultadoAouB + '</b>, a categoria não pode ser alterada.'
                    render res as JSON
                    return
                }
                if( params.dsCriterioAvalIucnOficina != params.dsCriterioAvalIucn ){
                    res.msg= 'Houveram <b>' + qtdRespostaA + ' resultados ' + resultadoAouB + '</b>, o critério não pode ser alterado.'
                    render res as JSON
                    return
                }
            }
        }

        if (params.sqCategoriaIucn && !params.dsJustificativa) {
            res.msg = 'Necessário informar a justificativa.'
            render res as JSON
            return
        }

        Ficha ficha = Ficha.get(params.sqFicha.toInteger())
        if (!ficha) {
            res.msg = 'Ficha Inválida'
            render res as JSON
            return
        }
        categoriaFinalFichaAtual = ficha.categoriaFinal

        if( ficha.situacaoFicha.codigoSistema == 'FINALIZADA' ) {
            res.msg = 'Ficha já está FINALIZADA'
            render res as JSON
            return
        }

        if( ficha.situacaoFicha.codigoSistema == 'PUBLICADA' ) {
            res.msg = 'Ficha já está PUBLICADA'
            render res as JSON
            return
        }

        if( ! params.stPossivelmenteExtinta )
        {
            params.stPossivelmenteExtinta = null
        }
        ficha.categoriaFinal = null

        // só alterar a situacao da ficha se ela estiver na situação Validada
        if( ficha.situacaoFicha == validada ) {
            ficha.situacaoFicha = emValidacao
        }
        ficha.dsJustMudancaCategoria = Util.trimEditor( params.dsJustMudancaCategoria )

        if ( params.sqCategoriaIucn ) {
            ficha.categoriaFinal = DadosApoio.get(params.sqCategoriaIucn.toInteger())
            if( ficha.categoriaFinal.codigoSistema != 'CR' )
            {
                params.stPossivelmenteExtinta = null
            }
            else
            {
                if( ! params.stPossivelmenteExtinta )
                {
                    params.stPossivelmenteExtinta = 'N'
                }
            }
        }
        ficha.dsCriterioAvalIucnFinal = params.dsCriterioAvalIucn ?: null
        ficha.stPossivelmenteExtintaFinal = params.stPossivelmenteExtinta ?: null
        ficha.dsJustificativaFinal = Util.trimEditor( params.dsJustificativa )
        if ( ! ficha.categoriaFinal ) {
            ficha.dtAceiteValidacao = null
        } else {
            if ( ficha.dsJustificativaFinal ) {
                if( ! ficha.dtAceiteValidacao || ficha.categoriaFinal != categoriaFinalFichaAtual ) {
                    ficha.dtAceiteValidacao = new Date()
                }
                VwFicha vwFicha = VwFicha.get( ficha.id )
                if( vwFicha.qtdPendencia == 0 ) {
                    ficha.situacaoFicha = validada
                }
                else {
                    ficha.situacaoFicha = validadaEmRevisao
                }

                // limpar motivo mudanca se não tiver tido mudanca de categoria
                Map dadosUltimaAvaliacao = ficha.ultimaAvaliacaoNacionalHist
                if(dadosUltimaAvaliacao.sqCategoriaIucn &&  dadosUltimaAvaliacao.sqCategoriaIucn.toInteger() == ficha.categoriaFinal.id.toInteger() )
                {
                    FichaMudancaCategoria.findAllByFicha(ficha).each{ it.delete() }
                    ficha.dsJustMudancaCategoria = null
                }
            }
        }
        ficha.save(flush: true)

        if( params.sqOficinaFicha )
        {
            OficinaFicha oficinaFicha           = OficinaFicha.get( params.sqOficinaFicha.toLong() )
            oficinaFicha.categoriaIucn          = ficha.categoriaFinal
            oficinaFicha.dsCriterioAvalIucn     = ficha.dsCriterioAvalIucnFinal
            oficinaFicha.dsJustificativa        = ficha.dsJustificativaFinal
            oficinaFicha.stPossivelmenteExtinta = ficha.stPossivelmenteExtintaFinal

            if( ficha.situacaoFicha == validada || ficha.situacaoFicha==validadaEmRevisao ){
                oficinaFicha.dtAvaliacao        = ficha.dtAceiteValidacao
                oficinaFicha.situacaoFicha      = ficha.situacaoFicha
            } else {
                oficinaFicha.dtAvaliacao            = null
            }
            oficinaFicha.save( flush:true )
        }

        res.type = 'success'
        res.msg = getMsg(1)
        render res as JSON
    }

    // inicio convidar validador para uma espécies definida (gride acompanhamento)
    def getFormConvidarValidador(){
        String noCientifico     = params.noCientifico ?: ''
        String noOficina
        boolean canModify=true

        if( !params.sqOficinaFicha ){
            render '<3>Parâmetros insuficientes</h3>'
            return
        }
        OficinaFicha oficinaFicha = OficinaFicha.get( params.sqOficinaFicha.toLong() )
        if( ! oficinaFicha ){
            render '<3>Parâmetros inválidos</h3>'
            return
        }
        noOficina = Util.capitalize( oficinaFicha.oficina.noOficina )

        render( template:'formConvidarValidador',model:[
            noCientifico            : noCientifico
            ,noOficina              : noOficina
            ,sqOficinaFicha         : params.sqOficinaFicha
            ,canModify              : canModify])
    }

    def saveFormConvidarValidador(){
        Map res =[status:0, msg:'', type:'success']
        String emailFrom = grailsApplication.config.app.email ?: 'salve@icmbio.gov.br'
        try{
            if( ! params.sqPessoaValidador ){
                throw  new Exception('Validador não selecionado')
            }
            if( params.deEmailValidador ){
                if( !params.deAssuntoEmailValidador || !params.txMensagemEmailValidador){
                    throw  new Exception('Dados para envio do e-mail incompletas')
                }
            } else if( params.txMensagemEmailValidador ) {
                throw  new Exception('E-mail não informado')
            }

            Pessoa pessoa = Pessoa.get( params.sqPessoaValidador.toLong())
            if( !pessoa ){
                throw  new Exception('Validador inválido')
            }
            OficinaFicha oficinaFicha = OficinaFicha.get( params.sqOficinaFicha.toLong())
            if( !oficinaFicha ){
                throw  new Exception('Oficina inválida')
            }

            // Validador convidado não pode ter papel de Avaliador na ultima oficina de avaliação da ficha
            List rows = sqlService.execSql("""with cte as (
                        select oficina.sq_oficina, oficina.no_oficina
                        from salve.oficina
                        inner join salve.oficina_ficha on oficina_ficha.sq_oficina = oficina.sq_oficina
                        inner join salve.dados_apoio tipo on tipo.sq_dados_apoio = oficina.sq_tipo_oficina
                        where tipo.cd_sistema = 'OFICINA_AVALIACAO'
                          and oficina_ficha.sq_ficha = ${oficinaFicha.ficha.id.toString()}
                        order by oficina.dt_fim desc
                        limit 1
                    )
                    select cte.no_oficina
                    from salve.oficina_ficha_participante ofp
                    inner join salve.oficina_ficha on oficina_ficha.sq_oficina_ficha = ofp.sq_oficina_ficha
                    inner join salve.oficina_participante op on op.sq_oficina_participante = ofp.sq_oficina_participante
                    inner join salve.dados_apoio papel on papel.sq_dados_apoio = ofp.sq_papel_participante
                    inner join cte on cte.sq_oficina = oficina_ficha.sq_oficina
                    where oficina_ficha.sq_ficha = ${oficinaFicha.ficha.id.toString()}
                    and op.sq_pessoa = ${pessoa.id.toString()}
                    and papel.cd_sistema ='AVALIADOR'
                    """)
            if( rows ){
                throw new Exception(Util.capitalize(pessoa.noPessoa) + ' é AVALIADOR da espécie na ' + rows[0].no_oficina + ' e portanto não pode ser convidado para ser validador.')
            }

            // verificar se o validador já existe na tabela ciclo_avaliacao_validadora
            CicloAvaliacaoValidador cav
            rows = sqlService.execSql("""select cav.sq_ciclo_avaliacao_validador
                from salve.ciclo_avaliacao_validador cav
                inner join salve.validador_ficha vf on vf.sq_ciclo_avaliacao_validador = cav.sq_ciclo_avaliacao_validador
                inner join salve.oficina_ficha on oficina_ficha.sq_oficina_ficha = vf.sq_oficina_ficha
                inner join salve.dados_apoio situacao on situacao.sq_dados_apoio = cav.sq_situacao_convite
                where cav.sq_validador = ${pessoa.id.toString()}
                and oficina_ficha.sq_oficina_ficha = ${oficinaFicha.id.toString()}
                and situacao.cd_sistema = 'CONVITE_ACEITO'
                """)
            if( rows ){
                cav = CicloAvaliacaoValidador.get(rows[0].sq_ciclo_avaliacao_validador.toLong() )
                //throw new Exception(Util.capitalize(pessoa.noPessoa) + ' já é VALIDADOR desta espécie')
            } else {

                //DadosApoio situacaoEmailNaoEnviado = dadosApoioService.getByCodigo('TB_SITUACAO_CONVITE_PESSOA', 'EMAIL_NAO_ENVIADO')
                DadosApoio situacaoConviteAceito = dadosApoioService.getByCodigo('TB_SITUACAO_CONVITE_PESSOA', 'CONVITE_ACEITO')
                cav = new CicloAvaliacaoValidador()
                cav.oficina         = oficinaFicha.oficina
                cav.cicloAvaliacao  = oficinaFicha.oficina.cicloAvaliacao
                cav.validador       = pessoa
                cav.situacaoConvite = situacaoConviteAceito
                cav.noUsuario       = session.sicae.user.noPessoa // quem convidou
            }
            cav.deEmail = params.deEmailValidador.toLowerCase()
            if ( cav.save(flush: true ) ) {
                ValidadorFicha vf = ValidadorFicha.findByCicloAvaliacaoValidadorAndOficinaFicha(cav,oficinaFicha)
                if( ! vf) {
                    vf = new ValidadorFicha()
                    vf.cicloAvaliacaoValidador = cav
                    vf.oficinaFicha = oficinaFicha
                    vf.save(flush: true)
                }
            }
            // ENVIAR O EMAIL PARA O VALIDADOR
            String emailMessage =''
            if( cav.deEmail ){
                // ENVIAR COM COPIA PARA O PONTO FOCAL
                DadosApoio papelPF = dadosApoioService.getByCodigo('TB_PAPEL_FICHA', 'PONTO_FOCAL')
                FichaPessoa fichaPessoa = FichaPessoa.createCriteria().get {
                    eq('inAtivo', 'S')
                    eq('papel', papelPF)
                    eq('vwFicha.id', oficinaFicha.vwFicha.id)
                    maxResults(1)
                }
                String emailPf = fichaPessoa?.pessoa?.email
                String nomePf  = fichaPessoa?.pessoa?.noPessoa

                String urlSalve = Util.getBaseUrl( request.getRequestURL().toString() )
                urlSalve = urlSalve.replaceAll(/http:\/\/10\.197\.93\.125:8080\/salve-estadual/,'https://dsv.biodiversidade.icmbio.gov.br/salve')
                String urlSalveExterno = grailsApplication.config.url.sicae
                urlSalveExterno = ((urlSalveExterno ?: 'https://sicae.sisicmbio.icmbio.gov.br') + '/usuario-externo/login').replaceAll('//usuario-externo/','/usuario-externo/')
                // alterar as variávies do texto do email
                String textoEmailCompleto = params.txMensagemEmailValidador
                textoEmailCompleto = textoEmailCompleto.replaceAll(/\{nome\}/, pessoa.noPessoa)
                textoEmailCompleto = textoEmailCompleto.replaceAll(/\{fichas\}/, Util.ncItalico(oficinaFicha.ficha.nmCientifico))
                textoEmailCompleto = textoEmailCompleto.replaceAll(/\{linkSalve\}/, '<a target="_blank" href="' + urlSalve + '">Clique aqui para acessar o Sistema SALVE.</a>')
                textoEmailCompleto = textoEmailCompleto.replaceAll(/\{linkSalveExterno\}/, '<a target="_blank" href="' + urlSalveExterno + '">Clique aqui para acessar o Sistema SALVE - EXTERNO.</a>')
                textoEmailCompleto = Util.trimEditor(textoEmailCompleto.replaceAll(/\{emailConvidado\}/, cav.deEmail))
                if( emailService.sendTo(cav.deEmail.toString(),
                    params.deAssuntoEmailValidador.toString(),
                    textoEmailCompleto.toString(),
                    emailFrom.toString(),
                    [:],
                    emailFrom.toString() + ( emailPf ? ','+emailPf : '') ) ) {
                    emailMessage='<br>Email enviado para o validador(a)'
                    if( emailPf) {
                        emailMessage += ' com cópia para o(a) PF '+ Util.nomeAbreviado(nomePf) +' ('+emailPf + ') '
                    } else {
                        if( nomePf ) {
                            emailMessage += '. PF ' + nomePf + ' está sem e-mail no SICA-e.'
                        } else {
                            emailMessage += '.  PF da ficha não encontrado.'
                        }
                    }
                    //cav.situacaoConvite = dadosApoioService.getByCodigo('TB_SITUACAO_CONVITE_PESSOA', 'EMAIL_ENVIADO')
                    //cav.save( flush:true )
                }
            }
           res.msg='Dados gravados com SUCESSO'+emailMessage
           res.type    = 'modal'
        } catch( Exception e ) {
            res.type    = 'modal'
            res.status  = 1
            res.msg     = e.getMessage()
        }



        render res as JSON
    }

    //-----------------------------------------------------------------

    def select2Validador() {

        FichaController fichaController = new FichaController()
        fichaController.select2PessoaFisica()
        return

        /*
        List lista
        Map data = ['items': []]
        if (!params?.q?.trim()) {
            render 'sem parametros'
            return
        }
        String q = params.q.toString().trim()
        lista = OficinaFichaParticipante.executeQuery("""
        select new map( b.pessoa.id as sqPessoa
        ,p.noPessoa as noPessoa
        ,pf.nuCpf as nuCpf
        ,b.deEmail as txEmail )
        from OficinaFichaParticipante a, OficinaParticipante b, Pessoa p, PessoaFisica pf, DadosApoio d
        where a.oficinaParticipante.id = b.id
        and b.pessoa.id = p.id
        and b.pessoa.id = pf.id
        and a.papel.id = d.id
        and d.codigoSistema = 'VALIDADOR'
        and lower( p.noPessoa ) like :q
        order by p.noPessoa
        """, [q: '%' + q.toLowerCase().trim() + '%'])
        String cpfFormatado = ''
        if (lista) {
            lista.each {
                cpfFormatado = Util.formatCpf(it.nuCpf)
                // considerar somente 1 email e dar preferencia pelo icmbio.gov.br
                Map existente = data.items.find { obj ->
                    obj.cpf == cpfFormatado
                }
                if (existente) {
                    if (it.txEmail.indexOf('icmbio.gov.br') > 0) {
                        existente.email = it.txEmail.toLowerCase()
                    }
                }else {
                    data.items.push(['id'       : it.sqPessoa,
                                     'descricao': Util.capitalize(it.noPessoa) + ' - ' + cpfFormatado,
                                     'cpf'      : cpfFormatado,
                                     'email'    : it.txEmail.toLowerCase()
                    ])
                }
            }
            render data as JSON
            return
        }
        render ''
        */
    }

    def getFormEditarFichaValidador() {
        List listNivelTaxonomico = NivelTaxonomico.list()
        List listCategorias         = dadosApoioService.getTable('TB_CATEGORIA_IUCN').findAll{ it.codigoSistema != 'OUTRA' }.sort{ it.ordem };
        List listGrupos             = dadosApoioService.getTable('TB_GRUPO').sort{ it.descricao }

        CicloAvaliacaoValidador reg = CicloAvaliacaoValidador.get(params.sqCicloAvaliacaoValidador.toLong())
        List listValidadores = CicloAvaliacaoValidador.executeQuery("""select new map( a.id as sqCicloAvaliacaoValidador, b.noPessoa as noPessoa )
            from CicloAvaliacaoValidador a
            inner join a.validador b
            where a.oficina.id = :sqOFicina and a.id <> :idAtual
            order by b.noPessoa
            """, [sqOFicina: reg.oficina.id, idAtual: params.sqCicloAvaliacaoValidador.toLong()])

        render(template: 'formEditarFichaValidador', model: [rndId                : params.sqCicloAvaliacaoValidador
                                                             , listNivelTaxonomico: listNivelTaxonomico
                                                             , listValidadores    : listValidadores
                                                             , listCategorias     : listCategorias
                                                             , validadorNome      : params.validador
                                                             , listGrupos         : listGrupos])
    }

    def getGridEditarFichasValidador() {

        params.sqCicloAvaliacaoValidador = (params.sqCicloAvaliacaoValidador ?: '0')

        String query = """select new map( a.id as sqValidadorFicha
                        , b.nmCientifico as nmCientifico
                        , c.jsonTrilha||'' as jsonTrilha
                        , b.categoriaIucn.codigoSistema
                        , d.codigoSistema as cdCategoriaIucn
                        , d.descricao as deCategoriaIucn
                        , b.grupo.descricao as dsGrupo
                         )
                        from ValidadorFicha a
                        inner join a.oficinaFicha.ficha b
                        left join b.categoriaIucn d
                        inner join b.taxon c
                        where a.cicloAvaliacaoValidador.id = :id
                        and a.resultado is null
                        """
        if (params.sqTaxonFiltro) {
            //Taxon taxon = Taxon.get( params.sqTaxonFiltro.toLong() )
            query += "\nand c.jsonTrilha||'' like '%" + params.noTaxonFiltro + "%'"
        }
        if( params.sqUnidadeFiltro ) {
            query += "\nand b.unidade.id = " + params.sqUnidadeFiltro
        }
        if( params.sqCategoriaFiltro ) {
            query += "\nand b.categoriaIucn.id in ( ${ params.sqCategoriaFiltro } )"
        }
        if( params.sqGrupoFiltro ) {
            query += "\nand b.grupo.id in ( ${ params.sqGrupoFiltro } )"
        }
        query += "\norder by b.nmCientifico"

        List lista = ValidadorFicha.executeQuery(query, [id: params.sqCicloAvaliacaoValidador.toLong()])
        render(template: 'divGridEditarFichasValidador', model: [rndId: params.sqCicloAvaliacaoValidador, lista: lista])
    }

    // ACOES
    def deleteFichaValidador() {
        Map res = [status: 1, msg: 'Ver parametros', type: 'error', errors: []]
        if (!params.ids) {
            res.msg = 'Nenhuma ficha selecionada!'
            render res as JSON
        }
        String query
        if (params.dupla == 'S') {
            query = """delete from ValidadorFicha where oficinaFicha.id in
                     (select oficinaFicha.id from ValidadorFicha where id in (${params.ids}) )
            """
        }else {
            query = """delete from ValidadorFicha where id in (${params.ids})"""
        }
        try {
            ValidadorFicha.executeUpdate(query)
            res.msg = 'Operação realizada com SUCESSO!'
            res.status = 0
            res.type = 'success'
        } catch (Exception e) {
            println e.getMessage()
            res.msg = e.getMessage()
        }
        render res as JSON
    }


    def transferirFichaValidador() {
        Map res = [status: 1, msg: 'Ver parametros', type: 'error', errors: []]
        /*println params
        res.msg = 'Parametros!'
        render res as JSON
        return
*/

        if (!params.ids) {
            res.msg = 'Nenhuma ficha selecionada!'
            render res as JSON
        }

        /** /
        println 'Parametros'
        println params
        res.msg = 'Parametros!'
        render res as JSON
        return
        /**/


        // validar se as fichas selecionadas podem ser atribuidas ao validador
        List validadorDestinatario = sqlService.execSql("select sq_validador from salve.ciclo_avaliacao_validador where sq_ciclo_avaliacao_validador = :sqCicloAvaliacaoValidador"
        ,[sqCicloAvaliacaoValidador:params.sqCicloAvaliacaoValidorPara.toLong()])

        if( ! validadorDestinatario ) {
            res.msg = 'Validador(a) destinatário(a) não informado(a)!'
            render res as JSON
        }

        Map resultadoValidacao = [:]
        if ( params.saveType == 'validate' || params.saveType == 'saveValids' ) {
            List fichasTransferir = sqlService.execSql("""select oficina_ficha.sq_oficina_ficha
            from salve.validador_ficha
            inner join salve.oficina_ficha on oficina_ficha.sq_oficina_ficha = validador_ficha.sq_oficina_ficha
            where validador_ficha.sq_validador_ficha in (${params.ids})""")
            resultadoValidacao = _validarFichasValidador(validadorDestinatario.sq_validador, fichasTransferir.sq_oficina_ficha)
            if( params.saveType == 'validate' && resultadoValidacao.fichasValidadorInvalidas ) {
                res.msg = ''
                res.errors = []
                res.type = 'info'
                res.status = 0
                res.fichasValidadorInvalidas = resultadoValidacao.fichasValidadorInvalidas
                render res as JSON
                return
            }
        }

        List fichasInvalidas = resultadoValidacao.papeis ? resultadoValidacao.papeis.unique{ it.sq_ficha }.sq_ficha : []

        String andFichasInvalidas = fichasInvalidas ? """and not exists ( select x.sq_validador_ficha from salve.validador_ficha x
                              inner join salve.oficina_ficha as y on y.sq_oficina_ficha = x.sq_oficina_ficha
                              where x.sq_validador_ficha = validador_ficha.sq_validador_ficha
                              and y.sq_ficha in (${fichasInvalidas.join(',')} )
                            )""" : ''

        String cmdSql = """update salve.validador_ficha set sq_ciclo_avaliacao_validador = ${params.sqCicloAvaliacaoValidorPara}
                        where validador_ficha.sq_ciclo_avaliacao_validador = ${ params.sqCicloAvaliacaoValidorDe }
                          and validador_ficha.sq_validador_ficha in ( ${params.ids})
                          and not exists ( select x.sq_validador_ficha from salve.validador_ficha x
                             where x.sq_ciclo_avaliacao_validador = ${params.sqCicloAvaliacaoValidorPara}
                               and x.sq_oficina_ficha = validador_ficha.sq_oficina_ficha)
                          ${andFichasInvalidas}
                        """
        try {
            sqlService.execSql(cmdSql)
            res.msg = 'Operação realizada com SUCESSO!'
            res.status = 0
            res.type = 'success'
        } catch (Exception e) {
            println e.getMessage()
            res.msg = e.getMessage()
        }
        render res as JSON
    }

    /**
     * pesquisa taxon para campo select2 do formulário de transferir / excluir fichas
     * dos validadores convidados
     * @return
     */
    def select2Taxon() {

        if ( ! params.sqCicloAvaliacaoValidador ) {
            render ''
            return
        }
        if (!params.q) {
            // exibir todas as opções independente do filtro para estes niveis
            if (params.nivel != 'REINO' && params.nivel != 'FILO' && params.nivel != 'CLASSE' && params.nivel != 'ORDEM') {
                render ''
                return
            }
            params.q = ''
        }
        // remover caracteres inválidos
        params.q = params.q.trim().replaceAll(/[^a-zA-Z-]/, ' ').replaceAll('/  /g', ' ')
        List niveisTaxonomicos = ['reino', 'filo', 'classe', 'ordem', 'familia', 'subfamilia', 'tribo', 'genero', 'especie', 'subespecie'];
        Map data = [page        : params?.page ? params?.page.toInteger() + 1 : 1,
                    totalRecords: 0,
                    items       : []]
        String nivel = params?.nivel?.toString()?.toLowerCase()
        /*
        String query ="""select new map( a.oficinaFicha.ficha.taxon.jsonTrilha||'' as jsonTrilha ) from ValidadorFicha a
        where lower( a.oficinaFicha.ficha.taxon.jsonTrilha||'') like :q
        and a.cicloAvaliacaoValidador.id=:sqCicloAvaliacaoValidador
        """
        JSONObject json
        List hint=[]
        ValidadorFicha.executeQuery(query,[q:'%'+params.q.toLowerCase()+'%',sqCicloAvaliacaoValidador:params.sqCicloAvaliacaoValidador.toLong()]).each {
            json = JSON.parse( it.jsonTrilha )
            if( json[nivel].no_taxon.toLowerCase().indexOf( params.q.toLowerCase() ) > -1 ) {
                if (!data.items.id?.contains(json[nivel].sq_taxon)) {
                    // criar array com trilha completa para gerar o hint no combo
                    niveisTaxonomicos.each {
                        if( json[it]?.no_taxon)
                        {
                            hint.push( json[it].no_taxon )
                        }
                    }
                    data.items.push([id         : json[nivel].sq_taxon
                                     , descricao: json[nivel].no_taxon
                                     , txHint   : hint.join(' > ')
                    ])
                }
            }
        }
        */

        String cmdSql = """select taxon.json_trilha from salve.validador_ficha
            inner join salve.oficina_ficha on oficina_ficha.sq_oficina_ficha = validador_ficha.sq_oficina_ficha
            inner join salve.ficha on ficha.sq_ficha = oficina_ficha.sq_ficha
            inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
            where validador_ficha.sq_ciclo_avaliacao_validador = :sqCicloAvaliacaoValidador
            and taxon.json_trilha ->'${nivel}'->>'no_taxon' ilike '%${params.q}%'
            """
        sqlService.execSql(cmdSql,[sqCicloAvaliacaoValidador:params.sqCicloAvaliacaoValidador.toLong()]).each {
            try {
                def json = JSON.parse(it.json_trilha.toString())
                // construir a árvore taxonômica para exibir no hint do select
                List hint = []
                niveisTaxonomicos.each {
                    try {
                        if (json[it]?.no_taxon) {
                            hint.push(json[it].no_taxon)
                        }
                    } catch (Exception e) {
                    }
                }
                Map tmpMap = [id         : json[nivel].sq_taxon
                              , descricao: json[nivel].no_taxon
                              , txHint   : hint.join(' > ') ]

                // evitar duplicidade na listagem
                if( ! data.items.find { it.id.toIntege() == tmpMap.id .toInteger() } )
                {
                    data.items.push( tmpMap )
                }
            } catch( Exception e ){}
        }
        render data as JSON
    }

    //---------------------------------------------------------------------------
    // REVISAO DA VALIDAÇÃO PELA COORDENACAO
    //----------------------------------------------------------------------------
    def getFormRevisaoCoordenacao(){
        boolean canModify = true

        ValidadorFicha validadorFicha = ValidadorFicha.get( params.sqValidadorFicha.toLong() )
        ValidadorFichaRevisao validadorFichaRevisao = ValidadorFichaRevisao.findByValidadorFicha(validadorFicha)
        String nomeValidador    = validadorFicha.cicloAvaliacaoValidador.validador.noPessoa
        List listCategoriaIucn  = dadosApoioService.getTable('TB_CATEGORIA_IUCN').findAll{ !(it.codigoSistema =~ /NE|OUTRA|AMEACADA/) }.sort{ it.ordem }
        List listResultados     = dadosApoioService.getTable('TB_RESULTADO_VALIDADOR')
        Ficha ficha             = validadorFicha.oficinaFicha.ficha
        // inicilizar a tela com o resultado do validador
        if( ! validadorFichaRevisao ) {
            validadorFichaRevisao = new ValidadorFichaRevisao()
            validadorFichaRevisao.resultado = validadorFicha.resultado
            validadorFichaRevisao.categoriaSugerida = validadorFicha.categoriaSugerida
            validadorFichaRevisao.deCriterioSugerido = validadorFicha.deCriterioSugerido
            validadorFichaRevisao.stPossivelmenteExtinta = validadorFichaRevisao.stPossivelmenteExtinta
        }

        render( template:'formCadRevisaoCoordenacao',model:[
            nomeValidador           : nomeValidador
            ,sqValidaorFicha        : validadorFicha.id
            ,ficha                  : ficha
            ,listCategoriaIucn      : listCategoriaIucn
            ,listResultados         : listResultados
            ,validadorFichaRevisao  : validadorFichaRevisao
            ,numValidador           : params.numValidador?:''
            ,canModify              : canModify])

    }


    def saveFormRevisaoCoordenacao(){
        Map res  = [status:1,msg:'',type:'error', errors:[], data:[:]]
        ValidadorFicha validadorFicha
        DadosApoio resultado
        DadosApoio categoriaSugerida

        try {
            if (!params.sqValidadorFicha) {
                res.errors.push('Validador não informado.')
            } else {
                validadorFicha = ValidadorFicha.get(params.sqValidadorFicha.toLong())
            }

            if (!params.sqFicha) {
                res.errors.push('Espécie não informada.')
            }

            if (!params.sqResultado) {
                res.errors.push('Resultado revisado não informado.')
            } else {
                resultado = DadosApoio.get(params.sqResultado.toLong())
            }

            params.justificativa = Util.trimEditor(params.txJustificativa)
            if (!params.txJustificativa) {
                res.errors.push('Necessário informar a justificativa.')
            }


            if ( resultado ) {
                //---------------------------------------------------------------------------
                if (resultado.codigoSistema == 'RESULTADO_A') {
                    params.sqCategoriaSugerida = null
                    params.deCriterioSugerido = null
                } else if (resultado.codigoSistema == 'RESULTADO_B') {
                    params.sqCategoriaSugerida = null
                    params.deCriterioSugerido = null
                } else if (resultado.codigoSistema == 'RESULTADO_C') {
                    if (!params.sqCategoriaSugerida) {
                        res.errors.push('Necessário informar a categoria.')
                    } else {
                        categoriaSugerida = DadosApoio.get(params.sqCategoriaSugerida.toLong())
                        if (!params.deCriterioSugerido && ("EN|VU|CR").indexOf(categoriaSugerida.codigoSistema) > -1) {
                            res.errors.push('Necessário informar a critério.')
                        }

                        if (categoriaSugerida.codigoSistema != 'CR' || !params?.stPossivelmventeExtinta) {
                            params.stPossivelmventeExtinta = null
                        }
                        if (categoriaSugerida.codigoSistema == 'CR' && params?.stPossivelmventeExtinta != 'S') {
                            params.stPossivelmventeExtinta = 'N'
                        }
                    }
                }
            }
            if( ! res.errors ) {
                if ( ! validadorFicha.revisaoCoordenacao ) {
                    validadorFicha.revisaoCoordenacao = new ValidadorFichaRevisao()
                    validadorFicha.revisaoCoordenacao.validadorFicha = validadorFicha
                }

                validadorFicha.revisaoCoordenacao.resultado              = resultado
                validadorFicha.revisaoCoordenacao.categoriaSugerida      = categoriaSugerida ?: null
                validadorFicha.revisaoCoordenacao.deCriterioSugerido     = params.deCriterioSugerido ?: null
                validadorFicha.revisaoCoordenacao.stPossivelmenteExtinta = params.stPossivelmenteExtinta ?:null
                validadorFicha.revisaoCoordenacao.txJustificativa        = params.txJustificativa ?:null

                if( ! validadorFicha.revisaoCoordenacao.validate() ){
                    println validadorFicha.revisaoCoordenacao.getErrors()
                    throw new Exception('Dados incompletos ao para gravar a revisão')
                }

                validadorFicha.revisaoCoordenacao.save()
                validadorFicha.save( flush:true )

                res.data    = [sqCicloAvaliacaoValidador: validadorFicha.revisaoCoordenacao.id]
                res.msg     = 'Revisão gravada com SUCESSO!'
                res.type    = 'success'
                res.status  = 0

                // atualizar a situacao da ficha em função das respostas registradas
                // atualizar o resultado final se for A-A ou B-A
                Map resultados = validadorFicha.getResultadosFicha()

                if( resultados.qtdValidadores > 1 )
                {
                    Ficha ficha = validadorFicha.oficinaFicha.ficha
                    OficinaFicha oficinaFicha = validadorFicha.oficinaFicha
                    PessoaFisica validador = validadorFicha.cicloAvaliacaoValidador.validador

                    if( resultados.qtdA > 1 ) // os dois foram A
                    {
                        // finalizar avaliação
                        ficha.categoriaFinal            = ficha.categoriaIucn
                        ficha.dsCriterioAvalIucnFinal   = ficha.dsCriterioAvalIucn
                        ficha.dsJustificativaFinal      = ficha.dsJustificativa
                        ficha.stPossivelmenteExtintaFinal = ficha.stPossivelmenteExtinta
                        // se a ficha tiver pendência passar para VALIDADA_EM_REVISAO
                        VwFicha vwFicha = VwFicha.get( ficha.id )
                        if( vwFicha.nuPendencia == 0 ) {
                            ficha.situacaoFicha = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'VALIDADA')
                        }
                        else
                        {
                            ficha.situacaoFicha = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'VALIDADA_EM_REVISAO')
                        }
                        ficha.dtAceiteValidacao    = new Date()
                        oficinaFicha.situacaoFicha = ficha.situacaoFicha
                    }
                    else if( resultados.qtdB > 1 || (resultados.qtdB==1 && resultados.qtdA == 1 ) ) // foi BB ou AB ou BA
                    {
                        // grava categoria e critério mas não finaliza
                        ficha.categoriaFinal            = ficha.categoriaIucn
                        ficha.dsCriterioAvalIucnFinal   = ficha.dsCriterioAvalIucn
                        ficha.stPossivelmenteExtintaFinal = ficha.stPossivelmenteExtinta
                        ficha.dsJustificativaFinal      = null
                        ficha.situacaoFicha             = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'VALIDADA_EM_REVISAO')
                        oficinaFicha.situacaoFicha      = ficha.situacaoFicha
                    }
                    // sempre que um validador salvar tem que limpar a validação final se tiver preenchida
                    else //if( resultados.qtdC > 0 ) // pelo menos 1 foi C
                    {
                        ficha.categoriaFinal                = null
                        ficha.dsCriterioAvalIucnFinal       = null
                        ficha.dsJustificativaFinal          = null
                        ficha.stPossivelmenteExtintaFinal   = null
                        ficha.dtAceiteValidacao             = null
                        ficha.situacaoFicha                 = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'EM_VALIDACAO')
                        oficinaFicha.situacaoFicha          = ficha.situacaoFicha
                        oficinaFicha.dtAvaliacao = null
                        // se 2 responderam C e houve consenso na categoria e criterio, gravar a data da validacao na ficha
                        /*if( resultados.qtdC > 1  ) {
                            // se 2 responderam C e houve consenso na categoria e criterio, gravar a data da validacao na ficha
                            oficinaFicha.dtAvaliacao = new Date()
                        }*/
                    }
                    oficinaFicha.save()
                    ficha.save(flush:true)
                }
            }
        } catch( Exception e ){
            res.msg     = e.getMessage()
            res.type    = 'error'
            res.status  = 1
        }
    render res as JSON
    }

    def deleteRevisaoCoordenacao(){
        Map res  = [status:1,msg:'',type:'error', errors:[]]
        Long sqValidadorFicha
        ValidadorFicha validadorFicha
       // ValidadorFichaRevisao validadorFichaRevisao

        try {
            if (!params.sqValidadorFichaRevisao) {
                throw new Exception('Revisão não informada.')
            }
            if (!params.sqValidadorFicha) {
                throw new Exception('Revisão não informada.')
            }

            ValidadorFichaRevisao.get( params.sqValidadorFichaRevisao.toLong() ).delete( flush:true )
            res.msg = 'Revisão excluída com SUCESSO!'
            res.type = 'success'
            res.status = 0

            // atualizar a situacao da ficha em função das respostas registradas
            validadorFicha = ValidadorFicha.get( params.sqValidadorFicha.toLong() )
            if ( validadorFicha ) {
                // atualizar o resultado final se for A-A ou B-A
                Map resultados = validadorFicha.resultadosFicha

                if (resultados.qtdValidadores < 3) {
                    Ficha ficha = validadorFicha.oficinaFicha.ficha
                    OficinaFicha oficinaFicha = validadorFicha.oficinaFicha
                    if (resultados.qtdA == 2) // os dois foram A
                    {
                        // finalizar avaliação
                        ficha.categoriaFinal = ficha.categoriaIucn
                        ficha.dsCriterioAvalIucnFinal = ficha.dsCriterioAvalIucn
                        ficha.dsJustificativaFinal = ficha.dsJustificativa
                        ficha.stPossivelmenteExtintaFinal = ficha.stPossivelmenteExtinta
                        // se a ficha tiver pendência passar para VALIDADA_EM_REVISAO
                        VwFicha vwFicha = VwFicha.get(ficha.id)
                        if (vwFicha.nuPendencia == 0) {
                            ficha.situacaoFicha = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'VALIDADA')
                        } else {
                            ficha.situacaoFicha = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'VALIDADA_EM_REVISAO')
                        }
                        ficha.dtAceiteValidacao = new Date()
                        oficinaFicha.situacaoFicha = ficha.situacaoFicha
                    } else if (resultados.qtdB == 2 || (resultados.qtdB == 1 && resultados.qtdA == 1)) // foi BB ou AB ou BA
                    {
                        // grava categoria e critério mas não finaliza
                        ficha.categoriaFinal = ficha.categoriaIucn
                        ficha.dsCriterioAvalIucnFinal = ficha.dsCriterioAvalIucn
                        ficha.stPossivelmenteExtintaFinal = ficha.stPossivelmenteExtinta
                        ficha.dsJustificativaFinal = null
                        ficha.situacaoFicha = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'VALIDADA_EM_REVISAO')
                        oficinaFicha.situacaoFicha = ficha.situacaoFicha
                    }
                    // sempre que um validador salvar tem que limpar a validação final se tiver preenchida
                    else //if( resultados.qtdC > 0 ) // pelo menos 1 foi C
                    {
                        ficha.categoriaFinal = null
                        ficha.dsCriterioAvalIucnFinal = null
                        ficha.dsJustificativaFinal = null
                        ficha.stPossivelmenteExtintaFinal = null
                        ficha.dtAceiteValidacao = null
                        if (ficha.situacaoFicha.codigoSistema == 'VALIDADA') {
                            ficha.situacaoFicha = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'EM_VALIDACAO')
                        } else {
                            ficha.situacaoFicha = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'EM_VALIDACAO')
                            ficha.dtAceiteValidacao = null
                        }
                        oficinaFicha.situacaoFicha = ficha.situacaoFicha
                    }
                    oficinaFicha.save()
                    ficha.save(flush: true)
                }
            }
        } catch( Exception e ){
            res.msg     = e.getMessage()
            res.type    = 'error'
            res.status  = 1
        }
    render res as JSON
    }


    /**
     * gerar gride dashboard das fichas em validação ou prontas para serem validadas
     * @return
     */
    def getDashboard(){
        Map res  = [status:0,msg:'',type:'success','html':'']
        try {
            if( !params.sqCicloAvaliacao ){
                throw new Exception('Ciclo de avaliação não informado')
            }
            String cmdSql = """with passo1 as (SELECT un.sg_unidade_org
                                 , grupo.ds_dados_apoio AS no_grupo
                                 , coalesce(categoria_anterior.cd_categoria_ant,'NE') as cd_categoria_ant
                                 , coalesce(categoria_avaliada.cd_categoria_ava,'NE') as cd_categoria_ava
                                 --, coalesce(cat_avaliada.cd_dados_apoio,'NE') as cd_categoria_ava
                                 , distribuida.st_distribuida
                            , FICHA.nm_cientifico
                            FROM salve.ficha
                                     INNER JOIN salve.dados_apoio situacao ON situacao.sq_dados_apoio = ficha.sq_situacao_ficha
                                     INNER JOIN salve.vw_unidade_org un ON un.sq_pessoa = ficha.sq_unidade_org
                                     INNER JOIN salve.dados_apoio AS grupo ON grupo.sq_dados_apoio = ficha.sq_grupo
                                     LEFT OUTER JOIN salve.dados_apoio as cat_avaliada on cat_avaliada.sq_dados_apoio = ficha.sq_categoria_iucn

                                -- categoria anterior
                                     LEFT JOIN LATERAL (
                                SELECT cat.cd_dados_apoio AS cd_categoria_ant
                                FROM salve.taxon_historico_avaliacao hist
                                         INNER JOIN salve.dados_apoio AS cat ON cat.sq_dados_apoio = hist.sq_categoria_iucn
                                         INNER JOIN salve.dados_apoio AS tipo ON tipo.sq_dados_apoio = hist.sq_tipo_avaliacao
                                WHERE hist.sq_taxon = ficha.sq_taxon
                                  AND tipo.cd_sistema = 'NACIONAL_BRASIL'
                                ORDER BY hist.nu_ano_avaliacao DESC, hist.sq_taxon_historico_avaliacao DESC
                                LIMIT 1
                                ) AS categoria_anterior ON TRUE


                                 -- categoria avaliada
                                LEFT JOIN LATERAL(
                                    select cat.cd_dados_apoio as cd_categoria_ava from salve.oficina_ficha of
                                    inner join salve.oficina on oficina.sq_oficina = of.sq_oficina
                                    inner join salve.dados_apoio cat on cat.sq_dados_apoio = of.sq_categoria_iucn
                                    inner join salve.dados_apoio as tipo on tipo.sq_dados_apoio = oficina.sq_tipo_oficina
                                    where of.sq_categoria_iucn is not NULL
                                    and of.sq_ficha = ficha.sq_ficha
                                    and tipo.cd_sistema = 'OFICINA_AVALIACAO'
                                    order by oficina.dt_fim DESC, oficina.sq_oficina DESC
                                    limit 1
                                ) as categoria_avaliada on true

                                -- validadores
                                     LEFT JOIN LATERAL (
                                SELECT COUNT(vf.*) > 1 AS st_distribuida
                                FROM salve.validador_ficha vf
                                         INNER JOIN salve.oficina_ficha of ON of.sq_oficina_ficha = vf.sq_oficina_ficha
                                         INNER JOIN salve.ciclo_avaliacao_validador cav ON cav.sq_ciclo_avaliacao_validador = vf.sq_ciclo_avaliacao_validador
                                         INNER JOIN salve.dados_apoio AS sit ON sit.sq_dados_apoio = cav.sq_situacao_convite
                                WHERE of.sq_ficha = ficha.sq_ficha
                                  AND sit.cd_sistema <> 'CONVITE_RECUSADO'
                                ) AS distribuida ON TRUE
                            WHERE ficha.sq_ciclo_avaliacao = :sqCicloAvaliacao
                              AND situacao.cd_sistema IN ('AVALIADA_REVISADA', 'EM_VALIDACAO')
             )
               , passo2 as (SELECT passo1.sg_unidade_org,
                                  passo1.no_grupo,
                                  passo1.cd_categoria_ant,
                                  passo1.cd_categoria_ava,
                                  COUNT(*)                                               AS nu_total,
                                  SUM(CASE WHEN passo1.st_distribuida THEN 1 ELSE 0 END) AS nu_distribuidas
                           FROM passo1
                           GROUP BY passo1.sg_unidade_org, passo1.no_grupo, passo1.cd_categoria_ant, passo1.cd_categoria_ava
            )
            select passo2.*, (passo2.nu_distribuidas / passo2.nu_total * 100)::integer as nu_percentual
            from passo2
            order by passo2.sg_unidade_org, passo2.no_grupo, passo2.cd_categoria_ant
            """
            /** /
            println  ' '
            println cmdSql
            /**/
            List rows = sqlService.execSql(cmdSql,[sqCicloAvaliacao:params.sqCicloAvaliacao.toLong()])
            res.html = g.render(template:'divGridDashboardValidacao', model:[rows:rows])
        } catch( Exception e ){
            res.msg     = e.getMessage()
            res.type    = 'error'
            res.status  = 1
        }
        render res as JSON
    }

    /**
     * gerar gride dashboard das fichas em validação de uma determinada oficina
     * @return
     */
    def getDashboardOficina(){
        Map res  = [status:0,msg:'',type:'success','html':'']
        try {
            if( !params.sqCicloAvaliacao ){
                throw new Exception('Ciclo de avaliação não informado')
            }
            String cmdSql = """with passo1 as (SELECT un.sg_unidade_org
                                 , grupo.ds_dados_apoio AS no_grupo
                                 , coalesce(categoria_anterior.cd_categoria_ant,'NE') as cd_categoria_ant
                                 --, coalesce(categoria_avaliada.cd_categoria_ava,'NE') as cd_categoria_ava
                                 , coalesce(cat_avaliada.cd_dados_apoio,'NE') as cd_categoria_ava
                                 , distribuida.st_distribuida
                                 , FICHA.nm_cientifico
                            FROM salve.oficina_ficha of
                            inner join salve.ficha on ficha.sq_ficha = of.sq_ficha
                            INNER JOIN salve.vw_unidade_org un ON un.sq_pessoa = ficha.sq_unidade_org
                            INNER JOIN salve.dados_apoio AS grupo ON grupo.sq_dados_apoio = ficha.sq_grupo
                            LEFT OUTER JOIN salve.dados_apoio as cat_avaliada on cat_avaliada.sq_dados_apoio = ficha.sq_categoria_iucn


                            -- categoria anterior
                            LEFT JOIN LATERAL (
                                SELECT cat.cd_dados_apoio AS cd_categoria_ant
                                FROM salve.taxon_historico_avaliacao hist
                                         INNER JOIN salve.dados_apoio AS cat ON cat.sq_dados_apoio = hist.sq_categoria_iucn
                                         INNER JOIN salve.dados_apoio AS tipo ON tipo.sq_dados_apoio = hist.sq_tipo_avaliacao
                                WHERE hist.sq_taxon = ficha.sq_taxon
                                  AND tipo.cd_sistema = 'NACIONAL_BRASIL'
                                ORDER BY hist.nu_ano_avaliacao DESC, hist.sq_taxon_historico_avaliacao DESC
                                LIMIT 1
                            ) AS categoria_anterior ON TRUE

                            /*
                            -- categoria avaliada
                            LEFT JOIN LATERAL(
                                select cat.cd_dados_apoio as cd_categoria_ava from salve.oficina_ficha of
                                                                                       inner join salve.oficina on oficina.sq_oficina = of.sq_oficina
                                                                                       inner join salve.dados_apoio cat on cat.sq_dados_apoio = of.sq_categoria_iucn
                                                                                       inner join salve.dados_apoio as tipo on tipo.sq_dados_apoio = oficina.sq_tipo_oficina
                                where of.sq_categoria_iucn is not NULL
                                  and of.sq_ficha = ficha.sq_ficha
                                  and tipo.cd_sistema = 'OFICINA_AVALIACAO'
                                order by oficina.dt_fim DESC, oficina.sq_oficina DESC
                                limit 1
                            ) as categoria_avaliada on true
                            */
                            -- validadores
                            LEFT JOIN LATERAL (
                                SELECT COUNT(vf.*) > 1 AS st_distribuida
                                FROM salve.validador_ficha vf
                                         INNER JOIN salve.oficina_ficha of ON of.sq_oficina_ficha = vf.sq_oficina_ficha
                                         INNER JOIN salve.ciclo_avaliacao_validador cav ON cav.sq_ciclo_avaliacao_validador = vf.sq_ciclo_avaliacao_validador
                                         INNER JOIN salve.dados_apoio AS sit ON sit.sq_dados_apoio = cav.sq_situacao_convite
                                WHERE of.sq_ficha = ficha.sq_ficha
                                  AND sit.cd_sistema <> 'CONVITE_RECUSADO'
                            ) AS distribuida ON TRUE
                            WHERE of.sq_oficina = :sqOficina
            )
               , passo2 as (SELECT passo1.sg_unidade_org,
                                   passo1.no_grupo,
                                   passo1.cd_categoria_ant,
                                   passo1.cd_categoria_ava,
                                   COUNT(*)                                               AS nu_total,
                                   SUM(CASE WHEN passo1.st_distribuida THEN 1 ELSE 0 END) AS nu_distribuidas
                            FROM passo1
                            GROUP BY passo1.sg_unidade_org, passo1.no_grupo, passo1.cd_categoria_ant, passo1.cd_categoria_ava
            )
            select passo2.*, (passo2.nu_distribuidas / passo2.nu_total * 100)::integer as nu_percentual
            from passo2
            order by passo2.sg_unidade_org, passo2.no_grupo, passo2.cd_categoria_ant
            """
            List rows = sqlService.execSql(cmdSql,[sqOficina:params.sqOficina.toLong()])
            res.html = g.render(template:'divGridDashboardValidacaoOficina', model:[rows:rows])
        } catch( Exception e ){
            res.msg     = e.getMessage()
            res.type    = 'error'
            res.status  = 1
        }
        render res as JSON
    }

    def getNomesValidadoresOficina(){
        Map res = [status:0, msg:'', type:'success', nomes:[]]
        List nomesValidadores = [] // para facilitar a filtragem do gride pelo validador
        try {
            if( !params.sqOficina ){
                throw new Exception('id da oficina não informado')
            }
            // CRIAR A LISTA COM OS NOMES DOS VALIDADORES DA OFICINA
            sqlService.execSql("""SELECT DISTINCT pf.no_pessoa
                    FROM salve.validador_ficha v
                             INNER JOIN salve.oficina_ficha ofi ON ofi.sq_oficina_ficha = v.sq_oficina_ficha
                             INNER JOIN salve.ciclo_avaliacao_validador cav ON cav.sq_ciclo_avaliacao_validador = v.sq_ciclo_avaliacao_validador
                             INNER JOIN salve.vw_pessoa_fisica pf ON pf.sq_pessoa = cav.sq_validador
                             INNER JOIN salve.dados_apoio AS situacao_convite ON situacao_convite.sq_dados_apoio = cav.sq_situacao_convite
                    WHERE ofi.sq_oficina = :sqOficina
                      AND situacao_convite.cd_sistema = 'CONVITE_ACEITO'
                    ORDER BY PF.no_pessoa;
                    """,[sqOficina:params.sqOficina.toLong() ] ).each{row->
                res.nomes.push(Util.capitalize(row.no_pessoa))
            }
        } catch( Exception e ){
            res.status=1
            res.msg=e.getMessage()
        }
        render res as JSON
    }

}
