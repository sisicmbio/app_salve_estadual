package br.gov.icmbio

import grails.converters.JSON

class EfeitoAmeacaController {

    def sqlService

    def index() { }


    def getTreeViewAmeaca()
    {
        List data =[]
        // selecionar as ameaças já cadatradas para marcar os checks
        Lista  listExistentes = new Lista( [] )
        List listAmeacas = DadosApoio.createCriteria().list()
                {
                    eq('pai', DadosApoio.findByCodigo('TB_CRITERIO_AMEACA_IUCN') )
                    order("ordem")
                }
        listAmeacas.each {
            data.push( getTreeRecursiveAmeaca( it , listExistentes ) )
        }
        // adicionar EFEITOS
        //def listEfeitos = dadosApoioService.getTable('TB_EFEITO_PRIM').sort{it.ordem}
        render data as JSON
    }

    def getTreeRecursiveAmeaca( DadosApoio dadosApoio, Lista listExistentes )
    {
        FichaAmeaca fichaAmeaca = listExistentes.list.find {
            return it.criterioAmeacaIucn.id == dadosApoio.id
        }

        //boolean existe = listExistentes.list.find{ FichaAmeaca fichaAmeaca -> fichaAmeaca.criterioAmeacaIucn.id == dadosApoio.id };
        boolean existe = (fichaAmeaca && fichaAmeaca.criterioAmeacaIucn.id == dadosApoio.id)
        Map node = [id:dadosApoio.id
                    ,title:dadosApoio.descricaoCompleta
                    ,icon:false
                    ,folder:false
                    ,selected:existe
                    ]
        if( dadosApoio.itens.size() > 0)
        {
            node.children = []
            dadosApoio.itens.sort{it.ordem}.each {
                node.children.push( getTreeRecursiveAmeaca(it,listExistentes) )
            }
        }
        return node
    }

    def getEfeitosAmeaca()
    {
        // selecionar os efeitos de acordo com a ameaça
        Map efeitos = [ids:[]]
        if( params.data ) {
            def jsonData = JSON.parse( params.data )
            EfeitoAmeaca.findAllByAmeaca(DadosApoio.get(jsonData.sqAmeaca.toLong())).each {
                efeitos.ids.push(it.efeito.id)
            }
        }
        render efeitos as JSON
    }

    /****************************************************************************************************/
    /****************************************************************************************************/
    /****************************************************************************************************/
    /****************************************************************************************************/
    /****************************************************************************************************/
    /****************************************************************************************************/
    /****************************************************************************************************/
    /****************************************************************************************************/
    def getTreeViewEfeito() {
        List data = []
        Lista listExistentes = new Lista( [] )
        List listEfeitos = DadosApoio.createCriteria().list()
        {
            eq('pai', DadosApoio.findByCodigo('TB_EFEITO_PRIM'))
            order("ordem")
        }
        listEfeitos.each {
                data.push( getTreeRecursiveEfeitoPrim(it,listExistentes ) )
        }
        render data as JSON
    }

    def getTreeRecursiveEfeitoPrim( DadosApoio dadosApoio, Lista listExistentes )
    {
        FichaAmeacaEfeito fichaAmeacaEfeito = listExistentes.list.find {
            return it.efeito.id == dadosApoio.id
        }

        boolean existe = (fichaAmeacaEfeito && fichaAmeacaEfeito.efeito.id == dadosApoio.id)
        Map node = [id:dadosApoio.id
                    ,title:dadosApoio.descricaoCompleta
                    ,icon:false
                    ,folder:false
                    ,selected:existe
                    ]
        if( dadosApoio.itens.size() > 0)
        {
            node.children = []
            dadosApoio.itens.sort{it.ordem}.each {
                node.children.push( getTreeRecursiveEfeitoPrim(it,listExistentes) )
            }
        }
        return node
    }

    def gravar()
    {
        Map res = [type:'error',msg:'Parâmetro data não informado!',status:1]
        if( params.data )
        {
            def jsonData = JSON.parse( params.data )

            // verificar quantos não podem ser excluídos porque estão sendo utilizado na tabela ficha_efeito_ameaca
            String qry = """select efeito.sq_dados_apoio as sq_efeito, efeito.cd_dados_apoio||' - '||efeito.ds_dados_apoio as ds_efeito 
                from salve.efeito_ameaca
                left outer join salve.dados_apoio efeito on efeito.sq_dados_apoio = efeito_ameaca.sq_efeito
                where sq_ameaca = :sqAmeaca """
            if( jsonData.idEfeitos ) {
                qry +="""\nand sq_efeito not in (${jsonData.idEfeitos.join(',')})"""
            }
            qry += """\nand exists ( select null from salve.ficha_ameaca_efeito 
                inner join salve.ficha_ameaca on ficha_ameaca.sq_ficha_ameaca = ficha_ameaca_efeito.sq_ficha_ameaca
                inner join salve.vw_efeito_prim on vw_efeito_prim.sq_efeito = ficha_ameaca_efeito.sq_efeito
                where ficha_ameaca.sq_criterio_ameaca_iucn = :sqAmeaca and vw_efeito_prim.ds_trilha like efeito.cd_dados_apoio || '%' LIMIT 1 
                ) order by efeito.de_dados_apoio_ordem"""
            List efeitosEmUso = sqlService.execSql( qry, [ sqAmeaca : jsonData.sqAmeaca.toLong() ] )
            jsonData.idEfeitos += efeitosEmUso.sq_efeito

            // REALIZAR A EXCLUSÃO DOS EFEITOS DESELECIONADOS
            qry = """delete from salve.efeito_ameaca where sq_efeito_ameaca in ( select sq_efeito_ameaca from salve.efeito_ameaca left outer join salve.dados_apoio efeito on efeito.sq_dados_apoio = efeito_ameaca.sq_efeito
                    where sq_ameaca = :sqAmeaca """
            if( jsonData.idEfeitos ) {
                qry +="""\nand sq_efeito not in ( ${jsonData.idEfeitos.join(',') } ) )"""
            }
            sqlService.execSql(qry, [ sqAmeaca : jsonData.sqAmeaca.toLong() ] )
            if( jsonData.idEfeitos ) {
                // ADICIONAR OS NOVOS EFEITOS SELECIONADOS
                qry = """insert into salve.efeito_ameaca ( sq_ameaca, sq_efeito )
                    select :sqAmeaca, sq_dados_apoio from salve.dados_apoio where sq_dados_apoio in (${jsonData.idEfeitos.join(',')})
                    and sq_dados_apoio not in ( select x.sq_efeito from salve.efeito_ameaca x where x.sq_ameaca = :sqAmeaca  )
                    """
            }
            sqlService.execSql(qry, [ sqAmeaca : jsonData.sqAmeaca.toLong() ] )
            res.status=0
            res.msg='Dados gravados com SUCESSO!'+(efeitosEmUso?'<br><p class="red">Efeito(s) em USO que NÃO foram excluído(s):</p>' + efeitosEmUso.ds_efeito.join('<br>') : '' )
            res.type='success'
        }

        render res as JSON
    }


}
