/**
 * Este controlador é resposável por receber as ações referentes ao cadastro aba Ameaças da ficha de espécies
 * Regras:
 *
 *
 *
 *
 *
 */

package br.gov.icmbio
import grails.converters.JSON

class FichaAmeacaController extends FichaBaseController {

    def dadosApoioService
    def fichaService
    def sqlService

	def index() {
		if( !params.sqFicha )
		{
			render 'ID da ficha não informado.'
			return;
		}
		Ficha ficha = Ficha.get(params.sqFicha)
        List listAmeacas = DadosApoio.createCriteria().list()
        {
            eq('pai', DadosApoio.findByCodigo('TB_CRITERIO_AMEACA_IUCN') )
            order("ordem");
        }

		render(template:"/ficha/ameaca/index",model:['ficha':ficha,'listAmeacas':listAmeacas])
	}

    def getTreeRecursiveEfeitoPrim( DadosApoio dadosApoio, Lista listExistentes )
    {
        FichaAmeacaEfeito fichaAmeacaEfeito = listExistentes.list.find {
            return it.efeito.id == dadosApoio.id
        }

        boolean existe = (fichaAmeacaEfeito && fichaAmeacaEfeito.efeito.id == dadosApoio.id)
        Map node = [id:dadosApoio.id
                    ,title:dadosApoio.descricaoCompleta
                    ,icon:false
                    ,folder:false
                    ,selected:existe
                    ,peso : ( fichaAmeacaEfeito && fichaAmeacaEfeito.nuPeso ? fichaAmeacaEfeito.nuPeso : 1 ) ]
        if( dadosApoio.itens.size() > 0)
        {
            node.children = []
            dadosApoio.itens.sort{it.ordem}.each {
                node.children.push( getTreeRecursiveEfeitoPrim(it,listExistentes) )
            }
        }
        return node
    }

    def getTreeRecursive( DadosApoio dadosApoio, Lista listExistentes )
    {
        FichaAmeaca fichaAmeaca = listExistentes.list.find {
            return it.criterioAmeacaIucn.id == dadosApoio.id
        }

        //boolean existe = listExistentes.list.find{ FichaAmeaca fichaAmeaca -> fichaAmeaca.criterioAmeacaIucn.id == dadosApoio.id };
        boolean existe = (fichaAmeaca && fichaAmeaca.criterioAmeacaIucn.id == dadosApoio.id)
        Map node = [id:dadosApoio.id
                    ,title:dadosApoio.descricaoCompleta
                    ,icon:false
                    ,folder:false
                    ,selected:existe
                    ,referenciaTemporal: fichaAmeaca ? (fichaAmeaca.referenciaTemporal ? fichaAmeaca.referenciaTemporal.id : 0) : 0
                    ,peso : ( fichaAmeaca && fichaAmeaca.nuPeso ? fichaAmeaca.nuPeso : 0 ) ]
        if( dadosApoio.itens.size() > 0)
        {
            node.children = []
            dadosApoio.itens.sort{it.ordem}.each {
                node.children.push( getTreeRecursive(it,listExistentes) )
            }
        }
        return node;
    }


    def getTreeAmeacas()
    {
        List data =[]

        if( !params.sqFicha )
        {
            render data as JSON
            return
        }

        // selecionar as ameaças já cadatradas para marcar os checks
        Lista  listExistentes = new Lista( FichaAmeaca.findAllByFicha( Ficha.get(params.sqFicha) ) )
        List listAmeacas = DadosApoio.createCriteria().list()
                {
                    eq('pai', DadosApoio.findByCodigo('TB_CRITERIO_AMEACA_IUCN') )
                    order("ordem")
                }
        listAmeacas.each {
            data.push( getTreeRecursive( it , listExistentes ) )
        }
        // adicionar EFEITOS
        //def listEfeitos = dadosApoioService.getTable('TB_EFEITO_PRIM').sort{it.ordem}
        render data as JSON
    }

    /**
     * método para listar os efeitos do prim válidos para uma determinada ameaça
     * @return
     */
    def getTreeEfeitoPrim()
    {
        List data =[]

        if( !params.sqFichaAmeaca )
        {
            render data as JSON
            return
        }
        // selecionar os efeitos de acordo com a ameaça
        List efeitoAmeaca = EfeitoAmeaca.findAllByAmeaca( DadosApoio.get( params.sqAmeaca.toLong() ) )

        if( efeitoAmeaca ) {
            // selecionar as ameaças já cadatradas para marcar os checks
            Lista listExistentes = new Lista(FichaAmeacaEfeito.findAllByFichaAmeaca(FichaAmeaca.get(params.sqFichaAmeaca)))
            List listEfeitos = DadosApoio.createCriteria().list()
                    {
                        eq('pai', DadosApoio.findByCodigo('TB_EFEITO_PRIM'))
                        'in'('id', efeitoAmeaca.efeito.id)
                        order("ordem")
                    }
            listEfeitos.each {
                data.push(getTreeRecursiveEfeitoPrim(it, listExistentes))
            }
        }
        render data as JSON
    }

    def saveTreeAmeaca() {
        Map data = [limparDescricao: false]

        if (!params.sqFicha) {
            return this._returnAjax(1, 'ID da ficha não informado.', "error", data)
        }

        Ficha ficha = Ficha.get(params.sqFicha.toLong())
        if (!ficha) {
            return this._returnAjax(1, 'ID da ficha inválido.', "error", data)

        }
        Boolean updated = false

        List idsSelecionados = []
        List idsValidos = []
        if (params.ids) {
            idsSelecionados = JSON.parse(params.ids)
        }
        // criar a lista das ameaças ordenada pelo codigo na ordem descendente para não incluir um codigo pai se o filho ja existir
        String ultimoCodigoProcessado = ''
        if (idsSelecionados) {
            VwAmeaca.createCriteria().list {
                'in'('id', idsSelecionados.id.collect { it.toLong() })
                order('ordem', 'desc')
            }.each {
                if (!ultimoCodigoProcessado || ultimoCodigoProcessado.indexOf(it.codigo + '.') != 0) {
                    ultimoCodigoProcessado = it.codigo
                    Integer index = idsSelecionados.findIndexOf { item -> item.id == it.id }
                    idsValidos.push(idsSelecionados[index])
                }
            }
        }
        // 1) excluir as ameaças existentes  e que não foram mantidas
        // 2) atualizar/adicionar as novas ameaças
        if ( idsValidos ) {
            FichaAmeaca.executeUpdate("delete FichaAmeaca fa where fa.ficha.id = :sqFicha and fa.criterioAmeacaIucn.id NOT in ( ${idsValidos.id.join(',')} )", [sqFicha: ficha.id])
            idsValidos.each {
                DadosApoio ameaca = DadosApoio.get(it.id.toLong())
                DadosApoio refTemp = null
                if (ameaca) {
                    Integer nuPeso = it.peso.toInteger()
                    if( it.referenciaTemporal.toLong() > 0 ) {
                        refTemp =  DadosApoio.get( it.referenciaTemporal.toLong() )
                    }
                    FichaAmeaca fa = FichaAmeaca.findByFichaAndCriterioAmeacaIucn(ficha, ameaca)
                    if (fa) {
                        fa.nuPeso = nuPeso
                        fa.referenciaTemporal = refTemp
                        fa.save(flush: true)
                        updated = true
                    } else {
                        // verificar se ja tem codigo filho cadastrado, se tiver não permitir cadastrar o pai
                        List filha = FichaAmeaca.createCriteria().list {
                            createAlias('criterioAmeacaIucn', 'x')
                            eq('ficha', ficha)
                            like('x.codigo', ameaca.codigo + '.%')
                            maxResults(1)
                        }
                        if ( ! filha ) {
                            fa = new FichaAmeaca(ficha: ficha, criterioAmeacaIucn: ameaca, nuPeso: nuPeso, referenciaTemporal: refTemp)
                            fa.save(flush: true)
                            updated = true
                        }
                    }
                }
            }
            if (Util.stripTags(Util.html2str(ficha.dsAmeaca)) =~ /(?i)Não foram encontradas informações para o táxon|Não foram identificadas ameaças que coloquem o táxon em risco de extinção em um futuro próximo/) {
                data.limparDescricao = true
                ficha.dsAmeaca = null
                ficha.save()
                updated = true
            }
        } else {
            // nenhuma ameaça selecionada
            FichaAmeaca.executeUpdate("delete FichaAmeaca fa where fa.ficha.id = :sqFicha", [sqFicha: ficha.id])
            updated = true
        }
        if( updated ) {
            ficha.dtAlteracao = new Date()
            ficha.save()
        }
        return this._returnAjax(0, getMsg(1), 'success', data)
    }

    def saveTreeEfeitoPrim() {
        Map data = [limparDescricao: false]

        if (!params.sqFichaAmeaca) {
            return this._returnAjax(1, 'ID da Ameaça não informado.', "error", data)
        }

        FichaAmeaca fichaAmeaca = FichaAmeaca.get( params.sqFichaAmeaca.toLong() )
        if ( ! fichaAmeaca) {
            return this._returnAjax(1, 'ID da Ameaça inválido.', "error", data)
        }

        List idsValidos = []
        if (params.ids)
        {
            idsValidos = JSON.parse( params.ids )
        }
        if( idsValidos.id ) {
            FichaAmeacaEfeito.executeUpdate("delete FichaAmeacaEfeito fa " +
                    "where fa.fichaAmeaca.id = :sqFichaAmeaca and fa.efeito.id NOT in ( ${idsValidos.id.join(',')} )", [sqFichaAmeaca: fichaAmeaca.id])
            idsValidos.each {
                DadosApoio efeito  = DadosApoio.get( it.id.toLong() )
                if( efeito ) {
                    Integer nuPeso = it.peso.toInteger()
                    FichaAmeacaEfeito fa = FichaAmeacaEfeito.findByFichaAmeacaAndEfeito( fichaAmeaca, efeito )
                    if (fa) {
                        fa.nuPeso = nuPeso
                    }else {
                        fa = new FichaAmeacaEfeito(fichaAmeaca: fichaAmeaca, efeito: efeito, nuPeso:nuPeso )
                    }
                    fa.save( flush: true)
                }
            }
        }
        return this._returnAjax(0, 'Dados gravados com SUCESSO!.', "success")
    }

    def saveFrmAmeaca()
    {
		if( !params.sqFicha )
		{
			return this._returnAjax(1, 'ID da ficha não informado.', "error")
		}
		if( !params.sqCriterioAmeacaIucn)
		{
			return this._returnAjax(1, 'Selecione um critério de ameaça!', "error")
		}

		FichaAmeaca reg

		if( params.sqFichaAmeaca )
		{
			reg = FichaAmeaca.get( params.sqFichaAmeaca)
		}
		else
		{
			reg = new FichaAmeaca()
			reg.ficha = Ficha.get( params.sqFicha )
		}
		reg.properties = params
		reg.criterioAmeacaIucn = DadosApoio.get(params.sqCriterioAmeacaIucn)
        try {
            reg.save(flush:true)
            this._returnAjax(0, getMsg(1), "success")
        } catch(Exception e) {
            println e.getMessage()
            this._returnAjax(1, getMsg(2), "error")
        }
    }

    def getGridAmeaca()
    {
		if( !params.sqFicha)
		{
			render ''
			return
		}
		List listFichaAmeaca = FichaAmeaca.findAllByFicha( Ficha.get( params.sqFicha ), [sort: "vwAmeaca.ordem"] )
		render( template:'/ficha/ameaca/divGridAmeaca',model:['listFichaAmeaca':listFichaAmeaca])
    }

    def getGridAnexo()
    {
        if( !params.sqFicha )
        {
            render  'ID da ficha não informado.';
            return;
        }
        DadosApoio contexto = dadosApoioService.getByCodigo('TB_CONTEXTO_ANEXO','MAPA_AMEACA');
        List dados = FichaAnexo.findAllByFichaAndContexto(Ficha.get( params.sqFicha),contexto );
        render( template:'/ficha/ameaca/divGridAnexos',model:[listFichaAnexo:dados]);
    }

    def editAmeaca()
    {
    	FichaAmeaca reg = FichaAmeaca.get(params.id);
		if( reg?.id )
		{
			render reg.asJson();
            return;
		}
		render [:] as JSON;
    }

    def deleteAmeaca()
    {
    	FichaAmeaca reg = FichaAmeaca.get(params.id);
		if( reg )
		{
			reg.delete(flush:true);
			return this._returnAjax(0, getMsg(4), "success")
		}
		else
		{
			return this._returnAjax(1, getMsg(51), "error")
		}
		render '';
    }

    def saveFrmAmeacaFicha()
    {
		if( !params.sqFicha )
		{
			return this._returnAjax(1, 'ID da ficha não informado.', "error")
		}
		Ficha ficha = Ficha.get( params.sqFicha )
		ficha.dsAmeaca = params.dsAmeaca

        try {
            ficha.save(flush:true)
            fichaService.updatePercentual( ficha )
            //fichaService.atualizarPercentualPreenchimento( ficha )
            this._returnAjax(0, getMsg(1), "success")
        } catch(Exception e) {
            println e.getMessage()
            this._returnAjax(1, getMsg(2), "error")
        }
    }


    def getGraficoEfeitoPrim()
    {
        Map lista  = [:]
        Map arvore = [name:'root','collapsed': false, children:[]]
        Map pai    = [:]

        FichaAmeaca fichaAmeaca = FichaAmeaca.get( params.sqFichaAmeaca.toLong() )
        if( !fichaAmeaca )
        {
            render arvore as JSON
        }

        Map pars  = ['sqFichaAmeaca':fichaAmeaca.id ]
        // ler os efeitos que estão sendo utilizados
        String qry = """select ds_trilha from salve.vw_efeito_prim where sq_efeito in( select sq_efeito from salve.ficha_ameaca_efeito where sq_ficha_ameaca = :sqFichaAmeaca )"""
        List efeitosUtilizados = []
        sqlService.execSql( qry, pars ).each {
            // extrair somente os codigo do campo trilha
            it.ds_trilha.split(/\|/).each { item ->
                efeitosUtilizados.push( (item.split(/\-/)[0]).trim() );
            }
        }
        pars  = ['sqAmeaca':fichaAmeaca.vwAmeaca.id ]
        // criar a arvore de efeitos da ameaça
        qry="""select c.codigo as cd_ameaca, d.cd_dados_apoio as cd_efeito
                    , c.descricao as ds_ameaca
                    , d.ds_dados_apoio as ds_efeito
                    , b.nu_nivel
                    from salve.efeito_ameaca a
                    inner join salve.vw_efeito_prim b on b.sq_efeito = a.sq_efeito
                    inner join salve.vw_ameacas c on c.id = a.sq_ameaca
                    inner join salve.dados_apoio d on d.sq_dados_apoio = b.sq_efeito
                    where a.sq_ameaca = :sqAmeaca
                    order by c.ordem,b.de_ordem"""

        sqlService.execSql(qry, pars).each {
            String cdAmeaca = it.cd_ameaca
            String cdEfeito = it.cd_efeito
            String cdAmeacaEfeito = cdAmeaca+'.'+cdEfeito.replaceAll(/E\./,'E')
            String dsAmeaca = it.ds_ameaca
            String dsEfeito = it.ds_efeito
            String keyAmeaca = 'a' + cdAmeaca
            String keyEfeito = 'a' + cdAmeacaEfeito
            if (it.nu_nivel.toInteger() == 1i) {
                if (!lista[keyAmeaca]) {
                    lista[keyAmeaca] = [name: dsAmeaca,title: cdAmeaca, emUso:true, children: []]
                    //arvore.children.push(lista[keyAmeaca])
                    arvore = lista[keyAmeaca]

                }
                pai = lista[keyAmeaca]
            }


            if (lista[keyEfeito]) {
                pai = lista[keyEfeito]
            } else {
                boolean  emUso = efeitosUtilizados.indexOf( cdEfeito ) > -1
                lista[keyEfeito] = [name: dsEfeito,title: cdEfeito, emUso:emUso, 'collapsed': false, children: []]
                List codigos = cdAmeacaEfeito.split('-')[0].trim().split(/\./)
                codigos.pop()
                String codigoPai = 'a' + codigos.join('.')
                pai = lista[ codigoPai ]
                if (pai) {
                    pai.children.push( lista[keyEfeito] )
                }
                pai = lista[keyEfeito]
            }
        }
        render arvore as JSON
    }
}
