package br.gov.icmbio

import grails.converters.JSON

class UsuarioWebController extends BaseController{

    def requestService

    def index() { }

    /**
     * renderizar o gride de usuários externos
     * @return
     */
    def getGrid() {

        // parametros para controle da paginação
        // controle da paginação
        String teste = '';
        Map pagination = [pageSize: 50, offset: 0, buttons:10, gridId:( params.gridId ?:'')]
        if (!params?.paginationPageNumber) {
            pagination.pageNumber = params.paginationPageNumber = 1
        } else {
            pagination.pageNumber = params.paginationPageNumber.toInteger()
        }
        if (params?.paginationTotalRecords) {
            pagination.totalRecords = params.paginationTotalRecords.toInteger()
        }
        // calcular o offset
        if( pagination.totalRecords ) {
            pagination.totalPages = Math.max(1, Math.ceil(pagination.totalRecords / pagination.pageSize))
            // pagina atual não pode ser maior que o total de paginas
            pagination.pageNumber = Math.min(pagination.pageNumber.toInteger(), pagination.totalPages.toInteger())
            // calcular o offset
            pagination.offset = (pagination.pageSize * (pagination.pageNumber - 1))
            if (pagination.offset >= pagination.totalRecords) {
                pagination.offset = Math.max(0, (pagination.totalRecords - pagination.pageSize) - 1)
            }
            pagination.offset = pagination.offset.toInteger()
        } else {
            pagination.totalPages = 0
        }
        Map listParams = [offset:pagination.offset]
        if( pagination.totalRecords ) {
           listParams.max = pagination.pageSize
        }
        List listUsuarios = WebUsuario.createCriteria().list(listParams) {
            if( params.noUsuario ){
                ilike('noUsuario','%'+params.noUsuario+'%')
            }
            if( params.deEmail ){
                ilike('username','%'+params.deEmail+'%')
            }
            if( params.enabled ){
                eq('enabled',( params.enabled=='S') )
            }
            if( params.accountLocked ){
                eq('accountLocked',( params.accountLocked=='S') )
            }
            if( params.sgInstituicao ){
                instituicao {
                    ilike('sgInstituicao', '%' + params.sgInstituicao + '%')
                }
            }
            if( params.sortColumns) {
                params.sortColumns.split(',').each {
                    List colunaOrdem =  it.split(':');
                    if( colunaOrdem[0]=='noInstituicao') {
                        instituicao {
                            order(colunaOrdem[0], colunaOrdem[1] )
                        }
                    } else {
                        order( colunaOrdem[0], colunaOrdem[1] )
                    }
                }
            }
        }

        // definir os dados para a paginação
        if( ! pagination.totalRecords ) {
            pagination.totalRecords = listUsuarios.size()
            pagination.totalPages = Math.max(1, Math.ceil(pagination.totalRecords / pagination.pageSize))
            pagination.pageNumber = 1
            pagination.rowNum     = 1
            listUsuarios = listUsuarios.collate( pagination.pageSize )[0]
        }
        render template:"divGridUsuarios", model:[listUsuarios:listUsuarios,pagination:pagination]
    }


    def alterarSenha()
    {
        Map res=[status:1,msg:'Ops.',type:'error']

        if( ! params.sqWebUsuario )
        {
            res.msg = 'Id do usuário não informado!'
            render res as JSON
            return
        }
        WebUsuario wu = WebUsuario.get( params.sqWebUsuario.toInteger())
        if( ! wu )
        {
            res.msg = 'Id do usuário iválido!'
            render res as JSON
            return
        }
        String url = grailsApplication.config.url.sistema.replaceAll(/\/salve\//,'/salve-consulta/') + 'app/setPassword'
        url = url.replaceAll(/8080/,'8090');
        String ambiente =  (session.envProduction?'PROD':'DEV')
        Map dataParams=[ sqWebUsuario:wu.id, deSenha : params.deSenha, hash:'3f801277c23f0730884de3ecd8f3de4bfaa253a263ffd439c268eea7f98ffb22'+ambiente]
        res.msg = requestService.doPost(url, dataParams )
        if( !res.msg )
        {
            res.msg = 'Erro ao alterar a senha!'
            println 'url:'+url
            println dataParams
        }
        else
        {
            res.type='success'
            res.status=0
        }
        render res as JSON
    }

    def bloquear()
    {
        Map res=[status:1,msg:'Ops.',type:'error']
        if( ! params.id )
        {
            res.msg = 'Id do usuário não informado!'
            render res as JSON
            return
        }
        WebUsuario wu = WebUsuario.get( params.id.toInteger())
        if( ! wu )
        {
            res.msg = 'Id do usuário iválido!'
            render res as JSON
            return
        }
        wu.accountLocked =true
        wu.save()
        res.type='success'
        res.status=0
        res.msg='Usuário bloqueado com SUCESSO!'
        render res as JSON
    }

    def desbloquear()
    {
        Map res=[status:1,msg:'Ops.',type:'error']
        if( ! params.id )
        {
            res.msg = 'Id do usuário não informado!'
            render res as JSON
            return
        }
        WebUsuario wu = WebUsuario.get( params.id.toInteger())
        if( ! wu )
        {
            res.msg = 'Id do usuário iválido!'
            render res as JSON
            return
        }
        wu.accountLocked = false
        wu.save()
        res.type='success'
        res.status=0
        res.msg='Usuário desbloqueado com SUCESSO!'
        render res as JSON
    }

    def excluir()
    {

        Map res=[status:1,msg:'Ops.',type:'error']
        if( ! params.id )
        {
            res.msg = 'Id do usuário não informado!'
            render res as JSON
            return
        }
        WebUsuario wu = WebUsuario.get( params.id.toInteger())
        if( ! wu )
        {
            res.msg = 'Id do usuário iválido!'
            render res as JSON
            return
        }
        Integer qtd = FichaColaboracao.countByUsuario(wu)
        qtd += FichaOcorrenciaConsulta.countByUsuario(wu)
        if( qtd > 0 )
        {
            res.msg = 'Usuário não pode ser excluído porque possui '+ qtd + (qtd==1?' colaboração!':' colaborações!')
        }

        else {
            wu.delete()
            //println wu.getErrors();
            res.type='success'
            res.status=0
            res.msg='Usuário excluido com SUCESSO!'
        }
        render res as JSON
    }

    def ativarDesativar()
    {
        Map res=[status:1,msg:'Ops.',type:'error']
        if( ! params.id )
        {
            res.msg = 'Id do usuário não informado!'
            render res as JSON
            return
        }
        WebUsuario wu = WebUsuario.get( params.id.toInteger())
        if( ! wu )
        {
            res.msg = 'Id do usuário iválido!'
            render res as JSON
            return
        }
        wu.enabled = ! wu.enabled
        wu.save()
        res.type='success'
        res.status=0
        res.msg='Conta '  + ( wu.enabled?'ATIVADA':'DESATIVADA') +' com SUCESSO!'
        render res as JSON
    }

}
