package br.gov.icmbio

import grails.converters.JSON
import groovy.sql.Sql
import org.codehaus.groovy.grails.web.json.JSONObject

import java.util.regex.Matcher

class TaxonomiaController extends BaseController
{
    def dataSource
    def requestService
    def sqlService
    def emailService

    def index() {
        //sleep(2000)
        render 'Taxononmia controller'
    }

    def formCadastro(){
        List listNivelTaxonomico = NivelTaxonomico.list();
        //List listNivelTaxonomico = NivelTaxonomico.findAllByNuGrauTaxonomicoGreaterThanEquals(grailsApplication.config.grau.especie)
        render(view:'formCadastro', model:[ listNivelTaxonomico:listNivelTaxonomico])
    }

    def save(){

        //return this._returnAjax(1,'testar!','error');
        if( ! params.sqNivelTaxonomico )
        {
            return this._returnAjax(1,'Campo Nível deve ser selecionado!','error');
        }

        if( ! params.noTaxon ) {
            return this._returnAjax(1,'Nome do táxon não informado!','error');
        }

        NivelTaxonomico nivelTaxonomico = NivelTaxonomico.get( params.sqNivelTaxonomico);

        // ajustar os nomes para capitalizado ou minusculos
        params.noTaxon = params.noTaxon.toLowerCase()
        if( ! ( nivelTaxonomico =~ /ESPECIE/ )  ){
            params.noTaxon = params.noTaxon.toString().capitalize()
        }
        if( params.noTaxon.split(' ').size()>1){
            params.noTaxon = params.noTaxon.split(' ').last()
        }

        // verificar se o taxon ja existe
        int qtd
        int id = ( params?.sqTaxon ? params.sqTaxon.toInteger() : 0 )
        Taxon taxonPai
        if( params.sqTaxonPai ){
            taxonPai = Taxon.get(params.sqTaxonPai)
            if( id ) {
                qtd = Taxon.countByPaiAndNivelTaxonomicoAndNoTaxonAndIdNotEqual(taxonPai, nivelTaxonomico, params.noTaxon, id);
            } else {
                qtd = Taxon.countByPaiAndNivelTaxonomicoAndNoTaxon(taxonPai, nivelTaxonomico, params.noTaxon);
            }
        }
        else {
            qtd = Taxon.countByPaiIsNullAndNivelTaxonomicoAndNoTaxonAndIdNotEqual(nivelTaxonomico,params.noTaxon,id);
        }
        if( qtd > 0 ) {
            return this._returnAjax(1,(taxonPai ? taxonPai.noTaxon + ' ' : '' ) + params.noTaxon + ' já existe na tabela taxon!','error');
        }

        /*
        println ' '
        println params.no_taxon
        return this._returnAjax(1,'TESTE JA EXISTE','error');
         */


        Taxon taxon

        //println nivelTaxonomico.deNivelTaxonomico;
        //return this._returnAjax(1,'testar!','success');

        if( params.sqTaxon )
        {
            taxon = Taxon.get( params.sqTaxon )
            taxon.situacaoNome = TaxonSituacao.findByGrupoAndCoSituacao(TaxonGrupo.findByCoGrupo('NOME_TAXON'),'VALIDO');
            taxon.noAutor=null
            taxon.nuAno=null
            if( taxon.noTaxon != params.noTaxon )
            {
                // GRAVAR HISTORICO
            }
        }
        else
        {
            taxon = new Taxon()
            // gravar os valores default
            taxon.situacaoNome = TaxonSituacao.findByGrupoAndCoSituacao(TaxonGrupo.findByCoGrupo('NOME_TAXON'),'VALIDO');
            taxon.stAtivo=true
            taxon.nuAno=null
            taxon.noAutor=null
            taxon.nivelTaxonomico = nivelTaxonomico
            if( params.sqTaxonPai ) {
                taxon.pai = Taxon.get(params.sqTaxonPai)
            }
        }
        taxon.properties = params;
        String ano = ''
        // extrair os últimos 4 digitos da string
        if( params.noAutor ) {
            try{
                Matcher m = (params.noAutor =~ /[0-9]{4}/)
                if( m ) {
                    ano = m[(m.size()-1).toInteger()]
                }
            } catch( Exception e ) {
                ano = params.noAutor.replaceAll(/[^0-9]/,'')
            }
            if( ano.isInteger() ) {
                taxon.nuAno = ano.toInteger()
            }
        }
        if( taxon.nivelTaxonomico.coNivelTaxonomico =~ /ESPECIE/){
            if( !taxon.noAutor ){
                return this._returnAjax(1,'Autor/Ano é obrigatório para o nivel ' + taxon.nivelTaxonomico.deNivelTaxonomico,'error')
            }
        }
        // ajustar nomes compostos, sempre gravar somente o último. Ex: puma concolor, pegar somente concolor
        taxon.noTaxon = taxon.noTaxon.replaceAll(/\s{2,}/, ' '); // remover espaços duplos
        List nomes = taxon.noTaxon.split(' ')
        if( nomes.size() > 1 ){
            taxon.noTaxon = nomes[nomes.size()-1]
        }
        // se o nivel for acima de genero gravar em caixa baixa senão gravar capitalizado
        if( taxon.nivelTaxonomico.nuGrauTaxonomico.toInteger() > 89 ){
            taxon.noTaxon = taxon.noTaxon.toString().toLowerCase()
        } else {
            taxon.noTaxon = taxon.noTaxon.toString().capitalize()
        }

        if( taxon.validate() )
        {
            taxon.save(flush:true)

            if( taxon.nuAno )
            {
                Ficha.findAllByTaxon( taxon ).each {
                    if( it.nmCientifico != taxon.noCientificoCompleto )
                    {
                        it.nmCientifico = taxon.noCientificoCompleto
                        it.save()
                    }
                }
            }

            // atualizar json_trilha da tabela taxon
            String query = "update taxonomia.taxon set json_trilha = taxonomia.criar_trilha( :id ) where sq_taxon = :id"
            Sql sql = new Sql(dataSource)
            try {
                sql.executeUpdate(query,[id:taxon.id])
            } catch (Exception e) {

                println e.getMessage()
            }
            sql.close()
        }
        else
        {
            List errors = []
            taxon.errors.allErrors.each {
                errors.push(it.field+ ' campo Obrigatório!');
            }
            if ( errors )
            {
                return this._returnAjax(1, errors.join('<br>'), "error");
            }
        }

        // VALIDAR TAXON NA BASE DO ICMBIO E SE NÃO EXISTIR ENVIAR EMAIL AUTOMATICO
        _checkTaxonExistsIcmbio(taxon.id)

        return this._returnAjax(0,'Dados gravados com SUCESSO','success');
    }

    def edit(){
        if( !params.sqTaxon )
        {
            return this._returnAjax(0,'Id do taxon não informado','error');
        }
        render Taxon.get( params.sqTaxon ).asJson();
    }

    def delete(){
        if( !params.sqTaxon )
        {
            return this._returnAjax(1,'Id do taxon não informado','error');
        }

        Taxon taxon = Taxon.get( params.sqTaxon )
        if( !taxon ){
            return this._returnAjax(1,'Id não cadastrado','error');
        }
        // se possuir registros filhos não pode ser excluído
        Taxon taxonFilho = Taxon.findByPai(taxon)
        if( taxonFilho ){
            return this._returnAjax(1,'Exclusão NÃO permitida por possuir registro(s) dependente(s)','error');
        }
        // se possuir ficha não permitir a exclusão
        Ficha ficha = Ficha.findByTaxon( taxon )
        if( ficha ){
            return this._returnAjax(1,'Exclusão NÃO permitida por possuir ficha cadastrada com este táxon','error');
        }
        taxon.delete()
        return this._returnAjax(0,'Registro EXCLUÍDO com SUCESSO','success');

    }

    //------------------------ sinonimias ---------------------------------
    def formSinonimia()
    {

        List listNivelTaxonomico = NivelTaxonomico.findAllByNuGrauTaxonomicoGreaterThanEquals(grailsApplication.config.grau.especie)
        render(view:'formSinonimia', model:[ listNivelTaxonomico:listNivelTaxonomico])
    }

    def saveSinonimia()
    {
        if( !params.sqTaxonAceito )
        {
            return this._returnAjax(1,'Informe o taxon aceito','error');
        }
        if( !params.sqTaxonSinonimo )
        {
            return this._returnAjax(1,'Informe o taxon sinônimo','error');
        }
        if( params.sqTaxonAceito == params.sqTaxonSinonimo )
        {
            return this._returnAjax(1,'Taxon aceito e sinônimo devem ser diferentes!','error');
        }
        Taxon aceito    = Taxon.get(params.sqTaxonAceito)
        Taxon sinonimo =  Taxon.get( params.sqTaxonSinonimo)

        // verificar se a sinoníma já existe
        int qtd = TaxonSinonimia.countByTaxonAceitoAndTaxonSinonimo(aceito,sinonimo)
        if( qtd > 0 )
        {
            return this._returnAjax(0,'Sinonímia já registrada','info');
        }
        // fazer troca do sinonimo virando aceito
        TaxonSinonimia.findAllByTaxonSinonimo(aceito).each {
            it.taxonSinonimo = sinonimo
            it.save(flush:true);

        }
        // fazer troca do sinonimo virando aceito
        TaxonSinonimia.findAllByTaxonAceito(sinonimo).each {
            it.taxonAceito = aceito
            it.save(flush:true);

        }
        if( ! TaxonSinonimia.findByTaxonSinonimoAndTaxonAceito(sinonimo,aceito) )
        {

            TaxonSinonimia ts = new TaxonSinonimia();
            ts.taxonAceito = aceito
            ts.taxonSinonimo = sinonimo
            ts.situacao = TaxonSituacao.findByCoSituacao('SINONIMIA');

            if (ts.validate())
            {
                ts.save(flush:true)
            }
        }
        return this._returnAjax(0,'Sinonímia gravada com SUCESSO','success');
    }

    def getGridSinonimias()
    {
        if( !params.sqTaxonAceito )
        {
            render 'Id do taxon aceito não informado!'
            return;
        }
        List listSinonimias = TaxonSinonimia.findAllByTaxonAceito( Taxon.get(params.sqTaxonAceito) )
        render template:'divGridSinonimia', model:[listSinonimias:listSinonimias]
    }
    def deleteSinonimia()
    {
        if( params.id )
        {
            TaxonSinonimia.get( params.id).delete( flush: true);
        }
        render ''
    }

    def consultarTaxonRemoto(){
        //Thread.sleep(500)
        Map res = [status:0,type:'success',msg:'',data:[:]]
        try {
            res.data = _searchTaxonRemoto( params.noTaxon ?: ''
                                         , params.sqNivelTaxonomico ? params.sqNivelTaxonomico.toLong() : null )
        } catch( Exception e ){
            res.status = 1
            res.msg = e.getMessage()
            res.type='error'
            println e.getMessage()
        }
        render res as JSON
    }


    /**
     * Verificar se o taxon editado pelo Estado existe na base corporativa
     * se não existir disparar e-mail com as informaçõe do taxon para a
     * equipe SALVE do ICMBio
     * @param sqTaxon
     */
    private _checkTaxonExistsIcmbio(Long sqTaxon = null ) {
        runAsync {
            String cmdSql = """select json_trilha::text as json_trilha from taxonomia.taxon where sq_taxon = :sqTaxon"""
            List rows = sqlService.execSql(cmdSql, [sqTaxon: sqTaxon])
            JSONObject json = JSON.parse(rows[0].json_trilha)
            List arvore = []
            List niveis = NivelTaxonomico.list().sort { it.nuGrauTaxonomico }

            // variaveis para disparar a pesquisa  no SINTAX
            String sqNivelEditado = ''
            String noTaxonEditado = ''
            niveis.each {
                String nivelLowercase = it.coNivelTaxonomico.toLowerCase();
                if (json[nivelLowercase]) {
                    arvore.push(it.deNivelTaxonomico + ': <b>' + json[nivelLowercase].no_taxon + '</b>')
                    sqNivelEditado = it.id
                    noTaxonEditado = json[nivelLowercase].no_taxon
                }
            }


            // DESTACAR O NIVEL TAXONOMICO NÃO ENCONTRADO EM VERMELHO
            arvore[arvore.size()-1] = arvore[arvore.size()-1].replace(/<b>/,'<b style="color:#ff0000;font-weight:bold;">')

            json = _searchTaxonRemoto(noTaxonEditado?:'', sqNivelEditado.toLong() )

            if( json.data.size() > 0 ){
                return
            }

            Configuracao configuracao = Configuracao.findByEstadoIsNotNull()
            String assunto = 'Táxon não encontrado no SINTAX'
            String mensagemEmail = """<p>Esta é uma mensagem automática enviado pelo SALVE Estadual quando o táxon não é encontrado na base de dados do ICMBio.</p>
            <br>Data do registro: <b>${new Date().format('dd/MM/yyyy')}</b>
            <br>Unidade: <b>${configuracao.noInstituicao}</b>
            <br>Estado: <b>${configuracao.estado.noEstado}</b>
            <p>${arvore.join("<br>")}</p>
            <p>Atenciosamente,
            <br>Equipe SALVE-Estadual</p>
            """
            String emailFrom = grailsApplication.config.app.email ?: 'noreplay@salve-estadual'
            emailService.sendTo('salve@icmbio.gov.br', assunto, mensagemEmail, emailFrom)
        }
    }

    private JSONObject _searchTaxonRemoto(String noTaxon=null, Long sqNivelTaxonomico=null) {
        //Thread.sleep(500)
        String urlApi = grailsApplication.config.url.salve.icmbio.api ? grailsApplication.config.url.salve.icmbio.api.toString() : ''
        String cmdSql = ''
        String cdNivelTaxonomico = ''
        if (!urlApi) {
            throw new Exception('URL da API SALVE ICMBio não configurada.')
        }

        if (!noTaxon) {
            throw new Exception('Nome do táxon não informado')
        }
        if (sqNivelTaxonomico) {
            NivelTaxonomico nivelTaxonomico = NivelTaxonomico.get(sqNivelTaxonomico)
            cdNivelTaxonomico = '/' + nivelTaxonomico.coNivelTaxonomico.toLowerCase()
        } else {
            cdNivelTaxonomico = '/reino'
        }
        return requestService.doGetJson(urlApi + "taxon/search/${params.noTaxon + cdNivelTaxonomico}")
    }


}
