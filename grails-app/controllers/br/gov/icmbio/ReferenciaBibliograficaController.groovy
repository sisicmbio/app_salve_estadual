package br.gov.icmbio
import grails.converters.JSON;

class ReferenciaBibliograficaController extends BaseController{
	def publicacaoService
	def fichaService
	def sqlService

    def index() { render ''}

    private String _findFile(String fileName = ''){
        String dirSalve  = grailsApplication.config.anexos.dir
        dirSalve = dirSalve ?: '/data/salve-estadual/anexos/'

        // procurar no diretorio /data/sintax/anexo
        String dirSintax = dirSalve.replaceAll(/\/salve\//,'/sintax/')
        File file = new File( dirSintax + fileName )
        if( file.exists() ){
            return dirSintax + fileName
        }

        // procurar no diretorio /data/salve-estadual/sintax
        file = new File( dirSalve + fileName )
        if( file.exists() ){
            return dirSalve + fileName
        }
        return ''
    }


	def save() {
		Publicacao reg;
    	if( params.sqPublicacao )
    	{
    		reg = Publicacao.get( params.sqPublicacao);
    	}
    	else
    	{
    		reg = new Publicacao()
    	}

    	params.inListaOficialVigente = (params.inListaOficialVigente=='S')
    	if( params.dtAcessoUrl )
    	{
			params.dtAcessoUrl = new Date().parse('dd/MM/yyyy', params.dtAcessoUrl)
		}
		else
		{
			params.dtAcessoUrl=null
		}
    	if( params.dtPublicacao )
    	{
			params.dtPublicacao = new Date().parse('dd/MM/yyyy', params.dtPublicacao)
		}
		else
		{
			params.dtPublicacao=null
		}
    	params.tipoPublicacao = TipoPublicacao.get( params.sqTipoPublicacao )

        List errors = []

        // se for informado o ANO ele deve estar entre 1500 e 2100 ou ser igual a 9999
        if( params.nuAnoPublicacao  ) {
            try {
                if( params.nuAnoPublicacao.toInteger() != 9999  && ( params.nuAnoPublicacao.toInteger() < 1500 || params.nuAnoPublicacao.toInteger() > 2100 ) ) {
                    return this._returnAjax(1, 'Ano da publicação deve ser maior que 1500 e menor que 2100 ou 9999 quando não possuir ano.', "error")
                }
            } catch( e ) {
                return this._returnAjax(1, 'Ano da publicação deve ser maior que 1500 e menor que 2100 ou 9999 quando não possuir ano.', "error")
            }
        }

        if( ! ( params.tipoPublicacao.coTipoPublicacao =~ /SITE|ATO_OFICIAL/) ) {
            if( ! params.nuAnoPublicacao  ) {
                errors.add('Ano da publicação deve ser informado!')
            }
            if( !params.noAutor ) {
                errors.add( 'Autor deve ser informado!' )
            }
        }

        if( params.tipoPublicacao.coTipoPublicacao == 'ATO_OFICIAL' ) {
            if( !params.dtPublicacao )
            {
                errors.add( 'Data de publicação deve ser informada!' )
            }
        }

        if ( errors ) {
            return this._returnAjax(1, errors.join('<br>'), "error")
        }

        reg.properties = params
		// adicionar a quebra de linha nos nomes dos autores se for informado com ponto e virgula
		if( params.noAutor ) {
			//reg.noAutor = params.noAutor.replaceAll(/\r+/,'').replaceAll(/\n+/,';').trim().replaceAll( /;\s*?/,'\n
			reg.noAutor = params.noAutor.replaceAll(/(\s{1,}?\r{1,})\s?/,';').replaceAll(/(\s{1,}?\n{1,})\s?/,';').trim().replaceAll( /;( {1,})?/,'\n')
        }


 		// se for site ou ato oficial não é necessário autor e ano
        if( ! reg.validate() ) {
    		def locale = Locale.getDefault()
    		reg.errors.allErrors.each {
				errors.add( messageSource.getMessage(it, locale) )
               }
    	}

    	if ( errors ) {
			return this._returnAjax(1, errors.join('<br>'), "error")
		}

		reg.save(flush:true)

		// gravar o anexo
		def f = request.getFile('fldAnexo')
    	if (f && ! f.empty)
    	{
			PublicacaoArquivo pa;
    		// excluir o anexo atual, só pode ter um
			List anexos = PublicacaoArquivo.findAllByPublicacao( reg );
 			anexos.each{
 				// verificar se o anexo não esta referenciado por outra publicaçao
 				Integer qtd = PublicacaoArquivo.countByPublicacaoNotEqualAndDeLocalArquivo(reg,it.deLocalArquivo);
 				if( ! qtd ) {
                    String fileName = _findFile(it.deLocalArquivo);
                    if( fileName ){
                        File file = new File( fileName )
                        file.delete();
                    }
 				}
 				pa = it
			}
			File savePath = new File( grailsApplication.config.anexos.dir );
			if( ! savePath.exists() )
			{
				if( ! savePath.mkdirs() )
				{
					return this._returnAjax(1, 'Não foi possivel criar o diretório ' + savePath.asString(), "error");
				}
			}
			String fileName = f.getOriginalFilename();
			String fileExtension = fileName.substring(fileName.lastIndexOf("."));
            String hashFileName = f.inputStream.text.toString().encodeAsMD5()+fileExtension
    		fileName = savePath.absolutePath+'/'+hashFileName
			if( ! new File(fileName).exists() )
			{
				f.transferTo( new File(fileName) );
			}
			if( ! pa ){
				pa = new PublicacaoArquivo();
				pa.publicacao = reg;
			}
			pa.deTipoConteudo 		= f.getContentType()
			pa.deLocalArquivo 		= hashFileName
			pa.noPublicacaoArquivo	= f.getOriginalFilename()
			pa.save( flush:true );
		}
		// adicionar a ref bib à ficha
		if( params.sqFicha ) {
			FichaRefBib frb = new FichaRefBib()
			frb.ficha =  Ficha.get (params.sqFicha.toInteger() )
			frb.publicacao = reg
			frb.sqRegistro = ( params.sqRegistro ? params.sqRegistro.toInteger() :params.sqFicha.toInteger() )
			frb.noTabela = params.noTabela ?:null
			frb.noColuna = params.noColuna ?:null
			frb.deRotulo = params.deRotulo ?:null
			frb.save( flush:true )
			fichaService.updatePercentual( frb.ficha.id.toLong() )
		}
    	return this._returnAjax(0, getMsg(1), "success",[sqPublicacao:reg.id]);
	}

	def edit()
	{
		if( !params.id)
    	{
			return this._returnAjax(1, "O ID não informado!", "error")
 		}
 	   	Publicacao reg = Publicacao.get(params.id.toInteger());
		if( reg )
		{
			render reg.asJson()
			return
		}
		render [:] as JSON;
    }

    def getGrid() {

 		if( ! params.q )
		{
			render 'sem parâmetro para pesquisar';
			return;
		}
 		if( params.q.size() < 3 )
		{
			render 'Informe pelo menos 3 caracteres para pesquisar!';
			return;
		}
		render( template:'divGridRefBib',model:['lista' : publicacaoService.search( params.q) ])
    }

    def getGridAnexos()
    {
 		if( ! params.sqPublicacao )
		{
			render 'Id não informado!';
			return;
		}
		List lista = PublicacaoArquivo.findAllByPublicacao(Publicacao.get( params.sqPublicacao ) );
		render( template:'divGridRefBibAnexos',model:['lista':lista]);
    }

    def delete()
    {
    	if( !params.id)
    	{
 			return this._returnAjax(1, "ID não informado!", "error")
 		}

 		try{

 			Publicacao p = Publicacao.get(params.id.toLong());
			if( !p )
			{
				return this._returnAjax(1, 'id publicação inválido', "error")
			}
 			//Integer qtd = FichaRefBib.countByPublicacao(p)
			Integer qtd = FichaRefBib.createCriteria().list{
				eq('publicacao.id', p?.id )
			}.unique{ it.publicacao }?.size()
			if( qtd > 0 )
 			{
				//return this._returnAjax(1, "Referência Bibliográfica em uso por "+qtd+' ficha(s).', "error")
				return this._returnAjax(1, 'Referência Bibliográfica em uso por '+qtd+' ficha(s).<div class="mt10 text-center"><button onClick="modal.alterarPublicacaoFicha('+p.id.toString()+')" class="btn btn-xs btn-primary">Ver as fichas</button></div>', "error")
 			}

 	   		// excluir os anexos
 			List anexos = PublicacaoArquivo.findAllByPublicacao( p );
 			anexos.each{

 				// verificar se o anexo não esta referenciado por outra publicaçao
 				qtd = PublicacaoArquivo.countByPublicacaoNotEqualAndDeLocalArquivo(p,it.deLocalArquivo);
 				if( ! qtd ) {
                    String fileName= _findFile(it.deLocalArquivo);
	 				if( fileName ) {
	 					// apagar do disco
	 					new File( fileName ).delete()
	 				}
 				}
 				// apagar da tabela
 				it.delete()
 			}
 	   		p.delete()
			return this._returnAjax(0, "Referência Excluída com SUCESSO", "success")
 	   	}
 	   	catch( Exception e )
 	   	{
			println e.getMessage()
			return this._returnAjax(1, 'Erro ao excluir a Referência Bibliográfica', "errror");
 	   	}
    }

    def deleteAnexo()
    {
    	if( !params.id)
    	{
 			return this._returnAjax(1, "ID não informado!", "error")
 		}
 		try{
 	   		// excluir os anexos
 			PublicacaoArquivo anexo = PublicacaoArquivo.get( params.id );
 			if( ! anexo )
 			{
				return this._returnAjax(1, "Anexo não encontrado", "info")
 			}

 			// verificar se o anexo não esta referenciado por outra publicaçao
 			Integer qtd = PublicacaoArquivo.countByPublicacaoNotEqualAndDeLocalArquivo(anexo.publicacao,anexo.deLocalArquivo);
 			if( ! qtd )
 			{
                String fileName = _findFile( anexo.deLocalArquivo )
	 			if( fileName  )	{
	 				// apagar do disco
	 				new File( fileName ).delete()
	 			}
 			}
 			// apagar da tabela
 			anexo.delete( flush:true);
			return this._returnAjax(0, "Anexo Excluído com SUCESSO", "success")
 	   	}
 	   	catch( Exception e )
 	   	{
			return this._returnAjax(1, 'Erro ao excluir a publicação', "errror");
 	   	}
    }

	def downloadAnexo() {
        String dirAnexos = ''
        if (!params.id) {
            response.sendError(404);
            return;
        }
        PublicacaoArquivo pa = PublicacaoArquivo.get( params.id.toLong() )
        String fileType = 'application/octet-stream'
        String fileName = _findFile('arquivo-nao-encontrado.pdf')
        File file = new File(fileName)
        if ( pa ) {
            String fullName = _findFile(pa.deLocalArquivo)
            if (fullName && new File( fullName).exists() ) {
                file = new File(fullName)
                fileType = (pa.deTipoConteudo ? pa.deTipoConteudo : fileType)
                fileName = pa.noPublicacaoArquivo.replaceAll(' ', '_').replaceAll(',', '')
            }
        }
        render(file: file, fileName: fileName, contentType: fileType)
    }

    def getGridImportacao() {
        List listPlanilhas = Planilha.findAllByNoContexto('REFBIB',[order:'desc',sort:'dtEnvio'] )
        render( template:'divGridImportacaoRefBib',model:['listPlanilhas':listPlanilhas])
    }

    def deletePlanilha()
    {
        if( !params.id )
        {
            render ''
        }
        Planilha.get( params.id.toInteger()).delete( flush:true);
        render ''
    }

	def getGridFichasPublicacao()
	{
		sleep(500)
		//  render 'gride fichas da publicacao'
		// render params
		if( ! params.sqPublicacao )
		{
			render 'id da publicação não informado'
			return
		}
		List listFichaRefBib = FichaRefBib.createCriteria().list{
			eq('publicacao.id', params.sqPublicacao.toLong() )
			vwFicha{
				order('nmCientifico')
			}
		}.unique{ it.publicacao }
		render( template:'divGridFichasComPublicacao',model:['lista' : listFichaRefBib ])
	}

	def saveCorrecaoDuplicidade()
	{
		sleep(500)
		Map res = [status:1,msg:'Executado...',type:'error']
		try {
			String cmdSql = """update salve.ficha_ref_bib set sq_publicacao = :sqPublicaoManter where sq_publicacao in ( ${params.sqPublicacoesExcluir} )"""
			sqlService.execSql(cmdSql, [sqPublicaoManter: params.sqPublicacaoManter.toLong()])

			// excluir as publicacoes duplicadas
			params.sqPublicacoesExcluir.split(',').each {
				params.id = it.toLong()
				delete() // excluir os anexos tambem
			}
			res.msg = 'Correção realizada com SUCESSO!'
			res.status=0
			res.type='success'
		} catch ( Exception e) {
			res.msg = e.getMessage()
		}
		render res as JSON
	}
}
