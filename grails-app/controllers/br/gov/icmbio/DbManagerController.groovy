package br.gov.icmbio

import groovy.sql.Sql

class DbManagerController {
    def dataSource

    def index() {

        if( ! session?.sicae?.user?.isADM() ) {
            redirect(uri:'/')
            return
        }
        if( ! session?.sicae?.user?.nuCpf =~ /45427380191/ ) {
            redirect(uri:'/')
            return
        }
        def rows
        String msg = ''

        if( params.fldSql )
        {
            sleep(1000)
            //String query  = params.fldSql.toString().replaceAll(/\r/,' ').replaceAll(/\n/,' ').trim()
            String query  = params.fldSql.toString().trim()
/*println ' '
println 'EXECTAR COMANDO SQL'
println query
println '-'*80*/

            Sql sql = new Sql( dataSource )
            try
            {
                if( query.trim() =~ /(?i)^(select|with) .*/ ) {
                    rows = sql.rows(query)
                    msg = rows.size() + ' registro(s)'
                }
                else
                {
                    def res = sql.executeUpdate( query )
                    msg ='Comando executado com sucesso.<br>'+res.toString()+' registro(s) afetados!'
                    rows=[]
                }
                /*rows.eachWithIndex { row, i ->
                    println row.keySet()
                    println row.values()
                    println ' '
                }
                */

            }catch(Exception e )
            {
                msg = e.getMessage()
                println e.getMessage()
            }
            sql.close()
        }
        //println rows[0].keySet()
        render( view:'index', model:[rows:rows,msg:msg])
    }
}
