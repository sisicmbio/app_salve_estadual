package br.gov.icmbio

import grails.converters.JSON

class CorpUsuarioController extends BaseController {

    def corpUsuarioService

    /**
     * Exibir o formulário de manutenção dos Usuários
     * @return
     */
    def index() {
        render(view:'index',model:[:] )
    }

    /**
     * Gravar/atualizar o Usuário no banco de dados
      * @return
     */
    def save(){
        Map res = [status:0, type:'success', msg:'Usuário gravado com SUCESSO', data:[:] ]
        try {
            res.data = corpUsuarioService.save(  params.sqPessoa ? params.sqPessoa.toLong() : null
                                   , params.nuCpf ?:''
                                   , params.deEmail ?:''
                                   , params.noPessoa ?:''
                                   , (params.stAtivo =='true' ? true : false )
                                   , params.deSenha  ?:''
                                   , params.sqInstituicao ? params.sqInstituicao.toLong() : null
                                   )

        } catch( Exception e ){
            res.status = 1
            res.type = 'error'
            res.msg = e.getMessage()
        }
        println ' '
        println res

        render res as JSON
    }

    /**
     * retornar os dados da Usuário para edição
     * @return
     */
    def edit(){
        Map res = [status:0, type:'success', msg:'', data:[:] ]
        try {
            res.data = corpUsuarioService.edit(  params.sqPessoa ? params.sqPessoa.toLong() : 0l)
        } catch( Exception e ){
            res.status = 1
            res.type = 'error'
            res.msg = e.getMessage()
        }
        render res as JSON
    }

    /**
     * excluir a usuário do banco de dados
     * @return
     */
    def delete(){
        Map res = [status:0, type:'success', msg:'Usuário excluída com SUCESSO', data:[:] ]
        try {
            res.data = corpUsuarioService.delete(  params.sqPessoa ? params.sqPessoa.toLong() : 0l)
        } catch( Exception e ){
            res.status = 1
            res.type = 'error'
            res.msg = e.getMessage()
        }
        render res as JSON
    }

    /**
     * criar o gride dos usuarios
     * @return
     */
    def grid(){
        Integer pageSize = 100
        Integer totalRecords = params.totalRecords ?: 0
        Integer totalPages
        Integer page
        try {
            if( !params.totalRecords ) {
                totalRecords = Usuario.createCriteria().count{}
            }
            totalPages = Math.ceil( totalRecords / pageSize )
            page = params.page ? params.page.toInteger() : 1
            page = Math.min(totalPages,page)
            List usuarios = corpUsuarioService.list( page, pageSize, params.q )
            render(template: 'gridUsuario', model: [usuarios: usuarios
                                                      , page:page
                                                      , totalRecords:totalRecords
                                                      , totalPages:totalPages])
        } catch( Exception e ) {
            render e.getMessage()
        }
    }


    /**
     * consultar usuário cadastrado pelo CPF
     * @return
     */
    def consultarCpf(){
        Map res = [status:0, type:'success', msg:'', data:[:] ]
        try {
            res.data = corpUsuarioService.consultarCpf(  params.nuCpf ?:'')
        } catch( Exception e ){
            res.status = 1
            res.type = 'error'
            res.msg = e.getMessage()
        }
        render res as JSON

    }


    /**
     * consultar usuário cadastrado pelo E-MAIL
     * @return
     */
    def consultarEmail(){
        Map res = [status:0, type:'success', msg:'', data:[:] ]
        try {
            res.data = corpUsuarioService.consultarEmail( params.deEmail ?:'')
        } catch( Exception e ){
            res.status = 1
            res.type = 'error'
            res.msg = e.getMessage()
        }
        render res as JSON

    }


    def perfil(){
        render( view:'perfil',model:[:])
    }
}
