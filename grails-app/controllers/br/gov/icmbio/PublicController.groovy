package br.gov.icmbio

import com.google.gson.stream.JsonReader

/**
 * Dúvidas
 *
 *
 *
 * 1) Analisar se podemos restringir a quantidade de registros nas exportações.
 * r) sim: 1000 para fichas e 100 para ocorrencias
 *
 * 1) Qual é o ano que deve ser colocado na seção Livro Vermelho?
 * r) o texto será alterado pela equipe e o ano fará parte do texto.
 *
 * 2) na criação da citação, deve ser removido os parentesis, o ano e o et. al do nome dos autores?
 * Ex: (Fernandes et al., 2014) fica Fernandes et al.
 *
 *
 * Ok 1) No calculo da quantidade de especies ameaçadas deve conta fichas da especies e subespecies ?
      r) Se a especie está ameaçada conta 1 registro, senão conta a quantidade de subepecies que estão ameaçadas
 *    r) Para as Avaliadas contam todas.
 *
 * Ok 2) Nomes comuns,Estados e biomas da espécie, também devem ser adicionados os das subespecies ?
 *    r) sempre as duas ocorrem no mesmo lugar

 * 3) Como será o tópico "Como citar?"
   r) Dever impresso nas fichas impressas e hmtl
      Ver no portal como foi feito para os registro de ocorrencias
      Ver no catalogo da fauna modelo para as fichas e pessoal vai me ajudar

 * 4) Bolar a seção sugerida pelo Rodrigo sobre os numeros do salve.
 * Ok 5) Alerta sobre exclusão de dados no SALVE.
 * Ok 6) Nome da camada "Salve Historico" no mapa vai ficar esse mesmo ?
      r) alterar para Registros Históricos (Salve)
 *
 * 7) Dúvida SALVE - a localização automática da UC quando o ponto é no mar, é possível?
 * r) sim, ver atol das rocas, costa dos corais, fernando de noronha
 *
 * 8) Quem somos
 *   OK - deixar somente a sala 4 e Rodrigo
 *   - Pontos focais
 *   ok  - adicionar coluna Ponto focal de Que ? (grupo)
 *   OK - Alterar para Abas iniciando pela Coordenação do processo, Pontos Focais, CT, Equipe apoio
 *
 * -------------------------------------------------------------------
 * 1) Somente as fichas PUBLICADAS podem ser visualizadas em  pdf/html ?
 * 2) Como será o cálculo dos totais na mudança de um ciclo para outro ?
 *    Na seção Especies Ameaçadas os cálculos são das fichas publicadas ou basta estar Avaliada ( aba 11.7 - preenchida )
 * 3) Na visualização da ficha html os valores não preenchidos devem ser ocultados?
 * 4) Qual será a última avaliação nacional ? O aba 11.7 ou a do histórico ( aba 7 )
 *    Se for da aba 11.7 qual será o Ano ?
 * 5) A ficha esta avaliada e publicada no ciclo 1 e avaliada e não publicada no ciclo 2,
 *    qual será a da ta da ultima Avaliação ?
 * 6) Deve ser exibido nomes comuns, ufs, biomas etc sempre da última ficha publicada ou
 * deve ser cumulativo de todos os ciclos. Ex. o nome X tem no ciclo 1 e foi retirado no ciclo 2.
 * 7) De onde virá o total da Lista Oficial da Fauna Ameaçada ? Está fixo em 1173 no exemplo
 * 8) Elaborar texto para os emails enviados com as exportações das pesquisas.
 * 9) Preciso fazer teste de preenchimento de campos sim/não e tabelas para ver como fica a impressao da ficha
 *    -  verificar o que das abas não está sendo impresso na ficha ou se ficou alguma coisa de fora
 * -----------------------------------------------------------------------------------
 *
 *
 **/
import grails.converters.JSON
import grails.util.Environment
import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import org.codehaus.groovy.grails.web.json.JSONArray
import org.codehaus.groovy.grails.web.json.JSONElement
import org.jsoup.Jsoup
import org.jsoup.safety.Whitelist
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream
import static groovy.io.FileType.FILES

class PublicController {

    def sqlService
    def dadosApoioService
    def fichaService
    def emailService
    def requestService

    int cacheDays = 0 // sem cache
    int maxRowsExport = 1000
    int maxRowsOccurrenceExport = 10000

    //String msgDiferencaCategorias = '(*) Essa categoria é resultado da avaliação mais recente da espécie. Entretanto, essa categoria ainda não foi oficializada por meio de Portaria do MMA. Para fins legais, deve-se utilizar a categoria %s, oficializada nas Portarias MMA 444/2014 ou 445/2014.'
    String msgDiferencaCategorias = '(*) Resultado da avaliação mais recente da espécie. Entretanto, essa categoria ainda não foi oficializada por meio de Portaria do MMA. Para fins legais, deve-se utilizar a categoria constante nas Portarias MMA 444/2014 ou 445/2014.'

    String captchaPublicKey = '6LccEfwUAAAAANVsH32vgjl1FuhAqMrB0ltO6LRu'
    String captchaSecretKey = '6LccEfwUAAAAAAB2bpZZFrLt35DaKOk1-KKTljoe'

    //https://developers.google.com/recaptcha/docs/v3
    //https://developers.google.com/recaptcha/docs/verify
    String captchaV3PublicKey = '6Ld5iaMZAAAAAF9AKP-_wnNDzeeX1ZTARbRSmtIN'
    String captchaV3SecretKey = '6Ld5iaMZAAAAAC6fQz4hYVouQghq4BPZcSn8Opsm'

    private boolean debug = (Environment.current.toString() == 'DEVELOPMENT')

    /**
     * metodo para criar log de depuracao
     * @param text
     */
    private void d(String text ){
        if( debug ) {
            if( text.trim() != '' ) {
                String h = new Date().format('HH:mm:ss')
                println h + ':' + text
            } else {
                println ' '
            }
        }
    }

    /**
     * método para remover os parentes e o ano do nome para gerar o texto "Como citar"
     * @param nome
     */
    private _tratarNomeCitacao(String nome='') {
        // remover espaços do inicio e fim
        nome = nome.trim()
        // remover asterico
        nome = nome.replaceAll(/\*/,'').trim()
        // remover parenteses
        nome = nome.replaceAll(/\(|\)/,'').trim()
        // trocar ponto e virgula por virgula
        nome = nome.replaceAll(/;/,',').trim();
        // remover ano do final
        nome = nome.replaceAll(/[0-9]{4}$/,'').trim();
        // remover a virgula do final
        nome = nome.replaceAll(/,$/,'').trim();
        return nome
    }

    private String _getPieColor(String codigo = '') {
        String color = ''
        List colors = ['#594157','#726da8','#7d8cc4','#a0d2db','#bee7e8'
                       ,'#362023','#700548','#8aa399','#dbf4a7'
                       ,'#247ba0','#70c1b3','#b2dbbf','#f3ffbd','#ff1654'
                       ,'#af3800','#fe621d','#0033cc','#aaff99'
                       ,'#999966'].sort();

        List predefinedKeys = ['MAMIFEROS','AVES','ANFIBIOS','REPTEIS','PEIXES_AGUA_DOCE','PEIXES_MARINHOS','INVERTEBRADOS_AGUA_DOCE','INVERTEBRADOS_MARINHOS','INVERTEBRADOS_TERRESTRES','EX','EW','RE','CR','EN','VU','NT','LC','DD','NA'].sort()
        // EN,CR,VU SÃO CORES PREFIXADAS
        if( codigo == 'EN') {
            color ='#FF8C00'
        } else if( codigo == 'CR') {
            color='#DC143C'
        } else if( codigo == 'VU') {
            color = '#FFD700'
        } else {
            int key = predefinedKeys.indexOf(codigo);
            if( key == -1 || ( key + 1 > colors.size() ) ) {
                color = Util.getRandomColor();
            } else {
                //println codigo+' esta predefinido na cor ' + colors[key]+ ' Key:' + key
                color = colors[key]
            }
        }
        return color;
    }

    /**
     * método para remover as tags HTML dos texto
     * @param text
     * @return
     */
    private _stripTags(String text ){
        return  Util.trimEditor( Jsoup.clean( Util.removeBgWhite( Util.html2str( text ) ) ,
                    Whitelist.none().addTags('p','i','b','strong','em') ) )
    }

    /**
     * metodo para encriptar os ids das fichas
     * @param value
     * @return
     */
    private String encryptStr( String value ){
        value = value ?: ''
        return AESCryption.encrypt( value, new Date().format('ddMMyyyy') )
        //return Util.encryptStr( value )
    }

    /**
     * metodo para desencriptar os ids das fichas
     * @param value
     * @return
     */
    private String decryptStr( String value ){
        value = value ?: ''
        try {
            return AESCryption.decrypt(value, new Date().format('ddMMyyyy'))
        } catch( Exception e ){
            println 'Metodo decryptStr - public controller'
            println 'Chave inválida ou vencida: ' + value
            return '0'
        }
        //return Util.decryptStr( value.replaceAll(' ', '+' ) )
    }

    /**
     * retornar a categoria final da ficha formatada
     * @param ficha
     * @return
     */
    private String _getCategoriaFinal( Ficha ficha ) {
        // ler a categoria final da ficha
        if( ficha && ficha.categoriaFinal ) {
            return  ficha.categoriaFinal.descricao + ' (' + ficha.categoriaFinal.codigoSistema + ')'
        }
        return ''
    }

    /**
     * retornar a lista de ids filhos quando o filtro selecionado
     * possuir hierarquia.
     * @example ameaças, usos etc
     * @param ids
     * @return
     */
    private String _getSubNiveisDadosApoio( String ids = '' ){
        String listIds = ''
        if( ids ) {
            sqlService.execSql("""WITH RECURSIVE arvore(sq_dados_apoio, sq_dados_apoio_pai) AS
                      ( SELECT da.sq_dados_apoio,
                               da.sq_dados_apoio_pai
                       FROM salve.dados_apoio da
                       WHERE da.sq_dados_apoio in( ${ids} )
                       UNION ALL SELECT dc.sq_dados_apoio,
                                        dc.sq_dados_apoio_pai
                       FROM arvore arvore_1
                       JOIN salve.dados_apoio dc ON dc.sq_dados_apoio_pai = arvore_1.sq_dados_apoio )
                    SELECT array_to_string(array_agg( distinct arvore.sq_dados_apoio), ', ') AS ids
                    FROM arvore;
                    """).each { row ->
                listIds = row[0]
            }
        }
        return listIds

    }

    /**
     * retornar a lista de ids filhos do USO
     * @example alimentação humana, veneno etc
     * @param ids
     * @return
     */
    private String _getSubNiveisUso( String ids = '' ){
        String listIds = ''
        if( ids ) {
            sqlService.execSql("""WITH RECURSIVE arvore(sq_uso, sq_uso_pai) AS
                      ( SELECT da.sq_uso,
                               da.sq_uso_pai
                       FROM salve.uso da
                       WHERE da.sq_uso in( ${ids} )
                       UNION ALL SELECT dc.sq_uso,
                                        dc.sq_uso_pai
                       FROM arvore arvore_1
                       JOIN salve.uso dc ON dc.sq_uso_pai = arvore_1.sq_uso )
                    SELECT array_to_string(array_agg( distinct arvore.sq_uso), ', ') AS ids
                    FROM arvore;;
                    """).each { row ->
                listIds = row[0]
            }
        }
        return listIds
    }

    /**
     * método para gravar a ação realizada pelo usuário na tabela de trilha de auditoria
     * @param acao
     * @param modulo
     * @param data
     * @return
     */
    private void _registrarTrilhaAuditoria( String acao, String modulo, Map data) {
        d('Registrando Trilha auditoria ')
        if( data ) {
            data.remove('reCaptchaToken')
        }
        TrilhaAuditoria ta = new TrilhaAuditoria()
        ta.deEnv = Environment.current.toString()
        ta.noAcao = acao ?: null
        ta.noModulo = modulo ?: null
        ta.deIp = null
        ta.dtLog = new Date()
        ta.nuCpf = null
        ta.noPessoa = 'publico'
        ta.txParametros = (data as JSON).toString().trim()
        ta.save()
        d('Trilha registrada com SUCESSO!')
    }

    /**
     * método para gerar planilha das fichas consultadas
     * @param data
     */
    private String _exportPlanilhaFichas(List listFichas=[], JSONArray criterias=null ) {
        DadosApoio contextoBacia = dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA','BACIA_HIDROGRAFICA')
        DadosApoio contextoOcorrencia = dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA','REGISTRO_OCORRENCIA')

        // criar query para recuperar as informações complementares das fichas
        List idsFichas = []
        listFichas.each {
            idsFichas.push( decryptStr( it.sq_ficha ).toString() )
        }
        if( idsFichas.size() > maxRowsExport ) {
            throw new Exception('Para exportação de fichas, selecione no máximo ' + maxRowsExport+' por vez')
        }

        String cmdSql ="""select final.sq_ficha
            ,final.sq_grupo_salve
            ,final.sq_tendencia_populacional
            ,final.nm_cientifico
            ,final.no_filo
            ,final.no_classe
            ,final.no_ordem
            ,final.no_familia
            ,final.de_criterio_final
            ,final.tx_justificativa_final
            ,final.de_endemica
            ,final.de_presenca_lista_vigente
            ,final.de_migratoria
            ,final.no_grupo
            ,final.de_tendencia_populacional
            ,final.no_autor
            -- campos N
            ,array_to_string( array_agg( distinct final.de_bacia ),', ') as de_bacia
            ,array_to_string( array_agg( distinct final.de_ameaca),', ') as de_ameaca
            ,array_to_string( array_agg( distinct final.de_uso ),', ') as de_uso
            ,array_to_string( array_agg( distinct final.de_acao_conservacao ),', ') as de_acao_conservacao
            ,array_to_string( array_agg( distinct final.no_uc ),', ') as no_uc
            ,array_to_string( array_agg( distinct final.de_lista_convencao ),'|') as de_lista_convencao
            FROM (
                select motor.*
                ,grupo.ds_dados_apoio as no_grupo
                ,tendenciaPopulacional.ds_dados_apoio as de_tendencia_populacional
                ,bacia.ds_dados_apoio as de_bacia
                ,ameacas.trilha as de_ameaca
                ,usos.trilha as de_uso
                ,acaoConservacao.trilha as de_acao_conservacao
                ,case when vw_uc.sg_unidade = '' then vw_uc.no_uc else vw_uc.sg_unidade end as no_uc
                ,listaConvencao.ds_dados_apoio as de_lista_convencao
                from (
                    select ficha.sq_ficha, ficha.sq_grupo_salve, ficha.sq_tendencia_populacional
                    ,coalesce( taxon.json_trilha->'subespecie'->>'no_taxon', taxon.json_trilha->'especie'->>'no_taxon') as nm_cientifico
                    ,taxon.no_autor
                    ,taxon.json_trilha->'filo'->>'no_taxon' as no_filo
                    ,taxon.json_trilha->'classe'->>'no_taxon' as no_classe
                    ,taxon.json_trilha->'ordem'->>'no_taxon' as no_ordem
                    ,taxon.json_trilha->'familia'->>'no_taxon' as no_familia
                    ,ficha.ds_criterio_aval_iucn_final as de_criterio_final
                    ,salve.remover_html_tags( ficha.ds_justificativa_final ) as tx_justificativa_final
                    ,salve.snd(ficha.st_endemica_brasil) as de_endemica
                    ,salve.snd(ficha.st_presenca_lista_vigente) as de_presenca_lista_vigente
                    ,salve.snd(ficha.st_migratoria) as de_migratoria
                    from salve.ficha
                    inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                    inner join salve.ciclo_avaliacao as ciclo on ciclo.sq_ciclo_avaliacao=ficha.sq_ciclo_avaliacao
                    where ciclo.in_publico = 'S'
                      AND ficha.sq_ficha in( ${ idsFichas.join(',') } )
                ) as motor
                left outer join salve.ficha_ocorrencia as ocorrencia_bacia on ocorrencia_bacia.sq_ficha = motor.sq_ficha AND ocorrencia_bacia.sq_contexto = ${contextoBacia.id.toString()}
                left outer join salve.dados_apoio as bacia on bacia.sq_dados_apoio = ocorrencia_bacia.sq_bacia_hidrografica
                LEFT OUTER JOIN salve.dados_apoio as grupo on grupo.sq_dados_apoio = motor.sq_grupo_salve
                LEFT OUTER JOIN salve.dados_apoio as tendenciaPopulacional on tendenciaPopulacional.sq_dados_apoio = motor.sq_tendencia_populacional
                --AMEAÇAS
                left outer join salve.ficha_ameaca on ficha_ameaca.sq_ficha = motor.sq_ficha
                left outer join salve.vw_ameacas as ameacas on ameacas.id=ficha_ameaca.sq_criterio_ameaca_iucn
                --USOS
                left outer join salve.ficha_uso on ficha_uso.sq_ficha = motor.sq_ficha
                left outer join salve.vw_usos as usos on usos.id = ficha_uso.sq_uso
                --AÇÕES CONSERVAÇÃO
                left outer join salve.ficha_acao_conservacao on ficha_acao_conservacao.sq_ficha = motor.sq_ficha
                left outer join salve.vw_acao_conservacao as acaoConservacao on acaoConservacao.id = ficha_acao_conservacao.sq_acao_conservacao
                --  UNIDADES DE CONSERVACAO
                LEFT OUTER JOIN salve.ficha_ocorrencia as ocorrencia_ucs on ocorrencia_ucs.sq_ficha = motor.sq_ficha AND ocorrencia_ucs.sq_contexto = ${ contextoOcorrencia.id.toString() } and ( ocorrencia_ucs.sq_uc_federal is not null or ocorrencia_ucs.sq_uc_estadual is not null or ocorrencia_ucs.sq_rppn is not null)
                LEFT OUTER JOIN salve.vw_uc ON ( vw_uc.sq_uc = ocorrencia_ucs.sq_uc_federal and vw_uc.in_esfera='F') OR ( vw_uc.sq_uc = ocorrencia_ucs.sq_uc_estadual and vw_uc.in_esfera='E') OR ( vw_uc.sq_uc = ocorrencia_ucs.sq_rppn and vw_uc.in_esfera='R')
                -- LISTAS E CONVENCOES
                LEFT OUTER JOIN salve.ficha_lista_convencao ON ficha_lista_convencao.sq_ficha = motor.sq_ficha
                LEFT OUTER JOIN salve.dados_apoio as listaConvencao on listaConvencao.sq_dados_apoio = ficha_lista_convencao.sq_lista_convencao

            ) as final
            GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16
            """
           /** /
            println '-'*80
            println '-'*80
            println '-'*80
            println cmdSql
            /**/
        List listFichasFinal = []
        try {
            //listFichasFinal = sqlService.execSql(cmdSql)
            listFichasFinal = _getDataFromCache(cmdSql, [:])
        } catch( Exception e ) {
            throw new Exception( e.getMessage() )
        }
        // array de nomes para criar a citação
        List nomesCitacao=[]
        // complementar a listFichaFinal com as colunas do grid consulta: Estados, biomas, categoria final etc..
        if( listFichasFinal ) {
            listFichas.each {
                Long sqFicha = decryptStr(it.sq_ficha).toLong();
                Map item = listFichasFinal.find {
                    it.sq_ficha == sqFicha
                }
                if( item.no_autor ) {
                    String nome = _tratarNomeCitacao( item.no_autor )
                    if( ! nomesCitacao.contains( nome ) ) {
                        nomesCitacao.push( nome )
                    };
                }
                if (item && item.nm_cientifico) {
                    //item['nm_cientifico'] = it.nm_cientifico ?: '' // usar nome cientifico da tabela taxon
                    item['no_comum'] = it.no_comum ?: ''
                    item['de_categoria_final_completa'] = it.de_categoria_final_completa ?: ''
                    item['de_periodo_avaliacao'] = it.de_periodo_avaliacao ?: ''
                    item['sg_estado'] = it.sg_estado ?: ''
                    item['no_bioma'] = it.no_bioma ?: ''
                    item['no_uc'] = item.no_uc
                } else {
                    item['nm_cientifico'] = ''
                    item['no_comum'] = ''
                    item['de_categoria_final_completa'] = ''
                    item['de_periodo_avaliacao'] = ''
                    item['sg_estado'] = ''
                    item['no_bioma'] = ''
                }
                // ajustar campos trilhas que veem unidos com pipe e vigula 1-sss,1.1-xxxx|2-...,2.1....
                if (item.de_ameaca) {
                    List tempArray = []
                    item.de_ameaca.split(',').each { item1 ->
                        item1.split('\\|').each { subItem ->
                            if (!tempArray.contains(subItem)) {
                                tempArray.push(subItem.toString().trim())
                            }
                        }
                    }
                    item.de_ameaca = tempArray.join('\n');
                }
                // usos
                if (item.de_uso) {
                    List tempArray = []
                    item.de_uso.split(',').each { item1 ->
                        item1.split('\\|').each { subItem ->
                            if (!tempArray.contains(subItem)) {
                                tempArray.push(subItem.toString().trim())
                            }
                        }
                    }
                    item.de_uso = tempArray.join('\n');
                }
                // açao de conservação
                if (item.de_acao_conservacao) {
                    List tempArray = []
                    item.de_acao_conservacao.split(',').each { item1 ->
                        item1.split('\\|').each { subItem ->
                            if (!tempArray.contains(subItem)) {
                                tempArray.push(subItem.toString().trim())
                            }
                        }
                    }
                    item.de_acao_conservacao = tempArray.sort().join('\n');
                }

                // lista e convenção
                if (item.de_lista_convencao ) {
                    List tempArray = []
                    item.de_lista_convencao.split(',').each { item1 ->
                        item1.split('\\|').each { subItem ->
                            if (!tempArray.contains(subItem)) {
                                tempArray.push( '- ' + subItem.toString().trim())
                            }
                        }
                    }
                    item.de_lista_convencao = tempArray.sort().join('\n');
                }
            }
        }
        String path     = grailsApplication.config.temp.dir
        String hora     = new Date().format('dd_MM_yyyy_hh_mm_ss')
        String fileName = path + 'salve_publico-exportacao-fichas-planilha-' + hora + '.xlsx'

        //---------------------------------------------------
        PlanilhaExcel planilha = new PlanilhaExcel( fileName,'Fichas exportadas',false)

        // informar os filtros utilizados na consulta
        planilha.setCriteria( criterias );

        // adicionar o texto da citação para ser impresso no final da planilha
        planilha.setCitation( nomesCitacao.join(', ') )

        planilha.setData( listFichasFinal )

        planilha.addColumn( ['title':'Filo'                             , 'column':'no_filo','autoFit':true] )
        planilha.addColumn( ['title':'Classe'                           , 'column':'no_classe','autoFit':true] )
        planilha.addColumn( ['title':'Ordem'                            , 'column':'no_ordem','autoFit':true] )
        planilha.addColumn( ['title':'Família'                          , 'column':'no_familia','autoFit':true] )
        planilha.addColumn( ['title':'Nome científico'                  , 'column':'nm_cientifico','autoFit':true] )
        planilha.addColumn( ['title':'Autor'                            , 'column':'no_autor','autoFit':true] )
        planilha.addColumn( ['title':'Nome comum'                       , 'column':'no_comum','autoFit':true] )
        planilha.addColumn( ['title':'Grupo'                            , 'column':'no_grupo','autoFit':true] )
        planilha.addColumn( ['title':'Última avaliação'                 , 'column':'de_periodo_avaliacao','autoFit':true] )
        planilha.addColumn( ['title':'Categoria'                        , 'column':'de_categoria_final_completa','autoFit':true] )
        planilha.addColumn( ['title':'Critério'                         , 'column':'de_criterio_final','autoFit':true] )
        planilha.addColumn( ['title':'Justificativa'                    , 'column':'tx_justificativa_final','autoFit':true] )
        planilha.addColumn( ['title':'Endêmica Brasil'                  , 'column':'de_endemica','autoFit':true] )
        planilha.addColumn( ['title':'Protegido Legislação'             , 'column':'de_presenca_lista_vigente','autoFit':true] )
        planilha.addColumn( ['title':'Estado'                           , 'column':'sg_estado','autoFit':true] )
        planilha.addColumn( ['title':'Bioma'                            , 'column':'no_bioma','autoFit':true] )
        planilha.addColumn( ['title':'Bacia Hidrográfica'               , 'column':'de_bacia','autoFit':true] )
        planilha.addColumn( ['title':'Unidade Conservação'              , 'column':'no_uc','autoFit':true] )

        planilha.addColumn( ['title':'Migratória'                       , 'column':'de_migratoria','autoFit':true] )
        planilha.addColumn( ['title':'Tendência Populacional'           , 'column':'de_tendencia_populacional','autoFit':true] )
        planilha.addColumn( ['title':'Ameaça'                           , 'column':'de_ameaca','autoFit':true] )
        planilha.addColumn( ['title':'Uso'                              , 'column':'de_uso','autoFit':true] )
        planilha.addColumn( ['title':'Ação Conservação'                 , 'column':'de_acao_conservacao','autoFit':true] )
        planilha.addColumn( ['title':'Listas e Convenções'              , 'column':'de_lista_convencao','autoFit':true] )

        if( ! planilha.run() ) {
            throw new Exception( planilha.getError() )
        }
        return fileName;
    }

    /**
     * método para gerar planilha com os registros de ocorrência
     * @param data
     */
    private String _exportPlanilhaOcorrencias( List listIdsFichas=[], List criterias = [] ) {
        //Map res         = [ status:0, msg:'',data:[:] ]
        String path     = grailsApplication.config.temp.dir
        String hora     = new Date().format('dd_MM_yyyy_hh_mm_ss')
        String dataAtual= new Date().format('yyyy-MM-dd')
        String fileName = path + 'salve_exportacao_registros_' + hora + '.xlsx'
        if( !listIdsFichas || listIdsFichas.size() == 0 ) {
            throw new Exception('Nenhuma ficha informada para exportação das ocorrências.')
        }
        if( listIdsFichas.size() > maxRowsExport ) {
            throw new Exception('Para exportação dos registros de ocorrências, selecione no máximo ' + maxRowsExport+' taxons por vez.')
        }

        d('Inicio exportação registros')
        d('Total de fichas: ' + listIdsFichas.size() )
        d('Iniciando consulta ao banco de dados...');
        String cmdSql ="""SELECT res.* from (
                -- LER OCORRÊNCIAS DO SALVE
                select tmp.*
                ,taxon.json_trilha->'reino'->>'no_taxon' as no_reino
                ,taxon.json_trilha->'filo'->>'no_taxon' as no_filo
                ,taxon.json_trilha->'classe'->>'no_taxon' as no_classe
                ,taxon.json_trilha->'ordem'->>'no_taxon' as no_ordem
                ,taxon.json_trilha->'familia'->>'no_taxon' as no_familia
                ,taxon.json_trilha->'genero'->>'no_taxon' as no_genero
                ,taxon.json_trilha->'especie'->>'no_taxon' as no_especie
                ,taxon.json_trilha->'subespecie'->>'no_taxon' as no_subespecie
                ,taxon.no_autor as no_autor_taxon
                from (
                Select ocorrencia.sq_ficha
                ,ocorrencia.nm_cientifico
                ,ocorrencia.nu_y
                ,ocorrencia.nu_x
                ,ocorrencia.no_prazo_carencia
                ,ocorrencia.dt_carencia
                ,ocorrencia.no_datum
                ,ocorrencia.no_formato_original
                ,ocorrencia.no_precisao
                ,ocorrencia.no_referencia_aproximacao
                ,ocorrencia.no_metodologia_aproximacao
                ,ocorrencia.tx_metodologia_aproximacao
                ,ocorrencia.no_taxon_citado
                ,ocorrencia.ds_presenca_atual
                ,ocorrencia.no_tipo_registro
                ,ocorrencia.nu_dia_registro
                ,ocorrencia.nu_mes_registro
                ,ocorrencia.nu_ano_registro
                ,ocorrencia.hr_registro
                ,ocorrencia.nu_dia_registro_fim
                ,ocorrencia.nu_mes_registro_fim
                ,ocorrencia.nu_ano_registro_fim
                ,ocorrencia.ds_data_desconhecida
                ,ocorrencia.no_localidade
                ,ocorrencia.ds_caracteristica_localidade
                ,ocorrencia.no_uc_federal
                ,ocorrencia.no_uc_estadual
                ,ocorrencia.no_rppn
                ,ocorrencia.no_pais
                ,ocorrencia.no_estado
                ,ocorrencia.no_municipio
                ,ocorrencia.no_ambiente
                ,ocorrencia.no_habitat
                ,ocorrencia.vl_elevacao_min
                ,ocorrencia.vl_elevacao_max
                ,ocorrencia.vl_profundidade_min
                ,ocorrencia.vl_profundidade_max
                ,ocorrencia.de_identificador
                ,ocorrencia.dt_identificacao
                ,ocorrencia.no_identificacao_incerta
                ,ocorrencia.ds_tombamento
                ,ocorrencia.ds_instituicao_tombamento
                ,ocorrencia.no_compilador
                ,ocorrencia.dt_compilacao
                ,ocorrencia.no_revisor
                ,ocorrencia.dt_revisao
                ,ocorrencia.no_validador
                ,ocorrencia.dt_validacao
                ,ocorrencia.no_autor
                ,ocorrencia.nu_ano_publicacao
                ,ocorrencia.de_titulo_publicacao
                ,ocorrencia.de_com_pessoal
                ,'SALVE' as no_base_dados
                ,case when ocorrencia.id_origem is null then concat('SALVE:',ocorrencia.sq_ficha_ocorrencia::text) else ocorrencia.id_origem::text end
                ,ocorrencia.tx_observacao
                FROM salve.vw_ficha_ocorrencia as ocorrencia
                inner join salve.ficha on ficha.sq_ficha = ocorrencia.sq_ficha
                inner join salve.ciclo_avaliacao as ciclo on ciclo.sq_ciclo_avaliacao = ficha.sq_ciclo_avaliacao
                WHERE ciclo.in_publico = 'S'
                  --AND ( ocorrencia.in_sensivel = 'N' or ocorrencia.in_sensivel is null)
                  AND ( ocorrencia.in_utilizado_avaliacao = 'S' or ocorrencia.in_utilizado_avaliacao is null )
                  AND ( ocorrencia.dt_carencia is null or ocorrencia.dt_carencia < '${dataAtual}' )
                  AND ( ocorrencia.in_sensivel is null or ocorrencia.in_sensivel='N' )
                  AND ocorrencia.sq_ficha in ( ${ listIdsFichas.join(',') } )
                ) tmp
                inner join salve.ficha on ficha.sq_ficha=tmp.sq_ficha
                inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon

                -- LER OCORRÊNCIAS DO PORTALBIO
                UNION ALL

                SELECT ficha.sq_ficha
                ,ficha.nm_cientifico
                ,st_x(op.ge_ocorrencia) AS nu_x
                ,st_y(op.ge_ocorrencia) AS nu_y
                ,null as  no_prazo_carencia
                ,op.dt_carencia
                ,null as no_datum
                ,null as no_formato_original
                ,case when left(tx_ocorrencia, 1)::text = '{' then op.tx_ocorrencia::json->>'precisaoCoord' else '' end as no_precisao
                ,null as no_referencia_aproximacao
                ,null as no_metodologia_aproximacao
                ,null as tx_metodologia_aproximacao
                ,null as no_taxon_citado
                ,null as ds_presenca_atual
                ,null as no_tipo_registro
                ,null as nu_dia_registro
                ,null as nu_mes_registro
                ,null as nu_ano_registro
                ,null as hr_registro
                ,null as nu_dia_registro_fim
                ,null as nu_mes_registro_fim
                ,null as nu_ano_registro_fim
                ,null as ds_data_desconhecida
                ,op.no_local as no_localidade
                ,null as ds_caracteristica_localidade
                ,null as no_uc_federal
                ,null as no_uc_estadual
                ,null as no_rppn
                ,null as no_pais
                ,null as no_estado
                ,null as no_municipio
                ,null as no_ambiente
                ,null as no_habitat
                ,null as vl_elevacao_min
                ,null as vl_elevacao_max
                ,null as vl_profundidade_min
                ,null as vl_profundidade_max
                ,null as de_identificador
                ,null as dt_identificacao
                ,null as no_identificacao_incerta
                ,null as ds_tombamento
                ,null as ds_instituicao_tombamento
                ,null as no_compilador
                ,null as dt_compilacao
                ,revisor.no_pessoa as no_revisor
                ,op.dt_revisao
                ,validador.no_pessoa as no_validador
                ,op.dt_validacao
                ,CASE WHEN op.no_autor is null then case when left( op.tx_ocorrencia, 1)::text = '{' then
                    case when op.tx_ocorrencia::json->>'autor'::text is null
                        then op.tx_ocorrencia::json->>'collector'::text
                    else op.tx_ocorrencia::json->>'autor'::text
                    end
                    else '' end else op.no_autor end as no_autor
                ,null as nu_ano_publicacao
                ,null as de_titulo_publicacao
                ,null as de_com_pessoal
                ,op.no_instituicao as no_base_dados
                ,case when op.id_origem is null then
                   case when left(tx_ocorrencia, 1)::text = '{' then op.tx_ocorrencia::json->>'uuid' else de_uuid end
                   else op.id_origem::text end as id_origem
                ,case when left(tx_ocorrencia, 1)::text = '{' then op.tx_ocorrencia::json->>'recordNumber' else '' end as tx_observacao
                ,taxon.json_trilha->'reino'->>'no_taxon' as no_reino
                ,taxon.json_trilha->'filo'->>'no_taxon' as no_filo
                ,taxon.json_trilha->'classe'->>'no_taxon' as no_classe
                ,taxon.json_trilha->'ordem'->>'no_taxon' as no_ordem
                ,taxon.json_trilha->'familia'->>'no_taxon' as no_familia
                ,taxon.json_trilha->'genero'->>'no_taxon' as no_genero
                ,taxon.json_trilha->'especie'->>'no_taxon' as no_especie
                ,taxon.json_trilha->'subespecie'->>'no_taxon' as no_subespecie
                ,taxon.no_autor as no_autor_taxon
                from salve.ficha_ocorrencia_portalbio  op
                inner join salve.ficha on ficha.sq_ficha = op.sq_ficha
                inner join salve.ciclo_avaliacao as ciclo on ciclo.sq_ciclo_avaliacao = ficha.sq_ciclo_avaliacao
                inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                left join salve.vw_pessoa as revisor on revisor.sq_pessoa = op.sq_pessoa_revisor
                left join salve.vw_pessoa as validador on validador.sq_pessoa = op.sq_pessoa_validador
                where ciclo.in_publico = 'S'
                  AND ( op.in_utilizado_avaliacao = 'S'  OR op.in_utilizado_avaliacao is null)
                  AND ( op.dt_carencia is null or op.dt_carencia < '${dataAtual}' )
                  AND op.sq_ficha in ( ${ listIdsFichas.join(',') } )
                  ) res
                  order by res.nm_cientifico
                """

        /** /

            println ' '
            println 'Exportacao dos registros'
            println cmdSql
        /**/

        List listOcorrencias = _getDataFromCache(cmdSql)
        d('Total de ocorrências:' + listOcorrencias.size())
        if( listOcorrencias.size() > maxRowsOccurrenceExport ) {
            throw new Exception('O total de registros ultrapassou o limite de máximo de '+ maxRowsOccurrenceExport +' por vez. ' +
                'Utilize os filtros para diminuir a quantidade de taxons da lista ou caso tenha utilizado a seleção por linhas, diminua a quantidade de linhas selecionadas.')
        }

        List nomesCitacao = []
        // adicionar italico no nome cientifico
        d('Executando pre-processamento...')
        listOcorrencias.each {
            it.nm_cientifico =  it.no_subespecie ?: it.no_especie //Util.ncItalico(it.nm_cientifico)
            if( it?.no_autor ) {
                String nome = it.no_autor.trim()
                if ( nome ) {
                    nome = Util.capitalize(nome)
                    if ( ! nomesCitacao.contains(nome) ) {
                        nomesCitacao.push(nome)
                    }
                }
            }
        }
        //println cmdSql
        d('Criando planilha...')
        PlanilhaExcel planilha = new PlanilhaExcel( fileName,'Ocorrências', false )
        planilha.setCriteria( criterias )
        planilha.setCitation( nomesCitacao.join(', ') )
        planilha.setObs('Obs: Registro em período de carência não estão listados!')
        planilha.setData( listOcorrencias)
        planilha.addColumn( ['title':'Nome científico'                  , 'column':'nm_cientifico'] )
        planilha.addColumn( ['title':'Autor'                            , 'column':'no_autor_taxon'] )
        planilha.addColumn( ['title':'Latitude'                         , 'column':'nu_y'   , 'type':'float'] )
        planilha.addColumn( ['title':'Longitude'                        , 'column':'nu_x'   , 'type':'float'] )
        //planilha.addColumn( ['title':'Prazo carência'                   , 'column':'no_prazo_carencia'] )
        //planilha.addColumn( ['title':'Fim carência'                     , 'column':'dt_carencia',   'type':'date'] )
        planilha.addColumn( ['title':'Datum'                            , 'column':'no_datum'] )
        planilha.addColumn( ['title':'Formato original'                 , 'column':'no_formato_original'] )
        planilha.addColumn( ['title':'Precisao da coordenada'           , 'column':'no_precisao'] )
        planilha.addColumn( ['title':'Referência aproximação'           , 'column':'no_referencia_aproximacao'] )
        planilha.addColumn( ['title':'Metodologia aproximação'          , 'column':'no_metodologia_aproximacao'] )
        planilha.addColumn( ['title':'Descrição do método aproximação'  , 'column':'tx_metodologia_aproximacao'] )
        planilha.addColumn( ['title':'Taxon citado'                     , 'column':'no_taxon_citado'] )
        planilha.addColumn( ['title':'Presença atual'                   , 'column':'ds_presenca_atual'] )
        planilha.addColumn( ['title':'Tipo registro'                    , 'column':'no_tipo_registro'] )
        planilha.addColumn( ['title':'Dia'                              , 'column':'nu_dia_registro'] )
        planilha.addColumn( ['title':'Mês'                              , 'column':'nu_mes_registro'] )
        planilha.addColumn( ['title':'Ano'                              , 'column':'nu_ano_registro'] )
        planilha.addColumn( ['title':'Hora'                             , 'column':'hr_registro'] )
        planilha.addColumn( ['title':'Dia fim'                          , 'column':'nu_dia_registro_fim'] )
        planilha.addColumn( ['title':'Mês fim'                          , 'column':'nu_mes_registro_fim'] )
        planilha.addColumn( ['title':'Ano fim'                          , 'column':'nu_ano_registro_fim'] )
        planilha.addColumn( ['title':'Data e Ano desconhecidos'         , 'column':'ds_data_desconhecida'] )
        planilha.addColumn( ['title':'Localidade'                       , 'column':'no_localidade'] )
        planilha.addColumn( ['title':'Característica da localidade'     , 'column':'ds_caracteristica_localidade'] )
        planilha.addColumn( ['title':'UC Federais'                      , 'column':'no_uc_federal'] )
        planilha.addColumn( ['title':'UC Não Federais'                  , 'column':'no_uc_estadual'] )
        planilha.addColumn( ['title':'RPPN'                             , 'column':'no_rppn'] )
        planilha.addColumn( ['title':'País'                             , 'column':'no_pais'] )
        planilha.addColumn( ['title':'Estado'                           , 'column':'no_estado'] )
        planilha.addColumn( ['title':'Município'                        , 'column':'no_municipio'] )
        planilha.addColumn( ['title':'Ambiente'                         , 'column':'no_ambiente'] )
        planilha.addColumn( ['title':'Hábitat'                          , 'column':'no_habitat'] )
        planilha.addColumn( ['title':'Elevação mínima'                  , 'column':'vl_elevacao_min', 'type':'float'] )
        planilha.addColumn( ['title':'Elevação máxima'                  , 'column':'vl_elevacao_max', 'type':'float'] )
        planilha.addColumn( ['title':'Profundidade mínima'              , 'column':'vl_profundidade_min', 'type':'float'] )
        planilha.addColumn( ['title':'Profundidade máxima'              , 'column':'vl_profundidade_max', 'type':'float'] )
        planilha.addColumn( ['title':'Identificador'                    , 'column':'de_identificador'] )
        planilha.addColumn( ['title':'Data identificação'               , 'column':'dt_identificacao',   'type':'date'] )
        planilha.addColumn( ['title':'Identificação incerta'            , 'column':'no_identificacao_incerta'] )
        planilha.addColumn( ['title':'Tombamento'                       , 'column':'ds_tombamento'] )
        planilha.addColumn( ['title':'Instituição tombamento'           , 'column':'ds_instituicao_tombamento'] )
        planilha.addColumn( ['title':'Compilador'                       , 'column':'no_compilador'] )
        planilha.addColumn( ['title':'Data compilação'                  , 'column':'dt_compilacao',   'type':'date'] )
        planilha.addColumn( ['title':'Revisor'                          , 'column':'no_revisor'] )
        planilha.addColumn( ['title':'Data revisão'                     , 'column':'dt_revisao',   'type':'date'] )
        planilha.addColumn( ['title':'Validador'                        , 'column':'no_validador'] )
        planilha.addColumn( ['title':'data validação'                   , 'column':'dt_validacao',   'type':'date'] )
        planilha.addColumn( ['title':'Autor registro'                            , 'column':'no_autor'] )
        planilha.addColumn( ['title':'Ano registro'                              , 'column':'nu_ano_publicacao'] )
        planilha.addColumn( ['title':'Título'                           , 'column':'de_titulo_publicacao'] )
        planilha.addColumn( ['title':'Comunicação Pessoal'              , 'column':'de_com_pessoal'] )
        planilha.addColumn( ['title':'Base dados'                       , 'column':'no_base_dados'] )
        planilha.addColumn( ['title':'Id Origem'                        , 'column':'id_origem'] )
        planilha.addColumn( ['title':'Observação'                       , 'column':'tx_observacao'] )
        if( ! planilha.run() ) {
            throw new Exception( planilha.getError() )
        }
        d('Fim planilha...')
        return fileName
    }

    /**
     * metodo para envio do arquivo exportado por email
     * @param strTo
     * @param strSubject
     * @param strMsg
     * @param strFileName
     * @return
     */
    private boolean _sendEmail(String strTo='', String strSubject='', String strMsg='', String strFileName='') {
        File anexo = new File( strFileName )
        if( anexo.exists() && strTo) {
            return emailService.sendTo(strTo
                    , strSubject ?: 'Exportação Sistema Salve.'
                    , strMsg ?: '<p>Segue anexo o arquivo com as informações solicitadas.</p>'
                    , null
                    , [fullPath: anexo.path, fileName: anexo.name])
        }
    }

    /**
     * método para gerar o pdf da lista de fichas e criar um arquivo zip para download
     * @param listIdsFichas
     * @return
     */
    private String _exportFichasZip( List listIdsFichas ) {
        String path     = grailsApplication.config.temp.dir
        String hora     = new Date().format('dd_MM_yyyy_hh_mm_ss')
        String fileName = path + 'salve_publico-exportacao-fichas-pdf-' + hora + '.zip'

        if( listIdsFichas.size() > maxRowsExport ) {
            throw new Exception('Para criação do arquivo ZIP, selecione no máximo ' + maxRowsExport+' fichas por vez')
        }

        d(' ')
        d('-'*40)
        d('Exportacao de ' + listIdsFichas.size()+' fichas para zip')
        d(' - inicio')
        d(' - criar arquivo zip: ' + fileName)

        //-------------------------------------------------------------------
        byte[] buffer = new byte[1024]
        FileOutputStream fos = new FileOutputStream(fileName)
        ZipOutputStream zos = new ZipOutputStream(fos)
        // loop gerar pdf
        try {
            listIdsFichas.each { id ->
                VwFicha ficha = VwFicha.get(id.toLong())
                if (ficha) {
                    /*
                    RtfFicha rtf = new RtfFicha(ficha.id.toInteger(),null,null,true)
                    rtf.unidade = ficha.sgUnidadeOrg.toString()
                    rtf.ciclo = ficha.getCicloAvaliacao().deCicloAvaliacao.toString()
                    String tempFileName = rtf.run()
                     */

                    PdfFicha pdf = new PdfFicha( ficha.id.toInteger(), null, true );
                    pdf.setContextoPublico(true)
                    pdf.unidade = ficha.sgUnidadeOrg.toString()
                    pdf.ciclo = ficha.getCicloAvaliacao().deCicloAvaliacao.toString()
                    pdf.setRefBibs(fichaService.getFichaRefBibs( ficha.id.toLong() ) )
                    pdf.setUfs(fichaService.getUfsNameByRegiaoText(  ficha.id.toLong() ) )
                    pdf.setBiomas(fichaService.getBiomasText( ficha.id.toLong() ))
                    pdf.setBacias(fichaService.getBaciasText( ficha.id.toLong() ))
                    String tempFileName = pdf.run()

                    File tempFile = new File(tempFileName)
                    if (tempFile.exists()) {
                        // adicionar ao zip e apagar
                        String entryName = tempFile.name
                        ZipEntry ze = new ZipEntry(entryName)
                        zos.putNextEntry(ze)
                        FileInputStream fis = new FileInputStream(tempFile)
                        int len
                        while ((len = fis.read(buffer)) > 0) {
                            zos.write(buffer, 0, len)
                        }
                        fis.close()
                        tempFile.delete()
                    }
                }
            }
            d(' - arquivo zip criado')
        } catch( Exception e) {
            throw e
        } finally {
            zos.closeEntry()
            zos.close()
        }
        return fileName
    }
    private String _cacheDir() {
        return grailsApplication?.config?.cache?.dir ?:'/data/salve-estadual/temp/'
    }
    private List _getDataFromCache( String cmd='', Map pars=[:] ) {
        String hashParams   = pars.hashCode()
        cmd = cmd.replaceAll(/-- ?.+/,'').replaceAll(/\n(\s+)/,' ')
        String hashSql      = cmd.hashCode()
        String cacheFileName= _cacheDir()+'public-' + hashParams+hashSql+'.cache'
        List data = []
        /** /
        if( debug ) {
            println ' '
            println 'CONSULTA MODULO PUBLICO - SQL - ' + new Date().format('dd/MM/yyyy HH:mm:ss')
            //println '_getDataFromCache()'
            println cmd
            println pars
        }
         /**/
        //d( '_getDataFromCache() ' + new Date().format('dd/MM/yyyy HH:mm:ss') + ' - '+cacheFileName)
        //d('Parametros:' + pars)
        File file = new File( cacheFileName )
        if( file.exists() && ! _cacheExpired( file ) ) {
            d('Cache recuperado...')
            def jsonSlurper = new JsonSlurper()
            data = jsonSlurper.parse(file)
        }
        if( ! data || data.size() == 0 ) {
            try {
                if( debug ) {
                    println '  - Inicio consulta ao banco de dados.:' + new Date().format('dd/MM/yyyy HH:mm:ss')
                }
                data = sqlService.execSql(cmd, pars)
                if( debug ) {
                    println '  - fim consulta ao banco de dados....:' + new Date().format('dd/MM/yyyy HH:mm:ss')
                }
                // só colocar no cache se a consulta retornou alguma linha
                if( data && data.size() > 0 ) {
                    def json_str = JsonOutput.toJson(data)
                    //def json_beauty = JsonOutput.prettyPrint( json_str )
                    file.write(json_str)
                    d('Dados colocados no CACHE...')
                }
            } catch( Exception e ) {
                throw new Exception( e.getMessage() );
            }
        }
        return data
    }
    /**
     * metodo para verificar se o arquivo em cache ja expirou
     * caso o tempo de vida tenha expirado, o arquivo é excluido
     * @param file
     * @return
     */
    private boolean _cacheExpired( File file) {
        // return true;
        //int maxDays = 1
        Date modifiedAt = new Date( file.lastModified() )
        int days = Util.dateDiff(modifiedAt, new Date(), 'D')
        d( "Dias " + days)
        if( days >= cacheDays ) {
            d( 'Excluido')
            file.delete();
            return true;
        };
        d( 'Cache valido')
        return false
    }

    //----------------------------------------------------------------------------------

    def index() {
        //if (!session?.sicae?.user || ! session?.sicae?.user?.isADM() || nuCpf != '45427380191' ) {
        //if (!session?.sicae?.user || ! session?.sicae?.user?.isPUB() || session.sicae.user.sqPessoa.toLong() != 9165l ) {


        if( ! session?.sicae?.user || session?.sicae?.user?.sqPessoa != 9165 ) {
            redirect(url: 'https://salve.icmbio.gov.br/')
            //render('<h1>Salve - Módulo Público</h1><h2>Necessário fazer login no SICA-e</h2><h3><a href="https://sicae.sisicmbio.icmbio.gov.br">Login</a></h3>')
            return;
            //redirect(uri: '/')
            //return;
        }

        if( session.sicae.user.sqPessoa.toLong() != 9165l ) {
            if ( ! session.sicae.user.isPUB() ) {
                render('<h1>Salve - Módulo Público</h1><h2>Acesso somente com perfil Público.</h2><h3><a href="https://sicae.sisicmbio.icmbio.gov.br">Selecionar perfil</a></h3>')
                return;
            }
        }

        if(params?.debug?.toUpperCase() == 'S') {
            session.setAttribute('debug','S')
        } else {
            session.setAttribute('debug','N')
        }

        CicloAvaliacao cicloAvaliacao = CicloAvaliacao.get( 3 )
        DadosApoio situacaoPublicada = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA','PUBLICADA')
        // ler os niveis taxonômicos
        List listNivelTaxonomico    = NivelTaxonomico.list()
        // ler categorias IUCN
        List listCategoria         = dadosApoioService.getTable('TB_CATEGORIA_IUCN').findAll{ it.codigoSistema != 'OUTRA' }.sort{ it.ordem };
        // ler estados
        List listEstado = Estado.list().sort{ it.noEstado}
        // ler biomas
        List listBioma = dadosApoioService.getTable('TB_BIOMA').sort{ it.ordem };
        // ler região
        List listRegiao = Regiao.list().sort{ it.noRegiao}

        // ler grupos especies para criação dos graficos das espécies em categoria de ameaça
        List listGrupoEspecie =  dadosApoioService.getTable('TB_GRUPO_SALVE').sort{ it.ordem };

        // desabilitar captcha por enquanto
        //captchaPublicKey='';
        //captchaV3PublicKey='';
        render(view: 'index', model:[captchaPublicKey:captchaV3PublicKey,
                /*  totalFichas         : Util.formatInt( rows[0].nu_fichas ?: 0)
                , totalPublicadas   : Util.formatInt( rows[0].nu_publicadas ?: 0)
                , totalAmeacadas    : Util.formatInt( rows[0].nu_ameacadas ?: 0)
                , totalPessoas      : Util.formatInt( rows[0].nu_pessoas ?: 0)*/
                listNivelTaxonomico   : listNivelTaxonomico
                , listCategoria         : listCategoria
                , listEstado            : listEstado
                , listBioma             : listBioma
                , listRegiao            : listRegiao
                , listGrupoEspecie      : listGrupoEspecie
        ])
    }

    /**
     * método para realizar as consultas de acordo com o(s) filtro(s)
     * definidos pelo usuário
     * @return
     */
    def search() {
        //this.debug = (session && session.getAttribute('debug') == 'S' ? true : false)
        debug=false;

        d( ' ')
        d('SEARCH PARAMS')
        d( params.toString(  ) )
        List listTemp=[]
        Taxon taxon = null
        NivelTaxonomico nivelEspecie = NivelTaxonomico.findByCoNivelTaxonomico('ESPECIE')
        NivelTaxonomico nivelSubespecie = NivelTaxonomico.findByCoNivelTaxonomico('SUBESPECIE')
        List listIds = []
        JSONArray criterias = []
        if( params.criterias && params.criterias.size() > 0 ) {
            criterias = (JSONArray) JSON.parse(params.criterias)
            d(' - Filtros:');
            criterias.each { it ->
                d('   - ' + it.title + ' = ' + it.value);
            }
        }
        String cmd = ''
        Map pars = [:]
        Map pagination = [records: 0, pageSize: 100, page: 1, offSet: 0, pages: 0]
        Map resultado = ['status': 0, msg: '', type: 'success', pagination: pagination, outputFormat: 'table', data: []]

        // validar recaptcha
        if( params.reCaptchaToken ) {
            String remoteIp = request.getRemoteAddr()
            if( ! remoteIp ) {
                remoteIp = request.getRemoteHost();
            }
            String captchaValidateUrl = "https://www.google.com/recaptcha/api/siteverify?secret=${captchaV3SecretKey}&response=${params.reCaptchaToken}&remoteip=${remoteIp}"
            //d('   - reCaptchaToken:' + params.reCaptchaToken );
            //d('   - remoteIp: ' + remoteIp )
            //d('   - url verify: ' + captchaValidateUrl)
            JSONElement dados = requestService.doGetJson( captchaValidateUrl )

            if( ! dados || ! dados.success || ! dados.score || dados.score < 0.7 ) {
                resultado.status = 1
                resultado.text  = 'Consulta negada pelo recaptcha.'
                resultado.msg = 'Você é um robô.'
                resultado.type ='info'
                resultado.data = []
                render resultado as JSON
                return;
            }
        } else {
            resultado.status = 1
            resultado.text  = 'Consulta negada pela falta do token de validação.'
            resultado.msg = 'Erro reCaptcha'
            resultado.type ='error'
            resultado.data = []
            render resultado as JSON
            return;
        }
        if (params.records && params.records.toInteger() > 0) {
            pagination.records = params.records.toInteger()
        }

        if (params.pageSize && params.pageSize.toInteger() > 0) {
            pagination.pageSize = params.pageSize.toInteger()
        }

        if (params.page && params.page.toInteger() > 0) {
            pagination.page = params.page.toInteger()
        }
        if (params.outputFormat) {
            resultado.outputFormat = params.outputFormat
        }

        /** /
        println ' '
        println 'Search modulo publico: Parametros'
        println params
        println ' '
        /**/

        // exportar todos os registros se all for true
        params.all = params.boolean('all',false)
        if( params.all ) {
            listIds=[]
        } else {
            if( params.selectedIds ) {
                listIds = params.selectedIds.split(',').collect {
                    decryptStr(it)
                }
            }
        }
        /**
         * Exportar as ocorrencias da(s) ficha(s) selecionada(s) no gride
         */
        if (params.outputFormat == 'ocorrencias') {
            if ( listIds.size() > 0 ) {
                try {
                    resultado.data = ['format': params.outputFormat, 'fileName': _exportPlanilhaOcorrencias(listIds, criterias)]
                    // gravar log
                    params.fileName=resultado.data.fileName
                    _registrarTrilhaAuditoria('exportar_ocorrencia','salve-publico',params)
                } catch(Exception e ){
                    params.email=null
                    resultado.msg = e.getMessage()
                    resultado.status=1
                    resultado.type='error'
                }
                if (params.email) {
                    if (!_sendEmail(params.email.toLowerCase()
                            , 'Exportação Salve'
                            , ''
                            , resultado.data.fileName)) {
                        resultado.msg = 'Erro no envio do email para ' + params.email
                        resultado.status = 1
                    } else {
                        String linkDownload = '<a title="Baixar o arquivo." href="javascript:void(0);" onClick="doOpenWindow(\'' + resultado.data.fileName + '\')"><i class="fa fa-download"></i></a>'
                        resultado.msg = 'Exportação finalizada.&nbsp;' + linkDownload
                        resultado.text = 'Arquivo foi enviado para o email ' + params.email
                    }
                    resultado.data.fileName = '';
                }
                render resultado as JSON
                return;
            }
        }
        //CicloAvaliacao cicloAvaliacao = CicloAvaliacao.get(3)
        DadosApoio situacaoPublicada = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'PUBLICADA')
        DadosApoio contextoEstado = dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA', 'ESTADO')
        DadosApoio contextoOcorrencia = dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA', 'REGISTRO_OCORRENCIA')
        //DadosApoio contextoConsulta = dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA', 'CONSULTA')
        DadosApoio contextoBioma = dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA', 'BIOMA')
        DadosApoio contextoBacia = dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA', 'BACIA_HIDROGRAFICA')

        try {
            if (params.sqTaxon) {
                taxon = Taxon.get(params.sqTaxon.toLong())
                if (!taxon) {
                    throw new Exception('Taxon inválido.')
                }
            }

            // necessário informar algum texto para pesquisa quando não for pesquisa avançada
            params.q = params.q ? params.q.trim() : ''
            params.q = URLDecoder.decode( params.q.toString(), "UTF-8" )
            if ((!params.q || params.q == '')
                    && !taxon
                    && !params.sqCategoria
                    && !params.sqEstado
                    && !params.sqRegiao
                    && !params.sqBioma
                    && !params.grupo // filtro graficos
                    && !params.categoria
                    && !params.sqGrupo
                    && !params.sqUc
                    && !params.sqAmeaca
                    && !params.sqUso
                    && !params.inPublicadas
                    && !params.sqBacia
                    && !params.sqPan

            ) {
                resultado.status=0
                resultado.msg  = 'Nenhum parâmetro informado.'
                resultado.text = 'Para realizar a pesquisa é preciso informar algum parâmetro.'
                resultado.type ='info'
                resultado.data = []
                render resultado as JSON
                return;
            }

            // aceitar somente se tiver no mínico 3 caracteres
            if (params.q && params.q.size() < 3) {
                resultado.status=0
                resultado.msg  = 'Mínimo 3 caracteres.'
                resultado.text = 'Para realizar a pesquisa é preciso informar pelo menos 3 caracteres.'
                resultado.type ='info'
                resultado.data = []
                render resultado as JSON
                return;
            }
                List whereLevel0=[]
                String orderBy = ''

                if( params.q ) {
                    //whereLevel0.push("concat( level0.nm_cientifico,' ', ficha_nome_comum.no_comum)::text ilike :q")
                    //pars.q = '%' + params.q + '%'
                    params.q = Util.removeAccents(params.q.toLowerCase() ).replaceAll(/^-|-$/,'').trim()
                    String regex = "'^${params.q}[ -]|[ -]${params.q}\$|[ -]${params.q}[ -]|^${params.q}\$'"
                    whereLevel0.push("vw.tx_nomes_taxon ~* ${regex}")
                }

                // filtrar somente as fichas publicadas ( Exportarcao ficha pdf para zip )
                if ( params.outputFormat == 'zip' || params?.inPublicadas == 'S' ) {
                    whereLevel0.push("vw.sq_ficha_publicada is not null")
                }

                // filtro pelo Taxon
                if (params.sqTaxon) {
                    taxon = Taxon.get(params.sqTaxon.toLong())
                    if (!taxon) {
                        throw new Exception('Taxon inválido.')
                    }
                    whereLevel0.push("vw.sq_${ taxon.nivelTaxonomico.coNivelTaxonomico.toLowerCase()}::bigint = "+taxon.id.toString() )
                    //whereLevel0.push()
                }

                // filtro pelo codigo da categoria ou as Ameaçadas
                if( params.categoria ) {
                    if ( params.categoria.toUpperCase() == 'AMEACADAS' ) {
                        params.sqCategoria = dadosApoioService.getCategoriasAmeacadas()?.id?.join(',');
                    } else {
                        // quando clica nas tabelas é enviado o codigo da categoria 'EN, LC '
                        DadosApoio categoria = dadosApoioService.getByCodigo('TB_CATEGORIA_IUCN', params.categoria)
                        if (!categoria) {
                            throw new Exception('Categoria ' + params.categoria + ' inválida!')
                        }
                        params.sqCategoria = categoria.id.toString()
                    }
                }
                if( params.sqCategoria ) {
                    listTemp=[]
                    params.sqCategoria.split(',').each {
                       DadosApoio categoria = DadosApoio.get( it.toInteger() )
                        if( categoria.codigoSistema == 'AMEACADA' ) {
                            listTemp += dadosApoioService.getCategoriasAmeacadas()?.id
                        }
                        else {
                            listTemp.push(categoria.id.toInteger())
                        }
                    }
                    if( listTemp ) {
                        whereLevel0.push('vw.sq_categoria_final in ( ' + listTemp.join(',') + ' )')
                    }
                }

                // filtro pelos Estados
                if( params.sqEstado ) {
                    listTemp = []
                    params.sqEstado.split(',').each {
                        if ( params.endemicaEstado == 'S') {
                            listTemp.push("vw.sq_estado = '|"+it.toString()+"|'")
                        }
                        else {
                            listTemp.push("vw.sq_estado like '%|"+it.toString()+"|%'")
                        }
                    }
                    whereLevel0.push( '('+ listTemp.join(' or ')+')' )
                }

                // filtro pelos biomas
                if( params.sqBioma ) {
                    listTemp = []
                    params.sqBioma.split(',').each {
                        if ( params.endemicaBioma == 'S') {
                            listTemp.push("vw.sq_bioma = '|"+it.toString()+"|'")
                        }
                        else {
                            listTemp.push("vw.sq_bioma like '%|"+it.toString()+"|%'")
                        }
                    }
                    whereLevel0.push( '('+ listTemp.join(' or ')+')' )
                }

                // filtro pelo codigo do grupo grupo
                if( params.grupo ) {
                    params.sqGrupo = ''
                    listTemp=[]
                    dadosApoioService.getTable('TB_GRUPO_SALVE').each {
                        if( it.codigo.toUpperCase() == params.grupo.toUpperCase() )  {
                            listTemp.push( it.id )
                        }
                    }
                    params.sqGrupo = listTemp.join(',');
                }
                if( params.sqGrupo ) {
                    listTemp = []
                    params.sqGrupo.split(',').each {
                        listTemp.push("vw.sq_grupo_salve = "+it.toString() )
                    }
                    whereLevel0.push( '('+ listTemp.join(' or ')+')' )
                }

                // filtro pela Ameaça
                if( params.sqAmeaca ) {
                    listTemp = []
                    params.sqAmeaca.split(',').each {
                        listTemp.push("vw.sq_ameaca like '%|"+it.toString()+"|%'")
                    }
                    whereLevel0.push( '('+ listTemp.join(' or ')+')' )
                }

                // filtro pelo uso
                if( params.sqUso ) {
                    listTemp = []
                    params.sqUso.split(',').each {
                        listTemp.push("vw.sq_uso like '%|"+it.toString()+"|%'")
                    }
                    whereLevel0.push( '('+ listTemp.join(' or ')+')' )
                }

                // filtro pelas bacias hidrográficas
                if( params.sqBacia ) {
                    listTemp = []
                    params.sqBacia.split(',').each {
                        listTemp.push("vw.sq_bacia like '%|"+it.toString()+"|%'")
                    }
                    whereLevel0.push( '('+ listTemp.join(' or ')+')' )

                }

                // filtro pelos PANs
                if( params.sqPan ) {
                    listTemp = []
                    params.sqPan.split(',').each {
                        listTemp.push("vw.sq_plano_acao like '%|"+it.toString()+"|%'")
                    }
                    whereLevel0.push( '('+ listTemp.join(' or ')+')' )
                }

                if( params.sqRegiao ) {
                    listTemp = []
                    params.sqRegiao.split(',').each {
                        if ( params.endemicaRegiao == 'S') {
                            listTemp.push("vw.sq_regiao = '|"+it.toString()+"|'")
                        }
                        else {
                            listTemp.push("vw.sq_regiao like '%|"+it.toString()+"|%'")
                        }
                    }
                    whereLevel0.push( '('+ listTemp.join(' or ')+')' )
                }

                if( params.sqUc ) {
                    // filtrar Unidade Conservação considerando a UC da especie e subespecies
                    // sendo a UC da especie no mesmo ciclo e as UCs das subepecies em qualquer ciclo
                    String colunaUc = 'vw.sq_uc_federal';
                    if( params.tipoUc =='estadual') {
                        colunaUc = 'vw.sq_uc_estadual'
                    } else if( params.tipoUc =='rppn') {
                        colunaUc = 'vw.sq_rppn'
                    }
                    listTemp = []
                    params.sqUc.split(',').each {
                        listTemp.push(colunaUc + " like '%|"+it.toString()+"|%'")
                    }
                    whereLevel0.push( '('+ listTemp.join(' or ')+')' )
                }

                cmd = """SELECT vw.sq_ficha
                    ,vw.sq_ficha_publicada
                    ,vw.nm_cientifico
                    ,vw.no_comum
                    ,vw.ds_grupo_salve
                    ,vw.sg_estado
                    ,case when vw.sq_categoria_final is not null then concat( vw.ds_categoria_final,' (',vw.cd_categoria_final,')') else '' end AS de_categoria_final_completa
                    ,vw.cd_categoria_final as cd_categoria_final
                    ,vw.ds_criterio_aval_iucn_final
                    ,vw.no_bioma
                    ,vw.de_periodo_avaliacao
                    ,vw.dt_fim_avaliacao
                    ,vw.cd_situacao_ficha
                    ,vw.nm_cientifico_anterior as nm_cientifico_atual
                    FROM salve.mv_dados_modulo_publico vw
                     -- filtros selecionados
                        ${ whereLevel0 ? 'WHERE vw.sq_categoria_final is not null AND ' + whereLevel0.join('\n\tAND ') : '' }
                    """

            // aplicar order by na query pela coluna selecionada
            if( params.sortColumn ) {
                orderBy  = "\norder by ${params.sortColumn}"
                if( params.sortOrder ) {
                    orderBy += " ${params.sortOrder}" // DESC OU ASC
                }
            }

            // complementar a query de acordo com o formato de saida: zip, table ...
            /**
             * pesquisa table normal para exibir na tela paginando
             */
            if (params.outputFormat == 'table') {
                if (!params.records || params.records.toInteger() < 1) {
                    //println 'Calculando total de registros...'
                    String cmdTotal = """SELECT COUNT(x.sq_ficha ) as total_records from (\n${cmd}\n) as x"""
                    //List listTotalRecords = sqlService.execSql(cmdTotal, pars)
                    List listTotalRecords = _getDataFromCache( cmdTotal.replaceAll(/(?i)order by/,"-- order by"), pars )
                    pagination.records = listTotalRecords[0].total_records
                }

                // calcular offset da paginacao
                pagination.offSet = (int) ((pagination.page - 1) * pagination.pageSize)
                pagination.pages = Math.max(1, (int) Math.ceil(pagination.records / pagination.pageSize))
                resultado.pagination = pagination

                // aplicar a paginação na query
                cmd += """${ orderBy?:''}\noffset ${pagination.offSet} limit ${pagination.pageSize}"""
            }
            /**
             * exportar os registros de ocorrências de todas as fichas pesquisadas
             */
            else if (params.outputFormat == 'ocorrencias') {
                // ler somente os ids das fichas
                cmd += orderBy
                String cmdTotal = """SELECT x.sq_ficha from ( ${cmd} ) as x"""
                List idsFichas = sqlService.execSql( cmdTotal, pars )?.sq_ficha
                if ( idsFichas ) {
                    resultado.data = ['format': params.outputFormat, 'fileName': _exportPlanilhaOcorrencias( idsFichas )]
                    params.fileName=resultado.data.fileName
                    _registrarTrilhaAuditoria('exportar_ocorrencia','salve-publico',params)
                    if (params.email) {
                        if (!_sendEmail(params.email.toLowerCase()
                                , 'Exportação Salve'
                                , ''
                                , resultado.data.fileName)) {
                            resultado.msg = 'Erro no envio do email para ' + params.email
                        } else {
                            String linkDownload = '<a title="Baixar o arquivo." href="javascript:void(0);" onClick="doOpenWindow(\''+resultado.data.fileName+'\')"><i class="fa fa-download"></i></a>'
                            resultado.msg = 'Exportação finalizada.&nbsp;' + linkDownload
                            resultado.text = 'Arquivo foi enviado para o email <b>' + params.email+'</b>'
                        }
                        resultado.data.fileName = '';
                    }
                }
                render resultado as JSON
                return
            }
            /**
             * exportar zip com o pdf de todas as fichas pesquisadas
             */
            else if (params.outputFormat == 'zip') {
                // ler somente os ids das fichas
                cmd += orderBy
                cmd  = """SELECT x.sq_ficha from ( ${cmd} ) as x"""
                List registros = _getDataFromCache( cmd, pars)
                if ( registros ) {
                    // criar array somente com os ids retornados na coluna sq_ficha
                    List idsFichas = registros.sq_ficha
                    resultado.data = ['format': params.outputFormat, 'fileName': _exportFichasZip( idsFichas )]
                    if (params.email) {
                        if (!_sendEmail(params.email.toLowerCase()
                                , 'Exportação Salve'
                                , ''
                                , resultado.data.fileName)) {
                            resultado.msg = 'Erro no envio do email para ' + params.email
                        } else {
                            String linkDownload = '<a title="Baixar o arquivo." href="javascript:void(0);" onClick="doOpenWindow(\''+resultado.data.fileName+'\')"><i class="fa fa-download"></i></a>'
                            resultado.msg  = 'Exportação finalizada.&nbsp;'+linkDownload
                            resultado.text = 'Arquivo foi enviado para o email <b>' + params.email+'</b>'
                        }
                        resultado.data.fileName = '';
                    }
                } else {
                    resultado.status = 1
                    resultado.msg    = 'Ficha(s) não publicada(s).'
                    resultado.text   = 'Somente os PDFs das fichas PUBLICADAS podem ser baixados.'
                }
                render resultado as JSON
                return
            }

            // get from cache - https://code-maven.com/groovy-json
            /*String cacheFileName = '/data/salve-estadual/temp/json.txt'
            def jsonSlurper = new JsonSlurper()
            render jsonSlurper.parse(new File( cacheFileName ))
            //render data
            return;*/
/** /
//zzz
println ' '
println cmd
println params

/**/


            // ler os dados do cache
            resultado.data = _getDataFromCache( cmd, pars )

            // executar a query sql
            // resultado.data = sqlService.execSql(cmd, pars)

            /*def json_str = JsonOutput.toJson( resultado.data )
            def json_beauty = JsonOutput.prettyPrint( json_str )
            File file = new File('/data/salve-estadual/temp/json.txt')
            file.write(json_beauty)
            println 'Cache salve em /data/salve-estadual/temp/json.txt';*/


            // fazer o tratamento para algumas colunas
            resultado.data.each {
                it.sq_ficha                     = encryptStr(it.sq_ficha.toString())
                it.nm_cientifico                = Util.ncItalico( it.nm_cientifico )
                it.nm_cientifico_sem_autor      = Util.ncItalico( it.nm_cientifico,true,false )
                it.de_criterio_final_completo   = it.ds_criterio_aval_iucn_final ? it.cd_categoria_final+' '+it.ds_criterio_aval_iucn_final :''
                //it.de_periodo_avaliacao = (it.dt_inicio_avaliacao ? it.dt_inicio_avaliacao+' a '+it.dt_fim_avaliacao : '')
                //it.de_categoria_final_completa = ( it.de_categoria_final ? (it.de_categoria_final + ' ('+it.cd_categoria_final + ')') : '')
            }
            if( resultado.data.size() == 0 ) {
                resultado.msg='Nenhum registro encontrado.';
            }


            // resultado no formato de planilha
            /**
             * Exportar para planilha o resultado da pesquisa sem paginação
             */
            if( params.outputFormat == 'xlsx') {
                d(' Exportação para planilha');
                resultado.data = ['format':params.outputFormat,'fileName': _exportPlanilhaFichas( resultado.data, criterias )]
                if( params.email ) {
                    if( ! _sendEmail( params.email.toLowerCase()
                            , 'Exportação Salve'
                            , ''
                            , resultado.data.fileName ) ) {
                        resultado.msg  = 'Erro no envio do email para ' + params.email
                        resultado.type = 'error'
                    } else {
                        String linkDownload = '<a title="Baixar o arquivo." href="javascript:void(0);" onClick="doOpenWindow(\''+resultado.data.fileName+'\')"><i class="fa fa-download"></i></a>'
                        resultado.msg = 'Exportação finalizada.&nbsp;'+linkDownload
                        resultado.text = 'Arquivo foi enviado para o email <b>' + params.email+'</b>'
                        resultado.type = 'success'
                    }
                    resultado.data.fileName='';
                }
            }

        } catch( Exception e) {
            println params
            resultado.status=1
            resultado.msg='Ocorreu um erro.'
            resultado.text = e.getMessage()
            resultado.type='error'
            resultado.data=[]
            //e.printStackTrace();
        }
        render resultado as JSON
    }

    def searchOld() {
        //this.debug = (session && session.getAttribute('debug') == 'S' ? true : false)
        debug=false;
        d( ' ')
        d('SEARCH PARAMS')
        d( params.toString(  ) )

        Taxon taxon = null
        NivelTaxonomico nivelEspecie = NivelTaxonomico.findByCoNivelTaxonomico('ESPECIE')
        NivelTaxonomico nivelSubespecie = NivelTaxonomico.findByCoNivelTaxonomico('SUBESPECIE')
        List listIds = []
        JSONArray criterias = []
        if( params.criterias && params.criterias.size() > 0 ) {
            criterias = (JSONArray) JSON.parse(params.criterias)
            d(' - Filtros:');
            criterias.each { it ->
                d('   - ' + it.title + ' = ' + it.value);
            }
        }
        String cmd = ''
        Map pars = [:]
        Map pagination = [records: 0, pageSize: 100, page: 1, offSet: 0, pages: 0]
        Map resultado = ['status': 0, msg: '', type: 'success', pagination: pagination, outputFormat: 'table', data: []]

        // validar recaptcha
        if( params.reCaptchaToken ) {
            String remoteIp = request.getRemoteAddr()
            if( ! remoteIp ) {
                remoteIp = request.getRemoteHost();
            }
            String captchaValidateUrl = "https://www.google.com/recaptcha/api/siteverify?secret=${captchaV3SecretKey}&response=${params.reCaptchaToken}&remoteip=${remoteIp}"
            //d('   - reCaptchaToken:' + params.reCaptchaToken );
            //d('   - remoteIp: ' + remoteIp )
            //d('   - url verify: ' + captchaValidateUrl)
            JSONElement dados = requestService.doGetJson( captchaValidateUrl )

            if( ! dados || ! dados.success || ! dados.score || dados.score < 0.7 ) {
                resultado.status = 1
                resultado.text  = 'Consulta negada pelo recaptcha.'
                resultado.msg = 'Você é um robô.'
                resultado.type ='info'
                resultado.data = []
                render resultado as JSON
                return;
            }
        } else {
            resultado.status = 1
            resultado.text  = 'Consulta negada pela falta do token de validação.'
            resultado.msg = 'Erro reCaptcha'
            resultado.type ='error'
            resultado.data = []
            render resultado as JSON
            return;
        }
        if (params.records && params.records.toInteger() > 0) {
            pagination.records = params.records.toInteger()
        }

        if (params.pageSize && params.pageSize.toInteger() > 0) {
            pagination.pageSize = params.pageSize.toInteger()
        }

        if (params.page && params.page.toInteger() > 0) {
            pagination.page = params.page.toInteger()
        }
        if (params.outputFormat) {
            resultado.outputFormat = params.outputFormat
        }

        /** /
         println ' '
         println 'Search modulo publico: Parametros'
         println params
         println ' '
         /**/

        // exportar todos os registros se all for true
        params.all = params.boolean('all',false)
        if( params.all ) {
            listIds=[]
        } else {
            if( params.selectedIds ) {
                listIds = params.selectedIds.split(',').collect { decryptStr(it) }
            }
        }
        /**
         * Exportar as ocorrencias da(s) ficha(s) selecionada(s) no gride
         */
        if (params.outputFormat == 'ocorrencias') {
            if ( listIds.size() > 0 ) {
                try {
                    resultado.data = ['format': params.outputFormat, 'fileName': _exportPlanilhaOcorrencias(listIds, criterias)]
                    // gravar log
                    params.fileName=resultado.data.fileName
                    _registrarTrilhaAuditoria('exportar_ocorrencia','salve-publico',params)
                } catch(Exception e ){
                    params.email=null
                    resultado.msg = e.getMessage()
                    resultado.status=1
                    resultado.type='error'
                }
                if (params.email) {
                    if (!_sendEmail(params.email.toLowerCase()
                        , 'Exportação Salve'
                        , ''
                        , resultado.data.fileName)) {
                        resultado.msg = 'Erro no envio do email para ' + params.email
                        resultado.status = 1
                    } else {
                        String linkDownload = '<a title="Baixar o arquivo." href="javascript:void(0);" onClick="doOpenWindow(\'' + resultado.data.fileName + '\')"><i class="fa fa-download"></i></a>'
                        resultado.msg = 'Exportação finalizada.&nbsp;' + linkDownload
                        resultado.text = 'Arquivo foi enviado para o email ' + params.email
                    }
                    resultado.data.fileName = '';
                }
                render resultado as JSON
                return;
            }
        }
        //CicloAvaliacao cicloAvaliacao = CicloAvaliacao.get(3)
        DadosApoio situacaoPublicada = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'PUBLICADA')
        DadosApoio contextoEstado = dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA', 'ESTADO')
        DadosApoio contextoOcorrencia = dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA', 'REGISTRO_OCORRENCIA')
        //DadosApoio contextoConsulta = dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA', 'CONSULTA')
        DadosApoio contextoBioma = dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA', 'BIOMA')
        DadosApoio contextoBacia = dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA', 'BACIA_HIDROGRAFICA')

        try {
            if (params.sqTaxon) {
                taxon = Taxon.get(params.sqTaxon.toLong())
                if (!taxon) {
                    throw new Exception('Taxon inválido.')
                }
            }

            // necessário informar algum texto para pesquisa quando não for pesquisa avançada
            params.q = params.q ? params.q.trim() : ''
            if ((!params.q || params.q == '')
                && !taxon
                && !params.sqCategoria
                && !params.sqEstado
                && !params.sqRegiao
                && !params.sqBioma
                && !params.grupo // filtro graficos
                && !params.categoria
                && !params.sqGrupo
                && !params.sqUc
                && !params.sqAmeaca
                && !params.sqUso
                && !params.inPublicadas
                && !params.sqBacia

            ) {
                resultado.status=0
                resultado.msg  = 'Nenhum parâmetro informado.'
                resultado.text = 'Para realizar a pesquisa é preciso informar algum parâmetro.'
                resultado.type ='info'
                resultado.data = []
                render resultado as JSON
                return;
            }

            // aceitar somente se tiver no mínico 3 caracteres
            if (params.q && params.q.size() < 3) {
                resultado.status=0
                resultado.msg  = 'Mínimo 3 caracteres.'
                resultado.text = 'Para realizar a pesquisa é preciso informar pelo menos 3 caracteres.'
                resultado.type ='info'
                resultado.data = []
                render resultado as JSON
                return;
            }

            // criar novo comando SQL
            if( true ) {
                List whereLevel0=[]
                List whereLevel2=[]

                if( params.q ) {
                    //whereLevel0.push("concat( level0.nm_cientifico,' ', ficha_nome_comum.no_comum)::text ilike :q")
                    //pars.q = '%' + params.q + '%'
                    params.q = Util.removeAccents(params.q)
                    String regex = "'^${params.q}[ -]|[ -]${params.q}\$|[ -]${params.q}[ -]|^${params.q}\$'"
                    whereLevel0.push("public.fn_remove_acentuacao( concat( level0.nm_cientifico,' ',ficha_sinonimia.no_sinonimia,' ', ficha_nome_comum.no_comum) )::text ~* ${regex}")
                }

                // filtrar somente as fichas publicadas ( Exportarcao ficha pdf para zip )
                if ( params.outputFormat == 'zip' || params?.inPublicadas == 'S' ) {
                    whereLevel2.push("ficha_publicada.sq_ficha_publicada is not null")
                }

                // filtro pelo codigo do grupo grupo
                if( params.grupo ) {
                    params.sqGrupo = ''
                    List gruposValidos=[]
                    dadosApoioService.getTable('TB_GRUPO_SALVE').each {
                        /*if( params.grupo == 'PEIXES') {
                            if( it.codigo == 'PEIXES_AGUA_DOCE' || it.codigo == 'PEIXES_MARINHOS' )  {
                                gruposValidos.push( it.id )
                            }
                        } else {
                            if( it.codigo == params.grupo )  {
                                gruposValidos.push( it.id )
                            }

                        }*/
                        if( it.codigo.toUpperCase() == params.grupo.toUpperCase() )  {
                            gruposValidos.push( it.id )
                        }
                    }
                    params.sqGrupo = gruposValidos.join(',');
                }

                // filtro pelo codigo da categoria
                if( params.categoria ) {
                    if ( params.categoria.toUpperCase() == 'AMEACADAS' ) {
                        params.sqCategoria = dadosApoioService.getCategoriasAmeacadas()?.id?.join(',');
                    } else {
                        DadosApoio categoria = dadosApoioService.getByCodigo('TB_CATEGORIA_IUCN', params.categoria)
                        if (!categoria) {
                            throw new Exception('Categoria ' + params.categoria + ' inválida!')
                        }
                        params.sqCategoria = categoria.id
                    }
                }

                if( params.sqEstado ) {
                    // filtro não endemicas no estado considerando o Estado da especie e subespecies
                    // sendo o Estado da especie no mesmo ciclo e os Estados das subepecies em qualquer ciclo
                    whereLevel0.push("""EXISTS ( -- filtro ESTADO(S)
                        SELECT null from salve.ficha_ocorrencia as fo
                        INNER JOIN salve.ficha on ficha.sq_ficha = fo.sq_ficha
                        INNER JOIN salve.ciclo_avaliacao as ciclo on ciclo.sq_ciclo_avaliacao = ficha.sq_ciclo_avaliacao
                        INNER JOIN taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                        INNER JOIN salve.dados_apoio categoria on categoria.sq_dados_apoio = ficha.sq_categoria_final
                        WHERE ( ( taxon.sq_taxon = level0.sq_taxon AND ficha.sq_ciclo_avaliacao = level0.sq_ciclo_avaliacao )
                            OR taxon.sq_taxon_pai = level0.sq_taxon )
                        AND ficha.sq_categoria_final is not null and categoria.cd_sistema <> 'NE'
                        and ciclo.in_publico = 'S'
                        --AND ficha.sq_ciclo_avaliacao = level0.sq_ciclo_avaliacao
                        --AND ficha.sq_situacao_ficha = ${situacaoPublicada.id.toString() }
                        AND fo.sq_contexto = ${contextoEstado.id.toString() }
                        AND fo.sq_estado in ( ${params.sqEstado} )
                        LIMIT 1)""")
                    if( params.endemicaEstado == 'S') {
                        whereLevel2.push( "level2.sg_estado in ( select x.sg_estado from salve.vw_estado as x where x.sq_estado in ( ${params.sqEstado} ) )")
                    }
                }

                if( params.sqBioma ) {
                    // filtro não endemicas no BIOMA considerando o bioma da especie e subespecies
                    // sendo o Bioma da especie no mesmo ciclo e os Biomas das subepecies em qualquer ciclo
                    whereLevel0.push("""EXISTS ( -- filtro BIOMA(S)
                        SELECT NULL FROM salve.ficha_ocorrencia AS fo
                        INNER JOIN salve.ficha ON ficha.sq_ficha = fo.sq_ficha
                        INNER JOIN salve.ciclo_avaliacao as ciclo on ciclo.sq_ciclo_avaliacao = ficha.sq_ciclo_avaliacao
                        INNER JOIN taxonomia.taxon ON taxon.sq_taxon = ficha.sq_taxon
                        INNER JOIN salve.dados_apoio categoria on categoria.sq_dados_apoio = ficha.sq_categoria_final
                        WHERE ( ( taxon.sq_taxon = level0.sq_taxon and ficha.sq_ciclo_avaliacao = level0.sq_ciclo_avaliacao)
                            OR taxon.sq_taxon_pai = level0.sq_taxon )
                        --AND ficha.sq_situacao_ficha = ${situacaoPublicada.id.toString() }
                        AND ficha.sq_categoria_final is not null and categoria.cd_sistema <> 'NE'
                        AND ciclo.in_publico = 'S'
                        -- AND ficha.sq_ciclo_avaliacao = level0.sq_ciclo_avaliacao
                        AND fo.sq_contexto  = ${contextoBioma.id.toString() }
                        AND fo.sq_bioma IN ( ${params.sqBioma } )
                        LIMIT 1)""")
                    if ( params.endemicaBioma == 'S' ) {
                        whereLevel2.push( "level2.no_bioma in ( select x.ds_dados_apoio from salve.dados_apoio as x where x.sq_dados_apoio in ( ${params.sqBioma} ) )")
                    }
                }

                if( params.sqBacia ) {
                    // filtro não endemicas na BACIA considerando a bacia da especie e subespecies
                    // sendo o Bacia da especie no mesmo ciclo e as Bacias das subepecies em qualquer ciclo
                    whereLevel0.push("""EXISTS ( -- filtro BACIAS(S)
                        SELECT NULL FROM salve.ficha_ocorrencia AS fo
                        INNER JOIN salve.ficha ON ficha.sq_ficha = fo.sq_ficha
                        INNER JOIN salve.ciclo_avaliacao as ciclo on ciclo.sq_ciclo_avaliacao = ficha.sq_ciclo_avaliacao
                        INNER JOIN taxonomia.taxon ON taxon.sq_taxon = ficha.sq_taxon
                        INNER JOIN salve.dados_apoio categoria on categoria.sq_dados_apoio = ficha.sq_categoria_final
                        WHERE ( ( taxon.sq_taxon = level0.sq_taxon and ficha.sq_ciclo_avaliacao = level0.sq_ciclo_avaliacao)
                            OR taxon.sq_taxon_pai = level0.sq_taxon )
                        --AND ficha.sq_situacao_ficha = ${situacaoPublicada.id.toString() }
                        AND ficha.sq_categoria_final is not null and categoria.cd_sistema <> 'NE'
                        AND ciclo.in_publico = 'S'
                        -- AND ficha.sq_ciclo_avaliacao = level0.sq_ciclo_avaliacao
                        AND fo.sq_contexto  = ${contextoBacia.id.toString() }
                        AND fo.sq_bacia_hidrografica IN ( ${params.sqBacia } )
                        LIMIT 1)""")
                    if ( params.endemicaBacia == 'S' ) {
                        whereLevel2.push( "level2.no_bacia in ( select x.ds_dados_apoio from salve.dados_apoio as x where x.sq_dados_apoio in ( ${params.sqBacia} ) )")
                    }
                    // fim bacia
                }

                if( params.sqRegiao ) {
                    // filtrar não endemicas na regiao considerando as Regiões da especie e subespecies
                    // sendo as Regiões da especie no mesmo ciclo e as Regiões  das subepecies em qualquer ciclo
                    whereLevel0.push("""EXISTS ( -- filtro REGIAO(OES)
                         SELECT NULL FROM salve.ficha_ocorrencia AS fo
                         INNER JOIN salve.ficha ON ficha.sq_ficha = fo.sq_ficha
                         INNER JOIN salve.ciclo_avaliacao as ciclo on ciclo.sq_ciclo_avaliacao = ficha.sq_ciclo_avaliacao
                         INNER JOIN taxonomia.taxon ON taxon.sq_taxon = ficha.sq_taxon
                         INNER JOIN salve.vw_estado estado ON estado.sq_estado = fo.sq_estado
                         INNER JOIN salve.dados_apoio categoria on categoria.sq_dados_apoio = ficha.sq_categoria_final
                         WHERE ( ( taxon.sq_taxon = level0.sq_taxon AND ficha.sq_ciclo_avaliacao = level0.sq_ciclo_avaliacao )
                             OR taxon.sq_taxon_pai = level0.sq_taxon )
                         --AND ficha.sq_situacao_ficha = ${situacaoPublicada.id.toString() }
                         AND ficha.sq_categoria_final is not null and categoria.cd_sistema <> 'NE'
                         AND ciclo.in_publico = 'S'
                          -- AND ficha.sq_ciclo_avaliacao = level0.sq_ciclo_avaliacao
                         AND fo.sq_contexto  = ${contextoEstado.id.toString() }
                         AND estado.sq_regiao in ( ${params.sqRegiao} )
                         LIMIT 1 )""")
                    if (params.endemicaRegiao == 'S') {
                        whereLevel2.push( "( SELECT COUNT( DISTINCT X.SQ_REGIAO) FROM SALVE.VW_ESTADO X WHERE X.SG_ESTADO = ANY( string_to_array( replace( level2.sg_estado,' ',''),',') ) ) = 1")
                    }
                }

                if( params.sqUc ) {
                    // filtrar Unidade Conservação considerando a UC da especie e subespecies
                    // sendo a UC da especie no mesmo ciclo e as UCs das subepecies em qualquer ciclo
                    String colunaUc = 'fo.sq_uc_federal';
                    if( params.sqTipoUc =='estadual') {
                        colunaUc = 'fo.sq_uc_estadual'
                    } else if( params.sqTipoUc =='rppn') {
                        colunaUc = 'fo.sq_rppn'
                    }
                    whereLevel0.push("""EXISTS ( -- filtro UC(S)
                        SELECT null from salve.ficha_ocorrencia as fo
                        INNER JOIN salve.ficha on ficha.sq_ficha = fo.sq_ficha
                        INNER JOIN salve.ciclo_avaliacao as ciclo on ciclo.sq_ciclo_avaliacao = ficha.sq_ciclo_avaliacao
                        INNER JOIN taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                        INNER JOIN salve.dados_apoio categoria on categoria.sq_dados_apoio = ficha.sq_categoria_final
                        WHERE ( ( taxon.sq_taxon = level0.sq_taxon AND ficha.sq_ciclo_avaliacao = level0.sq_ciclo_avaliacao )
                            OR taxon.sq_taxon_pai = level0.sq_taxon )
                        AND ficha.sq_categoria_final is not null and categoria.cd_sistema <> 'NE'
                        AND ciclo.in_publico = 'S'
                        --AND ficha.sq_ciclo_avaliacao = level0.sq_ciclo_avaliacao
                        --AND ficha.sq_situacao_ficha = ${situacaoPublicada.id.toString() }
                        AND fo.sq_contexto = ${contextoOcorrencia.id.toString() }
                        AND ${colunaUc} in ( ${params.sqUc} )
                        LIMIT 1)""")
                }

                // filtro pela Ameaça
                if( params.sqAmeaca ) {
                    // filtrar Ameaças considerando a Ameaça da especie e subespecies
                    // sendo a Ameaça da especie no mesmo ciclo e as Ameaças das subepecies em qualquer ciclo
                    String listArvore = _getSubNiveisDadosApoio( params.sqAmeaca )
                    whereLevel0.push("""EXISTS ( -- filtro AMEAÇA(S)
                        SELECT null from salve.ficha_ameaca fa
                        INNER JOIN salve.ficha on ficha.sq_ficha = fa.sq_ficha
                        INNER JOIN salve.ciclo_avaliacao as ciclo on ciclo.sq_ciclo_avaliacao = ficha.sq_ciclo_avaliacao
                        INNER JOIN taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                        INNER JOIN salve.dados_apoio categoria on categoria.sq_dados_apoio = ficha.sq_categoria_final
                        WHERE ( ( taxon.sq_taxon = level0.sq_taxon AND ficha.sq_ciclo_avaliacao = level0.sq_ciclo_avaliacao )
                            OR taxon.sq_taxon_pai = level0.sq_taxon )
                        AND ficha.sq_categoria_final is not null and categoria.cd_sistema <> 'NE'
                        AND ciclo.in_publico = 'S'
                        --AND ficha.sq_ciclo_avaliacao = level0.sq_ciclo_avaliacao
                        --AND ficha.sq_situacao_ficha = ${situacaoPublicada.id.toString() }
                        AND fa.sq_criterio_ameaca_iucn in ( ${ listArvore } )
                        LIMIT 1)""")
                }

                // filtro pelo Uso
                if( params.sqUso ) {
                    // filtrar USOS considerando a USO da especie e subespecies
                    // sendo a USO da especie no mesmo ciclo e as USOS das subepecies em qualquer ciclo
                    // ler os ids dos usos PAI da USOS selecionadas
                    String listArvore = _getSubNiveisUso( params.sqUso )

                    whereLevel0.push("""EXISTS ( -- filtro USO(S)
                        SELECT null from salve.ficha_uso fu
                        INNER JOIN salve.ficha on ficha.sq_ficha = fu.sq_ficha
                        INNER JOIN salve.ciclo_avaliacao as ciclo on ciclo.sq_ciclo_avaliacao = ficha.sq_ciclo_avaliacao
                        INNER JOIN taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                        INNER JOIN salve.dados_apoio categoria on categoria.sq_dados_apoio = ficha.sq_categoria_final
                        WHERE ( ( taxon.sq_taxon = level0.sq_taxon AND ficha.sq_ciclo_avaliacao = level0.sq_ciclo_avaliacao )
                            OR taxon.sq_taxon_pai = level0.sq_taxon )
                        AND ficha.sq_categoria_final is not null and categoria.cd_sistema <> 'NE'
                        AND ciclo.in_publico = 'S'
                        --AND ficha.sq_ciclo_avaliacao = level0.sq_ciclo_avaliacao
                        --AND ficha.sq_situacao_ficha = ${situacaoPublicada.id.toString() }
                        AND fu.sq_uso in ( ${ listArvore } )
                        LIMIT 1)""")
                }

                cmd = """SELECT level2.sq_ficha
            ,ficha_publicada.sq_ficha_publicada
            ,level2.nm_cientifico
            ,level2.no_comum
            ,level2.sg_estado
            ,case when categoria_final.ds_dados_apoio is not null then concat(categoria_final.ds_dados_apoio,' (',categoria_final.cd_dados_apoio,')') else '' end AS de_categoria_final_completa
            ,categoria_final.cd_dados_apoio as cd_categoria_final
            ,ficha.ds_criterio_aval_iucn_final
            ,level2.no_bioma
            ,avaliacao.de_periodo_avaliacao
            ,avaliacao.dt_fim_avaliacao
            ,situacao_ficha.cd_sistema as cd_situacao_ficha
            ,ficha_atual.nm_cientifico_atual
            FROM ( SELECT level1.sq_ficha, level1.sq_taxon, level1.nm_cientifico
                -- nomes comuns
                ,array_to_string( array_agg( distinct nome_comum.no_comum ),', ') as no_comum
                -- estados
                ,array_to_string( array_agg( distinct estado.sg_estado ),', ') as sg_estado
                -- biomas
                ,array_to_string( array_agg( distinct bioma.no_bioma ),', ') as no_bioma
                -- bacias
                ,array_to_string( array_agg( distinct bacia.no_bacia ),', ') as no_bacia
                FROM ( SELECT level0.sq_ficha, level0.sq_taxon, level0.nm_cientifico, level0.sq_ciclo_avaliacao
                    FROM (

                        SELECT ficha.sq_ficha, ficha.sq_taxon, ficha.nm_cientifico, ficha.sq_ciclo_avaliacao
                        FROM salve.vw_ficha_modulo_publico as ficha
                        INNER JOIN taxonomia.taxon ON taxon.sq_taxon = ficha.sq_taxon
                         -- AND taxon.sq_nivel_taxonomico = ${nivelEspecie.id.toString()}

                        -- filtrar fichas publicadas aqui
                        -- WHERE ficha.sq_situacao_ficha = ${situacaoPublicada.id.toString() }
                        -- filtrar as fichas avaliadas aqui
                           WHERE ficha.sq_grupo_salve is not null
                        -- filtrar nivel taxonomico aqui
                        ${ taxon ? "AND (taxon.json_trilha->'${ taxon.nivelTaxonomico.coNivelTaxonomico.toLowerCase() }'->>'sq_taxon')::integer = ${ taxon.id.toString() }" : '' }
                        -- filtrar grupo aqui
                        ${ params.sqGrupo ? "AND ficha.sq_grupo_salve in( ${ params.sqGrupo } )" : '' }
                        -- filtrar categoria(s) aqui
                        ${ params.sqCategoria ? "AND ficha.sq_categoria_final in( ${ params.sqCategoria } )" : '' }
                        -- filtrar ids aqui
                        ${ listIds.size() > 0 ? "AND ficha.sq_ficha in ( ${ listIds.join(',') } )" : '' }

                        ) as level0
                        ${params.q ? "LEFT OUTER JOIN salve.ficha_nome_comum on ficha_nome_comum.sq_ficha = level0.sq_ficha" : ''}
                        ${params.q ? "LEFT OUTER JOIN salve.ficha_sinonimia on ficha_sinonimia.sq_ficha = level0.sq_ficha" : ''}

                        -- filtrar nome cientifico/comum e antigos, estado, regiao, bacias e bioma aqui
                        ${ whereLevel0 ? 'WHERE ' + whereLevel0.join('\n\tAND ') : ''}
                ) as level1
                -- mostrar todos os nomes comuns da espécie e subespecies
                -- sendo que para as subespecie pode ser de qualquer ciclo
                LEFT JOIN LATERAL (
                    select distinct fnc.no_comum FROM salve.ficha_nome_comum fnc
                    inner join salve.ficha on ficha.sq_ficha = fnc.sq_ficha
                    inner join salve.ciclo_avaliacao as ciclo on ciclo.sq_ciclo_avaliacao = ficha.sq_ciclo_avaliacao
                    inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                    where ( taxon.sq_taxon = level1.sq_taxon or taxon.sq_taxon_pai = level1.sq_taxon )
                    -- and ficha.sq_categoria_final is not null
                    and ciclo.in_publico = 'S'
                    and ( fnc.de_regiao_lingua ilike 'portu%' or fnc.de_regiao_lingua is null or fnc.de_regiao_lingua = '' )
                ) AS nome_comum ON true

                -- mostrar os estados onde o taxon ocorre
                -- Quando for o mesmo nivel taxonomico, tem que ser o mesmo ciclo da ultima validacao
                -- Quando for nivel abaixo ( subespecie ), considerar qualquer ciclo
                left join lateral ( select distinct estado.sq_regiao, estado.sg_estado from salve.ficha_ocorrencia as fo
                    inner join salve.ficha on ficha.sq_ficha = fo.sq_ficha
                    INNER JOIN salve.ciclo_avaliacao as ciclo on ciclo.sq_ciclo_avaliacao = ficha.sq_ciclo_avaliacao
                    inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                    inner join salve.vw_estado as estado on estado.sq_estado = fo.sq_estado
                    where ( taxon.sq_taxon = level1.sq_taxon or taxon.sq_taxon_pai = level1.sq_taxon )
                    --and ficha.sq_situacao_ficha = ${ situacaoPublicada.id.toString() }
                    --and ficha.sq_categoria_final is not null
                    AND ciclo.in_publico = 'S'
                    --and ficha.sq_ciclo_avaliacao = level1.sq_ciclo_avaliacao
                    and fo.sq_contexto = ${contextoEstado.id.toString() }
                    order by estado.sq_regiao, estado.sg_estado
                ) as estado on true

                -- mostrar os biomas onde o taxon ocorre
                -- Quando for o mesmo nivel taxonomico, tem que ser o mesmo ciclo da ultima validacao
                -- Quando for nivel abaixo ( subespecie ), considerar qualquer ciclo
                left join lateral  (
                    SELECT distinct bioma.ds_dados_apoio as no_bioma FROM salve.ficha_ocorrencia as fo
                    inner join salve.ficha on ficha.sq_ficha = fo.sq_ficha
                    INNER JOIN salve.ciclo_avaliacao as ciclo on ciclo.sq_ciclo_avaliacao = ficha.sq_ciclo_avaliacao
                    inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                    inner join salve.dados_apoio bioma on bioma.sq_dados_apoio = fo.sq_bioma
                    where ( taxon.sq_taxon = level1.sq_taxon or taxon.sq_taxon_pai = level1.sq_taxon )
                    -- and ficha.sq_situacao_ficha = ${situacaoPublicada.id.toString() }
                    -- AND ficha.sq_categoria_final is not null
                    AND ciclo.in_publico = 'S'
                    -- AND ficha.sq_ciclo_avaliacao = level1.sq_ciclo_avaliacao
                    AND fo.sq_contexto  = ${ contextoBioma.id.toString() }
                ) as bioma on true

                -- mostrar as bacias onde o taxon ocorre
                -- Quando for o mesmo nivel taxonomico, tem que ser o mesmo ciclo da ultima validacao
                -- Quando for nivel abaixo ( subespecie ), considerar qualquer ciclo
                left join lateral  (
                    SELECT distinct bacia.ds_dados_apoio as no_bacia FROM salve.ficha_ocorrencia as fo
                    inner join salve.ficha on ficha.sq_ficha = fo.sq_ficha
                    INNER JOIN salve.ciclo_avaliacao as ciclo on ciclo.sq_ciclo_avaliacao = ficha.sq_ciclo_avaliacao
                    inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                    inner join salve.dados_apoio bacia on bacia.sq_dados_apoio = fo.sq_bacia_hidrografica
                    where ( taxon.sq_taxon = level1.sq_taxon or taxon.sq_taxon_pai = level1.sq_taxon )
                    -- and ficha.sq_situacao_ficha = ${situacaoPublicada.id.toString() }
                    -- AND ficha.sq_categoria_final is not null
                    AND ciclo.in_publico = 'S'
                    -- AND ficha.sq_ciclo_avaliacao = level1.sq_ciclo_avaliacao
                    AND fo.sq_contexto  = ${ contextoBacia.id.toString() }
                ) as bacia on true
                group by level1.sq_ficha, level1.sq_taxon, level1.nm_cientifico
            ) as level2
            -- ler dados da categoria atual
            inner join salve.ficha on ficha.sq_ficha = level2.sq_ficha
            left outer join salve.dados_apoio as categoria_final on categoria_final.sq_dados_apoio = ficha.sq_categoria_final
            LEFT OUTER JOIN salve.dados_apoio AS situacao_ficha ON situacao_ficha.sq_dados_apoio = ficha.sq_situacao_ficha

            -- ler a ultima ficha publicada
            LEFT JOIN LATERAL (
                SELECT ficha.sq_ficha as sq_ficha_publicada
                FROM salve.ficha
                WHERE ficha.sq_ficha = (
                    SELECT ultima_publicacao.sq_ficha
                    FROM salve.ficha ultima_publicacao
                    INNER JOIN salve.ciclo_avaliacao as ciclo ON ciclo.sq_ciclo_avaliacao = ultima_publicacao.sq_ciclo_avaliacao
                    WHERE ultima_publicacao.sq_taxon = level2.sq_taxon AND ultima_publicacao.sq_situacao_ficha = ${situacaoPublicada.id.toString()}
                    AND ciclo.in_publico = 'S'
                    ORDER BY ciclo.nu_ano DESC OFFSET 0  LIMIT 1
               )
            ) as ficha_publicada on true


            -- ler a ultima avaliacao, considerar a ultima oficina de avaliacao
            LEFT JOIN LATERAL (
                select to_char( oficina.dt_fim,'MM/yyyy') as de_periodo_avaliacao
                ,oficina.dt_fim as dt_fim_avaliacao
                from salve.oficina_ficha
                inner join salve.oficina on oficina.sq_oficina = oficina_ficha.sq_oficina
                inner join salve.dados_apoio on oficina.sq_tipo_oficina = dados_apoio.sq_dados_apoio
                INNER JOIN salve.ficha ficha on ficha.sq_ficha = oficina_ficha.sq_ficha
                INNER JOIN salve.ciclo_avaliacao as ciclo on ciclo.sq_ciclo_avaliacao = ficha.sq_ciclo_avaliacao
                where oficina_ficha.sq_ficha = level2.sq_ficha
                and dados_apoio.cd_sistema = 'OFICINA_AVALIACAO'
                AND ciclo.in_publico = 'S'
                order by oficina.dt_fim desc limit 1
                ) avaliacao ON true
                -- verificar se a ficha mudou de taxon ao ir para o proximo ciclo
                left join lateral (
                    select x.nm_cientifico as nm_cientifico_atual from
                        salve.ficha x
                        where x.sq_ficha_ciclo_anterior = ficha.sq_ficha
                        and x.sq_taxon <> ficha.sq_taxon
                    limit 1
                ) as ficha_atual on true
                -- filtrar aqui: Endemicas Estado, Bioma. Bacia, Região, fichas publicadas etc
                ${ whereLevel2 ? 'WHERE '+ whereLevel2.join('\nAND '):''}
            """
            }
            // fim criacao novo comando sql

            // aplicar order by na query pela coluna selecionada
            if( params.sortColumn ) {
                cmd += "\norder by ${params.sortColumn}"
                if( params.sortOrder ) {
                    cmd += " ${params.sortOrder}"
                }
            }

            // complementar a query de acordo com o formato de saida: zip, table ...
            /**
             * pesquisa table normal para exibir na tela paginando
             */
            if (params.outputFormat == 'table') {
                if (!params.records || params.records.toInteger() < 1) {
                    //println 'Calculando total de registros...'
                    String cmdTotal = """SELECT COUNT(x.sq_ficha ) as total_records from (\n${cmd}\n) as x"""
                    //List listTotalRecords = sqlService.execSql(cmdTotal, pars)
                    List listTotalRecords = _getDataFromCache( cmdTotal.replaceAll(/(?i)order by/,'-- order by'), pars )
                    pagination.records = listTotalRecords[0].total_records
                }

                // calcular offset da paginacao
                pagination.offSet = (int) ((pagination.page - 1) * pagination.pageSize)
                pagination.pages = Math.max(1, (int) Math.ceil(pagination.records / pagination.pageSize))
                resultado.pagination = pagination

                // aplicar a paginação na query
                cmd += """\noffset ${pagination.offSet} limit ${pagination.pageSize}"""
            }
            /**
             * exportar os registros de ocorrências de todas as fichas pesquisadas
             */
            else if (params.outputFormat == 'ocorrencias') {
                // ler somente os ids das fichas
                String cmdTotal = """SELECT x.sq_ficha from ( ${cmd} ) as x"""
                List idsFichas = sqlService.execSql( cmdTotal, pars )?.sq_ficha
                if ( idsFichas ) {
                    resultado.data = ['format': params.outputFormat, 'fileName': _exportPlanilhaOcorrencias( idsFichas )]
                    params.fileName=resultado.data.fileName
                    _registrarTrilhaAuditoria('exportar_ocorrencia','salve-publico',params)
                    if (params.email) {
                        if (!_sendEmail(params.email.toLowerCase()
                            , 'Exportação Salve'
                            , ''
                            , resultado.data.fileName)) {
                            resultado.msg = 'Erro no envio do email para ' + params.email
                        } else {
                            String linkDownload = '<a title="Baixar o arquivo." href="javascript:void(0);" onClick="doOpenWindow(\''+resultado.data.fileName+'\')"><i class="fa fa-download"></i></a>'
                            resultado.msg = 'Exportação finalizada.&nbsp;' + linkDownload
                            resultado.text = 'Arquivo foi enviado para o email <b>' + params.email+'</b>'
                        }
                        resultado.data.fileName = '';
                    }
                }
                render resultado as JSON
                return
            }
            /**
             * exportar zip com o pdf de todas as fichas pesquisadas
             */
            else if (params.outputFormat == 'zip') {
                // ler somente os ids das fichas
                cmd  = """SELECT x.sq_ficha from ( ${cmd} ) as x"""
                List registros = _getDataFromCache( cmd, pars)
                if ( registros ) {
                    // criar array somente com os ids retornados na coluna sq_ficha
                    List idsFichas = registros.sq_ficha
                    resultado.data = ['format': params.outputFormat, 'fileName': _exportFichasZip( idsFichas )]
                    if (params.email) {
                        if (!_sendEmail(params.email.toLowerCase()
                            , 'Exportação Salve'
                            , ''
                            , resultado.data.fileName)) {
                            resultado.msg = 'Erro no envio do email para ' + params.email
                        } else {
                            String linkDownload = '<a title="Baixar o arquivo." href="javascript:void(0);" onClick="doOpenWindow(\''+resultado.data.fileName+'\')"><i class="fa fa-download"></i></a>'
                            resultado.msg  = 'Exportação finalizada.&nbsp;'+linkDownload
                            resultado.text = 'Arquivo foi enviado para o email <b>' + params.email+'</b>'
                        }
                        resultado.data.fileName = '';
                    }
                } else {
                    resultado.status = 1
                    resultado.msg    = 'Ficha(s) não publicada(s).'
                    resultado.text   = 'Somente os PDFs das fichas PUBLICADAS podem ser baixados.'
                }
                render resultado as JSON
                return
            }

            // get from cache - https://code-maven.com/groovy-json
            /*String cacheFileName = '/data/salve-estadual/temp/json.txt'
            def jsonSlurper = new JsonSlurper()
            render jsonSlurper.parse(new File( cacheFileName ))
            //render data
            return;*/
/** /
//zzz
            println ' '
            println cmd
            println pars
/**/
            // ler os dados do cache
            resultado.data = _getDataFromCache( cmd, pars )

            // executar a query sql
            // resultado.data = sqlService.execSql(cmd, pars)

            /*def json_str = JsonOutput.toJson( resultado.data )
            def json_beauty = JsonOutput.prettyPrint( json_str )
            File file = new File('/data/salve-estadual/temp/json.txt')
            file.write(json_beauty)
            println 'Cache salve em /data/salve-estadual/temp/json.txt';*/


            // fazer o tratamento para algumas colunas
            resultado.data.each {
                it.sq_ficha                     = encryptStr(it.sq_ficha.toString())
                it.nm_cientifico                = Util.ncItalico( it.nm_cientifico )
                it.nm_cientifico_sem_autor      = Util.ncItalico( it.nm_cientifico,true,false )
                it.de_criterio_final_completo   = it.ds_criterio_aval_iucn_final ? it.cd_categoria_final+' '+it.ds_criterio_aval_iucn_final :''
                //it.de_periodo_avaliacao = (it.dt_inicio_avaliacao ? it.dt_inicio_avaliacao+' a '+it.dt_fim_avaliacao : '')
                //it.de_categoria_final_completa = ( it.de_categoria_final ? (it.de_categoria_final + ' ('+it.cd_categoria_final + ')') : '')
            }
            if( resultado.data.size() == 0 ) {
                resultado.msg='Nenhum registro encontrado.';
            }


            // resultado no formato de planilha
            /**
             * Exportar para planilha o resultado da pesquisa sem paginação
             */
            if( params.outputFormat == 'xlsx') {
                d(' Exportação para planilha');
                resultado.data = ['format':params.outputFormat,'fileName': _exportPlanilhaFichas( resultado.data, criterias )]
                if( params.email ) {
                    if( ! _sendEmail( params.email.toLowerCase()
                        , 'Exportação Salve'
                        , ''
                        , resultado.data.fileName ) ) {
                        resultado.msg  = 'Erro no envio do email para ' + params.email
                        resultado.type = 'error'
                    } else {
                        String linkDownload = '<a title="Baixar o arquivo." href="javascript:void(0);" onClick="doOpenWindow(\''+resultado.data.fileName+'\')"><i class="fa fa-download"></i></a>'
                        resultado.msg = 'Exportação finalizada.&nbsp;'+linkDownload
                        resultado.text = 'Arquivo foi enviado para o email <b>' + params.email+'</b>'
                        resultado.type = 'success'
                    }
                    resultado.data.fileName='';
                }
            }

        } catch( Exception e) {
            println params
            resultado.status=1
            resultado.msg='Ocorreu um erro.'
            resultado.text = e.getMessage()
            resultado.type='error'
            resultado.data=[]
            //e.printStackTrace();
        }
        render resultado as JSON
    }
    /**
     * metodo para gerar os dados JSON para construção dos gráficos das espécies
     * em categoria de ameaça ( CR, VU, EN )
     *
     * @return  [  anfibios  : ['title' : 'Anfíbios' , data: [ [VU: 0], [EN: 0], [CR: 0] ], total: 0]
     *           , aves      : ['title' : 'Aves'     , data: [ [VU: 0], [EN: 0], [CR: 0] ], total: 0]
     *           , peixes    : ['title' : 'Peixes'   , data: [ [VU: 0], [EN: 0], [CR: 0] ], total: 0]
     *           ...
     *          ]
     */
    def getGraficosEspeciesAmeacadas() {

        Map res = [status:0, msg:'', data:[:]]
        NivelTaxonomico nivelEspecie    = NivelTaxonomico.findByCoNivelTaxonomico('ESPECIE')
        NivelTaxonomico nivelSubespecie = NivelTaxonomico.findByCoNivelTaxonomico('SUBESPECIE')
        try {
                String cmdSql ="""select mv.cd_categoria_final as cd_categoria
                                ,mv.ds_categoria_final as no_categoria
                                ,concat( mv.ds_categoria_final, ' (',mv.cd_categoria_final,')') as no_categoria_completa
                                ,mv.cd_grupo_salve_sistema as cd_grupo
                                ,mv.ds_grupo_salve  as no_grupo
                                from salve.mv_dados_modulo_publico mv
                                where mv.cd_categoria_final_sistema in('CR','EN','VU' )
                                and mv.sq_grupo_salve is not null
                                order by mv.de_categoria_final_ordem
                        """
            Map graficoData = [:]
            sqlService.execSql(cmdSql).eachWithIndex { row, index ->
                String keyGrafico   = row.cd_grupo.toLowerCase()
                String keyCategoria = row.no_categoria_completa
                           // yellow                          orange                          red
                Map colors =['VU':_getPieColor('VU'),'EN':_getPieColor('EN'), 'CR':_getPieColor('CR')]
                String color = colors[row.cd_categoria];
                if( color ) {
                    if (!graficoData[keyGrafico]) {
                        graficoData[keyGrafico] = [title: row.no_grupo, data: [], total: 1]
                    } else {
                        graficoData[keyGrafico]['total'] += 1
                    }
                    // procurar a keyCategoria no array data do grupo ['VU':0,'EN':0 ...]
                    Map item = graficoData[keyGrafico].data.find { mapTemp ->
                        return mapTemp.containsKey(keyCategoria)
                    }
                    // se encontrou, acumula o total
                    if (item) {
                        item[keyCategoria]['total'] += 1
                    } else {
                        // criar o item
                        Map map = [:]
                        map.put(keyCategoria, [total: 1, categoria: row.cd_categoria, color: color])
                        graficoData[keyGrafico].data.push(map)
                    }
                }
            }
            res.data = graficoData
        } catch (Exception e) {
            res.status=1
            res.msg= e.getMessage()
        }
        render res as JSON
    }

    /**
     * calcular o totais apresentados na página inicial do site
     * @return
     */
    def getTotais() {
        Map res = [status: 0, msg: '', data:
                [
                        totalAvaliadas   : 0
                        , totalPublicadas: 0
                        , totalAmeacadas : 0
                        , totalPessoas   : 0
                ]
        ]
        String cmdSql
        DadosApoio situacaoPublicada = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA', 'PUBLICADA')
        DadosApoio papelAvaliador = dadosApoioService.getByCodigo('TB_PAPEL_OFICINA', 'AVALIADOR')
        DadosApoio conviteRecusado = dadosApoioService.getByCodigo('TB_SITUACAO_CONVITE_PESSOA', 'CONVITE_RECUSADO')

        // calcular totais pagina inicial
        cmdSql = """WITH cte AS
                  (SELECT count(*) AS nu_total_avaliadas,
                          sum(CASE
                                  WHEN cd_situacao_ficha = 'PUBLICADA' THEN 1
                                  ELSE 0
                              END) AS nu_total_publicadas,
                          sum(CASE
                                  WHEN cd_categoria_final IN ('CR','EN','VU') THEN 1
                                  ELSE 0
                              END) AS nu_total_ameacadas
                   FROM salve.mv_dados_modulo_publico)
                SELECT cte.*,
                       especialistas.nu_total_especialistas,
                       colaboradores.nu_total_colaboradores
                FROM cte -- calcular o total de especialistas
                LEFT JOIN LATERAL
               (
                  SELECT count(distinct oficina_participante.sq_pessoa ) as nu_total_especialistas
                  FROM salve.mv_dados_modulo_publico as fichas_publicas
                  INNER JOIN salve.oficina_ficha ON oficina_ficha.sq_ficha = fichas_publicas.sq_ficha
                  INNER JOIN salve.oficina_ficha_participante ON oficina_ficha_participante.sq_oficina_ficha = oficina_ficha.sq_oficina_ficha
                  INNER JOIN salve.oficina_participante ON oficina_participante.sq_oficina_participante = oficina_ficha_participante.sq_oficina_participante
                  WHERE oficina_ficha_participante.sq_papel_participante = ${papelAvaliador.id.toString()}
                    AND oficina_participante.sq_situacao <> ${conviteRecusado.id.toString()}
                ) AS especialistas ON TRUE
                LEFT JOIN LATERAL
                  ( SELECT count(DISTINCT tmp.sq_web_usuario) AS nu_total_colaboradores
                   FROM ( SELECT sq_web_usuario
                      FROM salve.ficha_colaboracao a
                      UNION ALL SELECT sq_web_usuario
                      FROM salve.ficha_consulta_anexo
                      UNION ALL SELECT sq_web_usuario
                      FROM salve.ficha_ocorrencia_consulta ) tmp
                   ) AS colaboradores ON TRUE"""

        _getDataFromCache(cmdSql).each { row ->
            res.data.totalAvaliadas = row.nu_total_avaliadas
            res.data.totalPublicadas = row.nu_total_publicadas
            res.data.totalAmeacadas = row.nu_total_ameacadas
            res.data.totalPessoas = row.nu_total_especialistas + row.nu_total_colaboradores
        }
        render res as JSON
    }

    /**
     * metodo para criar a ficha completa por partes de acordo com a seção solicitada
     * @params - id da ficha encriptado / section
     * @return
     */
    def fichaAjax() {

        //sleep(60000);
        Ficha ficha=null
        String view = 'fichaAjax/notFound'
        String noImage = 'no-image.png'

        Map model = [:]
        boolean isAjax = params?.ajax  || request?.xhr
        String urlSalve = params.urlSalve ?: '/salve-estadual/'//'https://salve.icmbio.gov.br/salve-estadual/';

        /** /
        println '-'*80
        println '-'*80
        println '-'*80
        println  params
        /**/

        if( ! params.sqFicha )
        {
            render ''
            return;
        }
        try {
            String id = decryptStr( params.sqFicha.replaceAll(' ', '+' ) )
            //println 'Id:'+id

            if( id.isNumber() ) {
                ficha = Ficha.get( id.toLong() )
            }
            if( ! ficha ) {
                throw  new Exception('Id da ficha inexistente!');
            }

            // ler as partes da ficha conforme for requisitado
            if( ! params.section ) {
                if( ! params.subitem ) {
                    view = 'fichaAjax/index'

                    // imprimir imagem principal
                    Map imagemPrincipal = fichaService.getFichaImagemPrincipal( ficha, true )
                    if( imagemPrincipal ) {
                        imagemPrincipal.url = urlSalve +'publico/getMultimidia/' + encryptStr( imagemPrincipal.id.toString() )
                    } else {
                        imagemPrincipal.url = urlSalve+'assets/public/'+noImage
                    }
                    // categoria da Oficina de Validação
                    String codigoCategoriaFinal = ficha.categoriaFinal ? ficha.categoriaFinal.codigoSistema.toLowerCase() : ''
                    String imagemCategoriaIucn = ''
                    String hintImagemCategoriaIucn = ''
                    if( ! codigoCategoriaFinal != '' ) {
                        String assetImage = 'public/iucn/all_' + codigoCategoriaFinal + '.png'
                        if( asset.assetPathExists( src : assetImage ) ) {
                            imagemCategoriaIucn = assetPath(src:assetImage)
                        }

                        //1
                        if( codigoCategoriaFinal == 'ex') {
                            hintImagemCategoriaIucn = 'Extinção em biologia e ecologia é o total desaparecimento de espécies, subespécies ou grupos de espécies. O momento da extinção é geralmente considerado sendo a morte do último indivíduo da espécie.'
                        }
                        //2
                        else if( codigoCategoriaFinal == 'ew') {
                            hintImagemCategoriaIucn = 'Extinta na natureza é um estado de conservação atribuído a espécies ou táxons baixos, os únicos membros vivos conhecidos dos quais estão sendo mantidos em cativeiro ou como uma população naturalizada fora de sua faixa histórica.'
                        }
                        //3
                        else if( codigoCategoriaFinal == 'cr') {
                            hintImagemCategoriaIucn = 'Espécie em perigo crítico é a categoria de espécies que correm o maior risco de extinção.'
                        }
                        //4 - EN - Jandaia-amarela
                        else if( codigoCategoriaFinal == 'en') {
                            hintImagemCategoriaIucn = 'Uma espécie em perigo é uma espécie que corre grave risco de extinção. A expressão é usada vagamente na linguagem coloquial para qualquer espécie com essa descrição, mas para biólogos conservacionistas ela se refere tipicamente às que estão classificadas como endangered pela Lista Vermelha da IUCN, onde é o mais severo estado de conservação para populações selvagens, a seguir ao estado Criticamente em perigo.'
                        }
                        //5 - VU - Alouatta discolor
                        else if( codigoCategoriaFinal == 'vu') {
                            hintImagemCategoriaIucn = 'Uma espécie é considerada vulnerável pela IUCN como provável em se tornar em perigo a menos que suas condições de ameaça diminuam.'
                        }
                        // 6 - NT
                        else if( codigoCategoriaFinal == 'nt') {
                            hintImagemCategoriaIucn = 'Uma espécie é considerada Quase Ameaçada (sigla NT da Lista Vermelha da IUCN) quando ela não se qualifica para as categorias criticamente em perigo, em perigo ou vulnerável neste momento, porém está perto de se enquadrar ou será enquadrada num futuro próximo.'
                        }
                        //7 - LC
                        else if( codigoCategoriaFinal == 'lc') {
                            hintImagemCategoriaIucn = 'Uma espécie pouco preocupante (LC, Least-Concern), é uma espécie categorizada pela União Internacional para a Conservação da Natureza e dos Recursos Naturais como avaliada mas não considerada em nenhuma categoria de ameaça de extinção.'
                        }

                    }

                    // quando a categoria da última avaliação nacional publicada for diferente da obitida na
                    // oficina de Avaliação, colocar asterisco e texto informativo
                    // ler a categoria da última avaliação nacional
                    Map ultimaAvaliacaoNacionalHist = fichaService.getUltimaAvaliacaoNacionalHist(ficha.id)
                    //println ultimaAvaliacaoNacionalHist

                    boolean categoriaFoiAlterada=true;
                    String textoDiferencaCategorias = this.msgDiferencaCategorias
                    if( ultimaAvaliacaoNacionalHist?.coCategoriaIucn ) {
                        if ( codigoCategoriaFinal?.toLowerCase() == ultimaAvaliacaoNacionalHist.coCategoriaIucn.toLowerCase()) {
                            categoriaFoiAlterada = false
                        } else {
                            textoDiferencaCategorias = sprintf(textoDiferencaCategorias, ultimaAvaliacaoNacionalHist.deCategoriaIucn)
                        }
                    }

                    // citacao
                    Map ultimaAvaliacao = ficha.ultimaAvaliacaoNacional
                    String comoCitar = fichaService.comoCitarFicha( ficha, ultimaAvaliacao?.mesAnoUltimaAvaliacao )


                    model = [
                              sqFichaEncrypted           : params.sqFicha
                            , imagemPrincipal            : imagemPrincipal
                            , nmCientificoItalicoComAutor: Util.ncItalico(ficha.nmCientifico)
                            , categoriaFinal             : _getCategoriaFinal(ficha)
                            , justificativaFinal         : ficha.dsJustificativaFinal
                            , imagemCategoriaIucn        : imagemCategoriaIucn
                            , hintImagemCategoriaIucn    : hintImagemCategoriaIucn
                            , categoriaFoiAlterada       : categoriaFoiAlterada
                            , ultimaAvaliacaoNacionalHist: ultimaAvaliacaoNacionalHist
                            , autoria                    : (ficha.dsCitacao ?: '')
                            , comoCitar                  : comoCitar
                            , textoDiferencaCategorias   : textoDiferencaCategorias


                    ]
                }
            }

            // ler a sessão recebida: CLASSIFICACAO_TAXONOMICA
            else if( params.section == 'CLASSIFICACAO_TAXONOMICA') {
                view = 'fichaAjax/classificacaoTaxonomica';
                // ler a árvore taxonomica
                Map taxonTree = ficha.taxon.getStructure()
                List listClassificacaoTaxonomica = [
                        ['nivel':'Filo'   ,'descricao':taxonTree.nmFilo],
                        ['nivel':'Classe' ,'descricao':taxonTree.nmClasse],
                        ['nivel':'Ordem'  ,'descricao':taxonTree.nmOrdem],
                        ['nivel':'Família','descricao':taxonTree.nmFamilia]
                ]

                // ler nomes comuns
                String nomesComuns = fichaService.getFichaNomesComunsText( ficha )

                // ler nomes antigos
                String nomesAntigos = fichaService.getFichaSinonimiasHtml(ficha)

                // ler notas taxonomicas
                String notasTaxonomicas  = ficha.dsNotasTaxonomicas ?: ''
                String notasMorfologicas = ficha.dsDiagnosticoMorfologico ?: ''
                model = [
                        listClassificacaoTaxonomica: listClassificacaoTaxonomica
                        ,nomesComuns                : nomesComuns
                        ,nomesAntigos               : nomesAntigos
                        ,notasTaxonomicas           : _stripTags( notasTaxonomicas )
                        ,notasMorfologicas          : _stripTags( notasMorfologicas )
                   ]
            }
            else if( params.section == 'DISTRIBUICAO') {
                view = 'fichaAjax/distribuicao';
                // ler distribuicao global
                String endemicaBrasil       = ficha.snd( ficha.stEndemicaBrasil ) ?:''
                String distribuicaoGlobal   = ficha.dsDistribuicaoGeoGlobal ?:''
                String distribuicaoNacional = ficha.dsDistribuicaoGeoNacional ?:''
                String estados              = fichaService.getUfsText(ficha)
                String biomas               = fichaService.getBiomasText(ficha)
                String baciasHidrograficas  = fichaService.getBaciasText(ficha)
                List listAreasRelevantes    = fichaService.getAreasRelevantesList(ficha)

                // ler id da imagem do mapa de distribuiçao
                DadosApoio contexto = dadosApoioService.getByCodigo('TB_CONTEXTO_ANEXO', 'MAPA_DISTRIBUICAO');
                FichaAnexo anexoMapaDistribuicao = FichaAnexo.findByFichaAndContextoAndInPrincipal(ficha, contexto, 'S')
                Map imagemDistribuicao = null;
                if( anexoMapaDistribuicao ) {
                    imagemDistribuicao = [ url      : urlSalve + 'publico/getAnexo/'+encryptStr( anexoMapaDistribuicao.id.toString() )
                                           ,'legenda' : anexoMapaDistribuicao.deLegenda?:'Sem legenda'

                    ]
                }
                model = [endemicaBrasil             : endemicaBrasil
                         ,distribuicaoGlobal         : _stripTags( distribuicaoGlobal )
                         ,distribuicaoNacional       : _stripTags( distribuicaoNacional )
                         ,estados                    : estados
                         ,biomas                     : biomas
                         ,baciasHidrograficas        : baciasHidrograficas
                         ,listAreasRelevantes        : listAreasRelevantes
                         ,imagemDistribuicao         : imagemDistribuicao

                   ]
            }
            else if( params.section == 'HISTORIA_NATURAL') {
                // História Natural
                view = 'fichaAjax/historiaNatural'
                String especieMigratoria = '' //
                if( ficha.stMigratoria && ficha.stMigratoria != 'D' ) {
                    especieMigratoria = ficha.snd( ficha.stMigratoria )
                    if (ficha.stMigratoria == 'S') {
                        especieMigratoria += ', padrão de deslocamento ' + ficha?.padraoDeslocamento?.descricao + '.'
                    }
                }
                String historiaNatural                  = ficha.dsHistoriaNatural?:''
                List listHabitoAlimentar                = FichaHabitoAlimentar.findAllByFicha( ficha )
                String habitoAlimentarEspecialista      = ficha.snd( ficha.stHabitoAlimentEspecialista)
                List listHabitoAlimentarEspecialista    = FichaHabitoAlimentarEsp.findAllByFicha(ficha)
                String obsHabitoAlimentar               = ficha.dsHabitoAlimentar ?:''
                String restritoHabitatPrimario          = ficha.snd(ficha.stRestritoHabitatPrimario)
                String especialistaMicroHabitat         = ''
                if( ficha.dsEspecialistaMicroHabitat && ficha.stEspecialistaMicroHabitat == 'S') {
                    especialistaMicroHabitat = ficha.snd(ficha.stEspecialistaMicroHabitat)
                }
                String obsEspecialistaMicroHabitat      = ficha.dsEspecialistaMicroHabitat ?: ''
                String obsHabitat                       = ficha.dsUsoHabitat ?: ''
                List listInteracoes                     = FichaInteracao.findAllByFicha(ficha)
                String intervaloNascimento              = ficha?.vlIntervaloNascimento  ? ficha.vlIntervaloNascimento.toString().replaceAll(/\.0$/, '') + ' ' + ficha?.unidIntervaloNascimento?.descricao : ''
                String tempoGestacao                    = ficha?.vlTempoGestacao        ? ficha.vlTempoGestacao.toString().replaceAll(/\.0$/, '') + ' ' + ( ficha?.unidTempoGestacao ? ficha.unidTempoGestacao.descricao : '' ) : ''
                String tamanhoProle                     = ficha.vlTamanhoProle          ? ficha.vlTamanhoProle.toString().replaceAll(/\.0$/, '') + ' individuo(s)' : ''
                List listMachoFemea                     = []
                String dsReproducao                     = ''
                if (ficha.vlMaturidadeSexualFemea || ficha.vlMaturidadeSexualMacho || ficha.vlPesoMacho
                        || ficha.vlPesoFemea || ficha.vlComprimentoMacho || ficha.vlComprimentoFemea
                        || ficha.vlSenilidadeReprodutivaMacho || ficha.vlSenilidadeReprodutivaFemea
                        || ficha.vlLongevidadeMacho || ficha.vlLongevidadeFemea) {
                    // maturidade sexual
                    listMachoFemea.push([campo: 'Maturidade Sexual',
                                         macho: [valor    : ficha?.vlMaturidadeSexualMacho ? ficha?.vlMaturidadeSexualMacho.toString().replaceAll(/\.0$/, '') : ''
                                                 , unidade: ficha?.unidMaturidadeSexualMacho?.descricao
                                         ],
                                         femea: [valor    : ficha?.vlMaturidadeSexualFemea ? ficha?.vlMaturidadeSexualFemea.toString().replaceAll(/\.0$/, '') : ''
                                                 , unidade: ficha?.unidMaturidadeSexualFemea?.descricao
                                         ]])
                    // peso
                    listMachoFemea.push([campo: 'Peso',
                                         macho: [valor    : ficha?.vlPesoMacho ? ficha.vlPesoMacho.toString().replaceAll(/\.0$/, '') : ''
                                                 , unidade: ficha?.unidadePesoMacho?.descricao
                                         ],
                                         femea: [valor    : ficha?.vlPesoFemea ? ficha.vlPesoFemea.toString().replaceAll(/\.0$/, '') : ''
                                                 , unidade: ficha?.unidadePesoFemea?.descricao
                                         ]])
                    // comprimento na maturidade sexual
                    listMachoFemea.push([campo: 'Comprimento na maturidade sexual',
                                         macho: [valor    : ficha?.vlComprimentoMacho ? ficha.vlComprimentoMacho.toString().replaceAll(/\.0$/, '') : ''
                                                 , unidade: ficha?.medidaComprimentoMacho?.descricao
                                         ],
                                         femea: [valor    : ficha?.vlComprimentoFemea ? ficha.vlComprimentoFemea.toString().replaceAll(/\.0$/, '') : ''
                                                 , unidade: ficha?.medidaComprimentoFemea?.descricao
                                         ]])
                    // comprimento max
                    listMachoFemea.push([campo: 'Comprimento máximo',
                                         macho: [valor    : ficha?.vlComprimentoMachoMax ? ficha.vlComprimentoMachoMax.toString().replaceAll(/\.0$/, '') : ''
                                                 , unidade: ficha?.medidaComprimentoMacho?.descricao
                                         ],
                                         femea: [valor    : ficha?.vlComprimentoFemeaMax ? ficha.vlComprimentoFemeaMax.toString().replaceAll(/\.0$/, '') : ''
                                                 , unidade: ficha?.medidaComprimentoFemea?.descricao
                                         ]])
                    // senilidade reprodutiva
                    listMachoFemea.push([campo: 'Senilidade Reprodutiva',
                                         macho: [valor    : ficha?.vlSenilidadeReprodutivaMacho ? ficha.vlSenilidadeReprodutivaMacho.toString().replaceAll(/\.0$/, '') : ''
                                                 , unidade: ficha?.unidadeSenilidRepMacho?.descricao
                                         ],
                                         femea: [valor    : ficha?.vlSenilidadeReprodutivaFemea ? ficha.vlSenilidadeReprodutivaFemea.toString().replaceAll(/\.0$/, '') : ''
                                                 , unidade: ficha?.unidadeSenilidRepFemea?.descricao
                                         ]])
                    // longevidade
                    listMachoFemea.push([campo: 'Longevidade',
                                         macho: [valor    : ficha?.vlLongevidadeMacho ? ficha.vlLongevidadeMacho.toString().replaceAll(/\.0$/, '') : ''
                                                 , unidade: ficha?.unidadeLongevidadeMacho?.descricao
                                         ],
                                         femea: [valor    : ficha?.vlLongevidadeFemea ? ficha.vlLongevidadeFemea.toString().replaceAll(/\.0$/, '') : ''
                                                 , unidade: ficha?.unidadeLongevidadeFemea?.descricao
                                         ]])
                    dsReproducao = ficha?.dsReproducao ?: ''
                }

                model = [ especieMigratoria              : especieMigratoria
                        ,historiaNatural                 : _stripTags( historiaNatural)
                        ,listHabitoAlimentar             : listHabitoAlimentar
                        ,habitoAlimentarEspecialista     : habitoAlimentarEspecialista
                        ,listHabitoAlimentarEspecialista : listHabitoAlimentarEspecialista
                        ,obsHabitoAlimentar              : _stripTags( obsHabitoAlimentar)
                        ,restritoHabitatPrimario         : restritoHabitatPrimario
                        ,especialistaMicroHabitat        : especialistaMicroHabitat
                        ,obsEspecialistaMicroHabitat     : _stripTags( obsEspecialistaMicroHabitat)
                        ,obsHabitat                      : _stripTags( obsHabitat)
                        ,listInteracoes                  : listInteracoes
                        ,intervaloNascimento             : intervaloNascimento
                        ,tempoGestacao                   : tempoGestacao
                        ,tamanhoProle                    : tamanhoProle
                        ,listMachoFemea                  : listMachoFemea
                        ,dsReproducao                    : _stripTags( dsReproducao )

                ]
            }
            else if( params.section == 'POPULACAO') {
                // POPULAÇÃO
                view = 'fichaAjax/populacao'
                String tendenciaPopulacional            = ficha?.tendenciaPopulacional?.descricao ?:''
                String razaoSexual                      = ficha.dsRazaoSexual ?: ''
                String taxaMortalidadeNatural           = ficha.dsTaxaMortalidade ?: ''
                String caracteristicaGenetica           = ficha.dsCaracteristicaGenetica ?: ''
                String obsPopulacao                     = ficha.dsPopulacao ?: ''
                // ler id da imagem do mapa de distribuiçao
                DadosApoio contexto = dadosApoioService.getByCodigo('TB_CONTEXTO_ANEXO', 'GRAFICO_POPULACAO');
                List listImagemPopulacao =[]
                FichaAnexo.findAllByFichaAndContexto(ficha, contexto).each {
                    listImagemPopulacao.push([ url: urlSalve + 'publico/getAnexo/'+encryptStr(it.id.toString() )
                                               ,'legenda': it.deLegenda?:'Sem legenda'
                                               ,'id'     : it.id.toString() // para o lightbox
                                               ,'alt'     : it.noArquivo
                    ] )
                }
                model = [ tendenciaPopulacional          : tendenciaPopulacional
                         ,razaoSexual                    : razaoSexual
                         ,taxaMortalidadeNatural         : _stripTags( taxaMortalidadeNatural )
                         ,caracteristicaGenetica         : _stripTags( caracteristicaGenetica )
                         ,obsPopulacao                   : _stripTags( obsPopulacao )
                         ,listImagemPopulacao            : listImagemPopulacao]
            }
            else if( params.section == 'AMEACA') {
                // AMEACA
                view = 'fichaAjax/ameaca'
                List listAmeaca = []
                FichaAmeaca.findAllByFicha(ficha).sort{ it.criterioAmeacaIucn.ordem }.each {
                    // Regioes
                    String regioes = ''
                    //println it.regioesText
                    if( it?.regioesText )
                    {
                        regioes = it.regioesText
                    }
                    listAmeaca.push( [ 'descricao'  : it?.criterioAmeacaIucn?.descricaoHieraquia
                                       ,'refBibHtml' : it.refBibHtml
                                       ,'regioes'    : regioes
                    ])
                }
                String obsAmeaca           = _stripTags( ficha.dsAmeaca ?: '' )

                // ler imagens da ameaça
                DadosApoio contexto = dadosApoioService.getByCodigo('TB_CONTEXTO_ANEXO', 'MAPA_AMEACA');
                List listImagemAmeaca = []
                FichaAnexo.findAllByFichaAndContexto(ficha, contexto).each {
                    listImagemAmeaca.push([ url: urlSalve + 'publico/getAnexo/'+encryptStr(it.id.toString() )
                                            ,'legenda': it.deLegenda?:'Sem legenda'
                                            ,'id'     : it.id.toString() // para o lightbox
                                            ,'alt'    : it.noArquivo
                    ] )
                }
                model= [ listAmeaca                     : listAmeaca
                        ,obsAmeaca                      : _stripTags( obsAmeaca )
                        ,listImagemAmeaca               : listImagemAmeaca
                ]

            }
            else if( params.section == 'USO') {
                // USOS
                List listUso = []
                FichaUso.findAllByFicha(ficha).each {
                    // Regioes
                    String regioes = ''
                    if( it?.regioesText )
                    {
                        regioes = it.regioesText
                    }
                    listUso.push( [ 'descricao'  : it?.uso?.descricaoHieraquia
                                    ,'refBibHtml' : it.refBibHtml
                                    ,'regioes'    : regioes
                    ])
                }
                String obsUso = _stripTags( ficha.dsUso ?: '' )
                // ler imagens do USO
                DadosApoio contexto = dadosApoioService.getByCodigo('TB_CONTEXTO_ANEXO', 'MAPA_USO');
                List listImagemUso = []
                FichaAnexo.findAllByFichaAndContexto(ficha, contexto).each {
                    listImagemUso.push([ url: urlSalve+'publico/getAnexo/'+encryptStr(it.id.toString())
                                         ,'legenda': it.deLegenda?:'Sem legenda'
                                         ,'id'     : it.id.toString() // para o lightbox
                                         ,'alt'    : it.noArquivo
                    ] )
                }

                // USO
                view = 'fichaAjax/uso'
                model = [listUso                        : listUso
                         ,obsUso                         : _stripTags( obsUso )
                         ,listImagemUso                  : listImagemUso
                ]

            }
            else if( params.section == 'CONSERVACAO') {
                view = 'fichaAjax/conservacao'
                String categoriaFinal = _getCategoriaFinal( ficha )
                Map ultimaAvaliacao = [ 'categoria'     : categoriaFinal
                                        ,'ano'          : categoriaFinal ? ficha.cicloAvaliacao.nuAno.toString() : ''
                                        ,'criterio'     : _stripTags( ficha.dsCriterioAvalIucnFinal ?: '')
                                        ,'justificativa': _stripTags( ficha.dsJustificativaFinal ?:'')
                ]

                List listHistoricoAvaliacao = []
                //println 'Ano <= ' + ( ultimaAvaliacao.ano.toInteger()+4 )
                TaxonHistoricoAvaliacao.findAllByTaxonAndNuAnoAvaliacaoLessThanEquals(ficha.taxon, ficha.cicloAvaliacao.nuAno + 4 ).sort { it.nuAnoAvaliacao.toString() + it.tipoAvaliacao.ordem }.each {
                    // a lista de historico contem todas as avaliações nacionais, estaduais e globais e no gride
                    listHistoricoAvaliacao.push(
                        [
                             'sqTaxonHistoricoAvaliacao' : encryptStr( it.id.toString() )
                            ,'tipo'             : it?.tipoAvaliacao?.descricao
                            , 'tipoCodigo'     : it?.tipoAvaliacao.codigo
                            , 'ano'            : it?.nuAnoAvaliacao?.toString()
                            , 'abrangencia'    : (it?.tipoAbrangencia?.codigoSistema == 'OUTRA' ? it.noRegiaoOutra : it?.abrangencia?.descricao)
                            , 'categoria'      : it?.categoriaIucn?.codigoSistema != 'OUTRA' ? it?.categoriaIucn?.descricaoCompleta: it?.deCategoriaOutra + '*'
                            , 'existeJustificativa' : ( it.txJustificativaAvaliacao ? true : false )
                            , 'categoriaCodigo': it?.categoriaIucn?.codigoSistema
                            , 'criterio'       : it?.deCriterioAvaliacaoIucn ?: ''
                            , 'refBibHtml'     : it?.refBibHtml
                        ]
                    )
                }

                String presencaListaVigente            = ficha.snd( ficha.stPresencaListaVigente )
                List listConvencao                      = []
                FichaListaConvencao.findAllByFicha(ficha).each {
                    listConvencao.push(['descricao' : it?.listaConvencao?.descricao
                                        ,'ano'      : it?.nuAno
                    ])
                }
                List listAcaoConservacao = []
                FichaAcaoConservacao.findAllByFicha(ficha).each {

                    // plano de ação ou ordenamento
                    String planoAcao = (it?.planoAcao?.sgPlanoAcao ? it.planoAcao.sgPlanoAcao : (it?.tipoOrdenamento?.descricao ?: '') )
                    listAcaoConservacao.push([
                            'acao'      : it.acaoConservacao?.descricaoHieraquia ?:''
                            //'acao'      : it.acaoConservacao?.descricaoCompleta ?:''
                            ,'situacao'  : it.situacaoAcaoConservacao?.descricao ?:''
                            ,'refBibHtml': it?.refBibHtml
                            ,'planoAcao' : planoAcao ?: ''
                    ])
                }
                String obsAcaoConservacao   = _stripTags( ficha.dsAcaoConservacao ?: '' )
                List listUc                 = []
                fichaService.getUcsOcorrencia( ficha.id ).each {
                    listUc.push([
                            'descricao'     : it?.fichaOcorrencia?.ucHtml
                            ,'refBibHtml'   : it?.fichaOcorrencia?.refBibHtml
                    ])
                }
                String obsPresencaUc        = _stripTags( ficha.dsPresencaUc ?: '')
                model = [ ultimaAvaliacao                : ultimaAvaliacao
                         ,listHistoricoAvaliacao         : listHistoricoAvaliacao
                         ,presencaListaVigente           : presencaListaVigente
                         ,listConvencao                  : listConvencao
                         ,listAcaoConservacao            : listAcaoConservacao
                         ,obsAcaoConservacao             : obsAcaoConservacao
                         ,listUc                         : listUc
                         ,obsPresencaUc                  : obsPresencaUc]

             }
            else if( params.section == 'PESQUISA') {
                view = 'fichaAjax/pesquisa'
                List listPesquisa = []
                FichaPesquisa.findAllByFicha(ficha).each{
                    listPesquisa.push([
                            'descricao'     : it?.tema?.descricao
                            ,'situacao'     : it?.situacao?.descricao
                            ,'refBibHtml'   : it?.refBibHtml
                    ])
                }
                String obsPesquisa = _stripTags( ficha.dsPesquisaExistNecessaria ?: '')
                model = [   listPesquisa  : listPesquisa
                            ,obsPesquisa  : obsPesquisa
                ]
            }
            else if( params.section == 'REFERENCIA_BIBLIOGRAFICA') {
                view = 'fichaAjax/referenciaBibliografica'
                List listReferencia = []
                FichaRefBib.createCriteria().list() {
                    eq('ficha', ficha)
                    isNotNull('publicacao') // não incluir a comunicação pessoal
                }.unique { it.publicacao }.sort { Util.removeAccents(it.publicacao?.noAutor) }.each {
                    listReferencia.push( [ descricao : it.referenciaHtml ] )
                }
                model = [ listReferencia : listReferencia ]
            }


            } catch (Exception e ) {
            render e.getMessage()
            return
        }
        //sleep(1000);
        //println model
        render(view: view, model: model);

    }

    /**
     * método para gerar a ficha em pdf e fazer o download
     */
    def fichaPdf() {
        Ficha ficha
        try {
            String id = decryptStr( params.sqFicha.replaceAll(' ', '+' ) )
            if( id.isNumber() ) {
                ficha = Ficha.get( id.toLong() )
            }
            if( !ficha ) {
                render '<h2>Ficha inválida</h2>';
                return;
            }
            PdfFicha oPdf = new PdfFicha(ficha.id.toInteger(), grailsApplication.config.temp.dir.toString())
            oPdf.setContextoPublico(true)
            oPdf.setMsgDiferencaCategorias( this.msgDiferencaCategorias )
            oPdf.setUnidade('Salve - público')
            oPdf.setCiclo( ficha.cicloAvaliacao.deCicloAvaliacao.toString() )
            String fileName = oPdf.run()
            render(file: new File( fileName ), fileName: "ficha_especie_" + (Util.ncItalico( ficha.nmCientifico,true,false).replaceAll(/ /, '_')) + '.pdf')
        } catch( Exception e ) {
            render e.getMessage()
        }
        return
    }

    def fichaHtml() {

        String urlSalve = params.urlSalve ?: 'https://salve.icmbio.gov.br/salve-estadual/';

        Ficha ficha;
        try {
            String id = decryptStr( params.sqFicha.replaceAll(' ', '+' ) )
            //println 'Id:'+id

            if( id.isNumber() ) {
                ficha = Ficha.get( id.toLong() )
            }
        } catch (Exception e ) {
            ficha = null;
        }

        if( !ficha ) {
            render '<h2>Ficha inválida</h2>';
            return;
        }

        // ler a categoria final da ficha
        String categoriaFinal = ''
        if( ficha.categoriaFinal ) {
            categoriaFinal = ficha.categoriaFinal.descricao + ' (' + ficha.categoriaFinal.codigoSistema + ')'
        }

        // ler a árvore taxonomica
        Map taxonTree = ficha.taxon.getStructure()
        List listClassificacaoTaxonomica = [
                ['nivel':'Filo'   ,'descricao':taxonTree.nmFilo],
                ['nivel':'Classe' ,'descricao':taxonTree.nmClasse],
                ['nivel':'Ordem'  ,'descricao':taxonTree.nmOrdem],
                ['nivel':'Família','descricao':taxonTree.nmFamilia]
        ]

        // ler nomes comuns
        String nomesComuns = fichaService.getFichaNomesComunsText( ficha )

        // ler nomes antigos
        String nomesAntigos = fichaService.getFichaSinonimiasHtml(ficha)

        // ler notas taxonomicas
        String notasTaxonomicas  = ficha.dsNotasTaxonomicas ?: ''
        String notasMorfologicas = ficha.dsDiagnosticoMorfologico ?: ''


        // ler distribuicao global
        String endemicaBrasil       = ficha.snd( ficha.stEndemicaBrasil) ?:''
        String distribuicaoGlobal   = ficha.dsDistribuicaoGeoGlobal ?:''
        String estados              = fichaService.getUfsText(ficha)
        String biomas               = fichaService.getBiomasText(ficha)
        String baciasHidrograficas  = fichaService.getBaciasText(ficha)
        List listAreasRelevantes    = fichaService.getAreasRelevantesList(ficha)

        // ler id da imagem do mapa de distribuiçao
        DadosApoio contexto = dadosApoioService.getByCodigo('TB_CONTEXTO_ANEXO', 'MAPA_DISTRIBUICAO');
        FichaAnexo anexoMapaDistribuicao = FichaAnexo.findByFichaAndContextoAndInPrincipal(ficha, contexto, 'S')
        Map imagemDistribuicao = null;
        if( anexoMapaDistribuicao ) {
            imagemDistribuicao = [ url      : urlSalve + 'publico/getAnexo/'+encryptStr( anexoMapaDistribuicao.id.toString() )
                                 ,'legenda' : anexoMapaDistribuicao.deLegenda?:'Sem legenda'

            ]
        }
        // História Natural
        String especieMigratoria = ficha.snd( ficha.stMigratoria )
        if( ficha.stMigratoria == 'S') {
            especieMigratoria += ', padrão de deslocamento ' + ficha?.padraoDeslocamento?.descricao+'.'
        }
        String historiaNatural = ficha.dsHistoriaNatural?:''
        List listHabitoAlimentar                = FichaHabitoAlimentar.findAllByFicha( ficha )
        String habitoAlimentarEspecialista      = ficha.snd( ficha.stHabitoAlimentEspecialista)
        List listHabitoAlimentarEspecialista    = FichaHabitoAlimentarEsp.findAllByFicha(ficha)
        String obsHabitoAlimentar               = ficha.dsHabitoAlimentar ?:''
        String restritoHabitatPrimario          = ficha.snd(ficha.stRestritoHabitatPrimario)
        String especialistaMicroHabitat         = ficha.snd(ficha.stEspecialistaMicroHabitat)
        String obsEspecialistaMicroHabitat      = ficha.dsEspecialistaMicroHabitat ?: ''
        String obsHabitat                       = ficha.dsUsoHabitat ?: ''
        List listInteracoes                     = FichaInteracao.findAllByFicha(ficha)
        String intervaloNascimento              = ficha?.vlIntervaloNascimento  ? ficha.vlIntervaloNascimento.toString().replaceAll(/\.0$/, '') + ' ' + ficha?.unidIntervaloNascimento?.descricao : ''
        String tempoGestacao                    = ficha?.vlTempoGestacao        ? ficha.vlTempoGestacao.toString().replaceAll(/\.0$/, '') + ' ' + ( ficha?.unidTempoGestacao ? ficha.unidTempoGestacao.descricao : '' ) : ''
        String tamanhoProle                     = ficha.vlTamanhoProle          ? ficha.vlTamanhoProle.toString().replaceAll(/\.0$/, '') + ' individuo(s)' : ''
        List listMachoFemea                     = []
        // maturidade sexual
        listMachoFemea.push([campo:'Maturidade Sexual',
                             macho:[ valor : ficha?.vlMaturidadeSexualMacho ? ficha?.vlMaturidadeSexualMacho.toString().replaceAll(/\.0$/,'') : ''
                                     , unidade : ficha?.unidMaturidadeSexualMacho?.descricao
                               ],
                             femea:[ valor : ficha?.vlMaturidadeSexualFemea ? ficha?.vlMaturidadeSexualFemea.toString().replaceAll(/\.0$/,'')  : ''
                                     , unidade : ficha?.unidMaturidadeSexualFemea?.descricao
                             ]])
        // peso
        listMachoFemea.push([campo:'Peso',
                             macho:[ valor : ficha?.vlPesoMacho ? ficha.vlPesoMacho.toString().replaceAll(/\.0$/,'') : ''
                                 , unidade : ficha?.unidadePesoMacho?.descricao
                            ],
                            femea :[ valor : ficha?.vlPesoFemea ? ficha.vlPesoFemea.toString().replaceAll(/\.0$/,'') : ''
                                     , unidade :  ficha?.unidadePesoFemea?.descricao
                             ]])
        // comprimento na maturidade sexual
        listMachoFemea.push([campo:'Comprimento na maturidade sexual',
                             macho:[ valor : ficha?.vlComprimentoMacho ? ficha.vlComprimentoMacho.toString().replaceAll(/\.0$/,'') : ''
                                   , unidade : ficha?.medidaComprimentoMacho?.descricao
                             ],
                             femea:[ valor : ficha?.vlComprimentoFemea ? ficha.vlComprimentoFemea.toString().replaceAll(/\.0$/,'') : ''
                                   , unidade : ficha?.medidaComprimentoFemea?.descricao
                             ]])
        // comprimento max
        listMachoFemea.push([campo:'Comprimento máximo',
                             macho:[ valor : ficha?.vlComprimentoMachoMax ? ficha.vlComprimentoMachoMax.toString().replaceAll(/\.0$/,'') : ''
                                     , unidade : ficha?.medidaComprimentoMacho?.descricao
                             ],
                             femea:[ valor : ficha?.vlComprimentoFemeaMax ? ficha.vlComprimentoFemeaMax.toString().replaceAll(/\.0$/,'') : ''
                                     , unidade : ficha?.medidaComprimentoFemea?.descricao
                             ]])
        // senilidade reprodutiva
        listMachoFemea.push([campo:'Senilidade Reprodutiva',
                             macho:[ valor : ficha?.vlSenilidadeReprodutivaMacho ? ficha.vlSenilidadeReprodutivaMacho.toString().replaceAll(/\.0$/,'') : ''
                                     , unidade : ficha?.unidadeSenilidRepMacho?.descricao
                             ],
                             femea:[ valor : ficha?.vlSenilidadeReprodutivaFemea ? ficha.vlSenilidadeReprodutivaFemea.toString().replaceAll(/\.0$/,'') : ''
                                     , unidade : ficha?.unidadeSenilidRepFemea?.descricao
                             ]])
        // longevidade
        listMachoFemea.push([campo:'Longevidade',
                             macho:[ valor : ficha?.vlLongevidadeMacho ? ficha.vlLongevidadeMacho.toString().replaceAll(/\.0$/,'')  : ''
                                     , unidade : ficha?.unidadeLongevidadeMacho?.descricao
                             ],
                             femea:[ valor : ficha?.vlLongevidadeFemea ? ficha.vlLongevidadeFemea.toString().replaceAll(/\.0$/,'') : ''
                                     , unidade : ficha?.unidadeLongevidadeFemea?.descricao
                             ]])


        String dsReproducao                     = ficha?.dsReproducao ?: ''

        // POPULAÇÃO
        String tendenciaPopulacional            = ficha?.tendenciaPopulacional?.descricao ?:''
        String razaoSexual                      = ficha.dsRazaoSexual ?: ''
        String taxaMortalidadeNatural           = ficha.dsTaxaMortalidade ?: ''
        String caracteristicaGenetica           = ficha.dsCaracteristicaGenetica ?: ''
        String obsPopulacao                     = ficha.dsPopulacao ?: ''
        // ler id da imagem do mapa de distribuiçao
        contexto = dadosApoioService.getByCodigo('TB_CONTEXTO_ANEXO', 'GRAFICO_POPULACAO');
        List listImagemPopulacao =[]
        FichaAnexo.findAllByFichaAndContexto(ficha, contexto).each {
            listImagemPopulacao.push([ url: urlSalve + 'publico/getAnexo/'+encryptStr( it.id.toString() )
                                      ,'legenda': it.deLegenda?:'Sem legenda'
                                      ,'id'     : it.id.toString() // para o lightbox
                                      ,'alt'     : it.noArquivo
            ] )
        }

        // AMEAÇAS
        List listAmeaca = []
        FichaAmeaca.findAllByFicha(ficha).sort{ it.criterioAmeacaIucn.ordem }.each {
            // Regioes
            String regioes = ''
            //println it.regioesText
            if( it?.regioesText )
            {
                regioes = it.regioesText
            }
            listAmeaca.push( [ 'descricao'  : it?.criterioAmeacaIucn?.descricaoHieraquia
                              ,'refBibHtml' : it.refBibHtml
                              ,'regioes'    : regioes
            ])
        }
        String obsAmeaca           = ficha.dsAmeaca ?: ''
        // ler imagens da ameaça
        contexto = dadosApoioService.getByCodigo('TB_CONTEXTO_ANEXO', 'MAPA_AMEACA');
        List listImagemAmeaca = []
        FichaAnexo.findAllByFichaAndContexto(ficha, contexto).each {
            listImagemAmeaca.push([ url: urlSalve + 'publico/getAnexo/'+encryptStr(it.id.toString())
                                       ,'legenda': it.deLegenda?:'Sem legenda'
                                       ,'id'     : it.id.toString() // para o lightbox
                                       ,'alt'    : it.noArquivo
            ] )
        }

        // USOS
        List listUso = []
        FichaUso.findAllByFicha(ficha).each {
            // Regioes
            String regioes = ''
            if( it?.regioesText )
            {
                regioes = it.regioesText
            }
            listUso.push( [ 'descricao'  : it?.uso?.descricaoHieraquia
                           ,'refBibHtml' : it.refBibHtml
                           ,'regioes'    : regioes
            ])
        }
        String obsUso = ficha.dsUso ?: ''
        // ler imagens do USO
        contexto = dadosApoioService.getByCodigo('TB_CONTEXTO_ANEXO', 'MAPA_USO');
        List listImagemUso = []
        FichaAnexo.findAllByFichaAndContexto(ficha, contexto).each {
            listImagemUso.push([ url: urlSalve+'publico/getAnexo/'+encryptStr( it.id.toString() )
                                    ,'legenda': it.deLegenda?:'Sem legenda'
                                    ,'id'     : it.id.toString() // para o lightbox
                                    ,'alt'    : it.noArquivo
            ] )
        }

        // CONSERVAÇÃO
        Map ultimaAvaliacao = [ 'categoria'     : categoriaFinal
                                ,'ano'          : categoriaFinal ? ficha.cicloAvaliacao.nuAno.toString() : ''
                                ,'criterio'     : ficha.dsCriterioAvalIucnFinal ?: ''
                                ,'justificativa': ficha.dsJustificativaFinal ? Util.removeBgWhite( ficha.dsJustificativaFinal ) :''
                               ]

        List listHistoricoAvaliacao = []
        //println 'Ano <= ' + ( ultimaAvaliacao.ano.toInteger()+4 )
        TaxonHistoricoAvaliacao.findAllByTaxonAndNuAnoAvaliacaoLessThanEquals(ficha.taxon, ficha.cicloAvaliacao.nuAno + 4 ).sort { it.nuAnoAvaliacao.toString() + it.tipoAvaliacao.ordem }.each {
            // a lista de historico contem todas as avaliações nacionais, estaduais e globais e no gride
            // nao pode ser listada a ultima avaliação nacional novamente
            if( it.tipoAvaliacao?.codigoSistema != 'NACIONAL_BRASIL' || ( it.tipoAvaliacao?.codigoSistema == 'NACIONAL_BRASIL'
                        && ultimaAvaliacao.ano
                        && it.nuAnoAvaliacao.toInteger() <= ultimaAvaliacao.ano.toInteger() ) ) {
                    listHistoricoAvaliacao.push(
                            [
                                    'tipo'          : it?.tipoAvaliacao?.descricao
                                    ,'ano'          : it?.nuAnoAvaliacao?.toString()
                                    ,'abrangencia'  : (it?.tipoAbrangencia?.codigoSistema == 'OUTRA' ? it.noRegiaoOutra : it?.abrangencia?.descricao )
                                    ,'categoria'    : it?.categoriaIucn?.codigoSistema != 'OUTRA'
                                     ? it?.categoriaIucn?.descricaoCompleta
                                     : it?.deCategoriaOutra+'*'
                                    ,'categoriaCodigo'  : it?.categoriaIucn?.codigoSistema
                                    ,'criterio'         : it?.deCriterioAvaliacaoIucn ?: ''
                                    ,'refBibHtml'       : it?.refBibHtml
                            ]
                    )
                }
        }

        String presencaListaVigente            = ficha.snd( ficha.stPresencaListaVigente )
        List listConvencao                      = []
        FichaListaConvencao.findAllByFicha(ficha).each {
            listConvencao.push(['descricao' : it?.listaConvencao?.descricao
                                ,'ano'      : it?.nuAno
                               ])
        }
        List listAcaoConservacao = []
        FichaAcaoConservacao.findAllByFicha(ficha).each {

            // plano de ação ou ordenamento
            String planoAcao = (it?.planoAcao?.sgPlanoAcao ? it.planoAcao.sgPlanoAcao : (it?.tipoOrdenamento?.descricao ?: '') )
            listAcaoConservacao.push([
                     'acao'      : it.acaoConservacao?.descricaoCompleta ?:''
                    ,'situacao'  : it.situacaoAcaoConservacao?.descricao ?:''
                    ,'refBibHtml': it?.refBibHtml
                    ,'planoAcao' : planoAcao ?: ''
            ])
        }
        String obsAcaoConservacao   = ficha.dsAcaoConservacao ?: ''
        List listUc                 = []
        fichaService.getUcsOcorrencia( ficha.id ).each {
            listUc.push([
                    'descricao'     : it?.fichaOcorrencia?.ucHtml
                    ,'refBibHtml'   : it?.fichaOcorrencia?.refBibHtml
                    ])
        }
        String obsPresencaUc        = ficha.dsPresencaUc ?: ''


        // PESQUISA
        List listPesquisa = []
        FichaPesquisa.findAllByFicha(ficha).each{
            listPesquisa.push([
                    'descricao'     : it?.tema?.descricao
                    ,'situacao'     : it?.situacao?.descricao
                    ,'refBibHtml'   : it?.refBibHtml
            ])
        }
        String obsPesquisa = ficha.dsPesquisaExistNecessaria ?: ''

        // REFERÊNCIA BIBLIOGRAFICA
        List listReferencia = []
        FichaRefBib.createCriteria().list() {
            eq('ficha', ficha)
            isNotNull('publicacao') // não incluir a comunicação pessoal
        }.unique { it.publicacao }.sort { it.publicacao.noAutor }.each {
            listReferencia.push( [ descricao : it.referenciaHtml ] )
        }

        render( view: 'fichaHtml', model:[ sqFichaEncrypted:params.sqFicha
                ,ficha                      : ficha
                ,nmCientificoItalicoComAutor: Util.ncItalico(ficha.nmCientifico)
                ,categoriaFinal             : categoriaFinal
                ,listClassificacaoTaxonomica: listClassificacaoTaxonomica
                ,nomesComuns                : nomesComuns
                ,nomesAntigos               : nomesAntigos
                ,notasTaxonomicas           : Util.removeBgWhite(notasTaxonomicas)
                ,notasMorfologicas          : Util.removeBgWhite(notasMorfologicas)

                // grupo distribução
                ,endemicaBrasil             : endemicaBrasil
                ,distribuicaoGlobal         : Util.removeBgWhite(distribuicaoGlobal)
                ,estados                    : estados
                ,biomas                     : biomas
                ,baciasHidrograficas        : baciasHidrograficas
                ,listAreasRelevantes        : listAreasRelevantes
                ,imagemDistribuicao         : imagemDistribuicao

                // História natural
                ,especieMigratoria          : especieMigratoria
                ,historiaNatural            : Util.removeBgWhite(historiaNatural)
                ,listHabitoAlimentar        : listHabitoAlimentar
                ,habitoAlimentarEspecialista : habitoAlimentarEspecialista
                ,listHabitoAlimentarEspecialista : listHabitoAlimentarEspecialista
                ,obsHabitoAlimentar              : Util.removeBgWhite(obsHabitoAlimentar)
                ,restritoHabitatPrimario         : restritoHabitatPrimario
                ,especialistaMicroHabitat        : especialistaMicroHabitat
                ,obsEspecialistaMicroHabitat     : Util.removeBgWhite(obsEspecialistaMicroHabitat)
                ,obsHabitat                      : Util.removeBgWhite(obsHabitat)
                ,listInteracoes                  : listInteracoes
                ,intervaloNascimento             : intervaloNascimento
                ,tempoGestacao                   : tempoGestacao
                ,tamanhoProle                    : tamanhoProle
                ,listMachoFemea                  : listMachoFemea
                ,dsReproducao                    : Util.removeBgWhite( dsReproducao )

                // POPULAÇÃO
                ,tendenciaPopulacional          : tendenciaPopulacional
                ,razaoSexual                    : razaoSexual
                ,taxaMortalidadeNatural         : taxaMortalidadeNatural
                ,caracteristicaGenetica         : caracteristicaGenetica
                ,obsPopulacao                   : Util.removeBgWhite( obsPopulacao )
                ,listImagemPopulacao            : listImagemPopulacao

                // AMEACA
                ,listAmeaca                     : listAmeaca
                ,obsAmeaca                      : obsAmeaca
                ,listImagemAmeaca               : listImagemAmeaca

                // USO
                ,listUso                        : listUso
                ,obsUso                         : obsUso
                ,listImagemUso                  : listImagemUso

                // CONSERVACAO
                ,ultimaAvaliacao                : ultimaAvaliacao
                ,listHistoricoAvaliacao         : listHistoricoAvaliacao
                ,presencaListaVigente           : presencaListaVigente
                ,listConvencao                  : listConvencao
                ,listAcaoConservacao            : listAcaoConservacao
                ,obsAcaoConservacao             : obsAcaoConservacao
                ,listUc                         : listUc
                ,obsPresencaUc                  : obsPresencaUc
                ,listPesquisa                   : listPesquisa
                ,obsPesquisa                    : obsPesquisa

                // REFERENCIA BIBLIOGRÁFICA
                ,listReferencia                 : listReferencia
        ] );
    }

    def exportOccurrencesOld() {
        Map res         = [ status:0, msg:'',data:[:] ]
        String path     = grailsApplication.config.temp.dir
        String hora     = new Date().format('dd_MM_yyyy_hh_mm_ss')
        String dataAtual= new Date().format('yyyy-MM-dd')
        res.data.fileName = path + 'salve_exportacao_registros_' + hora + '.xlsx'
        try {
            if( ! params.fichasIds ){
                throw new Exception('Nenhuma ficha informada para exportação das ocorrências.')
            }
            // desencriptar os ids das fichas
            List fichasIds = params.fichasIds.split(',').collect{ decryptStr(it) }

            String cmdSql ="""SELECT res.* from (
                    -- LER OCORRÊNCIAS DO SALVE
                    select tmp.*
                    ,taxon.json_trilha->'reino'->>'no_taxon' as no_reino
                    ,taxon.json_trilha->'filo'->>'no_taxon' as no_filo
                    ,taxon.json_trilha->'classe'->>'no_taxon' as no_classe
                    ,taxon.json_trilha->'ordem'->>'no_taxon' as no_ordem
                    ,taxon.json_trilha->'familia'->>'no_taxon' as no_familia
                    ,taxon.json_trilha->'genero'->>'no_taxon' as no_genero
                    from (
                    Select ocorrencia.sq_ficha
                    ,ocorrencia.nm_cientifico
                    ,ocorrencia.nu_y
                    ,ocorrencia.nu_x
                    ,ocorrencia.no_prazo_carencia
                    ,ocorrencia.dt_carencia
                    ,ocorrencia.no_datum
                    ,ocorrencia.no_formato_original
                    ,ocorrencia.no_precisao
                    ,ocorrencia.no_referencia_aproximacao
                    ,ocorrencia.no_metodologia_aproximacao
                    ,ocorrencia.tx_metodologia_aproximacao
                    ,ocorrencia.no_taxon_citado
                    ,ocorrencia.ds_presenca_atual
                    ,ocorrencia.no_tipo_registro
                    ,ocorrencia.nu_dia_registro
                    ,ocorrencia.nu_mes_registro
                    ,ocorrencia.nu_ano_registro
                    ,ocorrencia.hr_registro
                    ,ocorrencia.nu_dia_registro_fim
                    ,ocorrencia.nu_mes_registro_fim
                    ,ocorrencia.nu_ano_registro_fim
                    ,ocorrencia.ds_data_desconhecida
                    ,ocorrencia.no_localidade
                    ,ocorrencia.ds_caracteristica_localidade
                    ,ocorrencia.no_uc_federal
                    ,ocorrencia.no_uc_estadual
                    ,ocorrencia.no_rppn
                    ,ocorrencia.no_pais
                    ,ocorrencia.no_estado
                    ,ocorrencia.no_municipio
                    ,ocorrencia.no_ambiente
                    ,ocorrencia.no_habitat
                    ,ocorrencia.vl_elevacao_min
                    ,ocorrencia.vl_elevacao_max
                    ,ocorrencia.vl_profundidade_min
                    ,ocorrencia.vl_profundidade_max
                    ,ocorrencia.de_identificador
                    ,ocorrencia.dt_identificacao
                    ,ocorrencia.no_identificacao_incerta
                    ,ocorrencia.ds_tombamento
                    ,ocorrencia.ds_instituicao_tombamento
                    ,ocorrencia.no_compilador
                    ,ocorrencia.dt_compilacao
                    ,ocorrencia.no_revisor
                    ,ocorrencia.dt_revisao
                    ,ocorrencia.no_validador
                    ,ocorrencia.dt_validacao
                    ,ocorrencia.ds_utilizado_avaliacao
                    ,ocorrencia.ds_nao_utilizado_avaliacao
                    ,ocorrencia.no_autor
                    ,ocorrencia.nu_ano_publicacao
                    ,ocorrencia.de_titulo_publicacao
                    ,ocorrencia.de_com_pessoal
                    ,'SALVE' as no_base_dados
                    ,ocorrencia.id_origem::text
                    ,ocorrencia.tx_observacao
                    FROM salve.vw_ficha_ocorrencia as ocorrencia
                    inner join salve.ficha on ficha.sq_ficha = ocorrencia.sq_ficha
                    INNER JOIN salve.ciclo_avaliacao as ciclo on ciclo.sq_ciclo_avaliacao = ficha.sq_ciclo_avaliacao
                    WHERE ( ocorrencia.in_utilizado_avaliacao = 'S' OR ocorrencia.in_utilizado_avaliacao IS NULL)
                    AND  ( ocorrencia.in_sensivel = 'N' or ocorrencia.in_sensivel IS NULL )
                    AND ciclo.in_publico = 'S'
                    AND ( ocorrencia.dt_carencia is null or ocorrencia.dt_carencia < '${dataAtual}' )
                    AND ocorrencia.sq_ficha in ( ${ fichasIds.join(',') } )
                    ) tmp
                    inner join salve.ficha on ficha.sq_ficha=tmp.sq_ficha
                    inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon

                    -- LER OCORRÊNCIAS DO PORTALBIO
                    UNION ALL

                    SELECT ficha.sq_ficha
                    ,ficha.nm_cientifico
                    ,st_x(op.ge_ocorrencia) AS nu_x
                    ,st_y(op.ge_ocorrencia) AS nu_y
                    ,null as  no_praxo_carencia
                    ,op.dt_carencia
                    ,null as no_datum
                    ,null as no_formato_original
                    ,case when left(tx_ocorrencia, 1)::text = '{' then op.tx_ocorrencia::json->>'precisaoCoord' else '' end as no_precisao
                    ,null as no_referencia_aproximacao
                    ,null as no_metodologia_aproximacao
                    ,null as tx_metodologia_aproximacao
                    ,null as no_taxon_citado
                    ,null as ds_presenca_atual
                    ,null as no_tipo_registro
                    ,null as nu_dia_registro
                    ,null as nu_mes_registro
                    ,null as nu_ano_registro
                    ,null as hr_registro
                    ,null as nu_dia_registro_fim
                    ,null as nu_mes_registro_fim
                    ,null as nu_ano_registro_fim
                    ,null as ds_data_desconhecida
                    ,op.no_local as no_localidade
                    ,null as ds_caracteristica_localidade
                    ,null as no_uc_federal
                    ,null as no_uc_estadual
                    ,null as no_rppn
                    ,null as no_pais
                    ,null as no_estado
                    ,null as no_municipio
                    ,null as no_ambiente
                    ,null as no_habitat
                    ,null as vl_elevacao_min
                    ,null as vl_elevacao_max
                    ,null as vl_profundidade_min
                    ,null as vl_profundidade_max
                    ,null as de_identificador
                    ,null as dt_identificacao
                    ,null as no_identificacao_incerta
                    ,null as ds_tombamento
                    ,null as ds_instituicao_tombamento
                    ,null as no_compilador
                    ,null as dt_compilacao
                    ,revisor.no_pessoa as no_revisor
                    ,op.dt_revisao
                    ,validador.no_pessoa as no_validador
                    ,op.dt_validacao
                    ,salve.snd(op.in_utilizado_avaliacao::text) AS ds_utilizado_avaliacao
                    ,CASE WHEN op.in_utilizado_avaliacao = 'N'::bpchar THEN salve.remover_html_tags(op.tx_nao_utilizado_avaliacao) ELSE ''::text END AS ds_nao_utilizado_avaliacao
                    ,null as no_autor
                    ,null as nu_ano_publicacao
                    ,null as de_titulo_publicacao
                    ,null as de_com_pessoal
                    ,op.no_instituicao as no_base_dados
                    ,case when op.id_origem is null then
                       case when left(tx_ocorrencia, 1)::text = '{' then op.tx_ocorrencia::json->>'uuid' else '' end
                       else op.id_origem::text end as id_origem
                    ,case when left(tx_ocorrencia, 1)::text = '{' then op.tx_ocorrencia::json->>'recordNumber' else '' end as tx_observacao
                    ,taxon.json_trilha->'reino'->>'no_taxon' as no_reino
                    ,taxon.json_trilha->'filo'->>'no_taxon' as no_filo
                    ,taxon.json_trilha->'classe'->>'no_taxon' as no_classe
                    ,taxon.json_trilha->'ordem'->>'no_taxon' as no_ordem
                    ,taxon.json_trilha->'familia'->>'no_taxon' as no_familia
                    ,taxon.json_trilha->'genero'->>'no_taxon' as no_genero
                    from salve.ficha_ocorrencia_portalbio  op
                    inner join salve.ficha on ficha.sq_ficha = op.sq_ficha
                    INNER JOIN salve.ciclo_avaliacao as ciclo on ciclo.sq_ciclo_avaliacao = ficha.sq_ciclo_avaliacao
                    inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                    left join salve.vw_pessoa as revisor on revisor.sq_pessoa = op.sq_pessoa_revisor
                    left join salve.vw_pessoa as validador on validador.sq_pessoa = op.sq_pessoa_validador
                    where op.in_utilizado_avaliacao = 'S'
                        AND ciclo.in_publico = 'S'
                        and ( op.dt_carencia is null or op.dt_carencia < '${dataAtual}' )
                        AND op.sq_ficha in ( ${ fichasIds.join(',') } )
                    ) res
                    order by res.nm_cientifico
                    """
            List listOcorrencias = sqlService.execSql(cmdSql)
            // adicionar italico no nome cientifico
            listOcorrencias.each {
                it.nm_cientifico = Util.ncItalico(it.nm_cientifico)
            }
            //println cmdSql
            PlanilhaExcel planilha = new PlanilhaExcel( res.data.fileName,'Ocorrências' )
            planilha.setData( listOcorrencias)
            planilha.addColumn( ['title':'Nome científico'                  , 'column':'nm_cientifico'] )
            planilha.addColumn( ['title':'Latitude'                         , 'column':'nu_y'   , 'type':'float'] )
            planilha.addColumn( ['title':'Longitude'                        , 'column':'nu_x'   , 'type':'float'] )
            //planilha.addColumn( ['title':'Prazo carência'                   , 'column':'no_prazo_carencia'] )
            //planilha.addColumn( ['title':'Fim carência'                     , 'column':'dt_carencia',   'type':'date'] )
            planilha.addColumn( ['title':'Datum'                            , 'column':'no_datum'] )
            planilha.addColumn( ['title':'Formato original'                 , 'column':'no_formato_original'] )
            planilha.addColumn( ['title':'Precisao da coordenada'           , 'column':'no_precisao'] )
            planilha.addColumn( ['title':'Referência aproximação'           , 'column':'no_referencia_aproximacao'] )
            planilha.addColumn( ['title':'Metodologia aproximação'          , 'column':'no_metodologia_aproximacao'] )
            planilha.addColumn( ['title':'Descrição do método aproximação'  , 'column':'tx_metodologia_aproximacao'] )
            planilha.addColumn( ['title':'Taxon citado'                     , 'column':'no_taxon_citado'] )
            planilha.addColumn( ['title':'Presença atual'                   , 'column':'ds_presenca_atual'] )
            planilha.addColumn( ['title':'Tipo registro'                    , 'column':'no_tipo_registro'] )
            planilha.addColumn( ['title':'Dia'                              , 'column':'nu_dia_registro'] )
            planilha.addColumn( ['title':'Mês'                              , 'column':'nu_mes_registro'] )
            planilha.addColumn( ['title':'Ano'                              , 'column':'nu_ano_registro'] )
            planilha.addColumn( ['title':'Hora'                             , 'column':'hr_registro'] )
            planilha.addColumn( ['title':'Dia fim'                          , 'column':'nu_dia_registro_fim'] )
            planilha.addColumn( ['title':'Mês fim'                          , 'column':'nu_mes_registro_fim'] )
            planilha.addColumn( ['title':'Ano fim'                          , 'column':'nu_ano_registro_fim'] )
            planilha.addColumn( ['title':'Data e Ano desconhecidos'         , 'column':'ds_data_desconhecida'] )
            planilha.addColumn( ['title':'Localidade'                       , 'column':'no_localidade'] )
            planilha.addColumn( ['title':'Característica da localidade'     , 'column':'ds_caracteristica_localidade'] )
            planilha.addColumn( ['title':'UC Federais'                      , 'column':'no_uc_federal'] )
            planilha.addColumn( ['title':'UC Não Federais'                  , 'column':'no_uc_estadual'] )
            planilha.addColumn( ['title':'RPPN'                             , 'column':'no_rppn'] )
            planilha.addColumn( ['title':'País'                             , 'column':'no_pais'] )
            planilha.addColumn( ['title':'Estado'                           , 'column':'no_estado'] )
            planilha.addColumn( ['title':'Município'                        , 'column':'no_municipio'] )
            planilha.addColumn( ['title':'Ambiente'                         , 'column':'no_ambiente'] )
            planilha.addColumn( ['title':'Hábitat'                          , 'column':'no_habitat'] )
            planilha.addColumn( ['title':'Elevação mínima'                  , 'column':'vl_elevacao_min', 'type':'float'] )
            planilha.addColumn( ['title':'Elevação máxima'                  , 'column':'vl_elevacao_max', 'type':'float'] )
            planilha.addColumn( ['title':'Profundidade mínima'              , 'column':'vl_profundidade_min', 'type':'float'] )
            planilha.addColumn( ['title':'Profundidade máxima'              , 'column':'vl_profundidade_max', 'type':'float'] )
            planilha.addColumn( ['title':'Identificador'                    , 'column':'de_identificador'] )
            planilha.addColumn( ['title':'Data identificação'               , 'column':'dt_identificacao',   'type':'date'] )
            planilha.addColumn( ['title':'Identificação incerta'            , 'column':'no_identificacao_incerta'] )
            planilha.addColumn( ['title':'Tombamento'                       , 'column':'ds_tombamento'] )
            planilha.addColumn( ['title':'Instituição tombamento'           , 'column':'ds_instituicao_tombamento'] )
            planilha.addColumn( ['title':'Compilador'                       , 'column':'no_compilador'] )
            planilha.addColumn( ['title':'Data compilação'                  , 'column':'dt_compilacao',   'type':'date'] )
            planilha.addColumn( ['title':'Revisor'                          , 'column':'no_revisor'] )
            planilha.addColumn( ['title':'Data revisão'                     , 'column':'dt_revisao',   'type':'date'] )
            planilha.addColumn( ['title':'Validador'                        , 'column':'no_validador'] )
            planilha.addColumn( ['title':'data validação'                   , 'column':'dt_validacao',   'type':'date'] )
            planilha.addColumn( ['title':'Autor'                            , 'column':'no_autor'] )
            planilha.addColumn( ['title':'Ano'                              , 'column':'nu_ano_publicacao'] )
            planilha.addColumn( ['title':'Título'                           , 'column':'de_titulo_publicacao'] )
            planilha.addColumn( ['title':'Comunicação Pessoal'              , 'column':'de_com_pessoal'] )
            planilha.addColumn( ['title':'Base dados'                       , 'column':'no_base_dados'] )
            planilha.addColumn( ['title':'Id Origem'                        , 'column':'id_origem'] )
            planilha.addColumn( ['title':'Observação'                       , 'column':'tx_observacao'] )

            if( ! planilha.run() ) {
                throw new Exception( planilha.getError() )
            }

            //sleep(3000 )
            res.msg = 'Exportação finalizada com SUCESSO!'

        } catch( Exception e) {
            println ' '
            println 'Ocorreu um erro na exportacao dos registros.'
            println e.getMessage()
            res.status  = 1
            res.msg     = e.getMessage()
            res.data    = []
        }
        render res as JSON
    }

    def searchTaxon() {
        Map resultado = [status:0,msg:'',data:[]]
        try {
            if ( ! params.sqNivelTaxonomico ) {
                throw new Exception('Grupo taxonômico não informado!')
            }

            NivelTaxonomico nivelTaxonomico = NivelTaxonomico.get( params.sqNivelTaxonomico.toLong() )
            if( !nivelTaxonomico ) {
                throw new Exception('Grupo taxonômico inválido!')
            }

            if ( ! params.noTaxonSearch ) {
                throw new Exception( nivelTaxonomico.deNivelTaxonomico+ ' não informado(a)!')
            }
            Long  sqNivelSearch = nivelTaxonomico.id.toLong()
            String coNivelSearch = nivelTaxonomico.coNivelTaxonomico.toLowerCase()
            String noTaxonSearch = params.noTaxonSearch.trim()
            noTaxonSearch = noTaxonSearch.replaceAll(/[^a-zA-Z-]/, ' ').replaceAll('/  /g', ' ')
            if ( noTaxonSearch.size() < 3 ) {
                throw new Exception('Informe pelo menos 3 letras!')
            }
            String where = ''
            if ( params.searchType ) {
                if( params.searchType == 'startWith') {
                    where +="""and json_trilha->:coNivelSearch->>'no_taxon' ilike ''||:noTaxonSearch||'%'"""
                } else if( params.searchType == 'hasWord') {
                    where += """and json_trilha->:coNivelSearch->>'no_taxon' ilike any (array['${noTaxonSearch} %', '% ${noTaxonSearch}', '% ${noTaxonSearch} %','${noTaxonSearch}'] )"""
                }
            }
            if ( !where ) {
                throw new Exception('Parâmetros insuficientes!')
            }
            String cmdSql="""select taxon.sq_taxon
            , (json_trilha->:coNivelSearch->>'no_taxon')::TEXT AS no_taxon
            , json_trilha
            , nivel_taxonomico.co_nivel_taxonomico
            from taxonomia.taxon
            inner join taxonomia.nivel_taxonomico on nivel_taxonomico.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
            where taxon.sq_nivel_taxonomico = :sqNivelSearch
            and taxon.st_ativo=true
            $where
            order by json_trilha->:coNivelSearch->>'no_taxon'
            limit 300 offset 0"""
            _getDataFromCache(cmdSql, [ coNivelSearch:coNivelSearch
                                        , sqNivelSearch:sqNivelSearch
                                        , noTaxonSearch:noTaxonSearch ]).each {
            /*sqlService.execSql( cmdSql, [ coNivelSearch:coNivelSearch
                                          , sqNivelSearch:sqNivelSearch
                                          , noTaxonSearch:noTaxonSearch ] ).each {*/
                //println it.no_taxon
                resultado.data.push( [ 'id' : it.sq_taxon
                                       , 'taxon' : it.no_taxon
                                       , 'text' : it.no_taxon
                ])
            }
        } catch( Exception e ) {
            resultado.status = 1
            resultado.msg = e.getMessage()
        }
        render resultado as JSON
    }

    /**
     * método para email do fale conosco
     *
     * */
    def faleConosco() {
        Map res = [status: 0, msg: '', data: []]
        try {
            if( ! params.mensagem ) {
                throw new Exception('Mensagem não informada!')
            }
            if( ! params.assunto ) {
                throw new Exception('Assunto não informada!')
            }

            if( params.reCaptchaToken ) {
                String remoteIp = request.getRemoteAddr()
                if( ! remoteIp ) {
                    remoteIp = request.getRemoteHost();
                }
                String captchaValidateUrl = "https://www.google.com/recaptcha/api/siteverify?secret=${captchaV3SecretKey}&response=${params.reCaptchaToken}&remoteip=${remoteIp}"
                d('    - reCaptchaToken:' + params.reCaptchaToken );
                d('    - remoteIp: ' + remoteIp )
                d('    - url verify: ' + captchaValidateUrl)
                JSONElement dados = requestService.doGetJson( captchaValidateUrl )
                if( ! dados || ! dados.success || ! dados.score || dados.score < 0.7 ) {
                    res.status = 1
                    res.text  = 'Consulta negada pelo recaptcha.'
                    res.msg = 'Você é um robô.'
                    res.type ='info'
                    res.data = []
                    render res as JSON
                    return;
                }
            } else {
                res.status = 1
                res.text  = 'Solicitação negada pela falta do token de validação.'
                res.msg = 'Erro reCaptcha'
                res.type ='error'
                res.data = []
                render res as JSON
                return;
            }
            params.mensagem = params.mensagem.replaceAll('\\n','-lf-')
            String de = params.nome ? "<p>De: <b>${params.nome}</b></p>" : ''
            String email = params.email ? "<p>Email: <b>${params.email.toLowerCase()}</b></p>" : ''
            String mensagem = '<hr><p>' + Util.stripTags(params.mensagem) + '</p>'
            mensagem = mensagem.replaceAll('-lf-','<br>')
            String emailMessage = "${de}${email}${mensagem}"
            try {
                emailService.sendTo('salve@icmbio.gov.br', 'Salve público - '+params.assunto, emailMessage,
                        params.email ? params.email.toLowerCase() : '')
                res.msg = 'Email enviado com SUCESSO.'
            } catch (Exception e) {
                res.status = 1;
                res.msg = e.getMessage()
            }
        } catch (Exception e) {
            res.status = 1;
            res.msg = e.getMessage()
        }
        render res as JSON
    }

    /**
     * metodo para retornar o link do anexo da ficha
     */
    def getAnexo() {
        if ( ! params.id) {
            response.sendError(404);
            return;
        }
        // deixar somente numeros no id porque poder ser postado 1245.zip no caso dos shp
        params.id = decryptStr( params.id.toString()).replaceAll(/[^0-9]/,'')
        FichaAnexo pa = FichaAnexo.get(params.id.toLong())
        if (!pa ) {
            response.sendError(404)
            return
        }
        String hashFileName = pa.deLocalArquivo
        Integer posicao = hashFileName.lastIndexOf('/')
        if( posicao > 0 )
        {
            hashFileName = hashFileName.substring(posicao+1)
            pa.deLocalArquivo = hashFileName
            pa.save(flush:true)
        }
        File file
        if( params.thumb )
        {
            file = new File(grailsApplication.config.anexos.dir+'thumbnail.'+hashFileName);
            if ( file.exists() )
            {
                hashFileName = grailsApplication.config.anexos.dir+'thumbnail.'+hashFileName
            }
            else
            {
                // exibir imagem thumbnail padrao
                String fotoPadrao = grailsApplication.config.arquivos.dir+'thumbnail.no-image.jpeg'
                file = new File( fotoPadrao )
            }
        }
        if( ! file )
        {
            file = new File( grailsApplication.config.anexos.dir+hashFileName )
        }
        if ( ! file.exists() )
        {
            // exibir imagem padrao
            String fotoPadrao = grailsApplication.config.arquivos.dir+'no-image.jpeg'
            file = new File( fotoPadrao )
            if ( ! file.exists() ) {
                response.sendError(400)
                return
            }
        }
        String fileType = pa.deTipoConteudo ? pa.deTipoConteudo : 'application/octet-stream';
        String fileName = pa.noArquivo.replaceAll(' |,|\\(.+\\)', '_').replaceAll('_+','-').replaceAll('-\\.','.')
        if( fileType )
        {
            render(file: file, fileName: fileName, contentType: fileType)
        }
        else
        {
            render(file: file, fileName: fileName)
        }
    }

    /**
     * metodo para retornar o link da imagem da especie
     */
    def getMultimidia() {
        if (!params.id) {
            response.sendError(404);
            return;
        }
        params.id = decryptStr( params.id.toString() ).replaceAll(/[^0-9]/,'')
        FichaMultimidia reg = FichaMultimidia.get( params.id.toLong() )
        if (! reg ) {
            response.sendError(404);
            return;
        }
        String mmDir = grailsApplication.config.multimidia.dir
        String hashFileName = reg.noArquivoDisco
        Integer posicao = hashFileName.lastIndexOf('/')
        if( posicao > 0 )
        {
            hashFileName = hashFileName.substring(posicao+1)
            reg.noArquivoDisco = hashFileName
            reg.save(flush:true)
        }
        File file
        if( params.thumb )
        {
            file = new File(mmDir+'thumbnail.'+hashFileName);
            if ( file.exists() )
            {
                hashFileName = mmDir+'thumbnail.'+hashFileName
            }
            else
            {
                file=null
            }
        }
        if( ! file )
        {
            file = new File( mmDir+hashFileName )
        }
        if ( ! file.exists() )
        {
            response.sendError(400)
            return
        }
        String fileName = reg.noArquivo.replaceAll(' |,|\\(.+\\)', '_').replaceAll('_+','-').replaceAll('-\\.','.')
        String fileExtension = Util.getFileExtension(fileName)
        String fileType = 'application/octet-stream';
        if( reg.tipo.codigoSistema=='IMAGEM')
        {
            fileType = 'image/'+fileExtension
        }
        else if( reg.tipo.codigoSistema=='SOM')
        {
            fileType = 'audio/mp3'
        }
        if( fileType )
        {
            render(file: file, fileName: fileName, contentType: fileType)
        }
        else
        {
            render(file: file, fileName: fileName)
        }
    }

    /**
     * método para sortear as imagens das especies em destaque
     * @return
     */
    def getRandomImagesDestaque() {
        Map res = [status:0,msg:'',data:[]]
        int qtd_images = 20
        String sqlCmd
        try {
            if (params.qtd) {
                qtd_images = params.qtd.toInteger()
            }
            DadosApoio tipoImagem = dadosApoioService.getByCodigo('TB_TIPO_MULTIMIDIA','IMAGEM')
            DadosApoio situacaoPublicada = dadosApoioService.getByCodigo('TB_SITUACAO_FICHA','PUBLICADA')
            DadosApoio situacaoReprovada = dadosApoioService.getByCodigo('TB_SITUACAO_MULTIMIDIA','REPROVADA')
            String  categoriasAmeacadas = dadosApoioService.getCategoriasAmeacadas()?.id?.join(',')

            sqlCmd = """SELECT ficha_multimidia.sq_ficha, ficha_multimidia.sq_ficha_multimidia, ficha_multimidia.no_autor, ficha_multimidia.de_email_autor, ficha.nm_cientifico, nome_comum.no_comum
            ,case when ficha.sq_situacao_ficha = ${ situacaoPublicada.id.toString() } then true else false end as st_publicada
            FROM salve.ficha_multimidia
            INNER JOIN salve.mv_dados_modulo_publico as ficha on ficha.sq_ficha = ficha_multimidia.sq_ficha
            LEFT JOIN LATERAL(
                    select nome_comum.no_comum from salve.ficha_nome_comum nome_comum
                    WHERE nome_comum.sq_ficha = ficha.sq_ficha limit 1
                    ) as nome_comum on true
            WHERE sq_tipo = ${tipoImagem.id.toString() } -- imagem
              AND ficha.sq_grupo_salve is not null
            -- AND ficha.sq_situacao_ficha = ${ situacaoPublicada.id.toString() } -- publicadas
            -- and ficha.sq_categoria_final in ( ${ categoriasAmeacadas } ) -- ameacadas
            AND in_destaque = true
            AND sq_situacao <> ${situacaoReprovada.id.toString() } -- nao reprovada
            ORDER BY random()
            LIMIT ${qtd_images.toString()}"""
/** /
println ' '
println 'DESTAQUES'
println sqlCmd
/**/

            res.data = sqlService.execSql(sqlCmd)
            // encryptar os ids
            res.data.each {
                it.sq_ficha = encryptStr(it.sq_ficha.toString() )
                it.sq_ficha_multimidia = encryptStr(it.sq_ficha_multimidia.toString() )
                it.nm_cientifico = Util.ncItalico(it.nm_cientifico,true,true);
            }
        } catch ( Exception e ) {
            res.status=1
            res.msg=e.getMessage()
        }
        render res as JSON
    }

    /**
     * método para limpar os dados em cache no diretóri /data/salve-estadual/temp
     */
    def clearCache() {
        def dir = new File( _cacheDir() )
        def files = []
        if( dir.isDirectory() ) {
            dir.traverse(type: FILES, maxDepth: 0) { files.add(it) }
            files.each {
                if (it.toString() =~ /public-.+\.cache$/) {
                    if (!it.delete()) {
                        println 'erro ao excluir o arquivo ' + it.toString()
                    }
                }
            }
        }
        //redirect(uri: grailsApplication.config.url.sicae + '/usuario/logout?url-back=' + urlCallback)
        //redirect(uri:"http://www.icmbio.gov.br")
        //chain( action:'index',params:[cacheCleared:true] )
        flash.message = "Cache limpo!"
        chain( action:'index')
    }

    /**
     * método utilizado na pesquisa das Unidades de conservação
     * @return
     */
    def searchUcs() {
        Map res = [status: 0, msg: '', type: 'success', data:[]]
        try {
            Map pars = [q: '%' + params.q + '%', inEsfera: 'F']
            List where=[]
            // UCS FEDERAIS
            if( params.tipoUc ) {
                where.push('in_esfera = :inEsfera')
                if ( params.tipoUc == 'federal') {
                    pars.inEsfera = 'F'
                } else if ( params.tipoUc == 'estadual') {
                    pars.inEsfera = 'E'
                } else if (params.tipoUc == 'rppn') {
                    pars.inEsfera = 'R'
                }
            }
            if( params.q ) {
                where.push("public.fn_remove_acentuacao( concat(no_uc,' ',sg_unidade)  ) ilike :q")
                pars.q = '%' + Util.removeAccents(params).q + '%'
            }

            // retornar as colunas no formato do select2 id e text
            String cmdSql = """select sq_uc as id, case when sg_unidade is null or sg_unidade = '' then no_uc else sg_unidade end as text
                                from salve.vw_uc
            ${ where.size() > 0 ? ' WHERE ' + where.join(' AND ') : ''}
                                order by no_uc"""

            // executar a query ou ler do cache
            res.data = _getDataFromCache(cmdSql, pars)

            // ajustar os nomes para iniciais em maiúsculas
            //res.data.each { it.text = Util.capitalize(it.text) }
        } catch ( Exception e) {
            println e.getMessage()
            res.status  = 1
            res.type    = 'error'
            res.data    = [ ['id':0,'text' : 'Erro na consulta'] ]
        }
        render res as JSON
    }

    /**
     * método utilizado na pesquisa das Ameaças
     * @return
     */
    def searchAmeacas() {
        Map res = [status: 0, msg: '', type: 'success', data:[]]
        List where=[]
        try {
            Map pars = [q: '%' + params.q + '%']
            if( params.q ) {
                where.push("public.fn_remove_acentuacao(trilha) ilike :q")
                pars.q = '%' + Util.removeAccents( params.q ) + '%'
            }

            // retornar as colunas no formato do select2 id e text
            String cmdSql = """select id, descricao_completa as text, trilha as hint from salve.vw_ameacas
            ${ where.size() > 0 ? ' WHERE ' + where.join(' AND ') : ''}
            order by ordem"""

            // executar a query ou ler do cache
            res.data = _getDataFromCache(cmdSql, pars)

            // ajustar os nomes para iniciais em maiúsculas
            res.data.each { it.text = Util.capitalize(it.text) }
        } catch (Exception e ) {
            println e.getMessage()
            res.status  = 1
            res.type    = 'error'
            res.data    = [ ['id':0,'text' : 'Erro na consulta'] ]
        }
        render res as JSON
    }

    /**
     * método utilizado na pesquisa das Usos
     * @return
     */
    def searchUsos() {
        Map res = [status: 0, msg: '', type: 'success', data:[]]
        List where=[]
        try {
            Map pars = [:]
            if( params.q ) {
                where.push("public.fn_remove_acentuacao(trilha) ilike :q")
                pars.q = '%' + Util.removeAccents( params.q ) + '%'
            }

            // retornar as colunas no formato do select2 id e text
            String cmdSql = """select id, descricao_completa as text, trilha as hint from salve.vw_usos
            ${ where.size() > 0 ? ' WHERE ' + where.join(' AND ') : ''}
            order by ordem"""


            // executar a query ou ler do cache
            //println cmdSql
            //println pars
            //res.data = sqlService.execSql( cmdSql, pars )
            res.data = _getDataFromCache(cmdSql, pars)

            // ajustar os nomes para iniciais em maiúsculas
            res.data.each { it.text = Util.capitalize(it.text) }
        } catch (Exception e ) {
            println e.getMessage()
            res.status  = 1
            res.type    = 'error'
            res.data    = [ ['id':0,'text' : 'Erro na consulta'] ]
        }
        render res as JSON
    }

    /**
     * método utilizado na pesquisa das Bacias
     * @return
     */
    def searchBacias() {
        Map res = [status: 0, msg: '', type: 'success', data:[]]
        List where=[]
        try {
            Map pars = [:]
            if( params.q ) {
                where.push("public.fn_remove_acentuacao(trilha) ilike :q")
                pars.q = '%' + Util.removeAccents( params.q ) + '%'
            }

            // retornar as colunas no formato do select2 id e text
            String cmdSql = """select id, descricao_completa as text, trilha as hint from salve.vw_bacia_hidrografica
            ${ where.size() > 0 ? ' WHERE ' + where.join(' AND ') : ''}
            order by ordem"""
            res.data = _getDataFromCache(cmdSql, pars)

            // ajustar os nomes para iniciais em maiúsculas
            res.data.each { it.text = Util.capitalize(it.text) }
        } catch (Exception e ) {
            println e.getMessage()
            res.status  = 1
            res.type    = 'error'
            res.data    = [ ['id':0,'text' : 'Erro na consulta'] ]
        }
        render res as JSON
    }

    /**
     * método utilizado na pesquisa dos Planos de Ação Nacional- PAN
     * @return
     */
    def searchPans() {
        Map res = [status: 0, msg: '', type: 'success', data:[]]
        List where=[]
        try {
            Map pars = [:]
            if( params.q ) {
                where.push("public.fn_remove_acentuacao( concat( tx_plano_acao,' ',sg_plano_acao ) ) ilike :q")
                pars.q = '%' + Util.removeAccents( params.q ) + '%'
            }
            // retornar as colunas no formato do select2 id e text
            String cmdSql = """select sq_plano_acao as id, coalesce( sg_plano_acao, tx_plano_acao) as text from salve.plano_acao
            ${ where.size() > 0 ? ' WHERE ' + where.join(' AND ') : ''}
             order by text"""
            res.data = _getDataFromCache(cmdSql, pars)

            // ajustar os nomes para iniciais em maiúsculas
            res.data.each { it.text = Util.capitalize(it.text) }
        } catch (Exception e ) {
            println e.getMessage()
            res.status  = 1
            res.type    = 'error'
            res.data    = [ ['id':0,'text' : 'Erro na consulta'] ]
        }
        render res as JSON
    }

    /**
     * método para pesquisar os participantes das oficinas/colaboradores
     * - coordenadores de taxon, pontos focais ou equipe de apoio
     */
    def searchParticipantes() {
        Map res = [status: 0, msg: '', type: 'success', data:[]]
        List where=[]
        try {
            if( ! params.papel ) {
                throw new Exception('Papel participante não informado!')
            }
            Map pars = [:]
            DadosApoio papel = dadosApoioService.getByCodigo('TB_PAPEL_FICHA',params.papel)
            if( ! papel ) {
                throw new Exception('Papel participante ' + params.papel+' inexistente')
            }
            d('PESQUISAR PARTICIPANTE')
            d( '  - papel: ' + papel.codigoSistema )

            // Equipe de apoio é toda pessoa com perfil colaborador interno no SICA-e
            String cmdSql = ''

            if( papel.codigoSistema =~ /xPONTO_FOCAL|xCOORDENADOR_TAXON/) {
                cmdSql = """select final.no_pessoa
                        ,array_to_string(array_agg(DISTINCT coalesce(final.sg_unidade_org,wi.sg_instituicao)  ),', ') as no_unidade
                        ,array_to_string(array_agg(DISTINCT final.de_email),', ') as de_email
                        ,array_to_string(array_agg(DISTINCT final.ds_grupo),', ') as no_grupo
                        from (
                            select pessoa.no_pessoa
                            ,case when email.tx_email is not null then ui.sg_unidade_org else ue.sg_instituicao end  as sg_unidade_org
                            ,case when email.tx_email is null then usre.tx_email else email.tx_email end  as de_email
                            ,array_to_string(array_agg(DISTINCT grupo.ds_dados_apoio),', ') as ds_grupo
                            from (
                                select ficha.sq_taxon, fp.sq_papel, max( ficha.sq_ciclo_avaliacao) as sq_ciclo_avaliacao
                                from salve.ficha_pessoa fp
                                inner join salve.ficha on ficha.sq_ficha = fp.sq_ficha
                                inner join salve.ciclo_avaliacao on ciclo_avaliacao.sq_ciclo_avaliacao = ficha.sq_ciclo_avaliacao
                                where fp.in_ativo = 'S'
                                and fP.sq_papel = ${papel.id.toString() }
                                and ciclo_avaliacao.in_publico = 'S'
                                and fp.sq_grupo is not null
                                -- desconsiderar as fichas que foram para o proximo ciclo e mudaram de taxon e estão com PF ou CT atribuidos
                                and fp.sq_ficha not in (
                                        select ficha_1.sq_ficha_ciclo_anterior from salve.ficha_pessoa x
                                        inner join salve.ficha ficha_1 on ficha_1.sq_ficha = x.sq_ficha
                                        where x.in_ativo = 'S'
                                        and x.sq_papel = ${papel.id.toString() }
                                        and ficha_1.sq_ficha_ciclo_anterior is not null
                                )
                                group by ficha.sq_taxon, fp.sq_papel
                            ) motor
                            inner join salve.ficha on ficha.sq_taxon = motor.sq_taxon and ficha.sq_ciclo_avaliacao = motor.sq_ciclo_avaliacao
                            inner join salve.ficha_pessoa on ficha_pessoa.sq_ficha = ficha.sq_ficha and ficha_pessoa.sq_papel = motor.sq_papel
                            inner join salve.vw_pessoa pessoa on pessoa.sq_pessoa = ficha_pessoa.sq_pessoa
                            inner join salve.dados_apoio as grupo on grupo.sq_dados_apoio = ficha_pessoa.sq_grupo

                            -- instituicao da ficha
                            inner join salve.vw_unidade_org as ui on ui.sq_pessoa = ficha.sq_unidade_org
                            -- instituicao externa
                            LEFT JOIN salve.web_instituicao ue ON ue.sq_web_instituicao = ficha_pessoa.sq_web_instituicao
                            -- email instituicaon
                            LEFT OUTER JOIN salve.vw_email email ON email.sq_pessoa = ficha_pessoa.sq_pessoa and email.sq_tipo_email = 2
                            -- usuario externo
                            left outer join sicae.vw_usuario_externo usre on usre.sq_usuario_externo = ficha_pessoa.sq_pessoa

                            where ficha_pessoa.in_ativo = 'S'
                            and ficha_pessoa.sq_grupo is not null
                            group by pessoa.no_pessoa,
                                ui.sg_unidade_org,
                                ue.sg_instituicao,
                                email.tx_email,
                                usre.tx_email
                            ) as final

                        left outer join salve.web_usuario wu on wu.de_email = final.de_email
                        left outer join salve.web_instituicao wi on wi.sq_web_instituicao = wu.sq_web_instituicao
                        where final.de_email is not null
                        group by final.no_pessoa
                        order by public.fn_remove_acentuacao(final.no_pessoa)
                """
                /** /
                 println ' '
                 println 'PONTOS FOCAIS E COORDENADORES DE TAXON'
                 println cmdSql
                 /**/
            } else  if( papel.codigoSistema == 'PONTO_FOCAL' ) {
                cmdSql = """SELECT final.no_pessoa ,
                               array_to_string(array_agg(DISTINCT final.sg_unidade_org),', ') AS no_unidade,
                               array_to_string(array_agg(DISTINCT final.de_email),', ') AS de_email ,
                               array_to_string(array_agg(DISTINCT final.ds_grupo),', ') AS no_grupo
                        FROM
                          (SELECT pessoa.no_pessoa ,
                                  ui.sg_unidade_org,
                                  email.tx_email as de_email,
                                  array_to_string(array_agg(DISTINCT grupo.ds_dados_apoio),', ') AS ds_grupo
                           FROM
                             ( SELECT ficha.sq_taxon,
                                      fp.sq_papel,
                                      max(ficha.sq_ciclo_avaliacao) AS sq_ciclo_avaliacao
                              FROM salve.ficha_pessoa fp
                              INNER JOIN salve.ficha ON ficha.sq_ficha = fp.sq_ficha
                              INNER JOIN salve.ciclo_avaliacao ON ciclo_avaliacao.sq_ciclo_avaliacao = ficha.sq_ciclo_avaliacao
                              WHERE fp.in_ativo = 'S'
                                AND fp.sq_papel = ${papel.id.toString()}
                                AND ciclo_avaliacao.in_publico = 'S'
                                AND fp.sq_grupo IS NOT NULL
                                -- desconsiderar as fichas que foram para o proximo ciclo e mudaram de taxon e estão com PF ou CT atribuidos
                                AND fp.sq_ficha NOT IN
                                  (SELECT ficha_1.sq_ficha_ciclo_anterior
                                   FROM salve.ficha_pessoa x
                                   INNER JOIN salve.ficha ficha_1 ON ficha_1.sq_ficha = x.sq_ficha
                                   WHERE x.in_ativo = 'S'
                                     AND x.sq_papel = ${papel.id.toString()}
                                     AND ficha_1.sq_ficha_ciclo_anterior IS NOT NULL )
                              GROUP BY ficha.sq_taxon,
                                       fp.sq_papel) motor
                           INNER JOIN salve.ficha ON ficha.sq_taxon = motor.sq_taxon
                           AND ficha.sq_ciclo_avaliacao = motor.sq_ciclo_avaliacao
                           INNER JOIN salve.ficha_pessoa ON ficha_pessoa.sq_ficha = ficha.sq_ficha
                           AND ficha_pessoa.sq_papel = motor.sq_papel
                           INNER JOIN salve.vw_pessoa pessoa ON pessoa.sq_pessoa = ficha_pessoa.sq_pessoa
                           INNER JOIN salve.dados_apoio AS grupo ON grupo.sq_dados_apoio = ficha_pessoa.sq_grupo
                           -- instituicao da ficha
                           INNER JOIN salve.vw_unidade_org AS ui ON ui.sq_pessoa = ficha.sq_unidade_org -- instituicao externa
                           -- email corporativo
                           LEFT OUTER JOIN salve.vw_email email ON email.sq_pessoa = ficha_pessoa.sq_pessoa
                           WHERE ficha_pessoa.in_ativo = 'S'
                             AND ficha_pessoa.sq_grupo IS NOT NULL
                             -- email instituicional
                             AND email.sq_tipo_email = 2
                           GROUP BY pessoa.no_pessoa,
                                    ui.sg_unidade_org,
                                    email.tx_email
                          ) AS FINAL
                        WHERE final.de_email IS NOT NULL
                        GROUP BY final.no_pessoa
                        ORDER BY public.fn_remove_acentuacao(final.no_pessoa)
                    """

             }
             else if( papel.codigoSistema == 'COORDENADOR_TAXON' ) {
                cmdSql = """SELECT final.no_pessoa ,
                            array_to_string(array_agg(DISTINCT final.sg_instituicao),', ') AS no_unidade,
                            array_to_string(array_agg(DISTINCT final.de_email),', ') AS de_email ,
                            array_to_string(array_agg(DISTINCT final.ds_grupo),', ') AS no_grupo
                            FROM
                          (SELECT pessoa.no_pessoa ,
                                  wi.sg_instituicao,
                                  coalesce( email.tx_email, ue.tx_email ) as de_email,
                                  array_to_string(array_agg(DISTINCT grupo.ds_dados_apoio),', ') AS ds_grupo
                           FROM
                             ( SELECT ficha.sq_taxon,
                                      fp.sq_papel,
                                      max(ficha.sq_ciclo_avaliacao) AS sq_ciclo_avaliacao
                              FROM salve.ficha_pessoa fp
                              INNER JOIN salve.ficha ON ficha.sq_ficha = fp.sq_ficha
                              INNER JOIN salve.ciclo_avaliacao ON ciclo_avaliacao.sq_ciclo_avaliacao = ficha.sq_ciclo_avaliacao
                              WHERE fp.in_ativo = 'S'
                                AND fp.sq_papel = ${papel.id.toString()}
                                AND ciclo_avaliacao.in_publico = 'S'
                                AND fp.sq_grupo IS NOT NULL -- desconsiderar as fichas que foram para o proximo ciclo e mudaram de taxon e estão com PF ou CT atribuidos
                                AND fp.sq_ficha NOT IN
                                  ( SELECT ficha_1.sq_ficha_ciclo_anterior
                                   FROM salve.ficha_pessoa x
                                   INNER JOIN salve.ficha ficha_1 ON ficha_1.sq_ficha = x.sq_ficha
                                   WHERE x.in_ativo = 'S'
                                     AND x.sq_papel = ${papel.id.toString()}
                                     AND ficha_1.sq_ficha_ciclo_anterior IS NOT NULL )
                              GROUP BY ficha.sq_taxon,
                                       fp.sq_papel) motor
                           INNER JOIN salve.ficha ON ficha.sq_taxon = motor.sq_taxon
                           AND ficha.sq_ciclo_avaliacao = motor.sq_ciclo_avaliacao
                           INNER JOIN salve.ficha_pessoa ON ficha_pessoa.sq_ficha = ficha.sq_ficha AND ficha_pessoa.sq_papel = motor.sq_papel
                           INNER JOIN salve.vw_pessoa pessoa ON pessoa.sq_pessoa = ficha_pessoa.sq_pessoa
                           INNER JOIN salve.dados_apoio AS grupo ON grupo.sq_dados_apoio = ficha_pessoa.sq_grupo
                           -- instituicao web
                           INNER JOIN salve.web_instituicao wi on wi.sq_web_instituicao = ficha_pessoa.sq_web_instituicao
                           -- email corporativo
                           LEFT JOIN lateral (
                                select vw_email.tx_email from salve.vw_email
                                where vw_email.sq_pessoa = ficha_pessoa.sq_pessoa
                                limit 1
                           ) email on true
                           -- usuario externo para pegar o e-mail
                           left outer join sicae.vw_usuario_externo ue on ue.sq_usuario_externo = ficha_pessoa.sq_pessoa
                           WHERE ficha_pessoa.in_ativo = 'S'
                             AND ficha_pessoa.sq_grupo IS NOT NULL
                           GROUP BY pessoa.no_pessoa,
                                    wi.sg_instituicao,
                                    email.tx_email, ue.tx_email
                          ) AS FINAL
                        GROUP BY final.no_pessoa
                        ORDER BY public.fn_remove_acentuacao(final.no_pessoa)
                    """
            }
             else if( papel.codigoSistema == 'COLABORADOR_EXTERNO' ) {
                 // somente os bolsistas com perfil sicae colaborador interno
                 // select * from sicae.perfil where no_perfil = 'Colaborador Interno' and sq_sistema=24
                 // select * from sicae.vw_sistema where sg_sistema = 'SALVE'

                 cmdSql = """select motor.no_pessoa
                             , motor.de_email
                             , motor.no_unidade
                             , array_to_string(array_agg( distinct grupo.ds_dados_apoio ),', ') as no_grupo
                         from (
                           SELECT distinct vw_pessoa_fisica.no_pessoa,
                            vw_email.tx_email as de_email,
                            vw_unidade_org.sq_pessoa as sq_unidade_org,
                            vw_unidade_org.sg_unidade_org as no_unidade
                           FROM sicae.vw_usuario_perfil_int_ext
                             inner JOIN salve.vw_pessoa_fisica ON vw_usuario_perfil_int_ext.sq_pessoa = vw_pessoa_fisica.sq_pessoa
                             inner JOIN salve.vw_unidade_org ON vw_usuario_perfil_int_ext.sq_pessoa_uog_usuario_perfil = vw_unidade_org.sq_pessoa
                             inner JOIN salve.vw_email ON vw_email.sq_pessoa = vw_usuario_perfil_int_ext.sq_pessoa
                          WHERE vw_usuario_perfil_int_ext.sq_sistema = 24
                          AND vw_email.sq_tipo_email = 2
                          -- PERFIL COLABORADOR INTERNO
                          and vw_usuario_perfil_int_ext.sq_perfil = 109
                          and vw_email.tx_email::text  ~* 'bolsista|terceirizado'
                        ) as motor
                        inner join salve.vw_ficha ficha on ficha.sq_unidade_org = motor.sq_unidade_org
                        inner join salve.dados_apoio grupo on grupo.sq_dados_apoio = ficha.sq_grupo
                        group by motor.no_pessoa
                             , motor.de_email
                             , motor.no_unidade
                        order by public.fn_remove_acentuacao(motor.no_pessoa)
                             , motor.de_email
                             , motor.no_unidade"""
                /** /
                println ' '
                println 'COLABORADOR_INTERNO'
                println cmdSql
                /**/

            } else {
                // retornar as colunas no formato do select2 id e text
                //println 'PAPEL ' + papel.codigoSistema
                cmdSql = """select nivel1.sq_pessoa
                        , nivel1.no_pessoa
                        , nivel1.no_unidade
                        , nivel1.no_unidade_web
                        , nivel1.no_grupo
                        , Array_to_string(Array_agg(DISTINCT vw_email.tx_email ), ', ') AS de_email
                        , Array_to_string(Array_agg(DISTINCT vw_usuario_externo.tx_email ), ', ') AS de_email_externo
                        from (
                            select motor.sq_pessoa
                            ,pessoa.no_pessoa
                            ,Array_to_string(Array_agg( DISTINCT vw_unidade_org.sg_unidade_org ), ', ') AS no_unidade
                            ,Array_to_string(Array_agg( DISTINCT web_instituicao.no_instituicao ), ', ') AS no_unidade_web
                            ,Array_to_string(Array_agg( DISTINCT grupo.ds_dados_apoio ), ', ') AS no_grupo
                            from (
                                select DISTINCT ficha_pessoa.sq_pessoa, ficha.sq_unidade_org
                                , ficha_pessoa.sq_grupo, ficha_pessoa.sq_web_instituicao
                                from salve.ficha_pessoa
                                inner join salve.mv_dados_modulo_publico as ficha on ficha.sq_ficha = ficha_pessoa.sq_ficha
                                where ficha_pessoa.sq_papel = ${papel.id.toString()}
                                and ficha_pessoa.in_ativo = 'S'
                            ) as motor
                            INNER JOIN salve.vw_pessoa pessoa on pessoa.sq_pessoa = motor.sq_pessoa
                            INNER JOIN salve.vw_unidade_org on vw_unidade_org.sq_pessoa = motor.sq_unidade_org
                            LEFT JOIN salve.web_instituicao on web_instituicao.sq_web_instituicao = motor.sq_web_instituicao
                            LEFT JOIN salve.dados_apoio grupo on grupo.sq_dados_apoio = motor.sq_grupo
                            GROUP BY motor.sq_pessoa, pessoa.no_pessoa
                        ) as nivel1
                        LEFT OUTER JOIN salve.vw_email on vw_email.sq_pessoa = nivel1.sq_pessoa
                        LEFT OUTER JOIN sicae.vw_usuario_externo on vw_usuario_externo.sq_usuario_externo = nivel1.sq_pessoa
                        where vw_email.tx_email ilike '%bolsista%'
                        GROUP BY nivel1.sq_pessoa, nivel1.no_pessoa, nivel1.no_unidade, nivel1.no_unidade_web, nivel1.no_grupo
                        ORDER BY 2
                        """
                /** /
                println ' '
                println papel.codigoSistema
                println cmdSql
                /**/

            }
            // executar a query ou ler do cache
            /** /
            println ' '
            println 'SearchParticipantes()'
            println cmdSql
            /**/
            //res.data = sqlService.execSql( cmdSql, pars )
            res.data = _getDataFromCache(cmdSql, pars)

            // ajustar os nomes para iniciais em maiúsculas
            res.data.each {
                it.no_pessoa    = Util.capitalize( it.no_pessoa )
                if( papel.codigoSistema == 'COORDENADOR_TAXON' ) {
                    //it.no_unidade   = it.no_unidade_web ?: it.no_unidade
                    //it.de_email     = it.de_email_externo ?: it.de_email
                    //it.de_email     = it.de_email.toLowerCase()
                } else {
                    //it.no_unidade = it.no_unidade
                    it.de_email   = it.de_email.toLowerCase()
                }
                // tratamento do campo e-mail para exibir somente preferencialmente o do icmbio.gov.br
                if( it.de_email && it.de_email.indexOf('@icmbio' ) > 0 ) {
                    String emailIcmbio=''
                    it.de_email.split(',').each {
                        if( it.indexOf('@icmbio') > 0 ) {
                            emailIcmbio = it.trim()
                        }
                    }
                    it.de_email=emailIcmbio
                }
            }
        } catch (Exception e ) {
            println e.getMessage()
            res.status  = 1
            res.type    = 'error'
            res.data    = [ ['id':0,'text' : 'Erro na consulta'] ]
        }
        render res as JSON
    }


    /**
     * método para calcular os totais gerais exibido na seção Salve em Numeros
     * @return
     */
    def salveNumeros() {
        Map res = [status: 1, msg: 'Ok', type: 'error', data: [:]]
        //NivelTaxonomico nivelEspecie    = NivelTaxonomico.findByCoNivelTaxonomico('ESPECIE')
        //NivelTaxonomico nivelSubespecie = NivelTaxonomico.findByCoNivelTaxonomico('SUBESPECIE')
        String cmdSql
        try {
            cmdSql ="""select mv.cd_categoria_final as cd_categoria
                         ,concat( mv.ds_categoria_final, ' (',mv.cd_categoria_final,')') as no_categoria_completa
                         ,mv.de_categoria_final_ordem as de_ordem
                         ,coalesce(mv.cd_grupo_salve_sistema,'SG') as cd_grupo
                         ,coalesce(mv.ds_grupo_salve,'Sem grupo') as no_grupo
                         from salve.mv_dados_modulo_publico mv
                         where mv.cd_categoria_final is not null
                         order by mv.de_categoria_final_ordem, mv.cd_categoria_final
                        """

            Map graficoCategorias           = [:]
            Map graficoCategoriasAmeacadas  = [:]
            Map graficoCategoriasExtintas   = [:] // EX e RE

            Map graficoGrupos               = [:]
            Map graficoGruposAmeacadas      = [:]
            Map graficoGruposExtintas       = [:] // EX e RE

            _getDataFromCache( cmdSql ).each { row ->
                String keyCategoria = row.no_categoria_completa
                String keyGrupo = row.no_grupo

                if( keyCategoria ) {
                    if (!graficoCategorias) {
                        graficoCategorias = [title: 'Categorias', data: [], total: 1]
                    } else {
                        graficoCategorias.total += 1
                    }
                    // procurar a keyCategoria no array data do grupo ['VU':0,'EN':0 ...]
                    Map item = graficoCategorias.data.find { mapTemp ->
                        return mapTemp.containsKey(keyCategoria)
                    }
                    // se encontrou, acumula o total da categoria
                    if (item) {
                        item[keyCategoria].total += 1
                    } else {
                        // criar o item da categoria
                        Map map = [:]
                        map.put(keyCategoria, [total: 1, categoria: row.cd_categoria, color: _getPieColor(row.cd_categoria)])
                        graficoCategorias.data.push(map)
                    }
                    if (row.cd_categoria =~ /^(VU|EN|CR)$/) {
                        // calcular dados das ameaçadas
                        if (!graficoCategoriasAmeacadas) {
                            graficoCategoriasAmeacadas = [title: 'Categorias', data: [], total: 1]
                        } else {
                            graficoCategoriasAmeacadas.total += 1
                        }
                        // procurar a keyCategoria no array data do grupo ['VU':0,'EN':0 ...]
                        Map itemAmeacada = graficoCategoriasAmeacadas.data.find { mapTemp ->
                            return mapTemp.containsKey(keyCategoria)
                        }
                        // se encontrou, acumula o total da categoria
                        if (itemAmeacada) {
                            itemAmeacada[keyCategoria].total += 1
                        } else {
                            // criar o item da categoria
                            Map map = [:]
                            map.put(keyCategoria, [total: 1, categoria: row.cd_categoria, color: _getPieColor(row.cd_categoria)])
                            graficoCategoriasAmeacadas.data.push(map)
                        }
                    }
                }

                // grupos
                if( keyGrupo ) {
                    if (!graficoGrupos) {
                        graficoGrupos = [title: 'Grupos', data: [], total: 1]
                    } else {
                        graficoGrupos.total += 1
                    }
                    // procurar a keyGrupo no array data do grupo
                    Map itemGrupo = graficoGrupos.data.find { mapTemp ->
                        return mapTemp.containsKey(keyGrupo)
                    }
                    // se encontrou, acumula o total do grupo
                    if (itemGrupo) {
                        itemGrupo[keyGrupo].total += 1
                    } else {
                        // criar o item
                        Map map = [:]
                        map.put(keyGrupo, [total: 1, categoria: row.cd_grupo, color: _getPieColor(row.cd_grupo)])
                        graficoGrupos.data.push(map)
                    }


                    // grupos ameaçadas
                    if ( row.cd_categoria =~ /^(VU|EN|CR)$/) {
                        if (!graficoGruposAmeacadas) {
                            graficoGruposAmeacadas = [title: 'Grupos', data: [], total: 1]
                        } else {
                            graficoGruposAmeacadas.total += 1
                        }
                        // procurar a keyGrupo no array data do grupo
                        Map itemGrupoAmeacadas = graficoGruposAmeacadas.data.find { mapTemp ->
                            return mapTemp.containsKey(keyGrupo)
                        }
                        // se encontrou, acumula o total do grupo
                        if (itemGrupoAmeacadas) {
                            itemGrupoAmeacadas[keyGrupo].total += 1
                        } else {
                            // criar o item
                            Map map = [:]
                            map.put(keyGrupo, [total: 1, categoria: row.cd_grupo, color: _getPieColor(row.cd_grupo)])
                            graficoGruposAmeacadas.data.push(map)
                        }
                    }

                    // grupos extintas
                    if ( row.cd_categoria =~ /^(EX|RE)$/) {
                        if (!graficoGruposExtintas) {
                            graficoGruposExtintas = [title: 'Grupos', data: [], total: 1]
                        } else {
                            graficoGruposExtintas.total += 1
                        }
                        // procurar a keyGrupo no array data do grupo
                        Map itemGrupoExtinta = graficoGruposExtintas.data.find { mapTemp ->
                            return mapTemp.containsKey(keyGrupo)
                        }
                        // se encontrou, acumula o total do grupo
                        if (itemGrupoExtinta) {
                            itemGrupoExtinta[keyGrupo].total += 1
                        } else {
                            // criar o item
                            Map map = [:]
                            map.put(keyGrupo, [total: 1, categoria: row.cd_grupo, color: _getPieColor(row.cd_grupo)])
                            graficoGruposExtintas.data.push(map)
                        }
                    }
                }
            }
            // dados do gráfico categrias extintas EX e RE
            graficoCategorias.data.find { mapTemp ->
                mapTemp.find{ key, value ->
                    if( value.categoria =~ /^(EX|RE)$/ ) {
                        graficoCategoriasExtintas.put( key, value )
                    }
                }
            }

            res.data.dadosGraficoCategorias = graficoCategorias
            res.data.dadosGraficoGrupos     = graficoGrupos
            // colocar os grupos em ordem alfabética
            graficoGrupos.data.sort{ it.toString() }
            graficoGruposAmeacadas.data.sort{ it.toString() }
            graficoGruposExtintas.data.sort{ it.toString() }

            res.data.dadosGraficoCategoriasAmeacadas = graficoCategoriasAmeacadas
            res.data.dadosGraficoGruposAmeacadas     = graficoGruposAmeacadas
            // estes gráficos ainda não estão criados na tela
            res.data.dadosGraficoCategoriasExtintas  = graficoCategoriasExtintas // EX e RE
            res.data.dadosGraficoGruposExtintos      = graficoGruposExtintas // EX e RE

            res.status  = 0
            res.type    = 'success'
            res.msg     = '';
        } catch (Exception e) {
            println e.getMessage();
            res.msg = 'Erro executando a consulta.'
        }
        render res as JSON
    }

    def getJustificativaTaxonHistorico() {
        Map res = [status: 1, msg: '', type: 'error', data: [dsJustificativa:'']]
        try {
            if( ! params.sqTaxonHistorico ){
                throw  new Exception('Id não informado');
            }
            Long sqTaxonHistoricoAvaliacao = decryptStr(params.sqTaxonHistorico).toLong();
            if( !sqTaxonHistoricoAvaliacao ){
                throw  new Exception('Id invalido');
            }
            String cmdSql="select tx_justificativa_avaliacao from salve.taxon_historico_avaliacao where sq_taxon_historico_avaliacao = " +sqTaxonHistoricoAvaliacao.toString()
            sqlService.execSql(cmdSql).each { row ->
                res.data.dsJustificativa = row.tx_justificativa_avaliacao
            }
            res.status = 0
            res.type = 'success'
            res.msg = '';
        } catch (Exception e) {
            println e.getMessage();
            res.msg = 'Erro executando a consulta.'
        }
        render res as JSON
    }

    def getJustificativaExclusao() {
        Map res = [status: 1, msg: '', type: 'error', data: [dsJustificativa:'']]
        try {
            if( ! params.sqFicha ){
                throw  new Exception('Ficha não informada');
            }
            Long sqFicha = decryptStr(params.sqFicha).toLong();
            if( !sqFicha ){
                throw  new Exception('Ficha invalida');
            }
            String cmdSql="select ds_justificativa from salve.ficha where sq_ficha="+sqFicha.toString()
            sqlService.execSql(cmdSql).each { row ->
                res.data.dsJustificativa = row.ds_justificativa
            }
            res.status = 0
            res.type = 'success'
            res.msg = '';
        } catch (Exception e) {
            println e.getMessage();
            res.msg = 'Erro executando a consulta.'
        }
        render res as JSON
    }

}
