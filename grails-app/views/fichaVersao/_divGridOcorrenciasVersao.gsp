<!-- view: /views/fichaVersao/_divGridOcorrencias.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<table id="table${gridId}"  class="table table-striped table-sorting">
    <thead>
    %{-- PAGINACAO --}%
    <tr id="tr${gridId}Pagination">
        <td colspan="19" >
            <div class="row">
                <div class="col-sm-12 text-left"  style="min-height:0;height: auto;overflow: hidden;" id="div${(gridId ?: pagination?.gridId)}PaginationContent">
                    <g:if test="${pagination?.totalRecords}">
                        <g:gridPagination  pagination="${pagination}"/>
                    </g:if>
                </div>
            </div>
        </td>
    </tr>

    <tr id="tr${gridId}Columns" data-grid-id="${gridId}">
        <th style="min-width:50px;">#</th>
        <th style="min-width:100px;">Base<br>dados </th>
        <th style="min-width:200px;">Tipo<br>registro</th>
        <th style="min-width:200px;">Data</th>
        <th style="min-width:200px;">Presença atual<br>na coordenada</th>
        <th style="min-width:120px;">Altitude (m)</th>
        <th style="min-width:120px;">Sensível</th>
        <th style="min-width:120px;">Prazo<br>carência</th>
        <th style="min-width:120px;">Datum</th>
        <th style="min-width:120px;">Latitude</th>
        <th style="min-width:120px;">Longitude</th>
        <th style="min-width:120px;">Formato<br>original</th>
        <th style="min-width:200px;">Precisão da<br>coordenada</th>
        <th style="min-width:500px;">Descrição do método<br>aproximação</th>
        <th style="min-width:200px;">Situação do<br>registro</th>
        <th style="min-width:200px;">Motivo não<br>utilizado</th>
        <th style="min-width:500px;">Justificativa não utilizado</th>
        <th style="min-width:150px;">Id origem</th>
        <th style="min-width:500px;">Observação</th>
    </tr>
    </thead>
    <tbody>
    <g:if test="${rows}">
        <g:each var="row" in="${rows}" status="i">
            <tr id="tr-${row.id}">
                %{-- COLUNA NUMERAÇÃO --}%
                <td class="text-center"
                    id="td-${row.id}">
                    ${ i + (pagination ? pagination?.rowNum.toInteger() : 1 ) }
                </td>

                %{-- BASE DE DADOS --}%
                <td class="text-center">${row.no_base_dados}</td>

                %{-- TIPO DE REGISTRO --}%
                <td class="text-center">${row.de_tipo_registro}</td>

                %{-- DATA DO REGISTRO --}%
                <td class="text-center">${row.de_periodo}</td>

                %{-- PRESENÇA ATUAL NA COORDENADA --}%
                <td class="text-center">${row.de_presenca_atual}</td>

                %{-- ALTITUDE --}%
                <td class="text-center">${row.nu_altitude}</td>

                %{-- SENSÍVEL --}%
                <td class="text-center">${row.de_sensivel}</td>

                %{-- PRAZO CARÊNCIA --}%
                <td class="text-center">${row.de_carencia}</td>

                %{-- DATUM --}%
                <td class="text-center">${row.de_datum}</td>

                %{-- LATITUDE --}%
                <td class="text-center">${row.nu_latitude}</td>

                %{-- LONGITUDE --}%
                <td class="text-center">${row.nu_longitude}</td>

                %{-- FORMATO ORIGINAL --}%
                <td class="text-center">${row.de_formato_original}</td>

                %{-- PRECISÃO --}%
                <td class="text-center">${row.de_precisao}</td>

                %{-- DESCRIÇÃO DO MÉTODO APROXIMAÇÃO --}%
                <td>
                    <div style="max-height: 300px;overflow: auto;text-align: justify;">
                        ${ raw(row.tx_metodo_aproximacao) }
                    </div>
                </td>

                %{-- SITUAÇÃO DO REGISTRO--}%
                <td class="text-center">${row.de_situacao_avaliacao}</td>

                %{-- MOTIVO NÃO UTILIZADO--}%
                <td class="text-center">${row.de_motivo_nao_utilizado}</td>

                %{-- JUSTIFICATIVA NÃO UTILIZADO--}%
                <td>
                    <div style="max-height: 300px;overflow: auto;text-align: justify;">
                        ${ raw(row.tx_nao_utilizado) }
                    </div>
                </td>

                %{-- ID ORIGEM --}%
                <td class="text-center nowrap">${row.id_origem}</td>

                %{-- OBSERVAÇÃO--}%
                <td>
                    <div style="max-height: 300px;overflow: auto;text-align: justify;">
                        ${ raw(row.tx_observacao) }
                    </div>
                </td>
            </tr>
        </g:each>
    </g:if>
    <g:else>
        <tr>
            <td colspan="18">
                <p>Não existem registros</p>
            </td>
        </tr>
    </g:else>
    </tbody>
</table>
