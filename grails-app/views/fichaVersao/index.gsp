<!-- view: /views/fichaVersao/index.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<style>
div.ficha-versao * {
    font-size:14px;
}

div.ficha-versao p {
    padding: 0px 0px 2px;
    margin:0;
}

div.ficha-versao label {
    margin-right: 5px;
}

div.ficha-versao table td {
    vertical-align:top !important;
}

div.ficha-versao img.img-mapa {
    width:auto;
    height: auto;
}


div.panel-heading {
    text-align: center;
}

div.panel-title p {
    font-size:18px !important;

}

p.nome-cientifico {
    font-size:24px !important;
}

div.ficha-versao div.nivel-taxonomico {
    display:inline-block;
    min-width: 75px;
}

p.categoria-avaliacao{

}
p.data-avaliacao{
    font-size:13px !important;
}

div.ficha-versao img.imagem-principal {
    max-width: 200px;
    max-height: 140px;
}
</style>
<div id="container-ficha-versao" data-nu-versao="${fichaVersao.nuVersao}"
     data-sq-ficha-versao="${fichaVersao.id}"
     data-nm-cientifico-sem-autor="${raw( br.gov.icmbio.Util.ncItalico(jsonFicha.nm_cientifico,true))}"
     class="container-fluid ficha-versao">

    <div class="mt10 panel panel-default">
        <div class="panel-heading" role="tab">
            <h4 class="panel-title">
                <p class="nome-cientifico">${raw( br.gov.icmbio.Util.ncItalico(jsonFicha.nm_cientifico))}</p>
            </h4>
        </div>
    </div>

    <g:if test="${fichaVersao.contexto.codigoSistema == 'PUBLICACAO'}">
        <div class="panel panel-default">
            <g:if test="${jsonFicha.avaliacao?.resultado_avaliacao?.nu_mes_ano_ultima_avaliacao}">
            <div class="panel-heading" role="tab">
                <h4 class="panel-title">
                    <p class="categoria-avaliacao">Categoria: ${ raw(jsonFicha.avaliacao.resultado_avaliacao.ds_categoria_iucn +' ('+jsonFicha.avaliacao.resultado_avaliacao.cd_categoria_iucn+')')}</p>
                    <p class="data-avaliacao">Data da avaliação: ${jsonFicha.avaliacao?.resultado_avaliacao?.nu_mes_ano_ultima_avaliacao}</p>
                </h4>
            </div>
            <div class="panel-body">
                <g:editarTextoVersao label="Justificativa"
                                     target="dsJustificativa"
                                     table="ficha"
                                     column="ds_justificativa_final"
                                     jsonKey="avaliacao.resultado_avaliacao.ds_justificativa"
                                     text="${jsonFicha.avaliacao.resultado_avaliacao.ds_justificativa}">
                </g:editarTextoVersao>
            </div>
            </g:if>
            <g:else>
                <div class="panel-heading" role="tab">
                    <h4 class="panel-title">
                        <p class="categoria-avaliacao">Categoria: (ficha não foi avaliada)</p>
                    </h4>
                </div>
            </g:else>
        </div>
    </g:if>
    <g:else>
        <div class="panel panel-default">
            <div class="panel-heading" role="tab">
                <h4 class="panel-title">
                    <p class="categoria-avaliacao red bold">Ficha excluída</p>
                    <p class="data-avaliacao">Data da exclusão: ${fichaVersao.dtInclusao.format('dd/MM/yyyy')}</p>
                </h4>
            </div>
            <div class="panel-body">
                <p class="bold">Motivo</p>
                <p>${raw(fichaHistSituacaoExcluidaVersao?.fichaHistSituacaoExcluida?.dsJustificativa)}</p>
            </div>
        </div>

    </g:else>


    <div class="panel-group" id="accordion-ficha-versao" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingPage01">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion-ficha-versao" href="#collapsePage01" aria-expanded="true" aria-controls="collapsePage01">
                        Classificação Taxonômica
                    </a>
                </h4>
            </div>
            <div id="collapsePage01" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingPage01">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <p><div class="nivel-taxonomico">Filo:</div> <b>${jsonFicha.taxon_trilha.filo.no_taxon}</b></p>
                            <p><div class="nivel-taxonomico">Classe:</div> <b>${jsonFicha.taxon_trilha.classe.no_taxon}</b></p>
                            <p><div class="nivel-taxonomico">Ordem:</div> <b>${jsonFicha.taxon_trilha.ordem.no_taxon}</b></p>
                            <p><div class="nivel-taxonomico">Gênero:</div> <b><i>${jsonFicha.taxon_trilha.genero.no_taxon}</i></b></p>
                            <p><div class="nivel-taxonomico">Espécie:</div> <b><i>${jsonFicha.taxon_trilha.especie.no_taxon}</i></b></p>
                        </div>
                        <div class="col-sm-12 col-md-6 text-right">
                            <g:set var="noArquivo" value=""></g:set>
                            <g:each var="imagem" in="${jsonFicha.multimidia}">
                                <g:if test="${imagem.in_principal && !noArquivo}">
                                    <g:set var="noArquivo" value="${imagem.no_arquivo_disco}"></g:set>
                                </g:if>
                            </g:each>
                            <g:if test="${noArquivo}">
                                <img class="imagem-principal" alt="" src="download?fileName=/data/salve-estadual/multimidia/${noArquivo}">
                            </g:if>
                        </div>
                    </div>

%{--                    GRUPO E SUBGRUPO--}%
                    <div class="row">
                        <div class="col-sm-12 col-md-4">
                            <label>Grupo: </label><span class="bold">${jsonFicha.classificacao_taxonomica.grupo.ds_grupo}</span>
                        </div>
                        <div class="col-sm-12 col-md-8">
                            <label>Subgrupo: </label><span class="bold">${jsonFicha.classificacao_taxonomica.subgrupo.ds_subgrupo}</span>
                        </div>
                        <div class="col-sm-12">
                            <label>Recorte módulo público: </label><span class="bold">${jsonFicha.classificacao_taxonomica.recorte_publico.ds_recorte_publico}</span>
                        </div>
                    </div>


                    %{--                    NOMES COMUNS--}%
                    <div class="row mt10">
                        <div class="col-sm-12">
                            <p class="bold">Nomes comuns</p>
                            <p>${jsonFicha.classificacao_taxonomica.nomes_comuns_formatados}</p>
                        </div>
                    </div>

                    %{--                    NOMES ANTIGOS--}%
                    <div class="row mt10">
                        <div class="col-sm-12">
                            <p class="bold">Nomes antigos</p>
                            <p>${raw( jsonFicha.classificacao_taxonomica.nomes_antigos_formatados )}</p>
                        </div>
                    </div>

                    %{--                   NOTAS TAXONOMICAS --}%
                    <div class="row mt10">
                        <div class="col-sm-12">
                            %{--<p class="bold">Notas taxonômicas</p>
                            <span>${raw( jsonFicha.classificacao_taxonomica.notas_taxonomicas )}</span>--}%
                            <g:editarTextoVersao label="Notas taxonômicas"
                                                 target="dsNotasTaxonomicas"
                                                 table="ficha"
                                                 column="ds_notas_taxonomicas"
                                                 jsonKey="classificacao_taxonomica.notas_taxonomicas"
                                                 text="${ jsonFicha.classificacao_taxonomica.notas_taxonomicas }">
                            </g:editarTextoVersao>
                        </div>
                    </div>

                    %{--                   NOTAS MORFOLOGICAS --}%
                    <div class="row mt10">
                        <div class="col-sm-12">
                            <g:editarTextoVersao label="Notas morfológicas"
                                                 target="dsDiagnosticoMorfologico"
                                                 table="ficha"
                                                 column="ds_diagnostico_morfologico"
                                                 jsonKey="classificacao_taxonomica.notas_morfologicas"
                                                 text="${ jsonFicha.classificacao_taxonomica.notas_morfologicas }">
                            </g:editarTextoVersao>
                        </div>
                    </div>
                </div>
            </div>
        </div> %{--FIM PANEL CLASSIFICACAO TAXONOMICA--}%
    %{-- FIM PANEL 01--}%


    %{-- PANEL 02 - DISTRIBUIÇÃO --}%
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingPage02">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion-ficha-versao" href="#collapsePage02" aria-expanded="false" aria-controls="collapsePage02">
                        Distribuição
                    </a>
                </h4>
            </div>
            <div id="collapsePage02" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingPage02">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <p>
                                <label>Endêmica do Brasil?</label><span class="bold">${br.gov.icmbio.Util.snd(jsonFicha.distribuicao.st_endemica_brasil)}</span>
                            </p>
%{--                            DISTRIBUICAO GLOBAL--}%
                            <p>
                                <g:editarTextoVersao label="Distribuição geográfica global"
                                                     target="dsDistribuicaoGeoGlobal"
                                                     table="ficha"
                                                     column="ds_distribuicao_geo_global"
                                                     jsonKey="distribuicao.ds_distribuicao_geo_global"
                                                     text="${ jsonFicha.distribuicao.ds_ditribuicao_geo_global }">
                                </g:editarTextoVersao>
                            </p>

%{--                            DISTRIBUICAO NACIONALL--}%
                            <p>
                                <g:editarTextoVersao label="Distribuição geográfica nacional"
                                                     target="dsDistribuicaoGeoNacional"
                                                     table="ficha"
                                                     column="ds_distribuicao_geo_nacional"
                                                     jsonKey="distribuicao.ds_distribuicao_geo_nacional"
                                                     text="${ jsonFicha.distribuicao.ds_ditribuicao_geo_nacional }">
                                </g:editarTextoVersao>
                            </p>

%{--                            ALTITUDE--}%
                            <p>
                                <label>Altitude</label><span><label>máxima:</label><span class="bold">${jsonFicha.distribuicao.nu_max_altitude}m</span> / <label>mínima:</label><span class="bold">${jsonFicha.distribuicao.nu_min_altitude}m</span></span>
                            </p>

%{--                            <p>Não exibir o mapa on-line</p>--}%

                            %{-- MAPA DE DISTRIBUIÇÃO --}%
                            <p>
                                <label>Mapa de distribuição</label>
                                <div style="border:1px solid silver;text-align: center">
                                    <img class="img-mapa" src="http://salve.dev.icmbio.gov.br:8080/salve-estadual/download?fileName=/data/salve-estadual/anexos/a37bff1c6dc91b58576b09f01ebf5fc4.jpeg" alt="Mapa de distribuição">
                                </div>
                            </p>

                            <p class="mt10">Registros</p>
                            <div id="containerGridOcorrenciasVersao"
                                 style="width:100%;height: auto;max-height: 400px;overflow: auto;"
                                 data-params=""
                                 data-max-height="400px"
                                 data-min-height="200px"
                                 data-height="auto"
                                 data-field-id="id"
                                 data-url="fichaVersao/getGridOcorrencias"
                                >
                                 %{-- RENDERIZAR O GRIDE VAZIO--}%
                                <g:render template="divGridOcorrenciasVersao"
                                          model="[gridId: 'GridOcorrenciasVersao',rows:[],pagination:[:]]"></g:render>
                            </div>

                            %{--
                            <div style="width:100%;height: auto;max-height: 400px;overflow: auto;" >
                                <table class="table table-condensed table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th style="min-width:100px;">Base<br>dados </th>
                                            <th style="min-width:200px;">Tipo<br>registro</th>
                                            <th style="min-width:200px;">Data</th>
                                            <th style="min-width:200px;">Presença atual<br>na coordenada</th>
                                            <th style="min-width:120px;">Altitude (m)</th>
                                            <th style="min-width:120px;">Sensível</th>
                                            <th style="min-width:120px;">Prazo<br>carência</th>
                                            <th style="min-width:120px;">Datum</th>
                                            <th style="min-width:120px;">Latitude</th>
                                            <th style="min-width:120px;">Longitude</th>
                                            <th style="min-width:120px;">Formato<br>original</th>
                                            <th style="min-width:200px;">Precisão da<br>coordenada</th>
                                            <th style="min-width:500px;">Descrição do método<br>aproximação</th>
                                            <th style="min-width:200px;">Situação do<br>registro</th>
                                            <th style="min-width:200px;">Motivo não<br>utilizado</th>
                                            <th style="min-width:500px;">Justificativa não utilizado</th>
                                            <th style="min-width:150px;">Id origem</th>
                                            <th style="min-width:500px;">Observação</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>

                                            --}%
                                            %{-- BASE DE DADOS --}%%{--
                                            <td class="text-center">SALVE</td>

                                            --}%%{-- TIPO DE REGISTRO --}%%{--
                                            <td class="text-center">Sensores autônomos de registro</td>

                                            --}%
                                            %{-- DATA DO REGISTRO --}%
                                            %{--
                                            <td class="text-center">01/01/20222 a 15/01/2022</td>

                                            --}%%{-- PRESENÇA ATUAL NA CORRDENAD --}%%{--
                                            <td class="text-center">Desconhecido</td>

                                            --}%%{-- ALTITUDE --}%%{--
                                            <td class="text-center">10</td>

                                            --}%%{-- SENSÍVEL --}%%{--
                                            <td class="text-center">Não</td>

                                            --}%%{-- PRAZO CARÊNCIA --}%%{--
                                            <td class="text-center">Sem carência</td>

                                            --}%%{-- DATUM --}%%{--
                                            <td class="text-center">Côrrego alegre</td>

                                            --}%%{-- LATITUDE --}%%{--
                                            <td class="text-center">-15.00000000</td>

                                            --}%%{-- LONGITUDE --}%%{--
                                            <td class="text-center">-10.00000000</td>

                                            --}%%{-- FORMATO ORIGINAL --}%%{--
                                            <td class="text-center">GG MM SS NNN</td>

                                            --}%%{-- PRECISÃO --}%%{--
                                            <td class="text-center">Referência da área amostrada</td>

                                            --}%%{-- DESCRIÇÃO DO MÉTODO APROXIMAÇÃO --}%%{--
                                            <td>
                                                <div style="max-height: 300px;overflow: auto;text-align: justify;">
                                                Descrição do método. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aperiam assumenda dignissimos dolore dolorem doloremque ducimus, eos esse harum illo libero maxime necessitatibus neque optio pariatur quia repellendus, saepe similique. A consequuntur cum dolores ea et ipsam labore maiores, maxime, qui quia reprehenderit sequi sit, totam. Amet, debitis fuga laborum maxime, molestiae nobis odio, quaerat quidem repellendus sapiente suscipit tempore? Aut, iusto, quis. Dolores dolorum eius impedit in iusto sapiente, ullam ut? Architecto consectetur dolorum excepturi in ipsa, iste, itaque nobis reiciendis rem repellat, saepe sapiente sed sit tempora voluptatum. Aliquam asperiores aut blanditiis distinctio dolorum excepturi illum, impedit optio perspiciatis provident quasi quos totam vitae voluptas voluptate? Aut culpa facere facilis ipsa ipsum! Ab et eum expedita necessitatibus nobis. Amet aperiam at autem commodi, consectetur deserunt dignissimos ipsa ipsam laudantium maxime nam neque nesciunt non nulla, omnis optio placeat quae, quas quo quod quos saepe tenetur ullam unde voluptatum! Aspernatur consequatur dicta, enim perferendis possimus quisquam sequi. Aliquid deleniti expedita ipsam natus quidem quos repellat sapiente temporibus. Cupiditate, doloribus fuga illo inventore laborum minus molestiae provident reiciendis repudiandae tempore. A ad alias animi aperiam, cumque cupiditate dicta eaque et itaque, iure, libero magnam nihil nisi obcaecati optio praesentium sequi sint suscipit ut vero. Accusamus aspernatur cumque distinctio illum labore. Accusamus adipisci aliquam atque aut consequuntur debitis deserunt dignissimos dolore dolores exercitationem harum iure laudantium mollitia nesciunt, nisi odit qui quia quis ratione recusandae repellat reprehenderit, sed soluta temporibus tenetur. Ab, architecto culpa eveniet excepturi exercitationem facere fugit libero modi nemo neque nihil nostrum obcaecati officia pariatur quidem quo repellendus sed, tenetur veritatis voluptates! Ab animi ex ipsum obcaecati veritatis? Ad, dignissimos dolor ea eius eum, excepturi fugiat harum illum impedit ipsum modi molestias, nemo nobis officia perspiciatis tempora ullam voluptate? Distinctio eius nemo obcaecati perspiciatis quis saepe vitae voluptates.
                                                </div>
                                            </td>

                                            --}%%{-- SITUAÇÃO DO REGISTRO--}%%{--
                                            <td class="text-center">Confirmado e adicionado após a avaliação</td>

                                            --}%%{-- MOTIVO NÃO UTILIZADO--}%%{--
                                            <td class="text-center">Registro fora da distribuição nativa da espécie</td>

                                            --}%%{-- JUSTIFICATIVA NÃO UTILIZADO--}%%{--
                                            <td>
                                                <div style="max-height: 300px;overflow: auto;text-align: justify;">
                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi incidunt non quo soluta? Consequatur deleniti dolore doloremque doloribus ipsum iure iusto officia, quam ut vero! Iure, porro, sapiente. Exercitationem, nesciunt.
                                                </div>
                                            </td>


                                            --}%%{-- ID ORIGEM --}%%{--
                                            <td class="text-center nowrap">po-48487-jaijs</td>

                                            --}%%{-- OBSERVAÇÃO--}%%{--
                                            <td>
                                                <div style="max-height: 300px;overflow: auto;text-align: justify;">
                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi incidunt non quo soluta? Consequatur deleniti dolore doloremque doloribus ipsum iure iusto officia, quam ut vero! Iure, porro, sapiente. Exercitationem, nesciunt.
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>--}%

                            <p>Imagens de mapas e shapefiles</p>
                            -- preencher -- <hr>
                            <p>Estados</p>
                            -- preencher -- <hr>
                            <p>Biomas</p>
                            -- preencher -- <hr>
                            <p>Bacias hidrográficas</p>
                            -- preencher -- <hr>
                            <p>Ambiente restrito</p>
                            -- preencher -- <hr>
                            <p>Áreas relevantes</p>
                            -- preencher -- <hr>
                            <p>EOO / AOO</p>
                            -- preencher -- <hr>



                        </div>
                    </div>
                </div>
            </div>
        </div>
        %{-- FIM PANEL 02--}%

        %{-- PANEL 03 - HISTORIA NATURAL --}%
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingPage03">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion-ficha-versao" href="#collapsePage03" aria-expanded="false" aria-controls="collapsePage03">
                        História natural
                    </a>
                </h4>
            </div>
            <div id="collapsePage03" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingPage03">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-6">


                        </div>
                    </div>
                </div>
            </div>
        </div>
        %{-- FIM PANEL 03--}%


        %{-- PANEL 04 - POPULAÇÃO --}%
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingPage04">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion-ficha-versao" href="#collapsePage04" aria-expanded="false" aria-controls="collapsePage04">
                        População
                    </a>
                </h4>
            </div>
            <div id="collapsePage04" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingPage04">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-6">


                        </div>
                    </div>
                </div>
            </div>
        </div>
        %{-- FIM PANEL 04--}%

        %{-- PANEL 05 - AMEACAS --}%
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingPage05">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion-ficha-versao" href="#collapsePage05" aria-expanded="false" aria-controls="collapsePage05">
                        Ameaças
                    </a>
                </h4>
            </div>
            <div id="collapsePage05" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingPage05">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-6">


                        </div>
                    </div>
                </div>
            </div>
        </div>
        %{-- FIM PANEL 05--}%


        %{-- PANEL 06 - USOS --}%
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingPage06">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion-ficha-versao" href="#collapsePage06" aria-expanded="false" aria-controls="collapsePage06">
                        Usos
                    </a>
                </h4>
            </div>
            <div id="collapsePage06" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingPage06">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-6">


                        </div>
                    </div>
                </div>
            </div>
        </div>
        %{-- FIM PANEL 06--}%

        %{-- PANEL 07 - CONSERVAÇÃO --}%
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingPage07">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion-ficha-versao" href="#collapsePage07" aria-expanded="false" aria-controls="collapsePage07">
                        Conservação
                    </a>
                </h4>
            </div>
            <div id="collapsePage07" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingPage07">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-6">


                        </div>
                    </div>
                </div>
            </div>
        </div>
        %{-- FIM PANEL 07--}%


        %{-- PANEL 08 - PESQUISA --}%
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingPage08">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion-ficha-versao" href="#collapsePage08" aria-expanded="false" aria-controls="collapsePage08">
                        Pesquisa
                    </a>
                </h4>
            </div>
            <div id="collapsePage08" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingPage08">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-6">


                        </div>
                    </div>
                </div>
            </div>
        </div>
        %{-- FIM PANEL 08 - PESQUISA --}%


        %{-- PANEL 09 - EQUIPE TÉCNICA --}%
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingPage09">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion-ficha-versao" href="#collapsePage09" aria-expanded="false" aria-controls="collapsePage09">
                        Equipe técnica
                    </a>
                </h4>
            </div>
            <div id="collapsePage09" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingPage09">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                    </div>
                    </div>
                </div>
            </div>
        </div>
        %{-- FIM PANEL 09 - EQUIPE TÉCNICA --}%


        %{-- PANEL 10 - COMO CITAR --}%
        <g:if test="${fichaVersao.contexto.codigoSistema == 'PUBLICACAO'}">
            <div class="row mt20">
                <div class="col-sm-12">
                    <div style="padding:5px;border:1px solid gray;background-color: #efefef;">
                        <p class="bold">Como citar</p>
                        <p>${raw( jsonFicha.comoCitar)}</p>
                    </div>
                </div>
            </div>
            %{-- FIM PANEL 10 - COMO CITAR --}%
        </g:if>


        %{--        REFERÊNCIAS --}%
        <div class="row mt20">
            <div class="col-sm-12">
                <p class="bold">Referências bibliográficas</p>
            </div>
        </div>
%{--        FIM REFERENCIAS --}%

    </div>
    %{--    FIM PANEL GROUP ACCORDION--}%
    <asset:javascript src="fichaVersao.js"></asset:javascript>
</div>
