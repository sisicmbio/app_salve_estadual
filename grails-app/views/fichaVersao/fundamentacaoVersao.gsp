<!-- view: /views/fichaVersao/fundamentacaoVersao.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<style>
    div.justificativa{
        border:1px solid #d3d0d0;
        max-height: 200px;
        overflow-y:auto;
        background: #eee;
        padding:5px;
        text-align: justify;
    }
    div.chat {
        border:1px solid #d3d0d0;
        min-height: 400px;
        max-height: 600px;
        overflow-y:auto;
        background: #dfe3ea;
        padding:5px;
        text-align: justify;
        border-radius: 10px;
    }
</style>
<div id="container-fundamentacao-versao" data-nu-versao="${dadosFundamentacao.nuVersao}"  class="container-fluid fundamentacao-versao">

%{--    RESULTADO DA AVALIAÇÃO--}%
    <div class="mt10 panel panel-default">
        <div class="panel-heading" role="tab">
            <h4 class="panel-title">
                Resultado da Avaliação
            </h4>
        </div>
        <div class="panel-body">
            <div class="form-inline">
                    <div class="form-group">
                        <label class="control-label">
                            Categoria
                        </label>
                        <br>
                        <input type="text" class="form-control fld300" readonly
                               value="Menos Preocupante (LC)">
                    </div>

                    <div class="form-group">
                        <label class="control-label">
                            Critério
                        </label>
                        <br>
                        <input type="text" class="form-control fld250" readonly
                               value="A2BC">
                    </div>

                    <div class="form-group">
                        <label class="control-label">
                            PEX
                        </label>
                        <br>
                        <input type="text" class="form-control fld100" readonly
                               value="Não">
                    </div>
                    <br>
                    <div class="form-group w100">
                        <label class="control-label">
                            Justificativa
                        </label>
                        <br>
                        <div class="justificativa">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium ad animi corporis dolor eaque earum, esse, illum, labore laboriosam minima praesentium quibusdam recusandae rerum unde voluptatibus. Atque consequuntur quos temporibus!</div>

                    </div>
            </div>
        </div>
    </div>

    %{--    RESPOSTAS E CHAT --}%
    <div class="row">

        %{-- RESPOSTAS --}%
        <div class="col-sm-12 col-md-6">
            <fieldset>
            <legend>Respostas</legend>

                  %{--  ABAS VALIDADOR 1    --}%
%{--  LOOP RESPOSTAS AQUI--}%

                <ul class="nav nav-tabs">
                    <li class="active">
                        <a data-toggle="tab" href="#tabResultado" title="Nome do validador 1 aqui">Respostas</a>
                    </li>
                    %{-- SE HOUVE REVISAO DA COORDENACAO MOSTRA A OUTRA ABA--}%
                    <g:if test="${ true }">
                        <li class="">
                            <a data-toggle="tab" href="#tabRevisao" class="cursor-pointer">Revisão coordenação</a>
                        </li>
                    </g:if>
                </ul>
                <!-- Container das abas validador 1-->
                <div class="tab-content">
                    <div role="tabpanel" class="mt5 tab-pane tab-pane-ficha active" id="tabResultado">
                        <p>Nome:<b>Luis Teste</b><br>Resposta será exibida aqui</p>
                        <hr>
                        <p>Nome:<b>Maria Teste</b><br>Resposta: resposta será exibida aqui</p>
                    </div>
                    <g:if test="${ true }">
                        <div role="tabpanel" class="mt5 tab-pane tab-pane-ficha" id="tabRevisao">
                            <p>Revisão coordenação quando houver será exibida aqui</p>
                        </div>
                    </g:if>
                </div>


            </fieldset>




        </div>
        %{--   CHAT --}%
        <div class="col-sm-12 col-md-6">
            <fieldset>
                <legend>Bate-papo</legend>
                <div class="chat"></div>
            </fieldset>
        </div>
    </div>


    %{--    RESULTADO DA VALIDAÇÃO--}%
    <div class="mt10 panel panel-default">
        <div class="panel-heading" role="tab">
            <h4 class="panel-title">
                Resultado da Validação em 15/06/2012
            </h4>
        </div>
        <div class="panel-body">
            <div class="form-inline">
                <div class="form-group">
                    <label class="control-label">
                        Categoria
                    </label>
                    <br>
                    <input type="text" class="form-control fld300" readonly
                           value="Menos Preocupante (LC)">
                </div>

                <div class="form-group">
                    <label class="control-label">
                        Critério
                    </label>
                    <br>
                    <input type="text" class="form-control fld250" readonly
                           value="A2BC">
                </div>

                <div class="form-group">
                    <label class="control-label">
                        PEX
                    </label>
                    <br>
                    <input type="text" class="form-control fld100" readonly
                           value="Não">
                </div>
                <br>
                <div class="form-group w100">
                    <label class="control-label">
                        Justificativa
                    </label>
                    <br>
                    <div class="justificativa">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium ad animi corporis dolor eaque earum, esse, illum, labore laboriosam minima praesentium quibusdam recusandae rerum unde voluptatibus. Atque consequuntur quos temporibus!</div>

                </div>
            </div>
        </div>
    </div>

    <asset:javascript src="fundamentacaoVersao.js"></asset:javascript>
</div>
