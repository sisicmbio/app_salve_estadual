<div class="window-container text-left">
	<form id="frmAcaoConservacao" name="frmAcaoConservacao" class="form-inline" role="form">

      	<div class="form-group">
			<label for="codigo" class="control-label">
				Código
			</label>
			<br>
			<input name="codigo" id="codigo" type="text" class="form-control form-clean fld100" placeholder="1, 1.1" value="${acaoConservacao?.codigo}" required="false"/>
		</div>

		<div class="form-group">
			<label for="inputAcaoConservacao" class="control-label">
				Descrição da ação de conservação
			</label>
			<br>
			<input name="descricao" id="inputAcaoConservacao" type="text" class="form-control form-clean fld500" placeholder="Informe o nome do acaoConservacao" value="${acaoConservacao?.descricao}" required="true" data-msg-required="Campo Obrigatório"/>
		</div>
        <br>
        %{--        INTEGRACAO SIS/IUCN--}%
        <div class="form-group">
            <label for="sqIucnAcaoConservacao" class="control-label">
                Ação conservação (SIS/IUCN)
            </label>
            <br>

            <select name="sqIucnAcaoConservacao" id="sqIucnAcaoConservacao" class="form-control select2" style="width:600px;"
                    data-s2-placeholder="?"
                    data-s2-minimum-input-length="0"
                    data-s2-auto-height="true">
                <option value="">?</option>
                <g:each var="item" in="${listIucnAcaoConservacao}">
                    <option value="${item.id}" ${ item.id == iucnAcaoConservacaoApoio?.iucnAcaoConservacao?.id ? " selected":"" }>${ item.descricaoCompleta }</option>
                </g:each>
            </select>
        </div>
        <br>
        <div class="form-group">
            <label for="codigoSistema" class="control-label">
                Código sistema
            </label>
            <br>
            <input name="codigoSistema" id="codigoSistema" type="text" class="form-control form-clean fld400" placeholder="Código uso sistema" value="${acaoConservacao?.codigoSistema}" required="false"/>
        </div>
        <div class="form-group">
            <label for="ordem" class="control-label">
                Ordem listagem
            </label>
            <br>
            <input name="ordem" id="ordem" type="text" class="form-control form-clean fld200" placeholder="Ordem apresentação" value="${acaoConservacao?.ordem}" required="false"/>
        </div>

        <div class="fld panel-footer">
			<button id="btnSaveFrmAcaoConservacao" data-action="acaoConservacao.save" class="fld btn btn-success">Gravar</button>
		</div>
	</form>
</div>
