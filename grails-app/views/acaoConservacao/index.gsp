<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="ajax"/>
        <title>
        SALVE - Ações de Conservação
        </title>
    </head>
    <body>
        <div class="col-sm-12">
            <div class="panel panel-default w100p" style="">
                <div class="panel-heading">
                    <span>
                        Ações de Conservação SALVE
                    </span>
                    <button id="btnClose" data-action="app.unloadModule" class="btn btn-default pull-right btn-xs" title="Fechar"><span class="glyphicon glyphicon-remove btnClose"></span></button>
                    <button type="button" id="btnAdd" data-action='acaoConservacao.add' class="btn btn-default pull-right btn-xs" data-toggle="tooltip" data-placement="top" title="Nova ação de conservação"><span class="glyphicon glyphicon-plus"></span>&nbsp; Cadastrar</button>
                </div>

                <div class="panel-body">
                    <table id="treeview" class="table table-condensed table-hover table-striped fancytree-fade-expander treeTable">
                        <colgroup>
                        <col width="500"></col>
                        <col width="auto"></col>
                        <col width="120"></col>
                        </colgroup>
                        <thead>
                            <tr>
                                <th>Descrição SALVE</th>
                                <th>Ação de conservação SIS/IUCN</th>
                                <th>Ação</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td></td>
                                <td></td>
                                <td class="text-center text-nowrap actions" style="padding-top:4px !important;">
                                    <a data-action="acaoConservacao.edit" data-all="true"
                                       class="fld btn btn-default btn-xs btn-update" title="Alterar">
                                       <span class="glyphicon glyphicon-pencil"></span>
                                    </a>
                                    <a data-action="acaoConservacao.add" data-all="true"
                                       class="fld btn btn-default btn-xs btn-plus" title="Adicionar Subitem">
                                       <span class="glyphicon glyphicon-plus"></span>
                                    </a>

                                    <a data-action="acaoConservacao.delete" class="fld btn btn-default btn-xs btn-delete" title="Excluir">
                                       <span class="glyphicon glyphicon-remove"></span>
                                    </a>

                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>
