<!-- view: /views/gerenciarPapel/_divFicha.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<div id="divTbGerPapelFichas" class="mt10">
    <h4>${listFichas ? listFichas.size() : '0'}&nbsp;Fichas / <span class="total-selecionadas">0 selecionadas.</span></h4>
    <table class="table table-hover table-striped table-condensed table-bordered" id="tbGerPapelFichas">
        <thead>
        <th style="width:30px;">#</th>
        <th style="width:30px;"><i title="Marcar/Desmarcar Todos" data-container="divTbGerPapelFichas" data-action="checkUncheckAll" class="glyphicon glyphicon-unchecked check-all"></i></th>
        <th style="width:auto;">Nome Científico</th>
        </thead>
        <tbody>
        <g:if test="${listFichas}">
            <g:each in="${listFichas}" var="item" status="i">
                <tr id="gdFichaRow_${i + 1}">
                    <td class="text-center">${i+1}</td>
                    <td class="text-center">
                        <input name="sqFicha[${item.sq_ficha}]" value="${item.sq_ficha}" type="checkbox" class="checkbox-lg" data-action="contarChecked" data-container="divTbGerPapelFichas"/>
                    </td>
                    <td>${raw( br.gov.icmbio.Util.ncItalico( item.nm_cientifico,true,true,item.co_nivel_taxonomico ))}</td>
                </tr>
            </g:each>
        </g:if>
        <g:else>
            <tr>
                <td colspan="5" align="center">
                    <h4>Nenhuma Ficha Cadastrada!</h4>
                </td>
            </tr>
        </g:else>
        </tbody>
    </table>
</div>
