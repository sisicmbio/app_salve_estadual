<!-- view: /views/gerenciarPapel/_divGridPessoa -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<table class="table table-striped table-condensed table-bordered" id="tableDivGridPessoaPapel">
    <thead>
        <th style="width:30px;">#</th>
        <th style="width:150px;">Nome<br><select class="select-data-table" title="aplicar filtro nas linhas pelo nome selecionado." data-change="filtrarRows" class="form-control form-control-sm fldSelect-thin" style="max-width:200px;font-weight:normal;">
            <option value="">-- todos --</option>
            <g:each var="item" in="${nomesFiltro}" status="i">
                <option value="${item}">${item}</option>
            </g:each>
            </select>
        </th>
        <th style="width:150px;">Instituição<br><select class="select-data-table" title="aplicar filtro nas linhas pelo nome selecionado." data-change="filtrarRows" class="form-control form-control-sm fldSelect-thin" style="max-width:200px;font-weight:normal;">
            <option value="">-- todas --</option>
            <g:each var="item" in="${instituicoesFiltro}" status="i">
                <option value="${item}">${item}</option>
            </g:each>
            </select>
        </th>
        <th style="width:150px">Função<br><select class="select-data-table" title="aplicar filtro nas linhas pelo papel selecionado." data-change="filtrarRows" class="form-control form-control-sm fldSelect-thin" style="max-width:200px;font-weight:normal;" data-filter-class="subtr">
            <option value="">-- todos --</option>
            <g:each var="item" in="${papeisFiltro}" status="i">
                <option value="${item}">${item}</option>
            </g:each>
        </select>
        </th>
        <th style="width:200px">Grupo Avaliado<br><select class="select-data-table" title="aplicar filtro nas linhas pelo nome selecionado." data-change="filtrarRows" class="form-control form-control-sm fldSelect-thin" style="max-width:200px;font-weight:normal;" data-filter-class="subtr">
            <option value="">-- todos --</option>
            <g:each var="item" in="${gruposFiltro}" status="i">
                <option value="${item}">${item}</option>
            </g:each>
        </select>
        </th>
        <th style="width:200px">Vinculação Fichas<br><select class="select-data-table" title="aplicar filtro nas linhas pelo nome selecionado." data-change="filtrarRows" class="form-control form-control-sm fldSelect-thin" style="max-width:200px;font-weight:normal;" data-filter-class="subtr" data-filter-class="subtr">
            <option value="">-- todos --</option>
            <g:each var="item" in="${unidadesFiltro}" status="i">
                <option value="${item}">${item}</option>
            </g:each>
        </select>
        </th>
        <th style="width:*">Fichas</th>
    </thead>
    <tbody>
    <g:if test="${dataGrid.size() > 0 }">
        <g:each in="${dataGrid}" var="pessoa" status="i">
            <tr id="gdFichaRow_${pessoa.rowKey}" class="tr">
                <td style="text-align: center;width:30px;">${i+1}</td>
                <td style="width:200px;">${ pessoa.no_pessoa }</td>
                <td style="width:200px;">${ pessoa.sg_unidade_org }</td>
                <td colspan="4">
                    <table class="table table-condensed" style="background-color: transparent;">
                        <tbody>
                        <g:each var="papel" in="${pessoa.papeis}" status="j">
                            <tr class="subtr" data-tr-pai="gdFichaRow_${pessoa.rowKey}">
                                %{--FUNÇÃO--}%
                                <td style="width:150px;">${papel.no_papel}<br>
                                    <select title="Ativar/inativar a função em todas as fichas"
                                            data-sq-ciclo-avaliacao="${pessoa.sq_ciclo_avaliacao}"
                                            data-sq-pessoa="${pessoa.sq_pessoa}"
                                            data-row-num="${pessoa.rowKey}"
                                            data-sq-papel="${papel.sq_papel}"
                                            data-sq-grupo="${papel.sq_grupo}"
                                            data-sq-unidade-org="${papel.sq_unidade_usuario}"
                                            data-sq-unidade-ficha="${papel.sq_unidade_ficha}"
                                            data-change="gerenciarPapel.confirmarAtivarDesativar"
                                            data-div-fichas="#divFichas-${pessoa.rowKey}-${j}"
                                            class="form-control fldSelect-thin ${papel.in_ativo == 'S' ? 'green' : 'red'}" aria-invalid="false">
                                        %{--<option value="">-- ativar/desativar --</option>--}%
                                            <option value="S" ${papel.in_ativo == 'S' ? 'selected':''}>Ativo</option>
                                            <option value="N" ${papel.in_ativo != 'S' ? 'selected':''} class="red">Inativo</option>
                                    </select>
                                </td>
                                <td style="width:200px;" class="text-center">${papel.ds_grupo}</td>
                                %{--UNIDADE ORG--}%
                                <td style="width:200px;" class="text-center">${papel.sg_unidade_ficha}</td>
                                <td style="width:auto">
                                    <fieldset>
                                        <legend style="font-size:1.2em;">
                                            <a data-toggle="collapse" href="#divFichas-${pessoa.rowKey}-${j}"
                                               data-div-id="divFichas-${pessoa.rowKey}-${j}"
                                               data-action="gerenciarPapel.getListaFichas"
                                               data-sq-pessoa="${pessoa.sq_pessoa}"
                                               data-sq-hash="${pessoa.rowKey}"
                                               data-sq-papel="${papel.sq_papel}"
                                               data-sq-grupo="${papel.sq_grupo}"
                                               data-sq-unidade-org="${papel.sq_unidade_usuario}"
                                               data-sq-unidade-ficha="${papel.sq_unidade_ficha}"
                                               data-in-tipo-unidade="${papel.in_tipo_unidade}"
                                               data-sq-ciclo-avaliacao="${sqCicloAvaliacao}" class="tooltipstered" style="cursor: pointer; opacity: 1;" title="Clique para mostrar/esconder as fichas">
                                                ${papel.nu_fichas}&nbsp;Fichas
                                            </a>
                                        </legend>
                                        <div id="divFichas-${pessoa.rowKey}-${j}" class="panel-collapse collapse" role="tabpanel" style="">
                                        </div>
                                    </fieldset>
                                </td>
                            </tr>
                        </g:each>
                        </tbody>
                    </table>
                </td>
            </tr>
        </g:each>
    </g:if>
    <g:else>
        <tr>
            <td colspan="3" align="center">
                <h4>Nenhuma ficha encontrada!</h4>
            </td>
        </tr>
    </g:else>
    </tbody>
</table>
