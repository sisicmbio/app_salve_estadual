<%-- este arquivo contem a estrutura de abas para o módulo de gerenciamento das papeis na ficha --%>
<div id="gerenciarPapelContainer">
   <div class="row">
      <div class="col-sm-12">
         <div class="panel panel-default">
            <div class="panel-heading">
               <span>Gerenciar Função</span>
               <a id="btnClose" data-action="app.unloadModule" class="btn btn-default pull-right btn-xs" title="Fechar">
               <span class="glyphicon glyphicon-remove btnClose"></span>
               </a>
            </div>
            <div class="panel-body" id="fichaContainerBody">

                <div id="divSelectCicloAvaliacao" class="form-group">
                    <input type="hidden" id="sqCicloAvaliacao" name="sqCicloAvaliacao" value="${listCiclosAvaliacao[0]?.id}"/>
                </div>


                <form id="frmGerPapel" class="form-inline hidden" role="form">

                        <fieldset>
                        <legend style="font-size:1.2em;">
                            <a data-toggle="collapse" href="#formBody" class="tooltipstered" style="cursor: pointer; opacity: 1;" title="Mostrar/esconder seleção das fichas">
                                <i class="glyphicon glyphicon-plus"></i>&nbsp;Adicionar
                            </a>
                        </legend>

                        <div id="formBody" class="collapse">

                           <div class="fld form-group">
                               <label class="control-label">Função</label>
                               <br>
                               <select name="sqPapel" id="sqPapel" class="form-control fld300 fldSelect" data-change="gerenciarPapel.sqPapelChange">
                                   <option value="">?</option>
                                   <g:each var="item" in="${listPapeis}">
                                       <option data-codigo="${item.codigoSistema}" value="${item.id}">${item.descricao}</option>
                                   </g:each>
                               </select>
                           </div>
                            <div class="fld form-group">
                              <label class="control-label">Pessoa</label>
                              <br>
                              <select name="sqPessoa" id="sqPessoa" class="form-control fld500 select2"
                                data-s2-url="gerenciarPapel/select2PessoaFisica"
                                data-s2-placeholder="?"
                                data-s2-params="sqPapel"
                                data-s2-minimum-input-length="3"
                                data-s2-auto-height="true"
                                data-s2-on-change="gerenciarPapel.sqPessoaChange">
                              </select>
                            </div>

                            <div class="fld form-group" style="display: none">
                                <label class="control-label">Grupo avaliado</label>
                                <br>
                                <select name="sqGrupo" id="sqGrupo" class="form-control fld300 fldSelect">
                                    <option value="">?</option>
                                    <g:each var="item" in="${listGrupo}">
                                        <option data-codigo="${item.codigoSistema}" value="${item.id}">${item.descricao}</option>
                                    </g:each>
                                </select>
                            </div>

                            <br>
                            <div class="fld form-group" style="display:none">
                              <label class="control-label">Instituição</label>
                              <br>
                              <select name="sqWebInstituicao" id="sqWebInstituicao" class="form-control fld800 select2"
                                data-s2-url="main/select2WebInstituicao"
                                data-s2-placeholder="?"
                                data-s2-minimum-input-length="3"
                                data-s2-auto-height="true">
                              </select>
                            </div>

                            %{--Filtro da ficha--}%
                            <div class="hidden mt10" id="divFiltrosFicha"></div>

                            %{-- gride para selecionar ficha --}%
                            <div id="divGridFichas" style="max-height: 500px;overflow-x:hidden;overflow-y:auto;"></div>

                            %{--Botão gravar --}%
                            <div class="fld panel-footer">
                                <button type="button" data-action="gerenciarPapel.save"  class="fld btn btn-success">Gravar</button>
                                <button type="button" data-action="gerenciarPapel.reset" class="fld btn btn-danger pull-right" title="Limpar todos os campos do formulário e as fichas selecionadas no gride.">Limpar formulário</button>
                            </div>
                        </div>
                    </fieldset>

                    %{-- FILTRAR GRIDE PELO NOME DA ESPECIE --}%
                    <div class="fld400">
                        <div class="form-group">
                            <label>Localizar PF/CT/CE da espécie</label>
                            <br>
                            <div class="input-group input-group">
                                <input type="text" class="form-control ignoreChange input-bold-blue"
                                       data-enter="gerenciarPapel.updateGrid()"
                                       id="fldNoCientificoFiltrarPapel" style="display:inline">
                                <span class="input-group-addon">
                                    <i class="fa fa-search cursor-pointer" title="Localizar..." data-action="gerenciarPapel.updateGrid()"></i>
                                </span>
                                <span class="input-group-addon">
                                    <i class="fa fa-trash cursor-pointer" title="Limpar o campo" data-action="gerenciarPapel.resetNomeCientificoFiltrarPapel"></i>
                                </span>
                            </div>
                        </div>
                    </div>

                    %{-- gride para selecionar ficha --}%
                    <h4 class="mt10">Funções atribuídas</h4>
                    <div id="divGrid">
                        %{--    <g:render template="divGridFicha"></g:render> --}%
                    </div>
                </form>
            </div>
                %{-- fim panel body --}%
        </div>
        %{-- fim panel --}%
    </div>
    %{-- fim row 12 --}%
</div>
%{-- fim container --}%
