//# sourceURL=N:\SAE\sae\grails-app\views\gerenciarPapel\gerenciarPapel.js
//
;
var gerenciarPapel= {
    filtros:{},
    tabClick              : function ()
    {
        return true;
    },
    sqPapelChange:function( params ) {
        var cdPapel = $("#sqPapel option:selected").data('codigo');

        $("#sqPessoa").val(null).trigger('change');
        if( ! cdPapel || ! /^(COLABORADOR_EXTERNO|COORDENADOR_TAXON)$/.test(cdPapel) ) {
            $("#sqWebInstituicao").parent().hide();
        }
        else
        {
            $("#sqWebInstituicao").parent().show();
        }
        if( cdPapel != 'COORDENADOR_TAXON' ){
            $("#sqGrupo").parent().show();
        } else {
            $("#sqGrupo").parent().hide();
        }
    },

    sqPessoaChange:function( e ){ },

    sqCicloAvaliacaoChange: function (params) {
        var sqCicloAvaliacao = $("#gerenciarPapelContainer #sqCicloAvaliacao").val();
        if ( sqCicloAvaliacao )
        {
            $("#gerenciarPapelContainer #frmGerPapel,#divFiltrosFicha").removeClass('hidden');
            var data = {sqCicloAvaliacao: sqCicloAvaliacao,
                callback: 'gerenciarPapel.updateGridFichas',
                required:true,
                collapsed:false,
                mostrarFiltros:'comSemPapel'

            };
            app.loadModule(baseUrl + 'ficha/getFormFiltroFicha', data, '#gerenciarPapelContainer #divFiltrosFicha');
            gerenciarPapel.updateGrid();
        }
        else
        {
            $("#gerenciarPapelContainer #frmGerPapel,#divFiltrosFicha").addClass('hidden');
        }
    },
    reset:function() {
        $("#sqPapel").val('');
        $("#sqGrupo").val('');
        $("#sqPessoa").empty().trigger('change');
        $("#sqWebInstituicao").empty().trigger('change');
        // desmarcar as fichas
        $("#divGridFichas input:checkbox:checked").prop('checked',false).parent().parent().removeClass('tr-selected');
    },
    confirmarAtivarDesativar:function( params, field ) {
        var operacao = (field.value=='S' ? 'ATIVAÇÃO' : 'DESATIVAÇÃO');
        app.confirm('Confirma a <b>' + operacao+'</b> da função?',function() {
            gerenciarPapel.selectAtivoChange( params,field );
        })
    },
    selectAtivoChange:function( params,field ) {
        var data=params;
        data.sn = field.value;
        if( !data.sn )
        {
            return;
        }

        // alterar a cor da fonte do select
        $(field).removeClass('red green').addClass( (field.value=='S'?'green':'red' ) );
        app.ajax(app.url+'gerenciarPapel/setSituacao', data,
            function( res ) {
                if( res.status===0)
                {
                    gerenciarPapel.updateGrid(null,data)
                    /*
                    if( data.divFichas ) {

                        var aberto = $(data.divFichas).hasClass('in');
                        $(data.divFichas).collapse('hide');
                        $(data.divFichas).html('');
                        field.value='';
                        if( aberto )
                        {
                            var id = data.divFichas.replace(/^#/,'');
                            window.setTimeout(function(){
                                $("a[data-div-id="+id+"]").click();
                            },500)

                        }
                    }
                    else
                    {
                        $(field).closest('tr').removeClass('tr-active tr-inactive').addClass(data.sn=='S' ? 'tr-active' :'tr-inactive' );
                        $(field).removeClass('red green').addClass(data.sn=='S' ? 'green' :'red' );
                    }
                    */
                }
                else
                {
                    if( data.id ) {
                        field.value = 'N';
                    }
                }
        });
    },
    updateGridFichas:function( filtros ) {
        if( filtros.count  )
        {
            gerenciarPapel.filtros = filtros;
        }
        else
        {
            $("#divTbGerPapelFichas").html('');
            return;
        }
        if (! $("#gerenciarPapelContainer #sqCicloAvaliacao").val())
        {
            $("#gerenciarPapelContainer #divGridFichas").html('');
            return;
        }
        var data = gerenciarPapel.filtros || {};
        data.sqCicloAvaliacao = $("#gerenciarPapelContainer #sqCicloAvaliacao").val();

        var posicao = $("#gerenciarPapelContainer #divGridFichas").scrollTop();
        $("#gerenciarPapelContainer #divGridFichas").html('<h4>Carregando. ' + window.grails.spinner+'</h4><br>');
        //app.blockElement('#gerenciarPapelContainer #divGridFichas', 'Carregando gride...');
        app.ajax(app.url+'gerenciarPapel/getGridFichas', data,
            function(res) {
                //app.unblockElement('#gerenciarPapelContainer #divGridFichas');
                $("#gerenciarPapelContainer #divGridFichas").html( res );
                $("#gerenciarPapelContainer #divGridFichas").scrollTop( posicao );

                // ativar seleção de linhas com shift+click
                $("#gerenciarPapelContainer #divGridFichas").shiftSelectable();
            });
    },
    resetNomeCientificoFiltrarPapel:function(){
        $("#gerenciarPapelContainer #fldNoCientificoFiltrarPapel").val('');
        gerenciarPapel.updateGrid();
    },
    updateGrid:function( evt, params ) {
        var data={};
        if( params ) {
           data = app.params2data(params,data);
        }
        data.noCientificoFiltro = $("#gerenciarPapelContainer #fldNoCientificoFiltrarPapel").val();
        data.sqCicloAvaliacao=$("#gerenciarPapelContainer #sqCicloAvaliacao").val();
        app.ajax(app.url+'gerenciarPapel/getGrid', data,
            function(res) {
                if( params && params.rowNum ) {
                    // atualizar somente a linha do grid que foi alterada
                    $(res).find('tr[id^=gdFichaRow]').map( function(i,tr ) {
                        var $tr = $(tr);
                        if( $tr.size() == 1 ) {
                            $("#" + tr.id).html($tr.html())
                        }
                    });
                } else {
                    $("#divGrid").html(res);
                }
            },'Processando...');
    },
    save:function( params ) {
        // verificar se tem alguma ficha selecionada
        if( $("#divGridFichas input:checkbox:checked").size()==0)
        {
            app.alertInfo('Nenhuma ficha selecionada!');
            return;
        }
        if( $("#sqWebInstituicao").is(':visible') && !$("#sqWebInstituicao").val() )
        {
            app.alertInfo('Informe a instituição!');
            return;
        }
        if( $("#sqGrupo").is(':visible') && !$("#sqGrupo").val() )
        {
            app.alertInfo('Informe o grupo avaliado!');
            return;
        }
        if( ! $("#sqGrupo").is(':visible') ) {
            $("#sqGrupo").val('');
        }
        if( ! $("#sqWebInstituicao").is(':visible') ) {
            $("#sqWebInstituicao").val('');
        }

        var data = $("#frmGerPapel").serializeArray();
        data = app.params2data(params,data,'frmGerPapel');
        app.ajax(app.url+'gerenciarPapel/save', data,
            function(res) {
                if( res.status == 0 )
                {
                    // desmarcar as fichas
                    $("#divGridFichas input:checkbox:checked").prop('checked',false);
                    $("#tbGerPapelFichas thead th i.glyphicon-check").removeClass('glyphicon-check').addClass('glyphicon-unchecked');
                    gerenciarPapel.updateGrid();
                }
            });
    },
    deleteAllFichas:function(params,) {
        app.confirm('<br>Confirma exclusão de todas as fichas?',
            function() {
                app.ajax(app.url+'gerenciarPapel/deleteAllFichas',
                    params,
                    function(res) {
                        if( res.status == 0 )
                        {
                            gerenciarPapel.updateGrid();
                        }
                    });
            },
            function() {
                //app.unselectGridRow(btn);
            }, params, 'Confirmação', 'warning');
    },
    delete:function( params, btn) {
        app.selectGridRow(btn);
        app.confirm('<br>Confirma exclusão?',
            function() {
                app.ajax(app.url+'gerenciarPapel/delete',
                    params,
                    function(res) {
                        if( res.status == 0 )
                        {
                            gerenciarPapel.updateGrid();
                        }
                    });
            },
            function() {
                app.unselectGridRow(btn);
            }, params, 'Confirmação', 'warning');
    },

    getListaFichas:function( params, ele,evt) {
        var $div = $("#"+params.divId)
        if( $div.text().trim() != '' )
        {
            return
        }
        $div.html('<h5 style="text-align: center;"><b>Carregando...&nbsp;<i class="glyphicon glyphicon-refresh spinning"></i></b></h5><br>');
        app.ajax(app.url+'gerenciarPapel/getFichasPessoaFuncao',
            params,
            function(res) {
                $div.html(res);
            });
    }

    /*sqNivelTaxonomicoChange:function(params)
    {
        $("#frmGerPapel #sqTaxonFiltro").empty();
        if ($("#frmGerPapel #nivelFiltro").val())
        {
            $("#frmGerPapel #sqTaxonFiltro").parent().removeClass('hide');
        }
        else
        {
            $("#frmGerPapel #sqTaxonFiltro").parent().addClass('hide');
            gerenciarPapel.updateGrid();
        }
    },
    sqTaxonFiltroChange : function(params)
    {
        gerenciarPapel.applyFilter(params);
    },
    */
    /*applyFilter : function( params )
    {
        try{
            var coNivel = $("#nivelFiltro").val();
            var objData    = $("#sqTaxonFiltro").select2('data')
            $("#tbGerPapelFichas td").each( function() {
                var data = $(this).data('structure');
                if( data )
                {
                    if( !objData[0] || data[coNivel] == objData[0].descricao  )
                    {
                        $(this).parent().show();
                    }
                    else
                    {
                        $(this).parent().hide();
                    }
                }
            });
        } catch( e ){}
    }
    */
}
// inicializar a tela
setTimeout(function(){
    gerenciarPapel.sqCicloAvaliacaoChange();
},1000);

var filtrarRows = function( params,elm,evt){
    var colNum = params.colnum;
    var tableId = $(elm).data('table');
    var filterClass = $(elm).data('filterClass') || 'tr';
    var $rows;
    if( tableId ) {
        $rows = $( '#'+tableId.replace(/^#/,'') ).find('tr.'+filterClass);
    }
    else
    {
        $rows = $(elm).closest('table').find('tr.'+filterClass);
    }

    // limpar os outros selects de filtro
    if( elm.tagName == 'SELECT') {
        $('select.select-data-table').map(function (i, e) {
            if ( e != elm ) {
                $(e).val('');
            }
        });
    }

    if( $rows.size() == 0)
    {
        return;
    }
    //$(elm).closest('table').find('tbody tr.tr').each( function(i,tr) {
    var qtdShow = 0;
    var qtdHide = 0;
    var trPai  = ''
    $rows.each( function(i,tr) {
        if( $(tr).data('trPai') ) {
            if( trPai != $(tr).data('trPai')  )
            {
                if( trPai != '' )
                {
                    if ( qtdShow > 0 || qtdHide == 0  ) {
                        $('#' + trPai).show();
                    }
                    else if ( qtdHide > 0) {
                        $('#' + trPai).hide();
                    }
                }
                trPai = $(tr).data('trPai');
                qtdShow = 0;
                qtdHide = 0;
            }
        }
        else
        {
            $(tr).find('tr.subtr').show();
        }
        if (tr.innerText) {
            if (!elm.value || tr.innerHTML.indexOf(elm.value) > -1) {
                $( tr ).show();
                qtdShow++;
            }
            else
            {
                $(tr).hide();
                qtdHide++;
            }
        }
    });
    if( trPai != '' ) {
        if ( qtdShow > 0 || qtdHide == 0  ) {
            $('#' + trPai).show();
        }
        else if ( qtdHide > 0) {
            $('#' + trPai).hide();
        }
    }
}
