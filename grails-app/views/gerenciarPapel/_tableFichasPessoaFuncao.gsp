<!-- view: /views/gerenciarConsulta/_tableFichasPessoaFuncao.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<div style="max-height:400px;overflow-y:auto;diplay:block;">
    <table class="table table-hover table-condensed table-stripped">
       <g:each var="fichaPessoa" in="${listFichas}" status="i">
           <tr class="${fichaPessoa.in_ativo != 'S' ? 'tr-inactive' : 'tr-active'}">
               <td style="width:auto">${ raw( (i+1) +') ' + br.gov.icmbio.Util.ncItalico(fichaPessoa.nm_cientifico,true,true,fichaPessoa.co_nivel_taxonomico) )}</td>
               %{--<td style="text-align:center;width:120px;">--}%
                   %{--<select data-change="gerenciarPapel.selectAtivoChange" data-id="${fichaPessoa.id}" class="form-control ${fichaPessoa.inAtivo != 'S' ? 'red' : 'green'}">--}%
                       %{--<option value="S" ${fichaPessoa.inAtivo != 'N' ? ' selected':''}>Ativo</option>--}%
                       %{--<option value="N" ${fichaPessoa.inAtivo == 'N' ? ' selected':''}>Não Ativo</option>--}%
                   %{--</select>--}%
               %{--</td>--}%
               <td style="text-align:center;width:50px;">
                   <a data-action="gerenciarPapel.delete"
                      data-id="${fichaPessoa.sq_ficha_pessoa}"
                      class="btn btn-default btn-xs btn-delete" title="Excluir">
                       <span class="glyphicon glyphicon-remove"></span>
                   </a>
               </td>
           </tr>
       </g:each>
        <g:if test="${listFichas.size() >0 }">
            <tr>
                <td colspan="2" class="text-right">

                    <button type="button"
                            data-action="gerenciarPapel.deleteAllFichas"
                            data-sq-ciclo-avaliacao="${sqCicloAvaliacao}"
                            data-sq-hash="${params?.sqHash}"
                            data-sq-pessoa="${sqPessoa}"
                            data-sq-papel="${sqPapel}"
                            data-sq-grupo="${sqGrupo}"
                            data-sq-unidade-org="${sqUnidadeOrg}"
                            data-sq-unidade-ficha="${sqUnidadeFicha}"
                            data-in-tipo-unidade="${inTipoUnidade}"
                            class="btn btn-xs btn-danger">Excluir todas as fichas</button>
                </td>
            </tr>
        </g:if>
    </table>
</div>
