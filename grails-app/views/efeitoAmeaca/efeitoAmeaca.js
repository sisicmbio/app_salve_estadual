//# sourceURL=/views/efeitoAmeaca/efeitoAmeaca.js
;
var treeviewAmeaca;
var treeviewEfeito;
( function() {
    var efeitoAmeaca = {
        ameacaSelecionada: '',
        $btnGravar: $("#btnGravar"),
        init: function () {
            efeitoAmeaca.$btnGravar.on('click', function () {
                gravar();
            });

            // ajustar max-height
            var $rowContent = $("#rowContent");
            var maxH = $(document).height() - 210;
            $rowContent.height(maxH);
            // Inicializar treeview de ameaças
            treeviewAmeaca = $("#treeviewAmeacas").fancytree({
                icon: true, // Display node icons
                extensions: ["table", "filter"],
                tabindex: "1",
                checkbox: false,
                source: {
                    url: app.url + "efeitoAmeaca/getTreeViewAmeaca",
                    data: {},
                    cache: false,
                    dataType: "json",
                    loadError: function (e, data) {
                        alert('servidor ainda fora do ar!');
                        var error = data.error;
                        if (error.status && error.statusText) {
                            data.message = "Ajax error: " + data.message;
                            data.details = "Ajax error: " + error.statusText + ", status code = " + error.status;
                        } else {
                            data.message = "Custom error: " + data.message;
                            data.details = "An error occurred during loading: " + error;
                        }
                        ;
                    },
                    error: function (xhr, message) {
                        alert('servidor fora do ar : ' + xhr.statusText);
                    }
                },

                click: function (event, data) {
                    // https://github.com/mar10/fancytree/wiki/TutorialEvents
                    var node = data.node;
                    var tt = $.ui.fancytree.getEventTargetType(event.originalEvent);
                    if (tt == 'title') {
                        data.node.toggleExpanded();
                    }
                },
                select: function (event, data) {
                    var node = data.node;
                    if (node) {
                        if (node.isSelected()) {
                            node.setExpanded(true);
                        }
                    }
                },
                lazyLoad: function (event, data) {
                    //console.log(data.node.key)
                    data.result = {
                        url: app.url + "efeitoAmeaca/getTreeViewAmeaca?key=" + data.node.key, //"fancytree-master/demo/ajax-sub2.json",
                        error: function (xhr, message) {
                            alert('servidor fora do ar : ' + xhr.statusText);
                        },
                        loadError: function (e, data) {
                            alert('servidor ainda fora do ar!');
                            var error = data.error;
                            if (error.status && error.statusText) {
                                data.message = "Ajax error: " + data.message;
                                data.details = "Ajax error: " + error.statusText + ", status code = " + error.status;
                            } else {
                                data.message = "Custom error: " + data.message;
                                data.details = "An error occurred during loading: " + error;
                            }
                        },
                    }
                },
                //---------------------------------------------------------------
                init: function (event, data) {
                    treeviewAmeaca = $("#treeviewAmeacas").fancytree("getTree");
                },
                //---------------------------------------------------------------
                table: {
                    indentation: 10, // identação da hieraquia
                },
                //---------------------------------------------------------------
                activate: function (event, data) {
                    efeitoAmeaca.ameacaSelecionada = data.node.data.id;
                    marcarEfeitos(data.node.data.id);
                }
            });// fim treeview

            // ativar função de filtragem // http://wwwendt.de/tech/fancytree/demo/sample-ext-filter.html#
            $("#fldFiltrarTreeViewAmeaca").keyup(function (e) {
                applyFilter(e.target.value, treeviewAmeaca);
            }).focus();

            // Initializar treeview de efeitos
            $("#treeviewEfeitos").fancytree({
                icon: true, // Display node icons
                extensions: ["table", "filter"],
                selectMode: 3, // ao selecionar um filho selecionar os pais automaticamente
                tabindex: "1",
                checkbox: true,
                source: {
                    url: app.url + "efeitoAmeaca/getTreeViewEfeito",
                    data: {sqAmeaca: efeitoAmeaca.ameacaSelecionada},
                    cache: false,
                    dataType: "json",
                    loadError: function (e, data) {
                        alert('servidor ainda fora do ar!');
                        var error = data.error;
                        if (error.status && error.statusText) {
                            data.message = "Ajax error: " + data.message;
                            data.details = "Ajax error: " + error.statusText + ", status code = " + error.status;
                        } else {
                            data.message = "Custom error: " + data.message;
                            data.details = "An error occurred during loading: " + error;
                        }
                    },
                    error: function (xhr, message) {
                        alert('servidor fora do ar : ' + xhr.statusText);
                    }
                },
                click: function (event, data) {
                    // https://github.com/mar10/fancytree/wiki/TutorialEvents
                    var node = data.node;
                    var tt = $.ui.fancytree.getEventTargetType(event.originalEvent);
                    if (tt == 'title') {
                        data.node.toggleExpanded();
                    }
                    efeitoAmeaca.$btnGravar.prop('disabled', false)
                    efeitoAmeaca.$btnGravar.addClass('btn-primary');
                },
                select: function (event, data) {
                    var node = data.node;
                    if (node) {
                        if (node.isSelected()) {
                            node.setExpanded(true);
                        }
                    }
                    ;
                },
                lazyLoad: function (event, data) {
                    //console.log(data.node.key)
                    data.result = {
                        url: app.url + "efeitoAmeaca/getTreeViewEfeito?key=" + data.node.key, //"fancytree-master/demo/ajax-sub2.json",
                        error: function (xhr, message) {
                            alert('servidor fora do ar : ' + xhr.statusText);
                        },
                        loadError: function (e, data) {
                            alert('servidor ainda fora do ar!');
                            var error = data.error;
                            if (error.status && error.statusText) {
                                data.message = "Ajax error: " + data.message;
                                data.details = "Ajax error: " + error.statusText + ", status code = " + error.status;
                            } else {
                                data.message = "Custom error: " + data.message;
                                data.details = "An error occurred during loading: " + error;
                            }
                        },
                    };
                },
                //---------------------------------------------------------------
                init: function (event, data) {
                    //curNodeEfeito = data.tree.getFirstChild();
                    treeviewEfeito = $("#treeviewEfeitos").fancytree("getTree");
                },
                //---------------------------------------------------------------
                table: {
                    indentation: 15, // identação da hieraquia
                },
                //---------------------------------------------------------------
                activate: function (event, data) {
                    //curNodeEfeito = data.node;
                }
            });// fim treeview efeito

            // ativar função de filtragem // http://wwwendt.de/tech/fancytree/demo/sample-ext-filter.html#
            $("#fldFiltrarTreeViewEfeito").keyup(function (e) {
                applyFilter(e.target.value, treeviewEfeito);
            }).focus();

        },
    };

    efeitoAmeaca.init();

    //-----------------------------------------------------------------------------
    var marcarEfeitos = function (sqAmeaca) {
        // desmarcar todos os marcados
        treeviewEfeito.getSelectedNodes().map(function (node, index) {
            node.setSelected(false);
        });
        app.blockElement('#treeviewEfeitos');
        ajaxJson('/salve-estadual/efeitoAmeaca/getEfeitosAmeaca', {sqAmeaca: sqAmeaca}, null, function (res) {
            app.unblockElement("#treeviewEfeitos");
            if (res.ids) {
                treeviewEfeito.visit(function (node) {
                    node.setSelected(res.ids.indexOf(parseInt(node.data.id)) > -1)
                });
            }
        });
    };
    //-----------------------------------------------------------------------------
    var gravar = function () {
        efeitoAmeaca.$btnGravar.prop('disabled', true);
        efeitoAmeaca.$btnGravar.removeClass('btn-primary');
        var data = {sqAmeaca: efeitoAmeaca.ameacaSelecionada, idEfeitos: []};
        treeviewEfeito.getSelectedNodes().map(function (node, index) {
            data.idEfeitos.push(node.data.id);
            data.idEfeitos = getParentIds(node, data.idEfeitos);
        });
        ajaxJson('/salve-estadual/efeitoAmeaca/gravar', data, 'Gravarndo...', function (res) {
            console.log(res);
            marcarEfeitos(data.sqAmeaca);
        });
    };


    var getParentIds = function (node, idEfeitos) {

        if (node.parent.data.id && idEfeitos.indexOf(node.parent.data.id) == -1) {
            idEfeitos.push(node.parent.data.id);
            return getParentIds(node.parent, idEfeitos);
        }
        return idEfeitos

    };

    var applyFilter = function( match, tree )
    {
        var opts = {
            autoApply: true,   // Re-apply last filter if lazy data is loaded
            autoExpand: true, // Expand all branches that contain matches while filtered
            counter: true,     // Show a badge with number of matching child nodes near parent icons
            fuzzy: false,      // Match single characters in order, e.g. 'fb' will match 'FooBar'
            hideExpandedCounter: false,  // Hide counter badge if parent is expanded
            hideExpanders: false, // Hide expanders if all child nodes are hidden by filter
            highlight: true,   // Highlight matches by wrapping inside <mark> tags
            leavesOnly: false, // Match end nodes only
            nodata: true,      // Display a 'no data' status node if result is empty
            mode: "hide"       // dimm = Grayout unmatched nodes (pass "hide" to remove unmatched node instead)
        };
        if (!match) {
            tree.clearFilter();
            return
        }
        // Pass a string to perform case insensitive matching
        var filterFunc = tree.filterNodes;
        filterFunc.call(tree, match, opts);

    }

})();
