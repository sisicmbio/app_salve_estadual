<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="ajax"/>
    <title>
        SALVE - EfeitoxAmeaça
    </title>
</head>
<body>
<div class="col-sm-12">
    <div class="panel panel-default w100p" style="" id="panelEfeitoAmeaca">
        <div class="panel-heading">
            <span>
                Manutenção dos Efeitos X Ameaça
            </span>
            <button id="btnClose" data-action="app.unloadModule" class="btn btn-default pull-right btn-xs" title="Fechar"><span class="glyphicon glyphicon-remove btnClose"></span></button>
        </div>
        <div class="panel-body">
            <div class="row" style="height:auto;overflow-y: auto;" id="rowContent">
                <div class="col-sm-6">
                    <input type="text" id="fldFiltrarTreeViewAmeaca" value="" class="form-control mb5" style="width:100% !important" placeholder="Localizar ameaça..." autocomplete="off"/>
                    <table id="treeviewAmeacas" class="table table-condensed table-hover table-striped fancytree-fade-expander treeTable">
                        <colgroup>
                            <col width="500"></col>
                        </colgroup>
                        <thead>
                        <tr>
                            <th>Ameaças</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>

                </div>

                <div class="col-sm-6">
                    <input type="text" id="fldFiltrarTreeViewEfeito" value="" class="form-control mb5" style="width:100% !important" placeholder="Localizar efeito..." autocomplete="off"/>
                    <table id="treeviewEfeitos" class="table table-condensed table-hover table-striped fancytree-fade-expander treeTable">
                        <colgroup>
                            <col width="500"></col>
                        </colgroup>
                        <thead>
                        <tr>
                            <th>Efeitos</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>

                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 text-center">
                    <button id="btnGravar" class="btn btn-default" disabled>Gravar</button>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>