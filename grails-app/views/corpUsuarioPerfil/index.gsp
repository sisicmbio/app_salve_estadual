<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="ajax"/>
</head>
<body>
<div class="col-sm-12">
    <div class="panel panel-default w100p" id="pnlUsuario">
        <div class="panel-heading">
            <span>
                Gerenciar Perfil de Acesso
            </span>
            <button id="btnClose" data-action="app.unloadModule" class="btn btn-default pull-right btn-xs ml20" title="Fechar"><span class="glyphicon glyphicon-remove btnClose"></span></button>
            <button id="btnBack" data-action="app.loadModule('corpUsuario',{title:'Usuários'})" class="btn btn-info pull-right btn-xs" title="Voltar para tela de usuários"><i class="fa fa-arrow-left mr5"></i><span>Voltar</span></button>

        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-sm-12">
                    <g:if test="${!usuario}">
                        <div class="alert alert-danger">Usuário não encontrado</div>
                    </g:if>
                    <g:else>
                        <form id="frmUsuarioPerfil" class="form-inline">

                            %{--CAMPO ID--}%
                            <input type="hidden" id="sqPessoa" name="sqPessoa" value="${usuario.id}">


                            %{-- NOME --}%
                            <div class="form-group w100p">
                                <label class="control-label">Nome</label><br>
                                <input type="text" class="form-control w100p disabled" readonly value="${usuario.pessoaFisica.pessoa.noPessoa}">
                            </div>

                            <fieldset id="perfisFichaContainer" class="mt20 w100p">
                                <legend>Perfis de acesso geral</legend>
                                <div class="d-flex flex-column" style="max-width: 680px;">
                                    <g:each var="perfil" in="${perfisGeral}">
                                        <label class="label-checkbox">
                                            <input type="checkbox"
                                                   name="sqPerfilGeral"
                                                   class="checkbox-lg" value="${perfil.sqPerfil}" ${ usuario.hasPerfil(perfil.cdSistema) ? 'checked' : ''}>&nbsp;${perfil.noPerfil}
                                        </label>
                                    </g:each>
                                </div>
                            </fieldset>



                            <fieldset id="perfisFichaContainer" class="mt20 w100p">
                                <legend>Perfis de acesso por ficha atribuída (externos)</legend>
                                <div class="d-flex flex-column" style="max-width: 680px;">
                                    <g:each var="perfil" in="${perfisFicha}">
                                        <label class="label-checkbox">
                                            <input type="checkbox"
                                                   name="sqPerfilFicha"
                                                   class="checkbox-lg" value="${perfil.sqPerfil}" ${ usuario.hasPerfil(perfil.cdSistema) ? 'checked' : ''}>&nbsp;${perfil.noPerfil}
                                        </label>
                                    </g:each>
                                </div>
                            </fieldset>

                            %{-- PERFIL --}%
                            <fieldset id="perfisContainer" class="mt20 w100p">
                                <legend>Perfis de acesso por instituição</legend>
                                %{-- TIPO DE INSTITUICAO --}%
                                <div class="form-group">
                                    <label for="sqTipoInstituicao" class="label-required">Tipo de instituição</label><br>
                                    <select class="form-control fld300" id="sqTipoInstituicao" name="sqTipoInstituicao">
                                        <option value="" selected></option>
                                        <g:each var="item" in="${tiposInstituicao}">
                                            <option value="${item.id}" ${item.codigoSistema=='UNIDADE_ORGANIZACIONAL' ? 'selected':''}>${item.descricao}</option>
                                        </g:each>

                                    </select>
                                </div>
                                <br>

                                %{-- INSTITUICAO--}%
                                <div class="form-group w100p">
                                    <label for="sqInstituicao" class="label-required">Instituição</label><br>
                                    <select id="sqInstituicao" name="sqInstituicao" class="form-control select2 fld500"
                                            data-s2-params="sqTipoInstituicao,stInterna:S"
                                            data-s2-url="corpUsuarioPerfil/select2Instituicao"
                                            data-s2-on-change="corpUsuarioPerfil.instituicaoChange"
                                            data-s2-placeholder="?"
                                            data-s2-minimum-input-length="3"
                                            data-s2-maximum-selection-length="1">
                                            %{-- INICIALIZAR COM A LOTAÇÃO ATUAL DO USUÁRIO --}%
                                            <g:if test="${usuario?.pessoaFisica?.instituicao}">
                                                <option value="${usuario.pessoaFisica.instituicao.id}">
                                                        ${usuario.pessoaFisica.instituicao.sgInstituicao}
                                                 </option>
                                            </g:if>
                                    </select>
                                </div>

                                <div class="form-group w100p">
                                    <label class="label-required">Perfil</label>
                                    <div class="d-flex flex-column" style="max-width: 680px;">
                                        <g:each var="perfil" in="${perfisInstituicao}">
                                            <label class="label-checkbox">
                                                <input type="checkbox"
                                                       name="sqPerfilInstituicao"
                                                       class="checkbox-lg" value="${perfil.sqPerfil}">&nbsp;${perfil.noPerfil}
                                            </label>
                                        </g:each>
                                    </div>
                                </div>

%{--                                LISTAGEM DOS PERFIS POR INSTITUIÇÃO--}%
                                <div class="form-group w100p">
                                    <label class="control-label">Perfis atribuidos por instituição</label>
                                    <div id="divListaPerfilsPorInstituicaoWrapper" style="border-top:2px solid gray; padding-top:10px;height: auto; min-height: 100px;max-height: 500px; overflow-y: auto">
                                        <g:if test="${perfisInstituicao}">
                                            <span><p>Carregando...</p></span>
                                        </g:if>
                                        <g:else>
                                            <span><p>Nenhum perfil atribuido</p></span>
                                        </g:else>
                                    </div>
                                </div>
                            </fieldset>


                            %{--BOTOES--}%
                            <div class="panel-footer col-sm-12">
                                <button type="button" id="btnSave" onclick="corpUsuarioPerfil.save();" class="btn btn-success">Gravar</button>
                                <button type="button" class="btn btn-danger pull-right" onclick="corpUsuarioPerfil.reset();">Limpar</button>
                            </div>
                            <div class="col-sm-12"><small class="red fs10">* - campo obrigatório</small></div>
                        </form>
                    </g:else>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
