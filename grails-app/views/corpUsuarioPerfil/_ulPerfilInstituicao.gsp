<!-- view: /views/corpUsuarioPerfil/_ulPerfisPorInstituicao.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<g:if test="${perfisInstituicao}">
<ul>
    <g:each var="perfilInstituicao" in="${perfisInstituicao}">
        <li class="nowrap">
            <b>${perfilInstituicao.sgInstituicao}</b>
            <i class="fa fa-edit cursor-pointer blue" title="Altlerar perfil de acesso"
               data-sq-tipo="${perfilInstituicao.sqTipo}"
               data-sg-instituicao="${perfilInstituicao.sgInstituicao}"
               data-sq-instituicao="${perfilInstituicao.sqInstituicao}"
               data-action="corpUsuarioPerfil.edit"></i>
        </li>
        <ol>
            <g:each var="perfil" in="${perfilInstituicao.perfis}">
                <li class="nowrap" id="li-${perfil.sqUsuarioPerfilInstituicao}">
                    %{--<i class="fa fa-trash cursor-pointer red" title="Excluir o perfil"
                       data-sq-usuario-perfil-instituicao="${perfil.sqUsuarioPerfilInstituicao}"
                       data-no-perfil="${perfil.noPerfil}"
                       data-action="corpUsuarioPerfil.delete"></i>--}%
                    <span>${perfil.noPerfil}</span>
                </li>
            </g:each>
        </ol>
    </g:each>
</ul>
</g:if>
<g:else>
    <p>Nenhum perfil atribuido</p>
</g:else>