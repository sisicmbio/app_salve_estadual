//# sourceURL=corpUsuarioPerfil.js
;var corpUsuarioPerfil = {

    init: function () {
        corpUsuarioPerfil.updatePerfilInstituicao();
    },
    reset:function(){
        app.reset('frmUsuarioPerfil','sqPessoa,noPessoa,stInterna,sqTipoInstituicao');
    },
    save:function(){
        var data = app.serializeFields('frmUsuarioPerfil');
        app.ajax(baseUrl+'corpUsuarioPerfil/save',data,function(res){
            if( res.status == 0 ) {
                corpUsuarioPerfil.updatePerfilInstituicao();
            }
        },'Gravando...')
    },
    edit:function( params, ele, evt ) {
        if( params.sqInstituicao && params.sgInstituicao ){
            $("#sqTipoInstituicao").val( String(params.sqTipo ) );
            $("#sqTipoInstituicao").change();
            app.setSelect2('sqInstituicao',{id:params.sqInstituicao
                ,descricao:params.sgInstituicao}
                ,params.sqInstituicao,'',true)
        }
    },
    delete:function( params,ele, evt ) {
        app.confirm("Confirma e EXCLUSÃO do perfil <b>"+params.noPerfil+"</b> da instituição?",function(){
            var data = app.params2data(params,{});
            app.ajax(baseUrl+'corpUsuarioPerfil/delete',data,function(res){
                if( res.status == 0 ) {
                    if( data.sqUsuarioPerfilInstituicao ){
                        $("#li-"+data.sqUsuarioPerfilInstituicao).remove();
                    }
                }
            },'Excluíndo o perfil...')
        });

    },
    updatePerfilInstituicao:function(params){
        var data={sqPessoa:$("#sqPessoa").val()}
        app.blockElement('divListaPerfilsPorInstituicaoWrapper','Atualizando...',0,function(){
            app.ajax(baseUrl+'corpUsuarioPerfil/ulPerfilInstituicao',data,function(res){
                app.unblockElement("divListaPerfilsPorInstituicaoWrapper");
                var $div = $("#divListaPerfilsPorInstituicaoWrapper");
                if( res.status == 0 ) {
                    $div.html(res.html );
                } else {
                    $div.html('<p class="red">'+res.msg+'</p>');
                }
            })
        });

    },
    voltar:function( sqPessoa ){
        app.loadModule('corpUsuario')
    },
    instituicaoChange:function(){
        // desmarcar todos os perfis
        $("#perfisContainer").find('input:checkbox:checked').prop('checked',false);
        var data = { sqPessoa: $("#sqPessoa").val()
                  , sqInstituicao: $("#sqInstituicao").val()
        }
        if( !data.sqInstituicao){
            return;
        }
        app.ajax(baseUrl+'corpUsuarioPerfil/edit',data,function(res){
            if( res.status == 0 && res.data) {
                res.data.map(function(sqPerfil){
                    $("#perfisContainer").find('input:checkbox[value='+sqPerfil+']').prop('checked',true);
                });
            }
        },'')
    }
}
setTimeout(function(){corpUsuarioPerfil.init(),1000});