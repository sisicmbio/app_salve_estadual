<table class="mt10 table table-condensed table-hover table-borderless table-informativos table-informativos">
    <thead>
    <tr>
        <th>Nº</th>
        <th>Data</th>
%{--        <th>Período alerta</th>--}%
        <th>Publicado?</th>
        <th>Tema</th>
%{--        <th>Mensagem alerta</th>--}%
        <th>Arquivo</th>
        <th>Ação</th>
    </tr>
    </thead>
    <tbody>
    <g:each var="item" in="${listInformativos}" status="i">
        <tr id="tr-grid-informativo-${item.id}">
            <td class="text-center" style="width: 40px;">${ item.nuInformativo}</td>
            <td class="text-center" style="width: 90px;">${ item.dtInformativo.format('dd/MM/yyyy')}</td>
            %{--<td class="text-center" style="width: 90px;">${ item.periodoAlerta }
                <g:if test="${item.periodoAlerta}">
                <i class="ml-2 fa fa-group blue" data-sq-informativo="${item.id}" data-action="informativo.showPessoas"
                    title="Clique para visualizar os usuários que já leram."></i>
                </g:if>
            </td>--}%
            <td class="text-center" style="width: 90px;color:${item.inPublicado== 'S' ? 'green':'red'}">${ item.inPublicado=='S' ? 'Sim' :'Não' }</td>
            <td class="text-left" style="width: auto;">${item.deTema}</td>
%{--            <td class="text-justify" style="width: auto;">${ raw( item.txInformativo.replaceAll(/\n/,'<br>')) }</td>--}%
            <td class="text-left" style="width: auto;">${ item.noArquivo }</td>
            <td class="td-actions text-left" style="width:80px;">
                <a data-action="informativo.edit" data-sq-informativo="${item.id}" class="fld btn btn-default btn-xs btn-update" title="Alterar">
                    <span class="glyphicon glyphicon-pencil"></span>
                </a>
                <a data-action="informativo.delete" data-descricao="${item?.deTema}" data-sq-informativo="${item.id}" class="fld btn btn-default btn-xs btn-delete" title="Excluir">
                    <span class="glyphicon glyphicon-remove"></span>
                </a>
                <g:if test="${item.deLocalArquivo}">
                    <a href="/salve-estadual/download?type=informativo&fileName=${item.deLocalArquivo}" title="Baixar">
                        <i class="fa fa-download"></i>
                    </a>
                </g:if>

            </td>
        </tr>
    </g:each>
    </tbody>
</table>
