<!DOCTYPE html>
<html>
    <head>
        <g:set var="appName" value="${grailsApplication.metadata.getApplicationName().toUpperCase()}"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta http-equiv="content-language" content="pt-br">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="keywords" content="SALVE, ICMBIo, Avaliação, Espécie, Sistema - SALVE" />
        <meta name="reply-to" content="${raw(grailsApplication.config.app.email)}">
        <meta name="author" content="Instituto Chico Mendes de Conservação da Biodiversidade">
        <meta name="rights" content="Instituto Chico Mendes de Conservação da Biodiversidade" />
        <meta name="robots" content="index,nofollow">
        <meta name="googlebot" content="index,nofollow">
        <meta name="description" content="Sistema de Avaliação do Risco de Extinção da Biodiversidade - SALVE" />
        <meta name="generator" content="Grails 2 - Framework" />
        <meta name="language" content="pt-BR">
        <title>Salve - Assinatura Eletrônica</title>
        <link rel="shortcut icon" href="${assetPath(src: 'favicon.ico')}" type="image/x-icon">
        <link href="${assetPath(src: 'fonts.css')}" rel="stylesheet">
        <link rel="icon" href="${assetPath(src: 'favicon.ico')}" type="image/png">
        <link href="${assetPath(src: '/bootstrap/v336/css/bootstrap.css')}" rel="stylesheet">
        <link href="${assetPath(src: '/bootstrap/plugins/dialog/dialog.min.css')}" rel="stylesheet">
        <link href="${assetPath(src: 'api.css')}" rel="stylesheet">
    </head>
    <body>
        <div class="navbar navbar-default navbar-fixed-top app-navbar" id="appDivHeader">
            <div class="container-fluid">
                <div class="navbar-header">
                    <div class="app-title">${raw(grailsApplication.config.app.title + ' - ' + appName.toUpperCase())}<span style="">v.${grailsApplication.metadata.getApplicationVersion()}</span></div>
                </div>
                <ul class="nav navbar-nav navbar-right visible-lg">
                    <li style="margin-top:-7px;margin-right:-15px;"><a style="height:75px; !important;" href="http://www.icmbio.gov.br" target="_blank"><img src="${assetPath(src:'marcaICMBio.png')}"></a></li>
                </ul>
            </div>
        </div>
        <div class="container-fluid">
            <g:if test="${ mensagem }">
                <div class="alert alert-danger" style="display: block">${ raw( mensagem )}</div>
            </g:if>
            <g:layoutBody />
        </div>
        <script src="${assetPath(src: '/jquery/v224/jquery.min.js')}"></script>
        <script src="${assetPath(src: '/jquery/plugins/blockUI/blockUI.js')}"></script>
        <script src="${assetPath(src: '/bootstrap/v336/bootstrap.js')}"></script>
        <script src="${assetPath(src: '/bootstrap/plugins/dialog/dialog.min.js')}"></script>
        <g:layoutHead />
    </body>
</html>
