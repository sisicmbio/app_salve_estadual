<%@ page import="br.gov.icmbio.Util" %>
<html lang="pt-br" class="no-js">
<head>
	<g:set var="oSND"  value="${[ [key:'S',value:'Sim'], [key:'N', value:'Não'] ,[key:'D', value:'Desconhecido'] ]}"/>
	<g:set var="appName" value="${grailsApplication.metadata.getApplicationName().toLowerCase()}"/>
	<g:set var="version" value="${grailsApplication.metadata['app.version']}"/>
	<g:set var="tag" value="${grailsApplication.metadata['app.tag']}"/>
	<g:set var="noUsuario" value="${ br.gov.icmbio.Util.nomeAbreviado( session?.sicae?.user?.noUsuario ) }"/>
	<g:set var="noInstituicao" value="${ session?.config?.noInstituicao}"/>
	<g:set var="sgInstituicao" value="${ session?.config?.sgInstituicao}"/>
	<g:set var="linkInstituicao" value="${ session?.config?.dsLink}"/>
	<g:set var="siglaEstadoInstituicao" value="${ session?.config?.sgEstado ?:''}"/>

	<g:if env="production">
		<g:set var="env" value="production"/>
	</g:if>
	<g:else>
		<g:set var="env" value="desenv"/>
	</g:else>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta http-equiv="content-language" content="pt-br">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="keywords" content="SALVE, ICMBIo, Avaliação, Espécie, Sistema - SALVE" />
	<meta name="reply-to" content="${raw(grailsApplication.config.app.email)}">
    <meta name="author" content="Instituto Chico Mendes de Conservação da Biodiversidade">
    <meta name="rights" content="Instituto Chico Mendes de Conservação da Biodiversidade" />
	<meta name="robots" content="index,nofollow">
    <meta name="googlebot" content="index,nofollow">
    <meta name="description" content="Sistema de Avaliação do Risco de Extinção da Biodiversidade - SALVE" />
    <meta name="generator" content="Grails 2.5.6 - Framework" />
    <meta name="language" content="pt-BR">
	<title><g:layoutTitle default="${appName}"/>${env=='desenv' ? ' (DSV)':''}</title>
	<link rel="shortcut icon" href="${assetPath(src: 'favicon.ico')}" type="image/x-icon">
    <link rel="icon" href="${assetPath(src: 'favicon.ico')}" type="image/png">
	<link rel="apple-touch-icon" href="${assetPath(src: 'apple-touch-icon.png')}">
	<link rel="apple-touch-icon" sizes="114x114" href="${assetPath(src: 'apple-touch-icon-retina.png')}">
	<link rel="stylesheet" href="/${appName}/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<asset:stylesheet src="application.css"/>
	<g:javascript>
		;window.grails = {
			appName			: "${appName.toLowerCase()}",
			env 			: "${env}",
			userCpf 		: "${session?.sicae?.user ? session.sicae.user.nuCpf : '' }",
			perfilConsulta 	: ${session?.sicae?.user ? session.sicae.user.isCO() : true },
			perfilExterno 	: ${ session?.sicae?.user ? session.sicae.user.isEX() : true },
			coPerfil        : "${ session?.sicae?.user ? session.sicae.user.codigoPerfil : '' }",
			baseUrl:"${raw('/'+appName.toLowerCase()+'/')}",
			assetsRoot : "${ raw(asset.assetPath(src: ''))}",
			spinnerImage :"${raw(assetPath( src: 'spinner.gif'))}",
			spinner:"${raw("<i class=\\\"glyphicon glyphicon-refresh spinning\\\"></i>")}",
			tooltipImage :"${raw(assetPath( src: 'tooltip.gif'))}",
			markerFlagBlue:"${raw(assetPath( src: 'marker_flag_blue.png'))}",
			markerPinBlue:"${raw(assetPath( src: 'marker_pin_blue.png'))}",
			markerPinRed:"${raw(assetPath( src: 'marker_pin_red.png'))}",
			markerRed:"${raw(assetPath( src: 'marker_red.png'))}",
			markerGreen:"${raw(assetPath( src: 'marker_green.png'))}",
			markerBlue:"${raw(assetPath( src: 'marker_blue.png'))}",
			markerDotRed:"${raw(assetPath( src: 'marker_dot_red.png'))}",
			markerDotOrange:"${raw(assetPath( src: 'marker_dot_orange.png'))}",
			sisicmbio:"${ session?.sicae?.user?.cookie ? session.sicae.user.cookie : '' }",
			urlGoogleTranslate:"${grailsApplication.config.traducao.urlGoogle}",
			urlLibreTranslate:"${grailsApplication.config.traducao.urlLibreTranslate}",

		};

		%{-- encontrar a url base --}%
        var urlProtocol = window.location.protocol;
		var aTemp   = String( urlProtocol +'//'+ window.location.host + window.location.pathname+'/').split( '${appName}/');
		var baseUrl =  aTemp[0]+'${appName}/';
	    var webUserId="${session?.sicae?.user?.sqPessoa}";

	</g:javascript>
	</head>
	<body>

		%{-- SEO --}%
		<h1 style="display:none">${raw(grailsApplication.config.app.title + ' - ' + appName.toUpperCase())}</h1>

		%{-- cabeçalho --}%
		<div class="navbar navbar-default navbar-fixed-top app-navbar" id="appDivHeader">
			<div class="container-fluid">
			 	<div class="navbar-header">
                    <div class="app-title" style="font-size: 120% !important">

%{--						NOME DO SISTEMA + SIGLA + VERSAO--}%
						<span style="font-size:100% !important;">${raw( Util.str2html(grailsApplication.config.app.title) )} - ${raw( appName.toUpperCase())}</span>
						<span style="font-size:50% !important;" data-tag="${tag}" title="${tag + ' / '+grailsApplication.config?.dataSource?.name}">v.${grailsApplication.metadata.getApplicationVersion()}</span>

%{--						NOME DA INSTITUICAO --}%
						<g:if test="${ noInstituicao || grailsApplication.config.app.instituicao.nome}">
							<br>
							<span style="font-size:95% !important;">${noInstituicao ?: grailsApplication.config.app.instituicao.nome}</span>
						</g:if>

						<div class="visible-md visible-sm visible-xs">
							<div style="display:inline-block;">

                                %{-- MANUAL DO SISTEMA --}%
								<a target="_self" href="javascript:void(0)" title="Baixar manual do sistema (PDF)."
                                   onClick="downloadWithProgress({fileName:'manual_salve',originalFileName:'SALVE-manual-usuario.pdf'})">
									<span class="fa fa-book" style="font-size:1.2rem !important;color:green;"></span>
								</a>

                                %{--   TUTORIAL CONSTRUCAO DE MAPA--}%
                                <a target="_blank" href="/${appName}/main/download?fileName=tutorial_construcao_mapas.zip" title="Baixar tutorial criação de mapas (ZIP).">
                                    <span class="fa fa-book" style="font-size:1.2rem !important;color:green;"></span>
                                </a>

                                %{-- POLITICA DE DADOS --}%
%{--
                                <a target="_blank" href="/${appName}/main/download?fileName=politica_dados_avaliacao" title="Baixar política de dados da avaliação (PDF).">
									<span class="fa fa-book" style="font-size:1.2rem !important;color:green;"></span>
								</a>
--}%

                                %{-- IN AVALIAÇÃO --}%
                                <a target="_blank" href="/${appName}/main/download?fileName=in_avaliacao_risco_extincao_especies.pdf" title="Baixar a instrução normativa (PDF) sobre as diretrizes e procedimentos para a Avaliação do Risco de Extinção das Espécies da Fauna Brasileira">
									<span class="fa fa-book" style="font-size:1.2rem !important;color:green;"></span>
								</a>

                                %{-- Orientações para finalização das fichas --}%
                                <a target="_blank" href="/${appName}/main/download?fileName=diretrizes_revisao_fichas_publicacao.pdf" title="Baixar documento PDF das diretrizes para revisão das fichas para publicação">
                                    <span class="fa fa-book" style="font-size:1.2rem !important;color:green;"></span>
                                </a>

                                %{-- INFORMATIVOS--}%
                                <a href="javascript:modal.visualizarInformativosPublicados()" title="Ver os informativos">
									<span class="fa fa-comment" style="font-size:1.2rem !important;color:green;"></span>
								</a>
						 	</div>

							<g:if test="${session?.sicae?.user}">
								<div style="display:inline-block;">
									<a href="javascript:void(0)" onClick = "app.logout()">
						           		<span title="Encerrar a sessão" class="glyphicon glyphicon-user" style="font-size:1rem !important;"></span>
										<span style="font-size:1rem !important;color:green;" title="${ session?.sicae?.user?.noPessoa+'/'+session?.sicae?.user?.noPerfil+ (session?.sicae?.user?.sgUnidadeOrg ? '/' + session?.sicae?.user?.sgUnidadeOrg :'') }">
											.${br.gov.icmbio.Util.nomeAbreviado( session?.sicae?.user?.noUsuario ) } &nbsp; ${br.gov.icmbio.Util.nomeAbreviado( session?.sicae?.user?.noPerfil) }
										</span>
						    		</a>
								</div>
							</g:if>

							<g:if test="${session?.sicae?.user}">
								<div style="display:inline-block;">
									<a href="https://sicae.sisicmbio.icmbio.gov.br/index/home">
										<span title="Ir para o sistema SICA-e" style="font-size:1rem !important;color:green;">SICA-e</span>
									</a>
								</div>
							</g:if>
					    </div>
					</div>
				</div>
				<ul class="nav navbar-nav navbar-right visible-lg">

						  <li class="dropdown">
							<a href="#" class="dropdwon-toggle" style="height:68px;" data-toggle="dropdown" role="button">
								<span class="mt10 fa fa-question" style="font-size:2.3rem !important;color:green;"></span>
							</a>
	                        <ul class="dropdown-menu dropdown-menu-right" style="z-index:99 !important;">

%{--                                MANUAL DO SALVE--}%
								<li title="Baixar manual do usuário (PDF).">
 		                            <a target="_self" href="javascript:void(0)" onClick="downloadWithProgress({fileName:'manual_salve',originalFileName:'SALVE-manual-usuario.pdf'})">
                                      <span class="mr10 fa fa-book" aria-hidden="true"></span>Manual do usuário
                                    </a>
							    </li>

                                %{--  TUTORIAL CONSTRUCAO DE MAPA--}%
                                <li title="Baixar tutorial de contrução de mapas (ZIP).">
                                    <a target="_self" href="javascript:void(0)" onClick="downloadWithProgress({fileName:'tutorial_construcao_mapas.zip',originalFileName:'SALVE-tutorial-construcao-mapas.zip'})">
                                        <span class="mr10 fa fa-book" aria-hidden="true"></span>Tutorial construção de mapas
                                    </a>
                                </li>

%{--                                IN AVALIACAO ESPECIES--}%
                                <li title="Baixar a instrução normativa (PDF) sobre as diretrizes e procedimentos para a Avaliação do Risco de Extinção das Espécies da Fauna Brasileira">
                                    <a target="_self" href="javascript:void(0)" onClick="downloadWithProgress({fileName:'in_avaliacao_risco_extincao_especies.pdf',originalFileName:'SALVE-IN-Avaliacao-Risco-Extincao-Especies.pdf'})">
                                        <span class="mr10 fa fa-book" aria-hidden="true"></span>IN sobre avaliação de risco de extinção das espécies
                                    </a>
                                </li>

%{--                                DIRETRIZES PUBLICAO--}%
                                <li title="Baixar documento PDF das diretrizes para revisão das fichas para publicação">
                                     <a target="_self" href="javascript:void(0)" onClick="downloadWithProgress({fileName:'diretrizes_revisao_fichas_publicacao.pdf',originalFileName:'SALVE-Diretrizes-Revisao-Fichas-Publicacao.pdf'})">
                                        <span class="mr10 fa fa-book" aria-hidden="true"></span>Diretrizes para revisão das fichas para publicação
                                     </a>
                                </li>



                                %{--                                <li title="Baixar política de dados (PDF).">
                                                                   <a target="_blank" href="/${appName}/main/download?fileName=politica_dados_avaliacao"><span class="mr10 fa fa-book" aria-hidden="true"></span>Política de dados da avaliação</a>
                                                                </li>--}%

								<li class="" title="Enviar email para equipe SALVE"><a href="mailto:salve@icmbio.gov.br?subject=Dúvidas/sugestões referêntes ao SALVE."><i class="green mr10 fa fa-envelope" aria-hidden="true"></i>Contato</a></li>

                               <li class="" title="Informativos publicados sobre o SALVE"><a href="javascript:modal.visualizarInformativosPublicados()"><i class="blue mr10 fa fa-comments" aria-hidden="true"></i>Informativos</a></li>

                                %{-- <li class="disabled" title="Perguntas frequentes"><a href="#"><i class="mr10 fa fa-question-circle-o" aria-hidden="true"></i>FAQs</a></li> --}%


							</ul>
					      </li>

					<g:if test="${session?.sicae}">
						<li>
							<a href="javascript:void(0)" style="height:68px; !important;"
						       onClick = "app.logout()">
						       <center>
						       		<span title="Encerrar a sessão" class="glyphicon glyphicon-user" style="padding-top:3px;padding-right:3px;"><span>&nbsp;Sair<span></span>
									<hr style="margin:0;padding:0;">
                                        <g:if test="${ session?.sicae?.user}">
                                            <span id="spanLoginUserName" title="${ session?.sicae?.user?.noPessoa+'/'+session?.sicae?.user?.noPerfil+ ( session?.sicae?.user?.sgUnidadeOrg ? '/' + session?.sicae?.user?.sgUnidadeOrg : '' ) }">
                                             ${ br.gov.icmbio.Util.nomeAbreviado( session?.sicae?.user?.noUsuario ) }
                                                <br>${ br.gov.icmbio.Util.nomeAbreviado( session?.sicae?.user?.noPerfil ) }
                                            </span>
                                        </g:if>
						       </center>
						    </a>
						</li>
					</g:if>

%{--					LOGO DA INSTITUICAO--}%
					<g:if test="${siglaEstadoInstituicao}">
						<li style="margin-top:-3px;margin-right:-10px;">
%{--							SITE DA INSTITUICAO--}%
							<a style="height:75px; !important;"
							   href="${linkInstituicao ?: '#'}"
							   target="_blank"
							   title="Ir para o site - ${sgInstituicao}">
							<img style="width:auto;max-height: 63px;" src="${'download?fileName=/data/salve-estadual/arquivos/bandeiras-estados/'+siglaEstadoInstituicao.toLowerCase()+'.png'}" alt=""/>
						</a></li>
					</g:if>
				</ul>
			</div>
		</div>

		%{-- menu principal --}%
		<div id="appDivMainMenu"></div>

		%{-- mensagem inicializando --}%
		<div id="divAppLoading" class="text-center">
            <img src="${assetPath(src:'logomarca_salve_estadual_grande.png')}" >
            <h3>
                <br>
                <i class="glyphicon glyphicon-refresh spinning"></i>&nbsp;Inicializando. Aguarde...
            </h3>
		</div>

		%{-- Conteudo das telas --}%
		<div id="appDivContainer" class="container-fluid horizontal-center">
%{--			<g:if test="${flash.error}">
				<div class="container alert alert-danger" style="display:block;">${raw(flash.error)}</div>
        	</g:if>
			<g:if test="${flash.message}">
				<div class="container alert alert-success" style="display:block;">${raw(flash.message)}</div>
			</g:if>--}%
			<g:layoutBody/>
		</div>

    	<g:javascript src="tinymce/tinymce.min.js" async="true"/>
		<asset:javascript src="application.js" defer="true"/>
		<asset:javascript src="fichaCompleta.js" defer="true"/>
        <asset:javascript src="openlayers/brasil-geojson.js" async="true"/>
		<asset:javascript src="openlayers/ol3map.js" async="true"/>
		<asset:javascript src="openlayers/cluster.js" async="true"/>

		<g:if test="${session?.sicae?.user?.isVL()}">
             <asset:javascript src="validador.js" defer="true" />
        </g:if>
%{--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/orgchart/2.1.3/css/jquery.orgchart.min.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/orgchart/2.1.3/js/jquery.orgchart.min.js" type="text/javascript"></script>--}%

    <div class="modal modal-alerta-informativos" tabindex="-1" role="dialog" id="modalAlertaInformativos" data-sq-informativo="">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    %{--<span class="modal-title"><i class="blue fa fa-comment mr10"></i>Mensagem nº <span id="numeroInformativo"></span></span>--}%
                    <span class="modal-title"><i class="blue fa fa-comment mr10"></i>Mensagem</span></span>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">

                    <a href=";" title="Baixar o Pdf" id="linkAlertaInformativoLerMais" class="btn btn-primary pull-left" style="display:none;"><i class="fa fa-file-pdf-o">&nbsp;Ler mais!</i></a>

                    <button type="button" class="btn btn-primary fld100" onClick="app.closeModalAlertaInformativo()">
                        <i class="mr5 fa fa-check"></i>Ok!
                    </button>
                </div>
            </div>
        </div>
    </div>
    </body>
</html>
