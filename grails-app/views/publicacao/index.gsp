<div id="publicacaoContainer">
   <div class="row">
      <div class="col-sm-12">
         <div class="panel panel-default">
            <div class="panel-heading">
                    <span>Módulo Versionamento de Fichas</span>
               <a id="btnClose" data-action="app.unloadModule" class="btn btn-default pull-right btn-xs" title="Fechar">
               <span class="glyphicon glyphicon-remove btnClose"></span>
               </a>
            </div>
            <div class="panel-body">

                <div id="divSelectCicloAvaliacao" class="form-group">
                    <input type="hidden" id="sqCicloAvaliacao" name="sqCicloAvaliacao" value="${listCiclosAvaliacao[0]?.id}"/>
                </div>

                <!-- inicio  page content -->
                <div class="container-fluid" id="pageContent" style="display: none">

                    <!-- abas -->
                   <ul class="nav nav-tabs" id="ulTabs">
                            <li id="liTabNaoPublicadas" class="active"><a data-action="publicacao.tabClick" data-container="tabNaoPublicadas" data-toggle="tab" href="#tabNaoPublicadas">Fichas Finalizadas</a></li>
                            <li id="liTabPublicadas"><a data-action="publicacao.tabClick" data-container="tabPublicadas" data-toggle="tab" href="#tabPublicadas">Fichas versionadas</a></li>
                   </ul>

                    %{-- Container de todas abas --}%
                    <div id="tabsContent" class="tab-content">
                            %{-- aba fichas finalizadas --}%
                            <div role="tabpanel" class="tab-pane active" id="tabNaoPublicadas">
                                %{--Filtro das fichas NAO publicadas--}%
                                <div class="mt10 form-inline" id="divFiltrosFichaNaoPublicadas"></div>

                                %{-- gride para selecionar ficha --}%
                                %{--                            <div id="divGridFichasNaoPublicadas" class="mt10" style="max-height:780px;overflow-x:hidden;overflow-y:auto;"></div>--}%

                                %{-- gride fichas NÃO publicadas --}%
                                <div class="row">
                                    <div id="containerGridFichasNaoPublicadas"
                                         class="cols-sm-12"
                                         data-params="sqCicloAvaliacao"
                                         data-container-filters-id="divFiltrosFichaNaoPublicadas"
                                         data-height="800px"
                                         data-field-id="sqFicha"
                                         data-url="publicacao/getGridFichasNaoPublicadas"
                                         data-show-footer-pagination="false"
                                         data-on-load="publicacao.gridNaoPublicadasOnLoad">
                                        %{-- FAZER O CONTROLE DAS LINHAS SELECIONADAS--}%
                                        <input type="hidden" id="selectedIdsGridFichasNaoPublicadas" value="">
                                        %{-- RENDERIZAR O GRIDE VAZIO--}%
                                        <g:render template="divGridFichasNaoPublicadas"
                                                  model="[gridId: 'GridFichasNaoPublicadas', listFichas: [], pagination:[:]]">

                                        </g:render>
                                    </div>
                                </div>
                                %{-- BOTOES ABAIXO DO GRIDE--}%
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="panel-footer row">
                                            <div class="col-sm-12 text-left">
                                                <button data-action="publicacao.publicar" style="min-width:180px;" class="fld btn btn-success mt5 ml5">Versionar</button>
                                                <button data-action="publicacao.adicionarPendenciaRevisao" class="fld btn btn-warning ml5 mt5" title="Adicionar pendência de revisão na(s) ficha(s) selecionada(s)">Adicionar pendência revisão</button>
                                                <button data-action="publicacao.revisarPendenciaRevisao" class="fld btn btn-info mt5 ml5" title="Marcar todas as pendências como aceitas e revisadas">Marcar como revisada</button>
                                                <button data-action="publicacao.removerPendenciaRevisao" style="min-width:180px;" class="fld btn btn-danger mt5" title="Excluir as pendências de revisão">Excluir pendência</button>
                                                <span id="selectedRowsGridFichasNaoPublicadas" class="bold mt15 fs15" style="display: none;" >0 selecionadas</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        %{-- aba fichas versionadas --}%
                        <div role="tabpanel" class="tab-pane" id="tabPublicadas">
                            <form id="frmPublicacao" class="form-inline" style="display:none;" role="form">

                                %{--Filtro das fichas publicadas--}%
                                <div class="mt10" id="divFiltrosFichaPublicadas"></div>

                                %{-- gride fichas publicadas --}%
                                <div class="row">
                                    <div id="containerGridFichasPublicadas"
                                         class="cols-sm-12"
                                         data-params="sqCicloAvaliacao"
                                         data-container-filters-id="divFiltrosFichaPublicadas"
                                         data-height="800px"
                                         data-field-id="sqFicha"
                                         data-url="publicacao/getGridFichasPublicadas"
                                         data-show-footer-pagination="false"
                                         data-on-load="publicacao.gridPublicadasOnLoad">
                                        %{-- FAZER O CONTROLE DAS LINHAS SELECIONADAS--}%
                                        <input type="hidden" id="selectedIdsGridFichasPublicadas" value="">
                                        %{-- RENDERIZAR O GRIDE VAZIO--}%
                                        <g:render template="divGridFichasPublicadas"
                                                  model="[gridId: 'GridFichasPublicadas', listFichas: [], pagination:[:]]">

                                        </g:render>
                                    </div>
                                </div>
                                %{-- BOTOES ABAIXO DO GRIDE--}%
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="panel-footer row">
                                            <div class="col-sm-12 text-left">
%{--                                                NÃO TEM COMO CANCELAR O VERSIONAMENTO DA FICHA--}%
%{--                                                <button data-action="publicacao.cancelar" class="fld btn btn-danger mt5">Cancelar publicação</button>--}%
                                                <button data-action="publicacao.transmitirIucn"
                                                        title="Clique aqui para gerar o arquivo ZIP para envio das fichas selecionadas para o SIS Connect/IUCN"
                                                        class="fld btn btn-primary mt5">Gerar zip p/ SIS Connect</button>
                                                <g:if test="${ session.sicae.user.isADM() }">
                                                    <button data-action="publicacao.verificaDoiCrossref()" class="fld btn btn-info mt5">Consultar/Atualizar DOI em lote</button>
                                                </g:if>
                                                <span id="selectedRowsGridFichasPublicadas" class="bold mt15 fs15" style="display: none;" >0 selecionadas</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>

                    <!-- fim abas -->
                </div>
                <!-- fim page content -->
            </div>
            %{-- fim panel body --}%
        </div>
        %{-- fim panel --}%
    </div>
    %{-- fim row 12 --}%
</div>
%{-- fim container --}%
