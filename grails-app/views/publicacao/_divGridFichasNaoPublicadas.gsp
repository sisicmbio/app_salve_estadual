<!-- view: /views/publicacao/_divGridFichasNaoPublicadas -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<g:if test="${params.error}">
    <div style="display:none" id="divMsgError${gridId}">${params.error}</div>
</g:if>
<table id="table${gridId}" class="table table-sm table-hover table-striped table-sorting table-condensed table-bordered">
            <thead>
    %{-- PAGINACAO --}%
    <tr id="tr${gridId}Pagination">
        <td colspan="7" >
            <div class="row">
                <div class="col-sm-12 text-left"  style="min-height:0;height: auto;overflow: hidden;" id="div${(gridId ?: pagination?.gridId)}PaginationContent">
                    <g:if test="${pagination?.totalRecords}">
                        <g:gridPagination  pagination="${pagination}"/>
                    </g:if>
                </div>
            </div>
        </td>
    </tr>

    <tr id="tr${gridId}Columns" data-grid-id="${gridId}">
            <th style="width:40px;">#</th>
        <th style="width:30px;"><i id="checkUncheckAll" title="Marcar/Desmarcar Todos" onclick="publicacao.checkUncheckAll(this,'${gridId}')" class="cursor-pointer glyphicon glyphicon-unchecked check-all"></i></th>
            <th style="width:auto;">Nome Científico</th>
            <th style="width:100px;">Pendência<br>Revisão</th>
            <th style="width:200px;">Categoria</th>
%{--        <th style="width:150px;">Registros em carência</th>--}%
            <th style="width:300px;">Unidade responsável</th>
            <th style="width: 50px;" >Ação</th>
    </tr>
            </thead>
            <tbody>
            <g:each var="item" in="${listFicha}" status="i">
        <tr id="tr-${item.sq_ficha}">

            %{-- COLUNA NUMERAÇÃO --}%
            <td class="text-center"
                id="td-${item.sq_ficha}">
                ${ i + (pagination ? pagination?.rowNum.toInteger() : 1 ) }
            </td>

            %{-- COLUNA EXIBR/NAO EXIBIR MODULO PUBLICO --}%
                    <td class="text-center">
                        <input id="sqFicha_${item.sq_ficha}" value="${item.sq_ficha}"
                       data-possui-pendencia="${item.qtd_pendencia_revisao > 0}"
                       ${item.qtd_pendencia_revisao >0 ? raw('title=\"Ficha com pendência\"'):''}
                       data-grid-id="${gridId}" onchange="publicacao.selectRowGridNaoPublicadas(this)"
                    ${raw(item.st_exibir_modulo_publico ? '' : 'title="Ficha não está selecionada para ser exibida no SALVE público"')}
                               type="checkbox"
                               class="checkbox-lg"/>
                    </td>


            %{-- NOME CIENTIFICO --}%
                    <td>
                        ${ raw('<a href="javascript:void(0);" title="Visualizar ficha" onClick="showFichaCompleta('+
                            item.sq_ficha +
                            ',\''+ br.gov.icmbio.Util.ncItalico(item.no_cientifico_com_autor,false,true)+'\',true,\'publicacao\',\'\',false,\'\',\'\')">'+
                            br.gov.icmbio.Util.ncItalico( item.nm_cientifico,true,true, item.co_nivel_taxonomico )
                            +'</a>')}
                    </td>

                    %{--  PENDENCIAS --}%
                    <td class="text-center">
                        ${ raw( ( item.qtd_pendencia_revisao > 0 ) ? '<a href="javascript:modal.showPendenciaRevisaoFicha(' +
                            item.sq_ficha.toString() + ');"><span class="cursor-pointer ' + item.de_cor_pendencia + ' badge">' +
                            item.qtd_pendencia_revisao +
                            '</span></a>' : '<span class="bgGreen badge">0</span>')}

                    </td>

                    %{-- Categoria final --}%
                    <td>
                        <span class="${ item.de_criterio ? 'cursor-pointer' : ''}" title = "${item.de_criterio ? 'Critério: ' + item.de_criterio : ''}">${item?.de_categoria}</span>
                    </td>

                    %{-- Qtd Registros em carência --}%
%{--            <td class="text-center ${item?.qtd_carencia ? 'bg-danger':''}">${ (item?.qtd_carencia ?:'') }</td>--}%

                    %{-- Unidade responsável --}%
                    <td>${ item.sg_unidade_org }</td>

                    %{-- Botões --}%
                    <td class="td-actions">
                        <button type="button" data-action="print" data-no-cientifico="${item?.nm_cientifico}" data-sq-ficha="${item?.sq_ficha}" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="Imprimir Ficha" style="color:gray;">
                            <i class="fa fa-print btnPrint" aria-hidden="true"></i>
                        </button>

                        <button type="button" ${item.nu_fotos > 0 ? '' : 'disabled' } style="color:${item.nu_fotos > 0 ? 'blue' : 'gray'};"
                                data-action="publicacao.selectImages"
                                data-no-cientifico="${item?.nm_cientifico}"
                                data-sq-ficha="${item?.sq_ficha}"
                                class="btn btn-default btn-xs"
                                data-toggle="tooltip"
                                data-placement="top" title="${item.nu_fotos > 0 ? "Marcar/Desmarcar foto em destaque.": "Não possui foto para destaque"}">
                            <i class="fa fa-image" aria-hidden="true"></i>
                        </button>
                    </td>

                </tr>
            </g:each>
    <g:if test="${params.mensagemErro}">
        <script>
            app.alertInfo('${raw(params.mensagemErro)}')
        </script>
    </g:if>
    <g:if test="${params.mensagemSuccess}">
        <script>
            app.alertInfo('${raw(params.mensagemSuccess)}')
        </script>
    </g:if>
            </tbody>
        </table>
