<!-- view: /views/publicacao/_divGridFichasPublicadas -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<table id="table${gridId}" class="table table-sm table-hover table-striped table-sorting table-condensed table-bordered">
    <thead>
    %{-- PAGINACAO --}%
    <tr id="tr${gridId}Pagination">
        <td colspan="9" >
            <div class="row">
                <div class="col-sm-12 text-left"  style="min-height:0;height: auto;overflow: hidden;" id="div${(gridId ?: pagination?.gridId)}PaginationContent">
                    <g:if test="${pagination?.totalRecords}">
                        <g:gridPagination  pagination="${pagination}"/>
</g:if>
                </div>
            </div>
        </td>
    </tr>
    %{-- TITULOS --}%
    <tr id="tr${gridId}Columns" data-grid-id="${gridId}">
            <th style="width:40px;">#</th>
        <th style="width:30px;"><i id="checkUncheckAll" title="Marcar/Desmarcar Todos" onClick="publicacao.checkUncheckAll(this,'${gridId}')" class="cursor-pointer glyphicon glyphicon-unchecked check-all"></i></th>
        <th style="width:auto;min-width: 200px">Nome Científico</th>
        <th style="width:200px;">Categoria</th>
        <th style="width:200px;">Unidade<br>responsável</th>
%{--        <th style="width:100px;">Registros em<br>carência</th>--}%
        <th style="width:240px">DOI</th>
        <th style="width:100px">Versionada em</th>
        <th style="width:100px">SIS Connect<BR>IUCN</th>
            <th style="width: 50px;">Ação</th>
    </tr>
            </thead>
            <tbody>
                <g:each var="item" in="${listFicha}" status="i">
        <tr id="tr-${item.sq_ficha}">

    %{-- COLUNA NUMERAÇÃO --}%
        <td class="text-center"
            id="td-${item.sq_ficha}">
            ${ i + (pagination ? pagination?.rowNum.toInteger() : 1 ) }
        </td>

                        %{-- Checkbox --}%
                        <td class="text-center">
            <input id="sqFicha_${item.sq_ficha}" data-grid-id="${gridId}" onchange="publicacao.selectRowGridPublicadas(this)" value="${item.sq_ficha}" type="checkbox" class="checkbox-lg"/>
                        </td>

                        %{-- Nome cientifico --}%
                        <td>
                            ${raw( br.gov.icmbio.Util.ncItalico( item.nm_cientifico,true,true,item.co_nivel_taxonomico )) }
                            <g:if test="${item.versoes}">
                <div style="height:70px;overflow-y: auto">
                    <g:each var="versao" in="${item.versoes}" status="index">
                        <div style="display: block;">
                                    <input name="radioExibirVersaoModuloPublico${i+1}" value="${versao?.sq_ficha_versao}"
                                   title="Exibir no SALVE público"
                                           type="radio"
                                   ${versao.cd_contexto == 'VERSAO_REJEICAO_CONABIO' ? 'disabled':''}
                                           data-checked="${versao.st_publico ? true :false }"
                                   style="margin-left: 15px;"
                                           onclick="publicacao.publicarVersao(${(i+1)+','+versao.sq_ficha_versao+',"'+versao.nu_versao+'"'})"
                                           ${versao.st_publico==true ? 'checked' : ''}
                                           />
                                    <span>
                                        &nbsp;${ raw('<a id="aVersao'+versao.nu_versao+'" href="javascript:void(0);" title="Clique para visualizar a ficha versionada." onClick="showFichaVersao({sqFichaVersao:' +
                                    versao.sq_ficha_versao+'})"><span>V.'+versao.nu_versao+' <small class="gray">'+versao.dt_versao+'</small></span></a>'+
                                    (versao.cd_contexto=='VERSAO_REJEICAO_CONABIO' ? '<i class="fa fa-info-circle red ml5" title="'+versao.ds_contexto+'"></i>':'')
                                ) }
                                    </span>
                        </div>
                                </g:each>
                </div>
                            </g:if>
                            <g:else>
                                ${ raw('<a href="javascript:void(0);" title="Visualizar ficha" onClick="showFichaCompleta('+
                                    item.sq_ficha +
                                    ',\''+br.gov.icmbio.Util.ncItalico(item.no_cientifico_com_autor,false,true)+'\',true,\'publicacao\',\'\',false,\'\',\'\')">'+
                                    br.gov.icmbio.Util.ncItalico( item.nm_cientifico,true,true,item.co_nivel_taxonomico )
                                    +'</a>')}
                            </g:else>
                        </td>

                        %{-- Categoria final --}%
                        <td><span class="${ item.de_criterio ? 'cursor-pointer text-nowrap' : ''}" title = "${item.de_criterio ? 'Critério:' + item.de_criterio : ''}">${item.de_categoria}</span></td>

                        %{-- Unidade responsável --}%
                        <td>${ item.sg_unidade_org }</td>

                        %{-- Qtd Registros em carência --}%
%{--        <td class="text-center ${item?.qtd_carencia ? 'bg-danger':''}">${ (item?.qtd_carencia ?:'') }</td>--}%

    %{-- DOI (versão anterior)--}%
        <!--
                        <td class="text-center">
                        <span id="span-edit-doi-${item.sq_ficha}" class="text-nowrap" style="white-space: nowrap">
                            <span id="span-edit-doi-text-${item?.sq_ficha}" data-old-value="${item?.ds_doi}" class="content span-nu-doi">${ item?.ds_doi ?:''}</span>
                            <i onClick="publicacao.editDoi(${item.sq_ficha})"
                               class="fa fa-edit ml10 cursor-pointer"
                               title="Informar/Alterar nº DOI">
                            </i>
                        </span>
                        -->

                        %{-- DOI --}%
                        <td class="text-center">
                        <span id="span-edit-doi-${item.sq_ficha}" class="text-nowrap" style="white-space: nowrap">

                            <g:if test="${item.ds_doi}">
                                <span>
                                    <a title="Acessar DOI: ${item?.ds_doi}" target="_blank"
                                       href="https://www.doi.org/${item?.ds_doi}">${item?.ds_doi}</a>
                                </span>
                            </g:if>


                            <g:if test="${ item?.ds_doi == null }">
                                <g:if test="${ session.sicae.user.isADM() }">
                                    <i onClick="publicacao.verificaDoiCrossref(${item.sq_ficha})"
                                    class="fa fa-refresh ml10 cursor-pointer" style="color:blue"
                                    title="Consultar DOI no crossref.org">
                                    </i>
                                    <i onClick="publicacao.gerarAtualizarDoi(${item.sq_ficha})"
                                    class="fa fa-dollar ml10 cursor-pointer" style="color:green"
                                    title="Gerar/Atualizar DOI no crossref.org (possui custo)">
                                    </i>
                                </g:if>
                                %{-- Informar/Alterar nº DOI (DESCONTINUADO) --}%
                                <!--
                                <i onClick="publicacao.editDoi(${item.sq_ficha})"
                                class="fa fa-edit ml10 cursor-pointer"
                                title="Informar/Alterar nº DOI">
                                </i>
                                -->

                            </g:if>
                            <g:else>
                                <g:if test="${ session.sicae.user.isADM() }">
                                    <i onClick="publicacao.gerarAtualizarDoi(${item.sq_ficha})"
                                    class="fa fa-dollar ml10 cursor-pointer" style="color:green"
                                    title="Gerar/Atualizar DOI no crossref.org (possui custo)">
                                    </i>
                                </g:if>
                            </g:else>

                        </span>

                    %{-- Publicada em --}%

        <td class="text-center">
            <g:if test="${item.dt_publicacao}">
                ${raw( item?.dt_publicacao?.format('dd/MM/yyyy')+'<br>' + br.gov.icmbio.Util.nomeAbreviado(item?.no_pessoa)) }
            </g:if>
        </td>

                    %{-- Publicada por --}%
%{--                        <td>${br.gov.icmbio.Util.nomeAbreviado(item?.no_pessoa)}</td>--}%

                    %{-- Enviada IUCN --}%
                    <g:if test="${item?.sq_iucn_transmissao}">
                    <td class="text-center">${item?.dt_ultima_transmissao.format('dd/MM/yyyy HH:mm:ss')}
                        <g:if test="${item.st_erro}">
                            <a href="javascript:void(0);" title="clique para visualizar os problemas encontrados"
                                    data-action="publicacao.verLogTransmissao(${item?.sq_iucn_transmissao+','+item?.sq_ficha})">
                        <img class="cursor-pointer" src="/salve/assets/check48-red.png" style="width:16px;height: 16px">
                                </a>
                        </g:if>
                        <g:else>
                    <img src="/salve/assets/check48.png" style="width:16px;height: 16px">
                        </g:else>

                    </td>
                    </g:if>
                    <g:else>
                        <td>&nbsp;</td>
                    </g:else>


                    </td>

                        %{-- Ações --}%
                        <td class="td-actions">
                            <button type="button" data-action="print" data-no-cientifico="${item?.nm_cientifico}" data-sq-ficha="${item?.sq_ficha}" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="Imprimir Ficha" style="color:gray;">
                                <i class="fa fa-print btnPrint" aria-hidden="true"></i>
                            </button>
                            <button type="button" ${item.nu_fotos > 0 ? '' : 'disabled' }
                                    style="color:${item.nu_fotos > 0 ? 'blue' : 'gray'};" data-action="publicacao.selectImages"
                                    data-no-cientifico="${item?.nm_cientifico}"
                                    data-sq-ficha="${item?.sq_ficha}" class="btn btn-default btn-xs"
                                    data-toggle="tooltip" data-placement="top"
                                    title="${item.nu_fotos > 0 ? "Marcar/Desmarcar foto em destaque.": "Não possui foto para destaque"}">
                                <i class="fa fa-image" aria-hidden="true"></i>
                            </button>

                    </td>
                    </tr>
                </g:each>
    <g:if test="${params.mensagemErro}">
        <script>
            app.alertInfo('${raw(params.mensagemErro)}')
        </script>
            </g:if>
    <g:if test="${params.mensagemSuccess}">
        <script>
            app.alertInfo('${raw(params.mensagemSuccess)}')
        </script>
</g:if>
    </tbody>
</table>
