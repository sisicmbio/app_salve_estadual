<div id="selectImagesContainer" class="text-center">
    <div class="col-12 text-center">
        <h2 style="margin: 0;padding: 0;margin:10px 0;">${params.noCientifico}</h2>
    </div>

    <div class="col">
        <g:each var="item" in="${listImages}">
            <div id="divPanel-${item.id}" class="panel panel-warning"
                 style="margin-right:10px;display:inline-block;border:1px solid gray; background-color: #fff;max-width:250px;">
                <div class="panel-body" style="height: auto;width:250px;">
                    <img onClick="publicacao.showImage(${item.id},'${item.noArquivoDisco}')"
                        class="cursor-pointer img-responsive"
                        src="/salve/fichaMultimidia/getMultimidia/${item.id}">
                </div>
                <div class="panel-footer text-left" id="divFooter-${item.id}" style="background-color:${item.inDestaque?'#b2d5b2':''} ">
                    <p style="padding: 0;margin:0;">Autor:${item.noAutor}</p>
                    <g:if test="${item.deLegenda}">
                        <p style="padding: 0;margin:0;" title="${item.deLegenda}">Legenda: ${item?.deLegenda.toString()+'...'}</p>
                    </g:if>
                    <g:if test="${item.txMultimidia}">
                        <p style="padding: 0;margin:0;" title="${item.txMultimidia}">${item?.txMultimidia?.toString()?.substring(0,Math.min(20, item?.txMultimidia?.toString()?.size() ) )+'...'}</p>
                    </g:if>
                    <g:if test="${item.dtElaboracao}">
                        <p style="padding: 0;margin:0;">Data:${item?.dtElaboracao?.format('dd/MM/yyyy')}</p>
                    </g:if>
                    <hr style="padding: 0;margin:0;">
                    <label class="cursor-pointer" style="font-size:1.1rem !important;font-weight: bold !important;">
                        <input type="checkbox" class="checkbox-lg" ${item.inDestaque?'checked':''}
                            onClick="publicacao.updateImageDestaque(this,${item.id})">em destaque
                    </label>
                </div>
            </div>
        </g:each>
    </div>
</div>
