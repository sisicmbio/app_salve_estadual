//# sourceURL=publicacao.js

;var publicacao = {
    sqCicloAvaliacao:'',
    loadingGrid:true,
    idsSelecionados:[],
    textoPendencia:'',
    init:function() {
        // ajustar a altura maxima dos gride
        var maxHGrides = Math.max(300, window.innerHeight - 360); // minimo de 300px
        $("#containerGridFichasPublicadas").data('height', maxHGrides+'px')
        $("#containerGridFichasNaoPublicadas").data('height', maxHGrides+'px')

        // carregar form filtro fichas finalizadas e NÃO versionadas
        var data = {}
        data.hideFilters = 'situacao,envioIucn,andamentoRevisao';
        data.contexto    = 'publicacao';
        data.callback    = 'publicacao.updateGridNaoPublicadas';
        data.label       = 'Filtrar fichas <b>finalizadas</b>';
        app.loadModule(baseUrl + 'ficha/getFormFiltroFicha', data, '#divFiltrosFichaNaoPublicadas',function(){
            // adicionar evento PASTE no campo Nome cientifico dos filtros
            $('#divFiltrosFichaNaoPublicadas input[name=nmCientificoFiltro]').bind('paste', null,app.inputPaste);
        });

        // carregar form filtro fichas versionadas
        data = {};
        data.hideFilters ='situacao,pendencia,revisada';
        data.contexto = 'publicacao';
        data.callback    = 'publicacao.updateGridPublicadas';
        data.label = 'Filtrar ficha <b>versionadas</b>';

        // ajustar as larguras dos gride para aplicar o plugin floatHead
        var containerWidth = $("#publicacaoContainer").width() - 20
        $("#divGridFichasPublicadasWrapper").css('width', containerWidth+'px')
        $("#divGridFichasNaoPublicadasWrapper").css('width', containerWidth+'px')

        // carregar form filtro fichas PUBLICADAS
        app.loadModule(baseUrl + 'ficha/getFormFiltroFicha', data, '#divFiltrosFichaPublicadas',function(){
            // adicionar evento PASTE no campo Nome cientifico dos filtros
            $('#divFiltrosFichaPublicadas input[name=nmCientificoFiltro]').bind('paste', null,app.inputPaste);

            // disparar a consulta da primeira aba
        window.setTimeout(function(){
            publicacao.sqCicloAvaliacaoChange();
            },2000)
        });


    },
    /**
     * função chamada ao clicar em uma das abas
     * @param params
     * @param ele
     */
    tabClick:function(params,ele){
        if( params.container =='tabPublicadas') {
            if( $("#tableGridFichasPublicadas tbody tr").size() == 0 ) {
                publicacao.updateGridPublicadas();
            }
        }
    },

    /**
     * atualizar tela ao alterar o ciclo selecionado
     * @param params
     */
    sqCicloAvaliacaoChange:function( params ) {
        publicacao.sqCicloAvaliacao  = $("#sqCicloAvaliacao").val();
        publicacao.updateGridNaoPublicadas(params);
        $("#tableGridFichasPublicadas tbody").html('');
        app.selectTab('liTabNaoPublicadas');
        if( !publicacao.sqCicloAvaliacao ) {
            $("#pageContent").hide();
        } else {
            $("#pageContent").show();
        }
    },


    /**
     * atualizar o grid com as fichas na situação finalizada
     * @param params
     */
    updateGridPublicadas:function(params) {
        params = params || {};
        if( ! publicacao.sqCicloAvaliacao )
        {
            $("#frmPublicacao").hide();
            return;
        }
        $("#frmPublicacao").show();
        app.grid('GridFichasPublicadas', params);
    },

    /**
     * atualizar o gride das fichas não publicadas
     * @param params
     */
    updateGridNaoPublicadas:function(params) {
        params = params || {};
        if( ! publicacao.sqCicloAvaliacao )
        {
            $("#frmPublicacao").hide();
            return;
        }
        $("#frmPublicacao").show();
        params.sqCicloAvaliacao = publicacao.sqCicloAvaliacao;
        app.grid('GridFichasNaoPublicadas', params);
    },

    /**
     * callback ao renderizar o gride das fichas NÃO publicadas
     * @param params
     */
    gridNaoPublicadasOnLoad:function(params){
        publicacao.gridesOnLoad('GridFichasNaoPublicadas');
    },

    /**
     * callback ao renderizar o gride das fichas PUBLICADAS
     * @param params
     */
    gridPublicadasOnLoad:function(params){
        publicacao.gridesOnLoad('GridFichasPublicadas');
    },


    /**
     * funcao executado sempre que marcar/desmarcar uma linha
     * @param gridId
     */
    selectRowGrides:function(gridId){
        var $inputSelectedIds = $("#selectedIds"+gridId);
        var lista = ($inputSelectedIds.val() != "") ? $inputSelectedIds.val().split(',') : [];

        // ler todos os checkbox da tabela
        $("#table"+gridId+" input:checkbox[id^=sqFicha_]").map(function(index,ele){
            var index = lista.indexOf( String(ele.value) );
            if( ele.checked ){
                if ( index == -1 ) {
                    lista.push(ele.value)
        }
            } else {
                $(ele).closest('tr').removeClass('tr-selected');
                if ( index > -1 ) {
                    lista.splice(index,1)
                }
            }
        })
        $inputSelectedIds.val( lista.join(',') );
        if( lista.length > 0 ) {
            $("#selectedRows" + gridId).show().html(lista.length+' selecionada(s)');
        } else {
            $("#selectedRows" + gridId).hide().html('0 selecionada(s)');
        }
        $("#tr"+gridId+"Columns i#checkUncheckAll" ).removeClass('glyphicon-check').addClass('glyphicon-unchecked');
    },

    selectRowGridNaoPublicadas:function(ele){
        publicacao.selectRowGrides('GridFichasNaoPublicadas');
    },

    /**
     * funcao executado sempre ao carregar uma página dos grides
     * @param gridId
     */
    gridesOnLoad:function(gridId){
        // marcar checkbox selecionados ao troca de página
        var $checkAll = $("#tr"+gridId+"Columns i#checkUncheckAll" );
        if( $checkAll.size() == 1 && $checkAll.hasClass('glyphicon-check') ) {
            $("#table"+gridId+" input:checkbox[id^=sqFicha_]").prop('checked',true);
            var data = $("#records"+gridId).data()
            $("#selectedRows" + gridId).show().html(data.totalRecords+' selecionada(s)');
        }
        // input hidden com todos os ids selecionados em todas as páginas
        var $inputSelectedIds = $("#selectedIds"+gridId);

        // marcar as linhas selecionadas
        if( $inputSelectedIds.size() == 1 ) {
            var selectedIds = $inputSelectedIds.val() ? $inputSelectedIds.val().split(',') : [];
            selectedIds.map(function(id){
                if( id ) {
                    var input = $("#table"+gridId+" input:checkbox#sqFicha_" + id);
                    if (input.size() == 1) {
                        input.prop('checked', true);
                    }
                }
            });
        }
        // aplicar shiftSelectable
        $("#table"+gridId).shiftSelectable();
    },

    /***
     * Marcar/Desmarcar todas as fichas no grid
     * @param params
     * @param elem
     */
    checkUncheckAll: function( elem,gridId ) {
        var checked;
        elem = elem || $("#tr"+gridId+"Columns i#checkUncheckAll" );
        if ($(elem).hasClass('glyphicon-unchecked')) {
            checked = true;
            $(elem).removeClass('glyphicon-unchecked').addClass('glyphicon-check');
            var data = $("#records"+gridId).data()
            //$("#selectedRows" + gridId).show().html(data.totalRecords+' selecionada(s)');
            $("#selectedRows" + gridId).show().html('todas selecionada(s)');
        } else {
            checked = false;
            $(elem).removeClass('glyphicon-check').addClass('glyphicon-unchecked');
            $("#selectedRows" + gridId).hide().html('0 selecionada(s)');
        }
        $("#table"+gridId +' tbody').find('input:checkbox').each(function() {
            if( ! this.disabled ) {
            this.checked = checked;
                if( !checked ){
                    $(this).closest('tr').removeClass('tr-selected');
                } else {
                    $(this).closest('tr').addClass('tr-selected');
                }
            }
        });
    },

    publicar: function( params ) {
        var ids =  []
        var data = { ids : '', all:'N', sqCicloAvaliacao:publicacao.sqCicloAvaliacao };
        var mensagemComPendencia = '<div class="mt10 text-center red">Aviso: ficha com pendência não será versionada.</div>';
        // verificar se está marcado o check de todas
        var $checkboxAll = $("#trGridFichasNaoPublicadasColumns i#checkUncheckAll");
        if( $checkboxAll.hasClass('glyphicon-check') ) {
            data.ids = '';
            data.all = 'S';
        } else {
            $("#tableGridFichasNaoPublicadas input:checkbox[id^=sqFicha_]:checked").map( function(i,el) {
                ids.push( $(el).val() );
            } );

            if( ids.length == 0 ){
                app.alertInfo('Nenhuma ficha selecionada!');
                return;
            };
            data.ids=ids.join(',');
        }

        var checkRejeicao = '<br><label class="fs10 pointer red">Rejeição CONABIO?&nbsp;<input type="checkbox" id="checkRejeicaoConabio" value="S" title="Opção utilizada quando a proposta tiver sido rejeitada na CONABIO a a ficha deverá passar por uma nova avaliação"/></label>'

        app.confirm('Confirma o versionamento de <b>' + (data.all=='S'?'TODAS as fichas' : ids.length + ' fichas selecionada(s)')+'</b>?' + mensagemComPendencia + checkRejeicao, function() {
            var stRejeicaoConabio = $("#checkRejeicaoConabio").prop('checked') ? 'S' : 'N';
            app.confirm('Tem certeza?<div class="fs mt10">O versionamento não poderá ser desfeito e a(s) ficha(s) voltará(ão) para compilação.'
                +(stRejeicaoConabio=='S' ? '<br><span class="red">Opção REJEIÇÃO CONABIO foi selecionada</span>':'')+'</div>', function() {
                publicacao.updateGridNaoPublicadas({publicar: 'S', ids: data.ids, all: data.all, stRejeicaoConabio: stRejeicaoConabio})
                // se o gride das fichas publicadas estiver carregado, fazer a atualização
                if ($("#containerGridFichasPublicadas").data('grid')) {
                    publicacao.updateGridPublicadas();
                }
                var gridId = 'GridFichasNaoPublicadas'
                $("#selectedIds" + gridId).val('');
                $("#selectedRows" + gridId).hide();
            },null, data, 'Confirmação', 'warning');
        }, null, data, 'Confirmação', 'warning');

    },

    /*
    // NAO É POSSIVEL EXCLUIR OS VERSIONAMENTOS
    cancelar: function( params ) {
        var ids =  []
        var data = { ids : '', all:'N', sqCicloAvaliacao:publicacao.sqCicloAvaliacao };
        // verificar se está marcado o check de todas
        var $checkboxAll = $("#trGridFichasPublicadasColumns i#checkUncheckAll");
        if( $checkboxAll.hasClass('glyphicon-check') ) {
            data.ids = '';
            data.all = 'S';
        } else {
            $("#tableGridFichasPublicadas input:checkbox[id^=sqFicha_]:checked").map( function(i,el) {
                ids.push( $(el).val() );
            } );

            if( ids.length == 0 ){
                app.alertInfo('Nenhuma ficha selecionada!');
                return;
            };
            data.ids=ids.join(',');
        }
        app.confirm('Confirma CANCELAMENTO da publicação de <b>' + (data.all=='S'?'TODAS as fichas' : ids.length + ' fichas selecionada(s)')+'</b>?', function() {
           publicacao.updateGridPublicadas({cancelarPublicacao: 'S', ids:data.ids, all:data.all } )
        }, null, data, 'Confirmação', 'warning');
    },*/

    selectImages:function(params) {
        // function(strId, strTitle, strUrl, objData, callback, intWidth, intHeight, boolModal, boolMaximized, onClose, position, theme, resizable ) {
        app.panel('pnlSelectImages','Selecão de Imagens para Destaque','/publicacao/selectImages',params,function(panel){

        },800,600,true,false,null,'center')
    },

    showImage:function(urlImage) {

        alert('Exibir mensagem');
    },

    updateImageDestaque:function( ele, sqImagem) {
        var $footer = $("#selectImagesContainer div#divFooter-"+sqImagem);
        var data = { inDestaque:(ele.checked ? 'S': 'N'), sqFichaMultimidia:sqImagem }
        app.ajax(baseUrl + 'publicacao/updateDestaque',data, function(res) {
            if( res.status == 0 ) {
                if( ele.checked ) {
                    $footer.css('background-color','#b2d5b2');
                } else {
                    $footer.css('background-color','transparent');
                }
            } else {
                // voltar o check para o estado anterior
                $(ele).prop('checked', !ele.checked );
            }
        }, '', 'JSON');

    },
    editDoi:function(sqFicha) {
        var $spanWrapper = $("#span-edit-doi-"+sqFicha);
        var $spanText    = $("#span-edit-doi-text-"+sqFicha);
        $spanText.data('oldValue',$spanText.text() );
        $spanWrapper.hide();
        $spanWrapper.after('<div id="div-input-doi'+sqFicha+'" class="text-nowrap">' +
            '<input id="input-doi'+sqFicha+'" placeholder="nº DOI" class="fld form-control ignoreChange" style="min-width:230px;" value="'+$spanText.text()+'" ' +
            'type="text"> <i class="blue fld fa fa-save cursor-pointer" title="Gravar" onClick="publicacao.saveDoi('+sqFicha+',true)"></i> </div>');
        app.focus( 'input-doi' + sqFicha );
        var $input = $("#input-doi"+sqFicha);
        $input.on('keyup', function( e ) {
            if( e.key=='Enter' ) {
                $input.next().click();
            };
        });
    },
    saveDoi: function(sqFicha) {
        var $divInput    = $("#div-input-doi"+sqFicha);
        var $spanWrapper = $("#span-edit-doi-"+sqFicha);
        var $spanText    = $("#span-edit-doi-text-"+sqFicha);
        var $input       = $("#input-doi"+sqFicha);
        if( $input.val().trim().toLowerCase() != $spanText.data('oldValue').trim().toLowerCase() ) {
            var data = {sqFicha: sqFicha, dsDoi: $input.val().trim().toLowerCase() }
            app.ajax(baseUrl + 'publicacao/saveDoi',
                data,
                function (res) {
                    if (res.status == 0) {
                        $spanText.text(data.dsDoi);
                    }
                }, null, 'JSON');
        }
        $divInput.remove();
        $spanWrapper.show();
    },
    gerarAtualizarDoi: function(sqFicha) {
        var ids =  []
        if(Number.isInteger(sqFicha) ){ // envio/update ID individual
            ids.push(sqFicha);
            $("#span-edit-doi-"+sqFicha).after('<img src="images/loading.gif"></img>');
        }else{ // envios/updates vários IDs
            $("#divGridFichasPublicadas input:checkbox:checked").map( function(i,el) {
                ids.push( $(el).val() );
                $("#span-edit-doi-"+$(el).val()).after('<img src="images/loading.gif"></img>');
            });
        }
        if( ids.length == 0 )
        {
            app.alertInfo('Nenhuma ficha selecionada!');
            return;
        };

        var data = { ids : ids.join(',') };

        app.ajax(baseUrl + 'publicacao/gerarAtualizarDoi',
            data,
            function (res) {
                jQuery.support.cors = true;

                if (res.status == 0) {
                    app.alertInfo('Arquivos XML/DOI enviados com sucesso! Ficará em analise até homologação do crossref.org');
                }
                if( ids.length > 0 ){
                    setTimeout(() => {
                        ids.forEach(value => {
                            $("#span-edit-doi-"+value).next().remove();
                            $("#span-edit-doi-"+value).html('EM ANALISE');
                        });
                    }, 1000);
                }
            }, null, 'JSON');

    },
    verificaDoiCrossref: function(sqFicha){
        var ids =  []
        if(sqFicha && Number.isInteger(sqFicha) ){ // consulta individual
            ids.push(sqFicha);
            $("#span-edit-doi-"+sqFicha).after('<img src="images/loading.gif"></img>');
            $('#sqFicha_'+sqFicha).addClass('glyphicon-check');
            $('#sqFicha_'+sqFicha).prop('checked', true);
        }else{ // consulta de varias fichas
            $("#divGridFichasPublicadas input:checkbox:checked").map( function(i,el) {
                ids.push( $(el).val() );
                $("#span-edit-doi-"+$(el).val()).after('<img src="images/loading.gif"></img>');
            } );
        }

        if( ids.length == 0 )
        {
            return app.alertInfo('Nenhuma ficha selecionada!');
        }

        var data = { ids : ids.join(',')};
        app.ajax(baseUrl + 'publicacao/verificaDoiCrossref',
            data,
            function (res) {
                jQuery.support.cors = true;

                setTimeout(() => {
                    ids.forEach(value => { $("#span-edit-doi-"+value).next().remove(); });
                }, 1000);

                $("#tbFichasPublicadas tbody").find('input:checkbox').each(function() {
                    this.checked = false;
                    $('#checkUncheckAll').removeClass('glyphicon-check').addClass('glyphicon-unchecked');
                });

                // atualiza a coluna do grid com a informação obtida
                for (var sq_ficha in res.data) {
                    $('#span-edit-doi-'+sq_ficha).html(res.data[sq_ficha])
                }

            }, null, 'JSON');
    },
    adicionarPendenciaRevisao:function( params ) {
        var data = publicacao.getLinhasSelecionadas('GridFichasNaoPublicadas')
        if( data.all == 'S'){
            app.alertInfo('Adicionar Pendência Revisão não pode ser utilizada quando todas as fichas estiverem selecionadas');
            return;
        }
        params.onClose  = 'publicacao.updateGridNaoPublicadas';
        params.sqFichas = publicacao.idsSelecionados.join(',');
        params.contexto = 'revisao'
        showFormPendencia(params);
    },
    /**
     * excluir as pendneicas de revisão das fichas
     * @param params
     */
    removerPendenciaRevisao:function( params ){
        var data = publicacao.getLinhasSelecionadas('GridFichasNaoPublicadas')
        if( data.all == 'S'){
            app.alertInfo('Remover Pendência Revisão não pode ser executada quando todas as fichas estiverem selecionadas');
            return;
        }
        var data = {ids: publicacao.idsSelecionados.join(',') }
        if(!params.confirm){
            app.confirm('Confirma a exclusão das pendências de revisão de '+publicacao.idsSelecionados.length+' ficha(s)?',function(){
                data.confirm='S'
                publicacao.removerPendenciaRevisao( { confirm:true} )
            });
            return;
        }
        app.ajax(baseUrl + 'publicacao/deletePendenciaFichas',
            data,
            function (res) {
                if (res.status == 0) {
                    publicacao.updateGridNaoPublicadas();
                }
            }, null, 'JSON');

    },

    /**
     * marcar as pendencias de revisão como acietas e revisadas
     * @param params
     */
    revisarPendenciaRevisao:function( params ){
        var data = publicacao.getLinhasSelecionadas('GridFichasNaoPublicadas')
        if( data.all == 'S'){
            app.alertInfo('Pendência revisão não pode ser adicionada quando todas as fichas estiverem selecionadas');
            return;
        }
        var data = {ids: publicacao.idsSelecionados.join(',') }

        if(!params.confirm){
            app.confirm('Confirma a revisão em LOTE de '+publicacao.idsSelecionados.length+' ficha(s)?',function(){
                data.confirm='S'
                publicacao.revisarPendenciaRevisao( { confirm:true} )
            });
            return;
        }


        app.ajax(baseUrl + 'publicacao/revisarPendencias',
            data,
            function (res) {
                if (res.status == 0) {
                    publicacao.updateGridNaoPublicadas();
                }
            }, null, 'JSON');
    },

    getLinhasSelecionadas:function(gridId) {
            publicacao.idsSelecionados = [];
        var data = {ids: '', all: 'N', sqCicloAvaliacao: publicacao.sqCicloAvaliacao};
        // verificar se está marcado o check de todas
        var $checkboxAll = $("#tr" + gridId + "Columns i#checkUncheckAll");
        if ($checkboxAll.hasClass('glyphicon-check')) {
            data.ids = '';
            data.all = 'S';
        } else {
            $("#table" + gridId + " input:checkbox[id^=sqFicha_]:checked").map(function (i, el) {
                publicacao.idsSelecionados.push($(el).val());
            });
            if ( publicacao.idsSelecionados.length == 0) {
                app.alertInfo('Nenhuma ficha selecionada!');
            } else {
                data.ids = publicacao.idsSelecionados.join(',');
            }
        }
        return data
    },
    situacaoPendenciaChange:function( params, ele, evt ) {
        if (evt) {
            evt.preventDefault(); // não alterar o input checkbox ainda
        }
        if (!params.sqFicha && params.sqFichaPendencia) {
            app.alertInfo('Parâmetros incorretos');
        }
        if (params.sqFicha) {
            app.ajax(app.url + 'publicacao/updateSituacaoRevisao', {sqFicha: params.sqFicha, sqFichaPendencia: params.sqFichaPendencia},
                function (res) {
                    if (res.status == 0) {
                        ele.checked = ( res.data.stRevisado == 'S' )
                        $("#tdLogModalVisualizarPendenciaRevisao" + params.sqFichaPendencia ).html( res.data.log );
                        // flag para indicar que alguma ação foi feita na modal
                        $("#modalVisualizarPendenciaRevisaoChanged").val('S');
                    }
                }, 'Gravando...');
        }
    },
    editarPendencia:function( params ){
        if( params.sqFichaPendencia ) {
            app.closeWindow('modalVisualizarPendenciaRevisaoFicha');
            showFormPendencia(params)
        }
    },
    excluirPendencia:function( params ){
        if( params.sqFichaPendencia ) {
            if( ! params.confirm ) {
                app.confirm('Confirma excusão da pendência?',function(){
                    publicacao.excluirPendencia({contexto:params.contexto, sqFichaPendencia: params.sqFichaPendencia,confirm:true})
                })
                return;
            }
            app.ajax(app.url + 'publicacao/deletePendencia', {sqFicha: params.sqFicha, sqFichaPendencia: params.sqFichaPendencia,contexto:params.contexto},
                function (res) {
                    if (res.status == 0) {
                        $("#trPendenciaModalVisualizarPendenciaRevisao"+params.sqFichaPendencia).remove();
                        publicacao.updateGridNaoPublicadas(params)
                    }
                }, 'Gravando...');
        }
    },

    // INTEGRAÇÃO SIS/IUCN
    /**
     * Enviar as fichas selecionadas para o SIS/IUCN
     * @param params
     */
    transmitirIucn: function( params ) {
        var ids =  []
        var data = { ids : '', all:'N', sqCicloAvaliacao:publicacao.sqCicloAvaliacao };
        // verificar se está marcado o check de todas
        var $checkboxAll = $("#trGridFichasPublicadasColumns i#checkUncheckAll");
        if( $checkboxAll.hasClass('glyphicon-check') ) {
            data.ids = '';
            data.all = 'S';
        } else {
            $("#tableGridFichasPublicadas input:checkbox[id^=sqFicha_]:checked").map( function(i,el) {
                ids.push( $(el).val() );
            } );

            if( ids.length == 0 ){
                app.alertInfo('Nenhuma ficha selecionada!');
                return;
        }
            data.ids = ids.join(',')
        }

        if( ! params.confirm ) {
            app.confirm('Confirma TRANSMISSÃO de <b>' + (data.all == 'S' ? 'TODAS as fichas' : ids.length + ' fichas selecionada(s)</b> para o SIS/IUCN') + '?', function () {
                data.confirm = true
                data.transmitirIucn=true
                publicacao.updateGridPublicadas({transmitirIucn: 'S', ids:data.ids, all:data.all } )
            }, null, data, 'Confirmação', 'warning');
            return;
        }
    },
    verLogTransmissao:function(sqIucnTransmissao,sqFicha){
        app.ajax(app.url + 'publicacao/verLogTransmissao', {sqIucnTransmissao:sqIucnTransmissao,sqFicha:sqFicha},
            function( res ) {
                if ( res.status == 0 ) {
                    app.panel('pnlLogTransmissao','SALVE - Integracao SIS/IUCN - Log','',null,function( panel ){
                    }
                    ,800
                    ,600
                    ,false
                    ,true
                    ,null
                    ,null
                    ,"silver"
                    ,null,null
                    ,res.data.log)
                }
            }, 'Gravando...');
    },
    publicarVersao:function(rowNum, sqFichaVersao, nuVersao ) {
        //app.publicarVersao(sqFichaVersao)
        if( event ){event.preventDefault();}
        var $tr = $("#tbFichasPublicadas tbody tr#gdFichaRow_" + rowNum);
        var $radio = $tr.find("input[name=radioExibirVersaoModuloPublico"+rowNum+"][value="+sqFichaVersao+"]");
        var checked =  $radio.data('checked');
        var msg = checked ? 'Confirma o CANCELAMENTO do publicação da versão '+nuVersao+'?' :
            'Confirma a PUBLICAÇÃO da versão '+nuVersao+'?<br><span class="blinking-red">As informações da ficha ficarão visíveis para o público.</span>';
        app.confirm(msg,function(){
            app.publicarVersao(sqFichaVersao,function(res){
                $tr.find('input[type=radio]').data('checked',false);
                $radio.prop('checked',!checked);
                $radio.data('checked',!checked);
            });
        })
    },
};

// inicializar
window.setTimeout(function(){publicacao.init();},1000)
