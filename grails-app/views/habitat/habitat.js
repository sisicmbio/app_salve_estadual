//# sourceURL=/views/habitat.js
;
var CLIPBOARD = null;
var curNode;
var treeview;
var habitat = {
    init:function() {
        // Initialize Fancytree
        $("#treeview").fancytree({
            icon: true, // Display node icons
            extensions: [ "table"],
            tabindex: "1",
            source: {
                url: app.url+"habitat/getTreeView",
                data: {},
                cache: false,
                dataType: "json",
                loadError: function(e, data) {
                    alert('servidor ainda fora do ar!');
                    var error = data.error;
                    if (error.status && error.statusText) {
                        data.message = "Ajax error: " + data.message;
                        data.details = "Ajax error: " + error.statusText + ", status code = " + error.status;
                    } else {
                        data.message = "Custom error: " + data.message;
                        data.details = "An error occurred during loading: " + error;
                    }
                },
                error: function(xhr, message) {
                    alert('servidor fora do ar : ' + xhr.statusText);
                },
            },
            lazyLoad: function(event, data) {
                //console.log(data.node.key)
                data.result = {
                    url: app.url+"habitat/getTreeView?key=" + data.node.key, //"fancytree-master/demo/ajax-sub2.json",
                    error: function(xhr, message) {
                        alert('servidor fora do ar : ' + xhr.statusText);
                    },
                    loadError: function(e, data) {
                        alert('servidor ainda fora do ar!');
                        var error = data.error;
                        if (error.status && error.statusText) {
                            data.message = "Ajax error: " + data.message;
                            data.details = "Ajax error: " + error.statusText + ", status code = " + error.status;
                        } else {
                            data.message = "Custom error: " + data.message;
                            data.details = "An error occurred during loading: " + error;
                        }
                    },
                };
            },
            init: function(event, data) {
                curNode = data.tree.getFirstChild();
                treeview = $("#treeview").fancytree("getTree");
                // selecionar o node com key=1
                // data.tree.activateKey("38")
                // ou $("#treeview").fancytree("getTree").getNodeByKey("1").setActive();
                // selecionar pelo primeiro filho
                // ou data.tree.getFirstChild().setFocus();
            },
            //---------------------------------------------------------------
            table: {
                indentation: 20, // identação da hieraquia
            },
            //---------------------------------------------------------------
            activate: function(event, data) {
                curNode = data.node;
            },
            //---------------------------------------------------------------
            postProcess: function(event, data) {
                var orgResponse = data.response;
                if (orgResponse.status === "ok") {
                    data.result = orgResponse.result;
                } else {
                    data.result = {
                        error: "ERROR #" + orgResponse.faultCode + ": " + orgResponse.faultMsg
                    }
                }
            },
            //---------------------------------------------------------------
            renderColumns: function(event, data) {
                var node = data.node,
                $tdList = $(node.tr).find(">td");
                $tdList.eq(2).find("a").each(function(){
                    $(this).data('id',node.key);
                })
                $tdList.eq(1).html(node.data.iucnHabitat);
            },
        });// fim treeview
    },
    // fim init
    save:function( params )
    {
        var node = curNode;
        var data = $("#frmHabitat").serializeArray();
        data = app.params2data(params, data);
        if( node )
        {
            $(node.span).addClass("pending");
        }
        app.ajax(app.url+'habitat/save', data,
            function(res) {
                var iucnHabitat ='';
                if( node )
                {
                    $(node.span).removeClass("pending");
                }
                if( res.status==0)
                {
                    if( $('select[name="sqIucnHabitat"]').val() )
                    {
                        iucnHabitat = $('select[name="sqIucnHabitat"] option:selected').text().trim();
                    }
                    if( res.data.codigoSistema ) {
                        $("#codigoSistema").val(res.data.codigoSistema);
                    }
                    if( res.data.ordem ) {
                        $("#ordem").val(res.data.ordem);
                    }
                    if( data.operacao == 'habitat.edit')
                    {
                        $tdList = $(node.tr).find(">td");
                        node.setTitle(data.codigo+' '+data.descricao);
                        $tdList.eq(1).html(iucnHabitat);
                    }
                    else if( data.operacao == 'habitat.add')
                    {

                        if( node )
                        {
                            // adicionou um filho
                            node.editCreateNode('child',{'title':data.codigo+' '+data.descricao,'key':res.data.id,'iucnHabitat':iucnHabitat});
                        }
                        else
                        {
                            // adicionou um pai
                            var raiz = treeview.getRootNode();
                            node = raiz.addChildren({'title':data.codigo+' '+data.descricao,'key':res.data.id,'iucnHabitat':iucnHabitat});
                            curNode=node;
                        }
                        node.tree.activateKey(String(res.data.id));
                    }
                }
                app.closeWindow('cadHabitat');
            }, null, 'json'
        );


        //curNode.load();
    }, // uso
    edit:function( params )
    {
        habitat.showModal(params);
    },
    add:function( params )
    {
        if( ! params.id )
        {
            curNode=null;
        }
        habitat.showModal(params);
    },
    delete:function( params )
    {
        app.confirm('Confirma e exclusão do item <b>'+curNode.title+'</b>?',function(d,data)
        {
            app.ajax(app.url+'habitat/delete', params,
            function(res) {
                if( res.status == 0 )
                {
                    var proximo = (curNode.getNextSibling() || curNode.getParent() )
                    curNode.remove();
                    if( proximo )
                    {
                            curNode.tree.activateKey(proximo.key);
                    }
                }
            });
        })
    },
    showModal:function( params ) {
        app.openWindow({
             id:'cadHabitat'
            ,url:app.url + 'habitat/getForm'
            ,width:750
            ,data:{'reload':true,'id':params.id,'operacao':params.action}
            ,height:450
            ,title:'Manutenção do Habitat'
            ,autoOpen:true
            ,modal:true}
            // open
            ,function(data,e){
                // onShow
                data = data.data;
                // alterar os valores dos parametros que serão enviados ao clicar no botão salvar, de acordo com o contexto
                for (key in data) {
                    if (key !== 'action') {
                        $("#btnSaveFrmHabitat").data(key, data[key]);
                    }
                }
                app.focus('codigo');
            }
            // close
            ,function(data,e) {
                app.reset('frmCadHabitat');
        });
    }
}
habitat.init();
