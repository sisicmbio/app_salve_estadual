<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="ajax"/>
        <title>
        Tabelas de Apoio
        </title>
    </head>
    <body>
    <div class="col-sm-12">
            <div class="panel panel-default w100p" style="">
                <div class="panel-heading">
                    <span>
                        Tabelas de Apoio
                    </span>
                    <button id="btnClose" data-action="app.unloadModule" class="btn btn-default pull-right btn-xs" title="Fechar"><span class="glyphicon glyphicon-remove btnClose"></span></button>
                </div>

                <div class="panel-body">
                <table id="treeview" class="table table-condensed table-hover table-striped fancytree-fade-expander treeTable">
                    <colgroup>
                    <col style="width:auto"></col>
                    <col style="width:220px"></col>
                    <col style="width:480px"></col>
                    </colgroup>
                    <thead>
                        <tr>
                            <th>Descrição</th>
                            <th>Ordem</th>
                            <th>Código</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td></td>
                            <td style="white-space: nowrap" >
                                <img src="/salve-estadual/assets/en.png" title="Tradução habilitada." class="treeInput-image hidden img-traducao cursor-pointer">
                                <img src="/salve-estadual/assets/shp.png" title="Cadastrar shapefile." class="treeInput-image hidden img-shp cursor-pointer">
                                <input class="form-control treeInput" type="text" maxlenght="20" name="ordem" value="">
                            </td>
                            <td><input class="form-control treeInput" type="text" maxlenght="50" name="codigo" value=""></td>
                        </tr>
                    </tbody>
                </table>
            </div>

        <asset:javascript src="corpGeo.js" defer="true"/>
    </body>
</html>
