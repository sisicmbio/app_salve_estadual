<div class="modal" id="modalAdd" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 style="text-align:center;">
                    Administração - Cadastro de Registro
                </h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="formAdd">
                    <input type="hidden" id="id" name="id" value="${data?.id}"/>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">
                            Tabela:
                        </label>
                        <div class="col-sm-8">
                            <select name="pai" class="form-control" id="selectAdd">
                                <g:each in="${data}" var="item">
                                    <option value="${item?.id}">
                                        ${item?.descricao}
                                    </option>
                                    <g:if test="item.itens">
                                        <g:each in="${item.itens}" var="subItem">
                                            <option value="${subItem?.id}">
                                                &nbsp;&nbsp;&nbsp;${subItem?.descricao}
                                            </option>
                                        </g:each>
                                    </g:if>
                                 </g:each>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputCodigo" class="col-sm-2 control-label">
                            Código
                        </label>
                        <div class="col-sm-8">
                            <input name="codigo" type="text" class="form-control form-clean" id="inputCodigo" placeholder="Informo o código" value="${data?.codigo}"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputDescricao" class="col-sm-2 control-label">
                            Descrição
                        </label>
                        <div class="col-sm-8">
                            <input name="descricao" type="text" class="form-control form-clean" id="inputDescricao" placeholder="Informe a Descrição" value="${data?.descricao}" required="true" data-msg-required="Campo obrigatório"/>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-success" id="btnSave">
                    Gravar
                </button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">
                    Fechar
                </button>
            </div>
        </div>
    </div>
</div>
