//# sourceURL=/salve-estadual/views/dadosApoio/dadosApoio.js

// api: http://wwwendt.de/tech/fancytree/demo/sample-api.html
// jquery-ui icons https://code.google.com/archive/p/vallalarblogs/downloads?page=2
// metodos: http://www.wwwendt.de/tech/fancytree/doc/jsdoc/FancytreeNode.html#load
/*
$.each(node.getParent().children,function(k,v) { log( v ) }) ou
node.getParent().visit(function(n){log( n )})
*/


var CLIPBOARD = null;
var curNode;
$(function() {
    // Initialize Fancytree
    $("#treeview").fancytree({
        autoCollapse: false, // Automatically collapse all siblings, when a node is expanded
        autoScroll: false, // Automatically scroll nodes into visible area
        clickFolderMode: 1, // 1:activate, 2:expand, 3:activate and expand, 4:activate (dblclick expands)
        checkbox: false, // Show checkboxes
        debugLevel: 0, // 0:quiet, 1:normal, 2:debug
        disabled: false, // Disable control
        focusOnSelect: false, // Set focus when node is checked by a mouse click
        escapeTitles: false, // Escape `node.title` content for display

        generateIds: false, // Generate id attributes like <span id='fancytree-id-KEY'>
        idPrefix: "id_", // Used to generate node id´s like <span id='fancytree-id-<key>'>
        icon: true, // Display node icons
        keyboard: true, // Support keyboard navigation
        keyPathSeparator: "/", // Used by node.getKeyPath() and tree.loadKeyPath()
        minExpandLevel: 1, // 1: root node is not collapsible
        quicksearch: true, // Navigate to next node by typing the first letters

        rtl: false, // Enable RTL (right-to-left) mode
        selectMode: 1, // 1:single, 2:multi, 3:multi-hier
        tabindex: "0", // Whole tree behaves as one single control

        tooltip: false, // Use title as tooltip (also a callback could be specified)
        //titlesTabbable: true, // Node titles can receive keyboard focus // da problema ao editar o campo e clicar nele

        extensions: [ "edit", "glyph", "table", "gridnav", "wide", "contextMenu"],
        init: function(event, data) {
            //console.log('Init: chamado');
            // selecionar o node com key=1
            data.tree.activateKey("38")
            // ou $("#treeview").fancytree("getTree").getNodeByKey("1").setActive();
            // selecionar pelo primeiro filho
            // ou data.tree.getFirstChild().setFocus();
        },
        //
        dnd: {
            focusOnClick: true,
            preventVoidMoves: true,
            preventRecursiveMoves: true,
            autoExpandMS: 400,
            draggable: {
                //zIndex: 1000,
                // appendTo: "body",
                // helper: "clone",
                scroll: false,
                revert: "invalid"
            },
            dragStart: function(node, data) {
                return node.data.pai > 0
            },
            dragEnter: function(node, data) {

                if (node.parent !== data.otherNode.parent) {
                    return false;
                }
                //return true;
                return ["after", "before"]; // permite inserir antes e depois não como filho do nó destino

                /*
                    if( data.otherNode && node )
                    {
                        return ( node.data.pai == data.otherNode.data.pai && node.data.pai > 0 );
                    }
                    return false;
                    */
            },
            dragDrop: function(node, data) {
                //data.otherNode.copyTo(node, data.hitMode);
                data.otherNode.moveTo(node, data.hitMode);
            }
        },
        // Plugin: https://github.com/mar10/fancytree/wiki/ExtEdit
        // Exemplo: http://wwwendt.de/tech/fancytree/demo/sample-ext-edit.html#
        edit: {
            triggerStart: ["f2", "shift+click", "mac+enter"],
            //adjustWidthOfs: 200,   // null: don't adjust input size to content
            inputCss: { width: "400px","color":"blue","padding":"3px","font-size":"16px" },
            beforeEdit: function(event, data){
                // Return false to prevent edit mode
                return true;
            },
            close: function(event, data) {
                if (data.save && data.isNew) {
                    // Quick-enter: add new nodes until we hit [enter] on an empty title
                    $("#treeview").trigger("nodeCommand", {
                        cmd: "addSibling"
                    });
                }
            },
            save: function(event, data) {
                nodeSave( data )
            },
            beforeClose: function(event, data){
                if( data.originalEvent.type === "mousedown" ) {
                    data.originalEvent.preventDefault();
                    return false;
                }
              // Editing is about to end (either cancel or save).
              // Additional information is available:
              // - `data.dirty`:    true if text was modified by user.
              // - `data.input`:    The input element (jQuery object).
              //                    `data.input.val()` returns the new node title.
              // - `data.isNew`:    true if this node was newly created using `editCreateNode()`.
              // - `data.orgTitle`: The previous node title text.
              // - `data.originalEvent`:
              //                    Contains the originating event (i.e. blur, mousdown, keydown).
              // - `data.save`:     false if saving is not required, i.e. user pressed
              //                    cancel or text is unchanged.
              //                    This value may be changed to override default behavior.
              // Return false to prevent this (keep the editor open), for example when
              // validations fail.
            },
            close: function(event, data) {
               // Editor was removed.
               //var spanTxTraduzido = $('<span>'+data.orgTitle+'</span>').find('span.treeInput-text-traduzido')

                // Alteracoes integração com SIS/IUCN
                var tagSpanTraducao = '';
                if( data.node.data.traducao ) {
                    try{tagSpanTraducao = '<span ' + (data.orgTitle.split('<span ')[1]);} catch( e ){}
                }
                if( data.save ) {
                    // Since we started an async request, mark the node as preliminary
                    $(data.node.span).addClass("pending");
                }
                if( data.node.data.traducao ){
                    data.node.title += tagSpanTraducao
                }
            },
        },
        source: {
            url: app.url+"dadosApoio/getTreeView", //"data-apoio.php",
            data: {},
            cache: false,
            dataType: "json",
            loadError: function(e, data) {
                alert('servidor ainda fora do ar!');
                var error = data.error;
                if (error.status && error.statusText) {
                    data.message = "Ajax error: " + data.message;
                    data.details = "Ajax error: " + error.statusText + ", status code = " + error.status;
                } else {
                    data.message = "Custom error: " + data.message;
                    data.details = "An error occurred during loading: " + error;
                }
            },
            error: function(xhr, message) {
                alert('servidor fora do ar : ' + xhr.statusText);
            },
            //debugDelay: 1000
        },
        table: {
            //checkboxColumnIdx: 1, // coluna para adicionar o checkbox
            //nodeColumnIdx: 2, // coluna para informar o codigo da hierarquia getIndexHier()
            indentation: 20, // identação da hieraquia
        },

        gridnav: {
            autofocusInput: false,
            handleCursorKeys: true
        },
        activate: function(event, data) {
            //console.log('Activate: chamado.');
            curNode = data.node;
            //$("#echoActive").text(data.node.title);
            //              alert(node.getKeyPath());
            //if( data.node.url )
            //window.open(data.node.url, data.node.target);
        },
        deactivate: function(event, data) {
            //$("#echoSelected").text("-");
        },
        focus: function(event, data) {
            //$("#echoFocused").text(data.node.title);
        },
        blur: function(event, data) {
            //$("#echoFocused").text("-");
        },
        beforeSelect: function(event, data) {
            // A node is about to be selected: prevent this, for folder-nodes:
            //if( data.node.isFolder() ){
            //      return false;
            //}
            return true;
        },
        lazyLoad: function(event, data) {
            //console.log(data.node.key)
            data.result = {
                url: app.url+"dadosApoio/getTreeView?key=" + data.node.key, //"fancytree-master/demo/ajax-sub2.json",
                error: function(xhr, message) {
                    alert('servidor fora do ar : ' + xhr.statusText);
                },
                loadError: function(e, data) {
                    alert('servidor ainda fora do ar!');
                    var error = data.error;
                    if (error.status && error.statusText) {
                        data.message = "Ajax error: " + data.message;
                        data.details = "Ajax error: " + error.statusText + ", status code = " + error.status;
                    } else {
                        data.message = "Custom error: " + data.message;
                        data.details = "An error occurred during loading: " + error;
                    }
                },
                //debugDelay: 1000,
            };
        },
        renderColumns: function(event, data) {
            var node = data.node,
                $tdList = $(node.tr).find(">td");
            //$tdList.eq(0).text(node.getIndexHier());
            // coluna 2 não mexer
            // (Index #2 is rendered by fancytree)

            // Alteracoes integração com SIS/IUCN
            // preencher a coluna 4
            $inputOrdem = $tdList.eq(1).find("input");
            $inputOrdem.val(node.data.ordem);
            $tdList.eq(2).find("input").val(node.data.codigo);

            var parentNode = node.parent
            var habilitarTraducao = node.data.habilitarTraducao;
            var isBaciaHidrografica = node.data.isBaciaHidrografica;
            while( ! parentNode.isRoot() ) {
                habilitarTraducao = parentNode.data.habilitarTraducao;
                isBaciaHidrografica = parentNode.data.isBaciaHidrografica;
                parentNode = parentNode.parent;
            }
            // exibir / esconder a bandeira da tradução
            var $imgTraducao = $tdList.eq(1).find("img.img-traducao");
            var imgData={}
            if( ! habilitarTraducao){
                $imgTraducao.addClass('cursor-default');
                $imgTraducao.addClass('hidden');
                $inputOrdem.removeClass('treeInput-ordem-com-traducao');
            } else {
                $imgTraducao.removeClass('hidden');
                $inputOrdem.addClass('treeInput-ordem-com-traducao');
                if (! node.parent.isRoot() ) {
                    imgData.table = 'apoio';
                    imgData.id = node.key;
                    $imgTraducao.attr('title','Clique para editar a tradução.');
                    $imgTraducao.addClass('cursor-pointer');
                    $imgTraducao.off('click').on('click', function (e) {
                        var img = $(e.target);
                        var data = {idRegistroTraducao: img.data('id'), noTabelaTraducao: img.data('table')}
                        modal.janelaTraducaoTextoFixo(data, null, function (res) {
                            if( res.inSaved =='S') {
                                if (res.txRevisado && res.idRegistroTraducao) {
                                    $("#traducaoApoio-" + res.idRegistroTraducao).html(res.txRevisado);
                                }
                            }

                        });
                    })
                } else {
                    var srcImageRoot = $imgTraducao.prop('src').replace(/en\.png/,'en_disabled.png')
                    $imgTraducao.addClass('cursor-default');
                    $imgTraducao.attr('src',srcImageRoot);
                }
            }
            $imgTraducao.data(imgData);

            // abrir janela para edicao do shapefile da bacia
            var $imgShp = $tdList.eq(1).find("img.img-shp");
            if( $imgShp.size()==1) {
                imgData = {}
                if (!isBaciaHidrografica) {
                    $imgShp.addClass('cursor-default');
                    $imgShp.addClass('hidden');
                    $inputOrdem.removeClass('treeInput-ordem-com-bacia');
                } else {
                    $imgShp.removeClass('hidden');
                    $inputOrdem.addClass('treeInput-ordem-com-bacia');
                    if (!node.parent.isRoot()) {
                        imgData.table = 'apoio';
                        imgData.id = node.key;
                        imgData.codigo = node.data.codigo;
                        imgData.text = node.data.text;
                        $imgShp.attr('title', 'Clique para editar o shapefile.');
                        $imgShp.addClass('cursor-pointer');
                        $imgShp.off('click').on('click', function (e) {
                            var img = $(e.target);
                            var data = {
                                     id: img.data('id')
                                , table: img.data('table')
                                , text : img.data('text')
                                , codigo:img.data('codigo')
                            };
                            modal.janelaApoioShapeBacia(data, function(data){
                                // open
                                apoioShapeBacia.init(data);
                            }, function (res) {
                                if (res.inSaved == 'S') {
                                }
                            });
                        })
                    } else {
                        var srcImageRoot = $imgShp.prop('src').replace(/shp\.png/, 'shp_disabled.png')
                        $imgShp.addClass('cursor-default');
                        $imgShp.attr('src', srcImageRoot);
                    }
                }
                $imgShp.data(imgData);
            }

            // Static markup (more efficiently defined as html row template):
            // $tdList.eq(3).html("<input type='input' value='" + "" + "'>");
            // ...

        },
        postProcess: function(event, data) {
            var orgResponse = data.response;
            if (orgResponse.status === "ok") {
                data.result = orgResponse.result;
            } else {
                data.result = {
                    error: "ERROR #" + orgResponse.faultCode + ": " + orgResponse.faultMsg
                }
            }
        },
        contextMenu: {
            menu: {
                'addSibling':{
                    'name':'Adicionar Item',
                    'icon':'paste'
                },
                'edit': {
                    'name': 'Alterar',
                    'icon': 'edit'
                },
                'addChild':{
                    'name':'Adicionar Filho',
                    'icon':'add'
                },
                /*'cut': {
                    'name': 'Cut',
                    'icon': 'cut'
                },
                'copy': {
                    'name': 'Copy',
                    'icon': 'copy'
                },
                'paste': {
                    'name': 'Paste',
                    'icon': 'paste'
                },
                */
                'delete': {
                    'name': 'Excluir',
                    'icon': 'delete',
                    'disabled': false
                },
            },
            actions: function(node, action, options) {
                if( action == 'edit')
                {
                    node.editStart();
                }
                else if( action == 'addChild')
                {
                    node.editCreateNode('child',{'title':'','key':'-1','ordem':'','codigo':''});
                }
                else if( action == 'addSibling')
                {
                    node.editCreateNode('after',{'title':'','key':'-1','ordem':'','codigo':''});
                }
                else if( action == 'delete')
                {
                    nodeDelete( node );
                }
            }
        },
    }).on("nodeCommand", function(event, data) {});

    // escutar os campos da class tree input
    //$("input").find('.treeInput', )

    $(document).on('change', '.treeInput', function (evt) {

      //log( $(node).closest('tr') );
      //$(node).closest('tr').find('input').each(function(){ log( this.value ) } );
      //curNode.edit();
      nodeSave( curNode );

    });


    var nodeSave=function( data )
    {
        var node = curNode;
        var params       = node.data;
        params.descricao = ( (data && data.input) ? data.input.val() : node.title);
        // remover o texto <span> adicionado pelo processo de tradução
        params.descricao = params.descricao.split('<span')[0];
        params.id        = node.key
        params.pai       = ( /^root/.test(node.getParent().key) ? 0 : node.getParent().key );
        $(node.tr).find('input[name]').each( function()
            {
                params[this.name] = this.value;
            }
        );


        app.ajax(app.url+'dadosApoio/save', params, function(res)
        {
            node.setTitle(node.title);
            $(node.span).removeClass("pending");
            //node.parent.load(true);
            if( res.data.id)
            {
                if( node.key < 0 )
                {
                    node.key = res.data.id;
                    if( res.data.codigo )
                    {
                        $(node.tr).find('input[name=codigo]').val(res.data.codigo);
                    }
                }
            }
        }, null, 'json');
    }

    var nodeDelete=function( node )
    {

        app.confirm('Confirma Exclusão do Item <b>'+node.title+'</b>?',function(d,data)
            {
                params={id:node.key};
                app.ajax(app.url+'dadosApoio/delete', params, function(res)
                {
                    if( res.status == 0 )
                    {
                       node.remove();
                    }
                }, 'Excluindo...', 'json');
            }
        );

    }





});
