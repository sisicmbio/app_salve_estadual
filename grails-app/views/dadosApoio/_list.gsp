<table class="table table-hover table-striped table-condensed table-bordered">
    <thead>
        <th>#</th>
        <th>Descrição</th>
        <th>Código</th>
        <th>Ação</th>
    </thead>
    <tbody>
        <g:each in="${data}" var="item" status="i">
            <tr>
                <td class="text-center">${i + 1}</td>
                <td>${item?.descricao}</td>
                <td>${item?.codigo}</td>
                <td class="text-center">
                    <button type="button" data-id="${item?.id}" class="btn btn-default btn-xs btn-update" data-toggle="tooltip" data-placement="top" title="Alterar">
                        <span class="glyphicon glyphicon-pencil"></span>
                    </button>
                    <button type="button" data-id="${item?.id}" class="btn btn-default btn-xs btn-delete" data-toggle="tooltip" data-placement="top" title="Excluir">
                        <span class="glyphicon glyphicon-remove"></span>
                    </button>
                </td>
            </tr>
        </g:each>
    </tbody>
</table>

<center>
    <nav aria-label="Page navigation">
      <ul class="pagination pagination-sm">
        <g:if test="${pagination?.pageNumber && pagination?.pageNumber[0] > 1}">
            <li>
              <a href="#" class="pagination-previous" aria-label="Previous" value="${pagination?.pageCurrent - 1}">
                <span aria-hidden="true">&laquo;</span>
              </a>
            </li>
        </g:if>
        <g:each in="${pagination?.pageNumber}" var="page">
            <g:if test="${page == pagination?.pageCurrent}">
                <li class="active"><a href="#" id="pageCurrent">${page}</a></li>
                </g:if>
                <g:else>
                <li><a href="#">${page}</a></li>
                </g:else>
            </g:each>
        <g:if test="${pagination?.pageNumber && pagination?.pageNumber[pagination?.pageNumber.size() - 1]  != pagination?.pageCount}">
            <li>
              <a href="#" class="pagination-next" aria-label="Next" value="${pagination?.pageCurrent + 1}">
                <span aria-hidden="true">&raquo;</span>
              </a>
            </li>
        </g:if>
      </ul>
    </nav>
</center>
