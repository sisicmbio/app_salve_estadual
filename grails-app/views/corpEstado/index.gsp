<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="ajax"/>
    <style>
    .map {
        width: 100%;
        height: 600px;
        background: #e2ecef;
    }
    #sgEstado {
        text-transform: uppercase;
    }
    </style>

</head>
<body>
<div class="col-sm-12">
    <div class="panel panel-default w100p" id="pnlEstado">
        <div class="panel-heading">
            <span>
                Estados da Federação
            </span>
            <button id="btnClose" data-action="app.unloadModule" class="btn btn-default pull-right btn-xs" title="Fechar"><span class="glyphicon glyphicon-remove btnClose"></span></button>
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-sm-12 col-md-5">

                    <form id="frmEstado" class="row">

                        %{--CAMPO ID--}%
                        <input type="hidden" id="sqEstado" name="sqEstado" value="">


                        %{-- NOME --}%
                        <div class="form-group col-sm-12">
                            <label class="label-required" for="noEstado">Nome do Estado</label>
                            <input type="text" class="form-control" id="noEstado" name="noEstado" value="" maxlength="50">
                        </div>

%{--                        SIGLA--}%
                        <div class="form-group col-sm-12 col-md-2">
                            <label for="sgEstado" class="label-required">Sigla</label>
                            <input type="text" class="form-control fld50" id="sgEstado" name="sgEstado" value=""  data-mask="AA" maxlength="2">

                        </div>

                        %{-- REGIÃO --}%
                        <div class="form-group col-sm-12 col-md-10">
                            <label for="sqRegiao" class="label-required">Região</label>
                            <select class="form-control fld200" id="sqRegiao" name="sqRegiao">
                                <option value="">-- selecione --</option>
                                <g:each var="regiao" in="${regioes}">
                                    <option value="${regiao.id}">${regiao.noRegiao}</option>
                                </g:each>
                            </select>
                        </div>

                        <div class="form-group col-sm-12">
                            <label class="control-label">Shapefile do Estado</label>
                            <input name="fldShapefile" type="file" id="fldShapefile" class="file-loading"
                                   onchange="corpEstado.shapeFileChange(this)"
                                   accept="zip/*"
                                   data-show-preview="false"
                                   data-show-upload="false"
                                   data-show-caption="true"
                                   data-show-remove="true"
                                   data-max-file-count="1"
                                   data-allowed-file-extensions='["zip"]'
                                   data-main-class="input-group-sm"
                                   data-max-file-size="20000"
                                   data-browse-label="Arquivo..."
                                   data-preview-file-type="object">
                        </div>


                        %{--BOTOES--}%
                        <div class="panel-footer col-sm-12">
                            <button type="button" id="btnSave" onclick="corpEstado.save();" class="btn btn-success">Gravar</button>
                            <button type="button" class="btn btn-danger pull-right" onclick="corpEstado.reset();">Limpar</button>
                        </div>
                        <div class="col-sm-12"><small class="red fs10">* - campo obrigatório</small></div>
                    </form>

                    %{--                        GRIDE ABAIXO DO FORM--}%
                    <div class="row mt20">
                        <div class="col-sm-12">
                            <h4 id="numRecords">&nbsp;</h4>
                            <div style="width: auto;height: auto" id="gridContainer"></div>
                        </div>
                    </div>
                </div>
                %{--  MAPA --}%
                <div class="col-sm-12 col-md-7">
                    <div>
                        <h4>Polígono do Estado</h4>
                    </div>
                     <div id="mapShapeFile" style="width: 100%;height: 600px;background: #e2ecef;"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<asset:javascript src="corpGeo.js" defer="true"/>
</body>
</html>
