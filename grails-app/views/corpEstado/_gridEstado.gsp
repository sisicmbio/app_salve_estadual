<!-- view: /views/corpEstado/_gridEstado -->
<!-- controller:${params?.controller} -->
<!-- action: ${params?.action} -->
<table class="table table-striped table-responsive table-hover">
    <thead>
    <tr>
        <th style="width: auto">Nome</th>
        <th style="width:80px">Sigla</th>
        <th style="width:100px">Região</th>
        <th style="width: 100px;">Ações</th>
    </tr>
    </thead>
    <tbody>
    <g:each var="item" in="${estados}">
        <tr id="tr-${item.id}">

            %{-- NOME ESTADO --}%
            <td id="tdNoEstado" class="text-left">${item.noEstado}</td>

            %{-- SIGLA ESTADO --}%
            <td class="text-center" id="tdSgEstado" class="text-left">${item.sgEstado}</td>

            %{-- REGIAO --}%
            <td class="text-center" id="tdNoRegiao" class="text-left">${item?.regiao?.noRegiao}</td>

            %{-- ACOES --}%
            <td class="text-center">
                <i class="fa fa-edit fa-button cursor-pointer blue" title="Alterar" onClick="corpEstado.edit(${item.id})"></i>
                <i class="fa fa-remove fa-button cursor-pointer red" title="Excluir" onClick="corpEstado.delete(${item.id},'${item?.noEstado}')"></i>
            </td>
        </tr>
    </g:each>
    </tbody>
</table>