//# sourceURL=corpEstado.js
;var corpEstado = {
    map:null,
    init: function () {
        app.focus("noEstado");
        corpEstado.grid();
        corpEstado.showHideForm(); // inicializar o mapa
    },
    loadMap:function(){
        setTimeout(function(){
            corpEstado.map = corpGeo.initMap('mapShapeFile');
        },2000);
    },
    shapeFileChange:function(input){
        corpGeo.clearLayer( corpEstado.map,'layerShapefile' );
        corpGeo.loadShapefile(input.files[0], corpEstado.map,'layerShapefile',true);
    },
    reset:function(){
        $('form#frmEstado')[0].reset();
        $("#sqEstado").val(''); // reset não limpa campos hidden
        $("#fldShapefile").val('');
        corpGeo.clearLayer( corpEstado.map,'layerShapefile' );
        app.focus('noEstado');
    },
    save:function(){
        var data = {'sqEstado':$("#sqEstado").val()
            ,'noEstado':$("#noEstado").val()
            ,'sgEstado':$("#sgEstado").val()
            ,'sqRegiao':$("#sqRegiao").val()
            ,'wkt':corpGeo.generateLayerWKT(corpEstado.map,'layerShapefile')
        };
        var errors = [];
        if( ! data.noEstado ){
            errors.push('Nome do Estado deve ser informado')
        }
        if( ! data.sgEstado ){
            errors.push('Sigla do Estado deve ser informada')
        }
        if( ! data.sqRegiao ){
            errors.push('Região deve ser informada')
        }
        if( errors.length > 0 ){
            app.growl(errors.join('<br>'),'4','Erros encontrados','tc','','error');
            return;
        }
        app.ajax(baseUrl+'corpEstado/save',data,function(res){
            if( res.status == 0 ) {
                corpEstado.reset();
                corpEstado.grid( data );
            }
        },'Gravando...')
    },
    edit:function( sqEstado ) {
        if( sqEstado ) {
            app.ajax(baseUrl + 'corpEstado/edit', {sqEstado: sqEstado}, function (res) {
                if (res.status == 0) {
                    $("#sqEstado").val( res.data.sqEstado );
                    $("#noEstado").val( res.data.noEstado );
                    $("#sgEstado").val( res.data.sgEstado );
                    $("#sqRegiao").val( res.data.sqRegiao );
                    if( ! corpEstado.map ) {
                        corpEstado.loadMap();
                    }
                    corpGeo.showGeojson(corpEstado.map, 'layerShapefile', res.data.geojson ,true);
                    app.focus('noEstado');
                }
            },'Carregando...')
        }
    },
    delete:function( sqEstado, noEstado ) {
        if (sqEstado) {
            if ( app.confirm('Confirma a exclusão do Estado <b>' + noEstado + '</b>?', function () {
                app.ajax(baseUrl + 'corpEstado/delete', {sqEstado: sqEstado}, function (res) {
                    if (res.status == 0) {
                        corpEstado.reset()
                        $('#gridContainer table tbody tr#tr-' + sqEstado).remove();
                        corpEstado.updateNumRecords();
                    }
                }, 'Aguarde...')
            })
            ) ;
        }
    },
    grid:function(data){
        data = data || {};
        // atualizar somente a linha alterada no gride
        if( data.sqEstado && data.noEstado ){
            $('#gridContainer table tbody tr#tr-' + data.sqEstado + ' td#tdNoEstado').html(data.noEstado);
            $('#gridContainer table tbody tr#tr-' + data.sqEstado + ' td#tdSgEstado').html(data.sgEstado.toUpperCase());
            $('#gridContainer table tbody tr#tr-' + data.sqEstado + ' td#tdNoRegiao').html(data.noRegiao);
            return;
        }
        // recriar o gride
        $("#gridContainer").html('<div class="alert alert-info">Carregando tabela...'+grails.spinner+'</div>');
        app.ajax(baseUrl + 'corpEstado/grid', {}, function (res) {
            $("#gridContainer").html( res );
            corpEstado.updateNumRecords();
        },'')
    },
    updateNumRecords:function(){
        var numRecords = $("#gridContainer table tbody tr").size();
        $("#numRecords").html( String(numRecords ) + ( numRecords == 1 ? '&nbsp;Estado cadastrado' : '&nbsp;Estados cadastrados') );
    }
    /**
     * inicializar o mapa ao abrir o formulário
     * @param params
     */
    ,showHideForm:function(params){
        if( ! corpEstado.map ) {
            corpEstado.loadMap();
        }
    }
}
setTimeout(function(){corpEstado.init(),1000});
