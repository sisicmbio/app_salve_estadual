<!-- view: /views/gerenciarConsulta/_formConsolidar -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<form name="frmConsolidar" id="frmConsolidar"  class="form-inline w100p" role="form">

    <div id="divSelectConsultaConsolidar" style="display: none;" class="form-group mt5 w100p">
        <label class="control-label">Selecione:
            <label class="control-label cursor-pointer ml20 label-selected" style="padding-left:20px;"><input class="checkbox-lg" type="checkbox" checked="true" value="CD">&nbsp;Consulta direta</label>
            <label class="control-label cursor-pointer ml20 label-selected"><input class="checkbox-lg" type="checkbox" checked="true" value="CA">&nbsp;Consulta Ampla</label>
            <label class="control-label cursor-pointer ml20 label-selected"><input class="checkbox-lg" type="checkbox" checked="true" value="RE">&nbsp;Revisão</label>
            <label class="control-label ml20">&nbsp;|&nbsp;</label>
            <label class="control-label cursor-pointer ml20 label-selected"><input class="checkbox-lg" type="checkbox" checked="true" value="AB">&nbsp;Abertas</label>
            <label class="control-label cursor-pointer ml20 label-selected"><input class="checkbox-lg" type="checkbox" value="FE">&nbsp;Encerradas</label>
        </label>
        <br>
        <select id="sqConsultaConsolidar" disabled="true"
                name="sqConsultaConsolidar"
                data-change="gerenciarConsulta.sqConsultaConsolidarChange"
                data-s2-minimum-input-length="0"
                class="form-control fldSelect ignoreChange blue mt5 select-consulta select2 fld800">
            <option value="">-- selecione a consulta / revisão --</option>
            <g:each var="item" in="${listCicloConsulta}">
                <option data-situacao="${item.dtFim >= br.gov.icmbio.Util.hoje()  ? 'ABERTA':'FECHADA'}"
                        data-tipo="${item?.codigoSistema}"
                        value="${item.id}">${ item.dtInicio.format('dd/MM/yyyy')+ ' a ' + item.dtFim.format('dd/MM/yyyy')+' - '+item.deTituloConsulta}</option>
            </g:each>
        </select>
    </div>

    %{--Filtro da ficha--}%
    <div class="mt20" id="frmConsolidarFiltros">
         <g:render template="/templates/filtrosFicha" model="[callback:'gerenciarConsulta.updateGridConsolidar',listSituacaoFicha:listSituacaoFicha,outrosCampos:'/gerenciarConsulta/camposFiltrarFichas']"></g:render>
    </div>
</form>

%{-- gride fichas com as colaborações para consolidação pelo ponto focal--}%
<div id="containerGridConsolidar" class="mt10"
     data-params="sqCicloAvaliacao,sqConsultaConsolidar|sqCicloConsultaFiltro"
     data-container-filters-id="frmConsolidarFiltros"
     data-height="800px"
     data-field-id="sqFicha"
     data-url="gerenciarConsulta/getGridConsolidar">
    <g:render template="divGridConsolidar"
              model="[gridId: 'GridConsolidar']"></g:render>
</div>
