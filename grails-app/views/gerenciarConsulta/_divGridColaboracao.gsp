<!-- view: /views/gerenciarConsulta/_divGridColaboracao -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<div class="panel panel-default">
    <div class="panel-heading">
        <h4>Taxon:&nbsp;<b>${raw(ficha.taxon.noCientificoCompletoItalico)}</b></h4>
        <h5>Colaborador:&nbsp;<b>${usuario.noUsuario}</b></h5>
    </div>
    <div class="panel-body">
        <g:if test="${qtdColaboracaoOcorrencia > 0 }">
            <div class="alert alert-danger">
                <a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                <p>As colaborações referentes aos registros de ocorrência devem ser validadas/visualizadas clicando sobre o nome científico da tabela anterior.<p>
                <p><b>
                <g:if test="${qtdColaboracaoOcorrencia < 2 }">
                    Existe 1 colaboração de registro deste colaborador.
                </g:if>
                <g:else>
                    Existem ${qtdColaboracaoOcorrencia} colaborações de registro deste colaborador.
                </g:else>
                </b>
                </p>

            </div>
        </g:if>
        <table class="table table-sm table-striped table-condensed table-bordered table-colaboracao" id="grid-colaboracao-${params.idFichaConsulta}">
            <thead>
                <th style="width:50px;">Campo</th>
                <th style="width:150px;">Situação</th>
                <th style="width:400px;" id="th-${params.idFichaConsulta}-to">Texto Original</th>
                <th style="width:400px;" id="th-${params.idFichaConsulta}-tc">Texto Colaborador</th>
                <th style="width:120px;">Data</th>
            </thead>
            <tbody>
                <g:each var="item" in="${listFichaColaboracao}" status="i">
                    <g:if test="${item.dsColaboracao}">
                        <tr class="${ item.situacao.codigoSistema != 'NAO_AVALIADA'?'colaboracao-avaliada':'colaboracao-nao-avaliada' }">
                            <td class="text-center td-align-top">${ item.dsRotulo }
                                <g:if test="${item?.consultaFicha?.cicloConsultaFichaVersao}">
                                    <br><span>V.${item?.consultaFicha.cicloConsultaFichaVersao.fichaVersao.nuVersao}</span>
                                </g:if>

                            </td>
                            <td class="text-center td-align-top">
                                <g:if test="${ canModify && !item?.consultaFicha?.cicloConsultaFichaVersao }">
                                    <select id="selectColaboracao${item.id}" class="fldSelect fld150 form-control ${ item.situacao.codigoSistema != 'NAO_AVALIADA'?'colaboracao-avaliada':'colaboracao-nao-avaliada' }" data-params="id:${item.id}" data-contexto="consulta" data-fonte="consulta" data-change="updateSituacaoColaboracao">
                                    <g:each var="situacao" in="${listSituacaoColaboracao}">
                                        <option data-codigo="${situacao.codigoSistema}" value="${situacao.id}" ${ item?.situacao?.id==situacao?.id ? 'selected':''}>${situacao?.descricao}</option>
                                    </g:each>
                                    </select>
                                    <div id="divWrapperTextoNaoAceita${item.id}" style="max-width:200px;display:${item?.txNaoAceita?'block':'none'};">
                                        <div id="divTextoNaoAceita${item.id}" class="fldTextoLivre" style="font-size: 12px !important;">${item.txNaoAceita?:''}</div>
                                        <button type="button" data-action="editNaoAceita" data-id="${item.id}" class="btn btn-xs btn-default" title="Editar a justificativa." style="cursor: pointer;">Editar</button>
                                    </div>
                                </g:if>
                                <g:else>
                                    ${item?.situacao?.descricao}
                                    <div id="divWrapperTextoNaoAceita${item.id}" style="max-width:200px;display:${item?.txNaoAceita?'block':'none'};">
                                        <div id="divTextoNaoAceita${item.id}" class="fldTextoLivre" style="font-size: 12px !important;">${item.txNaoAceita?:''}</div>
                                    </div>
                                </g:else>
                            </td>
                            <g:if test="${item.noCampoFicha.toString() =~ /tx|ds/ && item.situacao.codigoSistema != 'NAO_AVALIDADA'}">
                                <td class="text-justify td-align-top" id="td-to-${params.idFichaConsulta+'-'+i}">
                                    <g:if test="${canModify && !item?.consultaFicha?.cicloConsultaFichaVersao}">
                                        <a href="javascript:void(0);" class="btn btn-xs btn-default" id="btn-${item.id}" data-target="div-colaboracao-${item.id}" onClick="exibirEditor(this)">Editar</a><br/>
                                    </g:if>
                                    <div class="td-texto" data-sq-ficha-colaboracao="${item.id}" style="border:none;overflow-x:hidden;min-width:300px;height:auto;overflow-y:auto;" id="div-colaboracao-${item.id}">${ raw( br.gov.icmbio.Util.removeBgWhite( item.valorOriginal) ) }</div>
                                </td>
                            </g:if>
                            <g:else>
                                <td class="text-justify td-align-top" id="td-to-${params.idFichaConsulta+'-'+i}">
                                    <div class="td-texto" data-sq-ficha-colaboracao="${item.id}" style="border:none;overflow-x:hidden;min-width:300px;min-height:200px;overflow-y:auto;" id="div_colaboracao_${item.id}">${ raw(  br.gov.icmbio.Util.removeBgWhite( item.valorOriginal ) ) }</div>
                                </td>
                            </g:else>
                            <td class="text-justify td-align-top" id="td-tc-${params.idFichaConsulta+'-'+i}">
                                <div class="td-texto">${raw( br.gov.icmbio.Util.removeBgWhite( item?.dsColaboracao ) + '<div style="color:#7a7a7a !important;">'+item?.dsRefBib+'</div>' )}</div>
                            </td>
                            <td class="text-center td-align-top">${ item?.dtAlteracao?.format('dd/MM/yyyy')}</td>
                        </tr>
                    </g:if>
                </g:each>
            </tbody>
        </table>
    </div>
</div>
