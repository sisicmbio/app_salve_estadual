<!-- view: /views/gerenciarConsulta/_divGridConvidadosConvidar -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<table class="table table-sm table-hover table-striped table-condensed table-bordered" id="tableConvidadosConvidar">
    <thead>
        <th style="width:30px;">#</th>
        <g:if test="${canModify}">
            <th style="width:30px;"><i title="Marcar/Desmarcar Todos" data-action="checkUncheckAll" class="glyphicon glyphicon-unchecked"></i></th>
        </g:if>
        <th style="width:200px">Convidado</th>
        <th style="width:200px">Email</th>
        <th style="width:150px">Instituição</th>
        <th style="width:auto">Ficha(s)</th>
        <th style="width:200px">Obs</th>
        <th style="width:100px;">Situação</th>
        <g:if test="${canModify}">
            <th style="width:20px;">Ação</th>
        </g:if>
    </thead>
    <tbody>
        <g:if test="${listCicloConsultaPessoas.size() > 0 }">
            <g:each var="item" in="${listCicloConsultaPessoas}" status="i">
                <tr>
                    <td class="text-center">${i+1}</td>

                    %{--CONVIDADO--}%
                    <g:if test="${canModify}">
                        <td class="text-center"><input id="sqCicloConsultaPessoa_${item.id}" value="${item.id}" type="checkbox" class="checkbox-lg ignoreChange"/></td>
                    </g:if>
                    <td>${item.pessoa.noPessoaCap}</td>

                    %{--EMAIL--}%
                    <td>${item.deEmail}</td>

                    %{--INSTITUIÇÃO--}%
                    <td class="text-center">${ raw( item.instituicao ? '<span title="' + item.instituicao.noInstituicao+'">' + (item.instituicao.sgInstituicao?:item.instituicao.noInstituicao) + '</span>': '' ) }</td>

                    %{--FICHAS--}%
                    <td>
                    <fieldset id="fieldSet-fichas-convidado-${i}">
                        <legend style="font-size:1.2em;">
                            <a data-toggle="collapse" href="#fichas-convidados-row-${i}" id="a-fichas-convidado-${i}"
                               data-row-num="${i}"
                               data-id="${item.id}"
                               data-can-modify="${canModify}"
                               data-action="gerenciarConsulta.showFichasConvidado"
                               class="tooltipstered" style="cursor: pointer; opacity: 1;" title="Clique para mostrar/esconder as fichas">
                                <span>${ item.fichas.size() }&nbsp;Fichas</span>
                            </a>
                        </legend>
                        <div id="fichas-convidados-row-${i}" class="panel-collapse collapse" role="tabpanel" style=""></div>
                    </fieldset>
                        %{--<fieldset id="fieldSet-${i}">
                            <legend style="font-size:1.2em;">
                                <a data-toggle="collapse" href="#fichas-convidados-row-${i+1}" id="a-${i}"
                                   class="tooltipstered" style="cursor: pointer; opacity: 1;" title="Clique para mostrar/esconder as fichas">
                                   <span>${ item.fichasHtml?.findAll("<br>").size()+1 }&nbsp;Fichas</span>
                                </a>
                            </legend>
                            <div id="fichas-convidados-row-${i+1}" class="panel-collapse collapse" role="tabpanel" style="">
                                ${raw(item.fichasHtml)}
                            </div>
                        </fieldset>--}%
                    </td>

                    %{--OBSERVACAO--}%
                    <td>${item.deObservacao}</td>

                    %{--SITUACAO--}%
                    <td class="text-center">
                        <g:if test="${canModify}">
                        <select class="form-control fldSelect fld160" data-id="${item.id}" onChange="gerenciarConsulta.changeSituacaoConvite(this)">
                                <g:each var="situacao" in="${listSituacoesConvite}">
                                    <option data-codigo="${situacao.codigoSistema}" value="${situacao.id}" ${situacao.id==item.situacao.id ? 'selected':''}>${situacao.descricao}</option>
                                </g:each>
                         </select>
                        </g:if>
                        <g:else>
                            <span>${item?.situacao?.descricao}</span>
                        </g:else>
                    </td>

                    %{--ACOES--}%
                    <g:if test="${canModify}">
                    <td class="td-actions">
                        <a data-action="gerenciarConsulta.editConvidado" data-id="${item.id}" class="btn btn-default btn-xs btn-update" title="Alterar">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </a>
                        <a data-action="gerenciarConsulta.deleteConvidado" data-descricao="${item.pessoa.noPessoa}" data-id="${item.id}" class="fld btn btn-default btn-xs btn-delete" title="Excluir">
                            <span class="glyphicon glyphicon-remove"></span>
                        </a>
                    </td>
                    </g:if>
                </tr>
            </g:each>
        </g:if>
        <g:else>
            <tr>
                <td colspan="7" align="center">
                    <h4>Nenhum convidado cadastrado!</h4>
                </td>
            </tr>
        </g:else>
    </tbody>
</table>
