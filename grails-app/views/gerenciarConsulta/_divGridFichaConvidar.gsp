<!-- view: /views/gerenciarConsulta/_divGridFichaConvidar -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<div style="display:block;overflow-y:auto;max-height:500px;padding:5px">
<table class="table table-hover table-striped table-condensed table-bordered" id="tbGerConsultaFichas">
    <thead>
    <th style="width:30px;"><i id="checkUncheckAll" title="Marcar/Desmarcar Todos" data-action="checkUncheckAll" class="glyphicon glyphicon-unchecked check-all"></i></th>
    <th style="width:*;">Nome Científico</th>
    </thead>
    <tbody>
    <g:if test="${listCicloConsultaFichas?.size() > 0}">
        <g:each var="item" in="${listCicloConsultaFichas}" status="i">
            <tr id="gdFichaConvidarRow_${i + 1}">
                <td class="text-center">
                    <input id="sqCicloConsultaFicha_${item.id}" value="${item.id}" type="checkbox" class="checkbox-lg ignoreChange"/>
                </td>
                <td>${raw(item.ficha.noCientificoItalico)}</td>
            </tr>
        </g:each>
    </g:if>
    <g:else>
        <tr>
            <td colspan="5" align="center">
                <h4>Nenhuma ficha selecionada para este ciclo!</h4>
            </td>
        </tr>
    </g:else>
    </tbody>
</table>
</div>
