<!-- view: /views/gerenciarConsulta/_divGridConsolidar -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<input type="hidden" id="canModify" value="${canModify}"/>
<table id="table${gridId}" class="table table-striped table-sorting">
    <thead>
    %{--    PAGINAÇÃO--}%
    <tr id="tr${gridId}Pagination">
        <td colspan="8">
            <div class="text-left" style="min-height:0;height: auto;overflow: hidden"
                 id="div${(gridId ?: pagination.gridId)}PaginationContent">
                <g:if test="${pagination?.totalRecords}">
                    <g:gridPagination pagination="${pagination}"/>
                </g:if>
            </div>
        </td>
    </tr>
    %{-- TITULOS DAS COLUNAS --}%
    <tr id="tr${gridId}Columns" data-grid-id="${gridId}">
        <th style="width:60px;">#</th>
        <th style="min-width:200px;width:auto;" class="sorting sorting_asc" data-field-name="nm_cientifico">Nome cientifico</th>
        <th style="width:200px;" class="sorting sorting_asc" data-field-name="ds_categoria_vigente">Categoria vigente</th>
        <th style="width:100px;" >Manter LC</th>
        <th style="width:400px;" >Colaboradorações</th>
        <th style="width:200px;" >Situação ficha</th>
    </tr>
    </thead>

    <tbody>
    <g:each var="row" in="${listRows}" status="i">
        <tr id="tr-${row.sq_ficha}">
            %{-- COLUNA NUMERAÇÃO --}%
            <td class="text-center" style="vertical-align: top !important;"
                id="td-${row.sq_ficha}">
                ${ i + (pagination ? pagination?.rowNum.toInteger() : 1 ) }
            </td>

            %{-- FICHA NÃO VERSIONADA --}%
            <g:if test="${ ! row.nu_versao }">

                %{-- NOME CIENTIFICO--}%
                <td  style="vertical-align: top !important;">
                    <g:if test="${row.in_colaboracao}">
                        ${raw('<a href="#" title="Clique para ver a ficha completa." onClick="showFichaCompleta(' + row.sq_ficha + ',\'' + br.gov.icmbio.Util.ncItalico(row.nm_cientifico,false) + '\',true,\'consulta\',\'gerenciarConsulta.confirmClose\',' + canModify + ')">' + br.gov.icmbio.Util.ncItalico( row.nm_cientifico,true ) + '</a>')}
                    </g:if>
                    <g:else>
                        ${raw( br.gov.icmbio.Util.ncItalico( row.nm_cientifico,true ) ) }
                    </g:else>
                </td>

                %{-- CATEGORIA--}%
                <td style="vertical-align: top !important;">
                    ${row.ds_categoria_vigente}
                </td>

                %{-- MANTER LC --}%
                <td class="text-center">
                <g:if test="${ row.cd_categoria_sistema == 'LC'}">
                    <g:if test="${canModify}">
                        <g:if test="${ row.cd_situacao_ficha_sistema ==~ /CONSOLIDADA|CONSULTA|CONSULTA_FINALIZADA|AVALIADA_EM_REVISAO/}">
                            <input type="checkbox" id="checkMaterLc${row.sq_ficha}" ${ row.st_manter_lc == true ? 'checked' : ''} onChange="gerenciarConsulta.changeManterLc(${row.sq_ficha},this)" class="checkbox-lg"/>
                        </g:if>
                        <g:else>
                            <input type="checkbox"  id="checkMaterLc${row.sq_ficha}" ${ row.st_manter_lc == true ? 'checked' : ''} onChange="gerenciarConsulta.changeManterLc(${row.sq_ficha},this)" disabled="disabled" class="checkbox-lg" title="Ficha não está em consulta ou consulta finalizada ou avaliada em revisão."/>
                        </g:else>
                    </g:if>
                    <g:else>
                        <g:if test="${ row.cd_categoria_sistema == 'LC' }">
                            ${ row.st_manter_lc ? 'Sim' : 'Não' }
                        </g:if>
                        <g:else>
                            &nbsp;
                        </g:else>
                    </g:else>
                </g:if>
                <g:else>
                    ${row.cd_categoria_sistema}
                    &nbsp;&nbsp;
                </g:else>
                </td>

                %{--  COLABORACOES--}%
                <td>
                   ${ raw( br.gov.icmbio.Util.formatarColaboracoes( row, false ) ) }
                </td>

                %{-- SITUACAO FICHA --}%
                <td style="vertical-align: top !important;">

                <g:if test="${canModify}">
                    <select class="form-control fldSelect fld170 ${row.cd_situacao_ficha_sistema == 'CONSOLIDADA' ? 'green' : ''}"
                            data-old-value="${ row.sq_situacao_ficha }"
                            data-sq-ficha="${row.sq_ficha}"
                            data-change="gerenciarConsulta.updateSituacaoFicha">
                        <g:each var="situacao" in="${listSituacaoFicha}">
                            <g:if test="${row.cd_situacao_ficha_sistema =~ /^(EM_VALIDACAO|VALIDADA|VALIDADA_EM_REVISAO|FINALIZADA)$/}">
                                <g:if test="${ situacao.id == row.sq_situacao_ficha || ( situacao.codigoSistema =~ /^(EM_VALIDACAO|VALIDADA|VALIDADA_EM_REVISAO|FINALIZADA)$/ )}">
                                    <option data-codigo="${situacao.codigoSistema}" value="${situacao.id}" ${situacao.id == row.sq_situacao_ficha ? 'selected':''}>${situacao.descricao}</option>
                                </g:if>
                            </g:if>
                            <g:else>
                                <g:if test="${situacao.id == row.sq_situacao_ficha || situacao.codigoSistema == 'CONSOLIDADA'|| situacao.codigoSistema == 'AVALIADA_REVISADA'}">
                                    <option data-codigo="${situacao.codigoSistema}" value="${situacao.id}" ${situacao.id == row.sq_situacao_ficha ? 'selected':''}>${situacao.descricao}</option>
                                </g:if>
                            </g:else>
                        </g:each>
                    </select>
                </g:if>
                <g:else>
                    ${row.ds_situacao_ficha}
                </g:else>
           </g:if>
            %{-- FICHA VERSIONADA --}%
            <g:else>
                %{-- NOME CIENTIFICO --}%
                <td  style="vertical-align: top !important;">
                    ${raw('<a href="#" title="Clique para ver a ficha completa." onClick="showFichaCompleta(' + row.sq_ficha + ',\'' + br.gov.icmbio.Util.ncItalico(row.nm_cientifico,false) + '\',true,\'consulta\',\'gerenciarConsulta.confirmClose\',false,null,null,'+(row?.sq_ficha_versao?:0) +')">' + br.gov.icmbio.Util.ncItalico( row.nm_cientifico,true ) + '</a>'+
                        '&nbsp;<a href="javascript:void(0);" title="Clique para ver a versão da ficha." onClick="showFichaVersao({sqCicloConsultaFichaVersao:' +
                        row.sq_ciclo_consulta_ficha_versao+'})">V.'+row.nu_versao+'</a>'
                    )}

%{--
                    ${ raw('<a href="javascript:void(0);" title="Clique para ver a versão da ficha." onClick="showFichaVersao({sqCicloConsultaFichaVersao:' +
                        row.sq_ciclo_consulta_ficha_versao+'})">' +
                        br.gov.icmbio.Util.ncItalico( row.nm_cientifico,true ) + '</a>&nbsp;<span>V.'+row.nu_versao+'</span>') }
--}%
                </td>

                %{-- CATEGORIA --}%
                <td style="vertical-align: top !important;">
                    ${row.ds_categoria_vigente}
                </td>

                %{-- MANTER LC --}%
                <td class="text-center">
                    <g:if test="${ row.cd_categoria_sistema == 'LC' }">
                        ${ row.st_manter_lc ? 'Sim' : 'Não' }
                    </g:if>
                    <g:else>
                        &nbsp;
                    </g:else>
                </td>

                %{--  COLABORACOES--}%
                <td>
                    ${ raw( br.gov.icmbio.Util.formatarColaboracoes( row, true ) ) }
                </td>


                %{-- SITUACAO DA FICHA --}%
                <td class="text-center" style="vertical-align: top !important;">
                    <span>Versionada</span>
                    %{--${row.ds_situacao_ficha}--}%
                </td>
        </g:else>
       </tr>
    </g:each>

    </tbody>

</table>
