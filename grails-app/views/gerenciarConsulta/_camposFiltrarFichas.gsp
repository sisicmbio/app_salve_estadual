<!-- view: /views/gerenciarConsulta/_camposFiltrarFicha.gsp -->
%{--
<div class="form-group">
<label for="sqCicloConsulta${rndId}" class="control-label">Selecionar Consulta</label>
<br>
<select id="sqCicloConsulta${rndId}" name="sqCicloConsultaFiltro" data-change="app.btnFiltroFichaClick" class="form-control fld300 fldSelect">
  <option value="">-- todas --</option>
  <g:each var="item" in="${listCicloConsulta}">
      <option value="${item.id}">${item.tipoConsulta.descricao+ ' - ' +item.periodoText+' - '+item.deTituloConsulta}</option>
  </g:each>
</select>
</div>--}%
<div class="form-group">
    <label for="noColaboradorFiltro${rndId}" class="control-label">Nome do Colaborador</label>
    <br>
    <input type="text" id="noColaboradorFiltro${rndId}" name="noColaboradorFiltro" class="form-control fld300"
           data-enter="app.btnFiltroFichaClick">
</div>

<div class="form-group">
    <label for="inColaboracaoFiltro${rndId}" class="control-label">Colaboração</label>
    <br>
    <select id="inColaboracaoFiltro${rndId}" name="inColaboracaoFiltro" class="form-control fld300 fldSelect">
        <option value="">-- todas --</option>
        <option value="s">Com colaboração</option>
        <option value="n">Sem colaboracao</option>
    </select>
</div>

%{--<div class="form-group">--}%
%{--<label for="inLcFiltro${rndId}" class="control-label">Somente LC</label>--}%
%{--<br>--}%
%{--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input name="inLcFiltro" id="inLcFiltro${rndId}" type="checkbox"  class="checkbox-lg" value="S" data-change="app.btnFiltroFichaClick" >--}%

%{--</div>--}%
