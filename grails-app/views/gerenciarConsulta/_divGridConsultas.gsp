<!-- view: /views/gerenciarConsulta/_divGridConsultas -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<input type="hidden" id="canModify" value="${canModify}"/>

<table id="table${gridId}" cellpadding="0" cellspacing="0" class="table table-striped table-sorting">
    <thead>
    %{--    PAGINAÇÃO--}%
    <tr id="tr${gridId}Pagination">
        <td colspan="8">
            <div class="text-left" style="min-height:0;height: auto;overflow: hidden"
                 id="div${(gridId ?: pagination.gridId)}PaginationContent">
                <g:if test="${pagination?.totalRecords}">
                    <g:gridPagination pagination="${pagination}"/>
                </g:if>
            </div>
        </td>
    </tr>

    %{-- COLUNAS COM OS CAMPOS PARA FILTRAGEM --}%
    <tr id="tr${gridId}Filters" class="table-filters" data-grid-id="${gridId}">
        %{-- coluna numeração de linha--}%
        <td>
            <div style="display:flex;flex-direction:row; justify-content:space-between;">
                <i class="fa fa-question-circle"
                   title="Utilize os campos ao lado para filtragem dos registros.<br> Informe o valor a ser localizado e pressione <span class='blue'>Enter</span> ou clique na imagem <i class='fa fa-filter'></i>.<br>Para remover o filtro limpe o campo e pressione <span class='blue'>Enter</span> novamente ou clique na imagem <i class='fa fa-eraser'></i>."></i>
                <i data-grid-id="${gridId}" class="fa fa-eraser" title="Limpar filtro(s)"></i>
                <i data-grid-id="${gridId}" class="fa fa-filter" title="Aplicar filtro(s) / Atualizar"></i>
            </div>
        </td>
        %{--filtro tipo--}%
        <td><select name="fldTipoConsula${gridId}" class="form-control table-filter" placeholder=""
                    data-field="sqTipoConsulta">
            <option value="">todos</option>
            <g:each var="item" in="${listTipoConsultaFicha}">
                <option data-codigo="${item.codigo}" value="${item.id}">${item.descricao}</option>
            </g:each>
            </select>
        </td>
        %{-- filtro situacao --}%
        <td><select name="fldSituacao${gridId}" class="form-control table-filter" placeholder=""
                    data-field="coSituacaoConsulta">
            <option value="">todas</option>
            <option data-codigo="ABERTA" value="ABERTA" ${ ( ! params.coSituacaoConsulta || params.coSituacaoConsulta == 'ABERTA' ? 'selected':'') }>Abertas</option>
            <option data-codigo="ENCERRADA" value="ENCERRADA" ${ (params.coSituacaoConsulta && params.coSituacaoConsulta=='ENCERRADA' ? 'selected':'') }>Encerradas</option>
        </select>
    </td>
        %{-- filtro titulo --}%
        <td><input type="text" name="fldDeTituloConsulta${gridId}" value="${params?.deTituloConsulta ?: ''}" class="form-control table-filter"
                   placeholder="título..." data-field="deTituloConsulta">
        </td>
        %{-- filtro periodo --}%
        <td></td>
        %{-- filtro unidade --}%
        <td><input type="text" name="fldDeTituloConsulta${gridId}" value="${params?.sgUnidadeOrg ?: ''}" class="form-control table-filter"
                   placeholder="unidade..." data-field="sgUnidadeOrg" data-enter="app.grid('${gridId}');">
        </td>
        %{-- filtro fichas --}%
        <td><input type="text" name="fldNoCientifico${gridId}" value="${params?.noCientifico ?: ''}" class="form-control table-filter"
               placeholder="nome cientifico..." data-field="noCientifico" data-enter="app.grid('${gridId}');">
        </td>
        %{-- ação --}%
        <td></td>
    </tr>

    %{-- COLUNAS COM OS TÍTULOS --}%
    <tr id="tr${gridId}Columns" data-grid-id="${gridId}">
        <th style="width:60px;">#</th>
        %{-- coluna tipo--}%
        <th style="width:100px;" class="sorting" data-field-name="ds_tipo_consulta">Tipo</th>
        %{-- coluna situção--}%
        <th style="width:130px;" class="sorting" data-field-name="st_aberta">Situação</th>
        %{--        coluna titulo--}%
        <th style="width:350px;">Título</th>
        %{--        coluna periodo--}%
        <th style="width:100px;" class="sorting sorting_desc" data-field-name="dt_inicio">Período</th>
        %{--        coluna Unidade--}%
        <th style="width:180px;" class="sorting" data-field-name="sg_unidade_org">Unidade</th>
        %{--        coluna fichas--}%
        <th style="width:300px;" class="sorting" data-field-name="nu_fichas">Fichas</th>
        <g:if test="${canModify}">
            <th style="width:80px;">Ação</th>
        </g:if>
    </tr>
    </thead>
    <tbody>
    <g:if test="${listConsultas}">
        <g:each var="item" in="${listConsultas}" status="i">
            <tr id="tr-${item.sq_ciclo_consulta}" class="${item.st_aberta ? 'black':'gray'}">
                <td style="width:60px;min-width:60px;" class="text-center">
                 <span>${raw(  i + ( pagination?.rowNum ?: 1 ) ) }</span>
                </td>
                <td class="text-center">${item.ds_tipo_consulta}</td>
                <td class="text-center">${ item.st_aberta ? 'Aberta' : 'Encerrada '}</td>
                <td class="text-center">${raw( item.de_titulo_consulta+(item.sg_consulta ? '<br><span title="Nome módulo consulta ampla/direta" class="green">('+item.sg_consulta+')</span>':'') )}</td>
                <td class="text-center">${raw(item.dt_inicio.format('dd/MM/yyyy')+ ' a<br>'+item.dt_fim.format('dd/MM/yyyy') )}</td>
                <td class="text-center">${item.sg_unidade_org}</td>
                <td>
                    <fieldset id="fieldSet-${i}">
                        <legend style="font-size:1.2em;">
                            <a data-toggle="collapse" href="#divFichas-${i}" id="a-${i}"
                               data-row-num="${i}"
                               data-id="${item.sq_ciclo_consulta}"
                               data-can-modify="${canModify?'S':'N'}"
                               data-action="gerenciarConsulta.linkToggleFichasClick"
                               class="tooltipstered" style="cursor: pointer; opacity: 1;" title="Clique para mostrar/esconder as fichas">
                                <span>${ item.nu_fichas }&nbsp;Fichas</span>
                            </a>
                        </legend>
                        <div id="divFichas-${i}" class="panel-collapse collapse" role="tabpanel" style=""></div>
                    </fieldset>
                </td>
                <g:if test="${canModify}">
                    <td class="td-actions td-align-bottom">
                        <button type="button" data-action="gerenciarConsulta.editConsulta" data-sq-ciclo-consulta="${item.sq_ciclo_consulta}" class="btn btn-default btn-xs btn-update" data-toggle="tooltip" data-placement="top" title="Alterar">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </button>
                        <button type="button" data-action="gerenciarConsulta.deleteConsulta" data-row="${i}" data-sq-ciclo-consulta="${item.sq_ciclo_consulta}" class="btn btn-default btn-xs btn-delete"
                                data-descricao="${item.de_titulo_consulta}"
                                data-toggle="tooltip" data-placement="top" title="Excluir">
                            <span class="glyphicon glyphicon-remove"></span>
                        </button>
                    </td>
                </g:if>
            </tr>
        </g:each>
    </g:if>
    <g:else>
        <tr>
            <td colspan="8" class="text-center">
                <h4>Nenhuma consulta encontrada</h4>
            </td>
        </tr>
    </g:else>
    </tbody>
</table>
