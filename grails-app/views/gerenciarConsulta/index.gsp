<%-- este arquivo contem a estrutura de abas para o módulo de gerenciamento das consultas amplas e diretas --%>
<div id="gerenciarConsultaContainer">
   <div class="row">
      <div class="col-sm-12">
         <div class="panel panel-default">
            <div class="panel-heading">
               <span>Módulo Consulta / Revisão</span>
               <button id="btnClose" data-action="app.unloadModule" class="btn btn-default pull-right btn-xs" title="Fechar">
               <span class="glyphicon glyphicon-remove btnClose"></span>
               </button>
            </div>
            <div class="panel-body" id="fichaContainerBody">
               <form id="frmGerConHist" class="form-inline" role="form">
                   <input type="hidden" id="sqCicloAvaliacao" name="sqCicloAvaliacao" value="${listCiclosAvaliacao[0]?.id}"/>
               </form>
               <br>
               <div id="divTabs" class="hidden">
                  <ul class="nav nav-tabs" id="ulTabsGerenciarConsulta">

                    <li id="liTabCadastro"     class="active"><a data-action="gerenciarConsulta.tabClick" xdata-url="cadastro" data-container="tabCadastro" data-on-load="" data-reload="true" data-params="" data-toggle="tab" href="#tabCadastro">Consultas/Revisões</a></li>
                    <li id="liTabConvidar"     class=""><a data-action="gerenciarConsulta.tabClick" data-url="gerenciarConsulta/convidar"  data-container="tabConvidar" data-on-load="gerenciarConsulta.tabConvidarInit" data-reload="true" data-params="sqCicloAvaliacao" data-toggle="tab" href="#tabConvidar">Convidados</a></li>
                    <g:if test="${session.sicae.user.canConsolidarConsulta() }">
                      <li id="liTabConsolidacao" class=""><a data-action="gerenciarConsulta.tabClick" data-url="gerenciarConsulta/consolidar" data-container="tabConsolidar" data-params="sqCicloAvaliacao" data-on-load="gerenciarConsulta.abaConsolidarInit" data-reload="false" data-toggle="tab" href="#tabConsolidar">Consolidar</a></li>
                    </g:if>
                    %{-- <li id="liTabHistorico"    class=""><a data-toggle="tab" href="#tabHistorico">Histórico</a></li> --}%

                  </ul>
                  %{-- Container de todas abas --}%
                  <div id="tabContent" class="tab-content">
                     <div role="tabpanel" class="tab-pane tab-pane-ficha active" id="tabCadastro">
                        <g:render template="formCadConsulta"></g:render>
                     </div>

                     <div role="tabpanel" class="tab-pane tab-pane-ficha" id="tabConvidar">
                         %{-- <g:render template="formConvidar"></g:render>--}%
                      </div>

                      <div role="tabpanel" class="tab-pane tab-pane-ficha" id="tabConsolidar">
                          %{-- <g:render template="divGridConsolidar"></g:render> --}%
                      </div>

                      <div role="tabpanel" class="tab-pane tab-pane-ficha" id="tabHistorico">
                          %{-- <g:render template="divGridHistorico"></g:render> --}%
                       </div>
                    </div>
                 %{-- fim container de todas abas --}%
               </div>
            %{-- fim divTabs --}%
            </div>
         %{-- fim panel body --}%
      </div>
      %{-- fim panel --}%
   </div>
   %{-- fim row 12 --}%
</div>
%{-- fim container --}%

%{--div auxiliar para exibir popup no mapa ao clica-lo --}%
<div class="point-popup" id="pointPopup" style="display:none;">
    <h2>DivPopup</h2>
</div>
