<!-- view: /views/gerenciarConsulta/_formCadConsulta -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<g:if test="${session.sicae.user.canAddConsulta()}">
    <fieldset class="mt10" id="frmGerConsultaContainer">
        <legend style="font-size:1.2em;">
            <a  id="btnShowForm"
                data-action="gerenciarConsulta.btnShowFormClick"
                data-target="acordeon_frmGerConsulta"
                data-focus="noOficina"
                title="Mostrar/Esconder formulário!">
                <i class="fa fa-plus green"/>&nbsp;Cadastrar</a>
        </legend>
%{--        <div class="fld panel-footer">
            <a id="btnShowForm" data-action="gerenciarConsulta.btnShowFormClick"
               title="Mostrar/Esconder formulário de cadastro."
               class="btn btn-default btn-sm"><i class="fa fa-plus">&nbsp;Formulário</i></a>
        </div>--}%
        <div id="acordeon_frmGerConsulta" class="panel-collapse Xcollapse" role="tabpanel" style="background-color:#ffffff;padding:5px;padding-top:0px;">
            <form id="frmGerConsulta" name="frmGerConsulta" class="form-inline hidden" role="form">
                <input type="hidden" name="sqCicloConsulta" id="sqCicloConsulta" value="">

                <div class="fld form-group">
                    <label for="sqTipoConsulta" class="control-label">Tipos</label>
                    <br>
                    <select name="sqTipoConsulta" id="sqTipoConsulta" class="fld form-control fld200 fldSelect" required="true" data-change="gerenciarConsulta.sqTipoConsultaChange">
%{--                        <option value="" selected="true">?</option>--}%
                        <g:each var="item" in="${listTipoConsultaFicha}" status="i">
                            <option data-codigo="${item.codigoSistema}" ${i==0?'selected':''} value="${item.id}">${item.descricao}</option>
                        </g:each>
                    </select>
                </div>

                <div class="fld form-group">
                    <label  class="control-label">Título</label>
                    <br>
                    <input name="deTituloConsulta" id="deTituloConsulta" type="text" value="" required="true" class="fld form-control fld400">
                </div>
                <div class="fld form-group">
                    <label for="sgConsulta" class="control-label" title="Nome que será exibido nas consultas ampas/diretas como link para filtragem das espécies.">Nome módulo consulta</label>
                    <br>
                    <input name="sgConsulta" id="sgConsulta" type="text" value="" required="true" class="fld form-control fld400">
                </div>
                <br>

                <div class="fld form-group">
                    <label for="dtInicio" class="control-label">Data Inicial</label>
                    <br>
                    <input name="dtInicio" id="dtInicio"  type="text" value="" required="true" class="fld form-control fld100 date">
                </div>

                <div class="fld form-group">
                    <label for="dtFim" class="control-label">Data Final</label>
                    <br>
                    <input name="dtFim" id="dtFim" type="text" value="" required="true" class="fld form-control fld100 date">
                </div>

                %{--Filtro da ficha--}%
                <div class="hidden mt10" id="divFiltrosFicha"></div>

                %{-- gride para selecionar ficha --}%
                <div id="divGridFichas" style="max-height: 500px;overflow-y:auto;"></div>

                %{-- botão --}%
                <div class="fld panel-footer">
                    <button data-action="gerenciarConsulta.saveConsulta" data-params="sqCicloAvaliacao" class="fld btn btn-primary">Gravar</button>
                    <button data-action="gerenciarConsulta.resetFrmConsulta" class="fld btn btn-danger">Limpar</button>
                </div>
            </form>
        </div>
    </fieldset>
</g:if>


%{--campo para localizar a consulta que tem determinada espécie--}%
%{--
<div class="fld400">
    <div class="form-group">
        <label>Localizar consulta/revisão da espécie</label>
        <br>
        <div class="input-group input-group">
            <input type="text" class="form-control ignoreChange input-bold-blue"
                   data-enter="gerenciarConsulta.updateGridConsultas"
                   id="fldNoCientificoFiltrarConsultaAbaCadastro" style="display:inline">
            <span class="input-group-addon">
                <i class="fa fa-search cursor-pointer" title="Localizar..." data-search="true" data-action="gerenciarConsulta.updateGridConsultas"></i>
            </span>
            <span class="input-group-addon">
                <i class="fa fa-trash cursor-pointer" title="Limpar o campo" data-action="gerenciarConsulta.resetFiltroConsultaAbaCadastro"></i>
            </span>
        </div>
    </div>
</div>
--}%

%{-- gride das consultas cadastradas --}%
<div id="containerDivGridConsultas"
     data-url="gerenciarConsulta/getGridConsultas"
     data-params="sqCicloAvaliacao,fldNoCientificoFiltrarConsultaAbaCadastro|noCientificoFiltrarConsulta">
    <g:render template="divGridConsultas" model="[gridId:'DivGridConsultas',canModify:true,listTipoConsultaFicha:listTipoConsultaFicha ]"></g:render>
</div>
<div class="end-page">&nbsp;</div>
