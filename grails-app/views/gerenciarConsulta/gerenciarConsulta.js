//# sourceURL=/views/gerenciarConsulta/gerenciarConsulta.js
//

var gerenciarConsulta = {
    filtrosAbaConsulta: {},
    filtrosAbaConvidados: {},
    filtrosAbaConsolidar: {},
    gridAbaConsolidar:null,
    init: function() {
        destroyEditors();
        $('#acordeon_frmGerConsulta').on('show.bs.collapse', function() {
            if ($("#divGridFichas table").length == 0) {
                //gerenciarConsulta.updateGridFichas();
            }
        });
        // iniciar a tela
        gerenciarConsulta.sqCicloAvaliacaoChange()
    },
    tabClick: function(aba, evt) {
        if (aba.container != 'tabCadastro') {
            $("#frmGerConHist #sqCicloAvaliacao").prop('disabled', true)
        } else {
            $("#frmGerConHist #sqCicloAvaliacao").prop('disabled', false)
        }
        return true;
    },
    btnUtilizarNaoUtilizarClick:function(params,ele,evt){
        var data = {};
        if ( params.inUtilizadoAvaliacao == 'N' && !params.txJustificativa) {
            data.fonte = params.fonte;
            data.idOcorrencia = params.idOcorrencia;
            data.id = params.id;
            data.contexto = params.contexto;
            data.sqFicha = params.sqFicha;
            data.inUtilizadoAvaliacao = params.inUtilizadoAvaliacao;
            app.ajax( baseUrl+'fichaCompleta/getJustificativasColaboracoes',data,function(res){
                if( res.status == 0 ) {
                    data.txJustificativa = res.txJustificativas;
                    data.sqMotivoNaoUtilizado = res.sqMotivo;
                    getJustificativa(data, gerenciarConsulta.btnUtilizarNaoUtilizarClick)
                }
            },'Lendo justificativas...','json');
            return;
        }
        data = app.params2data( params, data );
        data.action             = ( params.utilizarAvaliacao == 'S' ? 'utilizarNaAvaliacao': 'naoUtilizarNaAvaliacao');
        data.sn                 = params.inUtilizadoAvaliacao;
        data.ids                = [{id:params.idOcorrencia,bd:params.fonte}];
        //data.txJustificativa    = params.txJustificativa ||'';
        //data.sqMotivoNaoUtilizado = params.sqMotivoNaoUtilizado || '';
        //data.inUtilizadoAvaliacao = params.inUtilizadoAvaliacao;

        if (dlgJustificativa && dlgJustificativa.isOpened()) {
            dlgJustificativa.close();
        }
        ajaxJson( baseUrl + 'fichaCompleta/saveInUtilizadoAvaliacao', data,'Gravando...', function( res ) {

            if ( res.status == 0 ) {
                app.growl('Dados gravados com SUCESSO!', 2, '', 'tc', '', 'success');
                // CODIGO SALVE SEM CICLO
                var $btn = $("#btnChangeUtilizadoAvaliacao"+data.id);
                var $divParent = $btn.parent();
                $divParent.find('span[id^=wrapperSugestao]').map(function(index,span){
                    $(span).removeClass('colaboracao-nao-avaliada').addClass('colaboracao-avaliada');
                    $(span).find('input[type=radio][data-value=ACEITA]').prop('checked',true);
                });
                if( typeof( updateAndamentoAvaliacaoFichaCompleta ) == 'function'){
                    updateAndamentoAvaliacaoFichaCompleta();
                }
                // alterar texto e label do botão
                var $pUtilizado = $("p#pUtilizado"+data.id);
                if( data.sn=='S') {
                    $btn.html('Não utilizar na avaliação');
                    $btn.data('inUtilizadoAvaliacao','N')
                    $pUtilizado.html('Ponto utilizado na avaliação')
                } else {
                    $btn.html('Utilizar na avaliação');
                    $btn.data('inUtilizadoAvaliacao','S')
                    $pUtilizado.html('Ponto NÃO utilizado na avaliação')
                }
                if( typeof oMap == 'object' && typeof oMap.reloadLayers == 'function' ) {
                    try{ oMap.reloadLayers(); } catch( e ) {}
                }
                params={};
            };
        });
    },

    btnShowFormClick: function(param) {
        if ($("#frmGerConsulta").hasClass('hidden')) {
            $("#frmGerConsultaContainer #btnShowForm i").removeClass('fa-plus').addClass('fa-minus');
            $("#frmGerConsulta").removeClass('hidden');
            app.focus('noOficina','frmGerConsulta');
        } else {
            $("#frmGerConsultaContainer #btnShowForm i").removeClass('fa-minus').addClass('fa-plus');
            $("#frmGerConsulta").addClass('hidden');
        }
    },
    sqTipoConsultaChange: function(params) {
        if (!$("#frmGerConsulta #dtInicio").val()) {
            $("#frmGerConsulta #dtInicio").val(today());
            $("#frmGerConsulta #dtFim").val(today(90));
        }
        app.focus('deTituloConsulta', "frmGerConsulta");
        gerenciarConsulta.updateGridFichas();

        /*
        // exibir todas as linhas
        $("#tbGerConsultaFichas tr").show();

        // quando o tipo for Revisao Pos-oficina filtrar as fichas avaliadas
        if( $("#frmGerConsulta #sqTipoConsulta option:selected").data('codigo')=='REVISAO_POS_OFICINA' )
        {
            var $select = $("select[name^=sqSituacaoFiltro]");
            var value = $select.find("option[data-codigo=AVALIADA]").val();
            $select.prop('disabled',true);
            $select.val( value );
            gerenciarConsulta.updateGridFichas();
            //$("#divFiltrosFicha a[id^=btnFiltroFicha]:first").click();// simular clique no botão filtrar fichas
        }
        else
        {
            $("select[name^=sqSituacaoFiltro]").prop('disabled',false)
        }*/
    },

    sqCicloAvaliacaoChange: function(params) {

        var sqCicloAvaliacao = $("#gerenciarConsultaContainer #sqCicloAvaliacao").val();

        // gravar o ciclo selecionado no localstore
        app.saveField('sqCicloAvaliacao');

        // limpar filtros das fichas
        gerenciarConsulta.filtrosAbaConsulta = {};
        gerenciarConsulta.filtrosAbaConvidados = {};
        gerenciarConsulta.filtrosAbaConsolidar = {};

        // limpar gride
        $("#frmGerConsultaContainer #divGridFichas").html('');

        if (sqCicloAvaliacao) {
            $("#gerenciarConsultaContainer #divTabs,#divFiltrosFicha").removeClass('hidden');
            var data = {
                    sqCicloAvaliacao: sqCicloAvaliacao
                    ,required:true // tem que selecionar pelo menos um filtro
                    ,callback: 'gerenciarConsulta.updateGridFichas'
                    ,contexto: 'consulta'
                    ,hideFilters:'categoriaAvaliacao' // nesta fase as fichas não possuem a aba Avaliação preenchida
                };
                // carregar o formulario de filtros da ficha e o gride das fichas
            app.loadModule(baseUrl + 'ficha/getFormFiltroFicha', data, '#gerenciarConsultaContainer #divFiltrosFicha');
            $('#acordeon_frmGerConsulta').collapse('hide');
            gerenciarConsulta.updateGridConsultas();
        } else {
            $("#gerenciarConsultaContainer #divTabs,#divFiltrosFicha").addClass('hidden');
        }
    },

    updateGridFichas: function(filtros) {
        filtros = filtros || app.hasActiveFilter('divFiltrosFicha');
        if ( filtros ) {
            gerenciarConsulta.filtrosAbaConsulta = filtros;
        }

        if( ! filtros.count)
        {
            if( $("#divFiltrosFicha div[id^=divFiltrosFicha]").hasClass('in') ) {
                app.alertInfo('Selecione pelo menos um filtro para as fichas!')
            }
            return;
        }

        var data = gerenciarConsulta.filtrosAbaConsulta || {};
        var activeTab = $("ul#ulTabsGerenciarConsulta li.active")[0].id;
        var cicloAvaliacao = $("#gerenciarConsultaContainer #sqCicloAvaliacao").val();
        if (activeTab == 'liTabCadastro') {
            if (!cicloAvaliacao) {
                return;
            }
            data.sqCicloAvaliacao = cicloAvaliacao;
            data.sqTipoConsulta   = $("#sqTipoConsulta").val();
            $("#gerenciarConsultaContainer #divGridFichas").html('<h3>Carregando. '+window.grails.spinner+'</h3>');
            app.ajax(app.url + 'gerenciarConsulta/getGridFichas', data,
                function(res) {
                    $("#divGridFichas").html(res);
                    // ativar seleção de linhas com shift+click
                    $("#divGridFichas").shiftSelectable();
                }, null, 'html');
        }
    },
    resetFiltroConsultaAbaCadastro : function( params ){
        $("#fldNoCientificoFiltrarConsultaAbaCadastro").val('');
        gerenciarConsulta.updateGridConsultas(params);
    },

    updateGridConsultas: function(params,evt) {
        app.grid('DivGridConsultas');
        /*
        if (!$("#sqCicloAvaliacao").val()) {
            $("#divGridCadConsultas").html('');
            return;
        }
        var data = {
            sqCicloAvaliacao: $("#sqCicloAvaliacao").val()
        };
        data.noCientificoFiltrarConsulta = $("#fldNoCientificoFiltrarConsultaAbaCadastro").val() || ''
        if( params && params.search && !data.noCientificoFiltrarConsulta ) {
            return;
        }

        $("#gerenciarConsultaContainer #divGridConsultas").html('Carregando...');
        app.ajax(app.url + 'gerenciarConsulta/getGridConsultas', data,
            function(res) {
                $("#divGridConsultas").html(res);
                // exibir ou não o formulario de cadastro de consulta
                if ($("#gerenciarConsultaContainer #canModify").val() == 'false') {
                    $("#gerenciarConsultaContainer #frmGerConsultaContainer").hide();
                } else {
                    $("#gerenciarConsultaContainer #frmGerConsultaContainer").show();
                }
                // aplicar plugin datatable no gride
                $('#tableDivGridConsultas').DataTable( $.extend({},default_data_tables_options,
                    {
                        "order": [ 4, 'desc' ], // ordem inicial pelo período descendente
                        "columnDefs" : [
                            {"orderable": false, "targets": [0,6,7]}
                        ],"buttons": []
                    })
                );

            });*/
    },

    saveConsulta: function(params) {
        var sqCicloConsulta = $("#sqCicloConsulta").val();

        // verificar se tem alguma ficha selecionada
        if ( ! sqCicloConsulta && $("#divGridFichas input:checkbox:checked").size() == 0) {
            app.alertInfo('Nenhuma ficha selecionada!');
            return;
        }

        if ( ! $('#frmGerConsulta').valid()) {
            return;
        }

        var data = $("#frmGerConsulta").serializeArray();
        data = app.params2data(params, data, 'frmGerConsulta');
        data.ids = []
        $("#divGridFichas input:checkbox:checked").each(function() {
            data.ids.push(this.value);
        });
        data.ids = data.ids.join(',');
        app.ajax(app.url + 'gerenciarConsulta/saveConsulta', data,
            function(res) {
                if (res.status === 0) {
                    $("#divFiltrosFicha div[id^=divFiltrosFicha]").removeClass('in');
                    gerenciarConsulta.updateGridConsultas();
                    //gerenciarConsulta.updateGridFichas();
                    // se for uma inclusão limpar o campos da consulta
                    if( ! data.sqCicloConsulta ) {
                        app.reset('frmGerConsulta');
                    }
                    //$("#divFiltrosFicha a[id^=btnFiltroFicha]:first").click();// simular clique no botão filtrar fichas
                }
                if (res.errors.length > 0) {
                    app.alertError('<h3>Problemas encontrados:</h3><br >' + res.errors.join('<br />'));
                }
            },'Gravando consulta. Aguarde...',null,null,null,function(xhr){
                if( String( xhr.status ) == '502' ) {
                    app.reset('frmGerConsulta');
                    app.alertInfo('A gravação da consulta está em andamento e será concluída em alguns instantes.<br><br>Clique no botão Fechar para continuar a utilizar o SALVE.','Aviso',null,null,30);
                }
            });
    },
    editConsulta: function(params, btn) {
        if (!params.sqCicloConsulta) {
            app.alert('Id do ciclo não encontrado!');
            return;
        }

        /*if ($("#divGridFichas table").length == 0) {
            //gerenciarConsulta.updateGridFichas();
            return;
        }
        */
        var data = {}
        app.params2data(params, data);
        app.ajax(app.url + 'gerenciarConsulta/editConsulta', data,
            function(res) {
                if (res.fichas) {
                    app.setFormFields(res);
                    $("#frmGerConsulta").valid(); // remover bordas vermelhas
                    if (res.fichas.length) {
                        app.selectGridRow(btn);
                        // desmarcar todos os checkbox do grid fichas
                        $("#divGridFichas input:checkbox:checked").each(function() {
                            $(this).prop('checked', false);
                        });
                        $("#frmGerConsulta #checkUncheckAll").removeClass('glyphicon-check');
                        $.each(res.fichas, function(i, id) {
                            $("#sqFicha_" + id).prop('checked', true);
                        });
                        contarChecked({container:'divTbGerConsultaFichas'});

                    }
                    // expander o formulário
                    if ($("#frmGerConsulta").hasClass('hidden')) {
                        gerenciarConsulta.btnShowFormClick();
                    }
                    gerenciarConsulta.sqTipoConsultaChange();
                    $(document).scrollTop(0);
                    app.focus('deTituloConsulta','frmGerConsulta');
                }
            });
    },

    deleteConsulta: function(params, btn) {
        app.selectGridRow(btn);
        app.confirm('Confirma exclusão da consulta/revisão?<br><b>'+params.descricao+'</b>',
            function () {
                app.confirm('<br>A consulta/revisão <b>'+params.descricao+'</b> será excluída completamente.<br><br><span class="red bold">Tem certeza?</span>',
                    function () {
                        app.ajax(app.url + 'gerenciarConsulta/deleteConsulta',
                            params,
                            function (res) {
                                if (res.status == 0) {
                                    //var $tr = $("#tableDivGridConsultas #tr-" + params.sqCicloConsulta )
                                    //$tr.remove();
                                    gerenciarConsulta.resetFrmConsulta();
                                    gerenciarConsulta.updateGridConsultas();
                                    gerenciarConsulta.updateGridFichas();
                                    //$('#tableDivGridConsultas').DataTable().row( $tr ).remove().draw()
                                } else {
                                    app.unselectGridRow(btn);
                                }
                            }, 'Excluindo a consulta. Aguarde...');
                    },
                    function () {
                        app.unselectGridRow(btn);
                    }, params, 'ATENÇÃO!', 'danger');
            },
            function () {
                app.unselectGridRow(btn);
            }, params, 'Confirmação', 'warning');
    },
    resetFrmConsulta: function(params) {
        app.reset('frmGerConsulta','sqTipoConsulta');
        app.focus('deTituloConsulta');
        app.unselectGridRow('#gerenciarConsultaContainer #divGridConsultas');
    },
    showColaboracoes: function(idFicha, idUsuario, readonly,sqFichaVersao) {
        if (typeof(event) != 'undefined') {
            event.preventDefault();
        }
        var data = {
            idFicha: idFicha,
            sqFichaVersao:sqFichaVersao,
            idUsuario: idUsuario,
            readonly:readonly
        };
        tinymce.editors = [];
        app.panel('pnlShowColaboracoes_' + idFicha, 'Colaborações', 'gerenciarConsulta/getGridColaboracoes', data, function() {
            //editorOptions.selector = '.inlineEdit';
            //tinymce.init(editorOptions);
            // ajustar as colunas Texto Original e Texto Colaborador com a mesma largura
            window.setTimeout(function() {
                var tdTo = $("#td-to-" + idFicha + '-0'); // Texto Original
                var tdTc = $("#td-tc-" + idFicha + '-0'); // Texto Colaborador
                if (tdTo.size() > 0 && tdTo.size() > 0) {
                    var totalW = (tdTo.width() + tdTc.width()) / 2;
                    if (!isNaN(totalW)) {
                        tdTo.css('width', totalW);
                        tdTc.css('width', totalW);
                    }
                };
            }, 1000)
        }, null, null, null, true, function() {
            gerenciarConsulta.updateGridConsolidar(null,idFicha);
        }).setTheme('#0000ff');;
    },
    changeManterLc: function(id,checkbox) {
        if (!id) {
            return;
        }
        var data = {
             sqFicha: id
            ,stManterLc : ( checkbox.checked ? 'S' : 'N' )
        }
        app.ajax(app.url + 'gerenciarConsulta/changeManterLc', data,
            function(res) {
                if (res) {
                    app.alertInfo(res);
                    checkbox.checked = ! checkbox.checked;
                }
            });
    },
    /**
     * atualizar situacao da planilha de ocorrencias enviada na consulta
     * @param params
     * @param fld
     * @param evt
     */
    updateSituacaoColaboracaoPlanilha:function( params,fld,evt) {
        var $checkbox = $(evt.target);
        var data = $checkbox.data();
        var $link = $("#link-planilha-"+params.sqFichaConsultaAnexo);
        var data = {sqFichaConsultaAnexo:params.sqFichaConsultaAnexo
                    ,stRecebido:(evt.target.checked?'S':'N')
        }

        app.ajax(app.url + 'gerenciarConsulta/changeSituacaoColaboracaoPlanilha', data,
            function( res ) {
                if (res.status == 0) {
                    if (data.stRecebido == 'S') {
                        $link.removeClass('red');
                        $link.addClass('green');
                    } else {
                        $link.addClass('red');
                        $link.removeClass('green');
                    }
                }
            }
        );
    },
    /**
     * Atualizar a situação da colaboração com a ficha e ocorrências
     * dependendo do params.sq??? enviado pelo gride
     */
    updateSituacaoColaboracao: function(params, fld, evt) {
        var data = {
            sqFichaColaboracao: params.sqFichaColaboracao,
            sqFichaColaboracaoOcorrencia: params.sqFichaColaboracaoOcorrencia,
            sqSituacao: $(fld).val()
        }
        app.ajax(app.url + 'gerenciarConsulta/changeSituacaoColaboracao', data,
            function(res) {
                if ($.trim(res) != '') {
                    app.alertInfo(res);
                } else {

                    var codigo = $(fld).find('option:selected').data('codigo');
                    //var divId   = "div_colaboracao_" + params.sqFichaColaboracao;
                    if (codigo != 'NAO_AVALIADA') {
                        $(fld).closest('tr').removeClass('colaboracao-nao-avaliada');
                        $(fld).closest('tr').addClass('colaboracao-avaliada');
                    } else {
                        $(fld).closest('tr').removeClass('colaboracao-avaliada');
                        $(fld).closest('tr').addClass('colaboracao-nao-avaliada');
                    }
                }
            }
        );
    },

    /***************************************************************************/
    /* ABA CONVIDAR */
    /***************************************************************************/
    tabConvidarInit: function() {
        destroyEditors();
        // adicionar evento change nos radios de filtragem do campo select
        $("#divSelectPessoa input:checkbox").on('change',function(){
            gerenciarConsulta.selectFiltersChange();
        });
        if( !tinymce.activeEditor || tinymce.activeEditor.id != 'txEmail' )
        {
            var editorOptions = $.extend({},default_editor_options, {
                selector: '#txEmail',
                toolbar : 'newdocument | ' + default_editor_options.toolbar
            });
            tinymce.init(editorOptions);
        };
        gerenciarConsulta.selectFiltersChange();
    },
    sqCicloConsultaChange: function(params,elem ) {

        var sqCicloConsulta = $("#gerenciarConsultaContainer #sqCicloConsultaConvidar").val();
        var data= $("#tabConvidar #sqCicloConsultaConvidar option:selected").data()
        if( data.situacao )
        {
            if( data.situacao=='ABERTA')
            {
                $("#tabConvidar #fieldset_convidar,#fieldset_email").show();
            }
            else
            {
                $("#tabConvidar #fieldset_convidar,#fieldset_email").hide();
            }
        }

        gerenciarConsulta.filtrosAbaConvidados = {}
            // limpar grides
        $('#frmConvidar #divGridFichasConvidar,#divGridConvidadosConvidar').html('');
        // esconder o formulário de cadastro
        if( ! $('#acordeon_frmConvidar').hasClass('hidden') ) {
            $("#btnShowFormConvidar").click();
        }
        if ( sqCicloConsulta ) {
            $("#gerenciarConsultaContainer #frmConvidar,#divFiltrosFichaConvidar,#frmConvidarEmail,#fieldset_convidar,#fieldset_email").removeClass('hidden');
            var data = {
                    sqCicloConsulta: sqCicloConsulta,
                    callback: 'gerenciarConsulta.updateGridFichasConvidar',
                    contexto: 'consulta'
                };
                // carregar o formulario de filtros da ficha e o gride das fichas
            app.loadModule(baseUrl + 'ficha/getFormFiltroFicha', data, '#gerenciarConsultaContainer #divFiltrosFichaConvidar');
            gerenciarConsulta.updateGridConvidadosConvidar();
        } else {
            $("#gerenciarConsultaContainer #frmConvidar,#divFiltrosFichaConvidar,#frmConvidarEmail,#fieldset_convidar,#fieldset_email").addClass('hidden');
        }
    },
    selectFiltersChange:function()
    {
        var filtros = []
        // desmarcar radio selecionado e marcar o selecionado
        $('#divSelectPessoa label.control-label').removeClass('label-selected');
        $('#divSelectPessoa input:checkbox:checked').each( function(i,input) {
            $(input).parent().addClass('label-selected');
            filtros.push( input.value );
        });

        // mostrar/esconder as opções do select
        $("#sqCicloConsultaConvidar option").each(function(i, option ){
            var data = $(option).data();
            $(option).show();
            if( data.tipo ) {
                $(option).hide();
                // esconder situações indesejadas
                if (
                    ( data.situacao == 'ABERTA' && filtros.indexOf('A') > -1
                        || data.situacao == 'FECHADA' && filtros.indexOf('F') > -1
                    ) && ( data.tipo == 'CONSULTA_DIRETA' && filtros.indexOf('D') > -1 ||
                        data.tipo == 'REVISAO_POS_OFICINA' && filtros.indexOf('R') > -1)
                    )
                     {
                    $(option).show();
                }
            }
        })
        // limpar seleção atual
        $("#sqCicloConsultaConvidar").val('')
        gerenciarConsulta.sqCicloConsultaChange();
    },
    updateGridFichasConvidar: function(filtros) {
        gerenciarConsulta.filtrosAbaConvidados = filtros || {};
        var data = gerenciarConsulta.filtrosAbaConvidados;
        data.sqCicloConsulta = $("#gerenciarConsultaContainer #sqCicloConsultaConvidar").val();
        data.sqCicloAvaliacao = $("#gerenciarConsultaContainer #sqCicloAvaliacao").val();
        if (!data.sqCicloConsulta) {
            return;
        }
        var posicao = $("#gerenciarConsultaContainer #divGridFichasConvidar").scrollTop();
        app.blockElement('#gerenciarConsultaContainer #divGridFichasConvidar', 'Carregando gride...');
        app.ajax(baseUrl + 'gerenciarConsulta/getGridFichasConvidar', data, function(res) {
            $("#gerenciarConsultaContainer #divGridFichasConvidar").html(res);
            $("#gerenciarConsultaContainer #divGridFichasConvidar").scrollTop(posicao);

            // ativar seleção de linhas com shift+click
            $("#tbGerConsultaFichas").shiftSelectable();

        }, '', 'html');
    },
    updateGridConvidadosConvidar: function(params) {
        var data = {
            'sqCicloConsulta': $("#gerenciarConsultaContainer #sqCicloConsultaConvidar").val()
        };
        if (!data.sqCicloConsulta) {
            return;
        }
        app.ajax(baseUrl + 'gerenciarConsulta/getGridConvidadosConvidar', data, function(res) {
            $("#divGridConvidadosConvidar").html(res);
            // ativar seleção de linhas com shift+click
            $("#tableConvidadosConvidar").shiftSelectable();
        }, '', 'text');
    },
    saveFrmConvidar: function(params) {
        // verificar se tem alguma ficha selecionada
        if ($("#divGridFichasConvidar input:checkbox:checked").size() == 0) {
            app.alertInfo('Nenhuma ficha selecionada!');
            return;
        }
        if (!$('#frmConvidar').valid()) {
            return;
        }
        var data = $("#frmConvidar").serializeArray();
        data = app.params2data(params, data, 'frmConvidar');
        data.sqPessoaConvidada = $("#frmConvidar #sqPessoaConvidada").val();
        data.sqInstituicao = $("#frmConvidar #sqInstituicao").val();
        data.ids = []
        $("#divGridFichasConvidar input:checkbox:checked").each(function() {
            data.ids.push(this.value);
        });
        data.ids = data.ids.join(',');
        app.ajax(app.url + 'gerenciarConsulta/saveConvidado', data,
            function(res) {
                if (res.status == 0) {
                    gerenciarConsulta.resetFrmConvidar();
                    gerenciarConsulta.updateGridConvidadosConvidar();
                }
            }, 'Gravando...', 'json');
    },
    sqPessoaConvidadaChange: function(params) {
        if (params.select2.data().length > 0) {
            $("#frmConvidar #deEmail").val(params.select2.data()[0].email);
            app.focus('deObservacao');
        } else {
            $("#frmConvidar #deEmail").val('');
        }
    },

    enviarEmailConvidados: function(params) {
        if ($("#divGridConvidadosConvidar input:checkbox:checked").size() == 0) {
            app.alertInfo('Nenhum convidado selecionado!');
            return;
        }
        if (!$('#frmConvidarEmail').valid()) {
            return;
        }

        app.confirm('Confirma envio de email?',
            function() {
                // verificar se tem alguma ficha selecionada
                $("#txEmail").val(tinymce.get('txEmail').getContent());
                var data = $("#frmConvidarEmail").serializeArray();
                data = app.params2data(params, data, 'frmConvidarEmail');
                data.ids = []
                $("#divGridConvidadosConvidar input:checkbox:checked").each(function() {
                    data.ids.push(this.value);
                });
                data.ids = data.ids.join(',');
                app.ajax(app.url + 'gerenciarConsulta/enviarEmailConvidados', data,
                    function(res) {
                        if (res) {
                            app.alertInfo(res);
                        }
                        app.reset('frmConvidarEmail', 'deEmailCopia');
                        app.focus('deAssuntoEmail');
                        gerenciarConsulta.updateGridConvidadosConvidar();
                    }, 'Enviando Email. Aguarde...', 'text');
            });

    },
    getTextLastEmail: function(param) {
        var data = {
            sqCicloConsulta: $("#sqCicloConsultaConvidar").val()
        };
        app.ajax(app.url + 'gerenciarConsulta/getTextLastEmail', data,
            function(res) {
                if (res) {
                    var linhas = res.split('\n');
                    if (linhas[0].indexOf('Assunto:') == 0 && linhas[1].indexOf('Cc:') == 0 && linhas[2] == '') {
                        $("#deAssuntoEmail").val(linhas[0].replace(/Assunto: ?/, ''));
                        $("#deEmailCopia").val(linhas[1].replace(/Cc: ?/, ''));
                        linhas = linhas.splice(3);
                        tinymce.get('txEmail').setContent(linhas.join('\n'));
                        //$("#frmConvidarEmail #txEmail").html(linhas.join('\n'));
                    } else {
                        $("#frmConvidarEmail #txEmail").val(res);
                    }
                }
                app.focus('txEmail', 'frmConvidarEmail');
            });
    },
    editConvidado: function(params, btn) {
        if (!params.id) {
            return;
        }
        var data = {
            'sqCicloConsultaPessoa': params.id
        };
        app.ajax(app.url + 'gerenciarConsulta/editConvidado', data,
            function(res) {
                if (res.sqCicloConsultaPessoa) {
                    gerenciarConsulta.resetFrmConvidar();
                    app.selectGridRow(btn);
                    app.setFormFields(res, 'frmConvidar')
                    $("#sqPessoaConvidada").select2('enable', false)
                        // desmarcar todos os checkbox do grid fichasConvidar
                    $("#divGridFichasConvidar input:checkbox:checked").each(function() {
                        $(this).prop('checked', false);
                    });
                    // marcar os checkboxes com as fichas do convidado
                    $("#divGridFichasConvidar #checkUncheckAll").removeClass('glyphicon-check').addClass('glyphicon-unchecked')
                    $.each(res.fichas, function(i, id) {
                        $("#sqCicloConsultaFicha_" + id).prop('checked', true);
                    });
                    // expander o formulário
                    if( $('#acordeon_frmConvidar').hasClass('hidden') ) {
                        $("#btnShowFormConvidar").click();
                    }
                    $('#acordeon_frmConvidarEmail').collapse('hide');

                }
            }, 'Processando...', 'JSON');
    },
    resetFrmConvidar: function() {
        $("#btnSaveFrmConvidar").html('Gravar convidado');
        $("#sqPessoaConvidada").select2('enable');
        app.unselectGridRow('divGridConvidadosConvidar');
        app.reset('frmConvidar');
        app.focus('sqPessoaConvidada');
    },
    deleteConvidado: function(params, btn) {
        app.selectGridRow(btn);
        app.confirm('Confirma exclusão de ' + params.descricao + '?',
            function() {
                app.ajax(app.url + 'gerenciarConsulta/deleteConvidado',
                    params,
                    function(res) {
                        if (res) {
                            app.alertInfo(res);
                        } else {
                            gerenciarConsulta.updateGridConvidadosConvidar();
                            gerenciarConsulta.resetFrmConvidar();
                        }
                    });
            },
            function() {
                app.unselectGridRow(btn);
            }, params, 'Confirmação', 'warning');
    },
    changeSituacaoConvite: function(el) {
        var data = $(el).data()
        data.sqSituacao = el.value
        app.ajax(app.url + 'gerenciarConsulta/changeSituacaoConvite',
            data,
            function(res) {});
    },
    updateSituacaoFicha: function(params, ele, evt) {
        var $ele = $(ele);
        params.sqSituacao = ele.value
        evt.preventDefault();
        var chkManterLc = $("#checkMaterLc"+params.sqFicha);
        params.stManterLc = chkManterLc.is(':checked') ? 'S': '';
        // verificar se tem alguma colaboração pendente para alterar para a situação Avaliada
        if ( /^(CONSOLIDADA|FINALIZADA|VALIDADA)$/.test( $ele.find('option:selected').data('codigo') ) ) {
            if ($("#tabConsolidar td#tdColaboradores" + params.sqFicha + ' *.red').size() > 0) {
                $ele.val($ele.data('oldValue'))
                app.alertInfo('Ficha possui colaboração não validada!', 'Atenção');
                return;
            }
        }
        app.ajax(app.url + 'gerenciarConsulta/updateSituacaoFicha',
            params,
            function(res) {
                if (res) {
                    $ele.val($ele.data('oldValue'))
                    app.alertInfo( res );
                } else {
                    var value = $ele.find('option:selected').data('codigo');
                    $ele.data('oldValue', $ele.val());
                    if ( value == 'CONSOLIDADA') {
                        $ele.addClass('green');
                        $ele.closest('tr').addClass('ficha-consolidada')
                    } else {
                        $ele.removeClass('green');
                        $ele.closest('tr').removeClass('ficha-consolidada');
                    }
                    if( /CONSOLIDADA|CONSULTA|CONSULTA_FINALIZADA|AVALIADA_EM_REVISAO/.test( value ) )
                    {
                        $("#checkMaterLc"+params.sqFicha).prop('disabled',false);
                        $("#checkMaterLc"+params.sqFicha).prop('title','');
                    }
                    else
                    {
                        $("#checkMaterLc"+params.sqFicha).prop('disabled',true);
                        $("#checkMaterLc"+params.sqFicha).prop('title','Ficha não está em consulta ou consulta finalizada ou consolidada ou avaliada em revisão.');
                    }
                }
            });
    },
    showFichasConvidado: function( params,ele, evt){
        evt.preventDefault();
        var data = $(ele).data();
        gerenciarConsulta.getListaFichasConvidado( data.rowNum, data.id,false, data.canModify )
    },
    getListaFichasConvidado:function( rowNum, id , force, canModify) {
        var div = $("#fichas-convidados-row-" + rowNum);
        canModify = ( (typeof canModify == 'undefined' ) ? true : canModify );
        div.html('<h4>Carregando...' + window.grails.spinner + '</h4>');
        var data = {id: id, rowNum: rowNum, canModify:canModify };
        app.ajax(app.url + 'gerenciarConsulta/getListaFichasConvidado', data,
            function (res) {
                div.html(res);
                //var rows = div.find('input[type=checkbox]').size();
                var rows = div.find('li').size();
                $("#a-fichas-convidado-" + rowNum).find('span').html(rows + (rows > 1 ? ' Fichas' : ' Ficha'));
            });
        //}
    },
    deleteFichaConvidado:function( params )
    {
        app.ajax( app.url + 'gerenciarConsulta/deleteFichaConsultaConvidado', params,
            function(res) {
                if( res.status== 0 )
                {
                    //gerenciarConsulta.getListaFichas(params.rowNum, params.id,true);
                    //gerenciarConsulta.updateGridFichas();
                    gerenciarConsulta. getListaFichasConvidado(params.rowNum, params.id,true);
                }
            });
    },



    /******************************************************
     *  ABA CONSOLIDAR
     *
     */

    abaConsolidarInit: function() {
        // adicionar evento change nos radios de filtragem do campo select
        $("#divSelectConsultaConsolidar input:checkbox").on('change',function(){
            gerenciarConsulta.selectFiltersSqConsultaConsolidarChange();
        });
        var $selectConsultaConsolidar = $("#frmConsolidar #sqConsultaConsolidar");
        var ultimaConsultaConsolidada = app.lsGet('ultimaConsulta','');
        $selectConsultaConsolidar.val( ultimaConsultaConsolidada );
        $selectConsultaConsolidar.prop('disabled',false);
        $("#divSelectConsultaConsolidar").show();
        gerenciarConsulta.sqConsultaConsolidarChange();

        $("#sqConsultaConsolidar").select2({
           matcher: function (params, data) {

               // filtro pelo texto digitado pelo usuário
               if (params.term && data.text.indexOf(params.term) == -1) {
                   return null
               }

               var filtros = []
               var currentValue = $("#sqConsultaConsolidar").val();

               // desmarcar radio selecionado e marcar o selecionado
               $('#divSelectConsultaConsolidar label.control-label').removeClass('label-selected');
               $('#divSelectConsultaConsolidar input:checkbox:checked').each(function (i, input) {
                   $(input).parent().addClass('label-selected');
                   filtros.push(input.value);
               });
               var optionsData = $(data.element).data();
               if (optionsData.tipo) {
                   if (!(
                       ((optionsData.situacao == 'ABERTA' && filtros.indexOf('AB') > -1)
                           || (optionsData.situacao == 'FECHADA' && filtros.indexOf('FE') > -1)
                       ) && ((optionsData.tipo == 'CONSULTA_DIRETA' && filtros.indexOf('CD') > -1) ||
                           (optionsData.tipo == 'CONSULTA_AMPLA' && filtros.indexOf('CA') > -1) ||
                           (optionsData.tipo == 'REVISAO_POS_OFICINA' && filtros.indexOf('RE') > -1)
                       ))
                   ) {
                       data = null
                       if( String(optionsData.id) == String(currentValue) ){
                           currentValue=null;
                       }
                   }
               }
               return data
           }
        });



        /*
        // se o valor da última consulta estiver na lista então atualizar o gride
        if( $selectConsultaConsolidar.val() ) {
            gerenciarConsulta.updateGridConsolidar();
            $("#frmConsolidarFiltros").show();
        } else {
            $("#frmConsolidarFiltros").hide();
            app.growl('Selecione a consulta/revisao.',3,'','cc','medium','default')
        }
        */
    },

    sqConsultaConsolidarChange: function() {
        var $selectConsultaConsolidar = $("#frmConsolidar #sqConsultaConsolidar");
        app.lsSet('ultimaConsulta',$selectConsultaConsolidar.val() );
        gerenciarConsulta.filtrosAbaConsolidar = gerenciarConsulta.filtrosAbaConsolidar || {};
        gerenciarConsulta.filtrosAbaConsolidar.paginationCurrentPage=1;
        gerenciarConsulta.updateGridConsolidar();
        $("#frmConsolidarFiltros").show();
        /*
        if( ! $selectConsultaConsolidar.val() ) {
            $("#frmConsolidarFiltros").hide();
        } else {
            $("#frmConsolidarFiltros").show();
        }*/
    },

    selectFiltersSqConsultaConsolidarChange:function() {

        var currentValue = $("#sqConsultaConsolidar").val() || '';
        $('#sqConsultaConsolidar').val(null).trigger('change');
        // limpar o gride
        gerenciarConsulta.updateGridConsolidar();
        $("#frmConsolidarFiltros").show();
    },

    /* exigir confirmação ao fechar a ficha completa com a ficha sem consolidar*/
    confirmClose: function(panel) {
        gerenciarConsulta.updateGridConsolidar(null,panel.sqFicha);
        /*
        //não está funcionando
        if ( $("a.btn[data-action=consolidarFicha]").size() == 1 ) {
            app.confirm('Falta consolidar a ficha. Deseja sair sem consolidar?', function() {
                panel.close();
                gerenciarConsulta.updateGridConsolidar();
            })
            return ''; // app não fechar a janela tem que retornar alguma coisa
        }
        else
        {
             gerenciarConsulta.updateGridConsolidar();
        }
        */
    },
    updateGridConsolidar: function(filtros,sqFicha,pagination) {
        app.grid('GridConsolidar',{sqFicha:sqFicha});
    },
    /*
    updateGridConsolidarOld: function(filtros,idFicha,pagination) {
        var $selectConsultaConsolidar = $("#sqConsultaConsolidar");
        if (filtros) {
            gerenciarConsulta.filtrosAbaConsolidar = filtros;
        }
        pagination = pagination || {};
        var data = gerenciarConsulta.filtrosAbaConsolidar || {};
            // ler os filtros ativos para as fichas
        data.sqCicloAvaliacao = $("#gerenciarConsultaContainer #sqCicloAvaliacao").val();
        data.idFicha = idFicha;

        // filtrar consulta selecionada
        data.sqCicloConsultaFiltro=$selectConsultaConsolidar.val();

        // para realizar a consulta é necessário informar ou a consulta ou o nome de uma especies
        if( !data.sqCicloConsultaFiltro && !data.nmCientificoFiltro ){
            $("#divGridConsolidar").html('');
            if( data.count ){
                app.alertInfo('Necessário selecionar uma consulta ou revisão.');
            }
            return;
        }

        $.extend(data,pagination);
        if (!data.sqCicloAvaliacao) {
            return;
        }
        var $tr=null;
        var nuLilnhaAtual;
        if( idFicha ) {
            $tr = $('#grideConsolidarWrapper').find('tr[id=tr-' + idFicha + ']');
            nuLilnhaAtual = $tr.find('td:first').html();
            $tr.html('<td colspan="6" class="text-left" style="background-color: #f7e6aa;"><h4>Atualizando...'+window.grails.spinner+'</h4></td>');
        }
        else {
            app.blockElement('#gerenciarConsultaContainer #tabConsolidar', ($("#divGridConsolidar table").size() == 0 ? 'Carregando gride...' : 'Atualizando gride...'));
        }
        var position = $("#grideConsolidarWrapper").scrollTop();

        app.ajax(app.url + 'gerenciarConsulta/getGridConsolidar', data,
            function(res) {
                if( ! idFicha ) {
                    app.unblockElement('#gerenciarConsultaContainer #tabConsolidar');
                    $("#divGridConsolidar").html(res);
                    $("#grideConsolidarWrapper").scrollTop(position);
                    // posicionar a aba no topo da tela
                    $(document).scrollTop(133);
                    // calcular max Height baseado no monitor
                    //console.log( window.innerHeight - 480 );
                    var maxHeight = Math.max( window.innerHeight - 200,260);
                    gerenciarConsulta.gridAbaConsolidar = $('#tableGridConsolidar').DataTable( $.extend({},default_data_tables_options,
                        {
                            "deferRender"      : true
                            ,"scrollY"          : maxHeight
                            ,"scrollX"          : true
                            ,"scrollCollapse"   : true
                            ,"fixedHeader"      : false
                            ,"lengthChange"     : false
                            ,"order": []
                            ,"buttons": []
                            ,"columnDefs" : [
                                {"orderable": false, "targets": [0,1,2,3,4,5] },
                            ]
                        })
                    );

                } else {
                    // atualizar somente o conteudo da linha específica
                    $trFicha=$(res).find('tr[id=tr-'+idFicha+']').html()
                    if( $trFicha ) {
                        $tr.html($trFicha);
                        $tr.find('td:first').html(nuLilnhaAtual);
                        //gerenciarConsulta.gridAbaConsolidar.rows($tr).invalidate().draw();
                        $('#tableGridConsolidar').DataTable().rows($tr).invalidate('dom');
                    } else {
                        $tr.html('Problema ao atualizar a linha. Informe ao administrador.')
                    }
                }
            }, null, 'HTML'
        );
    },*/
    getGridConsolidarPaginado :function( params, event )
    {
        if( typeof event != 'undefined' )
        {
            event.preventDefault();
        }
        gerenciarConsulta.updateGridConsolidar(null,null,params)
    },
    linkToggleFichasClick:function( params,elem,evt )
    {
        evt.preventDefault();
        var data = $(elem).data();
        var $div = $("#divFichas-"+data.rowNum);
        if( $div.size() == 1 && $div.find('ol').size() == 0 ) {
            gerenciarConsulta.getListaFichas( data.rowNum, data.id )
        }
    },
    getListaFichas:function( rowNum, id , force)
    {
        var div = $("#divFichas-"+rowNum);
        //$(div).collapse('toggle');
        //if( div.html().trim() == '')
        //{
            div.html('<h4>Carregando...'+window.grails.spinner+'</h4>');
            var data = {id:id,rowNum:rowNum}
            app.ajax( app.url + 'gerenciarConsulta/getListaFichas', data,
                function(res) {
                    div.html(res);
                    // atualizar o total de fichas exibido na tela
                    var rows = div.find('li').size();
                    $("#a-"+rowNum).find('span').html(rows + ( rows > 1 ? ' Fichas':' Ficha') );
                });
        //}
    },
    deleteFicha:function( params )
    {
        app.ajax( app.url + 'gerenciarConsulta/deleteFichaConsulta', params,
            function(res) {
                if( res.status== 0 )
                {
                    // atualizar a lista de fichas
                    gerenciarConsulta.getListaFichas(params.rowNum, params.id,true);
                }
            });
    },

}
setTimeout( function() {
  gerenciarConsulta.init();
},1000);
