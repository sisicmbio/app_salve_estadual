<!-- view: /views/gerenciarConsulta/_formConvidar -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<div id="divSelectPessoa" class="form-group">
    <g:if test="${listConsultasDiretas.size() == 0}">
        <div class="alert alert-warning mt10">Nenhuma consulta direta cadastrada</div>
    </g:if>
    <g:else>
        <label for="sqCicloConsultaConvidar" class="control-label mb5">Selecione:
            <label class="control-label cursor-pointer ml20 label-selected"><input class="checkbox-lg" type="checkbox" checked="true" value="D">&nbsp;Consulta direta</label>
            <label class="control-label cursor-pointer ml20"><input class="checkbox-lg" type="checkbox" value="R">&nbsp;Revisão</label>
            %{--<label class="control-label cursor-pointer label-selected"><input type="radio" value="" checked="true" name="tipoConsulta">Todas</label>        </div>--}%
            %{--<label class="control-label cursor-pointer"><input type="radio" value="" name="situacaoConsulta">Todas</label>--}%
            <label class="control-label ml20">&nbsp;|&nbsp;</label>
            <label class="control-label cursor-pointer ml20 label-selected"><input class="checkbox-lg" type="checkbox" checked="true" value="A">&nbsp;Abertas</label>
            <label class="control-label cursor-pointer ml20"><input class="checkbox-lg" type="checkbox" value="F">&nbsp;Encerradas</label>
        </label>
        <br>
        <select id="sqCicloConsultaConvidar" name="sqCicloConsultaConvidar"
                data-change="gerenciarConsulta.sqCicloConsultaChange"
                class="form-control fldSelect blue ignoreChange select-consulta">
            <option value="">-- selecione a consulta DIRETA --</option>
            <g:each var="item" in="${listConsultasDiretas}">
%{--                <option data-situacao="${item.dtFim >= br.gov.icmbio.Util.hoje()  ? 'ABERTA':'FECHADA'}" data-tipo="${item.tipoConsulta.codigoSistema}" value="${item.id}">${item.deTituloConsulta + ' - ' + item.periodoText}</option>--}%
                <option data-situacao="${item.dtFim >= br.gov.icmbio.Util.hoje()  ? 'ABERTA':'FECHADA'}"
                        style="display:none;"
                        data-tipo="${item?.codigoSistema}"
                        value="${item.id}">${ item.dtInicio.format('dd/MM/yyyy')+ ' a ' + item.dtFim.format('dd/MM/yyyy')+' - '+item.deTituloConsulta}</option>
            </g:each>
        </select>
    </g:else>
</div>

<g:if test="${canModify}">
    <fieldset id="fieldset_convidar" class="hidden">
        <legend style="font-size:1.2em;">
            <a  id="btnShowFormConvidar"
                data-action="app.showHideContainer"
                data-target="acordeon_frmConvidar"
                data-focus="sqPessoaConvidada"
                title="Mostrar/Esconder formulário!">
                <i class="fa fa-plus green"/>&nbsp;Convidar</a>
        </legend>

        <div id="acordeon_frmConvidar" class="hidden" style="background-color:#ffffff;padding:5px;">
            <form id="frmConvidar" name="frmConvidar" class="form-inline hidden" role="form">
                <input type="hidden" name="sqCicloConsultaPessoa" id="sqCicloConsultaPessoa" value="">

                <div class="fld form-group">
                    <label class="control-label label-required">Nome:</label>
                    <br>
                    <select name="sqPessoaConvidada" id="sqPessoaConvidada" class="form-control fld400 ignoreChange select2" data-change="gerenciarConsulta.sqPessoaConvidadaChange"
                            style="font-size:12px;font-weight:normal"
                            data-s2-url="ficha/select2PessoaFisica"
                            data-s2-placeholder="?"
                            data-s2-minimum-input-length="3"
                            data-s2-auto-height="true" required>
                    </select>
                </div>

                <div class="fld form-group">
                    <label for="deEmail" class="control-label label-required">Email</label>
                    <br>
                    <input name="deEmail" id="deEmail" type="text" value="" class="form-control ignoreChange fld300" required>
                </div>

                <div class="fld form-group">
                    <label for="deObservacao" class="control-label">Observação</label>
                    <br>
                    <input name="deObservacao" id="deObservacao" type="text" value="" class="form-control ignoreChange fld400">
                </div>

                <br>

                %{-- INSTITUICAO--}%
                <div class="fld form-group w100p" style="max-width: 1024px;"  id="divConvidadoInstituicao">
                    <label for="sqInstituicao" class="control-label" title="A instituição preenchida permite a importação do participante para a oficina de avaliação">Instituição</label>
                    <br>
                    <select name="sqInstituicao" id="sqInstituicao" class="fld form-control select2" style="width:100%;font-size:12px;font-weight:normal;"
                            data-s2-url="main/select2Instituicao"
                            data-s2-placeholder="?"
                            data-s2-minimum-input-length="3"
                            data-s2-auto-height="true">
                    </select>
                </div>


                %{--Filtro da ficha--}%
                <div class="hidden mt10" id="divFiltrosFichaConvidar"></div>
                %{-- gride para selecionar ficha --}%
                <h4 class="mt10">Selecione a(s) Ficha(s)</h4>
                %{--GRIDE--}%
                <div id="divGridFichasConvidar" style="max-height: 500px;overflow-y:auto;"></div>
                %{-- botão --}%
                <div class="fld panel-footer">
                    <button id="btnSaveFrmConvidar" data-action="gerenciarConsulta.saveFrmConvidar" data-params="sqCicloConsultaConvidar" class="fld btn btn-success">Gravar convidado</button>
                    <button data-action="gerenciarConsulta.resetFrmConvidar" class="fld btn btn-danger">Limpar campos</button>
                </div>
            </form>
        </div>
    </fieldset>
</g:if>
%{-- gride para selecionar ficha --}%
<div id="divGridConvidadosConvidar" data-params="sqCicloConsultaConvidar">
    %{-- <g:render template="divGridPessoa"></g:render>--}%
</div>
<g:if test="${canModify}">
    <fieldset id="fieldset_email" class="hidden">
        <legend style="font-size:1.2em;">
            <a data-toggle="collapse" data-parent="#acordeon_frmConvidarEmail" href="#acordeon_frmConvidarEmail">
                <i class="glyphicon glyphicon-envelope"/>&nbsp;Elaborar Email</a>
        </legend>

        <div id="acordeon_frmConvidarEmail" class="panel-collapse collapse" role="tabpanel" style="background-color:#ffffff;padding:5px;">
            <form id="frmConvidarEmail" name="frmConvidarEmail" class="form-inline hidden" role="form">

                <div class="form-group" style="width: 100%">
                    <label for="" class="control-label">Assunto</label>
                    <br>
                    <input name="deAssuntoEmail" id="deAssuntoEmail" class="fld form-control w100p" value="" required></input>
                </div>
                <br/>

                <div class="form-group" style="width: 100%">
                    <label for="" class="control-label">Mensagem&nbsp;<i class="glyphicon glyphicon-save" title="Recuperar texto do último envio!"
                                                                         onClick="gerenciarConsulta.getTextLastEmail()"></i><span style="margin-left:10px;font-size:12px;color:gray;">Variáveis: <a
                            title="inserir" href="#" onClick="app.insertTextEditor(this)">{nome}</a>
                        , <a title="inserir" href="#" onClick="app.insertTextEditor(this)">{fichas}</a>
                        , <a title="inserir" href="#" onClick="app.insertTextEditor(this)">{linkSalveConsulta}</a>
                        , <a title="inserir" href="#" onClick="app.insertTextEditor(this)">{dataFinal}</a>
                        , <a title="inserir" href="#" onClick="app.insertTextEditor(this)">{emailConvidado}</a>
                    </span></label>
                    <br>
                    <textarea name="txEmail" id="txEmail" class="fld form-control fldTextarea" rows="10" style="width: 100%;height:500px;" required>${textoEmailPadrao}</textarea>
                </div>
                <br/>

                <div class="form-group" style="width: 100%">
                    <label for="deEmailCopia" class="control-label" title="Para enviar cópia para mais de um email, utilize a vírgula como separador.">Com cópia</label>
                    <br>
                    <input name="deEmailCopia" id="deEmailCopia" class="fld form-control w100p" value="${emailUsuario}"></input>
                </div>

                <div class="fld panel-footer">
                    <button data-action="gerenciarConsulta.enviarEmailConvidados" data-params="sqCicloConsultaConvidar" class="fld btn btn-primary">Enviar email</button>
                </div>
            </form>
        </div>
    </fieldset>
</g:if>
