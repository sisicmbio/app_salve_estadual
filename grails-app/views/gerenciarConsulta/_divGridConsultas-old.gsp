<!-- view: /views/gerenciarConsulta/_divGridConsultas -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<input type="hidden" id="canModify" value="${canModify}"/>
<table class="table table-hover table-striped table-condensed table-bordered" id="tableDivGridConsultas">
    <thead>
        <th style="width:70px;">#</th>
        <th style="width:100px;" class="select-filter">Tipo</th>
        <th style="width:100px;" class="select-filter">Situação</th>
        <th style="width:300px;">Título</th>
        <th style="width:200px;">Período</th>
        <th style="width:150px;"class="select-filter">Unidade</th>
        <th style="width:*;">Fichas</th>
        <g:if test="${canModify}">
            <th style="width:80px;">Ação</th>
        </g:if>
    </thead>
    <tbody>
        <g:if test="${listConsultas}">
            <g:each var="item" in="${listConsultas}" status="i">
                <tr id="gdFichaRow_${i + 1}">
                	<td class="text-center">
                        ${i+1}
                    </td>
                    <td class="text-center">${item.tipoConsulta.descricao}</td>
                    <td class="text-center">${item.isOpen?'Aberta':'Encerrada'}</td>
                    <td class="text-center">${item.deTituloConsulta}</td>
                    <td class="text-center">${item.periodoText}</td>
                    <td class="text-center">${item?.fichas?.size() > 0 ? item.fichas[0].vwFicha.sgUnidadeOrg :''}</td>
                    <td>
                        %{--<g:set var="tmpText"  value="${item.fichasText}"/>--}%
                        %{--<a class="cursor-pointer" data-id="${item.id}" data-action="gerenciarConsulta.getListaFichas" style="font-size:2rem;" data-toggle="collapse" data-target="#fichas-row-${i+1}" title="Clique para visualizar/esconder a lista de espécies">${ tmpText.findAll(",").size()+1} espécies</a>--}%
                        %{--<a class="cursor-pointer" data-id="${item.id}" data-action="gerenciarConsulta.getListaFichas" style="font-size:2rem;" data-toggle="collapse" data-target="#fichas-row-${i+1}" title="Clique para visualizar/esconder a lista de espécies">${ item?.fichas?.size() } fichas</a>--}%
                        %{--<div id="fichas-row-${i+1}" class="collapse" style="border-top:1px solid silver;"></div>--}%

                        <fieldset>
                            <legend style="font-size:1.2em;">
                                <a data-toggle="collapse" href="#divFichas-${i}"
                                   data-div-id="divFichas-${i}"
                                   data-action="gerenciarConsulta.getListaFichas"
                                   data-id="${item.id}"
                                   class="tooltipstered" style="cursor: pointer; opacity: 1;" title="Clique para mostrar/esconder as fichas">
                                   ${item?.fichas?.size()}&nbsp;Fichas
                                </a>
                            </legend>
                            <div id="divFichas-${i}" class="panel-collapse collapse" role="tabpanel" style=""></div>
                        </fieldset>
                    </td>
                    <g:if test="${canModify}">
                    <td class="td-actions">
                        <button type="button" data-action="gerenciarConsulta.editConsulta" data-sq-ciclo-consulta="${item?.id}" class="btn btn-default btn-xs btn-update" data-toggle="tooltip" data-placement="top" title="Alterar">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </button>
                        <button type="button" data-action="gerenciarConsulta.deleteConsulta" data-row="${i + 1}" data-sq-ciclo-consulta="${item?.id}" class="btn btn-default btn-xs btn-delete" data-toggle="tooltip" data-placement="top" title="Excluir">
                            <span class="glyphicon glyphicon-remove"></span>
                        </button>
                    </td>
                    </g:if>
                </tr>
            </g:each>
        </g:if>
        <g:else>
            <tr>
                <td colspan="4" align="center">
                    <h4>Nenhuma consulta cadastrada!</h4>
                </td>
            </tr>
        </g:else>
    </tbody>
</table>
