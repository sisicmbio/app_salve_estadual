<!-- view: /views/corpUsuario/_gridUsuario -->
<!-- controller:${params?.controller} -->
<!-- action: ${params?.action} -->
<div id="gridWrapper" style="width:100%;height:330px;border-bottom:1px solid gray;overflow-y: auto;">
    <table class="table table-striped table-responsive table-hover" id="tableUsuarios">
        <thead>
        <tr>
            <th style="width: 40px">#</th>
            <th style="width: auto">Nome</th>
            <th style="width: 300px">Instituição / Lotação</th>
            <th style="width: 80px;">Ativo?</th>
            <th style="width: 300px;">E-mail</th>
            <th style="width: 150px;">CPF</th>
            <th style="width: 400px;">Perfil</th>
            <th style="width: 80px;">Ações</th>
        </tr>
        </thead>
        <tbody>
        <g:each var="item" in="${usuarios}" status="index">
            <tr id="tr-${item.id}">
                <td class="text-center">${index+1}</td>
                <td id="tdNoPessoa" class="nowrap">${item.pessoaFisica.pessoa.noPessoa}</td>
                <td id="tdSgInstituicao" class="text-center"> ${item?.pessoaFisica?.instituicao?.sgInstituicao}</td>
                <td id="tdDsAtivo" class="text-center ${item.stAtivo ? 'green' : 'red'}">${item.stAtivo?'Sim':'Não'}</td>
                <td id="tdDeEmail">${item.pessoaFisica.pessoa.deEmail}</td>
                <td id="tdCpfFormatado">${item.pessoaFisica.cpfFormatado}</td>
                <td class="text-left">
                %{--<g:if test="${item?.perfis}">
                    <ol>
                        <g:each var="usuarioPerfil" in="${item.perfis.sort{it.perfil.noPerfil}}">
                            <li class="nowrap">${usuarioPerfil.perfil.noPerfil}</li>
                        </g:each>
                    </ol>
                </g:if>--}%

                    <g:set var="perfisInstituicao" value="${item.perfilPorInstituicao}"></g:set>
                    <g:if test="${perfisInstituicao}">
                        <ul>
                            <g:each var="perfilInstituicao" in="${perfisInstituicao}">
                                <li class="nowrap"><b>${perfilInstituicao.sgInstituicao}</b></li>
                                <ol>
                                    <g:each var="perfil" in="${perfilInstituicao.perfis}">
                                        <li class="nowrap">${perfil.noPerfil}</li>
                                    </g:each>
                                </ol>
                            </g:each>
                        </ul>

                    </g:if>
                    <g:else>
                        <ul>
                            <li>Sem perfil</li>
                        </ul>
                    </g:else>
                </td>

                <td>
                    %{--NÃO PERMITIR ALTERAR/EXCLUIR USUARIO ADMINISTRADOR--}%
                    <i class="fa fa-edit fa-button mouse-pointer blue" title="Alterar" onClick="corpUsuario.edit(${item.id})"></i>
                    <g:if test="${item.pessoaFisica?.pessoa?.deEmail == 'admin@salve'}">
                        <i class="fa fa-remove fa-button disabled gray"></i>
                    </g:if>
                    <g:else>
                        <i class="fa fa-remove fa-button mouse-pointer red" title="Excluir" onClick="corpUsuario.delete(${item.id},'${item?.pessoaFisica?.primeiroNome}')"></i>
                        <g:if test="${item.stAtivo}">
                            <i class="fa fa-list-ol fa-button mouse-pointer yellow" title="Atribuir/Alterar perfil" onClick="corpUsuario.perfil(${item.id})"></i>
                        </g:if>
                        <g:else>
                            <i class="fa fa-list-ol fa-button disabled gray"></i>
                        </g:else>
                    </g:else>
                </td>
            </tr>
        </g:each>
        </tbody>
    </table>
</div>
<g:if test="${usuarios.size()>0}">
    <div style="height:auto;border:none;text-align: center;">
        <ul class="pagination pagination-sm" data-total-records="${totalRecords}" data-total-pages="${totalPages}">
            <li class="page-item ${page==1 && page < totalPages ? 'disabled':'' }"><a class="page-link" href="javascript:void(0);"
                <g:if test="${page > 1}">
                    onClick="corpUsuario.grid({ page: ${page.toInteger() - 1 } })"
                </g:if>

            >Anterior</a></li>
            <li class="page-item"><a class="page-link" href="javascript:void(0);">Pág: <select class="fld40" onChange="corpUsuario.grid({ page: this.value })">
                <g:each var="pageNum" in="${1..totalPages}">
                    <option value="${pageNum}" ${pageNum==page?'selected':''}>${pageNum}</option>
                </g:each>
            </select>
            </a>
            </li>
            <li class="page-item ${page==totalPages ? 'disabled':'' }"><a class="page-link" href="javascript:void(0);"
                <g:if test="${page < totalPages}">
                    onClick="corpUsuario.grid({ page: ${page.toInteger() + 1 } })"
                </g:if>
            >Próxima</a></li>
        </ul>

    </div>
</g:if>
