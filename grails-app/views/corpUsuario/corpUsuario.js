//# sourceURL=corpUsuario.js
;var corpUsuario = {
    map:null,
    inputSearch:null,

    init: function () {
        app.focus("nuCpf");
        corpUsuario.grid();
        // adicionar evento tecla [enter} no campo pesquisar do gride
        corpUsuario.inputSearch = $("input#inputSearch");
        if( corpUsuario.inputSearch ){
            corpUsuario.inputSearch.on('keyup', function(evt ) {
                if( evt.keyCode == 13 || evt.key == 'Enter'){
                    corpUsuario.grid({ q:corpUsuario.inputSearch.val()});
                }
            })
        }
    },
    reset:function(){
        app.reset('frmUsuario');
        $("label[for=deSenha]").addClass('label-required');
        $("label[for=deSenha2]").addClass('label-required');
    },
    cpfChange:function( params, ele, evt){
        var data = { nuCpf:ele.value }
        if( data.nuCpf ) {
            app.ajax(baseUrl + 'corpUsuario/consultarCpf', data, function (res) {
                if (res.status == 0) {
                    var sqPessoa = $("#sqPessoa").val();
                    if( res.data.sqPessoa && ( !sqPessoa || sqPessoa != res.data.sqPessoa ) ) {
                        $("#nuCpf").val('');
                        app.focus('nuCpf');
                        app.growl('CPF já cadastrado para '+ res.data.noPessoa);
                    }

                }
            }, 'Verificando CPF...')
        }
    },
    emailChange:function( params, ele, evt){
        var data = { deEmail:ele.value }
        if( data.deEmail ) {
            app.ajax(baseUrl + 'corpUsuario/consultarEmail', data, function (res) {
                if (res.status == 0) {
                    var sqPessoa = $("#sqPessoa").val();
                    if( res.data.sqPessoa && ( !sqPessoa || sqPessoa != res.data.sqPessoa ) ) {
                        $("#deEmail").val('');
                        app.focus('deEmail');
                        app.growl('E-mail já cadastrado para '+ res.data.noPessoa);
                    }

                }
            }, 'Verificando CPF...')
        }
    },
    save:function(){
        var data = {'sqPessoa':$("#sqPessoa").val()
            ,'noPessoa':$("#noPessoa").val()
            ,'deEmail':$("#deEmail").val()
            ,'nuCpf':$("#nuCpf").val()
            ,'deSenha':$("#deSenha").val()
            ,'deSenha2':$("#deSenha2").val()
            ,'stAtivo':$("#stAtivo").val()
            ,'sqInstituicao':$("#sqInstituicao").val()
        };
        var errors = [];
        if( ! data.noPessoa ){
            errors.push('Nome deve ser informado');
        }
        if( ! data.deEmail && !data.nuCpf ){
            errors.push('E-mail ou CPF deve ser informado');
        }
        if( ! data.stAtivo ){
            errors.push('Campo Ativo/Não ativo deve ser informado');
        }

        // se for uma alteração, não precisa informar as senhas
        if( !data.sqPessoa && ! data.deSenha ){
            errors.push('Senha de acesso deve ser informada');
        }

        if( data.deSenha && data.deSenha != data.deSenha2 ){
            errors.push('Senhas informadas estão diferentes');
            $("#deSenha2").val('');
            app.focus('deSenha2');
        }

        if( errors.length > 0 ){
            app.growl(errors.join('<br>'),'4','Erros encontrados','tc','','error');
            return;
        }

        app.ajax(baseUrl+'corpUsuario/save',data,function(res){
            if( res.status == 0 ) {
                corpUsuario.reset();
                app.focus('nuCpf');
                corpUsuario.grid( res.data );
            }
        },'Gravando...')
    },
    edit:function( sqPessoa ) {
        if( sqPessoa ) {
            app.ajax(baseUrl + 'corpUsuario/edit', {sqPessoa: sqPessoa}, function (res) {
                if (res.status == 0) {
                    corpUsuario.reset();
                    app.focus('noPessoa');
                    $("#sqPessoa").val( res.data.sqPessoa );
                    $("#noPessoa").val( res.data.noPessoa );
                    $("#nuCpf").val( res.data.nuCpf );
                    $("#deEmail").val( res.data.deEmail );
                    $("#stAtivo").val( String(res.data.stAtivo) );
                    $("label[for=deSenha]").removeClass('label-required')
                    $("label[for=deSenha2]").removeClass('label-required')
                    if( res.data.sqInstituicao) {
                        app.setSelect2('sqInstituicao', {
                                id: res.data.sqInstituicao
                                , descricao: res.data.sgInstituicao
                            }
                            , res.data.sqInstituicao, '', false)
                    }
                }
            },'Carregando...')
        }
    },
    delete:function( sqPessoa, noPessoa ) {
        if (sqPessoa) {
            if ( app.confirm('Confirma a exclusão do usuário <b>' + noPessoa + '</b>?', function () {
                app.ajax(baseUrl + 'corpUsuario/delete', {sqPessoa: sqPessoa}, function (res) {
                    if (res.status == 0) {
                        corpUsuario.reset()
                        app.focus('nuCpf');
                        $('#gridContainer table tbody tr#tr-' + sqPessoa).remove();
                        corpUsuario.updateNumRecords();
                    }
                }, 'Aguarde...')
            })
            ) ;
        }
    },
    grid:function(data){
        data = data || {};


        // atualizar somente a linha alterada no gride
        if( data.sqPessoa && data.noPessoa ){
            var $tr = $('#gridContainer table tbody tr#tr-' + data.sqPessoa);
            if( $tr.size() == 1 ) {
                $tr.find('td#tdNoPessoa').html(data.noPessoa);
                $tr.find('td#tdDsAtivo').html(data.dsAtivo);
                $tr.find('td#tdDeEmail').html(data.deEmail);
                $tr.find('td#tdNuCpf').html(data.cpfFormatado);
                $tr.find('td#tdSgInstituicao').html(data.sgInstituicao);
                return;
            }
        }

        // paginação - pagina 1 como padrão
        data.page = data.page ? data.page : 1;
        $("#tableUsuarios").floatThead("destroy");
        // recriar o gride
        $("#gridContainer").html('<div class="alert alert-info">Carregando tabela...'+grails.spinner+'</div>');
        app.ajax(baseUrl + 'corpUsuario/grid', data,function (res) {
            $("#gridContainer").html( res );
            corpUsuario.updateNumRecords(data.totalRecords);
            setTimeout(function(){
                $("#tableUsuarios").floatThead({
                    floatTableClass: 'floatHead',
                    scrollContainer: true,
                    autoReflow: true
                });
            },1000);

        },'')
    },
    updateNumRecords:function(){
        var data = $("ul.pagination").data();
        var totalRecords = data.totalRecords ? data.totalRecords : $("#gridContainer table tbody tr").size();
        $("#numRecords").html( String(totalRecords ) + ( totalRecords == 1 ? '&nbsp;Usuario cadastrado' : '&nbsp;Usuarios cadastrados') );
    },
    perfil:function( sqPessoa ){
        app.loadModule('corpUsuarioPerfil',{ sqPessoa:sqPessoa })
    }
}
setTimeout(function(){corpUsuario.init(),1000});
