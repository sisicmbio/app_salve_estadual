<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="ajax"/>
</head>
<body>
<div class="col-sm-12">
    <div class="panel panel-default w100p" id="pnlUsuario">
        <div class="panel-heading">
            <span>
                Usuários do Sistema
            </span>
            <button id="btnClose" data-action="app.unloadModule" class="btn btn-default pull-right btn-xs" title="Fechar"><span class="glyphicon glyphicon-remove btnClose"></span></button>
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-sm-12">

                    <form id="frmUsuario" class="form-inline">

                        %{--CAMPO ID--}%
                        <input type="hidden" id="sqPessoa" name="sqPessoa" value="">


                        %{-- CPF --}%
                        <div class="form-group">
                            <label for="nuCpf" class="control-label">CPF</label><br>
                            <input type="text" class="form-control fld150" id="nuCpf" name="nuCpf"
                                    data-params="sqPessoa"
                                    data-change="corpUsuario.cpfChange"
                                    value="" maxlength="14" data-mask="000.000.000-00">
                        </div>


                        %{-- E-MAIL --}%
                        <div class="form-group">
                            <label for="deEmail" class="control-label">E-mail</label><br>
                            <input type="text"
                                   class="form-control fld300"
                                   data-params="sqPessoa"
                                   data-change="corpUsuario.emailChange"
                                   id="deEmail" name="deEmail" value="">
                        </div>

                        <br>


                        %{-- NOME --}%
                        <div class="form-group">
                            <label class="label-required" for="noPessoa">Nome</label><br>
                            <input type="text" class="form-control fld500" id="noPessoa" name="noPessoa" value="">
                        </div>

                        %{-- INSTITUIÇÃO --}%
                        <div class="form-group w100p">
                            <label for="sqInstituicao" class="label-required">Instituição / Lotação</label><br>
                            <select id="sqInstituicao" name="sqInstituicao" class="form-control select2 fld500"
                                    data-s2-url="corpUsuarioPerfil/select2Instituicao"
                                    data-s2-on-change="corpUsuarioPerfil.instituicaoChange"
                                    data-s2-placeholder="?"
                                    data-s2-minimum-input-length="3"
                                    data-s2-maximum-selection-length="1">
                            </select>
                        </div>

                        <br>
                        %{-- ATIVO SIM/NÃO --}%
                        <div class="form-group">
                            <label for="stAtivo" class="label-required">Ativo?</label><br>
                            <select class="form-control fld140" id="stAtivo" name="stAtivo">
                                <option value="">-- selecione --</option>
                                <option value="true">Sim</option>
                                <option value="false">Não</option>
                            </select>
                        </div>

                        <br>

                        <div class="form-group">
                            <div class="form-group">
                                <label for="deSenha" class="control-label label-required">Senha</label><br>
                                <input type="password" class="form-control fld200" id="deSenha" name="deSenha" value="">
                            </div>

                            <div class="form-group">
                                <label for="deSenha2" class="control-label label-required">Redigite a senha</label><br>
                                <input type="password" class="form-control fld200" id="deSenha2" name="deSenha2" value="">
                            </div>
                        </div>


                        %{--BOTOES--}%
                        <div class="panel-footer col-sm-12">
                            <button type="button" id="btnSave" onclick="corpUsuario.save();" class="btn btn-success">Gravar</button>
                            <button type="button" class="btn btn-danger pull-right" onclick="corpUsuario.reset();">Limpar</button>
                        </div>
                        <div class="col-sm-12"><small class="red fs10">* - campo obrigatório</small></div>
                    </form>

                    %{--  GRIDE ABAIXO DO FORM--}%
                    <div class="row mt20">
                        <div class="col-sm-12">
                            <div class="mb5" style="display: flex;justify-content: space-between;flex-direction: row;">
                                <div>
                                    <h4 id="numRecords">&nbsp;</h4>
                                </div>
                                <div class="input-group">
                                    <input id="inputSearch" type="text" class="form-control fld150" value="">
                                    <span class="input-group-addon" title="Informe o nome ou parte do nome e pressione [Enter] para pesquisar">?</span>
                                </div>
                            </div>
                            <div style="width: auto;height: auto;max-height: 400px;" id="gridContainer"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
