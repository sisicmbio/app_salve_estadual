<!-- view: /views/oficina/_divGridPapelSelFicha -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<div style="display:block;overflow-y:auto;max-height:300px;padding:5px" id="divTbOficinaSelFicha">
    <h4>${listFichas ? listFichas.size() : '0'}&nbsp;Fichas / <span class="total-selecionadas">0 selecionada.</span></h4>
    <table class="table table-sm table-hover table-striped table-condensed table-bordered" id="tbOficinaSelFicha">
        <thead>
        <th style="width:30px;">#</th>
        <th style="width:30px;"><i id="checkUncheckAll" title="Marcar/Desmarcar Todos" data-action="oficina.checkUncheckAll" data-container="divTbOficinaSelFicha" class="glyphicon glyphicon-unchecked check-all"></i></th>
        <th style="width:auto;">Nome Científico</th>
        <th style="width:200px;">Agrupamento</th>
        </thead>
        <tbody>
        <g:if test="${listFichas?.size() > 0}">
            <g:each in="${listFichas}" var="item" status="i">
                <tr id="gdFichaRow_${i + 1}">
                    <td class="text-center">${i+1}</td>
                    <td class="text-center">
                        <input id="sqFicha_${item.sq_ficha}" value="${item.sq_ficha}"
                               type="checkbox" class="checkbox-lg ignoreChange"
                               data-action="contarChecked" data-container="divTbOficinaSelFicha"/>
                    </td>
                    <td>${raw(br.gov.icmbio.Util.ncItalico( item.nm_cientifico,true,true, item.co_nivel_taxonomico ) )}</td>
                    <td>${item.ds_agrupamento?:''}</td>
                </tr>
            </g:each>
        </g:if>
        <g:else>
            <tr>
                <td colspan="5" align="center">
                    <h4>Nenhuma Ficha Cadastrada!</h4>
                </td>
            </tr>
        </g:else>
        </tbody>
    </table>
</div>
