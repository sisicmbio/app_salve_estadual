<!-- view: /views/oficina/_divGridOficinas.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<table id="table${gridId}" class="table table-hover table-striped table-condensed table-bordered">
    <thead>

    %{-- PAGINACAO --}%
    <tr id="tr${gridId}Pagination">
        <td colspan="10" >
            <div class="row">
                <div class="col-sm-8 text-left"  style="min-height:0;height: auto;overflow: hidden;" id="div${(gridId ?: pagination?.gridId)}PaginationContent">
                    <g:if test="${pagination?.totalRecords}">
                        <g:gridPagination  pagination="${pagination}"/>
                    </g:if>
                </div>
                %{-- CAMPO LOCALIZAR (Opcional) --}%
                <div class="col-sm-4">
                    <input type="text" placeholder="localizar..." class="form-control form-control-sm fld250 mt10 hidden"
                           style="float:right;"
                           data-grid-id="${gridId}"
                           onkeyup="gridSelectRows(this)" />
                </div>
            </div>
        </td>
    </tr>
    %{-- FILTROS --}%
    <tr id="tr${gridId}Filters" class="table-filters" data-grid-id="${gridId}">

%{--        COLUNA 1--}%
        <td>
            <div style="display:flex;flex-direction:row; justify-content:space-between;">
                <i class="fa fa-question-circle"
                   title="Utilize os campos ao lado para filtragem dos registros.<br> Informe o valor a ser localizado e pressione <span class='blue'>Enter</span>.<br>Para remover o filtro limpe o campo e pressione <span class='blue'>Enter</span> novamente."></i>
                <i data-grid-id="${gridId}" class="fa fa-filter" title="Aplicar filtro(s)"></i>
                <i data-grid-id="${gridId}" class="fa fa-eraser" title="Limpar filtro(s)"></i>
            </div>
        </td>

%{--        COLUNA OFICINA--}%
        <td>
        <input type="text" name="fldNoOficina${gridId}"
               value="${params?.noOficinaFiltro ?: ''}" class="form-control table-filter"
               placeholder="" data-field="noOficinaFiltro"
        >

    </td>


%{--        COLUNA LOCAL--}%
        <td>
        <input type="text" name="fldDeLocal${gridId}"
               value="${params?.deLocalFiltro ?: ''}" class="form-control table-filter"
               placeholder="" data-field="deLocalFiltro"
        >

    </td>


%{--        COLUNA PERIODO--}%
        <td></td>


%{--        COLUNA TIPO--}%
        <td>
        <select name="fldTipoOficina${gridId}" class="form-control table-filter"
                placeholder="" data-field="sqTipoOficinaFiltro">
            <option value="">Todos</option>
            <g:if test="${listTipoOficina}">
                <g:each var="item" in="${listTipoOficina}">
                    <option data-codigo="${item?.codigo}"
                            value="${item.id}">${item?.descricao}</option>
                </g:each>
            </g:if>
        </select>


        </td>

%{--        COLUNA UNIDADE--}%
        <td title="Informe o nome da unidade ou parte do nome para pesquisar.">
            <input type="text" name="fldSgUnidade${gridId}"
                   value="${params?.sgUnidadeFiltro ?: ''}" class="form-control table-filter"
                   placeholder="" data-field="sgUnidadeFiltro"
                >


        </td>

%{--        COLUNA SITUACAO--}%
        <td>
            <select name="fldSituacao${gridId}" class="form-control table-filter"
                placeholder="" data-field="sqSituacaoFiltro">
            <option value="">Todas</option>
            <g:if test="${listSituacaoOficina}">
                <g:each var="item" in="${listSituacaoOficina}">
                    <option data-codigo="${item?.codigo}"
                        ${item.codigoSistema=='ABERTA' ? ' selected ':''}
                            value="${item.id}">${item?.descricao}</option>
                </g:each>
            </g:if>
            </select>
        </td>


%{--        COLUNA ACAO--}%
        <td>
        <i class="green fa fa-file-excel-o" title="Exportar para planilha" onclick="oficina.exportToPlanilha();"></i>
    </td>
    </tr>


        <tr id="tr${gridId}Columns" data-grid-id="${gridId}">
            <th style="width:70px;">#</th>
            <th style="width:200px;">Oficina</th>
            <th style="width:auto;">Local</th>
            <th style="width:180px">Período</th>
            <th style="width:200px;">Tipo</th>
            <th style="width:200px;">Unidade</th>
%{--            <th style="width:200px;">Fonte</th>--}%
            <th style="width:80px;">Situação</th>
            <th style="width:80px;">Ação</th>
        </tr>
    </thead>


    <tbody>
    <g:if test="${listOficina}">
        <g:each var="item" in="${listOficina}" status="i">
            <tr id="tr-${item.sq_oficina}" class="${ item?.cd_situacao_sistema == 'ENCERRADA'?'red':''}">

%{--                NUMERO DA LINHA--}%
                <td class="text-center">
                    ${ i + ( pagination ? pagination?.rowNum.toInteger() : 1 ) }
                </td>

%{--                CHECKBOX--}%
                <td style="font-size:1.2em;font-weight:bold;">
                    <a title="Clique para Selecionar esta oficina!"
                       href="javascript:void(0)" data-action="oficina.selectOficina"
                       data-params="sqOficina:${item.sq_oficina}, canModify:${(item.cd_situacao_sistema =='ABERTA')},noOficina:${item.no_oficina.replace(/,/,'&comma;')},tipoOficina:${item.ds_tipo.replace(/,/,'&comma;')},codigoSistema:${item.cd_tipo_sistema}">${item.sg_oficina}</a></td>
%{--                LOCAL--}%
                <td class="text-center">${item.de_local}</td>
%{--                PERIODO--}%
                <td class="text-center">${item.de_periodo}</td>
%{--                TIPO--}%
                <td>${item.ds_tipo}</td>
%{--                UNIDADE ORGANIZACIONAL--}%
                <td>${item?.sg_unidade_org}</td>
%{--                FONTE DE RECURSO--}%
%{--                <td class="text-center">${item?.no_fonte_recurso}</td>--}%
%{--                SITUACAO--}%
                <td class="text-center">
                    <select name="sqSituacao-${item.sq_oficina}" data-table-value="_dtv${item?.ds_situacao}" data-sq-oficina="${item.sq_oficina}"
                            data-change="oficina.sqSituacaoChange" class="form-control fldSelect fld120 ${ item?.cd_situacao_sistema != 'ABERTA' ? 'red' : 'green'}">
                        <g:each in="${listSituacaoOficina}" var="situacao">
                            <option data-codigo="${situacao.codigoSistema}" value="${situacao.id}" ${ situacao?.id == item?.sq_situacao ? 'selected':''}>
                                ${situacao.descricao}
                            </option>
                        </g:each>
                    </select>
                </td>
%{-- ACAO --}%

                <td class="td-actions">
                    <a data-id="${item?.sq_oficina}" data-action="oficina.edit" class="btn btn-default btn-xs btn-update"
                       data-toggle="tooltip" title="Alterar">
                        <span class="glyphicon glyphicon-pencil"></span>
                    </a>
                    <a data-id="${item?.sq_oficina}" data-descricao="${item.no_oficina}" data-action="oficina.delete" class="btn btn-default btn-xs btn-delete" data-toggle="tooltip" title="Excluir">
                        <span class="glyphicon glyphicon-remove"></span>
                    </a>
                </td>

            </tr>
        </g:each>
    </g:if>
    <g:else>
        <tr><td class="text-center" colspan="9"><h4>Nenhuma oficina cadastrada</h4></td></tr>
    </g:else>
    </tbody>
</table>
