<!-- view: /views/oficina/_formSelecionarFicha.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<form id="frmOficinaFicha" name="frmOficinaFicha" data-tipo-oficina="${oficina.tipoOficina.codigoSistema}" class="form-inline" style="min-height: 571px;" role="form">
        %{--<legend class="nome-oficina-selecionada">Selecionada: ${oficina.noOficina}</legend>--}%
        <g:if test="${ canModify }">
            <fieldset class="mt10 mb20">
                <legend style="font-size:1.2em;">
                    <a  id="btnShowGrideFichasNaoSelecionadas"
                        data-action="app.showHideContainer"
                        data-target="divGridFichasContainer"
                        data-focus=""
                        data-callback="oficina.ficha.btnCadastrarClick"
                        title="Mostrar/Esconder gride!">
                        <i class="fa fa-plus green"/>&nbsp;Cadastrar</a>
                </legend>
                <div id="divGridFichasContainer" class="hidden">
                    <div id="divFiltrosFichas1" class=""></div>
                    %{-- gride para selecionar ficha --}%
                    <div id="divGridFichas1" style="min-height:200px;max-height: 500px;overflow-y:auto;"><br>Carregando. Aguarde...</div>
                    %{-- botão --}%
                    <div id="div-btn-add-ficha" class="fld panel-footer" style="display:none">
                        <a id="btnSelFichaAdd" data-action="oficina.ficha.save" class="fld btn btn-success">Gravar</a>
                    </div>
                </div>
            </fieldset>
        </g:if>

        <fieldset>
        %{-- GRIDE FICHAS JÁ SELECIONADAS --}%
        <div class="row" id="divGrid2Wrapper">
            %{--Filtro da ficha--}%
            <div class="mt10 col-sm-12" id="divFiltrosFichas2"></div>
            %{--gride--}%
            <div id="divGridFichas2" class="col-sm-12">Carregando. Aguarde...</div>
        </div>

        %{--acoes em lote--}%
        <div id="divAcaoLote" class="row" style="display:none;">
            <div class="col-sm-12">
              <div class="fld form-group">
                  <label for="dsAgrupamento" class="control-label">Agrupamento</label>
                  <br>
                  <input name="dsAgrupamento" id="dsAgrupamento" type="text" value="" class="fld form-control fld400 ignoreChange" >
              </div>
              <div class="fld form-group">
                  <label for="dsAgrupamento" class="control-label">&nbsp;</label>
                  <br>
                   <a id="btnSaveAgrupamento" data-action="oficina.ficha.saveAgrupamento" class="fld btn btn-primary">Salvar Agrupamento</a>
              </div>
              <div class="fld form-group">
                  <label for="dsAgrupamento" class="control-label">&nbsp;</label>
                  <br>
                   <a id="btnSelFichaDelete" data-action="oficina.ficha.delete" data-params="sqOficinaSelecionada|sqOficina" class="fld btn btn-danger">Remover Ficha(s)</a>
              </div>
            </div>
        </div>


    </fieldset>
</form>
<div class="end-page"></div>
