<!-- view: /views/oficina/_divGridExeTotais.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<div class="row">
    <div class="col-sm-12">
        <table class="table table-hover table-striped table-condensed" style="max-width:500px;">
            <thead>
                <th colspan="2">Resultado da Oficina</th>
            </thead>
            <tbody>
                <tr>
                    <td style="vertical-align: top !important;">
                        <table class="table table-hover table-striped table-condensed table-bordered" style="max-width:300px;">
                            <thead>
                                <th colspan="2">Espécies</th>
                            </thead>
                            <tbody>
                                <tr  style="background-color: #fff">
                                    <td>Avaliadas</td>
                                    <td class="text-center">${data.qtdAvaliada + data.qtdLc}</td>
                                </tr>
                                <tr class="${data.qtdNaoAvaliada==0 ? 'bgGreenLight':'bgWhite'}">
                                    <td>A avaliar</td>
                                    <td class="text-center">${data.qtdNaoAvaliada}</td>
                                </tr>
                                <g:if test="${data.qtdTransferida > 0 }">
                                    <tr style="background-color: #fff">
                                        <td>Transferidas</td>
                                        <td class="text-center">${data.qtdTransferida}</td>
                                    </tr>
                                </g:if>
                                <g:if test="${data.qtdExcluida > 0 }">
                                    <tr style="background-color: #fff">
                                        <td>Excluídas</td>
                                        <td class="text-center">${data.qtdExcluida}</td>
                                    </tr>
                                </g:if>
                                <tr style="background-color: #efefef;font-weight: bold;">
                                    <td>Total</td>
                                    <td class="text-center">${data.qtdLc+data.qtdAvaliada+data.qtdNaoAvaliada+data.qtdExcluida+data.qtdTransferida}</td>
                                </tr>
                                <tr style="background-color: #efefef;font-weight: bold;">
                                <td>Mantidas LC</td>
                                <td style="width:50px;" class="text-center">${data.qtdLc}</td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                    <td style="vertical-align: top !important;">
                        <table class="table table-hover table-striped table-condensed table-bordered">
                            <thead>
                                <th colspan="2">Categorias avaliadas</th>
                            </thead>
                            <tbody>

                                <g:if test="${data.qtdCategoria}">
                                    <g:each var="item" in="${data.qtdCategoria}">
                                        <tr style="${ item.key == 'Total' ? 'background-color: #efefef;font-weight: bold;' : 'background-color: #fff;' }">
                                            <td>${item.key}</td>
                                            <td style="width:50px;" class="text-center">${item.value}</td>
                                        </tr>
                                    </g:each>

                                </g:if>
                                <g:else>
                                    <tr><td>&nbsp;</td></tr>
                                </g:else>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </div> %{-- fim cols --}%
</div> %{-- fim row --}%
