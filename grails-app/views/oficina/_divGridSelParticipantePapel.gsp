<!-- view: /views/oficina/_divGridSelParticipantePapel.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<table class="table table-hover table-striped table-condensed table-bordered">
    <thead>
        <th style="width:30px;">#</th>
        <th style="width:30px;"><i title="Marcar/Desmarcar Todos" data-action="oficina.checkUncheckAll" class="glyphicon glyphicon-unchecked"></i></th>
        <th>Nome</th>
        <th>Email</th>
        <th style="width:*;">Instituição</th>
        <th style="width:120px;">Enviado Em</th>
    </thead>
    <tbody>
    <g:if test="${listParticipantes}">
        <g:each in="${listParticipantes}" var="item" status="i">
            <tr>
                <td class="text-center">${i+1}</td>
                <td class="text-center">
                    <input id="sqOficinaParticipante_${item.id}" value="${item.id}" type="checkbox" class="checkbox-lg ignoreChange"/>
                </td>
                <td>${br.gov.icmbio.Util.capitalize( item?.pessoa?.noPessoa)}</td>
                <td>${item.deEmail?:item?.pessoa?.email?.toLowerCase()}</td>
                <td class="text-center">${raw( item?.instituicao?.sgInstituicao ? '<span title="'+item.instituicao.noInstituicao+'">'+ item.instituicao.sgInstituicao+'</span>': '' ) }</td>
                <td class="text-center">${item?.dtEmail?.format("dd/MM/yyyy hh:mm")}</td>
            </tr>
        </g:each>
    </g:if>
    <g:else>
        <tr>
            <td colspan="4" align="center">
                <h4>Nenhum Participante Cadastrado!</h4>
            </td>
        </tr>
    </g:else>
    </tbody>
</table>
