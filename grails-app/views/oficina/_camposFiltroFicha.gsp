<g:if test="${ params.hideFilters.indexOf('categoria-oficina')< 0 }">
    <g:if test="${listCategorias}">
        <div class="form-group">
            <label for="coCategoriaOficinaFiltro${rndId}" class="control-label">Categoria oficina</label>
            <br>
            <select id="coCategoriaOficinaFiltro${rndId}" name="coCategoriaOficinaFiltro" data-change="app.btnFiltroFihaClick" class="form-control fld300 fldSelect">
                <option value="">-- todas --</option>
                <option value="0">-- sem categoria --</option>
                <g:each var="item" in="${listCategorias}">
                    <option data-codigo="${item.codigoSistema}" value="${item.codigo}">${item.descricaoCompleta}</option>
                </g:each>
            </select>
        </div>
    </g:if>
</g:if>

<g:if test="${ params.hideFilters.indexOf('agrupamento')< 0 }">
    <div class="form-group">
        <label for="dsAgrupamentoFiltro${rndId}" class="control-label">Agrupamento</label>
        <br>
        <input type="text" id="dsAgrupamentoFiltro${rndId}" name="dsAgrupamentoFiltro" data-enter="app.btnFiltroFichaClick" class="form-control fld300"/>
    </div>
</g:if>