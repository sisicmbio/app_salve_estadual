<!-- view: /views/oficina/_formExecucao.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

%{--AVALIAÇÃO EXPEDITA--}%
<g:if test="${avaliacaoExpedita}">
    <a data-sq-oficina="${params.sqOficinaSelecionada}" href="javascript:void(0);" title="Fazer avaliação expressa" data-action="oficina.execucao.avaliacaoExpedita" class="btn btn-primary btn-xs pull-right" style="margin-top: -23px;">Avaliação expressa</a>
</g:if>

<div id="divGridExeTotais" class="mt10 "></div>
<fieldset id="gridFichasLcWrapper" class="mt10">
    <legend>Espécies LC</legend>
    %{--Filtro da ficha--}%
    %{--<form class="form form-inline" class="mt10" id="frmFiltroFichaLc">--}%
        %{--<g:render template="/templates/filtrosFicha" model="[js: 'oficina.execucao', listNivelTaxonomico: listNivelTaxonomico, listSituacaoFicha: listSituacaoFicha, outrosCampos:'/oficina/camposFiltroFicha']"></g:render>--}%
    %{--</form>--}%
    <div class="mt10 form form-inline" id="divFiltrosFichasLc"></div>
    %{-- gride para selecionar ficha --}%
    %{--<div id="divGridExeFichaLc" style="overflow-x:hidden;overflow-y:auto;max-height: 500px;"></div>--}%
%{--    <div id="divGridExeFichaLc" style="overflow:hidden;height:auto"></div>--}%
    <div id="containerGridExeFichaLc"
         data-params="sqOficinaSelecionada"
         data-container-filters-id="gridFichasLcWrapper"
         data-height="600px"
         data-field-id="sqFicha"
         data-url="oficina/getGridExecucaoFichaLc"
         data-on-load="">
        <g:render template="divGridExeFichasLc"
                  model="[gridId: 'GridExeFichaLc',listFichas:[]]"></g:render>

    </div>
    <div class="fld hidden">
        <button id="btnImprimirLc" data-action="oficina.execucao.gerarRelatorioLc" data-params="sqOficinaSelecionada|sqOficina" type="button" class="btn btn-xs btn-primary mt5" title="Gerar relatório das fichas LC no formato DOC">Imprimir LC</button>
    </div>



</fieldset>

<fieldset id="gridFichasNaoLcWrapper" class="mt10">
    <legend>Espécies da avaliação</legend>
    %{--Filtro da ficha--}%
    %{--<form class="form form-inline" class="mt10" id="frmFiltroFichaNaoLc">--}%
        %{--<g:render template="/templates/filtrosFicha" model="[js: 'oficina.execucao', listNivelTaxonomico: listNivelTaxonomico, listSituacaoFicha: listSituacaoFicha,outrosCampos:'/oficina/camposFiltroFicha']"></g:render>--}%
    %{--</form>--}%
    <div class="mt10 form form-inline" id="divFiltrosFichasNaoLc"></div>
    %{--<div id="divGridExeFichaNaoLc" style="overflow-x:hidden;overflow-y:auto;max-height: 500px;"></div>--}%

    %{--
    Metodo Antigo
    <div id="divGridExeFichaNaoLc" style="overflow:hidden;height: auto;"></div>
    --}%

    <div id="containerGridExeFichaNaoLc"
         data-params="sqOficinaSelecionada|sqOficina"
         data-container-filters-id="gridFichasNaoLcWrapper"
         data-height="600px"
         data-field-id="sqFicha"
         data-url="oficina/getGridExecucaoFichaNaoLc"
         data-on-load="">
        <g:render template="divGridExeFichasNaoLc"
                  model="[gridId: 'GridExeFichaNaoLc',listFichas:[]]">
        </g:render>
    </div>

</fieldset>

<g:if test="${canModify}">
    <div class="fld panel-footer">
        <form id="frmPeriodoDocDiario" class="form-inline">
            <label for="dtInicio" class="control-label">Gerar documento</label>
            <br>
            %{--<input type="text" id="dtInicio" value="${new Date().format('dd/MM/yyyy')}" class="form-control date fld100 ignoreChange">--}%
            %{--<span>&nbsp;a&nbsp;</span>--}%
            %{--<input type="text" id="dtFim" value="31/01/2017" class="form-control fld100">--}%
            <div style="display:inline">
                <div class="input-group">
                    <input type="text" id="dtInicio" value="${new Date().format('dd/MM/yyyy')}" class="form-control date fld140 ignoreChange">
                    <div class="input-group-btn">
                        <button type="button" data-action="oficina.execucao.gerarDocFinal" data-tipo='diario' data-params="sqOficinaSelecionada|sqOficina" class="fld btn btn-primary btn-sm">Diário</button>
                    </div>
                </div>
                &nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;&nbsp;
                <button type="button" data-action="oficina.execucao.gerarDocFinal" data-params="sqOficinaSelecionada|sqOficina" class="fld btn btn-primary btn-sm">Final</button>
            %{--                &nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;&nbsp;
                <button type="button" id="divTextoEncaminhamentoToggle" class="fld btn btn-primary btn-sm" data-toggle="collapse" data-target="#divTextoEncaminhamento">Editar memória</button>--}%
                <button type="button" id="btnPrint" data-action="oficina.execucao.print" data-params="sqOficinaSelecionada|sqOficina,contexto:oficina,container:tabExecucao,todasPaginas:S" class="fld btn btn-success btn-sm pull-right">Imprimir Fichas</button>
            </div>
        </form>
    </div>

    <div class="panel-body panel-collapse collapse" id="divTextoEncaminhamento">
        <form class="form-inline" name="frmTextoMemoria" id="frmTextoMemoria" role="form">
            <div class="form-group" style="width: 100%">
                <label for="txEncaminhamento" class="control-label">Memória da oficina</label>
                <br>
                <textarea name="txEncaminhamento" id="txEncaminhamento" class="fld form-control fldTextarea" rows="14" style="width: 100%">${oficina?.txEncaminhamento}</textarea>
            </div>
            <div class="fld panel-footer">
            <button type="button" data-action="oficina.execucao.gerarMemoria" data-params="sqOficinaSelecionada|sqOficina" class="fld btn btn-primary">Gerar memória</button>
            </div>
        </form>
    </div>
</g:if>
<g:else>
   <br/>
</g:else>
<div class="form form-inline mt10">
    <div class="fld form-group">
        <div class="input-group">
            <div class="input-group-addon">Link SEI</div>
            <input type="url" class="form-control fld600" name="deLinkSei" id="deLinkSei" placeholder="Url para visualizar o processo no SEI" ${!canModify ?'disabled="true"':''} value="${oficina.deLinkSei?:''}">
            <div class="input-group-btn">
                <g:if test="${ canModify }">
                    <button type="button" data-params="sqOficinaSelecionada|sqOficina" data-action="oficina.execucao.saveLinkSei" class="btn btn-primary btn-sm">Gravar</button>
                </g:if>
                <button type="button" id="btnOficinaAbrirLinkSei" data-action="oficina.execucao.openLinkSei" class="btn btn-link btn-sm" title="Para visualizar a opção janela pop-up deve estar autorizada no navegador!">Ver processo</button>
                <g:if test="${ canModify }">
                    <button type="button" data-action="oficina.execucao.encerrarOficina" data-params="sqOficinaSelecionada|sqOficina" class="fld btn btn-sm btn-danger">Encerrar oficina</button>
                </g:if>
            </div>
        </div>
    </div>

</div>

<fieldset class="mt10">
        <legend>
        Anexar arquivo
        <a data-action="oficina.execucao.openModalSelAnexo"
            data-de-rotulo="Anexo da oficina"
            data-params="sqOficinaSelecionada|sqOficina"
            data-update-grid="divGridAnexoOficina"
            class="fld btn btn-default btn-xs btn-grid-subform"
            title="Adicionar Documento Pdf/Doc/Imagem gerados durante a oficina"><span class="glyphicon glyphicon-plus"></span>
        </a>
    </legend>
    <div id="divGridAnexoOficina">
        %{-- <g:render template="divGridAnexoOficina"></g:render> --}%
    </div>
</fieldset>
