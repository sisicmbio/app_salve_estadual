<!-- view: /views/oficina/_formCadMemoria.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
%{-- Formulário de cadastro da memoria da reuniao preparatoria --}%

<div class="col-sm-12">
    <form id="frmCadMemoria" name="frmCadMemoria" class="form-inline" role="form">
        <input type="hidden" id="sqOficinaMemoria" value="${oficinaMemoria?.id ?:''}">
            <div class="fld form-group">
                <label for="deLocal" class="control-label">Local da reunião</label>
                <br>
                <input name="deLocal" id="deLocal" type="text"
                       value="${oficina ? oficina.deLocal : ''}" class="fld form-control fld300" disabled>
            </div>

            %{-- PERIODO --}%
            <div class="form-group">
                <label for="dePeriodo" class="control-label">Período</label>
                <br/>
                <input value="${oficina?.periodo}" type="text" name="dePeriodo"
                       id="dePeriodo" class="fld form-control fld200" disabled>
            </div>

        <fieldset class="mt15">
            <legend>Encaminhamentos</legend>
            <div class="fld form-group">
                <label for="deLocalOficina" class="control-label">Local da oficina</label>
                <br>
                <input name="deLocalOficina" id="deLocalOficina" type="text"
                    required
                    value="${oficinaMemoria?.deLocalOficina ? oficinaMemoria?.deLocalOficina : ''}" class="fld form-control fld300">
            </div>

            %{-- DATA PROPOSTA --}%
            <div class="form-group">
                <label for="dtInicioOficina" class="control-label">Início</label>
                <br/>
                <input value="${oficinaMemoria?.dtInicioOficina ? oficinaMemoria.dtInicioOficina.format('dd/MM/yyyy') : ''}" type="text" name="dtInicioOficina"
                    required
                       id="dtInicioOficina" class="fld form-control date fldDate">
            </div>
            <div class="form-group">
                <label for="dtFimOficina" class="control-label">Fim</label>
                <br/>
                <input value="${oficinaMemoria?.dtFimOficina ? oficinaMemoria.dtFimOficina.format('dd/MM/yyyy') : ''}" type="text" name="dtFimOficina"
                       required
                       id="dtFimOficina" class="fld form-control date fldDate">
            </div>



            %{-- SALAS --}%
            <div class="form-group">
                <label for="deSalas" class="control-label">Nº de salas</label>
                <br>
                <input value="${oficinaMemoria?.deSalas ? oficinaMemoria.deSalas : ''}"
                       type="text" name="deSalas" id="deSalas" class="fld form-control fld70">
            </div>

            <br>


            %{-- CONVIDADOS --}%
            <div class="mt5" style="background-color: #efefef;padding:5px;display:inline-block;width:580px;">
                <div class="form-group">
                    <label for="txConvidado" class="control-label"
                           title="Para adicionar mais de um participante, informe um por linha, selecione a Função e clique no botão <b>Adicionar participante(s)</b>.<br>Exemplo:<br>Fulano de Tal / fulano@email.com<br>Ciclano de Tal / ciclano@email.com">Adicionar participante(s) da oficina</label>
                    <br/>
                    <textarea name="txConvidado" id="txConvidado"
                              rows="3" style="min-height: 100px;height: 100px;"
                              data-msg-required="Informe o(s) nome(s) do(s) participante(s) da oficina!"
                              class="fld form-control fldTextarea fld350"></textarea>
                </div>
                <div class="form-group">
                    <label for="sqPapel" class="control-label">Função</label>
                    <br>
                    <select name="sqPapel" id="sqPapel" class="fld form-control fld200" style="font-size:12px;font-weight:normal;">
                        <option value="" selected>-- selecione --</option>
                        <g:each var="item" in="${ listPapeis }">
                            <option  value="${item.id}">
                                ${item.descricao}
                            </option>
                        </g:each>
                    </select>
                    <br>
                    <button type="button" class="mt10 btn btn-sm btn-primary cursor-pointer"
                            data-action="oficina.memoria.save(true)"><i class="fa fa-plus mr5"></i>Adicionar participante(s)</button>
                </div>
            </div>

            <br>

            <div class="form-group">
                %{-- gride participantes --}%
                <div id="divGridParticipanteMemoria" style="width:700px;">Carregando...</div>
                <div id="divAcaoLoteMemoria" style="display:none;">
                    <button type="button" class="fld btn btn-sm btn-danger"
                            data-action="oficina.memoria.deleteParticipantes"
                            title="Excluir participante(s) selecionados(s).">Excluir</button>
                </div>
            </div>

            <br>

            <div class="form-group mt10">
                <label for="txMemoria" class="control-label">Outros comentários</label>
                <br/>
                <g:if test="${params.canModify}">
                    <textarea name="txMemoria" id="txMemoria"
                              class="fld form-control fldTextarea wysiwyg w100p">${oficinaMemoria?.txMemoria ? oficinaMemoria.txMemoria : ''}</textarea>
                </g:if>
                <g:else>
                    <div class="text-justify" style="min-height: 28px;max-height: 300px;overflow-y: auto;padding:3px;border:1px solid silver;background-color: #eeeeee">${oficinaMemoria?.txMemoria ? oficinaMemoria.txMemoria : ''}</div>
                </g:else>
            </div>

            %{-- botão --}%
                <div class="fld panel-footer">
                    <g:if test="${params.canModify}">
                        <button data-action="oficina.memoria.save(false)" class="fld btn btn-success">Gravar encaminhamentos</button>
                    </g:if>
                    <button data-action="oficina.memoria.gerarDocMemoria" class="fld btn btn-primary">Gerar Documento</button>
                </div>

        </fieldset>
    </form>
</div>

<div class="col-sm-12" style="display:none;" id="divContainerGridAnexoOficinaMemoria">
    <fieldset class="mt10">
        <legend>
            Anexar arquivo
            <a data-action="oficina.memoria.openModalSelAnexo"
               data-de-rotulo="Anexo da reunião preparatória"
               data-params="sqOficinaSelecionada|sqOficina,sqOficinaMemoria"
               data-update-grid="divGridAnexoOficinaMemoria"
               class="fld btn btn-default btn-xs btn-grid-subform"
               title="Adicionar pdf/doc/xls/imagem gerados durante a reunião preparatória"><span class="glyphicon glyphicon-plus"></span>
            </a>
        </legend>
        <div id="divGridAnexoOficinaMemoria"></div>
    </fieldset>
</div>

<div class="col-sm-12">
    <div class="end-page"></div>
</div>
