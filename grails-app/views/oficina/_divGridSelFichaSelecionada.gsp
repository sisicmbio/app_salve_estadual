<!-- view: /views/oficina/_divGridSelFichaSelecionada -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<g:if test="${pagination && pagination.pages > 1 }">
    <div style="display:block"
         data-sq-ciclo-avaliacao="${params.sqCicloAvaliacao}"
         data-sq-oficina="${params.sqOficina}"
         data-grid-hash="${pagination.hash}"
         data-grid-total-rows="${pagination.totalRows}"
         data-grid-page-size="${pagination.pageSize}"
         data-grid-controller="${params?.controller}"
         data-grid-action="${params?.action}"
         data-grid-on-page-change="oficina.ficha.gride2PageChange"
         data-grid-container-id="divTbOficinaSelSelecionadas">
        <button class="button btn-xs btn-link btn-secondary btn-pagination-prev"
                title="Página anterior"
                onclick="loadPaginateArrayPage(this)"
                data-grid-page="${pagination.page-1}"
        ><i class="fa fa-chevron-left"></i></button>
        <select class="ml5 mr5 fld50 sel-pagination-pages" id="selPagination2"
                onchange="loadPaginateArrayPage(this)">
            <g:each var="pageNum" in="${(1..pagination.pages)}">
                <option value="${pageNum}" ${(pagination.page.toInteger() == pageNum.toInteger() ? 'selected' : '' ) }>${pageNum}</option>
            </g:each>
        </select>
        <button class="button btn-xs btn-link btn-secondary btn-pagination-next"
                title="Página seguinte"
                onclick="loadPaginateArrayPage(this)"
                data-grid-page="${pagination.page+1}"
        ><i class="fa fa-chevron-right"></i></button>
        <span>${pagination.totalRows} fichas</span>
    </div>
</g:if>

<div style="display:block;overflow-y:auto;min-height:300px;max-height:700px;padding:5px" id="divTbOficinaSelSelecionadas">
    <h4>${listFichas ? listFichas.size() : '0'}&nbsp;Fichas / <span class="total-selecionadas">0 selecionada.</span></h4>
    <table class="table table-hover table-striped table-condensed table-bordered" id="tbOficinaSelSelecionadas">
        <thead>
        <tr>
            <th style="width:45px;">#</th>
            <g:if test="${canModify}">
                <th style="width:30px;"><i title="Marcar/Desmarcar Todos" data-action="oficina.checkUncheckAll" data-container="divTbOficinaSelSelecionadas" class="glyphicon glyphicon-unchecked"></i></th>
            </g:if>
            <th style="width:auto;">Nome Científico</th>
            <th style="width:150px;" class="select-filter">Agrupamento</th>
            <th style="width:200px;" class="select-filter">Grupo<br>Taxonômico</th>
            <th style="width:150px;" class="select-filter">Situação<br>oficina</th>
            %{-- <th style="width:20px;">Ação</th> --}%
        </tr>
        </thead>
        <tbody>
        <g:if test="${ listFichas }">
            <g:each var="item" in="${ listFichas }" status="i">
                <tr>
                    <td class="text-center">${i + 1 + ( params?.gridRowNum ? params.gridRowNum.toInteger() : 0 )}</td>
                    <g:if test="${canModify}">
                        <td class="text-center">
                            <input id="sqOficinaFicha_${item.sq_oficina_ficha}"
                                   name="sqOficinaFicha[${item.sq_oficina_ficha}]"
                                   value="${item.sq_oficina_ficha}" onChange="oficina.ficha.chkFichaSelecionadaChange(this)"
                                   type="checkbox" class="checkbox-lg ignoreChange"
                                   data-action="contarChecked" data-container="divTbOficinaSelSelecionadas"/>
                        </td>
                    </g:if>
                    <td>${raw(br.gov.icmbio.Util.ncItalico(item.nm_cientifico,true,true,item.co_nivel_taxonomico) )}</td>
                    <td class="text-center">${item.ds_agrupamento}</td>
                    <td class="text-center">${item.ds_grupo}</td>
                    <td class="text-center">${item.st_transferida_oficina_ficha ? 'Transferida' : item.ds_situacao_oficina_ficha }</td>
                </tr>
            </g:each>
        </g:if>
        <g:else>
            <tr><td colspan="4" class="text-center">
                <h4>Nenhuma ficha selecionada</h4>
            </td>
            </tr>
        </g:else>
        </tbody>
    </table>
</div>

