<!-- view: /views/oficina/_divGridSelPapel.gsp-->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<table class="table table-hover table-striped table-condensed table-bordered">
    <thead>
        <th style="width:30px;"><i class="glyphicon glyphicon-ok-circle"></i></th>
        <th>Papel</th>
    </thead>
    <tbody>
    <g:if test="${listPapeis}">
        <g:each var="item" in="${listPapeis}">
            <tr>
                <td class="text-center"><input name="sqPapelParticipante" value="${item.id}"type="radio"/></td>
                <td>${item.descricao}</td>
            </tr>
        </g:each>
    </g:if>
    <g:else>
        <tr>
            <td colspan="2" align="center">
                <h4>Nenhum Papel Cadastrado!</h4>
            </td>
        </tr>
    </g:else>

    </tbody>
</table>