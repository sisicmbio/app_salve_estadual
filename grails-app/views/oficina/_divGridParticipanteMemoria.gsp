<!-- view: /views/oficina/_divGridParticipanteMemoria.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<table style="max-width:600px;" class="table table-hover table-striped table-condensed table-bordered" id="tableOficinaParticipantesMemoria">
    <thead>
    <th style="width:40px;">#</th>
    <g:if test="${canModify}">
        <th style="width:40px;"><i title="Marcar/Desmarcar Todos" data-action="oficina.checkUncheckAll" class="glyphicon glyphicon-unchecked"></i></th>
    </g:if>
    <th style="width:200px;">Nome</th>
    <th style="width:200px;">Função</th>
    </thead>
    <tbody>
    <g:if test="${ listParticipantes }">
        <g:each var="item" in="${listParticipantes}" status="i">
            <tr id="tr-grid-particiante-${item.id}">
                <td class="text-center">${i+1}</td>
                <g:if test="${params.canModify}">
                    <td class="text-center">
                        <input id="sqOficinaParticipante_${item.id}" name="sqOficinaParticipante[${item.id}]" onChange="oficina.memoria.chkDelParticipanteMemoriaChange(this)" value="${item.id}" type="checkbox" class="checkbox-lg ignoreChange"/>
                    </td>
                </g:if>
                <td>
                     <span id="span-edit-nome-participante-${item.id}">
                       <span id="span-edit-nome-participante-text-${item.id}"
                             class="content">${br.gov.icmbio.Util.capitalize( item?.noParticipante )}</span>
                            <i onClick="oficina.editNomeParticipanteMemoriaReuniao(${item.id})" class="fa fa-edit ml10 cursor-pointer" title="Alterar nome"></i></span>
                </td>
                <td class="text-left" id="td-funcao-participante-${item.id}">
%{--                    ${item?.papel?.descricao}--}%
                    <span id="span-edit-funcao-participante-${item.id}">
                        <span id="span-edit-funcao-participante-text-${item.id}"
                              class="content">${item?.papel?.descricao}</span>
                        <i onClick="oficina.editFuncaoParticipanteMemoriaReuniao(${item.id},${item?.papel?.id},'${br.gov.icmbio.Util.capitalize( item?.noParticipante )}')" class="fa fa-edit ml10 cursor-pointer" title="Alterar funcao"></i></span>
                </td>
             </tr>
        </g:each>
    </g:if>
    <g:else>
        <tr>
            <td colspan="4"><h4>Nenhum participante cadastrado.</h4></td>
        </tr>
    </g:else>
        </tbody>
</table>
