<!-- view: /views/oficina/_divGridPapelParticipanteFicha.gsp-->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<table class="table table-hover table-striped table-condensed table-bordered" id="tableOficinaPapelParticipantes">
    <thead>
        <th style="width:400px;">Nome</th>
        <th style="width:200px;">Função</th>
        <g:if test="${oficina.tipoOficina.codigoSistema != 'OFICINA_VALIDACAO'}">
            <th style="width:*;">Fichas</th>
        </g:if>
        <th style="width:150px;">Assinatura<br> Doc. Final</th>
        <g:if test="${canModify}">
            <th style="width:80px;">Ação</th>
        </g:if>
    </thead>
    <tbody>
    %{--<g:if test="${dadosGride}">--}%
        %{--<g:each var="item" in="${dadosGride}">--}%
            %{--<g:each var="papel" in="${item.value}">--}%
                %{--${item.key}<br/>--}%
                %{--${papel.key}<br>--}%
                %{--${papel.value[0]}--}%
                %{--<hr>--}%

            %{--</g:each>--}%
        %{--</g:each>--}%
    %{--</g:if>--}%
    %{----}%

    <g:if test="${listOficinaParticipantes}">
        <g:each in="${ listOficinaParticipantes }" var="item" status="i">
            <tr id="tr-grid-particiante-ficha-${item.id}">
                <td>${br.gov.icmbio.Util.capitalize(item.noPessoa)}</td>
                <td class="text-center">${item.dsPapel}</td>
                <g:if test="${oficina.tipoOficina.codigoSistema != 'OFICINA_VALIDACAO'}">
                    <td class="text-left">
                        <fieldset>
                            <legend style="font-size:1.2em;">
                                <a data-toggle="collapse" href="#divFichas-${i}"
                                   data-div-id="divFichas-${i}"
                                   data-sq-oficina-participante="${item.id}"
                                   data-sq-papel="${item.sqPapel}"
                                   class="tooltipstered" style="cursor: pointer; opacity: 1;" title="Clique para mostrar/esconder as fichas"
                                   data-action="oficina.papel.getFichasParticipante">
                                   ${item.nuFicha}&nbsp;Fichas
                                </a>
                            </legend>
                            <div id="divFichas-${i}" class="panel-collapse collapse" style="max-height:300px;overflow:auto;text-align: justify; padding:5px;" role="tabpanel" style=""></div>
                        </fieldset>
                    </td>
                </g:if>

                %{-- ASSINATURA DOC FINAL--}%
                <td class="text-center">
                    <g:if test="${item.dtAssinatura}">
                        <span style="font-size:0.8rem !important;">Assinado em<br>${item.dtAssinatura.format('dd/MM/yyyy HH:mm:ss')}</span>
                    </g:if>
                    <g:else>
                        <g:if test="${ hasDocFinal }">

                            <button type="button" data-sq-oficina-participante="${item.id}" data-hash="${item.deHashAssinatura}"
                                    data-action="oficina.participante.enviarEmailAssinaturaEletronica"
                                    class="btn btn-xs${ item.dtEmailAssinatura ? ' btn-primary':' btn-default'}"
                                    title="Enviar email solicitando a assinatura eletrônica. ${ ( item.dtEmailAssinatura ? 'Último envio ' + item.dtEmailAssinatura.format('dd/MM/yyyy HH:mm:ss') : 'Email não enviado.' )}"><i class="fa fa-share" aria-hidden="true"></i></button>
                        </g:if>
                        <g:else>
                            <span>Não emitido</span>
                        </g:else>
                    </g:else>
                </td>


                <g:if test="${canModify}">
                    <td class="td-actions">
                        <a data-action="oficina.papel.edit" data-sq-papel-participante="${item.sqPapel}" data-id="${item.id}" class="fld btn btn-default btn-xs btn-update" title="Alterar">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </a>
                        <a data-action="oficina.papel.delete" data-descricao="${item.noPessoa+' como '+item.dsPapel}" data-sq-papel-participante="${item.sqPapel}" data-id="${item.id}" class="fld btn btn-default btn-xs btn-delete" title="Excluir">
                            <span class="glyphicon glyphicon-remove"></span>
                        </a>
                    </td>
                </g:if>
            </tr>

        </g:each>
    </g:if>



    %{--<a
    <g:if test="${listOficinaParticipantes}">
        <g:each in="${ listOficinaParticipantes }" var="item" status="i">
            <g:each var="sub" in="${item.papeisFichas}">
                <tr>
                    <td>${item.pessoa.noPessoa}</td>
                    <td>${sub.papel.descricao}</td>
                    <td>${raw(sub.fichas.noCientificoItalico.join(', '))}</td>
                    <g:if test="${canModify}">
                        <td class="td-actions">
                            <a data-action="oficina.papel.edit" data-sq-papel-participante="${sub.papel.id}" data-id="${item.id}" class="fld btn btn-default btn-xs btn-update" title="Alterar">
                                <span class="glyphicon glyphicon-pencil"></span>
                            </a>
                            <a data-action="oficina.papel.delete" data-descricao="${item.pessoa.noPessoa+' como '+sub.papel.descricao}" data-sq-papel-participante="${sub.papel.id}" data-id="${item.id}" class="fld btn btn-default btn-xs btn-delete" title="Excluir">
                                <span class="glyphicon glyphicon-remove"></span>
                            </a>
                        </td>
                    </g:if>
                </tr>
            </g:each>
        </g:each>
    </g:if>
    --}%
    <g:else>
        <g:if test="${mapColaboradoresAmpla.size() == 0}">
        <tr>
            <td colspan="5" align="center">
                <h4>Nenhum registro encontrado!</h4>
            </td>
        </tr>
        </g:if>
    </g:else>


        <g:set var="colspan" value="${oficina.tipoOficina.codigoSistema != 'OFICINA_VALIDACAO' ? 5 : 3}"></g:set>
        <g:if test="${ mapColaboradoresDireta.size()+mapColaboradoresAmpla.size() > 0}">
            <tr style="background-color:#f1e670;">
                <td colspan="${colspan}">
                    <b>Colaborador${mapColaboradoresAmpla.size()>1?'es':''} consulta ampla / direta</b>
                </td>
            </tr>
            %{--<tr style="background-color:#f1e670;"><td colspan="4"><b>Colaborador${mapColaboradoresDireta.size()>1?'es':''} Consulta Direta</b></td></tr>--}%
            %{-- <g:each var="item" in="${mapColaboradoresDireta}"> --}%
                %{-- <tr> --}%
                    %{-- <td colspan="2">${item.key}</td> --}%
                    %{-- <td>Colaborador</td> --}%
                    %{-- <td colspan="2">${raw(item.value.join(', '))}</td> --}%
                %{-- </tr> --}%
            %{-- </g:each> --}%

            <g:each var="item" in="${mapColaboradoresAmpla}">
                <tr>
                    <td colspan="${colspan==5?2:0}">${item.key}</td>
                    %{-- <td>Colaborador</td> --}%
                    <td colspan="3">${raw(item.value.join(', '))}</td>
                </tr>
            </g:each>
        </g:if>
    </tbody>
</table>
