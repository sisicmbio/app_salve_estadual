<!-- view: /views/oficina/_formCadParticipante.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

%{-- Formulário de cadastro de participantes em uma oficina --}%

%{--<legend class="nome-oficina-selecionada">&nbsp;</legend>--}%
<form id="frmCadParticipante" name="frmCadParticipante" class="form-inline" role="form">
  <g:if test="${canModify}">
    <fieldset class="mt10">
        <legend style="font-size:1.2em;">
          <a  id="btnShowFormCadParticipante"
              data-action="app.showHideContainer"
              data-target="acordeon_frmCadParticipante"
              data-focus="sqParticipante"
              data-callback=""
              title="Mostrar/Esconder formulário!">
              <i class="fa fa-plus green"/>&nbsp;Cadastrar</a>
        </legend>
        <div id="acordeon_frmCadParticipante" class="hidden" style="background-color:#ffffff;padding:5px;">
          <div class="fld form-group">
            <label class="control-label">Nome</label>
            <br>
            <select name="sqParticipante" class="form-control fld400 select2" data-change="oficina.participante.sqParticipanteChange" style="font-size:12px;font-weight:normal;" required="true"
              data-s2-url="ficha/select2PessoaFisica"
              data-s2-placeholder="?"
              data-s2-minimum-input-length="3"
              data-s2-auto-height="true">
            </select>
          </div>

          <div class="fld form-group">
            <label for="deEmail" class="control-label">Email</label>
            <br>
            <input name="deEmail" id="deEmail" type="text" value="" class="fld form-control fld300"  required="true">
          </div>

          <div class="fld form-group">
            <label for="sqInstituicao" class="control-label">Instituição</label>
            <br>
              <select name="sqInstituicao" id="sqInstituicao" class="fld form-control fld600 select2" style="font-size:12px;font-weight:normal;"
                      required="true"
                      data-msg-required="Instituição obrigatória"
                      data-s2-url="main/select2Instituicao"
                      data-s2-placeholder="?"
                      data-s2-minimum-input-length="3"
                      data-s2-auto-height="true">
              </select>
          </div>
          <br />

          %{-- botão --}%
          <div class="fld panel-footer">
            <button data-action="oficina.participante.save" class="fld btn btn-success">Gravar participante</button>
            <button data-action="oficina.participante.showModalImportarParticipante"
                    class="fld btn btn-primary"
                    data-sq-oficina="${oficina.id}"
                    title="Importar o(s) participante(s) das consultas diretas e revisões">Importar participantes</button>
          </div>
      </div>
  </fieldset>
  </g:if>
  %{-- gride para selecionar ficha --}%
  <div class="fld form-group">
    <div id="divGridParticipante"></div>
  </div>
  <g:if test="${canModify}">
    %{-- botão --}%
    <div class="fld panel-footer text-right" id="div-btn-delete" style="display:none">
      <button data-action="oficina.participante.delete" class="fld btn btn-danger btn-xs">Excluir Selecionados</button>
    </div>
  </g:if>
</form>

<g:if test="${canModify}">
  <fieldset id="fieldset_email" style="display:none">
      <legend style="font-size:1.2em;">
          <a data-toggle="collapse" data-parent="#acordeon_frmCadParticipanteEmail" href="#acordeon_frmCadParticipanteEmail">
              <i class="glyphicon glyphicon-envelope"/>&nbsp;Elaborar Email</a>
      </legend>
      <div id="acordeon_frmCadParticipanteEmail" class="panel-collapse collapse" role="tabpanel" style="background-color:#ffffff;padding:5px;">
        <form id="frmCadParticipanteEnviarEmail" name="frmCadParticipanteEnviarEmail" class="form-inline" role="form" enctype='multipart/form-data'>
            <div class="form-group" style="width: 100%">
                <label for="deAssuntoEmail" class="control-label">Assunto&nbsp;<i class="glyphicon glyphicon-save" title="Recuperar texto do último envio!" onClick="oficina.participante.getTextLastEmail()"></i></label>
                <br>
                <input type="text"  name="deAssuntoEmail" id="deAssuntoEmail" class="fld form-control fld400" value="" required="true"></input>
            </div>

            <div class="form-group" style="width: 100%">
              <label for="txEmail" class="control-label">Mensagem<span style="margin:0px 10px;font-size:12px;color:gray;"> Variáveis: <a title="inserir" href="#" onClick="app.insertTextEditor(this)">{nome}</a>, <a title="inserir" href="#" onClick="app.insertTextEditor(this)">{oficina}</a>, <a title="inserir" href="#" onClick="app.insertTextEditor(this)">{periodo}</a></span>
              </label>
              <br>
              <textarea name="txEmail" id="txEmail" class="fld form-control fldTextarea" rows="3"  style="height:200px;width:100%" required="true"></textarea>
            </div>

            <div class="form-group" style="widthx: 100%">
              <label for="deEmailCopia" class="control-label" title="Para enviar cópia para mais de um email, utilize a vírgula como separador.">Com cópia</label>
              <br>
              <input type="text" name="deEmailCopia" id="deEmailCopia" class="fld form-control fld600" value="${emailPadrao}">
            </div>

            <br />

            <div class="form-group" style="widthx: 100%">
              <label for="fldAnexo" class="control-label">Anexar Arquivo</label>
              <br>
              %{--<input type="file" name="fldAnexo" id="fldAnexo" class="fld form-control w100">--}%
                <input name="fldAnexo" type="file" id="fldAnexo" class="file-loading" accept="*"
                       data-show-preview="true"
                       data-show-upload="false"
                       data-show-caption="false"
                       data-show-remove="true"
                       data-max-file-count="1"
                       data-allowed-file-extensions='["jpg", "jpeg", "png","tiff","tif","zip","pdf","doc","docx","xls","xlsx","odt","ods","txt","rtf"]'
                       data-main-class="input-group-sm"
                       data-max-file-size="20000"
                       data-preview-file-type="object">
            </div>

            <div class="fld panel-footer">
              <button data-action="oficina.participante.sendMail" class="fld btn btn-primary">Enviar Email</button>
            </div>
      </form>
    </div>
  </fieldset>
</g:if>
<div class="end-page"></div>
