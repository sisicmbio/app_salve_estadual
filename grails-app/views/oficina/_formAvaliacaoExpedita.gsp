<!-- view: /views/oficina/_formAvaliacaoExpedita.gsp-->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<g:set var="oSND" value="${[[key: 'S', value: 'Sim'], [key: 'N', value: 'Não'], [key: 'D', value: 'Desconhecido']]}"/>
        <div style="padding:5px 5px" class="text-center" id="div-avaliacao-expedita-${params.sqOficina}">
            <input type="hidden" id="categoriaLC" data-descricao="${categoriaLC.descricaoCompleta}" value="${categoriaLC.id}">
            <input type="hidden" id="categoriaDD" data-descricao="${categoriaDD.descricaoCompleta}" value="${categoriaDD.id}">
            <table class="table table-hover table-striped table-condensed table-bordered" style="font-size:15px !important; ">
                <thead>
                <th>Nome Científico</th>
                <th style="white-space:normal !important;">É favorecido ou indiferente à conversão de habitats/perturbação?</th>
                <th style="white-space:normal !important;">Pelo menos ¾ dos registros do táxon estão em áreas amplas e íntegras >45.000km²?</th>
                <th style="white-space:normal !important;">A extensão de ocorrência é ampla (>45.000km²) ?</th>
                <th style="white-space:normal !important;">É frequente em inventários nos últimos dez anos ou três gerações?</th>
                <th style="white-space:normal !important;">Resultado</th>
                <g:if test="${ canModify }">
                   <th>Ação</th>
                </g:if>
                </thead>
                <tbody id="body-avaliacao-expedita-${params.sqOficina}">
                <g:if test="${listFichas}">
                    <g:each var="ficha" in="${listFichas}" status="i" >
                        <tr id="tr-${ficha.id}">
                            <td class="text-left bold" style="font-size:1.5rem;color:#000;min-width: 200px;">${raw(ficha.noCientificoItalico)}</td>
                            <td class="text-center">
                                <g:if test="${ canModify }">
                                <select id="stFavorecidoConversaoHabitats-${ficha.id}" data-change="oficina.execucao.stChangeExpedita" data-id="${ficha.id}" data-sq-oficina="${params.sqOficina}" class="form-control fldSelect">
                                    <option value="">?</option>
                                    <g:each var="item" in="${oSND}">
                                        <option value="${item.key}" ${ficha?.stFavorecidoConversaoHabitats == item.key ? ' selected' : ''}>${item.value}</option>
                                    </g:each>
                                </select>
                                </g:if>
                                <g:else>
                                    ${ficha.snd(ficha?.stFavorecidoConversaoHabitats)}
                                </g:else>
                            </td>
                            <td class="text-center">
                                <g:if test="${ canModify }">
                                <select id="stTemRegistroAreasAmplas-${ficha.id}" data-change="oficina.execucao.stChangeExpedita" data-id="${ficha.id}" data-sq-oficina="${params.sqOficina}" class="form-control fldSelect ${ficha?.stTemRegistroAreasAmplas ? '' : 'hidden'}">
                                    <option value="">?</option>
                                    <g:each var="item" in="${oSND}">
                                        <option value="${item.key}" ${ficha?.stTemRegistroAreasAmplas == item.key ? ' selected' : ''}>${item.value}</option>
                                    </g:each>
                                </select>
                              </g:if>
                               <g:else>
                                   ${ficha.snd(ficha?.stTemRegistroAreasAmplas)}
                               </g:else>
                            </td>
                            <td class="text-center">
                                <g:if test="${ canModify }">
                                <select id="stPossuiAmplaDistGeografica-${ficha.id}" data-change="oficina.execucao.stChangeExpedita" data-id="${ficha.id}" data-sq-oficina="${params.sqOficina}" class="form-control fldSelect ${ficha?.stPossuiAmplaDistGeografica ? '' : 'hidden'}">
                                    <option value="">?</option>
                                    <g:each var="item" in="${oSND}">
                                        <option value="${item.key}" ${ficha?.stPossuiAmplaDistGeografica == item.key ? ' selected' : ''}>${item.value}</option>
                                    </g:each>
                                </select>
                                </g:if>
                                <g:else>
                                    ${ficha.snd(ficha?.stPossuiAmplaDistGeografica)}
                                </g:else>  
                            </td>

                            <td class="text-center">
                              <g:if test="${ canModify }">
                                <select id="stFrequenteInventarioEoo-${ficha.id}" data-change="oficina.execucao.stChangeExpedita" data-id="${ficha.id}" data-sq-oficina="${params.sqOficina}" class="form-control fldSelect ${ficha?.stFrequenteInventarioEoo ? '' : 'hidden'}">
                                    <option value="">?</option>
                                    <g:each var="item" in="${oSND}">
                                        <option value="${item.key}" ${ficha?.stFrequenteInventarioEoo == item.key ? ' selected' : ''}>${item.value}</option>
                                    </g:each>
                                </select>
                                </g:if>
                                <g:else>
                                    ${ficha.snd(ficha?.stFrequenteInventarioEoo)}
                                </g:else>
  
                            </td>

                            %{--RESULTADO--}%
                            <td class="text-center bold" id="td-resultado-${ficha.id}">&nbsp;</td>

                            <g:if test="${ canModify }">
                                %{--AÇÃO--}%
                                <td class="td-actions">
                                    <a data-action="oficina.execucao.saveAvaliacaoExpedita" id="btn-save-avaliacao-expedita-${ficha.id}" data-descricao="${ficha.nmCientifico}" data-sq-oficina="${params.sqOficina}" data-id="${ficha.id}" class="fld btn btn-default btn-xs disabled" title="Gravar Avaliação">
                                        Gravar
                                    </a>
                                    <input type="hidden" id="sqCategoriaIucn-${ficha.id}" value=""/>
                                </td>
                            </g:if>
                        </tr>
                    </g:each>
                </g:if>
                <g:else>
                    <tr>
                        <td colspan="2" align="center">
                            <h4>Nenhum Papel Cadastrado!</h4>
                        </td>
                    </tr>
                </g:else>

                </tbody>
            </table>
        </div>