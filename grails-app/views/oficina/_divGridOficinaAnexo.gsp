<!-- view: /views/oficina/_divGridOficinaAnexo.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>Arquivo</th>
        <th>Legenda</th>
        <th>Ação</th>
    </tr>
    </thead>
    <tbody>
    <g:each var="item" in="${listOficinaAnexo}">
        <tr>
            <td class="text-left" style="width:200px;min-width:200px;">
               <g:if test="${item.de_tipo_conteudo.indexOf('image')==0 && item.de_tipo_conteudo.indexOf('tiff') ==-1 }">
                   <div class="text-center w100p">
                        <img src="/salve-estadual/oficina/getAnexo/${item.sq_oficina_anexo}/thumb?hash=${item?.de_local_arquivo?.substring(0,10)}" data-id="${item.sq_oficina_anexo}" alt="${item.no_arquivo}" class="img-rounded img-preview-grid"/>
                   </div>
                </g:if>
                <g:else>
                    <span class="nowrap">

                        <g:if test="${item.in_doc_final}">
                            %{--<a class="fld cursor-pointer" title="Enviar email para os participantes!" data-sq-oficina="${item.sq_oficina}" data-action="oficina.execucao.enviarEmailAssinaturaEletronica" href="javascript:void(0);">
                                <i class="fa fa-envelope-o" aria-hidden="true"></i>
                            </a>--}%
                            &nbsp;<span style="color:blue;">${item.no_arquivo}</span>&nbsp;${' ('+item.nu_assinaturas + ' assinatura' + (item.nu_assinaturas<2?'':'s')  +')'}
                        </g:if>
                        <g:else>
                            ${item.no_arquivo}
                        </g:else>

                        %{--
                        <a class="fld btn-download cursor-pointer" target="_blank" title="Baixar arquivo!" href="/salve-estadual/oficina/getAnexo/${item.sq_oficina_anexo}">
                            <i class="${item.in_doc_final ? 'fa fa-file-pdf-o' : 'fa fa-folder'}" aria-hidden="true"></i>
                        </a>&nbsp;
                        &nbsp;
                        <g:if test="${item.in_doc_final}">
                            <a class="fld cursor-pointer" title="Enviar email para os participantes!" data-sq-oficina="${item.sq_oficina}" data-action="oficina.execucao.enviarEmailAssinaturaEletronica" href="javascript:void(0);">
                                <i class="fa fa-envelope-o" aria-hidden="true"></i>
                            </a>
                            &nbsp;<span style="color:blue;">${item.no_arquivo}</span>&nbsp;${' ('+item.nu_assinaturas + ' assinatura' + (item.nu_assinaturas<2?'':'s')  +')'}
                        </g:if>
                        <g:else>
                            ${item.no_arquivo}
                        </g:else>--}%
                    </span>
                </g:else>
            </td>
            <td style="width:*">${item.de_legenda}</td>
            <td style="width:30px;text-align: right !important;" class="td-actions">
                <g:if test="${item.in_doc_final}">
                    <button class="fld btn btn-default btn-xs" title="Enviar email para os participantes assinarem eletrônicamente o documento.!" data-sq-oficina="${item.sq_oficina}" data-action="oficina.execucao.enviarEmailAssinaturaEletronica">
                        <i class="fa fa-envelope-o blue" aria-hidden="true"></i>
                    </button>
                </g:if>
                <a class="fld btn btn-default btn-xs btn-download" target="_blank" title="Baixar arquivo!" href="/salve-estadual/oficina/getAnexo/${item.sq_oficina_anexo}">
                    <span class="glyphicon glyphicon-download"></span>
                </a>
                <a data-action="oficina.execucao.deleteAnexo" data-descricao="${item.no_arquivo}" data-in-doc-final="${( item.in_doc_final ? 'S':'N' ) }" data-nu-assinaturas="${item.nu_assinaturas}" data-update-grid="divGridAmeacaAnexo" data-id="${item.sq_oficina_anexo}" class="fld btn btn-default btn-xs btn-delete" title="Excluir">
                    <span class="glyphicon glyphicon-remove"></span>
                </a>
            </td>
        </tr>
    </g:each>
    </tbody>
</table>
