<div class="modal" tabindex="-1" id="modalOficina" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4>Cadastro de Oficina</h4>
            </div>
            <div class="modal-body" id="modalOficinaBody">

                %{-- Controle de Abas --}%
                <ul class="nav nav-tabs" role="tablist" id="ulTabsOficina">
                    <li role="presentation" class="active"><a href="#tab1" aria-controls="Oficina" role="tab" data-toggle="tab">Oficina</a></li>
                    <li role="presentation"><a href="#tab3" data-action="tab3Click" aria-controls="Fichas" role="tab" data-toggle="tab">Fichas</a></li>
                    <li role="presentation"><a href="#tab2" data-action="tab2Click" data-url="oficina/participanteForm" data-container="tab2" data-reload="false" aria-controls="Participantes" role="tab" data-toggle="tab">Participantes</a></li>
                    <li role="presentation"><a href="#tab4" data-action="tab4Click" aria-controls="ParticipantesxFichas" role="tab" data-toggle="tab">Participantes X Fichas</a></li>
                </ul>

                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="tab1">
                        <form class="form-horizontal" tabindex="0" name="formOficina" id="formOficina" role="form">
                            <input type="hidden" id="id" name="id" value="${data?.id}"/>
                            %{-- Ciclo --}%
                            <div class="form-group">
                                <label for="nome" class="col-sm-3 control-label">
                                    Ciclo de Avaliação:
                                </label>
                                <div class="col-sm-9">
                                    <p><b>${cicloAvaliacao.deCicloAvaliacao}</b></p>
                                    <p><b>Período: ${cicloAvaliacao.nuAno+ ' a '+(cicloAvaliacao.nuAno+5)}</b></p>
                                </div>
                            </div>
                            %{-- Tipo de Oficina --}%
                            <div class="form-group">
                                <label for="nome" class="col-sm-3 control-label">
                                    Tipo de Oficina:
                                </label>
                                <div class="col-sm-9">
                                    <select name="sqTipoOficina" id="sqTipoOficina" class="form-control" tabindex="1" required="true" data-msg-required="Campo obrigatório">
                                        <option value="">
                                            -- selecione --
                                        </option>
                                        <g:each in="${tiposOficina}" var="tipo">
                                        <option ${tipo.id == data?.tipoOficina?.id?'selected':''} value="${tipo.id}">
                                            ${tipo.descricao}
                                        </option>
                                        </g:each>
                                    </select>
                                </div>
                            </div>
                            %{-- Unidade Organizacional --}%
                            <div class="form-group">
                                <label for="nome" class="col-sm-3 control-label">
                                    Unidade Organizacional:
                                </label>
                                <div class="col-sm-9">
                                    <select name="sqUnidade" id="sqUnidade" class="form-control" tabindex="1" required="true" data-msg-required="Campo obrigatório">
                                        <option value="">
                                            -- selecione --
                                        </option>
                                        <g:each in="${uorgs}" var="uorg">
                                        <option ${uorg.id == data?.unidade?.id?'selected':''} value="${uorg.id}">
                                            ${uorg.sgUnidade}
                                        </option>
                                        </g:each>
                                    </select>
                                </div>
                            </div>
                            %{-- nome --}%
                            <div class="form-group">
                                <label for="nome" class="col-sm-3 control-label">
                                    Nome da Oficina:
                                </label>
                                <div class="col-sm-9">
                                    <input value="Primeira Oficina" type="text" name="noOficina" id="noOficina" class="form-control" tabindex="1" maxlength="200" required="true" data-msg-required="Campo obrigatório"/>
                                </div>
                            </div>
                            %{-- sigla --}%
                            <div class="form-group">
                                <label for="nome" class="col-sm-3 control-label">
                                    Sigla:
                                </label>
                                <div class="col-sm-9">
                                    <input value="Oficina/2016" type="text" name="sgOficina" id="sgOficina" class="form-control" tabindex="2" maxlength="100" required="true" data-msg-required="Campo obrigatório"/>
                                </div>
                            </div>
                            %{-- local --}%
                            <div class="form-group">
                                <label for="nome" class="col-sm-3 control-label">
                                    Local:
                                </label>
                                <div class="col-sm-9">
                                    <input value="Sede do ICMBIO" type="text" name="deLocal" id="deLocal" class="form-control" tabindex="3" maxlength="200" required="true" data-msg-required="Campo obrigatório"/>
                                </div>
                            </div>
                            %{-- periodo --}%
                            <div class="form-group">
                                <label for="nome" class="col-sm-3 control-label">
                                    Período:
                                </label>
                                <div class="col-sm-4">
                                    <input value="2016-01-01" type="date" name="dtInicio" id="dtInicio" class="form-control fldData" tabindex="4" maxlength="10" required="true" data-msg-required="Campo obrigatório"/>
                                </div>
                                <label class="col-sm-1 subLabel">Ate:</label>
                                <div class="col-sm-4">
                                    <input value="2016-03-31" type="date" name="dtFim" id="dtFim" class="form-control fldData" tabindex="5" maxlength="10" required="true" data-msg-required="Campo obrigatório"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xm-12 col-sm-9 col-sm-offset-3" style="text-align:left;">
                                    <a class="btn btn-success btn-sm" data-action="oficina.save" id="btnSave" tabindex="6">Gravar</a>
                                </div>
                            </div>
                        </form>
                    </div>

                    %{-- Aba dos participantes --}%
                    <div role="tabpanel" class="tab-pane" id="tab2">

                    </div> %{-- fim da aba de participantes --}%


                    %{-- Aba das Fichas de Espécies --}%
                    <div role="tabpanel" class="tab-pane" id="tab3">
                        Aba das Fichas de Espécies
                        &nbsp;<br>
                    </div> %{-- fim da aba das Fichas de espécies --}%


                    %{-- Aba de Paticipante x Fichas Espécies --}%
                    <div role="tabpanel" class="tab-pane" id="tab4">
                        Aba das Fichas X Participantes
                        &nbsp;<br>
                    </div> %{-- fim da aba de Paticipante x Fichas Espécies --}%
                </div>
            </div> %{-- Fim modal body --}%
        </div>
    </div>
</div>