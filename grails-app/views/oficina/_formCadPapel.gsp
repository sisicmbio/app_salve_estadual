<!-- view: /views/oficina/_formCadPapel/-->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<g:if test="${canModify}">
    %{-- Formulário de associação do particpante com as fichas --}%
    <form class="form-inline" name="frmCadPapel" id="frmCadPapel" role="form">

        <fieldset>

            <div style="max-height:200px;overflow-y:auto;" id="divGridPapel">
                <g:render template="divGridSelPapel" model="['listPapeis':listPapeis]"></g:render>
            </div>
        </fieldset>


        <fieldset>
            <legend>Participantes</legend>
            <div style="max-height:200px;overflow-y:auto;" id="divGridPapelParticipante">
                <g:render template="divGridSelParticipantePapel" model="['listParticipantes':listParticipantes]"></g:render>
            </div>
        </fieldset>

        <g:if test="${oficina.tipoOficina.codigoSistema != 'OFICINA_VALIDACAO'}">
            <fieldset>
                <legend>Selecionar Fichas</legend>
                <div class="mt10" id="divFiltrosFichaPapel"></div>
                <div style="max-height:500px;overflow-x:hidden;overflow-y:auto;" id="divGridFichaPapel"></div>
             </fieldset>
        </g:if>
         %{-- botão --}%
        <div class="fld panel-footer">
            <button data-action="oficina.papel.save" class="fld btn btn-success">Gravar</button>
        </div>

    </form>
</g:if>
<fieldset class="mt10">
    <legend>Funções Atribuídas</legend>
    <div id="divGridPapelParticipanteFicha"></div>
</fieldset>
<div class="end-page"></div>

