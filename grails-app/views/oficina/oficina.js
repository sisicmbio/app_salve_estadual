//# sourceURL=oficina.js
;var oficina = {
    filtrosAbaFicha1: {},
    filtrosAbaFicha2: {},
    filtrosAbaPapel: {},
    filtrosAbaExecucaoLc: {},
    filtrosAbaExecucaoNaoLc: {},
    fichasSelecionadasGride1:[],
    fichasSelecionadasGride2:[],
    semCache: false,
    layout:'',

    // gride oficinas utilizando app.grid()
    gridOficinasLoad:function(){},
    exportToPlanilha:function(params){
        app.confirm('Confirma a exportação para planilha?',function(){
            oficina.updateGrid({format:'xls'});
        });
    },
    init: function() {
        oficina.disableTabs(true);
        destroyEditors();
        oficina.layout = $("#fichaContainerBody input#layout").val();
        // restaurar o ultimo ciclo utilizado e disparar a consulta
        window.setTimeout(function(){
            // Exibir a tela inicial
            oficina.sqCicloAvaliacaoChange({ sqCicloAvaliacao: $("#sqCicloAvaliacao").val() } );
        },1000);
    },
    disableTabs: function(disable) // desabilitar as abas
        {
            disable = (disable == false ? false : true);
            app.disableTab('ulTabsOficina', disable);
            app.disableTab('tabCadastro', false); // habilitar a primeira aba
            app.showHideTab('tabFicha,tabParticipante,tabMemoria,tabPapel,tabExecucao', disable);
        },
    clearTabs: function() {
        destroyEditors();
        app.selectTab('tabCadastro'); // selecionar a aba Cadastro
        $("#gerenciarOficina #divTabs #tabContent > div").each(function() {
            if (!/tabCadastro/.test(this.id)) {
                $(this).html('');
            }
            // limpar gride de fichas selecionadas
            //$("#frmOficinaFicha #divGridFichas").html('');
            //$("#frmOficinaFicha #divGridFichas2").html('');
        });
        $("#gerenciarOficina #sqOficinaSelecionada").val('');
        // $("#gerenciarOficina .nome-oficina-selecionada").html('');
        $("#span-oficina-selecionada").html('').hide();
    },
    tabClick: function(params) {
        return true;
    },
    sqSituacaoChange: function(params, select, evt) {
        var codigo = $(select).find('option:selected').data('codigo');
        var sqOficina = $(select).data('sqOficina');
        var novoValor = select.value;
        params.sqSituacao = novoValor;
        app.ajax(baseUrl + 'oficina/changeSituacao', params, function(res) {
            if (res.status == 0) {
                if (codigo != 'ABERTA') {
                    $(select).removeClass('green').addClass('red');
                    $("#tr-"+sqOficina).addClass('red');
                } else {
                    $(select).removeClass('red').addClass('green');
                    $("#tr-"+sqOficina).removeClass('red');
                }
                //oficina.updateGrid({sqOficina:sqOficina});
            } else {
                $(select).find('option[data-codigo='+codigo+']').prop('selected',false);
                if (codigo == 'ABERTA') {
                    codigo = 'ENCERRADA'
                } else {
                    codigo = 'ABERTA'
                }
                $(select).find('option[data-codigo='+codigo+']').prop('selected',true);
            }
        }, '', 'json');
    },
    sqCicloAvaliacaoChange: function(params) {

        // zerar os filtros das fichas
        oficina.filtrosAbaFicha1 = {};
        oficina.filtrosAbaFicha2 = {};
        var sqCicloAvaliacao = $("#sqCicloAvaliacao").val();
        oficina.disableTabs();
        oficina.clearTabs();
        // limpar aba Fichas
        $("#gerenciarOficina #divTabs #tabContent #tabFicha").html('');
        if (sqCicloAvaliacao) {
            $("#divTabs").removeClass('hidden');
        } else {
            $("#divTabs").addClass('hidden');
        }
        // gravar ciclo selecionado
        app.saveField('sqCicloAvaliacao');

        // atualizar gride
        oficina.updateGrid();
    },
    sqTipoOficinaChange: function() {
        var fld = $("#frmCadOficina #noOficina");
        var codigo = $("#frmCadOficina #sqTipoOficina option:selected").data('codigo')
        if ( fld.val().trim() == '' || / de $/.test( fld.val() )  )
        {
            if (/^REUNI/.test(codigo)) {
                fld.val($("#frmCadOficina #sqTipoOficina option:selected").text().trim() + ' da avaliação de ');
            } else {
                fld.val($("#frmCadOficina #sqTipoOficina option:selected").text().trim() + ' de ');
            }
        }
        if (codigo == 'OFICINA_VALIDACAO') {
            $("#frmCadOficina #sqUnidadeOrg").parent().addClass('hidden');
            $("#frmCadOficina #sqPontoFocal").parent().addClass('hidden');
        }
        else {
            $("#frmCadOficina #sqUnidadeOrg").parent().removeClass('hidden');
            $("#frmCadOficina #sqPontoFocal").parent().removeClass('hidden');
        }
    },
    checkUncheckAll: function(params, elem, evt) {
        checkUncheckAll(params,elem,evt);
    },
    btnShowFormClick: function(param) {
        /*
        if ($("#frmCadOficina").hasClass('hidden')) {
            $("#containerOficina #btnShowForm i").removeClass('fa-plus').addClass('fa-minus');
            $("#frmCadOficina").removeClass('hidden')
        } else {
            $("#containerOficina #btnShowForm i").removeClass('fa-minus').addClass('fa-plus');
            $("#frmCadOficina").addClass('hidden')
            app.reset('#frmCadOficina');
        }
        */
    },
    resetForm: function() {
        app.reset("frmCadOficina");
        app.focus('sqTipoOficina');
    },
    save: function(params) {
        if (!$("#frmCadOficina").valid()) {
            return;
        }
        var data = $("#frmCadOficina").serializeArray();
        app.params2data(params, data);
        app.ajax(baseUrl + 'oficina/saveOficina', data, function(res) {
            if (res.status == 0) {
                $("#btnShowForm").click();// fechar o formulário
                oficina.resetForm();
                oficina.updateGrid();
            }
        }, 'Gravando...', 'json');
    },
    edit: function(params) {
        if (!params.id) {
            return;
        }
        app.ajax(baseUrl + 'oficina/editOficina', {
            sqOficina: params.id
        }, function(res) {
            if (res) {
                // mostrar o formulário
                if ($("#frmCadOficina").hasClass('hidden')) {
                    $("#btnShowForm").click();
                }
                oficina.resetForm();
                app.setFormFields(res);
                app.focus('sqTipoOficina');
                oficina.sqTipoOficinaChange(); // mostrar/esconder campos de acordo com tipo da oficina
            }
        }, 'Carregando...', 'json');
    },
    delete: function(params, btn) {
        app.selectGridRow(btn);
        app.confirm('<br>Confirma exclusão da oficina <b>' + params.descricao + '</b>?', function () {
                app.confirm('<br>A oficina <b>' + params.descricao + '</b> será excluída completamente.<br><br><span class="red bold">Tem certeza?</span>', function () {
                     app.ajax(baseUrl + 'oficina/delete',
                            params,
                            function (res) {
                                if (res.status == 0) {
                                    oficina.updateGrid();
                                    oficina.resetForm();
                                }
                            });
                    },
                    function () {
                        app.unselectGridRow(btn);
                    }, params, 'ATENÇÃO!', 'danger');
            },
            function () {
                app.unselectGridRow(btn);
            }, params, 'Confirmação', 'warning');
    },
    resetFiltroOficinaAbaCadastro : function( params ){
        $("#fldNoCientificoFiltrarOficinaAbaCadastro").val('');
        oficina.updateGrid( params );
    },


    updateGrid: function(params) {
        params =  params || {};
        var data = app.params2data(params,{});
        delete data.action;
        app.grid('GridOficinas', data);
        return;


        var sqCicloAvaliacao = $("#sqCicloAvaliacao").val();
        var posicao = $("#containerOficina #divGridOficinas").scrollTop();
        var noCientificoFiltrarOficina = '';
        if (sqCicloAvaliacao) {

            noCientificoFiltrarOficina = $("#fldNoCientificoFiltrarOficinaAbaCadastro").val() || ''
            if( params && params.search && !noCientificoFiltrarOficina ) {
                return;
            }
            app.ajax(baseUrl + 'oficina/getGridOficinas', {
                sqCicloAvaliacao: sqCicloAvaliacao
                , noCientificoFiltrarOficina : noCientificoFiltrarOficina
                , layout : oficina.layout
            }, function(res) {
                $("#containerOficina #divGridOficinas").html(res);
                $("#containerOficina #divGridOficinas").scrollTop(posicao);
                if ($("#containerOficina #canModify").val() == 'false') {
                    $("#containerOficina #frmCadOficinaContainer").hide();
                } else {
                    $("#containerOficina #frmCadOficinaContainer").show();
                }
                $('#tableDivGridOficinas').DataTable( $.extend({},default_data_tables_options,
                    {
                        "order": [ 3, 'desc' ], // ordem inicial pelo período descendente
                        "columnDefs" : [
                            {"orderable": false, "targets": [0,7]}
                        ]
                    })
                );
            }, '', 'HTML');
        }
    },
    selectOficina: function(params) {
        if (params.sqOficina) {
            oficina.clearTabs();
            $("#gerenciarOficina #sqOficinaSelecionada").val(params.sqOficina);
            $("#gerenciarOficina #noOficinaSelecionada").val(params.noOficina);
            //$("#gerenciarOficina .nome-oficina-selecionada").html( params.noOficina);
            //$("#span-oficina-selecionada").html(': ' + params.noOficina + (params.canModify != 'false' ? '' : '&nbsp;<span class="red">(encerrada)</span>')).show('slow');
            $("#span-oficina-selecionada").html(': ' + params.noOficina + (params.canModify != 'false' ? '' : '&nbsp;<span class="red">(só leitura)</span>')).show('slow');
            oficina.disableTabs(false);
            if( /OFICINA_AVALIACAO|OFICINA_VALIDACAO/.test( params.codigoSistema ) ) {
                app.selectTab('tabFicha'); // selecionar a aba fichas
                app.showHideTab('tabMemoria', true);
                if( /OFICINA_VALIDACAO/.test( params.codigoSistema ) ) {
                    app.showHideTab('tabExecucao', true);
                }
            } else {
                app.showHideTab('tabFicha,tabPapel,tabExecucao', true);
                app.selectTab('tabParticipante'); // selecionar a aba participantes

            }
        }
    },
    abrirFicha: function(params) {
        app.alertInfo('Visualizar ficha completa ' + params.id)
    },


    /************************************************
     * ABA SELECIONAR FICHAS
     *
     */
    ficha: {
        init: function() {
            var sqCicloAvaliacao = $("#gerenciarOficina #sqCicloAvaliacao").val();
            if ($("#gerenciarOficina #sqOficinaSelecionada").val()) {
                var data = {
                        label:'Filtrar fichas incluídas na oficina',
                        sqCicloAvaliacao: sqCicloAvaliacao,
                        callback: 'oficina.ficha.updateGridFichas2',
                        onLoad: 'oficina.ficha.updateGridFichas2',
                        outrosCampos: "/oficina/camposFiltroFicha",
                        contexto:'avaliacao',
                        mostrarFiltros:'consultas',
                        hideFilters: 'oficina,categoria-oficina,situacao,categoriaAvaliacao', // nesta fase as fichas não possuem a aba Avaliação preenchida
                        sqOficina:$("#sqOficinaSelecionada").val() // exibir nos filtros somente os grupos das fichas da oficina
                    };
                    // carregar os filtros da ficha e criar o gride
                app.loadModule(baseUrl + 'ficha/getFormFiltroFicha', data, '#gerenciarOficina #divFiltrosFichas2');
            };
        },
        gride1PageChange:function( params ) {
            // marcar os checks das fichas selecionadas
            if( oficina.fichasSelecionadasGride1.length > 0 ) {
                oficina.fichasSelecionadasGride1.map( function( id ) {
                    $("#tabFicha #divGridFichas1 input[type=checkbox][value="+id+"]").prop('checked',true);
                });
                oficina.ficha.chkFichaChange(); // mostrar/esconder botão excluir
            }
        },
        gride2PageChange:function( params ) {
            // marcar os checks das fichas selecionadas
            if( oficina.fichasSelecionadasGride2.length > 0 ) {
                oficina.fichasSelecionadasGride2.map( function( id ) {
                    $("#tabFicha #divGridFichas2 input[type=checkbox][value="+id+"]").prop('checked',true);
                });
                oficina.ficha.chkFichaSelecionadaChange(); // mostrar/esconder acoes em lote das fichas selecionadas
            }
        },
        btnCadastrarClick:function(params)
        {
            if ($("#frmOficinaFicha #divGridFichas1 table").size() == 0) {
                // carregar os filtros da ficha e carregar o gride
                var sqCicloAvaliacao = $("#gerenciarOficina #sqCicloAvaliacao").val();
                var data = {
                    label:'Filtrar fichas não incluídas na oficina',
                    sqCicloAvaliacao: sqCicloAvaliacao,
                    callback: 'oficina.ficha.updateGridFichas1',
                    onLoad: 'oficina.ficha.updateGridFichas1',
                    hideFilters: 'oficina,agrupamento,situacao,categoriaAvaliacao', // nesta fase as fichas não possuem a aba Avaliação preenchida
                    mostrarFiltros: 'consultas'
                };
                $('#gerenciarOficina #divFiltrosFichas1').html('<h4 style="margin:0px;">Carregando gride...<i class="glyphicon glyphicon-refresh spinning"></i></h4>');
                app.loadModule(baseUrl + 'ficha/getFormFiltroFicha', data, '#gerenciarOficina #divFiltrosFichas1');
            };
        },
        updateGridFichas1: function(filtros) {
            if (filtros) {
                oficina.filtrosAbaFicha1 = filtros;
            }
            var data = oficina.filtrosAbaFicha1 || {};

            oficina.fichasSelecionadasGride1 = [];

            data.sqCicloAvaliacao = $("#gerenciarOficina #sqCicloAvaliacao").val();
            data.sqOficina = $("#gerenciarOficina #sqOficinaSelecionada").val();
            data.semCache = (oficina.semCache == true ? 'S' : '');

            // enviar a pagina atual
            data.gridPage = $("#frmOficinaFicha select#selPagination1").val();

            if (!data.sqCicloAvaliacao || !data.sqOficina) {
                return;
            }
            app.blockElement("#frmOficinaFicha #divGridFichas1", 'Carregando gride...');
            app.ajax(baseUrl + 'oficina/getGridFicha1', data, function(res) {
                $("#gerenciarOficina #divGridFichas1").html(res);
                app.unblockElement("#frmOficinaFicha #divGridFichas1");
                oficina.ficha.chkFichaChange(); // mostrar/esconder botão excluir
                // ativar seleção de linhas com shift+click
                $("#gerenciarOficina #divGridFichas1").shiftSelectable();
                var minHeight = Math.max(200,$("#gerenciarOficina #divGridFichas1").height() );
                $("#gerenciarOficina #divGridFichas1").css('min-height',minHeight + 'px');

            }, '', 'HTML');
        },
        updateGridFichas2: function(filtros) {
            if (filtros) {
                oficina.filtrosAbaFicha2 = filtros;
            }
            var data = oficina.filtrosAbaFicha2 || {};
            data.sqCicloAvaliacao = $("#gerenciarOficina #sqCicloAvaliacao").val();
            data.sqOficina = $("#gerenciarOficina #sqOficinaSelecionada").val();
            data.semCache = (oficina.semCache == true ? 'S' : '');

            // zerar fichas selecionadas
            oficina.fichasSelecionadasGride2 = [];

            // enviar a pagina atual
            data.gridPage = $("#frmOficinaFicha select#selPagination2").val();

            app.blockElement("#frmOficinaFicha #divGrid2Wrapper", 'Carregando gride...');
            app.ajax(baseUrl + 'oficina/getGridFicha2', data, function(res) {
                $("#gerenciarOficina #divGridFichas2").html(res);
                app.unblockElement("#frmOficinaFicha #divGrid2Wrapper");
                oficina.ficha.chkFichaSelecionadaChange(); // mostrar/esconder botão de excluir
                /*$('#tbOficinaSelSelecionadas').DataTable( $.extend({},default_data_tables_options,
                    {
                        "order": [ 2, 'asc' ], // ordem inicial pelo nome cientifico
                        "columnDefs" : [
                            {"orderable": false, "targets": [0,1]}
                        ]
                    })
                 );
                // ativar seleção de linhas com shift+click
                $("#tbOficinaSelSelecionadas").shiftSelectable();*/

            }, '', 'html');
        },
        chkFichaChange: function(ele) {
            if (ele && ele.checked && $(ele).data('situacao') == 'POS_OFICINA') {
                app.confirm('<br>Ficha já está avaliada. Confirma a seleção para esta oficina?',
                    function() {},
                    function() {
                        ele.checked = false;
                        oficina.ficha.chkFichaChange(); // remover a marcação
                    });
            }
            $("#tabFicha #divGridFichas1 input:checkbox").map(function(index,check){
               var id = parseInt( $(check).val() );
               var indexOf = oficina.fichasSelecionadasGride1.indexOf(id);
               if( $(check).is(':checked') ) {
                   if ( indexOf == -1 ) {
                        oficina.fichasSelecionadasGride1.push( id );
                   }
               } else {
                   if ( indexOf > -1 ) {
                       oficina.fichasSelecionadasGride1.splice(indexOf,1);
                   }
               }
            });

            $("#divGridFichas1 span.total-selecionadas").text( String(oficina.fichasSelecionadasGride1.length) + ' selecionada(s)');
            //if ($("#tabFicha #divGridFichas1 input:checkbox:checked").size() == 0) {
            if (oficina.fichasSelecionadasGride1.length == 0) {
                $("#tabFicha #div-btn-add-ficha").hide();
            } else {
                $("#tabFicha #div-btn-add-ficha").show();
            }
        },
        chkFichaSelecionadaChange: function() {
            $("#tabFicha #divGridFichas2 input:checkbox").map(function(index,check){
                var id = parseInt( $(check).val() );
                var indexOf = oficina.fichasSelecionadasGride2.indexOf(id);
                if( $(check).is(':checked') ) {
                    if ( indexOf == -1 ) {
                        oficina.fichasSelecionadasGride2.push( id );
                    }
                } else {
                    if ( indexOf > -1 ) {
                        oficina.fichasSelecionadasGride2.splice(indexOf,1);
                    }
                }
            });

            $("#divGridFichas2 span.total-selecionadas").text( String(oficina.fichasSelecionadasGride2.length) + ' selecionada(s)');
              if (oficina.fichasSelecionadasGride2.length == 0) {
    //        if ($("#tabFicha #divGridFichas2 input:checkbox:checked").size() == 0) {
                $("#tabFicha #divAcaoLote").hide();
            } else {
                $("#tabFicha #divAcaoLote").show();
            }
        },
        save: function(params) {
            // verificar se tem alguma ficha selecionada
            if ($("#gerenciarOficina #divGridFichas1 input:checkbox:checked").size() == 0) {
                app.alertInfo('Nenhuma ficha selecionada!');
                return;
            }
            data = {};
            data = app.params2data(params, data, 'frmOficinaFicha');
            data.sqOficina = $("#sqOficinaSelecionada").val();
            /*
            data.ids = []
            $("#gerenciarOficina #divGridFichas1 input:checkbox:checked").each(function() {
                data.ids.push(this.value);
            });
            data.ids = data.ids.join(',');
            */
            data.ids = oficina.fichasSelecionadasGride1.join(',');
            app.ajax(baseUrl + 'oficina/saveFicha', data,
                function(res) {
                    if (res.status == 0) {
                        $("#gerenciarOficina #divGridFichas").html(''); // limpar para carregar novamente
                        // atualizar os grides
                        oficina.semCache = true; // carregar os grides do banco de dados, não utilizar os dados em cache do cacheService
                        oficina.ficha.updateGridFichas1();
                        oficina.ficha.updateGridFichas2();
                        oficina.semCache = false;
                    }
                }, '', 'JSON'
            );
        },
        delete: function(params) {
            //if ($("#gerenciarOficina #divGridFichas2 input:checkbox:checked").size() == 0) {
            if (oficina.fichasSelecionadasGride2.length == 0 ) {
                app.alertInfo('Nenhuma ficha selecionada!');
                return;
            }
            app.confirm('<br>Confirma remoção da(s) ficha(s) da oficina?',
                function() {
                    data = {};
                    data = app.params2data(params, data, 'frmOficinaFicha');
                    data.ids=oficina.fichasSelecionadasGride2.join(',');
                    /*data.ids = [];
                    $("#gerenciarOficina #divGridFichas2 input:checkbox:checked").each(function() {
                        data.ids.push(this.value);
                    });
                    data.ids = data.ids.join(',');*/
                    app.ajax(baseUrl + 'oficina/deleteFicha', data,
                        function(res) {
                            if (res.status == 0) {
                                oficina.semCache = true;
                                oficina.ficha.updateGridFichas1(); // atualizar os grides
                                oficina.ficha.updateGridFichas2(); // atualizar os grides
                                oficina.semCache = false;
                            }
                        }, '', 'JSON'
                    );
                },
                function() { /*callback*/ }, params, 'Confirmação', 'warning');
        },
        saveAgrupamento: function(params) {
            // verificar se tem alguma ficha selecionada
            if( oficina.fichasSelecionadasGride2.length==0 ) {
            //if ($("#gerenciarOficina #divGridFichas2 input:checkbox[id^=sqOficinaFicha]:checked").size() == 0) {
                app.alertInfo('Nenhuma ficha selecionada!');
                return;
            }
            data = {}
            data = app.params2data(params, data, 'frmOficinaFicha');
            data.sqOficina = $("#sqOficinaSelecionada").val()
            data.dsAgrupamento = $("#gerenciarOficina #dsAgrupamento").val()
            data.ids = oficina.fichasSelecionadasGride2.join(',');

            //data.ids = []
            //$("#gerenciarOficina #divGridFichas2 input:checkbox[id^=sqOficinaFicha]:checked").each(function() {
            //    data.ids.push(this.value);
            //});
            //data.ids = data.ids.join(',');
            app.ajax(baseUrl + 'oficina/saveAgrupamento', data,
                function(res) {
                    if (res.status == 0) {
                        oficina.semCache = true;
                        oficina.ficha.updateGridFichas2(); // atualizar os grides
                        oficina.semCache = false;
                    }
                });
        },
    }, // fim aba ficha





    /************************************************
     * ABA PARTICIPANTES
     *
     */

    participante: {
        gride:null,
        init: function() {
            //$(".nome-oficina-selecionada").html( 'Selecionada: ' + $("#gerenciarOficina #noOficinaSelecionada").val() );

            // iniciar o editor tinyMce
            $('#acordeon_frmCadParticipanteEmail').on('show.bs.collapse', function(e) {
                if( !tinymce.activeEditor || tinymce.activeEditor.id != 'txEmail' )
                {
                    var editorOptions = $.extend({},default_editor_options, {
                        selector: '#txEmail',
                        toolbar : 'newdocument | ' + default_editor_options.toolbar
                    });
                    tinymce.init(editorOptions);
                };
                app.focus('deAssuntoEmail');
            });
            oficina.participante.updateGrid();


            /*tinymce.editors = [];
            var editorOptions = $.extend({},default_editor_options, {
                selector: '#txEmail',
                toolbar : 'newdocument | ' + default_editor_options.toolbar
            });
            window.setTimeout(function(){
                tinymce.init(editorOptions);
            },500);
            */
        },
        // importar os participantes das consultas diretas ou revisões
        showModalImportarParticipante:function( params ){
            params = params || {};
            var sqOficina = params.sqOficina;
            var sqCicloAvaliacao = $("#sqCicloAvaliacao").val();
            if( ! sqCicloAvaliacao ){
                return;
            }
            if( ! sqOficina ){
                return;
            }

            var data = {sqCicloAvaliacao:sqCicloAvaliacao, sqOficina:sqOficina};
            var minHeight = Math.max( window.innerHeight-200, 500 );
            var maxWidth = Math.min( $(window).width(), 800 );
            data.reload = true;
            data.modalName = 'modalOficinaImportarParticipante';
            app.openWindow({
                id: 'modalOficinaImportarParticipante',
                url: app.url + 'main/getModal',
                width: maxWidth,//($(window).width()-50),
                data: data,
                height:minHeight,
                title: '<i class="blue fa fa-group"></i>&nbsp;Importar Participante',
                autoOpen: true,
                modal: true
            }, function (data, e) {
                // onshow
                $("#modalOficinaImportarParticipante #divCicloConsultaImportarWrapper input:checkbox").on('change',function(){
                    oficina.participante.selectFiltersChange();
                });
                oficina.participante.selectFiltersChange();
            }, function (data, e) {
                //onClose
                oficina.participante.updateGrid();
            });
        },
        sqCicloConsultaImportarChange:function( params, ele ){
            var $select = $("#modalOficinaImportarParticipante #divCicloConsultaImportarWrapper select");
            if( ! $select.val() ){
                return;
            }
            var data = { sqCicloConsulta:$select.val() }
            $tbody = $("table#tableOficinaImportarParticipantes tbody");
            $tbody.html('<td colspan="3">Carregando. Aguarde...</td>');
            app.ajax(baseUrl + 'oficina/getGridImportarParticipante', data,
                function(res) {
                    $res = $(res).find('tbody')
                    $tbody.html( $res.html() );
                }, '', 'HTML'
            );

        },
        importarParticipante:function( params ) {
            var data = app.params2data(params,{})
            if( ! params.sqCicloConsultaImportar ) {
                app.alertInfo('Selecione uma consulta ou revisão');
                return;
            }
            // ler os participantes marcados no gride
            $table = $("#modalOficinaImportarParticipante #tableOficinaImportarParticipantes")
            data.ids = [];
            $table.find('tbody input:checkbox:checked').each(function(index, item){
                data.ids.push( item.value );
            })
            console.log( data )
            if( data.ids.length == 0 ) {
                app.alertInfo('Nenhum participante selecionado no gride.');
                return;
            }

            app.confirm('<br>Confirma importação?',
                function() {
                    data.ids = data.ids.join(',');
                    app.ajax(baseUrl + 'oficina/saveImportacaoParticipantes', data,
                        function(res) {
                            if (res.status == 0) {
                                app.closeWindow('modalOficinaImportarParticipante');
                            }
                        }, '', 'JSON'
                    );
                },
                function() { /*callback*/ }, params, 'Confirmação', 'warning');
        },
        chkImportarParticipanteChange:function( ele ){},


        selectFiltersChange:function (params, ele){
            var $container = $("#modalOficinaImportarParticipante #divCicloConsultaImportarWrapper");
            var filtros = []
            // desmarcar radio selecionado e marcar o selecionado
            $container.find('label.control-label').removeClass('label-selected');
            $container.find('input:checkbox:checked').each( function(i,input) {
                $(input).parent().addClass('label-selected');
                filtros.push( input.value );
            });

            // mostrar/esconder as opções do select
            var $select = $container.find("select#sqCicloConsultaImportar")

            $select.find("option").each(function(i, option ){
                var data = $(option).data();
                $(option).show();
                if( data.tipo ) {
                    $(option).hide();
                    // esconder situações indesejadas
                    if (
                        ( data.situacao == 'ABERTA' && filtros.indexOf('A') > -1
                            || data.situacao == 'FECHADA' && filtros.indexOf('F') > -1
                        ) && ( data.tipo == 'CONSULTA_DIRETA' && filtros.indexOf('D') > -1 ||
                        data.tipo == 'REVISAO_POS_OFICINA' && filtros.indexOf('R') > -1)
                    )
                    {
                        $(option).show();
                    }
                }
            });

            // limpar seleção atual
            $select.val('');

            // limpar o gride
            oficina.participante.sqCicloConsultaImportarChange(params);
        },


        sqParticipanteChange: function(params, ele) {
            $("#frmCadParticipante #deEmail").val('');
            if (params.select2.data().length > 0) {
                var data = params.select2.data()[0];
                $("#frmCadParticipante #deEmail").val(data.email);
                app.focus('noInstituicao');
            }
        },
        save: function(params) {
            if (!$("#frmCadParticipante").valid()) {
                return;
            }
            var data = $("#frmCadParticipante").serializeArray();
            data.sqOficina = $("#sqOficinaSelecionada").val();
            if (data.sqOficina) {
                app.ajax(baseUrl + 'oficina/saveParticipante', data,
                    function(res) {
                        if (res.status == 0) {
                            app.reset('frmCadParticipante');
                            oficina.participante.updateGrid();
                        } else if (res.errors) {
                            app.alertError(res.errors.join('<br/>'), 'Erro(s) Encontrado(s)')
                        }
                    }, '', 'JSON'
                );
            }
        },
        chkDelParticipanteChange: function() {
            if ($("#tabParticipante #divGridParticipante input:checkbox[id^=sqOficinaParticipante]:checked").size() == 0 ) {
                $("#tabParticipante #div-btn-delete,#fieldset_email").hide();
            } else {
                $("#tabParticipante #div-btn-delete,#fieldset_email").show();
            }
        },
        delete: function(params, btn) {

            // ler os itens selecionados
            var data = {}
            data.ids = []
            $("#frmCadParticipante #divGridParticipante input:checkbox[id^=sqOficinaParticipante]:checked").each(function() {
                data.ids.push(this.value);
            });
            data.ids = data.ids.join(',');

            app.selectGridRow(btn);
            app.confirm('<br>Confirma exclusão do(s) participante(s)?', function() {
                    app.ajax(baseUrl + 'oficina/deleteParticipante',
                        data,
                        function(res) {
                            if (res.status == 0) {
                                oficina.participante.chkDelParticipanteChange();
                                oficina.participante.updateGrid();
                            }
                        });
                },
                function() {
                    app.unselectGridRow(btn);
                }, params, 'Confirmação', 'warning');
        },
        updateGrid: function( params ) {
            var data = {};
            params = params || {};
            data.sqOficina = $("#sqOficinaSelecionada").val();
            if ( ! data.sqOficina ) {
                return;
            }
            var posicao = $("#frmCadParticipante #divGridParticipante").scrollTop();

            // quando passar o parametro id é para atualizar um linha especifica no gride
            var $tr;
            var nuLilnhaAtual;
            if( params.id )
            {
                $tr = $('#tr-grid-particiante-' + params.id );
                nuLilnhaAtual = $tr.find('td:first').html();
                $tr.html('<td colspan="8" class="tr-updating">Atualizando...&nbsp:<i class="glyphicon glyphicon-refresh spinning"></i></td>')
            }
            else {
                $("#frmCadParticipante #divGridParticipante").html('');
            }
            data = app.params2data( params, data );
            app.ajax(baseUrl + 'oficina/getGridParticipante', data,
                function(res) {
                    if( ! params.id ) {
                        $("#frmCadParticipante #divGridParticipante").html(res);
                        $("#frmCadParticipante #divGridParticipante").scrollTop(posicao);
                        // aplicar plugin datatable
                        oficina.participante.gride = $('#tableOficinaParticipantes').DataTable($.extend({}, default_data_tables_options,
                            {
                                "columnDefs": [
                                    {"orderable": false, "targets": [1, 6, 7]}
                                ]
                            })
                        );
                        // ativar seleção de linhas com shift+click
                        $("#tableOficinaParticipantes").shiftSelectable();
                    }
                    else {
                        $tr.html( $(res).find('tr#tr-grid-particiante-'+params.id).html() );
                        $tr.find('td:first').html( nuLilnhaAtual );
                        oficina.participante.gride.rows($tr).invalidate().draw();
                        // ativar seleção de linhas com shift+click
                        $($tr).shiftSelectable();
                    }

                }, '', 'HTML'
            );
        },
        getTextLastEmail: function() {
            var data = {
                sqOficina: $("#sqOficinaSelecionada").val()
            };
            app.ajax(baseUrl + 'oficina/getTextLastEmail', data,
                function(res) {
                    if (res) {
                        var linhas = res.split('\n');
                        if (linhas[0].indexOf('Assunto:') == 0 && linhas[1].indexOf('Cc:') == 0 && linhas[2] == '') {
                            $("#deAssuntoEmail").val(linhas[0].replace(/Assunto: ?/, ''));
                            $("#deEmailCopia").val(linhas[1].replace(/Cc: ?/, ''));
                            linhas = linhas.splice(3);
                            tinymce.get('txEmail').setContent(linhas.join('\n'));
                            //$("#frmConvidarEmail #txEmail").html(linhas.join('\n'));
                        } else {
                            $("#frmConvidarEmail #txEmail").val(res);
                        }
                    }
                    app.focus('txEmail', 'frmCadParticipante');
                });
        },
        sendMail: function(params) {
            var ids = [];
            $('#frmCadParticipanteEnviarEmail #txEmail').val(tinymce.get('txEmail').getContent());
            if (!$("#frmCadParticipanteEnviarEmail").valid()) {
                return;
            }
            var data = $("#tabParticipante #frmCadParticipanteEnviarEmail").serializefiles();
            data.append('sqOficina', $("#sqOficinaSelecionada").val());
            if (!data.get('sqOficina')) {
                app.alertError('Oficina não selecionada!');
                return;
            }
            var idSituacaoEmailEnviado = 0;
            $("#frmCadParticipante #divGridParticipante input:checkbox[id^=sqOficinaParticipante]:checked").each(function() {
                ids.push(this.value);
                if (!idSituacaoEmailEnviado) {
                    idSituacaoEmailEnviado = $("#divGridParticipante #sqSituacao-" + this.value + " option[data-codigo='EMAIL_ENVIADO']").val();
                }
            });

            if (ids.length == 0) {
                app.alertInfo('Nenhum participante selecionado!');
                return;
            }
            data.append('ids', ids.join(','));
            var msg = 'Confirma envio de email'
            if (data.get('deEmailCopia')) {
                msg += ' com cópia para:<b>' + data.get('deEmailCopia') + '</b>'
            }
            msg += '?'
            app.confirm('<br>' + msg, function() {
                app.ajax(baseUrl + 'oficina/enviarEmailParticipantes', data,
                    function(res) {
                        if (res.status == 1) {
                            app.alertError(res.errors.join('<br/>'))
                        } else {
                            // alterar o status para enviado
                            oficina.participante.updateGrid();
                            /*if (idSituacaoEmailEnviado) {
                                ids.forEach(function(id) {
                                    $("#divGridParticipante #sqSituacao-" + id).val(idSituacaoEmailEnviado);
                                })
                            }
                            */
                        }
                    }, 'Enviando email(s). Aguarde...', 'JSON');
            });
        },
        changeSituacaoConvite: function(el) {
            var data = $(el).data()
            data.sqSituacao = el.value
            app.ajax(baseUrl + 'oficina/changeSituacaoConvite',
                data,
                function(res) {});
        },
        enviarEmailAssinaturaEletronica:function( params, ele, evt )
        {
            try{evt.preventDefault();} catch( e ){};
            app.confirm('<br>Confirma o envio do e-mail para assinatura eletrônica?',
                function() {
                    app.ajax('oficina/enviarEmailAssinaturaEletronica',
                        params,
                        function(res) {
                            if(res.error)
                            {
                                app.alertError( res.error );
                            }
                            if (res.msg) {
                                // atualizar a linha do gride para atualizar o title do botao com a data do envio
                                if( params.sqOficinaParticipante )
                                {
                                    //oficina.participante.updateGrid( { id : params.sqOficinaParticipante} )
                                    oficina.papel.updateGrid( { sqOficinaParticipante : params.sqOficinaParticipante} )
                                }
                                //app.growl( res.msg,2,'Informação','tc' );
                            }
                        }, null, 'json');
                },null,null,'Assinatura Eletrônica');
        },
    },


    /************************************************
     * ABA MEMORIA
     *
     */
    memoria : {
        init: function () {
            oficina.memoria.updateGrid();
            oficina.memoria.updateGrideAnexo();

            // exibir os anexos se já exisitir memoria cadastrada
            var sqOficinaMemoria = $("#frmCadMemoria #sqOficinaMemoria").val()
            if( sqOficinaMemoria)
            {
                $("#divContainerGridAnexoOficinaMemoria").show();
            }
        },

        save: function (saveParticipantes) {
            var erros=[];

            var data = {
                'sqOficina'         : $("#gerenciarOficina #sqOficinaSelecionada").val()
                , 'sqOficinaMemoria': $("#frmCadMemoria #sqOficinaMemoria").val()
                , 'deLocalOficina'  : $("#frmCadMemoria #deLocalOficina").val()
                , 'dtInicioOficina' : $("#frmCadMemoria #dtInicioOficina").val()
                , 'dtFimOficina'    : $("#frmCadMemoria #dtFimOficina").val()
                , 'deSalas'         : $("#frmCadMemoria #deSalas").val()
                , 'txMemoria'       : trimEditor(tinyMCE.get('txMemoria').getContent() )
            };

            // campos obrigatórios
            if( ! data.deLocalOficina)
            {
                erros.push('- Informe o local da reunião.');
            }
            if( ! data.dtInicioOficina || !data.dtFimOficina )
            {
                erros.push('- Informe o período da avaliação.');
            }

            // marcar os campos obrigatórios em vermelho
            var formValid = $("#frmCadMemoria").valid();

            if( saveParticipantes )
            {
                data.sqPapel        = $("#frmCadMemoria #sqPapel").val();
                data.txConvidado    = $("#frmCadMemoria #txConvidado").val();
                data.acao           = 'addParticipante';
                if (!data.txConvidado) {
                    erros.push( '- Informe pelo menos um participante da oficina.');
                }
                if (!data.sqPapel) {
                    erros.push('- Selecione a função.');
                }
                if( erros.length > 0 )
                {
                    app.alertInfo( erros.join('<br>') );
                    return;
                }
            }
            if ( ! formValid ) {
                return;
            }
            app.ajax(baseUrl + 'oficina/saveFormMemoria', data, function (res) {
                if ( res.status == 0 ) {
                    if( saveParticipantes ) {
                        $("#frmCadMemoria #sqOficinaMemoria").val(res.data.sqOficinaMemoria);
                        oficina.memoria.updateGrid();
                        $("#frmCadMemoria #txConvidado").val('');
                        $("#divContainerGridAnexoOficinaMemoria").show();
                        app.focus('txConvidado');
                    }
                    app.resetChanges('frmCadMemoria');
                }
            }, 'Gravando...', 'json');
        },

        updateGrid: function (params) {
            app.ajax(baseUrl + 'oficina/getGridParticipanteMemoria', {
                sqOficina: $("#gerenciarOficina #sqOficinaSelecionada").val()
            }, function (res) {
                $("#frmCadMemoria #divGridParticipanteMemoria").html(res);
                $('#tableOficinaParticipantesMemoria').DataTable( $.extend({},default_data_tables_options,
                    {
                        "buttons": {"buttons": []},
                        "order": [ 2, 'asc' ], // ordem inicial pelo nome do participante
                        "columnDefs" : [
                            {"orderable": false, "targets": [0,1]}
                        ]
                    })
                );
            }, '', 'HTML');
        },
        chkDelParticipanteMemoriaChange: function() {
            if ($("#frmCadMemoria #divGridParticipanteMemoria input:checkbox:checked").size() == 0) {
                $("#frmCadMemoria #divAcaoLoteMemoria").hide();
            } else {
                $("#frmCadMemoria #divAcaoLoteMemoria").show();
            }
        },
        deleteParticipantes:function() {
            var data = { ids:[] };
            var checked = $("#frmCadMemoria #divGridParticipanteMemoria input:checkbox:checked");
            if( checked.length == 0 )
            {
                app.alertInfo('Nenhum participante selecionado.');
                return;
            }
            checked.each(function() {
                data.ids.push(this.value);
            });
            app.confirm('<br>Confirma a EXCLUSÃO do(s) participante(s)?', function() {
                data.ids = data.ids.join(',');
                app.ajax(baseUrl + 'oficina/deleteParticipantesMemoria',
                    data,
                    function(res) {
                        if (res.status == 0) {
                            oficina.memoria.updateGrid();
                            $("#divAcaoLoteMemoria").hide();
                        }
                    }, '', 'JSON');
            });
        },
        openModalSelAnexo: function(params) {
            app.panel('pnlOficinaMemoriaAnexo', 'Anexar Documento/Imagem da Reunião Preparatória', '/oficina/getFormAnexo'
                , params, function() {
            }, 800, 600, true, false, function() {
                oficina.memoria.updateGrideAnexo();
            })
        },
        updateGrideAnexo: function() {
            var data = {
                sqOficinaMemoria: $("#sqOficinaMemoria").val()
            }
            if( ! data.sqOficinaMemoria ){
                return;
            }
            var posicao = $("#divGridAnexoOficinaMemoria").scrollTop();
            app.ajax(app.url + 'oficina/getGridAnexoMemoria', data,
                function(res) {
                    $("#divGridAnexoOficinaMemoria").html(res);
                    $("#divGridAnexoOficinaMemoria").scrollTop(posicao);
                    window.setTimeout(function() {
                        bindDialogPreview('divGridAnexoOficinaMemoria');
                    }, 500);
                }, null, 'HTML');
        },
        saveAnexo: function(params) {
            if (!$("#frmAnexoOficina").valid()) {
                return;
            }
            var data = $("#frmAnexoOficina").serializefiles();
            data.append('sqOficinaMemoria', params.sqOficinaMemoria);
            if (!data.get('sqOficinaMemoria')) {
                app.alertError('ID da reunião preparatória não informado!');
                return;
            }
            app.blockUI('Salvando arquivo...',
                // on block
                function () {
                    app.ajax(app.url + 'oficina/saveAnexoMemoria', data,
                        function (res) {
                            app.unblockUI();
                            app.closeWindow();
                            if (res.status == 0) {
                                app.reset('frmAnexoOficina');
                            }
                        }, null, 'JSON');
                });
        },
        deleteAnexo:function(params,btn){
            app.selectGridRow(btn);
            app.confirm('<br>Confirma exclusão do arquivo <b>' + params.descricao + '</b>?',
                function() {
                    app.ajax('oficina/deleteAnexoMemoria',
                        params,
                        function( res ) {
                            if ( res.status == 0 ) {
                                oficina.memoria.updateGrideAnexo();
                            }
                        }, null, 'json');
                },
                function() {
                    app.unselectGridRow(btn);
                }, params, 'Confirmação', 'warning');

        },
        gerarDocMemoria:function() {
            if( $("#liTabMemoria").hasClass('changed') ) {
                app.alertInfo('Alterações não estão salvas.');
                return;
            }

            var sqOficinaMemoria = $("#frmCadMemoria #sqOficinaMemoria").val();
            if( !sqOficinaMemoria )
            {
                app.alertInfo('Dados ainda não foram gravados.')
                return;
            }
            var data = {sqOficinaMemoria:sqOficinaMemoria};

            app.confirm('<br>Confirma impressão?', function() {
                app.ajax(baseUrl + 'oficina/gerarDocMemoria',
                    data,
                    function(res) {
                        if (res.status == 0) {
                            if (res.data.fileName) {
                                window.open(app.url + 'main/download?fileName=' + res.data.fileName + '&delete=1', '_top', "width=420,height=230,scrollbars=no,status=1");
                            }
                        }
                    }, '', 'JSON');
            });
        }
    },



        // fim participante()
    /************************************************
     * APA PAPEL
     *
     */

    papel: {
        init: function() {
            oficina.papel.updateGrid();
            // carregar campos filtro para fichas
            var sqOficina = $("#gerenciarOficina #sqOficinaSelecionada").val();
            var data = {
                sqOficina: sqOficina,
                callback: 'oficina.papel.updateGridFichas',
                onLoad: 'oficina.papel.updateGridFichas',
                hideFilters: 'oficina',
                outrosCampos: '/oficina/camposFiltroFicha' // adicionar o campo agrupamento aos filtros
            }
            app.loadModule(baseUrl + 'ficha/getFormFiltroFicha', data, '#gerenciarOficina #divFiltrosFichaPapel');

            // ativar seleção de linhas com shift+click
            $("#divGridPapelParticipante").shiftSelectable();
        },

        // atualizar o gride com os papeis já atribuidos
        updateGrid: function( params ) {
            params  = params || {};
            var data = {
                sqOficina: $("#sqOficinaSelecionada").val(),
            };
            /*if( params.sqOficinaParticipante )
            {
                data.sqOficinaParticipante = params.sqOficinaParticipante
            }*/

            var posicao = $("#tabPapel #divGridPapelParticipanteFicha").scrollTop();

            // quando passar o parametro id é para atualizar um linha especifica no gride
            var $tr;
            var nuLilnhaAtual;
            if( params.sqOficinaParticipante )
            {
                $tr = $('#tr-grid-particiante-ficha-' + params.sqOficinaParticipante );
                nuLilnhaAtual = $tr.find('td:first').html();
                $tr.html('<td colspan="5" class="tr-updating">Atualizando...&nbsp:<i class="glyphicon glyphicon-refresh spinning"></i></td>')
            }
            else {
                $("#tabPapel #divGridPapelParticipanteFicha").html('');
            }
            data = app.params2data( params, data );

            app.ajax(baseUrl + 'oficina/getGridPapelParticipanteFicha',
                data,
                function(res) {
                    if( ! params.sqOficinaParticipante ) {
                        $("#divGridPapelParticipanteFicha").html(res);
                        $("#divGridPapelParticipanteFicha").scrollTop(posicao);
                    }
                    else {
                        $tr.html( $(res).find('tr#tr-grid-particiante-ficha-'+params.sqOficinaParticipante).html() );
                        if( nuLilnhaAtual ) {
                            $tr.find('td:first').html( nuLilnhaAtual );
                        }
                    }
                }, '', 'HTML');
        },

        // atualizar o gride com as fichas da oficina
        updateGridFichas: function(filtros) {
            if (filtros) {
                oficina.filtrosAbaPapel = filtros
            }
            var position = $("#frmCadPapel #divGridFichaPapel").scrollTop();
            var data = oficina.filtrosAbaPapel || {};
            data.sqOficina = $("#gerenciarOficina #sqOficinaSelecionada").val();
            app.ajax(baseUrl + 'oficina/getGridFichasPapel',
                data,
                function(res) {
                    $("#frmCadPapel #divGridFichaPapel").html(res);
                    $("#frmCadPapel #divGridFichaPapel").scrollTop(position);

                    // ativar seleção de linhas com shift+click
                    $("#frmCadPapel #divGridFichaPapel").shiftSelectable();
                }, '', 'HTML');
        },
        save: function() {
            var idsOficinaParticipantes = []
            var idsFichas = []
            if ($("#tabPapel #divGridPapel input:radio[name=sqPapelParticipante]:checked").size() == 0) {
                app.alertInfo('Nenhum Papel Selecionado!')
                return;
            }
            $("#tabPapel #divGridPapelParticipante input:checkbox[id^=sqOficinaParticipante]:checked").each(function(i, id) {
                idsOficinaParticipantes.push(parseInt(id.value));
            })
            if (idsOficinaParticipantes.length == 0) {
                app.alertInfo('Nenhum Participante Selecionado!')
                return;
            }
            if( $("#tabPapel #divGridFichaPapel").size() > 0 ) {
                $("#tabPapel #divGridFichaPapel input:checkbox[id^=sqFicha]:checked:checked").each(function (i, id) {
                    idsFichas.push(parseInt(id.value));
                })
                if (idsFichas.length == 0) {
                    app.alertInfo('Nenhuma Ficha Selecionada!')
                    return;
                }
            }
            var data = $("#frmCadPapel").serializeArray();
            data.sqOficina = $("#sqOficinaSelecionada").val();
            data.idsOficinaParticipantes = idsOficinaParticipantes.join(',');
            data.idsFichas = idsFichas.join(',');
            app.confirm('<br>Confirma Gravação?', function() {
                app.ajax(baseUrl + 'oficina/savePapel',
                    data,
                    function(res) {
                        if (res.status == 0) {
                            app.reset('frmCadPapel');
                            oficina.papel.updateGrid();
                        }
                    }, '', 'JSON');
            });
        },
        edit: function(params, btn) {
            app.selectGridRow(btn);
            app.ajax(baseUrl + 'oficina/editPapel',
                params,
                function(res) {
                    app.setFormFields(res, 'frmCadPapel')
                }, 'Carregando...', 'JSON');
        },
        delete: function(params, btn) {
            app.selectGridRow(btn);
            app.confirm('<br>Confirma Exclusao de <b>' + params.descricao + '</b>?', function() {
                app.ajax(baseUrl + 'oficina/deletePapel',
                    params,
                    function(res) {
                        if (!res) {
                            oficina.papel.updateGrid();
                        }
                    }, '', 'TEXT');
            }, function() {
                app.unselectGridRow('divGridPapelParticipanteFicha');
            });
        },
        getFichasParticipante: function( params )
        {
            var data = { sqOficinaParticipante : params.sqOficinaParticipante
                        ,sqPapel : params.sqPapel }
            // executar o carregamento somente 1 vez
            if( $("#"+params.divId).text().trim() != '' ) {
                return;
            }
            $("#"+params.divId).html( 'Buscando as fichas...' + window.grails.spinner);
            app.ajax(baseUrl + 'oficina/getFichasParticipante',data,
                function(res) {
                     $("#"+params.divId).html( res );
                }, '', 'TEXT');
        }
    },// FIM ABA PAPEL


    /************************************************************
     * ABA EXECUCAO
     *
     */

    execucao: {
        modalAberta: null,
        grideLc:null,
        gridNaoLc:null,
        scrollPositionsSaved:false,
        posicaoScrollDocument:0,
        posicaoScrollGrideLcs:0,
        posicaoScrollGrideNaoLcs:0,
        init: function() {
            // carregar os campos filtros para grides fichas
            var data = {
                sqOficina: $("#sqOficinaSelecionada").val(),
                callback: 'oficina.execucao.updateGridLc',
                onLoad: 'oficina.execucao.updateGridLc',
                hideFilters: 'agrupamento',
                contexto:'oficina',
                outrosCampos: '/oficina/camposFiltroFicha'
            }
            if (data.sqOficina) {
                // carregar os filtros da ficha e criar o carregar o gride
                app.loadModule(baseUrl + 'ficha/getFormFiltroFicha', data, '#gerenciarOficina #divFiltrosFichasLc');
                data.callback = "oficina.execucao.updateGridNaoLc"
                data.onLoad = "oficina.execucao.updateGridNaoLc"
                data.hideFilters = '';
                app.loadModule(baseUrl + 'ficha/getFormFiltroFicha', data, '#gerenciarOficina #divFiltrosFichasNaoLc');
            }
            // iniciar editor de texto no encaminhamento
            if (tinymce.get("txEncaminhamento")) {
                tinymce.get("txEncaminhamento").remove();
            }

            // inicializar o editor rico
            var editorOptions = $.extend({
                selector: '#frmTextoMemoria #txEncaminhamento',
            }, default_editor_options);
            tinymce.init(editorOptions);

            oficina.execucao.updateGridTotais();
            oficina.execucao.updateGrideAnexo();
            // evento abrir e fechar texto encaminhamentos
            $('#divTextoEncaminhamento').on('hidden.bs.collapse', function(e) {
                $("#divTextoEncaminhamentoToggle").text('Editar memória');
            });
            $('#divTextoEncaminhamento').on('show.bs.collapse', function(e) {
                $("#divTextoEncaminhamentoToggle").text('Fechar edição memória');
                window.setTimeout(function() {
                    tinymce.get('txEncaminhamento').focus();
                }, 300)
            });
        },
        showFichaCompleta: function( sqFicha, noCientifico, verColaboracoes, contexto, onClose, canModify, sqOficina, sqOficinaFicha) {
            // guardar as posicoes dos scrolls
            oficina.execucao.saveScrollPositions();
            // abrir modal da ficha completa - fichaCompleta.js
            showFichaCompleta( sqFicha, noCientifico, verColaboracoes, contexto, onClose, canModify, sqOficina, sqOficinaFicha)
        },
        // guardar as posicoes dos scrolls da tela e dos grides
        saveScrollPositions:function(){
            if( !oficina.execucao.scrollPositionsSaved) {
                oficina.execucao.posicaoScrollDocument = $(document).scrollTop();
                oficina.execucao.posicaoScrollGrideLcs = $("#tableGridFichasLc").parent().scrollTop();
                oficina.execucao.posicaoScrollGrideNaoLcs = $("#tableGridFichasNaoLc").parent().scrollTop();
                oficina.execucao.scrollPositionsSaved = true;
            }
        },
        restoreScrollPositions:function(){
            if( oficina.execucao.scrollPositionsSaved ) {
                $(document).scrollTop(oficina.execucao.posicaoScrollDocument);
                $("#tableGridFichasLc").parent().scrollTop(oficina.execucao.posicaoScrollGrideLcs);
                $("#tableGridFichasNaoLc").parent().scrollTop(oficina.execucao.posicaoScrollGrideNaoLcs);
                oficina.execucao.scrollPositionsSaved = false;
                oficina.execucao.posicaoScrollDocument = 0;
                oficina.execucao.posicaoScrollGrideLcs = 0;
                oficina.execucao.posicaoScrollGrideNaoLcs = 0;
            }
        },

        updateGridTotais: function() {
            var data = {
                sqOficina: $("#sqOficinaSelecionada").val()
            }
            if (!data.sqOficina) {
                return;
            }
            app.blockElement('#tabExecucao #divGridExeTotais', 'Atualizando...');
            app.ajax(baseUrl + 'oficina/getGridExecucaoTotais',
                data,
                function(res) {
                    app.unblockElement('#tabExecucao #divGridExeTotais');
                    $("#tabExecucao #divGridExeTotais").html(res)
                }, '', 'HTML');
        },
        updateGridLc: function(filtros, sqFicha) {
            var data = {sqFicha:sqFicha};
            app.grid('GridExeFichaLc', data,function(  ) {
                // verificar se o gride está vazio para mostrar ou não o botão imprimir lc
                if( $("tr#GridExeFichaLcEmpty").size() == 1 ) {
                    $("#btnImprimirLc").parent().addClass('hidden');
                    $("#containerGridExeFichaLc").css('height',"125px");
                } else {
                    $("#btnImprimirLc").parent().removeClass('hidden');
                    $("#containerGridExeFichaLc").css('height', 'auto'); // esta div possui o max-height definido
                }
            });
        },
        updateGridNaoLc: function(filtros, sqFicha) {
            filtros = filtros || {}
            var sqOficina = filtros.sqOficina ? filtros.sqOficina : $("#sqOficinaSelecionada").val();
            var data = {sqOficina:sqOficina,sqFicha:sqFicha};
            app.grid('GridExeFichaNaoLc', data,function( ) {
                if( $("tr#GridExeFichaNaoLcEmpty").size() == 1 ) {
                    $("#containerGridExeFichaNaoLc").css('height',"125px");
                } else {
                    $("#containerGridExeFichaNaoLc").css('height', 'auto'); // esta div possui o max-height definido
                }
            } );
        },
        confirmarLc: function(params) {
            var msg = params.situacao == 'confirmada' ? '<b>NÃO</b> confirmar' : 'Confirmar'
            var isEndemic = params.stEndemicaBrasil=='S';
            var htmlCheckboxAjusteRegional='';
            if( params.situacao != 'confirmada' && isEndemic ) {
                htmlCheckboxAjusteRegional='<br><label class="cursor-pointer"><input id="chkDlgConfirmarLCAjusteRegional" class="fld checkbox-lg" type="checkbox" value="S">&nbsp;Preencher o <b>Ajuste Regional</b> com os valores padrão.</label>'
            }
            app.confirm('<br>' + msg + ' a categoria LC para ficha <b>' + params.descricao + '</b>?'+htmlCheckboxAjusteRegional,

                function() {
                    var preencherAjusteRegional = $("#chkDlgConfirmarLCAjusteRegional:checked").val() =='S' && isEndemic ? 'S' : 'N';
                    var data = $.extend({ preencherAjusteRegional:preencherAjusteRegional }, params )
                    app.ajax(baseUrl + 'oficina/confirmarLc',
                        data,
                        function(res) {
                            if (res.status == 0) {
                                oficina.execucao.updateGrides(params);
                            }
                        }, '', 'JSON');
                },
                function() {}, params, 'Confirmação', 'warning');
        },
        editarJustificativa: function(params, btn, evt) {
            if (!params.sqOficinaFicha) {
                return;
            }
            if ($("#btn-editar-justificativa-" + params.sqOficinaFicha).text() == 'Editar') {
                $("#btn-editar-justificativa-" + params.sqOficinaFicha).text('Encerrar Edição');
                var colWidth = $(btn.closest('td')).width();
                if (colWidth < 600) {
                    colWidth = 600;
                }

                // limpar o campo antes de abrir o editor, o conteudo será carregado via ajax
                $('#div-justificativa-' + params.sqOficinaFicha).html('')

                // inicializar o editor rico
                var editorOptions = $.extend({}, default_editor_options,{
                    selector: '#div-justificativa-' + params.sqOficinaFicha,
                    height: 300,
                    width: colWidth,
                    plugins: default_editor_options.plugins+',save',
                    toolbar: 'save | newdocument | ' + default_editor_options.toolbar,
                    setup: function(ed) {
                        ed.on('init', function() {
                            // ler o texto completo do campo
                            app.ajax(baseUrl + 'main/getText', {table:'oficina_ficha',column:'ds_justificativa',id:params.sqOficinaFicha},
                                function(res) {
                                    if (res) {
                                        tinymce.get('div-justificativa-' + params.sqOficinaFicha).setContent(res);
                                    }
                                });
                        });
                    },
                    /** save **/
                    save_onsavecallback: function(editor) {
                        var data = $(editor.targetElm).data();
                        oficina.execucao.encerrarEdicao(data.id)
                    },
                });
                tinymce.init(editorOptions);
            } else {
                oficina.execucao.encerrarEdicao(params.sqOficinaFicha);
            }
        },

        encerrarEdicao: function(sqOficinaFicha) {
            if (sqOficinaFicha) {
                //var editor = tinymce.get('div-justificativa-' + sqOficinaFicha);
                //var alterado = editor.isDirty();
                $("#btn-editar-justificativa-" + sqOficinaFicha).text('Editar');
                tinymce.get('div-justificativa-' + sqOficinaFicha).destroy();
                oficina.execucao.salvarJustificativa(sqOficinaFicha);
                //applyTruncate('tabExecucao'); // truncar os textos das justificativas longas e adicionar botão paara visualizar o texto na íntegra
            }
        },
        salvarJustificativa: function(sqOficinaFicha) {
            if (!sqOficinaFicha) {
                return;
            }
            var div = $("#div-justificativa-" + sqOficinaFicha)
            div.hide();
            var data = div.data();
            data.dsJustificativa = corrigirCategoriasTexto( div.html() );
            app.ajax(baseUrl + 'oficina/salvarJustificativa', data, function(res) {
                // atualizar a linha alterada do gride
                if( data.lc.toUpperCase() == 'S') {
                    oficina.execucao.updateGridLc(null, data.idFicha);
                }
                else if( data.lc.toUpperCase() == 'N') {
                    oficina.execucao.updateGridNaoLc( null, data.idFicha );
                }
            });
        },
        avaliarLc: function(params) {
            app.confirm('<br>Confirma Avaliação de <b>' + params.descricao + '</b>?',
                function() {
                    app.ajax(baseUrl + 'oficina/avaliarLc',
                        params,
                        function(res) {
                            if (res.status == 0) {
                                oficina.execucao.updateGrides();
                            } else {
                                app.alertInfo(res.msg)
                            }
                        }, '', 'JSON');
                },
                function() {}, params, 'Confirmação', 'warning');
        },
        transferirRetornarFicha: function(params, btn) {
            var msg = (params.situacao == 'transferida' ? 'Confirma o RETORNO de <b>' + params.descricao + '</b> para esta oficina?' : 'Confirma a TRANSFERÊNCIA de <b>' + params.descricao + '</b> para próxima oficina?');
            var data = {sqFicha:params.sqFicha}; // para atualizar somente a linha do gride que foi alterada
            app.confirm('<br>' + msg,
                function() {
                    app.ajax(baseUrl + 'oficina/transferirRetornarFicha',
                        params,
                        function(res) {
                            if (res.status == 0) {
                                oficina.execucao.updateGrides(data);
                            } else {
                                app.alertInfo(res.msg)
                            }
                        }, '', 'JSON');
                },
                function() {}, params, 'Confirmação', 'warning');
        },
        excluirRestaurarFicha: function(params,ele) {
            var msg = (params.situacao == 'excluida' ? 'Confirma a RESTAURAÇÃO de <b>' + params.descricao + '</b> para esta oficina?' : 'Confirma a EXCLUSÃO de <b>' + params.descricao + '</b> desta oficina?')
            var data = {sqFicha:params.sqFicha}
            app.confirm('<br>' + msg,
                function() {
                    app.ajax(baseUrl + 'oficina/excluirRestaurarFicha',
                        params,
                        function(res) {
                            if (res.status == 0) {
                                oficina.execucao.updateGrides(data);
                            } else {
                                app.alertInfo(res.msg)
                            }
                        }, '', 'JSON');
                },
                function() {}, params, 'Confirmação', 'warning');
        },
        avaliacaoExpedita: function(params) {
            var nomeOficina = $("#span-oficina-selecionada").html()
            var panel = app.panel('win-avaliacao-expedita', 'Avaliação Expedidita', '/oficina/getFormAvaliacaoExpedita', params, function() {
                // on show
                // preencher a coluna Resultado de acordo com as regras da avaliação expedita
                $("#body-avaliacao-expedita-" + params.sqOficina + " select[id^='stFavorecidoConversaoHabitats']").each(function(i, obj) {
                    var params = $(obj).data();
                    params.rendering = true; // não habilitar os botões Salvar da coluna Ação
                    oficina.execucao.stChangeExpedita(params, obj);
                })
            }, null, null, false, false, 'oficina.execucao.avaliacaoExpeditaClose').headerTitle("Avaliação Expressa" + nomeOficina).setTheme('#FFC30A');
            //panel.option.onClose = 'oficina.execucao.avaliacaoExpeditaClose'
        },
        avaliacaoExpeditaClose: function() {
            oficina.execucao.updateGrides();
        },
        stChangeExpedita: function(params, select) {
            // aplicar as regras de exibição dos campos de acordo com as respostas
            // apenas exibir / esconder campos
            var contexto = "#div-avaliacao-expedita-" + params.sqOficina + ' ';
            var st1 = $(contexto + "#stFavorecidoConversaoHabitats-" + params.id);
            var st2 = $(contexto + "#stTemRegistroAreasAmplas-" + params.id);
            var st3 = $(contexto + "#stPossuiAmplaDistGeografica-" + params.id);
            var st4 = $(contexto + "#stFrequenteInventarioEoo-" + params.id);
            st2.addClass('hidden');
            st3.addClass('hidden');
            st4.addClass('hidden');
            // calcular coluna resultado
            var categoria;
            var resultado;
            $(contexto + "#td-resultado-" + params.id).text('');
            $(contexto + "#sqCategoriaIucn-" + params.id).val('');
            if (st1.val() == 'S')
                categoria = $("#categoriaLC")
            else { // pergunnta 1 é: N  ou D
                if (st1.val() != '') {
                    st2.removeClass('hidden');
                    if (st2.val() == 'S') {
                        categoria = $("#categoriaLC")
                    } else if (st2.val() == 'N') {
                        if (st1.val() == 'N') {
                            categoria = 'avaliar' // avaliar
                        } else {
                            // pergunta 3
                            st3.removeClass('hidden');
                        }
                    } else if (st2.val() == 'D') {
                        st3.removeClass('hidden');
                    }
                }
            }
            // pergunta 3 está visivel
            if (!st3.hasClass('hidden')) {
                st4.removeClass('hidden');
                if (st3.val() == 'S') {
                    if (st4.val() == 'S') {
                        categoria = $("#categoriaLC")
                    } else if (st4.val() == 'N') {
                        categoria = 'avaliar'
                    } else if (st4.val() == 'D') {
                        categoria = 'avaliar'
                    }
                } else if (st3.val() == 'N') {
                    if (st4.val() == 'D') {
                        categoria = $("#categoriaDD")
                    } else if (st4.val() == 'N') {
                        categoria = 'avaliar'
                    } else if (st4.val() == 'S') {
                        categoria = 'avaliar'
                    }
                } else if (st3.val() == 'D') {
                    if (st4.val() == 'N') {
                        categoria = $("#categoriaDD")
                    } else if (st4.val() == 'D') {
                        categoria = $("#categoriaDD")
                    } else if (st4.val() == 'S') {
                        categoria = 'avaliar'
                    }
                }
            }
            if (categoria) {
                if (categoria != 'avaliar') {
                    $("#td-resultado-" + params.id).text(categoria.data('descricao'));
                    $("#sqCategoriaIucn-" + params.id).val(categoria.val());
                } else {
                    $("#td-resultado-" + params.id).text(categoria);
                }
            }
            if ($("#td-resultado-" + params.id).text().trim() && !params.rendering) {
                $("#btn-save-avaliacao-expedita-" + params.id).removeClass('disabled').addClass('btn-primary');
            } else {
                $("#btn-save-avaliacao-expedita-" + params.id).addClass('disabled').removeClass('btn-primary');
            }
            params.rendering = false;
        },
        saveAvaliacaoExpedita: function(params) {
            var contexto = "#div-avaliacao-expedita-" + params.sqOficina + ' ';
            var tr = '#tr-' + params.id + ' ';
            params.sqCategoriaIucn = '';
            if ( $(contexto + '#sqCategoriaIucn-' + params.id).val() ) {
                params.sqCategoriaIucn = $(contexto + '#sqCategoriaIucn-' + params.id).val();
            }
            $( contexto + tr + " select").each(function(k, select ) {
                if ($(select).is(":visible")) {
                    if (select.value) {
                        params[select.id.replace('-' + params.id, '')] = select.value
                    } else {
                        app.alert.error('Necessário responder todas as perguntas!')
                        return;
                    }
                } else {
                    params[select.id.replace('-' + params.id, '')] = '';
                }
            });
            app.confirm('<br>Confirma avaliação expressa de <b>' + params.descricao + '</b>?',
                function() {
                    app.ajax(baseUrl + 'oficina/saveAvaliacaoExpedita',
                        params,
                        function(res) {
                            if (res.status == 0) {
                                if (params.sqCategoriaIucn) {
                                    // atualizar somente ao fechar a janela de avaliação expedita
                                    //oficina.execucao.updateGridTotais();
                                    //oficina.execucao.updateGridLc();
                                    //oficina.execucao.updateGridNaoLc();
                                }
                                // desabilitar o botão salvar da linha
                                $("#btn-save-avaliacao-expedita-" + params.id).addClass('disabled').removeClass('btn-primary');
                            } else {
                                app.alertInfo(res.msg)
                            }
                        }, '', 'JSON');
                },
                function() {}, params, 'Confirmação', 'warning');
        },
        gerarRelatorioLc: function(params) {
            if (!params.sqOficina) {
                return;
            }
            // verificar se existe fichas selecionadas
            params.ids = [];
            $("#tableGridFichasLc input[name^=sqFicha]:checkbox:checked").each(function(key,input){
                params.ids.push(input.value);
            });
            params.ids = params.ids.join(',');
            var options = '<div class="mt10 text-left"><h3>Opções:</h3>'+
                            '<label class="cursor-pointer"><input type="radio" name="radioJustificativa" checked="true" value="P">&nbsp;relatório padrão &nbsp;<i class="green fa fa-info-circle" title="Imprime a justificativa do ciclo anterior ou a justificativa deste ciclo, se a espécie estiver avaliada."></i></label>'+
                            '<br><label class="cursor-pointer"><input type="radio" name="radioJustificativa" value="S">&nbsp;Imprimir a proposta de justificativa gravada &nbsp;<i class="green fa fa-info-circle" title="Imprime a justificativa gravada das espécies ainda não avaliadas."></i></label>'
                            //'<br><label class="cursor-pointer"><input type="radio" name="radioJustificativa" value="N">&nbsp;imprimir as justificativas do ciclo anterior &nbsp;<i class=" blue fa fa-info-circle" title="Imprimir somente as justificativas do ciclo anterior mesmo a ficha estando avaliada."></i></label></div>'
            app.confirm('<br><b>Imprimir o relatório das fichas mantidas LC?</b>'+ options+'<br>',
                function() {
                    params.imprimirJustificativaOficina = $("input[name=radioJustificativa]:checked").val();
                    app.ajax(app.url + 'oficina/gerarRelatorioLc/', params, function(res) {
                        if (res.data.fileName) {
                            window.open(app.url + 'main/download?fileName=' + res.data.fileName + '&delete=1', '_top', "width=420,height=230,scrollbars=no,status=1");
                        }
                        return;
                    }, window.grails.spinner + '&nbsp;Gerando relatorio. Aguarde....', 'json');
                }, null, params, 'Confirmação', 'warning');
        },
        gerarMemoria: function(params) {
            if (!params.sqOficina) {
                return;
            }
            app.confirm('<br>Confirma impressão da memória da oficina?',
                function() {
                    params.dtInicio = $('#frmPeriodoDocDiario #dtInicio').val();
                    params.txEncaminhamento = tinymce.get("txEncaminhamento").getContent();
                    params.relatorio = true;
                    app.ajax(app.url + 'oficina/gerarMemoria/', params, function(res) {
                        if (res.data.fileName) {
                            window.open(app.url + 'main/download?fileName=' + res.data.fileName + '&delete=1', '_top', "width=420,height=230,scrollbars=no,status=1");
                        }
                        return;
                    }, window.grails.spinner + '&nbsp;Gerando Memória da Oficina. Aguarde....', 'json');
                }, null, params, 'Confirmação', 'warning');
        },
        gerarDocFinal: function(params) {
            if (!params.tipo) {

                // verificar se as funções dos participantes foram informadas
                app.ajax(app.url + 'oficina/existeFuncaoAtribuida/', params, function( res ) {
                    var mensgemAlerta = res.msg;
                    oficina.execucao.modalAberta = app.dialog('<div class="mt10 text-left">' +
                        ( mensgemAlerta != ''? '<div class="alert alert-danger blink-me">'+mensgemAlerta+'</div>':'')+
                        '<span class="blue"><label class="cursor-pointer" title="Utilizar esta opção alterar a ordem alfabética dos taxons.">Ordenar táxons por FAMÍLIA?&nbsp;&nbsp;<input class="checkbox-lg" value="S" id="chkOrdenadoPorOrdemFamilia" type="checkbox"></label></span>' +
                        '<br><br><span class="red">Somente as fichas com categoria e justificativa serão impressas.</span>' +
                        '<br><span class="blue"><label class="cursor-pointer" title="Utilizar esta opção para que as fichas sem categoria e justificativa sejam impressas com as informações da última avaliação nacional.">Considerar a categoria da última avaliação nacional<br>para as fichas não validadas?&nbsp;&nbsp;<input class="checkbox-lg" value="S" id="chkConsiderarUAN" type="checkbox"></label></span>' +

                        // '<br><br><span class="blue"><label class="cursor-pointer" title="Imprime a justificativa gravada das espécies ainda não avaliadas">Imprimir a proposta de justificativa gravada?&nbsp;&nbsp;<input class="checkbox-lg" value="S" id="chkImprimirPropostaJustificativa" type="checkbox"></label></span>'+

                        '<div class="text-center" style="border:none;margin-top: 10px; padding-top: 5px;">' +
                        '<button class="btn btn-info btn200" data-tipo="final-lc" data-params="chkConsiderarUAN,chkOrdenadoPorOrdemFamilia" data-action="oficina.execucao.gerarDocFinal">Espécies Mantidas LC</button>' +
                        //'<button class="btn btn-primary btn200 ml20" data-tipo="final-nao-lc" data-action="oficina.execucao.gerarDocFinal">Espécies Avaliadas</button><br><br>' +
                        '<button class="btn btn-success btn200 ml20" data-tipo="final-todas" data-params="chkConsiderarUAN,chkOrdenadoPorOrdemFamilia" data-action="oficina.execucao.gerarDocFinal">Todas as Espécies</button>' +
                        '</div>' +
                        //'<button class="btn btn-default btn200 ml20" data-tipo="final-revisadas" data-action="oficina.execucao.gerarDocFinal">Espécies Avaliadas Revisadas</button>' +
                        '</div>', 'Relatório Final', null, null, null);
                    setTimeout(function(){
                        $('div.alert-danger.blink-me').removeClass('blink-me')
                    },3000);
                });

                return;
            }
            if (oficina.execucao.modalAberta) {
                oficina.execucao.modalAberta.close();
                oficina.execucao.modalAberta = null;
            }
            params.dtInicio = $('#frmPeriodoDocDiario #dtInicio').val();
            params.sqOficina = $('#gerenciarOficina #sqOficinaSelecionada').val();
            var tipo = ''
            switch (params.tipo) {
                case 'final-nao-lc':
                    tipo = 'final das espécies Avaliadas que não foram Mantida LC?';
                    break;
                case 'final-lc':
                    tipo = 'final das espécies avaliadas <b>mantidas LC</b>?';
                    break;
                case 'final-revisadas':
                    tipo = 'final de <b>TODAS</b> as espécies avaliadas revisadas?';
                    break;
                case 'final-todas':
                    tipo = 'final de <b>TODAS</b> as espécies avaliadas?';
                    break;
                case 'diario':
                    tipo = '<b>DIÁRIO</b>: ' + params.dtInicio;
                    break;
            }
            if (!tipo) {
                return;
            }

            app.confirm('<br>Confirma emissão do relatório ' + tipo,
                function() {
                    params.dtInicio = $('#frmPeriodoDocDiario #dtInicio').val();
                    params.relatorio=true
                    app.ajax(app.url + 'oficina/gerarDocFinal/', params, function(res) {
                        if (res.data.fileName) {
                            window.open(app.url + 'main/download?fileName=' + res.data.fileName + '&delete=1', '_top', "width=420,height=230,scrollbars=no,status=1");
                        }
                        return;
                    }, window.grails.spinner + '&nbsp;Gerando Relatório. Aguarde....', 'json');
                }, null, params, 'Confirmação', 'warning');
        },
        updateGrides: function( params) {
            oficina.execucao.saveScrollPositions();
            oficina.execucao.updateGridTotais();
            if( params && params.sqFicha ) {
                var $tr = $('#tabExecucao #tr-' +params.sqFicha);
                if( $tr.size() ==1 )
                {
                    if( $tr.data('lc') == 'S')
                    {
                        oficina.execucao.updateGridLc(null,params.sqFicha);
                    }
                    else
                    {
                        oficina.execucao.updateGridNaoLc(null,params.sqFicha);
                    }
                }
            }
            else
            {
                oficina.execucao.updateGridLc();
                oficina.execucao.updateGridNaoLc();
            }

            // voltar os scrolls para as posições anteriores
            setTimeout(function(){
                oficina.execucao.restoreScrollPositions();
            },1000);

        },
        openModalSelAnexo: function(params) {
            app.panel('pnlFichaAnexo', 'Anexar Documento/Imagem da Oficina', '/oficina/getFormAnexo', params, function() {
                //alert( 'abriu');
            }, 800, 600, true, false, function() {
                oficina.execucao.updateGrideAnexo();
            })
        },
        onAnexoFileChange : function(params, ele, evt )
        {
            $("#divInRelatorioFinal,#brInRelatorioFinal").hide();
            if( params.fileinput.filenames.length == 1  )
            {
                if( /\.pdf$/i.test(params.fileinput.filenames[0]) )
                {
                    $("#brInRelatorioFinal,#divInRelatorioFinal").show('slow');
                }
                else {
                    $("#inRelatorioFinal").prop('checked',false);
                }
            }
            else {
                $("#inRelatorioFinal").prop('checked',false);
            }
        },
        inRelatorioFinalChange:function( params,ele,evt) {
            var $legenda = $("#deLegendaOficinaAnexo");
            var $checkbox = $("#inRelatorioFinal");
            var texto = 'Relatório final da oficina.';
            if ( $checkbox.prop('checked') ) {
                if ( ! $legenda.val()) {
                    $legenda.val(texto);
                    app.focus('deLegendaOficinaAnexo');
                };
            } else {
                if ( $legenda.val() == texto ) {
                    $legenda.val('');
                };
            };
        },
        updateGrideAnexo: function() {
            var data = {
                sqOficina: $("#sqOficinaSelecionada").val()
            }
            var posicao = $("#divGridAnexoOficina").scrollTop();
            app.ajax(app.url + 'oficina/getGridAnexo', data,
                function(res) {
                    $("#divGridAnexoOficina").html(res);
                    $("#divGridAnexoOficina").scrollTop(posicao);
                    window.setTimeout(function() {
                        bindDialogPreview('divGridAnexoOficina');
                    }, 500);
                }, null, 'HTML');
        },
        saveAnexo: function(params) {
            if (!$("#frmAnexoOficina").valid()) {
                return;
            }
            var data = $("#frmAnexoOficina").serializefiles();
            data.append('sqOficina', params.sqOficina);
            if (!data.get('sqOficina')) {
                app.alertError('ID da oficina não informado!');
                return;
            }
            app.blockUI('Salvando arquivo...',
                // on block
                function () {
                    app.ajax(app.url + 'oficina/saveAnexo', data,
                        function (res) {
                            app.unblockUI();
                            app.closeWindow();
                            if (res.status == 0) {
                                app.reset('frmAnexoOficina');
                                app.focus('deLegendaOficinaAnexo');
                                if (res.data && res.data.qtdParticipantes > 0) {
                                    oficina.execucao.enviarEmailAssinaturaEletronica(params);
                                    // atualizar o gride participantes se já estiver montado
                                    if ($("#divGridParticipante").size() == 1) {
                                        oficina.participante.updateGrid();
                                    }
                                }
                            }
                        }, null, 'JSON');
                });
        },
        deleteAnexo: function(params, btn) {
            app.selectGridRow(btn);
            var alertaDocFinal = '';
            // emitir alerta se a exclusão for do documento final
            if( params.inDocFinal == 'S' )
            {
                var nuAssinaturas = params.nuAssinaturas;
                alertaDocFinal = '<span style="color:red;font-size: 1.3rem;">ATENÇÃO!<br>Este é o documento final da oficina'+ (params.nuAssinaturas>0?' e já possui '+nuAssinaturas+' assinatura(s)':'') +'.</span><br>'
            }
            app.confirm(alertaDocFinal+'<br>Confirma exclusão do arquivo ' + params.descricao + '?',
                function() {
                    app.ajax('oficina/deleteAnexo',
                        params,
                        function(res) {
                            if ( res.status == 0 ) {
                                oficina.execucao.updateGrideAnexo();
                                if( params.inDocFinal =='S')
                                {
                                    // atualizar o gride participantes se já estiver montado
                                    if( $("#divGridParticipante").size() == 1 ) {
                                        oficina.participante.updateGrid();
                                    };
                                }
                            }
                        }, null, 'json');
                },
                function() {
                    app.unselectGridRow(btn);
                }, params, 'Confirmação', 'warning');
        },
        enviarEmailAssinaturaEletronica:function( params, ele, evt )
        {
            try{evt.preventDefault();} catch( e ){};
            app.confirm('<br>Deseja enviar os e-mail para os particpantes assinarem eletronicamente?',
                function() {
                    app.ajax('oficina/enviarEmailAssinaturaEletronica',
                        params,
                        function(res) {
                            if(res.error)
                            {
                                app.alertError( res.msg );
                            }
                            else {
                                 if( $("#divGridParticipante").size() == 1 ) {
                                     oficina.participante.updateGrid();
                                 }
                            }
                            /*if (res.msg) {
                                app.alertInfo(res.msg,'Informação');
                            }
                             */
                        }, null, 'json');
                },null,null,'Assinatura Eletrônica');
        },
        encerrarOficina: function(params, btn) {
            if (!params.sqOficina) {
                app.alertInfo('Oficina não selecionada!');
                return;
            }
            app.confirm('<br>Confirma Encerramento da Oficina?',
                function() {
                    app.ajax(app.url + 'oficina/encerrarOficina', params,
                        function(res) {
                            if (res.status == 0) {
                                oficina.sqCicloAvaliacaoChange(); // limpar todas as abas e posicionar na aba inicial
                            }
                        }, null, 'JSON');
                },
                null, params, 'Confirmação', 'warning');
        },
        saveLinkSei: function(params) {

            params.deLinkSei = $("#tabExecucao #deLinkSei").val()
            app.ajax(baseUrl + 'oficina/saveLinkSei',
                params,
                function(res) {
                    if (res.status == 0) {
                        app.resetChanges('tabExecucao'); // remover cor vermelha da aba
                    }
                }, null, 'JSON');
        },
        openLinkSei: function(params, fld, evt) {
            var link = $("#tabExecucao #deLinkSei").val();
            if (link) {
                var win = window.open(link, '_blank');
                win.focus();
            }
        },
        print: function(params) {
            var data = {ids:[]};
            $.extend(data,params);
            data.adColaboracao = 'N';
            data.adAvaliacao = 'N';
            data.adValidacao = 'N';
            if( params.contexto == 'oficina') {
                data.adColaboracao = 'S';
                data.adAvaliacao = 'S';
            }
            else if( contexto=='oficina-validacao')
            {
                data.adValidacao = 'S';
            }
            app.gerarZipFichas(data);
        },
    }, // FIM ABA EXECUCAO

    editEmailParticipante:function( sqOficinaParticipante,email) {
        var $spanWrapper = $("#span-edit-email-participante-"+sqOficinaParticipante);
        var $spanText    = $("#span-edit-email-participante-text-"+sqOficinaParticipante);
        $spanText.data('oldValue',$spanText.text() );
        $spanWrapper.hide();
        $spanWrapper.after('<div id="div-input-email-participante-'+sqOficinaParticipante+'">' +
            '<input id="input-email-participante-'+sqOficinaParticipante+'" class="fld form-control w90p ignoreChange" value="'+$spanText.text()+'" ' +
            'type="text"> <i class="fld fa fa-save ml10" title="Gravar" onClick="oficina.saveEmailParticipante('+sqOficinaParticipante+',true)"></i> </div>');
        app.focus( 'input-email-participante-' + sqOficinaParticipante );
        var $input = $("#input-email-participante-"+sqOficinaParticipante);
        $input.on('keyup', function( e ) {
            if( e.key=='Enter' ) {
                $input.next().click();
            };
        });
    },
    saveEmailParticipante: function(sqOficinaParticipante,email) {
        var $divInput    = $("#div-input-email-participante-"+sqOficinaParticipante);
        var $spanWrapper = $("#span-edit-email-participante-"+sqOficinaParticipante);
        var $spanText    = $("#span-edit-email-participante-text-"+sqOficinaParticipante);
        var $input       = $("#input-email-participante-"+sqOficinaParticipante);
        if( $input.val().trim().toLowerCase() != $spanText.data('oldValue').trim().toLowerCase() ) {
            var data = {sqOficinaParticipante: sqOficinaParticipante, deEmail: $input.val().trim().toLowerCase() }
            app.ajax(baseUrl + 'oficina/saveEmailParticipante',
                data,
                function (res) {
                    if (res.status == 0) {
                        $spanText.text(data.deEmail);
                    }
                }, null, 'JSON');
        }
        $divInput.remove();
        $spanWrapper.show();
    },

    editNomeParticipanteMemoriaReuniao:function( sqOficinaMemoriaParticipante, nome) {
        var $spanWrapper = $("#span-edit-nome-participante-"+sqOficinaMemoriaParticipante);
        var $spanText    = $("#span-edit-nome-participante-text-"+sqOficinaMemoriaParticipante);
        $spanText.data('oldValue',$spanText.text() );
        $spanWrapper.hide();
        $spanWrapper.after('<div id="div-input-nome-participante-'+sqOficinaMemoriaParticipante+'">' +
            '<input id="input-nome-participante-'+sqOficinaMemoriaParticipante+'" class="fld form-control w90p ignoreChange" value="'+$spanText.text()+'" ' +
            'type="text"> <i class="fld fa fa-save ml10 cursor-pointer blue" title="Gravar" onClick="oficina.saveNomeParticipanteMemoriaReuniao('+sqOficinaMemoriaParticipante+',true)"></i> </div>');
        app.focus( 'input-nome-participante-' + sqOficinaMemoriaParticipante );
        var $input = $("#input-nome-participante-"+sqOficinaMemoriaParticipante);
        $input.on('keyup', function( e ) {
            if( e.key=='Enter' ) {
                $input.next().click();
            };
        });


    },
    saveNomeParticipanteMemoriaReuniao:function(sqOficinaMemoriaParticipante,nome) {
        var $divInput    = $("#div-input-nome-participante-"+sqOficinaMemoriaParticipante);
        var $spanWrapper = $("#span-edit-nome-participante-"+sqOficinaMemoriaParticipante);
        var $spanText    = $("#span-edit-nome-participante-text-"+sqOficinaMemoriaParticipante);
        var $input       = $("#input-nome-participante-"+sqOficinaMemoriaParticipante);
        if( $input.val().trim().toLowerCase() != $spanText.data('oldValue').trim().toLowerCase() ) {
            var data = {sqOficinaMemoriaParticipante: sqOficinaMemoriaParticipante, noParticipante: $input.val().trim() }
            app.ajax(baseUrl + 'oficina/saveNomeParticipanteMemoriaReuniao',
                data,
                function (res) {
                    if (res.status == 0) {
                        $spanText.text(data.noParticipante);
                    }
                }, null, 'JSON');
        }
        $divInput.remove();
        $spanWrapper.show();
    },
    editFuncaoParticipanteMemoriaReuniao:function(sqOficinaMemoriaParticipante,currentValue,nomeParticipante) {
        var $modal = $("#modalSelectFuncaoParticipante");
        $modal.data('sqOficinaMemoriaParticipante',sqOficinaMemoriaParticipante);
        $modal.modal('show');
        $modal.find('h4#modalNomeParticipante').html( nomeParticipante );
        var $select = $modal.find("select#selectAlterarFuncaoParticipante");
        if( $select.find('option').size() == 0  ) {
            var options = $("#sqPapel > option").clone();
            $select.html(options);
        }
        $select.val( currentValue );
    },
    saveFuncaoParticipanteMemoriaReuniao : function() {
        var $modal = $("#modalSelectFuncaoParticipante");
        var data = $modal.data();
        var $select = $modal.find("select#selectAlterarFuncaoParticipante");
        data.sqPapel = $select.val();
        $modal.modal('hide');
        if( data.sqPapel ) {
            app.ajax(baseUrl + 'oficina/saveFuncaoParticipanteMemoriaReuniao',
                data,
                function (res) {
                    if (res.status == 0) {
                        $("#span-edit-funcao-participante-text-"+data.sqOficinaMemoriaParticipante).html( $select.find('option:selected').text().trim() );
                    }
                }, null, 'JSON');
        }

    }




};
oficina.init();
