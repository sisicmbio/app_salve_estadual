<table class="table table-hover table-striped table-condensed table-bordered">
	<thead>
		<th>Motivo(s) da Mudança</th>
		%{-- <th>Observação</th> --}%
		<g:if test="${canModify}">
			<th>Ação</th>
		</g:if>
	</thead>
	<tbody>
	<g:each var="item" in="${listaMotivoMudanca}">
		<tr>
			<td>${item.motivoMudanca.descricao}</td>
			%{-- <td>${item.txFichaMudancaCategoria}</td> --}%
			<g:if test="${canModify}">
				<td style="text-align:center;width:60px;">
				%{--<a data-action="editMotivoMudanca" data-id="${item.id}" class="fld btn btn-default btn-xs btn-update" title="Alterar">--}%
   					%{--<span class="glyphicon glyphicon-pencil"></span>--}%
				%{--</a>--}%
				<a data-action="deleteMotivoMudanca"
				   data-descricao="${item.motivoMudanca.descricao}"
				   data-sq-ficha="${ficha.id}"
				   data-container="${params.container}"
				   data-id="${item.id}" class="fld btn btn-default btn-xs btn-delete" title="Excluir">
				   <span class="glyphicon glyphicon-remove"></span>
				</a>
				</td>
			</g:if>
		</tr>
	</g:each>
	</tbody>
</table>
