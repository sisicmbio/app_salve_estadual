<!-- view: /views/oficina/_divGridParticipante.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<table data-title="${listOficinaParticipante.size()} particitante(s)" class="table table-hover table-striped table-condensed table-bordered w100p" id="tableOficinaParticipantes">
    <thead>
    <th style="width:45px;">#</th>
    <g:if test="${canModify}">
            <th style="width:30px;"><i title="Marcar/Desmarcar Todos" data-action="oficina.checkUncheckAll" class="glyphicon glyphicon-unchecked"></i></th>
        </g:if>
        <th style="min-width:200px;width:auto;">Nome</th>
        <th style="width:200px;">Instituição</th>
        <th style="width:350px;">Email</th>
        <th style="width:200px;">Enviado em</th>
        <th style="width:180px;">Situação</th>
        %{-- <th style="width:150px;">Assinatura<br> Doc. Final</th>--}%
        %{-- <th style="max-width:20px;">Ação</th> --}%
    </thead>
    <tbody>
    <g:if test="${ listOficinaParticipante }">
        <g:each var="item" in="${listOficinaParticipante}" status="i">
            <tr id="tr-grid-particiante-${item.id}">
                <td class="text-center">${i+1}</td>
                <g:if test="${canModify}">
                    <td class="text-center">
                        <input id="sqOficinaParticipante_${item.id}" name="sqOficinaParticipante[${item.id}]" onChange="oficina.participante.chkDelParticipanteChange(this)" value="${item.id}" type="checkbox" class="checkbox-lg ignoreChange"/>
                    </td>
                </g:if>
                <td>${br.gov.icmbio.Util.capitalize( item?.noPessoa )}</td>

                %{-- INSTITUICAO --}%
                <td class="text-center">${raw( item?.sgInstituicao ? '<span title="'+item.noInstituicao+'">'+ item.sgInstituicao+'</span>': '' ) }</td>

                %{-- EMAIL --}%
                <g:if test="${canModify}">
                    <td>
                        <span id="span-edit-email-participante-${item.id}"><span id="span-edit-email-participante-text-${item.id}" class="content text-nowrap">${ item.deEmail?:item?.pessoa?.email?.toLowerCase()}</span><i onClick="oficina.editEmailParticipante(${item.id})" class="fa fa-edit ml10 cursor-pointer" title="Alterar email"></i></span>
                    </td>
                </g:if>
                <g:else>
                    <td>${item.deEmail?:item?.pessoa?.email?.toLowerCase()}</td>
                </g:else>

                %{-- DATA EMAIL --}%
                <td class="text-center">${item?.dtEmail ? item.dtEmail.format('dd/MM/yyyy HH:mm') : ''}</td>

                <td class="text-center">
                    <g:if test="${canModify}">
                    <select id="sqSituacao-${item.id}" data-id="${item.id}" onChange="oficina.participante.changeSituacaoConvite(this)" class="form-control fld200 fldSelect-thin">
                        <g:each var="item2" in="${listSituacaoConvite}">
                            <option data-codigo="${item2.codigoSistema}"
                                    ${item2.id==item?.sqSituacao ?' selected':''}
                                    value="${item2.id}">${item2?.descricao}
                            </option>
                        </g:each>
                    </select>
                    </g:if>
                    <g:else>
                        ${item?.deSituacao}
                    </g:else>
                </td>

                %{-- ASSINATURA DOCUMENTO FINAL--}%
                %{--<td class="text-center">
                <g:if test="${item.dtAssinatura}">
                    <span style="font-size:0.8rem !important;">Assinado em<br>${item.dtAssinatura.format('dd/MM/yyyy HH:mm:ss')}</span>
                </g:if>
                <g:else>
                    <g:if test="${ hasDocFinal }">
                        <button type="button" data-sq-oficina-participante="${item.id}" data-hash="${item.deHashAssinatura}"
                                data-action="oficina.participante.enviarEmailAssinaturaEletronica"
                                class="btn btn-xs${ item.dtEmailAssinatura ? ' btn-primary':' btn-default'}"
                                title="Enviar email solicitando a assinatura eletrônica. ${ ( item.dtEmailAssinatura ? 'Último envio ' + item.dtEmailAssinatura.format('dd/MM/yyyy HH:mm:ss') : 'Email não enviado.' )}"><i class="fa fa-share" aria-hidden="true"></i></button>
                    </g:if>
                    <g:else>
                        <span>Não emitido</span>
                    </g:else>

                </g:else>

                </td>--}%
%{--                 <td class="td-actions">
                   <a data-action="oficina.participante.delete" data-descricao="${item.pessoa.noPessoa}" data-id="" class="fld btn btn-default btn-xs btn-delete" title="Excluir">
                        <span class="glyphicon glyphicon-remove"></span>
                    </a>
                </td>
 --}%            </tr>
        </g:each>

    </g:if>
    <g:else>
        <tr>
            <td colspan="5" align="center">
                <h4>Nenhum Participante Adicionado</h4>
            </td>
        </tr>
    </g:else>

    %{--
            <g:if test="${listConsultas}">
                <g:each in="${listConsultas}" var="item" status="i">
                    <tr id="gdFichaRow_${i + 1}">
                        <td>José da Silva</td>
                        <td>jose.silva@gmail.com</td>
                        <td>01/01/2017 14:30</td>
                        <td>Puma concolor,Alectrurus tricolor,Leontopithecus chrysomelas</td>
                        <td class="td-actions">
                            <button type="button" data-action="oficina.reenviarEmail" class="btn btn-default btn-xs btn-email" title="Reenviar Email">
                            <span>Reenviar</span>
                            </button>
                        </td>
                    </tr>
                </g:each>
            </g:if>
            <g:else>
                <tr>
                    <td colspan="5" align="center">
                        <h4>Nenhum Registro Encontrado!</h4>
                    </td>
                </tr>
            </g:else> --}%
    </tbody>
</table>
