<!-- view: /views/oficina/_formCadOficina.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<div id="containerOficina">
    %{-- Formulário de cadastro de Oficinas --}%
    <fieldset class="mt10" id="frmCadOficinaContainer">
        <legend style="font-size:1.2em;">
            <a  id="btnShowForm"
                data-action="app.showHideContainer"
                data-target="frmCadOficina"
                data-focus="sqTipoOficina"
                data-callback=""
                title="Mostrar/Esconder formulário!">
                <i class="fa fa-plus green"/>&nbsp;Cadastrar</a>
        </legend>
        <form class="form-inline hidden" name="frmCadOficina" id="frmCadOficina" role="form">
            <input type="hidden" id="sqOficina" name="sqOficina" value="${oficina?.id}"/>
            <div class="form-group">
                <label for="sqTipoOficina" class="label-required control-label">Tipo da oficina</label>
                <br>
                <select name="sqTipoOficina" id="sqTipoOficina"
                        class="form-control fldSelect"
                        data-change="oficina.sqTipoOficinaChange"
                        required="true" data-msg-required="Campo obrigatório">
                    <option value="">?</option>
                    <g:each in="${listTipoOficina}" var="item">
                        <option data-codigo="${item.codigoSistema}" value="${item.id}">
                            ${item.descricao}
                        </option>
                    </g:each>
                </select>
            </div>

    %{--         <div class="form-group">
                <label for="sqSituacao" class="control-label">Situação</label>
                <br>
                <select name="sqSituacao" id="sqSituacao" class="form-control fldSelect fld200" required="true" data-msg-required="Campo obrigatório">
                    <g:each in="${listSituacaoOficina}" var="item">
                        <option data-codigo="${item.codigoSistema}" value="${item.id}">
                            ${item.descricao}
                        </option>
                    </g:each>
                </select>
            </div>
     --}%
            <br>
            %{-- nome --}%
            <div class="form-group">
                <label for="noOficina" class="label-required control-label">Nome</label>
                <br>
                <input value="" type="text" name="noOficina" id="noOficina" class="form-control fld500" maxlength="200" required="true" data-msg-required="Campo obrigatório"/>
            </div>

            %{-- sigla --}%
            <div class="form-group">
                <label for="sgOficina" class="label-required control-label">Nome abreviado</label>
                <br>
                <input value="" type="text" name="sgOficina" id="sgOficina" class="form-control fld300" maxlength="100" required="true" data-msg-required="Campo obrigatório"/>
            </div>

            %{-- Unidade Organizacional --}%
            <div class="${listUnidade?.size() == 1 ? 'hidden':''}">
                <div class="form-group ${listUnidade?.size() == 1 ? 'hidden':''}">
                    <label for="sqUnidadeOrg" class="label-required control-label">Unidade organizacional</label>
                    <br>
                    <select name="sqUnidadeOrg" id="sqUnidadeOrg" class="form-control fld600" required="true" data-msg-required="Campo obrigatório">
                        <option value="">?</option>
                        <g:each in="${ listUnidade }" var="item">
                            <option ${ listUnidade?.size() == 1 || item.id == oficina?.unidade?.id ? 'selected' : ''} value="${item.id}">
                                ${item.sgUnidade}
                            </option>
                        </g:each>
                    </select>
                    %{-- <a href="javascript:void(0);" data-action="" >Cadastrar</a> --}%
                </div>
            </div>

            %{-- Ponto Focal --}%

            %{--<div class="form-group">
                <label for="sqPontoFocal" class="control-label">Ponto focal</label>
                <br>
                <select name="sqPontoFocal" id="sqPontoFocal" class="form-control fld600 select2"
                        data-s2-url="oficina/select2PF"
                        data-s2-placeholder="?"
                        data-s2-params="sqUnidadeOrg,sqCicloAvaliacao"
                        data-s2-minimum-input-length="3"
                        data-s2-auto-height="true" required>
                </select>
            </div>--}%

            %{-- local --}%
            <div class="form-group">
                <label for="deLocal" class="label-required control-label">Local</label>
                <br/>
                <input value="" type="text" name="deLocal" id="deLocal" class="form-control fld400" maxlength="200" required="true" data-msg-required="Campo obrigatório"/>
            </div>


            %{-- periodo --}%
            <div class="form-group">
                <label for="dtInicio" class="label-required control-label">Início</label>
                <br/>
                <input value="" type="text" name="dtInicio" id="dtInicio" class="form-control date fldDate" required="true" data-msg-required="Campo obrigatório"/>
            </div>
            <div class="form-group">
                <label for="dtFim" class="label-required control-label">Fim</label>
                <br/>
                <input value="" type="text" name="dtFim" id="dtFim" class="form-control date fldDate" required="true" data-msg-required="Campo obrigatório"/>
            </div>

            <br>

            <g:if test="${listFonteRecurso}">
                %{-- Fonte Recurso --}%
                <div class="form-group">
                    <label for="sqFonteRecurso" class="control-label">Fonte principal de recurso</label>
                    <br>
                    <select name="sqFonteRecurso" id="sqFonteRecurso" class="form-control fld600">
                        <option value="">Nenhuma</option>
                        <g:each var="item" in="${ listFonteRecurso }">
                            <option ${ item.id == oficina?.fonteRecurso?.id ? 'selected' : ''} value="${item.id}">
                                ${item.descricao}
                            </option>
                        </g:each>
                    </select>
                </div>
            </g:if>
            %{-- botão --}%
            <div class="fld panel-footer">
                <button data-action="oficina.save" data-params="sqCicloAvaliacao" class="fld btn btn-success">Gravar</button>
                <button data-action="oficina.resetForm" class="fld btn btn-danger">Limpar</button>
            </div>
        </form>
    </fieldset>

    %{-- gride com as oficinas cadastrada --}%
    <fieldset>
        %{--campo para localizar a consulta que tem determinada espécie--}%
        <div class="fld400">
            <div class="form-group" style="margin-bottom: 10px;">
                <label>Localizar avaliação da espécie</label>
                <br>
                <div class="input-group input-group">
                    <input type="text" class="form-control ignoreChange input-bold-blue"
                           placeholder="Informe o nome da espécie"
                           data-enter="oficina.updateGrid()"
                           id="fldNoCientificoFiltrarOficinaAbaCadastro" style="display:inline">
                    <span class="input-group-addon">
                        <i class="fa fa-search cursor-pointer" title="Localizar..." data-search="true" data-action="oficina.updateGrid"></i>
                    </span>
                    <span class="input-group-addon">
                        <i class="fa fa-trash cursor-pointer" title="Limpar o campo" data-action="oficina.resetFiltroOficinaAbaCadastro"></i>
                    </span>
                </div>
            </div>
        </div>

        %{--    GRIDE DAS OFICINAS --}%
    <div id="containerGridOficinas"
         data-params="sqCicloAvaliacao,fldNoCientificoFiltrarOficinaAbaCadastro|noCientificoFiltrarOficina"
         data-field-id="sq_oficina"
         data-url="oficina/getGridOficinas"
         data-on-load="oficina.gridOficinasLoad">
        <g:render template="divGridOficinas"
                  model="[gridId: 'GridOficinas'
                                  ,listSituacaoOficina:listSituacaoOficina]"></g:render>
    </div>


</fieldset>
</div>
