<form class="form-horizontal" tabindex="0" role="form" name="frmTab2" id="frmTab2">
 	<input type="hidden" id="idParticipante" name="id" value="${data?.id}"/>
	<div class="form-group">
		<label for="cpf" class="col-sm-2 control-label"> Cpf: </label>
		<div class="col-sm-3">
			<input value="" type="text" name="cpf" id="cpf" class="form-control cpf"  data-mask-change="cpfChange" data-rule-cpf="true" data-cpf-valid="ler_pessoa"
				tabindex="1" maxlength="14" required="true"
				data-msg-required="Campo obrigatório" />
		</div>
		<div class="col-sm-7">
			<input value="Luis Eugênio" type="text" name="nome" id="nome" class="form-control"
				tabindex="-1" disabled="true" />
		</div>
		
	</div>
	<div class="form-group">
		<label for="instituicao" class="col-sm-2 control-label"> Instiução: </label>
		<div class="col-sm-10">
			<input value="" type="text" name="instituicao" id="instituicao"
				class="form-control" tabindex="1" maxlength="255" required="true" data-msg-required="Campo obrigatório"/>
		</div>
	</div>
	<div class="form-group">
		<label for="Data" class="col-sm-2 control-label">Data: </label>
		<div class="col-sm-10">
			<input value="01032016" type="text" name="data" id="data"
				class="form-control date" tabindex="1" maxlength="10" required="true" data-msg-required="Campo obrigatório"/>
		</div>
	</div>
	<div class="form-group">
		<label for="cnpj" class="col-sm-2 control-label">Cnpj: </label>
		<div class="col-sm-10">
			<input value="" type="text" name="cnpj" id="cnpj" data-rule-cnpj="true"
				class="form-control cnpj" tabindex="1" maxlength="18"/>
		</div>
	</div>

	<div class="form-group">
		<div class="col-xm-12 col-sm-9 col-sm-offset-2"	style="text-align: left;">
			<a href="" data-action="participante.save" id="btnAddTab2"	class="btn btn-success btn-sm">Gravar</a>
		</div>
	</div>
</form>