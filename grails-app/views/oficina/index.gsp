<!-- view: /views/oficina/index.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<%-- este arquivo contem a estrutura de abas para o módulo de oficinas --%>
<div id="gerenciarOficina">
   <div class="row">
      <div class="col-sm-12">
         <div class="panel panel-default">
            <div class="panel-heading">
               <span>Módulo de Avaliação</span>
               <span id="span-oficina-selecionada" style="color:blue;display:none;">&nbsp;</span>
               <button id="btnClose" data-action="app.unloadModule" class="btn btn-default pull-right btn-xs" title="Fechar">
               <span class="glyphicon glyphicon-remove btnClose"></span>
               </button>
            </div>
            <div class="panel-body" id="fichaContainerBody">

               <input type="hidden" value="" name="sqOficinaSelecionada" id="sqOficinaSelecionada">
               <input type="hidden" value="" name="noOficinaSelecionada" id="noOficinaSelecionada">

               <form id="frmGerOficina" class="form-inline" role="form">
                   <input type="hidden" id="sqCicloAvaliacao" name="sqCicloAvaliacao" value="${listCicloAvaliacao[0]?.id}"/>
               </form>
               <br>
               %{-- inicio abas oficina --}%
               <div id="divTabs" class="hidden">
                  <ul class="nav nav-tabs" id="ulTabsOficina">
                     <li id="liTabCadastro"     class="active"><a data-action="oficina.tabClick" xdata-url="" data-container="tabCadastro" data-params="" data-toggle="tab" href="#tabCadastro">Cadastro</a></li>
                     <li id="liTabFicha"        class=""><a data-action="oficina.tabClick" data-url="oficina/getFormSelecionarFicha" data-container="tabFicha"        data-on-load="oficina.ficha.init"        data-reload="false" data-params="sqOficinaSelecionada" data-toggle="tab" href="#tabFicha">Fichas</a></li>
                     <li id="liTabParticipante" class=""><a data-action="oficina.tabClick" data-url="oficina/getFormParticipante"    data-container="tabParticipante" data-on-load="oficina.participante.init" data-reload="false" data-params="sqOficinaSelecionada" data-toggle="tab" href="#tabParticipante">Participantes</a></li>
                     <li id="liTabMemoria"      class=""><a data-action="oficina.tabClick" data-url="oficina/getFormMemoria"         data-container="tabMemoria" data-on-load="oficina.memoria.init"            data-reload="false" data-params="sqOficinaSelecionada" data-toggle="tab" href="#tabMemoria">Memória Reunião</a></li>
                     <li id="liTabPapel"        class=""><a data-action="oficina.tabClick" data-url="oficina/getFormPapel"           data-container="tabPapel"        data-on-load="oficina.papel.init"        data-reload="true" data-params="sqOficinaSelecionada" data-toggle="tab" href="#tabPapel">Função</a></li>
                     <li id="liTabExecucao"     class=""><a data-action="oficina.tabClick" data-url="oficina/getFormExecucao"        data-container="tabExecucao"     data-on-load="oficina.execucao.init"     data-reload="true"  data-params="sqOficinaSelecionada" data-toggle="tab" href="#tabExecucao">Execução</a></li>
                  </ul>
                  %{-- Container de todas abas --}%
                  <div id="tabContent" class="tab-content">
                     %{-- inicio aba Cadastro --}%
                     <div role="tabpanel" class="tab-pane tab-pane-ficha active" id="tabCadastro">
                        <g:render template="formCadOficina"  model="${pageScope.variables}"></g:render>
                     </div>

                     <div role="tabpanel" class="tab-pane tab-pane-ficha" id="tabFicha">
                     </div>

                     <div role="tabpanel" class="tab-pane tab-pane-ficha" id="tabParticipante">
                     </div>

                     <div role="tabpanel" class="tab-pane tab-pane-ficha" id="tabMemoria">
                     </div>

                     <div role="tabpanel" class="tab-pane tab-pane-ficha" id="tabPapel">
                      </div>

                      <div role="tabpanel" class="tab-pane tab-pane-ficha" id="tabExecucao">
                      </div>

                  </div>
               %{-- fim container de todas abas --}%
               </div>
               %{-- fim divTabs --}%
            </div>
            %{-- fim panel body --}%
         </div>
         %{-- fim panel --}%
      </div>
      %{-- fim col 12 --}%
   <div>
   %{-- fim row --}%
</div>
%{-- fim gerenciar oficina --}%

%{--div auxiliar para exibir popup no mapa ao clica-lo --}%
%{--<div class="point-popup" id="pointPopup" style="display:none;">--}%
    %{--<h2>DivPopup</h2>--}%
%{--</div>--}%

<!-- janela modal para alterar a função do participante na reunião preparatória -->
<div class="modal" id="modalSelectFuncaoParticipante" tabindex="-1" role="dialog" data-sq-oficina-memoria-participante="">
   <div class="modal-dialog" role="document">
       <div class="modal-content">
           <div class="modal-header">
               <span>Alterar função do participante</span>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                   <span aria-hidden="true">&times;</span>
               </button>
           </div>
           <div class="modal-body">
               <h4 id="modalNomeParticipante">&nbsp;</h4>
               <select class="form-control" id="selectAlterarFuncaoParticipante">
                   <g:each var="item" in="${ listPapeis }">
                       <option  value="${item.id}">
                           ${item.descricao}
                       </option>
                   </g:each>
               </select>
           </div>
           <div class="modal-footer">
               <button type="button" class="btn btn-primary" onclick="oficina.saveFuncaoParticipanteMemoriaReuniao()">Gravar</button>
               <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
           </div>
       </div>
   </div>
</div>
