<!-- view: /views/oficina/_divGridExeFichasNaoLc.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<table id="table${gridId}" class="table table-striped table-sorting">
    <thead>
    %{-- PAGINACAO --}%
    <tr id="tr${gridId}Pagination">
        <td colspan="10" >
            <div class="row">
                <div class="col-sm-8 text-left"  style="min-height:0;height: auto;overflow: hidden;" id="div${(gridId ?: pagination?.gridId)}PaginationContent">
                    <g:if test="${pagination?.totalRecords}">
                        <g:gridPagination  pagination="${pagination}"/>
                    </g:if>
                </div>
                %{-- CAMPO LOCALIZAR --}%
                <div class="col-sm-4">
                    <input type="text" placeholder="localizar..." class="form-control form-control-sm fld250 mt10 hidden"
                           id="inputLocalizar${gridId}"
                           style="float:right;"
                           data-grid-id="${gridId}"
                           onkeyup="gridSelectRows(this)" />
                </div>
            </div>
        </td>
    </tr>


    %{-- TITULOS --}%
    <tr id="tr${gridId}Columns" data-grid-id="${gridId}">
        <th style="width:30px;">#</th>
        %{--  COLUNA CHECKBOX--}%
        <th style="width:30px;"><i title="Marcar/Desmarcar Todos" data-grid-id="${gridId}" data-action="oficina.checkUncheckAll" class="glyphicon glyphicon-unchecked"></i></th>
        <th style="width:200px;">Nome científico</th>
        <th style="width:100px;">Agrupamento</th>
        <th style="width:100px;">Categoria</th>
        <th style="width:100px">Critério</th>
        <th style="min-width:400px;">Justificativa</th>
        <th style="width:90px">Data</th>
        <th style="width:80px">Situação<br>oficina</th>
        <g:if test="${ canModify }">
            <th style="width:100px">Ação</th>
        </g:if>
    </tr>
    </thead>

    <tbody>
    <g:if test="${listFichas?.size() > 0 }">
        <g:each var="item" in="${listFichas}" status="lin">
            <g:set var="noCientificoItalico" value="${br.gov.icmbio.Util.ncItalico(item.nm_cientifico, true, true, item.co_nivel_taxonomico)}"></g:set>
            <g:set var="noCientificoItalicoAutorAno" value="${br.gov.icmbio.Util.ncItalico(item.nm_cientifico,false, true, item.co_nivel_taxonomico)}"></g:set>
            <tr id="tr-${item.sq_ficha}" data-lc="N">

                %{--COLUNA NUMERAÇÃO--}%
                %{--<td class="text-center">${contadorLinha++}</td>--}%
                %{--COLUNA NUMERACAO--}%
                <td style="width:60px;min-width:60px;" class="text-center">
                    <span>${ lin + ( pagination?.rowNum ?: 1 ) }</span>
                </td>

                %{--COLUNA CHECKBOX--}%
                <td class="text-center">
                    <g:if test="${ item.nu_versao || (item.cd_situacao_oficina_ficha ==~ /EXCLUIDA|COMPILACAO/ ) }">
                        <span>&nbsp;</span>&nbsp;
                    </g:if>
                    <g:else>
                        <input id="chkFicha_${item.sq_oficina_ficha}" name="sqFicha[${item.sq_ficha}]" value="${item.sq_ficha}"  type="checkbox" class="checkbox-lg ignoreChange"/>
                    </g:else>
                </td>

                %{--COLUNA NOME CIENTIFICO--}%
                <td>
                    <g:if test="${ ! item.nu_versao }">
%{--  contexto:validacao  ${ raw('<a href="javascript:void(0);" title="Visualizar ficha" onClick="showFichaCompleta('+item.sq_ficha+',\''+noCientificoItalicoAutorAno+'\',true,\'validacao\',\'oficina.execucao.updateGrides\','+(canModify?true:false)+',\'\',\''+ item.sq_oficina_ficha+'\')">'+noCientificoItalico+'</a>')}--}%
                        ${ raw('<a href="javascript:void(0);" title="Visualizar ficha" onClick="showFichaCompleta('+item.sq_ficha+',\''+noCientificoItalicoAutorAno+'\',true,\'oficina\',\'oficina.execucao.updateGrides\','+(canModify?true:false)+',\'\',\''+ item.sq_oficina_ficha+'\')">'+noCientificoItalico+'</a>')}
                    </g:if>
                    <g:else>
                        ${raw('<a href="#" title="Clique para ver a ficha completa." onClick="showFichaCompleta('+item.sq_ficha+',\''+noCientificoItalicoAutorAno+'\',true,\'oficina\',\'oficina.execucao.updateGrides\',false,\'\',\''+ item.sq_oficina_ficha+'\','+(item.sq_ficha_versao?:0) +')">' + noCientificoItalico + '</a>'+
                            '&nbsp;<a href="javascript:void(0);" title="Clique para ver a versão da ficha." onClick="showFichaVersao({sqFichaVersao:' +
                            item.sq_ficha_versao+'})">V.'+item.nu_versao+'</a>'
                        )}
                    </g:else>
                </td>

                %{--COLUNA AGRUPAMENTO--}%
                <td class="text-center">${item.ds_agrupamento_oficina_ficha}</td>

                %{--COLUNA CATEGORIA--}%
                <td class="text-center">${raw( ( item?.de_categoria_oficina_ficha ? '<span class="cursor-pointer" title="'+item.de_categoria_oficina_ficha+'">' + item.cd_categoria_oficina_ficha+'</span>' : '')
                    + ( item.cd_categoria_sistema_oficina_ficha == 'CR' && item?.st_possivelmente_extinta=='S' ? '<br/><span class="blue">(PEX)</span>':'') )}</td>

                %{--COLUNA CRITERIO--}%
                <td class="text-center">${item.ds_criterio_iucn_oficina_ficha}</td>

                %{--   || item.nu_versao == item.nu_ultima_versao )--}%
                <g:if test="${ ! item.nu_versao && item.cd_situacao_oficina_ficha ==~ /VALIDADA|EXCLUIDA|POS_OFICINA/}">
                    <td>
                        <g:if test="${ canModify && !item.nu_versao }">
                            <button id="btn-editar-justificativa-${item.sq_oficina_ficha}" data-action="oficina.execucao.editarJustificativa" data-sq-oficina-ficha="${item.sq_oficina_ficha}" data-sq-ficha="${item.sq_ficha}" class="btn btn-default btn-xs">Editar</button><br/>
                        </g:if>
                    %{--<div id="div-justificativa-${item.sq_oficina_ficha}" data-id="${item.sq_oficina_ficha}" class="div-texto-longo">${raw( item.dsJustificativa ) }</div>--}%
                        <div id="div-justificativa-${item.sq_oficina_ficha}" data-lc="N" data-id="${item.sq_oficina_ficha}" data-id-ficha="${item.sq_ficha}" class="text-justify">
                            <g:renderLongText table="oficina_ficha" id="${item.sq_oficina_ficha}" column="ds_justificativa" text="${raw(item.ds_justificativa_oficina_ficha)}"/>
                        </div>

                    </td>
                </g:if>
                <g:else>
                    <td class="text-justify">
                        <g:renderLongText table="oficina_ficha" id="${item.sq_oficina_ficha}" column="ds_justificativa" text="${raw(item.ds_justificativa_oficina_ficha)}"/>
                    </td>
                </g:else>

            %{--COLUNA DATA AVALIACAO --}%
                <td class="text-center">${item?.dt_avaliacao_oficina_ficha?.format('dd/MM/yyyy HH:mm:ss') }</td>

                %{--COLUNA SITUACAO--}%
                <td class="text-center ${ ( item.cd_situacao_oficina_ficha=='EXCLUIDA') ? 'red' :
                    ( item.cd_situacao_oficina_ficha=='POS_OFICINA' || item.cd_situacao_oficina_ficha=='VALIDADA' ) ? 'green' :
                        item.st_transferida  ? 'orange' :  item.cd_situacao_oficina_ficha == 'AVALIADA_REVISADA' ? 'blue' : 'black' }">
                    ${raw( ( item.cd_situacao_oficina_ficha=='COMPILACAO' ? 'Transferida' : item.ds_situacao_oficina_ficha) ) }
                </td>

            %{--COLUNA ACOES--}%
                <g:if test="${ canModify }">
                    <td class="td-actions">

%{--                        <g:if test="${ ! item.nu_versao || item.nu_versao == item.nu_ultima_versao }">--}%
                        <g:if test="${ ! item.nu_versao || item.cd_situacao_oficina_ficha=='EXCLUIDA'}">
                        %{-- BOTÃO TRANSFERIR --}%

                            <g:if test="${item.cd_situacao_oficina_ficha != 'EXCLUIDA'}">
                                <button type="button" title="clique aqui para transferir a ficha" data-action="oficina.execucao.transferirRetornarFicha" data-situacao="${item.cd_situacao_oficina_ficha=='COMPILACAO' ? 'transferida':'consulta'}" data-descricao="${raw(noCientificoItalico)}" data-sq-oficina-ficha="${item?.sq_oficina_ficha}" data-sq-oficina="${params?.sqOficina}" data-sq-ficha="${item.sq_ficha}" class="btn btn-warning btn-xs btn-email black fld80 ${item.cd_situacao_oficina_ficha=='COMPILACAO' ? 'bgOrangeDisabled':''}">
                                    <span>${item.cd_situacao_oficina_ficha=='COMPILACAO' ? 'Retornar':'Transferir'}</span>
                                </button>
                            </g:if>
                            <g:else>
                                <button type="button" title="ficha excluída"  disabled="true" class="btn btn-warning btn-xs btn-email black fld80">
                                    <span>${item.cd_situacao_oficina_ficha=='COMPILACAO' ? 'Retornar':'Transferir'}</span>
                                </button>
                            </g:else>
                        %{-- BOTÃO EXCLUIR --}%
                            <button type="button" title="clique para excluir/restaurar a ficha da oficina" data-action="oficina.execucao.excluirRestaurarFicha" data-situacao="${item.cd_situacao_oficina_ficha=='EXCLUIDA'?'excluida':'consulta'}" data-descricao="${raw(noCientificoItalico)}" data-sq-oficina-ficha="${item?.sq_oficina_ficha}" data-sq-oficina="${params?.sqOficina}" data-sq-ficha="${item.sq_ficha}" class="btn btn-danger btn-xs btn-email fld80 black ${item.cd_situacao_oficina_ficha=='EXCLUIDA'?'bgRedDisabled':''}" >
                                <span>${item.cd_situacao_oficina_ficha=='EXCLUIDA'?'Restaurar':'Excluir'}</span>
                            </button>
                        </g:if>
                        <g:else>
                            <span>&nbsp;</span>
                        </g:else>
                    </td>
                </g:if>
            </tr>
        </g:each>
    </g:if>
    <g:else>
        <tr id="GridExeFichaNaoLcEmpty">
            <td colspan="13" class="text-center">
                <g:if test="${params.action == 'getFormExecucao'}">
                    <h5>Carregando...</h5>
                </g:if>
                <g:else>
                    <h5>Nenhuma ficha encontrada!</h5>
                </g:else>
            </td>
        </tr>
    </g:else>
    </tbody>
</table>
