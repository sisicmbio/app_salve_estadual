<!-- view: /views/oficina/_divGridOficinaAnexo.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>Arquivo</th>
        <th>Legenda</th>
        <th>Ação</th>
    </tr>
    </thead>
    <tbody>
    <g:each var="item" in="${listAnexos}">
        <tr>
            <td class="text-left" style="width:200px;min-width:200px;">
                <g:if test="${item.de_tipo_conteudo.indexOf('image')==0 && item.de_tipo_conteudo.indexOf('tiff') ==-1 }">
                    <div class="text-center w100p">
                        <img src="/salve-estadual/oficina/getAnexoMemoria/${item.sq_oficina_memoria_anexo}/thumb" data-id="${item.sq_oficina_memoria_anexo}" alt="${item.no_arquivo}" class="img-rounded img-preview-grid"/>
                    </div>
                </g:if>
                <g:else>
                    <span class="nowrap">
                            ${item.no_arquivo}
                    </span>
                </g:else>
            </td>
            <td class="text-center" style="width:auto">${item.de_legenda}</td>
            <td style="width:30px;text-align: right !important;" class="td-actions">
                <a class="fld btn btn-default btn-xs btn-download" target="_blank" title="Baixar arquivo!" href="/salve-estadual/oficina/getAnexoMemoria/${item.sq_oficina_memoria_anexo}">
                    <span class="glyphicon glyphicon-download"></span>
                </a>
                <a data-action="oficina.memoria.deleteAnexo" data-descricao="${item.no_arquivo}" data-update-grid="divGridAnexoOficinaMemoria" data-id="${item.sq_oficina_memoria_anexo}" class="fld btn btn-default btn-xs btn-delete" title="Excluir">
                    <span class="glyphicon glyphicon-remove"></span>
                </a>
            </td>
        </tr>
    </g:each>
    </tbody>
</table>
