<!-- view: /views/oficina/_list.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<table class="table table-hover table-striped table-condensed table-bordered">
    <thead>
        <th>#</th>
        <th>Nome</th>
        <th>Sigla</th>
        <th>Local</th>
        <th>Período</th>
        <th>Tipo</th>
        <th>Unidade</th>
        <th>Ação</th>

    </thead>
    <tbody>
        <g:each in="${data}" var="item" status="i">
            <tr>
                <td class="text-center">${i + 1}</td>
                <td>${ item?.noOficina }</td>
                <td>${ item?.sgOficina }</td>
                <td>${ item?.deLocal }</td>
                <td class="text-center">${item?.dtInicio.format('dd/MM/yyyy') + ' a '+ item?.dtFim.format('dd/MM/yyyy')}</td>
                <td>${item?.tipoOficina.descricao}</td>
                <td>${item?.unidade.sigla}</td>

                %{-- Botoes --}%
                <td style="text-align:center;">
                    <a data-id="${item?.id}" data-action="oficina.edit" class="btn btn-default btn-xs btn-update" data-toggle="tooltip" title="Alterar">
                        <span class="glyphicon glyphicon-pencil"></span>
                    </a>
                    <a data-id="${item?.id}" data-action="oficina.delete" class="btn btn-default btn-xs btn-delete" data-toggle="tooltip" title="Excluir">
                        <span class="glyphicon glyphicon-remove"></span>
                    </a>
                </td>
            </tr>
        </g:each>
    </tbody>
</table>