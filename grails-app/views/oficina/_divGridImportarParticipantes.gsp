<%@ page import="br.gov.icmbio.Util" %>
<!-- view: /views/oficina/_divGridImportarParticipante.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<table class="table table-borderless table-condensed table-hover table-informativos mt10" id="tableOficinaImportarParticipantes">
    <thead>
    <tr>
        <th class="text-center">#</th>
        <th style="width:40px;"
            class="no-export"><i title="Marcar/Desmarcar Todos"
                                 onClick="checkUncheckAll({},this)"
                                 class="glyphicon glyphicon-unchecked"></i></th>

        <th style="width: auto;">Nome</th>
        <th style="width: 150px;">Instituição</th>
    </tr>
    </thead>
    <tbody>
    <g:if test="${listParticipantes}">
        <g:each var="item" in="${listParticipantes}" status="i">
            <tr id="tr-grid-importar-particiante-${item.sq_ciclo_consulta_pessoa}">
                <td class="text-center">${i+1}</td>
                <td class="text-center">
                    <input id="sqOficinaParticipanteImportar_${item.sq_ciclo_consulta_pessoa}" name="sqOficinaParticipanteImportar[${item.sq_ciclo_consulta_pessoa}]" onChange="oficina.participante.chkImportarParticipanteChange(this)" value="${item.sq_ciclo_consulta_pessoa}" type="checkbox" class="checkbox-lg ignoreChange"/>
                </td>
                <td class="text-left">${Util.capitalize( item?.no_pessoa )}</td>
                <td>${item.sg_instituicao}</td>
            </tr>
        </g:each>
    </g:if>
    <g:else>
        <td colspan="4">
            <g:if test="${listParticipantes == null}">
                <h4>Selecione uma consulta ou revisão para exibir os participantes</h4>
            </g:if>
            <g:else>
                <h4 class="red">Nenhum participante com a instituição preenchida.</h4>
            </g:else>
        </td>
    </g:else>
    </tbody>
</table>
