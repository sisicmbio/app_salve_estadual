<!-- view: /views/oficina/_formAnexo.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
%{-- Formulário para adição de anexos na oficina e reunião preparatória --}%
<div class="container-fluid text-left">
    <form name="frmAnexoOficina" id="frmAnexoOficina" class="form-inline" role="form" enctype='multipart/form-data'>
        <div class="fld form-group" id="divOficinaAnexo">
            <label for="file">Selecionar Anexo</label>
            <br />

            %{-- data-max-image-width="1024" --}%
            <input name="fldOficinaAnexo" type="file" id="file"
                   data-change="oficina.execucao.onAnexoFileChange"
                   class="file-loading" required="true" accept="*/*"
            data-show-preview="true"
            data-show-upload="false"
            data-show-caption="false"
            data-show-remove="true"
            data-max-file-count="1"
            data-allowed-file-extensions='["pdf","jpg", "jpeg", "png","zip","doc","docx","odt","xls","xlsx","ods","ppt","bmp","odp"]'
            data-main-class="input-group-sm"
            data-max-file-size="20000"
            data-preview-file-type="object">
        </div>
        <g:if test="${ ! params?.sqOficinaMemoria }">
            <br style="display:none;" id="brInRelatorioFinal">
            <div class="fld checkbox" style="display:none;" id="divInRelatorioFinal">
                <label for="inRelatorioFinal" class="control-label" title="Indicar que é o documento final da oficina e poderá ser disponibilizado para a assinatura eletrônica." style="color:#00008b;font-size:1.2rem !important;">Este anexo é o documento final?
                <input name="inRelatorioFinal" id="inRelatorioFinal"
                       data-change="oficina.execucao.inRelatorioFinalChange"
                       type="checkbox"
                       class="fld checkbox-lg" value="S"/>
                </label>
            </div>
        </g:if>
        <br class="fld" />
        <div class="fld form-group">
            <label for="deLegendaOficinaAnexo" class="control-label" title="Breve comentário sobre o doc/foto anexada.">Legenda</label>
            <br>
            <input name="deLegendaOficinaAnexo"
                   id="deLegendaOficinaAnexo" type="text" class="fld form-control fld600" value=""/>
        </div>
        <div class="fld panel-footer">
            <g:if test="${ params?.sqOficinaMemoria }">
                <a id="btnSaveFrmOficinaAnexo" data-action="oficina.memoria.saveAnexo"
                   data-sq-oficina-memoria="${ params?.sqOficinaMemoria?:'' }"
                   class="fld btn btn-success">Gravar Arquivo</a>
            </g:if>
            <g:else>
                <a id="btnSaveFrmOficinaAnexo" data-action="oficina.execucao.saveAnexo"
                   data-sq-oficina="${ params.sqOficina }"
                   class="fld btn btn-success">Gravar Arquivo</a>
            </g:else>
        </div>
    </form>
</div>