<!-- view: /views/oficina/_divGridSelFicha -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<g:if test="${pagination && pagination.pages > 1 }">
    <div style="display:block"
         data-sq-ciclo-avaliacao="${params.sqCicloAvaliacao}"
         data-sq-oficina="${params.sqOficina}"
         data-grid-hash="${pagination.hash}"
         data-grid-total-rows="${pagination.totalRows}"
         data-grid-page-size="${pagination.pageSize}"
         data-grid-controller="${params?.controller}"
         data-grid-action="${params?.action}"
         data-grid-on-page-change="oficina.ficha.gride1PageChange"
         data-grid-container-id="divTbOficinaSelFicha">
        <button class="button btn-xs btn-link btn-secondary btn-pagination-prev"
                title="Página anterior"
                onclick="loadPaginateArrayPage(this)"
                data-grid-page="${pagination.page-1}"
                ><i class="fa fa-chevron-left"></i></button>
        <select class="ml5 mr5 fld50 sel-pagination-pages" id="selPagination1"
                onchange="loadPaginateArrayPage(this)">
            <g:each var="pageNum" in="${(1..pagination.pages)}">
                <option value="${pageNum}" ${(pagination.page.toInteger() == pageNum.toInteger() ? 'selected' : '' ) }>${pageNum}</option>
            </g:each>
        </select>
        <button class="button btn-xs btn-link btn-secondary btn-pagination-next"
                title="Página seguinte"
                onclick="loadPaginateArrayPage(this)"
                data-grid-page="${pagination.page+1}"
        ><i class="fa fa-chevron-right"></i></button>
        <span>${pagination.totalRows} fichas</span>
    </div>
</g:if>
<div style="display:block;overflow-y:auto;max-height:300px;padding:5px" id="divTbOficinaSelFicha">
    <h4>${listFichas ? listFichas.size() : '0'}&nbsp;Fichas / <span class="total-selecionadas">0 selecionada.</span></h4>
    <table class="table table-sm table-hover table-striped table-condensed table-bordered" id="tbOficinaSelFicha">
        <thead>
        <th style="width:30px;">#</th>
        <th style="width:30px;"><i id="checkUncheckAll" title="Marcar/Desmarcar Todos" data-action="oficina.checkUncheckAll" data-container="divTbOficinaSelFicha" class="glyphicon glyphicon-unchecked check-all"></i></th>
        <th style="width:auto;">Nome Científico</th>
        <th style="width:200px;">Unidade</th>
        <th style="width:200px;">Grupo Taxonômico</th>
        <th style="width:150px;">Situação</th>
        </thead>
        <tbody>
        <g:if test="${( listFichas?.size() > 0 ) }">
            <g:each in="${listFichas}" var="item" status="i">
                <tr id="gdFichaRow_${i + 1}">
                    <td class="text-center">${i + 1 + ( params?.gridRowNum ? params.gridRowNum.toInteger() : 0 )}</td>
                    <td class="text-center">
                        <input id="sqFicha_${item.sq_ficha}"
                               name="sqFicha[${item.sq_ficha}]"
                               value="${item.sq_ficha}"
                               data-situacao="${item.cd_situacao_ficha_sistema}"
                               onChange="oficina.ficha.chkFichaChange(this)"
                               type="checkbox"
                               class="checkbox-lg ignoreChange"
                               data-action="contarChecked"
                               data-container="divTbOficinaSelFicha"/>
                    </td>
                    <td>${raw( br.gov.icmbio.Util.ncItalico(item.nm_cientifico, true, true, item.co_nivel_taxonomico ) )}</td>
                    <td class="text-center">${item?.sg_unidade_org}</td>
                    <td class="text-center">${item?.ds_grupo}</td>
                    <td class="text-center">${item?.ds_situacao_ficha}</td>
                </tr>
            </g:each>
        </g:if>
        <g:else>
            <tr>
                <td colspan="6" align="center">
                    <g:if test="${oficina.tipoOficina.codigoSistema=='OFICINA_AVALIACAO'}">
                        <g:if test="${isPrimeiroCiclo}">
                            <h4>Nenhuma ficha disponível!</h4><h5 class="red">Para o PRIMEIRO CICLO é necessário que a ficha esteja CONSOLIDADA ou VALIDADA e ainda não tenha sido selecionada no gride abaixo!</h5>
                        </g:if>
                        <g:else>
                            <h4>Nenhuma ficha disponível!</h4><h5 class="red">É necessário que a ficha esteja CONSOLIDADA e ainda não tenha sido selecionada no gride abaixo!</h5>
%{--                        <h4>Nenhuma ficha disponível!</h4><h5 class="red">É necessário que a ficha esteja com ponto focal defindo, o ponto focal seja o mesmo informado no cadastro da oficina a situação seja CONSOLIDADA e ainda não tenha sido selecionada no gride abaixo!</h5>--}%
                        </g:else>
                    </g:if>
                    <g:elseif test="${oficina.tipoOficina.codigoSistema=='OFICINA_VALIDACAO'}">
%{--                        <h4>Nenhuma ficha disponível!</h4><h5 class="red">É necessário que a ficha esteja AVALIADA REVISADA e ainda não tenha sido selecionada no gride abaixo!</h5>--}%
                        <h4>Nenhuma ficha disponível!</h4><h5 class="red">É necessário que a ficha esteja AVALIADA REVISADA e ainda não tenha sido selecionada no gride abaixo!</h5>
                    </g:elseif>
                    <g:else>
                        <h4>Nenhuma ficha encontrada!</h4>
                    </g:else>
                </td>
            </tr>
        </g:else>
        </tbody>
    </table>
</div>
