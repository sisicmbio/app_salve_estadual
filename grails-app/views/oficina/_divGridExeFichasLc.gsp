<!-- view: /views/oficina/_divGridExeFichasLc.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<style>
p.elipse {
    max-width: 900px;
    width: auto;
}
</style>
<table id="table${gridId}" class="table table-striped table-sorting">
    <thead>
    %{-- PAGINACAO --}%
    <tr id="tr${gridId}Pagination">
        <td colspan="8" >
            <div class="row">
                <div class="col-sm-8 text-left"  style="min-height:0;height: auto;overflow: hidden;" id="div${(gridId ?: pagination?.gridId)}PaginationContent">
                    <g:if test="${pagination?.totalRecords}">
                        <g:gridPagination  pagination="${pagination}"/>
                    </g:if>
                </div>
                %{-- CAMPO LOCALIZAR --}%
                <div class="col-sm-4">
                    <input type="text" id="inputLocalizar${gridId}" placeholder="localizar..." class="form-control form-control-sm fld250 mt10 hidden"
                           style="float:right;"
                           data-grid-id="${gridId}"
                           onkeyup="gridSelectRows(this)" />
                </div>
            </div>
        </td>
    </tr>

    %{-- TITULOS --}%
    <tr id="tr${gridId}Columns" data-grid-id="${gridId}">

        %{-- COLUNA NUNERAÇÃO DA LINHA--}%
        <th style="width:60px;">#</th>

        %{--  COLUNA CHECKBOX--}%
        <th style="width:30px;"><i title="Marcar/Desmarcar Todos" data-grid-id="${gridId}" data-action="oficina.checkUncheckAll" class="glyphicon glyphicon-unchecked"></i></th>

        %{-- COLUNA NOME CIENTIFICO--}%
        <th style="width:200px;">Táxon</th>
        <th style="width:100px;">Categoria</th>
        <th style="width:auto">Justificativa</th>
        <th style="width:90px;" >Data</th>
        <th style="width:80px;">Situação<br>oficina</th>
        <g:if test="${ canModify }">
            <th style="width:150px;">Ação</th>
        </g:if>
    </tr>
    </thead>

    <tbody>
    <g:if test="${ listFichas?.size() > 0 }">
        <g:each var="item" in="${listFichas}" status="lin">
        %{-- se a categoria da oficina for LC e a final tambem for LC --}%
            <g:set var="noCientificoItalico" value="${br.gov.icmbio.Util.ncItalico(item.nm_cientifico,true)}"></g:set>
            <g:set var="noCientificoItalicoAutorAno" value="${br.gov.icmbio.Util.ncItalico(item.nm_cientifico,false)}"></g:set>
            <tr id="tr-${item.sq_ficha}" data-lc="S" data-id-sq-ficha="${item.sq_ficha}">
                %{--COLUNA NUMERACAO--}%
                <td style="width:60px;min-width:60px;" class="text-center">
                    <span>${ lin + ( pagination?.rowNum ?: 1 ) }</span>
                </td>

                %{--COLUNA CHECKBOX--}%
                <td class="text-center">
                    <g:if test="${ ! item.nu_versao }">
                        <input id="chkFicha_${item.sq_oficina_ficha}" name="sqFicha[${item.sq_ficha}]" value="${item.sq_ficha}"  type="checkbox" class="checkbox-lg ignoreChange"/>
                    </g:if>
                    <g:else>
                        <span>&nbsp;</span>
                    </g:else>
                </td>

                %{--COLUNA NOME CIENTIFICO--}%
                <td>
                    <g:if test="${ ! item.nu_versao }">
                        ${ raw('<a href="javascript:void(0);" title="Visualizar ficha" onClick="showFichaCompleta('+item.sq_ficha+',\''+ noCientificoItalicoAutorAno +'\',false,\'oficina\',\'oficina.execucao.updateGrides\','+(canModify?true:false)+',\'\',\'' + item.sq_oficina_ficha +'\')">'+noCientificoItalico+'</a>')}
                    </g:if>
                    <g:else>
                        ${ raw('<a href="javascript:void(0);" title="Clique para ver a versão da ficha." onClick="showFichaVersao({sqFichaVersao:' +
                            item.sq_ficha_versao+'})">' +
                            noCientificoItalico + '</a>&nbsp;<span>V.'+item.nu_versao+'</span>') }
                    </g:else>
                </td>
%{-- MODELO

                <td>
                    <g:if test="${ ! item.nu_versao }">
                        ${ raw('<a href="javascript:void(0);" title="Visualizar ficha" onClick="showFichaCompleta('+item.sq_ficha+',\''+noCientificoItalicoAutorAno+'\',true,\'oficina\',\'oficina.execucao.updateGrides\','+(canModify?true:false)+',\'\',\''+ item.sq_oficina_ficha+'\')">'+noCientificoItalico+'</a>')}
                    </g:if>
                    <g:else>
                        ${raw('<a href="#" title="Clique para ver a ficha completa." onClick="showFichaCompleta('+item.sq_ficha+',\''+noCientificoItalicoAutorAno+'\',true,\'oficina\',\'oficina.execucao.updateGrides\',false,\'\',\''+ item.sq_oficina_ficha+'\','+(item.sq_ficha_versao?:0) +')">' + noCientificoItalico + '</a>'+
                            '&nbsp;<a href="javascript:void(0);" title="Clique para ver a versão da ficha." onClick="showFichaVersao({sqFichaVersao:' +
                            item.sq_ficha_versao+'})">V.'+item.nu_versao+'</a>'
                        )}
                    </g:else>
                </td>
--}%





















                %{--COLUNA CATEGORIA--}%
                <td class="text-center">
                    <g:if test="${ item.de_categoria_oficina_ficha }">
                        ${ raw( ( item?.de_categoria_oficina_ficha ? '<span class="cursor-pointer" title="'+item.de_categoria_oficina_ficha+'">' + item.cd_categoria_oficina_ficha+'</span>' : '') ) }
                    </g:if>
                    <g:elseif test="${item.ds_categoria_iucn_nacional}" >
                        ${ raw( ( item.ds_categoria_iucn_nacional ? '<span class="cursor-pointer" title="'+item.ds_categoria_iucn_nacional+'">' + item.cd_categoria_iucn_nacional+'</span>' : '') ) }
                    </g:elseif>
                </td>

            %{--COLUNA JUSTIFICATIVA--}%
                <g:if test="${ !item.nu_versao && item.cd_situacao_oficina_ficha == 'POS_OFICINA'}">
                    <td>
                        <g:if test="${ canModify }">
                            <button id="btn-editar-justificativa-${item.sq_oficina_ficha}" data-action="oficina.execucao.editarJustificativa" data-sq-oficina-ficha="${item.sq_oficina_ficha}" class="btn btn-default btn-xs">Editar</button><br/>
                        </g:if>
                        <div id="div-justificativa-${item.sq_oficina_ficha}" data-lc="S" data-id="${item.sq_oficina_ficha}" data-id-ficha="${item.sq_ficha}" class="text-justify">
                            <g:renderLongText table="oficina_ficha" id="${item.sq_oficina_ficha}"
                                              column="ds_justificativa"
                                              text="${item?.ds_justificativa_oficina_ficha}"/>
                        </div>
                    </td>
                </g:if>
                <g:else>
                    <td class="text-justify">
                        <g:if test="${ item.ds_justificativa_oficina_ficha}">
                            <g:renderLongText table="oficina_ficha" id="${item.sq_oficina_ficha}"
                                              column="ds_justificativa"
                                              text="${item?.ds_justificativa_oficina_ficha}"/>

                        </g:if>
                        %{--<g:elseif test="${item?.ds_justificativa_ficha}">
                            <g:renderLongText text="${item?.ds_justificativa_ficha}"/>
                        </g:elseif>--}%
                        <g:elseif test="${item?.sq_taxon_historico_avaliacao}" >
                            <g:renderLongText table="taxon_historico_avaliacao" id="${item.sq_taxon_historico_avaliacao}"
                                              column="tx_justificativa_avaliacao"
                                              text="${item?.tx_justificativa_aval_nacional}"/>
                        </g:elseif>
                    </td>
                </g:else>

            %{--COLUNA DATA AVALIACAO --}%
                <td class="text-center">${item?.dt_avaliacao_oficina_ficha?.format('dd/MM/yyyy HH:mm:ss') }</td>

                %{-- COLUNA SITUACAO --}%
                <td class="text-center ${ item.cd_situacao_oficina_ficha == 'POS_OFICINA' ? 'green' :
                    ( item.cd_situacao_oficina_ficha=='AVALIADA_REVISADA' ? 'blue' : '')}">${raw( item.ds_situacao_oficina_ficha ) }</td>

            %{--COLUNA ACOES--}%
                <g:if test="${ canModify }">
                    <td class="td-actions" style="text-align:left !important;" >
                        <g:if test="${ ! item.nu_versao }">
                            <button type="button" data-st-endemica-brasil="${item?.st_endemica_brasil}" data-action="oficina.execucao.confirmarLc" data-situacao="${item.cd_situacao_oficina_ficha=='POS_OFICINA'?'confirmada':'nao_confirmada'}" data-descricao="${raw( noCientificoItalico )}" data-sq-ficha="${item.sq_ficha}" data-sq-oficina-ficha="${item.sq_oficina_ficha}" class="btn btn-primary btn-xs btn-email">
                                <span>${item.cd_situacao_oficina_ficha=='POS_OFICINA' || item.cd_situacao_oficina_ficha=='AVALIADA_REVISADA' ? 'Não confirmar' : 'Confirmar' } LC</span>
                            </button>
                            <g:if test="${ item.cd_situacao_oficina_ficha != 'POS_OFICINA' && item.cd_situacao_oficina_ficha != 'AVALIADA_REVISADA' }">
                                <button type="button" data-action="oficina.execucao.avaliarLc" data-descricao="${raw(noCientificoItalico)}" data-sq-ficha="${item.sq_ficha}" data-sq-oficina-ficha="${item.sq_oficina_ficha}" class="btn btn-success btn-xs btn-email">
                                    <span>Avaliar</span>
                                </button>
                            </g:if>
                        </g:if>
                        <g:else>
                            <span>&nbsp;</span>
                        </g:else>
                    </td>
                </g:if>
            </tr>
        </g:each>
    </g:if>
    <g:else>
        <tr id="GridExeFichaLcEmpty">
            <td colspan="8" class="text-center">
                <g:if test="${params.action == 'getFormExecucao'}">
                    <h5>Carregando...</h5>
                </g:if>
                <g:else>
                    <h5>Nenhuma ficha encontrada</h5>
                </g:else>
            </td>
        </tr>
    </g:else>
    </tbody>
</table>
