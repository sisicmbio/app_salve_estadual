<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="api"/>
        <asset:javascript src="/api/validarDoc.js"/>
    </head>
    <body>
        <style>
            label {
                font-size:1.6rem !important;
            }
            input {
                max-width: 200px;
                display:inline !important;
                font-size:1.4rem !important;
                color:#0000FF !important;
                text-align: center !important;
                font-weight: bold;
            }
            #btnValidar {
                margin-top: 15px;
                width:200px;
                font-size:1.3rem !important;
            }
            #btnVoltar {
                margin-top:-5px !important;
                display:none;
                width:150px;
            }
            .grecaptcha-badge {
                bottom:25px !important;
            }
            </style>
             <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title"><b>Validação de Documento</b><button onClick="document.location.replace('validarDoc');" id="btnVoltar" class="btn btn-danger btn-xs pull-right">Voltar</button></h3>
                </div>
                <div class="panel-body" id="panelBody">
                    <div class="row">
                        <div class="col-sm-12 text-center">
                          <label>Código de Acesso</label>
                          <br>
                          <input type="text" id="codigoAcesso" class="form-control" value="${codigoAcesso}" autofocus>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <input type="hidden" name="g-recaptcha-response" id="g-recaptcha-response" value=""/>
                            <button id="btnValidar" class="btn btn-primary">Enviar</button>
                        </div>
                    </div>
                </div>
            </div>
        <div class="g-recaptcha" data-sitekey="6Lc76qQUAAAAAFWXugs0BTxi5pLvPS2MbhQZsZYy"></div>
        <script src="https://www.google.com/recaptcha/api.js?hl=pt-BR&render=6Lc76qQUAAAAAFWXugs0BTxi5pLvPS2MbhQZsZYy"></script>
        <script>
            /*grecaptcha.ready(function() {
                grecaptcha.execute('6Lfhw6QUAAAAACaQIAHy89Dat_8OFjxyWaZRCH9E', {action: 'homepage'}).then(function(token) {
                $("#g-recaptcha-response").val( token );
                });
            });

             */

        </script>
    </body>
</html>