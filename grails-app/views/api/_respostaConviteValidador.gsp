<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>${grailsApplication.metadata.getApplicationName().toUpperCase()}/Resposta Convite</title>
    <style>
        body{
            background-color: #d6dce9;
        }
    </style>
</head>

<body>
    <h2>Sistema de Avaliação do Risco de Extinção da Biodiversidade - SALVE Estadual</h2>
    <hr>
<center>
    <g:if test="${msg}">
        <h3>${raw(msg)}</h3>
    </g:if>
</center>
</body>
</html>
