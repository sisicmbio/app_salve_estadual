<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="api"/>
    </head>
    <body>
        <input type="hidden" id="hash" value="${hash}">
        <input type="hidden" id="fileName" value="${fileName}">
        <g:if test="${hash}">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title"><b>Assinatura Eletrônica do Documento Final da Oficina</b>
                        <div style="display:inline-block;float:right;">
                            <button class="btn btn-xs btn-primary" id="prev">Anterior</button>
                            <button class="btn btn-xs btn-primary" id="next">Próxima</button>
                            <button class="btn btn-xs btn-primary" id="last">Fim</button>
                            <span>Página: <span id="page_num"></span> / <span id="page_count"></span></span>
                        </div>
                    </h3>
                </div>
                <div class="panel-body" id="panelBody">
                    <div id="viewPdf" style="border:1px solid blue;overflow-y: auto;">
                        <canvas id="the-canvas" style="border:none; direction: ltr;width: 100%"></canvas>
                    </div>
                </div>
            </div>
            <g:if test="${ ! reg.dtAssinatura }">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <button id="btnAssinar" class="btn btn-danger btn-danger-assinar">Assinar documento</button>
                    </div>
                </div>
            </g:if>
        </g:if>
        <asset:javascript src="jspdf/pdf.js"/>
        <asset:javascript src="/api/assinarDocFinal.js" defer="true"/>
    </body>
</html>
