<!-- view: /views/traducaoFicha/_index.gsp -->
<!-- controller:${params?.controller} -->
<!-- action: ${params?.action} -->
<div id="traducaoFichaContainer">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <span>Módulo Tradução Ficha</span>
                    <a id="btnClose" data-action="app.unloadModule" class="btn btn-default pull-right btn-xs" title="Fechar">
                        <span class="glyphicon glyphicon-remove btnClose"></span>
                    </a>
                </div>

                <div class="panel-body">

                    <div id="divSelectCicloAvaliacao" class="form-group">
                        <input type="hidden" id="sqCicloAvaliacao" name="sqCicloAvaliacao" value="${listCiclosAvaliacao[0]?.id}"/>
                    </div>
                    <form id="frmTraducaoFicha" class="form-inline" style="display:none;" role="form">
                        %{--Filtro das fichas --}%
                        <div class="mt10" id="divFiltrosTraducaoFicha"></div>
                        %{-- botões  --}%
                        <div id="divContainerBotoesTraducaoFicha" class="text-right mt10" style="padding-bottom:5px;width:100%;max-width:1500px;">
                        <button type="button" id="btnExportarPlanilha"
                                title="Exportar para planilha"
                                data-action="traducaoFicha.exportarPlanilha" class="btn btn-primary btn-xs" disabled="true">
                            Exportar</span>
                        </button>

                    </div>

                            %{-- gride para selecionar ficha --}%
                        <div id="containerGridTraducaoFicha"
                             data-params="sqCicloAvaliacao"
                             data-container-filters-id="divFiltrosTraducaoFicha"
                             data-field-id="sq_ficha"
                             data-on-load="traducaoFicha.gridTraducaoFichaOnLoad"
                             data-url="traducaoFicha/getGridFichaTraducao">
                            <input type="hidden" id="gridTraducaoFichaSelectedRows" value="">
                            <g:render template="gridTraducaoFicha"
                                      model="[gridId: 'GridTraducaoFicha']"></g:render>
                        </div>
                        <div class="panel panel-footer hidden" id="divButtonsTraducaoFicha">
                            <button class="btn btn-primary" data-action="traducaoFicha.traduzirLote">Traduzir fichas selecionadas</button>
                            <button class="btn btn-danger" data-action="traducaoFicha.excluirTraducaoLote">Excluir tradução fichas selecionadas</button>
                            <button class="btn btn-warning" data-action="traducaoFicha.exportarPdf">Gerar PDF fichas (zip)</button>
                        </div>
                    </form>
                </div>
                %{-- fim panel body --}%
            </div>
            %{-- fim panel --}%
        </div>
        %{-- fim row 12 --}%
    </div>
    %{-- fim row --}%
</div>
%{-- fim container --}%
