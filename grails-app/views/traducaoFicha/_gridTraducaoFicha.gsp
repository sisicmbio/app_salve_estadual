<%@ page import="br.gov.icmbio.Util" %>
<!-- view: /views/traducaoFicha/_gridTraducaoFicha -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<table id="table${gridId}" class="mt10 table table-striped table-sorting">
    <thead>
    %{-- PAGINACAO --}%
    <tr id="tr${gridId}Pagination">
        <td colspan="10">
            <div class="text-left" style="min-height:0;height: auto;overflow: hidden" id="div${(gridId ?: pagination.gridId)}PaginationContent">
                <g:if test="${pagination?.totalRecords}">
                    <g:gridPagination pagination="${pagination}"/>
                </g:if>
            </div>
        </td>
    </tr>

    %{-- FILTROS --}%
    <tr id="tr${gridId}Filters" class="table-filters" data-grid-id="${gridId}">

        <td colspan="2">
            <div style="display:flex;flex-direction:row; justify-content:space-between;">
                <i class="fa fa-question-circle"
                   title="Utilize os campos ao lado para filtragem dos registros informando o valor a ser localizado e pressionando Enter. Para remover o filtro limpe o campo e pressione Enter novamente."></i>
                <i data-grid-id="${gridId}" class="fa fa-eraser" title="Limpar filtro(s)"></i>
                <i data-grid-id="${gridId}" class="fa fa-filter" title="Aplicar filtro(s)"></i>
            </div>
        </td>

        %{--        FILTRO COLUNA NOME CIENTIFICO--}%
        <td class="text-left" title="Informe o nome científico completo ou em parte para pesquisar. Ex: Aburria jacutinga ou apenas Aburria ou jacutinga">
            <input type="text" name="fldNmCientifico${gridId}" data-field="nmCientificoFiltro" value="${params?.nmCientifico ?: ''}" class="table-filter">
        </td>

        %{--        PERCENTUAIS--}%
        <td></td>

        %{--        REVISORES --}%
        <td></td>

        %{--        FILTRO PELA UNIDADE--}%
        <g:if test="${grideFiltroUnidade}">
            <td class="text-left" title="Informe sigla ou parte da sigla da unidade para pesquisar.">
                <input type="text" name="fldSgUnidade${gridId}" data-field="sgUnidadeFiltro" value="${params?.sgUnidadeOrg ?: ''}" class="table-filter">
            </td>
        </g:if>
        <g:else>
            <td></td>
        </g:else>

        %{--        FILTRO PELA SITUAÇÃO DA FICHA --}%
        <td class="text-left">
            <select name="fldSqSituacao${gridId}" class="table-filter" data-field="sqSituacaoFiltro">
                <option value=""></option>
                <g:each var="item" in="${listSituacaoFicha}">
                    <option value="${item.id.toString()}">${item.descricao}</option>
                </g:each>
            </select>
        </td>

        <td colspan="9"></td>
    </tr>

    %{--    TITULOS--}%
    <tr id="tr${gridId}Columns" data-grid-id="${gridId}">
        <th style="width:60px;">#</th>
        <th style="width:40px;" class="no-export"><i id="checkAll-${gridId}" title="Marcar/Desmarcar Todos" data-table-id="table${gridId}" data-action="traducaoFicha.selecionarTodas"
                                                     class="glyphicon glyphicon-unchecked"></i></th>
        <th style="min-width:230px;width:auto" class="sorting sorting_asc" data-field-name="ficha.nm_cientifico">Nome científico</th>

        <th style="width:250px;" class="" data-field-name="">Traducão / revisão</th>

        <th style="width:200px;" class="sorting" data-field-name="no_revisor">Revisor</th>

        <th style="width:200px;" class="sorting" data-field-name="uo.sg_unidade_org">Unidade responsável</th>

        <th style="width:150px;" class="sorting" data-field-name="situacao.ds_dados_apoio">Situação ficha</th>
        <th style="width:80px;">Ação</th>
    </tr>
    </thead>
    <tbody>
    <g:if test="${rows}">
        <g:each var="ficha" in="${rows}" status="i">

            <tr id="tr-${ficha.sq_ficha.toString()}"  data-sq-ficha="${ficha.sq_ficha.toString()}" class="text-center ${(ficha.co_nivel_taxonomico == 'SUBESPECIE' ? 'bgSubespecie ' : '') /*+ (ficha.cd_situacao_ficha_sistema == 'PUBLICADA' ? 'inherit' : 'inherit')*/}">

                %{-- PRIMEIRA COLUNA - NUMERO DA LINHA --}%
                <td class="text-center">${ i.toInteger() + ((pagination ? pagination?.rowNum : 1) as Number)}</td>

                %{-- SEGUNDA COLUNA - CHECKBOX --}%
                <td class="text-center">
                    <input name="chkSqFicha" onchange="traducaoFicha.chkSqFichaChange(this, ${i + 1})"
                           data-grid-id="${gridId}"
                           value="${ficha?.sq_ficha}" type="checkbox" class="checkbox-lg"/>
                </td>

                %{-- TERCEIRA COLUNA - NOME CIENTIFICO --}%
                <td class="text-left" style="padding-left:${raw(ficha.co_nivel_taxonomico == 'SUBESPECIE' ? '30px !important;' : '')}">
                    <a style="font-weight:bold;" href="javascript:void(0);" title="traduzir/revisar" data-action="traducaoFicha.revisar"
                       data-no-cientifico="${Util.ncItalico(ficha?.nm_cientifico?.toString(), false, true, ficha?.co_nivel_taxonomico?.toString() )}" data-sq-ficha="${ficha?.sq_ficha}">
                        ${raw(Util.ncItalico(ficha?.nm_cientifico?.toString(), true, true, ficha?.co_nivel_taxonomico?.toString() ) ) }
                    </a>
                    <div id="andamentoTraducaoWrapper${ficha?.sq_ficha}" style="display: none;">
                         <g:progressBar id="andamentoTraducao${ficha?.sq_ficha}" percentual="0" label="Traduzindo..."></g:progressBar>
                    </div>

            </td>

                %{-- PERCENTUAIS--}%
                <td>
                    <g:progressBar id="percentTraduzido${ficha?.sq_ficha}" percentual="${ficha?.percentual_traduzido?:'0'}" label="Traduzido:"></g:progressBar>
                    <g:progressBar id="percentRevisado${ficha?.sq_ficha}" percentual="${ficha?.percentual_revisado?:'0'}" label="Revisado:"></g:progressBar>
                </td>

                %{-- NOMES DO(S) REVISOR(ES) --}%
                <td class="text-left">${raw(ficha?.no_revisor?.split(';')?.collect{br.gov.icmbio.Util.nomeAbreviado( it )}?.join('<br>'))}
                </td>

                %{-- QUARTA COLUNA - UNIDADE RESPONSAVEL --}%
                <td>${ficha.sg_unidade_org}</td>

                %{-- SETIMA COLUNA - SITUACAO --}%
                <td class="text-center">${ficha.ds_situacao_ficha}</td>

                %{-- NONA COLUNA - BOTOES --}%
                <td class="td-actions">
                    <button type="button" data-action="traducaoFicha.print" data-no-cientifico="${ficha.nm_cientifico}"
                            data-sq-ficha="${ficha.sq_ficha}" class="btn btn-default btn-xs" data-toggle="tooltip"
                            data-placement="top" title="Imprimir Ficha" style="color:gray;">
                        <i class="fa fa-print btnPrint" aria-hidden="true"></i>
                    </button>
                </td>
            </tr>
        </g:each>
    </g:if>
    <g:else>
        <tr>
            <g:if test="${errorMessage}">
                <td colspan="9" class="text-center"><h4 class="red"><b>${errorMessage}</b></h4></td>
            </g:if>
            <g:else>
                <td colspan="9" class="text-center">
                    <g:if test="${params?.action=='getGridFichaTraducao'}">
                        <h4><b>Nenhuma ficha encontrada!</b></h4>
                    </g:if>
                </td>
            </g:else>
        </tr>
    </g:else>
    </tbody>
</table>
