//# sourceURL=/views/traducaoFicha/fichaRevisao.js
;var fichaRevisao = {
    revisaoAtual    :{},
    sqFicha         :'',
    $container      :'',
    init : function() {
        setTimeout(function(){
            // ler o id da ficha que esta sendo revisada
            fichaRevisao.sqFicha = $("#sqFichaTraduzirRevisar").val();
            // ler o container (DOM)
            fichaRevisao.$container = $('#containerTraducaoRevisaoFicha-'+fichaRevisao.sqFicha);
            // adicionar evento para tradução nos elementos com a classe 'traduzir'
            fichaRevisao.$container.find("*.traduzir").each(function(index, ele ){
                var $ele  = $(ele)
                var data = $ele.data();
                if( data && data.traduzir ) {
                    data.traduzir = data.traduzir.replace(/'/g,'"');
                    $ele.data({'traduzir' : JSON.parse( data.traduzir)} );
                    $ele.off('click').on('click',function(e){
                        var elementData = $(e.target).data();
                        var dataTraduzir = elementData.traduzir
                        var rndId = dataTraduzir.rndId;
                        dataTraduzir.txOriginal = $("#textoOriginal-"+rndId).text();
                        dataTraduzir.txTraduzido = $("#textoTraduzido-"+rndId).text();
                        dataTraduzir.txRevisado = $("#textoCorrigido-"+rndId).text();
                        dataTraduzir.contexto = 'revisao';
                        if( dataTraduzir.tipo == 'fixo' ) {
                            dataTraduzir.noTabelaTraducao = 'traducao_texto' // exibir a forma compacta da janela de traducao de texto fixo
                            //console.log(dataTraduzir);
                            modal.janelaTraducaoTextoFixo(dataTraduzir, null, function (res) {
                                /*
                                {
                                  tipo: 'fixo'
                                , rndId: 'rndXpto'
                                , txOriginal: 'Instituto Chico Mendes de Conservação da Biodiversidade - ICMBio'
                                , txTraduzido: 'Chico Mendes Institute for Biodiversity Conservation - ICMBio'
                                , contexto: 'revisao'
                                , saved:'N/S'
                                }
                                 */
                                if( res.inSaved == 'S') {
                                    $("#textoCorrigido-"+res.rndId).text(res.txRevisado);
                                    $("#textoTraduzido-"+res.rndId).text(res.txTraduzido);
                                }
                            });
                        } else if( dataTraduzir.tipo == 'apoio') {
                            modal.janelaTraducaoTextoFixo(dataTraduzir, null, function (res) {
                                if( res.inSaved == 'S') {
                                    var $eleTextoRevisado = $("#textoCorrigido-" + res.rndId);
                                    var $eleTextoTraduzido = $("#textoTraduzido-" + res.rndId);
                                    if( $eleTextoRevisado.size()==1 ) {
                                        $eleTextoRevisado.text(res.txRevisado);
                                        $eleTextoTraduzido.text(res.txTraduzido);
                                        // se for uma tabela e tiver celulas com valor repetido, alterar automaticamente
                                        $( ele ).closest('tbody').find('td > span[id^=textoCorrigido]').map( function(i,span){
                                            if( $( span ).text()==res.txOriginal || $( span ).text() == res.txTraduzido ) {
                                                $( span).text( res.txRevisado );
                                            }
                                        } )
                                    }
                                }
                            });
                        } else if( dataTraduzir.tipo == 'uso') {
                            modal.janelaTraducaoTextoFixo(dataTraduzir, null, function (res) {
                                if( res.inSaved == 'S') {
                                    $("#textoCorrigido-" + res.rndId).text(res.txRevisado);
                                    $("#textoTraduzido-" + res.rndId).text(res.txTraduzido);
                                }
                            });
                        } else if( dataTraduzir.tipo == 'pan') {
                            modal.janelaTraducaoTextoFixo(dataTraduzir, null, function (res) {
                                if( res.inSaved == 'S') {
                                    $("#textoCorrigido-" + res.rndId).text(res.txRevisado);
                                    $("#textoTraduzido-" + res.rndId).text(res.txRevisado);
                                }
                            });
                        }
                    })
                }
            })
        },1000);

    }
    ,toggleTextoOriginal: function(){
        fichaRevisao.$container.find('span.texto-original').toggleClass('hidden');
    }
    /**
     * método para abrir todos os acordeons de uma só vez
     */
    ,abrirTodos:function(){
        $("i.fa-chevron-down").removeClass('fa-chevron-down').addClass('fa-chevron-up blue');
        $("div.collapse").addClass('in')
        $("div.traducaoRevisaoFicha").parent().scrollTop(0);
    }
    /**
     * método para fechar todos os acordeons de uma só vez
     */
    ,fecharTodos:function(){
        $("i.fa-chevron-up").removeClass('fa-chevron-up blue').addClass('fa-chevron-down');
        $("div.collapse").removeClass('in');
        $("div.traducaoRevisaoFicha").parent().scrollTop(0);

    }
    /**
     * método para fazer a tradução do texto chamando a API
     * @param params
     */
    ,traduzirTexto:function( params ){
        var txOriginalHtml  = $("#textoOriginal"+params.rndId ).html();
        var txOriginalText  = $("#textoOriginal"+params.rndId ).text();
        var $txTraduzido = $("#textoTraduzido"+params.rndId );
        var txTraduzidoText = $txTraduzido.text().trim();
        if( ! txOriginalText.trim() ){
            app.alertInfo('Texto em branco')
            return;
        }
        var aviso = ( txTraduzidoText == '' ? '' : '<br><span class="red bold">Atenção, a tradução atual será substituida.</span>')
        app.confirm('Confirma a tradução?'+aviso,function(){
          tradutor.traduzir( txOriginalHtml, function( txTraduzido ) {
              //$txTraduzido.html( txTraduzido );
              var data = {
                  noTabela    : params.noTabela
                  ,noColuna   : params.noColuna
                  ,sqRegistro : params.sqRegistro
                  ,txTraduzido: txTraduzido
                  ,rndId      : params.rndId
              }
              fichaRevisao.salvarTraducao(data);
          } )
        });
    }

    /**
     * método para marcar/desmarcar o campo como finalizado ou não a revisão
     * @param params
     * @param ele
     * @param evt
     */
    ,marcarRevisado:function( params, ele, evt ) {
        var $btnTraduzir       = $("#btnTraduzir"+params.rndId)
        var $btnRevisar        = $("#btnRevisar"+params.rndId)
        var $spanNoPessoaRevisou = $("#spanNoPessoaRevisou"+params.rndId)
        var data = app.params2data(params,{})
        if( ele.checked ) {
            data.inRevisado='S'
        } else {
            data.inRevisado='N'
            if( ! data.confirm ) {
                app.confirm("Deseja cancelar a revisão?", function (res) {
                    data.confirm = true;
                    ele.checked=false;
                    fichaRevisao.marcarRevisado(data,ele,evt);
                }, null, data, 'Cancelamento da revisão')
                evt.preventDefault();
                return
            }
        }
        app.ajax(baseUrl + 'traducaoFicha/saveRevisado',data,
            function(res) {
                if( res.status == 1 ){
                    ele.checked=!ele.checked
                }
                if( ele.checked ) {
                    $btnRevisar.addClass('hidden');
                    $btnTraduzir.addClass('hidden');
                    $spanNoPessoaRevisou.removeClass('hidden');
                } else {
                    $btnRevisar.removeClass('hidden');
                    $btnTraduzir.removeClass('hidden');
                    $spanNoPessoaRevisou.addClass('hidden');
                }
                // atualizar o nome e a hora do revisor
                if( res.data.noRevisor ) {
                    $spanNoPessoaRevisou.html( res.data.noRevisor);
                }
            },'Gravando...');

    }
    , modalHistorico:function( params ){
        app.alertInfo('Em desenvolvimento');
    }
    /**
     * metodo para ativar o editor de texto no campo traduzido e permitir a edição
     * @param params
     */
    ,revisarTraducao:function( params ) {

        // se tiver algum texto sendo revisado, cancelar a revisão
        if( fichaRevisao.revisaoAtual.editor ) {
            var isDirty = fichaRevisao.revisaoAtual.editor.isDirty()
            var rndIdAtual = fichaRevisao.revisaoAtual.rndId
            if( isDirty ) {
                app.confirm('Deseja salvar as alterações?', function () {
                    fichaRevisao.revisaoAtual.editor.save();
                    fichaRevisao.salvarEdicaoAtual(params);
                    fichaRevisao.cancelarEdicaoAtual(params);
                },function(){
                    fichaRevisao.cancelarEdicaoAtual(params);
                });
                return;
            }
            fichaRevisao.cancelarEdicaoAtual();
            if( params.rndId == rndIdAtual ){
                return;
            }
        }

        // nova revisaão
        fichaRevisao.revisaoAtual = $.extend({},params);
        fichaRevisao.revisaoAtual.$btnTraduzir       = $("#btnTraduzir"+params.rndId);
        fichaRevisao.revisaoAtual.$btnRevisar        = $("#btnRevisar"+params.rndId);
        fichaRevisao.revisaoAtual.$chkMarcarRevisado = $("#chkMarcarRevisado"+params.rndId);
        fichaRevisao.revisaoAtual.$divTextoTraduzido = $("#textoTraduzido"+params.rndId);
        fichaRevisao.revisaoAtual.$divBox = fichaRevisao.revisaoAtual.$divTextoTraduzido.closest('div.ficha-html-text-box');
        fichaRevisao.revisaoAtual.$divBox.scrollTop(0);
        var height = fichaRevisao.revisaoAtual.$divTextoTraduzido.height();
        var width  = fichaRevisao.revisaoAtual.$divTextoTraduzido.width();
        fichaRevisao.revisaoAtual.$btnTraduzir.attr('disabled',true);
        fichaRevisao.revisaoAtual.$chkMarcarRevisado.attr('disabled',true);
        fichaRevisao.revisaoAtual.$chkMarcarRevisado.parent().addClass('hidden');
        fichaRevisao.revisaoAtual.$btnRevisar.text('Encerrar edição');
        fichaRevisao.revisaoAtual.$btnRevisar.toggleClass('btn-danger');
        // ativar o editor richEdit no campo da traducação
        var editorOptions = $.extend({}, default_editor_options,{
            selector: "#textoTraduzido"+params.rndId,
            height: height-15,
            width: width,
            plugins: default_editor_options.plugins+',save',
            toolbar: 'save | newdocument | ' + default_editor_options.toolbar,
            setup: function(ed) {
                ed.on('init', function() {
                    fichaRevisao.revisaoAtual.$divBox.attr('style', 'max-height: 600px !important');
                });
            },
            /** save **/
            save_onsavecallback: function(editor) {
                fichaRevisao.salvarEdicaoAtual()
            },
        });
        //try{ tinymce.get("textoTraduzido"+params.rndId).destroy();} catch( e ){}
        tinymce.init(editorOptions);
        fichaRevisao.revisaoAtual.editor = tinymce.get("textoTraduzido"+params.rndId);
    }

    /**
     * método para salvar o texto sendo atual do editor richEdit
     * @param params
     */
    ,salvarEdicaoAtual:function(params){
        var txTraduzido = fichaRevisao.revisaoAtual.$divTextoTraduzido.html();
        var data = {
            noTabela   : fichaRevisao.revisaoAtual.noTabela
            ,noColuna   : fichaRevisao.revisaoAtual.noColuna
            ,sqRegistro : fichaRevisao.revisaoAtual.sqRegistro
            ,txTraduzido: txTraduzido
            ,rndId      : fichaRevisao.revisaoAtual.rndId
        }
        fichaRevisao.revisaoAtual.editor.startContent = txTraduzido;
        fichaRevisao.revisaoAtual.editor.setContent(txTraduzido);
        fichaRevisao.salvarTraducao( data );
    },
    /**
     * metodo para enviar o texto traduzido para o servidor e persistir no banco de dados
     * @param data
     */
    salvarTraducao:function( data ){
        if ( data.rndId ) {
            $("#textoTraduzido" + data.rndId).html('Traduzindo....');
        }
        app.ajax(baseUrl + 'traducaoFicha/saveTextoTraduzido',data,
            function(res) {
                if ( data.rndId ) {
                    $("#textoTraduzido" + data.rndId).html('');
                    if (res.data.txTraduzido) {
                        $("#textoTraduzido" + data.rndId).html(res.data.txTraduzido);
                    }
                }
                // se a edição não tiver sido cancelada, colocar isDirty() para false
                if( fichaRevisao.revisaoAtual.editor ) {
                    fichaRevisao.revisaoAtual.editor.save();// passar isDirty() para false
                }
            },'Gravando...');
    }

    /**
     * método para cancelar a edição
     * @param params
     */
    ,cancelarEdicaoAtual:function(params){
        fichaRevisao.revisaoAtual.editor.setContent( fichaRevisao.revisaoAtual.editor.startContent );
        fichaRevisao.revisaoAtual.$btnRevisar.text('Revisar');
        fichaRevisao.revisaoAtual.$btnTraduzir.attr('disabled', false);
        fichaRevisao.revisaoAtual.$chkMarcarRevisado.attr('disabled',false);
        fichaRevisao.revisaoAtual.$chkMarcarRevisado.parent().removeClass('hidden');
        fichaRevisao.revisaoAtual.$btnRevisar.toggleClass('btn-danger');
        try{fichaRevisao.revisaoAtual.editor.destroy();}catch(e){}
        fichaRevisao.revisaoAtual.$divBox.attr('style', 'max-height: 300px !important');
        fichaRevisao.revisaoAtual = {};
    }
}
fichaRevisao.init();
