//# sourceURL=views/publicacaoFicha/traducaoFicha.js
;var traducaoFicha = {
    sqCicloAvaliacao:'',
    loadingGrid:false, // evitar o carregamento do gride 2 vezes na inicialização
    init:function() {

        var data = {
            callback:'traducaoFicha.updateGrid',
            hideFilters :'situacao,pendencia,categoria',
            mostrarFiltros:'tradutores',
            label:'Filtrar fichas'
        }
        // carregar o formulário de filtro das fichas
        app.loadModule(baseUrl + 'ficha/getFormFiltroFicha', data, '#divFiltrosTraducaoFicha',function(){
            // adicionar evento PASTE no campo Nome cientifico dos filtros
            $('input[name=nmCientificoFiltro]').bind('paste', null,app.inputPaste);
        });

        setTimeout(function(){
            // restaurar o ultimo ciclo utilizado e montar o gride
            app.restoreField('sqCicloAvaliacao', traducaoFicha.sqCicloAvaliacaoChange );
        },2000)
    },



    /**
     * atualizar tela ao alterar o ciclo selecionado
     * @param params
     */
    sqCicloAvaliacaoChange:function( params ) {
        params = params || {};
        traducaoFicha.sqCicloAvaliacao  = $("#sqCicloAvaliacao").val();
        app.saveField('sqCicloAvaliacao');
        $("#tableGridTraducaoFicha tbody tr").remove();
        if( ! traducaoFicha.sqCicloAvaliacao )
        {
            $("#frmTraducaoFicha").hide();
            return;
        }
        $("#frmTraducaoFicha").show();
        if( traducaoFicha.sqCicloAvaliacao ) {
            traducaoFicha.updateGrid () ;
        }
    },

    /**
     * atualizar o grid com as fichas na situação finalizada
     * @param params
     */
    updateGrid:function(params) {
        //console.log('UPDATE GRIDE EXECUTADO');
        $("#btnExportarPlanilha").prop('disabled',true);
        traducaoFicha.loadingGrid=true;
        app.grid("GridTraducaoFicha",params, function() {
            $("#btnExportarPlanilha").prop('disabled',false);
            $("#tableGridTraducaoFicha").shiftSelectable()
            traducaoFicha.updatePagePercentuais()
            traducaoFicha.loadingGrid=false;
        });
    },

    /**
     * marcar / desmarcar todas as linhas do gride
     * @param params
     * @param elem
     */
    selecionarTodas: function(params, elem) {
        $("#gridTraducaoFichaSelectedRows").val('');
        traducaoFicha.checkUncheckAll(params, elem);
        traducaoFicha.chkSqFichaChange();
    },
    checkUncheckAll: function(params, elem, container) {
        var checked;
        if ($(elem).hasClass('glyphicon-unchecked')) {
            checked = true;
            $(elem).removeClass('glyphicon-unchecked').addClass('glyphicon-check');
        } else {
            checked = false;
            $(elem).removeClass('glyphicon-check').addClass('glyphicon-unchecked');
        }
        if( params.tableId )
        {
            $("#"+params.tableId.replace(/^#/,'') ).find('tbody input:checkbox:visible').each(function() {
                this.checked = checked;
                if( checked ) {
                    $(this).closest('tr').addClass('tr-selected');
                } else {
                    $(this).closest('tr').removeClass('tr-selected');
                }
            });
        }
        else {
            $(elem).closest('thead').next().find('input:checkbox:visible').each(function () {
                this.checked = checked;
            });
        }
    },
    /**
     * evento ao clicar no checkbox para selecionr uma linha
     */
    chkSqFichaChange: function() {
        // controle dos ids/linhas do gride selecionadas para mater a seleção na paginação
        var $inputSelectedIds = $("#gridTraducaoFichaSelectedRows");
        if ($inputSelectedIds.size() == 1) {
            var selectedIds = $inputSelectedIds.val() ? $inputSelectedIds.val().split(',') : [];
            // ler todos os checks do gride
            $("#tableGridTraducaoFicha input:checkbox[name=chkSqFicha]").each(function (index, item) {
                var value = String(item.value);
                var index = selectedIds.indexOf(value);
                if (item.checked) {
                    if (index == -1) {
                        selectedIds.push(value);
                    }
                } else {
                    if (index > -1) {
                        selectedIds.splice(index, 1)
                    }
                }
            });
            // gravar as rows selecionadas
            $inputSelectedIds.val(selectedIds.join(','));
            // mostrar/esconder o botão de traduzir no final do gride
            if (selectedIds.length > 0 ) {
                $("#divButtonsTraducaoFicha").removeClass('hidden');
            } else {
                $("#divButtonsTraducaoFicha").addClass('hidden');
            }
        }
    },
    /**
     * callback apos o gride ser atualizado
     * @param params
     */
    gridTraducaoFichaOnLoad:function(params){
        // marcar checkbox selecionados ao troca de página
        var $checkAll = $("#checkAll-GridTraducaoFicha");
        if( $checkAll.size() == 1 && $checkAll.hasClass('glyphicon-check') ) {
            $("#tableGridTraducaoFicha input:checkbox[name=chkSqFicha]").prop('checked',true);
        }
        // marcar de volta os checkboxes selecionados os carregar a página do gride
        var $inputSelectedIds = $("#gridTraducaoFichaSelectedRows");
        if( $inputSelectedIds.size()==1 ) {
            var selectedIds = $inputSelectedIds.val() ? $inputSelectedIds.val().split(',') : [];
            selectedIds.map(function (id) {
                if (id) {
                    var input = $("input:checkbox[name=chkSqFicha][value=" + id + "]");
                    if (input.size() == 1) {
                        input.prop('checked', true);
                    }
                }
            });
        }
        // atualizar os percentuais da tradução e da revisão
        traducaoFicha.updatePagePercentuais()
    }

    /**
     * fazer a tradução das fichas em lote
     * @param params
     */
    ,traduzirLote: function( params ) {
        var ids =  []
        $("#tableGridTraducaoFicha input:checkbox:checked").map( function(i,el) {
            ids.push( $(el).val() );
        } );
        if( ids.length == 0 )
        {
            app.alertInfo('Nenhuma ficha selecionada!');
            return;
        }
        var data = { ids : ids.join(','), all:'N', sqCicloAvaliacao:traducaoFicha.sqCicloAvaliacao };
        var filtros = app.hasActiveFilter('divFiltrosTraducaoFicha')
        var filtrosColunas = {}
        $("#trGridTraducaoFichaFilters input,select").each( function( i,ele ) {
            var $ele = $(ele);
            if( $ele.val() && $ele.data('field') ){
                filtrosColunas[$ele.data('field')]=String( $ele.val() ).trim();
            }
         });
        data = $.extend( data, filtros, filtrosColunas )
        // verificar se está marcado o check de todas
        var $checkboxAll = $("#frmTraducaoFicha i#checkAll-GridTraducaoFicha");
        var qtdFichas = ids.length
        if( $checkboxAll.hasClass('glyphicon-check') ) {
            data.ids = '';
            data.all = 'S';
            qtdFichas = $("#recordsGridTraducaoFicha").data('totalRecords');
        }
        app.confirm('Confirma tradução de <b>' + qtdFichas +' ficha(s)</b>?', function() {
            app.ajax(app.url + 'traducaoFicha/traduzirFichasEmLote', data, function(res) {
                if (res.status === 0) {
                    traducaoFicha.runAsyncTranslate()
                }
            },'Adicionando fichas na fila de tradução. Aguarde...');
        }, null, data, 'Confirmação', 'warning');

    },
    /**
     * excluir a tradução das fichas em lote
     * @param params
     */
     excluirTraducaoLote: function( params ) {
        var ids =  []
        $("#tableGridTraducaoFicha input:checkbox:checked").map( function(i,el) {
            ids.push( $(el).val() );
        } )
        if( ids.length == 0 )
        {
            app.alertInfo('Nenhuma ficha selecionada!');
            return;
        }
        var data = { ids : ids.join(','), all:'N', sqCicloAvaliacao:traducaoFicha.sqCicloAvaliacao };
        var filtros = app.hasActiveFilter('divFiltrosTraducaoFicha')
        var filtrosColunas = {}
        $("#trGridTraducaoFichaFilters input,select").each( function( i,ele ) {
            var $ele = $(ele);
            if( $ele.val() && $ele.data('field') ){
                filtrosColunas[$ele.data('field')]=String( $ele.val() ).trim();
            }
         });
        data = $.extend( data, filtros, filtrosColunas )
        // verificar se está marcado o check de todas
        var $checkboxAll = $("#frmTraducaoFicha i#checkAll-GridTraducaoFicha");
        if( $checkboxAll.hasClass('glyphicon-check') ) {
            data.ids = '';
            data.all = 'S';
        }
        app.confirm('Confirma a EXCLUSÃO da tradução de <b>' + ids.length+' ficha(s)</b>?', function() {
                app.confirm('TEM CERTEZA?', function() {
                    app.ajax(app.url + 'traducaoFicha/excluirTraducaoFichasEmLote', data, function (res) {
                        if (res.status === 0) {
                            traducaoFicha.updateGrid();
                        }
                    }, 'Adicionando fichas na fila de tradução. Aguarde...');
                }, null, data, 'Confirmação', 'warning');
        }, null, data, 'Confirmação', 'warning');

    },

    /**
     * iniciar o processo de tradução assyncrono das fichas adicionadas na fila
    */
    runAsyncTranslate:function(){
        app.ajax(app.url + 'traducaoFicha/runAsyncTranslate', {}, function(res) {
            if (res.status === 0) {
                traducaoFicha.updatePagePercentuais();
            }
        });
    }

    /**
     * abrir a ficha completa em html permitindo fazer a tradução ou a revisão da tradução da ficha
     * @param params
     */
    ,revisar: function( params ){
        var data = app.params2data( params,{} );
        modal.janelaTraducaoRevisaoFicha(data,function(){
            // on open
        },function( data ){
            // on close
            // atualizar andamento ficha no gride
            traducaoFicha.updateGrid({sq_ficha:data.sqFicha})
        });
    }
    ,print:function( params ){
        var data = {'language':'en','sqFicha':params.sqFicha,'noCientifico':params.noCientifico};
        print(data);
    },
    exportarPdf:function(params){
        var data = app.params2data(params, {});
        if( ! data.dlgConfirm ) {
            var ids = []
            $("#tableGridTraducaoFicha input:checkbox:checked").map( function(i,el) {
                ids.push( $(el).val() );
            } )
            if (ids.length == 0) {
                app.alertInfo('Nenhuma linha selecionada');
                return;
            }
            data.ids = ids.join(',')
            data.enviarEmail = true;
            return app.confirmWithEmail("Confirma a exportação de " + ids.length +"  ficha(s) PDF?",traducaoFicha.exportarPdf, data)
        }
        app.ajax(app.url + 'traducaoFicha/gerarPdfFichas', data, function (res) {
            setTimeout(getWorkers,5000);
        });
    },
    exportarPlanilha:function(params){
        var data = app.params2data(params,{format:'xls'})
        if( ! data.dlgConfirm ) {
            data.enviarEmail = true;
            return app.confirmWithEmail("Confirma a exportação do gride para planilha?",traducaoFicha.exportarPlanilha, data)
        }
        traducaoFicha.updateGrid(data);
    },
    updatePagePercentuais:function() {
        // criar a lista de ids da ficha de acordo com a página atual do gride
        var data = {ids: ''}
        var ids = []
        $("#tableGridTraducaoFicha tbody>tr").each(function (i, tr) {
            var sqFicha = $(tr).data('sqFicha')
            ids.push(sqFicha);
        })
        if (ids.length == 0) {
            return;
        }
        data.ids = ids.join(',')

        if (data.ids == '') {
            return;
        }
        app.ajax(app.url + 'traducaoFicha/getPercentuaisPage', data, function (res) {
            if (res.status == 0) {
                res.data.rows.map(function (item) {
                    var sqFicha = item.sqFicha
                    if( typeof( item.nuAndamentoTraducao) != 'undefined') {
                        var $divAndamentoTraducao = $("#andamentoTraducao" + sqFicha)
                        if ($divAndamentoTraducao.size() == 1) {
                            var wrapper = $("#andamentoTraducaoWrapper" + sqFicha)
                            var label = wrapper.find('div.label-progressbar')
                            if( ! res.data.finished ) {
                                wrapper.show();
                            }
                            if( wrapper.is(':visible') ) {
                                $divAndamentoTraducao.css('width', item.nuAndamentoTraducao + '%');
                                $divAndamentoTraducao.find('span').html(item.nuAndamentoTraducao + '%');
                                $divAndamentoTraducao.removeClass('progress-bar-danger progress-bar-warning progress-bar-success-dark progress-bar-success')
                                var cor = 'danger';
                                if (item.nuAndamentoTraducao > 19 && item.nuAndamentoTraducao < 90) {
                                    cor = 'warning'
                                } else if (item.nuAndamentoTraducao > 89 && item.nuAndamentoTraducao < 100) {
                                    cor = 'success-dark'
                                } else if (item.nuAndamentoTraducao > 99) {
                                    cor = 'success'
                                    item.nuAndamentoTraducao = 100
                                    if( label.size()== 1){
                                        label.html('Concluído:')
                                    }
                                }
                                $divAndamentoTraducao.addClass('progress-bar-' + cor)
                            }
                        }
                    }
                    if( typeof( item.nuPercentualTraduzido) != 'undefined') {
                        var $divTraduzido = $("#percentTraduzido" + sqFicha)
                        if ($divTraduzido.size() == 1) {
                            $divTraduzido.css('width', item.nuPercentualTraduzido + '%');
                            $divTraduzido.find('span').html(item.nuPercentualTraduzido + '%');
                            $divTraduzido.removeClass('progress-bar-danger progress-bar-warning progress-bar-success-dark progress-bar-success')
                            var cor = 'danger';
                            if (item.nuPercentualTraduzido > 19 && item.nuPercentualTraduzido < 90) {
                                cor = 'warning'
                            } else if (item.nuPercentualTraduzido > 89 && item.nuPercentualTraduzido < 100) {
                                cor = 'success-dark'
                            } else if (item.nuPercentualTraduzido > 99) {
                                cor = 'success'
                                item.nuPercentualTraduzido = 100
                            }
                            $divTraduzido.addClass('progress-bar-' + cor)
                        }
                    }
                    if( typeof( item.nuPercentualRevisado) != 'undefined'){
                        var $divRevisado = $("#percentRevisado" + sqFicha)
                        if ($divRevisado.size() == 1) {
                            $divRevisado.css('width', item.nuPercentualRevisado + '%');
                            $divRevisado.find('span').html(item.nuPercentualRevisado + '%');
                            $divRevisado.removeClass('progress-bar-danger progress-bar-warning progress-bar-success-dark progress-bar-success')
                            var cor = 'danger';
                            if (item.nuPercentualRevisado > 19 && item.nuPercentualRevisado < 90) {
                                cor = 'warning'
                            } else if (item.nuPercentualRevisado > 89 && item.nuPercentualRevisado < 100) {
                                cor = 'success-dark'
                            } else if (item.nuPercentualRevisado > 99) {
                                cor = 'success'
                                item.nuPercentualRevisado = 100
                            }
                            $divRevisado.addClass('progress-bar-' + cor)
                        }
                    }
                });
                if( ! res.data.finished ) {
                    setTimeout(traducaoFicha.updatePagePercentuais, 5000);
                }
            }

        });
    }
};

// inicializar
window.setTimeout(function(){traducaoFicha.init();},1000)
