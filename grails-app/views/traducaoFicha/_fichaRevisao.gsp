<!-- view: /views/traducaoFicha/_fichaRevisao.gsp -->
<!-- controller:${params?.controller} -->
<!-- action: ${params?.action} -->
<div class="lazy-load">
    traducaoFicha/fichaRevisao.js
</div>

%{--INICIO--}%
<div class="container-fluid traducaoRevisaoFicha" id="containerTraducaoRevisaoFicha-${params.sqFicha}">
    <div class="row text-left row-toolbar">
        <button type="button" class="btn btn-primary btn-sm" data-action="fichaRevisao.toggleTextoOriginal"><i class="fa fa-eye"></i> Mostrar/Esconder texto original</button>
        <button type="button" class="btn btn-primary btn-sm" data-action="fichaRevisao.abrirTodos"><i class="fa fa-expand"></i> Abrir tópicos</button>
        <button type="button" class="btn btn-primary btn-sm" data-action="fichaRevisao.fecharTodos"><i class="fa fa-compress"></i> Fechar tópicos</button>
    </div>
    <input type="hidden" id="sqFichaTraduzirRevisar" value="${params.sqFicha}">

    <g:if test="${fichaRevisao?.error}">
        <div class="row text-center mt10">
            <div class="alert alert-danger">${fichaRevisao?.error}</div>
        </div>
    </g:if>

    <div class="row text-center mb20">
        <p class="ficha-html-header">
            <g:textoFixo txOriginal="Instituto Chico Mendes de Conservação da Biodiversidade - ICMBio"
                         txOriginalClassName="display-block"
                         txRevisado="">
            </g:textoFixo>
        </p>

        <p class="ficha-html-subheader">
            <g:textoFixo txOriginal="Processo de Avaliação do Risco de Extinção da Fauna Brasileira"
                         txOriginalClassName="display-block"
                         txRevisado="">
            </g:textoFixo>
        </p>
        <p class="ficha-html-nao-publicada">
            <g:textoFixo txOriginal="NÃO PUBLICADA"
                         txOriginalClassName="display-block"
                         txRevisado="">
            </g:textoFixo>
        </p>
    </div>

    %{--    NOME CIENTIFICO--}%
    <div class="row text-center ficha-html-group">
        <span>${raw(fichaRevisao?.nm_cientifico)}</span>
    </div>

    %{--    AUTORES--}%
    <div class="row ficha-html-section ficha-html-text-justify">
        <div class="col-sm-12">
            <g:textoFixo txOriginal="Autoria"></g:textoFixo>
        </div>
        <g:textoLivre noTabela="ficha" noColuna="dsCitacao" sqRegistro="${fichaRevisao?.sq_ficha}"></g:textoLivre>
    </div>

    %{--    CATEGORIA --}%
    <div class="row text-center ficha-html-group">
        <i class="i-accordion fa fa-chevron-down" title="Abrir/Fechar tópico" data-toggle="collapse" data-target="#topicoCategoria" onclick="toggleImage(this)"></i>

        <p>
            <g:textoFixo txOriginal="Categoria"></g:textoFixo>: <g:textoFixo tipo="apoio" idRegistro="${fichaRevisao?.sq_categoria_validada}" txOriginal="${fichaRevisao?.ds_categoria_validada}"></g:textoFixo>&nbsp;(${fichaRevisao?.cd_categoria_validada})${raw( fichaRevisao.mostrarDisclaimer ? '<span class="red cursor-pointer" title="Categoria diferente da categoria vigente">*</span>':'')}
        </p>
        <g:if test="${fichaRevisao?.dt_validacao}">
            <p>
                <g:textoFixo txOriginal="Data da avaliação"></g:textoFixo>: ${fichaRevisao?.dt_validacao}
            </p>
        </g:if>
    </div>

    %{--    JUSTIFICATIVA --}%
    <div class="row text-left ficha-html-label collapse" id="topicoCategoria">
        <div class="col-sm-12">
            <g:textoFixo txOriginal="Justificativa"></g:textoFixo>
        </div>
        <g:textoLivre noTabela="ficha" noColuna="dsJustificativaFinal" sqRegistro="${fichaRevisao?.sq_ficha}"></g:textoLivre>

        <g:if test="${fichaRevisao.mostrarDisclaimer}">
            <p class="ficha-html-disclaimer">
                <span class="red">(*)</span> <g:textoFixo className="ficha-html-disclaimer" txOriginal="Resultado da avaliação mais recente da espécie. Entretanto, essa categoria ainda não foi oficializada por meio de Portaria do MMA. Para fins legais, deve-se utilizar a categoria constante nas Portarias MMA 444/2014 ou 445/2014."></g:textoFixo>
            </p>
        </g:if>
    </div>

    %{-- CLASSIFICACAO TAXONOMICA --}%
    <div class="row text-center ficha-html-group">
        <i class="i-accordion fa fa-chevron-down" title="Abrir/Fechar tópico" data-toggle="collapse" data-target="#topicoClassificacaoTaxonomica" onclick="toggleImage(this)"></i>

        <p>
            <g:textoFixo txOriginal="Classificação Taxonômica"></g:textoFixo>
        </p>
    </div>

    %{--ARVORE TAXONOMICA--}%
    <div class="text-left ficha-html-sublabel collapse" id="topicoClassificacaoTaxonomica">
        <div class="row">
            <div class="col-sm-12">
                <p><g:textoFixo txOriginal="Filo" className="label-arvore-taxonomica"></g:textoFixo>: ${fichaRevisao?.no_filo ?: '?'}</p>

                <p><g:textoFixo txOriginal="Classe" className="label-arvore-taxonomica"></g:textoFixo>: ${fichaRevisao?.no_classe ?: '?'}</p>

                <p><g:textoFixo txOriginal="Ordem" className="label-arvore-taxonomica"></g:textoFixo>: ${fichaRevisao?.no_ordem ?: '?'}</p>

                <p><g:textoFixo txOriginal="Família" className="label-arvore-taxonomica"></g:textoFixo>: ${fichaRevisao?.no_familia ?: '?'}</p>

                <p><g:textoFixo txOriginal="Gênero" className="label-arvore-taxonomica"></g:textoFixo>: <i>${fichaRevisao?.no_genero ?: '?'}</i></p>

                <p><g:textoFixo txOriginal="Espécie" className="label-arvore-taxonomica"></g:textoFixo>: <i>${fichaRevisao?.no_especie ?: '?'}</i></p>
                <g:if test="${fichaRevisao?.no_subespecie}">
                    <p><g:textoFixo txOriginal="Subespecie" className="label-arvore-taxonomica"></g:textoFixo>: <i>${fichaRevisao?.no_subespecie ?: '?'}</i></p>
                </g:if>
            </div>
        </div>

        <div class="row">
            %{-- NOMES COMUNS --}%
            <div class="col-sm-12 mt10">
                <p>
                    <g:textoFixo txOriginal="Nomes comuns"></g:textoFixo>
                </p>

                <p>
                    ${fichaRevisao?.no_comum}
                </p>
            </div>
        </div>

        %{-- NOMES COMUNS --}%
        <div class="row">
            <div class="col-sm-12 mt10">
                <p>
                    <g:textoFixo txOriginal="Nomes antigos"></g:textoFixo>
                </p>

                <p>
                    ${fichaRevisao?.no_antigo}
                </p>
            </div>
        </div>


        %{--    NOTAS TAXONOMICAS --}%
        <div class="row">
            <div class="col-sm-12 mt15">
                <g:textoFixo txOriginal="Notas taxonômicas"></g:textoFixo>
            </div>
            <g:textoLivre noTabela="ficha" noColuna="dsNotasTaxonomicas" sqRegistro="${fichaRevisao?.sq_ficha}"></g:textoLivre>
        </div>

        %{--    NOTAS MORFOLOGICAS --}%
        <div class="row">
            <div class="col-sm-12 mt15">
                <g:textoFixo txOriginal="Notas morfológicas"></g:textoFixo>
            </div>
            <g:textoLivre noTabela="ficha" noColuna="dsDiagnosticoMorfologico" sqRegistro="${fichaRevisao?.sq_ficha}"></g:textoLivre>
        </div>
    </div>

    %{--DISTRIBUICAO--}%
    <div class="row text-center ficha-html-group">
        <i class="i-accordion fa fa-chevron-down" title="Abrir/Fechar tópico" data-toggle="collapse" data-target="#topicoDistribuicao" onclick="toggleImage(this)"></i>

        <p>
            <g:textoFixo txOriginal="Distribuição"></g:textoFixo>
        </p>
    </div>
    <div class="text-left ficha-html-sublabel collapse" id="topicoDistribuicao">
         %{--ENDEMICA--}%
        <div class="row">
            <div class="col-sm-12">
                <g:textoFixo txOriginal="Endêmica do Brasil"></g:textoFixo>? <g:textoFixo txOriginal="${fichaRevisao?.ds_endemica_brasil}"></g:textoFixo>
            </div>
        </div>

        %{--    DISTRIBUICAO GLOBAL --}%
        <div class="row">
            <div class="col-sm-12 mt10">
                <g:textoFixo txOriginal="Distribuição global"></g:textoFixo>
            </div>
            <g:textoLivre noTabela="ficha" noColuna="dsDistribuicaoGeoGlobal" sqRegistro="${fichaRevisao?.sq_ficha}"></g:textoLivre>
        </div>

        %{--    DISTRIBUICAO GLOBAL --}%
        <div class="row">
            <div class="col-sm-12 mt20">
                <g:textoFixo txOriginal="Distribuição nacional"></g:textoFixo>
            </div>
            <g:textoLivre noTabela="ficha" noColuna="dsDistribuicaoGeoNacional" sqRegistro="${fichaRevisao?.sq_ficha}"></g:textoLivre>
        </div>

        %{-- ESTADOS --}%
        <div class="row">
            <div class="col-sm-12 mt20">
                <g:textoFixo txOriginal="Estados"></g:textoFixo>
            </div>

            <div class="col-sm-12">
                <p>${fichaRevisao?.no_estado}</p>
            </div>
        </div>

        %{-- BIOMAS --}%
        <div class="row">
            <div class="col-sm-12 mt20">
                <g:textoFixo txOriginal="Biomas"></g:textoFixo>
            </div>

            <div class="col-sm-12">
                <p>${fichaRevisao?.no_bioma}</p>
            </div>
        </div>

        %{-- BACIAS --}%
        <div class="row">
            <div class="col-sm-12 mt20">
                <g:textoFixo txOriginal="Bacias hidrográficas"></g:textoFixo>
            </div>

            <div class="col-sm-12">
                <p>${fichaRevisao?.no_bacia}</p>
            </div>
        </div>


        %{-- AREAS RELEVANTES --}%
        <div class="row">
            <div class="col-sm-12 mt20">
                <g:textoFixo txOriginal="Áreas relevantes"></g:textoFixo>
            </div>

            <div class="col-sm-12">
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-condensed table-bordered">
                        <thead>
                        <tr>
                            <th><g:textoFixo txOriginal="Tipo"></g:textoFixo></th>
                            <th><g:textoFixo txOriginal="Local"></g:textoFixo></th>
                            <th><g:textoFixo txOriginal="Estado"></g:textoFixo></th>
                            <th><g:textoFixo txOriginal="Município"></g:textoFixo></th>
                            <th><g:textoFixo txOriginal="Referência bibliográfica"></g:textoFixo></th>
                        </tr>
                        </thead>
                        <tbody>
                        <g:each var="item" in="${fichaRevisao.areasRelevantes}">
                            <tr>
                                <td>
                                    <g:textoFixo tipo="apoio" idRegistro="${item.sq_tipo}" txOriginal="${item.ds_tipo ?: ''}"></g:textoFixo>
                                </td>
                                <g:if test="${!item?.no_tabela}">
                                    <td><g:textoFixo txOriginal="${item.no_local ?: ''}"></g:textoFixo></td>
                                </g:if>
                                <g:else>
                                    <td>${item.no_local}</td>
                                </g:else>
                                <td>${item.no_estado ?: ''}</td>
                                <td>${item.no_municipio ?: ''}</td>
                                <td>${raw(br.gov.icmbio.Util.parseRefBib2Grid(item.json_ref_bib))}</td>
                            </tr>
                        </g:each>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    %{-- HISTORIA NATURAL --}%
    <div class="row text-center ficha-html-group">
        <i class="i-accordion fa fa-chevron-down" title="Abrir/Fechar tópico" data-toggle="collapse" data-target="#topicoHistoriaNatural" onclick="toggleImage(this)"></i>

        <p>
            <g:textoFixo txOriginal="História natural"></g:textoFixo>
        </p>
    </div>

    <div class="text-left ficha-html-sublabel collapse" id="topicoHistoriaNatural">

        %{-- ESPÉCIE MIGRATÓRIA? --}%
        <div class="row">
            <div class="col-sm-12 mt20">
                <g:textoFixo txOriginal="Espécie migratória"></g:textoFixo>? <g:textoFixo txOriginal="${fichaRevisao?.ds_migratoria}"></g:textoFixo>
            </div>
        </div>

        %{-- OBSERVACAO HISTORIA NATURAL --}%
        <div class="row">
            <g:textoLivre noTabela="ficha" noColuna="dsHistoriaNatural" sqRegistro="${fichaRevisao?.sq_ficha}"></g:textoLivre>
        </div>

        %{-- TABELA HABITO ALIMENTAR--}%
        <div class="row">
            <div class="col-sm-12 mt20 mb5">
                <g:textoFixo txOriginal="Hábito alimentar"></g:textoFixo>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-condensed table-bordered">
                        <thead>
                        <tr>
                            <th>
                                <g:textoFixo txOriginal="Tipo"></g:textoFixo>
                            </th>
                            <th><g:textoFixo txOriginal="Referência"></g:textoFixo></th>
                        </tr>
                        </thead>
                        <tbody>
                        <g:each var="item" in="${fichaRevisao.habitosAlimentares}">
                            <tr>
                                <td>
                                    <g:textoFixo tipo="apoio" idRegistro="${item.sq_tipo_habito_alimentar}" txOriginal="${item.ds_tipo ?: ''}"></g:textoFixo>
                                </td>
                                <td>${raw(br.gov.icmbio.Util.parseRefBib2Grid(item.json_ref_bib))}</td>
                            </tr>
                        </g:each>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        %{-- OBSERVAÇÕES SOBRE O HÁBITO ALIMENTAR --}%
        <div class="row">
            <div class="col-sm-12 mt20">
                <g:textoFixo txOriginal="Observações sobre o hábito alimentar"></g:textoFixo>
            </div>
            <g:textoLivre noTabela="ficha" noColuna="dsHabitoAlimentar" sqRegistro="${fichaRevisao?.sq_ficha}"></g:textoLivre>
        </div>

        %{-- RESTRITO A HABITAT PRIMÁRIO --}%
        <div class="row">
            <div class="col-sm-12 mt20">
                <g:textoFixo txOriginal="Restrito a habitat primário"></g:textoFixo>? <g:textoFixo txOriginal="${fichaRevisao?.ds_restrito_habita_primario}"></g:textoFixo>
            </div>
        </div>

        %{-- ESPECIALISTA EM MICRO HABITAT --}%
        <div class="row">
            <div class="col-sm-12 mt20">
                <g:textoFixo txOriginal="Especialista em micro habitat"></g:textoFixo>? <g:textoFixo txOriginal="${fichaRevisao?.ds_especialista_micro_habitat}"></g:textoFixo>
            </div>
        </div>

        %{-- OBSERVAÇÕES SOBRE O HABITAT--}%
        <div class="row">
            <div class="col-sm-12 mt20">
                <g:textoFixo txOriginal="Observações sobre o habitat"></g:textoFixo>
            </div>
            <g:textoLivre noTabela="ficha" noColuna="dsUsoHabitat" sqRegistro="${fichaRevisao?.sq_ficha}"></g:textoLivre>
        </div>

        %{-- TABELA INTERAÇÕES COM OUTRAS ESPÉCIES--}%
        <div class="row">
            <div class="col-sm-12 mt20 mb5">
                <g:textoFixo txOriginal="Interações com outras espécies"></g:textoFixo>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-condensed table-bordered">
                        <thead>
                        <tr>
                            <th><g:textoFixo txOriginal="Tipo"></g:textoFixo></th>
                            <th>Taxon</th>
                            <th><g:textoFixo txOriginal="Categoria"></g:textoFixo></th>
                            <th><g:textoFixo txOriginal="Referência bibliográfica"></g:textoFixo></th>
                        </tr>
                        </thead>
                        <tbody>
                        <g:each var="item" in="${fichaRevisao.interacoes}">
                            <tr>
                                <td>
                                    <g:textoFixo tipo="apoio" idRegistro="${item?.sq_tipo_interacao}" txOriginal="${item?.ds_tipo ?: ''}"></g:textoFixo>
                                </td>
                                <td>
                                    ${item?.no_taxon}
                                </td>
                                <td>
                                    <g:textoFixo tipo="apoio" idRegistro="${item?.sq_categoria}" txOriginal="${item?.ds_categoria ?: ''}"></g:textoFixo>${item.cd_categoria ? ' (' + item.cd_categoria + ')' : ''}
                                </td>
                                <td>${raw(br.gov.icmbio.Util.parseRefBib2Grid(item?.json_ref_bib))}</td>
                            </tr>
                        </g:each>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        %{-- OBSERVAÇÕES GERAIS SOBRE AS INTERAÇÕES --}%
        <div class="row mt20">
            <g:textoLivre noTabela="ficha" noColuna="dsInteracao" sqRegistro="${fichaRevisao?.sq_ficha}"></g:textoLivre>
        </div>


        %{-- REPRODUCAO --}%
        <div class="row">
            <div class="col-sm-12 mt20">
                <g:textoFixo txOriginal="Reprodução"></g:textoFixo>
            </div>
        </div>

        %{-- TEMPO DE GESTAÇÃO --}%
        <div class="row">
            <div class="col-sm-12 mt20">
                <g:textoFixo txOriginal="Tempo de gestação"></g:textoFixo>: <g:textoFixo tipo="regex" txOriginal="${fichaRevisao?.vl_tempo_gestacao}">
                </g:textoFixo>&nbsp;<g:textoFixo tipo="apoio" idRegistro="${fichaRevisao?.sq_unid_tempo_gestacao}" txOriginal="${fichaRevisao?.ds_tempo_gestacao}"></g:textoFixo>
            </div>
        </div>

        %{-- TAMANHO PROLE --}%
        <div class="row">
            <div class="col-sm-12 mt20">
                <g:textoFixo txOriginal="Tamanho da prole"></g:textoFixo>: <g:textoFixo tipo="regex" txOriginal="${fichaRevisao?.vl_tamanho_prole}">
                </g:textoFixo>&nbsp;<g:textoFixo txOriginal="Individuo(s)"></g:textoFixo>
            </div>
        </div>

        %{--    TABELA REPRODUCAO--}%
        <div class="row">
            <div class="col-sm-7 mt20">
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-condensed table-bordered">
                        <thead>
                        <tr>
                            <th><g:textoFixo txOriginal="Campo"></g:textoFixo></th>
                            <th><g:textoFixo txOriginal="Macho"></g:textoFixo></th>
                            <th><g:textoFixo txOriginal="Fêmea"></g:textoFixo></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><g:textoFixo txOriginal="Maturidade sexual"></g:textoFixo></td>
                            <td class="text-center">${fichaRevisao.vl_maturidade_sexual_macho} <g:textoFixo tipo="apoio" idRegistro="${fichaRevisao?.sq_unid_maturidade_sexual_macho}" txOriginal="${fichaRevisao?.ds_maturidade_sexual_macho}"></g:textoFixo></td>
                            <td class="text-center">${fichaRevisao.vl_maturidade_sexual_femea} <g:textoFixo tipo="apoio" idRegistro="${fichaRevisao?.sq_unid_maturidade_sexual_femea}" txOriginal="${fichaRevisao?.ds_maturidade_sexual_femea}"></g:textoFixo>
                            </td>
                        </tr>
                        <tr>
                            <td><g:textoFixo txOriginal="Peso médio (adulto)"></g:textoFixo></td>
                            <td class="text-center">${fichaRevisao.vl_peso_macho} <g:textoFixo tipo="apoio" idRegistro="${fichaRevisao?.sq_unidade_peso_macho}" txOriginal="${fichaRevisao?.ds_unidade_peso_macho}"></g:textoFixo></td>
                            <td class="text-center">${fichaRevisao.vl_peso_femea} <g:textoFixo tipo="apoio" idRegistro="${fichaRevisao?.sq_unidade_peso_femea}" txOriginal="${fichaRevisao?.ds_unidade_peso_femea}"></g:textoFixo></td>
                        </tr>
                        <tr>
                            <td><g:textoFixo txOriginal="Comprimento máximo"></g:textoFixo></td>
                            <td class="text-center">${fichaRevisao.vl_comprimento_macho_max} <g:textoFixo tipo="apoio" idRegistro="${fichaRevisao?.sq_medida_comprimento_macho_max}" txOriginal="${fichaRevisao?.ds_unid_comp_max_macho}"></g:textoFixo></td>
                            <td class="text-center">${fichaRevisao.vl_comprimento_femea_max} <g:textoFixo tipo="apoio" idRegistro="${fichaRevisao?.sq_medida_comprimento_femea_max}" txOriginal="${fichaRevisao?.ds_unid_comp_max_femea}"></g:textoFixo></td>
                        </tr>

                        <tr>
                            <td><g:textoFixo txOriginal="Comprimento na maturidade sexual"></g:textoFixo></td>
                            <td class="text-center">${fichaRevisao.vl_comprimento_macho} <g:textoFixo tipo="apoio" idRegistro="${fichaRevisao?.sq_medida_comprimento_macho}" txOriginal="${fichaRevisao?.ds_unid_comp_macho}"></g:textoFixo></td>
                            <td class="text-center">${fichaRevisao.vl_comprimento_femea} <g:textoFixo tipo="apoio" idRegistro="${fichaRevisao?.sq_medida_comprimento_femea}" txOriginal="${fichaRevisao?.ds_unid_comp_femea}"></g:textoFixo></td>
                        </tr>
                        <tr>
                            <td><g:textoFixo txOriginal="Senilidade reprodutiva"></g:textoFixo></td>
                            <td class="text-center">${fichaRevisao.vl_senilidade_reprodutiva_macho} <g:textoFixo tipo="apoio" idRegistro="${fichaRevisao?.sq_unidade_senilid_rep_macho}" txOriginal="${fichaRevisao?.ds_unid_senilidade_macho}"></g:textoFixo></td>
                            <td class="text-center">${fichaRevisao.vl_senilidade_reprodutiva_femea} <g:textoFixo tipo="apoio" idRegistro="${fichaRevisao?.sq_unidade_senilid_rep_femea}" txOriginal="${fichaRevisao?.ds_unid_senilidade_femea}"></g:textoFixo></td>
                        </tr>
                        <tr>
                            <td><g:textoFixo txOriginal="Longevidade"></g:textoFixo></td>
                            <td class="text-center">${fichaRevisao.vl_longevidade_macho} <g:textoFixo tipo="apoio" idRegistro="${fichaRevisao?.sq_unidade_longevidade_macho}" txOriginal="${fichaRevisao?.ds_unid_logevidade_macho}"></g:textoFixo></td>
                            <td class="text-center">${fichaRevisao.vl_longevidade_femea} <g:textoFixo tipo="apoio" idRegistro="${fichaRevisao?.sq_unidade_longevidade_femea}" txOriginal="${fichaRevisao?.ds_unid_logevidade_femea}"></g:textoFixo></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="row mt20">
            <g:textoLivre noTabela="ficha" noColuna="dsReproducao" sqRegistro="${fichaRevisao?.sq_ficha}"></g:textoLivre>
        </div>

    </div>

    %{-- POPULAÇÃO --}%
    <div class="row text-center ficha-html-group">
        <i class="i-accordion fa fa-chevron-down" title="Abrir/Fechar tópico" data-toggle="collapse" data-target="#topicoPopulacao" onclick="toggleImage(this)"></i>

        <p>
            <g:textoFixo txOriginal="População"></g:textoFixo>
        </p>
    </div>

    <div class="text-left ficha-html-sublabel collapse" id="topicoPopulacao">

        %{-- TEMPO GERACIONAL --}%
        <div class="row">
            <div class="col-sm-12 mt20">
                <g:textoFixo txOriginal="Tempo geracional"></g:textoFixo>: <span>${fichaRevisao?.vl_tempo_geracional?.toInteger()}</span>
                &nbsp;<g:textoFixo tipo="apoio" idRegistro="${fichaRevisao?.sq_medida_tempo_geracional}" txOriginal="${fichaRevisao?.ds_medida_tempo_geracional}"></g:textoFixo>
            </div>
        </div>

        %{-- TENDENCIA POPULACIONAL--}%
        <div class="row">
            <div class="col-sm-12 mt20">
                <g:textoFixo txOriginal="Tendência populacional"></g:textoFixo>: &nbsp;
                <g:textoFixo tipo="apoio" idRegistro="${fichaRevisao?.sq_tendencia_populacional}" txOriginal="${fichaRevisao?.ds_tendencia_populacional}"></g:textoFixo>
            </div>
        </div>

        %{-- OBSERVAÇÕES SOBRE O HÁBITO ALIMENTAR --}%
        <div class="row">
            <div class="col-sm-12 mt20">
                <g:textoFixo txOriginal="Características genéticas"></g:textoFixo>
            </div>
            <g:textoLivre noTabela="ficha" noColuna="dsCaracteristicaGenetica" sqRegistro="${fichaRevisao?.sq_ficha}"></g:textoLivre>
        </div>

        %{-- OBSERVAÇÕES SOBRE A POPULAÇÃO --}%
        <div class="row">
            <div class="col-sm-12 mt20">
                <g:textoFixo txOriginal="Observações sobre a população"></g:textoFixo>
            </div>
            <g:textoLivre noTabela="ficha" noColuna="dsPopulacao" sqRegistro="${fichaRevisao?.sq_ficha}"></g:textoLivre>
        </div>
    </div>


    %{--AMEAÇAS --}%
    <div class="row text-center ficha-html-group">
        <i class="i-accordion fa fa-chevron-down" title="Abrir/Fechar tópico" data-toggle="collapse" data-target="#topicoAmeaca" onclick="toggleImage(this)"></i>

        <p>
            <g:textoFixo txOriginal="Ameaças"></g:textoFixo>
        </p>
    </div>

    <div class="text-left ficha-html-sublabel collapse" id="topicoAmeaca">
        <div class="row">
            <g:textoLivre noTabela="ficha" noColuna="dsAmeaca" sqRegistro="${fichaRevisao?.sq_ficha}"></g:textoLivre>
        </div>

        %{-- TABELA TIPO DE AMEACA--}%
        <div class="row">
            <div class="col-sm-12 mt20">
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-condensed table-bordered">
                        <thead>
                        <tr>
                            <th><g:textoFixo txOriginal="Tipo de ameaça"></g:textoFixo></th>
                            <th><g:textoFixo txOriginal="Referência bibliográfica"></g:textoFixo></th>
                        </tr>
                        </thead>
                        <tbody>
                        <g:each var="item" in="${fichaRevisao?.ameacas}">
                            <tr>
                                <td>
                                    <span>${item?.codigo} -</span><g:textoFixo tipo="apoio" idRegistro="${item?.id}" txOriginal="${item?.descricao ?: ''}"></g:textoFixo>
                                </td>
                                <td>${raw(br.gov.icmbio.Util.parseRefBib2Grid(item?.json_ref_bib))}</td>
                            </tr>
                        </g:each>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    %{--        FIM AMEAÇA--}%

    %{--USOS --}%
    <div class="row text-center ficha-html-group">
        <i class="i-accordion fa fa-chevron-down" title="Abrir/Fechar tópico" data-toggle="collapse" data-target="#topicoUso" onclick="toggleImage(this)"></i>

        <p>
            <g:textoFixo txOriginal="Usos"></g:textoFixo>
        </p>
    </div>

    <div class="text-left ficha-html-sublabel collapse" id="topicoUso">
        <div class="row">
            <g:textoLivre noTabela="ficha" noColuna="dsUso" sqRegistro="${fichaRevisao?.sq_ficha}"></g:textoLivre>
        </div>
        %{-- TABELA TIPO DE USOS --}%
        <div class="row">
            <div class="col-sm-8 mt20">
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-condensed table-bordered">
                        <thead>
                        <tr>
                            <th><g:textoFixo txOriginal="Tipo de uso"></g:textoFixo></th>
                            <th><g:textoFixo txOriginal="Referência bibliográfica"></g:textoFixo></th>
                        </tr>
                        </thead>
                        <tbody>
                        <g:each var="item" in="${fichaRevisao?.usos}">
                            <tr>
                                <td>
                                    <span>${item?.codigo} -</span><g:textoFixo tipo="uso" idRegistro="${item?.id}" txOriginal="${item?.descricao ?: ''}"></g:textoFixo>
                                </td>
                                <td>${raw(br.gov.icmbio.Util.parseRefBib2Grid(item?.json_ref_bib))}</td>
                            </tr>
                        </g:each>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>  %{--     FIM USOS --}%


%{-- CONSERVACAO --}%
    <div class="row text-center ficha-html-group">
        <i class="i-accordion fa fa-chevron-down" title="Abrir/Fechar tópico" data-toggle="collapse" data-target="#topicoConservacao" onclick="toggleImage(this)"></i>

        <p>
            <g:textoFixo txOriginal="Conservação"></g:textoFixo>
        </p>
    </div>

    <div class="text-left ficha-html-sublabel collapse" id="topicoConservacao">
        %{-- TABELA TIPO DE USOS --}%
        <g:set var="existeCategoriaOutra" value="n"></g:set>
        <div class="row">
            <div class="col-sm-12 mt20">
                <div class="table-responsive">

                    <g:textoFixo txOriginal="Histórico de avaliação"></g:textoFixo>
                    <table class="table table-striped table-hover table-condensed table-bordered">
                        <thead>
                        <tr>
                            <th><g:textoFixo txOriginal="Tipo"></g:textoFixo></th>
                            <th><g:textoFixo txOriginal="Ano"></g:textoFixo></th>
                            <th><g:textoFixo txOriginal="Abrangência"></g:textoFixo></th>
                            <th><g:textoFixo txOriginal="Categoria"></g:textoFixo></th>
                            <th><g:textoFixo txOriginal="Critéŕio"></g:textoFixo></th>
                            <th><g:textoFixo txOriginal="Referência bibliográfica"></g:textoFixo></th>
                        </tr>
                        </thead>
                        <tbody>
                        <g:each var="item" in="${fichaRevisao?.historicoAvaliacoes}">
                            <tr>
                                %{-- TIPO --}%
                                <td class="text-center">
                                    <g:textoFixo tipo="apoio" idRegistro="${item?.sq_tipo}" txOriginal="${item?.ds_tipo ?: ''}"></g:textoFixo>
                                </td>

                                %{-- ANO --}%
                                <td class="text-center">${item?.nu_ano}</td>

                                %{-- ABRANGENCIA --}%
                                <td>
                                    <g:if test="${item?.no_regiao_outra}">
                                        <g:textoFixo tipo="fixo" txOriginal="${item?.no_regiao_outra ?: ''}"></g:textoFixo>
                                    </g:if>
                                    <g:else>
                                        ${item?.ds_abrangencia}
                                    </g:else>
                                </td>

                                %{-- CATEGORIA--}%
                                <td class="text-center">
                                    <g:if test="${item.de_categoria_outra}">
                                        <g:textoFixo tipo="fixo" txOriginal="${item.de_categoria_outra}"></g:textoFixo><span class="red">*</span>
                                        <g:set var="existeCategoriaOutra" value="s"></g:set>
                                    </g:if>
                                    <g:else>
                                        <g:textoFixo tipo="apoio" idRegistro="${item?.sq_categoria}" txOriginal="${item?.ds_categoria ?: ''}"></g:textoFixo>${item.cd_categoria ? ' (' + item.cd_categoria + ')' : ''}
                                    </g:else>
                                </td>

                                %{-- CRITERIO --}%
                                <td class="text-center">${item?.ds_criterio}</td>

                                %{-- REFERENCIA --}%
                                <td>${raw(br.gov.icmbio.Util.parseRefBib2Grid(item?.json_ref_bib))}</td>
                            </tr>
                        </g:each>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <g:if test="${existeCategoriaOutra == 's' }">
            <div class="row">
                <div class="col-sm-12 mt20 text-center">
                    (<span class="red">*</span>)<g:textoFixo txOriginal="Categoria não utilizada no método IUCN"></g:textoFixo>.
                </div>
            </div>
        </g:if>

        %{-- OBSERVAÇÕES SOBRE O HISTORICO AVALIACOES --}%
        <div class="row">
            <div class="col-sm-12 mt20">
                <g:textoFixo txOriginal="Observações gerais sobre o histórico de avaliação"></g:textoFixo>
            </div>
            <g:textoLivre noTabela="ficha" noColuna="dsHistoricoAvaliacao" sqRegistro="${fichaRevisao?.sq_ficha}"></g:textoLivre>
        </div>

        %{-- PRESENCA EM LISTA OFICIAL--}%
        <div class="row">
            <div class="col-sm-12 mt20">
                <g:textoFixo txOriginal="Presença em lista nacional oficial de espécies ameaçadas de extinção"></g:textoFixo>? <g:textoFixo txOriginal="${fichaRevisao?.ds_presenca_lista_vigente}"></g:textoFixo>
            </div>
        </div>


        %{-- TABELA PRESENÇA EM CONVENÇÃO --}%
        <div class="row">
            <div class="col-sm-8 mt20">
                <div class="table-responsive">
                    <g:textoFixo txOriginal="Presença em convenção"></g:textoFixo>
                    <table class="table table-striped table-hover table-condensed table-bordered">
                        <thead>
                        <tr>
                            <th><g:textoFixo txOriginal="Convenção"></g:textoFixo></th>
                            <th><g:textoFixo txOriginal="Ano"></g:textoFixo></th>
                        </tr>
                        </thead>
                        <tbody>
                        <g:each var="item" in="${fichaRevisao?.convencoes}">
                            <tr>
                                %{-- TIPO --}%
                                <td>
                                    <g:textoFixo tipo="apoio" idRegistro="${item?.sq_convencao}" txOriginal="${item?.ds_convencao ?: ''}"></g:textoFixo>
                                </td>

                                %{-- ANO --}%
                                <td class="text-center">${item?.nu_ano}</td>
                            </tr>
                        </g:each>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


        %{-- OBSERVAÇÕES SOBRE AÇÕES DE CONSERVACAO --}%
        <div class="row">
            <div class="col-sm-12 mt20">
                <g:textoFixo txOriginal="Ações de conservação"></g:textoFixo>
            </div>
            <g:textoLivre noTabela="ficha" noColuna="dsAcaoConservacao" sqRegistro="${fichaRevisao?.sq_ficha}"></g:textoLivre>
        </div>

        %{-- TABELA ACOES CONSERVACAO --}%
        <div class="row">
            <div class="col-sm-12 mt20">
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-condensed table-bordered">
                        <thead>
                        <tr>
                            <th><g:textoFixo txOriginal="Ação"></g:textoFixo></th>
                            <th><g:textoFixo txOriginal="Situação"></g:textoFixo></th>
                            <th><g:textoFixo txOriginal="Referência bibliográfica"></g:textoFixo></th>
                        </tr>
                        </thead>
                        <tbody>
                        <g:each var="item" in="${fichaRevisao?.acoes_conservacao}">
                            <tr>
                        %{-- ACAO --}%
                            <td class="text-left">

                                <span>${item?.codigo} -</span><g:textoFixo tipo="apoio" idRegistro="${item?.id}" txOriginal="${item?.descricao ?: ''}"></g:textoFixo>
                            </td>


                        %{-- SITUACAO --}%
                            <td class="text-center">
                                <g:textoFixo tipo="apoio" idRegistro="${item?.sq_situacao}" txOriginal="${item?.ds_situacao ?: ''}"></g:textoFixo>
                            </td>

                        %{-- REFERENCIA --}%
                            <td>${raw(br.gov.icmbio.Util.parseRefBib2Grid(item?.json_ref_bib))}</td>


                        %{-- imprimir o pan--}%
                            <g:if test="${item.sq_plano_acao}">
                                <tr>
                                    <td colspan="3" style="background-color: #efefef;padding-left: 30px !important;">
                                        <g:textoFixo tipo="pan" idRegistro="${item?.sq_plano_acao}" txOriginal="${ item?.sg_plano_acao ?: ''}"></g:textoFixo>
                                    </td>
                                </tr>
                            </g:if>

                            </tr>
                        </g:each>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        %{-- PRESENCA EM UC --}%
        <div class="row">
            <div class="col-sm-12 mt20">
                <g:textoFixo txOriginal="Presença em UC"></g:textoFixo>
            </div>
            <g:textoLivre noTabela="ficha" noColuna="dsPresencaUc" sqRegistro="${fichaRevisao?.sq_ficha}"></g:textoLivre>
        </div>
    </div>  %{--     FIM CONSERVACAO --}%


%{-- PESQUISA --}%
    <div class="row text-center ficha-html-group">
        <i class="i-accordion fa fa-chevron-down" title="Abrir/Fechar tópico" data-toggle="collapse" data-target="#topicoPesquisa" onclick="toggleImage(this)"></i>

        <p>
            <g:textoFixo txOriginal="Pesquisa"></g:textoFixo>
        </p>
    </div>

    <div class="text-left ficha-html-sublabel collapse" id="topicoPesquisa">
        <div class="row">
            <g:textoLivre noTabela="ficha" noColuna="dsPesquisaExistNecessaria" sqRegistro="${fichaRevisao?.sq_ficha}"></g:textoLivre>
        </div>
    </div>   %{--     FIM PESQUISA --}%


%{-- EQUIPE TÉCNICA --}%
    <div class="row text-center ficha-html-group">
        <i class="i-accordion fa fa-chevron-down" title="Abrir/Fechar tópico" data-toggle="collapse" data-target="#topicoEquipeTecnica" onclick="toggleImage(this)"></i>

        <p>
            <g:textoFixo txOriginal="Equipe técnica"></g:textoFixo>
        </p>
    </div>

    <div class="text-left ficha-html-sublabel collapse" id="topicoEquipeTecnica">

    </div>   %{-- FIM EQUIPE TECNICA --}%


%{-- COLABORADORES --}%
    <div class="row text-center ficha-html-group">
        <i class="i-accordion fa fa-chevron-down" title="Abrir/Fechar tópico" data-toggle="collapse" data-target="#topicoColaboradores" onclick="toggleImage(this)"></i>

        <p>
            <g:textoFixo txOriginal="Colaboradores"></g:textoFixo>
        </p>
    </div>
    <div class="text-left ficha-html-sublabel collapse" id="topicoColaboradores">

    </div>   %{-- FIM COLABORADORES --}%

    %{-- COMO CITAR --}%
    <div class="row text-center ficha-html-group">
        <i class="i-accordion fa fa-chevron-down" title="Abrir/Fechar tópico" data-toggle="collapse" data-target="#topicoComoCitar" onclick="toggleImage(this)"></i>
        <p>
            <g:textoFixo txOriginal="Como citar"></g:textoFixo>
        </p>
    </div>
    <div class="text-left ficha-html-sublabel collapse" id="topicoComoCitar">
    </div>   %{-- FIM COMO CITAR --}%


    %{-- REFERENCIAS BIBLIOGRAFICAS --}%
    <div class="row text-center ficha-html-group">
        <i class="i-accordion fa fa-chevron-down" title="Abrir/Fechar tópico" data-toggle="collapse" data-target="#topicoRefBib" onclick="toggleImage(this)"></i>
        <p>
            <g:textoFixo txOriginal="Referências Bibliográficas"></g:textoFixo>
        </p>
    </div>
    <div class="text-left ficha-html-sublabel collapse" id="topicoRefBib">
    </div>   %{-- FIM REFERENCIAS BIBLIOGRAFICAS --}%

    %{-- REFERENCIAS DOS REGISTROS --}%
    <div class="row text-center ficha-html-group">
        <i class="i-accordion fa fa-chevron-down" title="Abrir/Fechar tópico" data-toggle="collapse" data-target="#topicoRefRegistro" onclick="toggleImage(this)"></i>
        <p>
            <g:textoFixo txOriginal="Referências dos Registros"></g:textoFixo>
        </p>
    </div>
    <div class="text-left ficha-html-sublabel collapse" id="topicoRefRegistro">
    </div>   %{-- FIM REFERENCIAS REGISTROS --}%

</div> %{-- FIM CONTAINER REVISAO FICHA --}%
<div class="end-page"></div>
