<div class="window-container text-left">
	<form id="frmUso" name="frmUso" class="form-inline" role="form">
		<div class="form-group">
			<label for="sqCriterioAmeacaIucn" class="control-label">
				Critério de ameaça
			</label>
			<br>
			<select name="sqCriterioAmeacaIucn" id="sqCriterioAmeacaIucn" class="form-control select2" style="width:600px;"
				data-s2-placeholder="?"
				data-s2-minimum-input-length="0"
                data-s2-auto-height="true">
				<option value="">?</option>
				<g:each var="item" in="${listCriteriosAmeaca}">
					<option value="${item.id}" ${ item.id == uso?.criterioAmeacaIucn?.id ? " selected":"" }>${item.descricaoCompleta}</option>
					<g:each var="filho" in="${item.itens.sort{it.codigo}}">
						<option value="${filho.id}" ${ filho.id == uso?.criterioAmeacaIucn?.id ? " selected":"" }>&nbsp;&nbsp;${filho.descricaoCompleta}</option>
						<g:each var="neto" in="${filho.itens.sort{it.codigo}}">
							<option value="${neto.id}" ${ neto.id == uso?.criterioAmeacaIucn?.id ? " selected":"" }>&nbsp;&nbsp;&nbsp;&nbsp;${neto.descricaoCompleta}</option>
						</g:each>

					</g:each>
				</g:each>
			</select>
		</div>

        %{--        INTEGRACAO SIS/IUCN--}%
        <div class="form-group">
			<label for="sqIucnUso" class="control-label">
				Uso (SIS/IUCN)
			</label>
			<br>
			<select name="sqIucnUso" id="sqIucnUso" class="form-control select2" style="width:600px;"
				data-s2-placeholder="?"
				data-s2-minimum-input-length="0"
                data-s2-auto-height="true">
				<option value="">?</option>
				<g:each var="item" in="${listIucnUso}">
					<option value="${item.id}" ${ item.id == uso?.iucnUso?.id ? " selected":"" }>${ item.descricaoCompleta }</option>
				</g:each>
			</select>
		</div>

		<br />
		<div class="form-group">
			<label for="codigo" class="control-label">
				Código
			</label>
			<br>
			<input name="codigo" id="codigo" type="text" class="form-control form-clean fld80" placeholder="Código" value="${uso?.codigo}" required="false"/>
		</div>

		<div class="form-group">
			<label for="inputUso" class="control-label">
				Descrição do Uso
			</label>
			<br>
			<input name="descricao" id="inputUso" type="text" class="form-control form-clean fld500" placeholder="Informe o nome do uso" value="${uso?.descricao}" required="true" data-msg-required="Campo Obrigatório"/>
		</div>

        <br>

        <div class="form-group">
            <label for="ordem" class="control-label">
                Ordem listagem
            </label>
            <br>
            <input name="ordem" id="ordem" type="text" class="form-control form-clean fld200" placeholder="Ordem apresentação" value="${uso?.ordem}" required="false"/>
        </div>
		<div class="fld panel-footer">
			<button id="btnSaveFrmUso" data-action="uso.save" class="fld btn btn-success">Gravar</button>
		</div>
	</form>
</div>
