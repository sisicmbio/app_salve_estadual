<div class="col-sm-12">
    <div class="panel panel-default w100p" style="">
        <div class="panel-heading">
            <span>
                Manutenção da Classificação Taxonômica
            </span>
            <button id="btnClose" data-action="app.unloadModule" class="btn btn-default pull-right btn-xs" title="Fechar"><span class="glyphicon glyphicon-remove btnClose"></span></button>
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-sm-12" id="divTrilha" style="display: none">
                    <ol id="olTrilha" class="breadcrumb" style="background-color:#ffffc3;border: 1px solid #c9b97a">
                        %{--<li class="breadcrumb-item">Reino</li>
                        <li class="breadcrumb-item">Filo</li>
                        <li class="breadcrumb-item">Classe</li>--}%
                    </ol>
                </div>
            </div>
            <form id="frmCadTaxon" class="form-inline" role="form">
                <input name="sqTaxon" id="sqTaxon" type="hidden" value="" />
                <input name="sqTaxonRemoto" id="sqTaxonRemoto" type="hidden" value="" />
                <div class="fld form-group">
                    <label for="sqNivelPai" class="control-label">Nivel taxonômico superior</label>
                    <br>
                    <select id="sqNivelPai" name="sqNivelPai" data-change="cadastro.sqNivelPaiChange" class="fld form-control fld300 fldSelect">
                        <option value="">-- não possui --</option>
                        <g:each var="item" in="${listNivelTaxonomico}">
                            <option data-nivel="${item.nuGrauTaxonomico}" value="${item.coNivelTaxonomico}" ${item.nuGrauTaxonomico>100 ? 'disabled':''}>${item.deNivelTaxonomico}</option>
                        </g:each>
                    </select>
                </div>
                <div class="fld form-group" style="display:none">
                    <label for="sqTaxonPai" class="control-label">Qual?</label>
                    <br>
                    <select id="sqTaxonPai" name="sqTaxonPai" data-change="cadastro.sqTaxonPaiChange" class="fld form-control select2 fld300"
                            data-s2-params="sqNivelPai|nivel"
                            data-s2-url="ficha/select2Taxon"
                            data-s2-placeholder="?"
                            data-s2-maximum-selection-length="2">
                    </select>
                    <button id="btnAlterar" data-action="cadastro.edit" class="btn btn-primary btn-sm hidden" title="Clique aqui para alterar este nível selecionado">Alterar</button>
                    <button id="btnExcluir" data-action="cadastro.delete" class="btn btn-danger btn-sm hidden" title="Clique aqui para excluir este nível selecionado">Excluir</button>
                </div>

                <fieldset id="fldSet">


                    <div class="fld form-group">
                        <label for="sqNivelTaxonomico" class="control-label" id="lblNoSubnivel">Nivel</label>
                        <br>
                        <select id="sqNivelTaxonomico" name="sqNivelTaxonomico" class="fld form-control fld300 fldSelect">
                            <option data-codigo="" data-nivel="0" value="">?</option>
                            <g:each var="item" in="${listNivelTaxonomico}">
                                <option data-codigo="${item.coNivelTaxonomico}" data-nivel="${item.nuGrauTaxonomico}" value="${item.id}" ${item.nuGrauTaxonomico<20 ? 'disabled':''}>${item.deNivelTaxonomico}</option>
                            </g:each>
                        </select>
                    </div>
                    <div class="fld form-group">
                        <label for="noTaxon" class="control-label" id="lblNoTaxon" >Taxon</label>
                        <br>
                        <div class="input-group">
                            <input class="form-control fld300" type="text" name="noTaxon" id="noTaxon" value="" onchange="cadastro.noTaxonChange()">
                            <span class="input-group-btn">
                                <button id="btnConsultarTaxonRemoto" class="btn btn-primary btn-sm" data-action="cadastro.consultaTaxonRemoto"
                                        data-params="noTaxon,sqNivelTaxonomico,sqNivelPai"
                                        type="button">Consultar</button>
                            </span>
                            <span class="input-group-btn">
                                <i id="checkOk" style="display: none" class="ml5 green fa fa-check"></i>
                            </span>
                        </div>
                    </div>

                    <div class="fld form-group">
                        <label for="noAutor" class="control-label" >Autor e Ano</label>
                        <br>
                        <input class="form-control fld300" type="text" name="noAutor" id="noAutor" value="">
                    </div>
                    <br>
                    <div class="fld form-group">
                        <label for="inOcorreBrasil" class="control-label" >Ocorre no Brasil?</label>
                        <br>
                        <select id="inOcorreBrasil" name="inOcorreBrasil" class="fld form-control fldSelect fld150" required="true">
                            <option value="">-- selecione --</option>
                            <option value="1" selected>Sim</option>
                            <option value="0">Não</option>
                        </select>
                    </div>
                    %{--<div class="fld form-group">--}%
                        %{--<label for="" class="control-label" >Válido para Avaliação?</label>--}%
                        %{--<br>--}%
                        %{--<select id="inAvaliacao" name="inAvaliacao" class="fld form-control fldSelect fld200" required="true">--}%
                            %{--<option value="">-- selecione --</option>--}%
                            %{--<option value="1" selected>Sim</option>--}%
                            %{--<option value="0">Não</option>--}%
                        %{--</select>--}%
                    %{--</div>--}%
                    <div class="fld panel-footer">
                        <button id="btnSave" data-action="cadastro.save" class="fld btn btn-success">Gravar</button>
                        <button id="btnReset" data-action="cadastro.reset" class="fld btn btn-info hidden">Limpar formulário</button>
                    </div>
                </fieldset>
        </div>
    </div>
</div>
