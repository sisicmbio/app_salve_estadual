//# sourceURL=N:\SAE\sae\grails-app\views\taxonomia\cadastro.js
//
/* ************************************************************* */
/*                     CADASTRO DE TAXONOMIA
/* ************************************************************* */
;
var cadastro = {
    $divTrilha:null,
    $olTrilha:null,
    $checkOk:null,
    $btnConsultaTaxonRemoto:null,
    init: function() {
        cadastro.sqNivelPaiChange();
        cadastro.$divTrilha = $("#divTrilha");
        cadastro.$olTrilha  = cadastro.$divTrilha.find('ol#olTrilha');
        cadastro.$checkOk = $("#checkOk");
        cadastro.$btnConsultaTaxonRemoto = $("#btnConsultarTaxonRemoto");
    },
    sqNivelPaiChange: function(params) {
        var nivelSelecionado = $("#sqNivelPai option:selected").data('nivel') || '0'
        var editing = ($("#sqTaxon").val() > 0);
        $("#sqTaxonPai").empty(); // limpar select2
        $("#sqNivelTaxonomico").val('');
        var nivel = $("#sqNivelPai").val();
        if (nivel) {
            $("#sqTaxonPai").parent().show()
            $("#sqNivelTaxonomico").parent().show();
            $("#lblNoSubnivel").text('Subnível');
            $("#lblNoTaxon").text('Táxon');
        } else {
            $("#lblNoSubnivel").text('Nível');
            $("#lblNoTaxon").text('Reino');
            $("#sqTaxonPai").parent().hide()
            $("#sqNivelTaxonomico").val('').parent().hide();
        }
        // habilitar / desabilitar os nívels do novo táxon em função do Superior
        $("#sqNivelTaxonomico option").each(function() {
            if ($(this).data('nivel') == '0' || $(this).data('nivel') > nivelSelecionado) {
                $(this).show();
            } else {
                if (!editing) {
                    $(this).hide();
                }
            }
        });
        if ($("#sqNivelTaxonomico:visible").size() == 0) {
            //$("#sqNivelTaxonomico option[data-codigo=REINO]").prop('selected',true)
            $("#sqNivelTaxonomico").val(1);
        }
        cadastro.sqTaxonPaiChange();

    },
    sqTaxonPaiChange: function(params) {
        if ($("#sqTaxonPai").val()) {
            $("#btnAlterar").removeClass('hidden');
            $("#btnExcluir").removeClass('hidden');
        } else {
            $("#btnAlterar").addClass('hidden');
            $("#btnExcluir").addClass('hidden');
        }
    },
    save: function(params) {
        if ($("#sqTaxonPai:visible").size() > 0 && !$("#sqTaxonPai").val()) {
            return app.alertError('Selecione o táxon Superior!')
        }
        var subnivel = $("#sqNivelTaxonomico option:selected").val();
        if (!subnivel) {
            return app.alertError('Selecione o Subnível!')
        }
        if (!$("#noTaxon").val()) {
            return app.alertError('Informe a descrição!')
        }
        /*if (!$("#noAutor").val()) {
            return app.alertError('Informe o Autor e Ano!')
        }*/
        if (!$("#frmCadTaxon").valid()) {
            return;
        }
        var data = $("#frmCadTaxon").serializeArray();
        data = app.params2data(data, params);
        data.sqNivelTaxonomico = subnivel;
        app.confirm('Confirma inclusão do(a) ' + $("#lblNoTaxon").text() + '?', function() {
            app.ajax(app.url + 'taxonomia/save', data,
                function(res) {
                    if (!res.status) {
                        cadastro.reset()
                    }
                }, null, 'json'
            );
        })
    },
    edit: function(params) {
        var data = { "sqTaxon": $("#sqTaxonPai").val() };
        app.ajax(app.url + 'taxonomia/edit', data,
            function(res) {
                if (!res.status) {
                    $("#sqTaxonPai").val('');
                    app.setFormFields(res);
                    $("#sqNivelPai").attr('disabled', true);
                    $("#btnReset").removeClass('hidden');
                    $("#sqTaxonPai").parent().hide();
                    $("#sqNivelTaxonomico").parent().hide();
                    cadastro.setTrilha( res.json_trilha );
                }
            }, null, 'json'
        );
    },
    delete: function(params) {
        var data = {"sqTaxon": $("#sqTaxonPai").val()};
        app.confirm('Confirma a exclusão?', function () {
            app.ajax(app.url + 'taxonomia/delete', data,
                function (res) {
                    if (!res.status) {
                        cadastro.reset();
                        /*$("#sqTaxonPai").val('');
                        app.setFormFields(res);
                        $("#sqNivelPai").attr('disabled', true);
                        $("#btnReset").removeClass('hidden');
                        $("#sqTaxonPai").parent().hide();
                        $("#sqNivelTaxonomico").parent().hide();
                        cadastro.setTrilha(res.json_trilha);*/
                    }
                }, null, 'json');
        });
    },
    setTrilha:function( jsonTrilha ){
        var json = {}
        cadastro.$divTrilha.hide();
        try {
           json =  $.parseJSON( jsonTrilha );
        } catch (e ){
            return;
        }
        cadastro.$olTrilha.html('');
        var novaTrilha = [];
        for( key in json ){
            novaTrilha.push('<li title="'+ json[key].de_nivel_taxonomico+'" class="cursor-pointer breadcrumb-item">' + json[key].no_taxon + '</li>');
        }
        cadastro.$olTrilha.html(novaTrilha);
        cadastro.$divTrilha.show('slow');

    },
    reset: function(params) {
        //app.reset("frmCadTaxon","inOcorreBrasil,inValidacao"); // não limpar inOcorreBrasil e inValidacao
        $("#sqTaxon").val('');
        $("#sqTaxonRemoto").val('');
        $("#sqNivelPai").val('')
        $("#sqTaxonPai").empty()
        $("#sqNivelTaxonomico").val('')
        $("#noAutor,#noTaxon").val('')
        cadastro.$checkOk.hide();
        cadastro.$btnConsultaTaxonRemoto.prop('disabled',false);

        $("#sqNivelPai").attr('disabled', false);
        $("#btnReset").addClass('hidden');
        $("#sqTaxonPai").parent().show();
        $("#sqNivelTaxonomico").parent().show();
        cadastro.$divTrilha.hide('fast');
        cadastro.$olTrilha.html('');

        cadastro.sqNivelPaiChange();
        app.focus('sqNivelPai');
    },
    consultaTaxonRemoto:function(params,ele,evt){
        if( !params.noTaxon ) {
            return;
        }
        // limpar os campos remotos e esconder o check da validação remota
        cadastro.noTaxonChange();
        cadastro.$btnConsultaTaxonRemoto.prop('disabled',true);
        app.blockUI('Consultando a base taxonômica ICMBio! Aguarde...',function(){
            var data = app.params2data({}, params);
            app.ajax(app.url + 'taxonomia/consultarTaxonRemoto', data,
                function(res) {
                    if ( res.status == 0 ) {
                        data = res.data.data
                        if( res.data.message ){
                            app.alertInfo(res.data.message,'Mensagem');
                        }
                        if( data.length > 0 ){
                            data = data[0]
                            var sqTaxonRemoto = data.sq_taxon
                            var noTaxonRemoto = data.no_taxon
                            var noAutorRemoto = data.no_autor
                            var nuAnoRemoto   = data.nu_ano
                            $("#sqTaxonRemoto").val( sqTaxonRemoto);
                            $("#noTaxon").val( noTaxonRemoto );
                            if( noAutorRemoto ){
                                $("#noAutor").val( noAutorRemoto );
                            }
                            app.growl('Táxon encontrado na base taxonômica do ICMBio!',3,'Mensagem','tc','large','success')
                            cadastro.$checkOk.show();
                            app.focus('noAutor');
                            cadastro.setTrilha( data.json_trilha );
                        } else {
                            app.alertInfo('Táxon não existe na base de dados do ICMBio','',function(){
                                app.focus('noTaxon');
                            });
                            cadastro.$btnConsultaTaxonRemoto.prop('disabled',false);
                        }
                    }
                }, null, 'json'
            );
        })
    },
    noTaxonChange:function(){
        cadastro.$btnConsultaTaxonRemoto.prop('disabled',false);
        cadastro.$checkOk.hide();
        $("#sqTaxonRemoto").val('');
        cadastro.$olTrilha.html('');
    }
}
cadastro.init();
