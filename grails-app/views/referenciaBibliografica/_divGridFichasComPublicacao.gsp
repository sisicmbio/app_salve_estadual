<!-- view: /views/oficina/_divGridFichasComPublicacao.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<table class="table table-striped table-bordered table-hover">
   <thead>
      <tr>
         <th>Taxon</th>
         <th style="width:300px;">Ciclo avaliação</th>
      </tr>
   </thead>
   <tbody>
      <g:each var="item" in="${lista}">
         <tr>
            <td>${raw( br.gov.icmbio.Util.ncItalico( item.vwFicha.nmCientifico,true ) )}</td>
            <td>${item.vwFicha.cicloAvaliacao.deCicloAvaliacao}</td>
         </tr>
      </g:each>
   </tbody>
</table>