<!-- view: /views/ficha/index.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action} - gridName = modalCadRefBib-->


<g:set var="canEdit" value="${session.sicae.user.canEditRefBib()?true:false}" />

<table class="table table-striped table-bordered table-hover" id="tableGridConsultaRefBib">
   <thead>
      <tr>
         <th>Ano</th>
         <th>Referência</th>
         <th>Autor(es)</th>
         <th>Corrigir<br>Duplicidade
            <i class="label-help glyphicon glyphicon-question-sign blue tooltipstered"
               title='Caso você identifique duas ou mais referências em duplicidade,
                  <br>clique na caixa de seleção das mesmas e depois no botão <b>Manter</b>
                  <br>daquela que deverá permanecer e substitiur as demais na base de dados.
                  <p class="red"><b>Observação: utilize esta funcionalidade com muita cautela e atenção,
                  <br>tenha certeza de que as referências estejam relmente duplicadas pois
                  <br>as referências substituidas serão removidas definitivamente da base de dados.</b></p>'></i>
         </th>
          <th>Id<br>Salve
              <i class="label-help glyphicon glyphicon-question-sign blue tooltipstered"
                 title='Utilize este código para identificar esta referência<br>na planilha de importação de registros de ocorrências do SALVE'></i>
          </th>
          <th>Anexo</th>
          <th>Ação</th>
      </tr>
   </thead>
   <tbody>
      <g:each var="item" in="${lista}">
         <tr>
            <td class="text-center" style="width:60px">${raw(item?.nuAnoPublicacao)}</td>
            %{--<td>${raw(item.deTitulo)}</td>--}%

            %{-- TITULO--}%
             <td>
                <div style="max-width: 400px;overflow-x: auto;">
                    ${raw(item.deTitulo)}
                    <div class="text-left" style="padding-top:3px;margin-top:4px;border-top:0px dashed #757575;color:#c7bfbf;">${raw(item.referenciaHtml)}</div>
                </div>
            </td>

%{--             AUTOR --}%
            <td style="max-width:200px;">${raw(item.noAutor?.replaceAll(/\n|; ?+/,'<br/>'))}</td>

%{--             CORRIGIR DUPLICIDADE--}%
            <td class="text-center" style="width:100px;">
               <input class="checkbox-lg chk-corrigir-duplicidade" data-action="refBib.chkCorrigirDuplicidadeClick" type="checkbox" value="${item.id}" class="checkbox-lg">
               <br>
               <button class="btn-corrigir-duplicidade" id="btnCorrigirDuplicidade-${item.id}" data-action="refBib.btnCorrigirDuplicidadeClick" data-sq-publicacao="${item.id}" type="button" style="display:none;" title="Clique neste botão para manter esta na base de dados e eliminar as outras.">Manter</button>
            </td>

            %{--ID SALVE--}%
            <td style="width:100px;text-align: center">${item?.id?.toString()}</td>

             %{--ANEXO--}%
             <td style="width:100px;"class="text-center">
                 <g:if test="${item?.anexos?.size()>0}">
                     <g:link title="Baixar documento: ${item.anexos[0].noPublicacaoArquivo}" class="btn fld btn-default btn-xs btn-download" controller="referenciaBibliografica" action="downloadAnexo" id="${ item.anexos[0].id }">
                         <span class="glyphicon glyphicon-download-alt"></span>
                     </g:link>
                 </g:if>
             </td>

            <td class="td-actions">
                <a data-action="refBib.edit" data-id="${item.id}" class="btn fld btn-default btn-xs btn-update" title="${canEdit?'Alterar':'Ver Campos'}">
                   <span class="glyphicon glyphicon-pencil"></span>
                </a>
                <g:if test="${canEdit}">
                    <a  data-action="refBib.delete" data-id="${item.id}" class="btn fld btn-default btn-xs btn-delete" title="Excluir">
                        <span class="glyphicon glyphicon-remove"></span>
                    </a>
                </g:if>
            </td>
         </tr>
      </g:each>
   </tbody>
</table>
