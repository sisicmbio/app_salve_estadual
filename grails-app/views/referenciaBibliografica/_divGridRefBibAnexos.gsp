<table class="table table-striped table-bordered table-hover">
   <thead>
      <tr>
         <th>Documento Anexado</th>
         <th>Ação</th>
      </tr>
   </thead>
   <tbody>
      <g:each var="item" in="${lista}">
         <tr>
            <td>${raw(item.noPublicacaoArquivo)}</td>
            <td class="td-actions">
            <g:link title="Baixar" class="btn fld btn-default btn-xs btn-download" action="downloadAnexo" id="${item.id}">
               <span class="glyphicon glyphicon-download-alt"></span>
            </g:link>
            <a data-id="${item.id}" data-action="refBib.deleteAnexo" data-descricao="${item.noPublicacaoArquivo}" class="btn fld btn-default btn-xs btn-delete" title="Excluir">
               <span class="glyphicon glyphicon-remove"></span>
            </a>
            </td>
         </tr>
      </g:each>
   </tbody>
</table>
