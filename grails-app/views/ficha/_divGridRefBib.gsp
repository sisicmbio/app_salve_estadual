<!-- view: /views/ficha/_divGridRefBib.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action} - gridName = refBib -->

<g:set var="canEdit" value="${session.sicae.user.canEditRefBib()?true:false}" />
<table class="table table-striped table-bordered table-hover">
   <thead>
      <tr>
%{--         <td>Id Salve</td>--}%
         <th>Referência Bibliográfica</th>
         <th>Tags</th>
         <th>Anexo</th>
         <th>Ação</th>
      </tr>
   </thead>
   <tbody>
      <g:each var="item" in="${listFichaRefBib}">
         <tr>
            %{-- <td>${raw( item.publicacao ? item.publicacao.tituloAutorAno : item.noAutor+', '+item.nuAno )}</td> --}%
%{--            <td style="width:80px;text-align: center">${item?.publicacao?.id}</td>--}%
            <td>${raw( item.referenciaHtml + ( item.publicacao ? '<span style="color:silver">   (Id Salve:' + item.publicacao.id.toString() + ')<span>':'' ) )  }
             </td>
            <td>${raw(item.tagsAsHtml)}</td>
            <td class="text-center">
               <g:if test="${item?.publicacao?.anexos?.size()>0}">
                  <g:link title="Baixar documento: ${item.publicacao.anexos[0].noPublicacaoArquivo}" class="btn fld btn-default btn-xs btn-download" controller="referenciaBibliografica" action="downloadAnexo" id="${ item?.publicacao?.anexos[0]?.id }">
                     <span class="glyphicon glyphicon-download-alt"></span>
                  </g:link>
               </g:if>
            </td>

            <td class="td-actions">

            <g:if test="${ canEdit }">
               <a data-action="ficha.editRefBib" data-sq-ficha-ref-bib="${item?.id}"
                  data-no-autor="${item?.noAutor}" data-nu-ano="${item?.nuAno}"
                   data-all="false"
                  data-sq-publicacao="${item?.publicacao?.id}" data-all="false" class="btn fld btn-default btn-xs btn-update" title="Alterar">
                  <span class="glyphicon glyphicon-pencil"></span>
               </a>
               <a  data-action="ficha.deleteFichaRefBib"
                   data-sq-ficha-ref-bib="${item?.id}"
                   data-all="false"
                   class="btn fld btn-default btn-xs btn-delete" title="Excluir">
                   <span class="glyphicon glyphicon-remove"></span>
               </a>
            </g:if>

            </td>
         </tr>
      </g:each>
   </tbody>
</table>
