<form id="frmPesquisaDetalhe" role="form" class="form-inline">
	<input type="hidden" name="sqFichaPesquisa" value=""/>
	<div class="fld form-group">
		<label for="sqTemaPesquisa" class="control-label" title="Selecione 1 ou mais temas com a tecla control ou cmd pressionada!">Tema(s)</label>
		<br>
		%{--<select name="sqTemaPesquisa" class="form-control fld250 fldSelect" required="true">--}%
			%{--<option value="">-- selecione --</option>--}%
			%{--<g:each var="item" in="${listTema}">--}%
				%{--<option value="${item.id}">${item.descricao}</option>--}%
			%{--</g:each>--}%
		%{--</select>--}%
		<select name="sqTemaPesquisa" id="sqTemaPesquisa"
				class="form-control select2 fld500" multiple="multiple"
				data-s2-multiple="true"
				data-s2-minimum-input-length="0"
				data-s2-maximum-selection-length="10">
			<g:each var="item" in="${listTema}">
				<option value="${item.id}">${item.descricao}</option>
			</g:each>
		</select>
	</div>
	<br>

	<div class="fld form-group">
		<label for="" class="control-label">Situação</label>
		<br>
		<select name="sqSituacaoPesquisa" class="form-control fld250 fldSelect" required="true">
			<option value="">-- selecione --</option>
			<g:each var="item" in="${listSituacao}">
				<option value="${item.id}">${item.descricao}</option>
			</g:each>
		</select>
	</div>
	%{--
	Carlos pediu para ocultar este campo
	<br class="fld">
	<div class="fld form-group w100p">
		<label for="" class="control-label">Observações Sobre a Pesquisa</label>
		<br>
		<textarea name="txFichaPesquisa" rows="7" class="fld form-control fldTextarea wysiwyg w100p"></textarea>
	</div> --}%

	%{-- botão gravar --}%
	<div class="fld panel-footer">
	    <button id="btnSaveFrmPesquisaDetalhe" data-action="pesquisa.saveFrmPesquisaDetalhe" data-params="sqFicha" class="fld btn btn-success">Adicionar Pesquisa</button>
    	<button id="btnResetFrmPesquisaDetalhe" data-action="pesquisa.resetFrmPesquisaDetalhe" class="fld btn btn-info hide">Limpar Formulário</button>
	</div>
</form>

<div id="divGridPesquisa" class="mt10">
	<g:render template="/ficha/pesquisa/divGridPesquisa"></g:render>
</div>

<form id="frmPesquisa" role="form" class="form-inline">
	%{-- Observação da tabela ficha ds_pesquisa_exist_necessaria --}%
	<div class="form-group w100p">
        <label for="dsPesquisaExistNecessaria" class="control-label">Observações Gerais Sobre as Pesquisas Existentes/Necessárias
            <i data-action="ficha.openModalSelRefBibFicha"
               data-no-tabela="ficha"
               data-no-coluna="ds_pesquisa_exist_necessaria"
               data-sq-registro="${ficha.id}"
               data-de-rotulo="Pesquisas existentes/necessárias" class="fa fa-book ml10"
               title="Informar Referência Bibliográfica" style="color: green;" />
            </label>
		<br>
		<textarea name="dsPesquisaExistNecessaria" id="dsPesquisaExistNecessaria" rows="18" class="fld form-control fldTextarea wysiwyg w100p" required="true">${ficha.dsPesquisaExistNecessaria}</textarea>
	</div>
	%{-- botão gravar direto na ficha --}%
	<div class="fld panel-footer">
		<button data-action="pesquisa.saveFrmPesquisa" data-params="sqFicha" class="fld btn btn-success">Gravar Observações Gerais</button>
	</div>
</form>
