//# sourceURL=N:\SAE\sae\grails-app\views\ficha\pesquisa\pesquisa.js
//
;
var pesquisa = {
	tabPesquisaInit: function() {
		pesquisa.updateGridPesquisa();
	},

	saveFrmPesquisa: function(params) {
		if (!$('#frmPesquisa').valid()) {
			return false;
		}
		var data = $("#frmPesquisa").serializeArray();
		if (!data) {
			return;
		}
		data = app.params2data(params, data);
		if (!data.sqFicha) {
			app.alertError('ID da ficha não encontrado!');
			return;
		}
		app.ajax(app.url+'fichaPesquisa/saveFrmPesquisa', data,
			function(res) {
				app.reset('frmPesquisa','dsPesquisaExistNecessaria');
			}, null, 'json'
		);
	},
	saveFrmPesquisaDetalhe: function(params) {
		if (!$('#frmPesquisaDetalhe').valid()) {
			return false;
		}
		var data = $("#frmPesquisaDetalhe").serializeArray();
		if (!data) {
			return;
		}
		data = app.params2data(params, data);

		if( ! $("#frmPesquisaDetalhe #sqTemaPesquisa").val() )
		{
			app.alertInfo('Selecione 1 ou mais temas!');
			return;
        }

        // concatenar os temas selecionados com virgula
		data.sqTemaPesquisa  = $("#frmPesquisaDetalhe #sqTemaPesquisa").val().join(',');

		if (!data.sqFicha) {
			app.alertError('ID da ficha não encontrado!');
			return;
		}
		app.ajax(app.url+'fichaPesquisa/saveFrmPesquisaDetalhe', data,
			function(res) {
				pesquisa.resetFrmPesquisaDetalhe();
				pesquisa.updateGridPesquisa();
				app.focus('sqTemaPesquisa');
			}, null, 'json'
		);

	},
	resetFrmPesquisaDetalhe: function() {
		app.reset('frmPesquisaDetalhe');
		$("#sqTemaPesquisa").focus();
		$("#btnResetFrmPesquisaDetalhe").addClass('hide');
		$("#btnSaveFrmPesquisaDetalhe").html($("#btnSaveFrmPesquisaDetalhe").data('value'));
		app.unselectGridRow('divGridPesquisa');
	},
	updateGridPesquisa:function()
	{
		ficha.getGrid('pesquisa');
	},
	editPesquisa : function(params,btn)
	{
		app.ajax(app.url+'fichaPesquisa/editPesquisa', params,
			function(res) {
				$("#btnResetFrmPesquisaDetalhe").removeClass('hide');
				$("#btnSaveFrmPesquisaDetalhe")
					.data('value', $("#btnSaveFrmPesquisaDetalhe").html());
				$("#btnSaveFrmPesquisaDetalhe").html("Gravar Alteração");
				app.setFormFields(res, 'frmPesquisaDetalhe');
				app.selectGridRow(btn);
			});
	},
	deletePesquisa: function(params,btn) {
		app.selectGridRow(btn);
		app.confirm('<br>Confirma exclusão da Pesquisa <b>'+params.descricao+'</b>?',
			function() {
				app.ajax(app.url+'fichaPesquisa/deletePesquisa',
					params,
					function(res) {
						pesquisa.updateGridPesquisa();
					});
			},
			function() {
				app.unselectGridRow(btn);
			}, params, 'Confirmação', 'warning');
	},
};