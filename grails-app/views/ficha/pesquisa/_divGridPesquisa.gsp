<!-- view: /views/ficha/pesquisa/_divGridPesquisa.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<table class="table table-striped table-bordered table-hover">
   <thead>
      <tr>
         <th>Tema</th>
         <th>Situação</th>
         %{-- <th>Observações</th> --}%
         <th style="width:200px;">Ref. Bibliográfica</th>
         <g:if test="${ ! session.sicae.user.isCO() }">
            <th>Ação</th>
         </g:if>
      </tr>
   </thead>
   <tbody>
      <g:each var="item" in="${listPesquisa}">
      <tr>
         <td>${item.tema.descricao}</td>
         <td>${item.situacao.descricao}</td>
         %{-- <td>${item.txFichaPesquisa}</td> --}%
         <td>
            <a data-action="ficha.openModalSelRefBibFicha"
               data-no-tabela="ficha_pesquisa"
               data-no-coluna="sq_ficha_pesquisa"
               data-sq-registro="${item.id}"
               data-de-rotulo="Pesquisa - ${item.tema.descricao}"
               data-com-pessoal="true"
               data-update-grid="divGridPesquisa" class="fld btn btn-default btn-xs btn-grid-subform" title="Adicionar/Remover Refeferência bibliográfica."><span class="fa fa-book"></span></a>
               &nbsp;
               ${raw(item.refBibHtml)}
         </td>
         <g:if test="${ ! session.sicae.user.isCO() }">
            <td class="td-actions">
               %{--<a data-action="pesquisa.editPesquisa"  data-id="${item.id}" class="fld btn btn-default btn-xs btn-update" title="Alterar">--}%
                  %{--<span class="glyphicon glyphicon-pencil"></span>--}%
               %{--</a>--}%
               <a data-action="pesquisa.deletePesquisa" data-descricao="${item.tema.descricao}" data-id="${item.id}" class="fld btn btn-default btn-xs btn-delete" title="Excluir">
                  <span class="glyphicon glyphicon-remove"></span>
               </a>
            </td>
         </g:if>
      </tr>
      </g:each>
   </tbody>
</table>
