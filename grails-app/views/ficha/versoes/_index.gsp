<%@ page import="br.gov.icmbio.Util" %>
<!-- view: /views/ficha/versoes/_index.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<div style="max-width: 100%;overflow-y: auto">
<table class="mt10 table table-striped table-bordered table-hover table-responsive" id="tableFichaVersoes">
    <thead>
    <tr>
        <th style="width:70px;"><i class="fa fa-info-circle" title="Versão que está visível no módulo público"></i> Versão<br>publicada</th>
        <th style="width:100px;">Data</th>
        <th style="width:200px">Unidade</th>
        <th style="width:200px">Ponto focal</th>
        <th style="width:200px">Coordenador taxon</th>
        <th style="width:200px">Oficina avaliação</th>
        <th style="width:200px">Oficina validação</th>
%{--        <th style="width:200px">Apoio técnico</th>--}%
        <th style="width:200px">Colaboradores</th>
        <th style="width:80px;">Ação</th>
    </tr>
    </thead>
    <tbody>
    <g:each var="row" in="${rows}">
        <tr id="trVersao${row.sq_ficha_versao}">
            <td class="text-right nowrap ${row.st_publico ? 'bgGreenPastel':''}" >
                <g:if test="${row.cd_contexto=='VERSAO_PUBLICACAO'}">
                <input name="radioExibirVersaoModuloPublico${row.sq_ficha}" value="${row?.sq_ficha_versao}"
                       ${canModify ? '' : 'disabled' }
                       title="${ canModify ? 'Exibir no módulo público':''}"
                       type="radio"
                       class="fld radio-lg"
                       style="margin-left: 20px;"
                       onclick="ficha.publicarVersao(${row.sq_ficha_versao},${row.nu_versao})"
                    ${row.st_publico ? 'checked' : ''}
                />
                </g:if>
                <g:else>
                    <i class="red fa fa-info-circle" title="Ficha foi excluída do processo"></i>
                </g:else>
                <span class="ml5">v${row.nu_versao}</span>
            </td>

%{--            DATA VERSIONAMENTO--}%
            <td class="text-center" title="Versionado por ${row.no_pessoa}">${row.dt_inclusao}</td>

%{--            UNIDADE--}%
            <td class="text-center"><span class="nowrap">${row.sg_unidade_responsavel}</span></td>
%{--PONTO FOCAL            --}%
            <td class="text-center">
                <g:if test="${row?.js_funcoes}">
                    <g:set var="funcoes" value="${ grails.converters.JSON.parse( row.js_funcoes) }"></g:set>
                    <g:each var="funcao" in="${funcoes?.findAll{it.cd_papel_sistema=='PONTO_FOCAL'}}">
                        <span class="nowrap" title="${funcao.no_pessoa}">${ br.gov.icmbio.Util.nomeAbreviado(funcao.no_pessoa)}</span><br>
                    </g:each>
                </g:if>
            <g:else>
                <span>&nbsp;</span>
            </g:else>
        </td>

%{-- COORDENADOR DE TAXON           --}%
            <td class="text-center">
            <g:if test="${row?.js_funcoes}">
                <g:set var="funcoes" value="${ grails.converters.JSON.parse( row.js_funcoes) }"></g:set>
                <g:each var="funcao" in="${funcoes?.findAll{it.cd_papel_sistema=='COORDENADOR_TAXON'}}">
                    <span class="nowrap" title="${funcao.no_pessoa + ( funcao.no_instituicao ? '<br>Instituição: ' + funcao.no_instituicao : '')}">${ br.gov.icmbio.Util.nomeAbreviado(funcao.no_pessoa)}</span><br>
                </g:each>
            </g:if>
            <g:else>
                <span>&nbsp;</span>
            </g:else>
        </td>

%{-- OFICINA DE AVALIAÇÃO --}%
            <td class="text-center">
            <g:if test="${row?.js_oficinas}">
                <g:set var="oficinas" value="${ grails.converters.JSON.parse( row.js_oficinas) }"></g:set>
                <g:each var="oficina" in="${oficinas?.findAll{it.cd_tipo_sistema == 'OFICINA_AVALIACAO'}}">
                    <span class="nowrap" title="${oficina?.no_oficina+'<br>'+oficina.ds_periodo+'<br>'+oficina.de_local }">${oficina?.sg_oficina}</span><br>
                </g:each>
            </g:if>
            <g:else>
                <span>&nbsp;</span>
            </g:else>
        </td>

%{-- OFICINA DE VALIDAÇÃO --}%
            <td class="text-center">
            <g:if test="${row?.js_oficinas}">
                <g:set var="oficinas" value="${ grails.converters.JSON.parse( row.js_oficinas) }"></g:set>
                <g:each var="oficina" in="${oficinas?.findAll{it.cd_tipo_sistema == 'OFICINA_VALIDACAO'}}">
                    <span class="nowrap" title="${oficina?.no_oficina+'<br>'+oficina.ds_periodo+'<br>'+oficina.de_local }">${oficina?.sg_oficina}</span><br>
                </g:each>
            </g:if>
            <g:else>
                <span>&nbsp;</span>
            </g:else>
        </td>


%{-- APOIO TÉCNICO --}%
%{--

            <td class="text-center">
            <g:if test="${row?.js_papeis}">
                <g:set var="papeis" value="${ grails.converters.JSON.parse( row.js_papeis) }"></g:set>
                <g:each var="papel" in="${papeis?.findAll{it.cd_papel_sistema == 'APOIO_TECNICO'}}">
                    <span class="nowrap" title="${papel?.no_pessoa}">${br.gov.icmbio.Util.nomeAbreviado(papel?.no_pessoa)}</span><br>
                </g:each>
            </g:if>
            <g:else>
                <span>&nbsp;</span>
            </g:else>
        </td>

--}%

%{-- COLABORADORES --}%
            <td class="text-center">
                <span>${row.ds_colaboradores}</span>
            </td>
%{--
            <td class="text-center">
            <g:if test="${row?.js_colaboracoes}">
                <g:set var="colaboradores" value="${ grails.converters.JSON.parse( row.js_colaboracoes) }"></g:set>
                <g:each var="colaborador" in="${colaboradores}">
                    <span class="nowrap" title="${ colaborador?.no_usuario +' (' + colaborador?.de_email+')' }">${br.gov.icmbio.Util.nomeAbreviado(colaborador?.no_usuario)}</span><br>
                </g:each>
            </g:if>
            <g:else>
                <span>&nbsp;</span>
            </g:else>
            </td>
--}%


            <td class="td-actions">
                <button type="button"
                        data-action="showFichaVersao({sqFichaVersao:${row.sq_ficha_versao}})"
                        class="btn btn-default btn-xs"
                        data-toggle="tooltip"
                        data-placement="top"
                        title="Visualizar versão ${row.nu_versao}"
                        style="color:gray;">
                    <i class="fa fa-list btnPrint" aria-hidden="true"></i>
                </button>
               %{--
                <button type="button"
                        data-action="ficha.print"
                        data-no-cientifico=""
                        data-sq-ficha="${row.sq_ficha}"
                        data-sq-ficha-json="${row.sq_ficha_versao}"
                        class="btn btn-default btn-xs"
                        data-toggle="tooltip"
                        data-placement="top"
                        title="Imprimir versão ${row.nu_versao}"
                        style="color:gray;">
                    <i class="fa fa-print btnPrint" aria-hidden="true"></i>
                </button>--}%
            </td>
        </tr>
    </g:each>
    </tbody>
</table>
</div>
