<!-- view: /views/ficha/ameaca/_divGridAmeaca.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<table class="table table-striped table-bordered table-hover">
   <thead>
      <tr>
          <th style="min-width:300px;width:auto;">Vetor de ameaça</th>
          <th style="width:350px;">Efeito</th>
          <th style="width:200px;">Região</th>
          <th style="width:200px;">Georreferência</th>
          <th style="width:400px;">Ref. Bibliográfica</th>
          <g:if test="${ ! session.sicae.user.isCO() }">
            <th style="width:40px;">Ação</th>
          </g:if>
      </tr>
   </thead>
   <tbody>
   <g:each var="item" in="${listFichaAmeaca}">
      <tr>
         <td>
%{--             ${raw( item.criterioAmeacaIucn.descricaoHieraquia.replaceAll(/\s\/\s/,"<br>") ) }--}%
             ${raw( item.trilhaPath.replaceAll(/\s\/\s/,"<br>") +
                     (item.referenciaTemporal ? '<span title="Referência temporal." class="blue">&nbsp;('+ item.referenciaTemporal.descricao +')</span>&nbsp;':'')+
                     (item.nuPeso ? '<br><span title="Peso da ameaça no PRIM." class="blue">'+br.gov.icmbio.Util.pesoEfeito( item.nuPeso ) +'</span>':'')
             ) }
         </td>
         %{-- <td>${item.txFichaAmeaca}</td> --}%
         <td>
            <a data-action="ameaca.openModalSelEfeitoPrim"
               data-sq-ficha="${item.ficha.id}"
               data-sq-ameaca="${item.criterioAmeacaIucn.id}"
               data-sq-ficha-ameaca="${item.id}"
               data-de-rotulo="Ameaça - ${item.criterioAmeacaIucn.descricaoCompleta}"
               data-de-path="${item.trilhaPath}"
               data-update-grid="divGridAmeaca"
               class="fld btn btn-default btn-xs" title="Adicionar/Remover Efeitos PRIM."><span class="fa fa-arrows-alt"></span></a>
            <a data-action="ameaca.openModalGraficoPrim"
               data-sq-ficha="${item.ficha.id}"
               data-sq-ameaca="${item.criterioAmeacaIucn.id}"
               data-sq-ficha-ameaca="${item.id}"
               data-de-rotulo="Ameaça - ${item.criterioAmeacaIucn.descricaoCompleta}"
               data-de-path="${item.trilhaPath}"
               class="fld btn btn-default btn-xs green" title="Visualizar gráfico PRIM."><span class="fa fa-th-list"></span></a>

          <g:each var="efeito" in="${ item.treePesos }">
                    <br>
                    <span class="nowrap">
                    ${ raw( efeito.descricao + ( efeito.peso ?  ' - <span title="Peso do efeito no PRIM." class="blue">' + efeito.peso + '</span>' : '' ) ) }
                    </span>
                </g:each>
          </td>
          <td>
            <a data-action="selRegiao.openModalSelRegiao"
             data-no-contexto="AMEACA_REGIAO"
             data-sq-registro="${item.id}"
             data-de-rotulo="Ameaça - ${item.criterioAmeacaIucn.descricaoCompleta}"
             data-update-grid="divGridAmeaca" class="fld btn btn-default btn-xs btn-grid-subform"
             title="Incluir/Remover Região"><span class="glyphicon glyphicon-map-marker orange"></span></a>

              ${ raw(item.regioesHtml) }
         </td>

         <td>
            <a data-action="selGeo.openModalSelGeo"
               data-no-contexto="AMEACA_GEO"
               data-sq-registro="${item.id}"
               data-de-rotulo="Ameaça - ${item.criterioAmeacaIucn.descricaoCompleta}"
               data-update-grid="divGridAmeaca" class="fld btn btn-default btn-xs btn-grid-subform" title="Incluir/Remover Cordenada Geográfica"><span class="glyphicon glyphicon-map-marker blue"></span></a>
            ${raw(item.geoHtml)}
         </td>

         <td>
             <a data-action="ficha.openModalSelRefBibFicha"
                data-no-tabela="ficha_ameaca"
                data-no-coluna="sq_ficha_ameaca"
                data-sq-registro="${item.id}"
                data-de-rotulo="Ameaça - ${item.criterioAmeacaIucn.descricaoCompleta}"
                data-com-pessoal="true"
                data-update-grid="divGridAmeaca"
                class="fld btn btn-default btn-xs btn-grid-subform" title="Adicionar/Remover Refeferência bibliográfica."><span class="fa fa-book"></span></a>
                ${raw(item.refBibHtml)}
         </td>
         <g:if test="${ ! session.sicae.user.isCO() }">
              <td class="td-actions">
                 %{--
                <a data-action="ameaca.editAmeaca"  data-id="${item.id}" class="fld btn btn-default btn-xs btn-update" title="Alterar">
                   <span class="glyphicon glyphicon-pencil"></span>
                </a> --}%
                <a data-action="ameaca.deleteAmeaca" data-descricao="${item.criterioAmeacaIucn.descricaoCompleta}" data-id="${item.id}" class="fld btn btn-default btn-xs btn-delete" title="Excluir">
                   <span class="glyphicon glyphicon-remove"></span>
                </a>
             </td>
         </g:if>
      </tr>
   </g:each>
   </tbody>
</table>
