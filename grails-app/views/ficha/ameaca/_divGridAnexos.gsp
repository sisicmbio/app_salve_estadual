<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>Imagem</th>
        <th>Data</th>
        <th>Legenda</th>
        <th>Arquivo</th>
        %{-- <th>Principal?</th> --}%
        <th>Ação</th>
        %{-- <th></th> --}%
    </tr>
    </thead>
    <tbody>
    <g:each var="item" in="${listFichaAnexo}">
        <tr>
            <td class="text-center" style="width:160px;">
                <g:if test="${item.deTipoConteudo=='image/tiff'}">
                    &nbsp;
                </g:if>

                %{--IMAGENS E SHAPEFILES--}%
                <g:elseif test="${item.noArquivo.indexOf('.zip') > -1 }">
                    <img src="/salve-estadual/assets/shapefile32x37.png" data-id="${item.id}" data-file-name="${item.noArquivo}" alt="${item.noArquivo}"
                         class="img-rounded img-preview-grid"/>
                </g:elseif>
                <g:else>
                    <img src="/salve-estadual/ficha/getAnexo/${item.id}/thumb" data-id="${item.id}" data-file-name="${item.noArquivo}" alt="${item.noArquivo}" class="img-rounded img-preview-grid"/>
                </g:else>
            </td>
            <td class="text-center" style="width:100px;">${item.dtAnexo.format('dd/MM/yyyy')}</td>
            <td style="width:auto">
                <apan class="inline-edit-content">${item.deLegenda}</apan>
                <i class="ml5 fa fa-edit blue inline-edit" title="Alterar"
                   data-id="${item.id}"
                   data-contexto="ficha-anexo"></i>
            </td>
            <td style="width:200px;">${item.noArquivo}</td>
            %{-- <td style="width:80px;" class="text-center">${item.inPrincipal=='S'?'Sim':'Não'}</td> --}%
            <td style="width:30px;" class="td-actions">
                <a class="btn btn-default btn-xs btn-download" target="_blank" title="Baixar Arquivo!" href="/salve-estadual/ficha/getAnexo/${item.id}">
                    <span class="glyphicon glyphicon-download"></span>
                </a>

                <g:if test="${ ! session.sicae.user.isCO() }">
                <a data-action="ficha.deleteAnexo" data-descricao="${item.deLegenda}" data-update-grid="divGridAmeacaAnexo" data-id="${item.id}" class="fld btn btn-default btn-xs btn-delete" title="Excluir">
                    <span class="glyphicon glyphicon-remove"></span>
                </a>
                </g:if>
            </td>
            %{-- <td style="width:*"></td> --}%
        </tr>
    </g:each>
    </tbody>
</table>
