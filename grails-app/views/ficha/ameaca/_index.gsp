<%-- inicio form ameaca --%>
<form id="frmAmeaca" class="form-inline" role="form">
  <input type="hidden" name="sqFichaAmeaca" value="">
  <div class="fld form-group">
    <label class="control-label">Vetores de ameaça</label>
      <a data-action="ameaca.openModalSelAmeaca"
         class="fld btn btn-default btn-xs btn-grid-subform"
          data-params="sqFicha,modalName:selecionarAmeaca,contexto:Ameaças"
         title="Selecionar/Excluir Vetorres de ameaças">
          <span class="glyphicon glyphicon-plus"></span>
      </a>

  </div>

  %{-- foi suregido pelo carlos para ocultar este campo
  <br class="fld" />
  <div class="fld form-group w100p">
    <label for="" class="control-label">Observação Sobre a Ameaça</label>
    <br>
    <textarea name="txFichaAmeaca" rows="7" class="fld fldTextarea wysiwyg w100p"></textarea>
  </div>
  --}%

  %{--<div class="fld panel-footer">--}%
    %{--<button id="btnSaveFrmAmeaca" data-action="ameaca.saveFrmAmeaca" data-params="sqFicha" class="fld btn btn-success">Adicionar Ameaça</button>--}%
    %{--<button id="btnResetFrmAmeaca" data-action="ameaca.resetFrmAmeaca" class="fld btn btn-info hide">Limpar Formulário</button>--}%
  %{--</div>--}%
</form>
<%-- fim form frmAmeaca --%>
<div id="divGridAmeaca" class="mt10">
  %{-- <g:render template="/ficha/ameaca/divGridAmeaca"></g:render> --}%
</div>


%{-- inicio form texto ameaca da ficha --}%
<form id="frmAmeacaFicha" class="form-inline" role="form">
  <div class="form-group w100p">
   <label for="dsAmeaca" class="control-label">Observações Gerais Sobre as Ameaças
      <i class="fa fa-book ml10"
      data-action="ficha.openModalSelRefBibFicha"
      data-de-rotulo="Observações Gerais Sobre as Ameaças"
      data-no-coluna="dsAmeaca"
      data-no-tabela="ficha" data-sq-registro="${ficha.id}" style="color: green;" title="Informar Referência Bibliográfica"></i>
    </label>
    <g:if test="${ ! session.sicae.user.isCO() }">

      <div class="checkbox ml20">
          <label title="Clique aqui para preencher o campo com o texto">

              <span>Preencher com:</span>&nbsp;&nbsp;<input type="checkbox"
                    data-text="Não foram identificadas ameaças que coloquem o táxon em risco de extinção em um futuro próximo."
                    data-action="app.setDefaultText"
                    data-target="dsAmeaca"
                  ${ ( br.gov.icmbio.Util.html2str(ficha?.dsAmeaca) =~ /(?i)Não foram identificadas ameaças que coloquem o táxon em risco de extinção em um futuro próximo/ ) ? 'checked' : '' }
                     class="fld checkbox-lg">&nbsp;&nbsp;"N&atilde;o foram identificadas amea&ccedil;as que coloquem o t&aacute;xon em risco de extin&ccedil;&atilde;o em um futuro pr&oacute;ximo."

              %{--
              Este campo foi retirado por decisão da equipe salve 05/09/2018
              &nbsp;&nbsp;&nbsp;<input type="checkbox"--}%
                                       %{--data-action="app.setDefaultText"--}%
                                       %{--data-target="dsAmeaca"--}%
                                       %{--data-text="Não foram identificadas ameaças que coloquem o táxon em risco de extinção em um futuro próximo."--}%
                  %{--${ ficha?.dsAmeaca?.toLowerCase()?.indexOf('N&atilde;o foram encontradas amea&ccedil;as significativas.') > -1 ? 'checked' : '' }--}%
                     %{--class="checkbox-lg">&nbsp;&nbsp;"N&atilde;o foram encontradas amea&ccedil;as significativas."--}%
          </label>
      </div>
    </g:if>
    <br>
    <textarea name="dsAmeaca" id="dsAmeaca" rows="18" class="fld fldTextarea wysiwyg w100p">${ficha.dsAmeaca}</textarea>
  </div>
  <div class="fld panel-footer">
    <button id="btnSaveFrmAmeacaFicha" data-action="ameaca.saveFrmAmeacaFicha" data-params="sqFicha" class="fld btn btn-success">Gravar Observações Gerais</button>
  </div>
</form>
%{-- fim form texto ameaca da ficha --}%


%{-- imagens de mapas/shp --}%
<fieldset>
    <legend>
        Mapas e Shapefiles
        <a data-action="ficha.openModalSelAnexo"
           data-contexto="MAPA_AMEACA"
           data-de-rotulo="Mapas de Ameaça"
           data-show-principal="false"
           data-update-grid="divGridAmeacaAnexo"
           class="fld btn btn-default btn-xs btn-grid-subform"
           title="Adicionar Imagem/Shapefile de Ameaças"><span class="glyphicon glyphicon-plus"></span>
        </a>
    </legend>
</fieldset>
%{-- gride  --}%
<div class="" id="divGridAmeacaAnexo" data-contexto="MAPA_AMEACA"></div>
