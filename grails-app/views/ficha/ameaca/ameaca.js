//# sourceURL=ameaca.js
//
;
var gWindowHeight = Math.max(500,window.innerHeight-120);
var ameaca = {

    corrigirAlturaTable : function(contexto) {
        // ajustar a altura máxima
        window.setTimeout( function() {
            var $panel = $(contexto);
            if( $panel.size() == 1 ) {
                var $body = $(contexto+"_body");
                $body.scrollTop(999999);
                var scrollTop = $body.scrollTop();
                $body.scrollTop(0);
                var $divTreeChecks = $panel.find('div#div-treeChecks');
                var $divTreeviewPath = $panel.find('div#divTreeviewPath');
                var maxHeight = $panel.height() - 30 - ( $divTreeviewPath.offset().top + $divTreeviewPath.height() + 1.5 + scrollTop);
                $divTreeChecks.css({'height': Math.max(400, maxHeight ) + 'px', 'overflow': 'hidden', 'overflow-Y': 'auto'});
            }
        },1000)
    },
    openModalGraficoPrim:function(params)
    {
        // carregar js e css necessários para criação do gráfico
        if( typeof( $.fn.orgchart ) == 'undefined') {
            loadScript(['https://cdnjs.cloudflare.com/ajax/libs/orgchart/2.1.3/css/jquery.orgchart.min.css'
                            ,'https://cdnjs.cloudflare.com/ajax/libs/orgchart/2.1.3/js/jquery.orgchart.min.js'],function(){
                ameaca.openModalGraficoPrim(params);
            });
            return;
        }
        var data = {};
        data.reload = true;
        data.modalId = 'win-'+makeId();
        data = app.params2data(params, data);
        // inicio funções upload de planilhas
        $.jsPanel({
            // http://jspanel.de/api/
            id: data.modalId,
            //paneltype: 'modal',
            setStatus: 'maximized',
            container: 'body',
            draggable: {
                handle: 'div.jsPanel-titlebar, div.jsPanel-ftr',
                opacity: 0.8
            },
            dragit: {
                axis: false,
                containment: 'parent',
                handles: '.jsPanel-titlebar, .jsPanel-ftr.active', // do not set .jsPanel-titlebar to .jsPanel-hdr
                opacity: 0.8,
                start: false,
                drag: false,
                stop: false,
                disableui: true
            },
            resizable: {
                handles: 'n, e, s, w, ne, se, sw, nw',
                autoHide: false,
                minWidth: 500,
                minHeight: 500
            },
            resizeit: {
                containment: 'parent',
                handles: 'n, e, s, w, ne, se, sw, nw',
                minWidth: 500,
                minHeight: 500,
                maxWidth: 10000,
                maxHeight: 10000,
                start: false,
                resize: false,
                stop: false,
                disableui: true
            },

            //show:    'animated fadeInDownBig',
            show: 'animated slideInUp',
            //theme    : "green filledlight",
            theme: "green",
            headerTitle: 'Gráfico Efeito PRIM',
            contentSize: {width: 800, height: gWindowHeight},
            content: '<div id="'+data.modalId+'-content" style="outline:none;border:none;display:block;min-height:100%;overflow:auto;" class="div-grafico-prim"><span id="spinner">Carregando...' + window.grails.spinner + '</span></div>',
            callback: function (panel) {
                var $divContent = $("#"+data.modalId+'-content');
                $divContent.html('');
                $divContent.orgchart({
                    data            : app.url+'fichaAmeaca/getGraficoEfeitoPrim/'+data.sqFichaAmeaca,
                    direction       : 'l2r',
                    parentNodeSymbol: '',
                    //nodeContent     : "name",
                    nodeTemplate    : function(data){
                        if( data.emUso ) {
                            return '<div class="title" style="background-color: red !important;">' + data.title + '</div><div class="content cursor-pointer" title="' + data.name + '">' + data.name + '</div>';
                        }
                        return '<div class="title">' + data.title + '</div><div class="content cursor-pointer" title="' + data.name + '">' + data.name + '</div>';

                    }
                });
            }
        });
    },
    openModalSelEfeitoPrim:function( params )
    {
        params.modalName='selecionarEfeitoPrim';
        //alert('em desenvolvomento.')
        var data = {};
        data.reload = true;
        data = app.params2data(params, data);
        // inicio funções upload de planilhas
        $.jsPanel({
            // http://jspanel.de/api/
            id         : "modalSelEfeitoPrim",
            paneltype: 'modal',
            container: 'body',
            draggable:  {
                handle:  'div.jsPanel-titlebar, div.jsPanel-ftr',
                opacity: 0.8
            },
            dragit:             {
                axis:        false,
                containment: 'parent',
                handles:     '.jsPanel-titlebar, .jsPanel-ftr.active', // do not set .jsPanel-titlebar to .jsPanel-hdr
                opacity:     0.8,
                start:       false,
                drag:        false,
                stop:        false,
                disableui:   true
            },
            resizable: {
                handles:   'n, e, s, w, ne, se, sw, nw',
                autoHide:  false,
                minWidth:  500,
                minHeight: 500
            },
            resizeit:           {
                containment: 'parent',
                handles:     'n, e, s, w, ne, se, sw, nw',
                minWidth:    500,
                minHeight:   500,
                maxWidth:    10000,
                maxHeight:   10000,
                start:       false,
                resize:      false,
                stop:        false,
                disableui:   true
            },

            //show:    'animated fadeInDownBig',
            show:      'animated slideInUp',
            //theme    : "green filledlight",
            theme      : "yellow",
            headerTitle: 'Seleção de Efeitos',
            contentSize: {width: 800, height: gWindowHeight},
            content    : '<div id="div_content_ficha_selecionar_efeito_prim" style="outline:none;border:none;display:block;min-height:100%;"><span id="spinner">Carregando...' + window.grails.spinner + '</span></div>',
            callback   : function (panel)
            {
                // ler o formulário
                panel.find('.jsPanel-content').prop('id', panel.attr('id')+'_body' );
                app.loadModule(app.url + 'ficha/getModal', params, panel.attr('id')+'_body', function (res)
                {
                    // ajustar a altura da treeview para que o botão salvar fique visivel
                    $("#modalSelEfeitoPrim #treeChecks").css('max-height',gWindowHeight-120);

                    $("#modalSelEfeitoPrim #treeChecks").fancytree({
                        quicksearch: true,
                        selectMode : 2,
                        strings     : { loading: "Carregando...", loadError: "Erro de leitura!", moreData: "Mais...", noData: "Nenhum registro."},
                        focusOnSelect:false,
                        extensions: ["wide","filter","table"],
                        filter: {
                            autoApply: true,   // Re-apply last filter if lazy data is loaded
                            autoExpand: true, // Expand all branches that contain matches while filtered
                            counter: true,     // Show a badge with number of matching child nodes near parent icons
                            fuzzy: false,      // Match single characters in order, e.g. 'fb' will match 'FooBar'
                            hideExpandedCounter: false,  // Hide counter badge if parent is expanded
                            hideExpanders: false, // Hide expanders if all child nodes are hidden by filter
                            highlight: true,   // Highlight matches by wrapping inside <mark> tags
                            leavesOnly: false, // Match end nodes only
                            nodata: true,      // Display a 'no data' status node if result is empty
                            mode: "hide"       // dimm = Grayout unmatched nodes (pass "hide" to remove unmatched node instead)
                        },
                        source: {
                            data:data,
                            url: app.url+"fichaAmeaca/getTreeEfeitoPrim"
                        },
                        checkbox: true,
                        autoScroll:true,
                        click: function(event, data) {
                            // https://github.com/mar10/fancytree/wiki/TutorialEvents
                            var node = data.node;
                            var tt = $.ui.fancytree.getEventTargetType(event.originalEvent);
                            if( tt =='title' )
                            {
                                data.node.toggleExpanded();
                            }
                        },
                        select: function(event, data) {
                            var node = data.node;
                            if ( node ) {
                                if (node.isSelected()) {
                                    node.setExpanded(true);
                                    $(node.tr).find('select').show();
                                } else {
                                    $(node.tr).find('select').hide();
                                }
                            };
                        },
                        renderColumns: function(event, data) {
                            var node = data.node;
                            var $select = $(node.tr).find('select');
// passo 1

                            $($select[0]).val( node.data.referenciaTemporal );
                            $($select[1]).val( node.data.peso );
                            if( node.isSelected() )
                            {
                                $select.show();
                            }
                            //$tdList.eq(0).text(node.getIndexHier());
                            // coluna 2 não mexer
                            // (Index #2 is rendered by fancytree)

                            // preencher a coluna 4
                            //$tdList.eq(1).find("input").val(node.data.ordem);
                            //$tdList.eq(2).find("input").val(node.data.codigo);

                            // Static markup (more efficiently defined as html row template):
                            // $tdList.eq(3).html("<input type='input' value='" + "" + "'>");
                            // ...

                        },
                    });

                    // evitar que o botão gravar desapareca
                    ameaca.corrigirAlturaTable('#modalSelEfeitoPrim');

                    // ativar função de filtragem // http://wwwendt.de/tech/fancytree/demo/sample-ext-filter.html#
                    $("#frmModalSelEfeitoPrim #fldFiltrarTreeView").keyup(function(e){
                        var n,
                            tree = $.ui.fancytree.getTree(),
                            opts = {},
                            filterFunc = tree.filterNodes,
                            match = $(this).val();
                        if(e && e.which === $.ui.keyCode.ESCAPE || $.trim(match) === "")
                        {
                            $("#frmModalSelEfeitoPrim #btnTreeViewReset").click();
                            return;
                        }
                        if( match )
                        {
                            $("#frmModalSelEfeitoPrim #btnTreeViewReset").removeClass('hidden');
                        }
                        else
                        {
                            $("#frmModalSelEfeitoPrim #btnTreeViewReset").addClass('hidden');
                        }
                        // Pass a string to perform case insensitive matching
                        n = filterFunc.call(tree, match, opts);
                        //$("span#matches").text("(" + n + " matches)");
                    }).focus();

                    $("#frmModalSelEfeitoPrim #btnTreeViewReset").click(function(){
                        $("#modalSelEfeitoPrim #treeChecks").fancytree("getTree").clearFilter();
                        $("#frmModalSelEfeitoPrim fldFiltrarTreeView").val('').focus();
                    });

                    $("#frmModalSelEfeitoPrim #btnTreeViewNone").click(function(){
                        var node = $("#modalSelEfeitoPrim #treeChecks").fancytree("getTree").getActiveNode();
                        if( node )
                        {
                            node.setSelected(false);
                            node.visit(function(node)
                            {
                                if ( ! $(node.span).hasClass('fancytree-hide') )
                                {
                                    node.setSelected(false);
                                }
                            });
                        }


                        /*$("#modalSelEfeitoPrim #treeChecks").fancytree("getTree").visit(function(node){
                            if( ! $(node.span).hasClass('fancytree-hide') )
                            {
                                node.setSelected(false);
                            }
                        });
                        */
                        return false;
                    });
                    $("#frmModalSelEfeitoPrim #btnTreeViewAll").click(function(){

                        var node = $("#modalSelEfeitoPrim #treeChecks").fancytree("getTree").getActiveNode();
                        if( node )
                        {
                            node.setSelected(true);
                            node.visit(function(node)
                            {
                                if ( ! $(node.span).hasClass('fancytree-hide') )
                                {
                                    node.setSelected(true);
                                }
                            });
                        }
                        /*
                        $("#modalSelEfeitoPrim #treeChecks").fancytree("getTree").visit(function(node){
                            if( ! $(node.span).hasClass('fancytree-hide') )
                            {
                                node.setSelected(true);
                            }
                        });
                        */
                        return false;
                    });
                    $("#frmModalSelEfeitoPrim #btnTreeViewCollapseAll").click(function(){
                        $("#modalSelEfeitoPrim #treeChecks").fancytree("getRootNode").visit(function(node){
                            if( ! $(node.span).hasClass('fancytree-hide') )
                            {
                                node.setExpanded(false);
                            }
                        });
                    });
                    $("#frmModalSelEfeitoPrim #btnTreeViewExpandAll").click(function(){

                        $("#modalSelEfeitoPrim #treeChecks").fancytree("getRootNode").visit(function(node){
                            if( ! $(node.span).hasClass('fancytree-hide') )
                            {
                                node.setExpanded(true);
                            }
                        });
                        return false;
                    });
                }, '');
            },
        });
    },

    saveTreeEfeitoPrim:function( params )
    {
        var data = { ids : [] };
        $.each( $("#modalSelEfeitoPrim #treeChecks").fancytree('getTree').getSelectedNodes(),function(){
            var $select = $(this.tr).find('select');
            data.ids.push({id: this.data.id, peso:  ( $select.size() == 1 ? $select.val() : this.data.peso)  } );
        });
        app.params2data(params,data);
        data.ids = JSON.stringify(data.ids);
        app.blockUI('Gravando...',null,5000);
        app.ajax(app.url+'fichaAmeaca/saveTreeEfeitoPrim', data,
            function(res)
            {
                app.unblockUI();
                if (! res.status)
                {
                    ameaca.updateGridAmeaca();
                }
            }, null, 'json'
        );
    },
    //------------------------------------------------------------------------------------------------
    openModalSelAmeaca:function(params ) {
        var data = {};
        data.reload = true;
        //data.modalName = 'selecionarAmeaca';
        data = app.params2data(params, data);
            //return;

        // inicio funções upload de planilhas
        $.jsPanel({
            // http://jspanel.de/api/
            id         : "modalSelAmeaca",

            //headerControls: {controls: "closeonly"},
            //template:    jsPanel.tplContentOnly,
            //position:    {my: "center-top", at: "center-top", offsetY: 15},
            //position:    'left-top -5 5 DOWN',
            paneltype:   'modal',
            container:'body',
            draggable:       {
                handle:  'div.jsPanel-titlebar, div.jsPanel-ftr',
                opacity: 0.8
            },
            dragit:             {
                axis:        false,
                containment: 'parent',
                handles:     '.jsPanel-titlebar, .jsPanel-ftr.active', // do not set .jsPanel-titlebar to .jsPanel-hdr
                opacity:     0.8,
                start:       false,
                drag:        false,
                stop:        false,
                disableui:   true
            },
            resizable: {
                handles:   'n, e, s, w, ne, se, sw, nw',
                autoHide:  false,
                minWidth:  500,
                minHeight: 500
            },
            resizeit:           {
                containment: 'parent',
                handles:     'n, e, s, w, ne, se, sw, nw',
                minWidth:    500,
                minHeight:   500,
                maxWidth:    10000,
                maxHeight:   10000,
                start:       false,
                resize:      false,
                stop:        false,
                disableui:   true
            },

            //show:    'animated fadeInDownBig',
            show:      'animated slideInUp',
            //theme    : "green filledlight",
            theme      : "green",
            headerTitle: 'Seleção de Vetor de Ameaça',
            contentSize: {width: 800, height: gWindowHeight},
            content    : '<div id="div_content_ficha_selecionar_ameaca" style="outline:none;border:none;display:block;min-height:100%;"><span id="spinner">Carregando...' + window.grails.spinner + '</span></div>',
            callback   : function (panel)
            {
                // ler o formulário
                panel.find('.jsPanel-content').prop('id',panel.attr('id')+'_body')
                app.loadModule(app.url + 'ficha/getModal', params, panel.attr('id')+'_body', function (res)
                {

                    // ajustar a altura da treeview para que o botão salvar fique visivel
                    $("#modalSelAmeaca #treeChecks").css('max-height',gWindowHeight-140);

                    $("#modalSelAmeaca #treeChecks").fancytree({
                        quicksearch: true,
                        selectMode : 2,
                        strings     : { loading: "Carregando...", loadError: "Erro de leitura!", moreData: "Mais...", noData: "Nenhum registro."},
                        focusOnSelect:false,
                        extensions: ["wide","filter","table"],
                        filter: {
                            autoApply: true,   // Re-apply last filter if lazy data is loaded
                            autoExpand: true, // Expand all branches that contain matches while filtered
                            counter: true,     // Show a badge with number of matching child nodes near parent icons
                            fuzzy: false,      // Match single characters in order, e.g. 'fb' will match 'FooBar'
                            hideExpandedCounter: false,  // Hide counter badge if parent is expanded
                            hideExpanders: false, // Hide expanders if all child nodes are hidden by filter
                            highlight: true,   // Highlight matches by wrapping inside <mark> tags
                            leavesOnly: false, // Match end nodes only
                            nodata: true,      // Display a 'no data' status node if result is empty
                            mode: "hide"       // dimm = Grayout unmatched nodes (pass "hide" to remove unmatched node instead)
                        },
                        source: {
                            data:data,
                            url: app.url+"fichaAmeaca/getTreeAmeacas"
                        },
                        checkbox: true,
                        autoScroll:true,
                        /** expandir ao clicar */
                        click: function(event, data) {
                            // https://github.com/mar10/fancytree/wiki/TutorialEvents
                            var node = data.node;
                            var tt = $.ui.fancytree.getEventTargetType(event.originalEvent);
                            if( tt =='title' )
                            {
                                data.node.toggleExpanded();
                            }
                        },
                        /** desmarcar nivel superior ao selecionar um dos niveis inferiores*/
                        select: function(event, data) {
                            var node = data.node;
                            if ( node ) {
                                if (node.isSelected()) {
                                    node.setExpanded(true);
                                    $(node.tr).find('select').show();
                                    var parent=node.parent;
                                    while( parent ) {
                                        parent.setSelected(false);
                                        parent.unselectable=true;
                                        parent.hideCheckbox=true;
                                        parent.render(true);
                                        parent = parent.parent;
                                    }
                                } else {
                                    var parent=node.parent;
                                    var selectedChildren = parent.getChildren().filter( function(item) {
                                       return item.isSelected();
                                    });
                                    if( selectedChildren.length == 0 ) {
                                        while (parent) {
                                            parent.unselectable = false;
                                            parent.hideCheckbox = false;
                                            parent.render(true);
                                            parent = parent.parent;
                                        }
                                    }
                                    $(node.tr).find('select').hide();
                                }
                            };
                        },
                        renderColumns: function(event, data) {
                            var node = data.node
                            var $select = $(node.tr).find('select');

                            // referencia temporal
                            $($select[0]).val( node.data.referenciaTemporal );
                            // peso
                            $( $select[1]).val( node.data.peso );
                            if( node.isSelected() )
                            {
                                $select.show();
                            }
                            //$tdList.eq(0).text(node.getIndexHier());
                            // coluna 2 não mexer
                            // (Index #2 is rendered by fancytree)

                            // preencher a coluna 4
                            //$tdList.eq(1).find("input").val(node.data.ordem);
                            //$tdList.eq(2).find("input").val(node.data.codigo);

                            // Static markup (more efficiently defined as html row template):
                            // $tdList.eq(3).html("<input type='input' value='" + "" + "'>");
                            // ...

                        },
                    });
                    // evitar que o botão gravar desapareca
                    ameaca.corrigirAlturaTable("#modalSelAmeaca");

                    // ativar função de filtragem // http://wwwendt.de/tech/fancytree/demo/sample-ext-filter.html#
                    $("#frmModalSelAmeaca #fldFiltrarTreeView").keyup(function(e){
                        var n,
                        tree = $.ui.fancytree.getTree(),
                        opts = {},
                        filterFunc = tree.filterNodes,
                        match = $(this).val();
                        if(e && e.which === $.ui.keyCode.ESCAPE || $.trim(match) === "")
                        {
                            $("#frmModalSelAmeaca #btnTreeViewReset").click();
                            return;
                        }
                        if( match )
                        {
                            $("#frmModalSelAmeaca #btnTreeViewReset").removeClass('hidden');
                        }
                        else
                        {
                            $("#frmModalSelAmeaca #btnTreeViewReset").addClass('hidden');
                        }
                        // Pass a string to perform case insensitive matching
                        n = filterFunc.call(tree, match, opts);
                        //$("span#matches").text("(" + n + " matches)");
                    }).focus();

                    $("#frmModalSelAmeaca #btnTreeViewReset").click(function(){
                        $("#modalSelAmeaca #treeChecks").fancytree("getTree").clearFilter();
                        $("#frmModalSelAmeaca fldFiltrarTreeView").val('').focus();
                    });

                    $("#frmModalSelAmeaca #btnTreeViewNone").click(function(){
                        var node = $("#modalSelAmeaca #treeChecks").fancytree("getTree").getActiveNode();
                        if( node )
                        {
                            node.setSelected(false);
                            node.visit(function(node)
                            {
                                if ( ! $(node.span).hasClass('fancytree-hide') )
                                {
                                    node.setSelected(false);
                                }
                            });
                        }


                        /*$("#modalSelAmeaca #treeChecks").fancytree("getTree").visit(function(node){
                            if( ! $(node.span).hasClass('fancytree-hide') )
                            {
                                node.setSelected(false);
                            }
                        });
                        */
                        return false;
                    });
                    $("#frmModalSelAmeaca #btnTreeViewAll").click(function(){

                        var node = $("#modalSelAmeaca #treeChecks").fancytree("getTree").getActiveNode();
                        if( node )
                        {
                            node.setSelected(true);
                            node.visit(function(node)
                            {
                                if ( ! $(node.span).hasClass('fancytree-hide') )
                                {
                                    node.setSelected(true);
                                }
                            });
                        }
                        /*
                        $("#modalSelAmeaca #treeChecks").fancytree("getTree").visit(function(node){
                            if( ! $(node.span).hasClass('fancytree-hide') )
                            {
                                node.setSelected(true);
                            }
                        });
                        */
                        return false;
                    });
                    $("#frmModalSelAmeaca #btnTreeViewCollapseAll").click(function(){
                        $("#modalSelAmeaca #treeChecks").fancytree("getRootNode").visit(function(node){
                            if( ! $(node.span).hasClass('fancytree-hide') )
                            {
                                node.setExpanded(false);
                            }
                        });
                    });
                    $("#frmModalSelAmeaca #btnTreeViewExpandAll").click(function(){

                        $("#modalSelAmeaca #treeChecks").fancytree("getRootNode").visit(function(node){
                            if( ! $(node.span).hasClass('fancytree-hide') )
                            {
                                node.setExpanded(true);
                            }
                        });
                        return false;
                    });
                }, '');
            },
        });
    },
    saveTreeAmeaca:function( params ) {
        var data = {sqFicha:$("#sqFicha").val(),ids:[] };
        $.each( $("#modalSelAmeaca #treeChecks").fancytree('getTree').getSelectedNodes(),function(){
            var $select = $(this.tr).find('select');
            data.ids.push({id: this.data.id
                , referenciaTemporal:  ( $select.size() > 0 ? $($select[0]).val() : this.data.referenciaTemporal)
                , peso:  ( $select.size()> 0 ? $($select[1]).val() : this.data.peso)
            } );
        });
        data.ids = JSON.stringify(data.ids);
        app.blockUI('Gravando...',null,5000);
        app.ajax(app.url+'fichaAmeaca/saveTreeAmeaca', data,
            function(res)
            {
                app.unblockUI();
                if (! res.status)
                {
                    ameaca.updateGridAmeaca();
                }
                if( res.data.limparDescricao )
                {
                    tinymce.get('dsAmeaca').setContent('');
                    $("input[type=checkbox][data-target=dsAmeaca]").prop('checked',false);
                    app.alertInfo('Texto da descrição foi removido porque já existe ameaça cadastrada!');
                }

            }, null, 'json'
        );
    },
	tabFichaAmeacaInit: function() {
		ameaca.updateGridAmeaca();
        ameaca.updateGridAnexos();
	},
	saveFrmAmeaca: function(params) {
		if (!$('#frmAmeaca').valid()) {
			return false;
		}
		var data = $("#frmAmeaca").serializeArray();
		if (!data) {
			return;
		}
		data = app.params2data(params, data);
		if (!data.sqFicha) {
			app.alertError('ID da ficha não encontrado!');
			return;
		}
		app.ajax(app.url+'fichaAmeaca/saveFrmAmeaca', data,
			function(res) {
				ameaca.resetFrmAmeaca();
				ameaca.updateGridAmeaca();
			}, null, 'json'
		);
	},
	resetFrmAmeaca: function() {
		app.reset('frmAmeaca');
		$("#sqCriterioAmeacaIucn").focus();
		$("#btnResetFrmAmeaca").addClass('hide');
		$("#btnSaveFrmAmeaca").html($("#btnSaveFrmAmeaca").data('value'));
		app.unselectGridRow('divGridAmeaca');
	},
	updateGridAmeaca: function() {
		ficha.getGrid('ameaca');
	},
	editAmeaca: function(params, btn) {
		app.ajax(app.url+'fichaAmeaca/editAmeaca', params,
			function(res) {
				$("#btnResetFrmAmeaca").removeClass('hide');
				$("#btnSaveFrmAmeaca")
					.data('value', $("#btnSaveFrmAmeaca").html());
				$("#btnSaveFrmAmeaca").html("Gravar Alteração");
				app.setFormFields(res, 'frmAmeaca');
				app.selectGridRow(btn);
			});
	},
	deleteAmeaca: function(params,btn) {
		app.selectGridRow(btn);
		app.confirm('<br>Confirma exclusão da ameaca '+params.descricao+'?',
			function() {
				app.ajax(app.url+'fichaAmeaca/deleteAmeaca',
					params,
					function(res) {
						ameaca.updateGridAmeaca();
					});
			},
			function() {
				app.unselectGridRow(btn);
			}, params, 'Confirmação', 'warning');
	},
	saveFrmAmeacaFicha:function( params ){
		if (!$('#frmAmeacaFicha').valid()) {
			return false;
		}
		var data = $("#frmAmeacaFicha").serializeArray();
		if (!data) {
			return;
		}
		data = app.params2data(params, data);
		if (!data.sqFicha) {
			app.alertError('ID da ficha não encontrado!');
			return;
		}
		// se tiver ameaca cadastrada, limpar texto padrão
        if( $("#divGridAmeaca table tbody tr").length > 0 )
        {
            var textoAutomaticoAtual = tinymce.get('dsAmeaca').getContent({format : 'text'}).trim();
            if( /Não são conhecidas ameaças que indiquem risco de extinção em um futuro próximo/.test( textoAutomaticoAtual ) ) {
                tinymce.get('dsAmeaca').setContent('');
                data.dsAmeaca = '';
                $("input[type=checkbox][data-target=dsAmeaca]").prop('checked',false);
                app.alertInfo('Texto da observação das ameaças foi removido porque já existe ameaça cadastrada!');
            }
        }
		app.ajax(app.url+'fichaAmeaca/saveFrmAmeacaFicha', data,
			function(res) {
				app.focus('dsAmeaca');
                $("#liTabAmeaca").removeClass('changed')
            }, null, 'json'
		);

	},
    updateGridAnexos:function( params ){
        ficha.getGridAnexos('divGridAmeacaAnexo');
    },
}
