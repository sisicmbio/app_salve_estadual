//# sourceURL=/views/ficha/ficha.js
;
var modalSelRefBib;
var ficha = {
    filtros: {},
    carregando: false,
    semCache: false,
    scrollTopVoltarVoltar: -1,
    scrollTopBodyVoltar: -1,
    scrollTopGrideFichasVoltar: -1,
    paginationPageNumber:1,
    dataTable:null,
    init: function() {
        // limpar as instâncias do tinymce
        tinyMCE.editors = [];
        //
        ficha.sqCicloAvaliacaoChange()
    },
    doModalSelectFichaCicloAnterior:function(params){
      modal.selecionarFichaCicloAnterior( params );
    },
    frmSelecionarFichaCicloAnteriorInit:function(data){},
    saveFichaCicloAnterior:function( params ) {
        var data = {sqFicha: params.sqFicha, sqFichaCicloAnterior: params.sqFichaCicloAnterior};
        var select2Data = $("#sqFichaCicloAnterior").select2('data');
        if( select2Data ) {
            var nomeAtual = $("#divNmCientifico").text().trim();
            var nomeAntigo = select2Data[0].descricao.trim();
            app.confirm('Confirma gravação?<br><br>Nome atual:<b>'+nomeAtual+'</b><br>Nome antigo:<b>'+nomeAntigo+'</b>', function () {
                app.ajax(app.url + 'ficha/saveFichaCicloAnterior', data, function (res) {
                    if (res.status === 0) {
                        app.closeWindow('modalselecionarFichaCicloAnterior');
                        if( res.sqFichaSinonimia ) {
                            app.growl('Nome antigo adicionado ao gride.');
                            taxonomia.updateGridSinonimia();
                        }
                    }
                });
            }, null, data, 'Confirmação', 'info');
        }
    },
    alterarTaxon:function( params ) {
       modal.alterarTaxonFicha( $("#sqFicha").val() );
    },
    alterarArvoreTaxonomica:function( params ) {
        app.closeWindow('modalAlterarTaxonFicha');
        modal.alterarArvoreTaxonomica( params.sqFicha );
    },
    sqCicloAvaliacaoChange: function(params) {
        var $sqCicloAvaliacao = $("#fichaContainerBody #sqCicloAvaliacao");
        var sqCicloAvaliacao = $sqCicloAvaliacao.val();

        if (sqCicloAvaliacao) {
            // limpar gride
            $("#divGridFichasPaginationContent,#divGridFichasPaginationContentFooter").html('');
            $("#tableGridFichas tbody").html('<tr><td colspan="8"><h4>Selecinando as fichas. Aguarde...</h4></td></tr>')

            // mostrar a tabela e o rodapé quando tiver ciclo selecionado
            $("#tableGridFichas").parent().show();
            $("#divGridFichasPaginationContentFooter").show();

            // bloquear select ciclo avaliacao
            $("#frmGridFichas #sqCicloAvaliacao").prop('disabled',true);
            $("#fichaContainerBody #divFiltrosFicha,#divGridFichasPaginationContent,#divContainerBotoesFicha,#divLastChanged").removeClass('hidden');
            $("#divContainerGridFichas tbody tr").remove();
            $("#fichaLote").addClass('hidden');
            $("#btnAdd").removeClass('hidden');
            $("#fichaContainerBody #isLastCicle").val( $("#sqCicloAvaliacao option:selected").data('lastCicle') );

            // somente cadastrar ficha no primeiro ciclo se for adm
            var btnAddDisabled=false;
            if( ( $sqCicloAvaliacao.val() == '1' && isAdm ) || $sqCicloAvaliacao.val() != '1' ) {
                btnAddDisabled=false;
            } else {
                btnAddDisabled=true;
            }
            $("#btnAdd").prop('disabled',btnAddDisabled);
            window.setTimeout(function(){
                $("#btnAdd").css('cursor',(btnAddDisabled ? 'not-allowed' : 'default')  );
            },1000,btnAddDisabled);
            // ler o formulario de filtros da ficha e o gride com as fichas
            var data = {
                sqCicloAvaliacao: sqCicloAvaliacao,
                callback: 'ficha.updateGridFichas', // acao que sera executada ao aplicar um filtro
                onLoad: 'ficha.updateGridFichas', // função que será executado quando o formulario for carregado a primeira vez
                contexto: 'ficha',
                hideFilters:'pendencia',
                timeoutCallback:100,
                gridId:'GridFichas' // informar o gride para que o botão limpar filtros limpe tambem os filtros informados nas colunas do gride
            }
            // ler o formulário de filtros
            app.loadModule(baseUrl + 'ficha/getFormFiltroFicha', data, '#fichaContainerBody #divFiltrosFicha',function(){
                // adicionar evento PASTE no campo Nome cientifico dos filtros
                $('input[name=nmCientificoFiltro]').bind('paste', null,app.inputPaste);
            });
            setTimeout(function(){
                // aguardar o gride ser renderizado e a página estar carregada.
                ficha.updateLastChanged(); // atualizar div de acesso rapido das 3 ultimas fichas alteradas
            },3000);

            // quando for ciclo vigente não exibir as situações abaixo
            if( $("#loteSqFichaSituacao").data("isAdm") == 'N') {

                // TODO - alterar a identificação do primeiro ciclo aqui. Não pode ficar selectedIndex=1
                if ($sqCicloAvaliacao[0].selectedIndex == 1) { // primeiro ciclo
                    $("#loteSqFichaSituacao option[data-codigo=VALIDADA]").hide();
                    $("#loteSqFichaSituacao option[data-codigo=EM_VALIDACAO]").hide();
                    $("#loteSqFichaSituacao option[data-codigo=PUBLICADA]").hide();
                } else {
                    $("#loteSqFichaSituacao option[data-codigo=VALIDADA]").show();
                    $("#loteSqFichaSituacao option[data-codigo=EM_VALIDACAO]").show();
                    $("#loteSqFichaSituacao option[data-codigo=PUBLICADA]").show();
                }
            }

        } else {
            $("#tableGridFichas tbody tr").remove();
            $("#fichaContainerBody #divFiltrosFicha,#divLastChanged,#trGridFichasFilters,#divGridFichasPaginationContent,#divContainerBotoesFicha").addClass('hidden');
            $("#btnAdd").addClass('hidden');
            $("#tableGridFichas").floatThead('destroy');
            $("#tableGridFichas").parent().hide();
            $("#divGridFichasPaginationContentFooter").hide();
        }
        // limpar fichas selecionadas
        $("#gridFichasSelectedIds").val('');
        ficha.chkSqFichaChange();

    },
    updateLastChanged: function() {
        var sqCicloAvaliacao = $("#fichaContainerBody #sqCicloAvaliacao").val();
        if (!sqCicloAvaliacao) {
            return;
        }
        var $div = $("#fichaContainerBody #divLastChanged");
        var data = {
            sqCicloAvaliacao: sqCicloAvaliacao
        };
        $div.removeClass('hidden').html('');
        $.post(baseUrl + 'ficha/getLastChanged', data, function(res) {
            $div.html(res);
        });
    },
    updatePercentualImportacao: function() {
        //console.log('Verificar percentual ' + new Date());
        $.post(baseUrl + 'ficha/getAndamentoImportacaoPlanilhas', {
            noLog: true
        }, function(res) {
            if (res.data.length > 0) {
                $.each(res.data, function(k, v) {
                    var tdPercentual = $("#td_percentual_" + v.id);
                    $("#td_situacao_" + v.id).html('<span class="' + (v.percentual < 100 ? 'blue' : 'green') + '">' + v.situacao + '</span>');
                    tdPercentual.html(v.percentual + '%');
                    tdPercentual.css({'background-color': '#6565ff', 'color': 'white'});
                 });
                // se fechar a janela parar o cronometro
                if ($("#table_importacao_planilhas").size() > 0) {
                    window.setTimeout(function() {
                        ficha.updatePercentualImportacao();
                    }, 3000);
                }
            }
        }, null, 'JSON');
    },

    editarSubespecie: function(sqFicha) {
        event.preventDefault();
        ficha.edit({
            sqFicha: sqFicha
        });
    },

    getSubespecies: function() {
        var sqFicha = $("#sqFicha").val();
        if (!sqFicha) {
            return;
        }
        app.ajax(app.url + 'ficha/getSubespecies', {
            sqFicha: sqFicha
        }, function(res) {

            var itens = [];
            var div = $("#divSubespecies");
            div.hide();
            div.html('');
            if (res.length > 0) {
                if (res[0].coNivel == 'ESPECIE') {
                    div.append('<span><b>Espécie:&nbsp;</b></span>');
                    div.append('<a title="Clique para selecionar!" href="#" onClick="ficha.editarSubespecie(' + res[0].sqFicha + ')">' + res[0].noCientifico + '</a>');
                    res.shift(); // remover a especie do array
                }
                if (res.length > 0) {
                    div.append((div.html() != '' ? '&nbsp;&nbsp;' : '') + '<span><b>Subespécie' + (res.length > 1 ? 's' : '') + ':&nbsp;</b></span>');
                    $.each(res, function(k, item) {
                        if (item.coNivel == 'SUBESPECIE') {
                            div.append((k > 0 ? ', ' : '') + '<a title="Clique para selecionar!" href="#" onClick="ficha.editarSubespecie(' + item.sqFicha + ')">' + item.noCientifico + '</a>');
                        }
                    });
                }
                div.show('fast');
            }
        });
    },
    /**
     * Atualizar a combo com os ciclos válidos para receber as fichas copiadas
     **/
    updateSelectCicloDestinoCopia: function(param) {
        // limpar o select
        var selectCicloDestino = $("#sqCicloDestinoCopia");
        selectCicloDestino.find('option').remove();
        var flagAdicionar = true;
        var validOptions = [];
        var options = $("#sqCicloAvaliacao").find('option').clone();
        $.each(options, function(i, option) {
                if ($(this).val() == $("#sqCicloAvaliacao").val()) {
                    flagAdicionar = false;
                } else {
                    if (flagAdicionar) {
                        validOptions.push(this);
                    }
                }
            })
            // ler as options não selecionadas do select filtro das fichas
            //var options = $("#sqCicloAvaliacao").find('option:not(:selected)').clone();
        if (validOptions.length > 0) {
            selectCicloDestino.append(validOptions);
        } else {
            selectCicloDestino.append('<option value="" selected>Nenhum ciclo disponível</option>');
        }
    },
    tabAvaliacaoInit: function(params) {
        // var classe = $("#breadcrumb_nivel_3").html().toUpperCase();
        var grupo = $("#divEdicaoFicha #sqGrupoSalve option:selected").data('codigo');
        if ($("#liTabAvaExpedita").size() > 0) {
            //if (/INSECTA/.test(classe)) {
            if (grupo == 'INVERTEBRADOS_TERRESTRES') {
                $("#liTabAvaExpedita").show();
            } else {
                $("#liTabAvaExpedita").hide();
            }

            // abrir a aba 11.6 automaticamente mas as sugestão não foi aprovada pela equipe
            /*var aba116 = $("#liTabAvaResultado");
            if( aba116.size() == 1 ){
                $("#liTabAvaResultado a").click();
            }*/
        }
    },
    list: function(params, fld, event) {
        params = params || {};
        if (typeof event != 'undefined') {
            event.preventDefault();
        }
        //var data = $.merge($('#frmFichaFiltro').serializeArray(), $('#frmCicloAvaliacao').serializeArray());
        var data = ficha.filtros;
        data.sqCicloAvaliacao = $("#sqCicloAvaliacao").val();
        app.params2data(params, data);

        /*if (!$("#sqCicloAvaliacao").val()) {
            $("#divGridFicha").html('').addClass('hidden');
            $("#frmFichaFiltro,#divFiltrosFicha").addClass('hidden');
            return;
        }
        */

        var ajaxReturnType = 'html';
        if (params.csv == 'S') {
            data.csv = 'S';
            $("#btnExportCsvFicha").prop('disabled',true);
            $("#btnExportCsvFicha i").addClass('fa-cog');
            app.blockUI('Exportando fichas em formato CSV. Aguarde...');
            ajaxReturnType = 'json';
        } else {
            // app.blockElement('#frmGridFichas', 'Carregando gride...');
            // esconder formulario de ações em lote abaixo do gride
            $("#fichaLote").addClass('hidden');
            data.csv = 'N';
        }

        //var posicao = $("body").scrollTop();
        data.semCache = (ficha.semCache ? 'S' : '');
        if( ficha.paginationPageNumber > 0 ) {
            data.paginationPageNumber  = ficha.paginationPageNumber;
            ficha.paginationPageNumber = 0;
        }
        app.grid('GridFichas',data,function(res){
            ficha.carregando = false;
            app.unblockElement('#frmGridFichas');
            if (data.csv == 'S') {
                $("#btnExportCsvFicha").prop('disabled', false);
                $("#btnExportCsvFicha i").removeClass('fa-cog');
                if ( res.msg ) {
                    app.alert(res.msg);
                }
                if (res.status == 0) {
                    window.open(app.url + 'main/download?fileName=' + res.data.fileName + '&delete=1', '_top');
                }
            } else {
                // habilitar/desabilitar o botão de exportação
                var isEmpty =  $("#tableGridFichas tr[id^=tr-]").size() == 0;
                $("#divContainerBotaoExportar button#btnExportar").attr('disabled',isEmpty);

                // ativar seleção de linhas com shift+click
                $("#tableGridFichas").shiftSelectable();

                // atualizar status dos downloads em background se houver
                getWorkers();

                // preencher o select dos ciclos que pode ser selecionados para exportação das fichas
                ficha.updateSelectCicloDestinoCopia();
                /*if ($("#fichaContainerBody #isLastCicle").val() != 'S') {
                    $("#fichaContainerBody #loteSqFichaSituacao").parent().hide();
                } else {
                    $("#fichaContainerBody #loteSqFichaSituacao").parent().show();
                }*/
                // resposicionar scrollbar da página
                if( ficha.scrollTopBodyVoltar > -1 ) {
                    $(document).scrollTop( ficha.scrollTopBodyVoltar );
                    ficha.scrollTopBodyVoltar = -1;
                }
                // reposicionar o scrollbar do gride das fichas
                if( ficha.scrollTopGrideFichasVoltar > -1 ) {
                    $("#frmGridFichas div#containerGridFichas").scrollTop(ficha.scrollTopGrideFichasVoltar);
                    ficha.scrollTopGriBodyVoltarr = -1;
                }
            }
        });
        /*
        return;
        // gride antigo
        app.ajax(app.url + 'ficha/list', data, function(res) {
                ficha.carregando = false;
                app.unblockElement('#frmGridFichas');
                if (data.csv == 'S') {

                    $("#btnExportCsvFicha").prop('disabled', false);
                    $("#btnExportCsvFicha i").removeClass('fa-cog');
                    if (res.msg) {
                        app.alert(res.msg);
                    }
                    if (res.status == 0) {
                        window.open(app.url + 'main/download?fileName=' + res.data.fileName + '&delete=1', '_top');
                    }
                } else {
                    $("#divGridFicha").html(res);

                    // ativar seleção de linhas com shift+click
                    $("#divGridFicha").shiftSelectable();

                    // posicionar o gride das fichas no topo da tela
                    var posicao = Math.max( window.innerHeight - $("#divGridFicha").offset().top,290);
                    var gridHeight = Math.max( window.innerHeight - 330, 290);
                    //$("body").scrollTop(posicao);
                    // aplicar datatable
                    ficha.dataTable = $('#tableDivGridFicha').DataTable( $.extend({},default_data_tables_options,
                        {
                         "deferRender"      : true
                        ,"scrollY"          : gridHeight
                        ,"scrollX"          : true
                        ,"scrollCollapse"   : true
                        ,"fixedHeader"      : false
                        ,"lengthChange"     : false
                        ,"order": []
                        ,"buttons": []
                        ,"columnDefs" : [
                                {"orderable": false, "targets": [0,1,2,3,4,5,6,7,8] },
                            ]
                        })
                    );

                    // posicionar a barra lateral na posição anterior
                    if (ficha.scrollTopVoltarVoltar > -1) {
                        $(".dataTables_scrollBody").scrollTop(ficha.scrollTopVoltarVoltar );
                        $(document).scrollTop(ficha.scrollTopBodyVoltar );
                        ficha.scrollTopVoltarVoltar = -1;
                        ficha.scrollTopBodyVoltar = -1;
                        ficha.updateLastChanged();
                    }

                    // atualizar status dos downloads em background se houver
                    getWorkers();
                    //$(document).scrollTop(posicao);
                    // preencher o select dos ciclos que pode ser selecionados para exportação das fichas
                    ficha.updateSelectCicloDestinoCopia();
                    if ($("#fichaContainerBody #isLastCicle").val() != 'S') {
                        $("#fichaContainerBody #loteSqFichaSituacao").parent().hide()
                    } else {
                        $("#fichaContainerBody #loteSqFichaSituacao").parent().show()
                    }
                }
            }, '', ajaxReturnType);
        */
    },
    updateSituacaoPendencia:function( params, ele, evt , callback ){
        if( evt ){
            evt.preventDefault(); // não alterar o input checkbox ainda
        }
        if( !params.sqFicha && params.sqFichaPendencia ){
            app.alertInfo('Parâmetros incorretos');
        }
        if (params.sqFicha) {
            app.ajax(app.url + 'ficha/updateSituacaoPendencia', {sqFicha: params.sqFicha,sqFichaPendencia:params.sqFichaPendencia},
                function ( res ) {
                    app.eval( callback, res );
                },'Gravando...');
        }
    },
    showFormPendencia: function(params) {
        params.onClose = 'ficha.updateGridPendencia';
        showFormPendencia(params);
    },
    updateGridPendencia: function() {
        if ($("#taxTabContent #divGridPendencia").get(0)) {
            ficha.getGrid('pendencia');
        }
    },
    situacaoPendenciaChange : function(params,ele,evt){
        ficha.updateSituacaoPendencia( params, ele, evt,function( res ) {
            if( res.status == 0 ) {
                $("#spanLogRevisaoAbaPendencia" +params.sqFichaPendencia ).html(res.data.log ? res.data.log : '' );
                if (res.data.stPendente == 'S') {
                    ele.checked = false;
                    $("#tr-pendencia-"+params.sqFichaPendencia).removeClass('green').addClass('red');
                } else {
                    ele.checked = true;
                    $("#tr-pendencia-"+params.sqFichaPendencia).removeClass('red').addClass('green');
                }
            }
        } );
    },
    edit: function(data) {
        // guardar a posição do scroll para voltar na mesma posição
        ficha.scrollTopBodyVoltar = $(document).scrollTop();
        ficha.scrollTopGrideFichasVoltar = $("#frmGridFichas div#containerGridFichas").scrollTop();
        ficha.paginationPageNumber = $("#divGridFichasPaginationContent select option:selected").val();

        // esconder form de ações em lote
        $("#frmGridFichas").addClass('hidden');
        $("#fichaLote").addClass('hidden');
        // limpar conteúdo das abas
        $("#taxTabContent div.tab-pane-ficha").html('');
        if (!data['sqFicha']) {
            // para cadastrar uma ficha é necessário selecionar o ciclo de avalição
            if (!$("#sqCicloAvaliacao").val()) {
                app.alertInfo('Selecione o ciclo de avaliação!');
                return;
            }
        }

        // $("#spanNomeCientificoFicha").text(data.noCientifico);
        app.loadModule(app.url + 'ficha/edit', data, 'divEdicaoFicha', function() {
            if ($("#btnAdd").is(':visible') || $("#btnAdd").size() == 0) {
                // esconder botoes, divs etc...
                $("#btnAdd,#btnBack").toggle();
                $("#divEdicaoFicha").removeClass('hidden');
            }
            if (data['sqFicha']) {
                $("#btnPdfFicha,#btnCadPendencia").show();
                window.setTimeout(function() {
                    taxonomia.showFrmTaxonomia();
                    ficha.getSubespecies();
                    $("#spanSituacaoFicha").html('<b>Situação</b>: ' + $("#situacaoFicha").val() );
                    $("#btnAlterarParaExcluida").show();
                    /*if( $("#cdSituacaoFicha").val() !='EXCLUIDA'){
                        $("#btnAlterarParaExcluida").show();
                    } else {
                        $("#btnAlterarParaExcluida").hide();
                    }*/
                }, 700);
            } else {
                $("#btnPdfFicha,#btnCadPendencia").hide();
            }
        });
    },
    alterarSituacaoParaExcluida:function( params ) {
      modal.alterarSituacaoFichaParaExcluida( $("#sqFicha").val() );
    },
    verHistoricoSituacaoExcluida:function( sqFicha ) {
        modal.historicoSituacaoFicha(sqFicha);
    },
    delete: function(data) {
        // somente permitir a e exclusão da ficha se ela estiver na situão EXCLUIDA
        if( data.cdSituacao != 'EXCLUIDA'){
            app.alertInfo('Para excluir a ficha do banco de dados, ela precisa estar na situação EXCLUÍDA.')
            return;
        }
        app.confirm('Confirma exclusão da ficha <b>' + data.descricao + '</b>?', function () {
            app.confirm('A ficha <b>' + data.descricao + '</b> será excluída completamente do banco de dados do SALVE.<br><br><span class="bold red">Tem certeza?</span>', function () {
                app.ajax(app.url + 'ficha/delete', data, function (res) {
                    if (res.status === 0) {
                        //$("#gdFichaRow_" + data.row).remove();
                        $("#tr-" + data.sqFicha).remove();
                    }
                });
            }, null, data, 'ATENÇÃO', 'danger');
        }, null, data, 'Confirmação', 'warning');
    },
    onTaxonChange: function(evt) {
        // limpar os selects filhos ao alterar um dos pais
        var fld = evt.target;
        $("#divSearchTaxonTree select.select2").each(function() {
            if( $(this).data("s2Params") ) {
                if ($(this).data("s2Params").indexOf(fld.id) > -1) {
                    $(this).empty();
                }
            }
        });
        $("#divBreadcrumb").hide();
        $("#sqTaxon").val('');
        var data = $(fld).select2('data');
        if (data[0]) {
            // para cadastrar um ficha tem que ser uma especie ou subespecie preenchidos
            if ($("#w_idSubespecie").val()) {
                $("#sqTaxon").val($("#w_idSubespecie").val());
            } else if ($("#w_idEspecie").val()) {
                $("#sqTaxon").val($("#w_idEspecie").val());
            };
            // preencher campos pais
            $.each(data[0], function(key) {
                    if (/^id/.test(key) && data[0][key]) {
                        // não precisa mexer no próprio select2 ou se o ancestral ja tiver preenchido
                        if (data[0][key] != data[0]['id'] && !$("#w_" + key).select2('val')) {
                            var item = {
                                'id': data[0][key],
                                'text': data[0][key.replace('id', 'nm')],
                                'descricao': data[0][key.replace('id', 'nm')]
                            };
                            $("#w_" + key).html('<option value="' + item.id + '">' + item.text + '</option>');
                            $("#w_" + key).select2('data', item);
                        }
                    }
                });
                // atualizar o breadcrumb dos niveis taxonomicos exibido no topo da página
            $("#divBreadcrumb ol.breadcrumb").html('');
            if (data[0]['txNomes']) {
                data[0]['txNomes'].split(',').map(function(k) {
                    $("#divBreadcrumb ol.breadcrumb").append('<li style="text-transform:capitalize;" class="breadcrumb-item">' + k.capitalize() + '</li>');
                });
            }
        }
        // mostrar/esconder o nivel tribo
        data = $("#w_idClasse").select2('data');
        if (data.length == 0 || data[0].text && data[0].text.toUpperCase().trim() === 'INSECTA') {
            $("#w_idTribo,#w_idSubfamilia").parent().removeClass('hidden');
        } else {
            $("#w_idTribo,#w_idSubfamilia").parent().addClass('hidden');
        }
    },
    updateGridFichas: function(filtros) {
        if (ficha.carregando) {
            return;
        }
        $("#frmGridFichas #sqCicloAvaliacao").prop('disabled',true);
        ficha.carregando = true;
        ficha.filtros = filtros;
        ficha.list();
        // habilitar o campo select do ciclo
        $("#frmGridFichas #sqCicloAvaliacao").prop('disabled',false);
    },
    tabClick: function(data, link, evt) {
        $(evt.target).closest('div.tab-content').find('div.tab-content-tips').hide();

        // fazer refresh no mapa da aba distribuição
        if( data && data.container== "tabDistribuicao" ) {
            if( distribuicao && distribuicao.oMap && distribuicao.oMap.map ) {
               setTimeout(function(){
                   try{distribuicao.oMap.map.updateSize();}catch(e){}
               },2000);
            }
        } else if( data && data.container== "tabDisOcorrencia" ) {
            // fazer refresh no mapa da aba 2.6.1: ocorrencia
            if( ocorrencia && ocorrencia.oMap && ocorrencia.oMap.map ) {
               window.setTimeout(function(){
                   try{ocorrencia.oMap.map.updateSize();}catch(e){}
               },2000);
            }
        }
        return true;
    },
    voltar: function(params) {
        // exibir botoes, divs etc...
        $("#btnAdd,#btnBack,#btnPdfFicha").toggle();
        $("#spanNomeCientificoFicha").text('');
        $("#spanSituacaoFicha").text('');
        $("#spanBtnAlterarTaxon,#btnAlterarParaExcluida").hide();
        $("#btnPdfFicha").hide();
        $("#divEdicaoFicha").addClass('hidden');
        $("#frmGridFichas").removeClass('hidden');
        canModify = null;
        $("#sqFicha").val('');
        tinyMCE.editors = [];
        ficha.list(params);

        // destruir o objeto ocorrencia carregado na aba ocorrencia
        // no arquivo ocorrencia.js
        if( typeof ocorrencia == 'object')
        {
            delete ocorrencia;
        }
    },

    // janela modal para selecionar uma unica ref. bibliográfica para se salve junto com a tabela pai
    openModalSelRefBibTemp: function(data, e) {
        var height = window.innerHeight - 150;
        var width = $(window).width() - 100;
        data.height = height;
        // subespecies
        if( !data.sqFicha ) {
            data.sqFicha = $("#sqFicha").val();
        }
        data.reload = false;
        data.modalName = 'selecionarRefBibTemp';
        app.openWindow({
            id: 'selRefBibTemp',
            url: app.url + 'ficha/getModal',
            width: width,
            data: data,
            height: height,
            title: 'Selecionar Referência Bibliográfica',
            autoOpen: true,
            modal: true
        }, function(data, e) {
            // onShow
            data = data.data;
        }, function(data, e) {
            // onClose;
            var aba = $("#frmSelRefBibFichaTemp #ulTabSelRefBibTemp li.active a").data('tab');
            var data = $("#frmSelRefBibFichaTemp #sqPublicacao").select2('data')
            if( data.length > 0  ) {
                // publicacao selecionada
                $("#sqPublicacaoTemp").val($("#frmSelRefBibFichaTemp #sqPublicacao").select2('val'));
                $("#deTituloPublicacaoTemp").html(data[0].titulo);
                // limpar comunicação pessoal
                $("#noAutorRefTemp").val();
                $("#nuAnoRefTemp").val();
                // quando houver ref bib não tem carência
                $("#frmDisOcoGeoreferencia #sqApoioPrazoCarencia option").map(function(i,option){
                    if( $(option).data('codigo') == 'SEM_CARENCIA') {
                        $("#frmDisOcoGeoreferencia #sqApoioPrazoCarencia").val(option.value);
                    }
                })
            }
            else if( $("#frmSelRefBibFichaTemp #noAutorRef").val() != '' ) {
                // comunicação pessoal
                $("#noAutorRefTemp").val($("#frmSelRefBibFichaTemp #noAutorRef").val());
                $("#nuAnoRefTemp").val($("#frmSelRefBibFichaTemp #nuAnoRef").val());
                $("#deTituloPublicacaoTemp").html($("#noAutorRefTemp").val() + ($("#nuAnoRefTemp").val() ? ', ' + $("#nuAnoRefTemp").val() : ''));
                // llimpar publicação
                $("#sqPublicacaoTemp").val('');
                $("#frmDisOcoGeoreferencia #sqApoioPrazoCarencia").val('');
            }
            // $("#deTituloPublicacaoTemp").html('?');
            // limpar campos sel ref bib
            $("#frmSelRefBibFichaTemp #sqPublicacao").empty();
            $("#noAutorRef,#nuAnoRef").val('');
        });
    },
    sqPublicacaoTempChange: function(params) {
        var data = $("#frmSelRefBibFichaTemp #sqPublicacao").select2('data');
        if (data.length > 0) {
            app.closeWindow('selRefBibTemp');
        }
    },
    // janela modal para selecionar uma ref. bibliográfica a um campo da ficha
    openModalSelRefBibFicha: function(data, e) {
        var height = window.innerHeight - 150;
        var width = $(window).width() - 100;
        if (data.all) {
            height = 450; // não tem o gride na parte de baixo
        }

        data.comPessoal = ( data.comPessoal == 1 || data.comPessoal == true ? true : false )// mostrar/esconder a aba para informar a comunicação pessoal
        data.isComPessoal = ( data.isComPessoal == 'true' || data.isComPessoal == true ? true : false )// abrir a janela com a aba Comunicação pessoal selecionada
        data.height = height;

        // subespecies
        if( !data.sqFicha ) {
            data.sqFicha = $("#sqFicha").val();
        }
        data.reload = false;
        data.modalName = 'selecionarRefBib';
        app.openWindow({
            id: 'selRefBib',
            url: app.url + 'ficha/getModal',
            width: width,
            data: data,
            height: height,
            title: 'Selecionar Referência Bibliográfica',
            autoOpen: true,
            modal: true
        }, function(data, e) {
            // onShow
            data = data.data;

            $("#btnSaveFrmSelRefBibFicha").data('params','sqFicha');
            if( data.sqFicha )
            {
                $("#btnSaveFrmSelRefBibFicha").data('params','sqFicha:'+data.sqFicha)
            }
            $("#frmSelRefBibFicha").data('modified', false); // flag do form para save se foi alterado ou não

            $("#divGridRefBib").data('noTabela', data['noTabela']);
            $("#divGridRefBib").data('noColuna', data['noColuna']);
            $("#divGridRefBib").data('deRotulo', data['deRotulo']);
            $("#divGridRefBib").data('sqRegistro', data['sqRegistro']);
            if( data['comPessoal'] ) {
                $("#liTabSelComPessoal").show();
            }
            else {
                $("#liTabSelComPessoal").hide();
            }

            if( data['isComPessoal'] ) {
                app.selectTab('liTabSelComPessoal');
            } else {
                app.selectTab('liTabSelRefBib');
            }

            if (data.sqPublicacao) {
                $("#btnResetFrmSelRefBibFicha").removeClass('hide');
                $("#btnSaveFrmSelRefBibFicha").data('value', $("#btnSaveFrmSelRefBibFicha").html());
                $("#btnSaveFrmSelRefBibFicha").html("Gravar Alteração");
            }
            if (data.all) {
                $("#divGridRefBib").hide();
                $("#divGridRefBib").html('');
                app.setFormFields(data);
            } else {
                $("#divGridRefBib").show();
                ficha.updateGridRefBib(data.sqFicha);
            }
            // alterar os valores dos parametros que serão enviados ao clicar no botão salvar, de acordo com o contexto
            for (key in data) {
                if (key !== 'action') {
                    $("#btnSaveFrmSelRefBibFicha").data(key, data[key]);
                    $("#frmSelRefBibFicha #btnAddRefBibFicha").data(key, data[key]);
                }
                if (key == 'deRotulo') {
                    if (data[key]) {
                        $("#modalFrmSelRefBibFichaLabel").html(data[key]);
                    }
                }
            }
            // ler as últimas 5 referencias utilizadas na ficha
            var divUltimasReferencias = $("#divRefBibUltimasUtilizadas");
            app.ajax(app.url + 'ficha/getUltimasReferencias', data, function (res) {
                if (res.status === 0) {
                    if( res.data.length > 0 ){
                        var botoes = []
                        res.data.map(function(item){
                            botoes.push('<button type="button" class="btn btn-link btn-xs" ' +
                                'style="max-width:200px;text-overflow:ellipsis;white-space: nowrap;overflow: hidden" data-sq-publicacao="' +
                                item.sq_publicacao + '" data-text="' + item.de_titulo_autor_ano + '" ' +
                                'data-action="ficha.selecionarRefBibUtilizada" title="' + item.de_titulo_autor_ano + '">' + item.de_titulo_autor_ano.substr(0,30) + '</button>');
                        });
                        divUltimasReferencias.find('div.conteudo').html(botoes.join('\n') );
                        divUltimasReferencias.show();
                    } else {
                        divUltimasReferencias.hide();
                        divUltimasReferencias.find('div.conteudo').html('');
                    }
                }
            });

        }, function(data, e) {
            // onClose;
            $("#btnResetFrmSelRefBibFicha").addClass('hide');
            // limpar o gride anterior
            $("#divGridRefBib").html('');
            $("#btnResetFrmSelRefBibFicha").addClass('hide');
            $("#btnSaveFrmSelRefBibFicha").html($("#btnSaveFrmSelRefBibFicha").data('value'));
            app.unselectGridRow('divGridRefBib');
            app.unselectGridRow('divGridAllRefBib');
            if (data.data.updateGrid) {
                ficha.getGrid(data.data.updateGrid);
            }
            if (data.data.onClose) {
                app.eval(data.data.onClose /*,data.data*/); // se passar o data.data aqui para a funcao eval() nao funciona
            }
            app.reset('frmSelRefBibFicha');
            app.selectTab("tabSelRefBib")
        });
    },
    saveFrmSelRefBibFicha: function(data) {
        var aba = $("#frmSelRefBibFicha #ulTabSelRefBib li.active a").data('tab');
        if (aba == '1') {
            if (!$("#frmSelRefBibFicha #sqPublicacao").val()) {
                return app.alertInfo('Selecione uma referência bibliográfica!');
            }
        } else {
            if (!$("#frmSelRefBibFicha #noAutorRef").val()) {
                return app.alertInfo('Informe o nome do autor!');
            }
            $("#frmSelRefBibFicha #sqPublicacao").empty();
        }
        //data['deTags'] = $("#deTags").select2('val').join(';');
        data['deTagsIds'] = $("#frmSelRefBibFicha #deTagsIds").select2('val');
        // if( data['deTags'] instanceof Array )
        if (!data.sqFicha) {
            app.alertError('ID da ficha não encontrado!');
            return;
        }
        app.ajax(app.url + 'ficha/saveFrmRefBib', data,
            function(res) {
                if (res.status == 0) {
                    $("#frmSelRefBibFicha").data('modified', true);
                    app.reset('frmSelRefBibFicha'); // limpar formulário
                    $("#liTabSelComPessoal").removeClass('changed');
                    // se for alteração, fechar a modal ao salvar
                    if (data.all) {
                        app.closeWindow('selRefBib');
                        ficha.updateGridAllRefBib();
                    } else {
                        app.focus('sqPublicacao'); // colocar o cursor no campo de pesquisa da Ref. Bib.
                        ficha.updateGridRefBib(data.sqFicha); // atualizar o gride
                    }
                }
            }, null, 'json'
        );
    },
    editRefBib: function(data, btn) {

        modal.cadRefBib(data);

        /*
        app.ajax(app.url + 'ficha/editRefBib', data,
            function(res) {
                if (res) {
                    res.all = data.all
                    app.selectGridRow(btn);
                    if (res.all) {
                        ficha.openModalSelRefBibFicha(res);
                    } else {
                        $("#btnResetFrmSelRefBibFicha").removeClass('hide');
                        $("#btnSaveFrmSelRefBibFicha").data('value', $("#btnSaveFrmSelRefBibFicha").html());
                        $("#btnSaveFrmSelRefBibFicha").data('all', data.all);
                        $("#btnSaveFrmSelRefBibFicha").html("Gravar Alteração");
                        app.setFormFields(res);
                    }
                }
            }, null, 'json'
        );
        */
    },
    resetFrmSelRefBibFicha: function(params, btn) {
        app.reset('frmSelRefBibFicha');
        $("#sqFichaRefBib").focus();
        $("#btnResetFrmSelRefBibFicha").addClass('hide');
        $("#btnSaveFrmSelRefBibFicha").html($("#btnSaveFrmSelRefBibFicha").data('value'));
        app.unselectGridRow('divGridRefBib');
    },
    updateGridRefBib: function( sqFicha ) {
        ficha.getGrid('refBib',null,null,sqFicha);
    },
    updateGridAllRefBib: function() {
        ficha.getGrid('allRefBib');
    },
    selecionarRefBibUtilizada:function( params ) {
        app.setSelect2('sqPublicacao',{id:params.sqPublicacao},params.text,'frmSelRefBibFicha');
        app.growl('Referência selecionada',1,'Mensagem','tc','small','success')
    },
    /**
     * Excluir o publicacao da ficha
     * @param params
     * @param btn
     */
    deleteFichaPublicacao: function(params, btn) {
        if( !params.sqPublicacao ){
            app.alertInfo('Id referência bibliográfica não informado!')
            return;
        }if( !params.sqFicha ){
            app.alertInfo('Id ficha não informado!')
            return;
        }
        app.selectGridRow(btn);

        // ler onde a referencia está sendo utilizada na ficha
        app.ajax(app.url + 'ficha/getLocaisRefBibUtilizada',
            params,
            function( res ) {
                if( res.status == 0 ){
                    var locaisUtilizacao = ['Aba 9 - Ref. Bibliográficas']
                    if( res.data.length > 0 ){
                        locaisUtilizacao=[]
                        res.data.map( function( item ){
                            locaisUtilizacao.push( item.de_rotulo );
                        })
                    }
                    var nomesLocais = ( locaisUtilizacao.length>0 ? '<br><b>'+(locaisUtilizacao.length == 1 ? 'Local':'Locais' ) +' na ficha onde a referência foi adicionada:</b><br><div class="text-complementar-confirm">- '+locaisUtilizacao.join('<br>- ')+'</div>':'')

                    if( params.isCi ){
                        app.alertInfo('A exclusão desta referência deve ser feita onde ela foi adicionada na ficha.<br>' + nomesLocais );
                        return
                    }

                    app.confirm('<br>Confirma exclusão da referência bibliográfica da ficha?'+nomesLocais,
                        function() {

                            app.confirm('<br>Tem certeza que deseja excluir a referência?',
                                function() {
                                    app.ajax(app.url + 'ficha/deleteFichaPublicacao',
                                        params,
                                        function(res) {
                                            ficha.updateGridRefBib();
                                            ficha.updateGridAllRefBib();
                                        });
                                },
                                function() {
                                    app.unselectGridRow(btn);
                                }, params, 'Excluir a Referência', 'danger');
                        },
                        function() {
                            app.unselectGridRow(btn);
                        }, params, 'Confirmação', 'warning');
                }
            });
    },

    /**
     * Excluir o vinculo da ref bib com algum campo da ficha
     * @param params
     * @param btn
     */
    deleteFichaRefBib: function(params, btn) {
        if( !params.sqFichaRefBib ){
            app.alertInfo('Id do registro não informado!')
            return;
        }
        app.selectGridRow(btn);
        app.confirm('<br>Confirma exclusão da referência bibliográfica da ficha?',
            function() {
                app.ajax(app.url + 'ficha/deleteFichaRefBib',
                    params,
                    function(res) {
                        ficha.updateGridRefBib();
                        ficha.updateGridAllRefBib();
                    });
            },
            function() {
                app.unselectGridRow(btn);
            }, params, 'Confirmação', 'warning');
    },
    /**
     * Excluir a referencia bibliográfica
     * @param params
     * @param btn
     */
    deleteRefBib: function(params, btn) {
        if( !params.sqPopulacao ){
            app.alertInfo('Id da referência não informado.')
            return;
        }
        var data = app.params2data(params,{utilizacao:''})
        app.ajax(app.url + 'ficha/getReferenciaUtilizacao',
            params,
            function( res ) {
                if( res.data.rotulos.length > 0 ){
                    data.utilizacao = res.data.rotulos.join(', ' );
                    app.alertInfo("A referência não pode ser excluída por estar sendo utilizada em: <b>" + data.utilizacao + "</b>");
                    return; // não permitir  excluir
                }

                app.selectGridRow(btn);
                //app.confirm((data.utilizacao==''?'':'<br>A referência está sendo utilizada em: <b>'+data.utilizacao + '</b>.' )+'<br>Confirma exclusão da referência bibliográfica da ficha?',
                app.confirm('<br>Confirma exclusão da referência bibliográfica?',
                    function() {
                        app.ajax(app.url + 'ficha/deleteRefBib',
                            params,
                            function(res) {
                                ficha.updateGridRefBib();
                                ficha.updateGridAllRefBib();
                            });
                    },
                    function() {
                        app.unselectGridRow(btn);
                    }, params, 'Confirmação', 'warning');


            });
    },
    /**
     * Metodo para carregar os gride da ficha
     * @param  string  gridName - pode ser passado o id da div ou o nome do grid. Ex: getGrid('nomeum') ou getGrid('divGridNomeComum')
     * @return null
     */                                                      // subespecies
    getGrid: function(gridName, idContainer, callback, sqFicha, filtros ) {
        gridName = gridName.replace('#', '').replace('divGrid', '');
        var div = $("#" + gridName);
        var data = {};
        if (!div.get(0)) {
            div = $('#divGrid' + gridName.capitalize());
        }
        data = {
            'gridName': gridName.lcFirst()
        };

        if( filtros ) {
            $.extend(data,filtros)
        }

        if (!div.get(0)) {
            //app.alertError('Container do gride ' + gridName + ' não encontrado!');
            return;
        }

        // subespecies
        // sempre tem que mandar o id da ficha
        if( ! sqFicha ) {
            data['sqFicha'] = $("#sqFicha").val();
        }
        else
        {
            data['sqFicha'] = sqFicha
        }
        // enviar os parametros definidos na div container do grid
        data['params'] = div.data();
        data['container'] = idContainer || '';
        var posicao = div.scrollTop();

        app.blockElement($(div).attr('id'), 'Atualizando. Aguarde...',null,function(){
            // adicionar spinner na div
            //div.html('<div style="width:100%;height:100px;padding:20px;text-align:center;background-color:#fff;font-size:18px;">' + window.grails.spinner + '</div>');
            app.ajax(app.url + 'ficha/getGrid', data, function(res) {
                app.unblockElement($(div).attr('id'));
                // adicionar o resultado na div
                div.html(res);
                // inicializar exibição dos tooltips nos elementos
                app.initSecurity(div.prop('id'));
                app.initTooltips(div.prop('id'));
                bindDialogPreview(div.prop('id'));
                app.eval(callback, [res, data]);
            }, '', 'HTML');
        });
    },
    habitatPaiChange: function(params, data, evt) {
        var form = params.form;
        //var sqHabitatPai = $('#' + form + ' #' + evt.target.id );
        var sqHabitatPai = $( evt.target );
        var sqHabitat = $('#' + form + ' #' + params.child);
            // desabilitar o select filho
        sqHabitat.prop('disabled', true);
        // se for informado alguma valor, carregar os filhos
        if (sqHabitatPai.val()) {
            var data = {
                'sqHabitatPai': sqHabitatPai.val(),
                'sqFicha': $("#sqFicha").val()
            };
            $("#"+evt.target.id ).val( sqHabitatPai.val() );
            app.ajax(app.url + 'ficha/getHabitat', data, function(res) {
                sqHabitat.html('');
                sqHabitat.append('<option value="" selected>-- selecione --</option>');
                if (res.length) {
                    sqHabitat.prop('disabled', false);
                    $.each(res, function(key, item) {
                        sqHabitat.append('<option value="' + item['id'] + '" data-codigo="' + item['codigo'] + '">' + item['descricao'] + '</option>');
                    });
                }
            }, null, 'json');
        }
    },
    abrangenciaChange: function(params) {
        var form = params.form;
        var selectPai = $("#" + form + " #sqTipoAbrangencia");
        var selectFilho = $("#" + form + " #sqAbrangencia");
        var data = selectPai.find('option:selected').data();
        data = app.params2data(params, data)
            // alterar o name do select
        selectFilho.prop('name', String('sq_' + data.codigo.toLowerCase()).toCamel())
            // desabilitar o select
        selectFilho.prop('disabled', true);
        // limpar o select
        selectFilho.find('option').each(function(k, v) {
            if (this.value) {
                $(this).remove(); //or whatever else
            }
        });
        if (data.codigo == 'OUTRA') {
            if (!$("#" + form + " #sqTipoAbrangencia").parent().hasClass('hide')) {
                $("#" + form + " #noRegiaoOutra").parent().removeClass('hide');
            }
            $("#" + form + " #sqAbrangencia").parent().addClass('hide');
        } else {
            $("#" + form + " #noRegiaoOutra").parent().addClass('hide');
            if (!$("#" + form + " #sqTipoAbrangencia").parent().hasClass('hide')) {
                $("#" + form + " #sqAbrangencia").parent().removeClass('hide').css('display', 'inline-block');
            }
            // fazer chamada ajax para carregar o select filho
            if (data.codigo) {
                data.noCampo = selectFilho.prop('name');
                data.noDominio = data.noCampo.replace(/^sq/, '').ucFirst();
                data.noPropriedade = data.noDominio.lcFirst();
                data.sqFicha = $("#sqFicha").val();
                app.ajax(app.url + 'ficha/getTipoAbrangencia', data, function(res) {
                    var tmpVal = $("#" + form + " #sqAbrangencia").data('tmpVal');
                    $.each(res, function(k, v) {
                        selectFilho.append('<option value="' + v.codigo + '"' + (v.codigo == tmpVal ? ' selected' : '') + '>' + v.descricao + '</option>');
                    });
                    selectFilho.prop('disabled', (data.codigo == ''));
                }, '', 'JSON')
            }
        }
    },

    // janela modal para selecionar uma anexo a um contexto da ficha
    openModalSelAnexo: function(data, e) {
        data.sqFicha = $("#sqFicha").val();
        data.reload = true;
        data.modalName = 'fichaAnexo';
        app.openWindow({
            id: 'selAnexo',
            url: app.url + 'ficha/getModal',
            data: data,
            title: 'Selecionar Anexo',
            autoOpen: true,
            modal: true
        }, function(data, e) {
            // onShow
            data = data.data;
            if (!data.showPrincipal) {
                $("#inFichaAnexoPrincipal").parent().addClass('hidden');
            } else {
                $("#inFichaAnexoPrincipal").parent().removeClass('hidden');
            }
            // alterar os valores dos parametros que serão enviados ao clicar no botão salvar, de acordo com o contexto
            for (var key in data) {
                if (key !== 'action') {
                    $("#btnSaveFrmFichaAnexo").data(key, data[key]);
                }
                if (key == 'deRotulo') {
                    if (data[key]) {
                        $("#frmFichaAnexoLabelContexto").html(data[key]);
                    }
                }
            }
            if (data.updateGrid) {
                $("#" + data.updateGrid).data('contexto', data.contexto);
            }
        }, function(data, e) {
            // onClose;
            if (data.data.updateGrid) {
                ficha.getGridAnexos(data.data.updateGrid);
            }
            app.reset('frmSelAnexo');
        });
    },
    saveAnexo: function(params) {
        if (!$("#frmFichaAnexo").valid()) {
            return;
        }
        var data = $("#frmFichaAnexo").serializefiles();
        data.append('sqFicha', params.sqFicha);
        data.append('contexto', params.contexto);
        if( data.get('dtAnexo') ) {
            var dataAnexo = data.get( 'dtAnexo');
            if( ! app.isDate( dataAnexo)  ) {
                app.alertInfo('Data ' + dataAnexo + ' não existe.');
                return
            }
        }

        if (!data.get('sqFicha')) {
            app.alertError('ID da ficha não encontrado!');
            return;
        }
        app.ajax(app.url + 'ficha/saveAnexo', data,
            function(res) {
                if (res.status == 0) {
                    app.reset('frmFichaAnexo','dtAnexo');
                    app.focus('deLegendaFichaAnexo');
                    // não precisa atualizar o gride a cada foto, somente ao fechar o pop-up
                    /*if( params.updateGrid)
                     {
                     ficha.getGridAnexos(params.updateGrid);
                     }
                     */
                }
            }, null, 'json');
    },
    getGridAnexos: function(idGrid) {
        // ativar o zoom ao clicar na imagem
        ficha.getGrid(idGrid, '', function() {
            bindDialogPreview(idGrid);
        }); // fim getgrid
    },
    deleteAnexo: function(params, btn) {
        app.selectGridRow(btn);
        app.confirm('<br>Confirma exclusão do arquivo ' + params.descricao + '?',
            function() {
                app.ajax('ficha/deleteAnexo',
                    params,
                    function(res) {
                        if (params.updateGrid) {
                            ficha.getGrid(params.updateGrid);
                        }
                    });
            },
            function() {
                app.unselectGridRow(btn);
            }, params, 'Confirmação', 'warning');
    },

    // inicio funçõe de adição de autores na ficha
    openModalAutores: function(params) {
        $.jsPanel({
            id: "ficha_autores",
            theme: "#6FA04B",
            headerTitle: 'Cadastro de Autor',
            contentSize: {
                width: 700,
                height: 400
            },
            theme: "bootstrap-success",
            content: '<div id="div_content_cad_autor" style="border:none;display:block;min-height:100%;"<br><br><p>Carregando...' + window.grails.spinner + '</p></div>',
            resizable: false,
            callback: function(panel) {
                this.find('.jsglyph-minimize').hide();
                this.content.css("padding", "15px");
                panel.find('.jsPanel-content').prop('id', panel.attr('id') + '_body')
                app.loadModule(app.url + 'fichaAutor/getForm', params, panel.attr('id') + '_body', function(res) {}, '');
            },
        });
    },
    checkUncheckAll: function(params, elem, container) {
        var checked;
        if ($(elem).hasClass('glyphicon-unchecked')) {
            checked = true;
            $(elem).removeClass('glyphicon-unchecked').addClass('glyphicon-check');
        } else {
            checked = false;
            $(elem).removeClass('glyphicon-check').addClass('glyphicon-unchecked');
        }
        if( params.tableId )
        {
            $("#"+params.tableId.replace(/^#/,'') ).find('tbody input:checkbox:visible').each(function() {
                this.checked = checked;
                if( checked ) {
                    $(this).closest('tr').addClass('tr-selected');
                } else {
                    $(this).closest('tr').removeClass('tr-selected');
                }
            });
        }
        else {
            $(elem).closest('thead').next().find('input:checkbox:visible').each(function () {
                this.checked = checked;
            });
        }
    },
    selecionarTodas: function(params, elem) {
        $("#gridFichasSelectedIds").val('');
        ficha.checkUncheckAll(params, elem);
        ficha.chkSqFichaChange();
    },
    copiarFichasCiclo: function(params, btn, evt, copiarPendencias, copiarExcluidas) {
        var qtdFichas = $("#containerGridFichas input:checkbox:checked").size();

        if (qtdFichas === 0) {
            app.alert('Nenhuma ficha selecionada!');
            return;
        }

        if ( ! $("#sqCicloDestinoCopia option:selected").val() ) {
            app.alert('Selecione o ciclo de destino!');
            return;
        }


        if (!copiarPendencias) {
            app.confirm('<br>Deseja copiar também as PENDÊNCIAS?', function() {
                return ficha.copiarFichasCiclo(params, btn, evt, 'S');
            }, function() {
                return ficha.copiarFichasCiclo(params, btn, evt, 'N');
            }, null, 'Confirmação', 'warning');
            return;
        }

        if (!copiarExcluidas) {
            app.confirm('<br>Deseja copiar também as fichas EXCLUÍDAS?', function() {
                return ficha.copiarFichasCiclo(params, btn, evt, 'S', 'S');
            }, function() {
                return ficha.copiarFichasCiclo(params, btn, evt, 'N', 'N');
            }, null, 'Confirmação', 'warning');
            return;
        }

        var ciclo = $("#sqCicloDestinoCopia option:selected").text().trim()
        var mensagem = '<br>Confirma cópia de <b>' + qtdFichas + ' ficha' + (qtdFichas > 1 ? 's' : '') +
            '</b> para o ciclo <b>' + ciclo + '</b>?' +
              '<br><b>- '+(copiarPendencias == 'S' ? 'COM as pendências.' : 'SEM as pendências.') + '</b>'+
              '<br><b>- '+(copiarExcluidas == 'S' ? 'Copiar as fichas EXCLUÍDAS.' : 'NÃO copiar as fichas EXCLUÍDAS.') + '</b>'


        app.confirm(mensagem, function() {
                var data = {
                    sqCicloDestino: $("#sqCicloDestinoCopia").val(),
                    dsCicloDestino: $("#sqCicloDestinoCopia option:selected").text().trim(),
                    copiarPendencias: copiarPendencias,
                    copiarExcluidas: copiarExcluidas,
                    ids: []
                };
                $("#containerGridFichas input:checkbox:checked").each(function() {
                    data.ids.push(this.value);
                });
                if (data.length == 0) {
                    app.alertInfo('Nenhuma ficha selecionada!')
                    return;
                }
                data.ids = data.ids.join(',');
                app.ajax(app.url + 'ficha/copiarFicha', data, function(res) {
                    if (res.status === 0) {
                        app.growl('Cópia da(s) ficha(s) em andamento.',null,'Mensagem','tc');
                        window.setTimeout(function(){
                            getWorkers();
                        },3000)

                    }
                });
            }, null, null, 'Confirmação', 'warning');
    },
    alterarSituacaoEmLote: function(params) {
        if (!$("#loteSqFichaSituacao").val()) {
            app.alertInfo('Selecione a situação!');
            return;
        }
        var $sqCicloAvaliacao = $("#fichaContainerBody #sqCicloAvaliacao")
        var data = {
            sqCicloAvaliacao : $sqCicloAvaliacao.val(),
            sqNovaSituacao   : $("#fichaContainerBody #loteSqFichaSituacao").val(),
            dsJustificativa  : ( params.dsJustificativa ? params.dsJustificativa : ''),
            ids: []
        };
        //$("#fichaContainerBody #containerGridFichas input:checkbox:checked").each(function() {
        $("#fichaContainerBody #containerGridFichas input:checkbox:checked").each(function() {
            data.ids.push(this.value);
        });
        data.ids = data.ids.join(',');
        if( data.ids.length ==0 ){
            app.alertInfo('Nenhuma ficha selecionada!');
            return;
        }
        // se a situacao selecionada for excluir, exibir a tela para informar o motivo
        if( $("#fichaContainerBody #loteSqFichaSituacao option:selected").data('codigo') =='EXCLUIDA' && !data.dsJustificativa ) {
            var modalData = {
                id          : data.ids[0],  // usar o primeiro id só para abrir a modal com um id
                modalName   : 'textoLivre',
                label       : 'Justificativa',
                maxLength   : 2000
                }
            modal.janelaTextoLivre({
                title: 'Exclusão do Processo de Avaliação',
                'url': '/main/getModal',
                'data': modalData,
                'height': 400,
                'width' : 800,
                'theme': '#bfe3c3',
                'resizeble': false,
                'onClose': function(panel) {
                    var $textArea = panel.content.find('textarea#fldTextoLivre' + modalData.id);
                    var texto = $.trim($textArea.val());
                    if ( texto ) {
                        data.dsJustificativa = texto;
                        data.ids='';
                        ficha.alterarSituacaoEmLote( data );
                        return;
                    }
                }
            });
            return;
        }
        app.confirm('<br>Confirma alteração para a situação <b>' + $("#loteSqFichaSituacao option:selected").text().trim() + '</b>?', function() {

            ficha.executarAcaoEmLote(data);
            if( data.dsJustificativa ) {
                $("#gridFichasSelectedIds").val('');
                $("#tableGridFichas input:checkbox[name=chkSqFicha]").prop('checked',false)
                ficha.chkSqFichaChange();
            }

            // desmarcar as fichas
            $("#gridFichasSelectedIds").val('');
            $("#checkAll-GridFichas").removeClass('glyphicon-check').addClass('glyphicon-unchecked');
            $("#tableGridFichas input:checkbox[name=chkSqFicha]").prop('checked',false)
            $("#tableGridFichas tbody tr").removeClass('tr-selected');
            ficha.chkSqFichaChange();

        }, null, null, 'Confirmação', 'warning');
    },
    executarAcaoEmLote:function( data ) {
        app.ajax(app.url + 'ficha/alterarSituacaoEmLote', data, function(res) {
            if ( res.status === 0 ) {
                if( res.data.tokenAutorizacao ) {
                    var msgs = [];
                    if( res.data.qtdFichasAvaliadas > 0 ) {
                        msgs.push(res.data.qtdFichasAvaliadas +' ficha(s) com informações na aba 11.6-Resultados da avaliação.');
                    }
                    if( res.data.qtdFichasValidadas > 0 ) {
                        msgs.push( res.data.qtdFichasValidadas +' ficha(s) com informações na aba 11.7-Resultado da validação.');
                    }
                    msgs.push('<div class="blinking-red" style="padding 10px 0px;">ESTAS INFORMAÇÕES SERÃO APAGADAS DAS FICHAS</div>')
                    app.confirm( msgs.join('<br>') + '<br><b>Confirma a operação?</b>',function(){
                        app.confirm(  msgs.join('<br>'),function(){
                            data.tokenAutorizacao = res.data.tokenAutorizacao
                            ficha.executarAcaoEmLote(data);
                        },null,null,'TEM CERTEZA?','danger');
                    });
                } else {
                    ficha.semCache = true;
                    ficha.list();
                    ficha.semCache = false;
                }
            }
        }, "Processando...");
    },
    excluirFichaEmLote: function(params) {
        var qtdFichas = $("#containerGridFichas input:checkbox:checked").size();
        var ciclo = $("#sqCicloAvaliacao option:selected").text();
        app.confirm('<br><h3>ATENÇÃO!</h3><h4>Confirma a EXCLUSÃO de <b>' + qtdFichas + '</b> ficha(s) do ciclo <b>' + ciclo + '</b>?<br><br><span class="blinking-red">Somente as fichas na situação excluída serão excluídas.</span></h4>', function() {
            app.confirm('<br><h3>TEM CERTEZA?</h3>', function() {
                var data = {
                    sqNovaSituacao: $("#loteSqFichaSituacao").val(),
                    ids: []
                };
                $("#fichaContainerBody #containerGridFichas input:checkbox:checked").each(function() {
                    data.ids.push(this.value);
                });
                data.ids = data.ids.join(',');
                app.ajax(app.url + 'ficha/excluirFichaEmLote', data, function(res) {
                    if (res.status === 0) {
                        ficha.semCache = true;
                        ficha.updateGridFichas();
                        ficha.semCache = false;
                    }
                }, "Processando...");

            }, null, null, 'Confirmação de Exclusão de Fichas', 'danger');

        }, null, null, 'Confirmação', 'danger');

    },
    alterarUnidadeLote: function(params) {
        var qtdFichas = $("#containerGridFichas input:checkbox:checked").size();
        var unidade = $("#sqUnidadeDestino option:selected").text();
        app.confirm('<br><h3>ATENÇÃO!</h3><h4>Confirma a TRANSFERÊNCIA da(s) <b>' + qtdFichas + '</b> ficha(s) para a Unidade ' + unidade + '?', function() {
            app.confirm('<br><h3>TEM CERTEZA?</h3>', function() {
                var data = {
                    sqUnidadeDestino: $("#sqUnidadeDestino").val(),
                    ids: []
                };
                $("#fichaContainerBody #containerGridFichas input:checkbox:checked").each(function() {
                    data.ids.push(this.value);
                });
                data.ids = data.ids.join(',');
                app.ajax(app.url + 'ficha/alterarUnidadeLote', data, function(res) {
                    if (res.status === 0) {
                        ficha.semCache = true;
                        ficha.list();
                        ficha.semCache = false;
                    }
                }, "Processando...");

            }, null, null, 'Confirmação de transferência de ficha(s)', 'danger');

        }, null, null, 'Confirmação', 'danger');

    },
    alterarGrupoAvaliadoLote: function(params) {
        var qtdFichas = $("#containerGridFichas input:checkbox:checked").size();
        var grupo       = $("#sqGrupoAvaliadoLote option:selected").text();
        var subgrupo    = $("#sqSubgrupoAvaliadoLote option:selected").text();
        app.confirm('<br><h3>ATENÇÃO!</h3><h4>Confirma a alteração do GRUPO e SUBGRUPO?<br><br><b>'+
            qtdFichas + '</b> ficha(s) para:<br>- Grupo: <b>'+grupo+'</b><br>- Subgrupo: <b>'+subgrupo+'</b>', function() {
            app.confirm('<br><h3>TEM CERTEZA?</h3>', function() {
                var data = {
                    sqGrupoAvaliado: $("#sqGrupoAvaliadoLote").val(),
                    sqSubgrupo: $("#sqSubgrupoAvaliadoLote").val(),
                    ids: []
                };
                $("#fichaContainerBody #containerGridFichas input:checkbox:checked").each(function() {
                    data.ids.push(this.value);
                });
                data.ids = data.ids.join(',');
                app.ajax(app.url + 'ficha/alterarGrupoSubgrupoLote', data, function(res) {
                }, "Processando...");
            }, null, null, 'Confirmação de alteração de grupo e subgrupo da(s) ficha(s)', 'danger');
        }, null, null, 'Confirmação', 'danger');
    },

    alterarGrupoSalveLote: function(params) {
        var qtdFichas = $("#containerGridFichas input:checkbox:checked").size();
        var grupoSalve = $("#sqGrupoSalveLote option:selected").text();
        app.confirm('<br><h3>ATENÇÃO!</h3><h4>Confirma a alteração do GRUPO (SALVE)?<br><br><b>'+
            qtdFichas + '</b> ficha(s) para:<br>- Grupo SALVE: <b>'+grupoSalve+'</b>', function() {
            app.confirm('<br><h3>TEM CERTEZA?</h3>', function() {
                var data = {
                    sqGrupoSalve: $("#sqGrupoSalveLote").val(),
                    ids: []
                };
                $("#fichaContainerBody #containerGridFichas input:checkbox:checked").each(function() {
                    data.ids.push(this.value);
                });
                data.ids = data.ids.join(',');
                app.ajax(app.url + 'ficha/alterarGrupoSalveLote', data, function(res) {
                }, "Processando...");
            }, null, null, 'Confirmação de alteração de grupo (SALVE) da(s) ficha(s)', 'danger');
        }, null, null, 'Confirmação', 'danger');
    },
    alterarAutoriaLote: function(params) {
        var qtdFichas = $("#containerGridFichas input:checkbox:checked").size();
        var autoria = $("#txAutoriaLote").val().trim();
        var preenchimentoAutomatico = $("#checkAutoriaLote")[0].checked ? 'S' : 'N';
        var mensagem = "Confirma a alteração"
        if( ! autoria && preenchimentoAutomatico == 'N') {
            //app.alertInfo('Informe os nomes dos autores ou selecione o preenchimento automático.');
            mensagem = "Confirma a LIMPEZA"
        }

        app.confirm('<br><h3>ATENÇÃO!</h3><h4>' + mensagem + ' da AUTORIA de <b>'+
            qtdFichas + '</b> ficha(s)?', function() {
            app.confirm('<br><h3>TEM CERTEZA?</h3>', function() {
                var data = {
                    txAutoria: autoria,
                    preechimentoAutomatico: preenchimentoAutomatico,
                    ids: []
                };
                $("#fichaContainerBody #containerGridFichas input:checkbox:checked").each(function() {
                    data.ids.push(this.value);
                });
                data.ids = data.ids.join(',');
                app.ajax(app.url + 'ficha/alterarAutoriaLote', data, function(res) {
                    //console.log( res );
                }, "Processando ação em lote...");
            }, null, null, mensagem +' da AUTORIA da(s) ficha(s)', 'danger');
        }, null, null, 'Confirmação', 'danger');
    },
    alterarEquipeTecnicaLote: function(params) {
        var qtdFichas = $("#containerGridFichas input:checkbox:checked").size();
        var texto = $("#txEquipeTecnicaLote").val().trim();
        var preenchimentoAutomatico = 'N'; //$("#checkEquipeTecnicaLote")[0].checked ? 'S' : 'N';
        var mensagem = ""
        if( ! texto && preenchimentoAutomatico == 'N') {
            //app.alertInfo('Informe os nomes que compõem a equipe técnica ou selecione o preenchimento automático.');
           //return;
            mensagem = "Confirma a LIMPEZA do campo da EQUIPE TÉCNICA"
        }

        app.confirm('<br><h3>ATENÇÃO!</h3><h4>'+mensagem+' de <b>'+
            qtdFichas + '</b> ficha(s)?', function() {
            app.confirm('<br><h3>TEM CERTEZA?</h3>', function() {
                var data = {
                    txEquipeTecnica: texto,
                    preechimentoAutomatico: preenchimentoAutomatico,
                    ids: []
                };
                $("#fichaContainerBody #containerGridFichas input:checkbox:checked").each(function() {
                    data.ids.push(this.value);
                });
                data.ids = data.ids.join(',');
                app.ajax(app.url + 'ficha/alterarEquipeTecnicaLote', data, function(res) {
                    //console.log( res );
                }, "Processando ação em lote...");
            }, null, null, 'Confirmação de alteração da equipe técnica da(s) ficha(s)', 'danger');
        }, null, null, 'Confirmação', 'danger');
    },

    alterarColaboradoresLote: function(params) {
        var qtdFichas = $("#containerGridFichas input:checkbox:checked").size();
        var texto = $("#txColaboradoresLote").val().trim();
        var preenchimentoAutomatico = $("#checkColaboradoresLote")[0].checked ? 'S' : 'N';
        if( ! texto && preenchimentoAutomatico == 'N') {
            app.alertInfo('Informe os nomes dos colaboradores ou selecione o preenchimento automático.');
           return;
        }

        app.confirm('<br><h3>ATENÇÃO!</h3><h4>Confirma a alteração dos COLABORADORES de <b>'+
            qtdFichas + '</b> ficha(s)?', function() {
            app.confirm('<br><h3>TEM CERTEZA?</h3>', function() {
                var data = {
                    txColaboradores: texto,
                    preechimentoAutomatico: preenchimentoAutomatico,
                    ids: []
                };
                $("#fichaContainerBody #containerGridFichas input:checkbox:checked").each(function() {
                    data.ids.push(this.value);
                });
                data.ids = data.ids.join(',');
                app.ajax(app.url + 'ficha/alterarColaboradoresLote', data, function(res) {
                    //console.log( res );
                }, "Processando ação em lote...");
            }, null, null, 'Confirmação de alteração dos colaboradores da(s) ficha(s)', 'danger');
        }, null, null, 'Confirmação', 'danger');
    },

    txAutoriaLoteChange : function() {
      var autoria = $("#txAutoriaLote").val().trim();
      if( autoria ) {
          $("#divCheckAutoriaLote").hide();
      } else {
          $("#divCheckAutoriaLote").show();
      }
    },
    chkAutoriaLoteChange:function(){
        $("#txAutoriaLote").prop('disabled',$("#checkAutoriaLote")[0].checked)
        if( $("#checkAutoriaLote")[0].checked ) {
            $("#txAutoriaLote").hide();
        } else {
            $("#txAutoriaLote").show();
        }
    },

    txEquipeTecnicaLoteChange : function() {
      var texto = $("#txEquipeTecnicaLote").val().trim();
      if( texto ) {
          $("#divCheckEquipeTecnicaLote").hide();
      } else {
          $("#divCheckEquipeTecnicaLote").show();
      }
    },

    chkEquipeTecnicaLoteChange:function(){
        $("#txEquipeTecnicaLote").prop('disabled',$("#checkEquipeTecnicaLote")[0].checked)
        if( $("#checkEquipeTecnicaLote")[0].checked ) {
            $("#txEquipeTecnicaLote").hide();
        } else {
            $("#txEquipeTecnicaLote").show();
        }
    },

    txColaboradoresLoteChange : function() {
      var texto = $("#txColaboradoresLote").val().trim();
      if( texto ) {
          $("#divCheckColaboradoresLote").hide();
      } else {
          $("#divCheckColaboradoresLote").show();
      }
    },

    chkColaboradoresLoteChange:function(){
        $("#txColaboradoresLote").prop('disabled',$("#checkColaboradoresLote")[0].checked)
        if( $("#checkColaboradoresLote")[0].checked ) {
            $("#txColaboradoresLote").hide();
        } else {
            $("#txColaboradoresLote").show();
        }
    },
    gridFichaOnLoad:function(params){
        ficha.filtros={};
        // marcar checkbox selecionados ao troca de página
        var $checkAll = $("#checkAll-GridFichas");
        if( $checkAll.size() == 1 && $checkAll.hasClass('glyphicon-check') ) {
            $("#tableGridFichas input:checkbox[name=chkSqFicha]").prop('checked',true);
        }
        var $inputSelectedIds = $("#gridFichasSelectedIds");
        if( $inputSelectedIds.size()==1 ) {
            var selectedIds = $inputSelectedIds.val() ? $inputSelectedIds.val().split(',') : [];
            selectedIds.map(function(id){
                if( id ) {
                    var input = $("input:checkbox[name=chkSqFicha][value=" + id + "]");
                    if (input.size() == 1) {
                        input.prop('checked', true);
                    }
                }
            });
        }
    },
    chkSqFichaChange: function() {
        // controle dos ids/linhas do gride selecionadas para mater a seleção na paginação
        var $inputSelectedIds = $("#gridFichasSelectedIds");
        var $btnQtdSelecionados = $("#btnQtdSelecionados");
        $btnQtdSelecionados.html('0 selecionados');
        if ($inputSelectedIds.size() == 1) {
            var selectedIds = $inputSelectedIds.val() ? $inputSelectedIds.val().split(',') : [];
            // ler todos os checks do gride
            $("#tableGridFichas input:checkbox[name=chkSqFicha]").each(function (index, item) {
                var value = String(item.value);
                var index = selectedIds.indexOf(value);
                if (item.checked) {
                    if (index == -1) {
                        selectedIds.push(value);
                    }
                } else {
                    if (index > -1) {
                        selectedIds.splice(index, 1)
                    }
                }
            });
            $inputSelectedIds.val(selectedIds.join(','));
            // exibir o total de registros selecionados acima do gride
            if( $btnQtdSelecionados.size() == 1 ) {
                $btnQtdSelecionados.html('0 selecionados');
                $btnQtdSelecionados.hide();
                if( selectedIds.length > 0 ) {
                    var $checkAll = $("#checkAll-GridFichas");
                    if( $checkAll.size() == 1 && ! $checkAll.hasClass('glyphicon-check') ) {
                        $btnQtdSelecionados.html(selectedIds.length + ' selecionado(s)');
                        $btnQtdSelecionados.show();
                    }
                }
            }
        }


        if ($("#fichaContainerBody #containerGridFichas input:checkbox:checked").size() === 0) {
            $("#fichaLote").addClass('hidden');
        } else {
            $("#fichaLote").removeClass('hidden');
            // preencher selects das ações em lote
            var $selectGrupoAvaliado = $("#sqGrupoAvaliadoLote");
            if( $selectGrupoAvaliado.find('option').length < 2 ) {
                $.post(app.url + 'api/getTableApoio', {tableName: 'TB_GRUPO'}).done(function (res) {
                    if (res.data.length > 0) {
                        $.each(res.data, function () {
                            $selectGrupoAvaliado.append('<option data-codigo="' + this.codigo + '" value="' + this.id + '">' + this.descricao + '</option>')
                        });
                    }
                });
            }
            var $selectGrupoSalve = $("#sqGrupoSalveLote");
            if( $selectGrupoSalve.find('option').length < 2 ) {
                $.post(app.url + 'api/getTableApoio', {tableName: 'TB_GRUPO_SALVE'}).done(function (res) {
                    if (res.data.length > 0) {
                        $.each(res.data, function () {
                            $selectGrupoSalve.append('<option data-codigo="' + this.codigo + '" value="' + this.id + '">' + this.descricao + '</option>')
                        });
                    }
                });
            }
            var $selectSubgrupo = $("#sqSubgrupoAvaliadoLote");
            if( $selectSubgrupo.find('option').length < 2 ) {
                $.post(app.url + 'api/getTableApoio', {tableName: 'TB_SUBGRUPO'}).done(function (res) {
                    if (res.data.length > 0) {
                        $.each(res.data, function () {
                            $selectSubgrupo.append('<option data-grupo="'+this.pai.id+'" data-codigo="' + this.codigo + '" value="' + this.id + '">' + this.descricao + '</option>')
                        });
                    }
                });
            }
            ficha.filtrarSubgrupoLote();
        }
    },
    filtrarSubgrupoLote:function() {
        var $selectGrupoAvaliado = $("#sqGrupoAvaliadoLote");
        var $selectSubgrupo = $("#sqSubgrupoAvaliadoLote");
        $selectSubgrupo.val('');
        if ($selectGrupoAvaliado.val() == '') {
            $selectSubgrupo.prop('disabled', true)
        } else {
            $selectSubgrupo.prop('disabled', false);
            $selectSubgrupo.find('option[data-grupo]').each(function (i, item) {
                if ($(item).data('grupo') != $selectGrupoAvaliado.val()) {
                    $(item).hide();
                } else {
                    $(item).show();
                }
            });
        }
    },

    importarPlanilhaFichas: function(params) {
        $.jsPanel({
            container: "body",
            id: "ficha_importacao",
            theme: "#86d4ff",
            headerTitle: 'Importação de Planilha',
            contentSize: {
                width: $(window).width() - 100,
                height: window.innerHeight - 150
            },
            content: '<div id="div_content_ficha_importacao_planilha" style="border:none;display:block;min-height:100%;"<br><br><p>Carregando...' + window.grails.spinner + '</p></div>',
            resizable: true,
            dragit: {
                containment: 'parent',
                disableui: true,
            },
            callback: function() {
                this.find('.jsglyph-minimize').hide();
                this.content.css("padding", "15px");
                //this.content.prop('id','modal_ficha_importacao_planilha')
                app.loadModule(app.url + 'main/getModal', params, 'div_content_ficha_importacao_planilha', function(res) {
                    //$("#frmImportacaoFicha #divModalImportacaoFichas").data('params','sqCicloAvaliacao:'+$("#sqCicloAvaliacao").val() );
                    app.getGrid('modalImportacaoFichas', '', function() {
                        ficha.updatePercentualImportacao();
                    });
                }, '');
            },
        });
    },

    savePlanilhaImportacaoFicha: function(params) {
        // verificar se tem anexo
        if (!$("#frmImportacaoFicha #file").val()) {
            app.alertInfo('Nenhuma Planilha Selecionada! Clique no botão Procurar...');
            return;
        }

        var data = $("#frmImportacaoFicha").serializefiles();
        app.confirm('<br>Confirma a IMPORTAÇÃO ?', function() {
            // adicionar campos que não estão no formulário e são necessários
            data.append('sqCicloAvaliacao', params.sqCicloAvaliacao);
            data.append('contexto', params.contexto);

/*$.ajax({
  type: "POST",
  url: 'http://localhost:8080/salve-estadual/testes/timeout/120000',
  data: {},
  success: function(res){
    log( res)
  },
  error:function( xhr, msg,txt)
  {
    log( xhr )
    log('--------------')
    log( msg )
    log('--------------')
    log( txt )
    log('--------------')
  },
  dataType: 'TEXT'
});
*/

            app.ajax(app.url + 'main/savePlanilhaFicha', data,
                function(res) {
                    // limpar o campo anexo
                    if (res.status == 1) {
                        return;
                    }
                    if ($('#divGridModalImportacaoFichas').size() > 0) {
                        ficha.semCache = true;
                        ficha.list(); // atualizar o gride com as fichas importadas
                        ficha.semCache = false;
                        app.getGrid('modalImportacaoFichas'); // atualizar grid logs e inconsistências
                    }
                    if (res.status == 0) {
                        $("#frmImportacaoFicha input[type=file]").fileinput('clear');
                        var msg = [];
                        var size = Math.round(res.fileSize / 1024 / 1024);
                        var und = 'Mb';
                        if (size == 0) {
                            size = Math.round(res.fileSize / 1024);
                            und = "Kb";
                        }
                        msg.push('Arquivo processado: <b>' + res.fileName + '</b>');
                        msg.push('Tamanho do Arquivo: <b>' + size + und + '</b>');
                        msg.push('Planilha: <b>' + res.sheetName + '</b>');
                        msg.push('Linha(s): <b>' + (res.linhas) + '</b>');
                        if (res.colsNotFound.length > 0) {
                            msg.push('Coluna(s) Inválida(s): <b>' + res.colsNotFound.join(', ') + '</b>');
                        }
                        if (res.qtdLinhasComErro) {
                            msg.push('<span style="color:' + (res.qtdLinhasComErro > 0 ? '#dc0000' : '#009500') + '">' + res.qtdLinhasComErro + ' Linha(s) com erro.</span>');
                        }
                        var unidadeTempo = 'seg';
                        if (res.seconds > 60) {
                            res.seconds = res.seconds / 60;
                            unidadeTempo = 'min';
                        }
                        msg.push('Duração ' + Math.round(res.seconds) + ' ' + unidadeTempo)
                        app.alert(msg.join('<br>'), 'Resultado da Importação');
                    }
                }, /*'Processando. Aguarde..'*/ null, 'JSON'
            );
            // ativar cronometro
            app.blockUI('Salvando Histórico. Aguarde...');
            window.setTimeout(function() {
                // atualizar o gride histórico
                app.unblockUI();
                app.getGrid('modalImportacaoFichas', '', function() {
                    window.setTimeout(function() {
                        ficha.updatePercentualImportacao();
                    }, 2000)
                });
            }, 2000);

        }, null, data, 'Confirmação', 'warning');
    },
    deletePlanilha: function(params, btn) {
        app.selectGridRow(btn);
        app.confirm('<br>Confirma exclusão da Planilha ' + params.descricao + '?',
            function() {
                app.ajax(app.url + 'main/deletePlanilha',
                    params,
                    function(res) {
                        if (params.gride) {
                            app.getGrid(params.gride);
                        }
                    });
            },
            function() {
                app.unselectGridRow(btn);
            }, params, 'Confirmação', 'warning');
    },
    applyFilterGridFicha: function(contexto, idCampoNivel, idCampoTaxon) {
        //try{
        var coNivel = $("#" + idCampoNivel).val();
        var objData = $("#" + idCampoTaxon).select2('data')
        $("#" + contexto + " td").each(function() {
            var data = $(this).data('structure');
            if (data) {
                if (!objData[0] || data[coNivel] == objData[0].descricao) {
                    $(this).parent().show();
                } else {
                    $(this).parent().hide();
                }
            }
        });
        //} catch( e ){}
    },
    exportarPendencias:function( params,fld, evt ) {

        var container = params.container ? '#' + params.container.replace(/^#/, '') : '*';
        var idFichas = [];

        // ler as fichas marcadas
        $(container + " input:checkbox[name^=chkSqFicha]:checked").each(function () {
            idFichas.push(this.value);
        });

        var msgTitle = 'Marque esta opção para exportar as pendências somente das fichas selecionadas ou desmarque para exportar de todas as fichas do ciclo';
        var msgPergunta = 'Somente das ' + idFichas.length + ' fichas selecionadas?'
        if (idFichas.length == 1) {
            msgTitle = 'Marque esta opção para exportar as pendências da ficha selecionada ou desmarque para exportar de todas as fichas do cliclo';
            msgPergunta = 'Somente a da ficha selecionada?'
        }

        app.confirm('Confirma a exportação das pendências?' +
            (idFichas.length > 0 ?
                '<h3>Opções:</h3><label class="cursor-pointer">' +
                '<label class="cursor-pointer" title="' + msgTitle + '">' +
                '<input type="checkbox" id="chkPendenciaSelecionadas" checked="checked" class="checkbox-lg" value="S">&nbsp;&nbsp;' + msgPergunta + '</label>' : ''),
            function () {
                var todasPaginas = $("#chkPendenciaSelecionadas").is(':checked') ? 'N' : 'S';
                var data = {
                    sqFichaFiltro: (todasPaginas == 'N' ? idFichas.join(',') : ''),
                    sqCicloAvaliacao: $("#sqCicloAvaliacao").val(),
                };
                data = $.extend({},data, app.hasActiveFilter( container ) );
                app.ajax(app.url + 'ficha/exportPendencia', data, function (res) {

                    if (res.status == 0) {
                        window.setTimeout(function(){
                            getWorkers();
                        },5000);
                        window.setTimeout(function(){
                            getWorkers();
                        },60000);
                        app.growl('Exportação em andamento. Ao finalizar será exibida a tela para download do arquivo.');
                    }

                },'' , 'json');
                return false;
            },
            function () {
            },
            params, 'Confirmação', 'warning');
    },


    // exportacao csv utilizando novo app;grid()
    gridCsv:function( params ){
        var data = app.params2data( params,{});
        var gridId = data.gridId;
        // exibir dialog de confirmaçao com opção de informar o e-mail
        if( ! data.dlgConfirm ) {
            return app.confirmWithEmail('Confirma a exportação das fichas?',ficha.gridCsv,data)
        }
        app.grid(gridId,{format:'csv',email:currentUser.email},function( res ){
            if( res.msg ) {
                if( res.status==1) {
                    currentBoostrapModal = app.alert(res.msg, 'Aviso');
                } /*else {
                    currentBoostrapModal = app.alertInfo(res.msg + '<br><br>Clique em Fechar para continuar os trabalhos.', 'Exportando fichas...' + '&nbsp;<i class="fa fa-spinner fa-spin"></i>'
                        , getWorkers, null, 15);
                }*/
            }
            setTimeout(function(){getWorkers();},5000);
        });
    },

    // exportacao das fichas em pdf para arquivo zip utilizando novo app.grid()
    gridZipFichas:function( params ){
        var data = app.params2data( params,{});
        var gridId = data.gridId;
        //var titleDialog = '';
        data.format = data.format || 'pdf';
        if( ! data.dlgConfirm ) {
            // exibir dialog de confirmaçao com opção de informar o e-mail
            if (params.format.toLowerCase() == 'reg') {
                data.enviarEmail        = true;
                data.idsType            = 'ficha';
                data.showCheckCarencia  = true;
                data.showCheckSensivel  = true;
                if( window.grails.perfilExterno || (/CT|CO/.test(window.grails.coPerfil ) ) ) {
                    data.showCheckCarencia=false;
                    data.showCheckSensivel=false;
                }
                return app.dialogoExportarOcorrencias(data, ficha.gridZipFichas);
            } else {
                var mensagem = 'Confirma?'
                if( data.format == 'pen'){
                    mensagem = 'Confirma a exportação das pendências?';
                } else if( data.format == 'pdf' ){
                    mensagem = 'Confirma a exportação das fichas em PDF?';
                } else if( data.format == 'rtf' ){
                    mensagem = 'Confirma a exportação das fichas editáveis?';
                } else if( data.format == 'shp' ){
                    mensagem = 'Confirma a exportação dos shapefiles?';
                }
                return app.confirmWithEmail(mensagem, ficha.gridZipFichas, data)
            }
        }

        if( currentUser.email ) {
            data.email = currentUser.email
        }
        /*if( data.format == 'reg' ) {
            titleDialog = 'Exportando ocorrências...';
        } else if( data.format == 'pen' ) {
            titleDialog = 'Exportando pendências...';
        } else if( data.format == 'shp' ) {
            titleDialog = 'Exportando shapefiles...';
        } else {
            titleDialog = 'Exportando fichas...';
        }*/
        //app.grid(gridId,{format:data.format,email:currentUser.email},function( res ){
        app.grid(gridId,data,function( res ){
            if( res.msg ) {
                if( res.status == 1 ) {
                   currentBoostrapModal = app.alertInfo(res.msg);
                }
            }
            setTimeout(function(){getWorkers();},2000);
        });
    },




    exportarCsv: function(params, fld, evt) {
//******************************************************************************
        params = params || {};
        var container = params.container ? '#' + params.container.replace(/^#/, '') : '*';
        var idFichas = [];
        var totalFichas = 0;
        var gridId = params.gridId;
        var divTotal = $("#divGridFichasPaginationContent ul.pagination").data('totalRecords');
        totalFichas = parseInt("0" + divTotal );

        // ler as fichas marcadas
        $(container + " input:checkbox[name^=chkSqFicha]:checked").each(function () {
            idFichas.push(this.value);
        });

        // validar limite máximo
        if( idFichas.length > 0 ){
            totalFichas = idFichas.length;
        }

        if( totalFichas > maxFichasExportacao ){
            app.alertInfo('Limite máximo para exportação de fichas é de maxFichasExportacao fichas.<br>Utilize os filtros para reduzir a quantidade da lista ou marque as fichas desejadas.');
            return;
        }
        var msgTitle = 'Marque esta opção para exportar as fichas selecionadas ou desmarque para exportar todas as fichas do ciclo';
        var msgPergunta = 'Somente as ' + idFichas.length + ' fichas selecionadas?'
        if (idFichas.length == 1) {
            msgTitle = 'Marque esta opção para exportar a ficha selecionada ou desmarque para exportar de todas as fichas do cliclo';
            msgPergunta = 'Somente a ficha selecionada?'
        }

        app.confirm('Confirma a exportação do gride para planilha?' +
            (idFichas.length > 0 ?
                '<h3>Opções:</h3><label class="cursor-pointer">' +
                '<label class="cursor-pointer" title="' + msgTitle + '">' +
                '<input type="checkbox" id="chkCsvSelecionadas" checked="checked" class="checkbox-lg" value="S">&nbsp;&nbsp;' + msgPergunta + '</label>' : ''),
            function () {
                var todasPaginas = $("#chkCsvSelecionadas").is(':checked') ? 'N' : 'S';
                var data = {
                    sqFichaFiltro: (todasPaginas == 'N' ? idFichas.join(',') : ''),
                    sqCicloAvaliacao: $("#sqCicloAvaliacao").val(),
                };


                data = $.extend({},data, app.hasActiveFilter( container ) );
                /*if( data.count == 0 )
                {
                    var divFiltro = $('div[id^=divFiltrosFicha]');
                    if( divFiltro.size() > 0 ) {
                        $.extend(data, app.hasActiveFilter( $( divFiltro[0] ).attr('id') ) );
                    }
                }
                */
                // validar limite máximo novamente de maxFichasExportacao fichas por vez
                if( data.sqFichaFiltro == '' ) {
                    totalFichas = parseInt("0" + $("#divGridFichasPaginationContent ul.pagination").data('totalRecords') );
                    if( totalFichas > maxFichasExportacao ){
                        window.setTimeout(function(){
                            app.alertInfo('Limite máximo para exportação de fichas é de maxFichasExportacao fichas.<br>Utilize os filtros para reduzir a quantidade da lista ou marque as fichas desejadas.');
                        },1000);
                        //app.growl('Limite máximo para exportação de fichas é de maxFichasExportacao fichas.<br>Utilize os filtros para reduzir a quantidade da lista ou marque as fichas desejadas.');
                        return false;
                    }
                }

                app.ajax(app.url + 'ficha/exportCsv', data, function (res) {

                    if (res.status == 0) {
                        window.setTimeout(function(){
                            getWorkers();
                        },5000);
                        window.setTimeout(function(){
                            getWorkers();
                        },60000);
                        app.growl('Exportação em andamento. Ao finalizar será exibida a tela para download do arquivo.');
                    }

                },'' , 'json');
                // fichaDisOcorrencia/exportCsv/${params.sqFicha}
                return false;
            },
            function () {
            },
            params, 'Confirmação', 'warning');

//******************************************************************************
    },
    /*exportarCsvOld: function(params, fld, evt) {
        app.confirm('<br>Confirma Exportação para planilha?',
            function() {
                ficha.list(params, fld, evt);
                return false;
            },
            function() {},
            params, 'Confirmação', 'warning');
    },*/

    /**
     * função para exportação dos registros de ocorrências da(s) ficha(s) no formato planilha (xlsx)
     * @param params
     * sqCicloAvaliacao - Obrigatório
     * sqFicha          - não obrigatório. Pode ser sqFicha ou array de sqFichas.
     * divFiltros       - não obrigatório. Id da div (wrapper) dos filtros das fichas.
     * divGrid          - não obrigatório. Id da div (wrapper) do gride de fichas.
     * contexto         - nao obrigatório. Nome do módulo que está executando a exportação. Ex: ficha, abaDistribuicao etc.
     * @param fld       - não obrigatório. Botão/elemento clicado.
     * @param evt       - não obrigatório. Evento javascript padrão.
     */
    exportarOcorrencias:function( params, fld, evt ) {
        params = params || {};
        delete params.params
        var idFichas = [];
        var data={};
        var idContainerFiltrosFicha=null;
        var totalFichas = 0;
        if( !params.sqCicloAvaliacao ){
            app.growl('Necessário informar o ciclo de avaliação.');
            return;
        }
        data.sqCicloAvaliacao   = params.sqCicloAvaliacao;
        data.exportarOcorrencias= 'S';
        data.idsType            = 'ficha';
        data.showCheckCarencia  = true;
        data.showCheckSensivel  = true;
        data.enviarEmail        = true;
        data.contexto           = params.contexto || 'ficha';
        data.format             = 'reg' // registros de ocorrência

        // se for informada uma ficha especifica não precisa utilizar os filtros ativos
        // nem ler as fichas marcadas no gride
        if( ! params.sqFicha ) {
            if( params.divGrid ) {
                // ler as fichas marcadas no gride
                $("#containerGridFichas input:checkbox[name^=chkSqFicha]:checked").each(function () {
                    idFichas.push(this.value);
                });
                totalFichas = idFichas.length;
            }
            // se não marcou fichas no gride, ler os fitros e o total de fichas no topo do gride
            // caso contrário desconsiderar os filtros ativos
            if ( totalFichas == 0 ) {
                // se não for selecionada fichas para exportar, avisar o limite máximo permitido
                totalFichas = parseInt("0" + $("#" + params.divGrid + "TotalRecords" ).text() );
                if ( params.divFiltros ) {
                    try {
                        idContainerFiltrosFicha = $("div[id^=" + params.divFiltros + "]").attr('id');
                        data = $.extend({}, data, app.hasActiveFilter(idContainerFiltrosFicha));
                    } catch (e) {
                    }
                }
            }
        } else {
            if( typeof( params.sqFicha ) !== 'object') {
                idFichas = params.sqFicha.split(',');
            } else {
                idFichas = params.sqFicha;
            }
        }

        // limite máximo de fichas para exportação de ocorrências
        if (totalFichas > maxFichasExportacaoOcorrencias ) {
            app.alertInfo('Limite máximo para exportação dos registros de ocorrências é de '+maxFichasExportacaoOcorrencias+' fichas.<br>Utilize os filtros para reduzir a quantidade da lista ou marque as fichas desejadas.');
            return;
        }

        if( window.grails.perfilExterno || (/CT|CO/.test(window.grails.coPerfil ) ) ) {
            data.showCheckCarencia=false;
            data.showCheckSensivel=false;
        }
        app.blockUI('Aguarde...');
        // verificar se existe pelo menos um registro marcado como sensivel para mostrar o campo checkbox na janela de dialogo
        $.ajax({
            url: app.url + 'ficha/hasSensitiveOccurrence'
            , type: 'POST'
            , cache: false
            , timeout: 0
            , dataType: 'json'
            , data: { sqCicloAvaliacao:data.sqCicloAvaliacao, ids : idFichas.join(','), idsType:data.idsType }
        }).done(function ( res ) {
            app.unblockUI();
            data.sensivel = res.data.result;
            data.qtdFichas = idFichas.length;
            app.dialogoExportarOcorrencias(data,function( data ) {
                //data.ids        = idFichas.join(',');
                //app.ajax(app.url + 'ficha/list', data, function(res) {
                data.exportIds        = idFichas.join(',');
                app.ajax(app.url + 'ficha/getGridFicha', data, function(res) {
                    if (res.status == 0 ) {
                        window.setTimeout(function(){
                            getWorkers();
                        },5000);
                        window.setTimeout(function(){
                            getWorkers();
                        },60000);

                        setTimeout(function() {
                            currentBoostrapModal = app.alertInfo(res.msg + '<br><br>Clique em Fechar para continuar os trabalhos.'
                                , 'Exportando ocorrências&nbsp;<i class="fa fa-spinner fa-spin"></i>'
                                , getWorkers, null, 15);
                        },1000);

                        /*app.growl('Ao finalizar será exibida a tela para baixar o arquivo' +
                            (data.enviarPorEmail == 'S' ? ' que também será enviado para o seu e-mail' : '') + '.'+
                            '<br><br>Calculando registros...',8,'Exportação em andamento.','tc','large','info');

                         */
                    };
                }, '', 'json');
            });
        });
    },

    print: function(params) {
        // utilizar a função print em fichaCompleta.js
        print(params);
    },

    selAbaResultadoAvaliacao:function( params ) {
        if ($("#ulTabsAvaliacao").size() == 0) {
            app.selectTab("tabAvaliacao"); // carregar a ab 11
            window.setTimeout(function (params) {
                ficha.selAbaResultadoAvaliacao(params);
            }, 2000);
        } else {
            if ( $("#tabAvaResultado").size() == 0) {
                window.setTimeout(function (params) {
                    ficha.selAbaResultadoAvaliacao(params);
                }, 1000);
            } else {
                if( $("#tabAvaResultado").find('*[id^=dsCitacao]').size() == 0) {
                    app.selectTab("tabAvaResultado") // selecionar a aba ocorrencias
                    window.setTimeout(function (params) {
                        ficha.selAbaResultadoAvaliacao(params);
                    }, 1000);
                } else {
                    if( ! $("#liTabAvaResultado").is(":visible") ){
                        app.selectTab("tabAvaliacao");
                    }
                    window.setTimeout(function (params) {
                        $(document).scrollTop(999999);
                        $(document).scrollTop($(document).scrollTop() - 144);
                    }, 1000);
                }
            }
        }
    },
    publicarVersao:function( sqFichaVersao,nuVersao ) {
        if( event ){event.preventDefault();}
        var checked = $("#tableFichaVersoes tbody tr#trVersao" + sqFichaVersao + ' td:first').hasClass('bgGreenPastel');
        var msg = checked ? 'Confirma o CANCELAMENTO do publicação da versão '+nuVersao+'?' :
                        'Confirma a PUBLICAÇÃO da versão '+nuVersao+'?<br><span class="blinking-red">As informações da ficha ficarão visíveis para o público.</span>';
        app.confirm(msg,function(){
            app.publicarVersao(sqFichaVersao,function(res){
                $("#tableFichaVersoes tbody tr td").removeClass('bgGreenPastel');
                $("#tableFichaVersoes tbody tr#trVersao" + sqFichaVersao + " input[type=radio]").prop('checked', false);
                if( res.data.stPublico ) {
                    $("#tableFichaVersoes tbody tr#trVersao" + sqFichaVersao + ' td:first').addClass('bgGreenPastel');
                    $("#tableFichaVersoes tbody tr#trVersao" + sqFichaVersao + " input[type=radio]").prop('checked', true);
                };
            });
        })
    },

};

// TAXONOMIA


var taxonomia = {
    sqGrupoChange: function(params, data, event, limpar) {

        // atualizar o formulário da aba 2.1-Distribuição ( batimetria e altitude)
        if (typeof(distribuicao.tabDistribuicaoInit) == 'function') {
            distribuicao.tabDistribuicaoInit(params, data, event, limpar);
        }
        if (typeof(historiaNatural.setModoReproducao) == 'function') {
            historiaNatural.setModoReproducao();
        }
        // limpar subgrupo
        $("#sqSubgrupo").val(null).trigger('change');
        taxonomia.setSubgrupo();
        // localizar o ponto focal
        var $selectPF = $("select[name=sqPontoFocal]");
        var $selectCT = $("select[name=sqCoordenadorTaxon]");
        var sqGrupo = $("#sqGrupo").val();
        var sqCicloAvaliacao = $("#sqCicloAvaliacao").val();
        if ( sqGrupo && sqCicloAvaliacao ) {
            var data = {sqCicloAvaliacao: sqCicloAvaliacao, sqGrupo: sqGrupo};
            if ($selectPF.size() == 1) {
                $selectPF.val('');
                app.ajax(app.url + 'fichaTaxonomia/findPontoFocal', data, function (res) {
                    if (res && res.data && res.data.sqPessoa) {
                        /*if ($selectPF.find('option[value=' + res.data.sqPessoa + ']').size() == 1) {
                            $selectPF.val(res.data.sqPessoa);
                        }*/
                        app.setSelect2("sqPontoFocal",
                            {
                                id: res.data.sqPessoa
                                , text: res.data.noPessoa
                            }
                            , res.data.noPessoa);
                    }
                });
            }

            if ($selectCT.size() == 1) {
                $selectCT.val(null).trigger('change'); // limpar instituicao
                taxonomia.sqCoordenadorTaxonChange();
                app.ajax(app.url + 'fichaTaxonomia/findCoordenadorTaxon', data, function (res) {
                    if (res && res.data && res.data.sqPessoa) {
                        app.setSelect2("sqCoordenadorTaxon",
                            {
                                id: res.data.sqPessoa
                                , text: res.data.noPessoa
                            }
                            , res.data.noPessoa);
                    }
                });
            }
        }
    },
    setSubgrupo:function()    {
        // limpar e inicializar o subgrupo
        var $grupo = $("#sqGrupo");
        var $subgrupo = $("#sqSubgrupo");
        var currentGrupo = $subgrupo.data('current-grupo');
        var currentValue = $subgrupo.data('current-value');
        var currentText  = $subgrupo.data('current-text');
        if( currentGrupo == $grupo.val() && currentValue ) {

            if ($subgrupo.find('option').filter(function (index, option) {
                return parseInt(option.value) == parseInt(currentValue);
            }).length == 0) {
                var newOption = new Option(currentText, currentValue, false, false);
                $subgrupo.append(newOption);
            } else {
                $subgrupo.val(currentValue).trigger('change');
            }
        }
    },
    sqSubgrupoChange:function() {
        app.fieldChange( $("#sqSubgrupo")[0] );
    },
    sqCoordenadorTaxonChange: function(params, data, event, limpar) {
        var $selectCT = $("select[name=sqCoordenadorTaxon]");
        var $selectInstituicao = $("select[name=sqWebInstituicaoCT]");

        if( $selectCT.size() == 1 ) {
            $selectInstituicao.val(null).trigger('change');
            if( $selectCT.val() ) {
                $selectInstituicao.parent().show()
                // encontrar a instituicao do CT
                var data = { sqCicloAvaliacao:$("#sqCicloAvaliacao").val(),sqPessoa:$selectCT.val()};
                app.ajax(app.url + 'fichaTaxonomia/findInstituicaoCT', data, function (res) {
                    if ( res.status === 0 ) {
                        app.setSelect2("sqWebInstituicaoCT",
                            {id:res.data.sqWebInstituicao
                                , text: res.data.noWebInstituicao}
                            ,res.data.noWebInstituicao);
                    }
                });
            } else {
                $selectInstituicao.parent().hide()
            }
        }
    },
    saveFrmTaxonomia: function(params) {
        // validação inicial
        if (!$("#sqCicloAvaliacao").val()) {
            app.alertError('Necessário informar o ciclo de avaliação!')
            return;
        }
        if (!$("#sqTaxon").val()) {
            app.alertError('Necessário informar uma espécie ou subespecie!')
            return;
        }

        // identificar a operação submetida
        var operacao = ($("#sqFicha").val() ? 'EDICAO' : 'INCLUSAO');
        // serializar o formulário da aba taxonomia
        var data = $('#frmTaxonomia').serializeArray();
        // ler o sq da ficha atual se houver
        var sqFicha = $("#sqFicha").val();
        // adicionar os campos obrigatorios que que estão fora do frmTaxonomia
        data.push({
            'sqFicha': $("#sqFicha").val()
        });
        data.push({
            'sqCicloAvaliacao': $("#sqCicloAvaliacao").val()
        });
        data.push({
            'dsNotasTaxonomicas': $("#dsNotasTaxonomicas").val()
        });
        data.push({
            'dsDiagnosticoMorfologico': $("#dsDiagnosticoMorfologico").val()
        });
        data.push({
            'sqGrupo': $("#sqGrupo").val()
        });
        if( params.dsJustificativa ){
            data.push({'dsJustificativa':params.dsJustificativa} );
        };
        if (operacao == 'INCLUSAO') {

            // se o campo da unidade estiver visivel e não tiver preenchido mostrar mensagem
            if( $("#frmTaxonomia #sqUnidadeOrg").is(':visible') ) {
                if( ! $("#frmTaxonomia #sqUnidadeOrg").val() ) {
                    app.alertError('Necessário informar a unidade responsável!')
                    return;
                }
            }
            // na criação da ficha o nome científico será o da espécie ou subespecie selecionada
            data.push({
                'nmCientifico': ($("#w_idSubespecie").select2('data')[0] ? $("#w_idSubespecie").select2('data')[0].descricao : $("#w_idEspecie").select2('data')[0].descricao)
            });
            if( ! $("#sqGrupo").val() ){
                app.alertInfo('Necessário informar o grupo avaliado!')
                return;
            }
            if( $("#sqCoordenadorTaxon").val() && ! $("#sqWebInstituicaoCT").val() ){
                app.alertInfo('Necessário informar a instituição do coordenador de taxon!')
                return;
            }
            data.push({
                'sqCoordenadorTaxon': $("#sqCoordenadorTaxon").val()
            });
            data.push({
                'sqWebInstituicaoCT': $("#sqWebInstituicaoCT").val()
            });
        }
        var ajaxLoading='Gravando...'

        // gravar ficha
        app.ajax(app.url + 'fichaTaxonomia/save', data, function(res) {


            if( res.data && res.data.stInformarJustificativa ) {
                // aguardar o ajax finalizar e detravar a tela
                setTimeout( function() {
                    var dataModal = {};
                    dataModal.id = sqFicha;
                    dataModal.modalName = 'textoLivre';
                    dataModal.label = 'Justificativa';
                    dataModal.maxLength = 500;
                    modal.janelaTextoLivre({
                        title: 'Justificativa para a inclusão de subespécie de espécie ameaçada',
                        'url': '/main/getModal',
                        'data': dataModal,
                        'height': 300,
                        'width': 800,
                        'theme': '#00f',
                        'resizeble': false,
                        'callback':function(){
                           //
                        },
                        'onClose': function(panel) {
                            var $textArea = panel.content.find('textarea#fldTextoLivre' + dataModal.id);
                            if ($textArea.size() == 1) {
                                var dsJustificativa = $.trim($textArea.val());
                                if( dsJustificativa) {
                                    taxonomia.saveFrmTaxonomia({dsJustificativa: dsJustificativa});
                                }
                                return;
                            }
                        }
                    });
                },600);
                return;
            }

            app.resetChanges('frmTaxonomia');

            // atualizar os metadados do subgrupo
            var $subgrupo = $("#sqSubgrupo");
            var $grupo = $("#sqGrupo");
            $subgrupo.data('current-grupo',$grupo.val() );
            $subgrupo.data('current-value', $subgrupo.val() );
            $subgrupo.data('current-text', $subgrupo.text().trim() );

            if (operacao == 'INCLUSAO') {
                // ao retornar o da gravação, alimentar o campo sqFicha da página e habilitar os demais campos/abas etc
                if (res.data['sqFicha']) {
                    $("#sqFicha").val(res.data['sqFicha']);
                    ficha.edit({
                        'sqFicha': res.data['sqFicha'],
                        'noCientifico': res.data['noCientifico']
                    });
                } else {
                    return; // ocorreu algum erro que o sqFicha não foi gerado
                }
            } else {
                // limpar os campos da aba 2.1-distribuição de acordo com o grupo selecionado
                taxonomia.sqGrupoChange(null, null, null, true);
            }
        }, ajaxLoading, 'json');
    },
    showFrmTaxonomia: function() {
        //$('#divSearchTaxonTree').collapse('hide');
        $("#ulTabsFicha>li:gt(0),#divNomeComum,#divSinonimia,#divFooterTaxonomia,#divNotaMorfologica,#divNotaTaxonomica").toggle();
        $("#frmNomeComum,#frmSinonimia").toggle();
        $("#divBreadcrumb").show();
        // se nao tiver subespecie, esconer o campo
        if (!$("#w_idSubespece").select2('data')) {
            $("#w_idSubespece").parent().hide()
        }
        $("#btnSaveFrmTaxonomia").html('Gravar');
        // proteger os campos que não poderão ser editados após a gravação
        $("#divSearchTaxonTree select").prop('disabled', true)
            // exibir os nomes comuns e sinonímias existentes para a espécie selecionada
        taxonomia.updateGridNomeComum();
        taxonomia.updateGridSinonimia();

        var noCientifico = app.ellipseAutores( $("#divNmCientifico").html() );
        /*
        // exibir o nome cientifico da ficha selecionada no header
        var noCientifico = $("#divNmCientifico").html().trim();
        // parte 1 = no cientifico
        // parte 2 = autores
        var noCientificoParts = noCientifico.split('</i>');
        if( noCientificoParts[1]) {
            var qtdAutores = noCientificoParts[1].replace(/, ?[0-9]{4}/, '').replace('(', '').replace(')', '').split(',').length;
            if ( qtdAutores > 1 ) {
                noCientifico = noCientificoParts[0] + '</i>&nbsp;<i class="fa fa-commenting-o autores-ellipses" title="' + String(noCientificoParts[1]).trim() + '"></i>'
            }
        }*/

        $("#spanNomeCientificoFicha").html(noCientifico);
        $("#spanBtnAlterarTaxon").show();

        $("#sqGrupo option").show(); // exibir todos os grupos
        var reino = $("#breadcrumb_nivel_1").html().toUpperCase();
        var filo = $("#breadcrumb_nivel_2").html().toUpperCase();
        var classe = $("#breadcrumb_nivel_3").html().toUpperCase();
        /*if (!$("#sqGrupo").val()) {
            if (classe == 'AVES') {
                $("#sqGrupo option[data-codigo=AVES]").prop('selected', true)
            } else if (classe == 'MAMMALIA') {
                $("#sqGrupo option[data-codigo=MAMIFEROS]").prop('selected', true)
            } else if (classe == 'REPTILIA') {
                $("#sqGrupo option[data-codigo=REPTEIS]").prop('selected', true)
            } else if (classe == 'AMPHIBIA') {
                $("#sqGrupo option[data-codigo=ANFIBIOS]").prop('selected', true)
            }
        }*/
        // filtrar o campo Grupo de acordo com a classe/filo
        if (reino == 'ANIMALIA') {
            /*
            if (/ACTINOPTERYGII|SARCOPTERYGII|ELASMOBRANCHII|HOLOCEPHALI|MYXINICLASSE/.test(classe)) {
                $("#sqGrupo option").each(function() {
                    if (!/PEIXE|ELASMO/.test($(this).data('codigo'))) {
                        $(this).hide();
                    }
                });
            } else if (filo != 'CHORDATA') {
                $("#sqGrupo option").each(function() {
                    if (!/ABELHAS|ARACNIDEOS|COLEMBOLAS|COLEOPTERAS|FORMIGAS|INVERTEBRADOS_TERRESTRES|BORBOLETAS|MARIPOSAS|MIRIAPODAS|OLIGOQUETAS|MOLUSCOS_TERRESTRES'/.test( $(this).data('codigo') ) ) {
                    //if (!/INVERTEBRADOS_TERRESTRES|INVERTEBRADOS_MARINHOS|INVERTEBRADOS_AGUA_DOCE/.test($(this).data('codigo'))) {
                        $(this).hide();
                    }
                });
            }*/
        }
        // inicilizar select2 com o subgrupo atual
        taxonomia.setSubgrupo();
        /*
            Quando “Classe” = Actinopterygii, Sarcopterygii, Elasmobranchii, Holocephali ou Myxini, as opções são
            “Peixes Marinhos” e Peixes de água doce”
            Quando “Filo” ≠ “Chordata”, as opções são “Invertebrados terrestres”, “Invertebrados    marinhos” e “Invertebrados de água doce”
         */

    },
    editNomeComum: function(params, btn) {
        app.ajax(app.url + 'fichaTaxonomia/editNomeComum', params,
            function(res) {
                $("#btnResetFrmNomeComum").removeClass('hide');
                $("#btnSaveFrmNomeComum").data('value', $("#btnSaveFrmNomeComum").html());
                $("#btnSaveFrmNomeComum").html("Gravar Alteração");
                app.setFormFields(res, 'frmNomeComum');
                app.selectGridRow(btn);
                if (!$("#noComum:visible").size()) {
                    $("#btnToggleNomeComum").click()
                }
                app.focus('noComum');
            });
    },
    resetFrmNomeComum: function(params) {
        app.reset('frmNomeComum');
        $("#btnResetFrmNomeComum").addClass('hide');
        $("#btnSaveFrmNomeComum").html($("#btnSaveFrmNomeComum").data('value'));
        app.unselectGridRow('divGridNomeComum');
        app.focus('noComum');
    },
    saveFrmNomeComum: function(params) {
        // serializar o formulário do nome comum
        var data = $('#frmNomeComum').serializeArray();
        data = app.params2data(params, data);
        // ler o sq da ficha atual se houver
        if (!data.sqFicha) {
            app.alertError('ID da ficha não encontrado!');
            return;
        }
        if (!data.noComum) {
            app.alertError('Nome comum não informado!');
            app.focus('noComum');
            return;
        }
        if ($("#frmNomeComum").valid()) {
            // identificar a operação se é edição ou inclusao
            var operacao = (data.sqFichaNomeComum > 0 ? 'EDICAO' : 'INCLUSAO');
            // gravar nome comum
            app.ajax(app.url + 'fichaTaxonomia/saveNomeComum', data, function(res) {
                if (operacao == 'EDICAO') {
                    $("#btnToggleNomeComum").click()
                }
                //$("#sqFichaNomeComum").val(res.data.sqFichaNomeComum);
                taxonomia.resetFrmNomeComum();
                taxonomia.updateGridNomeComum();
            });
        }
    },
    editSinonimia: function(params, btn) {
        app.ajax(app.url + 'fichaTaxonomia/editSinonimia', params,
            function(res) {
                $("#btnResetFrmSinonimia").removeClass('hide');
                $("#btnSaveFrmSinonimia").data('value', $("#btnSaveFrmSinonimia").html());
                $("#btnSaveFrmSinonimia").html("Gravar Alteração");
                app.setFormFields(res, 'frmSinonimia');
                app.selectGridRow(btn);
                if (!$("#noSinonimia:visible").size()) {
                    $("#btnToggleSinonimia").click();
                }
                app.focus('noSinonimia');
            });
    },
    resetFrmSinonimia: function(params) {
        app.reset('frmSinonimia');
        $("#btnResetFrmSinonimia").addClass('hide');
        $("#btnSaveFrmSinonimia").html($("#btnSaveFrmSinonimia").data('value'));
        app.unselectGridRow('divGridSinonimia');
        app.focus('noSinonimia');
    },
    saveFrmSinonimia: function(params) {
        // serializar o formulário do nome comum
        var data = $('#frmSinonimia').serializeArray();
        data = app.params2data(params, data)
            // ler o sq da ficha atual se houver
        if (!data.sqFicha) {
            app.alertError('ID da ficha não encontrado!')
            return;
        }
        if (!data.noSinonimia) {
            app.alertError('Nome antigo não informado!')
            app.focus('noSinonimia');
            return;
        }

        if (!data.noAutor) {
            app.alertError('Autor não informado!')
            app.focus('noAutor');
            return;
        }

        /*if (!data.nuAno) {
            app.alertError('Ano não informado!')
            app.focus('nuAno');
            return;
        }
        */

        if ($("#frmSinonimia").valid()) {
            // identificar a operação se é edição ou inclusao
            var operacao = (data.sqFichaSinonimia > 0 ? 'EDICAO' : 'INCLUSAO');
            // gravar nome comum
            app.ajax(app.url + 'fichaTaxonomia/saveSinonimia', data, function(res) {
                if (operacao == 'EDICAO') {
                    $("#btnToggleSinonimia").click();
                }
                $("#sqFichaSinonimia").val(res.data.sqFichaSinonimia);
                taxonomia.resetFrmSinonimia();
                taxonomia.updateGridSinonimia();
            });
        }
    },
    updateGridNomeComum: function() {
        ficha.getGrid('nomeComum');
    },
    updateGridSinonimia: function() {
        ficha.getGrid('sinonimia');
    },
    deleteNomeComum: function(data) {
        app.confirm('<br>Confirma exclusão do <b>' + data.nomeComum + '</b>?', function() {
            app.ajax(app.url + 'fichaTaxonomia/deleteNomeComum', data, function(res) {
                taxonomia.updateGridNomeComum();
            });
        }, null, data, 'Confirmação', 'warning');
    },
    deleteSinonimia: function(data) {
        app.confirm('<br>Confirma exclusão do nome antigo <b>' + data.sinonimia + '</b>?', function() {
            app.ajax(app.url + 'fichaTaxonomia/deleteSinonimia', data, function(res) {
                taxonomia.updateGridSinonimia();
            });
        }, null, data, 'Confirmação', 'warning');
    },
};

// ################################################################################################################################ //
// 2.6.1 - GEOREFERENCIA                                                                                                            //
// ################################################################################################################################ //
var disOcoGeoreferencia = {
    map: null,
    mapInit: function(pMap) {
        map = pMap;
        map.on('click', function(evt) {
            if (evt && evt.coordinate) {
                evt.coordinate = ol.proj.toLonLat(evt.coordinate, 'EPSG:3857');
                $('#vlLatitude').val(evt.coordinate[1].toPrecision(8));
                $('#vlLongitude').val(evt.coordinate[0].toPrecision(8));
                disOcoGeoreferencia.addMarker(evt.coordinate);
            }
        });
        // adicionar marker no mapa com latitude e longitude vindo da edição do registro
        var data = {}
        data.vlLatitude = $('#vlLatitude').val();
        data.vlLongitude = $('#vlLongitude').val();
        disOcoGeoreferencia.viewPoint(data);
    },
    viewPoint: function(params) {
        var latLong = []
        if (params.vlLatitude && params.vlLongitude) {
            latLong.push(parseFloat(params.vlLongitude));
            latLong.push(parseFloat(params.vlLatitude));
            disOcoGeoreferencia.addMarker(latLong);
        }
    },
    removeMarkers: function() {
        $.each(map.getOverlays().getArray(), function(index, val) {
            map.removeOverlay(val);
        });
    },
    addMarker: function(coordinates) {
        disOcoGeoreferencia.removeMarkers();
        var img = document.createElement('img');
        img.src = window.grails.marker;
        img.width = 40;
        img.height = 40;
        map.addOverlay(new ol.Overlay({
            position: ol.proj.fromLonLat(coordinates),
            element: img,
            positioning: 'bottom-center'
        }));
        map.getView().setCenter(ol.proj.fromLonLat(coordinates));
        map.getView().setZoom(10);
    },
    saveFrmDisOcoGeoreferencia: function(params) {
        if (!$('#frmDisOcoGeoreferencia').valid()) {
            return false;
        }
        var data = $("#frmDisOcoGeoreferencia").serializeArray();
        data = app.params2data(params, data);
        if (!data.sqFicha) {
            app.alertError('ID da ficha não encontrado!');
            return;
        }
        if (!disOcoGeoreferencia.testCoordenada()) {
            return;
        }
        app.ajax(app.url + 'fichaDisOcorrencia/saveFrmDisOcoGeoreferencia', data, function(res) {
            if (!res['status']) {
                ocorrencia.updateGridOcorrencia();
                $('#sqFichaOcorrencia').val(res['data']['sqFichaOcorrencia']);
            }
        }, null, 'JSON');
    },
    testCoordenada: function(data) {
        // http://stackoverflow.com/questions/3518504/regular-expression-for-matching-latitude-longitude-coordinates
        var latLngRegex = /^[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?),\s*[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?)$/;
        var lat = $('#vlLatitude').val();
        var lng = $('#vlLongitude').val();
        if (lat && lng) {
            var latLng = lat + ', ' + lng;
            if (!latLngRegex.test(latLng)) {
                app.alertError('Coordenadas inválidas');
                return false;
            }
        } else {
            app.alertError('É necessário informar latitude e longitude');
            return false;
        }
        return true;
    },
    clearForm: function() {
        $('#frmDisOcoGeoreferencia').get(0).reset();
        $("#frmDisOcoGeoreferencia select.select2").each(function() {
            $(this).select2().val("?");
        });
        disOcoGeoreferencia.removeMarkers();
    }
};

// ################################################################################################################################ //
// 2 - Dados do Registro                                                                                                            //
// ################################################################################################################################ //
var disOcoDadosRegistro = {
    sqNivelOcoDadosDoRegistroChange: function(params) {
        $('#sqTaxonCitado').empty();
        if ($('#nivelOcoDadosDoRegistro').val()) {
            $('#sqTaxonCitado').prop('disabled', false);
        } else {
            $('#sqTaxonCitado').prop('disabled', true);
        }
    },

    saveFrmDisOcoRegistro: function(params) {
        if (!$('#frmDisOcoRegistro').valid()) {
            return false;
        }
        var data = $('#frmDisOcoRegistro').serializeArray();
        data = app.params2data(params, data);
        if (!data.sqFicha) {
            app.alertError('ID da ficha não encontrado!');
            return;
        }
        app.ajax(app.url + 'fichaDisOcorrencia/saveFrmDisOcoRegistro', data, function(res) {
            if (!res['status']) {
                ocorrencia.updateGridOcorrencia();
            }
        }, null, 'json');
    },

    disabledInputTaxon: function() {
        if ($('#sqTaxonCitado').val()) {
            $('#sqTaxonCitado').prop('disabled', false);
        }
    },
};

// ################################################################################################################################ //
// 2 - Distribuição                                                                                                                 //
// ################################################################################################################################ //
var distribuicao = {
    saveFrmDistribuicao: function(params) {
        if (!$('#frmDistribuicao').valid()) {
            return false;
        }
        var data = $("#frmDistribuicao").serializeArray();
        data = app.params2data(params, data);
        if (!data.sqFicha) {
            app.alertError('ID da ficha não encontrado!');
            return;
        }
        app.ajax(app.url + 'fichaDistribuicao/saveFrmDistribuicao', data, function(res) {}, null, 'json');
    },
};

// ################################################################################################################################ //
// 3 - Historia Natural
// ################################################################################################################################ //
var historiaNatural = {
    init: function() {},
    savefrmHistoriaNatural: function(params) {
        if (!$('#frmHistoriaNatural').valid()) {
            return false;
        }
        var data = $("#frmHistoriaNatural").serializeArray();
        data = app.params2data(params, data);
        if (!data.sqFicha) {
            app.alertError('ID da ficha não encontrado!');
            return;
        }
        app.ajax(app.url + 'fichaHistoriaNatural/saveFrmHistoriaNatural', data,
            function(res) {}, null, 'json');
    },
    savefrmHistNatPosicaoTrofica: function(params) {
        if (!$('#frmHisNatPosicaoTrofica').valid()) {
            return false;
        }
        var data = $("#frmHisNatPosicaoTrofica").serializeArray();
        data = app.params2data(params, data);
        if (!data.sqFicha) {
            app.alertError('ID da ficha não encontrado!');
            return;
        }
        app.ajax(app.url + 'fichaHistoriaNatural/saveFrmHisNatPosicaoTrofica', data,
            function(res) {}, null, 'json');
    },
    //--------------------------------------------------------------------------
    //                           ABA HABITO ALIMENTAR
    //--------------------------------------------------------------------------
    tabHabitoAlimentarInit: function(data) {
        historiaNatural.updateGridHabitoAlimentar();
        historiaNatural.stHabitoAlimentEspecialistaChange(); // mostrar/esconder o
        // form Especialista
    },
    saveFrmHisNatHabitoAlimentar: function(params) {
        if (!$('#frmHisNatHabitoAlimentar').valid()) {
            return false;
        }
        var data = $("#frmHisNatHabitoAlimentar").serializeArray();
        data = app.params2data(params, data);
        if (!data.sqFicha) {
            app.alertError('ID da ficha não encontrado!');
            return;
        }
        app.ajax(app.url + 'fichaHistoriaNatural/saveFrmHisNatHabitoAlimentar',
            data,
            function(res) {
                historiaNatural.resetFrmHisNatHabitoAlimentar();
                historiaNatural.updateGridHabitoAlimentar();
            }, null, 'json');
    },
    editHabitoAlimentar: function(params) {
        app.ajax(app.url + 'fichaHistoriaNatural/editHabitoAlimentar', params,
            function(res) {
                $("#btnResetFrmHisNatHabitoAlimentar").removeClass('hide');
                $("#btnSaveFrmHisNatHabitoAlimentar").data('value', $("#btnSaveFrmHisNatHabitoAlimentar").html());
                $("#btnSaveFrmHisNatHabitoAlimentar").html("Gravar Alteração");
                app.setFormFields(res, 'frmHisNatHabitoAlimentar');
            });
    },
    deleteHabitoAlimentar: function(params) {
        app.confirm('<br>Confirma exclusão do hábito alimentar <b>' +
            params.descricao + '</b>?',
            function() {
                app.ajax(app.url + 'fichaHistoriaNatural/deleteHabitoAlimentar',
                    params,
                    function(res) {
                        historiaNatural.updateGridHabitoAlimentar();
                    });
            },
            null, params, 'Confirmação', 'warning');
    },
    updateGridHabitoAlimentar: function() {
        ficha.getGrid('habitoAlimentar');
    },
    resetFrmHisNatHabitoAlimentar: function() {
        $("#frmHisNatHabitoAlimentar").get(0).reset();
        $("#sqTipoHabitoAlimentar").focus();
        $("#sqFichaHabitoAlimentar").val(''); // o reset não está limpando este campo
        $("#btnResetFrmHisNatHabitoAlimentar").addClass('hide');
        $("#btnSaveFrmHisNatHabitoAlimentar").html($("#btnSaveFrmHisNatHabitoAlimentar").data('value'));
    },
    stHabitoAlimentEspecialistaChange: function(e) {
        if ($("#stHabitoAlimentEspecialista").val() === 'S') {
            $("#divHabitoAlimentarEsp").removeClass('hide');
            if ($("#divGridHabitoAlimentarEsp").html() == '') {
                historiaNatural.updateGridHabitoAlimentarEsp();
            }
        } else {
            $("#divHabitoAlimentarEsp").addClass('hide');
        }
    },
    //--------------------------------------------------------------------------
    //                    HABITO ALIMENTAR ESPECIALISTA
    //--------------------------------------------------------------------------
    saveFrmHisNatHabitoAlimentarEsp: function(params) {
        if (!$('#frmHisNatHabitoAlimentarEsp').valid()) {
            return false;
        }
        var data = $("#frmHisNatHabitoAlimentarEsp").serializeArray();
        data = app.params2data(params, data);
        if (!data.sqFicha) {
            app.alertError('ID da ficha não encontrado!');
            return;
        }
        if (!$("#sqTaxonEspecialista").val()) {
            app.alertError('Selecione o Táxon!');
            return;
        }
        app.ajax(
            app.url + 'fichaHistoriaNatural/saveFrmHisNatHabitoAlimentarEsp',
            data,
            function(res) {
                historiaNatural.resetFrmHisNatHabitoAlimentarEsp(); // limpar o
                // formulário
                // depois de
                // salvar
                historiaNatural.updateGridHabitoAlimentarEsp(); // atualizar o gride
            }, null, 'json');
    },
    updateGridHabitoAlimentarEsp: function() {
        ficha.getGrid('habitoAlimentarEsp');
    },
    resetFrmHisNatHabitoAlimentarEsp: function() {
        $("#frmHisNatHabitoAlimentarEsp").get(0).reset();
        $("#nivel").focus();
        $("#sqFichaHabitoAlimentarEsp").val(''); // o reset não está limpando este campo
        $("#sqTaxonEspecialista").empty();
        historiaNatural.sqNivelTaxonomicoChange();
        $("#btnResetFrmHisNatHabitoAlimentarEsp").addClass('hide');
        $("#btnSaveFrmHisNatHabitoAlimentarEsp").html($("#btnSaveFrmHisNatHabitoAlimentarEsp").data('value'));
    },
    deleteHabitoAlimentarEsp: function(params) {
        app.confirm('<br>Confirma exclusão do hábito alimentar ESPECIALISTA <b>' +
            params.descricao + '</b>?',
            function() {
                app.ajax(app.url + 'fichaHistoriaNatural/deleteHabitoAlimentarEsp',
                    params,
                    function(res) {
                        historiaNatural.updateGridHabitoAlimentarEsp();
                    });
            },
            null, params, 'Confirmação', 'warning');
    },
    editHabitoAlimentarEsp: function(params) {
        app.ajax(
            app.url + 'fichaHistoriaNatural/editHabitoAlimentarEsp', params,
            function(res) {
                $("#btnResetFrmHisNatHabitoAlimentarEsp").removeClass('hide');
                $("#sqTaxonEspecialista").parent().removeClass('hide');
                $("#btnSaveFrmHisNatHabitoAlimentarEsp").data('value', $("#btnSaveFrmHisNatHabitoAlimentarEsp").html());
                $("#btnSaveFrmHisNatHabitoAlimentarEsp").html("Gravar Alteração");
                app.setFormFields(res, 'frmHisNatHabitoAlimentarEsp');
                app.setSelect2('sqTaxonEspecialista', res.sqTaxonEspecialista,
                    res.nmCientifico);
            });
    },
    sqNivelTaxonomicoChange: function(params) {
        $("#sqTaxonEspecialista").empty();
        if ($("#nivel").val()) {
            $("#sqTaxonEspecialista").parent().removeClass('hide');
        } else {
            $("#sqTaxonEspecialista").parent().addClass('hide');
        }
    },
};

// ################################################################################################################################ //
// 11.6 - Resultado Avaliacao
// ################################################################################################################################ //
var resultadoAvaliacao = {

    save:function(params) {
        var data = {};

        if (!params.container) {
            app.alertInfo('saveResultado: container não informado.');
            return;
        }
        if (!params.sqFicha) {
            app.alertInfo('saveResultado: Ficha não informada.');
            return;
        }

        // ajustar o id do container
        params.container = '#' + params.container.replace(/^#/, '');
        var $container = $(params.container);
        var $divDadosMotivoMudanca = $container.find("#divDadosMotivoMudanca" + params.sqFicha);
        var $dsCriterioAvalIucn = $container.find('#dsCriterioAvalIucn');
        var $stPossivelmenteExtinta = $container.find('#stPossivelmenteExtinta');

        // ler todos os campos - AVALIAÇÃO
        data.cdCategoriaIucn            = $container.find("select[name=sqCategoriaIucn] option:selected").data('codigo');

        data.dsJustMudancaCategoria     = corrigirCategoriasTexto(trimEditor(tinyMCE.get('dsJustMudancaCategoria' + params.sqFicha).getContent()));
        tinyMCE.get('dsJustMudancaCategoria' + params.sqFicha).setContent(data.dsJustMudancaCategoria);

        data.sqFicha                    = params.sqFicha;
        //data.dsCitacao                  = trimEditor(tinyMCE.get('dsCitacao' + params.sqFicha).getContent());
        data.dsCitacao                  = $('#dsCitacao' + params.sqFicha).val()
        //data.dsColaboradores            = trimEditor(tinyMCE.get('dsColaboradores' + params.sqFicha).getContent());
        data.dsColaboradores            = $("#dsColaboradores" + params.sqFicha).val();
        //data.dsEquipeTecnica            = trimEditor(tinyMCE.get('dsEquipeTecnica' + params.sqFicha).getContent());
        data.dsEquipeTecnica            = $('#dsEquipeTecnica' + params.sqFicha).val();
        data.sqCategoriaIucn            = $container.find("select[name=sqCategoriaIucn] option:selected").val();
        data.dsCriterioAvalIucn         = $container.find("input[name=dsCriterioAvalIucn]").val();
        data.deCategoriaOutra           = $container.find("input[name=deCategoriaOutra]").val();
        data.stPossivelmenteExtinta     = $container.find("input[name=stPossivelmenteExtinta]").is(':checked') ? 'S' : 'N';

        data.dsJustificativa            = corrigirCategoriasTexto( trimEditor(tinyMCE.get('dsJustificativaValidacao' + params.sqFicha).getContent()));
        tinyMCE.get('dsJustificativaValidacao' + params.sqFicha).setContent(data.dsJustificativa);

        data.sqTipoConectividade        = $container.find("select[name=sqTipoConectividade] option:selected").val();
        data.sqTendenciaImigracao       = $container.find("select[name=sqTendenciaImigracao] option:selected").val();
        data.stDeclinioBrPopulExterior  = $container.find("select[name=stDeclinioBrPopulExterior] option:selected").val();
        data.dsConectividadePopExterior = '';
        if( $container.find("#dsConectividadePopExterior"+params.sqFicha).size() == 1 ) {
            data.dsConectividadePopExterior = trimEditor(tinyMCE.get('dsConectividadePopExterior' + params.sqFicha).getContent());
        }

        // validar campos
        if (!data.sqCategoriaIucn && data.dsJustificativa ) {
            app.alertInfo('Necessário informar a categoria!');
            return;
        }

        // validar o critério
        if ($dsCriterioAvalIucn.is(':visible')) {
            if (!data.dsCriterioAvalIucn) {
                app.alertInfo('Necessário informar o critério!');
                return;
            }
        } else {
            data.dsCriterioAvalIucn = '';
        }

        // validar possivelmente extinta
        if (!$stPossivelmenteExtinta.is(':visible')) {
            data.stPossivelmenteExtinta = '';
        }

        if( data.cdCategoriaIucn != 'NE' )
        {
            if (!data.dsJustificativa && data.sqCategoriaIucn) {
                app.alertInfo('Necessário informar a justificativa!');
                return;
            }

            // validar mudanca de categoria
            if ($divDadosMotivoMudanca.is(':visible')) {
                if ($divDadosMotivoMudanca.find('#divGridMotivoMudancaCategoria tbody>tr').size() == 0) {
                    app.alertInfo('Motivo da mudança deve ser informado quando houver mudança de categoria.');
                    return;
                }

                // se a categoria mudou de CR,EN ou VU para qualquer outra diferente destas 3 a justificava
                // deve ser obrigatoria
                var cdCategoriaIucnUltimaAvaliacao = $container.find("input[name=cdCategoriaIucnUltimaAvaliacao]").val();
                if (cdCategoriaIucnUltimaAvaliacao && cdCategoriaIucnUltimaAvaliacao.match(/CR|EN|VU/)
                    && !data.cdCategoriaIucn.match(/CR|EN|VU/) && $('<p>' + data.dsJustMudancaCategoria + '</p>').text().trim() == '') {
                    app.alertInfo('Quando houver mudança de categoria AMEAÇADA para NÃO AMEAÇADA a justificativa é obrigatória.');
                    return;
                }
                // se for informado o texto ele deve ter no minimo 5 palavras
                var texto = trimEditor( tinyMCE.get('dsJustMudancaCategoria'+params.sqFicha).getContent({format : 'text'}));
                texto = texto.replace(/\s[a-z]{1}\s/gi,' ').replace(/\s[a-z]{2}\s/gi,' ').replace(/\s{2,}/gi,' ')
                if( texto && texto.split(' ').length < 5) {
                    app.alertInfo('A justificativa da mudança de catetoria deve conter no mímino 5 palavras.');
                    return;
                }

            } else {
                data.dsJustMudancaCategoria = '';
            }
        } else {
            data.dsJustMudancaCategoria = '';
        }

        // gravar resultado da Avaliação ABA 11.6
        app.confirm('<br>Confirma resultado da avaliação?',
            function () {
                app.ajax(app.url + 'fichaAvaliacao/saveResultadoAvaliacao',
                    data,
                    function (res) {
                        // remover cor vermelha da aba
                        app.resetChanges(params.container)
                    });
            },
            function () {
            }, params, 'Confirmação', 'warning');
    },

    saveAutoria:function(params) {
        var data = {};
       if (!params.sqFicha) {
            app.alertInfo('saveResultado: Ficha não informada.');
            return;
        }
        // ajustar o id do container
        data.sqFicha = params.sqFicha;
        //data.dsCitacao = trimEditor(tinyMCE.get('dsCitacao' + params.sqFicha).getContent());
        data.dsCitacao = $('#dsCitacao' + params.sqFicha).val();

        app.ajax(app.url + 'fichaAvaliacao/saveAutoria',
            data,
            function (res) {});
    },

    saveEquipeTecnica:function(params) {
        var data = {};
       if (!params.sqFicha) {
            app.alertInfo('saveResultado: Ficha não informada.');
            return;
        }
        // ajustar o id do container
        data.sqFicha = params.sqFicha;
        //data.dsEquipeTecnica = trimEditor(tinyMCE.get('dsEquipeTecnica' + params.sqFicha).getContent());
        data.dsEquipeTecnica = $("#dsEquipeTecnica" + params.sqFicha).val();
        app.ajax(app.url + 'fichaAvaliacao/saveEquipeTecnica',
            data,
            function (res) {});
    },

    saveColaboradores:function(params) {
        var data = {};
       if (!params.sqFicha) {
            app.alertInfo('saveResultado: Ficha não informada.');
            return;
        }
        // ajustar o id do container
        data.sqFicha = params.sqFicha;
        //data.dsColaboradores = trimEditor(tinyMCE.get('dsColaboradores' + params.sqFicha).getContent());
        data.dsColaboradores = $('#dsColaboradores' + params.sqFicha).val();
        app.ajax(app.url + 'fichaAvaliacao/saveColaboradores',
            data,
            function (res) {});
    }

};

// ################################################################################################################################ //
// 11.7 - Resultado Validacao
// ################################################################################################################################ //
var resultadoValidacao = {

    save: function (params) {
        var data = {};

        if (!params.container) {
            app.alertInfo('ficha.js/resultadoValidacao.save(): container não informado.');
            return;
        }
        if (!params.sqFicha) {
            app.alertInfo('ficha.js/resultadoValidacao.save(): Ficha não informada.');
            return;
        }

        // ajustar o id do container
        params.container = '#' + params.container.replace(/^#/, '');
        var $container = $(params.container);

        // div com os motivos da mudança de categoria
        var $divDadosMotivoMudanca  = $container.find("#divDadosMotivoMudanca" + params.sqFicha);
        var $dsCriterioAvalIucn     = $container.find('#dsCriterioAvalIucn');
        var $stPossivelmenteExtinta = $container.find('#stPossivelmenteExtinta');

        // ler todos os campos - VALIDAÇÃO
        data.cdCategoriaIucn        = $container.find("select[name=sqCategoriaIucn] option:selected").data('codigo');

        data.dsJustMudancaCategoria = corrigirCategoriasTexto( trimEditor(tinyMCE.get('dsJustMudancaCategoriaFinal' + params.sqFicha).getContent() ) );
        tinyMCE.get('dsJustMudancaCategoriaFinal' + params.sqFicha).setContent(data.dsJustMudancaCategoria);

        data.sqFicha                = params.sqFicha;
        data.sqCategoriaIucn        = $container.find("select[name=sqCategoriaIucn] option:selected").val();
        data.dsCriterioAvalIucn     = $container.find("input[name=dsCriterioAvalIucn]").val();
        data.deCategoriaOutra       = $container.find("input[name=deCategoriaOutra]").val();
        data.stPossivelmenteExtinta = $container.find("input[name=stPossivelmenteExtinta]").is(':checked') ? 'S' : 'N';
        data.dsJustificativa        = corrigirCategoriasTexto( trimEditor(tinyMCE.get('dsJustificativaFinal' + params.sqFicha).getContent() ) );
        tinyMCE.get('dsJustificativaFinal' + params.sqFicha).setContent(data.dsJustificativa);

        // validar campos
        if ( ! data.sqCategoriaIucn && data.dsJustificativa ) {
            app.alertInfo('Necessário informar a categoria!');
            return;
        }

        // validar o critério
        if ( $dsCriterioAvalIucn.is(':visible')) {
            if (!data.dsCriterioAvalIucn) {
                app.alertInfo('Necessário informar o critério!');
                return;
            }
        } else {
            data.dsCriterioAvalIucn = '';
        }

        // validar possivelmente extinta
        if (!$stPossivelmenteExtinta.is(':visible')) {
            data.stPossivelmenteExtinta = '';
        }

        if ( ! data.dsJustificativa && data.sqCategoriaIucn && data.cdCategoriaIucn != 'NE') {
            app.alertInfo('Necessário informar a justificativa!');
            return;
        }

        // validar mudanca de categoria
        if ($divDadosMotivoMudanca.is(':visible')) {
            if ($divDadosMotivoMudanca.find('#divGridMotivoMudancaCategoria tbody>tr').size() == 0) {
                app.alertInfo('Motivo da mudança deve ser informado quando houver mudança de categoria.');
                return;
            }

            // se a categoria mudou de CR,EN ou VU para qualquer outra diferente destas 3 a justificava
            // deve ser obrigatoria
            var cdCategoriaIucnUltimaAvaliacao = $container.find("input[name=cdCategoriaIucnUltimaAvaliacao]").val();
            if( cdCategoriaIucnUltimaAvaliacao && cdCategoriaIucnUltimaAvaliacao.match(/CR|EN|VU/)
                && ! data.cdCategoriaIucn.match(/CR|EN|VU/) && $('<p>' + data.dsJustMudancaCategoria + '</p>').text().trim()  == '' )
            {
                app.alertInfo('Quando houver mudança de categoria AMEAÇADA para NÃO AMEAÇADA a justificativa é obrigatória.');
                return;
            }
        } else {
            data.dsJustMudancaCategoria = '';
        }

        // gravar resultado da Avaliação ABA 11.6
        app.confirm('<br>Confirma resultado da validação?',
            function () {
                app.ajax(app.url + 'fichaAvaliacao/saveResultadoValidacao',
                    data,
                    function (res) {
                        // remover cor vermelha da aba
                        app.resetChanges(params.container)
                    });
            },
            function () {
            }, params, 'Confirmação', 'warning');

    } // fim save
};

// inializar pagina ficha
ficha.init();

/**
 * evento para tratar o retorno do cadastro da ref. bibliográfica
 */
$('#modalRefBib').off('hidden.bs.modal').on('hidden.bs.modal', function(e) {
    var data = $("#modalRefBib").data();
});



/***************************************************************/
/* Abrir modal para selecionar regiões em Ameaça e Usos        */
/* *************************************************************/
var selRegiao = {
    openModalSelRegiao: function(params) {
        params.modalName = 'selecionarRegiao';
        params.reload = true;
        params.sqFicha = $("#sqFicha").val();
        app.openWindow({
            id: 'selRegiao',
            url: app.url + 'ficha/getModal',
            data: params,
            title: 'Regionalização',
            autoOpen: true,
            modal: true
        }, function(data, e) {
            // onShow
            data = data.data;
            // configurar os campos da tela
            $("#frmModalSelRegiaoContexto").html(data.deRotulo);
            // adicionar os parametros no botão salvar
            $.each(data, function(k, v) {
                    if (/^(no|sq|ds)/.test(k)) {
                        $("#btnSaveFrmModalSelRegiao").data(k, v);
                    }
                })
                // adicionar o sqRegistro na div do gride
            $("#divGridSelRegiao").data('sqRegistro', data.sqRegistro);
            $("#divGridSelRegiao").data('noContexto', data.noContexto);
            ficha.getGrid('divGridSelRegiao');
        }, function(data, e) {
            app.reset('frmModalSelRegiaoContexto');
            app.unselectGridRow('divGridSelRegiao');
            if (data.data.updateGrid) {
                ficha.getGrid(data.data.updateGrid);
            }
        });
    },
    /*
     sqAbrangenciaChange:function( params )
     {
     var selectPai = $("#frmModalSelRegiao #sqTipoAbrangencia");
     var selectFilho = $("#frmModalSelRegiao #sqAbrangencia");
     var data   = selectPai.find('option:selected').data();
     // alterar o name do select
     selectFilho.prop('name',String('sq_'+data.codigo.toLowerCase()).toCamel() )

     // desabilitar o select
     selectFilho.prop('disabled',true);

     // limpar o select
     selectFilho.find('option').each(function(k,v) {
     if( this.value )
     {
     $(this).remove(); //or whatever else
     }
     });

     // fazer chamada ajax para carregar o select filho
     if( data.codigo )
     {
     if( data.codigo == 'OUTRA')
     {

     $("#frmModalSelRegiao #sqAbrangencia").parent().addClass('hide');
     $("#frmModalSelRegiao #noRegiaoOutra").parent().removeClass('hide');
     }
     else
     {
     $("#frmModalSelRegiao #noRegiaoOutra").parent().addClass('hide');
     $("#frmModalSelRegiao #sqAbrangencia").parent().removeClass('hide');
     data.noCampo        = selectFilho.prop('name');
     data.noDominio      = data.noCampo.replace(/^sq/,'').ucFirst();
     data.noPropriedade  = data.noDominio.lcFirst();
     app.ajax(app.url+'ficha/getTipoAbrangencia',data,function(res){
     var tmpVal = $("#frmModalSelRegiao #sqAbrangencia").data('tmpVal');
     $.each(res,function(k,v){
     selectFilho.append('<option value="'+v.codigo +'"'+ (v.codigo==tmpVal?' selected':'' )+'>'+v.descricao+'</option>');
     });
     selectFilho.prop('disabled',( data.codigo == '') );
     },'','JSON')
     }
     }
     },
     */
    saveFrmModalSelRegiao: function(params) {
        if (!$("#frmModalSelRegiao").valid()) {
            return;
        }
        var data = $("#frmModalSelRegiao").serializeArray();
        data = app.params2data(params, data);
        if (!data.sqTipoAbrangencia) {
            app.alertError('Selecione uma abrangência!')
        }
        if (!$("#sqAbrangencia").is(":visible")) {
            $("#sqAbrangencia").empty();
        } else {
            $('#noRegiaoOutra').val('');
        }
        // passar o nome da classe e o nome da coluna que será gravada na tabela ficha_ocorrencia. Ex:Pais e sqPais, Uc e sqUc, sqEstado etc..
        //data.noClasseDominio = $("#frmModalSelRegiao #sqAbrangencia").prop('name').replace(/^sq/,'').ucFirst();
        data.noCampo = $("#frmModalSelRegiao #sqAbrangencia").prop('name');
        data.noDominio = data.noCampo.replace(/^sq/, '').ucFirst();
        data.noPropriedade = data.noDominio.lcFirst();
        app.ajax(app.url + 'ficha/saveRegionalizacao', data, function(res) {
            if (res.status == 0) {
                selRegiao.resetFrmModalSelRegiao()
                ficha.getGrid('divGridSelRegiao');
            }
        }, 'Gravando. Aguarde...', 'JSON')
    },
    editRegionalizacao: function(params, btn) {
        params.noContexto = $("#divGridSelRegiao").data('noContexto');
        app.ajax(app.url + 'ficha/editRegionalizacao', params,
            function(res) {
                $("#btnResetFrmModalSelRegiao").removeClass('hide');
                $("#btnSaveFrmModalSelRegiao").data('value', $("#btnSaveFrmModaSellRegiao").html());
                $("#btnSaveFrmModalSelRegiao").html("Gravar Alteração");
                app.setFormFields(res, 'frmModalSelRegiao');
                app.selectGridRow(btn);
                // popular o select com as abrangencias
                tipoAbrangenciaChange({
                    contexto: 'frmModalSelRegiao'
                });
                //$("#frmModalSelRegiao #sqTipoAbrangencia").change();
            });
    },
    deleteRegionalizacao: function(params, btn) {
        params.noContexto = $("#divGridSelRegiao").data('noContexto');
        app.selectGridRow(btn);
        app.confirm('<br>Confirma exclusão da região ' + params.descricao + '?',
            function() {
                app.ajax(app.url + 'ficha/deleteRegionalizacao',
                    params,
                    function(res) {
                        selRegiao.resetFrmModalSelRegiao();
                        ficha.getGrid('divGridSelRegiao');
                    });
            },
            function() {
                app.unselectGridRow(btn);
            }, params, 'Confirmação', 'warning');
    },
    resetFrmModalSelRegiao: function() {
        app.reset('frmModalSelRegiao', 'noContexto');
        app.unselectGridRow('divGridSelRegiao');
        $("#btnResetFrmModalSelRegiao").addClass('hide');
    }
};

/*****************************************************************/
/* Abrir modal para selecionar coordenadas para pontos de Ameaça */
/* ***************************************************************/

var imgMarker = document.createElement('img');
imgMarker.src = window.grails.markerRed;
imgMarker.width = 12;
imgMarker.height = 20;

var selGeo = {
        map: null,
        contexto: '#frmModalSelGeo',
        openModalSelGeo: function(params) {
            params.modalName = 'selecionarGeo';
            params.reload = false;
            params.sqFicha = $("#sqFicha").val();
            app.openWindow({
                id: 'selGeo',
                url: app.url + 'ficha/getModal',
                data: params,
                title: 'Georeferencia',
                autoOpen: true,
                modal: true
            }, function(data, e) {
                // onShow
                data = data.data;
                app.reset('frmModalSelGeo'); // limpar formulario
                // configurar os campos da tela
                $("#frmModalSelGeoContexto").html(data.deRotulo);
                // adicionar os parametros no botão salvar
                $.each(data, function(k, v) {
                        if (/^(no|sq|ds)/.test(k)) {
                            $("#btnSaveFrmModalSelGeo").data(k, v);
                        }
                    })
                    // adicionar o sqRegistro na div do gride
                $("#winSelGeo #divGridSelGeo").data('sqRegistro', data.sqRegistro);
                $("#winSelGeo #divGridSelGeo").data('noContexto', data.noContexto);
                selGeo.updateGrid();
                if (!selGeo.map) {
                    // criar o mapa
                    var map = new ol.Map({
                        target: 'mapSelGeo',
                        layers: [
                            new ol.layer.Tile({
                                source: new ol.source.OSM(),
                                visible: true,
                                name: 'osm',
                            }),
                            new ol.layer.Vector({
                                source: new ol.source.Vector({
                                    features: []
                                }),
                                name: 'drawLayer',
                                visible: true,
                                zIndex: 2,
                                renderBuffer: 200,
                                attributions: [
                                    new ol.Attribution({
                                        html: 'Sistema de Avaliação - ICMBio'
                                    })
                                ],
                            }),
                        ],
                        controls: ol.control.defaults({
                            attributionOptions: ({
                                collapsible: false
                            })
                        }).extend([
                            new ol.control.FullScreen(),
                            new ol.control.ScaleLine(),
                            //new ol.control.ZoomSlider(),
                            new ol.control.MousePosition({
                                projection: 'EPSG:4326',
                                coordinateFormat: ol.coordinate.createStringXY(6),
                                undefinedHTML: 'X:? Y:?'
                            }),
                            //new ol.control.OverviewMap({ className: 'ol-overviewmap ol-custom-overviewmap'}),
                        ]),
                        view: new ol.View({
                            center: [-5328210.013394036, -1781534.300990922],
                            zoom: 4
                        })
                    });
                    selGeo.map = map;
                    selGeo.map.on('click', function(evt) {
                        if (evt && evt.coordinate && evt.originalEvent.ctrlKey) {
                            evt.coordinate = ol.proj.toLonLat(evt.coordinate, 'EPSG:3857');
                            $('#frmModalSelGeo #fldLongitude').val(evt.coordinate[0].toPrecision(10));
                            $('#frmModalSelGeo #fldLatitude').val(evt.coordinate[1].toPrecision(10));
                            selGeo.setGMSFields();
                            selGeo.updateMarker(false);
                        }
                    });
                }
                // atualizar a div do mapa
                window.setTimeout(function() {
                    var h = $("#frmModalSelGeo #mapSelGeo").height();
                    $("#frmModalSelGeo #selGeo .modal-body").scrollTop(5000);
                    var s = $("#frmModalSelGeo #selGeo .modal-body").scrollTop()
                    $("#frmModalSelGeo #selGeo .modal-body").scrollTop(0);
                    $("#frmModalSelGeo #mapSelGeo").height(h - s)
                    selGeo.clearMarkers();
                    selGeo.map.updateSize();
                }, 1000);
            }, function(data, e) {
                // on close
                selGeo.clearMarkers();
                app.reset('frmModalSelGeoContexto');
                app.unselectGridRow('divGridSelGeo');
                if (data.data.updateGrid) {
                    ficha.getGrid(data.data.updateGrid);
                }
            });
        },
        changeGDGMS: function() {
            if ($("#frmModalSelGeo #divLatGD").hasClass('hidden')) {
                $("#frmModalSelGeo #divLatGD,#frmModalSelGeo #divLonGD").removeClass('hidden');
                $("#frmModalSelGeo #divLatGMS,#frmModalSelGeo #divLonGMS").addClass('hidden');
                $(".brLatLon").addClass('hidden');
                selGeo.setGDFields();
                selGeo.setGDFields();
            } else {
                $("#frmModalSelGeo #divLatGD,#frmModalSelGeo #divLonGD").addClass('hidden');
                $("#frmModalSelGeo #divLatGMS,#frmModalSelGeo #divLonGMS").removeClass('hidden');
                $(".brLatLon").removeClass('hidden');
                selGeo.setGMSFields();
            }
        },
        setGMSFields: function() {
            // converter de gd para GMS
            var lat = $("#frmModalSelGeo #fldLatitude").val();
            var lon = $("#frmModalSelGeo #fldLongitude").val();
            if (lat) {
                var gms = app.gd2gms(lat, 'LAT');
                $("#frmModalSelGeo #fldMLat").val(gms.m);
                $("#frmModalSelGeo #fldGLat").val(gms.g);
                $("#frmModalSelGeo #fldSLat").val(gms.s);
                $("#frmModalSelGeo #fldHLat").val(gms.h.toUpperCase());
            }
            if (lon) {
                var gms = app.gd2gms(lon, 'LON');
                $("#frmModalSelGeo #fldGLon").val(gms.g);
                $("#frmModalSelGeo #fldMLon").val(gms.m);
                $("#frmModalSelGeo #fldSLon").val(gms.s);
                $("#frmModalSelGeo #fldHLon").val(gms.h.toUpperCase());
            }
        },
        setGDFields: function() {
            // converter de gms para GD
            var gLat = $("#frmModalSelGeo #fldGLat").val();
            var mLat = $("#frmModalSelGeo #fldMLat").val();
            var sLat = $("#frmModalSelGeo #fldSLat").val();
            var hLat = $("#frmModalSelGeo #fldHLat").val();
            if (gLat && mLat && sLat && hLat) {
                $("#frmModalSelGeo #fldLatitude").val(app.gms2gd(gLat, mLat, sLat, hLat));
            }
            var gLon = $("#frmModalSelGeo #fldGLon").val();
            var mLon = $("#frmModalSelGeo #fldMLon").val();
            var sLon = $("#frmModalSelGeo #fldSLon").val();
            var hLon = $("#frmModalSelGeo #fldHLon").val();
            if (gLon && mLon && sLon && hLon) {
                $("#frmModalSelGeo #fldLongitude").val(app.gms2gd(gLon, mLon, sLon, hLon));
            }
        },
        updateMarker: function(centerMap) {
            // se os campos GMS estiverem visívies, converter para GD antes plotar no mapa
            if ($("#frmModalSelGeo #divLatGD").hasClass('hidden')) {
                selGeo.setGDFields();
            }
            var data = {};
            data.fldLongitude = parseFloat($('#frmModalSelGeo #fldLongitude').val());
            data.fldLatitude = parseFloat($('#frmModalSelGeo #fldLatitude').val());
            if (selGeo.isLatLon(data.fldLatitude, data.fldLongitude, null, false)) {
                if (centerMap) {
                    selGeo.map.getView().setCenter(ol.proj.fromLonLat([data.fldLongitude, data.fldLatitude]));
                    if (selGeo.map.getView().getZoom() != 6) {
                        selGeo.map.getView().setZoom(6);
                    }
                }
                selGeo.showMarker(data);
            }
        },
        updateGrid: function() {
            ficha.getGrid('divGridSelGeo');
        },
        isLatLon: function(lat, lng, msgError, required) {
            // http://stackoverflow.com/questions/3518504/regular-expression-for-matching-latitude-longitude-coordinates
            var latLngRegex = /^[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?),\s*[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?)$/;
            if (lat && lng) {
                var latLng = lat + ', ' + lng;
                if (!latLngRegex.test(latLng)) {
                    if (msgError) {
                        app.alertError(msgError);
                    }
                    return false;
                }
            } else {
                if (required) {
                    app.alertError('É necessário informar latitude e longitude!');
                }
                return false;
            }
            return true;
        },
        saveFrmModalSelGeo: function(params) {
            if (!$("#frmModalSelGeo").valid()) {
                return;
            }
            // se os campos GMS estiverem visívies, converter para GD antes de gravar
            if ($("#frmModalSelGeo #divLatGD").hasClass('hidden')) {
                selGeo.setGDFields();
            }
            var data = $("#frmModalSelGeo").serializeArray();
            data = app.params2data(params, data);
            // aqui tem que ser o id da ameaca em questão
            if (!data.sqFicha) {
                app.alertError('ID da ficha não encontrado!');
                return;
            }
            if (!selGeo.isLatLon(data.fldLatitude, data.fldLongitude, 'Coordenada inválida', true)) {
                return
            }
            app.ajax(app.url + 'ficha/saveGeoreferencia', data, function(res) {
                if (res.status == 0) {
                    app.reset('frmModalSelGeo');
                    selGeo.updateGrid();
                    selGeo.clearMarkers();
                    $("#btnResetFrmModalSelGeo").addClass('hide');
                    app.focus('sqDatum');
                }
            }, 'Gravando. Aguarde...', 'JSON')
        },
        mapInit: function(map) {

            /*
             var s = $("#selGeo .modal-body")[0].scrollHeight -10;
             var h = $("#selGeo .modal-body").height()
             */
            // ajustar o mapa na tela para retirar barras de r'ola'gem
            var h = $("#frmModalSelGeo #mapSelGeo").height();
            $("#frmModalSelGeo #selGeo .modal-body").scrollTop(5000);
            var s = $("#frmModalSelGeo #selGeo .modal-body").scrollTop()
            $("#frmModalSelGeo #selGeo .modal-body").scrollTop(0);
            $("#frmModalSelGeo #mapSelGeo").height(h - s)
            selGeo.map = map;
            selGeo.map.updateSize();
            selGeo.map.on('click', function(evt) {
                if (evt && evt.coordinate && evt.originalEvent.ctrlKey) {
                    evt.coordinate = ol.proj.toLonLat(evt.coordinate, 'EPSG:3857');
                    $('#frmModalSelGeo #fldLongitude').val(evt.coordinate[0].toPrecision(10));
                    $('#frmModalSelGeo #fldLatitude').val(evt.coordinate[1].toPrecision(10));
                    selGeo.setGMSFields();
                    selGeo.updateMarker(false);
                }
            });
        },
        showMarker: function(params) {
            if (!selGeo.map || !selGeo.isLatLon(params.fldLatitude, params.fldLongitude, 'Coordenadas invalidas', false)) {
                return false;
            }
            selGeo.clearMarkers();
            var coordinates = [parseFloat(params.fldLongitude), parseFloat(params.fldLatitude)];
            selGeo.map.addOverlay(new ol.Overlay({
                position: ol.proj.fromLonLat(coordinates),
                element: imgMarker,
                positioning: 'bottom-center'
            }));
            selGeo.map.updateSize(); // atulizar a posição da marker
        },
        clearMarkers: function() {
            $.each(selGeo.map.getOverlays().getArray(), function(index, val) {
                selGeo.map.removeOverlay(val);
            });
        },
        editGeoreferencia: function(params, btn) {
            app.selectGridRow(btn);
            params.noContexto = $("#winSelGeo #divGridSelGeo").data('noContexto')
            app.ajax(app.url + 'ficha/editGeoreferencia', params,
                function(res) {
                    $("#frmModalSelGeo #btnResetFrmModalSelGeo").removeClass('hide');
                    $("#frmModalSelGeo #btnSaveFrmModalSelGeo").data('value', $("#frmModalSelGeo #btnSaveFrmModalSelGeo").html());
                    app.setFormFields(res, 'frmModalSelGeo')
                    selGeo.setGMSFields();
                    selGeo.updateMarker(true);
                });
        },
        resetFrmModalSelGeo: function() {
            app.reset('frmModalSelGeo');
            $("#frmModalSelGeo #sqDatum").focus();
            $("#frmModalSelGeo #btnResetFrmModalSelGeo").addClass('hide');
            $("#frmModalSelGeo #btnSaveFrmModalSelGeo").html($("#frmModalSelGeo #btnSaveFrmModalSelGeo").data('value'));
            app.unselectGridRow('divGridSelGeo');
            selGeo.clearMarkers();
        },
        deleteGeoreferencia: function(params, btn) {
            params.noContexto = $("#winSelGeo #divGridSelGeo").data('noContexto')
            app.selectGridRow(btn);
            app.confirm('<br>Confirma exclusão da coordenada ' + params.descricao + '?',
                function() {
                    app.ajax(app.url + 'ficha/deleteGeoreferencia',
                        params,
                        function(res) {
                            selGeo.updateGrid();
                            selGeo.resetFrmModalSelGeo();
                        });
                },
                function() {
                    app.unselectGridRow(btn);
                }, params, 'Confirmação', 'warning');
        }
    }
