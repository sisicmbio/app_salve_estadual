<table class="table table-striped table-bordered table-hover">
   <thead>
      <tr>
         <th style="width:190px;">Coordenadas</th>
         <th style="width:120px;">Datum</th>
         <th style="width:*">Observações</th>
         <th>Ação</th>
      </tr>
   </thead>
   <tbody>
   <g:each var="item" in="${listCoordenadas}">

      <tr>
         <td>${raw(item?.fichaOcorrencia?.coordenadaGmsHtml)}</td>
         <td>${item?.fichaOcorrencia?.datum?.descricao}</td>
         <td>${item?.fichaOcorrencia?.txLocal}</td>

         <td class="td-actions">
            <a id="btnEditGeoreferencia" data-action="selGeo.editGeoreferencia" data-id="${item.id}" class="fld btn btn-default btn-xs btn-update" title="Alterar">
               <span class="glyphicon glyphicon-pencil"></span>
            </a>
            <a id="btnDeleteGeoreferencia" data-action="selGeo.deleteGeoreferencia" data-descricao="${item?.fichaOcorrencia?.coordenadaGmsHtml}" data-id="${item.id}" class="fld btn btn-default btn-xs btn-delete" title="Excluir">
               <span class="glyphicon glyphicon-remove"></span>
            </a>
         </td>
      </tr>
   </g:each>
   </tbody>
</table>