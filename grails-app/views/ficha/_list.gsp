<table class="table table-hover table-striped table-condensed table-bordered">
    <thead>
        <th>#</th>
        <th>Nome Cintífico</th>
        <th>Autor</th>
        <th>Ano</th>
        <th>Ação</th>

    </thead>
    <tbody>
        <g:each in="${data}" var="item" status="i">
            <tr>
                <td class="text-center">${i + 1}</td>
                <td>${item?.nomeCientifico}</td>
                <td class="text-center">${itm.autor}</td>
                <td class="text-center">${item?.ano }</td>
                <td style="text-align:center;">
                    <button type="button" data-id="${item?.id}" class="btn btn-default btn-xs btn-update" data-toggle="tooltip" data-placement="top" title="Alterar">
                        <span class="glyphicon glyphicon-pencil"></span>
                    </button>
                    <button type="button" data-id="${item?.id}" class="btn btn-default btn-xs btn-delete" data-toggle="tooltip" data-placement="top" title="Excluir">
                        <span class="glyphicon glyphicon-remove"></span>
                    </button>
                </td>
            </tr>
        </g:each>
    </tbody>
</table>