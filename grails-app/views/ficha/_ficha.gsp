<!-- view: /views/ficha/_ficha.gsp/ -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<%-- este arquivo contem a estrutura de abas para o preenchimento da ficha de espécies --%>

<input id="sqFicha" value="${ficha.id}" type="hidden" />
<input id="canModify" value="${ canModify  ? 'true' : 'false' }" type="hidden" />
<input id="situacaoFicha" value="${ ficha.situacaoFicha.descricao }" type="hidden" />
<input id="cdSituacaoFicha" value="${ ficha.situacaoFicha.codigoSistema }" type="hidden" />

<g:javascript>
      var canModify = ${ canModify ? 'true' : 'false'};
</g:javascript>

<div class="row">
   <div class="col-sm-12">
      <div class="alert alert-success" id="divSubespecies" style="display:none;margin-bottom:5px;"></div>

      <div id="divCicloAvaliacao2" class="panel-heading sub-panel-heading">
         <a id="btnCadPendencia" data-action="ficha.showFormPendencia" data-sq-ficha="${ficha.id}" title="Cadastrar pendência" style="display:none;" class="fld btn btn-default btn-xs pull-right"><i class="glyphicon glyphicon-tags red"></i></a>
      </div>

      <%-- arvore taxonomica da espécie selecionada --%>
      <div id="divBreadcrumb">
         %{--<ol class="breadcrumb" style="background-color:${ canModify?'#d2e9cf':'#FBD6D6' }">--}%
         <ol class="breadcrumb" style="background-color:#d2e9cf;">
            <g:each in="${listTaxonHierarchy}" var="nome" status="i">
            <li id="breadcrumb_nivel_${i+1}" data-value="${nome}" class="breadcrumb-item">${raw( nome ) }</li>
            </g:each>
            <g:if test="${ ! canModify }">
               %{-- <li class="badget red pull-right">(somente leitura)</li> --}%
               <span class="badget red pull-right">(Consulta)</span>
            </g:if>
         </ol>
      </div>
      <ul class="nav nav-tabs" id="ulTabsFicha">
         <li id="liTabTaxonomia" class="active"><a data-action="ficha.tabClick" data-toggle="tab" href="#tabTaxonomia">1-Classificação Taxonômica</a></li>
         <li id="liTabDistribuicao" style="display: none;"><a data-action="ficha.tabClick" data-url="fichaDistribuicao" data-container="tabDistribuicao" data-on-load="distribuicao.tabDistribuicaoInit" data-reload="false" data-params="sqFicha" data-toggle="tab" href="#tabDistribuicao">2-Distribuição</a></li>
         <li id="liTabHistoriaNatural" style="display: none;"><a data-action="ficha.tabClick" data-url="fichaHistoriaNatural" data-container="tabHistoriaNatural" data-reload="false" data-params="sqFicha" data-toggle="tab" href="#tabHistoriaNatural">3-História Natural</a></li>
         <li id="liTabPopulacao" style="display: none;"><a data-action="ficha.tabClick" data-url="fichaPopulacao"  data-on-load="populacao.tabFichaPopulacaoInit" data-container="tabPopulacao" data-reload="false" data-params="sqFicha" data-toggle="tab" href="#tabPopulacao">4-População</a></li>
         <li id="liTabAmeaca" style="display: none;"><a data-action="ficha.tabClick" data-url="fichaAmeaca" data-on-load="ameaca.tabFichaAmeacaInit" data-container="tabAmeaca" data-reload="false" data-params="sqFicha" data-toggle="tab" href="#tabAmeaca">5-Ameaças</a></li>
         <li id="liTabUso" style="display: none;"><a data-action="ficha.tabClick" data-url="fichaUso" data-on-load="uso.tabFichaUsoInit" data-container="tabUso" data-reload="false" data-params="sqFicha" data-toggle="tab" href="#tabUso">6-Usos</a></li>
         <li id="liTabConservacao" style="display: none;"><a data-action="ficha.tabClick" data-url="fichaConservacao" data-container="tabConservacao" data-reload="false" data-params="sqFicha" data-toggle="tab" href="#tabConservacao">7-Conservação</a></li>
         <li id="liTabPesquisa" style="display: none;"><a data-action="ficha.tabClick" data-url="fichaPesquisa" data-on-load="pesquisa.tabPesquisaInit" data-container="tabPesquisa" data-reload="false" data-params="sqFicha" data-toggle="tab" href="#tabPesquisa">8-Pesquisa</a></li>
         <li id="liTabRefBibs"   style="display: none;"><a data-action="ficha.tabClick" data-url="fichaRefBib" data-container="tabRefBib" data-on-load="ficha.updateGridAllRefBib" data-reload="true" data-params="sqFicha" data-toggle="tab" href="#tabRefBib">9-Ref. Bibliográficas</a></li>
         <li id="liTabMultimidia" style="display: none;"><a data-action="ficha.tabClick" data-url="fichaMultimidia" data-container="tabMultimidia" data-reload="true" data-params="sqFicha" data-toggle="tab" href="#tabMultimidia" class="">10-Multimidia</a></li>
          <g:if test="${ ficha.situacaoFicha.codigoSistema != 'EXCLUIDA'}">
            <li id="liTabAvaliacao" style="display: none;"><a data-action="ficha.tabClick" data-url="fichaAvaliacao" data-container="tabAvaliacao" data-on-load="ficha.tabAvaliacaoInit"  data-reload="false" data-params="sqFicha" data-toggle="tab" href="#tabAvaliacao" class="tab-cinza">11-Avaliação</a></li>
          </g:if>
          <li id="liTabPendencia" style="display: none;"><a data-action="ficha.tabClick" data-url="fichaPendencia" data-container="tabPendencia" data-reload="true" data-params="sqFicha" data-toggle="tab" href="#tabPendencia" class="tab-cinza">12-Pendências<span id="tabPendenciaQtdPendencias" class="hide badge label-badge bgRed">0</span></a></li>
          <li id="liTabCreditos" style="display: none;"><a data-action="ficha.tabClick" data-url="fichaCreditos" data-container="tabCreditos" data-reload="true" data-params="sqFicha" data-toggle="tab" href="#tabCreditos" class="tab-cinza">13-Créditos</a></li>
          <li id="liTabVersoes" style="display: none;" class="pull-right"><a data-action="ficha.tabClick" data-url="fichaVersoes" data-container="tabVersoes" data-reload="true" data-params="sqFicha" data-toggle="tab" href="#tabVersoes" class="tab-cinza">Versões</a></li>
      </ul>
      %{-- Container de todas abas --}%
      <div id="taxTabContent" class="tab-content" style="min-height: 450px;">
         %{-- inicio aba Taxonomia --}%
         <div role="tabpanel" class="tab-pane tab-pane-ficha active" id="tabTaxonomia">
            <g:render template="taxonomia/index" model="['ficha':ficha,'taxonHierarchy':taxonHierarchy]"></g:render>
         </div>
         <div role="tabpanel" class="tab-pane tab-pane-ficha" id="tabDistribuicao">
            %{-- <g:render template="distribuicao/index"></g:render> --}%
         </div>
         %{-- fim aba Taxonomia --}% %{-- inicio aba Distribuicao --}%
         <div role="tabpanel" class="tab-pane tab-pane-ficha" id="tabHistoriaNatural">
            %{-- <g:render template="historiaNatural/index"></g:render> --}%
         </div>
         %{-- fim aba Distribuicao --}%
         <div role="tabpanel" class="tab-pane tab-pane-ficha" id="tabPopulacao">
            %{-- <g:render template="populacao/index"></g:render> --}%
         </div>
         %{-- inicio aba Populacao --}%
         <div role="tabpanel" class="tab-pane tab-pane-ficha" id="tabAmeaca">
            %{-- <g:render template="ameaca/index"></g:render> --}%
         </div>
         %{-- fim aba Populacao --}%
         <div role="tabpanel" class="tab-pane tab-pane-ficha" id="tabUso">
            %{-- <g:render template="uso/index"></g:render> --}%
         </div>
         %{-- fim aba ameacas --}%
         <div role="tabpanel" class="tab-pane tab-pane-ficha" id="tabConservacao">
            %{-- <g:render template="conservacao/index"></g:render> --}%
         </div>
         %{-- fim aba conservacao --}%
         <div role="tabpanel" class="tab-pane tab-pane-ficha" id="tabPesquisa">
            %{-- <g:render template="pesquisa/index"></g:render> --}%
         </div>
         %{-- fim aba Pesquisa --}%

        <div role="tabpanel" class="tab-pane tab-pane-ficha" id="tabRefBib"></div>
        %{-- fim aba RefBib --}%

        <div role="tabpanel" class="tab-pane tab-pane-ficha" id="tabAvaliacao"></div>

        %{-- fim aba Avaliacao --}%
        <div role="tabpanel" class="tab-pane tab-pane-ficha" id="tabPendencia"></div>

        <div role="tabpanel" class="tab-pane tab-pane-ficha" id="tabMultimidia"></div>
         %{-- fim aba Pendências --}%

        <div role="tabpanel" class="tab-pane tab-pane-ficha" id="tabCreditos"></div>
       %{-- fim aba Créditos --}%

        <div role="tabpanel" class="tab-pane tab-pane-ficha" id="tabVersoes"></div>
       %{-- fim aba versoes --}%
      </div>
      %{-- fim container de todas abas --}%
   </div>
   %{-- fim col-sm-12 --}%
</div>
%{-- fim row --}%
<div class="row">
   <div class="col-sm-12">
      <div class="panel-footer tab-footer-fichas">
         <p><b>Cadastrado por</b>:&nbsp;${ficha?.unidade?.siglaNome} / ${br.gov.icmbio.Util.capitalize(ficha?.usuario?.noPessoa)}</p>
         <p><b>Como citar</b>: ${raw(comoCitarFicha)} </p>
      </div>
   </div>
</div>
%{--
estrutura da modal para selecionar ref. bib nos campos da ficha ficha
<div id="modalSelRefBibFicha" style="display:none;">
   <div id="modalSelRefBibFichaBody" class="text-left">
      <%-- este form deverá ser carregado via ajax dependendo do campo da ficha --%>
      <form id="frmSelRefBibFicha" name="frmSelRefBibFicha" class="form-inline" role="form">
         <input type="hidden" name="sqFichaRefBib" id="sqFichaRefBib" value="" />
         <div class="form-group">
            <label class="control-label">Contexto</label>
            <br>
            <h4 id="modalFrmSelRefBibFichaLabel" class="form-control-static" style="color:#3C68F1"></h4>
         </div>
         <br />
         <g:if test="${canModify}">


         <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#tabSelRefBib">Referência Bibliográfica</a></li>
            <li><a data-toggle="tab" href="#tabSelComPessoal">Comunicação Pessoal</a></li>
         </ul>


         <div class="tab-content clearfix">

            <div role="tabpanel" class="tab-pane active" id="tabSelRefBib">
               <div class="form-group w100p">
                  <label class="control-label">Referência Bibliográfica</label>
                  <br>
                  <select id="sqPublicacao" name="sqPublicacao" class="fld form-control select2 fld800"
                     data-s2-url="ficha/select2RefBib"
                     data-s2-placeholder="Buscar por... ( min 3 caracteres )"
                     data-s2-minimum-input-length="3"
                     data-s2-template="refBibTemplate"
                     data-s2-auto-height="true">
                  </select>
                  <a data-action="$('#modalCadRefBib').modal('show');" class="btn btn-sm btn-default" title="Cadastrar Referência Bibliográfica">
                     <span class="glyphicon glyphicon glyphicon-plus" aria-hidden="true"></span>
                  </a>
               </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="tabSelComPessoal">
               <div class="form-group">
                  <label class="control-label">Autor</label>
                  <br>
                  <input type="text" name="noAutorRef" id="noAutorRef" class="form-control fld400" maxlength="100" value="" />
               </div>
               <div class="form-group">
                  <label class="control-label">Ano</label>
                  <br>
                  <input type="text"  name="nuAnoRef" id="nuAnoRef" class="fld form-control fld60" data-mask="0000"  maxlength="4" value=""/>
               </div>
            </div>
         </div>


         <div class="form-group w100p">
            <label class="control-label">Tag (opcional)</label>
            <br>
            <select name="deTagsIds" id="deTagsIds" class="form-control fld select2 fld830" multiple="multiple"
               data-s2-multiple="true"
               data-s2-minimum-input-length="0"
               data-s2-maximum-selection-length="5"
               >
               <g:each var="tag" in="${listTags}">
               <option value="${tag.id}">${tag.descricao}</option>
               </g:each>

            </select>

         </div>
         <div class="panel-footer mt10">
            <center>
               <a id="btnSaveFrmSelRefBibFicha" data-action="ficha.saveFrmSelRefBibFicha" data-params="sqFichaRefBib,sqFicha,sqPublicacao,nuAnoRef,noAutorRef" class="btn fld btn-success btn-sm">Adicionar Ref. Bibliográfica</a>
               <a id="btnResetFrmSelRefBibFicha" data-action="ficha.resetFrmSelRefBibFicha" class="btn fld btn-info btn-sm hide">Limpar Formulário</a>
            </center>
         </div>
         </g:if>
      </form>
      <div id="divGridRefBib" class="mt10"></div>
   </div>
</div>
--}%

<div id="refBibTemplate" class="hidden">
   <div style="display:block;border-bottom:1px dashed gray;">
      {citacao}
   </div>
</div>
<div id="refBibTemplate2" class="hidden">
   <div style="display:block;border-bottom:1px dashed gray;">
      <b>{titulo}</b><br/>
      {autor}/{ano}
   </div>
</div>
