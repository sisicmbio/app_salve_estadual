<h3>Histórico</h3>
<table class="table table-striped table-bordered table-hover" id="table_importacao_planilhas">
   <thead>
      <tr>
         <th style="width:100px">Data</th>
         <th style="width:100px">Situação</th>
         <th style="width:50px">%</th>
         <th style="width:*">Arquivo</th>
         <th style="width:200px">Responsável</th>
         <th style="width:200px">Comentário</th>
         <th style="width:80px">Ações</th>
      </tr>
   </thead>
   <tbody>
      <g:each var="item" in="${listPlanilhas}" status="i">
      <tr>
         <td class="text-center"><g:formatDate format='dd/MM/yyyy' date='${item.dtEnvio}' /></td>
          <td class="text-center" id="td_situacao_${item.id}">${item.inProcessando ? 'Processando...' : 'Finalizada'}</td>
          <td class="text-center" id="td_percentual_${item.id}">${item?.nuPercentual !=null ? item.nuPercentual.toString()+'%' : ''}</td>
         <td>${item.noArquivo}</td>
         <td>${item.pessoaFisica.noPessoa}</td>
         <td>${item.deComentario}</td>
         <td class="td-actions">
            <a data-id="${item.id}" data-action="ficha.deletePlanilha" data-gride="modalImportacaoFichas" data-descricao="${item.noArquivo}"class="fld btn btn-default btn-xs btn-delete" title="Excluir">
               <span class="glyphicon glyphicon-remove"></span>
            </a>
         </td>
      </tr>
      <tr id="tr_resultado_importacao_${item.id}">
         <td colspan="7">
             <div style="border-bottom : 2px dashed blue;padding-left: 30px;margin-bottom:20px">
               <a data-toggle="collapse" data-parent="#acordeon_log${i}" href="#acordeon_log_importacao_fichas_${i}">
                  <h4><u>Inconsistências Preenchimento</u></h4>
               </a>
               <div id="acordeon_log_importacao_fichas_${i}" class="panel-collapse collapse ${ ( i == 0 ? 'in' : '') }" role="tabpanel" style="background-color:#efefef;padding:5px;font-size:1.3em;">
                   ${ raw(item.lineErrors) }
                   <g:if test="${item.txLog}">
                       %{--<h4><u>Log Gravação</u></h4>--}%
                       ${ raw(item.txLog) }
                   </g:if>
               </div>
             </div>
         </td>
      </tr>
      </g:each>
   </tbody>
</table>
