<!-- view: /views/ficha/_divGridVisualizarPendencias.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<div style="font-size:0.85rem !important;text-align: center;margin-bottom: 8px;font-weight: bold;">${raw(arvoreTaxonomica)}</div>
<table class="table table-striped table-bordered table-hover">
   <thead>
      <tr>
         <th style="width:200px;">Autor/data</th>
         <th style="width:auto;">Pendência</th>
          <th style="min-width:80px;max-width: 200px;">Resolvida?</th>
      </tr>
   </thead>
   <tbody>
    <g:each var="item" in="${listPendencias}">
        <tr id="tr-pendencia-${item.id}">
            <g:if test="${item.contexto && item.contexto.codigoSistema=='SISTEMA'}">
                <td style="text-align: center">Sistema SALVE</td>

            </g:if>
            <g:else>
                <td style="text-align: center"> ${br.gov.icmbio.Util.nomeAbreviado(item.usuario.noPessoa)}<br>${item.dtPendencia.format('dd/MM/yyyy')}</td>
            </g:else>

            <td class="red" style="text-align: justify;">${ raw( item.txPendencia.replaceAll(/color: #000000;/,'').replaceAll(/background-color: #ffffff;/, '').replaceAll(/ href/, ' target="_blank" href') ) }</td>

            <td class="text-center" >
                <g:if test="${item?.contexto?.codigoSistema=='REVISAO'}">
                        <input type="checkbox" class="checkbox-lg"
                                        title="Marcar/desmarcar como resolvida"
                                        ${ item.stPendente == 'S' ? '' :'checked'}
                                        value="S"
                                        data-params="sqFicha:${params?.sqFicha},sqFichaPendencia:${item.id}"
                                        data-action="visualizarPendenciaFicha.updateSituacaoPendencia"/>
                         <div class="pendencia-log" id="spanLogRevisao${item.id}">${raw( item.logRevisao() ?: '&nbsp;')}</div>
                </g:if>
                <g:else>
                        ${ item.stPendente == 'S' ? 'Não' :'Sim' }
                </g:else>
            </td>

        </tr>
    </g:each>
    </tbody>
</table>
