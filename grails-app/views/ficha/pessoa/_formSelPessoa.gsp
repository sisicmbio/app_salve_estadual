<div class="lazy-load">
   /fichaPessoa
</div>
<div class="window-container text-left">
   <form class="form-inline" role="form" method="POST" id="frmSelFichaPessoa" tabindex="0">
       <div class="form-group">
         <label for="">Papel</label>
          <br />
          <select name="sqPapel" id="sqPapel" class="form-control fld200 select2" tabindex="0"
                  data-s2-placeholder="?"
                  data-s2-minimum-input-length="3"
                  data-s2-auto-height="true">
          </select>
      </div>
       <div class="form-group">
         <label for="">Autor</label>
          <br />
           <select name="sqPessoa" class="form-control fld400 select2"
                   data-s2-url="ficha/select2PessoaFisica"
                   data-s2-placeholder="?"
                   data-s2-minimum-input-length="3"
                   data-s2-auto-height="true">
           </select>
       </div>

       %{-- gride para selecionar ficha --}%
       <h4>Selecione a(s) Ficha(s)</h4>
       <div id="divGridFicha">
           <g:render template="divGridFicha"></g:render>
       </div>


      <div class="panel-footer" id="divFooter">
         <a href="javascript:void(0);" id="btnSaveCadAutor" class="btn btn-success" data-action="autor.save">Gravar</a>
         <a href="javascript:void(0);" id="btnResetCadAutor" class="btn btn-default hidden" data-action="autor.reset">Limpar Formulário</a>
      </div>
   </form>
   %{-- fim form --}%
   <div id="divGridModalAutores" data-sq-ficha="${sqFicha}" class="panel-footer mt10"></div>
</div>
