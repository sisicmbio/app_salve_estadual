<table class="table table-striped table-bordered table-hover">
   <thead>
      <tr>
         <th>Nome Completo</th>
         <th>Nome Citação</th>
         <th>Ação</th>
      </tr>
   </thead>
   <tbody>
      <g:each var="item" in="${listAutores}">
         <tr>
            %{-- <td>${raw( item.publicacao ? item.publicacao.tituloAutorAno : item.noAutor+', '+item.nuAno )}</td> --}%
            <td style="width:*;">${raw( item.autor.descricao )}</td>
            <td style="width:200px;">${raw( item.autor.codigo )}</td>

            <td class="td-actions">
               %{--<a data-action="autor.edit" data-id="${item.id}" data-all="false" class="btn fld btn-default btn-xs btn-update" title="Alterar">--}%
                  %{--<span class="glyphicon glyphicon-pencil"></span>--}%
               %{--</a>--}%
               <a data-id="${item.id}" data-action="autor.delete" data-descricao="${item.autor.descricao}" class="btn fld btn-default btn-xs btn-delete" title="Excluir">
                  <span class="glyphicon glyphicon-remove"></span>
               </a>
            </td>
         </tr>
      </g:each>
   </tbody>
</table>
