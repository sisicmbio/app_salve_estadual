//# sourceURL=N:\SAE\sae\grails-app\views\ficha\autor\autor.js
;
var autor = {
    sqAutorChange: function (params)
    {
        var data = $("#sqAutor").select2('data');
        if (data && data.length > 0)
        {
            $("#noCitacao").val(data[0].codigo);
        }
    },
    btnNovoClick : function (params)
    {
        if (parseInt($("#btnNovo").data('tag')) == 1)
        {
            // novo autor
            $("#btnNovo").data('tag', 2)
            $("#sqAutor").next().addClass('hidden'); // esconder o select2
            $("#noAutor").removeClass('hidden');
            $("#noCitacao").prop('disabled', false).val('');
            $("#btnNovo").html('Voltar')
            app.focus('noAutor');
        }
        else
        {
            // pesquisar
            $("#btnNovo").data('tag', 1)
            $("#sqAutor").next().removeClass('hidden'); // exibir o select2
            $("#noAutor").addClass('hidden');
            $("#btnNovo").html('Não Achei')
            $("#noCitacao").prop('disabled', true);
            app.focus('sqAutor');
        }
    },
    save : function (params)
    {
        if (!$("#frmModalCadAutor").valid())
        {
            return;
        }
        if (!$("#noAutor").val() && !$("#sqAutor"))
        {
            app.alert('Nenhum autor selecionado!')
            return;
        }
        if ($("#sqAutor").val() > 0)
        {
            $("#noAutor").val('');
        }
        var data = $("#frmModalCadAutor").serializeArray();
        if (!data)
        {
            return;
        }
        data = app.params2data(params, data);
        if (!data.sqFicha)
        {
            app.alertError('ID da ficha não encontrado!');
            return;
        }
        app.ajax(app.url + 'fichaAutor/save', data,
            function (res)
            {
                if (res.status === 0)
                {
                    autor.reset();
                    autor.updateGrid(true);
                }
            }, null, 'json'
        );
    },
    reset        : function ()
    {
        app.reset('frmModalCadAutor', 'sqFicha');
        $("#noAutor").focus();
        $("#btnResetCadAutor").addClass('hide');
        $("#btnSaveCadAutor").html($("#btnSaveCadAutor").data('value'));
        app.unselectGridRow('divGridModalAutores');
    },
    updateGrid   : function ( atualizarGridFichas )
    {
        ficha.getGrid('divGridModalAutores');
        if( atualizarGridFichas )
        {
           ficha.list();
        }
    },
    /*edit         : function (params, btn)
    {
        app.ajax(app.url + 'fichaAutor/edit', params,
            function (res)
            {
                $("#btnResetCadAutor").removeClass('hidden');
                $("#btnSaveCadAutor").data('value', $("#btnSaveCadAutor").html());
                $("#btnSaveCadAutor").html("Gravar Alteração");
                app.setFormFields(res, 'frmModalCadAutor');
                app.selectGridRow(btn);
            });
    },
    */
    delete       : function (params, btn)
    {
        app.selectGridRow(btn);
        app.confirm('<br>Confirma exclusão do autor '+params.descricao+'?',
            function() {
                app.ajax(app.url+'fichaAutor/delete',
                    params,
                    function(res) {
                        autor.updateGrid(true);
                    });
            },
            function() {
                app.unselectGridRow(btn);
            }, params, 'Confirmação', 'warning');
    },
};
autor.updateGrid(false);