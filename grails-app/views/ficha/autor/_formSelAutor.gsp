<div class="lazy-load">
   /fichaAutor
</div>
<div class="window-container text-left">
   <form class="form-inline" role="form" method="POST" id="frmModalCadAutor" tabindex="0">
      <input type="hidden" name="sqFicha" value="${sqFicha}">
      <input type="hidden" name="sqFichaAutor" value="">
      <div class="form-group">
         <label class="control-label">Nome Científico</label>
         <br>
         <h4 id="frmModalCadAutorContexto" class="form-control-static" style="color:#3C68F1">${noCientifico}</h4>
      </div>
      <br />
      <div class="form-group">
         <label for="">Autor</label>
          <br />
          <input name="noAutor" id="noAutor" type="text"  class="form-control fld500 hidden">
          <select name="sqAutor" id="sqAutor" data-change="autor.sqAutorChange" class="form-control fld400 select2" tabindex="0"
                  data-s2-url="fichaAutor/select2Autor"
                  data-s2-placeholder="?"
                  data-s2-minimum-input-length="3"
                  data-s2-auto-height="true">
          </select>
          <a href="javascript:void(0);" id="btnNovo" class="btn btn-default" data-tag="1" data-action="autor.btnNovoClick" tabindex="1">Não Achei</a>
      </div>
      <div class="form-group">
         <label class="control-label">Citação</label>
         <br>
         <input name="noCitacao" id="noCitacao" type="text" disabled="true" class="form-control fld300" required="true" tabindex="0">
      </div>
      <div class="panel-footer" id="divFooter">
         <a href="javascript:void(0);" id="btnSaveCadAutor" class="btn btn-success" data-action="autor.save">Gravar</a>
         <a href="javascript:void(0);" id="btnResetCadAutor" class="btn btn-default hidden" data-action="autor.reset">Limpar Formulário</a>
      </div>
   </form>
   %{-- fim form --}%
   <div id="divGridModalAutores" data-sq-ficha="${sqFicha}" class="panel-footer mt10"></div>
</div>
