<!-- Arquivo: views/ficha/conservacao/historicoAvaliacao/_formHistoricoAvaliacao.gsp -->
<g:set var="oSN"  value="${[ [key:'S',value:'Sim'], [key:'N', value:'Não'] ]}"/>

%{--BLOQUEAR EDIÇÃO DESTE CAMPO PARA TODOS--}%
<g:if test="${session?.sicae?.user?.isADM()}">
    <div class="form-group" style="display: inline-block;border-bottom:1px solid silver;margin-bottom:20px;" id="divStPresencaListaVigente">
        <label title="Campo bloqueado pela coordenação" class="control-label cursor-pointer">Presença na lista oficial nacional vigente?
            <input name="stPresencaListaVigente" id="stPresencaListaVigente" value="S" type="checkbox"
                   disabled="true"

                   ${ficha.stPresencaListaVigente == 'S' ? 'checked' : ''}
                   data-params="sqFicha"
%{--                   data-action="historicoAvaliacao.savePresencaListaVigente"--}%
                   class="fld checkbox-lg"/>
        </label>
    </div>
    <br>
</g:if>
<a id="btnShowHideFrmConHistorico"
   data-toggle="collapse"
   data-target="#frmConHistorico"
   onClick="toggleImage(this)"
   title="Clique para abrir ou fechar o formulário."
   class="fld mt10 btn btn-xs btn-default collapsed">Cadastrar histórico&nbsp;<i class="fa fa-chevron-down"></i></a>

%{--
<form name="frmConListaConvencao" id="frmConListaConvencao" role="form" class="form-inline">
	<div class="fld form-group">
		<!-- Este campo grava na tb_ficha - st_presenca_lista_vigente -->
		<label for="" class="control-label">Presença na Lista Oficial Nacional Vigente?</label>
		<br>
		<select name="stPresencaListaVigente" id="stPresencaListaVigente" data-change="listaConvencao.stPresencaListaVigenteChange"
				class="fld form-control fld300 fldSelect" required="true">
			<option value="">-- selecione --</option>
            <g:each var="item" in="${oSN}">
                <option value="${item.key}" ${ficha?.stPresencaListaVigente==item.key ? ' selected' :'' }>${item.value}</option>
            </g:each>
         </select>
     </div>
</form>
--}%

<form id="frmConHistorico" role="form" class="form-inline collapse">
	<input type="hidden" name="sqTaxonHistoricoAvaliacao" value="">
	<div class="fld form-group">
		<label for="sqTipoAvaliacao" class="control-label">Tipo Avaliação</label>
		<br>
		<select id="sqTipoAvaliacao" name="sqTipoAvaliacao" data-change="historicoAvaliacao.sqTipoAvaliacaoChange" class="fld form-control fld200 fldSelect" required="true">
			<option value="">-- selecione --</option>
			<g:each var="item" in="${listTipoAvaliacao}">
				<option data-codigo="${item.codigo}" value="${item.id}">${item.descricao}</option>
			</g:each>
		</select>
	</div>
	<div class="fld form-group">
		<label for="nuAnoAvaliacao" class="control-label">Ano</label>
		<br>
		<input name="nuAnoAvaliacao" id="nuAnoAvaliacao" type="text" class="fld form-control number fld100"
			   data-mask="0000" required="true" />
	</div>

	%{--<CATEGORIA--}%
	<div class="fld form-group">
		<label for="sqCategoriaIucn" class="control-label">Categoria</label>
		<br>
		<select name="sqCategoriaIucn" id="sqCategoriaIucn"
				class="fld form-control fld300 fldSelect"
				data-contexto="frmConHistorico"
				data-sq-ficha="${ficha.id}"
				data-container="frmConHistorico"
				data-change="app.categoriaIucnChange"
				required="true">
			<option value="">-- selecione --</option>
			<g:each var="item" in="${listCategoriaIucn}">
				<option data-codigo="${item.codigoSistema}" value="${item.id}" ${item.codigoSistema=='OUTRA' ? 'disabled':''}>${item.descricaoCompleta}</option>
			</g:each>
		</select>
	</div>


	%{-- OFICIAL --}%
	<div class="form-group">
		<label for="stOficial" class="control-label cursor-pointer label-checkbox" style="margin-top:17px;">Oficial?&nbsp;
			<input id="stOficial" name="stOficial"
				   class="checkbox-lg"
				   value="true"
				   type="checkbox"/>
		</label>
	</div>

	<br>

	%{--<CATEGORIA OUTRA--}%
	<div class="fld form-group" style="display:none;">
		<label for="deCategoriaOutra" class="control-label">&nbsp;</label>
		<br>
		-&nbsp;<input type="text" class="fld form-control fld300" name="deCategoriaOutra" id="deCategoriaOutra" value=""/>
	</div>


	%{-- CRITERIO --}%
	<div class="form-group" style="display:none;">
	  <label for="dsCriterioAvalIucn" class="control-label">Critério</label>
	  <br>
	  <input id="dsCriterioAvalIucn" name="dsCriterioAvalIucn"
			 type="text"
			 onkeyup="ficha.fldCriterioAvaliacaoKeyUp(this);"
			 readonly
			 class="fld form-control fld300 blue"/>
	  <button id="btnSelCriterioAvaliacao"
	  		  class="fld btn btn-default btn-sm"
		  	  type="button" title="Clique para Selecionar o Critério"
			  data-action="openModalSelCriterioAvaliacao"
			  data-field="dsCriterioAvalIucn"
			  data-container="frmConHistorico"
			  data-sq-ficha="${ficha.id}"
			  data-field-categoria="sqCategoriaIucn">...</button>
	</div>

	%{-- POSSIVELMENTE EXTINDA --}%
	<div class="form-group" style="display:none;">
		<label for="stPossivelmenteExtinta" class="control-label cursor-pointer label-checkbox" style="margin-top:17px;">Possivelmente Extinta?&nbsp;
		<input id="stPossivelmenteExtinta" name="stPossivelmenteExtinta"
			   class="checkbox-lg"
			   value="S"
			   type="checkbox"/>
		</label>
	</div>

	<br class="fld"/>

	%{-- ABRANGENCIA --}%
	<div class="form-group">
		 <label class="control-label">Abrangência</label>
		 <br>
		 <select id="sqTipoAbrangencia" name="sqTipoAbrangencia" data-contexto="frmConHistorico" data-change="tipoAbrangenciaChange" class="fld form-control fld200">
			<option data-codigo="" value="">?</option>
			<g:each var="item" in="${listTipoAbrangencia}">
				<option data-codigo="${item.codigo}" value="${item.id}">${item.descricao}</option>
			</g:each>
		 </select>
	</div>
	<div class="form-group">
		<label class="control-label">&nbsp;</label>
		<br>
		<span>-</span>
		<select id="sqAbrangencia"
					name="sqAbrangencia"
					class="fld form-control select2 fld600"
					disabled="true"
					data-s2-params="sqTipoAbrangencia"
					data-s2-url="ficha/getAbrangencias"
					data-s2-minimum-input-length="1"
					required="true">
					<option value="">?</option>
		 </select>
	</div>

   <div class="form-group">
		<label class="control-label">&nbsp;</label>
		<br>
		<span>-</span>
		<input type="text" name="noRegiaoOutra" id="noRegiaoOutra" class="fld form-control fld300" value=""/>
	</div>


	<br class="fld">

	<div class="fld form-group w100p">
		<label for="" class="control-label">Justificativa Avaliação</label>
		<br>
		<textarea name="txJustificativaAvaliacao" id="txJustificativaAvaliacao" rows="7" class="fld form-control fldTextarea wysiwyg w100p"></textarea>
	</div>
	%{--
	Carlos solicitou que retirasse por enquanto
	<br class="fld">
	<div class="fld form-group w100p">
		<label for="" class="control-label">Região de Abrangência</label>
		<br>
		<textarea name="txRegiaoAbrangencia" rows="7" class="fld form-control fldTextarea wysiwyg w100p"></textarea>
	</div> --}%
	%{-- botão gravar --}%
	<div class="fld panel-footer">
		<button id="btnSaveFrmConHistorico" data-action="historicoAvaliacao.saveFrmConHistorico" data-params="sqFicha" class="fld btn btn-success">Adicionar histórico</button>
		<button id="btnResetFrmConHistorico" data-action="historicoAvaliacao.resetFrmConHistorico" class="fld btn btn-danger hide">Limpar formulário</button>
	</div>
</form>

<div id="divGridHistoricoAvaliacao" class="mt10">
	%{-- <g:render template="/ficha/conservacao/historicoAvaliacao/divGridAvaliacao"></g:render> --}%
</div>


<form id="frmConHistoricoObs" role="form" class="form-inline mt25">
    %{-- Observação da tabela ficha ds_acao_conservacao --}%
    <div class="form-group w100p">
        <label for="dsHistoricoAvaliacao" class="control-label">Observações gerais sobre o histórico de avaliação</label>
        <br>
        <textarea name="dsHistoricoAvaliacao" id="dsHistoricoAvaliacao" rows="18" class="fld form-control fldTextarea wysiwyg w100p">${ficha.dsHistoricoAvaliacao}</textarea>
    </div>

    %{-- botão gravar direto na ficha --}%
    <div class="fld panel-footer">
        <button data-action="historicoAvaliacao.saveObservacao" data-params="sqFicha" class="fld btn btn-success">Gravar observações gerais</button>
    </div>
</form>
<div class="end-page"></div>
