<!-- view: /views/ficha/conservacao/historicoAvaliacao/_divGridHistoricoAvaliacao.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<g:set var="existeCategoriaOutra"  value="N"/>

<table class="table table-striped table-bordered table-hover">
   <thead>
      <tr>
         <th>Tipo avaliação</th>
         <th>Ano</th>
         <th>Oficial</th>
         <th>Tipo abrangência</th>
         <th>Abrangência</th>
         <th>Categoria</th>
         <th>Critério avaliação</th>
         <th>Justificativa</th>
         %{-- <th>Observações Abrangência</th> --}%
         <th style="width:200px;">Ref. bibliográfica</th>
         <g:if test="${ ! session.sicae.user.isCO() }">
            <th>Ação</th>
         </g:if>
      </tr>
   </thead>
   <tbody>
   <g:each var="item" in="${listHistorico}">
      <tr>
         <td class="text-center">${item?.tipoAvaliacao?.descricao}</td>
         <td class="text-center">${item.nuAnoAvaliacao}
             <g:if test="${item?.taxonHistoricoAvaliacaoVersao}">
                 <span class="fs11" style="padding: 0;margin:0;">
                     ${ raw('<a href="javascript:void(0);" title="Clique para ver a versão da ficha." onClick="showFichaVersao({sqFichaVersao:' +
                         item?.taxonHistoricoAvaliacaoVersao?.fichaVersao?.id+'})">V.' + item?.taxonHistoricoAvaliacaoVersao?.fichaVersao?.nuVersao + '</a>') }
                 </span>
             </g:if>
         </td>
         <td class="text-center text-nowrap">
            <Label class="cursor-pointer">
               <input type="checkbox" class="fld checkbox-lg" data-action="historicoAvaliacao.setOficial"
                      data-id="${item.id}"
                      data-params="sqFicha" ${item.stOficial?'checked':''}>&nbsp;
            </Label>
         </td>
         <td class="text-center">${ item.noRegiaoOutra ? 'Outra' : item?.abrangencia?.tipo?.descricao}</td>
         <td class="text-center">${ item.noRegiaoOutra ? item.noRegiaoOutra : item?.abrangencia?.descricao}</td>
         <td class="text-center" data-avaliacao-id="${item?.tipoAvaliacao?.id}" data-avaliacao-tipo="${item.tipoAvaliacao.codigo}" data-avaliacao-ano="${item.nuAnoAvaliacao}">
            <g:if test="${ item?.categoriaIucn }">
               <g:if test="${ item?.categoriaIucn?.codigoSistema != 'OUTRA'}">
                  ${ raw( '<span class="cursor-pointer" title="'+ item?.categoriaIucn?.descricao+'">' +
                        item?.categoriaIucn?.codigo +'</span>' + ( item.stPossivelmenteExtinta=='S' ? '<br><span style="color:blue">(PEX)</span>' : '' )
                     )}
               </g:if>
            </g:if>
            <g:if test="${ item?.categoriaIucn?.codigoSistema == 'OUTRA'}">
               <g:set var="existeCategoriaOutra" value="S"/>
               ${ raw( item.deCategoriaOutra+'<span class="red">*</span>') }
            </g:if>
         </td>
         <td class="text-center">${item.deCriterioAvaliacaoIucn?:''}
         </td>
          <td>
             <g:renderLongText table="taxon_historico_avaliacao" id="${item.id}" column="tx_justificativa_avaliacao" text="${raw(item.txJustificativaAvaliacao)}"/>
             %{--<div style="min-width:200px;max-height: 200px;padding:3px;overflow-x: auto;">${raw(item.txJustificativaAvaliacao)}</div>--}%
            %{-- <span class="badge" title="Lerem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum">
               ...
            </span> --}%
         </td>
         %{-- <td>${item.txRegiaoAbrangencia}</td> --}%
         <td>
            <a data-action="ficha.openModalSelRefBibFicha"
               data-no-tabela="taxon_historico_avaliacao"
               data-no-coluna="sq_taxon_historico_avaliacao"
               data-sq-registro="${item.id}"
               data-de-rotulo="Histórico Avaliação - ${item.tipoAvaliacao.descricao+'/'+item.nuAnoAvaliacao}"
               data-com-pessoal="true"
               data-update-grid="divGridHistoricoAvaliacao"
               class="fld btn btn-default btn-xs btn-grid-subform" title="Adicionar/Remover Refeferência bibliográfica."><span class="fa fa-book"></span></a>
               &nbsp;${ raw(item.getRefBibHtml( vwFicha.id.toLong() ) ) }
         </td>
         <td class="td-actions">
            <g:if test="${ ! session.sicae.user.isCO() }">

               <a data-action="historicoAvaliacao.editHistoricoAvaliacao" data-params="sqFicha" data-id="${item.id}" class="fld btn btn-default btn-xs btn-update" title="Alterar">
                  <span class="glyphicon glyphicon-pencil"></span>
               </a>
               <a data-action="historicoAvaliacao.deleteHistoricoAvaliacao" data-descricao="" data-params="sqFicha" data-id="${item.id}" class="fld btn btn-default btn-xs btn-delete" title="Excluir">
                  <span class="glyphicon glyphicon-remove"></span>
               </a>
            </g:if>
            <g:if test="${ item.idOrigem && item?.tipoAvaliacao?.codigoSistema == 'NACIONAL_BRASIL'}">
                <a data-action="historicoAvaliacao.visualizarFicha"
                   data-params="sqFicha"
                   data-format="pdf"
                   data-id-origem="${item?.idOrigem}"
                   data-id="${item?.id}"
                   class="fld btn btn-default btn-xs btn-update" title="Visutalizar ficha">
                   <span class="glyphicon glyphicon-cloud-download"></span>
                </a>
            </g:if>
         </td>
      </tr>
   </g:each>
   <g:if test="${ existeCategoriaOutra == 'S' }">
      <tr>
         <td colspan="9"><span class="red">*</span>&nbsp;Categoria não utilizada no método <a href="https://cmsdocs.s3.amazonaws.com/keydocuments/Categories_and_Criteria_en_web%2Bcover%2Bbckcover.pdf" target="_blank">IUCN</a>.</td>
      </tr>
   </g:if>
   <g:if test="${ ! listHistorico }">
       <tr>
           <td colspan="9"><small>Nenhum histórico cadastrado!</small></td>
       </tr>
   </g:if>
   </tbody>
</table>
