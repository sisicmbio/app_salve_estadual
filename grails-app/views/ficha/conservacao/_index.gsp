<ul class="nav nav-tabs nav-sub-tabs mt10" id="ulTabsConservacao">
    <li id="liTabConHistoricoAvaliacao" class="active"> <a data-action="ficha.tabClick" Xdata-url="fichaConservacao/tabConHistoricoAvaliacao" data-on-load="historicoAvaliacao.tabConHistoricoAvalicaoInit"
            data-container="tabConHistoricoAvaliacao" data-reload="true" data-params="sqFicha" data-toggle="tab" href="#tabConHistoricoAvaliacao">7.1-Histórico Avaliação</a></li>
    <li id="liTabConListaConvencao"> <a data-action="ficha.tabClick" data-url="fichaConservacao/tabConListaConvencao" data-on-load="listaConvencao.tabConListaConvencaoInit"
            data-container="tabConListaConvencao" data-reload="true" data-params="sqFicha" data-toggle="tab" href="#tabConListaConvencao">7.2-Listas e Convenções</a></li>
    <li id="liTabConAcaoConservacao"><a data-action="ficha.tabClick" data-url="fichaConservacao/tabConAcaoConservacao" data-on-load="acaoConservacao.tabConAcaoConservacaoInit"
            data-container="tabConAcaoConservacao" data-reload="false" data-params="sqFicha" data-toggle="tab" href="#tabConAcaoConservacao">7.3-Ações de Conservação</a></li>
    <li id="liTabConPresencaUc"> <a data-action="ficha.tabClick" data-url="fichaConservacao/tabConPresencaUC" data-on-load="presencaUc.tabConPresencaUcInit"
            data-container="tabConPresencaUc" data-reload="true" data-params="sqFicha" data-toggle="tab" href="#tabConPresencaUc">7.4-Presença em UC / TI</a></li>
    <li id="liTabSituacaoRegional"><a data-action="ficha.tabClick" data-url="fichaConservacao/tabSituacaoRegional"
            data-container="tabConSituacaoRegional" data-reload="false" data-params="sqFicha" data-toggle="tab" href="#tabConSituacaoRegional">7.5 - Situação Regional</a></li>

</ul>
<div id="conTabContent" class="tab-content">
    %{-- inicio aba conservação Historico das Avaliações --}%
    <div role="tabpanel" style="min-height:400px;" class="tab-pane tab-pane-ficha active" id="tabConHistoricoAvaliacao">
        <g:render template="/ficha/conservacao/historicoAvaliacao/formHistoricoAvaliacao" model="['ficha':ficha, 'listTipoAvaliacao':listTipoAvaliacao, 'listTipoAbrangencia':listTipoAbrangencia,'listCategoriaIucn':listCategoriaIucn]"></g:render>
    </div>

    %{-- fim aba conservação Listas e Convenções --}%
    <div role="tabpanel" style="min-height:400px;" class="tab-pane tab-pane-ficha" id="tabConListaConvencao"> %{--
    <g:render template="conservacao/listaConvencao/formListaConvencao"></g:render> --}%
    </div>

    <%-- fim aba conservação Ações de Conservação --%>
    <div role="tabpanel" style="min-height:400px;" class="tab-pane tab-pane-ficha" id="tabConAcaoConservacao"> %{--
    <g:render template="conservacao/acaoConservacao/formAcaoConservacao"></g:render> --}%
    </div>

    <%-- fim aba conservação Presença em Uc --%>
    <div role="tabpanel" style="min-height:400px;" class="tab-pane tab-pane-ficha" id="tabConPresencaUc"> %{--
    <g:render template="conservacao/presencaUc/formPresencaUc"></g:render> --}%
    </div>

    <%-- aba conservação Situacao regional --%>
    <div role="tabpanel" style="min-height:400px;" class="tab-pane tab-pane-ficha" id="tabConSituacaoRegional"></div>

<%-- fim aba conservação ambiente restrito--%>
</div>
<%--fim col-sm-12 --%>
