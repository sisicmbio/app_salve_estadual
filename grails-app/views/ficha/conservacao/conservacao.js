//# sourceURL=conservacao.js
;
var historicoAvaliacao = {
    init: function() {
        historicoAvaliacao.tabConHistoricoAvalicaoInit();
        setTimeout(function() {
            historicoAvaliacao.sqTipoAvaliacaoChange();
        }, 1000);
    },
    tabConHistoricoAvalicaoInit: function() {
        historicoAvaliacao.updateGridHistoricoAvaliacao();
    },
    savePresencaListaVigente: function( params ) {
        var stPresencaListaVigente = $("#stPresencaListaVigente").is(':checked') ?'S' : 'N' ;
        var data = { sqFicha:params.sqFicha, stPresencaListaVigente:stPresencaListaVigente};
        app.ajax(app.url + 'fichaConservacao/savePresencaListaVigente', data, function( res ) {
            if ( ! res.status ) { }
        }, 'Atualizando informações da ficha...', 'JSON');
    },
    sqTipoAvaliacaoChange: function() {
        var codigo = $("#sqTipoAvaliacao option:selected").data('codigo');
        $("#frmConHistorico #sqAbrangencia").parent().removeClass('hide');
        $("#frmConHistorico #sqTipoAbrangencia").parent().removeClass('hide');
        $("#frmConHistorico #noRegiaoOutra").parent().addClass('hide');
        $("#frmConHistorico #sqTipoAbrangencia option").removeClass('hide');

        // desabilitar a opção outra da categoria
        if( codigo == 'NACIONAL_BRASIL') {
            $("#frmConHistorico #sqCategoriaIucn option[data-codigo=OUTRA]").prop('disabled', true)
            // se a categoria selecionada for OUTRA tem que limpar
            if( $("#frmConHistorico #sqCategoriaIucn option:selected").data('codigo')=='OUTRA') {
                $("#frmConHistorico #sqCategoriaIucn").val('');
                $("#frmConHistorico #sqCategoriaIucn").change();
            }
        }
        else
        {
            $("#frmConHistorico #sqCategoriaIucn option[data-codigo=OUTRA]").prop('disabled',false)
        }

        if (codigo == 'ESTADUAL') {

            $("#frmConHistorico #sqTipoAbrangencia option").each(function() {
                if ($(this).data('codigo') && $(this).data('codigo') != 'ESTADO') {
                    $(this).addClass('hide');
                    $(this).prop('selected',false);
                }
                else {
                    $(this).prop('selected',true);
                    $(this).change(); // habilitar o campo de selecao do estado
                }
            });
            if ($("#frmConHistorico #sqTipoAbrangencia option:selected").data('codigo') != 'ESTADO') {
                $("#frmConHistorico #sqTipoAbrangencia").val(String.Empty);
                $("#frmConHistorico #sqAbrangencia").empty();
            }
        } else if (codigo == 'GLOBAL' || codigo == 'NACIONAL_BRASIL' || !codigo) {
            $("#frmConHistorico #sqAbrangencia").parent().addClass('hide');
            $("#frmConHistorico #sqTipoAbrangencia").parent().addClass('hide');
        } else if (codigo == 'NACIONAL_OUTRO_PAIS') {
            $("#frmConHistorico #sqTipoAbrangencia option").each(function() {
                if ($(this).data('codigo') && $(this).data('codigo') != 'PAIS') {
                    $(this).addClass('hide');
                }
            });
            if ($("#frmConHistorico #sqTipoAbrangencia option:selected").data('codigo') != 'PAIS') {
                $("#frmConHistorico #sqTipoAbrangencia").val(String.Empty);
                $("#frmConHistorico #sqAbrangencia").empty();
            }
        } else if (codigo == 'REGIONAL') {
            $("#sqTipoAbrangencia option").each(function() {
                var codigo = $(this).data('codigo');
                if (codigo && /ESTADO|PAIS/.test(codigo)) {
                    $(this).addClass('hide');
                }
            });
        }
    },
    saveFrmConHistorico: function(params) {
        if (!$('#frmConHistorico').valid()) {
            return false;
        }
        if ($("#frmConHistorico #sqTipoAbrangencia").parent().hasClass('hide')) {
            $("#frmConHistorico #sqTipoAbrangencia,#frmConHistorico #sqAbrangencia,#frmConHistorico #noRegiaoOutra").val(String.Empty);
        }
        if ($("#frmConHistorico #noRegiaoOutra").parent().hasClass('hide')) {
            $("#frmConHistorico #noRegiaoOutra").val(String.Empty);
        }
        if( ! $("#frmConHistorico #deCriterioAvaliacaoIucn").is(':visible') )
        {
            $("#frmConHistorico #deCriterioAvaliacaoIucn").val('');
        }
        else
        {
            if( $("#frmConHistorico #deCriterioAvaliacaoIucn").val() == '' && $("#sqTipoAvaliacao option:selected").data('codigo') == 'NACIONAL_BRASIL' )
            {
                app.alertInfo('Necessário informar o critério!');
                return;
            }
        }

        // campo outra
        if( $("#frmConHistorico #deCategoriaOutra").is(':visible') )
        {
            if( $("#frmConHistorico #deCategoriaOutra").val().trim() == '' )
            {
                app.alertInfo('Necessário informar a outra categoria!');
                return;
            }
        }
        else {
            $("#frmConHistorico #deCategoriaOutra").val('');
        }

        var data = $("#frmConHistorico").serializeAllArray();
        if (!data) {
            return;
        }
        data = app.params2data(params, data);

        if( $("#frmConHistorico #sqCategoriaIucn option:selected").data('codigo') == 'CR' )
        {
            data.stPossivelmenteExtinta = data.stPossivelmenteExtinta =='S' ? 'S' : 'N';
        }
        else
        {
            data.stPossivelmenteExtinta = '';
        }

        // a função ficha.abrangenciaChange altera o name do campo, então pegar pelo id
        data.sqAbrangencia = $("#frmConHistorico #sqAbrangencia").val();
        if (!data.sqFicha) {
            app.alertError('ID da ficha não encontrado!');
            return;
        }
        app.ajax(app.url + 'fichaConservacao/saveFrmConHistorico', data,
            function(res) {
                if (res.status == 0) {
                    // ao salvar historico de avaliacao uma Avaliacao Nacional, limpar as abas da Avaliação
                    // devido ao controle da mudança de categoria para exibir o formulário
                    if ($("#sqTipoAvaliacao option:selected").data('codigo') == 'NACIONAL_BRASIL') {
                        $("#tabAvaResultadoFinal").html('');
                        $("#tabAvaResultado").html('');
                        app.resetChanges('tabAvaResultadoFinal');
                        app.resetChanges('tabAvaResultado');
                    }
                    historicoAvaliacao.resetFrmConHistorico();
                    historicoAvaliacao.updateGridHistoricoAvaliacao();
                }
            }, null, 'json'
        );
    },
    resetFrmConHistorico: function() {
        app.reset('frmConHistorico');
        $("#sqTipoAvaliacao").focus();
        $("#btnResetFrmConHistorico").addClass('hide');
        $("#btnSaveFrmConHistorico").html($("#btnSaveFrmConHistorico").data('value'));
        $("#frmConHistorico #sqTipoAbrangencia").change();
        historicoAvaliacao.sqTipoAvaliacaoChange();
        app.unselectGridRow('divGridHistoricoAvaliacao');
    },
    updateGridHistoricoAvaliacao: function() {
        ficha.getGrid('historicoAvaliacao', null, function() {
            // atualizar informações da aba 11.5 - Resultado
            /* verificar se ainda precisa disso
            if (typeof avaliacao == 'object') {
                categoriaIucnChange(); // fichaCompleta.js
            }
            */
        });
    },
    editHistoricoAvaliacao: function(params, btn) {
        app.ajax(app.url + 'fichaConservacao/editHistoricoAvaliacao', params,
            function(res) {
                $("#btnResetFrmConHistorico").removeClass('hide');
                $("#btnSaveFrmConHistorico").data('value', $("#btnSaveFrmConHistorico").html());
                $("#btnSaveFrmConHistorico").html("Gravar Alteração");
                app.setFormFields(res, 'frmConHistorico');
                $("#frmConHistorico #sqAbrangencia").data('currentValue', res.sqAbrangencia);
                app.selectGridRow(btn);
                // tratar tipo outra
                if (!res.sqAbrangencia) {
                    $("#frmConHistorico #sqTipoAbrangencia option[data-codigo='OUTRA']").prop('selected', true);
                }
                historicoAvaliacao.sqTipoAvaliacaoChange(); // restringer os tipos de abrangencias
                tipoAbrangenciaChange({
                    contexto: 'frmConHistorico'
                }); // mostrar input outra

                // chamar o setFormFields novamente para alimentar os campos selects2 filho da abrangencia
                app.setFormFields(res, 'frmConHistorico');

                app.categoriaIucnChange({sqFicha:params.sqFicha,container:'frmConHistorico'}); // mostrar/esconder o campo criterio
                //$("#frmConHistorico #sqTipoAbrangencia").change();
                // abrir o formulário
                if( !$("#frmConHistorico").hasClass('in') )
                {
                    $("#btnShowHideFrmConHistorico").click();
                    app.focus("sqTipoAvaliacao","frmConHistorico");
                }
            });
    },
    deleteHistoricoAvaliacao: function(params, btn) {
        app.selectGridRow(btn);
        app.confirm('<br>Confirma exclusão do registro?',
            function() {
                app.ajax(app.url + 'fichaConservacao/deleteHistoricoAvaliacao',
                    params,
                    function(res) {
                        historicoAvaliacao.updateGridHistoricoAvaliacao();
                        historicoAvaliacao.resetFrmConHistorico();
                    });
            },
            function() {
                app.unselectGridRow(btn);
            }, params, 'Confirmação', 'warning');
    },
    saveObservacao:function( params, btn, evt ) {
        var data = $("#frmConHistoricoObs").serializeAllArray();
        if (!data) {
            return;
        }
        data = app.params2data(params, data);
        app.ajax(app.url + 'fichaConservacao/saveFrmConHistoricoObs', data,function(res) {
            app.resetChanges('tabConHistoricoAvaliacao');
        });
    },
    setOficial:function(params,ele,evt){
        evt.preventDefault();
        var data = app.params2data({},params);
        app.ajax(app.url + 'fichaConservacao/saveOficial', data,function(res) {
            if( res.status==0 ){
                $(ele).prop('checked',res.data.stOficial);
            }
        });
    },
    visualizarFicha:function(params,ele,evt) {
        var data = app.params2data({}, params);
        // recuperar o link da API do Estado que gera o pdf da ficha
        app.ajax(app.url + 'fichaConservacao/visualizarFicha', data, function (res) {
            if (res.status == 0) {
                if( res.data.urlDownload ){
                    // faz o download do pdf de acordo com o link do Estado ou Salve ICMBio
                    app.ajax(res.data.urlDownload, data, function (res) {
                        if( res.link) {
                            window.open(res.link, '_top');
                        }
                    },'Lendo a ficha...');
                }
            }
        });
    },
};

// ------------------------------------------

var listaConvencao = {
    tabConListaConvencaoInit: function() {
        //listaConvencao.stPresencaListaVigenteChange();
        listaConvencao.updateGridListaConvencao();
    },
    /*saveFrmConListaConvencao: function(params) {
        if (!$('#frmConListaConvencao').valid()) {
            return false;
        }
        var data = $("#frmConListaConvencao").serializeArray();
        if (!data) {
            return;
        }
        data = app.params2data(params, data);
        if (!data.sqFicha) {
            app.alertError('ID da ficha não encontrado!');
            return;
        }
        app.ajax(app.url + 'fichaConservacao/saveFrmConListaConvencao', data,
            function(res) {
                if (res.status == 0) {
                    app.resetChanges('frmConListaConvencao');
                }
            }, null, 'json'
        );
    },*/
    /*stPresencaListaVigenteChange: function(e) {
        var value = $("#stPresencaListaVigente").val();
        if (value == 'S' ) {
            $("#sqAtoOficial").parent().removeClass('hidden');
        } else {
            $("#sqAtoOficial").parent().addClass('hidden');
        }
    },*/
    saveFrmConListaConvencaoDetalhe: function(params) {
        if (!$('#frmConListaConvencaoDetalhe').valid()) {
            return false;
        }
        var data = $("#frmConListaConvencaoDetalhe").serializeArray();
        if (!data) {
            return;
        }
        data = app.params2data(params, data);
        if (!data.sqFicha) {
            app.alertError('ID da ficha não encontrado!');
            return;
        }
        app.ajax(app.url + 'fichaConservacao/saveFrmConListaConvencaoDetalhe', data,
            function(res) {
                listaConvencao.updateGridListaConvencao();
                app.reset('frmConListaConvencaoDetalhe');
                $("#sqListaConvencao").focus();
            }, null, 'json'
        );
    },
    updateGridListaConvencao: function() {
        ficha.getGrid('listaConvencao');
    },
    deleteListaConvencao: function(params, btn) {
        app.selectGridRow(btn);
        app.confirm('<br>Confirma exclusão da lista/convenção ' + params.descricao + '?',
            function() {
                app.ajax(app.url + 'fichaConservacao/deleteListaConvencao',
                    params,
                    function(res) {
                        listaConvencao.updateGridListaConvencao();
                    });
            },
            function() {
                app.unselectGridRow(btn);
            }, params, 'Confirmação', 'warning');
    },
};

var acaoConservacao = {
    tabConAcaoConservacaoInit: function() {
        acaoConservacao.updateGridAcaoConservacao();
        acaoConservacao.sqAcaoConservacaoChange();
    },
    saveFrmConAcaoConservacao: function(params) {
        if (!$('#frmConAcaoConservacao').valid()) {
            return false;
        }
        var data = $("#frmConAcaoConservacao").serializeArray();
        if (!data) {
            return;
        }
        data = app.params2data(params, data);
        if (!data.sqFicha) {
            app.alertError('ID da ficha não encontrado!');
            return;
        }
        app.ajax(app.url + 'fichaConservacao/saveFrmConAcaoConservacao', data,
            function(res) {
                //	acaoConservacao.updateGridAcaoConservacao();
                if (res.status == 0) {
                    app.resetChanges('frmConAcaoConservacao');
                }
            }, null, 'json'
        );
    },
    updateGridAcaoConservacao: function() {
        ficha.getGrid('acaoConservacao');
    },
    sqAcaoConservacaoChange: function(e) {
        var data = $("#frmConAcaoConservacaoDetalhe #sqAcaoConservacao option:selected").data();
        if (data) {
            var codigoSistema = data.codigoSistema ? data.codigoSistema : data.codigo;
            if (codigoSistema == 'PAN') {
                $("#frmConAcaoConservacaoDetalhe #sqPlanoAcao").parent().show();
            } else {
                $("#frmConAcaoConservacaoDetalhe #sqPlanoAcao").parent().hide();
            }
            if (codigoSistema == 'MANEJO_ORDENAMENTO_CACA_PESCA') {
                $("#frmConAcaoConservacaoDetalhe #sqTipoOrdenamento").parent().show();
            } else {
                $("#frmConAcaoConservacaoDetalhe #sqTipoOrdenamento").parent().hide();
            }
        } else {
            $("#frmConAcaoConservacaoDetalhe #sqTipoOrdenamento").parent().hide();
            $("#frmConAcaoConservacaoDetalhe #sqPlanoAcao").parent().hide();
        }
    },
    saveFrmConAcaoConservacaoDetalhe: function(params) {
        if (!$('#frmConAcaoConservacaoDetalhe').valid()) {
            return false;
        }
        var data = $("#frmConAcaoConservacaoDetalhe").serializeArray();
        if (!data) {
            return;
        }
        data = app.params2data(params, data);
        if (!data.sqFicha) {
            app.alertError('ID da ficha não encontrado!');
            return;
        }
        app.ajax(app.url + 'fichaConservacao/saveFrmConAcaoConservacaoDetalhe', data,
            function(res) {
                if (res.status == 0) {
                    acaoConservacao.updateGridAcaoConservacao();
                    acaoConservacao.resetFrmConAcaoConservacaoDetalhe();
                }
            }, null, 'json'
        );
    },
    editAcaoConservacao: function(params, btn) {
        app.ajax(app.url + 'fichaConservacao/editAcaoConservacao', params,
            function(res) {
                $("#btnResetFrmConAcaoConservacaoDetalhe").removeClass('hide');
                $("#btnSaveFrmConAcaoConservacaoDetalhe").data('value', $("#btnSaveFrmConAcaoConservacaoDetalhe").html());
                $("#btnSaveFrmConAcaoConservacaoDetalhe").html("Gravar Alteração");
                app.setFormFields(res, 'frmConAcaoConservacaoDetalhe');
                app.selectGridRow(btn);
            });
    },
    deleteAcaoConservacao: function(params, btn) {
        app.selectGridRow(btn);
        app.confirm('<br>Confirma exclusão da ação de conservação ' + params.descricao + '?',
            function() {
                app.ajax(app.url + 'fichaConservacao/deleteAcaoConservacao',
                    params,
                    function(res) {
                        acaoConservacao.updateGridAcaoConservacao();
                    });
            },
            function() {
                app.unselectGridRow(btn);
            }, params, 'Confirmação', 'warning');
    },
    resetFrmConAcaoConservacaoDetalhe: function() {
        app.reset('frmConAcaoConservacaoDetalhe');
        $("#sqAcaoConservacao").focus();
        $("#btnResetFrmConAcaoConservacaoDetalhe").addClass('hide');
        $("#btnSaveFrmConAcaoConservacaoDetalhe").html($("#btnSaveFrmConAcaoConservacaoDetalhe").data('value'));
        app.unselectGridRow('divGridAcaoConservacao');
        acaoConservacao.sqAcaoConservacaoChange();
    },
};

var presencaUc = {
    tabConPresencaUcInit: function() {
        presencaUc.updateGridPresencaUc();
        /*
         window.setTimeout(
         function () {
         $("#divAlertUc").hide('slow', function () {
         $("#divAlertUc").remove();
         });
         }, 15000);
         */
    },
    recalcularUcs:function( params ){
        var data = { sqFicha: $("#sqFicha").val() };
        if( !params.confirmacao ) {
            app.confirm('Confirma atualização das Unidades de Conservação?', function () {
                presencaUc.recalcularUcs({confirmacao:'S'})
            })
            return;
        }
        app.blockUI('Lendo registros utilizados. Aguarde...',function() {
            app.ajax(app.url + 'fichaConservacao/recalcularUcs', data, function (res) {
                app.unblockUI();
                if (!res.status) {
                    window.setTimeout(function () {
                        getWorkers();
                    }, 3000)
                }
            }, '', 'JSON');
        });
    },
    updateGridPresencaUc: function() {
        if ($("#divGridPresencaUc").size() > 0) {
            ficha.getGrid('presencaUc',null,function(){
                $("#consTablePresencaUcs").floatThead('destroy');
                var $table1 = $("#consTablePresencaUcs")
                $table1.floatThead({
                    scrollContainer: function( $table ) {
                        return $table.closest('div');
                    }
                });
            });
        }
    },
    saveFrmConPresencaUc: function(params) {
        if (!$('#frmConPresencaUc').valid()) {
            return false;
        }
        var data = $("#frmConPresencaUc").serializeArray();
        if (!data) {
            return;
        }
        data = app.params2data(params, data);
        if (!data.sqFicha) {
            app.alertError('ID da ficha não encontrado!');
            return;
        }
        app.ajax(app.url + 'fichaConservacao/saveFrmConPresencaUC', data,
            function(res) {
                if (res.status == 0) {
                    app.resetChanges('frmConPresencaUc');
                }
            }, null, 'json'
        );
    },
    selAbaCadUc: function(params) {
        if ($("#frmDistribuicao").size() == 0) {
            app.selectTab("tabDistribuicao"); // carregar o form
            window.setTimeout(function(params) {
                presencaUc.selAbaCadUc(params);
            }, 1000);
        } else {
            app.selectTab("tabDistribuicao"); // selecionar a aba
            if ($("#divSubAbasOcorrencia").size() == 0) {
                app.selectTab("tabDisOcorrencia") // selecionar a aba ocorrencias
                window.setTimeout(function(params) {
                    presencaUc.selAbaCadUc(params);
                }, 1000);
            } else {
                if ($("#frmDisOcoGeoreferencia").size() > 0) {
                    if ($("#sqFichaOcorrencia").val()) {
                        $("#btnCancelarEdicao").click();
                        $("#btnEditarOcorrencia").click() // clicar botão cadastrar
                    }
                } else {
                    app.selectTab("tabDisOcorrencia") // selecionar a aba ocorrencias
                    $("#btnEditarOcorrencia").click() // clicar botão cadastrar
                }
            }
        }
    },
};
historicoAvaliacao.init();
