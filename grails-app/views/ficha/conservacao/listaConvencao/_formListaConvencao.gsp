%{--<g:set var="oSN"  value="${[ [key:'S',value:'Sim'], [key:'N', value:'Não'] ]}"/>--}%
%{--ESTE FORMULÁRIO PASSOU PARA ABA 7.1 --}%
%{--
<form name="frmConListaConvencao" id="frmConListaConvencao" role="form" class="form-inline hidden">
	<div class="fld form-group">
		<!-- Este campo grava na tb_ficha - st_presenca_lista_vigente -->
		<label for="" class="control-label">Presença na Lista Oficial Nacional Vigente?</label>
		<br>
		<select name="stPresencaListaVigente" id="stPresencaListaVigente" data-change="listaConvencao.stPresencaListaVigenteChange"
				class="fld form-control fld300 fldSelect" required="true">
			<option value="">-- selecione --</option>
         	 <g:each var="item" in="${oSN}">
                  <option value="${item.key}" ${ficha?.stPresencaListaVigente==item.key ? ' selected' :'' }>${item.value}</option>
             </g:each>
		</select>
	</div>

	<div class="form-group">
         <label class="control-label">Ato Oficial</label>
         <br>
         <select id="sqAtoOficial" name="sqAtoOficial" class="fld form-control select2 fld600"
            data-s2-url="ficha/select2RefBib"
            data-s2-params="inListaOficialVigente:true"
            data-s2-placeholder="Buscar por... ( min 3 caracteres )"
            data-s2-minimum-input-length="3"
            data-s2-template="refBibTemplate"
            data-s2-auto-height="true">
         <g:if test="${atoOficial}">
         	<option value="${atoOficial.id}" selected>${atoOficial?.publicacao?.tituloAutorAno}</option>
         </g:if>
         </select>
         <a data-action="modal.cadRefBib" class="btn btn-sm btn-default" title="Cadastrar Ato Oficial">
            <span class="glyphicon glyphicon glyphicon-plus" aria-hidden="true"></span>
         </a>
      </div>

	<!-- botão gravar -->
	<div class="fld panel-footer">
		<button data-action="listaConvencao.saveFrmConListaConvencao" data-params="sqFicha" class="fld btn btn-success">Gravar</button>
	</div>
</form>
--}%
<form id="frmConListaConvencaoDetalhe" role="form" class="form-inline">
    <g:if test="${fichaSincronismoCites}">
        <span class="pull-right mr10 gray">Última verificação <a href="https://speciesplus.net/species#/taxon_concepts?taxonomy=cites_eu&taxon_concept_query=${br.gov.icmbio.Util.ncItalico(ficha.nmCientifico,true,false)}&geo_entities_ids=&geo_entity_scope=cites&page=1" target="_blank">SPECIES+/CITES</a> realizada em:&nbsp;${fichaSincronismoCites.dtSincronismo.format('dd/MM/yyyy')}</span>
    </g:if>
	<div class="fld form-group">
		<label for="sqListaConvencao" class="control-label">Lista/Convenção</label>
		<br>
		<select id="sqListaConvencao" name="sqListaConvencao"class="fld form-control fld600 fldSelect" required="true">
			<option value="">-- Selecione --</option>
			<g:each var="item" in="${listListasConvencoes}">
				<option value="${item.id}">${item.descricao}</option>
			</g:each>
		</select>
	</div>

	<div class="fld form-group">
		<label class="control-label">Ano</label>
		<br>
		<input name="nuAno" type="text" class="fld form-control number fld100" data-mask="0000"/>
	</div>

	%{-- botão gravar --}%
	<div class="fld panel-footer">
		<button data-action="listaConvencao.saveFrmConListaConvencaoDetalhe" data-params="sqFicha" class="fld btn btn-success">Adicionar lista/convenção</button>
	</div>
</form>

<div id="divGridListaConvencao" class="hidden mt10">
	%{-- <g:render template="/ficha/conservacao/divGridListaConvencao"></g:render> --}%
</div>
