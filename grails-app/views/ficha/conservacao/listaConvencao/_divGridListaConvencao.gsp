<table class="table table-striped table-bordered table-hover">
   <thead>
      <tr>
         <th>Presença em Convenção</th>
         <th>Ano</th>
         <g:if test="${ ! session.sicae.user.isCO() }">
            <th>Ação</th>
         </g:if>
      </tr>
   </thead>
   <tbody>
   <g:each var="item" in="${listFichaListaConvencao}">
      <tr>
         <td>${item?.listaConvencao?.descricao}</td>
         <td class="text-center">${item.nuAno}</td>
          <br>
            %{-- Regra alterada mediante solicitação do Coordenador juntamente com equipe em 03/Jun/2022 --}%
            <g:if test="${item?.listaConvencao?.codigo?.toString() == 'LISTA_OFICIAL_AMECADA_2014_2015' || item?.listaConvencao?.codigo?.toString() == 'LISTA_OFICIAL_AMECADA_2022' }">
               <g:if test="${ session.sicae.user.isADM() }">
                  <td class="td-actions">
                     <a data-id="${item.id}" data-action="listaConvencao.deleteListaConvencao"
                        data-descricao="${item?.listaConvencao?.descricao+' - '+item?.listaConvencao?.codigo}"
                        class="fld btn btn-default btn-xs btn-delete" title="Excluir"><span class="glyphicon glyphicon-remove"></span></a>
                  </td>
               </g:if>
            </g:if>
            <g:elseif test="${ ! session.sicae.user.isCO() }">
               <td class="td-actions">
                  <a data-id="${item.id}" data-action="listaConvencao.deleteListaConvencao"
                     data-descricao="${item?.listaConvencao?.descricao+' - '+item?.listaConvencao?.codigo}"
                     class="fld btn btn-default btn-xs btn-delete" title="Excluir"><span class="glyphicon glyphicon-remove"></span></a>
               </td>
            </g:elseif >
      </tr>
   </g:each>
   </tbody>
</table>
