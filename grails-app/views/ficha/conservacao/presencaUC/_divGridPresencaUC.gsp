<!-- view: /views/ficha/conservacao/_divGridPresencaUc.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<div style="height:auto;max-height: 300px;overflow: auto" >
    <table class="table table-striped table-bordered table-hover" id="consTablePresencaUcs">
        <thead>
        <tr>
            %{--<th>Unidade(s) de Conservação</th> --}%
            <th style="width:400px;">Sigla</th>
            <th style="width:200px">Estado</th>
            <th style="width:200px">Presença Atual</th>
            <th style="width:auto">Ref. Bibliográfica</th>
        </tr>
        </thead>
        <tbody>
        <g:set var="quebraEsfera"  value="*"/>
        <g:each var="item" in="${listUcsRegistros}">
            <g:if test="${item.cd_esfera != quebraEsfera}">
                <tr class="info table-topic">
                    <td colspan="4">
                        <g:set var="quebraEsfera"  value="${item.cd_esfera}"/>
                        ${ br.gov.icmbio.Util.nomeEsfera( item.cd_esfera, true ) }
                    </td>
                </tr>
            </g:if>
            <tr>
                <td>${ item.no_uc }</td>
                <td>${ item.no_estado ? item?.no_estado : '' }</td>
                <td class="text-center">${ item.de_presenca_atual }</td>
                <td>
                    ${raw( br.gov.icmbio.Util.parseRefBib2Grid( item.json_ref_bib.toString() ) ) }
                </td>
            </tr>
        </g:each>
        </tbody>
    </table>
</div>
