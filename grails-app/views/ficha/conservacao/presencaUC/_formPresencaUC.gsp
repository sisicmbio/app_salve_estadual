<div id="divAlertUc" class="alert alert-info" style="margin:5px;">
	<strong>Informação!</strong>&nbsp;O cadastro das UCs deve ser realizado na aba 2-Distribuição, sub-aba <a href="#" data-action="presencaUc.selAbaCadUc">2.6.1 - Georreferenciamento</a>.
%{--    <span class="pull-right"--}%
%{--           title="Atualizar a lista de UCs de acordo com os registros &quot;Utilizados na Avaliação&quot;."><a href="javascript:void(0);" data-action="presencaUc.recalcularUcs">Atualizar UCs&nbsp;<i class="fa fa-refresh"></i></a></span>--}%
</div>

<div class="alert alert-success" style="margin:5px;display:none;">
	<strong>Dica!</strong>&nbsp;Cadastre apenas Unidades com ocorrência comprovada da espécie, e sempre indique a referência bibliográfica (aba 2.6.6), podendo ser “comunicação pessoal”. Caso o ponto exato de coleta não seja conhecido, pode ser usado a coordenada do centróide ou uma coordenada genérica dentro da Unidade. O campo Presença Atual na Coordenada (aba 2.6.2) é obrigatório quando for selecionada uma UC. Casos de provável ocorrência, por exemplo quando a distribuição da espécie é ampla e sobrepõe várias unidades, não devem ser cadastradas. Esses casos podem ser detalhados no campo descritivo abaixo.
</div>

<div id="divGridPresencaUc" class="mt10">
    <span>Carregando gride UCs...<br></span>
	%{-- <g:render template="/ficha/conservacao/presencaUC/divGridPresencaUC"></g:render> --}%
</div>

<form id="frmConPresencaUc" role="form" class="form-inline">
	%{-- Observação da tabela ficha ds_presenca_uc --}%
	<div class="form-group w100p">
    <label for="dsPresencaUc" class="control-label">Observações gerais sobre a presença em UC
        <i data-action="ficha.openModalSelRefBibFicha"
           data-no-tabela="ficha"
           data-no-coluna="ds_presenca_uc"
           data-sq-registro="${ficha.id}"
           data-de-rotulo="Presença em UC" class="fa fa-book ml10"
           title="Informar Referência Bibliográfica" style="color: green;" />
    </label>

	<g:if test="${ ! session.sicae.user.isCO() }">
		<div class="checkbox ml20">
			<label title="Clique aqui para preencher o campo com o texto">
				<input type="checkbox" data-action="app.setDefaultText" data-target="dsPresencaUc"
					${ ficha?.dsPresencaUc?.toLowerCase()?.indexOf('n&atilde;o foram encontradas informa&ccedil;&otilde;es para o táxon.') > -1 ? 'checked' : '' }
					   class="checkbox-lg">&nbsp;&nbsp;Preencher com: "N&atilde;o foram encontradas informa&ccedil;&otilde;es para o táxon."
			</label>
		</div>
	</g:if>

    <br>
		<textarea name="dsPresencaUc" id="dsPresencaUc" rows="7" class="fld form-control fldTextarea wysiwyg w100p" required="true">${ficha.dsPresencaUc}</textarea>
	</div>

	%{-- botão gravar na ficha --}%
	<div class="fld panel-footer">
		<button data-action="presencaUc.saveFrmConPresencaUc" data-params="sqFicha" class="fld btn btn-success">Gravar Observação</button>
	</div>
</form>
