<!-- view: /views/ficha/conservacao/_divGridAcaoConservacao.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<table class="table table-striped table-bordered table-hover">
   <thead>
      <tr>
         <th>Ação Conservação</th>
         <th>Situação</th>
         <th>Plano de Ação</th>
         <th>Ordenamento</th>
         %{-- <th>Observações</th> --}%
         <th style="width: 200px;">Ref. Bibliográfica</th>
         <g:if test="${ ! session.sicae.user.isCO() }">
            <th>Ação</th>
         </g:if>
      </tr>
   </thead>
   <tbody>
   <g:each var="item" in="${listFichaAcaoConservacao}">
      <tr>
         <td>${item?.acaoConservacao?.descricaoCompleta}</td>
         <td>${item?.situacaoAcaoConservacao?.descricao}</td>
         <td>${item?.planoAcao?.sgPlanoAcao}</td>
         <td>${item?.tipoOrdenamento?.descricao}</td>
         %{-- <td>${item.txFichaAcaoConservacao}</td> --}%
         <td>
            <a data-action="ficha.openModalSelRefBibFicha"
               data-no-tabela="ficha_acao_conservacao"
               data-no-coluna="sq_ficha_acao_conservacao"
               data-sq-registro="${item.id}"
               data-de-rotulo="Ação de Conservação - ${item.acaoConservacao.descricaoCompleta}"
               data-com-pessoal="true"
               data-update-grid="divGridAcaoConservacao"
               class="fld btn btn-default btn-xs btn-grid-subform" title="Adicionar/Remover Refeferência bibliográfica."><span class="fa fa-book"></span></a>
               &nbsp;
               ${raw(item.refBibHtml)}
         </td>
         <g:if test="${ ! session.sicae.user.isCO() }">
            <td class="td-actions">
               <a data-action="acaoConservacao.editAcaoConservacao"  data-id="${item.id}" class="fld btn btn-default btn-xs btn-update" title="Alterar">
                  <span class="glyphicon glyphicon-pencil"></span>
               </a>
               <a data-action="acaoConservacao.deleteAcaoConservacao" data-descricao="${item.acaoConservacao.descricaoCompleta}" data-id="${item.id}" class="fld btn btn-default btn-xs btn-delete" title="Excluir">
                  <span class="glyphicon glyphicon-remove"></span>
               </a>
            </td>
         </g:if>
      </tr>
   </g:each>
   </tbody>
</table>
