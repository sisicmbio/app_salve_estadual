<form id="frmConAcaoConservacaoDetalhe" role="form" class="form-inline">
	<input name="sqFichaAcaoConservacao"  type="hidden" value="">
	<div class="fld form-group">
		<label for="" class="control-label">Ação de Conservação</label>
		<br>
           %{-- data-s2-url="fichaConservacao/select2AcaoConservacao" --}%
        <select name="sqAcaoConservacao" id="sqAcaoConservacao"
        	class="fld form-control select2 fld800"
           data-s2-minimum-input-length="0"
           data-s2-auto-height="true"
           data-s2-placeholder="Ação de Conservação"
           data-s2-on-change="acaoConservacao.sqAcaoConservacaoChange" required="true">
           	<option data-codigo="" value="">?</option>
            <g:each var="item" in="${listAcaoConservacao}">
            	<option data-codigo="${item.codigoSistema}" value="${item.id}">${item.descricaoCompleta}</option>
 	            <g:each var="itemFilho" in="${item.itens.sort{it.codigo}}">
	           		<option data-codigo="${itemFilho.codigoSistema}" value="${itemFilho.id}">&nbsp;&nbsp;${itemFilho.descricaoCompleta}</option>
		 	            <g:each var="itemNeto" in="${itemFilho.itens.sort{it.codigo}}">
	 	           			<option data-codigo="${itemNeto.codigoSistema}" value="${itemNeto.id}">&nbsp;&nbsp;&nbsp;&nbsp;${itemNeto.descricaoCompleta}</option>
			    		</g:each>
		    	</g:each>
           </g:each>
       </select>
	</div>

	<div class="fld form-group">
		<label for="" class="control-label">Situação da Ação</label>
		<br>
		<select name="sqSituacaoAcaoConservacao" class="fld form-control fld300 fldSelect" required="true">
			<option value="">-- selecione --</option>
			<g:each var="item" in="${listSituacaoAcaoConservacao}">
				<option value="${item.id}">${item.descricao}</option>
			</g:each>
		</select>
	</div>

	<div class="fld form-group">
		<label for="" class="control-label">Plano de Ação</label>
		<br>
        <select name="sqPlanoAcao" id="sqPlanoAcao" class="fld form-control select2 fld800"
           data-s2-url="fichaConservacao/select2PlanoAcao"
           data-s2-minimum-input-length="0"
           data-s2-auto-height="true"
           data-s2-placeholder="Plano de Ação">
       </select>
	</div>
	<div class="fld form-group">
		<label for="" class="control-label">Tipo Ordenamento</label>
		<br>
		<select name="sqTipoOrdenamento" id="sqTipoOrdenamento" class="fld form-control fld300 fldSelect">
			<option value="">-- Selecione --</option>
			<g:each var="item" in="${listTipoOrdenamento}">
				<option value="${item.id}">${item.descricao}</option>
			</g:each>
		</select>
	</div>
	%{-- Carlos solicitou que tirasse este campo
	<br class="fld">
	<div class="fld form-group w100p">
		<label for="" class="control-label">Observações Sobre a Ação de Conservação</label>
		<br>
		<textarea name="txFichaAcaoConservacao" rows="7" class="fld form-control fldTextarea wysiwyg w100p"></textarea>
	</div> --}%

	%{-- botão gravar --}%
	<div class="fld panel-footer">
	     <button id="btnSaveFrmConAcaoConservacaoDetalhe" data-action="acaoConservacao.saveFrmConAcaoConservacaoDetalhe" data-params="sqFicha" class="fld btn btn-success">Adicionar Ação Conservação</button>
    	 <button id="btnResetFrmConAcaoConservacaoDetalhe" data-action="acaoConservacao.resetFrmConAcaoConservacaoDetalhe" class="fld btn btn-info hide">Limpar Formulário</button>
	</div>
</form>

<div id="divGridAcaoConservacao" class="mt10">
	%{-- <g:render template="ficha/conservacao/acaoConservacao/divGridAcaoConservacao"></g:render> --}%
</div>

<form id="frmConAcaoConservacao" role="form" class="form-inline">

	%{-- Observação da tabela ficha ds_acao_conservacao --}%
	<div class="form-group w100p">
	 	<label for="dsAcaoConservacao" class="control-label">Observações Gerais Sobre as Ações de Conservação
	  	<i data-action="ficha.openModalSelRefBibFicha"
		     data-no-tabela="ficha"
		     data-no-coluna="ds_acao_conservacao"
		     data-sq-registro="${ficha.id}"
		     data-de-rotulo="Ações de Conservação" class="fa fa-book ml10" title="Informar Referência Bibliográfica" style="color: green;" />
		</label>
		<br>
		<textarea name="dsAcaoConservacao" id="dsAcaoConservacao" rows="18" class="fld form-control fldTextarea wysiwyg w100p">${ficha.dsAcaoConservacao}</textarea>
	</div>

	%{-- botão gravar direto na ficha --}%
	<div class="fld panel-footer">
		<button data-action="acaoConservacao.saveFrmConAcaoConservacao" data-params="sqFicha" class="fld btn btn-success">Gravar Observações Gerais</button>
	</div>
</form>
