<!-- view: /views/ficha/_divGridFicha.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<div class="row">
    <div class="col-sm-6"><h3>${params?.paginationTotalRecords?:fichas?.size()} Ficha(s) encontrada(s)</h3></div>
    <div class="col-sm-6" style="text-align: right; padding-top:20px;">
        <g:if test="${fichas}">
            <a id="btnExportCsvFicha" data-action="ficha.exportarCsv" class="btn btn-info mr5" data-params="csv:S">Exportar csv&nbsp;<i style="font-size:19px" class="fa fa-spin"/></a>
        </g:if>
        <g:if test="${ ! session.sicae.user.isCT() }">
            <a class="btn btn-warning pull-right mr5" target="_blank" title="Fazer o download da planilha modelo para preenchimento off-line dos registros de ocorrências!" href="/salve-estadual/main/download?fileName=planilha_modelo_importacao_registro">Baixar planilha</a>
            <a class="btn btn-primary pull-right mr5" data-action="ficha.importarPlanilhaFichas" data-params="sqFicha:-1,modalName:importacaoFicha,contexto:OCORRENCIA">Importar registros</a>
        </g:if>
         <g:if test="${ session.sicae.user.isADM() }">
                <a class="btn btn-danger pull-right mr5" data-action="ficha.importarPlanilhaFichas" data-params="sqFicha:-1,modalName:importacaoFicha,contexto:FICHA">Importar fichas</a>
        </g:if>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <table class="table table-hover table-striped table-condensed table-bordered">
            <thead>
                <th>#</th>
                <th style="width:30px;"><i title="Marcar/Desmarcar Todos" data-action="ficha.selecionarTodas" class="glyphicon glyphicon-unchecked"></i></th>
                <th style="*">Nome científico</th>
                <th style="width:300px;">Unidade responsável</th>
                <th style="width:130px;">Alterado por</th>
                %{--         <th>Autor</th>
                <th>Ano</th> --}%
                <th style="width:70px;">Pendências</th>
                %{--<th>Autor</th>--}%
                <th style="width:150px;">Situação</th>
                <th style="width:150px;">Preenchimento</th>
                <th style="width:80px;">Ação</th>
            </thead>
            <tbody>
                <g:if test="${fichas}">
                    <input type="hidden" id="isLastCicle" value="${fichas[0].cicloAvaliacao?.isLastCicle()?'S':'N'}">
                    <g:each in="${fichas}" var="ficha" status="i">
                    <g:set var="canModify" value="${ficha?.canModify(session?.sicae?.user) ? true : false}"/>
                    <tr class="${ ( ficha.situacaoFicha.codigoSistema=='VALIDADA'?'green':'')}" id="gdFichaRow_${ (params?.paginationOffset?:0) + i + 1}">
                        <td class="text-center">${ (params?.paginationOffset?:0) + i + 1}</td>
                        <g:if test="${ canModify }">
                            <td class="text-center"><input name="chkSqFicha" onchange="ficha.chkSqFichaChange(this, ${i + 1})" value="${ficha?.id}" type="checkbox" class="checkbox-lg"/></td>
                    </g:if>
                    <g:else>
                        <td>&nbsp;</td>
                    </g:else>
                    <td style="padding-left:${ raw( ficha?.taxon?.nivelTaxonomico?.coNivelTaxonomico=='SUBESPECIE'?'30px !important;':'')}">
                    <a style="font-weight:bold;" href="javascript:void(0);" data-action="ficha.edit" data-no-cientifico="${ficha?.nmCientifico}" data-sq-ficha="${ficha?.id}">
                        ${ raw( ficha?.noCientificoItalico ) }
                    </a>

                    </td>
                    <td title="${ficha?.unidade?.noPessoa}">${ficha?.unidade?.sgUnidade}</td>
                    <td class="text-center">
                        ${ raw( ficha.alteradoPorHtml ) }
                    </td>
                    <td class="text-center">${raw((ficha?.qtdPendencia > 0) ? '<span class="bgRed badge">' + ficha?.qtdPendencia + '</span>' : '<span class="bgGreen badge">0</span>')}</td>
                    <td class="text-center">${ficha?.situacaoFicha?.descricao}</td>
                     <td class="text-center">
                         <g:progressBar percentual="${ficha.percentualPreenchimento}"></g:progressBar>
                    </td>

                    <td class="td-actions">
                         <g:if test="${canModify && session.sicae.user.canDeleteFicha() }">

                            <button type="button" data-action="ficha.delete" data-row="${i + 1}" data-descricao="${ficha?.nmCientifico}" data-sq-ficha="${ficha?.id}" class="btn btn-default btn-xs btn-delete" data-toggle="tooltip" data-placement="top" title="Excluir">
                            <span class="glyphicon glyphicon-remove"></span>
                            </button>
                        </g:if>
                        <g:else>
                            <button type="button" disabled="true" class="btn btn-default btn-xs btn-delete" data-toggle="tooltip" data-placement="top" title="Sem permissão">
                            <span class="glyphicon glyphicon-remove disabled"></span>
                            </button>
                        </g:else>
                        <button type="button" data-action="ficha.print" data-no-cientifico="${ficha?.nmCientifico}" data-sq-ficha="${ficha?.id}" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="Imprimir Ficha" style="color:gray;">
                            <i class="fa fa-print btnPrint" aria-hidden="true"></i>
                        </button>
                    </td>
                  </tr>
                </g:each>
                </g:if>
                <g:else>
                <tr><td colspan="9" align="center"><h4>Nenhuma ficha encontrada!</h4><h5>Clique no botão Cadastrar para um nova ficha.</h5></td></tr>
                </g:else>
            </tbody>

            %{--Controle da paginação--}%
            <g:if test="${params?.paginationTotalPages?.toInteger()>1 && params?.paginationCurrentPage}">
                <tfoot>
                    <tr>
                        <td colspan="9" class="text-center">
                            <g:if test="${params?.paginationTotalPages?.toInteger() > 0 }">
                                <nav aria-label="Page navigation">
                                  <ul class="pagination">
                                    <li class="${params?.paginationCurrentPage?.toInteger() < 2 ? 'disabled' : ''}">
                                      <a href="#" ${params?.paginationCurrentPage?.toInteger() < 2 ? '' : 'onClick=ficha.list({paginationTotalRecords:'+params?.paginationTotalRecords+',paginationCurrentPage:1,paginationPageSize:'+params?.paginationPageSize+',paginationTotalPages:'+params?.paginationTotalPages+'})'} aria-label="Página Anterior" title="Prmeira Página">
                                        <i aria-hidden="true" class="glyphicon glyphicon-step-backward"></i>
                                      </a>
                                      <a href="#" ${params?.paginationCurrentPage?.toInteger() < 2 ? '' : 'onClick=ficha.list({paginationTotalRecords:'+params?.paginationTotalRecords+',paginationCurrentPage:'+(params?.paginationCurrentPage?.toInteger()-1)+',paginationPageSize:'+params?.paginationPageSize+',paginationTotalPages:'+params?.paginationTotalPages+'})'} aria-label="Página Anterior" title="Página Anterior">
                                        <i aria-hidden="true" class="glyphicon glyphicon-triangle-left"></i>
                                      </a>
                                    </li>
                                    <g:each var="pagina" in="${ (1..<params?.paginationTotalPages?.toInteger()+1) }">
                                        <li class="${params?.paginationCurrentPage?.toInteger() == pagina ? 'active' : ''}">
                                            <a href="#" ${params?.paginationCurrentPage?.toInteger() == pagina ? '' : 'onClick=ficha.list({paginationTotalRecords:'+params?.paginationTotalRecords+',paginationCurrentPage:'+(pagina)+',paginationPageSize:'+params?.paginationPageSize+',paginationTotalPages:'+params?.paginationTotalPages+'})'} aria-label="Página ${pagina}">
                                            ${pagina}
                                            </a>
                                        </li>
                                    </g:each>
                                    <li class="${params?.paginationCurrentPage?.toInteger() == params?.paginationTotalPages?.toInteger() ? 'disabled' : ''}">
                                      <a href="#" ${params?.paginationCurrentPage?.toInteger() == params?.paginationTotalPages?.toInteger() ? '' : 'onClick=ficha.list({paginationTotalRecords:'+params?.paginationTotalRecords+',paginationCurrentPage:'+(params?.paginationCurrentPage?.toInteger()+1)+',paginationPageSize:'+params?.paginationPageSize+',paginationTotalPages:'+params?.paginationTotalPages+'})'} aria-label="Próxima Página" title="Próxma Página">
                                        <i aria-hidden="true" class="glyphicon glyphicon-triangle-right"></i>
                                      </a>
                                      <a href="#" ${params?.paginationCurrentPage?.toInteger() == params?.paginationTotalPages?.toInteger()  ? '' : 'onClick=ficha.list({paginationTotalRecords:'+params?.paginationTotalRecords+',paginationCurrentPage:'+params?.paginationTotalPages+',paginationPageSize:'+params?.paginationPageSize+',paginationTotalPages:'+params?.paginationTotalPages+'})'} aria-label="Últma Página" title="Última Página">
                                        <i aria-hidden="true" class="glyphicon glyphicon-step-forward"></i>
                                      </a>
                                    </li>
                                  </ul>
                                </nav>
                            </g:if>
                        </td>
                    </tr>
                </tfoot>
            </g:if>
        </table>
    </div>
</div>
