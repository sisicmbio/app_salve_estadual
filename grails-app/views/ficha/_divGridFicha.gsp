<!-- view: /views/ficha/_divGridFicha.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<div class="row">
%{--    <div class="col-sm-4"><h3>${params?.paginationTotalRecords?:fichas?.size()} Ficha(s) encontrada(s)</h3></div>--}%
    <div class="col-sm-4"><h3><span id="divGridFichaTotalRecords">${pagination.totalRecords}</span> Ficha(s) encontrada(s)</h3></div>
    <div class="col-sm-8" style="text-align: right; padding-top:20px;">
    <div style="padding-right:10px;">
        %{--<EXPORTAR--}%
        <g:if test="${fichas}">
            <div class="btn-group">
                <button type="button" id="btnExportar" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Exportar <span class="caret"></span>
                </button>
                <ul class="dropdown-menu pull-right">
                    <li><a id="btnExportCsvFicha" data-action="ficha.exportarCsv"  class="btn btn-primary" data-params="container:fichaContainerBody" title="Exportar para planilha">Gride (planilha)</a></li>
                    <li><a id="btnGerarZipFichas" data-action="app.gerarZipFichas" class="btn btn-primary" data-params="container:fichaContainerBody,contexto:ficha,todasPaginas:S,formato:rtf" title="Gerar arquivo zip da(s) ficha(s) no formato doc.">Fichas editáveis (zip)</a></li>
                    <li><a id="btnGerarZipFichasPdf" data-action="app.gerarZipFichas" class="btn btn-primary" data-params="container:fichaContainerBody,contexto:ficha,todasPaginas:S,formato:pdf" title="Gerar arquivo zip da(s) ficha(s) no formato PDF.">Fichas PDF (zip)</a></li>
                    <li><a id="btnExportOcorrenciaCsvFicha" data-action="ficha.exportarOcorrencias" class="btn btn-primary" data-params="sqCicloAvaliacao,divGrid:divGridFicha,divFiltros:divFiltrosFicha" title="Exportar os registros de ocorrências da(s) ficha(s) para planilha">Ocorrências (planilha)</a></li>
                    <li><a id="btnExportPendenciasFicha" data-action="ficha.exportarPendencias" class="btn btn-primary" data-params="full:S,gridId:divGridFicha" title="Exportar para planilha as pendências da(s) ficha(s)">Pendências</a></li>
                    %{--<li role="separator" class="divider"></li>--}%
                </ul>
            </div>
        </g:if>

        %{-- IMPORTAR --}%
        <g:if test="${ ! session.sicae.user.isCO() }">
                %{-- colaborar externo nao pode porque nao tem uma unica unidade vinculada --}%
                <g:if test="${ ! session.sicae.user.isCE() }">
                    <div class="btn-group">
                        <button type="button" id="btnImportar" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Importar <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu pull-right">
                            <g:if test="${ session.sicae.user.isADM() }">
                                <li><a class="btn btn-primary" data-action="ficha.importarPlanilhaFichas" data-params="sqFicha:-1,modalName:importacaoFicha,contexto:FICHA" title="Fazer a importação da planilha das fichas.">Planilha de fichas</a></li>
                            </g:if>
                            <li><a class="btn btn-primary" data-action="ficha.importarPlanilhaFichas" data-params="sqFicha:-1,modalName:importacaoFicha,contexto:OCORRENCIA" title="Fazer a importação da planilha com os registros de ocorrências.">Planilha de ocorrências</a></li>
                        </ul>
                    </div>
                </g:if>

                %{-- BAIXAR --}%

                <g:if test="${ ! session.sicae.user.isCT() }">
                    <div class="btn-group">
                        <button type="button" id="btnDownload" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Baixar <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a class="btn btn-primary" target="_blank" title="Fazer o download da planilha modelo para preenchimento off-line dos registros de ocorrência!" href="/salve-estadual/main/download?fileName=planilha_modelo_importacao_registro">Planilha importação de OCORRÊNCIAS</a></li>
                            <g:if test="${ session.sicae.user.isADM() }">
                                <li><a class="btn btn-primary" target="_blank" title="Fazer o download da planilha modelo para importação de fichas off-line!" href="/salve-estadual/main/download?fileName=planilha_modelo_importacao_ficha">Planilha importação de FICHAS</a></li>
                            </g:if>
                        </ul>
                    </div>
                </g:if>
        </g:if>

        %{--
        <g:if test="${fichas}">
            <a id="btnExportCsvFicha" data-action="ficha.exportarCsv"  class="btn btn-primary" data-params="csv:S" title="Exportar para planilha (csv)">Exportar Fichas</a>
            <a id="btnExportOcorrenciaCsvFicha" data-action="app.exportarGrideOcorrenciaCsv" class="btn btn-primary" data-params="full:S,gridId:divGridFicha" title="Exportar os registros de ocorrências da(s) ficha(s)">Exportar ocorrências</a>
            <a id="btnGerarZipFichas" data-action="app.gerarZipFichas" class="btn btn-primary" data-params="container:fichaContainerBody,contexto:ficha,todasPaginas:S" title="Gerar arquivo zip da(s) ficha(s) no formato doc.">Exportar formato zip</a>

        </g:if>
        <g:if test="${ ! session.sicae.user.isCT() }">
            <a class="btn btn-primary" target="_blank" title="Fazer o download da planilha modelo para preenchimento off-line dos registros de ocorrências!" href="/salve-estadual/main/download?fileName=planilha_modelo_importacao_registro">Baixar planilha</a>
            <a class="btn btn-primary" data-action="ficha.importarPlanilhaFichas" data-params="sqFicha:-1,modalName:importacaoFicha,contexto:OCORRENCIA" title="Fazer a importação da planilha com os registros de ocorrências.">Importar registros</a>
        </g:if>
         <g:if test="${ session.sicae.user.isADM() }">
                <a class="btn btn-primary" data-action="ficha.importarPlanilhaFichas" data-params="sqFicha:-1,modalName:importacaoFicha,contexto:FICHA" title="Fazer a importação da planilha das fichas.">Importar fichas</a>
        </g:if>--}%
    </div>
 </div>

</div>

<div class="row">
    <div class="col-sm-12">
        <input type="hidden" id="isLastCicle" value="${cicloAvaliacao?.isLastCicle()?'S':'N'}">
        <table class="table table-hover table-striped table-condensed table-bordered display" style="width:100%" id="tableDivGridFicha">
            <thead>
                <th style="width:40px;">#</th>
                <th style="width:20px;" class="no-export"><i title="Marcar/Desmarcar Todos" data-table-id="tableDivGridFicha" data-action="ficha.selecionarTodas" class="glyphicon glyphicon-unchecked"></i></th>
                <th style="width:auto">Nome científico</th>
                <th style="width:300px;">Unidade responsável</th>
                <th style="width:130px;">Alterado em</th>
                %{--         <th>Autor</th>
                <th>Ano</th> --}%
                <th style="width:70px;">Pendências</th>
                %{--<th>Autor</th>--}%
                <th style="width:150px;">Situação</th>
                <th style="width:150px;">Preenchimento</th>
                <th style="width:80px;">Ação</th>
            </thead>
            <tbody>
                <g:if test="${fichas}">
                    <g:each in="${fichas}" var="ficha" status="i">
                        %{--<g:set var="vwFicha" value="${br.gov.icmbio.VwFicha.get( ficha.sq_ficha.toInteger() )}"/>--}%

                        <tr class="${ (ficha.co_nivel_taxonomico == 'SUBESPECIE' ? 'bgSubespecie ':'') + (ficha.cd_situacao_ficha_sistema == 'EXCLUIDA' ? 'red':'black') }" id="gdFichaRow_${ (params?.paginationOffset?:0) + i + 1}">

                            %{-- PRIMEIRA COLUNA - NUMERO DA LINHA --}%
                            <td class="text-center">${ (i + 1)+ pagination.offset }</td>

                            %{-- SEGUNDA COLUNA - CHECKBOX --}%
                            <td class="text-center">
                                <input name="chkSqFicha" onchange="ficha.chkSqFichaChange(this, ${i + 1})" value="${ficha?.sq_ficha}" type="checkbox" class="checkbox-lg"/>
                            </td>

                            %{-- TERCEIRA COLUNA - NOME CIENTIFICO --}%
                            <td style="padding-left:${ raw( ficha.co_nivel_taxonomico == 'SUBESPECIE'?'30px !important;':'')}">
                                <a style="font-weight:bold;" href="javascript:void(0);" data-action="ficha.edit" data-no-cientifico="${ficha?.nm_cientifico}" data-sq-ficha="${ficha?.sq_ficha}">
                                    ${ raw( br.gov.icmbio.Util.ncItalico( ficha.nm_cientifico,true,true,ficha?.co_nivel_taxonomico ) ) }
                                </a>
                            </td>

                            %{-- QUARTA COLUNA - UNIDADE RESPONSAVEL --}%
                            <td>${ficha.sg_unidade_org}</td>

                            %{-- QUINTA COLUNA - ALTERADO POR --}%
                            <td class="text-center">
                            <g:if test="${ ficha?.dt_alteracao }">
                                ${ raw( ficha.dt_alteracao.format('dd/MM/yyyy HH:mm:ss') + '<br>' +
                                        br.gov.icmbio.Util.capitalize( br.gov.icmbio.Util.getFirstName( ficha?.no_usuario_alteracao?:'',18 ) ) ) }
                            </g:if>
                            <g:else>
                                &nbsp;
                            </g:else>
                                %{--<g:set var="nomes" value="${ ficha?.no_usuario_alteracao?.split(' ')}"></g:set>
                                ${ raw( nomes && nomes[1] ? ficha.dt_alteracao.format('dd/MM/yyyy HH:mm:ss')   + '<br>' + nomes[0] + ' ' + nomes[1] : '' )}--}%
                            </td>

                            %{-- QUINTA COLUNA - PENDENCIAS --}%
                            <td class="text-center">
                            ${raw((ficha.nu_pendencia > 0) ? '<a href="javascript:modal.showPendenciaFicha(' + ficha.sq_ficha + ');"><span class="cursor-pointer bgRed badge">' + ficha.nu_pendencia + '</span></a>' : '<span class="bgGreen badge">0</span>')}</td>

                            %{-- SEXTA COLUNA - SITUACAO --}%
                            <td class="text-center">${ficha.ds_situacao_ficha}</td>

                            %{-- SETIMA COLUNA - PROGRESSO --}%
                            <td class="text-center">
                                <g:progressBar percentual="${ficha.vl_percentual_preenchimento}"></g:progressBar>
                            </td>


                            %{-- OITAVA COLUNA - BOTOES --}%
                            <td class="td-actions">
                                <g:if test="${ficha.st_alterar && session.sicae.user.canDeleteFicha() }">

                                    %{-- somente adm pode excluir ficha no primeiro ciclo--}%
                                    <g:if test="${ session.sicae.user.isADM() || cicloAvaliacao.id.toInteger() != 1 }">
                                        <button type="button" data-action="ficha.delete" data-row="${i + 1}" data-descricao="${ficha.nm_cientifico}" data-sq-ficha="${ficha.sq_ficha}" class="btn btn-default btn-xs btn-delete" data-toggle="tooltip" data-placement="top" title="Excluir">
                                            <span class="glyphicon glyphicon-remove"></span>
                                        </button>
                                    </g:if>
                                </g:if>
                                %{--<g:else>--}%
                                    %{--<button type="button" disabled="true" class="btn btn-default btn-xs btn-delete" data-toggle="tooltip" data-placement="top" title="Sem permissão">--}%
                                        %{--<span class="glyphicon glyphicon-remove disabled"></span>--}%
                                    %{--</button>--}%
                                %{--</g:else>--}%
                                <button type="button" data-action="ficha.print" data-no-cientifico="${ficha.nm_cientifico}" data-sq-ficha="${ficha.sq_ficha}" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="Imprimir Ficha" style="color:gray;">
                                    <i class="fa fa-print btnPrint" aria-hidden="true"></i>
                                </button>
                            </td>
                        </tr>
                    </g:each>
                </g:if>
                <g:else>
                    <tr>
                        <td colspan="9" align="center"><h4>Nenhuma ficha encontrada!</h4>

                            <g:if test="${ session.sicae.user.isCE() }">
                                <h4 class="red">Entre em contato com a Unidade ICMBIo responsável e solicite a atribuição de fichas ao seu perfil.</h4>
                            </g:if>
                            <g:else>
                                <h5>Clique no botão Cadastrar para um nova ficha.</h5>
                            </g:else>
                        </td>
                    </tr>
                </g:else>
            </tbody>
            %{--Controle da paginação--}%
            %{--<g:if test="${params?.paginationTotalPages?.toInteger()>1 && params?.paginationCurrentPage}">
                <tfoot>
                    <tr>
                        <td colspan="9" class="text-center">
                            <g:if test="${params?.paginationTotalPages?.toInteger() > 0 }">
                                <nav aria-label="Page navigation">
                                  <ul class="pagination">
                                    <li class="${params?.paginationCurrentPage?.toInteger() < 2 ? 'disabled' : ''}">
                                      <a href="#" ${params?.paginationCurrentPage?.toInteger() < 2 ? '' : 'onClick=ficha.list({paginationTotalRecords:'+params?.paginationTotalRecords+',paginationCurrentPage:1,paginationPageSize:'+params?.paginationPageSize+',paginationTotalPages:'+params?.paginationTotalPages+'},"",event)'} aria-label="Página Anterior" title="Prmeira Página">
                                        <i aria-hidden="true" class="glyphicon glyphicon-step-backward"></i>
                                      </a>
                                      <a href="#" ${params?.paginationCurrentPage?.toInteger() < 2 ? '' : 'onClick=ficha.list({paginationTotalRecords:'+params?.paginationTotalRecords+',paginationCurrentPage:'+(params?.paginationCurrentPage?.toInteger()-1)+',paginationPageSize:'+params?.paginationPageSize+',paginationTotalPages:'+params?.paginationTotalPages+'},"",event)'} aria-label="Página Anterior" title="Página Anterior">
                                        <i aria-hidden="true" class="glyphicon glyphicon-triangle-left"></i>
                                      </a>
                                    </li>
                                    <g:each var="pagina" in="${ (1..<params?.paginationTotalPages?.toInteger()+1) }">
                                        <li class="${params?.paginationCurrentPage?.toInteger() == pagina ? 'active' : ''}">
                                            <a href="#" ${params?.paginationCurrentPage?.toInteger() == pagina ? '' : 'onClick=ficha.list({paginationTotalRecords:'+params?.paginationTotalRecords+',paginationCurrentPage:'+(pagina)+',paginationPageSize:'+params?.paginationPageSize+',paginationTotalPages:'+params?.paginationTotalPages+'},"",event)'} aria-label="Página ${pagina}">
                                            ${pagina}
                                            </a>
                                        </li>
                                    </g:each>
                                    <li class="${params?.paginationCurrentPage?.toInteger() == params?.paginationTotalPages?.toInteger() ? 'disabled' : ''}">
                                      <a href="#" ${params?.paginationCurrentPage?.toInteger() == params?.paginationTotalPages?.toInteger() ? '' : 'onClick=ficha.list({paginationTotalRecords:'+params?.paginationTotalRecords+',paginationCurrentPage:'+(params?.paginationCurrentPage?.toInteger()+1)+',paginationPageSize:'+params?.paginationPageSize+',paginationTotalPages:'+params?.paginationTotalPages+'},"",event)'} aria-label="Próxima Página" title="Próxma Página">
                                        <i aria-hidden="true" class="glyphicon glyphicon-triangle-right"></i>
                                      </a>
                                      <a href="#" ${params?.paginationCurrentPage?.toInteger() == params?.paginationTotalPages?.toInteger()  ? '' : 'onClick=ficha.list({paginationTotalRecords:'+params?.paginationTotalRecords+',paginationCurrentPage:'+params?.paginationTotalPages+',paginationPageSize:'+params?.paginationPageSize+',paginationTotalPages:'+params?.paginationTotalPages+'},"",event)'} aria-label="Últma Página" title="Última Página">
                                        <i aria-hidden="true" class="glyphicon glyphicon-step-forward"></i>
                                      </a>
                                    </li>
                                  </ul>
                                </nav>
                            </g:if>
                        </td>
                    </tr>
                </tfoot>
            </g:if>
            --}%
        </table>
    </div>
    %{--Controle da paginação novo--}%
    <div class="panel-footer row">
        <div class="col-md-10">
            <g:tablePagination id="tableDivGridFichaPagination"
                           pagination="${pagination}"
                           filtros="${filtros}"
                           url="ficha/list"
                           blockId="fichaContainerBody"
                           blockMessage="Atualizando..."
                           tableId="tableDivGridFicha"
                           qtdButtons="10"
                           tag="mdduloFicha"
        />
    </div>

</div>
