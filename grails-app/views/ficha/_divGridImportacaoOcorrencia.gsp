<!-- view: /views/ficha/_divGridImportacaoOcorrencia.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<h3>Histórico da Importações</h3>
<table class="table table-striped table-bordered table-hover">
   <thead>
      <tr>
         <th style="width:100px">Data</th>
         <th style="width:100px">Situação</th>
         <th style="width:50px">%</th>
         <th style="width:*">Arquivo</th>
         <th style="width:200px">Responsável</th>
         <th style="width:200px">Comentário</th>
         <th style="width:80px">Ações</th>
      </tr>
   </thead>
   <tbody>
      <g:each var="item" in="${listPlanilhas}">
      <tr>
         <td class="text-center"><g:formatDate format='dd/MM/yyyy' date='${item.dtEnvio}' /></td>
         <td class="text-center">${item.inProcessando ? 'Processando...' : 'Finalizada'}</td>
         <td class="text-center"id="td_percentual_${item.id}">${item.nuPercentual.toString()}'</td>
         <td>${item.noArquivo}</td>
         <td>${item.pessoaFisica.noPessoa}</td>
         <td>${item.deComentario}</td>
         <td class="td-actions">
            <a data-id="${item.id}" data-action="ficha.deletePlanilha" data-gride="modalImportacaoOcorrencias" data-descricao="${item.noArquivo}"class="fld btn btn-default btn-xs btn-delete" title="Excluir">
               <span class="glyphicon glyphicon-remove"></span>
            </a>
         </td>
      </tr>
      <tr id="tr_resultado_imortacao_${item.id}">
         <td colspan="5">
             <div style="border:1px dashed blue;padding-left: 30px">
                <g:if test="${item.txLog}">
                    <h4><u>Log</u></h4>
                    ${ raw(item.txLog) }
                </g:if>
                <h4><u>Inconsistências</u></h4>
                ${ raw(item.lineErrors) }
             </div>
         </td>
      </tr>
      </g:each>
   </tbody>
</table>
