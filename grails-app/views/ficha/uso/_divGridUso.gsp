<!-- view: /views/ficha/uso/_divGridUso.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<table class="table table-striped table-bordered table-hover">
   <thead>
      <tr>
         <th style="width:auto;">Uso</th>
         <th style="width:200px;">Georreferência</th>
          <th style="width:200px;">Região</th>
          <th style="width:400px;">Ref. Bibliográfica</th>
          <g:if test="${ ! session.sicae.user.isCO() }">
              <th style="width:40px;">Ação</th>
          </g:if>
      </tr>
   </thead>
   <tbody>
   <g:each var="item" in="${listFichaUso}">

      <tr>
         <td>${raw( item.uso.descricaoHieraquia.replaceAll(/\s\/\s/,"<br>") ) }</td>

         <td>
            <a data-action="selGeo.openModalSelGeo"
               data-no-contexto="USO_GEO"
               data-sq-registro="${item.id}"
               data-de-rotulo="Uso"
               data-update-grid="divGridUso" class="fld btn btn-default btn-xs btn-grid-subform" title="Incluir/Remover Cordenada Geográfica"><span class="glyphicon glyphicon-map-marker blue"></span></a>
             &nbsp;
             ${raw(item.geoHtml)}
         </td>
         <td>
            <a data-action="selRegiao.openModalSelRegiao"
               data-no-contexto="USO_REGIAO"
               data-sq-registro="${item.id}"
               data-de-rotulo="Uso - ${item.uso.descricao}"
               data-update-grid="divGridUso" class="fld btn btn-default btn-xs btn-grid-subform"
               title="Incluir/Remover Região"><span class="glyphicon glyphicon-map-marker orange"></span></a>
               ${ raw(item.regioesHtml) }

         </td>

          <td>
              <a data-action="ficha.openModalSelRefBibFicha"
                 data-no-tabela="ficha_uso"
                 data-no-coluna="sq_ficha_uso"
                 data-sq-registro="${item.id}"
                 data-de-rotulo="Uso - ${item.uso.descricao}"
                 data-com-pessoal="true"
                 data-update-grid="divGridUso" class="fld btn btn-default btn-xs btn-grid-subform" title="Adicionar/Remover Refeferência bibliográfica."><span class="fa fa-book"></span></a>
              &nbsp;
              ${raw(item.refBibHtml)}
          </td>

          <g:if test="${ ! session.sicae.user.isCO() }">
              <td class="td-actions">
                 %{--
                <a data-action="uso.editUso"  data-id="${item.id}" class="fld btn btn-default btn-xs btn-update" title="Alterar">
                   <span class="glyphicon glyphicon-pencil"></span>
                </a>
                 --}%
                <a data-action="uso.deleteUso" data-descricao="${item.uso.descricao}" data-id="${item.id}" class="fld btn btn-default btn-xs btn-delete" title="Excluir">
                   <span class="glyphicon glyphicon-remove"></span>
                </a>
             </td>
          </g:if>


      </tr>
   </g:each>
   </tbody>
</table>
