//# sourceURL=N:\SAE\sae\grails-app\views\ficha\uso\uso.js
//
;
var gWindowHeight = Math.max(500,window.innerHeight-120);
var uso = {

    openModalSelUso:function(params )
    {
        var data = {};
        data.reload = true;
        data = app.params2data(params, data)
        $.jsPanel({
            // http://jspanel.de/api/
            id         : "modalSelUso",
            paneltype:   'modal',
            container:'body',

            draggable:       {
                handle:  'div.jsPanel-titlebar, div.jsPanel-ftr',
                opacity: 0.8
            },
            dragit:             {
                axis:        false,
                containment: 'parent',
                handles:     '.jsPanel-titlebar, .jsPanel-ftr.active', // do not set .jsPanel-titlebar to .jsPanel-hdr
                opacity:     0.8,
                start:       false,
                drag:        false,
                stop:        false,
                disableui:   true,
            },
            resizable: {
                handles:   'n, e, s, w, ne, se, sw, nw',
                autoHide:  false,
                minWidth:  500,
                minHeight: 500
            },
            resizeit:           {
                containment: 'parent',
                handles:     'n, e, s, w, ne, se, sw, nw',
                minWidth:    500,
                minHeight:   500,
                maxWidth:    10000,
                maxHeight:   10000,
                start:       false,
                resize:      false,
                stop:        false,
                disableui:   true
            },
            show:      'animated slideInUp',
            theme      : "green",
            headerTitle: 'Seleção de Usos',
            contentSize: {width: 800, height: gWindowHeight},
            content    : '<div id="div_content_ficha_selecionar_uso" style="outline:none;border:none;display:block;min-height:100%;"<span id="spinner">Carregando...' + window.grails.spinner + '</span></div>',
            callback   : function ()
            {

                // ler o formulário
                var content = this.content;
                app.loadModule(app.url + 'ficha/getModal', params, 'div_content_ficha_selecionar_uso', function (res)
                {
                    content.html(res);

                    // ajustar a altura da treeview para que o botão salvar fique visivel
                    $("#modalSelUso #treeChecks").css('max-height',gWindowHeight-150);

                    $("#modalSelUso #treeChecks").fancytree({
                        quicksearch: true,
                        selectMode : 2,
                        strings     : { loading: "Carregando...", loadError: "Erro de leitura!", moreData: "Mais...", noData: "Nenhum registro."},
                        focusOnSelect:false,
                        extensions: ["wide","filter"],
                        filter: {
                            autoApply: true,   // Re-apply last filter if lazy data is loaded
                            autoExpand: true, // Expand all branches that contain matches while filtered
                            counter: true,     // Show a badge with number of matching child nodes near parent icons
                            fuzzy: false,      // Match single characters in order, e.g. 'fb' will match 'FooBar'
                            hideExpandedCounter: false,  // Hide counter badge if parent is expanded
                            hideExpanders: false, // Hide expanders if all child nodes are hidden by filter
                            highlight: true,   // Highlight matches by wrapping inside <mark> tags
                            leavesOnly: false, // Match end nodes only
                            nodata: true,      // Display a 'no data' status node if result is empty
                            mode: "hide"       // dimm = Grayout unmatched nodes (pass "hide" to remove unmatched node instead)
                        },
                        source: {
                            data:data,
                            url: app.url+"fichaUso/getTreeUsos"
                        },
                        checkbox: true,
                        autoScroll:true,
                        /** expandir ao clicar */
                        click: function(event, data) {
                            // https://github.com/mar10/fancytree/wiki/TutorialEvents
                            var node = data.node;
                            var tt = $.ui.fancytree.getEventTargetType(event.originalEvent);
                            if( tt =='title' )
                            {
                                data.node.toggleExpanded();
                            }
                        },
                        /** desmarcar nivel superior ao selecionar um dos niveis inferiores*/
                        select: function(event, data) {
                            var node = data.node;
                            if ( node ) {
                                if (node.isSelected()) {
                                    node.setExpanded(true);
                                    $(node.tr).find('select').show();
                                    var parent=node.parent;
                                    while( parent ) {
                                        parent.setSelected(false);
                                        parent.unselectable=true;
                                        parent.hideCheckbox=true;
                                        parent.render(true);
                                        parent = parent.parent;
                                    }
                                } else {
                                    var parent=node.parent;
                                    var selectedChildren = parent.getChildren().filter( function(item) {
                                        return item.isSelected();
                                    });
                                    if( selectedChildren.length == 0 ) {
                                        while (parent) {
                                            parent.unselectable = false;
                                            parent.hideCheckbox = false;
                                            parent.render(true);
                                            parent = parent.parent;
                                        }
                                    }
                                    $(node.tr).find('select').hide();
                                }
                            };
                        },
                        /** fim novo */
                    });

                    // ativar função de filtragem // http://wwwendt.de/tech/fancytree/demo/sample-ext-filter.html#
                    $("#frmModalSelUso #fldFiltrarTreeView").keyup(function(e){
                        var n,
                        tree = $.ui.fancytree.getTree(),
                        opts = {},
                        filterFunc = tree.filterNodes,
                        match = $(this).val();
                        if(e && e.which === $.ui.keyCode.ESCAPE || $.trim(match) === "")
                        {
                            $("#frmModalSelUso #btnTreeViewReset").click();
                            return;
                        }
                        if( match )
                        {
                            $("#frmModalSelUso #btnTreeViewReset").removeClass('hidden');
                        }
                        else
                        {
                            $("#frmModalSelUso #btnTreeViewReset").addClass('hidden');
                        }
                        n = filterFunc.call(tree, match, opts);
                    }).focus();
                    $("#frmModalSelUso #btnTreeViewReset").click(function(){
                        $("#modalSelUso #treeChecks").fancytree("getTree").clearFilter();
                        $("#frmModalSelUso fldFiltrarTreeView").val('').focus();
                    });
                    $("#frmModalSelUso #btnTreeViewNone").click(function(){
                        var node = $("#modalSelUso #treeChecks").fancytree("getTree").getActiveNode();
                        if( node )
                        {
                            node.setSelected(false);
                            node.visit(function(node)
                            {
                                if ( ! $(node.span).hasClass('fancytree-hide') )
                                {
                                    node.setSelected(false);
                                }
                            });
                        }
                        return false;
                    });
                    $("#frmModalSelUso #btnTreeViewAll").click(function(){

                        var node = $("#modalSelUso #treeChecks").fancytree("getTree").getActiveNode();
                        if( node )
                        {
                            node.setSelected(true);
                            node.visit(function(node)
                            {
                                if ( ! $(node.span).hasClass('fancytree-hide') )
                                {
                                    node.setSelected(true);
                                }
                            });
                        }
                        return false;
                    });
                    $("#frmModalSelUso #btnTreeViewCollapseAll").click(function(){
                        $("#modalSelUso #treeChecks").fancytree("getRootNode").visit(function(node){
                            if( ! $(node.span).hasClass('fancytree-hide') )
                            {
                                node.setExpanded(false);
                            }
                        });
                    });
                    $("#frmModalSelUso #btnTreeViewExpandAll").click(function(){

                        $("#modalSelUso #treeChecks").fancytree("getRootNode").visit(function(node){
                            if( ! $(node.span).hasClass('fancytree-hide') )
                            {
                                node.setExpanded(true);
                            }
                        });
                        return false;
                    });
                }, '');
            },
        });
    },
    saveTreeUso:function( params )
    {
        var data = {sqFicha:$("#sqFicha").val(),ids:[] };
        $.each( $("#modalSelUso #treeChecks").fancytree('getTree').getSelectedNodes(),function(){
            data.ids.push(this.data.id);
        })
        data.ids = data.ids.join(',');
        app.ajax(app.url+'fichaUso/saveTreeUso', data,
            function(res)
            {
                if (! res.status)
                {
                    uso.updateGridUso();
                }
                if( res.data.limparDescricao )
                {
                    tinymce.get('dsUso').setContent('');
                    app.alertInfo('Texto da descrição foi removido porque existe uso cadastrado!');
                }
            }, null, 'json'
        );
    },

	//----------------------------------------------------------

	tabFichaUsoInit: function() {
		uso.updateGridUso();
        uso.updateGridAnexos();
	},
	saveFrmUso: function(params) {
		if (!$('#frmUso').valid()) {
			return false;
		}
		var data = $("#frmUso").serializeArray();
		if (!data) {
			return;
		}
		data = app.params2data(params, data);
		if (!data.sqFicha) {
			app.alertError('ID da ficha não encontrado!');
			return;
		}
		app.ajax(app.url+'fichaUso/saveFrmUso', data,
			function(res) {
				uso.resetFrmUso();
				uso.updateGridUso();
			}, null, 'json'
		);
	},
	resetFrmUso: function() {
		app.reset('frmUso');
		$("#sqCriterioUsoIucn").focus();
		$("#btnResetFrmUso").addClass('hide');
		$("#btnSaveFrmUso").html($("#btnSaveFrmUso").data('value'));
		app.unselectGridRow('divGridUso');
	},
	updateGridUso: function() {
		ficha.getGrid('uso');
	},
	editUso: function(params, btn) {
		app.ajax(app.url+'fichaUso/editUso', params,
			function(res) {
				$("#btnResetFrmUso").removeClass('hide');
				$("#btnSaveFrmUso")
					.data('value', $("#btnSaveFrmUso").html());
				$("#btnSaveFrmUso").html("Gravar Alteração");
				app.setFormFields(res, 'frmUso');
				app.selectGridRow(btn);
			});
	},
	deleteUso: function(params, btn) {
		app.selectGridRow(btn);
		app.confirm('<br>Confirma exclusão da uso <b>' + params.descricao + '</b>?',
			function() {
				app.ajax(app.url+'fichaUso/deleteUso',
					params,
					function(res) {
						uso.updateGridUso();
					});
			},
			function() {
				app.unselectGridRow(btn);
			}, params, 'Confirmação', 'warning');
	},
	saveFrmUsoFicha:function( params )
	{
		if (!$('#frmUsoFicha').valid()) {
			return false;
		}
		var data = $("#frmUsoFicha").serializeArray();
		if (!data) {
			return;
		}
		data = app.params2data(params, data);
		if (!data.sqFicha) {
			app.alertError('ID da ficha não encontrado!');
			return;
		}
        // se tiver uso cadastrado, limpar texto padrão
        if( $("#divGridUso table tbody tr").length > 0 )
        {
            if( tinymce.get('dsUso').getContent({format : 'text'}).trim() == 'Não foram encontradas informações para o táxon.')
            {
                tinymce.get('dsUso').setContent('');
                data.dsUso = '';
                app.alertInfo('Texto da descrição foi removido porque existe uso cadastrado!');
            }
        }
		app.ajax(app.url+'fichaUso/saveFrmUsoFicha', data,
			function(res) {
				app.focus('dsUso');
                $("#liTabUso").removeClass('changed')
			}, null, 'json'
		);

	},
    updateGridAnexos:function( params )
    {
        ficha.getGridAnexos('divGridUsoAnexo');
    },
}
