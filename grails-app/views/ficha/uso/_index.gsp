<%-- inicio form uso --%>
<form id="frmUso" class="form-inline" role="form">
   <input type="hidden" name="sqFichaUso" value="">

<div class="fld form-group">
    <label for="" class="control-label">Uso</label>
      <a data-action="uso.openModalSelUso"
         class="fld btn btn-default btn-xs btn-grid-subform"
          data-params="sqFicha,modalName:selecionarUso,contexto:Usos"
         title="Selecionar/Excluir Usos">
          <span class="glyphicon glyphicon-plus"></span>
      </a>
</div>

%{--    <div class="fld form-group">
      <label for="" class="control-label">Uso</label>
      <br>
       <select name="sqUso" class="fld form-control select2 fld800" required="true"
           data-s2-minimum-input-length="0"
           data-s2-auto-height="true"
           data-s2-placeholder="?">
          <option value="">?</option>
          <g:each var="item" in="${listUsos}">
              <option value="${item.id}">${item.descricaoCompleta}</option>
              <g:each var="filho" in="${item.itens.sort{it.ordem} }">
                  <option value="${filho.id}">&nbsp;&nbsp;${filho.descricaoCompleta}</option>
                  <g:each var="neto" in="${filho.itens.sort{ it.ordem } }">
                     <option value="${neto.id}">&nbsp;&nbsp;&nbsp;&nbsp;${neto.descricaoCompleta}</option>
                  </g:each>
              </g:each>
          </g:each>
       </select>
   </div>
 --}%



   %{-- Carlos sugeriu que este campo fosse ocultado
   <br>
   <div class="fld form-group w100p">
      <label for="" class="control-label">Observação Sobre Uso</label>
      <br>
      <textarea name="txFichaUso" rows="7" class="fld fldTextarea wysiwyg w100p"></textarea>
   </div>
   --}%
%{--
com a seleção via treeview não precisa mais dos botoes abaixo
<div class="fld panel-footer">
      <button id="btnSaveFrmUso" data-action="uso.saveFrmUso" data-params="sqFicha" class="fld btn btn-success">Adicionar Uso</button>
      <button id="btnResetFrmUso" data-action="uso.resetFrmUso" class="fld btn btn-info hide">Limpar Formulário</button>
   </div> --}%
</form>
<%-- fim form frmUso --%>

<div id="divGridUso">
   %{-- <g:render template="/ficha/uso/divGridUso"></g:render> --}%
</div>

%{-- inicio form texto usos da ficha --}%
<form id="frmUsoFicha" class="form-inline" role="form">
  <div class="form-group w100p">
    <label for="dsUso" class="control-label">Observações Gerais Sobre os Usos
      <i data-action="ficha.openModalSelRefBibFicha"
      data-no-tabela="ficha"
      data-no-coluna="dsUso"
      data-sq-registro="${ficha.id}"
      data-de-rotulo="Observações Gerais Sobre os Usos" class="fa fa-book ml10" title="Informar Referência Bibliográfica" style="color: green;" />
    </label>
    <g:if test="${ ! session.sicae.user.isCO() }">
      <div class="checkbox ml20">
          <label title="Clique aqui para preencher o campo com o texto">
              <input type="checkbox" data-action="app.setDefaultText" data-target="dsUso"
                  data-text="Não foram encontradas informações para o táxon."
                  ${ ficha?.dsUso?.toLowerCase()?.indexOf('n&atilde;o foram encontradas informa&ccedil;&otilde;es para o táxon.') > -1 ? 'checked' : '' }
                     class="fld checkbox-lg">&nbsp;&nbsp;Preencher com: "N&atilde;o foram encontradas informa&ccedil;&otilde;es para o táxon."
          </label>
      </div>
    </g:if>
    <br>
    <textarea name="dsUso" id="dsUso" rows="18" class="fld fldTextarea wysiwyg w100p">${ficha.dsUso}</textarea>
  </div>
  <div class="fld panel-footer">
    <button id="btnSaveFrmUsoFicha" data-action="uso.saveFrmUsoFicha" data-params="sqFicha" class="fld btn btn-success">Gravar</button>
  </div>
</form>



%{-- imagens de mapas/shp --}%
<fieldset>
    <legend>
        Mapas e Shapefiles
        <a data-action="ficha.openModalSelAnexo"
           data-contexto="MAPA_USO"
           data-de-rotulo="Mapas de Uso"
           data-show-principal="false"
           data-update-grid="divGridUsoAnexo"
           class="fld btn btn-default btn-xs btn-grid-subform"
           title="Adicionar Imagem/Shapefile de Usos"><span class="glyphicon glyphicon-plus"></span>
        </a>
    </legend>
</fieldset>
%{-- gride  --}%
<div class="" id="divGridUsoAnexo" data-contexto="MAPA_USO"></div>
