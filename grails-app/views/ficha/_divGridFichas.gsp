<%@ page import="br.gov.icmbio.Util" %>
<!-- view: /views/ficha/_divGridFichas.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<table id="table${gridId}" class="mt10 table table-striped table-sorting">
    <thead>
    %{-- PAGINACAO --}%
    <tr id="tr${gridId}Pagination">
        <td colspan="9">
            <div class="text-left" style="min-height:0;height: auto;overflow: hidden"
                 id="div${(gridId ?: pagination.gridId)}PaginationContent">
%{--                <g:if test="${pagination?.totalRecords}">--}%
                    <g:gridPagination pagination="${pagination}"/>
%{--                </g:if>--}%
            </div>
        </td>
    </tr>

    %{-- FILTROS --}%
    <tr id="tr${gridId}Filters" class="table-filters" data-grid-id="${gridId}">

        <td colspan="2">
            <div style="display:flex;flex-direction:row; justify-content:space-between;">
                <i class="fa fa-question-circle"
                   title="Utilize os campos ao lado para filtragem dos registros informando o valor a ser localizado e pressionando Enter. Para remover o filtro limpe o campo e pressione Enter novamente."></i>
                <i data-grid-id="${gridId}" class="fa fa-eraser" title="Limpar filtro(s)"></i>
                <i data-grid-id="${gridId}" class="fa fa-filter" title="Aplicar filtro(s)"></i>
            </div>
        </td>

%{--        FILTRO COLUNA NOME CIENTIFICO--}%
        <td class="text-left" title="Informe o nome científico completo ou em parte para pesquisar. Ex: Aburria jacutinga ou apenas Aburria ou jacutinga">
            <input type="text" name="fldNmCientifico${gridId}" data-field="nmCientificoFiltro" value="${params?.nmCientifico ?: ''}" class="table-filter">
        </td>

        %{--        FILTRO PELA UNIDADE--}%
        <g:if test="${grideFiltroUnidade}">
        <td class="text-left" title="Informe sigla ou parte da sigla da unidade para pesquisar.">
            <input type="text" name="fldSgUnidade${gridId}" data-field="sgUnidadeFiltro" value="${params?.sgUnidadeOrg ?: ''}" class="table-filter">
        </td>
        </g:if>
        <g:else>
            <td></td>
        </g:else>

        %{--        FILTRO PELO LOG DA ALTERACAO --}%
        <td class="text-left" title="Informe a data (dd/mm/aaaa) ou nome ou parte do nome do usuário para pesquisar.">
            <input type="text" name="fldLogAlteracao${gridId}" data-field="logAlteracaoFiltro" value="${params?.logAlteracao ?: ''}" class="table-filter">
        </td>

        %{--        FILTRO PELAS PENDENCIAS --}%
        <td class="text-left">
            <select name="fldPendencia${gridId}" class="table-filter" data-field="inPendenciaFiltro">
                <option value=""></option>
                <option value="2">Com</option>
                <option value="1">Sem</option>
            </select>
        </td>

        %{--        FILTRO PELA SITUAÇÃO DA FICHA --}%
        <td class="text-left">
            <select name="fldSqSituacao${gridId}" class="table-filter" data-field="sqSituacaoFiltro">
                <option value=""></option>
                <g:each var="item" in="${listSituacaoFicha}">
                    <option value="${item.id.toString()}">${item.descricao}</option>
                </g:each>
            </select>
        </td>

        <td colspan="9"></td>
    </tr>

    %{--    TITULOS--}%
    <tr id="tr${gridId}Columns" data-grid-id="${gridId}">
        <th style="width:60px;">#</th>
        <th style="width:40px;" class="no-export"><i id="checkAll-${gridId}" title="Marcar/Desmarcar Todos" data-table-id="table${gridId}" data-action="ficha.selecionarTodas"
                                                     class="glyphicon glyphicon-unchecked"></i></th>
        <th style="min-width:230px;width:auto" class="sorting sorting_asc" data-field-name="ficha.nm_cientifico">Nome científico</th>
        <th style="width:200px;" class="sorting" data-field-name="uo.sg_unidade_org">Unidade responsável</th>
        <th style="width:200px;" class="sorting" data-field-name="ficha.dt_alteracao">Alterado em</th>
        <th style="width:70px;" class="sorting" data-field-name="pendencias.nu_pendencia">Pendências</th>
        <th style="width:150px;" class="sorting" data-field-name="situacao.ds_dados_apoio">Situação</th>
        <th style="width:150px;" class="sorting" data-field-name="ficha.vl_percentual_preenchimento">Preenchimento</th>
        <th style="width:80px;">Ação</th>
    </tr>
    </thead>
    <tbody>
    <g:if test="${listFichas}">
        <g:each var="ficha" in="${listFichas}" status="i">

            <tr id="tr-${ficha.sq_ficha.toString()}" class="text-center ${(ficha.co_nivel_taxonomico == 'SUBESPECIE' ? 'bgSubespecie ' : '') + (ficha.cd_situacao_ficha_sistema == 'EXCLUIDA' ? 'red' : 'inherit')}">

               %{-- PRIMEIRA COLUNA - NUMERO DA LINHA --}%
                <td class="text-center">${ i.toInteger() + ((pagination ? pagination?.rowNum : 1) as Number)}</td>

                %{-- SEGUNDA COLUNA - CHECKBOX --}%
                <td class="text-center">
                    <input name="chkSqFicha" onchange="ficha.chkSqFichaChange(this, ${i + 1})"
                            data-grid-id="${gridId}"
                            value="${ficha?.sq_ficha}" type="checkbox" class="checkbox-lg"/>
                </td>

                %{-- TERCEIRA COLUNA - NOME CIENTIFICO --}%
                <td class="text-left" style="padding-left:${raw(ficha.co_nivel_taxonomico == 'SUBESPECIE' ? '30px !important;' : '')}">
                    <g:if test="${ ficha.cd_situacao_ficha_sistema != 'EXCLUIDA' }">
                        <a style="font-weight:bold;" href="javascript:void(0);" data-action="ficha.edit"
                           data-no-cientifico="${ficha?.nm_cientifico}" data-sq-ficha="${ficha?.sq_ficha}">
                            ${raw(Util.ncItalico(ficha?.nm_cientifico?.toString(), true, true, ficha?.co_nivel_taxonomico?.toString() ) ) }
                        </a>
                    </g:if>
                    <g:else>
                        <span class="red">${raw(Util.ncItalico(ficha?.nm_cientifico?.toString(), true, true, ficha?.co_nivel_taxonomico?.toString() ) ) }</span>
                        <g:each var="versao" in="${ficha?.versoes}">
                            <br>
                            <span>
                                &nbsp;${ raw('<a id="aVersao'+versao.nu_versao+'" href="javascript:void(0);" title="Clique para visualizar a ficha versionada." onClick="showFichaVersao({sqFichaVersao:' +
                                    versao.sq_ficha_versao+'})"><span>V.'+versao.nu_versao+' <small class="gray">'+versao.dt_versao+'</small></span></a>') }
                            </span>
                        </g:each>
                   </g:else>
                </td>

                %{-- QUARTA COLUNA - UNIDADE RESPONSAVEL --}%
                <td>${ficha.sg_unidade_org}</td>

                %{-- QUINTA COLUNA - ALTERADO POR --}%
                <td class="text-center">
                    <g:if test="${ficha?.dt_alteracao}">
                        ${raw(ficha.dt_alteracao.format('dd/MM/yyyy HH:mm:ss') + '<br>' +
                            Util.capitalize(Util.getFirstName(ficha?.no_usuario_alteracao ?: '', 18)))}
                    </g:if>
                    <g:else>
                        &nbsp;
                    </g:else>
                </td>

                %{-- SEXTA COLUNA - PENDENCIAS --}%
                <td class="text-center">
                    ${raw(( ficha.nu_pendencia.toInteger() > 0 ) ? '<a href="javascript:modal.showPendenciaFicha(' + ficha.sq_ficha.toString() + ');"><span class="cursor-pointer bgRed badge">' + ficha.nu_pendencia.toString() + '</span></a>' : '<span class="bgGreen badge">0</span>')}
               </td>

                %{-- SETIMA COLUNA - SITUACAO --}%
                <td class="text-center text-nowrap">
                    <span>${ficha.ds_situacao_ficha}</span>
                    %{-- MOSTRAR A JUSTIFICATIVA DA FICHA TER SIDO EXCLUIDA --}%
                    <g:if test="${ ficha.cd_situacao_ficha_sistema == 'EXCLUIDA' }">
                            <i class="fa fa-history" title="ver histórico" data-action="ficha.verHistoricoSituacaoExcluida(${ficha.sq_ficha})"></i>
                        %{--<g:renderLongText table="ficha" id="${ficha.sq_ficha}"
                                          column="ds_justificativa"
                                          title="Ver a justificativa"
                                          text=""/>--}%
                    </g:if>

            </td>

                %{-- OITAVA COLUNA - PROGRESSO --}%
                <td class="text-center">
                    <g:progressBar percentual="${ficha.vl_percentual_preenchimento}"></g:progressBar>
                </td>


                %{-- NONA COLUNA - BOTOES --}%
                <td class="td-actions">

                %{-- FICHAS EXCLUIDAS NÃO TEM AÇÃO --}%
                <g:if test="${ ficha.cd_situacao_ficha_sistema != 'EXCLUIDA'}">
                    %{-- <g:if test="${ficha.st_alterar && session.sicae.user.canDeleteFicha()}">  --}%
                        <button type="button" data-action="ficha.delete" data-row="${i + 1}"
                                data-descricao="${ficha.nm_cientifico}" data-sq-ficha="${ficha.sq_ficha}"
                                data-cd-situacao="${ficha?.cd_situacao_ficha_sistema}"
                                class="btn btn-default btn-xs btn-delete" data-toggle="tooltip" data-placement="top"
                                title="Excluir">
                            <span class="glyphicon glyphicon-remove"></span>
                        </button>
                    %{-- </g:if> --}%
                    <button type="button" data-action="ficha.print" data-no-cientifico="${ficha.nm_cientifico}"
                            data-sq-ficha="${ficha.sq_ficha}" class="btn btn-default btn-xs" data-toggle="tooltip"
                            data-placement="top" title="Imprimir Ficha" style="color:gray;">
                        <i class="fa fa-print btnPrint" aria-hidden="true"></i>
                    </button>
                </g:if>
                </td>
            </tr>
        </g:each>
    </g:if>
    <g:else>
        <tr>
            <g:if test="${errorMessage}">
                <td colspan="9" class="text-center"><h4 class="red"><b>${errorMessage}</b></h4></td>
            </g:if>
            <g:else>
                <td colspan="9" class="text-center">
                    <g:if test="${params?.action=='getGridFicha'}">
                        <h4><b>Nenhuma ficha encontrada!</b></h4>
                        <g:if test="${session.sicae.user.isCE()}">
                            <h4 class="red">Entre em contato com a Unidade ICMBIo responsável e solicite a atribuição de fichas ao seu perfil.</h4>
                        </g:if>
                        <g:else>
                            <h5>Clique no botão Cadastrar no titulo da janela para criar fichas.</h5>
                        </g:else>
                    </g:if>
                    <g:else>
                        <h4>&nbsp;</h4>
                    </g:else>
                </td>
            </g:else>
        </tr>
    </g:else>
    </tbody>
</table>
