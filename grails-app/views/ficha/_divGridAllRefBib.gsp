<!-- view: /views/ficha/index.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action} - gridName = allRefBib -->
<table class="table table-striped table-bordered table-hover">
   <thead>
      <tr>
         %{-- <th>Autor</th> --}%
         <th>Referências Bibliográficas Utilizadas na Ficha</th>
         <th>Tags</th>
         <th>Anexo</th>
         <g:if test="${ ! session.sicae.user.isCO() }">
            <th>Ação</th>
         </g:if>
      </tr>
   </thead>
   <tbody>
      <g:each var="item" in="${listFichaRefBib}">
      <tr>
         %{-- <td>${raw(item.publicacao?item.publicacao.noAutor:item.noAutor)}</td> --}%
         <td>${raw(item.de_ref_bibliografica + '<span style="color:silver">   (Id Salve:' + item.sq_publicacao.toString() + ')<span>' )  }
             <g:if test="${item.co_nivel_taxonomico=='SUBESPECIE'}">
                 <span style="color:#e58652;">&nbsp;(subespécie)</span>
             </g:if>
         </td>

         <td>${raw( '<span class="label label-default tag">'+item?.tags  + '</span>')}</td>

          %{--ANEXOS--}%
          <td class="text-center">
              <g:if test="${item?.anexos}">
                  <g:each var="anexo" in="${item.anexos}">
                      <g:link title="Baixar documento: ${anexo.noAnexo}" class="btn fld btn-default btn-xs btn-download"
                              controller="referenciaBibliografica" action="downloadAnexo" id="${ anexo.sqAnexo }">
                          <span class="glyphicon glyphicon-download-alt"></span>
                      </g:link>
                  </g:each>
              </g:if>
          </td>
         <g:if test="${ ! session.sicae.user.isCO() }">
            <td class="td-actions">
               <g:if test="${true}">
                  <a data-action="ficha.editRefBib"
                     data-sq-publicacao="${item?.sq_publicacao}"
                     data-all="true"
                     class="fld btn btn-default btn-xs btn-update" title="Alterar">
                     <span class="glyphicon glyphicon-pencil"></span>
                  </a>
               </g:if>
                <g:if test="${ item.can_delete }">
                <a data-sq-publicacao="${item.sq_publicacao}"
                    data-params="sqFicha"
                    data-is-ci="${isCI}"
                   data-all="true"
                   data-action="ficha.deleteFichaPublicacao" class="fld btn btn-default btn-xs btn-delete" title="Excluir">
                    <span class="glyphicon glyphicon-remove"></span>
                </a>
                </g:if>
                <g:else>
                    <span class="glyphicon glyphicon-remove disabled" title="A exclusão da referência deve ser feita<br>onde ela foi utilizada."></span>
                </g:else>
%{--                <g:if test="${item.ficha.taxon.id == taxon.id}">--}%
                %{--<g:if test="${true}">
                    <a data-sq-ficha-ref-bib="${item.id}"
                       data-all="true"
                       data-action="ficha.deleteFichaRefBib" class="fld btn btn-default btn-xs btn-delete" title="Excluir">
                        <span class="glyphicon glyphicon-remove"></span>
                    </a>
                </g:if>
                <g:else>
                    <a href="javascript:void(0);" class="fld btn btn-default btn-xs btn-delete cursor-pointer">
                        <span class="disabled glyphicon glyphicon-remove" title="Referência pertence à subespécie."></span>
                    </a>
                </g:else>
                --}%


            </td>
            </td>
         </g:if>
      </tr>
      </g:each>



   <g:if test="${listFichaRefBibPortalbio}">
       <tr>
           <td colspan="20" class="td-title">Referências dos registros</td>
       </tr>
       <g:each var="item" in="${listFichaRefBibPortalbio}">
           <tr>

           <td>${raw(item.de_ref_bibliografica + '<span style="color:silver">   (Id Salve:' + item.sq_publicacao.toString() + ')<span>') }

           <td>${raw('<span class="label label-default tag">'+item.tags+'</span>')}</td>

           %{--ANEXOS--}%
           <td class="text-center">
               <g:if test="${item?.anexos }">
                   <g:each var="anexo" in="${item.anexos}">
                       <g:link title="Baixar documento: ${anexo.noAnexo}" class="btn fld btn-default btn-xs btn-download"
                               controller="referenciaBibliografica" action="downloadAnexo" id="${ anexo.sqAnexo }">
                           <span class="glyphicon glyphicon-download-alt"></span>
                       </g:link>
                   </g:each>
               </g:if>
           </td>
           <g:if test="${ ! session.sicae.user.isCO() }">
               <td class="td-actions">
                   <a data-action="ficha.editRefBib"
                      data-sq-publicacao="${item?.sq_publicacao}"
                      data-id="${item?.sq_publicacao}" data-all="true"
                      class="fld btn btn-default btn-xs btn-update" title="Alterar">
                       <span class="glyphicon glyphicon-pencil"></span>
                   </a>
               </td>
           </g:if>
           </tr>
       </g:each>
   </g:if>


   </tbody>
</table>
