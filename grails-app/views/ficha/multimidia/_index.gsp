<!-- view: /views/ficha/multimidia/_index.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->


%{--<BOTÃO PARA EXIBIR/ESCONDER O FORMULÁRIO --}%
<a id="btnToggleFrmMultimidia"
   data-toggle="collapse"
   data-target="#frmMultimidia"
   onClick="toggleImage(this)"
   title="Clique para abrir ou fechar o formulário."
   class="fld mt10 btn btn-xs btn-default collapsed">Cadastrar Imagem/áudio&nbsp;<i class="fa fa-chevron-down"></i>
</a>


<form id="frmMultimidia" name="frmMultimidia" class="form-inline row panel-collapse collapse mt10" role="form" enctype='multipart/form-data' novalidate>
    <input id="sqFichaMultimidia" value="" type="hidden" name="sqFichaMultimidia">
    <div class="col-sm-6">

        <div class="form-group">
            <label for="sqTipo" class="control-label label-required">Tipo</label>
            <br>
            <select name="sqTipo" id="sqTipo" class="fld form-control fld200 fldSelect" data-change="multimidia.sqTipoChange" required>
            <option value="">-- selecione --</option>
            <g:each var="item" in="${listTipoMultimidia}">
                <option data-codigo="${item.codigoSistema}" value="${item.id}">${item.descricao}</option>
            </g:each>
            </select>
        </div>

        <div class="form-group">
            <label for="sqSituacao" class="control-label label-required">Situação</label>
            <br>
            <select name="sqSituacao" id="sqSituacao" class="fld form-control fld300 fldSelect" data-change="multimidia.sqSituacaoChange" required>
            <option value="">-- selecione --</option>
            <g:each var="item" in="${listSituacao}">
                <option data-codigo="${item.codigoSistema}" value="${item.id}">${item.descricao}</option>
            </g:each>
            </select>
        </div>

        <div style="display:none;" id="div-texto-reprovacao">
            <div class="form-group w100p">
                <label for="txNaoAceito" class="control-label">Justificativa da reprovação</label>
                <br>
                <textarea name="txNaoAceito" id="txNaoAceito" class="fld fldTextarea w100p" style="height:100px;max-height: 200px;" maxlength="500" placeholder="máx 500 caracteres!"></textarea>
            </div>
        </div>

        <div class="form-group">
            <label for="dtElaboracao" class="control-label">Data</label>
            <br>
            <input type="text" class="form-control fld150 date" name="dtElaboracao" id="dtElaboracao">
        </div>


        %{--<IMAGEM PRINCIPAL OU NÃO   --}%
        <div class="fld form-group">
            <label for="inPrincipal" class="control-label">&nbsp;</label>
            <br>
            <label title="A imagem princial é a que será impressa na ficha." class="ml10">
                <input name="inPrincipal" id="inPrincipal" type="checkbox" class="checkbox-lg" value="S">&nbsp; Imagem principal?
            </label>
        </div>

        <div class="fld form-group">
            <label for="inDestaque" class="control-label">&nbsp;</label>
            <br>
            <label title="A imagem em destaque será exibida aleatoriamente no módulo público." class="ml10">
                <input name="inDestaque" id="inDestaque" type="checkbox" class="checkbox-lg" value="S">&nbsp; Imagem em destaque?
            </label>
        </div>

        %{--<div class="fld form-group">--}%
            %{--<label for="inPrincipal" class="control-label">&nbsp;Imagem principal?</label>--}%
            %{--<br />--}%
            %{--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input name="inPrincipal" id="inPrincipal" type="checkbox"  class="checkbox-lg" value="S">&nbsp;<span class="small">(A imagem princial é a que será impressa na ficha.)</span>--}%
        %{--</div>--}%

  <br />

        <div class="form-group">
            <label for="noAutor" class="control-label label-required">Nome do autor</label>
            <br>
            <input type="text" class="form-control fld300" name="noAutor" id="noAutor" required>
        </div>

        <div class="form-group">
            <label for="deEmailAutor" class="control-label">Email do autor</label>
            <br>
            <input type="text" class="form-control fld300" name="deEmailAutor" id="deEmailAutor">
        </div>

        <div class="form-group">
            <label for="deLegenda" class="control-label label-required">Legenda</label>
            <br>
            <input type="text" class="form-control fld300" name="deLegenda" id="deLegenda" required>
        </div>
    <br/>
        <div class="form-group w100p">
            <label for="txMultimidia" class="control-label">Descrição</label>
            <br>
            <textarea name="txMultimidia" id="txMultimidia" class="fld fldTextarea w100p"></textarea>
        </div>

        <div class="fld panel-footer">
            <button id="btnSaveFrmMultimidia"  data-action="multimidia.save" data-params="sqFicha" class="fld btn btn-success">Gravar</button>
            <button id="btnResetFrmMultimidia" data-action="multimidia.reset" class="fld btn btn-info hide">Limpar Formulário</button>
        </div>
    </div>
    <div class="col-sm-6">
            <fieldSet>
                <legend id="legend_tipo_arquivo">?</legend>
                <input type="file" class="file file-loading" name="fldAnexo" id="fldAnexo">
            </fieldSet>
    </div>
</form>
<div id="divGridMultimidia" class="mt10">
</div>
