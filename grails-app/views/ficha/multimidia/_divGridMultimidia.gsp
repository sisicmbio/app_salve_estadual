<!-- view: /views/ficha/multimidia/_divGridMultimidia.gsp -->
<!-- controller:${params?.controller} -->
<!-- action: ${params?.action} -->

<div class="row" style="padding-top: 50px">
    <div class="col-sm-12">
        <fieldset>
            <legend>
                Multimídia cadastradas no SALVE Administração
            </legend>
        </fieldset>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <table class="table table-hover table-striped table-condensed table-bordered">
            <thead>
            <th style="width:100px">Tipo</th>
            <th style="width:100px;">Situação</th>
            <th style="width:200px;">Multimidia</th>
            <th style="width:80px;">Data</th>
            <th style="width:100px;">Principal?</th>
            <th style="width:100px;">Destaque?</th>
            <th style="width:200px;">Autor</th>
            <th style="width:200px;">Email</th>
            <th style="width:200px;">Legenda</th>
            <g:if test="${ ! session.sicae.user.isCO() }">
                <th style="width:60px;">Ação</th>
            </g:if>
            </thead>
            <tbody>
            <g:if test="${listFichaMultimidia}">
                <g:each in="${listFichaMultimidia}" var="item" status="i">
                    <tr class="${item.inPrincipal ? 'success' :'' }" data-id="tr-${item.id}">
                        <td class="text-center">${item.tipo.descricao}</td>
                        <td class="text-center ${item.situacao.codigoSistema==~/PENDENTE_APROVACAO|REPROVADA/?'red':'green'}">
                            <span>${item.situacao.descricao}</span>
                            <g:if test="${item.txNaoAceito}">
                                &nbsp;<i title="${ item.txNaoAceito }
                                    <br>Reprovado por:${ item?.reprovadoPor?.noPessoa}
                                    <br>Data reprovação:${ item?.dtReprovacao?.format('dd/MM/yyyy')}" class="green fa fa-comment cursor-pointer"></i>
                            </g:if>
                        </td>
                        <td class="text-center">
                            <g:if test="${item.tipo.codigoSistema=='IMAGEM'}">
                                <img src="/salve-estadual/fichaMultimidia/getMultimidia/${item.id}/thumb?hash=${item?.noArquivoDisco?.substring(0,10)}" title="Ver imagem" data-contexto="multimidia" alt="${item.noArquivo}<br/>" class="img-rounded img-preview-grid"/>
                                <a class="fld btn btn-default btn-xs btn-download" target="_blank" title="Baixar imagem!" href="/salve-estadual/fichaMultimidia/getMultimidia/${item.id}">
                                    <span class="glyphicon glyphicon-download"></span>
                                </a>
                            </g:if>
                            <g:else>
                                <g:if test="${item.tipo.codigoSistema=='SOM'}">

                                %{--<object style="displaa:none" type="application/x-shockwave-flash" data="js/OriginalMusicPlayer.swf" width="225" height="86">--}%
                                %{--<param name="movie" value="js/OriginalMusicPlayer.swf"/>--}%
                                %{--<param name="FlashVars" value="mediaPath=/salve-estadual/fichaMultimidia/getMultimidia/${item.id}" />--}%
                                %{--</object>--}%
                                    <audio controls preload="none" volume="0.1" loop style="width:300px">
                                        <source src="/salve-estadual/fichaMultimidia/getMultimidia/${item.id}" type="audio/mpeg">
                                        Seu navegador não suporta arquivos de audio.
                                    </audio>
                                </g:if>
                            </g:else>
                        </td>
                        <td class="text-center">${item?.dtElaboracao?.format('dd/MM/yyyy')}</td>


                        %{-- IMAGEM PRINCIPAL --}%
                        <td class="text-center ${item.inPrincipal ? 'text-success' :'red' }">
                            <Label class="cursor-pointer">
                                <g:if test="${item.tipo.codigoSistema=='IMAGEM'}">
                                    <g:if test="${item.situacao.codigoSistema==/APROVADA/}">
                                        <g:if test="${item.noArquivo =~ /\.(jpe?g|png|bmp)|$/}">
                                            <input type="checkbox" class="checkbox-lg" data-change="multimidia.updateImagemPrincialFicha"
                                                data-params="sqFicha,sqFichaMultimidia:${item.id}" ${ item.inPrincipal ? 'checked':''}>&nbsp;
                                        </g:if>
                                    </g:if>
                                </g:if>
                            </Label>
                        </td>

                        %{-- IMAGEM EM DESTAQUE--}%

                        <td class="text-center ${item.inDestaque ? 'text-success' :'red' }">
                            <Label class="cursor-pointer">
                                <g:if test="${item.tipo.codigoSistema=='IMAGEM'}">
                                    <g:if test="${item.situacao.codigoSistema==/APROVADA/}">
                                        <g:if test="${item.noArquivo =~ /\.(jpe?g|png|bmp)|$/}">
                                            <input type="checkbox" class="checkbox-lg" data-change="multimidia.updateImagemDestaqueFicha"
                                                data-params="sqFicha,sqFichaMultimidia:${item.id}" ${ item.inDestaque ? 'checked':''}>&nbsp;
                                        </g:if>
                                    </g:if>
                                </g:if>
                            </Label>

                        </td>
                        <td>${item.noAutor}</td>
                        <td>${item.deEmailAutor}</td>
                        <td>${item.deLegenda}</td>
                        <g:if test="${ ! session.sicae.user.isCO() }">
                            <td class="td-actions">
                                <a data-action="multimidia.edit" data-id="${item.id}" class="fld btn btn-default btn-xs btn-update" title="Alterar">
                                    <span class="glyphicon glyphicon-pencil"></span>
                                </a>
                                <a type="button" data-action="multimidia.delete" data-row="${i + 1}" data-descricao="${item.noArquivo}" data-id="${item?.id}" class="fld btn btn-default btn-xs btn-delete" data-toggle="tooltip" data-placement="top" title="Excluir">
                                    <span class="glyphicon glyphicon-remove"></span>
                                </a>
                            </td>
                        </g:if>
                    </tr>
                    <g:if test="${item.txMultimidia}">
                        <tr class="tr-descricao" data-id="tr-desc-${item.id}">
                            <td colspan="9" class="text-justify" >
                                <div style="padding:5px;color:#228b22">${item.txMultimidia}</div>
                            </td>
                        </tr>
                    </g:if>
                </g:each>
            </g:if>
            <g:else>
                <tr><td colspan="10" align="center"><h4>Nenhuma mídia cadastrada!</h4></td></tr>
            </g:else>
            </tbody>
        </table>
    </div>
</div>


<div class="row" style="padding-top: 50px">
    <div class="col-sm-12">
        <fieldset>
            <legend>
                Fotos enviadas por colaboradores
            </legend>
        </fieldset>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <table class="table table-hover table-striped table-condensed table-bordered">
            <thead>
            <th style="width:100px">Tipo</th>
            <th style="width:100px;">Situação</th>
            <th style="width:200px;">Multimidia</th>
            <th style="width:80px;">Data</th>
            <th style="width:100px;">Principal?</th>
            <th style="width:100px;">Destaque?</th>
            <th style="">Termo Aceite</th>
            <th style="width:200px;">Autor</th>
            <th style="width:200px;">Email</th>
            <th style="width:200px;">Legenda</th>
            <g:if test="${ ! session.sicae.user.isCO() }">
                <th style="width:60px;">Ação</th>
            </g:if>
            </thead>
            <tbody>
            <g:if test="${listFichaConsultaFoto}">
                <g:each in="${listFichaConsultaFoto}" var="item" status="i">
                    <tr class="${item.fichaMultimidia.inPrincipal ? 'success' :'' }" data-id="tr-${item.fichaMultimidia.id}">
                        <td class="text-center">${item.fichaMultimidia.tipo.descricao}</td>
                        <td class="text-center ${item.fichaMultimidia.situacao.codigoSistema==~/PENDENTE_APROVACAO|REPROVADA/?'red':'green'}">
                            <span>${item.fichaMultimidia.situacao.descricao}</span>
                            <g:if test="${item.fichaMultimidia.txNaoAceito}">
                                &nbsp;<i title="${ item.fichaMultimidia.txNaoAceito }
                                     <br>Reprovado por:${ item?.fichaMultimidia?.reprovadoPor?.noPessoa}
                                     <br>Data reprovação:${ item?.fichaMultimidia?.dtReprovacao?.format('dd/MM/yyyy')}" class="green fa fa-comment cursor-pointer"></i>
                            </g:if>
                        </td>
                        <td class="text-center">
                            <g:if test="${item.fichaMultimidia.tipo.codigoSistema == 'IMAGEM'}">
                                <img src="/salve-estadual/fichaMultimidia/getMultimidia/${item.fichaMultimidia.id}/thumb?hash=${item?.fichaMultimidia.noArquivoDisco?.substring(0,10)}" title="Ver imagem" data-contexto="multimidia" alt="${item.fichaMultimidia.noArquivo}<br/>" class="img-rounded img-preview-grid"/>
                                <a class="fld btn btn-default btn-xs btn-download" target="_blank" title="Baixar imagem!" href="/salve-estadual/fichaMultimidia/getMultimidia/${item.fichaMultimidia.id}">
                                    <span class="glyphicon glyphicon-download"></span>
                                </a>
                            </g:if>
                        </td>
                        <td class="text-center">${item?.fichaMultimidia.dtElaboracao?.format('dd/MM/yyyy')}</td>


                        %{-- IMAGEM PRINCIPAL --}%
                        <td class="text-center ${item.fichaMultimidia.inPrincipal ? 'text-success' :'red' }">
                            <Label class="cursor-pointer">
                                <g:if test="${item.inTipoTermoResp == 'USO_IRRESTRITO'}">
                                    <g:if test="${item.fichaMultimidia.situacao.codigoSistema==/APROVADA/}">
                                        <g:if test="${item.fichaMultimidia.noArquivo =~ /\.(jpe?g|png|bmp)|$/}">
                                            <input type="checkbox" class="checkbox-lg" data-change="multimidia.updateImagemPrincialFicha"
                                                data-params="sqFicha,sqFichaMultimidia:${item.fichaMultimidia.id}" ${ item.fichaMultimidia.inPrincipal ? 'checked':''}>&nbsp;
                                        </g:if>
                                    </g:if>
                                </g:if>
                            </Label>
                        </td>

                        %{-- IMAGEM EM DESTAQUE--}%
                        <td class="text-center ${item.fichaMultimidia.inDestaque ? 'text-success' :'red' }">
                            <Label class="cursor-pointer">
                                <g:if test="${item.inTipoTermoResp=='USO_IRRESTRITO'}">
                                    <g:if test="${item.fichaMultimidia.situacao.codigoSistema==/APROVADA/}">
                                        <g:if test="${item.fichaMultimidia.noArquivo =~ /\.(jpe?g|png|bmp)|$/}">
                                            <input type="checkbox" class="checkbox-lg" data-change="multimidia.updateImagemDestaqueFicha"
                                                data-params="sqFicha,sqFichaMultimidia:${item.fichaMultimidia.id}" ${ item.fichaMultimidia.inDestaque ? 'checked':''}>&nbsp;
                                        </g:if>
                                    </g:if>
                                </g:if>
                            </Label>

                        </td>
                        <td>
                            <g:if test="${item.inTipoTermoResp=='USO_RESTRITO_AVALIACAO'}">
                                Uso relacionado ao processo<br><small style="color: gray">Origem: Salve Consulta</small>
                            </g:if>
                            <g:else>
                                <g:if test="${item.inTipoTermoResp=='USO_IRRESTRITO'}">
                                    Uso amplo e irrestrito<br><small style="color: gray">Origem: Salve Consulta</small>
                                </g:if>
                            </g:else>
                        </td><!-- Uso irrestrito -->
                        <td>${item.fichaMultimidia.noAutor}</td>
                        <td>${item.fichaMultimidia.deEmailAutor}</td>
                        <td>${item.fichaMultimidia.deLegenda}</td>
                        <g:if test="${ ! session.sicae.user.isCO() }">
                            <td class="td-actions">
                                <a data-action="multimidia.edit" data-id="${item.fichaMultimidia.id}" data-tipo_termo_resp="${item.inTipoTermoResp}" class="fld btn btn-default btn-xs btn-update" title="Alterar">
                                    <span class="glyphicon glyphicon-pencil"></span>
                                </a>
                                <a type="button" data-action="multimidia.delete" data-row="${i + 1}" data-descricao="${item.fichaMultimidia.noArquivo}" data-id="${item?.fichaMultimidia.id}" class="fld btn btn-default btn-xs btn-delete" data-toggle="tooltip" data-placement="top" title="Excluir">
                                    <span class="glyphicon glyphicon-remove"></span>
                                </a>
                            </td>
                        </g:if>
                    </tr>
                    <g:if test="${item.fichaMultimidia.txMultimidia}">
                        <tr class="tr-descricao" data-id="tr-desc-${item.fichaMultimidia.id}">
                            <td colspan="9" class="text-justify" >
                                <div style="padding:5px;color:#228b22">${item.fichaMultimidia.txMultimidia}</div>
                            </td>
                        </tr>
                    </g:if>
                </g:each>
            </g:if>
            <g:else>
                <tr><td colspan="10" align="center"><h4>Nenhuma foto cadastrada!</h4></td></tr>
            </g:else>
            </tbody>
        </table>
    </div>
</div>
