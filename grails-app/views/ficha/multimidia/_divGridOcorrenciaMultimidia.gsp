<div class="row">
    <div class="col-sm-12">
        <table class="table table-hover table-striped table-condensed table-bordered mt10">
            <thead>
                <th style="width:100px">Tipo</th>
                <th style="width:100px;">Situação</th>
                <th style="width:200px;">Multimidia</th>
                <th style="width:80px;">Data</th>
                <th style="width:100px;">Principal?</th>
                <th style="width:100px;">Destaque?</th>
                <th style="width:200px;">Autor</th>
                <th style="width:200px;">Email</th>
                <th style="width:200px;">Legenda</th>
                <g:if test="${ ! session.sicae.user.isCO() }">
                    <th style="width:60px;">Ação</th>
                </g:if>
            </thead>
            <tbody>
                <g:if test="${listFichaOcorrenciaMultimidia}">
                    <g:each in="${listFichaOcorrenciaMultimidia}" var="item" status="i">
                        <tr>
                            <td class="text-center">${item.fichaMultimidia.tipo.descricao}</td>
                            <td class="text-center ${item.fichaMultimidia.situacao.codigoSistema=='PENDENTE_APROVACAO'?'red':'green'}">${item.fichaMultimidia.situacao.descricao}</td>
                            <td class="text-center">
                            <g:if test="${item.fichaMultimidia.tipo.codigoSistema=='IMAGEM'}">
                                <img src="/salve-estadual/fichaMultimidia/getMultimidia/${item.fichaMultimidia.id}/thumb" title="Ver imagem" data-contexto="multimidia" alt="${item.fichaMultimidia.noArquivo}<br/>" class="img-rounded img-preview-grid"/>
                                <a class="fld btn btn-default btn-xs btn-download" target="_blank" title="Baixar imagem!" href="/salve-estadual/fichaMultimidia/getMultimidia/${item.fichaMultimidia.id}">
                                    <span class="glyphicon glyphicon-download"></span>
                                </a>
                            </g:if>
                            <g:else>
                                <g:if test="${item.fichaMultimidia.tipo.codigoSistema=='SOM'}">

                                    %{--<object style="displaa:none" type="application/x-shockwave-flash" data="js/OriginalMusicPlayer.swf" width="225" height="86">--}%
                                              %{--<param name="movie" value="js/OriginalMusicPlayer.swf"/>--}%
                                              %{--<param name="FlashVars" value="mediaPath=/salve-estadual/fichaMultimidia/getMultimidia/${item.fichaMultimidia.id}" />--}%
                                    %{--</object>--}%
                                    <audio controls preload="none" volume="0.1" loop style="width:300px">
                                        <source src="/salve-estadual/fichaMultimidia/getMultimidia/${item.fichaMultimidia.id}" type="audio/mpeg">
                                        Seu navegador não suporta arquivos de audio.
                                    </audio>
                                </g:if>
                            </g:else>
                            </td>
                            <td class="text-center">${item.fichaMultimidia.dtElaboracao.format('dd/MM/yyyy')}</td>
                            <td class="text-center">${item.fichaMultimidia.inPrincipalText}</td>
                            <td class="text-center">${item.fichaMultimidia.inDestaqueText}</td>
                            <td>${item.fichaMultimidia.noAutor}</td>
                            <td>${item.fichaMultimidia.deEmailAutor}</td>
                            <td>${item.fichaMultimidia.deLegenda}</td>
                            <g:if test="${ ! session.sicae.user.isCO() }">
                                <td class="td-actions">
                                    <a data-action="tabMultimidia.edit" data-sq-ficha-multimidia="${item.fichaMultimidia.id}" data-sq-ficha-ocorrencia-multimidia="${item.id}" class="btn btn-default btn-xs btn-update" title="Alterar">
                                        <span class="glyphicon glyphicon-pencil"></span>
                                    </a>
                                    <a type="button" data-action="tabMultimidia.delete" data-row="${i + 1}" data-descricao="${item.fichaMultimidia.noArquivo}" data-sq-ficha-multimidia="${item.fichaMultimidia.id}" data-sq-ficha-ocorrencia-multimidia="${item.id}" class="btn btn-default btn-xs btn-delete" data-toggle="tooltip" data-placement="top" title="Excluir">
                                        <span class="glyphicon glyphicon-remove"></span>
                                    </a>
                                </td>
                            </g:if>
                        </tr>
                        <g:if test="${item.fichaMultimidia.txMultimidia}">
                            <tr class="tr-descricao">
                                <td colspan="9" class="text-justify" >
                                    <div style="padding:5px;color:#ff6d05">${item.fichaMultimidia.txMultimidia}</div>
                                </td>
                            </tr>
                        </g:if>
                    </g:each>
                </g:if>
                <g:else>
                    <tr><td colspan="10" align="center"><h4>Nenhuma mídia cadastrada!</h4></td></tr>
                </g:else>
            </tbody>
        </table>
    </div>
</div>
