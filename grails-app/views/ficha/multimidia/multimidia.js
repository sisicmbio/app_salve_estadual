//# sourceURL=N:\SAE\sae\grails-app\views\ficha\multimidia\multimidia.js
//
;
var multimidia = {
    init: function() {
        multimidia.updateGride();
        multimidia.sqTipoChange();

        $('#frmMultimidia').on('hidden.bs.collapse', function(e) {
            multimidia.reset()
            $("#btnToggleFrmMultimidia i").removeClass('glyphicon-minus');
        });
        $('#frmMultimidia').on('show.bs.collapse', function(e) {
            $("#btnToggleFrmMultimidia i").addClass('glyphicon-minus');
        });
    },
    save: function(params) {
        if (!$("#frmMultimidia").valid()) {
            return;
        }
        var data = $("#frmMultimidia").serializefiles();
        data.append('sqFicha', params.sqFicha);

        if (!data.get('sqFicha')) {
            app.alertError('ID da ficha não encontrado!');
            return;
        }
        if( $("#sqSituacao").find('option:selected').data('codigo') == 'REPROVADA' )
        {
            if( trimEditor( $("#txNaoAceito").val() ) == '' )
            {
                app.alertInfo('Necessário informar a justificativa da reprovação','',function(){
                    app.focus("#txNaoAceito");
                });
                return;
            }
        }
        app.ajax(app.url + 'fichaMultimidia/save', data,
            function(res) {
                if (res.status == 0) {
                    app.reset('frmMultimidia');
                    app.focus('sqTipo','frmMultimidia');
                    multimidia.updateGride();
                    $("##frmMultimidia #btnResetFrmMultimidia").addClass('hide');
                }
            }, 'Gravando...', 'json');
    },
    reset: function(params) {
        app.reset('frmMultimidia');
        multimidia.sqSituacaoChange();
        $("#frmMultimidia #btnResetFrmMultimidia").addClass('hide');
        app.unselectGridRow('divGridMultimidia');
        app.resetChanges('frmMultimidia'); // retirar a cor vermelha da aba
    },
    updateGride: function() {
        var data = {
            sqFicha: $("#sqFicha").val(),
        };
        app.ajax(app.url + 'fichaMultimidia/getGrid', data,
            function(res) {
                $("#divGridMultimidia").html(res);
                bindDialogPreview('divGridMultimidia');
            });

    },
    edit: function(params, btn) {
        app.ajax(app.url + 'fichaMultimidia/edit', params, function(res) {
            if (res) {
                $("#frmMultimidia").collapse('show');
                multimidia.reset();
                app.selectGridRow(btn);
                app.setFormFields(res, 'frmMultimidia');
                multimidia.sqTipoChange();
                $("#btnResetFrmMultimidia").removeClass('hide');
                multimidia.sqSituacaoChange();
            }
        }, 'Carregando...', 'JSON');
    },
    delete: function(params, btn) {
        app.selectGridRow(btn);
        app.confirm('<br>Confirma exclusão do arquivo <b>' + params.descricao + '</b>?',
            function() {
                app.ajax(app.url + 'fichaMultimidia/delete',
                    params,
                    function(res) {
                        app.unselectGridRow(btn);
                        if (!res) {
                            var trFoto = btn.closest('tr');
                            var trDescricao = trFoto.next('tr');
                            if (trDescricao.hasClass('tr-descricao')) {
                                trDescricao.remove();
                            }
                            trFoto.remove();
                        }
                    });
            },
            function() {
                app.unselectGridRow(btn);
            }, params, 'Confirmação', 'warning');
    },
    sqTipoChange: function(params) {
        if (!$("#frmMultimidia #sqSituacao").val()) {
            $("#frmMultimidia #sqSituacao").val($("#frmMultimidia #sqSituacao option[data-codigo=PENDENTE_APROVACAO]").val())
        }
        var dataAllowedPreviewTypes = ['audio'];
        var dataAllowedFileEextensions = ['mp3','ogg','wav'];
        var label = 'Selecionar'

        if ($("#frmMultimidia #sqTipo").val()) {
            $("#frmMultimidia #legend_tipo_arquivo").html($("#frmMultimidia #sqTipo option:selected").text());
            var codigo = $("#frmMultimidia #sqTipo option:selected").data('codigo');
            if (codigo == 'IMAGEM') {
                dataAllowedPreviewTypes = ['image'];
                dataAllowedFileEextensions = ['jpg','jpeg', 'bmp', 'png', 'gif'];
                label = 'Selecionar Imagem'
                $("#frmMultimidia #inPrincipal,#inDestaque").parent().show();
            } else if (codigo == 'SOM') {
                label = 'Selecionar Áudio'
                $("#frmMultimidia #inPrincipal,#inDestaque").parent().hide();
                $("#frmMultimidia #inPrincipal,#inDestaque").prop('checked', false)
            }
            $('#frmMultimidia #fldAnexo').fileinput('destroy');
            $('#frmMultimidia #fldAnexo').fileinput({
                'language': fileInputLanguage,
                'layoutTemplates': fileInputLayoutTemplates,
                'previewFileIconSettings': fileInputPreviewFileIconSettings,
                'browseLabel': label,
                'previewFileType': dataAllowedPreviewTypes,
                'showPreview': true,
                'showCaption': false,
                'showUpload': false,
                'allowedPreviewTypes': dataAllowedPreviewTypes,
                'allowedFileExtensions': dataAllowedFileEextensions,
            });
            $("#frmMultimidia #fldAnexo").parent().parent().parent().show();
        } else {
            $("#frmMultimidia #legend_tipo_arquivo").html('?');
            $("#frmMultimidia #fldAnexo").parent().parent().parent().hide();
            $("#frmMultimidia #inPrincipal,#inDestaque").parent().hide();
            $("#frmMultimidia #inPrincipal,#inDestaque").prop('checked', false)
        }
    },
    sqSituacaoChange : function(params,fld,evt) {
        var $div = $("#div-texto-reprovacao");
        if ($("#sqSituacao").find('option:selected').data('codigo') == 'REPROVADA') {
            $div.show();
        } else {
            $div.hide();
        };
    },
    updateImagemPrincialFicha:function( params,fld, evt ){
        var data ={sqFicha:params.sqFicha,sqFichaMultimidia:params.sqFichaMultimidia}
        data.checked = fld.checked ? 'S': 'N';
        app.ajax( app.url + 'fichaMultimidia/toggleImagemPrincipal', data, function(res) {
            multimidia.updateGride();
        }, 'Aguarde...', 'json');

    },
    updateImagemDestaqueFicha:function( params,fld, evt ){
        var data ={sqFicha:params.sqFicha,sqFichaMultimidia:params.sqFichaMultimidia}
        data.checked = fld.checked ? 'S': 'N';
        app.ajax( app.url + 'fichaMultimidia/toggleImagemDestaque', data, function(res) {
            multimidia.updateGride();
        }, 'Aguarde...', 'json');
    }
};
multimidia.init();
