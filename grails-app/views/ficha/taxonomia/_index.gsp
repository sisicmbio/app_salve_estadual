<!-- view: /views/ficha/taxonomia/_index.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

%{--
Formulário de seleção da espécie ou subespécie para iniciar o preenchimento da ficha
--}%
%{-- selecionar taxon --}%
<form id="frmTaxonomia" name="frmTaxonomia" class="form-inline row" role="form">
   <input id="sqTaxon" value="${ficha.taxon.id}" type="hidden" name="sqTaxon">
   <div class="col-sm-12">
      <g:if test="${ ! ficha.id }">
      <div id="divSearchTaxonTree">
         <fieldset id="fsSearchTaxonTree">
             <div class="text-center" style="display:block;font-size: 1.75rem;" >
                 <div class="form-group ">
                     <div class="checkbox mr15">
                         <label>
                             <input type="checkbox" name="w_diferenciar_letra" class="ignoreChange" value="S"> diferenciar maiúscula/minúscula
                         </label>
                     </div>
                 </div>
                 <div class="form-group">
                    <div class="radio mr15">
                       <label>
                          <input type="radio" name="w_like" value="iniciado_por" checked="true" class="ignoreChange"> iniciado por
                       </label>
                    </div>
                    <div class="radio mr15">
                         <label>
                             <input type="radio" name="w_like" value="contem_palavra" class="ignoreChange"> contém o texto
                         </label>
                     </div>
                     <div class="radio mr15">
                         <label>
                             <input type="radio" name="w_like" value="palavra_exata" class="ignoreChange"> contém a palavra
                         </label>
                     </div>
                 </div>
             </div>
            <hr class="hr-separador">

             <g:if test="${ ! session.sicae.user.sqUnidadeOrg}">
                 <div class="fld form-group">
                     <label for="sqUnidadeOrg" class="control-label"><b>Unidade responsável</b></label>
                     <br>
                     <select id="sqUnidadeOrg" name="sqUnidadeOrg" class="form-control select2 fld600"
                             data-s2-minimum-input-length="3"
                             data-s2-select-on-close="false"
                             data-s2-placeholder="-- selecione --"
                             data-s2-params="sqCicloAvaliacao"
                             data-s2-url="main/select2Instituicao">
                     </select>
                 </div>
                 <br>
             </g:if>

            <div class="form-group">
               <label for="w_idReino" class="control-label">Reino</label>
               <br>
               <select id="w_idReino" class="form-control select2 ignoreChange fld250"
                  data-s2-params="prioridade:1,nivel:'REINO',w_diferenciar_letra,w_like,ativos:true"
                  data-s2-url="ficha/select2Taxon"
                  data-s2-on-change="ficha.onTaxonChange"
                  data-s2-placeholder="?"
                  data-s2-minimum-input-length="0"
                  data-s2-maximum-selection-length="2">
               </select>
            </div>
            <div class="form-group">
               <label for="w_idFilo" class="control-label">Filo</label>
               <br>
               <select id="w_idFilo" class="form-control select2 ignoreChange fld250"
                  data-s2-params="prioridade:2,nivel:'FILO',w_idReino,w_diferenciar_letra,w_like,ativos:true"
                  data-s2-url="ficha/select2Taxon"
                  data-s2-on-change="ficha.onTaxonChange"
                  data-s2-placeholder="?"
                  data-s2-minimum-input-length="0"
                  data-s2-maximum-selection-length="2">
               </select>
            </div>

            <div class="form-group">
               <label for="w_idClasse" class="control-label">Classe</label>
               <br>
               <select id="w_idClasse" class="form-control select2 ignoreChange fld250"
                  data-s2-params="prioridade:3,nivel:'CLASSE',w_idReino,w_idFilo,w_diferenciar_letra,w_like,ativos:true"
                  data-s2-url="ficha/select2Taxon"
                  data-s2-on-change="ficha.onTaxonChange"
                  data-s2-placeholder="?"
                  data-s2-minimum-input-length="0"
                  data-s2-maximum-selection-length="2">
               </select>
            </div>


            <div class="form-group">
               <label for="w_idOrdem" class="control-label">Ordem</label>
               <br>
               <select id="w_idOrdem" class="form-control select2 ignoreChange fld250"
                  data-s2-params="prioridade:4,nivel:'ORDEM',w_idReino,w_idFilo,w_idClasse,w_diferenciar_letra,w_like,ativos:true"
                  data-s2-url="ficha/select2Taxon"
                  data-s2-on-change="ficha.onTaxonChange"
                  data-s2-placeholder="?"
                  data-s2-minimum-input-length="0"
                  data-s2-maximum-selection-length="2">
               </select>
            </div>

            <div class="form-group">
               <label for="w_idFamilia" class="control-label">Família</label>
               <br>
               <select id="w_idFamilia" class="form-control select2 ignoreChange fld250"
                  data-s2-params="prioridade:5,nivel:'FAMILIA',w_idReino,w_idFilo,w_idClasse,w_idOrdem,w_diferenciar_letra,w_like,ativos:true"
                  data-s2-url="ficha/select2Taxon"
                  data-s2-on-change="ficha.onTaxonChange"
                  data-s2-placeholder="?"
                  data-s2-maximum-selection-length="2">
               </select>
            </div>

            <div class="form-group">
               <label for="w_idSubfamilia" class="control-label">Subfamilia</label>
               <br>
               <select id="w_idSubfamilia" class="form-control select2 ignoreChange fld250"
                  data-s2-params="prioridade:6,nivel:'SUBFAMILIA',w_idReino,w_idFilo,w_idClasse,w_idOrdem,w_idFamilia,w_diferenciar_letra,w_like,ativos:true"
                  data-s2-url="ficha/select2Taxon"
                  data-s2-on-change="ficha.onTaxonChange"
                  data-s2-placeholder="?"
                  data-s2-maximum-selection-length="2">
               </select>
            </div>

             <div class="form-group">
                 <label for="w_idTribo" class="control-label">Tribo</label>
                 <br>
                 <select id="w_idTribo" class="form-control select2 ignoreChange fld250"
                         data-s2-params="prioridade:7,nivel:'TRIBO',w_idReino,w_idFilo,w_idClasse,w_idOrdem,w_idFamilia,w_idSubfamilia,w_diferenciar_letra,w_like"
                         data-s2-url="ficha/select2Taxon"
                         data-s2-on-change="ficha.onTaxonChange"
                         data-s2-placeholder="?"
                         data-s2-maximum-selection-length="2">
                 </select>
             </div>

             <div class="form-group">
               <label for="w_idGenero" class="control-label">Genero</label>
               <br>
               <select id="w_idGenero" class="form-control select2 ignoreChange fld250"
                  data-s2-params="prioridade:8,nivel:'GENERO',w_idReino,w_idFilo,w_idClasse,vw_idOrdem,w_idFamilia,w_idSubfamilia,w_idTribo,w_diferenciar_letra,w_like,ativos:true"
                  data-s2-url="ficha/select2Taxon"
                  data-s2-on-change="ficha.onTaxonChange"
                  data-s2-placeholder="?"
                  data-s2-maximum-selection-length="2">
               </select>
            </div>


            <div class="form-group">
               <label for="w_idEspecie" class="control-label blue">Espécie</label>
               <br>
               <select id="w_idEspecie" name="w_idEspecie" class="form-control select2 ignoreChange fld250"
                  data-s2-params="prioridade:9,nivel:'ESPECIE',w_idReino,w_idFilo,w_idClasse,w_idOrdem,w_idFamilia,w_idSubfamilia,w_idTribo,w_idGenero,w_diferenciar_letra,w_like,w_diferenciar_letra,w_like,ativos:true"
                  data-s2-url="ficha/select2Taxon"
                  data-s2-on-change="ficha.onTaxonChange"
                  data-s2-placeholder="?"
                  data-s2-minimum-input-length="3"
                  data-s2-maximum-selection-length="2">
               </select>
            </div>

            <div class="form-group">
               <label for="w_idSubespecie" class="control-label">Subespécie</label>
               <br>
               <select id="w_idSubespecie" name="w_idSubespecie" class="form-control select2 ignoreChange fld300"
                  data-s2-params="prioridade:10,nivel:'SUBESPECIE',w_idReino,w_idFilo,w_idClasse,w_idFamilia,w_idSubfamilia,w_idTribo,w_idGenero,w_idEspecie,w_idEspecie,w_diferenciar_letra,w_like,w_diferenciar_letra,w_like,ativos:true"
                  data-s2-url="ficha/select2Taxon"
                  data-s2-on-change="ficha.onTaxonChange"
                  data-s2-placeholder=" "
                  data-s2-maximum-selection-length="2">
               </select>
            </div>

             <br>

             <div class="form-group">
                 <label for="sqGrupo" class="label-required control-label"><b>Grupo avaliado</b></label>
                 <br>
                 <select name="sqGrupo" id="sqGrupo" data-change="taxonomia.sqGrupoChange" class="form-control fld fld300 fldSelect" value="${ficha?.grupo?.id}">
                     <option value="">?</option>
                     <g:each var="item" in="${grupos}">
                         <option data-codigo="${item.codigoSistema}" value="${item.id}" ${item.id == ficha?.grupo?.id ? "selected" :'' }>${item.descricao}</option>
                     </g:each>
                 </select>
             </div>
             <div class="form-group">
                 <label for="sqSubgrupo" class="control-label"><b>Subgrupo</b> (opcional)</label>
                 <br>
                 <select name="sqSubgrupo" id="sqSubgrupo" data-change="taxonomia.sqSubgrupoChange"
                         class="form-control fld fld300 fldSelect select2"
                         data-s2-minimum-input-length="0"
                         data-s2-auto-height="true"
                         data-s2-params="sqGrupo"
                         data-s2-url="ficha/select2Subgrupo">
                 </select>
             </div>


             %{--<g:if test = "${ listPontoFocal }">
                 <br>
                 <div class="form-group">
                     <label for="" class="control-label"><b>Ponto focal</b></label>
                     <br>
                     <select name="sqPontoFocal" class="form-control fldSelect ignoreChange fld600" style="color: #0D76E5;">
                         <option value="">-- não atribuir agora --</option>
                         <g:each var="pontoFocal" in="${listPontoFocal}">
                            <option value="${ pontoFocal.id}">${br.gov.icmbio.Util.capitalize(pontoFocal.noPessoa)} - ${pontoFocal.nuCpfFormatado}</option>
                         </g:each>
                     </select>
                 </div>
             </g:if>--}%
             <g:if test="${ session.sicae.user.isADM() || session.sicae.user.isPF() }">
                 <g:if test="${ session.sicae.user.isADM()}">
                     <br>
                     <div class="form-group">
                         <label for="sqPontoFocal" class="control-label"><b>Ponto focal</b></label>
                         <br>
                         <select name="sqPontoFocal" id="sqPontoFocal" class="form-control fld600 select2" style="color: #0D76E5;"
                                 data-s2-url="gerenciarPapel/select2PessoaFisica"
                                 data-s2-placeholder="?"
                                 data-s2-minimum-input-length="3"
                                 data-s2-params="sqPapel:855"
                                 data-s2-auto-height="true">
                         </select>
                     </div>
                 </g:if>
                 <br>
                 <div class="form-group">
                     <label for="sqCoordenadorTaxon" class="control-label"><b>Coordenador de táxon</b></label>
                     <br>
                     <select name="sqCoordenadorTaxon" id="sqCoordenadorTaxon" class="form-control fld600 select2" style="color: #0D76E5;"
                             data-s2-url="gerenciarPapel/select2PessoaFisica"
                             data-s2-placeholder="?"
                             data-s2-minimum-input-length="3"
                             data-s2-on-change="taxonomia.sqCoordenadorTaxonChange"
                             data-s2-params="sqGrupo"
                             data-s2-auto-height="true">
                     </select>
                 </div>
                 <br>
                 <div class="fld form-group" style="display: none">
                     <label class="control-label"><b>Instituição do coordenador de táxon</b></label>
                     <br>
                     <select name="sqWebInstituicaoCT" id="sqWebInstituicaoCT" class="form-control fld600 select2"
                             data-s2-url="main/select2WebInstituicao"
                             data-s2-placeholder="?"
                             data-s2-minimum-input-length="3"
                             data-s2-auto-height="true">
                     </select>
                 </div>
             </g:if>
         </fieldset>
      </div>
      <%-- fim divSearchTaxonTree--%>
      </g:if>
      <g:else>
      <div>
         <div class="form-group">
            <label for="divNmCientifico" class="control-label">Nome científico</label>
            <br>
            <div id="divNmCientifico" class="controls readonly" style="color:blue;font-weight:bold;">
               ${raw(ficha.taxon.noCientificoCompletoItalico)}
            </div>
            %{-- <input name="nmCientifico" id="nmCientifico" type="text" class="form-control fld fld400 fldSelect" style="color:#0000FF;" value="> --}%

         </div>
         <div class="form-group">
            <label for="sqGrupo" class="control-label">Grupo avaliado</label>
            <br>
            <select name="sqGrupo" id="sqGrupo" data-change="taxonomia.sqGrupoChange" class="form-control fld fld300 fldSelect" value="${ficha?.grupo?.id}">
               <option value="">?</option>
               <g:each var="item" in="${grupos}">
               <option data-codigo="${item.codigoSistema}" value="${item.id}" ${item.id == ficha?.grupo?.id ? "selected" :'' }>${item.descricao}</option>
               </g:each>
            </select>
         </div>
          <div class="form-group">
            <label for="sqSubgrupo" class="control-label">Subgrupo</label>
            <br>
            <select name="sqSubgrupo" id="sqSubgrupo" data-change="taxonomia.sqSubgrupoChange"
                    class="form-control fld fld300 fldSelect select2"
                    data-s2-minimum-input-length="0"
                    data-s2-auto-height="true"
                    data-s2-params="sqGrupo"
                    data-s2-url="ficha/select2Subgrupo"
                    data-current-grupo="${ficha?.grupo?.id}"
                    data-current-value="${ficha?.subgrupo?.id}"
                    data-current-text="${ficha?.subgrupo?.descricao}">
               %{--<option value="">?</option>
               <g:each var="item" in="${subgrupos}">
               <option data-codigo="${item.codigoSistema}" value="${item.id}" ${item.id == ficha?.subgrupo?.id ? "selected" :'' }>${item.descricao}</option>
               </g:each>--}%
            </select>
         </div>

%{--          GRUPOS SALVE--}%
          <g:if test="${session?.sicae?.user?.isADM() }">
              <div class="form-group">
                  <label for="sqGrupoSalve" class="control-label">Recorte módulo público</label>
                  <br>
                  <select name="sqGrupoSalve" id="sqGrupoSalve" class="form-control fld fld300 fldSelect" value="${ficha?.grupoSalve?.id}">
                      <option value="">-- nenhum --</option>
                      <g:each var="item" in="${gruposSalve}">
                          <option data-codigo="${item.codigoSistema}" value="${item.id}" ${item.id == ficha?.grupoSalve?.id ? "selected" :'' }>${item.descricao}</option>
                      </g:each>
                  </select>
              </div>
          </g:if>
      </div>
      </g:else>
   </div>
</form>

<div class="row" style="padding-top:10px;">
   %{-- nome comun --}%
   <div id="divNomeComum" class="col-sm-12" style="display: none;">
      <g:render template="taxonomia/divNomeComum"></g:render>
   </div>
   %{-- fim col-sm-12 - nome comum --}% %{-- sinonimias --}%
</div>

<div class="row">
   <div id="divSinonimia" class="col-sm-12" style="display: none;">
      <g:render template="taxonomia/divSinonimia" model="['ficha':ficha]"></g:render>
   </div>
</div>
<div class="row">
   %{-- fim col-sm-12 - sinonimia --}%
   <div id="divNotaTaxonomica" class="col-sm-12" style="display: none;">
      <div class="form-group w100p">
         <label for="dsNotasTaxonomicas" class="control-label">Notas Taxonômicas<i data-action="ficha.openModalSelRefBibFicha" data-no-tabela="ficha" data-no-coluna="ds_notas_taxonomicas" data-sq-registro="${ficha.id}" data-de-rotulo="Notas Taxonômicas" class="fa fa-book cursor-pointer ml10" title="Adicionar/Remover Refeferência bibliográfica." style="color: green;" /></label>
         <br>
         <textarea name="dsNotasTaxonomicas" id="dsNotasTaxonomicas" class="form-control fld w100p fldTextarea wysiwyg" rows="10">${ficha.dsNotasTaxonomicas}</textarea>
      </div>
   </div>
</div>
<div class="row">
   %{-- fim col-sm-12 - nota toxonomica --}%
   <div id="divNotaMorfologica" class="col-sm-12" style="display: none;">
    <div class="form-group w100p">
         <label for="dsDiagnosticoMorfologico" class="control-label">Notas Morfológicas<i data-action="ficha.openModalSelRefBibFicha" data-no-tabela="ficha" data-no-coluna="ds_notas_morfologicas" data-sq-registro="${ficha.id}" data-de-rotulo="Notas Morfológicas" class="fa fa-book ml10" title="Informar Referência Bibliográfica" style="color: green;" /></label>
         <br>
         <textarea name="dsDiagnosticoMorfologico" id="dsDiagnosticoMorfologico" class="form-control fld w100p fldTextarea wysiwyg" rows="10">${ficha.dsDiagnosticoMorfologico}</textarea>
      </div>
   </div>
   %{-- fim col-sm-12 - nota morfologica --}%
</div>
%{-- fim - nomes comuns, sinonimias e notas --}% %{-- inicio - botão salvar notas --}% %{-- fim - botão salvar notas --}%

<div class="panel-footer">
   <button id="btnSaveFrmTaxonomia" class="fld btn btn-success bt-sm" data-action="taxonomia.saveFrmTaxonomia">Criar Ficha</button>
</div>
