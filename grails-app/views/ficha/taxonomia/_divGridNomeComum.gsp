<!-- view: /views/ficha/taxonomia/_divGridNomeComum.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>Nome Comum</th>
            <th>Região/Língua</th>
            <th>Ref. Bibliográfica</th>
            <g:if test="${ ! session.sicae.user.isCO() }">
                <th>Ação</th>
            </g:if>
        </tr>
    </thead>
    <tbody>
        <g:if test="lista">
            <g:each var="item" in="${lista}">
                <tr>
                    <td>${item?.noComum }</td>
                    <td>${item?.deRegiaoLingua}</td>
                    <td>
                        <i data-action="ficha.openModalSelRefBibFicha" data-com-pessoal="true" data-no-tabela="ficha_nome_comum" data-no-coluna="no_comum" data-sq-registro="${item.id}"
                            data-de-rotulo="Nome Comum" data-update-grid="divGridNomeComum" class="fld fa fa-book tooltipstered"
                            title="Adicionar/Remover Refeferência bibliográfica."></i>&nbsp; ${raw(item.refBibHtml)}
                    </td>
                    <g:if test="${ ! session.sicae.user.isCO() }">
                        <td class="td-actions">
                            <a data-action="taxonomia.editNomeComum" data-id="${item.id}" class="fld btn btn-default btn-xs btn-update" title="Alterar">
                                <span class="glyphicon glyphicon-pencil"></span>
                            </a>
                            <a data-action="taxonomia.deleteNomeComum" data-nome-comum="${item?.noComum }" data-id="${item.id}" class="fld btn fld btn-default btn-xs btn-delete"
                                title="Excluir">
                                <span class="glyphicon glyphicon-remove"></span>
                            </a>
                        </td>
                    </g:if>
                </tr>
            </g:each>
        </g:if>
        <g:else>
            <tr>
                <td colspan="3">
                    Nenhum registro
                </td>
            </tr>
        </g:else>
    </tbody>
</table>
