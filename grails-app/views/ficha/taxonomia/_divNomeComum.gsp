<div class="div-area-verde">
    <form id="frmNomeComum" class="form-inline" style="display: none;" role="form">
        <div class="page-header group-header clearfix">
            %{--
            <button class="btn btn-default btn-xs pull-right" title="Exibir/Esconder formulário" style="margin:5px 5px 0 0" data-toggle="collapse" data-target=".nomeComumFields">
            <div class="nomeComumFields collapse in"><span class="glyphicon glyphicon-menu-down"></span></div>
            <div class="nomeComumFields collapse"><span class="glyphicon glyphicon-menu-up"></span></div>
            </button> --}%
            <h3>Nomes Comuns
            <a class="fld btn btn-default btn-xs" id="btnToggleNomeComum" title="Cadastrar" style="margin:0px 0px 0px 0px" data-toggle="collapse" data-target=".nomeComumFields">
            <div class="nomeComumFields collapse in"><i class="glyphicon glyphicon-plus" /></div>
            <div class="nomeComumFields collapse"><i class="glyphicon glyphicon-minus" /></div>
            </a>
            </h3>
        </div>
        <input name="sqFichaNomeComum" type="hidden" value="" />
        <fieldset class="nomeComumFields collapse">
            <div class="form-group">
                <label for="noComum" class="control-label" title="Quando houver vários nomes comuns da mesma região/lingua,<br>informe-os separados por vírgula!<br>Ex:nome1, nome2, nome3">Nome(s) Comum(ns) da espécie</label>
                <br>
                <input name="noComum" id="noComum" type="text" class="fld form-control fld400" placeholder="Ex: nome1, nome2..."/>
            </div>
            <div class="form-group">
                <label for="deRegiaoLingua" class="control-label">Região / Língua</label>
                <br>
                <input name="deRegiaoLingua" id="deRegiaoLingua" type="text" class="fld form-control fld250" />
            </div>
            <div class="panel-footer">
                <a id="btnSaveFrmNomeComum" class="fld btn btn-success btn-sm" data-action="taxonomia.saveFrmNomeComum" data-params="sqFicha,sqTaxon"
                title="Adicionar nome comum">Adicionar Nome Comum</a>
                <a id="btnResetFrmNomeComum" data-action="taxonomia.resetFrmNomeComum" class="btn btn-info hide">Limpar Campos</a>
            </div>
        </fieldset>
    </form>
    %{-- fim form TaxNomeComum --}% %{-- Gride Nomes Comuns --}%
    <div class="row">
        <div class="col-sm-12">
            <div id="divGridNomeComum" data-params="sqFicha,sqTaxon" data-grid-name="nomeComum" style="overflow-x: hidden; overflow-y: auto; max-height: 140px;"> %{--
            <g:render template="taxonomia/divGridNomeComum"></g:render> --}%
        </div>
        %{-- fim table grid nomes comuns --}%
    </div>
    %{-- fim com-sm-12 gride Nomes Comuns --}%
</div>
%{-- fim row gride Nomes Comuns --}%
</div>
%{-- fim div-area-verde --}%
