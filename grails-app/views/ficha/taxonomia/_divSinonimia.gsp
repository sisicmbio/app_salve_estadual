<div class="div-area-verde">

    <form id="frmSinonimia" class="form-inline" role="form" style="display: none;"> %{-- título do grupo --}%

<div class="page-header group-header clearfix">
    %{--
    <button class="btn btn-default btn-xs pull-right" title="Exibir/Esconder formulário" style="margin:5px 5px 0 0" data-toggle="collapse" data-target=".sinonimiaFields">
<div class="sinonimiaFields collapse in"><span class="glyphicon glyphicon-menu-down"></span></div>
<div class="sinonimiaFields collapse"><span class="glyphicon glyphicon-menu-up"></span></div>
</button> --}%
<h3>Nomes Antigos
    <button class="fld btn btn-default btn-xs" id="btnToggleSinonimia" title="Cadastrar" style="margin:0px 0px 0px 0px" data-toggle="collapse" data-target=".sinonimiaFields">
<div class="sinonimiaFields collapse in"><i class="glyphicon glyphicon-plus" /></div>
<div class="sinonimiaFields collapse"><i class="glyphicon glyphicon-minus" /></div>
</button>
</h3>
</div>

<fieldset class="sinonimiaFields collapse">
    <input name="sqFichaSinonimia" id="sqFichaSinonimia" type="hidden" value="" />
    <div class="form-group mr10">
        <label for="" class="control-label">Nome antigo</label>
        <br>
        <div class="input-group">
            <input name="noSinonimia" type="text" class="form-control fld fld250" id="noSinonimia" value="" />
            <g:if test="${ ! ficha.fichaCicloAnterior }">
                <span class="input-group-btn btn-group-sm">
                <button class="btn btn-default btn-xs" type="button" data-params="sqFicha:${ficha.id.toString()},sqCicloAvaliacao:${ficha.cicloAvaliacao.id.toString()}" data-action="ficha.doModalSelectFichaCicloAnterior">
                    <i class="fa fa-search cursor-pointer" title="Localizar o nome da espécie no ciclo anterior"></i>
                </button>
                </span>
            </g:if>
        </div>
    </div>


    <div class="form-group">
        <label for="" class="control-label">Autor e Ano</label>
        <br>
        <input name="noAutor" type="text" class="form-control fld fld300" id="noAutor" value="" />
    </div>

    %{--<div class="form-group">--}%
        %{--<label for="" class="control-label">Ano</label>--}%
        %{--<br>--}%
        %{--<input name="nuAno" type="text" class="form-control fld fld60" id="nuAno" value="" />--}%
    %{--</div>--}%
    <div class="panel-footer">
        <button id="btnSaveFrmSinonimia" class="btn fld btn-success btn-sm" data-action="taxonomia.saveFrmSinonimia" data-params="sqFicha,sqTaxon"
            title="Adicionar nome antigo">Adicionar nome antigo</button>
        <button id="btnResetFrmSinonimia" data-action="taxonomia.resetFrmSinonimia" class="btn btn-info hide">Limpar Campos</button>
    </div>
</fieldset>

</form>
%{-- fim form sinonimias --}% %{-- Gride sinonímias --}%

<div class="row">
    <div class="col-sm-12">
        <div id="divGridSinonimia" data-params="sqFicha,sqTaxon" data-grid-name="sinonimia" style="overflow-x: hidden; overflow-y: auto;max-height: 140px;" class="scrollable-area">
            %{--<g:render template="taxonomia/divGridSinonimia"></g:render> --}%
</div>
%{-- fim grid sinonimias --}%
</div>
%{-- fim col-sm-12 grid sinonimias --}%
</div>
%{-- fim row - gride sinonimias --}%
</div>
%{-- fim div-area-verde --}%
