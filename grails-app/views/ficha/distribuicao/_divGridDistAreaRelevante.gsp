<!-- view: /views/ficha/distribuicao/_divGridDistAreaRelevante.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th style="width: 120px;">Área Relevante</th>
            <th style="width: 120px;">Estado</th>
            <th style="width: 200px;">Município(s)</th>
            <th style="width: 200px;">Ref. Bibliográfica</th>
            <th style="width: 150px;">Local</th>
            <th style="width: auto;">Observação</th>
            <th style="width: 80px;">Ação</th>
        </tr>
    </thead>
    <tbody>
        <g:if test="${listAreas}">
            <g:each in="${listAreas}" var="item">
                <tr id="tr-sqFichaAreaRelevante-${item.id}">
                    <td>${item.tipoRelevancia.descricao}</td>
                    <td>${item.estado.noEstado}</td>
                    <td>
                        <a data-params="sqFichaAreaRelevancia:${item.id},sqEstado:${item.estado.id},noEstado:${item.estado.noEstado},txRelevancia:${item.tipoRelevancia.descricao}"
                           data-action="modal.cadMunicipioFicha"
                           data-callback="disAreaRelevante.updateGridDistAreaRelevante"
                           class="fld btn btn-default btn-xs btn-grid-subform"
                           title="Inclluir/Remover Município"><span class="glyphicon glyphicon-pushpin"></span></a> &nbsp;${item.municipiosHtml}
                        </td>
                        <td>
                            <a data-action="ficha.openModalSelRefBibFicha" class="fld"
                               data-com-pessoal="true"
                               data-no-tabela="ficha_area_relevancia"
                               data-no-coluna="sq_ficha_area_relevancia"
                               data-sq-registro="${item.id}" data-de-rotulo="Area Relevante - ${item?.tipoRelevancia?.descricao} "
                               data-update-grid="divGridDistAreaRelevante" class="btn btn-default btn-xs btn-grid-subform"
                               title="Adicionar/Remover Refeferência bibliográfica."><span class="fa fa-book"></span>
                            </a>
                            &nbsp; ${raw(item.refBibHtml)}
                        </td>
                        <td>${item.localHtml}</td>
                        <td>${raw( br.gov.icmbio.Util.stripTags(item.txRelevancia))}</td>
                        <td class="td-actions">
                            <a data-action="disAreaRelevante.editDisAreaRelevante" data-id="${item.id}" class="fld btn btn-default btn-xs btn-update" title="Alterar">
                                <span class="glyphicon glyphicon-pencil"></span>
                            </a>
                            <a class="fld btn btn-default btn-xs btn-delete" data-action="disAreaRelevante.deleteDisAreaRelevante" data-params="sqFichaAreaRelevancia:${item?.id}" data-descricao="${item.tipoRelevancia.descricao}" title="Excluir">
                                <span class="glyphicon glyphicon-remove"></span>
                            </a>
                        </td>
                    </tr>
                </g:each>
            </g:if>
            <g:else>
                <tr>
                    <td align="center" colspan="6">Nenhum Registro Cadastrado</td>
                </tr>
            </g:else>
        </tbody>
    </table>
