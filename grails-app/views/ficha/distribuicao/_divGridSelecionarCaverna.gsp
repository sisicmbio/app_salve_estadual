<!-- view: /views/ficha/distribuicao/_divGridSelecionarCaverna.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<table class="table table-striped table-condensed table-bordered" id="tableSelecionarCaverna">
    <thead>
    <tr>
        <th>#</th>
        <th>&nbsp;</th>
        <th>Nome</th>
        <th>Localidade</th>
        <th>Estado</th>
        <th>Municipio</th>
    </tr>
    </thead>
    <tbody>
    <g:if test="${listCavernas}">
        <g:each var="item" in="${listCavernas}" status="i">
        <tr id="tr-${item?.sq_caverna}">
            <td class="text-center">${(i+1)}</td>
            <td class="text-center">
                <input name="chkSqCaverna"
                       data-sq-caverna="${item?.sq_caverna}"
                       value="${item?.sq_caverna}" type="checkbox" class="checkbox-lg"/>
            </td>
            <td class="text-left">${item?.no_caverna}</td>
            <td class="text-left">${item?.no_localidade}</td>
            <td class="text-left">${item?.no_estado}</td>
            <td class="text-left">${item?.no_municipio}</td>
        </tr>

        </g:each>
    </g:if>
    <g:else>
        <tr>
            <td colspan="6">Nenhuma caverna encontrada</td>
        </tr>
    </g:else>
    </tbody>
</table>
