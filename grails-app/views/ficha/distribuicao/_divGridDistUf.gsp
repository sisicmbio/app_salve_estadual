<!-- view: /views/ficha/distribuicao/_divGridDistUf.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<g:if test="${ listUfsFicha }">
    <div style="height:auto;max-height: 400px;overflow: auto">
        <table class="table table-striped table-bordered table-hover" id="disTableFichaUfsFicha">
        <thead>
        <tr>
            <th style="width:200px;">Estado</th>
            <th style="width:auto;">Referência(s) Bibliográfica(s)</th>
        </tr>
        </thead>
        <tbody>
        <g:if test="${listUfsFicha}">
            <g:each in="${listUfsFicha}" var="item">
                <tr>
                    <td>${ item.no_estado }
                        <g:if test="${!isSubespecie}">
                            ${ raw(item.co_nivel_taxonomico == 'SUBESPECIE' ? '<i title="Estado da subespécie" class="ml10 tooltipster glyphicon glyphicon-info-sign"></i>' : '') }
                        </g:if>
                    </td>
                    <td>
                        <i data-action="ficha.openModalSelRefBibFicha"
                           data-no-tabela="ficha_uf"
                           data-no-coluna="sq_ficha_uf"
                           data-sq-ficha="${params.sqFicha}"
                           data-sq-registro="${item.sq_ficha_uf}"
                           data-de-rotulo="Estado - ${item.no_estado} "
                           data-com-pessoal="true"
                           data-update-grid="divGridDistUf"
                           title="Adicionar/Remover Refeferência bibliográfica." class="fld fa fa-book tooltipstered"></i>
                           ${raw( br.gov.icmbio.Util.parseRefBib2Grid( item?.json_ref_bibs ) ) }
                    </td>
                </tr>
            </g:each>
        </g:if>
        <g:else>
            <tr>
                <td class="text-center" colspan="3">Nenhum Estado cadastrado</td>
            </tr>
        </g:else>
        </tbody>
    </table>
    </div>
    <br>
</g:if>

<g:if test="${listUfsRegistros}">
    <div style="height:auto;max-height: 400px;overflow: auto">
        <table class="table table-striped table-bordered table-hover" id="disTableFichaUfsOcorrencia">
            <thead>
            <tr>
                <th style="width:200px;">Estado (ocorrência)</th>
                <th style="width:auto;">Referência(s) Bibliográfica(s)</th>
        </tr>
        </thead>
        <tbody>
        <g:if test="${listUfsRegistros}">
            <g:each in="${listUfsRegistros}" var="item">
                <tr>
                    <td>${item?.no_estado}
                    <g:if test="${ ! isSubespecie }">
                        ${ raw( item.co_nivel_taxonomico == 'SUBESPECIE' ? '<i title="Estado da ocorrência da subespécie" class="ml10 tooltipster glyphicon glyphicon-info-sign"></i>' : '')}
                    </g:if>
                    </td>
                    <td>
                        ${raw( br.gov.icmbio.Util.parseRefBib2Grid( item.json_ref_bibs ) ) }
                    </td>
                </tr>
            </g:each>
        </g:if>
        <g:else>
            <tr>
                <td class="text-center" colspan="3">Nenhum Estado nos registros</td>
            </tr>
        </g:else>
        </tbody>
        </table>
    </div>
</g:if>
<g:if test="${ !listUfsFicha && !listUfsRegistros}">
    <div class="alert alert-info">
        <h4>Nenhum Estado ou registro de ocorrência cadastrado.</h4>
    </div>
</g:if>
