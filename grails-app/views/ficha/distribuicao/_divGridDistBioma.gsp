<!-- view: /views/ficha/distribuicao/_divGridDistBioma.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<g:if test="${ listBiomasFicha }">
    <div style="height:auto;max-height: 400px;overflow: auto">
    <table class="table table-striped table-bordered table-hover" id="disTableFichaBiomasFicha">
       <thead>
          <tr>
             <th style="width:200px;">Bioma</th>
             <th style="width:auto;">Referência(s) Bibliográfica(s)</th>
          </tr>
       </thead>
       <tbody>
          <g:if test="${listBiomasFicha}">
             <g:each in="${listBiomasFicha}" var="item">
                <tr>
                   <td>${item?.no_bioma}
                        <g:if test="${!isSubespecie}">
                            ${ raw(item.co_nivel_taxonomico == 'SUBESPECIE' ? '<i title="Bioma da subespécie"  class="ml10 tooltipster glyphicon glyphicon-info-sign"></i>' : '') }
                        </g:if>
                   </td>
                   <td>
                   <i data-action="ficha.openModalSelRefBibFicha"
                      data-no-tabela="ficha_bioma"
                      data-no-coluna="sq_ficha_bioma"
                      data-sq-registro="${item.sq_ficha_bioma}"
                      data-sq-ficha="${params.sqFicha}"
                      data-de-rotulo="Bioma - ${item?.no_bioma} "
                      data-com-pessoal="true"
                      data-update-grid="divGridDistBioma"
                      class="fld fa fa-book tooltipstered"
                      title="Adicionar/Remover Refeferência bibliográfica."></i>
                       ${raw( br.gov.icmbio.Util.parseRefBib2Grid( item?.json_ref_bibs ) ) }
                   </td>
                </tr>
            </g:each>
          </g:if>
          <g:else>
             <tr>
                <td class="text-center" colspan="3">nenhum bioma cadastrado</td>
             </tr>
          </g:else>
       </tbody>
    </table>
    </div>
    <br>
</g:if>



<g:if test="${listBiomasRegistros}">
    <div style="height:auto;max-height: 400px;overflow: auto" >
        <table class="table table-striped table-bordered table-hover" id="disTableFichaBiomasOcorrencia">
            <thead>
            <tr>
                <th style="width:200px;">Bioma (ocorrência)</th>
                <th style="width:auto;">Referência(s) Bibliográfica(s)</th>
            </tr>
            </thead>
            <tbody>
            <g:if test="${listBiomasRegistros}">
                <g:each in="${listBiomasRegistros}" var="item">
                    <tr>
                        <td>${item?.no_bioma}
                            <g:if test="${!isSubespecie}">
                                ${ raw( item.co_nivel_taxonomico == 'SUBESPECIE' ? '<i title="Bioma da ocorrência da subespécie" class="ml10 tooltipster glyphicon glyphicon-info-sign"></i>' : '')}
                            </g:if>
                        </td>
                        <td>
                            ${raw( br.gov.icmbio.Util.parseRefBib2Grid( item.json_ref_bibs ) ) }
                        </td>
                    </tr>
                </g:each>
            </g:if>
            <g:else>
                <tr>
                    <td class="text-center" colspan="3">nenhum bioma nos registros</td>
                </tr>
            </g:else>
            </tbody>
        </table>
    </div>
</g:if>

<g:if test="${ !listBiomasFicha && !listBiomasRegistros}">
    <div class="alert alert-info">
        <h4>Nenhum Bioma ou registro de ocorrência cadastrado.</h4>
    </div>
</g:if>
