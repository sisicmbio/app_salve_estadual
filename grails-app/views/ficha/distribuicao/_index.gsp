<!-- view: /views/ficha/distribuicao/_index/ -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<div class="row">
    <div class="col-sm-5" id="divCamposDist">
        <%-- inicio frmDistribuicao --%>
        <form id="frmDistribuicao" class="form-inline" role="form">
            <%--inicio form-group endemica --%>
            <input type="hidden" id="nmCientificoMapa" value="${ficha.taxon.noCientifico}">
            <div class="fld form-group">
                <label class="control-label">Endêmica do Brasil</label>
                <br>
                <select name="stEndemicaBrasil" id="stEndemicaBrasil" data-change="distribuicao.stEndemicaBrasilChange" class="fld fldSelect form-control fld200">
                    <option value="">-- Selecione --</option>
                    <option value="S" ${ficha.stEndemicaBrasil == 'S' ? 'selected' : ''}>Sim</option>
                    <option value="N" ${ficha.stEndemicaBrasil == 'N' ? 'selected' : ''}>Não</option>
                    <option value="D" ${ficha.stEndemicaBrasil == 'D' ? 'selected' : ''}>Desconhecido</option>
                </select>
            </div>
            <br/>
            <%-- fim form-group en$("#grpAltitude").removeClass('hidden');
                    $("#nuMinAltitude,#nuMaxAltitude,#nuMinBatimetria,#nuMaxBatimetria").parent().parent().removeClass('hidden');demica --%>
            <%--inicio form-group campos distribução --%>
            <div class="form-group w100p">
                <label for="" class="control-label">Distribuição Geográfica Global
                    <i data-action="ficha.openModalSelRefBibFicha"
                       data-no-tabela="ficha"
                       data-no-coluna="ds_distribuicao_geo_global"
                       data-sq-registro="${ficha.id}"
                       data-de-rotulo="Distribuição Geográfica Global" class="fa fa-book ml10" title="Informar Referência Bibliográfica" style="color: green;"/>
                </label>
                <br>
                <textarea name="dsDistribuicaoGeoGlobal" rows="7" class="fld fldTextarea wysiwyg w100p" style="">${ficha?.dsDistribuicaoGeoGlobal}</textarea>
            </div>
            <br/>

            <div class="form-group w100p ${ficha.stEndemicaBrasil == 'S' ? 'hidden' : ''}" id="divDistGeoNacional">
                <label for="" class="control-label">Distribuição Geográfica Nacional
                    <i data-action="ficha.openModalSelRefBibFicha"
                       data-no-tabela="ficha"
                       data-no-coluna="ds_distribuicao_geo_nacional"
                       data-sq-registro="${ficha.id}"
                       data-de-rotulo="Distribuição Geográfica Nacional" class="fa fa-book ml10" title="Informar Referência Bibliográfica" style="color: green;"/>
                </label>
                <br>
                <textarea name="dsDistribuicaoGeoNacional" id="dsDistribuicaoGeoNacional" rows="7" class="fld fldTextarea wysiwyg w100p" style="">${ficha?.dsDistribuicaoGeoNacional}</textarea>
            </div>

            %{-- para apenas alguns grupos --}%
            <fieldset id="grpAltitude" class="mt10">
                <legend>Altitude e Profundidade</legend>

                <div class="form-group">
                    <label for="" class="control-label">Altitude Min</label>
                    <br>
                    <div class="input-group">
                        <input name="nuMinAltitude" id="nuMinAltitude" type="text" class="fld form-control fld100 number" value="${ficha.nuMinAltitude}"/>
                        <span class="input-group-addon" id="sizing-addon2">m</span>
                    </div>
                </div>

                <div class="form-group" style="margin-right:30px;">
                    <label for="" class="control-label">Altitude Max</label>
                    <br>

                    <div class="input-group">
                        <input name="nuMaxAltitude" id="nuMaxAltitude" type="text" class="fld form-control fld100 number" value="${ficha.nuMaxAltitude}"/>
                        <span class="input-group-addon" id="sizing-addon2">m</span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="" class="control-label">Profundidade Min</label>
                    <br>

                    <div class="input-group">
                        <input name="nuMinBatimetria" id="nuMinBatimetria" type="text" class="fld form-control fld100 number" value="${ficha.nuMinBatimetria}"/>
                        <span class="input-group-addon" id="sizing-addon2">m</span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="" class="control-label">Profundidade Max</label>
                    <br>

                    <div class="input-group">
                        <input name="nuMaxBatimetria" id="nuMaxBatimetria" type="text" class="fld form-control fld100 number" value="${ficha.nuMaxBatimetria}"/>
                        <span class="input-group-addon" id="sizing-addon2">m</span>
                    </div>
                </div>
            </fieldset>

            <%--fim form-group campos distribução --%>
            <div class="panel-footer">
                <button class="fld btn btn-success" data-action="distribuicao.saveFrmDistribuicao" data-params="sqFicha" title="Gravar Distribuição">Gravar Distribuição</button>
            </div>
        </form>
    </div>

    <div class="col-sm-7" id="divMapaDistribuicaoMain">
%{--        <button class="btn btn-primary" data-action="distribuicao.saveShpBaciasHidrograficas">Teste Gravação Shp</button>--}%

        %{-- <div id="mapaDistribuicaoMain" class="map" data-map-on-load="distribuicao.initMap" data-map-config='{"zoom":"4","center":[-47.925694, -15.804474]}'></div> --}%
        %{--<div class="point-popup" id="pointPopup"></div>--}%

        %{-- <div id="layers" class="ol-control-box">
            <div class="text-center;" style="border-bottom:1px solid #fff;margin-bottom:5px;">
                <span>Camadas</span>
            </div>
            <ul>
                <li><input type="radio" name="disLayer" value="osm" checked>Open Street Map</li>
                <li><input type="radio" name="disLayer" value="bingmaps">Bing</li>
                <li><input type="radio" name="disLayer" value="esri">ESRI</li>
                {-- <li><input type="radio" name="disLayer" value="stamen">Stamen</li> --}
                <li><input type="checkbox" checked="true" value="avaliacao">Avaliação</li>
                <li><input type="checkbox" checked="true" value="portalbio">Portalbio</li>
            </ul>
            <br/>
            <a class="btn btn-default btn-xs btn-block" title="Salvar imagem do mapa atual no formato png." data-action="distribuicao.exportarMapa">Exportar Mapa</a>
        </div>
        --}%
        <div id="mapaDistribuicaoMain" class="map" style="border:1px solid gray;height:880px;background-color: #f0f8ff">
                <p style="margin-top:10px;text-align: center">Carregando mapa...</p>
        </div>
        <div id="mapaDistribuicaoMainLegend" class="hidden">
            <fieldset>
                <legend>Legenda</legend>
                <div style="font-size:12px;" id="conteudo"></div>
            </fieldset>
        </div>

    </div>
</div>

<%--inicio gride ocorrencias--%>
<div class="row" id="rowGrideOcorrencia">
    <div class="col-sm-12">
        <fieldset>
            <legend>
                Registros
            </legend>

            <g:if test="${ ! session.sicae.user.isCO() }">
                <div class="alert alert-warning alert-dismissible bgOrange hidden" role="alert" id="div-alert-contextmenu">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Dica!</strong> Clique com o botão direito sobre a linha selecionada para exibir o menu de opções.
                </div>
            </g:if>
            <button id="btnExportCsvOcorrencia" data-action="distribuicao.exportarGrideOcorrenciaCsv"
                    class="btn btn-info mb5 pull-right"
                    data-params="sqCicloAvaliacao,sqFicha,contexto:abaDistribuicao">Exportar (planilha)&nbsp;<i style="font-size:19px" class="fa fa-spin"/></button>
        </fieldset>
        %{-- gride  --}%
%{--        <div id="divGridOcorrencia" data-grid-name="ocorrencia" data-params="grid:true" style="min-height:600px;max-height:600px;overflow-y:auto;border-bottom:2px solid gray;"></div>--}%
        <div id="containerGridOcorrencia" class="grid-container"
             data-params="sqFicha,sqCicloAvaliacao"
             data-container-filters-id=""
             data-height="600px"
             data-field-id="rowId"
             data-url="fichaDisOcorrencia/getGridOcorrencia">
            <g:render template="/ficha/distribuicao/divGridOcorrencia"
                      model="[gridId: 'GridOcorrencia']"></g:render>
        </div>

    </div>
</div>
<%--fim gride ocorrencias--%>


<div class="row">
    <div class="col-sm-12" style="margin-top:35px;margin-bottom:35px;">
        %{-- imagens de mapas --}%
        <fieldset>
            <legend>
                Imagens de mapas e shapefiles
                <a data-action="ficha.openModalSelAnexo"
                   data-contexto="MAPA_DISTRIBUICAO"
                   data-de-rotulo="Mapas de Distribuição / Shapefiles"
                   data-show-principal="true"
                   data-update-grid="divGridDistAnexo"
                   class="fld btn btn-default btn-xs btn-grid-subform"
                   title="Adicionar Imagem de Mapa de Distribuição"><span class="glyphicon glyphicon-plus"></span>
                </a>
            </legend>
        </fieldset>
        %{-- gride  --}%
        <div id="divGridDistAnexo" data-contexto="MAPA_DISTRIBUICAO" style="border-bottom:2px solid gray;padding-bottom:5px;"></div>
        <%-- fim form frmDistribucao --%>
    </div>
</div>

<%--incio abas distribuicao --%>
<div class="row">
    <div class="col-sm-12">
        <ul class="nav nav-tabs nav-sub-tabs mt10" id="ulTabsDistribuicao">
            <li id="liTabDisUF"><a data-action="ficha.tabClick" data-url="fichaDistribuicao/tabUf" data-container="tabDisUf" data-on-load="distribuicao.tabDisUfInit" data-reload="true" data-params="sqFicha" data-toggle="tab" href="#tabDisUf">2.1 - UF</a></li>
            <li id="liTabDisBioma"><a data-action="ficha.tabClick" data-url="fichaDistribuicao/tabBioma" data-container="tabDisBioma" data-on-load="distribuicao.tabDisBiomaInit" data-reload="true" data-params="sqFicha" data-toggle="tab" href="#tabDisBioma">2.2 - Bioma</a></li>
            <li id="liTabDisBaciaHidrografica"><a data-action="ficha.tabClick" data-url="fichaDistribuicao/tabBaciaHidrografica" data-container="tabDisBaciaHidrografica" data-on-load="distribuicao.tabDisBaciaHidrograficaInit" data-reload="true" data-params="sqFicha" data-toggle="tab"
                                                  href="#tabDisBaciaHidrografica">2.3 - Bacia Hidrográfica</a></li>
            <li id="liTabDisAmbienteRestrito"><a data-action="ficha.tabClick" data-url="fichaDistribuicao/tabAmbienteRestrito" data-container="tabDisAmbienteRestrito" data-on-load="distribuicao.tabDisAmbienteRestritoInit" data-reload="false" data-params="sqFicha" data-toggle="tab"
                                                 href="#tabDisAmbienteRestrito">2.4 - Ambiente Restrito</a></li>
            <li id="liTabDisAreaRelevante"><a data-action="ficha.tabClick" data-url="fichaDistribuicao/tabAreaRelevante" data-container="tabDisAreaRelevante" data-on-load="distribuicao.tabDisAreaRelevanteInit" data-reload="false" data-params="sqFicha" data-toggle="tab" href="#tabDisAreaRelevante">2.5 - Áreas Relevantes</a></li>

            <g:if test="${ ! session.sicae.user.isCO() }">
                <li id="liTabDisOcorrencia"><a data-action="ficha.tabClick" data-url="fichaDistribuicao/tabOcorrencia" data-container="tabDisOcorrencia" data-on-load="distribuicao.tabDisOcorrenciaInit" data-reload="false" data-params="sqFicha" data-toggle="tab" href="#tabDisOcorrencia">2.6 - Ocorrência</a></li>
            </g:if>

            <li id="liTabDisEooAoo"><a data-action="ficha.tabClick" data-url="fichaDistribuicao/tabEooAoo" data-container="tabDisEooAoo" data-on-load="distribuicao.tabDisEooAooInit" data-reload="true" data-params="sqFicha" data-toggle="tab" href="#tabDisEooAoo">2.7 - EOO / AOO</a></li>

            %{-- movida para aba 7.5         <li id="liTabSituacaoRegional"><a data-action="ficha.tabClick" data-url="fichaDistribuicao/tabSituacaoRegional" data-container="tabDisSituacaoRegional" data-reload="false" data-params="sqFicha" data-toggle="tab" href="#tabDisSituacaoRegional">2.7 - Situação Regional</a></li>--}%
        </ul>

        <div id="disTabContent" class="tab-content">
            <div class="tab-content-tips alert alert-success mt10"><b>Dica!</b>&nbsp;Clique nas abas acima para carregar o seu conteúdo!</div>
            %{-- inicio aba distribuicao UF --}%
            <div role="tabpanel" class="tab-pane tab-pane-ficha active" style="min-height:400px;" id="tabDisUf">
                %{-- <g:render template="/ficha/distribuicao/formUF"></g:render> --}%
            </div>
            %{-- início aba bioma --}%
            <div role="tabpanel" class="tab-pane tab-pane-ficha" style="min-height:400px;" id="tabDisBioma">
                %{-- <g:render template="distribuicao/formBioma"></g:render> --}%
            </div>
            <%-- inicio aba bacia Hidrográfica --%>
            <div role="tabpanel" class="tab-pane tab-pane-ficha" style="min-height:400px;" id="tabDisBaciaHidrografica">
                %{-- <g:render template="distribuicao/formBaciaHidrografica"></g:render> --}%
            </div>
            <%-- início aba distribuicao ambiente restrito --%>
            <div role="tabpanel" class="tab-pane tab-pane-ficha" style="min-height:400px;" id="tabDisAmbienteRestrito">
                %{-- <g:render template="distribuicao/formAmbienteRestrito"></g:render> --}%
            </div>
            <%-- início tab area relevancia --%>
            <div role="tabpanel" class="tab-pane tab-pane-ficha" style="min-height:400px;" id="tabDisAreaRelevante">
                %{-- <g:render template="distribuicao/formAreaRelevante"></g:render> --}%
            </div>
            <%-- início tab Ocorrência --%>
            <div role="tabpanel" class="tab-pane tab-pane-ficha" style="min-height:400px;" id="tabDisOcorrencia">
                %{--<g:render template="distribuicao/ocorrencia/index"></g:render> --}%
            </div>
            <%-- início tab EOO / AOO --%>
            <div role="tabpanel" class="tab-pane tab-pane-ficha" style="min-height:400px;" id="tabDisEooAoo">
                %{--<g:render template="distribuicao/formEooAoo/index"></g:render> --}%
            </div>
            %{--situacao regional - passou para a aba 7.5
                <div role="tabpanel" class="tab-pane tab-pane-ficha" style="min-height:400px;padding-top:20px;" id="tabDisSituacaoRegional">
            --}%
        </div>
    </div>
</div>
<%--fim col-sm-12 --%>
<%--fim abas distribuicao --%>
