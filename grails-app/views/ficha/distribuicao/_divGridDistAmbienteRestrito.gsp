<!-- view: /views/ficha/distribuicao/_divGridDistAmbienteRestrito.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<table class="table table-striped table-bordered table-hover">
   <thead>
      <tr>
         <th>Ambiente Restrito</th>
         <th style="width:200px;">Ref. Bibliográfica</th>
         <th>Observações</th>
         <th>Ação</th>
      </tr>
   </thead>
   <tbody>
      <g:if test="${listAmbientes}">
         <g:each in="${listAmbientes}" var="item">
         <tr>
            <td>
                ${raw(item.treeUp().split('\\|').join('<br>'))}
                <g:if test="${ item.restricaoHabitat.codigoSistema =='CAVERNAS'}">
                    <a data-action="disAmbienteRestrito.openModalSelCaverna"
                       data-sq-ficha="${params.sqFicha}"
                       data-sq-ficha-ambiente-restrito="${item.id}"
                       class="fld btn btn-default btn-xs btn-grid-subform"
                       title="Adicionar caverna"><span class="blue fa fa-plus"></span></a>
                   <g:if test="${item.cavernas}">
                       <div style="max-height: 150px;overflow-y: auto;border: none;">
                       <ul>
                       <g:each var="item2" in="${item.cavernas}">
                           <li id="li-caverna-${item2.id}" title="${item2.caverna?.noLocalidade}" class="cursor-pointer">${item2.caverna?.noCaverna +' / ' + item2.caverna?.noEstado}
                               <i class="ml5 fld fa fa-times red" title="Excluir caverna"
                                  data-action="disAmbienteRestrito.excluirCaverna"
                                  data-descricao="${item2.caverna?.noCaverna}"
                                  data-sq-ficha-amb-restrito-caverna="${item2.id}">
                               </i>
                           </li>
                       </g:each>
                       </ul>
                       </div>
                   </g:if>
                </g:if>
                %{--${ (item?.restricaoHabitat?.pai && item?.restricaoHabitat?.pai?.pai?.descricao ? item.restricaoHabitat.pai.pai.descricao + ' - ' : '') +
                   (item?.restricaoHabitat?.pai && item?.restricaoHabitat?.pai?.descricao ? item.restricaoHabitat.pai.descricao + ' - ':'') +
                   (item?.restricaoHabitat?.descricao ? item.restricaoHabitat.descricao : '' ) }--}%
            </td>
            <td>
               <a data-action="ficha.openModalSelRefBibFicha"
                  data-no-tabela="ficha_ambiente_restrito"
                  data-no-coluna="sq_ficha_ambiente_restrito"
                  data-sq-registro="${item.id}"
                  data-de-rotulo="Restrição Habitat - ${item?.restricaoHabitat?.descricao}"
                  data-com-pessoal="true"
                  data-update-grid="distAmbienteRestrito" class="fld btn btn-default btn-xs btn-grid-subform" title="Adicionar/Remover Refeferência bibliográfica."><span class="fa fa-book"></span></a>
                  &nbsp;
                  ${raw(item.refBibHtml)}
            </td>
            <td>${raw(item?.txFichaRestricaoHabitat)}</td>
            <td class="td-actions">
               <a data-action="disAmbienteRestrito.editDisAmbienteRestrito" data-id="${item.id}" class="fld btn btn-default btn-xs btn-update" title="Alterar">
                  <span class="glyphicon glyphicon-pencil"></span>
               </a>
               <a data-action="disAmbienteRestrito.deleteDisAmbienteRestrito" data-params="sqFichaAmbienteRestrito:${item.id},descricao:${item?.restricaoHabitat?.descricao}" class="fld btn btn-default btn-xs btn-delete" title="Excluir">
                     <span class="glyphicon glyphicon-remove"></span>
               </a>
           </td>
         </tr>
        </g:each>
      </g:if>
      <g:else>
         <tr>
            <td align="center" colspan="4">Nenhum Registro Cadastrado</td>
         </tr>
      </g:else>
   </tbody>
</table>


%{--
      <tr>
   <td>Goiás</td>
   <td>
      <a data-id="" data-action="ficha.openModalSelRefBib" class="btn btn-default btn-xs btn-grid-subform" title="Adicionar/Remover Refeferência bibliográfica.">
         <span class="fa fa-book"></span>
      </a>
      ref. bib 1 xpto / ref. bib 5 xpto
   </td>
         <td class="td-actions">
            <a data-id="" class="btn btn-default btn-xs btn-delete" title="Excluir"><span class="glyphicon glyphicon-remove"></span></a>
        </td>
      </tr>
--}%
