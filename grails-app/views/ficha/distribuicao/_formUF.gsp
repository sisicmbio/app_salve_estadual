
<%-- inicio formulario distribuicao uf --%>
<form id="frmDisUf" class="form-inline" role="form">
   <input name="id" id="id" type="hidden" value="" />
   <%-- início campo estado--%>

<div class="fld form-group">
    <label for="" class="control-label">Estado</label>
    <a data-action="disUf.openModalSelUf"
         class="fld btn btn-default btn-xs btn-grid-subform"
          data-params="sqFicha,modalName:selecionarUf,contexto:Estados"
         title="Selecionar/Excluir Estado">
          <span class="glyphicon glyphicon-plus"></span>
    </a>
</div>


%{--
alterado para seleção via treeview
<div class="fld form-group w100p">
      <label for="" class="control-label">Estado</label>
      <br>
      <select name="sqEstado" id="sqEstado" multiple="multiple" style="width:95%"
             class="fld form-control select2"
             required="true"
             data-s2-auto-height="true"
             data-s2-multiple="true"
             data-s2-minimum-input-length="0"
             data-s2-maximum-selection-length="28"
             data-msg-required="Campo Obrigatório">
          <g:each in="${estados}" var="estado">
            <option value="${estado.id}">${estado.noEstado}</option>
         </g:each>
      </select>
      <button data-action="disUf.selAllUf" title="Adicionar todos os Estados de uma só vez!" data-params="sqFicha" class="btn btn-default btn-sm">Todos</button>
   </div>
 --}%

  <%-- fim campo estado --%>

   <%-- inicio campo ref. bibliográfica
   <div class="form-group">
      <label for="" class="control-label">Referência Bibliográfica</label>
      <br>
      <select class="form-control fld400">
         <option value="">-- selecione --</option>
      </select>
      <a class="btn btn-default btn-sm" data-action="ficha.openModalSelRefBib" data-ref="dist_uf" title="Cadastrar Ref. Bibliográfica">
         <i class="glyphicon glyphicon-plus"></i>
      </a>
   </div>
   <%-- fim campo ref. bibliográfica --%>
%{--
   <br>
não precisa mais com a seleção via treview
   <div class="panel-footer">
      <button data-action="disUf.saveFrmDisUf" data-params="sqFicha" class="fld btn btn-success">Adicionar Estado</a>
   </div>
 --}%   <br>
   <%-- início divGridDistUf        --%>

   <div id="divGridDistUf" style="overflow-x: hidden; overflow-y: auto; min-height: 200px;" class="scrollable-area">
    <br>
    <br>
    <span><b>carregando...</b></span>
      %{-- <g:render template="/ficha/distribuicao/divGridDistUf"></g:render> --}%
   </div>
%{--    <div class="end-page"--}%
   <%-- fim divGridDistUf--%>
</form>
<%-- fim formulário distribuicao UF--%>
