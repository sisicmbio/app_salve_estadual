<%-- inicio formulario distribuicao Area Relevante --%>

<a id="btnShowHidefrmDisAreaRelevante"
   data-toggle="collapse"
   data-target="#frmDisAreaRelevante"
   onClick="toggleImage(this)"
   title="Clique para abrir ou fechar o formulário."
   class="fld mt10 btn btn-xs btn-default collapsed">Cadastrar área relevante&nbsp;<i class="fa fa-chevron-down"></i></a>

<form id="frmDisAreaRelevante" role="form" class="form-inline collapse">
    <input type="hidden" name="sqFichaAreaRelevante" value="" />
    <%-- início campo Area Relevante --%>
    <div class="fld form-group">
        <label for="" class="control-label">Área Relevante</label>
        <br />
        <select class="fld form-control fld300 fldSelect" required="true" name="sqTipoRelevancia">
            <option value="">-- Selecione --</option>
            <g:each in="${arRels}" var="arRel">
                <option value="${arRel?.id}">${arRel?.descricao}</option>
            </g:each>
        </select>
    </div>
    <%-- fim campo select Area Relevante  --%>
    <%-- início campo Estado --%>
    <div class="fld form-group">
        <label for="" class="control-label">Estado</label>
        <br />
        <select class="fld form-control fld200 fldSelect" required="true" name="sqEstado">
            <option value="">-- selecione --</option>
            <g:each in="${estados}" var="estado">
                <option value="${estado?.id}">${estado?.noEstado}</option>
            </g:each>
        </select>
    </div>
    <%-- fim campo select Area Relevante  --%>

    <%-- inicio campo select ucs --%>
    <div class="fld form-group">
        <label class="control-label">Unidade de Conservação</label>
        <br />
        <select name="sqControle" id="sqControle" data-change="disAreaRelevante.sqUcFederalChange" class="fld form-control fld500 select2"
                data-s2-url="ficha/select2Ucs"
                data-s2-placeholder="?"
                data-s2-minimum-input-length="1"
                data-s2-auto-height="true">
            <option value="">?</option>
        </select>
    </div>

    <%-- fim campo select ucs --%>
    <br/>



    <%-- inicio campo Local --%>
    <div class="fld form-group hidden">
        <label for="" class="control-label">Local</label>
        <br />
        <input  id="txLocal" name="txLocal" type="text" class="fld form-control fld400" required="true" />
    </div>
    <%-- fim campo Local --%>
    <br class="fld" />
    <%-- inicio campo Observacao --%>
    <div class="form-group w100p">
        <label for="" class="control-label">Observação</label>
        <br />
        <textarea class="fld form-control fldTextarea wysiwyg w100p" rows="7" name="txRelevancia"></textarea>
    </div>
    <br />
    <%-- fim campo Observacao --%>
    <div class="fld panel-footer">
        <button id="btnSaveFrmDisAreaRelevante" data-action="disAreaRelevante.saveFrmDisAreaRelevante" data-params="sqFicha" class="fld btn btn-success">Adicionar Área Relevante</button>
        <button id="btnResetFrmDisAreaRelevante" data-action="disAreaRelevante.resetFrmDisAreaRelevante" class="btn btn-info hide">Limpar</button>
    </div>
<%-- fim divGridDistAreaRelevante --%>
</form>
<%-- fim formulário distribuicao Area Relevante --%>

<%-- início divGridDistAreaRelevante --%>
<div id="divGridDistAreaRelevante" style="overflow-x: hidden; overflow-y: auto; min-height: 140px;" class="scrollable-area mt10" data-grid-name="distAreaRelevante">
%{--<g:render template="/ficha/distribuicao/divGridDistAreaRelevante"></g:render> --}%
</div>
