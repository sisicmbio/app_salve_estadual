<!-- view: /views/ficha/distribuica/_divGridAnexos.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<table class="table table-striped table-bordered table-hover">
  <thead>
    <tr>
      <th>Imagem</th>
      <th>Data</th>
      <th title="Imagem principal é que será utilizada na impressão da ficha.">Imagem<br>principal?</th>
      <th title="Shape file da Distribuição Geográfica Atual">Shape da distribuição<br>geográfica atual?</th>
      <th>Legenda / Observação</th>
      <th>Arquivo</th>
      <th>Ação</th>
      %{-- <th></th> --}%
    </tr>
  </thead>
  <tbody>
    <g:each var="item" in="${listFichaAnexo}">
      <tr>
          <td class="text-center" style="width:90px;">
            <g:if test="${item.deTipoConteudo=='image/tiff'}">
                &nbsp;
            </g:if>
            <g:elseif test="${item.noArquivo.indexOf('.zip') > -1 }">
             <img src="${appUrl}assets/shapefile32x37.png" data-id="${item.id}" data-file-name="${item.noArquivo}" alt="${item.noArquivo}"
                  class="img-rounded img-preview-grid"/>
            </g:elseif>
            <g:else>
                <img src="${appUrl}ficha/getAnexo/${item.id}/thumb" data-id="${item.id}" data-file-name="${item.noArquivo}" alt="${item.noArquivo}" class="img-rounded img-preview-grid"/>
            </g:else>
        </td>
%{--          DATA--}%
        <td style="width:80px;" class="text-center">${item?.dtAnexo ? item.dtAnexo.format('dd/MM/yyyy') : ''}</td>

%{--          PRINCIPAL--}%
          <g:if test="${item.noArquivo =~/\.zip/}">
                <td style="width:80px;" class="text-center">&nbsp;</td>
                <td style="width:80px;" class="text-center ${item.inPrincipal=='S'?'green':'red'}">
                    <Label class="cursor-pointer">
                       <input type="checkbox" class="fld checkbox-lg" data-change="distribuicao.updateImagemPrincialFicha" data-params="sqFicha,sqFichaAnexo:${item.id}" ${item.inPrincipal=='S'?'checked':''}>&nbsp;
                       ${ item.inPrincipal=='S'?'Sim':'Não'}
                    </Label>
                </td>
          </g:if>
          <g:else>
              <td style="width:80px;" class="text-center ${item.inPrincipal=='S'?'green':'red'}">
                  <Label class="cursor-pointer">
                      <input type="checkbox" class="fld checkbox-lg"  data-change="distribuicao.updateImagemPrincialFicha" data-params="sqFicha,sqFichaAnexo:${item.id}" ${item.inPrincipal=='S'?'checked':''}>&nbsp;
                       ${ item.inPrincipal=='S'?'Sim':'Não'}
                  </Label>

              </td>
              <td style="width:80px;" class="text-center">&nbsp;</td>
          </g:else>

          %{-- LEGENDA --}%
          <td style="width:auto">
                <apan class="inline-edit-content">${item.deLegenda}</apan>
                <i class="ml5 fld fa fa-edit blue inline-edit" title="Alterar"
                  data-id="${item.id}"
                  data-contexto="ficha-anexo"></i>
          </td>

        <td style="width:200px;">${item.noArquivo}</td>

        <td style="width:30px;" class="td-actions">
          <a class="fld btn btn-default btn-xs btn-download" target="_blank" title="Baixar imagem!" href="/salve-estadual/ficha/getAnexo/${item.id}">
              <span class="glyphicon glyphicon-download"></span>
          </a>
          <a data-action="ficha.deleteAnexo" data-descricao="${item.deLegenda}" data-update-grid="divGridDistAnexo" data-id="${item.id}" class="fld btn btn-default btn-xs btn-delete" title="Excluir">
            <span class="glyphicon glyphicon-remove"></span>
          </a>
        </td>
        %{-- <td style="width:*"></td> --}%
      </tr>
    </g:each>
  </tbody>
</table>
