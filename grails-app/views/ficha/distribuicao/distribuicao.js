var grupoSalve = $("#frmTaxonomia #sqGrupoSalve option:selected").data('codigo');
var grupoAvaliado = $("#frmTaxonomia #sqGrupo option:selected").data('codigo');
var gWindowHeight = Math.max(400,window.innerHeight-150);

showZee = /MARINH./i.test( grupoSalve ) ||
    /BRAQUIOPODAS|CEFALOPODAS|CNIDARIAS|ENTEROPNEUSTAS|EQUINODERMAS|ESPONJAS|PORIFERAS_MARINHO|INVERTEBRADOS_MARINHOS|MOLUSCOS_MARINHOS|POLIQUETAS|SIPUNCULA|ELASMOBRANQUIOS|PEIXES_MARINHOS/i.test(grupoAvaliado)
//# sourceURL=\views\ficha\distribuicao\distribuicao.js

// site exemplo de ler muitos pontos
// http://www.acuriousanimal.com/thebookofopenlayers3/chapter03_03_vector_source.html

var vectorLayerFixed = null;
var oLegend = {};
var loadingTab=false;
var distribuicao = {
    oMap:null,
    showZee:false,
    sqOcorrenciaEditar:null,
    idsRegistrosSelecionados:[],
    /*
    saveShpBaciasHidrograficas:function(){
        distribuicao.oMap.layers.map(function(layer){
            var name=layer.get('name');
            if( /BH/i.test( name ) ) {
                layer.getSource().getFeatures().map(function (feature, i) {
                    var featureWkt, modifiedWkt;
                    var wkt = new ol.format.WKT();
                    // criar o wkt da feature
                    var featureClone = feature.clone();
                    featureClone.getGeometry().transform('EPSG:3857', 'EPSG:4326');
                    featureWkt = wkt.writeFeature(featureClone);
                    var properties = featureClone.getProperties();
                    delete properties.text;
                    delete properties.geometry;
                    var data = $.extend({table:'bacias_hidrograficas',wkt:featureWkt}, properties);
                    app.ajax(app.url + 'main/saveWkt', data, function(res) {
                        if( res.status == 0 ) {
                            console.log( res );
                        }
                    });
                })
            }
        })
    },*/
    tabDistribuicaoInit: function(params, data, event, limpar) {
        // aguardar a rederizaçao do html da aba distribuiçao finalizar
        /*window.setTimeout(function(){
            distribuicao.initMap();
        },1000);
         */
        //

        $("#tableGridOcorrenciaBody").html('<br><b>Carregando...</b>');

        if( canModify ) {
            // criar o menu de contexto na divGridOcorrencia
            createContextMenu('tableGridOcorrenciaBody', 'distribuicao.updateGridOcorrencia'
                , [{
                    id: 'refbib'
                    , name: 'Informar Referência Bibliográfica'
                    , icon: 'fa-book'
                    , callback: function () {
                        // ler ids
                        var data = {sqFicha: $("#sqFicha").val(), sqRegistro: []};
                        $("#divGridOcorrencia input:checked").each(function (i, input) {
                            var ocorrencia = $(input).data();
                            if (/^salve/i.test(ocorrencia.bd)) {
                                data.sqRegistro.push(ocorrencia.id);
                            }
                        });
                        if (data.sqRegistro.length == 0) {
                            app.alertInfo('Nenhuma ocorrência do SALVE está selecionada!<br>Ocorrências de outras bases de dados não podem ser alteradadas no SALVE.');
                            return;
                        } else if (data.sqRegistro.length > 1) {
                            data.all = true; // ajstar a tela popup para não exibir o gride das ref bibs já cadastradas
                            data.sqRegistro = data.sqRegistro.join(',');
                        } else {
                            data.sqRegistro = data.sqRegistro = data.sqRegistro[0];
                        }
                        data.noTabela = "ficha_ocorrencia";
                        data.noColuna = "sq_ficha_ocorrencia";
                        data.comPessoal = 1;
                        data.updateGrid = "divGridOcorrencia";
                        data.deRotulo = "Ocorrência";
                        ficha.openModalSelRefBibFicha(data, null);
                    }
                }],{sqFicha:$("#sqFicha").val()});
        }
        // carregar o grid com as imagens dos gráficos
        distribuicao.updateGridAnexos();
        $("#grpAltitude legend").html('Altitude e Profundidade')
        var grupoSalve = $("#sqGrupoSalve option:selected").data('codigo');
        var grupoAvaliado = $("#sqGrupo option:selected").data('codigo');
        if (/MAMIFEROS|REPTEIS/.test(grupoSalve) || /CARNIVOROS_TERRESTRES|CERVIDEOS|LAGOMORFAS|MARSUPIAIS|MORCEGOS|PERISSODACTILAS|PRIMATAS|ROEDORES|TAIASSUIDAES|XENARTRAS|MAMIFEROS_AQUATICOS|ANFISBENAS|CROCODILIANOS|LAGARTOS|QUELONIOS_CONTINENTAIS|SERPENTES/.test( grupoAvaliado ) ) {
            $("#grpAltitude").removeClass('hidden');
            $("#nuMinAltitude,#nuMaxAltitude,#nuMinBatimetria,#nuMaxBatimetria").parent().parent().removeClass('hidden');
        } else if (/AVES|INVERTEBRADOS_TERRESTRES|INVERTEBRADOS_AGUA_DOCE|PEIXES_CONTINENTAIS|ANFIBIOS/.test(grupoSalve)
            || /AVES|ABELHAS|ARACNIDEOS|BORBOLETAS|COLEMBOLAS|COLEOPTERAS|FORMIGAS|INVERTEBRADOS_TERRESTRES|MARIPOSAS|MIRIAPODAS|MOLUSCOS_TERRESTRES|OLIGOQUETAS|ONICOFORAS|QUELONIOS_MARINHOS|PEIXES_AGUA_DOCE|PEIXES_CONTINENTAIS_NAO_AMAZONICOS/.test(grupoAvaliado ) ) {

            $("#grpAltitude").removeClass('hidden');
            $("#nuMinBatimetria,#nuMaxBatimetria").parent().parent().addClass('hidden');
            $("#nuMinAltitude,#nuMaxAltitude").parent().parent().removeClass('hidden');
            $("#grpAltitude legend").html('Altitude');
            if (limpar) {
                $("#nuMinBatimetria,#nuMaxBatimetria").val('');
            }
        } else if (/MARINH./.test(grupoSalve)
            || /BRAQUIOPODAS|CEFALOPODAS|CNIDARIAS|CRUSTACEOS_MARINHOS|ENTEROPNEUSTAS|EQUINODERMAS|ESPONJAS|PORIFERAS_MARINHO|INVERTEBRADOS_MARINHOS|MOLUSCOS_MARINHOS|POLIQUETAS|SIPUNCULA|ELASMOBRANQUIOS|PEIXES_MARINHOS/.test(grupoAvaliado)) {
            $("#grpAltitude").removeClass('hidden');
            $("#nuMinBatimetria,#nuMaxBatimetria").parent().parent().removeClass('hidden');
            $("#nuMinAltitude,#nuMaxAltitude").parent().parent().addClass('hidden');
            $("#grpAltitude legend").html('Profundidade');
            if (limpar) {
                $("#nuMinAltitude,#nuMaxAltitude").val('');
            }
        } else {
            $("#grpAltitude").addClass('hidden');
            if (limpar) {
                $("#nuMinAltitude,#nuMaxAltitude,#nuMinBatimetria,#nuMaxBatimetria").val('');
            }
        }
    },
    tabDisUfInit: function(params) {
        disUf.updateGridUf();
    },
    tabDisBiomaInit: function(params) {
        disBioma.updateGridBioma();
    },
    tabDisBaciaHidrograficaInit: function(params) {
        disBaciaHidrografica.updateGridBaciaHidrografica();
    },
    tabDisAmbienteRestritoInit: function(params) {
        disAmbienteRestrito.updateGridAmbienteRestrito();
    },
    tabDisAreaRelevanteInit: function(params) {
        disAreaRelevante.sqUcFederalChange();
        disAreaRelevante.updateGridDistAreaRelevante();
    },
    tabDisOcorrenciaInit: function(params) {
        //ocorrencia.updateGridOcorrencia(); // arquivo ocorrenia.js
        // carregar a primeira aba
        //ocorrencia.disableTabs(true);
        $( document ).scrollTop( $("#liTabDisOcoGeoreferencia").offset().top - 102 );
        if( distribuicao.sqOcorrenciaEditar ) {
            ocorrencia.editarOcorrencia({sqFicha:$("#sqFicha").val(),sqFichaOcorrencia:distribuicao.sqOcorrenciaEditar})
            distribuicao.sqOcorrenciaEditar=null;
        } else {
            //$("#liTabDisOcoGeoreferencia > a").click();
            ocorrencia.editarOcorrencia({sqFicha:$("#sqFicha").val(),sqFichaOcorrencia:''})
        }
    },
    tabDisEooAooInit:function( params, ele ,evt ){

    },

    initMap: function() {
        var grupoSalve = $("#frmTaxonomia #sqGrupoSalve option:selected").data('codigo');
        var grupoAvaliado = $("#frmTaxonomia #sqGrupo option:selected").data('codigo');
        showZee = /MARINH./i.test( grupoSalve ) ||
                  /BRAQUIOPODAS|CEFALOPODAS|CNIDARIAS|CRUSTACEOS_MARINHOS|ENTEROPNEUSTAS|EQUINODERMAS|ESPONJAS|PORIFERAS_MARINHO|INVERTEBRADOS_MARINHOS|MOLUSCOS_MARINHOS|POLIQUETAS|SIPUNCULA|ELASMOBRANQUIOS|PEIXES_MARINHOS/i.test(grupoAvaliado)
        $('#mapaDistribuicaoMain').html('');
        this.oMap = new Ol3Map({
            el: 'mapaDistribuicaoMain'
            , height: 670
            , gridId: 'containerGridOcorrencia'
            , callbackActions : function (res,options) {
                // console.log( 'Ação "'+options.action +'" ajax no mapa foi executada!' );
                if( options.action != '' ) {

                    if( options.action == 'transferir_registro') {
                        distribuicao.showModalTransferirFicha(res.ids, options);

                    } else if( options.action == 'locateRow'  ) {
                        window.setTimeout(function(){
                            distribuicao.updateGridOcorrencia( options );
                        },100)
                    } else if( options.action == 'saveEOO' ) {
                        var inputVlEoo = $("#tabAvaDistribuicao input[name=vlEoo]");
                        if (inputVlEoo.size() == 1) {
                            inputVlEoo.val(Number(options.vlEoo).formatMoney(0,',','.') );
                        }
                    } else if( options.action == 'deleteEOO' ) {
                        var inputVlEoo = $("#tabAvaDistribuicao input[name=vlEoo]");
                        if( inputVlEoo.size() == 1 ) {
                            inputVlEoo.val( '' );
                        }
                    } else if( options.action == 'saveAOO' ) {
                        var inputVlAoo = $("#tabAvaDistribuicao input[name=vlAoo]");
                        if (inputVlAoo.size() == 1) {
                            inputVlAoo.val(Number(options.vlAoo).formatMoney(0,',','.') );
                        }
                    } else if( options.action == 'deleteAOO' ) {
                        var inputVlAoo = $("#tabAvaDistribuicao input[name=vlAoo]");
                        if( inputVlAoo.size() == 1 ) {
                            inputVlAoo.val( '' );
                        }
                    } else if( !options.action.match(/(AOO|EOO)$/i) ) {
                        // não precisa atulizar o gride de ocorrencias quando a AOO for salva
                        window.setTimeout(function(){
                            distribuicao.updateGridOcorrencia();
                            // atualizar o gride Estados se estiver aberto
                            if( $("#disTableFichaUfsOcorrencia").is(":visible") ) {
                                disUf.updateGridUf();
                            }
                            // atualizar o gride Biomas se estiver aberto
                            if( $("#disTableFichaBiomasOcorrencia").is(":visible") ) {
                                disBioma.updateGridBioma();
                            }
                            // atualizar o gride Bacias se estiver aberto
                            if( $("#disTableFichaBaciasOcorrencia").is(":visible") ) {
                                disBaciaHidrografica.updateGridBaciaHidrografica();
                            }
                            // atualizar a ba 7.4-Presença em UC se estiver carregada
                            if( $("#divGridPresencaUc").size() == 1 && presencaUc && typeof( presencaUc.updateGridPresencaUc) == 'function' ) {
                                presencaUc.updateGridPresencaUc();
                            }
                        },2000)
                    }
                }
            }
            , readOnly: ! canModify
            , showToolbar: true
            , showZee : showZee
            , sqFicha: $("#sqFicha").val()
            , urlAction: 'fichaCompleta/mapActions'
        });
        distribuicao.oMap.run();
        getWorkers();
    },
    showModalTransferirFicha: function( ids, options ) {
        options = options || {};
        var height  = 380;
        var width   = 600;
        var data = {sqFicha:options.sqFicha };
        distribuicao.transferirRegistrosData={ids:ids, sqFichaOrigem:options.sqFicha};
        data.height = height;
        data.reload = false;
        data.modalName = 'tranferirRegistroEspecie';
        app.openWindow({
            id: 'modalTranferirRegistroEspecie',
            url: app.url + 'ficha/getModal',
            width: width,
            data: data,
            height: height,
            title: 'Transferência de Registros Entre Fichas',
            autoOpen: true,
            modal: true
        }, function (data, e) {
            // onShow
            //data = data.data;
        }, function (data, e) {
            // onClose;
            distribuicao.transferirRegistrosData={};
        });

    },
    saveTransferirRegistro:function( params, fld ,e){
        if( ! params.sqFichaSelecionada ){
            app.alertInfo('Selecione a ficha destino');
            return;
        }
        if( parseInt( params.sqFichaSelecionada) == parseInt( distribuicao.transferirRegistrosData.sqFichaOrigem ) ) {
            app.alertInfo('Ficha origem e destino são as mesmas. Selecione outra ficha.');
            return;
        }
        var ids = distribuicao.transferirRegistrosData.ids;
        var inMarcarUtilizado = $("#frmModalTransferirRegistroEspecie #inMarcarUtilizado:checked").val() || 'N'
        var data = { idRegistros:JSON.stringify(ids)
            ,sqFichaOrigem:distribuicao.transferirRegistrosData.sqFichaOrigem
            ,sqFichaDestino:params.sqFichaSelecionada
            ,inMarcarUtilizado:inMarcarUtilizado
        }

        app.confirm('Confirma transferência de <b>'+ids.length+'</b> registro(s) para a ficha selecionada?', function() {
            app.ajax( app.url + 'ficha/transferirRegistro', data, function(res) {
                if ( res.status == 0) {
                    app.closeWindow('modalTranferirRegistroEspecie');
                    distribuicao.refreshLayer();
                }
            }, 'Aguarde...', 'json')
        });
    },
    refreshLayer: function(params) {
        var dataParams = app.params2data({},params);
        try {
            distribuicao.oMap.reloadLayers(dataParams);
        } catch (e) {}
    },
    identificarPonto: function(params) {
        params.center = params.center == false ? false : true
        //console.log( params )
        distribuicao.oMap.identifyPoint({id:params.pontoId,bd:params.bd}, params.center)
    },
    //------------------ fim parte geo -----------------------------


    exportarGrideOcorrenciaCsv: function(params, fld, evt) {
        // utilizar nova tela padrao para de download de ocorrencias
        var data = { sqCicloAvaliacao:params.sqCicloAvaliacao, sqFicha:params.sqFicha, contexto:params.contexto};
        // TODO - implementar filtro pelas ocorrencias filtradas utilizando os filtros nos campos selects das colunas
        ficha.exportarOcorrencias(data,fld,evt);
        //console.log( params )


        /*
        params.sqOcorrenciasSelecionadasSalve=[];
        params.sqOcorrenciasSelecionadasPortalbio=[];
        $("#divGridOcorrencia input:checkbox:checked:visible").each(function(i,item){
            var data = $(item).data();
            if( data.bd=='salve' )
            {
                if( data.id) {
                    params.sqOcorrenciasSelecionadasSalve.push(data.id);
                }
            }
            else
            {
                if( data.id ) {
                    params.sqOcorrenciasSelecionadasPortalbio.push(data.id);
                }
            }
        });
        params.sqOcorrenciasSelecionadasSalve = params.sqOcorrenciasSelecionadasSalve.join(',');
        params.sqOcorrenciasSelecionadasPortalbio = params.sqOcorrenciasSelecionadasPortalbio.join(',');
        app.exportarGrideOcorrenciaCsv( params,fld, evt );
        */
    },
    stEndemicaBrasilChange: function(params) {
        if ($("#stEndemicaBrasil").val() == 'S') {
            $("#divDistGeoNacional").addClass('hidden');
        } else {
            $("#divDistGeoNacional").removeClass('hidden');
        }
    },
    saveFrmDistribuicao: function(params) {
        if (!$('#frmDistribuicao').valid()) {
            return false;
        }
        if (!$("#grpAltitude").is(":visible")) {
            $("#nuMinAltitude,#nuMaxAltitude,#nuMinBatimetria,#nuMaxBatimetria").val('');
        }
        if ($("#numMinAltitude").is(":visible")) {
            $("#numMinAltitude,#numMaxAltitude").val('')
        }
        if ($("#stEndemicaBrasil").val() == 'S') {
            $("#dsDistribuicaoGeoNacional").val('')
        }
        var data = $("#frmDistribuicao").serializeArray();
        data = app.params2data(params, data);
        if (!data.sqFicha) {
            app.alertError('ID da ficha não encontrado!');
            return;
        }
        app.ajax(app.url + 'fichaDistribuicao/saveFrmDistribuicao', data, function(res) {
            if (res.status == 0) {
                app.resetChanges('frmDistribuicao');
            }
        }, null, 'json')
    },
    updateGridAnexos: function() {
        ficha.getGridAnexos('divGridDistAnexo');
    },
    limparFiltroGrideOcorrencia:function(){
        $("#tableGridOcorrencia *.tableHeaderInputFilter").map(function(index, field){
            $(field).val('');
        });
        distribuicao.updateGridOcorrencia()
    },
    updateGridOcorrencia:function(params) {
        //localizar a página do registro selecionado no mapa
        params = params || {};
        if( params && params.bd && params.id ) {
            params.idFiltro = params.id;
            //params.bdFiltro = params.bd;
        }
        app.grid('GridOcorrencia', params ,function( res ){
            // localizar a linha no gride
            if( params.id && params.bd ) {
                //params.id = params.idFiltro // para o findGridRow funcionar
                //params.bd = params.bdFiltro // para o findGridRow funcionar
                distribuicao.oMap.findGridRow(params, params.scroll, false );
            }
            if( typeof tabGeoreferencia != 'undefined' ){
                tabGeoreferencia.showHideFieldLocalidadeDesconhecida();
            }
        });
        if( params && params.updateMap ) {
            distribuicao.oMap.reloadLayers();
        }

        /*
        // todo - montrar os filtros ativos no cabecalho da tabela
        var filtros = {}
        $("#tableGridOcorrencia *.tableHeaderInputFilter").map(function(index, field){
            var $field = $(field);
            if( $field.val() ){
                var data=$field.data();
                filtros[ data.field ]=$field.val()
            }
        });

        // manter a página corrente ao atualizar o gride
        $pagination = $("#tableGridOcorrenciasPagination")
        if( $pagination.size() > 0 ) {
            var $selectCurrentPage = $pagination.find("select.pagination-select");
            if( $selectCurrentPage.size() > 0) {
                filtros.paginationPageNumber = $selectCurrentPage.val();
            }

        }
        //localizar a página do registro selecionado no mapa
        if( params && params.bd && params.id ) {
            app.blockUI('Localizando registro. Aguarde...');
            filtros.idFilter = params.id;
            filtros.bdFilter = params.bd;
        }

        // verificar se a funcao foi chamada pelos inputs dos filtros
        try {
            if (params && params.target) {
                var elmnt = document.getElementById("rowGrideOcorrencia");
                elmnt.scrollIntoView(true);
                $(document).scrollTop($(document).scrollTop() - 100);
            }
        } catch(e){}
        ficha.getGrid('ocorrencia','',function(){
            app.unblockUI();
            if( filtros.idFilter && filtros.bdFilter ) {
                // localizar a linha no gride
                distribuicao.oMap.findGridRow(params, params.scroll, false );
            }

            //var maxHeight = Math.min( Math.max( parseInt($(window).height() - 200 ), 200), 500);
            //$('#tableGridOcorrencia').DataTable($.extend({}, default_data_tables_options,{})).columns.adjust();
            // $('#tableGridOcorrencia').DataTable( $.extend({},default_data_tables_options,
            //     {
            //         "deferRender"      : true
            //         ,"scrollY"          : maxHeight
            //         ,"scrollX"          : true
            //         ,"scrollCollapse"   : true
            //         ,"fixedHeader"      : false
            //         ,"lengthChange"     : false
            //         ,"columnDefs" : [
            //             {   "orderable": false, "targets": [1,3,11]
            //             }],
            //         "searching": true
            //         ,"buttons": []
            //     })
            // );
            //
            if( typeof tabGeoreferencia != 'undefined' ){
                tabGeoreferencia.showHideFieldLocalidadeDesconhecida();
            }
        },$("#sqFicha").val(), filtros );

        // atualiar gride dos Estados
        if( $("#divGridDistUf").size() == 1 )
        {
            disUf.updateGridUf(params);
        }

        if( params && params.updateMap )
        {
            distribuicao.oMap.reloadLayers();
        }*/
    },
    editarOcorrencia : function( params )  {
        // não carregou a aba 2.6 ainda

        if( typeof ocorrencia == 'undefined' )
        {
            if( !loadingTab )
            {
                loadingTab =true;
                distribuicao.sqOcorrenciaEditar = params.sqFichaOcorrencia;
                $("#liTabDisOcorrencia > a").click();
            }
            /*window.setTimeout(function(){
                distribuicao.editarOcorrencia( params );
            },3000)*/
        }
        else
        {
            loadingTab=false;
            ocorrencia.editarOcorrencia( params );
        }
    },
    updateImagemPrincialFicha:function( params,ele,evt ){
        var data= {sqFichaAnexo:params.sqFichaAnexo}
        app.ajax( app.url + 'ficha/toggleImagemPrincipal', data, function(res) {
            distribuicao.updateGridAnexos()
        }, 'Aguarde...', 'json');
    },


};

// ################################################################################################################################ //
// 2.1 - UF                                                                                                                         //
// ################################################################################################################################ //
var disUf = {
    openModalSelUf: function(params) {
        var data = {};
        data.reload = true;
        data = app.params2data(params, data)
        $.jsPanel({
            id: "modalSelUf",
            paneltype: 'modal',
            container: 'body',
            draggable: {
                handle: 'div.jsPanel-titlebar, div.jsPanel-ftr',
                opacity: 0.8
            },
            dragit: {
                axis: false,
                containment: 'parent',
                handles: '.jsPanel-titlebar, .jsPanel-ftr.active', // do not set .jsPanel-titlebar to .jsPanel-hdr
                opacity: 0.8,
                start: false,
                drag: false,
                stop: false,
                disableui: true
            },
            resizable: {
                handles: 'n, e, s, w, ne, se, sw, nw',
                autoHide: false,
                minWidth: 500,
                minHeight: 500
            },
            resizeit: {
                containment: 'parent',
                handles: 'n, e, s, w, ne, se, sw, nw',
                minWidth: 500,
                minHeight: 500,
                maxWidth: 10000,
                maxHeight: 10000,
                start: false,
                resize: false,
                stop: false,
                disableui: true
            },
            show: 'animated',
            theme: "green",
            headerControls: {
                controls: "closeonly"
            },
            headerTitle: 'Seleção de Estados',
            contentSize: {
                width: 700,
                height: gWindowHeight
            },
            content: '<div id="div_content_ficha_selecionar_estado" style="outline:none;border:none;display:block;min-height:100%;"<span id="spinner">Carregando...' + window.grails.spinner + '</span></div>',
            callback: function(panel) {
                // definir o id do body para a funão loadModulo injetar o html do form
                panel.find('.jsPanel-content').prop('id', panel.attr('id') + '_body')
                    // ler o formulário
                app.loadModule(app.url + 'ficha/getModal', params, panel.attr('id') + '_body', function(res) {

                    // ajustar a altura da treeview para que o botão salvar fique visivel
                    $("#modalSelUf #treeChecks").css('height',gWindowHeight-150);

                    $("#modalSelUf #treeChecks").fancytree({
                        quicksearch: true,
                        selectMode: 2,
                        strings: {
                            loading: "Carregando...",
                            loadError: "Erro de leitura!",
                            moreData: "Mais...",
                            noData: "Nenhum registro."
                        },
                        focusOnSelect: false,
                        extensions: ["wide", "filter"],
                        filter: {
                            autoApply: true, // Re-apply last filter if lazy data is loaded
                            autoExpand: true, // Expand all branches that contain matches while filtered
                            counter: true, // Show a badge with number of matching child nodes near parent icons
                            fuzzy: false, // Match single characters in order, e.g. 'fb' will match 'FooBar'
                            hideExpandedCounter: false, // Hide counter badge if parent is expanded
                            hideExpanders: false, // Hide expanders if all child nodes are hidden by filter
                            highlight: true, // Highlight matches by wrapping inside <mark> tags
                            leavesOnly: false, // Match end nodes only
                            nodata: true, // Display a 'no data' status node if result is empty
                            mode: "hide" // dimm = Grayout unmatched nodes (pass "hide" to remove unmatched node instead)
                        },
                        source: {
                            data: data,
                            url: app.url + "fichaDistribuicao/getTreeUfs"
                        },
                        checkbox: true,
                        autoScroll:true,
                    });
                    // ativar função de filtragem // http://wwwendt.de/tech/fancytree/demo/sample-ext-filter.html#
                    $("#frmModalSelUf #fldFiltrarTreeView").keyup(function(e) {
                        var n,
                            tree = $.ui.fancytree.getTree(),
                            opts = {},
                            filterFunc = tree.filterNodes,
                            match = $(this).val();
                        if (e && e.which === $.ui.keyCode.ESCAPE || $.trim(match) === "") {
                            $("#frmModalSelUf #btnTreeViewReset").click();
                            return;
                        }
                        if (match) {
                            $("#frmModalSelUf #btnTreeViewReset").removeClass('hidden');
                        } else {
                            $("#frmModalSelUf #btnTreeViewReset").addClass('hidden');
                        }
                        n = filterFunc.call(tree, match, opts);
                    }).focus();
                    $("#frmModalSelUf #btnTreeViewReset").click(function() {
                        $("#modalSelUf #treeChecks").fancytree("getTree").clearFilter();
                        $("#frmModalSelUf fldFiltrarTreeView").val('').focus();
                    });
                    $("#frmModalSelUf #btnTreeViewNone").click(function() {
                        var node = $("#modalSelUf #treeChecks").fancytree("getTree").getActiveNode();
                        if (node) {
                            node.setSelected(false);
                            node.visit(function(node) {
                                if (!$(node.span).hasClass('fancytree-hide')) {
                                    node.setSelected(false);
                                }
                            });
                        }
                        return false;
                    });
                    $("#frmModalSelUf #btnTreeViewAll").click(function() {
                        var node = $("#modalSelUf #treeChecks").fancytree("getTree").getActiveNode();
                        if (node) {
                            node.setSelected(true);
                            node.visit(function(node) {
                                if (!$(node.span).hasClass('fancytree-hide')) {
                                    node.setSelected(true);
                                }
                            });
                        }
                        return false;
                    });
                    $("#frmModalSelUf #btnTreeViewNone").click(function() {
                        $("#modalSelUf #treeChecks").fancytree("getTree").visit(function(node) {
                            if (!$(node.span).hasClass('fancytree-hide')) {
                                node.setSelected(false);
                            }
                        });
                        return false;
                    });
                    $("#frmModalSelUf #btnTreeViewAll").click(function() {
                        $("#modalSelUf #treeChecks").fancytree("getTree").visit(function(node) {
                            if (!$(node.span).hasClass('fancytree-hide')) {
                                node.setSelected(true);
                            }
                        });
                        return false;
                    });
                }, '');
            },
        });
    },
    saveTreeUf: function(params) {
        var data = {
            sqFicha: $("#sqFicha").val(),
            ids: []
        };
        $.each($("#modalSelUf #treeChecks").fancytree('getTree').getSelectedNodes(), function() {
            data.ids.push(this.data.id);
        })
        data.ids = data.ids.join(',');
        app.ajax(app.url + 'fichaDistribuicao/saveTreeUf', data,
            function(res) {
                if (!res.status) {
                    disUf.updateGridUf();
                }
            }, null, 'json'
        );
    },
    /*saveFrmDisUf: function(params) {
     if (!$('#frmDisUf').valid()) {
     return false;
     }
     var data = $("#frmDisUf").serializeArray();
     data = app.params2data(params, data);
     data.sqEstado = $("#frmDisUf #sqEstado").select2('val');

     if (!data.sqFicha) {
     app.alertError('ID da ficha não encontrado!');
     return;
     }
     app.ajax(app.url + 'fichaDistribuicao/saveFrmDisUf', data, function(res) {
     if (!res['status']) {
     //app.loadModule(app.url+'fichaDistribuicao/tabUf', data, 'tabDisUf');
     disUf.updateGridUf();
     app.reset('frmDisUf');
     }
     }, null, 'json')
     },
     */
    deleteDistUf: function(data) {
        app.confirm('Confirma exclusão do Estado <b>' + data.descricao + '</b>?', function() {
            app.ajax(app.url + 'fichaDistribuicao/deleteDistUf', data, function(res) {
                if (!res['status']) {
                    disUf.updateGridUf();
                }
            }, null, 'json')
        });
    },
    updateGridUf: function() {
        ficha.getGrid('distUf',null,function(){
            $("#disTableFichaUfsFicha,#disTableFichaUfsOcorrencia").floatThead('destroy');
            var $table1 = $("#disTableFichaUfsFicha")
                $table1.floatThead({
                scrollContainer: function( $table ) {
                    return $table.closest('div');
                }
            });
            var $table2 = $("#disTableFichaUfsOcorrencia")
                $table2.floatThead({
                scrollContainer: function( $table ) {
                    return $table.closest('div');
                }
            });
        });
    },
    selAllUf: function(data) {
        app.confirm('Confirma a seleção de TODAS as UFs?', function() {
            app.ajax(app.url + 'fichaDistribuicao/selAllUf', data, function(res) {
                if (!res['status']) {
                    disUf.updateGridUf();
                }
            }, null, 'json')
        });
    }
};


// ################################################################################################################################ //
// 2.2 - BIOMA                                                                                                                      //
// ################################################################################################################################ //
var disBioma = {
    openModalSelBioma: function(params) {
        var data = {};
        data.reload = true;
        data = app.params2data(params, data)
        $.jsPanel({
            // http://jspanel.de/api/
            id: "modalSelBioma",
            paneltype: 'modal',
            container: 'body',
            draggable: {
                handle: 'div.jsPanel-titlebar, div.jsPanel-ftr',
                opacity: 0.8
            },
            dragit: {
                axis: false,
                containment: false,
                handles: '.jsPanel-titlebar, .jsPanel-ftr.active', // do not set .jsPanel-titlebar to .jsPanel-hdr
                opacity: 0.8,
                start: false,
                drag: false,
                stop: false,
                disableui: false
            },
            resizable: {
                handles: 'n, e, s, w, ne, se, sw, nw',
                autoHide: false,
                minWidth: 500,
                minHeight: 500
            },
            resizeit: {
                containment: 'parent',
                handles: 'n, e, s, w, ne, se, sw, nw',
                minWidth: 500,
                minHeight: 500,
                maxWidth: 10000,
                maxHeight: 10000,
                start: false,
                resize: false,
                stop: false,
                disableui: true
            },
            show: 'animated slideInUp',
            theme: "green",
            headerTitle: 'Seleção de Biomas',
            contentSize: {
                width: 700,
                height: window.innerHeight - 100
            },
            content: '<div id="div_content_ficha_selecionar_uso" style="outline:none;border:none;display:block;min-height:100%;"<span id="spinner">Carregando...' + window.grails.spinner + '</span></div>',
            callback: function(panel) {
                // ler o formulário
                panel.find('.jsPanel-content').prop('id', panel.attr('id') + '_body')
                app.loadModule(app.url + 'ficha/getModal', params, panel.attr('id') + '_body', function(res) {
                    $("#modalSelBioma #treeChecks").fancytree({
                        quicksearch: true,
                        selectMode: 2,
                        strings: {
                            loading: "Carregando...",
                            loadError: "Erro de leitura!",
                            moreData: "Mais...",
                            noData: "Nenhum registro."
                        },
                        focusOnSelect: false,
                        extensions: ["wide", "filter"],
                        filter: {
                            autoApply: true, // Re-apply last filter if lazy data is loaded
                            autoExpand: true, // Expand all branches that contain matches while filtered
                            counter: true, // Show a badge with number of matching child nodes near parent icons
                            fuzzy: false, // Match single characters in order, e.g. 'fb' will match 'FooBar'
                            hideExpandedCounter: false, // Hide counter badge if parent is expanded
                            hideExpanders: false, // Hide expanders if all child nodes are hidden by filter
                            highlight: true, // Highlight matches by wrapping inside <mark> tags
                            leavesOnly: false, // Match end nodes only
                            nodata: true, // Display a 'no data' status node if result is empty
                            mode: "hide" // dimm = Grayout unmatched nodes (pass "hide" to remove unmatched node instead)
                        },
                        source: {
                            data: data,
                            url: app.url + "fichaDistribuicao/getTreeBiomas"
                        },
                        checkbox: true,
                    });
                    // ativar função de filtragem // http://wwwendt.de/tech/fancytree/demo/sample-ext-filter.html#
                    $("#frmModalSelBioma #fldFiltrarTreeView").keyup(function(e) {
                        var n,
                            tree = $.ui.fancytree.getTree(),
                            opts = {},
                            filterFunc = tree.filterNodes,
                            match = $(this).val();
                        if (e && e.which === $.ui.keyCode.ESCAPE || $.trim(match) === "") {
                            $("#frmModalSelBioma #btnTreeViewReset").click();
                            return;
                        }
                        if (match) {
                            $("#frmModalSelBioma #btnTreeViewReset").removeClass('hidden');
                        } else {
                            $("#frmModalSelBioma #btnTreeViewReset").addClass('hidden');
                        }
                        n = filterFunc.call(tree, match, opts);
                    }).focus();
                    $("#frmModalSelBioma #btnTreeViewReset").click(function() {
                        $("#modalSelBioma #treeChecks").fancytree("getTree").clearFilter();
                        $("#frmModalSelBioma fldFiltrarTreeView").val('').focus();
                    });
                    $("#frmModalSelBioma #btnTreeViewNone").click(function() {
                        var node = $("#modalSelBioma #treeChecks").fancytree("getTree").getActiveNode();
                        if (node) {
                            node.setSelected(false);
                            node.visit(function(node) {
                                if (!$(node.span).hasClass('fancytree-hide')) {
                                    node.setSelected(false);
                                }
                            });
                        }
                        return false;
                    });
                    $("#frmModalSelBioma #btnTreeViewAll").click(function() {
                        var node = $("#modalSelBioma #treeChecks").fancytree("getTree").getActiveNode();
                        if (node) {
                            node.setSelected(true);
                            node.visit(function(node) {
                                if (!$(node.span).hasClass('fancytree-hide')) {
                                    node.setSelected(true);
                                }
                            });
                        }
                        return false;
                    });
                    $("#frmModalSelBioma #btnTreeViewNone").click(function() {
                        $("#modalSelBioma #treeChecks").fancytree("getTree").visit(function(node) {
                            if (!$(node.span).hasClass('fancytree-hide')) {
                                node.setSelected(false);
                            }
                        });
                        return false;
                    });
                    $("#frmModalSelBioma #btnTreeViewAll").click(function() {
                        $("#modalSelBioma #treeChecks").fancytree("getTree").visit(function(node) {
                            if (!$(node.span).hasClass('fancytree-hide')) {
                                node.setSelected(true);
                            }
                        });
                        return false;
                    });
                }, '');
            },
        });
    },
    saveTreeBioma: function(params) {
        var data = {
            sqFicha: $("#sqFicha").val(),
            ids: []
        };
        $.each($("#modalSelBioma #treeChecks").fancytree('getTree').getSelectedNodes(), function() {
            data.ids.push(this.data.id);
        })
        data.ids = data.ids.join(',');
        app.ajax(app.url + 'fichaDistribuicao/saveTreeBioma', data,
            function(res) {
                if (!res.status) {
                    if( res.data.msgBiomaMarinho ){
                        app.alertInfo(res.data.msgBiomaMarinho,'Aviso de possível inconsistência.')
                    }
                    disBioma.updateGridBioma();
                }
            }, null, 'json'
        );
    },
    /*
     saveFrmDisBioma: function(params) {
     if (!$('#frmDisBioma').valid()) {
     return false;
     }
     var data = $("#frmDisBioma").serializeArray();
     data = app.params2data(params, data);
     data.sqBioma    = $("#frmDisBioma #sqBioma").select2('val');
     if (!data.sqFicha) {
     app.alertError('ID da ficha não encontrado!');
     return;
     }
     app.ajax(app.url + 'fichaDistribuicao/saveFrmDisBioma', data, function(res) {
     if (!res['status']) {
     disBioma.updateGridBioma();
     app.reset('frmDisBioma');
     }
     }, null, 'json')
     },
     */
    deleteDistBioma: function(data) {
        app.confirm('Confirma exclusão do bioma <b>' + data.descricao + '</b>?', function() {
            app.ajax(app.url + 'fichaDistribuicao/deleteDistBioma', data, function(res) {
                if (!res['status'] == 1) {
                    disBioma.updateGridBioma();
                }
            }, null, 'json')
        });
    },
    updateGridBioma: function() {
        ficha.getGrid('distBioma',null,function(){
            $("#disTableFichaBiomasFicha,#disTableFichaBiomasOcorrencia").floatThead('destroy');
            var $table1 = $("#disTableFichaBiomasFicha")
            $table1.floatThead({
                scrollContainer: function( $table ) {
                    return $table.closest('div');
                }
            });
            var $table1 = $("#disTableFichaBiomasOcorrencia")
            $table1.floatThead({
                scrollContainer: function( $table ) {
                    return $table.closest('div');
                }
            });
        });
    },

    selAllBioma: function(data) {
        app.confirm('Confirma a seleção de TODOS os Biomas?', function() {
            app.ajax(app.url + 'fichaDistribuicao/selAllBioma', data, function(res) {
                if (!res['status']) {
                    disBioma.updateGridBioma();
                    app.reset('frmDisBioma');
                }
            }, null, 'json')
        });
    }
};
// ################################################################################################################################ //
// 2.3 - BACIA HIDROGRAFICA                                                                                                         //
// ################################################################################################################################ //
var disBaciaHidrografica = {
    openModalSelBacia: function(params) {
        var data = {};
        data.reload = true;
        data = app.params2data(params, data)
        $.jsPanel({
            // http://jspanel.de/api/
            id: "modalSelBacia",
            paneltype: 'modal',
            container: 'body',
            draggable: {
                handle: 'div.jsPanel-titlebar, div.jsPanel-ftr',
                opacity: 0.8
            },
            dragit: {
                axis: false,
                containment: false,
                handles: '.jsPanel-titlebar, .jsPanel-ftr.active', // do not set .jsPanel-titlebar to .jsPanel-hdr
                opacity: 0.8,
                start: false,
                drag: false,
                stop: false,
                disableui: false
            },
            resizable: {
                handles: 'n, e, s, w, ne, se, sw, nw',
                autoHide: false,
                minWidth: 500,
                minHeight: 500
            },
            resizeit: {
                containment: 'parent',
                handles: 'n, e, s, w, ne, se, sw, nw',
                minWidth: 500,
                minHeight: 500,
                maxWidth: 10000,
                maxHeight: 10000,
                start: false,
                resize: false,
                stop: false,
                disableui: true
            },
            show: 'animated slideInUp',
            theme: "green",
            headerTitle: 'Seleção de Bacia Hidrográfica',
            contentSize: {
                width: 700,
                height: window.innerHeight - 100
            },
            content: '<div id="div_content_ficha_selecionar_bacia" style="outline:none;border:none;display:block;min-height:100%;"<span id="spinner">Carregando...' + window.grails.spinner + '</span></div>',
            callback: function(panel) {
                // ler o formulário
                panel.find('.jsPanel-content').prop('id', panel.attr('id') + '_body')
                app.loadModule(app.url + 'ficha/getModal', params, panel.attr('id') + '_body', function(res) {
                    $("#modalSelBacia #treeChecks").fancytree({
                        quicksearch: true,
                        checkbox: true,
                        selectMode: 3,
                        strings: {
                            loading: "Carregando...",
                            loadError: "Erro de leitura!",
                            moreData: "Mais...",
                            noData: "Nenhum registro."
                        },
                        focusOnSelect: false,
                        extensions: ["wide", "filter"],
                        filter: {
                            autoApply: true, // Re-apply last filter if lazy data is loaded
                            autoExpand: true, // Expand all branches that contain matches while filtered
                            counter: true, // Show a badge with number of matching child nodes near parent icons
                            fuzzy: false, // Match single characters in order, e.g. 'fb' will match 'FooBar'
                            hideExpandedCounter: false, // Hide counter badge if parent is expanded
                            hideExpanders: false, // Hide expanders if all child nodes are hidden by filter
                            highlight: true, // Highlight matches by wrapping inside <mark> tags
                            leavesOnly: false, // Match end nodes only
                            nodata: true, // Display a 'no data' status node if result is empty
                            mode: "hide" // dimm = Grayout unmatched nodes (pass "hide" to remove unmatched node instead)
                        },
                        source: {
                            data: data,
                            url: app.url + "fichaDistribuicao/getTreeBacias"
                        },
                        click: function(event, data) {
                            // https://github.com/mar10/fancytree/wiki/TutorialEvents
                            var tt = $.ui.fancytree.getEventTargetType(event.originalEvent);
                            if( tt =='title' )
                            {
                                data.node.toggleExpanded();
                            }
                        },
                        beforeSelect: function(event, data) {
                            //console.log( 'beforeSelect', data)
                            if( data.node.isFolder() || data.node.hasChildren() ){
                                data.node.toggleExpanded();
                                return false;
                            }
                        },
                        renderNode: function(event, data) {

                        },
                        createNode: function(event, data) {
                            //console.log('createNode',data)
                        },


                    });
                    // ativar função de filtragem // http://wwwendt.de/tech/fancytree/demo/sample-ext-filter.html#
                    $("#frmModalSelBacia #fldFiltrarTreeView").keyup(function(e) {
                        var n,
                            tree = $.ui.fancytree.getTree(),
                            opts = {},
                            filterFunc = tree.filterNodes,
                            match = $(this).val();
                        if (e && e.which === $.ui.keyCode.ESCAPE || $.trim(match) === "") {
                            $("#frmModalSelBacia #btnTreeViewReset").click();
                            return;
                        }
                        if (match) {
                            $("#frmModalSelBacia #btnTreeViewReset").removeClass('hidden');
                        } else {
                            $("#frmModalSelBacia #btnTreeViewReset").addClass('hidden');
                        }
                        n = filterFunc.call(tree, match, opts);
                    }).focus();
                    $("#frmModalSelBacia #btnTreeViewReset").click(function() {
                        $("#modalSelBacia #treeChecks").fancytree("getTree").clearFilter();
                        $("#frmModalSelBacia fldFiltrarTreeView").val('').focus();
                    });
                    $("#frmModalSelBacia #btnTreeViewNone").click(function() {
                        var node = $("#modalSelBacia #treeChecks").fancytree("getTree").getActiveNode();
                        if (node) {
                            node.setSelected(false);
                            node.visit(function(node) {
                                if (!$(node.span).hasClass('fancytree-hide')) {
                                    node.setSelected(false);
                                }
                            });
                        }
                        return false;
                    });
                    $("#frmModalSelBacia #btnTreeViewAll").click(function() {
                        var node = $("#modalSelBacia #treeChecks").fancytree("getTree").getActiveNode();
                        if (node) {
                            node.setSelected(true);
                            node.visit(function(node) {
                                if (!$(node.span).hasClass('fancytree-hide')) {
                                    node.setSelected(true);
                                }
                            });
                        }
                        return false;
                    });
                    $("#frmModalSelBacia #btnTreeViewNone").click(function() {
                        var node = $("#modalSelBacia #treeChecks").fancytree("getTree").getActiveNode();
                        if (node) {
                            node.setSelected(false);
                            node.visit(function(node) {
                                if (!$(node.span).hasClass('fancytree-hide')) {
                                    node.setSelected(false);
                                }
                            });
                        }
                        return false;
                    });
                    $("#frmModalSelBacia #btnTreeViewAll").click(function() {
                        var node = $("#modalSelBacia #treeChecks").fancytree("getTree").getActiveNode();
                        if (node) {
                            node.setSelected(true);
                            node.visit(function(node) {
                                if (!$(node.span).hasClass('fancytree-hide')) {
                                    node.setSelected(true);
                                }
                            });
                        }
                        return false;
                    });
                    $("#frmModalSelBacia #btnTreeViewCollapseAll").click(function() {
                        $("#modalSelBacia #treeChecks").fancytree("getRootNode").visit(function(node) {
                            if (!$(node.span).hasClass('fancytree-hide')) {
                                node.setExpanded(false);
                            }
                        });
                    });
                    $("#frmModalSelBacia #btnTreeViewExpandAll").click(function() {
                        $("#modalSelBacia #treeChecks").fancytree("getRootNode").visit(function(node) {
                            if (!$(node.span).hasClass('fancytree-hide')) {
                                node.setExpanded(true);
                            }
                        });
                        return false;
                    });
                }, '');
            },
        });
    },
    saveTreeBacia: function(params) {
        var data = {
            sqFicha: $("#sqFicha").val(),
            ids: []
        };
        $.each($("#modalSelBacia #treeChecks").fancytree('getTree').getSelectedNodes(), function() {
            data.ids.push(this.data.id);
        })
        data.ids = data.ids.join(',');
        app.ajax(app.url + 'fichaDistribuicao/saveTreeBacia', data,
            function(res) {
                if (!res.status) {
                    disBaciaHidrografica.updateGridBaciaHidrografica();
                }
            }, null, 'json'
        );
    },
    saveFrmDisBaciaHidrografica: function(params) {
        if (!$('#frmDisBaciaHidrografica').valid()) {
            return false;
        }
        var data = $("#frmDisBaciaHidrografica").serializeArray();
        data = app.params2data(params, data);
        data.sqBacia = $("#frmDisBaciaHidrografica #sqBaciaHidrografica").select2('val');
        if (!data.sqFicha) {
            app.alertError('ID da ficha não encontrado!');
            return;
        }
        app.ajax(app.url + 'fichaDistribuicao/saveFrmDisBaciaHidrografica', data, function(res) {
                if (!res['status']) {
                    disBaciaHidrografica.updateGridBaciaHidrografica();
                    app.reset('frmDisBaciaHidrografica');
                }
            }, null, 'json')
            /*
             if (!$('#frmDisBaciaHidrografica').valid()) {
             return false;
             }
             var data = {}
             data = app.params2data(params, data)
             if (!data.sqFicha) {
             app.alertError('ID da ficha não encontrado!');
             return;
             }
             if (!data.sqBaciaHidrografica) {
             app.alertError('Nenhuma bacia hidrográfica selecionada!');
             return;
             }
             app.ajax(app.url + 'fichaDistribuicao/saveFrmDisBaciaHidrografica', data, function(res) {
             if (!res['status']) {
             disBaciaHidrografica.updateGridBaciaHidrografica();
             app.reset('frmDisBaciaHidrografica');
             }
             }, null, 'json')
             */
    },
    deleteDistBaciaHidrografica: function(data) {
        app.confirm('Confirma exclusão da <b>' + data.descricao + '</b>?', function() {
            app.ajax(app.url + 'fichaDistribuicao/deleteDistBaciaHidrografica', data, function(res) {
                if (!res['status']) {
                    //app.loadModule(app.url+'fichaDistribuicao/tabBaciaHidrografica', data, 'tabDisBaciaHidrografica');
                    disBaciaHidrografica.updateGridBaciaHidrografica();
                }
            }, null, 'json')
        });
    },
    updateGridBaciaHidrografica: function(params) {
        ficha.getGrid('distBaciaHidrografica',null,function(){
            $("#disTableFichaBaciasFicha,#disTableFichaBaciasOcorrencia").floatThead('destroy');
            var $table1 = $("#disTableFichaBaciasFicha")
            $table1.floatThead({
                scrollContainer: function( $table ) {
                    return $table.closest('div');
                }
            });
            var $table1 = $("#disTableFichaBaciasOcorrencia")
            $table1.floatThead({
                scrollContainer: function( $table ) {
                    return $table.closest('div');
                }
            });
        });















    },
    selAllBacia: function(data) {
        app.confirm('Confirma a seleção de TODAS as Bacias Hidrográficas?', function() {
            app.ajax(app.url + 'fichaDistribuicao/selAllBacia', data, function(res) {
                if (!res['status']) {
                    disBaciaHidrografica.updateGridBaciaHidrografica();
                    app.reset('frmDisBaciaHidrografica');
                }
            }, null, 'json')
        });
    }
};
// ################################################################################################################################ //
// 2.4 - AMBIENTE RESTRITO                                                                                                          //
// ################################################################################################################################ //
var disAmbienteRestrito = {
    idRestricaoHabitatChange: function(data) {
        if (data && data['sqRestricaoHabitat']) {
            var input = $("#sqRestricaoHabitat");
            if ($(input).find('option:selected').data('codigo') == 'OUTRO') // Outro
            {
                // $("#sqSubRestricaoHabitat").next().addClass('hide');
                $("#deRestricaoHabitatOutro").parent().removeClass('hide');
                app.focus('deRestricaoHabitatOutro');
            } else {
                //$("#sqSubRestricaoHabitat").next().removeClass('hide');
                $("#deRestricaoHabitatOutro").parent().addClass('hide');
            }
        } else {
            $('#sqSubRestricaoHabitat').attr('disabled', true);
        }
    },
    saveFrmDisAmbienteRestrito: function(params) {
        if (!$('#frmDisAmbienteRestrito').valid()) {
            return false;
        }
        var data = $("#frmDisAmbienteRestrito").serializeArray();
        data = app.params2data(params, data)
        if (!data.sqFicha) {
            app.alertError('ID da ficha não encontrado!');
            return;
        }
        if (!data.sqRestricaoHabitat) {
            app.alertError('Ambiente restrito não selecionado!');
            return;
        }
        var input = $("#sqRestricaoHabitat");
        if ($(input).find('option:selected').data('codigo') == 'OUTRO' && !$.trim($("#deRestricaoHabitatOutro").val())) // Outro
        {
            app.alertError('Campo Outro deve ser preenchido!');
            return;
        }
        app.ajax(app.url + 'fichaDistribuicao/saveFrmDisAmbienteRestrito', data, function(res) {
            if (res['status'] === 0) {
                //app.loadModule(app.url+'fichaDistribuicao/tabAmbienteRestrito', data, 'tabDisAmbienteRestrito');
                disAmbienteRestrito.updateGridAmbienteRestrito();
                disAmbienteRestrito.resetFrmDisAmbienteRestrito();
            }
        }, null, 'json')
    },
    editDisAmbienteRestrito: function(params, btn) {
        app.ajax(app.url + 'fichaDistribuicao/editDisAmbienteRestrito', params,
            function(res) {
                $("#btnResetFrmDisAmbienteRestrito").removeClass('hide');
                $("#btnSaveFrmDisAmbienteRestrito").data('value', $("#btnSaveFrmDisAmbienteRestrito").html());
                $("#btnSaveFrmDisAmbienteRestrito").html("Gravar Alteração");
                app.setFormFields(res, 'frmDisAmbienteRestrito');
                app.selectGridRow(btn);
                // abrir o formulário
                if( !$("#frmDisAmbienteRestrito").hasClass('in') )
                {
                    $("#btnShowHidefrmDisAmbienteRestrito").click();
                    app.focus("sqRestricaoHabitat","frmDisAmbienteRestrito");
                }
            });
    },
    deleteDisAmbienteRestrito: function(data) {
        app.confirm('Confirma exclusão <b>' + data.descricao + '</b>?', function() {
            app.ajax(app.url + 'fichaDistribuicao/deleteDisAmbienteRestrito', data, function(res) {
                if (!res['status']) {
                    //app.loadModule(app.url+'fichaDistribuicao/tabAmbienteRestrito', data, 'tabDisAmbienteRestrito');
                    disAmbienteRestrito.updateGridAmbienteRestrito();
                }
            }, null, 'json')
        });
    },
    updateGridAmbienteRestrito: function(params) {
        ficha.getGrid('distAmbienteRestrito');
    },
    resetFrmDisAmbienteRestrito: function(param) {
        app.reset('frmDisAmbienteRestrito');
        $("#sqRestricaoHabitat").focus();
        $("#btnResetFrmDisAmbienteRestrito").addClass('hide');
        $("#btnSaveFrmDisAmbienteRestrito").html($("#btnSaveFrmDisAmbienteRestrito").data('value'));
        app.unselectGridRow('divGridDistAmbienteRestrito');
    },
    openModalSelCaverna: function(data, e) {
        var height = window.innerHeight - 150;
        var width = Math.min(800,$(window).width() - 100);
        var taxon = $("#spanNomeCientificoFicha").html();
        data.height = height;
        data.reload = true;
        data.modalName = 'selecionarCaverna';
        app.openWindow({
            id: 'selecionarCaverna',
            url: app.url + 'ficha/getModal',
            width: width,
            data: data,
            height: height,
            title: 'Selecionar Caverna - CANIE - '+taxon,
            autoOpen: true,
            modal: true
        }, function(data, e) {

        }, function(data, e) {
            disAmbienteRestrito.updateGridAmbienteRestrito();
        });
    },
    excluirCaverna:function(params,btn,evt){
        console.log( params );
        app.confirm('Confirma a exclusão da caverna <b>'+params.descricao+'</b>?', function(){
            var data = {sqFicha:$("#sqFicha").val(),sqFichaAmbRestritoCaverna:params.sqFichaAmbRestritoCaverna};
            app.ajax(baseUrl+'fichaDistribuicao/deleteCaverna',data,function(res){
                if( res.status == 0 ){
                    $("#li-caverna-"+params.sqFichaAmbRestritoCaverna).remove();
                }
            });

        });
    }
};
// ################################################################################################################################ //
// 2.5 - AREAS RELEVANTES                                                                                                           //
// ################################################################################################################################ //
var disAreaRelevante = {
    editDisAreaRelevante: function(params, btn) {
        app.ajax(app.url + 'fichaDistribuicao/editAreaRelevante', params,
            function(res) {
                disAreaRelevante.resetFrmDisAreaRelevante();
                $("#btnResetFrmDisAreaRelevante").removeClass('hide');
                $("#btnSaveFrmDisAreaRelevante").data('value', $("#btnSaveFrmDisAreaRelevante").html());
                $("#btnSaveFrmDisAreaRelevante").html("Gravar Alteração");

                app.setFormFields(res, 'frmDisAreaRelevante');
                app.selectGridRow(btn);
                // abrir o formulário
                if( !$("#frmDisAreaRelevante").hasClass('in') )
                {
                    $("#btnShowHidefrmDisAreaRelevante").click();
                    app.focus("sqTipoRelevancia","frmDisAreaRelevante");
                }
            });
    },
    saveFrmDisAreaRelevante: function(params) {
        if (!$('#frmDisAreaRelevante').valid()) {
            return false;
        }
        var sqUc = $("#frmDisAreaRelevante #sqControle").val();
        if (sqUc) {
            $("#frmDisAreaRelevante #txLocal").val('');
        }
        var data = $("#frmDisAreaRelevante").serializeArray();
        data = app.params2data(params, data);
        if (!data.sqFicha) {
            app.alertError('ID da ficha não encontrado!');
            return;
        }
        app.ajax(app.url + 'fichaDistribuicao/saveFrmDisAreaRelevante', data, function(res) {
            if (!res['status']) {
                disAreaRelevante.resetFrmDisAreaRelevante();
                disAreaRelevante.updateGridDistAreaRelevante();
            }
        }, null, 'json')
    },
    deleteDisAreaRelevante: function(data, btn) {
        app.selectGridRow(btn);
        app.confirm('Confirma exclusão da área relevante <b>' + data.descricao + '</b>?', function() {
            app.ajax(app.url + 'fichaDistribuicao/deleteDisAreaRelevante', data, function(res) {
                if (!res['status']) {
                    disAreaRelevante.updateGridDistAreaRelevante();
                }
            }, null, 'json')
        }, function() {
            app.unselectGridRow(btn);
        });
    },
    resetFrmDisAreaRelevante: function() {
        app.reset('frmDisAreaRelevante');
        $("#btnResetFrmDisAreaRelevante").addClass('hide');
        $("#btnSaveFrmDisAreaRelevante").html($("#btnSaveFrmDisAreaRelevante").data('value'));
        app.unselectGridRow('divGridDistAreaRelevante');
        app.focus("sqTipoRelevancia");
    },
    updateGridDistAreaRelevante: function(params) {
        ficha.getGrid('distAreaRelevante');
    },

    // municipios
    //
    /*
    openModalSelMunicipio: function(data,ele, evt) {


        $('#divGridMunicipiosAreaRelevante').data('sqFichaAreaRelevancia', data.sqFichaAreaRelevancia); // para filtrar o gride
        $('#btnSaveFrmSelMunicipioAreaRelevante').data('sqFichaAreaRelevancia', data.sqFichaAreaRelevancia); // para salvar o municipio
        $('#sqEstado').val(data.sqEstado);
        $('#sqFichaAreaRelevancia').val(data.sqFichaAreaRelevancia); // para salvar o municipio
        $('#txRelevanciaLabel').html(data.txRelevancia);
        $('#noEstadoLabel').html(data.noEstado);
        // adicionar evento ao exibir na janela modal
        $("#modalSelMunicipioAreaRelevante").off('shown.bs.modal').on('shown.bs.modal', function(e) {
            disAreaRelevante.updateGridMunicipios();
        });
        $("#modalSelMunicipioAreaRelevante").off('hidden.bs.modal').on('hidden.bs.modal', function(e) {
            disAreaRelevante.updateGridDistAreaRelevante();
        });
        $("#modalSelMunicipioAreaRelevante").modal('show');
    },
    */

    /*saveFrmMunicipio: function(params) {
        var data = $('#frmSelMunicipio').serializeArray();
        data = app.params2data(params, data);
        if (!data.sqMunicipio) {
            app.alertError('Selecione o município!');
            return;
        }
        app.ajax(app.url + 'fichaDistribuicao/saveFrmMunicipio', data, function(res) {
            if (!res['status']) {
                disAreaRelevante.updateGridMunicipios();
                app.reset('frmSelMunicipio', 'sqEstado,sqFichaAreaRelevancia');
                app.focus('sqMunicipio');
            }
        }, null, 'JSON');
    },
    deleteMunicipio: function(data) {
        app.confirm('Confirma exclusão do municipio <b>' + data.descricao + '</b>?', function() {
            app.ajax(app.url + 'fichaDistribuicao/deleteMunicipioAreaRelevante', data, function(res) {
                if (!res['status']) {
                    disAreaRelevante.updateGridMunicipios();
                }
            }, null, 'json')
        });
    },
    updateGridMunicipios: function(params) {
        ficha.getGrid('municipiosAreaRelevante');
    },
    */
    sqUcFederalChange: function(params) {
        var sqUc = $("#frmDisAreaRelevante #sqControle").val();
        if (sqUc) {
            $("#frmDisAreaRelevante #txLocal").parent().addClass('hidden');
        } else {
            $("#frmDisAreaRelevante #txLocal").parent().removeClass('hidden');
        }
    },
};

// ################################################################################################################################ //
// 2.7 - EOO / AOO
// ################################################################################################################################ //

var disEooAoo = {

    saveFrmDisEooAoo: function(params) {

        var data = $("#frmDisEooAoo").serializeArray();
        data = app.params2data(params, data);
        if (!data.sqFicha) {
            app.alertError('ID da ficha não encontrado!');
            return;
        }
        app.ajax(app.url + 'fichaDistribuicao/saveFrmDisEooAoo', data, function(res) {
            if (res.status==0) {
                // remover fundo vermelho da aba
                app.resetChanges('frmDisEooAoo');
            }
        }, null, 'json')
    },
    resetFrmDisEooAoo:function(){
        app.reset("frmDisEooAoo");
        app.focus('vlEoo');
    }
}



var olApi = {
    addPoint: function(map, layerName, lat, lon, color, radius, content, transparency, borderWidth) {
        color = color || '#39afff';
        radius = radius || 5;
        content = content || '';
        transparency = transparency || 0.8;
        borderWidth = borderWidth || 0.5;
        //var position =  ol.proj.transform( [lon,lat], 'EPSG:4326','EPSG:3857' )
        var position = ol.proj.fromLonLat([lon, lat]);
        var style = new ol.style.Style({
            image: new ol.style.Circle({
                radius: radius,
                stroke: new ol.style.Stroke({
                    color: '#000000',
                    width: borderWidth,
                }),
                fill: new ol.style.Fill({
                    color: hex2rgba(color, String(transparency)),
                })
            })
        });

        var ponto = new ol.Feature({
            geometry: new ol.geom.Point(position),
            text: content,
        });
        ponto.setStyle(style);

        map.getLayers().getArray().forEach(function(e) {
            if (e.get('name') == layerName) {
                e.getSource().addFeature(ponto);
                //map.getView().setCenter(position);
                //map.getView().setResolution(2.388657133911758);// é o mesmo que setZoom(x)
            }
        });
        return ponto;
    },
    addMarker: function(map, layerName, lat, lon, imagePath, content) {
        var objLayer = null;
        var position = ol.proj.fromLonLat([lon, lat]);
        imagePath = (imagePath ? imagePath : window.grails.markerDotOrange);
        map.getLayers().getArray().forEach(function(e) {
            if (e.get('name') == layerName) {
                objLayer = e;
            }
        });
        if (!objLayer) {
            info('Layer ' + layerName + ' não encontrado no mapa!')
            return null;
        }
        // definir o estilo do layer
        var iconStyle = new ol.style.Style({
            image: new ol.style.Icon( /** @type {olx.style.IconOptions} */ ({
                anchor: [16, 32],
                anchorXUnits: 'pixels',
                anchorYUnits: 'pixels',
                opacity: 0.75,
                src: imagePath,
                //anchor: [0.6, 20],
                //anchor: [60,145],
                //img: $('<img src="'+imagePath+'"/>')[0],
                //imgSize:[37,55],
                //size:[37,55],
                //scale:0.35,

            }))
        });
        // criar a feature
        var iconFeature = new ol.Feature({
            geometry: new ol.geom.Point(position),
            text: content,
        });
        iconFeature.setStyle(iconStyle);
        objLayer.getSource().addFeature(iconFeature);
        return iconFeature;
    },

    getLayer: function(map, layerName) {
        var objLayer = null;
        map.getLayers().getArray().forEach(function(e) {
            if (e.get('name') == layerName) {
                objLayer = e;
            }
        });
        return objLayer;
    },
    clearLayer: function(map, layerName) {
        var layer = this.getLayer(map, layerName);
        if (layer) {
            try {
                layer.getSource().clear();
            } catch (e) {}
        }
    },
    geolocation: function() {
        if (!mapGeolocation) {
            mapGeolocation = new ol.Geolocation({
                projection: distribuicao.map.getView().getProjection(),
                tracking: true
            });
        }
        var position = mapGeolocation.getPosition();
        if (position) {
            // para testar vou adicionar um ponto no layer portabio
            var feature = new ol.Feature({
                geometry: new ol.geom.Point(position),
                text: 'Minha localização é <br>'
            })
            distribuicao.layerPortalbio.getSource().addFeature(feature);
            distribuicao.map.getView().setCenter(position);
            //distribuicao.map.updateSize();
        }
        return false;
    },
    testeJsonp: function() {
        var url = 'https://portaldabiodiversidade.icmbio.gov.br/biocache-service/occurrences/search?q=raw_taxon_name:%22Puma%20concolor%22&callback=?';
        var url = 'https://portaldabiodiversidade.icmbio.gov.br/biocache-service/occurrences/search?q=raw_taxon_name:%22Puma%20concolor%22';
        /*
        $.ajax({
            url: 'https://portaldabiodiversidade.icmbio.gov.br/biocache-service/occurrences/search?q=raw_taxon_name:%22Puma%20concolor%22?callback'
            ,dataType: 'jsonp',
            //jsonpCallback: 'parseResponse'
        }).then(function(response) {
            console.log( response)
        });
        */

        /*
        $.ajax({
            url: url,
            contentType : "application/json",
            dataType: 'JSONP',
            crossOrigin: true,
            //jsonpCallback: '',
            type: 'GET',
            success: function (data) {
                console.log(data);
            }
        });
        */

        /*$.jsonp({
            url: url,
            callbackParameter: "callback"
        });
        */
    }
};
// caso o carregamento do mapa tenha falhado, tentar novamente em 4s
window.setTimeout(function() {
    if( ! distribuicao.oMap || ! distribuicao.oMap.map ) {
        $("#mapaDistribuicaoMain").on('click',function(){
            if( distribuicao.oMap ){
                $("#mapaDistribuicaoMain").off('click');
            } else {
                distribuicao.initMap();
                distribuicao.updateGridOcorrencia();
            }
        });
        distribuicao.initMap();
        distribuicao.updateGridOcorrencia();
    } else {
        distribuicao.updateGridOcorrencia();
    }
},4000);
