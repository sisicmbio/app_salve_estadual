<%-- inicio formulario distribuicao Bacia Hidrográfica --%>
<form id="frmDisBaciaHidrografica" class="form-inline" role="form">
   <input name="id" id="id" type="hidden" value="">
   <%-- início campo Bacia Hidrográfica --%>

<div class="fld form-group">
    <label for="" class="control-label">Bacia Hidrográfica</label>
    <a data-action="disBaciaHidrografica.openModalSelBacia"
         class="fld btn btn-default btn-xs btn-grid-subform"
          data-params="sqFicha,modalName:selecionarBacia,contexto:Bacia Hidrográfica"
         title="Selecionar/Excluir Bacia Hidrográfica">
          <span class="glyphicon glyphicon-plus"></span>
    </a>
</div>


%{--    <div class="fld form-group w100p">
      <label for="" class="control-label">Bacia Hidrográfica</label>
      <br>
      <select name="sqBaciaHidrografica" id="sqBaciaHidrografica" multiple="multiple" style="width:95%"
             class="fld form-control select2"
             required="true"
             data-s2-auto-height="true"
             data-s2-multiple="true"
             data-s2-minimum-input-length="0"
             data-s2-maximum-selection-length="0"
             data-msg-required="Campo Obrigatório">
         <g:each in="${listBacias}" var="bacia">
            <option value="${bacia.id}">${bacia.codigo+' - '+bacia.descricao}</option>
            <g:if test="bacia.itens">
               <g:each in="${bacia.itens.sort{it.ordem}}" var="subBacia">
                  <option value="${subBacia.id}">&nbsp;&nbsp;${subBacia.codigo+' - '+bacia.descricao+' / '+subBacia.descricao}</option>
               </g:each>
            </g:if>
         </g:each>
      </select>
      <button data-action="disBaciaHidrografica.selAllBacia" title="Adicionar todas os Bacias Hidrográficas de uma só vez!" data-params="sqFicha" class="btn btn-default btn-sm">Todas</button>
   </div> --}%
%{--    <div class="fld form-group">
      <label for="" class="control-label">Sub-Bacia</label>
      <br>
      <select class="fld form-control fld400 select2" disabled="true" id="subBacia">
         <option value="">-- Selecione --</option>
     </select>
   </div> --}%
   <%-- fim campo select Bacia Hidrográfica  --%>
   <br>
%{--    <div class="panel-footer">
      <button data-action="disBaciaHidrografica.saveFrmDisBaciaHidrografica" data-params="sqBaciaHidrografica,sqFicha" class="fld btn btn-success">Adicionar Bacia Hidrográfica</button>
   </div>
 --}%
   <%-- início divGridDistBaciaHidrografica --%>
   <div id="divGridDistBaciaHidrografica" style="overflow-x: hidden; overflow-y: auto; min-height: 200px;" class="scrollable-area">
    <br>
    <span><b>Carregando gride...</b></span>
      %{-- <g:render template="/ficha/distribuicao/divGridDistBaciaHidrografica"></g:render> --}%
   </div>
   <%-- fim divGridDistBaciaHidrografica--%>
</form>
<%-- fim formulário distribuicao Bacia Hidrográfica --%>
