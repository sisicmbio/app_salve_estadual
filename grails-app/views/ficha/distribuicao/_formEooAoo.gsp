<%-- inicio formulario distribuicao EOO / AOO --%>

<form id="frmDisEooAoo" role="form" class="form-inline">

    <div class="row">
        %{--        EOO--}%
        <div class="col-sm-12 col-md-6">

            <div class="form-group">
                <label class="control-label">Extensão da Ocorrência (EOO)</label>
                <br>
                <div class="input-group">
                    <input name="vlEoo"type="text" ${params.canModify ? '':'disabled'}
                           class="fld form-control number1d fld150" maxlength="13" value="${ficha.vlEoo}"/>
                    <span class="input-group-addon">Km&sup2;</span>
                </div>
            </div>

            <br>

            <div class="form-group w100p">
                <label for="dsJustificativaEoo" class="control-label">Memória de cálculo da EOO
                    <g:if test="${ params.canModify }">
                        <i data-action="ficha.openModalSelRefBibFicha" data-no-tabela="ficha" data-no-coluna="dsJustificativaEoo" data-sq-registro="${ficha.id}" data-de-rotulo="Memória de cálculo da EOO" class="fa fa-book cursor-pointer ml10" title="Adicionar/Remover Refeferência bibliográfica." style="color: green;" />
                    </g:if>
                </label>
                <br>
                <g:if test="${ params.canModify }">
                    <textarea name="dsJustificativaEoo" id="dsJustificativaEoo" rows="5" class="fld form-control fldTextarea wysiwyg w100p">${ficha?.dsJustificativaEoo}</textarea>
                </g:if>
                <g:else>
                    <div class="div-justificativa">${raw(ficha.dsJustificativaEoo)}</div>
                </g:else>
            </div>

        </div>

        %{--        AOO--}%
        <div class="col-sm-12 col-md-6">
            <div class="form-group ml20">
                <label class="control-label">Área de Ocupação (AOO)</label>
                <br>
                <div class="input-group">
                    <input name="vlAoo" type="text" class="fld form-control number1d fld150" ${params.canModify ? '':'disabled'}
                           maxlength="13" value="${ficha.vlAoo}"/>
                    <span class="input-group-addon">km&sup2;</span>
                </div>
            </div>

            <br>

            <div class="form-group w100p">
                <label for="dsJustificativaAoo" class="control-label">Memória de cálculo da AOO
                    <g:if test="${ params.canModify }">
                        <i data-action="ficha.openModalSelRefBibFicha" data-no-tabela="ficha" data-no-coluna="dsJustificativaAoo" data-sq-registro="${ficha.id}" data-de-rotulo="Memória de cálculo da AOO" class="fa fa-book cursor-pointer ml10" title="Adicionar/Remover Refeferência bibliográfica." style="color: green;" />
                    </g:if>
                </label>
                <br>
                <g:if test="${ params.canModify }">
                    <textarea name="dsJustificativaAoo" id="dsJustificativaAoo" rows="5" class="fld form-control fldTextarea wysiwyg w100p">${ficha?.dsJustificativaAoo}</textarea>
                </g:if>
                <g:else>
                    <div class="div-justificativa">${raw(ficha.dsJustificativaAoo)}</div>
                </g:else>
            </div>

        </div>
    </div>

    <%-- fim campo Observacao --%>
    <div class="fld panel-footer mt10">
        <button id="btnSaveFrmDisEooAoo" data-action="disEooAoo.saveFrmDisEooAoo" data-params="sqFicha" class="fld btn btn-success">Gravar</button>
        <button id="btnResetFrmDisEooAoo" data-action="disEooAoo.resetFrmDisEooAoo" class="btn btn-info hide">Limpar</button>
    </div>
</form>
