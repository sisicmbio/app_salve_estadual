<a id="btnShowHidefrmDisAmbienteRestrito"
   data-toggle="collapse"
   data-target="#frmDisAmbienteRestrito"
   onClick="toggleImage(this)"
   title="Clique para abrir ou fechar o formulário."
   class="fld mt10 btn btn-xs btn-default collapsed">Cadastrar ambiente restrito&nbsp;<i class="fa fa-chevron-down"></i></a>

<%-- inicio formulario distribuicao Ambiente Restrito --%>
<form id="frmDisAmbienteRestrito" class="form-inline collapse" role="form">
   <input name="sqFichaAmbienteRestrito" id="sqFichaAmbienteRestrito" type="hidden" value="">
   <%-- início campo Ambiente Restrito --%>
   <div class="fld form-group">
      <label for="" class="control-label">Ambiente Restrito</label>
      <br>
      <select class="fld form-control fld500 select2" required="true" name="sqRestricaoHabitat" id="sqRestricaoHabitat" data-change="disAmbienteRestrito.idRestricaoHabitatChange" data-params="sqRestricaoHabitat,sqFicha"
            data-s2-allow-clear="true"
            data-s2-minimum-input-length="0"
            data-s2-auto-height="true">
         <option value="">-- selecione --</option>
          <g:each var="item" in="${listHabitats}">
              <option ${ ( item.itens.size() == 0 ) ? '':'disabled="true"'} data-codigo="${item.codigoSistema}" value="${item.id}">${item.descricaoCompleta}</option>
              <g:each var="itemFilho" in="${item.itens.sort{it.codigo}}">
                  <option data-codigo="${itemFilho.codigoSistema}" value="${itemFilho.id}">&nbsp;&nbsp;${itemFilho.descricaoCompleta}</option>
                  <g:each var="itemNeto" in="${itemFilho.itens.sort{it.codigo}}">
                      <option data-codigo="${itemNeto.codigoSistema}" value="${itemNeto.id}">&nbsp;&nbsp;&nbsp;&nbsp;${itemNeto.descricaoCompleta}</option>
                  </g:each>
              </g:each>
          </g:each>
      </select>
    %{----}%
      %{---&nbsp;<select class="fld form-control fld400 select2" disabled="true" name="sqSubRestricaoHabitat" id="sqSubRestricaoHabitat"--}%
            %{--data-s2-minimum-input-length="0"--}%
            %{--data-s2-allow-clear="false"--}%
            %{--data-s2-auto-height="true">--}%
         %{--<option value="">-- selecione --</option>--}%
      %{--</select>--}%
   </div>
   <div class="fld form-group hide">
      <label for="" class="control-label">Outro</label><br>
      <input type="text" name="deRestricaoHabitatOutro" id="deRestricaoHabitatOutro" maxlength="255" class="form-control fld300">
   </div>
   <br>
   <%-- inicio campo Observacao --%>
   <div class="form-group w100p">
      <label for="" class="control-label">Observação</label>
      <br>
      <textarea class="fld form-control fldTextarea wysiwyg w100p" rows="7" name="txFichaRestricaoHabitat" id="txFichaRestricaoHabitat"></textarea>
   </div>
   <%-- fim campo Observacao --%>

   <div class="panel-footer">
      <button id="btnSaveFrmDisAmbienteRestrito" data-action="disAmbienteRestrito.saveFrmDisAmbienteRestrito" data-params="sqFicha" class="fld btn btn-success">Adicionar Ambiente Restrito</button>
      <button id="btnResetFrmDisAmbienteRestrito" data-action="disAmbienteRestrito.resetFrmDisAmbienteRestrito" class="fld btn btn-info hide">Limpar Formulário</button>
   </div>
   <br>
</form>
<%-- fim formulário distribuicao Ambiente Restrito --%>

<%-- início divGridDistAmbienteRestrito --%>
<div id="divGridDistAmbienteRestrito" style="overflow-xx: hidden; overflow-y: auto; max-hexight: 140px;" class="scrollable-area mt10">
    %{-- <g:render template="/ficha/distribuicao/divGridDistAmbienteRestrito"></g:render> --}%
</div>
<%-- fim divGridDistAmbiente Restrito --%>

