<!-- view: /views/ficha/distribuicao/_divGridOcorrencia.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<table id="table${gridId}" cellpadding="0" cellspacing="0" class="table table-striped table-sorting">
    <thead>
    %{-- PAGINACAO --}%

    <tr id="tr${gridId}Pagination">
        <td colspan="18">
            <div class="text-left" style="min-height:0;height: auto;overflow: hidden"
                 id="div${(gridId ?: pagination?.gridId)}PaginationContent">
                <g:if test="${ pagination ?.totalRecords}">
                    <g:gridPagination pagination="${pagination}"/>
                </g:if>
            </div>
        </td>
    </tr>

    %{-- FILTROS --}%
    <tr id="tr${gridId}Filters" class="table-filters" data-grid-id="${gridId}">
        <td colspan="2">
            <div style="display:flex;flex-direction:row; justify-content:space-between;">
                <i class="fa fa-question-circle"
                   title="Utilize os campos ao lado para filtragem dos registros.<br> Informe o valor a ser localizado e pressione <span class='blue'>Enter</span>.<br>Para remover o filtro limpe o campo e pressione <span class='blue'>Enter</span> novamente."></i>
                <i data-grid-id="${gridId}" class="fa fa-eraser" title="Limpar filtro(s)"></i>
                <i data-grid-id="${gridId}" class="fa fa-filter" title="Aplicar filtro(s)"></i>
            </div>
        </td>

%{--        <g:if test="${hasSubespecies}">--}%
            <td></td>
%{--        </g:if>--}%

        %{-- FILTRO PELO BANCO DE DADOS--}%
        <td title="Informe o nome ou parte do nome do banco de dados a ser pesquisado e pressione ENTER">
            <input type="text" name="fldBd${gridId}" value="${params?.bd ?: ''}"
                   class="form-control table-filter ignoreChange" placeholder="" data-field="bdFiltro">
        </td>

        %{-- FILTRO PELA LATITUDE E LONGITUDE --}%
        <td title="Informe a LATITUDE ou a LONGITUDE do ponto a ser pesquisado e pressione ENTER. Ex: -27.023054">
            <input type="text" name="fldTxCoordenada${gridId}" value="${params?.txCoordenada ?: ''}"
                   class="form-control table-filter ignoreChange" placeholder="" data-field="txCoordenadaFiltro">
        </td>

        %{--DATA REGISTRO--}%
        <td title="Informe a data, mes/ano ou o somente o ano para filtrar os registros e pressione ENTER">
            <input type="text" name="fldDtOrdenar${gridId}" value="${params?.dtOrdenar ?: ''}"
                   class="form-control table-filter ignoreChange" placeholder="" data-field="dtOrdenarFiltro">
        </td>
        <td></td> %{--CARENCIA--}%

        %{-- FILTRO PELA LOCALIADADE --}%
        <td title="Informe o nome ou parte do nome da localizade a ser pesquisada e pressione ENTER">
            <input type="text" name="fldNoLocalidade${gridId}" value="${params?.noLocalidade ?: ''}"
                   class="form-control table-filter ignoreChange" placeholder="" data-field="noLocalidadeFiltro">
        </td>

        %{-- FILTRO PELO ESTADO --}%
        <td title="Informe a sigla do Estado e pressione ENTER">
            <input type="text" name="fldSgEstado${gridId}" value="${params?.sgEstado ?: ''}"
                   class="form-control table-filter ignoreChange" placeholder="" data-field="sgEstadoFiltro">
        </td>

        %{-- FILTRO PELO MUNICIPIO --}%
        <td title="Informe o nome ou parte do nome do município e pressione ENTER">
            <input type="text" name="fldNoMunicipio${gridId}" value="${params?.noMunicipio ?: ''}"
                   class="form-control table-filter ignoreChange" placeholder="" data-field="noMunicipioFiltro">
        </td>

        <td></td>

        %{-- FILTRO PELA REF BIB --}%
        <td title="Informe o texto a ser pesquisado e pressione ENTER">
            <input type="text" name="fldRefBib${gridId}" value="${params?.refBib ?: ''}"
                   class="form-control table-filter ignoreChange" placeholder="" data-field="refBibFiltro">
        </td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>

    %{--    TITULOS--}%
    <tr id="tr${gridId}Columns" data-grid-id="${gridId}">
        %{-- NUMERACAO DE LINHA--}%
        <th style="width:40px;">#</th>

        %{-- MARCAR/DESMARCAR LINHA--}%
        <th style="width:50px;"><i title="Marcar/Desmarcar Todos"
             data-action="ficha.selecionarTodas"
             data-table-id="table${gridId}"
             class="glyphicon glyphicon-unchecked"></i>
        </th>

%{--            <g:if test="${hasSubespecies}">--}%
                <th style="width:150px;">Subespecie</th>
%{--            </g:if>--}%


        <th style="width:150px;" class="sorting sorting_desc" data-field-name="bd">Base Dados</th>
        <th style="width:180px;" class="sorting"  data-field-name="nu_y">Coordenadas</th>
        <th style="width:160px;" class="sorting" data-field-name="dt_ordenar">Registrado em</th>

        <th style="width:130px;" class="sorting"  data-field-name="dt_carencia">Carência</th>
        <th style="min-width:150px;max-width:350px;width:auto;" class="sorting"  data-field-name="no_localidade">Localidade</th>
        <th style="width:60px;" class="sorting"  data-field-name="no_estado">UF</th>
        <th style="width:120px;" class="sorting"  data-field-name="no_municipio">Município</th>
        <th style="width:100px;" class="sorting"  data-field-name="ds_presenca_atual">Presença<br>Atual?</th>
        <th style="min-width:200px;max-width:350px;width:auto;">Ref.<br>Bibliográfica</th>
        <th style="width:100px;" class="sorting"  data-field-name="dt_alteracao">Última<br>Alteração</th>
        <th style="min-width:200px;max-width:350px;width:auto;">Autor</th>
        <th style="min-width:200px;max-width:350px;width:auto;">Projeto</th>
        <th style="width:90px;" class="">Ação</th>
    </tr>
    </thead> %{-- PARAMETRO PARA CRIAÇÃO DO MENU DE CONTEXTO AO CLICAR COM O BOTÃO DIREITO DO MOUSE SEBRE O GRIDE--}%
    <tbody data-contexto="containerGridOcorrencia" id="tableGridOcorrenciaBody">
    <g:if test="${registros && registros.size() > 0}">
        <g:each in="${registros}" var="registro" status="i">
            <tr id="tr-${registro.rowId}" data-id="${registro.id}" data-bd="${registro.no_bd}">

                %{-- COLUNA NUMERAÇÃO --}%
                <td class="text-center"
                    id="tdFichaDist${registro.rowId}">
                    ${ i + (pagination ? pagination?.rowNum.toInteger() : 1 )  }
                </td>

                %{--  COLUNA CHECKBOX--}%
                <td class="text-center">
                    <g:if test="${ ! session.sicae.user.isCO() }">
                    <input name="chkSqOcorrencia"
                           data-contexto="divGridOcorrencia"
                           data-line="${i + 1}"
                           data-bd="${registro.no_bd}"
                           data-id="${registro.id}"
                           onchange="chkGridOcorrenciaChange(this)"
                           value="${registro.id}"
                           type="checkbox"
                           class="fld checkbox-lg"/>
                    </g:if>
                </td>
%{--                <g:if test="${hasSubespecies}">--}%
                    <td class="text-center ${registro.subespecie ? 'td-subespecie' : ''}">${registro.subespecie ? raw(registro.noCientificoCompletoItalico) : ''}</td>
%{--                </g:if>--}%

            %{--BASE DE DADOS --}%
                <td class="text-center">${raw(registro.no_fonte)}</td>

                %{--COORDENADAS--}%
                <td>
                    ${raw(registro.coordenadas)}
                    <g:if test="${registro.precisaoCoord}">
                        <br>
                        <span>${registro.precisaoCoord}</span>
                    </g:if>
                    %{--<g:if test="${registro.dataHora}">
                        <br>
                        <span>Data/ano:${registro.dataHora}</span>
                    </g:if>--}%

                    %{-- SENSIVEL --}%
                    <br>
                    <span>Sensível: ${registro.isSensivel ? 'Sim' : 'Não'}</span>


                    <br>
                    <g:if test="${registro.cdSituacaoAvaliacao != 'REGISTRO_EXCLUIDO'}">
                        <span class="${registro.corSituacaoAvaliacao}">${registro.dsSituacaoAvaliacao}</span>
                        <g:if test="${registro?.in_presenca_atual=='N'}">
                            <span class="red">&nbsp;(histórico)</span>

                        </g:if>
                    </g:if>
                    <g:if test="${registro.txJustificativaNaoUtilizado}">
                        <g:if test="${registro.dsMotivoNaoUtilizado}">
                            <div style="background-color:#fff2f2;border:1px solid #e8dfdf;height:auto;max-height: 300px;overflow: auto;line-height: 15px;font-size:12px !important;">
                                <span>Motivo: ${registro.dsMotivoNaoUtilizado}</span><br>
                                ${raw(registro.txJustificativaNaoUtilizado)}
                            </div>
                        </g:if>
                    </g:if>

                    <g:if test="${registro.idOrigem}">
                        <span style="display:block;color:#9d9d9d;font-size:10px !important;">Id Origem: ${registro.idOrigem}</span>
                    </g:if>

                </td>


                %{-- DATA OCORRENCIA --}%
                <td class="text-center">${raw(registro.dataHora)}</td>

                %{-- CARENCIA --}%
                <td class="text-center">${raw(registro.carencia)}</td>

                %{-- LOCALIDADE  --}%
                <td>${registro.localidade}</td>

                %{-- ESTADO --}%
                <td class="text-center">${registro.estado}</td>

                %{-- MUNICIPIO --}%
                <td class="text-center">${registro.municipio}</td>

                %{-- PRESENÇA ATUAL --}%
                <td class="text-center">${registro.presencaAtual}</td>

                %{-- REF BIB --}%
                <td>
                <g:if test="${registro.no_bd == 'salve'}">
                    <g:if test="${ registro.canEdit }">
                        <label class="control-label cursor-pointer">
                            <i data-action="ficha.openModalSelRefBibFicha"
                               data-no-tabela="ficha_ocorrencia"
                               data-no-coluna="sq_ficha_ocorrencia"
                               data-sq-registro="${registro.id}"
                               data-com-pessoal="true"
                                data-is-com-pessoal="${ (registro?.refBib =~ /com\. ?pess/) ? 'true':'false'}"
                               data-update-grid=""
                               data-on-close="distribuicao.updateGridOcorrencia"
                               data-de-rotulo="Ocorrência"
                               class="fld fa fa-book ml10" title="Adicionar/Remover Referência Bibliográfica"
                               style="color: green;"/>
                        </label>
                        <br/>
                    </g:if>
                    ${raw(registro.refBib)}</td>
                </g:if>
                <g:elseif test="${registro.cdSituacaoAvaliacao == 'REGISTRO_UTILIZADO_AVALIACAO'}">
                    <g:if test="${ registro.canEdit }">
                        <label class="control-label cursor-pointer">
                            <i data-action="ficha.openModalSelRefBibFicha"
                               data-no-tabela="ficha_ocorrencia_portalbio"
                               data-no-coluna="sq_ficha_ocorrencia_portalbio"
                               data-sq-registro="${registro.id}"
                               data-com-pessoal="true"
                               data-update-grid="divGridOcorrencia"
                               data-de-rotulo="Ocorrência"
                               class="fld fa fa-book ml10" title="Adicionar/Remover Referência Bibliográfica"
                               style="color: green;"/>
                        </label>
                        <br/>
                    </g:if>
                    ${raw(registro.refBib)}</td>
                </g:elseif>



            %{-- ALTERADO EM / POR --}%
                <td class="text-center">
                    ${raw(registro.log)}
                </td>

            %{-- AUTOR SISBIO --}%
            <td>${raw(registro?.no_autor)}</td>

            %{-- PROJETO SISBIO --}%
            <td>${raw(registro?.no_projeto)}</td>

                %{-- AÇÕES --}%
                <td class="td-actions">
                    <g:if test="${registro.no_bd == 'salve' && registro.canEdit}">
                        <a class="btn btn-default btn-xs btn-edit" title="Editar"
                           data-action="distribuicao.editarOcorrencia"
                           data-params="sqFicha,sqFichaOcorrencia:${registro.id}"><span
                            class="glyphicon glyphicon-pencil"></span></a>
                    %{--<a class="fld btn btn-default btn-xs btn-delete" title="Excluir" data-action="ocorrencia.deleteDisOcorrencia" data-params="sqFichaOcorrencia:${registro.id},sqFicha"><span class="glyphicon glyphicon-remove"></span></a>--}%
                    </g:if>
                </td>
            </tr>
        </g:each>
    </g:if>
    <g:else>
        <td colspan="18">
            <h3>Nenhum registro cadastrado!</h3>
        </td>
    </g:else>
    </tbody>
</table>
