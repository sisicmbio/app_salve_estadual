<!-- view: /views/ficha/distribuicao/_divGridDistBaciaHidrografica.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<g:if test="${listBaciasFicha}">
    <div style="height:auto;max-height: 400px;overflow: auto">
        <table class="table table-striped table-bordered table-hover" id="disTableFichaBaciasFicha">
            <thead>
            <tr>
                <th style="width:600px">Bacia Hidrográfica</th>
                <th style="width:auto;">Referência(s) Bibliográfica(s)</th>
                %{-- <th>Ação</th> --}%
            </tr>
            </thead>
            <tbody>
            <g:if test="${listBaciasFicha}">

                <g:set var="quebra" value="*"></g:set>

                <g:each in="${listBaciasFicha}" var="item">

                    <g:set var="matcher" value="${ ( item.tx_trilha =~ /.*\|/ ) }"></g:set>
                    <g:if test="${matcher.size() && matcher[0] != quebra }">
                        <g:set var="quebra" value="${matcher[0]}"></g:set>
                        <tr class="info table-topic">
                            <td colspan="2">${quebra.replaceAll(/\|/,'')}</td>
                        </tr>
                    </g:if>

                    <tr>
                        <td class="table-subtopic">
                            ${raw( item.cd_bacia+' - '+item.no_bacia)}
                            <g:if test="${!isSubespecie}">
                                ${ raw( item.co_nivel_taxonomico == 'SUBESPECIE' ? '<i title="Bacia da subespécie" class="ml10 tooltipster glyphicon glyphicon-info-sign"></i>' : '')}
                            </g:if>
                        </td>

                        <td>
                            <i data-action="ficha.openModalSelRefBibFicha"
                               data-no-tabela="ficha_bacia"
                               data-no-coluna="sq_ficha_bacia"
                               data-sq-registro="${item?.sq_ficha_bacia}"
                               data-sq-ficha="${params.sqFicha}"
                               data-de-rotulo="Bacia Hidrográfica - ${item.no_bacia} "
                               data-com-pessoal="true"
                               data-update-grid="divGridDistBaciaHidrografica"
                               title="Adicionar/Remover Refeferência bibliográfica." class="fld fa fa-book tooltipstered"></i>
                               ${raw( br.gov.icmbio.Util.parseRefBib2Grid( item?.json_ref_bibs ) ) }
                        </td>
                 </tr>
                </g:each>
            </g:if>
            <g:else>
                <tr>
                    <td class="text-center" colspan="3">nenhuma bacia cadastrada</td>
                </tr>
            </g:else>
            </tbody>
        </table>
    </div>
    <br>
</g:if>


<g:if test="${listBaciasRegistros}">
    <div style="height:auto;max-height: 400px;overflow: auto">
        <table class="table table-striped table-bordered table-hover"  id="disTableFichaBaciasOcorrencia">
            <thead>
            <tr>
                <th style="width:600px">Bacia Hidrográfica (ocorrências)</th>
                <th style="width:auto;">Referência(s) Bibliográfica(s)</th>
                %{-- <th>Ação</th> --}%
            </tr>
            </thead>
            <tbody>
            <g:if test="${listBaciasRegistros}">
                <g:set var="quebra" value="*"></g:set>
                <g:each in="${listBaciasRegistros}" var="item">
                    <g:set var="matcher" value="${ ( item.tx_trilha =~ /.*\|/ ) }"></g:set>
                    <g:if test="${matcher.size() && matcher[0] != quebra }">
                        <g:set var="quebra" value="${matcher[0]}"></g:set>
                        <tr class="info table-topic">
                            <td colspan="2">${quebra.replaceAll(/\|/,'')}</td>
                        </tr>
                    </g:if>
                    <tr>
                        <td class="table-subtopic">
                            ${raw( item.cd_bacia+' - '+item.no_bacia)}
                            <g:if test="${!isSubespecie}">
                                ${ raw( item.co_nivel_taxonomico == 'SUBESPECIE' ? '<i title="Bacia da subespécie" class="ml10 tooltipster glyphicon glyphicon-info-sign"></i>' : '')}
                            </g:if>
                        </td>
                        <td>
                            ${raw( br.gov.icmbio.Util.parseRefBib2Grid( item.json_ref_bibs ) ) }
                        </td>
                    </tr>
                </g:each>
            </g:if>
            <g:else>
                <tr>
                    <td class="text-center" colspan="3">nenhuma bacia encontra nos registros</td>
                </tr>
            </g:else>
            </tbody>
        </table>
    </div>
</g:if>
<g:if test="${ !listBaciasFicha && !listBaciasRegistros}">
    <div class="alert alert-info">
        <h4>Nenhuma Bacia ou registro de ocorrência cadastrado.</h4>
    </div>
</g:if>
