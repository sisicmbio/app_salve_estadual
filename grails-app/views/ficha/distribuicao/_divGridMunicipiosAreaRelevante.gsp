<table class="table table-striped table-bordered table-hover">
   <thead>
      <tr>
         <th>Município</th>
         <th>Observação</th>
         <th>Ação</th>
      </tr>
   </thead>
   <tbody>
      <g:each in="${listMunicipios}" var="item">
         <tr>
            <td>${item.municipio.noMunicipio}</td>
            <td>${raw(item.txRelevanciaMunicipio)}</td>
            <td class="td-actions">
               <a data-id="" class="btn btn-default btn-xs btn-delete" data-action="cadMunicipioFicha.delete" data-params="sqFichaAreaRelevanciaMunicipio:${item?.id},descricao:${item.municipio.noMunicipio}" title="Excluir">
                  <span class="glyphicon glyphicon-remove"></span>
               </a>
            </td>
         </tr>
      </g:each>
   </tbody>
</table>