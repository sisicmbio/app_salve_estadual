<%-- inicio formulario distribuicao bioma --%>
<form id="frmDisBioma" class="form-inline" role="form">
   <input name="id" id="id" type="hidden" value="">
   <%-- início campo bioma --%>

<div class="fld form-group">
    <label for="" class="control-label">Bioma</label>
    <a data-action="disBioma.openModalSelBioma"
         class="fld btn btn-default btn-xs btn-grid-subform"
          data-params="sqFicha,modalName:selecionarBioma,contexto:Biomas"
         title="Selecionar/Excluir Bioma">
          <span class="glyphicon glyphicon-plus"></span>
    </a>
</div>


%{--    <div class="fld form-group w100p">
      <label for="" class="control-label">Bioma</label>
      <br>
      <select name="sqBioma" id="sqBioma" multiple="multiple" style="width:95%"
             class="fld form-control select2"
             required="true"
             data-s2-auto-height="true"
             data-s2-multiple="true"
             data-s2-minimum-input-length="0"
             data-s2-maximum-selection-length="1000"
             data-msg-required="Campo Obrigatório">
         <g:each in="${biomas}" var="bioma">
            <option value="${bioma.id}">${bioma.descricao}</option>
         </g:each>
      </select>
      <button data-action="disBioma.selAllBioma" title="Adicionar todos os biomas de uma só vez!" data-params="sqFicha" class="btn btn-default btn-sm">Todos</button>
   </div>
   <br>
 --}%
   <%-- fim campo select bioma  --%>
%{--    <div class="fld panel-footer">
      <button data-action="disBioma.saveFrmDisBioma" data-params="sqFicha" class="fld btn btn-success">Adicionar Bioma</button>
   </div>
 --}%
   <br>
   <%-- início divGridDistBioma        --%>
   <div id="divGridDistBioma" style="overflow-x: hidden; overflow-y: auto; min-height: 200px;" class="scrollable-area">
    <br>
    <span><b>carregando...</b></span>
      %{-- <g:render template="/ficha/distribuicao/divGridDistBioma"></g:render> --}%
   </div>
%{--    <div class="end-page"--}%
   <%-- fim divGridDistBioma--%>
</form>
<%-- fim formulário distribuicao Bioma --%>
