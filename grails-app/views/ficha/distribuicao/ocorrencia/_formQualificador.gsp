<form id="frmDisOcoQualificador" class="form-inline" role="form">
   <div class="form-group">
      <label class="control-label">Identificador do Táxon</label>
      <br>
      <input name="deIdentificador" type="text" class="fld form-control fld400" value="${fichaOcorrencia.deIdentificador}"/>
   </div>
   <div class="form-group">
      <label class="control-label">Data da Identificação</label>
      <br>
      <input name="dtIdentificacao" type="text" class="fld form-control date" value="<g:formatDate format='dd/MM/yyyy' date='${fichaOcorrencia?.dtIdentificacao}'/>" />
   </div>
   <div class="form-group">
      <label class="control-label">Identificação Incerta ou Dúbia</label>
      <br>
      <select name="sqQualificadorValTaxon" class="fld form-control fld300 fldSelect">
         <option value="">?</option>
         <g:each var="item" in="${listQualificadores}">
            <option value="${item.id}" ${item.id==fichaOcorrencia?.qualificadorValTaxon?.id ? ' selected' : ''}>${item.descricao}</option>
         </g:each>
      </select>
   </div>
    <br />
   <div class="form-group">
      <label class="control-label">Tombamento</label>
      <br>
      <input name="txTombamento" type="text" class="fld form-control fld200" value="${fichaOcorrencia.txTombamento}" />
   </div>

   <div class="form-group">
      <label class="control-label">Instituição do Tombamento</label>
      <br>
      <input name="txInstituicao" type="text" class="fld form-control fld400" value="${fichaOcorrencia.txInstituicao}" />
   </div>
   <div class="panel-footer">
      <button data-action="tabQualificador.save" data-params="sqFichaOcorrencia" class="fld btn btn-success">Gravar Qualificador</button>
   </div>
</form>