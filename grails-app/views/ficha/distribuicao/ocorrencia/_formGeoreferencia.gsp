<!-- view: /views/ficha/distribuicao/ocorrencia/_formGeoreferencia.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<g:set var="oSND"  value="${[ [key:'S',value:'Sim'], [key:'N', value:'Não'] ,[key:'D', value:'Desconhecido'] ]}"/>
<form id="frmDisOcoGeoreferencia" class="form-inline" role="form">
   <div class="col-sm-6">
       %{-- LOCALIDADE DESCONHECIDA--}%
       <div class="form-group" style="display: block;border-bottom:1px solid silver;" id="divLocalidadeDesconhecida">
           <label for="stLocalidadeDesconhecida" class="control-label cursor-pointer">Localidade desconhecida
               <input name="stLocalidadeDesconhecida" id="stLocalidadeDesconhecida" value="S" type="checkbox"
                      ${stLocalidadeDesconhecida=='S' ? 'checked':''}
                      class="fld checkbox-lg"/>
           </label>
           <button type="button" id="btnSaveLocalidadeDesconhecida"
                   data-action="tabGeoreferencia.saveLocalidadeDesconhecida"
                   data-params="sqFicha"
                   class="fld btn btn-default btn-xs" title="Atualizar a localidade desconhecida S/N"><i class="ml5 mr5 fa fa-save"></i>Gravar</button>

       </div>

       <div class="form-group ${nuEstados > 0 ? 'hidden':''}" style="display: block;border-bottom:1px solid silver;" id="divSemUf">
           <label for="stSemUf" class="control-label cursor-pointer"
                  title="Utilize este campo para informar que, apesar de existir registros de ocorrência, a espécie não possui uma unidade da federação">Sem unidade da federação
               <input name="stSemUf" id="stSemUf" value="S" type="checkbox"
                      ${stSemUf=='S' ? 'checked':''}
                      class="fld checkbox-lg"/>
           </label>
           <button type="button" id="btnSaveSemUf"
                   data-action="tabGeoreferencia.saveSemUf"
                   data-params="sqFicha"
                   class="fld btn btn-default btn-xs" title="Atualizar sem unidade da federação S/N"><i class="ml5 mr5 fa fa-save"></i>Gravar</button>

       </div>
       %{--<hr class="hr-separador" style="width: 100%;border-top:1px solid blue;">
       <br>
--}%
       <br>
      <div class="form-group" style="margin-top:20px;">
          <label class="control-label">Referência bibliográfica
             <i data-action="ficha.openModalSelRefBibTemp"
                data-no-tabela="ficha_ocorrencia"
                data-no-coluna="sq_ficha_ocorrencia"
                data-sq-registro="${fichaOcorrencia?.id}"
                data-com-pessoal="true"
                data-update-grid="divGridDistOcoRefBib"
                data-de-rotulo="Ocorrência" class="fld fa fa-book ml10" title="clique para selecionar ref. bibliográfica/comunicação pessoal" style="color: green;"></i>
         </label>
          <input type="hidden" id="sqPublicacaoTemp" name="sqPublicacaoTemp" value="${fichaRefBib?.publicacao?.id}"/>
          <input type="hidden" id="noAutorRefTemp" name="noAutorRefTemp" value="${ fichaRefBib?.publicacao ? fichaRefBib?.publicacao.noAutor : fichaRefBib?.noAutor}"/>
          <input type="hidden" id="nuAnoRefTemp" name="nuAnoRefTemp" value="${ fichaRefBib?.publicacao ? fichaRefBib?.publicacao?.nuAnoPublicacao : fichaRefBib?.nuAno}"/>
          <br/>
            <i data-action="ficha.openModalSelRefBibTemp" class="fld"
            data-no-tabela="ficha_ocorrencia"
            data-no-coluna="sq_ficha_ocorrencia"
            data-sq-registro="${fichaOcorrencia?.id}"
            data-com-pessoal="true"
            data-update-grid="divGridDistOcoRefBib"
            data-de-rotulo="Ocorrência" title="clique para selecionar ref. bibliográfica/comunicação pessoal">
               <g:if test="${ fichaRefBib?.publicacao }">
                  <p id="deTituloPublicacaoTemp" class="fld tag form-control-static cursor-pointer">${ raw( fichaRefBib.publicacao.deTitulo ) }</p>
               </g:if>
               <g:elseif test="${fichaRefBib?.noAutor}">
                  <p id="deTituloPublicacaoTemp" class="fld tag form-control-static cursor-pointer">${ raw( fichaRefBib.referenciaHtml ) }</p>
               </g:elseif>
               <g:else>
                  <p id="deTituloPublicacaoTemp" class="fld tag form-control-static cursor-pointer">?</p>
               </g:else>
         </i>
      </div>

      <br>

      <fieldset style="background-color: #f0f8ff;border-bottom: 1px solid #98d7bd;">
           <legend>Tipo, Data e Hora</legend>
           <div class="fld form-group">
               <label class="control-label">Tipo do Registro</label>
               <br>
               <select  class="fld form-control fld260 fldSelect"
                    <g:if test="${ ! fichaOcorrencia?.id || fichaOcorrencia.nuAnoRegistro }">
                        required="true"
                    </g:if>
                    name="sqTipoRegistro" id="sqTipoRegistro">
                   <option value="">-- selecione --</option>
                   <g:each in="${tipoRegistro}" var="item">
                       <g:if test="${fichaOcorrencia?.tipoRegistro?.id == item?.id}">
                           <option value="${item?.id}" selected
                                   title="${item.codigoSistema == 'SENSOR_AUTONOMO_REGISTRO' ? 'Armadilhas/gravadores de foto/vídeo/som':''}">${item.descricao}</option>
                       </g:if>
                       <g:else>
                           <option value="${item?.id}"
                                   title="${item.codigoSistema == 'SENSOR_AUTONOMO_REGISTRO' ? 'Armadilhas/gravadores de foto/vídeo/som':''}">${item.descricao}</option>
                       </g:else>
                   </g:each>
               </select>
           </div>
           <br>
           %{--      DIA--}%
           <div class="form-group">
               <label for="nuDiaRegistro"
                      title="Informar a data ou período em que o registro foi realizado. Essa data pode ser diferente da data de publicação. É obrigatório informar pelo menos o ano da data do registro."
                      class="control-label">Dia</label>
               <br>
               <input type="text" name="nuDiaRegistro" id="nuDiaRegistro" class="fld form-control fld50" data-rule-range="[1,31]" data-mask="00"  maxlength="2" value="${fichaOcorrencia?.nuDiaRegistro}"/>
           </div>
           %{--      MES--}%
           <div class="form-group">
               <label class="control-label">Mês</label>
               <br>
               <input type="text" name="nuMesRegistro" id="nuMesRegistro" class="fld form-control fld50" data-rule-range="[1,12]" data-mask="00"  maxlength="2" value="${fichaOcorrencia?.nuMesRegistro}"/>
           </div>
           %{--      ANO--}%
           <div class="form-group">
               <label class="control-label">Ano</label>
               <br>
               <input type="text"
                      name="nuAnoRegistro" id="nuAnoRegistro"
                      class="fld form-control fld60"
                      data-mask="0000"
                      maxlength="4" data-rule-min="1500"
                       <g:if test="${ ! fichaOcorrencia?.id || fichaOcorrencia.nuAnoRegistro }">
                            required="true"
                       </g:if>
                      value="${fichaOcorrencia?.nuAnoRegistro}"/>
           </div>
           %{--     HORA--}%
           <div class="form-group">
               <label class="control-label">Hora</label>
               <br>
               <input type="text" name="hrRegistro" id="hrRegistro" class="fld form-control time fld100" value="${fichaOcorrencia?.hrRegistro}"/>
           </div>
           <div class="form-group">
               <label style="padding: 0px 10px;margin-top: 32px;">até</label>
           </div>
           %{--      ATE DIA--}%
           <div class="form-group">
               <label class="control-label">Dia</label>
               <br>
               <input type="text" name="nuDiaRegistroFim" id="nuDiaRegistroFim" class="fld form-control fld50" data-rule-range="[1,31]" data-mask="00"  maxlength="2" value="${fichaOcorrencia?.nuDiaRegistroFim}"/>
           </div>
           %{--      ATE MES--}%
           <div class="form-group">
               <label class="control-label">Mês</label>
               <br>
               <input type="text" name="nuMesRegistroFim" id="nuMesRegistroFim" class="fld form-control fld50" data-rule-range="[1,12]" data-mask="00"  maxlength="2" value="${fichaOcorrencia?.nuMesRegistroFim}"/>
           </div>
           %{--      ATE ANO--}%
           <div class="form-group">
               <label class="control-label">Ano</label>
               <br>
               <input type="text" name="nuAnoRegistroFim" id="nuAnoRegistroFim" class="fld form-control fld60" data-mask="0000"  maxlength="4" data-rule-min="1500" value="${fichaOcorrencia?.nuAnoRegistroFim}"/>
           </div>

           %{--DATA DESCONHECIDA--}%
           <div class="form-group">
               <label for="inDataDesconhecida" class="control-label cursor-pointer">Data e Hora/Período Desconhecidos
                   <input name="inDataDesconhecida"  id="inDataDesconhecida" value="S" data-default-checked="${ (fichaOcorrencia?.inDataDesconhecida=='S' ) ? true : false }"
                          type="checkbox" class="fld checkbox-lg"
                          data-action="disOcoDadosRegistro.inDataDesconhecidaChange"
                        ${fichaOcorrencia?.inDataDesconhecida=='S' ? ' checked':''}/>
               </label>
           </div>
            %{-- <hr style="margin:6px;"> --}%
       </fieldset>
%{--      FIM DATA DA OCORRENCIA--}%
      <div class="fld form-group">
               %{-- campos para armazenar o estado e municipio da uc localizado pelo centroide --}%
               <input type="hidden" name="sqPaisUc" id="sqPaisUc" value="${fichaOcorrencia?.pais?.id}">
               <input type="hidden" name="sqEstadoUc" id="sqEstadoUc" value="${fichaOcorrencia?.estado?.id}">
               <input type="hidden" name="sqMunicipioUc" id="sqMunicipioUc" value="${fichaOcorrencia?.municipio?.id}">
               <input type="hidden" name="noMunicipioUc" id="noMunicipioUc" value="${fichaOcorrencia?.municipio?.noMunicipio}">
               <input type="hidden" name="sqBiomaUc" id="sqBiomaUc" value="${fichaOcorrencia?.bioma?.id}">
               <input type="hidden" name="noBiomaUc" id="noBiomaUc" value="${fichaOcorrencia?.bioma?.descricao}">
               <input type="hidden" name="isCentroide" id="isCentroide" value="N">
              <label for="sqControle"
                     class="control-label"
                     title="Cadastre apenas Unidades com ocorrência comprovada da espécie, e sempre indique a referência bibliográfica, podendo ser “comunicação pessoal”.<br>Caso o ponto exato de coleta não seja conhecido, pode ser usado a coordenada do centróide ou uma coordenada genérica dentro da Unidade.<br>O campo Presença Atual na Coordenada (aba 2.6.2) é obrigatório quando for selecionada uma UC.<br>Casos de provável ocorrência, por exemplo quando a distribuição da espécie é ampla e sobrepõe várias unidades, não devem ser cadastradas.<br>Esses casos podem ser detalhados no campo descritivo da aba 7.4-Presença em UC.">Unidade de Conservação</label>
              <br>
              <select name="sqControle" id="sqControle" class="fld form-control fld500 select2"
                      data-s2-url="ficha/select2Ucs"
                      data-s2-on-change="tabGeoreferencia.getCentroide"
                      data-s2-placeholder="?"
                      data-s2-minimum-input-length="1"
                      data-s2-auto-height="true">
                  <option value="">?</option>
                  <g:if test="${fichaOcorrencia?.uc}">
                      <option value="${fichaOcorrencia?.uc?.id}" selected>${fichaOcorrencia?.ucHtml}</option>
                  </g:if>
              </select>
          </div>
      <br>
      <div class="form-group">
            <label class="control-label">Presença atual na coordenada</label>
            <br>
            <select  id="inPresencaAtual"
               name="inPresencaAtual"
                required="true"
               class="fld form-control fld200 fldSelect">
               <option value="">-- selecione --</option>
               <g:each var="item" in="${oSND}">
               <g:if test="${fichaOcorrencia?.inPresencaAtual == item.key}">
               <option value="${item.key}" selected>${item.value}</option>
               </g:if>
               <g:else>
               <option value="${item.key}">${item.value}</option>
               </g:else>
               </g:each>
            </select>
      </div>

%{--       altitude--}%
       <div class="form-group">
           <label class="control-label" for="nuAltitude" title="Em metros">Altitude</label>
           <br>
           <div class="input-group">
                <input id="nuAltitude" name="nuAltitude" type="text" class="fld form-control fld70" data-mask="0000" value="${fichaOcorrencia?.nuAltitude}">
                <span class="input-group-addon">m</span>
            </div>
       </div>

%{--   SENSIVEL --}%
       %{-- campo removido para nao admins em reunião remota (teams) dia 27/05/21 com Rodrigo e equipe --}%
       <g:if test="${ session?.sicae?.user?.isADM() }">
           <div class="form-group" style="display: inline-block;border-bottom:1px solid silver;margin: 24px 0px 0px 7px;" id="divLocalidadeDesconhecida">
               <label for="inSensivel" class="control-label cursor-pointer">Sensível ?
                   <input name="inSensivel" id="inSensivel" data-default-checked="${ (fichaOcorrencia?.inSensivel=='S' ) ? true : false }" value="S" type="checkbox"
                          class="fld checkbox-lg" name="inSensivel" ${ fichaOcorrencia?.inSensivel == 'S' ? ' checked':''}/>
               </label>
           </div>
       </g:if>

         <br>
      <div class="form-group">
         <label class="control-label">Prazo de carência.</label>
         <br>
         <select name="sqApoioPrazoCarencia" id="sqApoioPrazoCarencia" class="fld form-control fld250 fldSelect" required="true">
            <option data-codigo='' value="">-- selecione --</option>
            <g:each in="${prazoCarencia}" var="carencia">
               <g:if test="${fichaOcorrencia?.prazoCarencia?.id == carencia?.id}">
                    <option data-codigo="${carencia.codigoSistema}" value="${carencia?.id}" selected>${carencia?.descricao}</option>
              </g:if>
              <g:else>
                    <option data-codigo="${carencia.codigoSistema}" value="${carencia?.id}">${carencia?.descricao}</option>
            </g:else>
            </g:each>
         </select>
      </div>
      <div class="form-group">
         <label class="control-label">Datum</label>
         <br>
         <select name="sqApoioDatum" id="sqApoioDatum" class="fld form-control fld200 fldSelect" required="true">
            <option value="">-- selecione --</option>
            <g:each in="${datum}" var="dtm">
                <option data-codigo="${dtm?.codigo}" value="${dtm?.id}" ${fichaOcorrencia?.datum?.id == dtm?.id ? ' selected' : ''}>${dtm?.descricao}</option>
            </g:each>
         </select>
      </div>

      <br>

      <div class="form-group" id="divLatGD">
         <label class="control-label" for="fldLatitude" title="Clique no mapa com a telca CTRL pressionada para peencher os campos latitude e longitude com as coordenadas do ponto clicado!" data-tooltip-icon="pushpin">Latitude</label>
         <br>
         <input id="fldLatitude" name="fldLatitude" type="text" class="fld form-control fld150 boldAzul coordenada" value="${fichaOcorrencia?.geometry?.y}" required="true"/>
      </div>
      <div class="form-group"  id="divLonGD">
         <label class="control-label">Longitude</label>
         <br>
         <input id="fldLongitude" name="fldLongitude" type="text" class="fld form-control fld150 boldAzul coordenada" value="${fichaOcorrencia?.geometry?.x}" required="true"/>
         <button type="button" id="btnChangeCoordFormat" data-action="tabGeoreferencia.changeGDGMS" class="btn btn-default btn-sm" title="Utilizar formato GMS.">GMS</button>
         <button type="button" id="btnViewPoint" data-action="tabGeoreferencia.updateMarker(false)" class="fld btn btn-default btn-sm" title="Informe as coordenadas e clique aqui para visualizar a localização no mapa!"><i class="glyphicon glyphicon-map-marker"></i></button>
         <button type="button" id="btnCentroid" data-action="tabGeoreferencia.changeToCentroideMunicipio" class="fld btn btn-default btn-sm" title="Centroide do municipio - Informe as coordenadas e clique aqui para posicionar no centroide do município!">CM</button>
         <button type="button" id="btnCentroidEstado" data-action="tabGeoreferencia.changeToCentroideEstado" class="fld btn btn-default btn-sm" title="Centroide do Estado - Informe as coordenadas e clique aqui para posicionar no centroide do Estado!">CE</button>
      </div>

      <div class="form-group hidden" id="divLatGMS">
         <label class="control-label" for="fldLatitudeGMS" title="Clique no mapa com a telca CTRL pressionada para peencher os campos latitude e longitude com as coordenadas do ponto clicado!" data-tooltip-icon="pushpin">Latitude</label>
         <br>
         <div class="input-group">
            <div class="input-group">
               <span class="input-group-addon">G</span>
               <input id="fldGLat" id="fldLatitudeGMS" type="text" class="fld form-control fld50" required="true">
            </div>
            &nbsp;
            <div class="input-group">
               <span class="input-group-addon">M</span>
               <input id="fldMLat" type="text" class="fld form-control fld50" required="true">
            </div>
            &nbsp;
            <div class="input-group">
               <span class="input-group-addon">S</span>
               <input id="fldSLat" type="text" data-mask="99,00000" class="fld form-control fld90" required="true">
            </div>
            &nbsp;
            <div class="input-group">
               <span class="input-group-addon">H</span>
               <select id="fldHLat" class="fld form-control fld100 fldSelect">
                  <option value="S">Sul</option>
                  <option value="N">Norte</option>
               </select>
            </div>
         </div>
      </div>
      <br class="brLatLon hidden"/>

      <div class="form-group hidden" id="divLonGMS">
         <label class="control-label">Longitude</label>
         <br>
         <div class="input-group">
            <div class="input-group">
               <span class="input-group-addon">G</span>
               <input id="fldGLon" type="text" class="fld form-control fld50" required="true">
            </div>
            &nbsp;
            <div class="input-group">
               <span class="input-group-addon">M</span>
               <input id="fldMLon" type="text" class="fld form-control fld50" required="true">
            </div>
            &nbsp;
            <div class="input-group">
               <span class="input-group-addon">S</span>
               <input id="fldSLon" type="text" data-mask="99,00000" class="fld form-control fld90" required="true">
            </div>
            &nbsp;
            <div class="input-group">
               <span class="input-group-addon">H</span>
               <select id="fldHLon" class="fld form-control fld100 fldSelect">
                  <option value="W">Oeste</option>
                  <option value="E">Leste</option>
               </select>
            </div>
            &nbsp;
            <button type="button" data-action="tabGeoreferencia.changeGDGMS" class="btn btn-default btn-sm" title="Utilizar o formato GD">GD</button>
            &nbsp;
            <button type="button" data-action="tabGeoreferencia.updateMarker(false)" class="btn btn-default btn-sm" title="Informe as coordenadas e clique aqui para visualizar a localização no mapa!"><i class="glyphicon glyphicon-map-marker"></i></button>
         </div>
      </div>

      <br>

      <div class="form-group">
         <label class="control-label">Formato original</label>
         <br>
         <select name="sqApoioFormatoCoordOriginal" id="sqApoioFormatoCoordOriginal" class="fld form-control fld250 fldSelect">
            <option value="">-- selecione --</option>
            <g:each in="${formatoOriginal}" var="fOriginal">
            <g:if test="${fichaOcorrencia?.formatoCoordOriginal?.id == fOriginal?.id}">
             <option data-codigo="${fOriginal?.codigoSistema}" value="${fOriginal?.id}" selected>${fOriginal?.descricao}</option>
            </g:if>
            <g:else>
             <option data-codigo="${fOriginal?.codigoSistema}" value="${fOriginal?.id}">${fOriginal?.descricao}</option>
            </g:else>
            </g:each>
         </select>
      </div>
      <div class="form-group">
         <label class="control-label">Precisão da coordenada</label>
         <br>
         <select name="sqApoioPrecisaoCoordenada" id="sqApoioPrecisaoCoordenada" data-change="tabGeoreferencia.sqApoioPrecisaoCoordenadaChange" class="fld form-control fld250 fldSelect" required="true">
            <option data-codigo="" value="">-- selecione --</option>
            <g:each in="${precisaoCoord}" var="precCoord">
               <option data-codigo="${precCoord?.codigo}" value="${precCoord?.id}" ${fichaOcorrencia?.precisaoCoordenada?.id == precCoord?.id?' selected':''}>${precCoord?.descricao}</option>
            </g:each>
         </select>
      </div>
      <br>
      <div class="form-group">
         <label class="control-label">Referência da aproximação</label>
         <br>
         <select name="sqApoioRefAproximacao" id="sqApoioRefAproximacao" class="fld form-control fld250 fldSelect">
            <option data-codigo='' value="">-- selecione --</option>
            <g:each in="${refAproximacao}" var="refAprox">
            <g:if test="${fichaOcorrencia?.refAproximacao?.id == refAprox?.id}">
               <option data-codigo="${refAprox.codigoSistema}" value="${refAprox?.id}" selected>${refAprox?.descricao}</option>
            </g:if>
            <g:else>
               <option data-codigo="${refAprox.codigoSistema}" value="${refAprox?.id}">${refAprox?.descricao}</option>
            </g:else>
            </g:each>
         </select>
      </div>
      <div class="form-group">
         <label class="control-label">Método de aproximação</label>
         <br>
         <select name="sqApoioMetodoAproximacao" id="sqApoioMetodoAproximacao" class="fld form-control fld250 fldSelect">
            <option data-codigo='' value="">-- selecione --</option>
            <g:each in="${metodAproximacao}" var="metAprox">
            <g:if test="${fichaOcorrencia?.metodoAproximacao?.id == metAprox?.id}">
               <option data-codigo="${metAprox.codigoSistema}" value="${metAprox.id}" selected>${metAprox.descricao}</option>
            </g:if>
            <g:else>
               <option data-codigo="${metAprox.codigoSistema}"  value="${metAprox.id}">${metAprox.descricao}</option>
            </g:else>
            </g:each>
         </select>
      </div>
      <br>
      <div class="form-group w100p">
         <label class="control-label">Descrição do método de aproximação</label>
         <br>
         <textarea name="txMetodoAproximacao" rows="7" class="fld form-control fldTextarea wysiwyg w100p">${fichaOcorrencia?.txMetodoAproximacao}</textarea>
      </div>
      <br>

       <div class="form-group">
           <label class="control-label">Situação do registro</label>
           <br>
           <select name="sqSituacaoAvaliacao" id="sqSituacaoAvaliacao" class="fld form-control fld350 fldSelect" data-change="tabGeoreferencia.sqSituacaoAvaliacaoChange">
               <g:each var="item" in="${situacaoRegistro}">
                    <option data-codigo="${ item?.codigoSistema }" value="${item?.id}" ${ ( ! fichaOcorrencia?.situacaoAvaliacao && item.codigoSistema=='REGISTRO_UTILIZADO_AVALIACAO') || fichaOcorrencia?.situacaoAvaliacao?.id?.toString() == item?.id?.toString() ? 'selected' : ''}>${item?.descricao}</option>
               </g:each>
           </select>
       </div>
       <br>
       <div id="divMotivoNaoUtilizadoWrapper" class="w100p hidden" style="background-color: #fafad2;border:1px solid #d5dbd4;">
           <div class="form-group">
               <label class="control-label label-required">Motivo não utilizado</label>
               <br>
               <select name="sqMotivoNaoUtilizadoAvaliacao" id="sqMotivoNaoUtilizadoAvaliacao" class="fld form-control fld350 fldSelect">
                   <g:each var="item" in="${motivoNaoUtilizado}">
                       <option data-codigo="${item?.codigo}" value="${item?.id}" ${fichaOcorrencia?.motivoNaoUtilizadoAvaliacao?.id == item?.id }>${item?.descricao}</option>
                   </g:each>
               </select>
           </div>
           <br>
           <div class="form-group w100p">
               <label for="txNaoUtilizadoAvaliacao" class="control-label label-required">Justificativa não utilizado</label>
               <br>
               <textarea name="txNaoUtilizadoAvaliacao" id="txNaoUtilizadoAvaliacao" rows="5" class="fld form-control w100p">${fichaOcorrencia?.txNaoUtilizadoAvaliacaoSemUsuario}</textarea>
               <g:if test="${fichaOcorrencia?.usuarioDataTxNaoUtilizadoAvaliacao?.nome}">
                   <span class="font-small gray">Justificado por ${fichaOcorrencia?.usuarioDataTxNaoUtilizadoAvaliacao?.nome} em ${fichaOcorrencia?.usuarioDataTxNaoUtilizadoAvaliacao?.data}</span>
               </g:if>

           </div>
       </div>
      <br>
      <div class="form-group w100p">
         <label for="idOrigem" class="control-label" title="Este campo pode ser utilizado para evitar duplicidade ou auxiliar no controle e identificação de registro importados para o SALVE. Deve ser o identificador único (ID) da base de dados de origem.">ID da origem</label>
         <br>
         <input id="idOrigem" name="idOrigem" type="text" class="fld form-control fld200" maxlength="50" value="${fichaOcorrencia?.idOrigem}">
      </div>
      <br>
      <div class="form-group w100p">
         <label for="txObservacao" class="control-label">Observação</label>
         <br>
         <textarea name="txObservacao" id="txObservacao" rows="5" class="fld form-control w100p">${fichaOcorrencia?.txObservacao}</textarea>
      </div>
   </div>
   <div class="col-sm-6">
      %{-- <div id="map" class="map" style="height:600px; margin: 17px 0 0 -16px;" data-map-on-load="tabGeoreferencia.mapInit"></div> --}%
      <div id="mapTabOcorrencia" class="map" style="border:1px solid silver;height:670px;" data-map-on-load="tabGeoreferencia.mapInit"></div>
   </div>
   <div class="col-sm-12">
      <div class="panel-footer">
         <button type="button" data-action="tabGeoreferencia.save" data-params="sqFicha,sqFichaOcorrencia" class="fld btn btn-success">Gravar alteração</button>
         <button type="button" data-action="tabGeoreferencia.cancelarEdicaoOcorrencia" class="fld btn btn-info" title="Limpar os campos e iniciar uma novo registro.">Limpar</button>
         <button type="button" data-action="tabGeoreferencia.saveAsNew"
                title="Gravar novo registro no banco de dados a partir dos campos já preenchidos."
                data-params="sqFicha" id="btnSaveOccurrenceAs" class="fld btn btn-warning" style="display: none">Gravar novo registro</button>
      </div>
   </div>
</form>
