<form id="frmDisOcoLocalidade" class="form-inline" role="form">
   <div class="form-group">
      <label class="control-label">Localidade</label>
      <br />
      <input name="noLocalidade" type="text" maxlenght="100" class="fld form-control fld600" value="${fichaOcorrencia.noLocalidade}"/>
   </div>
   <br />
   <div class="form-group">
      <label class="control-label">Característica da Localização</label>
      <br />
      <input name="txLocal" type="text" maxlenght="255" class="fld form-control fld600" value="${fichaOcorrencia.txLocal}"/>
   </div>
   %{--  a UC passou para a primeira aba
   <br />
      <div class="form-group">
          <%-- inicio campo select ucs --%>
          <div class="fld form-group">
              <label class="control-label">Unidade de Conservação</label>
              <br />
              <select name="sqControle" id="sqControle" class="fld form-control fld500 select2"
                      data-s2-url="ficha/select2Ucs"
                      data-s2-placeholder="?"
                      data-s2-minimum-input-length="1"
                      data-s2-auto-height="true">
                  <option value="">?</option>
                  <g:if test="${fichaOcorrencia?.uc}">
                      <option value="${fichaOcorrencia.uc.id}" selected>${fichaOcorrencia.ucHtml}</option>
                  </g:if>
              </select>
          </div>
   </div> --}%
   <br />
   %{-- REMOVIDO CAMPO CONTINENTE DE ACORDO COM AS REGRAS DA PLANILHA - RODRIGO
   <div class="form-group">
      <label class="control-label">Continente</label>
      <br />
      <select name="sqContinente" class="fld form-control fld200 fldSelect">
         <option value="">?</option>
         <g:each var="item" in="${listContinentes}">
         <option value="${item.id}" ${item.id==fichaOcorrencia?.continente?.id ? ' selected' : ''}>${item.descricao}</option>
         </g:each>
      </select>
   </div> --}%
   <div class="form-group">
      <label class="control-label">País</label>
      <br />
      <select name="sqPais" id="sqPais" class="fld form-control fld200 select2"
            data-s2-minimum-input-length="0"
            data-s2-placeholder="?"
            data-s2-onChange="tabLocalidade.sqPaisChange"
            data-s2-auto-height="true">
            <option value="">?</option>
            <g:each var="item" in="${listPaises}">
               <option value="${item.id}" ${item.id==fichaOcorrencia?.pais?.id ? ' selected' : ''}>${item.noPais}</option>
            </g:each>
      </select>
   </div>
   <br />
   <div class="form-group" id="divEstado">
      <label class="control-label">Estado</label>
      <br />
      <select name="sqEstado" id="sqEstado" class="fld form-control fldSelect fld200">
         <option value="">?</option>
         <g:each var="item" in="${listEstados}">
            <option value="${item.id}" ${item.id==fichaOcorrencia?.estado?.id ? ' selected' : ''}>${item.noEstado+' - '+item.sgEstado}</option>
         </g:each>
      </select>
   </div>
   <div class="form-group" id="divMunicipio">
      <label class="control-label">Município</label>
      <br />
      <select class="fld form-control fld300 select2" name="sqMunicipio" id="sqMunicipio"
         data-s2-params="sqEstado"
         data-s2-url="ficha/select2Municipio"
         data-s2-placeholder="?"
         data-s2-minimum-input-length="3"
         data-s2-auto-height="true"
         >
         <g:if test="${fichaOcorrencia.municipio}">
            <option value="${fichaOcorrencia.municipio.id}" selected>${fichaOcorrencia.municipio.noMunicipio}</option>
         </g:if>
      </select>
   </div>

   <br>

   <div class="form-group">
      <label class="control-label">Habitat</label>
      <br>
      <select id="sqHabitatPai" name="sqHabitatPai" data-form="frmDisOcoLocalidade" class="fld form-control fld250 fldSelect" data-child="sqHabitatLocalidade" data-change="ficha.habitatPaiChange"
         data-s2-placeholder="?"
         data-s2-minimum-input-length="0">
         <option value="">-- Selecione --</option>
         <g:each var="item" in="${listHabitats}">
            <option value="${item.id}" data-codigo="${item.codigo}" ${item.id==fichaOcorrencia?.habitat?.rootId ? ' selected':''}>${item.descricao}</option>
         </g:each>
      </select>
      <span>-</span>
      <select id="sqHabitatLocalidade" name="sqHabitat" class="fld form-control fldSelect select2 fld600"
          data-s2-placeholder=" "
          data-s2-minimum-input-length="0"
          disabled="true">
         <option value="">?</option>
         <g:if test="${fichaOcorrencia?.habitat}">
           <option data-codigo="${fichaOcorrencia?.habitat?.codigo}" value="${fichaOcorrencia?.habitat.id}" selected>${fichaOcorrencia?.habitat?.descricao}</option>
       </g:if>
      </select>
   </div>

   <br>
  %{--  <div class="form-group">
      <label class="control-label">Habitat</label>
      <br />
      <select name="sqHabitat" class="fld form-control fld800">
         <option value="">?</option>
         <g:each var="item" in="${listHabitats}">
            <option value="${item.id}" ${item.id==fichaOcorrencia?.habitat?.id ? ' selected' : ''}>${item.descricao}</option>
               <g:each var="subItem" in="${item.itens}">
                  <option value="${subItem.id}" ${subItem.id==fichaOcorrencia?.habitat?.id ? ' selected' : ''}>&nbsp;&nbsp;&nbsp;${subItem.descricao}</option>
               </g:each>
         </g:each>
      </select>
   </div>
   <br/> --}%
   <div class="form-group">
      <label class="control-label">Elevação Min</label>
      <br />
      <div class="input-group">
         <input name="vlElevacaoMin" type="text" class="fld form-control fld150" data-mask="99999,00" value="${fichaOcorrencia.vlElevacaoMin}"  value="${fichaOcorrencia.vlElevacaoMin}"/>
         <div class="input-group-addon" title="metros">m</div>
      </div>
   </div>
   <div class="form-group">
      <label class="control-label">Elevação Max</label>
      <br />
      <div class="input-group">
         <input name="vlElevacaoMax" type="text" class="fld form-control fld150" data-mask="99999,00"  value="${fichaOcorrencia.vlElevacaoMax}"/>
         <div class="input-group-addon" title="metros">m</div>
      </div>
   </div>
   <br />
   <div class="form-group">
      <label class="control-label">Profundidade Min</label>
      <br />
      <div class="input-group">
         <input name="vlProfundidadeMin" type="text" class="fld form-control fld150" data-mask="99999,00" value="${fichaOcorrencia.vlProfundidadeMin}"/>
         <div class="input-group-addon" title="metros">m</div>
      </div>
   </div>
   <div class="form-group">
      <label class="control-label">Profundidade Max</label>
      <br />
      <div class="input-group">
         <input name="vlProfundidadeMax" type="text" class="fld form-control fld150" data-mask="99999,00" value="${fichaOcorrencia.vlProfundidadeMax}"/>
         <div class="input-group-addon" title="metros">m</div>
      </div>
   </div>
   <br />
   <div class="panel-footer">
      <button data-action="tabLocalidade.save" data-params="sqFichaOcorrencia" class="fld btn btn-success">Gravar Localidade</button>
   </div>
</form>
