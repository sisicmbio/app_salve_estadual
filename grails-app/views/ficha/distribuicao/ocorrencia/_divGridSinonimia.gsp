<!-- view: /views/ficha/distribuicao/ocorrencia/_divGridSinonimia -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>Nome antigo</th>
            <th>Autor/Ano</th>
            <th>Ref. Bibliográfica</th>
            <g:if test="${ ! session.sicae.user.isCO() }">
                <th>Ação</th>
            </g:if>
        </tr>
    </thead>
    <tbody>
        <g:if test="${listaFichaSinonimia}">
            <g:each var="item" in="${listaFichaSinonimia}">
                <tr>
                    <td><i>${item?.noSinonimia}</i>${ ( nomeAntigo && item?.noSinonimia && item?.noSinonimia == nomeAntigo ? ' (ciclo anterior)' : '')}</td>
                    <td>${item?.autorAno}</td>
                    <td>
                        <a data-action="ficha.openModalSelRefBibFicha" data-com-pessoal="true" data-no-tabela="ficha_sinonimia" data-no-coluna="no_sinonimia" data-sq-registro="${item.id}"
                            data-de-rotulo="Nome Antigo" data-update-grid="divGridSinonimia" class="fld btn btn-default btn-xs btn-grid-subform"
                            title="Adicionar/Remover Refeferência bibliográfica."><span class="fa fa-book"></span>
                        </a>
                        &nbsp; ${raw(item.refBibHtml)}
                    </td>
                    <g:if test="${ ! session.sicae.user.isCO() }">
                        <td class="td-actions">
                            <a data-action="taxonomia.editSinonimia" data-id="${item.id}" class="fld btn btn-default btn-xs btn-update" title="Alterar">
                                <span class="glyphicon glyphicon-pencil"></span>
                            </a>
                            <a data-action="taxonomia.deleteSinonimia" data-sinonimia="${item?.noSinonimia}" data-id="${item.id}" class="fld btn fld btn-default btn-xs btn-delete"
                                title="Excluir">
                                <span class="glyphicon glyphicon-remove"></span>
                            </a>
                        </td>
                    </g:if>
                </tr>
            </g:each>
        </g:if>
        <g:else>
            <tr>
                <td colspan="5">
                    Nenhum registro
                </td>
            </tr>
        </g:else>
    </tbody>
</table>
