<table class="table table-striped table-bordered table-hover">
   <thead>
      <tr>
         <th>Referências Bibliográficas</th>
         <g:if test="${ ! session.sicae.user.isCO() }">
            <th>Ação</th>
         </g:if>
      </tr>
   </thead>
   <tbody>
      <g:each var="item" in="${lista}">
      <tr>
         <td>${raw(item.referenciaHtml)}</td>

         <g:if test="${ ! session.sicae.user.isCO() }">
            <td class="td-actions">
               <a data-id="${item.id}" data-action="tabRefBib.deleteRefBib"
                   class="fld btn btn-default btn-xs btn-delete" title="Excluir">
                  <span class="glyphicon glyphicon-remove"></span>
               </a>
            </td>
         </g:if>
      </tr>
      </g:each>
   </tbody>
</table>
