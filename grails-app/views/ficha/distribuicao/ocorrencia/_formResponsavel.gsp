<g:set var="oSN" value="${[ [key:'S',value:'Sim'], [key:'N', value:'Não'] ]}" />
<form id="frmDisOcoResponsavel" class="form-horizontal" role="form">
      <div class="col-sm-4">
         <div class="page-header group-header">
            <h3>Compilador<small></small></h3>
         </div>
         <label class="control-label">Nome:</label>
          <select name="sqPessoaCompilador" class="fld form-control fld400 select2"
                  data-s2-url="ficha/select2PessoaFisica"
                  data-s2-placeholder="?"
                  data-s2-minimum-input-length="3"
                  data-s2-auto-height="true">
            >
            <g:if test="${fichaOcorrencia?.pessoaCompilador && fichaOcorrencia?.dtCompilacao }">
               <option value="${fichaOcorrencia.pessoaCompilador.id}" selected>${fichaOcorrencia?.pessoaCompilador?.noPessoa}</option>
            </g:if>
            </select>
            %{-- <label class="control-label">Local:</label>
            <input name="" class="form-control fld400" type="text" /> --}%
            <label class="control-label">Data:</label>
            <input name="dtCompilacao" class="fld form-control fld150 date" type="text" value="<g:formatDate format='dd/MM/yyyy' date='${fichaOcorrencia?.dtCompilacao}'/>" />
      </div>
      <div class="col-sm-4">
            <div class="page-header group-header">
               <h3>Revisor<small></small></h3>
            </div>
            <label class="control-label">Nome:</label>
            <select name="sqPessoaRevisor" class="fld form-control fld400 select2"
                  data-s2-url="ficha/select2PessoaFisica"
                  data-s2-placeholder="?"
                  data-s2-minimum-input-length="3"
                  data-s2-auto-height="true">
            >
            <g:if test="${fichaOcorrencia?.pessoaRevisor}">
               <option value="${fichaOcorrencia?.pessoaRevisor?.id}" selected>${fichaOcorrencia?.pessoaRevisor?.noPessoa}</option>
            </g:if>
            </select>
            %{-- <label class="control-label">Local:</label>
            <input class="form-control fld400" type="text" /> --}%
            <label class="control-label">Data:</label>
            <input name="dtRevisao" class="fld form-control fld150 date" type="text" value="<g:formatDate format='dd/MM/yyyy' date='${fichaOcorrencia?.dtRevisao}'/>"/>
      </div>
      <div class="col-sm-4">
            <div class="page-header group-header">
               <h3>Validador<small></small></h3>
            </div>
            <label class="control-label">Nome:</label>
            <select name="sqPessoaValidador" class="fld form-control fld400 select2"
                  data-s2-url="ficha/select2PessoaFisica"
                  data-s2-placeholder="?"
                  data-s2-minimum-input-length="3"
                  data-s2-auto-height="true">
            >
            <g:if test="${fichaOcorrencia?.pessoaValidador}">
               <option value="${fichaOcorrencia?.pessoaValidador?.id}" selected>${fichaOcorrencia?.pessoaValidador?.noPessoa}</option>
            </g:if>
            </select>
            %{-- <label class="control-label">Local:</label>
            <input class="form-control fld400" type="text" /> --}%
            <label class="control-label label-required">Data:</label>
            <input name="dtValidacao" class="fld form-control fld150 date" type="text" value="<g:formatDate format='dd/MM/yyyy' date='${fichaOcorrencia?.dtValidacao}'/>"/>
      </div>
%{--      CAMPOS PASSARAM PARA A ABA 2.6.1--}%
    %{--
      <div class="col-sm-12">
          <label class="control-label">Utilizar na avaliação?</label>
          <select name="inUtilizadoAvaliacao" id="inUtilizadoAvaliacao" class="fld form-control fld200 fldSelect" data-change="tabResponsaveis.inUtilizadoAvaliacaoChange">
              <option value="">?</option>
              <g:each var="item" in="${oSN}">
                  <option value="${item.key}" ${fichaOcorrencia.inUtilizadoAvaliacao==item.key ? ' selected' : '' }>${item.value}</option>
              </g:each>
          </select>
          <div>
            <label class="control-label label-required">Justificativa para não utilização</label>
            <textarea class="fld form-control fldTextarea wysiwyg w100p" rows="7" name="txNaoUtilizadoAvaliacao" id="txNaoUtilizadoAvaliacao">${fichaOcorrencia.txNaoUtilizadoAvaliacao}</textarea>
            <label class="control-label">Invalidado em:</label>
            <input name="dtInvalidado" class="fld form-control fld130 date" type="text" value="<g:formatDate format='dd/MM/yyyy' date='${fichaOcorrencia?.dtInvalidado}'/>"/>
          </div>
      </div>
      --}%
      <div class="col-sm-12">
         <div class="panel-footer">
            <button data-action="tabResponsaveis.save" data-params="sqFichaOcorrencia" class="fld btn btn-success">Gravar Responsáveis</button>
         </div>
      </div>
</form>
