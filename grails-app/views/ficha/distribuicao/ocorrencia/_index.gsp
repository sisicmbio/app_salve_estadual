<%--início sub-abas ocorrencia--%>
<div class="row mt10">
   <div id="divSubAbasOcorrencia" class="col-sm-12">
      <input id="sqFichaOcorrencia" name="sqFichaOcorrencia" type="hidden" value=""/>
      <%--inicio abas ocorrencia--%>
      <ul class="nav nav-pills" id="ulTabsDisOcorrencia" style="border-bottom:1px solid #337ab7;margin-bottom:6px;">
         <li id="liTabDisOcoGeoreferencia">  <a data-action="ficha.tabClick" data-url="fichaDisOcorrencia/tabGeoReferencia" data-container="tabDisOcoGeoreferencia" data-on-load="ocorrencia.tabGeoreferenciaInit" data-reload="false" data-params="sqFicha,sqFichaOcorrencia" data-toggle="tab" href="#tabDisOcoGeoreferencia">2.6.1 - Georreferenciamento</a></li>
         <li id="liTabDisOcoDadosRegistro">  <a data-action="ficha.tabClick" data-url="fichaDisOcorrencia/tabDisOcoDadosRegistro" data-container="tabDisOcoDadosRegistro" data-reload="false" data-params="sqFicha,sqFichaOcorrencia" data-on-load="disOcoDadosRegistro.tabDadosRegistroInit" data-toggle="tab" href="#tabDisOcoDadosRegistro">2.6.2 - Dados do Registro</a></li>
         <li id="liTabDisOcoLocalidade">     <a data-action="ficha.tabClick" data-url="fichaDisOcorrencia/tabDisOcoLocalidade" data-on-load="tabLocalidade.tabDisOcoLocalidadeInit" data-container="tabDisOcoLocalidade" data-reload="false" data-params="sqFicha,sqFichaOcorrencia" data-toggle="tab" href="#tabDisOcoLocalidade">2.6.3 - Localidade</a></li>
         <li id="liTabDisOcoQualificador">   <a data-action="ficha.tabClick" data-url="fichaDisOcorrencia/tabDisOcoQualificador" data-container="tabDisOcoQualificador" data-reload="false" data-params="sqFicha,sqFichaOcorrencia" data-toggle="tab" href="#tabDisOcoQualificador">2.6.4 - Qualificador</a></li>
         <li id="liTabDisOcoResponsavel">    <a data-action="ficha.tabClick" data-url="fichaDisOcorrencia/tabDisOcoResponsavel" data-container="tabDisOcoResponsavel" data-on-load="tabResponsaveis.init" data-reload="false" data-params="sqFicha,sqFichaOcorrencia" data-toggle="tab" href="#tabDisOcoResponsavel">2.6.5 - Responsáveis</a></li>
         <li id="liTabDisOcoRefBib">         <a data-action="ficha.tabClick" data-url="fichaDisOcorrencia/tabDisOcoRefBib" data-container="tabDisOcoRefBib" data-on-load="ocorrencia.tabRefBibInit" data-reload="true" data-params="sqFicha,sqFichaOcorrencia" data-toggle="tab" href="#tabDisOcoRefBib">2.6.6 - Ref. Bibliográfica</a></li>
         <li id="liTabDisOcoMultimidia">     <a data-action="ficha.tabClick" data-url="fichaDisOcorrencia/tabDisOcoMultimidia" data-container="tabDisOcoMultimidia" data-on-load="ocorrencia.tabMultimidiaInit" data-reload="true" data-params="sqFicha,sqFichaOcorrencia" data-toggle="tab" href="#tabDisOcoMultimidia">2.6.7 - Imagem/áudio</a></li>
      </ul>
      <div id="disOcoTabContent" class="tab-content">
         %{-- inicio aba distribuicao ocorrencia georeferencia --}%
         <div role="tabpanel" class="tab-pane tab-pane-ficha active" id="tabDisOcoGeoreferencia">
            %{-- <g:render template="/ficha/distribuicao/ocorrencia/formGeoreferencia"></g:render>*/--}%
         </div>
      %{-- fim aba distribuicao ocorrencia georeferencia --}% %{-- inicio aba distribuicao ocorrencia dados do registro --}%
         <div role="tabpanel" class="tab-pane tab-pane-ficha" id="tabDisOcoDadosRegistro">
            %{-- <g:render template="distribuicao/ocorrencia/formDadosRegistro"></g:render> --}%
         </div>
         %{-- fim aba distribuicao ocorrencia dados do registro --}% %{-- inicio aba distribuicao ocorrencia localidade --}%
         <div role="tabpanel" class="tab-pane tab-pane-ficha" id="tabDisOcoLocalidade">
            %{-- <g:render template="distribuicao/ocorrencia/formLocalidade"></g:render> --}%
         </div>
         %{-- fim aba distribuicao ocorrencia localidade --}% %{-- inicio aba distribuicao ocorrencia qualificador do registro --}%
         <div role="tabpanel" class="tab-pane tab-pane-ficha" id="tabDisOcoQualificador">
            %{-- <g:render template="distribuicao/ocorrencia/formQualificador"></g:render> --}%
         </div>
         %{-- fim aba distribuicao ocorrencia qualificador do registro --}% %{-- inicio aba distribuicao ocorrencia responsaveis pelo registro --}%
         <div role="tabpanel" class="tab-pane tab-pane-ficha row" id="tabDisOcoResponsavel">
            %{-- <g:render template="distribuicao/ocorrencia/formResponsavel"></g:render> --}%
         </div>
         %{-- fim aba distribuicao ocorrencia responsaveis pelo registro --}%
         <div role="tabpanel" class="tab-pane tab-pane-ficha" id="tabDisOcoRefBib">
            %{-- <g:render template="distribuicao/ocorrencia/formRefBib"></g:render> --}%
         </div>
         %{-- fim aba referência bibliográfica --}%

         <div role="tabpanel" class="tab-pane tab-pane-ficha" id="tabDisOcoMultimidia"></div>
         %{-- fim aba multimidia --}%
      </div>
      <%--fim tab content ocorrencia--%>
   </div>
   <%-- fim col-sm-12 - sub abas ocorrencia--%>
</div>
<%--fim row - sub-abas ocorrencia--%>
