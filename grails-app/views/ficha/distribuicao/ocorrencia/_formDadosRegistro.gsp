<g:set var="oSND"  value="${[ [key:'S',value:'Sim'], [key:'N', value:'Não'] ,[key:'D', value:'Desconhecido'] ]}"/>
<form id="frmDisOcoRegistro" class="form-inline" role="form">
   <div class="form-group">
      <label for="" class="control-label">Nivel Taxonômico</label>
      <br>
      <select  id="nivelOcoDadosDoRegistro"
         name="nivelOcoDadosDoRegistro"
         data-change="disOcoDadosRegistro.sqNivelOcoDadosDoRegistroChange"
         class="fld form-control fld200 fldSelect select2"
         data-s2-minimum-input-length="0">
         <option value="">-- selecione --</option>
         <g:each var="item" in="${listNiveisTaxonomicos}">
         <option value="${item.coNivelTaxonomico}" ${fichaOcorrencia?.taxonCitado?.nivelTaxonomico?.id == item.id ? ' selected':''} >${item.deNivelTaxonomico}</option>
         </g:each>
      </select>
   </div>
   <div class="form-group">
      <label for="" class="control-label">Táxon Originalmente Citado</label>
      <br/>
      <select  id="sqTaxonCitado"
         name="sqTaxonCitado"
         data-s2-visible="true"
         class="fld form-control select2 fld250"
         data-s2-params="nivelOcoDadosDoRegistro|nivel"
         data-s2-url="ficha/select2Taxon"
         data-s2-placeholder=" -- Selecione -- "
         data-s2-maximum-selection-length="2"
         disabled="true"
         required="true">
         <g:if test="${fichaOcorrencia?.taxonCitado?.id}">
         <option value="${fichaOcorrencia?.taxonCitado?.id}" selected>${fichaOcorrencia?.taxonCitado?.noCientifico}</option>
         </g:if>
      </select>
   </div>
   <br>
   %{-- campo passou para a primeira aba
    <div class="form-group">
      <label class="control-label">Presença Atual na Coordenada</label>
      <br>
      <select  id="inPresencaAtual"
         name="inPresencaAtual"
         class="fld form-control fld200 fldSelect">
         <option value="">-- selecione --</option>
         <g:each var="item" in="${oSND}">
         <g:if test="${fichaOcorrencia?.inPresencaAtual == item.key}">
         <option value="${item.key}" selected>${item.value}</option>
         </g:if>
         <g:else>
         <option value="${item.key}">${item.value}</option>
         </g:else>
         </g:each>
      </select>
   </div>
   <br> --}%

   %{-- modou para aba 2.6.1
   <div class="form-group">
      <label class="control-label">Tipo do Registro</label>
      <br>
      <select  class="fld form-control fld200 fldSelect"
         name="sqTipoRegistro" id="sqTipoRegistro">
         <option value="">-- selecione --</option>
         <g:each in="${tipoRegistro}" var="item">
         <g:if test="${fichaOcorrencia?.tipoRegistro?.id == item?.id}">
         <option value="${item?.id}" selected>${item.descricao}</option>
         </g:if>
         <g:else>
         <option value="${item?.id}">${item.descricao}</option>
         </g:else>
         </g:each>
      </select>
   </div>
--}%


%{--<div class="form-group">
      <label class="control-label">Data</label>
      <br>
      <input type="text" name="dtRegistro" class="fld form-control date fld150" value="<g:formatDate format='dd/MM/yyyy' date='${fichaOcorrencia?.dtRegistro}'/>" />
   </div>
 --}%

%{-- modou para aba 2.6.1
   <div class="form-group">
      <label class="control-label">Dia</label>
      <br>
      <input type="text" name="nuDiaRegistro" id="nuDiaRegistro" class="fld form-control fld50" data-rule-range="[1,31]" data-mask="00"  maxlength="2" value="${fichaOcorrencia?.nuDiaRegistro}"/>
   </div>
   <div class="form-group">
      <label class="control-label">Mês</label>
      <br>
      <input type="text" name="nuMesRegistro" id="nuMesRegistro" class="fld form-control fld50" data-rule-range="[1,12]" data-mask="00"  maxlength="2" value="${fichaOcorrencia?.nuMesRegistro}"/>
   </div>
   <div class="form-group">
      <label class="control-label">Ano</label>
      <br>
      <input type="text" name="nuAnoRegistro" id="nuAnoRegistro" class="fld form-control fld60" data-mask="0000"  maxlength="4" data-rule-min="1500" value="${fichaOcorrencia?.nuAnoRegistro}"/>
   </div>

   <div class="form-group">
      <label class="control-label">Hora</label>
      <br>
      <input type="text" name="hrRegistro" id="hrRegistro" class="fld form-control time fld100" value="${fichaOcorrencia?.hrRegistro}"/>
   </div>

   <div class="form-group">
      <label style="padding: 0px 10px;margin-top: 32px;">até</label>
   </div>

   <div class="form-group">
      <label class="control-label">Dia</label>
      <br>
      <input type="text" name="nuDiaRegistroFim" id="nuDiaRegistroFim" class="fld form-control fld50" data-rule-range="[1,31]" data-mask="00"  maxlength="2" value="${fichaOcorrencia?.nuDiaRegistroFim}"/>
   </div>
   <div class="form-group">
      <label class="control-label">Mês</label>
      <br>
      <input type="text" name="nuMesRegistroFim" id="nuMesRegistroFim" class="fld form-control fld50" data-rule-range="[1,12]" data-mask="00"  maxlength="2" value="${fichaOcorrencia?.nuMesRegistroFim}"/>
   </div>
   <div class="form-group">
      <label class="control-label">Ano</label>
      <br>
      <input type="text" name="nuAnoRegistroFim" id="nuAnoRegistroFim" class="fld form-control fld60" data-mask="0000"  maxlength="4" data-rule-min="1500" value="${fichaOcorrencia?.nuAnoRegistroFim}"/>
   </div>


   <div class="form-group">
      <label for="inDataDesconhecida" class="control-label cursor-pointer" style="margin-top:17px;">Data e Hora/Período Desconhecidos
         <input name="inDataDesconhecida" id="inDataDesconhecida" data-default-checked="${ (fichaOcorrencia?.inDataDesconhecida=='S' ) ? true : false }" value="S" type="checkbox" class="fld checkbox-lg" data-action="disOcoDadosRegistro.inDataDesconhecidaChange" name="inDataDesconhecida" ${fichaOcorrencia.inDataDesconhecida=='S' ? ' checked':''}/>
      </label>
   </div>
--}%

   <div class="panel-footer">
      <button data-action="disOcoDadosRegistro.saveFrmDisOcoRegistro" data-params="sqFicha,sqFichaOcorrencia" class="fld btn btn-success">Gravar Dados do Registro</button>
   </div>
</form>