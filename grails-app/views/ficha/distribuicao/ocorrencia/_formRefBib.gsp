
<%-- inicio formulario distribuicao referencias bibliográficas --%>
<form id="frmDisOcoRefBib" class="form-inline" role="form">
   <input name="id" id="id" type="hidden" value="" />
   <%-- inicio campo ref. bibliográfica --%>
   <div class="form-group">
      <label for="" class="control-label">Referência Bibliográfica

          <i data-action="ficha.openModalSelRefBibFicha" class="fld"
             data-no-tabela="ficha_ocorrencia"
             data-no-coluna="sq_ficha_ocorrencia"
             data-sq-registro="${sqFichaOcorrenciad}"
             data-com-pessoal="true"
             data-update-grid="divGridDistOcoRefBib"
             data-de-rotulo="Ocorrência" class="fa fa-book ml10" title="Informar Referência Bibliográfica" style="color: green;" />
      </label>
      %{--<br>--}%
         %{-- definir o parametro contexto como ocorrencia para exibir museus e banco de dados --}%
         %{--<select id="sqPublicacao" name="sqPublicacao" class="fld form-control select2 fld830"--}%
                           %{--data-s2-url="ficha/select2RefBib"--}%
                           %{--data-s2-placeholder="Buscar por... ( min 3 caracteres )"--}%
                           %{--data-s2-minimum-input-length="3"--}%
                           %{--data-s2-template="refBibTemplate"--}%
                           %{--data-s2-params="contexto:'ocorrencia'"--}%
                           %{--data-s2-auto-height="true">--}%
         %{--</select>--}%
         %{--<a data-action="modal.cadRefBib" class="btn btn-sm btn-default" title="Cadastrar Referência Bibliográfica">--}%
            %{--<span class="glyphicon glyphicon glyphicon-plus" aria-hidden="true"></span>--}%
         %{--</a>--}%
   </div>
   <%-- fim campo ref. bibliográfica --%>
   %{--<br>--}%
   %{--<div class="panel-footer">--}%
      %{--<button data-action="tabRefBib.save" data-params="sqFicha,sqFichaOcorrencia,sqPublicacao" class="btn btn-success">Adicionar Referência</button>--}%
   %{--</div>--}%
   %{--<br>--}%
   <%-- início divGridDistRefBib        --%>
   <div id="divGridDistOcoRefBib">
      %{-- <g:render template="/ficha/distribuicao/ocorrencia/divGridRefBib"></g:render>  --}%
   </div>
   <%-- fim divGridDistRefBib--%>
</form>
<%-- fim formulário distribuicao Ref. Bibliográfica --%>
