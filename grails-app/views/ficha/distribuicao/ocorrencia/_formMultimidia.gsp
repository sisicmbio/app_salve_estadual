<!-- view: /views/ficha/ocorrencia/_formMultimidia/ -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

%{--<BOTÃO PARA EXIBIR/ESCONDER O FORMULÁRIO --}%
<a id="btnToggleFrmDisOcoMultimidia"
   data-toggle="collapse"
   data-target="#frmDisOcoMultimidia"
   onClick="toggleImage(this)"
   title="Clique para abrir ou fechar o formulário."
   class="fld mt10 btn btn-xs btn-default collapsed">Cadastrar imagem/áudio&nbsp;<i class="fa fa-chevron-down"></i>
</a>
<%-- inicio formulario cadastro de midias na ocorrência --%>
<form id="frmDisOcoMultimidia" name="frmDisOcoMultimidia" class="form-inline row panel-collapse collapse" role="form" enctype='multipart/form-data' novalidate="true">

      %{--<ID QUANDO EDITANDO--}%
      <input id="sqFichaOcorrenciaMultimidia" value="" type="hidden" name="sqFichaOcorrenciaMultimidia">

      %{--<TIPO DE MIDIA--}%
      <div class="col-sm-6">
         <div class="form-group">
            <label for="sqTipo" class="control-label">Tipo</label>
            <br>
            <select name="sqTipo" id="sqTipo" class="fld form-control fld200 fldSelect" data-change="tabMultimidia.sqTipoChange" required>
               <option value="">-- selecione --</option>
               <g:each var="item" in="${listTipoMultimidia}">
                  <option data-codigo="${item.codigoSistema}" value="${item.id}">${item.descricao}</option>
               </g:each>
            </select>
         </div>

        %{--<SITUAÇÃO--}%
         <div class="form-group">
            <label for="sqSituacao" class="control-label">Situação</label>
            <br>
            <select name="sqSituacao" id="sqSituacao" class="fld form-control fld300 fldSelect" required>
               <option value="">-- selecione --</option>
               <g:each var="item" in="${listSituacao}">
                  <option data-codigo="${item.codigoSistema}" value="${item.id}">${item.descricao}</option>
               </g:each>
            </select>
         </div>
         <br>

         %{--<DATA ELABORACAO--}%
         <div class="form-group">
            <label for="dtElaboracao" class="control-label">Data</label>
            <br>
            <input type="text" class="form-control fld150 date" name="dtElaboracao" id="dtElaboracao" required="true" value="${new Date().format('dd/MM/yyyy)')}">
         </div>


        %{--<IMAGEM PRINCIPAL OU NÃO   --}%
        <div class="fld form-group">
            <label for="inPrincipal" class="control-label">&nbsp;</label>
            <br>
            <label title="A imagem princial é a que será impressa na ficha." class="ml10">
                <input name="inPrincipal" id="inPrincipal" type="checkbox" class="checkbox-lg" value="S">&nbsp; Imagem principal?
            </label>
        </div>

        <br>

         %{--<NOME DO AUTOR--}%
         <div class="form-group">
            <label for="noAutor" class="control-label">Nome do Autor</label>
            <br>
            <input type="text" class="form-control fld300" name="noAutor" id="noAutor" required="true" value="${session.sicae.user.noUsuario}">
         </div>

        %{--<EMAIL DO AUTOR--}%
        <div class="form-group">
            <label for="deEmailAutor" class="control-label">Email do Autor</label>
            <br>
            <input type="text" class="form-control fld300" name="deEmailAutor" id="deEmailAutor" value="${session.sicae.user.email}">
         </div>

        %{--<LEGENDA DA MIDIA--}%
         <div class="form-group">
            <label for="deLegenda" class="control-label">Legenda</label>
            <br>
            <input type="text" class="form-control fld300" name="deLegenda" id="deLegenda" value="Legenda da imagem">
         </div>
         <br>

        %{--<DESCRICAO DA MIDIA--}%
         <div class="form-group w100p">
            <label for="txMultimidia" class="control-label">Descrição</label>
            <br>
            <textarea name="txMultimidia" id="txMultimidia" class="fld fldTextarea w100p">Descrição da imagem</textarea>
         </div>

        %{--<BOTOES DO FORMULÁRO--}%
         <div class="fld panel-footer">
            <button id="btnSaveFrmDisOcoMultimidia"  data-action="tabMultimidia.save" data-params="sqFicha,sqFichaOcorrencia" class="fld btn btn-success">Gravar</button>
            <button id="btnResetFrmDisOcoMultimidia" data-action="tabMultimidia.reset" class="fld btn btn-info hide">Limpar formulário</button>
         </div>
      </div>

      %{--<SELECIONAR ARQUIVO--}%
      <div class="col-sm-6">
         <fieldSet>
            <legend id="legend_tipo_arquivo">Pré-visualização</legend>
            <input type="file" class="file file-loading" name="fldAnexo" id="fldAnexo">
         </fieldSet>
      </div>
</form>
<%-- fim formulário distribuicao Ref. Bibliográfica --%>

<%-- gride multimidia --%>
<div id="divGridDistOcoMultimidia"></div>
<%-- fim divGridDistMultimidia--%>
