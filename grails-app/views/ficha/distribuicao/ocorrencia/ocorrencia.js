//# sourceURL=/views/ficha/distribuicao/ocorrencia/ocorrencia.js
//
;
var geolocation;
var ocorrencia = {
    oMap: null,
    map : null,
    //layerPortalbio: null,
    //layerAvaliacao: null,
    clickCoordinate: null,
    updateGridOcorrencia: function(params) {
        //ficha.getGrid('ocorrencia');
        distribuicao.updateGridOcorrencia(params);
    },
    editarOcorrencia: function(params) {
        if (!params.sqFicha) {
            app.alertError('Id da ficha não informado!');
            return;
        }
        // limpar as abas e já carregar a primeira aba
        // limpar todas as abas para recarregar com valores do registro sendo editado
        ocorrencia.clearTabs(true);

        if ( params.sqFichaOcorrencia) {
            $("#sqFichaOcorrencia").val(params.sqFichaOcorrencia);
            // habilitar abas
            ocorrencia.disableTabs(false);
        } else {
            // limpar o sqFichaOcorrencia para carregar o form vazio
            $("#sqFichaOcorrencia").val('');
            // desabilitar abas
            ocorrencia.disableTabs(true);
        }
        // configurar a tela
        $("#btnImportarPlanilha,#btnExportCsvOcorrenca").addClass('hidden');
        $("#divSubAbasOcorrencia").removeClass('hidden');
        $("#liTabDisOcoGeoreferencia > a").click();
    },
    deleteDisOcorrencia: function(params, btn) {
        app.selectGridRow(btn);
        app.confirm('<br>Confirma exclusão da ocorrência?',
            function() {
                app.ajax(app.url + 'fichaDisOcorrencia/deleteDisOcorrencia', params,
                    function(res) {
                        ocorrencia.updateGridOcorrencia();
                    });
            },
            function() {
                app.unselectGridRow(btn);
            }, params, 'Confirmação', 'warning');
    },
    isLatLon: function(lat, lng, msgError, required) {
        // http://stackoverflow.com/questions/3518504/regular-expression-for-matching-latitude-longitude-coordinates
        var latLngRegex = /^[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?),\s*[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?)$/;
        if (lat && lng) {
            var latLng = lat + ', ' + lng;
            if (!latLngRegex.test(latLng)) {
                if (msgError) {
                    app.alertError(msgError);
                }
                return false;
            }
        } else {
            if (required) {
                app.alertError('É necessário informar latitude e longitude!');
            }
            return false;
        }
        return true;
    },
    clearTabs : function( editing ) // limpar o html das abas para ao clicar recarregar o conteudo
        {
            ocorrencia.oMap=null;
            //$("#disOcoTabContent").find('.tab-pane-ficha').html('');
            $("#divSubAbasOcorrencia #sqFichaOcorrencia").val('');
            $("#disOcoTabContent div.tab-pane").html('');
            $("#btnSaveOccurrenceAs").hide();
            // simular click na aba para carregament ajax
            if( ! editing ) {
                $("#liTabDisOcoGeoreferencia > a").click();
            }
        },
    disableTabs: function(disable) // desabilitar as abas
        {
            app.disableTab('ulTabsDisOcorrencia', disable);
            app.disableTab('tabDisOcoGeoreferencia', false); // habilitar a primeira aba
        },
    clearMarkers: function() {
        // não precisa, com aimgMarker é publico, a nova mata a ultima
        ocorrencia.oMap.clearAllDraw();
        //olApi.clearLayer(ocorrencia.map, 'drawLayer');
        /*$.each(ocorrencia.map.getOverlays().getArray(), function(index, marker ) {
         ocorrencia.map.removeOverlay(marker);
         // if ( marker &&  ! marker.H.element.portalbio )
         // {
         //    ocorrencia.map.removeOverlay(marker);
         // }
         });
         */
    },
    showMarker: function(params) {
        if ( ! ocorrencia.oMap.map || !ocorrencia.isLatLon(params.fldLatitude, params.fldLongitude, 'Coordenadas invalidas', false)) {
            return false;
        }
        ocorrencia.clearMarkers();
        var coordinates = [parseFloat(params.fldLongitude), parseFloat(params.fldLatitude)];
        olApi.addMarker(ocorrencia.oMap.map, 'Desenhos', coordinates[1], coordinates[0], window.grails.markerDotRed);
    },
    tabGeoreferenciaInit: function(params) {

        // mostrar os campos da justificativa e motivo da mudança
        tabGeoreferencia.sqSituacaoAvaliacaoChange()
        // posicionar o formulário no inicio da página
        $( document ).scrollTop( $("#liTabDisOcoGeoreferencia").offset().top - 102 );
        //$( document ).scrollTop( 2096);
        if ( ! ocorrencia.oMap ) {
            tabGeoreferencia.mapInit();
            tabGeoreferencia.setGMSFields();
            tabGeoreferencia.updateMarker(true);
            tabGeoreferencia.sqApoioPrecisaoCoordenadaChange();
            if ( ! params.sqFichaOcorrencia ) {
                ocorrencia.disableTabs(true);
            } else {
                disOcoDadosRegistro.inDataDesconhecidaChange();
            }
        }
        tabGeoreferencia.showHideFieldLocalidadeDesconhecida();

        // mostrar/esconder botão saveOccurrenceAs
        if( $("#sqFichaOcorrencia").val() ) {
            $("#btnSaveOccurrenceAs").show();
        } else {
            $("#btnSaveOccurrenceAs").hide();
        }
    },
    tabRefBibInit: function(params) {
        tabRefBib.updateGridRefBib();
    },
    tabMultimidiaInit: function(params) {
        tabMultimidia.updateGride();
        tabMultimidia.sqTipoChange();

        $('#frmDisOcoMultimidia').on('hidden.bs.collapse', function(e) {
            tabMultimidia.reset()
            $("#btnToggleFrmMultimidia i").removeClass('glyphicon-minus');
        });
        $('#frmDisOcoMultimidia').on('show.bs.collapse', function(e) {
            $("#btnToggleFrmMultimidia i").addClass('glyphicon-minus');
        });
    },

    importarPlanilha: function(params) {
        app.alertInfo('Ainda não Implementado!', 'Informação');
    },

};

// ################################################################################################################################ //
// 2.6.1 - Georeferencia
// ################################################################################################################################ //
var tabGeoreferencia = {

    /**
     * mostra/esconde o campo localidade desconhecida quando o gride de ocorrencias estiver vazio ou preenchido
     */
    showHideFieldLocalidadeDesconhecida : function() {
        // mostrar esconder o campo de localidade desconhecida da aba 2.6.1 quando não houver registros
        // Se no gride de ocorrencias não tiver registros no Brasil, habilitar o check de localidade desconhecida
        /** /
        if( $("#tableGridOcorrencias").size() == 1 ) {
            if ($("#tableGridOcorrencias tbody>tr[data-bd]").size() == 0) {
                $("#divLocalidadeDesconhecida").show();
            } else {
                $("#divLocalidadeDesconhecida").hide();
                $("#stLocalidadeDesconhecida").prop("checked", false);
            }
        }
         /**/
    },
    saveAsNew: function( params ) {
        params  = params || {};
        var data = {}
        data = app.params2data( params, data )
        $("#sqFichaOcorrencia").val('');
        tabGeoreferencia.save( data );
        app.unblockUI();
    },
    saveLocalidadeDesconhecida: function( params ) {
        var stLocalidadeDesconhecida = $("#stLocalidadeDesconhecida").is(':checked') ?'S' : 'N' ;
        var data = { sqFicha:params.sqFicha, stLocalidadeDesconhecida:stLocalidadeDesconhecida};
        if( data.stLocalidadeDesconhecida=='S') {
            var points = distribuicao.oMap.getValidPoints(true,true);
            if( points.length > 0 ) {

                // ativar o layer com os limites do brasil
                var checkLayerBrasil = $("#layer-switch-mapTabOcorrencia input[type=checkbox][value=Brasil]");
                if( ! checkLayerBrasil.prop('checked') ) {
                    checkLayerBrasil.click();
                }
                app.alertInfo('Encontrado(s) ' + (points.length)+' ponto(s) no Brasil.<br>Para gravação da <b>Localidade Desconhecida</b>, não pode existir ponto(s) no Brasil.');
                return;
            }
        }
        app.ajax(app.url + 'fichaDisOcorrencia/saveLocalidadeDesconhecida', data, function( res ) {
            if ( ! res.status ) {
                app.resetChanges('divSubAbasOcorrencia');
                if( stLocalidadeDesconhecida == 'S' && $("#fldLatitude").val() ) {
                    setTimeout(function(){
                        tabGeoreferencia.cancelarEdicaoOcorrencia();
                    },1000);
                }
            }
        }, 'Atualizando informações da ficha...', 'JSON');
    },
    saveSemUf: function( params ) {
        var stSemUf = $("#stSemUf").is(':checked') ? 'S' : 'N' ;
        var data = { sqFicha:params.sqFicha, stSemUf:stSemUf};
        if( data.stSemUf=='S') {
            /*
            var points = distribuicao.oMap.getValidPoints(true,true);
            if( points.length > 0 ) {
                // ativar o layer com os limites do brasil
                var checkLayerBrasil = $("#layer-switch-mapTabOcorrencia input[type=checkbox][value=Brasil]");
                if( ! checkLayerBrasil.prop('checked') ) {
                    checkLayerBrasil.click();
                }
                app.alertInfo('Encontrado(s) ' + (points.length)+' ponto(s) no Brasil.<br>Para gravação de <b>SEM UF</b>, não pode existir ponto(s) no Brasil.');
                return;
            }*/
        }
        app.ajax(app.url + 'fichaDisOcorrencia/saveSemUf', data, function( res ) {
            if ( ! res.status ) {
                app.resetChanges('divSubAbasOcorrencia');
            }
        }, 'Atualizando informações da ficha...', 'JSON');
    },
    save: function( params ) {
        var erros = [];
        /*
        // verificar se é somente atualização do campo localidade desconhecida
        if( $("#stLocalidadeDesconhecida").is(':checked') ) {
            return tabGeoreferencia.saveLocalidadeDesconhecida(params);
        }
        */

        // se os campos GMS estiverem visívies, converter para GD antes de gravar
        if ($("#divLatGD").hasClass('hidden')) {
            tabGeoreferencia.setGDFields();
        }
        if ( ! $("#sqFichaOcorrencia").val() && !$("#frmDisOcoGeoreferencia #sqPublicacaoTemp").val() && !$("#frmDisOcoGeoreferencia #noAutorRefTemp").val()) {
            erros.push( 'A campo <b>Referência Bibliográfica ou a Comunicação Pessoal</b> é obrigatória!');
            //app.alertInfo('Para criar um registro de ocrrência é necessário selecionar uma Referência Bibliográfica ou Comunicação Pessoal!');
            //return;
        }


        // validar data somente para novos registros
        /*if( ! params.sqFichaOcorrencia ) {
            // validar tipo do registro e periodo inicial
            if (!$("#sqTipoRegistro").val()) {
                erros.push('O campo <b>"Tipo do Registro"</b> é obrigatório!')
                //return app.alertError('O campo <b>"Tipo do Registro"</b> é obrigatório!');
            }
            if ($("#nuAnoRegistro").val()) {
                $("#inDataDesconhecida").val('N');
            } else {
                if (!$("#inDataDesconhecida").is(':checked')) {
                    erros.push('O campo <b>ANO</b> é obrigatório!')
                    //return app.alertError('Informe a data, ano ou informe que a data e ano são desconhecidos!');
                }
            }
        }
        */
        if ( !  $("#inDataDesconhecida").is(':checked')) {

            if (!$("#sqTipoRegistro").val()) {
                erros.push('O campo <b>"Tipo do Registro"</b> é obrigatório!')
                //return app.alertError('O campo <b>"Tipo do Registro"</b> é obrigatório!');
            }
            // o ano é obrigatório
            if ( ! $("#nuAnoRegistro").val() ) {
                erros.push('O campo <b>ANO</b> é obrigatório!<br>Ou marque a opção data e hora desconhecidos.')
            } else {

                $("#inDataDesconhecida").val('N');

                if ($('#nuAnoRegistro').val() < 1500) {
                    erros.push('O campo <b>ANO</b> deve ser <b>MAIOR QUE</b> 1500!');
                    //return app.alertError('Informe o ANO MAIOR QUE 1500!');
                }
                if ($('#nuAnoRegistroFim').val() && $('#nuAnoRegistroFim').val() < $('#nuAnoRegistro').val()) {
                    erros.push('Informe o <b>ANO FINAL MAIOR QUE</b> ' + $('#nuAnoRegistro').val())
                    //return app.alertError('Informe o ANO MAIOR QUE ' + $('#nuAnoRegistro').val() );
                }
                if ($("#nuDiaRegistro").val() && !app.isDate($("#nuDiaRegistro").val() + '/' + $("#nuMesRegistro").val() + '/' + $("#nuAnoRegistro").val())) {
                    erros.push('Valores informados nos campos <b>DIA/MÊS/ANO Inicial</b> não formam uma data válida!')
                    //return app.alertError('Valores informados nos campos <b>DIA/MÊS/ANO Inicial</b> não formam uma data válida!');
                }
                // validar perído final
                if ($("#nuDiaRegistroFim").val() && !app.isDate($("#nuDiaRegistroFim").val() + '/' + $("#nuMesRegistroFim").val() + '/' + $("#nuAnoRegistroFim").val())) {
                    erros.push('Valores informados nos campos <b>DIA/MÊS/ANO Final</b> não formam uma data válida!');
                    //return app.alertError( 'Valores informados nos campos <b>DIA/MÊS/ANO Final</b> não formam uma data válida!');
                }
                // validar hora
                if ($("#hrRegistro").val() && !app.isTime($("#hrRegistro").val())) {
                    erros.push('o campo <br>Hora</b> está inválida!');
                    //return app.alertError('Hora inválida!');
                }
            }
        }
        // fim validacao data e hora

        if( ! $("#inPresencaAtual").val() )
        {
            erros.push( 'O campo <b>"Presença Atual da Espécie na Coordenada"</b> é obrigatória!' )
            //return app.alertError('O campo <b>"Presença Atual da Espécie na Coordenada"</b> é obrigatório!');
        }

        if ($("#sqApoioPrecisaoCoordenada >option:selected").data('codigo') == 'APROXIMADA' && !$("#sqApoioRefAproximacao").val()) {
            erros.push( 'Quando a precisão da coordenada for APROXIMADA, é necessário informar a referência da aproximação!');
            //app.alertInfo('Quando a precisão da coordenada for APROXIMADA, é necessário informar a referência da aproximação!');
            //return false;
        }
        if ($("#sqApoioRefAproximacao:hidden").size() > 0) {
            $("#sqApoioRefAproximacao,#sqApoioMetodoAproximacao").val('');
        }
        var data = $("#frmDisOcoGeoreferencia").serializeAllArray();
        data = app.params2data(params, data);

        var observacaoLength = $('<div>'+data.txObservacao+'</div>').text().length;
        if( observacaoLength > 2000 )
        {
            erros.push( 'Campo observação contém '+observacaoLength+' caractres. Máximo permitido são 2000 caracteres.' )
            //app.alertInfo( 'Campo observação contém '+observacaoLength+' caractres. Máximo permitido são 2000 caracteres.');
            //return false;
        }

        if (!data.sqFicha) {
            erros.push( 'ID da ficha não encontrado!' );
            //app.alertError('ID da ficha não encontrado!');
            //return;
        }

        // validar situacao registro
        var codigo = $("#sqSituacaoAvaliacao option:selected").data('codigo')
        if( codigo == 'REGISTRO_NAO_UTILIZADO_AVALIACAO' ) {

            if( ! data.sqMotivoNaoUtilizadoAvaliacao ) {
                erros.push('Informe o MOTIVO do registro não ter sido utilizado na avaliação!');
            }

            if( String(data.txNaoUtilizadoAvaliacao).trim() == '' ) {
                erros.push('Informe a JUSTIFICATIVA do registro não ter sido utilizado na avaliação!');
            }
        }

        if( ! data.fldLatitude || ! data.fldLongitude )
        {
            erros.push( 'Os campo  <b>Latitude e Longitude</b> devem ser informadas!' );
        }
        else {
            if (!ocorrencia.isLatLon(data.fldLatitude, data.fldLongitude, 'Coordenada inválida', true)) {
                return
            }
        }
        // se tiver selecionado uma uc enviar a uf
        if ($("#sqControle").val() && $("#frmDisOcoLocalidade #sqPais").size() > 0) {
            data.sqPais = $("#frmDisOcoLocalidade #sqPais").val();
        }

        if ($("#sqControle").val() && $("#frmDisOcoLocalidade #sqEstado").size() > 0) {
            data.sqEstado = $("#frmDisOcoLocalidade #sqEstado").val();
        }

        if ($("#sqControle").val() && $("#frmDisOcoLocalidade #sqMunicipio").size() > 0) {
            data.sqMunicipio = $("#frmDisOcoLocalidade #sqMunicipio").val();
        }


        // colocar a aba 2.6.1 no topo da tela
        document.location.href="#liTabDisOcoGeoreferencia"; //rolar a tela para baixo
        $(document).scrollTop( $(document).scrollTop() - 100.9090576171875)

        // marcar os  campos obrigatórios não preenchidos
        if( !$('#frmDisOcoGeoreferencia').valid() ) {
            return;
        }
        if( erros.length > 0 )
        {
            app.alertInfo(erros.join('<br>') );
            return;
        }
        $("#stLocalidadeDesconhecida").prop('checked',false);
        data.stLocalidadeDesconhecida='N';

        // enviar os codigos dos campos Ref. Aproximação e Metodologia Aproximação porque se for ESTADO e CENTROIDE,
        // não precisa calcular a uc, municipio e bioma
        data.cdReferenciaAproximacao = $("#sqApoioRefAproximacao option:selected").data('codigo');
        data.cdMetodologiaAproximacao = $("#sqApoioMetodoAproximacao option:selected").data('codigo');
        if( data.nuAnoRegistro ){
            data.inDataDesconhecida = 'N'
        } else {
            data.inDataDesconhecida = 'S'
        }

        app.blockUI('Gravando e atualizando Estado, município, bioma, bacias e UCs na ficha. Aguarde...',function(){
            app.ajax(app.url + 'fichaDisOcorrencia/saveFrmDisOcoGeoreferencia', data, function(res) {
                app.unblockUI()
                if ( ! res.status) {
                    if( res.data && res.data.msgBiomaGrupoErrado ) {
                        app.alertInfo(res.data.msgBiomaGrupoErrado, 'Possivel inconsistência')
                    }
                    app.resetChanges('frmDisOcoGeoreferencia')
                    $("#isCentroide").val('N');
                    // aturalizar aba 7.4
                    if (typeof presencaUc != 'undefined' && presencaUc.updateGridPresencaUc) {
                        presencaUc.updateGridPresencaUc();
                    }
                    // limpar form ref bib temp
                    app.selectTab("tabSelRefBibTemp");
                    $("#frmSelRefBibFichaTemp #sqPublicacao").empty();
                    tabGeoreferencia.updateMarker(true);
                    app.disableTab('ulTabsDisOcorrencia', false); // habilitar as outras abas

                    /*
                    try {
                        distribuicao.refreshLayer();
                    } catch (e) {}
                    */


                    // mostrar no gride a página do registro
                    /*ocorrencia.updateGridOcorrencia({
                        idFiltro            :res.data.sqFichaOcorrencia,
                        bdFiltro            :'salve',
                        scroll              :true,
                        //rowId               :'salve-'+res.data.sqFichaOcorrencia
                    });*/


                    // inclusao
                    if ( ! $("#sqFichaOcorrencia").val() ) {
                        // editar a ficha depois da inclusão
                        if( res.data.sqFichaOcorrencia ) {
                            window.setTimeout(function(){
                                distribuicao.editarOcorrencia({sqFicha:$("#sqFicha").val(),sqFichaOcorrencia:res.data.sqFichaOcorrencia})
                                // atualizar o gride
                                ocorrencia.updateGridOcorrencia({
                                    idFiltro            :res.data.sqFichaOcorrencia,
                                    bdFiltro            :'salve',
                                    scroll              :true,
                                    //rowId               :'salve-'+res.data.sqFichaOcorrencia
                                })
                                // atualizar o mapa de cima
                                try {
                                    distribuicao.refreshLayer({msg:''});
                                } catch (e) {}

                            },2500);
                        }
                    } else {
                        // atualizar o gride
                        ocorrencia.updateGridOcorrencia({
                            idFiltro            :res.data.sqFichaOcorrencia,
                            bdFiltro            :'salve',
                            scroll              :true,
                            //rowId               :'salve-'+res.data.sqFichaOcorrencia
                        })

                        // atualizar o mapa de cima
                        try {
                            distribuicao.refreshLayer();
                        } catch (e) {}
                    }
                }
            }, null, 'JSON');
        });

    },
    cancelarEdicaoOcorrencia: function(params) {
        // configurar a tela
        ocorrencia.clearTabs(false);
    },
    mapInit: function() {
        if( ocorrencia.oMap )
        {
            return;
        }
        ocorrencia.oMap=true;
        // se alterar a coordenada recalcular Pais, Estado, Municipio, Bioma e UC
        $("#fldLatitude,#fldLongitude,#fldGLat,#fldMLat,#fldSLat,#fldHLat,#fldGLon,#fldMLon,#fldSLon,#fldHLon").off('change').on("change",function(){
            tabGeoreferencia.updateMarker(false);
            $("#sqControle").empty()
            $("#frmDisOcoGeoreferencia #sqPaisUc").val('');
            $("#frmDisOcoGeoreferencia #sqEstadoUc").val('');
            $("#frmDisOcoGeoreferencia #sqMunicipioUc").val('');
            $("#frmDisOcoGeoreferencia #noMunicipioUc").val('');
            $("#frmDisOcoLocalidade #sqPais").val('');
            $("#frmDisOcoLocalidade #sqEstado").val('');
            $("#frmDisOcoGeoreferencia #sqBiomaUc").val('');
            $("#frmDisOcoGeoreferencia #noBiomaUc").val('');
            $('#frmDisOcoLocalidade #sqMunicipio').empty()
            if( $("#fldLatitude").val()!='' && $("#fldLongitude").val()!='' )
            {
                tabGeoreferencia.getUcByCoord();
            }
        });
        ocorrencia.oMap = new Ol3Map({
            el: 'mapTabOcorrencia'
            , height: 670
            , readOnly: true
            , showToolbar: true
            , showEoo: false
            , showAoo: false
            , showZee : showZee
            , showUcFederal:true
            , expandLayerSwitcher:false
            , sqFicha: $("#sqFicha").val()
        });
        ocorrencia.oMap.run();

        // adicionar evento sigle click para seleção do ponto com a tecla controls pressionada
        ocorrencia.oMap.map.on('singleclick', function(evt) {
            if ( canModify && evt && evt.coordinate && ( evt.originalEvent.ctrlKey || evt.originalEvent.metaKey ) ) {
                //evt.coordinate = ol.proj.toLonLat(evt.coordinate, 'EPSG:3857');
                ocorrencia.clickCoordinate = evt.coordinate;
                var latLon = ol.proj.toLonLat(evt.coordinate, 'EPSG:3857');
                $('#fldLongitude').val(latLon[0].toPrecision(10));
                $('#fldLatitude').val(latLon[1].toPrecision(10));
                //$('#fldLongitude').val( evt.coordinate[0].toPrecision(10) );
                //$('#fldLatitude').val( evt.coordinate[1].toPrecision(10) );

                $('#sqApoioDatum').val($("#sqApoioDatum option[data-codigo=SIRGAS_2000]").val());

                if (!$('#sqApoioPrecisaoCoordenada').val()) {
                    $('#sqApoioPrecisaoCoordenada').val($("#sqApoioPrecisaoCoordenada option[data-codigo=APROXIMADA]").val());
                    $('#sqApoioPrecisaoCoordenada').change();
                }
                tabGeoreferencia.setGMSFields();
                tabGeoreferencia.updateMarker(false);
                $("#stLocalidadeDesconhecida").prop('checked',false);
            }
        });
    },
    setGMSFields: function() {
        // converter de gd para GMS
        var lat = $("#fldLatitude").val();
        var lon = $("#fldLongitude").val();
        if (lat) {
            var gms = app.gd2gms(lat, 'LAT');
            $("#fldGLat").val(gms.g);
            $("#fldMLat").val(gms.m);
            $("#fldSLat").val(gms.s);
            $("#fldHLat").val(gms.h.toUpperCase());
        }
        if (lon) {
            var gms = app.gd2gms(lon, 'LON');
            $("#fldGLon").val(gms.g);
            $("#fldMLon").val(gms.m);
            $("#fldSLon").val(gms.s);
            $("#fldHLon").val(gms.h.toUpperCase());
        }
    },
    setGDFields: function() {
        // converter de gms para GD
        var gLat = $("#fldGLat").val();
        var mLat = $("#fldMLat").val();
        var sLat = $("#fldSLat").val();
        var hLat = $("#fldHLat").val();
        if (gLat && mLat && sLat && hLat) {
            $("#fldLatitude").val(app.gms2gd(gLat, mLat, sLat, hLat));
        }
        var gLon = $("#fldGLon").val();
        var mLon = $("#fldMLon").val();
        var sLon = $("#fldSLon").val();
        var hLon = $("#fldHLon").val();
        if (gLon && mLon && sLon && hLon) {
            $("#fldLongitude").val(app.gms2gd(gLon, mLon, sLon, hLon));
        }
    },
    changeToCentroideMunicipio : function( ) {
        var data = {};
        var noMunicipio = $("#frmDisOcoGeoreferencia #noMunicipioUc").val();
        try {
            data.lon = parseFloat($('#fldLongitude').val());
            data.lat = parseFloat($('#fldLatitude').val());
            data.sqMunicipio = $("#frmDisOcoGeoreferencia #sqMunicipioUc").val();
        } catch( e )
        {
            data.lon = NaN;
            data.lat = NaN;
        }

        if(  isNaN( data.lat ) || isNaN( data.lon ) )
        {
            app.alertInfo('Coordenadas inválidas!');
            return;
        }

        // fazer a requisição ajax para calcular o centroid do municipio pela coordenada informada
        app.ajax(app.url + 'ficha/getCentroideMunicipio', data, function(res) {
            if ( res.status == 0 && res.data.text ) {
                if( res.data.x && res.data.y )
                {
                    $("#fldLatitude").val(parseFloat(res.data.y).toPrecision(10));
                    $("#fldLongitude").val(parseFloat(res.data.x).toPrecision(10));
                    // atualizar a marca e centralizar o mapa ( true )
                    tabGeoreferencia.updateMarker(true);
                    // atualizar os campos g m s
                    tabGeoreferencia.setGMSFields();
                    $("#isCentroide").val('S');
                    $('#sqApoioMetodoAproximacao').val($("#sqApoioMetodoAproximacao option[data-codigo=CENTROIDE]").val());
                    $('#sqApoioRefAproximacao').val($("#sqApoioRefAproximacao option[data-codigo=MUNICIPIO]").val());

                }
            };
        }, (data.sqMunicipio ? '' : 'Localizando centroide do Município '+noMunicipio+'. Aguarde!'), 'json'); // fim app.ajax
    },
    changeToCentroideEstado : function( ) {
        var data = {};
        var noMunicipio = $("#frmDisOcoGeoreferencia #noMunicipioUc").val();
        var sqEstado  = null;
        try {
            data.lon = parseFloat($('#fldLongitude').val());
            data.lat = parseFloat($('#fldLatitude').val());
            data.sqMunicipio = $("#frmDisOcoGeoreferencia #sqMunicipioUc").val();
            data.sqEstado = $("#frmDisOcoGeoreferencia #sqEstadoUc").val();
        } catch( e ) {
            data.lon = NaN;
            data.lat = NaN;
        }

        if(  isNaN( data.lat ) || isNaN( data.lon ) ) {
            app.alertInfo('Coordenadas inválidas!');
            return;
        }

        // fazer a requisição ajax para calcular o centroid do municipio pela coordenada informada
        app.ajax(app.url + 'ficha/getCentroideEstado', data, function(res) {
            if ( res.status == 0 && res.data.text ) {
                if( res.data.x && res.data.y )
                {
                    $("#fldLatitude").val(parseFloat(res.data.y).toPrecision(10));
                    $("#fldLongitude").val(parseFloat(res.data.x).toPrecision(10));
                    // atualizar a marca e centralizar o mapa ( true )
                    tabGeoreferencia.updateMarker(true);
                    // atualizar os campos g m s
                    tabGeoreferencia.setGMSFields();
                    $("#isCentroide").val('S');
                    $('#sqApoioMetodoAproximacao').val($("#sqApoioMetodoAproximacao option[data-codigo=CENTROIDE]").val());
                    $('#sqApoioRefAproximacao').val($("#sqApoioRefAproximacao option[data-codigo=ESTADO]").val());
                }
            };
        }, (data.sqMunicipio ? '' : 'Localizando centroide do Estado. Aguarde!'), 'json'); // fim app.ajax
    },
    updateMarker: function(centerMap) {
        if ( ! ocorrencia.oMap ) {
            return;
        }
        // se os campos GMS estiverem visívies, converter para GD antes plotar no mapa
        if ($("#divLatGD").hasClass('hidden')) {
            tabGeoreferencia.setGDFields();
        }
        var data = {};
        data.fldLongitude = parseFloat($('#fldLongitude').val());
        data.fldLatitude = parseFloat($('#fldLatitude').val());
        if (ocorrencia.isLatLon(data.fldLongitude, data.fldLatitude, null, false)) {
            if (centerMap) {
                ocorrencia.oMap.map.getView().setCenter(ol.proj.fromLonLat([data.fldLongitude, data.fldLatitude]));
            } else {
                // verificar se a coordenada esta em uma uc e seleciona-la
                tabGeoreferencia.getUcByCoord();
            }
            ocorrencia.showMarker(data);
        }
    },
    changeGDGMS: function() {
        if ($("#divLatGD").hasClass('hidden')) {
            $("#divLatGD,#divLonGD").removeClass('hidden');
            $("#divLatGMS,#divLonGMS").addClass('hidden');
            $(".brLatLon").addClass('hidden');
            tabGeoreferencia.setGDFields();
        } else {
            $("#divLatGD,#divLonGD").addClass('hidden');
            $("#divLatGMS,#divLonGMS").removeClass('hidden');
            $(".brLatLon").removeClass('hidden');
            tabGeoreferencia.setGMSFields();
        }
    },
    sqApoioPrecisaoCoordenadaChange: function(params) {
        var codigo = $("#sqApoioPrecisaoCoordenada option:selected").data('codigo');
        if (codigo != 'APROXIMADA') {
            $("#sqApoioRefAproximacao,#sqApoioMetodoAproximacao").parent().addClass('hidden');
        } else {
            $("#sqApoioRefAproximacao,#sqApoioMetodoAproximacao").parent().removeClass('hidden');
        }
    },
    getCentroide: function(evt) {

        // limpar os campos
        $('#sqApoioDatum').val('');
        $('#inPresencaAtual').val('');
        $('#sqApoioPrazoCarencia').val('');
        $('#sqApoioMetodoAproximacao').val('');
        $('#sqApoioPrecisaoCoordenada').val('');
        $('#sqApoioPrecisaoCoordenada').change();
        $('#sqApoioFormatoCoordOriginal').val('');
        $('#sqApoioRefAproximacao').val('');
        // limpar campos da aba localidade
        $("#frmDisOcoGeoreferencia #sqPaisUc").val('');
        $("#frmDisOcoGeoreferencia #sqEstadoUc").val('');
        $("#frmDisOcoGeoreferencia #sqMunicipioUc").val('');
        $("#frmDisOcoGeoreferencia #noMunicipioUc").val('');
        $("#frmDisOcoGeoreferencia #sqBiomaUc").val('');
        $("#frmDisOcoGeoreferencia #noBiomaUc").val('');
        $("#frmDisOcoLocalidade #sqPais").val('');
        $("#frmDisOcoLocalidade #sqEstado").val('');
        $('#frmDisOcoLocalidade #sqMunicipio').empty();
        $("#isCentroide").val('N');

        var id = evt.currentTarget.id;
        if (id) {
            var fld = $("#" + id);
            if (fld.val()) {
                // ler centroide, uf e municipio
                var data = {
                    sqUc: fld.val()
                };
                app.ajax(app.url + 'ficha/getCentroide', data, function(res) {
                    if (res.status == 0 && res.data.text) {
                        // ler somente as coordenadas
                        $("#fldLatitude").val(parseFloat(res.data.y).toPrecision(10));
                        $("#fldLongitude").val(parseFloat(res.data.x).toPrecision(10));
                        $('#sqApoioDatum').val($("#sqApoioDatum option[data-codigo=SIRGAS_2000]").val());
                        $('#sqApoioFormatoCoordOriginal').val($("#sqApoioFormatoCoordOriginal option[data-codigo=GRAUS_DECIMAIS]").val());
                        $('#sqApoioPrecisaoCoordenada').val($("#sqApoioPrecisaoCoordenada option[data-codigo=APROXIMADA]").val());
                        $('#sqApoioMetodoAproximacao').val($("#sqApoioMetodoAproximacao option[data-codigo=CENTROIDE]").val());
                        $('#sqApoioPrecisaoCoordenada').change();
                        $('#sqApoioRefAproximacao').val($("#sqApoioRefAproximacao option[data-codigo=UNIDADE_CONSERVACAO]").val());
                        // atualizar a marca e centralizar o mapa ( true )
                        tabGeoreferencia.updateMarker(true);
                        // atualizar os campos g m s
                        tabGeoreferencia.setGMSFields();
                        window.setTimeout(function() {
                            ocorrencia.oMap.setZoom(10);
                        }, 500);

                        if ($("#frmDisOcoLocalidade #sqEstado").size() > 0) {
                            if (res.data.sqPais) {
                                $("#frmDisOcoLocalidade #sqPais").val(res.data.sqPais);
                            }
                            if (res.data.sqEstado) {
                                $("#frmDisOcoLocalidade #sqEstado").val(res.data.sqEstado);
                            }
                            // se o municipio não tiver na lista do select2, limpar a lista e criar o municipio no select2
                            if (res.data.sqMunicipio) {
                                $('#frmDisOcoLocalidade #sqMunicipio').empty();
                                app.setSelect2('sqMunicipio', {
                                    'id': res.data.sqMunicipio,
                                    'descricao': res.data.noMunicipio
                                }, res.data.sqMunicipio, 'frmDisOcoLocalidade', false);
                            }
                        }
                        if (res.data.sqPais) {
                            $("#frmDisOcoGeoreferencia #sqPaisUc").val(res.data.sqPais);
                        }
                        if (res.data.sqEstado) {
                            $("#frmDisOcoGeoreferencia #sqEstadoUc").val(res.data.sqEstado);
                        }
                        if (res.data.sqMunicipio) {
                            $("#frmDisOcoGeoreferencia #sqMunicipioUc").val(res.data.sqMunicipio);
                            $("#frmDisOcoGeoreferencia #noMunicipioUc").val(res.data.noMunicipio);
                        }
                        if (res.data.sqBioma) {
                            $("#frmDisOcoGeoreferencia #sqBiomaUc").val(res.data.sqBioma);
                            $("#frmDisOcoGeoreferencia #noBiomaUc").val(res.data.noBioma);
                        }
                        // bloquear/desbloquear campos Pais, Municipio e Estado da aba 2.6.2-Dados do Registro
                        tabLocalidade.configFields();
                    } // fim if
                }, 'Localizando o Estado e Município. Aguarde!', 'json'); // fim app.ajax
            }
        }
        tabLocalidade.configFields();
    },
    getUcByCoord: function() {
        var lat = parseFloat($('#fldLatitude').val());
        var lon = parseFloat($('#fldLongitude').val());
        if (!isNaN(lat) && !isNaN(lon)) {
            // limpar os campos
            $("#isCentroide").val('N');
            $("#sqControle").empty();
            //$('#inPresencaAtual').val('');
            //$('#sqApoioPrazoCarencia').val('');
            // valores  padrão ao clicar no mapa
            $('#sqApoioDatum').val($("#sqApoioDatum option[data-codigo=SIRGAS_2000]").val());
            //$('#sqApoioFormatoCoordOriginal').val($("#sqApoioFormatoCoordOriginal option[data-codigo=GRAUS_DECIMAIS]").val());
            //$('#sqApoioPrecisaoCoordenada').val($("#sqApoioPrecisaoCoordenada option[data-codigo=APROXIMADA]").val());
            //$('#sqApoioRefAproximacao').val('');
            $('#sqApoioMetodoAproximacao').val($("#sqApoioMetodoAproximacao option[data-codigo=MAPA_SALVE]").val());
            $('#sqApoioPrecisaoCoordenada').change();
            //
            $("#frmDisOcoGeoreferencia #sqPaisUc").val('');
            $("#frmDisOcoGeoreferencia #sqEstadoUc").val('');
            $("#frmDisOcoGeoreferencia #sqMunicipioUc").val('');
            $("#frmDisOcoGeoreferencia #noMunicipioUc").val('');
            $("#frmDisOcoGeoreferencia #sqBiomaUc").val('');
            $("#frmDisOcoGeoreferencia #noBiomaUc").val('');
            // campos aba 7 - Localidade
            $("#frmDisOcoLocalidade #sqPais").val('');
            $("#frmDisOcoLocalidade #sqEstado").val('');
            $('#frmDisOcoLocalidade #sqMunicipio').empty();

            var data = {
                lat: lat,
                lon: lon,
                sqFicha:$("#sqFicha").val()
            }
            app.ajax(app.url + 'ficha/getUcByCoord', data, function(res) {
                if( res.msgAlertaBioma ){
                    app.alertInfo(res.msgAlertaBioma,'Possível inconsistência')
                }
                // tem uc
                if (res.sqControle) {
                    $('#frmDisOcoGeoreferencia #sqControle').empty()
                    app.setSelect2('sqControle', {
                        'id': res.sqControle,
                        'descricao': res.noUc
                    }, res.sqControle, 'frmDisOcoGeoreferencia', false);
                    $('#sqApoioRefAproximacao').val($("#sqApoioRefAproximacao option[data-codigo=UNIDADE_CONSERVACAO]").val());
                    $('#sqApoioPrecisaoCoordenada').change();
                }
                // se a aba localidade estiver visivel, carregar Pais, Estado e Município
                if ($("#frmDisOcoLocalidade #sqEstado").size() > 0) {

                    if (res.sqPais) {
                        $("#frmDisOcoLocalidade #sqPais").val(res.sqPais);
                    }
                    if (res.sqEstado) {
                        $("#frmDisOcoLocalidade #sqEstado").val(res.sqEstado);
                    }
                    if (res.sqMunicipio) {
                        $('#frmDisOcoLocalidade #sqMunicipio').empty();
                        app.setSelect2('sqMunicipio', {
                            'id': res.sqMunicipio,
                            'descricao': res.noMunicipio
                        }, res.sqMunicipio, 'frmDisOcoLocalidade', false);
                    }
                }
                $("#frmDisOcoGeoreferencia #sqPaisUc").val(res.sqPais);
                $("#frmDisOcoGeoreferencia #sqEstadoUc").val(res.sqEstado);
                $("#frmDisOcoGeoreferencia #sqMunicipioUc").val(res.sqMunicipio);
                $("#frmDisOcoGeoreferencia #noMunicipioUc").val(res.noMunicipio);
                $("#frmDisOcoGeoreferencia #sqBiomaUc").val(res.sqBioma);
                $("#frmDisOcoGeoreferencia #noBiomaUc").val(res.noBioma);
                // bloquear/desbloquear campos Pais, Municipio e Estado da aba 2.6.2-Dados do Registro
                tabLocalidade.configFields();
                app.focus('nuAnoRegistro','frmDisOcoGeoreferencia')
            //}, 'Localizando Estado, município, bioma e unidade de conservação na coordenada informada. Aguarde!', 'json');
            }, 'Localizando Unidade de Conservação na coordenada informada. Aguarde!', 'json');
        }
    },
    sqSituacaoAvaliacaoChange:function( params, ele, evt ){
        ele = ele || $("#frmDisOcoGeoreferencia select#sqSituacaoAvaliacao").get(0);
        var optionSelected = $(ele).find('option:selected');
        var value = ele.value;
        if( optionSelected.data('codigo') == 'REGISTRO_NAO_UTILIZADO_AVALIACAO'){
            $("#divMotivoNaoUtilizadoWrapper").removeClass('hidden');
            // quando não tem o params é a chamada de renderização inicial do form, sem o evento onChange do campo
            if( params ) {
                app.focus("txNaoUtilizadoAvaliacao");
            }
        } else {
            $("#divMotivoNaoUtilizadoWrapper").addClass('hidden')
            if( params ) {
                app.focus("idOrigem");
            }
        }
    }
};

// ################################################################################################################################ //
// 2.6.2 - Dados do Registro
// ################################################################################################################################ //

disOcoDadosRegistro = {
    tabDadosRegistroInit: function() {
        disOcoDadosRegistro.inDataDesconhecidaChange();
        disOcoDadosRegistro.disabledInputTaxon();
    },
    sqNivelOcoDadosDoRegistroChange: function(params) {
        $('#sqTaxonCitado').empty();
        if ($('#nivelOcoDadosDoRegistro').val()) {
            $('#sqTaxonCitado').prop('disabled', false);
        } else {
            $('#sqTaxonCitado').prop('disabled', true);
        }
    },
    saveFrmDisOcoRegistro: function(params) {
        if (!$('#frmDisOcoRegistro').valid()) {
            return false;
        }

        /* estes campos passaram para aba 2.6.1
        // validar periodo inicial
        if ($("#nuDiaRegistro").val() && !app.isDate($("#nuDiaRegistro").val() + '/' + $("#nuMesRegistro").val() + '/' + $("#nuAnoRegistro").val())) {
            return app.alertError('Valores informados nos campos <b>DIA/MÊS/ANO Inicial</b> não formam uma data válida!');
        }
        // validar perído final
        if ($("#nuDiaRegistroFim").val() && !app.isDate($("#nuDiaRegistroFim").val() + '/' + $("#nuMesRegistroFim").val() + '/' + $("#nuAnoRegistroFim").val())) {
            return app.alertError('Valores informados nos campos <b>DIA/MÊS/ANO Final</b> não formam uma data válida!');
        }

        // validar hora
        if ($("#hrRegistro").val() && !app.isTime($("#hrRegistro").val())) {
            return app.alertError('Hora inválida!');
        }
        /!*if( ! $("#sqTipoRegistro").val() )
         {
         return app.alertError('O campo <b>"Tipo do Registro""</b> é obrigatório!');
         }
         if( ! $("#inPresencaAtual").val() )
         {
         return app.alertError('O campo <b>"Presença Atual da Espécie na Coordenada"</b> é obrigatório!');
         }
         *!/
        if ($("#nuAnoRegistro").val()) {
            $("#inDataDesconhecida").val('N');
        } else {
            if (!$("#inDataDesconhecida").is(':checked')) {
                return app.alertError('Informe a data, ano ou informe que a data e ano são desconhecidos!');
            }
        }
        if ($('#nuAnoRegistro').val() && $('#nuAnoRegistro').val() < 1500) {
            return app.alertError('Informe o ANO MAIOR QUE 1500!');
        }

        if ($("#nuAnoRegistroFim").val()) {
            $("#inDataDesconhecida").val('N');
        } else {
            if (!$("#inDataDesconhecida").is(':checked')) {
                return app.alertError('Informe a data, ano ou informe que a data e ano são desconhecidos!');
            }
        }
        if ($('#nuAnoRegistroFim').val() && $('#nuAnoRegistroFim').val() < $('#nuAnoRegistro').val() ) {
            return app.alertError('Informe o ANO MAIOR QUE ' + $('#nuAnoRegistro').val() );
        }*/
        var data = $('#frmDisOcoRegistro').serializeArray();


        data = app.params2data(params, data);
        if (!data.sqFicha) {
            app.alertError('ID da ficha não encontrado!');
            return;
        }
        app.ajax(app.url + 'fichaDisOcorrencia/saveFrmDisOcoRegistro', data, function(res) {
            if (res.status === 0) {
                app.resetChanges('frmDisOcoRegistro');
            }
        }, null, 'json');
    },
    disabledInputTaxon: function() {
        if ($('#sqTaxonCitado').val()) {
            $('#sqTaxonCitado').prop('disabled', false);
        }
    },
    inDataDesconhecidaChange: function(params, fld) {
        if ($("#inDataDesconhecida").is(':checked')) {
            $("#nuDiaRegistro,#nuDiaRegistroFim").val('').prop('disabled', true);
            $("#nuMesRegistro,#nuMesRegistroFim").val('').prop('disabled', true);
            $("#nuAnoRegistro,#nuAnoRegistroFim").val('').prop('disabled', true);
            $("#hrRegistro,#hrRegistroFim").val('').prop('disabled', true);
            $("#nuAnoRegistro").removeClass('error');
        } else {
            $("#nuDiaRegistro,#nuDiaRegistroFim").prop('disabled', false);
            $("#nuMesRegistro,#nuMesRegistroFim").prop('disabled', false);
            $("#nuAnoRegistro,#nuAnoRegistroFim").prop('disabled', false);
            $("#hrRegistro").prop('disabled', false);
        }

        app.fieldChange(fld);
        return true;
    }
};


// ################################################################################################################################ //
// 2.6.3 - Localidade
// ################################################################################################################################ //
var tabLocalidade = {
    tabDisOcoLocalidadeInit: function() {
        if ( $("#frmDisOcoLocalidade #sqPais option:selected").val() == '') {
            $("#frmDisOcoLocalidade #sqPais option").each(function() {
                if (this.text == 'Brasil') {
                    //$(this).prop('selected',true)
                    $("#frmDisOcoLocalidade #sqPais").select2('val', this.value)
                        //$("#frmDisOcoLocalidade #sqPais").val( this.value );
                }
            });
        }
        tabLocalidade.configFields();

    },
    sqPaisChange:function(params)
    {
        var pais = $("#frmDisOcoLocalidade #sqPais option:selected").text().trim().toUpperCase();
        if( pais != 'BRASIL' && pais != 'BRAZIL' )
        {
            $("#frmDisOcoLocalidade #divEstado,#divMunicipio").hide();
        }
        else {
            $("#frmDisOcoLocalidade #divEstado,#divMunicipio").show();
        }
    },
    save: function(params) {
        if (!$('#frmDisOcoLocalidade').valid()) {
            return;
        }

        if( ! $("#frmDisOcoLocalidade #divEstado").is(":visible" ) )
        {
            $("#frmDisOcoLocalidade #sqEstado").val('');
            $("#frmDisOcoLocalidade #sqMunicipio").val('');
        }

        var data = $("#frmDisOcoLocalidade").serializeAllArray();
        data = app.params2data(params, data);
        data.sqControle = $("#frmDisOcoGeoreferencia #sqControle").val();

        if (!data.sqFichaOcorrencia) {
            app.alertError('ID não informado!');
            return;
        }


        if ($("#frmDisOcoRegistro #inPresencaAtual").size() == 1 && $("#sqControle").val() > 0 && !$("#inPresencaAtual").val()) {
            app.alertInfo('Para adicionar uma UC, o campo "<b>Presença Atual na Coordenada</b>" da aba anterior (Dados do Registro) deve ser preenchido e salvo!');
            return;
        }

        app.ajax(app.url + 'fichaDisOcorrencia/saveFrmDisOcoLocalidade', data, function(res) {
            // atualizar gride da aba 7.4-Presença em Uc
            if (res.status == 0) {
                app.resetChanges('frmDisOcoLocalidade');
                // TODO - atualizar a linha do gride de ocorrencias de acordo com o registro editado
                // distribuicao.updateGridOcorrencia();
            }
        }, null, 'JSON');
    },
    configFields: function() {

        if ($("#frmDisOcoGeoreferencia #sqMunicipioUc").val() && $("#sqMunicipio").val() != $("#frmDisOcoGeoreferencia #sqMunicipioUc").val()) {
            $("#frmDisOcoLocalidade #sqPais").val($("#frmDisOcoGeoreferencia #sqSqPaisUc").val());
            $("#frmDisOcoLocalidade #sqEstado").val($("#frmDisOcoGeoreferencia #sqEstadoUc").val());
            $('#frmDisOcoLocalidade #sqMunicipio').empty()
            app.setSelect2('sqMunicipio', {
                'id': $("#frmDisOcoGeoreferencia #sqMunicipioUc").val(),
                'descricao': $("#frmDisOcoGeoreferencia #noMunicipioUc").val()
            }, $("#frmDisOcoGeoreferencia #sqMunicipioUc").val(), 'frmDisOcoLocalidade', false);
        }

        if( ! $("#sqPais").hasClass('RO') ) {
            if ($("#sqHabitatPai").val()) {
                $("#sqHabitatLocalidade").prop('disabled', false)
            }

            // hablitar/desabilitar campos em função de campos de outras abas

            // 1) se tiver uc selecionada ou estiver com perfil conslta, não editar Estado

            if ($("#sqControle").val() && $("#sqPais").val()) {
                $("#sqPais").prop('disabled', true);
            } else {
                $("#sqPais").prop('disabled', false);
            }

            if ($("#sqControle").val() && $("#sqEstado").val()) {
                $("#sqEstado").prop('disabled', true);
            } else {
                $("#sqEstado").prop('disabled', false);
            }

            if ($("#sqControle").val() && $("#sqMunicipio").val()) {
                $("#sqMunicipio").prop('disabled', true);
            } else {
                $("#sqMunicipio").prop('disabled', false);
            }
        }
        // mostrar/esconder campos Estado e município se o pais não for BRASIL
        tabLocalidade.sqPaisChange();
    }
};

// ################################################################################################################################ //
// 2.6.4 - Qualificador
// ################################################################################################################################ //
var tabQualificador = {
    save: function(params) {
        if (!$('#frmDisOcoQualificador').valid()) {
            return;
        }
        var data = $("#frmDisOcoQualificador").serializeArray();
        data = app.params2data(params, data);
        if (!data.sqFichaOcorrencia) {
            app.alertError('ID não informado!');
            return;
        }
        app.ajax(app.url + 'fichaDisOcorrencia/saveFrmDisOcoQualificador', data, function(res) {

            if (res.status === 0) {
                app.resetChanges('frmDisOcoQualificador');
            }
        }, null, 'JSON');
    },
};


// ################################################################################################################################ //
// 2.6.5 - Responsáveis
// ################################################################################################################################ //
var tabResponsaveis = {
    init: function(params) {
        tabResponsaveis.inUtilizadoAvaliacaoChange();
    },
    save: function(params) {
        if (!$('#frmDisOcoResponsavel').valid()) {
            return;
        }
        /*
        if ($("#inUtilizadoAvaliacao").val() != 'N') {
            tinyMCE.get('txNaoUtilizadoAvaliacao').setContent('');
            $("#txNaoUtilizadoAvaliacao").val('');
        } else {
            if ( ! tinyMCE.get('txNaoUtilizadoAvaliacao').getContent().trim() ) {
                app.alertError('Justificativa é obrigatória!');
                return;
            }
        }
        */
        var data = $("#frmDisOcoResponsavel").serializeArray();
        data = app.params2data(params, data);
        if (!data.sqFichaOcorrencia) {
            return app.alertError('ID não informado!');
        }
        if (data.sqPessoaCompilador && !data.dtCompilacao) {
            return app.alertError('Data da compilação deve ser informada!');
        }
        if (data.sqPessoaRevisao && !data.dtRevisao) {
            return app.alertError('Data da revisão deve ser informada!');
        }
        if (data.sqPessoaValidacao && !data.dtValidacao) {
            return app.alertError('Data da validação deve ser informada!');
        }

        /*if (!data.sqPessoaCompilador && !data.sqPessoaRevisao && !data.sqPessoaValidacao) {
            return app.alertError('Dados incompletos!');
        }*/

        app.ajax(app.url + 'fichaDisOcorrencia/saveFrmDisOcoResponsavel', data, function(res) {
            if (res.status == 0) {
                app.resetChanges('frmDisOcoResponsavel')
            }
        }, null, 'JSON');
    },
    inUtilizadoAvaliacaoChange: function(params) {
        if ($("#inUtilizadoAvaliacao").val() == 'N') {
            $("#txNaoUtilizadoAvaliacao").parent().removeClass('hidden');
        } else {
            $("#txNaoUtilizadoAvaliacao").parent().addClass('hidden');
        }
    },
};

// ################################################################################################################################ //
// 2.6.6 - Referencia Bibliográfica
// ################################################################################################################################ //
var tabRefBib = {
    save: function(params) {
        if (!$('#frmDisOcoRefBib').valid()) {
            return;
        }
        var data = $("#frmDisOcoRefBib").serializeArray();
        data = app.params2data(params, data);
        if (!data.sqPublicacao) {
            return app.alertError('Selecione uma referência bibliográfica!');
        }
        app.ajax(app.url + 'fichaDisOcorrencia/saveFrmDisOcoRefBib', data, function(res) {
            if (res.status == 0) {
                tabRefBib.updateGridRefBib();
                app.reset('frmDisOcoRefBib');
                app.focus('sqPublicacao');
            }
        }, null, 'JSON');
    },
    deleteRefBib: function(params, btn) {
        app.selectGridRow(btn);
        app.confirm('<br>Confirma exclusão da Ref. Bibliográfica deste registro de ocorrência?',
            function() {
                app.ajax(app.url + 'fichaDisOcorrencia/deleteRefBib', params,
                    function(res) {
                        tabRefBib.updateGridRefBib();
                    });
            },
            function() {
                app.unselectGridRow(btn);
            }, params, 'Confirmação', 'warning');
    },
    updateGridRefBib: function(params) {
        $("#divGridDistOcoRefBib").data('sqFichaOcorrencia', $("#sqFichaOcorrencia").val());
        ficha.getGrid('distOcoRefBib')
    },

};

/*************************************************************
 * 2.6.7 - MULTIMIDIA
 *************************************************************/
var tabMultimidia = {
    updateGride: function()
    {
        var data = {
            sqFichaOcorrencia: $("#sqFichaOcorrencia").val(),
        };
        app.ajax(app.url + 'fichaMultimidia/getGridMultimidiaOcorrencia', data,
            function(res) {
                $("#divGridDistOcoMultimidia").html(res);
                bindDialogPreview('divGridDistOcoMultimidia');
            });
    },
    sqTipoChange: function(params) {
        if (!$("#frmDisOcoMultimidia #sqSituacao").val()) {
            $("#frmDisOcoMultimidia #sqSituacao").val($("#frmDisOcoMultimidia #sqSituacao option[data-codigo=PENDENTE_APROVACAO]").val())
        }
        var dataAllowedPreviewTypes = ['audio'];
        var dataAllowedFileEextensions = ['mp3','ogg','wav'];
        var label = 'Selecionar'

        if ($("#frmDisOcoMultimidia #sqTipo").val()) {
            $("#frmDisOcoMultimidia #legend_tipo_arquivo").html($("#frmDisOcoMultimidia #sqTipo option:selected").text());
            var codigo = $("#frmDisOcoMultimidia #sqTipo option:selected").data('codigo');
            if (codigo == 'IMAGEM') {
                dataAllowedPreviewTypes = ['image'];
                dataAllowedFileEextensions = ['jpg', 'bmp', 'png', 'gif'];
                label = 'Selecionar Imagem'
                $("#frmDisOcoMultimidia #inPrincipal").parent().show();
            } else if (codigo == 'SOM') {
                label = 'Selecionar áudio'
                $("#frmDisOcoMultimidia #inPrincipal").parent().hide();
                $("#frmDisOcoMultimidia #inPrincipal").prop('checked', false)
            }
            $('#frmDisOcoMultimidia #fldAnexo').fileinput('destroy');
            $('#frmDisOcoMultimidia #fldAnexo').fileinput({
                'language': fileInputLanguage,
                'layoutTemplates': fileInputLayoutTemplates,
                'previewFileIconSettings': fileInputPreviewFileIconSettings,
                'browseLabel': label,
                'previewFileType': dataAllowedPreviewTypes,
                'showPreview': true,
                'showCaption': false,
                'showUpload': false,
                'allowedPreviewTypes': dataAllowedPreviewTypes,
                'allowedFileExtensions': dataAllowedFileEextensions,
            });
            $("#frmDisOcoMultimidia #fldAnexo").parent().parent().parent().show();
        } else {
            $("#frmDisOcoMultimidia #legend_tipo_arquivo").html('?');
            $("#frmDisOcoMultimidia #fldAnexo").parent().parent().parent().hide();
            $("#frmDisOcoMultimidia #inPrincipal").parent().hide();
            $("#frmDisOcoMultimidia #inPrincipal").prop('checked', false)
        }
    },
    save: function(params) {
        if (!$("#frmDisOcoMultimidia").valid()) {
            return;
        }
        var data = $("#frmDisOcoMultimidia").serializefiles();
        data.append('sqFicha', params.sqFicha);
        data.append('sqFichaOcorrencia', params.sqFichaOcorrencia);

        if (!data.get('sqFicha')) {
            app.alertError('ID da ficha não encontrado!');
            return;
        }
        if (!data.get('sqFichaOcorrencia')) {
            app.alertError('ID da ocorrência não encontrado!');
            return;
        }
        app.ajax(app.url + 'fichaMultimidia/save', data,
            function(res) {
                if (res.status == 0) {
                    app.reset('frmDisOcoMultimidia');
                    app.focus('sqTipo','frmDisOcoMultimidia');
                    tabMultimidia.updateGride();
                    $("#frmDisOcoMultimidia #btnResetFrmDisOcoMultimidia").addClass('hide');
                }
            }, 'Gravando...', 'json');
    },
    reset: function(params) {
        app.reset('frmDisOcoMultimidia');
        $("#frmDisOcoMultimidia #btnResetFrmDisOcoMultimidia").addClass('hide');
        app.unselectGridRow('divGridDistOcoMultimidia');
        app.focus('sqTipo','frmDisOcoMultimidia');
    },

    edit: function(params, btn) {
        var data = { id:params.sqFichaMultimidia };
        app.ajax(app.url + 'fichaMultimidia/edit', data, function(res) {
            if (res) {
                $("#frmDisOcoMultimidia").collapse('show');
                tabMultimidia.reset();
                app.selectGridRow(btn);
                app.setFormFields(res, 'frmDisOcoMultimidia');
                $("#frmDisOcoMultimidia #sqFichaOcorrenciaMultimidia").val(params.sqFichaOcorrenciaMultimidia);
                tabMultimidia.sqTipoChange();
                $("#btnResetFrmDisOcoMultimidia").removeClass('hide');
            }
        }, 'Carregando...', 'JSON');
    },
    delete: function(params, btn) {
        var data = { id:params.sqFichaMultimidia
            ,sqFichaOcorrenciaMultimidia:params.sqFichaOcorrenciaMultimidia };
        app.selectGridRow(btn);
        app.confirm('<br>Confirma exclusão do arquivo <b>' + params.descricao + '</b>?',
            function() {
                app.ajax(app.url + 'fichaMultimidia/delete',
                    data,
                    function(res) {
                        if (!res) {
                            var tr1 = btn.closest('tr');
                            var tr2 = tr1.next('tr');
                            if( tr2.hasClass('tr-descricao') )
                            {
                                tr2.remove();
                            }
                            tr1.remove();
                        }
                    });
            },
            function() {
                app.unselectGridRow(btn);
            }, params, 'Confirmação', 'warning');
    },
}
