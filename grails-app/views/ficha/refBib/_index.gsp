<div style="padding:10px;">
    <label class="control-label">Referências Bibliográficas<i data-action="ficha.openModalSelRefBibFicha"
                                                              data-com-pessoal="true"
                                                              data-no-tabela="ficha"
                                                              data-no-coluna="ficha"
                                                              data-sq-registro="${ficha.id}"
                                                              data-de-rotulo="Ficha"
                                                              data-update-grid="allRefBib"
                                                              class="fld fa fa-book cursor-pointer ml10"
                                                              title="Adicionar referência bibliográfica"
                                                              style="color: green;" /></label>
</div>
<div id="divGridAllRefBib">
</div>
