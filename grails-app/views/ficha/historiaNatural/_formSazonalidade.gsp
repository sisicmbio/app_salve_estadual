<form id="frmHisNatSazonalidade" class="form-inline" role="form">
   <input name="sqFichaVariacaoSazonal" id="sqFichaVariacaoSazonal" type="hidden" value=""/>
   <div class="form-group">
      <label for="" class="control-label">Fase de Vida</label>
      <br>
      <select name="sqFaseVida" class="fld form-control fld200 fldSelect">
         <option value="">-- selecione --</option>
         <g:each var="item" in="${listFaseVida}">
            <option value="${item.id}">${item.descricao}</option>
         </g:each>
      </select>
   </div>
   <div class="form-group">
      <label for="" class="control-label">Época</label>
      <br>
      <select name="sqEpoca" class="fld form-control fld200 fldSelect">
         <option value="">-- selecione --</option>
         <g:each var="item" in="${listEpoca}">
            <option value="${item.id}">${item.descricao}</option>
         </g:each>
      </select>
   </div>
    <div class="form-group">
      <label for="" class="control-label">Habitat</label>
      <br>
      <select id="sqHabitatPaiSazonalidade" name="sqHabitatPai" class="fld form-control fld250 fldSelect" data-form="frmHisNatSazonalidade" data-child="sqHabitatSazonalidade" data-change="ficha.habitatPaiChange"
         data-s2-placeholder="?"
         data-s2-minimum-input-length="0"
         required="true">
         <option value="">-- Selecione --</option>
         <g:each var="item" in="${listHabitat}">
            <option value="${item.id}" data-codigo="${item.codigo}">${item.descricao}</option>
         </g:each>
      </select>
      <span>-</span>
      <select id="sqHabitatSazonalidade" name="sqHabitat" class="fld form-control fldSelect select2 fld600"
          data-s2-placeholder=" "
          data-s2-minimum-input-length="0"
          disabled="true">
         <option value="">?</option>
      </select>
   </div>
   <br>
   <div class="form-group" style="width: 100%">
      <label for="" class="control-label">Observação da Sazonalidade</label>
      <br>
      <textarea name="dsFichaVariacaoSazonal" class="fld form-control fldTextarea wysiwyg" rows="7" style="width: 100%"></textarea>
   </div>

   <div class="fld panel-footer">
      <button id="btnSaveFrmHisNatSazonalidade"  data-action="historiaNatural.saveFrmHisNatSazonalidade" data-params="sqFicha" class="fld btn btn-success">Adicionar Sazonalidade</button>
      <button id="btnResetFrmHisNatSazonalidade" data-action="historiaNatural.resetFrmHisNatSazonalidade" class="fld btn btn-info hide">Limpar Formulário</button>
   </div>
</form>

<div id="divGridVariacaoSazonal">
   %{-- <g:render template="/ficha/historiaNatural/divGridVariacaoSazonal"></g:render> --}%
</div>