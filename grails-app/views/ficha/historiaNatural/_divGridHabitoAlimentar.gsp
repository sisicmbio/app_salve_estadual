<!-- view: /views/ficha/historiaNatual/_divGridHabitoAlimentar.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<table class="table table-striped table-bordered table-hover">
   <thead>
      <tr>
         <th>Tipo</th>
         %{-- <th>Observacao</th> --}%
         <th>Ref. Bibliográfica</th>
         <th>Ação</th>
      </tr>
   </thead>
   <tbody>
      <g:each var="item" in="${listHabitoAlimentar}">
      <tr>
         <td>${item.tipoHabitoAlimentar.descricao}</td>
         %{-- <td>${item.dsFichaHabitoAlimentar}</td> --}%
         <td>
               <a data-action="ficha.openModalSelRefBibFicha"
                  data-no-tabela="ficha_habito_alimentar"
                  data-no-coluna="sq_ficha_habito_alimentar"
                  data-sq-registro="${item.id}"
                  data-de-rotulo="Hábito Alimentar - ${item?.tipoHabitoAlimentar?.descricao} "
                  data-update-grid="divGridHabitoAlimentar"
                  class="fld btn btn-default btn-xs btn-grid-subform" title="Adicionar/Remover Refeferência bibliográfica."><span class="fa fa-book"></span></a>
                  &nbsp;
                  ${raw(item.refBibHtml)}
               </td>

        </td>
         <td class="td-actions nowrap">
             <a data-action="historiaNatural.editHabitoAlimentar"  data-id="${item.id}" class="fld btn btn-default btn-xs btn-update" title="Alterar">
                <span class="glyphicon glyphicon-pencil"></span>
             </a>
             <a data-action="historiaNatural.deleteHabitoAlimentar" data-descricao="${item.tipoHabitoAlimentar.descricao}" data-id="${item.id}" class="fld btn btn-default btn-xs btn-delete" title="Excluir">
                <span class="glyphicon glyphicon-remove"></span>
             </a>
        </td>
      </tr>
      </g:each>
   </tbody>
</table>
