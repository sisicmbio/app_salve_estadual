<!-- view: /views/ficha/historiaNatual/_divGridHabitoAlimentarEspecialista.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<table class="table table-striped table-bordered table-hover">
   <thead>
      <tr>
         <th>Nível</th>
         <th>Táxon</th>
         <th>Categoria</th>
         <th>Observação</th>
         <th>Ref. Bibliográfica</th>
         <th>Ação</th>
      </tr>
   </thead>
   <tbody>
		<g:each var="item" in="${listHabitoAlimentarEsp}">
      <tr>
	           <td>${item.taxon.nivelTaxonomico.deNivelTaxonomico}</td>
	           <td class="nowrap">${item.taxon.noCientifico}</td>
	           <td>${ ( item?.categoriaIucn ? item.categoriaIucn.codigo+' - '+ item.categoriaIucn.descricao: '' )}</td>
                <td class="text-justify">
                     <g:renderLongText table="ficha_habito_alimentar_esp" id="${item.id}"
                                  column="ds_ficha_habito_alimentar_esp"
                                  text="${ raw( item.dsFichaHabitoAlimentarEsp ) }"/>
                </td>

            <td>
               <a data-action="ficha.openModalSelRefBibFicha"
                  data-no-tabela="ficha_habito_alimentar_esp"
                  data-no-coluna="sq_ficha_habito_alimentar_esp"
                  data-sq-registro="${item.id}"
                  data-de-rotulo="Hábito Alimentar Esp.: (${item?.taxon?.nivelTaxonomico?.deNivelTaxonomico+') '+item?.taxon?.noCientifico}"
                  data-update-grid="divGridHabitoAlimentarEsp"
                  data-com-pessoal="true"
                  class="fld btn btn-default btn-xs btn-grid-subform" title="Adicionar/Remover Refeferência bibliográfica."><span class="fa fa-book"></span></a>
                  &nbsp;
                  ${raw(item.refBibHtml)}
               </td>
         </td>
			   <td class="td-actions nowrap">
	               <a data-action="historiaNatural.editHabitoAlimentarEsp" data-id="${item.id}" class="fld btn btn-default btn-xs btn-update" title="Alterar">
	                  <span class="glyphicon glyphicon-pencil"></span>
	               </a>
	               	<a data-action="historiaNatural.deleteHabitoAlimentarEsp" data-descricao="${item.taxon.noTaxon}" data-id="${item.id}" class="fld btn btn-default btn-xs btn-delete" title="Excluir">
	                  <span class="glyphicon glyphicon-remove"></span>
	               </a>
        </td>
      </tr>
   		</g:each>
   </tbody>
</table>
