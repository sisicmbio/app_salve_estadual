<form id="frmHisNatInteracaoDetalhe" class="form-inline" role="form">
	<input name="sqFichaInteracao" id="sqFichaInteracao" type="hidden" value="" />
      <div class="page-header group-header">
            <h3>Interações</h3>
      </div>
      <div class="fld form-group">
         <label for="" class="control-label">Tipo</label>
         <br>
		<select name="sqTipoInteracao" class="fld form-control fld200 fldSelect">
			<option value="">-- selecione --</option>
			<g:each var="item" in="${listTipoInteracao}">
			<option value="${item.id}">${item.descricao}</option>
			</g:each>
         </select>
      </div>
      %{-- sugerido pelo Carlos para ocultar por enquanto
      <div class="fld form-group">
         <label for="" class="control-label">Classificacao</label>
         <br>
		<select name="sqClassificacao" class="fld form-control fld200 fldSelect">
			<option value="">-- selecione --</option>
			<g:each var="item" in="${listClassificacao}">
			<option value="${item.id}">${item.descricao}</option>
			</g:each>
         </select>
      </div> --}%
	  <div class="fld form-group">
		<label for="" class="control-label">Nivel Taxonômico</label>
        <br>
		<select id="nivelInteracao" name="nivelInteracao" data-change="historiaNatural.sqNivelTaxonomicoInteracaoChange" class="fld form-control fld200 fldSelect">
			<option value="">-- selecione --</option>
			<g:each var="item" in="${listNivelTaxonomico}">
			<option value="${item.coNivelTaxonomico}">${item.deNivelTaxonomico}</option>
			</g:each>
		</select>
	  </div>
      <div class="fld form-group">
         <label for="" class="control-label">Taxon</label>
		<br/>
		<select id="sqTaxonInteracao" name="sqTaxonInteracao" data-change="historiaNatural.sqTaxonInteracaoChange" data-s2-visible="false" class="fld form-control select2 fld250"
			data-s2-params="nivelInteracao|nivel"
			data-s2-url="ficha/select2Taxon"
			data-s2-placeholder="?"
			data-s2-maximum-selection-length="3"
			required="true">
         </select>
      </div>
	  <br class="fld">
      <div class="fld form-group hide">
         <label for="" class="control-label">Categoria</label>
         <br>
		<select name="sqCategoriaIucn" id="sqCategoriaIucn" class="fld form-control fldSelect">
			<option value="">-- selecione --</option>
			<g:each var="item" in="${listCategoriaIucn}">
			<option value="${item.id}" data-codigo="${item.codigo}">${item.descricaoCompleta}</option>
			</g:each>
         </select>
          <span class="hidden" id="sqCategoriaIucnLoading"></span>
      </div>

	%{--
	  sugerido por Carlos para ocultar por enquanto
      <br class="fld">
      <div class="form-group w100p">
		<label for="dsFichaInteracao" class="control-label">Observação Sobre a Interação</label>
         <br>
		<textarea name="dsFichaInteracao" class="fld form-control fldTextarea wysiwyg" rows="7" style="width: 100%"></textarea>
      </div>
 	--}%

      <div class="fld panel-footer">
		<button id="btnSaveFrmHisNatInteracaoDetalhe" data-action="historiaNatural.saveFrmHisNatInteracaoDetalhe" data-params="sqFicha" class="fld btn btn-success">Adicionar Interação</button>
        <button id="btnResetFrmHisNatInteracaoDetalhe" data-action="historiaNatural.resetFrmHisNatInteracaoDetalhe" class="fld btn btn-info hide">Limpar Formulário</button>
      </div>
</form>

<div id="divGridInteracao" class="mt10">
	%{-- <g:render template="/ficha/historiaNatural/divGridInteracao"></g:render> --}%
</div>

<form id="frmHisNatInteracao" name="frmHisNatInteracao" class="form-inline" role="form">
	<div class="form-group" style="width: 100%">
		<label for="dsInteracao" class="control-label">Observações Gerais Sobre as Interações
        <i data-action="ficha.openModalSelRefBibFicha"
           data-no-tabela="ficha"
           data-no-coluna="ds_interacao"
           data-sq-registro="${ficha.id}"
           data-de-rotulo="Interações"
           class="fa fa-book ml5" title="Informar Referência Bibliográfica" style="color: green;" />
        </label>
		<g:if test="${ficha?.taxon?.nivelTaxonomico?.coNivelTaxonomico == 'ESPECIE'}">
			<a class="btn btn-default btn-xs ml10" href="javascript:void(0);" data-sq-ficha="${ficha.id}" data-action="historiaNatural.copiarObsInteracaoSubespecie" title="Clique aqui para importar as observações da última subespécie alterada!">Copiar da Subespécie</a>
		</g:if>
		<br>
		<textarea id="dsInteracao" name="dsInteracao" class="fld form-control fldTextarea wysiwyg" rows="7" style="width: 100%" required>${ficha.dsInteracao}</textarea>
	</div>

	<div class="fld panel-footer">
		<button data-action="historiaNatural.saveFrmHisNatInteracao" data-params="sqFicha" class="fld btn btn-success">Gravar Observações</button>
	</div>
</form>
