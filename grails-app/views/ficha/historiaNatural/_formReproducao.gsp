<form id="frmHisNatReproducao" class="form-inline" role="form">
   <input name="id" id="id" type="hidden" value="" />
   <div class="form-group">
      <label for="sqModoReproducao" class="control-label">Modo de Reprodução</label>
      <br>
      <select name="sqModoReproducao" id="sqModoReproducao" class="fld form-control fld200 fldSelect">
         <option value="">-- selecione --</option>
         <g:each var="item" in="${listModoReproducao}">
            <option data-codigo="${item.codigo}" value="${item.id}" ${ficha.modoReproducao?.id==item.id ? ' selected' : ''}>${item.descricao}</option>
         </g:each>
      </select>
   </div>

   <div class="form-group">
      <label for="sqSistemaAcasalamento" class="control-label">Sistema de Acasalamento</label>
      <br>
      <select name="sqSistemaAcasalamento" class="fld form-control fld200 fldSelect">
         <option value="">-- selecione --</option>
         <g:each var="item" in="${listSistemaAcasalamento}">
         <option value="${item.id}" ${ficha.sistemaAcasalamento?.id==item.id ? ' selected':''}>${item.descricao}</option>
         </g:each>
      </select>
   </div>

   <div class="form-group">
      <label for="vlIntervaloNascimento" class="control-label">Intervalo de Nascimentos</label>
      <br>
      <input name="vlIntervaloNascimento" type="text" class="fld form-control fld200 fldInput" data-mask="999,0" value="${ficha.vlIntervaloNascimento}"/>
   </div>
   <div class="form-group">
      <label for="sqUnidIntervaloNascimento" title="Unidade de medida do intervalo de nascimento!" class="control-label">Unidade</label>
      <br>
      <select name="sqUnidIntervaloNascimento" class="fld form-control fldSelect">
         <option value="">-- selecione --</option>
         <g:each var="item" in="${listUnidadeTempo}">
         <option value="${item.id}" ${ficha.unidTempoGestacao?.id==item.id ? ' selected':''}>${item.descricao}</option>
         </g:each>
      </select>
   </div>

   <br>

   <div class="form-group">
      <label for="vlTempoGestacao" class="control-label">Tempo de Gestação</label>
      <br>
      <input name="vlTempoGestacao" type="text" class="fld form-control fld200" value="${ficha.vlTempoGestacao}"/>
   </div>
   <div class="form-group">
      <label for="sqUnidTempoGestacao" title="Unidade de medida do tempo de gestação!" class="control-label">Unidade</label>
      <br>
      <select name="sqUnidTempoGestacao" class="fld form-control fldSelect fld200">
         <option value="">-- selecione --</option>
         <g:each var="item" in="${listUnidadeTempo}">
         <option value="${item.id}" ${ficha.unidTempoGestacao?.id==item.id? ' selected':''} >${item.descricao}</option>
         </g:each>
      </select>
   </div>

   <div class="form-group">
      <label for="" class="control-label">Tamanho da Prole</label>
      <br>
      <div class="input-group">
         <input name="vlTamanhoProle" type="text" class="fld form-control fld200" value="${ficha.vlTamanhoProle}"/>
         <span class="input-group-addon" id="sizing-addon2">individuo(s)</span>
      </div>
   </div>

   <br>

   <%-- inicio tabela dados machos e fêmeas--%>
   <div class="form-group" style="width:100%;padding-top:10px;">
      <div class="row">
         <div class="col-xs-2 text-center" style="">&nbsp;</div>
         <div class="col-xs-2 text-center" style="border-top:2px solid #91d5b9;"><b>Macho</b></div>
         <div class="col-xs-2 text-center" style="border-top:2px solid #91d5b9;"><b>Fêmea</b></div>
      </div>

      <div class="row" style="padding-bottom:5px;">
         <div class="col-xs-2 text-center"><div style="font-weight:bold;margin-top:0px;border-bottom:2px solid #91d5b9">Campo</div></div>
         <div class="col-xs-1 text-right" style="padding-right:30px;border-bottom:2px solid #91d5b9;"><b>Valor</b></div>
         <div class="col-xs-1" style="padding-left:2px;border-bottom:2px solid #91d5b9;"><b>Unidade</b></div>
         <div class="col-xs-1 text-right" style="padding-right:30px;border-bottom:2px solid #91d5b9;"><b>Valor</b></div>
         <div class="col-xs-1" style="padding-left:2px;border-bottom:2px solid #91d5b9;"><b>Unidade</b></div>
      </div>

      %{-- maturidade sexual macho e femea --}%
      <div class="row">
         <div class="col-xs-2"><div style="border-bottom:1px dashed gray;"><label class="control-lebel">Maturidade sexual</label></div></div>
         <div class="col-xs-1 text-right" style="padding-right:0px;">
            <input name="vlMaturidadeSexualMacho" type="text" class="fld form-control fld70 macho fldInput"  data-mask="999,0" value="${ficha.vlMaturidadeSexualMacho}"/></div>
            <div class="col-xs-1 text-left"  style="padding-left:2px;">
               <select name="sqUnidMaturidadeSexualMacho" class="fld form-control fld120">
                  <option value="">?</option>
                  <g:each var="item" in="${listUnidadeTempo}">
                  <option value="${item.id}" ${ficha.unidMaturidadeSexualMacho?.id==item.id?' selected':''}>${item.descricao}</option>
                  </g:each>
               </select>
      </div>
            <div class="col-xs-1 text-right" style="padding-right:0px;">
               <input name="vlMaturidadeSexualFemea" type="text" class="fld form-control fld70 femea fldInput" data-mask="999,0"  value="${ficha.vlMaturidadeSexualFemea}"/></div>
               <div class="col-xs-1 text-left"  style="padding-left:2px;">
                  <select name="sqUnidMaturidadeSexualFemea" class="fld form-control fld120">
                     <option value="">?</option>
                     <g:each var="item" in="${listUnidadeTempo}">
                     <option value="${item.id}" ${ficha.unidMaturidadeSexualFemea?.id==item.id?' selected':''} >${item.descricao}</option>
                     </g:each>
                  </select>
               </div>
            </div>
         </div>
      </div>
      %{-- peso --}%
      <div class="row">
         <div class="col-xs-2"><div style="border-bottom:1px dashed gray;"><label class="control-lebel">Peso médio (adulto)</label></div></div>
         <div class="col-xs-1 text-right" style="padding-right:0px;">
            <input name="vlPesoMacho" type="text" class="fld form-control fld70 macho fldInput" data-mask="0000" maxlength="4" value="${ficha?.vlPesoMacho?.toInteger()}"/></div>
            <div class="col-xs-1 text-left"  style="padding-left:2px;">
               <select name="sqUnidadePesoMacho" class="fld form-control fld120">
                  <option value="">?</option>
                  <g:each var="item" in="${listUnidadePeso}">
                  <option value="${item.id}" ${ficha.unidadePesoMacho?.id==item.id ? ' selected':''}>${item.descricao}</option>
                  </g:each>
               </select>
            </div>
            <div class="col-xs-1 text-right" style="padding-right:0px;">
               <input name="vlPesoFemea" type="text" class="fld form-control fld70 femea fldInput" data-mask="0000" maxlength="4" value="${ficha?.vlPesoFemea?.toInteger()}"/></div>
               <div class="col-xs-1 text-left"  style="padding-left:2px;">
                  <select name="sqUnidadePesoFemea" class="fld form-control fld120">
                     <option value="">?</option>
                     <g:each var="item" in="${listUnidadePeso}">
                     <option value="${item.id}"  ${ficha.unidadePesoFemea?.id==item.id ? ' selected': ' '}>${item.descricao}</option>
                     </g:each>
                  </select>
               </div>
            </div>
         </div>
      </div>

      %{-- comprimento máximo--}%
      <div class="row">
         <div class="col-xs-2"><div style="border-bottom:1px dashed gray;"><label class="control-lebel">Comprimento máximo</label></div></div>
         <div class="col-xs-1 text-right" style="padding-right:0px;">
            <input name="vlComprimentoMachoMax" type="text" class="fld form-control fld70 macho fldInput" data-mask="999,0" value="${ficha.vlComprimentoMachoMax}"/></div>
            <div class="col-xs-1 text-left"  style="padding-left:2px;">
               <select name="sqMedidaComprimentoMachoMax" class="fld form-control fld120">
                  <option value="">?</option>
                  <g:each var="item" in="${listUnidadeMedida}">
                  <option value="${item.id}" ${ficha.medidaComprimentoMachoMax?.id==item.id ? ' selected':''}>${item.descricao}</option>
                  </g:each>
               </select>
            </div>
            <div class="col-xs-1 text-right" style="padding-right:0px;">
               <input name="vlComprimentoFemeaMax" type="text" class="fld form-control fld70 femea fldInput" data-mask="999,0" value="${ficha.vlComprimentoFemeaMax}"/></div>
               <div class="col-xs-1 text-left"  style="padding-left:2px;">
                  <select name="sqMedidaComprimentoFemeaMax" class="fld form-control fld120">
                     <option value="">?</option>
                     <g:each var="item" in="${listUnidadeMedida}">
                     <option value="${item.id}" ${ficha.medidaComprimentoFemeaMax?.id==item.id ? ' selected':''}>${item.descricao}</option>
                     </g:each>
                  </select>
               </div>
            </div>
         </div>
      </div>

            %{-- comprimento --}%
      <div class="row">
         <div class="col-xs-2"><div style="border-bottom:1px dashed gray;"><label class="control-lebel">Comprimento na maturidade sexual</label></div></div>
         <div class="col-xs-1 text-right" style="padding-right:0px;">
            <input name="vlComprimentoMacho" type="text" class="fld form-control fld70 macho fldInput" data-mask="999,0" value="${ficha.vlComprimentoMacho}"/></div>
            <div class="col-xs-1 text-left"  style="padding-left:2px;">
               <select name="sqMedidaComprimentoMacho" class="fld form-control fld120">
                  <option value="">?</option>
                  <g:each var="item" in="${listUnidadeMedida}">
                  <option value="${item.id}" ${ficha.medidaComprimentoMacho?.id==item.id ? ' selected':''}>${item.descricao}</option>
                  </g:each>
               </select>
            </div>
            <div class="col-xs-1 text-right" style="padding-right:0px;">
               <input name="vlComprimentoFemea" type="text" class="fld form-control fld70 femea fldInput" data-mask="999,0" value="${ficha.vlComprimentoFemea}"/></div>
               <div class="col-xs-1 text-left"  style="padding-left:2px;">
                  <select name="sqMedidaComprimentoFemea" class="fld form-control fld120">
                     <option value="">?</option>
                     <g:each var="item" in="${listUnidadeMedida}">
                     <option value="${item.id}" ${ficha.medidaComprimentoFemea?.id==item.id ? ' selected':''}>${item.descricao}</option>
                     </g:each>
                  </select>
               </div>
            </div>
         </div>
      </div>

      %{-- senilidade reprodutiva --}%
      <div class="row">
         <div class="col-xs-2">
            <div style="border-bottom:1px dashed gray;"><label class="control-lebel">Senilidade reprodutiva</label>
         </div>
      </div>
      <div class="col-xs-1 text-right" style="padding-right:0px;">
         <input name="vlSenilidadeReprodutivaMacho" type="text" class="fld form-control fld70 macho fldInput" data-mask="999,0" value="${ficha.vlSenilidadeReprodutivaMacho}"/></div>
         <div class="col-xs-1 text-left"  style="padding-left:2px;">
            <select name="sqUnidadeSenilidRepMacho" class="fld form-control fld120">
               <option value="">?</option>
               <g:each var="item" in="${listUnidadeTempo}">
               <option value="${item.id}" ${ficha.unidadeSenilidRepMacho?.id==item.id ?' selected':''}>${item.descricao}</option>
               </g:each>
            </select>
         </div>
         <div class="col-xs-1 text-right" style="padding-right:0px;">
            <input name="vlSenilidadeReprodutivaFemea" type="text" class="fld form-control fld70 femea fldInput" data-mask="999,0" value="${ficha.vlSenilidadeReprodutivaFemea}"/></div>
            <div class="col-xs-1 text-left"  style="padding-left:2px;">
               <select name="sqUnidadeSenilidRepFemea" class="fld form-control fld120">
                  <option value="">?</option>
                  <g:each var="item" in="${listUnidadeTempo}">
                  <option value="${item.id}" ${ficha.unidadeSenilidRepFemea?.id==item.id ?' selected':''}>${item.descricao}</option>
                  </g:each>
               </select>
      </div>
         </div>
      </div>

      %{-- longevidade --}%
      <div class="row" style="padding-bottom:10px;">
         <div class="col-xs-2"><div style="border-bottom:1px dashed gray;"><label class="control-lebel">Longevidade</label></div></div>
         <div class="col-xs-1 text-right" style="padding-right:0px;">
            <input name="vlLongevidadeMacho" type="text" class="fld form-control fld70 macho fldInput" data-mask="999,0" value="${ficha.vlLongevidadeMacho}"/></div>
            <div class="col-xs-1 text-left"  style="padding-left:2px;">
               <select name="sqUnidadeLongevidadeMacho" class="fld form-control fld120">
                  <option value="">?</option>
                  <g:each var="item" in="${listUnidadeTempo}">
                  <option value="${item.id}" ${ficha.unidadeLongevidadeMacho?.id==item.id ? ' selected' : ''}>${item.descricao}</option>
                  </g:each>
               </select>
            </div>
            <div class="col-xs-1 text-right" style="padding-right:0px;">
               <input name="vlLongevidadeFemea" type="text" class="fld form-control fld70 femea fldInput" data-mask="999,0" value="${ficha.vlLongevidadeFemea}"/>
            </div>
            <div class="col-xs-1 text-left"  style="padding-left:2px;">
               <select name="sqUnidadeLongevidadeFemea" class="fld form-control fld120">
                  <option value="">?</option>
                  <g:each var="item" in="${listUnidadeTempo}">
                  <option value="${item.id}" ${ficha.unidadeLongevidadeFemea?.id == item.id ? ' selected':''}>${item.descricao}</option>
                  </g:each>
               </select>
            </div>
      </div>
   </div>
   </div>

   <%--   fim tabela dados machos e fêmeas--%>

   <div class="form-group" style="width: 100%">
      <label for="dsDiagnosticoMorfologico" class="control-label">Observações Sobre a Reprodução
            <i data-action="ficha.openModalSelRefBibFicha"
              data-no-tabela="ficha"
              data-no-coluna="ds_reproducao"
              data-sq-registro="${ficha.id}"
              data-de-rotulo="Reprodução"
              class="fa fa-book ml10" title="Informar Referência Bibliográfica" style="color: green;" />
      </label>
      <br>
      <textarea name="dsReproducao" class="fld form-control fldTextarea wysiwyg" rows="7" style="width: 100%">${ficha.dsReproducao}</textarea>
   </div>
   <div class="fld panel-footer">
      <button data-action="historiaNatural.saveFrmHisNatReproducao" data-params="sqFicha" class="fld btn btn-success">Gravar Reprodução</button>
   </div>
</form>
