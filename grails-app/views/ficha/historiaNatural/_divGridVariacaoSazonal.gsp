<!-- view: /views/ficha/historiaNatual/_divGridVariacaoSazonal.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<table class="table table-striped table-bordered table-hover">
   <thead>
      <tr>
         <th>Fase de Vida</th>
         <th>Época</th>
         <th>Habitat</th>
         <th>Observação</th>
         <th style="width: 200px;">Ref. Bibliográfica</th>
         <th>Ação</th>
      </tr>
   </thead>
   <tbody>
   <g:each var="item" in="${listVariacaoSazonal}">
      <tr>
         <td>${item.faseVida.descricao}</td>
         <td>${item.epoca.descricao}</td>
         <td>${item.habitat.descricao}</td>
         <td>${raw(item.dsFichaVariacaoSazonal)}</td>
         <td>
            <a data-action="ficha.openModalSelRefBibFicha"
               data-no-tabela="ficha_variacao_sazonal"
               data-no-coluna="sq_ficha_variacao_sazonal"
               data-sq-registro="${item.id}"
               data-de-rotulo="Sazonalidade/Uso Habitat - ${item.faseVida.descricao+'/'+item.epoca.descricao+'/'+item.habitat.descricao}"
               data-com-pessoal="true"
               data-update-grid="divGridVariacaoSazonal"
               class="fld btn btn-default btn-xs btn-grid-subform" title="Adicionar/Remover Refeferência bibliográfica."><span class="fa fa-book"></span></a>
               &nbsp;
               ${raw(item.refBibHtml)}
         </td>
         <td class="td-actions">
            <a data-action="historiaNatural.editVariacaoSazonal" data-id="${item.id}" class="fld btn btn-default btn-xs btn-update" title="Alterar">
               <span class="glyphicon glyphicon-pencil"></span>
            </a>
               <a data-action="historiaNatural.deleteVariacaoSazonal" data-descricao="${item.faseVida.descricao}" data-id="${item.id}" class="fld btn btn-default btn-xs btn-delete" title="Excluir">
               <span class="glyphicon glyphicon-remove"></span>
            </a>
        </td>
      </tr>
   </g:each>
   </tbody>
</table>
