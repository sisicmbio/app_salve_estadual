//# sourceURL=N:\SAE\sae\grails-app\views\ficha\historiaNatural\historiaNatural.js
//
;
historiaNatural = {
	init: function() {
		historiaNatural.stMigratoriaChange();
	},
	lerCategoriaUltimaAvaliacao: function(idForm, idCampoNivel, idCampoTaxon, idCampoCategoria) {

		// ler automaticamente a categoria da ultima avaliação nacional, se for flora ler do ws do cncflora
		var nivel = $(idForm + ' ' + idCampoNivel).val();
		if (/ESPECIE|SUBESPECIE/.test(nivel) && $(idCampoTaxon).val()) {

			var noCientifico = $(idForm + ' ' + idCampoTaxon).select2('data')[0].nmCientifico;
			if (noCientifico) {
				var reino = $(idForm + ' ' + idCampoTaxon).select2('data')[0].nmReino.toUpperCase();
				var data = {
					noCientifico: noCientifico,
					reino: reino,
					sqTaxon: $(idForm + ' ' + idCampoTaxon).val()
				}
				$(idForm + ' ' + idCampoCategoria + 'Loading').html(window.grails.spinner).removeClass('hidden');
				$(idForm + ' ' + idCampoCategoria).prop('disabled', true);
				app.ajax(app.url + 'ficha/getCategoriaUltimaAvaliacaoNacional', data,
					function(res) {
						$(idForm + ' ' + idCampoCategoria + 'Loading').addClass('hidden');
						$(idForm + ' ' + idCampoCategoria).prop('disabled', false);
						if (res.error) {
							app.alertInfo(res.error);
						}
						if (res.sqCategoriaIucn) {
							$(idForm + ' ' + idCampoCategoria).val(res.sqCategoriaIucn);
						}
					}, null, 'json');
			}
		}
	},
	sqTaxonEspecialistaChange: function(params, data, evt, editing) {
		historiaNatural.lerCategoriaUltimaAvaliacao('#frmHisNatHabitoAlimentarEsp', '#nivel', '#sqTaxonEspecialista', '#sqCategoriaIucn')
	},
	sqTaxonInteracaoChange: function(params, data, evt, editing) {
		historiaNatural.lerCategoriaUltimaAvaliacao('#frmHisNatInteracaoDetalhe', '#nivelInteracao', '#sqTaxonInteracao', '#sqCategoriaIucn')
	},
	savefrmHistoriaNatural: function(params) {
		if (!$('#frmHistoriaNatural').valid()) {
			return false;
		}
		var data = $("#frmHistoriaNatural").serializeArray();
		data = app.params2data(params, data);
		if (!data.sqFicha) {
			app.alertError('ID da ficha não encontrado!');
			return;
		}
		app.ajax(app.url + 'fichaHistoriaNatural/saveFrmHistoriaNatural', data,
			function(res) {
				if (res.status == 0) {
					app.resetChanges('frmHistoriaNatural');
				}
			}, null, 'json');
	},
	savefrmHistNatPosicaoTrofica: function(params) {
		if (!$('#frmHisNatPosicaoTrofica').valid()) {
			return false;
		}
		var data = $("#frmHisNatPosicaoTrofica").serializeArray();
		data = app.params2data(params, data);
		if (!data.sqFicha) {
			app.alertError('ID da ficha não encontrado!');
			return;
		}
		app.ajax(app.url + 'fichaHistoriaNatural/saveFrmHisNatPosicaoTrofica', data,
			function(res) {
				if (res.status == 0) {
					app.resetChanges('frmHisNatPosicaoTrofica')
				}
			}, null, 'json');
	},
	stMigratoriaChange: function(params) {
		if ($("#stMigratoria").val() != 'S') {
			$("#sqPadroDeslocamento").parent().addClass('hidden')
		} else {
			$("#sqPadroDeslocamento").parent().removeClass('hidden')
		}
	},

	//--------------------------------------------------------------------------
	//                           ABA HABITO ALIMENTAR
	//--------------------------------------------------------------------------
	tabHabitoAlimentarInit: function(data) {
		historiaNatural.updateGridHabitoAlimentar();
		historiaNatural.stHabitoAlimentEspecialistaChange(); // mostrar/esconder o
		// form Especialista
	},
	saveFrmHisNatHabitoAlimentar: function(params) {
		if (!$('#frmHisNatHabitoAlimentar').valid()) {
			return false;
		}
		var data = $("#frmHisNatHabitoAlimentar").serializeArray();
		data = app.params2data(params, data);
		if (!data.sqFicha) {
			app.alertError('ID da ficha não encontrado!');
			return;
		}
		app.ajax(app.url + 'fichaHistoriaNatural/saveFrmHisNatHabitoAlimentar',
			data,
			function(res) {
				if (res.status == 0) {
					historiaNatural.updateGridHabitoAlimentar();
					historiaNatural.resetFrmHisNatHabitoAlimentar();
					app.resetChanges('frmHisNatHabitoAlimentar');
				}
			}, null, 'json');
	},
	editHabitoAlimentar: function(params, btn) {
		app.ajax(app.url + 'fichaHistoriaNatural/editHabitoAlimentar', params,
			function(res) {
				$("#btnResetFrmHisNatHabitoAlimentar").removeClass('hide');
				$("#btnSaveFrmHisNatHabitoAlimentar")
					.data('value', $("#btnSaveFrmHisNatHabitoAlimentar").html());
				$("#btnSaveFrmHisNatHabitoAlimentar").html("Gravar Alteração");
				app.setFormFields(res, 'frmHisNatHabitoAlimentar');
				app.selectGridRow(btn);
			});
	},
	deleteHabitoAlimentar: function(params) {
		app.confirm('<br>Confirma exclusão do hábito alimentar <b>' +
			params.descricao + '</b>?',
			function() {
				app.ajax(app.url + 'fichaHistoriaNatural/deleteHabitoAlimentar',
					params,
					function(res) {
						historiaNatural.updateGridHabitoAlimentar();
					});
			},
			null, params, 'Confirmação', 'warning');
	},
	updateGridHabitoAlimentar: function() {
		ficha.getGrid('habitoAlimentar');
	},
	resetFrmHisNatHabitoAlimentar: function() {
		app.reset('frmHisNatHabitoAlimentar');
		$("#sqTipoHabitoAlimentar").focus();
		$("#sqFichaHabitoAlimentar").val(''); // o reset não está limpando este campo
		$("#btnResetFrmHisNatHabitoAlimentar").addClass('hide');
		$("#btnSaveFrmHisNatHabitoAlimentar").html($("#btnSaveFrmHisNatHabitoAlimentar").data('value'));
		app.unselectGridRow('divGridHabitoAlimentar');
	},
	stHabitoAlimentEspecialistaChange: function(e) {
		if ($("#stHabitoAlimentEspecialista").val() === 'S') {
			$("#divHabitoAlimentarEsp").removeClass('hide');
			if ($("#divGridHabitoAlimentarEsp").html() == '') {
				historiaNatural.updateGridHabitoAlimentarEsp();
			}
		} else {
			$("#divHabitoAlimentarEsp").addClass('hide');
		}
	},
	saveFrmObsHabitoAlimentar: function(params) {
		var data = $("#frmObsHabitoAlimentar").serializeArray();
		data = app.params2data(params, data);
		if (!data.sqFicha) {
			app.alertError('ID da ficha não encontrado!');
			return;
		}
		app.ajax(app.url + 'fichaHistoriaNatural/saveFrmObsHabitoAlimentar', data, function(res) {
			if (res.status == 0) {
				app.resetChanges('frmObsHabitoAlimentar')
			}
		}, null, 'json');
	},

	//--------------------------------------------------------------------------
	//	       		    HABITO ALIMENTAR ESPECIALISTA
	//--------------------------------------------------------------------------
	saveFrmHisNatHabitoAlimentarEsp: function(params) {
		if (!$('#frmHisNatHabitoAlimentarEsp').valid()) {
			return false;
		}
		var data = $("#frmHisNatHabitoAlimentarEsp").serializeArray();
		data = app.params2data(params, data);
		if (!data.sqFicha) {
			app.alertError('ID da ficha não encontrado!');
			return;
		}
		if (!$("#sqTaxonEspecialista").val()) {
			app.alertError('Selecione o Táxon!');
			return;
		}
		app.ajax(
			app.url + 'fichaHistoriaNatural/saveFrmHisNatHabitoAlimentarEsp',
			data,
			function(res) {
				historiaNatural.updateGridHabitoAlimentarEsp(); // atualizar o gride
				historiaNatural.resetFrmHisNatHabitoAlimentarEsp(); // limpar o formulário
			}, null, 'json');
	},
	updateGridHabitoAlimentarEsp: function() {
		ficha.getGrid('habitoAlimentarEsp');
	},
	resetFrmHisNatHabitoAlimentarEsp: function() {
		app.reset('frmHisNatHabitoAlimentarEsp');
		app.focus('nivel');
		app.unselectGridRow();
		$("#sqFichaHabitoAlimentarEsp").val(''); // o reset não está limpando este campo
		$("#sqTaxonEspecialista").empty();
		historiaNatural.sqNivelTaxonomicoChange();
		$("#btnResetFrmHisNatHabitoAlimentarEsp").addClass('hide');
		$("#btnSaveFrmHisNatHabitoAlimentarEsp").html($("#btnSaveFrmHisNatHabitoAlimentarEsp").data('value'));
	},
	deleteHabitoAlimentarEsp: function(params) {
		app.confirm('<br>Confirma exclusão do hábito alimentar ESPECIALISTA <b>' +
			params.descricao + '</b>?',
			function() {
				app.ajax(app.url + 'fichaHistoriaNatural/deleteHabitoAlimentarEsp',
					params,
					function(res) {
						historiaNatural.updateGridHabitoAlimentarEsp();
					});
			},
			null, params, 'Confirmação', 'warning');
	},
	editHabitoAlimentarEsp: function(params, btn) {
		app.ajax(
			app.url + 'fichaHistoriaNatural/editHabitoAlimentarEsp', params,
			function(res) {
				$("#btnResetFrmHisNatHabitoAlimentarEsp").removeClass('hide');
				$("#sqTaxonEspecialista").parent().removeClass('hide');
				$("#btnSaveFrmHisNatHabitoAlimentarEsp")
					.data('value', $("#btnSaveFrmHisNatHabitoAlimentarEsp").html());
				$("#btnSaveFrmHisNatHabitoAlimentarEsp").html("Gravar Alteração");
				app.setFormFields(res, 'frmHisNatHabitoAlimentarEsp');
				app.setSelect2('sqTaxonEspecialista', res.sqTaxonEspecialista, res.nmCientifico);
				app.selectGridRow(btn);
				historiaNatural.sqNivelTaxonomicoChange(null, null, null, true);
			});
	},
	sqNivelTaxonomicoChange: function(params, data, evt, editing) {
		if (!editing) {
			$("#sqTaxonEspecialista").empty();
		}
		var nivel = $("#nivel").val();
		if (nivel) {
			$("#sqTaxonEspecialista").parent().removeClass('hide');
			// exibir categoria IUCN somente para epecies ou subespecies
			if (/ESPECIE|SUBESPECIE/.test(nivel)) {
				$("#frmHisNatHabitoAlimentarEsp #sqCategoriaIucn").parent().removeClass('hide');
			} else {
				$("#frmHisNatHabitoAlimentarEsp #sqCategoriaIucn").parent().addClass('hide');
			}

		} else {
			$("#sqTaxonEspecialista").parent().addClass('hide');
			$("#frmHisNatHabitoAlimentarEsp #sqCategoriaIucn").parent().addClass('hide');
		}

		// limpar o campo sqCategoriaIucn
		if ($("#frmHisNatHabitoAlimentarEsp #sqCategoriaIucn").parent().hasClass('hide')) {
			$("#frmHisNatHabitoAlimentarEsp #sqCategoriaIucn").val('');
		}

	},


	//--------------------------------------------------------------------------
	//                ABA HABITAT
	//--------------------------------------------------------------------------
	tabHabitatInit: function() {
		historiaNatural.stDiferMachoFemeaHabitatChange(); // mostrar/esconder campo descrição
		historiaNatural.stEspecialistaMicroHabitatChange(); // mostrar/esconder campo descrição
		historiaNatural.updateGridHabitat(); // carregar o gride com os habitas

	},
	saveFrmHisNatHabitat: function(params) {
		if (!$('#frmHisNatHabitat').valid()) {
			return false;
		}
		if (!$('#sqHabitatPai').val()) {
			app.alertError('Selecione o Habitat');
			return false;
		}
		var data = $("#frmHisNatHabitat").serializeArray();
		data = app.params2data(params, data);
		if (!data.sqFicha) {
			app.alertError('ID da ficha não encontrado!');
			return;
		}
		app.ajax(app.url + 'fichaHistoriaNatural/saveFrmHisNatHabitat', data,
			function(res) {
				if (res.status == 0) {
					// limpar e focar no campo correto
					if ($("#sqHabitat").val()) {
						$("#sqHabitat").select2().val(null).trigger("change");
						$("#sqHabitat").focus();
					} else {
						$("#sqHabitatPai").select2().val(null).trigger("change");
						$("#sqHabitatPai").focus();
					}
					historiaNatural.updateGridHabitat();
					app.resetChanges('frmHisNatHabitat');
				}
			}, null, 'json');
	},
	updateGridHabitat: function() {
		ficha.getGrid('habitat');
	},
	deleteHabitat: function(params) {
		app.confirm('<br>Confirma exclusão do hábitat <b>' +
			params.descricao + '</b>?',
			function() {
				app.ajax(app.url + 'fichaHistoriaNatural/deleteHabitat',
					params,
					function(res) {
						historiaNatural.updateGridHabitat();
					});
			},
			null, params, 'Confirmação', 'warning');
	},
	saveFrmHisNatHabitatDetalhe: function(params) {
		if (!$('#frmHisNatHabitatDetalhe').valid()) {
			return false;
		}
		if (!$('#sqFicha').val()) {
			app.alertError('ID da ficha não encontrado.');
			return false;
		}

		var data = $("#frmHisNatHabitatDetalhe").serializeArray();
		data = app.params2data(params, data);
		app.ajax(app.url + 'fichaHistoriaNatural/saveFrmHisNatHabitatDetalhe', data,
			function(res) {
				if (res.status == 0) {
					app.resetChanges('frmHisNatHabitatDetalhe');
				}
			}, null, 'json');
	},
	stEspecialistaMicroHabitatChange: function() {
		var stEspecialistaMicroHabitat = $("#stEspecialistaMicroHabitat").val();
		if (stEspecialistaMicroHabitat == 'S') {
			$("#divDsEspecialistaMicroHabitat").removeClass('hide');
		} else {
			$("#divDsEspecialistaMicroHabitat").addClass('hide');
		}
	},
	stDiferMachoFemeaHabitatChange: function() {
		var stDiferMachoFemeaHabitatChange = $("#stDiferMachoFemeaHabitat").val();
		if (stDiferMachoFemeaHabitatChange == 'S') {
			$("#divStDiferMachoFemeaHabitat").removeClass('hide');
		} else {
			$("#divStDiferMachoFemeaHabitat").addClass('hide');
		}
	},


	//--------------------------------------------------------------------------
	//                ABA INTERACAO
	//--------------------------------------------------------------------------
	tabInteracaoInit: function() {
		historiaNatural.updateGridInteracao();
	},
	saveFrmHisNatInteracao: function(params) {

		if (!$('#frmHisNatInteracao').valid()) {
			return false;
		}
		if (!$('#sqFicha').val()) {
			app.alertError('ID da ficha não encontrado.');
			return false;
		}

		var data = $("#frmHisNatInteracao").serializeArray();
		data = app.params2data(params, data);
		app.ajax(app.url + 'fichaHistoriaNatural/saveFrmHisNatInteracao', data,
			function(res) {
				app.focus('dsInteracao');
				app.resetChanges('frmHisNatInteracao');
			}, null, 'json');
	},
	sqNivelTaxonomicoInteracaoChange: function(params, data, evt, editing) {
		if (!editing) {
			$("#sqTaxonInteracao").empty();
		}
		var nivel = $("#nivelInteracao").val();
		// esconder mostrar select do táxon
		if ($("#nivelInteracao").val()) {
			$("#sqTaxonInteracao").parent().removeClass('hide');
		} else {
			$("#sqTaxonInteracao").parent().addClass('hide');
		}
		// esconder/mostrar categoria IUCN
		if (/ESPECIE|SUBESPECIE/.test(nivel)) {
			$("#frmHisNatInteracaoDetalhe #sqCategoriaIucn").parent().removeClass('hide');
		} else {
			$("#frmHisNatInteracaoDetalhe #sqCategoriaIucn").parent().addClass('hide');
			$("#frmHisNatInteracaoDetalhe #sqCategoriaIucn").val('');
		}

	},
	saveFrmHisNatInteracaoDetalhe: function(params) {
		if (!$('#frmHisNatInteracaoDetalhe').valid()) {
			return false;
		}
		if (!$('#sqFicha').val()) {
			app.alertError('ID da ficha não encontrado.');
			return false;
		}
		var data = $("#frmHisNatInteracaoDetalhe").serializeArray();
		data = app.params2data(params, data);
		if (!$("#frmHisNatInteracaoDetalhe #sqCategoriaIucn").parent().hasClass('hide') && !data.sqCategoriaIucn) {
			app.alertError('Categoria não pode ficar em branco.');
			return false;
		}

		app.ajax(app.url + 'fichaHistoriaNatural/saveFrmHisNatInteracaoDetalhe', data,
			function(res) {
				if (res.status == 0) {
					historiaNatural.resetFrmHisNatInteracaoDetalhe();
					historiaNatural.updateGridInteracao();
					app.resetChanges('frmHisNatInteracao');
				}
			}, null, 'json');
	},
	updateGridInteracao: function() {
		ficha.getGrid('interacao');
	},
	editInteracao: function(params, btn) {
		app.ajax(app.url + 'fichaHistoriaNatural/editInteracao', params,
			function(res) {
				app.setFormFields(res, 'frmHisNatInteracaoDetalhe');
				historiaNatural.sqNivelTaxonomicoInteracaoChange();
				app.setSelect2('sqTaxonInteracao', res.sqTaxonInteracao, res.nmCientifico);
				$('#btnResetFrmHisNatInteracaoDetalhe').removeClass('hide');
				$('#btnSaveFrmHisNatInteracaoDetalhe').data('value', $('#btnSaveFrmHisNatInteracaoDetalhe').html());
				$('#btnSaveFrmHisNatInteracaoDetalhe').html('Gravar Alteração');
				app.selectGridRow(btn);
				historiaNatural.sqNivelTaxonomicoInteracaoChange(null, null, null, true);
			});
	},
	deleteInteracao: function(params) {
		app.confirm('<br>Confirma exclusão da interação com <b>' +
			params.descricao + '</b>?',
			function() {
				app.ajax(app.url + 'fichaHistoriaNatural/deleteInteracao',
					params,
					function(res) {
						historiaNatural.updateGridInteracao();
					});
			},
			null, params, 'Confirmação', 'warning');
	},
	resetFrmHisNatInteracaoDetalhe: function() {
		app.focus('sqTipoInteracao');
		app.reset('frmHisNatInteracaoDetalhe');
		$('#nivelInteracao').trigger('change');
		$('#btnSaveFrmHisNatInteracaoDetalhe').html($('#btnSaveFrmHisNatInteracaoDetalhe').data('value'));
		$('#btnResetFrmHisNatInteracaoDetalhe').addClass('hide');
		historiaNatural.sqNivelTaxonomicoInteracaoChange();
		app.unselectGridRow();
	},

	//--------------------------------------------------------------------------
	//                ABA REPRODUCAO
	//--------------------------------------------------------------------------
	tabReproducaoInit: function(params) {
		historiaNatural.setModoReproducao();
	},
	saveFrmHisNatReproducao: function(params) {
		if (!$('#frmHisNatReproducao').valid()) {
			return false;
		}
		if (!$('#sqFicha').val()) {
			app.alertError('ID da ficha não encontrado.');
			return false;
		}

		var data = $("#frmHisNatReproducao").serializeArray();
		data = app.params2data(params, data);
		app.ajax(app.url + 'fichaHistoriaNatural/saveFrmHisNatReproducao', data,
			function(res) {
				if (res.status == 0) {
					app.resetChanges('frmHisNatReproducao');
				}
			}, null, 'json');
	},
	setModoReproducao: function() {
		var grupo = $("#sqGrupo option:selected").data('codigo');
		$("#sqModoReproducao option").show();
		if (/AVES/.test(grupo)) {
			$("#sqModoReproducao option[data-codigo!='OVIPARIDADE']").hide()
		} else if (/MAMIFEROS/.test(grupo)) {
			$("#sqModoReproducao option[data-codigo!='VIVIPARIDADE']").hide()
		}

	},



	//--------------------------------------------------------------------------
	//                ABA VARIACAO SAZONAL
	//--------------------------------------------------------------------------
	tabVariacaoSazonalInit: function(data) {
		historiaNatural.updateGridVariacaoSazonal();
	},
	saveFrmHisNatSazonalidade: function(params) {
		if (!$('#frmHisNatSazonalidade').valid()) {
			return false;
		}
		var data = $("#frmHisNatSazonalidade").serializeArray();
		data = app.params2data(params, data);
		if (!data.sqFicha) {
			app.alertError('ID da ficha não encontrado!');
			return;
		}
		app.ajax(app.url + 'fichaHistoriaNatural/saveFrmHisNatSazonalidade', data,
			function(res) {
				if (res.status === 0) {
					app.reset('frmHisNatSazonalidade');
					app.focus('sqFaseVida');
					historiaNatural.updateGridVariacaoSazonal();
				}
			}, null, 'json');
	},
	updateGridVariacaoSazonal: function() {
		ficha.getGrid('variacaoSazonal');
	},
	editVariacaoSazonal: function(params, btn) {
		app.ajax(app.url + 'fichaHistoriaNatural/editVariacaoSazonal', params,
			function(res) {
				$("#btnResetFrmHisNatSazonalidade").removeClass('hide');
				$("#btnSaveFrmHisNatSazonalidade").data('value', $("#btnSaveFrmHisNatSazonalidade").html());
				$("#btnSaveFrmHisNatSazonalidade").html("Gravar Alteração");
				app.setFormFields(res, 'frmHisNatSazonalidade');
				app.selectGridRow(btn);
			});
	},
	deleteVariacaoSazonal: function(params) {
		app.confirm('<br>Confirma exclusão da fase de vida <b>' +
			params.descricao + '</b>?',
			function() {
				app.ajax(app.url + 'fichaHistoriaNatural/deleteVariacaoSazonal',
					params,
					function(res) {
						historiaNatural.updateGridVariacaoSazonal();
					});
			},
			null, params, 'Confirmação', 'warning');
	},
	resetFrmHisNatSazonalidade: function() {
		app.reset('frmHisNatSazonalidade');
		app.unselectGridRow();
		$("#divGridVariacaoSazonal tr.rowSelected").removeClass('rowSelected');
		$("#btnResetFrmHisNatSazonalidade").addClass('hide');
		$("#btnSaveFrmHisNatSazonalidade").html($("#btnSaveFrmHisNatSazonalidade").data('value'));
	},
	copiarObsInteracaoSubespecie: function(params) {
		app.ajax(app.url + 'fichaHistoriaNatural/lerObsInteracaoSubespecie', {
				sqFicha: params.sqFicha
			},
			function(res) {
				if (res) {
					tinyMCE.get('dsInteracao').setContent(res);
					$("#liTabHistoriaNatural,#liTabHisNatInteracao").addClass('changed');
				} else {
					app.alertInfo('Nenhuma subespecies/observação encontrada!')
				}
			}
		)
	},
};
