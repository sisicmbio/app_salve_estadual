<table class="table table-striped table-bordered table-hover">
   <thead>
      <tr>
         <th>Habitat(s)</th>
         <th>Ref. Bibliográfica</th>
         <th>Ação</th>
      </tr>
   </thead>
   <tbody>
      <g:each var="item" in="${listHabitat}">
      <tr>
         <td>${ ( item.habitat.pai && item.habitat.pai.pai ? item.habitat.pai.descricao +' / ':'') + item.habitat.descricao}</td>
         <td>
            <a data-action="ficha.openModalSelRefBibFicha"
               data-no-tabela="ficha_habitat"
               data-no-coluna="sq_ficha_habitat"
               data-sq-registro="${item.id}"
               data-de-rotulo="Habitat - ${(item.habitat.pai && item.habitat.pai.pai ? item.habitat.pai.descricao +' / ':'') + item.habitat.descricao}"
               data-com-pessoal="true"
               data-update-grid="divGridHabitat" class="fld btn btn-default btn-xs btn-grid-subform" title="Adicionar/Remover Refeferência bibliográfica."><span class="fa fa-book"></span></a>
               &nbsp;
               ${raw(item.refBibHtml)}
            </td>
            <td class="td-actions">
               <a data-action="historiaNatural.deleteHabitat" data-descricao="${ ( item.habitat.pai ? item.habitat.pai.descricao +' / ':'') + item.habitat.descricao}" data-id="${item.id}" class="fld btn btn-default btn-xs btn-delete" title="Excluir">
                  <span class="glyphicon glyphicon-remove"></span>
               </a>
            </td>
         </tr>
         </g:each>
      </tbody>
   </table>
