<g:set var="oSND"  value="${[ [key:'S',value:'Sim'], [key:'N', value:'Não'] ,[key:'D', value:'Desconhecido'] ]}"/>
<form id="frmHisNatHabitat" name="frmHisNatHabitat" class="form-inline" role="form">
   <input name="id" id="id" type="hidden" value="" />
   <div class="fld form-group">
      <label for="" class="control-label">Habitat</label>
      <br>
      <select id="sqHabitatPai" name="sqHabitatPai" data-form="frmHisNatHabitat" data-change="ficha.habitatPaiChange" class="fld form-control fld250 fldSelect" data-child="sqHabitat"
         data-s2-placeholder="?"
         data-s2-minimum-input-length="0"
         required="true">
         <option value="">-- Selecione --</option>
         <g:each var="item" in="${listHabitat}">
            <option value="${item.id}" data-codigo="${item.codigo}">${item.descricao}</option>
         </g:each>
      </select>
      <span>-</span>
      <select id="sqHabitat" name="sqHabitat" class="fld form-control fldSelect select2 fld600"
          data-s2-placeholder=" "
          data-s2-minimum-input-length="0"
          disabled="true">
         <option value="">?</option>
      </select>
   </div>
   <div class="fld panel-footer">
      <button data-action="historiaNatural.saveFrmHisNatHabitat" data-params="sqFicha" class="fld btn btn-success">Adicionar Hábitat</button>
   </div>
</form>
<div id="divGridHabitat">
   %{-- <g:render template="/ficha/historiaNatural/divGridHabitat"></g:render> --}%
</div>


%{-- Parte 2 do preenchimento --}%


<form id="frmHisNatHabitatDetalhe" class="form-inline" role="form">
   <div class="form-group">
      <label for="stRestritoHabitatPrimario" class="control-label">Restrito ao Habitat Primário?</label>
      <br>
      <select id="stRestritoHabitatPrimario" name="stRestritoHabitatPrimario" class="fld form-control fld200 fldSelect">
         <option value="">-- selecione --</option>
         <g:each var="item" in="${oSND}">
            <option value="${item.key}" ${ficha.stRestritoHabitatPrimario==item.key ? 'selected' :'' }>${item.value}</option>
         </g:each>
      </select>
   </div>

   <div class="form-group">
      <label for="stEspecialistaMicroHabitat" class="control-label">Especialista Micro Habitat?</label>
      <br>
      <select id="stEspecialistaMicroHabitat" name="stEspecialistaMicroHabitat" data-change="historiaNatural.stEspecialistaMicroHabitatChange" class="fld form-control fld200 fldSelect">
         <option value="">-- Selecione --</option>
         <g:each var="item" in="${oSND}">
            <option value="${item.key}" ${ficha.stEspecialistaMicroHabitat==item.key ? 'selected' :'' }>${item.value}</option>
         </g:each>
      </select>
   </div>
   %{-- <br> --}%
   <div id="divDsEspecialistaMicroHabitat" class="form-group hide w100p">
      <label for="dsDiagnosticoMorfologico" class="control-label">Descrição do Micro Habitat
            <i data-action="ficha.openModalSelRefBibFicha"
              data-no-tabela="ficha"
              data-no-coluna="ds_especialista_micro_habitat"
              data-sq-registro="${ficha.id}"
              data-de-rotulo="Micro Habitat"
              class="fa fa-book ml10" title="Informar Referência Bibliográfica" style="color: green;" />
      </label>
      <br/>
      <input type="text" id="dsEspecialistaMicroHabitat" name="dsEspecialistaMicroHabitat" class="fld form-control fld800" value="${ficha.dsEspecialistaMicroHabitat}"/>
      %{--<textarea id="dsEspecialistaMicroHabitat" name="dsEspecialistaMicroHabitat" class="fld form-control fldTextarea wysiwyg w100p" rows="7">${ficha.dsEspecialistaMicroHabitat}</textarea> --}%
   </div>
   <br>
   <div class="form-group">
      <label for="stVariacaoSazonalHabitat" class="control-label">Variação Sazonal no Uso do Habitat?</label>
      <br>
      <select id="stVariacaoSazonalHabitat" name="stVariacaoSazonalHabitat" class="fld form-control fld400 fldSelect">
         <option value="">-- Selecione --</option>
         <g:each var="item" in="${oSND}">
            <option value="${item.key}" ${ficha.stVariacaoSazonalHabitat==item.key ? 'selected' :'' }>${item.value}</option>
         </g:each>
      </select>
   </div>

   <div class="form-group">
      <label for="stDiferMachoFemeaHabitat" class="control-label">Apresenta Diferença no Uso do Habitat Entre Macho e Fêmea?</label>
      <br>
      <select id="stDiferMachoFemeaHabitat" name="stDiferMachoFemeaHabitat" data-change="historiaNatural.stDiferMachoFemeaHabitatChange" class="fld form-control fld400 fldSelect">
         <option value="">-- Selecione --</option>
         <g:each var="item" in="${oSND}">
            <option value="${item.key}" ${ficha.stDiferMachoFemeaHabitat==item.key ? 'selected' :'' }>${item.value}</option>
         </g:each>
      </select>
   </div>

   <br>

   <div id="divStDiferMachoFemeaHabitat" class="form-group w100p">
       <label for="dsDiferencaMachoFemea" class="control-label">Observação Sobre a Diferença no Uso do Habitat Entre Macho e Fêmea
           <i data-action="ficha.openModalSelRefBibFicha"
              data-no-tabela="ficha"
              data-no-coluna="ds_diferenca_macho_femea"
              data-sq-registro="${ficha.id}"
              data-de-rotulo="Habitat - Diferença no Uso do Habitat Entre Macho e Fêmea"
              class="fa fa-book ml10" title="Informar Referência Bibliográfica" style="color: green;" />
       </label>
      <br>
      <textarea id="dsDiferencaMachoFemea" name="dsDiferencaMachoFemea" class="fld form-control fldTextarea wysiwyg w100p" rows="7">${ficha.dsDiferencaMachoFemea}</textarea>
   </div>

   <br>

   <div class="form-group w100p">
      <label for="dsDiagnosticoMorfologico" class="control-label">Observação Sobre o Habitat
            <i data-action="ficha.openModalSelRefBibFicha"
              data-no-tabela="ficha"
              data-no-coluna="ds_uso_habitat"
              data-sq-registro="${ficha.id}"
              data-de-rotulo="Habitat"
              class="fa fa-book ml10" title="Informar Referência Bibliográfica" style="color: green;" />
      </label>
      <br>
      <textarea id="dsUsoHabitat" name="dsUsoHabitat" id="dsDiagnosticoMorfologico" class="fld form-control fldTextarea wysiwyg w100p" rows="7">${ficha.dsUsoHabitat}</textarea>
   </div>

   <div class="fld panel-footer">
      <button data-action="historiaNatural.saveFrmHisNatHabitatDetalhe" data-params="sqFicha" class="fld btn btn-success">Gravar</button>
   </div>
</form>
