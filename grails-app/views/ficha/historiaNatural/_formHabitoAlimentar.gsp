<g:set var="oSND"  value="${[ [key:'S',value:'Sim'], [key:'N', value:'Não'] ,[key:'D', value:'Desconhecido'] ]}"/>
<div class="row">
   <div class="col-sm-6">
      <form id="frmHisNatPosicaoTrofica" class="form-inline" role="form">
         <%-- início campo posicao trófica --%>
%{-- carlos sugeriu ocultar este campo
         <div class="form-group">
            <label for="" class="control-label">Posição Trófica</label>
            <br>
            <select name="sqPosicaoTrofica" class="fld form-control fld150 fldSelect" required="true">
               <option value="">-- selecione --</option>
               <g:each var="item" in="${listPosicaoTrofica}">
                  <option value="${item.id}" ${ ficha?.posicaoTrofica?.id==item.id ? 'selected' :'' }>${item.descricao}</option>
               </g:each>
            </select>
         </div>
 --}%         <%-- fim campo posicão trófica --%>

         <%-- início campo Especialista S/N/D --%>
         <div class="form-group">
            <label for="" class="control-label">Especialista?</label>
			<br>
            <select id="stHabitoAlimentEspecialista" name="stHabitoAlimentEspecialista" data-change="historiaNatural.stHabitoAlimentEspecialistaChange" class="fld form-control fld200 fldSelect">
               <option value="">-- selecione --</option>
               <g:each var="item" in="${oSND}">
                  <option value="${item.key}" ${ficha?.stHabitoAlimentEspecialista==item.key ? 'selected' :'' }>${item.value}</option>
               </g:each>
            </select>
         </div>

         <%-- fim campo Especialista --%>
         <div class="panel-footer">
            <button data-action="historiaNatural.savefrmHistNatPosicaoTrofica" data-params="sqFicha" class="fld btn btn-success">Gravar Especialista</button>
         </div>
      </form>

      <form id="frmHisNatHabitoAlimentar" class="form-inline" role="form">
         <input type="hidden" id="sqFichaHabitoAlimentar" name="sqFichaHabitoAlimentar" value="">

         %{-- <div class="page-header group-header">
            <h3>Hábitos Alimentares<small></small></h3>
         </div> --}%
         <%-- início campo Tipo Habito Alimentar --%>
         <div class="fld form-group">
            <label for="" class="control-label">Tipo de Hábito Alimentar</label>
            <br>
            <select name="sqTipoHabitoAlimentar" class="fld form-control fld200 fldSelect">
               <option value="">-- selecione --</option>
               <g:each var="item" in="${listHabitoAlimentar}">
                  <option value="${item.id}" data-codigo="${item.codigo}">${item.descricao}</option>
               </g:each>
            </select>
         </div>
         <%-- fim campo Tipo Habito Alimentar --%>
         <br>
         <%-- inicio campo Observacao --%>
         <div class="form-group hide" style="width: 100%;">
                <label for="dsFichaHabitoAlimentar" class="control-label">Observação</label>
            <br>
            <textarea name="dsFichaHabitoAlimentar" class="fld form-control fldTextarea wysiwyg" style="width: 100%;" rows="4"></textarea>
         </div>
         <%-- fim campo Observacao --%>
         <div class="panel-footer">
            <button id="btnSaveFrmHisNatHabitoAlimentar" data-action="historiaNatural.saveFrmHisNatHabitoAlimentar" data-params="sqFicha" class="fld btn btn-success">Adicionar Tipo de Hábito Alimentar</button>
            <button id="btnResetFrmHisNatHabitoAlimentar" data-action="historiaNatural.resetFrmHisNatHabitoAlimentar" class="fld btn btn-info hide">Limpar Formulário</button>
         </div>
      </form>
      <div id="divGridHabitoAlimentar">
         %{-- <g:render template="/ficha/historiaNatural/divGridHabitoAlimentar"></g:render> --}%
      </div>
   </div>
   <%--   fim col-sm-6 - form habito alimentar   --%>

    <%--  HABITO ALIMENTER ESPECIALISTA --%>

   <div id="divHabitoAlimentarEsp" class="col-sm-6 hide">
      <div class="page-header group-header">
         <h3>Táxon Consumido<small></small></h3>
      </div>
      <form id="frmHisNatHabitoAlimentarEsp" class="form-inline" role="form">
         <input name="sqFichaHabitoAlimentarEsp" id="sqFichaHabitoAlimentarEsp" type="hidden" value="" />
         <div class="fld form-group">
            <label for="" class="control-label">Nivel Taxonômico</label>
            <br>
			   <select id="nivel" name="nivel" data-change="historiaNatural.sqNivelTaxonomicoChange" class="fld form-control fld200 fldSelect">
               <option value="">-- selecione --</option>
               <g:each var="item" in="${listNivelTaxonomico}">
                  <option value="${item.coNivelTaxonomico}">${item.deNivelTaxonomico}</option>
               </g:each>
            </select>
         </div>
         <div class="fld form-group">
            <label for="" class="control-label">Taxon</label>
			   <br/>
			<select id="sqTaxonEspecialista" name="sqTaxon" data-change="historiaNatural.sqTaxonEspecialistaChange" data-s2-visible="false" class="fld form-control select2 fld250"
				data-s2-params="nivel"
				data-s2-url="ficha/select2Taxon"
				data-s2-placeholder="?"
				data-s2-maximum-selection-length="2">
            </select>
         </div>
		   <br class="fld"/>
         <div class="fld form-group hide">
            <label for="" class="control-label">Categoria</label>
            <br>
            <select name="sqCategoriaIucn" id="sqCategoriaIucn" class="fld form-control fld300 fldSelect">
               <option value="">-- selecione --</option>
			      <g:each var="item" in="${listCategoriaIucn}">
                  <option value="${item.id}" data-codigo="${item.codigo}">${item.descricaoCompleta}</option>
               </g:each>
            </select>
             <span class="hidden" id="sqCategoriaIucnLoading"></span>
         </div>

         <div class="form-group" style="width: 100%">
            <label for="" class="control-label">Observações</label>
            <br>
            <textarea name="dsFichaHabitoAlimentarEsp" class="fld form-control fldTextarea wysiwyg" rows="7" style="width: 100%"></textarea>
         </div>
         <br>
         <div class="fld panel-footer">
            <button id="btnSaveFrmHisNatHabitoAlimentarEsp" data-action="historiaNatural.saveFrmHisNatHabitoAlimentarEsp" data-params="sqFicha" class="fld btn btn-success">Adicionar Espécie Consumida</button>
			   <button id="btnResetFrmHisNatHabitoAlimentarEsp" data-action="historiaNatural.resetFrmHisNatHabitoAlimentarEsp" class="fld btn btn-info hide">Limpar Formulário</button>
         </div>
      </form>
      <div id="divGridHabitoAlimentarEsp" class="mt10"></div>
   </div>
   <%--   fim col-sm-6 - form habito especialista   --%>

    <div id="divObsHabitoAlimentar" class="col-sm-12">
        <form id="frmObsHabitoAlimentar" class="form-inline" role="form">
            <div class="form-group" style="width: 100%">
                <label for="dsHabitoAlimentar" class="control-label">
                  Observações sobre o hábito alimentar
                  <i data-action="ficha.openModalSelRefBibFicha"
                  data-no-tabela="ficha"
                  data-no-coluna="ds_habito_alimentar"
                  data-sq-registro="${ficha.id}"
                  data-de-rotulo="Hábito Alimentar"
                  class="fa fa-book ml10" title="Informar Referência Bibliográfica" style="color: green;" />
                </label>
                <br>
                <textarea id="dsHabitoAlimentar" name="dsHabitoAlimentar" class="fld form-control fldTextarea wysiwyg" rows="7" style="width: 100%">${ficha.dsHabitoAlimentar}</textarea>
            </div>
            <div class="fld panel-footer">
                <button id="btnSaveFrmObsHabitoAlimentar" data-action="historiaNatural.saveFrmObsHabitoAlimentar" data-params="sqFicha" class="fld btn btn-success">Gravar Observações</button>
            </div>
        </form
    </div>
</div>
<%-- fim row--%>
