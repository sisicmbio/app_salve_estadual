<table class="table table-striped table-bordered table-hover">
   <thead>
      <tr>
         <th>Nivel</th>
         <th>Taxon</th>
         <th>Tipo</th>
         %{-- <th>Classificação</th> --}%
         <th>Categoria</th>
         <th>Ref. Bibliográfica</th>
          <g:if test="${temSubespecie}">
            <th>Subespecie</th>
          </g:if>
         <th>Ação</th>
      </tr>
   </thead>
   <tbody>
   <g:each var="item" in="${listInteracao}">
      <tr class="${ item.ficha != ficha ? 'brown':''}">
         <td>${item.taxon.nivelTaxonomico.deNivelTaxonomico}</td>
         <td>${item.taxon.noCientifico}</td>
         <td>${item.tipoInteracao.descricao}</td>
         %{-- <td>${item.classificacao.descricao}</td> --}%
         <td>${ (item.categoriaIucn ? item.categoriaIucn.descricaoCompleta : '') }</td>
         <td>
            <g:if test="${item.ficha == ficha}">
            <a data-action="ficha.openModalSelRefBibFicha"
               data-no-tabela="ficha_interacao"
               data-no-coluna="sq_ficha_interacao"
               data-sq-registro="${item.id}"
               data-de-rotulo="Interação - ${item.taxon.noCientifico}"
               data-com-pessoal="true"
               data-update-grid="divGridInteracao"
               class="fld btn btn-default btn-xs btn-grid-subform" title="Adicionar/Remover Refeferência bibliográfica."><span class="fa fa-book"></span></a>
               &nbsp;
             </g:if>
              ${raw(item.refBibHtml)}
         </td>
         %{--<td>${item.dsFichaInteracao}</td>--}%

       <g:if test="${temSubespecie}">
         <td>
           <g:if test="${item.ficha != ficha}">
                ${ item.ficha.noCientifico }
           </g:if>
         </td>
       </g:if>

         <td class="td-actions nowrap">
          <g:if test="${item.ficha == ficha}">
             <a data-action="historiaNatural.editInteracao"  data-id="${item.id}" class="fld btn btn-default btn-xs btn-update" title="Alterar">
                <span class="glyphicon glyphicon-pencil"></span>
             </a>
             <a data-action="historiaNatural.deleteInteracao" data-descricao="${item.taxon.noCientifico}" data-id="${item.id}" class="fld btn btn-default btn-xs btn-delete" title="Excluir">
                <span class="glyphicon glyphicon-remove"></span>
             </a>
          </g:if>
         </td>
      </tr>
   </g:each>
   </tbody>
</table>
