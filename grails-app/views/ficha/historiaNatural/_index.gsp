<%-- inicio frmHistoriaNatural --%>
<g:set var="oSND"  value="${[ [key:'S',value:'Sim'], [key:'N', value:'Não'] ,[key:'D', value:'Desconhecido'] ]}"/>

<form id="frmHistoriaNatural" class="form-inline" role="form">
   <%--inicio form-group descrição --%>
    <div class="form-group">
        <label for="" class="control-label">A espécie é migratória?</label>
        <br>
        <select name="stMigratoria" id="stMigratoria" data-change="historiaNatural.stMigratoriaChange" class="fld form-control fld200 fldSelect">
            <option value="">-- selecione --</option>
            <g:each var="item" in="${oSND}">
                <option value="${item.key}" ${ficha?.stMigratoria==item.key ? ' selected' :'' }>${item.value}</option>
            </g:each>
        </select>
    </div>
    <div class="form-group ${ficha.stMigratoria!='S' ? 'hidden' : ''}">
        <label for="" class="control-label">Qual o Padrão de Deslocamento?</label>
        <br>
        <select name="sqPadraoDeslocamento" id="sqPadroDeslocamento"class="fld form-control fld200 fldSelect">
            <option value="">-- selecione --</option>
            <g:each var="item" in="${listPadraoDeslocamento}">
                <option value="${item.id}" ${ficha?.padraoDeslocamento?.id==item.id ? ' selected' :'' }>${item.descricao}</option>
            </g:each>
        </select>
    </div>
    <br/>
   <div class="form-group w100p">
          <label for="dsHistoriaNatural" class="control-label">História Natural
            <i data-action="ficha.openModalSelRefBibFicha"
              data-no-tabela="ficha"
              data-no-coluna="ds_historia_natural"
              data-sq-registro="${ficha.id}"
              data-de-rotulo="História Natural"
              class="fa fa-book ml10" title="Informar Referência Bibliográfica" style="color: green;" />
         </label>
         <g:if test="${ ! session.sicae.user.isCO() }">
             <div class="checkbox ml20">
                <label title="Clique aqui para preencher o campo com o texto">
                   <input type="checkbox" data-action="app.setDefaultText" data-target="dsHistoriaNatural"
                          ${ ficha?.dsHistoriaNatural?.toLowerCase()?.indexOf('n&atilde;o foram encontradas informa&ccedil;&otilde;es para o táxon.') > -1 ? 'checked' : '' }
                          class="fld checkbox-lg">&nbsp;&nbsp;Preencher com: "N&atilde;o foram encontradas informa&ccedil;&otilde;es para o táxon."
               </label>
             </div>
         </g:if>
         <br>
         <textarea name="dsHistoriaNatural" id="dsHistoriaNatural" rows="7" class="fld fldTextarea wysiwyg w100p" style="">${ficha.dsHistoriaNatural}</textarea>
   </div>
   <%-- fim form-group descrição --%>

   <div class="panel-footer mb10">
      <button data-action="historiaNatural.savefrmHistoriaNatural" data-params="sqFicha" class="fld btn btn-success">Gravar História Natural</button>
   </div>

</form>
<%-- fim form frmHistoriaNatural --%>

<%--incio abas historia natural --%>
<div class="row">
   <div class="col-sm-12">
      <ul class="nav nav-tabs nav-sub-tabs" id="ulTabsHistoriaNatural">
         <li id="liTabHisNatHabitoAlimentar"><a data-action="ficha.tabClick" data-url="fichaHistoriaNatural/tabHisNatHabitoAlimentar" data-on-load="historiaNatural.tabHabitoAlimentarInit" data-container="tabHisNatHabitoAlimentar" data-reload="false" data-params="sqFicha" data-toggle="tab" href="#tabHisNatHabitoAlimentar">3.1-Hábito Alimentar</a></li>
         <li id="liTabHisNatHabitat">        <a data-action="ficha.tabClick" data-url="fichaHistoriaNatural/tabHisNatHabitat" data-on-load="historiaNatural.tabHabitatInit" data-container="tabHisNatHabitat" data-reload="false" data-params="sqFicha" data-toggle="tab" href="#tabHisNatHabitat">3.2-Habitat</a></li>
         <li id="liTabHisNatInteracao">      <a data-action="ficha.tabClick" data-url="fichaHistoriaNatural/tabHisNatInteracao" data-on-load="historiaNatural.tabInteracaoInit" data-container="tabHisNatInteracao" data-reload="false" data-params="sqFicha" data-toggle="tab" href="#tabHisNatInteracao">3.3-Interação</a></li>
         <li id="liTabHisNatReproducao">     <a data-action="ficha.tabClick" data-url="fichaHistoriaNatural/tabHisNatReproducao" data-container="tabHisNatReproducao" data-on-load="historiaNatural.tabReproducaoInit" data-reload="false" data-params="sqFicha" data-toggle="tab" href="#tabHisNatReproducao">3.4-Reprodução</a></li>
         <li id="liTabHisNatSazonalidade">   <a data-action="ficha.tabClick" data-url="fichaHistoriaNatural/tabHisNatSazonalidade" data-on-load="historiaNatural.tabVariacaoSazonalInit" data-container="tabHisNatSazonalidade" data-reload="false" data-params="sqFicha" data-toggle="tab" href="#tabHisNatSazonalidade">3.5-Sazonalidade e Variação no Uso do Habitat</a></li>
      </ul>
      <div id="disTabContent" class="tab-content">
          <div class="tab-content-tips alert alert-success mt10"><b>Dica!</b>&nbsp;Clique nas abas acima para carregar o seu conteúdo!</div>

          %{-- inicio aba Habito Alimentar --}%
         <div role="tabpanel" class="tab-pane tab-pane-ficha active" id="tabHisNatHabitoAlimentar">
            %{-- <g:render template="historiaNatural/formHabitoAlimentar"></g:render> --}%
         </div>
         %{-- fim aba Habito Alimentar --}% %{-- inicio aba Habitat --}%

         <div role="tabpanel" class="tab-pane tab-pane-ficha" id="tabHisNatHabitat">
            %{-- <g:render template="historiaNatural/formHabitat"></g:render> --}%
         </div>
         %{-- fim aba Habitat --}% %{-- inicio aba Interacao --}%

         <div role="tabpanel" class="tab-pane tab-pane-ficha" id="tabHisNatInteracao">
            %{-- <g:render template="historiaNatural/formInteracao"></g:render> --}%
         </div>
         %{-- fim aba Reproducao --}% %{-- inicio aba Reproducao --}%

         <div role="tabpanel" class="tab-pane tab-pane-ficha" id="tabHisNatReproducao">
            %{-- <g:render template="historiaNatural/formReproducao"></g:render> --}%
         </div>
         %{-- fim aba Reproducao --}% %{-- inicio aba Sazonalidade --}%

         <div role="tabpanel" class="tab-pane tab-pane-ficha" id="tabHisNatSazonalidade">
            %{-- <g:render template="historiaNatural/formSazonalidade"></g:render> --}%
         </div>
         %{-- fim aba Sazonalidade --}%


      </div>
      <%-- fim tab-content--%>
   </div>
   <%--fim col-sm-12--%>
</div>
<%-- fim row abas historia natural --%>
