<div id="divGridPendencia">
<g:if test="${listPendencias}">
<table class="table table-hover table-striped table-condensed table-bordered">
	<thead>
		<th>Data</th>
		<th>Assunto</th>
		<th>Responsável</th>
		<th>Pendente?</th>
		<th>Pendência</th>
		<th>Ação</th>
	</thead>
	<tbody>
		<g:each var="item" in="${listPendencias}">
			<tr style="color:${ item.stPendente == 'S' ? (  item.deAssunto != 'Campos obrigatórios'?'red':'blue' ) : 'green' }" id="tr-pendencia-${item.id}">
				<td style="width:100px;" class="text-center">${item.dtPendencia.format('dd/MM/yyyy')}</td>
				<td style="width:200px">${item.deAssunto}</td>
				<td style="width:200px">
                    <g:if test="${item.contexto && item.contexto.codigoSistema=='SISTEMA'}">
                       Sistema SALVE
                    </g:if>
                    <g:else>
                        ${item.usuario.noPessoa}
                    </g:else>
                </td>
				<td style="width:100px" class="text-center">${item.stPendente == 'S' ? 'Sim' : 'Não'}
                    <div class="pendencia-log" id="spanLogRevisaoAbaPendencia${item.id}">${raw(item.logRevisao())}</div>
                </td>
				<td style="width: auto;">${raw(item.txPendencia)}</td>
				<td style="text-align:center;width:100px;">
                    <g:if test="${ item.canEdit(session?.sicae) }">
                            <g:if test="${item.contexto && item.contexto.codigoSistema=='REVISAO' && !session.sicae.user.isADM() }">
                                <g:if test="${!item.dtRevisao}">
                                    <input type="checkbox" class="checkbox-lg"
                                       title="Marcar/desmarcar como resolvida"
                                    ${ item.stPendente == 'S' ? '' :'checked'}
                                       value="S"
                                       data-params="sqFicha:${params?.sqFicha},sqFichaPendencia:${item.id}"
                                       data-action="ficha.situacaoPendenciaChange" />
                                </g:if>
                            </g:if>
                        <g:else>
                            <a data-action="showFormPendencia" data-id="${item.id}" data-on-close="${params.onClose?:''}" class="fld btn btn-default btn-xs btn-update" title="Alterar">
                                <span class="glyphicon glyphicon-pencil"></span>
                            </a>
                            <a data-action="deletePendencia" data-id="${item.id}" data-callback="${params.onClose?:''}" class="fld btn btn-default btn-xs btn-delete" title="Excluir">
                                <span class="glyphicon glyphicon-remove"></span>
                            </a>
                        </g:else>

                    </g:if>
					%{--<g:if test="${ (item.deAssunto !='Campos obrigatórios') && (item.usuario.id == session?.sicae?.user?.sqPessoa || session?.sicae?.user?.isADM() ) }">--}%
					%{--<g:if test="${session.sicae.user.isVL() }">
							<g:if test="${item.usuario.id.toLong() == session.sicae.user.sqPessoa.toLong() }">
								<g:if test="${ (item.deAssunto !='Campos obrigatórios')  }">
									<a data-action="showFormPendencia" data-id="${item.id}" data-on-close="${params.onClose?:''}" class="fld btn btn-default btn-xs btn-update" title="Alterar">
										<span class="glyphicon glyphicon-pencil"></span>
									</a>
									<a data-action="deletePendencia" data-id="${item.id}" data-callback="${params.onClose?:''}" class="fld btn btn-default btn-xs btn-delete" title="Excluir">
										<span class="glyphicon glyphicon-remove"></span>
									</a>
								</g:if>
							</g:if>
					</g:if>
					<g:else>
						<g:if test="${ (item.deAssunto !='Campos obrigatórios')  }">
							<a data-action="showFormPendencia" data-id="${item.id}" data-on-close="${params.onClose?:''}" class="fld btn btn-default btn-xs btn-update" title="Alterar">
											<span class="glyphicon glyphicon-pencil"></span>
							</a>
							<a data-action="deletePendencia" data-id="${item.id}" data-callback="${params.onClose?:''}" class="fld btn btn-default btn-xs btn-delete" title="Excluir">
											<span class="glyphicon glyphicon-remove"></span>
							</a>
						</g:if>
					</g:else>
                    --}%
				</td>
			</tr>
		</g:each>
	</tbody>
</table>
</g:if>
<g:else>
	<p>
		<g:if test="${params.canModify}">
			%{--<h4>Não há pendência cadastrada, utilize o botão <i class="glyphicon glyphicon-tags red"!></i>&nbsp;&nbsp;no canto superior direito para cadastrar!</h4> --}%
			<h4>Não há pendência cadastrada. <button class="btn btn-xs btn-primary" onClick="$('#btnCadPendencia').click()">Cadastrar</button>
		</g:if>
		<g:else>
			<h4>Não há pendência cadastrada</h4>
		</g:else>
	</p>
</g:else>
</div>
