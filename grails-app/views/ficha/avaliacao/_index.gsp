<!-- view: /views/ficha/avaliacao/_index.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<!-- Obs: as funcoes JS da aba avaliacao estao no arquivo fichaCompleta.js porque esta aba é tambem alimentada na ficha completa -->


<g:set var="oSND"  value="${[ [key:'S',value:'Sim'], [key:'N', value:'Não'] ,[key:'D', value:'Desconhecido'] ]}"/>
%{--<g:set var="varDisabled" value="${ficha?.situacaoFicha?.ordem?.toInteger() > 1 ? '':'disabled' }"/>--}%
<g:set var="varDisabled" value="-"/>
<g:set var="varTitleSituacao" value="${ varDisabled=='disabled' ? 'title="Ficha em ' + ficha.situacaoFicha.descricao+'"' : '' }"/>
<ul class="nav nav-tabs nav-sub-tabs mt10" id="ulTabsAvaliacao">
    %{-- ABAS 11 FORAM DESATIVADAS --}%
%{--
    <li id="liTabAvaDistribuicao">         <a data-action="ficha.tabClick" data-url="fichaAvaliacao/tabAvaDistribuicao" data-container="tabAvaDistribuicao" data-reload="false" data-params="sqFicha" data-toggle="tab" data-contexto="ficha" href="#tabAvaDistribuicao">11.1-Distribuição e Taxonomia</a></li>
    <li id="liTabAvaPopulacao">            <a data-action="ficha.tabClick" data-url="fichaAvaliacao/tabAvaPopulacao" data-container="tabAvaPopulacao" data-reload="false" data-params="sqFicha" data-on-load="avaliacao.tabAvaPopulacaoInit" data-toggle="tab" data-contexto="ficha" href="#tabAvaPopulacao">11.2-População</a></li>

    <li id="liTabAvaAnaliseQuantitativa">  <a data-action="ficha.tabClick" data-url="fichaAvaliacao/tabAvaAnaliseQuantitativa" data-container="tabAvaAnaliseQuantitativa" data-reload="false" data-params="sqFicha" data-toggle="tab" data-contexto="ficha" href="#tabAvaAnaliseQuantitativa">11.3-Ameaças e Análise Quantitativa</a></li>
    <li id="liTabAvaAjusteRegional">       <a data-action="ficha.tabClick" data-url="fichaAvaliacao/tabAvaAjusteRegional" data-container="tabAvaAjusteRegional" data-reload="false" data-params="sqFicha,preview:S" data-toggle="tab" data-contexto="ficha" href="#tabAvaAjusteRegional">11.4-Ajuste Regional</a></li>
--}%

    %{-- Exibir esta abas somente quando a ficha não estiver em compilacao --}%
    <g:if test="${ ficha.cicloAvaliacao.nuAno > 2010}">
        %{--11.5-Avaliação Expressa--}%
%{--        <li id="liTabAvaExpedita"       class="${varDisabled} ${varTitleSituacao}" style="display:none"> <a data-action="ficha.tabClick" data-url="fichaAvaliacao/tabAvaExpedita" data-on-load="avaliacao.tabAvaExpeditaInit" data-container="tabAvaExpedita" data-reload="false" data-params="sqFicha" data-toggle="tab" href="#tabAvaExpedita">11.5-Avaliação Expressa</a></li>--}%
        %{-- 11.6-Resultado Avaliação --}%
        <li id="liTabAvaResultado"  class="${varDisabled} ${varTitleSituacao}"> <a data-action="ficha.tabClick" data-url="fichaAvaliacao/tabAvaResultado" data-on-load="avaliacao.tabAvaResultadoInit" data-contexto="ficha" data-container="tabAvaResultado" data-reload="true" data-params="sqFicha" data-can-modify="${ canModify ? 'true' : 'false'}" data-toggle="tab" href="#tabAvaResultado">11.1-Resultado avaliação</a></li>
    </g:if>
    <li id="liTabAvaResultadoFinal" class="${varDisabled} ${varTitleSituacao}"> <a data-action="ficha.tabClick" data-url="fichaAvaliacao/tabAvaResultadoFinal" data-on-load="avaliacao.tabValidacaoFinalInit" data-contexto="ficha" data-container="tabAvaResultadoFinal" data-reload="true" data-params="sqFicha" data-toggle="tab" href="#tabAvaResultadoFinal">11.2-Resultado validação</a></li>
</ul>

<div id="avaTabContent" class="tab-content">
    <%-- início aba avaliação resultado --%>
    <div role="tabpanel" class="tab-pane tab-pane-ficha subTabs10" id="tabAvaResultado"></div>

    <%-- início aba avaliação resultado final --%>
    <div role="tabpanel" class="tab-pane tab-pane-ficha subTabs10" id="tabAvaResultadoFinal"></div>


    %{-- inicio aba avaliação - Distribução --}%
%{--    <div role="tabpanel" class="tab-pane tab-pane-ficha active subTabs10" id="tabAvaDistribuicao">--}%
        %{-- <g:render template="avaliacao/formDistribuicao"></g:render> --}%
%{--    </div>--}%

    %{-- início aba avaliação População --}%
%{--    <div role="tabpanel" class="tab-pane tab-pane-ficha subTabs10" id="tabAvaPopulacao">--}%
        %{-- <g:render template="avaliacao/formPopulacao"></g:render> --}%
%{--    </div>--}%

    <%-- início aba avaliação Ameças--%>
%{--    <div role="tabpanel" class="tab-pane tab-pane-ficha subTabs10" id="tabAvaAmeaca">--}%
        %{-- <g:render template="avaliacao/formAmeaca"></g:render> --}%
%{--    </div>--}%

    <%-- início aba avaliação analise quantitativa --%>
%{--    <div role="tabpanel" class="tab-pane tab-pane-ficha subTabs10" id="tabAvaAnaliseQuantitativa">--}%
        %{-- <g:render template="avaliacao/formAnaliseQuantitativa"></g:render> --}%
%{--    </div>--}%

    <%-- início aba avaliação situação regional --%>
%{--    <div role="tabpanel" class="tab-pane tab-pane-ficha subTabs10" id="tabAvaAjusteRegional">--}%
        %{-- <g:render template="avaliacao/formAjusteRegional"></g:render> --}%
%{--    </div>--}%

    <%-- início aba avaliação expedita --%>
%{--    <div role="tabpanel" class="tab-pane tab-pane-ficha subTabs10" id="tabAvaExpedita">--}%
        %{-- <g:render template="avaliacao/formExpedita"></g:render> --}%
%{--    </div>--}%

</div>
