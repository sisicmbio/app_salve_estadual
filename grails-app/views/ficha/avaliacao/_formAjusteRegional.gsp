<g:set var="oSND"  value="${[ [key:'S',value:'Sim'], [key:'N', value:'Não'] ,[key:'D', value:'Desconhecido'] ]}"/>
%{--<g:set var="canModify"  value="${(params?.canModify||params.canModify==null) && params?.contexto != 'validador'}"/>--}%
<g:set var="canModify"  value="${canModify||canModify==null}"/>
<form id="frmAjusteRegional" role="form" class="form-inline">
   <g:if test="${canModify && ! session.sicae.user.isCO()}">
      <div class="checkbox">
         <label title="Clique aqui para preencher o campo com o texto padrão">
            <input type="checkbox" data-action="checkRespostaPadraoAjusteRegionalClick" data-target="dsConectividadePopExterior"
               ${  ficha.dsConectividadePopExterior?.toLowerCase()?.indexOf('xxx') > -1 ? 'checked' : '' }
                   class="fld checkbox-lg">&nbsp;&nbsp;"Texto padrão..."
         </label>
      </div>
   <br>
   </g:if>
   <div class="form-group">
      <label for="" class="control-label">Conectividade com populações de outros países</label>
      <br>
      <select name="sqTipoConectividade" class="fld form-control fld450 fldSelect" ${canModify?'':'disabled'}>
         <option value="">-- selecione --</option>
         <g:each var="item" in="${listConectividade}">
            <option data-codigo="${item.codigoSistema}" value="${item.id}" ${(ficha?.tipoConectividade?.id==item.id)?' selected':''}>${item.descricao}</option>
         </g:each>
      </select>
   </div>

   <div class="form-group">
      <label for="" class="control-label">Tendência de imigração no Brasil</label>
      <br>
      <select name="sqTendenciaImigracao" class="fld form-control fld300 fldSelect" ${canModify?'':'disabled'}>
         <option value="">-- selecione --</option>
         <g:each var="item" in="${listTendencia}">
            <option data-codigo="${item.codigoSistema}" value="${item.id}" ${ficha?.tendenciaImigracao?.id==item.id ? 'selected':''} >${item.descricao}</option>
         </g:each>
      </select>
   </div>

   <div class="form-group">
      <label for="" class="control-label">População brasileira pode declinar</label>
      <br>
      <select name="stDeclinioBrPopulExterior" class="fld form-control fld300 fldSelect" ${canModify?'':'disabled'}>
         <option value="">-- selecione --</option>
         <g:each var="item" in="${oSND}">
            <option data-codigo="${item.key}" value="${item.key}" ${ficha?.stDeclinioBrPopulExterior==item.key ? ' selected' :'' }>${item.value}</option>
         </g:each>
      </select>
   </div>

   <div class="form-group w100p">
      <label for="" class="control-label">Observações</label>
      <br>
      <g:if test="${canModify}">
           <textarea name="dsConectividadePopExterior" id="dsConectividadePopExterior" rows="7" class="fld form-control fldTextarea wysiwyg w100p">${ficha.dsConectividadePopExterior}</textarea>
      </g:if>
       <g:else>
           <div class="div-justificativa">${raw( ficha.dsConectividadePopExterior )}</div>
       </g:else>
   </div>
   %{-- botão gravar --}%
    <g:if test="${canModify}">
       <div class="fld panel-footer">
          <button data-action="avaliacao.saveFrmAjusteRegional" data-params="sqFicha" class="fld btn btn-success">Gravar Ajuste Regional</button>
       </div>
    </g:if>
</form>
