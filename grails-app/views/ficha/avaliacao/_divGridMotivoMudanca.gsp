<table class="table table-hover table-striped table-condensed table-bordered">
	<thead>
		<th>Motivo da Mudança</th>
		%{-- <th>Observação</th> --}%
		<th>Ação</th>
	</thead>
	<tbody>
	<g:each var="item" in="${listaMotivoMudanca}">
		<tr>
			<td>${item.motivoMudanca.descricao}</td>
			%{-- <td>${item.txFichaMudancaCategoria}</td> --}%
			<td style="text-align:center;width:100px;">
				<a data-action="avaliacao.editMotivoMudanca" data-id="${item.id}" class="fld btn btn-default btn-xs btn-update" title="Alterar">
   					<span class="glyphicon glyphicon-pencil"></span>
				</a>
				<a data-action="avaliacao.deleteMotivoMudanca" data-descricao="${item.motivoMudanca.descricao}" data-id="${item.id}" class="fld btn btn-default btn-xs btn-delete" title="Excluir">
					<span class="glyphicon glyphicon-remove"></span>
				</a>
			</td>
		</tr>
	</g:each>
	</tbody>
</table>
