<g:set var="oSND"  value="${[ [key:'S',value:'Sim'], [key:'N', value:'Não'] ,[key:'D', value:'Desconhecido'] ]}"/>
<form id="frmAnaliseQuantitativa" role="form" class="form-inline">
   %{-- Ameaças --}%
   <div class="form-group">
      <label for="" class="control-label">Nº de Localizações</label>
      <br>
      <input name="nuLocalizacoes" type="text" class="fld form-control number fld130" maxlength="5" ${params.canModify?'':'disabled'} value="${ficha.nuLocalizacoes}" />
   </div>
   <div class="form-group">
      <label for="" class="control-label">Tendência do Número de Localizações</label>
      <br>
      <select name="sqTendenNumLocalizacoes" class="fld form-control fld300 fldSelect" ${params.canModify?'':'disabled'}>
         <option value="">-- selecione --</option>
         <g:each var="item" in="${listTendencia}">
            <option value="${item.id}" ${ficha?.tendenNumLocalizacoes?.id==item.id ? 'selected':''} >${item.descricao}</option>
         </g:each>
      </select>
   </div>

   <div class="form-group">
      <label for="" class="control-label">Flutuação Extrema do Nº de Localizações</label>
      <br>
      <select name="stFlutuacaoNumLocalizacoes" class="fld form-control fld300 fldSelect" ${params.canModify?'':'disabled'}>
         <option value="">-- selecione --</option>
         <g:each var="item" in="${oSND}">
            <option value="${item.key}" ${ficha?.stFlutuacaoNumLocalizacoes==item.key ? ' selected' :'' }>${item.value}</option>
         </g:each>
      </select>
   </div>

   <br class="fld">

   <div class="form-group">
      <label for="" class="control-label"><br><br>Tendência da Qualidade do Habitat</label>
      <br>
      <select name="sqTendenQualidadeHabitat" class="fld form-control fld300 fldSelect" ${params.canModify?'':'disabled'}>
         <option value="">-- selecione --</option>
         <g:each var="item" in="${listTendencia}">
            <option value="${item.id}" ${ficha?.tendenQualidadeHabitat?.id==item.id ? 'selected':''} >${item.descricao}</option>
         </g:each>
      </select>
   </div>

   <div class="form-group">
      <label for="" class="control-label">AOO ou número de localizações restritas sob ameaça<br>futura plausível de levar a espécie à condição de CR<br>ou EX em curto prazo
      </label>
      <br>
      <select name="stAmeacaFutura" class="fld form-control fld300 fldSelect" ${params.canModify?'':'disabled'}>
         <option value="">-- selecione --</option>
         <g:each var="item" in="${oSND}">
            <option value="${item.key}" ${ficha?.stAmeacaFutura==item.key ? ' selected' :'' }>${item.value}</option>
         </g:each>
      </select>
   </div>

   <br />

   %{-- Analise quantitativa --}%
   <div class="form-group">
      <label for="" class="control-label">Probabilidade de Extinção no Brasil</label>
      <br>
      <select name="sqProbExtincaoBrasil" class="fld form-control fld300 fldSelect" ${params.canModify?'':'disabled'}>
         <option value="">-- selecione --</option>
         <g:each var="item" in="${listProbExtincao}">
            <option value="${item.id}" ${ficha?.probExtincaoBrasil?.id==item.id ? 'selected':''} >${item.descricao}</option>
         </g:each>
      </select>
   </div>

   <div class="form-group">
      <label for="" class="control-label">Método de Cálculo</label>
      <br>
      <g:if test="${params.canModify}">
        <textarea name="dsMetodoCalcPercExtinBr" rows="6" class="fld form-control fldTextarea wysiwyg fld900">${ficha.dsMetodoCalcPercExtinBr}</textarea>
      </g:if>
      <g:else>
        <div class="div-justificativa">${raw(ficha.dsMetodoCalcPercExtinBr)}</div>
       </g:else>
   </div>

   %{-- botão gravar --}%
    <g:if test="${params.canModify}">
       <div class="fld panel-footer">
          <button data-action="avaliacao.saveFrmAnaliseQuantitativa" data-params="sqFicha" class="fld btn btn-success">Gravar Ameaças e Análise Quantitativa</button>
       </div>
    </g:if>
</form>