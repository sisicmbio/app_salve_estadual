%{-- foi colocado junto com analise quantitativa

<g:set var="oSND"  value="${[ [key:'S',value:'Sim'], [key:'N', value:'Não'] ,[key:'D', value:'Desconhecido'] ]}"/>
<form id="frmAmeacas" role="form" class="form-inline">
   <div class="form-group">
      <label for="" class="control-label">Nº de Localizações</label>
      <br>
      <input name="nuLocalizacoes" type="text" class="fld form-control number fld130" maxlength="5" value="${ficha.nuLocalizacoes}" />
   </div>
   <div class="form-group">
      <label for="" class="control-label">Tendência do Número de Localizações</label>
      <br>
      <select name="sqTendenNumLocalizacoes" class="fld form-control fld300 fldSelect">
         <option value="">-- selecione --</option>
         <g:each var="item" in="${listTendencia}">
            <option value="${item.id}" ${ficha?.tendenNumLocalizacoes?.id==item.id ? 'selected':''} >${item.descricao}</option>
         </g:each>
      </select>
   </div>

   <div class="form-group">
      <label for="" class="control-label">Flutuação Extrema do Nº de Localizações</label>
      <br>
      <select name="stFlutuacaoNumLocalizacoes" class="fld form-control fld300 fldSelect">
         <option value="">-- selecione --</option>
         <g:each var="item" in="${oSND}">
            <option value="${item.key}" ${ficha?.stFlutuacaoNumLocalizacoes==item.key ? ' selected' :'' }>${item.value}</option>
         </g:each>
      </select>
   </div>

   <br class="fld">

   <div class="form-group">
      <label for="" class="control-label">Tendência da Qualidade do Habitat</label>
      <br>
      <select name="sqTendenQualidadeHabitat" class="fld form-control fld300 fldSelect">
         <option value="">-- selecione --</option>
         <g:each var="item" in="${listTendencia}">
            <option value="${item.id}" ${ficha?.tendenQualidadeHabitat?.id==item.id ? 'selected':''} >${item.descricao}</option>
         </g:each>
      </select>
   </div>

   <div class="form-group">
      <label for="" class="control-label">Ameaça Futura Capaz de Chegar a C.R.</label>
      <br>
      <select name="stAmeacaFutura" class="fld form-control fld300 fldSelect">
         <option value="">-- selecione --</option>
         <g:each var="item" in="${oSND}">
            <option value="${item.key}" ${ficha?.stAmeacaFutura==item.key ? ' selected' :'' }>${item.value}</option>
         </g:each>
      </select>
   </div>
  
   <div class="fld panel-footer">
      <button data-action="avaliacao.saveFrmAmeacas" data-params="sqFicha" class="fld btn btn-success">Gravar Ameaças</button>
   </div>
</form> --}%