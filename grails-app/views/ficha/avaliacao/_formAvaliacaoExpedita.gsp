<g:set var="oSND" value="${[[key: 'S', value: 'Sim'], [key: 'N', value: 'Não'], [key: 'D', value: 'Desconhecido']]}"/>
<form class="form-inline" role="form" method="POST" id="frmAvaExpedita">
    <div class="form-group">
        <label for="">1) É favorecido ou indiferente à conversão de habitats/perturbação?</label>
        <br/>
        <select id="stFavorecidoConversaoHabitats" name="stFavorecidoConversaoHabitats" data-change="avaliacao.stChangeExpedita"
                class="fld form-control fldSelect">
            <option value="">?</option>
            <g:each var="item" in="${oSND}">
                <option value="${item.key}" ${ficha?.stFavorecidoConversaoHabitats == item.key ? ' selected' : ''}>${item.value}</option>
            </g:each>
        </select>
    </div>
    <br/>

    <div class="form-group hidden">
        <label for="">2) Pelo menos &frac34; dos registros do táxon estão em áreas amplas e íntegras >45.000km&sup2;?</label>
        <br/>
        <select id="stTemRegistroAreasAmplas" name="stTemRegistroAreasAmplas" data-change="avaliacao.stChangeExpedita" class="fld form-control fldSelect">
            <option value="">?</option>
            <g:each var="item" in="${oSND}">
                <option value="${item.key}" ${ficha?.stTemRegistroAreasAmplas == item.key ? ' selected' : ''}>${item.value}</option>
            </g:each>
        </select>
    </div>
    <br/>

    <div class="form-group hidden">
        <label for="">3a) A extensão de ocorrência é ampla (>45.000km&sup2;) ?</label>
        <br/>
        <select id="stPossuiAmplaDistGeografica" name="stPossuiAmplaDistGeografica" data-change="avaliacao.stChangeExpedita" class="fld form-control fldSelect">
            <option value="">?</option>
            <g:each var="item" in="${oSND}">
                <option value="${item.key}" ${ficha?.stPossuiAmplaDistGeografica == item.key ? ' selected' : ''}>${item.value}</option>
            </g:each>
        </select>
    </div>
    <br/>

    <div class="form-group hidden">
        <label for="">3b) É frequente em inventários nos últimos dez anos ou três gerações?</label>
        <br/>
        <select id="stFrequenteInventarioEoo" name="stFrequenteInventarioEoo" data-change="avaliacao.stChangeExpedita" class="fld form-control fldSelect ">
            <option value="">?</option>
            <g:each var="item" in="${oSND}">
                <option value="${item.key}" ${ficha?.stFrequenteInventarioEoo == item.key ? ' selected' : ''}>${item.value}</option>
            </g:each>
        </select>
    </div>

    %{-- botão --}%
    <div class="fld panel-footer">
        <button data-action="avaliacao.saveAvaliacaoExpedita" data-params="sqFicha" class="fld btn btn-success">Gravar</button>
    </div>
</form>
