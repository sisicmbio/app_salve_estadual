<%@ page import="br.gov.icmbio.Util" %>
<!-- view: /views/ficha/avaliacao/_formResultadoFinal -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<div>

    %{--<FORMULÁRIO--}%
    <div class="form form-inline" role="form">

      %{-- CONTROLAR EDICAO ESPECIFICO DA ABA INDEPENDENTE DA FICHA--}%
      <input type="hidden" id="canModify" value="${canModify}"/>

      %{-- ARMAZENAR DADOS DA ULTIMA AVALIAÇÃO PARA CONTROLE DA MUDANCA DE CATEGORIA --}%
      <input type="hidden" name="sqCategoriaIucnUltimaAvaliacao" value="${dadosUltimaAvaliacao.sqCategoriaIucn}"/>
      <input type="hidden" name="cdCategoriaIucnUltimaAvaliacao" value="${dadosUltimaAvaliacao.coCategoriaIucn}"/>


      %{-- NÃO PERMITIR ALTERAR A CATEGORIA SE A FICHA ESTIVER VALIDADA EM DIANTE --}%
      <g:if test="${canModify && canModifyCategory}">
          %{--<CATEGORIA--}%
          <div class="form-group mb5">
             <label for="sqCategoriaIucn" class="control-label">Categoria</label>
             <br>
              <select name="sqCategoriaIucn" id="sqCategoriaIucn"
                      class="fld form-control fld400 fldSelect"
                      data-sq-ficha="${ficha.id}"
                      data-change="app.categoriaIucnChange"
                      data-container="tabAvaResultadoFinal">
                  <option value="">-- selecione --</option>
                  <g:each var="item" in="${listCategoriaIucn}">
                      <option data-codigo="${item.codigoSistema}" value="${item.id}"
                          ${ ( item?.id == ficha?.categoriaFinal?.id ) ? 'selected':''}>${item.descricaoCompleta}</option>
                  %{--                       ${ ( item?.id == ficha?.categoriaFinal?.id || (!ficha.categoriaFinal && item.codigoSistema=='XNE') ) ? 'selected':''}>${item.descricaoCompleta}</option>--}%
                  </g:each>
              </select>
          </div>

          %{--<CRITERIO--}%
          <div class="form-group mb5" style="display:none;">
             <label for="dsCriterioAvalIucn" class="control-label">Critério</label>
             <br>
             <input name="dsCriterioAvalIucn" id="dsCriterioAvalIucn"
                    class="fld form-control fld300 boldAzul"
                    type="text"
                    readonly="true"
                    onkeyup="fldCriterioAvaliacaoKeyUp(this);"
                    value="${ficha.dsCriterioAvalIucnFinal}"/>
             <button id="btnSelCriterioAvaliacao"
                     class="fld btn btn-default btn-sm"
                     type="button" title="Clique para selecionar o critério"
                     data-action="openModalSelCriterioAvaliacao"
                     data-field="dsCriterioAvalIucn"
                     data-container="tabAvaResultadoFinal"
                     data-sq-ficha="${ficha.id}"
                     data-field-categoria="sqCategoriaIucn">...</button>
          </div>

          %{--<POSSIVELMENTE EXTINTA--}%
          <div class="form-group mt10" style="display:none;">
             <label for="stPossivelmenteExtinta"
                    class="control-label cursor-pointer label-checkbox"
                    style="margin-top:17px;">Possivelmente Extinta?&nbsp;
                <input name="stPossivelmenteExtinta" id="stPossivelmenteExtinta"
                       class="checkbox-lg"
                       type="checkbox"
                       value="S"
                   ${ficha.stPossivelmenteExtintaFinal == 'S' ? ' checked':''}/>
             </label>
          </div>
          <br>
      </g:if>
      <g:else>
          <div class="form-group mb5">
              <label for="sqCategoriaIucn" class="control-label">Categoria</label>
              <br>
              <select name="sqCategoriaIucn" id="sqCategoriaIucn"
                      class="fld form-control fld400 fldSelect"
                      data-sq-ficha="${ficha.id}"
                      data-change="app.categoriaIucnChange"
                      data-container="tabAvaResultadoFinal"
                      disabled="true">
                  <option value=""></option>
                  <g:each var="item" in="${listCategoriaIucn}">
                      <g:if test="${item?.id == ficha?.categoriaFinal?.id}">
                          <option data-codigo="${item.codigoSistema}" value="${item.id}" selected>${item.descricaoCompleta}</option>
                      </g:if>
                  </g:each>
              </select>

          </div>

          %{--<CRITERIO--}%
          <div class="form-group mb5" style="display:none;">
              <label for="dsCriterioAvalIucn" class="control-label">Critério</label>
              <br>
              <input name="dsCriterioAvalIucn" id="dsCriterioAvalIucn"
                     class="fld form-control fld300 boldAzul"
                     type="text"
                     disabled="true"
                     onkeyup="fldCriterioAvaliacaoKeyUp(this);"
                     value="${ficha.dsCriterioAvalIucnFinal}"/>
          </div>

        %{--<POSSIVELMENTE EXTINTA--}%
          <div class="form-group mt10" style="display:none;">
              <label for="stPossivelmenteExtinta"
                     class="control-label cursor-pointer label-checkbox"
                     style="margin-top:17px;">Possivelmente Extinta?&nbsp;
                  <input name="stPossivelmenteExtinta" id="stPossivelmenteExtinta"
                         class="checkbox-lg"
                         type="checkbox"
                         disabled="true"
                         value="S"
                      ${ficha.stPossivelmenteExtintaFinal == 'S' ? ' checked':''}/>
              </label>
          </div>

      </g:else>
      %{--<JUSTIFICATIVA--}%
      <div class="form-group w100p">
          <br>
         <g:if test="${canModify}">
            <label for="dsJustificativaFinal${ficha.id}" class="control-label">Justificativa</label>
            <br>
            <textarea name="dsJustificativa" id="dsJustificativaFinal${ficha.id}" rows="7" class="fld form-control fldTextarea wysiwyg w100p texto">${raw(ficha.dsJustificativaFinal)}</textarea>
         </g:if>
         <g:else>
            <div class="input-group w100p">
               <span class="input-group-addon" style="width:150px;">Justificativa</span>
               <div class="text-justify" style="min-height: 28px;max-height:200px;overflow-y: auto;padding:3px;border:1px solid silver;background-color: lightyellow !important;">${raw(ficha.dsJustificativaFinal)}</div>
            </div>
         </g:else>
      </div>

      <br class="fld">

      %{--<MOTIVO DA MUDANCA--}%
      <div id="divDadosMotivoMudanca${ficha.id}"
           class="panel panel-success mt10" style="display:none;">
         <div class="panel-heading">${raw(dadosUltimaAvaliacao.coCategoriaIucn ? 'Mudança da categoria <span class="cursor-pointer" title="'+dadosUltimaAvaliacao.deCategoriaIucn+'">' + dadosUltimaAvaliacao.coCategoriaIucn+'</span>':'Mudança de categoria')}</div>
         <div class="panel-body" style="background-color:#8fbc8f;">
            <g:if test="${canModify}">
               <div class="form-group">
                  <label for="sqMotivoMudanca" class="control-label">Motivo da mudança</label>
                  <br>
                  <select id="sqMotivoMudanca"
                          class="fld form-control fld300 fldSelect"
                     ${!canModify?'disabled="true"':''}>
                     <option value="">-- selecione --</option>
                     <g:each var="item" in="${listMudancaCategoria}">
                        <option value="${item.id}">${item.descricao}</option>
                     </g:each>
                  </select>
                  <button type="button" id="btnSaveMotivoMudanca"
                          data-action="addMotivoMudanca"
                          data-sq-ficha="${ficha.id}"
                          data-container="tabAvaResultadoFinal"
                          data-contexto="tabAvaResultadoFinal"
                          data-params="sqFichaMudancaCategoria,sqMotivoMudanca,txFichaMudancaCategoria,canModify:${canModify}"
                          class="fld btn btn-sm btn-primary">Adicionar motivo mudança</button>
               </div>
            </g:if>
            <div id="divGridMotivoMudancaCategoria" class="mt10"></div>
            <div class="form-group w100p">
               <label for="dsJustMudancaCategoriaFinal${ficha.id}" class="control-label">Justificativa para a mudança de categoria</label>
               <br>
               <g:if test="${canModify}">
                  <textarea name="dsJustMudancaCategoriaFinal" id="dsJustMudancaCategoriaFinal${ficha.id}"
                            class="fld form-control fldTextarea wysiwyg w100p"
                            rows="5">${ficha.dsJustMudancaCategoria}</textarea>
               </g:if>
               <g:else>
                  <div class="text-justify" style="min-height: 28px;max-height: 300px;overflow-y: auto;padding:3px;border:1px solid silver;background-color: #eeeeee">${raw(ficha.dsJustMudancaCategoria)}</div>
               </g:else>
            </div>
         </div>
      </div> %{--<FIM MOTIVO DA MUDANCA--}%


       %{--<BOTAO SALVAR RESULTADO--}%
      <div class="fld panel-footer">
         <button class="fld btn btn-success"
                 type="button"
                 data-container="tabAvaResultadoFinal"
                 data-action="resultadoValidacao.save"
                 data-sq-ficha="${ficha.id}"
                 data-avaliada-revisada="N">Gravar validação
         </button>
      </div>

   </div>
</div>

%{--fim formResultadoFinal--}%
