<%@ page import="br.gov.icmbio.Util" %>
<!-- view: /views/ficha/avaliacao/_formResultadoOficina.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<g:set var="oSND"  value="${[ [key:'S',value:'Sim'], [key:'N', value:'Não'] ,[key:'D', value:'Desconhecido'] ]}"/>
<div>

    %{-- CONTROLAR EDICAO ESPECIFICO DA ABA INDEPENDENTE DA FICHA--}%
    <input type="hidden" id="canModify" value="${canModify}"/>

    %{--<REPETIR A ULTIMA AVALIACAO--}%
    <g:if test="${params.canModify}">
        <button id="btnRepetirUltimaAvaliacao"
                type="button"
                class="fld btn btn-primary btn-sm mt10"
                data-sq-ficha="${ficha.id}"
                data-action="app.repetirUltimaAvaliacao"
                data-container="tabAvaResultado">Repetir última avaliação</button>
        <br class="fld">
    </g:if>

    %{--<FORMULÁRIO--}%
    <div class="form form-inline" role="form">

        %{-- ARMAZENAR DADOS DA ULTIMA AVALIAÇÃO PARA CONTROLE DA MUDANCA DE CATEGORIA --}%
        <input type="hidden" name="sqCategoriaIucnUltimaAvaliacao" value="${dadosUltimaAvaliacao.sqCategoriaIucn}"/>
        <input type="hidden" name="cdCategoriaIucnUltimaAvaliacao" value="${dadosUltimaAvaliacao.coCategoriaIucn}"/>


        %{--<CATEGORIA--}%
        <g:if test="${canModifyCategory}">
            <div class="form-group mb5">
                <label for="sqCategoriaIucn" class="control-label">Categoria</label>
                <br>
                <select name="sqCategoriaIucn" id="sqCategoriaIucn"
                        class="fld form-control fld400 fldSelect"
                        data-sq-ficha="${ficha.id}"
                        data-change="app.categoriaIucnChange"
                        data-container="tabAvaResultado">
                    <g:if test="${ ! ficha?.categoriaIucn}">--}%
                        <option value="">-- selecione --</option>
                    </g:if>--}%
                    <g:each var="item" in="${listCategoriaIucn}">
                        <option data-codigo="${item.codigoSistema}" value="${item.id}" ${item?.id == ficha?.categoriaIucn?.id ? 'selected':''}>${item.descricaoCompleta}</option>
                    </g:each>
                </select>
            </div>

        %{--<CRITERIO--}%
            <div class="form-group mb5" style="display:none;">
                <label for="dsCriterioAvalIucn" class="control-label">Critério</label>
                <br>
                <input name="dsCriterioAvalIucn" id="dsCriterioAvalIucn"
                       class="fld form-control fld300 boldAzul"
                       type="text"
                       readonly="true"
                       onkeyup="fldCriterioAvaliacaoKeyUp(this);"
                       value="${ficha.dsCriterioAvalIucn}"/>
                <button id="btnSelCriterioAvaliacao"
                        class="fld btn btn-default btn-sm"
                        type="button" title="Clique para selecionar o critério"
                        data-action="openModalSelCriterioAvaliacao"
                        data-field="dsCriterioAvalIucn"
                        data-container="tabAvaResultado"
                        data-contexto="tabAvaResultado"
                        data-sq-ficha="${ficha.id}"
                        data-field-categoria="sqCategoriaIucn">...</button>
            </div>

        %{--<POSSIVELMENTE EXTINTA--}%
            <div class="form-group mt10" style="display:none;">
                <label for="stPossivelmenteExtinta"
                       class="control-label cursor-pointer label-checkbox"
                       style="margin-top:17px;">Possivelmente Extinta?&nbsp;
                    <input name="stPossivelmenteExtinta" id="stPossivelmenteExtinta"
                           class="checkbox-lg"
                           type="checkbox"
                           value="S"
                        ${ficha.stPossivelmenteExtinta == 'S' ? ' checked':''}/>
                </label>
            </div>
            <br>
        </g:if>
        <g:else>

            <div class="form-group mb5">
                <label for="sqCategoriaIucn" class="control-label">Categoria</label>
                <br>
                <select name="sqCategoriaIucn" id="sqCategoriaIucn"
                        class="fld form-control fld400 fldSelect"
                        data-sq-ficha="${ficha.id}"
                        data-change="app.categoriaIucnChange"
                        data-container="tabAvaResultado"
                        disabled="true">
                    <option value=""></option>
                    <g:each var="item" in="${listCategoriaIucn}">
                        <g:if test="${item?.id == ficha?.categoriaIucn?.id}">
                            <option data-codigo="${item.codigoSistema}" value="${item.id}" selected>${item.descricaoCompleta}</option>
                        </g:if>
                    </g:each>
                </select>
            </div>

        %{--<CRITERIO--}%
            <div class="form-group mb5" style="display:none;">
                <label for="dsCriterioAvalIucn" class="control-label">Critério</label>
                <br>
                <input name="dsCriterioAvalIucn" id="dsCriterioAvalIucn"
                       class="fld form-control fld300 boldAzul"
                       type="text"
                       readonly="true"
                       onkeyup="fldCriterioAvaliacaoKeyUp(this);"
                       value="${ficha.dsCriterioAvalIucn}"/>
            </div>

            %{--<POSSIVELMENTE EXTINTA--}%
            <div class="form-group mt10" style="display:none;">
                <label for="stPossivelmenteExtinta"
                       class="control-label cursor-pointer label-checkbox"
                       style="margin-top:17px;">Possivelmente Extinta?&nbsp;
                    <input name="stPossivelmenteExtinta" id="stPossivelmenteExtinta"
                           class="checkbox-lg"
                           type="checkbox"
                           disabled="true">
                           value="S"
                        ${ficha.stPossivelmenteExtinta == 'S' ? ' checked':''}/>
                </label>
            </div>
        </g:else>


        %{--<JUSTIFICATIVA--}%
        <div class="form-group w100p">
            <br>
            <g:if test="${canModify}">
                <label for="dsJustificativa${ficha.id}" class="control-label">Justificativa</label>
                <br>
                %{--TINYMCE TEM QUE TER IDs DISTINTOS PARA CADA TEXTAREA--}%
                <textarea name="dsJustificativa" id="dsJustificativaValidacao${ficha.id}" rows="5" class="fld form-control fldTextarea wysiwyg w100p texto">${raw(ficha.dsJustificativa)}</textarea>
            </g:if>
            <g:else>
                <div class="input-group w100p">
                    <span class="input-group-addon" style="width:150px;">Justificativa</span>
                    <div class="text-justify" style="min-height: 28px;max-height:200px;overflow-y: auto;padding:3px;border:1px solid silver;background-color: lightyellow !important;">${raw(ficha.dsJustificativa)}</div>
                </div>
            </g:else>
        </div>

        <br class="fld">

        %{--<MOTIVO DA MUDANCA--}%
        <div id="divDadosMotivoMudanca${ficha.id}"
             class="panel panel-success mt10" style="display:none;">
            <div class="panel-heading">${raw(dadosUltimaAvaliacao.coCategoriaIucn ? 'Mudança da categoria <span class="cursor-pointer" title="'+dadosUltimaAvaliacao.deCategoriaIucn+'">' + dadosUltimaAvaliacao.coCategoriaIucn+'</span>':'Mudança de categoria')}</div>
            <div class="panel-body" style="background-color:#8fbc8f;">
                <g:if test="${canModify}">
                    <div class="form-group">
                        <label for="sqMotivoMudanca" class="control-label">Motivo da mudança</label>
                        <br>
                        <select id="sqMotivoMudanca"
                                class="fld form-control fld300 fldSelect"
                                ${!canModify?'disabled="true"':''}>
                            <option value="">-- selecione --</option>
                            <g:each var="item" in="${listMudancaCategoria}">
                                <option value="${item.id}">${item.descricao}</option>
                            </g:each>
                        </select>
                        <button type="button" id="btnSaveMotivoMudanca"
                                data-action="addMotivoMudanca"
                                data-sq-ficha="${ficha.id}"
                                data-container="tabAvaResultado"
                                data-contexto="tabAvaResultado"
                                data-params="sqFichaMudancaCategoria,sqMotivoMudanca,txFichaMudancaCategoria,canModify:${canModify}"
                                class="fld btn btn-sm btn-primary">Adicionar motivo mudança</button>
                    </div>
                </g:if>
                <div id="divGridMotivoMudancaCategoria" class="mt10"></div>
                <div class="form-group w100p">
                    <label for="dsJustMudancaCategoria${ficha.id}" class="control-label">Justificativa para a mudança de categoria</label>
                    <br>
                    <g:if test="${canModify}">
                        <textarea name="dsJustMudancaCategoria" id="dsJustMudancaCategoria${ficha.id}"
                                  class="fld form-control fldTextarea wysiwyg w100p"
                                  rows="5">${ficha.dsJustMudancaCategoria}</textarea>
                    </g:if>
                    <g:else>
                        <div class="text-justify" style="min-height: 28px;max-height: 300px;overflow-y: auto;padding:3px;border:1px solid silver;background-color: #eeeeee">${raw(ficha.dsJustMudancaCategoria)}</div>
                    </g:else>
                </div>
            </div>
        </div> %{--<FIM MOTIVO DA MUDANCA--}%

        %{--AJUSTE REGIONAL QUANDO A FICHA NÃO FOR ENDEMICA DO BRASIL --}%
        <g:if test="${ ficha.stEndemicaBrasil != 'S' }">
            <br>
            <div id="divDadosAjusteRegional${ficha.id}"
                 class="panel panel-success mt10" style="display:inline-block;">
                <div class="panel-heading">Ajuste Regional</div>
                <div class="panel-body" style="background-color:#fffec5;">
                    <div class="form-group">
                        <div class="checkbox fld">
                            <label title="Clique aqui para preencher o campo com o texto padrão">
                                <br>
                                <input type="checkbox" data-action="checkRespostaPadraoAjusteRegionalClick"
                                       data-target="dsConectividadePopExterior${ficha.id}"
                                    ${ ('não foram encontradas informações para o táxon.' == br.gov.icmbio.Util.stripTags( ficha.dsConectividadePopExterior?.toLowerCase() ).trim()) ? 'checked' : '' }
                                       class="checkbox-lg">&nbsp;&nbsp;"Preencher com texto padrão"
                            </label>
                        </div>
                    </div>
                    <br class="fld">

                    %{--<Ajuste Reginal: TIPO CONECTIVIDADE--}%
                    <div class="form-group">
                        <label for="sqTipoConectividade" class="control-label">Conectividade com populações de outros países</label>
                        <br>
                        <select name="sqTipoConectividade" id="sqTipoConectividade"
                                class="fld form-control fld450 fldSelect">
                            <option value="">-- selecione --</option>
                            <g:each var="item" in="${listConectividade}">
                                <option data-codigo="${item.codigoSistema}" value="${item.id}" ${(ficha?.tipoConectividade?.id==item.id)?' selected':''}>${item.descricao}</option>
                            </g:each>
                        </select>
                    </div>

                    %{--<Ajuste Reginal: TENDÊNCIA DE IMIGRAÇÃO NO BRASIL --}%
                    <div class="form-group">
                        <label for="sqTendenciaImigracao" class="control-label">Tendência de imigração no Brasil</label>
                        <br>
                        <select name="sqTendenciaImigracao" id="sqTendenciaImigracao"
                                class="fld form-control fld300 fldSelect">
                            <option value="">-- selecione --</option>
                            <g:each var="item" in="${listTendencia}">
                                <option data-codigo="${item.codigoSistema}" value="${item.id}" ${ficha?.tendenciaImigracao?.id==item.id ? 'selected':''} >${item.descricao}</option>
                            </g:each>
                        </select>
                    </div>

                    %{--<Ajuste Reginal: POPULAÇÃO BRASILEIRA PODE DECLINAR --}%
                    <div class="form-group">
                        <label for="stDeclinioBrPopulExterior" class="control-label">População brasileira pode declinar</label>
                        <br>
                        <select name="stDeclinioBrPopulExterior" id="stDeclinioBrPopulExterior"
                                class="fld form-control fld300 fldSelect" ${canModify?'':'disabled'}>
                            <option value="">-- selecione --</option>
                            <g:each var="item" in="${oSND}">
                                <option data-codigo="${item.key}" value="${item.key}" ${ficha?.stDeclinioBrPopulExterior==item.key ? ' selected' :'' }>${item.value}</option>
                            </g:each>
                        </select>
                    </div>

                    %{--<Ajuste Reginal: OBSERVAÇÕES --}%
                    <div class="form-group w100p">
                        <label for="" class="control-label">Observações</label>
                        <br>
                        <g:if test="${canModify}">
                            <textarea name="dsConectividadePopExterior" id="dsConectividadePopExterior${ficha.id}"
                                      class="fld form-control fldTextarea wysiwyg w100p"
                                      rows="5">${ficha.dsConectividadePopExterior}</textarea>
                        </g:if>
                        <g:else>
                            <div class="text-justify" style="min-height: 28px;max-height:200px;overflow-y: auto;padding:3px;border:1px solid silver;">${raw(ficha.dsConectividadePopExterior)}</div>
                        </g:else>
                    </div>
                </div>
            </div>
            %{--<fim campos ajuste regional--}%
        </g:if>

<br>

        %{-- CITACAO / AUTORIA--}%
        <div class="form-group w100p" id="divDadosCitacao${ficha.id}">
            <g:if test="${canModifyEquipes}">
                <label for="dsCitacao${ficha.id}" class="control-label" style="margin-bottom:2px !important;"><b>Autoria</b>
                    <button type="button" title="Clique neste botão para gerar a autoria padrão composta pelo<br>nome do(s) <b>Coordenador(es) de Taxon ativo(s) + Avaliador(es)</b><br>cadastrado(s) na(s) avaliação(ões)."
                            data-action="gerarAutoria" data-sq-ficha="${ficha?.id}" class="btn btn-xs btn-default">Gerar a autoria</button>
                </label>
                <br>
            %{--TINYMCE TEM QUE TER IDs DISTINTOS PARA CADA TEXTAREA--}%
                <textarea name="dsCitacao" id="dsCitacao${ficha.id}" rows="5" class="form-control fldTextarea w100p texto">${raw(ficha.dsCitacao)}</textarea>
                <button class="btn btn-sm btn-primary mt5"
                        type="button"
                        data-container="tabAvaResultado"
                        data-action="resultadoAvaliacao.saveAutoria"
                        data-sq-ficha="${ficha.id}">Gravar a autoria
                </button>

            </g:if>
            <g:else>
                <div class="input-group w100p">
                    <span class="input-group-addon" style="width:150px;"><b>Autoria</b></span>
                    <div class="text-justify" style="min-height: 28px;max-height:200px;overflow-y: auto;padding:3px;border:1px solid silver;background-color: lightyellow !important;">${raw(ficha.dsCitacao)}</div>
                </div>
            </g:else>
        </div>

        %{-- EQUIPE TÉNCNICA --}%
        <div class="form-group w100p mt20" id="divDadosEquipeTecnica${ficha.id}">
            <g:if test="${canModifyEquipes}">
                <label for="dsCitacao${ficha.id}" class="control-label" style="margin-bottom:2px !important;"><b>Equipe técnica</b>
%{--                A equipe técnica será sempre cadastrada manualmente --}%
%{--                    <button type="button" title="Clique neste botão para adicionar os nomes."--}%
%{--                            data-action="gerarEquipeTecnica" data-sq-ficha="${ficha?.id}" class="btn btn-xs btn-default">Gerar equipe técnica</button>--}%
                </label>
                <br>
            %{--TINYMCE TEM QUE TER IDs DISTINTOS PARA CADA TEXTAREA--}%
                <textarea name="dsEquipeTecnica" id="dsEquipeTecnica${ficha.id}" rows="5" class="form-control fldTextarea w100p texto">${raw(ficha.dsEquipeTecnica)}</textarea>
                <button class="btn btn-sm btn-primary mt5"
                        type="button"
                        data-container="tabAvaResultado"
                        data-action="resultadoAvaliacao.saveEquipeTecnica"
                        data-sq-ficha="${ficha.id}">Gravar a equipe técnica
                </button>
            </g:if>
            <g:else>
                <div class="input-group w100p">
                    <span class="input-group-addon" style="width:150px;"><b>Equipe técnica</b></span>
                    <div class="text-justify" style="min-height: 28px;max-height:200px;overflow-y: auto;padding:3px;border:1px solid silver;background-color: lightyellow !important;">${raw(ficha.dsEquipeTecnica)}</div>
                </div>
            </g:else>
        </div>

        %{-- COLABORADORES --}%
        <div class="form-group w100p mt20" id="divDadosColaboradores${ficha.id}">
            <g:if test="${canModifyEquipes}">
                <label for="dsCitacao${ficha.id}" class="control-label" style="margin-bottom:2px !important;"><b>Colaboradores</b>
                    <button type="button" title="Clique neste botão para adicionar os nomes."
                            data-action="gerarColaboradores" data-sq-ficha="${ficha?.id}" class="btn btn-xs btn-default">Gerar colaboradores</button>
                </label>
                <br>
            %{--TINYMCE TEM QUE TER IDs DISTINTOS PARA CADA TEXTAREA--}%
                <textarea name="dsColaboradores" id="dsColaboradores${ficha.id}" rows="5" class="form-control fldTextarea w100p texto">${raw(ficha.dsColaboradores)}</textarea>
                <button class="btn btn-sm btn-primary mt5"
                        type="button"
                        data-container="tabAvaResultado"
                        data-action="resultadoAvaliacao.saveColaboradores"
                        data-sq-ficha="${ficha.id}">Gravar os colaboradores
                </button>
            </g:if>
            <g:else>
                <div class="input-group w100p">
                    <span class="input-group-addon" style="width:150px;"><b>Colaboradores</b></span>
                    <div class="text-justify" style="min-height: 28px;max-height:200px;overflow-y: auto;padding:3px;border:1px solid silver;background-color: lightyellow !important;">${raw(ficha.dsColaboradores)}</div>
                </div>
            </g:else>
        </div>

        %{--<BOTAO SALVAR RESULTADO--}%
        <div class="fld panel-footer">
            <button class="fld btn btn-success mt20"
                    type="button"
                    data-container="tabAvaResultado"
                    data-action="resultadoAvaliacao.save"
                    data-sq-ficha="${ficha.id}"
                    data-avaliada-revisada="N">Gravar a avaliação
            </button>
        </div>

</div>
%{--fim formResultadoOficina--}%
