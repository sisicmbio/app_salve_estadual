<!-- view: /views/ficha/avaliacao/_divGridDeclinioPopulacional.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<!-- Obs: as funcoes JS da aba avaliacao estao no arquivo fichaCompleta.js porque esta aba é tambem alimentada na ficha completa -->

<table class="table table-hover table-striped table-condensed table-bordered">
	<thead>
		<th>Declínio Populacional Selecionado</th>
		<g:if test="${ params.canModify }">
			<th>Ação</th>
		</g:if>
	</thead>
	<tbody>
	<g:each var="item" in="${listaDeclinios}">
		<tr>
			<td>${item?.declinioPopulacional?.descricao}</td>
			<g:if test="${ params.canModify }">
				<td style="text-align:center;width:100px;">
					<a data-action="avaliacao.deleteDeclinioPopulacional" data-descricao="${item?.declinioPopulacional.descricao}" data-id="${item.id}" class="fld btn btn-default btn-xs btn-delete" title="Excluir">
						<span class="glyphicon glyphicon-remove"></span>
					</a>
				</td>
			</g:if>
		</tr>
	</g:each>
	</tbody>
</table>
