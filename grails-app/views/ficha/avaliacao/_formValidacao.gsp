<g:set var="oSN" value="${[[key: 'S', value: 'Sim'], [key: 'N', value: 'Não']]}"/>
<br/>
<div class="alert alert-info">
    <h4>Rever o conteúdo desta aba!</h4>
</div>
%{--
<table class="table table-hover table-striped table-condensed table-bordered mt10">
    <thead>
    <th>#</th>
    <th >Validador</th>
    <th>Categoria</th>
    <th>Aceita?</th>
    <th>Categoria Sugerida</th>
    <th>Critério</th>
    <th>Aceita?</th>
    <th>Critério Sugerido</th>
    <th style="width:80px;">Ação</th>
    </thead>
    <tbody>
    <tr>
        <td class="text-center">1º</td>
        <td style="width:*;">Marcelo Teixeira</td>
        <td class="text-center">EX-Extinta</td>
        <td class="text-center">Não</td>
        <td class="text-center">Vulnerável (VU)</td>
        <td class="text-center">A1a</td>
        <td class="text-center">Não</td>
        <td class="text-center">A2a</td>
        <td class="td-actions">
            <a data-action="avaliacao.openModalComentario" data-params="sqFicha,noPessoa:Marcelo Teixeira" data-id="1" class="btn btn-default btn-sm" title="Comentários">
                Comentários
            </a>
        </td>
    </tr>
    <tr>
        <td class="text-center">2º</td>
        <td style="width:*;">Maria Abrão</td>
        <td class="text-center">Extinta (EX)</td>
        <td class="text-center">Não</td>
        <td class="text-center">Vulnerável (VU)</td>
        <td class="text-center">A1a</td>
        <td class="text-center">Não</td>
        <td class="text-center">A2a</td>
        <td class="td-actions">
            <a data-action="avaliacao.openModalComentario"  data-params="sqFicha,noPessoa:Maria Abrão" data-id="2" class="btn btn-default btn-sm" title="Comentários">
                Comentários
            </a>
        </td>
    </tr>
    </tbody>
</table>

<form id="frmValidacao" role="form" class="form-inline">
    <fieldset>
        <legend>
            Ponto Focal
        </legend>
        <div class="form-group">
            <label for="" class="control-label">Aceita as Validações?</label>
            <br>
            <select id="stCategoriaOk" name="stCategoriaOk" class="fld form-control fld200 fldSelect">
                <label for="" class="control-label">Justificativa</label>
                <option value="">-- selecione --</option>
                <g:each var="item" in="${oSN}">
                    <option value="${item.key}" ${ficha?.stCategoriaOk == item.key ? ' selected' : ''}>${item.value}</option>
                </g:each>
            </select>
        </div>

        <div class="form-group w100p mt10">
            <label for="" class="control-label">Justificativa</label>
            <br>
            <textarea name="dsJustificativaValidacao" id="dsJustificativaValidacao" rows="4" class="fld form-control fldTextarea wysiwyg w100p"></textarea>
        </div>

        <div class="fld panel-footer">
            <button data-action="avaliacao.saveFrmValidacao" data-params="sqFicha" class="fld btn btn-success">Gravar</button>
        </div>

    </fieldset>

</form>
--}%



%{--<ul class="nav nav-tabs nav-sub-tabs mt10" id="ulTabsAvaliacaoValidacao">--}%
    %{--<li id="litabAvaVal01" class="active"><a data-action="ficha.tabClick" data-container="tabAvaVa01" data-toggle="tab" href="#tabAvaVa01">Marcelo Teixeira</a></li>--}%
    %{--<li id="litabAvaVal02"><a data-action="ficha.tabClick" data-container="tabAvaVa02" data-toggle="tab" href="#tabAvaVa02">Marcia Abrão</a></li>--}%
%{--</ul>--}%

%{--<div id="avaValTabContent" class="tab-content">--}%
    %{--<div role="tabpanel" class="tab-pane tab-pane-ficha subTabs10 active" id="tabAvaVa01" style="background-color:#ccf083">--}%
        %{--<div role="form" class="form-inline">--}%
            %{--<div class="form-group">--}%
                %{--<label for="" class="control-label">Categoria</label>--}%
                %{--<br>--}%
                %{--<div class="blue fld200" id="">EX-Extinta</div>--}%
            %{--</div>--}%
            %{--<div class="form-group">--}%
                %{--<label for="" class="control-label">Aceita?</label>--}%
                %{--<br>--}%
                %{--<div id="" class="fld100 red">Não</div>--}%
            %{--</div>--}%
            %{--<div class="form-group">--}%
                %{--<label for="" class="control-label">Sugerida</label>--}%
                %{--<br>--}%
                %{--<div id="" class="fld200">VU-Vulnerável</div>--}%
            %{--</div>--}%

            %{--<br/>--}%

            %{--<div class="form-group">--}%
                %{--<label for="" class="control-label">Critério</label>--}%
                %{--<br>--}%
                %{--<div class="blue fld200" id="">A1+2</div>--}%
            %{--</div>--}%

            %{--<div class="form-group">--}%
                %{--<label for="" class="control-label">Aceita?</label>--}%
                %{--<br>--}%
                %{--<div id="" class="fld100 green">Sim</div>--}%
            %{--</div>--}%

            %{--<div class="form-group">--}%
                %{--<label for="" class="control-label">Sugerido</label>--}%
                %{--<br>--}%
                %{--<div class="green fld400" id="">Crit Sugerido</div>--}%
            %{--</div>--}%

            %{--<div class="form-group w100p">--}%
                %{--<label for="" class="control-label">Comentários</label>--}%
                %{--<textarea id="" rows="4" class="fld form-control fldTextarea wysiwygRO w100p"></textarea>--}%
            %{--</div>--}%
        %{--</div>--}%
    %{--</div>--}%

    %{--<div role="tabpanel" class="tab-pane tab-pane-ficha subTabs10" id="tabAvaVa02" style="background-color: #f0e487">--}%
        %{--<form id="frmValidacao02" role="form" class="form-inline">--}%
            %{--<div class="form-group">--}%
                %{--<label for="" class="control-label">Categoria</label>--}%
                %{--<br>--}%
                %{--<div class="blue fld200" id="">EX-Extinta</div>--}%
            %{--</div>--}%

            %{--<div class="form-group">--}%
                %{--<label for="" class="control-label">Aceita?</label>--}%
                %{--<br>--}%
                %{--<div id="" class="fld100 red">Não</div>--}%
            %{--</div>--}%

            %{--<div class="form-group">--}%
                %{--<label for="" class="control-label">Sugerida</label>--}%
                %{--<br>--}%
                %{--<div id="" class="fld200">VU-Vulnerável</div>--}%
            %{--</div>--}%
            %{--<br/>--}%

            %{--<div class="form-group">--}%
                %{--<label for="" class="control-label">Critério</label>--}%
                %{--<br>--}%
                %{--<div class="blue fld200" id="">A1+2</div>--}%
            %{--</div>--}%

            %{--<div class="form-group">--}%
                %{--<label for="" class="control-label">Aceita?</label>--}%
                %{--<br>--}%
                %{--<div id="" class="fld100">Sim</div>--}%
            %{--</div>--}%

            %{--<div class="form-group">--}%
                %{--<label for="" class="control-label">Sugerido</label>--}%
                %{--<br>--}%
                %{--<div class="green fld400" id="">Crit Sugerido</div>--}%
            %{--</div>--}%

            %{--<div class="form-group w100p">--}%
                %{--<label for="" class="control-label">Comentários</label>--}%
                %{--<textarea id="" rows="4" class="fld form-control fldTextarea wysiwygRO w100p"></textarea>--}%
            %{--</div>--}%
        %{--</form>--}%
    %{--</div>--}%
%{--</div>--}%
%{--<br />--}%

%{--<form id="frmValidacao_old" role="form" class="form-inline hidden">--}%
    %{--<div class="col-sm-12">--}%
        %{--<div class="form-group">--}%
            %{--<label for="" class="control-label">Aceita Categoria?</label>--}%
            %{--<br>--}%
            %{--<select id="stCategoriaOk" name="stCategoriaOk" data-change="avaliacao.stCategoriaOkChange" class="fld form-control fld200 fldSelect">--}%
                %{--<option value="">-- selecione --</option>--}%
                %{--<g:each var="item" in="${oSN}">--}%
                    %{--<option value="${item.key}" ${ficha?.stCategoriaOk == item.key ? ' selected' : ''}>${item.value}</option>--}%
                %{--</g:each>--}%
            %{--</select>--}%
        %{--</div>--}%

        %{-- se o campo anterior for NÃO --}%
        %{--<div class="form-group">--}%
            %{--<label for="" class="control-label">Categoria Sugerida</label>--}%
            %{--<br>--}%
            %{--<select id="sqCategoriaIucnSugestao" name="sqCategoriaIucnSugestao" data-change="avaliacao.sqCategoriaIucnSugestaoChange" class="fld form-control fld300 fldSelect">--}%
                %{--<option value="">-- selecione --</option>--}%
                %{--<g:each var="item" in="${listCategoriaIucn}">--}%
                    %{--<option data-codigo="${item.codigoSistema}" value="${item.id}" ${ficha?.categoriaIucnSugestao?.id == item.id ? ' selected' : ''}>${item.descricaoCompleta}</option>--}%
                %{--</g:each>--}%
            %{--</select>--}%
        %{--</div>--}%
        %{--<br class="fld">--}%

        %{--<div class="form-group">--}%
            %{--<label for="" class="control-label">Aceita Critério?</label>--}%
            %{--<br>--}%
            %{--<select id="stCriterioAvalIucnOk" name="stCriterioAvalIucnOk" data-change="avaliacao.stCriterioAvalIucnOkChange" class="fld form-control fld200 fldSelect">--}%
                %{--<option value="">-- selecione --</option>--}%
                %{--<g:each var="item" in="${oSN}">--}%
                    %{--<option value="${item.key}" ${ficha?.stCriterioAvalIucnOk == item.key ? ' selected' : ''}>${item.value}</option>--}%
                %{--</g:each>--}%
            %{--</select>--}%
        %{--</div>--}%

        %{-- se o campo anterior for Não --}%
        %{--<div class="form-group hide">--}%
            %{--<label for="" class="control-label">Critério Sugerido</label>--}%
            %{--<br>--}%
            %{--<input name="dsCriterioAvalIucnSugerido" id="dsCriterioAvalIucnSugerido" onkeyup="ficha.fldCriterioAvaliacaoKeyUp(this);" readonly type="text" class="fld form-control fld400 blue" value="${ficha.dsCriterioAvalIucnSugerido}"/>--}%
            %{--<button class="fld btn btn-default btn-sm" title="Clique para Selecionar o Critério de Avaliação" data-action="ficha.openModalSelCriterioAvaliacao" data-field="dsCriterioAvalIucnSugerido" data-params="sqFicha" id="btnSelCriterioAvaliacao">...</button>--}%
        %{--</div>--}%
    %{--</div>--}%

    %{--<div class="col-sm-6">--}%
        %{--<div class="form-group w100p">--}%
            %{--<label for="" class="control-label">Justificativa</label>--}%
            %{--<br>--}%
            %{--<textarea id="dsJustificativaResultadoX" rows="4" class="fld form-control fldTextarea wysiwygRO w100p">${ficha.dsJustificativa}</textarea>--}%
        %{--</div>--}%
    %{--</div>--}%

    %{--<div class="col-sm-6">--}%
        %{--<div class="form-group w100p">--}%
            %{--<label for="" class="control-label">Comentário</label>--}%
            %{--<br>--}%
            %{--<textarea name="dsJustificativaValidacaoNova" rows="4" class="fld form-control fldTextarea wysiwyg w100p"></textarea>--}%
        %{--</div>--}%

        %{--<div class="fld panel-footer">--}%
            %{--<button data-action="avaliacao.saveFrmValidacao" data-params="sqFicha" class="fld btn btn-success">Gravar</button>--}%
        %{--</div>--}%
    %{--</div>--}%
%{--</form>--}%

%{--<div class="col-sm-12 hidden">--}%
    %{--<div class="form-group w100p">--}%
        %{--<label for="" class="control-label">Histórico Comentários</label>--}%
        %{--<br>--}%
        %{--<textarea id="dsJustificativaValidacaoX" rows="7" readonly="true" class="fld form-control fldTextarea wysiwygRO w100p">${ficha.dsJustificativaValidacao}</textarea>--}%
    %{--</div>--}%
%{--</div>--}%
