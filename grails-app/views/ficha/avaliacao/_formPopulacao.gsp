<g:set var="oSND"  value="${[ [key:'S',value:'Sim'], [key:'N', value:'Não'] ,[key:'D', value:'Desconhecido'] ]}"/>
<form id="frmPopulacaoAba112" role="form" class="form-inline">

    <div class="col-sm-12" style="background-color: transparent;">
         <g:if test="${ficha.vlTempoGeracional}">
                %{--  TEMPO GERACIONAL--}%
                <div class="form-group">
                    <label class="control-label">Tempo geracional</label>
                    <br>
                    <span class="text-info">${ficha?.vlTempoGeracional?.toInteger()?.toString()}</span>&nbsp;
                    <span class="text-info">${ficha?.medidaTempoGeracional?.descricao}</span>
                    <input type="hidden" id="vlTempoGeracional" value="${ficha?.vlTempoGeracional?.toInteger()?.toString()}">
                    <input type="hidden" id="sqMedidaTempoGeracional" value="${ficha?.medidaTempoGeracional?.id}" data-codigo="${ficha?.medidaTempoGeracional?.codigo}">
                    %{--<select id="sqMedidaTempoGeracional" style="display: none">
                        <option data-codigo="${ficha?.medidaTempoGeracional?.codigo}" value="${ficha?.medidaTempoGeracional?.id}" selected>-</option>
                    </select>--}%
                </div>

                <div class="form-group">
                    <label>&nbsp;</label>
                    <br>
                    <span><i class="fa fa-arrow-right"></i></span>
                </div>
                %{--      GRAFICO--}%
                <div class="form-group">
                    <label>Gráfico</label>
                    <div id="graficoWrapper" style="position:relative;width:400px;height:55px;border:1px solid black;background-color:#efefef;-webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;">
                        %{-- barra central --}%
                        <div id="grafico" style="position:absolute;top:15px;left:10px;border-top:2.5px solid gray;width:380px;height:5px;background-color: #aed8ae;">
                            %{-- menos anos --}%
                            <div style="position:absolute;top:-18px;left:70px;border:none;width:50px;height: 20px;font-weight: bold;font-size:10px;text-align: center" id="divMenosAnos"></div>
                            %{-- mais anos --}%
                            <div style="position:absolute;top:-18px;left:250px;border:none;width:50px;height: 20px;font-weight: bold;font-size:10px;text-align: center" id="divMaisAnos"></div>
                            %{-- marca inicial --}%
                            <div style="position:absolute;top:-5px;left:20px;border-left:3px solid black;width:10px;height:10px;"></div>
                            %{-- marca central --}%
                            <div style="position:absolute;top:-5px;left:190px;border-left:3px solid black;width:10px;height:10px;"></div>
                            %{-- marca final --}%
                            <div style="position:absolute;top:-5px;left:360px;border-left:3px solid black;width:10px;height:10px;"></div>
                            %{-- ano inicial --}%
                            <div style="position:absolute;top:5px;left:3px;border:none;width:35px;height: 20px;font-weight: bold;font-size:14px;text-align:center;" id="divAnoInicial">?</div>
                            %{-- ano avaliacao --}%
                            <div style="position:absolute;top:5px;left:175px;border:none;width:35px;height: 20px;font-weight: bold;font-size:14px;" id="divAnoAvaliacao">${ new Date().format( 'yyyy' )}</div>
                            %{-- ano final --}%
                            <div style="position:absolute;top:5px;left:345px;border:none;width:35px;height: 20px;font-weight: bold;font-size:14px;" id="divAnoFinal">?</div>
                            %{-- texto central --}%
                            <div style="position:absolute;top:18px;left:0px;border:none;width:380px;height:10px;text-align: center;font-size:12px;">Ano da Avaliação</div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="form-group w100p">
                    <label class="control-label">Método de cálculo tempo geracional
                        <i data-action="ficha.openModalSelRefBibFicha" data-no-tabela="ficha" data-no-coluna="dsMetodCalcTempoGeracional" data-sq-registro="${ficha.id}" data-de-rotulo="Método de Cálculo Tempo Geracional" class="fa fa-book cursor-pointer ml10" title="Adicionar/Remover Refeferência bibliográfica." style="color: green;" />
                    </label>
                    <br>
                    <div class="text-justify w100p" style="padding:5px;max-height: 300px;overflow-y: auto;border:1px solid silver;">${raw(ficha.dsMetodCalcTempoGeracional)}</div>
                </div>

        </g:if>
        <g:else>
            <div class="form-group">
                <label>Tempo geracional não foi informado na aba 4-População.</label>
            </div>
        </g:else>
    </div>

    <div class="col-sm-12" style="background-color: transparent;">
    <fieldset>
      <legend class="legendLinha" style="height: 10px;">&nbsp;</legend>
      <div class="form-group">
         <label for="" class="control-label">Redução populacional passada com causas<br>reversíveis, compreendidas ou cessadas</label>
         <br>
         <div class="input-group">
            <input name="nuReducaoPopulPassadaRev" type="text" class="fld form-control number fld270" maxlength="3" ${params.canModify?'':'disabled'} value="${ficha.nuReducaoPopulPassadaRev}"/>
            <span class="input-group-addon">%</span>
         </div>
      </div>
      <div class="form-group">
         <label for="" class="control-label"> &nbsp;<br>Tipo de dados</label>
         <br>
         <select name="sqTipoReduPopuPassRev" class="fld form-control fld200 fldSelect" ${params.canModify?'':'disabled'}>
            <option value="">-- selecione --</option>
            <g:each var="item" in="${listTipoReducao}">
              <option value="${item.id}" ${ficha?.tipoReduPopuPassRev?.id==item.id?' selected':''}>${item.descricao}</option>
            </g:each>
         </select>
      </div>
      <br/>
      <div class="form-group">
         <label for="" class="control-label">Redução populacional passada com causas não<br> reversíveis, não compreendidas ou não cessadas</label>
         <br>
         <div class="input-group">
            <input name="nuReducaoPopulPassada" type="text" class="fld form-control number fld270" maxlength="3" ${params.canModify?'':'disabled'} value="${ficha.nuReducaoPopulPassada}"/>
            <span class="input-group-addon">%</span>
         </div>
      </div>
      <div class="form-group">
         <label for="" class="control-label">&nbsp;<br>Tipo de dados</label>
         <br>
         <select name="sqTipoReduPopuPass" class="fld form-control fld200 fldSelect" ${params.canModify?'':'disabled'}>
            <option value="">-- selecione --</option>
            <g:each var="item" in="${listTipoReducao}">
              <option value="${item.id}" ${ficha?.tipoReduPopuPass?.id==item.id?' selected':''}>${item.descricao}</option>
            </g:each>
         </select>
      </div>

      <br class="fld"/>

      <div class="form-group">
         <label for="" class="control-label">Redução populacional futura</label>
         <br>
         <div class="input-group">
            <input name="nuReducaoPopulFutura" type="text" class="fld form-control number fld270" maxlength="3" ${params.canModify?'':'disabled'} value="${ficha.nuReducaoPopulFutura}"/>
            <span class="input-group-addon">%</span>
         </div>
      </div>
      <div class="form-group">
         <label for="" class="control-label">Tipo de dados</label>
         <br>
         <select name="sqTipoReduPopuFutura" class="fld form-control fld200 fldSelect" ${params.canModify?'':'disabled'}>
            <option value="">-- selecione --</option>
            <g:each var="item" in="${listTipoReducao}">
              <option value="${item.id}" ${ficha?.tipoReduPopuFutura?.id==item.id?' selected':''}>${item.descricao}</option>
            </g:each>
         </select>
      </div>
      <br />
      <div class="form-group">
         <label for="" class="control-label">Redução populacional passada e futura</label>
         <br>
         <div class="input-group">
            <input name="nuReducaoPopulPassFutura" type="text" class="fld form-control number fld270" maxlength="3" ${params.canModify?'':'disabled'} value="${ficha.nuReducaoPopulPassFutura}" />
            <span class="input-group-addon">%</span>
         </div>
      </div>
      <div class="form-group">
         <label for="" class="control-label">Tipo de dados</label>
         <br>
         <select name="sqTipoReduPopuPassFutura" class="fld form-control fld200 fldSelect" ${params.canModify?'':'disabled'}>
            <option value="">-- selecione --</option>
            <g:each var="item" in="${listTipoReducao}">
               <option value="${item.id}" ${ficha?.tipoReduPopuPassFutura?.id==item.id?' selected':''}>${item.descricao}</option>
            </g:each>
         </select>
      </div>

      <br />

      <div class="form-group">
         <label for="" class="control-label">Declínio populacional baseado em</label>
         <br>
         <g:if test="${params.canModify}">
             <select id="sqDeclinioPopulacional" class="fld form-control fld800 fldSelect">
                <option value="">-- selecione --</option>
                <g:each var="item" in="${listTipoDeclinio}">
                  <option value="${item.id}" ${ficha?.declinioPopulacional?.id==item.id?' selected':''}>${item.descricao}</option>
                </g:each>
             </select>
             <button data-action="avaliacao.addDeclinioPopulacional" data-params="sqFicha,sqDeclinioPopulacional" class="fld btn btn-default" title="Selelcione uma opção e clique aqui para adicionar ao gride.">Adicionar</button>
         </g:if>
      </div>
      <div id="divGridDeclinioPopulacional" class="mt10" data-params="sqFicha" style="overflow-x: hidden; overflow-y: auto; max-width: 800px; max-height: 150px;"></div>
   </fieldset>

   <fieldset>
      <legend class="legendLinha" style="font-size:0 !important;">&nbsp;</legend>
      <div class="form-group">
         <label for="" class="control-label">% e período do declínio populacional</label>
         <br>
         <select name="sqPercDeclinioPopulacional" class="fld form-control fld300 fldSelect" ${params.canModify?'':'disabled'}>
            <option value="">-- Selecione --</option>
            <g:each var="item" in="${listPercentuais}">
               <option value="${item.id}" ${ficha?.percDeclinioPopulacional?.id==item.id ? ' selected' :'' }>${item.descricao}</option>
            </g:each>
         </select>
      </div>
   </fieldset>


   <fieldset>
      <legend>&nbsp;</legend>
      <div class="form-group">
         <label for="" class="control-label">População severamente fragmentada</label>
         <br>
         <select name="stPopulacaoFragmentada" class="fld form-control fld250 fldSelect" ${params.canModify?'':'disabled'}>
            <option value="">-- selecione --</option>
            <g:each var="item" in="${oSND}">
               <option value="${item.key}" ${ficha?.stPopulacaoFragmentada==item.key ? ' selected' :'' }>${item.value}</option>
            </g:each>
         </select>
      </div>
   </fieldset>


   <fieldset>
      <legend>&nbsp;</legend>
      <div class="form-group">
         <label for="" class="control-label">Nº de indivíduos maduros</label>
         <br>
         <div class="input-group">
            <input name="nuIndividuoMaduro" type="text" class="fld form-control number fld200" maxlength="5" ${params.canModify?'':'disabled'} value="${ficha.nuIndividuoMaduro}" />
         </div>
      </div>

      <div class="form-group">
         <label for="" class="control-label">Tendência populacional do nº de indivíduos maduros</label>
         <br>
         <select name="sqTendenNumIndividuoMaduro" class="fld form-control fld350 fldSelect" ${params.canModify?'':'disabled'}>
            <option value="">-- selecione --</option>
            <g:each var="item" in="${listTendencia}">
               <option value="${item.id}" ${ficha?.tendenNumIndividuoMaduro?.id==item.id ? 'selected':''} >${item.descricao}</option>
            </g:each>
         </select>
      </div>

      <div class="form-group">
         <label for="" class="control-label">Flutuação extrema do nº de indivíduos maduros</label>
         <br>
         <select name="stFlutExtremaIndivMaduro" class="fld form-control fld350 fldSelect" ${params.canModify?'':'disabled'}>
            <option value="">-- Selecione --</option>
            <g:each var="item" in="${oSND}">
               <option value="${item.key}" ${ficha?.stFlutExtremaIndivMaduro==item.key ? ' selected' :'' }>${item.value}</option>
            </g:each>
         </select>
      </div>

      <br class="fld"/>

      <div class="form-group">
         <label for="" class="control-label">Nº de subpopulação</label>
         <br>
         <input name="nuSubpopulacao" type="text" class="fld form-control number fld200" maxlength="5" ${params.canModify?'':'disabled'} value="${ficha.nuSubpopulacao}" />
      </div>

      <div class="form-group">
         <label for="" class="control-label">Tendência do nº de subpopulação</label>
         <br>
         <select name="sqTendenciaNumSubpopulacao" class="fld form-control fld350 fldSelect" ${params.canModify?'':'disabled'}>
            <option value="">-- selecione --</option>
            <g:each var="item" in="${listTendencia}">
               <option value="${item.id}" ${ficha?.tendenciaNumSubpopulacao?.id==item.id ? 'selected':''} >${item.descricao}</option>
            </g:each>
         </select>
      </div>

      <div class="form-group">
         <label for="" class="control-label">Flutuação extrema do nº de subpopulação</label>
         <br>
         <select name="stFlutExtremaSubPopulacao" class="fld form-control fld350 fldSelect" ${params.canModify?'':'disabled'}>
            <option value="">-- selecione --</option>
            <g:each var="item" in="${oSND}">
               <option value="${item.key}" ${ficha?.stFlutExtremaSubPopulacao==item.key ? ' selected' :'' }>${item.value}</option>
            </g:each>
         </select>
      </div>
   </fieldset>


   <fieldset>
      <legend>&nbsp;</legend>
      <div class="form-group">
         <label for="" class="control-label">Nº max de indivíduos em cada subpopulação</label>
         <br>
         <input name="nuIndiviSubpopulacaoMax" type="text" class="fld form-control number fld300" maxlength="6" ${params.canModify?'':'disabled'} value="${ficha.nuIndiviSubpopulacaoMax}" />
      </div>

      <div class="form-group">
         <label for="" class="control-label">% de indivíduos em cada subpopulação</label>
         <br>
         <div class="input-group">
            <input name="nuIndiviSubpopulacaoPerc" type="text" class="fld form-control number fld250" maxlength="3" ${params.canModify?'':'disabled'} value="${ficha.nuIndiviSubpopulacaoPerc}" />
            <span class="input-group-addon">%</span>
         </div>
      </div>
   </fieldset>

   %{-- botão gravar --}%
   <g:if test="${params.canModify}">
       <div class="fld panel-footer">
          <button data-action="avaliacao.saveFrmPopulacao" data-params="sqFicha" class="fld btn btn-success">Gravar População</button>
       </div>
   </g:if>
  </div>
</form>
