<g:if test="${!session.sicae.user.isCO()}">
   <div class="alert alert-info" style="margin:5px;">
      As informações desta aba são alimentadas na aba 11.6-Resultado da Avaliação.
   </div>
</g:if>
<div class="form form-inline">
   <div class="form-group">
      <label class="control-label">Conectividade com populações de outros países</label>
      <br>
      <input type="text" class="form-control fld300" value="${ficha?.tipoConectividade?.descricao}" readonly="readonly">
   </div>
   <div class="form-group">
      <label class="control-label">Tendência de imigração no Brasil</label>
      <br>
      <input type="text" class="form-control fld300" value="${ficha?.tendenciaImigracao?.descricao}" readonly="readonly">
   </div>
      <div class="form-group">
         <label for="" class="control-label">População brasileira pode declinar</label>
         <br>
         <input type="text" class="form-control fld300" value="${ficha.snd( ficha?.stDeclinioBrPopulExterior ) }" readonly="readonly">
      </div>
      <div class="form-group w100p">
         <label for="" class="control-label">Observações</label>
         <br>
         <div class="div-justificativa text-justify" style="padding:5px;min-height:150px;border:1px solid silver;" >${raw( ficha.dsConectividadePopExterior )}</div>
      </div>
</div>
