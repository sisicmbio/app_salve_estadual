<g:set var="oSND"  value="${[ [key:'S',value:'Sim'], [key:'N', value:'Não'] ,[key:'D', value:'Desconhecido'] ]}"/>
<form id="frmAvaDistribuicao" role="form" class="form-inline">

   <div class="form-group">
      <label for="" class="control-label">Ocorrência Marginal no Brasil</label>
      <br>
      <select name="stOcorrenciaMarginal" ${params.canModify ? '':'disabled'} class="fld form-control fld200 fldSelect">
         <option value="">-- Selecione --</option>
         <g:each var="item" in="${oSND}">
            <option value="${item.key}" ${ficha?.stOcorrenciaMarginal==item.key ? ' selected' :'' }>${item.value}</option>
         </g:each>
      </select>
   </div>

   <div class="form-group">
      <label for="" class="control-label">Espécie Elegível para Avaliação Nacional</label>
      <br>
      <select name="stElegivelAvaliacao" ${params.canModify ? '':'disabled'} class="fld form-control fld300 fldSelect">
         <option value="">-- Selecione --</option>
         <g:each var="item" in="${oSND}">
            <option value="${item.key}" ${ficha?.stElegivelAvaliacao==item.key ? ' selected' :'' }>${item.value}</option>
         </g:each>
      </select>
   </div>

   <div class="form-group">
      <label for="" class="control-label">Limitação Taxonômica para Avaliação</label>
      <br>
      <select name="stLimitacaoTaxonomicaAval" ${params.canModify ? '':'disabled'} class="fld form-control fld300 fldSelect">
         <option value="">-- Selecione --</option>
         <g:each var="item" in="${oSND}">
            <option value="${item.key}" ${ficha?.stLimitacaoTaxonomicaAval==item.key ? ' selected' :'' }>${item.value}</option>
         </g:each>
      </select>
   </div>
   %{-- botão gravar
   <div class="fld panel-footer">
      <button data-action="avaliacao.saveFrmAvaliacao" data-params="sqFicha" class="fld btn btn-success">Gravar</button>
   </div>
   --}%
   <br/>
   <div class="form-group">
      <label for="" class="control-label">Conhecida Apenas da Localidade Tipo ou Poucas Localidades</label>
      <br>
      <select name="stLocalidadeTipoConhecida" ${params.canModify ? '':'disabled'} class="fld form-control fld400 fldSelect">
         <option value="">-- Selecione --</option>
         <g:each var="item" in="${oSND}">
            <option value="${item.key}" ${ficha?.stLocalidadeTipoConhecida==item.key ? ' selected' :'' }>${item.value}</option>
         </g:each>
      </select>
   </div>


   <div class="form-group">
      <label for="" class="control-label">Região de Ocorrência Bem Amostrada</label>
      <br>
      <select name="stRegiaoBemAmostrada" ${params.canModify ? '':'disabled'} class="fld form-control fld300 fldSelect">
         <option value="">-- Selecione --</option>
         <g:each var="item" in="${oSND}">
            <option value="${item.key}" ${ficha?.stRegiaoBemAmostrada==item.key ? ' selected' :'' }>${item.value}</option>
         </g:each>
      </select>
   </div>
   <br />

   <div class="form-group">
      <label for="" class="control-label">Extensão da Ocorrência (EOO)</label>
      <br>
      <div class="input-group">
         <input name="vlEoo"type="text" ${params.canModify ? '':'disabled'}
                class="fld form-control number1d fld200" maxlength="13" value="${ficha.vlEoo}"/>
          <span class="input-group-addon">Km&sup2;</span>
      </div>
   </div>

    <g:if test="${hydroshedLevel}">
        <div class="form-group">
            <label for="" class="control-label">&nbsp;</label>
            <br>
            <input type="text" class="form-control disabled fld200" readonly value="${hydroshedLevel}">

            %{--<select name="nuHydroshedLevel" class="fld form-control fld200 fldSelect" ${params.canModify ? '':'disabled'}>
                <option value=""   ${!fichaEoo.nuHydroshedLevel ? ' selected':''}>Registros de ocorrência</option>
                <option value="8"  ${(fichaEoo?.nuHydroshedLevel?.toInteger()==8)?' selected':''}>Hydrosheds nivel 8</option>
                <option value="9"  ${(fichaEoo?.nuHydroshedLevel?.toInteger()==9)?' selected':''}>Hydrosheds nivel 9</option>
                <option value="10" ${(fichaEoo?.nuHydroshedLevel?.toInteger()==10)?' selected':''}>Hydrosheds nivel 10</option>
            </select>--}%
        </div>
   </g:if>


   <div class="form-group">
      <label for="" class="control-label">Tendência da EOO</label>
      <br>
      <select name="sqTendenciaEoo" class="fld form-control fld150 fldSelect" ${params.canModify ? '':'disabled'}>
         <option value="">-- Selecione --</option>
         <g:each var="item" in="${listTendencia}">
            <option value="${item.id}" ${(ficha?.tendenciaEoo?.id==item.id)?' selected':''}>${item.descricao}</option>
         </g:each>
      </select>
   </div>

   <div class="form-group">
      <label for="" class="control-label">Flutuação Extrema na EOO</label>
      <br>
      <select name="stFlutuacaoEoo" class="fld form-control fld270 fldSelect" ${params.canModify ? '':'disabled'}>
         <option value="">-- Selecione --</option>
         <g:each var="item" in="${oSND}">
            <option value="${item.key}" ${ficha?.stFlutuacaoEoo==item.key ? ' selected' :'' }>${item.value}</option>
         </g:each>
      </select>
   </div>

   <br />
   <div class="form-group">
      <label for="" class="control-label">Área de Ocupação (AOO)</label>
      <br>
      <div class="input-group">
         <input name="vlAoo" type="text" class="fld form-control number1d fld200" ${params.canModify ? '':'disabled'}
                maxlength="13" value="${ficha.vlAoo}"/>
         <span class="input-group-addon">km&sup2;</span>
      </div>
   </div>

   <div class="form-group">
      <label for="" class="control-label">Tendência da AOO</label>
      <br>
      <select name="sqTendenciaAoo" class="fld form-control fld150 fldSelect" ${params.canModify ? '':'disabled'}>
         <option value="">-- selecione --</option>
         <g:each var="item" in="${listTendencia}">
         <option value="${item.id}" ${(ficha?.tendenciaAoo?.id==item.id)?' selected':''}>${item.descricao}</option>
         </g:each>
      </select>
   </div>

   <div class="form-group">
      <label for="" class="control-label">Flutuação Extrema na AOO</label>
      <br>
      <select name="stFlutuacaoAoo" class="fld form-control fld300 fldSelect" ${params.canModify ? '':'disabled'}>
         <option value="">-- Selecione --</option>
         <g:each var="item" in="${oSND}">
            <option value="${item.key}" ${ficha?.stFlutuacaoAoo==item.key ? ' selected' :'' }>${item.value}</option>
         </g:each>
      </select>
   </div>
   <br>
   <div class="form-group w100p">
   <label for="dsJustificativaAooEoo" class="control-label">Explicação (EOO/AOO)
      <g:if test="${ params.canModify }">
        <i data-action="ficha.openModalSelRefBibFicha" data-no-tabela="ficha" data-no-coluna="dsJustificativaAooEoo" data-sq-registro="${ficha.id}" data-de-rotulo="Explicação ( EOO/ AOO )" class="fa fa-book cursor-pointer ml10" title="Adicionar/Remover Refeferência bibliográfica." style="color: green;" />
      </g:if>
   </label>
      <br>
      <g:if test="${ params.canModify }">
        <textarea name="dsJustificativaAooEoo" id="dsJustificativaAooEoo" rows="5" class="fld form-control fldTextarea wysiwyg w100p">${ficha.dsJustificativaAooEoo}</textarea>
      </g:if>
      <g:else>
         <div class="div-justificativa">${raw(ficha.dsJustificativaAooEoo)}</div>
      </g:else>
   </div>

   %{-- botão gravar --}%
   <g:if test="${ params.canModify }">
      <div class="fld panel-footer">
         <button data-action="avaliacao.saveFrmDistribuicao" data-params="sqFicha" class="fld btn btn-success">Gravar Distribuição</button>
      </div>
   </g:if>
</form>
