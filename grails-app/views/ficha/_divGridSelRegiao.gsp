<table class="table table-striped table-bordered table-hover">
   <thead>
      <tr>
         <th>Abrangencia</th>
         <th>Ragião</th>
         <th>Observações</th>
         <th>Ação</th>
      </tr>
   </thead>
   <tbody>
   <g:each var="item" in="${listRegioes}">

      <tr>
         <td>${item?.fichaOcorrencia?.tipoAbrangencia?.descricao}</td>
         <td>${item?.fichaOcorrencia?.abrangencia?.descricao}</td>
         <td>${item?.txLocal}</td>

         <td class="td-actions">
            <a id="btnEditRegionalizacao" data-action="selRegiao.editRegionalizacao" data-id="${item.id}" class="fld btn btn-default btn-xs btn-update" title="Alterar">
               <span class="glyphicon glyphicon-pencil"></span>
            </a>
            <a id="btnDeleteRegionalizacao" data-action="selRegiao.deleteRegionalizacao" data-descricao="${item?.fichaOcorrencia?.tipoAbrangencia?.descricao+' - '+item.noRegiao}" data-id="${item.id}" class="fld btn btn-default btn-xs btn-delete" title="Excluir">
               <span class="glyphicon glyphicon-remove"></span>
            </a>
         </td>
      </tr>
   </g:each>
   </tbody>
</table>