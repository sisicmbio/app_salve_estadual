<g:if test="${ ! session.sicae.user.isCO() }">
	<form id="frmPopulEstimada" class="form-inline mt20" role="form">
		<input type="hidden" name="sqFichaPopulEstimadaLocal" value=""/>

		%{-- ABRANGENCIA --}%
		<div class="form-group">
			 <label class="control-label">Abrangência</label>
			 <br>
			 <select id="sqTipoAbrangencia" name="sqTipoAbrangencia" data-contexto="frmPopulEstimada" data-change="tipoAbrangenciaChange" class="fld form-control fld200">
				<option data-codigo="" value="">?</option>
				<g:each var="item" in="${listTipoAbrangencia}">
					<option data-codigo="${item.codigo}" value="${item.id}">${item.descricao}</option>
				</g:each>
			 </select>
		</div>
		<div class="form-group">
			<label class="control-label">&nbsp;</label>
			<br>
			<span>-</span>
			 <select id="sqAbrangencia"
						name="sqAbrangencia"
						class="fld form-control select2 fld600"
						disabled="true"
						data-s2-params="sqTipoAbrangencia"
						data-s2-url="ficha/getAbrangencias"
						data-s2-minimum-input-length="1"
						required="true">
				<option value="">?</option>
			 </select>
			 %{--</select><select id="sqAbrangencia"
						name="sqAbrangencia"
						class="fld form-control select2 fld600"
						disabled="true"
						data-s2-minimum-input-length="0" required="true">
				<option value="">?</option>
			 </select>
			 --}%
		</div>
		<br/>
		<div class="fld form-group">
			<label for="" class="control-label">Localidade</label>
			<br>
			<input type="text" name="dsLocal" class="fld form-control fld400"/>
			%{-- <textarea name="dsLocal" rows="7" class="fld fldTextarea wysiwyg fld400"></textarea> --}%
		</div>
		<br/>
		<div class="fld form-group">
			<label class="control-label label-required">Período Início</label>
			<br>
			<input name="dsMesAnoInicio" class="fld form-control fld100" type="text" data-mask="00/0000" placeholder="MM/AAAA" required/>
		</div>

		<div class="fld form-group">
			<label class="control-label label-required">Período Fim</label>
			<br>
			<input name="dsMesAnoFim" class="fld form-control fld100" type="text" data-mask="00/0000" placeholder="MM/AAAA" required/>
		</div>

		<div class="fld form-group">
			<label class="control-label">Abundância</label>
			<br>
			<input name="deValorAbundancia" class="fld form-control fld200" type="text" />
		</div>
		<div class="fld form-group">
			<label for="" class="control-label">Unidade</label>
			<br>
			<select name="sqUnidAbundanciaPopulacao" class="fld form-control fld200 fldSelect">
				<option value="">-- selecione --</option>
				<g:each var="item" in="${listUnidadeAbundancia}">
					<option value="${item.id}">${item.descricao}</option>
				</g:each>
			</select>
		</div>
		<br>
		<div class="fld form-group w100p">
			<label for="dsEstimativaPopulacao" class="control-label">Observações</label>
			<br>
			<textarea name="dsEstimativaPopulacao" rows="4" class="fld fldTextarea wysiwyg w100p"></textarea>
		</div>

		<div class="fld panel-footer">
			<button id="btnSaveFrmPopulEstimada" data-action="populacao.saveFrmPopulEstimada" data-params="sqFicha" class="fld btn btn-success">Adicionar População</button>
			<button id="btnResetFrmPopulEstimada" data-action="populacao.resetFrmPopulEstimada" class="fld btn btn-info hide">Limpar Formulário</button>
		</div>
	</form>
</g:if>
<div id="divGridPopulEstimada" class="mt20">
	%{-- <g:render template="/ficha/populacao/divGridPopEstimadaLocal"></g:render> --}%
</div>
