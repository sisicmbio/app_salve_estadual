<!-- view: /views/ficha/populacao/_divGridAreaVida.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<table class="table table-striped table-bordered table-hover">
   <thead>
      <tr>
         <th style="width:200px">Localidade</th>
         <th style="width:90px">Área</th>
         <th style="width:80px">Ano</th>
         <th style="width:auto">Observação</th>
         <th style="width:200px">Ref. Bibliográfica</th>
         <g:if test="${ ! session.sicae.user.isCO() }">
            <th style="width:80px">Ação</th>
         </g:if>
      </tr>
   </thead>
   <tbody>
   <g:each var="item" in="${listFichaPopAreaVida}">
      <tr>
         <td>${item.txLocal}</td>
         <td>${item.vlArea +' '+ item.unidadeArea.descricao}</td>
         <td class="text-center">${item.nuAno}</td>
         <td>${raw( item.txArea )}</td>
         <td>
            <a data-action="ficha.openModalSelRefBibFicha"
               data-no-tabela="ficha_area_vida"
               data-no-coluna="sq_ficha_area_vida"
               data-sq-registro="${item.id}"
               data-de-rotulo="Área de vida - ${item.txLocal}"
               data-com-pessoal="true"
               data-update-grid="divGridPopAreaVida"
               class="fld btn btn-default btn-xs btn-grid-subform" title="Adicionar/Remover Refeferência bibliográfica."><span class="fa fa-book"></span></a>
               &nbsp;
               ${raw(item.refBibHtml)}
         </td>
         <g:if test="${ ! session.sicae.user.isCO() }">
            <td class="td-actions">
               <a data-action="populacao.editPopAreaVida"  data-id="${item.id}" class="fld btn btn-default btn-xs btn-update" title="Alterar">
                  <span class="glyphicon glyphicon-pencil"></span>
               </a>
               <a data-action="populacao.deletePopAreaVida" data-descricao="" data-id="${item.id}" class="fld btn btn-default btn-xs btn-delete" title="Excluir">
                  <span class="glyphicon glyphicon-remove"></span>
               </a>
           </td>
         </g:if>
      </tr>
   </g:each>
   </tbody>
</table>
