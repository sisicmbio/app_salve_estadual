<table class="table table-striped table-bordered table-hover">
  <thead>
    <tr>
      <th>Imagem/PDF</th>
      <th>Legenda</th>
      <th>Arquivo</th>
      %{-- <th>Principal?</th> --}%
      <g:if test="${ ! session.sicae.user.isCO() }">
        <th>Ação</th>
      </g:if>
      %{-- <th></th> --}%
    </tr>
  </thead>
  <tbody>
    <g:each var="item" in="${listFichaAnexo}">
      <tr>
        <td class="text-center" style="width:160px;">
        
        <g:if test="${ item.noArquivo.toLowerCase().lastIndexOf('.pdf') > 0 }">
          <a class="fld btn btn-default btn-xs btn-download" target="_blank" title="Baixar PDF!" href="/salve-estadual/ficha/getAnexo/${item.id}">
            <i class="red fa fa-file-pdf-o"></i>
          </a>        
        </g:if>  
        <g:else>
            <img src="/salve-estadual/ficha/getAnexo/${item.id}" data-id="${item.id}" alt="${item.deLegenda}" class="img-rounded img-preview-grid"/>
        </g:else>              
      
        </td>
        <td style="width:*">${item.deLegenda}</td>
        <td style="width:200px;">${item.noArquivo}</td>
          %{-- <td style="width:80px;" class="text-center">${item.inPrincipal=='S'?'Sim':'Não'}</td> --}%
        <g:if test="${ ! session.sicae.user.isCO() }">
          <td style="width:30px;" class="td-actions">
              <a class="fld btn btn-default btn-xs btn-download" target="_blank" title="Baixar arquivo!" href="/salve-estadual/ficha/getAnexo/${item.id}">
                  <span class="glyphicon glyphicon-download"></span>
              </a>

              <a data-action="ficha.deleteAnexo" data-descricao="${item.deLegenda}" data-id="${item.id}" data-update-grid="divGridPopulGraficos"  class="fld btn btn-default btn-xs btn-delete" title="Excluir">
              <span class="glyphicon glyphicon-remove"></span>
            </a>
          </td>
        </g:if>
        %{-- <td style="width:*"></td> --}%
      </tr>
    </g:each>
  </tbody>
</table>
