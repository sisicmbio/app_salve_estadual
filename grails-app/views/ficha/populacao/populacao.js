//# sourceURL=N:\SAE\sae\grails-app\views\ficha\populacao\populacao.js
;
populacao = {
	tabFichaPopulacaoInit: function(params) {
		populacao.updateGridPopulGraficos();
        var $form=$("#frmPopulacaoAba4");
        if( $form.size() == 1 ) {
            var $select = $form.find("#sqMedidaTempoGeracional");
            if( $select.size() == 1) {
                if (! $select.val()) {
                    $select.find("option").each(function () {
                        if (/^Ano/.test($(this).text())) {
                            $select.val(this.value);
                        }
                    });
                }
                var $input = $form.find("input#vlTempoGeracional");
                avaliacao.updateGrafico('frmPopulacaoAba4', 'vlTempoGeracional', 'sqMedidaTempoGeracional');
                //app.focus('vlTempoGeracional', 'frmPopulacaoAba4');
                if ($input.size() == 1) {
                    $input.on('keyup', function() {
                            avaliacao.updateGrafico('frmPopulacaoAba4', 'vlTempoGeracional', 'sqMedidaTempoGeracional')
                    });
                }
            }
        }
	},
	saveFrmPopulacao: function(params) {
		if (!$('#frmPopulacaoAba4').valid()) {
			return false;
		}
		var data = $("#frmPopulacaoAba4").serializeArray();
		data = app.params2data(params, data);
		if (!data.sqFicha) {
			app.alertError('ID da ficha não encontrado!');
			return;
		}
		/*
		if( data.dsMetodCalcTempoGeracional && ( ! data.vlTempoGeracional || ! data.sqMedidaTempoGeracional) ){
            app.alertError('Para informar o método de cálculo é necessário informar o tempo geracional e a unidade!');
            return;
        }*/

		app.ajax(app.url + 'fichaPopulacao/saveFrmPopulacao', data,
			function(res) {
				if (res.status == 0) {
					app.resetChanges('frmPopulacaoAba4')
				}
			}, null, 'json');
	},

    // exibir o gride com as imagens dos mapas/graficos
	updateGridPopulGraficos: function(params) {
		ficha.getGridAnexos('divGridPopulGraficos');
	},
	//-------------------------------------------------------------------
	// Aba População Conhecida / Estimada
	// ------------------------------------------------------------------
	tabPopulEstimadaInit: function(data) {
		populacao.updateGridPopulEstimada();
	},
	// utilizando função global da fichaCompleta.js
	/*tipoAbrangenciaChange: function(params) {
		var contexto = "#" + params.contexto + " ";
		var selectPai = $(contexto + ' #sqTipoAbrangencia')
		var selectFilho = $(contexto + ' #sqAbrangencia')
		selectFilho.prop('disabled', true);
		// limpar o select
		selectFilho.find('option').each(function(k, v) {
			if (this.value) {
				$(this).remove();
			}
		});
		if (!selectPai.val()) {
			return;
		}
		var coTipoAbrangencia = $(selectPai).find('option:selected').data('codigo');
		var sqTipoAbrangencia = $(selectPai).val();
		var data = {
			contexto:contexto,
			sqTipoAbrangencia: sqTipoAbrangencia,
			coTipoAbrangencia: coTipoAbrangencia
		};
		app.ajax(app.url + 'ficha/getAbrangencias', data, function(res) {
			var tmpVal = $(contexto + " #sqAbrangencia").data('tmpVal');
			$.each(res, function(k, v) {
				selectFilho.append('<option '+( v.codigoSistema?'data-codigo="'+v.codigoSistema+'"':'') +' value="' + v.id + '"' + (v.id == tmpVal ? ' selected' : '') + '>' + v.descricao + '</option>');
			});
			selectFilho.prop('disabled', (data.codigo == ''));
		}, '', 'JSON')
	},
	*/
	saveFrmPopulEstimada: function(params) {
		if (!$('#frmPopulEstimada').valid()) {
			return false;
		}
		var data = $("#frmPopulEstimada").serializeArray();
		if (!data) {
			return;
		}
		data = app.params2data(params, data);
		if (!data.sqFicha) {
			app.alertError('ID da ficha não encontrado!');
			return;
		}
		//log( data )
		//return;
		app.ajax(app.url + 'fichaPopulacao/saveFrmPopulEstimada', data,
			function(res) {
				if (res.status == 0) {
					populacao.resetFrmPopulEstimada();
					populacao.updateGridPopulEstimada();
				}
			}, null, 'json'
		);
	},
	updateGridPopulEstimada: function() {
		ficha.getGrid('populEstimada');
	},
	deletePopulEstimada: function(params, btn) {
		app.selectGridRow(btn);
		app.confirm('<br>Confirma exclusão?',
			function() {
				app.ajax(app.url + 'fichaPopulacao/deletePopulEstimada',
					params,
					function(res) {
						populacao.updateGridPopulEstimada();
					});
			},
			function() {
				app.unselectGridRow(btn);
			}, params, 'Confirmação', 'warning');

	},
	editPopulEstimada: function(params, btn) {

		app.ajax(app.url + 'fichaPopulacao/editPopulEstimada', params,
			function(res) {
				$("#btnResetFrmPopulEstimada").removeClass('hide');
				$("#btnSaveFrmPopulEstimada")
					.data('value', $("#btnSaveFrmPopulEstimada").html());
				$("#btnSaveFrmPopulEstimada").html("Gravar Alteração");
				app.setFormFields(res, 'frmPopulEstimada');
				tipoAbrangenciaChange({
					contexto: 'frmPopulEstimada'
				});
				app.selectGridRow(btn);
			});
	},
	resetFrmPopulEstimada: function() {
		app.reset('frmPopulEstimada');
		$("#dsMesAnoInicio").focus();
		$("#btnResetFrmPopulEstimada").addClass('hide');
		$("#btnSaveFrmPopulEstimada").html($("#btnSaveFrmPopulEstimada").data('value'));
		app.unselectGridRow('divGridPopulEstimada');
	},


	//-------------------------------------------------------------------
	// Aba População Area de Vida
	// ------------------------------------------------------------------
	tabPopAreaVidaInit: function(params) {
		populacao.updateGridPopAreaVida();
	},

	saveFrmPopAreaVida: function(params) {
		if (!$('#frmPopAreaVida').valid()) {
			return false;
		}
		var data = $("#frmPopAreaVida").serializeArray();
		if (!data) {
			return;
		}
		data = app.params2data(params, data);
		if (!data.sqFicha) {
			app.alertError('ID da ficha não encontrado!');
			return;
		}
		app.ajax(app.url + 'fichaPopulacao/saveFrmPopAreaVida', data,
			function(res) {
				populacao.resetFrmPopAreaVida();
				populacao.updateGridPopAreaVida();
			}, null, 'json'
		);
	},

	editPopAreaVida: function(params, btn) {
		app.ajax(app.url + 'fichaPopulacao/editPopAreaVida', params,
			function(res) {
				$("#btnResetFrmPopAreaVida").removeClass('hide');
				$("#btnSaveFrmPopAreaVida")
					.data('value', $("#btnSaveFrmPopAreaVida").html());
				$("#btnSaveFrmPopAreaVida").html("Gravar Alteração");
				app.setFormFields(res, 'frmPopAreaVida');
				app.selectGridRow(btn);
			});
	},

	deletePopAreaVida: function(params, btn) {
		app.selectGridRow(btn);
		app.confirm('<br>Confirma exclusão?',
			function() {
				app.ajax(app.url + 'fichaPopulacao/deletePopAreaVida',
					params,
					function(res) {
						populacao.resetFrmPopAreaVida();
						populacao.updateGridPopAreaVida();
					});
			},
			function() {
				app.unselectGridRow(btn);
			}, params, 'Confirmação', 'warning');

	},



	updateGridPopAreaVida: function() {
		ficha.getGrid('popAreaVida');
	},

	resetFrmPopAreaVida: function(params) {
		app.reset('frmPopAreaVida');
		$("#txLocal").focus();
		$("#btnResetFrmPopAreaVida").addClass('hide');
		$("#btnSaveFrmPopAreaVida").html($("#btnSaveFrmAreaVida").data('value'));
		app.unselectGridRow('divGridPopAreaVida');
	},
}
