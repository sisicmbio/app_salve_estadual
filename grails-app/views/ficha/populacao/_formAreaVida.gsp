<form id="frmPopAreaVida" class="form-inline" role="form">
   <input type="hidden" name="sqFichaAreaVida" value="" />
   <div class="fld form-group">
      <label for="" class="control-label">Local</label>
      <br>
      <input type="text" name="txLocal" class="fld form-control fld400" />
   </div>
   <div class="fld form-group">
      <label class="control-label">Área</label>
      <br>
      <input name="vlArea" class="fld form-control fld200" type="text"/>
   </div>

   <div class="fld form-group">
      <label for="" class="control-label">Unidade</label>
      <br>
      <select name="sqUnidadeArea" class="fld form-control fld150 fldSelect">
         <option value="">-- selecione --</option>
         <g:each var="item" in="${listUnidadeArea}">
   			<option value="${item.id}">${item.descricao}</option>
         </g:each>
      </select>
   </div>

   <div class="fld form-group">
      <label class="control-label">Ano</label>
      <br>
      <input name="nuAno" id="nuAnos" class="fld form-control fld100" type="text" data-mask="0000" />
   </div>
   <br>

   <div class="fld form-group w100p">
      <label for="" class="control-label">Observações Sobre Área de Vida</label>
      <br>
      <textarea name="txArea" rows="7" class="fld fldTextarea wysiwyg"></textarea>
   </div>

   <div class="fld panel-footer">
      <button id="btnSaveFrmPopAreaVida" data-action="populacao.saveFrmPopAreaVida" data-params="sqFicha" class="fld btn btn-success">Adicionar Área de Vida</button>
      <button id="btnResetFrmPopAreaVida" data-action="populacao.resetFrmPopAreaVida" class="fld btn btn-info hide">Limpar Formulário</button>
   </div>

</form>
<div id="divGridPopAreaVida">
   %{-- <g:render template="/ficha/populacao/divGridPopAreaVida"></g:render> --}%
</div>
