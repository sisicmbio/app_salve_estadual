<!-- view: /views/ficha/populacao/_divGridPopulEstimada.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<table class="table table-striped table-bordered table-hover">
   <thead>
      <tr>
         <th>Abrangência</th>
         <th>Período</th>
         <th>Abundância</th>
         <th>Localidade</th>
         <th>Observações</th>
         <th style="width:200px">Referência Bibliográfica</th>
         <g:if test="${ ! session.sicae.user.isCO() }">
            <th>Ação</th>
         </g:if>
      </tr>
   </thead>
   <tbody>
   <g:each var="item" in="${listFichaPopulEstimada}">
      <tr>
         <td class="text-center">${item?.abrangencia?.descricao}</td>
         <td class="nowrap text-center">${item.dsMesAnoInicio+ ' a '+item.dsMesAnoFim}</td>
         <td class="nowrap text-center">${ item?.unidAbundanciaPopulacao ? item?.deValorAbundancia+' '+item?.unidAbundanciaPopulacao?.descricao : item?.deValorAbundancia}</td>
         <td class="text-center">${item.dsLocal}</td>
         <td>${raw(item.dsEstimativaPopulacao)}</td>
         <td>
            <a data-action="ficha.openModalSelRefBibFicha"
               data-no-tabela="ficha_popul_estimada_local"
               data-no-coluna="sq_ficha_popul_estimada_local"
               data-sq-registro="${item.id}"
               data-de-rotulo="População Conhecida/Estimada - ${item.dsMesAnoInicio+ ' a '+item.dsMesAnoFim}"
               data-com-pessoal="true"
               data-update-grid="divGridPopulEstimada"
               class="fld btn btn-default btn-xs btn-grid-subform" title="Adicionar/Remover Refeferência bibliográfica."><span class="fa fa-book"></span></a>
               &nbsp;
               ${raw(item.refBibHtml)}
         </td>
         <g:if test="${ ! session.sicae.user.isCO() }">
            <td class="td-actions">
               <a data-action="populacao.editPopulEstimada" data-id="${item.id}" class="fld btn btn-default btn-xs btn-update" title="Alterar">
                  <span class="glyphicon glyphicon-pencil"></span>
               </a>
               <a data-action="populacao.deletePopulEstimada" data-descricao="" data-id="${item.id}" class="fld btn btn-default btn-xs btn-delete" title="Excluir">
                  <span class="glyphicon glyphicon-remove"></span>
               </a>
           </td>
         </g:if>
        </tr>
   </g:each>
   </tbody>
</table>
