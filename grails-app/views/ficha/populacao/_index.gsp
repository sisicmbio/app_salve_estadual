<%-- inicio frmPopulacao --%>
<g:set var="oSND"  value="${[ [key:'S',value:'Sim'], [key:'N', value:'Não'] ,[key:'D', value:'Desconhecido'] ]}"/>
  <form id="frmPopulacaoAba4" class="form-inline" role="form">
    <div class="form-group">
      <label for="" class="control-label">Tendência populacional</label>
      <br>
      <select name="sqTendenciaPopulacional" class="fld form-control fldSelect fld200">
        <option value="">-- selecione --</option>
        <g:each var="item" in="${listTendencia}">
          <option value="${item?.id}" ${ficha?.tendenciaPopulacional?.id==item?.id?' selected':''} >${item?.descricao}</option>
        </g:each>
      </select>
    </div>
     %{--Carlos solicitou remoção destes campos--}%

    %{--<div class="form-group">--}%
      %{--<label for="" class="control-label">Razão Sexual</label>--}%
      %{--<br>--}%
      %{--<input name="dsRazaoSexual" type="text" class="fld form-control text-center fld100" data-mask="0:0" value="${ficha.dsRazaoSexual}">--}%
    %{--</div>--}%
    %{--<div class="form-group">--}%
      %{--<label for="dsDiagnosticoMorfologico" class="control-label">Taxa de Mortalidade Natural--}%
        %{--<i data-action="ficha.openModalSelRefBibFicha"--}%
        %{--data-no-tabela="ficha"--}%
        %{--data-no-coluna="ds_taxa_mortalidade"--}%
        %{--data-sq-registro="${ficha.id}"--}%
        %{--data-de-rotulo="Taxa de Mortalidade Natural"--}%
        %{--class="fa fa-book ml10" title="Informar Referência Bibliográfica" style="color: green;" />--}%
      %{--</label>--}%
      %{--<br>--}%
      %{--<input name="dsTaxaMortalidade"  class="fld form-control fld300"  value="${ficha.dsTaxaMortalidade}"/>--}%
    %{--</div>--}%

    %{--<div class="form-group">--}%
      %{--<label for="" class="control-label">Taxa Cresc. Populacional</label>--}%
      %{--<br>--}%
      %{--<div class="input-group">--}%
        %{--<input name="vlTaxaCrescPopulacional" type="text" class="fld form-control fld150" value="${ficha.vlTaxaCrescPopulacional}"/>--}%
        %{--<div class="input-group-addon">%</div>--}%
      %{--</div>--}%
    %{--</div>--}%
    %{--<div class="form-group">--}%
      %{--<label for="" class="control-label">Período</label>--}%
      %{--<br>--}%
      %{--<select name="sqPeriodoTaxaCrescPopul" class="fld form-control fld200">--}%
        %{--<option value="">-- selecione --</option>--}%
        %{--<g:each var="item" in="${listUnidadeTempo}">--}%
          %{--<option value="${item.id}" ${ficha?.periodoTaxaCrescPopul?.id==item.id?' selected':''} >${item.descricao}</option>--}%
        %{--</g:each>--}%
      %{--</select>--}%
    %{--</div>--}%
    <br />

    <div class="form-group w100p">
      <label for="dsPopulacao" class="control-label">Observação sobre a população
        <i data-action="ficha.openModalSelRefBibFicha"
           data-no-tabela="ficha"
           data-no-coluna="ds_populacao"
           data-sq-registro="${ficha.id}"
           data-de-rotulo="População"
           class="fa fa-book ml10" title="Informar Referência Bibliográfica" style="color: green;" />
      </label>
      <g:if test="${ ! session.sicae.user.isCO() }">
        <div class="checkbox ml20">
          <label title="Clique aqui para preencher o campo com o texto">
            <input type="checkbox" data-action="app.setDefaultText" data-target="dsPopulacao" data-id="${tendenciaDesconhecida?.id?.toString()}"
              ${ ficha?.dsPopulacao?.toLowerCase()?.indexOf('n&atilde;o foram encontradas informa&ccedil;&otilde;es para o t&aacute;xon.') > -1 ? 'checked' : '' }
                   class="fld checkbox-lg">&nbsp;&nbsp;Preencher com: "N&atilde;o foram encontradas informa&ccedil;&otilde;es para o t&aacute;xon."
          </label>
        </div>
      </g:if>
      <br>
      <textarea name="dsPopulacao" id="dsPopulacao" rows="12" class="fld form-control fldTextarea wysiwyg w100p">${ficha.dsPopulacao}</textarea>
    </div>

    <br>

    %{--  TEMPO GERACIONAL--}%
      <div class="form-group">
          <label for="vlTempoGeracional" class="control-label">Tempo geracional</label>
          <br>
          <input name="vlTempoGeracional" id="vlTempoGeracional" type="text" class="fld form-control decimal fld120" value="${ ficha?.vlTempoGeracional?.toString()?.replaceAll(/\.0$/,'') }" maxlength="9" />
      </div>
      <div class="form-group">
          <label for="sqMedidaTempoGeracional" class="control-label">Unidade</label>
          <br>
          <select name="sqMedidaTempoGeracional" data-change="avaliacao.updateGrafico('frmPopulacaoAba4','vlTempoGeracional','sqMedidaTempoGeracional')" id="sqMedidaTempoGeracional" class="fld form-control fld100 fldSelect" ${params.canModify?'':'disabled'} >
              <option data-codigo="" value="">?</option>
              <g:each var="item" in="${listUnidadeTempo}">
                  <option data-codigo="${item.codigo}" value="${item.id}" ${ficha?.medidaTempoGeracional?.id==item.id ? 'selected':''} >${item.descricao}</option>
              </g:each>
          </select>
      </div>
      <div class="form-group">
          <label>&nbsp;</label>
          <br>
          <span><i class="fa fa-arrow-right"></i></span>
      </div>
      %{--      GRAFICO--}%
      <div class="form-group">
        <label>Gráfico</label>
        <div id="graficoWrapper" style="position:relative;width:400px;height:55px;border:1px solid black;background-color:#efefef;-webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;">
              %{-- barra central --}%
              <div id="grafico" style="position:absolute;top:15px;left:10px;border-top:2.5px solid gray;width:380px;height:5px;background-color: #aed8ae;">
                  %{-- menos anos --}%
                  <div style="position:absolute;top:-18px;left:70px;border:none;width:50px;height: 20px;font-weight: bold;font-size:10px;text-align: center" id="divMenosAnos"></div>
                  %{-- mais anos --}%
                  <div style="position:absolute;top:-18px;left:250px;border:none;width:50px;height: 20px;font-weight: bold;font-size:10px;text-align: center" id="divMaisAnos"></div>
                  %{-- marca inicial --}%
                  <div style="position:absolute;top:-5px;left:20px;border-left:3px solid black;width:10px;height:10px;"></div>
                  %{-- marca central --}%
                  <div style="position:absolute;top:-5px;left:190px;border-left:3px solid black;width:10px;height:10px;"></div>
                  %{-- marca final --}%
                  <div style="position:absolute;top:-5px;left:360px;border-left:3px solid black;width:10px;height:10px;"></div>
                  %{-- ano inicial --}%
                  <div style="position:absolute;top:5px;left:3px;border:none;width:35px;height: 20px;font-weight: bold;font-size:14px;text-align:center;" id="divAnoInicial">?</div>
                  %{-- ano avaliacao --}%
                  <div style="position:absolute;top:5px;left:175px;border:none;width:35px;height: 20px;font-weight: bold;font-size:14px;" id="divAnoAvaliacao">${ new Date().format( 'yyyy' )}</div>
                  %{-- ano final --}%
                  <div style="position:absolute;top:5px;left:345px;border:none;width:35px;height: 20px;font-weight: bold;font-size:14px;" id="divAnoFinal">?</div>
                  %{-- texto central --}%
                  <div style="position:absolute;top:18px;left:0px;border:none;width:380px;height:10px;text-align: center;font-size:12px;">Ano da Avaliação</div>
              </div>
          </div>
      </div>
      <br>
      <div class="form-group w100p">
          <label for="dsMetodCalcTempoGeracional" class="control-label">Método de cálculo tempo geracional
              <i data-action="ficha.openModalSelRefBibFicha" data-no-tabela="ficha" data-no-coluna="dsMetodCalcTempoGeracional" data-sq-registro="${ficha.id}" data-de-rotulo="Método de Cálculo Tempo Geracional" class="fa fa-book cursor-pointer ml10" title="Adicionar/Remover Refeferência bibliográfica." style="color: green;" />
          </label>
          <br>
          <textarea name="dsMetodCalcTempoGeracional" id="dsMetodCalcTempoGeracional" rows="3" class="fld form-control fldTextarea wysiwyg w100p">${ficha.dsMetodCalcTempoGeracional}</textarea>
      </div>
%{--  FIM TEMPO GERACIONAL--}%
      <br>
    <div class="form-group w100p">
      <label for="" class="control-label">Caracteristicas genéticas
        <i data-action="ficha.openModalSelRefBibFicha"
           data-no-tabela="ficha"
           data-no-coluna="ds_caracteristica_genetica"
           data-sq-registro="${ficha.id}"
           data-de-rotulo="Característica Genética"
           class="fa fa-book ml10" title="Informar Referência Bibliográfica" style="color: green;" />
      </label>
      <br />
      <textarea name="dsCaracteristicaGenetica" rows="4" class="fld form-control fldTextarea wysiwyg w100p">${ficha.dsCaracteristicaGenetica}</textarea>
    </div>

    <div class="fld panel-footer">
      <button data-action="populacao.saveFrmPopulacao" data-params="sqFicha" class="fld btn btn-success">Gravar</button>
    </div>
  </form>

%{-- Anexos --}%

%{-- modelo antigo sem a janela popup generica --}%
%{--   <form id="frmPopulacaoGrafico" class="form-inline" role="form" enctype='multipart/form-data'>

    <div class="fld form-group" id="divPopulacaoAnexoGrafico">
      <label for="file">Selecionar imagem</label>
      <br />
      <input name="fldPopulacaoAnexoGrafico" type="file" id="file" class="file-loading" required="true" accept="image/*"
      data-show-preview="true"
      data-show-upload="false"
      data-show-caption="false"
      data-show-remove="true"
      data-max-image-width="1024"
      data-max-image-height="720"
      data-max-file-count="1"
      data-allowed-file-extensions='["jpg", "jpeg", "png"]'
      data-main-class="input-group-sm"
      data-max-file-size="1024"
      data-preview-file-type="object">
    </div>
    <br class="fld" />
    <div class="fld form-group">
      <label for="" class="control-label">Legenda</label>
      <br>
      <input name="deLegendaPopulacaoGrafico" type="text" class="fld form-control fld600" value="" required="true" />
    </div>
    <div class="fld panel-footer">
      <button data-action="populacao.addGrafico" data-params="sqFicha" class="fld btn btn-success">Gravar Imagem</button>
    </div>
    <div class="" id="divGridPopulGraficos"></div>
  </form>
 --}%

%{-- imagens de distribuição --}%
<fieldset class="mt20">
    <legend>
        Arquivos anexos
        <a data-action="ficha.openModalSelAnexo"
            data-contexto="GRAFICO_POPULACAO"
            data-de-rotulo="Gráfico de População"
            data-show-principal="false"
            data-update-grid="divGridPopulGraficos"
            class="fld btn btn-default btn-xs btn-grid-subform"
            title="Adicionar documentos/imagens relacionados à população. Ex: gráficos, pdfs etc"><span class="glyphicon glyphicon-plus"></span>
        </a>
    </legend>
</fieldset>
%{-- gride  --}%
<div class="mb20" id="divGridPopulGraficos" data-contexto="GRAFICO_POPULACAO"></div>
<%-- fim form frmDistribucao --%>

  <%-- fim form frmPopulacao --%>

  <%--inicio abas Populacao --%>
  <div class="row">
    <div class="col-sm-12">
      <ul class="nav nav-tabs nav-sub-tabs mt10" id="ulTabsPopulacao">
        <li id="litabPopulEstimada"> <a data-action="ficha.tabClick" data-url="fichaPopulacao/tabPopulEstimada" data-on-load="populacao.tabPopulEstimadaInit" data-params="sqFicha" data-container="tabPopulEstimada" data-reload="false" data-params="id_ficha" data-toggle="tab" href="#tabPopulEstimada">4.1-Informação Local/Regional</a></li>
        <li id="liTabPopAreaVida"> <a data-action="ficha.tabClick" data-url="fichaPopulacao/tabPopAreaVida" data-on-load="populacao.tabPopAreaVidaInit" data-params="sqFicha" data-container="tabPopAreaVida" data-reload="false" data-params="id_ficha" data-toggle="tab" href="#tabPopAreaVida">4.2-Área de Vida</a></li>
      </ul>
      <div id="popTabContent" class="tab-content">
        %{-- inicio aba Populacao Conhecida / Estimada --}%
        <div role="tabpanel" style="min-height:400px;" class="tab-pane tab-pane-ficha active" id="tabPopulEstimada">
        </div>
        %{-- inicio aba Area de vida --}%
        <div role="tabpanel" style="min-height:400px;" class="tab-pane tab-pane-ficha" id="tabPopAreaVida">
        </div>
      </div>
    </div>
    <%--fim col-sm-12 --%>
  </div>
  <%--fim abas Populacao --%>
