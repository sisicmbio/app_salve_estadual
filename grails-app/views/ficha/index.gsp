<!-- view: /views/ficha/index.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<g:javascript>
      var isExterno = ${session.sicae.user.isEX() ? 'true' : 'false'};
      var isAdm = ${session.sicae.user.isADM() ? 'true' : 'false'};
</g:javascript>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <span>Módulo Ficha</span>
                <span id="spanNomeCientificoFicha"
                      style="color:blue;font-size:1.2em;font-weight:bold;margin-left:12px;"></span>
                <g:if test="${!session.sicae.user.isCO()}">
                    <span id="spanBtnAlterarTaxon" style="display: none" class="fld">
                        <button id="btnAlterarTaxon" data-action="ficha.alterarTaxon"
                                class="btn btn-default btn-xs"
                                type="button"
                                title="Utilize esta função para alterar o táxon da ficha<br/> para o atualmente válido.">Alterar</button>
                    </span>
                </g:if>



                <span id="spanSituacaoFicha" style="margin-left: 30px;margin-top:2px;font-weight: normal;">&nbsp;</span>
                <g:if test="${ ! session.sicae.user.isCO() }">
                    <i id="btnAlterarParaExcluida" data-action="ficha.alterarSituacaoParaExcluida"
                       style="display:none;margin-top: 5px"
                       class="fa fa-trash red"
                       title="Alterar a situação da ficha para Excluída da Avaliação ou alterar a justificativa da exclusão">
                    </i>
                </g:if>




                <button id="btnClose" data-action="app.unloadModule" class="btn btn-default pull-right btn-xs"
                        title="Fechar">
                    <span class="glyphicon glyphicon-remove btnClose"></span>
                </button>

                <button id="btnPdfFicha" data-action="ficha.print" style="display:none;"
                        data-params="sqFicha,divNmCientifico|noCientifico" class="btn btn-default pull-right btn-xs"
                        title="Imprimir Ficha">
                    <i class="fa fa-print btnPrint" aria-hidden="true"></i>&nbsp; Imprimir
                </button>
                <g:if test="${session.sicae.user.canAddFicha()}">
                    <button id="btnAdd" data-action="ficha.edit" data-form="frmCicloAvaliacao"
                            data-params="sqFicha,sqCicloAvaliacao,divNmCientifico|noCientifico"
                            class="btn btn-default pull-right btn-xs btnAdd" title="Cadastrar">
                        <span class="glyphicon glyphicon-plus"></span>&nbsp; Cadastrar
                    </button>
                </g:if>
                <button id="btnBack" data-action="ficha.voltar" data-params="voltar:S"
                        class="btn btn-default pull-right btn-xs btnAdd" style="display: none"
                        title="Voltar para a listagem">
                    <span class="glyphicon glyphicon-arrow-left"></span>&nbsp; Fichas
                </button>
            </div>

            <div class="panel-body" id="fichaContainerBody">
                <input type="hidden" id="isLastCicle" value="">
                <input type="hidden" id="sqCicloAvaliacao" value="${session.getAttribute('ciclo').id}">


                <div class="form form-inline" id="frmGridFichas">

                    <div class="form-group">
                        <g:if test="${params.canModify}">
                            <div id="divLastChanged" class="hidden text-left mt10">Últimas alterações: Atualizando...</div>
                        </g:if>

                    </div>

                    %{--Filtro da ficha--}%
                    <div class="hidden mt10" id="divFiltrosFicha">
                        %{-- <g:render template="/templates/filtrosFicha" model="[callback: 'ficha.updateGridFichas', listSituacaoFicha: listSituacaoFicha]"></g:render> --}%
                    </div>

                    %{-- Gride Fichas --}%
                    %{--                        <div id="divGridFicha" style="margin-top: 10px; width:100%;height:auto;overflow:hidden;" class="hidden" ></div>--}%


                    %{-- BOTÕES EXPORTAR/IMPORTAR/BAIXAR--}%
                    <div id="divContainerBotoesFicha" class="text-right mt10" style="padding-bottom:5px;width:100%;max-width:1500px;">

                        %{-- REGISTROS SELECIONADOS --}%
                        <div class="btn-group">
                            <button type="button" id="btnQtdSelecionados" class="btn btn-default btn-xs" style="display:none;" disabled>0 Selecionados</button>
                        </div>
                        %{-- EXPORTAR --}%
                        <div id="divContainerBotaoExportar" class="btn-group">
                            <button type="button" id="btnExportar" class="btn btn-primary btn-xs dropdown-toggle"
                                disabled="true" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Exportar <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu pull-right">
                                <li><a id="btnExportCsvFicha" data-action="ficha.gridCsv" class="btn btn-primary"
                                       data-grid-id="GridFichas"
                                       title="Exportar para planilha">Gride (planilha)</a></li>

                                %{-- CONSULTA EXTERNO NÃO PODE EXPORTAR FICHAS DOC - REUNIÃO 17/11/2022--}%
                                <g:if test="${ ! session.sicae.user.isCOE()}">
                                    <li><a id="btnGerarZipFichas" data-action="ficha.gridZipFichas" class="btn btn-primary"
                                           data-grid-id="GridFichas"
                                           data-params="container:fichaContainerBody,format:rtf"
                                           title="Gerar arquivo zip da(s) ficha(s) no formato doc.">Fichas editáveis (zip)</a>
                                    </li>
                                </g:if>

                                <li><a id="btnGerarZipFichasPdf" data-action="ficha.gridZipFichas"
                                       class="btn btn-primary"
                                       data-grid-id="GridFichas"
                                       data-params="container:fichaContainerBody,format:pdf"
                                       title="Gerar arquivo zip da(s) ficha(s) no formato PDF.">Fichas PDF (zip)</a>
                                </li>


%{--                                <li><a id="btnExportOcorrenciaCsvFicha" data-action="ficha.exportarOcorrencias"--}%
                                <li><a id="btnExportOcorrenciaCsvFicha" data-action="ficha.gridZipFichas"
                                       class="btn btn-primary"
                                       data-grid-id="GridFichas"
                                       data-params="container:fichaContainerBody,format:reg"
                                       title="Exportar os registros de ocorrências da(s) ficha(s) para planilha">Ocorrências (planilha)</a>
                                </li>

                                    %{-- CONSULTA EXTERNO NÃO PODE EXPORTAR PENDENCIAS - REUNIÃO 17/11/2022--}%
                                    <g:if test="${ ! session.sicae.user.isCOE()}">
                                        <li>
                                            <a id="btnExportPendenciasFicha" data-action="ficha.gridZipFichas"
                                               data-grid-id="GridFichas"
                                               data-params="container:fichaContainerBody,format:pen"
                                               class="btn btn-primary"
                                               title="Exportar para planilha as pendências da(s) ficha(s)">Pendências
                                            </a>
                                        </li>
                                    </g:if>

                                <li><a id="btnExportShapesFicha" data-action="ficha.gridZipFichas"
                                       data-grid-id="GridFichas"
                                       data-params="container:fichaContainerBody,format:shp"
                                       class="btn btn-primary"
                                       title="Exportar os shapefiles (zip) da Distribuição Geográfica Atual da(s) ficha(s)<br>informados na aba 2-distribuição.">Shapefiles da distribuição geográfica atual</a></li>
                                %{--<li role="separator" class="divider"></li>--}%
                            </ul>
                        </div>

                         %{-- IMPORTAR --}%
                        <g:if test="${!session.sicae.user.isCO()}">
                        %{-- colaborar externo nao pode porque nao tem uma unica unidade vinculada --}%
                            <g:if test="${!session.sicae.user.isCE()}">
                                <div class="btn-group">
                                    <button type="button" id="btnImportar"
                                            class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown"
                                            aria-haspopup="true" aria-expanded="false">
                                        Importar <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu pull-right">
                                        <g:if test="${session.sicae.user.isADM()}">
                                            <li><a class="btn btn-primary" data-action="ficha.importarPlanilhaFichas"
                                                   data-params="sqFicha:-1,modalName:importacaoFicha,contexto:FICHA"
                                                   title="Fazer a importação da planilha das fichas.">Planilha de fichas</a>
                                            </li>
                                        </g:if>
                                        <g:if test="${session.sicae.user.isADM() || session.sicae.user.isPF() || session.sicae.user.isCI()}">
                                            <li>
                                                <a class="btn btn-primary"
                                                    href="javascript:void(0)"
                                                    data-target=".navbar-collapse"
                                                    data-file-name="mapas"
                                                    data-title="Enviar Mapas - ZIP"
                                                    data-extension="zip"
                                                    data-max-size="250000"
                                                    onclick="modal.uploadArquivoSistema(this)">
                                                    Mapas imagens/shapefiles (zip) </a>
                                            </li>
                                        </g:if>
                                        <li>
                                            <a class="btn btn-primary" data-action="ficha.importarPlanilhaFichas"
                                               data-params="sqFicha:-1,modalName:importacaoFicha,contexto:OCORRENCIA"
                                               title="Fazer a importação da planilha com os registros de ocorrências.">Planilha de ocorrências</a>
                                        </li>
                                        <li>
                                            <a class="btn btn-primary"
                                               href="javascript:void(0)"
                                                title="Fazer a atualização das referências bibliográficas dos registros de ocorrências."
                                               data-target=".navbar-collapse"
                                               data-file-name="referencias_ocorrencias"
                                               data-title="Planilha - Atualizar/Cadastrar referências nos registros de ocorrências"
                                               data-extension="xls"
                                               data-max-size="250000"
                                               onclick="modal.uploadArquivoSistema(this)">
                                               Planilha referências das ocorrências (xls) </a>
                                        </li>
                                        <g:if test="${session.sicae.user.isADM()}">
                                            %{--Importar planilha Histórico Avaliação--}%
                                            <li><a class="btn btn-primary"
                                                href="javascript:void(0)"
                                                data-target=".navbar-collapse"
                                                data-file-name="historico_avaliacao"
                                                data-title="Importar planilha histórico de avaliação"
                                                data-extension="xls"
                                                data-max-size="250000"
                                                onclick="modal.uploadArquivoSistema(this)">
                                                Histórico de Avaliação </a>
                                            </li>
                                        </g:if>

                                        <g:if test="${session.sicae.user.isADM() || session.sicae.user.isPF() || session.sicae.user.isCI()}">
                                            <li>
                                                <a class="btn btn-primary"
                                                    href="javascript:void(0)"
                                                    data-target=".navbar-collapse"
                                                    data-file-name="fotos_audios"
                                                    data-title="Enviar Fotos/Áudios - ZIP"
                                                    data-extension="zip"
                                                    data-max-size="250000"
                                                    onclick="modal.uploadArquivoSistema(this)">
                                                    Fotos/Áudios (zip) </a>
                                            </li>
                                        </g:if>

                                    </ul>
                                </div>
                            </g:if>

                             %{-- BAIXAR --}%
                            <g:if test="${!session.sicae.user.isCT()}">
                                <div class="btn-group">
                                    <button type="button" id="btnDownload"
                                            class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown"
                                            aria-haspopup="true" aria-expanded="false">
                                        Baixar <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a class="btn btn-primary" target="_blank"
                                               title="Fazer o download da planilha modelo para preenchimento off-line dos registros de ocorrência!"
                                               href="/salve-estadual/main/download?fileName=planilha_modelo_importacao_registro">Planilha importação de ocorrências</a>
                                        </li>
                                        <li><a class="btn btn-primary" target="_blank"
                                               title="Fazer o download da planilha para atualização das referências bibliográficas dos registros de ocorrência!"
                                               href="/salve-estadual/main/download?fileName=planilha_atualizar_referencias_registros_salve">Planilha atualizar referências das ocorrências</a>
                                        </li>
                                        <g:if test="${session.sicae.user.isADM()}">
                                            <li><a class="btn btn-primary" target="_blank"
                                                   title="Fazer o download da planilha para fazer atualização das referências bibliográficas dos registros de ocorrência!"
                                                   href="/salve-estadual/main/download?fileName=planilha_modelo_importacao_ficha">Planilha importação de FICHAS</a>
                                            </li>

                                        </g:if>

                                        <g:if test="${session.sicae.user.isADM() || session.sicae.user.isPF() || session.sicae.user.isCI()}">
                                            <li><a class="btn btn-primary" target="_blank"
                                                   title="Fazer o download da planilha modelo para importação de mapas imagens/shapefiles (zip)!"
                                                   href="/salve-estadual/main/download?fileName=planilha_modelo_importacao_mapas_imagens.xls">Planilha importação de mapas IMAGENS/SHAPEFILES (zip)</a>
                                            </li>
                                        </g:if>
                                        <g:if test="${session.sicae.user.isADM()}">
                                            %{--Download planilha modelo Histórico Avaliação--}%
                                            <li><a class="btn btn-primary"
                                                target="_blank"
                                                title="Fazer o download da planilha modelo para importação de histórico de avaliação"
                                                href="/salve-estadual/main/download?fileName=planilha_modelo_importacao_historico_avaliacao.xls">
                                                Planilha importação de HISTÓRICO/AVALIAÇÃO</a>
                                            </li>
                                        </g:if>
                                        <g:if test="${session.sicae.user.isADM() || session.sicae.user.isPF() || session.sicae.user.isCI()}">
                                            <li><a class="btn btn-primary" target="_blank"
                                                   title="Fazer o download da planilha modelo para importação de foto/áudio (zip)!"
                                                   href="/salve-estadual/main/download?fileName=planilha_modelo_importacao_imagem_audio.xls">Planilha importação de FOTOS/ÁUDIOS (zip)</a>
                                            </li>
                                        </g:if>
                                    </ul>
                                </div>
                            </g:if>
                        </g:if>
                    </div>
                    %{--  GRIDE ----------------------------------------------}%
                    <div id="containerGridFichas" style="border-bottom:1px solid gray;width:100%;max-width:1500px"
                         data-params="sqCicloAvaliacao"
                         data-container-filters-id="divFiltrosFicha"
                         data-max-height="790px"
                         data-min-height="300px"
                         data-height="auto"

                         data-input-selected-ids="gridFichasSelectedIds"
                         data-field-id="sqFicha"
                         data-on-load="ficha.gridFichaOnLoad"
                         data-url="ficha/getGridFicha"
                         data-show-footer-pagination="true">
                        <input type="hidden" id="gridFichasSelectedIds" value="">
                        <g:render template="divGridFichas"
                                  model="[gridId: 'GridFichas', listSituacaoFicha: listSituacaoFicha,grideFiltroUnidade:grideFiltroUnidade]"></g:render>
                    </div>
%{--                <div id="divGridFichasPaginationContentFooter2" style="margin-top:10px;height:auto;overflow:hidden;text-align: left"></div>--}%


                %{-- AÇÕES EM LOTE ----------------------------------------------}%
                %{-- PERFIL CONSULTA NÃO TEM AÇÕES EM LOTE--}%
                    <g:if test="${!session.sicae.user.isCO() && !session.sicae.user.isCT()}">
                    %{-- Ações em Lote da Ficha --}%
                        <div role="form" id="fichaLote" class="mt15 hidden">
                            <fieldset>
                                <legend>Ações em lote</legend>
                                <g:if test="${!session.sicae.user.isCI() && !session.sicae.user.isCT() && !session.sicae.user.isCO()}">
                                    <div class="form-group">
                                        <label for="loteSqFichaSituacao"
                                               class="control-label label-bold">Alterar situação para</label>
                                        <br/>
                                        <select name="loteSqFichaSituacao" id="loteSqFichaSituacao"
                                                class="form-control fld250 fldSelect"
                                                data-is-adm="${session.sicae.user.isADM() ? 'S' : 'N'}" tabindex="1">
                                            <option value="">?</option>
                                            <g:each in="${listSituacaoFicha}" var="item">
                                                <option data-codigo="${item.codigoSistema}" value="${item.id}">
                                                    ${item.descricao}
                                                </option>
                                            </g:each>
                                        </select>
                                        <br><button data-action="ficha.alterarSituacaoEmLote"
                                                    class="fld btn btn-danger mt5">Executar</button>
                                    </div>
                                </g:if>

                            %{--<div class="form-group">--}%
                            %{--<button data-action="app.gerarZipFichas" id="btnGerarZipFichas" class="fld btn btn-primary" title="Gerar arquivo zipado de todas as fichas selecionadas.">Gerar Arquivo Zip</button>--}%
                            %{--</div>--}%
                            %{--<div class="form-group">--}%
                            %{--<button data-action="app.exportarGrideOcorrenciaCsv" data-params="full:N" data-grid-id="divGridFicha" class="fld btn btn-primary" title="Exportar ocorrências das fichas selecionadas.">Exportar Ocorrências</button>--}%
                            %{--</div>--}%
                            %{--CICLO DESTINO--}%
                                <g:if
                                    test="${session.sicae.user.isADM() || session.sicae.user.isPF() || session.sicae.user.isCI()}">
                                    <hr class="hr-separador hr-separatos-acoes-lote">

                                    <div class="form-group">
                                        <label for="sqCicloDestinoCopia"
                                               class="control-label label-bold">Copiar a(s) ficha(s) selecionada(s) para outro ciclo</label>
                                        <br/>
                                        <select name="sqCicloDestinoCopia" id="sqCicloDestinoCopia"
                                                class="form-control fld800 fldSelect" tabindex="2">
                                            <option value="">Nenhum ciclo disponível</option>
                                        </select>
                                        <br><button data-action="ficha.copiarFichasCiclo"
                                                    class="fld btn btn-danger mt5">Executar</button>
                                        <small
                                            class="red">(somente as fichas com abas 11.6 e 11.7 preenchidas serão copiadas para o ciclo selecinado).</small>

                                    </div>

                                %{--UNIDADE DESTINO--}%
                                    <g:if test="${session.sicae.user.isADM()}">
                                        <hr class="hr-separador hr-separatos-acoes-lote">

                                        <div class="form-group">
                                            <label
                                                class="control-label label-bold">Mover a(s) ficha(s) selecionada(s) para outra Unidade</label>
                                            <br/>
                                            <select name="sqUnidadeDestino" id="sqUnidadeDestino"
                                                    class="form-control fld800 fldSelect select2" tabindex="3"
                                                    data-s2-url="main/select2Unidade"
                                                    data-s2-placeholder="-- selecione a unidade destino --"
                                                    data-s2-minimum-input-length="3"
                                                    data-s2-auto-height="true">
                                                <option value="">?</option>
                                            </select>
                                            <br><button data-action="ficha.alterarUnidadeLote"
                                                        class="fld btn btn-danger mt5">Executar</button>
                                        </div>
                                    </g:if>

                                %{--                                        GRUPOS--}%
                                    <g:if test="${session.sicae.user.isADM()}">
                                        <hr class="hr-separador hr-separatos-acoes-lote">

                                        <div class="form-group">
                                            <label class="control-label label-bold">Alterar grupo avaliado</label>
                                            <br/>
                                            <select name="sqGrupoAvaliadoLote" id="sqGrupoAvaliadoLote"
                                                    data-id-subgrupo="sqSubgrupoAvaliadoLote"
                                                    onchange="ficha.filtrarSubgrupoLote(this)"
                                                    class="form-control fld300 fldSelect">
                                                <option value="">Nenhum</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label label-bold">Subgrupo</label>
                                            <br/>
                                            <select name="sqSubgrupoAvaliadoLote" id="sqSubgrupoAvaliadoLote"
                                                    class="form-control fld300 fldSelect">
                                                <option value="">Nenhum</option>
                                            </select>
                                        </div>
                                        <br>
                                        <button data-action="ficha.alterarGrupoAvaliadoLote"
                                                class="fld btn btn-danger mt5">Executar</button>

                                        <hr class="hr-separador hr-separatos-acoes-lote">

                                        <div class="form-group">
                                            <label
                                                class="control-label label-bold">Alterar grupo recorte módulo público</label>
                                            <br/>
                                            <select name="sqGrupoSalveLote" id="sqGrupoSalveLote"
                                                    class="form-control fld300 fldSelect">
                                                <option value="">Nenhum</option>
                                            </select>
                                            <br><button data-action="ficha.alterarGrupoSalveLote"
                                                        class="fld btn btn-danger mt5">Executar</button>
                                        </div>
                                    </g:if>

                                    <hr class="hr-separador hr-separatos-acoes-lote">
%{--AUTORIA--}%
                                    <div class="form-group w100p">
                                        <label class="control-label label-bold" for="txAutoriaLote"
                                               title="Informe o nome dos autores manualmente ou marque o preenchimento automático<br>para atualizar a autoria das fichas com o nome do <b>Coordenador de Taxon + nomes dos Avaliadores</b><br>cadastrados na última avaliação.">Atualizar autoria das fichas</label>
                                        <br/>
                                        %{--TINYMCE TEM QUE TER IDs DISTINTOS PARA CADA TEXTAREA--}%
                                        <textarea name="txAutoriaLote" id="txAutoriaLote" rows="5"
                                                  placeholder="Nomes dos autores..."
                                                  onkeyup="ficha.txAutoriaLoteChange()"
                                                  style="min-height: 200px;"
                                                  class="form-control fldTextarea w100p texto"></textarea>

                                        <div id="divCheckAutoriaLote">
                                            <label>
                                                <input type="checkbox" id="checkAutoriaLote"
                                                       onClick="ficha.chkAutoriaLoteChange()"
                                                       class="checkbox-lg"> Preencher autormaticamente?
                                            </label>
                                            <small class="red">(somente quem participou de alguma avaliação como AVALIADOR será listado)</small>
                                        </div>
                                        <br><button data-action="ficha.alterarAutoriaLote"
                                                    class="fld btn btn-danger mt5">Executar</button>
                                        <small
                                            class="red">(somente as fichas com aba 11.6 preenchida)</small>

                                    </div>

                                    <hr class="hr-separador hr-separatos-acoes-lote">

 %{-- EQUIPE TÉCNICA--}%
                                    <div class="form-group w100p">
                                        <label class="control-label label-bold" for="txEquipeTecnicaLote"
                                               title="Informe os nomes dos participantes que compõem a equipe técnica da(s) ficha(s) um por linha ou separados por vírgula ou ponto-e-vírgula.">Atualizar equipe técnica das fichas</label>
                                        <br/>
                                        %{--TINYMCE TEM QUE TER IDs DISTINTOS PARA CADA TEXTAREA--}%
                                        <textarea name="txEquipeTecnicaLote" id="txEquipeTecnicaLote" rows="5"
                                                  placeholder="Nomes da equipe técnica..."
                                                  onkeyup="ficha.txEquipeTecnicaLoteChange()"
                                                  style="min-height: 200px;"
                                                  class="form-control fldTextarea w100p texto"></textarea>

                                        %{--<div id="divCheckEquipeTecnicaLote">
                                            <label>
                                                <input type="checkbox" id="checkEquipeTecnicaLote"
                                                       onClick="ficha.chkEquipeTecnicaLoteChange()"
                                                       class="checkbox-lg"> Preencher autormaticamente?
                                            </label>
--}%%{--                                            <small class="red">(somente quem participou de alguma avaliação como EQUIPE TÉCNICA será listado)</small>--}%%{--
                                        </div>--}%
                                        <br><button data-action="ficha.alterarEquipeTecnicaLote"
                                                    class="fld btn btn-danger mt5">Executar</button>
%{--                                        <small--}%
%{--                                            class="red">(somente as fichas com aba 11.6 preenchida serão atualizadas)</small>--}%
                                    </div>

                                    <hr class="hr-separador hr-separatos-acoes-lote">
 %{-- COLABORADORES --}%
                                    <div class="form-group w100p">
                                        <label class="control-label label-bold" for="txColaboradoresLote"
                                               title="Informe os nomes dos colaboradores da(s) ficha(s).">Atualizar colaboradores das fichas</label>
                                        <br/>
                                        %{--TINYMCE TEM QUE TER IDs DISTINTOS PARA CADA TEXTAREA--}%
                                        <textarea name="txColaboradoresLote" id="txColaboradoresLote" rows="5"
                                                  placeholder="Nomes dos colaboradores..."
                                                  onkeyup="ficha.txColaboradoresLoteChange()"
                                                  style="min-height: 200px;"
                                                  class="form-control fldTextarea w100p texto"></textarea>

                                        <div id="divCheckColaboradoresLote">
                                            <label>
                                                <input type="checkbox" id="checkColaboradoresLote"
                                                       onClick="ficha.chkColaboradoresLoteChange()"
                                                       class="checkbox-lg"> Preencher autormaticamente?
                                            </label>
                                            <small class="red">(somente quem participou de alguma avaliação como COLABORADOR será listado)</small>
                                        </div>
                                        <br><button data-action="ficha.alterarColaboradoresLote"
                                                    class="fld btn btn-danger mt5">Executar</button>
                                        <small
                                            class="red">(somente as fichas com aba 11.6 preenchida serão atualizadas)</small>
                                    </div>



                                    <g:if test="${session.sicae.user.canDeleteFicha()}">
                                        <hr class="red hr-separador hr-separatos-acoes-lote">

                                        <div class="form-group">
                                            <label
                                                class="control-label label-bold red"><b>Excluir TODAS as fichas selecionadas</b>
                                            </label>
                                            <br><button data-action="ficha.excluirFichaEmLote"
                                                        class="fld btn btn-danger mt5">Executar</button>
                                        </div>
                                    </g:if>

                                </g:if>
                            </fieldset>
                        </div>
                    </g:if>
                </div> %{--div form filtros e gride --}%
                <div id="divEdicaoFicha" class="hidden"></div>
            </div> %{--col-12 --}%
        </div> %{-- panel-body --}%
    </div> %{-- panel --}%
</div> %{-- col 12 --}%
</div> %{-- row --}%
%{-- fim container ficha--}%
