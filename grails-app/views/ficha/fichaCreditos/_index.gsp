<!-- view: /views/ficha/fichaCreditos/_index.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<form id="frmCreditos" name="frmCreditos" class="mt10 form-inline row" >
    <div class="col-sm-12 mt10">
        <div id="divAlertUc" class="alert alert-info" style="">
        <strong>Informação!</strong>&nbsp;Para alterar os créditos, acesse a sub-aba <a href="#" data-action="ficha.selAbaResultadoAvaliacao">11.6 - Resultado Avaliação</a>.
        </div>
    </div>
    <div class="col-sm-12 mt10">
        <div class="page-header group-header clearfix">
            <h3><i class="fa fa-slideshare"></i>&nbsp;Autores</h3>
        </div>
        <div class="w100p " style="padding:10px;border:0px solid gray; height:auto;min-height: 100px;max-height: 300px;overflow: auto">
            ${raw(row.dsAutoria)}
        </div>
    </div>
    <div class="col-sm-12">
        <div class="page-header group-header clearfix">
            <h3><i class="fa fa-slideshare"></i>&nbsp;Equipe técnica</h3>
        </div>
        <div class="w100p" style="padding:10px;border:0px solid gray; height:auto;min-height: 100px;max-height: 300px;overflow: auto">
            ${raw(row.dsEquipeTecnica)}
        </div>
    </div>
    <div class="col-sm-12">
        <div class="page-header group-header clearfix">
            <h3><i class="fa fa-slideshare"></i>&nbsp;Colaboradores</h3>
        </div>
        <div class="w100p" style="padding:5px;height:auto;min-height: 100px;max-height: 300px;overflow: auto">
            ${raw(row.dsColaboradores)}
        </div>
    </div>
</form>
