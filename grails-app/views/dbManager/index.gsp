<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>
        Consulta Banco de Dados
    </title>
</head>
<body>
    <div class="col-sm-12">
        <div class="panel panel-default w100p" style="">
            <div class="panel-heading">
                <span>
                    Consulta Banco de Dados
                </span>

                <button id="btnClose" onClick="document.location.replace('/salve');" class="btn btn-default pull-right btn-xs" title="Fechar"><span class="glyphicon glyphicon-remove btnClose"></span></button>
            </div>


            <div class="panel-body">
                <form id="frmSql" class="form-inline" role="form" method="POST" onsubmit="return blockUi();">
                    <div class="fld form-group w100p">
                        <label for="fldSql" class="control-label">Comando</label>
                        <textarea id="fldSql" name="fldSql" class="form-control w100p" style="min-height: 100px;height:400px;font-family: 'Courier New';font-size:18px;">${params?.fldSql}</textarea>
                    </div>
                    <div class="fld panel-footer">
                        <button id="btnExecutar" class="fld btn btn-success">Executar</button>
                        <button id="btnLimparHist" onClick="limparHistorico()" class="fld btn btn-default pull-right" type="button">Limpar Histórico</button>
                    </div>
                </form>
                <label>Histórico</label><br>
                <div style="border:1px solid blue;height: auto;background-color:#efefef">
                    <textarea id="fldSqlHist" name="fldSqlHist" class="form-control w100p" readonly style="min-height: 40px;height:300px;font-family: 'Courier New';font-size:18px;"></textarea>
                </div>
             <div id="divGrid">
                 <g:if test="${msg}">
                     <div class="alert alert-warning">
                         ${raw(msg)}
                     </div>
                 </g:if>

                 <g:if test="${rows}">
                     <table class="table table-striped table-bordered">
                     <g:each status="reg" in="${rows}" var="row">
                         <g:if test="${ reg==0 }">
                             <thead>
                                 <tr>
                                    <g:each status="colNum" in="${rows[0].keySet() }" var="column">
                                    <th>${column}</th>
                                    </g:each>
                                 </tr>
                            </thead>
                         </g:if>
                         <tr>
                             <g:each status="rowNum" in="${ row.values() }" var="value">
                                 <td>${value}</td>
                             </g:each>
                         </tr>
                     </g:each>
                     </table>
                 </g:if>
              </div>
            </div>
        </div>
    </div>
<g:javascript>
    var init = function()
    {
        $('document').ready(function ()
        {
            var sql = app.lsGet('fldSql')
            if( sql )
            {
                $("#fldSql").val( sql );
                $("#fldSql").focus()
            }

            var sqlHist = app.lsGet('fldSqlHist')
            if( sqlHist )
            {
                $("#fldSqlHist").val( sqlHist );
            }

            $("#fldSql").bind('keyup',function(e) {
                e.preventDefault()
                var code = e.keyCode || e.which;
                if( code == 88 && event.altKey ) // alt + x
                {
                    $("#btnExecutar").click();
                }
            });

        })
    }
    var limparHistorico = function()
    {
        app.lsSet('fldSqlHist','' );
        $("#fldSqlHist").val('');
    }

    var blockUi = function()
    {
        var sql = $("#fldSql").val().trim();
        var hist = $("#fldSqlHist").val().trim();
        app.lsSet('fldSql',sql );
        if( sql && hist.toUpperCase().indexOf(sql.toUpperCase() ) == -1)
        {
            hist += '\n'+sql;
            app.lsSet('fldSqlHist',hist.trim() );
        }
        $("#divGrid").html('');
        app.blockUI('Executando...');
        return true;
    }
    window.setTimeout(function() { init() },1000);
   </g:javascript>
</body>
</html>
