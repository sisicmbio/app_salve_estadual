<div class="window-container text-left">
	<form id="frmPesquisa" name="frmPesquisa" class="form-inline" role="form">

      	<div class="form-group">
			<label for="codigo" class="control-label">
				Código
			</label>
			<br>
			<input name="codigo" id="codigo" type="text" class="form-control form-clean fld100" placeholder="1, 1.1" value="${pesquisa?.codigo}" required="false"/>
		</div>

		<div class="form-group">
			<label for="inputPesquisa" class="control-label">
				Descrição da pesquisa
			</label>
			<br>
			<input name="descricao" id="inputPesquisa" type="text" class="form-control form-clean fld500" placeholder="Informe o nome do pesquisa" value="${pesquisa?.descricao}" required="true" data-msg-required="Campo Obrigatório"/>
		</div>
        <br>
        %{--        INTEGRACAO SIS/IUCN--}%
        <div class="form-group">
            <label for="sqIucnPesquisa" class="control-label">
                Pesquisa (SIS/IUCN)
            </label>
            <br>
            <select name="sqIucnPesquisa" id="sqIucnPesquisa" class="form-control select2" style="width:600px;"
                    data-s2-placeholder="?"
                    data-s2-minimum-input-length="0"
                    data-s2-auto-height="true">
                <option value="">?</option>
                <g:each var="item" in="${listIucnPesquisa}">
                    <option value="${item.id}" ${ item.id == iucnPesquisaApoio?.iucnPesquisa?.id ? " selected":"" }>${ item.descricaoCompleta }</option>
                </g:each>
            </select>
        </div>

        <br>
        <div class="form-group">
            <label for="codigoSistema" class="control-label">
                Código sistema
            </label>
            <br>
            <input name="codigoSistema" id="codigoSistema" type="text" class="form-control form-clean fld400" placeholder="Código uso sistema" value="${pesquisa?.codigoSistema}" required="false"/>
        </div>
        <div class="form-group">
            <label for="ordem" class="control-label">
                Ordem listagem
            </label>
            <br>
            <input name="ordem" id="ordem" type="text" class="form-control form-clean fld200" placeholder="Ordem apresentação" value="${pesquisa?.ordem}" required="false"/>
        </div>

        <div class="fld panel-footer">
			<button id="btnSaveFrmPesquisa" data-action="pesquisa.save" class="fld btn btn-success">Gravar</button>
		</div>
	</form>
</div>
