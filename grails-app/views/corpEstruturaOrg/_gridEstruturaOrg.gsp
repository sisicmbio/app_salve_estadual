<!-- view: /views/corpEstruturaOrg/_gridEstruturaOrg -->
<!-- controller:${params?.controller} -->
<!-- action: ${params?.action} -->
<table id="tableEstruturaOrg">
    <colgroup>
        <col width="auto"></col>
        <col width="120px"></col>
        <col width="200px"></col>
        <col width="100px;"></col>
        <col width="150px;"></col>
        <col width="90px;"></col>
    </colgroup>
    <thead>
        <tr>
            <th>Nome instituição / unidade</th>
            <th>Sigla</th>
            <th>Nome contato</th>
            <th>Telefone</th>
            <th>Email</th>
            <th>Ações</th>
        </tr>
    </thead>
    <tbody>
       <tr>
            %{-- NOME INSTITUIÇÃO / UNIDADE --}%
            <td class="text-left text-nowrap"></td>

            %{-- SIGLA --}%
            <td class="text-center"></td>

            %{-- NOME DO CONTATO --}%
            <td class="text-left" ></td>

            %{-- TELEFONE DO CONTATO --}%
            <td class="text-left"></td>

            %{-- EMAIL DO CONTATO --}%
            <td class="text-center"></td>

            %{-- ACOES --}%
            <td class="text-center">
                <i class="fa fa-edit fa-button cursor-pointer blue" title="Alterar" onClick="corpEstruturaOrg.edit($id)"></i>
                <i class="fa fa-remove fa-button cursor-pointer red" title="Excluir" onClick="corpEstruturaOrg.delete($id,'$nome')"></i>
            </td>
        </tr>
    </tbody>
</table>
