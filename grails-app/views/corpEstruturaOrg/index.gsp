<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="ajax"/>
    <asset:stylesheet src="jquery/plugins/treetable/css/jquery.treetable.css" />
    <asset:stylesheet src="jquery/plugins/treetable/css/jquery.treetable.theme.salve.css" />
</head>
<body>
<div class="col-sm-12">
    <div class="panel panel-default w100p" id="pnlEstruturaOrg">
        <div class="panel-heading">
            <span>
                Instituição / Unidade Interna
            </span>
            <button id="btnClose" data-action="app.unloadModule" class="btn btn-default pull-right btn-xs" title="Fechar"><span class="glyphicon glyphicon-remove btnClose"></span></button>
        </div>

        %{-- COLLAPSE ICON --}%
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-12 text-left">
                    <button class="btn btn-secondary btn-xs" title="Mostrar/esconder o formulário"
                            data-toggle="collapse"
                            data-target="#frmEstruturaOrg"
                            aria-expanded="false"
                            aria-controls="formulário"><i id="collapseIcon" class="fa fa-chevron-down"></i>
                    </button>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">

                    <form id="frmEstruturaOrg" class="row collapse">

                        %{--CAMPO ID--}%
                        <input type="hidden" id="sqPessoa" name="sqPessoa" value="">
                        <input type="hidden" id="sqPai" name="sqPai" value="">



                        %{-- INSTITUICAO/UNIDADE SUPERIOR --}%
                        <div class="form-group col-sm-12">
                            <label class="" for="noPai" title="Para criar a hierarquia entre as instituições /nidades, clique na instituição/unidade superior na tabela abaixo ao cadastrar a instituição/unidade subordinada.">Nome da instituição / unidade superior</label>
                            <div class="input-group">
                                <input type="text" id="noPai" class="form-control disabled" readonly="true" placeholder="não possui (clique na tabela abaixo para selecionar)">
                                <span class="input-group-addon cursor-pointer red" title="Remover a instituição / unidade superior selecionada" onclick="corpEstruturaOrg.clearPai()">X</span>
                            </div>

                            %{--<select class="form-control fldSelect" id="sqPai" name="sqPai">
                            <option value="">-- selecione --</option>
                            <g:each var="item" in="${instituicoes}">
                                <option value="${item.id}">${item.sgInstituicao}</option>
                            </g:each>
                            </select>--}%
                        </div>

                        %{-- NOME DA INSTITUICAO / UNIDADE--}%
                        <div class="form-group col-sm-8">
                            <label class="label-required" for="noPessoa">Nome da instituição / unidade</label>
                            <input type="text" class="form-control" id="noPessoa" name="noPessoa" value="">
                        </div>

                        %{-- CNPJ--}%
                        <div class="form-group col-sm-4">
                            <label for="nuCnpj">CNPJ</label>
                            <input type="text" class="form-control fld200" id="nuCnpj" name="nuCnpj" value="" data-mask="00.000.000/0000-00" maxlength="18">
                        </div>


%{--                        SIGLA --}%
                        <div class="form-group col-sm-12">
                            <div class="form-inline">
                                <label for="sgInstituicao" class="label-required">Sigla</label><br>
                                <input type="text" class="form-control fld200" id="sgInstituicao" name="sgInstituicao" value="">
                                <label for="stInterna" class="ml10 cursor-pointer"
                                       title="Marque este campo quando a unidade fizer parte da sua estrutura organizacional">Unidade interna?
                                </label>
                                <input id="stInterna" name="stInterna" class="checkbox-lg" type="checkbox" value="true">

                            </div>
                        </div>


%{--                        NOME DO CONTATO --}%
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="noContato">Nome do contato</label>
                            <input type="text" class="form-control" id="noContato" name="noContato" value="">
                        </div>

%{--                        NUMERO TELEFONE DO CONTATO --}%
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="noContato">Telefone do contato</label>
                            <input type="text" class="form-control" id="nuTelefone" name="nuTelefone" value="">
                        </div>

%{--                       EMAIL DO CONTATO DO CONTATO --}%
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="deEmail">Email</label>
                            <input type="text" class="form-control" id="deEmail" name="deEmail" value="">
                        </div>

                        %{--BOTOES--}%
                        <div class="col-sm-12">
                            <div class="panel-footer">
                                <button type="button" id="btnSave" onclick="corpEstruturaOrg.save();" class="btn btn-success">Gravar</button>
                                <button type="button" class="btn btn-danger pull-right" onclick="corpEstruturaOrg.reset();">Limpar</button>
                            </div>
                        </div>
                        <div class="col-sm-12"><small class="red fs10">* - campo obrigatório</small></div>
                    </form>

                    %{-- GRIDE ABAIXO DO FORM--}%
                    <div class="row mt20">
                        <div class="col-sm-12">
                            <h4 id="numRecords">${instituicoes.size()+' registro(s)'}</h4>
                            <div style="width: auto;height: auto" id="gridContainer">
                                <g:render template="gridEstruturaOrg" model="[instituicoes:instituicoes]"></g:render>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div style="min-height: 200px;display: block"></div>
</div>
<asset:javascript src="jquery/plugins/treetable/jquery.treetable.js" defer="true"/>
<asset:javascript src="corpEstruturaOrg.js" defer="true"/>

</body>
</html>
