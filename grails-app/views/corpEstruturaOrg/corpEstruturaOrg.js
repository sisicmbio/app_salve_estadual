//# sourceURL=corpEstruturaOrg.js
;var corpEstruturaOrg = {
    "map":null,
    "tree":null,
    "table":null,
    "node":null,
    "nodePai":null,
    "init": function () {
        app.focus("noPessoa");
        corpEstruturaOrg.fancyTree();
        $('#frmEstruturaOrg').on('shown.bs.collapse', function () {
            $("#collapseIcon").removeClass('fa-chevron-down')
            $("#collapseIcon").addClass('fa-chevron-up')
        })
        $('#frmEstruturaOrg').on('hidden.bs.collapse', function () {
            $("#collapseIcon").removeClass('fa-chevron-up')
            $("#collapseIcon").addClass('fa-chevron-down')

        })
    },
    "reset":function(){
        app.reset('frmEstruturaOrg','sqPai,noPai,stInterna');
        app.focus('noPessoa');
        if( corpEstruturaOrg.node ) {
            corpEstruturaOrg.node.setActive(false);
            corpEstruturaOrg.node = null;
        }
    },
    "clearPai":function(){
        if( corpEstruturaOrg.nodePai ) {
            corpEstruturaOrg.nodePai.setActive(false);
            corpEstruturaOrg.nodePai = null;
        }
        $("#noPai").val('');
        $("#sqPai").val('');
        app.focus('noPessoa');
    },
    "fancyTree":function(){
        corpEstruturaOrg.table = $("#tableEstruturaOrg");
        corpEstruturaOrg.table.fancytree({
            "extensions": ["table"],
            "focusOnSelect":true,
            "selectMode": 1,
            "table": {
                "indentation": 10,      // indent 20px per node level
                "nodeColumnIdx": 0,     // render the node title into the 2nd column
            },
            "source": {
                "url": baseUrl + 'corpEstruturaOrg/treeview'
            },
            "lazyLoad": function(event, data) {
                data.result = {"url": baseUrl + 'corpEstruturaOrg/treeview/'+data.node.key}
            },
            "renderColumns": function(event, data) {
                var node = data.node,
                $tdList = $(node.tr).find(">td");
                // preencher as colunas da table
                $tdList.eq(1).text(node.data.sgInstituicao);
                $tdList.eq(2).text(node.data.noContato);
                $tdList.eq(3).text(node.data.nuTelefone);
                $tdList.eq(4).text(node.data.deEmail);
                $tdList.eq(5).html($tdList.eq(5).html().replace(/\$id/g,node.key).replace(/\$nome/g,node.data.sgInstituicao) );

           // EVENTOS DA TREEVIEW
            }, "activate": function(event, data) {
                if( data.node && ! data.node.isRoot()  ) {
                    corpEstruturaOrg.nodePai = data.node;
                    $("#noPai").val(data.node.title );
                    $("#sqPai").val(data.node.key );
                }
            },"expand": function(event, data) {
            },"collapse": function(event, data) {
            },"postProcess": function(event, data) {
                if( ! data.response[0] ){
                    app.growl('Não possui instituição / unidade subordinada',1,'','tc','large','error');
                }
            },
        });
        corpEstruturaOrg.tree = corpEstruturaOrg.table.fancytree("getTree");
    },
    "updateTree":function(data ){
        if( ! data.sqPessoa ){
            return;
        }
        var parentNode=null;
        var newNode = corpEstruturaOrg.tree.getNodeByKey(String(data.sqPessoa) );
        if( ! newNode ){
            if( data.sqPai ) {
               // é filho
               parentNode =  corpEstruturaOrg.tree.getNodeByKey( String(data.sqPai ) );
               if( parentNode ) {
                   if (parentNode.isExpanded()) {
                       // fechar todos os nós filhos de adicionar um novo filho
                       parentNode.visit(function(node) {
                           node.setExpanded(false);
                       });
                       setTimeout(function(){
                           newNode = parentNode.addChildren({
                               "key": data.sqPessoa,
                               "title": data.noPessoa,
                               "deEmail": data.deEmail,
                               "sgInstituicao": data.sgInstituicao,
                               "nuTelefone": data.nuTelefone,
                               "noContato": data.noContato,
                               "lazy": false,
                               "folder": false
                           })
                           parentNode.sortChildren(null, true);
                       },500);
                   } else {
                       parentNode.lazy = true;
                       parentNode.load(true);
                       parentNode.setExpanded(true);
                       newNode = corpEstruturaOrg.tree.getNodeByKey(String(data.sqPessoa) );
                   }
               }
            } else {
                // é pai
                var rootNode = corpEstruturaOrg.tree.getRootNode();

                // fechar todos os nós antes de adicionar um novo pai
                rootNode.visit(function(node) {
                    node.setExpanded(false);
                });
                setTimeout(function(){
                    rootNode.addChildren({
                        "key": data.sqPessoa,
                        "title": data.noPessoa,
                        "deEmail": data.deEmail,
                        "sgInstituicao": data.sgInstituicao,
                        "nuTelefone": data.nuTelefone,
                        "noContato": data.noContato,
                        "lazy": false,
                        "folder": true
                    })
                    rootNode.sortChildren(null,true);
                    rootNode.setActive(true);
                },1000);
            }
        } else {
            // node existente
            parentNode = newNode.getParent();
            newNode.data.sgInstituicao = data.sgInstituicao;
            newNode.data.noContato = data.noContato;
            newNode.data.nuTelefone = data.nuTelefone;
            newNode.data.deEmail = data.deEmail;
            newNode.setTitle(data.noPessoa); // update row
            // verificar se o nó mudou de pai
            if( String( parentNode.key ) != String( data.sqPai) ) {
                parentNode = corpEstruturaOrg.tree.getNodeByKey( String(data.sqPai) ) || corpEstruturaOrg.tree.getRootNode();
                if( parentNode.isExpanded()){
                    newNode.moveTo(parentNode, 'child');
                    parentNode.sortChildren(null,true);
                }
            }
        }
    },
    "save":function(){
        var data = {'sqPessoa':$("#sqPessoa").val()
            ,'sqPai':$("#sqPai").val()
            ,'noPessoa':$("#noPessoa").val()
            ,'sgInstituicao':$("#sgInstituicao").val()
            ,'stInterna': ($("#stInterna:checked").val() || false)
            ,'noContato':$("#noContato").val()
            ,'nuTelefone':$("#nuTelefone").val()
            ,'deEmail':$("#deEmail").val()
            ,'nuCnpj':$("#nuCnpj").val()
        };
        var errors = [];

        if( ! data.noPessoa ){
            errors.push('Nome da instituição / unidade deve ser informado')
        }

        if( ! data.sgInstituicao ){
            errors.push('Sigla da instituição / unidade deve ser informado')
        }
        if( data.nuCnpj && ! String(data.nuCnpj).isCNPJ() ){
            errors.push('CNPJ inexistente')
        }

        // errors
        if( errors.length > 0 ){
            app.growl(errors.join('<br>'),'4','Erros encontrados','tc','','error');
            return;
        }
        app.ajax(baseUrl+'corpEstruturaOrg/save',data,function(res){
            if( res.status == 0 ) {
                corpEstruturaOrg.updateTree( res.data );
                corpEstruturaOrg.reset();
            }
        },'Gravando...')
    },
    "edit":function(sqPessoa ) {
        if( sqPessoa ) {
            $('#frmEstruturaOrg').collapse('show');
            app.ajax(baseUrl + 'corpEstruturaOrg/edit', {"sqPessoa": sqPessoa}, function (res) {
                if (res.status == 0) {
                    $("#sqPai").val( res.data.sqPai );
                    $("#sqPessoa").val( res.data.sqPessoa );
                    $("#noPessoa").val( res.data.noPessoa );
                    $("#sgInstituicao").val( res.data.sgInstituicao );
                    $("#stInterna").prop('checked', res.data.stInterna );
                    $("#noContato").val( res.data.noContato );
                    $("#nuTelefone").val( res.data.nuTelefone );
                    $("#deEmail").val( res.data.deEmail );
                    $("#nuCnpj").val(  res.data.nuCnpj );
                    app.focus('noPessoa');
                    corpEstruturaOrg.node = corpEstruturaOrg.table.fancytree("getTree").getNodeByKey(String(sqPessoa));
                    corpEstruturaOrg.node.setActive();
                    corpEstruturaOrg.nodePai=corpEstruturaOrg.node.getParent();
                    $("#noPai").val('');
                    if( ! corpEstruturaOrg.nodePai.isRoot() ){
                        $("#noPai").val(corpEstruturaOrg.nodePai.title);
                    }
                }
            },'Carregando...')
        }
    },
    "delete":function(sqPessoa, noPessoa ) {
        if (sqPessoa) {
            if ( app.confirm('Confirma a exclusão do Instituição / unidade <b>' + noPessoa + '</b>?', function () {
                app.ajax(baseUrl + 'corpEstruturaOrg/delete', {"sqPessoa": sqPessoa}, function (res) {
                    if (res.status == 0) {
                        corpEstruturaOrg.table.fancytree("getTree").getNodeByKey(String(sqPessoa)).remove();
                        corpEstruturaOrg.reset()
                        corpEstruturaOrg.clearPai();
                        //corpEstruturaOrg.updateNumRecords();
                    }
                }, 'Aguarde...')
            })
            ) ;
        }
    },

    "updateNumRecords":function(){
        var numRecords = $("#gridContainer table tbody tr").size();
        $("#numRecords").html( String(numRecords ) + ( numRecords == 1 ? '&nbsp;Unidade cadastrada' : '&nbsp;Unidades cadastradas') );
    },

}
setTimeout(function(){corpEstruturaOrg.init(),1000});
