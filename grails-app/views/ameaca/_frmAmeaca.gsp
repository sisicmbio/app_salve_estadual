<div class="window-container text-left">
	<form id="frmAmeaca" name="frmAmeaca" class="form-inline" role="form">

      	<div class="form-group">
			<label for="codigo" class="control-label">
				Código
			</label>
			<br>
			<input name="codigo" id="codigo" type="text" class="form-control form-clean fld100" placeholder="1, 1.1" value="${ameaca?.codigo}" required="false"/>
		</div>

		<div class="form-group">
			<label for="inputAmeaca" class="control-label">
				Descrição da ameaça
			</label>
			<br>
			<input name="descricao" id="inputAmeaca" type="text" class="form-control form-clean fld500" placeholder="Informe o nome do ameaca" value="${ameaca?.descricao}" required="true" data-msg-required="Campo Obrigatório"/>
		</div>
        <br>
        %{--        INTEGRACAO SIS/IUCN--}%
        <div class="form-group">
            <label for="sqIucnAmeaca" class="control-label">
                Ameaça (SIS/IUCN)
            </label>
            <br>
            <select name="sqIucnAmeaca" id="sqIucnAmeaca" class="form-control select2" style="width:600px;"
                    data-s2-placeholder="?"
                    data-s2-minimum-input-length="0"
                    data-s2-auto-height="true">
                <option value="">?</option>
                <g:each var="item" in="${listIucnAmeaca}">
                    <option value="${item.id}" ${ item.id == iucnAmeacaApoio?.iucnAmeaca?.id ? " selected":"" }>${ item.descricaoCompleta }</option>
                </g:each>
            </select>
        </div>

        <br>
        <div class="form-group">
            <label for="codigoSistema" class="control-label">
                Código sistema
            </label>
            <br>
            <input name="codigoSistema" id="codigoSistema" type="text" class="form-control form-clean fld400" placeholder="Código uso sistema" value="${ameaca?.codigoSistema}" required="false"/>
        </div>
        <div class="form-group">
            <label for="ordem" class="control-label">
                Ordem listagem
            </label>
            <br>
            <input name="ordem" id="ordem" type="text" class="form-control form-clean fld200" placeholder="Ordem apresentação" value="${ameaca?.ordem}" required="false"/>
        </div>

        <div class="fld panel-footer">
			<button id="btnSaveFrmAmeaca" data-action="ameaca.save" class="fld btn btn-success">Gravar</button>
		</div>
	</form>
</div>
