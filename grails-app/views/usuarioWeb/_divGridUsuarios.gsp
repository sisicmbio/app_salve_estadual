<!-- view: /views/usuarioWeb/_divGridUsuarios -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<table id="table${gridId}" cellpadding="0" cellspacing="0" class="table table-striped table-sorting">
    <thead>
    <tr id="tr${gridId}Pagination">
        <td colspan="8">
             <div class="text-left" style="min-height:0;height: auto;overflow: hidden" id="div${(gridId ?: pagination.gridId)}PaginationContent">
                 <g:if test="${pagination?.totalRecords}">
                    <g:gridPagination  pagination="${pagination}"/>
                </g:if>
            </div>
        </td>
    </tr>
    <tr id="tr${gridId}Filters" class="table-filters" data-grid-id="${gridId}">
        <td>
            <div style="display:flex;flex-direction:row; justify-content:space-between;">
                <i class="fa fa-question-circle"
                   title="Utilize os campos ao lado para filtragem dos registros.<br> Informe o valor a ser localizado e pressione <span class='blue'>Enter</span>.<br>Para remover o filtro limpe o campo e pressione <span class='blue'>Enter</span> novamente."></i>
                <i data-grid-id="${gridId}" class="fa fa-eraser" title="Limpar filtro(s)" data-click="app.gridReset('${gridId}');"></i>
                <i data-grid-id="${gridId}" class="fa fa-filter" title="Aplicar filtro(s)" data-click="app.grid('${gridId}');"></i>
            </div>
        </td>
        <td><input type="text" name="fldNoUsuario${gridId}" value="${params?.noUsuario ?: ''}" class="form-control table-filter"
                   placeholder="nome" data-field="noUsuario" data-enter="app.grid('${gridId}');"></td>
        <td><input type="text" name="fldDeEmail{id}" value="${params?.deEmail ?: ''}" class="form-control table-filter"
                   placeholder="email" data-field="deEmail" data-enter="app.grid('${gridId}');"></td>
        <td><input type="text" name="fldsgInstituicao${gridId}" value="${params?.sgInstituicao ?: ''}" class="form-control table-filter"
                   placeholder="instituição" data-field="sgInstituicao" data-enter="app.grid('${gridId}');"></td>
        <td><select name="fldEnabled${gridId}" class="form-control table-filter" placeholder="" data-field="enabled"
                    data-change="app.grid('${gridId}');">
            <option value="">Todos</option>
            <option value="S" ${params?.enabled == 'S' ? 'selected' : ''}>Ativados</option>
            <option value="N" ${params?.enabled == 'N' ? 'selected' : ''}>Desativados</option></select></td>
        <td><select name="fldAccountLocked${gridId}" class="form-control table-filter" placeholder="" data-field="accountLocked"
                    data-change="app.grid">
            <option value="">Todos</option>
            <option value="S" ${params?.accountLocked == 'S' ? 'selected' : ''}>Bloqueados</option>
            <option value="N" ${params?.accountLocked == 'N' ? 'selected' : ''}>Desbloqueados</option></select></td>
        <td></td>
        <td><i class="fa fa-recycle cursor-pointer" title="Limpar os filtros" onclick="app.gridReset('${gridId}');"></i>
        </td>
    </tr>

    <tr id="tr${gridId}Columns" data-grid-id="${gridId}">
        <th style="width:60px;">#</th>
        %{--        <th style="width:300px;" class="sorting">Nome</th>--}%
        <th style="width:300px;" class="sorting sorting_asc" data-field-name="noUsuario"
            onclick="app.gridHeaderClick(this)">Nome</th>
        %{--        <th style="width:300px;" class="sorting_desc">Email</th>--}%
        <th style="width:300px;" class="sorting" data-field-name="username"
            onclick="app.gridHeaderClick(this)">Email</th>
        <th style="width:200px;" class="sorting" data-field-name="noInstituicao"
            onclick="app.gridHeaderClick(this)">Instituição</th>
        <th style="width:160px;">Ativada?</th>
        <th style="width:160px;">Bloqueada?</th>
        <th style="width:170px;" class="sorting" data-field-name="dtUltimoAcesso"
            onclick="app.gridHeaderClick(this)">Último acesso</th>
        <th style="width:130px;">Ação</th>
    </tr>
    </thead>
    <tbody>
    <g:if test="${listUsuarios?.size() > 0}">
        <g:each var="item" in="${listUsuarios}" status="i">
            <tr id="tr_${ (i + 1) + (gridId ?: '')}" class="${item.accountLocked ? 'red' : (item.enabled ? 'black' : 'orange')}">
                <td class="text-center">${(i + 1)}</td>
                <td>${item.fullName}</td>
                <td>${item.email}</td>
                <td>${item?.instituicao?.sgInstituicao}</td>
                <td class="text-center">${item.enabled ? 'Sim' : 'Não'}</td>
                <td class="text-center">${item.accountLocked ? 'Sim' : 'Não'}</td>
                <td class="text-center">${item.dtUltimoAcesso ? item.dtUltimoAcesso.format('dd/MM/yyyy HH:mm:ss') : ''}</td>
                <td class="td-actions">
                    <button type="button" data-action="usuarioWeb.alterarSenha" data-no-usuario="${item?.noUsuario}"
                            data-id="${item?.id}" class="btn btn-default btn-xs" data-toggle="tooltip"
                            data-placement="top" title="Redefinir senha">
                        <i class="fa fa-key" aria-hidden="true"></i>
                    </button>


                    <button type="button" data-action="usuarioWeb.desbloquear" ${item.accountLocked ? '' : 'disabled'}
                            data-no-usuario="${item?.noUsuario}" data-id="${item?.id}" class="btn btn-default btn-xs"
                            data-toggle="tooltip" data-placement="top" title="Desbloquear">
                        <i class="fa fa-unlock" aria-hidden="true"></i>
                    </button>
                    <button type="button" data-action="usuarioWeb.bloquear" ${item.accountLocked ? 'disabled' : ''}
                            data-no-usuario="${item?.noUsuario}" data-id="${item?.id}" class="btn btn-default btn-xs"
                            data-toggle="tooltip" data-placement="top" title="Bloquear">
                        <i class="fa fa-lock" aria-hidden="true"></i>
                    </button>


                    <button type="button" data-action="usuarioWeb.excluir" data-no-usuario="${item?.noUsuario}"
                            data-id="${item?.id}" class="btn btn-default btn-xs" data-toggle="tooltip"
                            data-placement="top" title="Excluir da base de dados" style="color:#ff0000;">
                        <i class="fa fa-remove" aria-hidden="true"></i>
                    </button>

                    <button type="button" data-action="usuarioWeb.ativarDesativar"
                            data-no-usuario="${item?.noUsuario}"
                            data-enabled="${item?.enabled ? 'S' : 'N'}" data-id="${item?.id}"
                            class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top"
                            title="${item.enabled ? 'Desativar' : 'Ativar'} conta">
                        <g:if test="${item.enabled}">
                            <i class="fa fa-user green" aria-hidden="true"></i>
                        </g:if>
                        <g:else>
                            <i class="fa fa-user red" aria-hidden="true"></i>
                        </g:else>
                    </button>
                </td>
            </tr>
        </g:each>
    </g:if>
    <g:else>
        <tr>
            <td colspan="7" align="center">
                <h4>Nenhum registro en encontrado!</h4>
            </td>
        </tr>
    </g:else>
    </tbody>
</table>
