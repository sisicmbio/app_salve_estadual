//# sourceURL=N:\SAE\sae\grails-app\views\publicacao\usuarioWeb.js

;var usuarioWeb = {
    sqCicloAvaliacao:'',
    init:function() {
        usuarioWeb.updateGrid();
    },

    /*filtrarTabela:function(evt, ele ) {
        usuarioWeb.updateGrid();
    },
    limparFiltros:function(){
        //clearHeaderFilters('tableUsuarios');
        usuarioWeb.updateGrid();
    },
    */

    /**
     * atualizar o grid com as fichas na situação finalizada
     * @param params
     */
    updateGrid:function( params ) {
        app.grid('GrideUsuariosWeb');
        /*
        var data = app.params2data(params,{});
        var $divGrid = $("#divGridUsuariosWeb");
        var $table = $divGrid.find('table').first();
        var sortData = tableSortFields( 'tableUsuariosHeader' );
        data = $.extend({}, data, getHeaderFilters( 'tableUsuarios' ),sortData );
        var posicao = $divGrid.scrollTop();
        app.blockElement('divGridUsuariosWeb','Carregando...');
        app.ajax(baseUrl + 'usuarioWeb/getGrid',data, function(res) {
            app.unblockElement('divGridUsuariosWeb');
            // ajustar a altura máxima do container do grid
            //var maxHeight = window.innerHeight - 180
            if( $table.size() == 0 ) {
                var wrapperTop = $divGrid.offset().top + 25;
                var maxHeight = window.innerHeight - wrapperTop;
                var maxWidth = $(window).width() - 10;
                $divGrid.css('height', maxHeight);
                $divGrid.css('width', maxWidth);
                $divGrid.css('overflow', 'auto');
                $divGrid.html( res );
                $table = $divGrid.find('table').first();

                // aplicar o plugin floatHead
                $table.floatThead({
                    floatTableClass: 'floatHead',
                    scrollContainer: true,
                    autoReflow: true
                });
                $table.floatThead('reflow');
            } else {
                var $res = $( $('<div style="display:none;">' + res + '</div>') );
                var $body = $res.find('tbody');
                var paginationHtml = $res.find('nav#divGridUsuariosWebPagination');
                $res.remove();
                if( paginationHtml.size() ){
                    var paginationHtml = paginationHtml.html();
                    $('#tableUsuariosPaginationDiv').html( paginationHtml);
                }
                $table.find('tbody').html( $body.html() );
                $table.floatThead('reflow');
            }
        }, '', 'HTML');
        */
    },

    /**
     * exibir o formulário de alteração de senha
     * @param params
     */
    alterarSenha: function( params )
    {
        var $form = $("#frmUsuarioWeb");
        var $usuarioNome = $form.find('#nomeUsuarioEdicao');
        var $id = $form.find('#sqWebUsuario');
        $usuarioNome.html( params.noUsuario );
        $id.val( params.id );
        $form.show();
        $form.find('[name=deSenha]').focus();
    },

    /**
     * limpar e escoder o formulário de alteração de senha
     */
    cancelar: function()
    {
        var $form = $("#frmUsuarioWeb");
        $form.find('#nomeUsuarioEdicao').html('');
        $form.find('#sqWebUsuario').val('');
        $form.find("[name=deSenha]").val('');
        $form.find("[name=deSenha2]").val('');
        $form.hide();
    },

    /**
     * gravar a nova senha
     * @param params
     */
    gravarSenha:function(params)
    {
        var $form = $("#frmUsuarioWeb");
        var $usuarioNome = $form.find('#nomeUsuarioEdicao');
        var $deSenha = $form.find("[name=deSenha]");
        var $deSenha2 = $form.find("[name=deSenha2]");
        if( !$deSenha.val() || !$deSenha2.val())
        {
            app.alertInfo('Informe a senha e a confirmação da senha!');
            return;
        }
        if( $deSenha.val() != $deSenha2.val() )
        {
            app.alertInfo('Senhas não conferem!');
            $deSenha2.focus();
            return;
        }
        var data = {sqWebUsuario : $form.find("#sqWebUsuario").val(), deSenha: $deSenha.val() }
        app.confirm('Confirma alteração da senha de <b>' + $usuarioNome.html() + '</b>?', function() {
            app.ajax(app.url + 'usuarioWeb/alterarSenha', data, function(res) {
                if (res.status === 0) {
                    usuarioWeb.updateGrid();
                    usuarioWeb.cancelar();
                }
            },'Alterando a senha. aguarde...');
        }, null, data, 'Confirmação', 'warning');

    },

    /**
     * desbloquear a conta para poder fazer login no sistema
     * @param params
     */
    desbloquear:function(params)
    {
        app.confirm('Confirma o DESBLOQUEIO do usuário <b>' + params.noUsuario + '</b>?', function() {
            app.ajax(app.url + 'usuarioWeb/desbloquear', params, function( res ) {
                if (res.status === 0) {
                    usuarioWeb.updateGrid();
                }
            },'Desbloqueando. aguarde...');
        }, null, null, 'Confirmação', 'warning');
    },


    /**
     * bloquear a conta para NÃO poder fazer login no sistema
     * @param params
     */
    bloquear:function(params)
    {
      app.confirm('Confirma o BLOQUEIO do usuário <b>' + params.noUsuario + '</b>?', function() {
            app.ajax(app.url + 'usuarioWeb/bloquear', params, function( res ) {
                if (res.status === 0) {
                    usuarioWeb.updateGrid();
                }
            },'Bloqueando. aguarde...');
        }, null, null, 'Confirmação', 'warning');

    },

    /**
     * excluir usuario externo do banco de dados
     * @param params
     */
    excluir:function(params)
    {
      app.confirm('Confirma a EXCLUÇÃO do usuário <b>' + params.noUsuario + '</b>?', function() {
            app.ajax(app.url + 'usuarioWeb/excluir', params, function( res ) {
                if (res.status === 0) {
                    usuarioWeb.updateGrid();
                }
            },'Excluindo. aguarde...');
        }, null, null, 'Confirmação', 'warning');

    },

    /**
     * ativar/desativar a conta do usuário que não conseguiu faze-lo pelo e-mail recebido
     * @param params
     */
    ativarDesativar:function(params)
    {

        app.confirm('Confirma a '+(params.enabled=='S'?'DESATIVAÇÃO':'ATIVAÇÃO') + ' da conta do usuário <b>' + params.noUsuario + '</b>?', function() {
            app.ajax(app.url + 'usuarioWeb/ativarDesativar', params, function( res ) {
                if (res.status === 0) {
                    usuarioWeb.updateGrid();
                }
            },'Processando. aguarde...');
        }, null, null, 'Confirmação', 'warning');
    },



};

// inicializar
window.setTimeout(function(){usuarioWeb.init();},1000)

/*
function getHeaderFilters( tableId ){
    var filters = {}
    $('#'+tableId+'Filters *.table-filter').each( function( i, it ){
        var $field = $(it);
        var data = $field.data();
        if( $field.val() && data.field ) {
            filters[data.field] = $field.val();
        }

    })
   return filters;
}
*/
/*
function clearHeaderFilters( tableId ){
    $('#'+tableId+'Filters *.table-filter').each( function( i, it ){
        var $field = $(it);
        var data = $field.data();
        if( $field.val() && data.field ) {
            $field.val('');
        }
    })
}
*/
/*
function tableHeaderClick( th ) {
    var $th=$(th);
    var $tr=$th.parent();
    var data=$th.data();
    var nextOrder='';
    // calcular a próxima ordem
    if( $th.hasClass('sorting_asc') ) {
        nextOrder = 'sorting_desc';
    } else {
        nextOrder = 'sorting_asc';
    }
    // limpar a ordem atual de todas as colunas
    $tr.find('th.sorting').removeClass('sorting_asc sorting_desc');

    // aplicar o indicador na coluna clicada
    $th.removeClass('sorting_asc sorting_desc').addClass(nextOrder);
    $th.addClass( nextOrder );
    app.grid( $tr.data('gridId') );

}
*/

/*
function tableSortFields( tableHeaderId ) {
    // encontrar as colunas que estão ordenadas / possuem as classes sorting_asc ou sorting_desc
    var result = []
    var data;
    $( "#" + tableHeaderId + " th.sorting_asc").each( function( i, th ) {
        data = $(th).data();
        if( data.fieldName ) {
           result.push( data.fieldName+':asc' );
        }
    });
    $( "#" + tableHeaderId + " th.sorting_desc").each( function( i, th ) {
        data = $(th).data();
        if( data.fieldName ) {
            result.push( data.fieldName+':desc' );
        }
    });
    return { sortFields:result.join(',') };
}
*/
