<div id="usuarioWebContainer">
   <div class="row">
      <div class="col-sm-12">
         <div class="panel panel-default">
            <div class="panel-heading">
               <span>Manutenção Usuários Consulta Ampla/Direta</span>
               <a id="btnClose" data-action="app.unloadModule" class="btn btn-default pull-right btn-xs" title="Fechar">
               <span class="glyphicon glyphicon-remove btnClose"></span>
               </a>
            </div>
            <div class="panel-body" id="fichaContainerBody">

                <form id="frmUsuarioWeb" class="form-inline" style="display:none;" role="form">
                    <input type="hidden" name="sqWebUsuario" id="sqWebUsuario" value=""/>
                    <fieldset>
                        <legend>Redefinir senha</legend>
                        <h4 style="margin-bottom:0px;" id="nomeUsuarioEdicao"></h4>
                        <div class="form-group">
                            <label class="control-label">Nova senha</label><br/>
                            <input type="password" name="deSenha" class="form-control">
                        </div>

                        <div class="form-group">
                            <label class="control-label">Redigite a senha</label><br/>
                            <input type="password" name="deSenha2" class="form-control">
                        </div>
                        <br/>
                        <div class="form-group mt10">
                            <button type="button" name="btnGravar" data-action="usuarioWeb.gravarSenha" class="btn btn-primary">Gravar</button>
                            <button type="button" name="btnCancelar" data-action="usuarioWeb.cancelar" class="btn btn-default">Cancelar</button>
                        </div>
                    </fieldset>
                </form>

                %{-- gride para selecionar usuario --}%
%{--                <div id="divGridUsuariosWeb" class="table-wrapper" style="height:600px;overflow:auto;"></div>--}%
                <div id="containerGrideUsuariosWeb" data-url="usuarioWeb/getGrid">
                    <g:render template="divGridUsuarios" model="[gridId:'GrideUsuariosWeb']"></g:render>
                </div>

            </div>
            %{-- fim panel body --}%
         </div>
    %{-- fim panel --}%
    </div>
%{-- fim row 12 --}%
</div>
%{-- fim container --}%
