<!-- view: /views/relatorio/rel002.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<div id="rel002Container">
   <div class="row">
      <div class="col-sm-12">
         <div class="panel panel-default">
            <div class="panel-heading">
               <span>Relatório de Oficinas</span>
               <a id="btnClose" data-action="app.unloadModule" class="btn btn-default pull-right btn-xs" title="Fechar">
               <span class="glyphicon glyphicon-remove btnClose"></span>
               </a>
            </div>
            <div class="panel-body" id="rel002Body">
                <div id="divRel002Content">
                    <fieldset>
                        <legend style="font-size:1.2em;">
                            <a data-toggle="collapse" href="#formFiltro" class="tooltipstered" style="cursor: pointer; opacity: 1;">
                                <i class="glyphicon glyphicon-filter"></i>&nbsp;Critérios
                            </a>
                        </legend>
                    </fieldset>
                    <form id="formFiltro" class="form-inline panel-collapse collapse in" style="background-color:#d3e0c6" role="form">

                        %{--CRITÉRIOS--}%

                        %{--CICLO DE AVALIACAO--}%
                        <div id="divSelectCicloAvaliacao" class="form-group">
                            <input type="hidden" id="sqCicloAvaliacaoFiltro" name="sqCicloAvaliacaoFiltro" value="${listCicloAvaliacao[0]?.id}"/>
                        </div>

                        %{--OFICINA--}%
                        %{--<div id="divSelectCicloAvaliacao" class="form-group hidden">
                            <label for="sqOficinaFiltro" class="control-label">Oficina</label>
                            <br>
                            <select id="sqOficinaFiltro" name="sqOficinaFiltro"
                                    class="form-control select2 fld600"
                                    data-s2-params="sqCicloAvaliacaoFiltro"
                                    data-s2-url="relatorio/select2Oficina"
                                    data-s2-placeholder="?"
                                    data-s2-minimum-input-length="0"
                                    data-s2-auto-height="true">
                            </select>
                        </div>--}%

                        %{-- PERIODO --}%
                        <div class="form-group">
                            <label for="dtInicioFiltro"
                                   title="Informe:<br> - data incial e final para pesquisar oficinas que ocorreram no período;<br> - somente a data inicial para filtrar as oficinas que ocorreram a partir da data informada;<br> - somente a data final para filtrar as oficinas que ocorreram até a data informada."
                                   class="control-label">Período entre</label>
                            <br/>
                            <input value="" type="text" name="dtInicioFiltro" id="dtInicioFiltro" class="form-control date fldDate"/>
                            <span> a </span>
                            <input value="" type="text" name="dtFimFiltro" id="dtFimFiltro" class="form-control date fldDate"/>
                        </div>


                        <div class="fld panel-footer">
                            <button type="button" id="btnPrint" data-action="rel002.print"
                                  class="fld btn btn-success">Gerar relatório</button>
                        </div>

                    </form>
                    %{-- GRIDE --}%
                     %{--<div id="divGrid"></div>--}%
                    <div id="divGrid" style="padding:5px;width:100%;height:auto;overflow:auto;border:0px;">
                </div> %{-- fim panel body --}%
            </div> %{-- fim panel --}%
      </div> %{-- fim col-12 --}%
   </div>  %{-- fim row --}%
</div>%{-- fim container --}%
