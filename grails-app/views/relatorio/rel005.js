//# sourceURL=rel005.js
;
var rel005 = {
    sqCicloSelecionado:0,
    filtrosLoaded:false,
    dataTable:null,
    init: function() {
        //console.log( 'rel005.js carregado.')
        setTimeout(function() {
            rel005.sqCicloChange();
        },1000);
    },
    reset:function(params,btn,event) {
        app.clearFilters(params,btn)
        $("select[name=comSemPendenciaFiltro]").val('T');
        rel005.updateGrid();
    },
    mostrarEspecies:function(params) {
        if (!params.sqOficina) {
            return;
        }
        var data = app.params2data(params, {formato:'grideEspeciesPendencia',winId : params.sqOficina+'-'+params.sqUnidadeOrg } )

        app.panel('pnlShowEspeciesRel005-' + data.winId
            , 'Espécies com pendências'
            , 'relatorio/rel005'
            , data, function () {
                /*
                var datatable = $('#tableRel005Pendencias').DataTable( $.extend({}, default_data_tables_options,{})).columns.adjust();
                var wh = window.innerHeight;
                var ot = $("#divGrid").offset().top+20;
                if( wh > ot ) {
                    $("#divGrid").height(wh - ot);
                };*/
                // aplicar plugin Datatable na tag table
                setTimeout( function() {
                    $('#tableRel005Pendencias-'+data.winId).DataTable($.extend({}, default_data_tables_options,{
                        "columnDefs": [
                            { "targets": [0], "searchable": false, "orderable": false},
                        ],
                        "searching": true,
                        "order": [[ 1, 'asc' ]]
                    })).columns.adjust();

                },1000);

        },800,600,true,false,null,'center','#384b38',false,false)
    },
    sqCicloChange:function(params)
    {
        rel005.sqCicloSelecionado = $("#sqCicloAvaliacao").val();
        $("#divRel005Content #divGrid").html('');
        if( rel005.sqCicloSelecionado )
        {
            rel005.filtrosLoaded = false;
            rel005.loadFiltros();
            rel005.updateGrid();
        }
        else
        {
            $("#divRel005Content").hide();
        }
    },
    loadFiltros:function( params )
    {
        if( ! rel005.filtrosLoaded ) {
            rel005.filtrosLoaded = true;
            var data = { idRel:'rel005', sqCicloAvaliacao: rel005.sqCicloSelecionado, tipoOficina:'VALIDACAO' };
            app.loadModule(baseUrl + 'relatorio/getCamposFiltro', data, 'formFiltro', function () {});
        }
        $("#divRel005Content").show();
    },
    filtroNivelTaxonomicoChange:function(params,element,event) {
        if (typeof event == 'undefined') {
            return;
        }
        var selectPai = $(event.target);
        var selectFilho = $("#sqTaxonFiltro");
        selectFilho.empty();
        if ($(selectPai).val()) {
            $(selectFilho).parent().removeClass('hide');
        } else {
            $(selectFilho).parent().addClass('hide');
        }
    },
    updateGrid:function()
    {
        $("#divRel005Content #divGrid").html('<h3>Gerando relatório. Aguarde...'+window.grails.spinner+'</h3>');
        var data = {formato:'html'};
        data = $.extend( data,rel005.getFiltros() );
        if ( ! data.sqCicloAvaliacao ) {
            app.alertInfo('Selecione o ciclo de avaliação!');
            return;
        }
        app.ajax('relatorio/rel005', data, function(res) {
            $("#divRel005Content #divGrid").html(res);


            rel005.datatable = $('#tableRel005').DataTable($.extend({}, default_data_tables_options,{})).columns.adjust();
            var wh = window.innerHeight;
            var ot = $("#divGrid").offset().top+20;
            if( wh > ot ) {
                $("#divGrid").height(wh - ot);
            };
            // aplicar plugin Datatable na tag table
            $('#tableRel005').DataTable($.extend({}, default_data_tables_options,{
                "columnDefs": [
                    { "targets": [0,8,9], "searchable": false, "orderable": false},
                ],
                "order": [[ 1, 'asc' ]]
            })).columns.adjust();
        }, null,'HTML');

    },
    csv: function(params) {
        var data = {formato:'csv'};
        data = $.extend( data,rel005.getFiltros() );
        if ( ! data.sqCicloAvaliacao) {
            app.alertInfo('Selecione o ciclo de avaliação!');
            return;
        }
        app.ajax('relatorio/rel005', data, function(res) {
            if (res.fileName) {
                window.open(app.url + 'main/download?fileName=' + res.fileName + '&delete=1', '_top', "width=420,height=230,scrollbars=no,status=1");
            }
        }, null, 'JSON');
    },
    pdf: function(params) {
        var data = {formato:'pdf'};
        data = $.extend( data,rel005.getFiltros() );
        if ( ! data.sqCicloAvaliacao) {
            app.alertInfo('Selecione o ciclo de avaliação!');
            return;
        }
        app.ajax('relatorio/rel005', data, function(res) {
            if (res.fileName) {
                window.open(app.url + 'main/download?fileName=' + res.fileName + '&delete=1', '_top', "width=420,height=230,scrollbars=no,status=1");
            }
        }, null, 'JSON');
    },
    getFiltros:function()
    {
        var filtros = { sqCicloAvaliacao : rel005.sqCicloSelecionado  }
        $("#formFiltro select").each( function(i,campo) {
            var $campo = $(campo)
            if( $campo.val() )
            {
                filtros[campo.name] = $campo.val();
            }
        } );
        return filtros;
    }

};
rel005.init();
