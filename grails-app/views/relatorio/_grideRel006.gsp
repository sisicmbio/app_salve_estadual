<!-- view: /views/relatorio/_grideRel006.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<table id="table${gridId}" class="table table-striped table-sorting">
    <thead>
    %{-- PAGINACAO --}%
    <tr id="tr${gridId}Pagination">
        <td colspan="130">
            <div class="text-left" style="min-height:0;height: auto;overflow: hidden" id="div${(gridId ?: pagination.gridId)}PaginationContent">
                <g:if test="${pagination?.totalRecords}">
                    <g:gridPagination  pagination="${pagination}"/>
                </g:if>
            </div>
        </td>
    </tr>

    %{--    TITULOS--}%
    <tr id="tr${gridId}Columns" data-grid-id="${gridId}">

        <th style="width:60px;">#</th>

        <th style="min-width:200px;width:200px;" class="sorting sorting_asc" data-field-name="nm_cientifico">Taxon</th>
        <th style="min-width:150px;width:130px;" class="sorting" data-field-name="no_grupo">Grupo avaliado</th>
        <th style="min-width:200px;width:200px;" class="sorting" data-field-name="sg_unidade_org">Unidade responsável</th>

        %{-- AVALIAÇÃO --}%
        <th style="min-width:200px;width:200px;" class="sorting" data-field-name="no_oficina_avaliacao">Oficina de avaliação</th>
        <th style="min-width:120px;width:200px;" class="sorting" data-field-name="ds_categoria_avaliacao">Categoria da avaliação</th>
        <th style="min-width:120px;width:200px;" class="sorting" data-field-name="ds_criterio_avaliacao">Critério da avaliação</th>

        %{-- VALIDAÇÃO --}%
        <th style="min-width:200px;width:200px;" class="sorting" data-field-name="no_oficina_validacao">Oficina de validação</th>
        <th style="min-width:200px;width:160px;" class="sorting" data-field-name="ds_situacao_validacao">Situação na validação</th>
        <th style="min-width:200px;width:160px;" class="sorting" data-field-name="dt_validacao">Data da valiação</th>
        <th style="min-width:200px;width:200px;" class="sorting" data-field-name="ds_categoria_validacao">Categoria da validação</th>
        <th style="min-width:200px;width:200px;" class="sorting" data-field-name="ds_criterio_validacao">Critério da validação</th>

        %{-- VALIDADOR 1--}%
        <th style="min-width:200px;width:200px;" class="sorting" data-field-name="no_validador_1">Validador 1</th>
        <th style="min-width:200px;width:200px;" class="sorting" data-field-name="ds_resposta_validador_1">Resposta validador 1</th>

        %{-- VALIDADOR 2--}%
        <th style="min-width:200px;width:200px;" class="sorting" data-field-name="no_validador_2">Validador 2</th>
        <th style="min-width:200px;width:200px;" class="sorting" data-field-name="ds_resposta_validador_2">Resposta validador 2</th>

        %{-- VALIDADOR 3--}%
        <th style="min-width:200px;width:200px;" class="sorting" data-field-name="no_validador_3">Validador 3</th>
        <th style="min-width:200px;width:200px;" class="sorting" data-field-name="ds_resposta_validador_3">Resposta validador 3</th>

        %{-- PONTO FOCAL--}%
        <th style="min-width:200px;width:200px;" class="sorting" data-field-name="no_ponto_focal">Ponto focal</th>

        %{-- COORDENADOR TAXON --}%
        <th style="min-width:200px;width:200px;" class="sorting" data-field-name="no_coordenador_taxon">Coordenador de taxon</th>

    </tr>
    </thead>
    <tbody>
    <g:if test="${rows}">


    <g:each var="row" in="${rows}" status="i">
        <tr id="tr-${row.sq_ficha}">
            %{-- COLUNA NUMERAÇÃO --}%
            <td class="text-center">
                ${ i + (pagination ? pagination?.rowNum.toInteger() : 1 ) }
            </td>

            %{-- COLUNA TAXON--}%
            <td class="text-left">
                ${ raw( row.nm_cientifico ) }
            </td>

            %{-- COLUNA GRUPO AVALIADO --}%
            <td class="text-center">
                ${ row.no_grupo }
            </td>

            %{-- COLUNA UNIDADE RESPONSAVEL --}%
            <td class="text-center">
                ${ row.sg_unidade_org }
            </td>

            %{-- COLUNA NOME OFICINA AVALIACAO --}%
            <td class="text-left">
                ${ row.no_oficina_avaliacao }
            </td>

            %{-- COLUNA CATEGORIA AVALIACAO --}%
            <td class="text-center">
                ${ row.ds_categoria_avaliacao }
            </td>

            %{-- COLUNA CRITERIO AVALIACAO --}%
            <td class="text-center">
                ${ row.ds_criterio_avaliacao }
            </td>


            %{-- COLUNA NOME OFICINA VALIDACAO --}%
            <td class="text-left">
                ${ row.no_oficina_validacao }
            </td>

            %{-- COLUNA SITUACAO FICHA NA VALIDACAO --}%
            <td class="text-left">
                ${ row.ds_situacao_validacao }
            </td>

            %{-- COLUNA DATA DA VALIDACAO --}%
            <td class="text-left">
                ${ row.dt_validacao }
            </td>


            %{-- COLUNA CATEGORIA VALIDACAO --}%
            <td class="text-center">
                ${ row.ds_categoria_validacao }
            </td>

            %{-- COLUNA CRITERIO VALIDACAO --}%
            <td class="text-center">
                ${ row.ds_criterio_validacao }
            </td>

            %{-- COLUNA NOME VALIDADOR 1 --}%
            <td class="text-center">
                ${ row.no_validador_1 }
            </td>
            %{-- COLUNA RESPOSTA VALIDADOR 1 --}%
            <td class="text-center">
                ${ raw(row.ds_resposta_validador_1) }
            </td>

            %{-- COLUNA NOME VALIDADOR 2 --}%
            <td class="text-center">
                ${ row.no_validador_2 }
            </td>
            %{-- COLUNA RESPOSTA VALIDADOR 2 --}%
            <td class="text-center">
                ${ raw(row.ds_resposta_validador_2) }
            </td>

            %{-- COLUNA NOME VALIDADOR 3 --}%
            <td class="text-center">
                ${ row.no_validador_3 }
            </td>
            %{-- COLUNA RESPOSTA VALIDADOR 3 --}%
            <td class="text-center">
                ${ raw(row.ds_resposta_validador_3) }
            </td>

            %{-- COLUNA NOME DO PONTO FOCAL --}%
            <td class="text-center">
                ${ raw(row.no_ponto_focal) }
            </td>

            %{-- COLUNA NOME DO(s) COORDENADOR(ES) DE TAXON --}%
            <td class="text-center">
                ${ raw(row.no_coordenador_taxon) }
            </td>


        </tr>
        </g:each>
    </g:if>
    <g:else>
        <g:if test="${errorMessage}">
            <td colspan="100" class="text-center"><h4 class="red"><b>${errorMessage}</b></h4></td>
        </g:if>
        <g:else>
            <g:if test="${params.gridId}">
                <tr><td colspan="100"><div class="alert alert-info"><b>Nenhum registro encontrado</b></div></td></tr>
            </g:if>
            <g:else>
                <tr><td colspan="100"><div style="height:${ ( gridHeight ?: '200')+'px'}"><b>Informe o(s) filtro(s) e clique em Aplicar ou Exportar</b></div>
                </td></tr>
            </g:else>
        </g:else>


    </g:else>
    </tbody>
</table>
