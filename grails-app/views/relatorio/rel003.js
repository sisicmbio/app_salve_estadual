//# sourceURL=rel003.js
;
var rel003 = {
    sqCicloAvaliacaoFiltro:null,
    relId:'',
    init: function() {
        setTimeout(function(){
            rel003.relId = $("#rndIdFiltro").val();
            rel003.reset();

            if( !appAlertInfoRel003 ) {
                app.alertInfo("As informações deste relatório NÃO são mais atualizadas em tempo real. Os dados extraídos serão referentes às movimentações realizadas até a data de ontem.<br><br>Atenciosamente,<br>Equipe SALVE."
                    , "A T E N Ç Ã O");
            }
            appAlertInfoRel003=true;

            app.restoreField('sqCicloAvaliacaoFiltro', rel003.sqCicloAvaliacaoFiltroChange);
            $('#nmCientificoFiltro').bind('paste', null,app.inputPaste);
        },600);
    },
    reset:function(){
        app.reset('formFiltro','rndIdFiltro,sqCicloAvaliacaoFiltro',true);
        $( document ).scrollTop(0);
        app.focus('nmCientificoFiltro');
        $("#radORUcs").prop('checked',true);
        $("#radOREstados").prop('checked',true);
        $("#radORBiomas").prop('checked',true);
        $("#radORAmeacas").prop('checked',true);
        $("#radORUsos").prop('checked',true);

    },
    print:function(params) {
        var filtros = rel003.getFiltros()
        // ExportAs:
        // html = gride
        // rows = planilha da consulta realizada
        // regs = planila com os registros de ocorrencia
        filtros.exportAs = ( params.exportAs ? params.exportAs : 'HTML'); // rows, regs
        filtros.marcarDesmarcarModuloPublico = params.marcarDesmarcarModuloPublico || ''
        var mensagemAjax = filtros.marcarDesmarcarModuloPublico ? 'Atualizando ficha(s). Aguarde...' : 'Consultando banco de dados...';
        if( filtros.exportAs == 'HTML' ) {

            $(document).scrollTop(1615.5555419921875);
                app.grid("GridRel003", filtros, function (res) {
                    $(document).scrollTop(1615.5555419921875);
                    app.unblockUI();
                    beep();
                },mensagemAjax);

        } else if( filtros.exportAs == 'ROW') {
            rel003.selectColumnsTaxons();
        } else if( filtros.exportAs == 'REG') {
            rel003.selectColumnsRegistros();
        }
    },
    sqCicloAvaliacaoFiltroChange:function(params) {
        rel003.sqCicloAvaliacaoFiltro = $("#sqCicloAvaliacaoFiltro").val();
        app.saveField('sqCicloAvaliacaoFiltro');
    },
    getFiltros:function() {
        var filtros = {print:true};
        $("#formFiltro").find('input,select').each(function (i,ele) {
            if( $(ele).val() )
            {
                if( ele.type == 'checkbox' ){
                    if( ele.checked ) {
                        filtros[ele.name] = $(ele).val();
                    }
                } else if( ele.type == 'radio' ){
                    if( ele.checked ) {
                        filtros[ele.name] = $(ele).val();
                    }
                }
                else {
                    if (typeof $(ele).val() == 'object') {
                        if( $(ele).hasClass('select2') )
                        {
                            var arrTemp=[];
                            $(ele).select2('data').forEach(function(option){
                                if( option.sqUc ) {
                                    //arrTemp.push(option.sqUc + (option.esfera ? option.esfera : '') );
                                    arrTemp.push(option.id);
                                } else {
                                    arrTemp.push(option.id);
                                };
                            });
                            filtros[ele.name] = arrTemp.join(',');
                        }
                        else {
                            filtros[ele.name] = $(ele).val().join(',');
                        }
                    } else {
                        filtros[ele.name] = $(ele).val();
                    }
                }
            }
        });
        return filtros;
    },
    sqGrupoChange:function( evt )
    {
        $('#sqSubgrupoFiltro').val(null).trigger('change');
        if( $('#sqGrupoFiltro').val() ) {
            //$("#divSubgrupoFiltro").show();
            $("#sqSubgrupoFiltro").prop('disabled',false);
        } else {
            //$("#divSubgrupoFiltro").hide();
            $("#sqSubgrupoFiltro").prop('disabled',true);
        }
    },
    selectColumnsRegistros:function(){
        var maxHeight = Math.min(685, ( window.innerHeight-50 ) );
        var maxWidth = Math.min(1024, ( $(window).width()-20 ) );
        var data = {};
        /*if( $("#xxxx") ) {
            app.alertInfo('Nenhum registro encontrado.')
            return;
        }*/
        data.height = maxHeight;
        data.reload = false;
        data.modalName = 'selecionarColunasExportacaoRegistros';
        app.openWindow({
            id: 'modalSelecionarColunasExportacaoRegistros',
            url: app.url + 'main/getModal',
            width: maxWidth,
            data: data,
            height: maxHeight,
            title: 'Selecione a(s) coluna(s) para exportação dos registros de ocorrências',
            autoOpen: true,
            modal: true
        }, function (data, e) {
            // onShow
            $("#divBodysSlecionarColunasExportacaoRegistros").css('height', (maxHeight-143)+'px');
            //data = data.data;
            $("#btnExportarColunasSelecionadasRegistros").off('click').on('click',function(){
                var colsSelected = ['no_cientifico','nu_latitude','nu_longitude'];
                var allCheckboxes = $("#frmselecionarColunasExportacaoRegistros").find('input[type=checkbox]');
                //$("#frmselecionarColunasExportacaoRegistros").find('input[type=checkbox]:checked').map(function( i, input ){
                allCheckboxes.map( function( i, input ) {
                    if( input.checked ) {
                        colsSelected.push(input.value);
                    }
                });
                if( colsSelected.length == 0 ){
                    app.alertInfo('Nenhuma coluna selecionada');
                    return;
                }
                // gravar no local storage se não tiver selecionado todos
                if( colsSelected.length < allCheckboxes.length) {
                    app.lsSet('rel003ColsSelected', colsSelected.join(','));
                }
                app.closeWindow('modalSelecionarColunasExportacaoRegistros');
                rel003.exportOcorrencias( {colsSelected:colsSelected} );
            });
            // recuperar a última seleção do local storage
            var lastSelected = app.lsGet('rel003ColsSelected','')
            if( lastSelected != '' ){
                $("#frmselecionarColunasExportacaoRegistros").find('input[type=checkbox]').prop('checked',false);
                lastSelected.split(',').map(function(item){
                    $("#frmselecionarColunasExportacaoRegistros").find('input[type=checkbox][value='+item+']').prop('checked',true);
                });
            }
        }, function (data, e) {
            // onClose;


        });
    },

    exportOcorrencias:function(data){
        data = data || {}
        data.exportAs               = 'REG';
        data.relId                  = rel003.relId;
        data.exportarOcorrencias    = 'S';
        data.enviarEmail            = true;
        data.dataTempFile           = 'rel_taxon_analitico_'+rel003.relId+'.json';
        data.showCheckCarencia      = true;
        data.showCheckSensivel      = true;
        data.colsSelected           = data.colsSelected || [];
        data = $.extend(data,rel003.getFiltros());

        if( window.grails.perfilExterno || (/CT|CI/.test(window.grails.coPerfil ) ) ) {
            data.showCheckCarencia=false;
            data.showCheckSensivel=false;
        }
        /*if( rel003.dataTable.data().length==0){
            app.alertInfo('Nenhum registro encontrado.')
            return;
        }*/


        // verificar se existe pelo menos um registro marcado como sensivel para mostrar o campo checkbox na janela de dialogo
        $.ajax({
            url: app.url + 'ficha/hasSensitiveOccurrence'
            , type: 'POST'
            , cache: false
            , timeout: 0
            , dataType: 'json'
            , data: { dataTempFile : data.dataTempFile, idsType:'taxon' }
        }).done(function ( res ) {

            data.sensivel = res.data.result;
            data.colsSelected = data.colsSelected.join(',');
            app.dialogoExportarOcorrencias(data,function( data ) {
                //app.growl('Exportação em andamento...',3,'','tc',null,'info')
                app.ajax(app.url + 'relatorio/rel003', data, function(res) {
                    if( res.status == 0 ) {
                        setTimeout(function(){getWorkers();},3000);
                    }
                });
            });
        });
    },

    selectColumnsTaxons:function(){
        var maxHeight = Math.min(500, ( window.innerHeight-50 ) );
        var maxWidth = Math.min(1024, ( $(window).width()-20 ) );
        var data = {};
        /*if( $("#xxxx") ) {
            app.alertInfo('Nenhum registro encontrado.')
            return;
        }*/
        data.height = maxHeight;
        data.reload = false;
        data.modalName = 'selecionarColunasExportacaoTaxons';
        app.openWindow({
            id: 'modalSelecionarColunasExportacaoTaxons',
            url: app.url + 'main/getModal',
            width: maxWidth,
            data: data,
            height: maxHeight,
            title: 'Selecione a(s) coluna(s) para exportação',
            autoOpen: true,
            modal: true
        }, function (data, e) {
            // onShow
            $("#divBodysSlecionarColunasExportacaoTaxons").css('height', (maxHeight-143)+'px');
            //data = data.data;
            $("#btnExportarColunasSelecionadasTaxons").off('click').on('click',function(){

                var colsSelected = ['no_taxon'];
                var allCheckboxes = $("#frmselecionarColunasExportacaoTaxons").find('input[type=checkbox]');
                //$("#frmselecionarColunasExportacaoRegistros").find('input[type=checkbox]:checked').map(function( i, input ){
                allCheckboxes.map( function( i, input ) {
                    if( input.checked ) {
                        colsSelected.push(input.value);
                    }
                });
                if( colsSelected.length == 0 ){
                    app.alertInfo('Nenhuma coluna selecionada');
                    return;
                }
                // gravar no local storage se não tiver selecionado todos
                if( colsSelected.length < allCheckboxes.length) {
                    app.lsSet('rel003ColsSelectedTaxons', colsSelected.join(','));
                }
                app.closeWindow('modalSelecionarColunasExportacaoTaxons');
                rel003.exportTaxons( {colsSelected:colsSelected} );
            });
            // recuperar a última seleção do local storage
            var lastSelected = app.lsGet('rel003ColsSelectedTaxons','')
            if( lastSelected != '' ){
                $("#frmselecionarColunasExportacaoTaxons").find('input[type=checkbox]').prop('checked',false);
                lastSelected.split(',').map(function(item){
                    $("#frmselecionarColunasExportacaoTaxons").find('input[type=checkbox][value='+item+']').prop('checked',true);
                });
            }
        }, function (data, e) {
            // onClose;
        });
    },

    exportTaxons:function( params ){
        var data = { colsSelected : params.colsSelected || [], email:params.email || '' };
        if( ! params.dlgConfirm ) {
            return app.confirmWithEmail('Confirma a exportação?',rel003.exportTaxons, data );
        }
        data = $.extend(data,rel003.getFiltros());
        data.exportAs = 'ROW'
        data.format='row';
        data.colsSelected = data.colsSelected.join(',');
        app.blockUI('Consultando banco de dados...',function(){
            app.grid("GridRel003", data, function (res) {
                app.unblockUI();
            });
        })
    },
    chkExibirModuloPublicoChange:function( ele ){
        var stExibir = ele.checked;
        var sqFicha = ele.value;
        var data ={stExibir:stExibir,sqFicha:sqFicha};
        $.ajax({
            url: app.url + 'ficha/marcarDesmarcarModuloPublico'
            , type: 'POST'
            , cache: false
            , timeout: 0
            , dataType: 'json'
            , data: data
        }).done(function ( res ) {

        });
    },
    marcarTodasAsFichasModuloPublico:function(){
        app.confirm('Confirma a <b>MARCAÇÃO</b> de TODAS as fichas para serem exibidas no módulo público?',function(){
            rel003.print({marcarDesmarcarModuloPublico:'s'})
        })
    },
    desmarcarTodasAsFichasModuloPublico:function(){
        app.confirm('Confirma a <b>DESMARCAÇÃO</b> de TODAS as fichas para serem exibidas no módulo público?',function(){
            rel003.print({marcarDesmarcarModuloPublico:'n'})
        })

    },

};
rel003.init();
