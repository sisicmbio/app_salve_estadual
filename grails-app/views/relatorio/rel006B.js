//# sourceURL=rel006.js
;
var rel006 = {
    sqCicloAvaliacaoFiltro:null,
    filtrosLoaded:false,
    starting:true, // flag para não disparar a consulta ao abrir a página
    scrollY:400,
    dataTable:null,
    init: function() {
        rel006.sqCicloAvaliacaoFiltroChange()
        rel006.print() // inicializar o plugin datatable no gride
    },
    clear:function() {},
    sqCicloAvaliacaoFiltroChange:function(params) {
        rel006.sqCicloAvaliacaoFiltro = $("#sqCicloAvaliacaoFiltro").val();
        if( ! rel006.sqCicloAvaliacaoFiltro ){
            $("#divGrid,#divFiltrosRel006Wrapper").hide();
        } else {
            $("#divGrid,#divFiltrosRel006Wrapper").show();
        }
        rel006.getOficinasValidacaoAjax();
    },
    getFiltros:function() {
        var filtros = {print:true,starting:rel006.starting ? 'S' : 'N'};
        rel006.starting=false;
        $("#formFiltro").find('input,select').each(function (i,ele) {
            if( $(ele).val() )
            {
                if( ele.type == 'checkbox' )
                {
                    if( ele.checked ) {
                        filtros[ele.name] = $(ele).val();
                    }
                }
                else {
                    if (typeof $(ele).val() == 'object') {
                        if( $(ele).hasClass('select2') )
                        {
                            var arrTemp=[];
                            $(ele).select2('data').forEach(function(option){
                                if( option.sqUc ) {
                                    arrTemp.push(option.sqUc + (option.esfera ? option.esfera : '') );
                                }
                                else
                                {
                                    arrTemp.push(option.id);
                                };
                            });
                            filtros[ele.name] = arrTemp.join(',');
                        }
                        else {
                            filtros[ele.name] = $(ele).val().join(',');
                        }
                    } else {
                        filtros[ele.name] = $(ele).val();
                    }
                }
            }
        });
        return filtros;
    },

    print:function() {
        // posicionar o gride das fichas no topo da tela
        var posicao = Math.max(window.innerHeight - $("#divGrid").offset().top, 305);
        var gridHeight = Math.max(window.innerHeight - 305, 305);

        if( rel006.dataTable)
        {
            rel006.dataTable.destroy(false);
        }
        rel006.dataTable = $('#tableRel006').DataTable( $.extend({}, default_data_tables_options, {
            "paging"           : true
            ,"pageLength"       : 100
            ,"deferRender"      : true
            ,"scrollY"          : gridHeight //rel006.scrollY+'px'
            ,"scrollX"          : true
            ,"scrollCollapse"   : true
            ,"fixedHeader"      : false
            ,"lengthChange"     : false
            ,"processing"       : false
            ,"ajax"             : { "url": "relatorio/rel006"
                ,"type": "POST"
                ,"data": function( data ) {
                    $.extend( data, rel006.getFiltros() );
                },
            },
            "columnDefs": [
                // "targets": "_all"
                { "targets": [0], "searchable": false, "orderable": false},
                { "targets": [0,2,3,5,6,8,9,10,11], "className": "text-center"},
            ],
            "order": [[ 1, 'asc' ]]
        }));

        rel006.dataTable.on( 'xhr.dt', function ( e, settings, json ) {
            app.unblockUI();
            if( json && json.errors.length > 0 ){
                app.alertInfo(json.errors.join('<br>'))
            }
            //window.setTimeout(function() { $(document).scrollTop(1027);},500);
        });

    },
    getOficinasValidacaoAjax:function( params ){
        var $select = $("#sqOficinaValidacaoFiltro");
        $select.prop('disabled',true);
        if( ! rel006.sqCicloAvaliacaoFiltro ){
            return;
        }
        var data = {sqCicloAvaliacao:rel006.sqCicloAvaliacaoFiltro}
        app.ajax(app.url + 'relatorio/getOficinaValidacao', data, function (res) {
            if (res.status === 0) {
                $select.prop('disabled',false);
                $select.empty().append('<option selected="selected" value="">-- selecione --</option>')
                res.data.map(function( item ){
                    $select.append(new Option(item.descricao, item.id) )
                    }
                );

            }
        });


    }
};
rel006.init();
