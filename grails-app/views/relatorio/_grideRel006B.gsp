<!-- view: /views/relatorio/_rel006.gsp -->
<!-- controller:${params?.controller} -->
<!-- action: ${params?.action} -->
<div style="margin:0px;" id="divTotalRegistros"><h3><span class="total">0</span> registro(s)</h3></div>
<table id="tableRel006" class="table table-bordered table-condensed table-hover display nowrap" style="width:100%">
    <thead>
        <tr>
            <th style="min-width:50px;">#</th>

            <th style="min-width:200px;">Taxon</th>
            <th style="min-width:120px;">Grupo avaliado</th>
            <th style="min-width:200px">Unidade responsável</th>

            <th style="min-width:200px;">Oficina de avaliação</th>
            <th style="min-width:200px;">Categoria da avaliação</th>
            <th style="min-width:200px;">Critério da avaliação</th>

            <th style="min-width:200px;">Oficina de validação</th>
            <th style="min-width:160px;">Situação na validação</th>
            <th style="min-width:160px;">Data validação</th>
            <th style="min-width:200px;">Categoria da validação</th>
            <th style="min-width:200px;">Critério da validação</th>

            <th style="min-width:200px;">Validador 1</th>
            <th style="min-width:200px;">Resposta validador 1</th>

            <th style="min-width:200px;">Validador 2</th>
            <th style="min-width:200px;">Resposta validador 2</th>

            <th style="min-width:200px;">Validador 3</th>
            <th style="min-width:200px;">Resposta validador 3</th>

            <th style="min-width:200px">Ponto focal</th>
            <th style="min-width:200px;">Coordenador de taxon</th>
        </tr>
    </thead>
</table>
