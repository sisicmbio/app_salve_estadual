<!-- view: /views/relatorio/rel003.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<div id="rel003Container">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <span>Relatório de Táxons - Analítico</span>
                    <a id="btnClose" data-action="app.unloadModule" class="btn btn-default pull-right btn-xs" title="Fechar">
                        <span class="glyphicon glyphicon-remove btnClose"></span>
                    </a>
                </div>
                <div class="panel-body" id="rel003Body" style="padding-left:10px;">
                    <div id="divRel003Content">
                        <fieldset>
                            <legend style="font-size:1.2em;">
                                <a data-toggle="collapse" href="#formFiltro" class="tooltipstered" style="cursor: pointer; opacity: 1;">
                                    <i class="glyphicon glyphicon-filter"></i>&nbsp;Filtros
                                </a>
                            </legend>
                        </fieldset>
                        <form id="formFiltro" class="form-inline panel-collapse collapse in" style="background-color:#d3e0c6;padding:5px;" role="form">

                            <input type="hidden" name='rndIdFiltro' id="rndIdFiltro" value="${rndId ?:''}">

                            %{--CRITÉRIOS--}%

                            %{--CICLO DE AVALIACAO--}%
                            <div id="divSelectCicloAvaliacao" class="form-group">
                                <input type="hidden" id="sqCicloAvaliacaoFiltro" name="sqCicloAvaliacaoFiltro" value="${listCicloAvaliacao[0]?.id}"/>
                            </div>

                            %{--  EXIBIR MÓDULO PÚBLICO--}%
                            <g:if test="${isAdm}">
                                <div class="form-group">
                                    <label for="stExibirModuloPublico" class="control-label">Exibidas no módulo público?</label>
                                    <br>
                                    <select name="stExibirModuloPublico" id="stExibirModuloPublico" class="form-control fld500 fldSelect" placeholder="Todas">
                                        <option value="" selected="selected"></option>
                                        <option value="S">Sim</option>
                                        <option value="N">Não</option>
                                    </select>
                                </div>
                            </g:if>
                            <br>

                            %{-- NOME CIENTIFICO --}%
                            <div class="form-group">
                                <label for="nmCientificoFiltro" class="control-label" title="Informe o nome científico completo ou em parte para selecionar as fichas. Ex: Puma, Puma concolor ou concolor apenas.<br>Tambem pode ser informada uma lista de espécies separadas por vírgula, neste caso, devem ser informados os nomes completos. Ex: Puma concolor, Aburria cujubi">Nome científico</label>
                                <br>
                                <input type="text" id="nmCientificoFiltro" name="nmCientificoFiltro" class="fld form-control fld500 tooltipstered"/>
                            </div>
                            <div class="form-group">
                                <label for="nmComumFiltro" class="control-label" title="Informe o nome comum ou em parte para selecionar as fichas. Ex: Macaco-aranha, Gambazinho">Nome comum</label>
                                <br>
                                <input type="text" id="nmComumFiltro" name="nmComumFiltro" class="fld form-control fld500 tooltipstered"/>
                            </div>

                            <br>

                            %{--<SITUACAO FICHA--}%
                            <div id="divSelectSituacao" class="form-group">
                                <label for="sqSituacaoFiltro" class="control-label">Situação da ficha
                                    <i title="Para selecionar mais de uma opção, mantenha a tecla CTRL pressionada ao clicar na lista de opções" class="label-help glyphicon glyphicon-question-sign blue tooltipstered" style="cursor: pointer;"></i>
                                    <span style="margin-left:50px;display:inline-block">Incluir fichas excluídas&nbsp;<input type="checkbox" id="chkExcluidasFiltro" name="chkExcluidasFiltro" value="S" class="fld form-contro checkbox-lg tooltipstered">
                                        <i title="Quando nenhuma situação é selecionada as fichas EXCLUÍDAS são filtradas automaticamente, marque esta opção para que elas sejam incluídas no resultado." class="label-help glyphicon glyphicon-question-sign blue tooltipstered" style="cursor: pointer;"></i>
                                    </span>
                                </label>
                                <br>
                                <select name="sqSituacaoFiltro" id="sqSituacaoFiltro" multiple="multiple" class="form-control fld500 select2 fldSelect"
                                        data-s2-minimum-input-length="0"
                                        data-s2-multiple="true"
                                        data-s2-maximum-selection-length="30">
                                    <g:each var="item" in="${listSituacaoFicha}">
                                        <option value="${item.id}">${item.descricao}</option>
                                    </g:each>
                                </select>
                            </div>

                            %{-- ENDEMICA --}%
                            <div class="form-group">
                                <label for="stEndemicaFiltro" class="control-label">Espécie endêmica?</label>
                                <br>
                                <select name="stEndemicaFiltro" id="stEndemicaFiltro" class="form-control fld500 fldSelect" placeholder="Todas">
                                    <option value="" selected="selected"></option>
                                    <option value="S">Sim</option>
                                    <option value="N">Não</option>
                                    <option value="D">Desconhecido</option>
                                </select>
                            </div>

                            <br>

                            %{-- FILTRO PELO NIVEL TAXONOMICO --}%

                            <div id="divFiltroTaxon" class="form-group">
                                <label for="nivelFiltro" class="control-label">Nível taxonômico</label>
                                <br>
                                <select id="nivelFiltro" name="nivelFiltro" data-change="app.filtroNivelTaxonomicoChange" data-params="rndId:${rndId},callback:app.btnFiltroFichaClick" class="fld form-control fld200 fldSelect">
                                    <option value="">-- todos --</option>
                                    <g:each var="item" in="${listNivelTaxonomico}">
                                    %{--<option value="${item.coNivelTaxonomico}">${item.deNivelTaxonomico}</option>--}%
                                        <option data-codigo="${item.coNivelTaxonomico}" value="${item.id}">${item.deNivelTaxonomico}</option>
                                    </g:each>
                                </select>
                            </div>

                            <div class="fld form-group">
                                <label for="sqTaxonFiltro${rndId}" class="control-label" title="Para selecionar mais de uma opção, mantenha a tecla CTRL pressionada ao clicar na lista de opções">Taxon</label>
                                <br>
                                <select id="sqTaxonFiltro${rndId}" name="sqTaxonFiltro" data-s2-visible="false" class="fld form-control select2" style="width:295px !important;"
                                        data-s2-params="nivelFiltro|sqNivel"
                                        data-s2-onchange="app.btnFiltroFichaClick"
                                        data-s2-url="ficha/select2TaxonNovo"
                                        data-s2-placeholder="?"
                                        data-s2-multiple="true"
                                        data-s2-maximum-selection-length="20"
                                        data-s2-minimum-input-length="2"
                                        data-s2-auto-height="true">
                                </select>
                            </div>

                        %{--< UNIDADE RESPONSÁVEL--}%
                            <g:if test="${session.sicae.user.isADM() }">
                                <div class="fld form-group">
                                    <label for="sqUnidadeFiltro" class="control-label" title="Para selecionar mais de uma opção, mantenha a tecla CTRL pressionada ao clicar na lista de opções">Unidade</label>
                                    <br>
                                    <select id="sqUnidadeFiltro" name="sqUnidadeFiltro" multiple="multiple" class="form-control select2 fld500"
                                            data-s2-params="sqCicloAvaliacaoFiltro"
                                            data-s2-url="relatorio/select2UnidadeOrg"
                                            data-s2-multiple="true"
                                            data-s2-maximum-selection-length="20"
                                            data-s2-minimum-input-length="2"
                                            data-s2-auto-height="true">
                                    </select>
                                </div>
                            </g:if>


                            <br>
                            <div style="border-top:3px dashed green;max-width:1025px;padding:3px;margin-top:20px">
                                %{-- CATEGORIA AVALIACAO ANTERIOR --}%
                                <div id="divSelectCategoriaHist" class="form-group">
                                    <label for="sqCategoriaHistFiltro" class="control-label" title="Para selecionar mais de uma opção, mantenha a tecla CTRL pressionada ao clicar na lista de opções">Categoria <b>histórico</b></label>
                                    <br>
                                    <select name="sqCategoriaHistFiltro" id="sqCategoriaHistFiltro" multiple="multiple" class="form-control fld500 select2 fldSelect"
                                            data-s2-minimum-input-length="0"
                                            data-s2-multiple="true"
                                            data-s2-maximum-selection-length="30">
                                        <g:each var="item" in="${listCategoria}">
                                            <option value="${item.id}">${item.descricaoCompleta}</option>
                                        </g:each>
                                    </select>
                                </div>

                                %{-- CRITÉRIO 1 --}%
                                <div class="form-group">
                                    <label for="dsCriterioHistFiltro" class="control-label" title="Para selecionar mais de uma opção, mantenha a tecla CTRL pressionada ao clicar na lista de opções">Critério histórico</label>
                                    <br>
                                    <select name="dsCriterioHistFiltro" id="dsCriterioHistFiltro" multiple="multiple" class="form-control fld500 select2 fldSelect"
                                            data-s2-minimum-input-length="0"
                                            data-s2-maximum-selection-length="5"
                                            data-s2-multiple="true">
                                        <option value="A">A. Redução da população</option>
                                        <option value="B">B. Distribuição geográfica restrita</option>
                                        <option value="C">C. Tamanho da população pequeno e com declínio</option>
                                        <option value="D">D. População muito pequena ou distribuição muito restrita</option>
                                        <option value="E">E. Análises quantitativas</option>
                                    </select>
                                    <span> ou exato </span>
                                    <input name="dsCriterioHistExatoFiltro" onkeyup="fldCriterioAvaliacaoKeyUp(this);" id="dsCriterioHistExatoFiltro" readonly type="text" class="fld form-control blue" style="width:200px" value=""/>
                                    <button id="btnSelCriterioAvaliacaoHist" class="fld btn btn-default btn-sm" title="Clique para selecionar o critério de avaliação" data-action="openModalSelCriterioAvaliacao" data-field="dsCriterioHistExatoFiltro" data-params="" data-field-categoria="" data-contexto="rel003Container">...</button>
                                </div>
                            </div>

                            %{-- PEX HISTÓRICO --}%
                            %{--<div class="form-group">
                                <label for="inPexHistFiltro" class="control-label" title="Possivelmente extinta na avaliação anterior.">PEX histórico</label>
                                <br>
                                <select name="inPexHistFiltro" id="inPexHistFiltro" class="form-control fldSelect fld100">
                                    <option value=""></option>
                                    <option value="S">Sim</option>
                                    <option value="N">Não</option>
                                </select>
                            </div>--}%

                            <br>

                            <div style="border-top:3px dashed green;max-width:1025px;padding:3px;" class="mt5">
                                %{-- CATEGORIA OFICINA--}%
                                <div id="divSelectCategoriaOficina" class="form-group">
                                    <label for="sqCategoriaOficinaFiltro" class="control-label" title="Para selecionar mais de uma opção, mantenha a tecla CTRL pressionada ao clicar na lista de opções">Categoria <b>avaliada</b></label>
                                    <br>
                                    <select name="sqCategoriaOficinaFiltro" id="sqCategoriaOficinaFiltro" multiple="multiple" class="form-control fld500 select2 fldSelect"
                                            data-s2-minimum-input-length="0"
                                            data-s2-multiple="true"
                                            data-s2-maximum-selection-length="30">
                                        <g:each var="item" in="${listCategoria}">
                                            <option value="${item.id}">${item.descricaoCompleta}</option>
                                        </g:each>
                                    </select>
                                </div>

                                %{-- CRITÉRIO OFICINA 2 --}%
                                <div class="form-group">
                                    <label for="dsCriterioOficinaFiltro" class="control-label" title="Para selecionar mais de uma opção, mantenha a tecla CTRL pressionada ao clicar na lista de opções">Critério avaliado</label>
                                    <br>
                                    <select name="dsCriterioOficinaFiltro" id="dsCriterioOficinaFiltro" multiple="multiple" class="form-control fld500 select2 fldSelect"
                                            data-s2-minimum-input-length="0"
                                            data-s2-maximum-selection-length="5"
                                            data-s2-multiple="true">
                                        <option value="A">A-Redução da população</option>
                                        <option value="B">B-Distribuição geográfica restrita</option>
                                        <option value="C">C-Tamanho da população pequeno e com declínio</option>
                                        <option value="D">D-População muito pequena ou distribuição muito restrita</option>
                                        <option value="E">E-Análises quantitativas</option>
                                    </select>
                                    <span> ou exato </span>
                                    <input name="dsCriterioOficinaExatoFiltro" onkeyup="fldCriterioAvaliacaoKeyUp(this);" id="dsCriterioOficinaExatoFiltro" readonly type="text" class="fld form-control blue" style="width:200px" value=""/>
                                    <button id="btnSelCriterioAvaliacaoOficina" class="fld btn btn-default btn-sm" title="Clique para selecionar o critério de avaliação" data-action="openModalSelCriterioAvaliacao" data-field="dsCriterioOficinaExatoFiltro" data-params="" data-field-categoria="" data-contexto="rel003Container">...</button>
                                </div>
                            </div>

                            %{-- PEX OFICINA --}%
                            %{--<div class="form-group">
                                <label for="inPexOficina" class="control-label" title="Possivelmente extinta na avaliação.">PEX avaliada</label>
                                <br>
                                <select name="inPexOficina" id="inPexOficina" class="form-control fldSelect fld100">
                                    <option value=""></option>
                                    <option value="S">Sim</option>
                                    <option value="N">Não</option>
                                </select>
                            </div>--}%

                            <br>

                            <div style="border-top:3px dashed green;max-width:1025px;padding:3px;" class="mt5">
                                %{-- CATEGORIA FINAL --}%
                                <div id="divSelectCategoriaFinal" class="form-group">
                                    <label for="sqCategoriaFinalFiltro" class="control-label" title="Para selecionar mais de uma opção, mantenha a tecla CTRL pressionada ao clicar na lista de opções">Categoria <b>validada</b></label>
                                    <br>
                                    <select name="sqCategoriaFinalFiltro" id="sqCategoriaFinalFiltro" multiple="multiple" class="form-control fld500 select2 fldSelect"
                                            data-s2-minimum-input-length="0"
                                            data-s2-multiple="true"
                                            data-s2-maximum-selection-length="30">
                                        <g:each var="item" in="${listCategoria}">
                                            <option value="${item.id}">${item.descricaoCompleta}</option>
                                        </g:each>
                                    </select>
                                </div>

                                %{-- CRITÉRIO FINAL 3 --}%
                                <div class="form-group">
                                    <label for="dsCriterioFinalFiltro" class="control-label" title="Para selecionar mais de uma opção, mantenha a tecla CTRL pressionada ao clicar na lista de opções">Critério validado</label>
                                    <br>
                                    <select name="dsCriterioFinalFiltro" id="dsCriterioFinalFiltro" multiple="multiple" class="form-control fld500 select2 fldSelect"
                                            data-s2-minimum-input-length="0"
                                            data-s2-maximum-selection-length="5"
                                            data-s2-multiple="true">
                                        <option value="A">A. Redução da população</option>
                                        <option value="B">B. Distribuição geográfica restrita</option>
                                        <option value="C">C. Tamanho da população pequeno e com declínio</option>
                                        <option value="D">D. População muito pequena ou distribuição muito restrita</option>
                                        <option value="E">E. Análises quantitativas</option>
                                    </select>
                                    <span> ou exato </span>
                                    <input name="dsCriterioFinalExatoFiltro" onkeyup="fldCriterioAvaliacaoKeyUp(this);" id="dsCriterioFinalExatoFiltro" readonly type="text" class="fld form-control blue" style="width:200px" value=""/>
                                    <button id="btnSelCriterioAvaliacaoFinal" class="fld btn btn-default btn-sm" title="Clique para selecionar o critério de avaliação" data-action="openModalSelCriterioAvaliacao" data-field="dsCriterioFinalExatoFiltro" data-params="" data-field-categoria="" data-contexto="rel003Container">...</button>
                                </div>

                                <g:if test="${ session.sicae.user.isADM() || session.sicae.user.isPF()}">
                                    %{-- PERIODO VALIDACAO --}%
                                    <div class="form-group">
                                        <label for="dtInicioValidacaoFiltro"
                                               title="Informe:<br> - data incial e final para pesquisar validações que ocorreram no período;<br> - somente a data inicial para filtrar as validações que ocorreram a partir da data informada;<br> - somente a data final para filtrar as validações que ocorreram até a data informada."
                                               class="control-label">Período validação</label>
                                        <br/>
                                        <input value="" type="text" name="dtInicioValidacaoFiltro" id="dtInicioValidacaoFiltro" class="form-control date fldDate"/>
                                        <span> a </span>
                                        <input value="" type="text" name="dtFimValidacaoFiltro" id="dtFimValidacaoFiltro" class="form-control date fldDate"/>
                                    </div>
                                </g:if>
                            </div>

                            %{-- PEX FINAL --}%
                            %{--<div class="form-group">
                                <label for="inPexFinal" class="control-label" title="Possivelmente extinta na avaliação final.">PEX validado</label>
                                <br>
                                <select name="inPexFinal" id="inPexFinal" class="form-control fldSelect fld100">
                                    <option value=""></option>
                                    <option value="S">Sim</option>
                                    <option value="N">Não</option>
                                </select>
                            </div>
    --}%
                            <br>


                            %{-- COMPARATIVO CATEGORIA --}%

                            <div style="border-top:3px dashed green; max-width:1025px;padding:2px;" class="mt5">
                                <div class="form-group">
                                    <label for="deComparativoFiltro" class="control-label" title="Comparação com a categoria anterior.">Comparativo</label>
                                    <br>
                                    <select name="deComparativoFiltro" id="deComparativoFiltro" multiple="multiple" class="form-control fld500 select2 fldSelect"
                                            data-s2-minimum-input-length="0"
                                            data-s2-multiple="true"
                                            data-s2-maximum-selection-length="30">
                                        <option value=""></option>
                                        <option value="MELHOR">Melhor</option>
                                        <option value="PIOR">Pior</option>
                                        <option value="IGUAL">Igual</option>
                                        <option value="NAO_APLICA">Não se aplica</option>
                                    </select>
                                </div>

                                %{-- COMPARATIVO CATEGORIA EM RELAÇÃO A--}%

                                <div class="form-group">
                                    <label for="deComparativoRelacaoFiltro" class="control-label" title="Fazer o comparativo entre quais resultados?">Entre os resultados de</label>
                                    <br>
                                    <select name="deComparativoRelacaoFiltro" id="deComparativoRelacaoFiltro" class="form-control fldSelect" style="width:485px;">
                                        <option value="" selected="true"></option>
                                        <option value="HO">Anterior X Avaliação</option>
                                        <option value="HF">Anterior X Validação</option>
                                        <option value="OF">Avaliação X Validação</option>
                                        %{--<option value="HFO" title="Quando não houver avaliação considerar a validação e virse-versa.">Anterior  X ( Avaliação ou Validação )</option>--}%
                                    </select>
                                </div>
                            </div>

                            <br>

                            %{-- GRUPO AVALIADO --}%
                            <div style="border-top:3px dashed green; max-width:1025px;padding:2px;">
                                <div class="form-group">
                                    <label for="sqGrupoFiltro" class="control-label" title="Para selecionar mais de uma opção, mantenha a tecla CTRL pressionada ao clicar na lista de opções">Grupo(s) avaliado(s)</label>
                                    <br>
                                    <select name="sqGrupoFiltro" id="sqGrupoFiltro" multiple="multiple" class="form-control fld500 select2 fldSelect"
                                            data-s2-minimum-input-length="0"
                                            data-s2-multiple="true"
                                            data-s2-onchange="rel003.sqGrupoChange"
                                            data-s2-maximum-selection-length="30">
                                        <g:each var="item" in="${listGrupo}">
                                            <option value="${item.id}">${item.descricao}</option>
                                        </g:each>
                                    </select>
                                </div>

                                %{-- SUBGRUPO --}%
                                <div class="form-group" id="divSubgrupoFiltro">
                                    <label for="sqSubgrupoFiltro" class="control-label" title="Para selecionar mais de uma opção, mantenha a tecla CTRL pressionada ao clicar na lista de opções">Subgrupo(s)</label>
                                    <br>
                                    <select name="sqSubgrupoFiltro" id="sqSubgrupoFiltro" disabled="true" multiple="multiple" class="form-control fld500 select2 fldSelect"
                                            data-s2-params="sqGrupoFiltro"
                                            data-s2-minimum-input-length="0"
                                            data-s2-multiple="true"
                                            data-s2-url="relatorio/select2Subgrupo"
                                            data-s2-maximum-selection-length="10">
                                        %{--<g:each var="item" in="${listSubgrupo}">
                                            <option value="${item.id}">${item.descricao}</option>
                                        </g:each>--}%
                                    </select>
                                </div>
                            </div>

                            <br>

                            %{-- BIOMA --}%
                            <div class="form-group">
%{--                                <label for="sqBiomaFiltro" class="control-label" title="Para selecionar mais de uma opção, mantenha a tecla CTRL pressionada ao clicar na lista de opções">Bioma&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="chkEndemicaBioma" value="S" class="checkbox-lg">&nbsp;&nbsp;Endêmica?</label>--}%

                            <label for="sqBiomaFiltro" class="control-label" title="Para selecionar mais de uma opção, mantenha a tecla CTRL pressionada ao clicar na lista de opções<br><br>Utilização dos campos E, OU e Somente:<br>
                                <b>OU</b>: a espécie deve ocorrer em um dos biomas selecionados<br>
                                <b>OU+Somente</b>: a espécie deve ocorrer em um dos biomas selecionados e somente nele<br>
                                <b>E</b>: a espécie deve ocorrer nos biomas selecionados<br>
                                <b>E+Somente</b>: a espécie deve ocorrer nos biomas selecionados e somente neles
                                ">Bioma&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="radANDORBiomas" id="radORBiomas" value="or" class="radio-lg">&nbsp;&ldquo;ou&rdquo;&nbsp;&nbsp;
                                <input type="radio" name="radANDORBiomas" id="radANDBiomas" value="and" class="radio-lg">&nbsp;&ldquo;e&rdquo; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="checkbox" name="chkSomenteBiomas" value="S" class="checkbox-lg">&ldquo;somente&rdquo;
                            </label>
                                <br>
                                <select name="sqBiomaFiltro" id="sqBiomaFiltro" multiple="multiple" class="form-control fld500 select2 fldSelect"
                                        data-s2-minimum-input-length="0"
                                        data-s2-multiple="true"
                                        data-s2-maximum-selection-length="30">
                                    <g:each var="item" in="${listBioma}">
                                        <option value="${item.id}">${item.descricao}</option>
                                    </g:each>
                                    <option value="0">Não preenchido</option>
                                </select>
                            </div>

                            %{-- ESTADO --}%
                            <div class="form-group">
%{--                                <label for="sqEstadoFiltro" class="control-label" title="Para selecionar mais de uma opção, mantenha a tecla CTRL pressionada ao clicar na lista de opções">Estado&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="chkEndemicaEstado" value="S" class="checkbox-lg">&nbsp;&nbsp;Endêmica?</label>--}%
                            <label for="sqEstadoFiltro" class="control-label" title="Para selecionar mais de uma opção, mantenha a tecla CTRL pressionada ao clicar na lista de opções<br><br>Utilização dos campos E, OU e Somente:<br>
                                <b>OU</b>: a espécie deve ocorrer em um dos Estados selecionados<br>
                                <b>OU+Somente</b>: a espécie deve ocorrer em um dos Estados selecionados e somente nele<br>
                                <b>E</b>: a espécie deve ocorrer nos Estados selecionados<br>
                                <b>E+Somente</b>: a espécie deve ocorrer nos estados selecionados e somente neles
                                ">Estado&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="radANDOREstados" id="radOREstados" value="or" class="radio-lg">&nbsp;&ldquo;ou&rdquo;&nbsp;&nbsp;
                                <input type="radio" name="radANDOREstados" id="radANDEstados" value="and" class="radio-lg">&nbsp;&ldquo;e&rdquo; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="checkbox" name="chkSomenteEstados" value="S" class="checkbox-lg">&ldquo;somente&rdquo;
                            </label>
                                <br>
                                <select name="sqEstadoFiltro" id="sqEstadoFiltro" multiple="multiple" class="form-control fld500 select2 fldSelect"
                                        data-s2-minimum-input-length="0"
                                        data-s2-multiple="true"
                                        data-s2-maximum-selection-length="30">
                                    <g:each var="item" in="${listEstado}">
                                        <option value="${item.id}">${item.noEstado}</option>
                                    </g:each>
                                </select>
                            </div>

                            <br>
                            %{-- UC --}%
                            <div class="form-group">
                                <label for="sqUcFiltro" class="control-label" title="Para selecionar mais de uma opção, mantenha a tecla CTRL pressionada ao clicar na lista de opções<br><br>Utilização dos campos E, OU e Somente:<br>
                                <b>OU</b>: a espécie deve ocorrer em uma das UCs selecionadas<br>
                                <b>OU+Somente</b>: a espécie deve ocorrer em uma das UCs selecionadas e somente nela<br>
                                <b>E</b>: a espécie deve ocorrer nas UCs selecionadas<br>
                                <b>E+Somente</b>: a espécie deve ocorrer nas UCs selecionadas e somente nelas
                                ">Unidade de conservação&nbsp;&nbsp;&nbsp;&nbsp;
                                    <input type="radio" name="radANDORUcs" id="radORUcs" value="or" class="radio-lg">&nbsp;&ldquo;ou&rdquo;&nbsp;&nbsp;
                                    <input type="radio" name="radANDORUcs" id="radANDUcs" value="and" class="radio-lg">&nbsp;&ldquo;e&rdquo; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <input type="checkbox" name="chkSomenteUcs" value="S" class="checkbox-lg">&ldquo;somente&rdquo;
                                </label>
                                <br>
                                <select name="sqUcFiltro" id="sqUcFiltro" multiple="multiple" class="fld form-control fld500 select2 fldSelect"
                                        data-s2-url="ficha/select2Ucs"
                                        data-s2-multiple="true"
                                        data-s2-maximum-selection-length="30"
                                        data-s2-minimum-input-length="1"
                                        data-s2-auto-height="true">
                                    <option value="">?</option>
                                </select>
                            </div>

                            %{-- ESPECIES QUE OCORREM OU NÃO EM UC --}%
                            <div class="form-group">
                                <label for="stOcorreEmUcFiltro" class="control-label">Ocorrência em UC?</label>
                                <br>
                                <select name="stOcorreEmUcFiltro" id="stOcorreEmUcFiltro" class="form-control fldSelect" style="width:500px" placeholder="Todas">
                                    <option value="" selected="selected"></option>
                                    <option value="S">Sim</option>
                                    <option value="N">Não</option>
                                </select>
                            </div>

                            <br>


                            %{-- MIGRATÓRIA --}%
                            <div class="form-group">
                                <label for="stMigratoriaFiltro" class="control-label" title="Para selecionar mais de uma opção, mantenha a tecla CTRL pressionada ao clicar na lista de opções">Espécie migratória?</label>
                                <br>
                                <select name="stMigratoriaFiltro" id="stMigratoriaFiltro" multiple="multiple" class="form-control fld500 select2 fldSelect"
                                        data-s2-minimum-input-length="0"
                                        data-s2-maximum-selection-length="3"
                                        data-s2-multiple="true">
                                    <option value="S">Sim</option>
                                    <option value="N">Não</option>
                                    <option value="D">Desconhecido</option>
                                </select>
                            </div>

                            %{--TENDENCIA POPULACIONAL--}%
                            <div class="form-group">
                                <label for="sqTendenciaPopulacionalFiltro" class="control-label" title="Para selecionar mais de uma opção, mantenha a tecla CTRL pressionada ao clicar na lista de opções">Tendência populacional</label>
                                <br>
                                <select name="sqTendenciaPopulacionalFiltro" id="sqTendenciaPopulacionalFiltro" multiple="multiple" class="form-control fld500 select2 fldSelect"
                                        data-s2-minimum-input-length="0"
                                        data-s2-maximum-selection-length="30"
                                        data-s2-multiple="true">
                                    <g:each var="item" in="${listTendenciaPopulacional}">
                                        <option value="${item.id}">${item.descricao}</option>
                                    </g:each>
                                </select>
                            </div>

                            <br>

                            %{--BACIA HIDROGRAFICA--}%
                            <div class="form-group">
                                <label for="sqBaciaHidrograficaFiltro" class="control-label" title="Para selecionar mais de uma opção, mantenha a tecla CTRL pressionada ao clicar na lista de opções">Bacia hidrográfica</label>
                                <br>
                                <select name="sqBaciaHidrograficaFiltro" id="sqBaciaHidrograficaFiltro" multiple="multiple" class="form-control fld500 select2 fldSelect"
                                        data-s2-minimum-input-length="0"
                                        data-s2-maximum-selection-length="30"
                                        data-s2-multiple="true">
                                    <g:each var="item" in="${listBaciaHidrografica}">
                                        <option value="${item.id}">${item.descricao}</option>
                                    </g:each>
                                </select>
                            </div>

                            %{-- AMEACA --}%
                            <div class="form-group">
%{--                                <label for="sqAmeacaFiltro" class="control-label" title="Para selecionar mais de uma opção, mantenha a tecla CTRL pressionada ao clicar na lista de opções">Ameaça</label>--}%

                            <label for="sqAmeacaFiltro" class="control-label" title="Para selecionar mais de uma opção, mantenha a tecla CTRL pressionada ao clicar na lista de opções<br><br>Utilização dos campos E, OU e Somente:<br>
                                <b>OU</b>: a espécie deve ocorrer em uma das ameaças selecionadas<br>
                                <b>OU+Somente</b>: a espécie deve ocorrer em uma das ameaças selecionadas e somente nela<br>
                                <b>E</b>: a espécie deve ocorrer nas ameaças selecionadas<br>
                                <b>E+Somente</b>: a espécie deve ocorrer nas ameaças selecionadas e somente nelas
                                ">Ameaça&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="radANDORAmeacas" id="radORAmeacas" value="or" class="radio-lg">&nbsp;&ldquo;ou&rdquo;&nbsp;&nbsp;
                                <input type="radio" name="radANDORAmeacas" id="radANDAmeacas" value="and" class="radio-lg">&nbsp;&ldquo;e&rdquo; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="checkbox" name="chkSomenteAmeacas" value="S" class="checkbox-lg">&ldquo;somente&rdquo;
                            </label>
                                <br>
                                <select name="sqAmeacaFiltro" id="sqAmeacaFiltro" multiple="multiple" class="form-control fld500 select2 fldSelect"
                                        data-s2-minimum-input-length="0"
                                        data-s2-maximum-selection-length="30"
                                        data-s2-multiple="true">
                                    <g:each var="item" in="${listAmeaca}">
                                        <option value="${item.id}">${item.descricao_completa}</option>
                                    </g:each>
                                </select>
                            </div>

                            <br>

                            %{-- USO --}%
                            <div class="form-group">
%{--                                <label for="sqUsoFiltro" class="control-label" title="Para selecionar mais de uma opção, mantenha a tecla CTRL pressionada ao clicar na lista de opções">Uso</label>--}%
                            <label for="sqUsoFiltro" class="control-label" title="Para selecionar mais de uma opção, mantenha a tecla CTRL pressionada ao clicar na lista de opções<br><br>Utilização dos campos E, OU e Somente:<br>
                                <b>OU</b>: a espécie deve ocorrer em um dos usos selecionados<br>
                                <b>OU+Somente</b>: a espécie deve ocorrer em um dos usos selecionados e somente nele<br>
                                <b>E</b>: a espécie deve ocorrer nos usos selecionados<br>
                                <b>E+Somente</b>: a espécie deve ocorrer nos usos selecionados e somente neles
                                ">Uso&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="radANDORUsos" id="radORUsos" value="or" class="radio-lg">&nbsp;&ldquo;ou&rdquo;&nbsp;&nbsp;
                                <input type="radio" name="radANDORUsos" id="radANDUsos" value="and" class="radio-lg">&nbsp;&ldquo;e&rdquo; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="checkbox" name="chkSomenteUsos" value="S" class="checkbox-lg">&ldquo;somente&rdquo;
                            </label>
                                <br>
                                <select name="sqUsoFiltro" id="sqUsoFiltro" multiple="multiple" class="form-control fld500 select2 fldSelect"
                                        data-s2-minimum-input-length="0"
                                        data-s2-maximum-selection-length="30"
                                        data-s2-multiple="true">
                                    <g:each var="item" in="${listUso}">
                                        <option value="${item.id}">${ item.descricao_completa }</option>
                                    </g:each>
                                </select>
                            </div>

                            %{-- LISTAS E CONVENCOES --}%
                            <div class="form-group">
                                <label for="sqListaFiltro" class="control-label" title="Para selecionar mais de uma opção, mantenha a tecla CTRL pressionada ao clicar na lista de opções">Listas e Convenções</label>
                                <br>
                                <select name="sqListaFiltro" id="sqListaFiltro" multiple="multiple" class="form-control fld500 select2 fldSelect"
                                        data-s2-minimum-input-length="0"
                                        data-s2-multiple="true"
                                        data-s2-maximum-selection-length="30">
                                    <g:each var="item" in="${listLista}">
                                        <option value="${item.id}">${item.descricao}</option>
                                    </g:each>
                                </select>
                            </div>

                            <br>

                            <div class="form-group">
                                <label for="stListaOficialFiltro" class="control-label">Presença na Lista Nacional Oficial Vigente?</label>
                                <br>
                                <select name="stListaOficialFiltro" id="stListaOficialFiltro" class="form-control fldSelect" style="width:500px" placeholder="Todas">
                                    <option value="" selected="selected"></option>
                                    <option value="S">Sim</option>
                                    <option value="N">Não</option>
                                </select>
                            </div>



                            %{-- ACOES DE CONSERVAÇÃO / INSTRUMENTO DE GESTÃO--}%
                            <div class="form-group">
                                <label for="sqAcaoFiltro" class="control-label" title="Para selecionar mais de uma opção, mantenha a tecla CTRL pressionada ao clicar na lista de opções">Ação de conservação</label>
                                <br>
                                <select name="sqAcaoFiltro" id="sqAcaoFiltro" multiple="multiple" class="form-control fld500 select2 fldSelect"
                                        data-s2-minimum-input-length="0"
                                        data-s2-multiple="true"
                                        data-s2-maximum-selection-length="30">
                                    <g:each var="item" in="${listAcao}">
                                        <option value="${item.id}">${item.descricao_completa}</option>
                                    </g:each>
                                </select>
                            </div>

                            <br>


                            %{-- OFICINA--}%
                            %{--<div class="fld form-group">
                                <label for="sqOficinaFiltro" class="control-label">Oficina</label>
                                <br>
                                <select id="sqOficinaFiltro" name="sqOficinaFiltro" multiple="multiple" class="form-control select2 fld500"
                                        data-s2-params="sqCicloAvaliacaoFiltro"
                                        data-s2-url="relatorio/select2Oficina"
                                        data-s2-multiple="true"
                                        data-s2-maximum-selection-length="5"
                                        data-s2-minimum-input-length="2"
                                        data-s2-auto-height="true">
                                </select>
                            </div>--}%

                            %{-- MULTIMIDIA --}%
                            <div class="form-group">
                                <label for="stMultimidiaFiltro" class="control-label">Multimidia</label>
                                <br>
                                <select name="stMultimidiaFiltro" id="stMultimidiaFiltro" class="form-control fldSelect" style="width:245px;" placeholder="Todas">
                                    <option value="" selected="selected"></option>
                                    <option value="COM_IMAGEM">Com imagem</option>
                                    <option value="COM_AUDIO">Com áudio</option>
                                    <option value="COM_IMAGEM_E_AUDIO">Com imagem e áudio</option>
                                    <option value="COM_IMAGEM_OU_AUDIO">Com imagem ou áudio</option>
                                    <option value="SEM_IMAGEM">Sem imagem</option>
                                    <option value="SEM_AUDIO">Sem áudio</option>
                                    <option value="SEM_IMAGEM_E_AUDIO">Sem imagem e áudio</option>
                                    <option value="SEM_IMAGEM_OU_AUDIO">Sem imagem ou áudio</option>
                                </select>
                            </div>

                            %{-- MAPA DE DISTRIBUIÇÃO --}%
                            <div class="form-group">
                                <label for="stMapaDistribuicaoFiltro" class="control-label">Mapa distribuição</label>
                                <br>
                                <select name="stMapaDistribuicaoFiltro" id="stMapaDistribuicaoFiltro" class="form-control fldSelect" style="width:250px;" placeholder="Todas">
                                    <option value="" selected="selected"></option>
                                    <option value="COM_IMAGEM">Com imagem</option>
                                    <option value="SEM_IMAGEM">Sem imagem</option>
                                </select>
                            </div>

                            <br>

                            %{--BOTÃO--}%
                            <div class="fld ml panel-footer" id="div-form-button">
                                <button type="button" id="btnPrint" data-action="rel003.print"
                                        class="fld btn btn-success" title="Visualizar abaixo a lista de taxons de acordo com os filtros selecionados">Aplicar</button>
                            <button type="button" id="btnClear" data-action="rel003.reset" class="fld btn btn-danger" title="Limpar a tela dos filtros">Limpar</button>
                            <button type="button" id="btnExportRows" data-params="exportAs:ROW" data-action="rel003.print;" class="fld btn btn-primary ml20" title="Exportar a lista de táxons para planilha de acordo com os filtros selecionados">Exportar planilha táxons</button>
                            <button type="button" id="btnExportRegs" data-params="exportAs:REG" data-action="rel003.print" class="fld btn btn-warning" title="Exportar os registros de ocorrência dos táxons para planilha de acordo com os filtros selecionados">Exportar planilha ocorrências</button>
                            </div>

                        </form>

                        %{--GRIDE--}%
                        <div id="containerGridRel003" class="mt10"
                             data-params=""
                             data-container-filters-id="formFiltro"
                             data-height="750px"
                             data-field-id="sq_ficha"
                             data-url="relatorio/rel003">
                            <g:render template="grideRel003"
                                      model="[gridId: 'GridRel003',gridHeight:750]"></g:render>
                        </div>
                    </div>
                </div> %{-- fim panel body --}%
            </div> %{-- fim panel --}%
        </div> %{-- fim col-12 --}%
    </div>  %{-- fim row --}%
</div>%{-- fim container --}%
<div class="end-page"></div>
