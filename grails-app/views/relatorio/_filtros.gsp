<!-- view: /views/relatorio/_filtro.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

%{-- FILTRO FUNÇÃO --}%
%{--
<div class="fld form-group">
    <label class="control-label" for="sqPapelFiltro">Função</label>
    <br>
    <select name="sqPapelFiltro" id="sqPapelFiltro" class="form-control fld300 fldSelect">
        <option value="">-- todas --</option>
        <g:each var="item" in="${listPapeis}">
            <option value="${item.id}">${item.descricao}</option>
        </g:each>
    </select>
</div>
--}%

%{-- FILTRO SITUACAO ATIVO/INATIVO--}%
%{--
<div class="fld form-group">
    <label for="sqSituacaoFiltro" class="control-label">Situação</label>
    <br/>
    <select name="sqSituacaoFiltro" id="sqSituacaoFiltro" class="form-control">
        <option value="">-- todos --</option>
        <option value="S">Ativo</option>
        <option value="N">Inativo</option>
    </select>
</div>
--}%

%{-- FILTRO UNIDADE ORG --}%
<div id="divFiltrosFicha${params.idRel}">

    <g:if test="${params.idRel=='rel006'}">
        <div class="fld form-group">
            <label for="sqUnidadeFiltro${params.idRel}" class="control-label">Unidade responsável</label>
            <br>
            <select id="sqUnidadeFiltro${params.idRel}" name="sqUnidadeFiltro" class="form-control select2 fld500"
                    data-s2-minimum-input-length="0"
                    data-s2-select-on-close="false"
                    data-s2-placeholder="-- todas --"
                    data-s2-params="sqCicloAvaliacao"
                    data-s2-url="ficha/select2InstituicaoFiltro">
            </select>
        </div>
        <br>
    </g:if>

    %{-- FILTRO NIVEL TAXONOMICO --}%
    <g:if test="${listNivelTaxonomico}">
        <div id="divFiltroTaxon" class="form-group">
            <label for="nivelFiltro${params.idRel}" class="control-label">Nivels taxonômico</label>
            <br>
            <select id="nivelFiltro${params.idRel}" name="nivelFiltro" data-change="${idRel?:'rel001'}.filtroNivelTaxonomicoChange" class="fld form-control fld200 fldSelect">
                <option value="">-- todos --</option>
                <g:each var="item" in="${listNivelTaxonomico}">
                    <option value="${item.coNivelTaxonomico}">${item.deNivelTaxonomico}</option>
                </g:each>
            </select>
        </div>
        <div class="fld form-group">
            <label for="sqTaxonFiltro${params.idRel}" class="control-label">Taxon</label>
            <br/>
            <select id="sqTaxonFiltro${params.idRel}" name="sqTaxonFiltro" data-s2-visible="false" class="fld form-control select2 fld250"
                    data-s2-params="nivelFiltro|nivel"
                    data-s2-url="ficha/select2Taxon"
                    data-s2-placeholder="?"
                    data-s2-maximum-selection-length="2">
            </select>
        </div>
    </g:if>

    %{-- FILTRO COM/SEM PENDENCIA --}%
    <g:if test="${params.idRel=='rel005'}">
        <br>
        <div class="form-group">
            <label for="comSemPendenciaFiltro${params.idRel}"
                   title="Filtrar as oficinas com/sem pendência de justificativa ou com/sem ficha em validação."
                   class="control-label">Pendência</label>
            <br>
            <select id="comSemPendenciaFiltro${params.idRel}" name="comSemPendenciaFiltro" class="fld form-control fld200 fldSelect">
                <option value="T">-- todas --</option>
                <option value="S" selected="true">COM pendência</option>
                <option value="N">SEM pendência</option>
            </select>
        </div>
    </g:if>


    <g:if test="${listOficinasAvaliacao}">
        %{-- FILTRO OFICINA AVALIAÇÃO --}%
        <div class="form-group">
            <label for="sqOficinaAvaliacaoFiltro${params.idRel}" class="control-label">Oficina de avaliação</label>
            <br>
            <select id="sqOficinaAvaliacaoFiltro${params.idRel}" name="sqOficinaAvaliacaoFiltro" class="fld form-control fld600 fldSelect">
                <option value="">-- todas --</option>
                <g:each var="item" in="${listOficinasAvaliacao}">
                    <option value="${item.id}">${item.sgOficina}</option>
                </g:each>
            </select>
        </div>
        <br>
    </g:if>

    <g:if test="${listOficinasValidacao}">
        %{-- FILTRO OFICINA Validacao--}%
        <div class="form-group">
            <label for="sqOficinaValidacaoFiltro${params.idRel}" class="control-label">Oficina de validação</label>
            <br>
            <select id="sqOficinaValidacaoFiltro${params.idRel}" name="sqOficinaValidacaoFiltro" class="fld form-control fld600 fldSelect">
                <option value="">-- todas --</option>
                <g:each var="item" in="${listOficinasValidacao}">
                    <option value="${item.id}">${item.sgOficina}</option>
                </g:each>
            </select>
        </div>
    </g:if>

    %{-- GRUPO AVALIADO --}%
    <g:if test="${listGrupos}">
        <div class="form-group">
            <label for="sqGrupoFiltro${params.idRel}" class="control-label">Grupo(s) valiados(s)</label>
            <br/>
            <select id="sqGrupoFiltro${params.idRel}"
                    name="sqGrupoFiltro" data-all-label="-- todos --"
                    data-multiple="true"
                    class="form-control fld fld350 fldSelect">
                <g:each var="item" in="${listGrupos}">
                    <option data-codigo="${item.id}"
                            value="${item.id}">${item.descricao}</option>
                </g:each>
            </select>
        </div>
        <br>
    </g:if>

    %{-- SITUACAO FICHA --}%
    <g:if test="${ params.idRel =~ /006/ }">
        <div id="divSelectSituacao" class="form-group">
            <label for="sqSituacaoFiltro${params.idRel}" class="control-label">Situação da ficha${ params.idRel =~ /006/ ? ' na validação' : '' }
    %{--            <i title="Para selecionar mais de uma opção, mantenha a tecla CTRL pressionada ao clicar na lista de opções" class="label-help glyphicon glyphicon-question-sign blue tooltipstered" style="cursor: pointer;"></i>--}%
            </label>
            <br>
            <select id="sqSituacaoFiltro${params.idRel}"
                    name="sqSituacaoFiltro" data-all-label="-- todas --"
                    data-multiple="true"
                    class="form-control fld fld350 fldSelect">
                <g:each var="item" in="${listSituacaoFicha}">
                    <option data-codigo="${item.id}"
                            value="${item.id}">${item.descricao}</option>
                </g:each>
            </select>
        </div>
    </g:if>

    %{-- PERIODO VALIDACAO --}%
    <g:if test="${ params.idRel =~ /006/ }">
        <div class="form-group">
            <label for="dtInicioValidacaoFiltro${params.idRel}"
                   title="Informe:<br> - data incial e final para pesquisar validações que ocorreram no período;<br> - somente a data inicial para filtrar as validações que ocorreram a partir da data informada;<br> - somente a data final para filtrar as validações que ocorreram até a data informada.<br>obs: neste filtro é considerada a data em que a ficha foi gravada como validada e não o período da oficina."
                   class="control-label">Período validação</label>
            <br/>
            <input value="" type="text" name="dtInicioValidacaoFiltro" id="dtInicioValidacaoFiltro${params.idRel}" class="form-control date fldDate"/>
            <span> a </span>
            <input value="" type="text" name="dtFimValidacaoFiltro" id="dtFimValidacaoFiltro" class="form-control date fldDate"/>
        </div>
    </g:if>

    %{-- NOME DO VALIDADOR --}%
    <g:if test="${ params.idRel =~ /006/ }">
        <br>
        <div class="form-group">
            <label for="noValidadorFiltro${params.idRel}" class="control-label" title="Informe o nome do validador(a) completo ou em parte para selecionar as fichas.<br>Para filtrar mais de um validador, informe os nomes separados por vírgula.">Nome do validador(a)</label>
            <br>
            <input type="text" id="noValidadorFiltro${params.idRel}" name="noValidadorFiltro" class="fld form-control fld400 tooltipstered"/>
        </div>
    </g:if>


    %{--BOTOES    --}%
    <div class="fld panel-footer">
        <button id="btnAplicarFiltro" data-rnd-id="${params.idRel}" data-action="${idRel?:'rel001'}.updateGrid" data-export-as="HTML" class="btn btn-success btn-xs">Aplicar filtro</button>
        <button id="btnLimparFiltro" data-rnd-id="${params.idRel}" data-action="${idRel?:'rel001'}.reset" class="btn btn-danger btn-xs">Limpar filtros</button>
    </div>
</div>
