//# sourceURL=rel006.js
;
var rel006 = {
    sqCicloSelecionado:0,
    filtrosLoaded:false,
    init: function() {
        setTimeout(function(){rel006.sqCicloChange();},2000);
    },
    /**
     * Exibir somente os grupos que fazem parte das fichas da oficina selecionada
     */
    oficinaValidacaoChange:function(){
        var sqOficina = $("select[name=sqOficinaValidacaoFiltro]").val();
        app.updateMultiSelectFiltroGruposAvaliados({container:'div#divFiltrosFicharel006'
            ,selectName:'sqGrupoFiltro'
            ,sqOficina:sqOficina});

    },
    reset:function(params,btn,event) {
        app.clearFilters(params,btn)
        $(document).scrollTop(0);
    },
    sqCicloChange:function(params)
    {
        rel006.sqCicloSelecionado = $("#sqCicloAvaliacao").val();
        $("#divRel006Content #divGrid").html('');
        if( rel006.sqCicloSelecionado )
        {
            rel006.filtrosLoaded = false;
            rel006.loadFiltros();
        }
        else
        {
            $("#divRel006Content").hide();
        }
    },
    loadFiltros:function( params )
    {
        if( ! rel006.filtrosLoaded ) {
            rel006.filtrosLoaded = true;
            var data = { sqCicloAvaliacao: rel006.sqCicloSelecionado, idRel:'rel006' };
            app.loadModule(baseUrl + 'relatorio/getCamposFiltro', data, 'formFiltro', function () {
                // adicionar evento change no select das oficinas de validação para atualizar os grupos de acorodo com a oficina
                $("select[name=sqOficinaValidacaoFiltro]").on('change',rel006.oficinaValidacaoChange);
            });
        }
        $("#divRel006Content").show();
    },
    getFiltros:function()
    {
        var filtros = { sqCicloAvaliacao : rel006.sqCicloSelecionado  }
        $("#formFiltro select").each( function(i,campo) {
            var $campo = $(campo)
            if( $campo.val() )
            {
                filtros[campo.name] = $campo.val();
            }
        } );
        return filtros;
    },
    updateGrid:function(params,ele,evt) {
        var filtros = rel006.getFiltros()
        filtros.exportAs = ( params.exportAs ? params.exportAs : 'HTML'); // rows, regs
        if( filtros.exportAs == 'HTML' ) {
            //$(document).scrollTop(1615.5555419921875);
            app.grid("GridRel006", filtros, function (res) {
                $(document).scrollTop(718);
                app.unblockUI();
                beep();
            },'Consultando banco de dados...');

        } else if( filtros.exportAs == 'ROW') {
            if( ! params.dlgConfirm ) {
                var data = { exportAs:filtros.exportAs };
                return app.confirmWithEmail('Confirma a exportação?',rel006.updateGrid, data );
            }
            filtros.format='row'
            filtros.email = params.email ? params.email : '';
            app.blockUI('Consultando banco de dados...',function(){
                app.grid("GridRel006", filtros, function (res) {
                    app.unblockUI();
                    /*
                    window.setTimeout(function () {
                        getWorkers();
                    }, 5000);
                    window.setTimeout(function () {
                        getWorkers();
                    }, 60000);

                     */
                });
            })
        }
    }

};
rel006.init();
