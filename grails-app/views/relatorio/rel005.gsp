<!-- view: /views/relatorio/rel005.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<div id="rel005Container">
   <div class="row">
      <div class="col-sm-12">
         <div class="panel panel-default">
            <div class="panel-heading">
               <span>Relatório Controle Validação</span>
               <a id="btnClose" data-action="app.unloadModule" class="btn btn-default pull-right btn-xs" title="Fechar">
               <span class="glyphicon glyphicon-remove btnClose"></span>
               </a>
            </div>
            <div class="panel-body" id="rel005Body">
                <div id="divSelectCicloAvaliacao" class="form-group">
                    <input type="hidden" id="sqCicloAvaliacao" name="sqCicloAvaliacao" value="${listCicloAvaliacao[0]?.id}"/>
                </div>

                <div id="divRel005Content" style="display:none;">
                    <fieldset>
                        <legend style="font-size:1.2em;">
                            <a data-toggle="collapse" href="#formFiltro" class="tooltipstered" style="cursor: pointer; opacity: 1;">
                                <i class="glyphicon glyphicon-filter"></i>&nbsp;Filtros
                            </a>
                        </legend>
                    </fieldset>
                    <form id="formFiltro" class="form-inline panel-collapse collapse" style="background-color:#d3e0c6" role="form"></form>

                    %{-- GRIDE--}%
                    <div id="divGrid"></div>

                </div> %{-- fim panel body --}%
            </div> %{-- fim panel --}%
      </div> %{-- fim col-12 --}%
   </div>  %{-- fim row --}%
</div>%{-- fim container --}%
