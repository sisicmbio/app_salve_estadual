//# sourceURL=rel004.js
;
var rel004 = {
    sqCicloAvaliacaoFiltro:null,
    filtrosLoaded:false,
    starting:true, // flag para não disparar a consulta ao abrir a página
    scrollY:400,
    dataTable:null,
    init: function() {
        // posicionar o gride das fichas no topo da tela
        var posicao = Math.max( window.innerHeight - $("#divGrid").offset().top, 305 );
        var gridHeight = Math.max( window.innerHeight - 305, 305);

        if( rel004.dataTable)
        {
            rel004.dataTable.destroy(false);
        }

        rel004.dataTable = $('#tableRel004').DataTable( $.extend({}, default_data_tables_options, {
             "paging"           : true
            ,"pageLength"       : 100
            ,"deferRender"      : true
            ,"scrollY"          : gridHeight //rel004.scrollY+'px'
            ,"scrollX"          : true
            ,"scrollCollapse"   : true
            ,"fixedHeader"      : false
            ,"lengthChange"     : false
            ,"processing"       : false
            ,"ajax"             : { "url": "relatorio/rel004"
                ,"type": "POST"
                ,"data": function( data ) {
                    $.extend( data, rel004.getFiltros() );
                },
            },
            "columnDefs": [
                // "targets": "_all"
                { "targets": [0], "searchable": false, "orderable": false},
                { "targets": [0,2,3], "className": "text-center"},
                // quebrar texto nas colunas Justificativas
                { "targets": [8,10],"className":"text-justify vaTop wrap"},
                { "targets": [6,7],"className":"text-left vaTop wrap"},
            ],
           "order": [[ 1, 'asc' ]]
        }));

        rel004.dataTable.on( 'xhr.dt', function ( e, settings, json ) {
            app.unblockUI();
            //window.setTimeout(function() { $(document).scrollTop(1027);},500);
        });
    },
    clear:function() {},
    sqCicloAvaliacaoFiltroChange:function(params)
    {
        rel004.sqCicloAvaliacaoFiltro = $("#sqCicloAvaliacaoFiltro").val();
    },
    getFiltros:function() {
        var filtros = {print:true,starting:rel004.starting ? 'S' : 'N'};
        rel004.starting=false;
        $("#formFiltro").find('input,select').each(function (i,ele) {
            if( $(ele).val() )
            {
                if( ele.type == 'checkbox' )
                {
                    if( ele.checked ) {
                        filtros[ele.name] = $(ele).val();
                    }
                }
                else {
                    if (typeof $(ele).val() == 'object') {
                        if( $(ele).hasClass('select2') )
                        {
                            var arrTemp=[];
                            $(ele).select2('data').forEach(function(option){
                                if( option.sqUc ) {
                                    arrTemp.push(option.sqUc + (option.esfera ? option.esfera : '') );
                                }
                                else
                                {
                                    arrTemp.push(option.id);
                                };
                            });
                            filtros[ele.name] = arrTemp.join(',');
                        }
                        else {
                            filtros[ele.name] = $(ele).val().join(',');
                        }
                    } else {
                        filtros[ele.name] = $(ele).val();
                    }
                }
            }
        });
        return filtros;
    },

    print:function()
    {
        // campos obrigatorios
        if ( ! $("#sqCicloAvaliacaoFiltro").val() ) {
            app.alertInfo('Selecione o ciclo de avaliação!');
            return;
        }
        app.blockUI('Processando...',function(){
            rel004.init();
        });
    },
    sqGrupoChange:function( evt )
    {
        $('#sqSubgrupoFiltro').val(null).trigger('change');
        if( $('#sqGrupoFiltro').val() ) {
            //$("#divSubgrupoFiltro").show();
            $("#sqSubgrupoFiltro").prop('disabled',false);
        } else {
            //$("#divSubgrupoFiltro").hide();
            $("#sqSubgrupoFiltro").prop('disabled',true);
        }
    }
};
rel004.init();
