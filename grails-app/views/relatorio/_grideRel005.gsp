<!-- view: /views/relatorio/_rel005.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<table class="table table-hover table-striped table-condensed table-bordered" data-table="true" id="tableRel005">
    <thead>
    <tr>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
    </tr>
        <tr>
            <th style="min-width:50px;width:50px;">#</th>
            <g:if test="${session.sicae.user.isADM()}">
                <th style="min-width:200px;width:200px;" class="select-filter">Centros</th>
            </g:if>
            <g:else>
                <th style="min-width:200px;width:200px;">Centro</th>
            </g:else>
            <th style="min-width:200px;width:200px;" class="select-filter">Oficina</th>
            <th style="min-width:100px;width:100px;" class="select-filter">Em validação</th>
            <th style="min-width:100px;width:100px;" class="select-filter">Justificativa<br>pendente</th>
        </tr>
    </thead>
    <tbody>
    <g:if test="${rows}">
        <g:each var="row" in="${rows}" status="i">
            <tr>
                <td class="text-center">${1+i}</td>
                <td class="text-leftr">${row.no_unidade}</td>
                <td class="text-center">${row.no_oficina}</td>
                <td class="text-center">${row.nu_em_validacao}</td>

                <td class="text-center"><span>${row.nu_pendente}</span>
                    <g:if test="${row.nu_pendente + row.nu_em_validacao > 0 }">
                    <i title="Visualizar as espécies" onClick="rel005.mostrarEspecies({'sqOficina':${row.sq_oficina},'sqUnidadeOrg':${row.sq_unidade_org}} )"
                          class="cursor-pointer mr5 fa fa-eye green pull-right" style="font-size: 20px !important;"></i>
                    </g:if>
                    <g:else>
                        <i class="mr5 fa fa-eye pull-right disabled" style="color:#d5d5d5; font-size: 20px !important;"></i>
                    </g:else>
                </td>

            </tr>
        </g:each>
        </g:if>
        <g:else>
            <tr><td class="text-center" colspan="5"><h4>Nenhum registro</h4></td></tr>
        </g:else>
    </tbody>
</table>
