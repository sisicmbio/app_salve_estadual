<!-- view: /views/relatorio/rel004.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<div id="rel004Container">
   <div class="row">
      <div class="col-sm-12">
         <div class="panel panel-default">
            <div class="panel-heading">
               <span>Relatorio Táxon - Gerencial</span>
               <a id="btnClose" data-action="app.unloadModule" class="btn btn-default pull-right btn-xs" title="Fechar">
               <span class="glyphicon glyphicon-remove btnClose"></span>
               </a>
            </div>
            <div class="panel-body" id="rel004Body" style="padding-left:10px;">
                <div id="divRel004Content">
                    <fieldset>
                        <legend style="font-size:1.2em;">
                            <a data-toggle="collapse" href="#formFiltro" class="tooltipstered" style="cursor: pointer; opacity: 1;">
                                <i class="glyphicon glyphicon-filter"></i>&nbsp;Filtros
                            </a>
                        </legend>
                    </fieldset>
                    <form id="formFiltro" class="form-inline panel-collapse collapse in" style="background-color:#d3e0c6;padding:5px;" role="form">

                        %{--CRITÉRIOS--}%

                        %{--CICLO DE AVALIACAO--}%
                        <div id="divSelectCicloAvaliacao" class="form-group">
                            <input type="hidden" id="sqCicloAvaliacaoFiltro" name="sqCicloAvaliacaoFiltro" value="${listCicloAvaliacao[0]?.id}"/>
                        </div>

                        <br>

                        %{-- NOME CIENTIFICO --}%
                        <div class="form-group">
                            <label for="nmCientificoFiltro" class="control-label" title="Informe o nome científico completo ou em parte para selecionar as fichas. Ex: Puma, Puma concolor ou concolor apenas.">Nome científico</label>
                            <br>
                            <input type="text" id="nmCientificoFiltro" name="nmCientificoFiltro" class="fld form-control fld500 tooltipstered"/>
                        </div>

                        %{-- FILTRO PELO NIVEL TAXONOMICO --}%
                        <div id="divFiltroTaxon" class="form-group">
                            <label for="nivelFiltro" class="control-label">Nível taxonômico</label>
                            <br>
                            <select id="nivelFiltro" name="nivelFiltro" data-change="app.filtroNivelTaxonomicoChange" data-params="rndId:,callback:app.btnFiltroFichaClick" class="fld form-control fld200 fldSelect">
                                <option value="">-- todos --</option>
                                <g:each var="item" in="${listNivelTaxonomico}">
                                %{--<option value="${item.coNivelTaxonomico}">${item.deNivelTaxonomico}</option>--}%
                                    <option data-codigo="${item.coNivelTaxonomico}" value="${item.id}">${item.deNivelTaxonomico}</option>
                                </g:each>
                            </select>
                        </div>

                        <div class="fld form-group">
                            <label for="sqTaxonFiltro" class="control-label">Taxon</label>
                            <br>
                            <select id="sqTaxonFiltro" name="sqTaxonFiltro" data-s2-visible="false" class="fld form-control select2" style="width:295px !important;"
                                    data-s2-params="nivelFiltro|sqNivel"
                                    data-s2-onchange="app.btnFiltroFichaClick"
                                    data-s2-url="ficha/select2TaxonNovo"
                                    data-s2-placeholder="?"
                                    data-s2-multiple="true"
                                    data-s2-maximum-selection-length="20"
                                    data-s2-minimum-input-length="2"
                                    data-s2-auto-height="true">
                            </select>
                        </div>

                        <br>
                        %{-- GRUPO AVALIADO --}%
                        <div class="form-group">
                            <label for="sqGrupoFiltro" class="control-label">Grupo(s) avaliado(s)</label>
                            <br>
                            <select name="sqGrupoFiltro" id="sqGrupoFiltro" multiple="multiple" class="form-control fld500 select2 fldSelect"
                                    data-s2-minimum-input-length="0"
                                    data-s2-multiple="true"
                                    data-s2-onchange="rel004.sqGrupoChange"
                                    data-s2-maximum-selection-length="30">
                                <g:each var="item" in="${listGrupo}">
                                    <option value="${item.id}">${item.descricao}</option>
                                </g:each>
                            </select>
                        </div>

                        %{-- SUBGRUPO --}%
                        <div class="form-group" id="divSubgrupoFiltro">
                            <label for="sqSubgrupoFiltro" class="control-label">Subgrupo(s)</label>
                            <br>
                            <select name="sqSubgrupoFiltro" id="sqSubgrupoFiltro" disabled="true" multiple="multiple" class="form-control fld500 select2 fldSelect"
                                    data-s2-params="sqGrupoFiltro"
                                    data-s2-minimum-input-length="0"
                                    data-s2-multiple="true"
                                    data-s2-url="relatorio/select2Subgrupo"
                                    data-s2-maximum-selection-length="10">
                                %{--<g:each var="item" in="${listSubgrupo}">
                                    <option value="${item.id}">${item.descricao}</option>
                                </g:each>--}%
                            </select>
                        </div>

                        %{--< UNIDADE RESPONSÁVEL--}%

                        <g:if test="${session.sicae.user.isADM() }">
                            <div class="fld form-group">
                                <label for="sqUnidadeFiltro" class="control-label">Unidade</label>
                                <br>
                                <select id="sqUnidadeFiltro" name="sqUnidadeFiltro" multiple="multiple" class="form-control select2 fld500"
                                        data-s2-params="sqCicloAvaliacaoFiltro"
                                        data-s2-url="relatorio/select2UnidadeOrg"
                                        data-s2-multiple="true"
                                        data-s2-maximum-selection-length="20"
                                        data-s2-minimum-input-length="2"
                                        data-s2-auto-height="true">
                                </select>
                            </div>
                        </g:if>
                        <br>

                        <br>
                        %{--BOTÃO--}%
                        <div class="fld panel-footer" id="div-form-button">
                            <button type="button" id="btnPrint" data-action="rel004.print"
                                    class="fld btn btn-success">Aplicar</button>
                            <button type="button" id="btnClear" data-action="app.reset('formFiltro','',true);" class="fld btn btn-danger">Limpar</button>
                        </div>
                    </form>
                    %{-- GRIDE --}%
                    <div id="divGrid" style="padding:5px;width:100%;height:auto;border:0px;">
                        <g:render template="grideRel004" model="[]"></g:render>
                    </div>
                </div>
            </div> %{-- fim panel body --}%
         </div> %{-- fim panel --}%
      </div> %{-- fim col-12 --}%
   </div>  %{-- fim row --}%
</div>%{-- fim container --}%
<div class="end-page"></div>


