<!-- view: /views/relatorio/_rel005Pendencias.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<div class="text-left" style="padding:5px;background-color: #d5e1c9;width: 100%;">
    <span>Centro: <b>${sgUnidade}</b></span><br>
    <span>Oficina: <b>${noOficina}</b></span>
</div>
<div style="border:none;padding: 0px; overflow-x:hidden;overflow-y: auto;height: calc(100% - 75px);" id="tableRel005PendenciasContainer-${winId}">
    <table class="table table-hover table-striped table-condensed table-bordered" data-table="true" id="tableRel005Pendencias-${winId}">
        <thead>
        <tr>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
            <tr>
                <th style="min-width:50px;width:50px;">#</th>
                <th style="min-width:200px;width:200px;">Espécie</th>
                <th style="min-width:200px;width:200px;" class="select-filter">Grupo</th>
                <th style="min-width:100px;width:100px;" class="select-filter">Validação</th>
                <th style="min-width:100px;width:100px;" class="select-filter">Justificativa</th>
            </tr>
        </thead>
        <tbody>

            <g:each var="row" in="${rows}" status="i">
                <tr>
                    <td class="text-center">${1+i}</td>
                    <td class="text-leftr">${raw(br.gov.icmbio.Util.ncItalico(row.nm_cientifico,true))}</td>
                    <td class="text-center">${row.no_grupo}</td>
                    <td class="text-center ${ row.in_validacao != 'Ok' ? 'red' :'green'}">${row.in_validacao}</td>
                    <td class="text-center ${ row.in_pendente  != 'Ok' ? 'red' :'green'}">${row.in_pendente}</td>
                </tr>
            </g:each>


        </tbody>
    </table>
</div>
