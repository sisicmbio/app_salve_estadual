//# sourceURL=rel002.js
;
var rel002 = {
    sqCicloAvaliacaoFiltro:null,
    filtrosLoaded:false,
    dataTable:null,
    init: function() {
        //console.log( 'rel002.js carregado.')
        rel002.sqCicloAvaliacaoFiltroChange()
    },
    clear:function()
    {
        $("#divRel002Content #divGrid").html('');
    },
    sqCicloAvaliacaoFiltroChange:function(params)
    {
        rel002.clear();
        rel002.sqCicloAvaliacaoFiltro = $("#sqCicloAvaliacaoFiltro").val();
        //var dataTemp = $('#sqOficinaFiltro').data('select2');
        //$('#sqOficinaFiltro').select2('destroy').select2( dataTemp.options.options );
    },
    getFiltros:function() {
        var filtros = {}
        $("#formFiltro").find('input,select').each(function (i,ele) {
            if( $(ele).val() )
            {
                filtros[ele.name] = $(ele).val();
            }
        });
        return filtros;
    },
    printParticipantes:function(){
        var data    = {formato:'participantes',print:true};
        data = $.extend( data,rel002.getFiltros() );
        app.blockUI('Gerando planilha dos participantes. Aguarde...'+grails.spinner,function(){
            app.ajax('relatorio/rel002', data, function(res) {
                app.unblockUI();
                if( res.status == 0){
                    if( res.data.fileName){
                        window.open(app.url + 'main/download?fileName=' + res.data.fileName + '&delete=1', '_top');
                    }
                }
            });
        });

    },
    print:function()
    {
        $("#divRel002Content #divGrid").html('<h3>Gerando relatório. Aguarde...'+window.grails.spinner+'</h3>');
        var data    = {formato:'html',print:true};
        data = $.extend( data,rel002.getFiltros() );
        if( rel002.datatable ) {
            rel002.datatable.destroy();
        }

        app.ajax('relatorio/rel002', data, function(res) {
            $("#divRel002Content #divGrid").html(res);

            // adicionar o botão "Planilha participantes" no plugin dataTables
            var newOptions = $.extend(true,{},default_data_tables_options);
            newOptions.buttons.push( {
                    "text": 'Planilha participantes',
                    "titleAttr"     : 'Exportar para planilha as informações dos participantes das oficinas',
                    className: 'btn-primary',
                    "action": function(e, dt, button, config) {
                        var that = this;
                        if( dt.data().rows().count() == 0 ){
                            app.alertInfo('Pesquisa não possui resultado.');
                            return;
                        }
                        app.confirm('Confirma exportação dos participantes?',function() {
                            rel002.printParticipantes()
                        });
                    }
            });
            // aplicar plugin Datatable na tag table
            rel002.datatable = $('#tableRel002').DataTable(newOptions).columns.adjust();
            var wh = window.innerHeight;
            var ot = $("#divGrid").offset().top+20;
            if( wh > ot ) {
                $("#divGrid").height(wh - ot);
            };
        }, null,'HTML');
    },
};
rel002.init();
