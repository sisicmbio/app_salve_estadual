<!-- view: /views/relatorio/rel006.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<div id="rel006Container">
   <div class="row">
      <div class="col-sm-12">
         <div class="panel panel-default">
            <div class="panel-heading">
               <span>Relatório Validação Analítico</span>
               <a id="btnClose" data-action="app.unloadModule" class="btn btn-default pull-right btn-xs" title="Fechar">
               <span class="glyphicon glyphicon-remove btnClose"></span>
               </a>
            </div>
            <div class="panel-body" id="rel006Body">


                    %{-- CICLO DE AVALIAÇÃO --}%
                    <div id="divSelectCicloAvaliacao" class="form-group">
                        <input type="hidden" id="sqCicloAvaliacao" name="sqCicloAvaliacao" value="${listCicloAvaliacao[0]?.id}"/>
                    </div>

                    <div id="divRel006Content" style="display:none;">
                    <fieldset>
                        <legend style="font-size:1.2em;">
                            <a data-toggle="collapse" href="#formFiltro" class="tooltipstered" style="cursor: pointer; opacity: 1;">
                                <i class="glyphicon glyphicon-filter"></i>&nbsp;Filtros
                            </a>
                        </legend>
                    </fieldset>
                    <form id="formFiltro" class="form-inline panel-collapse collapse in" style="background-color:#d3e0c6" role="form"></form>

                    %{--BOTÃO--}%
                    <div class="fld panel-footer mt20 text-right" id="div-form-button">
                        <button type="button" id="btnExportRows"
                                data-params="exportAs:ROW"
                                data-action="rel006.updateGrid;"
                                class="fld btn btn-primary btn-xs" title="Exportar a lista de táxons para planilha de acordo com os filtros selecionados">Exportar planilha</button>
                    </div>

                    %{-- GRIDE--}%

                    <div id="containerGridRel006" style="width:100%;overflow: hidden;"
                         data-params="sqCicloAvaliacao"
                         data-container-filters-id="divFiltrosFicharel006"
                         data-height="700px"
                         data-field-id="sqFicha"
                         data-url="relatorio/rel006"
                         data-on-load="">
                        <g:render template="grideRel006"
                                  model="[gridId: 'GridRel006']"></g:render>
                    </div>


                </div> %{-- fim panel body --}%
            </div> %{-- fim panel --}%
      </div> %{-- fim col-12 --}%
   </div>  %{-- fim row --}%
</div>%{-- fim container --}%
