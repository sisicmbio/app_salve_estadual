<!-- view: /views/relatorio/_rel001.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<table class="table table-hover table-striped table-condensed table-bordered" data-table="true" id="tableRel001">
    <thead>
    <tr>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
    </tr>
        <tr>
            <th style="min-width:50px;width:80px;">#</th>
            <th style="min-width:200px;width:200px;" class="select-filter">Nome</th>
            <th style="min-width:200px;width:200px;" class="select-filter">Instituição</th>
            <th style="min-width:150px;width:200px;" class="select-filter">Função</th>
            <th style="min-width:120px;width:120px;" class="select-filter">Situação</th>
            <th style="min-width:200px;width:200px;" class="select-filter">Grupo taxonômico</th>
            <th style="min-width:200px;width:200px;" class="select-filter">Vinculação fichas</th>
            <th style="min-width:120px;width:120px;" class="select-filter">Email</th>
            <th style="min-width:90px;width:90px;" >Qtd fichas</th>
            <th style="min-width:200px;width:200px;" >Fichas</th>
        </tr>
    </thead>
    <tbody>
    <g:if test="${rows}">
        <g:each var="row" in="${rows}" status="i">
            <tr>
                <td class="text-center">${1+i}</td>
                <td>${row.no_pessoa}</td>
                <td class="text-center">${row.sg_instituicao_pessoa}</td>
                <td class="text-center">${row.no_papel}</td>
                <td class="text-center">${row.in_ativo}</td>
                <td class="text-center">${row.no_grupo}</td>
                <td class="text-center">${row.sg_instituicao_ficha}</td>
                <td class="text-center">${raw(row.tx_emails)}</td>
                <td class="text-left" style="vertical-align: top !important;">
                    <fieldset>
                        <legend style="font-size:1.2em;">
                            <a data-toggle="collapse" href="#divFichas-${i+1}"
                               data-div-id="divFichas-${i+1}"
                               class="tooltipstered"
                               style="cursor: pointer;
                               opacity: 1;" title="Clique para mostrar/esconder as fichas">
                                ${ row.nu_fichas }
                            </a>
                        </legend>
                    </fieldset>
                </td>
                <td class="text-left">
                    <div id="divFichas-${i+1}" class="panel-collapse collapse text-nowrap" role="tabpanel">${raw( row.nm_cientificos ) }</div>
                </td>
            </tr>
        </g:each>
        </g:if>
        <g:else>
            <tr><td class="text-center" colspan="9"><h4>Nenhum registro</h4></td></tr>
        </g:else>
    </tbody>
</table>
