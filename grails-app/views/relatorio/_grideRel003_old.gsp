<!-- view: /views/relatorio/_rel003.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<table class="table table-hover table-striped table-condensed table-bordered"
       data-table="true" id="tableRel003"
       data-page-length='10'>
    <thead>
        <tr>
            <th style="min-width:50px;">#</th>
            <th style="min-width:200px;" class="select-filter">Taxon</th>
            <th style="min-width:200px;" class="select-filter">Categoria</th>
            <th style="min-width:200px;" class="select-filter">Critério</th>
            <th style="min-width:200px;" class="select-filter">Ciclo</th>
            <th style="min-width:200px;" class="select-filter">Grupo</th>
            <th style="min-width:200px;" class="select-filter" data-delimiter="<br>">Bioma</th>
            <th style="min-width:180px;" class="select-filter" data-delimiter="<br>">Uf</th>
            <th style="min-width:180px;" class="select-filter" data-delimiter="<br>">UC</th>
            <th style="min-width:200px;" class="select-filter" data-delimiter="<br>">Ameaça</th>
            <th style="min-width:400px;" class="select-filter" data-delimiter="<br>">Uso</th>
            <th style="min-width:500px;" class="select-filter" data-delimiter="<br>">Listas e Convenções</th>
            <th style="min-width:500px;" class="select-filter" data-delimiter="<br>">Instrumento de Gestão</th>
        </tr>
    </thead>
    <tbody>
    <g:if test="${rows && !erros}">
        <g:each var="row" in="${rows}" status="i">
            <tr>
                <td class="text-center"></td>

                <td class="text-left">${raw(row.nm_cientifico)}</td>
                <td class="text-center">${row.de_categoria_iucn}</td>
                <td class="text-center">${row.de_criterio_iucn}</td>
                <td class="text-center">${row.de_ciclo_avaliacao+' ('+row.nu_ano+')'}</td>
                <td class="text-center">${row.de_grupo}</td>
                <td class="text-left">${raw(row.de_biomas.replaceAll(/\,/,'<br>'))}</td>
                <td class="text-left">${raw(row.de_estados.replaceAll(/\,/,'<br>'))}</td>
                <td class="text-left">${raw(row.de_ucs.replaceAll(/\,/,'<br>'))}</td>
                <td class="text-left">${raw(row.de_ameacas.replaceAll(/\,/,'<br>'))}</td>
                <td class="text-left">${raw(row.de_usos.replaceAll(/\,/,'<br>'))}</td>
                <td class="text-left">${raw(row.de_listas.replaceAll(/\,/,'<br>'))}</td>
                <td class="text-left">${raw(row.de_acoes.replaceAll(/\,/,'<br>'))}</td>

            </tr>
        </g:each>
        </g:if>
        <g:else>
            <g:if test="${erros}">
                <tr>
                    <td class="text-left" colspan="12">
                        <div class="alert alert-danger" >
                                <ul>
                                <g:each var="erro" in="${erros}">
                                    <li><strong>${erro}</strong></li>
                                </g:each>
                                </ul>
                        </div>
                    </td>
                </tr>
            </g:if>
            <g:else>
                <tr><td class="text-center" colspan="12"><h4>Nenhum registro</h4></td></tr>
            </g:else>
        </g:else>
    </tbody>
</table>
