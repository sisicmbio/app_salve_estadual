<!-- view: /views/relatorio/_rel004.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
%{--<div style="margin:0px;" id="divTotalRegistros"><h3><span>0 registro(s)</span></h3></div>--}%
<div style="margin:0px;" id="divTotalRegistros"><h3><span class="total">0</span> registro(s) / <span class="selected">0</span> selecionado(s)</h3></div>
<table id="tableRel004" class="table table-bordered table-condensed table-hover display nowrap" style="width:100%">
    <thead>
        <tr>
            <th style="min-width:50px;">#</th>
            <th style="min-width:200px;">Taxon</th>
            <th style="min-width:120px;">Grupo avaliado</th>
            <th style="min-width:120px;">Subgrupo</th>
            <th style="min-width:200px">Unidade responsável</th>
            <th style="min-width:150px;">Situação</th>
            <th style="min-width:200px">Ponto focal</th>
            <th style="min-width:200px;">Coordenador de taxon</th>

            <th style="min-width:200px;">Oficina de avaliação</th>
            <th style="min-width:160px;">Período avaliação</th>

            <th style="min-width:200px;">Oficina de validação</th>
            <th style="min-width:160px;">Período validação</th>


        </tr>
    </thead>
</table>
