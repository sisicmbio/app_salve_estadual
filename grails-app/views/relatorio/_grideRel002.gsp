<!-- view: /views/relatorio/_rel002.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<div style="margin:0px;" id="divTotalRegistros"><h3><span class="total">${rows.size()} registro(s)</span> registro(s) / <span class="selected">0</span> selecionado(s)</h3></div>
<table class="table table-hover table-striped table-condensed table-bordered"
       data-table="true" id="tableRel002">
    <thead>
    <tr>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
    </tr>
        <tr>
            <th style="min-width:50px;">#</th>
            <th style="min-width:200px;" class="select-filter">Oficina</th>
            <th style="min-width:200px;" class="select-filter">Tipo</th>
            <th style="min-width:200px;" class="select-filter">Centro</th>
            <th style="min-width:200px;" class="select-filter" data-delimiter=",">Grupo</th>
            <th style="min-width:200px;" class="select-filter">Ciclo</th>
            <th style="min-width:200px;" class="select-filter">Local</th>
            <th style="min-width:120px;" class="">Data início</th>
            <th style="min-width:120px;" class="">Data fim</th>
            <th style="min-width:100px;" class="select-filter">Fonte recurso</th>
            <th style="min-width:110px;" class="select-filter">Situação</th>
            <th style="min-width:150px;width:100px;" class="">Qtd participantes</th>
            <th style="min-width:300px;width:300px;" class="select-filter" data-delimiter=",">Participantes</th>
            <th style="min-width:150px;width:100px;" class="">Qtd fichas&nbsp;<i class="label-help glyphicon glyphicon-question-sign blue" title="As fichas EXCLUÍDAS ou TRANSFERIDAS na oficina NÃO estão sendo contabilizadas."></i></th>
            <th style="min-width:300px;width:300px;" class="xselect-filter" data-delimiter=",">Fichas</th>
        </tr>
    </thead>
    <tbody>
    <g:if test="${rows && !erros}">
        <g:each var="row" in="${rows}" status="i">
            <tr>
                <td class="text-center">${1+i}</td>
                <td class="text-left">${row.sg_oficina}</td>
                <td class="text-center">${row.ds_tipo_oficina}</td>
                <td class="text-center">${row.sg_unidade_org}</td>
                <td class="text-center">${row.de_grupos}</td>
                <td class="text-center">${row.de_ciclo_avaliacao}</td>
                <td class="text-center">${row.de_local}</td>
%{--                <td class="text-center">${row.de_periodo}</td>--}%
                <td class="text-center">${row.dt_inicio}</td>
                <td class="text-center">${row.dt_fim}</td>
                <td class="text-center">${row.de_fonte_recurso}</td>
                <td class="text-center">${row.ds_situacao}</td>
                %{--<td class="text-left">${row.de_participantes}</td>--}%
                <td class="text-left" style="vertical-align: top !important;">
                    <fieldset>
                        <legend style="font-size:1.2em;">
                            <a data-toggle="collapse" href="#divParticipantes-${i+1}"
                               data-div-id="divParticipantes-${i+1}"
                               class="tooltipstered"
                               style="cursor: pointer;
                               opacity: 1;" title="Clique para mostrar/esconder os participantes">
                               %{--${(row.de_participantes.split(',').size())}--}%
                               ${ row.de_participantes ? (row.de_participantes.split(',').size() ) : '0'  }
                            </a>
                        </legend>
                    </fieldset>
                </td>

                %{-- PARTICIPANTES--}%
                <td class="text-left;">
                    <div id="divParticipantes-${i+1}" class="panel-collapse collapse"
                        role="tabpanel" style="">${row.de_participantes}
                    </div>
                </td>

                    %{--<td class="text-center">${raw('<a class="fs15 btn btn-xs btn-default fld50" title="mostrar/esconder fichas" data-toggle="collapse" href="#_div_fichas_'+i+'" style="cursor: pointer;">'+row.nu_fichas+'</a><br><div id="_div_fichas_'+i+'" class="panel-collapse collapse" style="margin:5px;text-align:left;max-height:300px;overflow-y:auto;padding:3px;border:1px solid silver;">'+row.de_fichas+'</div>')}</td>--}%
                <td class="text-left;" style="vertical-align: top !important;">
                <fieldset>
                    <legend style="font-size:1.2em;">
                        <a data-toggle="collapse" href="#divFichas-${i+1}"
                           data-div-id="divFichas-${i+1}"
                           style="cursor: pointer;
                           opacity: 1;" title="Clique para mostrar/esconder as fichas.">
                           ${(row.nu_fichas)}
                        </a>
                    </legend>
                </fieldset>
                </td>

                %{-- FICHAS--}%
                <td>
                    <div id="divFichas-${i+1}" class="panel-collapse collapse"
                         role="tabpanel" style="">${raw(row.de_fichas)}
                    </div>
                </td>
            </tr>
        </g:each>
        </g:if>
        <g:else>
            <g:if test="${erros}">
                <tr>
                    <td class="text-left" colspan="10">
                        <div class="alert alert-danger" >
                                <ul>
                                <g:each var="erro" in="${erros}">
                                    <li><strong>${erro}</strong></li>
                                </g:each>
                                </ul>
                        </div>
                    </td>
                </tr>
            </g:if>
            <g:else>
                <tr><td class="text-center" colspan="10"><h4>Nenhum registro</h4></td></tr>
            </g:else>
        </g:else>
    </tbody>
</table>

