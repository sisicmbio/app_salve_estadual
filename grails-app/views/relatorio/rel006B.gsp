<!-- view: /views/relatorio/rel006.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<div id="rel006Container">
   <div class="row">
      <div class="col-sm-12">
         <div class="panel panel-default">
            <div class="panel-heading">
               <span>Relatorio Validação - Analítico</span>
               <a id="btnClose" data-action="app.unloadModule" class="btn btn-default pull-right btn-xs" title="Fechar">
               <span class="glyphicon glyphicon-remove btnClose"></span>
               </a>
            </div>
            <div class="panel-body" id="rel006Body" style="padding-left:10px;">
                <div id="divRel006Content">
                    <fieldset>
                        <legend style="font-size:1.2em;">
                            <a data-toggle="collapse" href="#formFiltro" class="tooltipstered" style="cursor: pointer; opacity: 1;">
                                <i class="glyphicon glyphicon-filter"></i>&nbsp;Filtros
                            </a>
                        </legend>
                    </fieldset>
                    <form id="formFiltro" class="form-inline panel-collapse collapse in" style="background-color:#d3e0c6;padding:5px;" role="form">

                        %{--CRITÉRIOS--}%

                        %{--CICLO DE AVALIACAO--}%
                        <div id="divSelectCicloAvaliacao" class="form-group">
                            <label for="sqCicloAvaliacaoFiltro" class="control-label">Ciclo de Avaliação</label>
                            <br>
                            <select name="sqCicloAvaliacaoFiltro"
                                    id="sqCicloAvaliacaoFiltro"
                                    class="form-control fld500 fldSelect"
                                    data-change="rel006.sqCicloAvaliacaoFiltroChange">
                                <g:each var="cicloAvaliacao" in="${listCiclosAvaliacao}" status="i">
                                    <option value="${cicloAvaliacao.id}" ${ i==1 ? 'selected' :''}>${cicloAvaliacao.deCicloAvaliacao}</option>
                                </g:each>
                            </select>
                        </div>

                        <div id="divFiltrosRel006Wrapper" style="display:none;">
                            <br>

                            %{-- GRUPO AVALIADO --}%
                            <div class="form-group">
                                <label for="sqGrupoFiltro" class="control-label">Grupo(s) valiados(s)</label>
                                <br/>
                                <select name="sqGrupoFiltro" data-all-label="-- todos --"
                                        id="sqGrupoFiltro"
                                        data-multiple="true"
                                        class="form-control fld fld350 fldSelect">
                                    <g:each var="item" in="${listGrupos}">
                                        <option data-codigo="${item.id}"
                                                value="${item.id}">${item.descricao}</option>
                                    </g:each>
                                </select>
                            </div>

                            %{--< UNIDADE RESPONSÁVEL--}%
                            <g:if test="${session.sicae.user.isADM() }">
                                <div class="fld form-group">
                                    <label for="sqUnidadeFiltro" class="control-label">Unidade responsável</label>
                                    <br>
                                    <select id="sqUnidadeFiltro" name="sqUnidadeFiltro" multiple="multiple" class="form-control select2 fld500"
                                            data-s2-params="sqCicloAvaliacaoFiltro"
                                            data-s2-url="relatorio/select2UnidadeOrg"
                                            data-s2-multiple="true"
                                            data-s2-maximum-selection-length="20"
                                            data-s2-minimum-input-length="2"
                                            data-s2-auto-height="true">
                                    </select>
                                </div>
                            </g:if>

                            <br>

                            <div class="form-group">
                                <label for="sqOficinaValidacaoFiltro" class="control-label">Oficina</label>
                                <br/>
                                <select name="sqOficinaValidacaoFiltro"
                                        id="sqOficinaValidacaoFiltro"
                                        class="form-control fld fld700 fldSelect">
                                </select>
                            </div>

                            <br>


                            %{--BOTÃO--}%
                            <div class="fld panel-footer" id="div-form-button">
                                <button type="button" id="btnPrint" data-action="rel006.print"
                                        class="fld btn btn-success">Aplicar</button>
                                <button type="button" id="btnClear" data-action="app.reset('formFiltro','sqCicloAvaliacaoFiltro',true);" class="fld btn btn-danger">Limpar</button>
                            </div>
                        </div>
                    </form>
                    %{-- GRIDE --}%
                    <div id="divGrid" style="padding:5px;width:100%;height:auto;border:0px; display:none;">
                        <g:render template="grideRel006" model="[]"></g:render>
                    </div>
                </div>
            </div> %{-- fim panel body --}%
         </div> %{-- fim panel --}%
      </div> %{-- fim col-12 --}%
   </div>  %{-- fim row --}%
</div>%{-- fim container --}%
<div class="end-page"></div>
