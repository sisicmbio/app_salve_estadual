<!-- view: /views/relatorio/_rel003.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
%{--<div style="margin:0px;" id="divTotalRegistros"><h3><span>0 registro(s)</span></h3></div>--}%
<table id="table${gridId}"  class="table table-striped table-sorting">
    <thead>
    %{-- PAGINACAO --}%
    <tr id="tr${gridId}Pagination">
        <td colspan="130">
            <div class="text-left" style="min-height:0;height: auto;overflow: hidden" id="div${(gridId ?: pagination.gridId)}PaginationContent">
                <g:if test="${pagination?.totalRecords}">
                    <g:gridPagination  pagination="${pagination}"/>
                </g:if>
            </div>
        </td>
    </tr>

%{--    TITULOS--}%
    <tr id="tr${gridId}Columns" data-grid-id="${gridId}">
        <th style="width:60px;">#</th>
        <th style="min-width:200px;width:200px;" class="sorting sorting_asc" data-field-name="no_taxon">Taxon</th>

        <g:if test="${ session.sicae.user.isADM() }">
            <th style="min-width:100px;width:100px;">Exibir módulo<br>público
                <i class="ml5 fa fa-check-square-o" data-action="rel003.marcarTodasAsFichasModuloPublico" title="clique para MARCAR todas as fichas para serem exibidas no módulo publico.<br>Obs: todas as páginas serão consideradas."></i>
                <i class="ml5 fa fa-square-o" data-action="rel003.desmarcarTodasAsFichasModuloPublico" title="clique para DESMARCAR todas as fichas para serem exibidas no módulo publico.<br>Obs: todas páginas serão consideradas."></i>
            </th>
        </g:if>

        <th style="min-width:130px;width:130px;" class="sorting" data-field-name="no_familia">Família</th>
        <th style="min-width:130px;width:130px;" class="sorting" data-field-name="no_classe">Classe</th>
        <th style="min-width:130px;width:130px;" class="sorting" data-field-name="no_ordem">Ordem</th>
        <th style="min-width:200px;width:200px;" class="sorting" data-field-name="no_comum">Nome comun</th>
        <th style="min-width:150px;width:150px;" class="sorting" data-field-name="no_autor_taxon">Autor</th>
        <th style="min-width:80px;width:80px;"  class="sorting" data-field-name="nu_ano_taxon">Ano</th>
        <th style="min-width:200px;width:200px;" class="sorting" data-field-name="no_taxon_anterior">Nome taxon<br>ciclo anterior</th>
        %{-- <th style="min-width:200px;width:200px;" class="sorting" data-field-name="de_ciclo_avaliacao">Ciclo</th>  --}%
        <th style="min-width:200px;width:200px;" class="sorting" data-field-name="sg_unidade_org">Unidade</th>
        <th style="min-width:200px;width:200px;" class="sorting" data-field-name="de_situacao_ficha">Situação<br>ficha</th>
        <th style="min-width:120px;width:120px;" class="sorting" data-field-name="de_endemica_brasil">Espécie<br>endêmica?</th>
        %{--        HISTORICO --}%
        <th style="min-width:120px;width:120px;" class="sorting" data-field-name="de_protegida_legislacao">Protegido<br>legislação?</th>
        <th style="min-width:150px;width:150px;" class="sorting" data-field-name="de_categoria_anterior">Categoria<br>anterior</th>
        <th style="min-width:150px;width:150px;" class="sorting" data-field-name="de_criterio_anterior">Critério<br>anterior</th>
        %{--        AVALIACAO --}%
        <th style="min-width:150px;width:150px;" data-field-name="de_periodo_avaliacao">Periodo<br>avaliação</th>
        <th style="min-width:150px;width:150px;" class="sorting" data-field-name="de_categoria_avaliada">Categoria<br>avaliação</th>
        <th style="min-width:150px;width:150px;" class="sorting" data-field-name="">Critério<br>avaliação</th>
        <th style="min-width:150px;width:300px;">Justificativa<br>avaliação</th>

        %{--        VALIDAÇÃO --}%
        <th style="min-width:150px;width:150px;" data-field-name="de_periodo_validacao">Período<br>validação</th>
        <g:if test="${ session.sicae.user.isADM() || session.sicae.user.isPF() }">
            <th style="min-width:150px;width:150px;" class="sorting" data-field-name="dt_aceite_validacao">Data<br>validação</th>
        </g:if>
        <th style="min-width:150px;width:150px;" class="sorting" data-field-name="de_categoria_validada">Categoria<br>validação</th>
        <th style="min-width:150px;width:150px;" class="sorting" data-field-name="de_criterio_validado">Critério<br>validação</th>
        <th style="min-width:150px;width:300px;">Justificativa<br>validação</th>
%{--FALTA ESTA INFORMACAO        --}%
        <th style="min-width:200px;width:200px;" class="sorting" data-field-name="de_comparativo_categoria">Comparativo<br>categorias&nbsp;<i class="label-help glyphicon glyphicon-exclamation-sign blue tooltipstered" title="Anterior X Validação"></i></th>
        <th style="min-width:150px;width:200px;" class="sorting" data-field-name="de_motivo_mudanca">Motivo mudança</th>
        <th style="min-width:150px;width:200px;" class="sorting" data-field-name="no_grupo_avaliado">Grupo avaliado</th>
        <th style="min-width:200px;width:200px;" class="sorting" data-field-name="no_subgrupo_avaliado">Subgrupo</th>
        <th style="min-width:200px;width:200px;" class="sorting" data-field-name="no_grupo_recorte">Recorte<br>módulo público</th>
        <th style="min-width:200px;width:200px;" class="sorting" data-field-name="de_migratoria">Migratória?</th>
        <th style="min-width:200px;width:200px;" class="sorting" data-field-name="de_tendencia_populacional">Tendência<br>Populacional</th>
        <th style="min-width:200px;width:200px;" class="sorting" data-field-name="no_biomas">Bioma</th>
        <th style="min-width:200px;width:200px;" class="sorting" data-field-name="no_estados">Estado</th>
        <th style="min-width:300px;width:300px;" data-field-name="no_bacias">Bacia Hidrográfica</th>
        <th style="min-width:300px;width:300px;" class="sorting" data-field-name="no_ucs">Unidade conservação</th>
        <th style="min-width:200px;width:200px;">Código CNUC</th>
        <th style="min-width:300px;width:300px;"  data-field-name="no_ameacas">Ameaça</th>
        <th style="min-width:300px;width:300px;"  data-field-name="no_usos">Uso</th>
        <th style="min-width:300px;width:300px;" class="sorting" data-field-name="no_listas_convencoes">Listas e<br>convenções</th>
 %{--FALTA ESTA INFORMACAO        --}%
        <th style="min-width:300px;width:300px;" class="sorting" data-field-name="no_acoes_conservacao">Ação de<br>conservação</th>
        <th style="min-width:300px;width:300px;" class="sorting" data-field-name="no_pans">Plano de Ação</th>
        </tr>
    </thead>
    <tbody>
    <g:if test="${rows}">


    <g:each var="row" in="${rows}" status="i">
        <tr id="tr-${row.sq_ficha}">
            %{-- COLUNA NUMERAÇÃO --}%
            <td class="text-center">
                ${ i + (pagination ? pagination?.rowNum.toInteger() : 1 ) }
            </td>

            %{-- COLUNA TAXON--}%
            <td class="text-left">
                ${ raw( br.gov.icmbio.Util.ncItalico(row.no_taxon,true,true) ) }
            </td>

            %{--            EXIBIR MODULO PUBLICO--}%
            <g:if test="${ session.sicae.user.isADM() }">
                <td class="text-center">
                    <input name="chkSqFicha"
                           ${ row.st_exibir == true ? ' checked ':''}
                           onchange="rel003.chkExibirModuloPublicoChange(this)" value="${row?.sq_ficha}"
                           type="checkbox" class="checkbox-lg"/>
                </td>
            </g:if>

            %{-- COLUNA FAMILIA --}%
            <td class="text-center">
                ${ row.no_familia }
            </td>

            %{-- COLUNA CLASSE --}%
            <td class="text-center">
                ${ row.no_classe }
            </td>

            %{-- COLUNA ORDEM --}%
            <td class="text-center">
                ${ row.no_ordem }
            </td>

            %{-- COLUNA NOME COMUM --}%
            <td class="text-left">
                ${ row.no_comum }
            </td>

            %{-- COLUNA AUTOR --}%
            <td class="text-center">
                ${ row.no_autor_taxon }
            </td>

            %{-- COLUNA ANO  --}%
            <td class="text-center">
                ${ row.nu_ano_taxon }
            </td>

            %{-- COLUNA NOME TAXON CICLO ANTERIOR  --}%
            <td class="text-left">
                ${ raw( br.gov.icmbio.Util.ncItalico(row.no_taxon_anterior,true,true) ) }
            </td>

            %{-- COLUNA CICLO AVALIACAO  --}%
            %{--
                <td class="text-left">
                    ${ row.de_ciclo_avaliacao }
                </td>
            --}%

            %{-- COLUNA UNIDADE ORG --}%
            <td class="text-center">
                ${ row.sg_unidade_org }
            </td>

            %{-- COLUNA SITUACAO FICHA --}%
            <td class="text-center">
                ${ row.de_situacao_ficha }
            </td>

            %{-- COLUNA ENDEMICA  --}%
            <td class="text-center">
                ${ row.de_endemica_brasil }
            </td>

            %{-- COLUNA PROTEGIDA LEGISLAÇÃO  --}%
            <td class="text-center">
                ${ row.de_protegida_legislacao }
            </td>

            %{-- COLUNA CATEGORIA ANTERIOR  --}%
            <td class="text-center" style="background-color: #f9eed6">
                ${ row.de_categoria_anterior }
            </td>

            %{-- COLUNA CRITERIO ANTERIOR  --}%
            <td class="text-center" style="background-color: #f9eed6">
                ${ row.de_criterio_anterior }
            </td>

            %{-- COLUNA PERIODO AVALIACAO  --}%
            <td class="text-center" style="background-color: #F0FFF0FF">
                ${ row.de_periodo_avaliacao }
            </td>

            %{-- COLUNA CATEGORIA AVALIADA  --}%
            <td class="text-center" style="background-color: #F0FFF0FF">
                ${ row.de_categoria_avaliada }
            </td>

            %{-- COLUNA CRITERIO  AVALIADO  --}%
            <td class="text-center" style="background-color: #F0FFF0FF">
                ${ row.de_criterio_avaliado }
            </td>

            %{-- COLUNA JUSTIFICATIVA AVALIAÇÃO  --}%
            <td class="text-left" style="background-color: #F0FFF0FF">
                <div class="grid-col-long-text text-justify">
                    ${ raw( br.gov.icmbio.Util.stripTags3(row.tx_justificativa_avaliada,'i', 'b', 'strong', 'em','p','br') ) }

                </div>
            </td>

            %{-- COLUNA PERIODO VALIDACAO  --}%
            <td class="text-center" style="background-color: #FAFAD2FF">
                ${ row.de_periodo_validacao }
            </td>

            <g:if test="${ session.sicae.user.isADM() || session.sicae.user.isPF() }">
                %{-- COLUNA DATA VALIDACAO  --}%
                <td class="text-center" style="background-color: #FAFAD2FF">
                    ${ row.dt_aceite_validacao ? row.dt_aceite_validacao.format('dd/MM/yyyy') : '' }
                </td>
            </g:if>

            %{-- COLUNA CATEGORIA VALIDADA --}%
            <td class="text-center" style="background-color: #FAFAD2FF">
                ${ row.de_categoria_validada }
            </td>

            %{-- COLUNA CRITERIO VALIDACAO --}%
            <td class="text-center" style="background-color: #FAFAD2FF">
                ${ row.de_criterio_validado }
            </td>

            %{-- COLUNA JUSTIFICATIVA VALIDAÇÃO  --}%
            <td class="text-center" style="background-color: #FAFAD2FF">
                <div class="grid-col-long-text text-justify">
                    ${ raw( br.gov.icmbio.Util.stripTags2( row.tx_justificativa_validada ) ) }
                </div>
            </td>


            %{-- COLUNA COMPARATIVO CATEGORIAS --}%
            <td class="text-center" style="background-color: #e6e5e5">
                ${ row.de_comparativo_categoria }
            </td>

            %{-- COLUNA MOTIVO MUDANCA --}%
            <td class="text-left vaTop">
                ${ raw( br.gov.icmbio.Util.array2Ul( row.de_motivo_mudanca.toString().split(';').toList() ) ) }
            </td>

            %{-- COLUNA GRUPO AVALIADO --}%
            <td class="text-center">
                ${row.no_grupo_avaliado}
            </td>

            %{-- COLUNA SUBGRUPO AVALIADO --}%
            <td class="text-center">
                ${row.no_subgrupo_avaliado}
            </td>

            %{-- COLUNA RECORTE MODULO PUBLICO --}%
            <td class="text-center">
                ${row.no_grupo_recorte}
            </td>

            %{-- COLUNA MIGRATORIA --}%
            <td class="text-center">
                ${row.de_migratoria}
            </td>

            %{-- COLUNA TENDENCIA POPULACIONAL --}%
            <td class="text-center">
                ${row.de_tendencia_populacional}
            </td>

            %{-- COLUNA BIOMAS --}%
            <td class="text-left">
                ${row.no_biomas}
            </td>

            %{-- COLUNA ESTADOS --}%
            <td class="text-left">
                ${row.no_estados}
            </td>

            %{-- COLUNA BACIAS --}%
            <td class="text-left vaTop">
                <div class="grid-col-long-text">
                    ${ raw( br.gov.icmbio.Util.array2Br(br.gov.icmbio.Util.criarListaNomesFromArrayAgg( row.no_bacias ) ) ) }
                </div>
            </td>

            %{-- COLUNA UNIDADE DE CONSERVÇÃO --}%
            <td class="text-left vaTop">
                <div class="grid-col-long-text">
                    ${ raw( br.gov.icmbio.Util.array2Ul(br.gov.icmbio.Util.criarListaNomesFromArrayAgg( row.no_ucs ) ) ) }
                </div>
            </td>
            %{-- COLUNA CNUCS --}%
            <td class="text-left vaTop">
                <div class="grid-col-long-text">
                    ${ row.co_cnucs }
                </div>
            </td>

            %{-- COLUNA AMEACAS --}%
            <td class="text-left vaTop">
                <div class="grid-col-long-text">
                    ${ raw( br.gov.icmbio.Util.array2Ul(br.gov.icmbio.Util.criarListaNomesFromArrayAgg( row.no_ameacas ) ) ) }
                </div>
            </td>

            %{-- COLUNA USOS --}%
            <td class="text-left vaTop">
                <div class="grid-col-long-text">
                   ${ raw( br.gov.icmbio.Util.array2Ul(br.gov.icmbio.Util.criarListaNomesFromArrayAgg( row.no_usos ) ) ) }
                </div>
            </td>

            %{-- COLUNA LISTAS E CONVENCOES --}%
            <td class="text-left vaTop">
                <div class="grid-col-long-text">
                    ${ raw( br.gov.icmbio.Util.array2Ul(br.gov.icmbio.Util.criarListaNomesFromArrayAgg( row.no_listas_convencoes ) ) ) }
                </div>
            </td>

            %{-- COLUNA AÇÃO DE CONSERVAÇÃO --}%
            <td class="text-left vaTop">
                <div class="grid-col-long-text">
                    ${ raw( br.gov.icmbio.Util.array2Br(br.gov.icmbio.Util.criarListaNomesFromArrayAgg( row?.no_acoes_conservacao ) ) ) }
                </div>
            </td>

            %{-- COLUNA PANS --}%
            <td class="text-left vaTop">
                <div class="grid-col-long-text">
                    ${ raw( br.gov.icmbio.Util.array2Ul(br.gov.icmbio.Util.criarListaNomesFromArrayAgg( row.no_pans ) ) ) }
                </div>
            </td>
        </tr>
        </g:each>
    </g:if>
    <g:else>
        <g:if test="${errorMessage}">
            <td colspan="100" class="text-center"><h4 class="red"><b>${errorMessage}</b></h4></td>
        </g:if>
        <g:else>
            <g:if test="${params.gridId}">
                <tr><td colspan="100"><div class="alert alert-info"><b>Nenhum registro encontrado</b></div></td></tr>
            </g:if>
            <g:else>
                <tr><td colspan="100"><div style="height:${ ( gridHeight ?: '200')+'px'}"><b>Informe o(s) filtro(s) e clique em Aplicar ou Exportar</b></div>
                </td></tr>
            </g:else>
        </g:else>


    </g:else>
    </tbody>
</table>
