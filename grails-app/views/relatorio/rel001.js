//# sourceURL=grails-app/assets/javascripts/relatorio
;
var rel001 = {
    sqCicloSelecionado:0,
    filtrosLoaded:false,
    init: function() {
        //console.log( 'rel001.js carregado.')
        rel001.sqCicloChange();
    },

    reset:function()
    {
      app.reset('formFiltro',null,true);
      rel001.updateGrid();
    },
    sqCicloChange:function(params)
    {
        rel001.sqCicloSelecionado = $("#sqCicloAvaliacao").val();
        $("#divRel001Content #divGrid").html('');
        if( rel001.sqCicloSelecionado )
        {
            rel001.loadFiltros();
            rel001.updateGrid();
        }
        else
        {
            $("#divRel001Content").hide();
        }
    },
    loadFiltros:function( params )
    {
        if( ! rel001.filtrosLoaded ) {
            rel001.filtrosLoaded = true;
            var data = { sqCicloAvaliacao: rel001.sqCicloSelecionado };
            app.loadModule(baseUrl + 'relatorio/getCamposFiltro', data, 'formFiltro', function () {});
        }
        $("#divRel001Content").show();
    },
    filtroNivelTaxonomicoChange:function(params,element,event) {
        if (typeof event == 'undefined') {
            return;
        }
        var selectPai = $(event.target);
        var selectFilho = $("#sqTaxonFiltro");
        selectFilho.empty();
        if ($(selectPai).val()) {
            $(selectFilho).parent().removeClass('hide');
        } else {
            $(selectFilho).parent().addClass('hide');
        }
    },
    updateGrid:function()
    {
        $("#divRel001Content #divGrid").html('<h3>Gerando relatório. Aguarde...'+window.grails.spinner+'</h3>');
        var data = {formato:'html'};
        data = $.extend( data,rel001.getFiltros() );
        if ( ! data.sqCicloAvaliacao ) {
            app.alertInfo('Selecione o ciclo de avaliação!');
            return;
        }
        app.ajax('relatorio/rel001', data, function(res) {
            $("#divRel001Content #divGrid").html(res);

            // aplicar plugin Datatable na tag table
            $('#tableRel001').DataTable($.extend({}, default_data_tables_options,{
                "columnDefs": [
                    { "targets": [0,8,9], "searchable": false, "orderable": false},
                ],
                "order": [[ 1, 'asc' ]]
            })).columns.adjust();
        }, null,'HTML');

    },
    csv: function(params) {
        var data = {formato:'csv'};
        data = $.extend( data,rel001.getFiltros() );
        if ( ! data.sqCicloAvaliacao) {
            app.alertInfo('Selecione o ciclo de avaliação!');
            return;
        }
        app.ajax('relatorio/rel001', data, function(res) {
            if (res.fileName) {
                window.open(app.url + 'main/download?fileName=' + res.fileName + '&delete=1', '_top', "width=420,height=230,scrollbars=no,status=1");
            }
        }, null, 'JSON');
    },
    pdf: function(params) {
        var data = {formato:'pdf'};
        data = $.extend( data,rel001.getFiltros() );
        if ( ! data.sqCicloAvaliacao) {
            app.alertInfo('Selecione o ciclo de avaliação!');
            return;
        }
        app.ajax('relatorio/rel001', data, function(res) {
            if (res.fileName) {
                window.open(app.url + 'main/download?fileName=' + res.fileName + '&delete=1', '_top', "width=420,height=230,scrollbars=no,status=1");
            }
        }, null, 'JSON');
    },
    getFiltros:function()
    {
        var filtros = { sqCicloAvaliacao : rel001.sqCicloSelecionado  }
        $("#formFiltro select").each( function(i,campo) {
            var $campo = $(campo)
            if( $campo.val() )
            {
                filtros[campo.name] = $campo.val();
            }
        } );
        return filtros;
    }

};
rel001.init();
