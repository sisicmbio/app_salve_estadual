//# sourceURL=corpRegiao.js
;var corpRegiao = {
    map:null,
    init: function () {
        app.focus("noRegiao");
        corpRegiao.grid();
        corpRegiao.showHideForm(); // inicializar o mapa
    },
    loadMap:function(){
        setTimeout(function(){
            corpRegiao.map = corpGeo.initMap('mapShapeFile');
        },2000);
    },
    shapeFileChange:function(input){
        corpGeo.clearLayer( corpRegiao.map,'layerShapefile' );
        corpGeo.loadShapefile(input.files[0], corpRegiao.map,'layerShapefile',true);
    },
    reset:function(){
        $('form#frmRegiao')[0].reset();
        $("#sqRegiao").val(''); // reset não limpa campos hidden
        $("#fldShapefile").val('');
        corpGeo.clearLayer( corpRegiao.map,'layerShapefile' );
        app.focus('noRegiao');
    },
    save:function(){
        var data = {'sqRegiao':$("#sqRegiao").val()
            ,'noRegiao':$("#noRegiao").val()
            ,'nuOrdem':$("#nuOrdem").val()
            ,'wkt':corpGeo.generateLayerWKT(corpRegiao.map,'layerShapefile')
        };
        var errors = [];
        if( ! data.noRegiao ){
            errors.push('Nome da região deve ser informado')
        }
        if( errors.length > 0 ){
            app.growl(errors.join('<br>'),'4','Erros encontrados','tc','','error');
            return;
        }
        app.ajax(baseUrl+'corpRegiao/save',data,function(res){
            if( res.status == 0 ) {
                corpRegiao.reset();
                corpRegiao.grid( data );
            }
        },'Gravando...')
    },
    edit:function( sqRegiao ) {
        if( sqRegiao ) {
            app.ajax(baseUrl + 'corpRegiao/edit', {sqRegiao: sqRegiao}, function (res) {
                if (res.status == 0) {
                    $("#sqRegiao").val(res.data.sqRegiao );
                    $("#noRegiao").val(res.data.noRegiao );
                    $("#nuOrdem").val(res.data.nuOrdem );
                    if( ! corpRegiao.map ) {
                        corpRegiao.loadMap();
                    }
                    corpGeo.showGeojson(corpRegiao.map, 'layerShapefile', res.data.geojson, true );
                    app.focus('noRegiao');
                }
            },'Carregando...')
        }
    },
    delete:function( sqRegiao, noRegiao ) {
        if (sqRegiao) {
            if ( app.confirm('Confirma a exclusão da região <b>' + noRegiao + '</b>?', function () {
                app.ajax(baseUrl + 'corpRegiao/delete', {sqRegiao: sqRegiao}, function (res) {
                    if (res.status == 0) {
                        corpRegiao.reset()
                        $('#gridContainer table tbody tr#tr-' + sqRegiao).remove();
                        corpRegiao.updateNumRecords();
                    }
                }, 'Aguarde...')
            })
            ) ;
        }
    },
    grid:function(data){
        data = data || {};
        // atualizar somente a linha alterada no gride
        if( data.sqRegiao && data.noRegiao ){
            $('#gridContainer table tbody tr#tr-' + data.sqRegiao + ' td#tdNoRegiao').html(data.noRegiao);
            $('#gridContainer table tbody tr#tr-' + data.sqRegiao + ' td#tdNuOrdem').html(data.nuOrdem);
            return;
        }
        // recriar o gride
        $("#gridContainer").html('<div class="alert alert-info">Carregando tabela...'+grails.spinner+'</div>');
        app.ajax(baseUrl + 'corpRegiao/grid', {}, function (res) {
            $("#gridContainer").html( res );
            corpRegiao.updateNumRecords();
        },'')
    },
    updateNumRecords:function(){
        var numRecords = $("#gridContainer table tbody tr").size();
        $("#numRecords").html( String(numRecords ) + ( numRecords == 1 ? '&nbsp;região cadastrada' : '&nbsp;regiões cadastradas') );
    }
    /**
     * inicializar o mapa ao abrir o formulário
     * @param params
     */
    ,showHideForm:function(params){
        if( ! corpRegiao.map ) {
            corpRegiao.loadMap();
        }
    }
}
setTimeout(function(){corpRegiao.init(),1000});
