<!-- view: /views/corpRegiao/_gridRegiao -->
<!-- controller:${params?.controller} -->
<!-- action: ${params?.action} -->
<table class="table table-striped table-responsive table-hover">
    <thead>
    <tr>
        <th style="width: auto">Nome</th>
        <th style="width: 70px">Ordem</th>
        <th style="width: 100px;">Ações</th>
    </tr>
    </thead>
    <tbody>
    <g:each var="item" in="${regioes}">
        <tr id="tr-${item.id}">

            %{-- NOME DA REGIAO--}%
            <td id="tdNoRegiao" class="text-left">${item.noRegiao}</td>

            %{-- ORDEM--}%
            <td id="tdNuOrdem" class="text-center">${item.nuOrdem}</td>

            %{-- ACOES --}%
            <td class="text-center">
                <i class="fa fa-edit fa-button cursor-pointer blue" title="Alterar" onClick="corpRegiao.edit(${item.id})"></i>
                <i class="fa fa-remove fa-button cursor-pointer red" title="Excluir" onClick="corpRegiao.delete(${item.id},'${item?.noRegiao}')"></i>
                %{--                                        <i class="fa fa-map-marker fa-button mouse-pointer green" title="Shapefile" onClick="corpRegiao.editShapefile(${item.id},'${item?.noRegiao}')"></i>--}%
            </td>
        </tr>
    </g:each>
    </tbody>
</table>