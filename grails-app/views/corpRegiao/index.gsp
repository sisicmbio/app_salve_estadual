<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="ajax"/>
    <style>
    .map {
        width: 100%;
        height: 600px;
        background: #e2ecef;
    }
    </style>

</head>
<body>
<div class="col-sm-12">
    <div class="panel panel-default w100p" id="pnlRegiao">
        <div class="panel-heading">
            <span>
                Divisões Regionais - Brasil
            </span>
            <button id="btnClose" data-action="app.unloadModule" class="btn btn-default pull-right btn-xs" title="Fechar"><span class="glyphicon glyphicon-remove btnClose"></span></button>
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-sm-12 col-md-5">

                    <form id="frmRegiao" class="row">

                        %{--CAMPO ID--}%
                        <input type="hidden" id="sqRegiao" name="sqRegiao" value="">


                        %{-- NOME --}%
                        <div class="form-group col-sm-12 col-md-10">
                            <label class="label-required" for="noRegiao">Nome da região</label>
                            <input type="text" class="form-control" id="noRegiao" name="noRegiao" value="" maxlength="20">
                        </div>

                        <div class="form-group col-sm-12 col-md-2">
                            <label for="noRegiao">Ordem</label>
                            <input type="text" class="form-control fld50" id="nuOrdem" name="nuOrdem" value="" data-mask="00" maxlength="2">
                        </div>

                        <div class="form-group col-sm-12">
                            <label class="control-label">Shapefile da região</label>
                            <input name="fldShapefile" type="file" id="fldShapefile" class="file-loading"
                                   onchange="corpRegiao.shapeFileChange(this)"
                                   accept="zip/*"
                                   data-show-preview="false"
                                   data-show-upload="false"
                                   data-show-caption="true"
                                   data-show-remove="true"
                                   data-max-file-count="1"
                                   data-allowed-file-extensions='["zip"]'
                                   data-main-class="input-group-sm"
                                   data-max-file-size="20000"
                                   data-browse-label="Arquivo..."
                                   data-preview-file-type="object">
                        </div>


                        %{--BOTOES--}%
                        <div class="panel-footer col-sm-12">
                            <button type="button" id="btnSave" onclick="corpRegiao.save();" class="btn btn-success">Gravar</button>
                            <button type="button" class="btn btn-danger pull-right" onclick="corpRegiao.reset();">Limpar</button>
                        </div>
                        <div class="col-sm-12"><small class="red fs10">* - campo obrigatório</small></div>
                    </form>

                    %{--                        GRIDE ABAIXO DO FORM--}%
                    <div class="row mt20">
                        <div class="col-sm-12">
                            <h4 id="numRecords">&nbsp;</h4>
                            <div style="width: auto;height: auto" id="gridContainer"></div>
                        </div>
                    </div>
                </div>
                %{--  MAPA --}%
                <div class="col-sm-12 col-md-7">
                    <div>
                        <h4>Polígono da Região</h4>
                    </div>
                     <div id="mapShapeFile" style="width: 100%;height: 600px;background: #e2ecef;"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<asset:javascript src="corpGeo.js" defer="true"/>
</body>
</html>
