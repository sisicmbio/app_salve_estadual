<!-- view: /views/gerenciarValidacao/_telaInicialValidador.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
%{-- carrega o arquivo assets/validador.js na main.gsp --}%
<g:set var="rndId" value="${Math.abs(new Random().nextInt() % 10000) + 1}"/>

<div id="telaValidadorContainer" class="w100p" style="display:none;">

    <div class="panel panel-default" style="">
        <div class="panel-heading">
            <span>
                Ficha(s) Para Validação
            </span>
        </div>

        <div class="panel-body">
            %{--gride com as fichas --}%
            %{--            <div class="col-md" style="background-color: #efefef;padding:2px; border:1px solid silver;">--}%
            <div id="divFiltros">
                <div class="btn-group mb5" data-toggle="buttons">
                    <label class="btn btn-info-filtro">
                        Filtrar ficha por:
                    </label>
                    <label class="btn btn-default active mr10">
                        <input type="radio" name="filtro" value="PENDENCIAS" checked="checked"
                               onChange="validador.updateGrid()"/> Minhas pendências
                    </label>
                    <label class="btn btn-default mr10">
                        <input type="radio" name="filtro" value="VALIDACOES"
                               onChange="validador.updateGrid()"/> Minhas Validações
                    </label>
                    <label class="btn btn-default mr10">
                        <input type="radio" name="filtro" value="VALIDADAS"
                               onChange="validador.updateGrid()"/> Validadas
                    </label>
                    <label class="btn btn-default mr10">
                        <input type="radio" name="filtro" value="NAO_VALIDADAS"
                               onChange="validador.updateGrid()"/> Não Validadas
                    </label>
                    <label class="btn btn-default">
                        <input type="radio" name="filtro" value="TODAS" onChange="validador.updateGrid()"/> Todas
                    </label>
                </div>

                %{-- filtros complementares --}%
                <div style="margin-bottom:10px;width:100%;" class="form-inline">

                    %{-- FILTRO PELO NIVEL TAXONOMICO --}%

                    <div id="divFiltroTaxon" class="form-group">
                        <label for="nivelFiltro${rndId}" class="control-label">Nível taxonômico</label>
                        <br>
                        <select id="nivelFiltro${rndId}" name="nivelFiltro"
                                data-change="app.filtroNivelTaxonomicoChange"
                                data-params="rndId:${rndId},callback:app.btnFiltroFichaClick"
                                class="fld form-control fld200 fldSelect">
                            <option value="">-- todos --</option>
                            <g:each var="item" in="${listNivelTaxonomico}">
                                <option data-codigo="${item.coNivelTaxonomico}"
                                        value="${item.id}">${item.deNivelTaxonomico}</option>
                            </g:each>
                        </select>
                    </div>

                    <div class="fld form-group">
                        <label for="sqTaxonFiltro${rndId}" class="control-label"
                               title="Para selecionar mais de uma opção, pressione a tecla CTRL e clique nos itens da lista.">Taxon</label>
                        <br>
                        <select id="sqTaxonFiltro${rndId}" name="sqTaxonFiltro"
                                data-s2-visible="false"
                                class="fld form-control select2 fld300"
                                data-s2-params="nivelFiltro${rndId}|sqNivel"
                                data-s2-onchange="validador.aplicarFiltro"
                                data-s2-url="ficha/select2TaxonNovo"
                                data-s2-placeholder="?"
                                data-s2-multiple="false"
                                data-s2-maximum-selection-length="20"
                                data-s2-minimum-input-length="2"
                                data-s2-auto-height="true">
                        </select>
                    </div>


                    <div class="fld form-group" style="margin-left:15px;border-bottom:1px solid green;">
                        <label>Chat</label>
                        <br/>

                        <div class="radio mr15">
                            <label><input name="inComSemChatFiltro" id="inComSemChatTodos${rndId}" type="radio"
                                          data-change="validador.aplicarFiltro" class="radio-lg" value=""
                                          checked="true"/>&nbsp;Todas</label>
                        </div>

                        <div class="radio mr15">
                            <label><input name="inComSemChatFiltro" id="inComSemChatSim${rndId}" type="radio"
                                          data-change="validador.aplicarFiltro" class="radio-lg"
                                          value="S"/>&nbsp;Com chat</label>
                        </div>

                        <div class="radio">
                            <label><input name="inComSemChatFiltro" id="inComSemChatNao${rndId}" type="radio"
                                          data-change="validador.aplicarFiltro" class="radio-lg"
                                          value="N"/>&nbsp;Sem chat</label>
                        </div>
                    </div>
                </div>
            </div>

            <div id="divGridFichas" style="display:none;width:100%;height:auto;overflow:hidden;">
                <h4><i class="glyphicon glyphicon-refresh spinning"></i>&nbsp;Lendo fichas. Aguarde...</h4>
            </div>
        </div>
    </div>
</div>

