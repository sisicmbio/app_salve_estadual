<!-- view: /views/gerenciar/validacao/_formSelecionarFichas -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<div class="form form-inline">
    <label for="sqOficinaValidacaoFichas" class="control-label label-grande">Oficina</label>
    <br>
    <select id="sqOficinaValidacaoFichas" name="sqOficinaValidacaoFichas"
            data-change="gerenciarValidacao.sqOficinaValidacaoFichasChange"
            class="ignoreChange form-control fld600 fldSelect">
        <option value="">-- selecione --</option>
        <g:each var="oficina" in="${listTodasOficinas}">
            <option value="${oficina.id}">${oficina.sgOficina+' de '+oficina.periodo}</option>
        </g:each>
    </select>
</div>
<fieldset class="mt10">
    <legend style="font-size:1.2em;">
        <a  id="btnShowForm"
            data-action="app.showHideContainer"
            data-target="divSelecionarFichas"
            data-focus=""
            title="Mostrar/Esconder fichas!">
            <i class="fa fa-plus green"/>&nbsp;Selecionar ficha</a>
    </legend>

    <div id="divSelecionarFichas" class="form-group form-inline">

        %{--Filtro da ficha--}%
        <div class="mt10" id="divFiltrosFicha"></div>

        %{--gride fichas nao selecionadas --}%
        <div id="divGridFichasNaoSelecionadas"></div>
        %{-- botão --}%
        <div class="fld panel-footer">
            <button data-action="gerenciarValidacao.saveFichaSelecionadas" class="fld btn btn-primary">Gravar</button>
        </div>
    </div>

</fieldset>
<div id="divGridFichasSelecionadas" style="display:none;"></div>
