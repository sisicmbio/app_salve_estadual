<!-- view: /views/gerenciarValidacao/_divGridFichasAcomp.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<g:set var="cor1" value="#fff"></g:set>
<g:set var="cor2" value="#dfffdf"></g:set>
<g:set var="contador" value="${1}"></g:set>
<table id="table${gridId}" cellpadding="0" cellspacing="0" class="table table-striped table-sorting">
    <thead>
    %{-- PAGINACAO --}%
    <tr id="tr${gridId}Pagination">
        <td colspan="13">
            <div class="text-left" style="min-height:0;height: auto;overflow: hidden" id="div${(gridId ?: pagination.gridId)}PaginationContent">
                <g:if test="${pagination?.totalRecords}">
                    <g:gridPagination  pagination="${pagination}"/>
                </g:if>
            </div>
        </td>
    </tr>
   %{-- FILTROS --}%
   <tr id="tr${gridId}Filters" class="table-filters" data-grid-id="${gridId}">
        <td>
            <div style="display:flex;flex-direction:row; justify-content:space-between;">
                <i class="fa fa-question-circle"
                   title="Utilize os campos ao lado para filtragem dos registros.<br> Informe o valor a ser localizado e pressione <span class='blue'>Enter</span>.<br>Para remover o filtro limpe o campo e pressione <span class='blue'>Enter</span> novamente."></i>
                <i data-grid-id="${gridId}" class="fa fa-eraser" title="Limpar filtro(s)"></i>
                <i data-grid-id="${gridId}" class="fa fa-filter" title="Aplicar filtro(s)"></i>
            </div>
        </td>
        <td title="Informe o nome científico completo ou em parte para pesquisar. Ex: Aburria jacutinga, Aburria ou jacutinga"><input type="text" name="fldNmCientifico${gridId}" value="${params?.nmCientifico ?: ''}" class="ignoreChange form-control table-filter"
                   placeholder="" data-field="nmCientificoFiltro">
        </td>

        %{-- SITUACAO DA FICHA --}%
%{--        <td></td>--}%

        %{-- GRUPO AVALIADO--}%
        <td title="Informe o nome do grupo completo ou em parte para pesquisar. Ex; peixes, aves"><input type="text" name="fldNoGrupo${gridId}" value="${params?.noGrupo ?: ''}" class="ignoreChange form-control table-filter"
                   placeholder="" data-field="noGrupoFiltro">
        </td>
        %{-- CATEGORIA AVALIADA --}%
        <td title="Informe o(s) código(s) da(s) categoria(s) serparado(s) por vírgula. Ex: CR,LC"><input type="text" name="fldCoCategoriaAvaliada${gridId}" value="${params?.coCategoriaAvaliada ?: ''}" class="form-control table-filter text-uppercase"
                   placeholder=""
                   data-field="coCategoriaAvaliadaFiltro">
        </td>
        %{-- CRITERIO AVALIADO --}%
        <td><input type="text" name="fldDsCriterioAvaliado${gridId}" value="${params?.dsCriterioAvaliado ?: ''}" class="ignoreChange form-control table-filter"
                   placeholder="..."
                   data-field="dsCriterioAvaliadoFiltro">
        </td>
        %{-- NOMES DOS VALIDADORES--}%
        <td title="Informe o nome ou parte do nome do validador">
            <select class="ignoreChange form-control table-filter"
                    data-field="noValidadorFiltro"
                    onchange="gerenciarValidacao.acomp.filtroNomeValidadorChange()"
                    name="fldNoValidadorFiltro${gridId}"
                    id="fldNoValidadorFiltro${gridId}">
            </select>
            %{--<input autocomplete="off" list="fldNoValidadorFiltro${gridId}List" type="text" name="fldNoValidadorFiltro${gridId}"
                   value="${params?.noValidadorFiltro ?: ''}" class="ignoreChange form-control table-filter"
                   placeholder=""
                   data-field="noValidadorFiltro">
                   <datalist id="fldNoValidadorFiltro${gridId}List"></datalist>--}%
        </td>

        <td>
            %{--<select name="fldRespostas${gridId}"
                    class="form-control table-filter fldSelect fld250"
                    data-all-label="-- todas --"
                    data-multiple="true"
                    placeholder=""
                    data-field="deRespostasFiltro">
            <g:if test="${listRespostas}">
                <g:each var="item" in="${listRespostas}">
                    <option value="${item?.codigo}">${item?.descricao}</option>
                </g:each>
            </g:if>
            </select>--}%
        </td>
        %{-- SITUACAO DA FICHA NA OFICINA--}%
        <td>
            <input type="text" name="fldDsSituacaoFichaValidacao${gridId}"
                   value="${params?.dsSituacaoFichaValidacao ?: ''}" class="ignoreChange form-control table-filter"
                   placeholder=""
                   data-field="dsSituacaoFichaValidacaoFiltro">
        </td>
        %{-- CATEGORIA VALIDADA --}%
        <td title="Informe o(s) código(s) da(s) categoria(s) serparado(s) por vírgula. Ex: CR,LC">
            <input type="text" name="fldCoCategoriaValidada${gridId}"
                   value="${params?.coCategoriaValidada ?: ''}" class="form-control table-filter text-uppercase"
                   placeholder=""
                   data-field="coCategoriaValidadaFiltro">
        </td>
        %{-- CRITERIO VALIDADO --}%
        <td><input type="text" name="fldDsCriterioValidado${gridId}" value="${params?.dsCriterioValidado ?: ''}" class="ignoreChange form-control table-filter"
                   placeholder="..."
                   data-field="dsCriterioValidadoFiltro">
        </td>
        <td></td>
        <td></td>
        <td title="Informe a sigla ou parte da sigla da unidade.">
            <input type="text" name="fldsgUnidadeFiltro${gridId}"
                   value="${params?.sgUnidadeFiltro ?: ''}" class="ignoreChange form-control table-filter"
                   placeholder=""
                   data-field="sgUnidadeOrgFiltro">
        </td>
   </tr>

    %{-- TITULOS --}%
    <tr id="tr${gridId}Columns" data-grid-id="${gridId}">
        <th style="width:60px;">#</th>
        <th style="width:200px;" class="sorting sorting_asc" data-field-name="nm_cientifico">Nome cientifico</th>
%{--        <th style="width:120px;" class="sorting" data-field-name="ds_situacao_ficha">Situação<br>ficha</th>--}%
        <th style="width:150px;" class="sorting" data-field-name="ds_grupo">Grupo</th>
        <th style="width:120px;" class="sorting" data-field-name="cd_categoria_avaliada">Categoria<br>avaliada</th>
        <th style="width:120px;" class="sorting" data-field-name="ds_criterio_avaliado">Critério<br>avaliada</th>
        <th style="width:250px;" class="sorting" data-field-name="json_validadores">Validadores</th>
        <th style="width:300px;">Respostas</th>
        <th style="width:150px;" class="sorting" data-field-name="ds_situacao_ficha_validacao">Situação ficha<br>na validação</th>
        <th style="width:120px;" class="sorting" data-field-name="cd_categoria_final">Categoria<br>validada</th>
        <th style="width:120px;" class="sorting" data-field-name="ds_criterio_aval_iucn_final">Critério<br>validado</th>
        <th style="width:300px">Justificativa final</th>
        <th style="width:120px" class="sorting" data-field-name="dt_aceite_validacao">Data<br>validação</th>
        <th style="width:250px" class="sorting" data-field-name="sg_unidade_org">Unidade<br>responsável</th>
    </tr>
    </thead>
    <tbody>
    <g:if test="${dadosGride && dadosGride.size() > 0 }">
        <g:each var="item" in="${ dadosGride }" status="lin">

        %{--<g:set var="validadores" value="${JSON.parse(item.json_validadores)}"></g:set>--}%

            <tr style="background-color:${lin%2==0?cor1:cor2};" id="tr-${item.sq_ficha}" data-id-sq-ficha="${item.sq_oficina_ficha}">
                <td style="width:60px;min-width:60px;" class="text-center">
                    <span>${ lin + ( pagination?.rowNum ?: 1 ) }</span>
                </td>

                %{--NOME CIENTIFICO--}%
                <td>
                <g:if test="${! item.nu_versao }">
                    ${raw('<a href="javascript:void(0);" title="Clique para ver a ficha completa." onClick="showFichaCompleta(' + item.sq_ficha + ',\'' +
                        br.gov.icmbio.Util.ncItalico(item.nm_cientifico,false,true,item.co_nivel_taxonomico) + '\',false,\'validacao\',null,' + canModify + ',\'\',\''+item.sq_oficina_ficha+'\')">' +
                        br.gov.icmbio.Util.ncItalico(item.nm_cientifico,true,true,item.co_nivel_taxonomico) + '</a>')}

                </g:if>
                <g:else>
                    ${ raw('<a href="javascript:void(0);" title="Clique para ver a versão da ficha." onClick="showFichaVersao({sqFichaVersao:' +
                        item.sq_ficha_versao+'})">' +
                        br.gov.icmbio.Util.ncItalico(item.nm_cientifico,true ) + '</a>&nbsp;<span>V.'+item.nu_versao+'</span>') }

                </g:else>
                </td>

                %{--SITUACAO ATUAL DA FICHA--}%
                %{--                <td class="text-center ${item.cd_situacao_ficha_sistema=='VALIDADA' ?'green':'#000'}" >${item.ds_situacao_ficha}</td>--}%

                <td>
                    ${item.ds_grupo}
                </td>

                %{--CATEGORIA OFICINA--}%
                <td class="text-center">${item.cd_categoria_avaliada?:''}
                    <g:if test="${item.st_possivelmente_extinta}">
                        <span style="color:blue;">${raw(item.st_possivelmente_extinta == 'S' ? '<br>(PEX)':'')}</span>
                    </g:if>
                </td>

                %{--CRITÉRIO OFICINA--}%
                <td class="text-center">${ item.ds_criterio_avaliado_oficina ?: item.ds_criterio_avaliado}</td>

                %{--VALIDADORES--}%
                <td class="text-left" >
                    <ol style="padding-left: 20px;">
                        <g:each var="validadorFicha" in="${ item.validadores }" status="i">
                            <g:if test="${ validadorFicha.situacaoConvite =='CONVITE_ACEITO' && validadorFicha.valido }">
                                %{-- MOSTRAR OS NOMES DOS VALIDADORES SOMENTE PARA PERFIL ADMIN e PONTO FOCAL --}%
                                <li class="${validadorFicha.respondido ? 'green':"red"}">
                                    <g:if test="${ isAdm || isPF }">
                                            ${ br.gov.icmbio.Util.nomeAbreviado( validadorFicha.nome ) }
                                    </g:if>
                                    <g:else>
                                            <span>validador</span>
                                    </g:else>
                                </li>
                            </g:if>
                        </g:each>
                    </ol>

                    <g:if test="${ ! item.nu_versao }">
                        <div class="text-center text-nowrap">
                            <g:if test="${item.st_possui_chat}">
                                ${raw( '<i title="Com chat" class="fa fa-comment green mr5"></i>')}
                            </g:if>
                            <g:else>
                                ${raw( '<i title="Sem chat" class="fa fa-comment mr5" style="color:transparent;"></i>')}
                            </g:else>

                            <button type="button" title="Abrir a fundamentação"
                                    data-action="gerenciarValidacao.showFormFundamentacao"
                            %{-- Equipe pediu para retirar esta regra em 03/12/2019
                                        data-can-modify="${canModify && ( ! session.sicae.user.isCT() || item.cd_categoria_final ) }"
                                 --}%
                                    data-can-modify="${canModify }"
                                    data-sq-oficina-ficha="${item.sq_oficina_ficha}"
                                    data-sq-ficha="${item.sq_ficha}"
                                    data-contexto="validacao"
                                    data-no-cientifico="${br.gov.icmbio.Util.ncItalico(item.nm_cientifico,false,true,item.co_nivel_taxonomico)}" class="btn btn-primary btn-xs">Fundamentação</button>

                            %{-- EXIBIR POPUP CONVIDAR VALIDADOR --}%
                            <g:if test="${ session.sicae.user.isADM()}">
                                <g:if test="${! (item.cd_situacao_ficha_sistema =~ /FINALIZADA|VALIDADA/)}">
                                <button type="button" class="btn btn-primary btn-xs ml5" title="Adicionar validador"
                                        data-action="gerenciarValidacao.showFormConvidarValidador"
                                        data-can-modify="${canModify }"
                                        data-sq-oficina-ficha="${item.sq_oficina_ficha}"
                                        data-sq-ficha="${item.sq_ficha}"
                                        data-contexto="validacao"
                                        data-no-cientifico="${br.gov.icmbio.Util.ncItalico(item.nm_cientifico,true,true,item.co_nivel_taxonomico)}">Validador</button>
                                </g:if>
                                <g:else>
                                    <button type="button" class="btn btn-primary btn-xs ml5" title="ficha ${item.ds_situacao_ficha}"
                                    disabled="true">Validador</button>

                                </g:else>

                            </g:if>
                            ${raw( item.st_pf_convidado==true || item.st_ct_convidado==true ? '<i title="Convidado para o bate-papo." class="fa fa-comment blue ml5"></i>':'<i class="ml12">&nbsp;</i>')}
                    </div>
                    </g:if>
                    <g:else>
                        <g:if test="${item.st_possui_chat}">
                            ${raw( '&nbsp;<i title="Com chat" class="fa fa-comment green mr5"></i>')}
                        </g:if>
                        <g:else>
                            ${raw( '&nbsp;<i title="Sem chat" class="fa fa-comment mr5" style="color:transparent;"></i>')}
                        </g:else>

                        <button type="button" title="Abrir a fundamentação"
                                data-action="showFundamentacaoVersao"
                                data-sq-oficina-ficha="${item.sq_oficina_ficha}"
                                data-sq-ficha="${item.sq_ficha}"
                                class="btn btn-warning btn-xs">Fundamentação</button>

                    </g:else>
                </td>

                %{--RESPOSTAS--}%
                <td class="text-left" >
                    <ol style="padding-left: 20px;">
                        <g:each var="validadorFicha" in="${ item.validadores }" status="i">
                            <g:if test="${ validadorFicha.situacaoConvite =='CONVITE_ACEITO' && validadorFicha.valido && validadorFicha.resposta }">
                                <li>
                                    %{--Cat:${validadorFicha.categoria_sugerida}<br>--}%
                                    <span class="cursor-pointer" title="${ isAdm || isPF ? validadorFicha.nome: 'Validador(a) ' + (i+1)}">
                                        <g:if test="${validadorFicha.resposta_revisao}">
                                            <span class="text-strike">${ raw( validadorFicha.resposta?:' não respondeu.') }</span><br>
                                            <b>
                                            <g:renderLongText table="validador_ficha_revisao"
                                                              id="${validadorFicha?.sq_revisao}"
                                                              column="tx_justificativa"
                                                              text="Revisão da coordenação"
                                                              textColor="#f00"
                                                              title="Ver a justificativa"
                                                              maxLength="0" />
                                            </b>
                                            ${ raw(validadorFicha.resposta_revisao) }
                                        </g:if>
                                        <g:else>
                                                ${ raw( validadorFicha.resposta?:' não respondeu.') }
                                        </g:else>
                                    </span>
                                </li>
                            </g:if>
                        </g:each>
                    </ol>
                </td>

                %{--SITUACAO DA FICHA NA VALIDACAO--}%
%{--                <td class="text-center ${item.cd_situacao_ficha_sistema=='VALIDADA' ?'green':'#000'}" >${item.ds_situacao_ficha}</td>--}%
                <td class="text-center ${item.cd_situacao_ficha_validacao_sistema=='VALIDADA' ?'green':'#000'}" >${item.ds_situacao_ficha_validacao}</td>


                %{-- CATEGORIA FINAL--}%
                <td class="text-center">${item.cd_categoria_final}
                    <g:if test="${item.st_possivelmente_extinta_final}">
                        <span style="color:blue;">${raw(item.st_possivelmente_extinta_final == 'S' ? '<br>(PEX)':'')}</span>
                    </g:if>
                </td>

                %{-- CRITERIO FINAL --}%
                <td class="text-center">${item.ds_criterio_aval_iucn_final}</td>

                %{-- JUSTIFICATIVA --}%
                <td class="text-justify" >
                    <g:if test="${ item.st_pendente}">
                        <div class="text-center"><span class="red"><b>Pendente!</b></span></div>
                    </g:if>
                     <g:else>
                            <div id="div-justificativa-${item.sq_ficha}" data-id="${item.sq_ficha}"
                                 data-id-ficha="${item.sq_ficha}" class="text-justify">
                                <g:renderLongText table="ficha" id="${item.sq_ficha}" column="ds_justificativa_final" text="${raw(item.ds_justificativa_final)}"/>
                            </div>
                    </g:else>
                </td>
                <td class="text-center" >
                    %{--
                    ALTERADO PARA EXIBIR A DATA DA VALIDACAO GRAVADA NA TABELA OFICINA_FICHA E NÃO A DATA ACEITE VALIDACAO DA TABELA
                    FICHA. ACOMPANHAR PARA VER SE VAI ACONTECER ALGUMA DIVERGENCIA, A PRINCIPIO O CORRETO É A DA TA QUE A FICHA
                    FOI VALIDADA NAQUELA OFICINA SENAO TODA VEZ QUE FOR VISUALIZAR ESTA OFICINA VAI APARECER A DATA A ÚLTIMA VALIDAÇÃO
                    --}%
%{--                    <g:if test="${item.cd_situacao_ficha_sistema=='EM_VALIDACAO' || !item.dt_aceite_validacao }">&nbsp;</g:if>--}%
                    <g:if test="${item.cd_situacao_ficha_sistema=='EM_VALIDACAO' || !item.dt_validada }">&nbsp;</g:if>
                    <g:else>
%{--                        ${item?.dt_aceite_validacao?.format('dd/MM/yyyy')}<br>--}%
                        ${item?.dt_validada?.format('dd/MM/yyyy')}
                    </g:else>
                </td>
                <td class="text-center" >${item?.sg_unidade_org}</td>
            </tr>
        </g:each>
    </g:if> %{-- fim tem ficha--}%
    <g:else>
        <tr>
            <td colspan="13">
                <g:if test="${params.action == 'getTelaAcompanhamento'}">
                    <h3>Carregando...</h3>
                </g:if>
                <g:else>
                    <h3>Nenhuma ficha avaliada e revisada!
                        <g:if test="${!session.sicae.user.isADM()}">
                            <h4>Ou pode ser que as fichas ainda não tenham sido atribuídas a você no módulo Função.</h4>
                        </g:if>
                    </h3>
                </g:else>
            </td>
        </tr>
    </g:else>
    </tbody>
</table>
