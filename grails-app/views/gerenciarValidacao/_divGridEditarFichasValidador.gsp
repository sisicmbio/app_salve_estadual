<!-- view: /views/gerenciarValidacao/_divGridEditarFichasValidador.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<div style="height:100%;overflow: auto;">
    <table class="table table-hover table-striped table-condensed table-bordered" id="tableEditarFichasValidador${rndId}">
        <thead>
        <th style="width:30px;">#</th>
        <th style="width:30px;"><i title="Marcar/Desmarcar Todos" data-rnd-id="${rndId}" data-action="checkUncheckAll" data-cb="gerenciarValidacao.checkGridEditarFichaValidadorClick" class="glyphicon glyphicon-unchecked"></i></th>
        <th style="width:auto;">Nome Científico</th>
        <th style="width:150px;">Grupo avaliado</th>
        <th style="width:200px;">Categoria Avaliada</th>
        </thead>
        <tbody id="body${rndId}">
        <g:if test="${lista.size() > 0 }">
            <g:each var="item" in="${lista}" status="i">
                <tr>
                   <td class="text-center">${(i+1)}</td>
                   <td class="text-center">
                       <input id="sqValidadorFicha_${item.sqValidadorFicha}" value="${item.sqValidadorFicha}" type="checkbox" onClick="gerenciarValidacao.checkGridEditarFichaValidadorClick( { rndId:${rndId} })" class="fld checkbox checkbox-lg"/>
                   </td>
%{--                    NOME CIENTIFICO--}%
                   <td>
                       ${ raw( br.gov.icmbio.Util.ncItalico( item.nmCientifico,true ) )}
                   </td>

%{--                    GRUPO AVALIADO--}%
                    <td class="text-center">
                        ${item?.dsGrupo}
                    </td>

%{--                    CATEGORIA--}%
                    <td class="text-center">
                        ${ item.deCategoriaIucn ? item.deCategoriaIucn + ' ('+item.cdCategoriaIucn + ')' : '' }
                    </td>
                </tr>
            </g:each>
        </g:if>
        <g:else>
            <tr>
                <td colspan="4">
                    <h3>Nenhuma ficha encontrada!</h3>
                </td>
            </tr>
        </g:else>
        </tbody>
    </table>
</div>
