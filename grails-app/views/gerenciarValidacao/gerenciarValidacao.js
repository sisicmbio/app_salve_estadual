//# sourceURL=gerenciarValidacao.js
;
var gerenciarValidacao = {
    container: '#gerenciarValidacaoContainer ',
    sqCicloAvaliacao:0,
    datatable:null,
    grideConvites:null,
    updatingDashboard:false,
    updatingDashboardOficina:false,
    filtroAmeacadas:'',
    init: function() {
        /*destroyEditors();
        app.showHideTab('tabCadastro', false);
        gerenciarValidacao.updateGridOficinas();
        $("#sqOficinaValidacao").val('');
         */
        // Disparar a consulta
        window.setTimeout(function(){
           gerenciarValidacao.sqCicloAvaliacaoChange({ sqCicloAvaliacao: $("#sqCicloAvaliacao").val() } );
        },1000);
    },
    setValidador:function(ele ){
        if( event ) {
            event.preventDefault();
        }
        var data = $(ele).data();
        var noPessoa = $(ele).text();
        app.setSelect2(data.fieldSelect,{'id':data.id,'descricao':noPessoa},data.id)
        $("#"+data.fieldEmail).val( data.email);
    },

    selectAmeacadasChange:function(params,ele ){
        gerenciarValidacao.filtroAmeacadas = $(ele).val();
        var noOficina = '';
        var sgOficina = '';
        if( gerenciarValidacao.filtroAmeacadas == 'AMEACADAS'){
            noOficina = 'Oficina de validação de ameaçadas';
            sgOficina = 'Validação de ameaçadas';
        } else if (gerenciarValidacao.filtroAmeacadas == 'NAO_AMEACADAS') {
            noOficina = 'Oficina de validação de não ameaçadas';
            sgOficina = 'Validação de não ameaçadas';
        }
        /*
        // atualizar o campo filtro pela categoria
        $select = $("select[name=sqCategoriaFiltro]");
        $select.find('option').map(function(index, option){
            if( gerenciarValidacao.filtroAmeacadas=='AMEACADAS' && /EN|VU|CR/.test( $(option).data('codigoSistema') ) ) {
                $(option).prop('selected',true);
            } else {
                $(option).prop('selected',false);
            }
        })
        $select.multiselect('refresh');
         */
        if( noOficina ) {
            $("#noOficina").val(noOficina);
            $("#sgOficina").val(sgOficina);
            $("select[name=sqCategoriaFiltro]").closest('div.form-group').hide();
        } else {
            $("select[name=sqCategoriaFiltro]").closest('div.form-group').show();
        }
        gerenciarValidacao.updateGridFichaCadastro();
        app.focus('deLocal');
     },
    // Modulo Revisao coordenacao
    cadRevisaoCoordenacao:function(params,fld,evt){
        params = params || {}
        if( ! params.sqValidadorFicha ){
            return;
        }
        var data = app.params2data(params, {} )

        app.panel('pnlCadRevisaoCoordenacao-' + data.winId
            , 'Validação - Revisão pela Coordenação'
            , 'gerenciarValidacao/getFormRevisaoCoordenacao'
            , data, function () {
                // onOpen - configurar a tela
                setTimeout(function(){
                    gerenciarValidacao.resultadoRevisaoCoordChange(data);
                },1000)

            },800,600,true,false,function(){
                tinymce.remove('#txJustificativa');
            },'center','#384b38',false,false)
    },
    resultadoRevisaoCoordChange: function(params) {
        var frm = "#frmRevisaoCoordenacao" + params.sqFicha;
        var codigo = $(frm + " #sqResultado option:selected").data('codigo');
        if ( codigo ) {
            if (codigo == 'RESULTADO_A') {
                $(frm + " #divCategoria,#divCriterio").addClass('hide');
            } else if (codigo == 'RESULTADO_B') {
                $(frm + " #divCategoria,#divCriterio").addClass('hide');
            } else if (codigo == 'RESULTADO_C') {
                $(frm + " #divCategoria,#divCriterio").removeClass('hide');
                gerenciarValidacao.categoriaSugeridaChange(params);
            }
        } else {
            $(frm + " #divCategoria,#divCriterio").addClass('hide');
        }
    },
    categoriaSugeridaChange: function( params ) {
        contexto = "#frmRevisaoCoordenacao" + params.sqFicha + ' ';
        // se a categoria for 'VU,CR,EN não exibir o campo critério'
        if (!/EN|VU|CR/.test($(contexto + "#sqCategoriaSugerida option:selected").data('codigo'))) {
            $(contexto + "#deCriterioSugerido").parent().addClass('hide');
        } else {
            $(contexto + "#deCriterioSugerido").parent().removeClass('hide');
            if ($(contexto + "#sqCategoriaSugerida option:selected").data('codigo') == 'CR') {
                $(contexto + "#stPossivelmenteExtinta").closest('label').removeClass('hide');
            } else {
                $(contexto + "#stPossivelmenteExtinta").closest('label').addClass('hide');
            }
        }
    },
    saveFormRevisaoCoordenacao:function( params ){
        var data = app.params2data(params,{});
        var erros = [];
        var $form = $("#frmRevisaoCoordenacao"+params.sqFicha);
        var $categoriaSugerida  = $form.find('#sqCategoriaSugerida');
        var $resultado          = $form.find('#sqResultado');
        var $deCriterioSugerido = $form.find('#deCriterioSugerido');
        // validar resultado
        data.sqResultado        = $resultado.val();
        if( ! data.sqResultado ) {
            erros.push('Resultado é obrigatório');
        }

        data.sqCategoriaSugerida     = ''
        if( $categoriaSugerida.is(":visible") ) {
            data.sqCategoriaSugerida = $categoriaSugerida.val();
            if (!data.sqCategoriaSugerida ) {
                erros.push('Categoria sugerida é ogrigatória.');
            }
        }
        data.stPossivelmenteExtinta = '';
        data.deCriterioSugerido = '';
        if( $deCriterioSugerido.is(":visible") ) {
            data.stPossivelmenteExtinta = $form.find('#stPossivelmenteExtinta:checked').val();
            data.deCriterioSugerido = $deCriterioSugerido.val();
            if (!data.deCriterioSugerido && $deCriterioSugerido.is(":visible")) {
                erros.push('Critério sugerido é ogrigatório.');
            }
        }
        data.txJustificativa = trimEditor($form.find('#txJustificativa').val());
        if( ! data.txJustificativa.trim() ) {
            erros.push( 'Justificativa é obrigatoria');
        }
        if( erros.length > 0 ){
            app.alertInfo(erros.join('<br>'),'',function(){
                app.focus("txJustificativa");
            });
            return;
        }
        app.confirm("Confirma gravação da revisão?", function() {
            app.ajax(app.url + 'gerenciarValidacao/saveFormRevisaoCoordenacao', data, function( res ) {
                // atualizar o formulário da fundamentação
                if( params.numValidador ) {
                    // exibir aba
                    //$("#liTabRevisao"+params.numValidador).removeClass('hide');

                    // exibir o icone da aba
                    $("#tabRevisaoMarker"+params.numValidador).removeClass('hide');

                    // resultado
                    $("#dsResultadoRevisao"+params.numValidador).val($resultado.find('option:selected').text().trim());
                    $("#sqResultadoRevisao"+params.numValidador).val($resultado.val());
                    $("#coResultadoRevisao"+params.numValidador).val($resultado.find('option:selected').data('codigo'));
                    // categoria
                    if( data.sqCategoriaSugerida) {
                        $("#dsCategoriaRevisao" + params.numValidador).val($categoriaSugerida.find('option:selected').text().trim());
                        $("#sqCategoriaRevisao" + params.numValidador).val($categoriaSugerida.val());
                    } else {
                        $("#dsCategoriaRevisao" + params.numValidador).val('');
                        $("#sqCategoriaRevisao" + params.numValidador).val('');
                    }
                    // criterio
                    $("#dsCriterioRevisao"+params.numValidador).val(data.deCriterioSugerido);
                    if( data.stPossivelmenteExtinta ) {
                        $("#stPossivelmenteExtintaRevisao" + params.numValidador).removeClass('hide')
                    } else {
                        $("#stPossivelmenteExtintaRevisao" + params.numValidador).addClass('hide')
                    }
                    // justificativa
                    $("#txJustificativaRevisao"+params.numValidador).html( data.txJustificativa );
                }
                app.closeWindow('pnlCadRevisaoCoordenacao-'+params.sqFicha);
            });

        })
    },
    deleteRevisaoCoordenacao:function( params ){
        app.confirm("Confirma exclusão da revisao?", function() {
            app.ajax(app.url + 'gerenciarValidacao/deleteRevisaoCoordenacao', params, function( res ) {
                if( res.status == 0 ) {
                    app.reset("#frmRevisaoCoordenacao" + params.sqFicha );
                    if( params.numValidador ){
                        // esconder a aba
                        //$("#liTabRevisao"+params.numValidador).addClass('hide');

                        // selecionar  a aba do Validador
                        app.selectTab("liTabResultado"+params.numValidador);

                        // esconder o icone da aba
                        $("#tabRevisaoMarker"+params.numValidador).addClass('hide');
                        // resultado
                        $("#dsResultadoRevisao"+params.numValidador).val('');
                        $("#sqResultadoRevisao"+params.numValidador).val('');
                        $("#coResultadoRevisao"+params.numValidador).val('');
                        // categoria
                        $("#dsCategoriaRevisao"+params.numValidador).val('');
                        $("#sqCategoriaRevisao"+params.numValidador).val('');
                        // criterio
                        $("#dsCriterioRevisao"+params.numValidador).val('');
                        $("#stPossivelmenteExtintaRevisao" + params.numValidador).addClass('hide')
                        // justificativa
                        $("#txJustificativaRevisao"+params.numValidador).html('');
                    }
                    app.closeWindow('pnlCadRevisaoCoordenacao-'+params.sqFicha);
                }
            });
        })
    },
    // fim módulo revisao coordenacao

    // Modulo Convidar Terceiro Validador
    showFormConvidarValidador:function(params){
        params = params || {}
        if( ! params.sqOficinaFicha ){
            return;
        }
        var data = app.params2data(params, {} )

        app.panel('pnlCadValidador-' + data.sqOficinaFicha
            , 'Validação - Convidar Validador'
            , 'gerenciarValidacao/getFormConvidarValidador'
            , data, function () {
                // onOpen - configurar a tela
                var mensagemPadrao='<p>Prezado {nome},<br>' +
                    'Você está sendo convidado a contribuir na validação de: <b>{especie}</b>.<br>' +
                    'Obrigado pela disponibilidade.</p>' +
                    '<p>Atenciosamente,<br>' +
                    'Equipe SALVE</p>';
                var emailValidador = app.lsGet('salve-msg-email-validador',mensagemPadrao);
                emailValidador = emailValidador.replace(/\{especie\}/g,data.noCientifico)
                $("#deAssuntoEmailValidador").val('Convite para validação de ficha da espécie ' + $('<span>'+data.noCientifico+'</span>').text() )
                if( emailValidador ){
                    setTimeout(function() {
                        tinymce.activeEditor.setContent( emailValidador );
                        $("#txMensagemEmailValidador").val(emailValidador);
                    },3000);
                }
            },800,600,true,false,function() {
                // onClose
                tinymce.remove('#txMensagemEmailValidador');
                // passar sqFicha para o gride para que somente a linha correspondente seja atualizada
                gerenciarValidacao.acomp.updateGrid({ sqFicha:params.sqFicha});
            },'center','#384b38',false,false)
    },
    saveFormConvidarValidador:function(params){
        var erros = [];
        var nmCientifico = $("#nmCientificoConvidarValidador").val();
        if( !params.sqPessoaValidador ){
            erros.push('Validador não selecionado');
        }
        if( params.deEmailValidador && ! params.deAssuntoEmailValidador ){
            erros.push('Assunto do email não informado');
        }
        if( !params.deEmailValidador && params.deAssuntoEmailValidador ){
            erros.push('Email do validador não informado');
        }
        if( !params.txMensagemEmailValidador && params.deEmailValidador ){
            erros.push('Mensagem do email dnão informada');
        }
        if( params.deEmailValidador && ! String(params.deEmailValidador).isEmail() ) {
            erros.push('Email inválido');
        }
        if( erros.length > 0 ){
            app.alertInfo( erros.join('<br>'),'Erro(s) de preenchimento');
            return;
        }
        app.confirm('Confirma gravação?',function(){
            app.ajax(app.url + 'gerenciarValidacao/saveFormConvidarValidador', params, function(res) {
                if( res.status==0 ){
                    if( nmCientifico ) {
                        var regex = new RegExp(nmCientifico, 'g');
                        var mensagem = params.txMensagemEmailValidador.replace(regex, '{especie}');
                        app.lsSet('salve-msg-email-validador', mensagem);
                    }
                    app.closeWindow('pnlCadValidador-' + params.sqOficinaFicha);
                    gerenciarValidacao.updateGridConvites();
                }
            }, 'Gravando...', 'JSON');
        });
    },

    // Fim Modulo Convidar Terceiro Validador



    /**
     * atualizar os campos select de oficinas nas abas de convidar validador e acompanhamento
     */
    updateSelectsOficinas:function() {
        var curValAbaValidador  = $("#sqOficinaValidacao").val();
        var curValAbaAcomp      = $("#sqOficinaValidacaoAcomp").val();
        var data   = { "sqCicloAvaliacao" : $("#sqCicloAvaliacao").val() };
        app.ajax(app.url + 'gerenciarValidacao/selectOficinas', data, function(res) {
            $('#sqOficinaValidacao,#sqOficinaValidacaoAcomp').empty();
            $('#sqOficinaValidacao').append('<option value="">-- selecione --</option>');
            $('#sqOficinaValidacaoAcomp').append('<option value="">-- selecione --</option>');
            $.each(res, function( key, value ) {
                $('#sqOficinaValidacao').append('<option value="' + value['id'] + '">' + value['sgOficina']+' - '+value['periodo']+ '</option>');
                $('#sqOficinaValidacaoAcomp').append('<option value="' + value['id'] + '">' + value['sgOficina']+' - '+value['periodo']+ '</option>');
            });
        });
        $("#sqOficinaValidacao").val(curValAbaValidador);
        $("#sqOficinaValidacaoAcomp").val(curValAbaAcomp);
    },
    tabClick: function(tab) {
        return true;
    },
    checkUncheckAll: function(params, elem, evt) {
        checkUncheckAll(params, elem , evt );
        /*
        var checked;
        if ($(elem).hasClass('glyphicon-unchecked')) {
            checked = true;
            $(elem).removeClass('glyphicon-unchecked').addClass('glyphicon-check');
        } else {
            checked = false;
            $(elem).removeClass('glyphicon-check').addClass('glyphicon-unchecked');
        };
        $(elem).closest('thead').next().find('input:checkbox').each(function() {
            this.checked = checked;
        });
         */
    },
    sqCicloAvaliacaoChange: function(params) {
        gerenciarValidacao.sqCicloAvaliacao = $( gerenciarValidacao.container + " #sqCicloAvaliacao").val();
        gerenciarValidacao.clearTabs();
        // atualizar dashboard ou esconde-lo
        gerenciarValidacao.updateDashboard();
        if (gerenciarValidacao.sqCicloAvaliacao) {
            $("#containerOficina").show();
            //$('#tabCadastro').show();
            $("#divTabs").removeClass('hidden');

            // gravar ciclo selecionado
            app.saveField('sqCicloAvaliacao');

            // se não exitir a aba Cadastro, carregar a aba Acompanhamento
            if( $("#liTabCadastro").size() == 1 ) {
                var data = {
                    sqCicloAvaliacao: params.sqCicloAvaliacao
                    /* Solicitado remoção de filtro pendendia em reunião 04/11/2022 - CAGU */
                    , hideFilters: 'situacao,pendencia'
                    , callback: 'gerenciarValidacao.updateGridFichaCadastro'
                    , onLoad: 'gerenciarValidacao.updateGridFichaCadastro'
                };
                // carregar o formulario de filtros da ficha e o gride das fichas
                app.loadModule(baseUrl + 'ficha/getFormFiltroFicha', data, '#gerenciarValidacaoContainer #divFiltrosFichaCadastro');
                // atualizar o gride com as oficinas de validação cadastradas
                gerenciarValidacao.updateGridOficinas(params);
            }
            else
            {
                app.selectTab('liTabAcompanhamento');
            }
        } else {
            //$('#tabCadastro').hide();
            $("#divTabs").addClass('hidden');
            $("#containerOficina").hide();
        };
    },

    // ABA CADASTRO

    /**
     * atualizar o gride com as fichas que serão adicionadas na oficina de validação
     * @param params
     */
    updateGridFichaCadastro: function( filtros ) {
        if( gerenciarValidacao.sqCicloAvaliacao ) {
            // fazer a consulta ajax somente se o formulário estiver visível
            if( $( gerenciarValidacao.container + " #frmCadOficina").hasClass('hidden') )
            {
                return;
            }
            var data = filtros || {};
            data.fldAmeacadasFiltro = gerenciarValidacao.filtroAmeacadas;
            var $grid = $( gerenciarValidacao.container+" #divGridFichasCadastro" );
            data.sqCicloAvaliacao = gerenciarValidacao.sqCicloAvaliacao;
            $grid.html('<h3>Carregando. ' + window.grails.spinner + '</h3>');
            app.ajax(app.url + 'gerenciarValidacao/getGridFichasCadastro', data,
                function (res) {
                    $grid.html(res);
                    // ativar seleção de linhas com shift+click
                    $grid.shiftSelectable();
                }, null, 'html');
        };
    },

    /**
     * função callback ao criar o gride
     * @param params
     */
    gridOficinasLoad:function( params ){

    },
    /**
     * exportar para planilha o gride das oficinas
     * @param params
     */
    exportToPlanilha:function(params){
        app.confirm('Confirma a exportação para planilha?',function(){
            oficina.updateGrid({format:'xls'});
        });
    },
    /**
     * atualizar o gride das oficinas
     * @param params
     */
    updateGridOficinas: function( params ) {
        params =  params || {};
        var data = app.params2data(params,{});
        params.sqCicloAvaliacao = $("#sqCicloAvaliacao").val();
        delete data.action;
        app.grid('GridOficinas', data);
        return;


        var sqCicloAvaliacao = $("#sqCicloAvaliacao").val();
        var posicao = $("#containerOficina #divGridOficinas").scrollTop();
        var noCientificoFiltrarOficina = $("#fldNoCientificoFiltrarOficinaAbaCadastro").val();
        var $tr=null;
        if( params.sqOficina ) {
            $tr = $("#gdOficinasRow-" + params.sqOficina)
            $tr.html('Atualizando...');
        } else {
            $("#divGridOficinas").html('<b><br>Carregando oficinas...</b>');
        }

        if (sqCicloAvaliacao) {
            app.ajax( baseUrl + 'gerenciarValidacao/getGridOficinas', {
                sqOficina:params.sqOficina,
                sqCicloAvaliacao: sqCicloAvaliacao,
                noCientificoFiltrarOficina: noCientificoFiltrarOficina
            }, function(res) {
                if( $tr != null )
                {
                    // esconder o formulário se tiver visivel
                    if( $("#containerOficina #frmCadOficina").is(':visible')) {
                        $(gerenciarValidacao.container + " #btnShowForm").click();
                    }
                    $tr.html( $( res ).find('tr#gdOficinasRow-'+params.sqOficina).html() );
                    gerenciarValidacao.datatable.rows($tr).invalidate().draw();
                }
                else {
                    $("#containerOficina #divGridOficinas").html(res);
                    $("#containerOficina #divGridOficinas").scrollTop(posicao);
                    if ($("#containerOficina #canModify").val() == 'false') {
                        $("#containerOficina #frmCadOficinaContainer").hide();
                    } else {
                        $("#containerOficina #frmCadOficinaContainer").show();
                    }
                    gerenciarValidacao.datatable = $('#tableDivGridOficinas').DataTable($.extend({}, default_data_tables_options,
                        {
                            "order": [3, 'desc'], // ordem inicial pelo período descendente
                            "columnDefs": [
                                {"orderable": false, "targets": [0, 7]}
                            ]
                        })
                    );
                }
            }, '', 'HTML');
        }
    },

    resetFiltroOficinasAbaCadastro:function(params ) {
        $("#fldNoCientificoFiltrarOficinaAbaCadastro").val('');
        gerenciarValidacao.updateGridOficinas(params);
    },
    filtrarOficinaAbaAcompanhamento : function( params ){
        var $selectOficinas = $("#sqOficinaValidacaoAcomp");
        var $inputNomeCientifico = $("#fldNoCientificoFiltrarOficinaAbaAcompanhamento");
        var data   = { "sqCicloAvaliacao" : $("#sqCicloAvaliacao").val(),'noCientificoFiltrarOficina':$inputNomeCientifico.val() };
        // limpar o filtro
        if( !data.noCientificoFiltrarOficina){
            gerenciarValidacao.resetFiltroOficinasAbaAcompanhamento();
            return;
        }
        $("#sqOficinaValidacaoAcomp").val('');
        $("#sqOficinaValidacaoAcomp").change();
        $("#sqOficinaValidacaoAcomp option:gt(0)").addClass('hidden');
        app.ajax(baseUrl + 'gerenciarValidacao/selectOficinas', data, function(res) {
            app.unblockUI();
            if ( res && res.length > 0 ) {
                var $select = $("#divSelectOficinaValidacaoAcomp #sqOficinaValidacaoAcomp");
                var sqOficina='';
                $.each(res, function( key, value ) {
                    $select.find("option[value="+value['id']+"]").removeClass('hidden');
                    sqOficina = value['id'];
                });
                if( sqOficina ) {
                    // definir o filtro de ficha com o nome da especie
                    $("#divFiltroAbaAcompanhamento input[name=nmCientificoFiltro]").val( data.noCientificoFiltrarOficina);
                    $select.val( sqOficina );
                    $select.change(); // sqOficinaValidacaoChange
                    gerenciarValidacao.acomp.updateGrid();
                }
            } else {
                app.growl('Nenhuma oficina encontrada com: <b>"'+data.noCientificoFiltrarOficina+'"</b>',3,'Localizar oficina','tc','large','info');
            }
        },'Aguarde...','json');
    },


    resetFiltroOficinasAbaAcompanhamento : function(params ) {
        if( $("#fldNoCientificoFiltrarOficinaAbaAcompanhamento").val() ) {
            $("#fldNoCientificoFiltrarOficinaAbaAcompanhamento").val('');
            $("div#divFiltroAbaAcompanhamento input[name=nmCientificoFiltro]").val('');
            gerenciarValidacao.acomp.sqOficinaValidacaoChange();
        }
        $("div#divFiltroAbaAcompanhamento").removeClass('filtered');
        $("#sqOficinaValidacaoAcomp option").removeClass('hidden');

    },
    /**
     * salvar oficina de validação
     * @param params
     */
    save: function(params) {
        if (!$("#frmCadOficina").valid()) {
            return;
        }

        // verificar se tem alguma ficha selecionada
        /*if ( $("#divGridFichasCadastro input:checkbox:checked").size() == 0) {
            app.alertInfo('Nenhuma ficha selecionada!');
            return;
        }
        */

        // data final deve ser maior que a inicial
        var data = $("#frmCadOficina").serializeAllArray();
        app.params2data(params, data);

        data.ids = []
        $("#divGridFichasCadastro input:checkbox:checked").each(function() {
            data.ids.push(this.value);
        });
        data.ids = data.ids.join(',');

        app.ajax(baseUrl + 'gerenciarValidacao/saveOficina', data, function(res) {
            if (res.status == 0) {
                gerenciarValidacao.resetForm();
                app.resetChanges('tabCadastro');
                gerenciarValidacao.updateGridOficinas({sqOficina:data.sqOficina});
                gerenciarValidacao.updateCombosOficinas();
                // aplicar o filtro novamente
                $("#frmCadOficina *[id^=btnFiltroFicha]:first").click();
            }
        }, 'Gravando...', 'json');
    },
    edit: function(params) {
        if (!params.id) {
            return;
        }
        gerenciarValidacao.resetForm();
        app.ajax(baseUrl + 'oficina/editOficina', {
            sqOficina: params.id
        }, function(res) {
            if (res) {
                if ($("#frmCadOficina").hasClass('hidden')) {
                    $("#btnShowForm").click();
                }
                app.setFormFields(res,'frmCadOficina');
            }
        }, 'Carregando...', 'json');
    },
    delete: function(params, btn) {
        app.selectGridRow(btn);
        app.confirm('<br>Confirma exclusão?<br><br><b>' + params.descricao + '</b>', function() {
                app.confirm('<br>O registro: <b>'+ params.descricao + '</b> será excluída completamente.<br><br><span class="bold red">Tem certeza?</span>', function() {
                    app.ajax(baseUrl + 'oficina/delete',
                        params,
                        function (res) {
                            if (res.status == 0) {
                                gerenciarValidacao.resetForm();
                                gerenciarValidacao.updateGridOficinas({sqOficina: params.sqOficina});
                            }
                        });
                },function(){
                    app.unselectGridRow(btn);
                },params,'ATENÇÃO!','danger')
            },
            function() {
                app.unselectGridRow(btn);
            }, params, 'Confirmação', 'warning');
    },
    resetForm: function() {
        app.reset("frmCadOficina");
        $("#frmCadOficina #noOficina").val('Oficina de validação de ');
        app.focus('noOficina','frmCadOficina');
    },
    linkToggleFichasClick:function( params,elem,evt ){
        evt.preventDefault();
        var data = $(elem).data();
        // se não tiver expandido ler as fichas
        if(! $( $(elem).attr('href')).hasClass('in') ) {
            gerenciarValidacao.getListaFichasOficina(data.rowNum, data.id)
        }
    },
    getListaFichasOficina:function( rowNum, sqOficina){
        var div = $("#divFichas-"+rowNum);
        div.html('<h4>Carregando...'+window.grails.spinner+'</h4>');
        var data = {sqOficina:sqOficina,rowNum:rowNum}
        app.ajax( app.url + 'gerenciarValidacao/getListaFichasOficina', data,
            function(res) {
                div.html(res);
                //var rows = div.find('input[type=checkbox]').size();
                var rows = div.find('li').size();
                $("#a-"+rowNum).find('span').html(rows + ( rows > 1 ? ' Fichas':' Ficha') );

                // aplicar seleção de linhas por intervalo com a tecla shift
                div.shiftSelectable();

            });
    },
    deleteFichaOficina:function( params ) {
        app.ajax( app.url + 'gerenciarValidacao/deleteFichaOficina', params,
            function(res) {
                if( res.status== 0 )
                {
                    gerenciarValidacao.getListaFichasOficina(params.rowNum, params.sqOficina);
                    gerenciarValidacao.updateGridOficinas(params);
                }
            });

    },
    sqSituacaoChange: function(params, select) {
        var codigo = $(select).find('option:selected').data('codigo');
        var novoValor = select.value;
        params.sqSituacao = novoValor;
        app.ajax(baseUrl + 'oficina/changeSituacao', params, function(res) {
            if (res.status == 0) {
                if (codigo != 'ABERTA') {
                    $(select).removeClass('green').addClass('red');
                } else {
                    $(select).removeClass('red').addClass('green');
                }
                gerenciarValidacao.updateGridOficinas( params );
            } else {
                $(select).find('option[data-codigo='+codigo+']').prop('selected',false);
                if (codigo == 'ABERTA') {
                    codigo = 'ENCERRADA'
                } else {
                    codigo = 'ABERTA'
                }
                $(select).find('option[data-codigo='+codigo+']').prop('selected',true);
            }
        }, '', 'json');
    },
    updateCombosOficinas:function( params )
    {
        //var comboAbaConvidar = $("#sqOficinaValidacao");
        //var comboAbaAcomp    = $("#sqOficinaValidacaoAcomp");
        gerenciarValidacao.clearTabs();

    },
    /**
     * gerar fichas no formato doc para possbilitar fazer a validaçao
     * quando hover problemas de conexao com a internet
     * @param params
     */
    gerarZipFichas:function( params )
    {
        if( !params.id )
        {
            app.alertInfo('Nenhuma oficicina selecionada!');
            return;
        }

        var data = { ids : [], sqOficina : params.id };
        // pegar os ids das fichas selecionadas
        $("#gdOficinasRow-"+params.id).find('input:checkbox:checked').each(function(i,item){
            data.ids.push( $(item).data('sqFicha') );
        });

        $.extend(data,params);
        delete data.id;
        data.adColaboracao  = 'N';
        data.adAvaliacao    = 'N';
        data.adValidacao    = 'S';
        app.gerarZipFichas(data);

        /*
        app.confirm('Confirma criaçao do arquivo zip das fichas para validaçao OFF-LINE?',
            function() {
                app.ajax(baseUrl + 'gerenciarValidacao/gerarZipFichas', data, function (res) {
                    if (res.status == 0) {};
                }, 'Gravando...', 'JSON');
            });
        */

    },



    // FIM ABA CADASTRO


    // ABA CONVIDAR VALIDADOR
    sqOficinaValidacaoAbaConvidarChange: function( params ) {
        var container = "#tabConvidar";
        var sqOficina = $(container+" #sqOficinaValidacao").val();

        if ($(container+" #canModify").val() != 'false' && sqOficina ) {
            $(container+" #frmEmailContainer,#frmCadValidadoresContainer,#sqOficinaValidacaoRefresh").show();
            if ( $("#liTabConvidar").size() == 0) {
                app.selectTab('liTabAcompanhamento');
            } else {
                if( ! tinymce.activeEditor || tinymce.activeEditor.id != 'txEmail' ) {
                    $('#acordeon_frmConvidarEmail').on('show.bs.collapse', function (e) {
                        var editorOptions = $.extend({}, default_editor_options, {
                            selector: '#txEmail',
                            toolbar: 'newdocument | ' + default_editor_options.toolbar,
                            setup: function (editor) {
                                editor.on('init', function (e) {
                                    var textoEmailValidadores = app.lsGet('textoEmailValidadores');
                                    if (textoEmailValidadores) {
                                        tinymce.get('txEmail').setContent(textoEmailValidadores);
                                    }else{
                                        var textoEmailValidadoresPadrao = "Prezado(a) {nome},";
                                        textoEmailValidadoresPadrao += "<br><br>Você está sendo convidado(a) para mais uma etapa de validação do Processo de Avaliação do ICMBio. Favor aceitar até o dia {prazo}.";
                                        textoEmailValidadoresPadrao += "<br><br>Obrigado pela sua colaboração!";
                                        textoEmailValidadoresPadrao += "<br><br>Segue a lista das fichas a serem validadas:";
                                        textoEmailValidadoresPadrao += "<br><br>{fichas}";
                                        textoEmailValidadoresPadrao += "<br><br>Atenciosamente,";
                                        textoEmailValidadoresPadrao += "<br><br>Coordenação de Avaliação do Risco de Extinção da Fauna COFAU / ICMBio";
                                        tinymce.get('txEmail').setContent(textoEmailValidadoresPadrao);
                                    }
                                });
                            },
                        });
                        tinymce.init(editorOptions);
                        app.focus('deAssuntoEmail');
                    });
                };
            };
        } else {
            $(container+" #frmEmailContainer,#frmCadValidadoresContainer,#sqOficinaValidacaoRefresh").hide();
            //app.showHideTab('tabAcompanhamento', true);
        };
        //gerenciarValidacao.sqCicloAvaliacaoChange( params );
        // limpar o gride de fichas de cima
        $(gerenciarValidacao.container + "#divGridFichas").html('');
        $(gerenciarValidacao.container + "#divGridConvites").html('');
        gerenciarValidacao.updateGridConvites();
        gerenciarValidacao.resetFrmCadValidadores();
        gerenciarValidacao.updateDashboardOficina();

        // aplicar filtro nos grupos de acordo com a oficina selecionada
        app.updateMultiSelectFiltroGruposAvaliados({container:'div#divFiltroAbaConvidar',selectName:'sqGrupoFiltro',sqOficina:sqOficina});


    },

    radioFiltroOficinaValidadorChange:function(params,ele,evt){
      var $select = $("#sqOficinaValidacao");
      var sqOficina= $select.val();
      if( ele.value == 'FE' ){
          $select.find('option[data-situacao=ABERTA]').hide();
          $select.find('option[data-situacao=ENCERRADA]').show();
      } else {
          $select.find('option[data-situacao=ABERTA]').show();
          $select.find('option[data-situacao=ENCERRADA]').hide();
      }
      if( sqOficina != ''  && ! $select.find("option[value="+sqOficina+"]").is(':visible') ) {
            $select.val('');
            $select.change();
      }
    },

    radioFiltroOficinaAcompChange:function(params,ele,evt){
      var $select = $("#sqOficinaValidacaoAcomp");
      var sqOficina= $select.val();
      if( ele.value == 'FE' ){
          $select.find('option[data-situacao=ABERTA]').hide();
          $select.find('option[data-situacao=ENCERRADA]').show();
      } else {
          $select.find('option[data-situacao=ABERTA]').show();
          $select.find('option[data-situacao=ENCERRADA]').hide();
      }
      if( sqOficina != ''  && ! $select.find("option[value="+sqOficina+"]").is(':visible') ) {
            $select.val('');
            $select.change();
      }
    },

    clearTabs: function() {
        app.selectTab('tabCadastro'); // selecionar a aba Cadastro
        try {
            tinymce.get('txEmail').remove();
        } catch (e) {}
        $(gerenciarValidacao.container + "#divTabs #tabContent > div").each(function(i,tab) {
            if( $(tab).attr('id') != 'tabCadastro') {
                $(this).html('');
            }
        });
    },

    ediltEmailParticipante:function( sqCicloAvaliacaoValidador,email)
    {
        var $spanWrapper = $("#span-edit-email-participante-"+sqCicloAvaliacaoValidador);
        var $spanText    = $("#span-edit-email-participante-text-"+sqCicloAvaliacaoValidador);
        $spanText.data('oldValue',$spanText.text() );
        $spanWrapper.hide();
        $spanWrapper.after('<div id="div-input-email-participante-'+sqCicloAvaliacaoValidador+'">' +
            '<input id="input-email-participante-'+sqCicloAvaliacaoValidador+'" class="fld form-control w90p ignoreChange" value="'+$spanText.text()+'" ' +
            'type="text"> <i class="fld fa fa-save ml10" title="Gravar" onClick="gerenciarValidacao.saveEmailParticipante('+sqCicloAvaliacaoValidador+',true)"></i> </div>');
        app.focus( 'input-email-participante-' + sqCicloAvaliacaoValidador );
        var $input = $("#input-email-participante-"+sqCicloAvaliacaoValidador);
        $input.on('keyup', function( e ) {
            if( e.key=='Enter' ) {
                $input.next().click();
            };
        });
    },
    saveEmailParticipante: function(sqCicloAvaliacaoValidador,email)
    {
        var $divInput    = $("#div-input-email-participante-"+sqCicloAvaliacaoValidador);
        var $spanWrapper = $("#span-edit-email-participante-"+sqCicloAvaliacaoValidador);
        var $spanText    = $("#span-edit-email-participante-text-"+sqCicloAvaliacaoValidador);
        var $input       = $("#input-email-participante-"+sqCicloAvaliacaoValidador);
        if( $input.val().trim().toLowerCase() != $spanText.data('oldValue').trim().toLowerCase() ) {
            var data = {sqCicloAvaliacaoValidador: sqCicloAvaliacaoValidador, deEmail: $input.val().trim().toLowerCase() }
            app.ajax(baseUrl + 'gerenciarValidacao/saveEmailParticipante',
                data,
                function (res) {
                    if (res.status == 0) {
                        $spanText.text(data.deEmail);
                    }
                }, null, 'JSON');
        }
        $divInput.remove();
        $spanWrapper.show();
    },

    // VALIDADORES

    updateGridConvites: function() {
        var sqCicloAvaliacao = $(gerenciarValidacao.container + "#sqCicloAvaliacao").val();
        var sqOficina = $("#tabConvidar #sqOficinaValidacao").val();
        if( !sqOficina || !sqCicloAvaliacao )
        {
            return;
        }
        $("#tabConvidar #divGridConvites").html('');
        var posicao = $(gerenciarValidacao.container + '#divGridFichas').scrollTop();
        if (sqCicloAvaliacao && sqOficina) {
            $(gerenciarValidacao.container + "#divGridConvites").html('Carregando...'+window.grails.spinner);
            app.ajax(baseUrl + 'gerenciarValidacao/getGridConvites', {
                sqCicloAvaliacao: sqCicloAvaliacao, sqOficina:sqOficina,
            }, function(res) {
                $(gerenciarValidacao.container + "#divGridConvites").html(res);
                $(gerenciarValidacao.container + "#divGridConvites").scrollTop(posicao);

                // fixar colunas com datatable
                var maxHeight = Math.max( parseInt(window.innerHeight - 300 ), 200);
                gerenciarValidacao.grideConvites =  $( gerenciarValidacao.container + '#tableGridConvites').DataTable( $.extend({},default_data_tables_options,
                    {
                        "deferRender"      : true
                        ,"scrollY"          : maxHeight
                        ,"scrollX"          : true
                        ,"scrollCollapse"   : true
                        ,"fixedHeader"      : false
                        ,"lengthChange"     : false
                        ,"order": []
                        ,"buttons": []
                        ,"columnDefs" : [
                            {"orderable": false, "targets": [0,1,2,3,4,5,7,8] },
                        ]
                    })
                );


                // ativar seleção de linhas com shift+click
                $(gerenciarValidacao.container + "#divGridConvites").shiftSelectable();

            }, '', 'HTML');
        }
    },

    saveValidadores: function(params) {
        if ($(gerenciarValidacao.container + "#divGridFichas input:checkbox:checked").size() == 0) {
            app.alertInfo('Nenhuma ficha selecionada!');
            return;
        };
        if (!$("#frmCadValidadores").valid()) {
            return;
        };
        var data = $("#frmCadValidadores").serializeArray();
        data.sqCicloAvaliacao = $(gerenciarValidacao.container + "#sqCicloAvaliacao").val();
        data.sqOficina = $(gerenciarValidacao.container + "#sqOficinaValidacao").val();
        data.saveType = 'validate';
        if( !data.sqOficina )
        {
            app.alertInfo('Nenhuma oficicina selecionada!');
            return;
        }
        data.ids = [];
        $(gerenciarValidacao.container + "#divGridFichas input:checkbox[id^=sqOficinaFicha]:checked").each(function() {
            data.ids.push(this.value);
        });
        var qtdFichas = data.ids.length;
        data.ids = data.ids.join(',');
        app.confirm('Confirma gravação?<br><b>'+qtdFichas+' ficha(s) selecionada(s).</b>',
            function() {
                gerenciarValidacao.saveValidadoresAjax(data);
            });
    },
    saveValidadoresAjax: function(params) {
        var data = app.params2data({},params)
        app.ajax(baseUrl + 'gerenciarValidacao/saveFrmValidadores', data, function (res) {
            if (res.status == 0) {
                if( res.fichasValidadorInvalidas ) {
                    var linhas = [];
                    for(key in res.fichasValidadorInvalidas){
                        var papeis = res.fichasValidadorInvalidas[key].papeis;
                        for( papel in papeis ) {
                            var dsPapel = papeis[papel].ds_papel;
                            if (papel == 'FACILITADOR') {
                                dsPapel = 'foi facilitador(a)';
                            } else if (papel == 'PONTO_FOCAL'){
                                dsPapel = 'é ponto focal'
                            } else if (papel == 'COORDENADOR_TAXON'){
                                dsPapel = 'é coordenador(a) de taxon'
                            }
                            var noPessoa = '<b>'+res.fichasValidadorInvalidas[key].no_pessoa + '</b>&nbsp;<b><span class="red">'+ dsPapel + '</span></b> de:';
                            var fichas = '&nbsp;- ' + papeis[papel].fichas.join('<br>&nbsp;- ');
                            linhas.push(noPessoa+'<br>'+fichas)
                        }
                    }
                    var modal = BootstrapDialog.show({
                        title: 'Restrições encontradas',
                        size: BootstrapDialog.SIZE_WIDE,
                        message: linhas.join('<br>'),
                        buttons: [{
                            label: 'Ignorar restrições',
                            cssClass: 'btn-success',
                            //icon: 'glyphicon glyphicon-floppy-save',
                            action: function(dialog) {
                                data.saveType='saveAll'
                                window.setTimeout(function(){
                                    dialog.close();
                                },200);
                                gerenciarValidacao.saveValidadoresAjax(data)
                            }
                        },{
                            label: 'Considerar restrições',
                            cssClass: 'btn-warning',
                            //icon: 'glyphicon glyphicon-floppy-saved',
                            action: function(dialog) {
                                data.saveType = 'saveValids';
                                window.setTimeout(function(){
                                    dialog.close();
                                },200);
                                gerenciarValidacao.saveValidadoresAjax(data)
                            }
                        }, {
                            label: 'Cancelar gravação',
                            cssClass: 'btn-danger',
                            //icon: 'glyphicon glyphicon-ban-circle',
                            action: function(dialog) {
                                dialog.close()
                            }
                        }]
                    })
                    var maxH = Math.max(300,window.innerHeight-300)
                    $(modal.getModalBody()[0]).css('max-height',maxH+'px')
                    return;
                }
                gerenciarValidacao.updateGridFichas();
                gerenciarValidacao.updateGridConvites();
                gerenciarValidacao.updateDashboard();
                //gerenciarValidacao.resetFrmCadValidadores();
            }
        }, 'Gravando...', 'JSON');
    },
    resetFrmCadValidadores: function() {
        app.reset('divNomesValidadores');
    },

    updateGridFichas: function(filtrosAtivos) {
        var container='#tabConvidar';
        var data = {
            sqCicloAvaliacao: gerenciarValidacao.sqCicloAvaliacao
        };
        filtrosAtivos = filtrosAtivos || app.hasActiveFilter('divFiltroAbaConvidar');
        // filtrar pelas oficinas de validação
        var sqOficina = $("#sqOficinaValidacao").val();
        if( ! sqOficina || filtrosAtivos.count == 0)
        {
            $(container + " #divGridFichas").html('<h4 class="red">Para selecionar ficha(s) é necessário informar algum critério de filtragem.</h4>');
            return;
        }
        filtrosAtivos.sqOficinaFiltro = sqOficina;

        // quando clica no botão Aplicar Filtros
        if ( filtrosAtivos ) {
            data = $.extend(data, filtrosAtivos);
        };

        var posicao = $(container + ' #divGridFichas').scrollTop();
        if ( sqOficina ) {
            $(container + " #divGridFichas").html('Carregando...' + window.grails.spinner);
            app.ajax(baseUrl + 'gerenciarValidacao/getGridFichasOficina', data, function(res) {
                $(container + " #divGridFichas").html(res);
                $(container + " #divGridFichas").scrollTop(posicao);
                // aplicar datatable

                $('#tableFichasConvidar').DataTable( $.extend({},default_data_tables_options,
                    {
                        "columnDefs" : [
                            {   "orderable": false, "targets": [1,2]
                            }],
                        "searching": true
                        ,"buttons": []
                    })
                );
                $("#tableFichasConvidar_filter").hide();

                // ativar seleção de linhas com shift+click
                $("#tableFichasConvidar").shiftSelectable();


            }, '', 'HTML');
        };
    },
    sqPessoaChange: function(params, ele) {
        var email = $(ele).data('email');
        $("#" + email).val('');
        if (params.select2.data()) {
            var data = params.select2.data()[0];
            if (data) {
                $("#" + email).val(data.email);
            };
        };
    },
    sendMail: function(params) {
        // verificar se tem algum validador selecionado
        tinymce.get('txEmail').save();
        var data = $(gerenciarValidacao.container + "#frmValidacaoEnviarEmail").serializeArray();
        data.ids = [];
        $(gerenciarValidacao.container + "#divGridConvites input:checkbox:checked").each(function() {
            data.ids.push(this.value);
        });
        if (data.ids.length == 0) {
            app.alert('Nenhum validador selecionado!');
            return;
        };
        if (!$(gerenciarValidacao.container + "#frmValidacaoEnviarEmail").valid()) {
            return;
        };
        var textoEmail = tinymce.get('txEmail').getContent().replace(/\n/g, '').replace(/<p>&nbsp;<\/p>/g, '').trim();
        if (!textoEmail) {
            app.alertInfo('Texto do email deve ser informado!');
            return;
        };
        data.sqOficina = $(gerenciarValidacao.container + "#sqOficinaValidacao").val()
        app.lsSet('textoEmailValidadores', textoEmail); // gravar no local storage do browser o ultimo texto
        app.confirm('<br>Confirma Envio de Email para ' + data.ids.length + ' validador(es)?',
            function() {
                data.ids = data.ids.join(',');
                app.ajax(baseUrl + 'gerenciarValidacao/sendEmailValidadores', data, function(res) {
                    if ( res.status == 0 ) {
                        gerenciarValidacao.updateGridConvites();
                        gerenciarValidacao.updateGridFichas();
                    };
                }, 'Enviando Email...', 'JSON');
            });
    },
    deleteConvite: function(params, btn) {
        app.selectGridRow(btn);
        app.confirm('<br>Confirma exclusão do(a) validador(a) <b>' + params.descricao + '</b>?',
            function() {
                app.ajax(app.url + 'gerenciarValidacao/deleteConvite',
                    params,
                    function(res) {
                        if (res) {
                            app.alertInfo(res);
                        } else {
                            gerenciarValidacao.updateGridFichas();
                            gerenciarValidacao.updateGridConvites();
                            gerenciarValidacao.updateDashboardOficina();
                            // se a aba de acompanhamento tiver carregada, atualizar o gride de acompanhamento
                            if( $("#sqOficinaValidacaoAcomp").val()) {
                                gerenciarValidacao.acomp.updateGrid();
                            }
                        };
                    });
            },
            function() {
                app.unselectGridRow(btn);
            }, params, 'Confirmação', 'warning');
    },

    /************************************************************
     * ABA ACOMPANHAMENTO
     *
     */
    acomp: {
        modalAberta: null,
        sqOficinaValidacao: '',
        inicializando:true,
        dataTable:null,
        init: function () {
            var container ="#tabAcompanhamento";
            gerenciarValidacao.acomp.sqOficinaValidacao = $(container+" #sqOficinaValidacaoAcomp").val();
            if (gerenciarValidacao.acomp.sqOficinaValidacao) {
                $(container+" #divTotais,#divTelaAcompContainer,#sqOficinaValidacaoAcompRefresh,#divFiltroAbaAcompanhamento").show();
                /*if( gerenciarValidacao.acomp.inicializando ) {
                    var wh = window.innerHeight;
                    var $div  = $(container+" #divTelaAcompContainer");
                    $div.height( Math.max(600,wh-121))
                }*/
                gerenciarValidacao.acomp.inicializando=false;
                gerenciarValidacao.acomp.updateGrid();
                //gerenciarValidacao.acomp.updateGridFichasLc(); // não tem mais este gride
            }
            else {
                gerenciarValidacao.acomp.inicializando=true;
                $(container+" #divTotais,#divGridAcompanhamento,#divGridFichasLc").html('');
                //$("#gerenciarValidacaoContainer #divGridFichasLc").html('');
                $(container + " #divTotais,#divTelaAcompContainer,#sqOficinaValidacaoAcompRefresh,#divFiltroAbaAcompanhamento").hide();
                //$(container+" #divTotais").html('');
            }
        },
        tabAcompanhamentoLoaded:function() {
            if( ! $("#sqOficinaValidacaoAcomp").val() ) {
                app.growl('Selecione a oficina.', 3, '', 'cc', 'medium', 'default')
            }
        },
        sqOficinaValidacaoChange: function (params) {
            gerenciarValidacao.acomp.init();
            gerenciarValidacao.acomp.updateGruposFiltro();
            gerenciarValidacao.acomp.updateNomesValidadoresFiltro();
        },
        updateGrid: function (params) {
            app.grid('GridAcompanhamento',params);
            gerenciarValidacao.acomp.updateTotals();


            /*
            params = params || {};
            //var data = app.hasActiveFilter('#gerenciarValidacaoContainer #divFiltroAbaAcompanhamento');
            var data = app.hasActiveFilter('#gerenciarValidacaoContainer #divFiltrosFicha'+params.sqFicha);
            if( data && data.count < 1 ) {
                var div = $('#gerenciarValidacaoContainer #divSelectOficinaValidacaoAcomp div[id^=divFiltrosFicha]');
                if( div.size() == 1 ) {
                    var id = div.attr('id');
                    if( id ) {
                        data = app.hasActiveFilter( id );
                    }
                }
            }
            var msgLoading = (params.async ? '' : 'Atualizando...');
            data.sqCicloAvaliacao = $('#gerenciarValidacaoContainer #sqCicloAvaliacao').val();
            //var posicao = $(window).scrollTop();
            var $tr;
            var nuLinhaAtual;
            if (params.sqFicha) {
                $tr = $('#gerenciarValidacaoContainer #divGridAcompanhamento #tr-' + params.sqFicha);
                nuLinhaAtual = $tr.find('td:first').html();
                $tr.html('<td colspan="7" class="tr-updating">Atualizando...&nbsp;<i class="glyphicon glyphicon-refresh spinning"></i></td>');
                msgLoading=''; // evitar bloquear a tela toda
            }
            data = app.params2data(data, params);
            // tem que ter uma oficina selecionada
            data.sqOficinaFiltro = gerenciarValidacao.acomp.sqOficinaValidacao;
            if (!data.sqOficinaFiltro) {
                return;
            }
            app.ajax(app.url + 'gerenciarValidacao/getGridAcompanhamento', data, function (res) {
                if (params.sqFicha) {
                    if ($tr.size() == 1) {
                        $tr.html( $(res).find('tr#tr-' + params.sqFicha).html() );
                        $tr.find('td:first').html(nuLinhaAtual);
                        //$('#divTableGridFichasAcomp').DataTable().rows($tr).invalidate().draw();
                        $('#divTableGridFichasAcomp').DataTable().rows($tr).invalidate('dom');
                    }
                    //$("#tabExecucao #divGridExeFichaLc").html(res);
                } else {
                    $("#gerenciarValidacaoContainer #divGridAcompanhamento").html(res);

                    gerenciarValidacao.acomp.dataTable = $('#divTableGridFichasAcomp').DataTable($.extend({}, default_data_tables_options,
                        {
                            "paging": true
                            ,"pageLength": 100
                            ,"processing": true
                            ,"lengthChange": false

                            ,"deferRender"      : true
                            ,"columnDefs": [
                                {"searchable": false, "targets": 0},
                                {"orderable": false , "targets": [0,5,6]}
                            ],
                        })
                    );

                    $(window).scrollTop(99999);

                    // adicionar evento de filtro customizado para a coluna respostas
                    $.fn.dataTable.ext.search.push(function (settings, searchData, index, rowData, counter) {
                        var search = $('#selectRespostasFiltro').val();
                        if (search==null) {
                            return true;
                        }
                        var data = $(rowData[6]).text();
                        var qtdA = (data.match(/a\)/g) || []).length;
                        var qtdB = (data.match(/b\)/g) || []).length;
                        var qtdC = (data.match(/c\)/g) || []).length;
                        var resultado = false
                        search.forEach(function(item,cnt) {
                            if( ! resultado ) {
                                if (item == 'a-a' && qtdA == 2) {
                                    resultado = true;
                                }
                                if (item == 'b-b' && qtdB == 2) {
                                    resultado = true;
                                }
                                if (item == 'c-c' && qtdC == 2) {
                                    resultado = true;
                                }
                                if (item == 'a-b' && qtdA == 1 && qtdB == 1) {
                                    resultado = true;
                                }
                                if (item == 'a-c' && qtdA == 1 && qtdC == 1) {
                                    resultado = true;
                                }
                                if (item == 'b-c' && qtdB == 1 && qtdC == 1) {
                                    resultado = true;
                                }
                                if (item == '' && qtdA == 0 && qtdB == 0 && qtdC == 0) {
                                    resultado = true;
                                }
                            }
                        });
                        return resultado;
                    });
                }
                gerenciarValidacao.acomp.updateTotals();
            }, msgLoading, 'html');
            */
        },
        updateTotals: function ( params ) {
            params = params || {};
            if( event && event.target.tagName.toString() == 'TH' ) {
                var target = $(event.target);
                var targetData = $(target).data();
                if (targetData.sortOrder) {
                    if (targetData.sortOrder == 'asc') {
                        targetData.sortOrder = 'desc';
                    } else {
                        targetData.sortOrder = 'asc';
                    }
                } else {
                    targetData.sortOrder = 'asc'
                }
                $(target).data('sortOrder', targetData.sortOrder);
                params.sortOrder = targetData.sortOrder;
            }
            var data = app.params2data(params,{});
            data.sqCicloAvaliacao = $("#sqCicloAvaliacao").val();
            data.sqOficina = gerenciarValidacao.acomp.sqOficinaValidacao;
            app.ajax(app.url + 'gerenciarValidacao/getTabelaTotais', data, function (res) {
                $("#divTotais").html( res );
            });
        },
        /**
         * Exibir somente os grupos que fazem parte das fichas da oficina selecionada
         */
        updateGruposFiltro: function () {
            app.updateMultiSelectFiltroGruposAvaliados({container:'div#tabAcompanhamento',selectName:'sqGrupoFiltro',sqOficina:gerenciarValidacao.acomp.sqOficinaValidacao});
        },
        /**
         * Mostrar somente os nomes dos validadores da oficina selecionada no campo select de filtro pelo nome do validador
         */
        updateNomesValidadoresFiltro:function() {
            var data = {sqOficina: gerenciarValidacao.acomp.sqOficinaValidacao}
            //var $list = $("#fldNoValidadorFiltroGridAcompanhamentoList");
            var $select = $("#fldNoValidadorFiltroGridAcompanhamento");
            var valorAtual = $select.val();

            // limpar a lista
            $select.html('')
            app.ajax(app.url + 'gerenciarValidacao/getNomesValidadoresOficina', data, function (res) {
                if (res.status == 0) {
                    $select.append('<option value=""></option>');
                    res.nomes.map(function (nome, i) {
                        var selected = (nome==valorAtual) ? ' selected':'';
                        $select.append('<option value="' + nome +'"'+selected+'>' + nome + '</option>');
                    })
                }
            });
        },
        /**
         * atualizar o gride quando o nome do validador for selecionado para filtragem
         */
        filtroNomeValidadorChange:function(){
            gerenciarValidacao.acomp.updateGrid();
        },

        // exportar o gride de acompanhamento para planilha
        gridCsv:function( params ){
            var data = app.params2data( params,{});
            var gridId = data.gridId;
            // exibir dialog de confirmaçao
            if( ! data.dlgConfirm ) {
                return app.confirmWithEmail('Confirma a exportação?',gerenciarValidacao.acomp.gridCsv,data);
            }
            app.grid(gridId,{format:'csv',email:currentUser.email},function( res ){
                if( res.msg ) {
                    if( res.status==1) {
                        currentBoostrapModal = app.alert(res.msg, 'Aviso');
                    } else {
                        currentBoostrapModal = app.alertInfo(res.msg + '<br><br>Clique em Fechar para continuar os trabalhos.', 'Exportando fichas...' + '&nbsp;<i class="fa fa-spinner fa-spin"></i>'
                            , getWorkers, null, 15);
                    }
                }
                setTimeout(function(){getWorkers();},5000);
            });
        },

        filtrarRespostas:function(elm, e){

            gerenciarValidacao.acomp.dataTable.draw();
            //gerenciarValidacao.acomp.dataTable.column(6).search('a)').draw();
        },
        updateGridFichasLc: function () {

            return; // gride desativado, estão todas no mesmo gride de cima
            /*
            var data = {};
            data.sqCicloAvaliacao = $('#gerenciarValidacaoContainer #sqCicloAvaliacao').val();
            if (!data.sqCicloAvaliacao) {
                return;
            };

            // tem que ter uma oficina selecionada
            data.sqOficinaFiltro = gerenciarValidacao.acomp.sqOficinaValidacao;
            if( !data.sqOficinaFiltro )
            {
                return;
            }
            // quando passar o sqFicha é para atualizar somente 1 linha especifica
            // if( ! data.sqOficinaFiltro && !data.sqFicha ) {
            //     data.listOficinasFiltro = [];
            //     $("#divFiltroAbaAcompanhamento select[name=sqOficinaFiltro] option").each(function (i, option) {
            //         if (option.value) {
            //             data.listOficinasFiltro.push(option.value)
            //         }
            //     });
            //     data.listOficinasFiltro = data.listOficinasFiltro.join(',');
            // }
            //
            var posicao = $('body').scrollTop();
            app.ajax(app.url + 'gerenciarValidacao/getGridFichasLc', data, function(res) {
                $("#gerenciarValidacaoContainer #divGridFichasLc").html(res);
                $("body").scrollTop(posicao);
            }, '', 'html');

*/
        },

    },

    // FIM ABA ACOMPANHAMENTO
    sqCategoriaIucnChange: function(params) {
        app.categoriaIucnChange({container:'frmCadFundamentacao' + params.sqFicha});

        //mostrar/esconder os dados de mudanca de categoria
        if( params.sqFicha )
        {
            var sqFicha = params.sqFicha;
            var sqCategoriaFinal = $("#sqCategoriaFinal").val();
            var sqCategoriaAnterior = $("#sqCategoriaUltimaAvaliacaoNacional" + sqFicha).val();
            var cdCategoriaAnterior = $("#cdCategoriaUltimaAvaliacaoNacional" + sqFicha).val();
            var $divMotivoMudanca = $("#divDadosMotivoMudanca"+sqFicha);
            if ( parseInt(sqCategoriaFinal) != parseInt( sqCategoriaAnterior ) ) {
                $divMotivoMudanca.show('fast');
                updateGridMotivoMudanca(params);
            } else {
                $divMotivoMudanca.hide('fast');
            }
        }


        /*var contexto = '#frmCadFundamentacao' + params.sqFicha + ' ';
        //var sqCategoriaUltimaAvaliacao = $(contexto + "#sqCategoriaIucnUltimaAvaliacao" + params.sqFicha).val();
        var sqCategoriaIucn = $(contexto + "#sqCategoriaFinal" + params.sqFicha).val();
        // se a categoria for 'VU,CR,EN não exibir o campo critério'
        if (!/EN|VU|CR/.test($(contexto + "#sqCategoriaFinal" + params.sqFicha + " option:selected").data('codigo'))) {
            $(contexto + "#dsCriterioAvalIucnFinal" + params.sqFicha).parent().addClass('hide');
        } else {
            $(contexto + "#dsCriterioAvalIucnFinal" + params.sqFicha).parent().removeClass('hide');
        };
        if (/CR/.test($(contexto + "#sqCategoriaFinal" + params.sqFicha + " option:selected").data('codigo'))) {
            $(contexto + "#stPossivelmenteExtinta" + params.sqFicha).closest('div').removeClass('hide');
        } else {
            $(contexto + "#stPossivelmenteExtinta" + params.sqFicha).closest('div').addClass('hide');
        };
        */
    },

    showFormFundamentacao: function(params) {
        if (!params.sqFicha) {
            return;
        }
        params.canModify = ( params.canModify.toString() == 'false' || params.canModify  == 'false' ? false : true );
        var data = {};
        data = app.params2data(params, data);
        params.container = 'pnlTelaFundamentacao' + data.sqFicha;
        app.panel( params.container
            , 'Fundamentação ' + data.noCientifico
            , 'gerenciarValidacao/getFormFundamentacao', data, function() {
                var sqFicha = data.sqFicha;
                if (!sqFicha) {
                    app.alertInfo('Id da ficha não informado.');
                    return;
                } else {
                    // iniciar as requisições para atualizar as mensagens do chat na tela do usuário
                    salveWorker.startChat({ sqFicha:data.sqFicha, sqOficinaFicha:data.sqOficinaFicha} );
                    gerenciarValidacao.sqCategoriaIucnChange(params);
                    app.categoriaIucnChange(params);
                }
                // configurar eventos nos grupos de exibição dos dados da ficha completa
                $('#divWrapperFichaCompleta_' + sqFicha + ' .panel-colaboracao').on('show.bs.collapse', function(e) {
                    var data = $(e.currentTarget).data();
                    var id = e.currentTarget.id;
                    var btnId = id.replace(/panel-/, 'btn-');
                    // exibir a imagem Principal do mapa de distribuição
                    if (data.urlImagem) {
                        var tagImg = $(e.currentTarget).find('img[src=""]');
                        if (tagImg.size() == 1) {
                            tagImg.attr('src', data.urlImagem);
                        }
                    }
                    //-------------------------------------
                    if (data.mapa == 'S') {

                        if( $("#div-mapa-ocorrencias-"+sqFicha).data('loaded') != 'S' ) {
                            // aguardar expander a div
                            window.setTimeout(function () {
                                exibirMapa(data.sqFicha, 'validacao');
                                updateGridOcorrencias({sqFicha: sqFicha, contexto: 'validacao'});
                            }, 1500)
                        }
                    } else if (data.habitoAlimentar == 'S') {
                        var div = $('#div-gride-habito-alimentar-' + data.sqFicha);
                        div.html('<h4>Carregando tabela...</h4>');
                        app.ajax(baseUrl + 'fichaCompleta/getGridHabitoAlimentar', data, function (res) {
                            div.html(res);
                        }, '', 'HTML');
                    } else if (data.habitat == 'S') {
                        var div = $('#div-gride-habitat-' + data.sqFicha);
                        div.html('<h4>Carregando tabela...</h4>');
                        app.ajax(baseUrl + 'fichaCompleta/getGridHabitat', data, function(res) {
                            div.html(res);
                        }, '', 'HTML');
                    } else if (data.interacao == 'S') {
                        var div = $('#div-gride-interacao-' + data.sqFicha);
                        div.html('<h4>Carregando tabela...</h4>');
                        app.ajax(baseUrl + 'fichaCompleta/getGridInteracao', data, function(res) {
                            div.html(res);
                        }, '', 'HTML');
                    }
                });
                $('#divWrapperFichaCompleta_' + sqFicha + ' .panel-grupo').on('show.bs.collapse', function(e) {
                    var data = $(e.currentTarget).data();
                    //var id = e.currentTarget.id;
                    if (data.ameaca == 'S') {
                        var div = $('#div-gride-ameaca-' + data.sqFicha);
                        div.html('<h4>Carregando tabela...</h4>');
                        app.ajax(baseUrl + 'fichaCompleta/getGridAmeaca', data, function(res) {
                            div.html(res);
                        }, '', 'HTML');
                    } else if (data.uso == 'S') {
                        var div = $('#div-gride-uso-' + data.sqFicha);
                        div.html('<h4>Carregando tabela...</h4>');
                        app.ajax(baseUrl + 'fichaCompleta/getGridUso', data, function(res) {
                            div.html(res);
                        }, '', 'HTML');
                    } else if (data.conservacao == 'S') {
                        app.ajax(baseUrl + 'fichaCompleta/getHistoricoAvaliacoes', data, function(res) {
                            var fieldset = $('#fieldset-ultima-avaliacao-nacional-' + data.sqFicha);
                            fieldset.find('div#avaliada').addClass('hidden');
                            fieldset.find('div#nao-avaliada').removeClass('hidden');
                            if (res.ultimaAvaliacaoNacional) {
                                fieldset.find('div#ano').html(res.ultimaAvaliacaoNacional.nuAnoAvaliacao);
                                fieldset.find('div#categoria').html(res.ultimaAvaliacaoNacional.deCategoriaIucn);
                                fieldset.find('div#criterio').html(res.ultimaAvaliacaoNacional.deCriterioAvaliacaoIucn);
                                fieldset.find('div#justificativa').html(res.ultimaAvaliacaoNacional.txJustificativaAvaliacao);
                                fieldset.find('div#avaliada').removeClass('hidden');
                                fieldset.find('div#nao-avaliada').addClass('hidden');
                            }
                            fieldset = $('#fieldset-historico-avaliacoes-' + data.sqFicha);
                            fieldset.find('div#gride-historico').html(res.gridHistorico);
                            fieldset = $('#div-outros-grides-' + data.sqFicha);
                            fieldset.find('div#gride-convencoes').html(res.gridConvencoes);
                            fieldset.find('div#gride-acoes').html(res.gridAcoes);
                            fieldset.find('div#gride-uc').html(res.gridUcs);
                            updateGridAvaliacaoRegional({
                                sqFicha: data.sqFicha,
                                canModify:false,
                                contexto: '#div-outros-grides-' + data.sqFicha
                            });
                        }, '', 'JSON');
                    } else if (data.pesquisa == 'S') {
                        app.ajax(baseUrl + 'fichaCompleta/getGridPesquisa', data, function(res) {
                            var div = $('#panel-pesquisa-body');
                            div.find('div#gride-pesquisa').html(res);
                        }, '', 'HTML');
                    } else if (data.refBib == 'S') {
                        app.ajax(baseUrl + 'fichaCompleta/getRefBibs', data, function(res) {
                            var div = $('#panel-ref-bib-body');
                            div.find('div#gride-ref-bib').html(res);
                        }, '', 'HTML');
                    } else if (data.pendencia == 'S') {
                        app.ajax(baseUrl + 'fichaCompleta/getPendencias', data, function(res) {
                            var div = $('#panel-pendencia-body');
                            div.find('div#gride-pendencia').html(res);
                        }, '', 'HTML');
                    }

                });


            }, '', '', true, true, function(){
                // onclose
                // chatWorker.postMessage({'command':'stop' });
                gerenciarValidacao.acomp.updateGrid({
                    sqFicha: data.sqFicha, sqOficinaFicha:data.sqOficinaFicha
                });
            }).setTheme('#234734'); // #5858C7 #3a7c59

    },

    saveFundamentacao: function(params) {
        if( !params.sqFicha )
        {
            app.alertInfo('saveFundamentacao: ficha não informada.')
        }

        var $pnlTelaFundamentacao   = $("#pnlTelaFundamentacao" + params.sqFicha);
        var $container              = $pnlTelaFundamentacao.find("#frmCadFundamentacaoContainer"+params.sqFicha);
        var $formFundamentacao      = $container.find("#frmCadFundamentacao");
        var $dsCriterioAvalIucn     = $container.find("#dsCriterioAvalIucn");
        var $stPossivelmenteExtinta = $container.find("#stPossivelmenteExtinta");
        var $divDadosMotivoMudanca  = $container.find("#divDadosMotivoMudanca"+params.sqFicha );

        var data = {};
        data.sqFicha                       = params.sqFicha;
        data.sqOficinaFicha                = params.sqOficinaFicha;
        data.sqCategoriaIucn               = $formFundamentacao.find("select[name=sqCategoriaIucn] option:selected").val() || '';
        data.cdCategoriaIucn               = $formFundamentacao.find("select[name=sqCategoriaIucn] option:selected").data('codigo');
        data.dsCriterioAvalIucn            = $formFundamentacao.find("input[name=dsCriterioAvalIucn]").val() ||'';
        data.deCategoriaOutra              = $formFundamentacao.find("input[name=deCategoriaOutra]").val() || '';
        data.stPossivelmenteExtinta        = $formFundamentacao.find("input[name=stPossivelmenteExtinta]").is(':checked') ? 'S' : 'N';

        data.dsJustificativa               = corrigirCategoriasTexto( trimEditor(tinyMCE.get('dsJustificativa' + params.sqFicha).getContent()));
        tinyMCE.get('dsJustificativa' + params.sqFicha).setContent(data.dsJustificativa);

        data.dsJustMudancaCategoria        = corrigirCategoriasTexto(trimEditor(tinyMCE.get('dsJustMudancaCategoria' + params.sqFicha).getContent()));
        if( data.dsJustMudancaCategoria ){
            try {
                tinyMCE.get('dsJustMudancaCategoria' + params.sqFicha).setContent(data.dsJustMudancaCategoria);
            } catch( e ){}
        }

        if ($dsCriterioAvalIucn.is(':visible')) {
            if (!data.dsCriterioAvalIucn) {
                app.alertInfo('Necessário informar o critério!');
                return;
            }
        } else {
            data.dsCriterioAvalIucn = '';
        }

        // validar possivelmente extinta
        if (!$stPossivelmenteExtinta.is(':visible')) {
            data.stPossivelmenteExtinta = '';
        }

        if (!data.dsJustificativa) {
            app.alertInfo('Necessário informar a justificativa!');
            return;
        }

        // validar mudanca de categoria
        if ($divDadosMotivoMudanca.is(':visible')) {
            if ($divDadosMotivoMudanca.find('#divGridMotivoMudancaCategoria tbody>tr').size() == 0) {
                app.alertInfo('Motivo da mudança deve ser informado quando houver mudança de categoria.');
                return;
            }
            // se a categoria mudou de CR,EN ou VU para qualquer outra diferente destas 3 a justificava
            // deve ser obrigatoria
            var cdCategoriaIucnUltimaAvaliacao = $formFundamentacao.find("input[name=cdCategoriaIucnUltimaAvaliacao]").val();
            if( cdCategoriaIucnUltimaAvaliacao && cdCategoriaIucnUltimaAvaliacao.match(/CR|EN|VU/)
                && ! data.cdCategoriaIucn.match(/CR|EN|VU/) && data.dsJustMudancaCategoria == '' )
            {
                app.alertInfo('Quando houver mudança de categoria AMEAÇADA para NÃO AMEAÇADA a justificativa é obrigatória.');
                return;
            }
        } else {
            data.dsJustMudancaCategoria = '';
        }

        if( $pnlTelaFundamentacao.size() == 1) {
            data.sqCategoriaOficina             = $pnlTelaFundamentacao.find("#sqCategoriaOficina").val() || '';
            data.dsCriterioAvalIucnOficina      = $pnlTelaFundamentacao.find("#dsCriterioAvalIucnOficina").val() || '';
            data.stPossivelmenteExtintaOficina  = $pnlTelaFundamentacao.find("#stPossivelmenteExtintaOficina").val() ||'';
            data.sqCategoriaOficina             = $pnlTelaFundamentacao.find("#sqCategoriaOficina").val() || '';

            data.sqResultado1 = $pnlTelaFundamentacao.find("#sqResultado1").val();
            data.sqResultado2 = $pnlTelaFundamentacao.find("#sqResultado2").val();
            data.sqResultado3 = $pnlTelaFundamentacao.find("#sqResultado3").val();
            data.coResultado1 = $pnlTelaFundamentacao.find("#coResultado1").val();
            data.coResultado2 = $pnlTelaFundamentacao.find("#coResultado2").val();
            data.coResultado3 = $pnlTelaFundamentacao.find("#coResultado3").val();
            data.sqCategoriaSugerida1 = $pnlTelaFundamentacao.find("#sqCategoriaSugerida1").val();
            data.sqCategoriaSugerida2 = $pnlTelaFundamentacao.find("#sqCategoriaSugerida2").val();
            data.sqCategoriaSugerida3 = $pnlTelaFundamentacao.find("#sqCategoriaSugerida3").val();
            data.dsCriterioSugerido1 = $pnlTelaFundamentacao.find("#dsCriterioSugerido1").val();
            data.dsCriterioSugerido2 = $pnlTelaFundamentacao.find("#dsCriterioSugerido2").val();
            data.dsCriterioSugerido3 = $pnlTelaFundamentacao.find("#dsCriterioSugerido3").val();

            // ler os valores das Revisões e sobrescrever a resposta dos validadores quando houver revisao da coordenação
            if( $pnlTelaFundamentacao.find("#sqResultadoRevisao1").val() ) {
                data.sqResultado1 = $pnlTelaFundamentacao.find("#sqResultadoRevisao1").val();
            }
            if( $pnlTelaFundamentacao.find("#sqResultadoRevisao2").val() ) {
                data.sqResultado2 = $pnlTelaFundamentacao.find("#sqResultadoRevisao2").val();
            }
            if( $pnlTelaFundamentacao.find("#sqResultadoRevisao3").val() ) {
                data.sqResultado3 = $pnlTelaFundamentacao.find("#sqResultadoRevisao3").val();
            }
            //----------------------------------------
            if( $pnlTelaFundamentacao.find("#coResultadoRevisao1").val() ) {
                data.coResultado1 = $pnlTelaFundamentacao.find("#coResultadoRevisao1").val();
            }
            if( $pnlTelaFundamentacao.find("#coResultadoRevisao2").val() ) {
                data.coResultado2 = $pnlTelaFundamentacao.find("#coResultadoRevisao2").val();
            }
            if( $pnlTelaFundamentacao.find("#coResultadoRevisao3").val() ) {
                data.coResultado3 = $pnlTelaFundamentacao.find("#coResultadoRevisao3").val();
            }
            //----------------------------------------
            if( $pnlTelaFundamentacao.find("#sqCategoriaRevisao1").val() ) {
                data.sqCategoriaSugerida1 = $pnlTelaFundamentacao.find("#sqCategoriaRevisao1").val();
            }
            if( $pnlTelaFundamentacao.find("#sqCategoriaRevisao2").val() ) {
                data.sqCategoriaSugerida2 = $pnlTelaFundamentacao.find("#sqCategoriaRevisao2").val();
            }
            if( $pnlTelaFundamentacao.find("#sqCategoriaRevisao3").val() ) {
                data.sqCategoriaSugerida3 = $pnlTelaFundamentacao.find("#sqCategoriaRevisao3").val();
            }
            //----------------------------------------
            if( $pnlTelaFundamentacao.find("#dsCriterioRevisao1").val() ) {
                data.dsCriterioSugerido1 = $pnlTelaFundamentacao.find("#dsCriterioRevisao1").val();
            }
            if( $pnlTelaFundamentacao.find("#dsCriterioRevisao2").val() ) {
                data.dsCriterioSugerido2 = $pnlTelaFundamentacao.find("#dsCriterioRevisao2").val();
            }
            if( $pnlTelaFundamentacao.find("#dsCriterioRevisao3").val() ) {
                data.dsCriterioSugerido3 = $pnlTelaFundamentacao.find("#dsCriterioRevisao3").val();
            }
            //----------------------------------------

            var chatHabilitado = $("#btnSaveChat").is(':visible');
            var mensagem = 'Não será possível salvar a validação enquanto não houver consenso ' +
                'sobre a <b>categoria</b> e o <b>critério</b> entre os validadores/revisões e o responsável pelo táxon!';
            if (chatHabilitado) {
                mensagem += '<br>Utilize o campo "Mensagem" para argumentação com os validadores.';
            }
            // quando houverem somente 2 validadores
            //if( $("div[id^=divValidadorFicha]").size() < 3 ) {

            // o PF só pode gravar a validação quando houver consenso em pelo menos 2 validadores
            var qtdRespostaA = 0
            var qtdRespostaB = 0
            var qtdRespostaC = 0
            qtdRespostaA += ( data.coResultado1 == 'RESULTADO_A' ? 1 : 0);
            qtdRespostaA += ( data.coResultado2 == 'RESULTADO_A' ? 1 : 0);
            qtdRespostaA += ( data.coResultado3 == 'RESULTADO_A' ? 1 : 0);
            qtdRespostaB += ( data.coResultado1 == 'RESULTADO_B' ? 1 : 0);
            qtdRespostaB += ( data.coResultado2 == 'RESULTADO_B' ? 1 : 0);
            qtdRespostaB += ( data.coResultado3 == 'RESULTADO_B' ? 1 : 0);
            qtdRespostaC += ( data.coResultado1 == 'RESULTADO_C' ? 1 : 0);
            qtdRespostaC += ( data.coResultado2 == 'RESULTADO_C' ? 1 : 0);
            qtdRespostaC += ( data.coResultado3 == 'RESULTADO_C' ? 1 : 0);

            var categoriaSugerida = '';
            var criterioSugerido  = '';
            categoriaSugerida = data.sqCategoriaSugerida1 == data.sqCategoriaSugerida2 ?  data.sqCategoriaSugerida1 : categoriaSugerida;
            categoriaSugerida = data.sqCategoriaSugerida1 == data.sqCategoriaSugerida3 ?  data.sqCategoriaSugerida1 : categoriaSugerida;
            categoriaSugerida = data.sqCategoriaSugerida2 == data.sqCategoriaSugerida3 ?  data.sqCategoriaSugerida2 : categoriaSugerida;
            //----------------------
            criterioSugerido = data.dsCriterioSugerido1 == data.dsCriterioSugerido2 ?  data.dsCriterioSugerido1 : criterioSugerido;
            criterioSugerido = data.dsCriterioSugerido1 == data.dsCriterioSugerido3 ?  data.dsCriterioSugerido1 : criterioSugerido;
            criterioSugerido = data.dsCriterioSugerido2 == data.dsCriterioSugerido3 ?  data.dsCriterioSugerido2 : criterioSugerido;

/**/

            // tem que haver consenso entre pelo menos 2 validadores
            if( qtdRespostaC > 1 ) {
                // se houveram 2 respostas c e todos sugeriram a mesma categoria e criterios então aceitar se o PF acatar a sugestão
                if ( categoriaSugerida != data.sqCategoriaIucn || criterioSugerido != data.dsCriterioAvalIucn) {
                        app.alertInfo('A categoria e critério devem ser iguais aos sugeriodos pelos validadores ou pela revsião da coordenação.');
                        return;
                }
            } else if( qtdRespostaA < 2 && qtdRespostaB < 2 ) {
                    // Respostas A e B - so pode gravar se o PF informar a mesma caregoria e criterio
                    if( qtdRespostaA > 0 && qtdRespostaB > 0) {
                        if ( data.sqCategoriaOficina   != data.sqCategoriaIucn || data.dsCriterioAvalIucnOficina != data.dsCriterioAvalIucn ) {
                            app.alertInfo('A categoria e critério devem ser os aceitos pelos validadores.');
                            return;
                        }
                    } else {
                        app.alertInfo(mensagem);
                        return;
                    }
            } else {
                // se houverem 2 resposta A ou B então o pf só pode gravar se for a mesma categoria e criterio da avaliação
                var resultadoAouB = (qtdRespostaA > qtdRespostaB ? 'A' : 'B');
                if( data.sqCategoriaOficina != data.sqCategoriaIucn ){
                    app.alertInfo('Houveram <b>' + qtdRespostaA + ' resultados ' + resultadoAouB + '</b>, a categoria não pode ser alterada.','Mensagem');
                    return;
                }
                if( data.dsCriterioAvalIucnOficina != data.dsCriterioAvalIucn ){
                    app.alertInfo('Houveram <b>' + qtdRespostaA + ' resultados ' + resultadoAouB + '</b>, o critério não pode ser alterado.','Mensagem');
                    return;
                }
            }
/**/
        }



        mensagem = 'Confirma validação?<br>'
        var $select = $("select[name=sqCategoriaFinal]")
        if( $select.size() > 0 && ! $select.attr('disabled') ) {
            mensagem +='Após esta operação, a categoria NÃO poderá mais ser alterada, somente a justificativa.'
        }

        app.confirm(mensagem,
            function() {
                app.ajax(app.url + 'gerenciarValidacao/saveFundamentacao', data, function(res) {
                    //gerenciarValidacao.acomp.updateGrid({async:true,sqFicha:data.sqFicha}); // nao precisa porque será executado ao fechar a janela modal
                    gerenciarValidacao.updateDashboard();
                }, 'Gravando validação.', 'JSON');
            });
    },

    chkComunicarPendenciaChange: function(chk) {
        var data = {
            sqFicha: chk.value,
            selecionado: chk.checked ? 'S' : 'N',
        };
        app.ajax(app.url + 'gerenciarValidacao/marcarComunicarAlteracao', data, function(res) {
        }, '' );
    },
    sendEmailComunicarPendencia: function() {
        app.confirm('<br>Confirma Envio de Email para os validadores?',
            function() {
                var data = {
                    sqFichas: [],
                };
                $("#tabAcompanhamento input:checkbox[name=chkSqFichaComunicarPendencia]:checked").each(function(e) {
                    data.sqFichas.push(this.value);
                });
                data.sqFichas = data.sqFichas.join(',');
                app.ajax(app.url + 'gerenciarValidacao/enviarEmailPendenciaValidacao', data, function(res) {
                    if (res.status == 0) {
                        gerenciarValidacao.acomp.updateGrid();
                    }
                }, '' );
            });
    },

    getListaFichas:function(params) {
        var $div = $("#" + params.divId)
        if ($div.text().trim() != '') {
            return
        }
        $div.html('<h4 style="text-align: center;">Pesquisando as fichas.&nbsp;'+window.grails.spinner+'</h4><br>');
        app.ajax(app.url + 'gerenciarValidacao/getFichasValidador',
            params,
            function (res) {
                $div.html(res);
            });
    },

    editFichasValidador:function( params, ele,event ) {
        var wh = window.innerHeight;
        var sqOficina = $("#tabConvidar select#sqOficinaValidacao").val();
        app.panel('pnlEditarFichaValidador', "Manutenção das Fichas da Oficina de Validação", '/gerenciarValidacao/getFormEditarFichaValidador'
            , params, function() {
                // on show
                gerenciarValidacao.updateGridFichasValidador(params);
                app.updateMultiSelectFiltroGruposAvaliados({container:'div#pnlEditarFichaValidador',selectName:'sqGrupoFiltro',sqOficina:sqOficina});


            }, 840, Math.max(600,(wh-100 ) ), true /*modal*/ , false /*maximized*/ , function(e) {
                // on close
                gerenciarValidacao.updateGridConvites();
            });//.setTheme('#efefef');

    },
    updateGridFichasValidador:function( params )
    {
        if( typeof( params.target) != 'undefined')
        {
            params = $(params.target).data();
        }
        if( ! params.sqCicloAvaliacaoValidador )
        {
            return
        }
        var rndId = params.sqCicloAvaliacaoValidador;
        // filtro
        var $nivelFiltro    = $("#nivelFiltro"+rndId);
        var $sqTaxonFiltro  = $("#sqTaxonFiltro"+rndId);
        var $sqUnidadeFiltro  = $("#sqUnidadeFiltro"+rndId);
        var $sqCategoriaFiltro  = $("#sqCategoriaFiltro"+rndId);
        var $sqGrupoFiltro  = $("#sqGrupoFiltro"+rndId);
        var $div            = $("#divGridEditarFichasValidador"+rndId);
        var data            = { sqCicloAvaliacaoValidador:rndId, sqCategoriaFiltro:'' };
        data.sqUnidadeFiltro = $sqUnidadeFiltro.val();
        if( $sqGrupoFiltro.val() ) {
            data.sqGrupoFiltro = $sqGrupoFiltro.val().join(',');
        }
        if( $sqCategoriaFiltro.val() )
        {
            data.sqCategoriaFiltro=$sqCategoriaFiltro.val().join(',');
        }
        if( $sqTaxonFiltro.is(":visible") )
        {
            if( ! $sqTaxonFiltro.val() )
            {
                app.alertInfo('Selecione o táxon para filtro!');
                exit;
            }
            data.nivelFiltro = $nivelFiltro.val();
            data.sqTaxonFiltro = $sqTaxonFiltro.val();
            data.noTaxonFiltro = $sqTaxonFiltro.select2('data')[0].descricao;
        }
        $div.html('<h3>Carregando. ' + window.grails.spinner+'</h3>');
        $div.height( Math.max(300, $("#pnlEditarFichaValidador").height() - 380 ) );
        app.ajax(app.url + 'gerenciarValidacao/getGridEditarFichasValidador',
            data,
            function (res) {
                $div.html(res);
                $('#tableEditarFichasValidador'+rndId).DataTable( $.extend({},default_data_tables_options,
                    {
                        "order": [ 2, 'asc' ],
                        "columnDefs" : [
                            {   "orderable": false, "targets": [0,1]
                            }],
                        "searching": false
                        ,"buttons": []
                    })
                );


            });
    },
    deleteFichaValidador:function( params )
    {
        var dupla = params.dupla == 'S';
        var rndId = params.sqCicloAvaliacaoValidador;
        var $divGrid = $("#divGridEditarFichasValidador"+rndId);

        var idsValidorFicha = [];
        $("#body"+rndId).find('input[type=checkbox]:checked').each( function(i,item ){
            idsValidorFicha.push( item.value );
        });
        var qtdFichas = idsValidorFicha.length;
        if( qtdFichas == 0 )
        {
            app.alertInfo('Nenhuma ficha selecionada!');
            return;
        }

        params.ids=idsValidorFicha.join(',');
        app.confirm('Confirma e exclusão de <b>' + qtdFichas+'</b> ficha'+( qtdFichas > 1 ? '(s)':'') + (dupla ? ' da <b>DUPLA</b> de validadores':'')+'?',function() {
            app.ajax(app.url + 'gerenciarValidacao/deleteFichaValidador',
                params,
                function (res) {
                    if( res.status == 0 )
                    {
                        gerenciarValidacao.updateGridFichasValidador({sqCicloAvaliacaoValidador:rndId});
                    }
                });
        },null,params,'Exclusão de ficha(s)');
    },

    transferirFichaValidador:function( params ) {
        var rndId = params.sqCicloAvaliacaoValidador;
        var $select = $("#selectPara"+rndId);
        var idsValidorFicha = [];
        $("#body"+rndId).find('input[type=checkbox]:checked').each( function(i,item ){
            idsValidorFicha.push( item.value )
        });
        var qtdFichas = idsValidorFicha.length;
        if( qtdFichas == 0 )
        {
            app.alertInfo('Nenhuma ficha selecionada!');
            return;
        }
        params.ids=idsValidorFicha.join(',');
        params.para = $select.find('option:selected').text();
        var data = { rndId : String( rndId )
            , saveType                      : 'validate'
            , sqCicloAvaliacaoValidorDe     : String(rndId)
            , sqCicloAvaliacaoValidorPara   : $select.val()
            , ids                           : idsValidorFicha.join(',')
        };
        app.confirm('Confirma a transferência de '+qtdFichas+' ficha'+(qtdFichas > 1 ? '(s)':'')+'<br>de:<b> ' + params.de + '</b><br>para:<b>'+params.para+'</b>?',
            function() {
                gerenciarValidacao.transferirValidadorAjax(data);
            },null,params,'Transferência de fichas');
    },
    transferirValidadorAjax:function( params ){
        var data = app.params2data(params,{});
        app.ajax(app.url + 'gerenciarValidacao/transferirFichaValidador',
            data,
            function ( res ) {
                if (res.status == 0) {
                    if( res.fichasValidadorInvalidas ) {
                        var linhas = [];
                        for(key in res.fichasValidadorInvalidas){
                            var papeis = res.fichasValidadorInvalidas[key].papeis;
                            for( papel in papeis ) {
                                var dsPapel = papeis[papel].ds_papel;
                                if (papel == 'FACILITADOR') {
                                    dsPapel = 'foi facilitador(a)';
                                } else if (papel == 'PONTO_FOCAL') {
                                    dsPapel = 'é ponto focal'
                                } else if (papel == 'COORDENADOR_TAXON') {
                                    dsPapel = 'é coordenador(a) de taxon'
                                }
                                var noPessoa = '<b>' + res.fichasValidadorInvalidas[key].no_pessoa + '</b>&nbsp;<b><span class="red">' + dsPapel + '</span></b> de:';
                                var fichas = '&nbsp;- ' + papeis[papel].fichas.join('<br>&nbsp;- ');
                                linhas.push(noPessoa + '<br>' + fichas)
                            }
                        }
                        var modal = BootstrapDialog.show({
                            title: 'Restrições encontradas',
                            size: BootstrapDialog.SIZE_WIDE,
                            message: linhas.join('<br>'),
                            buttons: [{
                                label: 'Gravar excluíndo as restrições',
                                cssClass: 'btn-success',
                                //icon: 'glyphicon glyphicon-floppy-saved',
                                action: function(dialog) {
                                    data.saveType = 'saveValids';
                                    window.setTimeout(function(){
                                        dialog.close();
                                    },200);
                                    gerenciarValidacao.transferirValidadorAjax(data)
                                }
                            }, {
                                label: 'Gravar incluíndo as restrições',
                                cssClass: 'btn-warning',
                                //icon: 'glyphicon glyphicon-floppy-save',
                                action: function(dialog) {
                                    data.saveType='saveAll'
                                    window.setTimeout(function(){
                                        dialog.close();
                                    },200);
                                    gerenciarValidacao.transferirValidadorAjax(data)
                                }
                            }, {
                                label: 'Cancelar gravação',
                                cssClass: 'btn-danger',
                                //icon: 'glyphicon glyphicon-ban-circle',
                                action: function(dialog) {
                                    dialog.close()
                                }
                            }]
                        })
                        var maxH = Math.max(300,window.innerHeight-300)
                        $(modal.getModalBody()[0]).css('max-height',maxH+'px')
                        return;
                    }
                    gerenciarValidacao.updateGridFichasValidador({ sqCicloAvaliacaoValidador : data.rndId } );
                    gerenciarValidacao.updateGridFichas();
                };
            });
    },
    checkGridEditarFichaValidadorClick:function( params )
    {
        var qtd = $("#body"+params.rndId).find('input[type=checkbox]:checked').size();
        if( qtd > 0 )
        {
            $("#divAcoes"+params.rndId).show();
        }
        else {
            $("#divAcoes"+params.rndId).hide();
        };
    },

    updateDashboard:function(){
        if( gerenciarValidacao.updatingDashboard ){
            return;
        }
        var data = { sqCicloAvaliacao: $("#sqCicloAvaliacao").val() }
        var $div = $("#divDashboardValidacao");
        $table = $("#tabelDashboardValidacao");
        if( $table.length == 1 ) {
            try {
                $table.floatThead('destroy');
            } catch (e) {
            }
        }
        if( !data.sqCicloAvaliacao ){
            $("#fsDashboardValidacao").hide();
            $div.html('');
            return;
        }
        //$("#fsDashboardValidacao").show();
        return;

        $div.html('<br>Carregando painel...');
        gerenciarValidacao.updatingDashboard=true;
        app.ajax(app.url + 'gerenciarValidacao/getDashboard', data, function( res ) {
            gerenciarValidacao.updatingDashboard=false;
            if( res.status == 0 ){
                $div.html( res.html );
                setTimeout(function() {
                    $table = $("#tabelDashboardValidacao")
                    $table.floatThead({
                        floatTableClass: 'floatHead',
                        scrollContainer: true,
                        autoReflow: true
                    });
                    $table.floatThead('reflow');
                },1000);

            } else {
                $div.html('');
            }
        },'','json');
    },

    updateDashboardOficina:function(){
        if( gerenciarValidacao.updatingDashboardOficina ){
            return;
        }
        var data = { sqCicloAvaliacao: $("#sqCicloAvaliacao").val(), sqOficina:$("#sqOficinaValidacao").val() }
        var $div = $("#divDashboardValidacaoOficina");
        $table = $("#tabelDashboardValidacaoOficina");
        if( $table.length == 1 ) {
            try {
                $table.floatThead('destroy');
            } catch (e) {
            }
        }

        if( !data.sqOficina){
            $("#fsDashboardValidacaoOficina").hide();
            $div.html('');
            return;
        }
        //$("#fsDashboardValidacaoOficina").show();
        return;
        $div.html('<br>Carregando painel...');
        gerenciarValidacao.updatingDashboardOficina=true;
        app.ajax(app.url + 'gerenciarValidacao/getDashboardOficina', data, function( res ) {
            gerenciarValidacao.updatingDashboardOficina=false;
            if( res.status == 0 ){
                $div.html( res.html );
                setTimeout(function() {
                    $table = $("#tabelDashboardValidacaoOficina")
                    $table.floatThead({
                        floatTableClass: 'floatHead',
                        scrollContainer: true,
                        autoReflow: true
                    });
                    $table.floatThead('reflow');
                },1000);

            } else {
                $div.html('');
            }
        },'','json');
    },


};
gerenciarValidacao.init();
