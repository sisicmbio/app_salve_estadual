<!-- view: /views/gerenciarValidacao/_divGridFichasLc.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<div id="grideLcWrapper" style="max-height: 500px;overflow-y:auto;overflow-x:hidden;">
    <table class="table table-hover table-striped table-condensed table-bordered">
        <thead>
            <th style="width:30px;">#</th>
            <th style="width:150px;">Nome científico</th>
            <th style="width:200px;">Categoria</th>
            <th style="width:*;">Justificativa</th>
        </thead>
        <tbody>
            <g:if test="${listFichas.size() > 0 }">
                <g:each var="item" in="${listFichas}" status="i">
                    <tr>
                        <td>${i+1}</td>
                        <td>${ raw('<a href="javascript:void(0);" title="Visualizar ficha" onClick="showFichaCompleta('+item?.sq_ficha+',\''+item?.nm_cientifico+'\',false,\'oficina\',\'\',false)">'+item?.nm_cientifico+'</a>')}</td>
                        <td>${ (item.cd_categoria_oficina_ficha ? item?.de_categoria_oficina_ficha + ' ('+item.cd_categoria_oficina_ficha+')' : '') }</td>
                        <td> <div id="div-justificativa-${item.sq_oficina_ficha}" data-id="${item.sq_oficina_ficha}" class="text-justify">${raw( item?.ds_justificativa_oficina_ficha ) }</div> </td>
                        %{--<td>${i+1}</td>--}%
                        %{--<td>${ raw('<a href="javascript:void(0);" title="Visualizar ficha" onClick="showFichaCompleta('+item?.ficha?.id+',\''+item?.ficha?.noCientificoItalico+'\',false,\'oficina\',\'\',false)">'+item?.ficha?.noCientificoItalico+'</a>')}</td>--}%
                        %{--<td>${ item?.categoriaIucn?.descricaoCompleta }</td>--}%
                        %{--<td> <div id="div-justificativa-${item.id}" data-id="${item.id}" class="text-justify">${raw( item?.dsJustificativa ) }</div> </td>--}%
                    </tr>
                </g:each>
            </g:if>
            <g:else>
                <tr>
                    <td colspan="4">
                        <h3>Nenhuma ficha mantida LC.</h3>
                    </td>
                </tr>
            </g:else>
        </tbody>
    </table>
</div>
