<!-- view: /views/gerenciarValidacao/_divGridFichasCadastro -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<div style="display:block;overflow-y:auto;max-height:500px;padding:5px" id="divContainerGridFichasCadastro">
    <h4 class="mt10">${listFichas ? listFichas.size() : '0'}&nbsp;Fichas / <span class="total-selecionadas">0 selecionadas.</span></h4>
    <table class="table table-sm table-hover table-striped table-condensed table-bordered" id="tbFichasCadastro">
        <thead>
        <th style="width:30px;">#</th>
        <th style="width:30px;"><i id="checkUncheckAll" title="Marcar/Desmarcar Todos" data-action="checkUncheckAll" data-container="divContainerGridFichasCadastro" class="glyphicon glyphicon-unchecked check-all"></i></th>
        <th style="width:auto;">Nome Científico</th>
        <th style="width:220px;">Categoria</th>
        <th style="width:200px;">Situação</th>
        </thead>
        <tbody>
        <g:if test="${ listFichas }">
            <g:each in="${listFichas}" var="item" status="i">
                <tr id="gdFichaRow_${i + 1}" data-situacao="${item.cd_situacao_ficha_sistema}">
                    <td class="text-center">${i+1}</td>
                    <td class="text-center">
                        <input id="sqFicha_${item.sq_ficha}"
                               name="sqFicha[${item.sq_ficha}]"
                               value="${item.sq_ficha}"
                               type="checkbox"
                               class="checkbox-lg"
                               data-action="contarChecked"
                               %{--${ item.cd_situacao_ficha_sistema == 'EXCLUIDA' || item.st_transferida ? 'disabled="true"':''}
                               title="${ ( item.cd_situacao_ficha_sistema=='EXCLUIDA' ? 'Ficha EXCLUÍDA na oficina!': (item.st_transferida ? 'Ficha TRANSFERIDA na oficina!':'') ) }"
                               --}%
                               data-container="divContainerGridFichasCadastro"/>
                    </td>
                    <td>${raw( br.gov.icmbio.Util.ncItalico( item.nm_cientifico,true) )}</td>

                    %{-- CATEGORIA--}%
                    <td class="text-center">
                        <g:if test="${ item?.ds_categoria_avaliada_ficha}">
                            ${ item?.ds_categoria_avaliada_ficha+' ('+item?.cd_categoria_avaliada_ficha+')'}
                        </g:if>
                        <g:else>
                            <g:if test="${item?.ds_categoria_iucn_nacional}">
                                ${ item?.ds_categoria_iucn_nacional}
                            </g:if>
                            <g:else>
                                <span>Sem histórico</span>
                            </g:else>
                        </g:else>
                    </td>

                    <td class="text-center">${ ( item.st_transferida ? 'Transferida': item?.ds_situacao_ficha )}</td>
                    %{--<td>${raw( item.nmCientifico )}</td>--}%
                </tr>
            </g:each>
        </g:if>
        <g:else>
            <tr>
                <td colspan="4" align="center">
                    <h3>Nenhuma ficha encontrada!</h3>
                    <h4 class="red">Fichas devem estar na situação avaliada revisada!</h4>
                </td>
            </tr>
        </g:else>
        </tbody>
    </table>
</div>
