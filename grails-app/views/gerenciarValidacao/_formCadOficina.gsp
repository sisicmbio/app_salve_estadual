<!-- view: /views/gerenciarValidacao/_formCadOficina.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<div id="containerOficina">

    %{--    DASHBOARD VALIDACAO--}%
    <fieldset class="mt10" id="fsDashboardValidacao" style="display: none;">
        <legend style="font-size:1.2em;">
            <a  data-action="app.showHideContainer"
                data-target="divDashboardWrapper"
                data-focus=""
                title="Mostrar/Esconder tabela!">
                <i class="fa fa-chevron-up green"/>&nbsp;Acompanhamento da distribuição das fichas</a>
            <span><i class="fa fa-refresh green" title="Atualizar tabela" data-action="gerenciarValidacao.updateDashboard"></i></span>

        </legend>
        <div class="text-center mt5 mb20" id="divDashboardWrapper">
            <div id="divDashboardValidacao" style="height:auto;max-height: 300px;width:850px;max-width:850px;overflow: auto;text-align: left;"><br>Carregando painel...</div>
        </div>

    </fieldset>


    %{-- Formulário de cadastro de Oficinas --}%
    <div id="frmCadOficinaContainer">
%{--
        <div class="fld panel-footer">
            <a id="btnShowForm" data-action="gerenciarValidacao.btnShowFormClick"
                title="Mostrar/Esconder formulário de cadastro."
               class="btn btn-default btn-sm"><i class="fa fa-plus">&nbsp;Formulário</i></a>
        </div>
--}%
    <fieldset class="mt10" id="frmGerConsultaContainer">
        <legend style="font-size:1.2em;">
            <a  id="btnShowForm"
                data-action="app.showHideContainer"
                data-target="frmCadOficina"
                data-focus="noOficina"
                title="Mostrar/Esconder formulário!">
                <i class="fa fa-plus green"/>&nbsp;Cadastrar</a>
        </legend>
    <form class="form-inline hidden" name="frmCadOficina" id="frmCadOficina" role="form">
            %{--<h3><b>Oficina de Validação</b></h3>--}%
            <input type="hidden" id="sqOficina" name="sqOficina" value=""/>
            <div class="form-group hidden">
                <label for="sqTipoOficina" class="control-label">Tipo da oficina</label>
                <br>
                <select name="sqTipoOficina" id="sqTipoOficina"
                        class="form-control fldSelect"
                        required="true" disabled="true"
                        data-msg-required="Campo obrigatório">
                    <g:each in="${listTipoOficina}" var="item" status="i">
                        <option data-codigo="${item.codigoSistema}" value="${item.id}" ${status== 1 ? 'selected' : '' }>
                            ${item.descricao}
                        </option>
                    </g:each>
                </select>
            </div>

        <div class="form-group">
            <label for="fldAmeacadas" class="control-label">Oficina de</label>
            <br>
             <select class="form-control" id="fldAmeacadas" name="fldAmeacadas"
                    data-change="gerenciarValidacao.selectAmeacadasChange">
                <option value="">...</option>
                <option value="AMEACADAS">Ameaçadas</option>
                <option value="NAO_AMEACADAS">Não ameaçadas</option>
                </select>
            </div>

            %{-- nome --}%
            <div class="form-group">
                <label for="noOficina" class="control-label label-required">Nome</label>
                <br>
                <input value="Oficina de validação de " type="text" name="noOficina" id="noOficina" class="form-control fld500" maxlength="200" required="true" data-msg-required="Campo obrigatório"/>
            </div>

            %{-- sigla --}%
            <div class="form-group">
                <label for="sgOficina" class="control-label label-required">Nome abreviado</label>
                <br>
                <input value="" type="text" name="sgOficina" id="sgOficina" class="form-control fld300" maxlength="100" required="true" data-msg-required="Campo obrigatório"/>
            </div>

                <br>

            %{-- local --}%
            <div class="form-group">
                <label for="deLocal" class="control-label label-required">Local</label>
                <br/>
                <input value="" type="text" name="deLocal" id="deLocal" class="form-control fld400" maxlength="200" required="true" data-msg-required="Campo obrigatório"/>
            </div>

            %{-- periodo --}%
            <div class="form-group">
                <label for="dtInicio" class="control-label label-required">Início</label>
                <br/>
                <input value="" type="text" name="dtInicio" id="dtInicio" class="form-control date fldDate" required="true" data-msg-required="Campo obrigatório"/>
            </div>
            <div class="form-group">
                <label for="dtFim" class="control-label label-required">Fim</label>
                <br/>
                <input value="" type="text" name="dtFim" id="dtFim" class="form-control date fldDate" required="true" data-msg-required="Campo obrigatório"/>
            </div>

                <br>

                %{-- Fonte de Recurso --}%
                <div class="form-group" style="display:block;">
                    <label for="sqFonteRecurso" class="control-label">Fonte principal de recurso</label>
                    <br>
                    <select name="sqFonteRecurso" id="sqFonteRecurso" class="form-control fld600">
                        <option value="">Nenhuma</option>
                        <g:each var="item" in="${ listFonteRecurso }">
                            <option ${ item.id == oficina?.fonteRecurso?.id ? 'selected' : ''} value="${item.id}">
                                ${item.descricao}
                            </option>
                        </g:each>
                    </select>
                </div>

                %{--Filtro da ficha--}%
                <div class="hidden mt10" id="divFiltrosFichaCadastro"></div>

            %{-- gride para selecionar ficha --}%
            <div id="divGridFichasCadastro" style="max-height: 500px;overflow-y:auto;"></div>

            %{-- botão --}%
            <div class="fld panel-footer">
                <button data-action="gerenciarValidacao.save" data-params="sqCicloAvaliacao,sqOficina" class="fld btn btn-success">Gravar</button>
                <button data-action="gerenciarValidacao.resetForm" class="fld btn btn-danger">Limpar</button>
            </div>
        </form>
     </fieldset>
</div>

    %{-- gride com as oficinas cadastrada --}%
    <fieldset>
        <div class="fld400">
            <div class="form-group">
                <label>Localizar oficina da espécie</label>
                <br>
                <div class="input-group input-group">
                    <input type="text" class="form-control ignoreChange input-bold-blue"
                           placeholder="Informe o nome da espécie"
                           data-enter="gerenciarValidacao.updateGridOficinas()"
                           id="fldNoCientificoFiltrarOficinaAbaCadastro" style="display:inline">
                    <span class="input-group-addon">
                        <i class="fa fa-search cursor-pointer" title="Localizar..." data-action="gerenciarValidacao.updateGridOficinas"></i>
                    </span>
                    <span class="input-group-addon">
                        <i class="fa fa-trash cursor-pointer" title="Limpar o campo" data-action="gerenciarValidacao.resetFiltroOficinasAbaCadastro"></i>
                    </span>
                </div>
            </div>
        </div>

        %{--    GRIDE DAS OFICINAS --}%
        <div id="containerGridOficinas"
             data-params="sqCicloAvaliacao,fldNoCientificoFiltrarOficinaAbaCadastro|noCientificoFiltrarOficina"
             data-field-id="sq_oficina"
             data-url="gerenciarValidacao/getGridOficinas"
             data-on-load="gerenciarValidacao.gridOficinasLoad">
            <g:render template="divGridOficinas"
                      model="[gridId: 'GridOficinas'
                              ,listSituacaoOficina:listSituacaoOficina]"></g:render>
        </div>
    </fieldset>
</div>
<div class="end-page"></div>
