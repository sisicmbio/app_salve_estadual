<!-- view: /views/gerenciarValidacao/_telaAcompanhamento.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<div id="divSelectOficinaValidacaoAcomp" class="form-group form-inline">
    %{--<label for="sqOficinaValidacaoAcomp" class="control-label">Filtrar por oficina de validação</label>--}%
    <div class="fld400">
        <div class="form-group">
            <label>Localizar oficina da espécie</label>
            <br>
            <div class="input-group input-group">
                <input type="text" class="form-control ignoreChange input-bold-blue"
                       data-enter="gerenciarValidacao.filtrarOficinaAbaAcompanhamento"
                       id="fldNoCientificoFiltrarOficinaAbaAcompanhamento" style="display:inline">
                <span class="input-group-addon">
                    <i class="fa fa-search cursor-pointer" title="Localizar..."
                       data-action="gerenciarValidacao.filtrarOficinaAbaAcompanhamento"></i>
                </span>
                <span class="input-group-addon">
                    <i class="fa fa-trash cursor-pointer" title="Limpar o campo"
                       data-action="gerenciarValidacao.resetFiltroOficinasAbaAcompanhamento"></i>
                </span>
            </div>
        </div>
    </div>

    %{-- SELECT OFICINAS --}%
    <label for="sqOficinaValidacaoAcomp" class="control-label label-medio">Oficinas:
        <label class="control-label cursor-pointer ml30 label-selected"><input name="radioFiltroOficinaAcomp" class="checkbox-lg" type="radio" checked="true" value="AB" data-change="gerenciarValidacao.radioFiltroOficinaAcompChange">&nbsp;abertas</label>
        <label class="control-label cursor-pointer ml30 label-selected"><input name="radioFiltroOficinaAcomp" class="checkbox-lg" type="radio" value="FE" data-change="gerenciarValidacao.radioFiltroOficinaAcompChange">&nbsp;encerradas</label>
    </label>
    <br>

%{--    OFICINAS--}%
    <select id="sqOficinaValidacaoAcomp"
            name="sqOficinaValidacaoAcomp"
            data-change="gerenciarValidacao.acomp.sqOficinaValidacaoChange"
            class="mt5 ignoreChange form-control fld600 fldSelect blue">
        <option value="">-- selecione --</option>

        <g:each var="oficina" in="${listTodasOficinas}">
            <option value="${oficina.sq_oficina}"
                    data-situacao="${oficina?.cd_situacao_sistema}">
                    ${oficina.sg_oficina + ' de ' + oficina.de_periodo}
            </option>
        </g:each>
    </select><i id="sqOficinaValidacaoAcompRefresh"
                title="Atualizar gride de acompanhamento."
                class="ml5 glyphicon glyphicon-refresh cursor-pointer"
                style="display:none;"
                data-action="gerenciarValidacao.acomp.sqOficinaValidacaoChange"></i>

    %{-- TABELA COM TOTAIS DA EXECUCAO --}%
    <div style="overflow:hidden;height:auto;margin-top:10px" id="divTotais"></div>


    %{--Filtro da ficha--}%
    <div class="form form-inline mt10" id="divFiltroAbaAcompanhamento" style="display:none;">
        <g:render template="/templates/filtrosFicha"
                  model="[callback: 'gerenciarValidacao.acomp.updateGrid',
                          listRespostas: listRespostas,
                          excluirFiltro: [pendencia: true],
                          outrosCampos: '/gerenciarValidacao/camposFiltrarFichasAcomp',
                          isCollapsed: true,
                          isRequired: false,
                          gridId: 'GridAcompanhamento'
                  ]"></g:render>
    </div>

</div>

<div id="divTelaAcompContainer" style="display:none;overflow:hidden;height:auto;min-height:600px;">
    <div class="row">
        <div class="col-sm-12">
            <button type="btn btn-primary btn-xs" data-grid-id="GridAcompanhamento" data-action="gerenciarValidacao.acomp.gridCsv">Exportar</button>
        </div>
    </div>
    <div class="row mt5">
        <div class="col-sm-12">
            <div id="containerGridAcompanhamento"
                 data-params="sqCicloAvaliacao,sqOficinaValidacaoAcomp|sqOficinaFiltro"
                 data-container-filters-id="divFiltroAbaAcompanhamento"
                 data-height="auto"
                 data-max-height="800px"
                 data-field-id="sqFicha"
                 data-url="gerenciarValidacao/getGridAcompanhamento">
                <g:render template="divGridFichasAcomp"
                          model="[gridId: 'GridAcompanhamento', listRespostas: listRespostas]"></g:render>
            </div>
        </div>
    </div>
    %{--    <div class="row hidden">
            <div class="col-sm-12">
                <fieldset class="mt20">
                    <legend><h4>Espécies mantidas LC</h4></legend>
                    <div id="divGridFichasLc"><h4>Carregando gride... <i class="glyphicon glyphicon-refresh spinning"></i>
                    </h4></div>
                </fieldset>
            </div>
        </div>--}%
</div>
