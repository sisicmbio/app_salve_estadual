<!-- view: /views/gerenciarValidacao/indexValidador.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
%{-- carrega o arquivo assets/validador.js na main.gsp --}%
<g:set var="rndId" value="${Math.abs(new Random().nextInt() % 10000) + 1}"/>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>SALVE - Validação</title>
</head>

<body>

<div id="telaValidadorContainer" class="w100p" style="display: none">
    <div class="panel panel-default" style="">
        <div class="panel-heading">
            <span>
                Validação de Fichas
            </span>
        </div>

        <div class="panel-body">
            %{--gride com as fichas --}%


            %{-- filtros complementares --}%
            <div id="divFiltrosFicha"
                 style="background-color: #efefef;padding:2px; border:1px solid silver;"
                 class="col-md form-inline">
                <div class="btn-group mb5" data-toggle="buttons">
                    <label class="btn btn-info-filtro">
                        Filtrar ficha por:
                    </label>
                    <label class="btn btn-default active mr10">
                        <input type="radio" name="cdSituacaoFiltro" value="PENDENCIAS" checked="checked"
                               onChange="validador.updateGrid()"/> Minhas pendências
                    </label>
                    <label class="btn btn-default mr10">
                        <input type="radio" name="cdSituacaoFiltro" value="VALIDACOES"
                               onChange="validador.updateGrid()"/> Minhas Validações
                    </label>
                    <label class="btn btn-default mr10">
                        <input type="radio" name="cdSituacaoFiltro" value="VALIDADAS"
                               onChange="validador.updateGrid()"/> Validadas
                    </label>
                    <label class="btn btn-default mr10">
                        <input type="radio" name="cdSituacaoFiltro" value="NAO_VALIDADAS"
                               onChange="validador.updateGrid()"/> Não Validadas
                    </label>
                    <label class="btn btn-default">
                        <input type="radio" name="cdSituacaoFiltro" value="TODAS"
                               onChange="validador.updateGrid()"/> Todas
                    </label>
                </div>


                %{-- FILTRO PELO NIVEL TAXONOMICO --}%
                <br>

                <div id="divFiltroTaxon" class="form-group">
                    <label for="sqNivelFiltro${rndId}" class="control-label">Nível taxonômico</label>
                    <br>
                    <select id="sqNivelFiltro${rndId}" name="sqNivelFiltro"
                            data-change="app.filtroNivelTaxonomicoChange"
                            data-params="rndId:${rndId},callback:app.btnFiltroFichaClick"
                            class="fld form-control fld200 fldSelect">
                        <option value="">-- todos --</option>
                        <g:each var="item" in="${listNivelTaxonomico}">
                            <option data-codigo="${item.coNivelTaxonomico}"
                                    value="${item.id}">${item.deNivelTaxonomico}</option>
                        </g:each>
                    </select>
                </div>

                <div class="fld form-group">
                    <label for="sqTaxonFiltro${rndId}" class="control-label"
                           title="Para selecionar mais de uma opção, pressione a tecla CTRL e clique nos itens da lista.">Taxon</label>
                    <br>
                    <select id="sqTaxonFiltro${rndId}" name="sqTaxonFiltro"
                            data-s2-visible="false"
                            class="fld form-control select2 fld300"
                            data-s2-params="sqNivelFiltro${rndId}|sqNivel"
                            data-s2-onchange="validador.aplicarFiltro"
                            data-s2-url="ficha/select2TaxonNovo"
                            data-s2-placeholder="?"
                            data-s2-multiple="false"
                            data-s2-maximum-selection-length="20"
                            data-s2-minimum-input-length="2"
                            data-s2-auto-height="true">
                    </select>
                </div>


                <div class="fld form-group" style="margin-left:15px;margin-right:15px;border-bottom:1px solid green;">
                    <label>Chat</label>
                    <br>
                    <div class="radio mr15">
                        <label><input name="inComSemChatFiltro" id="inComSemChatTodos${rndId}" type="radio"
                                      data-change="validador.aplicarFiltro" class="radio-lg" value=""
                                      checked="true"/>&nbsp;Todas</label>
                    </div>

                    <div class="radio mr15">
                        <label><input name="inComSemChatFiltro" id="inComSemChatSim${rndId}" type="radio"
                                      data-change="validador.aplicarFiltro" class="radio-lg"
                                      value="S"/>&nbsp;Com chat</label>
                    </div>

                    <div class="radio">
                        <label><input name="inComSemChatFiltro" id="inComSemChatNao${rndId}" type="radio"
                                      data-change="validador.aplicarFiltro" class="radio-lg"
                                      value="N"/>&nbsp;Sem chat</label>
                    </div>
                </div>

                <div class="form-group mt10">

                    <label class="control-label">Oficina:</label>
                    <br>
                    <select class="form-control select blue" id="sqOficinaFiltro${rndId}" name="sqOficinaFiltro" onchange="validador.updateGrid()">
                        <option value="">-- todas --</option>
                        <g:each var="oficina" in="${ listOficinas }">
                            <option data-situacao="${oficina.situacao}"
                                value="${oficina.id}" ${params?.sqOficinaFiltro?.toString() == oficina?.id?.toString() ? ' selected' : ''}>
                                ${oficina.descricao}
                            </option>
                        </g:each>
                    </select>
                </div>
            </div>


%{--            <div id="divGridFichas" style="width:100%;height:auto;overflow:hidden;"></div>--}%
            <div id="containerGrideFichasValidador"
                 data-field-id='sqFicha'
                 data-page-change="false"
                 data-contexto="validador"
                 data-container-filters-id="divFiltrosFicha"
                 data-url="gerenciarValidacao/getGridFichasValidador" class="mt10">
                <g:render template="divGridFichasValidador"
                          model="[gridId:'GrideFichasValidador' ,listGrupos:listGrupos, listRespostas:listRespostas, listCategoriasAvaliadas:listCategoriasAvaliadas,listCriteriosAvaliados:listCriteriosAvaliados,listCategoriasValidadas:listCategoriasValidadas, listCriteriosValidados:listCriteriosValidados]"></g:render>
            </div>
        </div>
    </div>
</div>

</body>
</html>
