<!-- view: /views/gerenciar/validacao/_formCadValidadores -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->


<div id="divSelectOficinaValidacao" class="form-group form-inline">

%{--    <label class="control-label">Selecionar:
        <label class="control-label cursor-pointer ml20 label-selected"><input id="chkFiltroOficinaValidadorAberta" class="checkbox-lg" type="checkbox" checked="true" value="AB">&nbsp;Abertas</label>
        <label class="control-label cursor-pointer ml20 label-selected"><input id="chkFiltroOficinaValidadorEncerrada" class="checkbox-lg" type="checkbox" value="FE">&nbsp;Encerradas</label>
    </label>
    <br>--}%
    <label for="sqOficinaValidacao" class="control-label label-medio">Oficinas:
        <label class="control-label cursor-pointer ml30 label-selected"><input name="radioFiltroOficinaValidador" class="checkbox-lg" type="radio" checked="true" value="AB" data-change="gerenciarValidacao.radioFiltroOficinaValidadorChange">&nbsp;abertas</label>
        <label class="control-label cursor-pointer ml30 label-selected"><input name="radioFiltroOficinaValidador" class="checkbox-lg" type="radio" value="FE" data-change="gerenciarValidacao.radioFiltroOficinaValidadorChange">&nbsp;encerradas</label>
    </label>
    <br>
    <select id="sqOficinaValidacao" name="sqOficinaValidacao" data-change="gerenciarValidacao.sqOficinaValidacaoAbaConvidarChange" class="ignoreChange form-control fld600 fldSelect mt5">
        <option value="">-- selecione --</option>
        <g:each var="oficina" in="${listTodasOficinas}">
            <option data-situacao="${oficina?.situacao?.codigoSistema}"
                style="display: ${ oficina?.situacao?.codigoSistema == 'ENCERRADA' ? 'none' :'' }"
                    value="${oficina.id}">${oficina.sgOficina+' de '+oficina.periodo}</option>
        </g:each>
    </select><i id="sqOficinaValidacaoRefresh"
                title="Atualizar validadores convidados."
                class="ml5 glyphicon glyphicon-refresh cursor-pointer"
                data-action="gerenciarValidacao.sqOficinaValidacaoAbaConvidarChange"
                style="display:none;"></i>
</div>

%{--    DASHBOARD VALIDACAO POR OFICINA--}%
<fieldset class="mt10" id="fsDashboardValidacaoOficina" style="display: none;">
    <legend style="font-size:1.2em;">
        <a  data-action="app.showHideContainer"
            data-target="divDashboardValidacaoOficinaWrapper"
            data-focus=""
            title="Mostrar/Esconder tabela!">
            <i class="fa fa-chevron-up green"/>&nbsp;Acompanhamento da distribuição das fichas da oficina</a>
        <span><i class="fa fa-refresh green" title="Atualizar tabela" data-action="gerenciarValidacao.updateDashboardOficina"></i></span>
    </legend>
    <div class="text-center mt5 mb20" id="divDashboardValidacaoOficinaWrapper">
        <div id="divDashboardValidacaoOficina" style="height:auto;max-height: 300px;width:850px;max-width:850px;overflow: auto;text-align: left;"></div>
    </div>

</fieldset>



<div id="frmCadValidadoresContainer" style="display:none;">
    <fieldset class="mt10">
        <legend style="font-size:1.2em;">
            <a  id="btnShowForm"
                data-action="app.showHideContainer"
                data-target="acordeon_frmCadValidadores"
                data-focus="sqPessoa1"
                data-callback=""
                title="Mostrar/Esconder formulário!">
                <i class="fa fa-plus green"/>&nbsp;Convidar</a>
        </legend>
        <div id="acordeon_frmCadValidadores" class="hidden" style="background-color:#eaf3e1;border:1px solid #58a54a;padding:5px;border-radius:5px;">
            <form id="frmCadValidadores" name="frmCadValidadores" class="form-inline" role="form">
                <input type="hidden" value="${canModify}" id="canModify" name="canModify"/>
                %{--<span class="blue" style="margin:0px;font-size:1.25rem !important;">Validadores</span>--}%
                <div id="divNomesValidadores" style="border-radius:5px;border:1px solid #0B6C33;margin:5px 0px;padding:5px;background-color: #f5f5dc;">
                    %{--validador 1--}%
                    <div class="fld form-group">
                        <label class="control-label">Nome do 1º Validador:</label>
                        <br>
                        <div class="input-group">
                            <select name="sqPessoa1" id="sqPessoa1" class="form-control fld600 ignoreChange select2"
                                    data-email="deEmail1" required="true" data-change="gerenciarValidacao.sqPessoaChange"
                                    data-s2-url="gerenciarValidacao/select2Validador"
                                    data-s2-placeholder="?"
                                    data-s2-params="sqOficinaValidacao"
                                    data-s2-minimum-input-length="2"
                                    datax-s2-auto-height="true">
                            </select>
                            <div class="input-group-btn">
                                <button type="button" title="Selecionar últimos validadores convidados" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="caret"></span></button>
                                <ul class="dropdown-menu dropdown-menu-right" style="max-height: 400px;overflow-y: auto">
                                    <g:each var="validador" in="${listValidadores}">
                                        <li>
                                            <a href="javascript:void(0)" onclick="gerenciarValidacao.setValidador(this)"
                                               data-field-select="sqPessoa1"
                                               data-field-email="deEmail1"
                                               data-email="${validador.de_email}"
                                               data-id="${validador.sq_validador}">${validador.no_pessoa}</a></li>
                                    </g:each>

                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="fld form-group">
                        <label for="deEmail1" class="control-label">Email 1º Validador</label>
                        <br/>
                        <input name="deEmail1" id="deEmail1" type="text" value="" class="form-control ignoreChange fld300" required="true">
                    </div>

                    <br>

                    %{--validador 2--}%
                <div class="fld form-group">
                    <label class="control-label">Nome do 2º Validador:</label>
                    <br>
                    <div class="input-group">
                        <select name="sqPessoa2" id="sqPessoa2" class="form-control fld600 ignoreChange select2"
                                data-email="deEmail2" required="true" data-change="gerenciarValidacao.sqPessoaChange"
                                data-s2-url="gerenciarValidacao/select2Validador"
                                data-s2-placeholder="?"
                                data-s2-params="sqOficinaValidacao"
                                data-s2-minimum-input-length="2"
                                datax-s2-auto-height="true">
                        </select>
                        <div class="input-group-btn">
                            <button type="button" title="Selecionar últimos validadores convidados" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="caret"></span></button>
                            <ul class="dropdown-menu dropdown-menu-right" style="max-height: 400px;overflow-y: auto">
                                <g:each var="validador" in="${listValidadores}">
                                    <li>
                                        <a href="javascript:void(0)" onclick="gerenciarValidacao.setValidador(this)"
                                           data-field-select="sqPessoa2"
                                           data-field-email="deEmail2"
                                           data-email="${validador.de_email}"
                                           data-id="${validador.sq_validador}">${validador.no_pessoa}</a></li>
                                </g:each>

                            </ul>
                        </div>
                    </div>
                </div>

                <div class="fld form-group">
                    <label for="deEmail2" class="control-label">Email 2º Validador</label>
                    <br/>
                    <input name="deEmail2" id="deEmail2" type="text" value="" class="form-control ignoreChange fld300" required="true">
                </div>




                %{-- botão --}%
                    <div class="fld mt5">
                        <button data-action="gerenciarValidacao.resetFrmCadValidadores" class="fld btn btn-danger btn-xs">Limpar validadores</button>
                    </div>
                </div>

                %{--Filtro da ficha--}%
                <div class="mt10" id="divFiltroAbaConvidar">
                    <g:render template="/templates/filtrosFicha" model="[callback:'gerenciarValidacao.updateGridFichas',label:'Selecionar fichas',listSituacaoFicha:listSituacaoFicha,outrosCampos:'/gerenciarValidacao/camposFiltrarFichas',isCollapsed:false]"></g:render>
                </div>
                <div id="divGridFichas" style="max-height:300px;overflow-x:hidden;overflow-y:auto;"></div>

                %{-- botão --}%
                <div class="fld panel-footexr mt20">
                    <button data-action="gerenciarValidacao.saveValidadores" class="fld btn btn-success">Gravar convite</button>
                </div>
            </form>
        </div>
    </fieldset>
</div>

%{--CAMPO SELECT PARA FILTRAR CONVIDADOS PELA OFICINA--}%


%{--<div id="divSelectOficinaValidacao" class="form-group">--}%
    %{--<label for="sqOficinaValidacao" class="control-label label-grande">Convidados</label>--}%
    %{--<br>--}%
    %{--<select id="sqOficinaValidacao" name="sqOficinaValidacao" data-change="gerenciarValidacao.updateGridConvites" class="ignoreChange form-control fld600 fldSelect">--}%
        %{--<option value="">-- selecione a oficina --</option>--}%
        %{--<g:each var="oficina" in="${listTodasOficinas}">--}%
            %{--<option value="${oficina.id}">${oficina.sgOficina+' de '+oficina.periodo}</option>--}%
        %{--</g:each>--}%
    %{--</select>--}%
%{--</div>--}%

%{--GRIDE DE VALIDADORES CONVIDADOS--}%
<div id="divGridConvites" style="overflow: hidden;height: auto;"></div>

<div id="frmEmailContainer" style="display:none;border-top:2px solid silver;">
    <fieldset class="mt40">
        <legend style="font-size:1.2em;">
            <a data-toggle="collapse" data-parent="#acordeon_frmConvidarEmail" href="#acordeon_frmConvidarEmail">
                <i class="glyphicon glyphicon-envelope"/>&nbsp;Elaborar Convite</a>
        </legend>
        <div id="acordeon_frmConvidarEmail" class="panel-collapse collapse" role="tabpanel" style="background-color:#ffffff;ppadding:5px;">
            <form id="frmValidacaoEnviarEmail" name="frmValidacaoEnviarEmail" class="form-inline" role="form">
                <div class="form-group" style="width: 100%;">
                    <label for="deAssuntoEmail" class="control-label">Assunto</label>
                    <br>
                    <input name="deAssuntoEmail" id="deAssuntoEmail" class="fld form-control ignoreChange" required="true" style="width:100%" value="Convite para validação de ficha(s) de espécie(s)."/>
                </div>

                <br>

                <div class="form-group" style="width: 100%">
                    <label class="control-label">Mensagem&nbsp;<span style="margin-left:10px;font-size:12px;color:gray;"> Variáveis: <a title="inserir" href="#" onClick="app.insertTextEditor(this)">{nome}</a>
                        , <a title="inserir" href="#" onClick="app.insertTextEditor(this)">{fichas}</a>
                        , <a title="inserir" href="#" onClick="app.insertTextEditor(this)">{linkSalve}</a>
                        , <a title="inserir" href="#" onClick="app.insertTextEditor(this)">{linkSalveExterno}</a>
                        , <a title="inserir" href="#" onClick="app.insertTextEditor(this)">{prazo}</a>
                        , <a title="inserir" href="#" onClick="app.insertTextEditor(this)">{emailConvidado}</a>
                    </span>
                    </label>
                  <br/>
                    <textarea name="txEmail" id="txEmail" class="fld form-control fldTextarea ignoreChange" rows="10" style="height:200px;width:99%;"></textarea>
               </div>

                <br/>

               <div class="form-group">
                  <label for="deEmailCopia" class="control-label">Com cópia</label>
                  <br/>
                  <input name="deEmailCopia" id="deEmailCopia" class="fld form-control ignoreChange fld500" value="${grailsApplication.config.app.email}">
               </div>
               <div class="fld form-group">
                    <label for="dtValidadeConvite" class="control-label">Validade do convite</label>
                    <br/>
                    <input type="text" name="dtValidadeConvite" id="dtValidadeConvite"  required="true" value="${(new Date()+7).format('dd/MM/yyyy')}" class="form-control date ignoreChange fld150">
                </div>
               <div class="fld panel-footer">
                  <button data-action="gerenciarValidacao.sendMail" class="fld btn btn-success">Enviar emails</button>
               </div>
            </form>
        </div>
    </fieldset>
</div>
<div class="end-page"></div>
