<!-- view: /views/gerenciarValidacao/_tabelaTotais.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<div style="border:none;margin-bottom:20px;">

        <table class="table table-striped table-condensed" style="max-width:1200px;">
            <thead>
            <th colspan="3" style="font-size:1.3rem !important;">Resultado da Oficina</th>
            </thead>
            <tbody>
            <tr>
                <td style="vertical-align: top !important;">
                    <table class="table table-hover table-striped table-condensed table-bordered" style="max-width:500px;">
                        <thead>
                        <th colspan="2">Espécies</th>
                        </thead>
                        <tbody>
                        <tr  style="background-color: #fff;">
                            <td>Validadas</td>
                            <td class="text-center" style="width: 100px;">${resultado.validadas}</td>
                        </tr>
                        <tr class="">
                            <td class="nowrap">Em validação</td>
                            <td style="width:100px;" class="text-center">${resultado.em_validacao}</td>
                        </tr>

                        <tr style="background-color: #efefef;font-weight: bold;" class="${resultado.a_validar == 0 ? 'bgGreenLight':'bgWhite'}">
                            <td class="nowrap">A validar</td>
                            <td style="width:100px;" class="text-center">${resultado.a_validar}</td>
                        </tr>

                        <tr style="background-color: #fff">
                            <td>Total</td>
                            <td style="width:100px;" class="text-center">${resultado.total}</td>
                        </tr>
                        </tbody>
                    </table>
                </td>

                <td style="vertical-align: top !important;">
                    <table class="table table-striped table-condensed table-bordered">
                        <thead>
                        <th colspan="2">Categorias validadas</th>
                        </thead>
                        <tbody>
                        <tr>
                            <td style="vertical-align: top !important;">
                                <table class="table table-hover table-striped table-condensed table-bordered">
                                    <tbody>

                                        <g:each var="item" in="${resultado.categorias}">
                                        <tr>
                                            <td class="nowrap">${item.key}</td>
                                            <td style="width:100px;" class="text-center">${item.value}</td>
                                        </tr>
                                        </g:each>

                                    </tbody>
                                </table>
                            </td>

                            <td style="vertical-align: top !important;">
                                <table class="table table-hover table-striped table-condensed table-bordered">
                                    <tbody>
                                    <tr style="">
                                        <td class="nowrap">Categorias mantidas</td>
                                        <td style="width:100px;" class="text-center">${resultado.mantidas}</td>
                                    </tr>
                                    <tr>
                                        <td class="nowrap">Categorias alteradas</td>
                                        <td style="width:100px;" class="text-center">${resultado.alteradas}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>

                <td style="vertical-align: top !important;">

                        <table class="table table-striped table-condensed table-bordered" id="tableAcompDesempenho">
                            <thead>
                                <tr>
                                    <th colspan="4">Desempenho</th>
                                </tr>
                                <tr>
                                    <th class="cursor-pointer" style="width:200px;" data-sort-order="${params.sortDesempenho == 'nome'? params.sortOrder:''}" onclick="gerenciarValidacao.acomp.updateTotals({sortDesempenho:'nome'})">Nome&nbsp;${raw(params.sortDesempenho == 'nome'? '<i class="pull-right fa fa-sort-'+params.sortOrder+'"></i>':'')}</th>
                                    <th class="cursor-pointer" style="width:100px;" data-sort-order="${params.sortDesempenho == 'qtdFichas'? params.sortOrder:''}" onclick="gerenciarValidacao.acomp.updateTotals({sortDesempenho:'qtdFichas'})">Fichas&nbsp;${raw(params.sortDesempenho == 'qtdFichas'? '<i class="pull-right fa fa-sort-'+params.sortOrder+'"></i>':'')}</th>
                                    <th class="cursor-pointer" style="width:100px;" data-sort-order="${params.sortDesempenho == 'qtdRespondidas'? params.sortOrder:''}" onclick="gerenciarValidacao.acomp.updateTotals({sortDesempenho:'qtdRespondidas'})">Respondidas&nbsp;${raw(params.sortDesempenho == 'qtdRespondidas'? '<i class="fa fa-sort-'+params.sortOrder+'"></i>':'')}</th>
                                    <th class="cursor-pointer" style="width:100px;" data-sort-order="${params.sortDesempenho == 'percentual'? params.sortOrder:''}" onclick="gerenciarValidacao.acomp.updateTotals({sortDesempenho:'percentual'})">Percentual&nbsp;${raw(params.sortDesempenho == 'percentual'? '<i class="fa fa-sort-'+params.sortOrder+'"></i>':'')}</th>
                                </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td colspan="4">
                                    <div style="border:0px;padding:0px;max-height: 200px;overflow: hidden;overflow-y: auto;">
                                    <table border="0" cellpadding="0" cellspacing="0" class="table table-hover table-striped">
                                        <g:each var="item" in="${dadosDesempenho}">
                                            <tr class="border-bottom:1px solid silver;">
                                            <td style="width:200px;" class="nowrap">${item.nome}</td>
                                            <td style="width:100px;" class="text-center">${item.qtdFichas}</td>
                                            <td style="width:100px;" class="text-center">${item.qtdRespondidas}</td>
                                            <td style="width:100px;" class="text-center">${item.percentual}</td>
                                            </tr>
                                        </g:each>
                                    </table>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>

                </td>
            </tr>
            </tbody>
        </table>




