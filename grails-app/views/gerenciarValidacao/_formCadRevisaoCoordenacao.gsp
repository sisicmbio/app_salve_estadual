<div class="col-sm-12">
    <fieldset>
        <legend>
            <span>Revisando ${nomeValidador}</span>
        </legend>
        <form id="frmRevisaoCoordenacao${ficha.id}" name="frmRevisaoCoordencao" role="form" class="form-inline">
            <div class="form-group w100p">
                    <label for="sqResultado" class="control-label label-required">Resposta</label>
                    <br>
                    <select name="sqResultado" id="sqResultado" data-change="gerenciarValidacao.resultadoRevisaoCoordChange"
                            data-sq-ficha="${ficha.id}"
                            data-contexto="frmRevisaoCoordenacao${ficha.id}"
                            class="fld form-control fldSelect w100p">
                        <option value="">-- selecione --</option>
                        <g:each var="item" in="${listResultados}">
                            <option data-codigo="${item.codigoSistema}"
                                    value="${item.id}" ${item.id == validadorFichaRevisao?.resultado?.id ?'selected':''}>${item.descricao}</option>
                        </g:each>
                    </select>
            </div>

            <div class="form-group hide" id="divCategoria">
                    <label for="sqCategoriaSugerida" class="control-label label-required">Categoria sugerida</label>
                    <br>
                    <select name="sqCategoriaSugerida" id="sqCategoriaSugerida"
                            data-change="gerenciarValidacao.categoriaSugeridaChange"
                            data-sq-ficha="${ficha.id}"
                            data-contexto="frmRevisaoCoordenacao${ficha.id}" class="fld form-control fld400 fldSelect">
                        <option value="">-- selecione --</option>
                        <g:each var="item" in="${listCategoriaIucn}">
                            <option data-codigo="${item.codigoSistema}" value="${item.id}" ${item.id == validadorFichaRevisao?.categoriaSugerida?.id ?'selected':''} >${item.descricaoCompleta}</option>
                        </g:each>
                    </select>
                    <label for="stPossivelmenteExtinta" class="control-label cursor-pointer label-checkbox blue hide" style="margin-top:0px;">Possivelmente extinta?
                       &nbsp;<input name="stPossivelmenteExtinta" id="stPossivelmenteExtinta" value="S" class="checkbox-lg" type="checkbox" ${validadorFichaRevisao?.stPossivelmenteExtinta=='S' ? ' checked':''}/>
                    </label>
            </div>
            <div class="form-group hide"  id="divCriterio">
                <label for="deCriterioSugerido" class="control-label label-required">Critério sugerido</label>
                <br>
                <input name="deCriterioSugerido" id="deCriterioSugerido" onkeyup="fldCriterioAvaliacaoKeyUp(this);" readonly type="text" class="fld form-control fld400 boldAzul"
                       value="${validadorFichaRevisao?.deCriterioSugerido}"/>
                <button class="fld btn btn-default btn-sm" title="Clique para Selecionar o Critério"
                        data-action="openModalSelCriterioAvaliacao"
                        data-field="deCriterioSugerido"
                        data-contexto="frmRevisaoCoordenacao${ficha.id}"
                        data-sq-ficha="${ficha.id}"
                        data-field-categoria="sqCategoriaSugerida"
                        id="btnSelCriterioAvaliacao">...</button>
            </div>
            <div class="form-group w100p" id="divJustificativa">
                    <label for="dsJustificativa${ficha.id}" class="control-label label-required">Justificativa da revisão</label>
                    <br>
                    <textarea name="txsJustificativa" id="txJustificativa"
                              rows="10" class="fld form-control fldTextarea wysiwyg w100p">${raw(validadorFichaRevisao?.txJustificativa?:'')}</textarea>

            </div>

            <g:if test="${canModify}">
                <div class="fld panel-footer">
                    <button data-action="gerenciarValidacao.saveFormRevisaoCoordenacao"
                            data-sq-ficha="${ficha.id}"
                            data-num-validador="${numValidador}"
                            data-sq-validador-ficha-revisao="${validadorFichaRevisao?.id}"
                            data-sq-validador-ficha="${sqValidaorFicha}"
                            class="fld btn btn-success">Gravar revisão</button>
                    <g:if test="${validadorFichaRevisao?.id}">
                        <button data-action="gerenciarValidacao.deleteRevisaoCoordenacao"
                            data-sq-ficha="${ficha.id}"
                            data-num-validador="${numValidador}"
                            data-sq-validador-ficha="${sqValidaorFicha}"
                            data-sq-validador-ficha-revisao="${validadorFichaRevisao?.id}"
                            class="fld btn btn-danger pull-right">Excluir revisão</button>
                    </g:if>
                </div>
            </g:if>
        </form>




    </fieldset>
</div>
