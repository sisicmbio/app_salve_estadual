<!-- view: /views/gerenciarValidacao/_divGridConvites.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<h3 style="margin-bottom:0px;padding-bottom:0px;">Validadores convidados</h3>
<table class="table table-hover table-striped table-condensed table-bordered" style="width:100%" id="tableGridConvites">
    <thead>
        <g:if test="${canModify}">
            <th style="width:30px;"><i title="Marcar/Desmarcar Todos" data-action="gerenciarValidacao.checkUncheckAll" class="glyphicon glyphicon-unchecked"></i></th>
        </g:if>
        <th style="width:200px;">Nome</th>
        <th style="width:200px;">Email</th>
        <th style="width:400px;">Fichas</th>
        <th style="width:80px;">Enviado em</th>
        <th style="width:100px;">Validade</th>
        <th style="width:150px;">Situação</th>
        <th style="width:100px;">Enviado por</th>
        <g:if test="${canModify}">
            <th style="width:60px;">Ação</th>
        </g:if>
    </thead>
    <tbody>
    %{--listConvites = cicloAvaliacaoValidador--}%
    <g:if test="${listConvites?.size() > 0}">
        <g:each var="item" in="${listConvites}"  status="i">
            <tr class="${item.dsSituacaoConviteCodigo == 'CONVITE_ACEITO'?'green':
                    (item.dsSituacaoConviteCodigo=='CONVITE_RECUSADO'?'red':'')}">
                <g:if test="${canModify}">
                    <td class="text-center">
                        <g:if test="${item.dsSituacaoConviteCodigo != 'CONVITE_ACEITO'}">
                            <input id="sqCicloAvaliacaoValidador_${item.sqCicloAvaliacaoValidador}" value="${item.sqCicloAvaliacaoValidador}" type="checkbox" class="checkbox-lg"/>
                        </g:if>
                    </td>
                </g:if>
                <td>${ br.gov.icmbio.Util.capitalize( item.noValidador ) }</td>

                <g:if test="${canModify}">
                    <td>
                        <span id="span-edit-email-participante-${item.sqCicloAvaliacaoValidador}">
                            <span id="span-edit-email-participante-text-${item.sqCicloAvaliacaoValidador}"
                                  class="content">${ item?.deEmail?.toLowerCase() }
                            </span><i onClick="gerenciarValidacao.ediltEmailParticipante(${item.sqCicloAvaliacaoValidador})" class="fa fa-edit ml10 cursor-pointer" title="Alterar email"></i>
                        </span>
                    </td>
                </g:if>





                <td>
                <fieldset>
                    <legend style="font-size:1.2em;">
                        <a data-toggle="collapse" href="#divFichasValidador-${i+1}"
                           data-div-id="divFichasValidador-${i+1}"
                           data-id="${item.sqCicloAvaliacaoValidador}"
                           data-action="gerenciarValidacao.getListaFichas"
                           class="tooltipstered" style="cursor: pointer; opacity: 1;"
                           title="Clique para mostrar/esconder as fichas">
                            ${item.nuFichas}&nbsp;Fichas
                        </a>
                        <g:if test="${canModify}">
                            <button type="button"
                                    data-action="gerenciarValidacao.editFichasValidador"
                                    data-validador="${br.gov.icmbio.Util.capitalize( item.noValidador )}"
                                    data-sq-ciclo-avaliacao-validador="${item.sqCicloAvaliacaoValidador}"
                                    class="pull-right btn btn-default btn-xs">Editar</button>
                        </g:if>
                    </legend>
                    <div id="divFichasValidador-${i+1}" class="panel-collapse collapse" style="width:100%;padding:5px;" role="tabpanel"></div>
                </fieldset>
                </td>

                <td class="text-center">${item.dtEnviadoEm ? item.dtEnviadoEm.format('dd/MM/yyyy') :''}</td>
                <td class="text-center">${item.dtValidadeConvite ? item.dtValidadeConvite.format('dd/MM/yyyy'):''}</td>
                <td class="text-center">${item.dsSituacaoConvite}</td>
                <td class="text-center">${item.noEnviadoPor ?:''}</td>
                <g:if test="${canModify}">
                    <td class="td-actions">
                        <a data-action="gerenciarValidacao.deleteConvite"
                              data-descricao="${item.noValidador}"
                              data-id="${item.sqCicloAvaliacaoValidador}"
                              class="fld btn btn-default btn-xs btn-delete" title="Excluir">
                            <span class="glyphicon glyphicon-remove"></span>
                        </a>
                    </td>
                </g:if>
            </tr>
        </g:each>
    </g:if>
    <g:else>
        <tr>
            <td colspan="9">
                <h4>Nenhuma convite cadastrado!</h4>
            </td>
        </tr>
    </g:else>
    </tbody>
</table>
