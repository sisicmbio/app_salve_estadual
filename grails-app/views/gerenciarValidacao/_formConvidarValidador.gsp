<!-- view: /views/gerenciar/validacao/_formConvidarValidador -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->


<div class="text-left w100p" style="padding:5px;background-color: #d5e1c9;">
    <span>Oficina: <b>${raw(noOficina)}</b></span><br>
    <span>Espécie: <b>${raw(noCientifico)}</b></span>
</div>
<div class="col-sm-12">
    <form id="formConvidarValidador" name="frmRevisaoCoordencao" role="form" class="form-inline">
        <input type="hidden" id="nmCientificoConvidarValidador" value="${br.gov.icmbio.Util.ncItalico(noCientifico,true,false)}">
        <div class="fld form-group w100p">
            <label class="label-required">Nome do validador</label>
            <br>
            <select name="sqPessoaValidador" id="sqPessoaValidador" class="form-control ignoreChange select2 fld700"
                data-email="deEmailValidador" required="true" data-change="gerenciarValidacao.sqPessoaChange"
                data-s2-url="gerenciarValidacao/select2Validador"
                data-s2-placeholder="?"
                data-s2-params="sqOficinaValidacao"
                data-s2-minimum-input-length="2"
                datax-s2-auto-height="true">
            </select>
        </div>
        <div class="form-group w100p">
            <label class="control-label">Email</label>
            <br>
            <input type="text" class="form-control w100p" name="deEmailValidador" id="deEmailValidador" value=""/>
        </div>

        <div class="form-group w100p">
            <label for="deAssuntoEmailValidador" class="control-label">Assunto do email</label>
            <br>
            <input name="deAssuntoEmailValidador" id="deAssuntoEmailValidador" class="fld form-control ignoreChange w100p"
                   value="Convite para validação de ficha de espécie."/>
        </div>

        <div class="form-group w100p">
%{--            <label class="control-label">Mensagem email</label>--}%

            <label class="control-label">Mensagem&nbsp;%{--<span style="margin-left:10px;font-size:12px;color:gray;"> Variáveis:
                  <a title="inserir" href="#" onClick="app.insertTextEditor(this)">{nome}</a>
                , <a title="inserir" href="#" onClick="app.insertTextEditor(this)">{fichas}</a>
                , <a title="inserir" href="#" onClick="app.insertTextEditor(this)">{linkSalve}</a>
                , <a title="inserir" href="#" onClick="app.insertTextEditor(this)">{linkSalveExterno}</a>
                , <a title="inserir" href="#" onClick="app.insertTextEditor(this)">{emailConvidado}</a>
            </span>--}%
            </label>
            <br>
            <textarea name="txMensagemEmailValidador" id="txMensagemEmailValidador"
                      class="fld form-control fldTextarea wysiwyg ignoreChange w100p"
                      rows="10" style="height:200px;max-height:200px;"></textarea>
        </div>


        %{-- botão --}%
        <div class="fld panel-footexr mt20">
            <button data-action="gerenciarValidacao.saveFormConvidarValidador"
                    data-sq-oficina-ficha="${sqOficinaFicha}"
                    data-params="sqPessoaValidador,deEmailValidador,deAssuntoEmailValidador,txMensagemEmailValidador"
                    class="fld btn btn-success pull-left">Convidar</button>
        </div>
    </form>
</div>
