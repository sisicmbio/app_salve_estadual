<!-- view: /views/gerenciarValidacao/index.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<div id="gerenciarValidacaoContainer" style="overflow:hidden;">
   <div class="row">
      <div class="col-sm-12">
         <div class="panel panel-default">
            <div class="panel-heading">
               <span>Módulo Validação</span>
               <button id="btnClose" data-action="app.unloadModule" class="btn btn-default pull-right btn-xs" title="Fechar">
               <span class="glyphicon glyphicon-remove btnClose"></span>
               </button>
            </div>
            <div class="panel-body" id="fichaContainerBody">
               <form id="frmGerConHist" class="form-inline" role="form">
                   <input type="hidden" id="sqCicloAvaliacao" name="sqCicloAvaliacao" value="${listCiclosAvaliacao[0]?.id}"/>
               </form>
               <br>

               <div id="divTabs" class="hidden">
                  <ul class="nav nav-tabs" id="ulTabsGerenciarConsulta">
                      <g:if test="${ session.sicae.user.isADM() }">
                          <li id="liTabCadastro" class="active"><a data-action="gerenciarValidacao.tabClick" data-container="tabCadastro" data-params="" data-toggle="tab" href="#tabCadastro">Cadastro</a></li>
                          <li id="liTabConvidar" class=""><a data-container="tabConvidar" data-toggle="tab" data-action="gerenciarValidacao.tabClick" href="#tabConvidar" data-url="gerenciarValidacao/getFormPessoa" data-params="sqCicloAvaliacao" data-reload="false">Convidar Validador</a></li>
                       </g:if>
                     %{--<li id="liTabAcompanhamento" style="display:none;"><a data-toggle="tab" href="#tabAcompanhamento" data-action="gerenciarValidacao.tabClick" data-url="gerenciarValidacao/getTelaAcompanhamento" data-container="tabAcompanhamento" data-on-load="gerenciarValidacao.acomp.init" data-params="sqCicloAvaliacao" data-reload="false">Acompanhamento</a></li>--}%
                     <li id="liTabAcompanhamento"><a data-toggle="tab" href="#tabAcompanhamento" data-action="gerenciarValidacao.tabClick"
                                                     data-url="gerenciarValidacao/getTelaAcompanhamento"
                                                     data-container="tabAcompanhamento"
                                                     data-params="sqCicloAvaliacao"
                                                     data-on-load="gerenciarValidacao.acomp.tabAcompanhamentoLoaded"
                                                     data-reload="false">Acompanhamento</a></li>
                  </ul>
                  %{-- Container de todas abas --}%
                  <div id="tabContent" class="tab-content">
                       <g:if test="${ session.sicae.user.isADM() }">
                           <div role="tabpanel" class="tab-pane tab-pane-ficha active" id="tabCadastro">
                               <g:render template="formCadOficina"  model="${pageScope.variables}"></g:render>
                           </div>
                           <div role="tabpanel" class="tab-pane tab-pane-ficha" id="tabConvidar"></div>
                         </g:if>
                         <div role="tabpanel" class="tab-pane tab-pane-ficha" id="tabAcompanhamento"></div>
                  </div>
               %{-- fim container de todas abas --}%
               </div>
            %{-- fim divTabs --}%
            </div>
            %{-- fim panel body --}%
         </div>
         %{-- fim panel --}%
      </div>
      %{-- fim row 12 --}%
   </div>
   %{-- fim container --}%
