<!-- view: /views/gerenciarValidacao/_camposFiltrarFichaAcomp.gsp -->

<div class="fld form-group">
    <label for="noValidadorFiltro${rndId ?:''}" class="control-label" title="Informe o nome ou parte do nome de um validador.">Validador</label>
    <br>
    <input type="text" value="" name="noValidadorFiltro" id="noValidadorFiltro${rndId ?:''}" class="fld form-control fld300"/>
</div>
<br>
<g:if test="${ params?.hideFilters?.indexOf('justificativaPendente')< 0 }">
    <div class="form-group">
        <label for="inJustificativaPendenteFiltro${rndId ?:''}" class="control-label">Justificativa pendente?</label>
        <br>
        <select id="inJustificativaPendenteFiltro${rndId ?:''}" name="inJustificativaPendenteFiltro" class="form-control fld200 fldSelect">
            <option value="" selected="true" >-- todas --</option>
            <option value="S">Sim</option>
            <option value="N">Não</option>
        </select>
    </div>
</g:if>
<div class="form-group">
    <label for="deRespostasFiltro${rndId ?:''}" class="control-label">Respostas</label>
    <br>
    <select name="deRespostasFiltro" data-all-label="-- todos --"
            id="deRespostasFiltro${rndId ?:''}"
            data-multiple="true"
            class="form-control fld fld150 fldSelect">
    %{--<option value="">-- todos-- </option>--}%
        <g:each var="item" in="${listRespostas}">
            <option data-codigo="${item?.codigo}"
                    value="${item?.codigo}">${item.descricao}</option>
        </g:each>
    </select>

</div>

<div class="form-group" style="margin-left:5px;border-bottom:1px solid green;">
    <label>Chat</label>
    <br/>
    <div class="radio mr15">
        <label><input name="inComSemChatFiltro" id="inComSemChatTodosFiltro${rndId ?:''}" type="radio" data-change="$('#btnFiltroFicha${rndId}').click()" class="radio-lg" value="" checked="true"/>&nbsp;Todas</label>
    </div>
    <div class="radio mr15">
        <label><input name="inComSemChatFiltro" id="inComSemChatSimFiltro${rndId ?:''}" type="radio"  data-change="$('#btnFiltroFicha${rndId}').click()" class="radio-lg" value="S"/>&nbsp;Com chat</label>
    </div>
    <div class="radio">
        <label><input name="inComSemChatFiltro" id="inComSemChatNaoFiltro${rndId ?:''}" type="radio" data-change="$('#btnFiltroFicha${rndId}').click()" class="radio-lg" value="N"/>&nbsp;Sem chat</label>
    </div>
</div>
