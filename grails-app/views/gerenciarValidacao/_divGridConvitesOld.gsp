<!-- view: /views/gerenciarValidacao/_divGridConvites.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<h3 style="margin-bottom:0px;padding-bottom:0px;">Validadores convidados</h3>
<table class="table table-hover table-striped table-condensed table-bordered">
    <thead>
        <g:if test="${canModify}">
            <th style="width:30px;"><i title="Marcar/Desmarcar Todos" data-action="gerenciarValidacao.checkUncheckAll" class="glyphicon glyphicon-unchecked"></i></th>
        </g:if>
        <th style="width:200px;">Nome</th>
        <th style="width:200px;">Email</th>
        <th style="width:400px;">Fichas</th>
        <th style="width:80px;">Enviado em</th>
        <th style="width:100px;" >Validade</th>
        <th style="width:150px;" >Situação</th>
        <th style="width:100px;" >Enviado por</th>
        <g:if test="${canModify}">
            <th style="width:60px;">Ação</th>
        </g:if>
    </thead>
    <tbody>
    %{--listConvites = cicloAvaliacaoValidador--}%
    <g:if test="${listConvites?.size() > 0}">
        <g:each var="item" in="${listConvites}"  status="i">
            <tr class="${item.situacaoConvite.codigoSistema=='CONVITE_ACEITO'?'green':
                    (item.situacaoConvite.codigoSistema=='CONVITE_RECUSADO'?'red':'')}">
                <g:if test="${canModify}">
                    <td class="text-center">
                        <g:if test="${item.situacaoConvite.codigoSistema!='CONVITE_ACEITO'}">
                            <input id="sqCicloAvaliacaoValidador_${item.id}" value="${item.id}" type="checkbox" class="checkbox-lg"/>
                        </g:if>
                    </td>
                </g:if>
                <td>${br.gov.icmbio.Util.capitalize( item?.validador?.noPessoa ) }</td>
                <td>${item.deEmail}</td>
                <td>
                <fieldset>
                    <legend style="font-size:1.2em;">
                        <a data-toggle="collapse" href="#divFichas-${i}"
                           data-div-id="divFichas-${i}"
                           class="tooltipstered" style="cursor: pointer; opacity: 1;" title="Clique para mostrar/esconder as fichas">
                            ${item?.validadorFicha?.oficinaFicha?.size()}&nbsp;Fichas
                        </a>
                    </legend>
                    <div id="divFichas-${i}" class="panel-collapse collapse" style="text-align:justify;width:100%;padding:5px;" role="tabpanel" style="">
                        ${raw(item?.validadorFicha?.oficinaFicha?.vwFicha?.nmCientifico?.sort()?.collect{br.gov.icmbio.Util.ncItalico( it,true ) }?.join(', '))}
                    </div>
                </fieldset>
                </td>

                <td class="text-center">${item.dtEmail ? item.dtEmail.format('dd/MM/yyyy') :''}</td>
                <td class="text-center">${item.dtValidadeConvite ? item.dtValidadeConvite.format('dd/MM/yyyy'):''}</td>
                <td class="text-center">${item?.situacaoConvite?.descricao}</td>
                <td class="text-center">${item.noUsuario ?:''}</td>
                <g:if test="${canModify}">
                    <td class="td-actions">
                        <a data-action="gerenciarValidacao.deleteConvite" data-descricao="${item?.validador?.noPessoa}" data-id="${item.id}" class="fld btn btn-default btn-xs btn-delete" title="Excluir">
                            <span class="glyphicon glyphicon-remove"></span>
                        </a>
                    </td>
                </g:if>
            </tr>
        </g:each>
    </g:if>
    <g:else>
        <tr>
            <td colspan="9">
                <h4>Nenhuma convite cadastrado!</h4>
            </td>
        </tr>
    </g:else>
    </tbody>
</table>
