<!-- view: /views/gerenciar/validacao/_formEditarFichaValidador -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<div class="form-inline col-sm-12" role="form" id="divEditarFichaValidador${rndId}">
<div>
    <h3 class="text-center blue">${validadorNome}</h3>
</div>

    %{-- FILTRO PELO NIVEL TAXONOMICO --}%
    <input type="hidden" id="sqCicloAvaliacaoValidador${rndId}" value="${rndId}"/>
    <div class="form-group">
        <label for="idNivelFiltro${rndId}" class="control-label"><b>Filtro(s)</b>: Nível taxonômico</label>
        <br>
        <select id="nivelFiltro${rndId}" name="nivelFiltro" data-change="app.filtroNivelTaxonomicoChange" data-sq-ciclo-avaliacao-validador="${rndId}" data-params="rndId:${rndId},callback:gerenciarValidacao.updateGridFichasValidador" class="fld form-control fld200 fldSelect">
            <option value="">-- todos --</option>
            <g:each var="item" in="${listNivelTaxonomico}">
                <option value="${item.coNivelTaxonomico}">${item.deNivelTaxonomico}</option>
            </g:each>
        </select>
    </div>
    <div class="fld form-group">
        <label for="sqTaxonFiltro${rndId}" class="control-label">Taxon</label>
        <br/>
        <select id="sqTaxonFiltro${rndId}" name="sqTaxonFiltro"
                data-s2-visible="false" class="fld form-control select2 fld250"
                data-s2-params="nivelFiltro${rndId}|nivel,sqCicloAvaliacaoValidador${rndId}|sqCicloAvaliacaoValidador"
                data-s2-url="gerenciarValidacao/select2Taxon"
                data-s2-placeholder="?"
                data-s2-maximum-selection-length="2">
        </select>
        <button class="btn btn-primary btn-xs" id="btnFiltrar${rndId}" data-sq-ciclo-avaliacao-validador="${rndId}" data-action="gerenciarValidacao.updateGridFichasValidador">Filtrar</button>
    </div>

    <div class="fld form-group">
        <label for="sqUnidadeFiltro${rndId}" class="control-label">Unidade responsável</label>
        <br>
        <select id="sqUnidadeFiltro${rndId}" name="sqUnidadeFiltro" class="form-control select2 fld500"
                data-sq-ciclo-avaliacao-validador="${rndId}"
                data-s2-minimum-input-length="0"
                data-s2-select-on-close="false"
                data-s2-placeholder="-- todas --"
                data-s2-params="sqCicloAvaliacao,sqOficinaValidacao"
                data-s2-onchange="gerenciarValidacao.updateGridFichasValidador"
                data-s2-url="ficha/select2InstituicaoFiltro">
        </select>
    </div>

     <div class="fld form-group">
            <label for="sqGrupoFiltro${rndId}" class="control-label">Grupo avaliado</label>
            <br>
            <select id="sqGrupoFiltro${rndId}"
                 name="sqGrupoFiltro"
                 data-change="app.btnFiltroFichaClick"
                 data-multiple="true"
                 class="form-control fld350 fldSelect">
                 <g:each var="item" in="${listGrupos}">
                 <option data-codigo-sistema="${item.codigo}" value="${item.id}">${item.descricao}</option>
             </g:each>
         </select>
    </div>

    <div class="fld form-group">
            <label for="sqCategoriaFiltro${rndId}" class="control-label">Categoria avaliada</label>
            <br>
            <select id="sqCategoriaFiltro${rndId}"
                 name="sqCategoriaFiltro"
                 data-change="app.btnFiltroFichaClick"
                 data-multiple="true"
                 class="form-control fld350 fldSelect">
                 <g:each var="item" in="${listCategorias}">
                 <g:if test="${ ! (item.codigo ==~ /AMEACADA|PEX/) }">
                     <option data-codigo-sistema="${item.codigo}" value="${item.id}">${item.descricaoCompleta}</option>
                 </g:if>
             </g:each>
         </select>
         <button class="btn btn-primary btn-sm" data-params="sqCicloAvaliacaoValidador:${rndId}" data-action="gerenciarValidacao.updateGridFichasValidador">Aplicar</button>
        </div>

    %{--GRIDE--}%
    <div id="divGridEditarFichasValidador${rndId}">Carregando...</div>

    %{--ACOES--}%
    <div class="mt5" id="divAcoes${rndId}" style="display:none;">
        <fieldset>
            <legend>Ações</legend>
            <div class="form-group">
                <button type="button" class="btn btn-primary btn-xs" data-dupla="N" data-sq-ciclo-avaliacao-validador="${rndId}" data-action="gerenciarValidacao.deleteFichaValidador">Excluir</button>
                <button type="button" class="btn btn-danger btn-xs" data-dupla="S" data-sq-ciclo-avaliacao-validador="${rndId}" data-action="gerenciarValidacao.deleteFichaValidador">Excluir da dupla</button>
            </div>
            <br>
            <div class="form-group mt10">
                <label class="control-label">Transferir para</label>
                <select id="selectPara${rndId}" class="fld form-control select fld400">
                    <option value=""></option>
                    <g:each var="item" in="${listValidadores}">
                        <option value="${item.sqCicloAvaliacaoValidador}">${item.noPessoa}</option>
                    </g:each>

                </select>
                <button type="button" data-de="${validadorNome} "data-sq-ciclo-avaliacao-validador="${rndId}" data-action="gerenciarValidacao.transferirFichaValidador" class="btn btn-primary btn-xs">Transferir</button>
            </div>

        </fieldset>
    </div>
</div>

