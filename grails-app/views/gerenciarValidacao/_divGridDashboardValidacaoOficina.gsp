<!-- view: /views/gerenciarValidacao/_divGridDashboardValidacaoOficina.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<g:set var="totalDistribuidas" value="0"></g:set>
<g:set var="totalFichas" value="0"></g:set>
<table class="table table-hover table-striped table-condensed table-bordered" id="tabelDashboardValidacaoOficina">
    <thead>
    <th style="width:150px;">Centro</th>
    <th style="width:100px;">Grupo</th>
    <th style="width:100px;">Cat. Anterior</th>
    <th style="width:100px;">Cat. Avaliada</th>
    <th style="width:50px;" >Fichas</th>
    <th style="width:90px;" >Distribuídas</th>
    <th style="width:50px;" >%</th>
    </thead>
    <tbody>
    %{--listConvites = cicloAvaliacaoValidador--}%
    <g:if test="${rows?.size() > 0}">
        <g:each var="row" in="${rows}"  status="i">
            <tr>
                <td class="text-left">${row.sg_unidade_org}</td>
                <td class="text-center">${row.no_grupo}</td>
                <td class="text-center">${row.cd_categoria_ant == 'NE' ? '' : row.cd_categoria_ant }</td>
                <td class="text-center">${row.cd_categoria_ava == 'NE' ? '' : row.cd_categoria_ava }</td>
                <td class="text-center">${row.nu_total}</td>
                <td class="text-center">${row.nu_distribuidas}</td>
                <td class="text-center">${row.nu_percentual}%</td>
            </tr>
            <g:set var="totalDistribuidas" value="${ totalDistribuidas = totalDistribuidas.toInteger() + row.nu_distribuidas.toInteger() }"></g:set>
            <g:set var="totalFichas" value="${ totalFichas = totalFichas.toInteger() + row.nu_total.toInteger() }"></g:set>

        </g:each>
        <tr class="bg-light">
            <td colspan="4" class="text-right bold">Total:&nbsp;</td>
            <td class="text-center bold">${totalFichas}</td>
            <td class="text-center bold" >${totalDistribuidas}</td>
            <td class="text-center bold">${ ( totalFichas.toInteger() > 0 ? (totalDistribuidas.toInteger() / totalFichas.toInteger() * 100 ) : 0).toInteger().toString() }%</td>
        </tr>
    </g:if>
    <g:else>
        <tr>
            <td colspan="7">
                &nbsp;
            </td>
        </tr>
    </g:else>
    </tbody>
</table>
