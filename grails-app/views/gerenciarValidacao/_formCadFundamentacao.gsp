<!-- view: /views/gerenciarValidacao/_formCadFundamentacao.gsp/ -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<div id="frmCadFundamentacaoContainer${ficha.id}" style="padding: 0px 10px">
    <fieldset class="mt10" id="fsFichaCompleta">
        <legend style="font-size:1.2em;">
            <a id="btnShowFsFichaCompleata"
               data-action="app.showHideContainer"
               data-target="divWrapperFichaCompleta_${ficha.id}"
               data-focus=""
               data-callback=""
               title="Mostrar/Esconder ficha!">
                <i class="fa fa-search blue"/>&nbsp;Ficha</a>
        </legend>

        <div class="hidden" id="divWrapperFichaCompleta_${ficha.id}">
            <g:render template="/templates/ficha-completa/fichaCompleta"
                      model="[sqFicha: params.sqFicha, sqOficinaFicha: params.sqOficinaFicha, contexto: 'validacao', validadorFicha: '', classeCss: 'mt20']"></g:render>
        </div>
    </fieldset>


    %{--    INICIO CAMPOS DA VALIDACAO--}%

    <div class="row form form-inline">
        <div class="col-sm-12">
            <fieldset>
                <legend>Resultado Avaliação</legend>
                %{--informações da oficina--}%
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon" style="width:200px;">Categoria</span>
                        <input type="hidden" id="sqCategoriaOficina" value="${ficha?.categoriaIucn?.id}">
                        <input type="text" class="form-control bold"
                               style="width:300px;background-color: lightyellow !important;" readonly
                               value="${ficha?.categoriaIucn?.descricaoCompleta}">
                    </div>
                </div>

                <div class="form-group">
                    <g:if test="${ficha?.dsCriterioAvalIucn}">
                        <div class="input-group">
                            <span class="input-group-addon" style="width:200px;">Critério</span>
                            <input type="text" class="form-control bold"
                                   id="dsCriterioAvalIucnOficina"
                                   style="width:300px !important;background-color: lightyellow !important;" readonly
                                   value="${ficha?.dsCriterioAvalIucn}">
                        </div>
                    </g:if>
                </div>

                <div class="form-group">
                    <g:if test="${ficha.stPossivelmenteExtinta}">
                        <div class="input-group w100p">
                            <span class="input-group-addon" style="width:200px;">Possivelmente extinta?</span>
                            <input type="hidden" id="stPossivelmenteExtintaOficina" value="${ficha?.stPossivelmenteExtinta}">
                            <input type="text" class="form-control bold"
                                   style="width:60px;background-color: lightyellow !important;" readonly
                                   value="${ficha.snd(ficha?.stPossivelmenteExtinta)}">
                        </div>
                    </g:if>
                </div>
                <br>

                <div class="form-group w100p">
                    <div class="input-group w100p">
                        <span class="input-group-addon" style="width:200px;">Justificativa</span>

                        <div class="text-justify"
                             style="min-height: 28px;max-height:200px;overflow-y: auto;padding:3px;border:1px solid silver;background-color: lightyellow !important;">${raw((ficha.dsJustificativa ?: '').replaceAll(/background-color: ?#ffffff;/, 'transparent;'))}</div>
                    </div>
                    %{-- fim resultado oficina --}%
            </fieldset>
        </div>
    </div>


    <div class="row">
        <div class="col-sm-6 mt10" id="divValidadoresContainer${ficha.id}">

            <g:if test="${listValidadorFicha.size() > 0}">
                <g:each var="validadorFicha" in="${listValidadorFicha}" status="index">
                     <g:set var="num" value="${index.toInteger()+1}"/>
                    <fieldset class="${ num > 0 ? 'mt25' : 'mt5' }">
                        %{--<legend>

                            <g:if test="${session.sicae.user.isADM() || session.sicae.user.isPF()}">
                                <span title="${validadorFicha.cicloAvaliacaoValidador.validador.noPessoa}">Validador ${num}</span>
                                <button class="btn btn-outline-info pull-right"
                                    data-sq-validador-ficha="${validadorFicha.id}"
                                    data-sq-ficha="${ficha.id}"
                                    data-win-id="${ficha.id}"
                                    data-num-validador="${num}"
                                    data-action="gerenciarValidacao.cadRevisaoCoordenacao">Revisão coordenação</button>
                            </g:if>
                            <g:else>
                                <span>Validador ${num}</span>
                            </g:else>
                        </legend>--}%
                        <!-- inicio divValidadorFicha 1-->
                        <div id="divValidadorFicha-${validadorFicha.id}" class="form form-inline" data-sq-validador-ficha="${validadorFicha.id}" data-num="${num}">
                            <!-- inicio abas validador 1 -->
                            <ul class="nav nav-tabs" id="ulTabsResultadoValidacao">
                                <li id="liTabResultado${num}" class="${ validadorFicha.revisaoCoordenacao ? '' : 'active'}">
                                    <a data-toggle="tab" href="#tabResultado${num}" title="${ exibirNomeValidador ? validadorFicha.cicloAvaliacaoValidador.validador.noPessoa:'Nome ocultado'}">Validador nº ${num}</a>
                                </li>
                                 <g:if test="${ session.sicae.user.isADM() || validadorFicha.revisaoCoordenacao }">
                                     <li id="liTabRevisao${num}" class="${ validadorFicha.revisaoCoordenacao ? 'active' : ''}">
                                        <a data-toggle="tab" href="#tabRevisao${num}" class="cursor-pointer">Revisão coordenação <i id="tabRevisaoMarker${num}" class="fa fa-check blue ml5 ${ validadorFicha.revisaoCoordenacao ? '':'hide'}" title="Possui revisão"></i></a>
                                     </li>
                                 </g:if>
                            </ul>
                            <!-- Container das abas validador 1-->
                            <div class="tab-content">
                                <!-- aba campos do validador 1 -->
                                <div role="tabpanel" class="mt5 tab-pane tab-pane-ficha ${ validadorFicha.revisaoCoordenacao ? '' : 'active'}" id="tabResultado${num}">

                                    <div class="fld form-group">
                                        <label class="control-label">Resultado</label>
                                        <br>
                                        <input type="hidden" id="sqResultado${num}"
                                               value="${validadorFicha.resultado?.id}"/>
                                        <input type="hidden" id="coResultado${num}"
                                               value="${validadorFicha.resultado?.codigoSistema}"/>
                                        <input type="text" id="dsResultado${num}" readonly class="form-control fld350"
                                               value="${validadorFicha.resultado?.descricao}"/>
                                    </div>

                                    <div class="fld form-group">
                                        <label class="control-label">Categoria sugerida</label>
                                        <br>
                                        <input type="hidden" id="sqCategoriaSugerida${num}"
                                               value="${validadorFicha.categoriaSugerida?.id}"/>
                                        <input type="text" id="dsCategoriaSugerida${num}" readonly class="form-control fld350"
                                               value="${validadorFicha.categoriaSugerida?.descricaoCompleta}"/>
                                        <g:if test="${validadorFicha.categoriaSugerida?.codigoSistema == 'CR' && validadorFicha.stPossivelmenteExtinta == 'S'}">
                                            <span class="ml10 blue" id="stPossivelmenteExtinta${num}">(PEX)</span>
                                        </g:if>
                                        <g:else>
                                            <span class="ml10 blue hide" id="stPossivelmenteExtinta${num}"></span>
                                        </g:else>
                                    </div>

                                    <br>

                                    <div class="fld form-group">
                                        <label class="control-label">Critério sugerido</label>
                                        <br>
                                        <input type="text" id="dsCriterioSugerido${num}" readonly class="form-control fld350"
                                               value="${validadorFicha.deCriterioSugerido?:''}"/>
                                    </div>

                                    <br>

                                    <div class="fld form-group w100p">
                                        <label class="control-label">Fundamentação</label>
                                        <br>
                                        <div id="txJustificativa${num}" class="text-justify"
                                             style="width:100%;padding:3px; min-height:50px; max-height:200px;overflow-y: auto;border:1px solid #eeeeee">
                                            ${raw(validadorFicha.txJustificativa?:'')}
                                        </div>
                                    </div>
                                </div><!-- fim  aba campos do validador 1-->


                                <!-- início aba revisão coordenacao -->
                                <div role="tabpanel" class="mt5 tab-pane tab-pane-ficha ${ validadorFicha.revisaoCoordenacao ? 'active' : ''}" id="tabRevisao${num}">
                                    <div class="fld form-group">
                                        <label class="control-label">Resultado</label>
                                        <br>
                                        <input type="hidden" id="sqResultadoRevisao${num}"
                                               value="${validadorFicha.revisaoCoordenacao ? validadorFicha.revisaoCoordenacao?.resultado?.id : '' }"/>
                                        <input type="hidden" id="coResultadoRevisao${num}"
                                               value="${validadorFicha.revisaoCoordenacao ? validadorFicha.revisaoCoordenacao?.resultado?.codigoSistema : '' }"/>
                                        <input type="text" id="dsResultadoRevisao${num}" readonly class="form-control fld350"
                                               value="${validadorFicha.revisaoCoordenacao ? validadorFicha.revisaoCoordenacao?.resultado?.descricao :''}"/>
                                    </div>

                                    <div class="fld form-group">
                                        <label class="control-label">Categoria</label>
                                        <br>
                                        <input type="hidden" id="sqCategoriaRevisao${num}"
                                               value="${validadorFicha.revisaoCoordenacao ? validadorFicha.revisaoCoordenacao?.categoriaSugerida?.id :'' }"/>
                                        <input type="text" id="dsCategoriaRevisao${num}" readonly class="form-control fld350"
                                               value="${validadorFicha.revisaoCoordenacao ? validadorFicha.revisaoCoordenacao?.categoriaSugerida?.descricaoCompleta :''}"/>
                                        <span id="stPossivelmenteExtintaRevisao${num}"
                                              class="ml10 blue ${ !validadorFicha.revisaoCoordenacao || validadorFicha.revisaoCoordenacao?.stPossivelmenteExtinta != 'S' ? 'hide' : ''}">(PEX)</span>
                                    </div>

                                    <br>

                                    <div class="fld form-group">
                                        <label class="control-label">Critério</label>
                                        <br>
                                        <input type="text" id="dsCriterioRevisao${num}" readonly class="form-control fld350"
                                               value="${validadorFicha.revisaoCoordenacao ? validadorFicha.revisaoCoordenacao.deCriterioSugerido :''}"/>
                                    </div>

                                    <br>

                                    <div class="fld form-group w100p">
                                        <label class="control-label">Justificativa da revisão</label>
                                        <br>
                                        <div id="txJustificativaRevisao${num}" class="text-justify"
                                             style="width:100%;padding:3px; min-height:50px; max-height:200px;overflow-y: auto;border:1px solid #eeeeee">
                                            ${raw(validadorFicha.revisaoCoordenacao ? validadorFicha.revisaoCoordenacao.txJustificativa :'')}
                                        </div>
                                    </div>

                                    <!-- botão cadastrar revisao-->
                                    <g:if test="${ session.sicae.user.isADM() }">
                                        <div>
                                            <button class="btn btn-primary btn-xs pull-left"
                                            data-sq-validador-ficha="${validadorFicha.id}"
                                            data-sq-ficha="${ficha.id}"
                                            data-win-id="${ficha.id}"
                                            data-num-validador="${num}"
                                            data-action="gerenciarValidacao.cadRevisaoCoordenacao">Editar revisão<i class="fa fa-edit ml10"></i></button>
                                        </div>
                                    </g:if>
                                </div><!-- fim aba revisão coordenacao -->
                            </div><!-- fim contaier abas validador 1 -->
                        </div> <!-- fim divValidadorFicha1 -->
                    </fieldset>
               </g:each>
            </g:if>
            <g:else>
                <h4>Nenhum validador cadastrado</h4>
            </g:else>

        </div>

        <div class="col-sm-6 mt10" id="divChatContainer${ficha.id}">
            <g:if test="${listValidadorFicha.size() > 0}">
                <g:render template="/templates/formChat"
                          model="[idValidadorFicha: listValidadorFicha[0].id
                                  , sqFicha: params.sqFicha
                                  , sqOficinaFicha: params.sqOficinaFicha
                                  , contexto: 'validacao'
                                  , classeCss: ''
                                  , convidadoChat: convidadoChat]"></g:render>
            </g:if>
        </div>
    </div>

    <div class="row mt20">
        <div class="col-sm-12">
            <fieldset ${params?.canModify == false ? 'disabled="true"' : ''}>
                <legend class='border-red'>
                    Resultado Validação
                </legend>

                <form id="frmCadFundamentacao" class="form-inline" role="form">
                    %{-- controlar se houve mudança de categoria --}%
                    %{-- <input type="hidden" value="${dadosUltimaAvaliacaoNacional.sqCategoriaIucn}" id="sqCategoriaUltimaAvaliacao${ficha.id}"/> --}%

                    %{-- campos ocultos utilizados para exibir ou nao o motivo da mudanca--}%
                    %{--<input id="sqCategoriaUltimaAvaliacaoNacional${ficha.id}" type="hidden" value="${ultimaAvaliacaoNacional.sqCategoriaIucn}"/>--}%
                    %{--<input id="cdCategoriaUltimaAvaliacaoNacional${ficha.id}" type="hidden" value="${ultimaAvaliacaoNacional.coCategoriaIucn}"/>--}%

                    %{--armazenar dados da ultima avaliação--}%
                    <input type="hidden" id="sqCategoriaIucnUltimaAvaliacao" name="sqCategoriaIucnUltimaAvaliacao"
                           value="${dadosUltimaAvaliacao.sqCategoriaIucn}"/>
                    <input type="hidden" id="cdCategoriaIucnUltimaAvaliacao" name="cdCategoriaIucnUltimaAvaliacao"
                           value="${dadosUltimaAvaliacao.coCategoriaIucn}"/>

                    %{-- CATEGORIA FINAL - FUNDAMENTACAO --}%
                    <div class="form-group">
                        <label for="sqCategoriaIucn" class="control-label">Categoria</label>
                        <br>
                        <select ${(!session.sicae.user.isADM() && (session.sicae.user.isCT() || ficha.dtAceiteValidacao)) ? 'disabled' : ''}
                            id="sqCategoriaIucn"
                            name="sqCategoriaIucn"
                            data-contexto="frmCadFundamentacaoContainer${ficha.id}"
                            data-container="frmCadFundamentacaoContainer${ficha.id}"
                            data-sq-ficha="${ficha.id}"
                            data-change="app.categoriaIucnChange"
                            class="fld form-control fld300 fldSelect">
                            %{-- SE FOR PERFIL CT OU SE A FICHA ESTIVER VALIDADA MOSTRAR SOMENTE 1 OPÇÃO NO CAMPOS SELECT--}%
                            <g:if test="${( ! session.sicae.user.isADM() && ( session.sicae.user.isCT() || ficha.dtAceiteValidacao ) ) }">
                                <option
                                    data-codigo="${ficha?.categoriaFinal?.codigoSistema}"
                                    value="${ficha?.categoriaFinal?.id}"
                                    selected>
                                    ${ficha?.categoriaFinal?.descricaoCompleta?:''}
                                </option>
%{--   REUNIÃO DIA 01/06/2022 - NÃO É MAIS PARA MOSTRAR O RESULTADO DA ABA 11.6 COMO SUGESTÃO DE --}%
%{--   PREENCHIMENTO QUANDO A VALIDAÇÃO NÃO ESTIVER PREENCHIDA  --}%
                            %{--<g:if test="${ficha?.categoriaFinal}">
                                <option
                                data-codigo="${ficha?.categoriaFinal?.codigoSistema}"
                                value="${ficha?.categoriaFinal?.id}"
                                selected>
                                    ${ficha?.categoriaFinal?.descricaoCompleta}
                                </option>
                            </g:if>
                            <g:else>
                                <option data-codigo="${ficha?.categoriaIucn?.codigoSistema}"
                                        value="${ficha?.categoriaIucn?.id}"
                                        selected>${ficha?.categoriaIucn?.descricaoCompleta}
                                </option>
                            </g:else>--}%
                            </g:if>
                            <g:else>
                                <option value="">-- selecione --</option>
                                <g:each var="item" in="${listCategoriaIucn}">
                                    <option data-codigo="${item.codigoSistema}"
                                            value="${item.id}" ${(ficha.categoriaFinal ? ficha?.categoriaFinal?.id : 0) == item.id ? ' selected' : ''}>${item.descricaoCompleta}</option>
                                            %{-- value="${item.id}" ${(ficha.categoriaFinal ? ficha?.categoriaFinal?.id : ficha?.categoriaIucn?.id) == item.id ? ' selected' : ''}>${item.descricaoCompleta}</option>--}%
                                </g:each>
                            </g:else>
                        </select>
                    </div>
                    %{--CRITERIO FINAL - FUNDAMENTAÇAO--}%
                    <div class="form-group" style="display:none;">
                        <label for="dsCriterioAvalIucn" class="control-label">Critério</label>
                        <br>
                        <input name="dsCriterioAvalIucn"
                               id="dsCriterioAvalIucn"
                               readonly
                               type="text"
                               onkeyup="fldCriterioAvaliacaoKeyUp(this);"
                               class="fld form-control fld300 blue"
                               value="${ficha.categoriaFinal ? ficha.dsCriterioAvalIucnFinal : ''}"/>
%{--                               value="${ficha.categoriaFinal ? ficha.dsCriterioAvalIucnFinal : ficha.dsCriterioAvalIucn}"/>--}%
                        <g:if test="${params.canModify}">
                            <button id="btnSelCriterioAvaliacao"
                                    class="fld btn btn-default btn-sm"
                                    type="button" title="Clique para selecionar o critério de avaliação"
                                    data-sq-ficha="${ficha.id}"
                                    data-container="frmCadFundamentacaoContainer${ficha.id}"
                                    data-contexto="frmCadFundamentacaoContainer${ficha.id}"
                                    data-action="openModalSelCriterioAvaliacao"
                                    data-field="dsCriterioAvalIucn"
                                    data-field-categoria="sqCategoriaIucn">...</button>
                        </g:if>
                    </div>

                    %{-- Possivelmente Extinda --}%
                    <div class="form-group" style="display:none;">
                        <label for="stPossivelmenteExtinta" class="control-label cursor-pointer label-checkbox"
                               style="margin-top:17px;">Possivelmente Extinta?&nbsp;
                            <g:if test="${params.canModify}">
                                <input id="stPossivelmenteExtinta"
                                       name="stPossivelmenteExtinta"
                                       class="checkbox-lg"
                                       value="S"
                                       type="checkbox"
                                    ${(ficha.stPossivelmenteExtintaFinal ?: '') == 'S' ? ' checked' : ''}/>
%{--                                    ${(ficha.stPossivelmenteExtintaFinal ?: ficha.stPossivelmenteExtinta) == 'S' ? ' checked' : ''}/>--}%
                            </g:if>
                            <g:else>
                                <input name="stPossivelmenteExtinta" id="stPossivelmenteExtinta"
                                       value="${(ficha.categoriaFinal ? ficha.stPossivelmenteExtintaFinal : '') == 'S' ?: 'N'}"
%{--                                       value="${(ficha.categoriaFinal ? ficha.stPossivelmenteExtintaFinal : ficha.stPossivelmenteExtinta) == 'S' ?: 'N'}"--}%
                                       type="hidden" name="stPossivelmenteExtinta"/>
                                <label><b>${(ficha.categoriaFinal ? ficha.stPossivelmenteExtintaFinal : '') == 'S' ? 'Sim' : 'Não'}</b>
%{--                                <label><b>${(ficha.categoriaFinal ? ficha.stPossivelmenteExtintaFinal : ficha.stPossivelmenteExtinta) == 'S' ? 'Sim' : 'Não'}</b>--}%
                                </label>
                            </g:else>
                        </label>
                    </div>

                    <br class="fld">

                    <div class="form-group w100p">
                        <label for="dsJustificativaFinal${ficha.id}" class="control-label">Justificativa Final</label>
                        <br>
                        <g:if test="${session.sicae.user.isCT() && !ficha?.categoriaFinal}">
                            <div>${raw(ficha.categoriaFinal ? ficha.dsJustificativaFinal : '')}</div>
%{--                            <div>${raw(ficha.categoriaFinal ? ficha.dsJustificativaFinal : ficha.dsJustificativa)}</div>--}%
                        </g:if>
                        <g:else>
                            <g:if test="${params.canModify}">
                                <textarea name="dsJustificativa" id="dsJustificativa${ficha.id}" rows="5" class="fld form-control fldTextarea wysiwyg w100p">${raw(ficha.categoriaFinal ? ficha.dsJustificativaFinal : '')} </textarea>
%{--                                <textarea name="dsJustificativa" id="dsJustificativa${ficha.id}" rows="5" class="fld form-control fldTextarea wysiwyg w100p">${raw(ficha.categoriaFinal ? ficha.dsJustificativaFinal : ficha.dsJustificativa)} </textarea>--}%
                            </g:if>
                            <g:else>
                                <div>${raw(ficha.categoriaFinal ? ficha.dsJustificativaFinal : '')}</div>
%{--                                <div>${raw(ficha.categoriaFinal ? ficha.dsJustificativaFinal : ficha.dsJustificativa)}</div>--}%
                            </g:else>
                        </g:else>
                    </div>

                    %{--<MOTIVO DA MUDANCA--}%
                    <div id="divDadosMotivoMudanca${ficha.id}"
                         class="panel panel-success mt10" style="display:none;">
                        <div
                            class="panel-heading">${raw(dadosUltimaAvaliacao.coCategoriaIucn ? 'Mudança da categoria <span class="cursor-pointer" title="' + dadosUltimaAvaliacao.deCategoriaIucn + '">' + dadosUltimaAvaliacao.coCategoriaIucn + '</span>' : 'Mudança de categoria')}</div>

                        <div class="panel-body" style="background-color:#8fbc8f;">
                            <g:if test="${canModify}">
                                <div class="form-group">
                                    <label for="sqMotivoMudanca" class="control-label">Motivo da mudança</label>
                                    <br>
                                    <select id="sqMotivoMudanca"
                                            class="fld form-control fld300 fldSelect"
                                        ${!canModify ? 'disabled="true"' : ''}>
                                        <option value="">-- selecione --</option>
                                        <g:each var="item" in="${listMudancaCategoria}">
                                            <option value="${item.id}">${item.descricao}</option>
                                        </g:each>
                                    </select>
                                    <button type="button" id="btnSaveMotivoMudanca"
                                            data-action="addMotivoMudanca"
                                            data-sq-ficha="${ficha.id}"
                                            data-container="frmCadFundamentacaoContainer${ficha.id}"
                                            data-contexto="frmCadFundamentacaoContainer${ficha.id}"
                                            data-params="sqFichaMudancaCategoria,sqMotivoMudanca,txFichaMudancaCategoria,canModify:${canModify}"
                                            class="fld btn btn-sm btn-primary">Adicionar motivo mudança</button>
                                </div>
                            </g:if>
                            <div id="divGridMotivoMudancaCategoria" class="mt10"></div>

                            <div class="form-group w100p">
                                <label for="dsJustMudancaCategoria${ficha.id}"
                                       class="control-label">Justificativa para a mudança de categoria</label>
                                <br>
                                <g:if test="${canModify}">
                                    <textarea name="dsJustMudancaCategoria" id="dsJustMudancaCategoria${ficha.id}"
                                              class="fld form-control fldTextarea wysiwyg w100p"
                                              rows="5">${ficha.dsJustMudancaCategoria}</textarea>
                                </g:if>
                                <g:else>
                                    <div class="text-justify"
                                         style="min-height: 28px;max-height: 300px;overflow-y: auto;padding:3px;border:1px solid silver;background-color: #eeeeee">${raw(ficha.dsJustMudancaCategoria)}</div>
                                </g:else>
                            </div>
                        </div>
                    </div> %{--<FIM MOTIVO DA MUDANCA--}%

                %{--fim motivo da mudança--}%

                    <g:if test="${params.canModify}">
                        <g:if test="${!session.sicae.user.isCT() || ficha?.categoriaFinal}">
                            <div class="fld panel-footer">
                                <button id="btnSaveResultadoFinal${ficha.id}"
                                        data-action="gerenciarValidacao.saveFundamentacao"
                                        data-sq-ficha="${ficha.id}"
                                        data-sq-oficina-ficha="${oficinaFicha.id}"
                                        data-contexto="frmCadFundamentacaoContainer${ficha.id}"
                                        class="fld btn btn-success">Gravar validação</button>
                            </div>
                        </g:if>
                    </g:if>
                </form>
            </fieldset>
        </div>
    </div>
</div>
