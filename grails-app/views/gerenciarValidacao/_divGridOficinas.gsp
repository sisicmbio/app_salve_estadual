<!-- view: /views/gerenciarValidacao/_divGridOficinas.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<table id="table${gridId}" class="table table-hover table-striped table-condensed table-bordered">
    <thead>
    %{-- PAGINACAO --}%
    <tr id="tr${gridId}Pagination">
        <td colspan="8" >
            <div class="row">
                <div class="col-sm-8 text-left"  style="min-height:0;height: auto;overflow: hidden;" id="div${(gridId ?: pagination?.gridId)}PaginationContent">
                    <g:if test="${pagination?.totalRecords}">
                        <g:gridPagination  pagination="${pagination}"/>
                    </g:if>
                </div>
                %{-- CAMPO LOCALIZAR (Opcional) --}%
                <div class="col-sm-4">
                    <input type="text" placeholder="localizar..." class="form-control form-control-sm fld250 mt10 hidden"
                           style="float:right;"
                           data-grid-id="${gridId}"
                           onkeyup="gridSelectRows(this)" />
                </div>
            </div>
        </td>
    </tr>
%{--    FILTROS--}%
    <tr id="tr${gridId}Filters" class="table-filters" data-grid-id="${gridId}">

        %{--  COLUNA 1--}%
        <td>
            <div style="display:flex;flex-direction:row; justify-content:space-between;">
                <i class="fa fa-question-circle"
                   title="Utilize os campos ao lado para filtragem dos registros.<br> Informe o valor a ser localizado e pressione <span class='blue'>Enter</span>.<br>Para remover o filtro limpe o campo e pressione <span class='blue'>Enter</span> novamente."></i>
                <i data-grid-id="${gridId}" class="fa fa-filter" title="Aplicar filtro(s)"></i>
                <i data-grid-id="${gridId}" class="fa fa-eraser" title="Limpar filtro(s)"></i>
            </div>
        </td>


        %{-- COLUNA OFICINA--}%
        <td>
            <input type="text" name="fldNoOficina${gridId}"
                   value="${params?.noOficinaFiltro ?: ''}" class="form-control table-filter"
                   placeholder="" data-field="noOficinaFiltro"
            >
        </td>

        %{-- COLUNA LOCAL--}%
        <td>
            <input type="text" name="fldDeLocal${gridId}"
                   value="${params?.deLocalFiltro ?: ''}" class="form-control table-filter"
                   placeholder="" data-field="deLocalFiltro"
            >

        %{-- COLUNA PERIODO --}%
        <td></td>


        %{-- COLUNA SITUACAO--}%
        <td>
            <select name="fldSituacao${gridId}" class="form-control table-filter ignoreChange"
                    placeholder="" data-field="sqSituacaoFiltro">
                <option value="">Todas</option>
                <g:if test="${listSituacaoOficina}">
                    <g:each var="item" in="${listSituacaoOficina}">
                        <option data-codigo="${item?.codigo}"
                            ${item.codigoSistema=='ABERTA' ? ' selected ':''}
                                value="${item.id}">${item?.descricao}</option>
                    </g:each>
                </g:if>
            </select>
        </td>

        %{--        COLUNA FICHAS--}%
        <td></td>

       %{--        COLUNA ACAO--}%
        <td>
            <i class="green fa fa-file-excel-o" title="Exportar para planilha" onclick="gerenciarValidacao.exportToPlanilha();"></i>
        </td>
    </tr>

     %{-- TITULOS COLUNAS --}%
    <tr id="tr${gridId}Columns" data-grid-id="${gridId}">
        <th style="width:70px;">#</th>
        <th style="width:300px;">Oficina</th>
        <th style="width:auto;min-width: 150px">Local</th>
        <th style="width:120px">Período</th>
        <th style="width:130px">Validação</th>
        <th style="width:280px;">Fichas</th>
        <th style="width:80px;">Ação</th>
     </tr>
    </thead>
    <tbody>
    <g:if test="${listOficinas}">
        <g:each var="item" in="${listOficinas}" status="i">
            <tr id="tr-${item.sq_oficina}">

                %{-- NUMERO DA LINHA--}%
                <td class="text-center">
                    ${ i + ( pagination ? pagination?.rowNum.toInteger() : 1 ) }
                </td>

                %{-- SIGLA--}%
                <td>${item.sg_oficina}</td>

                %{-- LOCAL--}%
                <td class="text-center">${item.de_local}</td>

                %{-- PERIODO--}%
                <td class="text-center">${item.de_periodo}</td>

                %{-- fonte de financiamento --}%
                %{--
                <td class="text-center">${item?.fonteRecurso?.descricao}</td>
                --}%

                %{--SITUACAO--}%
                <td class="text-center">
                    <select name="sqSituacao-${item.sq_oficina}" data-table-value="_dtv${item?.ds_situacao}" data-sq-oficina="${item.sq_oficina}"
                            data-change="gerenciarValidacao.sqSituacaoChange" class="form-control fldSelect fld120 ${ item?.cd_situacao_sistema != 'ABERTA' ? 'red' : 'green'}">
                        <g:each in="${listSituacaoOficina}" var="situacao">
                            <option data-codigo="${situacao.codigoSistema}" value="${situacao.id}" ${ situacao?.id == item?.sq_situacao ? 'selected':''}>
                                ${situacao.descricao}
                            </option>
                        </g:each>
                    </select>
                </td>


                %{--FICHAS--}%
                <td>
                    <fieldset id="fieldSet-${i}">
                        <legend style="font-size:1.2em;">
                            <a data-toggle="collapse" href="#divFichas-${i}" id="a-${i}"
                               data-row-num="${i}"
                               data-id="${item?.sq_oficina}"
                               data-action="gerenciarValidacao.linkToggleFichasClick"
                               class="tooltipstered" style="cursor: pointer; opacity: 1;" title="Clique para mostrar/esconder as fichas">
                                <span>${item.nu_fichas}&nbsp;Fichas</span>
                            </a>
                        </legend>
                        <div id="divFichas-${i}" class="panel-collapse collapse" role="tabpanel" style="background-color: lightgoldenrodyellow"></div>
                    </fieldset>
                </td>

                %{-- ACOES --}%

                    <td class="td-actions td-align-bottom">

                        %{-- ALTERAR OFICINA --}%
                        <a data-id="${item?.sq_oficina}" data-action="gerenciarValidacao.edit" class="btn btn-default btn-xs btn-update" data-toggle="tooltip" title="Alterar">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </a>

                        %{-- BAIXAR FICHAS --}%
                        <a data-id="${item?.sq_oficina}" data-contexto="oficina-avaliacao" data-action="gerenciarValidacao.gerarZipFichas" class="btn btn-default btn-xs btn-print" data-toggle="tooltip" title="Fazer download das fichas no formato doc para validaçao off-line.">
                            <span class="glyphicon glyphicon-download-alt"></span>
                        </a>

                        %{-- EXCLUIR OFICINA --}%
                        <a data-id="${item?.sq_oficina}" data-descricao="${item.sg_oficina}" data-action="gerenciarValidacao.delete" class="btn btn-default btn-xs btn-delete" data-toggle="tooltip" title="Excluir">
                            <span class="glyphicon glyphicon-remove"></span>
                        </a>
                    </td>

            </tr>
        </g:each>
    </g:if>
    <g:else>
        <tr>
            <td class="text-center" colspan="7">
                <h4>Nenhuma validação cadastrada!</h4>
            </td>
        </tr>
    </g:else>
    </tbody>
</table>
