<!-- view: /views/gerenciarValidacao/_camposFiltrarFicha.gsp -->

%{-- <div class="form-group">
    <label for="coCategoriaOficinaFiltro${rndId}" class="control-label">Categoria na Oficina</label>
    <br>
    <select id="coCategoriaOficinaFiltro${rndId}" name="coCategoriaOficinaFiltro" data-change="$('#btnFiltroFicha${rndId}').click()" class="form-control fld300 fldSelect">
        <option value="">-- todas --</option>
        <g:each var="item" in="${listCategorias}">
            <option data-codigo="${item.codigoSistema}" value="${item.codigo}">${item.descricaoCompleta}</option>
        </g:each>
    </select>
</div> --}%

<div class="form-group" style="margin-left:5px;border-bottom:1px solid green;">
    <label>Validadores</label>
    <br/>
    <div class="radio mr15">
        <label><input name="inValidadoresFiltro" id="inValidadoresTodos${rndId}" type="radio" data-change="$('#btnFiltroFicha${rndId}').click()" class="radio-lg" value="" checked="true"/>&nbsp;Todas</label>
    </div>
    <div class="radio mr15">
        <label><input name="inValidadoresFiltro" id="inValidadoresSim${rndId}" type="radio"  data-change="$('#btnFiltroFicha${rndId}').click()" class="radio-lg" value="S"/>&nbsp;Com Validadores</label>
    </div>
    <div class="radio">
        <label><input name="inValidadoresFiltro" id="inValidadoresNao${rndId}" type="radio" data-change="$('#btnFiltroFicha${rndId}').click()" class="radio-lg" value="N"/>&nbsp;Sem Validadores</label>
    </div>
</div>
