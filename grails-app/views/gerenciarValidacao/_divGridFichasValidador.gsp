<!-- view: /views/gerenciarValidacao/_divGridFichasValidador.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

%{--<table class="table table-hover table-striped table-condensed table-bordered" id="tableFichasValidador">--}%
<table id="table${gridId}" cellpadding="0" cellspacing="0" class="table table-striped table-sorting">
    <thead>
    <tr id="tr${gridId}Pagination">
        <td colspan="10">
            <div class="text-left" style="min-height:0;height: auto;overflow: hidden" id="div${(gridId ?: pagination.gridId)}PaginationContent">
                <g:if test="${pagination?.totalRecords}">
                    <g:gridPagination pagination="${pagination}"/>
                </g:if>
            </div>
        </td>
    </tr>
    <tr id="tr${gridId}Filters" class="table-filters">
        <td>
            <div style="display:flex;flex-direction:row; justify-content:space-between;">
            <i class="fa fa-question-circle"
               title="Utilize os campos ao lado para filtragem dos registros.<br> Informe o valor a ser localizado e pressione <span class='blue'>Enter</span>.<br>Para remover o filtro limpe o campo e pressione <span class='blue'>Enter</span> novamente."></i>
            <i data-grid-id="${gridId}" class="fa fa-refresh" title="Limpar filtro(s)" onclick="app.gridReset('${gridId}');"></i>
            <i data-grid-id="${gridId}" class="fa fa-filter" title="Aplicar filtro(s)" onclick="app.grid('${gridId}');"></i>
            </div>
        </td>
        <td>
            <input type="text" name="fldNoCientifico${gridId}" value="${params?.noCientifico ?: ''}" class="form-control form-control-sm table-filter"
                   placeholder="nome cientifico" data-field="no_cientifico" data-enter="app.grid('${gridId}');"></td>

        </td>
        %{-- NOME COMUM--}%
        <td>
            <input type="text" name="fldNoComum${gridId}" value="${params?.noComum ?: ''}" class="form-control form-control-sm table-filter"
               placeholder="nome comum" data-field="no_comum" data-enter="app.grid('${gridId}');"></td>
        </td>
        %{-- GRUPOS--}%
        <td>
            <select name="fldGrupoFiltro${gridId}" class="form-control form-control-sm table-filter" placeholder="grupo" data-field="sq_grupo"
                data-change="app.grid('${gridId}');">
            <option value="">Todos</option>
            <g:each var="item" in="${listGrupos}">
                <option value="${item.id}">${item.descricao}</option>
            </g:each>
        </td>
        %{-- RESPOSTAS--}%
        <td>
            <select name="fldRespostaFiltro${gridId}" class="form-control form-control-sm table-filter" placeholder="resposta" data-field="sq_resposta"
                    data-change="app.grid('${gridId}');">
                <option value="">Todas</option>
                <g:each var="item" in="${listRespostas}">
                    <option value="${item.id}">${item.descricao}</option>
                </g:each>
        </td>
        %{-- CATEGORIA AVALIADA--}%
        <td>
            <select name="fldCategoriaAvaliadaFiltro${gridId}" class="form-control form-control-sm table-filter" placeholder="categoria" data-field="sq_categoria_avaliada"
                    data-change="app.grid('${gridId}');">
                <option value="">Todas</option>
                <g:each var="item" in="${listCategoriasAvaliadas}">
                    <option value="${item.id}">${item.descricao}</option>
                </g:each>
        </td>
        %{-- CRITERIO AVALIADO--}%
        <td>
            <select name="fldCriterioAvaliadoFiltro${gridId}" class="form-control form-control-sm table-filter" placeholder="critério" data-field="ds_criterio_avaliado"
                    data-change="app.grid('${gridId}');">
                <option value="">Todos</option>
                <g:each var="item" in="${listCriteriosAvaliados}">
                    <option value="${item.id}">${item.descricao}</option>
                </g:each>
        </td>
        %{-- CATEGORIA VALIDADA--}%
        <td>
            <select name="fldCategoriaValidadaFiltro${gridId}" class="form-control form-control-sm table-filter" placeholder="categoria" data-field="sq_categoria_validada"
                    data-change="app.grid('${gridId}');">
                <option value="">Todas</option>
                <g:each var="item" in="${listCategoriasValidadas}">
                    <option value="${item.id}">${item.descricao}</option>
                </g:each>
        </td>
        %{-- CRITERIO AVALIADO--}%
        <td>
            <select name="fldCriterioValidadoFiltro${gridId}" class="form-control form-control-sm table-filter" placeholder="critério" data-field="ds_criterio_validado"
                    data-change="app.grid('${gridId}');">
                <option value="">Todos</option>
                <g:each var="item" in="${listCriteriosValidados}">
                    <option value="${item.id}">${item.descricao}</option>
                </g:each>
        </td>
        <td></td>
    </tr>
    <tr id="tr${gridId}Columns" data-grid-id="${gridId}">
        <th style="width:60px;">#</th>
        <th style="width:200px;">Nome científico</th>
        <th style="width:200px;">Nome comum</th>
        <th style="width:200px;" class="select-filter">Grupo</th>
        <th style="width:200px;" class="select-filter">Minha<br>resposta</th>
        <th style="width:150px;" class="select-filter">Categoria<br>avaliada</th>
        <th style="width:200px;" class="select-filter">Critério<br>avaliado</th>
        <th style="width:150px;" class="select-filter">Categoria<br>validada</th>
        <th style="width:200px;" class="select-filter">Critério<br>validado</th>
        <th style="width:auto;min-width: 400px">Justificativa final</th>
    </tr>
    </thead>

    <tbody>

    <g:if test="${rows && rows.size() > 0}">
        <g:each var="item" in="${rows}" status="i">
            <g:set var="noCientifico" value="${br.gov.icmbio.Util.ncItalico(item?.nm_cientifico, true)}"></g:set>
            <tr id="tr-${item.sq_ficha}">
                <td style="width:60px;min-width:60px;"
                    class="text-center"><span>${raw(  i + ( pagination?.rowNum ?: 1 )  + (item.ds_resposta ? '</span>&nbsp;<i title="Respondida" class="green glyphicon glyphicon-ok"></i>' : ''))}
                </td>

                %{-- nome cientifico --}%
                <td style="min-width:200px;width:200px;">
                    <g:if test="${ ! item.nu_versao }">
                        ${raw('<a href="#" title="Clique para ver a ficha completa." onClick="showFichaCompleta(' + item?.sq_ficha + ',\'' +
                            br.gov.icmbio.Util.ncItalico(item?.nm_cientifico,false,true,item?.co_nivel_taxonomico) + '\',true,\'validador\',\'validador.updateGrid\',' + canModify + ',' + item?.sq_oficina + ',' + item?.sq_oficina_ficha + ')">' +
                            br.gov.icmbio.Util.ncItalico(item.nm_cientifico, true,true,item?.co_nivel_taxonomico) + '</a>')}
                    </g:if>
                    <g:else>
                        <a href="javascript:void(0);"
                           title="Clique para ver a validação." data-action="showFundamentacaoVersao"
                           data-header="Validação"
                           data-sq-oficina-ficha="${item.sq_oficina_ficha}" data-sq-ficha="${item.sq_ficha}"
                           >
                           ${raw(br.gov.icmbio.Util.ncItalico(item.nm_cientifico, true,true,item?.co_nivel_taxonomico))}
                        </a>
                        &nbsp;
                        <a href="javascript:void(0);" title="Clique para ver a versão da ficha."
                           onClick="showFichaVersao({sqFichaVersao:${item.sq_ficha_versao}})">V.${item.nu_versao}</a>
                    </g:else>
                    ${raw(item.nu_chat > 0 ? '&nbsp;<span title="Bate papo" class="cursor-pointer badge">' + item.nu_chat + '</span>' : '')}
                </td>

                %{-- nome comum --}%
                <td style="min-width:200px;width:200px;">
                    ${item?.no_comum}
                </td>

                %{--GRUPO--}%
                <td style="min-width:200px;width:200px;" class="text-center">${item?.ds_grupo}</td>

                %{--MINHA RESPOSTA--}%
                <td style="min-width:200px;width:200px;" class="text-left">${item?.ds_resposta}.
                    <g:if test="${item?.cd_categoria_sugerida}">
                    %{-- elementos com a class no-select-filter não são considerados como conteúdo da celula para a filtragem da coluna --}%
                        <div class="text-left no-select-filter" style="display:block;">
                            Minha categoria:&nbsp;<span><b>${raw(item.cd_categoria_sugerida ? '<span class="cursor-pointer" title="' + item.ds_categoria_sugerida + '">' + item.cd_categoria_sugerida + '</span>' : '')}</b>
                        </span>
                            <g:if test="${item.ds_possivelmente_extinta.toLowerCase() == 'sim'}">
                                <br><span class="blue">(PEX).</span>
                            </g:if>
                            <g:if test="${item.de_criterio_sugerido}">
                                <br>Meu critério:&nbsp;<span><b>${item.de_criterio_sugerido}</b></span>
                            </g:if>
                        </div>
                    </g:if>
                </td>

                %{-- CATEGORIA OFICINA--}%
                <td style="min-width:200px;width:200px;"
                    class="text-center">${raw((item?.cd_categoria_oficina ? '<span class="cursor-pointer" title="' + item?.ds_categoria_oficina + '">' + item?.cd_categoria_oficina + '</span>' : '') +
                    (item.cd_categoria_sistema_oficina == 'CR' ? (item.st_possivelmente_extinta_oficina ==~ /^S.*/ ? '<br><span class="blue">(PEX)</span>' : '') : ''))}
                </td>

                %{--CRITERIO OFICINA--}%
                <td style="min-width:200px;width:200px;" class="text-center">${item?.ds_criterio_oficina}</td>

                %{--CATEGORIA AVALIACAO FINAL--}%
                <td style="min-width:150px;width:150px;"
                    class="text-center">${raw(item?.cd_categoria_final ? '<span class="cursor-pointer" title="' + item.ds_categoria_final + '">' + item?.cd_categoria_final + '</span>' : '')}</td>

                %{--CRITERIO AVALIACAO FINAL--}%
                <td style="min-width:200px;width:200px;" class="text-center">${item?.ds_criterio_final}</td>

                %{--JUSTIFICAIVA AVALIACAO FINAL--}%
                <td style="min-width:400px;" class="text-center text-justify">
                    <div style="max-height: 300px;overflow-y: auto;padding:5px;">
                        ${raw(item?.ds_justificativa_final)}
                    </div>
                </td>
            </tr>
        </g:each>
    </g:if>
    <g:else>
        <tr>
            <td colspan="10">
                <div class="alert alert-secondary">
                    <g:if test="${!listCicloAvaliacaoValidador || listCicloAvaliacaoValidador.size() > 0}">
                        <span class="red"><h4>Nenhuma ficha encontrada.</h4></span>
                    </g:if>
                    <g:else>
                        <span class="red"><b>Nenhuma ficha disponível para sua validação!</b>
                        </span><br><br><b>Certifique-se:</b>:
                        <ul>
                            <li>de ter <b>recebido e aceitado</b> o convite para validação enviado por email</li>
                            <li>de que a oficina esteja <b>aberta</b></li>
                        </ul>
                        <g:each var="item" in="${listCicloAvaliacaoValidador}">
                            <p>Convite enviado em ${raw(item.dtEmail.format('dd/MM/yyyy HH:mm:ss') + ' -&gt; <b>' + item.situacaoConvite.descricao + '</b>')}</p>
                        </g:each>
                    </g:else>
                </div>
            </td>
        </tr>
    </g:else>

    </tbody>
</table>
