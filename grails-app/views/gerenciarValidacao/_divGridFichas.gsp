<!-- view: /views/gerenciarValidacao/_divGridFichas.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<table class="table table-hover table-striped table-condensed table-bordered" id="tableFichasConvidar">
    <thead>
        <th style="width:60px;">#</th>
        <th style="width:30px;"><i title="Marcar/Desmarcar Todos" data-action="gerenciarValidacao.checkUncheckAll" class="glyphicon glyphicon-unchecked"></i></th>
        <th style="width:300px;">Nome Científico</th>
        <th style="width:100px;" class="select-filter">Categoria</th>
        <th style="width:300px;">Validadores</th>
        <th style="width:100px;" class="select-filter">Grupo Taxonômico</th>
        <th style="width:auto" class="select-filter">Unidade</th>
    </thead>
    <tbody>
    <g:if test="${listOficinaFicha.size() > 0 }">
        <g:each var="item" in="${listOficinaFicha}" status="i">
            <tr>
               <td  class="text-center">${(i+1)}</td>
               <td class="text-center">
                <g:if test = "${item.qtdValidadoresValidos < 2 }">
                   <input id="sqOficinaFicha_${item.id}" value="${item.id}" type="checkbox" class="checkbox-lg"/>
                </g:if>
                <g:else>
                    &nbsp;
                </g:else>
               </td>
                <td>
                    ${ raw( br.gov.icmbio.Util.ncItalico( item?.vwFicha?.nmCientifico,true, true,item?.vwFicha?.coNivelTaxonomico ) )}
                </td>
                <td class="text-center">
                    ${ (item?.vwFicha?.categoriaIucn ? item.vwFicha.categoriaIucn.codigo : '---')}
                </td>
                <td>
                    <ol>
                    <g:each var="reg" in="${item?.validadorFicha}">
                        <li><g:if test="${reg?.cicloAvaliacaoValidador?.situacaoConvite?.codigoSistema == 'CONVITE_ACEITO'}">
                                <span title="Aceito" class="green">
                            </g:if>
                            <g:elseif test="${reg?.cicloAvaliacaoValidador?.situacaoConvite?.codigoSistema == 'CONVITE_RECUSADO'}">
                                <span title="Recusado" class="red">
                            </g:elseif>
                            <g:elseif test="${reg?.cicloAvaliacaoValidador?.dtValidadeConvite > new Date()}">
                                <span title="Vencido" class="orange">
                            </g:elseif>
                            <g:else>
                                <span>
                            </g:else>
                            ${br.gov.icmbio.Util.capitalize( reg?.cicloAvaliacaoValidador.validador?.noPessoa ) }
                            </span>
                        </li>
                    </g:each>
                    </ol>
                </td>
                <td class="text-center">${item?.vwFicha?.grupo?.descricao}</td>
                <td>${item?.vwFicha?.sgUnidadeOrg}</td>
            </g:each>
        </tr>
    </g:if>
    <g:else>
        <tr>
            <td colspan="6">
                <h3>Nenhuma oficina encontrada!</h3>
                <h4>Verifique se existe fichas adicionadas na oficina selecionada na situação avaliada revisada!</h4>
            </td>
        </tr>
    </g:else>
    </tbody>
</table>
