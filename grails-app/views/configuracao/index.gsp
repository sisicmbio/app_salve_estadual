<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main" />
    <title>SALVE Estadual - config</title>
</head>
<body>
<style>
div.preview-foto {
    border:1px dashed blue; display: inline-block;
    max-width: 120px;
    min-height: 70px;
    max-height: 100px;
    overflow: hidden;
}

div.preview-foto.estado {
    max-width: 120px;
    min-height: 70px;
    max-height: 70px;
}

div.preview-foto.brasao {
    max-width: 80px;
    min-height: 90px;
    max-height: 90px;
}
div.preview-foto img{
    width: 100%;
    display: none;
    aspect-ratio: 1 / 1;
}
</style>
<div id="frmConfigWrapper" style="display:flex;justify-content:center; align-items:center; min-width: 100vw; min-height: 85vh">
    <div class="row" style="width:600px;">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <span>Configurações iniciais do SALVE Estadual</span>
                </div>
                <div class="panel-body" id="frmConfigBody">
                    <form id="frmConfig" name="frmConfig" class="form-configuracao" style="margin: 10px;text-align: left;" enctype="multipart/form-data">
                        <label>Selecione o Estado</label>
                        <div style="display: flex; gap:5px; flex-direction: row;justify-content: left;align-items: center">
                            <div class="preview-foto estado" >
                                <img id="img-estado" src="" alt="img">
                            </div>
                            <div style="flex:1;">
                                <select id="sqEstado" name="sqEstado" onChange="configuracao.estadoChange()" class="form-control" placeholder="selecione..." required autofocus>
                                    <option value="">-- selecione --</option>
                                    <g:each var="estado" in="${estados}">
                                        <option data-sigla="${estado.sgEstado}" value="${estado.id}" ${configuracao?.estado?.id == estado.id ? 'selected':''}>${estado.noEstado}</option>
                                    </g:each>
                                </select>
                            </div>

                        </div>

                        <label>Nome da instituição</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-home"></i></span>
                            <input type="text" name="noInstituicao" class="form-control mt-5" required value="${configuracao?.noInstituicao?:''}">
                        </div>

                        <label>Sigla da instituição</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-home"></i></span>
                            <input type="text" name="sgInstituicao" class="form-control mt-5" required value="${configuracao?.sgInstituicao?:''}">
                        </div>

                        <label>Endereço do site (URL)</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-link"></i></span>
                            <input type="text" name="dsLink" placeholder="https://" class="form-control mt-5" value="${configuracao?.dsLink?:''}">
                        </div>

                        <label>Brasão</label>
                        <div style="display: flex;flex-direction: row; gap:5px; justify-content: left;align-items: center;">
                            <div class="preview-foto brasao">
                                <img id="img-brasao" src="" alt="img">
                            </div>
                            <div style="flex:1;">
                                <input type="hidden" id="brasaoCurrentFileName" name="brasaoCurrentFileName"
                                       value="${configuracao.noArquivoBrasao}">
                                <input type="file" id="noArquivoBrasao"
                                       data-show-preview="false"
                                       data-show-upload="false"
                                       name="noArquivoBrasao" onChange="configuracao.brasaoChange(this)" class="form-control">
                            </div>
                        </div>

                        <div class="mt20">
                            <button class="btn btn-lg btn-primary btn-block"
                                    onClick="configuracao.save()"
                                    type="button">
                                <span>Gravar</span><i class="ml10 fa fa-floppy-o"></i>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <asset:javascript src="configuracao.js" defer="true"/>
</body>
</html>
