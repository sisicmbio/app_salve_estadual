//# sourceURL=corpBioma.js
;var corpBioma = {
    map:null,
    init: function () {
        app.focus("noBioma");
        corpBioma.grid();
        corpBioma.showHideForm(); // inicializar o mapa
    },
    loadMap:function(){
        setTimeout(function(){
            corpBioma.map = corpGeo.initMap('mapShapeFile');
        },2000);
    },
    shapeFileChange:function(input){
        corpGeo.clearLayer( corpBioma.map,'layerShapefile' );
        corpGeo.loadShapefile(input.files[0], corpBioma.map,'layerShapefile',true);
    },
    reset:function(){
        $('form#frmBioma')[0].reset();
        $("#sqBioma").val(''); // reset não limpa campos hidden
        $("#fldShapefile").val('');
        corpGeo.clearLayer( corpBioma.map,'layerShapefile' );
        app.focus('noBioma');
    },
    save:function(){
        var data = {'sqBioma':$("#sqBioma").val()
            ,'noBioma':$("#noBioma").val()
            ,'wkt':corpGeo.generateLayerWKT(corpBioma.map,'layerShapefile')
        };
        var errors = [];
        if( ! data.noBioma ){
            errors.push('Nome do bioma deve ser informado')
        }
        if( errors.length > 0 ){
            app.growl(errors.join('<br>'),'4','Erros encontrados','tc','','error');
            return;
        }
        app.ajax(baseUrl+'corpBioma/save',data,function(res){
            if( res.status == 0 ) {
                corpBioma.reset();
                corpBioma.grid( data );
            }
        },'Gravando...')
    },
    edit:function( sqBioma ) {
        if( sqBioma ) {
            app.ajax(baseUrl + 'corpBioma/edit', {sqBioma: sqBioma}, function (res) {
                if (res.status == 0) {
                    $("#sqBioma").val(res.data.sqBioma );
                    $("#noBioma").val(res.data.noBioma );
                    if( ! corpBioma.map ) {
                        corpBioma.loadMap();
                    }
                    corpGeo.showGeojson(corpBioma.map, 'layerShapefile', res.data.geojson, true );
                    app.focus('noBioma');
                }
            },'Carregando...')
        }
    },
    delete:function( sqBioma, noBioma ) {
        if (sqBioma) {
            if ( app.confirm('Confirma a exclusão do bioma <b>' + noBioma + '</b>?', function () {
                app.ajax(baseUrl + 'corpBioma/delete', {sqBioma: sqBioma}, function (res) {
                    if (res.status == 0) {
                        corpBioma.reset()
                        $('#gridContainer table tbody tr#tr-' + sqBioma).remove();
                        corpBioma.updateNumRecords();
                    }
                }, 'Aguarde...')
            })
            ) ;
        }
    },
    grid:function(data){
        data = data || {};
        // atualizar somente a linha alterada no gride
        if( data.sqBioma && data.noBioma ){
            $('#gridContainer table tbody tr#tr-' + data.sqBioma + ' td#tdNoBioma').html(data.noBioma);
            return;
        }
        // recriar o gride
        $("#gridContainer").html('<div class="alert alert-info">Carregando tabela...'+grails.spinner+'</div>');
        app.ajax(baseUrl + 'corpBioma/grid', {}, function (res) {
            $("#gridContainer").html( res );
            corpBioma.updateNumRecords();
        },'')
    },
    updateNumRecords:function(){
        var numRecords = $("#gridContainer table tbody tr").size();
        $("#numRecords").html( String(numRecords ) + ( numRecords == 1 ? '&nbsp;bioma cadastrado' : '&nbsp;biomas cadastrados') );
    }
    /**
     * inicializar o mapa ao abrir o formulário
     * @param params
     */
    ,showHideForm:function(params){
        if( ! corpBioma.map ) {
            corpBioma.loadMap();
        }
    }
}
setTimeout(function(){corpBioma.init(),1000});
