<!-- view: /views/corpBioma/_gridBioma -->
<!-- controller:${params?.controller} -->
<!-- action: ${params?.action} -->
<table class="table table-striped table-responsive table-hover">
    <thead>
    <tr>
        <th style="width: auto">Nome</th>
        <th style="width: 100px;">Ações</th>
    </tr>
    </thead>
    <tbody>
    <g:each var="item" in="${biomas}">
        <tr id="tr-${item.id}">

            %{-- NOME DO BIOMA--}%
            <td id="tdNoBioma" class="text-left">${item.noBioma}</td>

            %{-- ACOES --}%
            <td class="text-center">
                <i class="fa fa-edit fa-button cursor-pointer blue" title="Alterar" onClick="corpBioma.edit(${item.id})"></i>
                <i class="fa fa-remove fa-button cursor-pointer red" title="Excluir" onClick="corpBioma.delete(${item.id},'${item?.noBioma}')"></i>
                %{--                                        <i class="fa fa-map-marker fa-button mouse-pointer green" title="Shapefile" onClick="corpBioma.editShapefile(${item.id},'${item?.noBioma}')"></i>--}%
            </td>
        </tr>
    </g:each>
    </tbody>
</table>