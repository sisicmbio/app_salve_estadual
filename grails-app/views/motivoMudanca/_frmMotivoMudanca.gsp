<div class="window-container text-left">
	<form id="frmMotivoMudanca" name="frmMotivoMudanca" class="form-inline" role="form">

      	<div class="form-group">
			<label for="codigo" class="control-label">
				Código
			</label>
			<br>
			<input name="codigo" id="codigo" type="text" class="form-control form-clean fld100" placeholder="1, 1.1" value="${motivoMudanca?.codigo}" required="false"/>
		</div>

		<div class="form-group">
			<label for="inputMotivoMudanca" class="control-label">
				Descrição do motivo da mudança
			</label>
			<br>
			<input name="descricao" id="inputMotivoMudanca" type="text" class="form-control form-clean fld500" placeholder="Informe o motivo da mudança" value="${motivoMudanca?.descricao}" required="true" data-msg-required="Campo Obrigatório"/>
		</div>
        <br>
        %{--        INTEGRACAO SIS/IUCN--}%
        <div class="form-group">
            <label for="sqIucnMotivoMudanca" class="control-label">
                Motivo (SIS/IUCN)
            </label>
            <br>
            <select name="sqIucnMotivoMudanca" id="sqIucnMotivoMudanca" class="form-control select2" style="width:600px;"
                    data-s2-placeholder="?"
                    data-s2-minimum-input-length="0"
                    data-s2-auto-height="true">
                <option value="">?</option>
                <g:each var="item" in="${listIucnMotivoMudanca}">
                    <option value="${item.id}" ${ item.id == iucnMotivoMudancaApoio?.iucnMotivoMudanca?.id ? " selected":"" }>${ item.descricaoCompleta }</option>
                </g:each>
            </select>
        </div>

        <br>
        <div class="form-group">
            <label for="codigoSistema" class="control-label">
                Código sistema
            </label>
            <br>
            <input name="codigoSistema" id="codigoSistema" type="text" class="form-control form-clean fld400" placeholder="Código uso sistema" value="${motivoMudanca?.codigoSistema}" required="false"/>
        </div>
        <div class="form-group">
            <label for="ordem" class="control-label">
                Ordem listagem
            </label>
            <br>
            <input name="ordem" id="ordem" type="text" class="form-control form-clean fld200" placeholder="Ordem apresentação" value="${motivoMudanca?.ordem}" required="false"/>
        </div>

        <div class="fld panel-footer">
			<button id="btnSaveFrmMotivoMudanca" data-action="motivoMudanca.save" class="fld btn btn-success">Gravar</button>
		</div>
	</form>
</div>
