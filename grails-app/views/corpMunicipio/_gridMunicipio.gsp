<!-- view: /views/corpMunicipio/_gridMunicipio -->
<!-- controller:${params?.controller} -->
<!-- action: ${params?.action} -->
<div id="gridWrapper" style="width:100%;height:230px;border-bottom:1px solid gray;overflow-y: auto;">
%{--<div id="gridContainer" style="border-bottom:1px solid gray;width:400px;height: 270px;">--}%
    <table class="table table-striped table-responsive table-hover" id="tableMunicipios">
        <thead>
        <tr>
            <th style="width: auto">Nome</th>
            <th style="width:100px">Código IBGE</th>
            <th style="width:150px">Estado</th>
            <th style="width:90px;">Ações</th>
        </tr>
        </thead>
        <tbody>
        <g:each var="item" in="${municipios}">
            <tr id="tr-${item.id}">

                %{-- NOME MUNICIPIO --}%
                <td id="tdNoMunicipio" class="text-left">${item.noMunicipio}</td>

                %{-- CÓDIGO IBGE --}%
                <td id="tdCoIbge" class="text-center" id="tdSgMunicipio" class="text-center">${item.coIbge}</td>

                %{-- NOME ESTADO --}%
                <td id="tdNoEstado" class="text-center">${item?.estado?.noEstado}</td>

                %{-- ACOES --}%
                <td class="text-center">
                    <i class="fa fa-edit fa-button cursor-pointer blue" title="Alterar" onClick="corpMunicipio.edit(${item.id})"></i>
                    <i class="fa fa-remove fa-button cursor-pointer red" title="Excluir" onClick="corpMunicipio.delete(${item.id},'${item?.noMunicipio}')"></i>
                </td>
            </tr>
        </g:each>
        </tbody>
    </table>
</div>
<g:if test="${municipios.size()>0}">
<div style="height:auto;border:none;text-align: center;">
    <ul class="pagination pagination-sm" data-total-records="${totalRecords}" data-total-pages="${totalPages}">
        <li class="page-item ${page==1 && page < totalPages ? 'disabled':'' }"><a class="page-link" href="javascript:void(0);"
            <g:if test="${page > 1}">
                onClick="corpMunicipio.grid({ page: ${page.toInteger() - 1 } })"
            </g:if>

        >Anterior</a></li>
        <li class="page-item"><a class="page-link" href="javascript:void(0);">Pág: <select class="fld40" onChange="corpMunicipio.grid({ page: this.value })">
                <g:each var="pageNum" in="${1..totalPages}">
                    <option value="${pageNum}" ${pageNum==page?'selected':''}>${pageNum}</option>
                </g:each>
            </select>
            </a>
        </li>
        <li class="page-item ${page==totalPages ? 'disabled':'' }"><a class="page-link" href="javascript:void(0);"
            <g:if test="${page < totalPages}">
                  onClick="corpMunicipio.grid({ page: ${page.toInteger() + 1 } })"
            </g:if>
        >Próxima</a></li>
    </ul>

</div>
</g:if>