//# sourceURL=corpMunicipio.js
;var corpMunicipio = {
    map:null,
    inputSearch:null,

    init: function () {
        app.focus("noMunicipio");
        corpMunicipio.grid();
        corpMunicipio.showHideForm(); // inicializar o mapa
        // adicionar evento tecla [enter} no campo pesquisar do gride
        corpMunicipio.inputSearch = $("input#inputSearch");
        if( corpMunicipio.inputSearch ){
            corpMunicipio.inputSearch.on('keyup', function( evt ) {
                if( evt.keyCode == 13 || evt.key == 'Enter'){
                    corpMunicipio.grid({ q:corpMunicipio.inputSearch.val()});
                }
            })
        }
    },
    loadMap:function(){
        setTimeout(function(){
            corpMunicipio.map = corpGeo.initMap('mapShapeFile');
        },2000);
    },
    shapeFileChange:function(input){
        corpGeo.clearLayer( corpMunicipio.map,'layerShapefile' );
        corpGeo.loadShapefile(input.files[0], corpMunicipio.map,'layerShapefile',true);
    },
    reset:function(){
        $('form#frmMunicipio')[0].reset();
        $("#sqMunicipio").val(''); // reset não limpa campos hidden
        $("#fldShapefile").val('');
        corpGeo.clearLayer( corpMunicipio.map,'layerShapefile' );
        app.focus('noMunicipio');
    },
    save:function(){
        var data = {'sqMunicipio':$("#sqMunicipio").val()
            ,'noMunicipio':$("#noMunicipio").val()
            ,'coIbge':$("#coIbge").val()
            ,'sqEstado':$("#sqEstado").val()
            ,'wkt':corpGeo.generateLayerWKT(corpMunicipio.map,'layerShapefile')
        };
        var errors = [];
        if( ! data.noMunicipio ){
            errors.push('Nome do municipio deve ser informado')
        }
        if( ! data.coIbge ){
            errors.push('Sigla do municipio deve ser informada')
        }
        if( ! data.coIbge ){
            errors.push('Código do IBGE deve ser informado')
        }
        if( errors.length > 0 ){
            app.growl(errors.join('<br>'),'4','Erros encontrados','tc','','error');
            return;
        }
        app.ajax(baseUrl+'corpMunicipio/save',data,function(res){
            if( res.status == 0 ) {
                corpMunicipio.reset();
                corpMunicipio.grid( data );
            }
        },'Gravando...')
    },
    edit:function( sqMunicipio ) {
        if( sqMunicipio ) {
            app.ajax(baseUrl + 'corpMunicipio/edit', {sqMunicipio: sqMunicipio}, function (res) {
                if (res.status == 0) {
                    $("#sqMunicipio").val( res.data.sqMunicipio );
                    $("#noMunicipio").val( res.data.noMunicipio );
                    $("#coIbge").val( res.data.coIbge );
                    $("#sqEstado").val( res.data.sqEstado );
                    if( ! corpMunicipio.map ) {
                        corpMunicipio.loadMap();
                    }
                    corpGeo.showGeojson(corpMunicipio.map, 'layerShapefile', res.data.geojson ,true);
                    app.focus('noMunicipio');
                }
            },'Carregando...')
        }
    },
    delete:function( sqMunicipio, noMunicipio ) {
        if (sqMunicipio) {
            if ( app.confirm('Confirma a exclusão do municipio <b>' + noMunicipio + '</b>?', function () {
                app.ajax(baseUrl + 'corpMunicipio/delete', {sqMunicipio: sqMunicipio}, function (res) {
                    if (res.status == 0) {
                        corpMunicipio.reset()
                        $('#gridContainer table tbody tr#tr-' + sqMunicipio).remove();
                        corpMunicipio.updateNumRecords();
                    }
                }, 'Aguarde...')
            })
            ) ;
        }
    },
    grid:function(data){
        data = data || {};
        // atualizar somente a linha alterada no gride
        if( data.sqMunicipio && data.noMunicipio ){
            $('#gridContainer table tbody tr#tr-' + data.sqMunicipio + ' td#tdNoMunicipio').html(data.noMunicipio);
            $('#gridContainer table tbody tr#tr-' + data.sqMunicipio + ' td#tdCoIbge').html(data.coIbge);
            $('#gridContainer table tbody tr#tr-' + data.sqMunicipio + ' td#tdNoEstado').html(data.noEstado);
            return;
        }
        // paginação - pagina 1 como padrão
        data.page = data.page ? data.page : 1;
        $("#tableMunicipios").floatThead("destroy");
        // recriar o gride
        $("#gridContainer").html('<div class="alert alert-info">Carregando tabela...'+grails.spinner+'</div>');
        app.ajax(baseUrl + 'corpMunicipio/grid', data,function (res) {
            $("#gridContainer").html( res );
            corpMunicipio.updateNumRecords(data.totalRecords);
            setTimeout(function(){
                $("#tableMunicipios").floatThead({
                    floatTableClass: 'floatHead',
                    scrollContainer: true,
                    autoReflow: true
                });
            },1000);

        },'')
    },
    updateNumRecords:function(){
        var data = $("ul.pagination").data();
        var totalRecords = data.totalRecords ? data.totalRecords : $("#gridContainer table tbody tr").size();
        $("#numRecords").html( String(totalRecords ) + ( totalRecords == 1 ? '&nbsp;Municipio cadastrado' : '&nbsp;Municipios cadastrados') );
    }
    /**
     * inicializar o mapa ao abrir o formulário
     * @param params
     */
    ,showHideForm:function(params){
        if( ! corpMunicipio.map ) {
            corpMunicipio.loadMap();
        }
    }
}
setTimeout(function(){corpMunicipio.init(),1000});
