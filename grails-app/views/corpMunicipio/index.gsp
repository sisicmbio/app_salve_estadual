<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="ajax"/>
    <style>
    .map {
        width: 100%;
        height: 600px;
        background: #e2ecef;
    }
    </style>
</head>
<body>
<div class="col-sm-12">
    <div class="panel panel-default w100p" id="pnlMunicipio">
        <div class="panel-heading">
            <span>
                Municípios
            </span>
            <button id="btnClose" data-action="app.unloadModule" class="btn btn-default pull-right btn-xs" title="Fechar"><span class="glyphicon glyphicon-remove btnClose"></span></button>
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-sm-12 col-md-5">

                    <form id="frmMunicipio" class="row">

                        %{--CAMPO ID--}%
                        <input type="hidden" id="sqMunicipio" name="sqMunicipio" value="">


                        %{-- NOME --}%
                        <div class="form-group col-sm-12">
                            <label class="label-required" for="noMunicipio">Nome do Município</label>
                            <input type="text" class="form-control" id="noMunicipio" name="noMunicipio" value="" maxlength="50">
                        </div>

%{--                        CODIGO IBGE --}%
                        <div class="form-group col-sm-12 col-md-4">
                            <label for="coIbge" class="label-required">Código IBGE</label>
                            <input type="text" class="form-control fld90" id="coIbge" name="coIbge" value=""  data-mask="9999999" maxlength="7">
                        </div>

                        %{-- ESTADO --}%
                        <div class="form-group col-sm-12 col-md-8">
                            <label for="sqEstado" class="label-required">Estado</label>
                            <select class="form-control fld200" id="sqEstado" name="sqEstado">
                                <option value="">-- selecione --</option>
                                <g:each var="estado" in="${estados}">
                                    <option value="${estado.id}">${estado.noEstado}</option>
                                </g:each>
                            </select>
                        </div>

                        <div class="form-group col-sm-12">
                            <label class="control-label">Shapefile do Município</label>
                            <input name="fldShapefile" type="file" id="fldShapefile" class="file-loading"
                                   onchange="corpMunicipio.shapeFileChange(this)"
                                   accept="zip/*"
                                   data-show-preview="false"
                                   data-show-upload="false"
                                   data-show-caption="true"
                                   data-show-remove="true"
                                   data-max-file-count="1"
                                   data-allowed-file-extensions='["zip"]'
                                   data-main-class="input-group-sm"
                                   data-max-file-size="20000"
                                   data-browse-label="Arquivo..."
                                   data-preview-file-type="object">
                        </div>


                        %{--BOTOES--}%
                        <div class="panel-footer col-sm-12">
                            <button type="button" id="btnSave" onclick="corpMunicipio.save();" class="btn btn-success">Gravar</button>
                            <button type="button" class="btn btn-danger pull-right" onclick="corpMunicipio.reset();">Limpar</button>
                        </div>
                        <div class="col-sm-12"><small class="red fs10">* - campo obrigatório</small></div>
                    </form>

                    %{--  GRIDE ABAIXO DO FORM--}%
                    <div class="row mt20">
                        <div class="col-sm-12">
                            <div class="mb5" style="display: flex;justify-content: space-between;flex-direction: row;">
                                <div>
                                    <h4 id="numRecords">&nbsp;</h4>
                                </div>
                                <div class="input-group">
                                    <input id="inputSearch" type="text" class="form-control fld150" value="">
                                    <span class="input-group-addon" title="Informe o nome ou parte do nome e pressione [Enter] para pesquisar">?</span>
                                </div>
                            </div>
                            <div style="width: auto;height: auto;max-height: 280px;" id="gridContainer"></div>
                        </div>
                    </div>
                </div>
                %{--  MAPA --}%
                <div class="col-sm-12 col-md-7">
                    <div>
                        <h4>Polígono do Município</h4>
                    </div>
                     <div id="mapShapeFile" style="width: 100%;height: 600px;background: #e2ecef;"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<asset:javascript src="corpGeo.js" defer="true"/>
</body>
</html>
