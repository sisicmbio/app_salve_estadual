<table class="table table-striped table-bordered table-hover">
   <thead>
      <tr>
         <th>Cpf</th>
         <th>Nome</th>
         <th>Orgão</th>
         <th>Perfil</th>
         <th>Ação</th>
      </tr>
   </thead>
   <tbody>
   <g:each var="item" in="${listUsuarios}">
      <tr>
         <td></td>
         <td></td>
         <td></td>

         <td class="td-actions">
            <a id="btnEditUsuario" data-action="usuario.edit" data-id="${item.id}" class="fld btn btn-default btn-xs btn-update" title="Alterar">
               <span class="glyphicon glyphicon-pencil"></span>
            </a>
            <a id="btnDeleteUsuario" data-action="usuario.delete" data-descricao="${item?.noPessoa}" data-id="${item.id}" class="fld btn btn-default btn-xs btn-delete" title="Excluir">
               <span class="glyphicon glyphicon-remove"></span>
            </a>
         </td>
      </tr>
   </g:each>
   </tbody>
</table>