<div id="usuarioContainer">
  <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          <span>Habilitar/Desabilitar Usuário</span>
          <button id="btnClose" data-action="app.unloadModule" class="btn btn-default pull-right btn-xs" title="Fechar">
          <span class="glyphicon glyphicon-remove btnClose"></span>
          </button>
          <button id="btnAdd" data-action="usuario.addUsuario" class="btn btn-default pull-right btn-xs btnAdd">
          <span class="glyphicon glyphicon-plus"></span>&nbsp; <span>Cadastrar</span>
          </button>
        </div>
        <div class="panel-body" id="cadUsuarioBody">
          <form id="frmCadUsuario" class="form-inline hidden" tabindex="0" role="form">
              <input type="hidden" id="sqLogin" name="sqLogin" />
              <div class="form-group">
                <label for="nuCpf" class="control-label">
                  C.P.F
                </label>
                <br />
                <input class="form-control fld150 cpf" type="text" name='nuCpf' id="nuCpf"/>
              </div>

              <br />

              <div class="form-group">
                <label for="noPessoa" class="control-label">
                  Nome
                </label>
                <br />
                <input class="form-control fld400" type="text" name='noPessoa' id="noPessoa" disabled="true" />
              </div>

              <br />

              <div class="form-group w100p">
                <label for="sqOrgao" class="control-label">
                  Orgão
                </label>
                <br />
              <select class="form-control fldSelect w100p">

              </select>
            </div>

            <br />

            <div class="form-group">
              <label for="deSenha" class="control-label">
                Senha
              </label>
              <br />
              <input class="form-control fld200" type="password" name="deSenha" id="deSenha" />
            </div>
            <div class="form-group">
              <label for="deSenha2" class="control-label">
                Redigite
              </label>
              <br />
              <input class="form-control fld200" type="password" name="deSenha2" id="deSenha" />
            </div>
            <br />
            <div class="form-group">
              <label class="control-label">
                Perfis
              </label>
              <br />
              <div class="checkbox">
                <label>
                  <input type="checkbox" id="chkAdministrador" name="chkAdministrador"> Administrador
                </label>
              </div>

              <div class="checkbox" style="margin-left:20px">
                <label>
                  <input type="checkbox" id="chkPontoFocal" name="chkPontoFocal"> Ponto Focal
                </label>
              </div>
            </div>
            %{-- Botões --}%
            <div class="fld panel-footer">
              <button id="btnSaveFrmUsuario" data-action="usuario.save" class="btn btn-success">Gravar</button>
              <button id="btnResetFrmUsuario" data-action="usuario.reset" class="btn btn-info hidden">Limpar Formulário</button>
            </div>
          </form>
          <div id="divGridUsuarios" class="mt10">
              <g:render template="divGridUsuarios" model="[listUsuarios:listUsuarios]"></g:render>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>