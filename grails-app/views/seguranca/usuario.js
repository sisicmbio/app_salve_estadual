//# sourceURL=N:\salve\salve\grails-app\views\seguranca\usuario.js
//
var usuario = {

    init:function()
    {
        $("#nuCpf").on('blur',function(){
            usuario.validarCpf();
        })
    },
    addUsuario:function( params )
    {
        $("#frmCadUsuario").toggleClass('hidden');
        if( $("#frmCadUsuario").hasClass('hidden') )
        {
            $("#btnAdd span:eq(1)").text('Cadastrar')
            $("#btnAdd span:eq(0)").toggleClass('glyphicon-arrow-left');
        }
        else
        {
            $("#btnAdd span:eq(1)").text('Usuários')
            $("#btnAdd span:eq(0)").toggleClass('glyphicon-arrow-left');
        }
    },
    validarCpf:function()
    {
        var cpf = $("#nuCpf").val();
        if( cpf && cpf.isCPF() )
        {
            console.log( 'Validar cpf no sicae ' + cpf);
        }


    },
}
usuario.init();