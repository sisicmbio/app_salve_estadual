<!-- view: /views/instituicaoa/_divGridModalCadInstituicao.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<table class="table table-hover table-striped table-condensed table-bordered">
    <thead>
        <th style="width:50px;">#</th>
        <th style="width:*;">Nome</th>
        <th style="width:200px;">Sigla</th>
        <th style="width:80px;">Ação</th>
    </thead>
    <tbody>
    <g:if test="${listWebInstituicao}">
        <g:each var="item" in="${listWebInstituicao}" status="i">
            <tr>
                <td class="text-center">${1+i}</td>
                <td>${item.noInstituicao}</td>
                <td>${item.sgInstituicao}</td>
                <td class="td-actions">
                     <a data-id="${item?.id}" data-action="instituicao.edit" class="btn btn-default btn-xs btn-update" data-toggle="tooltip" title="Alterar">
                        <span class="glyphicon glyphicon-pencil"></span>
                    </a>
                    <a data-id="${item?.id}" data-descricao="${item.noInstituicao}" data-action="instituicao.delete" class="btn btn-default btn-xs btn-delete" data-toggle="tooltip" title="Excluir">
                        <span class="glyphicon glyphicon-remove"></span>
                    </a>
                </td>
            </tr>
        </g:each>
    </g:if>
    <g:else>
        <tr><td class="text-center" colspan="8"><h4>Nenhuma Institiuição Encontrada</h4></td></tr>
    </g:else>
    </tbody>
</table>
