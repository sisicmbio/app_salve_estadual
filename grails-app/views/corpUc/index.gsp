<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="ajax"/>
    <style>
    .map {
        width: 100%;
        height: 600px;
        background: #e2ecef;
    }
    /*.form-group {
        margin-bottom: 0;
    }*/
    </style>
</head>
<body>
<div class="col-sm-12">
    <div class="panel panel-default w100p" id="pnlUc">
        <div class="panel-heading">
            <span>
                Unidades de Conservação - Brasil
            </span>
            <button id="btnClose" data-action="app.unloadModule" class="btn btn-default pull-right btn-xs" title="Fechar"><span class="glyphicon glyphicon-remove btnClose"></span></button>
        </div>

        <div class="panel-body">
            <fieldset class="col-sm-12">
                <legend>
                    <a  id="btnToggleForm"
                        data-action="app.showHideContainer"
                        data-target="divCadastroWrapper"
                        data-focus="sqEsfera"
                        data-callback="corpUc.showHideForm"
                        title="Mostrar/Esconder formulário!">
                        <i class="fa fa-plus green"/>&nbsp;Formulario</a>

                </legend>
                <div class="row hidden" id="divCadastroWrapper">

                    <div class="col-sm-12 col-md-5">

                        <form id="frmUc" class="row">

                            %{--CAMPO ID--}%
                            <input type="hidden" id="sqPessoa" name="sqPessoa" value="">


                            %{-- ESFERA --}%
                            <div class="form-group col-sm-12 col-md-12">
                                <label class="label-required" for="sqEsfera">Esfera</label>
                                <select class="form-control fld200" id="sqEsfera" name="sqEsfera">
                                    <option value="">-- selecione --</option>
                                    <g:each var="esfera" in="${esferas}">
                                        <option value="${esfera.id}">${esfera.descricao}</option>
                                    </g:each>
                                </select>
                            </div>

                            %{-- TIPO --}%
                            <div class="form-group col-sm-12 col-md-12">
                                <label class="label-required" for="sqTipo">Tipo</label>
                                <select class="form-control" id="sqTipo" name="sqTipo">
                                    <option value="">-- selecione --</option>
                                    <g:each var="tipo" in="${tipos}">
                                        <option value="${tipo.id}">${tipo.descricao}</option>
                                    </g:each>
                                </select>
                            </div>


                            <br>

                            %{-- NOME --}%
                            <div class="form-group col-sm-12 col-md-12">
                                <label class="label-required" for="noPessoa">Nome da UC</label>
                                <input type="text" class="form-control" id="noPessoa" name="noPessoa" value="">
                            </div>

                            <br>

                            %{-- SIGLA  --}%
                            <div class="form-group col-sm-12 col-md-12">
                                <label for="sgInstituicao">Sigla</label>
                                <input type="text" class="form-control" id="sgInstituicao" name="sgInstituicao" value="">
                            </div>

                            <br>

                            %{-- CNUC --}%
                            <div class="form-group col-sm-12 col-md-5">
                                <label for="cdCnuc" title="Código do Cadastro Nacional de Unidade de Conservação">CNUC</label>
                                <input type="text" class="form-control" id="cdCnuc" name="cdCnuc" value="" data-mask="000.00.0000" maxlength="11">
                                <a target="_blank" href="https://www.gov.br/icmbio/pt-br/servicos/geoprocessamento/mapa-tematico-e-dados-geoestatisticos-das-unidades-de-conservacao-federais/DadosGerais_UC_nov_2020.pdf"
                                   title="Visualizar tabela PDF das UC">Ver PDF</a>
                                <span>|</span>
                                <a target="_blank" href="https://www.icmbio.gov.br/educacaoambiental/politicas/snuc.html"
                                   title="Visitar o site sobre o CNUC">Sobre</a>
                            </div>


                            %{-- CNPJ --}%
                            <div class="form-group col-sm-12 col-md-5">
                                <label for="nuCnpj">CNPJ</label>
                                <input type="text" class="form-control" id="nuCnpj" name="nuCnpj" value="" data-mask="00.000.000/0000-00" maxlength="18">
                            </div>

                            <br>

                            %{-- SHAPE --}%
                            <div class="form-group col-sm-12">
                                <label class="control-label">Shapefile da UC</label>
                                <input name="fldShapefile" type="file" id="fldShapefile" class="file-loading"
                                       onchange="corpUc.shapeFileChange(this)"
                                       accept="zip/*"
                                       data-show-preview="false"
                                       data-show-upload="false"
                                       data-show-caption="true"
                                       data-show-remove="true"
                                       data-max-file-count="1"
                                       data-allowed-file-extensions='["zip"]'
                                       data-main-class="input-group-sm"
                                       data-max-file-size="20000"
                                       data-browse-label="Arquivo..."
                                       data-preview-file-type="object">
                            </div>


                            %{--BOTOES--}%
                            <div class="panel-footer col-sm-12">
                                <button type="button" id="btnSave" onclick="corpUc.save();" class="btn btn-success">Gravar</button>
                                <button type="button" class="btn btn-danger pull-right" onclick="corpUc.reset();">Limpar</button>
                            </div>
                            <div class="col-sm-12"><small class="red fs10">* - campo obrigatório</small></div>
                        </form>
                    </div>
                    %{--  MAPA --}%
                    <div class="col-sm-12 col-md-7">
                        <div>
                            <h4>Polígono da UC</h4>
                        </div>
                        <div id="mapShapeFile" style="width: 100%;height: 600px;background: #e2ecef;"></div>
                    </div>
                </div>
            </fieldset>

            %{-- GRIDE ABAIXO --}%
            <div class="col-sm-12 mt5">
                <h4 id="numRecords">&nbsp;</h4>
                <div style="width: auto;height: auto;" id="gridContainer"></div>
            </div>

        </div>
    </div>
</div>
<asset:javascript src="corpGeo.js" defer="true"/>
</body>
</html>
