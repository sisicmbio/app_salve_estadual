//# sourceURL=corpUc.js
;var corpUc = {
    map:null,
    init: function () {
        app.focus("sqEsfera");
        corpUc.grid();
    },
    loadMap:function(){
        // devido a div do formulario estar fechada, aguardar que a sua abertura
        // para renderizar o mapa na tela
        setTimeout(function(){
            corpUc.map = corpGeo.initMap('mapShapeFile');
        },2000);

    },
    shapeFileChange:function(input){
        // se o mapa não tiver sido criado, cria-lo.
        if( ! corpUc.map ) {
            corpUc.loadMap();
            setTimeout(function(){
                corpUc.shapeFileChange(input);
            },4000)
            return;
        }
        corpGeo.clearLayer( corpUc.map,'layerShapefile' );
        corpGeo.loadShapefile(input.files[0], corpUc.map,'layerShapefile',true);
    },
    reset:function(){
        app.reset('frmUc','sqEsfera,sqTipo');
        //$('form#frmUc')[0].reset();
        //$("#sqPessoa").val(''); // reset não limpa campos hidden
        $("#fldShapefile").val('');
        corpGeo.clearLayer( corpUc.map,'layerShapefile' );
        app.focus('noPessoa');
    },
    save:function(){
        var data = {'sqPessoa':$("#sqPessoa").val()
            ,'sqEsfera':$("#sqEsfera").val()
            ,'sqTipo':$("#sqTipo").val()
            ,'noPessoa':$("#noPessoa").val()
            ,'sgInstituicao':$("#sgInstituicao").val()
            ,'cdCnuc':$("#cdCnuc").val()
            ,'nuCnpj':$("#nuCnpj").val()
            ,'wkt':corpGeo.generateLayerWKT(corpUc.map,'layerShapefile')
        };
        var errors = [];
        if( ! data.sqEsfera ){
            errors.push('Esfera deve ser informada')
        }
        if( ! data.sqTipo ){
            errors.push('Tipo da UC deve ser informado')
        }
        if( ! data.noPessoa ){
            errors.push('Nome da UC deve ser informado')
        }
        if( errors.length > 0 ){
            app.growl(errors.join('<br>'),'4','Erros encontrados','tc','','error');
            return;
        }
        app.ajax(baseUrl+'corpUc/save',data,function(res){
            if( res.status == 0 ) {
                corpUc.reset();
                corpUc.grid( data );
            }
        },'Gravando...')
    },
    edit:function( sqPessoa ) {
        if( sqPessoa ) {
            if( !$("#mapShapeFile").is(":visible")){
                $("#btnToggleForm").click();
                setTimeout(function() {
                    corpUc.edit( sqPessoa);
                    },3000 );
                return;
            }
            app.ajax(baseUrl + 'corpUc/edit', {sqPessoa: sqPessoa}, function (res) {
                if (res.status == 0) {
                    $("#sqPessoa").val(res.data.sqPessoa );
                    $("#sqEsfera").val(res.data.sqEsfera );
                    $("#sqTipo").val(res.data.sqTipo );
                    $("#noPessoa").val(res.data.noPessoa );
                    $("#sgInstituicao").val(res.data.sgInstituicao );
                    $("#cdCnuc").val(res.data.cdCnuc );
                    $("#nuCnpj").val(res.data.nuCnpjFormatado );
                    if( ! corpUc.map ) {
                        corpUc.loadMap();
                    }
                    corpGeo.showGeojson(corpUc.map, 'layerShapefile', res.data.geojson, true );
                    app.focus('sqEsfera');
                }
            },'Carregando...')
        }
    },
    delete:function( sqPessoa, noPessoa ) {
        if (sqPessoa) {
            if ( app.confirm('Confirma a exclusão da UC <b>' + noPessoa + '</b>?', function () {
                app.ajax(baseUrl + 'corpUc/delete', {sqPessoa: sqPessoa}, function (res) {
                    if (res.status == 0) {
                        corpUc.reset()
                        $('#gridContainer table tbody tr#tr-' + sqPessoa).remove();
                        corpUc.updateNumRecords();
                    }
                }, 'Aguarde...')
            })
            ) ;
        }
    },
    grid:function(data){
        data = data || {};
        // atualizar somente a linha alterada no gride
        if( data.sqPessoa && data.noPessoa ){
            $('#gridContainer table tbody tr#tr-' + data.sqPessoa + ' td#tdNoPessoa').html(data.noPessoa);
            $('#gridContainer table tbody tr#tr-' + data.sqPessoa + ' td#tdSgInstituicao').html(data.sgInstituicao);
            $('#gridContainer table tbody tr#tr-' + data.sqPessoa + ' td#tdNoEsfera').html(data.noEsfera);
            $('#gridContainer table tbody tr#tr-' + data.sqPessoa + ' td#tdCdCnuc').html(data.cdCnuc);
            $('#gridContainer table tbody tr#tr-' + data.sqPessoa + ' td#tdNuCnpj').html(data.nuCnpjFormatado);
            return;
        }
        data.page = data.page ? data.page : 1;
        $("#tableUc").floatThead("destroy");

        // recriar o gride
        $("#gridContainer").html('<div class="alert alert-info">Carregando tabela...'+grails.spinner+'</div>');

        app.ajax(baseUrl + 'corpUc/grid', data, function (res) {
            $("#gridContainer").html( res );
            corpUc.updateNumRecords();
            setTimeout(function(){
                $("#tableUc").floatThead({
                    floatTableClass: 'floatHead',
                    scrollContainer: true,
                    autoReflow: true
                });
            },1000);
        },'')
    },
    updateNumRecords:function(){
        /*var numRecords = $("#gridContainer table tbody tr").size();
        $("#numRecords").html( String(numRecords ) + ( numRecords == 1 ? '&nbsp;UC cadastrada' : '&nbsp;UC cadastradas') );
         */
        /*
        var data = $("ul.pagination").data();
        var totalRecords = data.totalRecords ? data.totalRecords : $("#gridContainer table tbody tr").size();
        $("#numRecords").html( String(totalRecords ) + ( totalRecords == 1 ? '&nbsp;UC cadastrada' : '&nbsp;UC cadastradas') );
         */

    },
    searchGrid:function(){
        if( event && event.keyCode==13){
            var data = {}
            $("#tr-filters input").map(function(index,input){
                if( input.value ){
                    data[$(input).data('field')]=input.value;
                }
            });
            corpUc.grid(data);
        }
    },
    /**
     * inicializar o mapa ao abrir o formulário
     * @param params
     */
    showHideForm:function(params){
        if( ! corpUc.map ) {
            corpUc.loadMap();
        }
    }
}
setTimeout(function(){corpUc.init(),1000});
