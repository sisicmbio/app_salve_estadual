<!-- view: /views/corpUc/_gridUc -->
<!-- controller:${params?.controller} -->
<!-- action: ${params?.action} -->
<div id="gridWrapper" style="width:100%;height:calc(100vh - 290px);overflow-y:auto;border-bottom:1px solid gray;">
    <table class="table table-striped table-responsive table-hover" id="tableUc">
        <thead>
        <tr id="tr-filters">
            <th></th>
            %{-- FILTRO NOME/SIGLA--}%
            <th colspan="2">
                <input type="text" value="${params.noPessoa?:''}" class="form-control-sm w100p" onkeyup="corpUc.searchGrid()" data-field="noPessoa" placeholder="localizar...">
            </th>

            %{-- FILTRO ESFER A--}%
            <th>
                <input type="text" value="${params.noEsfera?:''}" class="form-control-sm w100p" onkeyup="corpUc.searchGrid()" data-field="noEsfera">
            </th>

            %{-- FILTRO TIPO --}%
            <th>
                <input type="text" value="${params.dsTipo?:''}" class="form-control-sm w100p" onkeyup="corpUc.searchGrid()" data-field="dsTipo">
            </th>

            %{-- FILTRO CNUC --}%
            <th>
                <input type="text" value="${params.cdCnuc?:''}" class="form-control-sm w100p" onkeyup="corpUc.searchGrid()" data-field="cdCnuc">
            </th>

            %{-- FILTRO CNPJ --}%
            <th>
                <input type="text" value="${params.nuCnpj?:''}" class="form-control-sm w100p" onkeyup="corpUc.searchGrid()" data-field="nuCnpj" data-mask="00.000.000/0000-00">
            </th>

            <th>
                &nbsp;
            </th>
        </tr>
        <tr>
            <th style="width: 50px">#</th>
            <th style="width: auto">Nome</th>
            <th style="width: 200px">Sigla</th>
            <th style="width: 120px">Esfera</th>
            <th style="width: 200px">Tipo</th>
            <th style="width: 120px">CNUC</th>
            <th style="width: 150px">CNPJ</th>
            <th style="width: 100px;">Ações</th>
        </tr>
        </thead>
        <tbody>
        <g:if test="${!unidades}">
            <tr>
                <td colspan="8">
                    <div class="alert alert-danger">Nenhum registro encontrado</div>
                </td>
            </tr>
        </g:if>
        <g:each var="item" in="${unidades}" status="index">
            <tr id="tr-${item.id}">

                %{-- NUMERO DA LINHA --}%
                <td class="text-center">${(index+1)+firstRowNum}</td>

                %{-- NOME DA UC--}%
                <td id="tdNoPessoa" class="text-left">${item.instituicao.pessoa.noPessoa}</td>

                %{-- SIGLA UC --}%
                <td id="tdSgInstituicao" class="text-center">${item.instituicao.sgInstituicao}</td>

                %{-- ESFERA --}%
                <td id="tdNoEsfera" class="text-center">${item.esfera.descricao}</td>

                %{-- TIPO UC --}%
                <td id="tdDsTipo" class="text-center">${item.tipo.descricao}</td>

                %{-- CNUC --}%
                <td id="tdCdCnuc" class="text-center">${item.cdCnuc}</td>

                %{-- CNPJ --}%
                <td id="tdNuCnpj" class="text-center">${item.instituicao.cnpjFormatado}</td>

                %{-- ACOES --}%
                <td class="text-center">
                    <i class="fa fa-edit fa-button cursor-pointer blue" title="Alterar" onClick="corpUc.edit(${item.id})"></i>
                    <i class="fa fa-remove fa-button cursor-pointer red" title="Excluir" onClick="corpUc.delete(${item.id},'${item?.instituicao.sgInstituicao}')"></i>
                    %{--                                        <i class="fa fa-map-marker fa-button mouse-pointer green" title="Shapefile" onClick="corpUc.editShapefile(${item.id},'${item?.noUc}')"></i>--}%
                </td>
            </tr>
        </g:each>
        </tbody>
    </table>
</div>

<g:if test="${unidades.size()>0}">
    <div style="height:auto;border:none;text-align: center;">
        <ul class="pagination pagination-sm" data-total-records="${totalRecords}" data-total-pages="${totalPages}">
            <li class="page-item ${page==1 && page < totalPages ? 'disabled':'' }"><a class="page-link" href="javascript:void(0);"
                <g:if test="${page > 1}">
                    onClick="corpUc.grid({ page: ${page.toInteger() - 1 } })"
                </g:if>

            >Anterior</a></li>
            <li class="page-item"><a class="page-link" href="javascript:void(0);">Pág: <select class="fld40" onChange="corpUc.grid({ page: this.value })">
                <g:each var="pageNum" in="${1..totalPages}">
                    <option value="${pageNum}" ${pageNum==page?'selected':''}>${pageNum}</option>
                </g:each>
            </select>
            </a>
            </li>
            <li class="page-item ${page==totalPages ? 'disabled':'' }"><a class="page-link" href="javascript:void(0);"
                <g:if test="${page < totalPages}">
                    onClick="corpUc.grid({ page: ${page.toInteger() + 1 } })"
                </g:if>
            >Próxima</a></li>
        </ul>
    </div>
</g:if>