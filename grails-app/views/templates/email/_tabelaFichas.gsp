<div style="width:800px;border:none;height:auto;">
	<table border="0" cellspacing="1" cellpadding="3" width="750">
		<thead>
			<tr bgColor="#EBF2E3">
				<td colspan="4" style="border:1px solid #8cb13d;">Fichas(s)</td>
			</tr>
			<tr>
				<td width="50" align="center" valign="top" bgColor="#EBF2E3" style="border:1px solid #8cb13d">#</td>
				<td width="300" align="center" valign="top" bgColor="#EBF2E3" style="border:1px solid #8cb13d">Grupo</td>
				<td width="200" align="center" valign="top" bgColor="#EBF2E3" style="border:1px solid #8cb13d">Nome cient&iacute;fico</td>
				<td width="200" align="center" valign="top" bgColor="#EBF2E3" style="border:1px solid #8cb13d">Nome comum</td>
			</tr>
		</thead>
		<tbody>
		<g:if test="${listFichas.size() == 0 }">
			<tr>
				<td colspan="4"><h3>Nenhuma ficha encontrada</h3></td>
			</tr>
		</g:if>
		<g:else>
			<g:each var="item" in="${ listFichas.sort{ it?.noCientificoItalico } }" status="i">
				<tr>
					<g:set var="nomesComuns" value="${item?.nomesComuns}"></g:set>
					<td width="50" align="center" valign="top" style="border:1px solid #8cb13d;">${(i+1)}</td>
					<td width="300" align="left"  valign="top" style="border:1px solid #8cb13d;">${item?.grupo?.descricao}</td>
					<td width="200" align="left"  valign="top" style="border:1px solid #8cb13d;">${raw(item?.noCientificoItalico)}</td>
					<td width="200" align="left"  valign="top" style="border:1px solid #8cb13d;">${raw(( nomesComuns ? nomesComuns.split(',')[0]:'&nbsp;'))}</td>
				</tr>
			</g:each>
		</g:else>
		</tbody>
	</table>
</div>
