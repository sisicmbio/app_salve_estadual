<g:set var="rndId"  value="${Math.abs(new Random().nextInt() % 10000) + 1}"/>
<div id="divLista-${rndId}" style="padding:5px;display: flex; flex-direction: column;border:1px solid silver;">
    <div style="max-height:300px;overflow:auto;">
         <ol id="ol-${rndId}">
            <g:each var="item" in="${lista}">
                <li class="nowrap">
                    <g:if test="${canModify}">
                        <div>
                            <label title="marcar/desmarcar para exclusão" style="margin:0px;padding:0px">
                                <input type="checkbox" class="checkbox-lg" value="${item.id}" data-rnd-id="${rndId}"
                                       data-sq-ficha="${item?.sqFicha}" data-action="tableDeleteItemCheck">${ raw( item.descricao ) }
                            </label>
                        </div>
                    </g:if>
                    <g:else>
                        ${raw( item.descricao )}
                    </g:else>
                </li>
            </g:each>
        </ol>
    </div>
    <g:if test="${canModify}">
        <div id="divListaFooter-${rndId}" style="display:none;border-top:1px solid silver;margin-top:5px;padding-top: 5px;">
            <button title="Excluir selecionadas" data-id="${id}" data-row-num="${rowNum}" data-cb="${callback}" data-action="tableDeleteItemExecute" data-rnd-id="${rndId}" class="btn btn-danger btn-xs pull-left">Excluir</button>
            <button title="Marcar/desmarcar todas" data-id="${id}" data-row-num="${rowNum}" data-action="tableDeleteItemCheckAll" data-rnd-id="${rndId}" class="btn btn-primary btn-xs pull-right" style="border-radius: 2px;">Todos</button>
        </div>
    </g:if>

</div>
