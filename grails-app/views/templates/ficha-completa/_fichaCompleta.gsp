<!-- view: /views/template/ficha-completa/_fichaCompleta.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<div class="hidden">Action:${params?.action}&nbsp;Controller:${params?.controller}&nbsp;</div>
<div id="container-ficha-colaboracoes" data-opened="N" class="container-fluid" style="font-size:1em;">
    <input id="sqFicha" value="${ficha?.id}" type="hidden"/>    
    <input id="sqCicloConsultaFicha" value="${cicloConsultaFicha?.id}" type="hidden"/>

    <g:if test="${contexto=='validador'}">
        <div class="row">
            <div class="col-sm-12">
                <fieldset class="mt10">
                    <legend class="topo" style="font-size:2.5em;">
                        ${raw(ficha.noCientificoItalico)}
                    </legend>
                </fieldset>

                <%-- arvore taxonomica da espécie selecionada --%>
                <div id="divBreadcrumb">
                    <ol class="breadcrumb" style=" background-color:#FFFFFF;">
                        <g:each in="${ficha.taxon.hierarchy}" var="nome" status="i">
                            <li id="breadcrumb_nivel_${i + 1}" data-value="${nome}" class="breadcrumb-item">${raw(nome)}</li>
                        </g:each>
                    </ol>
                </div>
            </div>
        </div>
    </g:if>

    <%--  Taxonomia --%>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-success" id="panel-taxonomia">
                <div class="panel-heading"><a class="cursor-pointer" data-toggle="collapse" data-target="#panel-taxonomia-body">Classificação Taxonômica</a>                                           
                    <button class="btn btn-default btn-xs pull-right open-close-all" title="Abrir/Fechar todos os grupos." onClick="openCloseAllPanels('container-ficha-colaboracoes')"><i class="fa fa-folder-open"></i></button>
                </div>
                
                <div class="panel-body panel-collapse collapse" id="panel-taxonomia-body">
                    <div id="ficha-tab-taxonomia" class="taxonomia-nivel">
                        <div class="nivel">Filo:</div><div class="nome">${taxonStructure.nmFilo}</div><br/>
                        <div class="nivel">Classe:</div><div class="nome">${taxonStructure.nmClasse}</div><br/>
                        <div class="nivel">Ordem:</div><div class="nome">${taxonStructure.nmOrdem}</div><br/>
                        <div class="nivel">Família:</div><div class="nome">${taxonStructure.nmFamilia}</div><br/>
                    </div>
                    <g:renderPanelColaboracoes campoFicha="nomesComuns" campoColaboracao="nomesComuns" rotulo="Nomes Comuns" ficha="${ficha}" cicloConsultaFicha="${cicloConsultaFicha}" listaSituacao="${listSituacaoColaboracao}"/>
                    <g:renderPanelColaboracoes campoFicha="sinonimiasHtml" campoColaboracao="sinonimias" rotulo="Sinonímias" ficha="${ficha}" cicloConsultaFicha="${cicloConsultaFicha}" listaSituacao="${listSituacaoColaboracao}"/>
                    <g:renderPanelColaboracoes campoFicha="dsNotasTaxonomicas" campoColaboracao="dsNotasTaxonomicas" rotulo="Notas Taxonômicas" ficha="${ficha}" cicloConsultaFicha="${cicloConsultaFicha}" listaSituacao="${listSituacaoColaboracao}" edit="true"/>
                    <g:renderPanelColaboracoes campoFicha="dsDiagnosticoMorfologico" campoColaboracao="dsDiagnosticoMorfologico" rotulo="Notas Morfológicas" ficha="${ficha}" cicloConsultaFicha="${cicloConsultaFicha}" listaSituacao="${listSituacaoColaboracao}" edit="true"/>
                    </div>
            </div>
        </div>
    </div>

        <%--  Distribuição Geográfica --%>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-success" id="panel-distribuicao-geografica">
                <div class="panel-heading"><a class="cursor-pointer" data-toggle="collapse" data-target="#panel-distribuicao-geografica-body">Distribuição Geográfica</a></div>
                <div class="panel-body panel-collapse collapse" id="panel-distribuicao-geografica-body">
                    <div class="div-question">Endêmica do Brasil?</div><div class="div-answer">${ficha.endemicaBrasil}</div><br/>
                    <g:renderPanelColaboracoes campoFicha="dsDistribuicaoGeoGlobal"   campoColaboracao="dsDistribuicaoGeoGlobal"   rotulo="Distribuição Global"   ficha="${ficha}" cicloConsultaFicha="${cicloConsultaFicha}" listaSituacao="${listSituacaoColaboracao}" edit="true"/>
                    <g:renderPanelColaboracoes campoFicha="dsDistribuicaoGeoNacional" campoColaboracao="dsDistribuicaoGeoNacional" rotulo="Distribuição Nacional" ficha="${ficha}" cicloConsultaFicha="${cicloConsultaFicha}" listaSituacao="${listSituacaoColaboracao}" edit="true"/>

                    <%-- Imagem Principal de Mapa Distribuição --%>
                    <div class="panel panel-success panel-colaboracao mt10" id="panel-imagem-mapa-distribuicao" data-url-imagem="ficha/showImagem?sqFicha=${ficha.id}&tipo=mapa-distribuicao">
                        <div class="panel-heading"><a class="cursor-pointer" data-toggle="collapse" data-target="#panel-imagem-mapa-distribuicao-body">Imagem Principal do Mapa de Distribuição</a></div>
                        <div class="panel-body text-center panel-collapse collapse" id="panel-imagem-mapa-distribuicao-body">
                            <img class="img-responsive img-rounded img-thumbnail" width="99%" height="auto" src="" alt="Mapa Distribuição"/>
                        </div>
                    </div>

                    <%-- Mapa da ocorrências --%>
                    <div class="panel panel-success panel-colaboracao mt10" id="panel-mapa-ocorrencias" data-mapa="S" data-sq-ficha="${ficha.id}" data-sq-ciclo-consulta-ficha="${cicloConsultaFicha?.id}">
                        <div class="panel-heading"><a class="cursor-pointer" data-toggle="collapse" data-target="#panel-mapa-ocorrencias-body">Mapa Registros de Ocorrência</a></div>
                        <div class="panel-body panel-collapse collapse" id="panel-mapa-ocorrencias-body">
                            <div id="div-mapa-ocorrencias-${ficha?.id}" data-loaded="N" class="map" style="width: 800px;height: 600px"></div>
                            <div id="div-gride-ocorrencias-${ficha?.id}" data-loaded="N" class="gride" style="width: 100%;height: auto"></div>
                        </div>
                    </div>

                    <%-- Estados --%>
                    <g:renderPanelColaboracoes campoFicha="estados" campoColaboracao="estados" rotulo="Estados" ficha="${ficha}" cicloConsultaFicha="${cicloConsultaFicha}" listaSituacao="${listSituacaoColaboracao}"/>

                    <%-- Biomas --%>
                    <g:renderPanelColaboracoes campoFicha="biomas" campoColaboracao="biomas" rotulo="Biomas" ficha="${ficha}" cicloConsultaFicha="${cicloConsultaFicha}" listaSituacao="${listSituacaoColaboracao}"/>

                </div>
            </div>
        </div>
    </div>

    <%--  História Natural --%>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-success" id="panel-historia-natural" data-habito-alimentar="S" data-sq-ciclo-consulta-ficha="${cicloConsultaFicha?.id}">
                <div class="panel-heading"><a class="cursor-pointer" data-toggle="collapse" data-target="#panel-historia-natural-body">História Natural</a></div>
                <div class="panel-body panel-collapse collapse" id="panel-historia-natural-body">
                    <div class="div-question">Espécie é Migratória?</div><div class="div-answer">${ficha.especieMigratoria}</div><br/>

                    <g:renderPanelColaboracoes campoFicha="dsHistoriaNatural" campoColaboracao="dsHistoriaNatural" rotulo="..." ficha="${ficha}" cicloConsultaFicha="${cicloConsultaFicha}" listaSituacao="${listSituacaoColaboracao}" edit="true"/>

                    <div class="panel panel-success panel-colaboracao mt10"id="panel-habito-alimentar" data-habito-alimentar="S" data-sq-ficha="${ficha.id}" data-sq-ciclo-consulta-ficha="${cicloConsultaFicha?.id}">
                        <div class="panel-heading"><a class="cursor-pointer" data-toggle="collapse" data-target="#panel-habito-alimentar-body">Hábito Alimentar</a></div>
                        <div class="panel-body panel-collapse collapse" id="panel-habito-alimentar-body">
                            <div id="div-gride-habito-alimentar-${ficha?.id}"></div>
                        </div>
                    </div>

                    <div class="panel panel-success panel-colaboracao mt10"id="panel-habitat" data-habitat="S" data-sq-ciclo-consulta-ficha="${cicloConsultaFicha?.id}" data-sq-ficha="${ficha.id}">
                        <div class="panel-heading"><a class="cursor-pointer" data-toggle="collapse" data-target="#panel-habitat-body">Habitat</a></div>
                        <div class="panel-body panel-collapse collapse" id="panel-habitat-body">
                            <div class="div-question">Restrito a habitat primário?</div><div class="div-answer">${ficha.snd(ficha.stRestritoHabitatPrimario)}</div><br/>
                            <div class="div-question">Especialista em micro habitat?</div><div class="div-answer">${ficha.snd(ficha.stEspecialistaMicroHabitat)}</div><br/>
                            <div class="div-question">${ficha.dsEspecialistaMicroHabitat}</div>
                            <div id="div-gride-habitat-${ficha?.id}" class="mt10"></div>
                            <g:renderPanelColaboracoes campoFicha="dsUsoHabitat" campoColaboracao="dsUsoHabitat" rotulo="Observações Sobre o Habitat" ficha="${ficha}" cicloConsultaFicha="${cicloConsultaFicha}" listaSituacao="${listSituacaoColaboracao}" edit="true"/>
                        </div>
                    </div>


                    <div class="panel panel-success panel-colaboracao mt10"id="panel-interacao" data-interacao="S" data-sq-ficha="${ficha.id}" data-sq-ciclo-consulta-ficha="${cicloConsultaFicha?.id}">
                        <div class="panel-heading"><a class="cursor-pointer" data-toggle="collapse" data-target="#panel-interacao-body">Interação com Outras Espécies</a></div>
                        <div class="panel-body panel-collapse collapse" id="panel-interacao-body">
                            <div id="div-gride-interacao-${ficha?.id}"></div>
                            <g:renderPanelColaboracoes campoFicha="dsInteracao" campoColaboracao="dsInteracao" rotulo="..." ficha="${ficha}" cicloConsultaFicha="${cicloConsultaFicha}" listaSituacao="${listSituacaoColaboracao}" edit="true"/>
                        </div>
                    </div>

                    <div class="panel panel-success panel-colaboracao mt10"id="panel-reproducao" data-sq-ciclo-consulta-ficha="${cicloConsultaFicha?.id}">
                        <div class="panel-heading"><a class="cursor-pointer" data-toggle="collapse" data-target="#panel-reproducao-body">Reprodução</a></div>
                        <div class="panel-body panel-collapse collapse" id="panel-reproducao-body" style="max-width:350px;">
                            <div class="div-question">Intervalo de Nascimento:</div><div class="div-answer">${ficha.intervaloNascimentoText}</div><br/>
                            <div class="div-question">Tempo de gestação:</div><div class="div-answer">${ficha.tempoGestacaoText}</div><br/>
                            <div class="div-question">Tamanho da prole:</div><div class="div-answer">${ficha.tamanhoProleText}</div><br/>

                            <table class="table table-condensed table-sm table-hover table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Campo</th>
                                    <th>Macho</th>
                                    <th>Fêmea</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Maturidade Sexual</td>
                                    <td>${ficha.vlMaturidadeSexualMacho ? ficha.vlMaturidadeSexualMacho.toString() + ' ' + ficha.unidMaturidadeSexualMacho?.descricao : ''}</td>
                                    <td>${ficha.vlPesoFemea ? ficha.vlPesoFemea.toString() + ' ' + ficha.unidadePesoFemea?.descricao : ''}</td>
                                </tr>
                                <tr>
                                    <td>Peso</td>
                                    <td>${ficha.vlPesoMacho ? ficha.vlPesoMacho.toString() + ' ' + ficha.unidadePesoMacho?.descricao : ''}</td>
                                    <td>${ficha.vlPesoFemea ? ficha.vlPesoFemea.toString() + ' ' + ficha.unidadePesoFemea?.descricao : ''}</td>
                                </tr>
                                <tr>
                                    <td>Comprimento</td>
                                    <td>${ficha.vlComprimentoMacho ? ficha.vlComprimentoMacho.toString() + ' ' + ficha.medidaComprimentoMacho?.descricao : ''}</td>
                                    <td>${ficha.vlComprimentoFemea ? ficha.vlComprimentoFemea.toString() + ' ' + ficha.medidaComprimentoFemea?.descricao : ''}</td>
                                </tr>
                                <tr>
                                    <td>Senilidade reprodutiva</td>
                                    <td>${ficha.vlSenilidadeReprodutivaMacho ? ficha.vlSenilidadeReprodutivaMacho.toString() + ' ' + ficha.unidadeSenilidRepMacho?.descricao : ''}</td>
                                    <td>${ficha.vlSenilidadeReprodutivaFemea ? ficha.vlSenilidadeReprodutivaFemea.toString() + ' ' + ficha.unidadeSenilidRepFemea?.descricao : ''}</td>
                                </tr>
                                <tr>
                                    <td>Longevidade</td>
                                    <td>${ficha.vlLongevidadeMacho ? ficha.vlLongevidadeMacho.toString() + ' ' + ficha.unidadeLongevidadeMacho?.descricao : ''}</td>
                                    <td>${ficha.vlLongevidadeFemea ? ficha.vlLongevidadeFemea.toString() + ' ' + ficha.unidadeLongevidadeFemea?.descricao : ''}</td>
                                </tr>
                                </tbody>
                            </table>
                            <g:renderPanelColaboracoes campoFicha="dsReproducao" campoColaboracao="dsReproducao" rotulo="..." ficha="${ficha}" cicloConsultaFicha="${cicloConsultaFicha}" listaSituacao="${listSituacaoColaboracao}" edit="true"/>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div> %{-- FIM HISTORIA NATURAL--}%

    <%-- População --%>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-success" id="panel-populacao"  data-sq-ciclo-consulta-ficha="${cicloConsultaFicha?.id}">
                <div class="panel-heading"><a class="cursor-pointer" data-toggle="collapse" data-target="#panel-populacao-body">População</a></div>
                <div class="panel-body panel-collapse collapse" id="panel-populacao-body">
                    <div class="div-question">Tendência Populacional:</div><div class="div-answer">${ficha?.tendenciaPopulacional?.descricao}</div><br/>
                    <g:renderPanelColaboracoes campoFicha="dsPopulacao" campoColaboracao="dsPopulacao" rotulo="Observações Sobre a População" ficha="${ficha}" cicloConsultaFicha="${cicloConsultaFicha}" listaSituacao="${listSituacaoColaboracao}" edit="true"/>
                    <g:renderPanelColaboracoes campoFicha="dsCaracteristicaGenetica" campoColaboracao="dsCaracteristicaGenetica" rotulo="Características Genéticas" ficha="${ficha}" cicloConsultaFicha="${cicloConsultaFicha}" listaSituacao="${listSituacaoColaboracao}" edit="true"/>
                </div>
            </div>
        </div>
    </div> %{-- FIM POPULAÇÃO --}%

    <%-- Ameaças --%>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-success panel-grupo" id="panel-ameaca"  data-ameaca="S" data-sq-ficha="${ficha.id}" data-sq-ciclo-consulta-ficha="${cicloConsultaFicha?.id}">
                <div class="panel-heading"><a class="cursor-pointer" data-toggle="collapse" data-target="#panel-ameaca-body">Ameaça</a></div>
                <div class="panel-body panel-collapse collapse" id="panel-ameaca-body">
                    <div id="div-gride-ameaca-${ficha?.id}"></div>
                    <g:renderPanelColaboracoes campoFicha="dsAmeaca" campoColaboracao="dsAmeaca" rotulo="..." ficha="${ficha}" cicloConsultaFicha="${cicloConsultaFicha}" listaSituacao="${listSituacaoColaboracao}" edit="true" opened="true"/>
                </div>
            </div>
        </div>
    </div> %{-- FIM AMEAÇAS --}%


    <%-- Uso --%>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-success panel-grupo" id="panel-uso"  data-uso="S" data-sq-ficha="${ficha.id}" data-sq-ciclo-consulta-ficha="${cicloConsultaFicha?.id}">
                <div class="panel-heading"><a class="cursor-pointer" data-toggle="collapse" data-target="#panel-uso-body">Uso</a></div>
                <div class="panel-body panel-collapse collapse" id="panel-uso-body">
                    <div id="div-gride-uso-${ficha?.id}"></div>
                    <g:renderPanelColaboracoes campoFicha="dsUso" campoColaboracao="dsUso" rotulo="..." ficha="${ficha}" cicloConsultaFicha="${cicloConsultaFicha}" listaSituacao="${listSituacaoColaboracao}" edit="true" opened="true"/>
                </div>
            </div>
        </div>
    </div> %{-- FIM USO --}%


    <%-- Conservação --%>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-success panel-grupo" id="panel-conservacao" data-sq-ficha="${ficha.id}" data-conservacao="S" data-sq-ciclo-consulta-ficha="${cicloConsultaFicha?.id}">
                <div class="panel-heading"><a class="cursor-pointer" data-toggle="collapse" data-target="#panel-conservacao-body">Conservação</a></div>
                <div class="panel-body panel-collapse collapse" id="panel-conservacao-body">
                    <fieldset id="fieldset-ultima-avaliacao-nacional-${ficha?.id}">
                        <label>
                            Última Avaliação Nacional
                        </label>
                        <br/>
                        <div class="div-question" id="nao-avaliada" class="hidden">Não Avaliada</div>
                        <div id="avaliada">
                            <div class="div-question">Ano</div><div class="div-answer" id="ano"></div><br/>
                            <div class="div-question">Categoria</div><div class="div-answer" id="categoria"></div><br/>
                            <div class="div-question">Critério</div><div class="div-answer" id="criterio"></div><br/>
                            <div class="div-question">Justificativa</div><div class="div-answer" id="justificativa"></div>
                        </div>
                    </fieldset>

                    <fieldset id="fieldset-historico-avaliacoes-${ficha?.id}">
                        <label>
                            Histórico das Avaliações
                        </label>
                        <div id="gride-historico"></div>
                    </fieldset>

                    <div class="div-question">Presença em Lista Nacional Oficial de Espécies Ameaçadas de Extinção?&nbsp;</div><div class="div-answer"> ${ficha.snd( ficha.stPresencaListaVigente)}</div>

                    <g:renderPanelColaboracoes campoFicha="dsAcaoConservacao" campoColaboracao="dsAcaoConservacao" rotulo="Ações de Conservação" ficha="${ficha}" cicloConsultaFicha="${cicloConsultaFicha}" listaSituacao="${listSituacaoColaboracao}" edit="true"/>

                    <div id="div-outros-grides-${ficha?.id}">
                        <fieldset>
                            <label>
                                Presença em Convenção
                            </label>
                            <div id="gride-convencoes"></div>
                        </fieldset>

                        <fieldset>
                            <label>Ações de Conservação</label>
                            <div id="gride-acoes"></div>
                        </fieldset>

                        <fieldset>
                            <label>Presenca em UC</label>
                            <div id="gride-uc"></div>
                        </fieldset>

                        <fieldset>
                            <label>Situação Regional</label>
                            <div id="divGridHistoricoAvaliacaoRegional"></div>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </div> %{-- FIM CONSERVACAO --}%

    <%-- Pesquisas --%>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-success panel-grupo" id="panel-pesquisa" data-pesquisa="S" data-sq-ficha="${ficha.id}" data-sq-ciclo-consulta-ficha="${cicloConsultaFicha?.id}">
                <div class="panel-heading"><a class="cursor-pointer" data-toggle="collapse" data-target="#panel-pesquisa-body">Pesquisa</a></div>
                <div class="panel-body panel-collapse collapse" id="panel-pesquisa-body">
                    <g:renderPanelColaboracoes campoFicha="dsPesquisaExistNecessaria" campoColaboracao="dsPesquisaExistNecessaria" rotulo="..." ficha="${ficha}" cicloConsultaFicha="${cicloConsultaFicha}" listaSituacao="${listSituacaoColaboracao}" edit="true" opened="true"/>
                    <div id="gride-pesquisa"></div>
                </div>
            </div>
        </div>
    </div> %{-- FIM PESQUISA --}%

<%-- Referências Bibliográficas --%>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-success panel-grupo" id="panel-ref-bib" data-ref-bib="S" data-sq-ficha="${ficha.id}" data-sq-ciclo-consulta-ficha="${cicloConsultaFicha?.id}">
                <div class="panel-heading"><a class="cursor-pointer" data-toggle="collapse" data-target="#panel-ref-bib-body">Referências Bibliográficas</a></div>
                <div class="panel-body panel-collapse collapse" id="panel-ref-bib-body">
                    <div id="gride-ref-bib"></div>
                </div>
            </div>
        </div>
    </div> %{-- FIM REF BIBLIOGRÁFICA --}%

    <g:if test="${cicloConsultaFicha}">
        <%--Encerramento da Ficha --%>
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-success panel-grupo" id="panel-fim" data-sq-ficha="${ficha.id}" data-sq-ciclo-consulta-ficha="${cicloConsultaFicha?.id}">
                    <div class="panel-heading"><a class="cursor-pointer red" data-toggle="collapse" data-target="#panel-fim-body">Consolidar Avaliação</a></div>
                    <div class="panel-body panel-collapse collapse in" id="panel-fim-body">
                        <a class="btn btn-primary" href="#" data-action="gerenciarConsulta.finalizarAvaliacao" data-params="sqCicloAvaliacao:${cicloConsultaFicha?.id}">Consolidar</a>
                    </div>
                </div>
            </div>
        </div> %{-- FIM ENCERRAMENTO FICHA --}%
    </g:if>

</div> %{-- FIM CONTAINER--}%
