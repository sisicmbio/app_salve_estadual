<!-- view: /views/templates/_filtrosFicha-->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<g:set var="rndId" value="${Math.abs(new Random().nextInt() % 10000) + 1}"/>
%{--Contexto:${params?.contexto}--}%
<fieldset>
    <legend style="font-size:1.2em;">
        <a title="Aplicar filtro(s) no gride de fichas" data-toggle="collapse" href="#divFiltrosFicha${rndId}">
            <i class="glyphicon glyphicon-filter"/>&nbsp;${raw(label ?: 'Filtrar ficha')}
        </a>
    </legend>

    %{-- inicio accordion --}%

    <div id="divFiltrosFicha${rndId}"
         class="panel-collapse collapse ${isCollapsed == null || isCollapsed == true ? '' : 'in'}" role="tabpanel"
         style="background-color:#d3e0c6;padding:5px;">

    %{-- nome da unidade --}%
        <g:if test="${!session.sicae.user.sqUnidadeOrg}">
            <input type="hidden" class="fld-controle" id="isExterno${rndId}" name="isExternoFiltro" value="S"/>
        </g:if>
        <g:elseif test="${!session.sicae.user.isADM() && !session.sicae.user.isCO()}">
            <div class="fld form-group">
                <label class="control-label">Unidade:&nbsp;<b class="blue">${session.sicae.user.noUnidadeOrg}</b>
                </label>
                <input type="hidden" id="sqUnidadeFiltro${rndId}" class="fld-controle" name="sqUnidadeFiltro"
                       value="${session.sicae.user.sqUnidadeOrg}">
            </div>
            <br>
        </g:elseif>
        <g:else>
        %{-- quando for ADM permitir filtro pela unidade --}%
            <g:if test="${session.sicae.user.isADM() || session.sicae.user.isCO()}">
                <br>

                <div class="fld form-group">
                    <label for="sqUnidadeFiltro${rndId}" class="control-label">Unidade responsável</label>
                    <br>
                    <select id="sqUnidadeFiltro${rndId}" name="sqUnidadeFiltro" class="form-control select2 fld500"
                            data-s2-minimum-input-length="0"
                            data-s2-select-on-close="false"
                            data-s2-placeholder="-- todas --"
                            data-s2-onchange="app.btnFiltroFichaClick"
                            data-s2-params="sqCicloAvaliacao"
                            data-s2-url="ficha/select2InstituicaoFiltro">
                    </select>
                </div>
            </g:if>
        </g:else>

%{--    ESTADO--}%

    <g:if test="${params?.contexto && params?.contexto == 'ficha'}">
            <div class="form-group">
                <label for="sqEstadoFiltro${rndId}" class="control-label"
                       title="Filtrar fichas que ocorre no Estado selecionado ou em pelo menos um dos selecionados.">Estado onde ocorre</label>
                <br/>
                <select name="sqEstadoFiltro" data-all-label="-- todos --"
                        id="sqEstadoFiltro${rndId}"
                        data-xchange="app.btnFiltroFichaClick"
                        data-multiple="true"
                        class="form-control fld fld350 fldSelect">
                %{--<option value="">-- todos-- </option>--}%
                    <g:each var="item" in="${listEstados}">
                        <option data-codigo="${item.id}"
                                value="${item.id}">${item.descricao}</option>
                    </g:each>
                </select>
            </div>
    </g:if>

    %{--  EXIBIR MÓDULO PÚBLICO--}%
    <g:if test="${session.sicae.user.isADM() && (params?.contexto =~ 'ficha|publicacao')}">
        <div class="form-group">
            <label for="stExibirModuloPublicoFiltro" class="control-label">Exibidas no módulo público?</label>
            <br>
            <select name="stExibirModuloPublicoFiltro" id="stExibirModuloPublicoFiltro" class="form-control fld250 fldSelect" placeholder="Todas">
                <option value="" selected="selected">-- todas --</option>
                <option value="S">Sim</option>
                <option value="N">Não</option>
            </select>
        </div>
    </g:if>



    <g:if test="${excluirFiltro?.nomeCientifico == null || excluirFiltro.nomeCientifico == false}">
        <br>
        <div class="fld form-group">
            <label for="nmCientificoFiltro${rndId}" class="control-label"
               title="${params.contexto != 'ficha' ?
                       'Informe o nome científico completo ou em parte para selecionar as fichas. Ex: Puma, Puma concolor ou concolor apenas.' :
                       'Informe o nome científico completo ou em parte para selecionar as fichas. Ex: Puma, Puma concolor ou concolor apenas.<br>Tambem pode ser informada uma lista de espécies separadas por vírgula, neste caso, devem ser informados os nomes completos. Ex: Puma concolor, Aburria cujubi'}">Nome científico</label>
            <br>
            <input type="text" id="nmCientificoFiltro${rndId}" name="nmCientificoFiltro" class="fld form-control fld300"
                   data-enter="app.btnFiltroFichaClick" )>
            <br>
        </div>
    </g:if>
    <g:else>
        <br>
    </g:else>

        <div class="fld form-group">
            <label for="nmComumFiltro${rndId}" class="control-label"
                   title="Informe o nome comum ou em parte para selecionar as fichas. Ex: Macaco-aranha, Gambazinho">Nome comum</label>
            <br>
            <input type="text" id="nmComumFiltro${rndId}" name="nmComumFiltro" class="fld form-control fld200"
                   data-enter="app.btnFiltroFichaClick" )>
            <br>
        </div>

        %{-- FILTRO PELO NIVEL TAXONOMICO --}%

        <div id="divFiltroTaxon" class="form-group">
            <label for="nivelFiltro${rndId}" class="control-label">Nível taxonômico</label>
            <br>
            <select id="nivelFiltro${rndId}" name="nivelFiltro" data-change="app.filtroNivelTaxonomicoChange"
                    data-params="rndId:${rndId},callback:app.btnFiltroFichaClick"
                    class="fld form-control fld200 fldSelect">
                <option value="">-- todos --</option>
                <g:each var="item" in="${listNivelTaxonomico}">
                %{--<option value="${item.coNivelTaxonomico}">${item.deNivelTaxonomico}</option>--}%
                    <option data-codigo="${item.coNivelTaxonomico}"
                            value="${item.id}">${item.deNivelTaxonomico}</option>
                </g:each>
            </select>
        </div>

        <div class="fld form-group" style="display: inline-block">
            <label for="sqTaxonFiltro${rndId}" class="control-label"
                   title="Para selecionar mais de uma opção, pressione a tecla CTRL e clique nos itens da lista.">Taxon</label>
            <br>
            <select id="sqTaxonFiltro${rndId}" name="sqTaxonFiltro"
                    data-s2-visible="false"
                    class="fld form-control select2 fld250"
                    data-s2-params="nivelFiltro${rndId}|sqNivel"
                    data-s2-onchange="app.btnFiltroFichaClick"
                    data-s2-url="ficha/select2TaxonNovo"
                    data-s2-placeholder="?"
                    data-s2-multiple="true"
                    data-s2-maximum-selection-length="20"
                    data-s2-minimum-input-length="2"
                    data-s2-auto-height="true">
            </select>
        </div>

        <br>

        <g:if test="${excluirFiltro?.pendencia == null || excluirFiltro.pendencia == false}">
            <div class="fld form-group">
                <label for="inPendenciaFiltro${rndId}" class="control-label">Pendências</label>
                <br>
                <select id="inPendenciaFiltro${rndId}" name="inPendenciaFiltro" data-change="app.btnFiltroFichaClick"
                        class="fld form-control fldSelect fld200">
                    <option value="">-- todas --</option>
                    <option value="2">Com Pendência</option>
                    <option value="1">Sem Pendência</option>
                </select>
            </div>
            <g:if test="${params?.contexto && params?.contexto == 'publicacao'}">
                <g:if test="${excluirFiltro?.revisada == null || excluirFiltro.revisada == false}">
                    <div class="fld form-group">
                        <label for="inRevisadasFiltro${rndId}" class="control-label">Revisadas</label>
                        <br>
                        <select id="inRevisadasFiltro${rndId}" name="inRevisadasFiltro" data-change="app.btnFiltroFichaClick"
                                class="fld form-control fldSelect fld200">
                            <option value="">-- todas --</option>
                            <option value="S">Sim</option>
                            <option value="N">Não</option>
                        </select>
                    </div>
                </g:if>
            </g:if>
       </g:if>

        <g:if test="${listSituacaoFicha}">
            <div class="fld form-group">
                <label for="sqSituacaoFiltro${rndId}" class="control-label">Situação
                    <g:if test="${ !( params?.contexto =~ /consulta/)}">
                        <span style="margin-left:50px;display:inline-block">Incluir fichas excluídas&nbsp;<input
                            type="checkbox" id="chkExcluidasFiltro${rndId}" name="chkExcluidasFiltro" value="S"
                            class="fld form-contro checkbox-lg">
                            <i title="Por padrão, as fichas em situação EXCLUÍDA não são apresentadas no grid de resultado. Se quiser incluí-las, marque o check box." class="label-help glyphicon glyphicon-question-sign blue tooltipstered"></i>
                        </span>
                    </g:if>
                </label>
                <br>
                <select id="sqSituacaoFiltro${rndId}"
                        name="sqSituacaoFiltro"
                        data-all-label="-- todas  (exceto excluídas) --"
                        data-rnd-id="${rndId}"
                        data-change="app.btnFiltroFichaClick"
                        class="fld form-control fldSelect fld350"
                        data-multiple="true">
                    <g:each var="item" in="${listSituacaoFicha}">
                        <g:if test="${params?.contexto == 'oficina' && item.codigo == 'COMPILACAO'}">
                            <option data-codigo-sistema="${item.codigo}" value="${item.id}">Transferida</option>
                        </g:if>
                        <g:else>
                            <option data-codigo-sistema="${item.codigo}" value="${item.id}">${item.descricao}</option>
                        </g:else>
                    </g:each>
                </select>
            </div>
        </g:if>

        <br>
        <g:if test="${listCategorias}">
            <g:if test="${excluirFiltro?.categoria == null && !excluirFiltro?.categoria}">

                <div class="form-group">
                    <label for="sqCategoriaFiltro${rndId}" title="Categoria do histórico. ( Aba 7 )" class="control-label">Categoria vigente </label>
                    <br>
                    <select id="sqCategoriaFiltro${rndId}"
                            name="sqCategoriaFiltro"
                            data-change="app.btnFiltroFichaClick"
                            data-multiple="true"
                            class="form-control fldSelect fld500">
                        <g:each var="item" in="${listCategorias}">
                            <g:if test="${!(item.codigo ==~ /NE|AMEACADA|PEX/)}">
                                <option data-codigo-sistema="${item.codigo}"
                                        value="${item.id}">${item.descricaoCompleta}</option>
                            </g:if>
                        </g:each>
                    </select>
                </div>

                %{-- Pex --}%
                <div class="form-group hide" style="margin-left: -20px;" id="fldStPexFiltro${rndId}">
                    <label class="control-label">PEX</label>
                    <br>
                    <select id="stPexFiltro${rndId}"
                            name="stPexFiltro"
                            class="fld form-control fldSelect">
                        <option value="">todas</option>
                        <option value="S">Sim</option>
                        <option value="N">Não</option>
                    </select>
                </div>

            </g:if>

            <g:if test="${excluirFiltro?.categoriaAvaliacao == null && !excluirFiltro?.categoriaAvaliacao}">
                <div class="form-group">
                    <label for="sqCategoriaAvaliacaoFiltro${rndId}" title="Selecione para filtrar pela categoria do resultado validado ( Aba 11.2 )" class="control-label">
                        <span class="mr20">Categoria da avaliação em andamento.</span><span class="cursor-pointer">Resultado validado?</span>&nbsp;
                        <input type="checkbox" id="chkCategoriaValidadaFiltro${rndId}" name="chkCategoriaValidadaFiltro" value="S"
                        class="fld checkbox-lg" style="margin-right: 0 !important;">
                    </label>
                    <br>
                    <select id="sqCategoriaAvaliacaoFiltro${rndId}"
                            name="sqCategoriaAvaliacaoFiltro"
                            data-change="app.btnFiltroFichaClick"
                            data-multiple="true"
                            class="form-control fldSelect fld500">
                        <g:each var="item" in="${listCategorias}">
                            <g:if test="${!(item.codigo ==~ /NE|AMEACADA|PEX/)}">
                                <option data-codigo-sistema="${item.codigo}"
                                        value="${item.id}">${item.descricaoCompleta}</option>
                            </g:if>
                        </g:each>
                    </select>
                </div>
                %{-- Pex --}%
                <div class="form-group hide" style="margin-left: -20px;" id="fldStPexAvaliacaoFiltro${rndId}">
                        <label class="control-label">PEX</label>
                        <br>
                        <select id="stPexAvaliacaoFiltro${rndId}"
                                name="stPexAvaliacaoFiltro"
                                class="fld form-control fldSelect">
                            <option value="">todas</option>
                            <option value="S">Sim</option>
                            <option value="N">Não</option>
                        </select>
                </div>
            </g:if>



            %{--
            <div class="form-group">
                <label for="coCategoriaFiltro${rndId}" class="control-label">
                    <g:if test="${ params?.container != 'tabConsolidar'}">
                        <span title="Corresponde à categoria validada mais recente. (aba 11.7 da ficha)">Categoria.<i class="label-help glyphicon glyphicon-question-sign blue tooltipstered"></i></span>
                        <g:if test="${params.contexto != 'publicacao'}">
                            <span style="margin-left:50px;display:inline-block">Resultado da avaliação neste ciclo&nbsp;<input
                                type="checkbox" id="chkAvaliadaNoCicloFiltro${rndId}" name="chkAvaliadaNoCicloFiltro" value="S"
                                class="fld checkbox-lg">
                                <i title="Marque esta opção para que o filtro considere o resultado da oficina de avaliação deste ciclo" class="label-help glyphicon glyphicon-question-sign blue tooltipstered"></i>
                            </span>
                        </g:if>
                    </g:if>
                    <g:else>
                        <span>Categoria vigente</span>
                    </g:else>
                </label>
                <span id="fldStPexFiltro${rndId}" class="hide">
                    <label class="control-label">&nbsp;&nbsp;/&nbsp;&nbsp;PEX</label>
                    <select id="stPexFiltro${rndId}"
                            name="stPexFiltro"
                            class="fld form-control fldSelect">
                        <option value="">todos</option>
                        <option value="S">Sim</option>
                        <option value="N">Não</option>
                    </select>
                </span>

                <br>
                <select id="sqCategoriaFiltro${rndId}"
                        name="sqCategoriaFiltro"
                        data-change="app.btnFiltroFichaClick"
                        data-multiple="true"
                        class="form-control fldSelect" style="width:453px;min-width:453px">
                    <g:each var="item" in="${listCategorias}">
                        <g:if test="${!(item.codigo ==~ /NE|AMEACADA|PEX/)}">
                            <option data-codigo-sistema="${item.codigo}"
                                    value="${item.id}">${item.descricaoCompleta}</option>
                        </g:if>
                    </g:each>
                </select>
            </div>
            --}%

            <br>
        </g:if>

        <g:if test="${listGrupos}">
            <div class="form-group">
                <label for="sqGrupoFiltro${rndId}" class="control-label">Grupo(s) avaliado(s)</label>
                <br/>
                <select name="sqGrupoFiltro" data-all-label="-- todos --"
                        id="sqGrupoFiltro${rndId}"
                        data-change="app.btnFiltroFichaClick"
                        data-multiple="true"
                        class="form-control fld fld350 fldSelect">
                %{--<option value="">-- todos-- </option>--}%
                    <g:each var="item" in="${listGrupos}">
                        <option data-codigo="${item.id}"
                                value="${item.id}" ${item.id == ficha?.grupo?.id ? "selected" : ''}>${item.descricao}</option>
                    </g:each>
                </select>
            </div>

            <g:if test="${listSubgrupos}">
                <div class="form-group">
                    <label for="sqSubGrupoFiltro${rndId}" class="control-label">Subgrupo(s)</label>
                    <br/>
                    <select name="sqSubgrupoFiltro" data-all-label="-- todos --" disabled="true"
                            id="sqSubgrupoFiltro${rndId}"
                            data-multiple="true"
                            class="form-control fld fld350 fldSelect">
                    %{--<option value="">-- todos-- </option>--}%
                        <g:each var="item" in="${listSubgrupos}">
                            <option title="${item?.pai?.descricao}" data-codigo="${item.id}"
                                    data-sq-grupo="${item?.pai?.id}"
                                    value="${item.id}" ${item.id == ficha?.subgrupo?.id ? "selected" : ''}>${item.descricao}</option>
                        </g:each>

                    </select>
                </div>
            </g:if>
        </g:if>

        <g:if test="${ !excluirFiltro?.envioIucn && params?.contexto && params?.contexto == 'publicacao'}">
            <br>
                <div class="fld form-group">
                    <label for="inEnvioIucnFiltro${rndId}" class="control-label">Envio IUCN</label>
                    <br>
                    <select id="inEnvioIucnFiltro${rndId}" name="inEnvioIucnFiltro"
                            data-change="app.btnFiltroFichaClick"
                            class="fld form-control fldSelect fld200">
                        <option value="">-- todas --</option>
                        <option value="S">Enviadas</option>
                        <option value="N">Não enviadas</option>
                    </select>
                </div>
                <div class="fld form-group">
                    <label for="inComSemDOI${rndId}" class="control-label">DOI</label>
                    <br>
                    <select id="inComSemDOI${rndId}" name="inComSemDOIFiltro"
                            data-change="app.btnFiltroFichaClick"
                            class="fld form-control fldSelect fld200">
                        <option value="">-- todas --</option>
                        <option value="S">Com DOI</option>
                        <option value="N">Sem DOI</option>
                    </select>
                </div>

        </g:if>


    %{--    TRADUTORES/REVISORES - INTEGRACAO SIS/IUCN --}%
    <g:if test="${listTradutores}">
        <br>
        <div class="form-group">
            <label for="sqTradutorFiltro${rndId}" class="control-label">Revisado por</label>
            <br/>
            <select name="sqTradutorFiltro" data-all-label="-- todos --"
                    id="sqTradutorFiltro${rndId}"
                    data-multiple="true"
                    class="form-control fld fld350 fldSelect">
%{--                    <option value="">-- todos-- </option>--}%
                <g:each var="item" in="${listTradutores}">
                    <option  value="${item.sq_pessoa}">${item.no_pessoa}</option>
                </g:each>
            </select>
        </div>
        <br>
        <div class="form-group">
            <label for="inAndamentoTraducao${rndId}" class="control-label">Andamento da tradução</label>
            <br>
            <select name="inAndamentoTraducaoFiltro" data-all-label="-- todos --"
                    id="inAndamentoTraducaoFiltro${rndId}"
                    class="form-control fld fld350 fldSelect">
                    <option value="">-- todos -- </option>
                    <option value="TRADUCAO_NAO_INICIADA">Tradução NÃO iniciada</option>
                    <option value="TRADUCAO_INICIADA">Tradução iniciada</option>
                    <option value="TRADUCAO_FINALIZADA">Tradução finalizada</option>
            </select>
        </div>
        <div class="form-group">
            <label for="inAndamentoRevisao${rndId}" class="control-label">Andamento da revisão da tradução</label>
            <br/>
            <select name="inAndamentoRevisaoFiltro" data-all-label="-- todos --"
                    id="inAndamentoRevisaoFiltro${rndId}"
                    class="form-control fld fld350 fldSelect">
                    <option value="">-- todos -- </option>
                    <option value="REVISAO_NAO_INICIADA">Revisão NÃO iniciada</option>
                    <option value="REVISAO_INICIADA">Revisão iniciada</option>
                    <option value="REVISAO_FINALIZADA">Revisão finalizada</option>
            </select>
        </div>
    </g:if>
    <g:else>
        <g:if test="${excluirFiltro?.andamentoRevisao == null || excluirFiltro.andamentoRevisao == false}">
            <div class="form-group">
                <label for="inAndamentoRevisao${rndId}" class="control-label">Andamento da revisao da tradução</label>
                <br/>
                <select name="inAndamentoRevisaoFiltro" data-all-label="-- todos --"
                        id="inAndamentoRevisaoFiltro${rndId}"
                        class="form-control fld fld350 fldSelect">
                    <option value="">-- todos -- </option>
                    <option value="REVISAO_NAO_INICIADA">Revisão NÃO iniciada</option>
                    <option value="REVISAO_INICIADA">Revisão iniciada</option>
                    <option value="REVISAO_FINALIZADA">Revisão finalizada</option>
                </select>
            </div>
        </g:if>
    </g:else>


%{-- MOSTRAR FILTRO RECORTE MODULO PUBLICO SOMENTE NO MODULO FICHA --}%
        <g:if test="${params?.contexto && params?.contexto == 'ficha'}">
            <g:if test="${listGruposSalve && session?.sicae?.user?.isADM()}">
                <div class="form-group">
                    <label for="sqGrupoSalveFiltro${rndId}" class="control-label">Recorte(s) módulo público</label>
                    <br/>
                    <select name="sqGrupoSalveFiltro" data-all-label="-- todos --"
                            id="sqGrupoSalveFiltro${rndId}"
                            data-change="app.btnFiltroFichaClick"
                            data-multiple="true"
                            class="form-control fld fld350 fldSelect">
                    %{--<option value="">-- todos-- </option>--}%
                        <option value="-1" title="Filtrar as fichas que o recorte módulo público não está preenchido">-- não preenchidos --</option>
                        <g:each var="item" in="${listGruposSalve.sort{it.descricao}}">
                            <option data-codigo="${item.id}"
                                    value="${item.id}" ${item.id == ficha?.grupo?.id ? "selected" : ''}>${item.descricao}</option>
                        </g:each>
                    </select>
                </div>
            </g:if>

        <br>
        %{--    FILTRO POR OFICINA--}%
        %{--<g:if test="${listOficinas}">
            <div class="form-group">
                <label for="sqOficinaFiltro${rndId}" class="control-label">Oficina</label>
                <br>
                <select id="sqOficinaFiltro${rndId}" name="sqOficinaFiltro" data-change="app.btnFiltroFichaClick"
                        class="fld form-control fld400 fldSelect">
                    <option value="">-- todas --</option>
                    <g:each var="item" in="${listOficinas}">
                        <option value="${item.id}">${item.sgOficina + ' de ' + item.periodo}</option>
                    </g:each>
                </select>
            </div>
        </g:if>--}%
            <g:if test="${listOficinas}">
                <div class="form-group">
                    <label for="sqOficinaMultiFiltro${rndId}" class="control-label">Oficina</label>
                    <br/>
                    <select name="sqOficinaMultiFiltro" data-all-label="-- todas --"
                            id="sqOficinaMultiFiltro${rndId}"
                            data-Xchange="app.btnFiltroFichaClick"
                            data-multiple="true"
                            class="form-control fld fld700 fldSelect">
                    %{--<option value="">-- todos-- </option>--}%
                        <g:each var="item" in="${listOficinas}">
                            <option title="${item.sgOficina}" value="${item.id}">${br.gov.icmbio.Util.capitalize(item.noOficina) + ' de ' + item.periodo}</option>
                        </g:each>
                    </select>
                </div>
            </g:if>
            <br>
            %{-- MULTIMIDIA --}%
            <div class="form-group">
                <label for="stImagemPrincipalFiltro" class="control-label"
                       title="Selecionar as fichas com ou sem a imagem principal da espécie cadastrada">Foto</label>
                <br>
                <select name="stImagemPrincipalFiltro" id="stImagemPrincipalFiltro" class="form-control fldSelect"
                        style="width:245px;" placeholder="Todas">
                    <option value="" selected="selected">-- todos --</option>
                    <option value="COM_IMAGEM">Com foto (principal)</option>
                    <option value="SEM_IMAGEM">Sem foto (principal)</option>
                </select>
            </div>

        %{-- MAPA DE DISTRIBUIÇÃO --}%
            <div class="form-group">
                <label for="stMapaDistribuicaoFiltro"
                       title="Selecionar as fichas com ou sem a imagem principal do mapa de distribuição cadastrada"
                       class="control-label">Mapa distribuição</label>
                <br>
                <select name="stMapaDistribuicaoFiltro" id="stMapaDistribuicaoFiltro" class="form-control fldSelect"
                        style="width:250px;" placeholder="Todas">
                    <option value="" selected="selected">-- todos --</option>
                    <option value="COM_IMAGEM">Com mapa (principal)</option>
                    <option value="SEM_IMAGEM">Sem mapa (principal)</option>
                </select>
            </div>


        </g:if>

        <g:if test="${params?.contexto && params?.contexto =~ /ficha|consulta/}">
            <div class="form-group">
                <label for="stListaOficialFiltro${rndId}" class="control-label">Presença na Lista Nacional Oficial Vigente?</label>
                <br>
                <select name="stListaOficialFiltro" id="stListaOficialFiltro${rndId}" class="form-control fldSelect fld300" placeholder="Todas">
                    <option value="" selected="selected">-- todas --</option>
                    <option value="S">Sim</option>
                    <option value="N">Não</option>
                </select>
            </div>
        </g:if>


        %{-- MIGRATÓRIA --}%
        %{-- DESATIVADO
        <div class="form-group">
            <label for="stMigratoriaFiltro" class="control-label">Espécie migratória</label>
            <br>
            <select name="stMigratoriaFiltro" id="stMigratoriaFiltro" class="form-control fld250 fldSelect">
                <option value="">-- todas --</option>
                <option value="S">Sim</option>
                <option value="N">Não</option>
                <option value="D">Desconhecido</option>
            </select>
        </div>
        --}%


%{-- quando for ADM permitir filtro pela unidade --}%
    <g:if test="${session.sicae.user.isADM() && params.contexto=='ficha'}">
        <br>
        <div class="fld form-group">
            <label for="dsPalavraChaveFiltro${rndId}" class="control-label">Pesquisar nos campos abertos da aba</label>
            <br>
            <select id="nuAbaFiltro${rndId}" name="nuAbaFiltro" class="form-control fld500"
                data-change="app.filtroNuAbaChange" data-params="rndId:${rndId}">
                <option value="" selected="true"></option>
                <option value="1">1-Taxonomia</option>
                <option value="2">2-Distribuição</option>
                <option value="3">3-História natural</option>
                <option value="4">4-População</option>
                <option value="5">5-Ameaças</option>
                <option value="6">6-Usos</option>
                <option value="7">7-Conservação</option>
                <option value="8">8-Pesquisa</option>
                <option value="11">11-Avaliação</option>
            </select>
            <div style="display:inline-block; visibility: hidden;" id="divDsPalavraChaveFiltro${rndId}">
            <span>&nbsp;a palavra:&nbsp;</span>
            <input type="text" class="form-control fld300"
                   value="" id="dsPalavraChaveFiltro${rndId}"
                   name="dsPalavraChaveFiltro">
            </div>
        </div>
    </g:if>


%{-- FILTRAR FICHAS QUE FORAM INCLUIDAS NO CICLO ATUAL--}%
        <g:if test="${params?.contexto && params?.contexto == 'ficha'}">
            <br>
            <div class="form-group" style="display: inline-block;border-bottom:1px solid silver;margin-top:21px;" id="divIncluidasCiclo">
                 <label for="chkIncluidasCicloFiltro${rndId}" title="Filtrar as espécies que foram cadastradas no ciclo selecionado ou que não tiveram resultado no ciclo anterior." class="control-label cursor-pointer">Espécies incluídas no ciclo selecionado
                    <input name="chkIncluidasCicloFiltro" id="chkIncluidasCicloFiltro${rndId}" value="S" type="checkbox" class="fld checkbox-lg tooltipstered" style="margin-right: 0 !important;">
                </label>
            </div>
            %{--<div class="form-group" style="display: inline-block;border-bottom:1px solid silver;margin-top:21px;" id="divInexistenteProxmoCiclo">
                <label for="chkInexistenteProximoCicloFiltro" title="Filtrar as fichas que ainda não forma cadastradas/copiadas para o próximo ciclo." class="control-label cursor-pointer">Espécies não cadastradas ciclo posterior
                    <input name="chkInexistenteProximoCicloFiltro" id="chkInexistenteProximoCicloFiltro" value="S" type="checkbox" class="fld checkbox-lg tooltipstered">
                </label>
            </div>--}%
        </g:if>

    %{-- FILTRAR PELA CONSULTA AMPLA / DIRETA--}%
        <g:if test="${listConsultas}">
            <div class="form-group">
                <label for="sqCicloConsultaFiltro${rndId}" class="control-label">Consulta Ampla/Direta</label>
                <br>
                <select id="sqCicloConsultaFiltro${rndId}" name="sqCicloConsultaFiltro"
                        data-change="app.btnFiltroFihaClick" class="form-control fld300 fldSelect">
                    <option value="">-- todas --</option>
                    <g:each var="item" in="${listConsultas}">
                        <option data-codigo="${item.id}"
                                value="${item.id}">${item.deTituloConsulta} - ${item.periodoText}</option>
                    </g:each>
                </select>
            </div>
        </g:if>


        <g:if test="${outrosCampos}">
            <g:render template="${outrosCampos}"></g:render>
        </g:if>


        <g:if test="${listMostrarFiltros?.comSemPapel}">
            <div class="fld form-group">
                <label for="inComSemPFFiltro${rndId}" class="control-label">Com/Sem Ponto Focal</label>
                <br>
                <select id="inComSemPFFiltro${rndId}" name="inComSemPFFiltro" data-change="app.btnFiltroFichaClick"
                        class="fld form-control fldSelect fld200">
                    <option value="">-- todas --</option>
                    <option value="COM">Com Ponto Focal</option>
                    <option value="SEM">Sem Ponto Focal</option>
                </select>
            </div>
        </g:if>

        <g:if test="${listMostrarFiltros?.comSemPapel}">
            <div class="fld form-group">
                <label for="inComSemCTFiltro${rndId}" class="control-label">Com/Sem Coordenador de Taxon</label>
                <br>
                <select id="inComSemCTFiltro${rndId}" name="inComSemCTFiltro" data-change="app.btnFiltroFichaClick"
                        class="fld form-control fldSelect fld200">
                    <option value="">-- todas --</option>
                    <option value="COM">Com Coordenador de Taxon</option>
                    <option value="SEM">Sem Coordenador de Taxon</option>
                </select>
            </div>
        </g:if>
        <br>


    <div class="fld form-group mt10">
            <a data-action="storeFilters(${rndId},'${callback}');app.updateGridFichas(${rndId},'${callback}',${isRequired == true ? true : false})"
               id="btnFiltroFicha${rndId}" class="btn btn-primary btn-xs"
               title="Informe o filtro desejado em um ou mais campos acima e clique aqui para aplicá-lo!">Aplicar filtro</a>
            <a data-action="app.clearFilters" id="btnFiltroFichaClear${rndId}"
               data-rnd-id="${rndId}"
               data-grid-id="${gridId?:''}"
               class="btn btn-danger btn-xs" title="Limpar todos os filtros!">Limpar filtros</a>
            %{--            <a data-action="restoreFilters(${rndId},'${callback}');" id="btnFiltroFichaRestore${rndId}" data-rnd-id="${rndId}"  class="btn btn-default btn-xs cursor-pointer" title="Restaurar último filtro aplicado!" style="display:none"><i class="fa fa-recycle"></i></a>--}%
            <span id="spanFiltrosFichaTipFiltrar${rndId}" class="label label-success tip"
                  style="margin-left:20px;display:none;color:#000 !important;border-radius:5px;"><i
                class="fa fa-lightbulb-o" aria-hidden="true"></i>&nbsp;clique no botão "Aplicar filtro".</span>
        </div>
    </div>
</fieldset>
<script>
    var getInputs = function (rndId) {
        return; //recurso desabilitado devido a erros entre os forms de filtros.
        return $("#divFiltrosFicha" + rndId).find('select[name*=Filtro],input[type=text][name*=Filtro],input[type=checkbox][name*=Filtro]:checked');
    };
    var storeFilters = function (rndId, contexto) {
        return; //recurso desabilitado devido a erros entre os forms de filtros.
        var data = {};
        var hasFilter = false;
        try {
            getInputs(rndId).each(function (i, ele) {
                var $ele = $(ele);
                var name = ele.name;
                var value = $ele.val();

                if (!hasFilter && value) {
                    hasFilter = true;
                }
                try {
                    if ($ele.hasClass('select2')) {
                        var text = [];
                        if (value) {
                            try {
                                $ele.select2('data').forEach(function (item) {
                                    text.push(item.descricao)
                                });
                            } catch (e) {
                            }
                        }
                        if (typeof value == 'string') {
                            data[name] = {value: value, text: text[0]};
                        } else {
                            data[name] = {value: value, text: text};
                        }
                    } else if (ele.tagName == 'SELECT') {
                        // SELECT MULTIPLE
                        if ($ele.data('multiple')) {
                            data[name] = {value: value}
                        } else {
                            // SELECT NORMAL
                            data[name] = {value: value}
                        }
                    } else if (ele.tagName == 'INPUT') {
                        // INPUT TEXT
                        if (ele.type == 'checkbox') {
                            data[name] = {value: true}
                        } else {
                            data[name] = {value: value}
                        }
                    }
                } catch (e) {
                }
            });
            if (hasFilter) {
                contexto = contexto.replace(/[^a-zA-Z]/gi, '').toLowerCase();
                app.lsSet(contexto, JSON.stringify(data));
                $("#btnFiltroFichaRestore" + rndId).show();
            }
        } catch (e) {
        }
    };
    var restoreFilters = function (rndId, contexto) {
        return; //recurso desabilitado devido a erros entre os forms de filtros.
        var contexto = contexto.replace(/[^a-zA-Z]/gi, '').toLowerCase();
        var data = app.lsGet(contexto);
        if (data) {
            try {
                data = JSON.parse(data);
                for (var name in data) {
                    var value = data[name].value;
                    var $ele = $("#" + name + rndId);
                    var modified = false;
                    if ($ele.size() == 1) {
                        if ($ele.hasClass('select2')) {
                            var text = data[name].text;
                            var dataAtual = $ele.select2('data');
                            if (text) {
                                if (typeof text == 'string') {
                                    // validar se o item ja existe
                                    if (dataAtual.filter(function (o) {
                                        return o.id == value;
                                    }).length == 0) {
                                        var newOption;
                                        newOption = new Option(text, value, false, false);
                                        $ele.append(newOption);
                                        modified = true;
                                    }
                                } else {
                                    text.forEach(function (item, index) {
                                        // validar se o item ja existe
                                        if (dataAtual.filter(function (o) {
                                            return o.id == value[index];
                                        }).length == 0) {
                                            var newOption;
                                            newOption = new Option(text[index], value[index], false, false);
                                            $ele.append(newOption);
                                            modified = true;
                                        }
                                    });
                                }
                                if (modified) {
                                    $ele.val(value);
                                    $ele.trigger('change');
                                }
                            }
                        } else if ($ele[0].tagName == 'SELECT') {
                            if ($ele.data('multiple')) {
                                $ele.multiselect('select', value);
                                $ele.multiselect('refresh');
                            } else {
                                $ele.val(value);
                                $ele.change();
                            }
                        } else if ($ele[0].tagName == 'INPUT') {
                            // INPUT TEXT
                            if ($ele[0].type == 'checkbox') {
                                $ele.prop('checked', true);
                            } else {
                                $ele.val(value);
                            }
                        }
                    }
                }
            } catch (e) {
            }
        } else {
            app.alert('Nenhum filtro foi salvo.');
        }
    };

    var restoreFiltersInit = function (rndId, contexto) {
        return; //recurso desabilitado devido a erros entre os forms de filtros.
        var btn = $("#btnFiltroFichaRestore" + rndId);
        btn.hide();
        /* não está funcionando, tem que consertar esse recurso
        var data = app.lsGet( contexto );
        if ( data ) {
            try {
                data = JSON.parse(data);
                var existFilter=false;
                for( key in data) {
                    if( !existFilter && data[key].value )
                    {
                        existFilter=true;
                    }
                }
                if( existFilter )
                {
                    btn.show('slow');
                }
            } catch (e) {}
        }
        */
    };
    //window.setTimeout(function() { restoreFiltersInit( ${rndId}, "${ params?.contexto}"); },300);
</script>
