<!-- view: /templates/_formChat.gsp/ -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<fieldset id="fieldSetChat${sqficha}" class="${classeCss}">
    <legend style="font-size:17px !important;">
        Bate-papo entre ponto focal, validadores e coordenadores de táxon<i title="Atualizar conversa!" data-sq-ficha="${sqFicha}" data-sq-oficina-ficha="${sqOficinaFicha}" class="ml10 glyphicon glyphicon-refresh cursor-pointer tooltipstered" style="cursor: pointer;" data-action="refreshChat"></i>
    </legend>
     <div id="divContainerChat${sqFicha}">
        <div class="chat"
             style="padding:3px;border:1px solid lightslategray;min-height: 248px;max-height:248px;overflow-y: auto;"
             id="divGridChat${sqFicha}" data-scrollbottom="true"></div>
        <g:if test="${params.canModify && !fichaVersionada }">
               <form class="form form-inline" id="frmChat">
                    <div class="form-group" style="width: 100%">
                        <label for="txChat${sqFicha}" class="control-label">Mensagem</label>
                        <br>
                        <textarea name="txChat" id="txChat${sqFicha}" class="form-control fldTextarea bold" rows="2" style="width: 100%;min-height:75px;font-size:16px;" required="true"></textarea>
                    </div>
                    <div class="panel-footer">
                        <button type="button" id="btnSaveChat" data-action="saveChat" data-contexto="${contexto}" data-sq-ficha="${sqFicha}" data-sq-oficina-ficha="${sqOficinaFicha}" data-sq-validador-ficha="${validadorFicha?.id}" class="btn btn-primary">Enviar&nbsp;<i style="color:#fff;" class="glyphicon glyphicon-send"></i></button>

                        <button type="button"
                                id="btnConvidarTodos"
                                data-action="chamarAtencaoTodosChat"
                                data-contexto="${contexto}"
                                data-sq-ficha="${sqFicha}"
                                data-sq-oficina-ficha="${sqOficinaFicha}"
                                class="ml20 pull-right btn btn-default tooltipster"
                                title="Enviar email para os participantes da validação convidando para o bate-papo.">
                            <span>Comunicar Todos</span>&nbsp;<i style="color:#ffaf04;" class="glyphicon glyphicon-envelope"></i></button>


%{--                        <g:if test="${ contexto == 'validador' }">
                            <button type="button" id="btnChamarAtencao"  data-action="chamarAtencaoChat" data-contexto="${contexto}" data-sq-ficha="${sqFicha}" data-sq-oficina-ficha="${sqOficinaFicha}" data-sq-validador-ficha="${validadorFicha?.id}" class="btn btn-warning" title="Utilize este botão para enviar um e-mail para o outro validador solicitando sua contribuição no bate-papo!">Avisar&nbsp;<i style="color:#fff;" class="glyphicon glyphicon-user"></i></button>
                            <button type="button" id="btnConvidarCTChat" data-action="convidar" data-contexto="${contexto}" data-sq-ficha="${sqFicha}" data-sq-oficina-ficha="${sqOficinaFicha}" data-sq-validador-ficha="${validadorFicha?.id}" data-codigo="CT" class="pull-right btn btn-default tooltipster" title="Enviar email para Coordeanador de Táxon convidando para participar do bate-papo."><span>Convidar CT</span>&nbsp;<i style="color:blue" class="glyphicon glyphicon-user"></i></button>
                        </g:if>
                        <g:else>
                            <button type="button"
                                    id="btnConvidarTodos"
                                    data-action="chamarAtencaoTodosChat"
                                    data-contexto="${contexto}"
                                    data-sq-ficha="${sqFicha}"
                                    data-sq-oficina-ficha="${sqOficinaFicha}"
                                    class="ml20 pull-right btn btn-default tooltipster"
                                    title="Enviar email para os participantes da validação convidando para o bate-papo.">
                                    <span>Comunicar Todos</span>&nbsp;<i style="color:#ffaf04;" class="glyphicon glyphicon-user"></i></button>
                        </g:else>--}%
                        %{--<button type="button" id="btnConvidarPFChat" data-action="convidar" data-contexto="${contexto}" data-sq-ficha="${sqFicha}" data-sq-oficina-ficha="${sqOficinaFicha}" data-sq-validador-ficha="${validadorFicha?.id}" data-codigo="PF" class="ml20 pull-right btn btn-default tooltipster" title="Enviar email para Ponto Focal para participar do bate-papo."><span>Comunicar PF</span>&nbsp;<i style="color:#ffaf04;" class="glyphicon glyphicon-user"></i></button>--}%
                    </div>
                </form>
        </g:if>
     </div>
</fieldset>
