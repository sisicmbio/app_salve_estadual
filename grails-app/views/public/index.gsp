<%@ page contentType="text/html;charset=UTF-8" %>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="keywords" content="Sistema SALVE-PÚBLICO, ICMBio, biodiversidade" />
    <meta name="description" content="Módulo de consulta pública ao banco de dados do Sistema de Avaliação do Risco de Extinção da Biodiversidade - SALVE" />
    <meta name="generator" content="grails - Open Source Framework" />
    <link rel="icon" href="${assetPath(src: 'favicon.ico')}" type="image/png">
    <title>SALVE público</title>

    <!-- Global site tag (gtag.js) - Google Analytics - LOCALHOST
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-166657017-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-166657017-1'); // desenv
        gtag('config', 'UA-23267823-5'); // production

    </script>-->
    <!-- Global site tag (gtag.js) - Google Analytics -->

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-W2T27KW');</script>
    <!-- End Google Tag Manager -->

    <!-- css -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">
    <link rel="stylesheet" href="/salve-estadual/assets/public/jquery.fancybox.min.css">
    <link rel="stylesheet" href="/salve-estadual/assets/public/lightbox2/css/lightbox.min.css"/>

    <link rel="stylesheet" href="/salve-estadual/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/salve-estadual/assets/public/bootstrap.min.css">
    <link rel="stylesheet" href="/salve-estadual/assets/public/datatables/dataTables.bootstrap4.min.css">

    <link rel="stylesheet" href="/salve-estadual/assets/public/animate.min.css">
    <link rel="stylesheet" href="/salve-estadual/assets/public/odometer-theme-default.css">
    <link rel="stylesheet" href="/salve-estadual/assets/public/select2.min.css"/>
    <link rel="stylesheet" href="/salve-estadual/assets/public/switches.css"/>
    <!-- openlayers -->
    <link rel="stylesheet" href="/salve-estadual/assets/public/openlayers/ol.css"/>
    <link rel="stylesheet" href="/salve-estadual/assets/public/openlayers/ol3-contextmenu.min.css"/>
    <link rel="stylesheet" href="/salve-estadual/assets/public/openlayers/ol-geocoder.min.css"/>
%{--    <link href="https://cdn.jsdelivr.net/npm/ol-geocoder@latest/dist/ol-geocoder.min.css" rel="stylesheet">--}%


    <!-- css do infinite carousel para as imagens das especies ameaçadas -->
    <link rel="stylesheet" href="/salve-estadual/assets/public/infinite-carousel/style.css"/>

    <link rel="stylesheet" href="/salve-estadual/assets/public/main.css"/>
    <link rel="stylesheet" href="/salve-estadual/assets/public/fichaAjax.css"/>

    <!-- javascripts -->
    <script defer src="/salve-estadual/assets/public/jquery-3.4.1.min.js"></script>
    <script defer src="/salve-estadual/assets/public/popper.min.js"></script>
    <script defer src="/salve-estadual/assets/public/bootstrap.min.js"></script>
    <script defer src="/salve-estadual/assets/public/lodash.min.js"></script>
    <script defer src="/salve-estadual/assets/public/jquery.blockUI.js"></script>
    <script defer src="/salve-estadual/assets/public/axios.min.js"></script>
    <script defer src="/salve-estadual/assets/public/openlayers/ol465.js"></script>
    <script defer src="/salve-estadual/assets/public/openlayers/geocoder/ol-geocoder.js"></script>
%{--    <script src="https://cdn.jsdelivr.net/npm/ol-geocoder"></script>--}%

    <script defer src="/salve-estadual/assets/public/openlayers/ol3mapPublic.js"></script>
    %{--    <script defer src="/salve-estadual/assets/public/sweetalert2@9.js"></script>--}%
    <script defer src="/salve-estadual/assets/public/sweetalert2.all.js"></script>
    <script defer src="/salve-estadual/assets/public/vue.js"></script>
    <script defer src="/salve-estadual/assets/public/datatables/jquery.dataTables.min.js"></script>
    <script defer src="/salve-estadual/assets/public/datatables/dataTables.bootstrap4.min.js"></script>

    <script defer src="/salve-estadual/assets/public/odometer.min.js"></script>
    <script defer src="/salve-estadual/assets/public/select2.min.js"></script>
    <script defer src="/salve-estadual/assets/public/select2-pt-BR.js"></script>
    <script defer src="/salve-estadual/assets/public/jquery.toaster.js"></script>
    <script defer src="/salve-estadual/assets/public/vuejs-paginate.js"></script>
    <script defer src="/salve-estadual/assets/public/jquery.bgswitcher.js"></script>
    <script defer src="/salve-estadual/assets/public/animatescroll.min.js"></script>
    <script defer src="/salve-estadual/assets/public/jquery.fancybox.min.js"></script>
    <script defer src="/salve-estadual/assets/public/lightbox2/js/lightbox.min.js"></script>
    <script defer src="/salve-estadual/assets/public/jquery.floatThead.min.js"></script>


    <script defer type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    %{--
        <script defer src="/salve-estadual/assets/public/graph/RGraph.common.core.js"></script>
        <script defer src="/salve-estadual/assets/public/graph/RGraph.pie.js"></script>

    --}%

    <!-- componentes vue-->
    <script defer src="/salve-estadual/assets/public/components/select2taxon.js"></script>
    <script defer src="/salve-estadual/assets/public/components/switches.js"></script>
    <script defer src="/salve-estadual/assets/public/components/select2multi.js"></script>
    <script defer src="/salve-estadual/assets/public/components/select2multiAjax.js"></script>
    <script defer src="/salve-estadual/assets/public/components/paginate.js"></script>
    <script defer src="/salve-estadual/assets/public/components/salveTableSearch.js"></script>
    <script defer src="/salve-estadual/assets/public/components/salveTableParticipantes.js"></script>


    <!-- script do infinite carousel para as imagens das especies ameaçadas -->
    %{--    https://www.jqueryscript.net/demo/Generic-Content-Slideshow-Carousel-Plugin-with-jQuery-jUsualSlider/--}%
    <script defer src="/salve-estadual/assets/public/infinite-carousel/main.js"></script>

    <!-- script da aplicação -->
    <script defer src="/salve-estadual/assets/public/main.js"></script>

    <!-- recaptcha google -->
%{--    <script src="https://www.google.com/recaptcha/api.js" async defer></script>--}%
    <script src="https://www.google.com/recaptcha/api.js?render=${captchaPublicKey}" async defer></script>

</head>

<body onload="inicializar()">
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W2T27KW" height="0" width="0" style="display:none;visibility:hidden"> </iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<div class="app-loading"
     style="display:flex; flex-direction: column; justify-content: center;
     align-items: center;
     position:absolute;top:0;left:0;z-index: 90000;
     background-color: rgb(255,255,255);
     background-image: url(/salve-estadual/assets/public/logotipo_icmbio_full.png);
     background-repeat: no-repeat;
     background-attachment: fixed;
     background-position: center;
     background-size: contain;
     width: 100%;height: 100vh;">
    <div>
        <h3 style="font-size:1.5rem;color:#024902;"><i class="fa-4x fa fa-spinner fa-spin"></i></h3>
    </div>
    %{--<div>
        <h1 style="font-size:2.8rem;">Bem vindo ao SALVE - público</h1>
    </div>
    <div>
        <h3 style="font-size:1.5rem;">Inicializando. Aguarde...<i class="fa fa-spinner fa-spin"></i></h3>
    </div>--}%
</div>

<div class="container-fluid" id="appVue">
    %{--   MENU NAVBAR TOPO--}%
    <nav id="navbarMain" class="navbar navbar-dark navbar-expand-lg fixed-top text-left">
        <div class="container-fluid">
            <a class="navbar-brand" href=";"
               @click.prevent="doScroll('#inicio')">
            </a>

            <button data-toggle="collapse" class="navbar-toggler" data-target="#navcol-1"><span
                    class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>

            <div class="collapse navbar-collapse" id="navcol-1" style="margin-right: 50px;">
                <ul class="nav navbar-nav m-auto">

                    <li class="nav-item active" role="presentation" title="Página inicial">
                        <a class="nav-link" href=";"
                           @click.prevent="doScroll('#inicio')">
                            <i class="home-icon fa fa-home"></i>
                        </a>
                    </li>

                    <li class="nav-item" role="presentation"><a class="nav-link"
                                                                @click.prevent="doScroll('#salveNumeros')"
                                                                href=";">Avaliação em<br>números</a></li>

                    <li class="nav-item" role="presentation"><a class="nav-link"
                                                                @click.prevent="doScroll('#especiesAmeacadas')"
                                                                href=";">Espécies em categoria<br>de ameaça</a></li>

                    <li class="nav-item" role="presentation"><a class="nav-link"
                                                                @click.prevent="doScroll('#comoFazAvaliacao')"
                                                                href=";">Como é feita a<br>Avaliação</a></li>

                    <li class="nav-item" role="presentation"><a class="nav-link"
                                                                @click.prevent="doScroll('#livroVermelho')"
                                                                href=";">Livro<br>Vermelho</a></li>

                    <li class="nav-item" role="presentation"><a class="nav-link"
                                                                @click.prevent="doScroll('#quemSomos')"
                                                                href=";">Quem<br>somos</a></li>

                    <li class="nav-item" role="presentation"><a class="nav-link"
                                                                @click.prevent="doScroll('#faleConosco')"
                                                                href=";">Fale<br>conosco</a>
                    </li>

                    <li class="nav-item" role="presentation"><a class="nav-link"
                                                                @click.prevent="doScroll('#sobreSalve')"
                                                                href=";">Sobre o<br>SALVE</a>
                    </li>

                    <li class="nav-item" role="presentation">
                        <a class="nav-link" target="_blank"
                           href="https://salve.icmbio.gov.br/salve-consulta/">Consultas<br>abertas</a>
                    </li>



                </ul>

                <section style="position: absolute;right:15px" class="d-none d-lg-block">
                    <a target="_blank" href="https://www.icmbio.gov.br">
                        <img class="logo-icmbio" src="/salve-estadual/assets/public/logotipo_icmbio.png">
                    </a>
                </section>
            </div>
        </div>
    </nav>

    <!-- section inicio -->

    <div id="inicio" class="section d-flex flex-column justify-content-center align-items-center">
        %{-- IMAGEM SALVE --}%
        <div v-show="layout>0" class="card bg-transparent">
            <div class="card-body text-center">
                <img class="img-fluid logomarca-salve" src="/salve-estadual/assets/public/logomarca_salve_estadual_grande.png">
                <h1 class="card-title title">Risco de Extinção da Fauna Brasileira</h1>
            </div>
        </div>

        <div id="cardSearch" class="card bg-transparent w-100">
            <div class="card-body text-left text-light">
                <h4 class="card-title d-none">Pesquisar</h4>

                <div class="input-group input-group-lg ">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-search"></i></span>
                    </div>
                    <input class="form-control" type="text"
                           id="inputSearch" autofocus="" autocomplete="on"
                           placeholder="Busca por nome científico ou nome comum..."
                           @keyup.enter.prevent="doSearch"
                           v-model="searchValue" maxlength="50">

                    <div class="input-group-append">
                        <button class="btn btn-primary" id="btnSearch" type="button" title="Pesquisar <Enter>"
                                @click.prevent="doSearch"><i class="fa fa-filter mr-2"></i><span
                                class="d-none d-lg-inline">Pesquisar</span></button>
                        <button class="btn btn-secondary" type="button" title="Limpar"
                                @click.prevent="doClearSearch"><i class="fa fa-trash"></i></button>
                    </div>
                </div>

                <div v-if="!advancedSearchVisible"
                     class="input-group w-100 d-flex justify-content-end">
                    <a href=";" title="Exibir mais opções de pesquisa."
                       class="btn btn-link link link-advanced-search"
                       @click.prevent="advancedSearchVisible = !advancedSearchVisible">
                        <i class="fa fa-search-plus"></i>
                        <span>Pesquisa avançada</span>
                    </a>
                </div>

                <div v-if="advancedSearchVisible" id="divAdvancedSearch" class="card w-100 mt-2">
                    <div class="card-header"><i @click.prevent="advancedSearchVisible=false"
                                                class="fa fa-times float-right"></i><h4>Pesquisa avançada</h4></div>

                    <div class="card-body text-left text-li">
                            %{-- CHECK FICHAS PUBLICADAS --}%
                            <div class="row">
                                <div class="form-group col-12">
                                    <switches label="Fichas publicadas"
                                              v-model="searchByPublicadas"
                                              :type-bold="false"
                                              theme="bootstrap"
                                              color="success"/>

                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-6">
                                    <switches label="Grupo taxonômico"
                                              v-model="searchByGrupoTaxonomico"
                                              :type-bold="false"
                                              theme="bootstrap"
                                              color="success"/>

                                </div>

                                <div class="form-group col-6">
                                    <switches label="Região"
                                              v-model="searchByRegiao"
                                              :type-bold="false"
                                              theme="bootstrap"
                                              color="success"/>
                                    %{--<input @click="searchByRegiao = !searchByRegiao" class="form-check-input" type="checkbox" id="searchRegiao">
                                    <label class="form-check-label" for="searchRegiao">
                                        Região
                                    </label>--}%
                                </div>
                            </div>

                            %{-- CHECK CATEGORIA --}%
                            <div class="row">
                                <div class="form-group col-6">
                                    <switches label="Categoria"
                                              v-model="searchByCategoria"
                                              :type-bold="false"
                                              theme="bootstrap"
                                              color="success"/>
                                    %{--<input @click="searchByCategoria = !searchByCategoria"
                                           class="form-check-input" type="checkbox" id="searchByCategoria">
                                    <label class="form-check-label" for="searchByCategoria">
                                        Categoria
                                    </label>--}%
                                </div>

                                %{-- CHECK ESTADO --}%
                                <div class="form-group col-6">
                                    <switches label="Estado"
                                              v-model="searchByEstado"
                                              :type-bold="false"
                                              theme="bootstrap"
                                              color="success"/>
                                    %{--<input @click="searchByEstado = !searchByEstado"
                                           class="form-check-input" type="checkbox" id="searchByEstado">
                                    <label class="form-check-label" for="searchByEstado">
                                        Estado
                                    </label>--}%
                                </div>
                            </div>
                            %{--CHECK BIOMA--}%
                            <div class="row">
                                <div class="form-group col-6">
                                    <switches label="Bioma"
                                              v-model="searchByBioma"
                                              :type-bold="false"
                                              theme="bootstrap"
                                              color="success"></switches>
                                </div>

                                %{--CHECK GRUPO TAXONOMICO--}%
                                <div class="form-group col-6">
                                    <switches label="Grupo"
                                              v-model="searchByGrupo"
                                              :type-bold="false"
                                              theme="bootstrap"
                                              color="success"></switches>
                                </div>
                            </div>

                            %{--CHECK UCs --}%
                            <div class="row">
                                <div class="form-group col-6">
                                    <switches label="Unidade conservação"
                                              v-model="searchByUc"
                                              :type-bold="false"
                                              theme="bootstrap"
                                              color="success"></switches>
                                </div>

                                %{--FIM CHECK UCs --}%

                                %{-- CHECK AMEAÇAS --}%
                                <div class="form-group col-6">
                                    <switches label="Ameaça"
                                              v-model="searchByAmeaca"
                                              :type-bold="false"
                                              theme="bootstrap"
                                              color="success"></switches>
                                </div>
                            </div>
                            %{--FIM CHECK AMEAÇA --}%

                            %{--CHECK USOS --}%
                            <div class="row">
                                <div class="form-group col-6">
                                    <switches label="Uso"
                                              v-model="searchByUso"
                                              :type-bold="false"
                                              theme="bootstrap"
                                              color="success"></switches>
                                </div>

                                <div class="form-group col-6">
                                    <switches label="Bacia hidrográfica"
                                          v-model="searchByBacia"
                                          :type-bold="false"
                                          theme="bootstrap"
                                          color="success"/>
                                    </div>
                                </div>
                            %{--FIM CHECK USO --}%

                            %{--CHECK PLANOS DE ACAO --}%
                            <div class="row">
                                <div class="form-group col-6">
                                    <switches label="Plano de Ação"
                                              v-model="searchByPan"
                                              :type-bold="false"
                                              theme="bootstrap"
                                              color="success"></switches>
                                </div>


                          </div>
                          %{--FIM CHECK PÁN --}%


                            <div class="row mt-3"></div>


                            %{--PESQUISAR TAXON--}%
                            <div class="row search-fields-group" v-if="searchByGrupoTaxonomico">
                                <div class="col-sm-12 col-md">
                                    <div class="form-group form-group-sm">
                                        <label for="searchTaxonNivel">Grupo taxonômico</label>
                                        <select v-model="advancedSearch.sqNivelTaxonomico" class="form-control"
                                                id="searchTaxonNivel">
                                            <option value="">-- selecione --</option>
                                            <g:each var="item" in="${listNivelTaxonomico}">
                                                <option data-codigo="${item.coNivelTaxonomico.toLowerCase()}"
                                                        value="${item.id}">${item.deNivelTaxonomico}</option>
                                            </g:each>
                                        </select>
                                    </div>
                                </div>
                                %{-- INICIADO POR...OU...CONTEM PALAVRA - CONDIÇÃO--}%
                                <div class="col-sm-12 col-md">
                                    <div class="form-group form-group-sm">
                                        <label for="searchType">Condição</label>
                                        <select v-model="advancedSearch.searchType"
                                                class="form-control"
                                                :disabled="advancedSearch.sqNivelTaxonomico == ''"
                                                id="searchType">
                                            <option selected value="startWith">Iniciado por</option>
                                            <option value="hasWord">Contem a palavra</option>
                                        </select>
                                    </div>
                                </div>
                                %{--TAXON                            --}%
                                <div class="col-sm-12 col-md">
                                    <div class="form-group form-group-sm">
                                        <label v-html="advancedSearch.noNivelTaxonomico">&nbsp;</label>
                                        <select2taxon id="searchTaxon"
                                                      class="form-control" :params="advancedSearch"
                                                      v-model="advancedSearch.sqTaxonSearch"
                                                      :disabled="advancedSearch.sqNivelTaxonomico == ''">
                                            <option disabled value="0">-- selecione --</option>
                                        </select2taxon>
                                    </div>
                                </div>
                            </div>
                            %{-- FIM SEARCH GRUPO TAXONOMICO--}%

                            %{-- REGIÃO --}%
                            <div class="row search-fields-group mt-2" v-if="searchByRegiao">
                                <div class="col-sm-12 col-md">
                                    <div class="form-group form-group-sm">
                                        <label>Região(ões)<i class="fa fa-info-circle ml-2"
                                                             title="Ao exibir a listagem, clique em uma ou mais opção de filtragem."></i>
                                        </label>
                                        <span class="float-right span-endemica">
                                            <switches label="Endêmica"
                                                      v-model="advancedSearch.endemicaRegiao"
                                                      :type-bold="false"
                                                      theme="bootstrap"
                                                      color="success"/>
                                        </span>
                                        <select2multi v-model="advancedSearch.sqRegiao" class="form-control"
                                                      id="searchRegiao">
                                            <g:each var="item" in="${listRegiao}">
                                                <option data-codigo="${item.id}"
                                                        value="${item.id}">${item.noRegiao}</option>
                                            </g:each>
                                        </select2multi>
                                    </div>
                                </div>
                            </div>
                            %{-- FIM REGIAO --}%

                            %{-- SELECT CATEGORIA --}%
                            <div class="row search-fields-group mt-2" v-if="searchByCategoria">
                                <div class="col-sm-12 col-md">
                                    <div class="form-group form-group-sm">
                                        <label>Categoria(s)<i class="fa fa-info-circle ml-2"
                                                              title="Ao exibir a listagem, clique em uma ou mais opção de filtragem."></i>
                                        </label>
                                        <select2multi v-model="advancedSearch.sqCategoria" class="form-control"
                                                      id="searchCategoria">
                                            <g:each var="item" in="${listCategoria}">
                                                <option data-codigo="${item.codigo}"
                                                        value="${item.id}">${item.descricaoCompleta}</option>
                                            </g:each>
                                        </select2multi>
                                    </div>
                                </div>
                            </div>
                            %{-- FIM CATEGORIA --}%

                            %{-- ESTADO --}%
                            <div class="row search-fields-group mt-2" v-if="searchByEstado">
                                <div class="col-sm-12 col-md">
                                    <div class="form-group form-group-sm">
                                        <label>Estado(s)<i class="fa fa-info-circle ml-2"
                                                           title="Ao exibir a listagem, clique em uma ou mais opção de filtragem."></i>
                                        </label>
                                        <span class="float-right span-endemica">
                                            <switches label="Endêmica"
                                                      v-model="advancedSearch.endemicaEstado"
                                                      :type-bold="false"
                                                      theme="bootstrap"
                                                      color="success"/>
                                        </span>
                                        <select2multi v-model="advancedSearch.sqEstado" class="form-control"
                                                      id="searchEstado">
                                            <g:each var="item" in="${listEstado}">
                                                <option data-codigo="${item.sgEstado}"
                                                        value="${item.id}">${item.noEstado + ' (' + item.sgEstado + ')'}</option>
                                            </g:each>
                                        </select2multi>
                                    </div>
                                </div>
                            </div>
                            %{-- FIM ESTADO --}%

                            %{-- SELECT BIOMA --}%
                            <div class="row search-fields-group mt-2" v-if="searchByBioma">
                                <div class="col-sm-12 col-md">
                                    <div class="form-group form-group-sm">
                                        <label>Bioma(s)<i class="fa fa-info-circle ml-2"
                                                          title="Ao exibir a listagem, clique em uma ou mais opção de filtragem."></i>
                                        </label>
                                        <span class="float-right span-endemica">
                                            <switches label="Endêmica"
                                                      v-model="advancedSearch.endemicaBioma"
                                                      :type-bold="false"
                                                      theme="bootstrap"
                                                      color="success"/>
                                        </span>
                                        <select2multi v-model="advancedSearch.sqBioma" class="form-control"
                                                      id="searchBioma">
                                            <g:each var="item" in="${listBioma}">
                                                <option data-codigo="${item.id}"
                                                        value="${item.id}">${item.descricao}</option>
                                            </g:each>
                                        </select2multi>
                                    </div>
                                </div>
                            </div>
                            %{-- FIM BIOMA --}%



                            %{-- SELECT GRUPO --}%
                            <div class="row search-fields-group mt-2" v-if="searchByGrupo">
                                <div class="col-sm-12 col-md">
                                    <div class="form-group form-group-sm">
                                        <label>Grupo(s)<i class="fa fa-info-circle ml-2"
                                                          title="Ao exibir a listagem, clique em uma ou mais opção de filtragem."></i>
                                        </label>
                                        <select2multi v-model="advancedSearch.sqGrupo" class="form-control"
                                                      id="searchGrupo">
                                            <g:each var="item" in="${listGrupoEspecie}">
                                                <option data-codigo="${item.id}"
                                                        value="${item.id}">${item.descricao}</option>
                                            </g:each>
                                        </select2multi>
                                    </div>
                                </div>
                            </div>
                            %{-- FIM SELECT GRUPO --}%

                            %{-- SELECT UCS --}%
                            <div class="row search-fields-group mt-2" v-if="searchByUc">
                                <div class="col-sm-12 col-md">
                                    <div class="form-group form-group-sm">
                                        <label>Unidade(s) de Conservação<i class="fa fa-info-circle ml-2"
                                                                           title="Ao exibir a listagem, clique em uma ou mais opção de filtragem."></i>
                                        </label>

                                        <div class="d-flex mt-2">
                                            <div class="cursor-pointer">
                                                <input type="radio" v-model="advancedSearch.tipoUc" value="federal"
                                                       name="radioUc" class="radio-lg radio-uc" id="radioUcFederal">
                                                <label class="" for="radioUcFederal">Federais</label>
                                            </div>

                                            <div class="ml-md-3 cursor-pointer">
                                                <input type="radio" v-model="advancedSearch.tipoUc" value="estadual"
                                                       name="radioUc" class="radio-lg radio-uc" id="radioUcEstadual">
                                                <label class="" for="radioUcEstadual">Estaduais</label>
                                            </div>

                                            <div class="ml-md-3 cursor-pointer">
                                                <input type="radio" v-model="advancedSearch.tipoUc" value="rppn"
                                                       name="radioUc" class="radio-lg radio-uc" id="radioUcRppn">
                                                <label class="" for="radioUcRppn">RPPNs</label>
                                            </div>
                                        </div>
                                        <select2multiajax class="form-control"
                                                          url="publico/searchUcs"
                                                          :tipo-uc="advancedSearch.tipoUc"
                                                          v-model="advancedSearch.sqUc"
                                                          id="searchUc">

                                        </select2multiajax>
                                    </div>
                                </div>
                            </div>
                            %{-- FIM SELECT UC --}%

                            %{-- SELECT AMEAÇA --}%
                            <div class="row search-fields-group mt-2" v-if="searchByAmeaca">
                                <div class="col-sm-12 col-md">
                                    <div class="form-group form-group-sm">
                                        <label>Ameaça<i class="fa fa-info-circle ml-2"
                                                        title="Ao exibir a listagem, clique em uma ou mais opção de filtragem."></i>
                                        </label>
                                        <select2multiajax class="form-control"
                                                          url="publico/searchAmeacas"
                                                          v-model="advancedSearch.sqAmeaca"
                                                          id="searchAmeaca">

                                        </select2multiajax>
                                    </div>
                                </div>
                            </div>
                            %{-- FIM SELECT AMEAÇA --}%

                            %{-- SELECT USO --}%
                            <div class="row search-fields-group mt-2" v-if="searchByUso">
                                <div class="col-sm-12 col-md">
                                    <div class="form-group form-group-sm">
                                        <label>Uso<i class="fa fa-info-circle ml-2"
                                                     title="Ao exibir a listagem, clique em uma ou mais opção de filtragem."></i>
                                        </label>
                                        <select2multiajax class="form-control"
                                                          url="publico/searchUsos"
                                                          v-model="advancedSearch.sqUso"
                                                          id="searchUso">
                                        </select2multiajax>
                                    </div>
                                </div>
                            </div>
                            %{-- FIM SELECT USO --}%

                            %{-- BACIA --}%
                            <div class="row search-fields-group mt-2" v-if="searchByBacia">
                                <div class="col-sm-12 col-md">
                                    <div class="form-group form-group-sm">
                                        <label>Bacia<i class="fa fa-info-circle ml-2"
                                                     title="Ao exibir a listagem, clique em uma ou mais opção de filtragem."></i>
                                        </label>
                                        <select2multiajax class="form-control"
                                                          url="publico/searchBacias"
                                                          v-model="advancedSearch.sqBacia"
                                                          id="searchBacia">
                                        </select2multiajax>
                                    </div>
                                </div>
                            </div>

                            %{-- PAN --}%
                            <div class="row search-fields-group mt-2" v-if="searchByPan">
                                <div class="col-sm-12 col-md">
                                    <div class="form-group form-group-sm">
                                        <label>Plano de Ação<i class="fa fa-info-circle ml-2"
                                                     title="Ao exibir a listagem, clique em uma ou mais opção de filtragem."></i>
                                        </label>
                                        <select2multiajax class="form-control"
                                                          url="publico/searchPans"
                                                          v-model="advancedSearch.sqPan"
                                                          id="searchPan">
                                        </select2multiajax>
                                    </div>
                                </div>
                            </div>

                            %{-- FIM BACIA --}%



                        </form>
                    </div>

                    <div class="card-footer d-flex flex-column flex-sm-row justify-content-between">
                        <div class="d-flex justify-content-center d-sm-block btn-group-sm">
                            <button @click.prevent="doSearch"
                                    type="button"
                                    class="btn btn-primary btn-block">Pesquisar</button>
                        </div>

                    </div>
                </div>
            </div>
        </div>


        %{-- TOTAIS --}%
        <div id="divTotals" v-show="layout==1" class="container mt-3 text-center">

            <div class="row">
                <div class="col-sm">
                    <div class="total">
                        <span class="my-odometer" id="totalAvaliadas"><i class="fa fa-spinner fa-spin"></i></span>
                        <br>
                        <label>Espécies avaliadas</label>
                    </div>
                </div>

                <div class="col-sm">
                    <div class="total">
                        %{--                        <span class="my-odometer">${totalPublicadas}</span>--}%
                        <span class="my-odometer" id="totalPublicadas"><i class="fa fa-spinner fa-spin"></i></span>
                        <br>
                        <label>Espécies com ficha publicada</label>
                    </div>
                </div>

                <div class="col-sm">
                    <div class="total">
                        <span class="my-odometer" id="totalAmeacadas"><i class="fa fa-spinner fa-spin"></i></span>
                        <i class="fa fa-asterisk ml-1"></i>
                        <br>
                        <label>Espécies em categoria de ameaça</label>
                    </div>
                </div>

                <div class="col-sm">
                    <div class="total">
                        %{--                        <span class="my-odometer">${totalPessoas}</span>--}%
                        <span class="my-odometer" id="totalPessoas"><i class="fa fa-spinner fa-spin"></i></span>
                        <br>
                        <label>Especialistas<br>envolvidos</label>
                    </div>
                </div>
            </div>
            %{--SAIBA MAIS--}%
            <div class="row">
                <div class="col-12 mt-4">
                    <div class="saiba-mais">
                        <p><i class="fa fa-asterisk"></i>&nbsp;&nbsp;&nbsp;&nbsp;O número apresentado, <strong>incluindo as subespécies</strong>, é resultado do processo contínuo de avaliação do risco de extinção da fauna brasileira. Porém, considerando as Portarias MMA N° 444 e 445/2014, temos 1.173 espécies oficialmente reconhecidas como ameaçadas de extinção.
                            <a href="#!" @click.prevent="doSaibaMais">Saiba mais</a>.
                        </p>

                    </div>
                </div>
            </div>

            %{--CAROUSEL IMAGENS--}%
            <div class="row">
                <div class="col-12 mt-3">
                    <!-- inicio infinite carousel Espécies em destaque -->
                    <div id="randomImagesDestaque" class="card w-100" data-loaded="false">
                        <div class="card-body w-100 demo">
                            <div style="border:0px solid white;">
                                <h3 class="card-title m-0 p-0">Espécies em Destaque</h3>
                            </div>
                            <!-- inicio container testes carousel -->
                            <div class="slider sliderEspeciesDestaque">
                                <ul class="slider__list">
                                    <li v-for="(item,i) in listImagensEspeciesDestaque" :key="item.sq_ficha_multimidia"
                                        class="slider__item">
                                        <div class="wrapper">
                                            <div class="card">
                                                <a :href="'/salve-estadual/publico/getMultimidia/'+item.sq_ficha_multimidia"
                                                   data-lightbox="image-destaque-livro-vermelho"
                                                   :data-title="item.nm_cientifico+'\nFoto: '+item.no_autor+'\nEmail:'+item.de_email_autor">
                                                    <img :src="'/salve-estadual/publico/getMultimidia/'+item.sq_ficha_multimidia"
                                                         class="img-fluid img-specie"
                                                         alt="sem imagem">
                                                </a>

                                                <div class="card-body">
                                                    <div class="nome-autor"
                                                         :title="item.de_email_autor">Autor: {{item.no_autor}}</div>

                                                    <a v-if="item.st_publicada" class="link link-ficha-html" title="Visualizar a ficha da espécie"
                                                       @click.prevent="doFichaHtml(item)"
                                                       href="javascript:void(0);" v-html="item.nm_cientifico">
                                                    </a>
                                                    <a v-else class="link link-ficha-html nao-publicada" title="A ficha da espécie não está publicada"
                                                           href="javascript:void(0);" v-html="item.nm_cientifico">
                                                    </a>

                                                    <div class="nome-comum" v-html="item.no_comum"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>

                                <div class="slider__nav slider__nav_left"></div>

                                <div class="slider__nav slider__nav_next"></div>
                                %{--      <div class="slider__info">123</div>--}%
                                <div class="slider__tabs"></div>
                            </div>
                            <!-- fim container testes carousel -->
                        </div>
                    </div>
                    <!-- fim infinite carousel -->
                </div>
            </div>
        </div> %{--FIM DIV TOTAIS--}%

    %{--TABELA RESULTADO PESQUISA --}%
        <div class="mt-3">
            <salve-table-search id="tableSearchResultMain" :pars="searchParams" :criterias="searchParamsCriterias"
                                v-on:search-end="doOnSearchEnd($event)"/>
        </div>
    </div>
    <!-- fim section inicio -->



    <!-- section salve em numeros -->
    <div id="salveNumeros" class="section mt-3">

        <div class="jumbotron jumbotron-fluid text-center">
            <h1 class="display-4">Avaliação em Números</h1>
        </div>

        <div class="card-body" style="padding:0 30px;">
            %{-- GRAFICOS COM TODAS AS CATEGORIAS --}%
            <div class="d-flex flex-column flex-md-row justify-content-center align-items-center wrapper-graficos">
                %{-- tabela--}%
                <div class="table-wrapper">
                    <table class="table table-sm table-striped table-borderless table-hover table-numeros">
                        <thead class="table-header text-center">
                        <tr>
                            <th></th>
                            <th>Categorias Avaliadas</th>
                            <th>Espécies</th>
                            <th>%</th>
                        </tr>
                        </thead>
                        <tbody class="table-body text-center">
                        <tr v-for="item in salveNumeros.categorias" :class="{'text-warning':item.ameacada}"
                            @click="doSearchSalveEmNumeros( {categoria:item.codigo, title:item.nome, ameacadas:false})"
                            >
                            <td class="text-center">
                                <div class="table-legenda" :style="'background-color:'+item.cor"></div>
                            </td>
                            <td class="text-nowrap text-left">{{item.nome}}</td>
                            <td>{{item.total|formatInt}}</td>
                            <td>{{item.percentual}}%</td>
                        </tr>
                        <tr class="table-footer">
                            <td colspan="2" class="text-center">Total</td>
                            <td class="text-center">{{_.sumBy(salveNumeros.categorias, 'total') | formatInt }}</td>
                            <td>100%</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                %{-- grafico--}%
                <div class="grafico-wrapper d-flex flex-column justify-content-center align-items-center">
                    <div class="graph-title">Categorias</div>

                    <div class="graph-draw" id="graphcategorias">
                        <i class="mt-5 fa fa-spinner fa-spin fa-3x"></i>
                    </div>
                </div>
            </div>
            %{-- FIM GRAFICO TODAS AS CATEGORIAS --}%

            %{-- TABELA E GRAFICO TODOS OS GRUPOS--}%
            <div class="mt-3 d-flex flex-column flex-md-row justify-content-center align-items-center wrapper-graficos">
                %{-- grafico--}%
                <div class="grafico-wrapper graph-area d-flex flex-column justify-content-center align-items-center">
                    <div class="graph-title">Grupos</div>

                    <div class="graph-draw" id="graphgrupos">
                        <i class="mt-5 fa fa-spinner fa-spin fa-3x"></i>
                    </div>
                </div>
                %{--tabela--}%
                <div class="table-wrapper">
                    <table class="table table-sm table-striped table-borderless table-hover table-numeros">
                        <thead class="text-center table-header">
                        <tr>
                            <th></th>
                            <th>Grupos Avaliados</th>
                            <th>Espécies</th>
                            <th>%</th>
                        </tr>
                        </thead>
                        <tbody class="table-body text-center">
                        <tr @click="doSearchSalveEmNumeros( {grupo:item.codigo,title:item.nome, ameacadas:false} )"
                            v-for="item in salveNumeros.grupos">
                            <td class="text-center">
                                <div class="table-legenda" :style="'background-color:'+item.cor"></div>
                            </td>
                            <td class="text-nowrap text-left">{{item.nome}}</td>
                            <td>{{item.total|formatInt}}</td>
                            <td>{{item.percentual}}%</td>
                        </tr>
                        <tr class="table-footer">
                            <td colspan="2" class="text-center">Total</td>
                            <td class="text-center">{{_.sumBy(salveNumeros.grupos, 'total') | formatInt }}</td>
                            <td>100%</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            %{-- FIM GRAFICO TODOS OS GRUPOS --}%

            %{-- GRAFICOS SALVE EM NUMEROS - ESPECIES AMEAÇADAS--}%

            %{-- GRAFICO CATEGORIAS AMEAÇADAS --}%
            <div class="mt-3 d-flex flex-column flex-md-row justify-content-center align-items-center wrapper-graficos">
                %{-- tabela--}%
                <div class="table-wrapper">
                    %{--                            <div><h3>Ameaçadas</h3></div>--}%
                    <table class="table table-sm table-striped table-borderless table-hover table-numeros">
                        <thead class="table-header text-center">
                        <tr>
                            <th colspan="4" class="with-subtitle">Ameaçadas</th>
                        </tr>
                        <tr class="subtitle">
                            <th></th>
                            <th>Categorias</th>
                            <th>Espécies</th>
                            <th>%</th>
                        </tr>
                        </thead>
                        <tbody class="table-body text-center">
                        <tr @click="doSearchSalveEmNumeros( {categoria:item.codigo,title:item.nome, ameacadas:true} )"
                            v-for="item in salveNumeros.categoriasAmeacadas">
                            <td class="text-center">
                                <div class="table-legenda" :style="'background-color:'+item.cor"></div>
                            </td>
                            <td class="text-nowrap text-left">{{item.nome}}</td>
                            <td>{{item.total|formatInt}}</td>
                            <td>{{item.percentual}}%</td>
                        </tr>
                        <tr class="table-footer">
                            <td colspan="2" class="text-center">Total</td>
                            <td class="text-center">{{_.sumBy(salveNumeros.categoriasAmeacadas, 'total') | formatInt }}</td>
                            <td>100%</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                %{-- grafico--}%
                <div class="grafico-wrapper d-flex flex-column justify-content-center align-items-center">
                    <div class="graph-title">Categorias Ameaçadas</div>

                    <div class="graph-draw" id="graphcategoriasAmeacadas">
                        <i class="mt-5 fa fa-spinner fa-spin fa-3x"></i>
                    </div>
                </div>
            </div>
            %{-- FIM GRAFICO CATEGORIAS AMEAÇADAS--}%

            %{-- TABELA E GRAFICO GRUPOS ESPECIES AMEAÇADAS --}%
            <div class="mt-3 d-flex flex-column flex-md-row justify-content-center align-items-center wrapper-graficos">
                %{-- grafico--}%
                <div class="grafico-wrapper graph-area d-flex flex-column justify-content-center align-items-center">
                    <div class="graph-title">Grupos</div>

                    <div class="graph-draw" id="graphgruposAmeacadas">
                        <i class="mt-5 fa fa-spinner fa-spin fa-3x"></i>
                    </div>
                </div>
                %{--tabela--}%
                <div class="table-wrapper">
                    %{--                            <div><h3 class="table-header">AmeaçadasX</h3></div>--}%
                    <table class="table table-sm table-striped table-borderless table-hover table-numeros">
                        <thead class="text-center table-header">
                        <tr><th colspan="4" class="with-subtitle" >Ameaçadas</th></tr>
                        <tr class="subtitle">
                            <th></th>
                            <th>Grupos</th>
                            <th>Espécies</th>
                            <th>%</th>
                        </tr>
                        </thead>
                        <tbody class="table-body text-center">
                        <tr @click="doSearchSalveEmNumeros( {grupo:item.codigo,title:item.nome,categoria:'AMEACADAS',ameacadas:true} )"
                            v-for="item in salveNumeros.gruposAmeacadas">
                            <td class="text-center">
                                <div class="table-legenda" :style="'background-color:'+item.cor"></div>
                            </td>
                            <td class="text-nowrap text-left">{{item.nome}}</td>
                            <td>{{item.total|formatInt}}</td>
                            <td>{{item.percentual}}%</td>
                        </tr>
                        <tr class="table-footer">
                            <td colspan="2" class="text-center">Total</td>
                            <td class="text-center">{{_.sumBy(salveNumeros.gruposAmeacadas, 'total') | formatInt }}</td>
                            <td>100%</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            %{-- FIM GRAFICO GRUPOS ESPECIES AMEAÇADAS--}%
        </div>

        %{-- TABELA REGISTROS--}%
        <div class="mt-3 w-100">
            <div v-if="clickGraphMessageVisible">
                <small>clique no gráfico/tabela para visualizar as lista das espécies</small>
            </div>
            <salve-table-search id="tableSearchResultGraphSalveNumeros"
                                :pars="graphSalveNumerosParams"
                                v-on:search-end="doOnSearchGraphEnd($event)"/>
        </div>

    </div>
    %{-- fim salve em numeros--}%

    <div id="especiesAmeacadas" class="section justify-content-start">
        <div class="card w-100">
            <div class="card-body">
                <div class="container">
                    <h1 class="card-title display-4">Espécies em Categoria de Ameaça</h1>

                    <div class="legenda-graficos row align-items-center mb-2"
                         style="height:50px;background-color: gray;">
                        <div class="col">
                            <div class="cor-legenda cor-legenda-vu"
                                 style="background-color: #FFD700">Vulnerável (VU)</div>
                        </div>

                        <div class="col">
                            <div class="col cor-legenda cor-legenda-en"
                                 style="background-color: #FF8C00">Em Perigo (EN)</div>
                        </div>

                        <div class="col">
                            <div class="col cor-legenda cor-legenda-cr"
                                 style="background-color: #DC143C">Criticamente em Perigo (CR)</div>
                        </div>

                    </div>

                    <div id="graphsWrapper" class="graph
                    d-flex
                    flex-wrap
                    flex-column
                    flex-md-row
                    justify-content-center
                    justify-content-md-around" data-loaded="false">

                        <g:each var="item" in="${listGrupoEspecie}">
                            <!-- grafico de ${item.descricao} -->
                            <div id="graphWrapper${item.codigoSistema.toLowerCase()}"
                                 class="mb-2 graph-area d-flex flex-column justify-content-center align-items-center">
                                <div id="graphTitle${item.codigoSistema.toLowerCase()}"
                                     class="graph-title">${item.descricao}</div>

                                <div id="graphImage${item.codigoSistema.toLowerCase()}"
                                     class="graph-image cursor-pointer"
                                     title="Clique para visualizar tabela"
                                     @click="doSearchGraphByGroup('${item.codigoSistema}','${item.descricao}')">
                                    <img src="/salve-estadual/assets/public/img_grafico_${item.codigoSistema.toLowerCase()}.png"
                                         style="height:50px"></div>

                                <div class="graph-draw" id="graph${item.codigoSistema.toLowerCase()}">
                                    <i class="mt-5 fa fa-spinner fa-spin fa-3x"></i>
                                </div>
                            </div>
                        </g:each>
                    </div>
                </div>

                <div class="card-footer">
                    <div v-if="clickGraphMessageVisible">
                        <small>clique no gráfico para visualizar a tabela das espécies</small>
                    </div>
                    <salve-table-search id="tableSearchResultGraphAmeacadas" :pars="graphAmeacasParams"
                                        v-on:search-end="doOnSearchGraphEnd($event)"/>
                </div>
            </div>
        </div>
    </div>

    <div id="comoFazAvaliacao" class="section justify-content-center">

        <div class="card w-100">
            <div class="card-body">
                <div class="container">
                    <h1 class="card-title display-4">Entenda o processo de avaliação</h1>
                    <p class="card-text text-justify">O processo de avaliação do risco de extinção da fauna brasileira, conduzido pelos Centros Nacionais de Pesquisa e Conservação do Instituto Chico Mendes, é executado em etapas e conta com a participação ativa da comunidade científica. Inicialmente é feita uma compilação e organização de informações e registros de ocorrência em fichas individuais para cada espécie. Em seguida é feita uma consulta ampla à sociedade brasileira e direta aos especialistas da comunidade científica para revisão das informações.
                    Depois de incorporadas as contribuições da etapa de consulta, o risco de extinção das espécies é avaliado seguindo o método de categoria e critérios da União Internacional para a Conservação da Natureza (UICN) em oficinas com a participação de especialistas da comunidade científica. Posteriormente é realizada uma validação dos resultados obtidos na avaliação para checagem da aplicação do método e verificação se todas as informações que subsidiam a categorização das espécies estão claras e coerentes. Após revisão das fichas os resultados são publicados.</p>
                    <div class="d-flex justify-content-center div-image">
                        <img class="img-fluid" style="max-height: 300px"
                             src="/salve-estadual/assets/public/etapas_processo_avaliacao.png">
                    </div>
                </div>
            </div>
        </div>

        <div class="card w-100 mt-2">
            <div class="card-body">
                <div class="container">
                    <h1 class="card-title display-4">Método de Avaliação</h1>
                    <p class="card-text text-justify">O método utilizado para avaliação do risco de extinção das espécies da fauna brasileira foi desenvolvido pela União Internacional para Conservação da Natureza – UICN, é amplamente utilizado em avaliações do estado de conservação de espécies em nível global e já adotado por diversos países. O método utiliza categorias e critérios para identificar o risco de extinção, é produto de amplas discussões entre a UICN e a comunidade científica, e é constantemente revisado. Para a identificação da categoria de risco de extinção de uma espécie são analisadas e combinadas informações sobre tamanho, fragmentação, flutuações ou declínio da população, extensão da distribuição geográfica, ameaças que afetam a espécie, e medidas de conservação já existentes.</p>
                    <p class="mt-2 text-justify">Uma espécie pode ser enquadrada em onze categorias distintas de acordo com o grau do risco de extinção em que se encontra. Por convenção, sempre que houver referência a determinada categoria utiliza-se o nome em português e a sigla original em inglês, entre parênteses.</p>
                    <ul class="text-left mt-2">
                        <li><a href="http://s3.amazonaws.com/iucnredlist-newcms/staging/public/attachments/3108/redlist_cats_crit_en.pdf"
                               target="_blank">IUCN Red List Categories and Criteria: Version 3.1 (2001)</a></li>
                        <li><a href="http://cmsdocs.s3.amazonaws.com/RedListGuidelines.pdf"
                               target="_blank">Guidelines for Using the IUCN Red List Categories and Criteria. Version 14 (2019)</a>
                        </li>
                        <li><a href="http://cmsdocs.s3.amazonaws.com/keydocuments/Reg_Guidelines_en_web%2Bcover%2Bbackcover.pdf"
                               target="_blank">Guidelines for Application of IUCN Red List Criteria at Regional and National Levels: Version 4.0. (2012)</a>
                        </li>
                    </ul>

                    <div class="d-flex justify-content-center div-image">
                        <img class="img-fluid" style="max-height: 400px;"
                             src="/salve-estadual/assets/public/arvore_categorias_iucn.jpeg">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- section livro vermelho -->
    <div id="livroVermelho" class="section justify-content-center">
        <div class="jumbotron jumbotron-fluid">
            <h1 class="display-4">Livro Vermelho</h1>

            <p class="text-justify">O Livro Vermelho da Fauna Brasileira Ameaçada de Extinção, publicado em 2018, apresentou o resultado do Processo de Avaliação coordenado pelo Instituto Chico Mendes e oficializado pelo Ministério do Meio Ambiente por meio das Portarias nº 444 e 445 de 2014. Foi resultado de um dos maiores esforços já empreendidos no mundo para avaliação de risco de extinção de espécies da fauna de um país, sendo avaliadas 12.254 espécies em 73 oficinas de trabalho com a participação de mais de 1.270 especialistas da comunidade científica brasileira e estrangeira, vindos de mais de 250 instituições.</p>
            <p class="text-justify mt-1">A obra está organizada em sete volumes:</p>
            <p class="text-justify mt-1">- Volume I – Apresentação e sumário geral do processo, incluindo a lista de todas as 12.254 espécies da fauna brasileira que foram avaliadas, com a respectiva categoria de risco de extinção.</p>
            <p class="text-left mt-1">As informações sobre as espécies ameaçadas são apresentadas em volumes específicos:</p>
            <p class="text-left">- Volume II - Mamíferos;</p>
            <p class="text-left">- Volume III - Aves;</p>
            <p class="text-left">- Volume IV - Repteis;</p>
            <p class="text-left">- Volume V - Anfíbios;</p>
            <p class="text-left">- Volume VI - Peixes;</p>
            <p class="text-left">- Volume VII - Invertebrados.</p>


            </p>
        </div>

        <div class="card w-100">
            <div class="card-body">
                <div class="container">
                    <h3 class="card-title">Lista Oficial da Fauna Ameaçada: 1.173 espécies</h3>

                    <div class="d-flex flex-column flex-lg-row justify-content-center align-items-center flex-wrap">
                        <div v-for="(item, index) in listLivroVermelho" :key="item.img"
                             class="mb-2 mb-md-0 mr-0 mr-md-4 mt-3">
                            <a :href="item.url" target="_blank">
                                <img :src="'/salve-estadual/assets/public/'+item.img" class="img-fluid img-book">
                            </a>

                            <p class="livroVermelhoVolume" v-html="item.volume ? item.volume : '&nbsp;'"></p>

                            <p class="livroVermelhoGrupo" v-html="item.grupo ? item.grupo : '&nbsp;'"></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- fim section livro vermelho -->

    <div id="quemSomos" class="section">
        <div class="card w-100 mt-3">
            <div class="card-body">
                <div class="container">
                    <h1 class="card-title display-4">Quem Somos</h1>

                    <p class="card-text text-justify">A avaliação do risco de extinção das espécies da fauna brasileira é executada por uma rede de especialistas que realiza um diagnóstico técnico-científico sobre o estado de conservação da fauna. Essa rede é capitaneada pelos Centros de Pesquisa do ICMBio, que atuam em parceria com instituições de pesquisa, sociedades científicas e organizações não governamentais (ONGs) de reconhecida atuação na área de conservação da biodiversidade.</p>
                    <p class="text-justify mt-2">Ao longo deste processo, alguns desses atores possuem funções distintas do conjunto de especialistas.</p>
                    <p class="text-left">São eles:</p>
                    <p class="text-justify mt-2">- <b>Coordenadores de Táxon</b>: especialistas da comunidade científica responsáveis por toda a orientação e decisões científicas relacionadas à avaliação.</p>
                    <p class="text-justify mt-2">- <b>Pontos focais</b>: servidores do ICMBio encarregados da condução do processo de avaliação de determinado grupo taxonômico.</p>
                    <p class="text-justify mt-2">- <b>Equipe de apoio</b>: especialistas (profissionais ou estudantes) que contribuem para a consolidação de informações e execução do processo de avaliação junto aos Centros de Pesquisa do ICMBio.</p>

                    %{-- INICIO ABAS--}%
                    <div class="mt-3 tabs-quem-somos" id="tabsQuemSomos">
                        <nav>
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active" id="nav-coordenacao-processo-tab" data-loaded="true"
                                   data-toggle="tab" href="#nav-coordenacao-processo" role="tab"
                                   aria-controls="nav-coordenacao-processo"
                                   aria-selected="true">Coordenação do Processo</a>

                                <a class="nav-item nav-link" id="nav-coordenadores-taxon-tab" data-loaded="false"
                                   data-papel="COORDENADOR_TAXON" data-toggle="tab" href="#nav-coordenadores-taxon"
                                   role="tab" aria-controls="nav-coordenadores-taxon"
                                   aria-selected="false">Coodenadores de Taxon</a>

                                <a class="nav-item nav-link" id="nav-pontos-focais-tab" data-loaded="false"
                                   data-papel="PONTO_FOCAL" data-toggle="tab" href="#nav-pontos-focais" role="tab"
                                   aria-controls="nav-pontos-focais" aria-selected="false">Pontos Focais</a>

                                <a class="nav-item nav-link" id="nav-equipe-apoio-tab" data-loaded="false"
                                   data-papel="COLABORADOR_EXTERNO" data-toggle="tab" href="#nav-equipe-apoio"
                                   role="tab" aria-controls="nav-equipe-apoio" aria-selected="false">Equipe Apoio</a>
                            </div>
                        </nav>

                        <div class="tab-content tab-content-quem-somos">
                            <div class="tab-pane fade show active p-3" id="nav-coordenacao-processo" role="tabpanel"
                                 aria-labelledby="nav-coordenacao-processo-tab">
                                <table class="table table-sm table-bordered table-striped table-hover table-search-result">
                                    <thead class="text-center">
                                    <tr style="font-size: 1.15rem;" class="text-center">
                                        <th style="width:40px;height: 40px;">#</th>
                                        <th style="width:250px;">Nome</th>
                                        <th style="width:160px;">Cargo</th>
                                        <th style="width:140px;">Currículo Lattes</th>
                                        <th style="width:300px;">Email</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr v-for="(item, i) in listQuemSomos" :key="item.email">
                                        <th style="width:40px;" class="text-center">{{i+1}}</th>
                                        <td style="width:250px;" v-html="item.nome"></td>
                                        <td style="width:160px;" v-html="item.cargo"></td>
                                        <td style="width:300px;" class="text-center">
                                            <template v-if="item.lattes">
                                                <a class="link-lattes" :href="item.lattes" target="_blank"
                                                   title="Ver currículo lattes">
                                                    <i class="fa fa-eye"></i>
                                                </a>
                                            </template>
                                            <template v-else>
                                                <span><i class="fa fa-eye disabled"></i></span>
                                            </template>
                                        </td>
                                        <td style="width:250px;">
                                            <a class="email" :href="'mailto:'+item.email">{{ item.email }}</a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>


                            <div class="tab-pane fade show p-3" id="nav-coordenadores-taxon" role="tabpanel"
                                 aria-labelledby="nav-coordenadores-taxon-tab">
                                <salve-table-participantes ref="tableCoordenadoresTaxon" id="tableCoordenadoresTaxon"
                                                           unidade-header="Instituição"
                                                           :list-data="quemSomos.COORDENADOR_TAXON"></salve-table-participantes>
                            </div>

                            <div class="tab-pane fade show p-3" id="nav-pontos-focais" role="tabpanel"
                                 aria-labelledby="nav-pontos-focais-tab">
                                <salve-table-participantes ref="tablePontosFocais" id="tablePontosFocais"
                                                           :list-data="quemSomos.PONTO_FOCAL"></salve-table-participantes>
                            </div>

                            <div class="tab-pane fade show p-3" id="nav-equipe-apoio" role="tabpanel"
                                 aria-labelledby="nav-equipe-apoio-tab">
                                <salve-table-participantes ref="tableEquipeApoio" id="tableEquipeApoio"
                                                           :list-data="quemSomos.COLABORADOR_EXTERNO"></salve-table-participantes>
                            </div>

                        </div>

                    </div>
                    %{-- FIM ABAS--}%
                    %{--
                                        <h2 class="mb-3 mt-3">Coordenação do Processo</h2>
                                        <div class="table-quem-somos" style="color:#fff;">
                                            <table id="tableQuemSomos2" class="table table-sm table-bordered table-striped table-hover table-search-result">
                                                <thead class="text-center">
                                                    <tr style="font-size: 1.15rem;" class="text-center">
                                                        <th style="width:50px;height: 50px;">#</th>
                                                        <th>Nome</th>
                                                        <th>Cargo</th>
                                                        <th style="width:140px;">Currículo Lattes</th>
                                                        <th>Email</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <tr v-for="(item, i) in listQuemSomos" :key="item.email">
                                                    <th class="text-center">{{i+1}}</th>
                                                    <td v-html="item.nome"></td>
                                                    <td v-html="item.cargo"></td>
                                                    <td class="text-center">
                                                        <template v-if="item.lattes">
                                                            <a class="link-lattes" :href="item.lattes" target="_blank" title="Ver currículo lattes">
                                                                <i class="fa fa-eye"></i>
                                                            </a>
                                                        </template>
                                                        <template v-else>
                                                            <span> <i class="fa fa-eye disabled"></i></span>
                                                        </template>
                                                    </td>
                                                    <td>
                                                        <a class="email" :href="'mailto:'+item.email">{{ item.email }}</a>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <div class="d-flex justify-content-start participantes-wrapper">
                                                <i class="fa fa-user-plus fa-2x mr-md-3 text-warning" title="clque nos botões para visualizar os demais participantes."></i>
                                                <button @click="doPopupParticipantes('PONTO_FOCAL')" class="btn btn-outline-light mr-md-3" type="button">Pontos focais</button>
                                                <button @click="doPopupParticipantes('COORDENADOR_TAXON')" class="btn btn-outline-light mr-md-3" type="button">Coordenadores de taxon</button>
                                                <button @click="doPopupParticipantes('COLABORADOR_EXTERNO')" class="btn btn-outline-light" type="button">Equipe de apoio</button>
                                            </div>
                                        </div>--}%
                    %{--
                                        <div class="d-flex flex-row justify-content-center flex-wrap quem-somos-fotos" >
                                           <div class="profile mb-sm-3 mr-md-3" v-for="(item, i) in listQuemSomos" :key="item.email">
                                                <div class="img-box mb-2">
                                                        <template v-if="item.foto">
                                                            <img :src="item.foto" class="img-responsive">
                                                        </template>
                                                        <template v-else>
                                                            <i class="fa fa-user"></i>
                                                        </template>
                                                </div>
                                                <h1><strong v-html="item.nome"></strong></h1>
                                                <h2 v-html="item.cargo"></h2>
                                                <p v-if="item.lattes"><a class="link-lattes" :href="item.lattes" target="_blank">Link Currículo Lattes</a></p>
                                                <p><i class="fa fa fa-envelope mr-2"></i><a :href="'mailto:'+item.email">{{ item.email }}</a></p>
                                            </div>
                                        </div>

                                        <h2 class="mb-3 mt-3">Coordenadores de Taxon</h2>
                                        <div class="d-flex flex-row justify-content-center flex-wrap quem-somos-fotos" >
                                            <div class="profile mb-sm-3 mr-md-3"><div class="img-box mb-2"><i class="fa fa-user"></i></div> <h1><strong>Nome</strong></h1> <h2>Analista ambiental</h2> <p><a href="#!" target="_blank" class="link-lattes">Link Currículo Lattes</a></p> <p><i class="fa fa fa-envelope mr-2"></i><a href="mailto:xxxxx@icmbio.gov.br">xxxxxx@icmbio.gov.br</a></p></div>
                                            <div class="profile mb-sm-3 mr-md-3"><div class="img-box mb-2"><i class="fa fa-user"></i></div> <h1><strong>Nome</strong></h1> <h2>Analista ambiental</h2> <p><a href="#!" target="_blank" class="link-lattes">Link Currículo Lattes</a></p> <p><i class="fa fa fa-envelope mr-2"></i><a href="mailto:xxxxx@icmbio.gov.br">xxxxxx@icmbio.gov.br</a></p></div>
                                            <div class="profile mb-sm-3 mr-md-3"><div class="img-box mb-2"><i class="fa fa-user"></i></div> <h1><strong>Nome</strong></h1> <h2>Analista ambiental</h2> <p><a href="#!" target="_blank" class="link-lattes">Link Currículo Lattes</a></p> <p><i class="fa fa fa-envelope mr-2"></i><a href="mailto:xxxxx@icmbio.gov.br">xxxxxx@icmbio.gov.br</a></p></div>
                                        </div>

                                        <h2 class="mb-3 mt-3">Pontos Focais</h2>
                                        <div class="d-flex flex-row justify-content-center flex-wrap quem-somos-fotos" >
                                            <div class="profile mb-sm-3 mr-md-3"><div class="img-box mb-2"><i class="fa fa-user"></i></div> <h1><strong>Nome</strong></h1> <h2>Analista ambiental</h2> <p><a href="#!" target="_blank" class="link-lattes">Link Currículo Lattes</a></p> <p><i class="fa fa fa-envelope mr-2"></i><a href="mailto:xxxxx@icmbio.gov.br">xxxxxx@icmbio.gov.br</a></p></div>
                                            <div class="profile mb-sm-3 mr-md-3"><div class="img-box mb-2"><i class="fa fa-user"></i></div> <h1><strong>Nome</strong></h1> <h2>Analista ambiental</h2> <p><a href="#!" target="_blank" class="link-lattes">Link Currículo Lattes</a></p> <p><i class="fa fa fa-envelope mr-2"></i><a href="mailto:xxxxx@icmbio.gov.br">xxxxxx@icmbio.gov.br</a></p></div>
                                            <div class="profile mb-sm-3 mr-md-3"><div class="img-box mb-2"><i class="fa fa-user"></i></div> <h1><strong>Nome</strong></h1> <h2>Analista ambiental</h2> <p><a href="#!" target="_blank" class="link-lattes">Link Currículo Lattes</a></p> <p><i class="fa fa fa-envelope mr-2"></i><a href="mailto:xxxxx@icmbio.gov.br">xxxxxx@icmbio.gov.br</a></p></div>
                                        </div>

                    --}%

                </div>
            </div>
        </div>
    </div>

    <div id="faleConosco" class="section">
        <div class="card" style="width: 75%; max-width: 550px !important;">
            <div class="card-body">
                <div>
                    <h2 class="card-title display-4">Fale conosco</h2>
                </div>

                <form class="form text-left form-fale-conosco">

                    <div class="form-group">
                        <label>Seu nome</label>
                        <input type="text" id="fldUserName" maxlength="150"
                               v-model="faleConosco.nome"
                               class="form-control" placeholder="(opcional)" aria-label="Seu nome">
                    </div>

                    <div class="form-group">
                        <label>Seu e-mail</label>
                        <input type="text" maxlength="150"
                               v-model="faleConosco.email"
                               class="form-control" placeholder="(opcional)" aria-label="Seu email">
                    </div>

                    <div class="form-group">
                        <label>Assunto</label>
                        <input type="text" id="fldSubject" maxlength="150"
                               v-model="faleConosco.assunto"
                               class="form-control" placeholder="" aria-label="Assunto">
                    </div>


                    <div class="form-group">
                        <label>Mensagem</label>
                        <textarea class="form-control" aria-label="With textarea"
                                  v-model="faleConosco.mensagem"></textarea>
                    </div>

                    <div class="form-group">
                        <label>Os campos nome e e-mail são opcionais, ao preenchê-los você está optando por receber uma resposta da equipe do Sistema SALVE.
                        </label>
                    </div>
                </form>

                <button type="button" @click.prevent="doSendEmail" :disabled="faleConosco.mensagem.trim() ==''"
                        class="mr-4 btn" :class="faleConosco.mensagem.trim() == '' ? 'btn-secondary' : 'btn-success'">
                    <i class="fa fa-send mr-2"></i>Enviar
                </button>
                <button type="button" @click.prevent="doClearFaleConosco"
                        class="btn btn-danger">
                    <i class="fa fa-trash mr-2"></i>Limpar
                </button>

                %{--<div class="mt-2 d-flex justify-content-center">
                    <g:if test="${captchaPublicKey}">
                        <div class="g-recaptcha" data-sitekey="${captchaPublicKey}"></div>
                    </g:if>
                </div>--}%

            </div>
        </div>
    </div>
    %{--    FIM FALECONOSCO--}%

    %{--    SOBRE O SALVE E PARCEIROS --}%
    <div id="sobreSalve" class="section">
        <div class="card w-100">
            <div class="card-body">
                <div class="container">
                    <h1 class="card-title display-4">Sobre o Sistema SALVE</h1>
                    <p class="card-text text-justify">O Sistema de Avaliação do Risco de Extinção da Biodiversidade – SALVE foi elaborado com o objetivo de facilitar a gestão do processo de avaliação da fauna brasileira. Funciona como uma base de dados para o armazenamento e organização das informações sobre as espécies e como uma ferramenta de controle, acompanhamento e execução das diferentes etapas do processo, desde o início da compilação de dados  sobre as espécies, passando pela organização das informações sobre os pesquisadores e instituições parceiras envolvidas, até a avaliação do risco de extinção das espécies e publicação dos resultados.</p>
                    <p class="mt-2 text-justify">O SALVE foi desenvolvido pelo ICMBio e é operado por técnicos do Instituto. Os parceiros, especialistas da comunidade científica, atuam incluindo dados sobre as espécies da fauna no banco de dados e auxiliando o processo na organização, revisão e correção dessas informações, além da avaliação do risco de extinção das espécies nas oficinas e posterior validação dos resultados. </p>

                    <div class="entidades-parceiras-title mt-5">Realização</div>
%{--                    <div class="d-flex flex-column flex-md-row flex-wrap justify-content-around align-items-center" style="flex-basis: 80%;">--}%


                    <div style="display:flex; flex-direction: row; flex-wrap: wrap; align-items: center; justify-content: space-between;xflex-basis: 100%;">
                        <div v-for="item in listCentros" :key="item.id" class="mt-1 centros" style="padding:20px;">
                            <a target="_blank" :href="item.site" :title="item.nome"><img class="" :src="item.imgUrl" style="max-height: 90px;"alt="Logo"></a>
                        </div>
                    </div>


                    <div class="entidades-parceiras-title mt-5">Entidades parceiras</div>
                    <div class="d-flex flex-column flex-md-row justify-content-around align-items-center logos-parceiros mt-1">
                        %{--<div class="logo-parceiro ml-3">
                            <a href="https://www.gov.br/planalto/pt-br" target="_blank"><img class="img-responsive logo-parceiro" src="/salve-estadual/assets/public/parceiros/governo.png"></a>
                        </div>

                        <div class="logo-parceiro ml-3">
                            <a href="https://www.mma.gov.br/" target="_blank"><img  class="img-responsive logo-parceiro" src="/salve-estadual/assets/public/parceiros/mma.png"></a>
                        </div>

                        <div class="logo-parceiro ml-3">
                            <a href="https://www.icmbio.gov.br/portal/" target="_blank"><img  class="img-responsive logo-parceiro" src="/salve-estadual/assets/public/parceiros/icmbio.png"></a>
                        </div>--}%



%{--
                        <div class="logo-parceiro ml-3">
                            <a href="https://www.funbio.org.br/programas_e_projetos/gef-mar-funbio/" target="_blank"><img  class="img-responsive logo-parceiro" src="/salve-estadual/assets/public/parceiros/gef-mar-150.png"></a>
                        </div>
                        <div class="logo-parceiro ml-3">
                            <a href="http://proespecies.eco.br/" target="_blank"><img  class="img-responsive logo-parceiro" src="/salve-estadual/assets/public/parceiros/pro-especies.png"></a>
                        </div>
                        <div class="logo-parceiro ml-3">
                            <a href="http://cnpq.br/" target="_blank"><img  class="img-responsive logo-parceiro" src="/salve-estadual/assets/public/parceiros/cnpq.png"></a>
                        </div>

--}%


                        <div class="logo-parceiro ml-3">
                            <a href="https://www.wwf.org.br/" target="_blank"><img  class="img-responsive logo-parceiro" src="/salve-estadual/assets/public/parceiros/wwf.png"></a>
                        </div>
                        <div class="logo-parceiro ml-3">
                            <a href="https://www.funbio.org.br/" target="_blank"><img  class="img-responsive logo-parceiro" src="/salve-estadual/assets/public/parceiros/funbio.png"></a>
                        </div>
                        <div class="logo-parceiro ml-3">
                            <a href="https://www.thegef.org/" target="_blank"><img  class="img-responsive logo-parceiro" src="/salve-estadual/assets/public/parceiros/gef.png"></a>
                        </div>
                        <div class="logo-parceiro ml-3">
                            <a href="https://www.gov.br/mma/pt-br" target="_blank"><img  class="img-responsive logo-parceiro" src="/salve-estadual/assets/public/parceiros/mma.png"></a>
                        </div>
                        <div class="logo-parceiro ml-3">
                            <a href="https://www.gov.br/pt-br" target="_blank"><img  class="img-responsive logo-parceiro" src="/salve-estadual/assets/public/parceiros/governo.png"></a>
                        </div>



                    </div>

                </div>
            </div>
        </div>
    </div>
    %{-- FIM SOBRE O SALVE E PARCEIROS --}%


    %{--MODAL PARTICIPANTES ( PONTOS FOCAIS, COORDENADORES E APOIO )--}%
    %{--
        <div class="modal fade" id="modalParticipantes" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-xl modal-dialog-scrollable modal-dialog-centered" role="document">
                <div class="modal-content modal-participantes">
                    <div class="modal-header">
                        <h5 class="modal-title"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" style="min-height: 300px;">

                        <table class="table table-hover table-sm table-bordered table-search-result dataTable" id="tableModalParticipantes">
                            <thead>
                            <tr class="text-center">
                                <th style="min-width:30px;width:30px;height: 40px;">#</th>
                                <th style="width:300px;">Nome</th>
                                <th style="width:auto;">Unidade</th>
                                <th style="width:300px;">Email</th>
                            </tr>
                            </thead>
                            <tbody>
                            <template v-if="! searchingParticipantes">
                            <tr class="text-center" v-for="(item,i) in listParticipantes" :key="item.de_email">
                                <td></td>
                                <td class="text-left">{{item.no_pessoa}}</td>
                                <td>{{item.no_unidade}}</td>
                                <td>{{item.de_email}}</td>
                            </tr>
                            </template>
                            <template v-else>
                                <tr>
                                    <td colspan="4">
                                        <h5>Consultando...<i class="fa fa-spinner fa-spin"></i></h5>
                                    </td>
                                </tr>
                            </template>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
                    </div>
                </div>
            </div>
        </div>
    --}%

</div>
<!-- espaço final da página -->
<div style="min-height:200px;"></div>

%{-- template vue para a tabela de resultado das pesquisas utilizada no
     componente salve-table-search
 --}%
<script type="text/x-template" id="templateTableSearchResult">
<div v-show="searchResults.length > 0"
     :id="id ? id+'Wrapper' : 'tableSearchResultWrapper'+idTemp"
     style="position:relative;"
     class="table-responsive-sm table-search-result-wrapper">
    <!-- components: salveTableSearch.js -->
    <table :id="id ? id : 'tableSearchResult'+idTemp"
           class="table table-hover table-striped table-condensed table-bordered table-search-result">
        <thead class="table-head-fixed">
        <tr>
            <th colspan="10" class="text-left text-md-center">
                <div class="d-flex justify-content-between">
                    <div class="table-search-result-header" v-html="header ? header:''"></div>
                    <div><i @click="doClearResult" title="Limpar resultado" class="cursor-pointer text-danger fa fa-times"></i></div>

                </div>
            </th>
        </tr>
        <tr>
            <th colspan="10">
                <div v-if="searchResults.length > 0"
                     class="d-flex flex-column flex-md-row align-items-start align-items-md-center justify-content-md-between">
                    <div class="table-search-result-title" v-html="title"></div>

                    <div class="table-search-result-total-records">{{totalRegistros}}</div>

                    <div>
                        <div class="d-flex flex-column flex-md-row justify-content-around">
                            <div>
                                <button @click.prevent="doExportSearchResult()" type="button"
                                        class="mr-2 mb-2 mb-md-0 btn btn-sm btn-warning" :disabled="exportingPlanilha">
                                    <template v-if="!exportingPlanilha">
                                        Exportar tabela
                                    </template>
                                    <template v-else>
                                        Exportando...<i class="fa fa-spinner fa-spin"></i>
                                    </template>
                                </button>
                            </div>

                            <div>
                                <button @click.prevent="doDownloadRegistros()" type="button"
                                        class="mr-2 mb-2 mb-md-0 btn btn-sm btn-warning"
                                        :disabled="exportingOcorrencias">
                                    <template v-if="!exportingOcorrencias">
                                        Exportar registros
                                    </template>
                                    <template v-else>
                                        Exportando...<i class="fa fa-spinner fa-spin"></i>
                                    </template>
                                </button>
                            </div>

                            <div>
                                <button @click.prevent="doDownloadFichasZip()" type="button"
                                        class="mr-2 mb-2 mb-md-0 btn btn-sm btn-warning" :disabled="exportingFichasZip">
                                    <template v-if="!exportingFichasZip">
                                        Baixar fichas (zip)
                                    </template>
                                    <template v-else>
                                        Gerando zip...<i class="fa fa-spinner fa-spin"></i>
                                    </template>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </th>
        </tr>
        <tr class="text-center">
            <th scope="col">#</th>
            <th scope="col">
                <i class="fa cursor-pointer"
                   @click.prevent="doSelectAllRows"
                   :class="{'fa-square-o':!selectedAllRows,'fa-check-square-o text-warning':selectedAllRows}"
                   :title="'Marcar/Desmarcar todos.\nUtilize esta opção para: Exportar tabela, Exportar registros e Baixar fichas (zip).'"></i>
                %{--<template v-if="totalRowsSelected > 0 ">
                    <div class="total-rows-selected">{{totalRowsSelected}}</div>
                </template>--}%
            </th>
            <th scope="col" class="col-nome-cientifico">Espécie <i @click.prevent="doSort"
                                                                   data-sort-column="nm_cientifico"
                                                                   data-sort-order="asc"
                                                                   class="fa fa-sort-alpha-asc float-right cursor-pointer sort-active"
                                                                   title="Ordenar"></i></th>
            <th scope="col" class="col-nome-comum">Nome comum <i @click.prevent="doSort" data-sort-column="no_comum"
                                                                 data-sort-order="asc"
                                                                 class="fa fa-sort-alpha-asc float-right cursor-pointer sort-inactive"
                                                                 title="Ordenar"></i></th>
            <th scope="col" class="col-grupo">Grupo <i @click.prevent="doSort" data-sort-column="ds_grupo_salve"
                                                                 data-sort-order="asc"
                                                                 class="fa fa-sort-alpha-asc float-right cursor-pointer sort-inactive"
                                                                 title="Ordenar"></i></th>

            <th scope="col" class="col-categoria">Categoria
                <i title="Para visualizar o(s) critério(s) passe o mouse sobre a categoria de ameaça." class="fa fa-info-circle ml-1 mr-1 text-info"></i>
                                                              <i @click.prevent="doSort"
                                                               data-sort-column="de_categoria_final_completa"
                                                               data-sort-order="asc"
                                                               class="fa fa-sort-alpha-asc float-right cursor-pointer sort-inactive"
                                                               title="Ordenar"></i></th>
            <th scope="col" class="col-periodo-ultima-avaliacao">Última <span class="text-nowrap">avaliação <i
                    @click.prevent="doSort" data-sort-column="dt_fim_avaliacao" data-sort-order="asc"
                    class="fa fa-sort-alpha-asc cursor-pointer sort-inactive" title="Ordenar"></i></span></th>
            <th scope="col" class="col-ufs">UF(s) <i @click.prevent="doSort" data-sort-column="sg_estado"
                                                     data-sort-order="asc"
                                                     class="fa fa-sort-alpha-asc float-right cursor-pointer sort-inactive"
                                                     title="Ordenar"></i></th>
            <th scope="col" class="col-biomas">Bioma(s) <i @click.prevent="doSort" data-sort-column="no_bioma"
                                                           data-sort-order="asc"
                                                           class="fa fa-sort-alpha-asc float-right cursor-pointer sort-inactive"
                                                           title="Ordenar"></i></th>
            <th scope="col" class="col-acao">Ação</th>
        </tr>
        </thead>

        <tbody>
        <tr v-for="(item, index) in searchResults" :key="item.sq_ficha" class="text-center" :class="{'tr-ficha-excluida':item.cd_situacao_ficha == 'EXCLUIDA'}">
            <th scope="row">{{ pagination.offSet + index + 1 }}</th>
            <th scope="row">
                <template v-if="item.cd_situacao_ficha != 'EXCLUIDA'">
                    <input title="selecionar para exportação" class="checkbox-lg cursor-pointer"
                       type="checkbox" :checked="isRowSelected( item )"
                       @click="doSelectTableRow($event,item)">
                </template>
            </th>
            <td class="text-left">
                <div class="td-nome-cientifico">
                    <template v-if="item.nm_cientifico_atual">
                        <i class="fa fa-info-circle text-info mr-1 cursor-pointer"
                           :title="stripTags(item.nm_cientifico) +' é nome antigo de '+item.nm_cientifico_atual"></i>
                    </template>

                    <template v-if="item.cd_situacao_ficha=='EXCLUIDA'">
                        <i class="fa fa-info-circle text-danger mr-1 cursor-pointer" @click.prevent="doShowJustificativaExclusao(item)" title="Ficha foi excluída da avaliação. Clique para ler a justificativa."></i>
                    </template>

                    <template v-if="item.sq_ficha_publicada">
                        <a class="link" title="Visualizar ficha publicada" @click.prevent="doFichaHtml(item)"
                           href="javascript:void(0);" v-html="item.nm_cientifico">
                        </a>
                    </template>
                    <template v-else>
                        <template v-if="item.cd_situacao_ficha=='EXCLUIDA'">
                            <span class="disabled" v-html="item.nm_cientifico"></span>
                        </template>
                        <template>
                            <span class="disabled" title="Não possui ficha publicada" v-html="item.nm_cientifico"></span>
                        </template>
                    </template>
                </div>
            </td>
            <td><div class="td-nome-comum">{{ item.no_comum }}</div></td>
            <td><div class="td-grupo">{{ item.ds_grupo_salve }}</div></td>
            <td>
                <span :title="  item.de_criterio_final_completo ? 'Critério(s): ' + item.de_criterio_final_completo:'Categoria não possui critério'">{{ item.de_categoria_final_completa }}</span>
            </td>
            <td class="text-nowrap">{{ item.de_periodo_avaliacao}}</td>
            <td>{{ item.sg_estado }}</td>
            <td>{{ item.no_bioma }}</td>
            <td class="text-nowrap">
                <template v-if="item.cd_situacao_ficha != 'EXCLUIDA'">
                    <template v-if="item.sq_ficha_publicada">
                        <a target="_blank" :href="'/salve-estadual/publico/fichaPdf/'+item.sq_ficha">
                            <i title="Visualizar a ficha em PDF" class="fa fa-file-pdf-o"></i>
                        </a>
                    </template>
                    <template v-else>
                        <i title="Não possui ficha publicada" class="fa fa-file-pdf-o disabled"></i>
                    </template>
                </template>
                %{--<a target="_blank" href=";" @click.prevent="doDownloadRegistros(item)">
                    <i title="Baixar planilha de registros de ocorrência." class="ml-2 fa fa-download"></i>
                </a>--}%
            </td>
        </tr>
        </tbody>
        <tfoot>
        <tr class="text-center">
            <td colspan="10">
                <nav v-if="pagination.pages>1">
                    <paginate
                            v-model="pagination.page"
                            :page-count="pagination.pages"
                            :click-handler="onPageChange"
                            prev-text='&nbsp;<i class="fa fa-angle-left"></i>'
                            next-text='&nbsp;<i class="fa fa-angle-right"></i>'

                            first-button-text='&nbsp;<i class="fa fa-angle-double-left"></i>'
                            last-button-text='&nbsp;<i class="fa fa-angle-double-right"></i>'
                            :first-last-button="true"
                            :hide-prev-next="true"

                            :container-class="'pagination justify-content-start'"
                            :page-link-class="'page-link'"
                            :next-link-class="'page-link'"
                            :prev-link-class="'page-link'"
                            :page-class="'page-item'"
                            :disabled-class="'page-item-disabled'">
                        <span slot="breakViewContent">
                            %{--                                        &nbsp;<i class="fa fa-arrows-h"></i>&nbsp;--}%
                            &nbsp;<i class="fa fa-ellipsis-h"></i>&nbsp;
                        %{--<svg width="16" height="20" viewBox="0 0 16 3">
                            <circle fill="#999999" cx="2" cy="2" r="2" />
                            <circle fill="#999999" cx="8" cy="2" r="2" />
                            <circle fill="#999999" cx="14" cy="2" r="2" />
                        </svg>--}%
                        </span>
                    </paginate>
                </nav>
            </td>
        </tr>
        </tfoot>
    </table>
</div>
</script>

%{-- template vue para a tabela de participantes da seção quem somos utilizado no componente salve-table-participantes --}%
<script type="text/x-template" id="templateTableParticipantes">
<div class="salveTableParticipantes">
    <table class="table table-hover table-sm table-bordered table-search-result dataTable" :id="id?id:idTemp">
        <thead>
        <tr class="text-center">
            <th style="min-width:30px;width:30px;height: 40px;">#</th>
            <th style="width:250px;">Nome</th>
            <th style="width:150px;">{{ unidadeHeader ? unidadeHeader : 'Unidade'}}</th>
            <th v-if="id!='tableEquipeApoio'">Grupo</th>
            <th>Email</th>
        </tr>
        </thead>
        <tbody>
        <template v-if="listData && listData.length > 0 ">
            <tr class="text-center" v-for="(item,i) in listData" :key="item.de_email+(i)">
                <th style="min-width:30px;width:30px;"></th> %{-- Utilizar dataTable para numerar --}%
                <td style="width:250px;" class="text-left">{{item.no_pessoa}}</td>
                <td style="width:150px;">{{item.no_unidade}}</td>
                <td v-if="id!='tableEquipeApoio'">{{item.no_grupo}}</td>
                <td>{{item.de_email}}</td>
            </tr>
        </template>
        <template v-else>
            <tr>
                <td colspan="4">
                    <h5>Consultando...<i class="fa fa-spinner fa-spin"></i></h5>
                </td>
            </tr>
        </template>
        </tbody>
    </table>
</div>
</script>

<script defer type="text/javascript">
    window.reCaptchaPublicKey = '${captchaPublicKey}';
    function inicializar() {
       <g:if test="${flash.message}">
        $('document').ready(function () {
            window.setTimeout(function () {
                swal2("${flash.message}", '', 'success');
            }, 2000)
        });
        %{--        <div class="message" style="display: block">${flash.message}</div>--}%
        </g:if>
    }
</script>

</body>

</html>
