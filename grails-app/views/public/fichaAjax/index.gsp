<!-- Estrutura inicial de tópicos da ficha -->
<div class="container-fluid ficha-ajax ficha-html" style="position:relative;top:0;left:0;" id="div-${sqFichaEncrypted}">

    <div class="ficha-header d-flex justify-content-between">
        <div>
            <span class="ficha-title">Ficha da Espécie</span>
        </div>
        <div>
            <button data-fancybox-close
                    class="btn btn-sm btn-danger btn-close">
                <i title="fechar" class="cursor-pointer fa fa-times"></i>
            </button>
        </div>
    </div>

    <!-- botao fechar
    <button data-fancybox-close class="btn btn-sm btn-outline-danger btn-close"
            style="width:35px;position: absolute;top:4px;right:12px; outline: none;">
        <i title="fechar" class="cursor-pointer fa fa-times"></i>
    </button>
     -->

    <div class="card card-specie mb-3" style="width:auto;">
        <div class="card-body">
            <div class="row no-gutters">
                <div class="col-12 col-md-4 order-md-last text-center">
                    <a href="${imagemPrincipal.url}"
                       data-lightbox="image-specie"
                       data-title="${imagemPrincipal?.autor}">
                            <img src="${imagemPrincipal.url}" class="img-fluid img-header" alt="sem-imagem">
                    </a>
                    <p class="card-text mb-0 p-0 d-block text-center autor"><small class="text-muted">Autor:${imagemPrincipal?.autor}</small></p>
                </div>
                <div class="col-12 col-md-8 align-self-center order-md-first">
                    <p class="card-title card-text text-left">Avaliação do Risco de Extinção de
                    <span class="nome-cientifico">${ raw( nmCientificoItalicoComAutor) }</span>
                </div>
            </div>
        </div>
    </div>

    %{--<div class="card">
        <div class="card-header static d-flex flex-column flex-md-row align-content-center align-items-center justify-content-center">
                <div>
                    <span>Avaliação do Risco de Extinção de <b class="text-warning">${ raw( nmCientificoItalicoComAutor) }</b> </span>
                </div>
                <g:if test="${imagemPrincipal}">
                    <div>
                        <img class="img-responsive img-fluid ml-0 ml-md-2" src="${imagemPrincipal.url}"
                         style="cursor:pointer;border-radius:5px;max-height: 120px;">
                        <p class="ml-0 ml-md-2" style="margin:0;padding:0;"><small style="font-size:0.8rem">Autor:Luis Eugênio</small></p>
                    </div>
                </g:if>
        </div>
    </div>
--}%
    <div class="card mt-3">
        <div class="card-header static current-category text-center d-flex justify-content-between align-items-center">
%{--            <div class="pull-right"><img style="width:32px;height:32px;" src="/salve-estadual/assets/public/NT.png"></img></div>--}%

            <g:if test="${ categoriaFinal }">
            <div class="categoria-avaliacao text-left w-100">
                <span>Categoria: <b class="text-warning">${ categoriaFinal }
                    <g:if test="${categoriaFoiAlterada}">
                        <i class="text-danger fa fa-asterisk ml-1"></i>
                    </g:if>
                    </b>
                </span>
                <br>
                <label class="label-justificativa">Justificativa</label>
                <div class="text-justificativa">${raw(justificativaFinal)}</div>
                %{--<g:if test="${categoriaFoiAlterada}">
                    <small class="mr-2  text-justify texto-mudanca-categoria"><i class="text-danger fa fa-asterisk mr-1"></i>
                        Essa categoria é resultado da avaliação mais recente da espécie e está sendo disponibilizada para subsidiar eventuais ações de conservação que a beneficiem. Entretanto, essa categoria ainda não foi oficializada por meio de Portaria do MMA. Para fins legais, deve-se utilizar
                        a categoria <b>${ultimaAvaliacaoNacionalHist.deCategoriaIucn}</b>, oficializada nas Portarias MMA
                    <a target="_blank" href="http://pesquisa.in.gov.br/imprensa/jsp/visualiza/index.jsp?jornal=1&pagina=121&data=18/12/2014">444/2014</a> ou
                    <a target="_blank" href="http://pesquisa.in.gov.br/imprensa/jsp/visualiza/index.jsp?jornal=1&pagina=126&data=18/12/2014">445/2014</a>.
                    </small>
                </g:if>--}%

            </div>
                <g:if test="${ false && imagemCategoriaIucn }">
                    <div style="font-size:14px !important;background-color: #beefc696;border-radius: 5px;padding:3px;">
                        <span style="cursor:help" title="${hintImagemCategoriaIucn}">
                             <img alt="${categoriaFinal}" src="${imagemCategoriaIucn}" width="200px" height="53px">
                        </span>
                    </div>
                </g:if>
            </g:if>
            <g:else>
                <div>
                    <span>Categoria: <b class="text-warning">Não avaliada</b></span>
                </div>
                <div style="background-color: #beefc696;border-radius: 5px;">
                    <span style="cursor:help" title="Não avaliada">
                        <img alt="Não avaliada" src="/salve-estadual/assets/public/iucn/ne.png">
                    </span>
                </div>
            </g:else>


        </div>
    </div>
    <g:if test="${categoriaFoiAlterada}">
        <div class="texto-mudanca-categoria">
            <small class="mr-2 text-justify">${textoDiferencaCategorias}
%{--            <small class="mr-2 text-justify"><i class="fa fa-asterisk"></i>&nbsp;Essa categoria é resultado da avaliação mais recente da espécie e está sendo disponibilizada para subsidiar eventuais ações de conservação que a beneficiem. Entretanto, essa categoria ainda não foi oficializada por meio de Portaria do MMA. Para fins legais, deve-se utilizar a categoria <strong>${ultimaAvaliacaoNacionalHist.deCategoriaIucn}</strong>, oficializada nas Portarias MMA--}%
%{--            <a target="_blank" href="http://pesquisa.in.gov.br/imprensa/jsp/visualiza/index.jsp?jornal=1&pagina=121&data=18/12/2014">444/2014</a> ou--}%
%{--            <a target="_blank" href="http://pesquisa.in.gov.br/imprensa/jsp/visualiza/index.jsp?jornal=1&pagina=126&data=18/12/2014">445/2014</a>.--}%
            </small>
        </div>
    </g:if>

    %{--CLASSIFICAÇÃO TAXONÔMICA--}%
    <div class="card card-collapse mt-3">
        <div class="card-header dynamic" data-toggle="collapse" data-target="#cardBodyClassificacaoTaxonomica-${sqFichaEncrypted}">
            <i class="fa fa-plus icon-state"></i>
            <span>Classificação Taxonômica</span>
        </div>
        <div class="collapse-wrapper collapse" id="cardBodyClassificacaoTaxonomica-${sqFichaEncrypted}"
            data-loaded="false"
            data-section="CLASSIFICACAO_TAXONOMICA"
        >
            <div class="card-body content-ajax">
                <!-- ajax -->
            </div> %{-- FIM CARD BODY --}%
        </div> %{-- FIM COLLAPSE --}%
    </div> %{-- FIM CARD CLASSIFICACAO TAXONOMICA--}%

    %{-- DISTRIBUIÇÃO --}%
    <div class="card card-collapse mt-3">
        <div class="card-header dynamic" data-toggle="collapse" data-target="#cardBodyDistribuicao-${sqFichaEncrypted}">
            <i class="fa fa-plus icon-state"></i>
            <span>Distribuição</span>
        </div>
        <div class="collapse-wrapper collapse" id="cardBodyDistribuicao-${sqFichaEncrypted}"
             data-loaded="false"
             data-section="DISTRIBUICAO">
            <div class="card-body content-ajax">
                <!-- ajax -->
            </div> %{-- FIM CARD BODY --}%
        </div> %{-- FIM COLLAPSE --}%
    </div> %{-- FIM CARD DISTRIBUIÇÃO --}%

    %{-- HISTÓRIA NATURAL --}%
    <div class="card card-collapse mt-3">
        <div class="card-header dynamic" data-toggle="collapse" data-target="#cardBodyHistoriaNatural-${sqFichaEncrypted}">
            <i class="fa fa-plus icon-state"></i>
            <span>História Natural</span>
        </div>
        <div class="collapse-wrapper collapse" id="cardBodyHistoriaNatural-${sqFichaEncrypted}"
             data-loaded="false"
             data-section="HISTORIA_NATURAL">
            <div class="card-body content-ajax">
                <!-- ajax -->
            </div> %{-- FIM CARD BODY--}%
        </div>
    </div>%{-- FIM HISTÓRIA NATURAL --}%

    %{-- POPULAÇÃO --}%
    <div class="card card-collapse mt-3">
        <div class="card-header dynamic" data-toggle="collapse" data-target="#cardBodyPopulacao-${sqFichaEncrypted}">
            <i class="fa fa-plus icon-state"></i>
            <span>População</span>
        </div>
        <div class="collapse-wrapper collapse" id="cardBodyPopulacao-${sqFichaEncrypted}"
             data-loaded="false"
             data-section="POPULACAO">
            <div class="card-body content-ajax">
                <!-- ajax -->
            </div> %{-- FIM CARD BODY--}%
        </div>
    </div>


    %{-- AMEAÇA --}%
    <div class="card card-collapse mt-3">
        <div class="card-header dynamic" data-toggle="collapse" data-target="#cardBodyAmeaca-${sqFichaEncrypted}">
            <i class="fa fa-plus icon-state"></i>
            <span>Ameaça</span>
        </div>
        <div class="collapse-wrapper collapse" id="cardBodyAmeaca-${sqFichaEncrypted}"
             data-loaded="false"
             data-section="AMEACA">
            <div class="card-body content-ajax">
                <!-- ajax -->
            </div> %{-- FIM CARD BODY--}%
        </div>
    </div>

    %{-- USO --}%
    <div class="card card-collapse mt-3">
        <div class="card-header dynamic" data-toggle="collapse" data-target="#cardBodyUso-${sqFichaEncrypted}">
            <i class="fa fa-plus icon-state"></i>
            <span>Uso</span>
        </div>
        <div class="collapse-wrapper collapse" id="cardBodyUso-${sqFichaEncrypted}"
             data-loaded="false"
             data-section="USO">
            <div class="card-body content-ajax">
                <!-- ajax -->
            </div> %{-- FIM CARD BODY--}%
        </div>
    </div>

    %{-- CONSERVAÇÃO --}%
    <div class="card card-collapse mt-3">
        <div class="card-header dynamic" data-toggle="collapse" data-target="#cardBodyConservacao-${sqFichaEncrypted}">
            <i class="fa fa-plus icon-state"></i>
            <span>Conservação</span>
        </div>
        <div class="collapse-wrapper collapse" id="cardBodyConservacao-${sqFichaEncrypted}"
             data-loaded="false"
             data-section="CONSERVACAO">
            <div class="card-body content-ajax">
                <!-- ajax -->
            </div> %{-- FIM CARD BODY--}%
        </div>
    </div>

    %{-- PESQUISA --}%
    <div class="card card-collapse mt-3">
        <div class="card-header dynamic" data-toggle="collapse" data-target="#cardBodyPesquisa-${sqFichaEncrypted}">
            <i class="fa fa-plus icon-state"></i>
            <span>Pesquisa</span>
        </div>
        <div class="collapse-wrapper collapse" id="cardBodyPesquisa-${sqFichaEncrypted}"
             data-loaded="false"
             data-section="PESQUISA">
            <div class="card-body content-ajax">
                <!-- ajax -->
            </div> %{-- FIM CARD BODY--}%
        </div>
    </div>

    %{-- REFERÊNCIA BIBLIOGRÁFICA --}%
    <div class="card card-collapse mt-3">
        <div class="card-header dynamic" data-toggle="collapse" data-target="#cardBodyRefBibliografica-${sqFichaEncrypted}">
            <i class="fa fa-plus icon-state"></i>
            <span>Referências Bibliográficas</span>
        </div>
        <div class="collapse-wrapper collapse" id="cardBodyRefBibliografica-${sqFichaEncrypted}"
             data-loaded="false"
             data-section="REFERENCIA_BIBLIOGRAFICA">
            <div class="card-body content-ajax">
                <!-- ajax -->
            </div> %{-- FIM CARD BODY--}%
        </div>
    </div>

    %{-- AUTORIA --}%
    <div class="card card-collapse mt-3">
        <div class="card-header dynamic" data-toggle="collapse" data-target="#cardBodyAutoria-${sqFichaEncrypted}">
            <i class="fa fa-minus icon-state"></i>
            <span>Autoria</span>
        </div>
        <div class="collapse-wrapper collapse show" id="cardBodyAutoria-${sqFichaEncrypted}"
             data-loaded="true"
             data-section="AUTORIA">
            <div class="card-body content-ajax">
                <p>${raw(autoria)}</p>
            </div> %{-- FIM CARD BODY--}%
        </div>
    </div>

    %{-- COMO CITAR --}%
    <div class="card card-collapse mt-3">
        <div class="card-header dynamic" data-toggle="collapse" data-target="#cardBodyComoCitar-${sqFichaEncrypted}">
            <i class="fa fa-plus icon-state"></i>
            <span>Como citar</span>
        </div>
        <div class="collapse-wrapper collapse" id="cardBodyComoCitar-${sqFichaEncrypted}"
             data-loaded="true"
             data-section="CITACAO">
            <div class="card-body content-ajax">
                <p>${raw(comoCitar)}</p>
            </div> %{-- FIM CARD BODY--}%
        </div>
    </div>
</div> %{--FIM CONTAINER--}%
