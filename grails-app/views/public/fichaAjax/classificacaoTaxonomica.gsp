<g:each var="item" in="${ listClassificacaoTaxonomica }" >
    <div class="row">
        <div class="col-2" style="max-width: 100px;">
            <span class="font-weight-bold">${ item?.nivel }</span>
        </div>
        <div class="col">
            <span>${ item?.descricao }</span>
        </div>
    </div>
</g:each>

%{--NOMES COMUNS--}%
<div class="row mt-2">
    <div class="col title-bold">
        <span>Nomes comuns</span>
    </div>
</div>
<div class="row">
    <div class="col">
        <span>${ nomesComuns }</span>
    </div>
</div>

%{-- NOMES ANTIGOS --}%
<div class="row mt-2">
    <div class="col title-bold">
        <span>Nomes antigos</span>
    </div>
</div>
<div class="row mt-2">
    <div class="col">
        <span>${ raw( nomesAntigos ) }</span>
    </div>
</div>

%{-- NOTAS TAXONOMICAS --}%
<div class="row mt-2">
    <div class="col title-bold">
        <span>Notas taxonômicas</span>
    </div>
</div>
<div class="row">
    <div class="col">
        <span>${ raw( notasTaxonomicas ) }</span>
    </div>
</div>

%{-- NOTAS MORFOLOGICAS --}%
<g:if test="${ raw( notasMorfologicas ) }">
    <div class="row">
        <div class="col title-bold">
            <span>Notas morfológicas</span>
        </div>
    </div>
    <div class="row">
        <div class="col box-texto">
            <span>${ raw( notasMorfologicas ) }</span>
        </div>
    </div>
</g:if>