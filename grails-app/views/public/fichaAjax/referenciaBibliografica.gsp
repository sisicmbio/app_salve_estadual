%{-- REFERÊNCIAS BIBLIOGRÁFICAS --}%
<g:if test="${ listReferencia }">
    <div class="row">
        <div class="col box-texto">
            <g:each var="item" in="${ listReferencia }">
                <p>${ raw( item.descricao ) }</p>
            </g:each>
        </div>
    </div>
</g:if>