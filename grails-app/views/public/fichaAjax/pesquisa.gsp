%{--  PESQUISA--}%
<g:if test="${listPesquisa}">
    <div class="row">
        <div class="col">
            <div class="table-responsive-md">
                <table class="table table-sm table-bordered table-condensed table-hover table-striped">
                    <thead class="thead-dark">
                    <tr class="text-center">
                        <th>Tema</th>
                        <th>Situação</th>
                        <th class="text-center">Referência bibliográfica</th>
                    </tr>
                    </thead>
                    <tbody>
                    <g:each var="item" in="${ listPesquisa }">
                        <tr>
                            <td>${ raw( item.descricao ) }</td>
                            <td class="text-center">${ raw( item.situacao ) }</td>
                            <td>${ raw( item.refBibHtml ) }</td>
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</g:if>

%{-- PESQUISA - OBSERVACAO --}%
<g:if test="${ obsPesquisa }">
    <div class="row">
        <div class="col box-texto">
            <span>${ raw( obsPesquisa ) }</span>
        </div>
    </div>
</g:if>