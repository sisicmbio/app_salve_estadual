%{-- TABLE AMEAÇAS--}%
<g:if test="${ listAmeaca }">
    <div class="row">
        <div class="col">
            <div class="table-responsive-md">
                <table class="table table-sm table-bordered table-condensed table-hover table-striped">
                    <thead class="thead-dark">
                    <tr>
                        <th>Ameaça</th>
                        <th class="text-center">Referência bibliográfica</th>
                    </tr>
                    </thead>
                    <tbody>
                    <g:each var="item" in="${listAmeaca}">
                        <tr>
                            <td>${raw( item.descricao ) }</td>
                            <td>${raw( item.refBibHtml ) }</td>
                        </tr>
                        <g:if test="${item.regioes}">
                            <tr>
                                <td colspan="2" style="padding-left:35px;background-color: #0c5460;">
                                    <span>${ item.regioes} </span>
                                </td>
                            </tr>
                        </g:if>
                    </g:each>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</g:if>
%{-- AMEACA - OBSERVACÃO --}%
%{--                <div class="row">
                    <div class="col titulo-negrito">
                        <span>Observaçãoes sobre a ameaça</span>
                    </div>
                </div>--}%
<g:if test="${ obsAmeaca }">
    <div class="row">
        <div class="col box-texto">
            <span>${ raw( obsAmeaca ) }</span>
        </div>
    </div>
</g:if>

%{-- AMEAÇA - IMAGENS--}%
<g:if test="${ listImagemAmeaca }">
    <div class="row">
        <div class="col d-flex flex-column flex-md-row justify-content-center align-items-center justify-content-lg-between mt-3" >
            <g:each var="imagem" in="${ listImagemAmeaca }">
                <a href="${imagem.url}"
                   data-lightbox="image-ameaca"
                   data-title="${imagem.legenda}">
                    <div class="card mr-2" style="width: auto">
                        <img class="card-img-top" alt="imagem"
                             src="${ imagem.url }"/>
                        <div class="card-body">
                            <h6 class="text-legend">${ imagem.legenda }</h6>
                        </div>
                    </div>
                </a>
            </g:each>
        </div>
    </div>
</g:if>
