%{-- ENDEMICA BRASIL--}%
<div class="row">
    <div class="col">
        <span>Endêmica do Brasil?&nbsp;<b>${endemicaBrasil}</b></span>
    </div>
</div>

%{-- DISTRIBUICAO GLOBAL--}%
<div class="row mt-2">
    <div class="col title-bold">
        <span>Distribuição global</span>
    </div>
</div>
<div class="row">
    <div class="col">
        <span>${ raw( distribuicaoGlobal ) }</span>
    </div>
</div>

%{-- DISTRIBUICAO NACIONAL--}%
<g:if test="${ raw( distribuicaoNacional ) }">
    <div class="row mt-2">
        <div class="col title-bold">
            <span>Distribuição nacional</span>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <span>${ raw( distribuicaoNacional ) }</span>
        </div>
    </div>
</g:if>

%{-- ESTADOS --}%
<div class="row">
    <div class="col title-bold">
        <span>Estados</span>
    </div>
</div>
<div class="row">
    <div class="col">
        <span>${ raw( estados ) }</span>
    </div>
</div>

%{-- BIOMAS --}%
<div class="row mt-2">
    <div class="col title-bold">
        <span>Biomas</span>
    </div>
</div>
<div class="row">
    <div class="col">
        <span>${ raw( biomas ) }</span>
    </div>
</div>

%{-- BACIAS HIDROGRAFICAS --}%
<g:if test="${ raw( baciasHidrograficas ) }">
    <div class="row mt-2">
        <div class="col title-bold">
            <span>Bacias hidrográficas</span>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <span>${ raw( baciasHidrograficas ) }</span>
        </div>
    </div>
</g:if>

%{-- AREAS RELEVANTES --}%
<g:if test="${ listAreasRelevantes }">
    <div class="row mt-2">
        <div class="col title-bold">
            <span>Áreas relevantes</span>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="table-responsive-md">
                <table class="table table-sm table-bordered table-condensed table-hover table-striped">
                    <thead class="thead-dark">
                    <tr>
                        <th>Tipo</th>
                        <th>Local</th>
                        <th>Estado</th>
                        <th>Município</th>
                        <th>Ref. bibliográfica</th>
                    </tr>
                    </thead>
                    <tbody>
                    <g:each var="item" in="${listAreasRelevantes}">
                        <tr>
                            <td>${raw( item?.tipoRelevancia?.descricao ) }</td>
                            <td>${raw( item.localHtml ) }</td>
                            <td>${raw( item.estado.noEstado ) }</td>
                            <td>${raw( item.municipiosHtml ) }</td>
                            <td>${raw( item.refBibHtml ) }</td>
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</g:if>

%{-- IMAGEM DO MAPA DE DISTRIBUIÇÃO --}%
<div class="row">
    <div class="col title-bold">
        <span>Mapa de distribuição - Imagem</span>
    </div>
</div>
<div class="row">
    <div class="col">
        <g:if test="${imagemDistribuicao}">
            <div class="card w-100">
                <a data-lightbox="image-distribuicao" href="${imagemDistribuicao.url}"
                    data-title="${ imagemDistribuicao.legenda }">
                    <img class="img-responsive img-fluid"
                     src="${imagemDistribuicao.url}" alt="${imagemDistribuicao.legenda}"/>
                </a>
                <div class="card-body">
                    <h6 class="text-legend">${ imagemDistribuicao.legenda }</h6>
                </div>
            </div>
        </g:if>
        <g:else>
            <span>Sem imagem</span>
        </g:else>
    </div>
</div>

%{-- MAPA--}%
<div class="row">
    <div class="col title-bold">
        <span>Mapa de distribuição - Online</span>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="card w-100">
            <div class="map" style="width:100%;" id="onlineMap"></div>
            <div class="mapa-disclaimer">O mapa online pode conter registros distintos daqueles da imagem do mapa anexado a ficha.
                <span class="mapa-saiba-mais cursor-pointer" onclick="saibaMaisMapa()" title="Clique para saber mais">Saiba mais...</span>
            </div>
        </div>
    </div>
</div>