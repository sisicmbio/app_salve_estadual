%{-- CONSERVAÇÃO - ÚLTIMA AVALIAÇÃO NACIONAL --}%
%{--

<g:if test="${ ultimaAvaliacao.categoria }">
    <div class="row">
        <div class="col title-bold">
            <span>Última avaliação nacional</span>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <span>Ano:&nbsp;<b>${ ultimaAvaliacao.ano }</b></span>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <span>Categoria:&nbsp;<b>${ ultimaAvaliacao.categoria }</b></span>
        </div>
    </div>
    <g:if test="${ultimaAvaliacao.criterio}">
        <div class="row">
            <div class="col">
                <span>Critério:&nbsp;<b>${ ultimaAvaliacao.criterio }</b></span>
            </div>
        </div>
    </g:if>
    <div class="row">
        <div class="col box-texto mt-2">
            <span>${ raw( ultimaAvaliacao.justificativa ) }</span>
        </div>
    </div>
</g:if>
--}%

%{-- CONSERVAÇÃO - TABELA DOS HISTORICOS DE AVALIAÇÃO--}%
<g:if test="${ listHistoricoAvaliacao }">
    <div class="row">
        <div class="col mt-3">
            <div class="table-responsive-md">
                <table class="table table-sm table-bordered table-condensed table-hover table-striped">
                    <thead class="thead-dark">
                    <tr class="text-center">
                        <th>Tipo</th>
                        <th>Ano</th>
                        <th>Abrangência</th>
                        <th>Categoria</th>
                        <th>Critério</th>
                        <th>Referência Biobliográfica</th>
                    </tr>
                    </thead>
                    <tbody>
                    <g:each var="item" in="${listHistoricoAvaliacao}">
                        <tr class="text-center ${ item.tipoCodigo == 'NACIONAL_BRASIL' ? 'text-warning' : '' }">
                            <td class="${ item.tipoCodigo == 'NACIONAL_BRASIL' ? 'text-bold' : '' }">${raw( item?.tipo ) }</td>
                            <td>${raw( item.ano ) }</td>
                            <td>${raw( item.abrangencia ) }</td>
                            <td>${raw( item.categoria + ( item.existeJustificativa ? '&nbsp;<span onclick="lerJustificativa(\''+item.sqTaxonHistoricoAvaliacao+'\');" class="cursor-pointer fa fa-info-circle" title="Ver a justificativa"></span>':'') )}
                            </td>
                            <td>${raw( item.criterio ) }</td>
                            <td>${raw( item.refBibHtml ) }</td>
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</g:if>

%{-- CONSERVACAO - Presença em Lista Nacional Oficial de Espécies Ameaçadas de Extinção--}%
<div class="row">
    <div class="col mt-2">
        <span>Presença em lista nacional oficial de espécies ameaçadas de extinção:&nbsp;<b>${ presencaListaVigente }</b></span>
    </div>
</div>

%{-- CONSERVAÇÃO - Presença em convenção --}%
<g:if test="${ listConvencao }">
    <div class="row">
        <div class="col mt-2 title-bold">
            <span>Presença em convenção</span>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="table-responsive-md">
                <table class="table table-sm table-bordered table-condensed table-hover table-striped">
                    <thead class="thead-dark">
                    <tr class="text-center">
                        <th>Convenção</th>
                        <th>Ano</th>
                    </tr>
                    </thead>
                    <tbody>
                    <g:each var="item" in="${ listConvencao }">
                        <tr>
                            <td>${ raw( item.descricao ) }</td>
                            <td class="text-center">${ item.ano }</td>
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</g:if>


%{-- CONSERVACAO - Ações de Conservação --}%
<g:if test="${ listAcaoConservacao }">
    <div class="row">
        <div class="col mt-2 title-bold">
            <span>Ações de Conservação</span>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="table-responsive-md">
                <table class="table table-sm table-bordered table-condensed table-hover table-striped">
                    <thead class="thead-dark">
                    <tr class="text-center">
                        <th>Ação</th>
                        <th>Situação</th>
                        <th>Referência bibilográfica</th>
                    </tr>
                    </thead>
                    <tbody>
                    <g:each var="item" in="${ listAcaoConservacao }">
                        <tr>
                            <td>${ raw( item.acao ) }</td>
                            <td class="text-center">${ item.situacao }</td>
                            <td class="text-center">${ raw( item.refBibHtml ) }</td>
                        </tr>
                        <g:if test="${item.planoAcao}">
                            <tr>
                                <td colspan="3" style="padding-left:35px;background-color: #0c5460;">
                                    <span>-&nbsp;${ raw( item.planoAcao ) }</span>
                                </td>
                            </tr>
                        </g:if>
                    </g:each>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</g:if>

<g:if test="${ obsAcaoConservacao }">
    <div class="row">
        <div class="col box-texto mt-2">
            <span>${ raw( obsAcaoConservacao ) }</span>
        </div>
    </div>
</g:if>

%{-- CONSERVACAO - Presença em UC --}%

<g:if test="${ listUc }">
    <div class="row mt-2">
        <div class="col mt-2 title-bold">
            <span class="text-bold">Presença em UC</span>
            %{--<g:if test="${listUc}">
                <span><b>Sim</b></span>
            </g:if>--}%
        </div>
    </div>

    <div class="row">
        <div class="col mt-2">
            <div class="table-responsive-md">
                <table class="table table-sm table-bordered table-condensed table-hover table-striped">
                    <thead class="thead-dark">
                    <tr class="text-center">
                        <th>UC</th>
                        <th>Referência bibliográfica</th>
                    </tr>
                    </thead>
                    <tbody>
                    <g:each var="item" in="${ listUc }">
                        <tr>
                            <td>${ raw( item.descricao ) }</td>
                            <td>${ raw( item.refBibHtml) }</td>
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</g:if>

%{-- CONSERVACAO - OBS PRESENCA EM UC--}%
<g:if test="${ obsPresencaUc }">
    <div class="row">
        <div class="col box-texto">
            <span>${ raw( obsPresencaUc ) }</span>
        </div>
    </div>
</g:if>
