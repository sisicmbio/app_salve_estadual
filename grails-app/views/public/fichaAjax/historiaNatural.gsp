%{-- HISTÓRIA NATURAL --}%

%{-- ESPÉCIE MIGRATORIA --}%
<g:if test="${ especieMigratoria }">
    <div class="row">
        <div class="col">
            <span>Espécie migratória ?&nbsp;<b>${especieMigratoria}</b></span>
        </div>
    </div>
</g:if>

%{--TEXTO HISTÓRIA NATURAL --}%
<div class="row mt-2">
    <div class="col" class="text-justify">
        ${ raw( historiaNatural ) }
    </div>
</div>

%{-- HABITO ALIMETAR --}%
<g:if test="${ listHabitoAlimentar }">
    <div class="row">
        <div class="col title-bold">
            <span>Hábito alimentar</span>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="table-responsive-md">
                <table class="table table-sm table-bordered table-condensed table-hover table-striped">
                    <thead class="thead-dark">
                    <tr>
                        <th>Tipo</th>
                        <th>Ref. bibliográfica</th>
                    </tr>
                    </thead>
                    <tbody>
                    <g:each var="item" in="${listHabitoAlimentar}">
                        <tr>
                            <td>${raw( item?.tipoHabitoAlimentar?.descricao) }</td>
                            <td>${raw( item.refBibHtml ) }</td>
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</g:if>

%{-- HABITO ALIMETAR ESPECIALISTA --}%
<g:if test="${listHabitoAlimentarEspecialista}">
    <div class="row">
        <div class="col">
            <span>Hábito alimentar especialista?&nbsp;<b>${habitoAlimentarEspecialista}</b></span>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <div class="table-responsive-md">
                <table class="table table-sm table-bordered table-condensed table-hover table-striped">
                    <thead class="thead-dark">
                    <tr>
                        <th>Tipo</th>
                        <th>Categoria</th>
                    </tr>
                    </thead>
                    <tbody>
                    <g:each var="item" in="${ listHabitoAlimentarEspecialista }">
                        <tr>
                            <td>${ raw( item?.taxon?.noCientifico ) }</td>
                            <td>${ raw( item?.categoriaIucn?.descricaoCompleta ) }</td>
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</g:if>

%{-- OBSERVAÇÕES SOBRE O HABITO ALIMETAR --}%
<g:if test="${obsHabitoAlimentar}">
    <div class="row">
        <div class="col title-bold">
            <span>Observações sobre o habito alimentar</span>
        </div>
    </div>
    <div class="row">
        <div class="col box-texto">
            <span>${raw( obsHabitoAlimentar ) }</span>
        </div>
    </div>
</g:if>

%{-- RESTRITO A HABITAT PRIMÁRIO    --}%
<g:if test="${ restritoHabitatPrimario }">
    <div class="row mt-3">
        <div class="col">
            <span>Restrito a habitat primário?&nbsp;<b>${restritoHabitatPrimario}</b></span>
        </div>
    </div>
</g:if>

%{-- ESPECIALISTA EM MICRO HABITAT --}%
<g:if test="${especialistaMicroHabitat}">
    <div class="row">
        <div class="col">
            <span>Especialista em micro habitat?&nbsp;<b>${especialistaMicroHabitat}</b></span>
        </div>
    </div>
</g:if>

%{-- OBSERVAÇÕES SOBRE ESPECIALISTA MICRO HABITAT --}%
<g:if test="${especialistaMicroHabitat}">
    <div class="row mt-2">
        <div class="col title-bold">
            <span>Micro habitat</span>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <span>${raw( obsEspecialistaMicroHabitat ) }</span>
        </div>
    </div>
</g:if>

%{-- OBSERVAÇÃO GERAL SOBRE O HABITAT--}%
<g:if test="${obsHabitat}">
    <div class="row mt-2">
        <div class="col title-bold">
            <span>Observações sobre o habitat</span>
        </div>
    </div>
    <div class="row">
        <div class="col box-texto">
            <span>${raw( obsHabitat ) }</span>
        </div>
    </div>
</g:if>

%{-- INTERAÇÕES COM OUTRAS ESPÉCIES --}%
<g:if test="${listInteracoes}">
    <div class="row mt-2">
        <div class="col title-bold">
            <span>Interações com outras espécies</span>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="table-responsive-md">
                <table class="table table-sm table-bordered table-condensed table-hover table-striped">
                    <thead class="thead-dark">
                    <tr>
                        <th>Tipo</th>
                        <th>Taxon</th>
                        <th>Categoria</th>
                        <th>Ref. bibliográfica</th>
                    </tr>
                    </thead>
                    <tbody>
                    <g:each var="item" in="${listInteracoes}">
                        <tr>
                            <td>${raw( item?.tipoInteracao?.descricao ) }</td>
                            <td>${raw( item?.taxon?.noCientifico ) }</td>
                            <td>${raw( item?.categoriaIucn?.descricaoCompleta ) }</td>
                            <td>${raw( item?.refBibHtml ) }</td>
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</g:if>

%{-- REPRODUÇÃO - INTERVALO DE NASCIMENTO --}%
<g:if test="${ intervaloNascimento }">
    <div class="row">
        <div class="col">
            <span>Intervalo de nascimentos:&nbsp;<b>${ raw( intervaloNascimento ) }</b></span>
        </div>
    </div>
</g:if>

%{-- REPRODUÇÃO - Tempo de gestação --}%
<g:if test="${tempoGestacao}">
    <div class="row">
        <div class="col">
            <span>Tempo de gestação:&nbsp;<b>${ raw( tempoGestacao ) }</b></span>
        </div>
    </div>
</g:if>

%{-- REPRODUÇÃO - Tamanho da prole --}%
<g:if test="${tamanhoProle}">
    <div class="row">
        <div class="col">
            <span>Tamanho da prole:&nbsp;<b>${ raw( tamanhoProle ) }</b></span>
        </div>
    </div>
</g:if>

%{-- REPRODUÇÃO - Quadro macho e femea --}%
<g:if test="${ listMachoFemea }">
    <div class="row">
        <div class="col">
            <div class="table-responsive-md mt-3" style="max-width: 600px;">
                <table class="table table-sm table-bordered table-condensed table-hover table-striped">
                    <thead class="thead-dark text-center">
                    <tr>
                        <th rowspan="2">Campo</th>
                        <th colspan="2">Macho</th>
                        <th colspan="2">Fêmea</th>
                    </tr>
                    <tr>
                        <th>Valor</th>
                        <th>Unidade</th>
                        <th>Valor</th>
                        <th>Unidade</th>
                    </tr>
                    </thead>
                    <tbody>
                    <g:each var="item" in="${listMachoFemea}">
                        <tr class="text-center">
                            <td class="text-left">${raw( item.campo ) }</td>
                            <td>${raw( item.macho.valor ) }</td>
                            <td>${raw( item.macho.unidade ) }</td>
                            <td>${raw( item.femea.valor ) }</td>
                            <td>${raw( item.femea.unidade ) }</td>
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</g:if>

%{-- REPRODUÇÃO - OBSERVAÇÕES --}%
<g:if test="${ dsReproducao }">
    <div class="row">
        <div class="col title-bold">
            <span>Observações sobre a reprodução</span>
        </div>
    </div>
    <div class="row">
        <div class="col box-texto">
            <span>${ raw( dsReproducao ) }</span>
        </div>
    </div>
</g:if>