%{-- TABLE USOS--}%
<g:if test="${ listUso }">
    <div class="row">
        <div class="col">
            <div class="table-responsive-md">
                <table class="table table-sm table-bordered table-condensed table-hover table-striped">
                    <thead class="thead-dark">
                    <tr>
                        <th>Uso</th>
                        <th class="text-center">Referência bibliográfica</th>
                    </tr>
                    </thead>
                    <tbody>
                    <g:each var="item" in="${listUso}">
                        <tr>
                            <td>${raw( item.descricao ) }</td>
                            <td>${raw( item.refBibHtml ) }</td>
                        </tr>
                        <g:if test="${item.regioes}">
                            <tr>
                                <td colspan="2" style="padding-left:35px;background-color: #0c5460;">
                                    <span>${ item.regioes} </span>
                                </td>
                            </tr>
                        </g:if>
                    </g:each>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</g:if>
%{-- USO - OBSERVACÃO --}%
%{--                <div class="row">
                    <div class="col titulo-negrito">
                        <span>Observaçãoes sobre o uso</span>
                    </div>
                </div>--}%

<g:if test="${obsUso}">
    <div class="row">
        <div class="col box-texto">
            <span>${ raw( obsUso ) }</span>
        </div>
    </div>
</g:if>
%{-- USO - IMAGENS--}%
<g:if test="${ listImagemUso }">
    <div class="row">
        <div class="col d-flex flex-column flex-md-row justify-content-center align-items-center justify-content-lg-between mt-3" >
            <g:each var="imagem" in="${ listImagemUso }">
                <a href="${imagem.url}"
                   data-lightbox="image-uso"
                   data-title="${imagem.legenda}">
                    <div class="card mr-2" style="width: auto;">
                        <img class="card-img-top" alt="imagem"
                             src="${ imagem.url }"/>
                        <div class="card-body">
                            <h6 class="text-legend">${ imagem.legenda }</h6>
                        </div>
                    </div>
                </a>
            </g:each>
        </div>
    </div>
</g:if>