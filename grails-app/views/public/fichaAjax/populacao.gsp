%{-- POPULAÇÃO - TENDÊNCIA POPULACIONAL --}%
<div class="row">
    <div class="col">
        <span>Tendência populacional:&nbsp;<b>${tendenciaPopulacional}</b></span>
    </div>
</div>

%{-- POPULAÇÃO - RAZÃO SEXUAL  --}%
<g:if test="${ razaoSexual }">
    <div class="row">
        <div class="col">
            <span>Razão sexual:&nbsp;<b>${ razaoSexual }</b></span>
        </div>
    </div>
</g:if>

%{-- POPULAÇÃO - Taxa de mortalidade natural --}%
<g:if test="${ taxaMortalidadeNatural }">
    <div class="row">
        <div class="col">
            <span>Taxa de mortalidade natural:&nbsp;<b>${ taxaMortalidadeNatural }</b></span>
        </div>
    </div>
</g:if>

%{-- POPULAÇÃO - Características Genéticas  --}%
<g:if test="${ caracteristicaGenetica }">
    <div class="row mt-2">
        <div class="col title-bold">
            <span>Características Genéticas</span>
        </div>
    </div>
    <div class="row">
        <div class="col box-texto">
            <span>${ raw( caracteristicaGenetica ) }</span>
        </div>
    </div>
</g:if>

%{-- POPULAÇÃO - OBSERVAÇÕES  --}%
<g:if test="${ obsPopulacao }">
    <div class="row mt-2">
        <div class="col title-bold">
            <span>Observações sobre a população</span>
        </div>
    </div>
    <div class="row">
        <div class="col box-texto">
            <span>${ raw( obsPopulacao ) }</span>
        </div>
    </div>
</g:if>

%{-- POPULAÇÃO - IMAGENS--}%
<g:if test="${ listImagemPopulacao }">
    <div class="row">
        <div class="col d-flex flex-column flex-md-row justify-content-center align-items-center justify-content-lg-between mt-3" >
            <g:each var="imagem" in="${listImagemPopulacao}">
                <a href="${imagem.url}"
                   data-lightbox="image-populacao"
                   data-title="${imagem.legenda}">
                    <div class="card" style="width: 18rem;">
                        <img class="card-img-top" alt="imagem"
                             src="${ imagem.url }"/>
                        <div class="card-body">
                            <h6 class="text-legend">${ imagem.legenda }</h6>
                        </div>
                    </div>
                </a>
            </g:each>
        </div>
    </div>
</g:if>