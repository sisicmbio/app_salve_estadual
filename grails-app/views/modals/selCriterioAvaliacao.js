//# sourceURL=N:\SAE\sae\grails-app\views\modal\selCriterioAvaliacao.js
// $(".fancytree-container").toggleClass("fancytree-connectors",true)

 /*$("#btnSelectAll").click(function(){
            $("#tree2").fancytree("getTree").visit(function(node){
                node.setSelected(true);
            });
            return false;
        });
*/
var criarCodigo=function()
{
    function loopNodes(nodes)
    {
        var resultado='';
        var grupo;
        var sinal;
        var value;
        var g4='';
        $.each(nodes, function(k,node)
        {
            var level=node.getLevel();
            if( resultado == "" )
            {
                resultado=node.key;
            }
            else
            {
                value = node.data.value;
                sinal = '';
                grupo = '';
                if( level==2 ) // D2
                {
                    if( node.key.indexOf( resultado ) > - 1)
                    {
                        resultado=node.key;
                        value='';
                    }
                    else
                    {
                        sinal='+'
                    }
                }
                else if( level==3 ) // A2a
                {
                    if( node.key.indexOf( resultado ) > - 1)
                    {
                        resultado=node.key;
                        value='';
                    }
                    else
                    {
                        sinal='+'
                        grupo=node.key.substring(2,1); // 2
                    }
                }
                else if( level==4 ) // C1ai  "B1bicii,iii,iv"
                {
                        //grupo = node.key.substring(1,0) // 'C'
                        pai = node.key.substring(2,1)   // 2
                        if( resultado.indexOf(pai) == -1)
                        {
                            grupo ='';
                            value = node.key.substring(1); // c
                            sinal = '+'
                        }
                        else
                        {
                            texto = resultado.substring( resultado.indexOf(pai) );
                            filho = node.key.substring(2,3)   // b
                            if( texto.indexOf(filho) == -1)
                            {
                                grupo ='';
                                value = node.key.substring(2); // b
                                sinal = ''
                            }
                        }

                }
                if( grupo && resultado.indexOf(grupo) > -1 )
                {
                    resultado+=value;
                }
                else
                {
                    resultado+=sinal+grupo+value;
                }
            }
        })
        return resultado.replace(/\)\(/g,',');
    }
    var fld=$("#codigo");
    var resultado='';
    var parcial='';
    $.each(["A", "B", "C", "D", "E"], function(k,v)
    {   parcial = loopNodes( $("#tree"+v).fancytree("getTree").getSelectedNodes() ) ;
        if( parcial )
        {
            resultado+=( resultado==""?"":"; ")+parcial ;
        }
    });
    /*var codigoA=loopNodes( $("#treeA").fancytree("getTree").getSelectedNodes() );
    var codigoB=loopNodes( $("#treeB").fancytree("getTree").getSelectedNodes() );
    var codigoC=loopNodes( $("#treeC").fancytree("getTree").getSelectedNodes() );
    var codigoD=loopNodes( $("#treeD").fancytree("getTree").getSelectedNodes() );
    var codigoE=loopNodes( $("#treeE").fancytree("getTree").getSelectedNodes() );
    */
    $('#divCriterioResultado').html( resultado );
 }

 var updateTrees=function()
 {
    var ids=[];
    var arvores;
    var folhas=['a','b','c','d','e'];
    var n1,n2
    var troncos;
    var pos;
    var tree;
    var node;
    var criterio = $('#divCriterioResultado').text().trim()

    // processar cada arvore
    if( criterio )
    {
        arvores = criterio.split('; ');
        $.each( arvores, function(k0,raiz)
        {
            //console.log( arvore );
            n1=raiz.substring(0,1); // A, B, C, D, E
            tree = $("#tree"+n1).fancytree("getTree");
            $.each( tree.getSelectedNodes(),function(k,node){
                node.setActive(false);
                node.setSelected( false);
            });

            //console.log( n1 );
            //ids.push( n1 );
            node = tree.getNodeByKey(n1);
            node.setActive(true);
            node.setSelected(true)
            // quebrar pelos niveis A1,A2, B1, B2 etc..
            $.each( raiz.replace(/\+/g,n1).split(n1).splice(1), function(k1,tronco){
                if( tronco )
                {
                    n2=n1+tronco.substring(0,1)
                    //ids.push( n2 );
                    node = tree.getNodeByKey(n2);
                    node.setActive(true);
                    node.setSelected(true)
                    $.each( folhas, function(k2,letra)
                    {
                        // verificar se tem a letra no tronco
                        pi = tronco.indexOf( letra );
                        if( pi >-1 )
                        {
                            pos=[];
                            folhas.slice( k2+1 ).map( function( folha ){
                                pos.push(tronco.indexOf( folha ) == -1 ? 1000 : tronco.indexOf( folha ) );
                            })
                            // encontrar o valor minimo do array de posições
                            pf = Math.min.apply(Math,pos);
                            folha = n2+tronco.substring( pi,pf )
                            if( folha.indexOf( '(') > -1 )
                            {
                                frutos = folha.substring( folha.indexOf('(')+1,folha.indexOf(')')).split(',');
                                folha  = folha.substring(0,folha.indexOf('('));
                                ids.push(folha);
                                //console.log( 'folha:'+folha)
                                $.each(frutos, function(k2,fruto){
                                    //console.log( folha+'('+fruto+')' );
                                    //ids.push(folha+'('+fruto+')');
                                    node = tree.getNodeByKey(folha+'('+fruto+')');
                                    node.setActive(true);
                                    node.setSelected(true)
                                });
                            }
                            else
                            {
                                //ids.push(folha);
                                node = tree.getNodeByKey(folha);
                                node.setActive(true);
                                node.setSelected(true);
                            }
                        }
                    })
                }
            })
        })
    }
 }

 $("#treeA,#treeB,#treeC,#treeD,#treeE").fancytree({
        checkbox: true, // Show checkboxes
        icon: false, // Display node icons
        minExpandLevel: 5, // 1: root node is not collapsible
        selectMode: 2, // 1:single, 2:multi, 3:multi-hier
        extensions: ["contextMenu"],
        select: function(event, data){criarCodigo()},
        init: function(event, data) {
           window.setTimeout(function(){updateTrees();},500);
        },
        contextMenu: {
             menu: {}
        }
});
