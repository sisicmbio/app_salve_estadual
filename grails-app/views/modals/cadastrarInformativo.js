//# sourceURL=cadastrarInformativo.js
//
var informativo = {

    init: function (data) {
        setTimeout(function(){
            app.focus('deTema','frmModalCadInformativo')
        },1000);

    },
    tabClick: function(data, link, evt) {
        return true;
    },
    save: function (params) {
        var data;
        var erros=[];

        if ( ! $('#frmModalCadInformativo').valid() ) {
            return false;
        }

        data = $("#frmModalCadInformativo").serializefiles();

        if( ! data.get('nuInformativo') ) {
            erros.push('O número é obrigatório.');
        }

        if( ! data.get('dtInformativo') ) {
            erros.push('A data é obrigatória.');
        }

        if( ! data.get('deTema') ) {
            erros.push('O tema é obrigatório.');
        }

        if( ! data.get('fldAnexo') && !data.get('sqInformativo') ) {
            erros.push('O PDF é obrigatório.');
        }

        if( erros.length > 0 ) {
            return app.alertInfo(erros.join('<br>') );
        }

        app.ajax(app.url + 'informativo/save', data,
            function (res) {
                if (res.status == 0) {
                    app.clearInputFile('frmModalCadInformativo');
                    informativo.reset();
                    informativo.calcularProximoNumero();
                    // atualizar a linha dos gride que foi alterada
                    informativo.updateGridLine(res.data);
                }
            }, null, 'json'
        );
    },
    reset: function () {
        app.reset('frmModalCadInformativo');
        //$('#frmModalCadInformativo #file').prop('required',true);
        $('#frmModalCadInformativo #btnCalcularProximoNumero').show();
        app.resetChanges('cadInformativoWrapper');
        $('#frmModalCadInformativo #inPublicado').prop('checked',true);
        informativo.calcularProximoNumero();
    },
    edit:function( params ){
        var data={sqInformativo:params.sqInformativo};
        app.ajax(app.url + 'informativo/edit',
            params,
            function(res) {
                //$('#frmModalCadInformativo #file').prop('required',false);
                app.setFormFields(res,'frmModalCadInformativo');
                app.selectTab("liTabCadInformativoForm");
                app.focus('deTema');
                $('#frmModalCadInformativo #btnCalcularProximoNumero').hide();
            });
    },
    delete:function(params,btn) {
        app.selectGridRow(btn);
        var data={sqInformativo:params.sqInformativo};
        app.confirm('<br>Confirma exclusão do informativo:'+params.descricao+'?',
            function() {
                app.ajax(app.url + 'informativo/delete',
                    data,
                    function( res ) {
                        $("#tr-grid-informativo-" + data.sqInformativo).remove();
                        // se excluir o registro em edição, limpar o formulario
                        if( data.sqInformativo == $("#frmModalCadInformativo #sqInformativo").val() ) {
                            informativo.reset();
                        }
                    });
            },
            function() {
                app.unselectGridRow(btn);
            }, params, 'Confirmação', 'warning');
    },
    calcularProximoNumero:function(){
        app.ajax(app.url + 'informativo/calcularProximoNumero', {},
            function (res) {
                if (res.status == 0) {
                    $("#frmModalCadInformativo #nuInformativo").val(res.data.proximoNumero);
                    app.focus('dtInformativo','frmModalCadInformativo');
                }
            }, null, 'json'
        );
    },
    /** atualizar uma unica linha do gride*/
    updateGridLine:function( params) {
        var data = {sqInformativo:params.sqInformativo};
        var $table =$("#divGridInformativos table");
        if( $table.size() == 0 ) {
            return;
        }
        app.ajax(app.url + 'informativo/getGrid', data,function(res) {
            var $tr=$("#tr-grid-informativo-"+data.sqInformativo);
            if( $tr.size() == 1 ){
                $tr.html( $(res).find('tr#tr-grid-informativo-' + data.sqInformativo).html() );
                app.selectTab('liTabCadInformativoHistorico');
            } else {
                $table.find('tbody').prepend($(res).find('tr#tr-grid-informativo-' + data.sqInformativo));
            }
        });
    },
    showPessoas:function( params ) {
        var data={sqInformativo:params.sqInformativo};
        app.ajax(app.url + 'informativo/showPessoas',
            params,
            function(res) {
                var pessoas=[]
                if( res.data.pessoas ) {
                    res.data.pessoas.map( function(item,i) {
                        pessoas.push('<li>'+item.no_pessoa + ' em '+item.dt_visualizacao + '</li>');
                    });
                }
                var conteudo  =  ( pessoas.length > 0 ? pessoas.join('') : 'Não foi lido até o momento!' );
                app.alertInfo('<div style="max-height: 400px;overflow-y: auto"><ol>'  + conteudo +'</ol></div>','Lido por');
            });
    }
}
informativo.init();