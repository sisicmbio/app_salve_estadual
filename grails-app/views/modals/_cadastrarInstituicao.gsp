<!-- view: /views/.gsp -->
<!-- controller:${params?.controller} -->
<!-- action: ${params?.action} -->
<div class="lazy-load">
   /modals/cadastrarInstituicao
</div>
<div class="window-container text-left">

   %{-- criar abas cadastro e pesquisar --}%
   <ul class="nav nav-tabs" id="ulTabsCadInstituicao">
      <li id="liTabCadInstituicaoForm" class="active"><a data-toggle="tab" href="#tabCadInstituicaoForm">Cadastrar</a></li>
      <li id="liTabCadInstituicaoPesquisa"><a data-toggle="tab" href="#tabCadInstituicaoPesquisa">Consultar</a></li>
   </ul>
   %{-- Container de todas abas --}%
   <div id="taxTabContent" class="tab-content">
      %{-- inicio aba Form Cad Ref Bib--}%
      <div role="tabpanel" class="tab-pane active" id="tabCadInstituicaoForm">
         %{-- inicio form --}%
         <form class="form-inline" role="form" id="frmModalCadInstituicao">
            <input type="hidden" name="sqWebInstituicao" value="">
            <div class="form-group">
                <label for="">Nome da Instituição</label>
                <br />
                <input name="noInstituicao" type="text" class="form-control fld700" required="true">
            </div>

            <br/>
            
            <div class="form-group">
                <label for="">Sigla</label>
                <br />
                <input name="sgInstituicao" type="text" class="form-control fld300" required="true">
            </div>

            %{-- BOTOES --}%
            <div class="panel-footer" id="divFooter">
                <a href="#" id="btnSaveCadInstituicao" class="btn btn-success" data-action="instituicao.save">Gravar</a>
                <a href="#" id="btnResetCadInstituicao" class="btn btn-default" data-action="instituicao.reset">Limpar Formulário</a>
                <br/>
                <span style="color:red;font-size:12px;">Antes de cadastrar uma instituição, verifique se a mesma já não existe no banco de dados na aba Consultar!</span>
            </div>
            
         </form>
         %{-- fim form --}%
         <div id="divGridModalCadInstituicaoAnexos" class="panel-footer mt10 hide" data-params="sqWebInstituicao"></div>
      </div>
      %{-- fim aba Form Cad Ref Bib--}%
      %{-- inicio aba Form Pesquisa Ref Bib--}%
      <div role="tabpanel" class="tab-pane" id="tabCadInstituicaoPesquisa">
         <form class="form-inline" role="form">
            <div class="form-group w100p">
               <label class="control-label">Consultar Instituição</label>
               <br>
               <input type="text" value="" id="fldCadInstituicaoPesquisar" data-enter="$('#btnCadInstituicaoPesquisar').click()" class="form-control ignoreChange fld700" placeholder="Pesquisar por... ( mínimo 3 caracteres)" />
            </div>
            <div class="panel-footer mt10">
               <a id="btnCadInstituicaoPesquisar"  data-action="instituicao.pesquisarInstituicao" data-params="fldCadInstituicaoPesquisar" class="btn btn-success">Pesquisar</a>
            </div>
         </form>
         <div id="divGridModalCadInstituicao" class="mt10"></div>
      </div>
      %{-- fim aba Form Pesquisa Ref Bib--}%
   </div>

</div>