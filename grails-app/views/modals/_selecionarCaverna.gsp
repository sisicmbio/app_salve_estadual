<!-- view: /views/modals/_selecionarCaverna.gsp -->
<!-- controller:${params?.controller} -->
<!-- action: ${params?.action} -->
<div class="lazy-load">
    /modals/selecionarCaverna
</div>
<div class="window-container text-left" id="selecionarCavernaWrapper">

    %{-- FILTROS --}%
    <div class="panel panel-title" id="divFiltros">
        <fieldset>
            <legend>Filtrar cavernas por</legend>

            <form class="form-inline" id="frmSelecionarCavernaFiltro" novalidate="novalidate">

%{--                ESTADO--}%
                <div class="form-group">
                    <label for="sqEstadoFiltro" class="control-label">Estado</label>
                    <br/>
                    <select name="sqEstadoFiltro" data-all-label="-- todos --"
                            id="sqEstadoFiltro"
                            data-change="selecionarCaverna.estadoChange"
                            data-multiple="false"
                            class="form-control fld fld200 fldSelect">
                        <option value="" selected="true">-- todos --</option>
                       <g:each var="item" in="${listEstados}">
                            <option data-codigo="${item.id}"
                                    value="${item.id}">${item.noEstado}</option>
                        </g:each>
                    </select>
                </div>

%{--                MUNICIPIO--}%
                <div class="form-group" id="divFiltroMunicipio" style="display: none;">
                    <label class="control-label">Município</label>
                    <br>
                    <select class="fld form-control fld300 select2" name="sqMunicipio" id="sqMunicipioFiltro"
                            data-s2-params="sqEstadoFiltro"
                            data-s2-url="ficha/select2Municipio"
                            data-s2-placeholder="?"
                            data-s2-minimum-input-length="3"
                            data-s2-auto-height="true"
                    >
                    </select>
                </div>

                <br>
                %{--                NOME--}%
                <div class="form-group">
                    <label class="control-label" for="noCavernaFiltro"
                           title="Informe o nome ou parte de nome da caverna para fazer a pesquisa.<br>Exemplo:Gruta, Barra, Serra do etc">Nome:</label><br>
                    <input class="form-control fld500" type="text" value="" id="noCavernaFiltro">
                </div>

                <div class="mt10 footer">
                    <button type="button" class="btn btn-sm btn-primary" data-action="selecionarCaverna.atualizarGride">Consultar</button>
                    <button type="button" class="btn btn-sm btn-danger" data-action="selecionarCaverna.limparFiltros">Limpar</button>
                    <div class="mt5 text-center">
                        <small class="red fs12 bold">Caso não encontre a caverna, entre em contato com o <a target="_blank" href="https://www.gov.br/icmbio/pt-br/assuntos/centros-de-pesquisa/cecav">CECAV</a> pelo e-mail: <a href="mailto:cecav.sede@icmbio.gov.br">cecav.sede@icmbio.gov.br</a> e solicite o cadastramento</small>
                    </div>
                </div>
            </form>
        </fieldset>
    </div>

    <div class="divGridCavernasWrapper mt10">
        <div id="divGridSelecionarCaverna" style="max-height: 340px;overflow-y: auto;">

        </div>
    </div>

    <div class="mt10 footer">
        <button type="button" class="btn btn-sm btn-success"
            data-sq-ficha="${params.sqFicha}"
            data-sq-ficha-ambiente-restrito="${params.sqFichaAmbienteRestrito}"
            data-action="selecionarCaverna.gravar">Gravar</button>
    </div>
</div>
