<!-- view: /views/modals/_selecionarColunasExportacaoRegistros.gsp -->
<!-- controller:${params?.controller} -->
<!-- action: ${params?.action} -->
<div class="lazy-load">
    /modals/selecionarColunasExportacaoRegistros
</div>
<div class="window-container text-left">
    <form id="frmselecionarColunasExportacaoRegistros" name="frmselecionarColunasExportacaoRegistros" class="form-inline" role="form">

        %{-- BOTOES --}%
        <div style="height: 33px;">
            <a href="javascript:void(0);" id="btnSelecionarTodos" data-checked="true" class="btn btn-sm btn-primary pull-left" data-action="selColsExportRegistros.marcarDesmarcarTodos">Marcar/Desemarcar todos</a>
%{--            <a href="javascript:void(0);" class="btn btn-danger pull-right ml15" data-action="app.closeWindow('modalSelecionarColunasExportacaoRegistros')">Fechar</a>--}%
            <a href="javascript:void(0);" class="btn btn-success pull-right" id="btnExportarColunasSelecionadasRegistros">Exportar</a>
        </div>

        <div style="">Colunas "<b>Nome científico</b>", "<b>Latitude</b>" e "<b>Longitude</b>" serão exportadas automaticamente</div>

        <div style="height: 515px; overflow: auto;border-top:1px solid gray;margin-top:5px;" id="divBodysSlecionarColunasExportacaoRegistros">
            <g:each var="item" in="${listColunas}" status="i">
                <label class="text-nowrap cursor-pointer" style="min-width:200px; width:30%;padding-top:2px;">
                <input type="checkbox" checked value="${item.column}">&nbsp;${item.title}</label>
            </g:each>
        </div>

    </form>
</div>
