//# sourceURL=\views\modal\alterarSituacaoFichaParaExcluida.js
//
var frmAlterarSituacaoFichaParaExcluida = {
    init:function(data){
        // fazer a leitura da justificativa atual e atualiar o campo dsJustificativaExclusao
        frmAlterarSituacaoFichaParaExcluida.canModify = $("#canModify").val();
        if( data.sqFicha ) {
            app.blockElement("#frmModalAlterarSituacaoFichaParaExcluida")
            app.ajax(app.url + 'ficha/getJustificativaExcluida', {sqFicha:data.sqFicha},
                function (res) {
                    if (res.status == 0) {
                        window.setTimeout(function(){
                            app.unblockElement("#frmModalAlterarSituacaoFichaParaExcluida")
                            /*if( canModify ) {
                                $("#modalAlterarSituacaoFichaParaExcluida #divFooter").show()
                                tinyMCE.DOM.setStyle( tinyMCE.DOM.get("dsJustificativaExclusao" + '_ifr'), 'height', ($("#modalAlterarSituacaoFichaParaExcluida").height()-230)+'px');
                            } else {
                                $("#modalAlterarSituacaoFichaParaExcluida #divFooter").hide()
                                tinyMCE.DOM.setStyle( tinyMCE.DOM.get("dsJustificativaExclusao" + '_ifr'), 'height', ($("#modalAlterarSituacaoFichaParaExcluida").height()-130)+'px');
                            }
                            tinyMCE.get('dsJustificativaExclusao').setContent( res.data.dsJustificativa ? res.data.dsJustificativa  : 'Excluída por não ocorrer no Brasil');
                            $("#dsJustificativaExclusao").val( tinyMCE.get('dsJustificativaExclusao').getContent());
                             */
                            $textArea = $("#modalAlterarSituacaoFichaParaExcluida #dsJustificativaExclusao")
                            if( canModify ) {
                                $("#modalAlterarSituacaoFichaParaExcluida #divFooter").show()
                                $textArea.prop('readonly',false)
                            } else {
                                $("#modalAlterarSituacaoFichaParaExcluida #divFooter").hide()
                                $textArea.prop('readonly',true)
                            }
                            $textArea.val( res.data.dsJustificativa ? res.data.dsJustificativa  : 'Excluída por não ocorrer no Brasil')
                            if( ! res.data.dsJustificativa ){
                                app.growl('Ficha não possui histórico de exclusão.<br>Foi adicionado o texto padrão na justificativa como sugestão!',8,'Aviso!')
                            }
                            app.focus('dsJustificativaExclusao')
                        },1000)
                    } else {
                        app.blockElement("#frmModalAlterarSituacaoFichaParaExcluida")
                        app.focus('dsJustificativaExclusao')
                    }
                }, null, 'json'
            );
        }
    },
    save : function(params) {
        var data = $("#frmModalAlterarSituacaoFichaParaExcluida").serializeArray();
        data = app.params2data(params, data);
        data.sqFicha = $("#divEdicaoFicha #sqFicha").val();
        if (!data.sqFicha) {
            app.alertError('ID da ficha não encontrado!');
            return;
        }
        app.ajax(app.url + 'ficha/alterarSituacaoFichaParaExcluida', data,
            function(res) {
                if ( res.status == 0 ) {
                    $("#situacaoFicha").val( res.data.dsSituacao );
                    $("#cdSituacaoFicha").val( res.data.cdSituacao );
                    $("#spanSituacaoFicha").html('<b>Situação</b>: ' + $("#situacaoFicha").val() );
                    if( res.data.cdSituacao == 'EXCLUIDA') {
                        // remover a aba 11 do formulário
                        $("#liTabAvaliacao").remove();
                        $("#tabAvaliacao").remove();
                    }
                    app.closeWindow('modalAlterarSituacaoFichaParaExcluida');
                }
            }, null, 'json'
        );
    },
};
