<!-- view: /views/modals/_cadastrarAlerta.gsp -->
<!-- controller:${params?.controller} -->
<!-- action: ${params?.action} -->
<div class="lazy-load">
    /modals/cadastrarAlerta
</div>
<div class="window-container text-left" id="cadAlertaWrapper">

    %{-- criar abas cadastro e historico --}%
    <ul class="nav nav-tabs" id="ulTabsCadAlerta">
        <li id="liTabCadAlertaForm" class="active"><a data-toggle="tab" href="#tabCadAlertaForm">Cadastrar</a></li>
        <li id="liTabCadAlertaHistorico"><a data-toggle="tab"
                                                 data-action="cadastroAlerta.tabClick"
                                                 data-url="informativo/getGridAlerta"
                                                 data-container="divGridAlertas"
                                                 data-reload="false"
                                                 href="#tabCadAlertaPesquisa">Histórico</a></li>
    </ul>
    %{-- Container de todas abas --}%
    <div id="taxTabContent" class="tab-content">
        %{-- inicio aba Form Cad Ref Bib--}%
        <div role="tabpanel" class="tab-pane active" id="tabCadAlertaForm">
            %{-- inicio form --}%
            <form class="form-inline" role="form" id="frmModalCadAlerta">
                <input type="hidden" name="sqAlerta" id="sqAlerta" value="">

                %{-- PERIODO ALERTA --}%
                <div class="form-group">
                    <label for="dtInicioAlerta">Início alerta</label>
                    <br />
                    <input name="dtInicioAlerta" id="dtInicioAlerta" type="text" class="fld form-control date fld120" value="">
                </div>
                <div class="form-group ml15">
                    <label for="dtFimAlerta" title="Informe o perído ou apenas a data final para que a mensagem de alerta<br>seja exibida automaticamente aos usuário ao acessar o SALVE<br>no período ou até a data final informada.">Fim alerta</label>
                    <br />
                    <input name="dtFimAlerta" id="dtFimAlerta" type="text" class="fld form-control date fld120" value="">
                </div>

                %{-- PUBLICADO --}%
                <div class="form-group mt10 ml15" style="border-bottom:1px solid silver;">
                <label for="inPublicado" class="control-label cursor-pointer label-checkbox"
                       title="Marque como publicada para que o alerta seja exibido ao usuários."
                       style="margin-top:17px;">Publicado?&nbsp;
                    <input name="inPublicado" id="inPublicado" class="fld checkbox-lg tooltipstered" type="checkbox" checked="true" value="S">
                </label>
                </div>

                <br>

                <div class="form-group">
                    <label for="txAlerta">Mensagem de alerta</label>
                    <br />
                    <textarea name="txAlerta" id="txAlerta" rows="4" class="fld form-control fld700"></textarea>
                </div>

                <br>

                %{-- BOTOES --}%
                <div class="panel-footer mt20" id="divFooter">
                    <a href="#" id="btnSaveCadAlerta" class="btn btn-success" data-action="cadastroAlerta.save">Gravar</a>
                    <a href="#" id="btnResetCadAlerta" class="btn btn-default" data-action="cadastroAlerta.reset">Limpar Formulário</a>
                    <br/>
                </div>
            </form>
            %{-- fim form --}%
        </div>
        %{-- fim aba Form Cad Ref Bib--}%

        %{-- inicio aba Form Pesquisa Ref Bib--}%
        <div role="tabpanel" class="tab-pane" id="tabCadAlertaPesquisa">
            <div id="divGridAlertas"></div>
        </div>
        %{-- fim aba Form Pesquisa Ref Bib--}%
    </div>
</div>