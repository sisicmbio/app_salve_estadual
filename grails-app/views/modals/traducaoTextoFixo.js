//# sourceURL=/views/modals/traducaoTextoFixo.js
var traducaoTextoFixo = {
    noTabelaTraducao:'',
    idRegistroTraducao:'',
    init : function() {
        // delay para aguardar a renderização da janela modal
        setTimeout(function(){
            app.focus('txOriginal','frmModalTraducaoTextoFixo');
            traducaoTextoFixo.noTabelaTraducao = $("#noTabelaTraducao").val();
            traducaoTextoFixo.idRegistroTraducao = $("#idRegistroTraducao").val();
            // se for passado o nome da tabela e o valor ainda ainda não tiver sido traduzido, fazer a tradução automática
            if( traducaoTextoFixo.noTabelaTraducao && $("#txTraduzido").val() == '' ) {
                traducaoTextoFixo.traduzir();
            } else
            // se o texto original e o texto traduzido estiverem com o mesmo valor então fazer a tradução automática
            if( $("#txOriginal").val() != '' && $("#txOriginal").val() == $("#txTraduzido").val() ) {
                traducaoTextoFixo.traduzir();
            }
            app.grid('GridTraducaoTextoFixo');
        },1000);
    },

    /**
     * traduzir o texto original em português
     * @param params
     * @param fld
     * @param evt
     */
    traduzir: function(params,fld,evt){
        var txOriginal = $("#txOriginal").val();
        if( !txOriginal){
            app.alertInfo('Informe a palavra ou texto para tradução',null,function(){
                app.focus('txOriginal','frmModalTraducaoTextoFixo');
            })
            return;
        }
        tradutor.traduzir(txOriginal,function( txTraduzido ) {
            $("#txTraduzido").val( txTraduzido );
            $("#txRevisado").val( txTraduzido );
            app.focus('txRevisado', 'frmModalTraducaoTextoFixo');
        });
    },


    /**
     * gravar o registro
     * @param params
     */
    save:function(params){
        var txOriginal=$("#txOriginal").val().trim()
        var txTraduzido=$("#txTraduzido").val().trim()
        var txRevisado=$("#txRevisado").val().trim()
        if( !txOriginal || !txTraduzido || !txRevisado ) {
            app.alertInfo('Preencha todos os campos.')
            return;
        }
        var data = {
            sqTraducaoTexto:$("#sqTraducaoTexto").val()
            ,txOriginal         : txOriginal
            ,txTraduzido        : txTraduzido
            ,txRevisado        : txRevisado
            ,noTabelaTraducao   : traducaoTextoFixo.noTabelaTraducao
            ,idRegistroTraducao : traducaoTextoFixo.idRegistroTraducao
        }
        app.ajax( app.url + 'traducao/save',
            data,
            function( res ) {
                if( res.status == 0 ) {
                    app.grid('GridTraducaoTextoFixo', {sqTraducaoTexto: $("#sqTraducaoTexto").val()});
                    $("#frmModalTraducaoTextoFixo #inSaved").val('S'); // controle para saber se a janela foi fechada com ou sem clicar no botão Gravar
                    if( ! traducaoTextoFixo.noTabelaTraducao) {
                        traducaoTextoFixo.reset()
                    } else {
                        app.closeWindow("modalTraducaoTextoFixo");
                    }
                }
            });
    },

    /**
     * alterar o registro
     * @param params
     */
    edit : function( params ){
        var data = {sqTraducaoTexto:params.sqTraducaoTexto}
        app.ajax( app.url + 'traducao/edit',
            data,
            function( res ) {
                if ( res.data && res.data.tx_original ) {
                    $("#txOriginal").val(res.data.tx_original);
                    $("#txTraduzido").val(res.data.tx_traduzido);
                    $("#txRevisado").val(res.data.tx_revisado);
                    $("#sqTraducaoTexto").val(res.data.sq_traducao_texto);
                    $("#btnSaveTraducao").html('Gravar alteração');
                }
            });
    },

    /**
     * excluir o registro
     * @param params
     */
    delete:function( params ){
        app.confirm('Confirma a exclusão de ' + params.descricao+'?', function() {
            var data = {sqTraducaoTexto:params.sqTraducaoTexto}
            app.ajax( app.url + 'traducao/delete',
                data,
                function( res ) {
                    if( res.status == 0) {
                        app.grid('GridTraducaoTextoFixo');
                        traducaoTextoFixo.reset()
                    }
                });
        });
    },

    /**
     * limpar o formulário
     */
    reset:function(){
        $("#sqTraducaoTexto").val('');
        if( ! traducaoTextoFixo.noTabelaTraducao ) {
            $("#txOriginal").val('');
            app.focus('txOriginal','frmModalTraducaoTextoFixo');
        } else {
            app.focus('txRevisado','frmModalTraducaoTextoFixo');
        }
        $("#txTraduzido").val('');
        $("#txRevisado").val('');
        $("#btnSaveTraducao").html('Gravar');
    }

}
traducaoTextoFixo.init();
