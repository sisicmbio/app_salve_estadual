//# sourceURL=N:\SAE\sae\grails-app\views\modal\cadastrarInstituicao.js
//
;

var instituicao = {
    init: function() {

    },
    getGrid: function(params) {
        app.getGrid('modalCadInstituicao');
    },

    pesquisarInstituicao: function(params) {
        if (!$.trim(params.fldCadInstituicaoPesquisar)) {
            app.alertInfo('Informe uma palavra chave!')
            return;
        }
        if ($.trim(params.fldCadInstituicaoPesquisar).length < 3) {
            app.alertInfo('Informe no mínimo 3 caracteres para pesquisar!')
            return;
        }
        $('#divGridModalCadInstituicao').data('q', params.fldCadInstituicaoPesquisar);
        instituicao.getGrid();
    },
    save: function(params) {
        if (!$('#frmModalCadInstituicao').valid()) {
            return false;
        }
        var data = $("#frmModalCadInstituicao").serializeArray();
        app.ajax(app.url + 'instituicao/save', data,
            function(res) {
                if (res.status == 0) {
                    instituicao.reset();
                    instituicao.getGrid();
                } else if (res.errors) {
                    app.alertError('Erro(s) Encontrado(s)<br/><br/>' + res.errors.join('<br/>'))
                }

            }, null, 'json'
        );
    },
    reset: function(params) {
        app.reset('frmModalCadInstituicao');
        app.focus('noInstituicao');
    },
    edit: function(params, btn) {
        app.ajax(app.url + 'instituicao/edit', params,
            function(res) {
                if (res) {
                    // Comentário
                    app.setFormFields(res, 'frmModalCadInstituicao');
                    app.selectTab('tabCadInstituicaoForm');
                }
            }, null, 'json'
        );
    },
    delete: function(params, btn) {
        app.selectGridRow(btn);
        app.confirm('<br>Confirma exclusão da instituição <b>' + params.descricao + '</b>?',
            function() {
                app.ajax(app.url + 'instituicao/delete',
                    params,
                    function(res) {
                        if (res) {
                            app.alertError(res)
                        } else {
                            instituicao.getGrid()
                        }
                    });
            },
            function() {
                app.unselectGridRow(btn);
            }, params, 'Confirmação', 'warning');
    },


};
instituicao.init();