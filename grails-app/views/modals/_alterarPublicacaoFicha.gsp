<!-- view: /views/modals/alterarPublicacaoFicha.gsp -->
<!-- controller:${params?.controller} -->
<!-- action: ${params?.action} -->
<div class="lazy-load">
   /modals/alterarPublicacaoFicha
</div>
<div class="window-container text-left">
    <input type="hidden" id="sqPublicacaoSelecionada" value="${params.sqPublicacao}">
    <div class="alert alert-success" style="margin-bottom: 0px;" id="divReferenciaSelecionada">
        %{--<h4 style="margin-top:0px;"><b>Referência selecionada</b></h4>--}%
        ${raw(publicacao?.referenciaHtml)}
    </div>
    %{--
  <div>
      <label class="control-label">Alterar a referência selecionada com</label>
      <select id="sqPublicacaoAlterarFichas" class="form-control select2" style="width:100%"
              data-s2-params="sqPublicacao"
              data-s2-url="/salve-estadual/ficha/select2RefBib"
              data-s2-placeholder="Procurar titulo/autor... ( min 3 caracteres )"
              data-s2-minimum-input-length="3"
              data-s2-auto-height="true">
      </select>
  </div>

  <div class="fld panel-footer">
      <button id="btnSave" data-action="alterarPublicacaoFicha.save" data-sq-publicacao="${params.sqPublicacao}" class="btn btn-primary">Gravar</button>
      <button id="btnSaveDelete" data-action="alterarPublicacaoFicha.saveDelete" data-sq-publicacao="${params.sqPublicacao}" title="Atualizar as fichas com a nova referência e excluir a antiga." class="btn btn-danger pull-right">Gravar e excluir</button>
  </div>
--}%
    <div class="mt10">
        <h4>Fichas</h4>
        <div id="divGridFichasComPublicacao"></div>
    </div>
</div>
