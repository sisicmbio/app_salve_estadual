//# sourceURL=N:\SAE\sae\grails-app\views\modal\alterarTaxonFicha.js
//
var frmAlterarTaxonFicha = {
    $spanNomeCientificoFicha:'',
    $spanNomeTaxonAtual:'',
    $divNmCientifico:'',
    init:function(data)
    {
        // aguardar o form ser renderizado na janela
        //$("#spanNomeTaxonAtual").html('Carregando...');
        window.setTimeout(function(){
            frmAlterarTaxonFicha.$spanNomeCientificoFicha = $("#spanNomeCientificoFicha");
            frmAlterarTaxonFicha.$divNmCientifico = $("#divNmCientifico");
            frmAlterarTaxonFicha.$spanNomeTaxonAtual = $("#spanNomeTaxonAtual");
            frmAlterarTaxonFicha.$spanNomeTaxonAtual.html( frmAlterarTaxonFicha.$spanNomeCientificoFicha.html() );
        },2000);

    },
    save : function(params)
    {
        var data = $("#frmModalAlterarTaxonFicha").serializeArray();
        data = app.params2data(params, data);
        data.sqFicha = $("#divEdicaoFicha #sqFicha").val();
        if (!data.sqFicha) {
            app.alertError('ID da ficha não encontrado!');
            return;
        }
        if (!data.sqTaxonNovo) {
            app.alertError('Táxon destino não selecionado!');
            return;
        }
        app.ajax(app.url + 'ficha/alterarTaxonFicha', data,
            function(res) {
                if (res.status == 0) {
                    if( res.data.novoNoCientifico)
                    {
                        frmAlterarTaxonFicha.$spanNomeCientificoFicha.html( res.data.novoNoCientifico );
                        frmAlterarTaxonFicha.$divNmCientifico.html( res.data.novoNoCientifico );
                        app.closeWindow('modalAlterarTaxonFicha');
                    }
                }
            }, null, 'json'
        );


    },
};


