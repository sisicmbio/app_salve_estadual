<div class="window-container text-left">
    <form name="frmUpdateArquivoSistema" id="frmUpdateArquivoSistema" class="form-inline" role="form" enctype='multipart/form-data'>
        <div class="fld form-group" id="divFichaAnexo">
            <input type="hidden" name="fileName" id="fileName" value="${fileName}">
            <label for="file">Selecione o Arquivo!</label>
            <br />
            <input name="fldArquivoSistema" type="file" id="file"
                   class="file-loading" required="true" accept="${accept}"
                onchange="uploadFileChange(this)"
            data-show-preview="false"
            data-show-upload="false"
            data-show-caption="true"
            data-show-remove="true"
            data-max-file-count="1"
            data-allowed-file-extensions='${extension}'
            data-main-class="input-group-sm"
            data-max-file-size="${maxSize}"
            data-preview-file-type="object">
        </div>
        <div class="upload-preview" style="overflow-y:auto;display:none;margin-top:10px;border:1px solid gray;width:100%;height:200px;background-color: #c8c8c8">
            <img src="" id="preview_img" class="upload-preview-img" style="width: 100%;object-fit: cover;"/>
        </div>
        <div class="fld panel-footer">
            <button id="btnSaveFrmUpdateManual" data-action="app.updateArquivoSistema" data-params="sqCicloAvaliacao" class="fld btn btn-success">Enviar</button>
        </div>
    </form>
</div>
