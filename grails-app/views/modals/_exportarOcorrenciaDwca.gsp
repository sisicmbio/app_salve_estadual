<div class="window-container text-left">
    <form name="frmExportarOcorrenciaPortalbio" name="frmExportarOcorrenciaPortalbio" class="form-inline" role="form">
        <div class="fld form-group" id="divFichaAnexo">
            <label>Ciclo de Avaliação</label>
            <br />
            <select class="form-control w100p" name="sqCicloAvaliacao">
                <option value="">-- selecione o ciclo --</option>
                <g:each var="item" in="${listCicloAvaliacao}">
                    <option value="${item.id}">${item.deCicloAvaliacao}</option>
                </g:each>
            </select>
        </div>
        <div class="fld panel-footer">
            <a id="btnSaveFrmExportarOcorrenciaPortalbio"
               data-action="app.exportarOcorrenciaPortalbio" class="fld btn btn-success">Gerar Arquivo ZIP</a>
        </div>
    </form>
</div>
