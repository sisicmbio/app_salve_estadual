<div class="window-container text-left">
	<form id="frmModalSelRegiao" name="frmModalSelRegiao" class="form-inline" role="form">
		<input type="hidden" id="id" name="id" value="" />
		<input type="hidden" id="noContexto" name="noContexto" value="${params.noContexto}" />
        <div class="form-group">
            <label class="control-label">Contexto</label>
            <br>
            <h4 id="frmModalSelRegiaoContexto" class="form-control-static" style="color:#3C68F1">Nome do conexto</h4>
        </div>

        <br />

    	%{-- ABRANGENCIA --}%
		<div class="form-group">
	         <label class="control-label">Abrangência</label>
	         <br>
	         <select id="sqTipoAbrangencia" name="sqTipoAbrangencia" data-contexto="frmModalSelRegiao" data-change="tipoAbrangenciaChange" class="fld form-control fld200">
	         	<option data-codigo="" value="">?</option>
	         	<g:each var="item" in="${listAbrangencia}">
	         		<option data-codigo="${item.codigo}" value="${item.id}">${item.descricao}</option>
	         	</g:each>
	         </select>
	    </div>
		<div class="form-group">
			<label class="control-label">&nbsp;</label>
	        <br>
			<span>-</span>
	         <select id="sqAbrangencia"
	         			name="sqAbrangencia"
	         			class="fld form-control select2 fld600"
	         			disabled="true"
	                    data-s2-params="sqTipoAbrangencia"
	                    data-s2-url="ficha/getAbrangencias"
	                    data-s2-minimum-input-length="1"
	                    required="true">
	         	<option value="">?</option>
	         </select>
	    </div>

%{--   		<div class="form-group">
	         <label class="control-label">Abrangência</label>
	         <br>
	         <select id="sqTipoAbrangencia" name="sqTipoAbrangencia" data-form="frmModalSelRegiao"
	         	data-is-ocorrencia="true" data-change="ficha.abrangenciaChange" class="fld form-control fld200" required="true">
	         	<option data-codigo="" value="">?</option>
	         	<g:each var="item" in="${listAbrangencia}">
	         		<option data-codigo="${item.codigo}" value="${item.id}">${item.descricao}</option>
	         	</g:each>
	         </select>
        </div>
		<div class="form-group">
			<label class="control-label">&nbsp;</label>
	        <br>
			<span>-</span>
	         <select id="sqAbrangencia"
	         			name="sqAbrangencia"
	         			class="fld form-control select2 fld600"
	         			disabled="true"
	         			data-s2-minimum-input-length="0" required="true">
	         	<option value="">?</option>
	         </select>
        </div> --}%


		<div class="form-group hide">
			<label class="control-label">&nbsp;</label>
	        <br>
			<span>-</span>
	         <input type="text" name="noRegiaoOutra" id="noRegiaoOutra" class="fld form-control fld300" value=""/>
        </div>
        <br>
		<div class="form-group w100p">
   			<label for="txLocal" class="control-label">Observações Sobre a Região</label>
    		<br>
    		<textarea name="txLocal" id="txLocal" rows="10" class="fld fldTextarea w100p"></textarea>
  		</div>
  		<div class="fld panel-footer">
	   		<button id="btnSaveFrmModalSelRegiao" data-action="selRegiao.saveFrmModalSelRegiao" class="fld btn btn-success">Gravar</button>
  		    <button id="btnResetFrmModalSelRegiao" data-action="selRegiao.resetFrmModalSelRegiao" class="fld btn btn-info hide">Limpar Formulário</button>
	</div>
	</form>

	<div id="divGridSelRegiao" class="mt10">

	</div>

</div>