<!-- view: /views/modals/_importacaoFicha.gsp -->
<!-- controller:${params?.controller} -->
<!-- action: ${params?.action} -->
<div class="window-container text-left">
    <form name="frmImportacaoFicha" id="frmImportacaoFicha" class="form-inline" role="form" enctype='multipart/form-data'>
        <div class="fld form-group" id="divImportacaoFicha">
            <label for="file">Selecionar Planilha</label>
            <br>

            %{-- data-max-image-width="1024" --}%
            <input name="file" id="file" type="file"  class="file-loading" required="true" accept="application/vnd.ms-excel" style="width:0 !important;"
            data-show-preview="false"
            data-show-upload="false"
            data-show-caption="true"
            data-show-remove="false"
            data-max-file-count="1"
            data-allowed-file-extensions='["xls","xlsx"]'
            data-main-class="input-group-sm"
            data-max-file-size="50000"
            data-preview-file-type="object">
        </div>
        <br class="fld">
        <div class="fld form-group">
            <label for="deComentario" class="control-label">&nbsp;</label>
            <br>
            <input name="deComentario" id="deComentario" type="text" class="fld form-control fld600" value="" placeholder="Descriçao da planilha (opcional)"/>
        </div>
        <div class="fld panel-footer">
            <button id="btnSaveFrmImportacaoFicha" data-action="ficha.savePlanilhaImportacaoFicha" data-params="deComentario,sqCicloAvaliacao,contexto:${params.contexto}" class="fld btn btn-success">Importar Planilha</button>
        </div>
        %{-- gride  --}%
        <div class="" id="divGridModalImportacaoFichas" data-params="sqCicloAvaliacao,contexto:${params.contexto}"></div>
    </form>
</div>
