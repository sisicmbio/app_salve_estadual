<!-- view: /views/modals/_modalUserJobs.gsp -->
<!-- controller:${params?.controller} -->
<!-- action: ${params?.action} -->
<div class="lazy-load">
    /modals/modalUserJobs.js
</div>
<div id="modalUserJobs-container text-left container-fluid">
    <div class="row">
        <div class="col-sm-12 text-left mb10">
            <button id="btnClearUserJobs" type="button" class="btn btn-primary" data-action="modalUserJobs.clear" ${jobs.size()==0 ? 'disabled="true"' : '' }>Limpar tarefas</button>
%{--            <button id="btnRefreshUserJobs" type="button" class="btn btn-success" data-action="modalUserJobs.refresh">Atualiazar</button>--}%
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 text-left">
        <table class="table table-condensed table-hover table" id="tableUserJobs">
            <thead>
                <tr>
                    <th style="width:50px;">Nº</th>
                    <th style="width:100px;">Data<br>criação</th>
                    <th style="width:100px;">Ultima<br>Atualização</th>
                    <th style="width:auto;">Descricao</th>
                    <th style="width:200px;">E-mail<br>Destinatário</th>
                    <th style="width:200px;">Andamento</th>
                    <th style="width:100px;">Situação</th>
                    <th style="width:80px;">Ação</th>
                </tr>
            </thead>
            <tbody>
            <g:if test="${jobs}">
                <g:each var="job" in="${jobs}">
                    <tr id="tr-${job.id}">
                        <td class="text-center">${job?.id}</td>
                        <td class="text-center">${job?.dtInclusao?.format('dd/MM/yyyy HH:mm:ss')}</td>
                        <td class="text-center" id="dtUltimaAtualizacaoCol-${job.id}">${job?.dtUltimaAtualizacao?.format('dd/MM/yyyy HH:mm:ss')}</td>
                        <td class="text-left">${job.deRotulo}</td>
                        <td class="text-center">${job.deEmail}</td>

%{--    ṔROGRESSO--}%
                        <td class="text-center">
                            <div class="progress" id="progress-${job.id}">
                                <div class="progress-bar" role="progressbar"
                                     style="min-width: 2em; width: ${job.percentual()}%;">${job.percentual()}%
                                </div>
                            </div>
                        <small class="progress-andamento" id="progress-andamento-${job.id}">&nbsp;</small>

                        </td>

%{--                        SITUACAO--}%
                        <td class="text-center" id="deSituacaoCol-${job.id}">${job.situacao()}</td>

%{--BOTOES--}%
                        <td class="text-center">
                            <button type="button" data-action="modalUserJobs.delete"
                                data-descricao="${job.deRotulo}"
                                data-id="${job.id}" class="btn btn-default btn-xs btn-delete" title="Excluir">
                                <span class="glyphicon glyphicon-remove"></span>
                            </button>

                            <g:set var="disabled" value="${ !job?.noArquivoAnexo || job?.percentual() < 100 ? 'disabled':''}"></g:set>
                            <button id="btnDownloadUserJob-${job.id}" type="button"
                                    data-action="modalUserJobs.baixar" ${disabled}
                               data-no-arquivo="${job.noArquivoAnexo}"
                               data-id="${job.id}" class="btn btn-default btn-xs btn-delete ${disabled}" title="Baixar o arquivo">
                                <span class="glyphicon glyphicon-download"></span>
                            </button>


                        </td>

                    </tr>
                </g:each>
            </g:if>
            <g:else>
                <tr><td colspan="8"><h4><br>Nenhuma tarefa registrada</h4></td></tr>
            </g:else>
            </tbody>
        </table>
        </div>
    </div>
</div>
