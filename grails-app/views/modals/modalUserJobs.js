//# sourceURL=modalUserJobs.js
modalUserJobs = {
    init:function() {
        //modalUserJobs.refresh();
    },
    delete:function( params, ele, evt ){
        app.confirm('Confirma e exclusão da tarefa '+params.id+'?', function() {
            var data = { sqUserJob:params.id };
            app.ajax(app.url + 'main/deleteUserJob', data,
                function (res) {
                    if (res.status == 0) {
                        if( res.data.sqUserJob ){
                            $("table#tableUserJobs tbody tr#tr-"+res.data.sqUserJob).remove();
                            if( $("table#tableUserJobs tbody tr").size() == 0){
                                $("#btnClearUserJobs").prop("disabled",true);
                            }
                        }
                    }
                }, null, 'json'
            );
        },null,null,'Cofirmação');
    },
    clear:function( params, ele, evt ){
        app.confirm('Confirma e exclusão de TODAS as tarefas?', function() {
            var data = { sqUserJob:-1 };
            app.ajax(app.url + 'main/deleteUserJob', data,
                function (res) {
                    if (res.status == 0) {
                        if( res.data ){
                            $("table#tableUserJobs tbody").html('<td colspan="7"><span><h4>Tarefas excluídas com SUCESSO</h4></span></td>');
                            $("#btnClearUserJobs").prop("disabled",true);
                        }
                    }
                }, null, 'json'
            );
        },null,null,'Cofirmação');

    },
    baixar:function( params, ele, evt ){
        var noArquivo = params.noArquivo
        app.confirm('Baixar o arquivo?', function() {
            setTimeout(function(){
                downloadWithProgress({fileName: noArquivo})
            },500)
        },null,null,'Cofirmação');
    },
    /*
    refresh:function( params,ele,evt ) {

        // interromper o cronometro atual
        if( modalUserJobs.timeout ){
            window.clearTimeout( modalUserJobs.timeout );
        }
        modalUserJobs.timeout=null

        if( ! $("#modalUserJobs").is(":visible") ) {
            return;
        }
        ajaxJson(app.url+'main/getUserJobs',{},'',function(res){
            res.data.jobs.forEach(function(job, index){
                processarUserJob(job)
            })
        });
    }*/
}
modalUserJobs.init();


