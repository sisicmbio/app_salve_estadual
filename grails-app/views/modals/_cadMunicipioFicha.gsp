<!-- view: /views/modals/_cadMunicipioFicha.gsp -->
<!-- controller:${params?.controller} -->
<!-- action: ${params?.action} -->
<div class="lazy-load">
    /modals/cadMunicipioFicha
</div>
<form class="form-horizontal text-left" role="form" id="frmSelMunicipio">
    <input type="hidden" id="sqEstado" name="sqEstado" value="${params.sqEstado}">
    <input type="hidden" id="sqFichaAreaRelevancia" name="sqFichaAreaRelevancia" value="${params.sqFichaAreaRelevancia}">
    <div class="form-group">
        <div class="col-sm-4">
            <label class="control-label">Área de Relevância</label>
            <h4 id="txRelevanciaLabel" class="form-control-static" style="color:#3C68F1">${params.txRelevancia}</h4>
        </div>
        <div class="col-sm-4">
            <label class="control-label">Estado</label>
            <h4 id="noEstadoLabel" class="form-control-static" style="color:#3C68F1">${params.noEstado}</h4>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-12">
            <label class="control-label">Município</label>
            <br>
            <select class="form-control fld300 select2" name="sqMunicipio" id="sqMunicipio"
                    data-s2-params="sqEstado:${params.sqEstado}"
                    data-s2-url="ficha/select2Municipio"
                    data-s2-placeholder="?"
                    data-s2-minimum-input-length="3"
                    data-s2-auto-height="true"
                    data-s2-container="frmSelMunicipio">
            </select>
        </div>
    </div>

    <div class="form-group w100p">
        <div class="col-sm-12">
            <label for="txRelevanciaMunicipio" class="control-label">Observação</label>
            <br/>
            <textarea class="form-control fldTextarea wysiwyg w100p" rows="7" id="txRelevanciaMunicipio" name="txRelevanciaMunicipio"></textarea>
        </div>
    </div>

    <div class="panel-footer">
        <button id="btnSaveFrmSelMunicipioAreaRelevante" data-action="cadMunicipioFicha.save" class="btn btn-success">Gravar</button>
    </div>

    <div class="form-group">
        <div class="col-sm-12">
            <div id="divGridMunicipiosAreaRelevante" data-sq-ficha-area-relevancia="${params.sqFichaAreaRelevancia}"></div>
        </div>
    </div>
</form>
