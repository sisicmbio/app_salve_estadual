<div id="winSelGeo" class="window-container text-left">

<div class="col-sm-6">

	<form id="frmModalSelGeo" name="frmModalSelGeo" class="form-inline" role="form">
		<input type="hidden" id="id" name="id" value="" />
		<div class="form-group">
			<label class="control-label">Contexto</label>
			<br>
			<h4 id="frmModalSelGeoContexto" class="form-control-static" style="color:#3C68F1">Nome do conexto</h4>
		</div>
		<br />
		<div class="form-group">
			<label class="control-label">Datum</label>
			<br />
			<select name="sqDatum" id="sqDatum" class="fld form-control fld200 fldSelect" required="true">
				<option value="">-- selecione --</option>
				<g:each in="${listDatum}" var="item">
					<option value="${item.id}">${item.descricao}</option>
				</g:each>
			</select>
		</div>
		<br />
		<div class="form-group" id="divLatGD">
			<label class="control-label" for="fldLatitude" title="Clique no mapa com a telca CTRL pressionada para peencher os campos latitude e longitude com as coordenadas do ponto clicado!" data-tooltip-icon="pushpin">Latitude</label>
			<br />
			<input id="fldLatitude" name="fldLatitude" type="text" class="fld form-control fld150 boldAzul coordenada" value="${fichaOcorrencia?.geometry?.x}" required="true"/>
		</div>
		<div class="form-group"  id="divLonGD">
			<label class="control-label">Longitude</label>
			<br />
			<input id="fldLongitude" name="fldLongitude" type="text" class="fld form-control fld150 boldAzul coordenada" value="${fichaOcorrencia?.geometry?.y}" required="true"/>
			<button id="btnChangeCoordFormat" data-action="selGeo.changeGDGMS" class="btn btn-default btn-sm" title="Utilizar formato GMS.">GMS</button>
			<button id="btnViewPoint" data-action="selGeo.updateMarker" class="btn btn-default btn-sm" title="Informe as coordenadas e clique aqui para visualizar a localização no mapa!"><i class="glyphicon glyphicon-map-marker"></i></button>
		</div>
		<div class="form-group hidden" id="divLatGMS">
			<label class="control-label" for="fldLatitudeGMS" title="Clique no mapa com a telca CTRL pressionada para peencher os campos latitude e longitude com as coordenadas do ponto clicado!" data-tooltip-icon="pushpin">Latitude</label>
			<br />
			<div class="input-group">
				<div class="input-group">
					<span class="input-group-addon">G</span>
					<input id="fldGLat" type="text" class="fld form-control fld50" required="true">
				</div>
				&nbsp;
				<div class="input-group">
					<span class="input-group-addon">M</span>
					<input id="fldMLat" type="text" class="fld form-control fld50" required="true">
				</div>
				&nbsp;
				<div class="input-group">
					<span class="input-group-addon">S</span>
					<input id="fldSLat" type="text" data-mask="99,00000" class="fld form-control fld90" required="true">
				</div>
				&nbsp;
				<div class="input-group">
					<span class="input-group-addon">H</span>
					<select id="fldHLat" class="fld form-control fld100 fldSelect">
						<option value="S">Sul</option>
						<option value="N">Norte</option>
					</select>
				</div>
			</div>
		</div>
		<br class="brLatLon hidden"/>
		<div class="form-group hidden" id="divLonGMS">
			<label class="control-label">Longitude</label>
			<br />
			<div class="input-group">
				<div class="input-group">
					<span class="input-group-addon">G</span>
					<input id="fldGLon" type="text" class="fld form-control fld50" required="true">
				</div>
				&nbsp;
				<div class="input-group">
					<span class="input-group-addon">M</span>
					<input id="fldMLon" type="text" class="fld form-control fld50" required="true">
				</div>
				&nbsp;
				<div class="input-group">
					<span class="input-group-addon">S</span>
					<input id="fldSLon" type="text" data-mask="99,00000" class="fld form-control fld90" required="true">
				</div>
				&nbsp;
				<div class="input-group">
					<span class="input-group-addon">H</span>
					<select id="fldHLon" class="fld form-control fld100 fldSelect">
						<option value="W">Oeste</option>
						<option value="E">Leste</option>
					</select>
				</div>
				&nbsp;
				<button data-action="selGeo.changeGDGMS" class="btn btn-default btn-sm" title="Utilizar o formato GD">GD</button>
				&nbsp;
				<button data-action="selGeo.updateMarker" class="btn btn-default btn-sm" title="Informe as coordenadas e clique aqui para visualizar a localização no mapa!"><i class="glyphicon glyphicon-map-marker"></i></button>
			</div>
		</div>


		<br>
		<div class="form-group w100p">
			<label for="txLocal" class="control-label">Observações Sobre a Coordenada</label>
			<br>
			<textarea name="txLocal" id="txLocal" rows="4" class="fld fldTextarea w100p"></textarea>
		</div>
		<div class="fld panel-footer">
			<button id="btnSaveFrmModalSelGeo" data-action="selGeo.saveFrmModalSelGeo" class="fld btn btn-success">Gravar</button>
			<button id="btnResetFrmModalSelGeo" data-action="selGeo.resetFrmModalSelGeo" class="fld btn btn-info hide">Limpar Formulário</button>
		</div>
	</form>
	<div id="divGridSelGeo" class="mt10">
	</div>
</div>

<div class="col-sm-6" style="background-color:#efefef">
     <div id="mapSelGeo" class="map" data-map-on-load="selGeo.mapInit" style="max-height:570px;"></div>
</div>
</div>