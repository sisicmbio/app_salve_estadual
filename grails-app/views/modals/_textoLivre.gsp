<div class="text-left" style="margin:0px 5px 0px 5px">
    <form class="form-inline" role="form" method="POST" id="frmTextoLivre">
        <div class="form-group w100p">
            <label for="fldTextoLivre${id?:''}">${label?:''}</label>
            <br/>
            <textarea id="fldTextoLivre${id?:''}" name="fldTextoLivre"
                      maxlength="${maxLength?:500}"
                      modified="N"
                      class="fld form-control fldTextarea w100p text-justify" style="height:162px;min-height:100px">${currentText}</textarea>
            <span class="red" style="font-size:12px;">Máximo ${maxLength?:'500'} caracteres</span>
        </div>

        %{-- botão --}%
        <div class="fld panel-footer">
            <button data-action="btnGravarTextoLivreClick" data-params="id:${id?:''},fldTextoLivre${id?:''}" class="fld btn btn-success">${btnLabel?:'Gravar'}</button>
        </div>
    </form>
</div>
