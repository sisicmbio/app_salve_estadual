<!-- view: /views/modals/alterarSituacaoFichaParaExcluida.gsp -->
<!-- controller:${params?.controller} -->
<!-- action: ${params?.action} -->
<div class="lazy-load">
   /modals/alterarSituacaoFichaParaExcluida
</div>
<div class="window-container text-left">
    <form class="form form-inline" role="form" id="frmModalAlterarSituacaoFichaParaExcluida">
        <div class="form-group w100p">
            <label style="font-size:2rem;font-weight: bold;">Justificativa</label>
            <textarea name="dsJustificativaExclusao"
                      id="dsJustificativaExclusao" class="form-control fld w100p fldTextarea text-justify" style="font-size: 17px !important;" rows="10">Lendo histórico...</textarea>
        </div>
        %{-- BOTOES --}%
        <div class="panel-footer mt25" id="divFooter" style="display: none">
            <button tyle="button" class="btn btn-success" data-action="frmAlterarSituacaoFichaParaExcluida.save">Gravar</button>
        </div>
    </form>
    %{-- fim form --}%

</div>
