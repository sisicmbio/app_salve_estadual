<div id="frmModalSelAmeaca" class="form-inline" role="form" style="padding:10px;">
    <div class="form-group w100p">
        <div class="text-center mb5">
            <button id="btnTreeViewReset" class="btn btn-default btn-sm hidden">Limpar</button>
            <button id="btnTreeViewAll" class="btn btn-default btn-sm">Marcar Todos</button>
            <button id="btnTreeViewNone" class="btn btn-default btn-sm">Desmarcar Todos</button>
            <button id="btnTreeViewExpandAll"   class="btn btn-default btn-sm">Expandir Todos</button>
            <button id="btnTreeViewCollapseAll" class="btn btn-default btn-sm">Fechar Todos</button>
        </div>
        <input type="text" id="fldFiltrarTreeView" value="" class="form-control mb10" style="width:100% !important" placeholder="Localizar..." autocomplete="off"/>
        <div id="divTreeviewPath" class="blue">${params.dePath ?: ''}</div>
    </div>
</div>
<div id="div-treeChecks" style="height:auto;overflow: auto;position: relative">
    <table id="treeChecks" class="table table-condensed table-hover table-striped fancytree-fade-expander treeTable">
        <colgroup>
            <col width="*"></col>
            <col width="180px"></col>
            <col width="160px"></col>
        </colgroup>
        <thead>
        <tr>
            <th>Descrição</th>
            <th>Referência temporal</th>
            <th>Peso</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td></td>
            <td>
                <select class="form-control fld150 selectRT" style="display:none;" name="classificacao">
                    <option value="0" selected="true"></option>
                    <g:each var="item" in="${referenciasTemporais}">
                        <option data-codigo-sistema="${item.codigoSistema}" value="${item.id}">${item.descricao}</option>
                    </g:each>
                </select>
            </td>
            <td>
                <select class="form-control fld150 selectPeso" style="display:none;" name="classificacao">
                    <option value="0" selected="true"></option>
                    <option value="1">Secundário (1)</option>
                    <option value="2">Principal (2)</option>
                    </select>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div class="fld panel-footer">
    <button id="btnTreeViewSave" data-action="ameaca.saveTreeAmeaca"  class="btn btn-success">Gravar Alterações</button>
</div>
