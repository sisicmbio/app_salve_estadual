//# sourceURL=visualizarInformativo.js
var visualizarInformativo = {
    init : function() {
        var data = $("#modalVisualizarInformativoWrapper").data();
        visualizarInformativo.getInformativo( data.numeroInicial,false )
    },
    getInformativo:function(nuInformativo, marcarComoLido) {
        marcarComoLido = marcarComoLido === false ? false : true;
        var data = { nuInformativo:nuInformativo, marcarComoLido:marcarComoLido ? 'S' : 'N' }
        app.ajax(app.url + 'informativo/show',
            data,
            function( res ) {
               $("#modalVisualizarInformativoWrapper").html( res );
            });
    },
}
visualizarInformativo.init();