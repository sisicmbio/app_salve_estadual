//# sourceURL=apoioShapeBacia.js
//
var apoioShapeBacia = {
    map:null,
    init:function(data)
    {
        app.focus("txGeo");
        // aguardar o form ser renderizado na janela
        window.setTimeout(function(){
            apoioShapeBacia.map = corpGeo.initMap('mapShapeFile');
            //ler os dados da tabela dados_apoio_geo
            app.ajax(app.url + 'dadosApoio/getGeoData', data,
                function(res) {
                   if (res.status == 0) {
                        $("#frmApoioShapeBacia #sqDadosApoio").val(res.data.id);
                        $("#frmApoioShapeBacia #txGeo").val(res.data.txGeo);
                        $("#frmApoioShapeBacia #cdGeo").val(res.data.cdGeo);
                        if( res.data.geojson ) {
                            corpGeo.showGeojson(apoioShapeBacia.map, 'layerShapefile', res.data.geojson, true);
                        }
                   }
                }, null, 'json'
            );

        },1000);

    },
    shapeFileChange:function(input){
        corpGeo.clearLayer( apoioShapeBacia.map,'layerShapefile' );
        corpGeo.loadShapefile(input.files[0], apoioShapeBacia.map,'layerShapefile',true);
    },
    reset:function(){
        app.reset('frmApoioShapeBacia','sqDadosApoio');
        corpGeo.clearLayer( apoioShapeBacia.map,'layerShapefile' );
        app.focus('txGeo');
    },

    save : function(params) {
        var data = {'sqDadosApoio':$("#sqDadosApoio").val()
            ,'txGeo':$("#txGeo").val()
            ,'cdGeo':$("#cdGeo").val()
            ,'wkt':corpGeo.generateLayerWKT(apoioShapeBacia.map,'layerShapefile')
        };
        var errors = [];
        if( ! data.txGeo ){
            errors.push('Nome da bacia deve ser informado')
        }

        if( ! data.wkt ){
            errors.push('Shape deve ser informado')
        }

        if( errors.length > 0 ){
            app.growl(errors.join('<br>'),'4','Erros encontrados','tc','','error');
            return;
        }
        app.ajax(baseUrl+'dadosApoio/saveGeo',data,function(res){
            if( res.status == 0 ) {
                //apoioShapeBacia.reset();
            }
        },'Gravando...')
    },

};
