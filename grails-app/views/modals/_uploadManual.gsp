<div class="window-container text-left">
    <form name="frmUpdateManual" id="frmUpdateManual" class="form-inline" role="form" enctype='multipart/form-data'>
        <div class="fld form-group" id="divFichaAnexo">
            <label for="file">Selecionar Arquivo Pdf do Manual</label>
            <br />
            <input name="fldArquivoZip" type="file" id="file" class="file-loading" required="true" accept="pdf/*"
            data-show-preview="false"
            data-show-upload="false"
            data-show-caption="true"
            data-show-remove="true"
            data-max-file-count="1"
            data-allowed-file-extensions='["pdf"]'
            data-main-class="input-group-sm"
            data-max-file-size="20000"
            data-preview-file-type="object">
        </div>
        <div class="fld panel-footer">
            <button id="btnSaveFrmUpdateManual" data-action="app.updateManual" class="fld btn btn-success">Enviar</button>
        </div>
    </form>
</div>