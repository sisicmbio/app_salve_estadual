<!-- view: /views/modals/visualizarPendenciaRevisaoFicha.gsp -->
<!-- controller:${params?.controller} -->
<!-- action: ${params?.action} -->
<div class="window-container text-left" style="height:auto;" id="divVisualizarPendenciaRevisaoWrapper">
    <input type="hidden" id="modalVisualizarPendenciaRevisaoChanged" value="N">
    <table class="table table-hover table-striped table-condensed table-bordered">
        <thead>
        <th>Data</th>
        <th>Cadastrada por</th>
        <th>Descrição</th>
        <th>Situação</th>
        <th>Envio do e-mail</th>
        <th>Ação</th>
        </thead>
        <tbody>
        <g:each var="fichaPendencia" in="${pendencias}">
            <tr id="trPendenciaModalVisualizarPendenciaRevisao${fichaPendencia.id.toString()}">

                %{-- DATA --}%
                <td style="width:100px;" class="text-center">${fichaPendencia.dtPendencia.format('dd/MM/yyyy')}</td>

                %{-- CADASTRADA POR --}%
                <td class="text-center">${br.gov.icmbio.Util.nomeAbreviadoReverse( fichaPendencia.usuario.noPessoa) }</td>

                %{-- TEXTO --}%
                <td id="tdTextoPendenciaModalVisualizarPendenciaRevisao${fichaPendencia.id.toString()}" style="width: auto;text-align: justify;">${raw(fichaPendencia.txPendencia)}</td>

                %{-- LOG--}%
                <td id="tdLogModalVisualizarPendenciaRevisao${fichaPendencia.id.toString()}" class="text-center" style="width:180px;max-width: 180px;"> ${raw( fichaPendencia.logRevisao() ?: 'Pendente')} </td>

                %{-- EMAIL--}%
                <td class="text-center" style="width:260px">

                <g:each var="email" in="${fichaPendencia?.pendenciaEmail}">
                        <g:if test="${email.txLog }">
                            <g:if test="${ email.dtEnvio }">
                                <g:set var="emailParts" value="${email.txLog.split(/\|/)}"></g:set>
                                <p title="${ emailParts.size() > 1 ? emailParts[1] : emailParts[0] }">
                                    ${ email.dtEnvio ? email.dtEnvio.format('dd/MM/yyyy') + ' - '+ emailParts[0] : 'Agendado'}
                                </p>
                            </g:if>
                            <g:else>
                               <p>${email.txLog}</p>
                            </g:else>
                        </g:if>
                        <g:else>
                            <p>
                            ${ email.dtEnvio ? email.dtEnvio.format('dd/MM/yyyy') : 'Agendado'}
                            </p>
                        </g:else>

                    </g:each>
                </td>

                %{-- ACAO--}%
                <td class="td-actions">

                <label title="${fichaPendencia.usuarioResolveu ? 'Marcar/desmarcar como revisada':'Aguardando ser resolvida'}">
                    Revisada?&nbsp;<input type="checkbox" class="checkbox-lg"
                       ${ fichaPendencia.dtRevisao ? 'checked' : ''}
                       value="S"
                       ${fichaPendencia.usuarioResolveu ? '':'disabled' }
                       data-params="contexto:revisao,sqFicha:${fichaPendencia.ficha.id},sqFichaPendencia:${fichaPendencia.id}"
                       data-action="publicacao.situacaoPendenciaChange" />
                </label>
                    <a data-action="publicacao.editarPendencia" data-params="contexto:revisao" data-sq-ficha-pendencia="${fichaPendencia.id}" class="btn btn-default btn-xs btn-update" title="Alterar">
                        <span class="glyphicon glyphicon-pencil"></span>
                    </a>

                    <a data-action="publicacao.excluirPendencia" data-params="contexto:revisao" data-sq-ficha-pendencia="${fichaPendencia.id}" class="btn btn-default btn-xs btn-delete" title="Excluir">
                        <span class="glyphicon glyphicon-remove"></span>
                    </a>

                    </td>
                </tr>
        </g:each>

        </tbody>
    </table>

</div>
