   <div class="window-container text-left">
      <%-- este form deverá ser carregado via ajax dependendo do campo da ficha --%>
      <form id="frmSelRefBibFichaTemp" name="frmSelRefBibFichaTemp" class="form-inline" role="form">
            <ul class="nav nav-tabs" id="ulTabSelRefBibTemp">
               <li class="active"><a data-tab="1" data-toggle="tab" href="#tabSelRefBibTemp">Referência Bibliográfica</a></li>
               <li><a data-tab="2" data-toggle="tab" href="#tabSelComPessoalTemp">Comunicação Pessoal</a></li>
            </ul>
            %{-- container abas --}%
            <div class="tab-content clearfix">
               %{-- inicio aba Referencia Bibliografica --}%
               <div role="tabpanel" class="tab-pane active" id="tabSelRefBibTemp">
                  <div class="form-group w100p">
                      <label class="control-label" for="sqPublicacao"
                             title="Exemplos de pesquisas:<br>- por palavra chave, ex: banco de dados, peixes fluviais, relatório técnico, Gilberto etc<br>- por autor e ano, ex: Paulo 2015, Rodrigo 2020<br>- pelo ID da referência, ex: id: 123456">Referência bibliográfica</label>
                      <br>
                     <select id="sqPublicacao" name="sqPublicacao" data-change="ficha.sqPublicacaoTempChange" class="fld form-control select2 fld800"
                        data-s2-url="ficha/select2RefBib"
                        data-s2-placeholder="Buscar por... ( min 3 caracteres ) ou id:9999"
                        data-s2-minimum-input-length="3"
                        data-s2-template="refBibTemplate"
                        data-s2-params="contexto"
                        data-s2-auto-height="true">
                     </select>
                     <a data-action="modal.cadRefBib()" class="fld btn btn-sm btn-default" title="Cadastrar Referência Bibliográfica">
                        <span class="glyphicon glyphicon glyphicon-plus" aria-hidden="true"></span>
                     </a>
                  </div>
               </div>
               <div role="tabpanel" class="tab-pane" id="tabSelComPessoalTemp">
                  <div class="form-group">
                     <label class="control-label">Autor</label>
                     <br>
                     <input type="text" name="noAutorRef" id="noAutorRef" class="form-control fld400" maxlength="100" value="" />
                  </div>
                  <div class="form-group">
                     <label class="control-label">Ano</label>
                     <br>
                     <input type="text"  name="nuAnoRef" id="nuAnoRef" class="fld form-control fld60" data-mask="0000"  maxlength="4" value=""/>
                  </div>
               </div>
            </div>
            %{-- fim container abas --}%
           <div class="panel-footer mt10">
               <center>
                   <a id="btnCloseFrmSelRefBibFichaTemp" data-action="app.closeWindow('selRefBibTemp')" data-form="frmSelRefBibFicha" data-params="sqFicha" class="btn fld btn-success btn-sm">Ok</a>
               </center>
            </div>

   </form>
   </div>
</div>
