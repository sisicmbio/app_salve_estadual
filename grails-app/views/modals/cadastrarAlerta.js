//# sourceURL=cadastrarAlerta.js
//
var cadastroAlerta = {
    init: function (data) {
        setTimeout(function(){
            app.focus('dtInicioAlerta','frmModalCadAlerta')
        },1000);

    },
    tabClick: function(data, link, evt) {
        return true;
    },
    save: function (params) {
        var data = {};
        var erros=[];

        if ( ! $('#frmModalCadAlerta').valid() ) {
            return false;
        }

        data.sqAlerta       = $("#frmModalCadAlerta #sqAlerta").val();
        data.dtInicioAlerta = $("#frmModalCadAlerta #dtInicioAlerta").val();
        data.dtFimAlerta    = $("#frmModalCadAlerta #dtFimAlerta").val();
        data.inPublicado    = $("#frmModalCadAlerta #inPublicado:checked").size() == 1 ? 'S': 'N';
        data.txAlerta       = $("#frmModalCadAlerta #txAlerta").val();


        if( ! data.txAlerta ) {
            erros.push('Necessário informaar a mensagem.');
        }
        if( ! data.dtFimAlerta ) {
            erros.push('Necessário informaar a data final do período de exibição do alerta.');
        }

        if( erros.length > 0 ) {
            return app.alertInfo(erros.join('<br>') );
        }

        app.ajax(app.url + 'informativo/saveAlerta', data,
            function (res) {
                if (res.status == 0) {
                    //cadastroAlerta.reset();
                    // atualizar a linha dos gride que foi alterada
                    cadastroAlerta.updateGridLine(res.data);
                }
            }, null, 'json'
        );
    },
    reset: function () {
        app.reset('frmModalCadAlerta');
        app.resetChanges('cadAlertaWrapper');
        $('#frmModalCadAlerta #inPublicado').prop('checked',true);
    },
    edit:function( params ){
        var data={sqAlerta:params.sqAlerta};
        app.ajax(app.url + 'informativo/editAlerta',
            params,
            function(res) {
                //$('#frmModalCadAlerta #file').prop('required',false);
                app.setFormFields(res,'frmModalCadAlerta');
                app.selectTab("liTabCadAlertaForm");
                app.focus('txInformativo');
            });
    },
    delete:function(params,btn) {
        app.selectGridRow(btn);
        var data={sqAlerta:params.sqInformativo};
        app.confirm('<br>Confirma exclusão do alerta?',
            function() {
                app.ajax(app.url + 'informativo/deleteAlerta',
                    data,
                    function( res ) {
                        $("#tr-grid-alerta-" + data.sqAlerta).remove();
                        // se excluir o registro em edição, limpar o formulario
                        if( data.sqAlerta == $("#frmModalCadAlerta #sqAlerta").val() ) {
                            cadastroAlerta.reset();
                        }
                    });
            },
            function() {
                app.unselectGridRow(btn);
            }, params, 'Confirmação', 'warning');
    },

    /** atualizar uma unica linha do gride*/
    updateGridLine:function( params) {
        var data = {sqAlerta:params.sqAlerta};
        var $table =$("#divGridAlertas table");
        if( $table.size() == 0 ) {
            return;
        }
        app.ajax(app.url + 'informativo/getGridAlerta', data,function(res) {
            var $tr=$("#tr-grid-alerta-"+data.sqAlerta);
            if( $tr.size() == 1 ){
                $tr.html( $(res).find('tr#tr-grid-alerta-' + data.sqAlerta).html() );
                app.selectTab('liTabCadAlertaHistorico');
            } else {
                $table.find('tbody').prepend($(res).find('tr#tr-grid-alerta-' + data.sqAlerta));
            }
        });
    },
    showPessoas:function( params ) {
        var data={sqAlerta:params.sqAlerta};
        app.ajax(app.url + 'informativo/showPessoas',
            params,
            function(res) {
                var pessoas=[]
                if( res.data.pessoas ) {
                    res.data.pessoas.map( function(item,i) {
                        pessoas.push('<li>'+item.no_pessoa + ' em '+item.dt_visualizacao + '</li>');
                    });
                }
                var conteudo  =  ( pessoas.length > 0 ? pessoas.join('') : 'Não foi lido até o momento!' );
                app.alertInfo('<div style="max-height: 400px;overflow-y: auto"><ol>'  + conteudo +'</ol></div>','Lido por');
            });
    }
}
cadastroAlerta.init();