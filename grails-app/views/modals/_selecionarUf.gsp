<div id="frmModalSelUf" class="form-inline" role="form" style="padding:10px;">
    <div class="form-group w100p">
        <div class="text-center mb5">
            <button id="btnTreeViewReset" class="btn btn-default btn-sm hidden">Limpar</button>
            <button id="btnTreeViewAll" class="btn btn-default btn-sm">Marcar Todos</button>
            <button id="btnTreeViewNone" class="btn btn-default btn-sm">Desmarcar Todos</button>
        </div>
        <input type="text" id="fldFiltrarTreeView" value="" class="form-control mb10" style="width:100% !important" placeholder="Localizar..." autocomplete="off"/>
    </div>
</div>
<div id="treeChecks" style="height:auto;overflow: auto;position: relative"></div>
<div class="fld panel-footer">
    <button id="btnTreeViewSave" data-action="disUf.saveTreeUf"  class="btn btn-success">Gravar Alterações</button>
</div>
