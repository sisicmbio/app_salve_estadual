<!-- view: /views/modals/alterarArvoreTaxonomica.gsp -->
<!-- controller:${params?.controller} -->
<!-- action: ${params?.action} -->

%{--carregar o arquivo javascript alterarArvoreTaxonomica.js--}%
<div class="lazy-load">
    /modals/alterarArvoreTaxonomica
</div>

<div class="window-container text-left">
    <form class="form form-inline" role="form" id="frmModalAlterarArvoreTaxonomica">
        <input type="hidden" id="sqTaxonAntigo" value="">
        <input type="hidden" id="sqTaxonFilho" value="">
        %{-- DE --}%
        <div class="form-group">
            <label>Alterar a estrutura atual</label><br>

            <table class="table table-condensed table-sm table-bordered" id="tableAlterarArvoreTaxonomica">
                <tr>
                    <g:each var="item" in="${params.listArvoreAtual}">
                        <td class="text-center font-weight-bold" style="padding:3px !important;background-color:${item.sqTaxonFilho ? 'transparent':'#efefef'}">
                            <span>${item.noNivel}</span>
                        </td>
                    </g:each>
                </tr>
                <tr>
                    <g:each var="item" in="${params.listArvoreAtual}">
                        <g:if test="${ item.sqTaxonFilho }">
                            <td title="Clique para alterar este nílvel" class="text-center nivel-link" style="padding:3px !important;" id="td${item.noNivel}">
                                <a href="javascript:void(0);" onclick="frmAlterarArvoreTaxonomica.nivelClick(this)"
                                   data-no-nivel="${item.noNivel}" data-codigo="${item.coTaxon}"
                                   data-sq-taxon-filho="${item.sqTaxonFilho}"
                                   data-sq-taxon="${item.sqTaxon}">${item.noTaxon}</a>
                            </td>
                        </g:if>
                        <g:else>
                            <td class="text-center" style="padding:3px !important;background-color: #efefef">
                               <span>${item.noTaxon}</span>
                            </td>
                        </g:else>
                    </g:each>
                </tr>
            </table>
        </div>
        %{-- PARA--}%
        <div class="form-group w100p" style="display: none" id="divSelectSqTaxonNovo">
            <label class="text-left" style="font-size:2rem;font-weight: bold;width:auto;" id="labelAlterarPara">Alterar para:</label>
            <br>
            <select id="sqTaxonNovo" class="form-control select2 fld250"
                data-s2-params="sqTaxonAntigo"
                data-s2-url="ficha/select2TaxonAlteracao"
                data-s2-placeholder=" "
                data-s2-minimum-input-length="0">
        </select>

%{--        MOTIVO DA MUDANÇA--}%
        <div class="form-group form-group-sm w100p">
            <label>Motivo da mudança</label> <br>
            <textarea rows="5" name="txHistorico" id="txHistorico" placeholder="Explique o motivo..."
                        class="form-control w100p" style="resize: none;"></textarea>
        </div>


        %{-- BOTOES --}%
        <div class="panel-footer mt25" id="divFooter">
            <button tyle="button" class="btn btn-success" data-params="sqFicha,sqTaxonAntigo|sqTaxonPaiAntigo,sqTaxonNovo|sqTaxonPaiNovo,sqTaxonFilho|sqTaxon,txHistorico" data-action="frmAlterarArvoreTaxonomica.save">Gravar</button>
        </div>
    </form>
    %{-- fim form --}%
</div>
