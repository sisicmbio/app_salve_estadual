<!-- view: /views/modals/_modalHistoricoSituacaoExcluida.gsp -->
<!-- controller:${params?.controller} -->
<!-- action: ${params?.action} -->
<div id="modalHistoricoSituacaoExcluidaWrapper">
<table class="table table-borderless table-condensed table-hover table-striped">
    <thead>
        <tr>
            <th style="width:90px;">Data</th>
            <th style="width:250px;">Usuário</th>
            <th style="width: auto;">Justificativa</th>
        </tr>
    </thead>
    <tbody>
    <g:each var="item" in="${listHistorico}">
        <tr>
            <td class="text-center">
                ${item.dt_inclusao}
            </td>
            <td class="text-left">
                ${item.no_pessoa}
            </td>
            <td class="text-center" style="text-align: justify">
                ${raw(br.gov.icmbio.Util.stripTagsFichaPdf(item.ds_justificativa))}
            </td>
        </tr>
    </g:each>
    </tbody>
</table>
