<!-- view: /views/modals/selecionarFichaCicloAnterior.gsp -->
<!-- controller:${params?.controller} -->
<!-- action: ${params?.action} -->
%{--<div class="lazy-load">
    /modals/alterarTaxonFicha
</div>--}%
<div class="window-container text-left">
    <g:if test="${ cicloAnterior }">
        <form class="form form-inline" role="form" id="frmModalSelecionarFichaCicloAnterior">
            <div class="form-group">
                <label style="font-size:2rem;font-weight: bold;">Selecionar ficha&nbsp;</label>
                <br>
                <select name="sqFichaCicloAnterior" id="sqFichaCicloAnterior" class="form-control select2 fld500"
                        data-s2-params="sqCicloAvaliacao"
                        data-s2-url="ficha/select2FichaCicloAnterior"
                        data-s2-placeholder="nome científico..."
                        data-s2-minimum-input-length="3"
                        data-s2-maximum-selection-length="1">
                </select>
            </div>
            %{-- BOTOES --}%
            <div class="panel-footer mt25" id="divFooter">
                <button tyle="button" class="btn btn-success"
                        data-params="sqFicha:${params.sqFicha},sqFichaCicloAnterior"
                        data-action="ficha.saveFichaCicloAnterior">Gravar</button>
            </div>
        </form>
        </g:if>
    <g:else>
        <div class="alert alert-info">
            <span>Ciclo anterior inexistente!</span>
        </div>
    </g:else>
    %{-- fim form --}%
</div>
