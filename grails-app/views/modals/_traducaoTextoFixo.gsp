<!-- view: /views/modals/_traducaoTextoFixo -->
<!-- controller:${params?.controller} -->
<!-- action: ${params?.action} -->
<div class="lazy-load">
    /modals/traducaoTextoFixo
</div>

<div id="modalTraducaoTextoFixoWrapper" style="padding:10px;">

    %{-- inicio form --}%
    <form class="form-inline" role="form" id="frmModalTraducaoTextoFixo">
        <input type="hidden" name="sqTraducaoTexto" id="sqTraducaoTexto" value="">
        <input type="hidden" name="inSaved" id="inSaved" value="N">
        <div class="form-group w100p">
            <label class="label-required" for="txOriginal">Texto</label>
            <br>
            <div class="input-group w100p">
                <input type="hidden" name="noTabelaTraducao" id="noTabelaTraducao" value="${params.noTabelaTraducao ?:''}">
                <input type="hidden" name="idRegistroTraducao" id="idRegistroTraducao" value="${params.idRegistroTraducao}">
                <input name="txOriginal" id="txOriginal" type="text" class="fld form-control" value="${ params?.txOriginal ? params.txOriginal.toString().trim() : ''}" ${params?.noTabelaTraducao ? 'readonly':''}>
                <span class="input-group-btn"><button class="btn btn-default btn-sm" data-action="traducaoTextoFixo.traduzir" type="button" title="Informe a palavra ou texto e clique aqui para fazer a tradução automática."><img src="/salve-estadual/assets/en.png" class="traducao-flag"/></button></span>
            </div>
        </div>

        <br>

        <div class="form-group w100p">
            <label class="label-required" for="txTraduzido">Tradução automática</label>
            <br>
            <input name="txTraduzido" id="txTraduzido" readonly value="${params?.txTraduzido ?params.txTraduzido.toString().trim() : '' }" type="text" class="fld form-control w100p">
        </div>
        <br>

        <div class="form-group w100p">
            <label class="label-required" for="txTraduzido">Tradução revisada</label>
            <br>
            <input name="txRevisado" id="txRevisado" value="${params?.txRevisado ? params.txRevisado.toString().trim() : '' }" type="text" class="fld form-control w100p">
        </div>

        %{-- BOTOES --}%
        <div class="panel-footer mt20" id="divFooter">
            <a href="#" id="btnSaveTraducao" class="btn btn-success" data-action="traducaoTextoFixo.save">Gravar</a>
            <a href="#" id="btnResetFormTraducao" class="btn btn-default" data-action="traducaoTextoFixo.reset">Limpar</a>
            <br/>
        </div>
    </form>
    <g:if test="${ ! params.noTabelaTraducao }">
    %{-- fim form --}%
    %{--    GRIDE--}%
    <div id="containerGridTraducaoTextoFixo"
         data-height="400px"
         data-field-id="sq_traducao_texto"
         data-url="traducao/getGridTraducaoTextoFixo">
        <g:render template="/traducao/gridTraducaoTextoFixo"
                  model="[gridId: 'GridTraducaoTextoFixo']"></g:render>
    </div>
    </g:if>

</div> %{-- FIM CONTAINER--}%
