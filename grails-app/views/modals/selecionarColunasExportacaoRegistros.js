//# sourceURL=/views/modals/selecionarColunasExportacaoRegistros.js
;
var selColsExportRegistros = {
    form:null,
    btnSelecionarTodos:null,
    init: function () {
        selColsExportRegistros.form = $("#frmselecionarColunasExportacaoRegistros");
        selColsExportRegistros.btnSelecionarTodos = selColsExportRegistros.form.find('a#btnSelecionarTodos');
    },
    marcarDesmarcarTodos:function(){
        var data =selColsExportRegistros.btnSelecionarTodos.data();
        selColsExportRegistros.form.find('input[type=checkbox]').prop('checked',!data.checked);
        selColsExportRegistros.btnSelecionarTodos.data('checked', !data.checked );
    },
    exportar:function(){

    }
}
selColsExportRegistros.init();
