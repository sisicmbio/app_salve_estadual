<!-- view: /views/modals/alterarTaxonFicha.gsp -->
<!-- controller:${params?.controller} -->
<!-- action: ${params?.action} -->
<div class="lazy-load">
   /modals/alterarTaxonFicha
</div>
<div class="window-container text-left">
    <form class="form form-inline" role="form" id="frmModalAlterarTaxonFicha">
        %{--<input type="hidden" name="w_idGenero" value="${structure.idGenero}">--}%
        %{--<input type="hidden" name="w_idOrdem" value="${structure.idOrdem}">--}%
        <input type="hidden" name="w_idClasse" value="${structure.idClasse}">
        <div class="form-group">
            <p class="form-control-static" style="font-size:1.5rem;font-weight: bold;">Classe:&nbsp;${structure.nmClasse}</p><br/>
            <p class="form-control-static" style="font-size:2rem;font-weight: bold;">De:<span style="margin-left:20px;" id="spanNomeTaxonAtual">Carregando...</span></p>
        </div>
        <br>
        <div class="form-group">
            <label style="font-size:2rem;font-weight: bold;">Para:&nbsp;</label>
            <g:if test="${params.especie}">
                <label>
                    <select name="sqTaxonNovo" class="form-control select2 fld400"
                            data-s2-params="prioridade:9,nivel:'ESPECIE',w_idClasse"
                            data-s2-url="ficha/select2Taxon"
                            data-s2-placeholder="${params.especie?'Espécie':'Subespécie'}"
                            data-s2-minimum-input-length="3"
                            data-s2-maximum-selection-length="2">
                </select>
                </label>
            </g:if>
            <g:else>

                <select name="cdNivelPesquisar" class="form-control fld150" style="padding-top: 2px;">
                    <option value="ESPECIE">Espécie</option>
                    <option value="SUBESPECIE" selected>Subespécie</option>
                </select>

                <select name="sqTaxonNovo" class="form-control select2 fld400"
                        data-s2-params="prioridade:10,nivel:'SUBESPECIE',w_idGenero,cdNivelPesquisar"
                        data-s2-url="ficha/select2Taxon"
                        data-s2-placeholder=" "
                        data-s2-maximum-selection-length="2">
                </select>
            </g:else>
        </div>

        %{-- BOTOES --}%
        <div class="panel-footer mt25" id="divFooter">
            <button tyle="button" class="btn btn-success" data-action="frmAlterarTaxonFicha.save">Gravar</button>
           %{-- <g:if test="${session?.sicae?.user?.isADM()}">
                <button tyle="button" class="btn btn-secondary pull-right"
                        title="Utilize esta opção para alterar a árvore taxonômica da espéce/subespecie."
                    data-sq-ficha="${params.sqFicha}"
                    data-action="ficha.alterarArvoreTaxonomica">Alterar Árvore Taxonômica </button>
            </g:if>--}%
        </div>
    </form>
    %{-- fim form --}%

</div>
