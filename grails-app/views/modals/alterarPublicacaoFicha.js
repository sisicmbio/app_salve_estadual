//# sourceURL=alterarPublicacaoFicha.js
//
var alterarPublicacaoFicha = {
    idPublicacaoSelecionada:'',
    init:function()
    {
        alterarPublicacaoFicha.idPublicacaoSelecionada = $("#sqPublicacaoSelecionada").val();
        alterarPublicacaoFicha.updateGrid();
    },
    save:function(params)
    {
      alert('salvar');
    },
    saveDelete:function(params)
    {
        alert('salvar e excluir');
    },
    updateGrid:function(params)
    {
        //$("#divGridFichasComPublicacao").html('<h2>aqui vai o gride via ajax da publicacao ' + alterarPublicacaoFicha.idPublicacaoSelecionada+'</h2>');
        var data = {sqPublicacao : alterarPublicacaoFicha.idPublicacaoSelecionada };
        var url = app.url + "referenciaBibliografica/getGridFichasPublicacao"
        $("#divGridFichasComPublicacao").html('Carregando...');
        app.ajax( url, data,
            function(res) {
                if (res) {
                    $("#divGridFichasComPublicacao").html(res);
                }
            }, null, 'html'
        );
    },
};
alterarPublicacaoFicha.init();


