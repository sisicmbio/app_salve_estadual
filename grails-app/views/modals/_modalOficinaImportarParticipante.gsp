<!-- view: /views/modal/_modalOficinaImportarParticipante -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action} -->

<div>
    <div class="text-left" id="divCicloConsultaImportarWrapper">
        <label for="sqCicloConsultaImportar" class="control-label mb5">Selecione:
            <label class="control-label cursor-pointer ml20 label-selected"><input class="checkbox-lg" type="checkbox" checked="true" value="D">&nbsp;Consulta direta</label>
            <label class="control-label cursor-pointer ml20"><input class="checkbox-lg" type="checkbox" checked="true" value="R">&nbsp;Revisão</label>
            <label class="control-label ml20">&nbsp;|&nbsp;</label>
            <label class="control-label cursor-pointer ml20 label-selected"><input class="checkbox-lg" type="checkbox" value="A">&nbsp;Abertas</label>
            <label class="control-label cursor-pointer ml20"><input class="checkbox-lg" type="checkbox" checked="true" value="F">&nbsp;Encerradas</label>
        </label>
        <select id="sqCicloConsultaImportar" name="sqCicloConsultaImportar" data-change="oficina.participante.sqCicloConsultaImportarChange"
                class="form-control fldSelect blue ignoreChange select-consulta mt10 w100p">
            <option value="">-- selecione uma consulta ou revisão --</option>
            <g:if test="${ listConsultas }">
                <g:each var="item" in="${listConsultas}">
                    <option data-situacao="${item.dt_fim >= br.gov.icmbio.Util.hoje()  ? 'ABERTA':'FECHADA'}"
                            data-tipo="${item?.cd_tipo_consulta_sistema}"
                            value="${item.sq_ciclo_consulta}">${ item.dt_inicio.format('dd/MM/yyyy')+ ' a ' + item.dt_fim.format('dd/MM/yyyy')+' - '+item.de_titulo_consulta}</option>
                </g:each>
            </g:if>
        </select>
        <p class="red text-center" style="font-size:8px">Somente os participantes com a instituição informada na consulta/revisão serão listados.</p>
    </div>

    <div>
        <g:render template="/oficina/divGridImportarParticipantes"></g:render>
    </div>
    %{--<table class="table table-borderless table-condensed table-hover table-informativos mt10" id="tableOficinaImportarParticipantes">
        <thead>
        <tr>
            <th class="text-center" style="width:40px;">#</th>
            <th style="width:40px;"
                class="no-export"><i title="Marcar/Desmarcar Todos"
                data-action="app.checkUncheckAll"
                class="glyphicon glyphicon-unchecked"></i></th>
            <th style="width: auto;">Nome</th>
            <th style="width: 90px;">Instituição</th>
        </tr>
        </thead>
        <tbody>
            <td colspan="3">
                <h4>Selecione uma consulta ou revisão para exibir os participantes</h4>
            </td>
        </tbody>
    </table>--}%


    <div class="fld panel-footer">
        <button data-action="oficina.participante.importarParticipante"
                data-params="sqCicloConsultaImportar,sqCicloAvaliacao:${sqCicloAvaliacao},sqOficina:${sqOficina}"
                class="fld btn btn-success">Importar</button>
    </div>
</div>
