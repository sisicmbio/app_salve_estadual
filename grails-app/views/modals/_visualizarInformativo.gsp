<!-- view: /views/modals/_visualizarInformativo.gsp -->
<!-- controller:${params?.controller} -->
<!-- action: ${params?.action} -->
<div class="lazy-load">
    /modals/visualizarInformativo
</div>
<div id="modalVisualizarInformativoWrapper" data-numero-inicial="${params?.numero ?: ''}">
    <h2>Carregando...</h2>
</div>
