<!-- view: /views/modals/_cadastrarInformativo.gsp -->
<!-- controller:${params?.controller} -->
<!-- action: ${params?.action} -->
<div class="lazy-load">
    /modals/cadastrarInformativo
</div>
<div class="window-container text-left" id="cadInformativoWrapper">

    %{-- criar abas cadastro e historico --}%
    <ul class="nav nav-tabs" id="ulTabsCadInformativo">
        <li id="liTabCadInformativoForm" class="active"><a data-toggle="tab" href="#tabCadInformativoForm">Cadastrar</a></li>
        <li id="liTabCadInformativoHistorico"><a data-toggle="tab"
                                                 data-action="informativo.tabClick"
                                                 data-url="informativo/getGrid"
                                                 data-container="divGridInformativos"
                                                 data-reload="false"
                                                 href="#tabCadInformativoPesquisa">Histórico</a></li>
    </ul>
    %{-- Container de todas abas --}%
    <div id="taxTabContent" class="tab-content">
        %{-- inicio aba Form Cad Ref Bib--}%
        <div role="tabpanel" class="tab-pane active" id="tabCadInformativoForm">
            %{-- inicio form --}%
            <form class="form-inline" role="form" id="frmModalCadInformativo" enctype='multipart/form-data'>
                <input type="hidden" name="sqInformativo" id="sqInformativo" value="">

                <div class="form-group mr15">
                    <label class="label-required" for="nuInformativo">Nº</label>
                    <br />
                    <input name="nuInformativo" id="nuInformativo" type="text" class="fld form-control fld80 nu-informativo" data-mask = "00000" value="${proximoNumero}">
                    <i id="btnCalcularProximoNumero" title="Calcular o próximo número" class="fa fa-cog" data-action="informativo.calcularProximoNumero"></i>
                </div>
                <div class="form-group">
                    <label class="label-required" for="dtInformativo">Data</label>
                    <br />
                    <input name="dtInformativo" id="dtInformativo" type="text" class="fld form-control date fld120" value="${new Date().format('dd/MM/yyyy')}">
                </div>

                %{-- PUBLICADO --}%
                <div class="form-group mt10 ml15" style="border-bottom:1px solid silver;">
                <label for="inPublicado" class="control-label cursor-pointer label-checkbox"
                       title="Marque como publicado para que o informativo fique disponível aos usuários."
                       style="margin-top:17px;">Publicado?&nbsp;
                    <input name="inPublicado" id="inPublicado" class="fld checkbox-lg tooltipstered" type="checkbox" checked="true" value="S">
                </label>
                </div>
                <br>

                <div class="form-group">
                    <label class="label-required" for="deTema">Tema do informativo</label>
                    <br />
                    <input name="deTema"  id="deTema" type="text" class="fld form-control fld600">
                </div>

                <br>

                <div class="fld form-group" id="divFichaAnexo">
                    <label class="label-required" for="file">Arquivo pdf</label>
                    <br />
                    <input name="fldAnexo" type="file" id="file" class="file-loading" accept="application/pdf"
                           data-show-preview="false"
                           data-show-upload="false"
                           data-show-caption="true"
                           data-show-remove="true"
                           data-max-file-count="1"
                           data-allowed-file-extensions='["pdf"]'
                           data-main-class="input-group-sm"
                           data-max-file-size="20000"
                           data-preview-file-type="object">
                </div>

                %{-- BOTOES --}%
                <div class="panel-footer mt20" id="divFooter">
                    <a href="#" id="btnSaveCadInformativo" class="btn btn-success" data-action="informativo.save">Gravar</a>
                    <a href="#" id="btnResetCadInformativo" class="btn btn-default" data-action="informativo.reset">Limpar Formulário</a>
                    <br/>
                </div>
            </form>
            %{-- fim form --}%
        </div>
        %{-- fim aba Form Cad Ref Bib--}%

        %{-- inicio aba Form Pesquisa Ref Bib--}%
        <div role="tabpanel" class="tab-pane" id="tabCadInformativoPesquisa">
            <div id="divGridInformativos"></div>
        </div>
        %{-- fim aba Form Pesquisa Ref Bib--}%
    </div>
</div>