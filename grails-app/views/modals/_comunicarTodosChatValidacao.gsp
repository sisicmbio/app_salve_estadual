<div class="text-left" style="margin:0px 5px 0px 5px">
    <form class="form-inline" role="form" method="POST" id="frmComunicarTodosChatValidacao">
        <g:if test="${ listCts || listValidadores }">
        <div>
            <g:if test="${ listCts && listCts.size() > 1 }">
                <div id="divCoordenadoresTaxon" style="border:none;height:auto;max-height: 100px;overflow-y: auto;margin-bottom: 10px;">
                    <fieldset>
                        <legend>Selecione o(s) coordenador(es) de táxon</legend>
                        <g:each var="item" in="${listCts}">
                            <label class="text-nowrap cursor-pointer" style="padding-top:2px;display: block;">
                                <input type="checkbox" class="chkCT" value="${item.sqPessoa}">&nbsp;${ raw( item.nome ) }
                            </label>
                        </g:each>
                    </fieldset>
                </div>
            </g:if>

            <g:if test="${ listValidadores && listValidadores.size() > 1 }">
                <div id="divValidadores" style="border:none;height:auto;max-height: 100px;overflow-y: auto">
                    <fieldset>
                        <legend>Selecione o(s) validador(es)</legend>
                        <g:each var="item" in="${listValidadores}">
                            <label class="text-nowrap cursor-pointer" style="padding-top:2px;display: block;">
                                <input type="checkbox" class="chkVL" value="${item.sqPessoa}">&nbsp;${ raw( item.nome ) }
                            </label>
                        </g:each>
                    </fieldset>
                </div>
            </g:if>
         </div>
        </g:if>

        <div class="form-group w100p">
            <label for="fldMsgEmail${id?:''}">${label?:'Mensagem'}</label>
            <br/>
            <textarea id="fldMsgEmail${id?:''}" name="fldMsgEmail"
                      maxlength="${maxLength?:500}"
                      modified="N"
                      class="fld form-control fldTextarea w100p text-justify" style="height:162px;min-height:100px">${currentText}</textarea>
            <span class="red" style="font-size:12px !important;">Máximo ${maxLength?:'500'} caracteres</span>
        </div>

        %{-- botão --}%
        <div class="fld panel-footer">
            <button name="btnSaveEmailMessage" class="fld btn btn-success">${btnLabel?:'Enviar'}</button>
        </div>
    </form>
</div>
