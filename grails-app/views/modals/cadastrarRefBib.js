//# sourceURL=N:\SAE\sae\grails-app\views\modal\cadastrarRefBib.js
//
;
var refBib = {
    file: null,
    selectedText:'',
    init: function () {
        refBib.sqTipoPublicacaoChange();
    },
    copySelectedText:function(ele){
        refBib.selectedText = window.getSelection().toString().trim();
        try{event.preventDefault();} catch( e){}
    },
    addItalicTags:function( params,ele,evt){
        if( refBib.selectedText){
            var $input = $("input[name=deTitulo]");
            var value = $input.val();
            var selectedText = refBib.selectedText.replace(/<\/?i>/gi,'')
            value = value.replace( refBib.selectedText, '<i>'+selectedText+'</i>');
            $input.val( value );
            refBib.selectedText='';
            $input.focus();

        }
    },
    searchCrossref:function(params,ele,evt) {
        var $deDoi = $("#frmModalCadRefBib input[name=deDoi]");
        if (!$deDoi.val()) {
            app.alert('Informe o nº do DOI');
            return;
        }
        app.blockUI('Pesquisando...');

        // api: https://github.com/CrossRef/rest-api-doc#parameters
        // https://api.crossref.org/works/10.1111/jfb.14169
        // var url = "https://api.crossref.org/works/funders/"+$deDoi.val().trim()
        // https://api.crossref.org/works/10.11646/zootaxa.4496.1.42
        //var url = "https://api.crossref.org/works/" + encodeURIComponent($deDoi.val().trim())+'/agency'
        var url = "https://api.crossref.org/works/" + encodeURIComponent($deDoi.val().trim())
        $.get(url, function (res) {
            if (res && res.status == 'ok') {
                var $form = $("#frmModalCadRefBib");
                var textoTemp = '';
                // preencher os campos do form
                var data = res.message;

                if( data.type ) {
                    var codigo = '';
                    if( data.type=='book-chapter') {
                        codigo='CAPITULO_LIVRO';
                    } else if( data.type=='book') {
                        codigo='LIVRO';
                    }
                    if( codigo ) {
                        var $select = $form.find('#sqTipoPublicacao')
                        var $option = $select.find('option[data-codigo='+codigo+']');
                        if( $option ) {
                            if( $select.val() != $option.attr('value') ) {
                                $select.val( $option.attr('value') );
                                $select.change();
                            }
                        }
                    }

                }

                // AUTORES
                try {
                    if (data.author) {
                        var autores = []
                        data.author.map(function (item) {
                            autores.push(item.family + ' ' + item.given)
                        })
                        if (autores.length > 0) {
                            $form.find("textarea[name=noAutor]").val(autores.join("\n"))
                        }
                    }
                } catch (e) {
                }

                // TITULO
                try {
                    if (data.title && data.title.length > 0) {
                        var titulo = [];
                        data.title[0].split('\n').map(function (item) {
                            titulo.push(String(item).trim());
                        });
                        $form.find("input[name=deTitulo]").val(titulo.join(' '))
                    }
                } catch (e) {}

                // TITULO DO LIVRO
                try {
                    if ( data['container-title'] && data['container-title'].length > 0 ) {
                        var titulo = [];
                        data['container-title'][0].split('\n').map(function (item) {
                            titulo.push(String(item).trim());
                        });
                        $form.find("input[name=deTituloLivro]").val(titulo.join(' '))
                    }
                } catch (e) {}


                // REVISTA CIENTIFICA
                try {
                    if (data["container-title"] && data["container-title"].length > 0) {
                        textoTemp = String(data["container-title"][0]).trim()
                        $form.find("input[name=noRevistaCientifica]").val(textoTemp);
                    }
                } catch (e) {
                }

                // ANO DE PUBLICACAO
                try {
                    if (data["published-print"] && data["published-print"]['date-parts'] && data["published-print"]['date-parts'].length > 0) {
                        //data["published-online"].['date-parts'][0]
                        $form.find('input[name=nuAnoPublicacao]').val(String(data["published-print"]['date-parts'][0][0]).trim());
                    }
                    else if (data["published-online"] && data["published-online"]['date-parts'] && data["published-online"]['date-parts'].length > 0) {
                        //data["published-online"].['date-parts'][0]
                        $form.find('input[name=nuAnoPublicacao]').val(String(data["published-online"]['date-parts'][0][0]).trim());
                    }
                } catch (e) {
                }

                // DATA PUBLICACAO


                // VOLUME
                if (data.volume) {
                    $form.find('input[name=deVolume]').val(String(data.volume).trim());
                }

                // EDICAO - ISSUE
                if (data.issue) {
                    $form.find('input[name=deIssue]').val(String(data.issue).trim());
                }

                // PAGINAS
                if (data.page) {
                    $form.find('input[name=dePaginas]').val(String(data.page).trim());
                }

                // ISSN
                if (data['issn-type'] && data['issn-type'].length > 0) {
                    data['issn-type'].map(function (item) {
                        if (item.type && item.type.toLowerCase() == 'print') {
                            $form.find('input[name=deIssn]').val(String(item.value).trim());
                        }
                    })
                }

                // ISBN
                if (data.ISBN && data.ISBN.length > 0) {
                   $form.find('input[name=deIsbn]').val( String(data.ISBN[0]).trim() );
                }



            } else {
                app.alertInfo('DOI não encontrado!');
            }
        }).always(function () {
            app.unblockUI()
        }).fail(function (xhr, text, msg) {
            app.alertInfo('DOI não encontrado!');
        })
    },
    sqTipoPublicacaoChange: function (params) {
        var codigo = $("#sqTipoPublicacao option:selected").data('codigo');
        //$("#label_issn").text('Nº ISSN');
        /*if (codigo == 'ATO_OFICIAL') {
            $("#inListaOficialVigente").parent().removeClass('hidden');
        } else {

            if (/^(CAPITULO_LIVRO|LIVRO)/.test(codigo)) {
                $("#label_issn").text('Nº ISBN');
            }
            $("#inListaOficialVigente").parent().addClass('hidden');
            $("input:radio[name=inListaOficialVigente]").prop('checked', false)
        }
        */
        refBib.layout(codigo)
    },
    pesquisarRefBib: function (params) {
        if (!$.trim(params.fldCadRefBibPesquisar)) {
            app.alertInfo('Informe uma palavra chave!')
            return;
        }
        if ($.trim(params.fldCadRefBibPesquisar).length < 3) {
            app.alertInfo('Informe no mínimo 3 caracteres para pesquisar!')
            return;
        }
        $('#divGridModalCadRefBib').data('q', params.fldCadRefBibPesquisar);
        refBib.getGrid();
    },
    save: function (params) {
        if (!$('#frmModalCadRefBib').valid()) {
            return false;
        }

        // Ano 9999 está utilizado para quando a referencia não possuir ano
        try {
            if ($("#nuAnoPublicacao").val() != '9999') {
                if (parseInt($("#nuAnoPublicacao").val()) < 1500 || parseInt($("#nuAnoPublicacao").val()) > 2100) {
                    //return app.alertError('Ano da publicação deve ser maior que 1500 e menor que 2100 ou 9999 quando não possuir ano.')
                }
            }
        } catch(e) {
            //return app.alertError('Ano da publicação deve ser maior que 1500 e menor que 2100 ou 9999 quando não possuir ano.')
        }

        // limpar os campos que não fazem parte do tipo do documento selecionado
        $("div.hidden").each(function () {
            var divId = this.id;
            $(this).find('input').each(function () {
                $(this).val('');
                if (this.type == 'file') {
                    app.clearInputFile(divId);
                }
            });
        })
        var data;
        data = $("#frmModalCadRefBib").serializefiles();
        if (typeof (params.sqFicha) == 'string') {
            data.set('sqFicha', params.sqFicha);
            data.set('sqRegistro', params.sqRegistro);
            data.set('noColuna', params.noColuna);
            data.set('noTabela', params.noTabela);
            data.set('deRotulo', params.deRotulo);
        }

        // se for um ato oficial tem que selecionar se é uma lista oficial ou não
        var codigo = $("#sqTipoPublicacao option:selected").data('codigo');
        if (codigo == 'ATO_OFICIAL' && $("input:radio[name=inListaOficialVigente]:checked").size() == 0) {
            return app.alertError('Necessário informar se o ato oficial é uma lista oficial vigente ou não!!')
        }
        if( data.get('sqPublicacao') != '' && params.confirmacao != 'S' ) {
            var mensagemConfirmacao = "A alteração destas informações afetará todas as fichas do SALVE " +
                "que estão utilizando esta referência. <br>" +
                "<b>Tem certeza que deseja gravar suas alterações?</b>"
            app.confirm(mensagemConfirmacao, function () {
                var newParams = $.extend( {confirmacao:'S'}, params );
                refBib.save( newParams );
                return;
            },null,null,'ATENÇÃO')
            return;
        }
        app.ajax(app.url + 'referenciaBibliografica/save', data,
            function (res) {
                if (res.status == 0) {
                    var sqTipoPublicacao = $("#sqTipoPublicacao").val();
                    app.clearInputFile('frmModalCadRefBib');
                    refBib.reset();
                    $("#sqTipoPublicacao").val(sqTipoPublicacao);
                    if (params.sqFicha) {
                        app.closeWindow('modalCadRefBib');
                        ficha.updateGridRefBib();
                        if (params.noTabela == 'ficha') {
                            ficha.updateGridAllRefBib();
                        }
                    }
                }
            }, null, 'json'
        );
    },
    reset: function (params) {
        $("div#divAlertOnEdit").hide();
        $("#divGridModalCadRefBibAnexos").addClass('hide').html('');
        app.reset('frmModalCadRefBib', 'sqTipoPublicacao');
        app.focus('noAutor');
    },
    getGrid: function (params) {
        app.getGrid('modalCadRefBib');
    },
    edit: function (params, btn) {
        app.ajax(app.url + 'referenciaBibliografica/edit', params,
            function (res) {
                if (res) {
                    app.setFormFields(res, 'frmModalCadRefBib');
                    app.selectTab('tabCadRefBibForm');
                    $("#divGridModalCadRefBibAnexos").removeClass('hide');
                    app.getGrid('modalCadRefBibAnexos', 'frmModalCadRefBib');
                    refBib.sqTipoPublicacaoChange();
                    // exibir alerta ao editar a ref bib
                    setTimeout(function () {
                        if ( $("#frmModalCadRefBib input[name=sqPublicacao]").val()) {
                            $("div#divAlertOnEdit").show('slow');
                        }
                    }, 1000);
                }
            }, null, 'json'
        );
    },
    delete: function (params, btn) {
        app.selectGridRow(btn);
        app.confirm('<br>Confirma exclusão da Referência Bibliográfica?',
            function () {
                app.ajax(app.url + 'referenciaBibliografica/delete',
                    params,
                    function (res) {
                        refBib.getGrid()
                    });
            },
            function () {
                app.unselectGridRow(btn);
            }, params, 'Confirmação', 'warning');
    },
    deleteAnexo: function (params, btn) {
        app.selectGridRow(btn);
        app.confirm('<br>Confirma exclusão do anexo <b>' + params.descricao + '</b>?',
            function () {
                app.ajax(app.url + 'referenciaBibliografica/deleteAnexo',
                    params,
                    function (res) {
                        app.getGrid('modalCadRefBibAnexos');
                    });
            },
            function () {
                app.unselectGridRow(btn);
            }, params, 'Confirmação', 'warning');
    },
    layout: function (codigo) {
        var divs = ['autor', 'autorExemplo', 'titulo', 'revista', 'ano','dataPublicacao', 'volume', 'issue', 'pagina', 'tituloLivro', 'editor', 'editora', 'cidade', 'edicao', 'url', 'dataAcesso', 'universidade', 'doi', 'issn', 'isbn', 'citacao', 'anexo', 'inListaOficialVigente'];
        var tipos = {
            "ARTIGO_CIENTIFICO": ['autor', 'autorExemplo', 'titulo', 'ano', 'revista', 'volume', 'issue', 'pagina', 'doi', 'issn', 'citacao', 'anexo'],
            "ATO_OFICIAL": ['autor', 'autorExemplo', 'titulo', 'dataPublicacao', 'pagina', 'url', 'dataAcesso', 'citacao', 'anexo', 'inListaOficialVigente'],
            "BANCO_DADOS_INSTITUCIONAL": ['autor', 'autorExemplo', 'titulo', 'ano', 'citacao'],
            "BANCO_DADOS_PESSOAL": ['autor', 'autorExemplo', 'titulo', 'ano', 'citacao'],
            "CAPITULO_LIVRO": ['autor', 'autorExemplo', 'titulo', 'ano', 'pagina', 'tituloLivro', 'editor', 'editora', 'cidade', 'edicao', 'universidade', 'doi', 'isbn', 'anexo'],
            "DISSERTACAO_MESTRADO": ['autor', 'autorExemplo', 'titulo', 'ano', 'pagina', 'cidade', 'universidade', 'citacao', 'anexo'],
            "MONOGRAFIA": ['autor', 'autorExemplo', 'titulo', 'ano', 'pagina', 'cidade', 'universidade', 'citacao', 'anexo'],
            "LIVRO": ['autor', 'autorExemplo', 'titulo', 'ano', 'pagina', 'editora', 'cidade', 'edicao', 'doi', 'isbn', 'citacao', 'anexo'],
            "MIDIA_DIGITAL": ['autor', 'autorExemplo', 'titulo', 'ano', 'volume', 'issue', 'pagina', 'editora', 'cidade', 'url', 'dataAcesso', 'doi', 'issn', 'citacao', 'anexo'],
            "MUSEU": ['autor', 'autorExemplo', 'titulo', 'ano', 'citacao', 'anexo'],
            "PLANO_MANEJO_UC": ['autor', 'autorExemplo', 'titulo', 'ano', 'pagina', 'editora', 'cidade', 'url', 'dataAcesso', 'doi', 'issn', 'citacao', 'anexo'],
            "RELATORIO_TECNICO": ['autor', 'autorExemplo', 'titulo', 'ano', 'pagina', 'editora', 'cidade', 'url', 'dataAcesso', 'doi', 'issn', 'citacao', 'anexo'],
            "RESUMO_CONGRESSO": ['autor', 'autorExemplo', 'titulo', 'ano', 'tituloLivro', 'editor', 'editora', 'cidade', 'url', 'dataAcesso', 'doi', 'issn', 'citacao', 'anexo'],
            "SITE": ['autor', 'autorExemplo', 'titulo', 'ano', 'tituloLivro', 'url', 'dataAcesso', 'doi', 'issn', 'citacao'],
            "TESE_DOUTORADO": ['autor', 'autorExemplo', 'titulo', 'ano', 'pagina', 'cidade', 'universidade', 'citacao', 'anexo'],

        };
        // remover obrigatoriedade de autor e ano quando for SITE
        if (codigo == 'SITE') {
            $("#frmModalCadRefBib #noAutor, #frmModalCadRefBib input[name=nuAnoPublicacao]").prop('required', false)
        } else {
            $("#frmModalCadRefBib #noAutor, #frmModalCadRefBib input[name=nuAnoPublicacao]").prop('required', true)
        }

        if( codigo=='ATO_OFICIAL')
        {
            $("#divAutor label:first").html('Fonte');
            $("#divAutor span:first").hide();
            $("#divAutorExemplo").css('visibility','hidden');
        }
        else {
            $("#divAutor label:first").html('Autor(es)');
            $("#divAutor span:first").show();
            $("#divAutorExemplo").css('visibility','visible');
        }
        if( codigo=='ARTIGO_CIENTIFICO') {
            $("#tabCadRefBibForm #divPagina > label").text('Página(s)');
        } else {
            $("tabCadRefBibForm #divPagina > label").text('Nº páginas')
        }

        $.each(divs, function (k, v) {
            $("#div" + v.ucFirst()).removeClass('hidden').addClass('hidden')
        });
        $("#divFooter").addClass('hidden');
        if ($("#sqTipoPublicacao").val()) {
            // var aTemp = tipos[codigo].size()==0 ? ['autor','autorExemplo ,'titulo', 'ano'] : tipos[codigo];
            var aTemp = tipos[codigo] || ['autor', 'autorExemplo', 'titulo', 'ano'];
            if (aTemp) {
                $.each(aTemp, function (k, v) {
                    $("#div" + v.ucFirst()).removeClass('hidden');

                });
                $("#divFooter").removeClass('hidden');
            }
        }
        // esconder as quebras duplicadas
        $('br').removeClass('hidden');
        $('br').each(function () {
            if (!$(this).next().is(":visible") && $(this).nextAll('br').size() > 0) {
                if (!$($(this).nextAll('br')[0]).hasClass('hidden')) {
                    $(this).addClass('hidden');
                }
            }
        });
    },
    enviarPlanilha: function (params) {
        $.jsPanel({
            container: "body",
            id: "refbib_importacao",
            theme: "#86d4ff",
            headerTitle: 'Importação de Referências Bibliográficas',
            contentSize: {
                width: $(window).width() - 100,
                height: window.innerHeight - 150
            },
            content: '<div id="div_content_importacao_ref_bib" style="border:none;display:block;min-height:100%;"<br><br><p>Carregando...' + window.grails.spinner + '</p></div>',
            resizable: true,
            dragit: {
                containment: 'parent',
                disableui: true,
            },
            callback: function () {
                this.find('.jsglyph-minimize').hide();
                this.content.css("padding", "15px");
                //this.content.prop('id','modal_ficha_importacao_planilha')
                $("#frmImportacaoRefBib #file").remove();
                app.loadModule(app.url + 'main/getModal', params, 'div_content_importacao_ref_bib', function (res) {
                    refBib.updateGridImportacao();
                    // inicializar evento upload
                    $("#frmImportacaoRefBib #file").on('change', function (evt) {
                        refBib.file = null;
                        if (evt.target.files.length > 0) {
                            refBib.file = evt.target.files[0];
                        }
                    });
                }, '');
            },
        });
    },
    savePlanilha: function (params) {
        // verificar se tem anexo
        if (!$("#frmImportacaoRefBib #file").val()) {
            app.alertInfo('Nenhuma Planilha Selecionada! Clique no botão Procurar...');
            return;
        }
        var data = {};
        if (refBib.file && /\.bib/.test(refBib.file.name)) {
            data = $("#frmImportacaoRefBib").serializeArray();
            data.contexto = params.contexto;
            var reader = new FileReader();
            var dadosJson = [];
            reader.onload = (function (theFile) {
                return function (e) {
                    data = $("#frmImportacaoRefBib").serializefiles();
                    data.append('contexto', params.contexto);
                    data.append('fileName', refBib.file.name);
                    // https://github.com/mikolalysenko/bibtex-parser
                    var textoBib = utf8_decode(e.target.result);
                    var pos = textoBib.indexOf('@');
                    if (pos > -1) {
                        textoBib = textoBib.substring(pos - 1);
                        var aItens = textoBib.split('@');
                        aItens.forEach(function (item) {
                            if (item) {
                                item = clearItem(item);
                                var json = bibtexParse.toJSON("@" + item);
                                dadosJson.push(json);
                            }
                        });
                    }
                    data.append('conteudo', JSON.stringify(dadosJson));
                    data.append('fileSize', refBib.file.size);
                    app.confirmarImportacaoPlanilha(data, refBib.updateGridImportacao);
                };
            })(refBib.file);
            reader.readAsBinaryString(refBib.file);
        } else {
            data = $("#frmImportacaoRefBib").serializefiles();
            data.append('contexto', params.contexto);
            app.confirmarImportacaoPlanilha(data, refBib.updateGridImportacao);
        }
    },
    baixarPlanilha: function (params) {
        app.alertInfo('Baixar')
    },
    updateGridImportacao: function (params) {
        var data = {};
        var posicao = $("#divGridImportacaoRefBibs").scrollTop();
        app.ajax(app.url + 'referenciaBibliografica/getGridImportacao', data,
            function (res) {
                $("#divGridImportacaoRefBibs").html(res);
                $("#divGridImportacaoRefBibs").scrollTop(posicao);
                app.updatePercentualImportacao();
            }, null, 'html');
    },
    deletePlanilha: function (params, btn) {
        app.selectGridRow(btn);
        app.confirm('<br>Confirma exclusão da planilha <b>' + params.descricao + '</b>?',
            function () {
                app.ajax(app.url + 'referenciaBibliografica/deletePlanilha',
                    params,
                    function (res) {
                        refBib.updateGridImportacao();
                    });
            },
            function () {
                app.unselectGridRow(btn);
            }, params, 'Confirmação', 'warning');

    },

    /*
        métodos da funcionalidade: Corrigir Duplicidades
    */

    chkCorrigirDuplicidadeClick: function (params, ele, evt) {
        var $container = $("#tableGridConsultaRefBib");
        var value=ele.value;
        if( ele.checked )
        {
           $("#btnCorrigirDuplicidade-"+value).addClass('checked');
        }
        else
        {
            $("#btnCorrigirDuplicidade-"+value).removeClass('checked');
        }

        // contar se tem mais de 1 marcado e exibir os botoes "manter esta"
        var checkboxesChecked  = $container.find('input[type=checkbox].chk-corrigir-duplicidade:checked');

        // se a quantidade de checks marcados for maior que 1, exibir os botoes "Manter esta" ou esconder caso contrario
        if( checkboxesChecked.size() > 1 )
        {
            $container.find('button.btn-corrigir-duplicidade.checked').show('fast');
        }
        else
        {
            $container.find('button.btn-corrigir-duplicidade:visible').hide('fast');
        }


    },
    btnCorrigirDuplicidadeClick: function (params, ele, evt) {
        var $container = $("#tableGridConsultaRefBib");
        var sqPublicacoesExcluir = [];
        var sqPublicacaoManter = $(ele).data('sqPublicacao');
        app.selectGridRow(ele);
        app.confirm('Confirma a correção das duplicidades ?',
            function () {

                $container.find('input[type=checkbox].chk-corrigir-duplicidade:checked').map(function(i,checkbox){
                    if( parseInt(checkbox.value) != parseInt( sqPublicacaoManter ) )
                    {
                        sqPublicacoesExcluir.push( checkbox.value );
                    }
                });
                var data = { sqPublicacaoManter: sqPublicacaoManter, sqPublicacoesExcluir:sqPublicacoesExcluir.join(',') }

                app.ajax(app.url + 'referenciaBibliografica/saveCorrecaoDuplicidade',
                    data,
                    function (res) {
                        app.unselectGridRow(ele);
                        if (res.status == 0) {
                            refBib.getGrid()
                        }
                    },'Processando. Aguarde...');
            },function(){
                app.unselectGridRow(ele);
            }, params, 'Confirmação', 'warning');


    },


};
refBib.init();

var clearItem = function( item ) {
    //item = item.replace(/\n/g,'')
    /*item = item.replace(/\\\\'{A}/g, 'Á');
    item = item.replace(/\\\\'{E}/g, 'É');
    item = item.replace(/\\\\'{I}/g, 'Í');
    item = item.replace(/\\\\'{O}/g, 'Ó');
    item = item.replace(/\\\\'{U}/g, 'Ú');
    item = item.replace(/\\\\'{a}/g, 'á');
    item = item.replace(/\\\\'{e}/g, 'é');
    item = item.replace(/\\\\'{i}/g, 'í');
    item = item.replace(/\\\\'{o}/g, 'ó');
    item = item.replace(/\\\\'{u}/g, 'ú');
    item = item.replace(/\\\\'{S}/g, 'Ś');
    item = item.replace(/\\\\~{a}/g, 'ã');
    item = item.replace(/\\\\~{o}/g, 'õ');
    item = item.replace(/\\\\~{A}/g, 'Ã');
    item = item.replace(/\\\\~{O}/g, 'Õ');
    item = item.replace(/\\\\\^\{a\}/g, 'â');
    item = item.replace(/\\\\\^\{e\}/g, 'ê');
    item = item.replace(/\\\\\^\{o\}/g, 'ô');
    item = item.replace(/\\\\\^\{A\}/g, 'Â');
    item = item.replace(/\\\\\^\{E\}/g, 'Ê');
    item = item.replace(/\\\\\^\{O\}/g, 'Ô');
    item = item.replace(/\\\\~{}/g, '~');
    item = item.replace(/\\\\c{c}/g, 'ç');
    item = item.replace(/\\\\c{C}/g, 'Ç');
    item = item.replace(/--/g, '-');
    item = item.replace(/\\\\&/g, '&');
    item = item.replace(/â/g, 'à'); // tipo 2
    item = item.replace(/â/g, "`"); // tipo 1
    item = item.replace(/\\\\'{y}/g, 'Ý');
    item = item.replace(/\\\\v{S}/g, "Š");
    item = item.replace(/Å/g, "ń");
    item = item.replace(/\\\\\\"{o}/g, "ö");
    item = item.replace(/\\\\`{e}/, 'è');
    item = item.replace(/â/g, '');
    item = item.replace(/Â°|Âº/g, 'º');
    item = item.replace(/Âª/g, 'ª');
    item = item.replace(/‘/g, "'");
    item = item.replace(/\\\\_/g, '_');
    item = item.replace(/\$\\\\backslash\$/g, "\\");
    item = item.replace(//g, "`");
    item = item.replace(/ Â` /g, '`');
    item = item.replace(/\\:/g, ':');
    item = item.replace(/‘/g, "'");
    item = item.replace(/â€˜ /g, "'");
    item = item.replace(/{\\_}/g,'_');
    */

    // remover as quebras de linha
    var re = new RegExp('\n','gm');
    item = item.replace(re, '');

    // agudo
    var de  = ['a','e','i','o','u'];
    var para= ['á','é','í','ó','ú'];
    de.forEach( function( vogal,i ){
        item = item.replace(new RegExp("{\\\\'{"+vogal+"}}",'gm'),para[i]);
        item = item.replace(new RegExp("{\\\\'{"+vogal.toUpperCase()+"}}",'gm'),para[i].toUpperCase());
    });
    // circunflexo
    para= ['â','ê','î','ô','û'];
    de.forEach( function( vogal,i ){
        item = item.replace(new RegExp("{\\\\\\^{"+vogal+"}}",'g'),para[i]);
        item = item.replace(new RegExp("{\\\\\\^{"+vogal.toUpperCase()+"}}",'g'),para[i].toUpperCase());
    });
    // crase
    para= ['à','è','ì','ò','ù'];
    de.forEach( function( vogal,i ){
        item = item.replace(new RegExp("{\\\\`{"+vogal.toUpperCase()+"}}",'gm'),para[i].toUpperCase());
    });
    // til
    para= ['ã','ẽ','ĩ','õ','ũ'];
    de.forEach( function( vogal,i ){
        item = item.replace(new RegExp("{\\\\~{"+vogal+"}}",'gm'),para[i]);
        item = item.replace(new RegExp("{\\\\~{"+vogal.toUpperCase()+"}}",'gm'),para[i].toUpperCase());
    });
    // cedilha
    item = item.replace(/{\c{c}}/gm,'ç');
    item = item.replace(/{\c{C}}/gm,'Ç');

    // underline
    item = item.replace( /{\\_}/g,'_');

    // backslash
    item = item.replace(/\$\\backslash\$/g, "\\");

    // menor que
    item = item.replace(/\textless/g,'<');

    // maior que
    item = item.replace(/\textgreater/g,'>');

    // & comercial
    item = item.replace(/ \\&/g,'&');

    return item;
}

var utf8_decode = function( str_data )
{
    // http://kevin.vanzonneveld.net
    // +   original by: Webtoolkit.info (http://www.webtoolkit.info/)
    // +	  input by: Aman Gupta
    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // *	 example 1: utf8_decode('Kevin van Zonneveld');
    // *	 returns 1: 'Kevin van Zonneveld'

    var tmp_arr = [], i = ac = c = c1 = c2 = 0;

    while (i < str_data.length) {
        c = str_data.charCodeAt(i);
        if (c < 128) {
            tmp_arr[ac++] = String.fromCharCode(c);
            i++;
        }
        else if ((c > 191) && (c < 224)) {
            c2 = str_data.charCodeAt(i + 1);
            tmp_arr[ac++] = String.fromCharCode(((c & 31) << 6) | (c2 & 63));
            i += 2;
        }
        else {
            c2 = str_data.charCodeAt(i + 1);
            c3 = str_data.charCodeAt(i + 2);
            tmp_arr[ac++] = String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
            i += 3;
        }
    }
    return tmp_arr.join('');
};
