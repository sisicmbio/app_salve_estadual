<div class="window-container text-left">
    <form class="form-inline" role="form" method="POST" id="frmModalComentarioValidador${id}">
        <div class="form-group w100p">
            <label for="">Comentário</label>
            <br/>
            <textarea id="fldComentarioAvaliador${id}" name="fldComentarioAvaliador" rows="4" class="fld form-control fldTextarea wysiwygRO w100p"><b>Marcelo - 15/01/2017 14:25:25</b><br>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived.<hr><b>Ponto Focal - 16/01/2017 17:12:45</b><br>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived.</textarea>
        </div>

        <br />

        <div class="form-group w100p">
            <label for="">Novo Comentário</label>
            <br/>
            <textarea id="fldNovoComentrioChat${id}" name="fldNovoComentrioChat${id}" rows="4" class="fld form-control fldTextarea wysiwyg w100p"></textarea>
        </div>
        <br />

        %{-- botão --}%
        <div class="fld panel-footer">
            <button data-action="avaliacao.saveComentario" params-id="${id}"class="fld btn btn-success">Enviar</button>
        </div>
    </form>
</div>