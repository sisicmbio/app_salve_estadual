<!-- view: /views/modals/_tranferirRegistroEspecie.gsp -->
<!-- controller:${params?.controller} -->
<!-- action: ${params?.action} -->
<div class="lazy-load">
    /modals/transferirRegistroEspecie
</div>
<div class="window-container text-left" id="modalTransferirRegistroEspecieWrapper">

            %{-- inicio form --}%
            <form class="form-inline" role="form" id="frmModalTransferirRegistroEspecie">
                %{-- SELECIONAR FICHA --}%
                <div class="fld form-group">
                    <label for="sqFichaSelecionada" class="control-label">Ficha destino</label>
                    <br>
                    <select id="sqFichaSelecionada" name="sqFichaSelecionada" class="form-control select2 fld500"
                            data-s2-minimum-input-length="1"
                            data-s2-select-on-close="false"
                            data-s2-placeholder="-- selecione --"
                            data-s2-params="sqCicloAvaliacao"
                            data-s2-url="ficha/select2Ficha">
                    </select>
                </div>

                <div class="fld form-group">
                    <label for="inMarcarUtilizado" class="control-label" title="Aplicável somente aos registros NÃO UTILIZADOS que estiverem sendo transferidos.">
                        <input type="checkbox" id="inMarcarUtilizado" class="fld checkbox-lg" value="S">&nbsp;transferir e marcar como "Utilizados na Avaliação"</label>
                </div>

                %{-- BOTOES --}%
                <div class="panel-footer mt20" id="divFooter">
                    <a href="#" id="btnSaveTransferirRegistrosEspecie" class="btn btn-success float-left" data-params="sqFichaSelecionada" data-action="distribuicao.saveTransferirRegistro">Transferir</a>
                    <a href="#" class="btn btn-danger float-right" data-action="app.closeWindow('modalTranferirRegistroEspecie')">Fechar</a>
                </div>

              %{--  <div class="mt10 alert alert-danger">
                    <div class="text-center">
                        <h3><b>Atenção</b></h3>
                    </div>
                    <h4>
                        <ol>
                            <li style="margin-bottom:5px;">Somente registros não utilizados na avaliação serão transferidos;</li>
                            <li>Registros existentes na ficha destino serão ignorados.</li>
                        </ol>
                    </h4>
                </div>--}%

            </form>
            %{-- fim form --}%
        </div>
    </div>
</div>
