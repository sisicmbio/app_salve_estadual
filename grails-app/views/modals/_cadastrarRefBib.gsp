<!-- view: /views/modals/_cadastrarRefBib.gsp -->
<!-- controller:${params?.controller} -->
<!-- action: ${params?.action} -->

<div class="lazy-load">
   /modals/cadastrarRefBib
</div>
<div class="window-container text-left">

    <div id="divAlertOnEdit" class="alert alert-danger text-center" style="font-size:23px !important;display:none">Alterações feitas nesta tela afetam todas as fichas do banco de dados.</div>
   %{-- criar abas cadastro e pesquisar --}%
   <ul class="nav nav-tabs" id="ulTabsCadRefBib">
      <li id="liTabCadRefBibForm" class="active"><a data-toggle="tab" href="#tabCadRefBibForm">Cadastrar</a></li>
      <li id="liTabCadRefBibPesquisa"><a data-toggle="tab" href="#tabCadRefBibPesquisa">Consultar</a></li>
      <li id="liTabCadRefBibImportacao" class="pull-right"><a data-toggle="tab" class="tab-amarela-clara" href="#tabImportarPlanilha">Importar</a></li>
   </ul>
   %{-- Container de todas abas --}%
   <div id="taxTabContent" class="tab-content">
      %{-- inicio aba Form Cad Ref Bib--}%
      <div role="tabpanel" class="tab-pane active" id="tabCadRefBibForm">
         %{-- inicio form --}%
         <form class="form-inline" role="form" method="POST" action="/referenciaBibliografica/save" id="frmModalCadRefBib" enctype='multipart/form-data'>
            <input type="hidden" name="sqPublicacao" value="">
               <div class="form-group">
                  <label for="sqTipoPublicacao">Tipo documento</label>
                  <br>
                  <select name="sqTipoPublicacao" id="sqTipoPublicacao" class="form-control" required="true" data-change="refBib.sqTipoPublicacaoChange">
                     <option value="">?</option>
                     <g:each var="item" in="${listTipoPublicacao}">
                     <option data-codigo="${item.coTipoPublicacao}" value="${item.id}">${item.deTipoPublicacao}</option>
                     </g:each>
                  </select>
               </div>

                <div class="form-group" id="divDoi">
                    <label>Nº DOI</label>
                    <br>
                    <input name="deDoi" type="text" class="form-control fld400">
                    <i title="Infome o nº do DOI e clique na lupa para pesquisar e preencher<br>os campos com os dados do site crossref.org" class="fa fa-search cursor-pointer" data-action="refBib.searchCrossref"></i>
                </div>

%{--                <div class="form-group" id="divSubTipo">
                  <label>Nome Documento</label>
                  <br>
                  <select name="sqSubTipoPublicacao"  id="sqSubTipoPublicacao" class="form-control" required="true">
                     <option value="">?</option>
                     <option data-codigo="DISSERTACAO" value="T">Dissertação</option>
                     <option data-codigo="MONOGRAFIA" value="M">Monografia</option>
                     <option data-codigo="TESE" value="T">Tese</option>
                  </select>
               </div>
 --}%
               <div class="form-group" id="divInListaOficialVigente">
                  <label class="control-label"> É uma lista oficial vigente? </label>
                  <br>
                  <div class="form-control" id="inListaOficialVigente" style="max-height: 31px;">
                     <div class="radio">
                        <label style="padding-top: 0px;"><input class="cursor-pointer" type="radio" name="inListaOficialVigente" value="S">Sim</label>
                     </div>
                     <div class="radio" style="padding-left: 20px;">
                        <label style="padding-top: 0px;"><input class="cursor-pointer" type="radio" name="inListaOficialVigente" checked value="N">Não</label>
                     </div>
                  </div>
               </div>

               <br>

               <div class="form-group" id="divAutor">
                  <label for="noAutor">Autor(es)</label>&nbsp;&nbsp;<span>(Para vários autores, informe 1 por linha)</span>
                  <br>
                  <textarea name="noAutor" id="noAutor" rows="4" class="form-control fld400" required="true"></textarea>
               </div>
               <div class="form-group" id="divAutorExemplo">
                  <label>Exemplo(s)</label>
                  <br>
                  <p class="form-control-static">
                     Horowitz, L.B.<br/>Jorge, R.S.P.<br/>Barbosa, E.G.
                  </p>
               </div>

               <br>
               <div class="form-group w100p" id="divTitulo">
                   <label>Título
                        <button type="button" class="fld ml10 btn btn-secondary btn-xs btn-editor"
                                data-action="refBib.addItalicTags">
                               <i title="Adicionar marcação de itálico no texto selecionado!" class="fa fa-italic"></i>
                        </button>
                   </label>
                  <br>
                  <input name="deTitulo" type="text" class="form-control w100p"
                      onfocusout="refBib.copySelectedText(this)"
                         required="true">
               </div>
               <br>
               <div class="form-group" id="divRevista">
                  <label>Revista científica</label>
                  <br>
                  <input name="noRevistaCientifica" type="text" class="form-control fld200">
               </div>

               %{--ANO DA PUBLICACAO--}%
               <div class="form-group" id="divAno">
                  <label for="nuAnoPublicacao" title="O ano da publicação deve estar entre 1500 e 2100.<br>Quando não possuir ano preencha com 9999.">Ano de publicação</label>
                  <br>
                  <input name="nuAnoPublicacao" id="nuAnoPublicacao" type="text" class="form-control fld150" data-mask="0000" required="true">
               </div>

               %{--DATA DA PUBLICACAO--}%
               <div class="form-group" id="divDataPublicacao">
                  <label>Data de publicação</label>
                  <br>
                  <input name="dtPublicacao" type="text" class="form-control date fld120" required="true">
               </div>

               <div class="form-group" id="divVolume">
                  <label>Volume</label>
                  <br>
                  <input name="deVolume" type="text" class="form-control fld100">
               </div>
               <div class="form-group" id="divIssue">
                  <label>Edição</label>
                  <br>
                  <input name="deIssue" type="text" class="form-control fld100">
               </div>
               <div class="form-group"  id="divPagina">
                  <label>Nº de páginas</label>
                  <br>
                  <input name="dePaginas" type="text" class="form-control fld100" maxlength="15">
               </div>
               <div class="form-group"  id="divTituloLivro">
                  <label>Título do livro/publicação/congresso</label>
                  <br>
                  <input name="deTituloLivro" type="text" class="form-control fld500">
               </div>
               <br id="x"/>
               <div class="form-group" id="divEditor">
                  <label>Editor(es)</label>&nbsp;&nbsp;<span>(Para vários editores, informe 1 por linha)</span>
                  <br>
                   <textarea name="deEditores" rows="4" class="form-control fld400" required="true"></textarea>
                  %{--<input name="deEditores" type="text" class="form-control fld500">--}%
               </div>
               <div class="form-group"  id="divEditora">
                  <label>Editora(s)</label>
                  <br>
                  <input name="noEditora" type="text" class="form-control fld500">
               </div>
               <br>
               <div class="form-group" id="divCidade">
                  <label>Cidade</label>
                  <br>
                  <input name="noCidade" type="text" class="form-control fld200">
               </div>
               <div class="form-group" id="divEdicao">
                  <label>Nº edição do livro</label>
                  <br>
                  <input name="nuEdicaoLivro" type="text" class="form-control fld150">
               </div>

               <div class="form-group" id="divUrl">
                  <label>Url de acesso</label>
                  <br>
                  <input name="deUrl" type="text" class="form-control url fld300">
               </div>
               <div class="form-group" id="divDataAcesso">
                  <label>Data de acesso</label>
                  <br>
                  <input name="dtAcessoUrl" type="text" class="form-control date fld120">
               </div>

               <div class="form-group" id="divUniversidade">
                  <label>Universidade</label>
                  <br>
                  <input name="noUniversidade" type="text" class="form-control fld300">
               </div>

               %{--<div class="form-group" id="divDoi">
                  <label>Nº DOI</label>
                  <br>
                  <input name="deDoi" type="text" class="form-control fld200">
               </div>--}%

               <div class="form-group" id="divIssn">
                  <label id="label_issn">Nº ISSN</label>
                  <br>
                  <input name="deIssn" type="text" class="form-control fld200">
               </div>
               <div class="form-group" id="divIsbn">
                  <label id="label_isbn">Nº ISBN </label>
                  <br>
                  <input name="deIsbn" type="text" class="form-control fld200">
               </div>
               %{-- a citação é campo calculado
               <div class="form-group" id="divCitacao">
                  <label>Citação</label>
                  <br>
                  <input name="deRefBibliografica" type="text" class="form-control fld500">
               </div>
               --}%
               <br>
               <div class="form-group" id="divAnexo">
                  <label>Anexo</label>
                  <br>
                  <input name="fldAnexo" type="file" id="file" class="form-control file"
                  data-show-preview="true"
                  data-show-upload="false"
                  data-show-caption="true"
                  data-show-remove="true"
                  data-max-file-count="1"
                  data-allowed-file-extensions='["pdf", "zip"]'
                  data-main-class="input-group-sm"
                  data-max-file-size="40000"
                  data-preview-file-type="object">
                  <!--any,image,text,video... -->
               </div>
               <div class="panel-footer" id="divFooter">
                  <div class="row">
                     <div class="col-sm-9">
                         <a href="#" id="btnSaveCadRefBib" class="btn btn-success" data-action="refBib.save" title="Gravar a referência bibliográfica.">Gravar</a>
                         <a href="#" id="btnResetCadRefBib" class="btn btn-default" data-action="refBib.reset" title="Limpar os campos do formulário.">Limpar Formulário</a>
                     </div>
                     %{--<div class="col-sm-3 text-right">--}%
                         %{--<a href="#" id="btnSaveCadRefBibFicha" class="btn btn-warning" data-action="refBib.save" title="clique aqui para gravar a referência bibliográfica e já adicioná-la à ficha atual!">Salvar e adicionar à ficha</a>--}%
                     %{--</div>--}%
                  </div>
              </div>
            </fieldset>
         </form>
         %{-- fim form --}%
         <div id="divGridModalCadRefBibAnexos" class="panel-footer mt10 hide" data-params="sqPublicacao"></div>
      </div>
      %{-- fim aba Form Cad Ref Bib--}%
      %{-- inicio aba Form Pesquisa Ref Bib--}%
      <div role="tabpanel" class="tab-pane" id="tabCadRefBibPesquisa">
         <form class="form-inline" role="form">
            <div class="form-group w100p">
               <label class="control-label" for="fldCadRefBibPesquisar" title="Exemplos de pesquisa:<br>- por palavra chave, ex: banco de dados, peixes fluviais, relatório técnico, Gilberto etc<br>- por autor e ano, ex: Paulo 2015, Rodrigo 2020<br>- pelo ID da referência, ex: id: 123456">Consultar Referência Bibliográfica</label>
               <br>
               <input type="text" value="" id="fldCadRefBibPesquisar" data-enter="$('#btnCadRefBibPesquisar').click()" class="form-control ignoreChange fld800" placeholder="Pesquisar por... ( mínimo 3 caracteres) ou id:9999" />
            </div>
            <div class="panel-footer mt10">
               <a id="btnCadRefBibPesquisar"  data-action="refBib.pesquisarRefBib" data-params="fldCadRefBibPesquisar" class="btn btn-success">Pesquisar</a>
            </div>
         </form>
         <div id="divGridModalCadRefBib" class="mt10">
         </div>
      </div>
      %{-- fim aba Form Pesquisa Ref Bib--}%
      %{-- inicio aba importar planilha --}%
      <div role="tabpanel" class="tab-pane" id="tabImportarPlanilha">

      <div class="alert alert-info" style="margin:20px;">Este módulo permite a importação de referências bibliográficas a partir da planilha padrão (xls ou xlsx) disponível para download no botão abaixo  e tembém dos arquivos .bib extraidos pelo programa <b><a target="_blank" href="http://www.jabref.org/">JabRef</a></b> no formato <b><a href="http://www.bibtex.org/" target="_blank">BibTex</a></b>.<br/><br/><a href="http://libguides.mit.edu/c.php?g=176186&p=1159535" target="_blank">Clique aqui para saber como exportar do Mendey para o formato BibTex</a> </div>

      <br/>
         <div class="form form-inline text-center w100p" style="padding-top:40px;">
            <button type="button" data-action="refBib.enviarPlanilha" data-params="modalName:importacaoRefBib" class="btn btn-primary btn-lg">Enviar Planilha ou Arquivo .bib</button>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a class="btn btn-warning btn-lg" target="_blank" title="Fazer o download da planilha modelo para preenchimento off-line dos registros de ocorrências!" href="/salve-estadual/main/download?fileName=planilha_modelo_importacao_ref_bib">Baixar Planilha</a>

         </div>
      </div>
      %{-- fim aba importar planilha --}%

   </div>

</div>
