//# sourceURL=selecionarCaverna.js
selecionarCaverna = {
    init:function() {
        //modalUserJobs.refresh();
        console.log('Selecionar Caverna iniciado')
    },
    atualizarGride:function(){
        // ler os filtros
        $frm = $("form#frmSelecionarCavernaFiltro");
        var noCavernaFiltro = $frm.find('input#noCavernaFiltro').val();
        var sqEstadoFiltro  = $frm.find('select#sqEstadoFiltro').val();
        var sqMunicipioFiltro  = $frm.find('select#sqMunicipioFiltro').val();
        var data = {
            noCavernaFiltro:noCavernaFiltro
            ,sqEstadoFiltro:sqEstadoFiltro
            ,sqMunicipioFiltro:sqMunicipioFiltro
        }
        if( !data.noCavernaFiltro && !data.sqEstadoFiltro ){
            app.growl('Informe algum parâmetro para pesquisar');
            return;
        }
        if( data.noCavernaFiltro && data.noCavernaFiltro.length<3){
            app.growl('Informe no mímino 3 caracteres para o nome da caverna');
            return;
        }

        $("#tableSelecionarCaverna").floatThead('destroy');
        $("#divGridSelecionarCaverna").html('Consultando...');
        app.ajax(baseUrl+'fichaDistribuicao/getGridSelecionarCaverna',data,function(res){
            $("#divGridSelecionarCaverna").html( res );
            $("#tableSelecionarCaverna").floatThead({
                floatTableClass: 'floatHead',
                scrollContainer: true,
                autoReflow: true
            });
        });
    },
    limparFiltros:function(){
        app.reset('frmSelecionarCavernaFiltro','',true);

    },
    estadoChange:function( fld, params, evt ){
        $("#sqMunicipioFiltro").empty();
        if( $("#sqEstadoFiltro").val() ){
            $("#divFiltroMunicipio").show();
        } else {
            $("#divFiltroMunicipio").hide();
        }
    },
    gravar: function (params, btn, evt) {
        console.log( params );
        var data = {
                sqFicha:params.sqFicha
                ,sqFichaAmbienteRestrito:params.sqFichaAmbienteRestrito
                ,idsCavernas:[]
        };
        // verificar se marcou pelo menos uma caverna
        var chkCavernas = $("#tableSelecionarCaverna tbody input:checkbox:checked");
        if (chkCavernas.size() == 0) {
            app.growl("Nenhuma caverna selecioinada");
            return;
        }
        $("#tableSelecionarCaverna tbody input:checkbox:checked").map(function(index, it){
            data.idsCavernas.push( it.value );
        })
        data.idsCavernas = data.idsCavernas.join(',');
        app.confirm('Confirma a gravação?', function () {
            app.ajax(baseUrl + 'fichaDistribuicao/saveCavernas', data, function (res) {

            });
        })
    },

}
selecionarCaverna.init();


