//# sourceURL=N:\SAE\sae\grails-app\views\modal\alterarTaxonFicha.js
//
var frmAlterarArvoreTaxonomica = {
    init:function(data){},
    tdSelected:'',
    nivelClick:function( ele ) {
        var data = $(ele).data();
        console.log(data);
        var $form = $("#frmModalAlterarArvoreTaxonomica");
        var $labelAlterarPara = $form.find('#labelAlterarPara');
        var $table = $("#tableAlterarArvoreTaxonomica");
        var $td = $table.find('#td' + data.noNivel);
        frmAlterarArvoreTaxonomica.tdSelected = $td;
        $("#frmModalAlterarArvoreTaxonomica #sqTaxonNovo").val(null).trigger('change');
        $labelAlterarPara.html('Alterar <b class="text-danger">'+data.noNivel.toLowerCase() + ' ' + $(ele).text() + '</b> para');
        $form.find("#sqTaxonAntigo").val(data.sqTaxon);
        $form.find("#sqTaxonFilho").val(data.sqTaxonFilho);
        // retirar destaque das celulas
        $table.find('td.nivel-link').removeClass('bg-danger');
        // adicionar destaque das celulas
        $td.addClass('bg-danger');
        // exibir campo select de pesquisa
        $form.find("#divSelectSqTaxonNovo").show('slow');
    },
    save : function(params)
    {
        console.log( params );
        if( !params.sqTaxon ) {
            app.alertInfo('Parâmetros incorretos. Falta o taxon.','Aviso');
            return;
        }
        if( !params.sqTaxonPaiAntigo ) {
            app.alertInfo('Parâmetros incorretos. Falta taxon pai antigo.','Aviso');
            return;
        }
        if( !params.sqTaxonPaiNovo ) {
            app.alertInfo('Informe o novo nível taxonômico.','Aviso',function(){
                app.focus("sqTaxonNovo ","frmModalAlterarArvoreTaxonomica");
            });
        }
        if( ! params.txHistorico ) {
            app.alertInfo('Motivo da mundança deve ser preenchido','Aviso',function(){
                app.focus("txHistorico ","frmModalAlterarArvoreTaxonomica");
            });
            return;
        }

        app.ajax(app.url + 'ficha/alterarArvoreTaxonomica', params,
            function(res) {
                console.log(res.data)
                if (res.status == 0) {
                    // atualizar dados do link
                    var $a = $( frmAlterarArvoreTaxonomica.tdSelected.find('a') );
                    $a.html( res.data.noNovoTaxon);
                    $a.data('sqTaxon', res.data.sqNovoTaxon );
                    frmAlterarArvoreTaxonomica.tdSelected.removeClass('bg-danger');
                    $("#frmModalAlterarArvoreTaxonomica #divSelectSqTaxonNovo").hide('fast');
                }
            }, null, 'json'
        );
    },
};


