<!-- view: /views/modals/_apoioShapeBacia.gsp -->
<!-- controller:${params?.controller} -->
<!-- action: ${params?.action} -->

<div class="lazy-load">
    /modals/apoioShapeBacia
</div>

<div id="modalApoioShapeBaciaWrapper" class="container-fluid">
    <div class="row">
        <div class="col-sm-12 col-md-5">

            <form id="frmApoioShapeBacia" class="row">

                %{--CAMPO ID--}%
                <input type="hidden" id="sqDadosApoio" name="sqDadosApoio" value="">


                %{-- NOME --}%
                <div class="form-group col-sm-12 col-md-9">
                    <label class="label-required" for="txGeo">Nome da bacia</label>
                    <input type="text" class="form-control" id="txGeo" name="txGeo" value="" maxlength="50">
                </div>

                <div class="form-group col-sm-12 col-md-3">
                    <label for="txGeo">Código</label>
                    <input type="text" class="form-control fld90" id="cdGeo" name="cdGeo" value="">
                </div>

                <div class="form-group col-sm-12">
                    <label class="control-label label-required">Shapefile da bacia</label>
                    <input name="fldShapefile" type="file" id="fldShapefile" class="file-loading"
                           onchange="apoioShapeBacia.shapeFileChange(this)"
                           accept="zip/*"
                           data-show-preview="false"
                           data-show-upload="false"
                           data-show-caption="true"
                           data-show-remove="true"
                           data-max-file-count="1"
                           data-allowed-file-extensions='["zip"]'
                           data-main-class="input-group-sm"
                           data-max-file-size="20000"
                           data-browse-label="Arquivo..."
                           data-preview-file-type="object">
                </div>


                %{--BOTOES--}%
                <div class="panel-footer col-sm-12">
                    <button type="button" id="btnSave" onclick="apoioShapeBacia.save();" class="btn btn-success">Gravar</button>
                    <button type="button" class="btn btn-danger pull-right" onclick="apoioShapeBacia.reset();">Limpar</button>
                </div>
                <div class="col-sm-12"><small class="red fs10">* - campo obrigatório</small></div>
            </form>
        </div>
        %{--  MAPA --}%
        <div class="col-sm-12 col-md-7">
            <div>
                <h4>Polígono da bacia</h4>
            </div>
            <div id="mapShapeFile" style="width: 100%;height: 600px;background: #e2ecef;"></div>
        </div>
    </div>
</div>
