//# sourceURL=/views/modals/selecionarColunasExportacaoTaxons.js
;
var selColsExportTaxons = {
    form:null,
    btnSelecionarTodos:null,
    init: function () {
        selColsExportTaxons.form = $("#frmselecionarColunasExportacaoTaxons");
        selColsExportTaxons.btnSelecionarTodos = selColsExportTaxons.form.find('a#btnSelecionarTodos');
    },
    marcarDesmarcarTodos:function(){
        var data =selColsExportTaxons.btnSelecionarTodos.data();
        selColsExportTaxons.form.find('input[type=checkbox]').prop('checked',!data.checked);
        selColsExportTaxons.btnSelecionarTodos.data('checked', !data.checked );
    },
    exportar:function(){

    }
}
selColsExportTaxons.init();
