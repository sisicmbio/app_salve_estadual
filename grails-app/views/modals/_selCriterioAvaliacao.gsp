<!-- view: /views/modals/_selCriterioAvaliacao/ -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<div class="lazy-load">
	/modals/selCriterioAvaliacao
</div>
<style>
	.tree-root
	{
		/*font-size: 1.4em !important;*/
		font-weight: bold !important;
	}

	ul.fancytree-container
	{
		overflow:hidden;
	}

	span.fancytree-checkbox
	{
		cursor:pointer;
	}

	.fancytree-title
	{
		padding:0px !important;
		margin:0px !important;
		margin-top: -3px !important;
	}

	.fancytree-node {
		padding-top: 0px;
		min-height: 18px !important;
	}

	.fancytree-selected
	{
		background-color: #BAE9B6 !important;
		font-family: inherit !important;
		font-style:normal !important;
		border-radius: 5px;
	}

	span.fancytree-node
	{
		height: 16px !important;
	}
	.fancytree-unselectable .fancytree-checkbox
	{
		visibility: hidden !important;
	}

	.separador {
		border-bottom: 1px dashed #00f;
		margin-top: 0px;
		margin-left: -50px;
		min-height: 3px;
		width: 135px;
		height: 12px !important;
	}

	span.fancytree-title
	{
		font-style:normal !important;
		font-size:inherit !important;
		background:none !important;
		border:none !important;
		margin-top: -2px !important;
	}



</style>
<div class="window-container text-left" style="background-color: beige;" id="divSelecionarCriterioAvaliacao">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12">
				<div id="divCriterioResultado" class="text-center" style="padding-top:1px;font-size:15px;font-weight:bold;height:22px;border:none;">&nbsp;</div>
			</div>
			<input type="hidden" id="codCriterioSelecionado" value=""/>
		</div>
		<div class="row">
			<div class="col-sm-4">
				<div id="treeA">
					<ul id="treeData">
						<li data-value="A" id="A" class="folder tree-root unselectable">A
							<ul>
								<li data-value="1" id="A1" class="folder  unselectable">A1
									<ul>
										<li data-value="a" id="A1a">a</li>
										<li data-value="b" id="A1b">b</li>
										<li data-value="c" id="A1c">c</li>
										<li data-value="d" id="A1d">d</li>
										<li data-value="e" id="A1e">e</li>
									</ul>
								</li>
							<li class="unselectable"><div class="separador"></div></li>
								<li data-value="2" id="A2" class="folder unselectable">A2
									<ul>
										<li data-value="a" id="A2a">a</li>
										<li data-value="b" id="A2b">b</li>
										<li data-value="c" id="A2c">c</li>
										<li data-value="d" id="A2d">d</li>
										<li data-value="e" id="A2e">e</li>
									</ul>
								</li>
							<li class="unselectable"><div class="separador"></div></li>
								<li data-value="3" id="A3" class="folder unselectable">A3
									<ul>
										%{-- <li data-value="a" id="A3a">a</li> --}%
										<li data-value="b" id="A3b">b</li>
										<li data-value="c" id="A3c">c</li>
										<li data-value="d" id="A3d">d</li>
										<li data-value="e" id="A3e">e</li>
									</ul>
								</li>
							<li class="unselectable"><div class="separador"></div></li>
								<li data-value="4" id="A4" class="folder  unselectable">A4
									<ul>
										<li data-value="a" id="A4a">a</li>
										<li data-value="b" id="A4b">b</li>
										<li data-value="c" id="A4c">c</li>
										<li data-value="d" id="A4d">d</li>
										<li data-value="e" id="A4e">e</li>
									</ul>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
			<div class="col-sm-4">
				<div id="treeB">
					<ul id="treeData">
						<li id="B" data-value="B" class="folder tree-root unselectable">B
							<ul>
								<li data-value="1" id="B1" class="folder unselectable">B1
									<ul>
										<li data-value="a" id="B1a">a</li>
										<li data-value="b" id="B1b" class="unselectable">b
											<ul>
												<li data-value="(i)"  id="B1b(i)">i</li>
												<li data-value="(ii)" id="B1b(ii)">ii</li>
												<li data-value="(iii)" id="B1b(iii)">iii</li>
												<li data-value="(iv)" id="B1b(iv)">iv</li>
												<li data-value="(v)" id="B1b(v)">v</li>
											</ul>
										</li>
										<li data-value="c" id="B1c" class="unselectable">c
											<ul>
												<li data-value="(i)" id="B1c(i)">i</li>
												<li data-value="(ii)" id="B1c(ii)">ii</li>
												<li data-value="(iii)" id="B1c(iii)">iii</li>
												<li data-value="(iv)" id="B1c(iv)">iv</li>
											</ul>
										</li>
									</ul>
								</li>
							<li class="unselectable"><div class="separador"></div></li>
								<li data-value="2" id="B2" class="folder unselectable">B2
									<ul>
										<li data-value="a" id="B2a">a</li>
										<li data-value="b" id="B2b" class="unselectable">b
											<ul>
												<li data-value="(i)" id="B2b(i)">i</li>
												<li data-value="(ii)" id="B2b(ii)">ii</li>
												<li data-value="(iii)" id="B2b(iii)">iii</li>
												<li data-value="(iv)" id="B2b(iv)">iv</li>
												<li data-value="(v)" id="B2b(v)">v</li>
											</ul>
										</li>
										<li data-value="c" id="B2c" class="unselectable">c
											<ul>
												<li data-value="(i)" id="B2c(i)">i</li>
												<li data-value="(ii)" id="B2c(ii)">ii</li>
												<li data-value="(iii)" id="B2c(iii)">iii</li>
												<li data-value="(iv)" id="B2c(iv)">iv</li>
											</ul>
										</li>
									</ul>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
			<div class="col-sm-4">
				<div id="treeC">
					<ul id="treeData">
						<li data-value="C" id="C" title="C" class="folder tree-root unselectable">C
							<ul>
								<li data-value="1" id="C1" class="folder">C1</li>
								<li class="unselectable"><div class="separador"></div></li>
								<li data-value="2" id="C2" class="folder unselectable">C2
									<ul>
										<li data-value="a" id="C2a" class="unselectable">a
											<ul>
												<li data-value="(i)" id="C2a(i)">i</li>
												<li data-value="(ii)"id="C2a(ii)">ii</li>
											</ul>
										</li>
										<li data-value="b" id="C2b">b</li>
									</ul>
								</li>
							</ul>
						</li>
					</ul>
				</div>

				<br />
				<div id="treeD">
					<ul id="treeData">
						<li data-value="D" id="D" title="D" class="folder tree-root${params.codigoCategoria=='' || params.codigoCategoria == 'CR' || params.codigoCategoria == 'EN' ? '':' unselectable'}">D
							<g:if test="${params.codigoCategoria=='VU' || params.codigoCategoria==''}">
								<ul>
									<li data-value="1" id="D1" class="folder">D1</li>
									<li class="unselectable"><div class="separador"></div></li>
									<li data-value="2" id="D2" class="folder">D2</li>
								</ul>
							</g:if>
						</li>
					</ul>
				</div>
				<br />
				<div id="treeE">
					<ul id="treeData">
						<li data-value="E" id="E" title="E" class="folder tree-root">E</li>
					</ul>
				</div>
				<br />
				<br />
				<div class="fld text-center">
					<button data-action="validarCriterioSelecionado" data-field="${params.field}" data-contexto="${params.contexto}"  class="btn btn-primary btn-sm">Confirmar Critério</button>
					%{-- <button data-action="$('#codCriterioSelecionado').val($('#divCriterioSelecionado').html());app.closeWindow('selCriterioAvaliacao')" class="btn btn-primary btn-sm">Confirmar Critério</button> --}%
				</div>
			</div>
		</div>
			</div>
		</div>
	</div>
</div>
