<!-- view: /views/modals/_selecionarInformativo.gsp -->
<!-- controller:${params?.controller} -->
<!-- action: ${params?.action} -->
<table class="table table-condensed table-hover table-borderless table-informativos table-informativos">
    <thead>
    <tr>
        <th>Nº</th>
        <th>Data</th>
        <th>Tema</th>
        <th>Descrição</th>
        <th>Ação</th>
    </tr>
    </thead>
        <tbody>
        <g:each var="item" in="${listInformativos}" status="i">
        <tr>
            <td class="text-center" style="width: 50px;">${item.nuInformativo}</td>
            <td class="text-center" style="width: 90px;">${item.dtInformativo.format('dd/MM/yyyy')}</td>
            <td class="text-left" style="width: 300px;">${item.deTema}</td>
            <td class="text-justify" style="width: auto;">${item.txInformativo}</td>
            <td class="text-center">
                <a href="/salve-estadual/download?type=informativo&fileName=${item.deLocalArquivo}" title="Baixar"><i class="fa fa-download"></i></a>
            </td>
        </tr>
        </g:each>
    </tbody>
</table>
