//# sourceURL=\views\modal\visualizarPendenciaFicha.js
//
var visualizarPendenciaFicha = {
    init: function (data) {

        // fazer a leitura da justificativa atual e atualiar o campo dsJustificativaExclusao
        if (data.sqFicha) {
            app.ajax(app.url + 'ficha/getGridVisualizarPendencias', {sqFicha: data.sqFicha},
                function (res) {
                     $("#divVisualizarPendenciaWrapper").html( res );
                });
        }
    },
    updateSituacaoPendencia:function( data, ele, evt ) {
        evt.preventDefault();
        ficha.updateSituacaoPendencia( data, ele, evt,function( res ) {
            if( res.status == 0 ) {
                ficha.updateGridFichas({sqFicha:data.sqFicha});
                $("#spanLogRevisao" +data.sqFichaPendencia ).html(res.data.log ? res.data.log : '' );
                if (res.data.stPendente == 'S') {
                    ele.checked = false;
                    $("#tr-pendencia-"+data.sqFichaPendencia+' td.green').removeClass('green').addClass('red');
                } else {
                    ele.checked = true;
                    $("#tr-pendencia-"+data.sqFichaPendencia +' td.red').removeClass('red').addClass('green');
                }
            }
        } );
    }
};
