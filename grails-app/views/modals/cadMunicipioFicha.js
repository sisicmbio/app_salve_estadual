//# sourceURL=cadMunicipioFicha.js
var cadMunicipioFicha = {
    init : function(params)
    {
        //$('#divGridMunicipiosAreaRelevante').data('sqFichaAreaRelevancia', data.sqFichaAreaRelevancia); // para filtrar o gride
    },
    save:function( params )
    {
        var data = $("#frmSelMunicipio").serializeAllArray();
        if( ! data.sqMunicipio )
        {
            app.alertInfo('Informe o município!');
            return;
        }
        app.ajax(app.url + 'fichaDistribuicao/saveFrmMunicipio', data, function(res) {
            if ( ! res['status'] ) {
                cadMunicipioFicha.updateGride();
                app.reset('frmSelMunicipio', 'sqEstado,sqFichaAreaRelevancia');
                app.focus('sqMunicipio');
            }
        }, null, 'JSON');
    },
    updateGride:function( params )
    {
        ficha.getGrid('municipiosAreaRelevante');
    },
    delete: function(data) {
        app.confirm('Confirma exclusão do municipio <b>' + data.descricao + '</b>?', function() {
            app.ajax( app.url + 'fichaDistribuicao/deleteMunicipioAreaRelevante', data, function(res) {
                if (!res['status']) {
                    cadMunicipioFicha.updateGride();
                }
            }, null, 'json')
        });
    },
};