<!-- view: /views/modal/_modalInformativosPublicados -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action} -->
<div class="informativo-wrapper">
    <!-- cabecalho fixo data tabela -->
    <table class="table table-borderless table-condensed table-hover table-informativos">
        <thead>
            <tr>
                <th style="width: 50px;">Nº</th>
                <th style="width:55px;"><i class="fa fa-download"></i></th>
                <th style="width: auto;">Tema</th>
                <th style="width: 90px;">Publicação</th>
            </tr>
        </thead>
    </table>
    <!-- conteudo rolavel data tabela -->
    <div style="max-height:385px;overflow-y: auto;border:none;padding: 0;margin: 0">
        <table class="table table-bordered table-striped table-condensed table-hover table-informativos" id="tableVisualizarInformativosPublicados">
            <tbody>
            <g:each var="item" in="${listInformativosPublicados}">
                <tr>
                    <td class="text-center" style="width: 50px;">
                        ${item.nuInformativo}
                    </td>
                    <td class="text-center" style="width:55px;">

                        <g:if test="${item.deLocalArquivo}">
%{--                            <a href="/salve-estadual/download?type=informativo&fileName=${item.deLocalArquivo}" title="Baixar PDF"><i class="red fa fa-file-pdf-o"></i></a>--}%
                            <a href="javascript:void(0);" title="Baixar o PDF"
                               onclick="downloadWithProgress({'fileName':'${item.deLocalArquivo}','type':'informativo','originalFileName':'${item.noArquivo}'})" title="Baixar PDF"><i class="red fa fa-file-pdf-o"></i></a>
                        </g:if>
                        <g:else>
                            <span><i title="Não possui PDF" class="gray fa fa-file-pdf-o"></i></span>
                        </g:else>
                    %{--
                    <g:if test="${item.txInformativo}">
                        <a href="javascript:app.verificarInformativoAlerta(${item.id})" title="Visualizar mensagem"><i class="fa fa-file-text"></i></a>
                    </g:if>
                    <g:else>
                        <span><i title="Não possui mensagem" class="gray fa fa-file-text"></i></span>
                    </g:else>--}%
                    </td>
                    <td class="text-left" style="width: auto;">${item.deTema}
                        <g:if test="${item.txInformativo && !item.deLocalArquivo }">
                            <a href="javascript:app.verificarInformativoAlerta(${item.id})" title="Visualizar mensagem"><i class="ml10 fa fa-comment"></i></a>
                        </g:if>
                    </td>
                    <td class="text-left" style="width:90px;">
                        ${ item.dtInformativo.format('dd/MM/yyyy') }
                    </td>
                </tr>
            </g:each>
            </tbody>
        </table>
    </div>
</div>
