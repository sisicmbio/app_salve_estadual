<!-- view: /views/modals/_importacaoRefBib.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<div class="window-container text-left">
    <form name="frmImportacaoRefBib" id="frmImportacaoRefBib" class="form-inline" role="form" enctype='multipart/form-data'>
        <div class="fld form-group" id="divImportacaoRefBib">
            <label for="file">Selecionar Arquivo/Planilha</label>
            <br />

            %{-- data-max-image-width="1024" --}%
            <input name="file" id="file" type="file"  class="file-loading" required="true" accept="application/vnd.ms-excel,text/*.bib" style="width:0 !important;"
            data-show-preview="false"
            data-show-upload="false"
            data-show-caption="true"
            data-show-remove="false"
            data-max-file-count="1"
            data-allowed-file-extensions='["xls","xlsx","bib"]'
            data-main-class="input-group-sm"
            data-max-file-size="100000"
            data-preview-file-type="object">
        </div>
        <br class="fld" />
        <div class="fld form-group">
            <label for="deComentario" class="control-label">Comentário</label>
            <br>
            <input name="deComentario" id="deComentario" type="text" class="fld form-control fld600" value="Importação Referência Bibliográfica" placeholder="Comentário (opcional)"/>
        </div>
        <div class="fld panel-footer">
            <button id="btnSaveFrmImportacaoRefBib" data-action="refBib.savePlanilha" data-params="deComentario,contexto:refbib" class="fld btn btn-success">Importar Arquivo</button>
        </div>
        %{-- gride  --}%
        <div class="" id="divGridImportacaoRefBibs" data-params="contexto:refbib"></div>
    </form>

</div>
