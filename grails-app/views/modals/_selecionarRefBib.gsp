   <div class="window-container text-left">
      <%-- este form deverá ser carregado via ajax dependendo do campo da ficha --%>
      <form id="frmSelRefBibFicha" name="frmSelRefBibFicha" class="form-inline" role="form">
         <input type="hidden" name="sqFichaRefBib" id="sqFichaRefBib" value="" />
         <input type="hidden" id="contexto" value="" />
         <div class="form-group">
            <label class="control-label">Contexto</label>
            <br>
               <h4 id="modalFrmSelRefBibFichaLabel" class="form-control-static" style="color:#3C68F1">Contexto da Ref. bib</h4>
         </div>
         <br />
         <g:if test="${canModify}">
            <ul class="nav nav-tabs" id="ulTabSelRefBib">
               <li id="liTabSelRefBib" class="active"><a data-toggle="tab" data-tab="1" href="#tabSelRefBib">Referência Bibliográfica</a></li>
               <li id="liTabSelComPessoal"><a data-toggle="tab" data-tab="2" href="#tabSelComPessoal">Comunicação Pessoal</a></li>
            </ul>
            %{-- container abas --}%
            <div class="tab-content clearfix">

               %{-- inicio aba Referencia Bibliografica --}%

                <div role="tabpanel" class="tab-pane active" id="tabSelRefBib">
                   %{--filtros pelos campos específicos--}%
                   <div class="form-group">
                         <label class="control-label">Pesquisar somente pelo campo</label>
                        <br>
                        <label class="cursor-pointer mr15"> <input type="radio" id="refBibSearchForAutor" name="refBibSearchFor" value="autor">Autor</label>
                        <label class="cursor-pointer mr15"> <input type="radio" id="refBibSearchForTitulo" name="refBibSearchFor" value="titulo">Título</label>
                        <label class="cursor-pointer mr15"> <input type="radio" id="refBibSearchForAno" name="refBibSearchFor" value="ano">Ano</label>
                        <span>ou</span>
                        <label class="cursor-pointer ml15"> <input type="radio" id="refBibSearchFor" name="refBibSearchFor" checked="true" value="">Todos</label>
                  </div>
                <br/>
                  <div class="form-group">
                     <label for="sqPublicacao" title="Quando a referencia não existir ou possuir erros cadastrais, clique no botão Cadastrar ou Editar respectivamente.<br>Exemplos de pesquisas:<br>- por palavra chave, ex: banco de dados, peixes fluviais, relatório técnico, Gilberto etc<br>- por autor e ano, ex: Paulo 2015, Rodrigo 2020<br>- pelo ID da referência, ex: id: 123456" class="control-label">Referência Bibliográfica</label>
                     <br>
                     <select id="sqPublicacao" name="sqPublicacao" class="fld form-control select2 fld780"
                        data-s2-url="ficha/select2RefBib"
                        data-s2-placeholder="Pesquisar por... ( min 3 caracteres ) ou id:9999"
                        data-s2-minimum-input-length="3"
                        data-s2-template="refBibTemplate"
                        data-s2-params="contexto,refBibSearchFor"
                        data-s2-auto-height="true">
                     </select>
                     <a data-action="modal.cadRefBib" id="btnAddRefBibFicha" class="btn btn-sm btn-default"
                        title="Clique aqui para cadastrar referência bibliográfica.">
                        Cadastrar
                     </a>
                      <div class="ultimas-referencias"  id="divRefBibUltimasUtilizadas">
                          <label>Últimas referências utilizadas</label>
                          <div class="conteudo"></div>
                      </div>
                  </div>
                </div>

                %{--comunicação pessoal--}%
                <div role="tabpanel" class="tab-pane" id="tabSelComPessoal">
                    <div class="form-group">
                        <label class="control-label">Autor</label>
                        <br>
                        <input type="text" name="noAutorRef" id="noAutorRef" class="fld form-control fld400" maxlength="100" value="" />
                    </div>
                    <div class="form-group">
                        <label class="control-label">Ano</label>
                        <br>
                        <input type="text"  name="nuAnoRef" id="nuAnoRef" class="fld form-control fld60" data-mask="0000"  maxlength="4" value=""/>
                    </div>
                    <br>
                    <div class="text-left text-secondary">exemplo: T.L. Silva</div>
                </div>

            </div>
            %{-- fim container abas --}%
            <div class="form-group w100p">
               <label class="control-label">Tag (opcional)</label>
               <br>
               <select name="deTagsIds" id="deTagsIds" class="form-control fld select2 fld830" multiple="multiple"
                  data-s2-multiple="true"
                  data-s2-minimum-input-length="0"
                  data-s2-maximum-selection-length="5"
                  >
                  <g:each var="tag" in="${listTags}">
                     <option value="${tag.id}">${tag.descricao}</option>
                  </g:each>
               </select>
            </div>
            <div class="panel-footer mt10 text-center">
               <a id="btnSaveFrmSelRefBibFicha" data-action="ficha.saveFrmSelRefBibFicha" data-form="frmSelRefBibFicha" data-params="sqFicha" class="btn fld btn-success btn-sm">Adicionar Ref. Bibliográfica</a>
               <a id="btnResetFrmSelRefBibFicha" data-action="ficha.resetFrmSelRefBibFicha" class="btn fld btn-info btn-sm hide">Limpar Formulário</a>
            </div>
         </g:if>
      </form>
      <div id="divGridRefBib" class="mt10"></div>
   </div>
</div>
