<div class="window-container text-left">
    <div class="form-group">
        <label class="control-label">Contexto</label>
        <br>
        <h4 id="frmFichaAnexoLabelContexto" class="form-control-static" style="color:#3C68F1">Nome do conexto</h4>
    </div>
    <form name="frmFichaAnexo" id="frmFichaAnexo" class="form-inline" role="form" enctype='multipart/form-data'>
        <div class="fld form-group" id="divFichaAnexo">
            <label for="file" class="label-required">Selecionar Anexo.</label>
            <br />
            %{-- data-max-image-width="1024" --}%
            <input name="fldFichaAnexo" type="file" id="file" class="file-loading" required="true" accept="image/*;zip/*"
            data-show-preview="true"
            data-show-upload="false"
            data-show-caption="false"
            data-show-remove="true"
            data-max-file-count="1"
            data-allowed-file-extensions='["jpg", "jpeg", "png","tiff","tif","zip","pdf"]'
            data-main-class="input-group-sm"
            data-max-file-size="20000"
            data-preview-file-type="object">
        </div>
        <br class="fld" />
        <div class="fld form-group">
            <g:if test="${params?.contexto=='MAPA_DISTRIBUICAO'}">
                <label class="control-label label-required">Legenda/Observação</label>
                <br>
                <textarea name="deLegendaFichaAnexo" rows="5" class="fld form-control fld500" required="true"></textarea>
            </g:if>
            <g:else>
                <label class="control-label label-required">Legenda</label>
                <br>
                <input name="deLegendaFichaAnexo" type="text" class="fld form-control fld500" value="" required="true" />
            </g:else>

        </div>

        <div class="fld form-group">
            <label for="" class="control-label">Data do arquivo</label>
            <br>
            <input name="dtAnexo" type="text" class="fld form-control fld100 date" value="${new Date().format('dd/MM/yyyy')}">
        </div>

        <g:if test="${params?.contexto=='MAPA_DISTRIBUICAO'}">
            <br>
        </g:if>

        <div class="fld form-group" style="border-bottom: 1px solid silver;margin-top:20px;margin-left:15px;">
            <g:if test="${params?.contexto=='MAPA_DISTRIBUICAO'}">
                <label for="inFichaAnexoPrincipal"
                       title="Marque a imagem como principal para que ela seja impressa na ficha ou<br>no caso de shape marque se é o da distribuição geográfica atual."
                       class="control-label cursor-pointer boldAzul">Imagem principal ou shape da distribuição geográfica Atual?
                    <input name="inFichaAnexoPrincipal" id="inFichaAnexoPrincipal"
                           value="S" type="checkbox"
                           class="fld checkbox-lg">
                </label>
            </g:if>
            <g:else>
                <label for="inFichaAnexoPrincipal"
                    title="Marque a imagem como principal para que ela seja impressa na ficha.<br>Caso já exista uma outra imagem marcada como principal, ela será desmarcada."
                       class="control-label cursor-pointer boldAzul">Principal?
                    <input name="inFichaAnexoPrincipal" id="inFichaAnexoPrincipal"
                           value="S" type="checkbox"
                           class="fld checkbox-lg">
                </label>
            </g:else>
        </div>


        %{--<div class="fld form-group">
            <label for="inFichaAnexoPrincipal" class="control-label">Principal?</label>
            <br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input name="inFichaAnexoPrincipal" id="inFichaAnexoPrincipal" type="checkbox"  class="checkbox-lg" value="S">
        </div>
--}%
        <div class="fld panel-footer">
            <a id="btnSaveFrmFichaAnexo" data-action="ficha.saveAnexo" data-params="sqFicha" class="fld btn btn-success">Gravar Arquivo</a>
        </div>

        %{-- gride  --}%
        <div class="" id="divGridFichaAnexos"></div>
    </form>


</div>
