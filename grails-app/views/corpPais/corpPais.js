//# sourceURL=corpPais.js
;var corpPais = {
    map:null,
    init: function () {
        app.focus("noPais");
        corpPais.grid();
        corpPais.showHideForm(); // inicializar o mapa
    },
    loadMap:function(){
        setTimeout(function(){
            corpPais.map = corpGeo.initMap('mapShapeFile');
        },2000);
    },
    shapeFileChange:function(input){
        corpGeo.clearLayer( corpPais.map,'layerShapefile' );
        corpGeo.loadShapefile(input.files[0], corpPais.map,'layerShapefile',true);
    },
    reset:function(){
        $('form#frmPais')[0].reset();
        $("#sqPais").val(''); // reset não limpa campos hidden
        $("#fldShapefile").val('');
        corpGeo.clearLayer( corpPais.map,'layerShapefile' );
        app.focus('noPais');
    },
    save:function(){
        var data = {'sqPais':$("#sqPais").val()
            ,'noPais':$("#noPais").val()
            ,'nuOrdem':$("#nuOrdem").val()
            ,'coPais':$("#coPais").val()
            ,'wkt':corpGeo.generateLayerWKT(corpPais.map,'layerShapefile')
        };
        var errors = [];
        if( ! data.noPais ){
            errors.push('Nome do país deve ser informado')
        }
        if( ! data.coPais ){
            errors.push('Codigo ISO do país deve ser informado')
        }
        if( errors.length > 0 ){
            app.growl(errors.join('<br>'),'4','Erros encontrados','tc','','error');
            return;
        }
        app.ajax(baseUrl+'corpPais/save',data,function(res){
            if( res.status == 0 ) {
                corpPais.reset();
                corpPais.grid( data );
            }
        },'Gravando...')
    },
    edit:function( sqPais ) {
        if( sqPais ) {
            app.ajax(baseUrl + 'corpPais/edit', {sqPais: sqPais}, function (res) {
                if (res.status == 0) {
                    $("#sqPais").val(res.data.sqPais );
                    $("#noPais").val(res.data.noPais );
                    $("#coPais").val(res.data.coPais );
                    if( ! corpPais.map ) {
                        corpPais.loadMap();
                    }
                    corpGeo.showGeojson(corpPais.map, 'layerShapefile', res.data.geojson, true );
                    app.focus('noPais');
                }
            },'Carregando...')
        }
    },
    delete:function( sqPais, noPais ) {
        if (sqPais) {
            if ( app.confirm('Confirma a exclusão do país <b>' + noPais + '</b>?', function () {
                app.ajax(baseUrl + 'corpPais/delete', {sqPais: sqPais}, function (res) {
                    if (res.status == 0) {
                        corpPais.reset()
                        $('#gridContainer table tbody tr#tr-' + sqPais).remove();
                        corpPais.updateNumRecords();
                    }
                }, 'Aguarde...')
            })
            ) ;
        }
    },
    grid:function(data){
        data = data || {};
        // atualizar somente a linha alterada no gride
        if( data.sqPais && data.noPais ){
            $('#gridContainer table tbody tr#tr-' + data.sqPais + ' td#tdNoPais').html(data.noPais);
            $('#gridContainer table tbody tr#tr-' + data.sqPais + ' td#tdCoPais').html(data.coPais);
            return;
        }
        // recriar o gride
        $("#gridContainer").html('<div class="alert alert-info">Carregando tabela...'+grails.spinner+'</div>');
        app.ajax(baseUrl + 'corpPais/grid', {}, function (res) {
            $("#gridContainer").html( res );
            corpPais.updateNumRecords();
        },'')
    },
    updateNumRecords:function(){
        var numRecords = $("#gridContainer table tbody tr").size();
        $("#numRecords").html( String(numRecords ) + ( numRecords == 1 ? '&nbsp;país cadastrado' : '&nbsp;países cadastrados') );
    },
    /**
     * inicializar o mapa ao abrir o formulário
     * @param params
     */
    showHideForm:function(params){
        if( ! corpPais.map ) {
            corpPais.loadMap();
        }
    }
}
setTimeout(function(){corpPais.init(),1000});
