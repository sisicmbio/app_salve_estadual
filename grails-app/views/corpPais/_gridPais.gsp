<!-- view: /views/corpPais/_gridPais -->
<!-- controller:${params?.controller} -->
<!-- action: ${params?.action} -->
<table class="table table-striped table-responsive table-hover">
    <thead>
    <tr>
        <th style="width: auto">Nome</th>
        <th style="width: 70px">Código (ISO)</th>
        <th style="width: 100px;">Ações</th>
    </tr>
    </thead>
    <tbody>
    <g:each var="item" in="${paises}">
        <tr id="tr-${item.id}">

            %{-- NOME DO PAÍS --}%
            <td id="tdNoPais" class="text-left">${item.noPais}</td>

            %{-- CÓDIGO ISO --}%
            <td id="tdCoPais" class="text-center">${item.coPais}</td>

            %{-- ACOES --}%
            <td class="text-center">
                <i class="fa fa-edit fa-button cursor-pointer blue" title="Alterar" onClick="corpPais.edit(${item.id})"></i>
                <i class="fa fa-remove fa-button cursor-pointer red" title="Excluir" onClick="corpPais.delete(${item.id},'${item?.noPais}')"></i>
            </td>
        </tr>
    </g:each>
    </tbody>
</table>