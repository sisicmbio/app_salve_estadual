<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="ajax"/>
    <style>
    .map {
        width: 100%;
        height: 600px;
        background: #e2ecef;
    }
    #coPais {
        text-transform: uppercase;
    }
    </style>
</head>
<body>
<div class="col-sm-12">
    <div class="panel panel-default w100p" id="pnlPais">
        <div class="panel-heading">
            <span>
                Paises
            </span>
            <button id="btnClose" data-action="app.unloadModule" class="btn btn-default pull-right btn-xs" title="Fechar"><span class="glyphicon glyphicon-remove btnClose"></span></button>
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-sm-12 col-md-5">

                    <form id="frmPais" class="row">

                        %{--CAMPO ID--}%
                        <input type="hidden" id="sqPais" name="sqPais" value="">


                        %{-- NOME --}%
                        <div class="form-group col-sm-12 col-md-9">
                            <label class="label-required" for="noPais">Nome do país</label>
                            <input type="text" class="form-control" id="noPais" name="noPais" value="" maxlength="50">
                        </div>

                        <div class="form-group col-sm-12 col-md-3">
                            <label for="noPais">Código (ISO)</label>
                            <input type="text" class="form-control fld90" id="coPais" name="coPais" value="" data-mask="AAA" maxlength="3">
                        </div>

                        <div class="form-group col-sm-12">
                            <label class="control-label">Shapefile do país</label>
                            <input name="fldShapefile" type="file" id="fldShapefile" class="file-loading"
                                   onchange="corpPais.shapeFileChange(this)"
                                   accept="zip/*"
                                   data-show-preview="false"
                                   data-show-upload="false"
                                   data-show-caption="true"
                                   data-show-remove="true"
                                   data-max-file-count="1"
                                   data-allowed-file-extensions='["zip"]'
                                   data-main-class="input-group-sm"
                                   data-max-file-size="20000"
                                   data-browse-label="Arquivo..."
                                   data-preview-file-type="object">
                        </div>


                        %{--BOTOES--}%
                        <div class="panel-footer col-sm-12">
                            <button type="button" id="btnSave" onclick="corpPais.save();" class="btn btn-success">Gravar</button>
                            <button type="button" class="btn btn-danger pull-right" onclick="corpPais.reset();">Limpar</button>
                        </div>
                        <div class="col-sm-12"><small class="red fs10">* - campo obrigatório</small></div>
                    </form>

                    %{--                        GRIDE ABAIXO DO FORM--}%
                    <div class="row mt20">
                        <div class="col-sm-12">
                            <h4 id="numRecords">&nbsp;</h4>
                            <div style="width: auto;height: auto" id="gridContainer"></div>
                        </div>
                    </div>
                </div>
                %{--  MAPA --}%
                <div class="col-sm-12 col-md-7">
                    <div>
                        <h4>Polígono do País</h4>
                    </div>
                     <div id="mapShapeFile" style="width: 100%;height: 600px;background: #e2ecef;"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<asset:javascript src="corpGeo.js" defer="true"/>
</body>
</html>
