<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main" />
    <g:if test="${ ! session?.sicae?.user }">
        <title>SALVE Estadual - Login</title>
    </g:if>
    <g:else>
        <title>SALVE Estadual</title>
    </g:else>
</head>
<body>
<g:if test="${ ! session?.sicae?.user }">
    <div id="frmLogin" style="visibility:hidden;display:flex;justify-content:center; align-items:center; min-width: 100vw; min-height: 85vh">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span>Acesso ao sistema</span>
                    </div>
                    <div class="panel-body" id="frmLoginBody">
                        <i class="fa fa-user-circle image"></i>
                        <i class="fa fa-spinner fa-spin image hidden "></i>
                        <form class="form-login" style="margin: 10px;text-align: left;">

                            <label>CPF/email</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-at"></i></span>
                                <input type="email" id="inputEmail" class="form-control" placeholder="Email/CPF" required autofocus value="admin@salve-estadual">
                            </div>

                            <label>Senha</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                <input type="password" id="inputPassword" onkeyup="login.enterKey()" class="form-control mt-5" placeholder="Senha" required value="123456">
                            </div>

                            <div class="mt-5 text-right">
                                <button type="button"
                                        class="btn btn-xs btn-link" tabindex="-1" onclick="login.showFormForgotPassword()">
                                    <span>Esqueci a senha</span><i class="ml5 fa fa-exclamation"></i>
                                </button>
                            </div>
                            <button class="btn btn-lg btn-primary btn-block mt-5"
                                    onClick="login.login()"
                                    type="button">
                                <span>Entrar</span><i class="ml10 fa fa-key"></i>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</g:if>
<g:else>  <g:if test="${listPerfis}">
    <div id="frmLogin" style="visibility:visible;display:flex;justify-content:center; align-items:center; min-width: 100vw; min-height: 85vh">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span>Acesso ao sistema - Perfil</span>
                    </div>
                    <div class="panel-body" id="frmLoginPerfilBody">

%{--                        <i class="fa fa-user-circle image"></i>--}%
                        <form class="form-login" style="margin: 10px;text-align: left;">

                            <div class="row">
                                <div class="form-group col-sm-12">
                                    <label class="label-required">Selecione o perfil de acesso</label>
                                    <select class="form-control" id="sqUsuarioPerfil" required
                                        onchange="login.selectPerfil()">
                                        <option>-- selecione --</option>
                                        <g:each var="item" in="${listPerfis}">
                                            <option value="${item.id}">
                                                ${item.perfil.noPerfil }
                                            </option>
                                        </g:each>
                                    </select>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-sm-12" style="display: none;">
                                    <label class="label-required">Selecione a instituição</label>
                                    <select class="form-control" id="sqUsuarioPerfilInstituicao" required="true"></select>
                                </div>
                            </div>

                            <div class="row mt10">
                                <div class="form-group col-sm-12">
                                    <button class="btn btn-lg btn-primary fld100 pull-left disabled"
                                            disabled="true"
                                            id="btnEntrar"
                                            onClick="login.loginPerfil()"
                                            type="button">Entrar</button>

                                    <button class="btn btn-lg btn-secondary fld100 pull-right"
                                            onClick="login.voltar()"
                                            type="button">Sair</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</g:if>
</g:else>
<asset:javascript src="login.js" defer="true"/>
</body>
</html>
