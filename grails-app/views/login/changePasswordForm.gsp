<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main" />
    <title>SALVE-Alterar Senha</title>
</head>
<body>
    <div id="frmLogin" style="visibility:hidden;display:flex;justify-content:center; align-items:center; min-width: 100vw; min-height: 85vh">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span>Alterar senha de acesso</span>
                    </div>
                    <div class="panel-body" id="frmLoginBody" style="width: 380px;height: 390px;text-align: center">
                        <i class="fa fa-lock image"></i>
                        <form class="form-login" style="margin: 10px;text-align: left;">
                                <input type="hidden" name="inputHash" id="inputHash" value="${hash}">

                                <label>Nova senha</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                    <input type="password" id="inputPassword1" class="form-control mt-5" placeholder="Senha" required value="">
                                </div>

                                <label>Redigite a senha</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                    <input type="password" id="inputPassword2" onkeyup="login.enterKey()" class="form-control mt-5" placeholder="Redidigite" required value="">
                                </div>

                            <div class="mt20">
                                <button class="btn btn-secondary fld100 pull-left"
                                        onClick="login.voltar()"
                                        type="button">
                                    <i class="mr10 fa fa-reply"></i><span>Voltar</span>
                                </button>
                                <button class="btn btn-primary pull-right fld100"
                                            onClick="login.changePassword()"
                                            type="button">
                                        <span>Gravar</span><i class="ml10 fa fa-floppy-o"></i>
                                    </button>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asset:javascript src="login.js" defer="true"/>

</body>
</html>
