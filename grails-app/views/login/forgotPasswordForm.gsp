<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main" />
    <title>SALVE-Esqueci minha senha</title>
</head>
<body>
<div id="frmLogin" style="visibility:hidden;display:flex;justify-content:center; align-items:center; min-width: 100vw; min-height: 85vh">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <span>Recuperação / alteração de senha</span>
                </div>
                <div class="panel-body" id="frmLoginBody" style="width: 380px;height: 320px;text-align: center">
                    <i class="fa fa-at image"></i>
                    <form class="form-login" style="margin: 10px;text-align: left;">

                        <label>Informe seu e-mail</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-at"></i></span>
                            <input type="email" id="inputEmail" class="form-control" placeholder="Email" required autofocus value="${email}">
                        </div>

                        <div class="mt20">

                            <button class="btn btn-secondary fld100 pull-left"
                                    onClick="login.voltar()"
                                    type="button">
                                <i class="mr10 fa fa-reply"></i><span>Voltar</span>
                            </button>

                            <button class="btn btn-primary fld100 pull-right"
                                    onClick="login.sendResetPasswordMail()"
                                    type="button">
                                <span>Enviar</span><i class="ml10 fa fa-send"></i>
                            </button>


                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<asset:javascript src="login.js" defer="true"/>
</body>
</html>
