<!-- view: /views/fichaCompleta/_divGridHabitas.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<g:if test="${listFichaHabitats}">
    <table class="table table-condensed table-sm table-hover table-bordered table-striped">
        <thead>
        <tr>
            <th style="width:400px;">Habitat</th>
            <th>Referência Bibliográfica</th>
        </tr>
        </thead>
        <tbody>
        <g:each var="item" in="${listFichaHabitats}" status="i">
            <tr>
                <td>${item?.descricao}</td>
                <td>${raw(item.refBibHtml)}</td>
            </tr>
        </g:each>
        </tbody>
    </table>
</g:if>