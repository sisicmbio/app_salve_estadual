<!-- view: /views/fichaCompleta/index.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<style>
#container-ficha-completa div,span,i,td,p
{
    font-size: 14px !important;
    font-family: "open_sansregular", "Times New Roman","Times","Arial","Helvetica", "Lato", "Roboto", "sans-serif" !Important;
}

#container-ficha-completa .panel-heading *
{
    font-size: 18px !important;
}
#container-ficha-completa .panel-success
{
    margin-bottom:5px !important;
}

#container-ficha-completa .glyphicon
{
    font-family: 'Glyphicons Halflings' !important;
}

#container-ficha-completa .table-ocorrencias table,td,th,i,a,span,p,label {
    font-size: 13px !important;
}

.jsPanel-titlebar .fa {
    font: normal normal normal 25px/1 FontAwesome !important;
}

#container-ficha-completa .fa
{
    font: normal normal normal 14px/1 FontAwesome !important;
}

#container-ficha-completa .mce-ico
{
    font-family: 'open_sansregular','tinymce',"Times New Roman" !important;
}

#container-ficha-completa .btn
{
    font-size: 14px !important;
}

.jsPanel-title * {
    font-size: 22px !important;
}

</style>
<div id="container-ficha-completa" data-opened="N" class="container-fluid" style="font-size:1em;">
    <input id="sqCicloConsultaFicha" value="${cicloConsultaFicha?.id}" type="hidden" />
    <input id="sqFicha" value="${ficha?.id}" type="hidden" />
    <input id="sqOficina" value="${params.sqOficina}" type="hidden" />
    <input id="sqOficina" value="${params.sqOficina}" type="hidden" />
    <input id="contexto" value="${contexto?:'consulta'}" type="hidden" />
    <input id="canModify" value="${canModify ? 'S':'N'}" type="hidden" />
    <div class="row" style="margin-top:25px;">
        <div class="col-sm-12" style="position: fixed;z-index:2;top:55px;right:15px">
            <g:if test="${canModify || contexto == 'publicacao' }">
                <a id="btnCadPendencia" title="Cadastrar Pendência" data-contexto="${contexto}" data-sq-ficha="${ficha.id}" data-action="showFormPendencia" data-on-close="updateGridPendencia" class="btn btn-default btn-xs pull-right tooltipstered" style="border:none;cursor: pointer;margin:1px 6px;"><i class="glyphicon glyphicon-tags red"></i></a>
            </g:if>

        %{--<fieldset class="mt10">--}%
        %{--<legend class="topo" style="font-size:2.5em;padding:2px 20px 0px 10px;">--}%
        %{--<g:if test="${canModify}">--}%
        %{--<a id="btnCadPendencia" title="Cadastrar Pendência" data-sq-ficha="${ficha.id}" data-action="showFormPendencia" data-callback="updateGridPendencia" class="btn btn-default btn-xs pull-right tooltipstered" style="cursor: pointer;"><i class="glyphicon glyphicon-tags red"></i></a>--}%
        %{--</g:if>--}%
        %{--${raw(ficha.noCientificoItalico)}--}%
        %{--</legend>--}%
        %{--</fieldset>--}%


        <%-- arvore taxonomica da espécie selecionada --%>
        <%--     <div id="divBreadcrumb">
                <ol class="breadcrumb" style=" background-color:#FFFFFF;">
                    <g:each in="${ficha.taxon.hierarchy}" var="nome" status="i">
                        <li id="breadcrumb_nivel_${i + 1}" data-value="${nome}" class="breadcrumb-item">${i > 5 ? nome.toLowerCase() : nome.capitalize()}</li>
                    </g:each>
                </ol>
            </div>
        --%>
        </div>
    </div>

    <%--  Taxonomia --%>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-success" id="panel-taxonomia">
                <div class="panel-heading">
                    <a class="cursor-pointer" data-toggle="collapse" data-target="#panel-taxonomia-body">Classificação Taxonômica</a>&nbsp;
                    <span id="status-taxonomia" class="red bold" aria-hidden="true"></span>
                    <g:if test="${false && canModify }">
                        <button class="btn btn-default btn-xs pull-right mr5"
                                data-contexto="consulta" data-sq-ficha="${ficha.id}"
                                data-assunto="Classificação taxonômica"
                                title="Registrar pendência"
                                data-action="showFormPendencia" data-on-close="updateGridPendencia"
                                style="border:none;cursor: pointer;"><i class="fa fa-tags red" aria-hidden="true"></i></button>
                    </g:if>
                    <button class="btn btn-default btn-xs pull-right open-close-all" title="Abrir/Fechar todos os grupos." onClick="openCloseAllPanels()"><i class="fa fa-folder-open"></i></button>

                </div>
                <div class="panel-body panel-collapse collapse" id="panel-taxonomia-body">
                    <div class="row">

                        <div id="ficha-tab-taxonomia" class="taxonomia-nivel col-sm-9">
                            <div class="nivel">Filo:</div>
                            <div class="nome">${taxonStructure.nmFilo}</div><br/>
                            <div class="nivel">Classe:</div>
                            <div class="nome">${taxonStructure.nmClasse}</div><br/>
                            <div class="nivel">Ordem:</div>
                            <div class="nome">${taxonStructure.nmOrdem}</div><br/>
                            <div class="nivel">Família:</div>
                            <div class="nome">${taxonStructure.nmFamilia}</div><br/>
                            <div class="nivel">Gênero:</div>
                            <div class="nome"><i>${taxonStructure.nmGenero}</i></div><br/>
                            <div class="nivel">Espécie:</div>
                            <div class="nome"><i>${taxonStructure.nmEspecie}</i></div><br/>
                            <g:if test="${taxonStructure.nmSubespecie}">
                                <div class="nivel">Subespécie:</div>
                                <div class="nome"><i>${taxonStructure.nmCientifico}</i></div><br/>
                            </g:if>
                        </div>

                        %{--IMAGEM PRINCIPAL DA ESPECIE--}%
                        <div class="taxonomia-nivel col-sm-3 text-right">
                            <g:if test="${dadosImagemPrincipal}">
                                <img title="clique para visualizar em tamanho real!" src="/salve-estadual/fichaMultimidia/getMultimidia/${dadosImagemPrincipal.id}" alt="${ficha.noCientificoItalico}" class="tooltipster img-preview img-rounded" style="margin-top:5px; width: 140px; height: 140px;">
                            </g:if>
                            <g:else>
                                <img alt="imagem não disponível" class="img-rounded" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMTQwIiBoZWlnaHQ9IjE0MCIgdmlld0JveD0iMCAwIDE0MCAxNDAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjwhLS0KU291cmNlIFVSTDogaG9sZGVyLmpzLzE0MHgxNDAKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNjQ4YTY0ZmQwNCB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE2NDhhNjRmZDA0Ij48cmVjdCB3aWR0aD0iMTQwIiBoZWlnaHQ9IjE0MCIgZmlsbD0iI0VFRUVFRSIvPjxnPjx0ZXh0IHg9IjQ1IiB5PSI3NC44Ij4xNDB4MTQwPC90ZXh0PjwvZz48L2c+PC9zdmc+" data-holder-rendered="true" style="margin-top:5px; width: 140px; height: 140px;">
                            </g:else>
                        </div>

                    </div>

                    <%-- usar a taglib FichaTagLib.gsp --%>
                    <g:renderPanelColaboracoes sqFichaVersao="${sqFichaVersao}" canModify="${canModify}" grupo="taxonomia" campoFicha="nomesComuns" campoColaboracao="nomesComuns" rotulo="Nomes Comuns" ficha="${ficha}" verColaboracoes="${verColaboracoes}" contexto="${contexto}" listSituacaoColaboracao="${listSituacaoColaboracao}"/>
                    <g:renderPanelColaboracoes sqFichaVersao="${sqFichaVersao}" canModify="${canModify}" grupo="taxonomia" campoFicha="sinonimiasHtml" campoColaboracao="sinonimias" rotulo="Nomes Antigos" ficha="${ficha}" verColaboracoes="${verColaboracoes}" contexto="${contexto}" listSituacaoColaboracao="${listSituacaoColaboracao}" />
                    <g:renderPanelColaboracoes sqFichaVersao="${sqFichaVersao}" canModify="${canModify}" grupo="taxonomia" campoFicha="dsNotasTaxonomicas" campoColaboracao="dsNotasTaxonomicas" rotulo="Notas Taxonômicas" ficha="${ficha}" verColaboracoes="${verColaboracoes}" contexto="${contexto}" listSituacaoColaboracao="${listSituacaoColaboracao}" edit="true" />
                    <g:renderPanelColaboracoes sqFichaVersao="${sqFichaVersao}" canModify="${canModify}" grupo="taxonomia" campoFicha="dsDiagnosticoMorfologico" campoColaboracao="dsDiagnosticoMorfologico" rotulo="Notas Morfológicas" ficha="${ficha}" verColaboracoes="${verColaboracoes}" contexto="${contexto}" listSituacaoColaboracao="${listSituacaoColaboracao}" edit="true" />
                </div>
            </div>
        </div>
    </div>

    <%--  Distribuição Geográfica --%>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-success" id="panel-distribuicao-geografica">
                <div class="panel-heading">
                    <a class="cursor-pointer" data-toggle="collapse" data-target="#panel-distribuicao-geografica-body">Distribuição Geográfica</a>
                    <g:if test="${ contexto =~ /consulta|oficina/ }">
                        <span id="status-distribuicao-geografica" class="red bold"></span>
                    </g:if>
                    <g:if test="${false && canModify }">
                        <button class="btn btn-default btn-xs pull-right mr5"
                                data-contexto="${contexto}" data-sq-ficha="${ficha.id}"
                                data-assunto="Distribuição geográfica"
                                title="Registrar pendência"
                                data-action="showFormPendencia" data-on-close="updateGridPendencia"
                                style="border:none;cursor: pointer;"><i class="fa fa-tags red" aria-hidden="true"></i></button>
                    </g:if>
                </div>

                <div class="panel-body panel-collapse collapse" id="panel-distribuicao-geografica-body">
                    <div class="div-question">Endêmica do Brasil?</div>
                    <div class="div-answer">${ficha.endemicaBrasil}</div><br/>
                    <g:renderPanelColaboracoes sqFichaVersao="${sqFichaVersao}" canModify="${canModify}" grupo="distribuicao-geografica" campoFicha="dsDistribuicaoGeoGlobal" campoColaboracao="dsDistribuicaoGeoGlobal" rotulo="Distribuição Global" ficha="${ficha}" verColaboracoes="${verColaboracoes}" contexto="${contexto}" listSituacaoColaboracao="${listSituacaoColaboracao}" edit="true" />
                    <g:renderPanelColaboracoes sqFichaVersao="${sqFichaVersao}" canModify="${canModify}" grupo="distribuicao-geografica" campoFicha="dsDistribuicaoGeoNacional" campoColaboracao="dsDistribuicaoGeoNacional" rotulo="Distribuição Nacional" ficha="${ficha}" verColaboracoes="${verColaboracoes}" contexto="${contexto}" listSituacaoColaboracao="${listSituacaoColaboracao}" edit="true" />

                    <%-- Imagem Principal de Mapa Distribuição --%>
                    <div class="panel panel-success panel-colaboracao mt10" id="panel-imagem-mapa-distribuicao" data-url-imagem="ficha/showImagem?sqFicha=${ficha.id}&tipo=mapa-distribuicao">
                        <div class="panel-heading"><a class="cursor-pointer" data-toggle="collapse" data-target="#panel-imagem-mapa-distribuicao-body"><i class="fa fa-picture-o"></i>&nbsp;Imagem Principal do Mapa de Distribuição</a></div>
                        <div class="panel-body text-center panel-collapse collapse" id="panel-imagem-mapa-distribuicao-body">
                            <img class="img-responsive img-rounded img-thumbnail" width="99%" height="auto" src="" alt="Mapa Distribuição" />
                        </div>
                    </div>

                    <%-- <g:if test="${ contexto.toLowerCase() != 'validador'}"> --%>
                    <%-- Mapa da ocorrências --%>
                    <div class="panel panel-success panel-colaboracao mt10" id="panel-mapa-ocorrencias" data-mapa="S" data-sq-ficha="${ficha.id}">
                        <div class="panel-heading"><a class="cursor-pointer" data-toggle="collapse" data-target="#panel-mapa-ocorrencias-body"><i style="color:#0000ff;" class="fa fa-map-marker"></i>&nbsp;${fichaVersionada ? '' : 'Mapa de'} Registros de Ocorrência</a>
                            <g:if test="${ contexto =~ /consulta|oficina/}">
                                <span id="sub-status-mapa-ocorrencias" class="red bold"></span>
                            </g:if>
                        </div>
                        <div class="panel-body panel-collapse collapse" id="panel-mapa-ocorrencias-body">
                            <g:if test="${ !fichaVersionada }">
                                <div id="div-mapa-ocorrencias-${ficha?.id}"
                                     data-read-only="${canModify==false || contexto=='validador'}"  data-sq-ficha-versao="${sqFichaVersao}"
                                     data-contexto="${contexto}" data-loaded="N" class="map" style="width: 800px;height: 600px"></div>
                                <br>
                            </g:if>
                            %{--<div id="mapaFichaCompletaLegend-${ficha?.id}">--}%
                            %{--<fieldset>--}%
                            %{--<legend style="cursor:pointer;">Legenda</legend>--}%
                            %{--<div style="font-size:12px;" id="conteudo"></div>--}%
                            %{--</fieldset>--}%
                            %{--</div>--}%

                            <div id="div-gride-ocorrencias-${ficha?.id}" data-read-only="${canModify==false}"  data-sq-ficha-versao="${sqFichaVersao}" data-contexto="${contexto}" data-loaded="N" class="gride" style="width: 100%;height: auto"></div>
                        </div>
                    </div>
                <%--</g:if> --%>

                <%-- Estados --%>
                    <g:renderPanelColaboracoes sqFichaVersao="${sqFichaVersao}" canModify="${canModify}" grupo="distribuicao-geografica" campoFicha="estadosPorRegiao" campoColaboracao="estados" rotulo="Estados" ficha="${ficha}" verColaboracoes="${verColaboracoes}" contexto="${contexto}" listSituacaoColaboracao="${listSituacaoColaboracao}" />

                <%-- Biomas --%>
                    <g:renderPanelColaboracoes sqFichaVersao="${sqFichaVersao}" canModify="${canModify}" grupo="distribuicao-geografica" campoFicha="biomas" campoColaboracao="biomas" rotulo="Biomas" ficha="${ficha}" verColaboracoes="${verColaboracoes}" contexto="${contexto}" listSituacaoColaboracao="${listSituacaoColaboracao}" />

                <%-- Areas Relevantes --%>
                    <g:renderPanelColaboracoes sqFichaVersao="${sqFichaVersao}" canModify="${canModify}" grupo="distribuicao-geografica" campoFicha="areasRelevantes" campoColaboracao="areasRelevantes" rotulo="Áreas Relevantes" ficha="${ficha}" verColaboracoes="${verColaboracoes}" contexto="${contexto}" listSituacaoColaboracao="${listSituacaoColaboracao}" />

                </div>
            </div>
        </div>
    </div>

    <%--  História Natural --%>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-success" id="panel-historia-natural" data-habito-alimentar="S" data-sq-ficha="${ficha.id}">
                <div class="panel-heading">
                    <a class="cursor-pointer" data-toggle="collapse" data-target="#panel-historia-natural-body">História Natural</a>
                    <span id="status-historia-natural" class="red bold"></span>
                    <g:if test="${false && canModify }">
                        <button class="btn btn-default btn-xs pull-right mr5"
                                data-contexto="${contexto}" data-sq-ficha="${ficha.id}"
                                data-assunto="História natual"
                                title="Registrar pendência"
                                data-action="showFormPendencia" data-on-close="updateGridPendencia"
                                style="border:none;cursor: pointer;"><i class="fa fa-tags red" aria-hidden="true"></i></button>
                    </g:if>

                </div>
                <div class="panel-body panel-collapse collapse" id="panel-historia-natural-body">
                    <div class="div-question">Espécie é Migratória?</div>
                    <div class="div-answer">${ficha.especieMigratoria}</div><br/>

                    <g:renderPanelColaboracoes sqFichaVersao="${sqFichaVersao}" canModify="${canModify}" grupo="historia-natural" campoFicha="dsHistoriaNatural" campoColaboracao="dsHistoriaNatural" rotulo="..." ficha="${ficha}" verColaboracoes="${verColaboracoes}" contexto="${contexto}" listSituacaoColaboracao="${listSituacaoColaboracao}" edit="true" opened="true"/>

                    <div class="panel panel-success panel-colaboracao mt10" id="panel-habito-alimentar" data-habito-alimentar="S" data-sq-ficha="${ficha.id}">
                        <div class="panel-heading"><a class="cursor-pointer" data-toggle="collapse" data-target="#panel-habito-alimentar-body">Hábito Alimentar</a></div>
                        <div class="panel-body panel-collapse collapse" id="panel-habito-alimentar-body">
                            <div id="div-gride-habito-alimentar-${ficha?.id}"></div>
                        </div>
                    </div>

                    <div class="panel panel-success panel-colaboracao mt10" id="panel-habitat" data-habitat="S" data-sq-ficha="${ficha.id}">
                        <div class="panel-heading"><a class="cursor-pointer" data-toggle="collapse" data-target="#panel-habitat-body">Habitat</a>
                            &nbsp;<span id="sub-status-dsUsoHabitat-${ficha.id}" class="green"></span>
                        </div>

                        <div class="panel-body panel-collapse collapse" id="panel-habitat-body">
                            <div class="div-question">Restrito a habitat primário?</div>
                            <div class="div-answer">${ficha.snd(ficha.stRestritoHabitatPrimario)}</div><br/>
                            <div class="div-question">Especialista em micro habitat?</div>
                            <div class="div-answer">${ficha.snd(ficha.stEspecialistaMicroHabitat)}</div><br/>
                            <div class="div-question">${ficha.dsEspecialistaMicroHabitat}</div>
                            <center>
                                <div style="max-width:75%">
                                    <div id="div-gride-habitat-${ficha?.id}" class="mt10"></div>
                                </div>
                            </center>
                            <g:renderPanelColaboracoes sqFichaVersao="${sqFichaVersao}" hideStatus="true" canModify="${canModify}" grupo="historia-natural" campoFicha="dsUsoHabitat" campoColaboracao="dsUsoHabitat" rotulo="Observações Sobre o Habitat" ficha="${ficha}" verColaboracoes="${verColaboracoes}" contexto="${contexto}" listSituacaoColaboracao="${listSituacaoColaboracao}" edit="true" />
                        </div>
                    </div>


                    <g:renderPanelColaboracoes sqFichaVersao="${sqFichaVersao}" canModify="${canModify}" grupo="historia-natural" campoFicha="dsInteracao" campoColaboracao="dsInteracao" rotulo="Interação com Outras Espécies" ficha="${ficha}" verColaboracoes="${verColaboracoes}" contexto="${contexto}" listSituacaoColaboracao="${listSituacaoColaboracao}" edit="true" opened="true"/>
                    <div id="div-gride-interacao-${ficha?.id}"></div>

                    <div class="panel panel-success panel-colaboracao mt10" id="panel-reproducao" data-sq-ficha="${ficha.id}">
                        <div class="panel-heading"><a class="cursor-pointer" data-toggle="collapse" data-target="#panel-reproducao-body">Reprodução</a>
                            &nbsp;<span id="sub-status-dsReproducao-${ficha.id}" class="green"></span>
                        </div>
                        <div class="panel-body panel-collapse collapse" id="panel-reproducao-body">
                            <div class="div-question">Intervalo de Nascimento:</div>
                            <div class="div-answer">${ficha.intervaloNascimentoText}</div><br/>
                            <div class="div-question">Tempo de gestação:</div>
                            <div class="div-answer">${ficha.tempoGestacaoText}</div><br/>
                            <div class="div-question">Tamanho da prole:</div>
                            <div class="div-answer">${ficha.tamanhoProleText}</div><br/>
                            <div style="max-width:350px">
                                <table class="table table-condensed table-sm table-hover table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Campo</th>
                                        <th>Macho</th>
                                        <th>Fêmea</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>Maturidade Sexual</td>
                                        <td>${ficha.vlMaturidadeSexualMacho ? ficha.vlMaturidadeSexualMacho.toString() + ' ' + ficha.unidMaturidadeSexualMacho?.descricao : ''}</td>
                                        <td>${ficha.vlPesoFemea ? ficha.vlPesoFemea.toString() + ' ' + ficha.unidadePesoFemea?.descricao : ''}</td>
                                    </tr>
                                    <tr>
                                        <td>Peso</td>
                                        <td>${ficha.vlPesoMacho ? ficha.vlPesoMacho.toString() + ' ' + ficha.unidadePesoMacho?.descricao : ''}</td>
                                        <td>${ficha.vlPesoFemea ? ficha.vlPesoFemea.toString() + ' ' + ficha.unidadePesoFemea?.descricao : ''}</td>
                                    </tr>
                                    <tr>
                                        <td>Comprimento</td>
                                        <td>${ficha.vlComprimentoMacho ? ficha.vlComprimentoMacho.toString() + ' ' + ficha.medidaComprimentoMacho?.descricao : ''}</td>
                                        <td>${ficha.vlComprimentoFemea ? ficha.vlComprimentoFemea.toString() + ' ' + ficha.medidaComprimentoFemea?.descricao : ''}</td>
                                    </tr>
                                    <tr>
                                        <td>Senilidade reprodutiva</td>
                                        <td>${ficha.vlSenilidadeReprodutivaMacho ? ficha.vlSenilidadeReprodutivaMacho.toString() + ' ' + ficha.unidadeSenilidRepMacho?.descricao : ''}</td>
                                        <td>${ficha.vlSenilidadeReprodutivaFemea ? ficha.vlSenilidadeReprodutivaFemea.toString() + ' ' + ficha.unidadeSenilidRepFemea?.descricao : ''}</td>
                                    </tr>
                                    <tr>
                                        <td>Longevidade</td>
                                        <td>${ficha.vlLongevidadeMacho ? ficha.vlLongevidadeMacho.toString() + ' ' + ficha.unidadeLongevidadeMacho?.descricao : ''}</td>
                                        <td>${ficha.vlLongevidadeFemea ? ficha.vlLongevidadeFemea.toString() + ' ' + ficha.unidadeLongevidadeFemea?.descricao : ''}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <g:renderPanelColaboracoes sqFichaVersao="${sqFichaVersao}" canModify="${canModify}" hideStatus="true" grupo="historia-natural" campoFicha="dsReproducao" campoColaboracao="dsReproducao" rotulo="..." ficha="${ficha}" verColaboracoes="${verColaboracoes}" contexto="${contexto}" listSituacaoColaboracao="${listSituacaoColaboracao}" edit="true" opened="true"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> %{-- FIM HISTORIA NATURAL--}%

<%-- População --%>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-success panel-grupo" id="panel-populacao" data-populacao="S" data-sq-ficha="${ficha.id}">
                <div class="panel-heading">
                    <a class="cursor-pointer" data-toggle="collapse" data-target="#panel-populacao-body">População</a>
                    <span id="status-populacao" class="red bold"></span>
                    %{--<g:if test="${canModify }">
                        <button class="btn btn-default btn-xs pull-right mr5"
                                data-contexto="consulta" data-sq-ficha="${ficha.id}"
                                data-assunto="População"
                                title="Registrar pendência"
                                data-action="showFormPendencia" data-on-close="updateGridPendencia"
                                style="border:none;cursor: pointer;"><i class="fa fa-tags red" aria-hidden="true"></i></button>
                    </g:if>--}%

                </div>
                <div class="panel-body panel-collapse collapse" id="panel-populacao-body">
                    <div class="div-question">Tendência Populacional:</div>
                    <div class="div-answer">${ficha?.tendenciaPopulacional?.descricao}</div><br/>
                    <g:if test="${ficha?.vlTempoGeracional}">
                        <div class="div-question">Tempo geracional:</div>
                        <div class="div-answer">${ficha?.tempoGeracionalText}</div><br/>
                         %{-- GRAFICO--}%
                        <div class="form-group">
                            <label>Gráfico</label>
                            <div style="position:relative;width:400px;height:55px;border:1px solid black;background-color:#efefef;-webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;">
                                %{-- barra central --}%
                                <div style="position:absolute;top:15px;left:10px;border-top:2.5px solid gray;width:380px;height:5px;background-color: #aed8ae;">
                                    %{-- menos anos --}%
                                    <div style="position:absolute;top:-18px;left:70px;border:none;width:50px;height: 20px;font-weight: bold;font-size:10px;text-align: center">${dadosGraficoTempoGeracional.menosAnos.toString()}</div>
                                    %{-- mais anos --}%
                                    <div style="position:absolute;top:-18px;left:250px;border:none;width:50px;height: 20px;font-weight: bold;font-size:10px;text-align: center">+${dadosGraficoTempoGeracional.maisAnos.toString()}</div>
                                    %{-- marca inicial --}%
                                    <div style="position:absolute;top:-5px;left:20px;border-left:3px solid black;width:10px;height:10px;"></div>
                                    %{-- marca central --}%
                                    <div style="position:absolute;top:-5px;left:190px;border-left:3px solid black;width:10px;height:10px;"></div>
                                    %{-- marca final --}%
                                    <div style="position:absolute;top:-5px;left:360px;border-left:3px solid black;width:10px;height:10px;"></div>
                                    %{-- ano inicial --}%
                                    <div style="position:absolute;top:5px;left:3px;border:none;width:35px;height: 20px;font-weight: bold;font-size:14px;text-align:center;">${dadosGraficoTempoGeracional.anoInicial}</div>
                                    %{-- ano avaliacao --}%
                                    <div style="position:absolute;top:5px;left:175px;border:none;width:35px;height: 20px;font-weight: bold;font-size:14px;">${dadosGraficoTempoGeracional.anoAvaliacao}</div>
                                    %{-- ano final --}%
                                    <div style="position:absolute;top:5px;left:345px;border:none;width:35px;height: 20px;font-weight: bold;font-size:14px;">${dadosGraficoTempoGeracional.anoFinal}</div>
                                    %{-- texto central --}%
                                    <div style="position:absolute;top:18px;left:0px;border:none;width:380px;height:10px;text-align: center;font-size:12px;">Ano da Avaliação</div>
                                </div>
                            </div>
                        </div>
                       %{--                    FIM GRAFICO--}%
                    </g:if>

                    <g:renderPanelColaboracoes sqFichaVersao="${sqFichaVersao}" canModify="${canModify}" grupo="populacao" campoFicha="dsPopulacao" campoColaboracao="dsPopulacao" rotulo="Observações Sobre a População" ficha="${ficha}" verColaboracoes="${verColaboracoes}" contexto="${contexto}" listSituacaoColaboracao="${listSituacaoColaboracao}" edit="true" />
                    <g:renderPanelColaboracoes sqFichaVersao="${sqFichaVersao}" canModify="${canModify}" grupo="populacao" campoFicha="dsCaracteristicaGenetica" campoColaboracao="dsCaracteristicaGenetica" rotulo="Características Genéticas" ficha="${ficha}" verColaboracoes="${verColaboracoes}" contexto="${contexto}" listSituacaoColaboracao="${listSituacaoColaboracao}" edit="true" />

                    <g:if test="${contexto =~ /oficina|validacao/}">
                        <%-- Anexos populacao --%>
                        <div class="panel panel-success panel-colaboracao mt10" id="panel-anexos-populacao">
                            <div class="panel-heading">
                                <a class="cursor-pointer"
                                   data-toggle="collapse"
                                   data-target="#panel-anexos-populacao-body">
                                    <i class="fa fa-table"></i>&nbsp;Anexos
                                </a>
                            </div>
                            <div class="panel-body text-center panel-collapse collapse" id="panel-anexos-populacao-body">
                            </div>
                        </div>
                    </g:if>
                </div>
            </div>
        </div>
    </div> %{-- FIM POPULAÇÃO --}%

<%-- Ameaças --%>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-success panel-grupo" id="panel-ameaca" data-ameaca="S" data-sq-ficha="${ficha.id}">
                <div class="panel-heading">
                    <a class="cursor-pointer" data-toggle="collapse" data-target="#panel-ameaca-body">Ameaça</a>
                    <span id="status-ameaca" class="red bold"></span>
                    <g:if test="${false && canModify }">
                        <button class="btn btn-default btn-xs pull-right mr5"
                                data-contexto="consulta" data-sq-ficha="${ficha.id}"
                                data-assunto="Ameaça"
                                title="Registrar pendência"
                                data-action="showFormPendencia" data-on-close="updateGridPendencia"
                                style="border:none;cursor: pointer;"><i class="fa fa-tags red" aria-hidden="true"></i></button>
                    </g:if>

                </div>
                <div class="panel-body panel-collapse collapse" id="panel-ameaca-body">
                    <g:renderPanelColaboracoes sqFichaVersao="${sqFichaVersao}" canModify="${canModify}" grupo="ameaca" campoFicha="dsAmeaca" campoColaboracao="dsAmeaca" rotulo="..." ficha="${ficha}" verColaboracoes="${verColaboracoes}" contexto="${contexto}" listSituacaoColaboracao="${listSituacaoColaboracao}" edit="true" opened="true"/>
                    <br>
                    <div class="text-center">
                        <div style="max-width:75%;">
                            <div id="div-gride-ameaca-${ficha?.id}"></div>
                        </div>
                    </div>
                    <g:if test="${contexto =~ /oficina|validacao/}">
                        <%-- Imagem Mapa Ameaça mais recente --%>
                        %{--<div class="panel panel-success panel-colaboracao mt10" id="panel-imagem-ameaca" data-url-imagem="ficha/showImagem?sqFicha=${ficha.id}&tipo=mapa-ameaca">
                            <div class="panel-heading"><a class="cursor-pointer" data-toggle="collapse" data-target="#panel-imagem-mapa-ameaca-body"><i class="fa fa-picture-o"></i>&nbsp;Imagem Ameaça</a></div>
                            <div class="panel-body text-center panel-collapse collapse" id="panel-imagem-mapa-ameaca-body">
                                <img class="img-responsive img-rounded img-thumbnail" width="99%" height="auto" src="" alt="Mapa Ameaça" />
                            </div>
                        </div>--}%
                        <%-- Anexos das ameaças --%>
                        <div class="panel panel-success panel-colaboracao mt10" id="panel-anexos-ameaca">
                            <div class="panel-heading">
                                    <a class="cursor-pointer"
                                       data-toggle="collapse"
                                       data-target="#panel-anexos-ameaca-body">
                                        <i class="fa fa-table"></i>&nbsp;Anexos
                                    </a>
                            </div>
                            <div class="panel-body text-center panel-collapse collapse" id="panel-anexos-ameaca-body">
                            </div>
                        </div>
                    </g:if>
                </div>
            </div>
        </div>
    </div> %{-- FIM AMEAÇAS --}%


<%-- Uso --%>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-success panel-grupo" id="panel-uso" data-uso="S" data-sq-ficha="${ficha.id}">
                <div class="panel-heading">
                    <a class="cursor-pointer" data-toggle="collapse" data-target="#panel-uso-body">Uso</a>
                    <span id="status-uso" class="red bold"></span>
                    <g:if test="${false && canModify }">
                        <button class="btn btn-default btn-xs pull-right mr5"
                                data-contexto="consulta" data-sq-ficha="${ficha.id}"
                                data-assunto="Uso"
                                title="Registrar pendência"
                                data-action="showFormPendencia" data-on-close="updateGridPendencia"
                                style="border:none;cursor: pointer;"><i class="fa fa-tags red" aria-hidden="true"></i></button>
                    </g:if>

                </div>
                <div class="panel-body panel-collapse collapse" id="panel-uso-body">
                    <g:renderPanelColaboracoes sqFichaVersao="${sqFichaVersao}" canModify="${canModify}" grupo="uso" campoFicha="dsUso" campoColaboracao="dsUso" rotulo="..." ficha="${ficha}" verColaboracoes="${verColaboracoes}" contexto="${contexto}" listSituacaoColaboracao="${listSituacaoColaboracao}" edit="true" opened="true" />
                    <br>

                    <div class="text-center">
                        <div style="max-width:75%;">
                            <div id="div-gride-uso-${ficha?.id}"></div>
                        </div>
                    </div>
                    <g:if test="${contexto =~ /oficina|validacao/}">
                    <%-- Anexos dos usos --%>
                        <div class="panel panel-success panel-colaboracao mt10" id="panel-anexos-usos">
                            <div class="panel-heading">
                                <a class="cursor-pointer"
                                   data-toggle="collapse"
                                   data-target="#panel-anexos-usos-body">
                                    <i class="fa fa-table"></i>&nbsp;Anexos
                                </a>
                            </div>
                            <div class="panel-body text-center panel-collapse collapse" id="panel-anexos-usos-body">
                            </div>
                        </div>
                    </g:if>
                </div>
            </div>
        </div>
    </div> %{-- FIM USO --}%


<%-- Conservação --%>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-success panel-grupo" id="panel-conservacao" data-conservacao="S" data-sq-ficha="${ficha.id}">
                <div class="panel-heading">
                    <a class="cursor-pointer" data-toggle="collapse" data-target="#panel-conservacao-body">Conservação</a>
                    <span id="status-conservacao" class="red bold"></span>
                    <g:if test="${false && canModify }">
                        <button class="btn btn-default btn-xs pull-right mr5"
                                data-contexto="consulta" data-sq-ficha="${ficha.id}"
                                data-assunto="Conservação"
                                title="Registrar pendência"
                                data-action="showFormPendencia" data-on-close="updateGridPendencia"
                                style="border:none;cursor: pointer;"><i class="fa fa-tags red" aria-hidden="true"></i></button>
                    </g:if>

                </div>
                <div class="panel-body panel-collapse collapse" id="panel-conservacao-body">
                    <fieldset id="fieldset-ultima-avaliacao-nacional-${ficha?.id}">
                        <label><b>Última Avaliação Nacional</b></label>
                        <br/>
                        <div class="div-question" id="nao-avaliada" class="hidden">Não Avaliada</div>
                        <div id="avaliada">
                            <div class="div-question">Ano</div>
                            <div class="div-answer" id="ano"></div><br/>
                            <div class="div-question">Categoria</div>
                            <div class="div-answer" id="categoria"></div><br/>
                            <div class="div-question">Critério</div>
                            <div class="div-answer" id="criterio"></div><br/>
                            <div class="div-question">Justificativa</div>
                            <div class="div-answer" id="justificativa"></div>
                        </div>
                    </fieldset>

                    <fieldset id="fieldset-historico-avaliacoes-${ficha?.id}">
                        <div id="gride-historico"></div>
                    </fieldset>

                    <div class="mt20">
                        <div class="div-question">Presença em Lista Nacional Oficial de Espécies Ameaçadas de Extinção?&nbsp;</div>
                        <div class="div-answer"> ${ficha.snd( ficha.stPresencaListaVigente)}</div>
                    </div>

                    <div id="div-outros-grides-${ficha?.id}">

                        <div id="gride-convencoes"></div>

                        <g:renderPanelColaboracoes sqFichaVersao="${sqFichaVersao}" canModify="${canModify}" grupo="conservacao" campoFicha="dsAcaoConservacao" campoColaboracao="dsAcaoConservacao" rotulo="Ações de Conservação" ficha="${ficha}" verColaboracoes="${verColaboracoes}" contexto="${contexto}" listSituacaoColaboracao="${listSituacaoColaboracao}" edit="true" />
                        <br/>
                        <center>
                            <div style="max-width:75%">
                                <div id="gride-acoes"></div>
                            </div>
                        </center>

                        <g:renderPanelColaboracoes sqFichaVersao="${sqFichaVersao}" canModify="${canModify}" grupo="conservacao" campoFicha="dsPresencaUc" campoColaboracao="dsPresencaUc" rotulo="Presença em UC" ficha="${ficha}" verColaboracoes="${verColaboracoes}" contexto="${contexto}" listSituacaoColaboracao="${listSituacaoColaboracao}" edit="true" />
                        <br/>
                        <center>
                            <div style="max-width:75%">
                                <div id="gride-uc"></div>
                            </div>
                        </center>
                    </div>

%{--                    INICIO--}%

                %{--Situação Regional--}%
                    <g:if test="${ !( contexto =~ /consulta|validador/ ) }">
                        <div class="panel panel-default mt10 text-left" style="margin-bottom:-2px;" id="panel-situacao-regional-${ficha?.id}">
                            <div class="panel-heading">
                                <a class="cursor-pointer collapsed" data-toggle="collapse" data-target="#panel-situacao-regional-${ficha?.id}-body" aria-expanded="false">Situação Regional</a>
                            </div>
                            <div class="panel-body panel-collapse collapse" id="panel-situacao-regional-${ficha.id}-body" aria-expanded="false" style="height: 10px;">
                                <g:if test="${canModify }">
                                %{-- BOTAO ABRIR/FECHAR O FORMULARIO DA SITUACAO REGIONAL --}%
                                    <a href="#frmAvaliacaoRegional"
                                       id="btnShowHidefrmAvaliacaoRegional"
                                       data-toggle="collapse"
                                       onClick="toggleImage(this)"
                                       class="fld mt10 btn btn-xs btn-default collapsed">Cadastrar&nbsp;<i class="fa fa-chevron-down"></i></span></a>

                                    <form id="frmAvaliacaoRegional" name="frmAvaliacaoRegional" role="form" class="form-inline collapse">
                                        <input type="hidden" name="sqFichaAvaliacaoRegional" id="sqFichaAvaliacaoRegional" value="">
                                %{-- ABRANGENCIA --}%
                                    <div class="form-group">
                                        <label class="control-label">Tipo da abrangência</label>
                                        <br>
                                        <select id="sqTipoAbrangencia"
                                                name="sqTipoAbrangencia"
                                                data-sq-ficha="${ficha.id}"
                                                data-contexto="frmAvaliacaoRegional"
                                                data-change="tipoAbrangenciaChange" class="fld form-control fld200">
                                            <option data-codigo="" value="">?</option>
                                            <g:each var="item" in="${listTipoAbrangencia}">
                                                <option data-codigo="${item.codigo}" value="${item.id}">${item.descricao}</option>
                                            </g:each>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">&nbsp;</label>
                                        <br>
                                        <span>-</span>
                                        <select id="sqAbrangencia"
                                                name="sqAbrangencia"
                                                class="fld form-control select2 fld600"
                                                disabled="true"
                                                data-s2-params="sqTipoAbrangencia"
                                                data-s2-url="ficha/getAbrangencias"
                                                data-s2-minimum-input-length="1"
                                                required="true">
                                            <option value="">?</option>
                                        </select>
                                    </div>
                                    <br/>
                                %{--FIM ABRANGÊNCIA--}%
                                %{--
                                <div class="fld form-group">
                                    <label for="nuAnoAvaliacao" class="control-label">Ano</label>
                                    <br>
                                    <input name="nuAnoAvaliacao" id="nuAnoAvaliacao" type="text" class="fld form-control number fld100" data-mask="0000" required="true" />
                                </div>
                                --}%
                                    <div class="fld form-group">
                                        <label for="sqCategoriaIucn" class="control-label">Categoria</label>
                                        <br>
                                        <select name="sqCategoriaIucn" id="sqCategoriaIucn"
                                                data-change="app.categoriaIucnChange"
                                                data-sq-ficha="${ficha.id}"
                                                data-container="frmAvaliacaoRegional"
                                                class="fld form-control fld300 fldSelect">
                                            <option value="">-- selecione --</option>
                                            <g:each var="item" in="${listCategoriaIucn}">
                                                <option data-codigo="${item.codigoSistema}" value="${item.id}">${item.descricaoCompleta}</option>
                                            </g:each>
                                        </select>
                                    </div>
                                    <div class="form-group" style="display:none;">
                                        <label for="dsCriterioAvalIucn" class="control-label">Critério</label>
                                        <br>
                                        <input name="dsCriterioAvalIucn" id="dsCriterioAvalIucn"
                                               onkeyup="ficha.fldCriterioAvaliacaoKeyUp(this);" readonly type="text"
                                               class="fld form-control fld300 blue"/>
                                        <button type="button"
                                                class="fld btn btn-default btn-sm" title="Clique para Selecionar o Critério"
                                                data-action="openModalSelCriterioAvaliacao"
                                                data-field="dsCriterioAvalIucn"
                                                data-field-categoria="sqCategoriaIucn"
                                                data-container="frmAvaliacaoRegional"
                                                data-sq-ficha="${ficha.id}">...</button>
                                    </div>
                                %{-- Possivelmente Extinda --}%
                                    <div class="form-group" style="display:none">
                                        <label for="stPossivelmenteExtinta" class="control-label cursor-pointer label-checkbox" style="margin-top:17px;">Possivelmente extinta?
                                            <input name="stPossivelmenteExtinta" id="stPossivelmenteExtinta"
                                                   type="checkbox"
                                                   class="fld checkbox-lg" value="S"/>

                                        </label>
                                    </div>
                                    <br>
                                %{--
                                <div class="fld form-group">
                                    <label for="sqCategoriaIucn" class="control-label">Categoria regional</label>
                                    <br>
                                    <input type="text" class="fld form-control fld600" name="deCriterioAvalRegional" id="deCriterioAvalRegional" value=""/>
                                </div>
                                --}%
                                    <div class="fld form-group w100p">
                                        <label for="dsJustificativa" class="control-label">Justificativa situação regional</label>
                                        <br>
                                        <textarea name="dsJustificativa" id="dsJustificativa" rows="7" class="fld form-control fldTextarea wysiwyg w100p"></textarea>
                                    </div>
                                %{-- botão gravar --}%
                                    <div class="fld panel-footer">
                                        <button type="button" id="btnSaveFrmAvaliacaoRegional" data-action="saveFrmAvaliacaoRegional" data-sq-ficha="${ficha.id}" data-contexto="frmAvaliacaoRegional" class="fld btn btn-success">Gravar situação regional</button>
                                        <button type="button" id="btnResetFrmAvaliacaoRegional" data-action="resetFrmAvaliacaoRegional" data-sq-ficha="${ficha.id}" class="fld btn btn-info hide" data-contexto="frmAvaliacaoRegional">Limpar formulário</button>
                                    </div>
                                    </form>
                                </g:if>
                                <div id="divGridHistoricoAvaliacaoRegional" class="mt10"></div>
                            </div>
                        </div>
                    </g:if>
                %{--fim situacao regional--}%



%{--                    FIM --}%


                </div>
            </div>

        </div>
    </div> %{-- FIM CONSERVACAO --}%

<%-- Pesquisas --%>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-success panel-grupo" id="panel-pesquisa" data-pesquisa="S" data-sq-ficha="${ficha.id}">
                <div class="panel-heading">
                    <a class="cursor-pointer" data-toggle="collapse" data-target="#panel-pesquisa-body">Pesquisa</a>
                    <span id="status-pesquisa" class="red bold"></span>
                    <g:if test="${false && canModify }">
                        <button class="btn btn-default btn-xs pull-right mr5"
                                data-contexto="${contexto}" data-sq-ficha="${ficha.id}"
                                data-assunto="Pesquisa"
                                title="Registrar pendência"
                                data-action="showFormPendencia" data-on-close="updateGridPendencia"
                                style="border:none;cursor: pointer;"><i class="fa fa-tags red" aria-hidden="true"></i></button>
                    </g:if>

                </div>
                <div class="panel-body panel-collapse collapse" id="panel-pesquisa-body">
                    <g:renderPanelColaboracoes sqFichaVersao="${sqFichaVersao}" canModify="${canModify}" grupo="pesquisa" campoFicha="dsPesquisaExistNecessaria" campoColaboracao="dsPesquisaExistNecessaria" rotulo="..." ficha="${ficha}" verColaboracoes="${verColaboracoes}" contexto="${contexto}" listSituacaoColaboracao="${listSituacaoColaboracao}" edit="true" opened="true" />
                    <br/>
                    <center>
                        <div style="max-width:75%">
                            <div id="gride-pesquisa"></div>
                        </div>
                    </center>
                </div>
            </div>
        </div>
    </div> %{-- FIM PESQUISA --}%

<%-- Referências Bibliográficas --%>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-success panel-grupo" id="panel-ref-bib" data-ref-bib="S" data-sq-ficha="${ficha.id}">
                <div class="panel-heading">
                    <a class="cursor-pointer" data-toggle="collapse" data-target="#panel-ref-bib-body">Referências Bibliográficas</a>
                    <g:if test="${false && canModify }">
                        <button class="btn btn-default btn-xs pull-right mr5"
                                data-contexto="${contexto}" data-sq-ficha="${ficha.id}"
                                data-assunto="Referência bibliográfica"
                                title="Registrar pendência"
                                data-action="showFormPendencia" data-on-close="updateGridPendencia"
                                style="border:none;cursor: pointer;"><i class="fa fa-tags red" aria-hidden="true"></i></button>
                    </g:if>
                </div>
                <div class="panel-body panel-collapse collapse" id="panel-ref-bib-body">
                    <div id="gride-ref-bib"></div>
                </div>
            </div>
        </div>
    </div> %{-- FIM REF BIBLIOGRÁFICA --}%


<%-- Pendências --%>
%{--<g:if test="${params?.contexto !='consulta' && params?.contexto !='validador'}">--}%
    <g:if test="${params?.contexto !='consulta'}">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-success panel-grupo" id="panel-pendencia" data-pendencia="S" data-sq-ficha="${ficha.id}">
                    <div class="panel-heading">
                        <a class="cursor-pointer" data-toggle="collapse" data-target="#panel-pendencia-body">Pendências</a>
                        %{--<g:if test="${canModify }">
                            <button class="btn btn-default btn-xs pull-right mr5"
                                    data-contexto="consulta" data-sq-ficha="${ficha.id}"
                                    data-assunto=""
                                    title="Registrar pendência"
                                    data-action="showFormPendencia" data-on-close="updateGridPendencia"
                                    style="border:none;cursor: pointer;"><i class="fa fa-tags red" aria-hidden="true"></i></button>
                        </g:if>--}%
                    </div>
                    <div class="panel-body panel-collapse collapse" id="panel-pendencia-body">
                        <div style="max-height:300px;overflow-y:auto;" id="gride-pendencia"></div>
                    </div>
                </div>
            </div>
        </div>
    </g:if>

<%-- Anexos da ficha--%>
    <g:if test="${params?.contexto != 'consulta'}">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-success panel-grupo" id="panel-anexos-ficha" data-anexos-ficha="S"  data-sq-ficha="${ficha.id}">
                    <div class="panel-heading">
                        <a class="cursor-pointer" data-toggle="collapse" data-target="#panel-anexos-ficha-body">Anexos</a>
                    </div>
                    <div class="panel-body panel-collapse collapse" id="panel-anexos-ficha-body">
                        <div style="max-height:400px;overflow-y:auto;" id="gride-anexos-ficha"></div>
                    </div>
                </div>
            </div>
        </div>
    </g:if>




%{-- FIM ANEXOS FICHA --}%

    <g:if test="${ contexto != 'validador' }">
    <%-- CITAÇÃO / AUTORIA --%>
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-success panel-grupo" id="panel-citacao" data-citacao="S" data-sq-ficha="${ficha.id}">
                    <div class="panel-heading">
                        <a class="cursor-pointer" data-toggle="collapse" data-target="#panel-citacao-body">Autoria</a>
                        <g:if test="${false && canModify }">
                            <button class="btn btn-default btn-xs pull-right mr5"
                                    data-contexto="${contexto}" data-sq-ficha="${ficha.id}"
                                    data-assunto="Citação"
                                    title="Registrar pendência"
                                    data-action="showFormPendencia" data-on-close="updateGridPendencia"
                                    style="border:none;cursor: pointer;"><i class="fa fa-tags red" aria-hidden="true"></i></button>
                        </g:if>
                    </div>
                    <div class="panel-body panel-collapse collapse" id="panel-citacao-body">
                        <g:if test="${canModify && contexto != 'publicacao' && oficinaFicha}">
                            <button type="button" title="Clique neste botão para gerar a autoria padrão composta pelo<br>nome do(s) <b>Coordenador(es) de Taxon ativo(s) + Avaliador(es)</b><br>cadastrado(s) na(s) avaliação(ões)."
                            data-action="gerarAutoria" data-update-ficha="true" data-sq-oficina-ficha="${oficinaFicha?.id}" data-sq-ficha="${ficha?.id}" class="btn btn-xs btn-default">Gerar a autoria</button>
                            <g:renderPanelColaboracoes sqFichaVersao="${sqFichaVersao}" canModify="${canModify}" grupo="citacao" campoFicha="dsCitacao" campoColaboracao="dsCitacao" rotulo="..." ficha="${ficha}" verColaboracoes="${verColaboracoes}" contexto="${contexto}" listSituacaoColaboracao="${listSituacaoColaboracao}" edit="true" opened="true" />
                        </g:if>
                        <g:else>
                            <p>${raw(ficha?.dsCitacao ?: 'Não cadastrada')}</p>
                        </g:else>
                    </div>
                </div>
            </div>
        </div> %{-- FIM CITACAO --}%


        <g:if test="${contexto=='publicacao'}">

            <%-- COLABORADORES --%>
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-success panel-grupo" id="panel-colaboradores" data-sq-ficha="${ficha.id}">
                        <div class="panel-heading">
                            <a class="cursor-pointer" data-toggle="collapse" data-target="#panel-colaboradores-body">Colaboradores</a>
                        </div>
                        <div class="panel-body panel-collapse collapse" id="panel-colaboradores-body">
                            <p>${ raw( ficha?.dsColaboradores ?: 'Não cadastrada. Veja aba 11.6')}</p>
                        </div>
                    </div>
                </div>
            </div>
            %{-- FIM COLABORADORES --}%


            %{-- EQUIPE TÉCNICA--}%
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-success panel-grupo" id="panel-equipe-tecnica" data-sq-ficha="${ficha.id}">
                        <div class="panel-heading">
                            <a class="cursor-pointer" data-toggle="collapse" data-target="#panel-equipe-tecnica-body">Equipe técnica</a>
                        </div>
                        <div class="panel-body panel-collapse collapse" id="panel-equipe-tecnica-body">
                            <p>${ raw( ficha?.dsEquipeTecnica ?: 'Não cadastrada. Veja aba 11.6')}</p>
                        </div>
                    </div>
                </div>
                %{-- FIM EQUIPE TÉCNICA--}%

            </div> %{-- FIM COLABORADORES --}%
        </g:if>
    </g:if>


    <g:if test="${ contexto == 'validacao' }">
        <%-- DADOS DA FUNDAMENTAÇÃO --%>
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-success panel-grupo" id="panel-fundamentacao"data-fundamentacao="S"
                    data-sq-oficina-ficha="${oficinaFicha?.id}" data-sq-ficha="${ficha.id}">
                    <div class="panel-heading">
                        <a class="cursor-pointer" data-toggle="collapse" data-target="#panel-fundamentacao-body">Fundamentação</a>
                    </div>
                    <div class="panel-body panel-collapse collapse" id="panel-fundamentacao-body">
                    </div>
                </div>
            </div>
        </div> %{-- FIM FUNDAMENTACAO --}%
    </g:if>



%{-- TELAS PARA VALIDAÇÃO --}%

    <g:if test="${params?.contexto=='consulta'}">
    <%--Encerramento da Ficha na fase consulta --%>

        <div class="row">
            <div class="col-sm-12">

                <g:if test="${ !canModify || ficha.situacaoFicha.codigoSistema == 'CONSOLIDADA' }">
                    <span><h3 style="color:#006400">Situação atual da ficha: ${ficha.situacaoFicha.descricao}</h3></span>
                </g:if>
                <g:else>
                    <a class="btn btn-success mr15" href="javascript:void(0)" data-action="consolidarFicha" data-params="sqFicha:${ficha?.id}">Consolidar</a>
                    <a class="btn btn-primary" href="javascript:void(0)" data-action="salvarRevisaoFechar" data-params="sqFicha:${ficha?.id}">Salvar e fechar</a>
                </g:else>

            %{-- <div class="panel panel-success panel-grupo" id="panel-fim" data-sq-ficha="${ficha.id}">
                <div class="panel-heading"><a class="cursor-pointer red" Xdata-toggle="collapse" data-target="#panel-fim-body">Consolidar Avaliação</a></div>
                <div class="panel-body panel-collapse collapse in" id="panel-fim-body">
                    <g:if test="${ !canModify || ficha.situacaoFicha.codigoSistema == 'CONSOLIDADA' }">
                         <span><h3>${ficha.situacaoFicha.descricao}</h3></span>
                    </g:if>
                    <g:else>
                        <a class="btn btn-primary" href="javascript:void(0)" data-action="consolidarFicha" data-params="sqFicha:${ficha?.id}">Consolidar</a>
                    </g:else>
                </div>
            </div> --}%
            </div>
        </div> %{-- FIM ENCERRAMENTO FICHA --}%
    </g:if>
    <g:if test="${params?.contexto=='oficina' || params?.contexto == 'validacao' || params?.contexto == 'validador'}">
    <%--Encerramento da Ficha oficina --%>
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-success panel-grupo" id="panel-fim" data-sq-ficha="${ficha.id}">
                    <div class="panel-heading"><a class="cursor-pointer red" style="font-size:1.5rem !important;">Avaliação</a></div>
                    <div class="panel-body" id="panel-fim-body">
                        <g:if test="${ params?.contexto ==~ /oficina/}">
                            <g:render template="/fichaCompleta/formResultadoOficina" model="[oficinaFicha:oficinaFicha,canModify:canModify,dadosUltimaAvaliacao:dadosUltimaAvaliacao, listCategoriaIucn:listCategoriaIucn,flagEditCategoria:flagEditCategoria,listMudancaCategoria:listMudancaCategoria]"></g:render>
                        </g:if>
                        <g:else>
                            <g:render template="/fichaCompleta/formResultadoOficina" model="[oficinaFicha:oficinaFicha,canModify:false,dadosUltimaAvaliacao:dadosUltimaAvaliacao]"></g:render>
                        </g:else>
                    </div>
                </div>
            </div>
        </div> %{-- FIM ENCERRAMENTO FICHA --}%
    </g:if>

    <g:if test="${params?.contexto=='validador'}">
        <%--validação pos-oficina --%>
        <div class="row">
            <div class="col-sm-12">
                <div class="mt10 panel panel-success panel-grupo" data-sq-ficha="${ficha.id}">
                    <div class="panel-heading">
                        <a class="cursor-pointer red"><span style="font-size:1.5rem !important;">Validação</span></a>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-6" id="divValidadoresContainer${ficha.id}">
                                <fieldset class="mt20">
                                    <legend>
                                        Minha validação
                                    </legend>
                                    <div id="divValidadorFicha-${validadorFicha1.id}" data-sq-validador-ficha="${validadorFicha1.id}" data-num="${validadoresPosicoes[validadorFicha1.id.toString()]}">
                                        <g:render template="/fichaCompleta/formRespostaValidador"
                                                  model="[canModify:!fichaVersionada,listCategoriaIucn:listCategoriaIucn
                                                          ,listResultados:listResultados
                                                          ,validadorFicha1:validadorFicha1
                                                          ,sqOficinaFicha:params.sqOficinaFicha]"></g:render>
                                    </div>
                                </fieldset>

                                <g:each var="outroValidador" in="${listOutrosValidadores}" status="index">
%{--                                    exibir as respostas quando ambos tiverem respondido--}%
                                    <g:set var="mostrarRespostas" value="${validadorFicha1?.resultado && outroValidador.resultado}"/>

%{--                                    <g:if test="${ validadorFicha1?.resultado && outroValidador.resultado}">--}%
                                        <fieldset id="fieldsetRespostasOutrosValidadores-${outroValidador.id}" class="mt20 ${ mostrarRespostas ?: 'hide' }">
                                            <g:if test="${contexto == 'validador'}">
                                                <legend>Resposta do outro validador</legend>
                                            </g:if>
                                            <g:else>
                                                <legend>Resposta do ${validadoresPosicoes[outroValidador.id.toString()]}º validador</legend>
                                            </g:else>

                                            <div id="divValidadorFicha-${outroValidador.id}" class="mt10" data-sq-validador-ficha="${outroValidador.id}"
                                                 style="padding-left:5px;background-color:#efefef;"  data-num="${validadoresPosicoes[outroValidador.id.toString()]}">
                                                <p style="line-height:15px"><b><span id="dsResultado${outroValidador.id}">${mostrarRespostas ? outroValidador?.resultado?.descricao : ''}</span></b></p>
                                                <p style="line-height:15px" class="${outroValidador?.categoriaSugerida ?: 'hide'}" id="pCategoriaSugerida-${outroValidador.id}">
                                                    Categoria sugerida: <b> <span id="dsCategoriaSugerida${outroValidador.id}">${ mostrarRespostas ? outroValidador?.categoriaSugerida?.descricaoCompleta :'' }</span></b>
                                                </p>
                                                <div id="divCriterioSugerido-${outroValidador.id}" class="${outroValidador?.deCriterioSugerido?'':'hide'}">
                                                    <p id="pCriterioSugerido-${outroValidador.id}">
                                                        Criterio: <b><span id="deCriterioSugerido${outroValidador.id}">${ mostrarRespostas ? outroValidador?.deCriterioSugerido : '' }</span></b></p>
                                                    <p id="pStPossivelmenteExtinta-${outroValidador.id}" class="${ outroValidador?.stPossivelmenteExtinta=='S' || outroValidador?.stPossivelmenteExtinta=='N'?'' : 'hide'}">Possivelmente extinta:<b><span id="stPossivelmenteExtinda${outroValidador.id}"> ${mostrarRespostas ? ( outroValidador?.stPossivelmenteExtinta == 'S' ? 'SIM' : 'NÃO') :'' }</span></b></p>
                                                </div>

                                                <div id="divFundamentacao-${outroValidador.id}" class="${outroValidador?.txJustificativa ? '':'hide'}">
                                                    <label><b>Fundamentação</b></label>
                                                    <br>
                                                    <div id="txJustificativa${outroValidador.id}" style="overflow-y:auto;max-height: 300px;min-height: 30px;text-align: justify;padding-right: 5px;">
                                                        ${raw( mostrarRespostas ? outroValidador?.txJustificativa : '') }
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                </g:each>
                            </div>
%{--                            se for o validador 3 acessando e ainda não tiver respondido, não mostrar o chat--}%
                            <div class="col-sm-6 ${ validadorNumero > 2 && !validadorFicha1.resultado ? 'hide' :'' }" id="divChatContainer${ficha.id}">
                                <g:render template="/templates/formChat" model="[sqFicha:params.sqFicha
                                                                                 , sqOficinaFicha:params.sqOficinaFicha
                                                                                 , contexto:'validador'
                                                                                 //, xfichaVersionada: fichaVersionada
                                                                                 ,validadorFicha:validadorFicha1
                                                                                 ,classeCss:'mt20']"></g:render>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> %{-- FIM VAILDAÇÃO POS-OFICINA --}%
    </g:if>

    %{-- RESULTADO FINAL  - VALIDAÇÃO  --}%
    <g:if test="${ ! ( params?.contexto ==~ /oficina|consulta|validador/ )}">
        <div class="row">
            <div class="col-sm-12">
                <g:render template="/fichaCompleta/formValidacaoPreview" model="[ficha:ficha,dadosUltimaAvaliacao:dadosUltimaAvaliacao]"></g:render>
            </div>
        </div>
    </g:if>
    <g:else>
        <div class="end-page"></div>
    </g:else>
</div> %{-- FIM CONTAINER--}%
