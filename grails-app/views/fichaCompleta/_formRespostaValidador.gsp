<!-- view: /views/fichaCompleta/_formRespostaValidador.gsp/ -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<form id="frmRespostaValidador${ficha.id}" name="frmRespostaValidador" role="form" class="form-inline">
    <input type="hidden" id="sqFicha" value="${ficha.id}"/>
    <div class="form-group">
        <g:if test="${canModify}">
            <label for="sqResultado" class="control-label">Resposta</label>
            <br>
            <select name="sqResultado" id="sqResultado" data-change="sqResultadoValidadorChange" data-sq-ficha="${ficha.id}" data-contexto="frmRespostaValidador${ficha.id}" class="fld form-control fldSelect w100p">
                <option value="">-- selecione --</option>
                <g:each var="item" in="${listResultados}">
                    <option data-codigo="${item.codigoSistema}"
                      value="${item.id}" ${item.id == validadorFicha1?.resultado?.id ?'selected':''}>${item.descricao}</option>
                </g:each>
            </select>
        </g:if>
        <g:else>
            <div class="input-group w100p">
                <span class="input-group-addon" style="width:150px;">Resposta</span>
                <input type="text" class="form-control" style="width:300px;" readonly value="${validadorFicha1?.resultado?.descricao}">
            </div>
        </g:else>
    </div>

    <div class="form-group hide" id="divCategoria">
        <g:if test="${canModify}">
            <label for="sqCategoriaSugerida" class="control-label">Categoria sugerida</label>
            <br>
            <select name="sqCategoriaSugerida" id="sqCategoriaSugerida" data-change="sqCategoriaSugeridaChange" data-sq-ficha="${ficha.id}" data-contexto="frmRespostaValidador" class="fld form-control fld400 fldSelect">
                <option value="">-- selecione --</option>
                <g:each var="item" in="${listCategoriaIucn}">
                    <option data-codigo="${item.codigoSistema}" value="${item.id}" ${item.id == validadorFicha1?.categoriaSugerida?.id ?'selected':''} >${item.descricaoCompleta}</option>
                </g:each>
                %{--<g:if test="${validadorFicha1?.stPossivelmenteExtinta=='S' || validadorFicha1?.stPossivelmenteExtinta=='N'}">--}%
                    %{--<label class="control-labellabel-checkbox" style="margin-top:0px;">Possivelmente Extinta?--}%
                        %{--&nbsp;<input class="checkbox-lg" disabled type="checkbox" ${validadorFicha1?.stPossivelmenteExtinta=='S' ? ' checked':''}/>--}%
                    %{--</label>--}%
                %{--</g:if>--}%
            </select>
            <label for="stPossivelmenteExtinta" class="control-label cursor-pointer label-checkbox blue hide" style="margin-top:0px;">Possivelmente extinta?
            &nbsp;<input name="stPossivelmenteExtinta" id="stPossivelmenteExtinta" value="S" class="checkbox-lg" type="checkbox" ${validadorFicha1?.stPossivelmenteExtinta=='S' ? ' checked':''}/>
            </label>

        </g:if>
        <g:else>
            <div class="input-group w100p">
                <span class="input-group-addon" style="width:150px;">Categoria sugerida</span>
                <input type="text" class="form-control" style="width:300px;" readonly value="${validadorFicha1?.categoriaSugerida?.descricaoCompleta}">
            </div>
        </g:else>
    </div>
    <div class="form-group hide"  id="divCriterio">
        <g:if test="${canModify}">
            <label for="deCriterioSugerido" class="control-label">Critério sugerido</label>
            <br>
            <input name="deCriterioSugerido" id="deCriterioSugerido" onkeyup="fldCriterioAvaliacaoKeyUp(this);" readonly type="text" class="fld form-control fld400 boldAzul" value="${validadorFicha1?.deCriterioSugerido}"/>
            <button class="fld btn btn-default btn-sm" title="Clique para Selecionar o Critério" data-action="openModalSelCriterioAvaliacao" data-field="deCriterioSugerido" data-contexto="frmRespostaValidador${ficha.id}" data-sq-ficha="${ficha.id}" data-field-categoria="sqCategoriaSugerida" id="btnSelCriterioAvaliacao${ficha.id}">...</button>
        </g:if>
        <g:else>
            <div class="input-group w100p">
                <span class="input-group-addon" style="width:150px;">Critério Sugerido</span>
                <input type="text" class="form-control" style="width:300px !important;" readonly value="${validdorFicha1?.deCriterioSugerido}">
                <g:if test="${validadorFicha1?.stPossivelmenteExtinta=='S' || validadorFicha1?.stPossivelmenteExtinta=='N'}">
                    <label class="control-labellabel-checkbox" style="margin-top:0px;">Possivelmente extinta?
                        &nbsp;<input class="checkbox-lg" disabled type="checkbox" ${validadorFicha1?.stPossivelmenteExtinta=='S' ? ' checked':''}/>
                    </label>
                </g:if>
            </div>
        </g:else>
    </div>
    <div class="form-group w100p" id="divJustificativa">
        <g:if test="${canModify}">
            <label for="dsJustificativa${ficha.id}" class="control-label">Fundamentação</label>
            <br>
            <textarea name="dsJustificativaValidador" id="dsJustificativaValidador${ficha.id}" rows="10" class="fld form-control fldTextarea wysiwyg w100p">${raw(validadorFicha1?.txJustificativa?:'')}</textarea>
        </g:if>
        <g:else>
        %{--<div class="text-justify" style="max-height: 300px;overflow-y: auto;padding:3px;border:1px solid silver;">${raw(ficha.dsJustificativa)}</div>--}%
            <div class="input-group w100p">
                <span class="input-group-addon" style="width:150px;">Fundamentação</span>
                <div class="text-justify" style="max-height:200px;overflow-y: auto;padding:3px;border:1px solid silver;">${raw( validadorFicha1?.txJustificativa) }</div>
            </div>
        </g:else>
    </div>

    <g:if test="${canModify}">
        <div class="fld panel-footer">
            <button data-action="saveResultadoValidador" data-sq-ficha="${ficha.id}" data-sq-oficina-ficha="${sqOficinaFicha}" data-contexto="frmRespostaValidador${ficha.id}" class="fld btn btn-success">Gravar</button>
            %{--<button data-contexto="${contexto}" data-sq-ficha="${ficha.id}" data-action="showFormPendencia" data-callback="updateGridPendencia" class="fld btn btn-danger pull-right" >Registrar pendência</button>--}%
            <button onClick="$('#pnlShowFichaCompleta_${ficha.id} #btnCadPendencia').click()" class="fld btn btn-danger pull-right" >Registrar pendência</button>

        </div>
    </g:if>
</form>
