<!-- view: /views/fichaCompleta/_divGridAcoesConservacao.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<g:if test="${listFichaAcaoConservacao}">
    <table class="table table-sm table-hover table-striped table-condensed table-bordered">
        <thead>
            <th>Ação</th>
            <th>Situação</th>
            <th>Referência Bibliográfica</th>
        </thead>
        <tbody>
        <g:if test="${listFichaAcaoConservacao}">
                <g:each in="${listFichaAcaoConservacao}" var="item" status="i">
                    <tr id="gdFichaRow_${i + 1}">
                        <td>${item?.acaoConservacao?.descricaoCompleta}</td>
                        <td>${item?.situacaoAcaoConservacao?.descricao}</td>
                        <td>${raw(item?.refBibHtml)}</td>
                    </tr>
                </g:each>
            </g:if>
            <g:else>
                <tr>
                    <td colspan="3" align="center">
                        <h4>Nenhum registro encontrado!</h4>
                    </td>
                </tr>
            </g:else>
        </tbody>
    </table>
</g:if>
