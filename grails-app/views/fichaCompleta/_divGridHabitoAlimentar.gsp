<!-- view: /views/fichaCompleta/_divGridHabitoAlimentar.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<g:if test="${listFichaHabitoAlimentar}">
    <center>
        <table class="table table-stripped table-condensed" style="max-width:75%;">
            <thead>
            <tr>
                <th style="width:150px;">Tipo</th>
                <th>Referência Bibliográfica</th>
            </tr>
            </thead>
            <tbody>
            <g:each var="item" in="${listFichaHabitoAlimentar}" status="i">
                <tr>
                    <td>${item?.tipoHabitoAlimentar?.descricao}</td>
                    <td>${raw(item.refBibHtml)}</td>
                </tr>
            </g:each>
            </tbody>
        </table>
    </center>
</g:if>
<fieldset class="mt10">
    <legend style="font-size:0.9em;">
        <div class="div-question">Hábito Alimentar Especialista?</div>&nbsp;<div class="div-answer">${ficha?.snd(ficha.stHabitoAlimentEspecialista)}</div>
    </legend>
    <g:if test="${listFichaHabitoAlimentarEsp}">
        <center>
            <table class="table table-condensed table-sm table-hover table-bordered table-striped" style="max-width:75%">
                <thead>
                <tr>
                    <th style="width:150px;white-space: nowrap">Taxon</th>
                    <th>Categoria</th>
                </tr>
                </thead>
                <tbody>
                <g:each var="item" in="${listFichaHabitoAlimentarEsp}" status="i">
                    <tr>
                        <td>${raw(item?.taxon?.noCientificoCompletoItalico)}</td>
                        <td>${item.categoriaText}</td>
                    </tr>
                </g:each>
                </tbody>
            </table>
        </center>
    </g:if>
</fieldset>