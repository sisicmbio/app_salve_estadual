<div>
    <fieldset disabled="true"}>
        <legend class='border-red'>
            <b style="color:#5A78A9;font-size:15px;"> c) Resultado validação</b>
        </legend>
        <div class="form-inline" role="form">
            <g:if test="${ contexto == 'validacao'}">
                %{-- CATEGORIA DA VALIDACAO --}%
                <div class="form-group">
                    <label class="control-label">Categoria</label>
                    <br>
                    <input type="text" readonly class="fld form-control fld300" value="${oficinaFicha?.categoriaIucn?.descricaoCompleta}">
                </div>

                <g:if test="${ oficinaFicha.dsCriterioAvalIucn}">
                    %{-- CRITERIO FINAL DA VALIDACAO - FUNDAMENTAÇAO--}%
                    <div class="form-group" style="">
                        <label class="control-label">Critério</label>
                        <br>
                        <input type="text" readonly class="fld form-control fld300" value="${oficinaFicha?.dsCriterioAvalIucn?:''}">
                    </div>
                </g:if>
                <g:if test="${ oficinaFicha.categoriaIucn && oficinaFicha.categoriaIucn.codigoSistema=='CR'}">
                %{-- POSSIVELMENTE EXTINTA DA VALIDACAO--}%
                    <div class="form-group" style="">
                        <label class="control-label cursor-pointer label-checkbox" style="">Possivelmente Extinta?&nbsp;</label>
                        <br>
                        <input type="text" value="${ficha.snd(oficinaFicha.stPossivelmenteExtinta)}" class="fld form-control fld200" readonly>
                    </div>
                </g:if>
                <br>
                 <div class="form-group w100p">
                     <label class="control-label">Justificativa</label>
                     <br>
                     <div style="border:1px solid silver;min-height: 60px;background-color: lightgoldenrodyellow">${raw(oficinaFicha.dsJustificativa?:'')}</div>
                 </div>
            </g:if>
            <g:else>
            %{-- CATEGORIA FINAL - FUNDAMENTACAO --}%
                <div class="form-group">
                    <label class="control-label">Categoria</label>
                    <br>
                    <input type="text" readonly class="fld form-control fld300" value="${ficha?.categoriaFinal?.descricaoCompleta}">
                </div>
                <g:if test="${ ficha.categoriaFinal && ficha.categoriaFinal.codigoSistema==~/CR|VU|EN/}">
                    %{--CRITERIO FINAL - FUNDAMENTAÇAO--}%
                    <div class="form-group" style="">
                        <label class="control-label">Critério</label>
                        <br>
                        <input type="text" readonly class="fld form-control fld300" value="${ficha.dsCriterioAvalIucnFinal?:''}">
                    </div>
                    <g:if test="${ ficha.categoriaFinal && ficha.categoriaFinal.codigoSistema=='CR'}">
                        %{-- POSSIVELMENTE EXTINTA --}%
                        <div class="form-group" style="">
                            <label class="control-label cursor-pointer label-checkbox" style="">Possivelmente Extinta?&nbsp;</label>
                            <br>
                            <input type="text" value="${ficha.snd(ficha.stPossivelmenteExtintaFinal)}" class="fld form-control fld200" readonly>
                        </div>
                    </g:if>
                    <br>
                    <div class="form-group w100p">
                        <label class="control-label">Justificativa Final</label>
                        <br>
                        <div style="border:1px solid silver;min-height: 60px;background-color: lightgoldenrodyellow">${raw(ficha.dsJustificativaFinal?:'')}</div>
                    </div>
                    </g:if>
            </g:else>
        </div>

        %{-- MOTIVO DA MUDANCA --}%
        <g:if test="${ ficha.categoriaFinal && dadosUltimaAvaliacao.sqCategoriaIucn && dadosUltimaAvaliacao.sqCategoriaIucn.toLong() != ficha.categoriaFinal.id.toLong() }">
            <div id="divContainerMotivoMudancaFinal${ficha.id}" class="panel panel-success mt10">
                <div class="panel-heading">${raw(dadosUltimaAvaliacao.coCategoriaIucn ? 'Mudança da categoria <span class="cursor-pointer" title="'+dadosUltimaAvaliacao.deCategoriaIucn+'">' + dadosUltimaAvaliacao.coCategoriaIucn+'</span>':'Mudança de categoria')}</div>
                <div class="panel-body" style="background-color:#8fbc8f;" id="divDadosMotivoMudanca${ficha.id}">
                    %{-- CRIAR O GRIDE SEM A ACAO EXCLUIR--}%
                    <input type="hidden" id="canModify" value="false">
                    %{-- GRIDE MOTIVO MUDANCA--}%
                    <div id="divGridMotivoMudancaCategoria" class="mt10"></div>
                    %{-- JUSTIFICATIVA MOTIVO MUDANCA--}%
                    <div class="form-group w100p">
                        <label class="control-label">Justificativa para a mudança de categoria</label>
                        <br>
                        <div class="text-justify" style="min-height: 28px;max-height: 300px;overflow-y: auto;padding:3px;border:1px solid silver;background-color: #eeeeee">${raw(ficha.dsJustMudancaCategoria)}</div>
                    </div>
                </div>
            </div> %{--<FIM MOTIVO DA MUDANCA--}%
        </g:if>
    </fieldset>
</div>
<div class="end-page"></div>
