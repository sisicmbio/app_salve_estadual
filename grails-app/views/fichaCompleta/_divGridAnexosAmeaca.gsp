<!-- view: /views/fichaCompleta/_divGridAnexosAmeaca.gsp -->
<!-- controller:${params?.controller} -->
<!-- action: ${params?.action} -->
<table class="table table-striped table-bordered table-hover" style="max-width: 75%;">
    <thead>
    <tr>
        <th>Data</th>
        <th>Legenda</th>
        <th>Arquivo</th>
        <th>Baixar</th>
    </tr>
    </thead>
    <tbody>
    <g:each var="item" in="${ listAnexos}">
        <tr>
            <td style="width:100px;">${item?.dtAnexo?.format('dd/MM/yyyy')}</td>
            <td class="text-left" style="width:auto">${item.deLegenda}</td>
            <td style="width:300px;">${item.noArquivo}</td>
            <td class="text-center" style="width:80px;">
                <g:if test="${item.deTipoConteudo =~ /image/}">
                   <img src="/salve-estadual/ficha/getAnexo/${item.id}/thumb" data-id="${item.id}" data-file-name="${item.noArquivo}" alt="${item.noArquivo}" class="img-rounded img-preview-grid"/>
                </g:if>
                <g:else>
                    <a class="fld btn btn-default btn-xs btn-download" target="_blank" title="Baixar arquivo" href="/salve-estadual/ficha/getAnexo/${item.id}">
                        <i class="blue fa fa-download"></i>
                    </a>
                </g:else>
            </td>
        </tr>
    </g:each>
    </tbody>
</table>
