<!-- view: /views/fichaCompleta/_divGridUso.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<g:if test="${listFichaUso}">
    <table class="table table-condensed table-sm table-hover table-bordered table-striped">
        <thead>
        <tr>
            <th style="width:400px;">Uso</th>
            <th>Referência Bibliográfica</th>
        </tr>
        </thead>
        <tbody>
        <g:each var="item" in="${listFichaUso}" status="i">
            <tr>
                <td>${item?.uso?.descricao}</td>
                <td>${raw(item.refBibHtml)}</td>
                <g:if test="${item.regioesText}">
                    <tr><td colspan="2" style="padding-left:35px;">${raw( item.regioesHtml )}</td></tr>
                </g:if>
            </tr>
        </g:each>
        </tbody>
    </table>
</g:if>