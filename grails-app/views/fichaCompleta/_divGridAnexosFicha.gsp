<!-- view: /views/fichaCompleta/_divGridAnexosFicha -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>Anexo</th>
        <th>Data</th>
        <th>Legenda / Observação</th>
        <th>Arquivo</th>
        <th>Ação</th>
    </tr>
    </thead>
    <tbody>
    <g:each var="item" in="${listFichaAnexo}">
        <tr>
            <td class="text-center" style="width:90px;">
                <g:if test="${item.deTipoConteudo=='image/tiff'}">
                    &nbsp;
                </g:if>
                <g:elseif test="${item.noArquivo.indexOf('.zip') > -1 }">
                    <img src="${appUrl}assets/shapefile32x37.png" data-id="${item.id}" data-file-name="${item.noArquivo}" alt="${item.noArquivo}"
                         class="img-rounded img-preview-grid"/>
                </g:elseif>
                <g:else>
                    <img src="${appUrl}ficha/getAnexo/${item.id}/thumb" data-id="${item.id}" data-file-name="${item.noArquivo}" alt="${item.noArquivo}" class="img-rounded img-preview-grid"/>
                </g:else>
            </td>
            %{-- DATA --}%
            <td style="width:80px;" class="text-center">${item?.dtAnexo ? item.dtAnexo.format('dd/MM/yyyy') : ''}</td>

            %{-- LEGENDA --}%
            <td style="width:auto;">
                ${item.deLegenda}
            </td>

            <td style="width:200px;">${item.noArquivo}</td>

            <td style="width:30px;" class="td-actions">
                <a class="fld btn btn-default btn-xs btn-download" target="_blank" title="Baixar imagem!" href="/salve-estadual/ficha/getAnexo/${item.id}">
                    <span class="glyphicon glyphicon-download"></span>
                </a>
            </td>
        </tr>
    </g:each>
    </tbody>
</table>
