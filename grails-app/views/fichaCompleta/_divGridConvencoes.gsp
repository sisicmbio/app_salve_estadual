<!-- view: /views/fichaCompleta/_divGridConvencoes.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<g:if test="${listFichaListaConvencao}">
    <center>
        <fieldset style="max-width:75%;" id="fieldset-presenca-convencao-${sqFicha}">
            <label><b>Presença em Convenção</b></label>
            <table class="table table-sm table-hover table-striped table-condensed table-bordered">
            <thead>
                <th>Convenção</th>
                <th>Ano</th>
            </thead>
            <tbody>
            <g:if test="${listFichaListaConvencao}">
                    <g:each in="${listFichaListaConvencao}" var="item" status="i">
                        <tr id="gdFichaRow_${i + 1}">
                            <td>${item?.listaConvencao?.descricao }</td>
                            <td>${item?.nuAno?.toString() }</td>
                        </tr>
                    </g:each>
                </g:if>
                <g:else>
                    <tr>
                        <td colspan="2" align="center">
                            <h4>Nenhum registro encontrado!</h4>
                        </td>
                    </tr>
                </g:else>
            </tbody>
            </table>
         </fieldset>
    </center>
</g:if>
