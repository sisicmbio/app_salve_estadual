<style>
.mce-ico
{
   font-family: 'tinymce',"Times New Roman" !important;
}
</style>
<div id="div-pendencia-wrapper" class="col-sm-12">
   <form id="frmPendencia" name="frmPendencia" rule="form" class="form-inline">
      <input type="hidden" name="sqFichaPendencia" id="sqFichaPendencia" value="${fichaPendencia.id}">
      <input type="hidden" name="contexto" id="contexto" value="${params.contexto?:''}">
      <input type="hidden" name="frmPendenciaChanged" id="frmPendenciaChanged" value="N">
      %{-- para adicionar a pendencia em lote nas fichas selecionadas --}%
      <input type="hidden" name="sqFichas" id="sqFichas" value="${params?.sqFichas}">
      <div class="form-group">
         <label for="deAssunto" class="control-label">Assunto</label>
         <br>
         <input type="text" id="deAssunto" list="listAssuntoPendencia" name="deAssunto" class="form-control ignoreChange fld300" maxlength="100" value="${fichaPendencia?.deAssunto}"/>
         <datalist id="listAssuntoPendencia">
            <option value="Ameaça">
            <option value="Avaliação">
            <option value="Citação">
            <option value="Classificação Taxonômica">
            <option value="Conservação">
            <option value="Distribuição Geográfica">
            <option value="História Natural">
            <option value="Pesquisa">
            <option value="População">
            <option value="Revisão">
            <option value="Referência Bibliográfica">
            <option value="Uso">
            <option value="Validação">

         </datalist>
      </div>
       <g:if test="${ ! params.sqFichas }">
          <div class="form-group">
             <label for="stPendente" class="control-label">Pendente?</label>
             <br>
             <select id="stPendente" name="stPendente" class="form-control ignoreChange fld200 fldSelect" required="true">
                <option value="S" ${fichaPendencia.stPendente=='S'?'selected':''} >Sim</option>
                <option value="N"  ${fichaPendencia.stPendente=='N'?'selected':''}>Não</option>
             </select>
          </div>
          <br>
       </g:if>
       <g:else>
           <input type="hidden" id="stPendente" name="stPendente" value="S">
       </g:else>
      <div class="form-group" style="width: 100%">
         <label for="txPendencia" class="control-label">Descrição da Pendência</label>
         <br>
         <textarea name="txPendencia" id="txPendencia" class="form-control ignoreChange fldTextarea wysiwyg" rows="7" style="width: 100%" required="true">${fichaPendencia.txPendencia}</textarea>
      </div>
      <div class="form-group">
         <label for="noPessoa" class="control-label">Cadastrado por</label>
         <br>
         <input type="text" id="noPessoa" disabled="true" class="form-control ignoreChange fld300" value="${session?.sicae?.user.noUsuario}"/>
      </div>
      <div class="panel-footer mt20">
         <button id="btnSaveFrmPendencia" data-action="savePendencia" data-contexto="${params?.contexto}" data-sq-ficha="${params.sqFicha}" class="btn btn-success">${fichaPendencia.id ?"Gravar Alteração":"Gravar Pendência"}</button>
      </div>
   </form>
</div>
