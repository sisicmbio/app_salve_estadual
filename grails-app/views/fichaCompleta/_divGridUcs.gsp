<!-- view: /views/fichaCompleta/_divGridUcs.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<g:if test="${listFichaOcorrencia}">
    <table class="table table-sm table-hover table-striped table-condensed table-bordered">
        <thead>
            <th>Uc</th>
            <th>Referência Bibliográfica</th>
        </thead>
        <tbody>
        <g:if test="${listFichaOcorrencia}">
                <g:each in="${listFichaOcorrencia}" var="item" status="i">
                    <tr id="gdFichaRow_${i + 1}">
                        <td>${item.fichaOcorrencia?.ucHtml}</td>
                        <td>${raw(item.fichaOcorrencia?.refBibHtml)}</td>
                    </tr>
                </g:each>
            </g:if>
            <g:else>
                <tr>
                    <td colspan="2" align="center">
                        <h4>Nenhum registro encontrado!</h4>
                    </td>
                </tr>
            </g:else>
        </tbody>
    </table>
</g:if>
