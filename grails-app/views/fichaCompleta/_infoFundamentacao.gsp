<!-- view: /views/fichaCompleta/_infoFundamentacao.gsp/ -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

 <div class="col-sm-6" id="divInfoValidadoresContainer" style="min-height:500px;border: 1px solid silver;">
    <g:if test="${ listValidadorFicha.size() == 0 }">
        <div class="alert alert-info">Nenhum validador convidado</div>
    </g:if>

    <g:else>
        <g:each var="item" in="${ listValidadorFicha }" status="rowNum">
            <fieldset>
                <legend data-toggle="collapse" data-target="#divInfoValidadorFicha-${item.id}">
                    <span class="cursor-pointer blue" title="${ session.sicae.user.isADM() || session.sicae.user.isPF() ? item.cicloAvaliacaoValidador.validador.noPessoa : ''}">Validador ${rowNum+1}</span>
                </legend>
                <div id="divInfoValidadorFicha-${item.id}"
                        class="form form-inline collapse in"
                        data-sq-validador-ficha="${item.id}" data-num="${rowNum+1}">
                    <div class="fld form-group">
                        <label class="control-label">Resultado</label>
                        <br>
                        <span class="bold">${ item.resultado?.descricao ?: 'Ainda não informado'}</span>
                    </div>
                    <g:if test="${ item.categoriaSugerida }">
                        <br>
                        <div class="fld form-group">
                            <label class="control-label">Categoria sugerida</label>
                            <br>
                            <span class="bold">${item.categoriaSugerida?.descricaoCompleta ?: ''}"</span>
                            <g:if test="${item.categoriaSugerida?.codigoSistema=='CR' && item.stPossivelmenteExtinta=='S'}">
                                <span class="ml10 blue">(Possivemente extinta)</span>
                            </g:if>
                        </div>
                    </g:if>

                    <g:if test="${ item.deCriterioSugerido }">
                        <br>
                        <div class="fld form-group">
                            <label class="control-label">Critério sugerido</label>
                            <br>
                            <span class="bold">${item.deCriterioSugerido}"</span>
                        </div>
                    </g:if>

                    <g:if test="${ item.txJustificativa }">
                        <br>
                        <div class="fld form-group w100p">
                            <label class="control-label">Fundamentação</label>
                            <br>
                            <div class="bold text-justify" style="line-height:18px;width:100%;padding:3px; min-height:50px; max-height:200px;overflow-y: auto;border:1px solid #eeeeee">
                                ${raw(item.txJustificativa)}
                            </div>
                        </div>
                    </g:if>
                </div>
            </fieldset>
        </g:each>
    </g:else>
</div>

<div class="col-sm-6" id="divInfoChatContainer" style="max-height:500px;overflow:auto;border: 1px solid silver;">
    <div class="chat" style="height:auto;">
        <g:if test="${listChats.size() > 0 }">
            <g:each var="item" in="${listChats}">
                <p class="${ sqUsuarioLogado.toLong()==item.pessoa.id.toLong() ? 'chat-right':'chat-left'}">
                    <g:if test="${nomesValidadores[item.pessoa.id.toString()]}">
                        <b>${ nomesValidadores[item.pessoa.id.toString()]?.numero + ' validador' } - ${item.dtMensagem.format('dd/MM/yy HH:mm:ss')}</b>
                    </g:if>
                    <g:else>
                        <b>${ br.gov.icmbio.Util.nomeAbreviado( item.pessoa.noPessoa ) } - ${item.dtMensagem.format('dd/MM/yy HH:mm:ss')}</b>
                    </g:else>
                    <br>${ item.deMensagem }
                </p>
            </g:each>
        </g:if>
        <g:else>
                <div class="alert alert-info">Não houve bate-papo</div>
        </g:else>
    </div>
</div>
