<!-- view: /views/gerenciarConsulta/_divGridRefBibs -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<table class="table table-sm table-hover table-striped table-condensed table-bordered">
    <tbody>
    <g:if test="${listFichaRefBib || listFichaRefBibPortalbio }">
        <g:if test="listFichaRefBib">
            <g:each in="${listFichaRefBib}" var="item" status="i">
                <tr id="gdFichaRow_${i + 1}">
                    <td class="td-ref-bib">${raw( item ) }</td>
                </tr>
            </g:each>
        </g:if>
        <g:if test="listFichaRefBibPortalbio">
            <tr>
                <td class="td-title">Referências dos registros</td>
            </tr>
            <g:each in="${listFichaRefBibPortalbio}" var="item" status="i">
                <tr id="gdFichaRow_${i + 1}">
                    <td class="td-ref-bib">${raw( item ) }</td>
                </tr>
            </g:each>
        </g:if>
    </g:if>
    <g:else>
        <tr>
            <td align="center">
                <h4>Nenhum registro encontrado!</h4>
            </td>
        </tr>
    </g:else>
    </tbody>
</table>
