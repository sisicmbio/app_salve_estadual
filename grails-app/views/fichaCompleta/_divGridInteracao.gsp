<!-- view: /views/fichaCompletaa/_divGridInteracao.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<table class="table table-condensed table-sm table-hover table-bordered table-striped">
    <thead>
    <tr>
        <th style="width:200px;">Tipo</th>
        <th>Taxon</th>
        <th>Categoria</th>
        <th>Referência Bibliográfica</th>
    </tr>
    </thead>
    <tbody>
    <g:each var="item" in="${listFichaIteracao}" status="i">
        <tr>
            <td>${item?.tipoInteracao?.descricao}</td>
            <td>${raw( item?.taxon?.noCientificoItalico )}</td>
            <td>${item?.categoriaIucn?.descricaoCompleta}</td>
            <td>${raw(item.refBibHtml)}</td>
        </tr>
    </g:each>
    </tbody>
</table>