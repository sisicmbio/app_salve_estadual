<!-- view: /views/fichaCompleta/_divGridChats.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<g:if test="${listChats.size() > 0 }">
	<g:set var="controle" value="0"></g:set>
	<g:each var="item" in="${ listChats }">
        <g:set var="nomeChat" value="Mensagem"></g:set>
		<p class="chat ${ usuario.id.toInteger()==item.pessoa.id.toInteger() ? 'chat-right':'chat-left'}">
			<g:if test="${session.sicae.user.isADM()|| session.sicae.user.isPF() }">
				%{--<b>${item.pessoa.noPessoa} - ${item.dtMensagem.format('dd/MM/yy HH:mm:ss')}</b>--}%
                <g:if test="${usuario.id.toInteger()==item.pessoa.id.toInteger()}">
                    <g:set var="nomeChat" value="Você"></g:set>
                </g:if>
                <g:else>
                    <g:if test="${nomesValidadores && nomesValidadores[item.pessoa.id.toString()]}">
                        <g:set var="nomeChat" value="${nomesValidadores[ item.pessoa.id.toString() ].numero+' validador'}"></g:set>
                    </g:if>
                    <g:else>
                        <g:set var="nomeChat" value="${br.gov.icmbio.Util.nomeAbreviado( item.pessoa.noPessoa)}"></g:set>
                    </g:else>

                </g:else>
			</g:if>

			<g:else>
				<g:if test="${session.sicae.user.isPF()}">

                    %{--<g:if test="${usuario.id.toInteger()==item.pessoa.id.toInteger()}">
                        <g:set var="nomeChat" value="Você"></g:set>
                    </g:if>
                    <g:else>
                        <g:if test="${item.sgPerfil=='VL'}">
                            <g:set var="nomeChat" value="Validador"></g:set>
                        </g:if>
                        <g:elseif test="${item.sgPerfil=='AD'}">
                            <g:set var="nomeChat" value="Administrador"></g:set>
                        </g:elseif>
                        <g:elseif test="${item.sgPerfil=='PF'}">
                            <g:set var="nomeChat" value="Ponto Focal"></g:set>
                        </g:elseif>
                    </g:else>
                    --}%
					%{--<b>Ponto Focal - ${item.dtMensagem.format('dd/MM/yy HH:mm:ss') }</b>--}%
				</g:if>
				<g:elseif test="${session.sicae.user.isVL()}">
                    <g:if test="${usuario.id.toInteger()==item.pessoa.id.toInteger()}">
                        <g:set var="nomeChat" value="Você"></g:set>
                    </g:if>
                    <g:else>
                        <g:if test="${item.sgPerfil=='VL'}">
                            <g:set var="nomeChat" value="Validador"></g:set>
                        </g:if>
                        <g:elseif test="${item.sgPerfil=='AD'}">
                            <g:set var="nomeChat" value="Administrador"></g:set>
                        </g:elseif>
                        <g:elseif test="${item.sgPerfil=='PF'}">
                            <g:set var="nomeChat" value="Ponto Focal"></g:set>
                        </g:elseif>
                    </g:else>
					%{--<b>Validador - ${item.dtMensagem.format('dd/MM/yy HH:mm:ss') }</b>--}%
				</g:elseif>
				<g:else>
					%{--<b>Mensagem - ${item.dtMensagem.format('dd/MM/yy HH:mm:ss')}</b>--}%
                    <g:if test="${usuario.id.toInteger()==item.pessoa.id.toInteger()}">
                        <g:set var="nomeChat" value="Você"></g:set>
                    </g:if>

                </g:else>
			</g:else>
            <b>${nomeChat} - ${item.dtMensagem.format('dd/MM/yy HH:mm:ss')}</b>
			%{-- exibir botão excluir --}%
			%{--<g:if test="${item.pessoa.id.toInteger() == usuario.id.toInteger() }">--}%
				%{--<a class="pull-right" href="javascript:void(0);" onClick="excluirChat( ${vwFicha.id},${item.id} )"  title="Remover mensagem"><span class="glyphicon glyphicon-remove red"></span></a></b>--}%
			%{--</g:if>--}%
		    <br>${item.deMensagem}
		</p>
		<g:set var="controle" value="${item.pessoa.id.toString()}"></g:set>
	</g:each>
</g:if>
