<!-- view: /views/fichaCompleta/_divGridAmeaca.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<g:if test="${listFichaAmeaca}">
    <table class="table table-condensed table-sm table-hover table-bordered table-striped">
        <thead>
        <tr>
            <th style="width:400px;">Ameaça</th>
            <th>Referência Bibliográfica</th>
        </tr>
        </thead>
        <tbody>
        <g:each var="item" in="${listFichaAmeaca}" status="i">
            <tr>
                <td>${item?.criterioAmeacaIucn?.descricaoHieraquia}</td>
                <td>${raw(item.refBibHtml)}</td>
                <g:if test="${item.regioesText}">
                    <tr><td colspan="2" style="padding-left:35px;">${raw( item.regioesHtml )}</td></tr>
                </g:if>
            </tr>
        </g:each>
        </tbody>
    </table>
</g:if>