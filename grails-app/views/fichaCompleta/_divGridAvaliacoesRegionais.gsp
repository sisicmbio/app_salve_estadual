<!-- view: /views/fichaCompleta/_divGridAvaliacoesRegionais.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<table class="mt10 table table-condensed table-sm table-hover table-bordered table-striped">
    <thead>
    <tr>
        <th style="width:50px">#</th>
        <th style="width:200px;">Região</th>
        <th style="width:200px;">Categoria</th>
        <th style="width:200px;">Critério</th>
        %{--<th style="width:200px;">Categoria<br>regional</th>--}%
        <th style="width:auto">Justificativa</th>
        <g:if test="${canModify}">
            <th>Ação</th>
        </g:if>
    </tr>
    </thead>
    <tbody>
    <g:if test="${listFichaAvaliacaoRegional}">
        <g:each var="item" in="${listFichaAvaliacaoRegional}" status="i">
            <tr>
                <td class="text-center">${i+1}</td>
                <td class="text-center">${ item?.abrangencia?.descricao ?: '(outra)' }</td>
                <td class="text-center">${ raw(item?.categoriaIucn?.descricaoCompleta + (item.stPossivelmenteExtinta=='S' ? '<br><span class="blue">(PEX)</span>' :''))}</td>
                <td class="text-center">${ item.deCriterioAvaliacaoIucn}</td>
                %{--<td>${ item.deCriterioAvaliacaoRegional}</td>--}%
                %{--<td class="text-justify">${raw(item.txJustificativaAvaliacao)}</td>--}%
                <td class="text-justify">
                    <g:renderLongText table="ficha_avaliacao_regional" id="${item.id}"
                                      column="tx_justificativa_avaliacao"
                                      text="${ raw(item.txJustificativaAvaliacao) }"/>
                </td>
                <g:if test="${ canModify }">
                    <td class="td-actions">
                        <a data-action="editAvaliacaoRegional"
                           data-sq-ficha="${params.sqFicha}"
                           data-container="frmAvaliacaoRegional"
                           data-id="${item.id}"
                           data-all="false"
                           class="btn fld btn-default btn-xs btn-update"
                           title="Alterar">
                           <span class="glyphicon glyphicon-pencil"></span>
                        </a>
                        <a data-id="${item.id}"
                             data-sq-ficha="${params.sqFicha}"
                             data-container="frmAvaliacaoRegional"
                             data-descricao="${item?.abrangencia?.descricao}"
                             data-action="deleteAvaliacaoRegional" class="btn fld btn-default btn-xs btn-delete" title="Excluir">
                            <span class="glyphicon glyphicon-remove"></span>
                        </a>
                    </td>
                </g:if>
            </tr>
        </g:each>
    </g:if>
    </tbody>
</table>

