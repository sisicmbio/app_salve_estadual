<%@ page import="br.gov.icmbio.Util" %>
<!-- view: /views/fichaCompleta/_formResultadoOficina/ -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
%{-- FORMULARIOS DA FICHA REFERENTES A APLICACAO DOS CRITERIOS --}%

<g:set var="canChangeCategoria" value="${ canModify && ! ( ficha.stManterLc )}"></g:set>
<g:set var="oSND"  value="${[ [key:'S',value:'Sim'], [key:'N', value:'Não'] ,[key:'D', value:'Desconhecido'] ]}"/>
<div>
    <fieldset class="mt20">
        <legend style="margin-bottom:0px;">
            <b style="color:#5A78A9;font-size:15px;">a) Aplicação dos critérios</b>
        </legend>
        <div style="margin-top:5px;border-radius:7px;border:1px solid #9B9595;padding:5px;height:auto;min-height:55px;overflow-y:auto;">
            <ul class="nav nav-tabs" id="ulTabsFichaCompletaAvaliacao">
                <li id="liTabFichaCompletaValidacao1"><a data-action="app.tabClick" data-url="fichaAvaliacao/tabAvaDistribuicao" data-sq-ficha="${ficha.id}" data-container="tab1"        data-can-modify="${canModify}" data-contexto="${params.contexto}" data-toggle="tab" href="#tab1">Distribuição e taxonomia</a></li>
                <li id="liTabFichaCompletaValidacao2"><a data-action="app.tabClick" data-url="fichaAvaliacao/tabAvaPopulacao" data-sq-ficha="${ficha.id}" data-container="tab2"           data-can-modify="${canModify}" data-contexto="${params.contexto}" data-toggle="tab" href="#tab2" data-on-load="avaliacao.tabAvaPopulacaoInit">População</a></li>
                <li id="liTabFichaCompletaValidacao3"><a data-action="app.tabClick" data-url="fichaAvaliacao/tabAvaAnaliseQuantitativa" data-sq-ficha="${ficha.id}" data-container="tab3" data-can-modify="${canModify}" data-contexto="${params.contexto}" data-toggle="tab" href="#tab3">Ameaças e análise quantitativa</a></li>
                %{--<li id="liTabFichaCompletaValidacao4"><a data-action="app.tabClick" data-url="fichaAvaliacao/tabAvaAjusteRegional" data-sq-ficha="${ficha.id}" data-container="tab4"      data-can-modify="${canModify}" data-contexto="${params.contexto}" data-toggle="tab" href="#tab4">Ajuste regional</a></li>--}%
            </ul>
            <div id="tabsAvaliacaoFichaCompletaContent" class="tab-content">
                     <div role="tabpanel" class="tab-pane tab-pane-ficha-completa" id="tab1" style="background-color:#f5f5dc;">
                     </div>
                     <div role="tabpanel" class="tab-pane tab-pane-ficha-completa" id="tab2" style="background-color:#f5f5dc;">
                     </div>
                     <div role="tabpanel" class="tab-pane tab-pane-ficha-completa" id="tab3" style="background-color:#f5f5dc;">
                     </div>
            </div>
        </div>
    </fieldset>

    <br/>

    <fieldset>
        <legend style="margin-bottom:0px;">
           <b style="color:#5A78A9;font-size:15px;"> b) Resultado Avaliação</b>
        </legend>
        <g:if test="${ ficha?.stTransferida }">
            <div class="alert alert-success" role="alert"><h4>Ficha <b>transferida</b></h4></div>
        </g:if>
        <g:elseif test="${ oficinaFicha?.situacaoFicha.codigoSistema == 'EXCLUIDA'}">
            <div class="alert alert-info" role="alert"><h4>Ficha <b>EXCLUÍDA</b> da oficina.</h4></div>
        </g:elseif>
        <g:else>
        <div id="divResultadoOficina" role="form" class="form-inline">
            <input type="hidden" id="sqFicha" value="${ficha.id}"/>
            <input type="hidden" id="sqOficinaFicha" value="${oficinaFicha?.id}"/>
            <input type="hidden" id="canModify" value="${ ( canModify?.toString() == 'false' ? 'false' : 'true') }"/>

            %{--armazenar dados da ultima avaliação--}%
            <input type="hidden" id="sqCategoriaIucnUltimaAvaliacao" name="sqCategoriaIucnUltimaAvaliacao" value="${dadosUltimaAvaliacao.sqCategoriaIucn}"/>
            <input type="hidden" id="cdCategoriaIucnUltimaAvaliacao" name="cdCategoriaIucnUltimaAvaliacao" value="${dadosUltimaAvaliacao.coCategoriaIucn}"/>

            %{-- campos serão atualizados pela modal de avaliação expedita --}%
            <input type="hidden" id="stFavorecidoConversaoHabitats"  name="stFavorecidoConversaoHabitats" value="${ficha.stFavorecidoConversaoHabitats}">
            <input type="hidden" id="stTemRegistroAreasAmplas"       name="stTemRegistroAreasAmplas"      value="${ficha.stTemRegistroAreasAmplas}">
            <input type="hidden" id="stPossuiAmplaDistGeografica"    name="stPossuiAmplaDistGeografica"   value="${ficha.stPossuiAmplaDistGeografica}">
            <input type="hidden" id="stFrequenteInventarioEoo"       name="stFrequenteInventarioEoo"      value="${ficha.stFrequenteInventarioEoo}">

            <g:if test="${ canChangeCategoria && dadosUltimaAvaliacao.sqCategoriaIucn && flagEditCategoria && canModify}">
                <button id="btnRepetirUltimaAvaliacao"
                        class="fld btn btn-primary btn-sm mt10"
                        type="button"
                        data-contexto="${params.contexto}"
                        data-container="divResultadoOficina"
                        data-sq-ficha="${ficha.id}"
                        data-action="app.repetirUltimaAvaliacao">Repetir última avaliação</button>
                <br/>
            </g:if>

            %{--CATEGORIA --}%
            <div class="form-group mb5">
                <label for="sqCategoriaIucn" class="control-label">Categoria</label>
                <br>
                <select id="sqCategoriaIucn" name="sqCategoriaIucn"
                        data-sq-ficha="${ficha.id}"
                        data-change="app.categoriaIucnChange"
                        data-container="divResultadoOficina"
                        ${ canModify && canChangeCategoria ? '' : ' disabled="true"'}
                        class="fld form-control fld400 fldSelect">
                    <option value="">-- selecione --</option>
                    <g:each var="item" in="${listCategoriaIucn}">
                        %{--<option data-codigo="${item.codigoSistema}" value="${item.id}" ${item?.id== oficinaFicha?.categoriaIucn?.id ?'selected':''}>${item.descricaoCompleta}</option>--}%
                        <g:if test="${ ficha.stManterLc == true && ( !ficha.categoriaIucn || ficha.categoriaIucn.codigoSistema == 'NE') }">
                            <option data-codigo="${item.codigoSistema}" value="${item.id}" ${item?.codigoSistema== 'LC' ?'selected':''}>${item.descricaoCompleta}</option>
                        </g:if>
                        <g:else>
                            <option data-codigo="${item.codigoSistema}" value="${item.id}" ${item?.id== ficha?.categoriaIucn?.id ?'selected':''}>${item.descricaoCompleta}</option>
                        </g:else>
                    </g:each>
                </select>
            </div>

            %{--CRITERIO--}%
            <div class="form-group mb5">
                <label for="dsCriterioAvalIucn" class="control-label">Critério</label>
                <br>
                <input id="dsCriterioAvalIucn" name="dsCriterioAvalIucn"
                       class="fld form-control fld300 boldAzul"
                       type="text"
                       onkeyup="fldCriterioAvaliacaoKeyUp(this);"
                       readonly="true"
                       value="${ficha.dsCriterioAvalIucn}"/>
                <g:if test="${ canModify && canChangeCategoria }">
                    <button id="btnSelCriterioAvaliacao"
                            class="fld btn btn-default btn-sm"
                            type="button" title="Clique para selecionar o critério"
                            data-action="openModalSelCriterioAvaliacao"
                            data-field="dsCriterioAvalIucn"
                            data-container="divResultadoOficina"
                            data-contexto="divResultadoOficina"
                            data-sq-ficha="${ficha.id}"
                            data-sq-oficina-ficha="${oficinaFicha.id}"
                            data-field-categoria="sqCategoriaIucn">...</button>
                </g:if>
            </div>

            %{--POSSIVELMENTE EXTINTA--}%
            <div class="form-group mt10">
                    <label for="stPossivelmenteExtinta" class="control-label cursor-pointer label-checkbox" style="margin-top:17px;">Possivelmente Extinta?&nbsp;
                    <input name="stPossivelmenteExtinta" id="stPossivelmenteExtinta"
                           class="checkbox-lg"
                           type="checkbox"
                           value="S"
                           ${canModify && canChangeCategoria ? '' :' disabled="disabled"'}
                           ${ficha.stPossivelmenteExtinta=='S' ? ' checked':''}/>
                    </label>
            </div>
            <br>
            %{--JUSTIFICATIVA--}%
            <div class="form-group w100p">
                <g:if test="${canModify}">
                    <label for="dsJustificativa${ficha.id}" class="control-label">Justificativa</label>
                    <br>
                    <textarea name="dsJustificativa" id="dsJustificativa${ficha.id}" rows="5" class="fld form-control fldTextarea wysiwyg w100p texto">${raw( br.gov.icmbio.Util.removeBgWhite( ficha.dsJustificativa ?: oficinaFicha?.dsJustificativa ) )}</textarea>
                    <g:if test="${params.contexto=='oficina'}">
                            <button class="fld btn btn-primary mt5"
                                type="button"
                                id="btnSaveJustificativa-${ficha.id}"
                                data-action="saveJustificativa"
                                data-sq-ficha="${ficha.id}"
                                data-sq-oficina-ficha="${oficinaFicha?.id}"
                                data-container="divResultadoOficina"
                                data-avaliada-revisada="N">Gravar a justificativa
                            </button>
                    </g:if>
                </g:if>
                <g:else>
                    <div class="input-group w100p">
                        <span class="input-group-addon" style="width:200px;">Justificativa</span>
                        <div class="text-justify" style="min-height: 28px;max-height:200px;overflow-y: auto;padding:3px;border:1px solid silver;background-color: lightyellow !important;">${ raw( (  ficha?.categoriaIucn ? br.gov.icmbio.Util.removeBgWhite( ficha.dsJustificativa ) : '' ) ) }</div>
                    </div>
                </g:else>
            </div>

            <br class="fld">

            %{-- MOTIVO DA MUDANCA--}%
            <g:if test="${ ! ficha.categoriaFinal }">
            <div id="divDadosMotivoMudanca${ficha.id}"
                 class="panel panel-success mt10" style="display:none;">
                <div class="panel-heading">${raw(dadosUltimaAvaliacao.coCategoriaIucn ? 'Mudança da categoria do ciclo anterior: <span class="cursor-pointer" title="'+dadosUltimaAvaliacao.deCategoriaIucn+'">' + dadosUltimaAvaliacao.coCategoriaIucn+'</span>':'Mudança de categoria')}</div>
                <div class="panel-body" style="background-color:#8fbc8f;">
                     <g:if test="${canModify}">
                        <div class="form-group">
                            <label for="sqMotivoMudanca" class="control-label">Motivo da mudança</label>
                            <br>
                            <select id="sqMotivoMudanca"
                                    class="fld form-control fld300 fldSelect"
                                ${!canModify?'disabled="true"':''}>
                                <option value="">-- selecione --</option>
                                <g:each var="item" in="${listMudancaCategoria}">
                                    <option value="${item.id}">${item.descricao}</option>
                                </g:each>
                            </select>
                            <button type="button" id="btnSaveMotivoMudanca"
                                    data-action="addMotivoMudanca"
                                    data-sq-ficha="${ficha.id}"
                                    data-container="divResultadoOficina"
                                    data-contexto="divResultadoOficina"
                                    data-params="sqFichaMudancaCategoria,sqMotivoMudanca,txFichaMudancaCategoria,canModify:${canModify}"
                                    class="fld btn btn-sm btn-primary">Adicionar motivo mudança</button>
                        </div>
                    </g:if>
                    <div id="divGridMotivoMudancaCategoria" class="mt10"></div>
                    <div class="form-group w100p">
                        <label for="dsJustMudancaCategoria${ficha.id}" class="control-label">Justificativa para a mudança de categoria</label>
                        <br>
                        <g:if test="${canModify}">
                            <textarea name="dsJustMudancaCategoria" id="dsJustMudancaCategoria${ficha.id}"
                                      class="fld form-control fldTextarea wysiwyg w100p"
                                      rows="5">${ficha.dsJustMudancaCategoria}</textarea>
                        </g:if>
                        <g:else>
                            <div class="text-justify" style="min-height: 28px;max-height: 300px;overflow-y: auto;padding:3px;border:1px solid silver;background-color: #eeeeee">${ raw( br.gov.icmbio.Util.removeBgWhite( ficha?.dsJustMudancaCategoria ) ) }</div>
                        </g:else>
                    </div>
                </div>
            </div> %{--<FIM MOTIVO DA MUDANCA--}%
            </g:if>

            %{--AJUSTE REGIONAL QUANDO A FICHA NÃO FOR ENDEMICA DO BRASIL --}%
            <g:if test="${ ficha.stEndemicaBrasil != 'S' }">
                <div class="panel panel-success mt20" id="divDadosAjusteRegional${ficha.id}">
                    <div class="panel-heading"><a>Ajuste Regional</a></div>
                     <div class="panel-body" style="background-color:#fffec5;">
            %{--                         AJUSTE REGIGIONA PODERA SER ALTERADO A QUALQUER TEMPO--}%
                         <g:if test="${true||canModify}">
                            <div class="form-group">
                                <div class="checkbox fld">
                                    <label title="Clique aqui para preencher as observações campo com o texto padrão.">
                                        <input type="checkbox" data-action="checkRespostaPadraoAjusteRegionalClick"
                                                data-text="Não são conhecidas informações sobre a conectividade da população brasileira com a(s) de outro(s) países. Por isso a categoria indicada na avaliação não foi alterada no ajuste regional."
                                                data-target="dsConectividadePopExterior${ficha.id}"
                                            ${ ('não são conhecidas informações sobre a conectividade da população brasileira com a(s) de outro(s) países. por isso a categoria indicada na avaliação não foi alterada no ajuste regional.' == br.gov.icmbio.Util.stripTags( ficha.dsConectividadePopExterior?.toLowerCase() ).trim()) ? 'checked' : '' }
                                               class="fld checkbox-lg">&nbsp;&nbsp;Preencher o campo "Observações" com o texto padrão
                                    </label>
                                </div>
                            </div>
                            <br class="fld">
                        </g:if>

                        %{--<Ajuste Reginal: TIPO CONECTIVIDADE--}%
                        <div class="form-group">
                            <label for="sqTipoConectividade" class="control-label">Conectividade com populações de outros países</label>
                            <br>
                            <select name="sqTipoConectividade" id="sqTipoConectividade"
                                    class="fld form-control fld450 fldSelect" ${true||canModify?'':'disabled'}>
                                <option value="">-- selecione --</option>
                                <g:each var="item" in="${listConectividade}">
                                    <option data-codigo="${item.codigoSistema}" value="${item.id}" ${(ficha?.tipoConectividade?.id==item.id)?' selected':''}>${item.descricao}</option>
                                </g:each>
                            </select>
                        </div>

                        %{--<Ajuste Reginal: TENDÊNCIA DE IMIGRAÇÃO NO BRASIL --}%
                        <div class="form-group">
                            <label for="sqTendenciaImigracao" class="control-label">Tendência de imigração no Brasil</label>
                            <br>
                            <select name="sqTendenciaImigracao" id="sqTendenciaImigracao"
                                    class="fld form-control fld300 fldSelect" ${true||canModify?'':'disabled'}>
                                <option value="">-- selecione --</option>
                                <g:each var="item" in="${listTendencia}">
                                    <option data-codigo="${item.codigoSistema}" value="${item.id}" ${ficha?.tendenciaImigracao?.id==item.id ? 'selected':''} >${item.descricao}</option>
                                </g:each>
                            </select>
                        </div>

                        %{--<Ajuste Reginal: POPULAÇÃO BRASILEIRA PODE DECLINAR --}%
                        <div class="form-group">
                            <label for="stDeclinioBrPopulExterior" class="control-label">População brasileira pode declinar</label>
                            <br>
                            <select name="stDeclinioBrPopulExterior" id="stDeclinioBrPopulExterior"
                                    class="fld form-control fld300 fldSelect" ${true||canModify?'':'disabled'}>
                                <option value="">-- selecione --</option>
                                <g:each var="item" in="${oSND}">
                                    <option data-codigo="${item.key}" value="${item.key}" ${ficha?.stDeclinioBrPopulExterior==item.key ? ' selected' :'' }>${item.value}</option>
                                </g:each>
                            </select>
                        </div>

                        %{--<Ajuste Reginal: OBSERVAÇÕES --}%
                        <div class="form-group w100p">
                            <label for="" class="control-label">Observações</label>
                            <br>
                            <g:if test="${true||canModify}">
                                <textarea name="dsConectividadePopExterior" id="dsConectividadePopExterior${ficha.id}"
                                          class="fld form-control fldTextarea wysiwyg w100p"
                                          rows="5">${ficha.dsConectividadePopExterior}</textarea>
                            </g:if>
                            <g:else>
                                <div class="text-justify" style="min-height: 28px;max-height:200px;overflow-y: auto;padding:3px;border:1px solid silver;">${raw(ficha.dsConectividadePopExterior)}</div>
                            </g:else>
                        </div>
                        %{--<FIM CAMPOS AJUSTE REGIONAL--}%
                         <div>
                        %{-- Quando não puder mofificar a ficha as informações de ajuste regionao poderão ser atualizados--}%
                         <g:if test="${!canModify}">
                             <div class="fld panel-footer">
                                 <button class="fld btn btn-success"
                                         type="button"
                                         id="btnSaveAjusteRegional-${ficha.id}"
                                         data-action="saveAjusteRegional"
                                         data-sq-ficha="${ficha.id}"
                                         data-sq-oficina-ficha="${oficinaFicha?.id}"
                                         data-container="divDadosAjusteRegional${ficha.id}"
                                         >Gravar ajuste regional</button>
                             </div>
                         </g:if>

                        </div>
                    </div>
                </div>
            </g:if>

            <g:if test="${canModify}">
                <div class="fld panel-footer">
                    <button class="fld btn btn-success"
                            type="button"
                            id="btnSaveResultado-${ficha.id}"
                            data-action="saveResultado"
                            data-sq-ficha="${ficha.id}"
                            data-sq-oficina-ficha="${oficinaFicha?.id}"
                            data-container="divResultadoOficina"
                            data-avaliada-revisada="N">Gravar avaliação</button>
               </div>
            </g:if>
        </div> %{-- FIM CAMPOS RESULTADO OFICINA--}%

        <g:if test="${canModify}">
            <div class="fld panel-footer">
                <button type="button"
                        data-action="saveResultado"
                        data-sq-oficina-ficha="${oficinaFicha?.id}"
                        data-sq-ficha="${ficha.id}"
                        data-container="divResultadoOficina"
                        data-avaliada-revisada="S"
                        class="fld btn btn-primary pull-right"
                        title="Alterar a situação da ficha para Alterada/Revisada">Gravar como "Avaliada revisada"</button>
            </div>
        </g:if>
    </g:else>
    </fieldset> %{-- RESULTADO --}%
</div>
