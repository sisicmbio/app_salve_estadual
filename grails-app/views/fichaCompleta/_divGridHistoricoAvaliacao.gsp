<!-- view: /views/fichaCompleat/_divGridHistoricoAvaliacao.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<g:if test="${listAvaliacoes}">
    <center>
        <fieldset style="max-width:75%;" id="fieldset-historico-avaliacoes-${sqFicha}">
            <label><b>Histórico das Avaliações</b></label>

            <table class="table table-condensed table-sm table-hover table-bordered table-striped">
                <thead>
                <tr>
                    <th style="width:200px;">Tipo</th>
                    <th style="width:80px;">Ano</th>
                    <th>Abrangência</th>
                    <th>Categoria</th>
                    <th>Critério</th>
                    <th>Referância Bibliográfica</th>
                </tr>
                </thead>
                <tbody>
                <g:each var="item" in="${listAvaliacoes}" status="i">
                    <tr>
                        <td>${item?.tipoAvaliacao?.descricao}</td>
                        <td class="text-center">${item?.nuAnoAvaliacao}</td>
                        <td>${ item.noRegiaoOutra ? item.noRegiaoOutra : item?.abrangencia?.descricao }</td>
                        <td>${ item?.categoriaIucn.descricaoCompleta}</td>
                        <td>${item?.deCriterioAvaliacaoIucn}</td>
                        <td>${raw(item.refBibHtml)}</td>
                    </tr>
                </g:each>
                </tbody>
            </table>
        </fieldset>
    </center>
</g:if>
