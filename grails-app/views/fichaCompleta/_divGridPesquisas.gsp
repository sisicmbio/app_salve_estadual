<!-- view: /views/gerenciarConsulta/_divGridPesquisa -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<g:if test="${listFichaPesquisa}">
    <table class="table table-sm table-hover table-striped table-condensed table-bordered">
        <thead>
            <th style="width:200px">Tema</th>
            <th style="width:200px">Situação</th>
            <th>Referência Bibliográfica</th>
        </thead>
        <tbody>
        <g:if test="${listFichaPesquisa}">
                <g:each in="${listFichaPesquisa}" var="item" status="i">
                    <tr id="gdFichaRow_${i + 1}">
                        <td>${item?.tema?.descricao}</td>
                        <td>${item?.situacao.descricao}</td>
                        <td>${raw(item?.refBibHtml)}</td>
                    </tr>
                </g:each>
            </g:if>
            <g:else>
                <tr>
                    <td colspan="3" align="center">
                        <h4>Nenhum registro encontrado!</h4>
                    </td>
                </tr>
            </g:else>
        </tbody>
    </table>
</g:if>
