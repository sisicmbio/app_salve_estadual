<!-- view: /views/fichaCompleta/_divGridOcorrencias -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->
<g:set var="readOnly" value="${params.readOnly=="true"}"></g:set>
<fieldset>
    <legend style="font-size:1.2em;">
        Registro(s)
    </legend>
    <div class="div-pagination form-inline">
        <select class="fldSelect fld170 form-control"
                data-contexto="${contexto}" data-change="updatePaginaGridOcorrencias"
                data-pagination-total-records="${pagination.totalRecords}"
                data-pagination-total-pages="${pagination.totalPages}"
                data-sq-ficha="${ficha?.id}"
                id="selectPaginacaoGrideOcorrencias">
            <g:each var="i" in="${ (1..pagination.totalPages.toInteger()) }">
                <option value="${i}" ${i.toInteger() == pagination.pageNumber.toInteger() ? 'selected':''}>Página ${ i + ' / ' + pagination.totalPages.toString() }</option>
            </g:each>
        </select>
        <br>
        <g:if test="${ params?.contexto =~ /consulta|oficina/ }">
            <div class="form-inline ml15" style="display: inline-block">
                <label class="cursor-pointer" title="Filtrar somente os registro que tiveram alguma colaboração">Com colaboração:
                    <input type="radio" name="radioFiltroGrideOcorrencias"
                           value="CC" ${ ! params?.filtroSelecionado || params?.filtroSelecionado == 'CC' ? 'checked':''}
                           onchange="radioFiltroGrideOcorrenciasClick()"/>
                </label>
                <g:if test="${ params?.contexto =~ /oficina/ }">
                    <label class="ml20 cursor-pointer" title="Filtrar somente os registro com pendência para resolver em oficina">Resolver em oficina:
                        <input type="radio" name="radioFiltroGrideOcorrencias"
                               value="RO" ${ ! params?.filtroSelecionado || params?.filtroSelecionado == 'RO' ? 'checked':''}
                               onchange="radioFiltroGrideOcorrenciasClick()"/>
                    </label>
                </g:if>
                <label class="ml20 cursor-pointer" title="Filtrar todos os registros com ou sem alguma colaboração">Todos:
                    <input type="radio" name="radioFiltroGrideOcorrencias"
                           value="T" ${ params?.filtroSelecionado == 'T' ? 'checked':''}
                           onchange="radioFiltroGrideOcorrenciasClick()"/>
                </label>

            </div>
        </g:if>
    </div>

    <div style="height: auto;min-height:200px; max-height:500px;overflow-y:auto;" id="divContainerOcorrenciasFichaCompleta-${ficha?.id?.toString()}">
        <table class="table table-condensed table-sm table-hover table-bordered table-striped table-ocorrencias" id="tableOcorrenciasFichaCompleta-${ficha?.id?.toString()}">
            <thead>
            <tr>
                <th style="min-width:30px;">#</th>
                <th style="width:140px;">Coordenadas</th>
                <th>Estado</th>
                <th>Município</th>
                <th>Localidade</th>
                <th>Precisão</th>
                <th>Autor/Responsável</th>
                <th>Data do Registro</th>
                <th style="width:auto;">Referência Bibliográfica</th>
                <th>Base de Dados</th>
            %{-- No contexto da Validação ou do Validador externo não pode alterar a situação --}%
                <g:if test="${ contexto != 'publicacao' && contexto != 'validacao' && contexto !='validador' }">
                    <th>Situação</th>
                </g:if>
            </tr>
            </thead>

            <tbody>
            <g:each var="item" in="${listOcorrencias}" status="i">
            %{--            <div style="border:1px solid red;max-height: 100px; overflow-y: auto">${item}</div>--}%
                <g:set var="isPontoDaConsulta" value="${(item.no_base_dados =~ /(?i)consulta/ ) ? true : false }"></g:set>
                <g:if test="${ isPontoDaConsulta || ( item.qtd_sim + item.qtd_nao) > 0  }">
                    <tr data-id="${item.id}" data-id-ocorrencia="${item.id_ocorrencia}"
                    data-contexto="${contexto}"
                    data-fonte="${item.no_fonte}"
                    data-bd="${item.no_fonte.toLowerCase()}"
                %{--                class="revisor ${item.avaliado || ( item.qtd_nao == 0 && !isPontoDaConsulta ) ? 'colaboracao-avaliada' : 'colaboracao-nao-avaliada'}"--}%
                %{--                class="${item.qtd_nao == 0 ? 'colaboracao-avaliada' : 'colaboracao-nao-avaliada'}"--}%
                    >
                </g:if>
                <g:else>
                    <tr data-id="${item.id}" data-id-ocorrencia="${item.id_ocorrencia}"
                    data-contexto="${contexto}"
                    data-fonte="${item.no_fonte}"
                    data-bd="${item.no_fonte.toLowerCase()}">
                </g:else>
                <td class="text-center" id="tdFichaCompletaOcorrencia${item.no_fonte+item.id}">${pagination.rowNum+i}</td>
                <td style="width:200px;">${raw(item.coordenadas)}</td>
                <td class="text-center" style="">${item.no_estado}</td>
                <td class="text-center" style="">${item.no_municipio}</td>
                <td style="">${item.no_localidade}</td>
                <td class="text-center" style="">${item.no_precisao}</td>
                <td style="">${item.no_autor}</td>
                <td class="text-center" style="">${item?.dt_ocorrencia?.replaceAll(/00:00:00/,'')}</td>
                <td style="">${raw(item.refBib)}</td>
                <td class="text-center" style="">${item.no_base_dados}</td>

            %{-- contexto da Validaçao ou Validador nao exibir a coluna SITUACAO--}%
                <g:if test="${ contexto != 'publicacao' && contexto != 'validacao' && contexto != 'validador'}">
                    <td class="text-center" style="width:200px;min-width:200px;">
                        <g:if test="${ isPontoDaConsulta || ( item.qtd_sim + item.qtd_nao > 0) }">
                            <div style="font-size:10px;text-align:left;border:1px solid silver;padding:5px;">

                                <g:if test="${ item.ds_situacao_avaliacao  }">
                                    <p id="pUtilizado${item.id}" class="blue text-center">${item.ds_situacao_avaliacao}</p>
                                </g:if>


                                <g:if test="${ isPontoDaConsulta || item.qtd_nao > 0 }">

                                %{--
                                <div style="border:1px dashed red;max-width:200px;max-height: 100px; overflow-y: auto">
                                    <pre>
                                    ${item}
                                    </pre>
                                </div>
                                --}%
                                    <g:if test="${ isPontoDaConsulta }">
                                    %{--                                  ${ sqFichaVersao+'=' + item.sq_ficha_versao}<br>--}%

                                        <g:if test="${ ( ( sqFichaVersao.toInteger()==0 && !item.sq_ficha_versao ) || (item.sq_ficha_versao?.toInteger() == sqFichaVersao?.toInteger() ) ) }">

                                            <span id="wrapperSugestaoPonto${item.id}"
                                                  class="revisor ${ item.avaliado ? 'colaboracao-avaliada':'colaboracao-nao-avaliada'}"
                                                  data-campo-ficha="mapa-ocorrencias" data-grupo="distribuicao-geografica">
                                                <label style="padding:0px;cursor:pointer;">
                                                    <input name="radioRevisor${item.id}"
                                                        ${ readOnly ? ' disabled ' :''}
                                                           onclick="radioConsolidarPontoConsultaClick(this);"  data-id="${item.id}" data-contexto="${contexto}"
                                                        ${ !item.cd_situacao_sistema|| item.cd_situacao_sistema == 'NAO_AVALIADA' ? 'checked':''}
                                                           data-sq-ficha-ocorrencia-consulta="${item.id}" type="radio" data-value="NAO_AVALIADA"> Não resolvido
                                                </label>
                                                <br>
                                                <label style="padding:0px;cursor:pointer;">
                                                    <input name="radioRevisor${item.id}"
                                                        ${ readOnly ? ' disabled ' :''}
                                                           onclick="radioConsolidarPontoConsultaClick(this);" data-id="${item.id}" data-contexto="${contexto}"
                                                        ${ item.cd_situacao_sistema =="ACEITA" ?'checked':''}
                                                           data-sq-ficha-ocorrencia-consulta="${item.id}" type="radio" data-value="ACEITA"> Resolvido
                                                </label>
                                                <g:if test="${ contexto!='validacao' }">
                                                    <br>
                                                    <label style="padding:0px;cursor:pointer;">
                                                        <input name="radioRevisor${item.id}" ${ item.cd_situacao_sistema == 'RESOLVER_OFICINA' ? 'checked':''}
                                                            ${ readOnly ? ' disabled ' :''}
                                                               onclick="radioConsolidarPontoConsultaClick(this);" data-id="${item.id}" data-contexto="${contexto}"
                                                               data-sq-ficha-ocorrencia-consulta="${item.id}" type="radio" data-value="RESOLVER_OFICINA"> Resolver em oficina
                                                    </label>
                                                </g:if>
                                            </span>
                                        </g:if>
                                    </g:if>

                                    <g:if test="${ contexto != 'validacao' && !readOnly}">

                                        <button type="button" id="btnChangeUtilizadoAvaliacao${ item.id }"
                                            ${ readOnly ? ' disabled ' :''}
                                                data-id="${item.id}"
                                                data-id-ocorrencia="${item.id_ocorrencia}"
                                                data-fonte="${item.no_fonte}"
                                                data-sq-ficha="${ficha.id}"
                                                data-in-utilizado-avaliacao="${item.cd_situacao_avaliacao_sistema=='REGISTRO_UTILIZADO_AVALIACAO' ? 'N' : 'S' }"
                                                data-contexto="${contexto}"
                                                data-action="gerenciarConsulta.btnUtilizarNaoUtilizarClick"
                                                class="btn btn-primary btn-sm center-block mt10"
                                                title="Marcar o ponto como utilizado ou não utilizado na avaliação.">
                                            ${ item.cd_situacao_avaliacao_sistema == 'REGISTRO_UTILIZADO_AVALIACAO' ? 'NÃO utilizar' : 'Utilizar'} na avaliação
                                        </button>
                                    </g:if>
                                </g:if>
                                <g:if test="${item.sugestoes}">

                                %{--<div style="border:1px dashed red;max-width:200px;max-height: 100px; overflow-y: auto">
                                     ${item.sugestoes}
                                </div>--}%

                                    <p style="margin:0!important; margin-top:3px !important;font-weight: bold;">${ (item.qtd_sim + item.qtd_nao) == 1 ? 'Colaboração':'Colaborações'}:</p>
                                    <g:each var="sugestao" in="${item.sugestoes}">
                                        <g:set var="cssClass" value="${ sugestao.cd_situacao_sistema == 'NAO_AVALIADA' ||
                                            (sugestao.cd_situacao_sistema == 'RESOLVER_OFICINA' && contexto == 'oficina') ? 'colaboracao-nao-avaliada':'colaboracao-avaliada'}"></g:set>
                                        <span id="wrapperSugestao${sugestao.sq_ficha_ocorrencia_validacao}" class="revisor ${cssClass}"
                                              data-campo-ficha="mapa-ocorrencias" data-grupo="distribuicao-geografica"
                                              style="padding:0px;white-space: nowrap;" title="${br.gov.icmbio.Util.capitalize(sugestao.no_usuario)}">
                                            <span>${raw( br.gov.icmbio.Util.nomeAbreviado( sugestao.no_usuario).trim() )}</span>
                                            <g:if test="${sugestao.in_valido == 'N' }">
                                                <div style="padding-left: 10px;border-top:1px solid gray;margin-bottom:10px;">
                                                    <span>
                                                        ${raw('<b>'+sugestao.de_valido+'</b> concorda')}&nbsp;<i class="blue cursor-pointer fa fa-commenting-o" title="${sugestao?.tx_observacao?:'Comentário não foi informado'}"></i>
                                                    </span>

                                                    <br>

                                                    <label style="padding:0px;cursor:pointer;">
                                                        <input name="radioRevisor${sugestao.sq_ficha_ocorrencia_validacao}" onclick="radioSugestaoClick(this);"
                                                            ${ readOnly ? ' disabled ' :''}
                                                            ${ sugestao.cd_situacao_sistema == 'NAO_AVALIADA' ? 'checked':''}
                                                               data-contexto="${contexto}"
                                                               data-sq-ficha-ocorrencia-validacao="${sugestao.sq_ficha_ocorrencia_validacao}" type="radio" data-value="NAO_AVALIADA"> Não resolvido
                                                    </label>
                                                    <br>
                                                    <label style="padding:0px;cursor:pointer;">
                                                        <input name="radioRevisor${sugestao.sq_ficha_ocorrencia_validacao}" onclick="radioSugestaoClick(this);"
                                                            ${ readOnly ? ' disabled ' :''}
                                                            ${ sugestao.cd_situacao_sistema == "ACEITA" ? 'checked':''}
                                                               data-contexto="${contexto}"
                                                               data-sq-ficha-ocorrencia-validacao="${sugestao.sq_ficha_ocorrencia_validacao}" type="radio" data-value="ACEITA"> Resolvido
                                                    </label>
                                                    <g:if test="${ contexto!='validacao' }">
                                                        <br>
                                                        <label style="padding:0px;cursor:pointer;">
                                                            <input name="radioRevisor${sugestao.sq_ficha_ocorrencia_validacao}" onclick="radioSugestaoClick(this);"
                                                                ${ readOnly ? ' disabled ' :''}
                                                                ${ sugestao.cd_situacao_sistema == 'RESOLVER_OFICINA' ? 'checked':''}
                                                                   data-contexto="${contexto}"
                                                                   data-sq-ficha-ocorrencia-validacao="${sugestao.sq_ficha_ocorrencia_validacao}" type="radio" data-value="RESOLVER_OFICINA"> Resolver em oficina
                                                        </label>
                                                    </g:if>
                                                </div>
                                            </g:if>
                                            <g:else>
                                                <span>&nbsp;concorda</span>
                                            </g:else>
                                        </span>
                                    </g:each>

                                </g:if>


                            %{--
                                                    <i class="blue fa fa-users"></i>&nbsp;<span id="span_revisor${item.id}"
                                                      class="revisor ${ item.avaliado ? 'colaboracao-avaliada':'colaboracao-nao-avaliada'}"
                                                      data-campo-ficha="mapa-ocorrencias" data-grupo="distribuicao-geografica">Revisor</span><br/>
                                                    <label style="padding:0px;">
                                                       <input name="radioRevisor${item.id}" onclick="radioRevisorClick(this);" data-id="${item.id}"
                                                              ${item.value==""?'checked':''}
                                                              data-sq-ficha-ocorrencia-validacao="" type="radio" data-value="">Nenhum
                                                    </label>
                                                      <g:if test="${ contexto!='validacao' && contexto != 'validacao'}">
                                                        <br/>
                                                        <label style="padding:0px;">
                                                           <input name="radioRevisor${item.id}" ${item.value=='RESOLVER_OFICINA' ? 'checked':''}
                                                                onclick="radioRevisorClick(this);" data-id="${item.id}"
                                                                data-sq-ficha-ocorrencia-validacao="" type="radio" data-value="RESOLVER_OFICINA">Resolver em oficina
                                                        </label>
                                                      </g:if>
                                                    <br>
                                                    <g:each var="sugestao" in="${item.sugestoes}">
                                                       <label style="padding:0px;">
                                                           <input name="radioRevisor${item.id}" onclick="radioRevisorClick(this);" data-id="${item.id}"
                                                               ${ item.value == sugestao.usuario.username ? 'checked':''}
                                                               data-sq-ficha-ocorrencia-validacao="${sugestao.id}" type="radio" data-value="${sugestao.inValido}">
                                                               ${sugestao.usuario.noUsuario+': '+sugestao.inValidoText}
                                                               <g:if test="${sugestao.inValido == 'N' }">
                                                                    &nbsp;<i class="blue cursor-pointer fa fa-info-circle" title="${sugestao.txObservacao}"></i>
                                                               </g:if>
                                                       </label>
                                                       <br>
                                                    </g:each>
                            --}%

                            </div>
                        </g:if>
                        <g:else>
                        %{-- Ocorrências cadastradas diretamente no salve não precisam de validação --}%
                            <g:if test="${item.no_fonte != 'salve'}">
                                <g:if test="${ contexto !='consulta' || item.no_fonte != 'portalbio' }">
                                    <select id="selectColaboracaoOcorrencia${item.id}"
                                        ${ readOnly ? ' disabled ' :''}
                                            class="fldSelect fld170 form-control ${item.avaliado ? 'colaboracao-avaliada' : 'colaboracao-nao-avaliada'}" data-change="updateSituacaoColaboracao" data-campo-ficha="mapa-ocorrencias" data-grupo="distribuicao-geografica">
                                        <g:each var="situacao" in="${listSituacaoColaboracao}">
                                            <option data-codigo="${situacao.codigoSistema}"
                                                    value="${situacao.id}" ${ ( situacao.codigoSistema == item?.cd_situacao_sistema || (!item.cd_situacao_sistema && situacao.codigoSistema == 'NAO_AVALIADA') ) ? ' selected':''}>${situacao.descricao}</option>
                                        </g:each>
                                    </select>
                                    <div id="divWrapperTextoNaoAceita${item.id}" style="display:${item.tx_nao_aceita ? 'block' : 'none'};">
                                        <div id="divTextoNaoAceita${item.id}" class="fldTextoLivre" style="font-size: 12px !important;">${item.tx_nao_aceita}</div>
                                        <button type="button"
                                            ${ readOnly ? ' disabled ' :''}
                                                data-action="editNaoAceita" data-id="${item.id}" data-select-id="selectColaboracaoOcorrencia${item.id}" class="btn btn-xs btn-default tooltipstered" style="cursor: pointer;" title="Editar a justificativa.">Editar</button>
                                    </div>
                                </g:if>
                            </g:if>
                            <g:else>
                                <span class="blue">${item.ds_situacao_avaliacao}</span>
                            </g:else>
                        </g:else>
                    </td>
                </g:if>
                </tr>
            %{--  EXIBIR AS FOTOS ENVIADAS PELOS COLABORADORES DAS CONSULTAS --}%
                <g:if test="${ isPontoDaConsulta && item.multimidias }">
                    <tr data-id-ocorrencia="${item.id_ocorrencia}" data-contexto="${contexto}" data-fonte="${item.no_fonte}" data-bd="${item.no_fonte.toLowerCase()}">
                        <td colspan="11">
                            <div class="div-fotos-consultas" style="background-color: #fff;margin-bottom:20px;">
                                <g:each var="multimidia" in="${ item.multimidias }">
                                    <div class="card text-center" style="padding:3px;width: auto; max-width:300px; border: 1px solid #d2d2e2; display:inline-block;border-radius:10px; background-color: #f0ffff;">
                                        <img class="card-img-top img-rounded img-preview-grid"
                                             src="/salve-estadual/fichaMultimidia/getMultimidia/${multimidia.id}/thumb"
                                             data-id="${multimidia.id}" data-file-name="${multimidia.no_arquivo}"
                                             title="Ver imagem"
                                             data-contexto="multimidia"
                                             alt="${multimidia.no_arquivo}">
                                        <div class="card-body" style="font-size: 12px !important;">
                                            <p style="font-size:inherit !important;max-height: 150px;text-align:justify;overflow: auto;">${ multimidia.tx_nao_aceito }</p>
                                            <ul class="list-group list-group-flush text-left">
                                                <li class="list-group-item" style="padding:0px;">Autor:<b>${multimidia.no_autor }</b> </li>
                                                <li class="list-group-item" style="padding:0px;">Data/hora:<b>${multimidia?.dt_multimidia }</b></li>
                                                <li class="list-group-item" style="padding:0px;">Arquivo:<b>${ multimidia.no_arquivo }</b></li>
                                                <g:if test="${ !readOnly && contexto != 'validador' }">
                                                    <li class="list-group-item text-center" style="padding:2px 2px;">
                                                        <select class="fld form-control fld250 fldSelect ${multimidia.cd_situacao_sistema == 'PENDENTE_APROVACAO' ? 'red':'green'}"
                                                                data-sq-ficha-multimidia="${multimidia.id}"
                                                                data-campo-fichaX="mapa-ocorrencias"
                                                                data-grupo="distribuicao-geografica"
                                                                data-change="updateSituacaoMultimidia">
                                                            <g:each var="situacao" in="${ listSituacaoMultimidia }">
                                                                <option data-codigo="${situacao.codigo}" value="${situacao.id}" ${multimidia.cd_situacao_sistema == situacao.codigo ? 'selected':''}>${situacao.descricao}</option>
                                                            </g:each>
                                                        </select>
                                                    </li>
                                                </g:if>
                                                <li class="list-group-item" style="padding:0px;"><div class="text-justify" style="display:${ multimidia.tx_nao_aceito ? '' : 'none'};border:1px solid #cac6c6;line-height:1;padding:5px;margin:3px;font-size:15px !important;" id="div-multimidia-nao-aceita-${multimidia.id}">${ multimidia.tx_nao_aceito}</div></li>

                                            </ul>
                                        </div>
                                    </div>
                                </g:each>
                            </div>
                        </td>
                    </tr>
                </g:if>
            </g:each>
            </tbody>
        </table>
    </div>
</fieldset>
