<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
</head>
<body>
<g:if test="${msgError}">
    <div class="alert alert-danger">
        <h2>Testes - ${msgError}</h2>
    </div>
</g:if>
<g:else>
    <div id="divTestes" class="mt10 container flex flex-columnr" style="visibility:hidden;border: 1px solid gray; background-color: #fff">
        <div>
            <h2>SALVE - Rotinas de Manutenção</h2>
            <hr>
        </div>
        <div style="font-size:18px !important;">
            <ol>
                <li><a href="/salve-estadual/teste?gerarImagensCorousel=S">Gerar imagens 210x450 para carousel modulo publico</a></li>
                <li><a href="/salve-estadual/teste?limparCampoTexto=S">Limpar tags ms-word de campo texto da ficha</a></li>
                <li><a href="/salve-estadual/teste?updateRefBibPortalbio=S">Atualizar referências das ocorrências utilizadas do PORTALBIO</a></li>
                <li><a href="/salve-estadual/teste?atualizarEstadoBiomaUcOcorrenciasGrupoAvaliado=S">Atualizar Estado, município, bioma, bacias e UCS por grupo avaliado</a></li>
                <li><a href="/salve-estadual/teste?popularRegistro=S&sqFicha=0">Atualizar Estado, município, bioma, bacias e UCS pelo ID da ficha</a></li>
                <li><a href="/salve-estadual/teste?updateMv=S&moduloPublico=S">Atualizar visão materializada Módulo Público</a></li>
                <li><a href="/salve-estadual/teste?updateMv=S&relAnalitico=S">Atualizar visão materializada Relatório Taxon Analítico</a></li>
                <li><a href="/salve-estadual/teste?workers=S">Mostrar workers da sessão</a></li>
                <li><a href="/salve-estadual/teste?testarGeoService=S">Testar services/geoService()</a></li>
                <li><a href="/salve-estadual/teste?limpezaDiaria=S">Executar rotinas de limpeza diária de dados inconsistentes no banco de dados</a></li>
                <li><a href="/salve-estadual/teste?updateAutorias=S">Alimentar a autoria das fichas publicadas que não possuem autoria</a></li>
                <li><a href="/salve-estadual/teste?limparTagWord=S">Remover tags ms-word das justificativas e colocar itálico no nome científico (taxon_historico_avaliacao)</a></li>
                <li><a href="/salve-estadual/teste?ajustarPercentualFichas=S">Ajustar o percentual de preenchimento das fichas de uma unidade alteradas antes ou até a data informada. parametros: data=dd/mm/yyyy&sqUnidade=xxx</a></li>
                <li><a href="/salve-estadual/teste?requestInfo=S">Exibir as informações do request / servidor remoto</a></li>
                <li><a href="/salve-estadual/teste?testarTradutores=S&texto=Nada do que foi será de novo do jeito que já foi um dia.">Testar serviços de tradução on-line</a></li>
                <li><a href="/salve-estadual/teste?enviarEmailCordanacaoPendenciasResolvidas=S">Enviar e-mail para a cordenacao das fichas com pendência de revisâo resolvidas</a></li>
                <li><a href="/salve-estadual/teste?enviarEmailPendenciasRevisao=S">Enviar e-mail para Pontos Focais com as fichas com pendência de revisâo pendente</a></li>
                <li><a href="/salve-estadual/teste?testarApiSpeciesplus=S&noCientifico=Puma concolor">Teste API speciesplus - listas CITES</a></li>
            </ol>
        </div>
    </div>
    <g:javascript>
        ( function start2() {
            if ( typeof($) == 'undefined' || $("#divAppLoading").size() > 0) {
                setTimeout(start2, 500);
                return;
            }
            $("#divTestes").css('visibility','visible');
        })()

    </g:javascript>
 </g:else>
</body>
</html>
