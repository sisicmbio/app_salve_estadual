<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="ajax"/>
        <style>
        .panel-body{
            background-color: #fff;
        }

        </style>
    </head>

    <body>

        <div class="panel panel-default" style="width:800px;position:relative;" id="divPanel">
            <div class="panel-heading" style="padding: 5px;">
                <span class="panel-title">Cadastro de ...</span>
            </div>
            <div class="panel-body" id="divPanelBody">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            </div>
            <div class="panel-footer" style="padding:5px;">
                <a class="btn btn-default btn-group-sm">Gravar</a>
            </div>
        </div>




        <!-- Modal -->
        <div id="myModal" class="modal" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modal Header</h4>
              </div>
              <div class="modal-body">
                <p>Some text in the modal.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>


              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>

          </div>
        </div>
         <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>



<div class="lazy-load">
  //http://localhost:8080/sae/javascript/taxon
  //taxon
</div>

<script type="text/javascript">
alert('Arquivo carregado corretamente');
$(document).ready(function() {
    $('#searchApoio').on('change', function() {
        listParents();
    });
    initPage();
});

function initPage() {
    $('#btnAdd').off('click').on('click', function() {
        add();
    });
    $('#btnSave').off('click').on('click', function() {
        save();
    });
    $('.btn-update').off('click').on('click', function() {
        var id = $(this).data('id');
        edit(id);
    });
    $('.btn-delete').off('click').on('click', function() {
        var id = $(this).data('id');
        app.confirm('Confirma exclusão?', function() {
            deleteApoio(id);
        });
    })
}
/**
 * renderiza o formulário para adicionar registros
 */
function add() {
    app.ajax(app.url+'apoio/add', null, function(res) {
        $('#form').html(res);
        updateSelect(app.url+'apoio/listParentsJSON', 'selectAdd', '-- Nova Tabela -- ', 'id', 'descricao');
        $('#modalAdd').modal({
            backdrop: true
        });
        initPage();
    }, null, 'html', null);
}
/**
 * realiza a busca dos registros 'pai'
 * @return {[type]} [description]
 */
function listParents() {
    var data = $('#frmSearch').serializeArray();
    app.ajax(app.url+'apoio/listParents', data, function(res) {
        $('#grid').html(res);
        //initPage();
    }, null, 'html', null);
}
/**
 * [listChidren description]
 * @param  {[type]} data [description]
 * @return {[type]}      [description]
 */
function edit(id) {
    var data = {
        'id': id
    };
    app.ajax(app.url+'apoio/edit', data, function(res) {
        $('#form').html(res);
        $('#modalAdd').modal({
            backdrop: true
        });
        initPage();
    }, null, 'html', null);
}
/**
 * [deleteApoio description]
 * @param  {[type]} data [description]
 * @return {[type]}      [description]
 */
function deleteApoio(id) {
    var data = {
        'id': id
    };
    app.ajax(app.url+'apoio/delete', data, function(res) {
        app.growl(res['msg'], null, null, null, null, res['type']);
        updateSelect(app.url+'apoio/listParentsJSON', 'searchApoio', 'Selecione ', 'id', 'descricao');
        updateSelect(app.url+'apoio/listParentsJSON', 'searchApoio', 'Selecione ', 'id', 'descricao');
        listParents();
    }, null, 'json');
}
/**
 * [save description]
 * @param  {[type]} data [description]
 * @return {[type]}      [description]
 */
function save(data) {
    validation('formAdd');
    var data = $('#formAdd').serializeArray();
    app.ajax(app.url+'apoio/save', data, function(res) {
        app.growl(res['msg'], null, null, null, null, res['type']);
        if ($('#selectAdd').val() === '') {
            updateSelect(app.url+'apoio/listParentsJSON', 'selectAdd', '-- Nova Tabela -- ', 'id', 'descricao');
            updateSelect(app.url+'apoio/listParentsJSON', 'searchApoio', 'Selecione ', 'id', 'descricao');
        }
        listParents();
    }, 'Gravando...', 'json');
}
/**
 * [updateSelect description]
 * @param  {[type]} url        [description]
 * @param  {[type]} idSelect   [description]
 * @param  {[type]} optDefault [description]
 * @param  {[type]} optValue   [description]
 * @param  {[type]} optText    [description]
 * @return {[type]}            [description]
 */
function updateSelect(url, idSelect, optDefault, optValue, optText) {
    app.ajax(url, null, function(res) {
        $('#' + idSelect).empty();
        $('#' + idSelect).append('<option value="">' + optDefault + '</option>');
        $.each(res, function(key, value) {
            $('#' + idSelect).append('<option value="' + value[optValue] + '">' + value[optText] + '</option>');
        });
    }, null, 'json')
}

function validation(id) {
    var validador = $("#" + id).validate({
        rules: {
            descricao: "required"
        },
        messages: {
            descricao: 'Campo Descrição não pode estar vazio!'
        }
    });
    validador.form();
}
</script>


    </body>
</html>
