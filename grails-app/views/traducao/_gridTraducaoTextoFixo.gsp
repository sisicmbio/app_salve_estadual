<!-- view: /views/traducao/_gridTraducaoTextoFixo.gsp -->
<!-- controller:${params?.controller} -->
<!-- action: ${params?.action} -->
<table id="table${gridId}" class="table table-striped table-sorting">
    <thead>
    %{-- PAGINACAO --}%
    <tr id="tr${gridId}Pagination">
        <td colspan="5">
            <div class="text-left" style="min-height:0;height: auto;overflow: hidden" id="div${(gridId ?: pagination.gridId)}PaginationContent">
                <g:if test="${pagination?.totalRecords}">
                    <g:gridPagination pagination="${pagination}"/>
                </g:if>
            </div>
        </td>
    </tr>

    %{-- FILTROS --}%
    <tr id="tr${gridId}Filters" class="table-filters" data-grid-id="${gridId}">
        <td>
            <div style="display:flex;flex-direction:row; justify-content:space-between;">
                <i class="fa fa-question-circle"
                   title="Utilize os campos ao lado para filtragem dos registros.<br> Informe o valor a ser localizado e pressione <span class='blue'>Enter</span>.<br>Para remover o filtro limpe o campo e pressione <span class='blue'>Enter</span> novamente."></i>
                <i data-grid-id="${gridId}" class="fa fa-filter" title="Aplicar filtro(s)"></i>
                <i data-grid-id="${gridId}" class="fa fa-eraser" title="Limpar filtro(s)"></i>
            </div>
        </td>

        <td title="Informe a palavra para pesquisar">
            <input type="text" name="txOriginalFiltro${gridId}" value="${params?.txOriginalFiltro ?: ''}" class="form-control table-filter"
                   placeholder="" data-field="txOriginal">
        </td>

        <td title="Informe a tradução para pesquisar">
            <input type="text" name="txTraduzidoFiltro${gridId}" value="${params?.txTraduzidoFiltro ?: ''}" class="form-control table-filter"
                   placeholder="" data-field="txTraduzido">
        </td>

        %{-- CORRIGIDO--}%
        <td>
            <input type="text" name="txRevisadoFiltro${gridId}" value="${params?.txRevisadoFiltro ?: ''}" class="form-control table-filter"
                   placeholder="" data-field="txRevisado">
        </td>

        %{-- acao --}%
        <td></td>
    </tr>

    %{-- TITULOS --}%
    <tr id="tr${gridId}Columns" data-grid-id="${gridId}">
        <th style="width:60px;">#</th>
        <th style="width:200px;" class="sorting sorting_asc" data-field-name="tx_original">Texto</th>
        <th style="width:200px;" class="sorting" data-field-name="tx_traduzido">Tradução automática</th>
        <th style="width:200px;" class="sorting" data-field-name="tx_revisado">Tradução revisada</th>
        <th style="width:50px;">Ação</th>
    </tr>
    </thead>

    <tbody>

    <g:if test="${rows}">
        ${rows}
        <g:each var="row" in="${rows}" status="i">
            <tr id="tr-${row?.sq_traducao_texto}">
                %{-- COLUNA NUMERAÇÃO --}%
                <td class="text-center"
                    id="td-${row.sq_traducao_texto}">
                    ${i + (pagination ? pagination?.rowNum?.toInteger() : 1)}
                </td>
                %{-- TEXTO ORIGINAL --}%
                <td>${row.tx_original}</td>
                %{-- TEXTO TRADUZIDO --}%
                <td>${row.tx_traduzido}</td>
                %{-- TEXTO CORRIGIDO --}%
                <td>${row.tx_revisado}</td>
                %{-- AÇÃO --}%
                <td class="td-actions">
                    <a data-action="traducaoTextoFixo.edit" data-sq-traducao-texto="${row.sq_traducao_texto}" class="btn btn-default btn-xs btn-update" title="Alterar">
                        <span class="glyphicon glyphicon-pencil"></span>
                    </a>
                    <a data-action="traducaoTextoFixo.delete" data-descricao="${row.tx_original}" data-sq-traducao-texto="${row.sq_traducao_texto}" class="btn btn-default btn-xs btn-delete" title="Excluir">
                        <span class="glyphicon glyphicon-remove"></span>
                    </a>
            </td>
            </tr>
        </g:each>
    </g:if>
    <g:else>
        <tr>
            <td colspan="4">Nenhuma palavra encontrada</td>
        </tr>
    </g:else>
    </tbody>
</table>
