<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="ajax"/>
        <g:javascript>
            $(document).ready(function() {
                $( ".dropdown-menu" )
                .mouseenter(function() {
                    $(this).parent('li').addClass('active');
                })
                .mouseleave(function() {
                    $(this).parent('li').removeClass('active');
                });

            });
        </g:javascript>
        <style>
            .dropdown-submenu > a:after {
                display:block;content:" ";float:right;width:0;height:0;border-color:transparent;border-style:solid;border-width:5px 0 5px 5px;border-left-color:#cccccc;margin-top:5px;margin-right:-10px;
            }
            .dropdown-submenu {
              position: relative;
            }
            .dropdown-submenu > .dropdown-menu {
              top: 0;
              left: 100%;
              margin-top: -6px;
              margin-left: -1px;
            }
            .dropdown-submenu:hover > .dropdown-menu {
              display: block;
            }
            .dropdown-submenu:hover > a:after {
              border-left-color: #fff;
            }
            .dropdown-submenu.pull-left {
              float: none;
            }
            .dropdown-submenu.pull-left > .dropdown-menu {
              left: -100%;
              margin-left: 10px;
            }
        </style>
    </head>
    <body>
       <div class="navbar navbar-default navbar-fixed-top" style="margin-top:68px;z-index:1 !important;" id="div-main-menu-container">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                    %{-- <div class="navbar-brand" id="divBrand"><img id="imgBrand" src="${assetPath(src:'spinner.gif')}"/></div> --}%
                    <div class="navbar-brand" id="imgBrand"><i class="glyphicon glyphicon-refresh spinning"/>&nbsp;</div>
                </div>
                <div class="navbar-collapse collapse navbar-responsive-collapse">
                    <ul class="nav navbar-nav">

                    <g:if test="${ session.sicae.user.isADM() }">
                        <li class="dropdown">
                            <a href="javascript:void(0)" data-target="#" class="dropdown-toggle" data-toggle="dropdown">
                                Administração<b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="dropdown">
                                    <a href="javascript:void(0)" data-target=".navbar-collapse" onclick="app.loadModule('cicloAvaliacao')">Ciclo de Avaliação</a>
                                </li>
                                 <li class="dropdown">
                                    <a href="javascript:void(0)" data-target=".navbar-collapse" onclick="app.loadModule('usuarioWeb')">Usuários da Consulta Externa</a>
                                </li>
                                <li class="dropdown">
                                    <a href="javascript:void(0)" data-target=".navbar-collapse" onclick="modal.exportOccurrencesDwca()">Exportar Ocorrências p/ PortalBio</a>
                                </li>
                                %{-- <li class="dropdown">
                                    <a href="javascript:void(0)" data-target=".navbar-collapse" onclick="modal.cadRefBib()">Referência Bibliográfica</a>
                                </li> --}%
                                <li class="dropdown dropdown-submenu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Tabelas auxiliares</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="javascript:void(0)" data-target=".navbar-collapse" onclick="app.loadModule('dadosApoio')">Apoio</a></li>
                                        <li><a href="javascript:void(0)" data-target=".navbar-collapse" onclick="app.loadModule('uso')">Uso</a></li>
                                        <li><a href="javascript:void(0)" data-target=".navbar-collapse" title="Manutenção do cadastro de Instituições" onclick="modal.cadInstituicao()">Instituições</a></li>
                                        %{-- <li><a href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse" onclick="app.loadModule('criterioAmeacaIucn')">Ameaças</a></li> --}%
                                    </ul>
                                </li>

                                <li class="dropdown">
                                    %{--<a href="javascript:void(0)" data-target=".navbar-collapse" onclick="modal.uploadManual()">Atualizar Manual</a>--}%
                                    <a href="javascript:void(0)" data-target=".navbar-collapse"
                                       data-file-name="manual_salve.pdf"
                                       data-title="Atualizar Manual do Sistema"
                                       data-extension="pdf"
                                       data-max-size="10000"
                                       onclick="modal.uploadArquivoSistema(this)">Atualizar manual do sistema
                                    </a>
                                    <a href="javascript:void(0)" data-target=".navbar-collapse"
                                       data-file-name="planilha_modelo_importacao_registros_salve.zip"
                                       data-title="Atualizar Planilha Importação Registros"
                                       data-extension="zip"
                                       data-max-size="4000"
                                       onclick="modal.uploadArquivoSistema(this)">Atualizar planilha importação de registros
                                    </a>
                                    <a href="javascript:void(0)" data-target=".navbar-collapse"
                                       data-file-name="planilha_modelo_importacao_fichas_salve.zip"
                                       data-title="Atualizar Planilha Importação Fichas"
                                       data-extension="zip"
                                       data-max-size="4000"
                                       onclick="modal.uploadArquivoSistema(this)">Atualizar planilha importação de fichas

                                    </a><a href="javascript:void(0)" data-target=".navbar-collapse"
                                       data-file-name="planilha_modelo_importacao_ref_bib.zip"
                                       data-title="Atualizar Planilha Importação Ref. Bibliográficas"
                                       data-extension="zip"
                                       data-max-size="40000"
                                       onclick="modal.uploadArquivoSistema(this)">Atualizar planilha importação de ref. bibliográficas
                                    </a>

                                    <a href="javascript:void(0)" data-target=".navbar-collapse"
                                       data-file-name="planilha_importacao_taxon.csv"
                                       data-title="Enviar Planilha Táxons (CSV)"
                                       data-extension="csv"
                                       data-max-size="10000"
                                       onclick="modal.uploadArquivoSistema(this)">Enviar Planilha Táxons (csv)
                                    </a>

                                </li>

                                </ul>
                            </li>
                        </g:if>

                        %{-- Fim menu Administração --}%

                        <g:if test="${ session.sicae.user.menuFicha() }">
                            <li class="dropdown">
                                <a href="javascript:void(0)" Xdata-toggle="collapse"
                                   data-target=".navbar-collapse"
                                   onclick="app.loadModule('ficha')">Ficha</a>
                            </li>
                        </g:if>

                        %{--Menu Gerenciar--}%
                        <g:if test="${session.sicae.user.menuGerenciar()}">
                            <li class="dropdown">
                                <a href="javascript:void(0)" data-target="#" class="dropdown-toggle" data-toggle="dropdown">
                                    Gerenciar<b class="caret"></b>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="dropdown dropdown-submenu"></li>
                                    <g:if test="${session.sicae.user.menuFuncao()}">
                                        <li><a href="javascript:void(0)" data-target=".navbar-collapse" onclick="app.loadModule('gerenciarPapel');return false;">Função</a></li>
                                    </g:if>
                                    <g:if test="${session.sicae.user.menuConsulta()}">
                                        <li><a href="javascript:void(0)" data-target=".navbar-collapse" onclick="app.loadModule('gerenciarConsulta');return false;">Consultas / Revisão </a></li>
                                    </g:if>
                                    <g:if test="${session.sicae.user.menuOficina()}">
                                        <li><a href="javascript:void(0)" data-target=".navbar-collapse" onclick="app.loadModule('oficina');return false;">Oficinas</a></li>
                                    </g:if>
                                    <g:if test="${session.sicae.user.menuValidacao()}">
                                        <li><a href="javascript:void(0)" data-target=".navbar-collapse" onclick="app.loadModule('gerenciarValidacao');return false;">Validação</a></li>
                                    </g:if>
                                    <g:if test="${ session.sicae.user.menuPublicacao()}">
                                        <li><a href="javascript:void(0)" data-target=".navbar-collapse" onclick="app.loadModule('publicacao');return false;">Publicação</a></li>
                                    </g:if>


                                </ul>
                            </li>
                        </g:if>


                    <g:if test="${ session.sicae.user.menuRefBib() }">
                        <li class="dropdown">
                            <a href="javascript:void(0)" data-target=".navbar-collapse" onclick="modal.cadRefBib()">Referência bibliográfica</a>
                        </li>
                    </g:if>

                    %{--
                    <g:if test="${ session.sicae.user.menuTaxonomia() }">
                        <li class="dropdown">
                            <a href="javascript:void(0)" data-target="#" class="dropdown-toggle" data-toggle="dropdown">
                                Taxonomia<b class="caret"></b>
                            </a>
                             <ul class="dropdown-menu">
                                 <li><a href="javascript:void(0)" data-target=".navbar-collapse" onclick="null;app.loadModule('taxonomia/cadastro')">Alterar/incluir</a></li>
                                 <li><a href="javascript:void(0)" data-target=".navbar-collapse" onclick="null;app.loadModule('taxonomia/sinonimia')">Sinonimias</a></li>
                             </ul>
                        </li>
                    </g:if>
                    --}%
                    <g:if test="${ session.sicae.user.menuRelatorio() }">
                        <li class="dropdown">
                            <a href="javascript:void(0)" data-target="#" class="dropdown-toggle" data-toggle="dropdown">
                                Relatórios<b class="caret"></b>
                            </a>
                             <ul class="dropdown-menu">
                                 %{--<li><a href="javascript:void(0)" data-target=".navbar-collapse" onclick="null;app.relatorio('PF_CT',260)">Pontos focais/Coordenadores de táxon</a></li>--}%
                                 <li><a href="javascript:void(0)" data-target=".navbar-collapse" onclick="null;app.loadModule('relatorio/rel001')">Função</a></li>
                                 <li><a href="javascript:void(0)" data-target=".navbar-collapse" onclick="null;app.loadModule('relatorio/rel002')">Oficinas</a></li>
                                 <li><a href="javascript:void(0)" data-target=".navbar-collapse" onclick="null;app.loadModule('relatorio/rel003')">Táxons</a></li>
                                 %{--<li><a href="javascript:void(0)" data-target=".navbar-collapse" onclick="null;app.loadModule('relatorio/rel003')">Históricos</a></li>--}%
                             </ul>
                        </li>
                    </g:if>

                    %{--SAIR--}%
                    <li class="dropdown visible-xs-block">
                        <a href="javascript:void(0)" data-target=".navbar-collapse" onclick="app.logout()">Sair</a>
                    </li>

                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown" id="li-menu-workers">
                            <a href="javascript:void(0)" title="Atividade(s) em endamento."><span class="badge"></span></a>
                        </li>
                        %{-- <li class="dropdown visible-lg-block">
                            <a href="javascript:void(0)" Xdata-toggle="collapse" data-target=".navbar-collapse" onclick="app.clearCache()" class="green" title="Atualizar memória tempoŕaria com dados atualizados.">Limpar Cache</a>
                        </li> --}%
                    </ul>
                </div>
            </div>
        </div> %{-- fim menu principal --}%
    </body>
</html>
