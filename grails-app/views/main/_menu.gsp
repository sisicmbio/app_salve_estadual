<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="ajax"/>
        <g:javascript>
            $(document).ready(function() {
                $( ".dropdown-menu" )
                .mouseenter(function() {
                    $(this).parent('li').addClass('active');
                })
                .mouseleave(function() {
                    $(this).parent('li').removeClass('active');
                });

            });
        </g:javascript>
        <style>
            .dropdown-submenu > a:after {
                display:block;content:" ";float:right;width:0;height:0;border-color:transparent;border-style:solid;border-width:5px 0 5px 5px;border-left-color:#cccccc;margin-top:5px;margin-right:-10px;
            }
            .dropdown-submenu {
              position: relative;
            }
            .dropdown-submenu > .dropdown-menu {
              top: 0;
              left: 100%;
              margin-top: -6px;
              margin-left: -1px;
            }
            .dropdown-submenu:hover > .dropdown-menu {
              display: block;
            }
            .dropdown-submenu:hover > a:after {
              border-left-color: #fff;
            }
            .dropdown-submenu.pull-left {
              float: none;
            }
            .dropdown-submenu.pull-left > .dropdown-menu {
              left: -100%;
              margin-left: 10px;
            }
        </style>
    </head>
    <body>
       <div class="navbar navbar-default navbar-fixed-top" style="margin-top:68px;z-index:1 !important;" id="div-main-menu-container">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                    %{-- <div class="navbar-brand" id="divBrand"><img id="imgBrand" src="${assetPath(src:'spinner.gif')}"/></div> --}%
                    <div class="navbar-brand float-left" id="imgBrand"><i id="appSpinner" style="display:none;" class="glyphicon glyphicon-refresh spinning"/></div>
                </div>
                <div class="navbar-collapse collapse navbar-responsive-collapse">
                    <ul class="nav navbar-nav">

                    <g:if test="${ session.sicae.user.isADM() }">
                        <li class="dropdown">
                            <a href="javascript:void(0)" data-target="#" class="dropdown-toggle" data-toggle="dropdown">
                                Administração<b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="dropdown">
%{--                                    <a href="javascript:void(0)" data-target=".navbar-collapse" onclick="app.loadModule('cicloAvaliacao')">Ciclo de Avaliação</a>--}%
                                    <a href="configuracao/" data-target=".navbar-collapse">Configurações iniciais</a>
                                </li>
                                %{--<li class="dropdown">
                                    <a href="javascript:void(0)" data-target=".navbar-collapse" onclick="modal.exportOccurrencesDwca()">Exportar Ocorrências p/ PortalBio</a>
                                </li>--}%
                                %{-- <li class="dropdown">
                                    <a href="javascript:void(0)" data-target=".navbar-collapse" onclick="modal.cadRefBib()">Referência Bibliográfica</a>
                                </li> --}%
                                <li class="dropdown dropdown-submenu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Tabelas auxiliares</a>
                                    <ul class="dropdown-menu">

                                        %{--   TABELAS CORPORATIVAS --}%
                                        <li class="dropdown dropdown-submenu">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Corporativas</a>
                                            <ul class="dropdown-menu">

                                                <li><a href="javascript:void(0)" data-target=".navbar-collapse" onclick="app.loadModule('corpUsuario',{title:'Usuários'})">Usuários</a></li>
                                                <li role="separator" class="divider"></li>
                                                    <li><a href="javascript:void(0)" data-target=".navbar-collapse" onclick="app.loadModule('corpEstruturaOrg',{title:'Manutenção das instituições / Unidades'})">Instituição / Unidade</a></li>
                                                    <li><a href="javascript:void(0)" data-target=".navbar-collapse" onclick="app.loadModule('corpUc',{title:'Unidades de Conservação'})">Unidades de Conservação</a></li>
                                                <li role="separator" class="divider"></li>
                                                <li><a href="javascript:void(0)" data-target=".navbar-collapse" onclick="app.loadModule('corpPais',{title:'Países'})">Países</a></li>
                                                <li><a href="javascript:void(0)" data-target=".navbar-collapse" onclick="app.loadModule('corpRegiao',{title:'Divisões Regionais'})">Regiões</a></li>
                                                <li><a href="javascript:void(0)" data-target=".navbar-collapse" onclick="app.loadModule('corpEstado',{title:'Estados da Federação'})">Estados</a></li>
                                                <li><a href="javascript:void(0)" data-target=".navbar-collapse" onclick="app.loadModule('corpMunicipio',{title:'Municípios'})">Municípios</a></li>
                                                <li><a href="javascript:void(0)" data-target=".navbar-collapse" onclick="app.loadModule('corpBioma',{title:'Biomas'})">Biomas</a></li>

                                            </ul>
                                        </li>

                                        <li role="separator" class="divider"></li>

                                        <li><a href="javascript:void(0)" data-target=".navbar-collapse" onclick="app.loadModule('dadosApoio')">Apoio</a></li>

                                        <li class="dropdown dropdown-submenu">
                                             <a href="#" class="dropdown-toggle" data-toggle="dropdown">SIS/IUCN</a>
                                             <ul class="dropdown-menu">
                                                <li><a href="javascript:void(0)" data-target=".navbar-collapse" onclick="app.loadModule('iucnTabelas',{tableName:'iucnAmeaca',title:'Ameaças SIS/IUCN'})">Ameaças</a></li>
                                                <li><a href="javascript:void(0)" data-target=".navbar-collapse" onclick="app.loadModule('iucnTabelas',{tableName:'iucnUso',title:'Usos SIS/IUCN'})">Usos</a></li>
                                                <li><a href="javascript:void(0)" data-target=".navbar-collapse" onclick="app.loadModule('iucnTabelas',{tableName:'iucnAcaoConservacao',title:'Ações de Conservação SIS/IUCN'})">Ações de conservação</a></li>
                                                <li><a href="javascript:void(0)" data-target=".navbar-collapse" onclick="app.loadModule('iucnTabelas',{tableName:'iucnHabitat',title:'Habitats SIS/IUCN'})">Habitats</a></li>
                                                <li><a href="javascript:void(0)" data-target=".navbar-collapse" onclick="app.loadModule('iucnTabelas',{tableName:'iucnMotivoMudanca',title:'Motivos mudança de categoria SIS/IUCN'})">Motivos mudança categoria</a></li>
                                                <li><a href="javascript:void(0)" data-target=".navbar-collapse" onclick="app.loadModule('iucnTabelas',{tableName:'iucnPesquisa',title:'Pesquisas SIS/IUCN'})">Pesquisas necessárias</a></li>
                                             </ul>
                                        </li>

                                        <li role="separator" class="divider"></li>
                                        <li><a href="javascript:void(0)" data-target=".navbar-collapse" onclick="app.loadModule('acaoConservacao')">Ações de conservação</a></li>
                                        <li><a href="javascript:void(0)" data-target=".navbar-collapse" onclick="app.loadModule('ameaca')">Ameaças</a></li>
                                        <li><a href="javascript:void(0)" data-target=".navbar-collapse" onclick="app.loadModule('motivoMudanca')">Motivo mudança categoria</a></li>
                                        <li><a href="javascript:void(0)" data-target=".navbar-collapse" onclick="app.loadModule('habitat')">Habitats</a></li>
                                        <li><a href="javascript:void(0)" data-target=".navbar-collapse" onclick="app.loadModule('pesquisa')">Pesquisas</a></li>
                                        <li><a href="javascript:void(0)" data-target=".navbar-collapse" onclick="app.loadModule('uso')">Usos</a></li>
                                        <li role="separator" class="divider"></li>

%{--                                        <li><a href="javascript:void(0)" data-target=".navbar-collapse" title="Manutenção do cadastro de Instituições" onclick="modal.cadInstituicao()">Instituições</a></li>--}%
                                        <li><a href="javascript:void(0)" data-target=".navbar-collapse" title="Manutenção do Efeito X Ameaça" onclick="app.loadModule('efeitoAmeaca')">Manutenção do Efeito X Ameaça</a></li>
                                        <li><a href="javascript:void(0)" data-target=".navbar-collapse" title="Manutenção do glossário de tradução dos textos estáticos." onclick="modal.janelaTraducaoTextoFixo()">Glossário tradução</a></li>
                                        %{-- <li><a href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse" onclick="app.loadModule('criterioAmeacaIucn')">Ameaças</a></li> --}%
                                    </ul>
                                </li>
%{--                                <li class="dropdown">
                                    <a href="javascript:void(0)" data-target=".navbar-collapse" onclick="app.loadModule('usuarioWeb')">Usuários da Consulta Externa</a>
                                </li>--}%
                                <li role="separator" class="divider"></li>
                                <li class="dropdown">
                                    %{--<a href="javascript:void(0)" data-target=".navbar-collapse" onclick="modal.uploadManual()">Atualizar Manual</a>--}%
                                    <a href="javascript:void(0)" data-target=".navbar-collapse" title="Manutenção de Informativos"
                                       onclick="modal.janelaCadastrarInformativo()">Informativos</a>

                                    <a href="javascript:void(0)" data-target=".navbar-collapse" title="Manutenção de alertas aos usuários"
                                       onclick="modal.janelaCadastrarAlerta()">Alertas</a>
                                </li>

                                <li role="separator" class="divider"></li>

                                <li class="dropdown">
                                    <a href="javascript:void(0)" data-target=".navbar-collapse"
                                       data-file-name="manual_salve.pdf"
                                       data-title="Atualizar Manual do Sistema - PDF"
                                       data-extension="pdf"
                                       data-max-size="50000"
                                       onclick="modal.uploadArquivoSistema(this)">Atualizar manual do sistema
                                    </a>

                                    <a href="javascript:void(0)" data-target=".navbar-collapse"
                                       data-file-name="in_avaliacao_risco_extincao_especies.pdf"
                                       data-title="Atualizar a IN sobre as diretrizes e procedimentos para a Avaliação do Risco de Extinção das Espécies da Fauna Brasileira - PDF"
                                       data-extension="pdf"
                                       data-max-size="10000"
                                       onclick="modal.uploadArquivoSistema(this)">Atualizar Instrução Normativa
                                    </a>

                                    <a href="javascript:void(0)" data-target=".navbar-collapse"
                                       data-file-name="diretrizes_revisao_fichas_publicacao.pdf"
                                       data-title="Atualizar Diretrizes para Revisão das Fichas para Publicação - PDF"
                                       data-extension="pdf"
                                       data-max-size="10000"
                                       onclick="modal.uploadArquivoSistema(this)">Atualizar diretrizes para revisão das fichas para publicação
                                    </a>


                                    <a href="javascript:void(0)" data-target=".navbar-collapse"
                                       data-file-name="tutorial_construcao_mapas.zip"
                                       data-title="Atualizar manual construção de mapas - ZIP"
                                       data-extension="zip"
                                       data-max-size="40000"
                                       onclick="modal.uploadArquivoSistema(this)">Atualizar manual construção de mapas
                                    </a>
                                </li>

                                <li role="separator" class="divider"></li>

                                <li class="dropdown">
                                    <a href="javascript:void(0)" data-target=".navbar-collapse"
                                       data-file-name="planilha_modelo_importacao_registros_salve.zip"
                                       data-title="Atualizar Planilha Importação Registros - ZIP"
                                       data-extension="zip"
                                       data-max-size="4000"
                                       onclick="modal.uploadArquivoSistema(this)">Atualizar planilha importação de registros
                                    </a>
                                    <a href="javascript:void(0)" data-target=".navbar-collapse"
                                       data-file-name="planilha_modelo_importacao_registros_salve_consulta.zip"
                                       data-title="Atualizar Planilha Importação Registros (Módulo Consulta) - ZIP"
                                       data-extension="zip"
                                       data-max-size="4000"
                                       onclick="modal.uploadArquivoSistema(this)">Atualizar planilha importação de registros do módulo consulta
                                    </a>
                                    <a href="javascript:void(0)" data-target=".navbar-collapse"
                                       data-file-name="planilha_modelo_importacao_fichas_salve.zip"
                                       data-title="Atualizar Planilha Importação Fichas - ZIP"
                                       data-extension="zip"
                                       data-max-size="4000"
                                       onclick="modal.uploadArquivoSistema(this)">Atualizar planilha importação de fichas

                                    </a><a href="javascript:void(0)" data-target=".navbar-collapse"
                                       data-file-name="planilha_modelo_importacao_ref_bib.zip"
                                       data-title="Atualizar Planilha Importação Ref. Bibliográficas - ZIP"
                                       data-extension="zip"
                                       data-max-size="40000"
                                       onclick="modal.uploadArquivoSistema(this)">Atualizar planilha importação de ref. bibliográficas
                                    </a>

                                    <a href="javascript:void(0)" data-target=".navbar-collapse"
                                       data-file-name="planilha_modelo_importacao_mapas_imagens.xls"
                                       data-title="Atualizar Planilha Importação Mapas/Imagens"
                                       data-extension="xls"
                                       data-max-size="40000"
                                       onclick="modal.uploadArquivoSistema(this)">Atualizar planilha importação de mapas/imagens
                                    </a>

                                    <a href="javascript:void(0)" data-target=".navbar-collapse"
                                        data-file-name="planilha_modelo_importacao_historico_avaliacao.xls"
                                        data-title="Atualizar Planilha Importação Histórico/Avaliação"
                                        data-extension="xls"
                                        data-max-size="40000"
                                        onclick="modal.uploadArquivoSistema(this)">Atualizar planilha importação histórico/avaliação
                                    </a>

                                </li>

                                <li role="separator" class="divider"></li>

                                <li class="dropdown">
                                    <a href="javascript:void(0)" data-target=".navbar-collapse"
                                       data-file-name="salve-assinatura-eletronica.png"
                                       data-title="Atualizar imagem da assinatura eletrônica"
                                       data-extension="png,jpg,jpeg"
                                       data-max-size="5000"
                                       onclick="modal.uploadArquivoSistema(this)">Atualizar a imagem da assinatura eletrônica
                                    </a>

                                    <a href="javascript:void(0)" data-target=".navbar-collapse"
                                       data-file-name="arquivo-nao-encontrado.pdf"
                                       data-title="Atualizar pdf padrão para &quot;Arquivo Não Encontrado&quot;"
                                       data-extension="pdf"
                                       data-max-size="1000"
                                       onclick="modal.uploadArquivoSistema(this)">Atualizar PDF padrão para "Arquivo Não Encontrado"
                                    </a>
                                </li>

                                <li role="separator" class="divider"></li>

                                <li class="dropdown">
                                    <a href="javascript:void(0)" data-target=".navbar-collapse"
                                       data-file-name="planilha_importacao_taxon.csv"
                                       data-title="Enviar Planilha Táxons - CSV"
                                       data-extension="csv"
                                       data-max-size="10000"
                                       onclick="modal.uploadArquivoSistema(this)">Enviar planilha importação de táxons (csv)
                                    </a>
                                </li>

                                <li class="dropdown">
                                    <a href="javascript:void(0)" data-target=".navbar-collapse"
                                       data-file-name="shapefile"
                                       data-title="Enviar Shapefile - ZIP"
                                       data-extension="zip"
                                       data-max-size="80000"
                                       onclick="modal.uploadArquivoSistema(this)">Enviar arquivo shapefile (zip)
                                    </a>
                                </li>

                                </ul>
                            </li>
                        </g:if>
                        %{-- Fim menu Administração --}%

                        %{--<MODULOS--}%
                        <g:if test="${session.sicae.user.menuModulos()}">
                            <li class="dropdown">
                                <a href="javascript:void(0)" data-target="#" class="dropdown-toggle" data-toggle="dropdown">
                                    Módulos<b class="caret"></b>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="dropdown dropdown-submenu"></li>
                                    <g:if test="${session.sicae.user.menuFicha()}">
                                        <li><a href="javascript:void(0)" onclick="app.loadModule('ficha')">Ficha</a></li>
                                    </g:if>
                                    <g:if test="${session.sicae.user.menuConsulta()}">
                                        <li><a href="javascript:void(0)" onclick="app.loadModule('gerenciarConsulta');return false;">Consultas/Revisão </a></li>
                                    </g:if>
                                    <g:if test="${session.sicae.user.menuOficina()}">
                                        <li><a href="javascript:void(0)" onclick="app.loadModule('oficina');return false;">Avaliação</a></li>
                                    </g:if>
                                    <g:if test="${session.sicae.user.menuValidacao()}">
                                        <li><a href="javascript:void(0)" onclick="app.loadModule('gerenciarValidacao');return false;">Validação</a></li>
                                    </g:if>
                                    <g:if test="${ session.sicae.user.menuPublicacao()}">
                                        <li><a href="javascript:void(0)" onclick="app.loadModule('publicacao');return false;">Versionamento Ficha</a></li>
                                    </g:if>
                                </ul>
                            </li>
                        </g:if>


                        %{--Menu Gerenciar--}%
                        <g:if test="${session.sicae.user.menuGerenciar()}">
                            <li class="dropdown">
                                <a href="javascript:void(0)" data-target="#" class="dropdown-toggle" data-toggle="dropdown">
                                    Gerenciar<b class="caret"></b>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="dropdown dropdown-submenu"></li>
                                    <g:if test="${session.sicae.user.menuFuncao()}">
                                        <li><a href="javascript:void(0)" data-target=".navbar-collapse" onclick="app.loadModule('gerenciarPapel');return false;">Função</a></li>
                                    </g:if>
                                    <g:if test="${ session.sicae.user.menuRefBib()}">
                                        <li><a href="javascript:void(0)" onclick="modal.cadRefBib()">Referência bibliográfica</a></li>
                                    </g:if>
                                    <g:if test="${ session.sicae.user.menuTraducao()}">
%{--                                    <g:if test="${ session.sicae.user.isTradutor()}">--}%
                                        <li><a href="javascript:void(0)" onclick="app.loadModule('traducaoFicha');return false;">Tradução Fichas</a></li>
                                    </g:if>
                                </ul>
                            </li>
                        </g:if>

                    <g:if test="${ session.sicae.user.menuTaxonomia() }">
                        <li class="dropdown">
                            <a href="javascript:void(0)" data-target="#" class="dropdown-toggle" data-toggle="dropdown">
                                Taxonomia<b class="caret"></b>
                            </a>
                             <ul class="dropdown-menu">
                                 <li><a href="javascript:void(0)" data-target=".navbar-collapse" onclick="null;app.loadModule('taxonomia/cadastro')">Manutenção da classificação taxonômica</a></li>
                             </ul>
                        </li>
                    </g:if>

                    <g:if test="${ session.sicae.user.menuRelatorio() }">
                        <li class="dropdown">
                            <a href="javascript:void(0)" data-target="#" class="dropdown-toggle" data-toggle="dropdown">
                                Relatórios<b class="caret"></b>
                            </a>
                             <ul class="dropdown-menu">
                                 %{--<li><a href="javascript:void(0)" data-target=".navbar-collapse" onclick="null;app.relatorio('PF_CT',260)">Pontos focais/Coordenadores de táxon</a></li>--}%
                                 <li><a href="javascript:void(0)" data-target=".navbar-collapse" onclick="null;app.loadModule('relatorio/rel001')">Função</a></li>
                                 <li><a href="javascript:void(0)" data-target=".navbar-collapse" onclick="null;app.loadModule('relatorio/rel002')">Oficinas</a></li>
                                 <li><a href="javascript:void(0)" data-target=".navbar-collapse" onclick="null;app.loadModule('relatorio/rel003')">Táxons (analítico)</a></li>
                                 <li><a href="javascript:void(0)" data-target=".navbar-collapse" onclick="null;app.loadModule('relatorio/rel004')">Táxons (gerencial)</a></li>
                                <g:if test="${ session.sicae.user.isADM() || session.sicae.user.isPF() }">
                                     <li><a href="javascript:void(0)" data-target=".navbar-collapse" onclick="null;app.loadModule('relatorio/rel005')">Validação (controle)</a></li>
                                </g:if>
                                 <g:if test="${ session.sicae.user.isADM() }">
                                     <li><a href="javascript:void(0)" data-target=".navbar-collapse" onclick="null;app.loadModule('relatorio/rel006')">Validação (analítico)</a></li>
                                </g:if>
                             </ul>
                        </li>
                    </g:if>

                    %{--SAIR--}%
                    <li class="dropdown visible-xs-block">
                        <a href="javascript:void(0)" data-target=".navbar-collapse" onclick="app.logout()">Sair</a>
                    </li>

                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown" id="li-menu-workers">
                            <a href="javascript:app.openModalWorkers()"
                               title="Atividade(s) em endamento."><span class="blink-me badge"></span></a>
                        </li>
                        <li class="dropdown" id="li-menu-user-jobs">
                            <a href="javascript:app.openModalUserJobs()"
                               title="Tarefas"><span class="badge"><i class="fa fa-cog"></i></span></a>
                        </li>
                        %{-- <li class="dropdown visible-lg-block">
                            <a href="javascript:void(0)" Xdata-toggle="collapse" data-target=".navbar-collapse" onclick="app.clearCache()" class="green" title="Atualizar memória tempoŕaria com dados atualizados.">Limpar Cache</a>
                        </li> --}%
                    </ul>
                </div>
            </div>
        </div> %{-- fim menu principal --}%
    </body>
</html>
