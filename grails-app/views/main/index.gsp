<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
	</head>
	<body>
        <g:if test="${!session?.sicae?.user}">
            %{--<meta http-equiv="refresh" content="10;URL='${grailsApplication.config.url.sicae}'" />--}%

                <div class="panel panel-success" style="display:none;margin-top:15%;min-width:200px; width:25%;background-color:transparent;border:1.5px solid #e4e8df;box-shadow: 1px 10px 30px -10px rgba(0,0,0,0.5);" id="divNewSession">
                    <div class="panel-body text-center">
                        <g:if test="${flash.message}">
                            <div class="alert alert-danger" style="display: block;font-size:18px;font-weight:bold;">${flash.message}</div>
                        </g:if>
                        <p>
                            <h3>Sessão encerrada!</h3>
                        </p>
                        <div>
                            <a title="Clique aqui para inicializar uma nova sessão SICA-e!" href="${grailsApplication.config.url.sicae}" class="btn btn-success btn-lg" style="color:#fff !important;"><span class="glyphicon glyphicon-lock mr10"></span>Nova sessão</a>
                        </div>
                    </div>
                    <br/>
                </div>

        </g:if>
        <g:else>
            <g:if test="${session?.sicae?.user.isVL()}">
                <g:render template="/gerenciarValidacao/telaInicialValidador"></g:render>
            </g:if>
        </g:else>
	</body>
</html>
