<style>
.dashboard {
}
.dashboard table td {
    text-align: center;
}
.dashboard th {
	white-space: nowrap;
	padding: 4px !important;
	vertical-align: middle !important;
	text-align: center !important;
	/*background-color: #EFF0EE;
	background-image: -moz-linear-gradient(top, #DCDCDC, #EFF0EE);
	background-image: -ms-linear-gradient(top, #DCDCDC, #EFF0EE);
	background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#DCDCDC), to(#EFF0EE));
	background-image: -webkit-linear-gradient(top, #DCDCDC, #EFF0EE);
	background-image: -o-linear-gradient(top, #DCDCDC, #EFF0EE);
	background-image: linear-gradient(top, #DCDCDC, #EFF0EE);
	background-repeat: repeat-x;
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#DCDCDC', endColorstr='#EFF0EE', GradientType=0);
	 */

    /*-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;
	-webkit-box-shadow: 0 1px 3px rgba(0, 0, 0, .25), inset 0 -1px 0 rgba(0, 0, 0, .1) !important;
	-moz-box-shadow: 0 1px 3px rgba(0, 0, 0, .25), inset 0 -1px 0 rgba(0, 0, 0, .1) !important;
	box-shadow: 0 1px 3px rgba(0, 0, 0, .25), inset 0 -1px 0 rgba(0, 0, 0, .1) !important;
     */
}
</style>
<!-- controller:main/dashboard -->
<!-- view/main/_dashboard.gsp -->
<div class="col-sm-12 dashboard">
	<g:each var="dados" in="${resultado}">
    <div class="row mb10">
        <div class="col-sm-12">
            <fieldset>
            <legend>${ br.gov.icmbio.Util.formatInt( dados.qtdFichas ) } fichas</legend>
%{--            <p>Nome: <b>${dados.tituloCiclo}</b></p>--}%
%{--            <p class="fs15">Quantidade: <b>${ br.gov.icmbio.Util.formatInt( dados.qtdFichas ) }</b></p>--}%

			%{-- painel 1 --}%
			<div class="panel panel-success">
		 		<div class="panel-heading dashboard-header-participacao">
		 			Participação das Unidades
		 		</div>
		 		<div class="panel-body" style="">
		    		<table class="table table-hover">
		    			<thead>
		    			<tr>
		    				<th>Sigla</th>
		    				<th>Fichas</th>
		    				<th>%</th>
		    				<th>Preenchimento</th>
		    				<th>Avaliadas</th>
		    				<th>Publicadas</th>
							%{-- <th>Validadas</th> --}%
                        </tr>
                        </thead>
                        <tbody>
                        <g:if test="dados.centros">
                            <g:each var="item" in="${dados.centros.sort{it.key} }">
                                <tr>
                                    <td style="text-align:left;">${item.key}</td>
                                    <td>${ br.gov.icmbio.Util.formatInt( item.value.somaFichas ) }</td>

                                    <td>${dados.qtdFichas       > 0 ? String.format('%.1f', (item.value.somaFichas / dados.qtdFichas * 100 ) ) : '0' }%</td>
                                    <td>${item.value.somaFichas > 0 ? ( (int) (item.value.somaPercentual / item.value.somaFichas ) ) : '0' }%</td>
                                    <td>${br.gov.icmbio.Util.formatInt(item.value.qtdFichaAvaliadas)} (${ item.value.somaFichas > 0 ? (int) ( item.value.qtdFichaAvaliadas / item.value.somaFichas   * 100 ) : '0'}%)</td>
                                    <td>${br.gov.icmbio.Util.formatInt(item.value.qtdFichaPublicadas)} (${ item.value.somaFichas > 0 ? (int) ( item.value.qtdFichaPublicadas / item.value.somaFichas   * 100 ) : '0'}%)</td>
									%{--  <td>${item.value.qtdFichasValidadas}</td> --}%
                                </tr>
                            </g:each>
                        </g:if>
                        </tbody>
                    </table>
                  </div>
            </div>
            %{-- painel 2 --}%
			<div class="panel panel-success  mt30">
		 		<div class="panel-heading dashboard-header-consultas">Consultas</div>
		 		<div class="panel-body">
		    		<table class="table table-hover">
		    			<thead>
		    			<tr>
		    				<th>Tipo</th>
		    				<th>Titulo</th>
		    				<th>Período</th>
		    			</tr>
		    			</thead>
		    			<tbody>
                        <g:if test="${dados.consultas}">
                            <g:each var="item" in="${ dados.consultas }">
                                <tr class="${item.stAberta ? 'light-yellow':''}">
                                    <td class="nowrap">${item.tipoConsulta}</td>
                                    <td style="text-align:left;">${item.deTituloConsulta}</td>
                                    <td class="nowrap">${item.periodoText}</td>
                                </tr>
                            </g:each>
                        </g:if>

		    			</tbody>
		    		</table>
		  		</div>
			</div>

			%{-- painel 3 --}%
			<div class="panel panel-success mt30">
		 		<div class="panel-heading dashboard-header-oficinas">Oficinas</div>
		 		<div class="panel-body" style="">
		    		<table class="table table-hover">
		    			<thead>
		    			<tr>
		    				<th>Titulo</th>
		    				<th>Local</th>
		    				<th>Unidade</th>
		    				<th>Período</th>
		    				<th>Situação</th>
		    			</tr>
		    			</thead>
		    			<tbody>
                            <g:if test="${dados.oficinas}">
                                <g:each var="item" in="${ dados.oficinas }">
                                    <tr class="${item.stAberta ? 'light-green':''}">
                                        <td style="text-align:left;">${item?.sgOficina}</td>
                                        <td style="text-align:left;">${item?.deLocal}</td>
                                        <td style="text-align:left;" class="nowrap">${item.sgUnidade}</td>
                                        <td class="nowrap">${item.dePeriodo}</td>
                                        <td>${item.deSituacao}</td>
                                    </tr>
                                </g:each>
                        </g:if>
		    			</tbody>
		    		</table>
		  		</div>
			</div>
			%{-- painel 4 --}%
			%{-- painel 5 --}%
			%{-- painel 6 --}%
			%{-- painel 7 --}%

        </fieldset>

		</div> %{-- fim col --}%

		%{--<div class="col-sm-4" style="display:none;">
            <fieldset>
                <legend>Ficha Técnica
                	<button type="button" onClick="$('div.dashboard').remove()" class="btn btn-default btn-xs pull-right mt10"><i class="red glyphicon glyphicon-remove"></i></button>
                </legend>
                <div>
                    <table class="table table-hover table-bordered w100p">
                        <tr><td>Responsável</td><td class="td-bold"  style="text-align:left;">
                            <ul style="font-weight:bold;">
                                <li>Rodrigo Silva Pinto Jorge</li>
                            </ul>
                        </td></tr>
                        <tr><td>Gestores</td><td class="td-bold"  style="text-align:left;">
                        <ul  style="font-weight:bold;">
                            <li>Artur Pereira Brant</li>
                            <li>Carlos Eduardo Carvalho Guidorizi</li>
                            <li>Estevão Carino</li>
                            <li>Tainah Corrêa Seabra Guimarães</li>
                            <li>.</li>
                            <li>.</li>
                        </ul>
                        </td>
                        </tr>

                        <tr><td>Colaboradores</td><td class="td-bold"  style="text-align:left;">
                        <ul style="font-weight:bold;">
                            <li>Danyhelton Douglas</li>
                            <li>Pedro Luiz Migliari</li>
                            <li>Urbano Lopes</li>
                            <li>Verônica Silva</li>
                            <li>.</li>
                            <li>.</li>
                            <li>.</li>
                            <li>.</li>
                        </tr>

                        <tr><td>Desenvolvedor</td><td class="td-bold"  style="text-align:left;">
                            <ul style="font-weight:bold;">
                                <li>Luis Eugênio Barbosa</li>
                            </ul>
                        </td>
                        </tr>
                    </table>
                </div>
            </fieldset>
		</div>--}%
	</div>
	</g:each>
</div>
