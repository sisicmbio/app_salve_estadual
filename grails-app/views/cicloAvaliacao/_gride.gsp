<table class="table table-hover table-striped table-condensed table-bordered">
    <thead>
        <th style="width:20px;">#</th>
        <th style="width:120px;">Ano</th>
        <th style="width:*">Descrição</th>
        <th style="width:100px;">Situação</th>
        <th style="width:100px;">Consulta/Oficina</th>
        <th style="width:100px;">Público?</th>
        <th style="width:100px;">Ação</th>

    </thead>
    <tbody>
        <g:each in="${listCiclos}" var="item" status="i">
            <tr>
                <td class="text-center">${i + 1}</td>
                <td class="text-center">${item?.nuAno + ' a '+ (item?.nuAno+4) }</td>
                <td>${item?.deCicloAvaliacao}</td>
                <td class="text-center" data-situacao="${item?.inSituacao}">
                    <div class="${item?.inSituacao=='A'?'cicloAberto':'cicloFechado'}">
                    <g:if test="${item?.inSituacao=='A'}">
                        Aberto
                    </g:if>
                    <g:else>
                        Fechado
                    </g:else>
                    </div>
                </td>
                <td class="text-center" data-situacao="${item?.inOficina}">
                    <div class="${item?.inOficina=='S' ? 'cicloAberto' : 'cicloFechado'}">
                    <g:if test="${item?.inOficina=='S'}">
                        Habilitado
                    </g:if>
                    <g:else>
                        Desabilitado
                    </g:else>
                    </div>
                </td>
                <td class="text-center" data-situacao="${item?.inPublico}">
                    <div class="${item?.inPublico=='S' ? 'cicloAberto' : 'cicloFechado'}">
                    <g:if test="${item?.inPublico=='S'}">
                        Sim
                    </g:if>
                    <g:else>
                        Não
                    </g:else>
                    </div>
                </td>


                <td class="td-actions">
                    <button type="button" data-id="${item?.id}" data-action="cicloAvaliacao.edit" class="btn btn-default btn-xs btn-update" data-toggle="tooltip" data-placement="top" title="Alterar">
                        <span class="glyphicon glyphicon-pencil"></span>
                    </button>
                    <button type="button" data-id="${item?.id}" data-descricao="${item?.nuAno + ' a '+ (item?.nuAno+5)}"data-action="cicloAvaliacao.delete" class="btn btn-default btn-xs btn-delete" data-toggle="tooltip" data-placement="top" title="Excluir">
                        <span class="glyphicon glyphicon-remove"></span>
                    </button>
                </td>
            </tr>
        </g:each>
    </tbody>
</table>