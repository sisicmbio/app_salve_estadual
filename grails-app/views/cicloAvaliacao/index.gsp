<style type="text/css">
            .fldAno{
                max-width: 100px;
            }
            .cicloAberto{
                background-color: #0B6C33;
                color:#fff;
                -webkit-border-radius: 7px;
                -moz-border-radius: 7px;
                border-radius: 7px;
                padding:0 3px 0 3px;
                min-width: 70px;
            }
            .cicloFechado{
                background-color: #C50505;
                color:#fff;
                -webkit-border-radius: 7px;
                -moz-border-radius: 7px;
                border-radius: 7px;
                padding:0 3px 0 3px;
                min-width: 70px;
            }
</style>
<div id="cicloAvaliacaoContainer">
   <div class="row">
      <div class="col-sm-12">
         <div class="panel panel-default">
            <div class="panel-heading">
               <span>Ciclo de Avaliação</span>
               <button id="btnClose" data-action="app.unloadModule" class="btn btn-default pull-right btn-xs" title="Fechar">
               <span class="glyphicon glyphicon-remove btnClose"></span>
               </button>
               <button id="btnAdd" data-action="cicloAvaliacao.addCiclo" class="btn btn-default pull-right btn-xs btnAdd">
               <span class="glyphicon glyphicon-plus"></span>&nbsp; Cadastrar
               </button>
            </div>
            <div class="panel-body" id="cadCicloAvaliacaoBody">

               <form class="form-inline hidden" tabindex="0" id="frmCadCicloAvaliacao" role="form">
                  <g:hiddenField name="sqCicloAvaliacao" value="" />
                  <div class="form-group">
                     <label for="nuAno" class="control-label">
                        Ano Inicial
                     </label>
                     <br/>
                      <g:field type="number" maxlength="4" data-change="cicloAvaliacao.nuAnoChange" name="nuAno" id="nuAno" tabindex="1" value="${data?.ano}" class="form-control fldAno" required="true" data-msg-required="Campo obrigatório" data-rule-digits="true" data-rule-range="[2000,3000]" data-msg-range="Ano deve estar entre 2000 e 3000!"/>
                  </div>
                  <div class="form-group">
                     <label for="nuAno2" class="control-label">
                        Ano Final
                     </label>
                     <br/>
                     <g:field type="number" maxlength="4" name="nuAno2" id="nuAno2" disabled tabindex="2" value="${data?.ano ? data.ano+5:''}" class="form-control fldAno" required="true" data-msg-required="Campo obrigatório" data-rule-digits="true" data-rule-range="[2000,3000]" data-msg-range="Ano deve estar entre 2000 e 3000!"/>
                  </div>

                   <div class="form-group">
                       <label for="inSituacao" class="control-label">
                           Situação
                       </label>
                       <br/>
                       <g:select name="inSituacao" id="inSituacao" from="${ ['A':'Aberto', 'F':'Fechado'] }" value="${data?.situacao}"  tabindex="3" optionKey="key" optionValue="value" noSelection="['':'-- selecione --']" required="true" class="form-control fldSelect" data-error="Este campo é obrigatório"/>
                   </div>

                   <div class="form-group">
                       <label for="inOficina" class="control-label">
                           Consulta/Oficina
                       </label>
                       <br/>
                       <g:select name="inOficina" id="inOficina" from="${ ['S':'Habilitado', 'N':'Desabilitado'] }" value="${data?.oficina}"  tabindex="4" optionKey="key" optionValue="value" noSelection="['':'-- selecione --']" required="true" class="form-control fldSelect" data-error="Este campo é obrigatório"/>
                   </div>

                   <div class="form-group">
                       <label for="inPublico" class="control-label" title="Esta opção hablita/desabilita as informações do ciclo de serem consultadas no módulo público.">
                           Módulo publico?
                       </label>
                       <br/>
                       <g:select name="inPublico" id="inPublico" from="${ ['S':'Sim', 'N':'Não'] }" value="${data?.oficina}"  tabindex="4" optionKey="key" optionValue="value" noSelection="['':'-- selecione --']" required="true" class="form-control fldSelect" data-error="Este campo é obrigatório"/>
                   </div>

                   <br/>

                  <div class="form-group w100p">
                     <label for="deCicloAvaliacao" class="control-label">
                        Descrição
                     </label>
                     <br/>
                     <g:textArea name="deCicloAvaliacao" value="" tabindex="5" rows="5" cols="40" escapeHtml="true" class="form-control w100p" required="true"/>
                  </div>

                  %{-- Botões --}%
                  <div class="fld panel-footer">
                     <button id="btnSaveFrmCicloAvaliacao" data-action="cicloAvaliacao.save" class="btn btn-success">Gravar</button>
                     <button id="btnResetFrmCicloAvaliacao" data-action="cicloAvaliacao.reset" class="btn btn-info hidden">Limpar Formulário</button>
                  </div>

               </form>
               <div id="divGridCicloAvaliacao" class="mt10"></div>
            </div>
         </div>
      </div>
   </div>
</div>
