//# sourceURL=N:\SAE\sae\grails-app\views\cicloAvaliacao\cicloAvaliacao.js
 var cicloAvaliacao = {
    form      : '#frmCadCicloAvaliacao',
    init      : function (params)
    {
        cicloAvaliacao.updateGrid();
    },
    updateGrid: function (params)
    {
        app.ajax(app.url + 'cicloAvaliacao/getGrid', null,
            function (res)
            {
                $("#cicloAvaliacaoContainer #divGridCicloAvaliacao").html(res);
            }, null, 'html'
        );
    },
    showForm  : function (params, ele, evt, show)
    {
        if (!$(cicloAvaliacao.form).is(":visible"))
        {
            $(cicloAvaliacao.form).removeClass('hidden');
            $("#btnAdd").html('Fechar Formulário');
            app.focus('nuAno');
        }
        else
        {
            if (!show)
            {
                $(cicloAvaliacao.form).addClass('hidden');
                $("#btnAdd").html('Cadastrar');
            }
        }
    },
    nuAnoChange : function(params)
    {
        var ano = parseInt( $("#nuAno").val() );
        $("#nuAno2").val(isNaN(ano)  ? '' : ( ano + 4 ) );
    },
    addCiclo  : function (params)
    {
        cicloAvaliacao.reset();
        cicloAvaliacao.showForm(null, null, null, null);
    },
    save      : function (params)
    {
        var form = $(cicloAvaliacao.form);
        if (!form.valid())
        {
            return;
        }
        var data = form.serializeArray();
        app.ajax(app.url + 'cicloAvaliacao/save', data, function (res)
        {
            if (res.status === 0)
            {
                cicloAvaliacao.updateGrid();
                cicloAvaliacao.reset();
                cicloAvaliacao.showForm(); // fechar o formulário
            }
        }, null, 'json');
    },
    reset     : function (params)
    {
        app.reset(cicloAvaliacao.form);
        $(cicloAvaliacao.form + " #btnResetFrmCicloAvaliacao").addClass('hidden');
        $(cicloAvaliacao.form + " #btnSaveFrmCicloAvaliacao").html($(cicloAvaliacao.form + " #btnSaveFrmCicloAvaliacao").data('value'));
        app.focus('nuAno');
    },
    edit      : function (params, btn)
    {
        if (params.id)
        {
            app.ajax(app.url + 'cicloAvaliacao/edit', params,
                function (res)
                {
                    $("#btnResetFrmCicloAvaliacao").removeClass('hide');
                    $("#btnSaveFrmCicloAvaliacao").data('value', $("#btnSaveFrmCicloAvaliacao").html());
                    $("#btnSaveFrmCicloAvaliacao").html("Gravar Alteração");
                    app.setFormFields(res, cicloAvaliacao.form);
                    app.selectGridRow(btn);
                    cicloAvaliacao.nuAnoChange();
                    cicloAvaliacao.showForm(null, null, null, true);
                });
        }
    },
    delete    : function (params, btn)
    {
        app.selectGridRow(btn);
        app.confirm('<br>Confirma exclusão do ciclo ' + params.descricao + '?',
            function ()
            {
                app.ajax(app.url + 'cicloAvaliacao/delete', params,
                    function (res)
                    {
                        cicloAvaliacao.reset(cicloAvaliacao.form);
                        cicloAvaliacao.updateGrid();
                    });
            },
            function ()
            {
                app.unselectGridRow(btn);
            }, params, 'Confirmação', 'warning');
    },
}
cicloAvaliacao.init();

