//# sourceURL=/views/iucnTabelas.js
;
var iucnTabelas = {
    tableName:'',
    title:'',
    modalTitle:'',
    init:function() {
       iucnTabelas.tableName = $("#iucnTableName").val();
       iucnTabelas.title = $("#iucnTableTitle").val();
       iucnTabelas.modalTitle = 'Manutenção da Tabela ' + iucnTabelas.title
       iucnTabelas.updateGrid();
    },
    updateGrid:function( params ){
        var data = {tableName:iucnTabelas.tableName}
        var scrollPosition = $(document).scrollTop();
        $("#iucnTable div.panel-body").html( grails.spinner );
        app.ajax(app.url+'iucnTabelas/getGrid', data,
            function(res) {
                $("#iucnTable div.panel-body").html(res);
                $(document).scrollTop(scrollPosition);
            }
       );
    },
    // fim init
    add:function( params ) {
        iucnTabelas.showModal(params);
    },
    save:function( params )    {
        var data = $("#frmIucnTabelas").serializeArray();
        data = app.params2data(params, data);
        data.tableName = iucnTabelas.tableName;

        app.ajax(app.url+'iucnTabelas/save', data,
            function(res) {
                if( res.status == 0 ) {
                    $("#frmIucnTabelas input[name=id]").val(res.data.id);
                    app.closeWindow('cadIucnTabelas');
                    iucnTabelas.updateGrid();
                }
            }, null, 'json'
        );


        //curNode.load();
    }, // uso
    edit:function( params ) {
        iucnTabelas.showModal(params);
    },
    delete:function( params ) {

        app.confirm('Confirma e exclusão?<br><br><b>'+params.descricao+'</b>',function() {
            var data = { id:params.id, tableName:iucnTabelas.tableName };
            app.ajax(app.url+'iucnTabelas/delete', data,
            function(res) {
                if( res.status == 0 ) {
                    iucnTabelas.updateGrid();
                }
            });

        })
    },
    showModal:function( params ) {
        app.openWindow({
             id:'cadIucnTabelas'
            ,url:app.url + 'iucnTabelas/getForm'
            ,width:750
            ,data:{'reload':true,'id':params.id,'operacao':params.action, tableName:iucnTabelas.tableName}
            ,height:350
            ,title:iucnTabelas.modalTitle
            ,autoOpen:true
            ,modal:true
            },
            // open
            function(data,e){
                // onShow
                data = data.data;
                // alterar os valores dos parametros que serão enviados ao clicar no botão salvar, de acordo com o contexto
                for (key in data) {
                    if (key !== 'action') {
                        $("#btnSaveFrmIucnTabelas").data(key, data[key]);
                    }
                }
                app.focus('codigo');
            },
            // close
            function(data,e) {
                app.reset('frmCadIucnTabelas');

            }
          );
    }
}
setTimeout(function() {
    iucnTabelas.init();
},1000);
