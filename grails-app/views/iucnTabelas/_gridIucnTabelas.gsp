<!-- view: /views/iucnTabelas/_gridIucnTabelas.gsp -->
<!-- controller:${params?.controller}-->
<!-- action: ${params?.action}-->

<table class="table table-condensed table-hover table-striped">
    <thead>
    <tr>
        <th>Código</th>
        <th>Descrição</th>
        <th>Ordem</th>
        <th>Ação</th>
    </tr>
    </thead>
    <tbody>
    <g:if test="${rows}">
        <g:each var="row" in="${rows}">
        <tr>
            <td>${row.codigo}</td>
            <td>${row.descricao}</td>
            <td>${row.ordem}</td>

            <td class="text-center text-nowrap actions" style="padding-top:4px !important;">
                <a data-action="iucnTabelas.edit" data-all="true"
                   data-id="${row.id}"
                   class="fld btn btn-default btn-xs btn-update" title="Alterar">
                    <span class="glyphicon glyphicon-pencil"></span>
                </a>
                %{--<a data-action="iucnTabelas.add" data-all="true"
                   class="fld btn btn-default btn-xs btn-plus" title="Adicionar Subitem">
                   <span class="glyphicon glyphicon-plus"></span>
                </a>--}%
                <a data-action="iucnTabelas.delete"
                   data-id="${row.id}"
                   data-descricao="${ row.codigo + ' '+ row.descricao}"
                   class="fld btn btn-default btn-xs btn-delete" title="Excluir">
                   <span class="glyphicon glyphicon-remove"></span>
                </a>

            </td>
        </tr>
    </g:each>
    </g:if>
    <g:else>
        <tr><td colspan="5">Nenhum registro encontrado</td></tr>
    </g:else>
    </tbody>
</table>
