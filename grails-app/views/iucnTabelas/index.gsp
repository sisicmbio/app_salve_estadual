<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="ajax"/>
        <title>
        SALVE - Manutanção das Tabelas SIS/IUCN
        </title>
    </head>
    <body>
        <div class="col-sm-12">
            <div class="panel panel-default w100p" id="iucnTable">
                <div class="panel-heading">
                    <span>
                        ${params.title ?: 'SIS/IUCN'}
                    </span>
                    <input type="hidden" id="iucnTableName" value="${params.tableName}">
                    <input type="hidden" id="iucnTableTitle" value="${params.title}">
                    <button id="btnClose" data-action="app.unloadModule" class="btn btn-default pull-right btn-xs" title="Fechar"><span class="glyphicon glyphicon-remove btnClose"></span></button>
                    <button type="button" id="btnAdd" data-table-name="${params.tableName}" data-action='iucnTabelas.add' class="btn btn-default pull-right btn-xs" data-toggle="tooltip" data-placement="top" title="Novo registro"><span class="glyphicon glyphicon-plus"></span>&nbsp; Cadastrar</button>
                </div>

                <div class="panel-body">


                </div>
            </div>
        </div>
    </body>
</html>
