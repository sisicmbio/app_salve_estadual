<div class="window-container text-left">
	<form id="frmIucnTabelas" name="frmIucnTabelas" class="form-inline" role="form">
        <input type="hidden" name="id" value="${reg.id ?:''}">
      	<div class="form-group">
			<label for="codigo" class="control-label">
				Código
			</label>
			<br>
			<input name="codigo" id="codigo" type="text" class="form-control form-clean fld80" placeholder="Código" value="${reg?.codigo}" required="true"/>
		</div>

		<div class="form-group">
			<label for="inputIucnTabelas" class="control-label">
				Descrição
			</label>
			<br>
			<input name="descricao" id="inputIucnTabelas" type="text" class="form-control form-clean fld500" placeholder="Informe o nome" value="${reg?.descricao}" required="true" data-msg-required="Campo Obrigatório"/>
		</div>
        <br>
        <div class="form-group">
            <label for="ordem" class="control-label">
                Ordem listagem
            </label>
            <br>
            <input name="ordem" id="ordem" type="text" class="form-control form-clean fld120" placeholder="Ordem" value="${reg?.ordem}" required="true"/>
        </div>

        <div class="fld panel-footer">
			<button id="btnSaveFrmIucnTabelas" data-action="iucnTabelas.save" class="fld btn btn-success">Gravar</button>
		</div>
	</form>
</div>
