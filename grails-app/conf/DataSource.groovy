dataSource {
  pooled = true
  driverClassName = "org.postgresql.Driver"
  dialect="org.hibernate.spatial.dialect.postgis.PostgisDialect"
  formatSql=false
  logSql=false
  loggingSql=false
  useSqlComments=false

}
/*
mongodb {
    options {
        connectionsPerHost = 10
        maxWaitTime = 120000
        autoConnectRetry = true
        connectTimeout = 300
        socketTimeout = 0 // The socket timeout. 0 == infinite
        sslEnabled = false // Specifies if the driver should use an SSL connection to Mongo
    }
}
*/

hibernate {
  cache.use_second_level_cache = false
  cache.use_query_cache = false
  cache.region.factory_class = 'org.hibernate.cache.ehcache.EhCacheRegionFactory' // Hibernate 4
}

// environment specific settings
environments
{
    development {}
    test {}
    production {}
}
