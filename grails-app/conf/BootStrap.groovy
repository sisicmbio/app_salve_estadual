import br.gov.icmbio.CicloAvaliacao
import br.gov.icmbio.DadosApoio
import br.gov.icmbio.Instituicao
import br.gov.icmbio.UserJobs
import br.gov.icmbio.Perfil
import br.gov.icmbio.Usuario
import br.gov.icmbio.Pessoa
import br.gov.icmbio.PessoaFisica
import br.gov.icmbio.UsuarioPerfil
import br.gov.icmbio.Util

import java.nio.charset.Charset

class BootStrap {

  	def grailsApplication
    def sqlService

	def init = { servletContext ->
        println ' '
        println ' '
        println ' '
        this.configProxy()
        this.clearUserJobs()
        this.initDb()


        /*if ( ! DadosApoio.findByPaiAndCodigoSistema(tbSituacaoFicha,'AVALIADA_EM_REVISAO'))
        {
            new DadosApoio(codigo: 'AVALIADA_EM_REVISAO', codigoSistema: 'AVALIADA_EM_REVISAO', descricao: 'Avaliada em Revião', pai: tbSituacaoFicha, ordem: 'd').save(flush: true);
        }
        */

        /*******************************************************
        * Variaveis da applicacao
        *******************************************************/
        if( ! grailsApplication.config.app.title ){
            grailsApplication.config.app.title = 'Sistema de Avaliação do Risco de Extinção da Biodiversidade'
        }
        if( ! grailsApplication.config.app.sigla ){
            grailsApplication.config.app.sigla = 'SALVE-Estadual'
        }
        if (! grailsApplication.config.app.email ){
            grailsApplication.config.app.email="salve-estaduall@estado.gov.br"
        }
        if( ! grailsApplication.config.app.instituicao.nome ){
            grailsApplication.config.app.instituicao.nome=''
        }
        if( ! grailsApplication.config.app.instituicao.sigla ){
            grailsApplication.config.app.instituicao.sigla=''
        }
        if( ! grailsApplication.config.app.instituicao.logo.padrao ){
            grailsApplication.config.app.instituicao.logo.padrao=''
        }
        if( ! grailsApplication.config.app.instituicao.logo.pequena ){
            grailsApplication.config.app.instituicao.logo.pequena=''
        }

        if( ! grailsApplication.config.app.instituicao.site.padrao){
            grailsApplication.config.app.instituicao.site.padrao=''
        }


        // converter html entities para caracteres normais. Ex: &ccedil; -> ç
        grailsApplication.config.app.instituicao.nome = Util.html2str(new String( grailsApplication.config.app.instituicao.nome.toString().getBytes("ISO-8859-1") )  )
        grailsApplication.config.app.title = Util.html2str(new String( grailsApplication.config.app.title.toString().getBytes("ISO-8859-1") ))

        println 'Application title:' + grailsApplication.config.app.title

        /*******************************************************
        * Variaveis do DOI / Crossref - https://www.crossref.org
        *******************************************************/
        if (! grailsApplication.config.doi.crossref.prefix_icmbio ){
            grailsApplication.config.doi.crossref.prefix_icmbio='10.37002'
        }
        if (! grailsApplication.config.doi.crossref.operation ){
            grailsApplication.config.doi.crossref.operation='doMDUpload'
        }
        if (! grailsApplication.config.doi.crossref.login_id ){
            grailsApplication.config.doi.crossref.login_id='icbbio'
        }
        if (! grailsApplication.config.doi.crossref.works.url ){
            grailsApplication.config.doi.crossref.works.url='https://api.crossref.org/works/'
        }


        if(grailsApplication.config.env == 'production'){ // env production
            if (! grailsApplication.config.doi.crossref.login_passwd ){
                grailsApplication.config.doi.crossref.login_passwd='ic_6921_xfj'
            }
            if (! grailsApplication.config.doi.crossref.url ){
                grailsApplication.config.doi.crossref.url='https://doi.crossref.org/servlet/deposit'
            }
            if (! grailsApplication.config.doi.crossref.user.id ) {
                grailsApplication.config.doi.crossref.urer.id = '88943'
            }
        } else { // env dev
            grailsApplication.config.doi.crossref.login_passwd='d011cmb10'
            grailsApplication.config.doi.crossref.url='https://test.crossref.org/servlet/deposit'
            if (! grailsApplication.config.doi.crossref.user.id ){
                grailsApplication.config.doi.crossref.urer.id = '200000471'
            }
        }
        /******************************************************/


        if (! grailsApplication.config.env )
        {
            grailsApplication.config.env='production'
        }

        if (!grailsApplication.config.url.sistema)
        {
            grailsApplication.config.url.sistema = '/salve-estadual/'
        }

        if (!grailsApplication.config.url.search.portalbio)
        {
            grailsApplication.config.url.search.portalbio = '/salve-estadual/api/searchPortalbio/'
        }

        if ( ! grailsApplication.config.url.sicae) {
            grailsApplication.config.url.sicae = 'https://sicae.sisicmbio.icmbio.gov.br/'
        }

        if ( ! grailsApplication.config.url.salve.api) {
            grailsApplication.config.url.salve.api = 'https://salve-estadual/api/'
        }

        if ( ! grailsApplication.config.url.salve.icmbio.api) {
            grailsApplication.config.url.salve.icmbio.api = 'https://salve.icmbio.gov.br/salve-api/'
        }

        if (! grailsApplication.config.url.salve.consulta )
        {
            grailsApplication.config.url.salve.consulta="http://localhost:8090/salve-consulta/"
        }

        if (! grailsApplication.config.tomcat.dir )
        {
            grailsApplication.config.tomcat.dir="/opt/apache-tomcat-7.0.78/"
        }

        if (! grailsApplication.config.cache.dir ) {
            grailsApplication.config.cache.dir="/data/salve-estadual/cache/"
        }

        if (! grailsApplication.config.iucn.dir ) {
            grailsApplication.config.iucn.dir="/data/salve-estadual/iucn/"
        }

        if ( ! grailsApplication.config.traducao.urlGoogle ) {
            grailsApplication.config.traducao.urlGoogle = "https://translate.googleapis.com/translate_a/single?client=gtx&sl=pt-br&tl=en&dt=t&q="
        }

        if ( ! grailsApplication.config.traducao.urlLibreTranslate ) {
            grailsApplication.config.traducao.urlLibreTranslate = "https://libretranslate.icmbio.gov.br/translate"
        }

        // criar os diretórios de trabalho utilzados pela aplicacao
        String dirAnexos = grailsApplication.config.anexos.dir;
        File file
        if (dirAnexos)
        {
            file = new File(dirAnexos);
            if (!file.exists() && !file.mkdirs())
            {
                println 'Nao foi possível criar o diretório: ' + (dirAnexos ?: '/data/salve-estadual/anexos')
                System.exit(1);
            }
        }
        String dirArquivos = grailsApplication.config.arquivos.dir;
        if (dirArquivos)
        {
            file = new File(dirArquivos);
            if (!file.exists() && !file.mkdirs())
            {
                println 'Nao foi possível criar o diretório: ' + (dirArquivos ?: '/data/salve-estadual/arquivos');
                System.exit(1);
            }

        }
        // criar os diretórios de trabalho utilzados pela aplicacao
        String dirPlanilhas = grailsApplication.config.planilhas.dir
        if (dirPlanilhas)
        {
            file = new File(dirPlanilhas);
            if (!file.exists() && !file.mkdirs())
            {
                println 'Nao foi possível criar o diretorio: ' + (dirPlanilhas ?: '/data/salve-estadual/planilhas')
                System.exit(1);
            }

        }
        // criar os diretórios de trabalho utilzados pela aplicacao
        String dirTemp = grailsApplication.config.temp.dir
        if (dirTemp)
        {
            file = new File(dirTemp);
            if (!file.exists() && !file.mkdirs())
            {
                println 'Nao foi possível criar o diretório: ' + (dirTemp ?: '/data/salve-estadual/temp')
                System.exit(1);
            }
        }
        String dirMultimidia = grailsApplication.config.multimidia.dir
        if (dirMultimidia)
        {
            file = new File(dirMultimidia);
            if (!file.exists() && !file.mkdirs())
            {
                println 'Nao foi possível criar o diretório: ' + (dirMultimidia ?: '/data/salve-estadual/multimidia')
                System.exit(1);
            }
        }

        String dirCache = grailsApplication.config.cache.dir;
        if ( dirCache )
        {
            file = new File( dirCache );
            if (!file.exists() && !file.mkdirs())
            {
                println 'Nao foi possível criar o diretório: ' + ( dirCache  ?: '/data/salve-estadual/cache')
                System.exit(1);
            }
        }

        String dirIucn = grailsApplication.config.iucn.dir
        if ( dirIucn ) {
            file = new File( dirIucn );
            if ( ! file.exists() && ! file.mkdirs() )
            {
                println 'Nao foi possível criar o diretório: ' + (dirIucn ?: '/data/salve-estadual/iucn');
                System.exit(1);
            }
        }


        println '-' * 100
        println 'Ambiente'
        println '- env=' + grailsApplication?.config?.env
        println ' '
        println '='*45
        println '=========== DATASOURCE CONFIG =============='
        println '='*45
        grailsApplication.config.dataSource.each{ key, value ->
            println key + ' = ' + ( key == 'password' ? "**********"  : value.toString() )
        }
        println '='*45
        println ' '
        println 'Email padrão'
        println 'app.email=' + grailsApplication?.config.app?.email

        //println grailsApplication?.config?.dataSource?.password

        println 'Urls Externas'
        println '- url.solr=' + grailsApplication?.config?.url?.solr
        println '- url.biocache=' + grailsApplication?.config?.url?.biocache
        println '- url.sicae=' + grailsApplication?.config?.url?.sicae
        println '- url.cncfloraWsTaxon=' + grailsApplication?.config?.url?.cncfloraWsTaxon
        println '- url.biocache=' + grailsApplication?.config?.url?.biocache
        println '- url.sistema =' + grailsApplication?.config?.url?.sistema
        println '- url.salve.consulta=' + grailsApplication?.config?.url?.salve?.consulta


        println ''
        println 'Configuracao Proxy'
        println '- proxy.host=' + grailsApplication?.config?.proxy?.host
        println '- proxy.port=' + grailsApplication?.config?.proxy?.port
        println '- proxy.user=' + grailsApplication?.config?.proxy?.user
        println '- proxy.host=' + grailsApplication?.config?.proxy?.host
        println '- proxy.password=' + (grailsApplication?.config?.proxy?.password ? 'Sim' : 'Sem senha');

        // verificar configurações para envio de email
        if (!grailsApplication?.config?.email?.host)
        {
            println 'Dados para envio de email não configurado'
        } else
        {
            println 'email.host....: ' + grailsApplication?.config?.email?.host
            println 'email.username: ' + grailsApplication?.config?.email?.username
            println 'email.password: ' + grailsApplication?.config?.email?.password
            println 'email.from....: ' + grailsApplication?.config?.email?.from
            println 'email.port....: ' + grailsApplication?.config?.email?.port
        }
        println '-' * 100

        println ''
        println 'Google'
        println '- google.api.key=' + grailsApplication?.config?.google?.api?.key

        println ''
        println 'Diretorios'
        if (grailsApplication?.config?.temp?.dir)
        {
            println '- temp.dir = ' + grailsApplication?.config?.temp?.dir + '  ' + (canReadDir(grailsApplication?.config?.temp?.dir) ? 'Leitura: sim' : 'Leitura: nao') + '  ' + (canWriteDir(grailsApplication?.config?.temp?.dir) ? 'Escrita: sim' : 'Escrita: nao')
        } else
        {
            println '- temp.dir = NÃO DEFINIDO'
        }
        if (grailsApplication?.config?.anexos?.dir)
        {
            println '- anexos.dir = ' + grailsApplication?.config?.anexos?.dir + '  ' + (canReadDir(grailsApplication?.config?.anexos?.dir) ? 'Leitura: sim' : 'Leitura: nao') + '  ' + (canWriteDir(grailsApplication?.config?.anexos?.dir) ? 'Escrita: sim' : 'Escrita: nao')
        } else
        {
            println '- anexos.dir = NÃO DEFINIDO'
        }
        if (grailsApplication?.config?.iucn?.dir)
        {
            println '- iucn.dir = ' + grailsApplication?.config?.iucn?.dir + '  ' + (canReadDir(grailsApplication?.config?.iucn?.dir) ? 'Leitura: sim' : 'Leitura: nao') + '  ' + (canWriteDir(grailsApplication?.config?.iucn?.dir) ? 'Escrita: sim' : 'Escrita: nao')
        } else
        {
            println '- iucn.dir = NÃO DEFINIDO'
        }


        if ( ! grailsApplication?.config?.anexos?.publicacao?.dir )
        {
            grailsApplication?.config?.anexos?.publicacao?.dir = grailsApplication.config.anexos.dir //.replaceAll(/\/salve\//,'/sintax/');
        }
        String dirAnexosPublicacao = grailsApplication.config.anexos.publicacao.dir;
        if (dirAnexosPublicacao)
        {
            file = new File(dirAnexosPublicacao);
            if (!file.exists() && !file.mkdirs())
            {
                println 'Nao foi possível criar o diretório: ' + (dirAnexosPublicacao ?: '/data/salve-estadual/anexos')
                grailsApplication?.config?.anexos?.publicacao?.dir = grailsApplication.config.anexos.dir
            }
        }
        if (grailsApplication?.config?.anexos?.publicacao?.dir)
        {
            println '- anexos.publicacao.dir = ' + grailsApplication?.config?.anexos?.publicacao?.dir + '  ' + (canReadDir(grailsApplication?.config?.anexos?.publicacao?.dir) ? 'Leitura: sim' : 'Leitura: nao') + '  ' + (canWriteDir(grailsApplication?.config?.anexos?.publicacao?.dir) ? 'Escrita: sim' : 'Escrita: nao')
        } else
        {
            println '- anexos.dir = NÃO DEFINIDO'
        }

        if (grailsApplication?.config?.planilhas?.dir)
        {
            println '- planilhas.dir = ' + grailsApplication?.config?.planilhas?.dir + '  ' + (canReadDir(grailsApplication?.config?.planilhas?.dir) ? 'Leitura: sim' : 'Leitura: nao') + '  ' + (canWriteDir(grailsApplication?.config?.planilhas?.dir) ? 'Escrita: sim' : 'Escrita: nao')
        } else
        {
            println '- planilhas.dir = NÃO DEFINIDO'
        }
        if (grailsApplication?.config?.arquivos?.dir)
        {
            println '- arquivos.dir = ' + grailsApplication?.config?.arquivos?.dir + '  ' + (canReadDir(grailsApplication?.config?.arquivos?.dir) ? 'Leitura: sim' : 'Leitura: nao') + '  ' + (canWriteDir(grailsApplication?.config?.arquivos?.dir) ? 'Escrita: sim' : 'Escrita: nao')
        } else
        {
            println '- arquivos.dir = NÃO DEFINIDO'
        }
        if (grailsApplication?.config?.multimidia?.dir)
        {
            println '- multimidia.dir = ' + grailsApplication?.config?.multimidia?.dir + '  ' + (canReadDir(grailsApplication?.config?.arquivos?.dir) ? 'Leitura: sim' : 'Leitura: nao') + '  ' + (canWriteDir(grailsApplication?.config?.multimidia?.dir) ? 'Escrita: sim' : 'Escrita: nao')
        } else
        {
            println '- multimidia.dir = NÃO DEFINIDO'
        }
        if (grailsApplication?.config?.tomcat?.dir)
        {
            println '- tomcat.dir = ' + grailsApplication?.config?.tomcat.dir
        } else
        {
            println '- tomcat.dir = NÃO DEFINIDO'
        }
        println '-' * 100
        println ' '
    }

	def destroy = {
	}


    def clearUserJobs(){
        println ' '
        println 'Jobs'
        println 'Excluindo jobs nao finalizados'
        UserJobs.list().each{ job->
            if( job.stCancelado || job.stExecutado ) {
                println ' - job '+job.id + ' ' + job?.deRotulo
                job.delete()
            }
        }
    }

	def configProxy()
	{
		println 'Detectando proxy...';
		if( grailsApplication?.config?.proxy?.host )
		{
            System.setProperty("http.proxyHost", grailsApplication?.config?.proxy?.host);
            System.setProperty("http.proxyPort", grailsApplication?.config?.proxy?.port);
            if( grailsApplication?.config?.proxy?.user)
            {
                System.setProperty("http.proxyUser",grailsApplication.config.proxy.user);
            }
            if( grailsApplication?.config?.proxy?.password)
            {
                System.setProperty("http.proxyPassword",grailsApplication.config.proxy.password);
            }
		}


    	if( System.getProperty("http.proxyHost" ) )
    	{
    		// ler as configurações definidas para o proxy
			if( System.getProperty("http.proxyUser")  )
			{
		        def encoded = new String( System.getProperty("http.proxyUser") + ':' + System.getProperty("http.proxyPassword") ).bytes.encodeBase64().toString();
				System.setProperty("encoded",encoded);
			}
			println "Utilizando Proxy"
	 	    println "- Proxy host:" + System.getProperty("http.proxyHost");
		    println "- Proxy port:" + System.getProperty("http.proxyPort");
		    println "- Proxy user:" + System.getProperty("http.proxyUser");
		    println "- Proxy password:" + System.getProperty("http.proxyPassword").toString().substring(0,3)+('*'*10)
		}
		else
		{
			println 'Sem Proxy'
		}
	}

	boolean canReadDir( String dir)
	{
		try {
			if( !dir )
			{
				return true
			}
			File file;
			//testar o acesso
			file = new File(dir)
			if( !file || ! file.isDirectory() )
			{
				return false;
			}
			return true;
		} catch ( Exception e ) {
			return false;
		}
	}

	boolean canWriteDir( String dir)
	{
		try {
			if( !dir )
			{
				return true
			}
			File file;
			//testar o acesso
			file = new File(dir)
			if( !file || ! file.isDirectory() )
			{
				return false;
			}
			// testar escrita
	        file = new File(dir+'test.txt')
	      	file.write "Teste de acesso SALVE\n"
	      	//file.delete()
      		return true;
      	} catch( Exception e ) {
      		return false;
      	}
	}

    /**
     * criar registros iniciais nas tabelas
     * @return
     */
    def initDb(){

        // tem que exisitir o ciclo id=2
        CicloAvaliacao ciclo = CicloAvaliacao.get(2)
        if( ! ciclo ){
            println ' '
            println 'Criado o CICLO 2'
            // usuar sqlService para forçar o id=2
            sqlService.execSql("""insert into salve.ciclo_avaliacao(sq_ciclo_avaliacao,nu_ano,de_ciclo_avaliacao,in_situacao,in_oficina,in_publico) values(2,2016,'Ciclo de Avaliação','A','S','S')""");
        }

        DadosApoio tbTipoInstituicao = DadosApoio.findByCodigoSistema('TB_TIPO_INSTITUICAO')
        if (! tbTipoInstituicao )
        {
            tbTipoInstituicao = new DadosApoio(codigo: 'TB_TIPO_INSTITUICAO', codigoSistema: 'TB_TIPO_INSTITUICAO', descricao: 'Tabela de tipos de instituição');
            tbTipoInstituicao.save(flush: true)
            new DadosApoio(codigo: 'UNIDADE_ORGANIZACIONAL', codigoSistema: 'UNIDADE_ORGANIZACIONAL', descricao: 'Unidade Organizacional', pai: tbTipoInstituicao).save(flush: true);
        }
        DadosApoio tipoUnidadeOrg = DadosApoio.findByCodigoAndPai('UNIDADE_ORGANIZACIONAL',tbTipoInstituicao)
        if (! tipoUnidadeOrg ) {
            tipoUnidadeOrg = new DadosApoio()
            tipoUnidadeOrg.pai = tipoUnidadeOrg
            tipoUnidadeOrg.descricao = 'Unidade Organizacional'
            tipoUnidadeOrg.codigo = 'UNIDADE_ORGANIZACIONAL'
            tipoUnidadeOrg.codigoSistema = 'UNIDADE_ORGANIZACIONAL'
            tipoUnidadeOrg.save(flush: true)
        }
        // criar a instituição MMA
        if( Instituicao.count() == 0  ) {
            Instituicao mma = Instituicao.findBySgInstituicao('MMA')
            if (! mma ) {
                Pessoa p = new Pessoa(noPessoa:'Ministério do Meio Ambiente')
                new Instituicao(pessoa:p,sgInstituicao: 'MMA',tipo:tipoUnidadeOrg)
            }

            Instituicao icmbio = Instituicao.findBySgInstituicaoIlike('ICMBio')
            if (! icmbio ) {
                Pessoa p = new Pessoa(noPessoa:'Instituto Chico Mendes de Conservação da Biodiversidade')
                new Instituicao(pessoa:p, pai: mma, sgInstituicao: 'IBMBio',tipo:tipoUnidadeOrg, nuCnpj: '08829974000194')
            }

            Instituicao ibama = Instituicao.findBySgInstituicaoIlike('IBAMA')
            if (! ibama ) {
                Pessoa p = new Pessoa(noPessoa:'Instituto Brasileiro do Meio Ambiente e dos Recursos Naturais Renováveis')
                new Instituicao(pessoa:p, pai: mma, sgInstituicao: 'IBAMA',tipo:tipoUnidadeOrg, nuCnpj: '03659166000102')
            }

        }

        DadosApoio tbSituacaoFicha = DadosApoio.findByCodigoSistema('TB_SITUACAO_FICHA')
        if (! tbSituacaoFicha )
        {
            tbSituacaoFicha = new DadosApoio(codigo: 'TB_SITUACAO_FICHA', codigoSistema: 'TB_SITUACAO_FICHA', descricao: 'Tabela de situações das fichas de espécies');
            tbSituacaoFicha.save(flush: true)
            new DadosApoio(codigo: 'COMPILACAO', codigoSistema: 'COMPILACAO', descricao: 'Compilação', pai: tbSituacaoFicha, ordem: 'a').save(flush: true);
        }

        DadosApoio tbEsfera = DadosApoio.findByCodigoSistema('TB_ESFERA_ADMINISTRATIVA')
        if (! tbEsfera )
        {
            tbEsfera = new DadosApoio(codigo: 'TB_ESFERA_ADMINISTRATIVA', codigoSistema: 'TB_ESFERA_ADMINISTRATIVA', descricao: 'Tabela de esferas adminstrativas');
            tbEsfera.save(flush: true)
            new DadosApoio(codigo: 'FEDERAL', codigoSistema: 'FEDERAL', descricao: 'Federal', pai: tbEsfera, ordem: 'a').save(flush: true);
            new DadosApoio(codigo: 'ESTADUAL', codigoSistema: 'ESTADUAL', descricao: 'Estadual', pai: tbEsfera, ordem: 'b').save(flush: true);
            new DadosApoio(codigo: 'MUNICIPAL', codigoSistema: 'MUNICIPIAL', descricao: 'Municipal', pai: tbEsfera, ordem: 'c').save(flush: true);
        }

        DadosApoio tbTipoUc = DadosApoio.findByCodigoSistema('TB_TIPO_UC')
        if (! tbTipoUc )
        {
            tbTipoUc = new DadosApoio(codigo: 'TB_TIPO_UC', codigoSistema: 'TB_TIPO_UC', descricao: 'Tabela de tipos de Unidades de Conservação');
            tbTipoUc.save(flush: true)
            new DadosApoio(codigo: 'ESEC'     ,codigoSistema: 'ESTACAO_ECOLOGICA'   ,descricao:'Estação Ecológica',pai:tbTipoUc,ordem:'a').save(flush:true);
            new DadosApoio(codigo: 'Floresta' ,codigoSistema: 'FLORESTA_ESTADUAL'   ,descricao:'Floresta Estadual',pai:tbTipoUc,ordem:'b').save(flush:true);
            new DadosApoio(codigo: 'FLONA'    ,codigoSistema: 'FLORESTA_NACIONAL'   ,descricao:'Floresta Nacional',pai:tbTipoUc,ordem:'c').save(flush:true);
            new DadosApoio(codigo: 'MN'       ,codigoSistema: 'MONUMENTO_NATURAL'   ,descricao:'Monumento Natural',pai:tbTipoUc,ordem:'d').save(flush:true);
            new DadosApoio(codigo: 'PEM'      ,codigoSistema: 'PARQUE_ESTADUAL'     ,descricao:'Parque Estadual',pai:tbTipoUc,ordem:'e').save(flush:true);
            new DadosApoio(codigo: 'PARNA'    ,codigoSistema: 'PARQUE_NACIONAL'         ,descricao:'Parque Nacional',pai:tbTipoUc,ordem:'f').save(flush:true);
            new DadosApoio(codigo: 'REVIS'    ,codigoSistema: 'REFIGIO_VIDA_SILVESTRE'  ,descricao:'Refúgio de Vida Silvestre',pai:tbTipoUc,ordem:'g').save(flush:true);
            new DadosApoio(codigo: 'REBIO'    ,codigoSistema: 'RESERVA_BIOLOGICA'       ,descricao:'Reserva Biológica',pai:tbTipoUc,ordem:'h').save(flush:true);
            new DadosApoio(codigo: 'RDS'      ,codigoSistema: 'RESERVA_DESENVOLVIMENTO_SUSTENTAVEL',descricao:'Reserva de Desenvolvimento Sustentável',pai:tbTipoUc,ordem:'i').save(flush:true);
            new DadosApoio(codigo: 'RESEX'    ,codigoSistema: 'RESERVA_EXTRATIVISTA'    ,descricao:'Reserva Extrativista',pai:tbTipoUc,ordem:'j').save(flush:true);
            new DadosApoio(codigo: 'RPPN'     ,codigoSistema: 'RESERVA_PARTICULAR_PATRIMONIO_NATURAL',descricao:'Reserva Particular do Patrimônio Natural',pai:tbTipoUc,ordem:'k').save(flush:true);
            new DadosApoio(codigo: 'APA'      ,codigoSistema: 'AREA_PROTECAO_AMBIENTAL' ,descricao:'Área de Proteção Ambiental',pai:tbTipoUc,ordem:'l').save(flush:true);
            new DadosApoio(codigo: 'ARIE'     ,codigoSistema: 'AREA_RELEVANTE_INTERESSE_ECONOMICO',descricao:'Área de Relevante Interesse Ecológico',pai:tbTipoUc,ordem:'m').save(flush:true);
        }


        if ( ! DadosApoio.findByPaiAndCodigoSistema(tbSituacaoFicha,'CONSULTA'))
        {
            new DadosApoio(codigo: 'CONSULTA', codigoSistema: 'CONSULTA', descricao: 'Consulta', pai: tbSituacaoFicha, ordem: 'b').save(flush: true);
        }

        if ( ! DadosApoio.findByPaiAndCodigoSistema(tbSituacaoFicha,'CONSULTA_FINALIZADA'))
        {
            new DadosApoio(codigo: 'CONSULTA_FINALIZADA', codigoSistema: 'CONSULTA_FINALIZADA', descricao: 'Consulta Finalizada', pai: tbSituacaoFicha, ordem: 'c').save(flush: true);
        }

        DadosApoio tbTiposSincronismo = DadosApoio.findByCodigoSistema('TB_TIPO_SINCRONISMO')
        if (! tbTiposSincronismo )
        {
            tbTiposSincronismo = new DadosApoio(codigo: 'TB_TIPO_SINCRONISMO', codigoSistema: 'TB_TIPO_SINCRONISMO', descricao: 'Tabela de tipos de sincronismos');
            tbTiposSincronismo.save(flush: true)
        }
        if ( ! DadosApoio.findByPaiAndCodigoSistema(tbTiposSincronismo,'SINCRONISMO_REGISTRO_OCORRENCIA')) {
            new DadosApoio(codigo: 'SINCRONISMO_REGISTRO_OCORRENCIA', codigoSistema: 'SINCRONISMO_REGISTRO_OCORRENCIA', descricao: 'Sincronismo de Registros de Ocorrências', pai: tbTiposSincronismo).save(flush: true);
        }
        if ( ! DadosApoio.findByPaiAndCodigoSistema(tbTiposSincronismo,'SINCRONISMO_CITES')) {
            new DadosApoio(codigo: 'SINCRONISMO_CITES', codigoSistema: 'SINCRONISMO_CITES', descricao: 'Sincronismo CITES', pai: tbTiposSincronismo).save(flush: true);
        }
        if ( ! DadosApoio.findByPaiAndCodigoSistema(tbTiposSincronismo,'SINCRONISMO_HISTORICAO_AVALIACAO')) {
            new DadosApoio(codigo: 'SINCRONISMO_HISTORICO_AVALIACAO', codigoSistema: 'SINCRONISMO_HISTORICO_AVALIACAO', descricao: 'Sincronismo histórico de avaliação', pai: tbTiposSincronismo).save(flush: true);
        }


        if ( ! DadosApoio.findByPaiAndCodigoSistema(tbSituacaoFicha,'CONSULTA'))
        {
            new DadosApoio(codigo: 'CONSULTA', codigoSistema: 'CONSULTA', descricao: 'Consulta', pai: tbSituacaoFicha, ordem: 'b').save(flush: true);
        }

        // criar contexto versionamento ficha
        DadosApoio tbContextoVersionamento = DadosApoio.findByCodigoSistema('TB_CONTEXTO_VERSAO_FICHA')
        if (! tbContextoVersionamento )
        {
            tbContextoVersionamento = new DadosApoio(codigo: 'TB_CONTEXTO_VERSAO_FICHA', codigoSistema: 'TB_CONTEXTO_VERSAO_FICHA', descricao: 'Tabela de tipos de sincronismos')
            tbContextoVersionamento.save(flush: true)
        }
        if ( ! DadosApoio.findByPaiAndCodigoSistema(tbContextoVersionamento,'VERSAO_PUBLICACAO')) {
            new DadosApoio(codigo: 'VERSAO_PUBLICACAO', codigoSistema: 'VERSAO_PUBLICACAO', descricao: 'Publicação da ficha', pai: tbContextoVersionamento).save(flush: true)
        }

        if ( ! DadosApoio.findByPaiAndCodigoSistema(tbContextoVersionamento,'VERSAO_VALIDACAO')) {
            new DadosApoio(codigo: 'VERSAO_VALIDACAO', codigoSistema: 'VERSAO_VALIDACAO', descricao: 'Versão provisória', pai: tbContextoVersionamento).save(flush: true)
        }

        // criar os perfis basicos
        List perfisSalve = [
                [noPerfil:'Administrador',cdSistema:'ADM'],
                [noPerfil:'Ponto Focal',cdSistema:'PONTO_FOCAL'],
                [noPerfil:'Coordenador de Taxon',cdSistema:'COORDENADOR_TAXON'],
                [noPerfil:'Colaborador',cdSistema:'COLABORADOR'],
                [noPerfil:'Colaborador Eventual',cdSistema:'COLABORADOR_EVENTUAL'],
                [noPerfil:'Consulta',cdSistema:'CONSULTA'],
                [noPerfil:'Validador',cdSistema:'VALIDADOR'],
                [noPerfil:'Tradutor',cdSistema:'TRADUTOR'],
        ]
        perfisSalve.each{perfil ->
            if( !Perfil.findByCdSistema( perfil.cdSistema) ){
                new Perfil( perfil ).save(flush:true)
                println 'Perfil '+ perfil.cdSistema +' adicionado na tabela perfil';
            }
        }
        if( Usuario.count() > 0 ){
            return
        }
        Pessoa p = Pessoa.findByNoPessoa('Administrador')
        if( !p ) {
            p = new Pessoa()
            p.noPessoa  = 'Administrador'
            p.deEmail   = 'admin@salve-estadual'
            p.save(flush: true)
            println 'Pessoa Administrador criada com email admin@salve-estadual'
        }

        PessoaFisica pf = PessoaFisica.get( p.id )
        if( !pf ){
            pf = new PessoaFisica()
            pf.pessoa = p
            pf.nuCpf = '0'*11
            pf.save(flush:true)
            println 'CPF ' + pf.getCpfFormatado()
        }

        Usuario u = Usuario.get( pf.id )
        if(!u){
            u = new Usuario()
            u.pessoaFisica = pf
            u.deSenha = Util.md5('123456')
            u.stAtivo=true
            u.save( flush:true)
            println 'Usuario Administrador (admin@salve-estadual) criado. Senha de acesso: 123456'
        }

        Perfil adm = Perfil.findByCdSistema('ADM')
        UsuarioPerfil usuarioPerfil = UsuarioPerfil.findByUsuarioAndPerfil(u,adm);
        println ' '
        println ' '
        println ' '
        println 'Usuario perfil:'+usuarioPerfil
        println 'u:' + u
        println 'perfil:'+adm
        if( ! usuarioPerfil ){
            println 'Criando....'
            new UsuarioPerfil(usuario:u, perfil:adm).save( flush:true)
            println 'Perfil ADM atribuido ao Administrador'
        }

    }

}
