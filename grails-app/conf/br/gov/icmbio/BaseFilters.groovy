package br.gov.icmbio

import org.apache.catalina.core.ApplicationHttpRequest
import org.springframework.web.context.request.RequestContextHolder
import org.springframework.web.context.request.ServletRequestAttributes
import org.springframework.web.multipart.commons.CommonsMultipartFile
import grails.converters.JSON
import grails.util.Environment

//http://www.itexto.net/devkico/?p=29
class BaseFilters {
    def filters = {
        // gravar na tabela trilha_auditoria as ações que alteram dados
        all(controller: '*', action: '*') {
            before = {
                /*if( response ) {
                    response.setHeader("Cache-Control", "public,max-age=604800");
                }*/
                boolean validRequest = (request != null && params && params?.action && params?.controller && session && session?.sicae?.user?.noUsuario)

                if ( ! validRequest ) {
                    return
                }

                // lista de action que não precisam ser gravadas da tabela de auditoria
                List invalidActions = ['updateUserJob', 'javascript', 'dashboard', 'ping']
                if (params.action.toString() =~ /(?i)worker|(^get)/) {
                    validRequest = false
                } else {
                    if (invalidActions.contains(params.action)) {
                        validRequest = false
                    }
                }

                if (!validRequest) {
                    return
                }
                Map paramsLog = [:]

                if ((params.action ==~ /^(alt|cad|adic|add|ins|update|change|set|grav|exc|salv|save|del|map|rel[0-9]|trad|gerar|toggle).*/)) {
/*
println 'BaseFilters '
println 'Request Class ' + request.getClass()
println '- '*50
println ' '
*/
// org.apache.catalina.core.ApplicationHttpRequest
// org.springframework.web.multipart.support.DefaultMultipartHttpServletRequest


                    params.each {
                        try {
                            if (it?.key != 'controller' && it?.key != 'action') {
                                if (request && it.value && (it.value.getClass() == CommonsMultipartFile)) {
                                    CommonsMultipartFile f = request.getFile(it.key)
                                    if (f && !f.empty) {
                                        paramsLog[it.key] = f.getOriginalFilename() + ', size:'
                                        f.getSize().toString() + 'kb, type: ' + f.getContentType()
                                    }
                                } else {
                                    paramsLog[it.key] = it.value.toString()
                                }
                            }
                        } catch (Exception e) {
                        }
                    }
                    try {

                        // não gravar log se estiver no banco de dados - READONLY (cópia da produção)
                        return;
                        if (! ( grailsApplication?.config?.dataSource?.url ==~ /.*32\.111.*/) ) {
                            TrilhaAuditoria ta = new TrilhaAuditoria()
                            ta.dtLog = new Date()
                            try {
                                ta.deIp = getRequestIp(request)
                            } catch (Exception e) {
                                ta.deIp = ''
                            }
                            ta.deEnv = Environment.current.toString()
                            ta.noPessoa = session.sicae.user.noUsuario
                            ta.nuCpf = session.sicae.user.nuCpf
                            ta.noModulo = params.controller
                            ta.noAcao = params.action
                            ta.txParametros = (paramsLog as JSON).toString().trim()
                            ta.save()
                        }
                    } catch (Exception e) {
                        log.info(e.getMessage())
                    }
                }
            }
            after = { Map model ->
            }
            afterView = { Exception e ->
            }
        }
    }

    /**
     * Identiricar o ip do usuário
     * @return
     */
    private String getRequestIp( javax.servlet.http.HttpServletRequestWrapper request) {
        String ipAddress = ''
        try {
            if (request) {
                ipAddress = request.getHeader("Client-IP")
                if (!ipAddress) {
                    ipAddress = request.getHeader("X-Forwarded-For")
                } else if (!ipAddress) {
                    ipAddress = request.remoteAddr
                } else if (!ipAddress) {
                    ipAddress = request.getRemoteAddr();
                }
            }
            if (!ipAddress) {
                ipAddress = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getRemoteAddr();
            }
            ipAddress = (ipAddress == '0:0:0:0:0:0:0:1') ? '127.0.0.1' : ipAddress
            ipAddress = (ipAddress == 'localhost' ? '127.0.0.1': ipAddress )
            return ipAddress
        } catch ( Exception e )
        {
            return '0'
        }
    }
}
