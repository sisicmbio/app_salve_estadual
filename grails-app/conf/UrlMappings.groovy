// da erro esta url http://localhost:8080/salve-estadual/?sqFichaPopulEstimadaLocal=&dsMesAnoInicio=&dsMesAnoFim=&vlAbundancia=&sqUnidAbundanciaPopulacao=&dsLocal=&dsEstimativaPopulacao=
class UrlMappings {

	static mappings = {
		/*"/$controller/$action?/$id?(.$format)?"{
		 constraints {
		 // apply constraints here
		 }
		 }
		*/

        // ROTAS - SALVE-ESTADUAL
        // ---------------------------------------------------------------------
        "/"(controller: 'login', action: 'index')


        // CONFIGURACAO DO SISTEMA
        "/configuracao"(controller: 'configuracao', action: 'index')
        "/configuracao/save"(controller: 'configuracao', action: 'save')


        //LOGIN/LOGOUT
        "/logout"(controller: 'login', action: 'logout')
        "/login"(controller: 'login', action: 'login')

        "/login/login"(controller: 'login', action: 'login')
        "/login/selectPerfil"(controller: 'login', action: 'selectPerfil')
        "/login/selectPerfilInstituicao"(controller: 'login', action: 'selectPerfilInstituicao')
        "/login/loginPerfil"(controller: 'login', action: 'loginPerfil')
        "/login/logout"(controller: 'login', action: 'logout')
        "/login/forgotPasswordForm"(controller: 'login', action: 'forgotPasswordForm')
        "/login/sendResetPasswordMail"(controller: 'login', action: 'sendResetPasswordMail')
        "/login/changePasswordForm/$hash"(controller: 'login', action: 'changePasswordForm')
        "/login/changePassword"(controller: 'login', action: 'changePassword')





        // Corporativo USUARIO
        "/corpUsuario"(controller: 'corpUsuario', action: 'index')
        "/corpUsuario/save"(controller: 'corpUsuario', action: 'save')
        "/corpUsuario/edit"(controller: 'corpUsuario', action: 'edit')
        "/corpUsuario/delete"(controller: 'corpUsuario', action: 'delete')
        "/corpUsuario/grid"(controller: 'corpUsuario', action: 'grid')
        "/corpUsuario/consultarCpf"(controller: 'corpUsuario', action: 'consultarCpf')
        "/corpUsuario/consultarCpf"(controller: 'corpUsuario', action: 'consultarCpf')
        "/corpUsuario/consultarEmail"(controller: 'corpUsuario', action: 'consultarEmail')

        // Corporativo USUARIO-PERFIL
        "/corpUsuarioPerfil"(controller: 'corpUsuarioPerfil', action: 'index')
        "/corpUsuarioPerfil/save"(controller: 'corpUsuarioPerfil', action: 'save')
        "/corpUsuarioPerfil/edit"(controller: 'corpUsuarioPerfil', action: 'edit')
        "/corpUsuarioPerfil/delete"(controller: 'corpUsuarioPerfil', action: 'delete')
        "/corpUsuarioPerfil/ulPerfilInstituicao"(controller: 'corpUsuarioPerfil', action: 'ulPerfilInstituicao')
        "/corpUsuarioPerfil/select2Instituicao"(controller: 'corpUsuarioPerfil', action: 'select2Instituicao')



        // Corporativo PAIS
        "/corpPais"(controller: 'corpPais', action: 'index')
        "/corpPais/save"(controller: 'corpPais', action: 'save')
        "/corpPais/edit"(controller: 'corpPais', action: 'edit')
        "/corpPais/delete"(controller: 'corpPais', action: 'delete')
        "/corpPais/grid"(controller: 'corpPais', action: 'grid')

        // Corporativo ESTADO
        "/corpEstado"(controller: 'corpEstado', action: 'index')
        "/corpEstado/save"(controller: 'corpEstado', action: 'save')
        "/corpEstado/edit"(controller: 'corpEstado', action: 'edit')
        "/corpEstado/delete"(controller: 'corpEstado', action: 'delete')
        "/corpEstado/grid"(controller: 'corpEstado', action: 'grid')

        // Corporativo MUNICIPIO
        "/corpMunicipio"(controller: 'corpMunicipio', action: 'index')
        "/corpMunicipio/save"(controller: 'corpMunicipio', action: 'save')
        "/corpMunicipio/edit"(controller: 'corpMunicipio', action: 'edit')
        "/corpMunicipio/delete"(controller: 'corpMunicipio', action: 'delete')
        "/corpMunicipio/grid"(controller: 'corpMunicipio', action: 'grid')

        // Corporativo REGIAO
        "/corpRegiao"(controller: 'corpRegiao', action: 'index')
        "/corpRegiao/save"(controller: 'corpRegiao', action: 'save')
        "/corpRegiao/edit"(controller: 'corpRegiao', action: 'edit')
        "/corpRegiao/delete"(controller: 'corpRegiao', action: 'delete')
        "/corpRegiao/grid"(controller: 'corpRegiao', action: 'grid')

        // Corporativo BIOMA
        "/corpBioma"(controller: 'corpBioma', action: 'index')
        "/corpBioma/save"(controller: 'corpBioma', action: 'save')
        "/corpBioma/edit"(controller: 'corpBioma', action: 'edit')
        "/corpBioma/delete"(controller: 'corpBioma', action: 'delete')
        "/corpBioma/grid"(controller: 'corpBioma', action: 'grid')

        // Corporativo UC
        "/corpUc"(controller: 'corpUc', action: 'index')
        "/corpUc/save"(controller: 'corpUc', action: 'save')
        "/corpUc/edit"(controller: 'corpUc', action: 'edit')
        "/corpUc/delete"(controller: 'corpUc', action: 'delete')
        "/corpUc/grid"(controller: 'corpUc', action: 'grid')

        // Corporativo Unidade Organizacional
        "/corpEstruturaOrg"(controller: 'corpEstruturaOrg', action: 'index')
        "/corpEstruturaOrg/save"(controller: 'corpEstruturaOrg', action: 'save')
        "/corpEstruturaOrg/edit"(controller: 'corpEstruturaOrg', action: 'edit')
        "/corpEstruturaOrg/delete"(controller: 'corpEstruturaOrg', action: 'delete')
        "/corpEstruturaOrg/treeview/$id?"(controller: 'corpEstruturaOrg', action: 'treeview')




        //-------------------------------------------------------

		"/alterarPerfil"(controller:'main',action:'alterarPerfil')
		"/p/$p?/$sgUnidade?/$nuCpf?"(controller:'main',action:'simularLogin')
		"/limparTempDir/$dias?"(controller:'main',action:'limparTempDir')
		"/validarDoc/$codigoAcesso?"(controller:'api',action:'validarDoc')
		"/validardoc/$codigoAcesso?"(controller:'api',action:'validarDoc')
		"/validar-doc/$codigoAcesso?"(controller:'api',action:'validarDoc')

        "/dbManager"(controller:'dbManager', action:'index')

        "/api"(controller:'api', action:'index')
		"/api/respostaConvite/$hash/$resposta"(controller:'api', action:'respostaConvite')
		"/api/fichaEspecie/$noCientifico?"(controller:'api', action:'fichaEspecie')
		"/api/fichaHash/$hash?"(controller:'api', action:'fichaHash')
		"/api/imagem/$sqFicha/$tipo"(controller:'api', action:'imagem')
		"/api/ocorrencias/$webUserId/$sqFicha?/$formato?/$bd?/$cor?/$utilizadas?/$excluidas?/$layerId?"(controller:'api', action:'ocorrencias')
		"/api/ocorrenciasModuloPublico/$webUserId/$sqFicha?/$formato?/$bd?/$cor?/$utilizadas?/$excluidas?/$layerId?"(controller:'api', action:'ocorrenciasModuloPublico')
		"/api/camadasOcorrenciasModuloPublico/$sqFicha?"(controller:'api', action:'camadasOcorrenciasModuloPublico')
		"/api/layer/aooFicha/$sqFicha"(controller:'api', action:'aooFicha')
		"/api/layer/eooFicha/$sqFicha"(controller:'api', action:'eooFicha')
		"/api/layer/biomas"(controller:'api', action:'getLayerBiomas')
		"/api/exportOccurrencesDwca/$sqCicloAvaliacao?"(controller:'api', action:'exportOccurrencesDwca')
		"/api/searchPortalbio/$noCientifico"(controller:'api', action:'searchPortalbio')
		"/api/searchRefBib/$p?/$ano?/$campo?"(controller:'api', action:'searchRefBib')
		"/api/shapefile/$name?"(controller:'api', action:'shapefile')
		"/api/assinarDocFinalOficina/$hash?"(controller:'api', action:'assinarDocFinalOficina')
		"/api/getTableApoio"(controller:'api', action:'getTableApoio')
		"/api/getLayer"(controller:'api', action:'getLayer')
        "/api/camadasOcorrencias"(controller:'api', action:'camadasOcorrencias')
        "/api/estadosCentroide"(controller:'api', action:'estadosCentroide')
        "/api/equipeAutorizacaoSisbio"(controller:'api', action:'equipeAutorizacaoSisbio')


        // novos metodos para criar os pontos no mapa só com informações basicas
		"/api/getLayerOcorrenciasSalve"(controller:'api', action:'getLayerOcorrenciasSalve')
		"/api/getLayerOcorrenciasSalveConsulta"(controller:'api', action:'getLayerOcorrenciasSalveConsulta')
        // portalbio
        "/api/getLayerOcorrenciasPortalbio"(controller:'api', action:'getLayerOcorrenciasPortalbio')

		"/api/getInfoOcorrencia"(controller:'api', action:'getInfoOcorrencia')
		"/api/translate"(controller:'api', action:'translate')

		"/api/historicoAvaliacao"(controller:'api', action:'getHistoricoAvaliacao')
		"/api/layerOcorrencias"(controller:'api', action:'layerOcorrencias')

        // FICHA VERSÕES EM HTML
        "/fichaVersao"(controller:'fichaVersao', action:'index')
        "/fichaVersao/fundamentacaoVersao"(controller:'fichaVersao', action:'fundamentacaoVersao')
        "/fichaVersao/updateJson"(controller:'fichaVersao', action:'updateJson')
        "/fichaVersao/getGridOcorrencias"(controller:'fichaVersao', action:'getGridOcorrencias')

        // AÇÕES DO CADASTRO DE INFORMATIVOS
        "/informativo/save"(controller:'informativo', action:'save')
        "/informativo/saveAlerta"(controller:'informativo', action:'saveAlerta')
        "/informativo/getGrid"(controller:'informativo', action:'getGrid')
        "/informativo/getGridAlerta"(controller:'informativo', action:'getGridAlerta')
        "/informativo/edit"(controller:'informativo', action:'edit')
        "/informativo/editAlerta"(controller:'informativo', action:'editAlerta')
        "/informativo/delete"(controller:'informativo', action:'delete')
        "/informativo/deleteAlerta"(controller:'informativo', action:'deleteAlerta')
        "/informativo/calcularProximoNumero"(controller:'informativo', action:'calcularProximoNumero')
        //"/informativo/show"(controller:'informativo', action:'show')
        "/informativo/getInformativoAlerta"(controller:'informativo', action:'getInformativoAlerta')
        "/informativo/showPessoas"(controller:'informativo', action:'showPessoas')
        "/informativo/setRead"(controller:'informativo', action:'setRead')

        //"/"(view:"/index")
        "/javascript/$module"( controller:"main",action:'javascript' )
        "/javascript/$module?/$submodule?"( controller:"main",action:'javascript' )


        "/main/ping"( controller:"main", action:"ping")
        "/main/saveWkt"( controller:"main", action:"saveWkt")
        "/main/restoreSession"( controller:"main", action:"restoreSession")
        "/main/saveEditableLabels"( controller:"main", action:"saveEditableLabels")

        "/main/menu"( controller:"main", action:"menu")
        "/main/getModal"( controller:"main", action:"getModal")
        "/main/getGrid"( controller:"main", action:"getGrid")
        "/main/savePlanilhaFicha"( controller:"main", action:"savePlanilhaFicha")
        "/main/savePlanilhaRefBib"( controller:"main", action:"savePlanilhaRefBib")
        "/main/deletePlanilha"( controller:"main", action:"deletePlanilha")
        "/main/download"( controller:"main", action:"download")
        "/download"( controller:"main", action:"download")
        "/main/getLog/$qtd?"( controller:"main", action:"getLog")
		"/main/importarEspecies/$historico?"(controller: "main", action:"importarEspecies")
		"/main/updateManual"(controller: "main", action:"updateManual")
		"/main/updateArquivoSistema"(controller: "main", action:"updateArquivoSistema")
		"/main/select2Unidade"(controller: "main", action:"select2Unidade")
		"/main/select2Instituicao"(controller: "main", action:"select2Instituicao")
		"/main/select2WebInstituicao"(controller: "main", action:"select2WebInstituicao")

		"/main/getAndamentoImportacaoPlanilhas"(controller: "main", action:"getAndamentoImportacaoPlanilhas")
		"/main/verificarTerminoConsultas"(controller: "main", action:"verificarTerminoConsultas")
		"/main/dashboard"(controller: "main", action:"dashboard")
		"/main/clearCache/$key?"(controller: "main", action:"clearCache")
		"/main/listCache"(controller: "main", action:"listCache")
		"/main/getWorkers"(controller: "main", action:"getWorkers")
		"/main/deleteWorker/$id?"(controller: "main", action:"deleteWorker")
		"/main/deleteWorker/$id?"(controller: "main", action:"deleteWorker")
		"/main/gc/"(controller: "main", action:"gc")
		"/main/mem/"(controller: "main", action:"mem")
		"/main/getText"(controller:"main", action:"getText")
		"/main/htmlSanitizer"(controller:"main", action:"htmlSanitizer")
		"/main/select2Categoria"(controller:"main", action:"select2Categoria")
        "/main/deleteAllUserWorkers"(controller: "main", action:"deleteAllUserWorkers")
        "/main/deleteUserJob"(controller: "main", action:"cancelUserJob")
        "/main/getUserJobs"(controller: "main", action:"getUserJobs")
        "/main/updateUserJob"(controller: "main", action:"updateUserJob")


		"/seguranca/usuario"(controller: "seguranca", action:"usuario")
        "/getLog/$qtd?"( controller:"main", action:"getLog")
        "/cancelarTarefa/$idTarefa?"(controller: "main", action:"cancelWorker")

        "500"(view:'/error')

        "/login"(controller:'main',action:'login');
		"/logout"(controller:'main',action:'logout');

		"/testes"(controller: "testes", action:"index")
		"/teste"(controller: "testes", action:"teste")
		"/testes/zipar"(controller: "testes", action:"zipar")
		"/testes/coords"(controller: "testes", action:"coords")
		"/testes/pdf"(controller: "testes", action:"pdf")
        "/testes/timeout/$sleep?"(controller: "testes", action:"timeout")
		"/testes/cargaAbrangencia"(controller: "testes", action:"cargaAbrangencia")
        "/testes/limparTagWord"(controller: "testes", action:"limparTagWord")

        // RELATÓRIOS
		"/relatorio/rel001"(controller: "relatorio", action:"rel001")
		"/relatorio/rel002"(controller: "relatorio", action:"rel002")
		"/relatorio/rel003"(controller: "relatorio", action:"rel003")
		"/relatorio/rel004"(controller: "relatorio", action:"rel004")
		"/relatorio/rel005"(controller: "relatorio", action:"rel005")
		"/relatorio/rel006"(controller: "relatorio", action:"rel006")
		"/relatorio/getCamposFiltro/"(controller: "relatorio", action:"getCamposFiltro")
		"/relatorio/getOficinaValidacao"(controller: "relatorio", action:"getOficinaValidacao")
		"/relatorio/select2Oficina/"(controller: "relatorio", action:"select2Oficina")
		"/relatorio/select2UnidadeOrg/"(controller: "relatorio", action:"select2UnidadeOrg")
		"/relatorio/select2Subgrupo/"(controller: "relatorio", action:"select2Subgrupo")


		"/portalbio/getOcorrencias"(controller: "portalbio", action:"getOcorrencias")

		"/ficha"(controller: "ficha")
		"/ficha/getGridFicha"(controller: "ficha", action:"getGridFicha")
		"/ficha/getFormFiltroFicha"(controller:"ficha", action:"getFormFiltroFicha")
        "/ficha/select2Subgrupo"(controller:"ficha", action:"select2Subgrupo")
        "/ficha/select2Taxon"(controller:"ficha", action:"select2Taxon")
        "/ficha/select2TaxonNovo"(controller:"ficha", action:"select2TaxonNovo")
        "/ficha/select2TaxonAlteracao"(controller:"ficha", action:"select2TaxonAlteracao")
        "/ficha/getTipoAbrangencia"(controller:"ficha", action:"getTipoAbrangencia")
        "/ficha/getAbrangencias"(controller:"ficha", action:"getAbrangencias") // nova utilizando a tabela de abrangencias
        "/ficha/gerarPdf"(controller:"ficha", action:"gerarPdf")
        "/ficha/gerarZipFichas"(controller:"ficha", action:"gerarZipFichas")
        "/ficha/gerarRtf"(controller:"ficha", action:"gerarRtf")
        "/ficha/exportCsv"(controller:"ficha", action:"exportCsv")
        "/ficha/exportPendencia"(controller:"ficha", action:"exportPendencia")
        "/ficha/select2InstituicaoFiltro"(controller:"ficha", action:"select2InstituicaoFiltro")
        "/ficha/copiarFicha"(controller:"ficha", action:"copiarFicha")
        "/ficha/copiarFicha"(controller:"ficha", action:"copiarFicha")
        "/ficha/getSubespecies"(controller:"ficha", action:"getSubespecies")
        "/ficha/getCentroide"(controller:"ficha", action:"getCentroide")
        "/ficha/getCentroideMunicipio"(controller:"ficha", action:"getCentroideMunicipio")
        "/ficha/getCentroideEstado"(controller:"ficha", action:"getCentroideEstado")
        "/ficha/getUcByCoord"(controller:"ficha", action:"getUcByCoord")
        "/ficha/getAndamentoImportacaoPlanilhas"(controller:"ficha", action:"getAndamentoImportacaoPlanilhas")
        "/ficha/showImagem"(controller:"ficha", action:"showImagem")
        "/ficha/getPontos"(controller:"ficha", action:"getPontos")
        "/ficha/getLastChanged"(controller:"ficha", action:"getLastChanged")
        "/ficha/alterarTaxonFicha"(controller:"ficha", action:"alterarTaxonFicha")
        "/ficha/alterarArvoreTaxonomica"(controller:"ficha", action:"alterarArvoreTaxonomica")
        "/ficha/alterarSituacaoFichaParaExcluida"(controller:"ficha", action:"alterarSituacaoFichaParaExcluida")
        "/ficha/getJustificativaExcluida"(controller:"ficha", action:"getJustificativaExcluida")
        "/ficha/select2FichaCicloAnterior"(controller:"ficha", action:"select2FichaCicloAnterior")
        "/ficha/saveFichaCicloAnterior"(controller:"ficha", action:"saveFichaCicloAnterior")
        "/ficha/select2Ficha"(controller:"ficha", action:"select2Ficha")
        "/ficha/transferirRegistro"(controller:"ficha", action:"transferirRegistro")
        "/ficha/updateSituacaoPendencia"(controller:"ficha", action:"updateSituacaoPendencia")
        "/ficha/getUltimasReferencias"(controller:"ficha", action:"getUltimasReferencias")
        "/ficha/marcarDesmarcarModuloPublico"(controller:"ficha", action:"marcarDesmarcarModuloPublico")



		// metodos da exibição da ficha completa
        "/fichaCompleta"(controller:"fichaCompleta", action:"index")
        "/fichaCompleta/index"(controller:"fichaCompleta", action:"index")
        "/fichaCompleta/getGridOcorrencias"(controller:"fichaCompleta", action:"getGridOcorrencias")
        "/fichaCompleta/updateSituacaoColaboracao"(controller: "fichaCompleta",action:"updateSituacaoColaboracao")
        "/fichaCompleta/getGridHabitoAlimentar"(controller: "fichaCompleta",action:"getGridHabitoAlimentar")
        "/fichaCompleta/getGridHabitat"(controller: "fichaCompleta",action:"getGridHabitat")
        "/fichaCompleta/getGridInteracao"(controller: "fichaCompleta",action:"getGridInteracao")
        "/fichaCompleta/getGridAmeaca"(controller: "fichaCompleta",action:"getGridAmeaca")
        "/fichaCompleta/getGridAnexosAmeaca"(controller: "fichaCompleta",action:"getGridAnexosAmeaca")
        "/fichaCompleta/getGridUso"(controller: "fichaCompleta",action:"getGridUso")
        "/fichaCompleta/getGridAnexosUso"(controller: "fichaCompleta",action:"getGridAnexosUso")
        "/fichaCompleta/getHistoricoAvaliacoes"(controller: "fichaCompleta",action:"getHistoricoAvaliacoes")
        "/fichaCompleta/getGridPesquisa"(controller: "fichaCompleta",action:"getGridPesquisa")
        "/fichaCompleta/getRefBibs"(controller: "fichaCompleta",action:"getRefBibs")
        "/fichaCompleta/getPendencias"(controller: "fichaCompleta",action:"getPendencias")
        "/fichaCompleta/consolidarFicha"(controller: "fichaCompleta",action:"consolidarFicha")
        "/fichaCompleta/saveResultadoAvaliacao"( controller: "fichaCompleta", action: "saveResultadoAvaliacao")
        "/fichaCompleta/saveAjusteRegional"( controller: "fichaCompleta", action: "saveAjusteRegional")
        "/fichaCompleta/addMotivoMudanca"( controller: "fichaCompleta", action: "addMotivoMudanca")
        "/fichaCompleta/getGridMotivoMudanca"( controller: "fichaCompleta", action: "getGridMotivoMudanca")
        "/fichaCompleta/deleteMotivoMudanca"( controller: "fichaCompleta", action: "deleteMotivoMudanca")
        "/fichaCompleta/repetirUltimaAvaliacao"( controller: "fichaCompleta", action: "repetirUltimaAvaliacao")
        "/fichaCompleta/updateTextoFicha"( controller: "fichaCompleta", action: "updateTextoFicha")
        "/fichaCompleta/getFormPendencia"( controller: "fichaCompleta", action: "getFormPendencia")
        "/fichaCompleta/savePendencia"( controller: "fichaCompleta", action: "savePendencia")
        "/fichaCompleta/deletePendencia"( controller: "fichaCompleta", action: "deletePendencia")
        "/fichaCompleta/editPendencia"( controller: "fichaCompleta", action: "editPendencia")
        "/fichaCompleta/saveAvaliadaRevisada"( controller: "fichaCompleta", action: "saveAvaliadaRevisada")
        "/fichaCompleta/getCitacao"( controller: "fichaCompleta", action: "getCitacao")
        "/fichaCompleta/getAutoria"( controller: "fichaCompleta", action: "getAutoria")
        "/fichaCompleta/getEquipeTecnica"( controller: "fichaCompleta", action: "getEquipeTecnica")
        "/fichaCompleta/getColaboradores"( controller: "fichaCompleta", action: "getColaboradores")
		"/fichaCompleta/getGridDeclinioPopulacional"(controller:"fichaCompleta", action:"getGridDeclinioPopulacional")
		"/fichaCompleta/saveInUtilizadoAvaliacao"(controller:"fichaCompleta", action:"saveInUtilizadoAvaliacao")
		"/fichaCompleta/updateSituacaoMultimidia"(controller: "fichaCompleta",action:"updateSituacaoMultimidia")
		"/fichaCompleta/getInfoFundamentacao"(controller: "fichaCompleta",action:"getInfoFundamentacao")
		"/fichaCompleta/getOpcoesNaoUtilizacaoOcorrencia"(controller: "fichaCompleta",action:"getOpcoesNaoUtilizacaoOcorrencia")
		"/fichaCompleta/getJustificativasColaboracoes"(controller: "fichaCompleta",action:"getJustificativasColaboracoes")
		"/fichaCompleta/saveJustificativaOficina"(controller: "fichaCompleta",action:"saveJustificativaOficina")
        "/fichaCompleta/getGridAnexosPopulacao"(controller: "fichaCompleta",action:"getGridAnexosPopulacao")

        // revisor
        "/fichaCompleta/saveRevisor"( controller: "fichaCompleta", action: "saveRevisor")
        "/fichaCompleta/updateSituacaoPontoConsulta"( controller: "fichaCompleta", action: "updateSituacaoPontoConsulta")
        "/fichaCompleta/updateSituacaoSugestao"( controller: "fichaCompleta", action: "updateSituacaoSugestao")

        // validador
        "/fichaCompleta/saveResultadoValidador"( controller: "fichaCompleta", action: "saveResultadoValidador")
        "/fichaCompleta/saveChatValidacao"( controller: "fichaCompleta", action: "saveChatValidacao")
        "/fichaCompleta/updateGridChat"( controller: "fichaCompleta", action: "updateGridChat")
		"/fichaCompleta/getGridChats"( controller: "fichaCompleta", action: "getGridChats")
		"/fichaCompleta/deleteChat"( controller: "fichaCompleta", action: "deleteChat")
		"/fichaCompleta/convidarChat"( controller: "fichaCompleta", action: "convidarChat")
		"/fichaCompleta/chamarAtencaoChat"( controller: "fichaCompleta", action: "chamarAtencaoChat")
		"/fichaCompleta/chamarAtencaoTodosChat"( controller: "fichaCompleta", action: "chamarAtencaoTodosChat")

        // avaliação regional
        "/fichaCompleta/saveAvaliacaoRegional"( controller: "fichaCompleta", action: "saveAvaliacaoRegional")
        "/fichaCompleta/getGridAvaliacaoRegional"( controller: "fichaCompleta", action: "getGridAvaliacaoRegional")
        "/fichaCompleta/editAvaliacaoRegional"( controller: "fichaCompleta", action: "editAvaliacaoRegional")
        "/fichaCompleta/deleteAvaliacaoRegional"( controller: "fichaCompleta", action: "deleteAvaliacaoRegional")

		// ações realizadas no mapa
        "/fichaCompleta/mapActions"( controller: "fichaCompleta", action: "mapActions")

		// anexos ficha
        "/fichaCompleta/getAnexosFicha"( controller: "fichaCompleta", action: "getAnexosFicha")


        // Modulo de efeitos das ameacas - PRIM
		"/efeitoAmeaca"(controller: "efeitoAmeaca")
		"/efeitoAmeaca/getTreeViewEfeito"(controller: "efeitoAmeaca", action:'getTreeViewEfeito')
		"/efeitoAmeaca/getTreeViewAmeaca"(controller: "efeitoAmeaca", action:'getTreeViewAmeaca')
		"/efeitoAmeaca/getEfeitosAmeaca"(controller: "efeitoAmeaca", action:'getEfeitosAmeaca')
		"/efeitoAmeaca/gravar"(controller: "efeitoAmeaca", action:'gravar')

		"/oficina"(controller: "oficina")


		// Modulo Apoio
        "/dadosApoio"(controller: "dadosApoio")
		//"/dadosApoio/add"(controller:"dadosApoio", action:"add")
		"/dadosApoio/save"( controller: "dadosApoio", action: "save")
		"/dadosApoio/delete"( controller: "dadosApoio", action: "delete")
		"/dadosApoio/getTreeView"( controller: "dadosApoio", action: "getTreeView")
		"/dadosApoio/getGeoData"( controller: "dadosApoio", action: "getGeoData")
		"/dadosApoio/saveGeo"( controller: "dadosApoio", action: "saveGeo")
		/*"/dadosApoio/edit"( controller: "dadosApoio", action: "edit")
		"/dadosApoio/delete"(controller:"dadosApoio", action:"delete")
		"/dadosApoio/listParents"( controller: "dadosApoio", action: "listParents")
		"/dadosApoio/listParentsJSON"( controller: "dadosApoio", action: "listParentsJSON")
		*/

		// Modulo criterioAmeacaIucn
		"/criterioAmeacaIucn/add"(controller:"criterioAmeacaIucn", action:"add")
		"/criterioAmeacaIucn/save"(controller:"criterioAmeacaIucn", action:"save")
		"/criterioAmeacaIucn/edit"(controller:"criterioAmeacaIucn", action:"edit")
		"/criterioAmeacaIucn/delete"(controller:"criterioAmeacaIucn", action:"delete")
		"/criterioAmeacaIucn/addChild"(controller:"criterioAmeacaIucn", action:"addChild")
		"/criterioAmeacaIucn/listParents"(controller:"criterioAmeacaIucn", action:"listParents")
		"/criterioAmeacaIucn/listParentsJSON"(controller:"criterioAmeacaIucn", action:"listParentsJSON")

		// Modulo acaoConservacao
		"/acaoConservacao/add"(controller:"acaoConservacao", action:"add")
		"/acaoConservacao/save"(controller:"acaoConservacao", action:"save")
		"/acaoConservacao/edit"(controller:"acaoConservacao", action:"edit")
		"/acaoConservacao/delete"(controller:"acaoConservacao", action:"delete")
		"/acaoConservacao/addChild"(controller:"acaoConservacao", action:"addChild")
		"/acaoConservacao/listParents"(controller:"acaoConservacao", action:"listParents")
		"/acaoConservacao/listParentsJSON"(controller:"acaoConservacao", action:"listParentsJSON")

		// Modulo baciaHidrografica
		"/baciaHidrografica/add"(controller:"baciaHidrografica", action:"add")
		"/baciaHidrografica/save"( controller: "baciaHidrografica", action: "save")
		"/baciaHidrografica/edit"( controller: "baciaHidrografica", action: "edit")
		"/baciaHidrografica/delete"(controller:"baciaHidrografica", action:"delete")
		"/baciaHidrografica/listParents"( controller: "baciaHidrografica", action: "listParents")
		"/baciaHidrografica/listParentsJSON"( controller: "baciaHidrografica", action: "listParentsJSON")


		// Modulo habitat
        "/habitat"(controller: "habitat")
        "/habitat/getTreeView"(controller:"habitat", action:"getTreeView")
        "/habitat/getForm"(controller:"habitat", action:"getForm")
        "/habitat/save"(controller:"habitat", action:"save")
        "/habitat/delete"(controller:"habitat", action:"delete")

		// Modulo uso
        "/uso"(controller: "uso")
		"/uso/getTreeView"(controller:"uso", action:"getTreeView")
		"/uso/getForm"(controller:"uso", action:"getForm")
		"/uso/save"(controller:"uso", action:"save")
		"/uso/delete"(controller:"uso", action:"delete")

		// "/uso/add"(controller:"uso", action:"add")
		// "/uso/save"( controller: "uso", action: "save")
		// "/uso/edit"( controller: "uso", action: "edit")
		// "/uso/delete"(controller:"uso", action:"delete")
		// "/uso/select2"( controller: "uso", action: "select2")
		// "/uso/listParents"( controller: "uso", action: "listParents")
		// "/uso/listParentsJSON"( controller: "uso", action: "listParentsJSON")
		// "/uso/listCriterioAmeacaIucnJSON"( controller: "uso", action: "listCriterioAmeacaIucnJSON")

        // Modulo ameaca
        "/ameaca"(controller: "ameaca")
        "/ameaca/getTreeView"(controller:"ameaca", action:"getTreeView")
        "/ameaca/getForm"(controller:"ameaca", action:"getForm")
        "/ameaca/save"(controller:"ameaca", action:"save")
        "/ameaca/delete"(controller:"ameaca", action:"delete")

        // Modulo motivo de mudanca de categoria
        "/motivoMudanca"(controller: "motivoMudanca")
        "/motivoMudanca/getTreeView"(controller:"motivoMudanca", action:"getTreeView")
        "/motivoMudanca/getForm"(controller:"motivoMudanca", action:"getForm")
        "/motivoMudanca/save"(controller:"motivoMudanca", action:"save")
        "/motivoMudanca/delete"(controller:"motivoMudanca", action:"delete")

        // Modulo pesquisa
        "/pesquisa"(controller: "pesquisa")
        "/pesquisa/getTreeView"(controller:"pesquisa", action:"getTreeView")
        "/pesquisa/getForm"(controller:"pesquisa", action:"getForm")
        "/pesquisa/save"(controller:"pesquisa", action:"save")
        "/pesquisa/delete"(controller:"pesquisa", action:"delete")

        // Modulo acao conservacao
        "/acaoConservacao"(controller: "acaoConservacao")
        "/acaoConservacao/getTreeView"(controller:"acaoConservacao", action:"getTreeView")
        "/acaoConservacao/getForm"(controller:"acaoConservacao", action:"getForm")
        "/acaoConservacao/save"(controller:"acaoConservacao", action:"save")
        "/acaoConservacao/delete"(controller:"acaoConservacao", action:"delete")


        // Modulo ameaca SIS/IUCN
        "/iucnAmeaca"(controller: "iucnAmeaca")
        "/iucnAmeaca/getGrid"(controller:"iucnAmeaca", action:"getGrid")
        "/iucnAmeaca/getForm"(controller:"iucnAmeaca", action:"getForm")
        "/iucnAmeaca/save"(controller:"iucnAmeaca", action:"save")
        "/iucnAmeaca/delete"(controller:"iucnAmeaca", action:"delete")

        "/iucnTabelas"(controller: "iucnTabelas")
        "/iucnTabelas/getGrid"(controller:"iucnTabelas", action:"getGrid")
        "/iucnTabelas/getForm"(controller:"iucnTabelas", action:"getForm")
        "/iucnTabelas/save"(controller:"iucnTabelas", action:"save")
        "/iucnTabelas/delete"(controller:"iucnTabelas", action:"delete")





        // Modulo Ciclo de Avaliação
		"/cicloAvaliacao"(controller: "cicloAvaliacao", action:"index")
		"/cicloAvaliacao/getGrid"( controller: "cicloAvaliacao", action: "getGrid")
		"/cicloAvaliacao/save"( controller: "cicloAvaliacao", action: "save")
		"/cicloAvaliacao/edit"( controller: "cicloAvaliacao", action: "edit")
		"/cicloAvaliacao/delete"(controller:"cicloAvaliacao", action:"delete")



		// Modulo Oficina de Avaliação
        "/oficina/getGridOficinas"(controller:"oficina", action:"getGridOficinas")
        "/oficina/select2PF"(controller:"oficina", action:"select2PF") // selecionar Ponto Focal
        "/oficina/saveOficina"( controller: "oficina", action: "saveOficina")
        "/oficina/editOficina"( controller: "oficina", action: "editOficina")
        "/oficina/getFormSelecionarFicha"( controller: "oficina", action: "getFormSelecionarFicha")
        "/oficina/getGridFicha1"( controller: "oficina", action: "getGridFicha1")
        "/oficina/getGridFicha2"( controller: "oficina", action: "getGridFicha2")
        "/oficina/saveFicha"( controller: "oficina", action: "saveFicha")
        "/oficina/deleteFicha"( controller: "oficina", action: "deleteFicha")
        "/oficina/delete"(controller:"oficina", action:"delete")
        "/oficina/changeSituacao"(controller:"oficina", action:"changeSituacao")
        "/oficina/saveEmailParticipante"(controller:"oficina", action:"saveEmailParticipante")
        // eventos da edição direta no gride do nome e da função do participante
        "/oficina/saveNomeParticipanteMemoriaReuniao"(controller:"oficina", action:"saveNomeParticipanteMemoriaReuniao")
        "/oficina/saveFuncaoParticipanteMemoriaReuniao"(controller:"oficina", action:"saveFuncaoParticipanteMemoriaReuniao")

        //"/oficina/list"(controller:"oficina", action:"list")
        // ABA PARTICIPANTES DA OFICINA
        "/oficina/getFormParticipante"(controller:"oficina", action:"getFormParticipante")
        "/oficina/saveParticipante"(controller:"oficina", action:"saveParticipante")
        "/oficina/getGridParticipante"(controller:"oficina", action:"getGridParticipante")
        "/oficina/deleteParticipante"(controller:"oficina", action:"deleteParticipante")
        "/oficina/getTextLastEmail"(controller:"oficina", action:"getTextLastEmail")
        "/oficina/enviarEmailParticipantes"(controller:"oficina", action:"enviarEmailParticipantes")
        "/oficina/changeSituacaoConvite"(controller:"oficina", action:"changeSituacaoConvite")
        "/oficina/saveLinkSei"(controller:"oficina", action:"saveLinkSei")
        "/oficina/saveAgrupamento"(controller:"oficina", action:"saveAgrupamento")
        "/oficina/getGridImportarParticipante"(controller:"oficina", action:"getGridImportarParticipante")
        "/oficina/saveImportacaoParticipantes"(controller:"oficina", action:"saveImportacaoParticipantes")

		// ABA MEMORIA REUNIAO PREPARATORIA
		"/oficina/getFormMemoria"(controller:"oficina", action:"getFormMemoria")
		"/oficina/saveFormMemoria"(controller:"oficina", action:"saveFormMemoria")
		"/oficina/addParticipante"(controller:"oficina", action:"addParticipante")
		"/oficina/getGridParticipanteMemoria"(controller:"oficina", action:"getGridParticipanteMemoria")
		"/oficina/deleteParticipantesMemoria"(controller:"oficina", action:"deleteParticipantesMemoria")
		"/oficina/getGridAnexoMemoria"(controller:"oficina", action:"getGridAnexoMemoria")
		"/oficina/saveAnexoMemoria"(controller:"oficina", action:"saveAnexoMemoria")
        "/oficina/getAnexoMemoria/$id/$thumb?"( controller:"oficina", action:"getAnexoMemoria")
        "/oficina/deleteAnexoMemoria"( controller:"oficina", action:"deleteAnexoMemoria")
		"/oficina/gerarDocMemoria"( controller: "oficina", action: "gerarDocMemoria")


        // ABA PAPEL
        "/oficina/getFormPapel"(controller:"oficina", action:"getFormPapel")
        "/oficina/getGridPapelParticipanteFicha"(controller:"oficina", action:"getGridPapelParticipanteFicha")
        "/oficina/savePapel"( controller: "oficina", action: "savePapel")
        "/oficina/editPapel"( controller: "oficina", action: "editPapel")
        "/oficina/deletePapel"( controller: "oficina", action: "deletePapel")
        "/oficina/getGridFichasPapel"( controller: "oficina", action: "getGridFichasPapel")
        "/oficina/getFichasParticipante"( controller: "oficina", action: "getFichasParticipante")

        // ABA EXECUCAO
        "/oficina/getFormExecucao"( controller: "oficina", action: "getFormExecucao")
        "/oficina/getGridExecucaoTotais"( controller: "oficina", action: "getGridExecucaoTotais")
        "/oficina/getGridExecucaoFichaLc"( controller: "oficina", action: "getGridExecucaoFichaLc")
        "/oficina/getGridExecucaoFichaNaoLc"( controller: "oficina", action: "getGridExecucaoFichaNaoLc")
        "/oficina/confirmarLc"( controller: "oficina", action: "confirmarLc")
        "/oficina/salvarJustificativa"( controller: "oficina", action: "salvarJustificativa")
        "/oficina/avaliarLc"( controller: "oficina", action: "avaliarLc")
        "/oficina/transferirRetornarFicha"( controller: "oficina", action: "transferirRetornarFicha")
        "/oficina/excluirRestaurarFicha"( controller: "oficina", action: "excluirRestaurarFicha")
        "/oficina/getFormAvaliacaoExpedita"( controller: "oficina", action: "getFormAvaliacaoExpedita")
        "/oficina/saveAvaliacaoExpedita"( controller: "oficina", action: "saveAvaliacaoExpedita")
        "/oficina/saveAnexo"( controller: "oficina", action: "saveAnexo")
        "/oficina/deleteAnexo"( controller: "oficina", action: "deleteAnexo")
        "/oficina/getGridAnexo"( controller:"oficina", action:"getGridAnexo")
        "/oficina/getAnexo/$id/$thumb?"( controller:"oficina", action:"getAnexo")
        "/oficina/getFormAnexo"( controller: "oficina", action: "getFormAnexo")
        "/oficina/encerrarOficina"( controller: "oficina", action: "encerrarOficina")
        // relatórios oficina
        "/oficina/gerarMemoria"( controller: "oficina", action: "gerarMemoria")
        "/oficina/existeFuncaoAtribuida"( controller: "oficina", action: "existeFuncaoAtribuida")
        "/oficina/gerarDocFinal"( controller: "oficina", action: "gerarDocFinal")
        "/oficina/gerarRelatorioLc"( controller: "oficina", action: "gerarRelatorioLc")
        "/oficina/enviarEmailAssinaturaEletronica"( controller: "oficina", action: "enviarEmailAssinaturaEletronica")
        "/oficina/getGruposFichasOficina"( controller: "oficina", action: "getGruposFichasOficina")


//----------------------------------------------------------------------------------------------------

		// Modulo Ficha
		"/ficha/edit"( controller: "ficha", action: "edit")
		"/ficha/delete"( controller:"ficha", action:"delete")
		"/ficha/list"( controller:"ficha", action:"list")
		"/ficha/getGrid"( controller:"ficha", action:"getGrid")
		"/ficha/getModal"( controller:"ficha", action:"getModal")
		"/ficha/select2RefBib"( controller:"ficha", action:"select2RefBib")
		"/ficha/select2Tags"( controller:"ficha", action:"select2Tags")
		"/ficha/saveFrmRefBib"( controller:"ficha", action:"saveFrmRefBib")
		"/ficha/deleteRefBib"( controller:"ficha", action:"deleteRefBib")
		"/ficha/deleteFichaRefBib"( controller:"ficha", action:"deleteFichaRefBib")
		"/ficha/editRefBib"( controller:"ficha", action:"editRefBib")
		"/ficha/select2Municipio"( controller:"ficha", action:"select2Municipio")
		"/ficha/select2PessoaFisica"( controller:"ficha", action:"select2PessoaFisica")
		"/ficha/semPermissao"( controller:"ficha", action:"semPermissao")
		"/ficha/getHabitat"( controller:"ficha", action:"getHabitat")
        "/ficha/getCategoriaUltimaAvaliacaoNacional"(controller: "ficha", action:"getCategoriaUltimaAvaliacaoNacional")
        "/ficha/select2Ucs"( controller:"ficha", action:"select2Ucs")
        "/ficha/select2UcsEsfera"( controller:"ficha", action:"select2UcsEsfera")
        "/ficha/getGridImportacaoFichas"( controller:"ficha", action:"getGridImportacaoFichas")
        "/ficha/getGridVisualizarPendencias"( controller:"ficha", action:"getGridVisualizarPendencias")
        "/ficha/getReferenciaUtilizacao"( controller:"ficha", action:"getReferenciaUtilizacao")
        "/ficha/deleteFichaPublicacao"( controller:"ficha", action:"deleteFichaPublicacao")


		// regionalização USOS E AMEAÇAS
		"/ficha/saveRegionalizacao"(controller:"ficha", action:"saveRegionalizacao")
		"/ficha/editRegionalizacao"( controller:"ficha", action:"editRegionalizacao")
		"/ficha/deleteRegionalizacao"(controller:"ficha", action:"deleteRegionalizacao")

		// georeferencia ameaca e uso
		"/ficha/saveGeoreferencia"(controller:"ficha", action:"saveGeoreferencia")
		"/ficha/editGeoreferencia"( controller:"ficha", action:"editGeoreferencia")
		"/ficha/deleteGeoreferencia"(controller:"ficha", action:"deleteGeoreferencia")
		// anexos
		"/ficha/getAnexo/$id/$thumb?"( controller:"ficha", action:"getAnexo")
		"/ficha/saveAnexo"(controller:"ficha", action:"saveAnexo")
		"/ficha/deleteAnexo"(controller:"ficha", action:"deleteAnexo")
		"/ficha/toggleImagemPrincipal"(controller:"ficha", action:"toggleImagemPrincipal")

        // autores
        "/fichaAutor/getForm"(controller: "fichaAutor", action:"getForm")
        "/fichaAutor/save"(controller: "fichaAutor", action:"save")
        "/fichaAutor/edit"(controller: "fichaAutor", action:"edit")
        "/fichaAutor/delete"(controller: "fichaAutor", action:"delete")
        "/fichaAutor/select2Autor"(controller: "fichaAutor", action:"select2Autor")
        // ações em lote
        "/ficha/alterarSituacaoEmLote"(controller: "ficha", action:"alterarSituacaoEmLote")
        "/ficha/excluirFichaEmLote"(controller: "ficha", action:"excluirFichaEmLote")
        "/ficha/alterarUnidadeLote"(controller: "ficha", action:"alterarUnidadeLote")
        "/ficha/alterarGrupoSubgrupoLote"(controller: "ficha", action:"alterarGrupoSubgrupoLote")
        "/ficha/alterarGrupoSalveLote"(controller: "ficha", action:"alterarGrupoSalveLote")
        "/ficha/alterarAutoriaLote"(controller: "ficha", action:"alterarAutoriaLote")
        "/ficha/alterarEquipeTecnicaLote"(controller: "ficha", action:"alterarEquipeTecnicaLote")
        "/ficha/alterarColaboradoresLote"(controller: "ficha", action:"alterarColaboradoresLote")
        "/ficha/hasSensitiveOccurrence"(controller: "ficha", action:"hasSensitiveOccurrence")




//----------------------------------------------------------------------------------------------------

		// Aba 1 - Ficha/Taxonomia
		"/fichaTaxonomia/"( controller:"fichaTaxonomia", action:"index")
		"/fichaTaxonomia/save"( controller:"fichaTaxonomia", action:"save")
		"/fichaTaxonomia/editNomeComum"( controller:"fichaTaxonomia", action:"editNomeComum")
		"/fichaTaxonomia/saveNomeComum"( controller:"fichaTaxonomia", action:"saveNomeComum")
		"/fichaTaxonomia/editSinonimia"( controller:"fichaTaxonomia", action:"editSinonimia")
		"/fichaTaxonomia/saveSinonimia"( controller:"fichaTaxonomia", action:"saveSinonimia")
		"/fichaTaxonomia/deleteNomeComum"( controller:"fichaTaxonomia", action:"deleteNomeComum")
		"/fichaTaxonomia/deleteSinonimia"( controller:"fichaTaxonomia", action:"deleteSinonimia")
		"/fichaTaxonomia/findPontoFocal"( controller:"fichaTaxonomia", action:"findPontoFocal")
		"/fichaTaxonomia/findCoordenadorTaxon"( controller:"fichaTaxonomia", action:"findCoordenadorTaxon")
		"/fichaTaxonomia/findInstituicaoCT"( controller:"fichaTaxonomia", action:"findInstituicaoCT")

//----------------------------------------------------------------------------------------------------

		// Aba 2 - Ficha/Distribuicao
		"/fichaDistribuicao/"( controller:"fichaDistribuicao", action:"index")
		"/fichaDistribuicao/tabUf"( controller:"fichaDistribuicao", action:"tabUf")
		"/fichaDistribuicao/tabBioma"( controller:"fichaDistribuicao", action:"tabBioma")
		"/fichaDistribuicao/tabBaciaHidrografica"( controller:"fichaDistribuicao", action:"tabBaciaHidrografica")
		"/fichaDistribuicao/tabAmbienteRestrito"( controller:"fichaDistribuicao", action:"tabAmbienteRestrito")
		"/fichaDistribuicao/tabAreaRelevante"( controller:"fichaDistribuicao", action:"tabAreaRelevante")
		"/fichaDistribuicao/tabOcorrencia"( controller:"fichaDisOcorrencia", action:"index")
		//"/fichaDistribuicao/tabSituacaoRegional"( controller:"fichaDistribuicao", action:"tabSituacaoRegional")
		"/fichaDistribuicao/saveFrmDistribuicao"( controller:"fichaDistribuicao", action:"saveFrmDistribuicao")
		"/fichaDistribuicao/getPoints"( controller:"fichaDistribuicao", action:"getPoints")
		"/fichaDistribuicao/tabEooAoo"( controller:"fichaDistribuicao", action:"tabEooAoo")
		"/fichaDistribuicao/saveFrmDisEooAoo"( controller:"fichaDistribuicao", action:"saveFrmDisEooAoo")

		//"/fichaDistribuicao/saveAnexo"( controller:"fichaDistribuicao", action:"saveAnexo")
		//"/fichaDistribuicao/deleteAnexo"( controller:"fichaDistribuicao", action:"deleteAnexo")



			// Aba 2.1 - Ficha/Distribuicao/UF
			//"/fichaDistribuicao/saveFrmDisUf"( controller:"fichaDistribuicao", action:"saveFrmDisUf")
			"/fichaDistribuicao/getTreeUfs"( controller:"fichaDistribuicao", action:"getTreeUfs")
			"/fichaDistribuicao/saveTreeUf"( controller:"fichaDistribuicao", action:"saveTreeUf")
			"/fichaDistribuicao/getTreeBiomas"( controller:"fichaDistribuicao", action:"getTreeBiomas")
			"/fichaDistribuicao/saveTreeBioma"( controller:"fichaDistribuicao", action:"saveTreeBioma")
			"/fichaDistribuicao/getTreeBacias"( controller:"fichaDistribuicao", action:"getTreeBacias")
			"/fichaDistribuicao/saveTreeBacia"( controller:"fichaDistribuicao", action:"saveTreeBacia")

			"/fichaDistribuicao/deleteDistUf"( controller:"fichaDistribuicao", action:"deleteDistUf")
			"/fichaDistribuicao/selAllUf"( controller:"fichaDistribuicao", action:"selAllUf")
			"/fichaDistribuicao/selAllBioma"( controller:"fichaDistribuicao", action:"selAllBioma")
			"/fichaDistribuicao/selAllBacia"( controller:"fichaDistribuicao", action:"selAllBacia")

			// Aba 2.2 - Ficha/Distribuicao/Bioma
			"/fichaDistribuicao/saveFrmDisBioma"( controller:"fichaDistribuicao", action:"saveFrmDisBioma")
			"/fichaDistribuicao/deleteDistBioma"( controller:"fichaDistribuicao", action:"deleteDistBioma")

			// Aba 2.3 - Ficha/Distribuicao/Bacia Hidrografica
			"/fichaDistribuicao/getSubBacia"( controller:"fichaDistribuicao", action:"getSubBacia")
			"/fichaDistribuicao/saveFrmDisBaciaHidrografica"( controller:"fichaDistribuicao", action:"saveFrmDisBaciaHidrografica")
			"/fichaDistribuicao/deleteDistBaciaHidrografica"( controller:"fichaDistribuicao", action:"deleteDistBaciaHidrografica")

			// Aba 2.4 - Ficha/Distribuicao/Ambiente Restrito
			"/fichaDistribuicao/getRestricaoHabitat"( controller:"fichaDistribuicao", action:"getRestricaoHabitat")
			"/fichaDistribuicao/saveFrmDisAmbienteRestrito"( controller:"fichaDistribuicao", action:"saveFrmDisAmbienteRestrito")
			"/fichaDistribuicao/editDisAmbienteRestrito"( controller:"fichaDistribuicao", action:"editDisAmbienteRestrito")
			"/fichaDistribuicao/deleteDisAmbienteRestrito"( controller:"fichaDistribuicao", action:"deleteDisAmbienteRestrito")
			"/fichaDistribuicao/getGridDistCavernas"( controller:"fichaDistribuicao", action:"getGridDistCavernas")
			"/fichaDistribuicao/getGridSelecionarCaverna"( controller:"fichaDistribuicao", action:"getGridSelecionarCaverna")
			"/fichaDistribuicao/deleteCaverna"( controller:"fichaDistribuicao", action:"deleteCaverna")
			"/fichaDistribuicao/saveCavernas"( controller:"fichaDistribuicao", action:"saveCavernas")

			// Aba 2.5 - Ficha/Distribuicao/Area Relevante
			"/fichaDistribuicao/saveFrmDisAreaRelevante"( controller:"fichaDistribuicao", action:"saveFrmDisAreaRelevante")
			"/fichaDistribuicao/deleteDisAreaRelevante"( controller:"fichaDistribuicao", action:"deleteDisAreaRelevante")
			"/fichaDistribuicao/editAreaRelevante"( controller:"fichaDistribuicao", action:"editAreaRelevante")
			"/fichaDistribuicao/saveFrmMunicipio"( controller:"fichaDistribuicao", action:"saveFrmMunicipio")
			"/fichaDistribuicao/deleteMunicipioAreaRelevante"( controller:"fichaDistribuicao", action:"deleteMunicipioAreaRelevante")

			// Aba 2.6 - Ficha/Distribuicao/Ocorrencia
			"/fichaDisOcorrencia/"( controller:"fichaDisOcorrencia", action:"index")
			"/fichaDisOcorrencia/tabGeoReferencia"( controller:"fichaDisOcorrencia", action:"tabGeoReferencia")
			"/fichaDisOcorrencia/tabDisOcoDadosRegistro"( controller:"fichaDisOcorrencia", action:"tabDisOcoDadosRegistro")
			"/fichaDisOcorrencia/tabDisOcoLocalidade"( controller:"fichaDisOcorrencia", action:"tabDisOcoLocalidade")
			"/fichaDisOcorrencia/tabDisOcoQualificador"( controller:"fichaDisOcorrencia", action:"tabDisOcoQualificador")
			"/fichaDisOcorrencia/tabDisOcoResponsavel"( controller:"fichaDisOcorrencia", action:"tabDisOcoResponsavel")
			"/fichaDisOcorrencia/tabDisOcoRefBib"( controller:"fichaDisOcorrencia", action:"tabDisOcoRefBib")
			"/fichaDisOcorrencia/tabDisOcoMultimidia"( controller:"fichaDisOcorrencia", action:"tabDisOcoMultimidia")
			"/fichaDisOcorrencia/getGridOcorrencia"( controller:"fichaDisOcorrencia", action:"getGridOcorrencia")

				// Modulo Ficha/Distribuicao/Ocorrencia/Georeferencia
				"/fichaDisOcorrencia/saveFrmDisOcoGeoreferencia"( controller:"fichaDisOcorrencia", action:"saveFrmDisOcoGeoreferencia")
				"/fichaDisOcorrencia/editGeoReferencia"( controller:"fichaDisOcorrencia", action:"editGeoReferencia")
				"/fichaDisOcorrencia/deleteDisOcorrencia"( controller:"fichaDisOcorrencia", action:"deleteDisOcorrencia")
				"/fichaDisOcorrencia/saveLocalidadeDesconhecida"( controller:"fichaDisOcorrencia", action:"saveLocalidadeDesconhecida")
				"/fichaDisOcorrencia/saveSemUf"( controller:"fichaDisOcorrencia", action:"saveSemUf")

				// Modulo Ficha/Distribuicao/Ocorrencia/Dados do registro
				"/fichaDisOcorrencia/saveFrmDisOcoRegistro"( controller:"fichaDisOcorrencia", action:"saveFrmDisOcoRegistro")

				// Modulo Ficha/Distribuicao/Ocorrencia/Localidade 2.6.3
				"/fichaDisOcorrencia/saveFrmDisOcoLocalidade"( controller:"fichaDisOcorrencia", action:"saveFrmDisOcoLocalidade")

				// Modulo Ficha/Distribuicao/Ocorrencia/Qualificador 2.6.4
				"/fichaDisOcorrencia/saveFrmDisOcoQualificador"( controller:"fichaDisOcorrencia", action:"saveFrmDisOcoQualificador")

				// Modulo Ficha/Distribuicao/Ocorrencia/Responsavel 2.6.5
				"/fichaDisOcorrencia/saveFrmDisOcoResponsavel"( controller:"fichaDisOcorrencia", action:"saveFrmDisOcoResponsavel")

				// Modulo Ficha/Distribuicao/Ocorrencia/Responsavel 2.6.5
				"/fichaDisOcorrencia/saveFrmDisOcoRefBib"( controller:"fichaDisOcorrencia", action:"saveFrmDisOcoRefBib")
				"/fichaDisOcorrencia/deleteRefBib"( controller:"fichaDisOcorrencia", action:"deleteRefBib")

//----------------------------------------------------------------------------------------------------

// Aba 3 - Modulo Ficha/Historia Natural
"/fichaHistoriaNatural/"( controller:"fichaHistoriaNatural", action:"index")
"/fichaHistoriaNatural/saveFrmHistoriaNatural"( controller:"fichaHistoriaNatural", action:"saveFrmHistoriaNatural")
// 3.1 - Aba Hábito Alimentar
"/fichaHistoriaNatural/tabHisNatHabitoAlimentar"( controller:"fichaHistoriaNatural", action:"tabHisNatHabitoAlimentar")
"/fichaHistoriaNatural/saveFrmHisNatPosicaoTrofica"( controller:"fichaHistoriaNatural", action:"saveFrmHisNatPosicaoTrofica")


// habito alimentar normal
"/fichaHistoriaNatural/saveFrmHisNatHabitoAlimentar"( controller:"fichaHistoriaNatural", action:"saveFrmHisNatHabitoAlimentar")
"/fichaHistoriaNatural/deleteHabitoAlimentar"( controller:"fichaHistoriaNatural", action:"deleteHabitoAlimentar")
"/fichaHistoriaNatural/editHabitoAlimentar"( controller:"fichaHistoriaNatural", action:"editHabitoAlimentar")
// habito alimentar especialista
"/fichaHistoriaNatural/saveFrmHisNatHabitoAlimentarEsp"( controller:"fichaHistoriaNatural", action:"saveFrmHisNatHabitoAlimentarEsp")
"/fichaHistoriaNatural/deleteHabitoAlimentarEsp"( controller:"fichaHistoriaNatural", action:"deleteHabitoAlimentarEsp")
"/fichaHistoriaNatural/editHabitoAlimentarEsp"( controller:"fichaHistoriaNatural", action:"editHabitoAlimentarEsp")

// 3.2 - Aba Habitat
"/fichaHistoriaNatural/tabHisNatHabitat"( controller:"fichaHistoriaNatural", action:"tabHisNatHabitat")
"/fichaHistoriaNatural/saveFrmHisNatHabitat"( controller:"fichaHistoriaNatural", action:"saveFrmHisNatHabitat")
"/fichaHistoriaNatural/saveFrmHisNatHabitatDetalhe"( controller:"fichaHistoriaNatural", action:"saveFrmHisNatHabitatDetalhe")
"/fichaHistoriaNatural/deleteHabitat"( controller:"fichaHistoriaNatural", action:"deleteHabitat")

// 3.3 - Aba Interação
"/fichaHistoriaNatural/saveFrmObsHabitoAlimentar"( controller:"fichaHistoriaNatural", action:"saveFrmObsHabitoAlimentar")
"/fichaHistoriaNatural/tabHisNatInteracao"( controller:"fichaHistoriaNatural", action:"tabHisNatInteracao")
"/fichaHistoriaNatural/saveFrmHisNatInteracao"( controller:"fichaHistoriaNatural", action:"saveFrmHisNatInteracao")
"/fichaHistoriaNatural/saveFrmHisNatInteracaoDetalhe"( controller:"fichaHistoriaNatural", action:"saveFrmHisNatInteracaoDetalhe")
"/fichaHistoriaNatural/editInteracao"( controller:"fichaHistoriaNatural", action:"editInteracao")
"/fichaHistoriaNatural/deleteInteracao"( controller:"fichaHistoriaNatural", action:"deleteInteracao")
"/fichaHistoriaNatural/lerObsInteracaoSubespecie"(controller:"fichaHistoriaNatural", action:"lerObsInteracaoSubespecie")

// 3.4 - Aba Reproducao
"/fichaHistoriaNatural/tabHisNatReproducao"( controller:"fichaHistoriaNatural", action:"tabHisNatReproducao")
"/fichaHistoriaNatural/saveFrmHisNatReproducao"( controller:"fichaHistoriaNatural", action:"saveFrmHisNatReproducao")

// 3.5 - Aba Variação Sazonal
"/fichaHistoriaNatural/tabHisNatSazonalidade"( controller:"fichaHistoriaNatural", action:"tabHisNatSazonalidade")
"/fichaHistoriaNatural/saveFrmHisNatSazonalidade"( controller:"fichaHistoriaNatural", action:"saveFrmHisNatSazonalidade")
"/fichaHistoriaNatural/editVariacaoSazonal"( controller:"fichaHistoriaNatural", action:"editVariacaoSazonal")
"/fichaHistoriaNatural/deleteVariacaoSazonal"( controller:"fichaHistoriaNatural", action:"deleteVariacaoSazonal")

//----------------------------------------------------------------------------------------------------

// Aba 4 - Ficha/População
"/fichaPopulacao/"( controller:"fichaPopulacao", action:"index")
"/fichaPopulacao/saveFrmPopulacao"( controller:"fichaPopulacao", action:"saveFrmPopulacao")
//"/fichaPopulacao/saveFrmPopulacaoGrafico"( controller:"fichaPopulacao", action:"saveFrmPopulacaoGrafico")
//"/fichaPopulacao/deletePopulGrafico"( controller:"fichaPopulacao", action:"deletePopulGrafico")
//"/fichaPopulacao/getGrafico/$id"( controller:"fichaPopulacao", action:"getGrafico")

// Aba 4.1 - Populacao Conhecida / Estimada
"/fichaPopulacao/tabPopulEstimada"( controller:"fichaPopulacao", action:"tabPopulEstimada")
"/fichaPopulacao/saveFrmPopulEstimada"( controller:"fichaPopulacao", action:"saveFrmPopulEstimada")
"/fichaPopulacao/editPopulEstimada"( controller:"fichaPopulacao", action:"editPopulEstimada")
"/fichaPopulacao/deletePopulEstimada"( controller:"fichaPopulacao", action:"deletePopulEstimada")
// Aba 4.2 - Populacao Area de Vida
"/fichaPopulacao/tabPopAreaVida"( controller:"fichaPopulacao", action:"tabPopAreaVida")
"/fichaPopulacao/saveFrmPopAreaVida"( controller:"fichaPopulacao", action:"saveFrmPopAreaVida")
"/fichaPopulacao/editPopAreaVida"( controller:"fichaPopulacao", action:"editPopAreaVida")
"/fichaPopulacao/deletePopAreaVida"( controller:"fichaPopulacao", action:"deletePopAreaVida")

//----------------------------------------------------------------------------------------------------

// Aba 5 - Ficha/Ameaças
"/fichaAmeaca/"( controller:"fichaAmeaca", action:"index")
"/fichaAmeaca/select2CriterioAmeacaIucn"( controller:"fichaAmeaca", action:"select2CriterioAmeacaIucn")
"/fichaAmeaca/saveFrmAmeaca"( controller:"fichaAmeaca", action:"saveFrmAmeaca")
"/fichaAmeaca/editAmeaca"( controller:"fichaAmeaca", action:"editAmeaca")
"/fichaAmeaca/deleteAmeaca"( controller:"fichaAmeaca", action:"deleteAmeaca")
"/fichaAmeaca/saveFrmAmeacaFicha"( controller:"fichaAmeaca", action:"saveFrmAmeacaFicha")
"/fichaAmeaca/getTreeAmeacas"( controller:"fichaAmeaca", action:"getTreeAmeacas")
"/fichaAmeaca/getTreeEfeitoPrim"( controller:"fichaAmeaca", action:"getTreeEfeitoPrim")
"/fichaAmeaca/saveTreeAmeaca"( controller:"fichaAmeaca", action:"saveTreeAmeaca")
"/fichaAmeaca/saveTreeEfeitoPrim"( controller:"fichaAmeaca", action:"saveTreeEfeitoPrim")
"/fichaAmeaca/getGraficoEfeitoPrim/$sqFichaAmeaca"( controller:"fichaAmeaca", action:"getGraficoEfeitoPrim")

//----------------------------------------------------------------------------------------------------

// Aba 6 - Ficha/Usos
"/fichaUso/"( controller:"fichaUso", action:"index")
"/fichaUso/select2Uso"( controller:"fichaUso", action:"select2Uso")
"/fichaUso/saveFrmUso"( controller:"fichaUso", action:"saveFrmUso")
"/fichaUso/editUso"( controller:"fichaUso", action:"editUso")
"/fichaUso/deleteUso"( controller:"fichaUso", action:"deleteUso")
"/fichaUso/saveFrmUsoFicha"( controller:"fichaUso", action:"saveFrmUsoFicha")
"/fichaUso/getTreeUsos"( controller:"fichaUso", action:"getTreeUsos")
"/fichaUso/saveTreeUso"( controller:"fichaUso", action:"saveTreeUso")

//----------------------------------------------------------------------------------------------------

// Aba 7 - Ficha/Conservacao
"/fichaConservacao/"( controller:"fichaConservacao", action:"index")
// Aba 7.1 - Histórico de Avaliações
"/fichaConservacao/editHistoricoAvaliacao"( controller:"fichaConservacao", action:"editHistoricoAvaliacao")
"/fichaConservacao/deleteHistoricoAvaliacao"( controller:"fichaConservacao", action:"deleteHistoricoAvaliacao")
"/fichaConservacao/tabConHistoricoAvaliacao"( controller:"fichaConservacao", action:"tabConHistoricoAvaliacao")
"/fichaConservacao/select2Abrangencia"( controller:"fichaConservacao", action:"select2Abrangencia")
"/fichaConservacao/saveFrmConHistorico"( controller:"fichaConservacao", action:"saveFrmConHistorico")
"/fichaConservacao/saveFrmConHistoricoObs"( controller:"fichaConservacao", action:"saveFrmConHistoricoObs")
"/fichaConservacao/savePresencaListaVigente"( controller:"fichaConservacao", action:"savePresencaListaVigente")
"/fichaConservacao/saveOficial"( controller:"fichaConservacao", action:"saveOficial")
"/fichaConservacao/visualizarFicha"( controller:"fichaConservacao", action:"visualizarFicha")

// Aba 7.2 - Lista e Convenções
"/fichaConservacao/tabConListaConvencao"( controller:"fichaConservacao", action:"tabConListaConvencao")
"/fichaConservacao/saveFrmConListaConvencao"( controller:"fichaConservacao", action:"saveFrmConListaConvencao")
"/fichaConservacao/saveFrmConListaConvencaoDetalhe"( controller:"fichaConservacao", action:"saveFrmConListaConvencaoDetalhe")
"/fichaConservacao/deleteListaConvencao"( controller:"fichaConservacao", action:"deleteListaConvencao")
// aba 7.3 - Ações de Conservação
"/fichaConservacao/tabConAcaoConservacao"( controller:"fichaConservacao", action:"tabConAcaoConservacao")
"/fichaConservacao/saveFrmConAcaoConservacao"( controller:"fichaConservacao", action:"saveFrmConAcaoConservacao")
"/fichaConservacao/saveFrmConAcaoConservacaoDetalhe"( controller:"fichaConservacao", action:"saveFrmConAcaoConservacaoDetalhe")
"/fichaConservacao/editAcaoConservacao"( controller:"fichaConservacao", action:"editAcaoConservacao")
"/fichaConservacao/deleteAcaoConservacao"( controller:"fichaConservacao", action:"deleteAcaoConservacao")
"/fichaConservacao/select2PlanoAcao"( controller:"fichaConservacao", action:"select2PlanoAcao")
"/fichaConservacao/select2AcaoConservacao"( controller:"fichaConservacao", action:"select2AcaoConservacao")
// Aba 7.4 - Presenca UC
"/fichaConservacao/tabConPresencaUC"( controller:"fichaConservacao", action:"tabConPresencaUC")
"/fichaConservacao/saveFrmConPresencaUC"( controller:"fichaConservacao", action:"saveFrmConPresencaUC")
"/fichaConservacao/recalcularUcs"( controller:"fichaConservacao", action:"recalcularUcs")
// Aba 7.5 - Situacao Regional
"/fichaConservacao/tabSituacaoRegional"( controller:"fichaConservacao", action:"tabSituacaoRegional")

//----------------------------------------------------------------------------------------------------

// Aba 8 - Ficha/Pesquisa
"/fichaPesquisa/"( controller:"fichaPesquisa", action:"index")
"/fichaPesquisa/saveFrmPesquisa"( controller:"fichaPesquisa", action:"saveFrmPesquisa")
"/fichaPesquisa/saveFrmPesquisaDetalhe"( controller:"fichaPesquisa", action:"saveFrmPesquisaDetalhe")
"/fichaPesquisa/editPesquisa"( controller:"fichaPesquisa", action:"editPesquisa")
"/fichaPesquisa/deletePesquisa"( controller:"fichaPesquisa", action:"deletePesquisa")

//----------------------------------------------------------------------------------------------------

// Aba 11 - Ficha/Avaliação
"/fichaAvaliacao/"( controller:"fichaAvaliacao", action:"index")

// Aba 11.1 - Distribuição
"/fichaAvaliacao/tabAvaDistribuicao"( controller:"fichaAvaliacao", action:"tabAvaDistribuicao")
"/fichaAvaliacao/saveFrmDistribuicao"( controller:"fichaAvaliacao", action:"saveFrmDistribuicao")

// Aba 11.2 - População
"/fichaAvaliacao/tabAvaPopulacao"( controller:"fichaAvaliacao", action:"tabAvaPopulacao")
"/fichaAvaliacao/saveFrmPopulacao"( controller:"fichaAvaliacao", action:"saveFrmPopulacao")
"/fichaAvaliacao/addDeclinioPopulacional"( controller:"fichaAvaliacao", action:"addDeclinioPopulacional")
"/fichaAvaliacao/deleteDeclinioPopulacional"( controller:"fichaAvaliacao", action:"deleteDeclinioPopulacional")

// Aba 11.3 - Ameaças e Análise Quantitativa
"/fichaAvaliacao/tabAvaAnaliseQuantitativa"( controller:"fichaAvaliacao", action:"tabAvaAnaliseQuantitativa")
"/fichaAvaliacao/saveFrmAnaliseQuantitativa"( controller:"fichaAvaliacao", action:"saveFrmAnaliseQuantitativa")

// Aba 11.4 - Ajustes Regionais
"/fichaAvaliacao/tabAvaAjusteRegional"( controller:"fichaAvaliacao", action:"tabAvaAjusteRegional")
"/fichaAvaliacao/saveFrmAjusteRegional"( controller:"fichaAvaliacao", action:"saveFrmAjusteRegional")

// aba 11.5 - Avaliação Expedita
 "/fichaAvaliacao/tabAvaExpedita"( controller:"fichaAvaliacao", action:"tabAvaExpedita")
 "/fichaAvaliacao/saveFrmAvaExpedita"( controller:"fichaAvaliacao", action:"saveFrmAvaExpedita")

// Aba 11.6 - Resultado Avaliacao
"/fichaAvaliacao/tabAvaResultado"( controller:"fichaAvaliacao", action:"tabAvaResultado")
"/fichaAvaliacao/saveResultadoAvaliacao"( controller:"fichaAvaliacao", action:"saveResultadoAvaliacao")
"/fichaAvaliacao/saveAutoria"( controller:"fichaAvaliacao", action:"saveAutoria")
"/fichaAvaliacao/saveEquipeTecnica"( controller:"fichaAvaliacao", action:"saveEquipeTecnica")
"/fichaAvaliacao/saveColaboradores"( controller:"fichaAvaliacao", action:"saveColaboradores")

// Aba 11.7 - Resultado Validação
"/fichaAvaliacao/tabAvaValidacao"( controller:"fichaAvaliacao", action:"tabAvaValidacao")
"/fichaAvaliacao/saveResultadoValidacao"( controller:"fichaAvaliacao", action:"saveResultadoValidacao")

// Aba 11.8 - Resultado Final
"/fichaAvaliacao/tabAvaResultadoFinal"( controller:"fichaAvaliacao", action:"tabAvaResultadoFinal")
"/fichaAvaliacao/saveFrmResultadoFinal"( controller:"fichaAvaliacao", action:"saveFrmResultadoFinal")

//----------------------------------------------------------------------------------------------------

// aba 9 - Ficha Ref. Bibliograficas
"/fichaRefBib/"( controller: "fichaRefBib", action: "index")
"/ficha/getLocaisRefBibUtilizada/"( controller: "ficha", action: "getLocaisRefBibUtilizada")

//----------------------------------------------------------------------------------------------------

// aba 11 - Ficha Pendências
"/fichaPendencia/"( controller: "fichaPendencia", action: "getGridPendencia")
"/fichaPendencia/saveFrmPendencia"( controller:"fichaPendencia", action:"saveFrmPendencia")
"/fichaPendencia/editPendencia"( controller:"fichaPendencia", action:"editPendencia")
"/fichaPendencia/deletePendencia"( controller:"fichaPendencia", action:"deletePendencia")


// aba 12 - aba Multimidia
"/fichaMultimidia/"( controller:"fichaMultimidia", action:"index")
"/fichaMultimidia/save"( controller:"fichaMultimidia", action:"save")
"/fichaMultimidia/edit"( controller:"fichaMultimidia", action:"edit")
"/fichaMultimidia/delete"( controller:"fichaMultimidia", action:"delete")
"/fichaMultimidia/getGrid"( controller:"fichaMultimidia", action:"getGrid")
"/fichaMultimidia/getGridMultimidiaOcorrencia"( controller:"fichaMultimidia", action:"getGridMultimidiaOcorrencia")
"/fichaMultimidia/getMultimidia/$id/$thumb?"( controller:"fichaMultimidia", action:"getMultimidia")
"/fichaMultimidia/toggleImagemPrincipal"( controller:"fichaMultimidia", action:"toggleImagemPrincipal")
"/fichaMultimidia/toggleImagemDestaque"( controller:"fichaMultimidia", action:"toggleImagemDestaque")


// aba 13 - aba Creditos
"/fichaCreditos/"( controller:"ficha", action:"creditos")

// aba 14 - aba versões
"/fichaVersoes/"        ( controller:"ficha", action:"versoes")
"/ficha/publicarVersao" ( controller:"ficha", action:"publicarVersao")

//----------------------------------------------------------------------------------------------------

// Modulo Referência Bibliográfica
"/referenciaBibliografica/save"( controller: "referenciaBibliografica", action: "save")
"/referenciaBibliografica/edit"( controller: "referenciaBibliografica", action: "edit")
"/referenciaBibliografica/getGrid"( controller: "referenciaBibliografica", action: "getGrid")
"/referenciaBibliografica/getGridAnexos"( controller: "referenciaBibliografica", action: "getGridAnexos")
"/referenciaBibliografica/delete"( controller: "referenciaBibliografica", action: "delete")
"/referenciaBibliografica/deleteAnexo"( controller: "referenciaBibliografica", action: "deleteAnexo")
"/referenciaBibliografica/downloadAnexo"( controller: "referenciaBibliografica", action: "downloadAnexo")
"/referenciaBibliografica/getGridImportacao"( controller: "referenciaBibliografica", action: "getGridImportacao")
"/referenciaBibliografica/deletePlanilha"( controller: "referenciaBibliografica", action: "deletePlanilha")
"/referenciaBibliografica/getGridFichasPublicacao"( controller: "referenciaBibliografica", action: "getGridFichasPublicacao")
"/referenciaBibliografica/saveCorrecaoDuplicidade"( controller: "referenciaBibliografica", action: "saveCorrecaoDuplicidade")


// Módulo cadastro de instituição
"/instituicao/save"( controller: "instituicao", action: "save")
"/instituicao/delete"( controller: "instituicao", action: "delete")
"/instituicao/edit"( controller: "instituicao", action: "edit")
"/instituicao/pesquisar"( controller: "instituicao", action: "pesquisar")





// modulo gerenciar validacao
"/gerenciarValidacao"(controller: "gerenciarValidacao")
"/gerenciarValidacao/getFormSelecionarFicha"( controller: "gerenciarValidacao", action: "getFormSelecionarFicha")
"/gerenciarValidacao/getGridFichasCadastro"(controller: "gerenciarValidacao", action:'getGridFichasCadastro')
"/gerenciarValidacao/saveOficina"(controller: "gerenciarValidacao", action:'saveOficina')
"/gerenciarValidacao/getListaFichasOficina"(controller: "gerenciarValidacao", action:'getListaFichasOficina')
"/gerenciarValidacao/deleteFichaOficina"(controller: "gerenciarValidacao", action:'deleteFichaOficina')
"/gerenciarValidacao/getDashboard"(controller: "gerenciarValidacao", action:'getDashboard')
"/gerenciarValidacao/getDashboardOficina"(controller: "gerenciarValidacao", action:'getDashboardOficina')

// aba convidar validador
"/gerenciarValidacao/getFormPessoa"(controller: "gerenciarValidacao", action:'getFormPessoa')
"/gerenciarValidacao/getGridFichasOficina"(controller: "gerenciarValidacao", action:'getGridFichasOficina')
"/gerenciarValidacao/saveFrmValidadores"(controller: "gerenciarValidacao", action:'saveFrmValidadores')
"/gerenciarValidacao/getGridConvites"(controller: "gerenciarValidacao", action:'getGridConvites')
"/gerenciarValidacao/getFichasValidador"(controller: "gerenciarValidacao", action:'getFichasValidador')
"/gerenciarValidacao/sendEmailValidadores"(controller: "gerenciarValidacao", action:'sendEmailValidadores')
"/gerenciarValidacao/deleteConvite"(controller: "gerenciarValidacao", action:'deleteConvite')
"/gerenciarValidacao/select2Validador"(controller: "gerenciarValidacao", action:'select2Validador')
"/gerenciarValidacao/select2Taxon"(controller: "gerenciarValidacao", action:'select2Taxon')
"/gerenciarValidacao/getGridOficinas"(controller:"gerenciarValidacao", action:"getGridOficinas")
"/gerenciarValidacao/selectOficinas"(controller:"gerenciarValidacao", action:"selectOficinas")
"/gerenciarValidacao/saveEmailParticipante"(controller:"gerenciarValidacao", action:"saveEmailParticipante")

 // aba acompanhamento
"/gerenciarValidacao/getTelaAcompanhamento"( controller: "gerenciarValidacao", action: "getTelaAcompanhamento")
"/gerenciarValidacao/getGridAcompanhamento"( controller: "gerenciarValidacao", action: "getGridAcompanhamento")
"/gerenciarValidacao/getFormFundamentacao"( controller: "gerenciarValidacao", action: "getFormFundamentacao")
"/gerenciarValidacao/marcarComunicarAlteracao"( controller: "gerenciarValidacao", action: "marcarComunicarAlteracao")
"/gerenciarValidacao/enviarEmailPendenciaValidacao"( controller: "gerenciarValidacao", action: "enviarEmailPendenciaValidacao")
"/gerenciarValidacao/getGridFichasLc"( controller: "gerenciarValidacao", action: "getGridFichasLc")
"/gerenciarValidacao/getTabelaTotais"( controller: "gerenciarValidacao", action: "getTabelaTotais")
"/gerenciarValidacao/getFormConvidarValidador"( controller: "gerenciarValidacao", action: "getFormConvidarValidador")
"/gerenciarValidacao/saveFormConvidarValidador"( controller: "gerenciarValidacao", action: "saveFormConvidarValidador")
"/gerenciarValidacao/getNomesValidadoresOficina"( controller: "gerenciarValidacao", action: "getNomesValidadoresOficina")

 // modulo dos validadores
 "/gerenciarValidacao/validador"( controller: "gerenciarValidacao", action: "indexValidador")
 "/gerenciarValidacao/getGridFichasValidador"( controller: "gerenciarValidacao", action: "getGridFichasValidador")
 "/gerenciarValidacao/saveFundamentacao"( controller: "gerenciarValidacao", action: "saveFundamentacao")
 "/gerenciarValidacao/getFormEditarFichaValidador"( controller: "gerenciarValidacao", action: "getFormEditarFichaValidador")
 "/gerenciarValidacao/getGridEditarFichasValidador"( controller: "gerenciarValidacao", action: "getGridEditarFichasValidador")
 "/gerenciarValidacao/deleteFichaValidador"( controller: "gerenciarValidacao", action: "deleteFichaValidador")
 "/gerenciarValidacao/transferirFichaValidador"( controller: "gerenciarValidacao", action: "transferirFichaValidador")

 // modulo Revisão da Coordenação
 "/gerenciarValidacao/getFormRevisaoCoordenacao"( controller: "gerenciarValidacao", action: "getFormRevisaoCoordenacao")
 "/gerenciarValidacao/saveFormRevisaoCoordenacao"( controller: "gerenciarValidacao", action: "saveFormRevisaoCoordenacao")
 "/gerenciarValidacao/deleteRevisaoCoordenacao"( controller: "gerenciarValidacao", action: "deleteRevisaoCoordenacao")




// modulo gerenciar papel na ficha
"/gerenciarPapel"(controller: "gerenciarPapel")
"/gerenciarPapel/getGrid"(controller: "gerenciarPapel", action: "getGrid")
"/gerenciarPapel/getGridFichas"(controller: "gerenciarPapel", action: "getGridFichas")
"/gerenciarPapel/save"(controller: "gerenciarPapel", action: "save")
"/gerenciarPapel/delete"(controller: "gerenciarPapel", action: "delete")
"/gerenciarPapel/deleteAllFichas"(controller: "gerenciarPapel", action: "deleteAllFichas")
"/gerenciarPapel/setSituacao"(controller: "gerenciarPapel", action: "setSituacao")
"/gerenciarPapel/getFichasPessoaFuncao"(controller: "gerenciarPapel", action: "getFichasPessoaFuncao")
"/gerenciarPapel/select2PessoaFisica"(controller: "gerenciarPapel", action: "select2PessoaFisica")

// modulo gerencia consulta Ampla e Direta
"/gerenciarConsulta"(controller: "gerenciarConsulta")
"/gerenciarConsulta/getGridFichas"(controller: "gerenciarConsulta", action: "getGridFichas")
"/gerenciarConsulta/getGridConsultas"(controller: "gerenciarConsulta", action: "getGridConsultas")
"/gerenciarConsulta/saveConsulta"(controller: "gerenciarConsulta", action: "saveConsulta")
"/gerenciarConsulta/editConsulta"(controller: "gerenciarConsulta", action: "editConsulta")
"/gerenciarConsulta/deleteConsulta"(controller: "gerenciarConsulta", action: "deleteConsulta")
"/gerenciarConsulta/getListaFichas"(controller: "gerenciarConsulta", action: "getListaFichas")
"/gerenciarConsulta/deleteFichaConsulta"(controller: "gerenciarConsulta", action: "deleteFichaConsulta")

// aba convidar
"/gerenciarConsulta/convidar"(controller: "gerenciarConsulta", action: "convidar")
"/gerenciarConsulta/getGridFichasConvidar"(controller: "gerenciarConsulta", action: "getGridFichasConvidar")
"/gerenciarConsulta/saveConvidado"(controller: "gerenciarConsulta", action: "saveConvidado")
"/gerenciarConsulta/getListaFichasConvidado"(controller: "gerenciarConsulta", action: "getListaFichasConvidado")
"/gerenciarConsulta/deleteFichaConsultaConvidado"(controller: "gerenciarConsulta", action: "deleteFichaConsultaConvidado")

"/gerenciarConsulta/consolidar"(controller: "gerenciarConsulta", action: "consolidar")
"/gerenciarConsulta/getGridConsolidar"(controller: "gerenciarConsulta",action:"getGridConsolidar")
"/gerenciarConsulta/getGridColaboracoes"(controller: "gerenciarConsulta",action:"getGridColaboracoes")
"/gerenciarConsulta/changeManterLc"(controller: "gerenciarConsulta",action:"changeManterLc")
"/gerenciarConsulta/updateFicha"(controller: "gerenciarConsulta",action:"updateFicha")
"/gerenciarConsulta/changeSituacaoColaboracao"(controller: "gerenciarConsulta",action:"changeSituacaoColaboracao")
"/gerenciarConsulta/changeSituacaoColaboracaoPlanilha"(controller: "gerenciarConsulta",action:"changeSituacaoColaboracaoPlanilha")
"/gerenciarConsulta/getGridConvidadosConvidar"(controller: "gerenciarConsulta",action:"getGridConvidadosConvidar")
"/gerenciarConsulta/enviarEmailConvidados"(controller: "gerenciarConsulta",action:"enviarEmailConvidados")
"/gerenciarConsulta/getTextLastEmail"(controller: "gerenciarConsulta",action:"getTextLastEmail")
"/gerenciarConsulta/editConvidado"(controller: "gerenciarConsulta",action:"editConvidado")
"/gerenciarConsulta/deleteConvidado"(controller: "gerenciarConsulta",action:"deleteConvidado")
"/gerenciarConsulta/changeSituacaoConvite"(controller: "gerenciarConsulta",action:"changeSituacaoConvite")
"/gerenciarConsulta/updateSituacaoFicha"(controller: "gerenciarConsulta",action:"updateSituacaoFicha")

"/gerenciarConsulta/consolidarFicha"(controller: "gerenciarConsulta",action:"consolidarFicha")




// modulo taxonomia
"/taxonomia"(controller: "taxonomia")
"/taxonomia/cadastro"(controller: "taxonomia", action:"formCadastro")
"/taxonomia/save"    (controller: "taxonomia", action:"save")
"/taxonomia/edit"    (controller: "taxonomia", action:"edit")
"/taxonomia/delete"    (controller: "taxonomia", action:"delete")
"/taxonomia/sinonimia"(controller: "taxonomia", action:"formSinonimia")
"/taxonomia/saveSinonimia"    (controller: "taxonomia", action:"saveSinonimia")
"/taxonomia/editSinonimia"    (controller: "taxonomia", action:"editSinonimia")
"/taxonomia/deleteSinonimia"    (controller: "taxonomia", action:"deleteSinonimia")
"/taxonomia/getGridSinonimias"    (controller: "taxonomia", action:"getGridSinonimias")
"/taxonomia/consultarTaxonRemoto"    (controller: "taxonomia", action:"consultarTaxonRemoto")



// modulo publicacao
"/publicacao"  				   (controller: "publicacao", action:"index")
"/publicacao/getGridFichasPublicadas"    (controller: "publicacao", action:"getGridFichasPublicadas")
"/publicacao/getGridFichasNaoPublicadas"    (controller: "publicacao", action:"getGridFichasNaoPublicadas")
"/publicacao/publicar"    		(controller: "publicacao", action:"publicar")
"/publicacao/cancelar"    		(controller: "publicacao", action:"cancelar")
"/publicacao/selectImages" 		(controller: "publicacao", action:"selectImages")
"/publicacao/updateDestaque"    (controller: "publicacao", action:"updateDestaque")
"/publicacao/saveDoi"           (controller: "publicacao", action:"saveDoi")
"/publicacao/savePendenciaFichas"             (controller: "publicacao", action:"savePendenciaFichas")
"/publicacao/deletePendenciaFichas"           (controller: "publicacao", action:"deletePendenciaFichas")
"/publicacao/deletePendencia"                 (controller: "publicacao", action:"deletePendencia")
"/publicacao/updateSituacaoRevisao"           (controller: "publicacao", action:"updateSituacaoRevisao")
"/publicacao/revisarPendencias"               (controller: "publicacao", action:"revisarPendencias")
"/publicacao/gerarAtualizarDoi"               (controller: "publicacao", action:"gerarAtualizarDoi")
"/publicacao/verificaDoiCrossref"             (controller: "publicacao", action:"verificaDoiCrossref")
"/publicacao/transmitirIucn"                  (controller: "publicacao", action:"transmitirIucn")
"/publicacao/verLogTransmissao"                  (controller: "publicacao", action:"verLogTransmissao")

// módulo usuários web
"/usuarioWeb"  				   	(controller: "usuarioWeb", action:"index")
"/usuarioWeb/getGrid" 		   	(controller: "usuarioWeb", action:"getGrid")
"/usuarioWeb/alterarSenha"		(controller: "usuarioWeb", action:"alterarSenha")
"/usuarioWeb/bloquear"			(controller: "usuarioWeb", action:"bloquear")
"/usuarioWeb/desbloquear"		(controller: "usuarioWeb", action:"desbloquear")
"/usuarioWeb/excluir"			(controller: "usuarioWeb", action:"excluir")
"/usuarioWeb/ativarDesativar"	(controller: "usuarioWeb", action:"ativarDesativar")

// Módulo Público
"/publico"  				   	(controller: "public", action:"index")
"/publico/clearCache"           (controller: "public", action:"clearCache")
"/publico/totais"            	(controller: "public", action:"getTotais")
"/publico/graficosAmeacadas"   	(controller: "public", action:"getGraficosEspeciesAmeacadas")
"/publico/search"  			  	(controller: "public", action:"search")
"/publico/searchTaxon"   	  	(controller: "public", action:"searchTaxon")
"/publico/searchUcs"   	     	(controller: "public", action:"searchUcs")
"/publico/searchAmeacas"   	  	(controller: "public", action:"searchAmeacas")
"/publico/searchUsos"   	  	(controller: "public", action:"searchUsos")
"/publico/searchBacias"   	  	(controller: "public", action:"searchBacias")
"/publico/searchPans"   	  	(controller: "public", action:"searchPans")
"/publico/searchParticipantes" 	(controller: "public", action:"searchParticipantes")
//"/publico/fichaHtml/$sqFicha" 	(controller: "public", action:"fichaHtml")
"/publico/fichaPdf/$sqFicha" 	(controller: "public", action:"fichaPdf")
"/publico/fichaAjax/$sqFicha" 	(controller: "public", action:"fichaAjax")
"/publico/exportOccurrences" 	(controller: "public", action:"exportOccurrences")
"/publico/faleConosco"      	(controller: "public", action:"faleConosco")
"/publico/getAnexo/$id/$thumb?" ( controller:"public", action:"getAnexo")
"/publico/getMultimidia/$id/$thumb?" ( controller:"public", action:"getMultimidia")
"/publico/getRandomImagesDestaque/$qtd?" ( controller:"public", action:"getRandomImagesDestaque")
"/publico/salveNumeros"      	(controller: "public", action:"salveNumeros")
"/publico/getJustificativaExclusao"      	(controller: "public", action:"getJustificativaExclusao")
"/publico/getJustificativaTaxonHistorico"      	(controller: "public", action:"getJustificativaTaxonHistorico")

// integração com sis-iucn
"/traducao/getGridTraducaoTextoFixo"      	(controller: "traducao", action:"getGridTraducaoTextoFixo")
"/traducao/save"      	(controller: "traducao", action:"save")
"/traducao/edit"      	(controller: "traducao", action:"edit")
"/traducao/delete"     	(controller: "traducao", action:"delete")
"/traduzir"          	(controller: "traducao", action:"traduzir")

// traducao das fichas
"/traducaoFicha"      	(controller: "traducaoFicha", action:"index")
"/traducaoFicha/getGridFichaTraducao"  (controller: "traducaoFicha", action:"getGridFichaTraducao")
"/traducaoFicha/traduzirFichasEmLote"  (controller: "traducaoFicha", action:"traduzirFichasEmLote")
"/traducaoFicha/fichaRevisao"  (controller: "traducaoFicha", action:"fichaRevisao")
"/traducaoFicha/saveTextoTraduzido"  (controller: "traducaoFicha", action:"saveTextoTraduzido")
"/traducaoFicha/saveRevisado"  (controller: "traducaoFicha", action:"saveRevisado")
"/traducaoFicha/getPercentuaisPage"  (controller: "traducaoFicha", action:"getPercentuaisPage")
"/traducaoFicha/runAsyncTranslate"  (controller: "traducaoFicha", action:"runAsyncTranslate")
"/traducaoFicha/excluirTraducaoFichasEmLote"  (controller: "traducaoFicha", action:"excluirTraducaoFichasEmLote")
"/traducaoFicha/gerarPdfFichas"  (controller: "traducaoFicha", action:"gerarPdfFichas")


		/*
		"/$control/?**" {
			controller = "main"
			action = "index"
			constraints {
				control(validator: {
					def controller = it;
					println 'Controller:'+it;
					return true; })
			}
		}
		*/
	}
}
