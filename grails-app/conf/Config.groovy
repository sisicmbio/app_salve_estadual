import org.apache.log4j.*


// variaveis de configuracao
default_config = [:]
default_config.appName = 'salve-estadual'

// criar diretórios em /data
File configDir = new File( "/data/${default_config.appName}")
if( ! configDir.exists() ) {
    if( ! configDir.mkdirs()){
        println ' '
        println ' '
        println 'Erro inicializando a aplicacao SALVE-Estadual.'
        println '  - diretorio /data nao encontrado ou sem acesso de leitura e escrita'
        println ' '
        System.exit(1);
    }
}

if( ! new File( "/data/${default_config.appName}/logs").exists() ) {
    new File("/data/${default_config.appName}/logs").mkdirs()
}

default_config.file = "/data/${default_config.appName}/config.properties"
File configFile = new File( default_config.file )
if( ! configFile.exists() ) {
    println ' '
    println ' '
    println ' '
    println 'Erro inicializando a aplicacao SALVE-Estadual.'
    println '  - Arquivo de configuracoes '+default_config.file+' inexistente'
    println '# ------------------------------------------------------------';
    println '# Exemplo configuracao da conexao com postgres';
    println 'dataSource.url=jdbc:postgresql://localhost/db_salve_esadual';
    println 'dataSource.username=postgres';
    println 'dataSource.password=postgres';
    println 'dataSource.configClass=br.gov.icmbio.ForeingKeyRename';
    println 'dataSource.dbCreate=none';
    println '#------------------------------------------------------------'
    System.exit(1);
}

// nenhuma configura externa
grails.config.locations = []
grails.config.locations.add( "file:" + default_config.file )


/*
// ler o arquivo de configuracoes da variavel de ambiete
if( System.getenv(ENV_NAME)  &&  new File( System.getenv(ENV_NAME) ).exists())
{
  println 'OK - Lendo arquivo de configuracoes da variavel ENV_NAME';
  grails.config.locations.add( "file:" + System.getenv(ENV_NAME) );
}
else if( new File(default_config).exists() )
{
  println 'OK - Lendo arquivo de configuracoes em '+default_config
  grails.config.locations.add( "file:" + default_config );
}
else
{
  println 'Não existe arquivo de configuracoes externo.'
}
*/
//System.exit(1);
//
//
//
// locations to search for config files that get merged into the main config;
// config files can be ConfigSlurper scripts, Java properties files, or classes
// in the classpath in ConfigSlurper format

// grails.config.locations = [ "classpath:${appName}-config.properties",
//                             "classpath:${appName}-config.groovy",
//                             "file:${userHome}/.grails/${appName}-config.properties",
//                             "file:${userHome}/.grails/${appName}-config.groovy"]

// if (System.properties["${appName}.config.location"]) {
//    grails.config.locations << "file:" + System.properties["${appName}.config.location"]
// }

grails.gorm.failOnError = false
grails.gorm.autoFlush = false

grails.project.groupId = 'br.gov.icmbio'

// The ACCEPT header will not be used for content negotiation for user agents containing the following strings (defaults to the 4 major rendering engines)
grails.mime.disable.accept.header.userAgents = ['Gecko', 'WebKit', 'Presto', 'Trident']
grails.mime.types = [ // the first one is the default format
    all:           '*/*', // 'all' maps to '*' or the first available format in withFormat
    zip:           'application/zip',
    atom:          'application/atom+xml',
    css:           'text/css',
    csv:           'text/csv',
    form:          'application/x-www-form-urlencoded',
    html:          ['text/html','application/xhtml+xml'],
    js:            'text/javascript',
    json:          ['application/json', 'text/json'],
    multipartForm: 'multipart/form-data',
    rss:           'application/rss+xml',
    text:          'text/plain',
    hal:           ['application/hal+json','application/hal+xml'],
    xml:           ['text/xml', 'application/xml'],
    png:           ['image/png'],
    pdf:           'application/pdf'
]

// URL Mapping Cache Max Size, defaults to 5000
//grails.urlmapping.cache.maxsize = 1000

// Legacy setting for codec used to encode data with ${}
grails.views.default.codec = "html"
grails.views.gsp.encoding = "UTF-8"
grails.converters.encoding = "UTF-8"

// The default scope for controllers. May be prototype, session or singleton.
// If unspecified, controllers are prototype scoped.
grails.controllers.defaultScope = 'singleton'

// GSP settings
grails {
    views {
        gsp {
            encoding = 'UTF-8'
            htmlcodec = 'xml' // use xml escaping instead of HTML4 escaping
            codecs {
                expression = 'html' // escapes values inside ${}
                scriptlet = 'html' // escapes output from scriptlets in GSPs
                taglib = 'none' // escapes output from taglibs
                staticparts = 'none' // escapes output from static template parts
            }
        }
        // escapes all not-encoded output at final stage of outputting
        // filteringCodecForContentType.'text/html' = 'html'
    }
}


grails.converters.encoding = "UTF-8"
// scaffolding templates configuration
grails.scaffolding.templates.domainSuffix = 'Instance'

// Set to false to use the new Grails 1.2 JSONBuilder in the render method
grails.json.legacy.builder = false
// enabled native2ascii conversion of i18n properties files
grails.enable.native2ascii = true
// packages to include in Spring bean scanning
grails.spring.bean.packages = []
// whether to disable processing of multi part requests
grails.web.disable.multipart=false

// request parameters to mask when logging exceptions
grails.exceptionresolver.params.exclude = ['password']

// configure auto-caching of queries by default (if false you can cache individual queries with 'cache: true')
grails.hibernate.cache.queries = false

// configure passing transaction's read-only attribute to Hibernate session, queries and criterias
// set "singleSession = false" OSIV mode in hibernate configuration after enabling
grails.hibernate.pass.readonly = false
// configure passing read-only to OSIV session by default, requires "singleSession = false" OSIV mode
grails.hibernate.osiv.readonly = false

environments {
    development {
        grails.logging.jul.usebridge = true
    }
    production {
        grails.logging.jul.usebridge = false
        // TODO: grails.serverURL = "http://www.changeme.com"
    }
}


// https://logging.apache.org/log4j/1.2/apidocs/org/apache/log4j/DailyRollingFileAppender.html
// https://gist.github.com/steinwaywhw/ff46c692a50c8dd4cdc3
// http://grails-dev.blogspot.com/2012/09/setting-up-log4j-configuration-in.html

// log4j configuration
/*
off
fatal
error
warn
info
debug
trace
all
*/
def logFileName = "/data/"+default_config.appName+"/logs/salve.log"
log4j.main = {
    // https://logging.apache.org/log4j/1.2/apidocs/org/apache/log4j/PatternLayout.html
    def pattern = new PatternLayout("[%p] [%d{ISO8601}] [%c{1}] %m%n")
    appenders {
        appender new DailyRollingFileAppender(
                name:"file",
                file:logFileName,
                layout: pattern,
                datePattern: "'.'yyyy-MM-dd")
        console name:"stdout",
                layout: pattern
    }
    environments {
        development {
            root {
                info "file","stdout"
            }
        }
        production  {
            root {
                info "file"
            }
        }
    }

    all  'grails.app'

    debug  'org.codehaus.groovy.grails.web.servlet'

    info 'org.codehaus.groovy.grails.web.pages',          // GSP
         'org.codehaus.groovy.grails.web.sitemesh'       // layouts
       //'org.codehaus.groovy.grails.web.servlet'        // controllers

    warn    'org.springframework',
            'org.hibernate',
            'grails.plugins.springsecurity',
            'groovyx.net.http'

    error   'org.codehaus.groovy.grails.web.servlet',        // controllers
            'org.codehaus.groovy.grails.web.pages',          // GSP
            'org.codehaus.groovy.grails.web.sitemesh',       // layouts
            'org.codehaus.groovy.grails.web.mapping.filter', // URL mapping
            'org.codehaus.groovy.grails.web.mapping',        // URL mapping
            'org.codehaus.groovy.grails.commons',            // core / classloading
            'org.codehaus.groovy.grails.plugins',            // plugins
            'org.codehaus.groovy.grails.orm.hibernate',      // hibernate integration
            'org.springframework',
            'org.hibernate',
            'net.sf.ehcache.hibernate'
}












grails.databinding.dateFormats = [
        'dd-MM-yyyy', 'dd-MM-yyyy HH:mm:ss.S', "dd-MM-yyyy'T'HH:mm:ss'Z'"
]

grails.assets.bundle=false
grails.assets.minifyJs=false
grails.assets.minifyCss=false

grails.plugin.wkhtmltox.binary="/usr/bin/wkhtmltopdf"
grails.cache.enabled = true

/*
grails {
    mongodb {
        host = "localhost"
        port = 27017
        username = ""
        password = ""
        databaseName = "test"
        stateless = false
    }
}
*/



// TESTAR A CONEXÃO COM BANCO DE DADOS
//--------------------------------------------------------------------------------------------
/*

// NAO FUNCIONOU DEVIDO AO ERRO: No suitable driver found for jdbc:postgresql://localhost:54001/db_dev_cotec

List lines = configFile.getText().split("\n")
String dataSourceUrl = lines.find{it =~ /^dataSource\.url=/}?.split('dataSource.url=')?.getAt(1)
String dataSourceUserName = lines.find{it =~ /^dataSource\.username=/}?.split('dataSource.username=')?.getAt(1)
String dataSourcePassword = lines.find{it =~ /^dataSource\.password=/}?.split('dataSource.password=')?.getAt(1)
if( !dataSourceUrl || !dataSourceUserName || !dataSourcePassword ){
    println ' '
    println ' '
    println 'ERRO SALVE-ESTADUAL - configuracao da conexao com banco de dados nao definida ou incompleta no arquivo ' + configFile.getPath()
    println 'Parametros necessarios:'
    println 'dataSource.url=jdbc:postgresql://localhost:5432/db_salve_estadual'
    println 'dataSource.username=postgres'
    println 'dataSource.password=postgre$'
    System.exit(1);
}

try {
    Class.forName("org.postgresql.Driver");
    //Connection conexao = DriverManager.getConnection(dataSourceUrl, dataSourceUserName, dataSourcePassword);
    Connection conexao = DriverManager.getConnection("jdbc:postgresql://localhost:54001/db_dev_cotec",'postgres','postgre$');
} catch (ClassNotFoundException e) {
    // Erro caso o driver JDBC não foi instalado
    println ' '
    println ' '
    println 'ERRO SALVE-ESTADUAL - CONEXÃO COM BANCO DE DADOS - DRIVER NAO INSTALADO';
    println e.getMessage();
    System.exit(1);
} catch (SQLException e) {
    // Erro caso haja problemas para se conectar ao banco de dados
    println ' '
    println ' '
    println 'ERRO CONEXÃO COM BANCO DE DADOS';
    println e.getMessage();
    println 'Parametros utilizados:'
    println 'dataSource.url='+dataSourceUrl
    println 'dataSource.username='+dataSourceUserName
    println 'dataSource.password='+dataSourcePassword.replaceAll(/./,'*')
    System.exit(1);
}
*/
