grails.servlet.version = "3.0" // Change depending on target container compliance (2.5 or 3.0)
grails.project.class.dir = "target/classes"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir = "target/test-reports"
grails.project.work.dir = "target/work"
grails.project.target.level = 1.6
grails.project.source.level = 1.6
grails.project.war.file = "target/${appName}.war"
//grails.server.port.http = 8080

//
grails.project.fork = [
    // configure settings for compilation JVM, note that if you alter the Groovy version forked compilation is required
    //  compile: [maxMemory: 256, minMemory: 64, debug: false, maxPerm: 256, daemon:true],
    // configure settings for the test-app JVM, uses the daemon by default
    test: [maxMemory: 768, minMemory: 64, debug: false, maxPerm: 256, daemon:true],
    //test: false,
    // configure settings for the run-app JVM
    run: [maxMemory: 8192, minMemory:8192, debug: false, maxPerm: 4096, forkReserve:false, permSize:4096],
    // configure settings for the run-war JVM
    war: [maxMemory: 8192, minMemory: 8000, debug: false, maxPerm: 4096, forkReserve:false,permSize:4096, Xlint:'unchecked' ],
    // configure settings for the Console UI JVM
    console: [maxMemory: 768, minMemory: 64, debug: false, maxPerm: 256]
]

grails.project.dependency.resolver = "maven" // or ivy
grails.project.dependency.resolution = {
    // inherit Grails' default dependencies
    inherits("global") {
        // specify dependency exclusions here; for example, uncomment this to disable ehcache:
        // excludes 'ehcache'
    }
    log "error" // log level of Ivy resolver, either 'error', 'warn', 'info', 'debug' or 'verbose'
    checksums true // Whether to verify checksums on resolve
    legacyResolve false // whether to do a secondary resolve on plugin installation, not advised and here for backwards compatibility

    repositories {
        inherits true // Whether to inherit repository definitions from plugins
        grailsCentral()
        mavenCentral()
    }

    dependencies {
        compile "org.apache.commons:commons-csv:1.5"
    }

    plugins {
        build ":tomcat:7.0.70" // or ":tomcat:8.0.22"
        compile ':cache:1.1.8'
        compile ":asset-pipeline:2.9.1"
        runtime ":hibernate4:4.3.10" // or ":hibernate:3.6.10.18"
        runtime ":database-migration:1.4.0"
        compile "org.grails.plugins:rest:0.8"
        compile "org.grails.plugins:html-cleaner:0.4"
        compile "org.grails.plugins:ziplet:0.5", {excludes "slf4j-nop"}
        compile ':ziplet:0.5' // https://grails.org/plugin/ziplet
        compile "org.grails.plugins:quartz:1.0.2" // executar os jobs criados no diretório /jobs/
        compile "org.grails.plugins:executor:0.3" // executar tarefas em backgrouns


        // Uncomment these to enable additional asset-pipeline capabilities
        //compile ":sass-asset-pipeline:2.14.1" // https://github.com/bertramdev/sass-grails-asset-pipeline
        //compile ":sass-asset-pipeline:1.9.0"
        //compile ":less-asset-pipeline:1.10.0"
        //compile ":coffee-asset-pipeline:1.8.0"
        //compile ":handlebars-asset-pipeline:1.3.0.3"
    }
}

grails.gorm.failOnError=true

grails.gorm.default.mapping = {
    version false
    autoTimestamp false
}

// testar se é aqui que fica isso ou  no config.groovy
grails.assets.bundle=false
grails.assets.minifyJs=false
grails.assets.minifyCss=false
