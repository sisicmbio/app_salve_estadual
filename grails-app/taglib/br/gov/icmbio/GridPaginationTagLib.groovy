package br.gov.icmbio

class GridPaginationTagLib {
    static defaultEncodeAs = [taglib:'html']
    //static encodeAsForTags = [tagName: [taglib:'html'], otherTagName: [taglib:'none']]
    def gridPagination = { attrs, body ->

        boolean  debug = false
        List errors = []

        // parametros recebidos
        def pagination      = attrs.pagination
        if( ! pagination ) {
            return
        }
        // definir valores padrão dos parametros
        pagination.buttons     = (pagination.buttons ?: 5)

        if( debug ) {
            println ' '
            println  'gridePaginationTagLib() ' + new Date().format('HH:mm:ss')
            pagination.each{ key, value ->
                println '  - ' + key.toString() + '=' + value.toString()
            }
        }


        // validar parametros obrigatórios
        if ( ! pagination ) {
            errors.push('parâmetro "pagination" é obrigatório')
        }
        if ( ! pagination.gridId ) {
            errors.push('parâmetro "gridId" é obrigatório')
        }
        if( errors ) {
            out << raw('<div class="alert alert-danger"><strong>Erro Paginação do gride</strong><ol>'+
                        errors.collect{ '<li>' + it + '</li>' }.join('')+
                    '</ol></div>')
            return
        }

        // deve haver no mínimo 2 páginas para exibir o componente de paginação
        /*
        if( ! pagination.totalPages || pagination.totalPages  < 2 ) {
            return;
        }

        */

        // TOTAL DE REGISTROS
        //out << raw("""<div style="border:none;display:flex;flex-direction:row;flex-wrap: wrap;justify-content: flex-start;align-content: center;">""")
        out << raw("""<div class="row">""")
        out << raw("""<div class="col-sm-12 col-md-2" style="max-width:300px;">""")
            //out<< raw("""<div id="grid${pagination.gridId}TotalRecords" class="grid-total-records" >
            //${ Util.formatInt( pagination.totalRecords.toInteger() ) } registros</div>""")
         out << raw("""<nav>
              <ul class="pagination pagination-sm">
                <li class="disabled">
                  <a id="records${pagination.gridId}" style="padding-top:0px; padding-bottom:1px;cursor:default; !important" href="#" aria-label="Total de registros" data-total-records="${pagination.totalRecords}">
                    <span style="white-space:nowrap;font-size:1.4em !important;" aria-hidden="true">${ Util.formatInt( pagination.totalRecords.toInteger() ) } registro${pagination.totalRecords.toInteger() > 1 ? 's' :''}</span>
                  </a>
                </li>
              </ul
            </nav>
            """)
        out << raw("""</div>""")
        out << raw("""<div class="col-sm-12 col-md-10">""")
        // início da construção do html da paginação
        out << raw("""<nav aria-label="Paginação">
                    <ul class="pagination pagination-pages"
                        data-grid-id="${pagination.gridId}"
                        data-current-page="${pagination.currentPage}"
                        data-total-records="${pagination.totalRecords}"
                        data-total-pages="${pagination.totalPages}"
                        >
                    """)

        /*out<< raw("""<li class="page-item page-total-records disabled">
           <a class="page-link" href="javascript:void(0);" >${ Util.formatInt( pagination.totalRecords.toInteger() ) } registros</a></li>""")
    */
        if( pagination.totalPages  && pagination.totalPages > 1 ) {
            // início
            out << raw("""<li class="page-item first-page${pagination.pageNumber == 1 ? ' disabled' : ''}" onClick="app.gridPageClick(this,1)"><a class="page-link" href="javascript:void(0);">Página Inicial</a></li>""")
            // anterior
            out << raw("""<li class="page-item previous-page${pagination.pageNumber == 1 ? ' disabled' : ''}"  onClick="app.gridPageClick(this,${pagination.pageNumber - 1})"><a class="page-link" href="javascript:void(0);">Anterior</a></li>""")
            // select páginas
            out << raw("""<li class="page-item"><a class="page-link page-link-current-page" href="javascript:void(0);">""")
            out << raw("""<select class="pagination-select" onChange="app.gridPageClick(this,this.value)">""")
            for (i in (1..(pagination.totalPages.toInteger()))) {
                out << raw("""<option value="${i}" ${i.toInteger() == pagination.pageNumber.toInteger() ? 'selected' : ''}>${i}</option>""")
            }
            out << raw("""</select></a></li>""")

            // botões páginas individuais
            if (pagination.pageNumber.toInteger() < pagination.totalPages.toInteger() && pagination.buttons.toInteger() > 0) {
                Integer pageNumber  = pagination.pageNumber.toInteger()
                Integer totalPages  = pagination.totalPages.toInteger()
                Integer buttons     = pagination.buttons.toInteger()
                for ( int i in ( ( Math.min(  (pageNumber + 1i), totalPages ) )..(Math.min( ( pageNumber + buttons ), totalPages ) ) ) ) {
                //for ( int i in ( ( Math.min((pagination.pageNumber.toInteger() + 1i), pagination.totalPages.toInteger()))..(Math.min((pagination.pageNumber.toInteger() + pagination.buttons.toInteger()), pagination.totalPages.toInteger() ) ) ) ) {
                    out << raw("""<li class="page-item"><a class="page-link" href="javascript:void(0);" onClick="app.gridPageClick(this, ${i.toInteger()})">${i.toInteger()}</a></li>""")
                }
            }

            // botões finais
            if (pagination.pageNumber.toInteger() + pagination.buttons.toInteger() < pagination.totalPages.toInteger()) {
                // próxima
                out << raw("""<li class="page-item"><a class="page-link" href="javascript:void(0);" onClick="app.gridPageClick(this, ${pagination.pageNumber + 1} )">Próxima</a></li>""")
                // última
                out << raw("""<li class="page-item"><a class="page-link" href="javascript:void(0);" onClick="app.gridPageClick(this, ${pagination.totalPages} )">Última</a></li>""")
            }
        }

        // fim paginação
        out << raw("""</ul></nav>""")
        out << raw("""</div>""")
        out << raw("""</div>""")
        out << raw("""</div>""")
        // fim  da construção do html da paginação
    }
}
