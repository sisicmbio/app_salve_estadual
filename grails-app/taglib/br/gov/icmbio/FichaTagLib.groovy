package br.gov.icmbio

import org.jsoup.Jsoup
import org.jsoup.safety.Whitelist

class FichaTagLib {
    static defaultEncodeAs = [taglib: 'html']
    //static encodeAsForTags = [tagName: [taglib:'html'], otherTagName: [taglib:'none']]

    def renderPanelColaboracoes = { attrs, body ->

        def ficha = attrs.ficha
        def campoFicha = attrs.campoFicha
        def campoColaboracao = attrs.campoColaboracao
        def rotulo = attrs.rotulo
        def listSituacaoColaboracao = attrs.listSituacaoColaboracao
        def edit = attrs.edit
        def button = ''
        def opened = (attrs.opened == 'true')
        def id = campoColaboracao + '-' + ficha.id
        def lista = []
        def verColaboracoes = attrs.verColaboracoes
        def contexto = (attrs?.contexto ?: '')
        def situacaoColaboracao = ''
        def grupo = attrs?.grupo ?: ''
        def canModify = attrs?.canModify.toString() != 'false' ? 'true' : 'false'
        def hideStatus = (attrs.hideStatus == 'true') // retirar marcação de quantas pendências faltam. Ex: 0/3
        Long sqFichaVersao  = attrs?.sqFichaVersao ?  attrs?.sqFichaVersao.toLong() : null

        if (verColaboracoes == 'true') {
            if (contexto == 'oficina') {
                situacaoColaboracao = 'RESOLVER_OFICINA'
            }
            // exibir as colaborações nos modulos consulta e avaliação e não exibir na validacao
            if (contexto =~ /consulta|ficina/ ) {
                lista = ficha.getColaboracoes(campoColaboracao, situacaoColaboracao, sqFichaVersao)
            }
        }
        if (rotulo == '...') {
            opened = true
        }

/** /
 println 'Edit:'+edit
 println campoColaboracao
 println contexto
 println verColaboracoes
 println "-"*100
 println ' '
 /**/


        if (!ficha) {
            out << '<h3>Ficha não informada</h3>'
            return
        }

        if (verColaboracoes != 'true') {
            edit = 'false'
            opened = true
        }

        if (canModify == 'false') {
            edit = 'false'
        }

        if (canModify == 'true') {
            if (hideStatus) {
                button = ''
                opened = true
            } else {
                if (contexto =~ /consulta|oficina/) {
                    button = '&nbsp;<span id="sub-status-' + id + '" class="green"></span>'
                }
            }

            if (edit == 'true') {
                button += '&nbsp;<a href="javascript:void(0);" class="btn btn-xs btn-default" id="btn-' + id + '" data-campo-colaboracao="' + campoColaboracao + '" data-target="div-' + id + '" onClick="exibirEditor(this)" style="display:' + (opened ? '' : 'none') + '">Editar</a>'
            }
        } else {
            if (contexto =~ /consulta|oficina/) {
                button = '&nbsp;<span id="sub-status-' + id + '" class="green"></span>'
            }
        }

        out << raw("""
            <div class="panel panel-success panel-colaboracao mt10 text-left" style="margin-bottom:-2px;" id="panel-${
            id
        }" data-editor="S">
                <div class="panel-heading">
                    <a class="cursor-pointer" data-toggle="collapse" data-target="#panel-${id}-body">${
            (rotulo != '...' ? rotulo : '<i title="Abrir/Fechar" class="hidden fa fa-address-card fa-fw" aria-hidden="true"></i>')
        }</a>${button}
                </div>
                <div class="panel-body panel-collapse collapse${opened ? ' in' : ''}" id="panel-${id}-body">""")

        if (edit == 'true') {
            out << raw("""
                        <div class="texto" id="div-${id}" data-sq-ficha="${ficha?.id}" data-campo-ficha="${
                campoFicha
            }">${ ficha[campoFicha ] ? ficha[campoFicha ].replaceAll(/font-family/,'ff').replaceAll(/font-size/,'fs'): ''}</div>
                    """)

        }else {
            out << raw("""
                        <p class="texto">${ficha[campoFicha] ?: ''}</p>
                    """)
        }
        if (lista) {
            out << raw("""<div style="color:#179ce5;">Colaborações</div>
                        <table class="table table-condensed table-sm table-hover table-striped table-bordered table-colaboracao" style="max-width:100%;">
                            <thead>
                            <tr>
                                <th style="width:110px">Data</th>
                                <th>Colaborador</th>
                                <th>Colaboração</th>
                                <th style="width:170px;">Situação</th>
                            </tr>
                            </thead>
                            <tbody>""")
            if (lista.size() == 0) {
                out << raw('<tr><td colspan="4"><h4>Não houve colaboração</h4></td></tr>')
            } else {
                lista.each { item ->
                    out << raw("""
                                   <tr class="${
                        item.situacao.codigoSistema ==~ ( contexto == 'oficina' ? /NAO_AVALIADA|RESOLVER_OFICINA/ : /NAO_AVALIADA/) ? 'colaboracao-nao-avaliada' : 'colaboracao-avaliada'
                    }" data-contexto="${contexto}" data-fonte="consulta">
                                    <td class="text-center">${item.dtAlteracao.format('dd/MM/yyyy hh:mm')}</td>
                                    <td>${item.usuario.noUsuario}</td>
                                    <td><div style="max-height:400px;max-width:100%;overflow:auto;">${ raw( Util.trimEditor( Util.removeBgWhite( item.dsColaboracao ) ) + '</div>\n<div style="color:#7a7a7a !important;">' + item.dsRefBib + '</div>') }
                    """)

                    if ( canModify == 'true' && contexto != 'validacao' ) {
                        out << raw("""<td class="text-center" style="color:inherit !important;">
                                        <select id="selectColaboracao${item.id}"
                                        class="fldSelect fld170 form-control" style="color:inherit !important;"
                                        data-grupo="${grupo}" data-campo-ficha="${id}" data-params="contexto:${contexto},id:${item.id}" data-change="updateSituacaoColaboracao">""")
                            listSituacaoColaboracao.each {
                            out << raw("""<option data-codigo="${it.codigoSistema}" value="${it.id}" ${
                                it.id == item.situacao.id ? ' selected' : ''
                            }>${it.descricao}</option>""")
                        }
                        out << raw("""
                                            </select>
                                            <div id="divWrapperTextoNaoAceita${item.id}" style="display:${item?.txNaoAceita?'block':'none'};">
                                                <div id="divTextoNaoAceita${item.id}" class="fldTextoLivre" style="font-size: 12px !important;">${item?.txNaoAceita?:''}</div>
                                                <button type="button" title="Editar a justificativa" data-action="editNaoAceita" data-id="${item.id}" class="btn btn-xs btn-default" title="Editar a justificativa.">Editar</button>
                                            </div>
                                            </td>
                                            """)
                    } else {
                        out << raw("""
                                            <td class="text-center">
                                                <span data-grupo="${grupo}" data-campo-ficha="${id}" class="revisor ${item.situacao.codigoSistema=='NAO_AVALIADA'?'colaboracao-nao-avaliada':'colaboracao-avaliada'}">${ item.situacao.descricao}</span>
                                            <div id="divWrapperTextoNaoAceita${item.id}" style="display:${item?.txNaoAceita?'block':'none'};">
                                                <div id="divTextoNaoAceita${item.id}" class="fldTextoLivre" style="font-size: 12px !important;">${item?.txNaoAceita?:''}</div>
                                            </div>
                                            </td>""")
                    }
                    out << raw("""
                                    </tr>
                                """)
                }
            }
            out << raw("""
                            </tbody>
                        </table>""")
        }
        out << raw("""
                    </div>
               </div>
            """)
    }

    def renderLongText = { attrs, body ->
        Integer maxLength   = attrs.maxLength ? attrs.maxLength.toInteger() : 150i
        String title        = attrs.title ?: 'Ver o texto completo'
        String textColor    = attrs.textColor ?: '#000'
        attrs.text = attrs.text?:''
        if( !attrs.text ) {
            maxLength = 0
        }
        //String texto = Util.stripTags( attrs.text.trim() ).trim()
        String texto = Util.html2str(Jsoup.clean( attrs.text, Whitelist.none().addTags('i','b','strong','em') ) )
        if( maxLength && texto.length().toInteger() < ( maxLength + 1) )
        {
            texto = '<p class="elipse"><span>' + texto+ '<span></p>'
        }
        else {

            if ( maxLength ) {
                Integer diferenca = maxLength - Util.stripTags(texto.substring(0, maxLength)).length()
                if (diferenca > 0) {
                    maxLength += diferenca
                }
                if (texto) {
                    maxLength = Math.min(maxLength, texto.length())
                    texto = '<p class="elipse"><span style="color:'+textColor+'">' + texto.substring(0, maxLength) + '</span><a title="'+title+'" href="javascript:void(0);" data-table="' + attrs.table + '" data-id="' + attrs.id + '" data-column=" ' + attrs.column + '" data-action="app.showText"><i class="fa fa-commenting-o" aria-hidden="true"></i></a><br></p>'
                }
            } else {
                if( texto ) {
                    texto = '<p data-y="1" class="elipse"><span style="color:' + textColor + '">' + texto + '</span><a title="' + title + '" href="javascript:void(0);" data-table="' + attrs.table + '" data-id="' + attrs.id + '" data-column=" ' + attrs.column + '" data-action="app.showText"><i class="fa fa-commenting-o" aria-hidden="true"></i></a><br></p>'
                } else {
                    texto = '<a title="'+title+'" href="javascript:void(0);" data-table="' + attrs.table + '" data-id="' + attrs.id + '" data-column=" ' + attrs.column + '" data-action="app.showText"><i class="fa fa-commenting-o" aria-hidden="true"></i></a>'
                }
            }
        }
        out << raw('<span>'+texto+'</span>') // colocar dentro de uma span caso o corte ocorra dentro de uma tag aberta
    }
}
