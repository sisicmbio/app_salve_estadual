package br.gov.icmbio

class ProgressBarTagLib {
    static defaultEncodeAs = [taglib:'html']
    //static encodeAsForTags = [tagName: [taglib:'html'], otherTagName: [taglib:'none']]

    def progressBar = { attrs, body ->

        def percentual = attrs.percentual.toInteger()
        def label = attrs.label ?: ''
        def cor='success'
        def attrId = attrs?.id ? 'id="'+attrs?.id+'" ' : ''

        if( percentual < 20)
        {
            cor='danger'
        }
        else if( percentual > 19 && percentual < 90)
        {
            cor='warning'
        }

        else if( percentual > 89 && percentual < 100 )
        {
            cor='success-dark'
        }
        else if( percentual > 99 )
        {
            cor='success'
            percentual=100
        }
        out << raw("""
            <div style="display:flex;flex-direction:row; align-items:stretch">
               ${label ? '<div class="label-progressbar" style="margin-right:5px;width:110px;text-align:right;">' + label +'</div>' : '' }
                <div class="progress" style="width:100%;margin-bottom:5px;">
                    <div ${attrId} class="progress-bar progress-bar-${cor}" role="progressbar" aria-valuenow="${percentual}" aria-valuemin="0" aria-valuemax="100" style=";min-width:30px;width: ${percentual.toString() +'%'}">
                         <span>${percentual.toString() +'%'}</span>
                    </div>
                </div>
            </div>
        """)
       // }
    }
}
