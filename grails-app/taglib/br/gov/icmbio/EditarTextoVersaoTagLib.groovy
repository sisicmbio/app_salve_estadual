package br.gov.icmbio

class EditarTextoVersaoTagLib {
    static defaultEncodeAs = [taglib:'html']

    def editarTextoVersao = { attrs, body ->

        out << raw("""
            <p class="bold">${attrs.label?:''}<i
                    onclick="fichaVersao.showInlineEdit(this)"
                    data-target="${attrs.target?:''}"
                    data-colunm="${attrs.column?:''}"
                    data-table="${attrs.table?:''}"
                    data-key="${attrs.jsonKey?:''}"
                    class="ml10 fa fa-edit blue cursor-pointer" title="Editar"></i>
                </p>
                <div class="div-editor-mce" id="${attrs.target?:''}Editor">
                    ${attrs.text?:''}
                </div>
        """)
    }
}
