package br.gov.icmbio

class TraducaoTagLib {
    static defaultEncodeAs = [taglib:'html']
    //static encodeAsForTags = [tagName: [taglib:'html'], otherTagName: [taglib:'none']]

    def textoFixo = { attrs, body ->
        def rndId               = Util.randomString(6).toLowerCase()
        def tipo                       = attrs.tipo ?: 'fixo'
        def txOriginalClassName        = attrs.txOriginalClassName ?: '' // fazer o controle de exibir o texto original na frente ou embaixo da tradução
        def className                  = attrs.className ?: '' // adicionar alguma classe no texto traduzido
        def txOriginal                 = attrs?.txOriginal ?: ''
        def txTraduzido                = attrs?.txTraduzido ?: ''
        def txRevisado                = attrs?.txRevisado ?: ''
        def idRegistroTraducao   = attrs?.idRegistro ? attrs?.idRegistro.toLong() :''
        def noTabelaTraducao     = ''
        //def noColunaTraducao     = ''

        if( !txOriginal ){
            out << ''
            return
        }

        // remover espaços inicio e fim dos textos
        txOriginal  = txOriginal.toString().replaceAll(/\\n/,'').trim()
        txTraduzido = txTraduzido.toString().replaceAll(/\\n/,'').trim()
        txRevisado = txRevisado.toString().replaceAll(/\\n/,'').trim()

/** /
 println ' '
 println 'TRADUCAO TAGLIB - attrs'
 println attrs
 /**/

// depurar
/** /
 if( txOriginal=='Categoria'){
     println ' '
     println 'CATEGORIA'
     println 'TRADUCAO TAGLIB - attrs'
     println attrs
 }
 /**/


        if( tipo=='fixo') {
            /** /
            println ' '
            println 'FIXO'
            println attrs
            /**/
            // localiar o texto traduzido se não tiver sido informado
            if (txOriginal && !txRevisado) {
                TraducaoTexto tt = TraducaoTexto.findByTxOriginal(txOriginal)
                txTraduzido = tt ? tt?.txTraduzido : txOriginal
                txRevisado = tt ? tt?.txRevisado   : txTraduzido
            }
        } else if( tipo == 'apoio' ) {
                noTabelaTraducao='apoio'
                TraducaoDadosApoio tt = TraducaoDadosApoio.findByDadosApoio( DadosApoio.get( idRegistroTraducao ) )
                txTraduzido = tt?.txTraduzido ?: txOriginal
                txRevisado = tt?.txRevisado ?: txTraduzido
        } else if( tipo == 'uso' ) {
                noTabelaTraducao  = 'uso'
                TraducaoUso tt  = TraducaoUso.findByUso( Uso.get( idRegistroTraducao ) )
                txTraduzido = tt?.txTraduzido ?: txOriginal
                txRevisado = tt?.txRevisado ?: txTraduzido
        } else if( tipo == 'pan' ) {
                noTabelaTraducao  = 'pan'
                TraducaoPan tt  = TraducaoPan.findByPlanoAcao( PlanoAcao.get( idRegistroTraducao ) )
                txTraduzido = tt?.txTraduzido ?: txOriginal
                txRevisado = tt?.txRevisado ?: txTraduzido
        } else if( tipo=='regex') {
            txTraduzido = Util.traducaoRegex( txOriginal )
            txRevisado = txTraduzido
        }
        txTraduzido = txTraduzido.trim()
        def spanTextoCorrigido = """<span id="textoCorrigido-${rndId}" class="texto-traduzido traduzir ${className}" data-traduzir="{'tipo':'${tipo}','rndId':'${rndId}','noTabelaTraducao':'${noTabelaTraducao}','idRegistroTraducao':'${idRegistroTraducao}'}" title="${txOriginal}">${txRevisado}</span>"""
        def spanTextoTraduzido = """<span id="textoTraduzido-${rndId}" class="hidden">${txTraduzido}</span>"""
        def spanTextoOriginal = """<span id="textoOriginal-${rndId}" class="texto-original ${txOriginalClassName} hidden">${txOriginal}</span>"""
        out << raw(spanTextoCorrigido+''+spanTextoOriginal+''+spanTextoTraduzido)
    }

    /**
     * gerar html para tradução dos campos textos abertos da ficha
     */
    def textoLivre = {attrs, body ->
        try {
            // parametros obrigatorios
            def rndId = Util.randomString(6).toLowerCase()
            def noTabelaTraducao = attrs?.noTabela ? attrs?.noTabela.toLowerCase() : ''
            def noColunaTraducao = attrs?.noColuna
            def idRegistroTraducao = attrs?.sqRegistro ? attrs?.sqRegistro.toLong() : 0
            //def htmlBotaoTraduzir = """"""
            //def htmlBotaoRevisar= """"""
            //def htmlCheckBoxRevisado =""""""
            def textoOriginal = """"""
            def textoTraduzido = """"""
            def isRevisado = false
            def noPessoaRevisou = ''

            // ler o texto original
            String cmdSql = """select new map( t.${noColunaTraducao} as texto) from ${noTabelaTraducao.toString().capitalize()} as t where id = :id"""
            List rows = TraducaoTabela.executeQuery(cmdSql, [id: idRegistroTraducao])
            if( rows ) {
                textoOriginal = rows[0].texto
                // remover style font
                //textoOriginal = textoOriginal.replaceAll(/Times New Roman/,'open_sansregular')
                //textoOriginal = textoOriginal.replaceAll(/ ?font[a-zA-Z0-9-]{0,}: ?[0-9a-zA-Z#!% "',_-]{1,}; ?/,'')

                // manter somente as tags html necessárias
                textoOriginal = Util.stripTagsTraducao( textoOriginal )
                if( Util.trimEditor(textoOriginal) != '' ) {

                    // localizar o registro da tabela traducaoTabela
                    String nomeColunaSnakeCase = Util.toSnakeCase(noColunaTraducao)
                    TraducaoTabela traducaoTabela = TraducaoTabela.findByNoTabelaAndNoColuna(noTabelaTraducao, nomeColunaSnakeCase)
                    if (!traducaoTabela) {
                        traducaoTabela = new TraducaoTabela(noTabela: noTabelaTraducao, noColuna: nomeColunaSnakeCase, dsColuna: noTabelaTraducao + '/' + nomeColunaSnakeCase)
                        traducaoTabela.save(flush: true)
                        //throw new Exception('Coluna ' + noTabelaTraducao + '/' + nomeColunaSnakeCase+' não cadastrada para tradução')
                    }

                    // localizar a tradução se houver
                    TraducaoRevisao traducaoRevisao = TraducaoRevisao.findByTraducaoTabelaAndSqRegistro(traducaoTabela, idRegistroTraducao)
                    if (traducaoRevisao) {
                        textoTraduzido = traducaoRevisao.txTraduzido ?: ''
                        textoTraduzido = Util.stripTagsTraducao(textoTraduzido)
                        isRevisado = traducaoRevisao?.dtRevisao ? true : false
                        if (traducaoRevisao.pessoa && traducaoRevisao.dtRevisao) {
                            noPessoaRevisou = traducaoRevisao.pessoa ? (' por ' + traducaoRevisao?.pessoa?.noPessoa + ' em ' + traducaoRevisao?.dtRevisao?.format('dd/MM/yyyy HH:mm')) : ''
                        }
                    }
                }
                //println 'TEXTO TRADUZIDO'
                //println textoTraduzido
            }
            def html = ""
            if( textoOriginal != '' ) {
                html = """<div class="col-sm-12">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-sm-12 mt5 ficha-html-toolbar">
                                <button type="button" id="btnTraduzir${rndId}" data-params="rndId:${rndId},noTabela:${noTabelaTraducao},noColuna:${noColunaTraducao},sqRegistro:${idRegistroTraducao}"
                                        data-action="fichaRevisao.traduzirTexto"
                                        class="btn btn-info btn-xs ${isRevisado ? 'hidden' : ''}">Traduzir</button>
                            </div>
                            <div class="col-sm-12 ficha-html-text-justify ficha-html-text-box mt5">
                                <div id="textoOriginal${rndId}">${textoOriginal}</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-sm-12 mt5 ficha-html-toolbar">
                                <button type="button" id="btnRevisar${rndId}" data-params="rndId:${rndId},noTabela:${noTabelaTraducao},noColuna:${noColunaTraducao},sqRegistro:${idRegistroTraducao}"
                                        data-action="fichaRevisao.revisarTraducao"
                                        class="btn btn-info btn-xs ${isRevisado ? 'hidden' : ''}">Revisar</button>
                                <button type="button" id="btnModalHistorico${rndId}" data-params="rndId:${rndId},noTabela:${noTabelaTraducao},noColuna:${noColunaTraducao},sqRegistro:${idRegistroTraducao}"
                                        data-action="fichaRevisao.modalHistorico"
                                        title="Exibir o histório das revisões"
                                        class="btn btn-secondary btn-xs hidden">Histórico</button>
                                <span class="pull-right">
                                    <label class="cursor-pointer">
                                        <input type="checkbox" id="chkMarcarRevisado${rndId}"
                                               data-params="rndId:${rndId},noTabela:${noTabelaTraducao},noColuna:${noColunaTraducao},sqRegistro:${idRegistroTraducao}"
                                               data-action="fichaRevisao.marcarRevisado" ${isRevisado ? 'checked' : ''}
                                               class="checkbox-lg" title="Marcar/desmarcar como revisado">Revisado
                                    </label>
                                    <span id="spanNoPessoaRevisou${rndId}" class="ficha-html-nome-revisou">${noPessoaRevisou}</span>
                                </span>
                            </div>

                            <div class="col-sm-12 ficha-html-text-justify ficha-html-text-box mt5">
                                <div id="textoTraduzido${rndId}">${textoTraduzido}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>"""
            }
            out << raw( html )
        } catch ( Exception e ){
            out << raw('<div class="alert alert-danger">'+e.getMessage()+'</div>')
        }
    }
}
