package br.gov.icmbio

class TablePaginationTagLib {
    static defaultEncodeAs = [taglib:'html']
    //static encodeAsForTags = [tagName: [taglib:'html'], otherTagName: [taglib:'none']]
    def tablePagination = { attrs, body ->

        boolean  debug = false

        // parametros recebidos
        def pagination      = attrs.pagination
        def navId           = attrs.id
        def blockId         = attrs.blockId
        def blockMessage    = attrs.blockMessage
        def tableId         = attrs.tableId
        def url             = attrs.url
        def tag             = attrs.tag
        def filters         = attrs.filtros
        def qtdButtons      = attrs.qtdButtons // quantidade de botoes para acesso direto às páginas

        // definir valores padrão dos parametros
        blockMessage    = blockMessage ?: 'Atualizando...'
        qtdButtons      = qtdButtons ?: 5
/*
        if( debug ) {
            println ' '
            println  'tablePaginationTagLib() ' + new Date().format('HH:mm:ss')
            println '  - navId = '+navId
            println '  - blockId = '+blockId
            println '  - blockMessage = '+blockMessage
            println '  - url = '+url
            println '  - tag = '+tag
            println '  - filters = '+filters
            println '  - qtdButtons ='+qtdButtons
            println '  - pagination = ' + pagination
        }
*/
        // validar parametros obrigatórios
        List errors = []
        if ( ! navId ) {
            errors.push('parâmetro "id" é obrigatório')
        }
        if ( ! pagination ) {
            errors.push('parâmetro "pagination" é obrigatório')
        }
        if ( ! url ) {
            errors.push('parametro "url" é obrigatória')
        }
        if( errors ) {
            out << raw('<div class="alert alert-danger"><strong>Erro Paginação</strong><ol>'+
                        errors.collect{ '<li>' + it + '</li>' }.join('')+
                    '</ol></div>')
            return
        }

        // deve haver no minimo 2 paginas para exibir o componente de paginação
        if( !pagination.totalPages || pagination.totalPages  < 2 ) {
            return;
        }

        // inicio da construcao do html da paginação
        out << raw("""<nav aria-label="Paginação" id="${navId}">
                    <ul class="pagination"
                        data-pagination-block-id="${blockId}"
                        data-pagination-block-message="${blockMessage}"
                        data-pagination-table-id="${tableId}"
                        data-pagination-url="${url}"
                        data-pagination-tag="${tag}"
                        data-pagination-total-records="${pagination.totalRecords}"
                        data-pagination-filters='${filters}'>
                    """)

        // inicio
        out << raw("""<li class="page-item first-page${pagination.pageNumber==1?' disabled':''}" onClick="app.tablePagination(this,1)"><a class="page-link" href="javascript:vokd(0);">Página Inicial</a></li>""")
        // anterior
        out << raw("""<li class="page-item previous-page${pagination.pageNumber==1?' disabled':''}"  onClick="app.tablePagination(this,${pagination.pageNumber-1})"><a class="page-link" href="javascript:vokd(0);">Anterior</a></li>""")
        // select paginas
        out << raw("""<li class="page-item"><a class="page-link page-link-current-page" href="javascript:vokd(0);">""")
        out << raw("""<select class="pagination-select" onChange="app.tablePagination(this,this.value)">""")
            for( i in (1..(pagination.totalPages.toInteger() ) ) ) {
                out << raw("""<option value="${ i }" ${ i.toInteger() == pagination.pageNumber.toInteger() ?'selected':''}>${ i }</option>""")
            }
        out<< raw("""</select></a></li>""")

        // botoes paginas individuais
        if( pagination.pageNumber.toInteger() < pagination.totalPages.toInteger() && qtdButtons.toInteger() > 0 ) {
            for( i in ((Math.min( ( pagination.pageNumber.toInteger() + 1),pagination.totalPages.toInteger()) )..(Math.min( ( pagination.pageNumber.toInteger() + qtdButtons.toInteger() ),pagination.totalPages.toInteger()))) ) {
                out << raw( """<li class="page-item"><a class="page-link" href="javascript:void(0);" onClick="app.tablePagination(this, ${i.toInteger()})">${i.toInteger()}</a></li>""")
            }
        }

        // botões finais
        if( pagination.pageNumber.toInteger() + qtdButtons.toInteger() < pagination.totalPages.toInteger() ) {
            // proxima
            out << raw("""<li class="page-item"><a class="page-link" href="javascript:void(0);" onClick="app.tablePagination(this, ${ pagination.pageNumber+1 } )">Próxima</a></li>""")
            // última
            out << raw("""<li class="page-item"><a class="page-link" href="javascript:void(0);" onClick="app.tablePagination(this, ${ pagination.totalPages } )">Última</a></li>""")
        }
        out<< raw('</ul></nav>')
        // fim  da construcao do html da paginação
    }
}
