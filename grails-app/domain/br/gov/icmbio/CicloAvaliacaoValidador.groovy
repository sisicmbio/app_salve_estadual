package br.gov.icmbio

class CicloAvaliacaoValidador
{

    PessoaFisica validador
    DadosApoio situacaoConvite
    String deEmail
    Date dtEmail
    Date dtResposta
    Date dtValidadeConvite
    String noUsuario
    String deHash
    CicloAvaliacao cicloAvaliacao // TODO - REMOVER ESTA RELAÇÃO COM CICLO AVALIACAO

    //static belongsTo = [cicloAvaliacao:CicloAvaliacao]
    static belongsTo = [oficina:Oficina]
    static hasMany = [validadorFicha:ValidadorFicha]

    static constraints = {
        dtEmail nullable: true
        dtResposta nullable:true
        dtValidadeConvite nullable:true
        noUsuario nullable: true
        deHash nullable:true
    }

    static mapping = {
        version 	false
        table           name:'salve.ciclo_avaliacao_validador'
        id 			    column:'sq_ciclo_avaliacao_validador',generator:'identity'
        oficina         column:'sq_oficina'
        cicloAvaliacao  column:'sq_ciclo_avaliacao'
        validador       column:'sq_validador'
        situacaoConvite column:'sq_situacao_convite'
    }

    boolean getIsValid()
    {
        boolean result=false
        if( situacaoConvite)
        {
            // validador ainda não respondeu mas ainda pode aceitar
            if( situacaoConvite.codigoSistema=='EMAIL_ENVIADO'&& dtValidadeConvite>=new Date() )
            {
                result = true
            }
            else if( situacaoConvite.codigoSistema=='CONVITE_ACEITO')
            {
                result = true
            }
            else if( situacaoConvite.codigoSistema=='EMAIL_NAO_ENVIADO')
            {
                result = true
            }
        }
        return result
    }

}
