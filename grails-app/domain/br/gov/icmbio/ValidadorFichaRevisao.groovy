package br.gov.icmbio

class ValidadorFichaRevisao {
    DadosApoio resultado
    DadosApoio categoriaSugerida
    String deCriterioSugerido
    String txJustificativa
    String stPossivelmenteExtinta
    Date dtInclusao
    Date dtAlteracao
    static belongsTo = [validadorFicha: ValidadorFicha]

    static constraints = {
        categoriaSugerida nullable: true
        deCriterioSugerido nullable: true
        dtAlteracao nullable: true
        stPossivelmenteExtinta nullable: true
    }

    static mapping = {
        version false
        table               name: 'salve.validador_ficha_revisao'
        id                  column: 'sq_validador_ficha_revisao', generator: 'identity'
        validadorFicha      column: 'sq_validador_ficha'
        categoriaSugerida   column: 'sq_categoria_sugerida'
        resultado           column: 'sq_resultado'
    }

    def beforeValidate() {
        if( !dtInclusao ){
            dtInclusao = new Date()
            dtAlteracao = dtInclusao
        }
    }

    def beforeUpdate(){
        dtAlteracao = new Date()
    }
}
