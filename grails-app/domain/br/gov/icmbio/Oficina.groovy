package br.gov.icmbio

import grails.converters.JSON

class Oficina
{
    String noOficina
    String sgOficina
    String deLocal
    Date dtInicio
    Date dtFim
    DadosApoio situacao
    String txEmailParticipantes // texto do último email enviado
    String txEmailValidadores // texto do último email enviado aos validadores
    String txEncaminhamento
    DadosApoio tipoOficina
    CicloAvaliacao cicloAvaliacao
    Unidade unidade
    Pessoa pontoFocal
    String deLinkSei
    DadosApoio fonteRecurso

    static hasMany = [oficinaFichas : OficinaFicha, participantes: OficinaParticipante, anexos:OficinaAnexo ]

    static constraints = {
        pontoFocal nullable: true
        unidade nullable: true
        txEncaminhamento nullable: true, blank: true
        txEmailParticipantes nullable: true, blank: true
        txEmailValidadores nullable: true, blank: true
        deLinkSei nullable: true, blank: true
        dtFim(validator: { value, object ->
            if (object.dtFim < object.dtInicio)
            {
                return ['oficina.dtFim.lower.than.message']
            }
        })
        fonteRecurso nullable: true
    }

    static mapping = {
        version false
        sort dtInicio: 'desc'
        table name: 'salve.oficina'
        id column: 'sq_oficina', generator: 'identity'
        unidade column       : 'sq_unidade_org'
        cicloAvaliacao column: 'sq_ciclo_avaliacao'
        tipoOficina column   : 'sq_tipo_oficina'
        pontoFocal column    : 'sq_pessoa'
        situacao column      : 'sq_situacao'
        fonteRecurso column : 'sq_fonte_recurso'
    }

    String getPeriodo()
    {
        return this.dtInicio.format('dd/MM/yyyy')+' a ' + this.dtFim.format('dd/MM/yyyy')
    }

    Boolean getIsOpen()
    {
        if( dtFim ) {
            if (dtFim >= Util.hoje() ) {
                return true
            }
        }
        return false
    }

    public asJson()
    {
        Map data = [:]
        data.put( 'sqOficina'		   ,this.id)
        data.put( 'sqTipoOficina'    ,this.tipoOficina.id)
        data.put( 'noOficina'		    ,this.noOficina)
        data.put( 'sgOficina'        ,this.sgOficina)
        data.put( 'sqUnidadeOrg'     ,this?.unidade?.id?:'')
        data.put( 'dsUnidadeOrg'     ,this?.unidade?.sgInstituicao ?: '')
        data.put( 'sqPontoFocal'     ,this?.pontoFocal?.id ?:'')
        data.put( 'dsPontoFocal'	    ,this?.pontoFocal?.noPessoa ?: '')
        data.put( 'deLocal'	        ,this.deLocal)
        data.put( 'dtInicio'     	,this.dtInicio.format('dd/MM/yyyy') )
        data.put( 'dtFim'           ,this.dtFim.format('dd/MM/yyyy') )
        data.put( 'sqSituacao'	    ,this.situacao.id )
        data.put( 'sqFonteRecurso'	    ,this?.fonteRecurso?.id ?:'' )
        return data as JSON
    }

    boolean canModify()
    {
        if( this?.situacao?.codigoSistema == 'ABERTA' ) {
            return true
        }
        return false
    }

    boolean hasDocFinal()
    {
        return OficinaAnexo.countByOficinaAndInDocFinal( this,true) > 0
    }

    Integer nuFichasVersionadas(){
        if( !id ){
            return 0
        }
        List data = OficinaFicha.executeQuery("""select new map( count(a.id) as nu_fichas )
            from OficinaFicha a
            join a.oficinaFichaVersao b
            where a.oficina.id = :sqOficina""",[sqOficina:id])
        return data[0].nu_fichas
    }


}
