package br.gov.icmbio

class UcEstadual {

    String noPessoa

    static constraints = {
    }

    static mapping = {
        version 	false
        table       name:'geo.vw_uc_estadual'
        id          column:'gid'
        noPessoa    column:'nome_uc1'
    }
}
