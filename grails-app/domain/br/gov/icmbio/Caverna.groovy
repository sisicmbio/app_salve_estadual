package br.gov.icmbio

class Caverna {

    String noCaverna
    String noLocalidade
    String noEstado
    String noMunicipio
    Integer coRegistroNacional

    static constraints = {
        coRegistroNacional nullable: true
        noMunicipio nullable: true
    }

    static mapping = {
        version 	false
        sort        noCaverna: 'asc'
        table       name 	: 'salve.caverna'
        id          column 	: 'sq_caverna',generator:'assigned'
     }
}
