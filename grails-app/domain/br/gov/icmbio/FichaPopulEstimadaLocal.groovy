package br.gov.icmbio
import grails.converters.JSON;

class FichaPopulEstimadaLocal {

	Ficha ficha
	DadosApoio unidAbundanciaPopulacao
	String dsLocal
	String dsMesAnoInicio
	String dsMesAnoFim
	String dsEstimativaPopulacao
    String deValorAbundancia
    Abrangencia abrangencia

    static constraints = {
    	dsLocal nullable:true
    	unidAbundanciaPopulacao nullable:true
    	dsEstimativaPopulacao nullable:true
        abrangencia nullable: true
        deValorAbundancia nullable:true
       }

    static mapping = {
		version 				false
	   	table       			name 	: 'salve.ficha_popul_estimada_local'
	   	id          			column 	: 'sq_ficha_popul_estimada_local',generator:'identity'
	   	ficha 					column  : 'sq_ficha'
	   	unidAbundanciaPopulacao column 	: 'sq_unid_abundancia_populacao'
        abrangencia             column  : 'sq_abrangencia'
	}

    public asJson()
    {
    	Map data = [:]
	    data.put( 'sqFichaPopulEstimadaLocal'	,this.id)
        data.put( 'sqFicha'						,this.ficha.id)
        data.put( 'sqUnidAbundanciaPopulacao'	,this?.unidAbundanciaPopulacao?.id)
        data.put( 'deValorAbundancia'   		,this.deValorAbundancia)
        data.put( 'dsLocal'						,this.dsLocal)
        data.put( 'dsMesAnoInicio'				,this.dsMesAnoInicio)
        data.put( 'dsMesAnoFim'					,this.dsMesAnoFim)
        data.put( 'dsEstimativaPopulacao'		,this.dsEstimativaPopulacao)
        data.put( 'sqTipoAbrangencia'		    ,this?.abrangencia?.tipo?.id)
        data.put( 'dsTipoAbrangencia'		    ,this?.abrangencia?.tipo?.descricao)
        data.put( 'sqAbrangencia'		        ,this?.abrangencia?.id)
        data.put( 'dsAbrangencia'		        ,this?.abrangencia?.descricao)
        return data as JSON
    }

    public String getRefBibHtml()
    {
        //List nomes = [];
        List refs = FichaRefBib.findAllByFichaAndNoTabelaAndNoColunaAndSqRegistro(this.ficha,'ficha_popul_estimada_local','sq_ficha_popul_estimada_local',this.id);
        return Util.formatRefBib(refs,true)

        /*refs.each{
            if( it.publicacao )
            {
                nomes.push( it.publicacao.citacao+'&nbsp;<span class="label label-warning tag cursor-pointer" title="'+it.publicacao.deTitulo.replaceAll(/"/,"&quot;")+'">...</span>' );
            }
            else
            {
                nomes.push( it.noAutor + ', ' + it.nuAno + ' (Com.Pess.)&nbsp;<span class="label label-warning tag cursor-pointer" title="Comunicação Pessoal">...</span>' );
            }
        }
        return nomes.join('<br>');

         */
    }
}
