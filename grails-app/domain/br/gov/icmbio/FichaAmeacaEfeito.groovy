package br.gov.icmbio

class FichaAmeacaEfeito {

    DadosApoio efeito
    VwEfeitoPrim vwEfeitoPrim
    Integer nuPeso

    static belongsTo = [fichaAmeaca:FichaAmeaca]

    static constraints = {
        nuPeso nullable:true
        vwEfeitoPrim nullable: true
    }

    static mapping = {
        version 				false
        table       			name 	: 'salve.ficha_ameaca_efeito'
        id          			column 	: 'sq_ficha_ameaca_efeito',generator:'identity'
        fichaAmeaca 			column  : 'sq_ficha_ameaca'
        efeito                  column  : 'sq_efeito'
        vwEfeitoPrim            column  : 'sq_efeito', insertable: false, updateable: false
    }

    String getPeso()
    {
        return Util.pesoEfeito( nuPeso )
    }

    List getTrilhaEfeitoList()
    {
        return vwEfeitoPrim.trilhaList
    }

    String getTrilhaEfeitoPath()
    {
        return vwEfeitoPrim.trilhaPath
    }

    List getTrilhaAmeacaList()
    {
        return fichaAmeaca.trilhaList
    }

    String getTrilhaAmeacaPath()
    {
        return fichaAmeaca.trilhaPath
    }
}
