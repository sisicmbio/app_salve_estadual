package br.gov.icmbio

class VwFichaUc implements Serializable {

    Long sqFicha
    String cdEsfera
    Long sqUc
    String noUc
    String inPresencaAtual
    String dePresencaAtual
    String sgEstado
    String noEstado
    Integer nuOrdem
    String jsonRefBib

    static constraints = {
    }

    static mapping = {
        //cache : true
        version false
        //sort   nuOrdem: 'asc'
        table  name: 'salve.vw_ficha_uc'
        id composite:['sqFicha','sqUc','cdEsfera'], insertable: false, updateable: false
    }
}
