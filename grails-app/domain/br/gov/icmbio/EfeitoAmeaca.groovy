package br.gov.icmbio

class EfeitoAmeaca {

    static belongsTo = [ameaca:DadosApoio, efeito:DadosApoio]

    static constraints = {
    }

    static mapping = {
        version false
        table name    : 'salve.efeito_ameaca'
        id column     : 'sq_efeito_ameaca', generator: 'identity'
        ameaca column : 'sq_ameaca'
        efeito  column: 'sq_efeito'
    }
}
