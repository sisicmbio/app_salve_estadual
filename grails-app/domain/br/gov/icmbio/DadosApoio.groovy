
package br.gov.icmbio

class DadosApoio {
   //transient utilityService
   String codigo
   String descricao
   String ordem
   String codigoSistema
   String nivel
   DadosApoio pai

   // Alteracoes integração com SIS/IUCN
   static hasMany = [itens : DadosApoio, traducaoDadosApoio:TraducaoDadosApoio ];

   static constraints = {
       	descricao  nullable:false, blank:false, maxSize:100
       	codigo     nullable:true, blank:true, maxSize:50
        ordem      nullable:true, blank:true, maxSize:50
        codigoSistema nullable:true, blank:true
        nivel       nullable:true, blank:true
        pai         nullable: true
   }

   static mapping = {
   		version 	  false
    	sort        ordem: 'asc'
     	table       name:'salve.dados_apoio'
     	id          column:'sq_dados_apoio',generator:'identity'
     	pai         column:'sq_dados_apoio_pai'
     	descricao   column:'ds_dados_apoio'
     	codigo      column:'cd_dados_apoio'
        ordem       column:'de_dados_apoio_ordem'
        itens       column:'sq_dados_apoio_pai'
        codigoSistema column:'cd_sistema'
        nivel       column:'cd_dados_apoio_nivel'
      }

   def beforeValidate() {
       /*if( ! this.codigo )
      {
        this.formatCodigo(this.descricao);
      }
      else if( this?.pai?.codigoSistema == 'TB_TAG' )
      {
        this.formatCodigo(this.codigo);
      }

      if( ! this.codigoSistema )
      {
        this.codigoSistema=this.codigo;
      }
      */
       // Util.stripTagsFichaPdf()
       if ( this.codigo && !this.codigoSistema) {
           this.codigoSistema = this.codigo;
           // colocar o codigo sistema no formato 001.001.002
           if ( this.codigoSistema =~ /^[0-9]{1,2}\.?/) {
               this.codigoSistema = this.codigoSistema.split('\\.').collect { it.toString().padLeft(3, '0') }.join('.')
           }
       }
    }

    public String formatCodigo(String newValue)
    {
        /*if( newValue )
        {
            this.codigo = utilityService.removeAccents( newValue.toUpperCase().replaceAll(/ / ,'_').toUpperCase());
        }
        return this.codigo;
        */
    }

    public String getDescricaoCompleta()
    {
      if( this.codigo ==~ /AMEACADA|OUTRA/)
      {
            return this.descricao
      }
        if( this.pai && this.pai.codigoSistema=='TB_CATEGORIA_IUCN')
        {
            return this.descricao+' ('+this.codigo+')'
        }
      return this.codigo+' - '+this.descricao
    }

    public Integer  getRootId()
    {
      if ( this.pai && this?.pai?.pai )
      {
        return this.pai.rootId
      }
      return this.id
    }

    public getDescricaoHieraquia()
    {
        List resultado = [this.descricaoCompleta]
        if( this?.pai?.pai ) {
            resultado.push(this.pai.descricaoCompleta)
            if (this?.pai?.pai?.pai) {
                resultado.push(this.pai.pai.descricaoCompleta)
                if (this?.pai?.pai?.pai?.pai) {
                   resultado.push(this.pai.pai.pai.descricaoCompleta)
                }
            }
        }
        return resultado.reverse().join(' / ')
    }

}
