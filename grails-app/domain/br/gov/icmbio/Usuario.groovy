package br.gov.icmbio

class Usuario {

    PessoaFisica pessoaFisica
    String deSenha
    String deHash
    Date dtAlteracao
    Date dtUltimoAcesso
    Boolean stAtivo

    static hasMany = [perfis:UsuarioPerfil]

    static constraints = {
        perfis nullable: true
        stAtivo nullable: true
        deHash nullable: true, blank:false
        dtUltimoAcesso nullable: true
    }

    static mapping = {
        version false
        table       name: 'salve.usuario'
        id          column:'sq_pessoa', generator: 'foreign', params: [property: 'pessoaFisica']
        pessoaFisica insertable: false, updateable: false, column: 'sq_pessoa'
    }

    def beforeValidate() {
        this.dtAlteracao = new Date()
    }

    String getCpfFormatado(){
        return Util.formatCpf( pessoaFisica?.nuCpf )
    }

    String getNoPessoa(){
        return pessoaFisica?.noPessoa ?: ''
    }

    String getNoUsuario(){
        return getNoPessoa()
    }

    String getEmail(){
        return pessoaFisica?.email ?: ''
    }

    Map asMap(){
        return [
                sqPessoa        : id  ?: '',
                noPessoa        : pessoaFisica?.pessoa?.noPessoa ?: '',
                nuCpf           : cpfFormatado,
                deEmail         : email,
                stAtivo         : stAtivo,
                dsAtivo         : stAtivo ? 'Sim' : 'Não',
                sqInstituicao   : pessoaFisica?.instituicao?.id ?: '',
                sgInstituicao   : pessoaFisica?.instituicao?.sgInstituicao?:'',
                dtUltimoAcesso  : dtUltimoAcesso ? dtUltimoAcesso.format('dd/MM/yyyy') : ''
        ]
    }

    /**
     * verificar se o usuário tem um determinado perfil
     * @param cdPerfilSistema
     * @param sqInstituicao
     * @return
     */
    boolean hasPerfil( String cdPerfilSistema = '', Long sqInstituicao = null ) {
        if( !cdPerfilSistema || !perfis ){
            return false
        }
        return perfis.find{it.perfil.cdSistema == cdPerfilSistema.toUpperCase() }
    }

    /**
     * identificar se o usuário tem perfil Administrador
     * @return
     */
    boolean isAdm() {
        if( perfis.size() == 0 ){
            return false;
        }
        if( perfis.find{it.perfil.cdSistema == 'ADM'} ) {
            return true
        }
        return false
    }

    /**
     * criar a lista de perfis por unidade
     * @return
     */
    List getPerfilPorInstituicao(){

        Map data = [:]
        UsuarioPerfilInstituicao.createCriteria().list {
            usuarioPerfil{
                eq('usuario',this)
            }
        }.each {
                if( ! data[it.instituicao.id] ){
                    data[it.instituicao.id] = [
                             sqInstituicao: it.instituicao.id
                            ,sqTipo:it.instituicao.tipo.id
                            ,noInstituicao: it.instituicao.pessoa.noPessoa
                            ,sgInstituicao: it.instituicao.sgInstituicao ?: it.instituicao.pessoa.noPessoa
                            ,perfis:[]
                            ]
                }
                data[it.instituicao.id].perfis.push([
                         sqUsuarioPerfilInstituicao : it.id
                        ,sqPerfil                   : it.usuarioPerfil.perfil.id
                        ,noPerfil                   : it.usuarioPerfil.perfil.noPerfil
                ])
        }
        List res = []
        data.each{key,value ->
            value.perfis.sort{it.noPerfil}
            res.push( value )
        }
        return res.sort{it.noInstituicao}

    }

}
