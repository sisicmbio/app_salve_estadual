package br.gov.icmbio

/**
 * classe para manter compabilidade no codigo onde existe
 * referencia a classe Uc
 */
class Uc {

    transient geoService

    Instituicao instituicao
    DadosApoio esferaAdm
    DadosApoio tipo

    static constraints = {
    }

    static mapping = {
    	version 	false
    	sort        noUc  : 'asc'
     	table       name  : 'salve.unidade_conservacao'
        id          column:'sq_pessoa', generator: 'foreign', params: [property: 'instituicao']
        instituicao insertable: false, updateable: false, column: 'sq_pessoa'
        esferaAdm   column: 'sq_esfera'
        tipo        column: 'sq_tipo'
    }

    Long getSqUc() {
        return id ? id.toLong() : null

    }

    String getNoUc(){
        if( instituicao ) {
            return (instituicao.pessoa.noPessoa)
        }
        return ''
    }

    String getEsfera(){
        return esferaAdm ? esferaAdm.descricao : ''
    }

    String getInEsfera(){
        if( !esferaAdm ){
            return ''
        }
        if( esferaAdm.codigoSistema == 'FEDERAL'){
            return 'F'
        } else if( esferaAdm.codigoSistema == 'ESTADUAL'){
            return 'E'
        } else if( esferaAdm.codigoSistema == 'RPPN'){
            return 'R'
        }
        return ''
    }

    String getSgUnidade(){
        return getSigla()
    }

    String getSigla() {
        if( instituicao ) {
            return (instituicao.sgInstituicao)
        }
        return ''
    }

    Map getCentroide() {
        return geoService.getCentroide( this );
    }
}
