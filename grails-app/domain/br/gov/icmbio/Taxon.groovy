package br.gov.icmbio

import grails.converters.JSON
import org.codehaus.groovy.grails.web.json.JSONObject

class Taxon {

	NivelTaxonomico nivelTaxonomico
    TaxonSituacao situacaoNome
    Taxon pai
    String noTaxon
    Boolean stAtivo
    Boolean inOcorreBrasil
    String noAutor
    Integer nuAno
    String jsonTrilha

    static constraints = {
        nuAno nullable: true
        pai nullable: true
        noAutor nullable:true,blank:true
        jsonTrilha nullable: true, blank:true
    }

    //static fetchMode = [pai: 'eager']

    static mapping = {
    	version 	false
        //cache       true
	   	table       name:'taxonomia.taxon'
	   	id         	column:'sq_taxon', generator:'identity'
		nivelTaxonomico column:'sq_nivel_taxonomico'
		pai 			column:'sq_taxon_pai'
        situacaoNome    column:'sq_situacao_nome'
        jsonTrilha type:'text',insertable: false,updateable: false

        //pai fetch: 'left outer join'
        // pai lazy: false

	}

    def beforeValidate() {
        this.nuAno = null;
        if( this.noAutor )
        {
            String ano = this.noAutor.toString().replaceAll(/[^0-9]/,'');
            if( ano.isInteger() )
            {
                this.nuAno = ano.toInteger();
            }
        }
    }

    /*def beforeInsert = {

    }*/

    /*def afterInsert = {


    }
    */

    String getNoCientifico()
    {
        if( this.nivelTaxonomico.coNivelTaxonomico == 'ESPECIE')
        {
            return this?.pai?.noTaxon+' '+this?.noTaxon
        }
        else if( this.nivelTaxonomico.coNivelTaxonomico == 'SUBESPECIE')
        {
            return this?.pai?.pai?.noTaxon+' '+this?.pai?.noTaxon+' '+this?.noTaxon
        }
        return this.noTaxon;
    }

	String getNoCientificoCompleto()
    {
        if( this.noCientifico && this.noAutor )
        {
            if( this.noAutor != 'null' ) {
                return this.noCientifico + ' ' + this.noAutor
            }
        }
        return this.noCientifico
    }

    String getNoCientificoItalico()
    {
        if( this.nivelTaxonomico.coNivelTaxonomico == 'ESPECIE')
        {
            return '<i>' + this?.pai?.noTaxon+' '+this?.noTaxon+'</i>'
        }
        else if( this.nivelTaxonomico.coNivelTaxonomico == 'SUBESPECIE')
        {
            return '<i>' + this?.pai?.pai?.noTaxon+' '+this?.pai?.noTaxon+' '+this?.noTaxon+'</i>'
        }
        return '<i>' + this.noTaxon+'</i>'
    }


    String getNoCientificoCompletoItalico()
	{
        if( this.noCientifico && this.noAutor )
        {
		  return '<i>'+this.noCientifico.replaceAll("'",'`') + '</i> '+ ( this.noAutor && this.noAutor != 'null' ? ' ' + this.noAutor.replaceAll("'",'`') : '' )
        }
        return '<i>' + this.noCientifico.replaceAll("'",'`') + '</i>'
    }

    String asJson() {
        Map data = [:]
        data.put('sqTaxon', this.id)
        data.put('sqTaxonPai', this?.pai?.id);
        data.put('noTaxon', this.noTaxon)
        data.put('noAutor', this.noAutor)
        data.put('inOcorreBrasil', this.inOcorreBrasil ? '1' : '0')
        data.put('sqNivelTaxonomico', this?.nivelTaxonomico?.id);
        data.put('coNivelTaxonomico', this?.nivelTaxonomico?.coNivelTaxonomico);
        data.put('deNivelTaxonomico', this?.nivelTaxonomico?.deNivelTaxonomico);
        data.put('json_trilha', this?.jsonTrilha)
        return data as JSON
    }


    Map getStructure()
    {
        Map structure = [:]
        if ( this.id ) {
            // construir a estrutura pela coluna jsonTrilha
            try {
                if ( this.jsonTrilha ) {
                    JSONObject jsonTrilha = JSON.parse( this.jsonTrilha )
                    structure = [coNivelTaxonico: this.nivelTaxonomico.coNivelTaxonomico.toUpperCase(), nmCientifico: this.noTaxon.capitalize()]
                    NivelTaxonomico.withCriteria{
                        order 'nuGrauTaxonomico'
                    }.each {
                        if( it.coNivelTaxonomico ) {
                            String nivel = it.coNivelTaxonomico.toString().toLowerCase()
                            String id = 'id' + nivel.capitalize()
                            String nome = 'nm' + nivel.capitalize()
                            if( jsonTrilha[ nivel ] ) {
                                structure[id] = jsonTrilha[nivel].sq_taxon.toString()
                                if (nivel == 'ESPECIE' || nivel == 'SUBESPECIE')
                                {
                                    structure[nome] = jsonTrilha[nivel].no_taxon.toString()
                                }else {
                                    structure[nome] = jsonTrilha[nivel].no_taxon.toString().toLowerCase().capitalize()
                                }
                            }
                        }
                    }
                    if (structure.coNivelTaxonico == 'ESPECIE') {
                        structure.nmCientifico =  structure.nmEspecie
                    } else if (structure.coNivelTaxonico == 'SUBESPECIE') {
                        structure.nmCientifico =  structure.nmSubespecie
                    }
                }
            } catch (Exception e) {
                println e.getMessage()
            }
            // se jsonTrilha estiver vazio ou com erro de JSON construir recursivamente lendo da tabela taxonomia.taxon
            if (!structure) {
                structure = recursiveTaxon(this)
                if (this.nivelTaxonomico.coNivelTaxonomico == 'ESPECIE') {
                    structure['nmCientifico'] = structure['nmEspecie']
                } else if (this.nivelTaxonomico.coNivelTaxonomico == 'SUBESPECIE') {
                    structure['nmCientifico'] = structure['nmSubespecie']
                } else {
                    structure['nmCientifico'] = structure['nm' + this.nivelTaxonomico.coNivelTaxonomico?.toLowerCase()?.capitalize()]
                }
                structure['coNivelTaxonomico'] = this.nivelTaxonomico.coNivelTaxonomico
            }
        }
        return structure
    }

    String getStructureJson()
    {
        return getStructure() as JSON
    }

    protected Map recursiveTaxon( Taxon t )
    {

        Map mapa = [:]
        if( this.id )
        {
            mapa.put('id' + t.nivelTaxonomico.coNivelTaxonomico.toLowerCase().capitalize(), t.id)
            mapa.put('nm' + t.nivelTaxonomico.coNivelTaxonomico.toLowerCase().capitalize(), t.noTaxon.toLowerCase().capitalize() )
            if (t.pai)
            {
                mapa << recursiveTaxon(t.pai)
            }
        }
        return mapa
    }

    /*
    Lista a hierarquia no estilo breadcrumb
    Exemplo: ['Animalia', 'Chordata', 'Reptilia', 'Serpentes', 'Colubridae', 'Pseudaspis', 'Cana']
    */
    List getHierarchy()
    {
        List h = [];
        String abreItalico
        String fechaItalico
        Map structure
        if( this.id ) {
            structure = this.structure
            NivelTaxonomico.withCriteria {
                order 'nuGrauTaxonomico'
            }.each {
                if (it.coNivelTaxonomico) {
                    String nivel = it.coNivelTaxonomico.toString().toLowerCase()
                    String key = 'nm' + nivel.capitalize()
                    if( structure[ key ] ) {
                        if ((key ==~ /^nm.*/) && key != 'nmCientifico') {
                            if ( key == 'nmGenero' || key == 'nmEspecie' || key == 'nmSubespecie') {
                                abreItalico = '<i>'
                                fechaItalico = '</i>'
                            } else {
                                abreItalico = ''
                                fechaItalico = ''
                            }
                            h.push(abreItalico + structure[key] + fechaItalico)
                        }
                    }
                }
            }
        }
        return h
    }
    List getNomesComuns()
    {
        // aqui deverão ser lidos os nomes comuns direto da tabela nome_comum da taxonomia e não da ficha
        return []
    }
    List getSinonimias()
    {
        // aqui deverão ser lidos os nomes comuns direto da tabela sinonimia da taxonomia e não da ficha
        return []
    }

    String getNomesComunsText()
    {
        // aqui deverão ser lidos os nomes comuns direto da tabela nome_comum da taxonomia e não da ficha
        return '<falta implementar>';
    }

    String getSinonimiasText()
    {
        return '<falta implementar>';
    }
}
