package br.gov.icmbio
import grails.converters.JSON;

class FichaAcaoConservacao {

	Ficha ficha
    VwFicha vwFicha
	DadosApoio acaoConservacao
	DadosApoio situacaoAcaoConservacao
	PlanoAcao planoAcao
	DadosApoio tipoOrdenamento
	String txFichaAcaoConservacao



    static constraints = {
    	planoAcao nullable:true
    	tipoOrdenamento nullable:true
    	txFichaAcaoConservacao nullable:true;blank:true;
        vwFicha nullable:true
    }


	static mapping = {
		version 				false
	   	table       			name 	: 'salve.ficha_acao_conservacao'
	   	id          			column 	: 'sq_ficha_acao_conservacao',generator:'identity'
	   	ficha 					column  : 'sq_ficha'
        vwFicha                 column  : 'sq_ficha',insertable: false,updateable: false
	   	acaoConservacao			column 	: 'sq_acao_conservacao'
		situacaoAcaoConservacao column 	: 'sq_situacao_acao_conservacao'
		planoAcao 				column 	: 'sq_plano_acao'
		tipoOrdenamento			column 	: 'sq_tipo_ordenamento'
	}

	public String getDescricaoCompleta()
  	{
    	return this.acaoConservacao.codigo+' - '+this.acaoConservacao.descricao;
  	}

  	public asJson()
    {
    	Map data = [:]
	    data.put( 'sqFichaAcaoConservacao'		,this.id)
        data.put( 'txFichaAcaoConservacao'		,this.txFichaAcaoConservacao)
        data.put( 'sqAcaoConservacao'  		 	,this.acaoConservacao.id)
        data.put( 'deAcaoConservacao'  		 	,this.acaoConservacao.codigo+' - '+this.acaoConservacao.descricao)
        data.put( 'sqSituacaoAcaoConservacao'   ,this.situacaoAcaoConservacao.id)
        data.put( 'codigoSistema'	      		,this.acaoConservacao.codigoSistema)
        data.put( 'sqPlanoAcao'                 ,this.planoAcao?.id)
        data.put( 'dePlanoAcao'                 ,this.planoAcao?.sgPlanoAcao)
        data.put( 'sqTipoOrdenamento'           ,this.tipoOrdenamento?.id)
        return data as JSON
    }

    public String getRefBibHtml()
    {
        //List nomes = [];
        List refs = FichaRefBib.findAllByFichaAndNoTabelaAndNoColunaAndSqRegistro(this.ficha,'ficha_acao_conservacao','sq_ficha_acao_conservacao',this.id);
        return Util.formatRefBib(refs,true)

        /*refs.each{
            if( it.publicacao )
            {
                nomes.push( it.publicacao.citacao+'&nbsp;<span class="label label-warning tag cursor-pointer" title="'+it.publicacao.deTitulo.replaceAll(/"/,"&quot;")+'">...</span>' );
            }
            else if( it.noAutor )
            {
                nomes.push( it.noAutor + (it.nuAno ? ', ' + it.nuAno : '') + ' (Com.Pess.)&nbsp;<span class="label label-warning tag cursor-pointer" title="Comunicação Pessoal">...</span>' );
            }
        }
        return nomes.join('<br>')

         */
    }

    public String getRefBibText() {
        //List nomes = [];
        List refs = FichaRefBib.findAllByFichaAndNoTabelaAndNoColunaAndSqRegistro(this.ficha,'ficha_acao_conservacao','sq_ficha_acao_conservacao',this.id);
        return Util.formatRefBib(refs,false)

        /*refs.each{
            if( it.publicacao )
            {
                nomes.push( it.publicacao.citacao);
            }
            else if( it.noAutor )
            {
                nomes.push( it.noAutor + (it.nuAno ? ', ' + it.nuAno : '') + ' (Com.Pess.)' );
            }
        }
        return nomes.join('\n');

         */
    }
}
