package br.gov.icmbio

class CicloConsultaFicha {

    Ficha ficha
    VwFicha vwFicha

    private transient deleted = false
    boolean deleted

    static belongsTo = [cicloConsulta:CicloConsulta]
    static hasMany = [colaboracoes:FichaColaboracao,colaboracoesOcorrencia:FichaOcorrenciaConsulta]
    static hasOne = [cicloConsultaFichaVersao:CicloConsultaFichaVersao ]

    static constraints = {
        vwFicha    nullable:true
        cicloConsultaFichaVersao nullable: true
    }
    static mapping = {
        version 	false
        table           name  :'salve.ciclo_consulta_ficha'
        id              column:'sq_ciclo_consulta_ficha', generator:'identity'
        ficha           column:'sq_ficha'
        vwFicha         column:'sq_ficha',insertable: false, updateable: false
        cicloConsulta   column:'sq_ciclo_consulta'
    }
}
