package br.gov.icmbio

class FichaSincronismo {
    DadosApoio tipo
    Integer nuMinutosIntervalo
    Date dtSincronismo

    static belongsTo = [ficha: Ficha]

    static constraints = {

    }

    static mapping = {
        version false
        table               name: 'salve.ficha_sincronismo'
        id                  column: 'sq_ficha_sincronismo', generator: 'identity'
        tipo                column: 'sq_tipo_sincronismo'
        ficha               column: 'sq_ficha'
    }

    def beforeValidate() {
        if( !dtSincronismo ){
            dtSincronismo = new Date()
        }
    }
}
