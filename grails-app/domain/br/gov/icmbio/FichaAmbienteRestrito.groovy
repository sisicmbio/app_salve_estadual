package br.gov.icmbio
import grails.converters.JSON;

class FichaAmbienteRestrito {

	Ficha ficha
	DadosApoio restricaoHabitat
	String txFichaRestricaoHabitat
	String deRestricaoHabitatOutro

    static hasMany = [cavernas:FichaAmbRestritoCaverna]

    static constraints = {
    	ficha nullable: false
    	restricaoHabitat nullable: false
    	txFichaRestricaoHabitat nullable: true
    	deRestricaoHabitatOutro nullable: true
    }

    static mapping = {
    	version 	false
        sort        restricaoHabitat: 'asc'
	   	table       name 	: 'salve.ficha_ambiente_restrito'
	   	id          column 	: 'sq_ficha_ambiente_restrito',generator:'identity'
	   	ficha		column 	: 'sq_ficha'
	   	restricaoHabitat column: 'sq_restricao_habitat'
    }

    public String getRefBibHtml()
 	{

        // 		List nomes = [];
 		List refs = FichaRefBib.findAllByFichaAndNoTabelaAndNoColunaAndSqRegistro(this.ficha,'ficha_ambiente_restrito','sq_ficha_ambiente_restrito',this.id);
        return Util.formatRefBib(refs,true)

        /*
        refs.each{
 			if( it.publicacao )
            {
                nomes.push( it.publicacao.citacao+'&nbsp;<span class="label label-warning tag cursor-pointer" title="'+it.publicacao.deTitulo.replaceAll(/"/,"&quot;")+'">...</span>' );
            }
            else
            {
                nomes.push( it.noAutor + ', ' + it.nuAno + ' (Com.Pess.)&nbsp;<span class="label label-warning tag cursor-pointer" title="Comunicação Pessoal">...</span>' );
            }
 		}
 		return nomes.join('<br>');
        */

 	}

    public asJson()
    {
        Map data = [:]
        data.put( 'sqFichaAmbienteRestrito'     ,this.id)
        data.put( 'sqRestricaoHabitat'          ,this.restricaoHabitat?.id)
        data.put( 'dsRestricaoHabitat'          ,this.restricaoHabitat?.descricao)
        data.put( 'deRestricaoHabitatOutro'     ,this.deRestricaoHabitatOutro)
        data.put( 'txFichaRestricaoHabitat'     ,this.txFichaRestricaoHabitat)
        return data as JSON
    }

    String treeUp(){
        if( ! restricaoHabitat ){
            return ''
        }
        DadosApoio temp = this.restricaoHabitat.pai
        List arvore=[]
        int deep = 0
        while(temp.pai && deep < 10 ){
            deep++
            arvore.push(temp.codigo + ' - ' + temp.descricao)
            temp=temp.pai?:null
        }
        arvore.push( restricaoHabitat.codigo + ' - '+restricaoHabitat.descricao)
        return arvore.join('|')
    }

}
