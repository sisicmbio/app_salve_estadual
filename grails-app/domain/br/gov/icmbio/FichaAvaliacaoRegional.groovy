package br.gov.icmbio

import grails.converters.JSON

class FichaAvaliacaoRegional {

    Ficha ficha
    Abrangencia abrangencia     // Goiás, Cerrado
    DadosApoio categoriaIucn
    Integer nuAnoAvaliacao
    String deCriterioAvaliacaoIucn
    String txJustificativaAvaliacao
    String stPossivelmenteExtinta
    //String deCriterioAvaliacaoRegional

    static constraints = {
        abrangencia				    nullable:true
        deCriterioAvaliacaoIucn		nullable:true
        txJustificativaAvaliacao    nullable:true
        stPossivelmenteExtinta      nullable:true
        //deCriterioAvaliacaoRegional nullable:true
    }

    static mapping = {
        version 				false
        sort                    nuAnoAvaliacao:'asc'
        table       			name 	: 'salve.ficha_avaliacao_regional'
        id          			column 	: 'sq_ficha_avaliacao_regional',generator:'identity'
        ficha 					column  : 'sq_ficha'
        categoriaIucn           column  : 'sq_categoria_iucn'
        abrangencia             column  : 'sq_abrangencia'      // Goias, Cerrado,
    }

    public asJson()
    {
        Map data = [:]
        data.put( 'sqFichaAvaliacaoRegional'	,this.id)
        data.put( 'sqTipoAbrangencia'           ,this?.abrangencia.tipo?.id)
        data.put( 'deTipoAbrangencia'			,this?.abrangencia?.tipo?.descricao)
        data.put( 'sqAbrangencia' 		        ,this?.abrangencia?.id )
        data.put( 'deAbrangencia'               ,this?.abrangencia?.descricao)
        data.put( 'stPossivelmenteExtinta' 	    ,this?.stPossivelmenteExtinta)

        data.put( 'sqCategoriaIucn'				,this?.categoriaIucn?.id)
        data.put( 'nuAnoAvaliacao'				,this?.nuAnoAvaliacao)
        data.put( 'dsCriterioAvalIucn'		    ,this?.deCriterioAvaliacaoIucn)
        //data.put( 'deCriterioAvalRegional'        ,this?.deCriterioAvaliacaoRegional)
        data.put( 'dsJustificativa'	     ,this?.txJustificativaAvaliacao)
        return data as JSON
    }
}
