package br.gov.icmbio
import grails.converters.JSON

class FichaAmbRestritoCaverna {

	Caverna caverna;
	static belongsTo = [fichaAmbienteRestrito:FichaAmbienteRestrito]
	static constraints = {
    }

	static mapping = {
		version false;
		//sort nuAno: 'asc';
		table name: 'salve.ficha_amb_restrito_caverna'
		id column: 'sq_ficha_amb_restrito_caverna', generator: 'identity'
        caverna column: 'sq_caverna'
        fichaAmbienteRestrito column: 'sq_ficha_ambiente_restrito'
	}

}
