package br.gov.icmbio

import grails.converters.JSON

class CicloConsultaPessoa {

    // private transient deleted = false
    // boolean deleted

    PessoaFisica pessoa
    DadosApoio situacao
    Date dtEnvio
    String deEmail
    String deObservacao
    Instituicao instituicao

    static belongsTo = [cicloConsulta:CicloConsulta]
    static hasMany = [fichas:CicloConsultaPessoaFicha]

    static constraints = {
        deObservacao nullable: true
        instituicao nullable: true
        dtEnvio nullable: true
    }

    static mapping = {
        version 	false
        table           name   : 'salve.ciclo_consulta_pessoa'
        id              column : 'sq_ciclo_consulta_pessoa', generator:'identity'
        cicloConsulta   column : 'sq_ciclo_consulta'
        pessoa          column : 'sq_pessoa'
        situacao        column : 'sq_situacao'
        instituicao     column : 'sq_instituicao'
    }

    String getFichasHtml()
    {
        List itens = []
        Email email = Email.findByPessoa( pessoa )
        WebUsuario usuario
        if( email )
        {
            usuario = WebUsuario.findByUsername(email?.txEmail)
        }
        Integer qtdColaboracoes = 0
        fichas.each {


            if( usuario )
            {
                // verificar se tem colaboração
                qtdColaboracoes = FichaColaboracao.countByConsultaFichaAndUsuario(it.cicloConsultaFicha,usuario)
            }
            if( qtdColaboracoes > 0 )
            {
                itens.push('<a href="javascript:void(0);" onClick="gerenciarConsulta.showColaboracoes(' + it.cicloConsultaFicha.ficha.id + ',' + usuario.id +')" class="'+(qtdColaboracoes>0?' green':'')+'">'+ it.cicloConsultaFicha.ficha.noCientifico+'</a>')
            }
            else
            {
                itens.push('<span>'+it.cicloConsultaFicha.ficha.noCientificoItalico+'</span>')
            }

        }
        return itens.join('<br>')
    }

    String getFichasEmail()
    {
        List itens = []
        fichas.eachWithIndex { it, i ->
            //itens.push( (i+1) +') ' + it.cicloConsultaFicha.ficha.noCientifico )
            itens.push( '<li>' + it.cicloConsultaFicha.vwFicha.nmCientifico+'</li>' )
        }
        itens.sort()
        return '<ul style="font-size:14px;">'+itens.join("")+'</ul>'
    }

    public asJson() {
        Map data = [:]
        data.put( 'sqCicloConsultaPessoa'	,this.id)
        data.put( 'sqPessoaConvidada'   	    ,this.pessoa.id)
        data.put( 'sqInstituicao'            ,this?.instituicao?.id)
        data.put( 'noInstituicao'            ,this?.instituicao?.noInstituicao)
        data.put( 'sgInstituicao'            ,this?.instituicao?.sgInstituicao)
        data.put( 'noPessoaConvidada'	    ,this.pessoa.noPessoa)
        data.put( 'deEmail'	                ,this.deEmail)
        data.put( 'deObservacao'	            ,this.deObservacao)
        data.put( 'fichas'	                ,this.fichas.cicloConsultaFicha.id)
        return data as JSON
    }

}
