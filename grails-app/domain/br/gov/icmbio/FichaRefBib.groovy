package br.gov.icmbio
import grails.converters.JSON

class FichaRefBib {

	Ficha ficha
	VwFicha vwFicha
	Publicacao publicacao
	String noTabela
	String noColuna
	String deRotulo
	Integer sqRegistro
	Integer nuAno
	String noAutor

    static constraints = {
    	deRotulo 	maxSize:200
    	noAutor		maxSize:100,nullable:true,blank:true
    	noTabela 	maxSize:50, nullable:true,blank:true
    	noColuna 	maxSize:50, nullable:true,blank:true
    	nuAno 		nullable:true
    	publicacao 	nullable:true
		vwFicha 	nullable:true

    }

	static mapping = {
		version 				false;
	   	table       			name 	: 'salve.ficha_ref_bib';
	   	id          			column 	: 'sq_ficha_ref_bib',generator:'identity';
	   	ficha 					column  : 'sq_ficha';
	   	vwFicha 				column  : 'sq_ficha', insertable: false, updateable: false;
	   	publicacao 				column  : 'sq_publicacao';
	}

    def beforeValidate() {
    	if( this.noTabela )
    	{
    		this.noTabela = this.noTabela.toLowerCase();
    	}
    	if( this.noColuna )
    	{
    		this.noColuna = this.noColuna.toLowerCase();
    	}
    	if( this.publicacao)
    	{
    		this.noAutor = null;
    		this.nuAno = null;
    	}
    }

    String getTagsIds()
	{
		List tags = FichaRefBibTag.findAllByFichaRefBib( this );
		List result=[];
		tags.each {
			result.push( it.tag.id )
		}
		return result
	}

	String getTagsAsHtml()
	{
		List tags = FichaRefBibTag.findAllByFichaRefBib( this );
		List result=[];
		tags.each {
			//result.push( '<span class="badge">'+it.tag.descricao+'</span>' )
			result.push( '<span class="label label-default tag">'+it.tag.descricao+'</span>' )
		}
		return result.join( ' ');
	}

    JSON asJson()
    {
    	Map data = [:]
	    data.put( 'sqFichaRefBib'   ,this.id)
        data.put( 'sqPublicacao'    ,( this.publicacao ? this.publicacao.id : null ) )
        data.put( 'dePublicacao'    ,( this.publicacao ? this.publicacao.tituloAutorAno : null) )
        data.put( 'noTabela'        ,this.noTabela)
        data.put( 'noColuna'        ,this.noColuna)
        data.put( 'sqRegistro'      ,this.sqRegistro)
        data.put( 'deRotulo'        ,this.deRotulo)
        data.put( 'deTagsIds'       ,this.tagsIds)
        data.put( 'noAutorRef'      ,this.noAutor ?:'' )
        data.put( 'nuAnoRef'        ,this.nuAno ?:'' )
        return data as JSON
    }

    String getReferenciaHtml()
    {
        //return  Util.formatRefBib([this],true)
        if( this.publicacao ) {
            String tag = ''
            FichaRefBibTag.executeQuery("""select new map( tag.descricao as noTag) from FichaRefBibTag a
            where a.fichaRefBib = :fichaRefBib
              and a.tag.descricao in ('a','b','c','d','e','f','g','h','i','j')
            order by a.tag.descricao""",[fichaRefBib:this],[rows:1]).each{ row ->
                tag = row.noTag
            }
            return this.publicacao.getReferenciaHtml(tag)
        }
        else {
            if( this.noAutor) {
                return Util.formatarComunicacaoPessoal(this.noAutor, this.nuAno)
                //String comPess = Util.nome2citacao(this.noAutor.trim()) + (this.nuAno ? ', ' + this.nuAno.toString() : '')
                //comPess = comPess.replaceAll(/\.(\s[0-9]{4})$/, '.,$1') + ' (Com.Pess.)'
                //return comPess
            }

            // this.noAutor+' com.pess. '+(this.nuAno?:'')+'.'
        }
        return ''
    }
}
