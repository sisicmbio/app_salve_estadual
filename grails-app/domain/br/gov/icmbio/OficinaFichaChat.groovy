package br.gov.icmbio

class OficinaFichaChat {

    static belongsTo = ['oficinaFicha':OficinaFicha]
    PessoaFisica pessoa
    Date dtMensagem
    String deMensagem
    String sgPerfil

    static constraints = {
        pessoa nullable:false
        oficinaFicha nullable:false
        dtMensagem nullable:false
        deMensagem nullable:false
        sgPerfil nullable:false
    }

    static mapping = {
        version false
        table         name:'salve.oficina_ficha_chat'
        id            column:'sq_oficina_ficha_chat', generator: 'identity'
        oficinaFicha  column:'sq_oficina_ficha'
        pessoa 	      column:'sq_pessoa'
    }

    def beforeValidate() {
        if( ! dtMensagem )
        {
            dtMensagem = new Date()
        }
    }
}
