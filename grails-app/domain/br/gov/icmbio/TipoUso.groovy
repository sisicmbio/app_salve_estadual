package br.gov.icmbio

class TipoUso {

String deTipoUso
String coTipoUso

    static constraints = {
    }

    static mapping = {
		version 				false;
	   	table       			name 	: 'taxonomia.tipo_uso';
	   	id          			column 	: 'sq_tipo_uso',generator:'identity';
	}
}
