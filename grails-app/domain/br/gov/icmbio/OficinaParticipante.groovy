package br.gov.icmbio

class OficinaParticipante {

	String deEmail
	Date dtEmail
	Instituicao instituicao
	Pessoa pessoa
    DadosApoio situacao

    static hasMany = [ oficinaFichaParticitpantes : OficinaFichaParticipante ]
	static belongsTo = [oficina: Oficina]

    static constraints = {
    	dtEmail nullable: true
    	deEmail nullable: true, blank: true
    }

    static mapping = {
    	version false
     	table       name:'salve.oficina_participante'
     	id          column:'sq_oficina_participante', generator: 'identity'
     	oficina	    column:'sq_oficina'
     	pessoa      column:'sq_pessoa'
     	instituicao column:'sq_instituicao'
     	situacao    column:'sq_situacao'
    }

    List getPapeisFichas()
    {
        List data=[]
        this.oficinaFichaParticitpantes.each{ oficinaFichaParticipante ->
            def item = data.find {
                it.pessoa == this.pessoa && it.papel == oficinaFichaParticipante.papel
            }
            if( ! item )
            {
                data.push( [pessoa: this.pessoa
                              ,papel : oficinaFichaParticipante.papel
                              ,fichas: [ oficinaFichaParticipante.oficinaFicha.vwFicha ] ] )

            }
            else
            {
                item.fichas.push( oficinaFichaParticipante.oficinaFicha.vwFicha )
            }
        }
        return data
    }

    String getEmail()
    {
        if( this.deEmail )
        {
            return this.deEmail.toLowerCase()
        }
        else
        {
            return this.pessoa?.deEmail?.toLowerCase()
        }
    }

}
