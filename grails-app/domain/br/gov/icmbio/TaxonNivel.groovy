package br.gov.icmbio

class TaxonNivel {

    String deNivelTaxonomico
    Integer nuGrauTaxonomico
    Boolean inNivelIntermediario
    String coNivelTaxonomico

    static constraints = {
    }

    static mapping = {
        version		false
        sort		nuGrauTaxonomico: 'asc'
        table       name 	: 'taxonomia.nivel_taxonomico'
        id			column:'sq_nivel_taxonomico'
    }

}