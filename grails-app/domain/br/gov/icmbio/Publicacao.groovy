package br.gov.icmbio
import grails.converters.JSON;

class Publicacao {

	TipoPublicacao tipoPublicacao
	String deTitulo
	String noAutor
	Integer nuAnoPublicacao
	Date  dtPublicacao
	String noRevistaCientifica
	String deVolume
	String deIssue
	String dePaginas
	String deRefBibliografica
	Boolean inListaOficialVigente
	String deTituloLivro
	String deEditores
	String noEditora
	String noCidade
	String nuEdicaoLivro
	String deUrl
	Date dtAcessoUrl
	String noUniversidade
	String deDoi
    String deIssn
	String deIsbn

    static hasMany = [anexos : PublicacaoArquivo ]

	static constraints = {
		noRevistaCientifica nullable:true,blank:true
		deVolume nullable:true,blank:true
		deIssue nullable:true,blank:true
		dePaginas nullable:true,blank:true
		deRefBibliografica nullable:true,blank:true
		deTituloLivro nullable:true,blank:true
		deEditores nullable:true,blank:true
		noEditora nullable:true,blank:true
		noCidade nullable:true,blank:true
		nuEdicaoLivro nullable:true,blank:true
		deUrl nullable:true,blank:true
		dtAcessoUrl nullable:true
		noUniversidade nullable:true,blank:true
		deDoi nullable:true,blank:true
        deIssn nullable:true,blank:true
		deIsbn nullable:true,blank:true
		noAutor nullable: true
		nuAnoPublicacao nullable: true
		dtPublicacao nullable: true
	}

	static mapping = {
		version 				false
		sort 					noAutor: 'asc'
	   	table       			name 	: 'taxonomia.publicacao'
	   	id          			column 	: 'sq_publicacao',generator:'identity'
	   	tipoPublicacao 			column 	: 'sq_tipo_publicacao'
	}
    /*
    def beforeUpdate(){
        // gerar a referencia bibliografica no formato de impressao
        this.deRefBibliografica = this.referenciaHtml;
    }*/

	def beforeValidate() {
		if( this.deTitulo ) {
            this.deTitulo = this.deTitulo.trim().replaceAll(/\s{2,}/ , ' ').trim().replaceAll("'",'`')
		}
        if( this.noAutor ){
            this.noAutor = this.noAutor.trim().replaceAll(/\s{2,}/ , ' ').trim()
        }
        // gerar a referencia bibliografica no formato de impressao
        this.deRefBibliografica = Util.getReferenciaHtml( this )
	}

	String getTituloAutorAno()
    {
		String resultado = this.deTitulo
		if ( this.noAutor )
		{
			resultado += ' / ' + this.noAutor
		}
		if( nuAnoPublicacao ) {
            String nuAno = nuAnoPublicacao.toString()
            nuAno = ( nuAno == '9999' ? 's.d.' : nuAno )
			resultado += ' / ' + nuAno
		}
		else if( this.dtPublicacao )
		{
			resultado += ( ' / ' + this.dtPublicacao.format('dd/MM/yyyy') )
		}
		return resultado
    }

    String getAutores()
    {
		if( ! this.noAutor )
		{
			return ''
		}
        return getNomes( this.noAutor)
    }

    String getEditores(String editores )
    {
        return getNomes( editores)
    }

    String getNomes(String nomes)
    {
        if( ! nomes ) {
            return '';
        }
        //return Util.nome2citacao( this.noAutor ) // nao é a mesma formatacao
        //String noAutores = this.noAutor.toString().trim().replaceAll(/\n\r?/,'|');
        String noAutores = nomes.trim().replaceAll(/\n\r?/,'|');
        noAutores = noAutores.replaceAll(/\s{2,}/,' ')
        noAutores = noAutores.replaceAll(/\s?\|/,';')
        List listNomes = noAutores.split(';')

        // trocar todos os '; & \n' para ';'
        // nomes = nomes.trim().replaceAll(/\s{2,}/ , ' ')
        // List listNomes = nomes.trim().replaceAll(/\s+;\s+|\s+&\s+|\n|\r/,'; ').split(';');
        // ler somente o sobrenome dos nomes;
        listNomes.eachWithIndex{ nome,i->
            if( nome.indexOf(',') )
            {
                listNomes[i]=nome.split(',')[0]
            }
        }

        switch( listNomes.size())
        {
            case 1:
                return listNomes[0].trim();
                break;
            case 2:
                return listNomes[0].trim()+' & '+listNomes[1].trim();
                break;
            default:
                return listNomes[0].trim()+' <i>et al.</i>';
        }
    	return nomes;
    }

    public String getCitacao()
    {
        String autores = this.autores
        return Util.getCitacao( autores,this.nuAnoPublicacao, this.dtPublicacao)
		/*if ( this.nuAnoPublicacao ) {
			autores += ', '+ this.nuAnoPublicacao
		}
		else if ( this.dtPublicacao ) {
			autores += ', '+ this.dtPublicacao.format('dd/MM/yyyy')
		}
        return autores*/
    }


	public asJson()
    {
    	Map data = [:]
		data.put('sqPublicacao'				,this.id);
		data.put('sqTipoPublicacao'			,this.tipoPublicacao.id);
		data.put('deTipoPublicacao'			,this.tipoPublicacao.deTipoPublicacao);
		data.put('inListaOficialVigente'	,(this.inListaOficialVigente ? 'S' :'N') );
		data.put('noAutor'					,(this?.noAutor ? this.noAutor.toString().replaceAll(/; +/,';').replaceAll(/;/,"\n" ) : null ) );
		data.put('deTitulo'					,this.deTitulo);
		data.put('noRevistaCientifica'		,this.noRevistaCientifica)
		data.put('nuAnoPublicacao'			,this?.nuAnoPublicacao?:'')
		data.put('dtPublicacao'			    ,( this.dtPublicacao ? this.dtPublicacao.format('dd/MM/yyyy') : null ) )
		data.put('deVolume'					,this.deVolume);
		data.put('deIssue'					,this.deIssue);
		data.put('dePaginas'				,this.dePaginas);
		data.put('deRefBibliografica'		,this.deRefBibliografica);
		data.put('deTituloLivro'			,this.deTituloLivro);
		data.put('deEditores'				,this.deEditores);
		data.put('noEditora'				,this.noEditora);
		data.put('noCidade'					,this.noCidade);
		data.put('nuEdicaoLivro'			,this.nuEdicaoLivro);
		data.put('deUrl'					,this.deUrl);
		data.put('dtAcessoUrl'				,( this.dtAcessoUrl ? this.dtAcessoUrl.format('dd/MM/yyyy') : null ) );
		data.put('noUniversidade'			,this.noUniversidade);
		data.put('deDoi'					,this.deDoi);
        data.put('deIssn'                   ,this.deIssn);
		data.put('deIsbn'					,this.deIsbn);
	    return data as JSON
    }

    String getReferenciaHtml(tag='') {
        return deRefBibliografica ?: Util.getReferenciaHtml( this )
    }
}
