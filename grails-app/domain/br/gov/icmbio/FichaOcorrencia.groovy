/**
 * Domain da classe FichaOcorrencia.
 * 1. documentação relacionada com o tipo geometry:
 * 		- http://www.hibernatespatial.org/
 * 		- http://www.hibernatespatial.org/repository/org/hibernate/hibernate-spatial/4.3/
 *
 * 2 - Quando a precisão da coordenada for APROXIMADA, o campo referencia da aproximação é obrigatório
 *
 *
 *
 *
 *
 */
package br.gov.icmbio
import com.vividsolutions.jts.geom.Geometry
import org.hibernate.spatial.GeometryType

class FichaOcorrencia {
    transient geoValidate = true
    // desativar a validação do Estado e municipio na coordenada durante a importação de planilhas
    transient ucControle //  controle de gravação das UCs em federal,estadual ou rppn

    Ficha ficha
    VwFicha vwFicha
    DadosApoio tipoRegistro
    DadosApoio refAproximacao
    DadosApoio datum
    DadosApoio precisaoCoordenada
    DadosApoio formatoCoordOriginal
    DadosApoio continente
    DadosApoio metodoAproximacao
    DadosApoio habitat
    DadosApoio qualificadorValTaxon // identificacao incerta
    DadosApoio prazoCarencia
    DadosApoio contexto
    DadosApoio baciaHidrografica
    DadosApoio bioma
    DadosApoio tipoAbrangencia
    DadosApoio situacao // TB_SITUACAO_COLABORACAO ACEITA/NAO_ACEITA/RESOLVER_OFICINA ETC..
    DadosApoio situacaoAvaliacao // TB_SITUACAO_REGISTRO_OCORRENCIA: REGISTRO_UTILIZADO_AVALIACAO / REGISTRO_NAO_UTILIZADO_AVALIACAO / REGISTRO_NAO_CONFIRMADO / REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO
    Abrangencia abrangencia
    DadosApoio divisaoAdministrativa
    DadosApoio ecoregiao
    PessoaFisica pessoaRevisor
    PessoaFisica pessoaValidador
    PessoaFisica pessoaCompilador

    Pais pais
    Estado estado
    Municipio municipio
    String txMetodoAproximacao
    Taxon taxonCitado
    Integer sqUcFederal
    Integer sqUcEstadual
    Integer sqRppn
    String txLocal
    String noLocalidade
    Date dtRegistro // talvez não va precisar mais deste campo
    String hrRegistro
    Double vlElevacaoMin
    Double vlElevacaoMax
    Double vlProfundidadeMin
    Double vlProfundidadeMax
    String deIdentificador
    Date dtIdentificacao
    String deCentroResponsavel
    String inPresencaAtual
    String txTombamento
    String txInstituicao
    Date dtCompilacao
    Date dtRevisao
    Date dtValidacao
    Integer nuDiaRegistro
    Integer nuMesRegistro
    Integer nuAnoRegistro
    Integer nuDiaRegistroFim
    Integer nuMesRegistroFim
    Integer nuAnoRegistroFim

    String inDataDesconhecida
    String inUtilizadoAvaliacao
    String txNaoUtilizadoAvaliacao
    DadosApoio motivoNaoUtilizadoAvaliacao
    Date dtInvalidado
    Date dtInclusao
    Date dtAlteracao
    PessoaFisica pessoaInclusao
    PessoaFisica pessoaAlteracao
    String idOrigem
    String txObservacao
    String txNaoAceita

    Integer nuAltitude
    String inSensivel

    Geometry geometry

    static constraints = {
        ficha nullAble: false
        vwFicha nullable:true
        estado nullable: true
        bioma nullable: true
        pessoaInclusao nullable: true
        pessoaAlteracao nullable: true

        estado(validator: { value, object ->
            // validar se o estado abrange o ponto informado
            if (!object.geoValidate) {
                return true
            }
            if (object.geometry && object.contexto && object.contexto == DadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA', 'REGISTRO_OCORRENCIA')) {
                // validar se o estado bate com a coordenada
                if (value) {
                    Estado estado = GeoService.getUfByCoord((BigDecimal) object.geometry.x, (BigDecimal) object.geometry.y)
                    if (estado && value.id != estado.id) {
                        println 'Estado invalido'
                        println 'Ocorrencia id: ' + object?.id
                        println estado.sgEstado + ' e ' + value.sgEstado
                        println '---------------------------------------------------'
                        return ['ficha.ocorrencia.estado.fora.coordenada.message']
                    }
                }             }
            return true
        })
        municipio(validator: { value, object ->
            if (!object.geoValidate) {
                return true
            }
            // validar se o estado abrange o ponto informado
            if (object.geometry && object.municipio && object.estado && object.contexto && object.contexto == DadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA', 'REGISTRO_OCORRENCIA')) {
                // validar se a coordenada bate com o municipio
                if (value) {
                    Municipio municipio = GeoService.getMunicipioByCoord((BigDecimal) object.geometry.x, (BigDecimal) object.geometry.y, object.estado)
                    if (municipio && value.id != municipio.id) {
                        println 'Muncipio invalido'
                        println 'Ocorrencia id: ' + object?.id
                        println municipio.noMunicipio + ' e ' + value.noMunicipio
                        println '---------------------------------------------------'
                        return ['ficha.ocorrencia.municipio.fora.coordenada.message']
                    }
                } else {
                    println 'FichaOcorrencia, localizar Municipio pelo ponto '
                }
            }
            return true
        })

        baciaHidrografica nullable: true
        divisaoAdministrativa nullable: true
        ecoregiao nullable: true

        geometry nullable: true
        prazoCarencia nullable: true
        datum nullable: true
        formatoCoordOriginal nullable: true

        // regra de negocio para a coluna precisao da coordenada
        precisaoCoordenada nullable: true
        precisaoCoordenada(validator: { value, object ->
            if (object?.precisaoCoordenada?.codigo == 'APROXIMADA') {
                if (!object?.refAproximacao?.id) {
                    //return ['fichaOcorrencia.refAproximacao','Para precisão APROXIMADA, é Necessário informar a referência da aproximação!'];
                    return ['ficha.ocorrencia.referencia.aproximacao.aproximada.message'];
                }
            }
            return true;
        })


        refAproximacao nullable: true
        metodoAproximacao nullable: true
        txMetodoAproximacao nullable: true

        taxonCitado nullable: true
        inPresencaAtual nullable: true, maxSize: 1, inList: ['S', 'N', 'D']
        tipoRegistro nullable: true

        dtRegistro nullable: true
        inDataDesconhecida nullable: true, inList: ['S', 'N']
        nuDiaRegistro nullable: true;
        nuMesRegistro nullable: true;
        nuAnoRegistro nullable: true;
        nuDiaRegistroFim nullable: true;
        nuMesRegistroFim nullable: true;
        nuAnoRegistroFim nullable: true;

        hrRegistro nullable: true

        continente nullable: true
        pais nullable: true
        municipio nullable: true

        qualificadorValTaxon nullable: true
        habitat nullable: true
        txLocal nullable: true
        noLocalidade nullable: true
        deIdentificador nullable: true
        dtIdentificacao nullable: true
        txTombamento nullable: true
        txInstituicao nullable: true

        pessoaCompilador nullable: true
        pessoaRevisor nullable: true
        pessoaValidador nullable: true
        dtCompilacao nullable: true
        dtValidacao nullable: true
        dtRevisao nullable: true

        deCentroResponsavel nullable: true
        vlElevacaoMin nullable: true
        vlElevacaoMax nullable: true
        vlProfundidadeMin nullable: true
        vlProfundidadeMax nullable: true
        sqUcFederal nullable: true
        sqUcEstadual nullable: true
        sqRppn nullable: true
        tipoAbrangencia nullable: true
        abrangencia nullable: true
        inUtilizadoAvaliacao nullable: true, inList: ['S', 'N', 'E']
        txNaoUtilizadoAvaliacao nullable: true
        motivoNaoUtilizadoAvaliacao nullable:true
        dtInvalidado nullable: true
        situacao nullable: true
        situacaoAvaliacao: nullable: false
        idOrigem nullable: true, blank:true
        txObservacao nullable:true, blank:true
        txNaoAceita nullable:true, blank:true
        nuAltitude nullable:true
        inSensivel nullable:true, inList: ['S', 'N']
        //stAdicionadoAposAvaliacao nullable:true
    }

    static mapping = {
        version false
        table name: 'salve.ficha_ocorrencia'
        id column: 'sq_ficha_ocorrencia', generator: 'identity'
        ficha column: 'sq_ficha'
        vwFicha column:'sq_ficha',insertable: false,updateable: false
        contexto column: 'sq_contexto'
        estado column: 'sq_estado'

        bioma column: 'sq_bioma'
        baciaHidrografica column: 'sq_bacia_hidrografica'

        habitat column: 'sq_habitat'
        continente column: 'sq_continente'
        pais column: 'sq_pais'
        municipio column: 'sq_municipio'

        geometry type: GeometryType, sqlType: "GEOMETRY"
        geometry column: 'ge_ocorrencia'
        prazoCarencia column: 'sq_prazo_carencia'
        datum column: 'sq_datum'
        formatoCoordOriginal column: 'sq_formato_coord_original'
        precisaoCoordenada column: 'sq_precisao_coordenada'
        refAproximacao column: 'sq_ref_aproximacao'
        metodoAproximacao column: 'sq_metodo_aproximacao'

        taxonCitado column: 'sq_taxon_citado'
        tipoRegistro column: 'sq_tipo_registro'
        qualificadorValTaxon column: 'sq_qualificador_val_taxon'

        pessoaCompilador column: 'sq_pessoa_compilador'
        pessoaRevisor column: 'sq_pessoa_revisor'
        pessoaValidador column: 'sq_pessoa_validador'
        tipoAbrangencia column: 'sq_tipo_abrangencia'
        abrangencia column: 'sq_abrangencia'
        divisaoAdministrativa column: 'sq_divisao_administrativa'
        ecoregiao column: 'sq_ecoregiao'
        pessoaInclusao column: 'sq_pessoa_inclusao'
        pessoaAlteracao column: 'sq_pessoa_alteracao'
        situacao column: 'sq_situacao'
        situacaoAvaliacao column: 'sq_situacao_avaliacao'
        motivoNaoUtilizadoAvaliacao column: 'sq_motivo_nao_utilizado_avaliacao'
    }

    /*def afterDelete()
    {

    }
    */
    /*def afterInsert(){
        println ' '
        println 'FichaOcorrencia() - AFTER INSERT EXECUTADO ' + new Date().format('hh:mm:ss');
    }
    def afterUpdate(){
        // executa 3 vezes ao gravar
        println ' '
        println 'FichaOcorrencia() - AFTER UPDATE EXECUTADO ' + new Date().format('hh:mm:ss');
    }*/

    def beforeValidate() {
        if (!this.dtInclusao) {
            this.dtInclusao = new Date()
            this.pessoaInclusao = this.pessoaAlteracao
        }
        if( ! this.situacaoAvaliacao ){
            this.situacaoAvaliacao = DadosApoio.findByCodigoSistemaAndPai('REGISTRO_NAO_CONFIRMADO', DadosApoio.findByCodigoSistema('TB_SITUACAO_REGISTRO_OCORRENCIA'));
        }
        this.dtAlteracao = new Date()

        // SRID padrão: 4674
        if( this.geometry  && this.geometry?.SRID == 0) {
            geometry.SRID = 4674
        }

        /*
        if (!this.geometry && !this.tipoAbrangencia)
        {
            DadosApoio apoio;
            String codigo;
            if (this.estado)
            {
                codigo = 'ESTADO'
            } else if (this.bioma)
            {
                codigo = 'BIOMA'
            } else if (this.baciaHidrografica)
            {
                codigo = 'BACIA_HIDROGRAFICA'
            } else if (this.ecoregiao)
            {
                codigo = 'ECOREGIAO'
            }
            if (codigo)
            {
                apoio = DadosApoio.findByCodigoSistemaAndPai(codigo, DadosApoio.findByCodigoSistema('TB_TIPO_ABRANGENCIA'));
                this.tipoAbrangencia = DadosApoio.get(apoio.id);
            }
        }
        */
        // gravar a uc de acordo com a esfera
        if (this?.ucControle) {
            this.sqUcFederal = null;
            this.sqUcEstadual = null;
            this.sqRppn = null;
            if (this.ucControle.inEsfera == 'F') {
                this.sqUcFederal = this.ucControle.sqUc;
            } else if (this.ucControle.inEsfera == 'E') {
                this.sqUcEstadual = this.ucControle.sqUc;
            } else if (this.ucControle.inEsfera == 'R') {
                this.sqRppn = this.ucControle.sqUc;
            }
        }
        if (this.inUtilizadoAvaliacao == 'N' && !this.dtInvalidado) {
            this.dtInvalidado = new Date();
        }
    }
/*
   def afterDelete() {
      String coluna='';
      if( this.contexto.codigoSistema =='ESTADO')
      {
          coluna = 'sq_estado'
      }
      else if( this.contexto.codigoSistema == 'BIOMA')
      {
          coluna = 'sq_bioma'
      }
      else if( this.contexto.codigoSistema == 'BACIA_HIDROGRAFICA')
      {
          coluna = 'sq_bacia_hidrografica'
      }
      else if( this.contexto.codigoSistema == 'REGISTRO_OCORRENCIA')
      {
          coluna = 'sq_ficha_ocorrencia'
      }

      if( coluna )
      {
          FichaRefBib item;
          FichaRefBib.findAllByFichaAndNoTabelaAndNoColunaAndSqRegistro(this.ficha,'ficha_ocorrencia',coluna,this.id).each {
            item = it;
            item.withNewSession {item.delete();}
          }
      }
   }
   */


    def beforeDelete() {

        // considerar campo abrangencia ou contexto para exlcuir as ref. bibliográficas
        String noColuna = '';
        // pelo contexto é aqui
        if (this.contexto.codigoSistema == 'ESTADO') {
            noColuna = 'sq_estado'
        } else if (this.contexto.codigoSistema == 'BIOMA') {
            noColuna = 'sq_bioma'
        } else if (this.contexto.codigoSistema == 'BACIA_HIDROGRAFICA') {
            noColuna = 'sq_bacia_hidrografica'
        } else if (this.contexto.codigoSistema == 'REGISTRO_OCORRENCIA') {
            noColuna = 'sq_ocorrencia'
        }

        if (noColuna) {
            FichaRefBib.withNewSession {
                FichaRefBib.findAllByNoTabelaAndSqRegistro('ficha_ocorrencia', id).each
                        {
                            it.delete(flush: true)
                        }
            }
        }
        return true;
    }


    String getRefBibHtml() {
        String coluna = 'sq_ficha_ocorrencia';

        if (this.contexto.codigoSistema == 'ESTADO') {
            coluna = 'sq_estado'
        } else if (this.contexto.codigoSistema == 'BIOMA') {
            coluna = 'sq_bioma'
        } else if (this.contexto.codigoSistema == 'BACIA_HIDROGRAFICA') {
            coluna = 'sq_bacia_hidrografica'
        }
        List refs = FichaRefBib.findAllByFichaAndNoTabelaAndNoColunaAndSqRegistro(this.ficha, 'ficha_ocorrencia', coluna, this.id);
        return Util.formatRefBib(refs,true)
        //List nomes = [];
            /*refs.each {
                String valor = ''
                if (it.publicacao) {
                    //nomes.push(it.publicacao.citacao + '&nbsp;<span class="label label-warning tag cursor-pointer" title="' + it.publicacao.deTitulo.replaceAll(/"/, "&quot;") + '">...</span>');
                    valor = it.publicacao.citacao + '&nbsp;<i class="fa fa-commenting-o" aria-hidden="true" title="' + it.publicacao.deTitulo.replaceAll(/"/, "&quot;") + '"></i>';
                } else if( it.noAutor ) {
                    //valor = it.noAutor + (it.nuAno ? ', ' + it.nuAno : '') + '&nbsp;<i class="fa fa-commenting-o blue" aria-hidden="true" title="Comunicação pessoal"></i>' //'&nbsp;<span class="label label-warning tag cursor-pointer" title="Comunicação Pessoal"></span>';
                    // Barbosa, Luis., 2020 (Com.Pess.)
                    valor = it.noAutor + (it.nuAno ? ', ' + it.nuAno : '') + '&nbsp;(Com.Pess.)';
                }
                if (valor && nomes.indexOf( valor ) == -1 ) {
                    nomes.push(valor)
                }
            }*/
        //return ''
        //return nomes.join('<br>');
    }

    /*String getCdSituacaoRegistro() {
        String situacao = ''
        if( inUtilizadoAvaliacao=='S'){
            situacao = 'CONFIRMADO'
            if( stAdicionadoAposAvaliacao ){
                situacao = 'CONFIRMADO_ADICIONADO_APOS_AVALIACAO'
            }
        } else {
            situacao = 'NAO_CONFIRMADO'
        }
        return situacao
    }

    String getDeSituacaoRegistro() {
        String situacao = ''
        if( inUtilizadoAvaliacao=='S'){
            situacao = 'Utilizado na Avaliação'
            if( stAdicionadoAposAvaliacao ){
                situacao = 'Confirmado e adicionado após a avaliação'
            }
        } else {
            situacao = 'Não utilizado na avaliação'
        }
        return situacao
    }*/

    String getTxNaoUtilizadoAvaliacaoSemUsuario() {
        Map data = Util.extrairTextoUsuarioDataTxNaoUtilizadoAvaliacao( txNaoUtilizadoAvaliacao )
        if (data.texto ) {
            return data.texto
        }
        return ''
    }

    /**
     * @example Justificativa por nao utilizar o registro na avaliação<br/>Cadastrado por Luís Eugênio Barbosa em 30/01/2022 15:43:23
     * @return ['nome':'Luis Eugênio Barbosa', data:'30/01/2022 15:43:23']
     */
    Map getUsuarioDataTxNaoUtilizadoAvaliacao() {
        return Util.extrairTextoUsuarioDataTxNaoUtilizadoAvaliacao( txNaoUtilizadoAvaliacao )
    }

    String getRefBibText() {
        String coluna = 'sq_ficha_ocorrencia';
        if (this.contexto.codigoSistema == 'ESTADO') {
            coluna = 'sq_estado'
        } else if (this.contexto.codigoSistema == 'BIOMA') {
            coluna = 'sq_bioma'
        } else if (this.contexto.codigoSistema == 'BACIA_HIDROGRAFICA') {
            coluna = 'sq_bacia_hidrografica'
        }
        List refs = FichaRefBib.findAllByFichaAndNoTabelaAndNoColunaAndSqRegistro(this.ficha, 'ficha_ocorrencia', coluna, this.id);
        return Util.formatRefBib(refs,false)

            /*List nomes = [];
            if( coluna != 'sq_ficha_ocorrencia' || this.geometry ) {
                List refs = FichaRefBib.findAllByFichaAndNoTabelaAndNoColunaAndSqRegistro(this.ficha, 'ficha_ocorrencia', coluna, this.id);
                refs.each {
                    if (it.publicacao) {
                        nomes.push(it.publicacao.citacao);
                    } else if (it.noAutor) {
                        nomes.push(it.noAutor + (it.nuAno ? ', ' + it.nuAno : '') + ' (Com.Pess.)')
                    }
                }
            }
            return nomes.join('\n');
           */
    }

    String getRefBibDWCA() {
        String coluna = 'sq_ficha_ocorrencia';

        if (this.contexto.codigoSistema == 'ESTADO') {
            coluna = 'sq_estado'
        } else if (this.contexto.codigoSistema == 'BIOMA') {
            coluna = 'sq_bioma'
        } else if (this.contexto.codigoSistema == 'BACIA_HIDROGRAFICA') {
            coluna = 'sq_bacia_hidrografica'
        }
        List nomes = [];
        if( coluna != 'sq_ficha_ocorrencia' || this.geometry ) {
            List refs = FichaRefBib.findAllByFichaAndNoTabelaAndNoColunaAndSqRegistro(this.ficha, 'ficha_ocorrencia', coluna, this.id);
            refs.each {
                if (it.publicacao) {
                    nomes.push(it.publicacao.referenciaHtml.replaceAll(/;  ?/, '; ').replaceAll(/\r/, ''))
                } else if (it.noAutor) {
                    nomes.push(it.noAutor + (it.nuAno ? ', ' + it.nuAno : '') + ' (Com.Pess.)')
                }
            }
        }
        return nomes.join('<br>');
    }


    /**
     * retornar a referência bibliográfica no formato MAP
     * @return
     */
    Map getRefBibMap() {

        Map res=[titulo:'',autor:'',ano:'',comPessoal:'']

        String coluna = 'sq_ficha_ocorrencia';

        if (this.contexto.codigoSistema == 'ESTADO') {
            coluna = 'sq_estado'
        } else if (this.contexto.codigoSistema == 'BIOMA') {
            coluna = 'sq_bioma'
        } else if (this.contexto.codigoSistema == 'BACIA_HIDROGRAFICA') {
            coluna = 'sq_bacia_hidrografica'
        }
        if( coluna != 'sq_ficha_ocorrencia' || this.geometry ) {
            List refs = FichaRefBib.findAllByFichaAndNoTabelaAndNoColunaAndSqRegistro(this.ficha, 'ficha_ocorrencia', coluna, this.id);
            refs.each {
                if (it.publicacao) {
                    res.titulo = it.publicacao.deTitulo
                    res.ano = it.publicacao.nuAnoPublicacao
                    res.autor = it.publicacao.noAutor
                } else if( it.noAutor ) {
                    res.comPessoa = it.noAutor + (it.nuAno ? ', ' + it.nuAno : '') + ' (Com.Pess.)'
                }
            }
        }
        return res
    }

    String getNoRegiao() {
        // o nome da região, vai depender de qual campo está preenchido, uf, bacia, pais, município etc...
        if (this.continente) {
            return this.continente.descricao;
        }
        if (this.pais) {
            return this.pais.noPais;
        }

        if (this.estado) {
            return this.estado.noEstado + '-' + this.estado.sgEstado;
        }

        if (this.municipio) {
            return this.municipio.noMunicipio + '-' + this.municipio.estado.sgEstado;
        }

        if (this.baciaHidrografica) {
            String pai = ''
            if (this.baciaHidrografica.pai) {
                pai = this.baciaHidrografica.pai.descricao + ' / ';
            }
            return this.baciaHidrografica.codigo + ' - ' + pai + this.baciaHidrografica.descricao;
        }

        if (this.bioma) {
            return this.bioma.descricao;
        }

        if (this.uc) {
            return this.uc?.sigla
        }

        if (this.divisaoAdministrativa) {
            return this.divisaoAdministrativa.descricao
        }

        if (this.ecoregiao) {
            return this.ecoregiao.descricao
        }

        return this.noLocalidade;
    }

    Integer getIdRegiao() {
        // o nome da região, vai depender de qual campo está preenchido, uf, bacia, pais, município etc...
        if (this.continente) {
            return this.continente.id;
        }
        if (this.pais) {
            return this.pais.id;
        }

        if (this.estado) {
            return this.estado.id;
        }

        if (this.municipio) {
            return this.municipio.id;
        }

        if (this.baciaHidrografica) {
            return this.baciaHidrografica.id;
        }

        if (this.bioma) {
            return this.bioma.id;
        }

        if (this.uc) {
            return this.uc.id;
        }

        if (this.divisaoAdministrativa) {
            return this.divisaoAdministrativa.id;
        }

        if (this.ecoregiao) {
            return this.ecoregiao.id;
        }
        return 0;
    }

    String getCoordenadaGmsHtml() {

        if (!this.geometry) {
            return '';
        }

        Double valor
        String nslw;
        Map result = ['lat'  : ['graus': 0, 'minutos': 0, 'segundos': 0, 'gms': '']
                      , 'lon': ['graus': 0, 'minutos': 0, 'segundos': 0, 'gms': '']
        ];

        // latitude
        valor = this.geometry.y
        if (valor < 0) {
            nslw = 'S'
            valor *= -1;
        } else {
            nslw = 'N'
        }
        result.lat.graus = valor.round();
        result.lat.minutos = (Double) ((valor - result.lat.graus) * 60).round();
        result.lat.segundos = (Double) ((((valor - result.lat.graus) * 60) - result.lat.minutos) * 60).round(2);
        result.lat.gms = result.lat.graus.toString() + "° " + result.lat.minutos.toString() + "' " + result.lat.segundos.toString() + "\" " + nslw;
        // longitude
        valor = this.geometry.x
        if (valor < 0) {
            nslw = 'W'
            valor *= -1;
        } else {
            nslw = 'L'
        }

        result.lon.graus = valor.round();
        result.lon.minutos = (Double) ((valor - result.lon.graus) * 60).round();
        result.lon.segundos = (Double) ((((valor - result.lon.graus) * 60) - result.lon.minutos) * 60).round(2);
        result.lon.gms = result.lon.graus.toString() + "° " + result.lon.minutos.toString() + "' " + result.lon.segundos.toString() + "\" " + nslw;
        return 'Lat: ' + result.lat.gms + '<br/>Lon: ' + result.lon.gms;
    }

    /**
     * retorna string com o nome ou sigla da uc
     * @return [description]
     */
    String getUcHtml() {
        Uc uc = this.uc
        if (uc) {
            return uc.noUc ? Util.capitalize( uc.noUc ) : uc.sgUnidade;
        }
        return ''
    }

    /**
     * retorno objeto uc
     * @return [description]
     */
    Uc getUc() {
        Integer id = 0;
        String esfera = '';
        if (this.sqUcFederal) {
            id = this.sqUcFederal;
            esfera = 'F'
        } else if (this.sqUcEstadual) {
            id = this.sqUcEstadual;
            esfera = 'E'
        } else if (this.sqRppn) {
            id = this.sqRppn;
            esfera = 'R'

        }
        if (id > 0) {
            return Uc.findBySqUcAndInEsfera(id, esfera);
        }
        return null;
    }

    String getInPresencaAtualText() {
        if (this.inPresencaAtual == 'S') {
            return 'Sim'
        } else if (this.inPresencaAtual == 'N') {
            return 'Não'
        } else if (this.inPresencaAtual == 'D') {
            return 'Desconhecida'
        }
        return ''
    }

    String getInUtilizadoAvaliacaoText() {
        if (this.inUtilizadoAvaliacao == 'E') {
            return 'Excluído'
        }

        if (this.contexto.codigoSistema == 'CONSULTA') {
            if (this.inUtilizadoAvaliacao == 'S') {
                return 'Sim'
            } else {
                return 'Não'
            }
        } else {
            if (this.inUtilizadoAvaliacao == 'N') {
                return 'Não'
            } else if (this.inUtilizadoAvaliacao == 'D') {
                return 'Desconhecido'
            } else {

                return 'Sim'
            }
        }
        return ''
    }

    String getInDataDesconhecidaText() {
        if (this.inDataDesconhecida == 'S') {
            return 'Sim'
        } else if (this.inDataDesconhecida == 'N') {
            return 'Não'
        } else if (this.inDataDesconhecida == 'D') {
            return 'Desconhecido'
        }
        return ''
    }

    Double getLatDecimal() {
        return (this?.geometry ? this.geometry.y : null)
    }

    Double getLonDecimal() {
        return (this?.geometry ? this.geometry.x : null)
    }

    String getNoUcFederal() {
        if (this.sqUcFederal) {
            Uc uc = Uc.findBySqUcAndInEsfera(this.sqUcFederal, 'F');
            if (uc) {
                return uc.sgUnidade ?: uc.noUc
            }
        }
        return ''
    }

    String getNoUcEstadual() {
        if (this.sqUcEstadual) {
            Uc uc = Uc.findBySqUcAndInEsfera(this.sqUcFederal, 'E');
            if (uc) {
                return uc.sgUnidade ?: uc.noUc
            }
        }
        return ''
    }

    String getNoRppn() {
        if (this.sqRppn) {
            Uc uc = Uc.findBySqUcAndInEsfera(this.sqUcFederal, 'R');
            if (uc) {
                return uc.sgUnidade ?: uc.noUc
            }
        }
        return ''
    }

    DadosApoio getAmbiente() {
        if (this.habitat) {
            return DadosApoio.get(this.habitat.rootId)
        }
        return null;
    }


    String getDataHora() {
        String resultado = ''
        if ( inDataDesconhecida == 'S' ) {
            resultado = 'desconhecida'
        } else if (nuAnoRegistro && nuMesRegistro && nuDiaRegistro) {
            resultado = nuDiaRegistro + '/' + nuMesRegistro.toString().padLeft(2, '0') + '/' + nuAnoRegistro
        } else if (nuAnoRegistro && nuMesRegistro) {
            resultado = nuAnoRegistro + '/' + nuMesRegistro.toString().padLeft(2, '0')
        } else if (nuAnoRegistro) {
            resultado = nuAnoRegistro
        }
        if (hrRegistro) {
            resultado += ' ' + hrRegistro
        }

        // data fim
        if (nuAnoRegistroFim && nuMesRegistroFim && nuDiaRegistroFim) {
            resultado += ' a ' + nuDiaRegistroFim + '/' + nuMesRegistroFim.toString().padLeft(2, '0') + '/' + nuAnoRegistroFim
        } else if (nuAnoRegistroFim && nuMesRegistroFim) {
            resultado += ' a ' + nuAnoRegistroFim + '/' + nuMesRegistroFim.toString().padLeft(2, '0')
        } else if (nuAnoRegistroFim) {
            resultado += ' a ' + nuAnoRegistroFim
        }
        return resultado
    }

    public String getNomeAbreviado() {
        if (pessoaInclusao) {
            return Util.nomeAbreviado(pessoaInclusao.noPessoa)
        }
        return ''
    }


    Date getCarencia()
    {
        Integer prazo = 0i
        if( this.dtInclusao )
        {
            if( this.prazoCarencia )
            {
                try {
                    String medida = this.prazoCarencia.codigoSistema.replaceAll(/[0-9_]/, '')
                    if (medida ==~ /DIAS?|MES(ES)?|ANOS?/)
                        prazo = this.prazoCarencia.codigoSistema.replaceAll(/[^0-9]/, '').toInteger()
                    if (prazo > 0) {

                        if (medida ==~ /DIAS?/) {
                            return this.dtInclusao + prazo
                        } else if (medida ==~ /ANOS?/) {
                            return this.dtInclusao + (365 * prazo)
                        } else if (medida ==~ /MES(ES)?/) {
                            return this.dtInclusao + (30 * prazo)
                        } else {
                            return null
                        }
                    }
                }
                catch( Exception e ) {
                    println 'Prazo carencia invalido ' +this?.prazoCarencia?.codigoSistema
                    return null
                }
            }
        }
        return null
    }


    String getDataFimCarencia()
    {
        Date dtFimCarencia = this.carencia
        if( dtFimCarencia )
        {
            return this.prazoCarencia.descricao + ' ('+ dtFimCarencia.format('dd/MM/yyyy')+')'
        }
        return 'Sem carência'

    }


    Boolean getEmCarencia()
    {
        if( ! this.carencia )
        {
            return true
        }
        if ( this.carencia > new Date() ) {
            return true
        }
        return false
    }

    String getIdOrigemText()
    {
        if( ! idOrigem )
        {
            return id.toString()
        }
        return idOrigem.toString()
    }

    Map getRefBib2Planilha() {
        Map result = [refBibs: [], autores: []]
        if (this.id) {
            FichaRefBib.createCriteria().list {
                eq('noTabela', 'ficha_ocorrencia')
                eq('noColuna', 'sq_ficha_ocorrencia')
                eq('sqRegistro', this.id.toInteger())
            }.each {
                String refBib = it.referenciaHtml
                if( ! result.refBibs.contains(refBib) ){
                    result.refBibs.push( refBib )
                }
                String autor = ''
                if( it.noAutor ){
                    autor = Util.nome2citacao( it.noAutor.trim() ) + ( it.nuAno ? ', ' + it.nuAno.toString() : '')
                    autor = autor.replaceAll(/\.(\s[0-9]{4})$/, '.,$1') + ' (Com.Pess.)'
                } else if ( it.publicacao ) {
                    autor = Util.getNomesEtAll(it.publicacao.noAutor) + (it.publicacao.nuAnoPublicacao ? ', ' + it.publicacao.nuAnoPublicacao.toString() : '')
                }
                autor = Util.stripTags3(autor)
                if( ! result.autores.contains( autor ) ){
                    result.autores.push( autor )
                }
            }
        }
        return result
    }


}
