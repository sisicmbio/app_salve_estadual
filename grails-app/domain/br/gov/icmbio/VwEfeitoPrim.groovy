package br.gov.icmbio

class VwEfeitoPrim {

    String dsEfeito
    String dsTrilha
    String deOrdem
    Integer nuNivel
    static constraints = {
    }

    static mapping = {
        version 				false
        sort                    deOrdem :  'asc'
        table       			name 	: 'salve.vw_efeito_prim'
        id          			column 	: 'sq_efeito', insertable: false, updateable: false
    }

    List getTrilhaList()
    {
        if( dsTrilha ) {
            return dsTrilha.split('\\|').reverse()
        }
        return []
    }

    String getTrilhaPath()
    {
        if( dsTrilha ) {
            return dsTrilha.split('\\|').reverse().join(' / ')
        }
        return ''
    }
}
