package br.gov.icmbio

class CicloConsultaFichaVersao {

    CicloConsultaFicha cicloConsultaFicha
    FichaVersao fichaVersao

    static constraints = {

    }
    static mapping = {
        version 	false
        table                name  : 'salve.ciclo_consulta_ficha_versao'
        id                   column:'sq_ciclo_consulta_ficha', generator: 'foreign', params: [property: 'cicloConsultaFicha']
        cicloConsultaFicha   column: 'sq_ciclo_consulta_ficha', insertable: false, updateable: false
        fichaVersao          column : 'sq_ficha_versao'
    }
}
