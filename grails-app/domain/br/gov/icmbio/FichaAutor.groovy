package br.gov.icmbio
import grails.converters.JSON;

class FichaAutor {

    Ficha ficha;
    DadosApoio autor;

    static constraints = {
    }

    static mapping = {
        version     false
        sort        id: 'asc'
        table       name:'salve.ficha_autor'
        id          column:'sq_ficha_autor'
        ficha       column: 'sq_ficha'
        autor       column:'sq_autor'
    }


    public asJson()
    {
        Map data = [:]
        data.put( 'sqFichaAutor'     ,this.id)
        data.put( 'sqAutor'          ,this.autor.id)
        data.put( 'deAutor'          ,this.autor.descricao)
        data.put( 'noCitacao'        ,this.autor.codigo)
        return data as JSON
    }

}