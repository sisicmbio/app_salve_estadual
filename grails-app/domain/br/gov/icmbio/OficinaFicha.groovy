package br.gov.icmbio


class OficinaFicha {

    private transient deleted = false
    boolean deleted

	Ficha ficha
    VwFicha vwFicha
    // manter histórico da ficha ao final da oficina
    DadosApoio categoriaIucn

    DadosApoio situacaoFicha
    String dsCriterioAvalIucn
    String stPossivelmenteExtinta
    String dsJustificativa
    Date dtAvaliacao
    String dsAgrupamento
    Boolean stTransferida = false
    Boolean stPfConvidado = false // Ponto Focal convidado ?
    Boolean stCtConvidado = false // Coordenador de Taxon convidado ?

    static belongsTo = [oficina:Oficina]
    //static hasMany = [validadorFicha:ValidadorFicha,oficinaFichaChat:OficinaFichaChat]
    static hasMany = [validadorFicha:ValidadorFicha,oficinaFichaChat:OficinaFichaChat]
    static hasOne = [oficinaFichaVersao:OficinaFichaVersao]

    static constraints = {
        categoriaIucn nullable: true
        dsCriterioAvalIucn nullable: true
        dsJustificativa nullable: true
        dtAvaliacao nullable: true
        dsAgrupamento nullable:true
        vwFicha nullable:true
        situacaoFicha nullable:true
        stTransferida nullable:true, defaultValue:false
        stPfConvidado nullable:true, defaultValue:false
        stCtConvidado nullable:true, defaultValue:false
        stPossivelmenteExtinta nullable:true, defaultValue:false
        oficinaFichaVersao nullable: true
    }

    static mapping = {
    	version false
     	table       name:'salve.oficina_ficha'
     	id          column:'sq_oficina_ficha', generator: 'identity'
     	oficina	    column:'sq_oficina'
     	ficha 	    column:'sq_ficha'
     	vwFicha     column:'sq_ficha',insertable: false,updateable: false
        categoriaIucn column:'sq_categoria_iucn'
        situacaoFicha column:'sq_situacao_ficha'
    }

    int getQtdValidadoresValidos()
    {
        int resultado = 0
        validadorFicha.each{
            if( it.cicloAvaliacaoValidador.situacaoConvite.codigoSistema!='CONVITE_RECUSADO')
            {
                resultado ++
            }
        }
        return resultado
    }

    /**
     * retornar true se houver pelo menos um validador pendente de comunicação
     * @return [description]
     */
    boolean getInComunicarAlteracao()
    {
        boolean resultado = false
        validadorFicha.each{
            if( it.cicloAvaliacaoValidador.situacaoConvite.codigoSistema=='CONVITE_ACEITO' && it.inComunicarAlteracao )
            {
                resultado = true
            }
        }
        return resultado
    }

    /**
     * retornar a data mais recente do último comunicado
     * @return [description]
     */
    Date getDtUltimoComunicado()
    {
        Date resultado = null
        validadorFicha.each{
            if( it.cicloAvaliacaoValidador.situacaoConvite.codigoSistema=='CONVITE_ACEITO')
            {
                if( it.dtUltimoComunicado )
                {
                if( !resultado || it.dtUltimoComunicado > resultado )
                    resultado = it.dtUltimoComunicado
                }
            }
        }
        return resultado
    }

    /**
     * retornar a data mais recente do último comunicado no formato texto
     * @return [description]
     */
    String getDtUltimoComunicadoText()
    {
        String resultado = ''
        Date data = this.getDtUltimoComunicado()
        if( data )
        {
            resultado = 'Último email '+data.format('dd/MM/yyyy HH:mm:ss')
        }
        return resultado
    }
}
