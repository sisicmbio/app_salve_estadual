package br.gov.icmbio

class FichaDeclinioPopulacional {

	Ficha ficha
	DadosApoio declinioPopulacional

    static constraints = {

    }

    static mapping = {
		version 				false
	   	table       			name 	: 'salve.ficha_declinio_populacional'
	   	sort 					declinioPopulacional:'asc'
	   	id          			column 	: 'sq_ficha_declinio_populacional',generator:'identity'
	   	ficha 					column  : 'sq_ficha'
	   	declinioPopulacional column 	: 'sq_declinio_populacional'
	}
}
