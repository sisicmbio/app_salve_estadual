package br.gov.icmbio

class WebUsuario
{

	WebInstituicao instituicao
	String username // vai ser o email
	String password
	boolean enabled = false // st_conta_ativada
	boolean accountLocked = false
	boolean accountExpired = false
	boolean passwordExpired = false
	String deHashAtivacao
	String noUsuario
	Date dtUltimoAcesso
	Date dtInclusao


    WebUsuario(String username, String password) {
		this()
		this.username = username
		this.password = password
	}

	def beforeValidate() {
		if( !this.dtInclusao )
		{
			this.dtInclusao = new Date()
		}
        if( this.email )
        {
            this.username = this.username.toLowerCase();
        }
		this.username = this.username.toLowerCase().trim()
	}

	static constraints = {
		username blank: false//, unique: true
		password blank: false
		deHashAtivacao  nullable:true, blank:true
		dtUltimoAcesso  nullable:true
	}

	static mapping = {
		version false
		table       name		: 'salve.web_usuario'
		id column				: 'sq_web_usuario', generator:'identity'
		password column			: 'de_senha'
		username column			: 'de_email',unique: true
		enabled  column			: 'st_conta_ativada'
		accountLocked column	: 'st_conta_bloqueada'
		accountExpired column	: 'st_conta_expirada'
		passwordExpired column	: 'st_senha_expirada'
		deHashAtivacao column 	: 'de_hash_ativacao'
		noUsuario column 		: 'no_usuario'
		dtUltimoAcesso column 	: 'dt_ultimo_acesso'
		dtInclusao column 		: 'dt_inclusao'
		instituicao column		: 'sq_web_instituicao'
	}

	String getFullName()
	{
		return noUsuario
	}

	String getEmail()
	{
		return username
	}
}
