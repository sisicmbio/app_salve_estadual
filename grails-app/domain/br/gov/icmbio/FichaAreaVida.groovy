package br.gov.icmbio
import grails.converters.JSON;

class FichaAreaVida {

	Ficha ficha
	DadosApoio unidadeArea
	String txLocal
	String vlArea
	Integer nuAno
	String txArea

    static constraints = {
    	nuAno		nullable:true
    	txArea		nullable:true
       }

    static mapping = {
		version 				false
	   	table       			name 	: 'salve.ficha_area_vida'
	   	id          			column 	: 'sq_ficha_area_vida',generator:'identity'
	   	ficha 					column  : 'sq_ficha'
	   	unidadeArea				column 	: 'sq_unidade_area'
	}

    public asJson()
    {
    	Map data = [:]
	    data.put( 'sqFichaAreaVida'				,this.id)
        data.put( 'sqFicha'						,this.ficha.id)
        data.put( 'sqUnidadeArea'				,this.unidadeArea.id)
        data.put( 'nuAno'						,this.nuAno)
        data.put( 'vlArea'						,this.vlArea)
        data.put( 'txArea'						,this.txArea)
        data.put( 'txLocal'						,this.txLocal)
        return data as JSON
    }

    public String getRefBibHtml()
    {
        //List nomes = [];
        List refs = FichaRefBib.findAllByFichaAndNoTabelaAndNoColunaAndSqRegistro(this.ficha,'ficha_area_vida','sq_ficha_area_vida',this.id);
        return Util.formatRefBib(refs,true)

        /*refs.each{
            if( it.publicacao )
            {
                nomes.push( it.publicacao.citacao+'&nbsp;<span class="label label-warning tag cursor-pointer" title="'+it.publicacao.deTitulo.replaceAll(/"/,"&quot;")+'">...</span>' );
            }
            else
            {
                nomes.push( it.noAutor + ', ' + it.nuAno + ' (Com.Pess.)&nbsp;<span class="label label-warning tag cursor-pointer" title="Comunicação Pessoal">...</span>' );
            }
        }
        return nomes.join('<br>');

         */
    }
}
