package br.gov.icmbio
import grails.converters.JSON;

class FichaVariacaoSazonal {

	Ficha ficha
	DadosApoio faseVida
	DadosApoio epoca
	DadosApoio habitat
	String dsFichaVariacaoSazonal

    static constraints = {
    	ficha nullable:false

    }

    static mapping = {
	 	version 	false
	  	// sort        nmComum : 'asc'
	   	table       name 	: 'salve.ficha_variacao_sazonal'
	   	id          column 	: 'sq_ficha_variacao_sazonal',generator:'identity'
	   	ficha 		column 	: 'sq_ficha'
	   	epoca   	column 	: 'sq_epoca'
	   	faseVida 	column  : 'sq_fase_vida'
	   	habitat 	column  : 'sq_habitat'
 	}

 	public asJson()
    {
    	Map data = [:]
	    data.put( 'sqFichaVariacaoSazonal',this.id)
        data.put( 'sqFicha',this.ficha.id)
        data.put( 'sqFaseVida',this.faseVida.id)
        data.put( 'sqEpoca',this.epoca.id)
		data.put( 'sqHabitat',this.habitat.id)
        data.put( 'dsFichaVariacaoSazonal',this.dsFichaVariacaoSazonal)
        data as JSON
    }

    public String getRefBibHtml() {
        //List nomes = [];
        List refs = FichaRefBib.findAllByFichaAndNoTabelaAndNoColunaAndSqRegistro(this.ficha,'ficha_variacao_sazonal','sq_ficha_variacao_sazonal',this.id);
        return Util.formatRefBib(refs,true)

        /*
        refs.each{
            if( it.publicacao )
            {
                nomes.push( it.publicacao.citacao+'&nbsp;<span class="label label-warning tag cursor-pointer" title="'+it.publicacao.deTitulo.replaceAll(/"/,"&quot;")+'">...</span>' );
            }
            else
            {
                nomes.push( it.noAutor + ', ' + it.nuAno + ' (Com.Pess.)&nbsp;<span class="label label-warning tag cursor-pointer" title="Comunicação Pessoal">...</span>' );
            }
        }
        return nomes.join('<br>');

         */
    }
}
