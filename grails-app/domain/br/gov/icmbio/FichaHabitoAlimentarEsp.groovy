package br.gov.icmbio
import grails.converters.JSON;

class FichaHabitoAlimentarEsp {
	Ficha ficha
    Taxon taxon
    DadosApoio categoriaIucn
    String  dsFichaHabitoAlimentarEsp

    static constraints = {
		ficha nullable:false
		taxon nullable:false
		categoriaIucn nullable:true
		dsFichaHabitoAlimentarEsp  nullable:true
    }
	static mapping = {
    	version 			false
	   	table       		name 	: 'salve.ficha_habito_alimentar_esp'
	   	id          		column 	: 'sq_ficha_habito_alimentar_esp',generator:'identity'
	   	ficha 				column  : 'sq_ficha'
		taxon 				column 	: 'sq_taxon'
		categoriaIucn 		column	: 'sq_categoria_iucn'
    }

	public asJson()
    {
    	Map data = [:]
	    data.put( 'sqFichaHabitoAlimentarEsp',this.id)
        data.put( 'sqFicha',this.ficha.id)
        data.put( 'sqTaxonEspecialista',this.taxon.id)
        data.put( 'deTaxonEspecialista',this.taxon.noCientifico) // para preencher o select2
        data.put( 'nivel',this.taxon.nivelTaxonomico.coNivelTaxonomico)
		data.put( 'nmCientifico',this.taxon.noCientifico)
        data.put( 'sqCategoriaIucn',( this.categoriaIucn ? this.categoriaIucn.id : null) )
        data.put( 'dsFichaHabitoAlimentarEsp',this.dsFichaHabitoAlimentarEsp)
        data as JSON
    }

    public String getRefBibHtml() {
        //List nomes = [];
        List refs = FichaRefBib.findAllByFichaAndNoTabelaAndNoColunaAndSqRegistro(this.ficha,'ficha_habito_alimentar_esp','sq_ficha_habito_alimentar_esp',this.id);
        return  Util.formatRefBib(refs,true)
        /*
        refs.each{
            if( it.publicacao )
            {
                nomes.push( it.publicacao.citacao+'&nbsp;<span class="label label-warning tag cursor-pointer" title="'+it.publicacao.deTitulo.replaceAll(/"/,"&quot;")+'">...</span>' );
            }
            else
            {
                nomes.push( it.noAutor + ', ' + it.nuAno + ' (Com.Pess.)&nbsp;<span class="label label-warning tag cursor-pointer" title="Comunicação Pessoal">...</span>' );
            }
        }
        return nomes.join('<br>');
        */
    }

    public String getCategoriaText()
    {
        if( categoriaIucn )
        {
            return categoriaIucn.descricaoCompleta
        }
        return ''
    }

}
