package br.gov.icmbio

class IucnPesquisaApoio {

    IucnPesquisa iucnPesquisa
    DadosApoio pesquisa
    Date dtInclusao
    Date dtAlteracao

    static constraints = {
    }

    static mapping = {
        version				false
        table				name            : 'salve.iucn_pesquisa_apoio'
        id          		column          : 'sq_iucn_pesquisa_apoio',generator:'identity'
        iucnPesquisa          column          : 'sq_iucn_pesquisa'
        pesquisa              column          : 'sq_dados_apoio'
    }

    def beforeValidate() {
        if( ! this.dtInclusao ) {
            dtInclusao = new Date()
        }
        this.dtAlteracao = new Date()
    }

}
