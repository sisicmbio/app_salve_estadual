package br.gov.icmbio

class FichaOcorrenciaRegistro {

    FichaOcorrencia fichaOcorrencia
    Registro registro
    Date dtInclusao
    static constraints = {
    }
    static mapping = {
        version 				false
        table       			name 	: 'salve.ficha_ocorrencia_registro'
        id          			column 	: 'sq_ficha_ocorrencia_registro',generator:'identity'
        registro                column  : 'sq_registro'
        fichaOcorrencia         column  : 'sq_ficha_ocorrencia'
    }
}
