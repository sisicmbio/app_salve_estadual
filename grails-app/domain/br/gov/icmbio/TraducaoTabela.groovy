package br.gov.icmbio

class TraducaoTabela {

    String noTabela
    String noColuna
    String dsColuna
    Date dtInclusao
    Date dtAlteracao

    static constraints = {
        noTabela nullable: false, blank: false
        noColuna nullable: false, blank: false
        dsColuna nullable: false, blank: false
    }

    static mapping = {
        version false
        sort noTabela: 'asc'
        table name: 'salve.traducao_tabela'
        id column: 'sq_traducao_tabela', generator: 'identity'
    }

    def beforeValidate() {
        if (!dtInclusao) {
            dtInclusao = new Date()
        }
        dtAlteracao = new Date()
    }
}
