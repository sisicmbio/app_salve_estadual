package br.gov.icmbio
import grails.converters.JSON;

class FichaAreaRelevancia {

    transient ucControle; // recebe a uc selecionada para a classe fazer a gravação no campo correto de acordo com a esfera

	Ficha ficha
	DadosApoio tipoRelevancia
	Estado estado
    Integer sqUcFederal
    Integer sqUcEstadual
    Integer sqRppn
    String txRelevancia
	String txLocal

    static constraints = {
    	txRelevancia nullable: true
    	txLocal nullable: true
        sqUcFederal nullable: true
        sqUcEstadual nullable: true
        sqRppn nullable:true

    }

    static mapping = {
    	version 	false
    	sort        id : 'asc'
	   	table       name 	: 'salve.ficha_area_relevancia'
	   	id          column 	: 'sq_ficha_area_relevancia',generator:'identity'
	   	ficha		column 	: 'sq_ficha'
	   	tipoRelevancia column: 'sq_tipo_relevancia'
	   	estado      column: 'sq_estado'
    }
    def beforeValidate() {
        this.sqUcFederal  = null;
        this.sqUcEstadual = null;
        this.sqRppn       = null;

        // gravar a uc de acordo com a esfera
        if( this?.ucControle )
        {
            this.txLocal = null;
            if( this.ucControle.inEsfera == 'F')
            {
                this.sqUcFederal = ucControle.sqUc
            }
            else if( this.ucControle.inEsfera == 'E')
            {
                this.sqUcEstadual = this.ucControle.sqUc;
            }
            else if( this.ucControle.inEsfera == 'R')
            {
                this.sqRppn       = this.ucControle.sqUc;
            }
        }
    }


 	public asJson()
    {
    	Map data = [:]
	    data.put( 'sqFichaAreaRelevante',this.id)
	    data.put( 'sqTipoRelevancia'	,this.tipoRelevancia.id)
        data.put( 'dsTipoRelevancia'	,this.tipoRelevancia.descricao)
        data.put( 'sqEstado'      	  	,this.estado.id)
        data.put( 'dsEstado'			,this.estado.noEstado)
        if( this.uc )
        {
            data.put('sqControle', this.uc.id);
            data.put('dsControle', this.getUcHtml() );
        }
        data.put( 'txRelevancia'		,this.txRelevancia)
        data.put( 'txLocal'				,this.txLocal)
        return data as JSON
    }

    public String getRefBibHtml()
    {
        //List nomes = [];
        List refs = FichaRefBib.findAllByFichaAndNoTabelaAndNoColunaAndSqRegistro(this.ficha,'ficha_area_relevancia','sq_ficha_area_relevancia',this.id);
        return Util.formatRefBib(refs,true)
        /*refs.each{
            if( it.publicacao )
            {
                nomes.push( it.publicacao.citacao+'&nbsp;<span class="label label-warning tag cursor-pointer" title="'+it.publicacao.deTitulo.replaceAll(/"/,"&quot;")+'">...</span>' );
            }
            else
            {
                nomes.push( it.noAutor + ', ' + it.nuAno + ' (Com.Pess.)&nbsp;<span class="label label-warning tag cursor-pointer" title="Comunicação Pessoal">...</span>' );
            }
        }
        return nomes.join('<br>');*/

    }

    public String getRefBibText()
    {
        //List nomes = [];
        List refs = FichaRefBib.findAllByFichaAndNoTabelaAndNoColunaAndSqRegistro(this.ficha,'ficha_area_relevancia','sq_ficha_area_relevancia',this.id);
        return Util.formatRefBib(refs,false)
        /*refs.each{
            if( it.publicacao )
            {
                nomes.push( it.publicacao.citacao )
            }
            else
            {
                nomes.push( it.noAutor + ', ' + it.nuAno + ' (Com.Pess.)')
            }
        }
        return nomes.join("\n");*/
    }

    public String getMunicipiosHtml()
    {
        List nomes = [];
        List municipios = FichaAreaRelevanciaMunicipio.findAllByFichaAreaRelevancia(this);
        municipios.each{
            nomes.push( it.municipio.noMunicipio/*+'-'+it.municipio.estado.sgEstado*/);
        }
        return nomes.join(', ');
    }

    /**
     * Retorna o nome da uc ou do local quando não tiver uc
     * @return [description]
     */
    public getLocalHtml()
    {
        String uc = this.getUcHtml();
        if( ! uc  )
        {
            return this.txLocal ?: '';
        }
        return uc?:'';
    }

    /**
     * retorna string com o nome ou sigla da uc
     * @return [description]
     */
    String getUcHtml()
    {
        Uc uc = this.uc
        if (uc)
        {
            return uc.noUc ? uc.noUc : uc.sgUnidade;
        }
        return '';
    }

    /**
     * retorno objeto uc
     * @return [description]
     */
    Uc getUc()
    {
        Integer id = 0;
        if (this.sqUcFederal)
        {
            id = this.sqUcFederal;
        }
        else if (this.sqUcEstadual)
        {
            id = this.sqUcEstadual;
        }
        else if (this.sqRppn)
        {
            id=this.sqRppn;
        }
        if( id > 0 )
        {
            return Uc.get(id)
        }
        return null;
    }
}
