package br.gov.icmbio

class IucnAmeaca {

    String cdIucnAmeaca
    String dsIucnAmeaca
    String dsIucnAmeacaOrdem
    Date dtInclusao
    Date dtAlteracao

    static constraints = {
    }

    static mapping = {
        version				false
        sort 				dsIucnAmeacaOrdem  : 'asc'
        table				name                : 'salve.iucn_ameaca'
        id          		column              : 'sq_iucn_ameaca',generator:'identity'
    }

    def beforeValidate() {
        if( ! this.dtInclusao ) {
            dtInclusao = new Date()
        }
        this.dtAlteracao = new Date()
        if( !this.dsIucnAmeacaOrdem ){
            this.dsIucnAmeacaOrdem = this.cdIucnAmeaca
        }
    }

    String getDescricaoCompleta() {
        return this.cdIucnAmeaca+' - '+this.dsIucnAmeaca
    }
}
