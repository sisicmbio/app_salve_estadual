package br.gov.icmbio

class IucnHabitat {

    String cdIucnHabitat
    String dsIucnHabitat
    String dsIucnHabitatOrdem
    Date dtInclusao
    Date dtAlteracao

    static constraints = {
    }

    static mapping = {
        version				false
        sort 				dsIucnHabitatOrdem  : 'asc'
        table				name                : 'salve.iucn_habitat'
        id          		column              : 'sq_iucn_habitat',generator:'identity'
    }

    def beforeValidate() {
        if( ! this.dtInclusao ) {
            dtInclusao = new Date()
        }
        this.dtAlteracao = new Date()
        if( !this.dsIucnHabitatOrdem ){
            this.dsIucnHabitatOrdem = this.cdIucnHabitat
        }
    }

    String getDescricaoCompleta() {
        return this.cdIucnHabitat+' - '+this.dsIucnHabitat
    }
}
