package br.gov.icmbio
import grails.converters.JSON;


class FichaSinonimia {
	Ficha ficha
	String noSinonimia
	String noAutor
	Integer nuAno

    static constraints = {
    	ficha nullable:false
        noSinonimia nullable:false,blank:false
        noAutor nullable:true
        nuAno nullable:true,min:1500, max:9999
    }

	static mapping = {
	 	version 	false
	  	sort        noSinonimia : 'asc'
	   	table       name 	: 'salve.ficha_sinonimia'
	   	id          column 	: 'sq_ficha_sinonimia',generator:'identity'
	   	ficha 		column 	: 'sq_ficha'
 	}


	def beforeValidate() {
		this.nuAno = null;
		if( this.noAutor )
		{
			String ano = this.noAutor.toString().replaceAll(/[^0-9]/,'');
			if( ano.isInteger() )
			{
				this.nuAno = ano.toInteger();
			}
		}
	}


	public asJson()
    {
    	Map data = [:]
	    data.put( 'sqFichaSinonimia'	,this.id)
        data.put( 'noSinonimia'		,this.noSinonimia)
        data.put( 'noAutor'				,this.noAutor)
        //data.put( 'nuAno'      			,this.nuAno)
        return data as JSON
    }

 	public String getRefBibHtml()
 	{
 		//List nomes = [];
 		List refs = FichaRefBib.findAllByFichaAndNoTabelaAndNoColunaAndSqRegistro(this.ficha,'ficha_sinonimia','no_sinonimia',this.id);
        return  Util.formatRefBib(refs,true)
 		/*refs.each{
 			if( it.publicacao )
            {
                //nomes.push( it.publicacao.citacao+'&nbsp;<span class="label label-warning tag cursor-pointer" title="'+it.publicacao.deTitulo.replaceAll(/"/,"&quot;")+'">...</span>' );
                nomes.push(it.publicacao.autores+'&nbsp;<span class="label label-warning tag cursor-pointer" title="'+it.publicacao.citacao.replaceAll(/"/,"&quot;")+'">...</span>')
            }
            else
            {
                nomes.push( it.noAutor + ', ' + it.nuAno + ' (Com.Pess.)&nbsp;<span class="label label-warning tag cursor-pointer" title="Comunicação Pessoal">...</span>');
            }
 		}
 		return nomes.join('<br>');
 		 */
 	}

    String getAutorAno() {
        if ( this.noAutor && this.nuAno) {
            //if( ! ( this.noAutor.trim().matches( '.* ?' + this.nuAno.toString() +'$' ) ) ) {
            if( this.noAutor.trim().indexOf( this.nuAno.toString() ) == -1 ) {
                return this.noAutor+', '+this.nuAno.toString()
            }
        }
        return this.noAutor
    }
}
