package br.gov.icmbio

class OficinaMemoria {

    String deLocalOficina
    Date dtInicioOficina
    Date dtFimOficina
    String deSalas
    String txMemoria

    static hasMany = [ participantes : OficinaMemoriaParticipante
                      ,anexos        : OficinaMemoriaAnexo ]

    static belongsTo = [oficina: Oficina]

    static constraints = {
        txMemoria nullable: true
        deLocalOficina nullable: true
    }

    static mapping = {
        version false
        table       name:'salve.oficina_memoria'
        id          column:'sq_oficina_memoria', generator: 'identity'
        oficina	    column:'sq_oficina'

    }

    String getPeriodo() {
        String periodo = ''
        if( dtInicioOficina && dtFimOficina ) {
            if ( dtInicioOficina == dtFimOficina ) {
                periodo = dtInicioOficina.format('dd/MM/yyyy')
            } else if (dtInicioOficina.format('MM/yyyy') == dtFimOficina.format('MM/yyyy')) {
                periodo = dtInicioOficina.format('dd') + ' a ' + dtFimOficina.format('dd/MM/yyyy')
            } else {
                periodo = dtInicioOficina.format('dd/MM/yyyy') + ' a ' + dtFimOficina.format('dd/MM/yyyy')
            }
        }
        return periodo
    }

}
