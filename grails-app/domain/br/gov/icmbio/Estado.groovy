package br.gov.icmbio
import com.vividsolutions.jts.geom.Geometry
import org.hibernate.spatial.GeometryType

class Estado {

	String noEstado
    String sgEstado
    Regiao regiao
    Pais pais
    Geometry geometry
    Date dtInclusao
    Date dtAlteracao

    static constraints = {
        geometry nullable: true
    	sgEstado nullable: false, blank: false, unique: true
    	noEstado nullable: false, blank: false, unique: true
        regiao nullable: false
        dtInclusao nullable: true
        dtAlteracao nullable: true
    }

    static mapping = {
    	version 	false
    	sort        noEstado: 'asc'
     	table       name  : 'salve.estado'
        id          column: 'sq_estado',generator:'identity'
     	pais 		column: 'sq_pais'
        regiao      column: 'sq_regiao'
        geometry    column: 'ge_estado'
        geometry    type: GeometryType, sqlType: "GEOMETRY"
    }

    def beforeValidate() {
        if ( ! this.dtInclusao ) {
            this.dtInclusao = new Date()
        }
        this.dtAlteracao = new Date()
    }
}
