package br.gov.icmbio

class VwUnidadeOrg {

  String sgUnidade
  String noPessoa
  Long sqTipo
  String nuCnpj
  String noContato
  String nuTelefone
  Date dtAlteracao

  static constraints = {
   }

 static mapping = {
      version false
	  table         name:'salve.vw_unidade_org'
	  id			column:'sq_pessoa'
      sgUnidade     column:'sg_instituicao'

  }

  String getSiglaNome() {
      if( sgUnidade ) {
          return sgUnidade + ((noPessoa == sgUnidade) ? '' : ' - ' + noPessoa)
      }
      return ''
  }
}