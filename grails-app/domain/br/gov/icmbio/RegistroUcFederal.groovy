package br.gov.icmbio

class RegistroUcFederal {

    Registro registro
    VwUnidadeOrg ucFederal
    Date dtInclusao

    static constraints = {
    }

    static mapping = {
        version false
        table       name    : 'salve.registro_uc_federal'
        id          column  : 'sq_registro_uc_federal',generator:'identity'
        ucFederal   column  : 'sq_uc_federal'
        registro    column  : 'sq_registro'
    }

    def beforeValidate() {
        if ( ! this.dtInclusao ) {
            this.dtInclusao = new Date()
        }
    }

}
