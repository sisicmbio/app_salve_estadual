package br.gov.icmbio

class FichaAreaRelevanciaMunicipio {

	FichaAreaRelevancia fichaAreaRelevancia
    Municipio municipio
	String txRelevanciaMunicipio

    static constraints = {
    	municipio nullable: false
    	fichaAreaRelevancia nullable: false
    	txRelevanciaMunicipio nullable: true
    }

    static mapping = {
    	version 	false
	   	table       name 	: 'salve.ficha_area_relevancia_municipio'
	   	id          column 	: 'sq_ficha_area_relevancia_municipio',generator:'identity'
	   	municipio           column: 'sq_municipio'
	   	fichaAreaRelevancia column: 'sq_ficha_area_relevancia'
    }
}
