package br.gov.icmbio

class TraducaoRevisaoHist {

    PessoaFisica pessoa
    TraducaoRevisao traducaoRevisao
    String txTraduzido
    Date dtRevisao
    Date dtInclusao

    static constraints = {
        dtRevisao nullable: true
    }

    static mapping = {
        version false
        sort noTabela: 'asc'
        table name: 'salve.traducao_revisao_hist'
        id column: 'sq_traducao_revisao_hist', generator: 'identity'
        pessoa column :'sq_pessoa'
        traducaoRevisao column: 'sq_traducao_revisao'
    }

    def beforeValidate() {
        if ( ! dtInclusao) {
            dtInclusao = new Date()
        }
    }
}
