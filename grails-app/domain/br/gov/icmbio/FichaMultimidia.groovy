package br.gov.icmbio

import grails.converters.JSON

class FichaMultimidia {

    Ficha ficha
    DadosApoio tipo
    DadosApoio situacao
    String deLegenda
    String txMultimidia
    String noAutor
    String deEmailAutor
    Date dtElaboracao
    String noArquivo
    String noArquivoDisco
    Date dtInclusao
    Boolean inPrincipal
    Boolean inDestaque
    String txNaoAceito

    Date dtAprovacao
    Pessoa aprovadoPor
    Date dtReprovacao
    Pessoa reprovadoPor

    static constraints = {
        txMultimidia nullable:true
        deEmailAutor nullable:true
        dtAprovacao nullable:true
        dtElaboracao nullable:true
        aprovadoPor nullable:true
        dtReprovacao nullable:true
        reprovadoPor nullable:true
        txNaoAceito nullable:true
    }

    static mapping = {
        version 				false
        table       			name 	: 'salve.ficha_multimidia'
        id          			column 	: 'sq_ficha_multimidia',generator:'identity'
        ficha 					column  : 'sq_ficha'
        tipo                    column  : 'sq_tipo'
        situacao                column  : 'sq_situacao'
        aprovadoPor             column  : 'sq_pessoa_aprovou'
        reprovadoPor           column   : 'sq_pessoa_reprovou'

    }
    def beforeValidate()
    {
        if( !dtInclusao )
        {
            dtInclusao = new Date()
        }
        if( situacao.codigoSistema != 'APROVADA')
        {
            dtAprovacao=null
            aprovadoPor=null
        }
        if( situacao.codigoSistema != 'REPROVADA')
        {
            dtReprovacao=null
            reprovadoPor=null
        }
    }

    def afterUpdate()
    {
        //println 'afterUpdate() verificar principal'
    }

    def afterInsert()
    {
        //println 'afterInsert() verificar principal'
    }

    public asJson()
    {
        Map data = [:]
        data.put( 'sqFichaMultimidia',this.id)
        data.put( 'sqTipo'           ,this.tipo.id)
        data.put( 'sqSituacao'       ,this.situacao.id)
        data.put( 'deLegenda'        ,this.deLegenda)
        data.put( 'txMultimidia'     ,this.txMultimidia)
        data.put( 'noAutor'          ,this.noAutor)
        data.put( 'deEmailAutor'     ,this.deEmailAutor)
        data.put( 'dtElaboracao'     ,this.dtElaboracao ? this.dtElaboracao.format('dd/MM/yyyy') : '' )
        data.put( 'inPrincipal'      ,this.inPrincipal)
        data.put( 'inDestaque'       ,this.inDestaque)
        data.put( 'txNaoAceito'      ,this.txNaoAceito)
        return data as JSON
    }

    String getInPrincipalText() {
        if( inPrincipal )
        {
            return 'Sim'
        }
        return 'Não'
    }

    String getInDestaqueText() {
        if( inDestaque )
        {
            return 'Sim'
        }
        return 'Não'
    }
}
