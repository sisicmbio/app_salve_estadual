package br.gov.icmbio

// tabela de atividades executando em segundo plano
class Worker {

    Pessoa pessoa
    String deRotulo // texto descritivo sobre a atividade que está sendo executada';
    String deCallback // Função javascript que será executado no browser ao finalizar';
    String txMensagem // mensagem que será exibida ao usuário
    Date dtInicio // hora inicial da atividade';
    Date dtFim // data e hora final da atividade';
    String txResultado // Resultado que será retornado para a aplicação';
    Double vlMax // Valor máximo para calculo do percentual de andamento';
    Double vlAtual // Valor atual já processado para calculo do percentual';

    static constraints = {
        dtInicio nullable:true
        dtFim nullable:true
        txResultado nullable:true
        deCallback nullable:true
        txMensagem nullable:true
    }

    static mapping = {
        version     false
        sort        dtInicio: 'asc'
        table       name:'salve.worker'
        id          column:'sq_worker'
        pessoa      column:'sq_pessoa'
    }

    def beforeValidate() {

        if( !deRotulo )
        {
            deRotulo='Executando...'+( id ? '('+id.toString()+')':'')
        }
        if( ! vlMax )
        {
            vlMax=100d
        }
        if( ! vlAtual && vlAtual!= 0 )
        {
            vlAtual=0d
        }
    }

    String getPercentual()
    {
        if( vlMax )
        {
            return Util.formatNumber(vlAtual / vlMax * 100,'#0.0')+'%'
        }
        return ''
    }
}
