package br.gov.icmbio

class PlanilhaFicha extends PlanilhaLinha {

    Ficha ficha
    String txAutorAno
    String txCentro
    String txGrupo
    String txSubgrupo
    String txCpfPf
    String txCpfCt
    String txNomeComum
    String txSinonimia
    String txNotasTaxonomicas
    String txNotasMorfologicas
    String txEndemicaBrasil
    String txDistribuicaoGlobal
    String txDistribuicaoNacional
    String txAltitudeMaxima
    String txAltitudeMinima
    String txBatimetriaMaxima
    String txBatimetriaMinima
    String txHistoriaNatural
    String txEspecieMigratoria
    String txPadraoDeslocamento
    String txTendenciaPopulacional
    String txPopulacao
    String txAmeaca
    String txUso
    String txPresencaListaOficial
    String txAcaoConservacao
    String txPresencaUc
    String txPesquisa
    String txTipoAvaliacao
    String txAnoAvaliacao
    String txEstadoAvaliacao
    String txCategoriaAvaliacao
    String txCriterioAvaliacao
    String txJustificativaAvaliacao
    String txPossivelmenteExtinta
    String txPendencias

    static constraints = {

        // campos não obrigatórios
        ficha                   nullable:true
        txAutorAno              nullable:true,blank:true
        txCentro                nullable:true,blank:true
        txGrupo                 nullable:true,blank:true
        txSubgrupo              nullable:true,blank:true
        txCpfPf                 nullable:true,blank:true
        txCpfCt                 nullable:true,blank:true
        txNomeComum             nullable:true,blank:true
        txSinonimia             nullable:true,blank:true
        txNotasTaxonomicas      nullable:true,blank:true
        txNotasMorfologicas     nullable:true,blank:true
        txEndemicaBrasil        nullable:true,blank:true
        txDistribuicaoGlobal    nullable:true,blank:true
        txDistribuicaoNacional  nullable:true,blank:true
        txAltitudeMaxima        nullable:true,blank:true
        txAltitudeMinima        nullable:true,blank:true
        txBatimetriaMaxima      nullable:true,blank:true
        txBatimetriaMinima      nullable:true,blank:true
        txHistoriaNatural       nullable:true,blank:true
        txEspecieMigratoria     nullable:true,blank:true
        txPadraoDeslocamento    nullable:true,blank:true
        txTendenciaPopulacional nullable:true,blank:true
        txPopulacao             nullable:true,blank:true
        txAmeaca                nullable:true,blank:true
        txUso                   nullable:true,blank:true
        txPresencaListaOficial  nullable:true,blank:true
        txAcaoConservacao       nullable:true,blank:true
        txPresencaUc            nullable:true,blank:true
        txPesquisa              nullable:true,blank:true
        txTipoAvaliacao         nullable:true,blank:true
        txAnoAvaliacao          nullable:true,blank:true
        txEstadoAvaliacao       nullable:true,blank:true
        txCategoriaAvaliacao    nullable:true,blank:true
        txCriterioAvaliacao     nullable:true,blank:true
        txJustificativaAvaliacao nullable:true,blank:true
        txPossivelmenteExtinta   nullable:true,blank:true
        txPendencias             nullable:true,blank:true

    }
    static mapping = {
        table       name:   'salve.planilha_ficha'
        id          column: 'sq_planilha_linha'
        ficha       column: 'sq_ficha'
    }


    def beforeInsert()
    {
        super.beforeInsert();

        if( ! this.txCentro )
        {
            this.erros.push('Erro: Nome do Centro/Sigla está em branco!')
        }
        else
        {
            if( ! isAdm )
            {
                // validar se o centro é o mesmo do usuário que está fazendo a importação
                Unidade unidade = Unidade.findByNoPessoaOrSgUnidade(this.txCentro,this.txCentro)
                if( this.planilha.unidade != unidade)
                {
                    this.erros.push('Erro: Centro ' + this.txCentro+' inválido, difere da lotação do usuário '+this.planilha.unidade.sgUnidade  );
                }
            }
        }
        //this.txErros = this.erros.join('<br/>') + ( this.txErros ? '<br/>' + this.txErros: '');
        return true
    }

}
