package br.gov.icmbio
import grails.converters.JSON
import liquibase.exception.DatabaseException
import org.codehaus.groovy.grails.web.json.JSONObject

class FichaHistoricoAvaliacao {

    Ficha ficha
	VwFicha vwFicha
    Taxon taxon
	DadosApoio tipoAvaliacao
    //DadosApoio tipoAbrangencia // Estado, bioma
	Abrangencia abrangencia     // Goiás, Cerrado
 	DadosApoio categoriaIucn
	Integer nuAnoAvaliacao
	String deCriterioAvaliacaoIucn
	String txJustificativaAvaliacao
	String txRegiaoAbrangencia
    String stPossivelmenteExtinta
	String noRegiaoOutra
	String deCategoriaOutra

    static constraints = {
        tipoAbrangencia             nullable:true
    	abrangencia				    nullable:true
    	deCriterioAvaliacaoIucn		nullable:true
    	txJustificativaAvaliacao	nullable:true
    	txRegiaoAbrangencia			nullable:true
    	noRegiaoOutra				nullable:true
        vwFicha 				    nullable:true
        taxon                       nullable:true
        stPossivelmenteExtinta      nullable:true, defaultValue:false
        deCategoriaOutra            nullable:true
       }

    static mapping = {
		version 				false
        sort                    nuAnoAvaliacao:'asc'
	   	table       			name 	: 'salve.ficha_historico_avaliacao'
	   	id          			column 	: 'sq_ficha_historico_avaliacao',generator:'identity'
	   	ficha 					column  : 'sq_ficha'
	   	vwFicha 				column  : 'sq_ficha', insertable: false, updateable: false
	   	tipoAvaliacao			column 	: 'sq_tipo_avaliacao'
        categoriaIucn           column  : 'sq_categoria_iucn'
        //tipoAbrangencia         column  : 'sq_tipo_abrangencia' // Estado, bioma
        abrangencia             column  : 'sq_abrangencia'      // Goias, Cerrado,
        taxon                   column  : 'sq_taxon'
	}
    def beforeInsert()
    {
        taxon = ficha.taxon
        return true
    }

    JSON asJson()
    {
    	Map data = [:]
	    data.put( 'sqFichaHistoricoAvaliacao'	,this.id)
        data.put( 'sqTipoAvaliacao'				,this.tipoAvaliacao.id)

        data.put( 'sqTipoAbrangencia'           ,this?.abrangencia?.tipo?.id)
    	data.put( 'deTipoAbrangencia'			,this?.abrangencia?.tipo?.descricao)
    	data.put( 'sqAbrangencia' 		        ,this?.abrangencia?.id )
    	data.put( 'deAbrangencia' 		        ,this?.abrangencia?.descricao)

        data.put( 'sqCategoriaIucn'				,this?.categoriaIucn?.id)
        data.put( 'deCategoriaOutra'				,this?.deCategoriaOutra)
        data.put( 'nuAnoAvaliacao'				,this.nuAnoAvaliacao)
        data.put( 'deCriterioAvaliacaoIucn'		,this.deCriterioAvaliacaoIucn)
        data.put( 'stPossivelmenteExtinta'	    ,this.stPossivelmenteExtinta)
        data.put( 'txJustificativaAvaliacao'	,this.txJustificativaAvaliacao)
        data.put( 'txRegiaoAbrangencia'			,this.txRegiaoAbrangencia)
        data.put( 'noRegiaoOutra'				,this.noRegiaoOutra)
        return data as JSON
    }

    String getRefBibHtml()
    {
        //List nomes = [];
        List refs = FichaRefBib.findAllByFichaAndNoTabelaAndNoColunaAndSqRegistro(this.ficha,'ficha_historico_avaliacao','sq_ficha_historico_avaliacao',this.id);
        return  Util.formatRefBib(refs,true)
        /*refs.each{
            if( it.publicacao )
            {
                nomes.push( it.publicacao.citacao+'&nbsp;<span class="label label-warning tag cursor-pointer" title="'+it.publicacao.deTitulo.replaceAll(/"/,"&quot;")+'">...</span>' );
            }
            else
            {
                nomes.push( it.noAutor + ', ' + it.nuAno + ' (Com.Pess.)&nbsp;<span class="label label-warning tag cursor-pointer" title="Comunicação Pessoal">...</span>' );
            }
        }
        return nomes.join('<br>');
        */
    }

    String getRefBibText()
    {
        //List nomes = [];
        List refs = FichaRefBib.findAllByFichaAndNoTabelaAndNoColunaAndSqRegistro(this.ficha,'ficha_historico_avaliacao','sq_ficha_historico_avaliacao',this.id);
        return  Util.formatRefBib(refs,false)
        /*refs.each{
            if( it.publicacao )
            {
                nomes.push( it.publicacao.citacao);
            }
            else
            {
                nomes.push( it.noAutor + ', ' + it.nuAno + ' (Com.Pess.)');
            }
        }
        return nomes.join('\n');
         */
    }

    DadosApoio getTipoAbrangencia()
    {
        return this?.abrangencia?.tipo
    }
}
