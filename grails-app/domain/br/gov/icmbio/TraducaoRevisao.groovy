package br.gov.icmbio

class TraducaoRevisao {

    PessoaFisica pessoa
    TraducaoTabela traducaoTabela
    Long sqRegistro
    String txTraduzido
    Date dtRevisao
    Date dtInclusao
    Date dtAlteracao

    static constraints = {
        dtRevisao nullable: true
        pessoa nullable: true
    }

    static mapping = {
        version false
        sort noTabela: 'asc'
        table name: 'salve.traducao_revisao'
        id column: 'sq_traducao_revisao', generator: 'identity'
        pessoa column :'sq_pessoa'
        traducaoTabela column: 'sq_traducao_tabela'
    }

    def beforeValidate() {
        if (!dtInclusao) {
            dtInclusao = new Date()
        }
        dtAlteracao = new Date()
    }
}
