package br.gov.icmbio

class FichaHabitat {
	Ficha ficha
	DadosApoio habitat
    static constraints = {
    	habitat nullable:false
    	ficha 	nullable:false;
    }
    static mapping = {
    	version 			false
        sort                habitat : 'asc'
	   	table       		name 	: 'salve.ficha_habitat'
	   	id          		column 	: 'sq_ficha_habitat',generator:'identity'
	   	ficha 				column  : 'sq_ficha'
	   	habitat 			column 	: 'sq_habitat'
    }
    public String getDescricao()
    {
        return (this.habitat.pai && this.habitat.pai.pai ? this.habitat.pai.descricao +' / ':'') + this.habitat.descricao
    }

    public String getRefBibHtml()
    {
        //List nomes = [];
        List refs = FichaRefBib.findAllByFichaAndNoTabelaAndNoColunaAndSqRegistro(this.ficha,'ficha_habitat','sq_ficha_habitat',this.id);
        return Util.formatRefBib(refs,true)

        /*
        refs.each{
            if( it.publicacao )
            {
                nomes.push( it.publicacao.citacao+'&nbsp;<span class="label label-warning tag cursor-pointer" title="'+it.publicacao.deTitulo.replaceAll(/"/,"&quot;")+'">...</span>' );
            }
            else
            {
                nomes.push( it.noAutor + ', ' + it.nuAno + ' (Com.Pess.)&nbsp;<span class="label label-warning tag cursor-pointer" title="Comunicação Pessoal">...</span>' );
            }
        }
        return nomes.join('<br>');
        */
    }

    public String getRefBibText()
    {
        //List nomes = [];
        List refs = FichaRefBib.findAllByFichaAndNoTabelaAndNoColunaAndSqRegistro(this.ficha,'ficha_habitat','sq_ficha_habitat',this.id);
        return Util.formatRefBib(refs,false)

        /*
        refs.each{
            if( it.publicacao )
            {
                nomes.push( it.publicacao.citacao )
            }
            else
            {
                nomes.push( it.noAutor + ', ' + it.nuAno + ' (Com.Pess.)' )
            }
        }
        return nomes.join('\n')
         */
    }














}
