package br.gov.icmbio

import org.h2.engine.User

class UserJobs {
    private transient ultimoPercentual = 0

    PessoaFisica usuario
    String deRotulo
    String deAndamento
    Integer vlMax
    Integer vlAtual
    String jsExecutar
    String deErro
    String deMensagem
    Date dtInclusao
    Date dtInicio
    Date dtFim
    Date dtUltimaAtualizacao
    String idElemento
    String deEmail
    String deEmailAssunto
    String noArquivoAnexo
    Boolean stEmailEnviado
    Boolean stCancelado
    Boolean stExecutado

    static constraints = {
        deAndamento nullable: true
        vlMax nullable: true
        vlAtual nullable: true
        jsExecutar nullable: true
        deErro nullable: true
        deMensagem nullable: true
        dtInicio nullable: true
        dtFim nullable: true
        dtUltimaAtualizacao nullable: true
        idElemento nullable: true
        deEmail nullable: true
        deEmailAssunto nullable: true
        noArquivoAnexo nullable: true
        stEmailEnviado nullable: true
        stCancelado nullable: true
        stExecutado nullable: true

    }

    static mapping = {
    	version 	false
     	table       name:'salve.user_jobs'
        id          column : 'sq_user_jobs', generator: 'identity'
        usuario     column : 'sq_pessoa'
    }

    def beforeValidate() {
        if( ! dtInclusao ) {
            dtInclusao = new Date()
        }
        // ajustar valores padrão
        // stCancelado = stCancelado == null ? false : stCancelado
        // stExecutado = stExecutado == null ? false : stExecutado
        if( vlMax && !vlAtual){
            vlAtual = 0
            deAndamento = '0%'
        }
        // remover espaços duplicados na menagem
        if( deMensagem ){
            deMensagem = deMensagem.replaceAll(/\s{2,}/,' ')
        }
    }

    /**
     * calcular o percentual colcluido da tarefa
     * @return
     */
    Double percentual(){
        Double current = 0
        if( vlMax > 0 && vlAtual > 0 ){
            if( vlAtual < vlMax) {
                current = Util.calcularPercentual(vlAtual, vlMax, 1)
            } else {
                current = 100d
            }
        }
        return current
    }

    String situacao(){
        String msg = ''
        if( deErro ){
            msg = deErro
        } else {
            if( stCancelado ){
                msg = 'Cancelado'
            } else if( stExecutado ){
                if( stEmailEnviado ) {
                    msg = 'Email enviado'
                } else {
                    msg = 'Executada'
                }
            } else {
                msg = 'Executando...'
            }
        }
        return msg
    }

    void stepIt( String msgAndamento = '' ) {
        boolean cancelado = false
        if( id ) {
            // verificar se o JOB foi cancelado
            withNewSession {
                UserJobs uj
                uj = UserJobs.createCriteria().get {
                    eq('id', id)
                }
                cancelado = uj.stCancelado
            }
        }
        if( vlMax ) {
            //println 'userJobs.stepIt()'
            vlAtual += 1
            if (vlAtual > vlMax) {
                vlAtual = vlMax
            }

            if( ! stCancelado ) {
                stCancelado = cancelado
            }
            if (msgAndamento) {
                deAndamento = msgAndamento
            } else {
                deAndamento = '%s'
            }
            if (vlAtual == vlMax) {
                dtFim = new Date()
            }
            // adicionar o percentual no andamento no lugar do %s se possui
            //Double p = percentual()
            //println vlMax + ' / ' + vlAtual + ' - ' + p + '%'
            deAndamento = sprintf(deAndamento, percentual().toString() + '%')
            dtUltimaAtualizacao = new Date()
            Double percentualAtual = percentual()
            if( Math.round(percentualAtual) > Math.round(ultimoPercentual) || dtFim ){
                ultimoPercentual = percentualAtual
            }
            save( flush:true )
        }
    }
}
