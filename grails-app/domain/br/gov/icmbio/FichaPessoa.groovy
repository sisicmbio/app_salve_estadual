package br.gov.icmbio

class FichaPessoa {

    Ficha ficha
    VwFicha vwFicha
    Pessoa pessoa
    WebInstituicao webInstituicao
    DadosApoio papel
    DadosApoio grupo
    String inAtivo

    static constraints = {
        vwFicha nullable: true
        webInstituicao nullable:true
        grupo nullable: true
    }

    static mapping = {
        version 			false
        table       		name 	: 'salve.ficha_pessoa'
        id          		column 	: 'sq_ficha_pessoa',generator:'identity'
        ficha 				column  : 'sq_ficha'
        vwFicha             column:'sq_ficha',insertable: false,updateable: false
        papel   			column 	: 'sq_papel'
        grupo     			column 	: 'sq_grupo'
        pessoa              column  : 'sq_pessoa'
        webInstituicao      column  : 'sq_web_instituicao'
    }
}
