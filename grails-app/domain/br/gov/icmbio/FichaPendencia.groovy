package br.gov.icmbio
import grails.converters.JSON;

class FichaPendencia {

	Ficha ficha
	VwFicha vwFicha
	PessoaFisica usuario
	DadosApoio contexto
	String deAssunto
	String txPendencia
	Date dtPendencia
	String stPendente
    PessoaFisica usuarioResolveu
    Date dtResolvida
    PessoaFisica usuarioRevisou
    Date dtRevisao
    static hasMany = [pendenciaEmail:FichaPendenciaEmail]

    static constraints = {
    	deAssunto nullable:true,blank:true
    	stPendente inList:['S','N']
		contexto nullable:true
		vwFicha nullable:true
        usuarioResolveu nullable: true
        dtResolvida nullable: true
        usuarioRevisou nullable: true
        dtRevisao nullable: true
    }

    static mapping = {
		version 				false;
	   	table       			name 	: 'salve.ficha_pendencia';
	   	id          			column 	: 'sq_ficha_pendencia',generator:'identity';
	   	ficha 					column  : 'sq_ficha';
		vwFicha     			column	: 'sq_ficha', insertable: false, updateable: false;
	   	usuario 				column  : 'sq_usuario';
        usuarioResolveu         column  : 'sq_usuario_resolveu'
        usuarioRevisou          column  : 'sq_usuario_revisou'
		contexto                column  : 'sq_contexto';
	}


    boolean canEdit( Map sessionSicae = [:] ) {
        // se for pendencia automática do sistema não pode ser alterada
        if( this.contexto && this.contexto.codigoSistema == 'SISTEMA' ){
            return false
        }

        if( sessionSicae && sessionSicae.user && this.contexto ) {
            // administradores podem editar
            if( sessionSicae.user.isADM() ) {
                //println 'ADM ok'
                return true;
            }

            // validador só pode editar suas proprias pendencias
            if( sessionSicae.user.isVL() ) {
                //println 'é um validador'
              if( usuario.id.toLong() == sessionSicae.user.sqPessoa.toLong() && this.contexto.codigoSistema == 'VALIDACAO' ) {
                  //println 'ok'
                 return true
             }
             //println 'nao'
            } else {
                // se for pendneica de revisao, somente ADM pode editar, os demais só podem marcar como resolvida ou não
                if ( this.contexto && this.contexto.codigoSistema == 'REVISAO' && sessionSicae.user.isADM()) {
                    //println 'ADM, Contexto revisao ok'
                    return true
                }
                // se a pendencia for do próprio usuário ele pode altera-la
                //println 'Id usuario dono:' + usuario.id.toLong()
                //println 'Id usuario logado:' + sessionSicae?.user.sqPessoa.toLong()
                if (sessionSicae?.user?.sqPessoa && usuario?.id?.toLong() == sessionSicae?.user?.sqPessoa?.toLong()) {
                    //println 'ok'
                    return true
                }
                //println 'nao'

                // se a pendencia for de uma ficha da mesma unidade do usuário ele pode altera-la
                //println 'Id unidade pendencia:' + sessionSicae?.user.sqUnidadeOrg.toLong()
                //println 'Id unidade usuario:' + vwFicha.sqUnidadeOrg.toLong()

                if ( sessionSicae?.user.sqUnidadeOrg?.toLong() == vwFicha?.sqUnidadeOrg?.toLong()) {
                    //println 'ok'
                    return true
                }
                //println 'nao'
            }
        } else {
            //println 'SEM SESSÃO'
        }
        return false
    }

    String logRevisao(){
        List resultado = []
        if( usuarioResolveu ){
            resultado.push('Resolvido por: ' + Util.nomeAbreviado(usuarioResolveu.noPessoa)+' em '+dtResolvida.format('dd/MM/yyyy') )
        }
        if( usuarioRevisou ){
            resultado.push('Revisado por: ' + Util.nomeAbreviado(usuarioRevisou.noPessoa)+' em '+dtRevisao.format('dd/MM/yyyy'))
        }
        return resultado.join('<br>')
    }


	public asJson()
    {
    	Map data = [:]
	    data.put( 'sqFichaPendencia'		    ,this.id)
	    data.put( 'sqUsuarioResolveu'	    ,this?.usuarioResolveu?.id)
	    data.put( 'sqUsuarioRevisao'		    ,this?.usuarioRevisou?.id)
        data.put( 'txPendencia'				,this.txPendencia)
        data.put( 'noPessoa'     			,this.usuario.noPessoa)
        data.put( 'deAssunto'				,this.deAssunto)
        data.put( 'dtPendencia'				,this.dtPendencia.format('dd/MM/yyyy'))
        data.put( 'stPendente'				,this.stPendente)
		// contexto
		data.put( 'sqContexto'				,this?.contexto?.id)
		data.put( 'cdContexto'				,this?.contexto?.cdSistema)
		data.put( 'dsContexto'				,this?.contexto?.descricao)
        data.put( 'noPessoaResolveu'  		,this?.usuarioResolveu?.noPessoa)
        data.put( 'noPessoaRevisou'  		,this?.usuarioRevisou?.noPessoa)
        data.put( 'dtResolvida'   			,this.dtResolvida?.format('dd/MM/yyyy'))
        data.put( 'dtRevisao'   			,this.dtRevisao?.format('dd/MM/yyyy'))

		return data as JSON
    }
}
