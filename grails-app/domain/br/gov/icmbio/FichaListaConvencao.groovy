package br.gov.icmbio
import grails.converters.JSON;

class FichaListaConvencao {


	Ficha ficha
	VwFicha vwFicha
	DadosApoio listaConvencao
	Integer nuAno

    static constraints = {
    	nuAno nullable:true
        vwFicha nullable:true
       }

    static mapping = {
		version 				false
	   	table       			name 	: 'salve.ficha_lista_convencao'
	   	id          			column 	: 'sq_ficha_lista_convencao',generator:'identity'
	   	ficha 					column  : 'sq_ficha'
        vwFicha                 column  : 'sq_ficha',insertable: false,updateable: false
	   	listaConvencao			column 	: 'sq_lista_convencao'
	}

    public asJson()
    {
    	Map data = [:]
	    data.put( 'sqFichaListaAmeaca'			,this.id)
        data.put( 'nuAno'						,this.nuAno)
        data.put( 'sqListaConvencao'       		,this.listaConvencao.id)
        data.put( 'deListaConvencao'			,this.listaConvencao.descricao+' - '+listaConvencao.codigo)
        return data as JSON
    }
}