package br.gov.icmbio

class FichaHistSituacaoExcluidaVersao {

    FichaHistSituacaoExcluida fichaHistSituacaoExcluida
    FichaVersao fichaVersao

    static constraints = {

    }
    static mapping = {
        version 	false
        table               name  : 'salve.ficha_hist_sit_exc_versao'
        id                  column: 'sq_ficha_hist_situacao_excluida', generator: 'foreign', params: [property: 'fichaHistSituacaoExcluida']
        fichaHistSituacaoExcluida  insertable: false, updateable: false, column: 'sq_ficha_hist_situacao_excluida'
        fichaVersao         column : 'sq_ficha_versao'
    }
}
