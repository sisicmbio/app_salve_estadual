package br.gov.icmbio

import org.hibernate.spatial.GeometryType

class RegistroEstado {

    Registro registro
    Estado estado
    Date dtInclusao

    static constraints = {
    }

    static mapping = {
        version 				false
        table       			name 	: 'salve.registro_estado'
        id          			column 	: 'sq_registro',generator:'assigned'
        estado                  column  : 'sq_estado'
        registro                column  : 'sq_registro', insertable: false, updateable: false
    }

    def beforeValidate() {
        if ( ! this.dtInclusao ) {
            this.dtInclusao = new Date()
        }
        this.id = registro?.id
    }
}
