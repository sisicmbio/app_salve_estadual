package br.gov.icmbio
import grails.converters.JSON;

class FichaNomeComum {

	String noComum
	String deRegiaoLingua
	Ficha ficha
	VwFicha vwFicha

    static constraints = {
    	ficha nullable:false
        noComum nullable:false, blank:false
        deRegiaoLingua nullable:true
		vwFicha nullable:true
    }

    static mapping = {
	 	version 	false
	  	sort        id      : 'asc'
	   	table       name 	: 'salve.ficha_nome_comum'
	   	id          column 	: 'sq_ficha_nome_comum',generator:'identity'
	   	ficha 		column 	: 'sq_ficha'
	   	vwFicha 	column 	: 'sq_ficha',insertable: false,updateable: false
 	}

	public asJson()
    {
    	Map data = [:]
	    data.put( 'sqFichaNomeComum'	,this.id)
        data.put( 'noComum'				,this.noComum)
        data.put( 'deRegiaoLingua'      ,this.deRegiaoLingua)
        return data as JSON
    }

 	public String getRefBibHtml() {
 		//List nomes = [];
 		List refs = FichaRefBib.findAllByVwFichaAndNoTabelaAndNoColunaAndSqRegistro(this.vwFicha,'ficha_nome_comum','no_comum',this.id);
        return  Util.formatRefBib(refs,true)
 		/*refs.each{
 			if( it.publicacao )
            {
                nomes.push( it.publicacao.citacao+'&nbsp;<span class="label label-warning tag cursor-pointer" title="'+it.publicacao.deTitulo.replaceAll(/"/,"&quot;")+'">...</span>' );
            }
            else
            {
                nomes.push( it.noAutor + ', ' + it.nuAno + ' (Com.Pess.)&nbsp;<span class="label label-warning tag cursor-pointer" title="Comunicação Pessoal">...</span>' );
            }
 		}
 		return nomes.join('<br>');
 		*/
 	}
}
