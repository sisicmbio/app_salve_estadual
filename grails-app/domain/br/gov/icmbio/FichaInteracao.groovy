package br.gov.icmbio
import grails.converters.JSON

class FichaInteracao {

    Ficha       ficha
    DadosApoio  tipoInteracao
    DadosApoio  classificacao
    Taxon       taxon
    DadosApoio  categoriaIucn
	String 		dsFichaInteracao

    static constraints = {
    	ficha 			nullable:false
        classificacao nullable:true
        categoriaIucn nullable:true
        dsFichaInteracao nullable:true, blank:true
    }

    static mapping ={
    	version 			false
	   	table       		name 	: 'salve.ficha_interacao'
	   	id          		column 	: 'sq_ficha_interacao',generator:'identity'
	   	ficha 				column  : 'sq_ficha'
	   	tipoInteracao 		column 	: 'sq_tipo_interacao'
	   	classificacao 		column	: 'sq_classificacao'
	   	taxon 				column  : 'sq_taxon'
	   	categoriaIucn 		column  : 'sq_categoria_iucn'
    }

    public asJson()
    {
    	Map data = [:]
    	data.put('sqFichaInteracao'	,this.id);
        data.put('sqFicha'			,this.ficha.id)
        data.put('sqTipoInteracao'	,this.tipoInteracao.id)
        data.put('sqClassificacao'	,(this.classificacao ? this.classificacao.id:null ) )
        data.put('sqTaxonInteracao' ,this.taxon.id)
        data.put('nivelInteracao'   ,this.taxon.nivelTaxonomico.coNivelTaxonomico)
        data.put('nmCientifico'     ,this.taxon.noCientifico)
        data.put('sqCategoriaIucn'	,( this.categoriaIucn ? this.categoriaIucn.id : null) )
        data.put('dsFichaInteracao'	,this.dsFichaInteracao)
        return data as JSON
    }

    public String getRefBibHtml()
    {
        //List nomes = [];
        List refs = FichaRefBib.findAllByFichaAndNoTabelaAndNoColunaAndSqRegistro(this.ficha,'ficha_interacao','sq_ficha_interacao',this.id);
        return Util.formatRefBib(refs,true)
        /*
        refs.each{
            if( it.publicacao )
            {
                nomes.push( it.publicacao.citacao+'&nbsp;<span class="label label-warning tag cursor-pointer" title="'+it.publicacao.deTitulo.replaceAll(/"/,"&quot;")+'">...</span>' );
            }
            else
            {
                nomes.push( it.noAutor + ', ' + it.nuAno + ' (Com.Pess.)&nbsp;<span class="label label-warning tag cursor-pointer" title="Comunicação Pessoal">...</span>' );
            }
        }
        return nomes.join('<br>');

         */
    }

    public String getRefBibText()
    {
       // List nomes = [];
        List refs = FichaRefBib.findAllByFichaAndNoTabelaAndNoColunaAndSqRegistro(this.ficha,'ficha_interacao','sq_ficha_interacao',this.id);
        return Util.formatRefBib(refs,false)
        /*
        refs.each{
            if( it.publicacao )
            {
                nomes.push( it.publicacao.citacao)
            }
            else
            {
                nomes.push( it.noAutor + ', ' + it.nuAno + ' (Com.Pess.)')
            }
        }
        return nomes.join('\n')
        */
    }
}
