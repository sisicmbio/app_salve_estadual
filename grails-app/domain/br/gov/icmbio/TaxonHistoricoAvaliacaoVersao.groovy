package br.gov.icmbio

import grails.converters.JSON

class TaxonHistoricoAvaliacaoVersao {

    TaxonHistoricoAvaliacao taxonHistoricoAvaliacao
    FichaVersao fichaVersao

    static constraints = {
    }

    static mapping = {
        version 				false
        table                   name: 'salve.taxon_historico_avaliacao_versao'
        id                      column:'sq_taxon_historico_avaliacao', generator: 'foreign', params: [property: 'taxonHistoricoAvaliacao']
        taxonHistoricoAvaliacao insertable: false, updateable: false, column: 'sq_taxon_historico_avaliacao'
        fichaVersao             column : 'sq_ficha_versao'
    }
}
