package br.gov.icmbio

class OficinaAnexo {

    PessoaFisica pessoa
    String noArquivo
    String deTipoConteudo
    String deLocalArquivo
    String deLegenda
    Date dtInclusao
    Boolean inDocFinal = false
    static belongsTo = [oficina:Oficina]

    static constraints = {
        deLegenda nullable: true
        inDocFinal nullable: true
    }

    static mapping = {
        version 	false
        sort        dtInclusao: 'desc'
        table       name:'salve.oficina_anexo'
        id          column:'sq_oficina_anexo',generator:'identity'
        oficina     column :'sq_oficina'
        pessoa      column :'sq_pessoa'
        inDocFinal  defaultValue: false
    }

    def beforeValidate() {
        this.dtInclusao = new Date()
    }

}
