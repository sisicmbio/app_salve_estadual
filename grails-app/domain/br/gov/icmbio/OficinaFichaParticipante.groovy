package br.gov.icmbio

class OficinaFichaParticipante {

    DadosApoio papel
    static belongsTo = [oficinaFicha:OficinaFicha, oficinaParticipante:OficinaParticipante]
    static constraints = {
    }

    static mapping = {
        version 	false
        table               name  : 'salve.oficina_ficha_participante'
        id                  column: 'sq_oficina_ficha_participante',generator:'identity'
        oficinaFicha        column: 'sq_oficina_ficha'
        oficinaParticipante column: 'sq_oficina_participante'
        papel               column: 'sq_papel_participante'
    }
}
