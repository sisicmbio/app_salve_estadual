package br.gov.icmbio

class ValidadorFicha {

    //static transient dadosApoioService

    static belongsTo = [cicloAvaliacaoValidador:CicloAvaliacaoValidador,oficinaFicha:OficinaFicha]
    static hasOne = [revisaoCoordenacao:ValidadorFichaRevisao]
    DadosApoio resultado
    DadosApoio categoriaSugerida
    String deCriterioSugerido
    String txJustificativa
    String stPossivelmenteExtinta
    Date dtAlteracao
    Date dtUltimoComunicado
    Boolean inComunicarAlteracao

    //String stAceitaCategoria
    //String stAceitaCriterio

    static constraints = {
        categoriaSugerida nullable: true
        deCriterioSugerido nullable: true
        //stAceitaCategoria nullable:true
        //stAceitaCriterio nullable: true
        dtAlteracao nullable:true
        txJustificativa nullable:true
        resultado nullable: true
        dtUltimoComunicado nullable:true
        inComunicarAlteracao nullable:true,default:false
        stPossivelmenteExtinta nullable: true
        revisaoCoordenacao nullable: true
    }

    static mapping = {
        version false
        table name: 'salve.validador_ficha'
        id column: 'sq_validador_ficha', generator: 'identity'
        cicloAvaliacaoValidador column:'sq_ciclo_avaliacao_validador'
        oficinaFicha column: 'sq_oficina_ficha'
        categoriaSugerida column:'sq_categoria_sugerida'
        resultado column :'sq_resultado'
    }

/**
 * Calcula a quantidade de validadores válidos e respostas A, B ou C
 * considerando as revisões da coordenação quando houverem
 * @return map com totais
 */
    Map getResultadosFicha()
    {
        Map result = [qtdValidadores:0,qtdA:0,qtdB:0,qtdC:0]
        ValidadorFicha.createCriteria().list{
            // o filtro tem que ser pelo oficinaFicha.id senão a ficha fica com mais de 2
            // validadore quando tiver as oficinas de avaliacao e validação
            eq('oficinaFicha.id', oficinaFicha.id )
            /*oficinaFicha {
                eq('ficha.id',oficinaFicha.ficha.id)
            }
            */
        }.each{
                // se o convite estiver valido contabilizar a resposta
                if( it.cicloAvaliacaoValidador.isValid)
                {
                    result.qtdValidadores++
                    // se a resposta do validador tiver uma revisão da coordenação, vale a revisão
                    if( it.revisaoCoordenacao ) {
                        if (it.revisaoCoordenacao.resultado?.codigoSistema == "RESULTADO_A") {
                            result.qtdA++
                        } else if (it.revisaoCoordenacao.resultado?.codigoSistema == "RESULTADO_B") {
                            result.qtdB++
                        } else if (it.revisaoCoordenacao.resultado?.codigoSistema == "RESULTADO_C") {
                            result.qtdC++
                        }
                    }
                    else {
                        if (it.resultado?.codigoSistema == "RESULTADO_A") {
                            result.qtdA++
                        } else if (it.resultado?.codigoSistema == "RESULTADO_B") {
                            result.qtdB++
                        } else if (it.resultado?.codigoSistema == "RESULTADO_C") {
                            result.qtdC++
                        }
                    }
                }
        }
        return result
    }


    String getTxPendencia()
    {
//        if( id )
//        {
//            DadosApoio contextoValidacao = dadosApoioService.getByCodigo('TB_CONTEXTO_PENDENCIA','VALIDACAO')
//
//            // gravar a pendencia
//            FichaPendencia fp = FichaPendencia.createCriteria().get{
//                eq('ficha.id'   , oficinaFicha.vwFicha.id )
//                eq('contexto.id', contextoValidacao.id )
//                eq('usuario.id'  , session.sicae.user.sqPessoa.toLong() )
//            }
//
//        }
        return ''
    }
}
