package br.gov.icmbio
import grails.converters.JSON

class FichaUsoRegiao {

    FichaUso fichaUso;
	FichaOcorrencia fichaOcorrencia
    static constraints = {
    	fichaOcorrencia nullable:true
    }

    static mapping = {

		version 	  	false
     	table       	name 	:'salve.ficha_uso_regiao'
     	id          	column	:'sq_ficha_uso_regiao',generator:'identity'
     	fichaUso 		column  :'sq_ficha_uso'
     	fichaOcorrencia column  :'sq_ficha_ocorrencia'
	}
	public String getNoRegiao()
	{
        String regiao = this?.fichaOcorrencia?.noRegiao
        if ( ! regiao )
        {
            regiao = this?.fichaOcorrencia?.abrangencia?.descricao
        }
		return regiao
	}

	public String getTxLocal()
	{
		return this?.fichaOcorrencia?.txLocal;
	}

	public String getSqTipoAbrangencia()
	{
		return this?.fichaOcorrencia?.tipoAbrangencia?.id;
	}
	public String getDeTipoAbrangencia()
	{
		return this?.fichaOcorrencia?.tipoAbrangencia?.descricao;
	}

    public asJson()
    {
        Map data = [:]
        data.put( 'id',this.id)
        if( this.fichaOcorrencia.geometry )
        {
            data.put( 'sqDatum'			,this.fichaOcorrencia?.datum?.id);
            data.put( 'fldLatitude'      ,this.fichaOcorrencia?.geometry?.y);
            data.put( 'fldLongitude'     ,this.fichaOcorrencia?.geometry?.x);
        }
        else
        {
            data.put( 'sqTipoAbrangencia'			,this.fichaOcorrencia?.tipoAbrangencia.id)
            data.put( 'sqAbrangencia' 		        ,this.fichaOcorrencia?.abrangencia?.id) // id varialvel de acordo com o tipo da abrangencia
            data.put( 'dsAbrangencia' 		        ,this.fichaOcorrencia?.abrangencia?.descricao) // nome varialvel de acordo com o tipo da abrangencia
            data.put( 'noRegiaoOutra'				,this.fichaOcorrencia?.noLocalidade)
        }
        data.put( 'txLocal'						,this.getTxLocal())
        return data as JSON
    }
}