package br.gov.icmbio

class VwAbrangenciaTrilha {


    VwAbrangencia pai
    String deAbrangencia
    String deAbrangenciaSemAcento
    DadosApoio tipo
    String cdSistema
    String cdAbrangencia
    String deOrdem
    String dsTrilha
    Integer nuNivel

    static constraints = {
    }

    static mapping = {
        version 				false
        table       			name 	: 'salve.vw_abrangencia_trilha'
        id          			column 	: 'sq_agrangencia', insertable: false, updateable: false
        pai                     column  : 'sq_abrangencia_pai'
        tipo                    column  : 'sq_tipo'
    }
}
