package br.gov.icmbio

class Esfera {

    String noEsfera

    static constraints = {
    }

    static mapping = {
        version 	false
        table       name:'corporativo.vw_esfera'
        id          column:'sq_esfera'
    }
}
