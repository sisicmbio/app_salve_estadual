package br.gov.icmbio

//import groovy.transform.EqualsAndHashCode
//import groovy.transform.ToString

//@EqualsAndHashCode(includes='authority')
//@ToString(includes='authority', includeNames=true, includePackage=false)
class WebRole {

	//private static final long serialVersionUID = 1

	String authority

	WebRole(String authority) {
		this()
		this.authority = authority
	}

	static constraints = {
		authority blank: false, unique: true
	}

	static mapping = {
		version false
		cache true
		table       name	: 'salve.web_role'
		id 			column	: 'sq_web_role', generator:'identity'
		authority 	column	: 'no_role'
	}
}
