package br.gov.icmbio

class RegistroBacia {

    DadosApoioGeo baciaGeo
    Date dtInclusao
    Registro registro

    //static belongsTo = [registro:Registro]

    static constraints = {
    }

    static mapping = {
        version 				false
        table       			name 	: 'salve.registro_bacia'
        id          			column 	: 'sq_registro_bacia',generator:'identity'
        baciaGeo                column  : 'sq_bacia_geo'
        registro                column  : 'sq_registro'
    }

    def beforeValidate() {
        if ( ! this.dtInclusao ) {
            this.dtInclusao = new Date()
        }
    }
}
