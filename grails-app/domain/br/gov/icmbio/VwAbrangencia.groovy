package br.gov.icmbio

class VwAbrangencia {

    VwAbrangencia pai
    String deAbrangencia
    String deAbrangenciaSemAcento
    DadosApoio tipo
    String cdSistema
    String cdAbrangencia
    String deOrdem

    static constraints = {
    }

    static mapping = {
        version 				false
        table       			name 	: 'salve.vw_abrangencia'
        id          			column 	: 'sq_abrangencia', insertable: false, updateable: false
        pai                     column  : 'sq_abrangencia_pai'
        tipo                    column  : 'sq_tipo'
    }

}
