package br.gov.icmbio

class Pessoa {

	String noPessoa
    String deEmail
    Date dtInclusao
    Date dtAlteracao

    static constraints = {
        deEmail nullable: true,blank: false
        dtInclusao nullable: true
        dtAlteracao nullable: true
    }

    static mapping = {
	 	version 		false
		table       	name:'salve.pessoa'
		id 				column:'sq_pessoa',generator:'identity'
	}

    def beforeValidate() {
        if( ! dtInclusao ) {
            dtInclusao = new Date()
        }
        dtAlteracao = new Date()
    }

	String getEmail(){
        return deEmail ?: ''
    }

    String getNoPessoaCap() {
        return Util.capitalize( noPessoa ? noPessoa.toLowerCase() : '' )
    }

    String getPrimeiroNome() {
        return Util.capitalize( noPessoa ? noPessoa.toLowerCase() : '' )
    }
}
