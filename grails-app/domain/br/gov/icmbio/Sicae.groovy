package br.gov.icmbio
/**
 * [12:16, 18/11/2020] Rodrigo Dibio:
 * COLABORADOR INTERNO: visualiza e baixa registros em carência
 * COLABORADOR EXTRNO: não visualiza nem baixa registros em carência
 * De resto, acho que as permissões permanecem como estavam
 * CONSULTA: não visualiza nem baixa registros em carência
 */
//import br.gov.icmbio.Util
import grails.converters.JSON

class Sicae {

    Long sqSistema;
    String sgSistema;
    String noSistema;
    String txSistema;
    String nuCpf;
    String noUsuario;
    Long sqPerfil
    String noPerfil
    String cdPerfil
    Long sqUnidadeOrg
    String noUnidadeOrg
    String sgUnidadeOrg
    String error
    String cookie
    Long sqPessoa
    String dateTime

    static constraints = {}
    static mapping = {}

    def isAttached()
    {
        return false
    }

    String getNuCpfFormatado()
    {
        return Util.formatCpf(this.nuCpf)
    }

    String getNoPessoa()
    {
        return this.noUsuario;
    }

    Integer getSqPessoa()
    {
        if (!this.sqPessoa)
        {
            PessoaFisica pf = PessoaFisica.findByNuCpf(this.nuCpf);
            if (pf)
            {
                this.sqPessoa = pf.id;
            }
        }
        return this.sqPessoa;
    }

    PessoaFisica getPessoa()
    {
        if ( this.sqPessoa )
        {
            return PessoaFisica.get(this.sqPessoa)

        }
        else if( this.nuCpf )
        {
            return PessoaFisica.findByNuCpf(this.nuCpf)
        }
        return null
    }

    String getNoUnidadeOrg()
    {
        if (this?.noUnidadeOrg)
        {
            return this.noUnidadeOrg
        }
        if (!this.sqUnidadeOrg)
        {
            return ''
        }
        Instituicao instituicao = Instituicao.get( this.sqUnidadeOrg )
        sgUnidadeOrg = instituicao?.sgInstituicao ?: ''
        noUnidadeOrg = instituicao?.pessoa?.noPessoa ?: ''
        return sgUnidadeOrg
    }

    String getSgUnidadeOrg()
    {
        if (this?.sgUnidadeOrg)
        {
            return sgUnidadeOrg
        }
        if (!this?.sqUnidadeOrg)
        {
            return ''
        }
        Instituicao instituicao = Instituicao.get(this?.sqUnidadeOrg)
        sgUnidadeOrg = instituicao?.sgInstituicao ?: ''
        return sgUnidadeOrg
    }

    String getEmail()
    {
        PessoaFisica pf = PessoaFisica.findByNuCpf(this.nuCpf)
        if (pf)
        {
            //
            Email email = Email.findByPessoaAndTipo(pf, 2)
            if (email)
            {
                return email.txEmail
            } else
            {
                email = Email.findByPessoa(pf)
                if (email)
                {
                    return email.txEmail
                }
            }
        }
        return ''
    }

    /**
     * Indica se o usuário tem perfil público ( perfil criado temporariamente para acessar o módulo publico logado)
     * @return [description]
     */
    boolean isPUB()
    {
        return ( Util.removeAccents( this?.noPerfil?.toUpperCase()) == 'PUBLICO' ||
            Util.removeAccents( this?.noPerfil?.toUpperCase()) == 'PUBLICO EXTERNO')
    }

    /**
     * Indica se o usuário tem perfil de administrador
     * @return [description]
     */
    boolean isADM()
    {
        return (this?.noPerfil?.toUpperCase() == 'ADMINISTRADOR' || this.cdPerfil=='ADM')
    }

    /**
     * Indica se o usuário tem perfil de ponto focal ou não
     * @return [description]
     */
    boolean isPF()
    {
        return (this?.noPerfil?.toUpperCase() == 'PONTO FOCAL' || this?.noPerfil?.toUpperCase() == 'PONTO FOCAL EXTERNO')
    }

    /**
     * Indica se o usuário tem perfil de Coordenador de Taxon
     * @return [description]
     */
    boolean isCT()
    {
        return ( Util.removeAccents(this?.noPerfil?.toUpperCase()) == 'COORDENADOR DE TAXON' || Util.removeAccents(this?.noPerfil?.toUpperCase()) == 'COORDENADOR DE TAXON EXTERNO')
    }

    /**
     * Indica se o usuário tem perfil de Colaborador interno
     * @return [description]
     */
    boolean isCI()
    {
        return ( this?.noPerfil?.toUpperCase() == 'COLABORADOR' || this?.noPerfil?.toUpperCase() == 'COLABORADOR INTERNO' )
    }

    /**
     * Indica se o usuário tem perfil de Colaborador externo
     * @return [description]
     */
    boolean isCE()
    {
        return (this?.noPerfil?.toUpperCase() == 'COLABORADOR EXTERNO')
    }

    /**
     * Indica se o usuário tem perfil de Colaborador eventual
     * @return [description]
     */
    boolean isEVT()
    {
        return (this?.noPerfil?.toUpperCase() == 'COLABORADOR EVENTUAL')
    }

    /**
     * Indica se o usuário tem perfil de Colaborador interno ou externo
     * @return [description]
     */
    boolean isCL()
    {
        return ( this.isCI() || this.isCE() )
    }


    /**
     * Indica se o usuário tem perfil de Validador
     * @return [description]
     */
    boolean isVL()
    {
        return ( this?.noPerfil?.toUpperCase() == 'VALIDADOR' || this?.noPerfil?.toUpperCase() == 'VALIDADOR EXTERNO')
    }

    /**
     * Indica se o usuário tem perfil externo
     * @return [description]
     */
    boolean isEX()
    {
        return (this?.noPerfil?.toUpperCase() =~ /EXTERN(O|A)$/)
    }

    /**
     * Indica se o usuário tem perfil de Consulta
     * @return [description]
     */
    boolean isCO()
    {
        return (this?.noPerfil?.toUpperCase() ==~ /^CONSULTA.*/)
    }

    /**
     * Indica se o usuário tem o perfil de Tradutor
     * @return
     */
    boolean isTradutor(){
        return (this?.noPerfil?.toUpperCase() ==~ /^TRADUTOR.*/)

    }

    /**
     * Indica se o usuário tem perfil de Consulta Externo
     * @return [description]
     */
    boolean isCOE()
    {
        return (this?.noPerfil?.toUpperCase() == 'CONSULTA EXTERNO')
    }


    /**
     * Indica se o usuário pode acessar o menu Ficha
     * @return [description]
     */
    boolean menuFicha()
    {
        return !isVL() && !isEVT()
    }


    /**
     * Indica se o usuário pode acessar o menu Gerenciar
     * @return [description]
     */
    boolean menuModulos()
    {
        return isADM() || isPF() || isCL() || isCT() || isCO() || isEVT()
    }

    /**
     * Indica se o usuário pode acessar o menu Gerenciar
     * @return [description]
     */
    boolean menuGerenciar()
    {
        return isADM() || isPF() || isCL() || isCT() || isTradutor()
    }

    /**
     * Indica se o usuário pode acessar o menu Gerenciar Função
     * @return [description]
     */
    boolean menuFuncao()
    {
        return isADM()
    }

    /**
     * Indica se o usuário pode acessar o menu Gerenciar Consulta
     * @return [description]
     */
    boolean menuConsulta()
    {
        return isADM() || isPF() || isCL() || isCT()
    }

    /**
     * Indica se o usuário pode acessar o menu Gerenciar Oficina
     * @return [description]
     */
    boolean menuOficina()
    {
        return isADM() || isPF() || isEVT()
    }

    /**
     * Indica se o usuário pode acessar o menu Gerenciar Validação
     * @return [description]
     */
    boolean menuValidacao()
    {
        return isADM() || isCT() || isPF() || isEVT()
    }

    /**
     * Indica se o usuário pode acessar o menu de Ref. Bibliográficas
     * @return [description]
     */

    boolean menuRefBib()
    {
        return isADM() || isPF() || isCT() || isCL()
    }

    boolean menuTaxonomia()
    {
        return isADM()
    }

    boolean menuRelatorio()
    {
        // REMOVER ACESSO DO PEFIL CONSULTA EXTERNO - REUNIÃO DIA 17/11/2022
        return (isADM() || isPF() || isCT() || isCI() || isCO() ) && !isCOE()
    }

    boolean menuPublicacao()
    {
        return isADM()
    }

    boolean menuTraducao(){
        return isADM() || isTradutor()
        //return isADM() || isPF() || isCI() || isCE() || isTradutor()
        //return isTradutor()
    }

    boolean canAddFicha()
    {
        return isADM() || isPF() || isCL()
    }

    boolean canDeleteFicha()
    {
        return isADM() || isPF()
    }

    boolean canEditFicha()
    {
        return isADM() || isPF() || isCL()
    }

    boolean canEditRefBib()
    {
        return isADM() || isPF() || isCL() || isCT()
    }


    // tela gerenciar Consulta
    //
    boolean canAddConsulta()
    {
        return isADM() || isPF()
    }

    boolean canDeleteConsulta()
    {
        return isADM() || isPF()
    }

    boolean canEditConsulta()
    {
        return isADM() || isPF()
    }

    boolean canConvidarConsultaAmpla()
    {
        return isADM() || isPF()
    }

    boolean canConvidarConsultaDireta()
    {
        return isADM() || isPF() || isCL()
    }

    /**
     * Permite visualizar a aba Consolidar de Gerenciar Consulta
     * @return [description]
     */
    boolean canConsolidarConsulta()
    {
        return isADM() || isPF() || isCT() || isCL()
    }

    String getCodigoPerfil(){
        if( isADM() ) {
            return 'ADM' // ADMINSTRADOR
        } else if( isPF() ) {
            return 'PF' // PONTO FOCAL
        } else if( isCT() ) {
            return 'CT' // COORDENADOR TAXON
        } else if( isCI() ) {
            return 'CI' // COLABORAR INTERNO
        } else if( isCE() ) {
            return 'CE' // COLABORADOR EXTERNO
        } else if( isCO() ) {
            return 'CO' // CONSULTA
        } else if( isPUB() ) {
            return 'PUB' // PUBLICO
        } else if( isVL() ) {
            return 'VL' // VALIDADOR
        } else if( isEVT() ) {
            return 'EVT' // COLABORADOR EVENTUAL
        } else if( isCOE() ) {
            return 'COE' // CONSULTA EXTERNA
        } else {
            return ''
        }
    }
    /**
     * Perfil Externo, Consulta e Coordenador de taxon NAO  podem exportar registros em carência
     * @return
     */
    boolean canExportRegsCarencia(){
        return ! ( isEX() || isCO() || isCT() )
    }

    /**
     * Perfil Externo e Consuta NÃO podem editar registros em carência
     * @return
     */
    boolean canEditRegsCarencia(){
        return ! ( isEX() || isCO() )
    }

    /**
     * Perfil Externo e Consuta NÃO podem editar registros em carência
     * @return
     */
    boolean canViewRegsCarencia(){
        return canExportRegsCarencia() || canEditRegsCarencia()
    }



    JSON asJson()
    {
        Map dadosSicae =[:]
        dadosSicae.sqSistema=sqSistema?:''
        dadosSicae.sgSistema=sgSistema?:''
        dadosSicae.noSistema=noSistema?:''
        dadosSicae.txSistema=txSistema?:''
        dadosSicae.nuCpf=nuCpf?:''
        dadosSicae.noUsuario=noUsuario?:''
        dadosSicae.sqPerfil=sqPerfil?:''
        dadosSicae.noPerfil=noPerfil?:''
        dadosSicae.cdPerfil=cdPerfil?:''
        dadosSicae.coPerfilSistema=codigoPerfil
        dadosSicae.sqUnidadeOrg=sqUnidadeOrg?:''
        dadosSicae.noUnidadeOrg=noUnidadeOrg?:''
        dadosSicae.sgUnidadeOrg=sgUnidadeOrg?:''
        dadosSicae.cookie=cookie?:''
        dadosSicae.sqPessoa=sqPessoa?:''
        dadosSicae.dateTime=dateTime?:''
        return dadosSicae as JSON
    }
}
