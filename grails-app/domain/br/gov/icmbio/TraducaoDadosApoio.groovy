package br.gov.icmbio

// Alteracoes integração com SIS/IUCN
class TraducaoDadosApoio {

    String txTraduzido
    String txRevisado
    Date dtInclusao
    Date dtAlteracao

    static belongsTo = [dadosApoio:DadosApoio]

    static constraints = {
        txTraduzido nullable: false, blank:false
        txRevisado nullable: false, blank:false
    }

    static mapping = {
        version 	  false
        sort        txTraduzido: 'asc'
        table       name:'salve.traducao_dados_apoio'
        id          column:'sq_traducao_dados_apoio',generator:'identity'
        dadosApoio  column:'sq_dados_apoio'
    }

    def beforeValidate() {
        if( ! dtInclusao ) {
            dtInclusao = new Date()
        }
        dtAlteracao = new Date()
    }
}
