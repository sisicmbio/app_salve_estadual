package br.gov.icmbio

class VwBacia {

    VwBacia pai
    String codigo
    String codigoSistema
    String descricao
    String descricaoCompleta
    String descricaoSemAcento
    String nivel
    String ordem
    String trilha
    static constraints = {
    }

    static mapping = {
        version 				false
        sort                    ordem :  'asc'
        table       			name 	: 'salve.vw_bacia_hidrografica'
        id          			column 	: 'id', insertable: false, updateable: false
        pai                     column  : 'id_pai'
    }

    List getTrilhaList()
    {
        if( trilha ) {
            return trilha.split('\\|').reverse()
        }
        return []
    }

    String getTrilhaPath()
    {
        if( trilha ) {
            return trilha.split('\\|').join(' / ')
        }
        return ''
    }
}
