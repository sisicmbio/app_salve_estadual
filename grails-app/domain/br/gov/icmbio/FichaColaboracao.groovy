package br.gov.icmbio

 class FichaColaboracao {
    DadosApoio situacao
    //DadosApoio tipoConsulta // identificar a partir de que tipo de consulta a colaboração foi feita. Direta,ampla ou revisão
    Usuario usuario
    String noCampoFicha
    String dsRotulo
    String dsColaboracao
    String dsRefBib
    Date dtInclusao
    Date dtAlteracao
    String txNaoAceita

    static belongsTo = [consultaFicha:CicloConsultaFicha]

    static constraints = {
        dtAlteracao nullable: true
        dsColaboracao nullable:true
        txNaoAceita nullable:true, blank:true
        //tipoConsulta nullable:true
    }

    static mapping = {
        version false
        table       name		: 'salve.ficha_colaboracao'
        id          column	    : 'sq_ficha_colaboracao', generator:'identity'
        consultaFicha column    : 'sq_ciclo_consulta_ficha'
        situacao    column      : 'sq_situacao'
        usuario     column      : 'sq_web_usuario'
        //tipoConsulta column     : 'sq_tipo_consulta'
    }

    def beforeValidate() {
        if( ! this.dtInclusao )
        {
            this.dtInclusao = new Date()
            this.dtInclusao = this.dtInclusao
        }
        else
        {
            this.dtAlteracao = new Date()
        }
    }

    def beforeInsert() {

    }

    def beforeUpdate() {

    }
    // identifica se a colaboração foi feita por um especialista convidado ou cidação comum
    boolean isDireta() {
        // TODO - VERIFICAR SE PARA A IDENTIFICACAO DA CONSULTA É PRECISO AINDA FAZER ASSIM OU SE JA TEM GRAVADO O ID DA CONSULTA NA COLABORACAO
        List lista = CicloConsultaPessoaFicha.createCriteria().list {
            createAlias('cicloConsultaPessoa', 'ccp')
            eq('ccp.pessoa', PessoaFisica.get(it.id))
            eq('cicloConsultaFicha', consultaFicha)
        } as List
        return (lista.size() > 0)
    }

    String getValorOriginal()
    {
        try
        {
            String campoTexto = noCampoFicha
            if( campoTexto == 'sinonimias' ) {
                campoTexto = 'sinonimiasHtml'
            }
            return consultaFicha.ficha[campoTexto]
        }
        catch( Exception e) {
            println e.getMessage()
        }
        return ''
    }
}
