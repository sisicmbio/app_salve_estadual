package br.gov.icmbio

class IucnMotivoMudancaApoio {

    IucnMotivoMudanca iucnMotivoMudanca
    DadosApoio motivoMudanca
    Date dtInclusao
    Date dtAlteracao

    static constraints = {
    }

    static mapping = {
        version				false
        table				name            : 'salve.iucn_motivo_mudanca_apoio'
        id          		column          : 'sq_iucn_motivo_mudanca_apoio',generator:'identity'
        iucnMotivoMudanca          column          : 'sq_iucn_motivo_mudanca'
        motivoMudanca              column          : 'sq_dados_apoio'
    }

    def beforeValidate() {
        if( ! this.dtInclusao ) {
            dtInclusao = new Date()
        }
        this.dtAlteracao = new Date()
    }

}
