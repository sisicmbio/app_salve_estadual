package br.gov.icmbio

// Alteracoes integração com SIS/IUCN
class TraducaoPan {

    String txTraduzido
    String txRevisado
    Date dtInclusao
    Date dtAlteracao

    static belongsTo = [planoAcao: PlanoAcao]

    static constraints = {
        txTraduzido nullable: false, blank:false
        txRevisado nullable: false, blank:false
        planoAcao nullable: false
    }

    static mapping = {
        version 	  false
        sort        txTraduzido: 'asc'
        table       name:'salve.traducao_pan'
        id          column:'sq_traducao_pan',generator:'identity'
        planoAcao   column:'sq_plano_acao'
    }

    def beforeValidate() {
        if( ! dtInclusao ) {
            dtInclusao = new Date()
        }
        dtAlteracao = new Date()
    }
}
