package br.gov.icmbio

class FichaOcorrenciaValidacao {

	FichaOcorrencia fichaOcorrencia
	FichaOcorrenciaPortalbio fichaOcorrenciaPortalbio
    CicloConsultaFicha cicloConsultaFicha
	WebUsuario usuario
    DadosApoio situacao
	String inValido
	String txObservacao
	Date dtInclusao
    static hasOne = [fichaOcorrenciaValidacaoVersao:FichaOcorrenciaValidacaoVersao]

    static constraints = {
    	fichaOcorrencia nullable:true
    	//fichaOcorrenciaConsulta nullable:true
    	fichaOcorrenciaPortalbio nullable:true
    	txObservacao nullable:true
    	inValido nullable:true
        cicloConsultaFicha nullable: true
        fichaOcorrenciaValidacaoVersao  nullable: true
        situacao nullable: true
    }

    static mapping = {
      	version 					false
	   	table       				name  : 'salve.ficha_ocorrencia_validacao'
	   	id          				column: 'sq_ficha_ocorrencia_validacao'
	   	fichaOcorrencia 			column: 'sq_ficha_ocorrencia'
	   	fichaOcorrenciaPortalbio    column: 'sq_ficha_ocorrencia_portalbio'
	   	usuario 				    column: 'sq_web_usuario'
        cicloConsultaFicha          column: 'sq_ciclo_consulta_ficha'
        situacao                    column: 'sq_situacao'
    }

    def beforeValidate()
    {
    	dtInclusao = new Date()
    }

    String getInValidoText()
    {
        if( inValido =='S')
        {
            return 'Concordo'
        }
        else if( inValido =='N')
        {
            return 'Não concordo'
        }
        return ''
    }
}
