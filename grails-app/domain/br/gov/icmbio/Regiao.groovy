package br.gov.icmbio

import com.vividsolutions.jts.geom.Geometry
import org.hibernate.spatial.GeometryType

class Regiao
{
    String noRegiao
    Integer nuOrdem
    Geometry geometry
    Date dtInclusao
    Date dtAlteracao

    static constraints = {
        geometry nullable: true
        nuOrdem nullable: true
        dtInclusao nullable: true
        dtAlteracao nullable: true
    }

    static mapping = {
        version false
        sort        noRegiao:'asc'
        table name: 'salve.regiao'
        id          column: 'sq_regiao',generator:'identity'
        geometry    column:'ge_regiao'
        geometry    type: GeometryType, sqlType: "GEOMETRY"
    }

    def beforeValidate() {
        if ( ! this.dtInclusao ) {
            this.dtInclusao = new Date()
        }
        this.dtAlteracao = new Date()
    }
}
