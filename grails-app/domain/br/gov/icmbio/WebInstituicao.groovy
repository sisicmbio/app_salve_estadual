package br.gov.icmbio
import grails.converters.JSON

class WebInstituicao {

    VwUnidadeOrg instituicao
    String noInstituicao
    String sgInstituicao // sigla
    Date dtInclusao

    static constraints = {
        instituicao nullable:false
        noInstituicao nullable:true,blank:true
        sgInstituicao nullable:true,blank:true
    }

    static mapping = {
        version false
        table name:'salve.web_instituicao'
        id column:'sq_web_instituicao', generator:'identity'
        instituicao column:'sq_unidade_org'
    }

    def beforeValidate() {
        if( !this.dtInclusao )
        {
            this.dtInclusao = new Date()
        }
    }

    String getNoInstituicao()
    {
        return this?.instituicao ? this?.instituicao?.sgUnidade : this?.noInstituicao
    }

    public asJson()
    {
        Map data = [:]
        data.put( 'sqWebInstituicao'            ,this.id)
        data.put( 'noInstituicao'               ,this.noInstituicao)
        data.put( 'sgInstituicao'               ,this.sgInstituicao)
        return data as JSON
    }





}
