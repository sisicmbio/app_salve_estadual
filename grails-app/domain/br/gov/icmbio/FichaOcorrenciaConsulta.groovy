package br.gov.icmbio

import grails.converters.JSON

class FichaOcorrenciaConsulta {

	FichaOcorrencia fichaOcorrencia
    DadosApoio situacao
    WebUsuario usuario
    String txObservacao

    static belongsTo = [cicloConsultaFicha:CicloConsultaFicha]

    static constraints = {
        txObservacao nullable:true,blank:true
        situacao nullable: true
    }

    static mapping = {
        version             false
        table               name:'salve.ficha_ocorrencia_consulta'
        id                  column:'sq_ficha_ocorrencia_consulta',generator:'identity'
        usuario             column:'sq_web_usuario'
        cicloConsultaFicha  column:'sq_ciclo_consulta_ficha'
        fichaOcorrencia     column:'sq_ficha_ocorrencia'
        situacao            column:'sq_situacao'
    }

    // identifica se a colaboração foi feita por um especialista convidado ou cidação comum
    boolean isDireta()
    {
        // localicar sq_pessoa pele email do web_usuario
        Email email = Email.findByTxEmail( usuario.username )
        if( ! email )
        {
            return false
        }
        else
        {
            List lista = CicloConsultaPessoaFicha.createCriteria().list {
                createAlias('cicloConsultaPessoa', 'ccp')
                eq('ccp.pessoa', email.pessoa)
                eq('cicloConsultaFicha', cicloConsultaFicha)
            }
            return (lista.size() > 0)
        }
    }

    public asJson()
    {
        Map data = [:]
        data.put( 'sqFichaOcorrenciaConsulta'   ,this.id)
        data.put( 'vlLat'                       ,this?.fichaOcorrencia.geometry.y)
        data.put( 'vlLon'                       ,this?.fichaOcorrencia?.geometry.x)
        data.put( 'sqDatum'                     ,this?.fichaOcorrencia?.datum?.id)

        data.put( 'nuDiaRegistro'               ,this?.fichaOcorrencia?.nuDiaRegistro)
        data.put( 'nuMesRegistro'               ,this?.fichaOcorrencia?.nuMesRegistro)
        data.put( 'nuAnoRegistro'               ,this?.fichaOcorrencia?.nuAnoRegistro)
        data.put( 'hrRegistro'                  ,this?.fichaOcorrencia?.hrRegistro)
        data.put( 'inDataDesconhecida'          ,this?.fichaOcorrencia?.inDataDesconhecida)

        data.put( 'sqPrecisaoCoordenada'        ,this?.fichaOcorrencia?.precisaoCoordenada?.id)
        data.put( 'sqRefAproximacao'            ,this?.fichaOcorrencia?.refAproximacao?.id)
        data.put( 'txObservacao'                ,this?.txObservacao)
        data.put( 'noLocalidade'                ,this?.fichaOcorrencia?.noLocalidade)

        // ler dados da referencia bibliografica / comunicacao pessoa
        FichaRefBib refBibOcorrencia = FichaRefBib.findByFichaAndSqRegistroAndNoTabelaAndNoColuna(fichaOcorrencia.ficha, fichaOcorrencia.id, 'ficha_ocorrencia', 'sq_ficha_ocorrencia')
        data.put( 'txRefBib'                    ,( refBibOcorrencia?.publicacao ? refBibOcorrencia.publicacao.referenciaHtml : '' ) )
        data.put( 'sqPublicacao'                ,  refBibOcorrencia?.publicacao?.id)
        data.put( 'deComunicacaoPessoal'        ,( refBibOcorrencia?.publicacao ? '' : (refBibOcorrencia?.noAutor?:'') ) )
        return data as JSON
    }

    List getMultimidias()
    {
        if( this.id ) {
            return FichaConsultaMultimidia.createCriteria().list {
                eq('fichaOcorrenciaConsulta.id', this.id)
            }
        }
        return []
    }

}
