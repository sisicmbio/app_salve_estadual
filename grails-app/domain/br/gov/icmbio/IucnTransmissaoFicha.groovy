package br.gov.icmbio

class IucnTransmissaoFicha {

    Ficha ficha
    IucnTransmissao iucnTransmissao

    static constraints = {

    }

    static mapping = {
        version false
        table name: 'salve.iucn_transmissao_ficha'
        id column: 'sq_iucn_transmissao_ficha', generator: 'identity'
        ficha column: 'sq_ficha'
        iucnTransmissao column:'sq_iucn_transmissao'
    }

}
