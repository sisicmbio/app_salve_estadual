package br.gov.icmbio

class IucnAcaoConservacaoApoio {

    IucnAcaoConservacao iucnAcaoConservacao
    DadosApoio acaoConservacao
    Date dtInclusao
    Date dtAlteracao

    static constraints = {
    }

    static mapping = {
        version				false
        table				name            : 'salve.iucn_acao_conservacao_apoio'
        id          		column          : 'sq_iucn_acao_conservacao_apoio',generator:'identity'
        iucnAcaoConservacao          column          : 'sq_iucn_acao_conservacao'
        acaoConservacao              column          : 'sq_dados_apoio'
    }

    def beforeValidate() {
        if( ! this.dtInclusao ) {
            dtInclusao = new Date()
        }
        this.dtAlteracao = new Date()
    }

}
