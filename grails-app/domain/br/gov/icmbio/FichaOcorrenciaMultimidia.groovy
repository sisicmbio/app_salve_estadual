package br.gov.icmbio

class FichaOcorrenciaMultimidia {

    static belongsTo = [fichaOcorrencia:FichaOcorrencia]
    FichaMultimidia fichaMultimidia

    static constraints = {
    }

    static mapping = {
        version 				false
        table       			name 	: 'salve.ficha_ocorrencia_multimidia'
        id          			column 	: 'sq_ficha_ocorrencia_multimidia',generator:'identity'
        fichaOcorrencia	        column  : 'sq_ficha_ocorrencia'
        fichaMultimidia	        column  : 'sq_ficha_multimidia'
    }
}
