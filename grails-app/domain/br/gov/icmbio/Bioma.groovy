package br.gov.icmbio
import com.vividsolutions.jts.geom.Geometry
import org.hibernate.spatial.GeometryType

class Bioma {

	String noBioma
	Geometry geometry
	Date dtInclusao
	Date dtAlteracao

	static constraints = {
		geometry nullable: true
		dtInclusao nullable: true
		dtAlteracao nullable: true
	}

    static mapping = {
    	version 	false
    	sort        noBioma: 'asc'
     	table       name:'salve.bioma'
     	id          column:'sq_bioma',generator:'identity'
		geometry    column: 'ge_bioma'
		geometry    type: GeometryType, sqlType: "GEOMETRY"

	}

	def beforeValidate() {
		if ( ! this.dtInclusao ) {
			this.dtInclusao = new Date()
		}
		this.dtAlteracao = new Date()
	}
}
