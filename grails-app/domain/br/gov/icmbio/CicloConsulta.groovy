package br.gov.icmbio

import grails.converters.JSON

class CicloConsulta {

    CicloAvaliacao cicloAvaliacao
    DadosApoio tipoConsulta
    VwUnidadeOrg unidade
    String deTituloConsulta
    String sgConsulta // sigla da consulta / nome curto
    Date dtInicio
    Date dtFim
    String txEmail

    static hasMany = [pessoas:CicloConsultaPessoa,
                      fichas :CicloConsultaFicha]

    static constraints = {
        txEmail nullable: true
    }

    static mapping = {
        version 	false
        table           name  :'salve.ciclo_consulta'
        id              column:'sq_ciclo_consulta', generator:'identity'
        cicloAvaliacao  column:'sq_ciclo_avaliacao'
        tipoConsulta    column:'sq_tipo_consulta'
        unidade         column:'sq_unidade_org'
    }

    String getFichasText()
    {
        //TODO - UTILIZAR CicloConsultaFichas.executeQuery() aqui.
        List itens = []
        this.fichas.each{
            //itens.push( it.ficha.noCientificoItalico)
            itens.push( Util.ncItalico(it.vwFicha.noCientificoItalico,true ) )
        }
        return itens.sort().join(', ')
    }

    String getPeriodoText()
    {
        if( this.dtInicio && this.dtFim )
        {
            return this.dtInicio.format('dd/MM/yyyy')+ ' a ' + this.dtFim.format('dd/MM/yyyy')
        }
        return '';
    }
    def asJson()
    {
        List fichas = []
        Map data = [:]
        data.put( 'sqCicloConsulta'			,this.id)
        data.put( 'sqTipoConsulta'			,this.tipoConsulta.id)
        data.put( 'deTituloConsulta'    		,this.deTituloConsulta)
        data.put( 'sgConsulta'         		,this.sgConsulta ?: this.deTituloConsulta)
        data.put( 'sqUnidadeOrg'            , this.unidade.id)
        data.put( 'dsUnidadeOrg'            , this.unidade.sgUnidade)
        data.put( 'dtInicio'				    ,this.dtInicio.format('dd/MM/yyyy') )
        data.put( 'dtFim'				        ,this.dtFim.format('dd/MM/yyyy') )
        /*this.fichas.each {
            fichas.push( it.vwFicha.id )
        }
        */
        data.put( 'fichas',fichas)
        return data as JSON
    }

    Boolean getIsOpen()
    {
        if( dtFim ) {
            if (dtFim >= Util.hoje() ) {
                return true
            }
        }
        return false
    }

    Integer nuFichasVersionadas(){
        if( !id ){
            return 0
        }
        List data = CicloConsultaFicha.executeQuery("""select new map( count(a.id) as nu_fichas )
            from CicloConsultaFicha  a
            join a.cicloConsultaFichaVersao b
            where a.cicloConsulta.id = :sqCicloConsulta""",[sqCicloConsulta:id])
        return data[0].nu_fichas
    }
}
