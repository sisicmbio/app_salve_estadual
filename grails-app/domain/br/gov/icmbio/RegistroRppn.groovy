package br.gov.icmbio

class RegistroRppn {

    Registro registro
    Rppn  rppn
    Date dtInclusao

    static constraints = {
    }

    static mapping = {
        version false
        table           name    : 'salve.registro_rppn'
        id              column  : 'sq_registro',generator: 'assigned'
        rppn            column  : 'sq_rppn'
        registro        column  : 'sq_registro', insertable: false, updateable: false
    }

    def beforeValidate() {
        if ( ! this.dtInclusao ) {
            this.dtInclusao = new Date()
        }
        this.id = registro?.id
    }
}
