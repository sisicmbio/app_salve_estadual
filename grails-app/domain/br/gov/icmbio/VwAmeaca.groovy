package br.gov.icmbio

class VwAmeaca {

    VwAmeaca pai
    String codigo
    String descricao
    String descricaoCompleta
    String descricaoSemAcento
    String ordem
    Integer nivel
    String trilha

    static constraints = {
    }

    static mapping = {
        version 				false
        sort                    ordem :  'asc'
        table       			name 	: 'salve.vw_ameacas'
        id          			column 	: 'id', insertable: false, updateable: false
        pai                     column  : 'id_pai'
    }

    List getTrilhaList()
    {
        if( trilha ) {
            return trilha.split('\\|').reverse()
        }
        return []
    }

    String getTrilhaPath()
    {
        if( trilha ) {
            return trilha.split('\\|').join(' / ')
        }
        return ''
    }
}
