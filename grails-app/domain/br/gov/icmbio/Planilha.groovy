package br.gov.icmbio

class Planilha
{

    CicloAvaliacao cicloAvaliacao
    VwUnidadeOrg unidade
    PessoaFisica pessoaFisica
    String noArquivo
    String deCaminho
    String deComentario
    String noContexto // Ficha, ocorrencia ...
    Date dtEnvio
    String txLog
    Integer nuPercentual
    Boolean inProcessando

    static hasMany = [ linhas: PlanilhaLinha]

    static constraints = {
        cicloAvaliacao nullable:true
        linhas nullable: true;
        dtEnvio nullable: false;
        txLog nullable:true;
        noArquivo maxSize: 255;
        deCaminho maxSize: 255;
        deComentario maxSize: 255;
        noContexto maxSize:50;
    }


    static mapping = {
        version false
        table name: 'salve.planilha'
        id column: 'sq_planilha', generator: 'identity'
        cicloAvaliacao column: 'sq_ciclo_avaliacao'
        unidade column: 'sq_unidade_org'
        pessoaFisica column: 'sq_pessoa'
    }

    def beforeValidate()
    {
        this.dtEnvio = new Date();
    }
    void clearLines()
    {
        PlanilhaLinha.findAllByPlanilha(this).each {
            it.delete()
        }
        this.linhas=null;
    }

    String getLineErrors()
    {
        if( this.linhas )
        {
            List erros = []
            this.linhas.sort{ it.nuLinha }.eachWithIndex { linha, i ->

                if ( linha.txErros )
                {
                    erros.push('<li><b><span style="color:#f00">Linha ' + linha.nuLinha.toString() + '</span></b><br>' + linha.txErros + '</li>');
                }
            }
            if ( erros )
            {
                return '<ul>' + erros.join('') + '</ul>';
            }
            else
            {
                if( txLog && txLog.find('Erro' ) )
                {
                    return ''
                }
                else
                {
                    return '<p class="green"><b>Nenhuma linha com erro!</b></p>';
                }
            }
        }
        return '';


    }
}
