package br.gov.icmbio

class TrilhaAuditoria {


    Date dtLog
    String deIp
    String deEnv
    String noPessoa
    String nuCpf
    String noModulo
    String noAcao
    String txParametros

    static constraints = {
        deIp nullable: true
        nuCpf  nullable: true
        txParametros nullable:true
    }

    static mapping = {
        version false
        sort        dtLog: 'asc'
        table       name:'salve.trilha_auditoria'
        id          column:'sq_trilha_auditoria', generator: 'identity'
    }

    def beforeValidate()
    {
        if( !dtLog)
        {
            dtLog = new Date()
        }
    }
}
