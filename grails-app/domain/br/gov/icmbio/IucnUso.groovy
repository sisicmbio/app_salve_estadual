package br.gov.icmbio
class IucnUso {

    String dsIucnUso
    String cdIucnUso
    String dsIucnUsoOrdem
    Date dtInclusao
    Date dtAlteracao

    static constraints = {
    }

    static mapping = {
        version				false
        sort 				dsIucnUsoOrdem  : 'asc'
        table				name            : 'salve.iucn_uso'
        id          		column          : 'sq_iucn_uso',generator:'identity'
    }

    def beforeValidate() {
        if( ! this.dtInclusao ) {
            dtInclusao = new Date()
        }
        this.dtAlteracao = new Date()
        if( !this.dsIucnUsoOrdem ){
            this.dsIucnUsoOrdem = this.cdIucnUso
        }
    }

    String getDescricaoCompleta() {
        return this.cdIucnUso+' - '+this.dsIucnUso
    }
}
