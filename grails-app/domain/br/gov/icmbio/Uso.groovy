package br.gov.icmbio

class Uso {

    Uso pai
	DadosApoio criterioAmeacaIucn
    IucnUso iucnUso
    String descricao
    String codigo
    String ordem

	static hasMany = [itens: Uso]
    static constraints = {
    	descricao nullable: false, blank: false
        codigo nullable:true,blank:true
    	criterioAmeacaIucn nullable: true
        iucnUso nullable: true
    }
    static mapping = {
    	version				false
    	sort 				ordem:'asc'
    	table				name:'salve.uso'
    	id          		column: 'sq_uso',generator:'identity'
	   	descricao			column: 'ds_uso'
        codigo              column: 'co_uso'
	   	pai         		column: 'sq_uso_pai'
        itens               column: 'sq_uso_pai'
        ordem               column: 'nu_ordem'
	   	criterioAmeacaIucn  column: 'sq_criterio_ameaca_iucn'
        iucnUso             column: 'sq_iucn_uso'
    }

    public String getDescricaoCompleta() {
        return this.codigo+' - '+this.descricao
    }

    public getDescricaoHieraquia() {
        List resultado = [this.descricaoCompleta]
        if( this.pai ) {
            resultado.push(this.pai.descricaoCompleta)
            if (this.pai.pai) {
                resultado.push(this.pai.pai.descricaoCompleta)
           }
        }
        return resultado.reverse().join(' / ')
    }

    def beforeValidate() {
      if( this.codigo )
      {
        this.ordem = '';
        this.codigo.split('\\.').each {
            this.ordem += (this.ordem ? '.' : '' ) + it.toString().padLeft(3,'0');
        }
      }
    }
}
