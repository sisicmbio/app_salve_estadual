package br.gov.icmbio

class TaxonGrupo {

    String deGrupo
    String coGrupo

    static constraints = {
    }

    static mapping = {
        version		false
        sort		deGrupo: 'asc'
        table       name 	: 'taxonomia.grupo'
        id			column  : 'sq_grupo'
    }
}
