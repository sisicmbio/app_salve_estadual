 package br.gov.icmbio

class TaxonHist {

	Taxon taxon
	Pessoa pessoa
 	NivelTaxonomico nivelTaxonomico
    TaxonSituacao situacao
    Taxon pai
    String noTaxon
    Boolean stAtivo
    Boolean inOcorreBrasil
    String noAutor
    Integer nuAno
    Date dtHistorico
    String deHistorico

    static constraints = {
        noAutor nullable:true,blank:true
        nuAno nullable: true
        pai nullable: true
        deHistorico nullable:true,blank:true
    }

    static mapping = {
    	version 	   false
	   	table           name   :'taxonomia.taxon_hist'
	   	id         	    column :'sq_taxon_hist', generator: 'identity'
        pessoa          column :'sq_pessoa'
		nivelTaxonomico column :'sq_nivel_taxonomico'
        situacao        column :'sq_situacao'
		taxon           column :'sq_taxon'
		pai 			column :'sq_taxon_pai'

	}

    def beforeValidate() {
        this.dtHistorico = new Date()
    }


}
