package br.gov.icmbio

class Abrangencia
{
    Abrangencia pai
    DadosApoio tipo // tipo da abrangência: Estado, Municipio, Bioma etc..
    Pais pais
    Regiao regiao
    Estado estado
    Municipio municipio
    VwUnidadeOrg ucFederal
    UcEstadual ucEstadual
    Rppn rppn
    DadosApoio salve
    //static hasMany = [itens : DadosApoio ]
    static constraints = {
        pais nullable:true
        regiao nullable:true
        estado nullable:true
        municipio nullable:true
        ucFederal nullable:true
        ucEstadual nullable:true
        rppn nullable:true
        salve nullable:true
    }

    static mapping = {
        version false
        table name: 'salve.abrangencia'
        id column: 'sq_abrangencia', generator: 'identity'
        pai column : 'sq_abrangencia_pai'
        //itens column:'sq_dados_apoio_pai'
        tipo column:'sq_tipo'
        municipio column: 'sq_municipio'
        ucFederal column: 'sq_uc_federal'
        estado column: 'sq_estado'
        ucEstadual column: 'sq_uc_nao_federal'
        rppn column: 'sq_rppn'
        pais column: 'sq_pais'
        regiao column: 'sq_regiao'
        salve column: 'sq_dados_apoio'
    }

    String getDescricao() {
        String descricao = ''
        if( salve )
        {
            descricao = salve.descricao
        }
        else {
            try {
                if (tipo.codigoSistema == 'PAIS') {
                    descricao = pais.noPais
                }else if (tipo.codigoSistema == 'ESTADO') {
                    descricao = estado.noEstado
                }else if (tipo.codigoSistema == 'MUNICIPIO') {
                    descricao = municipio.noMunicipio
                }else if (tipo.codigoSistema == 'DIVISAO_ADMINISTRATIVA') {
                    descricao = regiao.noRegiao
                }else if (tipo.codigoSistema == 'UNIDADE_CONSERVACAO') {
                    if (ucFederal) {
                        descricao = ucFederal.sgUnidade
                    }else if (ucEstadual) {
                        descricao = ucEstadual.noPessoa
                    }else if (rppn) {
                        descricao = rppn.sgRppn
                    }
                }
            }
            catch( Exception e )
            {
                descricao=''
            }
        }

        return descricao
    }

    String getCodigo()
    {
        String resultado=''
        if (salve)
        {
            if( salve.codigo )
            {
                resultado = salve.codigo
            }
        }
        return resultado
    }

    String getOrdem()
    {
        String resultado=''
        if (salve)
        {
            if( salve.ordem )
            {
                resultado = salve.ordem
            }
            else
            {
                resultado = salve.descricao.toUpperCase()
            }
        }
        return resultado
    }
}
