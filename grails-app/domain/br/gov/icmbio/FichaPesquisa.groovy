package br.gov.icmbio
import grails.converters.JSON;

class FichaPesquisa {

    Ficha       ficha
    DadosApoio  tema
    DadosApoio  situacao
	String txFichaPesquisa

    static constraints = {
    	txFichaPesquisa nullable:true,blank:true;
    }

    static mapping = {
		version 				false
	   	table       			name 	: 'salve.ficha_pesquisa'
	   	id          			column 	: 'sq_ficha_pesquisa',generator:'identity'
	   	ficha 					column  : 'sq_ficha'
	   	tema 					column 	: 'sq_tema_pesquisa'
	   	situacao				column 	: 'sq_situacao_pesquisa'
	}

	public asJson()
    {
    	Map data = [:]
	    data.put( 'sqFichaPesquisa'		,this.id)
        data.put( 'txFichaPesquisa'		,this.txFichaPesquisa)
        data.put( 'sqTemaPesquisa'      ,this.tema.id)
        data.put( 'sqSituacaoPesquisa'	,this.situacao.id)
        return data as JSON
    }

    public String getRefBibHtml()
    {
        //List nomes = [];
        List refs = FichaRefBib.findAllByFichaAndNoTabelaAndNoColunaAndSqRegistro(this.ficha,'ficha_pesquisa','sq_ficha_pesquisa',this.id);
        return Util.formatRefBib(refs,true)

        /*refs.each{
            if( it.publicacao )
            {
                nomes.push( it.publicacao.citacao+'&nbsp;<span class="label label-warning tag cursor-pointer" title="'+it.publicacao.deTitulo.replaceAll(/"/,"&quot;")+'">...</span>' );
            }
            else
            {
                nomes.push( it.noAutor + ', ' + it.nuAno + ' (Com.Pess.)&nbsp;<span class="label label-warning tag cursor-pointer" title="Comunicação Pessoal">...</span>' );
            }
        }
        return nomes.join('<br>');
         */
    }

    public String getRefBibText()
    {
        //List nomes = [];
        List refs = FichaRefBib.findAllByFichaAndNoTabelaAndNoColunaAndSqRegistro(this.ficha,'ficha_pesquisa','sq_ficha_pesquisa',this.id);
        return Util.formatRefBib(refs,false )
        /*refs.each{
            if( it.publicacao )
            {
                nomes.push( it.publicacao.citacao);
            }
            else
            {
                nomes.push( it.noAutor + ', ' + it.nuAno + ' (Com.Pess.)');
            }
        }
        return nomes.join('\n');
         */
    }
}
