package br.gov.icmbio

class IucnAcaoConservacao {

    String cdIucnAcaoConservacao
    String dsIucnAcaoConservacao
    String dsIucnAcaoConservacaoOrdem
    Date dtInclusao
    Date dtAlteracao

    static constraints = {
    }

    static mapping = {
        version				false
        sort 				dsIucnAcaoConservacaoOrdem  : 'asc'
        table				name                : 'salve.iucn_acao_conservacao'
        id          		column              : 'sq_iucn_acao_conservacao',generator:'identity'
    }

    def beforeValidate() {
        if( ! this.dtInclusao ) {
            dtInclusao = new Date()
        }
        this.dtAlteracao = new Date()
        if( !this.dsIucnAcaoConservacaoOrdem ){
            this.dsIucnAcaoConservacaoOrdem = this.cdIucnAcaoConservacao
        }
    }

    String getDescricaoCompleta() {
        return this.cdIucnAcaoConservacao+' - '+this.dsIucnAcaoConservacao
    }
}
