package br.gov.icmbio

class FichaOcorrenciaValidacaoVersao {

    FichaOcorrenciaValidacao fichaOcorrenciaValidacao
    FichaVersao fichaVersao

    static constraints = {
    }

    static mapping = {
        version 				false
        table                   name: 'salve.ficha_ocorrencia_validacao_versao'
        id                      column:'sq_ficha_ocorrencia_validacao', generator: 'foreign', params: [property: 'fichaOcorrenciaValidacao']
        fichaOcorrenciaValidacao insertable: false, updateable: false, column: 'sq_ficha_ocorrencia_validacao'
        fichaVersao             column : 'sq_ficha_versao'
    }
}
