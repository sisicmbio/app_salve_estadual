package br.gov.icmbio

class OficinaAnexoAssinatura {


    Date dtAssinatura
    Date dtEmail
    String deHash
    String txObservacao

    static belongsTo = [oficinaParticipante:OficinaParticipante,oficinaAnexo:OficinaAnexo]

    static constraints = {
        txObservacao nullable: true
        dtAssinatura nullable: true
        dtEmail nullable:true
    }

    static mapping = {
        version 	false
        table               name    : 'salve.oficina_anexo_assinatura'
        id                  column  : 'sq_oficina_anexo_assinatura',generator:'identity'
        oficinaParticipante column  : 'sq_oficina_participante'
        oficinaAnexo        column  : 'sq_oficina_anexo'

    }

    def beforeValidate() {
        if( ! deHash ) {
            if( this.oficinaParticipante && this.oficinaAnexo ) {
                this.deHash = Util.sha1(new Date().format('yyyy-MM-dd HH:mm:ss') + Util.randomString(16 ) )
            }
        }
    }

}
