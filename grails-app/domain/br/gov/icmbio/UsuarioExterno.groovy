package br.gov.icmbio

class UsuarioExterno {

    String noUsuarioExterno
    String txEmail

    static constraints = {
    }

    static mapping = {
      version     false
      table         name:'sicae.vw_usuario_externo'
      id            column:'sq_usuario_externo'
   }

}
