package br.gov.icmbio

class FichaPendenciaEmail {

    FichaPendencia fichaPendencia
    String txLog
    Date dtEnvio
    Date dtInclusao


    static constraints = {
        dtEnvio nullable: true
        txLog nullable: true
    }

    static mapping = {
        version 	false
        table           name  :'salve.ficha_pendencia_email'
        id              column:'sq_ficha_pendencia_email', generator:'identity'
        fichaPendencia  column:'sq_ficha_pendencia'
    }

    def beforeValidate() {
        if ( ! dtInclusao ) {
            dtInclusao = new Date()
        }
    }
}

