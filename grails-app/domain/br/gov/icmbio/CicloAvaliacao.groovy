package br.gov.icmbio
import grails.converters.JSON

class CicloAvaliacao {

	Integer nuAno
	String deCicloAvaliacao
	String inSituacao
	String inOficina
    String inPublico

	//static hasMany = [oficinas : Oficina ];
	static constraints = {

		nuAno nullable: false, minValue: 2000
		deCicloAvaliacao nullable: false, blank: false
		inSituacao nullable: false, blank: false, maxSize: 1, inList: ['A', 'F']
		inOficina nullable: false, blank: false, maxSize: 1, inList: ['S', 'N']
        inPublico nullable: false, blank: false, maxSize: 1, inList: ['S', 'N']
    }

	static mapping = {
		version false;
		sort nuAno: 'asc';
		table name: 'salve.ciclo_avaliacao';
		id column: 'sq_ciclo_avaliacao', generator: 'identity';
	}

	public asJson() {
		Map data = [:]
		data.put('sqCicloAvaliacao', this.id)
		data.put('nuAno', this.nuAno)
		data.put('deCicloAvaliacao', this.deCicloAvaliacao)
		data.put('inSituacao', this.inSituacao)
		data.put('inOficina', this.inOficina)
		data.put('inPublico', this.inPublico)
		return data as JSON
	}

	Integer getAnoFinal() {
		if (this.nuAno > 0) {
			return this.nuAno + 4
		}else {
			return this.nuAno
		}
	}

	boolean isLastCicle() {
		//CicloAvaliacao lastCicle = CicloAvaliacao.findAllByInOficina('S',[sort: 'nuAno', order: 'desc', max:1] )[0]
		//return lastCicle?.id?.toInteger() == this.id.toInteger()
		// POR ENQUANTO CONSIDERAR SE O CICLO ESTÁ ABERTO OU NÃO DEPOIS AJUSTAR PARA VALER SOMENTE O ULTIMO CICLO
		return this.inSituacao == 'A'
	}
}
