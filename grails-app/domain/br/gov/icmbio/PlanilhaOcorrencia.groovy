package br.gov.icmbio

class PlanilhaOcorrencia extends PlanilhaLinha
{
    FichaOcorrencia ocorrencia
    String txLatitude
    String txLongitude
    String txPrazoCarencia
    String txDatum
    String txFormatoOriginal
    String txPrecisaoCoordenada
    String txReferenciaAproximacao
    String txMetodoAproximacao
    String txDescricaoMetodoAproximacao
    String txTaxonOriginalCitado
    String txPresencaAtual
    String txTipoRegistro
    String txDia
    String txMes
    String txAno
    String txHora
    String txDataAnoDesconhecidos
    String txLocalidade
    String txCaracteristicaLocalidade
    String txUcFederal
    String txUcEstadual
    String txRppn
    String txPais
    String txEstado
    String txMunicipio
    String txAmbiente
    String txHabitat
    String txElevacaoMinima
    String txElevacaoMaxima
    String txProfundidadeMinima
    String txProfundidadeMaxima
    String txIdentificador
    String txDataIdentificacao
    String txIndicacaoIncerta
    String txTombamento
    String txInstituicaoTombamento
    String txCompilador
    String txDataCompilacao
    String txRevisor
    String txDataRevisao
    String txValidador
    String txDataValidacao
    String txUsadoAvaliacao
    String txJustificativaNaoUso
    String idOrigem
    String idRefBibSalve
    String deDoi
    String txTituloRefBib
    String txAutorRefBib
    String txAnoRefBib
    String txComunicacaoPessoal
    String txObservacao

    static constraints = {

        ocorrencia nullable:  true
        txLatitude nullable: true, blank: true
        txLongitude nullable: true, blank: true
        txPrazoCarencia nullable: true, blank: true
        txDatum nullable: true, blank: true
        txFormatoOriginal nullable: true, blank: true
        txPrecisaoCoordenada nullable: true, blank: true
        txReferenciaAproximacao nullable: true, blank: true
        txMetodoAproximacao nullable: true, blank:true
        txDescricaoMetodoAproximacao nullable: true, blank: true
        txTaxonOriginalCitado nullable: true, blank: true
        txPresencaAtual nullable: true, blank: true
        txTipoRegistro nullable: true, blank: true
        txDia nullable: true, blank: true
        txMes nullable: true, blank: true
        txAno nullable: true, blank: true
        txHora nullable: true, blank: true
        txDataAnoDesconhecidos nullable: true, blank: true
        txLocalidade nullable: true, blank: true
        txCaracteristicaLocalidade nullable: true, blank: true
        txUcFederal nullable: true, blank: true
        txUcEstadual nullable: true, blank: true
        txRppn nullable: true, blank: true
        txPais nullable: true, blank: true
        txEstado nullable: true, blank: true
        txMunicipio nullable: true, blank: true
        txAmbiente nullable: true, blank: true
        txHabitat nullable: true, blank: true
        txElevacaoMinima nullable: true, blank: true
        txElevacaoMaxima nullable: true, blank: true
        txProfundidadeMinima nullable: true, blank: true
        txProfundidadeMaxima nullable: true, blank: true
        txIdentificador nullable: true, blank: true
        txDataIdentificacao nullable: true, blank: true
        txIndicacaoIncerta nullable: true, blank: true
        txTombamento nullable: true, blank: true
        txInstituicaoTombamento nullable: true, blank: true
        txCompilador nullable: true, blank: true
        txDataCompilacao nullable: true, blank: true
        txRevisor nullable: true, blank: true
        txDataRevisao nullable: true, blank: true
        txValidador nullable: true, blank: true
        txDataValidacao nullable: true, blank: true
        txUsadoAvaliacao nullable: true, blank: true
        txJustificativaNaoUso nullable: true, blank: true
        idOrigem nullable:true,blank:true
        idRefBibSalve nullable:true,blank:true
        deDoi nullable:true,blank:true
        txTituloRefBib nullable:true,blank:true
        txAutorRefBib nullable:true,blank:true
        txAnoRefBib nullable:true,blank:true
        txComunicacaoPessoal nullable:true,blank:true
        txObservacao nullable:true,blank:true
    }
    static mapping = {
        table       name: 'salve.planilha_ocorrencia'
        id          column: 'sq_planilha_linha'
        ocorrencia  column: 'sq_ficha_ocorrencia'
    }
}
