package br.gov.icmbio

class IucnHabitatApoio {

    IucnHabitat iucnHabitat
    DadosApoio habitat
    Date dtInclusao
    Date dtAlteracao

    static constraints = {
    }

    static mapping = {
        version				false
        table				name            : 'salve.iucn_habitat_apoio'
        id          		column          : 'sq_iucn_habitat_apoio',generator:'identity'
        iucnHabitat          column          : 'sq_iucn_habitat'
        habitat              column          : 'sq_dados_apoio'
    }

    def beforeValidate() {
        if( ! this.dtInclusao ) {
            dtInclusao = new Date()
        }
        this.dtAlteracao = new Date()
    }

}
