package br.gov.icmbio
import grails.converters.JSON;


class FichaMudancaCategoria {

	Ficha ficha
	DadosApoio motivoMudanca
	String txFichaMudancaCategoria

    static constraints = {
    	txFichaMudancaCategoria nullable:true,blank:true
    }

        static mapping = {
	 	version 	false
	   	table       name 	: 'salve.ficha_mudanca_categoria'
	   	id          column 	: 'sq_ficha_mudanca_categoria',generator:'identity'
	   	ficha 		column 	: 'sq_ficha'
	   	motivoMudanca column: 'sq_motivo_mudanca'
 	}

    public asJson()
    {
    	Map data = [:]
	    data.put( 'sqFichaMudancaCategoria'		,this.id)
        data.put( 'sqMotivoMudanca'       		 ,this.motivoMudanca.id)
        data.put( 'txFichaMudancaCategoria'		,this.txFichaMudancaCategoria)
        return data as JSON
    }


}