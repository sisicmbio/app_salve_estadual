package br.gov.icmbio

class CicloConsultaPessoaFicha {

    transient deleted = false

    static belongsTo = [cicloConsultaPessoa:CicloConsultaPessoa, cicloConsultaFicha:CicloConsultaFicha]

    static constraints = {}

    static mapping = {
        version 	false
        table               name  : 'salve.ciclo_consulta_pessoa_ficha'
        id                  column: 'sq_ciclo_consulta_pessoa_ficha', generator:'identity'
        cicloConsultaPessoa column: 'sq_ciclo_consulta_pessoa'
        cicloConsultaFicha  column: 'sq_ciclo_consulta_ficha'
    }
}
