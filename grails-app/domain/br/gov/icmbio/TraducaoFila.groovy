package br.gov.icmbio

// Alteracoes integração com SIS/IUCN
class TraducaoFila {

    Integer nuPercentual
    Boolean stExecutando
    Date dtInclusao
    Date dtAlteracao

    static belongsTo = [ficha: Ficha]

    static constraints = {
        nuPercentual nullable: false
        stExecutando nullable: false
    }

    static mapping = {
        version 	  false
        sort        dtInclusao: 'asc'
        table       name:'salve.traducao_fila'
        id          column:'sq_traducao_fila',generator:'identity'
        ficha       column:'sq_ficha'
    }

    def beforeValidate() {
        dtAlteracao = new Date()
        if( ! dtInclusao ) {
            dtInclusao = new Date()
        }
        if( nuPercentual == null ) {
            nuPercentual = 0
        }
        if( stExecutando == null ){
            stExecutando = false
        }
    }
}
