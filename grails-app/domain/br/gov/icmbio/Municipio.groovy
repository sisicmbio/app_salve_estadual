package br.gov.icmbio

import com.vividsolutions.jts.geom.Geometry
import org.hibernate.spatial.GeometryType

class Municipio {

	Estado estado
	String noMunicipio
	Integer coIbge
	Date dtInclusao
	Date dtAlteracao
	Geometry geometry

    static constraints = {
		geometry nullable: true
		dtInclusao nullable: true
		dtAlteracao nullable: true
	}

    static mapping = {
    	version false
    	sort        noMunicipio: 'asc'
     	table       name:'salve.municipio'
     	id          column:'sq_municipio',generator:'identity'
     	estado	    column:'sq_estado'
		geometry    column: 'ge_municipio'
		geometry    type: GeometryType, sqlType: "GEOMETRY"
	}

	def beforeValidate() {
		if ( ! this.dtInclusao ) {
			this.dtInclusao = new Date()
		}
		this.dtAlteracao = new Date()
	}

	Map asMap(){
		return [sqMunicipio	: id,
				noMunicipio	: noMunicipio,
				noEstado 	: estado ? estado.noEstado : '',
				sqEstado 	: estado ? estado.id : '',
				coIbge		: coIbge ?: ''
		]
	}
}
