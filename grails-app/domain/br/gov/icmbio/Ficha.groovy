package br.gov.icmbio

import grails.converters.JSON
import org.codehaus.groovy.grails.web.json.JSONObject
import org.springframework.web.context.request.RequestContextHolder


class Ficha {
    //private transient updatePercentual = true
    //static transient dadosApoioService
    static transient fichaService, sqlService
    private transient dadosUltimaAvaliacaoNacional = [:]
    //private transient listaPendenciasPreenchimento = [ lastDate:null, lista:[] ] // lista de campos obrigatórios não preenchidos

    Unidade unidade
    CicloAvaliacao cicloAvaliacao
    Ficha fichaCicloAnterior
    Taxon taxon
    PessoaFisica usuario
    PessoaFisica alteradoPor
    PessoaFisica publicadoPor
    Date dtPublicacao
    Date dtAlteracao

    DadosApoio situacaoFicha
    DadosApoio grupoSalve
    DadosApoio grupo
    DadosApoio subgrupo
    DadosApoio posicaoTrofica
    DadosApoio modoReproducao
    DadosApoio sistemaAcasalamento
    DadosApoio unidIntervaloNascimento
    DadosApoio unidTempoGestacao
    DadosApoio unidMaturidadeSexualFemea
    DadosApoio unidMaturidadeSexualMacho
    DadosApoio unidadePesoFemea
    DadosApoio unidadePesoMacho
    DadosApoio medidaComprimentoFemea
    DadosApoio medidaComprimentoMacho
    DadosApoio medidaComprimentoFemeaMax
    DadosApoio medidaComprimentoMachoMax
    DadosApoio unidadeSenilidRepFemea
    DadosApoio unidadeSenilidRepMacho
    DadosApoio unidadeLongevidadeFemea
    DadosApoio unidadeLongevidadeMacho
    DadosApoio tendenciaPopulacional
    DadosApoio periodoTaxaCrescPopul
    DadosApoio tendenciaEoo
    DadosApoio tendenciaAoo
    DadosApoio medidaTempoGeracional
    DadosApoio tipoReduPopuPassRev
    DadosApoio tipoReduPopuPass
    DadosApoio tipoReduPopuFutura
    DadosApoio tipoReduPopuPassFutura
    DadosApoio declinioPopulacional
    DadosApoio percDeclinioPopulacional
    DadosApoio tendenNumIndividuoMaduro
    DadosApoio tendenciaNumSubpopulacao
    DadosApoio tendenNumLocalizacoes
    DadosApoio tendenQualidadeHabitat
    DadosApoio probExtincaoBrasil
    DadosApoio tipoConectividade
    DadosApoio tendenciaImigracao
    DadosApoio padraoDeslocamento
    DadosApoio categoriaIucn
    DadosApoio categoriaIucnSugestao
    DadosApoio categoriaFinal

    String nmCientifico
    String dsNotasTaxonomicas
    String dsDiagnosticoMorfologico
    String stEndemicaBrasil
    String dsDistribuicaoGeoGlobal
    String dsDistribuicaoGeoNacional
    Integer nuMaxAltitude
    Integer nuMinAltitude
    Integer nuMinBatimetria
    Integer nuMaxBatimetria
    String dsHistoriaNatural
    String stHabitoAlimentEspecialista
    String dsHabitoAlimentar
    String stRestritoHabitatPrimario
    String stEspecialistaMicroHabitat
    String dsEspecialistaMicroHabitat
    String stVariacaoSazonalHabitat
    String stDiferMachoFemeaHabitat
    String dsUsoHabitat
    String dsInteracao
    Double vlIntervaloNascimento
    String vlTempoGestacao
    String vlTamanhoProle
    Double vlMaturidadeSexualFemea
    Double vlMaturidadeSexualMacho
    Double vlPesoFemea
    Double vlPesoMacho
    Double vlComprimentoFemea
    Double vlComprimentoMacho
    Double vlComprimentoFemeaMax
    Double vlComprimentoMachoMax
    Double vlSenilidadeReprodutivaFemea
    Double vlSenilidadeReprodutivaMacho
    Double vlLongevidadeFemea
    Double vlLongevidadeMacho
    String dsReproducao
    String dsRazaoSexual
    String dsTaxaMortalidade
    Integer vlTaxaCrescPopulacional
    String dsCaracteristicaGenetica
    String dsPopulacao
    String dsAmeaca
    String dsUso
    String stPresencaListaVigente
    String dsAcaoConservacao
    String dsPresencaUc
    String dsPesquisaExistNecessaria
    String stOcorrenciaMarginal
    String stElegivelAvaliacao
    String stLimitacaoTaxonomicaAval
    Double vlEoo
    String stFlutuacaoEoo
    Double vlAoo
    String stFlutuacaoAoo
    String dsJustificativaAooEoo
    String dsJustificativaEoo
    String dsJustificativaAoo
    String stLocalidadeTipoConhecida
    String stRegiaoBemAmostrada
    Double vlTempoGeracional
    String dsMetodCalcTempoGeracional
    Integer nuReducaoPopulPassadaRev
    Integer nuReducaoPopulPassada
    Integer nuReducaoPopulFutura
    Integer nuReducaoPopulPassFutura
    String stPopulacaoFragmentada
    Integer nuIndividuoMaduro
    String stFlutExtremaIndivMaduro
    Integer nuSubpopulacao
    String stFlutExtremaSubPopulacao
    Integer nuIndiviSubpopulacaoMax
    Integer nuIndiviSubpopulacaoPerc
    Integer nuLocalizacoes
    String stFlutuacaoNumLocalizacoes
    String stAmeacaFutura
    String dsMetodoCalcPercExtinBr
    String stDeclinioBrPopulExterior
    String dsConectividadePopExterior
    String dsCriterioAvalIucn
    String dsJustificativa
    String dsJustMudancaCategoria
    String dsPendencias
    String stCategoriaOk
    String stCriterioAvalIucnOk
    String dsCriterioAvalIucnSugerido
    String dsJustificativaValidacao
    String dsCriterioAvalIucnFinal
    String dsJustificativaFinal
    String stPossivelmenteExtintaFinal
    Date dtAceiteValidacao
    String dsIssn
    String dsDoi
    String dsCitacao
    String stMigratoria
    String stPossivelmenteExtinta
    String stFavorecidoConversaoHabitats
    String stTemRegistroAreasAmplas
    String stPossuiAmplaDistGeografica
    String stFrequenteInventarioEoo
    String dsDiferencaMachoFemea
    Integer vlPercentualPreenchimento
    Boolean stManterLc
    Boolean stTransferida
    String dsHistoricoAvaliacao
    String stLocalidadeDesconhecida
    String dsEquipeTecnica
    String dsColaboradores
    String stSemUf

    static hasMany = [pendencias: FichaPendencia]

    static constraints = {
        unidade nullable: false
        cicloAvaliacao nullable: false
        fichaCicloAnterior nullable: true
        taxon nullable: false
        //taxonomia                   	nullable:true
        usuario nullable: false

        alteradoPor nullable: true
        dtAlteracao nullable: true
        publicadoPor nullable: true
        dtPublicacao nullable: true

        situacaoFicha nullable: false
        grupoSalve nullable: true
        grupo nullable: true
        subgrupo nullable: true
        nmCientifico nullable: false, blank: false
        dsNotasTaxonomicas nullable: true
        dsDiagnosticoMorfologico nullable: true
        stEndemicaBrasil nullable: true, maxSize: 1, inList: ['S', 'N', 'D']
        dsDistribuicaoGeoNacional nullable: true
        dsDistribuicaoGeoGlobal nullable: true
        nuMinAltitude nullable: true
        nuMaxAltitude nullable: true
        nuMinBatimetria nullable: true
        nuMaxBatimetria nullable: true
        dsHistoriaNatural nullable: true
        posicaoTrofica nullable: true
        stHabitoAlimentEspecialista nullable: true, maxSize: 1, inList: ['S', 'N', 'D']
        dsHabitoAlimentar nullable: true
        stRestritoHabitatPrimario nullable: true, maxSize: 1, inList: ['S', 'N', 'D']
        stEspecialistaMicroHabitat nullable: true, maxSize: 1, inList: ['S', 'N', 'D']
        dsEspecialistaMicroHabitat nullable: true
        stVariacaoSazonalHabitat nullable: true, maxSize: 1, inList: ['S', 'N', 'D']
        stDiferMachoFemeaHabitat nullable: true, maxSize: 1, inList: ['S', 'N', 'D']
        dsDiferencaMachoFemea nullable: true
        dsUsoHabitat nullable: true
        dsInteracao nullable: true
        modoReproducao nullable: true
        sistemaAcasalamento nullable: true
        vlIntervaloNascimento nullable: true
        unidIntervaloNascimento nullable: true
        vlTempoGestacao nullable: true
        unidTempoGestacao nullable: true
        vlTamanhoProle nullable: true
        vlMaturidadeSexualFemea nullable: true
        unidMaturidadeSexualFemea nullable: true
        vlMaturidadeSexualMacho nullable: true
        unidMaturidadeSexualMacho nullable: true
        vlPesoFemea nullable: true
        unidadePesoFemea nullable: true
        vlPesoMacho nullable: true
        unidadePesoMacho nullable: true
        vlComprimentoFemea nullable: true
        medidaComprimentoFemea nullable: true
        vlComprimentoMacho nullable: true
        medidaComprimentoMacho nullable: true
        vlComprimentoFemeaMax nullable: true
        medidaComprimentoFemeaMax nullable: true
        vlComprimentoMachoMax nullable: true
        medidaComprimentoMachoMax nullable: true
        vlSenilidadeReprodutivaFemea nullable: true
        unidadeSenilidRepFemea nullable: true
        vlSenilidadeReprodutivaMacho nullable: true
        unidadeSenilidRepMacho nullable: true
        vlLongevidadeFemea nullable: true
        unidadeLongevidadeFemea nullable: true
        vlLongevidadeMacho nullable: true
        unidadeLongevidadeMacho nullable: true
        dsReproducao nullable: true
        tendenciaPopulacional nullable: true
        dsRazaoSexual nullable: true
        dsTaxaMortalidade nullable: true
        vlTaxaCrescPopulacional nullable: true
        periodoTaxaCrescPopul nullable: true
        dsCaracteristicaGenetica nullable: true
        dsPopulacao nullable: true
        dsAmeaca nullable: true
        dsUso nullable: true
        stPresencaListaVigente nullable: true
        dsAcaoConservacao nullable: true
        dsPresencaUc nullable: true
        dsPesquisaExistNecessaria nullable: true
        stOcorrenciaMarginal nullable: true
        stElegivelAvaliacao nullable: true
        stLimitacaoTaxonomicaAval nullable: true
        vlEoo nullable: true
        tendenciaEoo nullable: true
        stFlutuacaoEoo nullable: true
        vlAoo nullable: true
        tendenciaAoo nullable: true
        stFlutuacaoAoo nullable: true
        dsJustificativaAooEoo nullable: true
        dsJustificativaEoo nullable: true
        dsJustificativaAoo nullable: true
        stLocalidadeTipoConhecida nullable: true
        stRegiaoBemAmostrada nullable: true
        vlTempoGeracional nullable: true
        medidaTempoGeracional nullable: true
        dsMetodCalcTempoGeracional nullable: true
        nuReducaoPopulPassadaRev nullable: true
        tipoReduPopuPassRev nullable: true
        nuReducaoPopulPassada nullable: true
        tipoReduPopuPass nullable: true
        nuReducaoPopulFutura nullable: true
        tipoReduPopuFutura nullable: true
        nuReducaoPopulPassFutura nullable: true
        tipoReduPopuPassFutura nullable: true
        declinioPopulacional nullable: true
        stPopulacaoFragmentada nullable: true
        nuIndividuoMaduro nullable: true
        tendenNumIndividuoMaduro nullable: true
        stFlutExtremaIndivMaduro nullable: true
        stMigratoria nullable: true, maxSize: 1, inList: ['S', 'N', 'D']
        percDeclinioPopulacional nullable: true
        nuSubpopulacao nullable: true
        tendenciaNumSubpopulacao nullable: true
        stFlutExtremaSubPopulacao nullable: true
        nuIndiviSubpopulacaoMax nullable: true
        nuIndiviSubpopulacaoPerc nullable: true
        tendenNumLocalizacoes nullable: true
        nuLocalizacoes nullable: true
        stFlutuacaoNumLocalizacoes nullable: true
        tendenQualidadeHabitat nullable: true
        stAmeacaFutura nullable: true
        probExtincaoBrasil nullable: true
        dsMetodoCalcPercExtinBr nullable: true
        tipoConectividade nullable: true
        tendenciaImigracao nullable: true
        stDeclinioBrPopulExterior nullable: true
        dsConectividadePopExterior nullable: true
        dsCriterioAvalIucn nullable: true
        dsJustificativa nullable: true
        dsJustMudancaCategoria nullable: true
        dsPendencias nullable: true
        categoriaIucn nullable: true
        stCategoriaOk nullable: true
        stCriterioAvalIucnOk nullable: true
        dsCriterioAvalIucnSugerido nullable: true
        dsJustificativaValidacao nullable: true
        categoriaIucnSugestao nullable: true
        categoriaFinal nullable: true
        dsCriterioAvalIucnFinal nullable: true
        dsJustificativaFinal nullable: true
        dsIssn nullable: true
        dsDoi nullable: true
        dsCitacao nullable: true
        stPossivelmenteExtinta nullable: true, maxSize: 1, inList: ['S', 'N']
        stPossivelmenteExtintaFinal nullable: true, maxSize: 1, inList: ['S', 'N']
        stFavorecidoConversaoHabitats nullable: true, maxSize: 1, inList: ['S', 'N', 'D']
        stTemRegistroAreasAmplas nullable: true, maxSize: 1, inList: ['S', 'N', 'D']
        stPossuiAmplaDistGeografica nullable: true, maxSize: 1, inList: ['S', 'N', 'D']
        stFrequenteInventarioEoo nullable: true, maxSize: 1, inList: ['S', 'N', 'D']
        padraoDeslocamento nullable: true
        dtAceiteValidacao nullable: true
        vlPercentualPreenchimento nullable: true
        stManterLc nullable: true
        stTransferida nullable: true
        dsHistoricoAvaliacao nullable: true
        stLocalidadeDesconhecida nullable: true, maxSize: 1, inList: ['S', 'N']
        dsEquipeTecnica nullable: true
        dsColaboradores nullable: true
        stSemUf nullable: true, inList: ['S', 'N']
    }

    static mapping = {
        version false
        //cache: true
        table name: 'salve.ficha'
        id column: 'sq_ficha', generator: 'identity'
        unidade column: 'sq_unidade_org'
        cicloAvaliacao column: 'sq_ciclo_avaliacao'
        situacaoFicha column: 'sq_situacao_ficha'
        taxon column: 'sq_taxon'
        fichaCicloAnterior column: 'sq_ficha_ciclo_anterior'
        //taxonomia 					column:'sq_taxon',insertable: false, updateable: false
        usuario column: 'sq_usuario'
        alteradoPor column: 'sq_usuario_alteracao'
        publicadoPor column: 'sq_usuario_publicacao'
        grupoSalve column: 'sq_grupo_salve'
        grupo column: 'sq_grupo'
        subgrupo column: 'sq_subgrupo'
        posicaoTrofica column: 'sq_posicao_trofica'
        modoReproducao column: 'sq_modo_reproducao'
        sistemaAcasalamento column: 'sq_sistema_acasalamento'
        unidIntervaloNascimento column: 'sq_unid_intervalo_nascimento'
        unidTempoGestacao column: 'sq_unid_tempo_gestacao'
        unidMaturidadeSexualFemea column: 'sq_unid_maturidade_sexual_femea'
        unidMaturidadeSexualMacho column: 'sq_unid_maturidade_sexual_macho'
        unidadePesoFemea column: 'sq_unidade_peso_femea'
        unidadePesoMacho column: 'sq_unidade_peso_macho'
        medidaComprimentoFemea column: 'sq_medida_comprimento_femea'
        medidaComprimentoMacho column: 'sq_medida_comprimento_macho'
        medidaComprimentoFemeaMax column: 'sq_medida_comprimento_femea_max'
        medidaComprimentoMachoMax column: 'sq_medida_comprimento_macho_max'
        unidadeSenilidRepFemea column: 'sq_unidade_senilid_rep_femea'
        unidadeSenilidRepMacho column: 'sq_unidade_senilid_rep_macho'
        unidadeLongevidadeFemea column: 'sq_unidade_longevidade_femea'
        unidadeLongevidadeMacho column: 'sq_unidade_longevidade_macho'
        tendenciaPopulacional column: 'sq_tendencia_populacional'
        periodoTaxaCrescPopul column: 'sq_periodo_taxa_cresc_popul'
        tendenciaEoo column: 'sq_tendencia_eoo'
        tendenciaAoo column: 'sq_tendencia_aoo'
        medidaTempoGeracional column: 'sq_medida_tempo_geracional'
        tipoReduPopuPassRev column: 'sq_tipo_redu_popu_pass_rev'
        tipoReduPopuPass column: 'sq_tipo_redu_popu_pass'
        tipoReduPopuFutura column: 'sq_tipo_redu_popu_futura'
        tipoReduPopuPassFutura column: 'sq_tipo_redu_popu_pass_futura'
        declinioPopulacional column: 'sq_declinio_populacional'
        percDeclinioPopulacional column: 'sq_perc_declinio_populacional'
        tendenNumIndividuoMaduro column: 'sq_tenden_num_individuo_maduro'
        tendenciaNumSubpopulacao column: 'sq_tendencia_num_subpopulacao'
        tendenNumLocalizacoes column: 'sq_tenden_num_localizacoes'
        tendenQualidadeHabitat column: 'sq_tenden_qualidade_habitat'
        probExtincaoBrasil column: 'sq_prob_extincao_brasil'
        tipoConectividade column: 'sq_tipo_conectividade'
        tendenciaImigracao column: 'sq_tendencia_imigracao'
        categoriaIucn column: 'sq_categoria_iucn'
        categoriaIucnSugestao column: 'sq_categoria_iucn_sugestao'
        categoriaFinal column: 'sq_categoria_final'
        padraoDeslocamento column: 'sq_padrao_deslocamento'
        stSemUf nullable: true, inList: ['S', 'N']
    }

    def beforeDelete() {
        FichaOcorrencia.withNewSession {
            FichaOcorrencia.findAllByFicha(this).each
                {
                    it.delete(flush: true)
                }
        }
        FichaRefBib.withNewSession {
            FichaRefBib.findAllByFicha(this).each
                {
                    it.delete(flush: true)
                }
        }
    }

    /*def beforeInsert() {
    }
    */

    def beforeUpdate() {
        // cancelar a publicação se a situação da ficha for alterada de PUBLICADA
        if (dtPublicacao && situacaoFicha.codigoSistema != 'PUBLICADA') {
            dtPublicacao = null
            publicadoPor = null
        }

        if( ! stSemUf ) {
            stSemUf = 'N'
        }
        /*if( ! fichaAnterior )
		{
			Ficha.createCriteria().list {
				cicloAvaliacao {
					lt('nuAno', it.cicloAvaliacao.nuAno )
					order('nuAno','desc')
				}
				taxon {
					eq('id',it.taxon.id)
				}
				maxResults(1)
			}.each {
				this.fichaAnterior = it
			}
		}
		*/
        /*if( situacaoFicha.codigoSistema == 'COMPILACAO') {
			dtAceiteValidacao = null
			stManterLc = null
			stTransferida = null
			categoriaIucnSugestao = null
			categoriaIucn = null
			dsCriterioAvalIucn = null
			dsJustificativa = null
			categoriaFinal = null
			dsCriterioAvalIucnFinal = null
			dsJustificativaFinal = null
		}
		*/
    }

    def beforeValidate() {

        if (vlPercentualPreenchimento == null) {
            vlPercentualPreenchimento = 0
        }

        if (!this.fichaCicloAnterior) {
            this.fichaCicloAnterior = fichaAnterior
        }

        if( ! stSemUf ) {
            stSemUf = 'N'
        }
        /*
		if( this.grupo )
		{
			if( this.grupo.codigoSistema == 'PEIXES_AGUA_DOCE' || this.grupo.codigoSistema == 'AVES')
			{
				this.nuMinBatimetria=null
				this.nuMaxBatimetria=null
			}
			else if( this.grupo.codigoSistema.indexOf('MARINHO') == -1
				&& this.grupo.codigoSistema.indexOf('TERRESTRE') == -1
                    && this.grupo.codigoSistema != 'MAMIFEROS'
                    && this.grupo.codigoSistema != 'REPTEIS' )
			{
				this.nuMinBatimetria=null
                this.nuMaxBatimetria=null
                this.nuMinAltitude=null
                this.nuMaxAltitude=null
            }
			else if( this.grupo.codigoSistema.indexOf('MARINHO') > 0 )
			{
				this.nuMinAltitude=null
                this.nuMaxAltitude=null
            }

		}
		*/
        if (this.stMigratoria != 'S') {
            this.padraoDeslocamento = null;
        }
        // gravar o nome cientifico
        this.nmCientifico = this.taxon.noCientifico
        if (this?.taxon?.noCientificoCompleto && this.nmCientifico != this?.taxon?.noCientificoCompleto) {
            this.nmCientifico = this?.taxon?.noCientificoCompleto
        }
        // atualizar usuário alterou e data da alteração
        updateLog()
    }

    String getNoCientifico() {
        return nmCientifico
    }

    String getNoCientificoItalico() {
        return taxon.noCientificoItalico
        /*
        List aPartes = nmCientifico.split( ' ')
        if( aPartes.size() > 1 )
        {
            //return '<i>' + aPartes[0]+' '+aPartes[1]+'</i> ' + aPartes.minus([aPartes[0],aPartes[1]]).join( ' ').trim()
            return '<i>' + aPartes[0]+' '+aPartes[1]+'</i> ' + aPartes.drop(2).join(' ').trim()
        }
        return '<i>' + nmCientifico.toString().trim() + '</i>'
        */
    }

    Integer getQtdPendencia() {
        return FichaPendencia.countByFichaAndStPendente(this, 'S')
    }

    boolean canModify(Sicae user = null ) {
        // se a ficha ainda não existir retornar true
        if ( ! id ) {
            return true
        }

        if( !user || ! user?.sqPerfil ){
            return false
        }

        // usuário consulta
        if( user.isCO()  ) {
            return false
        }

        // Administrador e colaborador externo podem porque independem da unidade de lotação
        if( user.isADM() || user.isCE() ) {
            return true
        }

        // quando for coordenador de taxon EXTERNO não tem unidade organizacional
        boolean resultado = (unidade?.id == user?.sqUnidadeOrg )
        if( user.isCT() && !user?.sqUnidadeOrg )
        {
            DadosApoio pai = DadosApoio.findByCodigoSistema('TB_PAPEL_FICHA')
            DadosApoio ct = DadosApoio.findByPaiAndCodigoSistema(pai,'COORDENADOR_TAXON')
            if( ct ) {
                resultado = ( FichaPessoa.createCriteria().count {
                    eq('vwFicha.id', sqFicha)
                    eq('pessoa.id', user.sqPessoa.toLong())
                    eq('inAtivo', 'S')
                    eq('papel.id', ct.id)
                } > 0 )
            }
        }
        return resultado
    }

    /**
     * Encontrar a ficha correspondente no ciclo anterior
     * @return
     */
    Ficha getFichaAnterior() {
        if (this.fichaCicloAnterior) {
            return fichaCicloAnterior
        }
        // recupera o ciclo anterior
        List ciclos = CicloAvaliacao.where {
            nuAno < this.cicloAvaliacao.nuAno
        }.list(sort: 'nuAno', order: 'desc', max: 1)
        if (ciclos) {
            return Ficha.findByTaxonAndCicloAvaliacao(this.taxon, ciclos[0])
        }
        return null
    }

    String getAutoresHtml() {
        List resultado = []
        FichaAutor.findAllByFicha(this).each {
            resultado.add(it.autor.codigo)
        }
        return resultado.join('; ')
    }

    List getSubespecies() {
        if (this.taxon) {
            return Taxon.findAllByPai(this.taxon)
        }
        return []
    }

    List getFichasSubespecie() {
        if (this.taxon) {
            return executeQuery("""select new map(f.id as sqFicha, f.nmCientifico as nmCientifico, taxon.id as sqTaxon) from Ficha f where f.taxon.pai = :taxon""", [taxon: taxon])
        }
        return []
    }

    //-----------------------------------------
    String getCategoriaIucnText() {
        if (this.categoriaIucn) {
            return this.categoriaIucn.descricao + ' (' + this.categoriaIucn.codigo + ')'
        }
        return ''
    }

    String getCategoriaUltimaAvaliacaoNacionalText() {
        if (!dadosUltimaAvaliacaoNacional) {
            dadosUltimaAvaliacaoNacional = getUltimaAvaliacaoNacional()
        }
        return dadosUltimaAvaliacaoNacional.deCategoriaIucn
    }

    String getCodigoCategoriaUltimaAvaliacaoNacionalText() {
        if (!dadosUltimaAvaliacaoNacional) {
            dadosUltimaAvaliacaoNacional = getUltimaAvaliacaoNacional()
        }
        return dadosUltimaAvaliacaoNacional.coCategoriaIucn
    }

    String getCriterioUltimaAvaliacaoNacionalText() {
        if (!dadosUltimaAvaliacaoNacional) {
            dadosUltimaAvaliacaoNacional = getUltimaAvaliacaoNacional()
        }
        return dadosUltimaAvaliacaoNacional.deCriterioAvaliacaoIucn
    }

    /**
     * recuperar a utltima avaliacao nacional da tabela taxon_historico_avaliacao
     * @return
     */
    Map getUltimaAvaliacaoNacionalHist() {
        if (!fichaService) {
            fichaService = new FichaService()
        }
        return fichaService.getUltimaAvaliacaoNacionalHist(this.id)
    }

    /**
     * recupera a ultima avaliacao nacional da tabela taxon_historico_avaliacao ou da aba 11.7 - Resultado da Validação
     * @return
     */
    Map getUltimaAvaliacaoNacional() {
        //if( !dadosUltimaAvaliacaoNacional )
        //{
        if (!fichaService) {
            //println 'Instanciada FichaService em Ficha.groovy'
            fichaService = new FichaService()
        }
        dadosUltimaAvaliacaoNacional = fichaService.getUltimaAvaliacaoNacional(this.id)
        //}
        return dadosUltimaAvaliacaoNacional
    }

    // campos da colaboração
    /*
	* Ler as colaborações feitas em um determinado campo da this.
	* A situação deve ser vazia ou de acordo com codigo do sistema da tabela de apoio TB_SITUCAO_COLABORACAO
	* Ex: "NAO_AVALIADA" "ACEITA" "NAO_ACEITA" "PARCIAL" "RESOLVER_OFICINA"
	*/

    List getColaboracoes(String campo, String situacao = '', Long sqFichaVersao = null) {
        return fichaService.getColaboracoes(this, campo, situacao,sqFichaVersao)
    }

    /**
     * listar os pontos recebidos durante aa consultas amplas e diretas
     * @return
     */
    List getColaboracoesOcorrencia(String situacao = '') {
        return fichaService.getColaboracoesOcorrencia(this, situacao)
    }

    /**
     * Ler os colaboradores
     * @param campo
     * @param situacao
     * @return
     */
    String getColaboradoresHtml() {
        return fichaService.getColaboradoresHtml(this.id)
    }

    String getSinonimias() {
        return fichaService.getFichaSinonimias(this)
    }

    String getSinonimiasHtml() {
        return fichaService.getFichaSinonimiasHtml(this)
    }

    String getPrimeiroNomeComum() {
        String nomesComuns = fichaService.getFichaNomesComunsText(this)
        if (nomesComuns) {
            return nomesComuns.split(', ')[0]
        }
        return ''
    }

    String getNomesComuns() {
        return fichaService.getFichaNomesComunsText(this.id)
    }

    String getEstados() {
        return fichaService.getUfsText(this)
    }

    String getEstadosPorRegiao() {
        return fichaService.getUfsNameByRegiaoText(this)
    }

    String getBiomas() {
        return fichaService.getBiomasText(this)
    }

    String getBacias() {
        return fichaService.getBaciasText(this)
    }

    String getAreasRelevantes() {
        return fichaService.getAreasRelevantesText(this)
    }

    List getAreasRelevantesList() {
        return fichaService.getAreasRelevantesList(this)
    }

    Map getUcs() {
        return fichaService.getUcsFromOcorrencia(this?.id)
    }

    String getEndemicaBrasil() {
        switch (stEndemicaBrasil) {
            case 'S': return 'Sim'; break
            case 'N': return 'Não'; break
            case 'D': return 'Desconhecido'; break
            default:
                return ''
        }
    }

    String getEspecieMigratoria() {
        switch (stMigratoria) {
            case 'S': return 'Sim'; break
            case 'N': return 'Não'; break
            case 'D': return 'Desconhecido'; break
            default:
                return ''
        }
    }

    String getImagemPrincipalDistribuicao() {
        return fichaService.getFichaDistribuicaoMapaPrincipal(this)
    }

    String snd(String value) {
        switch (value) {
            case 'S': return 'Sim'; break
            case 'N': return 'Não'; break
            case 'D': return 'Desconhecido'; break
            default:
                return ''
        }
    }

    String getIntervaloNascimentoText() {
        return (this?.vlIntervaloNascimento ? this.vlIntervaloNascimento.toString().replaceAll(/\.0$/, '') + ' ' + this?.unidIntervaloNascimento?.descricao : '')
    }

    String getTempoGestacaoText() {
        return (this.vlTempoGestacao ? this.vlTempoGestacao.toString().replaceAll(/\.0$/, '') + ' ' + this?.unidTempoGestacao?.descricao : '')
    }

    String getTamanhoProleText() {
        return (this.vlTamanhoProle ? this.vlTamanhoProle.toString().replaceAll(/\.0$/, '') + ' individuo(s)' : '')
    }

    // verificar se pode alterar a categoria em função da avaliação expedita
    boolean isCategoriaEditable() {
        Boolean flagEditCategoria = true
        if (this.stFavorecidoConversaoHabitats == 'S' || this.stTemRegistroAreasAmplas == 'S' || (this.stPossuiAmplaDistGeografica == 'S' && this.stFrequenteInventarioEoo == 'S')) {
            flagEditCategoria = false
        } else if (this.stFavorecidoConversaoHabitats == 'N' || this.stTemRegistroAreasAmplas == 'D' || (this.stPossuiAmplaDistGeografica == 'D' && this.stFrequenteInventarioEoo == 'N')) {
            flagEditCategoria = false
        } else if (this.stFavorecidoConversaoHabitats == 'N' || this.stTemRegistroAreasAmplas == 'D' || (this.stPossuiAmplaDistGeografica == 'D' && this.stFrequenteInventarioEoo == 'D')) {
            flagEditCategoria = false
        }
        return flagEditCategoria
    }

    // calcula a categoria em função das respostas da avaliação expedita
    DadosApoio getCategoriaAvaliacaoExpedita() {
        String p1, p2, p3, p4 // perguntas 1 a 4
        String categoria = ''
        boolean p3Visivel = false
        p1 = this.stFavorecidoConversaoHabitats
        p2 = this.stTemRegistroAreasAmplas
        p3 = this.stPossuiAmplaDistGeografica
        p4 = this.stFrequenteInventarioEoo
        if (p1 == 'S') {
            categoria = 'LC'
        } else {
            // pergunta 1 é: N, D  ou ''
            if (p1 != '') {
                if (p2 == 'S') {
                    categoria = 'LC'
                } else if (p2 == 'N') {
                    if (p1 == 'N') {
                        categoria = 'avaliar' // avaliar
                    } else {
                        // pergunta 3
                        p3Visivel = true
                    }
                } else if (p2 == 'D') {
                    p3Visivel = true
                }
            }
        }
        // pergunta 3 está visivel
        if (!p3Visivel) {
            if (p3 == 'S') {
                if (p4 == 'S') {
                    categoria = 'LC'
                } else if (p4 == 'N') {
                    categoria = 'avaliar'
                } else if (p4 == 'D') {
                    categoria = 'avaliar'
                }
            } else if (p3 == 'N') {
                if (p4 == 'D') {
                    categoria = 'DD'
                } else if (p4 == 'N') {
                    categoria = 'avaliar'
                } else if (p4 == 'S') {
                    categoria = 'avaliar'
                }

            } else if (p3 == 'D') {
                if (p4 == 'N') {
                    categoria = 'DD'
                } else if (p4 == 'D') {
                    categoria = 'DD'
                } else if (p4 == 'S') {
                    categoria = 'avaliar'
                }
            }
        }
        if (categoria != '' && categoria != 'avaliar') {
            return fichaService.getDadosApoioByCodigo('TB_CATEGORIA_IUCN', categoria)
        }
        return null
    }

    String getAlteradoPorHtml() {
        if (!alteradoPor || !dtAlteracao) {
            return ''
        }
        List nomes = alteradoPor.noPessoa.split(' ')
        return nomes[0] + (nomes[1] ? ' ' + nomes[1] : '') + (nomes[2] ? ' ' + nomes[2].substring(0, 1) + '.' : '') + '<br/>' + dtAlteracao?.format('dd/MM/yyyy HH:mm:ss')
    }
    /**
     * Atualizar o usuário que fez a ultima alteração
     */
    void updateLog() {
        if (this.isDirty()) {
            try {
                def session = RequestContextHolder.currentRequestAttributes().session
                if (session?.sicae?.user) {
                    this.dtAlteracao = new Date()
                    this.alteradoPor = PessoaFisica.get(session.sicae.user.sqPessoa)
                }
            } catch (Exception e) {
                println e.getMessage()
            }
        }
    }


    /**
     * Alimentar o array listaPendenciasPreenchimento com as pendências padão da ficha
     */
    /*private void criarListaPendenciasPadrao() {
		listaPendenciasPreenchimento = [lastDate:null, lista:[] ]
		// verificar se tem texto distribuicao global
		if( !this.stEndemicaBrasil || !Util.trimEditor(this.dsDistribuicaoGeoGlobal )  )
		{
			listaPendenciasPreenchimento.lista.push('Campo "<b>Endêmica do Brasil?</b>" e "<b>Distribuição Geográfica Global</b>" na aba 2 devem ser preenchidos!' )
		}

		// possui mapa distribuição principal
		if( !FichaAnexo.findByFichaAndContextoAndInPrincipal(this, dadosApoioService.getByCodigo('TB_CONTEXTO_ANEXO', 'MAPA_DISTRIBUICAO'), 'S')) {
			listaPendenciasPreenchimento.lista.push('Nenhuma "<b>Imagens de MAPA ou SHAPEFILE</b>" anexado na aba 2 como principal!')
		}

		// verificar se tem UF
		if (!FichaOcorrencia.findByFichaAndContexto(this, dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA', 'ESTADO'))) {
			listaPendenciasPreenchimento.lista.push('Nenhuma "<b>UF</b>" informada na aba 2.1!')
		}

		// verificar se tem BIOMA
		if (!FichaOcorrencia.findByFichaAndBiomaIsNotNull(this)) {
			listaPendenciasPreenchimento.lista.push('Nenhum "<b>BIOMA</b>" informado na aba 2.2!')
		}

		// possui algum registro de ocorrencia
		if (!FichaOcorrencia.findByFichaAndContexto(this, dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA', 'REGISTRO_OCORRENCIA'))) {
			listaPendenciasPreenchimento.lista.push('Nenhuma "<b>OCORRÊNCIA</b>" informada na aba 2.6!')
		}

		// historia natural
		if (!Util.trimEditor(this.dsHistoriaNatural)) {
			listaPendenciasPreenchimento.lista.push('Descrição de "<b>HISTÓRIA NATUAL</b>" não informada na aba 3!')
		}

		// tendencia populacional
		if (!this.tendenciaPopulacional) {
			listaPendenciasPreenchimento.lista.push('Campo "<b>Tendência Populacional</b>" não informado na aba 4!')
		}

		// Obs sobre a população
		if (!Util.trimEditor(this.dsPopulacao)) {
			listaPendenciasPreenchimento.lista.push('Campo "<b>Observação Sobre a População</b>" não informado na aba 4!')
		}

		// Obs sobre a ameaça
		if (!Util.trimEditor(this.dsAmeaca)) {
			listaPendenciasPreenchimento.lista.push('Campo "<b>Observações Gerais Sobre as Ameaças</b>" não informado na aba 5!')
		}

		// Obs sobre o Uso
		if (!Util.trimEditor(this.dsUso)) {
			listaPendenciasPreenchimento.lista.push('Campo "<b>Observações Gerais Sobre os Usos</b>" não informado na aba 6!')
		}

		// Obs sobre presença em uc ou UC informada contam 1 ponto
		if (!Util.trimEditor(this.dsPresencaUc) && !FichaOcorrencia.createCriteria().list {
			eq('ficha', this)
			eq('contexto', dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA', 'REGISTRO_OCORRENCIA'))
			and {
				or {
					isNotNull('sqUcFederal')
					isNotNull('sqUcEstadual')
					isNotNull('sqRppn')
				}
			}
			maxResults(1)
		}) {
			listaPendenciasPreenchimento.lista.push('Nenhuma ocorrência em "<b>UC</b>" informada na aba 2.6 ou campo "<b>Observações Gerais Sobre a Presença UC</b>" foi preenchido na aba 7.4!')
		}

		// ref. bibliográfica ou comunicacao pessoal
		if (!FichaRefBib.findByFicha(this)) {
			listaPendenciasPreenchimento.lista.push('Nenhuma "<b>Referência Bibliográfica</b>" foi informada na ficha!')
		}

		// atualizar a data do processamento
		listaPendenciasPreenchimento.lastDate = new Date()
	}
	*/

    /*
	private void gravarListaPendenciasPreenchimento()
	{
		int totalCamposMonitorados = 12

		if (updatingPercentual) {
			return
		}

		if (!this.id) {
			return
		}
		Ficha.withNewSession { session ->
			Ficha ficha = Ficha.get( this.id )
			ficha.updatingPercentual = true
			ficha.updateLog()

			FichaPendencia pendencia = FichaPendencia.findByFichaAndDeAssunto(this, 'Campos obrigatórios')
			// criar a lista com as pendencias
			ficha.criarListaPendenciasPadrao()
			int qtdPendencias = ficha.listaPendenciasPreenchimento.lista.size()
			println 'Qtd pendencias: ' + qtdPendencias
			println "-" * 100
			println ' '

			if (qtdPendencias > totalCamposMonitorados) {
				qtdPendencias = totalCamposMonitorados
			}

			if (ficha.listaPendenciasPreenchimento.lista.size() == 0 && pendencia) {
				pendencia.delete()
			}else if (ficha.listaPendenciasPreenchimento.lista.size() > 0) {

				if (!pendencia) {
					pendencia = new FichaPendencia()
					pendencia.ficha = ficha//Ficha.get(this.id)
					pendencia.deAssunto = 'Campos obrigatórios'
				}
				if (pendencia.ficha) {
					pendencia.stPendente = 'S'
					pendencia.dtPendencia = new Date()
					pendencia.txPendencia = '<ol>' + ficha.listaPendenciasPreenchimento.lista.collect {
						'<li>' + it + '</li>'
					}.join('') + '</ol>'
					pendencia.usuario = ficha.alteradoPor//this.alteradoPor
					pendencia.save(flush:true)
				}
			}
			updatingPercentual = true
			ficha.vlPercentualPreenchimento = (totalCamposMonitorados - qtdPendencias) / totalCamposMonitorados * 100
			println 'Percentual ' + ficha.vlPercentualPreenchimento
			ficha.save( flush:true )
			updatingPercentual = false
		}
	}
	*/

    Integer getPercentualPreenchimento() {
        return this.vlPercentualPreenchimento ?: 0
    }

    /**
     * Recuperar os validadores e o resultado das valida valiaçao
     * @result{"Luis Teste":{"de_criterio_sugerido":"","st_possivelmente_extinta":"","cd_situacao_convite":"CONVITE_ACEITO","dt_alteracao":"","in_respondido":"N","de_resposta":"","de_categoria_sugerida":"","in_comunicar_alteracao":"N","in_convite_vencido":"S","dt_ultimo_comunicado":null}}* @param sqFicha
     * @param sqOficina
     * @return
     */
    JSONObject getValidadoresJson(Long sqOficina) {
        try {
            String cmd = "select json_validadores from salve.json_validadores( :sqFicha, :sqOficina )"
            List reg = sqlService.execSql(cmd, [sqFicha: this.id, sqOficina: sqOficina])
            return JSON.parse(reg[0].json_validadores)
        } catch (Exception e) {
            return JSON.parse('{}')
        }
    }

    /**
     * imprimir o valor e a nunidade de medida do tempo geracional
     * @return
     */
    String getTempoGeracionalText() {
        if (!vlTempoGeracional) {
            return ''
        }
        return vlTempoGeracional.toString().replaceAll(/\.0$/, '') + ' ' + medidaTempoGeracional?.descricao
    }

}
