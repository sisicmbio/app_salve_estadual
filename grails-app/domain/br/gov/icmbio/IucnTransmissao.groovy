package br.gov.icmbio

class IucnTransmissao {

    PessoaFisica pessoa
    Date dtTransmissao
    String txLog
    String noArquivoZip

    //static hasMany = [fichas : IucnTransmissaoFicha ]

    static constraints = {
        txLog nullable: true
    }

    static mapping = {
        version false
        table name: 'salve.iucn_transmissao'
        id column: 'sq_iucn_transmissao', generator: 'identity'
        pessoa column: 'sq_pessoa'
    }

    def beforeValidate() {
        if ( ! this.dtTransmissao ) {
            this.dtTransmissao = new Date()
        }
    }

}
