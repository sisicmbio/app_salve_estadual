package br.gov.icmbio

class RegistroUcEstadual {

    Registro registro
    UcEstadual ucEstadual
    Date dtInclusao

    static constraints = {
    }

    static mapping = {
        version false
        table           name    : 'salve.registro_uc_estadual'
        id              column  : 'sq_registro', generator: 'assigned'
        ucEstadual      column  : 'sq_uc_estadual'
        registro        column  : 'sq_registro', insertable: false, updateable: false
    }

    def beforeValidate() {
        if ( ! this.dtInclusao ) {
            this.dtInclusao = new Date()
        }
        this.id = registro?.id

    }

}
