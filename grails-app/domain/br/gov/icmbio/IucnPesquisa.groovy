package br.gov.icmbio

class IucnPesquisa {

    String cdIucnPesquisa
    String dsIucnPesquisa
    String dsIucnPesquisaOrdem
    Date dtInclusao
    Date dtAlteracao

    static constraints = {
    }

    static mapping = {
        version				false
        sort 				dsIucnPesquisaOrdem  : 'asc'
        table				name                : 'salve.iucn_pesquisa'
        id          		column              : 'sq_iucn_pesquisa',generator:'identity'
    }

    def beforeValidate() {
        if( ! this.dtInclusao ) {
            dtInclusao = new Date()
        }
        this.dtAlteracao = new Date()
        if( !this.dsIucnPesquisaOrdem ){
            this.dsIucnPesquisaOrdem = this.cdIucnPesquisa
        }
    }

    String getDescricaoCompleta() {
        return this.cdIucnPesquisa+' - '+this.dsIucnPesquisa
    }
}
