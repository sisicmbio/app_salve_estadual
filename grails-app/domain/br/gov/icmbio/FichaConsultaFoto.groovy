package br.gov.icmbio

import grails.converters.JSON

class FichaConsultaFoto {
    
    WebUsuario usuario
    FichaMultimidia fichaMultimidia
    String inTipoTermoResp
    Date dtAceiteTermoResp
    String nuIpComputador
    String noNavegador
    CicloConsultaFicha consultaFicha
    

    static constraints = {
    }

    static mapping = {
        version 				false
        table       			name 	: 'salve.ficha_consulta_foto'
        id          			column 	: 'sq_ficha_consulta_foto',generator:'identity'
        usuario                 column  : 'sq_web_usuario'
        fichaMultimidia	        column  : 'sq_ficha_multimidia'
        inTipoTermoResp         column  : 'in_tipo_termo_resp' 
        consultaFicha           column  : 'sq_ciclo_consulta_ficha'          
    }

    def beforeValidate() {
        if( ! this.dtAceiteTermoResp )
        {
            this.dtAceiteTermoResp = new Date()
        }
    }

    public asJson()
    {
        Map data = [:]
        data.put('sqFichaConsultaFoto'  ,this.id)        
        data.put('sqFichaMultimidia'    ,this.fichaMultimidia.id)
        data.put('txMultimidia'         ,this.fichaMultimidia.txMultimidia)
        data.put('deEmailAutor'         ,this.fichaMultimidia.deEmailAutor)
        data.put('noAutor'              ,this.fichaMultimidia.noAutor)
        data.put('inTipoTermoResp'      ,this.inTipoTermoResp)
        
        return data as JSON
    }

}
