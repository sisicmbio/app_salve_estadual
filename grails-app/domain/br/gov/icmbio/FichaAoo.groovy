package br.gov.icmbio
import com.vividsolutions.jts.geom.Geometry
import org.hibernate.spatial.GeometryType
import org.postgis.MultiPolygon


class FichaAoo {

    Ficha ficha
    Geometry geometry
    Date dtInclusao
    Date dtAlteracao

    static constraints = {
       geometry nullable:true
    }

    static mapping = {
        version 				false
        table       			name 	: 'salve.ficha_aoo'
        id          			column 	: 'sq_ficha_aoo',generator:'identity'
        ficha 					column  : 'sq_ficha'
        geometry                type: GeometryType, sqlType: "GEOMETRY"
        geometry				column 	: 'ge_aoo'
    }

    def beforeValidate() {
        if ( ! this.dtInclusao ) {
            this.dtInclusao = new Date()
        }
        this.dtAlteracao = new Date()
    }

}

