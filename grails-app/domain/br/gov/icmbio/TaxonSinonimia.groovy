package br.gov.icmbio

class TaxonSinonimia {

    Taxon taxonAceito
    Taxon taxonSinonimo
    TaxonSituacao situacao

    static constraints = {
    }

    static mapping = {
        version		false
        sort		taxonAceito: 'asc'
        table       name 	: 'taxonomia.sinonimia'
        id			column  : 'sq_sinonimia'
        taxonAceito column  : 'sq_taxon_aceito'
        taxonSinonimo column  : 'sq_taxon_sinonimo'
        situacao      column  : 'sq_situacao'
    }
}
