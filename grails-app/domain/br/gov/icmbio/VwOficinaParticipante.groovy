package br.gov.icmbio

class VwOficinaParticipante {

    Pessoa  pessoa
    WebInstituicao instituicao

    Integer sqOficina
    String  noPessoa
    Integer sqSituacao
    String  noInstituicao
    String  sgInstituicao
    String  deSituacao
    String  cdSituacaoSistema
    String  deEmail
    Date    dtEmail
    String  deAssuntoEmail
    Date    dtEmailAssinatura
    Date    dtAssinatura
    String  deHashAssinatura

    static constraints = {
    }

    static mapping = {
        //cache : true
        version false
        sort   noPessoa:  'asc'
        table  name: 'salve.vw_oficina_participante'
        id     column : 'sq_oficina_participante', insertable: false, updateable: false
        pessoa column : 'sq_pessoa', insertable: false, updateable: false
        instituicao column : 'sq_instituicao', insertable: false, updateable: false
        pessoa  lazy: true
        instituicao lazy: true
    }

}








