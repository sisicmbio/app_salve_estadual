package br.gov.icmbio

class OficinaMemoriaParticipante {

    String noParticipante
    DadosApoio papel

    static belongsTo = [oficinaMemoria: OficinaMemoria]

    static constraints = {

    }


    static mapping = {
        version false
        table       name:'salve.oficina_memoria_participante'
        id          column:'sq_oficina_memoria_participante', generator: 'identity'
        papel       column:'sq_papel'
        oficinaMemoria column:'sq_oficina_memoria'
    }
}
