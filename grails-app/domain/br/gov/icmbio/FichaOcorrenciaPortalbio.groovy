package br.gov.icmbio

import com.vividsolutions.jts.geom.Geometry
import org.hibernate.spatial.GeometryType

class FichaOcorrenciaPortalbio {

    Ficha ficha
    VwFicha vwFicha
    String deUuid
    String noCientifico
    String noLocal
    String noInstituicao
    String noAutor
    Date dtOcorrencia
    Date dtCarencia
    String txOcorrencia
    String deAmeaca
    DadosApoio situacao
    DadosApoio situacaoAvaliacao

    Date dtRevisao
    Date dtValidacao
    PessoaFisica pessoaRevisor
    PessoaFisica pessoaValidador
    String inUtilizadoAvaliacao
    String txNaoUtilizadoAvaliacao
    DadosApoio motivoNaoUtilizadoAvaliacao
    String txObservacao

    Integer idOrigem
    Date dtAlteracao
    String noBaseDados

    Integer sqPais
    Integer sqEstado
    Integer sqMunicipio
    Integer sqBioma
    Integer sqBaciaHidrografica
    Integer sqUcFederal
    Integer sqUcEstadual
    Integer sqRppn
    String inDataDesconhecida
    String txNaoAceita
    String inPresencaAtual

    Geometry geometry

    static constraints = {
        ficha           unique:['ficha','noAutor','dtOcorrencia','geometry']
        vwFicha         nullable:true
        noLocal         nullable:true
        noInstituicao   nullable:true
        //noAutor         nullable:true
        dtOcorrencia    nullable:true
        dtCarencia      nullable:true
        deAmeaca        nullable:true
        situacao        nullable:true
        situacaoAvaliacao nullable: false
        dtRevisao       nullable:true
        dtValidacao     nullable:true
        pessoaRevisor   nullable:true
        pessoaValidador nullable:true
        inUtilizadoAvaliacao nullable: true, inList: ['S', 'N','E']
        inDataDesconhecida nullable: true, inList: ['S', 'N']
        txNaoUtilizadoAvaliacao nullable:true
        motivoNaoUtilizadoAvaliacao nullable:true
        txOcorrencia nullable:true,blank: true
        txObservacao nullable:true,blank: true
        idOrigem nullable: true
        dtAlteracao nullable: true
        noBaseDados nullable: true
        sqPais nullable:true
        sqEstado nullable:true
        sqMunicipio nullable:true
        sqBioma nullable:true
        sqBaciaHidrografica nullable:true
        sqUcFederal nullable:true
        sqUcEstadual nullable:true
        sqRppn nullable:true
        txNaoAceita nullable:true, blank:true
        inPresencaAtual nullable: true, maxSize: 1, inList: ['S', 'N', 'D']
    }

    static mapping = {
        version     false
        sort        id: 'asc'
        table       name:'salve.ficha_ocorrencia_portalbio'
        id          column:'sq_ficha_ocorrencia_portalbio',generator:'identity'
        ficha       column:'sq_ficha'
        vwFicha     column:'sq_ficha',insertable: false,updateable: false
        geometry    type: GeometryType, sqlType: "GEOMETRY"
        geometry    column: 'ge_ocorrencia'
        situacao    column:'sq_situacao'
        situacaoAvaliacao column: 'sq_situacao_avaliacao'
        pessoaValidador column:'sq_pessoa_validador'
        pessoaRevisor column:'sq_pessoa_revisor'
        motivoNaoUtilizadoAvaliacao column:'sq_motivo_nao_utilizado_avaliacao'
    }

    /*def beforeInsert()
    {
        return (dtOcorrencia != null)
    }*/

    def beforeValidate() {
        // SRID padrão: 4674
        if (this.geometry && this.geometry?.SRID == 0) {
            geometry.SRID = 4674
        }

        if( ! this.situacaoAvaliacao ){
            this.situacaoAvaliacao = DadosApoio.findByCodigoSistemaAndPai('REGISTRO_NAO_CONFIRMADO', DadosApoio.findByCodigoSistema('TB_SITUACAO_REGISTRO_OCORRENCIA'));
        }

        if( ! this.inPresencaAtual ) {
            this.inPresencaAtual = 'S'
        }
    }

    def beforeUpdate() {
        dtAlteracao = new Date()
    }

    String getInPresencaAtualText() {
        if (this.inPresencaAtual == 'S') {
            return 'Sim'
        } else if (this.inPresencaAtual == 'N') {
            return 'Não'
        } else if (this.inPresencaAtual == 'D') {
            return 'Desconhecida'
        }
        return ''
    }

    String getInUtilizadoAvaliacaoText()
    {
        if (this.inUtilizadoAvaliacao == 'S')
        {
            return 'Sim'
        }
        else if (this.inUtilizadoAvaliacao == 'N')
        {
            return 'Não'
        }
        else if (this.inUtilizadoAvaliacao == 'E')
        {
            return 'Excluído'
        }
        else if (this.inUtilizadoAvaliacao == 'D')
        {
            return 'Desconhecido'
        }
        return ''
    }

    Double getLatDecimal() {
        return (this?.geometry ? this.geometry.y : null)
    }

    Double getLonDecimal() {
        return (this?.geometry ? this.geometry.x : null)
    }

    Map getRefBib2Planilha() {
        Map result = [refBibs: [], autores: []]
        if (this.id) {
            FichaRefBib.createCriteria().list {
                eq('noTabela', 'ficha_ocorrencia_portalbio')
                eq('noColuna', 'sq_ficha_ocorrencia_portalbio')
                eq('sqRegistro', this.id.toInteger())
            }.each {
                String refBib = it.referenciaHtml
                if( ! result.refBibs.contains(refBib) ){
                    result.refBibs.push( refBib )
                }
                String autor = ''
                if( it.noAutor ){
                    autor = Util.nome2citacao( it.noAutor.trim() ) + ( it.nuAno ? ', ' + it.nuAno.toString() : '')
                    autor = autor.replaceAll(/\.(\s[0-9]{4})$/, '.,$1') + ' (Com.Pess.)'
                } else if ( it.publicacao ) {
                    autor = Util.getNomesEtAll(it.publicacao.noAutor) + (it.publicacao.nuAnoPublicacao ? ', ' + it.publicacao.nuAnoPublicacao.toString() : '')
                }
                autor = Util.stripTags3(autor)
                if( ! result.autores.contains( autor ) ){
                    result.autores.push( autor )
                }
            }
        }
        return result
    }



}
