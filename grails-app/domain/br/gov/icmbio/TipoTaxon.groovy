package br.gov.icmbio

class TipoTaxon {

	String descricao

	static constraints = {
	}

	static mapping = {
		version		(false)
		sort		(descricao:'asc')
		table		(tableName:'salve.tipo_taxon')
		id			(column:'sq_tipo_taxon', cache:false)
		descricao	(column:'de_tipo_taxon', cache:false)
	}
}