package br.gov.icmbio

class FichaVersionamento {

    Ficha ficha
    String inStatus
    String dsLog
    Date dtInclusao

    static constraints = {
        ficha nullable: true
        dsLog nullable: true
        inStatus nullable: true
        dtInclusao nullable: true
    }

    static mapping = {
        version false;
        table name: 'salve.ficha_versionamento';
        id column: 'sq_ficha', generator: 'foreign', params: [property: 'ficha']
        ficha insertable: false, updateable: false, column: 'sq_ficha'
    }

    def beforeValidate() {
        dtInclusao = new Date()
    }
}
