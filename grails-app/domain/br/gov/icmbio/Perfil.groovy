package br.gov.icmbio

class Perfil {

    String noPerfil
    String cdSistema
    Date dtInclusao
    Date dtAlteracao

    static constraints = {}

    static mapping = {
        version false
        sort 		noPerfil: 'asc'
        table       name: 'salve.perfil'
        id          column: 'sq_perfil' ,generator:'identity'
    }


    def beforeValidate() {
        if( ! this.dtInclusao ) {
            this.dtInclusao = new Date()
        }
        this.dtAlteracao = new Date()

        // gerar o codigo do perfil para uso global nas regras de negocio
        if( ! cdSistema && noPerfil){
            String codigo =
            cdSistema = Util.removeAccents( noPerfil.toString().toUpperCase())
                    .replaceAll(/(?i)\s[a-z]{2,3}\s/,'_')
                    .replaceAll(/\s/,'_')
        }
    }
}
