package br.gov.icmbio

import com.vividsolutions.jts.geom.Geometry
import org.hibernate.spatial.GeometryType

class Pais {

	String noPais
    String coPais // código ISO do país Ex: BRA, ESP, ALA
    Geometry geometry
    Date dtInclusao
    Date dtAlteracao

    static constraints = {
    	noPais nullable: false, blank: false
    	coPais nullable: false, blank: false
        geometry nullable: true
        dtInclusao nullable: true
        dtAlteracao nullable: true

    }

    static mapping = {
    	version 	false
        sort        noPais  : 'asc'
        table       name    :'salve.pais'
        id          column  : 'sq_pais',generator:'identity'
        geometry    column  : 'ge_pais'
        geometry    type    : GeometryType, sqlType: "GEOMETRY"
    }

    def beforeValidate() {
        if ( ! this.dtInclusao ) {
            this.dtInclusao = new Date()
        }
        this.dtAlteracao = new Date()
    }
}
