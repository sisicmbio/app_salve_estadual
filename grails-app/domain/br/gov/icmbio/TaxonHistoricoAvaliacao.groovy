package br.gov.icmbio

import grails.converters.JSON

class TaxonHistoricoAvaliacao {

    Taxon taxon
    DadosApoio tipoAvaliacao
    Abrangencia abrangencia     // Goiás, Cerrado
    DadosApoio categoriaIucn
    Integer nuAnoAvaliacao
    String deCriterioAvaliacaoIucn
    String txJustificativaAvaliacao
    String stPossivelmenteExtinta
    String noRegiaoOutra
    String deCategoriaOutra
    Boolean stOficial
    Integer idOrigem

    static hasOne = [taxonHistoricoAvaliacaoVersao:TaxonHistoricoAvaliacaoVersao]

    static constraints = {
        taxon                       nullable:false
        tipoAvaliacao               nullable:false
        nuAnoAvaliacao              nullable:false

        abrangencia				    nullable:true
        categoriaIucn               nullable:true
        deCriterioAvaliacaoIucn		nullable:true
        txJustificativaAvaliacao	nullable:true
        stPossivelmenteExtinta      nullable:true, defaultValue:false
        noRegiaoOutra				nullable:true
        deCategoriaOutra            nullable:true
        taxonHistoricoAvaliacaoVersao nullable: true
        stOficial                     nullable: true
        idOrigem                      nullable: true
    }

    static mapping = {
        version 				false
        //sort                    nuAnoAvaliacao:'taxon'
        table       			name 	: 'salve.taxon_historico_avaliacao'
        id          			column 	: 'sq_taxon_historico_avaliacao',generator:'identity'
        taxon 					column  : 'sq_taxon'
        tipoAvaliacao			column 	: 'sq_tipo_avaliacao'
        categoriaIucn           column  : 'sq_categoria_iucn'
        abrangencia             column  : 'sq_abrangencia'      // Goias, Cerrado,
    }

    JSON asJson()
    {
        Map data = [:]
        data.put( 'sqTaxonHistoricoAvaliacao'	,this.id)
        data.put( 'sqTipoAvaliacao'				,this.tipoAvaliacao.id)
        data.put( 'nuAnoAvaliacao'				,this.nuAnoAvaliacao)

        data.put( 'sqAbrangencia' 		        ,this?.abrangencia?.id )
        data.put( 'deTipoAbrangencia'			    ,this?.abrangencia?.tipo?.descricao)
        data.put( 'deAbrangencia' 		        ,this?.abrangencia?.descricao)

        data.put( 'sqCategoriaIucn'				,this?.categoriaIucn?.id)
        data.put( 'deCategoriaOutra'				,this?.deCategoriaOutra)

        data.put( 'dsCriterioAvalIucn'	    	,this.deCriterioAvaliacaoIucn)
        data.put( 'stPossivelmenteExtinta'	    ,this.stPossivelmenteExtinta)
        data.put( 'txJustificativaAvaliacao'	    ,this.txJustificativaAvaliacao)
        data.put( 'noRegiaoOutra'				    ,this.noRegiaoOutra)
        data.put( 'stOficial'				    ,this.stOficial)
        return data as JSON
    }

    String getRefBibHtml( Long sqFicha = null )
    {
        //List nomes = []
        List refs = FichaRefBib.createCriteria().list {
            eq('noTabela', 'taxon_historico_avaliacao')
            eq('noColuna', 'sq_taxon_historico_avaliacao')
            eq('sqRegistro', this.id.toInteger())
            if ( sqFicha ) {
                eq('ficha.id', sqFicha)
            }
            isNotNull('publicacao') // para não incluir as comunicações pessoais
        }
        return Util.formatRefBib(refs,true)

    }

    String getRefBibText( Long sqFicha = null )
    {
        //List nomes = []
        List refs = FichaRefBib.createCriteria().list{
            eq('noTabela','taxon_historico_avaliacao' )
            eq('noColuna','sq_taxon_historico_avaliacao')
            eq('sqRegistro', this.id.toInteger())
            if ( sqFicha ) {
                eq('ficha.id', sqFicha)
            }
            isNotNull('publicacao') // para não incluir as comunicações pessoais
        }
        return Util.formatRefBib(refs,false)
    }

    DadosApoio getTipoAbrangencia()
    {
        return this?.abrangencia?.tipo
    }
}
