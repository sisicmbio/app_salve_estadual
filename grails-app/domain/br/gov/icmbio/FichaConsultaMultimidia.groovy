package br.gov.icmbio

class FichaConsultaMultimidia {

    static belongsTo = [fichaOcorrenciaConsulta:FichaOcorrenciaConsulta]
    FichaMultimidia fichaMultimidia

    static constraints = {
    }

    static mapping = {
        version 				false
        table       			name 	: 'salve.ficha_consulta_multimidia'
        id          			column 	: 'sq_ficha_consulta_multimidia',generator:'identity'
        fichaOcorrenciaConsulta	column  : 'sq_ficha_ocorrencia_consulta'
        fichaMultimidia	        column  : 'sq_ficha_multimidia'
    }
}
