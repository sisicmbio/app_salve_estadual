package br.gov.icmbio

import com.vividsolutions.jts.geom.Geometry
import org.hibernate.spatial.GeometryType

class UnidadeConservacao
{
    Instituicao instituicao
    String cdCnuc
    DadosApoio esfera
    DadosApoio tipo
    Geometry geometry
    Date dtAlteracao

    static constraints = {
        cdCnuc nullable: true
        geometry nullable: true
    }

    static mapping = {
        version false
        table name: 'salve.unidade_conservacao'
        id          column:'sq_pessoa', generator: 'foreign', params: [property: 'instituicao']
        instituicao insertable: false, updateable: false, column: 'sq_pessoa'
        esfera      column: 'sq_esfera'
        tipo        column: 'sq_tipo'
        geometry    column:'ge_uc'
        geometry    type: GeometryType, sqlType: "GEOMETRY"
    }

    def beforeValidate() {
        this.dtAlteracao = new Date()
    }
}

