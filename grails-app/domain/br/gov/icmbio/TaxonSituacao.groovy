package br.gov.icmbio

class TaxonSituacao {

    TaxonGrupo grupo
    String deSituacao
    String coSituacao

    static constraints = {
    }

    static mapping = {
        version		false
        sort		deSituacao: 'asc'
        table       name 	: 'taxonomia.situacao'
        id			column  : 'sq_situacao'
        grupo       column  : 'sq_grupo'
    }
}
