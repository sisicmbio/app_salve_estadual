package br.gov.icmbio

class TraducaoFichaPercentual {

    Ficha ficha
    Integer nuPercentualTraduzido
    Integer nuPercentualRevisado

    static constraints = {
        nuPercentualRevisado nullable: true
        nuPercentualTraduzido nullable: true
    }

    static mapping = {
        version false
        table name: 'salve.traducao_ficha_percentual'
        id column: 'sq_ficha', generator: 'foreign', params: [property: 'ficha']
        ficha insertable: false, updateable: false, column: 'sq_ficha'
    }

    def beforeValidate() {
        if( ! nuPercentualTraduzido ){
            nuPercentualTraduzido = 0
        }

        if( ! nuPercentualRevisado ){
            nuPercentualRevisado = 0
        }
    }
}
