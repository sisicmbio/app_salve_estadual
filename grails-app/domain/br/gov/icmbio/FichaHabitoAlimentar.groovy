package br.gov.icmbio
import grails.converters.JSON

class FichaHabitoAlimentar {

	Ficha  ficha;
	DadosApoio tipoHabitoAlimentar
    String dsFichaHabitoAlimentar

    static constraints = {
    	dsFichaHabitoAlimentar 	nullable:true, blank:true
    	tipoHabitoAlimentar 	nullable:false
    }

    static mapping = {
    	version 			false
	   	table       		name 	: 'salve.ficha_habito_alimentar'
	   	id          		column 	: 'sq_ficha_habito_alimentar',generator:'identity'
	   	ficha 				column  : 'sq_ficha'
	   	tipoHabitoAlimentar column 	: 'sq_tipo_habito_alimentar'
    }

    public asJson()
    {
    	Map data = [:]
	    data.put( 'sqFichaHabitoAlimentar',this.id)
        data.put( 'sqFicha',this.ficha.id)
        data.put( 'sqTipoHabitoAlimentar',this.tipoHabitoAlimentar.id)
        data.put( 'dsFichaHabitoAlimentar',this.dsFichaHabitoAlimentar)
        data.put( 'dsTipoHabitoAlimentar',this.tipoHabitoAlimentar.descricao)
        data.put( 'cdTipoHabitoAlimentar',this.tipoHabitoAlimentar.codigo)
        data.put( 'cdTipoHabitoAlimentar',this.tipoHabitoAlimentar.codigo)
        return data as JSON
    }

    public String getRefBibHtml() {
        //List nomes = [];
        List refs = FichaRefBib.findAllByFichaAndNoTabelaAndNoColunaAndSqRegistro(this.ficha,'ficha_habito_alimentar','sq_ficha_habito_alimentar',this.id);
        return Util.formatRefBib(refs,true)
        /*
        refs.each{
            if( it.publicacao )
            {
                nomes.push( it.publicacao.citacao+'&nbsp;<span class="label label-warning tag cursor-pointer" title="'+it.publicacao.deTitulo.replaceAll(/"/,"&quot;")+'">...</span>' );
            }
            else
            {
                nomes.push( it.noAutor + ', ' + it.nuAno + ' (Com.Pess.)&nbsp;<span class="label label-warning tag cursor-pointer" title="Comunicação Pessoal">...</span>' );
            }
        }
        return nomes.join('<br>');
        */
    }

    public String getRefBibText()
    {
        //List nomes = [];
        List refs = FichaRefBib.findAllByFichaAndNoTabelaAndNoColunaAndSqRegistro(this.ficha,'ficha_habito_alimentar','sq_ficha_habito_alimentar',this.id);
        return Util.formatRefBib(refs,false)
        /*
        refs.each{
            if( it.publicacao )
            {
                nomes.push( it.publicacao.citacao);
            }
            else
            {
                nomes.push( it.noAutor + ', ' + it.nuAno + ' (Com.Pess.)');
            }
        }
        return nomes.join("\n");
         */
    }

}
