package br.gov.icmbio

import groovy.sql.Sql

class FichaVersao {
    def dataSource

    PessoaFisica usuario
    Ficha ficha
    DadosApoio contexto
    Integer nuVersao
    Boolean stPublico
    Date dtInclusao
    // campo jsonb não pode ter no gorm


    static constraints = {

    }

    static mapping = {
        version false
        table name: 'salve.ficha_versao'
        id column: 'sq_ficha_versao', generator: 'identity'
        ficha column: 'sq_ficha'
        usuario column: 'sq_usuario_inclusao'
        ficha column: 'sq_ficha'
        contexto column:'sq_contexto'
    }

    def beforeValidate() {

        if (!dtInclusao) {
            dtInclusao = new Date()
        }
    }

    String getJsFicha(){
        String result = ''
        try {
            String query = "select js_ficha::text from salve.ficha_versao where sq_ficha_versao = "+id.toString()
            Sql sql = new Sql(dataSource)
            sql.rows(query).each{
                result = it.js_ficha
            }
            sql.close()
        } catch (Exception e) {
            println e.getMessage()
        }
        return result
    }

    String getJsRegistros(){
        String result = ''
        try {
            String query = "select js_ocorrencias::text from salve.ocorrencias_json where sq_ficha_versao = "+id.toString()
            Sql sql = new Sql(dataSource)
            sql.rows(query).each{
                result = it.js_ocorrencias
            }
            sql.close()
        } catch (Exception e) {
            println e.getMessage()
        }
        return result
    }


}
