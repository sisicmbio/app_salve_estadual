package br.gov.icmbio
class FichaHistSituacaoExcluida {

    Ficha ficha
    Date dtInclusao
    PessoaFisica usuario
    String dsJustificativa

    static constraints = {

    }

    static mapping = {
        version 				false
        table       			name 	: 'salve.ficha_hist_situacao_excluida'
        id          			column 	: 'sq_ficha_hist_situacao_excluida',generator:'identity'
        ficha 					column  : 'sq_ficha'
        usuario                 column  : 'sq_usuario_inclusao'
    }

    def beforeValidate() {
        if ( ! this.dtInclusao ) {
            this.dtInclusao = new Date()
        }
    }

}
