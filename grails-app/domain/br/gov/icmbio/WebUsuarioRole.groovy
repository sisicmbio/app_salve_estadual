package br.gov.icmbio

import grails.gorm.DetachedCriteria
import groovy.transform.ToString

import org.apache.commons.lang.builder.HashCodeBuilder

@ToString(cache=true, includeNames=true, includePackage=false)
class WebUsuarioRole
{

	//private static final long serialVersionUID = 1

	WebUsuario webUsuario
	WebRole webRole
	Date dtInclusao

    WebUsuarioRole(WebUsuario u, WebRole r) {
		this()
		webUsuario = u
		webRole = r
	}

	def beforeValidate() {
		if( !this.dtInclusao )
		{
			this.dtInclusao = new Date()
		}
	}

	@Override
	boolean equals(other) {
		if (!(other instanceof WebUsuarioRole)) {
			return false
		}

		other.webUsuario?.id == webUsuario?.id && other.webRole?.id == webRole?.id
	}

	@Override
	int hashCode() {
		def builder = new HashCodeBuilder()
		if (webUsuario) builder.append(webUsuario.id)
		if (webRole) builder.append(webRole.id)
		builder.toHashCode()
	}

	static WebUsuarioRole get(long webUsuarioId, long webRoleId) {
		criteriaFor(webUsuarioId, webRoleId).get()
	}

	static boolean exists(long webUsuarioId, long webRoleId) {
		criteriaFor(webUsuarioId, webRoleId).count()
	}

	private static DetachedCriteria criteriaFor(long webUsuarioId, long webRoleId) {
		WebUsuarioRole.where {
			webUsuario == WebUsuario.load(webUsuarioId) &&
			webRole == WebRole.load(webRoleId)
		}
	}

	static WebUsuarioRole create(WebUsuario webUsuario, WebRole webRole, boolean flush = false) {
		def instance = new WebUsuarioRole(webUsuario: webUsuario, webRole: webRole)
		instance.save(flush: flush, insert: true)
		instance
	}

	static boolean remove(WebUsuario u, WebRole r, boolean flush = false) {
		if (u == null || r == null) return false

		int rowCount = WebUsuarioRole.where { webUsuario == u && webRole == r }.deleteAll()

		if (flush) { WebUsuarioRole.withSession { it.flush() } }

		rowCount
	}

	static void removeAll(WebUsuario u, boolean flush = false) {
		if (u == null) return

		WebUsuarioRole.where { webUsuario == u }.deleteAll()

		if (flush) { WebUsuarioRole.withSession { it.flush() } }
	}

	static void removeAll(WebRole r, boolean flush = false) {
		if (r == null) return

		WebUsuarioRole.where { webRole == r }.deleteAll()

		if (flush) { WebUsuarioRole.withSession { it.flush() } }
	}

	static constraints = {
		webRole validator: { WebRole r, WebUsuarioRole ur ->
			if (ur.webUsuario == null || ur.webUsuario.id == null) return
			boolean existing = false
			WebUsuarioRole.withNewSession {
				existing = WebUsuarioRole.exists(ur.webUsuario.id, r.id)
			}
			if (existing) {
				return 'userRole.exists'
			}
		}
	}

	static mapping = {
		version false
		//id composite: ['webUsuario', 'webRole']
		table name		 : 'salve.web_usuario_role';
		id column		 : 'sq_web_usuario_role',generator:'identity'
		webUsuario column: 'sq_web_usuario'
		webRole column 	 : 'sq_web_role'
	}
}
