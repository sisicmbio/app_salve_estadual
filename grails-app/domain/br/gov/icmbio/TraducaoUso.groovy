package br.gov.icmbio

// Alteracoes integração com SIS/IUCN
class TraducaoUso {

    String txTraduzido
    String txRevisado
    Date dtInclusao
    Date dtAlteracao

    static belongsTo = [uso:Uso]

    static constraints = {
        txTraduzido nullable: false, blank:false
        txRevisado nullable: false, blank:false
        uso nullable: false
    }

    static mapping = {
        version 	  false
        sort        txTraduzido: 'asc'
        table       name:'salve.traducao_uso'
        id          column:'sq_traducao_uso',generator:'identity'
        uso         column:'sq_uso'
    }

    def beforeValidate() {
        if( ! dtInclusao ) {
            dtInclusao = new Date()
        }
        dtAlteracao = new Date()
    }
}
