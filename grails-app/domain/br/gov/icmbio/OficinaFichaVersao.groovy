package br.gov.icmbio

class OficinaFichaVersao {

    OficinaFicha oficinaFicha
    FichaVersao fichaVersao

    static constraints = {

    }
    static mapping = {
        version 	false
        table               name  : 'salve.oficina_ficha_versao'
        id                  column:'sq_oficina_ficha', generator: 'foreign', params: [property: 'oficinaFicha']
        oficinaFicha        insertable: false, updateable: false, column: 'sq_oficina_ficha'
        fichaVersao         column : 'sq_ficha_versao'
    }
}
