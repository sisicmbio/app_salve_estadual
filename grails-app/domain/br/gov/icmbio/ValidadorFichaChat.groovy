package br.gov.icmbio

class ValidadorFichaChat {

    static belongsTo = [validadorFicha:ValidadorFicha]
    PessoaFisica pessoa
    Date dtMensagem
    String deMensagem
    String sgPerfil
    static constraints = {
        validadorFicha nullable:true
    }

    static mapping = {
        version false
        table           name: 'salve.validador_ficha_chat'
        id              column: 'sq_validador_ficha_chat', generator: 'identity'
        validadorFicha  column: 'sq_validador_ficha'
        pessoa          column: 'sq_pessoa'
    }
}
