package br.gov.icmbio
import grails.converters.JSON;

class FichaAmeaca {

	Ficha ficha
    VwAmeaca vwAmeaca
	DadosApoio criterioAmeacaIucn
	DadosApoio referenciaTemporal
	String txFichaAmeaca
    Integer nuPeso
    static hasMany = [fichaAmeacaEfeitos:FichaAmeacaEfeito]

    static constraints = {
    	txFichaAmeaca	nullable:true
        nuPeso nullable:true
        referenciaTemporal nullable: true
        vwAmeaca nullable: true
       }

    static mapping = {
		version 				false
	   	table       			name 	: 'salve.ficha_ameaca'
	   	id          			column 	: 'sq_ficha_ameaca',generator:'identity'
	   	ficha 					column  : 'sq_ficha'
	   	criterioAmeacaIucn		column 	: 'sq_criterio_ameaca_iucn'
        vwAmeaca                column  : 'sq_criterio_ameaca_iucn', insertable: false, updateable: false
        referenciaTemporal      column  : 'sq_referencia_temporal'
	}

    /*def afterDelete()
    {
        this.ficha.gravarListaPendenciasPreenchimento()
    }

    def afterInsert()
    {
        this.ficha.gravarListaPendenciasPreenchimento()
    }
    */


    public asJson()
    {
    	Map data = [:]
	    data.put( 'sqFichaAmeaca'				,this.id)
        data.put( 'txFichaAmeaca'				,this.txFichaAmeaca)
        data.put( 'sqCriterioAmeacaIucn'         ,this.criterioAmeacaIucn.id)
        data.put( 'deCriterioAmeacaIucn'		    ,this.criterioAmeacaIucn.descricaoCompleta)
        data.put( 'nuPeso'		                ,this.nuPeso?:'')
        data.put( 'sqReferenciaTemporal'		    ,this.referenciaTemporal ? this.referenciaTemporal.id:'')
        data.put( 'dsReferenciaTemporal'		    ,this.referenciaTemporal ? this.referenciaTemporal.descricao:'')
        return data as JSON
    }

    public String getRefBibHtml()
    {
        //List nomes = [];
        List refs = FichaRefBib.findAllByFichaAndNoTabelaAndNoColunaAndSqRegistro(this.ficha,'ficha_ameaca','sq_ficha_ameaca',this.id);
        return Util.formatRefBib(refs,true)

        /*refs.each{
            if( it.publicacao )
            {
                //nomes.push( it.publicacao.citacao+'&nbsp;<span class="label label-warning tag cursor-pointer" title="'+it.publicacao.deTitulo.replaceAll(/"/,"&quot;")+'">...</span>' );
                nomes.push( it.publicacao.citacao+'&nbsp;<span class="label label-warning tag cursor-pointer" title="'+it.publicacao.referenciaHtml.replaceAll(/"/,"&quot;")+'">...</span>' );
            }
            else
            {
                nomes.push( it.noAutor + ', ' + it.nuAno + ' (Com.Pess.)&nbsp;<span class="label label-warning tag cursor-pointer" title="Comunicação Pessoal">...</span>' );
            }
        }
        return nomes.join('<br>');

         */
    }

    public String getRefBibText()
    {
        //List nomes = [];
        List refs = FichaRefBib.findAllByFichaAndNoTabelaAndNoColunaAndSqRegistro(this.ficha,'ficha_ameaca','sq_ficha_ameaca',this.id);
        return Util.formatRefBib(refs,false)
        /*refs.each{
            if( it.publicacao )
            {
                nomes.push( it.publicacao.citacao);
            }
            else
            {
                nomes.push( it.noAutor + ', ' + it.nuAno + ' (Com.Pess.)' )
            }
        }
        return nomes.join('\n');

         */
    }

    public String getRegioesHtml()
    {
        List regioes = [];
         DadosApoio contexto = DadosApoio.findByCodigo('TB_CONTEXTO_OCORRENCIA');
        contexto = DadosApoio.findByCodigoSistemaAndPai('AMEACA_REGIAO',contexto);
        List dados = FichaAmeacaRegiao.createCriteria().list {
            createAlias('fichaOcorrencia', 'b')
            eq("fichaAmeaca", this)
            eq('b.contexto',contexto)
        }
        dados.each {
            regioes.push( '<li>'+it.deTipoAbrangencia+(it?.noRegiao ? ' - '+it.noRegiao:'')+'</li>');
        }

        return (regioes.size() > 1 ? '<br>':'') + '<ol>'+regioes.join('')+'</ol>';
    }

    public String getRegioesText()
    {
        List regioes = [];
         DadosApoio contexto = DadosApoio.findByCodigo('TB_CONTEXTO_OCORRENCIA');
        contexto = DadosApoio.findByCodigoSistemaAndPai('AMEACA_REGIAO',contexto);
        List dados = FichaAmeacaRegiao.createCriteria().list {
            createAlias('fichaOcorrencia', 'b')
            eq("fichaAmeaca", this)
            eq('b.contexto',contexto)
        }
        dados.eachWithIndex { regiao, index ->
            int i = index+97
            regioes.push( Character.toString( (char)i ) + '. '+regiao.deTipoAbrangencia+ (regiao?.noRegiao ? ' - '+regiao.noRegiao : '') )
        }
        return regioes.join('\n')
    }

    public String getGeoHtml()
    {
        List coords = [];
        DadosApoio contexto = DadosApoio.findByCodigo('TB_CONTEXTO_OCORRENCIA');
        contexto = DadosApoio.findByCodigoSistemaAndPai('AMEACA_GEO',contexto);
        List dados = FichaAmeacaRegiao.createCriteria().list {
            createAlias('fichaOcorrencia', 'b')
            eq("fichaAmeaca", this)
            eq('b.contexto',contexto)
        }
        dados.each {
            coords.push( it.fichaOcorrencia.coordenadaGmsHtml);
        }
        return '<br/>'+coords.join('<hr class="hr-separador">');
    }

    String getEfeitosHtml()
    {
        List linhas = []
        fichaAmeacaEfeitos.each {

        }
        return linhas.join('<br>')
    }

    List getTrilhaList()
    {
        if( vwAmeaca )
        {
            return vwAmeaca.trilhaList
        }
        return []
    }

    String getTrilhaPath()
    {
        if( vwAmeaca )
        {
            return vwAmeaca.trilhaPath
        }
        return ''
    }

    List getTreePesos()
    {
        List tree = []
        fichaAmeacaEfeitos.each {
            Map item = tree.find { it2 -> it.efeito.codigo + ' - ' + it.efeito.descricao == it2.descricao }
            if( ! item ) {
                String codigo = it.efeito.ordem.split('\\.').collect{it.toString().padLeft(3,'0')}.join('.')
                tree.push([ordem: codigo, descricao: it.efeito.codigo + ' - ' + it.efeito.descricao, peso: it.peso])
            }
            else {
                item.peso = it.peso
            }
            it.trilhaEfeitoList.each { efeito ->
                if ( ! tree.find { it2 ->
                    it2.descricao.toString().trim() == efeito.toString().trim()
                }) {
                    Integer posicao = efeito.toString().indexOf(' - ')
                    if( posicao > -1 ) {
                        String codigo = efeito.toString().substring(0, posicao).split('\\.').collect {
                            it.toString().padLeft(3, '0')
                        }.join('.')
                        tree.push([ordem: codigo, descricao: efeito.toString().trim(), peso: ''])
                    }
                }
            }
        }
        tree.sort{ it.ordem }
    }
}
