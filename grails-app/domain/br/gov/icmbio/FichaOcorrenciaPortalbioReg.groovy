package br.gov.icmbio

class FichaOcorrenciaPortalbioReg {

    FichaOcorrenciaPortalbio fichaOcorrenciaPortalbio
    Registro registro
    Date dtInclusao
    static constraints = {
    }
    static mapping = {
        version 				false
        table       			name 	: 'salve.ficha_ocorrencia_portalbio_reg'
        id          			column 	: 'sq_ficha_ocorrencia_portalbio_reg',generator:'identity'
        registro                column  : 'sq_registro'
        fichaOcorrenciaPortalbio column : 'sq_ficha_ocorrencia_portalbio'
    }
}
