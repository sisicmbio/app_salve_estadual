package br.gov.icmbio

import com.vividsolutions.jts.geom.Geometry
import org.hibernate.spatial.GeometryType

class Registro {

    Date dtInclusao
    Geometry geometry

    static constraints = {
    }

    static mapping = {
        version 				false
        table       			name 	: 'salve.registro'
        id          			column 	: 'sq_registro',generator:'identity'
        geometry                type    : GeometryType, sqlType: "GEOMETRY"
        geometry                column  : 'ge_registro'
    }

    def beforeValidate() {
        if ( ! this.dtInclusao ) {
            this.dtInclusao = new Date()
        }
        // SRID padrão: 4674
        if ( this.geometry && this.geometry?.SRID == 0 ) {
            geometry.SRID = 4674
        }

    }

}
