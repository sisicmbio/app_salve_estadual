package br.gov.icmbio

class IucnAmeacaApoio {

    IucnAmeaca iucnAmeaca
    DadosApoio ameaca
    Date dtInclusao
    Date dtAlteracao

    static constraints = {
    }

    static mapping = {
        version				false
        table				name            : 'salve.iucn_ameaca_apoio'
        id          		column          : 'sq_iucn_ameaca_apoio',generator:'identity'
        iucnAmeaca          column          : 'sq_iucn_ameaca'
        ameaca              column          : 'sq_dados_apoio'
    }

    def beforeValidate() {
        if( ! this.dtInclusao ) {
            dtInclusao = new Date()
        }
        this.dtAlteracao = new Date()
    }

}
