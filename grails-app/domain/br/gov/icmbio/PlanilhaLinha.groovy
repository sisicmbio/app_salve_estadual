package br.gov.icmbio

class PlanilhaLinha {

    transient erros=[], taxon, dadosApoio = [:];
    transient taxonService
    transient isAdm = false
    transient isRegistro = false // false = Importação de ficha, true=Importação de ocorrencias

    String txNomeCientifico
    String txErros
    Integer nuLinha

    static belongsTo = [ planilha:Planilha]

    static constraints = {
        txErros     nullable:true,blank:true
    }

    static mapping = {
            version false
            tablePerHierarchy false
            id column       : 'sq_planilha_linha', generator: 'identity'
            table name      : 'salve.planilha_linha'
            planilha column : 'sq_planilha'
    }

    def beforeValidate()
    {
        this.txErros = this.erros.join('<br/>');
    }

    def beforeInsert()
    {
        this.taxon = null;
        if( ! this.txNomeCientifico || this.txNomeCientifico.trim() == '' )
        {
            this.erros.push('Erro: Nome Científico não informado!')
        }
        this.txNomeCientifico = this.txNomeCientifico.trim().replaceAll(/[^a-zA-Z-]/,' ').replaceAll('/  /g',' ')
        if( ! this.nuLinha )
        {
            this.erros.push('Erro: Número da linha não informado!');
        }
        if( ! this.planilha )
        {
            this.erros.push('Erro: Planilha não informada!');
        }
        VwFicha ficha
        Integer id

        // quando for importação de registros de ocorrencia, procurar na ficha o taxon
        this.taxon = null
        if( isRegistro )
        {
            // identificar se é especie ou subespecie
            List aTemp = this.txNomeCientifico.split(' ')
            String coNivelTaxonomico = ( aTemp.size() > 2 ) ? 'SUBESPECIE' : 'ESPECIE';
            ficha = VwFicha.findBySqCicloAvaliacaoAndNmCientificoIlikeAndCoNivelTaxonomico(planilha.cicloAvaliacao.id,this.txNomeCientifico+'%', coNivelTaxonomico)

            if( ficha )
            {
                this.taxon = ficha.taxon
            }
        }
        else
        {
            // verificar se existe o taxon na tabela taxon
            id = taxonService.taxonExists( this.txNomeCientifico )
            this.taxon = Taxon.get( id.toInteger() )
        }


        // validar se o nome cientifico já esta sendo avalidado
        if( this.taxon  )
        {
            if( this.taxon && this?.planilha?.unidade && this?.planilha?.cicloAvaliacao )
            {
                //println 'Validar Taxon ' + taxon.noCientifico;
                //println 'Unidade ' +this?.planilha?.unidade.sgUnidade;
                //println 'Ciclo ' + this?.planilha.cicloAvaliacao.deCicloAvaliacao;
                //println 'id Taxon ' + taxon.id;
                //println 'id Unidade ' +this.planilha.unidade.id
                //println 'id Ciclo ' + this.planilha.cicloAvaliacao.id
                if( !ficha )
                {
                    ficha = VwFicha.findBySqTaxonAndSqCicloAvaliacao(this.taxon.id ,this.planilha.cicloAvaliacao.id)
                }
                if( !isAdm && ficha && ficha.sqUnidadeOrg.toInteger() != this.planilha.unidade.id.toInteger())
                {
                    //println 'Taxon já cadastrada'
                    this.erros.push('Erro: Taxon já esta sendo avaliado neste ciclo pela unidade '+ficha.sgUnidadeOrg)
                    this.taxon=null;
                }
            }
        }
        else
        {
            if( isRegistro )
            {
                this.erros.push('Erro: Ficha não cadastrada para o táxon <b>' + this.txNomeCientifico+'</b>')
            }
            else
            {
                this.erros.push('Erro: Taxon <b>' + this.txNomeCientifico+'</b> não cadastrado no Sistema SINTAX.')
            }

        }
        return true;// sempre deve retornar true
    }
}
