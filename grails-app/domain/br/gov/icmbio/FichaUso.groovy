package br.gov.icmbio
import grails.converters.JSON;

class FichaUso {

	Ficha ficha
	Uso uso
	String txFichaUso

    static constraints = {
    	txFichaUso	nullable:true
       }

    static mapping = {
		version 				false
	   	table       			name 	: 'salve.ficha_uso'
	   	id          			column 	: 'sq_ficha_uso',generator:'identity'
	   	ficha 					column  : 'sq_ficha'
	   	uso                		column 	: 'sq_uso'
	}

    /*def afterDelete()
    {
        this.ficha.gravarListaPendenciasPreenchimento()
    }

    def afterInsert()
    {
        this.ficha.gravarListaPendenciasPreenchimento()
    }
    */

    public asJson()
    {
    	Map data = [:]
	    data.put( 'sqFichaUso'		,this.id)
        data.put( 'txFichaUso'		,this.txFichaUso)
        data.put( 'sqUso'           ,this.uso.id)
        data.put( 'deUso'		    ,this.uso.descricaoCompleta)
        return data as JSON
    }

    public String getRefBibHtml()
    {
        //List nomes = [];
        List refs = FichaRefBib.findAllByFichaAndNoTabelaAndNoColunaAndSqRegistro(this.ficha,'ficha_uso','sq_ficha_uso',this.id);
        return Util.formatRefBib(refs,true)
        /*refs.each{
            if( it.publicacao )
            {
                //nomes.push( it.publicacao.citacao+'&nbsp;<span class="label label-warning tag cursor-pointer" title="'+it.publicacao.deTitulo.replaceAll(/"/,"&quot;")+'">...</span>' );
                nomes.push( it.publicacao.citacao+'&nbsp;<span class="label label-warning tag cursor-pointer" title="'+it.publicacao.referenciaHtml.replaceAll(/"/,"&quot;")+'">...</span>' );
            }
            else
            {
                nomes.push( it.noAutor + ', ' + it.nuAno + ' (Com.Pess.)&nbsp;<span class="label label-warning tag cursor-pointer" title="Comunicação Pessoal">...</span>' );
            }
        }
        return nomes.join('<br>');
        */
    }
    public String getRefBibText() {
        //List nomes = [];
        List refs = FichaRefBib.findAllByFichaAndNoTabelaAndNoColunaAndSqRegistro(this.ficha,'ficha_uso','sq_ficha_uso',this.id);
        return Util.formatRefBib(refs,false)
        /*refs.each{
            if( it.publicacao )
            {
                nomes.push( it.publicacao.citacao)
            }
            else
            {
                nomes.push( it.noAutor + ', ' + it.nuAno + ' (Com.Pess.)')
            }
        }
        return nomes.join('\n');
         */
    }

    public String getRegioesHtml()
    {
        List regioes = [];
        /*FichaUsoRegiao.findAllByFichaUso(this).each{
            regioes.push( '<li>'+it.deTipoAbrangencia+' - '+it.noRegiao+'</li>')
        }
        */

        DadosApoio contexto = DadosApoio.findByCodigo('TB_CONTEXTO_OCORRENCIA');
        contexto = DadosApoio.findByCodigoSistemaAndPai('USO_REGIAO',contexto);
        List dados = FichaUsoRegiao.createCriteria().list {
            createAlias('fichaOcorrencia', 'b')
            eq("fichaUso", this)
            eq('b.contexto',contexto)
        }
        dados.each {
            regioes.push( '<li>'+it.deTipoAbrangencia+(it.noRegiao?' - ' + it.noRegiao : '')+'</li>');
        }

        return (regioes.size() > 1 ? '<br>':'') + '<ol>'+regioes.join('')+'</ol>';
    }

    public String getRegioesText()
    {
        List regioes = [];
        DadosApoio contexto = DadosApoio.findByCodigo('TB_CONTEXTO_OCORRENCIA');
        contexto = DadosApoio.findByCodigoSistemaAndPai('USO_REGIAO',contexto);
        List dados = FichaUsoRegiao.createCriteria().list {
            createAlias('fichaOcorrencia', 'b')
            eq("fichaUso", this)
            eq('b.contexto',contexto)
        }
        dados.eachWithIndex {regiao,index ->
            regioes.push( (index+1)+'. '+regiao.deTipoAbrangencia + ( regiao.noRegiao ?' - '+regiao.noRegiao:''))
        }
        return regioes.join('\n')
    }

    public String getGeoHtml()
    {
        List coords = [];
        DadosApoio contexto = DadosApoio.findByCodigo('TB_CONTEXTO_OCORRENCIA');
        contexto = DadosApoio.findByCodigoSistemaAndPai('USO_GEO',contexto);
        List dados = FichaUsoRegiao.createCriteria().list {
            createAlias('fichaOcorrencia', 'b')
            eq("fichaUso", this)
            eq('b.contexto',contexto)
        }
        dados.each {
            coords.push( it.fichaOcorrencia.coordenadaGmsHtml);
        }
        return '<br/>'+coords.join('<hr class="hr-separador">');
    }
}
