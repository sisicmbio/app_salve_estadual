package br.gov.icmbio

class FichaUf {

    def sqlService

    Ficha ficha
    Estado estado
    VwFicha vwFicha

    static constraints = {
        ficha nullable:false
        estado nullable:false
        vwFicha nullable:true
    }

    static mapping = {
        version 				false
        table       			name 	: 'salve.ficha_uf'
        id          			column 	: 'sq_ficha_uf',generator:'identity'
        ficha 					column  : 'sq_ficha'
        estado             		column 	: 'sq_estado'
        vwFicha                 column  : 'sq_ficha',insertable: false,updateable: false
    }

    String getJsonRefBib(){
        if( ficha && id ) {
            List rows = sqlService.execSql("""
        select json_object_agg( concat('rnd',trunc((random()*100000))::text),
            json_build_object( 'sq_publicacao', x.sq_publicacao
                , 'de_titulo', publicacao.de_titulo
                , 'no_autor', x.no_autor
                , 'nu_ano', x.nu_ano) ) as json_ref_bib
        from salve.ficha_ref_bib x
        left outer join taxonomia.publicacao on publicacao.sq_publicacao = x.sq_publicacao
        where x.sq_ficha = :sqFicha
        and x.no_tabela = 'ficha_uf'
        and x.no_coluna = 'sq_ficha_uf'
        and x.sq_registro = :sqRegistro""", [sqFicha: ficha.id, sqRegistro: id])
            return rows[0].json_refs_bibs.toString()
        }
        return null
    }

}
