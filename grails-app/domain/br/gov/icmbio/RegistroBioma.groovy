package br.gov.icmbio

class RegistroBioma {

    Registro registro
    Bioma bioma
    Date dtInclusao

    static constraints = {
    }

    static mapping = {
        version 				false
        table       			name 	: 'salve.registro_bioma'
        id          			column 	: 'sq_registro', generator:'assigned'
        bioma                   column  : 'sq_bioma'
        registro                column  : 'sq_registro', insertable: false, updateable: false
    }

    def beforeValidate() {
        if ( ! this.dtInclusao ) {
            this.dtInclusao = new Date()
        }
        this.id = registro?.id
    }
}
