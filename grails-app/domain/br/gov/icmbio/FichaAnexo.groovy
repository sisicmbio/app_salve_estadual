package br.gov.icmbio

class FichaAnexo {

    static transient fichaService

    Ficha ficha
    DadosApoio contexto
    String deLegenda
    String noArquivo
    String deTipoConteudo
    String deLocalArquivo
    String inPrincipal
    Date dtAnexo

    static constraints = {
        inPrincipal maxSize: 1, inList: ['S', 'N']
        dtAnexo nullable:true
    }

    static mapping = {
        version false
        sort dtAnexo: 'desc'
        table name: 'salve.ficha_anexo'
        id column: 'sq_ficha_anexo', generator: 'identity'
        ficha column: 'sq_ficha'
        contexto column: 'sq_contexto'
    }

    def beforeInsert() {
        if( ! dtAnexo ) {
            dtAnexo = new Date();
        }
        // só pode ter um principal por contexto
        if (this.inPrincipal == 'S') {
            FichaAnexo.findAllByFichaAndContextoAndInPrincipal(this.ficha, this.contexto, 'S').each {
                it.inPrincipal = 'N';
            }
        }
    }

    def beforeUpdate() {
        if( ! dtAnexo ) {
            dtAnexo = new Date();
        }
        // só pode ter um principal por contexto
        if (this.inPrincipal == 'S') {
            FichaAnexo.findAllByFichaAndContextoAndInPrincipalAndIdNotEqual(this.ficha, this.contexto, 'S',this.id).each {
                it.inPrincipal = 'N';
            }
        }
    }

    def afterInsert()
    {
        // esta dando erro assim
        /*if( this.ficha ) {
            fichaService.updatePercentual( this.ficha.id )
        }
        */
    }
}
