package br.gov.icmbio

// Alteracoes integração com SIS/IUCN
class TraducaoTexto {

    String txOriginal
    String txTraduzido
    String txRevisado
    Date dtInclusao
    Date dtAlteracao

    static constraints = {
        txOriginal nullable: false, blank:false
        txTraduzido nullable: false, blank:false
        txRevisado nullable: false, blank:false
    }

    static mapping = {
        version 	  false
        sort        txOriginal: 'asc'
        table       name:'salve.traducao_texto'
        id          column:'sq_traducao_texto',generator:'identity'
    }

    def beforeValidate() {
        if( ! dtInclusao ) {
            dtInclusao = new Date()
        }
        dtAlteracao = new Date()
    }
}
