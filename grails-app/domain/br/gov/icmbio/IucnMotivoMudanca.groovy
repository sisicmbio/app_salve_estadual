package br.gov.icmbio

class IucnMotivoMudanca {

    String cdIucnMotivoMudanca
    String dsIucnMotivoMudanca
    String dsIucnMotivoMudancaOrdem
    Date dtInclusao
    Date dtAlteracao

    static constraints = {
    }

    static mapping = {
        version				false
        sort 				dsIucnMotivoMudancaOrdem  : 'asc'
        table				name                : 'salve.iucn_motivo_mudanca'
        id          		column              : 'sq_iucn_motivo_mudanca',generator:'identity'
    }

    def beforeValidate() {
        if( ! this.dtInclusao ) {
            dtInclusao = new Date()
        }
        this.dtAlteracao = new Date()
        if( !this.dsIucnMotivoMudancaOrdem ){
            this.dsIucnMotivoMudancaOrdem = this.cdIucnMotivoMudanca
        }
    }

    String getDescricaoCompleta() {
        return this.cdIucnMotivoMudanca+' - '+this.dsIucnMotivoMudanca
    }
}
