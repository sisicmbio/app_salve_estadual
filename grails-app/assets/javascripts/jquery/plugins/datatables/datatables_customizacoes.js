//# sourceURL=datatables_customizacoes.js

/**
 * configurações padrão do plugin DATATABLES
 * https://datatables.net/
 *
 */
var exportCnt=0; // contador temporário para numeração das linhas
var default_data_tables_options = {
    //"orderCellsTop": true,
    "paging":   false,
    "ordering": true,
    "retrieve": true, // evitar erro se inicializar mais de uma vez seguida
    "order": [ 0, 'asc' ],
    "searching": true,
    "searchHighlight": true,
    "dom": '<"dt-title">Bflrtip', // https://datatables.net/reference/option/dom

    // https://datatables.net/reference/option/columns.orderable
    "columnDefs": [
        { "orderable": false, "targets": [] }
    ],
    "info":     false,
    // https://datatables.net/reference/option/language
    "drawCallback": function( settings ) {
        //var api = this.api();
        //alert( api.rows().length )
        //alert( 'DataTables has redrawn the table' );
    },
    "language": {
        "search": "Localizar:",
        "zeroRecords": "<b>Nenhum registro encontrado!</b>",
        "enfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
        "enfoEmpty": "Mostrando 0 até 0 de 0 registros",
        "enfoFiltered": "(Filtrados de _MAX_ registros)",
        "enfoPostFix": "",
        "enfoThousands": ".",
        "lengthMenu": "_MENU_ resultados por página",
        "loadingRecords": "Carregando...",
        "processing": "Processando...",
        "zeroRecords": "Nenhum registro encontrado",
        "paginate": {
            "next": "Próxima",
            "previous": "Anterior",
            "first": "Primeira",
            "last": "Última"
        },
        "aria": {
            "sortAscending": ": Ordenar colunas de forma ascendente",
            "sortDescending": ": Ordenar colunas de forma descendente"
        }
    },
    //"buttons": {
    //    "name": 'primary',
        "buttons": [{
            "extend": 'excelHtml5',
            "text": 'Exportar p/ planilha',
            className: 'btn-primary',
            "orientation": 'landscape',
            "title":'SALVE - exportação',
            "titleAttr"     : 'Exportar o resultado da pesquisa abaixo para planilha',
            "filename"  : function(){
                var d = new Date();
                return 'salve-exportacao-'+ d.getDate()+'-'+(d.getMonth()+1)+'-'+d.getFullYear()+'-'+d.getHours()+''+d.getMinutes()+''+d.getSeconds()
            },
            "extension" :'.xlsx',
            "messageTop": function(a,b,c){
                return ''
            },
            "messageBottom":function(){
                var d = new Date();
                return 'Sistema SALVE - exportação realizada em ' + d.getDate()+'-'+(d.getMonth()+1)+'-'+d.getFullYear()+' as '+d.getHours()+' '+d.getMinutes()+' '+d.getSeconds();
            },
            "action": function(e, dt, button, config) {

                var that = this;
                if( dt.data().rows().count() == 0 ){
                    app.alertInfo('Pesquisa não possui resultado.');
                    return;
                }
                // Add code to make changes to table here
                app.confirm('Confirma a exportação da tabela para planilha?', function(){
                    app.blockUI('Exportando...',function(){
                        window.setTimeout(function(){
                            $.fn.dataTable.ext.buttons.excelHtml5.action.call(that,e, dt, button, config);
                            app.unblockUI();
                        },2000);
                    });
                })
                // Call the original action function afterwards to
                // continue the action.
                // Otherwise you're just overriding it completely.
                //$.fn.dataTable.ext.buttons.excelHtml5.action(e, dt, button, config);
            },


            "exportOptions": {
                /*columns:'.exportable,'*/
                /*columns: [ 0, 1, 2, 3,4,5 ],*/
                columns: function (idx, data, node) {
                    if (node.innerText == 'Ação' || $(node).hasClass('th-actions') || $(node).hasClass('no-export')) {
                        return false;
                    }
                    return true;
                },
                format: {
                    body: function (data, row, column, node) {

                        if( column == 0 && /^[0-9]/.test( row ) )
                        {
                            try {
                                data = String(parseInt(row) + 1);
                            } catch( e ) {}
                        }
                        data = data.trim().replace(/<br\/?>/gi,'\n');
                        var itensText=[];
                        var contador='';
                        if( /^<select/.test(data.trim() ) )
                        {
                            data = $(data).find('option:selected').text().trim();
                        }
                        else if( /^<(o|u)l/.test(data.trim() ) )
                        {
                            var isOl = /^<ol/.test( data.trim());
                            $(data).find('li').each( function(i,li){
                                    if( isOl )
                                    {
                                        contador=(i+1)+'. ';
                                    }
                                    itensText.push( contador + $( li ).text().trim() )
                                }
                            );
                            data = itensText.join('\n');
                        }
                        else if( /^</.test(data.trim() ) )
                        {
                            data = $(data).text();
                        }
                        return sanitizeHtml( data );
                    },
                    header: function (data, row, column, node) {
                        data = data.replace(/&nbsp;/gi,' ').replace(/<br\/?>/gi,' ').trim();
                        return data.split('<')[0];
                    }
                }
            },

            // se precisar formatar os dados
            /*"customize": function(data,b,c,d,e ){
            },
            */
        }],
    //},
    initComplete: function (datatable) {
       var that=this;
        datatable.updateFilters=true; // flag para o evento "order.dt" não entrar em loop
        $('.dataTables_filter input').removeClass('ignoreChange').addClass('ignoreChange');
        var data = that.data();
        if( data.title )
        {
            //$( this.api().table().container()).find('div.col-sm-6:first').html('<div class="datatable-title" id="' + this.attr('id') + '-dt-title">'+data.title+'</div>');
            $( that.api().table().container()).find('div.dt-title').html('<div class="datatable-title" id="' + this.attr('id') + '-dt-title">'+data.title+'</div>');
            //$('#'+this.attr('id')+"_wrapper div.row>div.col-sm-6:first").html('<h4 style="margin-top:20px">'+data.title+'</h4>');
        }

        this.on( 'processing.dt', function ( e, settings, processing ) {
            var rows = this.rows.length;
            if( processing ) {
                app.blockElement('formFiltro');
                $("#divTotalRegistros span").html('0');
                $("#divTotalRegistros span.selected").html('0');
            }
            else
            {
                app.unblockElement('formFiltro');
                //$("#divTotalRegistros span").html(rows + ( rows+' ' > 1 ? ' registros.' : ' registro.') );
                if (rows > 0) {
                    beep();
                }
            }
        } );

        this.on( 'draw.dt', function ( e, settings) {
            var dataTable = $(this).dataTable();
            //var count = this.rows( { selected: true } ).count();
            //var rows = Math.max(0,this.rows.length - 2)
            var total = dataTable.api().rows().count();
            var rows = dataTable.api().page.info().recordsDisplay;
            if( rows == total )
            {
                rows = 0;
            }
            if( total > 0 ) {
                $("#divTotalRegistros span.total").html(total);
                $("#divTotalRegistros span.selected").html(rows);
            }
        });

        /*this.on( 'click.td', function (e) {
            return false;
        } );
        */
        // numerar as linhas de 1..n
        this.on( 'order.dt search.dt', function () {
            var columns = $(this).dataTable().api().columns(0);
            var pageInfoStart = $(this).dataTable().api().page.info().start || 0;
            var rowStartNum = $(this).dataTable().fnSettings().oInit.rowNumberStart || (pageInfoStart+1);
            columns.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+rowStartNum;
            });
        } );

        // numerar as linhas
        /*this.on( 'order.dt search.dt', function (e) {
            e.preventDefault();
            that.dataTable.Api().columns(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
                cell.style.textAlign = 'center';
            } );
        });
        */

        /*this.on( 'search.dt', function (e,settings) {
            log( e.target.search() )
        } );
        */
        /*// atualizra os filtros apos ordenar pela coluna
        this.on( 'order.dt', function (event,dtable, sort,col,a,b,c) {
            if( dtable.updateFilters ) {
                $( $(this).find('thead').find('select')[0]).change()
            }
            //console.log( col );
            //console.log( datatable )
            //console.log( datatable.teste )
        } );
        */

        this.api().columns('.select-filter').every( function () {
            var column = this;
            var header = column.header(); // th

            // se a tabela tiver o header com duas linhas, colocar os filtros na linha de cima
            var $this = $(header)
            var $tr = $this.parent();
            var col = $tr.children().index($this);
            var $headerFiltro = $tr.prev().children().eq(col);
            $headerFiltro = $headerFiltro.size() > 0 ? $headerFiltro : header;
            var thData = $(header).data();
            //$( header ).find('select.select-data-table').remove();
            //$( header ).find('span.multiselect-native-select').remove();
            $( header ).find('div.div-select-data-table').remove();
            thData.type = thData.type || 'text';
            thData.delimiter = thData.delimiter || '';
            if( header.innerHTML.trim() != '#' && header.innerHTML.trim() != 'Ação' ) {
                //var div  = $('<div style="display:block;padding:0px;margin:0px;width:100%;" class="div-select-data-table"><select multiple="multiple" data-delimiter="'+thData.delimiter+'" data-index="'+header.cellIndex+'" data-type="'+thData.type+'" class="select-data-table" style="min-width: 95% !important;max-width: 95% !important;"></select></div>').appendTo($(column.header()));
                var div  = $('<div style="display:block;padding:0px;margin:0px;width:100%;z-index:3000;" class="div-select-data-table">' +
                    '<select multiple="multiple" data-delimiter="'+thData.delimiter+'" data-index="'+header.cellIndex+'" data-type="'+thData.type+'" class="select-data-table" style="min-width: 95% !important;max-width: 95% !important;"></select></div>').appendTo( $headerFiltro );
                var select  = div.find('select.select-data-table')
                    /*.on('click',function(e){
                        e.preventDefault();
                        e.stopPropagation();
                        e.stopImmediatePropagation();
                    })*/.on('change', function (e) {
                        var data = $(this).data();
                        var val = $(this).val();
                        datatable.filters=[];
                        //data.multi=false;

                        // loop nos selects do header
                        $(e.target).closest('table').find('thead').find('select').each( function( i , el ) {
                            var $th = $(el).closest('th');
                            var data = $(el).data();
                            // só ler os filtro quen não possuirem um search customizado
                            if( ! data.custom ) {
                                $th.removeClass('red');
                                if ($(el).val()) {
                                    // marcar de vermelho colunas com filtro ativo
                                    $th.addClass('red');
                                    data.values = $(el).val();
                                    datatable.filters.push(data);
                                }
                            }
                            /*if (e.target != el) {
                                $(el).val('');
                            }
                            */
                        });


                        // chamar $.fn.dataTable.ext.search() implementado
                        column.draw(); // aplicar os filtros em todas as colunas que possuirem filtros ativos
//                        $("#divTotalRegistros span").html(column.rows( { filter : 'applied'} ).data().length+' registro(s)');

                        /*
                        if( typeof( val ) == 'string' ) {
                            if ( val ) {
                                val = $.fn.dataTable.util.escapeRegex(
                                    val
                                    //typeof( val ) == 'object' ? val.join(' ') : val
                                );
                            }
                        }
                        else if( typeof val =='object')
                        {
                            data.multi=true;
                        }

                        //$('div.dataTables_filter input').val( val );
                        // limpar os outros selects se exisiterem

                        $(e.target).closest('table').find('thead').find('select').each( function( i , el ) {
                            var $th = $(el).closest('th');
                            $th.removeClass('red');
                            if( $(el).val() )
                            {
                                filtros.push( $(el).val() );
                                $th.addClass('red');
                            }
                            // if (e.target != el) {
                            //     $(el).val('');
                            // }

                        });

                        //$('div.dataTables_filter input').val( filtros.join('|' ) );
                        datatable.updateFilters=false;
                        if( data.type =='select' || data.multi )
                        {
                            datatable.filter={value:val,columnIndex:data.index};
                            val = '';
                        }
                        //column.draw();
                        //column.search( val ? '^'+val+'&' :'',true,false).draw();
                        //column.search( val ,true,false).draw();
                        //column.search(  val ? '^'+val+'$':''  ,true,false).draw();
                        column.search(  val ? val.trim() : ''  ,true,false).draw();
                        datatable.filter={};
                        datatable.updateFilters=true;
                        //column.search( val, true, false ).draw();
                        //column.search( $.fn.DataTable.ext.type.search.select( this, this.value ), true, false ).draw();
                        */
                    });

                column.data().unique().sort().each(function (d, j) {
                    if( /^<select/.test( d.trim() ) )
                    {
                        $(d).find('option').each(function(i,option) {
                            var value = option.innerHTML.trim()
                            if( $(select).find('option[value='+value+']').length == 0 ) {
                                $(select).append('<option value="' + value + '">' + value + '</option>')
                            }
                        });
                    }
                    else {

                        // remover elementos  com class no-select-filter para não iterferirem na filtragem do conteudo
                        var wrapper = $("<div>"+d+"</div>");
                        wrapper.find('*.no-select-filter').remove();
                        d = wrapper.html();
                        //var dStripped = d.stripHTML().trim();
                        var dStripped = wrapper.text().trim();
                        if( dStripped != '') {
                            var options = [dStripped];
                            var headerData = $(header).data();
                            if (headerData.delimiter) {
                                options = dStripped.split(headerData.delimiter);
                            }
                            if (options.length > 0) {
                                options.map(function (value) {
                                    value = value.trim();
                                    if ($(select).find('option[value="' + value + '"]').size() == 0) {
                                        $(select).append('<option value="' + value + '">' + value + '</option>');
                                    }
                                });
                            }
                        }
                    }
                });
                sortSelectOptions(select,true);
            }
            else {
                $('<br>').appendTo($(column.header() ) );
            }
        } );

        var options = typeof( default_multi_select_options) != 'undefined' ? default_multi_select_options : {};
        options.numberDisplayed=1
        /*var multiSelectOPtions = {
            includeSelectAllOption: true
            ,enableFiltering: true
            ,selectAllText: 'Todos!'
            ,enableCaseInsensitiveFiltering: true
            ,filterPlaceholder: 'procurar...'
            //,includeResetOption: true
            //,includeResetDivider: true
            ,resetText: "Limpar"
            ,nonSelectedText: ''
            ,allSelectedText: 'Todos'
            ,buttonWidth: '95%'
            //,disableIfEmpty: true
        };
        */
        //$('#example-destroy').multiselect('destroy');
        $( this.api().table().container()).find('thead').find('select').multiselect(options);
        // para as tabelas com fixed head o dropdown esta ficando por baixo, por isso a position esta sendo alterada para relative
        $( this.api().table().container()).find('div.dataTables_scroll').find('ul.multiselect-container').css('position','relative');
    }
}; // end default data tables

/**
 *  metodos para ordenar colunas no formato de data dd/mm/yyyy pelo plugin dataTables
 *  https://datatables.net/examples/plug-ins/sorting_auto.html
 *  https://datatables.net/plug-ins/sorting/date-euro
 */
$.fn.dataTable.ext.type.detect.unshift(
    function ( d ) {
        d = d || '';
        d = d.trim();
        if( /^[0-9]{2}\/[0-9]{2}\/[0-9]{2,4}/.test( d ) )
            return 'date-column';
        else if( /^<select/.test( d ) )
        {
            return 'select-column'
        }
        else if( /^[0-9]/.test(sanitizeHtml(d)) )
        {
            return 'numeric-column'
        }
        return null;
    }
);
$.fn.dataTable.ext.type.order['date-column-pre'] = function ( d ) {
    d=d||'';
    d = d.split(' ')[0].split('/').reverse().join('/');
    return d
};
$.fn.dataTable.ext.type.order['select-column-pre'] = function ( d ) {
    d=d||'';
    return $(d).find('option:selected').text().trim()
};
$.fn.dataTable.ext.type.order['numeric-column-pre'] = function ( d ) {
    d=d||'';
    d = d.split('\n')[0];
    d = sanitizeHtml( d );
    return parseInt( d );
};


/**
 * métodos para filtrar pelas colunas select do datagrid
 */
$.fn.dataTable.ext.type.search.html = function ( data ) {
    data = data || '';
    data = data.replace( /<.*?>/g, '' ).trim();
    return data;
};
$.fn.dataTable.ext.type.search.string = function ( data ) {
    data = data || '';
    return data
};
/*$.fn.dataTable.ext.type.search.select = function ( select , value ) {
    var option = $(select).find('option:selected')
    return option.text()
    //value == option.text();
};
*/
/**
 * método de filtragem ao digitar no campo Localizar no gride: do plugin datatable
 */
$.fn.dataTable.ext.search.push( function (settings, searchData, index, rowData, counter) {
        var search = $('div.dataTables_filter input').val();
        var columnIndex=-1;
        var linhaValida = true;

    // se não digitou no campo search, aplicar filtros por coluna
        if( ! search )
        {
            // se não existir filtro retornar true
            if( ! settings.filters )
            {
                return linhaValida;
            }
            // fazer loop nos filtros ativos
            settings.filters.map( function(filter,i)
            {
               var coluna  = filter.index;
               var values = filter.values;
               var type   = filter.type;
               var delimiter = filter.delimiter;
               var colunaValida = false;
               //log( filter )

               // sempre o valor para pesquisar deve ser um array de valores
               if( typeof values == 'string' )
               {
                   values=[values]
               }

               // verificar se o valor atual da coluna bate com algum valor selecionado nos filtros
               values.map(function(value,i) {
                   value = sanitizeHtml(value).toLowerCase();
                   if (type == 'select') {
                       var selectValue = $(rowData[coluna]).find('option:selected').text().trim();
                       if (selectValue.toLowerCase() == value ) {
                           colunaValida = true;
                       }
                   } else {
                       if( delimiter )
                       {
                           if( rowData[coluna].trim() != '' ) {
                               rowData[coluna].split( delimiter ).forEach( function(item) {
                                   if ( sanitizeHtml(item.trim().toLowerCase()) == value ) {
                                       colunaValida = true;
                                   }
                               });
                           }
                       }
                       else {

                           var wrapper = $("<div>"+rowData[coluna].toLowerCase()+"</div>");
                           wrapper.find('*.no-select-filter').remove();
                           var d = wrapper.html();

                           if ( sanitizeHtml(d.toLowerCase() ).trim() == value.trim() ) {
                               colunaValida = true;
                           }
                       }
                   }
                });

                if( ! colunaValida )
                {
                    linhaValida = false;
                }
            });
            return linhaValida;
        }
        else {
            // remover espaços
            search = search.trim();
        }

        if( settings.filter && settings.filter.columnIndex && settings.filter.value )
        {
            columnIndex= settings.filter.columnIndex;
            if( typeof settings.filter.value =='object')
            {
                search = settings.filter.value.join('|');
            }
            else {
                search = settings.filter.value;
            }
        }
        if (search == "") {
            return true;
        } else {
            var filtros = search.split('|');
            var vf=false;
            var valido = true;
            filtros.map(function(filtro){
                if( !columnIndex ) {
                    vf = false;
                }
                rowData.map( function(item,index) {
                    if( columnIndex ==-1 || index==columnIndex ) {
                        if (!vf) {
                            item = item.trim();
                            if (item.startsWith("<input")) {
                                item = $(item).val();
                            } else if (item.startsWith("<select")) {
                                item = $(item).find('option:selected').text().trim();
                            } else if (item.startsWith("<a")) {
                                item = item.replace(/<.*?>/g, '').trim();
                            }
                            if (item.trim() == filtro.trim()) {
                                vf = true;
                            }
                            else if (item.match(new RegExp(filtro, "i")) !== null) {
                                vf = true;
                            }
                        }
                    }
                });
                if( columnIndex ) {
                    valido = vf;
                }
                else if ( !vf && valido) {
                    valido = false;
                }
            });
            return valido;
        }
    }
);
