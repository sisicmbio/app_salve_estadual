//# sourceURL=configuracao.js
var configuracao = {
    fileBrasao:null,
    init:function(){
        // exibir as imagens no formulário quando houver
        configuracao.estadoChange();
        configuracao.brasaoChange();
    },
    ajax:function( url, data, callback ) {
        $.ajax({
            url:baseUrl+'configuracao/'+url,
            data:data,
            //dataType:'json'
        }).done(function(res) {
            callback( res );
        }).error( function() {
            app.growl('Requisição ajax deu erro',null,null,'tc',null,'error')
        });
    },
    estadoChange:function(){

        var sgEstado = $("#sqEstado option:selected").data('sigla');
        if( sgEstado ){
            $("#img-estado").show();
            $("#img-estado").attr('src',baseUrl+'download?fileName=/data/salve-estadual/arquivos/bandeiras-estados/'+sgEstado.toLowerCase()+'.png');
        } else {
            $("#img-estado").attr('src','');
            $("#img-estado").hide();
        }
    },
    brasaoChange:function(e){
        var $input=$("#noArquivoBrasao");
        if( e && e.files ){
            configuracao.fileBrasao = e.files[0];
            if( configuracao.fileBrasao.type == 'image/png') {
                $("#img-brasao").attr('src', URL.createObjectURL(configuracao.fileBrasao));
                $("#img-brasao").show();
            } else {
                app.growl('Selecione uma imagem do tipo png',2,'Imagem','tc','medium','error');
                return;
            }
        } else {
            var current = $("#brasaoCurrentFileName").val();
            if( current ){
                $("#img-brasao").attr('src',baseUrl+'download?fileName=/data/salve-estadual/arquivos/brasoes-estados/'+current );
                $("#img-brasao").show();
            } else {
                $("#img-brasao").attr('src','');
                $("#img-brasao").hide();
            }
        }
    },
    save:function(){
        var formData = $("#frmConfig").serializefiles();

        var errors= [];

        if( !formData.get('sqEstado') ) {
          errors.push('Estado não selecionado');
        }
        if( !formData.get('noInstituicao') ) {
          errors.push('Nome da instituição é obrigatório');
        }

        if( !formData.get('sgInstituicao') ) {
          errors.push('Sigla da instituição é obrigatório');
        }
        if( errors.length){
            app.alertInfo(errors.join('<br>'),'Mensagem');
            return;
        }

        app.ajax(app.url + 'configuracao/save', formData,function(res){
            if( res.status == 0 ){
                window.location.href = baseUrl;
            }
        });

        /*
        var email = $("#inputEmail").val();
        var senha = $("#inputPassword").val();
        if( !email || !senha ){
            app.growl('Preencha todos os campos.',null,null,'tc',null,'error')
            $("#inputEmail").focus();
            return;
        }
        localStorage.setItem('login_email', email );
        var $form = $("#frmLoginBody")
        $form.find('input,button').prop('disabled',true);
        $form.find('i.fa-user-circle').addClass('hidden');
        $form.find('i.fa-spinner').removeClass('hidden');
        login.ajax( 'login',{email:email,senha:senha},function( res ) {
            $form.find('input,button').prop('disabled',false);
            $form.find('i.fa-user-circle').removeClass('hidden');
            $form.find('i.fa-spinner').addClass('hidden');
            if( res.msg ){
                app.growl(res.msg,null,null,'tc',null,'error')
            }
            if( res.status == 0 ) {
                window.location.href = baseUrl;
            }
        });*/
    }
}
setTimeout(function(){configuracao.init();},1000);
