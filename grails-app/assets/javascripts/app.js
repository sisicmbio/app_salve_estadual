//# sourceURL=app.js
;
/**
 * Protect window.console method calls, e.g. console is not defined on IE
 * unless dev tools are open, and IE doesn't define console.debug
 *
 * Chrome 41.0.2272.118:
 * debug,error,info,log,warn,dir,dirxml,table,trace,assert,count,markTimeline,profile,profileEnd,time,timeEnd,timeStamp,timeline,timelineEnd,group,groupCollapsed,groupEnd,clear
 * Firefox 37.0.1:
 * log,info,warn,error,exception,debug,table,trace,dir,group,groupCollapsed,groupEnd,time,timeEnd,profile,profileEnd,assert,count
 * Internet Explorer 11:
 * select,log,info,warn,error,debug,assert,time,timeEnd,timeStamp,group,groupCollapsed,groupEnd,trace,clear,dir,dirxml,count,countReset,cd
 * Safari 6.2.4:
 * debug,error,log,info,warn,clear,dir,dirxml,table,trace,assert,count,profile,profileEnd,time,timeEnd,timeStamp,group,groupCollapsed,groupEnd
 * Opera 28.0.1750.48:
 * debug,error,info,log,warn,dir,dirxml,table,trace,assert,count,markTimeline,profile,profileEnd,time,timeEnd,timeStamp,timeline,timelineEnd,group,groupCollapsed,groupEnd,clear
 */
var xpanel=null;
var methods = [
    "assert",
    "cd",
    "clear",
    "count",
    "countReset",
    "debug",
    "dir",
    "dirxml",
    "error",
    "exception",
    "group",
    "groupCollapsed",
    "groupEnd",
    "info",
    "log",
    "markTimeline",
    "profile",
    "profileEnd",
    "select",
    "table",
    "time",
    "timeEnd",
    "timeStamp",
    "timeline",
    "timelineEnd",
    "trace",
    "warn"
];

// quantidade de fichas que podem ser exportadas para planilha
var maxFichasExportacao=15000;
// quantidade de fichas que podem ser selecionadas para exportação dos registros de ocorrências
var maxFichasExportacaoOcorrencias=3000;
var currentBoostrapModal=null;
var currentUser = {nome:'',email:''}
var length = methods.length;
var console = (window.console = window.console || {});
var method;
var appName = window.grails.appName;
var jsPanels = [];
var currentPanel = null;
var lastMap;
var mapGeolocation;
var updatePercentualImportacaoRunning = false;
var mapAnimateFeatureRunning = false;
var checkingWorkers = false;
var cleaningUpHtml = false;
var tinymce;
var timeoutAlertasInformativos=null;
var noop = function() {};
var appAlertInfoRel003=false;

// configuracao padrão para o plugin multiselect

var default_multi_select_options = {
    includeSelectAllOption: true
    ,enableFiltering: false
    ,selectAllText: 'Todos!'
    ,enableCaseInsensitiveFiltering: false
    ,filterPlaceholder: ''
    //,includeResetOption: true
    //,includeResetDivider: true
    ,resetText: "Limpar"
    ,nonSelectedText: ''
    ,allSelectedText: 'Todos'
    ,buttonWidth: '95%'
    ,numberDisplayed: 3
    ,nSelectedText : 'selecionados'
    //,disableIfEmpty: true
};


// configuração padrão do editor tinymce
//var default_editor_style =  '.mce-content-body {min-height:150px !important;text-align:justify;margin:0px 10px!important;font-size:14pt !important;font-family:"Open Sans","Times New Roman", Times, serif !important;}'
var default_editor_style =  '.mce-content-body {min-height:150px !important;text-align:justify;margin:0px 10px!important;font-size:14pt !important;font-family:open_sansregular,"Open Sans","Times New Roman", Times, serif !important;}'

// configurações padrão do editor de texto rico wysiwyg
var default_editor_options = {
    mode: 'specific_textareas',
    content_style: default_editor_style,
    content_css : "/salve-estadual/assets/fonts.css",
    elementpath: false,
    menubar: false,
    statusbar: false,
    skin: "icmbio", //http://skin.tinymce.com/
    language: 'pt_BR',
    height: 200,
    //min_height: 400,
    //autoresize_min_height: 400,
    //autoresize_overflow_padding: 50
    autoresize_max_height: 400,
    autoresize_bottom_margin: 10,
    browser_spellcheck: true,
    //plugins: "textcolor,link,lists,autoresize,fullscreen,paste",
    plugins: "textcolor,link,lists,fullscreen,paste",
    toolbar: 'undo redo | bold italic underline subscript superscript | alignleft aligncenter alignright alignjustify | forecolor backcolor | bullist numlist | link unlink openlink | outdent indent | removeformat | fullscreen | selectall',
    inline: false,
    //autoresize_on_init:false,
    paste_retain_style_properties: "color background i b u",
    //paste_word_valid_elements: "b,strong,i,u,em,h1,h2,h3,h4,p",
    paste_convert_word_fake_lists: false,
    paste_merge_formats: true,
    paste_webkit_styles: "all",
    paste_preprocess: function(plugin, args) {
        //console.log('antes', args.content);
        // https://github.com/cure53/DOMPurify
        // var clean = DOMPurify.sanitize(args.content,{ALLOWED_TAGS: ['a','b', 'i', 'p','em'], ALLOWED_ATTR: ['style']});


        args.content = limparCampoTexto( args.content );

        // limpar font
        args.content = args.content.replace(/\n/g,' ');
        args.content = args.content.replace(/ ?-webkit-[a-z0-9-:'"!% ,]+(;|")/ig,'');
        args.content = args.content.replace(/(<meta[^>]*>)/ig, '');

        // tag font
        args.content = args.content.replace(/font-color:/ig,'xont-color:');
        args.content = args.content.replace(/<font color="/ig,'<xont color="');
        args.content = args.content.replace(/ ?font-[a-z0-9-:'"!% ,]+(;|")/ig,'').replace(/(<font[^>]*>)|(<\/font>)/ig, '');
        args.content = args.content.replace(/xont-color:/,'font-color:');
        args.content = args.content.replace(/<xont color="/ig,'<font color="');

        // tags de form
        args.content = args.content.replace(/(<form[^>]*>)|(<\/form>)/ig, '');
        args.content = args.content.replace(/(<input[^>]*>)|(<\/input>)/ig, '');
        args.content = args.content.replace(/(<button[^>]*>)|(<\/button>)/ig, '');
        args.content = args.content.replace(/(<select[^>]*>)|(<\/select>)/ig, '');
        args.content = args.content.replace(/(<textarea[^>]*>)|(<\/textarea>)/ig, '');
        args.content = args.content.replace(/(<iframe[^>]*>)|(<\/iframe>)/ig, '');

        // remover <h1><h2>...<hn>
        args.content = args.content.replace(/ ?<\/?h[1-7]>/g,'');
        args.content = args.content.replace(/ ?text-[a-z0-9-:'"!% ,]+(;|")/ig,'');
        args.content = args.content.replace(/ ?letter-[a-z0-9-:'"!% ,]+(;|")/ig,'');
        args.content = args.content.replace(/ ?white-[a-z0-9-:'"!% ,]+(;|")/ig,'');
        args.content = args.content.replace(/ ?(line|word|margin)-[a-z0-9-:'"!% ,]+(;|")/ig,'');
        args.content = args.content.replace(/ ?(float|widows|orphans|display):[a-z0-9-'"!% ,]+(;|")/ig,'');
        //args.content = args.content.replace(/ ?style=">/ig,'>');
        args.content = args.content.replace(/ ?style=""?>/ig,'>');
        // remover background-color
        args.content = args.content.replace(/ ?background-color: rgb\(.+?\);/ig,'')
        //console.log(' ')
        //console.log('depois', args.content);


        //log( args.content);
        //args.content = args.content.replace(/font-[a-zA-Z-]+:.+(;|"|')/ig,'').replace(/(<font[^>]*>)|(<\/font>)/ig, '');
        //args.content = args.content.replace(/line-height\:[^;]+;?|text-align\:[^;]+;?|white-space\:[^;]+;?|margin\:[^;]+;?|box-sizing\:[^;]+;?|border\:[^;]+;?|padding\:[^;]+;?|text-indent\:[^;]+;?|windows\:[^;]+;?|outline\:[^;]+;?|position\:[^;]+;?|z-índex\:[^;]+;?|<\/?h[1-7]>|<\/?strong>/g, '');
        //args.content = args.content.replace(/&nbsp;/g, ' ').replace(/> +/g,'>').trim();
        //log( args.content)
    }
};

var wallpapers = ['borboleta1.jpg', 'passaro3.jpg', 'natureza1.jpg', 'passaro1.jpg', 'passaro2.jpg'];
// $('html').css('background','rgba(0, 0, 0, 0) url("http://localhost:8080/salve-estadual/assets/wallpapers/passaro1.jpg") no-repeat fixed 50% 50% / cover padding-box border-box');
while (length--) {
    method = methods[length];
    // define undefined methods as noops to prevent errors
    if (!console[method])
        console[method] = noop;
}
debug = (function(args) {
    window.console.debug(args);
})
info = (function(args) {
    window.console.info(args);
})
warn = (function(args) {
    window.console.warn(args);
})
log = (function(args) {
        window.console.log(args);
    })
    // camadas para openlayers
    /*
     Bing Layers
     */
var bingLayers = [
    "Road",
    "Aerial",
    "AerialWithLabels",
    "collinsBart",
    "ordnanceSurvey"
];
/*
 Camadas disponíves para exibir no openlayers
 */
var mapLayers = [];

/* configuração padrão do plugin fileInput*/
fileInputLanguage = "pt-BR";
fileInputLayoutTemplates = {
    main1: "{preview}\n" + "<div style=\"min-width:200px;\" class=\'input-group {class}\'>\n" +
        "   <div class=\'input-group-btn\'>\n" +
        "       {browse}\n" + "       {upload}\n" +
        "       {remove}\n" + "   </div>\n" +
        "   {caption}\n" + "</div>"
};
fileInputPreviewFileIconSettings = {
    'docx': '<i class="fa fa-file-word-o text-primary"></i>',
    'doc': '<i class="fa fa-file-word-o text-primary"></i>',
    'odt': '<i class="fa fa-file-word-o text-primary"></i>',
    'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
    'xls': '<i class="fa fa-file-excel-o text-success"></i>',
    'ods': '<i class="fa fa-file-excel-o text-success"></i>',
    'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
    'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
    'jpeg': '<i class="fa fa-file-photo-o text-warning"></i>',
    'gif': '<i class="fa fa-file-photo-o text-warning"></i>',
    'tiff': '<i class="fa fa-file-photo-o text-warning"></i>',
    'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
    'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
}

var app = (function() {
    // variaveis privadas
    // var _url = 'http://localhost:8080/salve-estadual/';
    //var _url = $(location).attr('href').replace(/;$/, '').replace(/^#/, '');
    var _url = window.grails.baseUrl;
    var _window = {};
    var _ajaxCount = 0;
    var _updatingDashboard = false;
    var _clearingFilters = false;
    var _tooltipErrorSeconds = 3000;
    var _inputMasksClass = {
        'cpf': {
            'mask': '000.000.000-00',
            'options': {
                selectOnFocus: true
            }
        },
        'cnpj': {
            'mask': '00.000.000/0000-00',
            'options': {
                selectOnFocus: true
            }
        },
        "date": {
            'mask': '00/00/0000',
            'options': {
                placeholder: "__/__/____",
                selectOnFocus: true
            }
        },
        'time': {
            'mask': '00:00:00',
            'options': {
                selectOnFocus: true
            }
        },
        'cep': {
            'mask': '00000-000',
            'options': {
                selectOnFocus: true
            }
        },
        'phone': {
            'mask': '0000-0000',
            'options': {
                selectOnFocus: true
            }
        },
        'phone-ddd': {
            'mask': '(00) 0000-0000',
            'options': {
                selectOnFocus: true
            }
        },
        'number1d': {
            'mask': '#.##0,9',
            'options': {reverse:true}
        },
        'money': {
            'mask': '#.###,99',
            'options': {
                reverse: true
            }
        },
        'number': {
            'mask': '0#',
            'options': {}
        },
        'numberFormated': {
            'mask': '#.##0',
            'options': {reverse:true}
        },
        'decimal': {
            'mask': '#.###,99999999',
            'options': {
                reverse: true,
            }
        },
        'coordenada': {
            'mask': 'snnn.00000000',
            'options': {
                'translation': {
                    s: {
                        pattern: /[\-]/,
                        optional: true
                    },
                    n: {
                        pattern: /[0-9]/,
                        optional: true
                    }
                }
            }
        },
        'ip-address': {
            'mask': '099.099.099.099'
        }
    };
    /**
     * função para inicializar a aplicação
     */
    var _run = function() {

        // inicalizar timeout de verificação de novos informativos 10s
        timeoutAlertasInformativos = window.setTimeout( app.verificarInformativoAlerta ,10000);

        _captureClicks(); // adicionar e processar o evento click nas abas e botoes dinâmicamente
        _loadMainMenu(); // construir o menu principal

        // colocar background na pagina
        //$("html").css('background-image','url("images/wallpapers/bg5.png")');

        (function($) {
            $.fn.serializefiles = function() {
                var obj = $(this);
                /* ADD FILE TO PARAM AJAX */
                var formData = new FormData();
                $.each($(obj).find("input[type='file']"), function(i, tag) {
                    $.each($(tag)[0].files, function(i, file) {
                        formData.append(tag.name, file);
                    });
                });
                var params = $(obj).serializeArray();
                $.each(params, function(i, val) {
                    formData.append(val.name, val.value);
                });
                return formData;
            };

            $.fn.serializeAllArray = function () {
                var obj = {};
                $('input', this).each(function () {
                    var name = this.name ? this.name : $(this).attr('id');
                    if ( name ) {
                        if( this.type.toLowerCase() == 'checkbox' || this.type.toLowerCase() == 'radio')
                        {
                            obj[name] = this.checked ? $(this).val():'';
                        }
                        else {
                            obj[name] = $(this).val();
                        }
                    }
                });
                $('select', this).each(function () {
                    var name = this.name ? this.name : $(this).attr('id');
                    if (name) {
                        obj[name] = $(this).val();
                    }
                });
                $('textarea', this).each(function () {
                    var name = this.name ? this.name : $(this).attr('id');
                    if (name) {
                        obj[name] = $(this).val();
                    }
                });
                return obj;
            };

            // metodo para centralizar elemento na tela
            $.fn.center = function() {
                var windowHeight = window.innerHeight
                var elementHeight = $(this).outerHeight();
                this.css("position", "absolute");
                this.css("top", Math.max(0, ( windowHeight - elementHeight) / 2 ) );
                this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) + $(window).scrollLeft()) + "px");
                return this;
            };

            // callback onClose do jsPanel
            /*$(document).on('jspanelbeforeclose', function(event, id) {
                var o = jsPanels.filter(function(obj, i) {
                    return obj.id == id;
                });
                if (o.length > 0) {
                    if (o[0].panel.option.onBeforeClose) {
                        app.eval(o[0].panel.option.onBeforeClose);
                    }
                }
                jsPanels = jsPanels.filter(function(obj, i) {
                    return obj.id != id
                });
            });
            */
            // callback onClose do jsPanel
            $(document).on('jspanelclosed', function(event, id) {
            //$(document).on('jspanelbeforeclose', function(event, id) {
                var o = jsPanels.filter(function(obj, i) {
                    return obj.id == id;
                });
                /*if (o.length > 0) {
                    if (o[0].panel.option.onClose) {
                        //app.eval(o[0].panel.option.onClose);
                        //return false;
                    }
                }
                */
                jsPanels = jsPanels.filter(function(obj) {
                    return obj.id != id
                });
            });
        })(jQuery);
        // escutar as teclas pressionadas e tratar a tecla Enter
        $(document).keypress(function(e) {
            if (e.which == 13) {
                if (e.target && e.target.tagName == 'INPUT') {
                    e.preventDefault(); // evitar que a tecla enter submeta o formulário
                }
                if (e.target && $(e.target).data('enter')) {
                    //e.stopPropagation();
                    //e.stopImmediatePropagation();
                    return _eval($(e.target).data('enter'), e);
                }
            }
        });
        // alterar a cor da aba ao editar algum campo
        $(document).keyup(function(e) {
            if (e.originalEvent && e.originalEvent.target) {
                app.fieldChange(e.originalEvent.target);
            } else if (e.target) {
                app.fieldChange(e.target);
            }
        });
        /********************************************/
        // traduzir jquery validator
        jQuery.extend(jQuery.validator.messages, {
            required: "Campo obrigatório.",
            remote: "Campo inválido.",
            email: "Email inválido.",
            url: "URL inválida.",
            date: "Data inválida.",
            dateISO: "Data inválida (ISO).",
            number: "Número inválido.",
            digits: "Dígitos inválidos.",
            equalTo: "Valores são diferentes.",
            accept: "Extensão de arquivo inválida.",
            maxlength: jQuery.validator.format("Máximo de {0} caracteres"),
            minlength: jQuery.validator.format("Mínimo de {0} caracteres."),
            rangelength: jQuery.validator.format(
                "Número de caracteres deve ser de {0} a {1}."),
            range: jQuery.validator.format("Valor deve estar entre {0} e {1}."),
            max: jQuery.validator.format("Valor deve ser menor ou igual a {0}."),
            min: jQuery.validator.format("Valor deve ser maior ou igual a {0}.")
        });
        // adicionar regras de validação para campos cpf e cnpj / ex de uso no
        // elemento html: data-rule-cpf="true"
        jQuery.validator.addMethod("cpf", function(value, element, params) {
            value = value.replace(/[^0-9]/g, '');
            var valid = String(value).isCPF() || this.optional(element);
            var onValid = $(element).data('cpf-valid');
            if (valid && onValid) {
                _eval(onValid, [value, element, params]);
                /*
                 * try { onValid = eval(onValid); } catch (e) { onValid = null; } ; if
                 * (typeof (onValid) === 'function') { try { onValid(value, element,
                 * params); } catch (e) { } }
                 */
            }
            return valid;
        }, $.validator.format("CPF está incorreto."));
        // validador de cnpj. Ex: html: data-rule-cnpj="true"
        jQuery.validator.addMethod("cnpj", function(value, element, params) {
            value = value.replace(/[^0-9]/g, '');
            var valid = String(value).isCNPJ() || this.optional(element);
            var onValid = $(element).data('cnpj-valid');
            if (valid && onValid) {
                _eval(onValid, [value, element, params]);
                /*
                 * try { onValid = eval(onValid); } catch (e) { onValid = null; } ; if
                 * (typeof (onValid) === 'function') { try { onValid(value, element,
                 * params); } catch (e) { } }
                 */
            }
            return valid;
        }, "CNPJ está incorreto.");
        jQuery.validator.addMethod("date", function(value, element) {
            return this.optional(element) || /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/.test(value);
        }, "Data Inválida");
        /**
         * Configurar a exibição do tooltip baseado nos parametros data-tooltipster
         * do elemento.
         * Exemplo: <i class="label-help glyphicon glyphicon-question-sign"
         * data-tooltipster='{"side":"top","position":"left","theme":"light"}'
         * title="Exemplo tooltipster"></i>
         */
        $.tooltipster.on('init', function(event) {
            var $origin = $(event.origin);
            var dataOptions = $origin.attr('data-tooltipster');
            if (dataOptions) {
                try {
                    dataOptions = JSON.parse(dataOptions);
                    $.each(dataOptions, function(name, option) {
                        event.instance.option(name, option);
                    });
                } catch (e) {
                    dataOptions = null;
                }
            }
        });
        // transformar somente primeira letra para minuscula
        String.prototype.lcFirst = function() {
            return this.charAt(0).toLowerCase() + this.slice(1);
        };
        // transformar somente a primeira letra para maiúscula
        String.prototype.ucFirst = function() {
            return this.charAt(0).toUpperCase() + this.slice(1);
        };
        // adicionar o métod capitalize na classe String
        String.prototype.capitalize = function() {
            return this.replace(/(^|\s)([a-z])/g, function(m, p1, p2) {
                return p1 + p2.toUpperCase();
            });
        };
        // completar uma string a esquerda
        String.prototype.padLeft = function(padString, length) {
            var str = this;
            while (str.length < length)
                str = padString + str;
            return str;
        };
        // completar uma string a direita
        String.prototype.padRight = function(padString, length) {
            var str = this;
            while (str.length < length)
                str = str + padString;
            return str;
        };
        if (typeof String.prototype.toCamel !== 'function') {
            String.prototype.toCamel = function() {
                return this.replace(/[-_]([a-z])/g, function(g) {
                    return g[1].toUpperCase();
                })
            };
        };
        if (typeof String.prototype.fromCamel !== 'function') {
            String.prototype.fromCamel = function() {
                return this.replace(/(^[a-z]+)|[0-9]+|[A-Z][a-z]+|[A-Z]+(?=[A-Z][a-z]|[0-9])/g, function(match, first) {
                    if (first) match = match[0].toUpperCase() + match.substr(1);
                    return match + ' ';
                }).trim().toLowerCase().replace(/ /g, '-')
            };
        };
        // evitar botão voltar do browser
        //window.onbeforeunload = function() { return "É possível que as alterações feitas sejam perdidas."; };

        /*(function (global) {

            if(typeof (global) === "undefined")
            {
                throw new Error("window is undefined");
            }

            var _hash = "!";
            var noBackPlease = function () {
                global.location.href += "#";

                // making sure we have the fruit available for juice....
                // 50 milliseconds for just once do not cost much (^__^)
                global.setTimeout(function () {
                    global.location.href += "!";
                }, 50);
            };

            // Earlier we had setInerval here....
            global.onhashchange = function () {
                if (global.location.hash !== _hash) {
                    global.location.hash = _hash;
                }
            };

            global.onload = function () {

                noBackPlease();

                // disables backspace on page except on input fields and textarea..
                document.body.onkeydown = function (e) {
                    var elm = e.target.nodeName.toLowerCase();
                    if (e.which === 8 && (elm !== 'input' && elm  !== 'textarea')) {
                        e.preventDefault();
                    }
                    // stopping event bubbling up the DOM tree..
                    e.stopPropagation();
                };

            };
        })(window);
        */

        // evitar botao voltar do browser
        /*history.pushState(null, null, document.URL);
        window.addEventListener('popstate', function () {
            if( ! document.URL.match(/#/) ) {
                //app.alertInfo('Ops!!!\nDevido a tecnologia de carregamento dinâmico dos formulários do SALVE, não existe navegação entre páginas, apenas o conteúdo central que é atualizado, assim não existe histórico de navegação e por isso o botão VOLTAR está desligado.');
                app.alertInfo('Ops!!!\nBotão voltar está desabilitado para evitar a perda de dados.');
            }
            else
            {
                try {
                    var link = $('#' + document.URL.replace(/.+#/, ''));
                    if ( link.size() == 1 ) {
                        var top = link.offset().top;
                        if (top > -1) {
                            // encontrar o proximo elemento com barra de roagem
                            var parentContainer = link.parent();
                            var times=100;
                            while( parentContainer[0].tagName != 'HTML' && times > 0 && parentContainer.parent().size() == 1 && parentContainer.scrollTop() == 0 )
                            {
                                parentContainer = parentContainer.parent();
                                times --;
                            };
                            window.setTimeout(function () {
                                    var headerHeight = ( parentContainer[0].tagName == 'HTML' ? 102 : 1 );
                                    parentContainer.scrollTop( top - headerHeight );
                            }, 500);
                        }
                    }
                } catch( e ) {}
            };
            document.URL = baseUrl;
            history.pushState(null, null, baseUrl );
        });
        */

        // evitar botao voltar do browser
        history.pushState(null, null, document.URL);
        window.addEventListener('popstate', function () {
            if( ! document.URL.match(/#/g) )
            {
                app.alertInfo('Ops!!!\nBotão voltar desabilitado. Utilize o menu do SALVE para navegar entre os módulos.');
            }
            else {
                if( ! document.URL.match(/#!$/) ) {
                    window.setTimeout(function(){document.location.replace(baseUrl.replace(/#!$/, '') + '#!');},1000);
                }
            }
            document.URL = baseUrl;
            history.pushState(null, null, document.URL);
            //app.alertInfo('Ops!!!\nDevido a tecnologia de carregamento dinâmico dos formulários do SALVE, não existe navegação entre páginas, apenas o conteúdo central que é atualizado, assim não existe histórico de navegação e por isso o botão VOLTAR está desligado.');
        });

        // verficar workers
        getWorkers()

    };
    // --------------------------------------------------------------------------------------
    /* Função para interromper a execução */
    var _sleep = function(sleepSeconds, callback) {
        sleepSeconds = sleepSeconds || 1;
        var now = new Date().getTime();
        while (new Date().getTime() < now + sleepSeconds) {}
        try {
            callback && callback();
        } catch( e ) { console.log( e.message ) }
        /*if (typeof callback == 'function') {
            try {
                callback.replace(/;$/, '');
                callback.call();
            } catch (e) {};
        };*/
    };
    /**
     * Captura o evento clique dos botões/links adicionados ao body e executa a
     * função definda no atributo data-action
     * Parametros extras que serão processados no evento:
     * data-form: serializa o form e envia na variavel data
     * data-url: carrega o conteudo da url via ajax no data-container informado
     * data-params: ainda não implementado
     */
    var _captureClicks = function(e) {

        // eventos válidos data-action, data-click, data-change
        var arrEvents = ['action', 'change'];
        var _processEvent = function(e, evtName) {
                if (!e) {
                    return;
                }
                // verificar se o evento ocorreu em um botão ou link que deve ser tratado
                var target = null;
                var data = {};
                for (var i in arrEvents) {
                    if ($(e.target).data(arrEvents[i])) {
                        target = e.target;
                    } else if ($(e.target).parent().data(arrEvents[i])) {
                        target = $(e.target).parent();
                    }
                    if (e.target.type == 'button' || e.target.tagName == 'BUTTON') {
                        e.preventDefault(); // evitar o click do button seja propagado e submeta o formulário
                    }
                    if (target) {
                        // todos os eventos serão tratados como data.action
                        $(target).data('action', $(target).data(arrEvents[i]));
                    }
                }
                // cancelar click nos campos link sem href
                if (target.tagName === 'A' &&
                    (!target.href || /;$/.test(target.href) || target.href === _url)) {
                    try {
                        e.stopPropagation();
                    } catch (e) {}
                    try {
                        e.preventDefault();
                    } catch (e) {}
                    try {
                        e.stopImmediatePropagation();
                    } catch (e) {}
                }
                if (target) {
                    // se o botão ou o link estiver desabilitado,
                    // cancelar o click;
                    if ($(target).hasClass("disabled") ||
                        $(target).parent().hasClass("disabled")) {
                        e.preventDefault();
                        try {
                            e.stopImmediatePropagation();
                        } catch (e) {}
                        return;
                    }
                    data = $(target).data();
                    // se for passado o data-form, serializar o form e mandar junto com o
                    // data. Os campos do form devem ter o atributo name definido
                    if (data.form) {
                        try {
                            var formData = $("#" + data.form).serializeArray();
                            // limpar todos os atributos relacionados aos campos do formulário
                            formData.map(function(k, v) {
                                data[k.name] = '';
                            });
                            // adicionar os campos do formulário no parametro data
                            formData.map(function(k, v) {
                                if (data[k.name]) {
                                    data[k.name] += ',' + k.value;
                                } else {
                                    data[k.name] = k.value;
                                }
                            });
                        } catch (e) {
                            warn(e);
                        }
                    };
                    // se for passado o data-params, ler os valores e adicionar na variavel
                    // data
                    // O data-params pode ser em dois formatos:
                    // a) valor fixo: nome:joao,cep:71000000
                    // b) valor dinâmico lido diretamente dos campos da tela: nome,endereco
                    if (data.params) {
                        try {
                            var param = data.params.split(',');
                            param.map(function(k) {
                                var key, value;
                                if (k.indexOf(':') != -1) // se for passado nome:joao então assumir o valor passado
                                {
                                    var aTemp = k.split(':');
                                    key = aTemp[0];
                                    value = aTemp[1];
                                } else // ler o valor via jquery
                                {
                                    key = k;
                                    if (key.indexOf('|') > 0) {
                                        key = key.split('|')
                                        k = key[0]
                                        key = key[1]
                                    }

                                    var container = "";
                                    value = $(container+"#" + k).val();

                                    if ( value=='' && data.container )
                                    {
                                        container = '#' + data.container.replace(/^#/,'')+' ';
                                        value = $(container+"#" + k).val();
                                    }

                                    if( $( container + "#" + k).length > 0 ) {
                                        if ($(container + "#" + k).prop('type') == 'checkbox') {
                                            if (!$(container + "#" + k).prop('checked')) {
                                                value = value.toUpperCase() == 'S' ? 'N' : (value.toLowerCase() == 'true' ? 'false' : value);
                                            }
                                        }
                                        if (!value) {
                                            try {
                                                if ($(container + "#" + k).prop('tagName').toUpperCase() != 'SELECT') {
                                                    value = String($(container + "#" + k).text()).trim();
                                                }
                                            } catch (e) {
                                                value = String($(container + "#" + k).text()).trim();
                                            }
                                        }
                                    }
                                }
                                data[key] = value;
                            })
                        } catch (e) {
                            warn(e);
                        }
                    }
                    if (data.action) {
                        try {
                            if (data.action.indexOf('(') == -1) {
                                if (app.eval(data.action, [data, target, e]) === false) {
                                    try {
                                        e.stopPropagation();
                                    } catch (e) {}
                                    try {
                                        e.preventDefault();
                                    } catch (e) {}
                                    try {
                                        e.stopImmediatePropagation();
                                    } catch (e) {}
                                    return;
                                }
                            } else {
                                if (app.eval(data.action) === false) {
                                    try {
                                        e.stopPropagation();
                                    } catch (e) {}
                                    try {
                                        e.preventDefault();
                                    } catch (e) {}
                                    try {
                                        e.stopImmediatePropagation();
                                    } catch (e) {}
                                    return;
                                }
                            }
                        } catch (error) {
                            try {
                                e.stopPropagation();
                            } catch (e) {}
                            try {
                                e.preventDefault();
                            } catch (e) {}
                            try {
                                e.stopImmediatePropagation();
                            } catch (e) {}
                            app.alertError('Evento <b>' + data.action +
                                '</b> não definido ou <b>' + error.message + '</b>');
                            return;
                        }
                    }
                    // ler o conteudo ajax se houver data.url no elemento
                    if (data.url) {
                        if (!data.loaded || !$.trim($('#' + data.container).html())) {
                            // evitar que o conteudo ajax seja
                            // injetado novamente
                            if (!data.reload) {
                                $(target).data('loaded', true);
                            }
                            _loadModule(data.url, data, data.container);
                        }
                    }
                }
            };
            // ativar o listener para capturar e adicionar os eventos nos elementos
            // da página e nos injetados via ajax
        $('body').on('change',
            'select[data-change], input[data-change], textarea[data-change]',
            function(e) {
                _processEvent(e, 'change');
            });
        $('body').on('click', 'button[data-action], a[data-action], i[data-action], input[type=checkbox][data-action]',
            function(e) {
                _processEvent(e, 'click');
            });
        $('body').on('click', 'i.inline-edit',
            function(e) {
                var $i = $(e.target);
                var $textElement = $( $i.parent().find('*.inline-edit-content') )
                if( $textElement.size() == 1 ) {
                    var text = $textElement.text();
                    $i.hide();
                    $textElement.hide();
                    $i.after('<p class="text-nowrap"><input type="text" class="input-inline-edit" onKeyUp="inlineEditKeyUp(this)" value="' + text + '" style="width:95%">&nbsp;<i class="fa fa-floppy-o blue cursor-pointer" title="Gravar" onClick="saveInlineEditContent(this)"></i></p>');
                    $i.parent().find('input').focus();

                }

            });
    };
    // --------------------------------------------------------------------------------------
    /**
     * Função para executar chamadas ajax
     *
     * @param String strUrl - A string containing the URL to which the request is
     * sent.
     * @param Json jsonData - Data to be sent to the server.
     * @param Function fncCallback - callback function.
     * @param String strLoadingMessage - if set, the browser will be locked during
     * the request
     * @param String strReturnType - The type of data that you're expecting back
     * from the server. Defalt: json
     * @param String strMethod - The HTTP method to use for the request (e.g.
     * "POST", "GET" ): Default: POST
     * @param Function fncAways - callback function always called
     * @param Function fncError - callback function if error occur
     * @param Boolean boolCache - If set to false, it will force requested pages
     * not to be cached by the browser. Default: true
     * @return null
     */
    var _ajax = function(strUrl, objData, fncCallback, strLoadingMessage,
        strReturnType, strMethod, fncAlways, fncError,
        boolCache, boolProcessData, strContentType) {
        var isFormData = false;
        // quando tem anexo no formulário tem que definir estes valores
        if (objData instanceof FormData) {
            isFormData = true;
            if (!boolProcessData) {
                boolProcessData = false;
            }
            if (!strContentType) {
                strContentType = false;
            }
        } else {
            boolProcessData = boolProcessData === false ? false : true;
            strContentType = strContentType ? strContentType : 'application/x-www-form-urlencoded; charset=UTF-8';
        }
        // definir valores padrão
        objData = objData || {};
        objData.container = objData.container || '';
        boolCache = boolCache === false ? false : true;
        strMethod = String(strMethod).toUpperCase() === 'GET' ? 'GET' : 'POST';
        strReturnType = String(strReturnType).toLowerCase() === '' ? 'text' : strReturnType;
        if (!strUrl) {
            return;
        }
        // evitar chamadas recursivas
        if (objData.ajax === true || !strUrl) {
            return;
        }
        boolAsync = true; // sempre assyncrono
        try {
            // se não começar com http: deve começar com /salve-estadual/ e adicionar o
            // caminho completo da url da aplicação
            if (!/^https?:/.test(strUrl)) {
                // senão iniciar com /, adicionar a /
                if (!/\//.test(strUrl)) {
                    strUrl = '/' + strUrl;
                }
                // remover /salve-estadual/ do início da url porque a _url já tem
                //strUrl = _url + strUrl.replace(/^\/salve\//, '');
                strUrl = _url + strUrl.replace(new RegExp('^\/' + appName + '\/'), '');
                // remover as barras duplas da url //
                strUrl = strUrl.replace(/\/\//g, '/');
                if (strUrl.indexOf('/' + appName) === 0) {
                    strUrl = baseUrl + strUrl.substring(appName.length + 2);
                }
            }
            // console.log( strUrl );
            // alguns elementos controlados pelo bootstrap possuem no attributo
            // data
            // objetos que devem ser eliminados dos parametros que serão
            // enviados
            // via ajax
            var filterData = {};
            if (!isFormData) {
                for (key in objData) {
                    if (!/tooltipster|\./.test(key)) {
                        if (key === 'params') {
                            filterData = _params2data(objData[key], filterData, objData.container);
                        } else if (typeof(objData[key]) == 'object') {
                            try {
                                // aceitar o formato {'name':'sq_x','value':'xxxx'}
                                if (objData[key] && objData[key].hasOwnProperty('name')) {
                                    // tratar campos checkbox com name no formato xxx[x]
                                    var fieldName = objData[key]['name'];
                                    var fieldValue = objData[key]['value'];
                                    if (fieldName.indexOf('[') > -1) {
                                        fieldName = fieldName.substring(0, fieldName.indexOf('['));
                                        if (!filterData[fieldName]) {
                                            filterData[fieldName] = [];
                                        }
                                        if (filterData[fieldName].indexOf(fieldValue) == -1) {
                                            filterData[fieldName].push(fieldValue);
                                        }
                                    } else {
                                        filterData[objData[key]['name']] = objData[key]['value'];
                                    }
                                } else if (objData[key] instanceof Array) {
                                    filterData[key] = objData[key];
                                } else {
                                    // aceitar o formato {'asdfas':'yyyyy'}
                                    if (!isNaN(key)) {
                                        filterData = $.extend(filterData, objData[key]);
                                    }
                                }
                            } catch (e) {
                                warn(e);
                            }
                        } else {
                            if (key.indexOf('[') > -1) {
                                var fieldName = key.substring(0, key.indexOf('['));
                                var fieldValue = objData[key];
                                if (!filterData[fieldName]) {
                                    filterData[fieldName] = [];
                                }
                                if (filterData[fieldName].indexOf(fieldValue) == -1) {
                                    filterData[fieldName].push(fieldValue);
                                }
                            } else {
                                filterData[key] = objData[key];
                            }
                        }
                    }
                }
                objData = filterData;
            }
            filterData = null;
            objData.ajax = true; // flag de controle para evitar loop infinito e utilizar a função ajax do jquery
            $.ajax({
                url: strUrl,
                type: strMethod,
                async: boolAsync,
                cache: boolCache,
                dataType: strReturnType,
                data: objData,
                processData: boolProcessData,
                contentType: strContentType,
                //headers: { "Accept-Encoding" : "gzip" },
                statusCode: {
                    404: function() {
                        // app.alertError("página não encontrada:\n\n" +
                        // strUrl);
                    }
                },
                beforeSend: function() {
                    _ajaxCount++;
                    $("#appSpinner").show();
                    if (strLoadingMessage && _ajaxCount == 1) {
                        _blockUI(strLoadingMessage)
                    }
                },
                complete: function() {
                    _ajaxCount--;
                    if (_ajaxCount < 1) {
                        _ajaxCount = 0;
                        $("#appSpinner").hide();
                    }
                    if (_ajaxCount == 0) {
                        _unblockUI();
                    }
                },
            }).done(function(res) {
                _ajaxCount--;
                if (_ajaxCount < 1) {
                    _ajaxCount = 0;
                    $("#appSpinner").hide();
                }
                if (strLoadingMessage && _ajaxCount == 0) {
                    _unblockUI();
                }

                // novo
                /*
                // testar melhor isso.
                try {
                    if ( typeof (res == 'object') && res.data && res.data.restoreSession && grails.sisicmbio) {
                        restoreSession(res);
                        return;
                    }
                } catch( e ){}
                */

                if (res && res.msg && res.type) {
                    var qtdLinhas = Math.max( ( res.msg.toLowerCase().replace(/^<br\/?>/,'').match(/br\/?>/g) || []).length, 0 );
                    var segundos = (res.msg.toUpperCase().indexOf('SUCESSO') > -1 ? 1 : 5);
                     segundos = (res.msg.toUpperCase().indexOf('ALERTA') > -1 ? 60 : segundos);
                     segundos = (res.msg.toUpperCase().indexOf('AVISO') > -1 ? 15 : segundos);
                     // adicionar 3 segundos por linha
                     segundos += ( 4 * qtdLinhas);
                     if( qtdLinhas > 2 || res.type=='modal' ) {
                         app.alertInfo(res.msg,'Mensagem')
                     } else {
                         _growl(res.msg, segundos, null, 'tc', null, res.type);
                     }
                }
                // if( /save/.test( strUrl ))
                // {
                //     app.isTabModified();
                // }
                try {
                    if (res && res.substring(0, 1) == '{') {
                        var data = $.parseJSON(res);
                        if (data && /efetuar login/i.test(data.msg)) {

                            app.alertInfo(data.msg, 'Sessão Encerrada', function () {
                                window.location.replace(baseUrl);
                            });

                            /*if( !grails.sisicmbio) {
                                app.alertInfo(data.msg, 'Sessão Encerrada', function () {
                                    window.location.replace(baseUrl);
                                });
                            } else {
                                restoreSession( res );
                            }*/
                            res = ''
                        }
                    }
                } catch (e) {}
                try {
                    fncCallback && fncCallback(res);
                } catch (e) {
                    // mensagem de erro ao palicar datacell em uma tabela vazia
                    if( typeof e.message !='undefined') {
                        if ( e.message.indexOf('_DT_CellIndex') == -1 ) {
                            warn(e);
                        }
                    }
                }
            }).error(function(xhr, status, exception) {
                var message;
                var statusErrorMap = {
                    '0': "Sistema entrou em manutenção ou você perdeu a conexão com a internet.<br>Aguarde alguns minutos e tente novamente.\n\n",
                    '400': "Server understood the request, but request content was invalid.",
                    '401': "Unauthorized access.",
                    '404': "Page not found.",
                    '403': "Forbidden resource can't be accessed.",
                    '407': "Proxy Authentication Required.",
                    '500': "Internal server error.",
                    '504': "Tempo máximo de resposta atingido.",
                    '503': "Service unavailable."
                };
                if (String(xhr.status) != '') {
                    message = statusErrorMap[xhr.status];
                    if (!message) {
                        message = (xhr.responseText ? xhr.responseText :"Unknown Error.") + " \nStatus: " + xhr.status +
                            '\nException:' + String(exception);
                        if (xhr.status == '200' &&
                            String(exception).indexOf('Unexpected token A in JSON at position 0')) {
                            message +=
                                '\n\n<span style="color:red;font-weight:bold;">Provavelmente a ação não está mapeada corretamente ou<br/>o retorno da ação não está no formato JSON.</span>';
                        }
                    };
                } else if (exception == 'parsererror') {
                    message = "Error.\nParsing JSON Request failed.";
                } else if (exception == 'timeout') {
                    message = "Request Time out.";
                } else if (exception == 'abort') {
                    message = "Request was aborted by the server";
                } else {
                    message = "Unknown Error. \nStatus: " + xhr.status +
                        '\nException:' + exception;
                };
                if( String( xhr.status ) == '502' )
                {
                    try{fncError && fncError( xhr, status, exception );} catch(e) {}
                    return;
                }
                if (!fncError) {
                    _alertError(message + '\nUrl: ' + strUrl);
                } else {
                    fncError && fncError(xhr, status, exception);
                }
            }).always(function() {
                fncAlways && fncAlways();
            });
        } catch (error) {
            _alertError(error)
        };
    };
    // --------------------------------------------------------------------------------------
    /**
     * Função para carregar dinamicamente o módulo e os arquivos js do módulo na
     * pagina Ao receber o html do módulo, a função le o conteudo da div com a
     * classe .lazy-load e carrega os arquivos js especificados
     */
    var _loadModule = function(strModule, objData, container, callback, strLoadingMessage) {

        // clonar o objeto data
        var data = {};
        var cb = ''; // callback
        if (typeof objData == 'object') {
            data = jQuery.extend({}, objData);
        }

        // chamada do menu principal, sempre limpar a area de trabalho
        if (!container) {
            // criar o dashboard
            $("#appDivContainer").html('');
            $("#appDivContent").html('');
        }
        container = container || 'appDivContent';
        container = container.replace(/^#/, '');
        strLoadingMessage = ((typeof(strLoadingMessage) == 'undefined') ? 'Carregando...' + window.grails.spinner : strLoadingMessage);
        isWindow = false;
        // não é uma janelas gijgo
        if ($("#" + container + " div.modal-body").size() == 0) {
            if ($('#' + container).size() == 0) {
                $("body").append('<div id="' + container + '"></div>');
            }
            /*if (!showAnimation) {
             $("#" + container)
             .html('</br><center>Carregando...<img src="' +
             window.grails.spinnerImage + '"/></center>');
             }
             */
        } else {
            isWindow = true;
            $("#" + container + " div.modal-body").html(strLoadingMessage);
            strLoadingMessage = null;
        }
        if( /getFormFiltroFicha$/.test(strModule) )
        {
            strLoadingMessage = '';
        }
        _ajax(strModule, data, function(res) {
            // remover painel dashboard quando não for uma janela modal
            try {
                if (!data || !data.modalName) {
                    $(".dashboard").remove();
                }
            } catch( e ) {}

            // ajuste para janelas gijgo
            //se existir uma div com classe modal-body, adicionar o conteudo html dentro dela
            if (isWindow) {
                $("#" + container + " div.modal-body").addClass('hidden');
                $("#" + container + " div.modal-body").html(res);
            } else {
                $("#" + container).addClass('hidden'); // exibir somente depois de passar pelo _initSecurity()
                $("#" + container).html(res);
            }
            // extrair e carregar os scripts lazy-load da página
            strModule = strModule.replace(baseUrl, '');
            var lazyLoadFiles = $('<span>' + res + '</span>').find('div.lazy-load');
            if (lazyLoadFiles.size() === 0) {
                lazyLoadFiles = '/' + strModule;
            } else {
                lazyLoadFiles = $.trim(lazyLoadFiles.html());
                if (lazyLoadFiles.indexOf(strModule) === -1 &&
                    lazyLoadFiles.indexOf('//' + strModule) === -1) {
                    //lazyLoadFiles += $.trim('\n/' + strModule);
                }
            }
            lazyLoadFiles = lazyLoadFiles.replace(/ /g, '').split('\n');
            $.each(lazyLoadFiles, function(index, file) {
                if (file.substring(0, 1) !== '/' && file.substring(0, 4) != 'http') {
                    file = '/' + file;
                }
                if (file.substring(0, 2) !== '//') {
                    if (file.substring(0, 1) === '/') {
                        file = _url + 'javascript' + file
                    }
                    // console.log( file );
                    $.cachedScript(file).done(function(script, textStatus) {
                        //console.log( script );
                    }).fail(function(jqxhr, settings, exception) {
                        warn(settings);
                        warn(exception);
                        warn(jqxhr);
                        _alertError('Erro na função app.loadModule.\n\nArquivo: ' +
                            file + '\n\nErro: ' + exception);
                    });
                }
            });
            // inicializar tooltips de validação nos campos
            $('#' + container).find('input,select,textarea').tooltipster({
                trigger: 'custom',
                onlyOne: false,
                theme: 'tooltipster-error',
                animation: 'fade',
                timer: _tooltipErrorSeconds,
                side: 'top',
                debug:false,
                position: 'right'
            });
            window.setTimeout(function(container) {
                // aplicar mascaras nos campos ( plugin jquery.mask );
                _initMasks(container);
                // inicializar exibição dos tooltips nos elementos
                _initTooltips(container);

                // inicializar campos select com class select2
                _initSelect2(container);

                // aplicar o plugin multiselect nos campos selects com data-multiple=true
                _initMultiSelect(container);

                // desabilitar/esconder campos
                _initSecurity( container );

                // inicializar div map
                //_initMap(container);
                // inicializar editor de texto rico
                _initEditor(container);

                // adicionar onChange padrão em todos os campos selects para marcar a aba como alterada
                $("#" + container + ' select').each(function() {
                        if (typeof $(this).data('defaultValue') == 'undefined') {
                            $(this).data('default-value', this.value);
                        }
                        if (!$(this).hasClass('ignoreChange')) {
                            $(this).on('change', function() {
                                app.fieldChange(this);
                            });
                        }
                    })
                    /*$("#" + container.replace(/^#/,'') + ' select').on('change',function()
                     {
                     app.fieldChange(this);
                     })
                     */
                    // inicializar eventos de teclas nos inputs com data-enter
                    // _initKeys(container);
                    // aplicar regras de validação ao formulário ( plugin jquery.validate );
                $('#' + container).find('form').each(function() {
                    _initValidate(this);
                });

                // chamar a função callback ou onLoad recebida
                if (data) {
                    /*
                    // callback deve ser utilizado para quando houver mudança no filtro e não
                    // para quando os filtros forem carregados, para isso utilizar o parametro onLoad
                    if (data.callback) {
                        cb = data.callback;
                        data.callback = '';
                        window.setTimeout(function() {
                            //log('Executar callback:' + data.callback);
                            _eval(cb, data);
                        }, 500);
                    }*/
                    if (data.onLoad) {
                        window.setTimeout(function() {
                            _eval(data.onLoad, data);
                            try {
                                if (data.toggle == 'tab') {
                                    var liTab = $("#li" + data.container.toString().capitalize())
                                    if (liTab.size() == 1) {
                                        // posicionar a aba no topo da tela
                                        $( document).scrollTop( liTab.offset().top - 103.1875 );
                                    }
                                }
                            } catch( e ){}
                        }, 2000);
                    }
                }

                // atualizar monitor de serviços em segundo plano
                getWorkers();

            }, ( data.timeoutCallback && data.timeoutCallback > 0 ? data.timeoutCallback : 1000 ) , container);
            // inicializar campo de upload
            $("input:file").each( function() {

                $(this).fileinput({
                    language: "pt-BR",
                    layoutTemplates: {
                        main1: "{preview}\n" + "<div style=\"min-width:200px;\" class=\'input-group {class}\'>\n" +
                            "   <div class=\'input-group-btn\'>\n" +
                            "       {browse}\n" + "       {upload}\n" +
                            "       {remove}\n" + "   </div>\n" +
                            "   {caption}\n" + "</div>"
                    },
                    previewFileIconSettings: {
                        'docx': '<i class="fa fa-file-word-o text-primary"></i>',
                        'doc': '<i class="fa fa-file-word-o text-primary"></i>',
                        'odt': '<i class="fa fa-file-word-o text-primary"></i>',
                        'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
                        'xls': '<i class="fa fa-file-excel-o text-success"></i>',
                        'ods': '<i class="fa fa-file-excel-o text-success"></i>',
                        'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
                        'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
                        'jpeg': '<i class="fa fa-file-photo-o text-warning"></i>',
                        'gif': '<i class="fa fa-file-photo-o text-warning"></i>',
                        'tiff': '<i class="fa fa-file-photo-o text-warning"></i>',
                        'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
                        'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
                    },
                });



                /*$(this).on('change', function( event )
                {
                    console.log( 'file change' );
                    if( event && event.target.value )
                    {
                        app.blockUI('Lendo arquivo...');
                    }
                });
                */

                /*$(this).on('fileselect', function(event, numFiles, label ) {
                    console.log( 'file selected');
                });
                */

                /*$(this).on('filebatchselected', function(event, numFiles, label ) {
                    console.log( 'file batch selected');
                    app.unblockUI();
                });
                */
                // mostrar/esconder checkbox de seleção de imagem principal
                $(this).on('fileloaded', function(event, file, previewId, index, reader) {
                    //console.log( 'file loaded');
                    $("#frmFichaAnexo #inFichaAnexoPrincipal").parent().show();
                    if( file )
                    {
                        if( ! /\.(jpg|jpeg|bmp|png)$/.test( file.name.toLowerCase() ) )
                        {
                            $("#frmFichaAnexo #inFichaAnexoPrincipal").parent().hide();
                            $("#frmFichaAnexo #inFichaAnexoPrincipal").prop('checked',false)
                        }
                    }
                });
            });
            try {
                if (res && /^\{/.test(res)) {
                    var tmpJson = $.parseJSON(res)

                    if ( /efetuar login/i.test( tmpJson.msg ) ) {
                        app.alertInfo(tmpJson.msg);
                        /*if( !grails.sisicmbio ) {
                            app.alertInfo(tmpJson.msg);
                        } else {
                            restoreSession(res);
                            return;
                        }*/
                        res = '';
                    }
                }
            } catch (e) {}
            /*
            // verificar se existe o data-callback e chamar a função
            if (data) {
               if (data.callback) {
                    cb = data.callback;
                    data.callback = '';
                    window.setTimeout(function() {
                        //log('Executar callback:' + data.callback);
                        _eval(cb, data);
                    }, 2000);
                }
                if (data.onLoad) {
                    window.setTimeout(function() {
                        _eval(data.onLoad, data);
                    }, 2000);
                }
            }*/
            // chamar a função de callback se foi informada
            callback && callback(res);
        }, strLoadingMessage, 'HTML');
    };
    /**
     * Função para carregar o menu principal da aplicação
     */
    var _loadMainMenu = function() {
        if ($("#appDivMainMenu").get(0)) {
            _ajax(_url.replace('?', '') + 'main/menu', null, function(res) {
                $("#divAppLoading").remove();
                if( res ) {

                    $("#appDivMainMenu").html(res);
                    _updateDashboard();
                } else {
                    $("#frmLogin").css('visibility','visible');
                }


            }, null, 'HTML');
        }
    };
    // --------------------------------------------------------------------------------------
    /**
     * Função para exibir caixa de dialogo na tela utilizando o plugin
     * BootstrapDialog
     * Exemplo: app.dialog('Olá','Mensagem
     * Dialogo',5000,'info','Ok','large',function(data){console.log(
     * data);},{nome:'luis'});
     */
    var _dialog = function(msg, title, seconds, type, btnLabel, size, callback,
        data, glyphicon) {
        if( !msg || typeof msg != 'string' )
        {
            return;
        }
        // http://nakupanda.github.io/bootstrap3-dialog/#available-options
        title = title || 'Mensagem';
        type = type || 'primary'; // primary,info,success,warning,danger
        btnLabel = btnLabel || 'Fechar';
        size = size || 'normal'; // small, large, wide, small
        glyphicon = glyphicon || '';
        var timer = null;
        type = (type == 'error' ? 'danger' : type);
        msg = String(msg).replace(/^<br>/i,'');
        /*if( msg.indexOf('<h') == -1 ) {
            msg = '<h3>'+msg+'</h3>'
        };*/
        var modal = BootstrapDialog.show({
            title: title,
            message: msg,
            description: title,
            closable: false,
            closeByBackdrop: false,
            closeByKeyboard: false,
            keyboard: false,
            animate: false,
            draggable: true,
            size: 'size-' + size,
            cssClass: 'app-dialog-' + type,
            type: 'type-' + type,
            data: data,
            buttons: [{
                label: btnLabel,
                cssClass: 'btn-sm btn-' + type,
                icon: 'glyphicon ' + glyphicon,
                autospin: true,
                action: function(dialogRef) {
                    dialogRef.enableButtons(false);
                    dialogRef.setClosable(false);
                    clearTimeout(timer);
                    if (typeof callback == 'function') {
                        window.setTimeout(function() {
                            try {
                                callback && callback(data);
                                callback = null;
                            } catch (e) {}
                        }, 300)
                    }
                    dialogRef.close();
                }
            }]
        });
        var maxH = Math.max(500,window.innerHeight-200)
        $(".modal-body").css({'max-height':maxH+'px'});
        if (seconds > 0) {
            timer = window.setTimeout(function() {
                callback && callback(data);
                modal.close()
            }, seconds * (seconds > 999 ? 1 : 1000))
        }
        return modal;
    };
    // --------------------------------------------------------------------------------------
    /**
     * Função para exibir dialogo de alerta utilizando o plugin bootstrapDialog
     * Exemplo: app.alert('Está é uma mensagem de
     * exemplo','Mensagem',function(data){console.log(data);},{'nome':'luis'},1000);
     */
    var _alert = function(msg, title, callback, data, seconds) {
        // evitar abrir dois alerts ao mesmo tempo
        if( $("div.modal-header.bootstrap-dialog-draggable").size() > 0 )
        {
            return;
        }
        return _dialog(msg, title, seconds, 'primary', 'Fechar', null, callback, data);

    };
    // --------------------------------------------------------------------------------------
    /**
     * Função para exibir dialogo de alerta de erro utilizando o plugin
     * bootstrapDialog Exemplo: app.alertError('Está é uma mensagem de
     * exemplo','Mensagem',function(data){console.log(data);},{'nome':'luis'},1000);
     */
    var _alertError = function(msg, title, callback, data, seconds) {
        title = title || 'Erro';
        // evitar abrir dois alerts ao mesmo tempo
        if( $("div.modal-header.bootstrap-dialog-draggable").size() > 0 )
        {
            return;
        }
        return _dialog(msg, title, seconds, 'danger', 'Fechar', null, callback, data);
    };
    // --------------------------------------------------------------------------------------
    /**
     * Função para exibir dialogo de alerta de informação utilizando o plugin
     * bootstrapDialog Exemplo: app.alertInfo('Está é uma mensagem de
     * exemplo','Mensagem',function(data){console.log(data);},{'nome':'luis'},1000);
     */
    var _alertInfo = function(msg, title, callback, data, seconds) {
        title = title || 'Atenção';
        // evitar abrir dois alerts ao mesmo tempo
        if( $("div.modal-header.bootstrap-dialog-draggable").size() > 0 )
        {
            return;
        }
        return _dialog(msg, title, seconds, 'info', 'Fechar', null, callback, data);
    };
    // --------------------------------------------------------------------------------------
    /**
     * Função para exibir caixa de confirmação utizando o plugin BootstrapDialog
     * Exemplo: app.confirm('Tem Certeza?',function(d,data){console.log(
     * data),console.log(d);alert('ok');},null,{'nascimento':'17/07/2016'},'Vai
     * querer?','primary','Quero','Não Quero')
     * size: small, normal, large, wide, large
     */
    var _confirm = function(msg, callbackYes, callbackNo, data, title, type, labelYes, labelNo, onShow, size) {
        msg = msg || 'Confirma?';
        msg = msg.replace(/^<br>/i,'');
        size = size || BootstrapDialog.SIZE_LARGE;
        if( size == 'small') {
            size = BootstrapDialog.SIZE_SMALL;
        } else if( size == 'wide') {
            size = BootstrapDialog.SIZE_WIDE;
        } else if( size == 'large') {
            size = BootstrapDialog.SIZE_LARGE;
        } else if( size == 'normal') {
            size = BootstrapDialog.SIZE_NORMAL;
        }
        /*if( msg.indexOf('<h') == -1 ) {
            msg = '<h3>'+msg+'</h3>';
        }*/
        labelYes = labelYes || 'Sim';
        labelNo = labelNo || 'Não';
        title = title || 'Confirmação';
        type = type || 'primary'; // primary,info,success,warning,danger
        var callbackYesNo = null;
        return BootstrapDialog.confirm({
            title: title,
            size: size,
            animate: true,
            message: msg,
            closable: false,
            closeByBackdrop: false,
            closeByKeyboard: false,
            type: 'type-' + type,
            draggable: true, // <-- Default value is false
            btnCancelLabel: labelYes, // <-- Default value is 'Cancel',
            btnOKLabel: labelNo, // <-- Default value is 'OK',
            btnOKClass: 'btn-danger', // botoes invertidos para exibir o Sim
            // primeiro
            btnCancelClass: 'btn-success',
            onshown: function(dialogRef) {
                app.eval( onShow, dialogRef );
            },
            callback: function(result) {
                result = !result;
                _callbackYesNo = null;
                if (result) {
                    callbackYesNo = callbackYes;
                } else {
                    callbackYesNo = callbackNo;
                }
                if (typeof callbackYesNo === 'function') {
                    // timeou para esperar o fechamento do dialog e depois
                    // executar o callback
                    window.setTimeout(function() {
                        try {
                            callbackYesNo.call(null, result, data);
                        } catch (e) {}
                    }, 300)
                }
            }
        });
    };

    /**
     * função para exibir caixa de confirmação com possibilidade de informar o e-mail
     * @site https://nakupanda.github.io/bootstrap3-dialog/
     * @param msg
     * @param callbackYes
     * @param data
     * @returns {*}
     * @private
     */
    var _confirmWithEmail = function(msg, callbackYes, data ) {
        var inputEmail='<div class="mt10 form-group form-group-sm"><label>Receber o resultado no email abaixo <small>(opcional)</small></label><br><input id="inputEmailDlgConfirm" type="text" autofocus placeholder="" class="form-control" value="'+currentUser.email+'"></div>';
        var checkComPendencia='<div class="form-group form-group-sm"><label class="cursor-pointer" title="Imprimir a(s) pendência(s) no final da(s) ficha(s).">Imprimir a(s) pendência(s)? <input id="checkPendenciasDlgConfirm" type="checkbox" class="checkbox-lg" value="S"></label></div>';
        var radioExportacaoShp='<div class="form-group form-group-sm">' +
            '<label class="cursor-pointer" title="Exportar somente os arquivos marcados como Mapa de Distribuição Atual."><input type="radio" checked class="radio-lg" value="A" name="radioExportShp">&nbsp;&nbsp;Somente os shapefiles marcados como atuais?</label>'+
            '<br>'+
            '<label class="cursor-pointer" title="Exportar todos os arquivos marcados e não marcados como Mapa de Distribuição Atual."><input type="radio" class="radio-lg" value="T" name="radioExportShp">&nbsp;&nbsp;Todos os shapefiles?</label>'+
            '</div>';
        var inputs = inputEmail
        if( data.format == 'pdf' ) {
            inputs += checkComPendencia
        } else if( data.format == 'shp' ) {
            inputs += radioExportacaoShp
        }
        return app.confirm(msg + inputs ,function(result) {
            currentUser.email = String($("#inputEmailDlgConfirm").val()).trim();
            if( String( currentUser.email ) == 'undefined' ){
                currentUser.email = '';
            }
            if( ! String( currentUser.email ).isEmail() ){
                BootstrapDialog.alert({title:'Atenção!',message:'Email inválido: <b>'+currentUser.email+'</b>', callback:function() {
                     app.confirmWithEmail(msg,callbackYes,data);
                   }
                });
            } else {
                data.dlgConfirm = true;
                data.email = currentUser.email;
                data.pendencias = $("#checkPendenciasDlgConfirm").is(":checked") ? 'S' : 'N';
                if( $("div.bootstrap-dialog-body input[name=radioExportShp]").size() > 0 ) {
                    data.shapefiles = $("div.bootstrap-dialog-body input[name=radioExportShp]:checked").val()
                }
                app.eval(callbackYes,data);
            }
        },null,null,'Exportação de Dados',null,null,null,function(){ // onshow
            app.focus("#inputEmailDlgConfirm");
        });
    }
    // --------------------------------------------------------------------------------------
    /**
     * função para bloquear um elemento da página utilizando o plugin msg
     * Exemplo: app.blockUI('Aguarde...');
     */
    var _blockUI = function(msg, onBlock, onUnblock, seconds, zIndex) {
        seconds = seconds || 0;
        zIndex = zIndex||5000;
        return $.msg({
            content: msg,
            bgPath: 'assets/jquery/plugins/msg/',
            center: {
                against: 'parent'
            },
            z: zIndex,
            timeOut: seconds * (seconds > 999 ? 1 : 1000),
            fadeOut: 200,
            fadeIn: 200,
            clickUnblock: false,
            autoUnblock: (seconds > 0),
            beforeUnblock: function() {
                onUnblock && onUnblock();
            },
            afterBlock: function() {
                setTimeout( function() {
                    onBlock && onBlock();
                },1000);
            }
        });
    };
    // --------------------------------------------------------------------------------------
    /**
     * função para desbloquear um elemento da página utilizando o plugin msg
     * Exemplo: app.unblockUI();
     */
    var _unblockUI = function() {
        return $.msg('unblock');
    };
    // --------------------------------------------------------------------------------------
    /**
     * função para bloquear um elemento da página utilizando o plugin
     * jquery.blockUI
     */
    var _blockElement = function(id, message, seconds, onBlock, onUnblock) {
        var eleId;
        if ( id ) {
            message = message || '';
            seconds = seconds || 0;
            if( typeof( id ) == 'string') {
                eleId = id;
            }
            else
            {
                eleId = $(id).attr('id');
            }
            if( ! eleId )
            {
                return;
            }
            $("#" + eleId.replace(/^#/, '') ).block({
                message: message,
                timeout: seconds,
                baseZ: 2000,
                bindEvents: false,
                onBlock: onBlock,
                onUnblock: onUnblock,
                overlayCSS: {
                    backgroundColor: '#000',
                    opacity: 0.3,
                    cursor: 'wait',
                },
                css: {
                    'font-weight': 'bold',
                    color: '#000',
                    border: '1px solid #000',
                    padding: '10px',
                    backgroundColor: '#fff',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    'border-radius': '10px',
                    opacity: 0.9,
                },
            });
        }
    };
    // --------------------------------------------------------------------------------------
    /**
     * função para desbloquear um elemento da página utilizando o plugin
     * jquery.blockUI
     */
    var _unblockElement = function( id ) {
        if( typeof( id ) == 'string') {
            elementId = id;
        }
        else
        {
            elementId = $(id).attr('id');
        }
        if( ! elementId ) {
            return;
        }
        if ( elementId ) {
            elementId = '#' + elementId.replace(/^#/, '');
            $( elementId ).unblock();
        }
    };
    // --------------------------------------------------------------------------------------
    /**
     * Função para exibir mensagem instantânea na tela delayOnHover: while
     * hovering over the alert, prevent it from being dismissed (true | false -
     * default: true) duration: the duration (in milliseconds) for which the
     * alert is displayed (default: 3200) fixed: whether the alert should be
     * fixed rather than auto-dismissed (true | false - default: false)
     * location: the alert's position ('tl' | 'tr' | 'bl' | 'br' | 'tc' | 'bc' - default: 'tr')
     * size: the alert's size ('small' | 'medium' | 'large' - default: 'medium')
     * style: the alert's style ('default' | 'error' | 'info'| 'notice' | 'warning' - default: 'default')
     * Ex: app.growl('Teste de utilização do método growl',5,'Atenção','tc','large','',false)
     */
    var _growl = function(message, seconds, title, location, size, style, fixed,
        delayOnHover) {
        message = message || '';
        seconds = seconds || 5;
        title = title || '';
        location = location || 'tr';
        size = size || 'large';
        style = style || 'warning';
        style = (style == 'danger' ? 'error' : style );
        fixed = (fixed === true ? true : false);
        if (['success', 'default', 'info', 'error', 'notice', 'warning'].indexOf(style) == -1 ) {
            style = 'info';
        }
        delayOnHover = (delayOnHover === false ? false : true);
        if (message) {
            // $.growlUI(title, message, seconds * (seconds > 999 ? 1 : 1000));
            $.growl({
                message: String(message).replace(/\n/g,'<br>'),
                title: title,
                duration: seconds * (seconds > 999 ? 1 : 1000),
                location: location,
                size: size,
                fixed: fixed,
                style: style,
                delayOnHover: delayOnHover
            });
        };
    };
    // --------------------------------------------------------------------------------------
    /**
     * Retornar a quantidade de minutos passados
     *
     * @param integer
     *            intTime - valor no formato retornado pela função: new
     *            Date().getTime();
     * @return integer
     */
    var _calcElapsedMinutes = function(intTime) {
        return Math.round((new Date().getTime() - intTime) / 1000 / 60);
    };
    /**
     * Função para gravar dados na área de armazenamento do navegador do usuário
     * utilizando plugin jstorage
     *
     * @param string
     *            key - é o valor que será utilizado para recuperar os dados
     *            gravados
     * @Exemple lsSet(key, value[, options])
     */
    var _lsSet = function(key, value, seconds) {
        if (!$.jStorage.storageAvailable()) {
            return false;
        }
        $.jStorage.set(key, value);
        if (seconds) {
            $.jStorage.setTTL(key, seconds);
        };
        return true;
    };
    // --------------------------------------------------------------------------------------
    /**
     * Função para recuperar dados gravados da área de armazenamento do browser
     * do usuário utilizando plugin jstorage
     *
     * @param string
     *            key - é o valor que será utilizado para recuperar os dados
     *            gravados
     * @param mix
     *            value - dados que serão armazenados.
     * @Exemple lsGet(key[, default])
     */
    var _lsGet = function(key, defaultValue) {
        if (!$.jStorage.storageAvailable()) {
            return null;
        }
        return $.jStorage.get(key, defaultValue);
    };
    // --------------------------------------------------------------------------------------
    /**
     * Função para deletar dados da área de armazenamento do navegador do
     * usuário utilizando plugin jstorage
     *
     * @param string
     *            key - é o valor que será utilizado para recuperar os dados
     *            gravados
     * @Exemple lsDelete(key);
     */
    var _lsDel = function(key) {
        if (!$.jStorage.storageAvailable()) {
            return false;
        }
        $.jStorage.deleteKey(key);
        return true;
    };
    // --------------------------------------------------------------------------------------
    /**
     * Função para limpar a área de armazenamento do navegador do usuário
     * utilizando plugin jstorage
     *
     * @Exemple lsFlush();
     */
    var _lsClear = function(key) {
        if (!$.jStorage.storageAvailable()) {
            return false;
        }
        $.jStorage.flush();
        return true;
    };
    // --------------------------------------------------------------------------------------
    /**
     * Solicitar confirmação do usuário antes de encerrar o módulo
     * @param data
     * @return null
     */
    var _unloadModule =
        function(data) {
            data = data || {}; // evitar erro se a função for chamada sem o
            // parametro data
            if (data.confirmClose) {
                _confirm('Deseja fechar este módulo?',
                    function() {
                        _unloadModule(false);
                        _updateDashboard();
                    });
                return;
            };
            // $("#appDivBody").html('');
            $("#appDivContainer,#appDivContent").html('');
            _updateDashboard();
        }
        /**
         * função para habilitar/desabilitar uma, varias ou todas as abas da página
         * ou de um tablist Ex: disableTab('tab1'); // id da aba
         * disableTab('tab1,tab2'); // ids das abas disableTab('ulTabs'); // id do
         * tablist
         */
    var _disableTab =
        function(id, tf) {
            tf = tf === false ? false : true; // true ou false
            // se não for infornado o(s) ids, desabilitar todas as abas existentes
            // na
            // página
            if (!id) {
                if (tf) {
                    $("a[data-toggle=tab]").parent().addClass('disabled');
                } else {
                    $("a[data-toggle=tab]").parent().removeClass('disabled');
                }
                return;
            }
            // se for passado o id do container das abas, desabilitar somente as
            // abas
            // filhas
            if ($("#" + id).is('ul')) {
                $("#" + id).find('li').each(function() {
                    if (tf) {
                        $(this).addClass('disabled');
                    } else {
                        $(this).removeClass('disabled');
                    }
                });
                return;
            }
            if (id) {
                var ids = id.split(',');
                $.each(ids, function(k, v) {
                    v = "#" + v.replace(/^#/, '');
                    if (tf) {
                        $("a[href='" + v + "']").parent().addClass('disabled');
                    } else {
                        $("a[href='" + v + "']").parent().removeClass('disabled');
                    }
                });
            }
        }
        /**
         * função para esconder/mostrar uma, varias ou todas as abas da página
         * ou de um tablist Ex: showHideTab('tab1'); // id da aba
         * showHideTab('tab1,tab2'); // ids das abas showHideTab('ulTabs'); // id do
         * tablist
         */
    var _showHideTab = function(id, hide) {
            hide = hide === false ? false : true; // true ou false
            // se não for infornado o(s) ids, desabilitar todas as abas existentes
            // na
            // página
            if (!id) {
                if (hide) {
                    $("a[data-toggle=tab]").parent().hide();
                } else {
                    $("a[data-toggle=tab]").parent().show();
                }
                return;
            }
            // se for passado o id do container das abas, desabilitar somente as
            // abas
            // filhas
            if ($("#" + id).is('ul')) {
                $("#" + id).find('li').each(function() {
                    if (hide) {
                        $(this).hide();
                    } else {
                        $(this).show();
                    }
                });
                return;
            }
            if (id) {
                var ids = id.split(',');
                $.each(ids, function(k, v) {
                    v = "#" + v.replace(/^#/, '');
                    if (hide) {
                        $("a[href='" + v + "']").parent().hide();
                    } else {
                        $("a[href='" + v + "']").parent().show();
                    }
                });
            }
        }
        // --------------------------------------------------------------------------------------
    var _selectTab = function(tabId) {
        tabId = '#' + tabId.replace(/^#/, '');
        if ($("a[href='" + tabId + "']").size() > 0) {
            try {
                $("a[href='" + tabId + "']").click();
            } catch (e) {}
        } else {
            try {
                $(tabId + ">a").click()
            } catch (e) {}
        }
    }

    var _unselectTab = function(tabId) {
        tabId = '#' + tabId.replace(/^#/, '');
        if ($(tabId).size() == 1 && $(tabId).hasClass('active')) {
            $(tabId).removeClass('active');
            $($(tabId + '> a').attr('href')).removeClass('active');
            return true;
        }
        return false;
    }

    /**
     * Percorrer todos os inputs do container e aplicar a mascara de edicao e
     * eventos
     * Exemplo com atributo class: ( ver variavel gloal _inputMasksClass as
     * máscaras pre-definidas )
     *  <input class="cpf"/>
     *  <input class="phone"/>
     *  <input class="phone-ddd"/>
     *  <input class="money"/>
     * Exemplo com eventos:
     *  <input data-mask-change="teste.cpfChange"/>
     *  <input data-mask-keyPress="console.log(e)"/>
     *  <input data-mask-completed="alert('ok')"/>
     * Exemplo com mask:
     *  <input data-mask="999/99"/>
     *  <input data-mask="00/00/00"/>
     *  <input data-mask="AAA 000-S0S"/>
     *  <input data-mask="#.##0,00"/>
     * Composição das máscaras:
     *  A=aceita letras e numeros
     *  0=aceita somente numeros
     *  S=aceita somente letras
     *  9=aceita digito numérico opcional
     *  #=aceita padrão de repeticao
     */
    var _initMasks = function(container) {
            $('#' + container.replace(/^#/, '')).find('input').each(function() {
                var input = this;
                var options = {}; // ex: {reverse: false}
                var mask = '';
                // ler os eventos definidos no elemento
                var maskChange = $(this).data('mask-change');
                var maskCompleted = $(this).data('mask-completed');
                var maskKeyPress = $(this).data('mask-keyPress');
                var maskReverse = ($(this).data('mask-reverse') === true);
                // verificar se o elemento possui alguma classe de mascara padrão.
                // Ex: cpf, cnpj, money, phone etc..
                for (key in _inputMasksClass) {
                    if ($(this).hasClass(key)) {
                        options = _inputMasksClass[key].options;
                        mask = _inputMasksClass[key].mask;
                    }
                }
                // sobrescrever a mascara padrão
                if ($(this).data('mask')) {
                    mask = $(this).data('mask');
                }
                if (maskChange) {
                    try {
                        // remover (this) do nome da função
                        maskChange = maskChange.replace(/\( *this *\)/, '');
                        options['onChange'] = function() {
                            _eval(maskChange, input);
                        };
                    } catch (e) {}
                }
                if (maskCompleted) {
                    try {
                        // remover (this) da função
                        maskCompleted = maskCompleted.replace(/\( *this *\)/, '');
                        options['onCompleted'] = function() {
                            _eval(maskCompleted, input);
                        };
                    } catch (e) {}
                }
                if (maskKeyPress) {
                    try {
                        // remover (this) da função
                        maskKeyPress = maskKeyPress.replace(/\( *this *\)/, '');
                        options['onKeyPress'] = function() {
                            _eval(maskKeyPress, input);
                        };
                    } catch (e) {}
                }
                if (maskReverse) {
                    options['reverse'] = true;
                }
                if (mask) {
                    var input = $(this);
                    window.setTimeout(function(){
                        input.mask(mask, options);
                    },1000);
                }
            });
        }
        /**
         * Método para inicializar as validações dos campos do formulário.
         *
         * Exemplo de como utilizar os parametros no formulário
         * <form    data-validate-success="alert('formulário está ok')"
         *          data-validate-error="teste.error"
         *          data-validate-focus-cleanup="false"
         *          data-validate-debug="false">
         *
         * Exemplo de utilização das validações nos campos
         *          <input  data-rule-cpf="true"
         *                  data-cpf-valid="teste.cpfValid"
         *                  required="true"
         *                  data-msg-required="Campo obrigatório" />
         *          <input data-rule-range="[1,31]" name="dia"/>
         *
         * Para validar o formulário deve ser chamado o metodo
         * $("#id-do-form").valid() que retorna true ou false
         * e destaca os campos inválidos na tela
         */
    var _initValidate =
        function(form, rules, messages, onSuccess, onError, focusCleanup, debug) {
            if (typeof(form) == 'object') {
                form = $(form).attr('id');
            }
            if (!form) {
                return;
            }
            rules = rules || {};
            messages = messages || {};
            debug = (debug === true ? true : false);
            focusCleanup = (focusCleanup === true ? true : false);
            form = '#' + form.replace(/^#/, '');
            // o evento validate-successs deve ser utilizando quando for utilizado o
            // metodo submit do form
            var formOnSuccess = $(form).data('validate-success');
            if (formOnSuccess) {
                onSuccess = function(form) {
                    _eval(formOnSuccess, form);
                }
            };
            var formOnError = $(form).data('validate-error');
            if (formOnError) {
                onError = function(event, validator) {
                    _eval(formOnError, event, validator);
                }
            };
            var formDebugMode =
                ($(form).data('validate-debug') === true) ? true : false;
            if (formDebugMode) {
                debug = true;
            };
            var formFocusCleanup =
                ($(form).data('validate-focus-cleanup') === true) ? true : false;
            if (formFocusCleanup) {
                focusCleanup = true;
            };
            $(form).validate({
                debug: debug,
                focusCleanup: focusCleanup,
                //onfocusout: true,
                //focusinvalid :false,
                //onkeyup: false,
                errorClass: 'error',
                validClass: 'valid',
                success: function(label, element) {
                    try {
                        // http://iamceege.github.io/tooltipster/#demos
                        $(element).tooltipster('hide');
                    } catch (e) {}
                    label.addClass("valid");
                },
                errorPlacement: function(error, element) {
                    if ($(error).text()) {
                        try {
                            $(element).tooltipster('content', $(error).text());
                            $(element).tooltipster('show');
                        } catch (e) {}
                    };
                    // var name = $element.attr("name");
                    // $("#error" + name).append($error);
                },
                submitHandler: function(form) {
                    onSuccess && onSuccess(form)
                },
                invalidHandler: function(event, validator) {
                    if (onError) {
                        onError(event, validator);
                    } else {
                        // var errors = validator.numberOfInvalids();
                        // app.alertError('Existe(m) ' + errors + ' erro(s) no
                        // formulário.');
                    }
                },
                rules: rules,
                messages: messages
            });
        }
        /**
         * Inicializa a classe tooltipster nos elementos que possuirem o atributo
         * title e for
         * Exemplo:  <label for="cpf" title="Informe o cpf do perticipante">Campo
         * Cpf:</label>
         */
    var _initTooltips =
        function(container) {
            if (!container) {
                return;
            }
            // processar todos os elementos com title
            $('#' + container.replace(/^#/, '') + ' [title]').each(function() {
                var obj = $(this);
                var title = obj.attr('title');
                var tagName = obj.prop('tagName');
                var theme = obj.data('tooltip-theme');
                var side = obj.data('tooltip-side');
                var position = obj.data('tooltip-position');
                var icon = obj.data('tooltipIcon') || 'question-sign';
                side = side || 'top';
                position = position || 'top';
                theme = theme || 'tooltipster-yellow';
                // não funciona o tooltipster com option dos selects
                if( tagName.toUpperCase() != 'OPTION') {
                    if (typeof (canModify) != 'undefined' && canModify === false) {
                        if (title == 'Editar' || title == 'Alterar') {
                            title = 'Visualizar'
                            obj.prop('title', title);
                            obj.find('span.glyphicon-pencil').removeClass('glyphicon-pencil').addClass('glyphicon-zoom-in');
                        }
                    }
                    // se o hint começar com Excluir, alterar o tema para vermelho
                    if (/^Excluir/.test(title)) {
                        theme = 'tooltipster-error';
                    }
                    //theme = theme || 'tooltipster-light';
                    //theme = theme || 'tooltipster-yellow';
                    // se for a tag label adicionar dentro dela um interrogação
                    if (tagName == 'LABEL' && obj.attr('for')) {
                        // remover tooltip original do elemento e adicionar na tag <i>
                        // dentro dele
                        obj.removeAttr('title');
                        // criar elemento <i> dentro da tag
                        obj.append(
                            '&nbsp;<i class="label-help glyphicon glyphicon-' + icon + ' blue" data-tooltipster=\'{"side":"' +
                            side + '","position":"' + position + '","theme":"' + theme +
                            '"}\' title="' + title + '"></i>');
                        obj.find('i').tooltipster({
                            theme: theme,
                            side: side,
                            animation: 'grow',
                            position: position,
                            debug: false,
                            contentAsHTML: true
                        });
                    } else {
                        obj.tooltipster({
                            theme: theme,
                            side: side,
                            animation: 'grow',
                            position: position,
                            debug: false,
                            contentAsHTML: true,
                        });
                        // alterar o cursor quando os campos não forem input nem textarea
                        if (!/INPUT|TEXTAREA/.test(tagName)) {
                            obj.css('cursor', 'pointer')
                        }
                    }
                }
            });
            $("i[title]").tooltipster({debug:false});
        }
        /**
         * [description]
         * Eventos select2:
         *
         *  - change -> is fired whenever an option is selected or removed.
         *  - select2:close -> is fired whenever the dropdown is closed.
         *  - select2:closing -> is fired before this and can be prevented.
         *  - select2:open -> is fired whenever the dropdown is opened.
         *  - select2:opening -> is fired before this and can be prevented.
         *  - select2:select -> is fired whenever a result is selected.
         *  - select2:selecting -> is fired before this and can be prevented.
         *  - select2:unselect -> is fired whenever a result is unselected.
         *  - select2:unselecting -> is fired before this and can be prevented.
         *
         *  - O parâmetros s2-params pode ser passado nos seguintes formatos:
         *    1. key:value
         *    2. key|alias:value
         *    3. key|alias
         *    4. key
         *
         */
    var _initSelect2 =
        function(container) {
            $('#' + container.replace(/^#/, '') + ' select.select2').each(function() {
                var userEvents = {};
                var selectInput = $(this);
                var data = selectInput.data();
                // definir opções padrão
                var options = {
                    'multiple': false,
                    'language': 'pt-BR',
                    'placeholder': '',
                    'url': '',
                    'pageSize': 100,
                    'cache': true,
                    'template': "",
                    'allowClear': true,
                    'minimumInputLength': 1,
                    'minimumResultsForSearch': 0,
                    'maximumInputLength': 100,
                    'maximumResultsForSearch': 100,
                    'maximumSelectionLength': 2,
                    'selectOnClose': false,
                    'closeOnSelect': true,
                    'tags': false,
                    'tokenSeparators': ';',
                    'params': '',
                    'theme': 'bootstrap',
                    'autoHeight': true,
                    'container': '',
                };
                $.each(data, function(index, val) {
                    if (/^s2/.test(index)) {
                        index = index.slice(2);
                        index = index.charAt(0).toLowerCase() + index.slice(1);
                        if ((/^on/.test(index))) {
                            try {
                                userEvents[(index.slice(2)).toLowerCase()] = eval(val);
                            } catch (e) {
                                warn(e);
                            }
                        } else {
                            options[index] = val;
                        }
                    }
                });
                if (!options.container) {
                    options.container = container.replace(/^#/, '');
                }
                if (options.url) {
                    if (typeof options['minimumInputLength'] === 'undefined') {
                        options['minimumInputLength'] = 3;
                    }
                    options['delay'] = options['delay'] || 2000;
                    options['ajax'] = {
                            url: options.url,
                            dataType: 'JSON',
                            method: 'POST',
                            delay: options.delay,
                            data: function(params) {
                                var data = {
                                    q: params.term,
                                    ajax: 1,
                                    page: params.page || 1,
                                    pageSize: options.pageSize,
                                    offset: params.page ? ((params.page - 1) * (options.pageSize)) : 0,
                                    totalRecords: params.totalRecords || 0
                                };
                                if (options.params) {
                                    options.params.split(',').map(function(v) {
                                        v = v.split(':');
                                        var alias;
                                        var input;
                                        alias = v[0].split('|');
                                        v[0] = alias[0];
                                        alias[1] = alias[1] || alias[0];
                                        if (!v[1]) {
                                            input = $("#" + options.container + " [name=" + v[0] + "]")
                                            if (input.size() == 0) {
                                                // localizar o input subindo pra fora do container informado
                                                input = $("#" + options.container + " #" + v[0]);
                                                if (input.size() == 0) {
                                                    var containers = options.container.split(' ');
                                                    input = $("#" + containers[0].replace(/^#/, '') + " #" + v[0]);
                                                    if (input.size() == 0 && containers[1]) {
                                                        input = $("#" + containers[1].replace(/^#/, '') + " #" + v[0]);
                                                    }
                                                    if (input.size() == 0) {
                                                        input = $("#" + v[0]);
                                                    }
                                                }
                                            }
                                            if (input.size() == 1) {
                                                v[1] = ''
                                                try {
                                                    if (input[0].type.toLowerCase() == 'checkbox') {
                                                        if (input[0].checked)
                                                        {
                                                            v[1] = input.val();
                                                        }
                                                    }
                                                    else {
                                                        v[1] = input.val();
                                                    }
                                                } catch( e ) {
                                                    v[1] = '';
                                                }
                                            }
                                            // pode ser radio ou checkbox selecionados
                                            else if ( input.size() > 1)
                                            {
                                                v[1] = [];
                                                input.map( function(i,input){
                                                    if( $(input).prop('checked') ){
                                                        v[1].push( input.value );
                                                    };
                                                });
                                                v[1] = v[1].join(',');
                                            }
                                        } else {
                                            // SALVE SEM CICLO
                                            // transformar "true" em true e numeros em valores
                                            try {
                                                v[1] = eval(v[1]);
                                            } catch(e){
                                            }
                                        }
                                        if (v[1] || v[1] === false) {
                                            try {
                                                if ( v[1] instanceof Array )
                                                {
                                                    v[1] = v[1].join(',');
                                                }
                                                data[alias[1]] = v[1]
                                            } catch (e) {
                                                warn(e);
                                            };
                                        }
                                    });
                                    // data.nivel = 'ESPECIE';
                                }
                                return data;
                            },
                            processResults: function(data, params) {
                                params.page = data.page || 1;
                                params.totalRecords = data.totalRecords;
                                if( data.message )
                                {
                                    app.growl( data.message );
                                }
                                if( data.error )
                                {
                                    app.alertError(data.error );
                                }
                                return {
                                    results: data.items,
                                    pagination: {
                                        more: (params.page * options.pageSize) < data.totalRecords
                                    }
                                };
                            },
                            cache: options.cache,
                        } // fim options['ajax'];
                } // fim if url
                options['templateResult'] = function(item) {
                    if (item['loading'] || item['id']) {
                        var text = '';
                        var hint = '';
                        if (!item.hasOwnProperty('descricao') &&
                            item.hasOwnProperty('text')) {
                            item['descricao'] = item['text'];
                        }
                        if (options.template && !item.loading) {
                            var template = $('#' + options.template).html();
                            for (key in item) {
                                text = item['descricao'];
                                try {
                                    template = template.replace('{' + key + '}', item[key]);
                                } catch (e) {
                                    template = '<span>' + (text ? text : item.id) + '</span>';
                                    break;
                                }
                            }
                            return $(template);
                        } else {
                            // text = item['descricao'] || item['text'];
                            if (item['descricao']) {
                                if (item.loading) {
                                    //return $('<span>' + item['descricao'] + '<i class="glyphicon glyphicon-refresh spinning"></i></span>');
                                    return $('<span>' + item['descricao'] + window.grails.spinner + '</span>');
                                }
                                if( item.txHint )
                                {
                                    hint = ' title="'+item.txHint+'"'
                                }
                                return $('<span'+hint+'>' + item['descricao'] + '</span>');
                            }
                            return;
                        }
                    } else {
                        return 'Carregando...';
                    }
                };
                options['templateSelection'] = function(item) {
                    var text = $.trim(item['descricao'] || item['text']);
                    // quando existir codigo html no texto tem que retornar como objeto jquery
                    text = text.replace(/^(&nbsp;)+/, '');
                    if (text.indexOf('</') > -1) {
                        return $('<span>' + text + '</span>');
                    }
                    return text;
                };
                var isHide = selectInput.parent().hasClass('hide');
                var isNone = false
                if (!isHide) {
                    isNone = selectInput.parent().css('display') == 'none';
                };
                // para ativar o select 2 ele deve estar visivel na tela para evitar
                // problema na largura
                selectInput.parent().show();
                selectInput.parent().removeClass('hide');
                // inicializar o select 2
                $(selectInput).select2(options)
                if (isNone) {
                    selectInput.parent().hide();
                }
                if (isHide) {
                    selectInput.parent().addClass('hide');
                }
                // esconder o select2 e o label
                if (options.visible === false) {
                    selectInput.parent().addClass('hide');
                }
                // aplicar o auto-height
                if (options['autoHeight']) {
                    $(selectInput).on('select2:open', function(e) {
                        /*
                         var container = $(this).select('select2-container');
                         var position = container.offset().top;
                         var availableHeight = window.innerHeight - position; //- container.outerHeight();
                         var bottomPadding = 10; // Set as needed
                         var maxH= (availableHeight - bottomPadding) + 'px';
                         */
                        //var maxH = ($(window).height() / 2) + 'px';
                        var maxH = ( Math.max(400, window.innerHeight - 130) / 2 )+'px';
                        //var maxH=$(window).height()-200;
                        /*var childNodes;
                         if( $('ul.select2-results__options').size() > 0 )
                         {
                         childNodes=$('ul.select2-results__options')[0].childNodes.length;
                         if( childNodes > 10 )
                         {
                         maxH='500px';
                         }
                         }
                         */
                        $('ul.select2-results__options').css({
                            'max-height': maxH,
                            'height': maxH
                        });
                    });
                }
                // não abrir o dropdown ao limpar
                $(selectInput).on('select2:unselecting', function() {
                    var self = this;
                    setTimeout(function() {
                        $(self).select2('close');
                    }, 0);
                });

                $(selectInput).on('select2:open', function(e) {
                    //console.log('select2.open')
                    if( options.url ) {
                        var id = $(this).attr('id');
                        if( id )
                        {
                            $("#select2-"+id+"-results li:gt(0)").remove();
                        }
                    }
                });
                // adicionar os eventos do usuario ao campo select2
                $.each(userEvents, function(evt, fnc) {
                    if (evt === 'change') {
                        $(selectInput).on(evt, function(e) {
                            // quando a função change possui uma chamada de
                            // recarregamento da pagiana via ajax, a lista do select2
                            // fica perdida na tela
                            // fechar a lista de opções antes de disparar o evento
                            // change
                            // mas não funcionou ainda
                            //
                            // if( ! $(e.target).select2("data") )
                            // {
                            //    $(e.target).select2("close");
                            //}

                            try {
                                fnc(e);
                            } catch (e) {
                                warn(e);
                            }
                        });
                    } else {
                        $(selectInput).on('select2:' + evt, fnc);
                    }
                });
            });
        };
        /**
         * Percorrer todos as divs do container procurando id="map" e inicializa o
         * mapa e os eventos.
         * @example <div id="mapaOcorrencia" class="map" data-map-on-load="distribuicao.mapInit" data-map-config='{"zoom":"4"}'>
         * @param  {[type]} container [description]
         * @return {[type]}           [description]
         */
    var _initMap = function(container) {}
    /**
     * percorrer todos os elementos com a classe 'fld' e colocar restrição de readonly se canModify for false
     * @param container
     * @private
     */
    var _initSecurity = function(container) {
            // $("#" + container).removeClass('hidden');
            var canModifyContainer = ! ( typeof(canModify) != 'undefined' && canModify === false );
            if (!container || !$('#sqFicha').val()) {
                $("#" + container).removeClass('hidden');
                if ($("#" + container + " div.modal-body")) {
                    $("#" + container + " div.modal-body").removeClass('hidden');
                }
                return;
            }
            if( ! container )
            {
                return;
            };
            var $container = $("#" + container.replace(/^#/, '') );
            if( $container.size() == 0)
            {
                return;
            };
            var $inputHiddenCanModify = $container.find('input#canModify');
            if( $inputHiddenCanModify.size() == 1 )
            {
                if( $inputHiddenCanModify.val() =='true' || $inputHiddenCanModify.val() == 'S' )
                {
                    canModifyContainer = true;
                }
                else if( $inputHiddenCanModify.val() =='false' || $inputHiddenCanModify.val() =='N' )
                {
                    canModifyContainer = false;
                }
            }

            // modo somente leitura
            if ( ! canModifyContainer ) {
                //if ( ! canModify ) {
                $container.find(".fld").each(function() {

                    // remover os atributos que atribuem os eventos ao elemento
                    $(this).data('change','');
                    $(this).data('action','');
                    $(this).data('select','');
                    $(this).data('enter','');
                    $(this).removeAttr('data-change','');
                    $(this).removeAttr('data-action','');
                    $(this).removeAttr('data-select','');
                    $(this).removeAttr('data-enter','');

                    if (/TEXTAREA|INPUT/i.test(this.tagName)) {
                        if( $(this).hasClass('wysiwyg') )
                        {
                            $(this).removeClass('wysiwyg').addClass('wysiwygRO');
                        }
                        else {
                            if (this.type.toLowerCase() == 'checkbox') {
                                $(this).prop('disabled', true);
                                $(this).addClass('RO');
                            } else {
                                $(this).prop('readonly', true);
                                $(this).addClass('RO');
                            }
                        }
                    } else if (/SELECT/i.test(this.tagName)) {
                        $(this).prop('disabled', true );
                        $(this).addClass('RO');
                    } else if (/BUTTON|A|I/i.test(this.tagName)) {
                        $(this).remove();
                    } else if (/DIV|BR/i.test(this.tagName)) {
                        $(this).remove();
                    } else {
                        $(this).prop('disabled', true);
                        $(this).addClass('RO');
                    }
                });
            }
            if ( $container.find("div.modal-body") ) {
                 $container.find("div.modal-body").removeClass('hidden');
            }
            $container.removeClass('hidden');
        };

    /*
     Metodo para ativar o editor de texto nos campos textareas com a classe wysiwyg
     */
    var _initEditor = function(container) {
            var selector = '#' + container.replace(/^#/, '') + ' .wysiwyg';
            tinymce.remove(selector);
            tinymce.remove(selector + 'RO');
            canModify = ( typeof( canModify ) == 'undefined' ? true : canModify)

            // opções especificas
            var custom_options = {
                selector: '#' + container.replace(/^#/, '') + ' .wysiwyg',
                readonly: (canModify ? 0 : 1),
                setup: function (editor) {
                    editor.on('blur', function (e) {
                         tinymce.activeEditor.setContent(tinymce.activeEditor.getContent().replace(/&nbsp;\s?/g,' '));
                    });
                    editor.on('change', function (e) {
                        //tinymce.activeEditor.getContent();
                        //log( editor.getContent() );
                        editor.save();
                    });

                    // possibilitar selecionar e copiar com o editor no modo somente leitura
                    editor.on('SwitchMode', function() {
                            if (editor.readonly) {
                                editor.readonly = 1;
                            }
                        });

                    editor.on("keyup", function (event) {
                        try {
                            // alterar a cor da aba para vermelha quando mexer no texto
                            var li = $('#li' + $("#" + tinymce.activeEditor.id).closest('div[role=tabpanel]').attr('id').ucFirst());
                            if (li.size() == 0) {
                                li = $('#li' + $("#" + tinymce.activeEditor.id).closest('div[role=tabpanel]').attr('id'));
                            }
                            if (li.size() > 0) {
                                li.addClass('changed');
                                // marcar a aba superior se for uma subAba
                                var li2 = $('#li' + li.closest('div[role=tabpanel]').attr('id').ucFirst());
                                if (li2.size() == 0) {
                                    li2 = $('#li' + li.closest('div[role=tabpanel]').attr('id').ucFirst());
                                }
                                if (li2.size() > 0) {
                                    li2.addClass('changed');
                                }
                            }
                        } catch (erro) {
                        }
                    });
                    editor.on("keydown", function (event) {
                        // tratamento para a tecla tab
                        if (event.keyCode == 9) { // tab pressed
                            editor.execCommand('mceInsertContent', false, '&emsp;');
                            event.preventDefault();
                            event.stopPropagation();
                            return false;
                        }

                    });
                    editor.on("init", function () {
                        if ( $(editor.targetElm).prop("readonly") === true) {
                            $(this.contentAreaContainer.parentElement).find("div.mce-toolbar-grp").hide();
                        };
                        // alterar a fonte padrão para Open Sans
                        //tinymce.activeEditor.setContent(tinymce.activeEditor.getContent().replace(/&nbsp;\s?/g,' '));
                        var text = editor.getContent()
                        if( /Times New Roman/.test( text ) ) {
                            text = text.replace(/Times New Roman/g,'open_sansregular');
                            $(editor.targetElm).html(text);
                            editor.setContent(text, {format : 'html'});
                        }
                        /*var id = editor.id;
                        var height = 400;
                        document.getElementById(id + '_ifr').style.height = height + 'px';
                        */
                    });
                    /*
                    editor.on("paste",function(e)
                    {
                        // https://www.tinymce.com/docs/advanced/editor-command-identifiers/
                        // padronizar fonte ao colar
                        //var content = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('text/plain');
                        //e.preventDefault();
                        //editor.execCommand('mceInsertContent', false, content);
                        app.editorAfterPaste(e)
                    });*/

                },
            };

            tinymce.init($.extend({},custom_options, default_editor_options));


            // somente leitura
            tinymce.init({
                mode: 'specific_textareas',
                selector: '#' + container.replace(/^#/, '') + ' .wysiwygRO',
                content_style: default_editor_style,
                elementpath: false,
                menubar: false,
                readonly: 1,
                statusbar: false,
                skin: "icmbio",
                language: 'pt_BR',
                min_height: 200,
                browser_spellcheck: true,
                plugins: "textcolor,link,lists",
                toolbar: false,
                setup: function (editor) {
                    // possibilitar selecionar e copiar com o editor no modo somente leitura
                    editor.on('SwitchMode', function () {
                        if (editor.readonly) {
                            editor.readonly = 1;
                        }
                    });
                },
            });
        };

    /**
     * aplicar text justify ao texto colado
     * @private
     */
    var _editorAfterPaste = function(e) {

            window.setTimeout( function(){
                var texto = tinymce.activeEditor.getContent();
                texto = texto.replace(/background-color: (#.{6})/,'bgc: $1"')

                tinymce.activeEditor.setContent(texto);

                tinymce.activeEditor.execCommand('selectAll',false);
                //tinymce.activeEditor.execCommand('RemoveFormat',false);

                tinymce.activeEditor.execCommand('FontName',false,"open_sansregular");
                //tinymce.activeEditor.execCommand('FontName',false,"Open Sans");
                //tinymce.activeEditor.execCommand('FontName',false,"Times New Roman");
                //tinymce.activeEditor.execCommand('FontName',false,"open_sansregular");
                tinymce.activeEditor.execCommand('FontSize',false,"14pt");

                //tinymce.activeEditor.execCommand('FontName',false,"Open Sans");
                //tinymce.activeEditor.execCommand('FontSize',false,"12pt");


                // tentar preservar a cor de fundo do texto quando estiver no padrão: style="background-color: #xxxxx;"
                //tinymce.activeEditor.execCommand('BackColor',false,"#ffffff");
                //tinymce.activeEditor.execCommand('ForeColor',false,"#000000");

                //tinymce.activeEditor.execCommand('HiliteColor',false,"#ffffff");
                tinymce.activeEditor.execCommand('JustifyFull',false);
                tinymce.activeEditor.selection.collapse(); // remover seleção do texto

                texto = tinymce.activeEditor.getContent();
                texto = texto.replace(/bgc: (#.{6})/,'background-color: $1');

                tinymce.activeEditor.setContent(texto);

                //window.setTimeout(()=>{
                    //var e = new KeyboardEvent("keypress", {bubbles : true, cancelable : true, key : "Q", char : "Q", shiftKey : true});
                    //tinymce.activeEditor.fire('keypress',{bubbles : true, cancelable : true, key : "Q", char : "Q", shiftKey : true} )
                    //$("#dsNotasTaxonomicas")[0].dispatchEvent(e);

                    //tinymce.activeEditor.fire('keypress',{bubbles : true, cancelable : true, key : "Q", char : "Q", shiftKey : true} );
                    //tinymce.fire('keypress',{bubbles : true, cancelable : true, key : "Q", char : "Q", shiftKey : true} );
                    //console.log('Home triggered');
                //},5000);

            },300);
            //
            /*
            // https://www.tinymce.com/docs/advanced/editor-command-identifiers/
            // padronizar fonte ao colar
            app.blockUI('Padronizando texto inserido.');

            window.setTimeout( function() {
                var data = { 'html' : tinymce.activeEditor.getContent() }
                app.ajax(app.url + 'main/htmlSanitizer', data,
                    function (res) {
                        app.unblockUI();
                        tinymce.activeEditor.setContent(res);
                        tinymce.activeEditor.execCommand('selectAll', false);
                        //tinymce.activeEditor.execCommand('RemoveFormat',false);
                        tinymce.activeEditor.execCommand('FontName', false, 'Times New Roman');
                        tinymce.activeEditor.execCommand('FontSize', false, '14pt');
                        tinymce.activeEditor.execCommand('JustifyFull', false);
                    }, null, 'HTML');
            },500);
            */
        }

    var _initMultiSelect = function(container) {

        if( !container )
        {
            return;
        }
        $('#' + container.replace(/^#/, '') + ' select').each(function(i,select) {
            if( $(select).data('multiple') )
            {
                $(select).prop('multiple',true);
                var width = $(select).css('width') || 'auto';
                var data = $(select).data();
                var allText = ( data.allLabel ? data.allLabel : '-- todas --');
                try {
                    if (width != 'auto') {
                        width = Math.max(100, parseInt(width));
                    }
                } catch( e ) {
                    width='auto';
                }
                var enableFiltering = false;
                if( ! data.enableFiltering ){
                    if( typeof data.enableFiltering =='undefined') {
                        enableFiltering = ( select.options.length > 20 );
                    } else {
                        enableFiltering = false;
                    }
                } else {
                    enableFiltering = true;
                }
                var options = {
                    includeSelectAllOption: false
                    ,enableFiltering : enableFiltering
                    ,enableCaseInsensitiveFiltering : enableFiltering
                    ,enableFullValueFiltering:false
                    ,filterPlaceholder : 'localizar...'
                    ,includeResetOption: false
                    ,includeResetDivider:false
                    ,resetText:'Limpar'
                    ,buttonWidth:width
                    ,numberDisplayed: 3
                    ,nSelectedText : 'selecionados'
                    ,nonSelectedText : allText
                };
                $(select).val(''); // não vir com a primeira opção selecionada
                $(select).multiselect( $.extend({},default_multi_select_options,options) );

            }
        });

    };

        /*   var _initKeys = function(container) {
         $('#' + container.replace(/^#/, '')).find('input[data-enter]').each(function() {
         $(this).bind('on')
         });
         }
         */
        /**
         * método para validar/executar funções passadas como string
         */
    var _eval = function(f, p, retry) {
            retry = (retry === false ? false : true);
            p = p || arguments;
            /*if (/^\$\(/.test(f))
            {
                return;
            }
            */
            try {
                var func = eval(f);
                if (typeof(func) === 'function') {
                    // testar chamar com arguments do javascript ex:
                    // f.apply({},arguments);
                    if ($.isArray(p)) {
                        return func.apply(this, p);
                    } else {
                        return func(p);
                    }
                } else if (!func && !/(\)|;)$/.test(f) ) {
                    /*if (window.grails.env == 'desenv') {
                        warn('Função ' + f + ' não encontrada!')
                    }*/
                };
            } catch (e) {
                if (!retry) {
                    if (window.grails.env == 'desenv') {
                        console.error(e);
                    }
                } else {
                    window.setTimeout(function () {
                        return app.eval(f, p, false);
                    }, 1500);
                }
            }
            ;
        };

        /**
         * Metodo para transformar uma string de nomes de campos separados por virgula
         * em objeto key,value para serem postados
         * Se não for informado o valor do campo, o método tentará ler de algum campo
         * da página que tiver o mesmo id
         *
         * @param  string params - relação de campos separados por vírgula ex:
         * "nome,endereco,codigo:1"
         * @param  object data   - objeto que receberá os novos valores ex: {}
         * @param  object container  - id do container para buscas dos campos apenas dentro dele, se não for informado será assumido o body
         * @return object        - objeto retornado com a relação de chaves e valores
         * ex: {nome:'teste',endereco:'ruax',codigo:'1'}
         */
    var _params2data = function(params, data, container) {
            var fld;
            params = params || {};
            params = params.data ? params.data : params;
            if (params) {
                if (typeof(params) === 'object') {
                    for (key in params) {
                        if (key === 'params') {
                            _params2data(params[key], data, container);
                        } else {
                            if (!/tooltipster/.test(key)) {
                                data[key] = (params[key] || params[key]===false ? params[key] : $("#" + params[key]).val());
                            }
                        }
                    }
                } else {
                    var aTemp;
                    params.split(',').map(function(v) {
                        v = v.split(':');
                        if (!v[1]) {
                            aTemp = v[0].split('|');
                            v[0] = aTemp[0];
                            if (!container) {
                                if ($("#" + v[0]).size() > 0) {
                                    v[1] = $("#" + v[0]).val(); // pegar pelo id
                                } else {
                                    v[1] = $("*[name='" + v[0] + "']").val(); // pegar pelo name
                                }
                            } else {
                                container = "#" + container.replace(/^#/, '');
                                fld = $(container + ' [name=' + v[0] + ']').get(0) || $(container + ' [id=' + v[0] + ']'); // pegar pelo name dentro do conatainer
                                if (fld) {
                                    v[1] = $(fld).val();
                                }
                            }
                            if (aTemp[1]) {
                                v[0] = aTemp[1];
                            }
                        } else {
                            try {
                                v[1] = eval(v[1]);
                            } catch (e) {
                                v[1] = v[1];
                            }
                        }
                        if (v[1]) {
                            var aTemp = v[0].split('|');
                            v[0] = aTemp[0];
                            try {
                                data[v[0]] = v[1]
                            } catch (e) {};
                        }
                    });
                }
            }
            // ajustar o parametro
            for (key in data) {
                if (!isNaN(key)) {
                    if (data[key].hasOwnProperty('name') && data[key].hasOwnProperty('value')) {
                        data[data[key].name] = data[key].value;
                    }
                }
            }
            return data;
        }
        // --------------------------------------------------------------------------------------
    var _setFormFields = function(data, form) {
            // falta implementar por formulário
            if (!data) {
                return;
            }
            // formato retornado palo metodo _returnAjax dos controllers
            if (data.data) {
                data = data.data;
            }
            for (key in data) {
                var flds = null;
                if (form) {
                    form = form.replace(/^#/, '');
                    flds = $("#" + form + " *[name='" + key + "']");
                    if (flds.size() == 0) {
                        flds = $("#" + form + " *[id='" + key + "']");
                        if (flds.size() == 0) {
                            // checkbox dos grides com ids no formato "nome_key"
                            flds = $("#" + form + ' *[id^=' + key + '_]');
                        }
                    }
                } else {
                    flds = $("#" + key);
                    if (flds.size() == 0) {
                        flds = $("*[name='" + key + "']");
                        if (flds.size() == 0) {
                            // checkbox dos grides com ids no formato "nome_key"
                            //flds=$('#'+key+'_'+data[key] );
                            flds = $('*[id^=' + key + '_]');
                        }
                    }
                }
                if (flds.size() > 0) {
                    $.each(flds, function(k, oFld) {
                        var fieldData = $(oFld).data();
                        // se o select estiver desabilitado guardar o valor temp para quando for populado definir o valor inicial
                        if( data[key] == null )
                        {
                            data[key] = '';
                        }
                        if (oFld.tagName == 'SELECT') {
                            $(oFld).data('tmpVal', String(data[key]));
                        }
                        if ($(oFld).hasClass("select2")) {
                            //if ($(oFld).select2('data').length > 0 && $(oFld).find("option[value='" + data[key] + "']").length > 0) {
                            if (oFld.options.length > 0 && oFld.selectedIndex > -1 && (oFld.options.length != 1 || oFld.options[0].value != '')) {
                                var newValue = String(data[key]);
                                if (newValue.indexOf('[') > -1) // é um multiselect ex: tags [123,456,789]
                                {
                                    app.setSelect2(key, {}, newValue);
                                } else {
                                    if ($(oFld).find("option[value='" + data[key] + "']").length > 0) {
                                        if (String(data[key]) != String($(oFld).select2('val'))) {
                                            if ($(oFld).find("option").length > 1) {
                                                $(oFld).find("option[value=" + data[key] + "]").prop('selected', 'true');
                                                $(oFld).select2().val(data[key]).trigger('change');
                                            } else {
                                                $(oFld).select2('val', data[key]).trigger('change');
                                            }
                                        }
                                    }
                                }
                            } else {
                                $(oFld).empty();
                                // tentar inicializar o select2 se o campo descrição tiver o mesmo nome do campo chave iniciado por "ds" ou "de"
                                var deField = key.replace('sq', 'de');
                                var dsField = key.replace('sq', 'ds');
                                var noField = key.replace('sq', 'no');
                                var txField = data[deField] || data[dsField] || data[noField];
                                if (txField) {
                                    app.setSelect2(key, data, txField);
                                }
                            }
                        } else if (oFld.type == 'radio') {
                            if (oFld.value == data[key]) {
                                $(oFld).prop('checked', true);
                                //$('input[name=inListaOficialVigente][value=S]' ).prop('checked',true)
                            } else {
                                $(oFld).prop('checked', false);
                            }
                        } else if (oFld.type == 'checkbox') {
                            try {
                                if (oFld.value == data[key] || String(data[key]).indexOf(oFld.value) > -1 || String(data[key]).indexOf(parseInt(oFld.value)) > -1 || data[key] == true) {
                                    $(oFld).prop('checked', true);
                                } else {
                                    $(oFld).prop('checked', false);
                                }
                            } catch (e) {}
                        } else if (oFld.type == 'textarea') {
                            $(oFld).val(data[key]);
                            if ($(oFld).hasClass("wysiwyg")) {
                                tinymce.get(oFld.id).setContent(data[key]);
                            }
                        } else {
                            $(oFld).val(data[key]);
                        }
                    }); // each
                }
            }
        }
        // --------------------------------------------------------------------------------------
    var _setSelect2 = function(id, data, value, context, callChange) {
            value = value || '';
            context = context || '';
            callChange = typeof callChange == 'undefined' ? true : callChange;
            id = id.replace(/^#/, '');
            var fld = $("#" + (context != '' ? context.replace(/^#/, '') + ' #' : '') + id);
            if (fld.size() == 0) {
                fld = $('select[name=' + id + ']');
            }
            if (fld.size() > 0) {
                if (String(value).indexOf('[') == 0) {
                    try {
                        value = eval(value);
                        if (value instanceof Array) {
                            if (callChange) {
                                fld.select2().val(value).trigger("change");
                            }
                        }
                    } catch (e) {}
                } else {
                    var itemId = data;
                    if (typeof(data.id) != 'undefined') {
                        itemId = data.id;
                    };
                    var deField = id.replace('sq', 'de');
                    var dsField = id.replace('sq', 'ds');
                    var coField = id.replace('sq', 'co');
                    if (typeof(data[id]) != 'undefined') {
                        itemId = data[id];
                    }
                    // procurar o texto no dsCampo ou deCampo no data recebido
                    if (value == '') {
                        value = data[deField] || data[dsField] || data[coField];
                    }
                    fld.html('<option value="' + itemId + '">' + value + '</option>');
                    var item = fld.select2('data');
                    if (typeof data == 'object') {
                        $.each(data, function(k, v) {
                            if (typeof(item[0][k]) == 'undefined') {
                                item[0][k] = v;
                            }
                        });
                        fld.select2('data', item);
                    } else {
                        /*item.id=data;
                         item.title=value;
                         item.descricao=value;
                         fld.select2('data', item);
                         */
                    }
                    if (typeof($(fld).change) == 'function') {
                        if (callChange) {
                            $(fld).change();
                        }
                    }
                }
            }
        }
        // --------------------------------------------------------------------------------------
        /**
         * Método para posicionar o foco para edição no controle informado
         */
    var _focus = function(id, contexto) {
            try {
                if (contexto) {
                    contexto = contexto.replace(/^#/, '')
                };
                var fld = $( (contexto ? "#" + contexto + ' ' : '') + '#' + id.replace(/^#/, '')).get(0) || $((contexto ? "#" + contexto + ' ' : '*') + "[name='" + id + "']").get(0);
                if (fld ) {
                    window.setTimeout(function() {
                        $(fld).focus();
                    }, 1000)
                }
            } catch (e) {};
        }
        /**
         * Método para limpar os campos de um formulario
         * @param  string id - id do form
         * @params boolFiltros = se true os campos terminados com Filtro tambem serão limpos
         * @return null
         */
    var _reset = function(id, strExcept, boolFiltros) {
            var arrExcept = [];
            if (strExcept) {
                arrExcept = strExcept.split(',');
            }
            if( typeof( boolFiltros) == 'undefined' )
            {
                boolFiltros=false;
            }
            boolFiltros = boolFiltros == true ? true : false;
            app.resetChanges(id);
            // limpar os input file
            app.clearInputFile(id);
            // limpar os campos
            $("#" + id.replace(/^#/, '')).find("input[type=file],input[type=number],input[type=date],input[type=text],input[type=password],input[type=hidden],input[type=checkbox],input[type=radio],textarea,option,select,i#checkUncheckAll").each(function() {
                var skip = false;
                var eleId = this.id;
                var eleName = this.name;
                // se o campo não tiver id nem name, ignorar  pode ser dos multiselects dos filtros
                if (!eleId && !eleName) {
                    skip = true;
                }

                // verificar se é para limpar os filtros
                if ( ! boolFiltros) {
                    if (eleName && /Filtro$/.test(eleName)) {
                        skip = true;
                    }
                    if (eleId && /Filtro[0-9]/.test(eleName)) {
                        skip = true;
                    }
                }


                if ( !skip && strExcept ) {
                    // quando for option e o pai for select, ler o id do select
                    if ( this.tagName == 'OPTION' && /SELECT/gi.test( $(this).parent()[0].tagName) ) {
                        eleId   = $(this).parent().prop('id')
                        eleName = $(this).parent().prop('name')
                    }
                    if (arrExcept.indexOf(eleId) > -1 || arrExcept.indexOf(eleName) > -1) {
                        skip = true;
                    }
                }
                if (!skip) {
                    if ( ! boolFiltros && this.name && /Filtro$/.test(this.name)) // não limpar campos de filtros
                    {
                        skip = true;
                    }
                }
                if (!skip) {
                    $(this).data('changed', false);
                    if ( this.tagName == 'OPTION' ) {
                        if ($(this).prop('selected')) {
                            $(this).prop('selected', false); // remover a propriedade selected
                        }
                    } else if (this.tagName == 'SELECT') {
                        if ($(this).data('s2-url')) {
                            $(this).empty();
                            $(this).trigger('change');
                        } else if ($(this).hasClass('select2')) {
                            if ($(this).val()) {
                                $(this).val([]).trigger('change');
                            }

                        } else {
                            // limpar selects combinados
                            if ($(this).val() != '') {
                                $(this).val('');
                                $(this).change();
                            }
                        }
                    } else if (this.tagName == 'INPUT' || this.tagName == 'TEXTAREA') {
                        if (this.type == 'checkbox' || this.type == 'radio') {
                            if ($(this).prop('checked')) {
                                $(this).prop('checked', false);
                                try {
                                    if ($(this).parent()[0].tagName.toUpperCase() == 'TD') {
                                        $(this).parent().parent().removeClass('tr-selected');
                                    }
                                } catch(e) {}
                            }
                        } else if (this.type == 'textarea' && $(this).hasClass('wysiwyg')) {
                            if (tinymce.get(this.id).getContent() != '') {
                                tinymce.get(this.id).setContent('');
                                $(this).val('');
                            }
                        } else {
                            if ($(this).val() != '') {
                                $(this).val('');
                            }
                        }
                    } else if (this.tagName == 'I') {
                        $(this).removeClass('glyphicon-check').addClass('glyphicon-unchecked');
                    }
                }
            });
            // atualizar situação modificadas da aba atual.
            app.isTabModified();

        }
        /**
         * Método para destacar a linha de um grid alterando a cor de fundo
         */
    var _selectGridRow = function(elem) {
            $(elem).parents('table').find('tr.rowSelected').removeClass('rowSelected')
            $(elem).closest('tr').addClass('rowSelected')
        }
        /**
         * Método para remover o destaque da linha do gride
         * Pode ser passado o id do grid ou o botão clicado
         */
    var _unselectGridRow = function(gridId) {
            if (typeof gridId == 'object') // botão
            {
                $(gridId).parents('table').find('tr.rowSelected').removeClass('rowSelected')
                return;
            }
            if (gridId) {
                gridId = gridId.replace(/^#/, '');
                $("#" + gridId + " tr.rowSelected").removeClass('rowSelected');
            } else {
                $("tr.rowSelected").removeClass('rowSelected');
            }
        }
        /**
         * Método para validar data no formato dd/mm/yyyy
         */
    var _isDate = function(data) {
            try {
                if (data.indexOf('/') > 0) {
                    data = data.split('/');
                } else if (data.indexOf('-') > 0) {
                    data = data.split('-');
                };
                var dia = data[0]
                var mes = data[1]
                var ano = data[2]
                    //Criando um objeto Date usando os valores ano, mes e dia.
                var novaData = new Date(ano, (mes - 1), dia);
                var mesmoDia = parseInt(dia, 10) == parseInt(novaData.getDate());
                var mesmoMes = parseInt(mes, 10) == parseInt(novaData.getMonth()) + 1;
                var mesmoAno = parseInt(ano) == parseInt(novaData.getFullYear());
                if (!((mesmoDia) && (mesmoMes) && (mesmoAno))) {
                    return false
                }
            } catch (e) {}
            return true;
        }
        /**
         * Método para validar hora no formato HH:MM:SS
         * @param  {[type]}  hora [description]
         * @return {Boolean}      [description]
         */
    var _isTime = function(hora) {
            try {
                hora = hora.split(':');
                hrs = hora[0];
                min = hora[1];
                seg = hora[2] || 0;
                if ((hrs < 0) || (hrs > 23) || (min < 0) || (min > 59) || (seg < 0) || (seg > 59)) {
                    return false;
                }
            } catch (e) {
                return false;
            }
            return true;
        }
        /*
         * Converte coordenada decimal para GMS
         * Ex: gd2gms(45.5454578,'LAT');
         */
    var _gd2gms = function(dd, latlon) {
            dd = parseFloat(String(dd).replace(/\,/, '.'));
            if (isNaN(dd)) {
                return [];
            };
            var deg = Math.floor(Math.abs(dd));
            var min = Math.floor((Math.abs(dd) - deg) * 60);
            var sec = (60 * ((Math.abs(dd) - deg) * 60 - min)).toFixed(5);

            if( parseInt(sec) == 60 ) {
                sec = '0.00000';
                min++;
            }
            if( parseInt(min) >= 60 ) {
                min=0;
                deg++;
            }

            sec = String(sec).replace(/\./, ',');
            var result = [];
            if (latlon.toUpperCase() == 'LAT') {
                result.g = deg;
                result.m = min;
                result.s = sec;
                result.h = (dd <= 0.000000 ? 'S' : 'N');
            }

            if (latlon.toUpperCase() == 'LON') {
                result.g = deg;
                result.m = min;
                result.s = sec;
                result.h = 'W';
            }
            return result
        }
        /**
         * Função para converter Graus, minutos, segundos e Hemisfério para graus decimais
         * Ex: gms2gd(16,41,25.651,'S')
         */
    var _gms2gd = function(g, m, s, h) {
            if (g === '') {
                return 0;
            }
            s = String(s).replace(',', '.');
            var gInt = parseInt(g);
            var mInt = parseInt(m);
            var sFloat = parseFloat(s);
            var gd = gInt + mInt / 60 + sFloat / 3600;
            if (h.match(/w|s/i)) {
                gd = gd * -1;
            }
            return gd.toPrecision(10);
        }
        // --------------------------------------------------------------------------------------
    var _clearInputFile = function(id) {
            $("#" + id.replace(/^#/, '')).find(".fileinput-remove").click();
        }
        // --------------------------------------------------------------------------------------
        /**
         http://gijgo.com/

         Exemplo:
         app.openWindow({
                 id:'id_da_janela'
                ,url:app.url + 'ficha/getModal'
                ,width:950
                ,height:600
                ,data:{}
                ,title:'Título da Janela'
                ,autoOpen:true
                ,modal:true }
         ,function(data,e){// onOpen},
         ,function(data,e){// onClose},
         });
         */
        // TODO - Trocar gijgo para jsPanel
    var minHeight = Math.max(500,window.innerHeight-300);
    var _openWindow = function(jsonOptions, onShow, onClose) {
        if( jsonOptions.data.reload )
        {
            jsonOptions.reload = jsonOptions.data.reload;
        };
        if( ! jsonOptions.data.title && jsonOptions.title ){
            jsonOptions.data.title = jsonOptions.title
        };
        var _bringToFront = function(winId) {
                $.each(_window, function(k, v) {
                    if (winId != k) {
                        if (! $(v[0]).data('modal') ) {
                            $(v[0]).css('z-index', 'auto');
                        }
                        else
                        {
                            $(v[0]).css('z-index', '9999');
                        }
                    } else {
                        $(v[0]).css('z-index', '10002');
                    }
                });
            }
            // configurações padrão
        var _defaultOptions = {
            id: '',
            data: {},
            url: null,
            reload: false,
            autoOpen: false,
            closeOnEscape: true,
            draggable: true,
            height: minHeight,
            minHeight: null,
            maxHeight: null,
            width: ( $(window).width() - 100),
            minWidth: null,
            maxWidth: null,
            modal: true,
            resizeble: false,
            title: '',
            uiLibrary: 'bootstrap', //jqueryui,foundation
            drag: function(e) {
                //var top = parseInt($(e.target).css('top'));
                if (e.clientY <= 20) {
                    $(e.target).css('top', "0px");
                    return false;
                }
                if (e.clientY > (window.innerHeight - 10)) {
                    $(e.target).css('top', ( window.innerHeight-50 ) + "px" );
                    ///$(e.target).css('top', ($(window).height() - e.target.offsetHeight - 10) + 'px');
                    return false;
                }
                if (e.clientX <= 10) {
                    $(e.target).css('left', '0px');
                    return false;
                }
                if (e.clientX > ($(window).width() - 10)) {
                    $(e.target).css('left', ($(window).width() - e.target.offsetWidth) + 'px');
                    return false;
                }
                return true; // e.clientY > 0 && e.clientX > 10 && e.clientX < $(window).width() && (e.clientY-10) < ($(window).height()-10)
                // não deixar sobrepor o menu principal
                /*if (top < 103) {
                 $(e.target).css('top', "103px");
                 return false;
                 }
                 */
            },
            closed: function(e) {
                var data = $(e.target).data();
                _eval(onClose, [data, e]);
            },
            opened: function(e) {
                var data = $(e.target).data();
                if ( data.data.title ) {
                    $("#" + data.id + " h4.modal-title").html(data.data.title);
                } else if (data.title){
                    $("#" + data.id + " h4.modal-title").html(data.title);
                }
                $('#' + data.id + ' div.modal-body').css({
                    'height': (data.height - 70) + 'px',
                    'overflow-y': 'auto !important'
                })
                if (data.url) {
                    // ler a primeira vez o conteúdo ajax
                    if (!$('#' + data.id).data('loaded')) {
                        // se reload for false, não carregar novamente na proxima chamada
                        if (!data.reload) {
                            $('#' + data.id).data('loaded', true);
                        }
                        // strModule, data, container, callback, showAnimation
                        app.loadModule(data.url, data.data, data.id, function (res) {
                            _eval(onShow, [data, e]);
                        });
                    }
                    else {
                        _eval(onShow, [data, e]);
                    }
                } else {
                    _eval(onShow, [data, e]);
                }
            },
        };
        jsonOptions = $.extend({},_defaultOptions, jsonOptions);
        if (!jsonOptions.id) {
            return;
        }
        jsonOptions.id = jsonOptions.id.replace(/^#/, '');
        if (!_window[jsonOptions.id]) {
            var div = $("#" + jsonOptions.id);
            if (div.size() == 0) {
                div = $('<div id="' + jsonOptions.id + '" style="display:none;"></div>').appendTo('body');
            } else {
                div.css('display', 'none');
            }
            _window[jsonOptions.id] = div.dialog(jsonOptions);
            _bringToFront(jsonOptions.id);
            div.center();
            //if (!jsonOptions.modal)
            //{
            div.on('click', function() {
                $.each(_window, function(k, v) {
                    if (jsonOptions.id != k) {
                        if (!$(v[0]).data('modal')) {
                            $(v[0]).css('z-index', 'auto');
                        }
                    } else {
                        $(v[0]).css('z-index', '10002');
                    }
                });
            });
            /*} else {
             $(_window[jsonOptions.id][0]).css('z-index', '2100');
             }
             */
        } else {
            if (!_window[jsonOptions.id].isOpen()) {
                if (jsonOptions.data.height) {
                    $(_window[jsonOptions.id][0]).css('height', jsonOptions.data.height + 'px');
                }
                _window[jsonOptions.id].data('data', jsonOptions.data);
                _window[jsonOptions.id].open();
            }
            _bringToFront(jsonOptions.id);
        }

        return _window[jsonOptions.id];
    };

    var _closeWindow = function(id) {
        if (!id) {
            if (jsPanels.length > 0) {
                jsPanels[jsPanels.length - 1].panel.close();
            }
        } else {
            if (_window[id]) {
                _window[id].close();
            } else {
                var item = jsPanels.filter(function(obj, i) {
                    return obj.id == id;
                });
                if (item) {
                    if( item[0].panel.header.find('div.jsPanel-btn-close').size() == 1 ) {
                        item[0].panel.header.find('div.jsPanel-btn-close').click(); // assim chama o onClose do panel
                    }
                    else {
                        item[0].panel.close(); // assim não chama o onclose do panel
                    }
                }
            }
        }
    };
    var _getWindow = function(id) {
        if (_window[id]) {
            return _window[id];
        } else {
            var item = jsPanels.filter(function(obj, i) {
                return obj.id == id;
            });
            if (item) {
                return item[0].panel;
            }
            return null;
        }
    };
    var _logout = function() {
        // confirmar somente se estiver logado
        if ( $("span#spanLoginUserName").size() == 1  ) {
            app.confirm('<br><b>Deseja encerrar a sessão</b>?',
                function () {
                    window.location.href = app.url + 'login/logout';
                },
                null, null, 'Confirmação', 'danger');
        } else {
            window.location.href = app.url + 'login/logout';
        }
    }
        // --------------------------------------------------------------------------------------
        /**
         * Metodo para carregar os grides
         * @param  string  gridName - pode ser passado o id da div ou o nome do grid. Ex: getGrid('nomeum') ou getGrid('divGridNomeComum')
         * @return null
         */
    var _getGrid = function(gridName, idContainer, callback) {
        gridName = gridName.replace(/^#/, '').replace('divGrid', '');
        var div = $("#divGrid" + gridName.capitalize());
        var data = {};
        if (!div.get(0)) {
            div = $('#' + gridName);
        }
        data = {
            'gridName': gridName.lcFirst()
        };
        if (!div.get(0)) {
            app.alertError('Container do gride ' + gridName + ' não encontrado!');
            return;
        }
        // enviar os parametros definidos na div container do grid
        data['params'] = div.data();
        data['container'] = idContainer || '';
        // adicionar spinner na div
        //div.html('<div style="width:100%;height:100px;text-align:center;background-color:#fff;"><img src="' + window.grails.spinnerImage + '" style="margin-top:40px;width:16px;height:16px"></div>');
        //div.html('<div style="width:100%;height:100px;pading:20px;text-align:center;background-color:#fff;font-size:18px;"><i class="glyphicon glyphicon-refresh spinning"></i></div>');
        div.html('<div style="width:100%;height:100px;padding:20px;text-align:center;background-color:#fff;font-size:18px;">' + window.grails.spinner + '</div>');
        app.ajax('main/getGrid', data, function(res) {
            // adicionar o resultado na div
            div.html(res);
            // inicializar exibição dos tooltips nos elementos
            app.initSecurity(div.prop('id'));
            app.initTooltips(div.prop('id'));
            app.eval(callback, [res, data]);
        }, null, 'html');
    };
    // --------------------------------------------------------------------------------------
    var maxHeight = Math.max(500,window.innerHeight-200);
    var _panel = function(strId, strTitle, strUrl, objData, callback, intWidth, intHeight, boolModal, boolMaximized, onClose, position, theme, resizable, canMaximize, content ) {
        intWidth    = intWidth  || ($(window).width() - 50);
        intHeight   = intHeight || maxHeight;
        position    = position  || 'center-top';
        theme       = theme     || '#efefef';
        resizable   = ( resizable == false ? false : true );
        boolMaximized   = ( boolMaximized == true ? true : false );
        canMaximize  = canMaximize == false ? false : true;
        content     = content   || '';

        // verificar se já está aberto
        var item = jsPanels.filter(function(obj, i) {
            return obj.id == strId;
        });

        if (item.length > 0) {
            var status = item[0].panel.data('status');
            if (status != 'undefined') {
                item[0].panel.front();
                if (/^smallified/.test(status)) {
                    item[0].panel.smallify();
                } else if (status == 'minimized') {
                    item[0].panel.maximize();
                }
                return item[0].panel;
            } else {
                app.closeWindow(strid);
            }
        }

        var options = {
                id: strId,
                position: position,
                headerTitle: strTitle,
                paneltype: (boolModal ? 'modal' : ''),
                container: 'body',
                headerControls: (boolModal ? {
                    controls: 'all',
                    minimize: 'remove',
                    smallify: 'remove',
                    maximize: (canMaximize?'add':'remove'),
                } : {
                    controls: 'all',
                    maximize: (canMaximize?'add':'remove'),
                    minimize: 'remove',
                    smallify: 'remove'
                }),
                controls: {
                    confirmClose: "Deseja fechar esta janela?"
                },
                draggable: ! boolMaximized, // não arrastar quando estiver maximizada
                /*draggable: (boolMaximized ? { handle:'x'}: {
                    handle: 'div.jsPanel-titlebar, div.jsPanel-ftr',
                    opacity: 0.8
                }),
                */
                dragit: {
                    axis: false,
                    containment: 'parent',
                    handles: '.jsPanel-titlebar, .jsPanel-ftr.active', // do not set .jsPanel-titlebar to .jsPanel-hdr
                    opacity: 0.8,
                    start: false,
                    drag: false,
                    stop: false,
                    disableui: true
                },

                resizable: {
                    handles: 'n, e, s, w, ne, se, sw, nw',
                    autoHide: false
                    //minWidth: intWidth+50,
                    //minHeight: intHeight+50
                },

                resizeit: {
                    containment: 'parent',
                    handles: 'n, e, s, w, ne, se, sw, nw',
                    minWidth :Math.min( intWidth    ,10000),
                    minHeight:Math.min( intHeight+47,10000),
                    maxWidth: 10000,
                    maxHeight: 10000,
                    start: false,
                    resize: false,
                    stop: false,
                    disableui: true
                },

                show: false // 'jsPanelFadeIn'
                ,theme: theme,
                content: '<div id="' + strId + '_content" style="outline:none;border:none;display:block;min-height:100%;">'+content+'</div>',
                footerToolbar: '',
                contentSize: {
                    width: intWidth,
                    height: intHeight
                },
                onclose:null,
                onbeforeclose: onClose, // função callback executada ao fechar a janela
                callback: function(panel) {
                        // executado no onShow do panel
                        if (boolMaximized) {
                            panel.resize({
                                width:Math.max(300,window.innerWidth-60)
                                ,height:Math.max(200,window.innerHeight-40)
                            })
                        }
                        if (jsPanel.modalcount > 0) {
                            jsPanel.modalcount--;
                        }
                        // ler a url
                        if (strUrl) {
                            var panelBody =  panel.find('.jsPanel-content').prop('id', panel.attr('id') + '_body')
                            app.loadModule(app.url + strUrl, objData, panel.attr('id') + '_body', function(res) {
                                if (callback) {
                                    window.setTimeout(function() {
                                        app.eval(callback, panel);
                                    }, 500);
                                }
                            });
                        } else {
                            if (callback) {
                                window.setTimeout(function() {
                                    app.eval(callback,panel);
                                }, 500);
                            }
                        }

                        /*if ( onClose ) {
                            $(panel).find('.jsglyph-close').on('click', function(evt) {
                                evt.preventDefault();
                                evt.stopPropagation();
                                evt.stopImmediatePropagation();
                                if (typeof app.eval(onClose, panel) == 'undefined') {
                                    panel.close();
                                }
                            });
                        } // fim if onClose
                        */



                    } // fim callback
            }; // fim options

        if( !resizable || boolMaximized )
        {
            try {
                delete options.resizable;
                delete options.resizeit;
                options.resizable = false;
            } catch( e ) {}
        }

        if( boolMaximized )
        {
            options.dragit = {};
            options.headerControls.normalize = 'remove';
        }

        // http://jspanel.de/api/
        //boolModal = boolModal === true ? true : false;
        currentPanel = $.jsPanel({
            config: options
        });
        currentPanel.option.data = objData;
        // adicionar na pilha de panels abertos
        jsPanels.push({
            id: strId,
            panel: currentPanel
        });
        return currentPanel;
    };
    // --------------------------------------------------------------------------------------
    var _download = function(params) {
        app.confirm('Confirma o download?', function() {
            window.loacation.href = app.url + 'main/' + params.fileName;
        });
    };
    // --------------------------------------------------------------------------------------
    var _resetChanges = function(container) {
            if (!container) {
                return;
            }
            container = '#' + container.replace(/^#/, '');
            $(container).find('input,select,textarea').data('changed', false);
            app.isTabModified();

            // remover bordas vermelhas dos campos obrigatórios
            try{
                if( $(container)[0].tagName.toUpperCase()=='FORM') {
                    $(container).validate().resetForm();
                } else {
                    var form = $(container).find('form:first');
                    if (form.size() == 1) {
                        form.validate().resetForm()
                    }
                }
                $(container).removeClass('filtered');
            } catch( e ) {}
        }
        // --------------------------------------------------------------------------------------
    var _fieldChange = function(input) {
        if (input && /^INPUT|SELECT/i.test(input.tagName.toUpperCase())) {
            if (input && $(input).hasClass('ignoreChange')) {
                return;
            }
            if (!$(input).prop('disabled') && !$(input).prop('readonly')) {
                var curTab = '';
                // tentar ler a aba do input
                try {
                    curTab = $("#li" + $(input).closest('div[role=tabpanel]').attr('id').ucFirst());
                    if (!curTab) {
                        $("#li" + $(input).closest('div[role=tabpanel]').attr('id'));
                    }
                } catch (e) {
                    curTab = ''
                }
                if (curTab) {
                    if (typeof(input.defaultValue) == 'string') {
                        if (input.type.toLowerCase() == 'checkbox') {
                            var changed = input.checked != $(input).data('defaultChecked');
                            $(input).data('changed', changed);
                        } else if (input.defaultValue != input.value) {
                            $(input).data('changed', true);
                        } else {
                            $(input).data('changed', false);
                        }
                    } else if (typeof $(input).data('defaultValue') != 'undefined') {
                        var changed = input.value != $(input).data('defaultValue');
                        $(input).data('changed', changed);
                    } else {
                        $(input).data('changed', true);
                    }
                    // alterar a cor da aba
                    app.isTabModified(curTab)
                }
            }
        }
    };
    // --------------------------------------------------------------------------------------
    /**
     O parametro curTab é o id do "li" da aba e não do ul do nav-tabs
     */
    var _isTabModified = function(curTab) {
            // tabs em ordem de hieraquia
            var validTabs = ['ulTabsAvaliacao', 'ulTabsConservacao', 'ulTabsPopulacao', 'ulTabsHistoriaNatural', 'ulTabsDisOcorrencia', 'ulTabsDistribuicao', 'ulTabsFicha', 'ulTabsOficina','ulTabsGerenciarConsulta','ulTabsCadInformativo'];
            if (curTab) {
                // verificar se passou o id da aba ou o objeto li da aba
                if (typeof curTab == 'string') {
                    curTab = $("#" + curTab.replace(/^#/, ''));
                }
                validTabs = [$(curTab).parent().attr('id')];
            };
            for (var key in validTabs) {
                //curTab = curTab || $("#"+validTabs[key] + " li.active");
                curTab = $("#" + validTabs[key] + " li.active");
                if (curTab) {
                    var divTab = $(curTab).find("a").attr('href');
                    var count = 0;
                    $(divTab).find('input,select,textarea').each(function() {
                        if ($(this).data('changed')) {
                            //log(this.name + ' modificado!');
                            count++;
                        }
                    })
                    if (count > 0) {
                        curTab.addClass('changed')
                    } else {
                        curTab.removeClass('changed')
                    }
                }
            }
            return count;
        };
        /**
         * Atualizar arquivo do sistema em /data/arquivos
         * @param params
         */
    var _updateArquivoSistema = function(params) {
            if (!$("#frmUpdateArquivoSistema").valid()) {
                return;
            };
            var data = $("#frmUpdateArquivoSistema").serializefiles();

            if(params.sqCicloAvaliacao){
                data.append('sqCicloAvaliacao', params.sqCicloAvaliacao);
            }

            data.append('originalFileName', data.get('fldArquivoSistema').name );
            app.ajax(app.url + 'main/updateArquivoSistema', data,
                function(res) {
                    if (res.status == 0) {
                        app.reset('frmUpdateArquivoSistema');
                        app.closeWindow('uploadArquivoSistema');
                    }
                }, null, 'json');
        };
    // utilizado nos filtros das fichas pelo nivel taxonomico
    var _filtroNivelTaxonomicoChange = function(params,element,event) {

        if (typeof event == 'undefined') {
            return;
        }
        params.rndId = params.rndId || '';
        var selectPai = $(event.target);
        var selectFilho = $("#sqTaxonFiltro" + params.rndId);
        selectFilho.empty();
        if ($(selectPai).val()) {
            $(selectFilho).parent().removeClass('hide');
        } else {
            $(selectFilho).parent().addClass('hide');
            if (params.callback && !app._clearingFilters) {
                for( key in params ) {
                    if( /^tooltipster/.test( key ) )
                    {
                        delete params[key];
                    }
                }
                app.eval(params.callback,params);
            }
        }
    };

    // utilizado nos filtros das fichas pela palavra chave de uma aba
    var _filtroNuAbaChange = function(params,element,event) {

        if (typeof event == 'undefined') {
            return;
        }
        params.rndId = params.rndId || '';
        var $select = $(event.target);
        var $div = $("#divDsPalavraChaveFiltro"+params.rndId);
        if( $select.val() == '' ){
            $div.css('visibility','hidden')
        } else {
            $div.css('visibility','visible')
            $div.find('input').focus();
        }

    };

    var _insertTextEditor = function(el) {
        if (typeof(event) != 'undefined') {
            event.preventDefault();
        }
        try {
            tinymce.activeEditor.execCommand('mceInsertContent', false, $(el).text());
        } catch (e) {}
    }

    var _serializeFields = function(container, data) {
        data = data || {};
        $("#" + container.replace('/^#/', '')).find('input,select').each(function() {
            var fld = $(this);
            var val = fld.val();
            if( fld.prop('type').toLowerCase() == 'checkbox' && !this.checked ) {
                val = ''
            }
            if (val) {
                var name = fld.attr('name');
                if( name ){
                    if( data[name] ) {
                        data[name] += ',' + fld.val()
                    } else {
                        data[name] = fld.val();
                    }
                }
            }
        });
        return data
    }

    var _clearFilters = function(params, btn) {
        if (typeof event != 'undefined') {
            event.preventDefault();
        }
        if (btn) {
            // fichar multiselect se existir algum aberto
            $(document).click();
            app._clearingFilters = true; // evitar chamar updateGridFichas nos callbacks dos campos que tiverem este evento
            var data = $(btn).data();
            // limpar os campos de filtragem da ficha
            if (data.rndId) {
                $("#divFiltrosFicha" + data.rndId).find('input,select').each(function() {
                    var regexinputsFiltro = new RegExp('Filtro'+data.rndId+'$');
                    if( regexinputsFiltro.test( this.id ) ) {
                        if ($(this).hasClass('select2')) {
                            if ($(this).select2('val')) {
                                $(this).empty();
                                $(this).trigger('change');
                            }
                        } else {
                            try {
                                // limpar campo select multiple
                                if ($(this).data('multiple')) {
                                    //$(this).next('div').find('input[type=checkbox]').prop('checked',false)
                                    $(this).multiselect('deselectAll',false);
                                    $(this).multiselect('refresh');
                                } else {
                                    if( this.type == 'checkbox' ) {
                                        this.checked=false;
                                    } else if( this.type == 'radio' ) {
                                        this.checked=false;
                                    } else {
                                        $(this).val('');
                                    };
                                    var dataChange = $(this).data('change');
                                    // não precisa chamar o evento click do botão filtrar a cada campo limpo
                                    if (dataChange && !/btnFiltroFicha/.test(dataChange)) {
                                        $(this).change();
                                    }
                                }
                            } catch (e) {
                                console.log('ClearFilters', e );
                            }
                        }
                    }
                });
                $("#divFiltrosFicha" + data.rndId).removeClass('filtered');
            } else {
                app.reset($(btn).closest('form').attr('id'));
            }
            // clicar no botão filtrar
            $("#btnFiltroFicha" + data.rndId).click();
            app._clearingFilters = false;
            $("#fldStPexFiltro"+data.rndId).addClass('hide');
            $("#chkAvaliadaNoCicloFiltro"+data.rndId).parent().css('margin-left','70px');
            // limpar os filtros das colunas do gride quando o idGrid for informado
            if( params.gridId ){
                $("#container"+params.gridId.capitalize()).parent().find('*.table-filter').each(function(index,item){
                    $(item).val( '')
                    $(item).removeClass('filtered-green');
                })
            }
            return;
        }

    };
    var _gerarZipFichas = function(params) {
        var data = {ids: []};
        params = params || {};
        var container = params.container ? '#' + params.container : '*';
        data.contexto = params.contexto ? params.contexto : 'ficha';
        $.extend(data, params);
        var pergunta = 'Confirma exportação das fichas no formato ('+( params.formato == 'rtf' ? 'doc' : 'pdf' )  +') para arquivo zip?';

        // ler linhas marcadas
        $(container + " input:checkbox[name^=chkFicha]:checked").each(function () {
            data.ids.push(this.value);
        });
        if( data.ids.length == 0 ) {
            $(container + " input:checkbox[name^=sqFicha]:checked").each(function () {
                data.ids.push(this.value);
            });
            if( data.ids.length == 0 ) {
                $( container + " input:checkbox[name^=chkSqFicha]:checked").each(function () {
                    data.ids.push(this.value);
                });
            }
            if( data.ids.length == 0 ) {
                $( container + " input:checkbox[name^=chkFicha]:checked").each(function () {
                    data.ids.push(this.value);
                });
            }
        }
        var msgTitle = 'Marque esta opção para exportar as fichas selecionadas ou desmarque para exportar todas as fichas'
        var msgPergunta = 'Somente das ' + data.ids.length + ' fichas selecionadas?'
        if (data.ids.length == 1) {
            msgTitle = 'Marque esta opção para exportar a ficha selecionada ou desmarque para exportar todas as fichas';
            msgPergunta = 'Somente da ficha selecionada?'
        }
        app.confirm('<br>' + pergunta +
            (data.ids.length > 0 ?
                    '<h3>Opções:</h3>' +
                    '<label class="cursor-pointer" title="' + msgTitle + '">' +
                    '<input type="checkbox" id="chkZipSelecionadas"' +
                    'checked="checked" class="checkbox-lg"' +
                    'value="S">&nbsp;&nbsp;' + msgPergunta + '</label>' : ''
            )
            , function () {
                data.sqCicloAvaliacao = $("#sqCicloAvaliacao").val();
                if ($("#chkZipSelecionadas").size() > 0) {
                    data.todasPaginas = $("#chkZipSelecionadas").is(':checked') ? 'N' : 'S';
                }
                if (data.todasPaginas == 'N') {
                    data.ids = data.ids.join(',');
                }
                else {
                    data.ids = '';
                }
                $.extend(data, app.hasActiveFilter(data.container));

                // para utilizar app.ajax() tem que limpar estas variáveis
                data.action = '';
                data.container = '';
                data.params = '';
                app.ajax(app.url + 'ficha/gerarZipFichas', data, function (res) {
                    if (res.status == 0) {
                        window.setTimeout(function () {
                            getWorkers();
                        }, 5000);
                        window.setTimeout(function () {
                            getWorkers();
                        }, 60000);
                        app.growl('Gerando arquivo zip' + (data.ids.length == 1 ? ' das fichas.' : '') + '. Ao finalizar será exibida a tela para download do arquivo.');
                    }
                }, '', 'JSON');
            }, null, null, 'Confirmação', 'warning');
    };
    /**
     * método para alterar a cor de fundo da div de filtros da ficha quando houver filtro selecionado
     * @param  string idContainer - id do elemento que contem os campos de filtro
     * @return Boolean
     */
    var _hasActiveFilter = function(idContainer, contexto ) {
        contexto = contexto || '';
        var dataFilters = {
            count: 0
        }
        if( !idContainer || idContainer == '*')
        {
            return dataFilters;
        }
        idContainer = idContainer.replace(/^#/, '');
        var $container = $( '#'+idContainer );

        //caso o conatainer informado não seja a divFiltrosFicha, encontrar a
        //divFiltrosFicha dentro do container que esteja com algum filtro ativo
        var $divFiltrosFicha = $container.find('div.filtered[id^=divFiltrosFicha]');
        if( $divFiltrosFicha.size() == 1 )
        {
            $container = $divFiltrosFicha
        }

        if( $container.size() == 0 )
        {
            return dataFilters;
        }
        if( $container.find('div[id^=' +idContainer +']').size() == 1 )
        {
            $container = $container.find('div[id^=' +idContainer +']');
        }
        // procurar pelo container de filtros de ficha dentro do container informado
        $divFiltrosFicha = $container.find('div[id^=divFiltrosFicha]');
        if( $divFiltrosFicha.get(0) )
        {
            $container = $( $divFiltrosFicha.get(0) )
        }

        //var fields = $container.find('input,select,checkbox,radio');
        var fields = $container.find('input[name$=Filtro],select[name$=Filtro]');
        var result = false;
        if (fields.length > 0) {
            $.each(fields, function(i, e) {
                try {
                    if( ! $(this).hasClass('fld-controle') ) {
                        var value = $(e).val();
                        if (value) {
                            if (/^(radio|checkbox)$/.test(e.type.toLowerCase())) {
                                result = e.checked;
                            } else {
                                result = true;
                            }
                            if (result) {
                                var fieldName = $(e).attr('name');
                                if (/Filtro$/.test(fieldName)) // campos terminados com "Filtro"
                                {
                                    if (e.tagName.toUpperCase() == 'SELECT') {
                                        if (value instanceof Array) {
                                            var arrTemp = []
                                            $(e).find('option:selected').each(function (i, option) {
                                                if ($(option).data('codigo')) {
                                                    arrTemp.push($(option).data('codigo'));
                                                } else {
                                                    arrTemp.push($(option).val());
                                                }
                                            });
                                            dataFilters[fieldName] = arrTemp.join(',');
                                        } else {
                                            if ($(e).find('option:selected').data('codigo')) {
                                                dataFilters[fieldName] = $(e).find('option:selected').data('codigo');
                                            } else if (value) {
                                                dataFilters[fieldName] = value;
                                            }
                                        }
                                    } else {
                                        dataFilters[fieldName] = value;
                                    }
                                    dataFilters.count++;
                                }
                            }
                        }
                    }
                } catch (e) {}
            });
        };
        // exceções
        if (dataFilters.nivelFiltro && !dataFilters.sqTaxonFiltro) {
            //delete dataFilters.nivelFiltro;
            dataFilters.count--;
        }

        if (dataFilters.count > 0) {
            var lsKey = 'filtrosFicha' + ( contexto? '-' : '' ) + contexto;

            // alterar a cor de fundo do container
            if( contexto !='validador') {
                $container.addClass('filtered');
            }
            // gravar os filtros no storage local
            try {
                app.lsSet( lsKey, JSON.stringify( dataFilters ) );
            } catch( e ) {}
            //console.log( 'filtrosFicha_'+$container.attr('id'));
            //console.log( dataFilters );
        } else {
            $container.removeClass('filtered')
        }
        return dataFilters
    };

    var _updateGridFichas = function(rndId, callback,required) {
        var data = app.hasActiveFilter('divFiltrosFicha' + rndId);

        // esconder multiselect aberto
        $( document ).click();

        if( data.count == 0 && required )
        {
            if( !app._clearingFilters ) {
                app.alertInfo('Nenhum filtro selecionado!');
            }
            return;
        }
        app.eval(callback, data);
    };


    var _btnFiltroFichaClick = function(evt, p2, newEvent ) {
        if ( newEvent ) {
            evt = newEvent
        }

        var rndId = evt.target.id.split('Filtro')[1];

        if( evt.target.tagName == 'INPUT' && evt.target.type=='text' )
        {
            $("#btnFiltroFicha" + rndId).click();
            return;
        };
        var selectCategoria = $("#sqCategoriaFiltro"+rndId);
        if( selectCategoria.length ) {
            if( $("#sqCategoriaFiltro"+rndId+" option:selected[data-codigo-sistema=CR]").size() > 0 )
            {
                $("#fldStPexFiltro"+rndId).removeClass('hide');
            }
            else
            {
                $("#fldStPexFiltro"+rndId).addClass('hide');
            }
        }

        var selectCategoriaAvaliacao = $("#sqCategoriaAvaliacaoFiltro"+rndId);
        if( selectCategoriaAvaliacao.length ) {
            if( $("#sqCategoriaAvaliacaoFiltro"+rndId+" option:selected[data-codigo-sistema=CR]").size() > 0 )
            {
                $("#fldStPexAvaliacaoFiltro"+rndId).removeClass('hide');
            }
            else {
                $("#fldStPexAvaliacaoFiltro" + rndId).addClass('hide');
            }
        }

        // localiar o botão do formulário de filtro das fichas e disparar o evento click do mesmo
        try {

            if ( rndId ) {
                if( ! $("#spanFiltrosFichaTipFiltrar"+rndId).data('fadeIn') ) {
                    $("#spanFiltrosFichaTipFiltrar"+rndId).data('fadeIn',true);
                    $("#spanFiltrosFichaTipFiltrar" + rndId).hide().fadeIn(300).fadeOut(6000);
                }
            }
            // mostrar/esconder os subgrupos em funçao do(s) grupo(s) selecionados
            if( /^sqGrupoFiltro/.test( evt.target.id ) )
            {
                var $grupo = $(evt.target );
                var gruposSelecionados = $grupo.val();
                var $subgrupo = $("#sqSubgrupoFiltro" + rndId);
                var valSubgrupo = $subgrupo.val();
                var $wrapper = $subgrupo.next();
                var count=0;
                $subgrupo.find("option").each( function( i, option ) {
                    var data = $(option).data();
                    if( gruposSelecionados && gruposSelecionados.includes( String(data.sqGrupo) ) )
                    {
                        $wrapper.find("input[value="+data.codigo+"]").closest('a').show();
                        count++;
                    } else {
                        $wrapper.find("input[value="+data.codigo+"]").closest('a').hide();
                        if( valSubgrupo ) {
                            // remover valor selecionado do subgrupo
                            valSubgrupo = valSubgrupo.filter(function (value) {
                                return String(value) != String(data.codigo);
                            });
                        }
                    }
                });
                $subgrupo.val( valSubgrupo );
                $subgrupo.multiselect('refresh');
                $subgrupo.multiselect(  ( count==0 ? 'disable' : 'enable' ) );
            }
        } catch (e) {}

    };

    var _updatePercentualImportacao = function(contexto) {
        if (window.grails.env.toUpperCase() == 'DESENV') {
            //log('Verificar percentual ' + new Date());
        }
        if (updatePercentualImportacaoRunning) {
            return
        }
        updatePercentualImportacaoRunning = true;
        $.post(baseUrl + 'main/getAndamentoImportacaoPlanilhas', {
            noLog: true,
            contexto: contexto
        }, function(res) {
            updatePercentualImportacaoRunning = false;
            if (res.data.length > 0) {
                $.each(res.data, function(k, v) {
                    $("#td_situacao_" + v.id).html('<span class="' + (v.percentual < 100 ? 'blue' : 'green') + '">' + v.situacao + '</span>');
                    $("#td_percentual_" + v.id).html(v.percentual + '%');
                });
                // ao fechar a janela de importação, parar o cronometro
                if ($("#table_importacao_planilhas td[id^='td_percentual_']").size() > 0) {
                    window.setTimeout(function() {
                        app.updatePercentualImportacao()
                    }, 2000);
                } else {
                    updatePercentualImportacaoRunning = false;
                }
            } else {
                updatePercentualImportacaoRunning = false;
            }
        }, null, 'JSON');
    };

    var _confirmarImportacaoPlanilha = function(data, onEnd) {
        if (!data || (!data.contexto && !data.get('contexto'))) {
            app.alertInfo('Contexto não informado.')
            return
        }
        app.confirm('<br>Confirma a IMPORTAÇÃO ?', function() {
            // aguardar o tempo da planilha ser salve e zerar o percentual na tabela salve.planilha
            window.setTimeout(function() {
                app.eval(onEnd); // exibir o gride limpo sem os logs anteriores
                app.updatePercentualImportacao(); // iniciar o monitoramento
            }, 2000);

            app.ajax(app.url + 'main/savePlanilhaRefBib', data,
                function(res) {
                    if (res.status == 1) {
                        return;
                    }
                    app.eval(onEnd);
                    if (res.status == 0) {
                        var msg = [];
                        var size = Math.round(res.fileSize / 1024 / 1024);
                        var und = 'Mb';
                        if (size == 0) {
                            size = Math.round(res.fileSize / 1024);
                            und = "Kb";
                        }
                        msg.push('Arquivo processado: <b>' + res.fileName + '</b>');
                        msg.push('Tamanho do Arquivo: <b>' + size + und + '</b>');
                        msg.push('Planilha: <b>' + res.sheetName + '</b>');
                        msg.push('Linha(s): <b>' + (res.linhas) + '</b>');
                        if (res.colsNotFound.length > 0) {
                            msg.push('Coluna(s) Inválida(s): <b>' + res.colsNotFound.join(', ') + '</b>');
                        }
                        if (res.qtdLinhasComErro) {
                            msg.push('<span style="color:' + (res.qtdLinhasComErro > 0 ? '#dc0000' : '#009500') + '">' + res.qtdLinhasComErro + ' Linha(s) com erro.</span>');
                        };
                        var unidadeTempo = 'seg';
                        if (res.seconds > 60) {
                            res.seconds = res.seconds / 60;
                            unidadeTempo = 'min';
                        }
                        msg.push('Duração ' + Math.round(res.seconds) + ' ' + unidadeTempo)
                        app.alert(msg.join('<br>'), 'Resultado da Importação');
                    }
                }, /*'Processando. Aguarde..'*/ null, 'JSON'
            );
        }, null, data, 'Confirmação', 'warning');
    };

    var _updateDashboard = function() {
            if (app.updatingDashboard) {
                return;
            };
            var $div = $("#appDivContainer")
            if( $div && $div.html().trim() != '')
            {
                return;
            };
            var data = {};
            app.updatingDashboard = true;
            $.post(baseUrl + "main/dashboard", data, function(res) {
                app.updatingDashboard = false;
                app.sleep(2,function(){
                    if (res && $('#divNewSession').size() == 0) {
                        if( $("#appDivContent").size() == 0 || $("#appDivContent").html().trim() == '' )
                        {
                            $("#appDivContainer").html(res);
                        }
                    };
                },res);

            }).always(function() {
                app.updatingDashboard = false;
            });
        } // fim updateDashboard

    var _tabClick = function(params, tab, evt) {
        var tabId = $(tab).parent().attr('id');
        if (/^liTabFichaCompletaValidacao?/.test(tabId)) {
            return !app.unselectTab(tabId);
        }
        return true
    };

    var _clearCache = function() {
        $.post(baseUrl + "main/clearCache", {}, function(res) {
            if (res) {
                app.alertInfo(res)
            }
        });
    };

    var _exportarOcorrenciaPortalbio = function(params)
    {
        var noCiclo = $('form[name="frmExportarOcorrenciaPortalbio"] select[name="sqCicloAvaliacao"] option:selected').text();
        var sqCicloAvaliacao = $('form[name="frmExportarOcorrenciaPortalbio"] select[name="sqCicloAvaliacao"] option:selected').val();
        if( !sqCicloAvaliacao )
        {
            app.alertInfo('Selecione o ciclo de avaliação!');
            return;
        }
        app.confirm('<h3>Confirma exportação das ocorrências do ciclo <b>'+noCiclo+'</b>?</h3>',function()
        {

            app.blockUI('Gerando arquivo...');
            $.post( baseUrl + "api/exportOccurrencesDwca", {sqCicloAvaliacao:sqCicloAvaliacao}, function(res) {
                app.unblockUI();
                if (res) {
                    if( res.indexOf('erro') == -1 )
                    {
                        window.open(app.url + 'main/download?fileName=' + res + '&delete=1', '_top');
                    }
                    else {
                        app.alertError(res);
                    }
                }
            });
        });
    };

    var _exportarGrideOcorrenciaCsv = function(params, fld, evt) {
        params.container = params.container || 'divFiltrosFicha';
        params.contexto = params.contexto || 'ficha';
        var idFichas = [];
        params.full = params.full || 'N';

        if (!params.sqFicha) {
            if (params.gridId) {

                $("#" + params.gridId.replace(/^#/, '') + " input:checkbox:checked:visible[name=chkSqFicha]").each(function () {
                    idFichas.push(this.value);
                });
            }
            /*else {
                return;
            }
            */
        } else {
            if (!Array.isArray(params.sqFicha)) {
                idFichas.push(params.sqFicha);
            }
        }
        if (Array.isArray(params.sqFicha)) {
            idFichas = params.sqFicha;
        }
        /*if (idFichas.length == 0) {
            app.alertInfo('Nenhum ficha selecionada para exportação!');
            return;
        }
        */
        //}
        // verificar se tem alguma ficha com ocorrencia sensivel para mostrar/esconder o checkbox
        $.ajax({
                url: app.url + 'ficha/hasSensitiveOccurrence'
                , type: 'POST'
                , cache: false
                , timeout: 0
                , dataType: 'json'
                , data: { idFichas:idFichas.join(',') }
            }).done(function ( res ) {
                setTimeout(function(){
                    if( res.data.result ) {
                        $("#pChkCsvSensivel").show();
                    }
                },1000)
        });
        var msgTitle = 'Marque esta opção para exportar as ocorrências das fichas selecionadas ou desmarque para exportar de todas as fichas';
        var msgPergunta = 'Somente das ' + idFichas.length + ' fichas selecionadas?';
        var msgPerguntaUtilizados ='Exportar SOMENTE os registros utilizados na avaliação.'
        var msgPerguntaNaoUtilizados ='Exportar SOMENTE os registros NÃO utilizados na avaliação.'
        //var msgPerguntaAdicionadosAposAvaliacao ='Exportar SOMENTE os registros adicionados após a avalição.'
        var msgUtilizados ='Marque esta opção para exportar somente os registros UTILIZADOS na avaliação'
        var msgNaoUtilizados ='Marque esta opção para exportar somente os registros NÃO UTILIZADOS na avaliação'
        //var msgAcicionadosAposUltimaAvaliacao ='Marque esta opção para exportar somente os registros que estão marcados como ADICIONADOS APÓS A ÚLTIMA AVALIAÇÃO'

        if (idFichas.length == 1) {
            msgTitle = 'Marque esta opção para exportar as ocorrências da ficha selecionada ou desmarque para exportar de todas as fichas';
            msgPergunta = 'Somente da ficha selecionada?';
        }

        if (params.contexto == 'abaDistribuicao') {
            msgTitle = '';
            msgPergunta = '';
        }
        var aviso1 = '<div class="avisos"><p><b>SALVE - Exportação de Registros de Ocorrência</b></p>'+
            '<p>Ao baixar a planilha e utilizar os dados a partir do SALVE, você concorda em:'+
            '<ul><li>citar os autores e as bases ou sistemas de origem dos dados quando estes forem utilizados na elaboração de publicação ou outro tipo de trabalho ou produto, a não ser quando especificado de forma diferente pelo autor.</li></ul></p>'+
            '</div>';

        var aviso2 = 'Ao baixar a planilha e utilizar os dados a partir do SALVE, você está ciente que dados EM CARÊNCIA:' +
            '<ul><li>não poderão ser disponibilizados diretamente em publicações técnicas ou científicas (a não ser quando autorizado formalmente pelo autor ou quando envolver análises e sínteses em níveis taxonômicos iguais ou superiores à Ordem);</li>' +
            '<li>apenas poderão ser disponibilizados a terceiros quando estiverem cientes das condições aqui especificadas (preferencialmente por escrito, quando tratar-se de pessoa externa ao ICMBio);</li></ul>';

        var linkPoliticaDados = '<div class="avisos mt15"><p><a target="_blank" href="/salve-estadual/main/download?fileName=politica_dados_avaliacao"><i class="fa fa-external-link" aria-hidden="true"></i>&nbsp;Acesse a íntegra da Política de Dados e Informações do processo de avaliação do estado de conservação da fauna brasileira.</a></p></div>'
        var opcoes = [];

        opcoes.push('<label class="cursor-pointer" style="margin-bottom:0px;" title="' + msgUtilizados + '"><input type="radio" name="chkCsvUtilizados" value="U"  id="chkCsvUtilizados" checked="true" class="checkbox-lg">&nbsp;&nbsp;' + msgPerguntaUtilizados + '</label><br>');
        opcoes.push('<label class="cursor-pointer" style="margin-bottom:0px;" title="' + msgNaoUtilizados + '"><input type="radio" name="chkCsvUtilizados" value="NU" id="chkCsvNaoUtilizados"  class="checkbox-lg">&nbsp;&nbsp;' + msgPerguntaNaoUtilizados + '</label><br>');
        //opcoes.push('<label class="cursor-pointer" style="margin-bottom:0px;" title="' + msgAdicionadosAposUltimaAvaliacao + '"><input type="radio" name="chkCsvAdicionadosAposUltimaAvaliacao" value="NU" id="chkCsvAdicionadosAposUltimaAvaliacao"  class="checkbox-lg">&nbsp;&nbsp;' + msgPerguntaAdicionadosAposAvaliacao + '</label><br>');
        opcoes.push('<label class="cursor-pointer" style="margin-bottom:10px;" title="Exportar todos os registros"><input type="radio" name="chkCsvUtilizados" value="T"  id="chkCsvUtilizadosNaoUtilizados"  class="checkbox-lg">&nbsp;&nbsp;Todos</label>');

        //if (!isExterno && ! window.grails.perfilConsulta ) {
        if ( ! window.grails.perfilExterno ) {
            opcoes.push('<p style="display:none" id="pChkCsvSensivel"><label class="cursor-pointer red"><input type="checkbox" id="chkCsvSensivel" class="checkbox-lg" checked value="S">&nbsp;&nbsp;Exportar os registros sensíveis?</label></p>');
            opcoes.push('<p><label class="cursor-pointer red"><input type="checkbox" data-action="chkCsvCarenciaClick" id="chkCsvCarencia" class="checkbox-lg" value="S">&nbsp;&nbsp;Exportar os registros EM CARÊNCIA?</label></p>' +
                '<div id="divConcordarPoliticaDados" class="alert alert-danger" style="display:none;">' + aviso2 + '<label class="cursor-pointer"><input type="checkbox" id="chkConcordarPoliticaDados" class="checkbox-lg">&nbsp;<b>Declaro que li e aceito as condições de uso acima descritos.</b></label></p></div>'
            );
        }
        if (idFichas.length > 0 && msgTitle) {
            opcoes.push('<br><label class="cursor-pointer" style="margin-bottom:10px;" title="' + msgTitle + '"><input type="checkbox" id="chkCsvSelecionadas" checked="checked" class="checkbox-lg" value="S">&nbsp;&nbsp;' + msgPergunta + '</label>');
        }
        msgPergunta = '<div class="avisos"><p><b>Confirma a exportação das ocorrências?</b></p>';
        if (opcoes.length > 0) {
            msgPergunta += '<p style="margin-bottom:0;">Opções:</p>' + opcoes.join('');
        }
        msgPergunta += '</div>';

        var w = app.confirm( aviso1 + msgPergunta + linkPoliticaDados, function () {}, function () {
        }, params, 'Confirmação', 'warning');

        /** reprogramar o botão SIM para verificar se concordou com a politica de dados*/
        w.getButtons()[0].action = function (e) {
            if ( $("#chkCsvCarencia").is(':checked') && !$("#chkConcordarPoliticaDados").is(":checked")) {
                app.growl('Para exportar os registros em carência você deve concordar com os termos da política de dados.', 5, '<b>Atenção</b>', 'tc', 'large', 'error');
            } else {
                //w.getButtons()[0].action = defaultAction;
                //var btn = w.$modalFooter.find('button');
                var data = {
                    sqFicha: idFichas.join(','),
                    contexto: params.contexto,
                    sqOcorrenciasSelecionadasSalve: (params.sqOcorrenciasSelecionadasSalve ? params.sqOcorrenciasSelecionadasSalve : ''),
                    sqOcorrenciasSelecionadasPortalbio: (params.sqOcorrenciasSelecionadasPortalbio ? params.sqOcorrenciasSelecionadasPortalbio : ''),
                    sqCicloAvaliacao: $("#sqCicloAvaliacao").val(),
                    excluirRegistrosSensiveis: ($("#chkCsvSensivel").is(':checked') ? 'N' : 'S'),
                    excluirRegistrosEmCarencia: ($("#chkCsvCarencia").is(':checked') ? 'N' : 'S'),
                    somenteUtilizadosAvaliacao: $("input[name=chkCsvUtilizados]:checked").val()
                };
                if ( $("#chkCsvSelecionadas").size() > 0 ) {
                    data.todasPaginas = $("#chkCsvSelecionadas").is(':checked') ? 'N' : 'S';
                    if( data.todasPaginas == 'S' )
                    {
                        delete data.sqFicha;
                    }
                } else {
                    if( data.sqFicha ) {
                        data.todasPaginas = 'N';
                    } else {
                        data.todasPaginas = 'S';
                    }
                }
                $.extend(data,app.hasActiveFilter( params.container) );
                exportarOcorrenciasCsv(data);
                w.close();
            };
        };
    };

    // novo
    var _dialogoExportarOcorrencias = function( params, callback ) {
        params = params || {};
        var rndId                      = getRandomString(6);
        var linkPoliticaDados           ='<div class="avisos mt15"><p><a target="_blank" href="/salve-estadual/main/download?fileName=politica_dados_avaliacao"><i class="fa fa-external-link" aria-hidden="true"></i>&nbsp;Acesse a íntegra da Política de Dados e Informações do processo de avaliação do estado de conservação da fauna brasileira.</a></p></div>'
        var msgPerguntaUtilizados       ='Exportar SOMENTE os registros utilizados na avaliação.'
        var msgPerguntaNaoUtilizados    ='Exportar SOMENTE os registros NÃO utilizados na avaliação.'
        var msgPerguntaAdicionadosAposAvaliacao ='Exportar SOMENTE os registros confirmados e adicionados após a avaliação.'

        var msgAvisoRegistrosPublico    = '<p style="color:red;font-width: bold;" class="blink-me">Somente os registros de acesso público serão exportados.</p>';
        var titleUtilizados             ='Marque esta opção para exportar somente os registros UTILIZADOS na avaliação'
        var titleNaoUtilizados          ='Marque esta opção para exportar somente os registros NÃO UTILIZADOS na avaliação'
        var titleSomenteSelecao         ='Marque esta opção para exportar as ocorrências das fichas selecionadas ou desmarque para exportar de todas as fichas';
        var titleAdicionadoAposAvaliacao='Marque esta opção para exportar os registros confirmados e adicionadas após a avalição.';
        var opcoes = [];

        var aviso1 = '<div class="avisos"><p><b>SALVE - Exportação de Registros de Ocorrência</b></p>'+
            '<p>Ao baixar a planilha e utilizar os dados a partir do SALVE, você concorda em:'+
            '<ul><li>citar os autores e as bases ou sistemas de origem dos dados quando estes forem utilizados na elaboração de publicação ou outro tipo de trabalho ou produto, a não ser quando especificado de forma diferente pelo autor.</li></ul></p>'+
            '</div>';

        var aviso2 = 'Ao baixar a planilha e utilizar os dados a partir do SALVE, você está ciente que dados EM CARÊNCIA:' +
            '<ul><li>não poderão ser disponibilizados diretamente em publicações técnicas ou científicas (a não ser quando autorizado formalmente pelo autor ou quando envolver análises e sínteses em níveis taxonômicos iguais ou superiores à Ordem);</li>' +
            '<li>apenas poderão ser disponibilizados a terceiros quando estiverem cientes das condições aqui especificadas (preferencialmente por escrito, quando tratar-se de pessoa externa ao ICMBio);</li></ul>';

        opcoes.push('<label class="cursor-pointer" style="margin-bottom:0px;" title="' + titleUtilizados + '"><input type="radio" name="chkCsvUtilizados'+rndId+'" value="S"  id="chkCsvUtilizados'+rndId+'" checked="true" class="checkbox-lg">&nbsp;&nbsp;' + msgPerguntaUtilizados + '</label><br>');
        opcoes.push('<label class="cursor-pointer" style="margin-bottom:0px;" title="' + titleNaoUtilizados + '"><input type="radio" name="chkCsvUtilizados'+rndId+'" value="N" id="chkCsvNaoUtilizados'+rndId+'"  class="checkbox-lg" >&nbsp;&nbsp;' + msgPerguntaNaoUtilizados + '</label><br>');
        opcoes.push('<label class="cursor-pointer" style="margin-bottom:0px;" title="' + titleAdicionadoAposAvaliacao + '"><input type="radio" name="chkCsvUtilizados'+rndId+'" value="AAA" id="chkCsvAdicionadosAposAvaliacao'+rndId+'"  class="checkbox-lg" >&nbsp;&nbsp;' + msgPerguntaAdicionadosAposAvaliacao + '</label><br>');
        opcoes.push('<label class="cursor-pointer" style="margin-bottom:10px;" title="Exportar todos os registros"><input type="radio" name="chkCsvUtilizados'+rndId+'" value="T"  id="chkCsvUtilizadosNaoUtilizados'+rndId+'"  class="checkbox-lg">&nbsp;&nbsp;Todos</label>');

        if( params.showCheckSensivel ) {
            if( params.sensivel ) {
                opcoes.push('<p id="pChkCsvSensivel' + rndId + '"><label class="cursor-pointer red">' +
                    '<input type="checkbox" id="chkCsvSensivel' + rndId + '" class="checkbox-lg" checked value="S">&nbsp;&nbsp;Exportar os registros sensíveis?</label></p>');
            }
        }
        if( params.showCheckCarencia ) {
            msgAvisoRegistrosPublico='';
            opcoes.push('<p><label class="cursor-pointer red"><input type="checkbox" data-action="chkCsvCarenciaClick" id="chkCsvCarencia' + rndId + '" class="checkbox-lg" value="S">&nbsp;&nbsp;Exportar os registros EM CARÊNCIA?</label></p>' +
                '<div id="divConcordarPoliticaDados" class="alert alert-danger" style="display:none;">' + aviso2 + '<label class="cursor-pointer">' +
                '<input type="checkbox" id="chkConcordarPoliticaDados' + rndId + '" class="checkbox-lg">&nbsp;<b>Declaro que li e aceito as condições de uso acima descritos.</b></label></p></div>');
        }

        if ( params.selecao ) {
            opcoes.push('<p><label class="cursor-pointer" style="margin-bottom:10px;" title="' + titleSomenteSelecao + '">' +
                '<input type="checkbox" id="chkCsvSelecionadas'+rndId+'" checked="checked" class="checkbox-lg" value="S">&nbsp;&nbsp;' + msgPergunta + '</label></p>');
        }

        if( params.enviarEmail){
            opcoes.push('<p><label class="cursor-pointer" style="margin-bottom:10px;" title="Desmarque esta opção caso NÃO queira receber a planilha com os dados exportados no seu e-mail">' +
                '<input type="checkbox" id="chkEnviarPorEmail'+rndId+'" class="checkbox-lg" value="S">&nbsp;&nbsp;Enviar para meu e-mail.</label></p>');
        }

        var msgPergunta = '<div class="avisos"><p><b>Confirma a exportação das ocorrências?</b></p>' + msgAvisoRegistrosPublico;


        if (opcoes.length > 0) {
            msgPergunta += '<p style="margin-bottom:5px;">Opções:</p>' + opcoes.join('');
        }
        msgPergunta += '</div>';

        var content = '<div id="content'+rndId+'" style="padding:5px;">' + aviso1 + msgPergunta + linkPoliticaDados + '</div>';

        var dialog = app.confirm( content, null,null,params , 'Exportação de Registros', 'primary',null,null,null,'wide');

        /** reprogramar o botão SIM para verificar se concordou com a politica de dados*/
        dialog.getButtons()[0].action = function (e) {
            if ($("#chkCsvCarencia" + rndId).is(':checked') && !$("#chkConcordarPoliticaDados" + rndId).is(":checked")) {
                app.growl('Para exportar os registros em carência você deve declarar que leu os termos da política de dados.', 5, '<b>Atenção</b>', 'tc', 'large', 'error');
            } else {
                params.dlgConfirm                   = true;
                params.enviarEmail                  = ( params.enviarEmail && $("#chkEnviarPorEmail" + rndId).is(':checked') ? 'S' : 'N');
                params.excluirRegistrosSensiveis    = ( ! params.sensivel || $("#chkCsvSensivel" + rndId).is(':checked') ? 'N' : 'S');
                params.excluirRegistrosEmCarencia   = ($("#chkCsvCarencia" + rndId).is(':checked') ? 'N' : 'S');
                params.utilizadosAvaliacao          = $("input[name=chkCsvUtilizados" + rndId + "]:checked").val()
                if ($("#chkCsvSelecionadas"+rndId).size() > 0) {
                    params.todasPaginas = $("#chkCsvSelecionadas"+rndId).is(':checked') ? 'N' : 'S';
                    if (params.todasPaginas == 'S') {
                        delete params.sqFicha;
                    }
                } else {
                    if( params.qtdFichas == 0 ) {
                        params.todasPaginas = 'S';
                    } else {
                        params.todasPaginas = 'N';
                    }
                }
                if( params.container ) {
                    $.extend(params, app.hasActiveFilter( params.container ) );
                }
                if( typeof callback == 'function' ) {
                    try{callback( params );} catch( e ){}
                }
                dialog.close();
                /*window.setTimeout(function(){
                    app.alertInfo('Somente os registros de acesso público serão exportados.','AVISO');
                },1000);*/

            };
        };
    }

    var _setDefaultText = function(params,ele,evt) {
        var check = evt.target;
        var data = $(check).data();

        if( ! data.target )
        {
            return;
        }
        if( !data.text )
        {
            data.text = 'Não foram encontradas informações para o táxon.';
        };
        if( data.target=='dsPresencaUc' ) {
            if( $("#divGridPresencaUc").find('tbody tr').size() > 0){
                app.alertInfo('Utilize esta opção somente quando não houver UC registrada na ficha.');
                check.checked=false;
                return;
            }
        }
        var editor = tinymce.get(data.target)
        if( ! editor )
        {
            return
        };
        if( check.checked ) {
            if( editor.getContent({format : 'text'}).trim() == '' )
            {
                $("#"+data.target).val(data.text);
                editor.setContent(data.text);
                app.fieldChange(check);
                if( params.target == 'dsPopulacao') {
                    var id = $(check).data('id');
                    $("select[name=sqTendenciaPopulacional]").val(id);
                }
            }
            else {
                app.alertInfo('Remova o texto atual da descrição antes de aplicar o texto padrão!');
                check.checked =false;
            }
        } else
        {
            if( editor.getContent({format : 'text'}).trim().indexOf( data.text ) != -1 )
            {
                $("#"+data.target).val('');
                editor.setContent('');
                app.fieldChange(check);
            }
        }

    };

    var _showText = function(params, ele, evt)
    {
        // quando é chamado a partir do popup do mapa vem só o elemento clicado
        if( !params && ele ) {
            params = $(ele).data();
        }
        var maxHeight = window.innerHeight - 130;
        app.openWindow({
            id: 'showText',
            title: 'Texto completo',
            url: app.url + 'main/getText',
            data:params,
            height: Math.max( 300, maxHeight / 2),
            width:  Math.max( 400,( $(window).width()) / 2),
            reload: true,
            autoOpen: true,
            modal: true
        }, function(data, e) {
            $('#' + data.id + ' div.modal-body').css({'background-color':'#ebf1f1','text-align':'justify'});
            var html =  $('#' + data.id + ' div.modal-body').html().replace(' background-color: #ffffff;','');
            $('#' + data.id + ' div.modal-body').html(html);
        },function(data,e) {
            $('#' + data.id + ' div.modal-body').html('');
        });

    };

    var _showLongText = function( texto, title )
    {
        title = title ? title : 'Visualização do texto';
        var data ={conteudo:texto.trim().replace(/&nbsp;/g,' ')}
        app.openWindow({
            id: 'showLongText',
            title: title,
            data:data,
            autoOpen: true,
            modal: true
        }, function(data, e) {
            $('#' + data.id + ' div.modal-body').html( data.data.conteudo );
            $('#' + data.id + ' div.modal-body').css({'background-color':'#ebf1f1','text-align':'justify'});
        },function(data,e) {
            $('#' + data.id + ' div.modal-body').html('');
        });
    };

    var _relatorio = function( codigo,altura,largura )
    {
        var data ={title:'',url:'',id:'winRelatorio'}
        switch( codigo )
        {
            case 'PF_CT':
                data.title = 'Relatório de Pontos Focais e/ou Coordenadores de Táxons';
                data.url = 'relatorio/rel001';
                data.codigo = codigo
        }
        if( ! data.title )
        {
            app.alertError('Relatório não informado!');
            return;
        }

        app.openWindow({
            id: data.id,
            title: data.title,
            url:data.url,
            data:data,
            width:(largura?largura:800),
            height:(altura?altura:600),
            autoOpen: true,
            modal: true,
            reload:true
        }, function(data, e) {
            var $divBody = $('#' + data.id + ' div.modal-body');
            $divBody.css({'background-color':'#ebf1f1','text-align':'left'});
        },function(data,e) {
            $('#' + data.id + ' div.modal-body').html('');
        });
    };

    /**
     * mostrar/esconder campos do critério iucn em função da categoria selecionada
     * @param params
     * @private
     */
    var _categoriaIucnChange = function( params ) {
        if (!params.container) {
            return
        }
        if (!params.sqFicha) {
            return
        }
        // colocar o ID no padrão #xxxx
        params.container = '#' + params.container.replace(/^#/, '');

        // ler o os campos que possuem validação
        var sqCategoriaIucnUltimaAvaliacao  = $(params.container + " input[name=sqCategoriaIucnUltimaAvaliacao]").val();
        var cdCategoriaIucnUltimaAvaliacao = $(params.container + " input[name=cdCategoriaIucnUltimaAvaliacao]").val();
        var $sqCategoriaIucn                = $(params.container + " select[name=sqCategoriaIucn] option:selected");
        var $dsCriterioAvalIucn             = $(params.container + " input[name=dsCriterioAvalIucn]");
        var $deCategoriaOutra               = $(params.container + " input[name=deCategoriaOutra]");
        var $stPossivelmenteExtinta         = $(params.container + " input[name=stPossivelmenteExtinta]");
        var $divDadosMotivoMudanca          = $(params.container + " #divDadosMotivoMudanca"+params.sqFicha);
        var $divDadosCitacao                = $(params.container + " #divDadosCitacao"+params.sqFicha);
        var cdCategoriaIucn                 = $sqCategoriaIucn.data('codigo');
        // se a categoria for CR exibir campo possivelmente extinta
        if ( cdCategoriaIucn == 'CR') {
            $stPossivelmenteExtinta.parent().parent().show();
        } else {
            $stPossivelmenteExtinta.parent().parent().hide();
        }

        // se a categoria for ameacada ('VU,CR,EN) não exibir o campo critério'
        if ( /EN|VU|CR/.test( cdCategoriaIucn ) && cdCategoriaIucn != 'POSSIVELMENTE_EXTINTA' ) {
            $dsCriterioAvalIucn.parent().show();
        } else {
            $dsCriterioAvalIucn.parent().hide();
        }

        // campo outra quando for avaliacao regional
        if( $deCategoriaOutra.size() == 1 ) {
            // se a categoria for OUTRA exibir o campo para informar a categoria
            if ( cdCategoriaIucn == 'OUTRA') {
                $deCategoriaOutra.parent().show()
                app.focus('#deCategoriaOutra', params.container);
            } else {
                $deCategoriaOutra.parent().hide();
            }
        }


        // campo da citacao / autoria
        if( $divDadosCitacao.size() == 1 ) {
            if ( $sqCategoriaIucn.val() == '' ||
                cdCategoriaIucn == 'NE' ) {
                $divDadosCitacao.hide();
            } else {
                $divDadosCitacao.show();
            }
        }

        // campos mudanca de categoria
        if( $divDadosMotivoMudanca.size() == 1 ) {
            if ( ! sqCategoriaIucnUltimaAvaliacao ||
                cdCategoriaIucnUltimaAvaliacao == 'NE' ||
                $sqCategoriaIucn.val() == '' ||
                cdCategoriaIucn == 'NE' ||
                (sqCategoriaIucnUltimaAvaliacao == $sqCategoriaIucn.val()) ) {
                $divDadosMotivoMudanca.hide();
            } else {
                $divDadosMotivoMudanca.show();
                app.updateGridMotivoMudancaCategoria(params);
            }
        }

    };

    var _repetirUltimaAvaliacao = function( params, ele, evt) {
        if( !params.container )
        {
            app.alertInfo('container não especificado.');
        }
        if( !params.sqFicha )
        {
            app.alertInfo('ficha  não especificada.');
        }
        // ajustar o id do container
        params.container = '#' + params.container.replace(/^#/, '');
        var $container = $( params.container );
        if( $container.size() == 0 )
        {
            app.alertInfo('_repetirUltimaAvaliacao: container não encontrado!');
            return;
        }

        app.confirm('<br>Deseja repetir os dados da última avaliação nacional?',
            function() {
                // limpar o formulario
                var $sqCategoriaIucn                = $container.find("select[name=sqCategoriaIucn]");
                var $dsCriterioAvalIucn             = $container.find("input[name=dsCriterioAvalIucn]");
                var $stPossivelmenteExtinta         = $container.find("input[name=stPossivelmenteExtinta]");
                app.ajax(baseUrl + 'fichaCompleta/repetirUltimaAvaliacao',
                    params,
                    function(res) {
                        if (typeof res.sqCategoriaIucn != 'undefined' && res.sqCategoriaIucn > 0) {
                            trimEditor( tinyMCE.get('dsJustificativa' + params.sqFicha).setContent('') );
                            $sqCategoriaIucn.val(res.sqCategoriaIucn);
                            $dsCriterioAvalIucn.val(res.deCriterioAvaliacaoIucn);
                            $stPossivelmenteExtinta.prop('checked', ( res.stPossivelmenteExtinta == 'S') );
                            tinymce.get('dsJustificativa'+params.sqFicha).setContent( res.txJustificativaAvaliacao );
                            app.categoriaIucnChange(params);
                            app.growl('Dados última avaliação recuperados com SUCESSO!');
                        } else {
                            app.alertInfo('Nenhuma avaliação NACIONAL cadastrada na aba 7-Conservação.');
                        }
                    });
            });

    };

    var _updateGridMotivoMudancaCategoria = function( params ) {
        var data = {};
        var $divMotivoMudanca;

        if( !params.container )
        {
            app.alertInfo('app.js/_updateGridMotivoMudancaCategoria() : container não especificado.');
        }
        if( !params.sqFicha )
        {
            app.alertInfo('app.js/_updateGridMotivoMudancaCategoria() : ficha não especificada.');
        }
        // ajustar o id do container
        params.container = '#' + params.container.replace(/^#/, '');
        var $container = $( params.container );
        if( $container.size() == 0 )
        {
            app.alertInfo('app.js/_updateGridMotivoMudancaCategoria(): container inválido!');
            return;
        }

        // verificar se o container tem o campo hidden canModify
        var $inputCanModify = $(params.container ).find('input#canModify');
        data.canModify = canModify; // global ao carregar a ficha
        if( typeof( params.canModify) != 'undefined')
        {
            data.canModify = params.canModify == 'false' || ! params.canModify ? false : true;
        }
        if( $inputCanModify.size() == 1)
        {
            data.canModify = $inputCanModify.val().toLowerCase() =='true' || $inputCanModify.val().toLowerCase() == 'S';
        }

        $divMotivoMudanca = $container.find("#divDadosMotivoMudanca" + params.sqFicha );
        if ($divMotivoMudanca.size() == 0) {
            return;
        }
        data.sqFicha = params.sqFicha;
        data.container = params.container;
        app.ajax(baseUrl + 'fichaCompleta/getGridMotivoMudanca',
            data,
            function (res) {
                var $divGrid = $divMotivoMudanca.find("#divGridMotivoMudancaCategoria");
                $divGrid.html( res );
                // esconder o motivo da mudanca se nao houver motivo cadastrado e não for nas abas 11.6 ou 11.7
                var isAbas11 =  /tabAvaResultado$|tabAvaResultadoFinal$/.test(params.container);
                var $selectCategoriaDisabled = $container.find('select[name=sqCategoriaIucn]').is(":disabled");
                //if( $divGrid.find('table>tbody>tr').size() == 0 && ( $container.find('select[name=sqCategoriaIucn]').is(":disabled") ) ) {
                if( $divGrid.find('table>tbody>tr').size() == 0 && $selectCategoriaDisabled && !isAbas11 ) {
                    $divMotivoMudanca.hide();
                } else {
                    $divMotivoMudanca.show();
                }
            }, '', 'HTML');

    };

    var _showHideContainer = function( params, e, evt ) {
        var container='';
        if( params.target )
        {
            params.target = "#"+params.target.replace(/^#/g,'');
        }
        else {
            return;
        }
        var container = $( params.target );
        if( container.size() == 0 )
        {
            return;
        }
        if( container.hasClass('hidden') )
        {
            container.removeClass('hidden');
            if( $(e).find('i.fa').hasClass('fa-chevron-down') ){
                $(e).find('i.fa').removeClass('fa-chevron-down').addClass('fa-chevron-up');
            } else {
                $(e).find('i.fa').removeClass('fa-plus').addClass('fa-minus');
            }
            if( params.focus )
            {
                app.focus(params.focus);
            }
            if( params.callback )
            {
                var callback = params.callback;
                delete params.callback;
                app.eval(callback,params);
            }
        }
        else
        {
            container.addClass('hidden');
            if( $(e).find('i').hasClass('fa-chevron-up') ){
                $(e).find('i.fa').removeClass('fa-chevron-up').addClass('fa-chevron-down');
            } else {
                $(e).find('i.fa').removeClass('fa-minus').addClass('fa-plus');
            }
        }
    };

    var _saveField = function( fieldId ) {
        // guardar a ultimo ciclo selecionado
        var $field = $('#' + fieldId.replace(/^#/,'') );
        if( $field.size() == 1 && !!$field.val() && $field.val() != '' && $field.val() != 'null' ) {
            app.lsSet( window.grails.userCpf + '.' + fieldId, $field.val() );
        }
    };

    var _restoreField = function( fieldId, callback, params ) {
        var $field = $('#' + fieldId.replace(/^#/,'') );
        if( $field.size() == 1 ) {
            var lastValue = app.lsGet(window.grails.userCpf+'.'+fieldId,'');
            if( lastValue ) {
                if( $field[0].tagName.toLowerCase()=='select'){
                    if( $field.find("option[value="+lastValue+"]").size() == 1 ) {
                        $field.val( lastValue );
                    } else {
                        return;
                    }
                } else {
                    $field.val( lastValue );
                }
                if( callback ) {
                    window.setTimeout(function() {
                        callback( params );
                    },500)
                }
            }
        }
    };
    /**
     * função utilizada juntamente com a tagLig paginate para fazer a requisição ajax da paginação de dados.
     * Os parametros necessários para a paginação serão passados na propriedade "DATA" do botão da paginação clicado
     * @param ele - botão da paginação clicado
     * @param page - número da página solicitada
     * @private
     */
    var _tablePagination = function (ele, page) {
        if (event) {
            event.preventDefault();
        };
        var $ul = $(ele).closest('ul');
        var data = $ul.data();
        var tableId = "#" + data.paginationTableId;
        var $tbody = $(tableId).find('tbody');
        var $thead = $(tableId).find('thead');
        var $navPagination = $("nav" + tableId + 'Pagination');
        var urlAjax = baseUrl + data.paginationUrl;
        var blockId = data.paginationBlockId ? "#" + data.paginationBlockId : tableId;
        var blockMessage = ( data.paginationBlockMessage ? data.paginationBlockMessage : 'Atualizando...' );
        var params = data.paginationParams;
        // filtros que estavam sendo utilizados no momento da paginação
        var dataFilters = data.paginationFilters;
        // colocar os filtros no formato correto para a api fichaService.search
        if (dataFilters) {
            for (var key in dataFilters) {
                data[key] = dataFilters [key];
            }
        }
        data.paginationPageNumber = page;

        try {
            // filtros dos inputs no cabecalho da tabela
            $thead.find("*.tableHeaderInputFilter").map(function (index, field) {
                var $field = $(field);
                if ($field.val()) {
                    if ($field.data('field')) {
                        data[$field.data('field')] = $field.val()
                    }
                }
            });
        } catch( e ) {}

        // adicionar parametros complementares quando informados
        if (params) {
            params.split(',').map(function (item, i) {
                var arrTemp = item.split(':');
                data[arrTemp[0]] = (arrTemp.length == 2 ? arrTemp[1] : $("#" + arrTemp[0]).val());
            });
        }
        //data.sqCicloAvaliacao = publicacao.sqCicloAvaliacao;
        app.blockElement( blockId,blockMessage );
        // excluir parametros desnecessários
        try {
            delete data.paginationFilters;
            delete data.paginationTableId;
            delete data.paginationUrl;
            delete data.paginationBlockId;
            delete data.paginationParams;
        } catch ( e ) {}
        app.ajax(urlAjax, data, function (res) {
            // atualizar as linhas do gride
            $tbody.html($(res).find('tbody').html());
            // atualizar a paginação
            $navPagination.html( $(res).find("#" + $navPagination.attr('id') ).html() );
            app.unblockElement(blockId);
        }, '', 'HTML');
    };

    /**
     * função para verificar se existe algum informativo tipo alerta não lido pelo usuário
     * e exibi-lo.
     * Pode ser informado o id de um informativo específico para que este seja
     * visualizado novamente
     * @private
     */
    var _verificarInformativoAlerta = function ( sqInformativo ) {
        // se janela de alerta estiver aberta abortar
        var $modal = $("#modalAlertaInformativos");
        var data = {sqInformativo:sqInformativo, noLog:true}; // no log para não ficar imprimindo na tela o request
        if( $modal.is(":visible") ) {
            return;
        }
        $.post( app.url + 'informativo/getInformativoAlerta', data).done( function( res ) {
            if( res.data.sqInformativo ) {
                openInformativoAlerta(res.data);
            } else {
                if( timeoutAlertasInformativos ) { clearTimeout( timeoutAlertasInformativos ); }
                //5 minutos
                if( res.data.interromper == 'N') {
                    timeoutAlertasInformativos = window.setTimeout( app.verificarInformativoAlerta, 60000 * 5 );
                }
            }
        });
    };

    /**
     * fechar a janela de alerta com o informativo e marcar como lido
     * @private
     */
    var _closeModalAlertaInformativo = function(){
        var $modal = $("#modalAlertaInformativos");
        if( $modal.is(":visible") ) {
            var data = $modal.data();
            $modal.modal('hide');
            if( data.sqInformativo ) {
                // gravar leitura do informativo pelo usuario
                $.post(  app.url + 'informativo/setRead', {sqInformativo:data.sqInformativo} )
                    .done( function( res ) {
                        app.verificarInformativoAlerta();
                    });
            } else {
                app.verificarInformativoAlerta();
            }
        }
    }

    /**
     * Exibir janela modal com as tarefas asynconas utilizando nova tabela user_jobs
     * em execução com o link para permitir o cancelamento
     * @private
     */
    var _openModalUserJobs = function() {
        modal.visualizarUserJobs();
    }

    /**
     * Exibir janela modal com as tarefas asynconas em execução com o link para
     * permitir o cancelamento
     * @private
     */
    var _openModalWorkers = function() {
        /*
        // TODO - IMPLEMENTAR O CONTROLE DE TAREFAS UTILIZANDO TABELA NO BANCO DE DADOS
        if( confirm('Cancelar atividade(s)?') ){
            app.ajax(app.url + 'main/deleteAllUserWorkers',{}, function (res) {
                console.log(res );
            });
        }*/
    }
    /**
     * função para separar os autores do nome cientifico e exibir os autores utilizando hint
     * @param noCientifico
     * @returns {string}
     * @private
     */
     var _ellipseAutores = function( noCientifico ) {
        if ( ! noCientifico ) {
            return ''
        }
        try {
            // parte 1 = no cientifico
            // parte 2 = autores
            var noCientificoParts = noCientifico.split('</i>');
            if (noCientificoParts[1]) {
                var qtdAutores = noCientificoParts[1].replace(/, ?[0-9]{4}/, '').replace('(', '').replace(')', '').split(',').length;
                if (qtdAutores > 1) {
                    var noAutores = $('<span>'+noCientificoParts[1]+'</span>').text(); // remover tags html
                    noCientifico = noCientificoParts[0] + '</i>&nbsp;<i class="fa fa-commenting-o autores-ellipses" title="' + String(noAutores).trim() + '"></i>'
                }
            }
        } catch( e ) {}
        return noCientifico;
    }

    /**
     * Criar gride com recurso de paginaçao, ordenaçao por coluna e cabeçalho fixo a
     * partir de uma table existente.
     * Regras:
     * 1) a table tem que estar dentro de um container com o id formado por "container"+gridId
     * 2) a table tem que ter um id unico formado por "table"+gridId
     * 3) (opcional) a table pode ter na tag thead uma tr para fazer a paginação com id formado por "tr"+gridId+"Pagination"
     *    que por sua vez deverá conter uma div com id formado por "div"+gridId+"PaginationContent"
     * 4) (opcional) a table pode ter na tag thead uma tr para filtragem por coluna com id formado por "tr"+gridId+"Filters"
     *   com o atributo "data-grid-id" com o id do gride
     * 5) a tr que contem os títulos da colunas deve ter id formador por "tr"+gridId+"Columns" e deve conter o atributo "data-grid-id" com o id do gride
     *   as colunas que forem passiveis de ordenação devem ter a classe "sorting", o atributo "data-field-name" e o evento click: "app.gridHeaderClick(this)"
     * 6) o conteudo retornado pela requisição ajas do gride deve conter a tag "tbody" e se houver paginação a tag "nav"
     *
     * @example
     * <div id="containerGridFichas" style="border-bottom:1px solid gray;width:100%;max-width:1500px"
     *       data-params="sqCicloAvaliacao"
     *       data-container-filters-id="divFiltrosFicha"
     *       data-height="790px"
     *       data-input-selected-ids="gridFichasSelectedIds"
     *       data-field-id="sqFicha"
     *       data-on-load="ficha.gridFichaOnLoad"
     *       data-url="ficha/getGridFicha">
     *       <input type="hidden" id="gridFichasSelectedIds" value="">
     *       <g:render template="divGridFichas"
     *       model="[gridId: 'GridFichas', listSituacaoFicha: listSituacaoFicha,grideFiltroUnidade:grideFiltroUnidade]"></g:render>
     *  </div>
     * @param gridId
     * @private
     */
    var _grid = function( gridId, filters, callback, loadingMessage ) {
        filters         = filters || {};
        gridId          = gridId || '';
        var sort        = {};
        var fieldId     = '';
        var data        = {};
        var format      = filters.format || 'table';

        // ler o container do gride
        try {

            if( !gridId){
                return;
            }
            gridId = gridId.ucFirst(); // o id do gride deve conter a primeira letra em maiúscula
            data.gridId = gridId;

            var $container = $("#container" + gridId );
            if( $container.size() == 0 ) {
                return;
            }
            // se for a primeira vez, ajustar a altura do container caso ele não tenha o height definido
            var containerData = $container.data();
            containerData.showFooterPagination = (typeof containerData.showFooterPagination == 'undefined' ? true : containerData.showFooterPagination);
            var maxWidth = containerData.width ? containerData.width : $container.width()-15; // -15 para dar espaço a direita para a barra de rolagem

            if( ! containerData.grid ) {
                $container.data('grid',true); // flag para informa que o gride já foi processado uma vez

                // caso tenha mudado de aba e o gride ainda não foi renderizado
                if( !$container.is(":visible") && $container.width() < 200 ) {
                    maxWidth = $container.closest('.panel').width();
                    if( maxWidth < 100 ){
                        maxWidth = $(window).width() - 27;
                    }
                }

            // gravar e definir o maxWidth do container
            if( maxWidth) {
                $container.data('width', parseInt(maxWidth))
                $container.css({"width": maxWidth + "px"});
            }

                if( ! containerData.height ){
                    // ler a altura total da window do browser
                    var maxHeight = Math.max(500,window.innerHeight-130);
                    // ler o topo do container na tela
                    var containerTop = $container.offset().top + 25;
                    // descontar o cabecalho e o menu da aplicacao
                    var heightFinal = Math.max(400,maxHeight - containerTop);
                    //var heightFinal = maxHeight - containerTop);
                    // para aplicar o plugin floatHead a div do container que envolve a table tem que ter o width defido
                    //var maxWidth = containerData.width ? containerData.width : $container.width()-15; // -15 para dar espaço a direita para a barra de rolagem
                    // definir a altura do gride
                    $container.data('height',heightFinal);
                    $container.css( {"width":maxWidth + "px", "min-height":heightFinal+"px","height":"auto","max-height":heightFinal + "px", "overflow":"auto"} );
                } else {
                    if( containerData.height == 'auto'){
                        $container.css({"width":maxWidth + "px", "height": containerData.height, "overflow-y": "hidden"});
                    } else {
                        // se terminar em numero adicionar px ao height
                        containerData.height += (/[0-9]$/.test(containerData.height) ? 'px' : '');
                        $container.css({"width":maxWidth + "px", "height": containerData.height, "max-height": containerData.height, "overflow": "auto"});
                    }
                }
                if( containerData.maxHeight ) {
                    containerData.maxHeight += (/[0-9]$/.test(containerData.maxHeight) ? 'px' : '');
                    $container.css({"max-height": containerData.maxHeight, "overflow": "auto"});
                }
                if( containerData.minHeight ) {
                    containerData.minHeight += (/[0-9]$/.test(containerData.maxHeight) ? 'px' : '');
                    $container.css({"min-height": containerData.minHeight, "overflow-y": "auto"});
                }

                // adicionar eventos limpar filtros e aplicar filtro
                $("#tr"+gridId+'Filters i.fa-eraser').off('click').on('click',function(){
                    app.gridReset(gridId,{},callback);
                });
                $("#tr"+gridId+'Filters i.fa-refresh').off('click').on('click',function(){
                    app.gridReset(gridId,{},callback);
                });
                $("#tr"+gridId+'Filters i.fa-filter').off('click').on('click',function(){
                    app.grid(gridId,{},callback);
                });

                $("#tr"+gridId+"Filters *.table-filter").each( function( i, ele) {
                    var $ele    = $( ele );
                    var tagName = ele.tagName.toUpperCase();
                    if( ! $ele.data('enter') && !$ele.data('change') ) {
                        if (tagName == 'SELECT') {
                            $ele.off('change').on('change', function () {
                                app.grid( gridId,{},callback );
                            });
                        } else if (tagName == 'INPUT') {
                            $(ele).off('keyup').on('keyup', function ( evt ) {
                                if( evt.keyCode == 13 || evt.which == 13) {
                                    app.grid(gridId,{},callback);
                                }
                            });
                        }
                    }
                })

                // fixar as larguras das colunas que tiverem width defindo
                $("#tr"+gridId+"Columns th").map( function(i, th ) {
                    try {
                        var width = String(th.style.width);
                        var $th = $(th);
                        if ( /^[1-9]/.test( width ) ) {
                            $th.css('min-width', width);
                        }
                        // adicionar e evento click para ordenar pela coluna
                        if( $th.hasClass('sorting') && $th.attr('onclick') == undefined ){
                            $th.unbind('click').bind('click',function(evt) {
                                app.gridHeaderClick(this,callback);
                            });
                        }
                    } catch( e ) {}
                });

                if( containerData.showFooterPagination ) {
                    // criar o container para exibição da paginação embaixo do gride
                    $container.after('<div id="div' + gridId + 'PaginationContentFooter" style="margin-top:10px;height:auto;overflow:hidden;text-align: left"></div>');
                }
            }

            // ler os metadados do gride
            var gridData = $container.data();

            // ler os parametros informados via data-params
            if( gridData.params ) {
                gridData.params.split(',').map(function(p){
                    if( /:/.test(p) ){
                        var aTemp = p.split(':');
                        filters[aTemp[0]]=aTemp[1];
                    } else {
                        var key = p;
                        if( /\|/.test(p) ) {
                            var aTemp = p.split('|');
                            p = aTemp[0];
                            key=aTemp[1];
                        }
                        var pField  = $('#'+p);
                        if( pField.size() > 0 ) {
                            filters[key] = $( pField[0]).val();
                        }
                    }
                });
            }
            // ler filtros das colunas da tabela
            var $containerFilters = $("#tr" + gridId + 'Filters');
            if( $containerFilters.size() == 1 ) {
                $containerFilters.find('input,select').each(function (i, it) {
                    var $field = $(it);
                    var fieldData = $field.data();
                    if ($field.val() && fieldData.field) {
                        if( $field.val() instanceof Array ) {
                            if( $field.val()[0] != '') {
                                filters[fieldData.field] = $field.val().join('|');
                            }
                        } else {
                            filters[fieldData.field] = $field.val().trim();
                        }
                        $field.addClass('filtered-green');
                    } else {
                        $field.removeClass('filtered-green');
                    }
                })
            }

            // ler outros filtros de fora do gride
            if( containerData.containerFiltersId ) {
                $.extend(filters, app.hasActiveFilter( containerData.containerFiltersId,containerData.contexto ) );
            }

            if( containerData.pageChange ) {
                // ler a paginacao atual
                $container.data('pageChange',false);
                var $containerPagination = $("#div" + gridId + 'PaginationContent');
                if ( $containerPagination.size() == 1 ) {
                    var $ulPagination = $containerPagination.find('ul.pagination-pages');
                    if ($ulPagination.size() == 1) {
                        data.paginationPageNumber = $ulPagination.data('currentPage') || 1;
                        data.paginationTotalRecords = $ulPagination.data('totalRecords') || 0;
                        data.paginationTotalPages = $ulPagination.data('totalPages') || 0;
                    }
                }
            }

            // ler order
            var $containerColumns = $("#tr" + gridId + 'Columns');
            if( ! $containerColumns.size() ) {
                alert('A tag TR das colunas não foi identificada no padrão: tr+GridName+Columns\nExemplo: #tr'+gridId+'Columns');
                return;
            }
            var sortFields  = [];
            $containerColumns.find('th.sorting').each(function(i, th ){
                var fieldData   = $(th).data();
                var sortOrder   = '';
                if( fieldData.fieldName ) {
                    if ($(th).hasClass('sorting_asc')) {
                        sortOrder ='asc';
                    } else if ( $(th).hasClass('sorting_desc')) {
                        sortOrder ='desc';
                    }
                    if( sortOrder != '') {
                        sortFields.push( fieldData.fieldName + ':' + sortOrder);
                    }
                }
            });
            sort = { "sortColumns": sortFields.join(',') }

            // ler container tbody do gride
            var $table = $("#table" + gridId);
            if( ! $table.size() ) {
                return;
            }

            // Formato CSV/PDF/RTF/PEN, ler os checks selecionados ou o checkAll
            // quando não for informado um id especifico
            //if (/csv|pdf|rtf|reg|pen|shp/.test(format.toLowerCase())) {
            if ( format.toLowerCase() !='table' ) {
                var $checkAll = $("#checkAll-" + gridId)
                // se existir o elemento com id checkAll+GridId e tiver com a classse glyphicon-checked a exportação será de todos os registros
                filters.exportAll = false;
                if ($checkAll.size() == 1) {
                    filters.exportAll = $checkAll.hasClass('glyphicon-check');
                }
                // se a exportação não for de todos os registros, selecionar no gride as linhas selecionadas
                // os checkboxes devem estar com name no padrão chk+colunaId. Ex: chkSqFicha
                if (!filters.exportAll) {
                    filters.exportIds = []
                    if( containerData.inputSelectedIds ) {
                        filters.exportIds = $("#"+containerData.inputSelectedIds).val().trim();
                    } else {
                        $container.find("input[type=checkbox][name=chk" + containerData.fieldId.ucFirst() + "]:checked").map(function (index, ele) {
                            if (ele.value) {
                                filters.exportIds.push(ele.value);
                            }
                        });
                        filters.exportIds = filters.exportIds.join(',');
                    }
                }
            }

            // criar objeto com os dados para post
            var url = gridData.url;
            $.extend( data, filters, sort );

            // se for passado o id da ficha, atualizar somente a linha correspondente
            var $trEdited   = null;
            var $tdEdited   = null;
            var $spanEdited = null;
            var rowNumEdited= '';
            fieldId = containerData.fieldId;
            if( fieldId && data[fieldId] ){
                $trEdited = $table.find('tbody tr[id=tr-' + data[fieldId] + ']');
                $tdEdited = $trEdited.find('td:first');
                $spanEdited = $tdEdited.find('span:first');
                try {
                    if( $spanEdited.size() == 1 ){
                        rowNumEdited = $spanEdited.html().trim();
                    } else {
                        rowNumEdited = $tdEdited.html().trim();
                    }
                } catch( e ){}
                $tdEdited.html(window.grails.spinner);
            } else {
                if( format == 'table' ) {
                    if( loadingMessage ) {
                        app.blockUI(loadingMessage);
                    }
                    else {
                        $container.scrollTop(0);
                        app.blockElement($container, 'Processando...');
                    }
                }
            }
            $.ajax( {
                url: baseUrl + url
                , type: 'POST'
                , data : data
                , dataType: (format == 'table' ? 'HTML' : 'JSON' )
                , timeout: 80000
                , success: function( res ) {
                    if( format == 'table') {
                        if (rowNumEdited != '') {
                            if( loadingMessage ) {
                                app.unblockUI();
                            } else {
                                app.unblockElement($container);
                            }
                        }
                        // adicionar tbody na tabela
                        var $res = $($('<div style="display:none;">' + res + '</div>'));
                        var $body = $res.find('tbody');
                        if ($body.size() == 0 && $res.text().trim() != '') {
                            //app.alertInfo('<p>Mensagem criação do gride:'+url+'</p><p>'+$res.text()+'</o>');
                            $('#div' + gridId + 'PaginationContent').html('');
                            var msg = ''
                            if( window.grails.env == 'desenv') {
                                msg = '<p>Mensagem criação do gride: <b>' + url + '</b></p><p><b>' + $res.text() + '</b></p>';
                            } else {
                                msg = '<p><b>' + $res.text() + '</b></p>';
                            }
                            $table.find('tbody').html('<td colspan="100"><span class="red">' + msg + '</span></td>');
                            app.eval(callback, res);
                            return;
                        }
                        var $paginationContent = $res.find('#div' + gridId + 'PaginationContent');

                        // sempre que houver o sq_ficha atualizar somente a linha correspondente
                        // as TRs devem estar com id no padrão: tr-99999
                        var rowUpdated = false;
                        if (data[fieldId]) {
                            // procurar por <span>xxx</span> na primeira coluna para manter a numeração correta do gride
                            if (rowNumEdited != '') {
                                var $trNew = $body.find('tr:first');
                                if ($trNew.size() == 1) {
                                    $trEdited.html($trNew.html());
                                    $tdEdited = $trEdited.find('td:first');
                                    if ($tdEdited.size() == 1) {
                                        $spanEdited = $tdEdited.find('span:first');
                                        // se não tiver o span com a numeracao da linha utilizar a td inteira
                                        if ($spanEdited.size() == 1) {
                                            $spanEdited.html(rowNumEdited);
                                        } else {
                                            $tdEdited.html(rowNumEdited);
                                        }
                                    }
                                }
                                rowUpdated = true;
                            }
                        }
                        if (!rowUpdated) {
                            // atualizar a paginação
                            $("#div" + gridId + "PaginationContent").html($paginationContent.html());
                            if( containerData.showFooterPagination ) {
                                // se não houver paginação não exibir somente o total de registros
                                try {
                                    $("#div" + gridId + "PaginationContentFooter").html('');
                                    if ($("#div" + gridId + "PaginationContent").find('ul[data-total-records]').data('total-pages') > 1) {
                                        $("#div" + gridId + "PaginationContentFooter").html($paginationContent.html());
                                    }
                                } catch( e ){}
                            }

                            $table.find('tbody').html($body.html());
                            try {
                                  if (!containerData.floatHeadApplied && $container.is(':visible') ) {
                                    $container.data('floatHeadApplied', true);
                                    // aplicar plugin float thead
                                    $table.floatThead({
                                        floatTableClass: 'floatHead',
                                        scrollContainer: true,
                                        autoReflow: true
                                        /*scrollContainer: function ($table) {
                                            return $table.closest('.table-wrapper');
                                        }*/
                                    });
                                }
                                $table.floatThead('reflow');

                            } catch (e) {
                                console.log('Typeof:', typeof ($table.floatThead));
                                console.log(e);
                            }
                        }
                        // mostrar/esconder o campo localizar do gride
                        $("#inputLocalizar" + gridId).addClass('ignoreChange');
                        if( $("#"+gridId+"Empty").size() > 0 ) {
                            $("#inputLocalizar" + gridId).addClass('hidden');
                        } else  {
                            $("#inputLocalizar" + gridId).removeClass('hidden');
                        }
                        $res.remove();
                    } else {

                        if( res.msg ) {
                            if( res.type == 'info' ) {
                                app.alert(res.msg);
                            } else {
                                  app.growl(res.msg,5,'','tc','large','info')
                            }
                        }
                        window.setTimeout(function () {
                            getWorkers();
                        }, 5000);
                        window.setTimeout(function () {
                            getWorkers();
                        }, 60000);
                        if (format == 'csv') {

                        }
                    }
                    app.eval( callback, res );
                    if( containerData.onLoad ) {
                        app.eval(containerData.onLoad, data, false );
                    }
                }
            }).fail(function (jqXHR, textStatus, errorThrown) {
                app.alertInfo('<span>Erro gride '+gridId+'<br><b>' + baseUrl+url + '</b><br>Mensagem: ' + errorThrown + '</span>' ,'Erro execução SALVE');
            }).always(function(){
                if( loadingMessage ){
                    app.unblockUI();
                } else {
                    app.unblockElement( $container );
                }
            });

        } catch( e ) {
            console.log( e );
        }
    }


    /**
     * limpar os filtros ativos no cabeçalho do gride e atualizar o gride
     * @param gridId
     * @private
     */
    var _gridReset = function( gridId,filters,callback ) {
        var $containerFilters = $("#tr" + gridId + 'Filters');
        if( $containerFilters.size() == 1 ) {
            $containerFilters.find('input,select').each(function (i, it) {
                var $field = $(it);
                var fieldData = $field.data();
                $field.removeClass('filtered-green');
                if ( $field.val() && fieldData.field) {
                    $field.val('');
                }
            })
        }
        app.grid( gridId,filters,callback );
    }

    /**
     * Atualizar a imagem indicativa de ordenaçao crescente/decrescente da coluna
     * e atualizar o grideç
     * @param th
     * @private
     */
    var _gridHeaderClick = function( th ) {
        var $th=$(th);
        var $tr=$th.parent();
        var nextOrder='';
        // calcular a próxima ordem
        if( $th.hasClass('sorting_asc') ) {
            nextOrder = 'sorting_desc';
        } else {
            nextOrder = 'sorting_asc';
        }
        // limpar a ordem atual de todas as colunas
        $tr.find('th.sorting').removeClass('sorting_asc sorting_desc');
        // aplicar o indicador na coluna clicada
        $th.addClass( nextOrder );
        app.grid( $tr.data('gridId'));
    }

    var _gridPageClick = function( ele, page ) {
        if( ! ele  || !page){
            return;
        }
        var $ele     = $( ele );
        var $ul     = $ele.closest('ul');
        var ulData  = $ul.data();
        var gridId = ulData.gridId;
        var $container = $("#container"+gridId);
        $container.data('pageChange',true);
        $ul.data('currentPage',page);
        // pode haver botoes de paginação em cima e em baixo do gride
        // e quando clicar no de baixo tem que setar a pagina no de cima (principal)
        var uls = $("ul[data-grid-id='"+gridId+"']");
        if( uls.length > 1 ) {
            $($("ul[data-grid-id='"+gridId+"']")[0]).data('currentPage', page);
        }
        app.grid( gridId );
    }


    /**
     * Exibir as opções do campo select multiple de grupos avaliados de acordo com a oficina selecionada
     * @param {'sqOficina':'',selectName:'',container:''},
     * @private
     */
    var _updateMultiSelectFiltroGruposAvaliados = function( params ){
        params = params || {}
        if( !params.container ){
            return;
        }
        if( !params.selectName ){
            params.selectName = 'sqGrupoFiltro';
        }
        // reexibir todas as opções quando a oficina não for especificada
        if( !params.sqOficina ) {
            $(params.container+" select[name="+params.selectName+"]").parent().find('ul.multiselect-container input:checkbox').map(function (index, input) {
                 $(input).closest('li').show();
            })
        }
        var data={};
        data.sqOficina = params.sqOficina;
        if( data.sqOficina ) {
            app.ajax(app.url + 'oficina/getGruposFichasOficina', data, function (res) {
                var gruposValidos = [];
                res.data.map( function( item ) {
                    gruposValidos.push( String( item.sq_grupo ) );
                })
                if( gruposValidos.length > 0 ) {
                    $(params.container+" select[name="+params.selectName+"]").parent().find('ul.multiselect-container input:checkbox').map(function (index, input) {
                        var value = String(input.value);
                        if (gruposValidos.indexOf(value) > -1) {
                            $(input).closest('li').show();
                        } else {
                            $(input).closest('li').hide();
                        }
                    })
                }
            });
        }
    }

    /**
     * fazer o tratamento do copy/paste nos inputs necessários
     * @param evt
     * @private
     */
    var _inputPaste = function(evt ){
        var $this = this;
        try {
            evt.stopPropagation();
            evt.preventDefault();
            var cd = evt.originalEvent.clipboardData;
            var text = cd.getData('text/plain');
            var lista = text.trim().replace(/\t|\n|,/gm, ';').split(';')
            $this.value = String( ( $this.value =='' ? '': $this.value+'; ' ) + lista.join('; ')).replace(/[;,]{2,}/g,';');
        } catch( error ) {
            navigator.clipboard.readText().then(function (text) {
                var lista = text.trim().replace(/\t|\n|,/gm, ';').split(';')
                $this.value = String(($this.value=='' ? '' : $this.value+'; ') + lista.join('; ')).replace(/[;,]{2,}/g,';');
            }, function (err) {
            });
        }

    }

    /**
     * método para alterar a versão da ficha json que ficará disponibilizada
     * para o módulo público
     * @param sqFichaJson
     * @private
     */
    var _publicarVersao = function( sqFichaJson, callback ) {
        app.ajax(app.url + 'ficha/publicarVersao', {sqFichaJson: sqFichaJson},
            function (res) {
                app.eval(callback, res);
            }, 'Processando...');
    }

     var container='';

    /*var _saveFilters = function(id)
    {
        console.log('Save filters de ' + id);
    };

    var _restoreFilters = function(id)
    {
        console.log('Restore filters de ' + id);
    };*/

        return {
            url: _url,
            run: _run,
            sleep: _sleep,
            loadModule: _loadModule,
            ajax: _ajax,
            dialog: _dialog,
            alertError: _alertError,
            alertInfo: _alertInfo,
            alert: _alert,
            confirm: _confirm,
            confirmWithEmail:_confirmWithEmail,
            blockUI: _blockUI,
            unblockUI: _unblockUI,
            blockElement: _blockElement,
            unblockElement: _unblockElement,
            growl: _growl,
            calcElapsedMinutes: _calcElapsedMinutes,
            lsSet: _lsSet,
            lsGet: _lsGet,
            lsDel: _lsDel,
            lsClear: _lsClear,
            unloadModule: _unloadModule,
            disableTab: _disableTab,
            showHideTab: _showHideTab,
            initMasks: _initMasks,
            eval: _eval,
            initSelect2: _initSelect2,
            params2data: _params2data,
            initMap: _initMap,
            setFormFields: _setFormFields,
            setSelect2: _setSelect2,
            focus: _focus,
            reset: _reset,
            selectGridRow: _selectGridRow,
            unselectGridRow: _unselectGridRow,
            initTooltips: _initTooltips,
            initSecurity: _initSecurity,
            isDate: _isDate,
            isTime: _isTime,
            gd2gms: _gd2gms,
            gms2gd: _gms2gd,
            selectTab: _selectTab,
            clearInputFile: _clearInputFile,
            openWindow: _openWindow,
            closeWindow: _closeWindow,
            getWindow: _getWindow,
            getGrid: _getGrid,
            logout: _logout,
            panel: _panel,
            download: _download,
            isTabModified: _isTabModified,
            fieldChange: _fieldChange,
            resetChanges: _resetChanges,
            updateArquivoSistema: _updateArquivoSistema,
            filtroNivelTaxonomicoChange: _filtroNivelTaxonomicoChange,
            insertTextEditor: _insertTextEditor,
            serializeFields: _serializeFields,
            clearFilters: _clearFilters,
            gerarZipFichas: _gerarZipFichas,
            hasActiveFilter: _hasActiveFilter,
            updatePercentualImportacao: _updatePercentualImportacao,
            confirmarImportacaoPlanilha: _confirmarImportacaoPlanilha,
            updateGridFichas: _updateGridFichas,
            btnFiltroFichaClick: _btnFiltroFichaClick,
            updateDashboard: _updateDashboard,
            updatingDashboard: _updatingDashboard,
            tabClick: _tabClick,
            unselectTab: _unselectTab,
            clearCache: _clearCache,
            exportarOcorrenciaPortalbio: _exportarOcorrenciaPortalbio,
            exportarGrideOcorrenciaCsv: _exportarGrideOcorrenciaCsv,
            dialogoExportarOcorrencias: _dialogoExportarOcorrencias,
            setDefaultText: _setDefaultText,
            showLongText: _showLongText,
            showText: _showText,
            editorAfterPaste: _editorAfterPaste,
            relatorio: _relatorio,
            initMultiSelect: _initMultiSelect,
            categoriaIucnChange: _categoriaIucnChange,
            updateGridMotivoMudancaCategoria: _updateGridMotivoMudancaCategoria,
            repetirUltimaAvaliacao: _repetirUltimaAvaliacao,
            showHideContainer: _showHideContainer,
            saveField:_saveField,
            restoreField:_restoreField,
            tablePagination:_tablePagination,
            verificarInformativoAlerta:_verificarInformativoAlerta,
            closeModalAlertaInformativo:_closeModalAlertaInformativo,
            openModalWorkers : _openModalWorkers,
            openModalUserJobs : _openModalUserJobs,
            ellipseAutores:_ellipseAutores,
            grid:_grid,
            gridReset:_gridReset,
            gridHeaderClick:_gridHeaderClick,
            gridPageClick : _gridPageClick,
            updateMultiSelectFiltroGruposAvaliados:_updateMultiSelectFiltroGruposAvaliados,
            inputPaste:_inputPaste,
            filtroNuAbaChange:_filtroNuAbaChange,

            /*saveFilters:_saveFilters,
            restoreFilters:_restoreFilters*/
            publicarVersao:_publicarVersao
        }
})();

// janelas modais
var modal = (function() {

    var _width = function () {
        return ($(window).width() - 50);
    };

    var _height = function () {
        return Math.max(500,window.innerHeight-200);
    };

    var _cadRefBib = function (params, ele, evt) {
        params = params || {};
        var data = params;
        // editar a comunicação pessoal apenas
        if( ! data.sqPublicacao && data.noAutor && data.sqFichaRefBib ){
            $("#noAutorRef").val( data.noAutor);
            $("#nuAnoRef").val( data.nuAno);
            $("#btnSaveFrmSelRefBibFicha").data('sqFichaRefBib', data.sqFichaRefBib);
            app.selectTab('liTabSelComPessoal');
            app.focus('noAutorRef');
            return;
        }
        data.reload = false;
        data.modalName = 'cadastrarRefBib';
        //var windowHeight = ($(window).height() - 20 )
        var windowHeight = Math.max(500,window.innerHeight-130);
        app.openWindow({
            id: 'modalCadRefBib',
            url: app.url + 'main/getModal',
            width: 1100,
            data: data,
            height: Math.min( 650, windowHeight),
            title: 'Gerenciar - Referência Bibliográfica',
            autoOpen: true,
            modal: true
        }, function (data, e) {
            var data = data.data;
            var btn = $("#btnSaveCadRefBib");
            //var windowHeight = ($(window).height() - 120 );
            var bodyH = Math.min(650,windowHeight);
            // redimensionar a altura da janela de acordo com a tela do usuário
            $('#modalCadRefBib div.modal-body').css('height',bodyH + 'px' );
            $("#modalCadRefBib").css('height',(bodyH+77.5)+'px');
            //var btn2 = $("#btnSaveCadRefBibFicha");
            //btn.attr('title','Gravar a referência bibliográfica.');
            btn.html('Gravar')
            btn.removeClass('btn-warning');
            btn.data('sqFicha', '');
            btn.data('sqRegistro', '');
            btn.data('noColuna', '');
            btn.data('noTabela', '');
            btn.data('deRotulo', '');
            btn.data('sqFichaRefBib', '');
            if( data.sqFichaRefBib ) {
                btn.data('sqFichaRefBib', data.sqFichaRefBib);
            }
            //btn.hide();
            if (data.sqPublicacao) {
                window.setTimeout(function () {
                    refBib.edit({"id": data.sqPublicacao});
                }, 500);
            } else {
                if (data.sqFicha) {
                    //btn.attr('title','gravar a referência bibliográfica e já adicioná-la à ficha atual!');
                    btn.html('Gravar e adicionar à ficha');
                    btn.addClass('btn-warning');
                    btn.data('sqFicha', data.sqFicha);
                    btn.data('sqRegistro', data.sqRegistro);
                    btn.data('noColuna', data.noColuna);
                    btn.data('noTabela', data.noTabela);
                    btn.data('deRotulo', data.deRotulo);
                }
            }
        }, function (data, e) {
        });
    };

    var _cadInstituicao = function () {
        //alert('Ainda não implementado');
        var data = {};
        data.reload = false;
        data.modalName = 'cadastrarInstituicao';
        app.openWindow({
            id: 'modalCadInstituicao',
            url: app.url + 'main/getModal',
            width: 750,
            data: data,
            height: this.intHeight,
            title: 'Cadastro de Instituição',
            autoOpen: true,
            modal: true
        }, function (data, e) {
        }, function (data, e) {
        });
    };

    var _uploadArquivoSistema = function (e) {
        var data = $(e).data();
        data.reload = true;
        data.modalName = 'uploadArquivoSistema';
        app.openWindow({
            id: 'uploadArquivoSistema',
            url: app.url + 'main/getModal',
            data: data,
            width: 500,
            height: 400,
            title: data.title,
            autoOpen: true,
            modal: true
        }, function (data, e) {
        }, function (data, e) {
        });
    };

    var _exportOccurrencesDwca = function (params) {
        var data = {};
        params = params || {}
        if (params.id) {
            data.id = params.id
        }
        data.reload = false;
        data.modalName = 'exportarOcorrenciaDwca';
        app.openWindow({
            id: 'exportarOcorrenciaDwca',
            url: app.url + 'main/getModal',
            width: 400,
            height: 300,
            data: data,
            title: 'Exportação Ocorrência PortalBio',
            autoOpen: true,
            modal: true
        }, function (data, e) {
            // on onOpen
        }, function (data, e) {
            //onClose
        });
    };

    var _alterarTaxonFicha = function (sqFicha) {
        //alert('Ainda não implementado');
        var data = {};
        data.reload = true;
        data.sqFicha = sqFicha;
        data.modalName = 'alterarTaxonFicha';
        app.openWindow({
            id: 'modalAlterarTaxonFicha',
            url: app.url + 'main/getModal',
            width: 750,
            data: data,
            height: 265,
            title: 'Alteração do táxon da ficha',
            autoOpen: true,
            modal: true
        }, function (data, e) {
            frmAlterarTaxonFicha.init(data);
        }, function (data, e) {/*onClose*/
        });
    };

    var _alterarArvoreTaxonomica = function (sqFicha) {
        //alert('Ainda não implementado');
        var data = {};
        data.reload = true;
        data.sqFicha = sqFicha;
        data.modalName = 'alterarArvoreTaxonomica';
        app.openWindow({
            id: 'modalAlterarArvoreTaxonomica',
            url: app.url + 'main/getModal',
            width: 700,
            data: data,
            height: 500,
            title: 'Alteração da Árvore Taxonômica (Sintax)',
            autoOpen: true,
            modal: true
        }, function (data, e) {
            frmAlterarArvoreTaxonomica.init(data);
        }, function (data, e) {/*onClose*/
        });
    };

    var _selecionarFichaCicloAnterior = function ( params ) {
        var data = {};
        data.reload = true;
        data.sqFicha = params.sqFicha;
        data.sqCicloAvaliacao = params.sqCicloAvaliacao;
        data.modalName = 'selecionarFichaCicloAnterior';
        app.openWindow({
            id: 'modalselecionarFichaCicloAnterior',
            url: app.url + 'main/getModal',
            width: 750,
            data: data,
            height: 265,
            title: 'Selecionar a Ficha do Ciclo Anterior',
            autoOpen: true,
            modal: true
        }, function (data, e) {
            ficha.frmSelecionarFichaCicloAnteriorInit( data );
        }, function (data, e) {/*onClose*/
        });
    };


    var _alterarPublicacaoFicha = function (sqPublicacao) {
        var data = {};
        data.reload = true;
        data.sqPublicacao = sqPublicacao;
        data.modalName = 'alterarPublicacaoFicha';
        // app.closeWindow('modalCadRefBib');
        app.openWindow({
            id: 'modalAlterarPublicacaoFicha',
            url: app.url + 'main/getModal',
            width: 750,
            data: data,
            //height: 265,
            title: 'Fichas que utilizam a referência',
            autoOpen: true,
            modal: true
        }, function (data, e) {

        }, function (data, e) {/*onClose*/
        });
    };

    var _cadMunicipioFicha = function (params) {
        var data = {};
        data = app.params2data(params, data);
        data.reload = true;
        data.modalName = 'cadMunicipioFicha';
        app.openWindow({
                id: 'modalMunicipioFicha',
                url: app.url + 'main/getModal',
                width: 800,
                height: 610,
                data: data,
                title: 'Município da Área Relevante',
                autoOpen: true,
                modal: true
            }, function (data, e) {
                cadMunicipioFicha.updateGride();
            }
            , function (data, e) {/*onClose*/
                delete data.data.callback;
                app.eval(params.callback,data.data);

            });
    };
    /**
     * Janela genérica para solicitar algum texto ao usuário
     * @param params
     * @private
     */
    var _janelaTextoLivre = function (params) {
        params.id = params.id ? params.id : 'win' + makeId();
        params.onOpen = params.callback ? params.callback : null;
        params.onClose = params.onClose ? params.onClose : null;
        params.title = params.title ? params.title : '';
        params.btnLabel = params.btnLabel ? params.btnLabel : 'Gravar';
        params.label = params.label ? params.label : '';
        params.url = params.url ? params.url : '';
        params.data = params.data ? params.data : {};
        params.width = params.width ? params.width : 500;
        params.height = params.height ? params.height : 300;
        params.position = params.position ? params.position : 'center';
        params.theme = params.theme ? params.theme : "#a3c4a3";
        // configurações padão
        params.data.id = params.data.id ? params.data.id : params.id;
        params.data.maxLength = params.data.maxLength ? params.data.maxLength : 2000;
        params.data.btnLabel = params.btnLabel;
        params.data.label = params.data.label ? params.data.label : params.label;
        params.text = params.text ? params.text : ''
        if( !params.url ) {
            params.url              = '/main/getModal';
            params.data.modalName   = params.data.modalName ? params.data.modalName : 'textoLivre';
            params.data.label       = params.data.label ? params.data.label : '';
        }

        var panel = app.panel(params.id
            , params.title
            , params.url
            , params.data
            , function () {
                var idTextArea = 'textarea#fldTextoLivre' + params.id;
                setTimeout(function(){
                    var textArea = $( idTextArea );
                    textArea.focus();
                    textArea.css('height',params.height-110+'px');
                    textArea.css('max-height',params.height-110+'px');
                    if( params.text ){
                        textArea.val( params.text );
                    }
                    app.eval(params.onOpen, [params.id, textArea] );
                },1000);
            }
            , params.width
            , params.height
            , true
            , false
            , params.onClose
            , params.position
            , params.theme
            , params.resizeble
        );
    };

    var _visualizarInformativosPublicados = function() {
        var data = {};
        //var minHeight = Math.max( $(window).height()-300, 500 );
        var minHeight = Math.max(500,window.innerHeight-300);
        var maxWidth = Math.min( $(window).width(), 600 );
        data.reload = true;
        data.modalName = 'modalInformativosPublicados';
        app.openWindow({
            id: 'modalInformativosPublicados',
            url: app.url + 'main/getModal',
            width: maxWidth,//($(window).width()-50),
            data: data,
            height:500,//minHeight,
            title: '<i class="blue fa fa-comments"></i>&nbsp;Informativos - SALVE',
            autoOpen: true,
            modal: true
        }, function (data, e) {
            // onshow
        }, function (data, e) {
            //onClose
            // marcar o informativo como lido
            /*
            if( data.data.numero ) {
                var data = { nuInformativo:data.data.numero, marcarComoLido:true ? 'S' : 'N' }
                app.ajax(app.url + 'informativo/show', data,function( res ) {
                    window.setTimeout(app.verificarInformativoAlerta,5000);
                });
            } else {
                app.verificarInformativoAlerta();
            }*/
        });
    }

    var _janelaCadastrarInformativo = function(params) {
        var data = {};
        //var minHeight = Math.max( $(window).height()-300, 550 );
        var minHeight = Math.max(550,window.innerHeight-300);
        data.reload = true;
        data.modalName = 'cadastrarInformativo';
        app.openWindow({
            id: 'modalCadastrarInformativo',
            url: app.url + 'main/getModal',
            width: ($(window).width()-150),
            data: data,
            height:minHeight,
            title: 'Manutenção dos Informativos',
            autoOpen: true,
            modal: true
        }, function (data, e) {
            // callback
        }, function (data, e) {
            //onClose
        });
    }

    var _janelaCadastrarAlerta = function(params) {
        var data = {};
        //var minHeight = Math.max( $(window).height()-300, 550 );
        var minHeight = Math.max(550,window.innerHeight-300);
        data.reload = true;
        data.modalName = 'cadastrarAlerta';
        app.openWindow({
            id: 'modalCadastrarAlerta',
            url: app.url + 'main/getModal',
            width: ($(window).width()-150),
            data: data,
            height:minHeight,
            title: 'Manutenção de Alertas',
            autoOpen: true,
            modal: true
        }, function (data, e) {
            // callback
        }, function (data, e) {
            //onClose
        });
    }
    var _alterarSituacaoFichaParaExcluida = function (sqFicha) {
        //alert('Ainda não implementado');
        var data = {};
        data.reload = true;
        data.sqFicha = sqFicha;
        data.modalName = 'alterarSituacaoFichaParaExcluida';
        app.openWindow({
            id: 'modalAlterarSituacaoFichaParaExcluida',
            url: app.url + 'main/getModal',
            width: 840,
            data: data,
            height: 480,
            title: 'Excluir a ficha da Avaliação',
            autoOpen: true,
            modal: true
        }, function (data, e) {
            frmAlterarSituacaoFichaParaExcluida.init(data.data);
        }, function (data, e) {/*onClose*/
            tinyMCE.remove('#dsJustificativaExclusao');
        });
    };

    var _showPendenciaFicha = function (sqFicha) {
        var data = {};
        data.reload = true;
        data.sqFicha = sqFicha;
        data.modalName = 'visualizarPendenciaFicha';
        app.openWindow({
            id: 'modalVisualizarPendenciaFicha',
            url: app.url + 'main/getModal',
            width: 840,
            data: data,
            height: 480,
            title: 'Pendência(s)',
            autoOpen: true,
            modal: true
        }, function (data, e) {
            visualizarPendenciaFicha.init(data.data);
        }, function (data, e) {/*onClose*/
        });
    };
    var _showPendenciaRevisaoFicha = function (sqFicha) {
        var data = {};
        data.reload = true;
        data.sqFicha = sqFicha;
        data.modalName = 'visualizarPendenciaRevisaoFicha';
        app.openWindow({
            id: 'modalVisualizarPendenciaRevisaoFicha',
            url: app.url + 'main/getModal',
            width: $(window).width()-100,
            data: data,
            height: 480,
            title: 'Pendência Revisão',
            autoOpen: true,
            modal: true
        }, function (panel, e) {
        }, function (panel, e) {/*onClose*/
            var sqFicha = panel.data.sqFicha;
            publicacao.updateGridNaoPublicadas({sqFicha:sqFicha});
        });
    };

    var _janelaComunicarTodosChat = function (data, onSave ) {
        var data = data || {};
        data.reload = true;
        data.modalName = 'comunicarTodosChatValidacao';
        var maxHeight = modal.height();
        var maxWidth  = Math.min(modal.width(),800);
        app.openWindow({
            id: 'modalComunicarTodosChatValidacao',
            url: app.url + 'main/getModal',
            width: maxWidth,
            height: maxHeight,
            data: data,
            title: 'Enviar e-mail para todos',
            autoOpen: true,
            modal: true
        }, function (data, e) {
            setTimeout(function(){
                data                = data.data;
                var form            = $("#frmComunicarTodosChatValidacao");
                var divCTs          = form.find('div#divCoordenadoresTaxon');
                var divCTsH         = divCTs.is(':visible') ? divCTs.height() : 0 ;
                var divVLs          = form.find('div#divValidadores');
                var divVLsH         = divVLs.is(':visible') ? divVLs.height() : 0 ;
                var textAreaHeidht  = ( maxHeight - ( 230 + divCTsH + divVLsH) );
                var $textarea       = form.find('textarea');
                var $btnSave        = form.find('button[name=btnSaveEmailMessage]');
                $textarea.height( textAreaHeidht );
                //var mensagemPadrao = 'Prezado(a) {nome},\nhouve alteração no bate-papo da validação do táxon {taxon}.\nEstamos solicitando sua contribuição.'
                var txEmail = app.lsGet('mensagemEmailChatChamarAtencaoTodos');
                if( txEmail ){
                    $textarea.val( txEmail );
                } else {
                    txEmail = 'Prezado(a) {nome},\nhouve alteração no bate-papo da validação do táxon {taxon}.\nEstamos solicitando sua contribuição.'
                    $textarea.val( txEmail );
                }

                $btnSave.off('click').on('click', function(event) {
                    data.listCts = [];
                    data.listVls = [];

                    // coordenadores de taxon selecionados
                    form.find('input.chkCT:checked').map(function (i, item) {
                        data.listCts.push(item.value);
                    });
                    if (form.find('input.chkCT').size() > 0 && data.listCts.length == 0) {
                        app.alertInfo('Selecione um dos coordenadores de taxon.');
                        return;
                    }

                    // validadores selecionados
                    form.find('input.chkVL:checked').map(function (i, item) {
                        data.listVls.push(item.value);
                    });
                    if (form.find('input.chkVL').size() > 0 && data.listVls.length == 0 && data.listCts.length == 0) {
                        app.alertInfo('Selecione um dos validadores.');
                        return;
                    }

                    /*if( divCTs.size() == 1 && divCTs.is(':visible') && data.listCts.length == 0 ) {
                        app.alertInfo('Necessário selecionar pelo menos um coordenador de táxon.', '', function () {
                        });
                        return;
                    }

                    if( divVLs.size() == 1 && divVLs.is(':visible') && data.listVls.length == 0 ) {
                        app.alertInfo('Necessário selecionar pelo menos um validador.', '', function () {
                        });
                        return;
                    }*/

                    if ( form.find('input.chkVL').size() > 0 && data.listVls.length == 0) {
                        data.listVls.push(-1);// item fake para que o filtro seja utilizado
                    }



                    data.txEmail = form.find('textarea').val();
                    if (!data.txEmail) {
                        app.alertInfo('Mensagem do e-mail deve ser informada.', '', function () {
                            app.focus("fldMsgEmail");
                        });
                        return;
                    }
                    app.confirm('Confirma o envio dos e-mails?', function () {
                        app.lsSet('mensagemEmailChatChamarAtencaoTodos', data.txEmail);
                        $("#btnConvidarTodos").prop('disabled', true);
                        $("#btnConvidarTodos i").removeClass('glyphicon-envelope').addClass('glyphicon-refresh spinning');
                        data.listCts = data.listCts.join(',');
                        data.listVls = data.listVls.join(',');
                        if ( onSave ) {
                            onSave( data );
                        }
                    })
                    return;
                })
            },1000);
        }, function (data, e) {/*onClose*/

        });
    };

    // Inegração SALVE / SIS-IUCN
    /**
     * exibir a janela para tradução/manutenção dos texto fixos
     * @param params
     * @param onOpen
     * @param onClose
     * @private
     */
    var _janelaTraducaoTextoFixo = function(params, cbOnOpen, cbOnClose) {
        var data = app.params2data( params,{} );
        //var minHeight = Math.max( $(window).height()-300, 650 );
        var minHeight = Math.max(650,window.innerHeight-300);
        var maxWidth = Math.min( ($(window).width()-150 ),1200);
        // inicilizar os parâmetros opcionais quando não forem passados
        data.noTabelaTraducao = data.noTabelaTraducao || '';
        data.idRegistroTraducao = data.idRegistroTraducao || '';
        data.reload = true;
        data.modalName = 'traducaoTextoFixo';

        if( data.noTabelaTraducao ){
            minHeight=330
        }

        app.panel('modalTraducaoTextoFixo'
            ,'Glossário de Tradução'
            , 'main/getModal'
            ,data
        ,function(){
                // onOpen
                $("#frmModalTraducaoTextoFixo #inSaved").val('N');
                cbOnOpen ? cbOnOpen( data ) : null;
            }
            ,maxWidth
            , minHeight
            ,false
            ,false,
            function(panel){
                // on Close
                data.inSaved        = $("#frmModalTraducaoTextoFixo #inSaved").val();
                data.txOriginal     = $("#frmModalTraducaoTextoFixo #txOriginal").val();
                data.txTraduzido    = $("#frmModalTraducaoTextoFixo #txTraduzido").val();
                data.txRevisado    = $("#frmModalTraducaoTextoFixo #txRevisado").val();
                cbOnClose ? cbOnClose( data ) : null
                return true
            }
            ,'center'
            ,'#a2ffa9'
            ,false,false
        );


    };

    // Inegração SALVE / SIS-IUCN
    /**
     * exibir a ficha no formato html para fazer a tradução ou a revisão da tradução
     * @param params
     * @param onOpen
     * @param onClose
     * @private
     */
    var _janelaTraducaoRevisaoFicha = function(params, onOpen, onClose) {

        var data = app.params2data( params,{} );
        //var minHeight = $(window).height()-120;
        var minHeight = Math.max(500,window.innerHeight-120);
        var maxWidth = $(window).width()-50;
        var titulo = '<img src="/salve-estadual/assets/en.png" class="traducao-flag"/> Tradução/Revisão Ficha  ' + data.noCientifico;
        var url = 'traducaoFicha/fichaRevisao';
        data.reload = true;
        data.modalName = 'traducaoRevisaoFicha';

        var win = app.panel('pnlTraducaoRevisaoFicha_' + params.sqFicha
            , titulo
            , url
            , data
            ,function() {
                // on open
                onOpen ? onOpen( data ) : null;
                $('body').css('overflow','hidden');
            }
            ,null // minHeight
            ,null // maxWidth
            ,true
            ,true
            ,function( res ){
               // on close
               onClose ? onClose( data ) : null;
               $('body').css('overflow','auto');
            // on close
            },''
            ,'#d4e1c9' //,'#a4a7ce'
            );


            /*
            app.openWindow({
                id: 'modalTraducaoRevisaoFicha',
                url: app.url + 'main/getModal',
                data: data,
                width: maxWidth,
                height: minHeight,
                title: 'Tradução/Revisao Ficha - <b>' + data.noCientifico+'</b>',
                autoOpen: true,
                modal: true
            }, function (data, e) {
                // callback
                onOpen ? onOpen( data ) : null
            }, function (data, e) {
                //onClose
                onClose ? onClose( data.data ) : null
            });*/
    };


    /**
     * exibir o gride com o histórico das justificativas ao alterar a ficha para
     * a situação excluída
     */
    var _historicoSituacaoFicha = function(sqFicha) {
        if( ! sqFicha ) {
            return;
        }
        var data = {sqFicha:sqFicha};
        //var minHeight = Math.max( $(window).height()-300, 500 );
        var minHeight = Math.max(500,window.innerHeight-300);
        var maxWidth = Math.min( $(window).width(), 800 );
        data.reload = true;
        data.modalName = 'modalHistoricoSituacaoExcluida';
        app.openWindow({
            id: 'modalHistoricoSituacaoExcluida',
            url: app.url + 'main/getModal',
            width: maxWidth,
            data: data,
            height:500,
            title: '<i class="blue fa fa-table"></i>&nbsp;Histórico Sistuação Excluída',
            autoOpen: true,
            modal: true
        }, function (data, e) {
            // onshow
        }, function (data, e) {
            //onClose
        });
    }

    var _visualizarUserJobs = function() {
        var data = {};
        //var minHeight = $(window).height()-250;
        var minHeight = Math.max(500,window.innerHeight-250);
        var maxWidth = $(window).width() - 50;
        data.reload = true;
        data.modalName = 'modalUserJobs';
        app.openWindow({
            id:  data.modalName,
            url: app.url + 'main/getModal',
            width: maxWidth,
            height:minHeight,
            data: data,
            title: '<i class="blue fa fa-cogs"></i>&nbsp;Minhas Tarefas',
            autoOpen: true,
            modal: true
        }, function (data, e) {
            // onshow
        }, function (data, e) {
            //onClose
        });
    }


    /**
     * exibir a janela para edição do shapefile da bacia hidrográfica
     * @param params
     * @param onOpen
     * @param onClose
     * @private
     */
    var _janelaApoioShapeBacia = function(params, cbOnOpen, cbOnClose) {
        var data = app.params2data( params,{} );
        var minHeight = Math.max(650,window.innerHeight-300);
        var maxWidth = Math.min( ($(window).width()-150 ),1200);
        // inicilizar os parâmetros opcionais quando não forem passados
        data.noTabela   = data.noTabela || 'apoio';
        data.id         = data.id || 0;
        data.text       = data.text || '';
        data.codigo     = data.codigo || '';
        data.reload = true;
        data.modalName = 'apoioShapeBacia';

        app.panel('modalApoioShapeBacia'
            ,'Edição do shapefile da bacia'
            , 'main/getModal'
            ,data
            ,function(){
                // onOpen
                cbOnOpen ? cbOnOpen( data ) : null;
            }
            , maxWidth
            , minHeight
            ,false
            ,false,
            function(panel){
                // on Close
                cbOnClose ? cbOnClose( data ) : null
                return true
            }
            ,'center'
            ,'#a2ffa9'
            ,false
            ,false
        );
    };




    return {
        width:_width,
        height:_height,
        cadRefBib: _cadRefBib,
        uploadArquivoSistema: _uploadArquivoSistema,
        cadInstituicao: _cadInstituicao,
        exportOccurrencesDwca:_exportOccurrencesDwca,
        alterarTaxonFicha : _alterarTaxonFicha,
        alterarArvoreTaxonomica : _alterarArvoreTaxonomica,
        cadMunicipioFicha : _cadMunicipioFicha,
        alterarPublicacaoFicha : _alterarPublicacaoFicha,
        janelaTextoLivre: _janelaTextoLivre,
        visualizarInformativosPublicados : _visualizarInformativosPublicados,
        janelaCadastrarInformativo : _janelaCadastrarInformativo,
        janelaCadastrarAlerta : _janelaCadastrarAlerta,
        alterarSituacaoFichaParaExcluida : _alterarSituacaoFichaParaExcluida,
        showPendenciaFicha : _showPendenciaFicha,
        showPendenciaRevisaoFicha : _showPendenciaRevisaoFicha,
        selecionarFichaCicloAnterior:_selecionarFichaCicloAnterior,
        janelaComunicarTodosChat:_janelaComunicarTodosChat,
        janelaTraducaoTextoFixo:_janelaTraducaoTextoFixo,
        janelaTraducaoRevisaoFicha:_janelaTraducaoRevisaoFicha,
        historicoSituacaoFicha: _historicoSituacaoFicha,
        visualizarUserJobs:_visualizarUserJobs,
        janelaApoioShapeBacia:_janelaApoioShapeBacia,
    }
})();

var today = function(somarDias) {
        if (!somarDias) {
            somarDias = 0;
        };
        var aTemp = new Date(new Date().setDate(new Date().getDate() + somarDias)).toJSON().slice(0, 10).slice(0, 10).split('-');
        return aTemp[2] + '/' + aTemp[1] + '/' + aTemp[0];
    }
    /**
     * função para inicializar a janela de preview da imagem ao clicar
     * @param container
     */
var bindDialogPreview = function(container) {
    $('#' + container.replace(/^#/,'') + ' .img-preview-grid,.img-preview').each(function(k, v) {
        $(this).on('click', function() {
            var url = $(this).attr('src').replace(/\/thumb/, '');
            var data = $(this).data();
            var title = $(this).attr('alt');
            var id = $(this).data('id');
            if( !id )
            {
                id = makeId()
            };
            var ww = Math.min($(window).width() - 20, 1237);
            //var wh = Math.min( $(window).height() - 140, 682);
            var wh = Math.max(500,window.innerHeight-250);
            $.jsPanel({
                id: "preview_" + String(id),
                headerTitle: title,
                headerControls: {
                    controls: "closeonly"
                },
                contentSize: {
                    width: ww,
                    height: wh
                },
                theme: "bootstrap-success",
                draggable: false,
                closeOnEscape: true,
                callback: function(panel) {
                    this.find('.jsglyph-minimize').hide();
                    this.content.css("padding", "15px");
                    /*this.content.css({
                        "background-image": "url('" + url + "')",
                        //"background-size": "100% 100%",
                        "background-repeat": "no-repeat",
                        "background-position": "center",
                        "padding": "15px"
                    });
                    */
                    if( /\.zip$/.test(data.fileName) )
                    {
                        this.content.css("padding", "2px");
                        this.content.html('<div id="map'+data.id+'" style="width:auto;height:auto;max-width:100%;max-height:100%;background-color:#efefef;"></div>')
                        window.setTimeout(function(){
                            showShapeFile(data);
                        },1000);
                    }
                    else {
                        this.content.css("overflow","auto");
                        this.content.html('<p class="text-center"><img src="' + url + '" style="max-width:100%;width:auto;height: auto;"></p>')
                    }
                },
            });
        });
    });
};

// funções da tela modal de montagem do critério utilizadio na aba 10.6-Resultado e FichaCompleta/index
var openModalSelCriterioAvaliacao = function(params) {
    var data = {};
    if( ! data.contexto && params.container ) {
        data.contexto = params.container
    };
    var frm = data.contexto ? "#" + data.contexto.replace(/^#/,'') + ' ' : '';
    data.reload = true;
    data.modalName = 'selCriterioAvaliacao';
    data = app.params2data(params, data);
    data.codigoCategoria = '';
    if( data.fieldCategoria )
    {
        data.codigoCategoria = $(frm+' #'+data.fieldCategoria+' option:selected').data('codigo');
    }

    app.panel(data.modalName, 'Selecionar Critério de Avaliação', 'ficha/getModal', data, function() {
        //var frm = data.contexto ? "#" + data.contexto + ' ' : '';

        // onShow
        $('#divSelecionarCriterioAvaliacao #codCriterioSelecionado').val($(frm + "#" + data.field).val());
        if (data.field) {
            $("#divSelecionarCriterioAvaliacao #divCriterioResultado").html($(frm + "#" + data.field).val());
        }
        if( data.fieldCategoria )
        {
            var codigoCategoria = $(frm+' #'+data.fieldCategoria+' option:selected').data('codigo');
        }
    }, 600, 570, true, false);
};


var fldCriterioAvaliacaoKeyUp = function(input) {
    try {
        if (event) {
            if (event.which == 13) {
                //$(input.form).find('#btnSelCriterioAvaliacao').click();
                $(input).next().click()
            } else if (event.which == 46) {
                // delete
                input.value = '';
            }
        }
    } catch( e ) {}
};

var validarCriterioSelecionado = function(params) {
    var criterio = $('#divSelecionarCriterioAvaliacao #divCriterioResultado').text().trim();
    // para o critério B tem que ter no minimo 2 subnivel selecionado
    if (criterio.indexOf('B') > -1) {
        var grupos = ['a', 'b', 'c'];
        var contador;
        var erro = false;
        $.each(criterio.split(';'), function(k, g) {
            if (g.trim().indexOf('B') == 0) {
                $.each(g.split('+'), function(k1, p) {
                    contador = 0;
                    $.each(grupos, function(k1, l) {
                        contador += (p.indexOf(l) > -1 ? 1 : 0);
                    });
                    if (!erro && contador < 2) {
                        erro = true;
                    }
                });
            }
        });
        if (erro) {
            app.alertInfo('Cada item do grupo B, deve possuir no mínimo 2 subitens selecionados!')
            return;
        }
    }
    $('#divSelecionarCriterioAvaliacao #codCriterioSelecionado').val($('#divSelecionarCriterioAvaliacao #divCriterioSelecionado').html());
    var form = params.contexto ? "#" + params.contexto + ' ' : '';
    if (params.field) {
        $(form + "#" + params.field).val($("#divSelecionarCriterioAvaliacao #divCriterioResultado").text());
    }
    app.closeWindow();
}

var mapAnimateFeature = function(map, vectorLayer, feature, pulsateCount, center) {
    if (mapAnimateFeatureRunning) {
        return;
    }
    mapAnimateFeatureRunning = true;
    var mapExtent = map.getView().calculateExtent(map.getSize())

    pulsateCount = pulsateCount || 5;
    center = (center == true ? true : false);
    var style, image, r, currR, maxR, sign;
    var geomTemp = feature.getGeometry();
    var pontoTemp = new ol.Feature({
        geometry: geomTemp,
        name: 'pulse-' + new Date()
    });

    if (!center && !ol.extent.containsCoordinate(mapExtent, feature.getGeometry().getCoordinates())) {
        //log( 'não precisa animar...')
        return;
    }
    vectorLayer.getSource().addFeature(pontoTemp);
    pontoTemp.setStyle(new ol.style.Style({
        image: new ol.style.Circle({
            radius: 5,
            fill: new ol.style.Fill({
                color: 'rgba(0,0,0,0)'
            }),
            stroke: new ol.style.Stroke({
                color: 'rgba(255,0,0,0.6)',
                width: 2
            })
        })
    }));
    //map.getView().setCenter(ol.proj.transform([lon, lat], 'EPSG:4326', 'EPSG:3857'))
    if (center) {
        map.getView().setCenter(pontoTemp.getGeometry().getCoordinates());
    }
    image = pontoTemp.getStyle().getImage();
    style = pontoTemp.getStyle();
    image = style.getImage();
    r = image.getRadius();
    currR = r;
    maxR = 4 * r;
    sign = 1;
    var pulsate = function(event) {
        var vectorContext = event.vectorContext;
        if (currR > maxR) {
            sign = -1;
            pulsateCount--;
        } else if (currR < r) {
            sign = 1;
            if (!pulsateCount) {
                map.un('postcompose', pulsate);
                vectorLayer.getSource().removeFeature(pontoTemp);
                mapAnimateFeatureRunning = false;
                return;
            }
        }
        currR += sign * 1.5;
        vectorContext.drawFeature(pontoTemp, new ol.style.Style({
            image: new ol.style.Circle({
                radius: currR,
                fill: image.getFill(),
                stroke: image.getStroke()
            })
        }));
        map.render();
    };
    map.on('postcompose', pulsate);
};
// -- fim

var ajaxJson = function( url, data, msg, callback) {
    if( msg )
    {
        app.blockUI(msg);
    }
    $.ajax({
            url: url
            , type: 'POST'
            , cache: false
            , timeout: 0
            //, contentType : "application/json; charset=utf-8"
            , dataType: 'json'
            , data: { data:JSON.stringify(data) }
            , error: function (xhr, message, error) {
                alert(message + 'requisição ajax\nCódigo: ' + xhr.status + ': ' + error + '\nUrl: ' + url );
            },
        }
    ).done(function (res) {
        if( res && res.msg )
        {
            res.type = (res.type ? res.type : 'success');
            if( res.type == 'error' ) {
                app.alertError( res.msg );
            }
            else {
                app.alertInfo(res.msg);
            }
            res.msg='';
        }
        callback && callback( res );
    }).always(function(res){
        if( msg )
        {
            app.unblockUI();
        }
        getWorkers();
    } )
}

var ajaxPost = function( url, data, msg, callback,contentType) {
    contentType = contentType || 'application/x-www-form-urlencoded; charset=UTF-8'; // 'application/json; charset=utf-8'
    if( msg ) { app.blockUI( msg); }
    $.ajax({
            url: url
            , type: 'POST'
            , cache: false
            , timeout: 0
            , contentType : contentType
            , data: data
            , error: function (xhr, message, error) {
                alert(message + 'requisição ajax\nCódigo: ' + xhr.status + ': ' + error + '\nUrl: ' + url );
            },
        }
    ).done(function (res) {
        if( res && res.msg ){
            res.type = (res.type ? res.type : 'success');
            if( res.type == 'error' ) {
                app.alertError( res.msg );
            }
            else {
                app.alertInfo(res.msg);
            }
            res.msg='';
        }
        callback && callback( res );
    }).always(function(res){
        if( msg ){
            app.unblockUI();
        }
        getWorkers();
    } )
}

/**
 *
 * @param delay
 */
var getWorkers = function()
{
    //log('getWorkers() executado. ' + new Date() )
    // não iniciar worker na tela de login
    if( $("#divNewSession").size() > 0 )
    {
        return;
    }
    // inicializar o trabalho
    chkWorker.postMessage({command:'start'});
}

var checkUncheckAll = function( params, elem, evt)
{
    var checked;
    if( $(elem).hasClass('glyphicon-unchecked') )
    {
        checked=true;
        $(elem).removeClass('glyphicon-unchecked').addClass('glyphicon-check');
    }
    else
    {
        checked=false;
        $(elem).removeClass('glyphicon-check').addClass('glyphicon-unchecked');
    }
    // guardar o ultimo checkbox encontrado
    var lastCheck=null;
    var $tableBody = null;

    // encontrar $tableBody no gride com datatable
    //var $divDataTableScrollBody = $( $e.closest("div.dataTables_scroll")).find('div.dataTables_scrollBody');
    if( !params.gridId ) {
        if ($(elem).closest("div.dataTables_scroll").size() == 1) {
            $tableBody = $($(elem).closest("div.dataTables_scroll")).find('div.dataTables_scrollBody');
        } else {
            $tableBody = $(elem).closest('thead').next();
        }
    } else {
        $tableBody = $("#table" + params.gridId.replace(/^#/,'') ).find('tbody');
    }
    if( $tableBody.size() == 1 )
    {
        $tableBody.find('input:checkbox').each(function (i, e) {
            if (!$(this).is(':disabled')) {
                this.checked = checked;
                if (checked) {
                    $(this).closest('tr').addClass('tr-selected');
                } else {
                    $(this).closest('tr').removeClass('tr-selected');
                }
                lastCheck = this;
            }
        });
    }

    // executar o evento onChange do ultimo checkbox encontrado
    if( lastCheck ) {
        try {
            $(lastCheck).change();
        } catch (e) {
        }

    }
    contarChecked( params, elem, evt);
    if( params.cb )
    {
        app.eval( params.cb, params);
    }
};

var contarChecked = function( params, elem, evt)
{
    var total = 0;
    var span = null;
    if( params.container ) {
        var div = $("#" + params.container);
        if( div ) {
            total = div.find('input:checked').length;
            span = div.find('span.total-selecionadas');
            if (span) {
                span.html( total + ' selecionada'+(total>1?'s':''));
            }
        }
    }
};

var makeId = function makeid() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < 5; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
};

var applyTruncate = function(container)
{
    $('#'+container.replace(/^#/,'')+' .div-texto-longo').each(function(i, ele ){
        ele = $(ele);
        var id = 'truncate-'+makeId();
        var texto = ele.text().trim();
        var btn='';
        if( ! ele.attr('data-truncate-id') ) {
            ele.attr('data-truncate-id', id);
            if (texto.length > 100) {
                texto = texto.substr(0, 100) + '...';
                btn = '&nbsp;<button title="Ver o texto na íntegra." type="button" data-id="' + id + '" class="btn btn-xs btn-truncate" onClick="showTruncate(this)"><i class="fa fa-eye" aria-hidden="true"></i></button>';
            }
            ele.closest('td').append('<div id="'+id+'" class="truncate">' + texto + '</div>' + btn);
        }
    });
};

var showTruncate = function( ele)
{
    var id = $(ele).data('id');
    if( id )
    {
        app.showLongText( $('div [data-truncate-id="'+id+'"]').html().trim() );
    }
};

/**
 * remover todas as instancias do editor TINYMCE
 */
var destroyEditors = function()
{
    if(typeof(tinymce) !== 'undefined') {

        while (tinymce.editors.length > 0) {
            tinymce.remove(tinymce.editors[0]);
        }
        tinymce.editors = [];
        if ( tinymce.activeEditor) {
            tinymce.activeEditor.remove();
        }
    }
};



//----------------------------------------------------------------
/**
 * Sanitizer which filters a set of whitelisted tags, attributes and css.
 * For now, the whitelist is small but can be easily extended.
 * https://www.quaxio.com/html_white_listed_sanitizer/
 *
 * @param bool whether to escape or strip undesirable content.
 * @param map of allowed tag-attribute-attribute-parsers.
 * @param array of allowed css elements.
 * @param array of allowed url scheme
 */
function HtmlWhitelistedSanitizer(escape, tags, css, urls) {
    this.escape = escape;
    this.allowedTags = tags;
    this.allowedCss = css;

    // Use the browser to parse the input but create a new HTMLDocument.
    // This won't evaluate any potentially dangerous scripts since the element
    // isn't attached to the window's document. It also won't cause img.src to
    // preload images.
    //
    // To be extra cautious, you can dynamically create an iframe, pass the
    // input to the iframe and get back the sanitized string.
    this.doc = document.implementation.createHTMLDocument();

    if (urls == null) {
        urls = ['http://', 'https://'];
    }

    if (this.allowedTags == null) {
        // Configure small set of default tags
        var unconstrainted = function(x) { return x; };
        var globalAttributes = {
        };
        var url_sanitizer = HtmlWhitelistedSanitizer.makeUrlSanitizer(urls);
        this.allowedTags = {
            'a': HtmlWhitelistedSanitizer.mergeMap(globalAttributes, {
                'href': url_sanitizer
            }),
            'p': globalAttributes,
            'span': globalAttributes,
            'br': globalAttributes,
            'b': globalAttributes,
            'i': globalAttributes,
            'em': globalAttributes,
            'u': globalAttributes
        };
    }
    if (this.allowedCss == null) {
        this.allowedCss = ['color', 'background-color'];
    }
}

HtmlWhitelistedSanitizer.makeUrlSanitizer = function(allowed_urls) {
    return function(str) {
        if (!str) { return ''; }
        for (var i in allowed_urls) {
            if (str.startsWith(allowed_urls[i])) {
                return str;
            }
        };
        return '';
    };
}

HtmlWhitelistedSanitizer.mergeMap = function(/*...*/) {
    var r = {};
    for (var arg in arguments) {
        for (var i in arguments[arg]) {
            r[i] = arguments[arg][i];
        }
    }
    return r;
}

HtmlWhitelistedSanitizer.prototype.sanitizeString = function(input) {
    var div = this.doc.createElement('div');
    div.innerHTML = input;

    // Return the sanitized version of the node.
    return this.sanitizeNode(div).innerHTML;
}

HtmlWhitelistedSanitizer.prototype.sanitizeNode = function(node) {
    // Note: <form> can have it's nodeName overriden by a child node. It's
    // not a big deal here, so we can punt on this.
    var node_name = node.nodeName.toLowerCase();
    if (node_name == '#text') {
        // text nodes are always safe
        return node;
    }
    if (node_name == '#comment') {
        // always strip comments
        return this.doc.createTextNode('');
    }
    if (!this.allowedTags.hasOwnProperty(node_name)) {
        // this node isn't allowed
        if (this.escape) {
            return this.doc.createTextNode(node.outerHTML);
        }
        return this.doc.createTextNode('');
    }

    // create a new node
    var copy = this.doc.createElement(node_name);

    // copy the whitelist of attributes using the per-attribute sanitizer
    for (var n_attr = 0; n_attr < node.attributes.length; n_attr++) {
        var attr = node.attributes.item(n_attr).name;
        if (this.allowedTags[node_name].hasOwnProperty(attr)) {
            var sanitizer = this.allowedTags[node_name][attr];
            copy.setAttribute(attr, sanitizer(node.getAttribute(attr)));
        }
    }
    // copy the whitelist of css properties
    for (var css in this.allowedCss) {
        copy.style[this.allowedCss[css]] = node.style[this.allowedCss[css]];
    }

    // recursively sanitize child nodes
    while (node.childNodes.length > 0) {
        var child = node.removeChild(node.childNodes[0]);
        copy.appendChild(this.sanitizeNode(child));
    }
    return copy;
};


/**
 * só aceitar background-color e color
 */
/*
DOMPurify.addHook('afterSanitizeAttributes', function(node) {
    // Check all style attribute values and proxy them
    filtraStyle( node );
    // if ( node.hasAttribute('style') )
    // {
    //     var styles = node.style;
    //     var output = [];
    //     for(var prop = styles.length-1; prop >= 0; prop--) {
    //         if( node.style[styles[prop] ] && /^background-color$|^color$/.test( styles[prop].toLowerCase() ) )
    //         {
    //             log( styles[prop], node.style[styles[prop] ] );
    //             output.push(styles[prop] + ':' + node.style[styles[prop]] + ';');
    //         }
    //     }
    //     // re-add styles in case any are left
    //     if (output.length) {
    //         node.setAttribute('style', output.join(""));
    //     } else {
    //         node.removeAttribute('style');
    //     }
    // }
    //
    if( node.hasChildNodes() )
    {
        node.childNodes.forEach(function(node) {
            filtraStyle( node );
        })
    }
});
*/
/*
function filtraStyle( node )
{
    if (node.hasAttribute && node.hasAttribute('style') )
    {
        var styles = node.style;
        var output = [];
        for(var prop = styles.length-1; prop >= 0; prop--) {
            if( node.style[styles[prop] ] && /^background-color$|^color$/.test( styles[prop].toLowerCase() ) )
            {
                log( styles[prop], node.style[styles[prop] ] );
                output.push(styles[prop] + ':' + node.style[styles[prop]] + ';');
            }
        }
        // re-add styles in case any are left
        if (output.length) {
            node.setAttribute('style', output.join(""));
        } else {
            node.removeAttribute('style');
        }
    }
}
*/

var tableDeleteItemCheck = function( params )
{
    var count = $("#ol-"+params.rndId).find('input[type=checkbox]:checked').size();
    if( count == 0 ) {
        $("#divListaFooter-" + params.rndId).hide();
    }
    else
    {
        $("#divListaFooter-" + params.rndId).show();
    }
};

var tableDeleteItemCheckAll = function ( params )
{
    //var count = $("#ol-"+params.rndId).find('input[type=checkbox]:checked').size();
    var count = $("#ol-"+params.rndId).find('input[type=checkbox]').not(":checked").size();
    if( count > 0 ) {
        $("#ol-"+params.rndId).find('input[type=checkbox]').prop('checked',true);
    }
    else
    {
        $("#ol-"+params.rndId).find('input[type=checkbox]').prop('checked',false);
        $("#divListaFooter-"+params.rndId).hide();
    }
};

var tableDeleteItemExecute = function( params )
{
    var checks = $("#ol-"+params.rndId).find('input[type=checkbox]:checked');
    if( checks.size() > 0 ) {
       if( app.confirm('Confirma exclusão'+(checks.size() > 1 ?' de ' + checks.size() + ' itens?':'')+'?',
           function() {
                params.values=[];
                checks.map( function(i,item){
                   params.values.push( item.value );
                });
                params.values=params.values.join(',');
                app.eval(params.cb,params,false);
           })
       );
    }
};
/**
 * criar função para permitir selecionar checkbox pressionando a tacla shift
 * Usage: $("#tableDivGridFicha").shiftSelectable()
 */
$( document).ready(function() {

    $.fn.shiftSelectable = function() {
        var $chkboxes   = this.find('.checkbox-lg');
        var lastChecked = null;
        var bgTag       = 'tr';
        $chkboxes.click(function(e) {

            // alterar a cor de fundo da linha
            if( this.checked ) {
                $(this).closest(bgTag).addClass('tr-selected');
            }
            else
            {
                $(this).closest(bgTag).removeClass('tr-selected');
            }

            if( ! lastChecked) {
                if( this.checked ) {
                    lastChecked = this;
                }
                return;
            }


            if(e.shiftKey) {
                var start = $chkboxes.index(this);
                var end = $chkboxes.index(lastChecked);
                //$chkboxes.closest('tr').removeClass('tr-selected')
                $chkboxes.slice(Math.min(start, end), Math.max(start, end) + 1).prop('checked', this.checked);
                if( this.checked ) {
                    $chkboxes.slice(Math.min(start, end), Math.max(start, end) + 1).closest(bgTag).addClass('tr-selected');
                }
                else
                {
                    $chkboxes.slice(Math.min(start, end), Math.max(start, end) + 1).closest(bgTag).removeClass('tr-selected');
                }
                //$chkboxes.slice(Math.min(start,end), Math.max(start,end)+ 1)
                lastChecked=this;

            }
            else {
                lastChecked = null;
            }
        });

    };
});



/****************************************************************/
/**************** WORKERS DA APLICAÇÃO **************************/
/****************************************************************/
var chkWorker;
$( document).ready(function() {
    if (!window.Worker) {
        console.log('Navegador sem suporte a Worker!');
        return
    }

    // se o usuario ainda não estiver autenticado, não executar o worker
    if( $("#spanLoginUserName").size() == 0 ) {
        return;
    }

    // Worker para monitorar as atividads em segundo plano
    chkWorker = new Worker(baseUrl + 'assets/workers/chkWorkers.js');
    chkWorker.onmessage = function (event) {
        var res = event.data;

        if ( res.error ) {
            app.growl(res.error,10,'Erro na execução da tarefa','tc','','error')
            return;
        }
        // ler a tag <li> localizada no canto superior direito da tela
        var $badge = $("#li-menu-user-jobs span.badge")
        if( $badge.size() == 0 ) {
            // menu ainda não foi carregado ou o usuário não está logado
            return;
        }
        // adicionar/remove animação na imagem do canto superior direito da tela
        if (res.length == 0) {
            // não existe mais tarefa em execução
            $badge.removeClass('blink-me');
            $badge.find('i').removeClass('fa-spin');
            return;
        } else {
            // existe tarefa em execução
            $badge.addClass('blink-me');
            $badge.find('i').addClass('fa-spin');
        }

        // processar as tarefas retornadas pelo worker
        res.map(function (job, i) {
            if ( job && job.type == 'job') {
                processarUserJob(job);
            }
        });
    }
});

/**
 * fazer o processamento do job retornado pelo loop chkWorker
 * @param jobs
 */
function processarUserJob( job ) {
    var isModalJobsOpened = $("#modalUserJobs").is(":visible");
    /*
    console.log('processar jobs')
    console.log(job)
    */

    if (!job && !job.type == 'job') {
        return;
    }

    var vlPercentual = parseInt(String(job.vlPercentual));

    // atualizar as colunas do gride da tela de acompanhamento dos jobs se estiver aberta
    if (isModalJobsOpened) {
        for (var key in job) {
            // atualizar linha do gride
            if (key == 'vlPercentual') {
                var $divProgress = $("div#progress-" + job.id);
                if ($divProgress.size() == 1) {
                    var $divProgressBar = $divProgress.find('div.progress-bar');
                    var $divAndamento = $('small#progress-andamento-'+job.id);
                    $divProgressBar.css('width', job.vlPercentual + '%');
                    $divProgressBar.html(job.vlPercentual + '%')
                    $divAndamento.html(job.deAndamento);
                    // habilitar o botão de baixar o arquivo
                    if (vlPercentual >= 100) {
                        var $btnDownload = $("button#btnDownloadUserJob-" + job.id)
                        var buttonData = $btnDownload.data();
                        if (buttonData.noArquivo) {
                            $btnDownload.prop('disabled', false);
                            $btnDownload.removeClass('disabled');
                        }
                    }
                }
            } else {
                // atualizar as colunas da tr de acordo com o nome da key
                $("tr#tr-" + job.id + ' td#' + key + 'Col-' + job.id).html(job[key]);
            }
        }
    }

    // executar javascript quando o job estiver terminado e ainda não tiver sido executado
    if (vlPercentual >= 100 && !job.stExecutado) {
        // atualizar o job para executado
        app.ajax('main/updateUserJob', {sqUserJob: job.id, stExecutado: true}, function (res) {
            if (res.status == 0) {
                // executar o js
                if (job.jsExecutar) {
                    app.eval(job.jsExecutar, [job], false);
                }
                // fazer o download do arquivo
                if (job.noArquivoAnexo && !job.deEmail) {
                    var noArquivoAnexo = job.noArquivoAnexo;
                    setTimeout(function () {
                        downloadWithProgress({fileName: noArquivoAnexo});
                    }, 500);
                }
                if (isModalJobsOpened) {
                    app.closeWindow('modalUserJobs');
                    // atualizar gride jobs usuario
                    job.stExecutado = true;
                    job.deSituacao = 'Executado';
                    job.jsExecutar = '';
                    job.noArquivoAnexo = '';
                    processarUserJob(job);
                }
            }
        });
    } else if (vlPercentual == 0 && job.deErro && !job.stExecutado && !job.stCancelado) {
        app.growl(job.deErro, 5, 'Tarefa finalizada', 'tc', 'large', 'error')
        app.ajax('main/updateUserJob', {sqUserJob: job.id, stExecutado: true});
    }
}


var showShapeFile = function( params ){
    app.blockUI('Criando mapa...');
    var el = "map" + params.id
    var osm = new ol.layer.Tile({
        source: new ol.source.OSM({
            maxZoom: 19,
            crossOrigin: 'anonymous'
        }),
        visible: true,
    });

    var view =  new ol.View({
        center: [-6019690.043300373, -2814310.6931742462] // Brasil
        ,zoom: 4
    })

    var style = new ol.style.Style({
        // cor preenchimento
        fill: new ol.style.Fill({
            //color: 'rgba(255, 255, 100, 0.2)'
            color: '#f3001d'
        }),
        // cor da borda
        stroke: new ol.style.Stroke({
            color: '#790000',
            width: 1
        }),
        // cor e preenchimento quando o desenho for um ponto
        image: new ol.style.Circle({
            radius: 4,
            fill: new ol.style.Fill({
                color: '#f3001d'
            }),
            stroke: new ol.style.Stroke({
                color: '#790000',
                width: 1
            }),
        })
    });
    var map = new ol.Map({
        layers: [osm],
        target: el,
        view: view
    });

    //shp("/salve-estadual/assets/testeshp.zip").then(function(geojson){
    shp(baseUrl+"ficha/getAnexo/"+params.id+'.zip').then(function(geojson){
        //console.log( geojson)
        //JSON.stringify(data)

        var features = (new ol.format.GeoJSON() ).readFeatures( geojson, { featureProjection: 'EPSG:3857' });
        features = features.filter(function(feature,i){
            var geometry = feature.getGeometry();
            if( geometry.getType() == 'Point' ) {
                var coords = geometry.getCoordinates();
                if (isNaN(coords[1]) || isNaN(coords[0]) || Math.abs(coords[0]) === Infinity) {
                    return false;
                } else {
                    return true;
                }
            } else {
                return true;
            }
        });

        //var layer = new ol.layer.Vector({
        var layer = new ol.layer.Vector({
            source: new ol.source.Vector({
                features :  features//(new ol.format.GeoJSON() ).readFeatures( geojson, { featureProjection: 'EPSG:3857' }),
            }),
            style:style,
            visible: true
        });
        map.addLayer( layer );
        app.unblockUI();
        map.getView().fit( layer.getSource().getExtent(), map.getSize() );
        /*var extent = ol.proj.transformExtent( data.bbox, "EPSG:4326", "EPSG:3857");
        map.getView().fit(extent,map.getSize() );
        map.getView().setZoom(map.getView().getZoom()-1)
        */

    },function(){
        // erro ao abrir o arquivo zip do shapefile
        app.alertInfo('Arquivo shapefile inválido!');
        app.unblockUI();
    });
};

var sortSelectOptions = function(selector, skip_first) {
    var options;
    if( typeof selector=='string') {
        options = (skip_first) ? $(selector + ' option:not(:first)') : $(selector + ' option');
    } else if( typeof selector=='object') {
        options = (skip_first) ? $(selector).find('option:not(:first)') : $(selector).find('option');
    }

    var arr = options.map(function(_, o) { return { t: $(o).text(), v: o.value, s: $(o).prop('selected') }; }).get();
    arr.sort(function(o1, o2) {
        var t1 = o1.t.toLowerCase(), t2 = o2.t.toLowerCase();
        return t1 > t2 ? 1 : t1 < t2 ? -1 : 0;
    });
    options.each(function(i, o) {
        o.value = arr[i].v;
        $(o).text(arr[i].t);
        if (arr[i].s) {
            $(o).attr('selected', 'selected').prop('selected', true);
        } else {
            $(o).removeAttr('selected');
            $(o).prop('selected', false);
        }
    });
};
/**
 * limpar tags html do texto
 * @param html
 * @returns {*}
 */
var sanitizeHtml = function( html )
{
    return html.replace(/<\/?[^>]+(>|$)/g, "").replace(/&nbsp;/ig,' ').trim();
};

/**
 * função para emitir um Beep autofalante do no computador do usuário
 */
function beep() {
    try {
        var snd = new Audio("data:audio/wav;base64,//uQRAAAAWMSLwUIYAAsYkXgoQwAEaYLWfkWgAI0wWs/ItAAAGDgYtAgAyN+QWaAAihwMWm4G8QQRDiMcCBcH3Cc+CDv/7xA4Tvh9Rz/y8QADBwMWgQAZG/ILNAARQ4GLTcDeIIIhxGOBAuD7hOfBB3/94gcJ3w+o5/5eIAIAAAVwWgQAVQ2ORaIQwEMAJiDg95G4nQL7mQVWI6GwRcfsZAcsKkJvxgxEjzFUgfHoSQ9Qq7KNwqHwuB13MA4a1q/DmBrHgPcmjiGoh//EwC5nGPEmS4RcfkVKOhJf+WOgoxJclFz3kgn//dBA+ya1GhurNn8zb//9NNutNuhz31f////9vt///z+IdAEAAAK4LQIAKobHItEIYCGAExBwe8jcToF9zIKrEdDYIuP2MgOWFSE34wYiR5iqQPj0JIeoVdlG4VD4XA67mAcNa1fhzA1jwHuTRxDUQ//iYBczjHiTJcIuPyKlHQkv/LHQUYkuSi57yQT//uggfZNajQ3Vmz+Zt//+mm3Wm3Q576v////+32///5/EOgAAADVghQAAAAA//uQZAUAB1WI0PZugAAAAAoQwAAAEk3nRd2qAAAAACiDgAAAAAAABCqEEQRLCgwpBGMlJkIz8jKhGvj4k6jzRnqasNKIeoh5gI7BJaC1A1AoNBjJgbyApVS4IDlZgDU5WUAxEKDNmmALHzZp0Fkz1FMTmGFl1FMEyodIavcCAUHDWrKAIA4aa2oCgILEBupZgHvAhEBcZ6joQBxS76AgccrFlczBvKLC0QI2cBoCFvfTDAo7eoOQInqDPBtvrDEZBNYN5xwNwxQRfw8ZQ5wQVLvO8OYU+mHvFLlDh05Mdg7BT6YrRPpCBznMB2r//xKJjyyOh+cImr2/4doscwD6neZjuZR4AgAABYAAAABy1xcdQtxYBYYZdifkUDgzzXaXn98Z0oi9ILU5mBjFANmRwlVJ3/6jYDAmxaiDG3/6xjQQCCKkRb/6kg/wW+kSJ5//rLobkLSiKmqP/0ikJuDaSaSf/6JiLYLEYnW/+kXg1WRVJL/9EmQ1YZIsv/6Qzwy5qk7/+tEU0nkls3/zIUMPKNX/6yZLf+kFgAfgGyLFAUwY//uQZAUABcd5UiNPVXAAAApAAAAAE0VZQKw9ISAAACgAAAAAVQIygIElVrFkBS+Jhi+EAuu+lKAkYUEIsmEAEoMeDmCETMvfSHTGkF5RWH7kz/ESHWPAq/kcCRhqBtMdokPdM7vil7RG98A2sc7zO6ZvTdM7pmOUAZTnJW+NXxqmd41dqJ6mLTXxrPpnV8avaIf5SvL7pndPvPpndJR9Kuu8fePvuiuhorgWjp7Mf/PRjxcFCPDkW31srioCExivv9lcwKEaHsf/7ow2Fl1T/9RkXgEhYElAoCLFtMArxwivDJJ+bR1HTKJdlEoTELCIqgEwVGSQ+hIm0NbK8WXcTEI0UPoa2NbG4y2K00JEWbZavJXkYaqo9CRHS55FcZTjKEk3NKoCYUnSQ0rWxrZbFKbKIhOKPZe1cJKzZSaQrIyULHDZmV5K4xySsDRKWOruanGtjLJXFEmwaIbDLX0hIPBUQPVFVkQkDoUNfSoDgQGKPekoxeGzA4DUvnn4bxzcZrtJyipKfPNy5w+9lnXwgqsiyHNeSVpemw4bWb9psYeq//uQZBoABQt4yMVxYAIAAAkQoAAAHvYpL5m6AAgAACXDAAAAD59jblTirQe9upFsmZbpMudy7Lz1X1DYsxOOSWpfPqNX2WqktK0DMvuGwlbNj44TleLPQ+Gsfb+GOWOKJoIrWb3cIMeeON6lz2umTqMXV8Mj30yWPpjoSa9ujK8SyeJP5y5mOW1D6hvLepeveEAEDo0mgCRClOEgANv3B9a6fikgUSu/DmAMATrGx7nng5p5iimPNZsfQLYB2sDLIkzRKZOHGAaUyDcpFBSLG9MCQALgAIgQs2YunOszLSAyQYPVC2YdGGeHD2dTdJk1pAHGAWDjnkcLKFymS3RQZTInzySoBwMG0QueC3gMsCEYxUqlrcxK6k1LQQcsmyYeQPdC2YfuGPASCBkcVMQQqpVJshui1tkXQJQV0OXGAZMXSOEEBRirXbVRQW7ugq7IM7rPWSZyDlM3IuNEkxzCOJ0ny2ThNkyRai1b6ev//3dzNGzNb//4uAvHT5sURcZCFcuKLhOFs8mLAAEAt4UWAAIABAAAAAB4qbHo0tIjVkUU//uQZAwABfSFz3ZqQAAAAAngwAAAE1HjMp2qAAAAACZDgAAAD5UkTE1UgZEUExqYynN1qZvqIOREEFmBcJQkwdxiFtw0qEOkGYfRDifBui9MQg4QAHAqWtAWHoCxu1Yf4VfWLPIM2mHDFsbQEVGwyqQoQcwnfHeIkNt9YnkiaS1oizycqJrx4KOQjahZxWbcZgztj2c49nKmkId44S71j0c8eV9yDK6uPRzx5X18eDvjvQ6yKo9ZSS6l//8elePK/Lf//IInrOF/FvDoADYAGBMGb7FtErm5MXMlmPAJQVgWta7Zx2go+8xJ0UiCb8LHHdftWyLJE0QIAIsI+UbXu67dZMjmgDGCGl1H+vpF4NSDckSIkk7Vd+sxEhBQMRU8j/12UIRhzSaUdQ+rQU5kGeFxm+hb1oh6pWWmv3uvmReDl0UnvtapVaIzo1jZbf/pD6ElLqSX+rUmOQNpJFa/r+sa4e/pBlAABoAAAAA3CUgShLdGIxsY7AUABPRrgCABdDuQ5GC7DqPQCgbbJUAoRSUj+NIEig0YfyWUho1VBBBA//uQZB4ABZx5zfMakeAAAAmwAAAAF5F3P0w9GtAAACfAAAAAwLhMDmAYWMgVEG1U0FIGCBgXBXAtfMH10000EEEEEECUBYln03TTTdNBDZopopYvrTTdNa325mImNg3TTPV9q3pmY0xoO6bv3r00y+IDGid/9aaaZTGMuj9mpu9Mpio1dXrr5HERTZSmqU36A3CumzN/9Robv/Xx4v9ijkSRSNLQhAWumap82WRSBUqXStV/YcS+XVLnSS+WLDroqArFkMEsAS+eWmrUzrO0oEmE40RlMZ5+ODIkAyKAGUwZ3mVKmcamcJnMW26MRPgUw6j+LkhyHGVGYjSUUKNpuJUQoOIAyDvEyG8S5yfK6dhZc0Tx1KI/gviKL6qvvFs1+bWtaz58uUNnryq6kt5RzOCkPWlVqVX2a/EEBUdU1KrXLf40GoiiFXK///qpoiDXrOgqDR38JB0bw7SoL+ZB9o1RCkQjQ2CBYZKd/+VJxZRRZlqSkKiws0WFxUyCwsKiMy7hUVFhIaCrNQsKkTIsLivwKKigsj8XYlwt/WKi2N4d//uQRCSAAjURNIHpMZBGYiaQPSYyAAABLAAAAAAAACWAAAAApUF/Mg+0aohSIRobBAsMlO//Kk4soosy1JSFRYWaLC4qZBYWFRGZdwqKiwkNBVmoWFSJkWFxX4FFRQWR+LsS4W/rFRb/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////VEFHAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAU291bmRib3kuZGUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMjAwNGh0dHA6Ly93d3cuc291bmRib3kuZGUAAAAAAAAAACU=");
        snd.duration = 5;
        snd.play();
    } catch (e) {
    }
}

/**
 * função para alterar a imagem do botão toggle ao abrir e fechar a área
 * @param ele
 */
var toggleImage = function(ele) {
    //if( $(ele).hasClass('fa-chevron-down') || $(ele).hasClass('fa-chevron-up') ){
    if( $(ele).is('.fa-chevron-down, .fa-chevron-up') ) {
        $i = $(ele)
    } else {
        $i = $(ele).find('i.fa')
    }
    if( $i.size()==1) {
        if ($(ele).hasClass('fa-chevron-down')) {
            $(ele).removeClass('fa-chevron-down').addClass('fa-chevron-up blue');
        } else {
            $(ele).removeClass('fa-chevron-up blue').addClass('fa-chevron-down');
        }
    }
};

/**
 *
 * @param params
 */
var btnGravarTextoLivreClick = function( params )
{
    var $textArea = $("#fldTextoLivre"+params.id);
    if( $textArea.size() == 0 )
    {
        return;
    }
    // remover formatacao html
    var texto = trimEditor( sanitizeHtml( $textArea.val() ) );
    $textArea.val( texto );
    if( !texto )
    {
        return;
    }
    // indicar na função callback que a janela foi fechada pelo botão Gravar
    $textArea.prop('modified','S');
    app.closeWindow();
};

/**
 * Ação executada ao clicar no botão gravar da modal _textoLivre
 * @param params
 * @param ele
 * @param evt
 */
var editNaoAceita = function( params, ele, evt )
{
    var id = params.id;
    var $select=null;
    if( params.selectId )
    {
        $select = $("#"+params.selectId.replace(/^#/,'') );
    }
    else
    {
        $select = $("#selectColaboracao"+id);
    }
    if( $select.size() == 1 )
    {
        // chamar a janela popup de edição
        $select.change();
    }
};

var refreshChat = function(params) {
    if( params.sqFicha && params.sqOficinaFicha ) {
        app.blockUI('Atualizando chat....',function(){
            salveWorker.startChat({sqFicha      :params.sqFicha
                ,sqOficinaFicha :params.sqOficinaFicha});
        },function(){
            app.growl('Chat atualizado.',2,'','tc','small','success');
        },2)
    }
};

/**
 * mostrar exibir o texto sobre a política de dados ao marcar a opção
 * Exportar dados em carência
 * @param params
 * @param ele
 * @param evt
 */
var chkCsvCarenciaClick = function( params, ele, evt )
{
    if( ele.checked )
    {
        $("#divConcordarPoliticaDados").show('slow');
    }
    else {
        $("#divConcordarPoliticaDados").hide();
    }
};

var exportarOcorrenciasCsv = function( data ) {
    if( ! data )
    {
        return;
    }
    if( data.contexto == 'ficha') {
        if (data.todasPaginas && data.todasPaginas == 'S' && !data.simNao) {
            app.confirm('<br><b>Confirma a exportação dos registros de TODAS as fichas</b>?',
                function () {
                    data.simNao = 'S'
                    exportarOcorrenciasCsv(data);
                },
                null, null, 'Confirmação', 'warning');
            return;
        }
    }

    app.ajax(app.url + 'fichaDisOcorrencia/exportCsv', data, function(res) {
        if (res.status == 0) {
            window.setTimeout(function(){
                getWorkers();
            },5000);
            window.setTimeout(function(){
                getWorkers();
            },60000);
            app.growl('Ao finalizar será exibida a tela para download do arquivo.<br><br>Calculando registros...',8,'Exportação em andamento.','tc','large','info');
        };
    }, 'Exportando registros em formato CSV. Aguarde...', 'json');
 };

var loadScript = function( files, callback ) {
    if( typeof files == 'string' )
    {
        files = [files];
    }
    if( files ) {
        $.each(files, function (index, file) {
            if (file.substring(0, 1) !== '/' && file.substring(0, 4) != 'http') {
                file = '/' + file;
            }
            if (file.substring(0, 2) !== '//') {
                if (file.substring(0, 1) === '/') {
                    file = _url + 'javascript' + file
                }
            }
                // adicionar css
            if (/\.css$/.test(file)) {
                    $("head").append($("<link rel='stylesheet' href='" + file + "' type='text/css' media='screen' />"));
            } else {
                // adicionar js
                $.cachedScript(file).done(function (script, textStatus) {
                    if (textStatus == 'success') {
                        app.eval(callback);
                    } else {
                        console.info(textStatus + '\nloadScript: ' + file);
                    }
                }).fail(function (jqxhr, settings, exception) {
                    warn(settings);
                    warn(exception);
                    warn(jqxhr);
                    console.error('Erro na função app.loadModule.\n\nArquivo: ' +
                        file + '\n\nErro: ' + exception);
                });
            }
        });
    }
};
/**
 * Exibir a janela modal com o texto do informativo
 * @param data
 */
var openInformativoAlerta = function( data ) {
    var $modal = $("#modalAlertaInformativos");
    if( $modal.is(":visible") ) {
        return;
    }
    var linkVerMais = $modal.find('a#linkAlertaInformativoLerMais');
    if( linkVerMais.size() == 1 ) {
        if (data.deLocalArquivo) {
            linkVerMais.prop('href', '/salve-estadual/download?type=informativo&fileName=' + data.deLocalArquivo);
            linkVerMais.show();
        } else {
            linkVerMais.hide();
        }
    }
    $modal.data('nuInformativo',data.nuInformativo);
    $modal.data('sqInformativo',data.sqInformativo);
    $modal.find('span#numeroInformativo').html(data.nuInformativo);
    $modal.find('div.modal-body').html( (data.deTema ? '<p class="informativo-tema">'+data.deTema+'</p>' : '') +
        '<p class="informativo-texto">'+data.txInformativo+'</p>');
    $modal.modal({backdrop:'static',keyboard: false,show:true,focus:true})
}
/**
 * Exibir preview da imagem na modal de upload de arquivos
 * @param input
 */
var uploadFileChange = function( input ) {
    try {
        const file = input.files[0];
        var $form = $("#frmUpdateArquivoSistema")
        if (file && file.name.match(/\.(png|jpe?g)$/i)) {
            $form.find('div.upload-preview').show();
            var $img = $form.find('img#preview_img');
            const fileReader = new FileReader();
            fileReader.addEventListener("load", function () {
                $img[0].setAttribute("src", this.result);
            });
            fileReader.readAsDataURL(file);
        } else {
            $form.find('div.upload-preview').hide();
        }
        // valida o tamanho do anexo com o limite definido
        const maxFileSize = input.getAttribute('data-max-file-size');
        if(maxFileSize && parseInt(maxFileSize) > 0){
            if(input.files.item(0)){
                const fsize = input.files.item(0).size;
                if( fsize > 0 && Math.round((fsize / 1024)) > maxFileSize){
                    const sizeLimit = parseFloat(parseInt(input.getAttribute('data-max-file-size')) / 1024).toFixed(2);
                    app.alertError("O arquivo anexado é maior que o limite permitido. \nLimite permitido: " + sizeLimit + "MB" );
                }
            }
        }
    } catch( e ) {}
}

var stripTags = function(html, tagsToPreserve) {
    return html.replace(/<(\/?)(\w+)[^>]*\/?>/g, (_, endMark, tag) => {
        return tagsToPreserve.includes(tag) ? '<' + endMark + tag + '>' :'';
    }).replace(/<!--.*?-->/g, '');
}

var limparCampoTexto = function( texto ) {


    if( ! texto ) {
        return ''
    }

    var attributosPreservar = ['background-color:'];

    var tagsPreservar = ['strong','em','sup','sub','span','ol','li','ul','i', 'a', 'p', 'b']
    tagsPreservar.forEach(function(tag) {
        texto = texto.replaceAll('<' + tag+'>' , '#'  + tag +'#'); // <p> #p#
        texto = texto.replaceAll('<' + tag+' ' , '#'  + tag +':'); // <p style="">  #p:style="">
        texto = texto.replaceAll('</' + tag+'>', '#/' + tag +'#'); // </p> #/p#
    });

    texto = stripTags( texto ,[])

    /*
    var $div = $('<div style="display:none;">'+texto+'<div>')
    texto = $div.text();
    $div.remove();
    */

    var tagsExcluir = [
        // remover tags no format xxxx-xxxxx:xxxxx
        ' ?((caret|-webkit|word|white|letter|text|font|overflow)-([a-z]{1,}(-[a-z-]{1,})?)|ff): ((\'[0-9a-zA-Z-_ \\\\()\\.]{1,}\'"|["%0-9a-zA-Z-_ \\\\()\\.]{1,}),?){1,}; ?'
        // remover background image, repeat, attachment, size, origin, positition e clip
        ,' ?background-(image|repeat|attachment|size|origin|position|clip): [a-z0-9% \\.-]{1,};?'
        // remover: caret-color: xxxx e color: xxxxx
        //,' ?color: #?[0-9A-Z\\(\\) ,]{1,}; ?'
        // remover class=""
        ,' ?class="(hps|MsoNormal|apple-style-span|longtext1?|BodyText21)"'
        // remover line-height: xxx
        ,' ?line-height: ?([a-z0-9%\\.]{1,}); ?'
    ];



    // voltar tags preservadas
    tagsPreservar.forEach(function(tag) {
        texto = texto.replaceAll('#/' + tag + '#', '</' + tag + '>'); // </p> #/p#
        texto = texto.replaceAll('#'  + tag + ':', '<'  + tag  + ' ' ); // <p style="">  #p:style="">
        texto = texto.replaceAll('#'  + tag + '#', '<'  + tag  + '>' ); // <p> #p#
    });


    // proteger atributos
    attributosPreservar.map( function( attr){
        var attrInvertido = attr.split('').reverse().join('');
        texto = texto.replaceAll( attr, attrInvertido);
    });

    // limpar tags
    tagsExcluir.map( function(regex){
        var re = new RegExp(regex,'gim');
        texto = texto.replace( re, '');
    });

    // desproteger atributos
    attributosPreservar.map( function( attr){
        var attrInvertido = attr.split('').reverse().join('');
        texto = texto.replaceAll( attrInvertido, attr );
    });

    // remover as tags vazias
    texto = texto.replace(/<([^<\/>]*)>([\s]*?|(\?R))<\/\1>/gmi,'')

    /**
     * remover atributos style e class completo mantendo
     * somente se forem os styles no formato abaixo:
     * style="background-color: #xxxxxx;">
     * style="color: #xxxxxx;">
     * style="background-color: #xxxxxx; color: #xxxxxx;">
     * style="background: #xxxxxx;">
     * style="background: #xxxxxx; color: #xxxxxx;">
     */
    texto = texto.replace(/style="background: (#.{6}); color: (#.{6});">/gmi,'sbgc: $1; cl: $2;">')
    texto = texto.replace(/style="background: (#.{6};?)">/gmi,'sbgc: $1;">')
    texto = texto.replace(/style="background-color: (#.{6}); color: (#.{6});">/gmi,'sbgc: $1; cl: $2;">')
    texto = texto.replace(/style="background-color: (#.{6});">/gmi,'sbgc: $1;">')
    texto = texto.replace(/style="color: (#.{6});">/gmi,'sc: $1;">')

    texto = texto.replace(/(style|class)=([\s\S"]+?)"/gmi,'');

    texto = texto.replace(/sbgc: (#.{6}); cl: (#.{6});">/gmi,'style="background-color:$1; color: $2;">');
    texto = texto.replace(/sbgc: (#.{6});">/gmi,'style="background-color:$1;">');
    texto = texto.replace(/sc: (#.{6});">/gmi,'style="color:$1;">');

    // console.log(' ')
    //console.log( texto );
    return texto;
 }

 // novo
 function restoreSession( data ) {
     data = data || {};
     var cookie = window.grails.sisicmbio
     if( cookie ) {
         if( ! data.silent ) {
             app.growl('Tentando restaurar sua última sessão. Aguarde...', 15, 'Sistema está on-line', 'tc');
         }
         stopPing();
         $.ajax({
             url: app.url + 'main/restoreSession'
             , type: 'POST'
             , cache: false
             , timeout: 0
             , dataType: 'json'
             , data: {cookie: cookie}
         }).done(function (res) {
             if (res.error) {
                 app.alertError((res.msg ? res.msg + '<br><br>' : '') + res.error);
             } else {
                 if( res.status == 1 ){
                    app.alertInfo( res.msg );
                 } else {
                     if (!data.silent) {
                         app.alertInfo('Sessão restaurada com SUCESSO!');
                     }
                     startPing();
                 }
             }
         }).fail(function (xhr, status, error) {
             app.alertInfo('Sistema está em manutenção. Tente novamente em alguns minutos.');
         }).always(function () {
             $("div#growls").html('');
             app.unblockUI();
         });
     }
}

/**
 * função para baixar aquivos e exibir o progresso do download
 * @param params
 * @returns {Promise<void>}
 */
async function downloadWithProgress( params ) {
    BootstrapDialog.closeAll();
    params = params || {};
    params.originalFileName = params.originalFileName ? params.originalFileName : params.fileName;
    if( !params.fileName ) {
        return;
    }
    var downloadUrl =  baseUrl + 'download?fileName='+params.fileName+'&downloadName=novo.xlsx'
    var percent = 0
    var progressAlert = app.alert('<div class="progress"><div class="progress-bar" id="prograssBar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%">0%</div></div>')
    var $divProgress = progressAlert.getModalBody().find('div.progress-bar')
    var receivedLength = 0; // received that many bytes at the moment
    var chunks = []; // array of received binary chunks (comprises the body)
    try {
        var response = await fetch( downloadUrl );
        if( response.status != 200 ){
            var msg = 'Ocorreu um erro ' + response.statusText + ' ('+response.status+')'
            if(response.status == 503) {
                msg = 'Arquivo inexistente.'
            }
            throw (msg)
        }
        var reader = response.body.getReader();
        var contentLength = +response.headers.get('Content-Length');
        while (true) {
            var {done, value} = await reader.read();
            if (done) {
                break;
            }
            chunks.push(value);
            receivedLength += value.length;
            percent = parseInt(String(receivedLength / contentLength * 100));
            $divProgress.text(String(percent) + '%');
            $divProgress.css('width', $divProgress.text());
        }
    } catch( e ){
        $divProgress.css('width', '100%');
        $divProgress.html( '<small>'+ e + '</small>' );
        return;
    }
    //var blob = new Blob(chunks, { type: "application/pdf" });
    var blob = new Blob(chunks, { type: "application/octetstream" });
    const url = URL.createObjectURL(blob);
    var a = document.createElement("a");
    document.body.appendChild(a);
    a.style = "display: none";
    a.href = url;
    var downloadFileName = params.originalFileName;
    try{
        var posBarra = params.originalFileName.lastIndexOf('/')+1
        downloadFileName = params.originalFileName.substring(posBarra);
    }catch(e){}
    a.download = downloadFileName
    a.click();
    URL.revokeObjectURL(url);
    document.body.removeChild(a);
    setTimeout(function(){progressAlert.close();},1000);
}


/**
 * habilitar a tecla Enter para gravar quando estiver editando algum texto inline
 * @param ele
 */
var inlineEditKeyUp = function( ele ){
    if( event && (event.key == 'Enter' || event.keyCode == 13) ){
        $(ele).next('i.fa').click();
    }
}

/**
 * fazer a gravação das edições inline de texto
 * @param ele
 */
var saveInlineEditContent=function(ele){
    var $i = $( ele );
    var $p = $i.parent();
    var $textElement = $p.parent().find('*.inline-edit-content');
    var $inlineEditI = $p.parent().find('i.inline-edit');
    var $input  = $p.find('input.input-inline-edit');
    var data    = $inlineEditI.data();
    data.newText = String($input.val()).trim();
    var changed = String( $textElement.text() ).trim() != data.newText;
    $p.remove();
    //$p.html('<small>Gravando...</small>');
    $textElement.show();
    $inlineEditI.show();
    if( changed ) {
        app.ajax(baseUrl + 'main/saveEditableLabels', data, function (res) {
            //$p.remove();
            if( res.status == 0 ) {
                $textElement.text(data.newText );
            }
        }, null, 'json');
    }
}

var corrigirCategoriasTexto = function( texto ) {
    // texto.replace(/Dados Insuficientes ?-? ?(\(?[A-Z]{2}\)?([ .]))/igm,'Dados Insuficienties (DD)$2');
    var categorias = [

        {nome: 'Possivelmente Extinta', sigla: 'PEX'},
        {nome: 'Regionalmente Extinta', sigla: 'RE'},
        {nome: 'Extinta na Natureza', sigla: 'EW'},
        {nome: 'Criticamente em Perigo', sigla: 'CR'},
        {nome: 'Vulnerável', sigla: 'VU'},
        {nome: 'Em Perigo', sigla: 'EN'},
        {nome: 'Quase Ameaçada', sigla: 'NT'},
        {nome: 'Dados Insuficientes', sigla: 'DD'},
        {nome: 'Não Aplicável', sigla: 'NA'},
        {nome: 'Não Avaliada', sigla: 'NE'},
        {nome: 'Extinta', sigla: 'EX'},
        {nome: 'Menos Preocupante', sigla: 'LC'}
    ]
    texto = decodeHTMLEntity(texto) + ' '
    categorias.map(function (categoria, index) {
        var re = new RegExp(categoria.nome + ' ?-? ?(\\(?'+categoria.sigla+'\\)?([> ,.<]))', 'gim');
        texto = texto.replace(re, categoria.nome + ' (' + categoria.sigla + ')$2');
    });
    return $.trim(texto);
}
/**
 * função para converter os caracteres especiais html para o correspondente da lingua portuguesa
 * Ex: &ccedil; -> ç
 * @param text
 * @returns {*}
 */
var decodeHTMLEntity = function(text){
    var span = document.createElement('span');
    var res = text.replace(/&[#A-Za-z0-9]+;/gi, function( entity, position, text ) {
            span.innerHTML = entity;
            return span.innerText;
        });
    $(span).remove();
    return res
}

/**
 * janela modal para exibir uma determinada versão da ficha a partir do json do versionamento
 * @param params
 */
var showFichaVersao = function( params ) {
    var fullscreen=false
    var windowWidth = Math.min(800,$(window).width() - 50);
    var windowHeight = Math.max(500,window.innerHeight-130);
    var panel = app.panel('pnlFichaVersao', 'Ficha Versão', 'fichaVersao/', params, function(panel) {
            // onOpen
            fichaVersao.panelData = panel.find("div#container-ficha-versao").data();
            panel.headerTitle('Ficha ' + fichaVersao.panelData.nmCientificoSemAutor + ' - Versão ' + fichaVersao.panelData.nuVersao);

            // inicializar o módulo
            setTimeout(function(){fichaVersao.init()},1000);
        }
        ,windowWidth
        ,windowHeight
        ,true
        ,fullscreen,
        function() {
                if( fichaVersao.currentEditor.instance ) {
                    tinymce.get(fichaVersao.editorOptions.selector.replace(/^#/, '')).destroy();
                }
        },'cc',"#EFEFEF"
        ,false
        ,false);
};


/**
 * habilitar a tecla Enter para gravar quando estiver editando algum texto inline
 * @param ele
 */
var showFundamentacaoVersao = function(params){
    var data = app.params2data({},params );
    // alert('mostrar a fundamentação da validação da ficha versionada');

    var fullscreen=false
    var windowWidth = Math.min(800,$(window).width() - 50);
    //var windowHeight = $(window).height() - 130;
    var windowHeight = Math.max(500,window.innerHeight-130);
    var panel = app.panel('pnlFundamentacaoVersao', ( params.header ? params.header : 'Fundamentação' ), 'fichaVersao/fundamentacaoVersao', data, function() {
            // onOpen
        }
        ,windowWidth
        ,windowHeight
        ,true,fullscreen,null,'cc',"#EFEFEF",false,false);
}

var loadPaginateArrayPage = function( ele ) {
    var $ele=$(ele);
    var $divPagination = $ele.parent();
    var data = $.extend({},$ele.data(), $divPagination.data());
    if( ele.tagName.toUpperCase()=='SELECT'){
        data.gridPage = ele.value;
    }
    data.gridPage     = parseInt( data.gridPage );
    data.gridPageSize = parseInt( data.gridPageSize);
    data.gridRowNum   = data.gridPageSize * ( data.gridPage - 1 );
    var url = app.url+data.gridController+'/'+data.gridAction
    var $container = $("#"+data.gridContainerId);
    var $btnPrev = $ele.parent().find('button.btn-pagination-prev');
    var $btnNext = $ele.parent().find('button.btn-pagination-next');
    var $select  = $ele.parent().find('select.sel-pagination-pages');
    var totalPages = $select.find('option').size();
    var nextPage =  Math.min(totalPages, (data.gridPage + 1)  );
    var prevPage = Math.max(1, ( data.gridPage - 1) );
    var currentPage = data.gridPage;
    $btnPrev.data('gridPage',prevPage );
    $btnNext.data('gridPage',nextPage);
    $select.data('gridPage', currentPage );
    $select.val( String( currentPage ) );
    $container.html( 'Carregando...');
    $.ajax({
          url: url
        , type: 'POST'
        , cache: false
        , timeout: 30000
        , data:data
        , dataType: 'html'
    }).done(function( res ){
        var html = $(res).find("table").parent().html();
        if( html ) {
            $container.html(html);
            $container.shiftSelectable();
        } else  {
            $container.html(res);
        }
        if( data.gridOnPageChange ){
            app.eval( data.gridOnPageChange, data,false );
        }
    });
}
