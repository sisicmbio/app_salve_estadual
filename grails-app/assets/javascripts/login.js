//# sourceURL=login.js
var login = {
    init:function(){
        var storedEmail = localStorage.getItem('login_email') ||''
        $("#inputEmail").val(storedEmail);
        if( storedEmail ){
            if( $("#inputPassword").size() == 1 ) {
                $("#inputPassword").focus();
            } else if( $("#inputPassword1").size() == 1 ) {
                $("#inputPassword1").focus();
            }
        } else {
            $("#inputEmail").focus();
        }
    },
    ajax:function( url, data, callback ) {
        $.ajax({
            url:baseUrl+'login/'+url,
            data:data,
            dataType:'json'
        }).done(function(res) {
            callback( res );
        }).error( function() {
            app.growl('Requisição ajax deu erro',null,null,'tc',null,'error')
        });
    },
    voltar:function(){
        //window.location.href = baseUrl;
        app.logout();
    },
    login:function(){
        var email = $("#inputEmail").val();
        var senha = $("#inputPassword").val();
        if( !email || !senha ){
            app.growl('Preencha todos os campos.',null,null,'tc',null,'error')
            $("#inputEmail").focus();
            return;
        }
        localStorage.setItem('login_email', email );
        var $form = $("#frmLoginBody")
        $form.find('input,button').prop('disabled',true);
        $form.find('i.fa-user-circle').addClass('hidden');
        $form.find('i.fa-spinner').removeClass('hidden');
        login.ajax( 'login',{email:email,senha:senha},function( res ) {
            $form.find('input,button').prop('disabled',false);
            $form.find('i.fa-user-circle').removeClass('hidden');
            $form.find('i.fa-spinner').addClass('hidden');
            if( res.msg ){
                app.growl(res.msg,null,null,'tc',null,'error')
            }
            if( res.status == 0 ) {
                window.location.href = baseUrl;
            }
        });
    },
    showFormForgotPassword:function(){
        var currentEmail = $("#inputEmail").val();
        localStorage.setItem('login_email', currentEmail );
        document.location.replace(baseUrl+'login/forgotPasswordForm');
    }
    ,sendResetPasswordMail:function(){
        var email = $("#inputEmail").val();
        if( !email ){
            app.growl('Email não informado.',null,null,'tc',null,'error')
            $("#inputEmail").focus();
            return;
        }
        login.ajax('sendResetPasswordMail', {email:email}, function( res ) {
            if( res.msg ){
                if( res.status==0) {
                    app.alertInfo(res.msg, 'Mensagem', function () {
                        window.location.href = baseUrl;
                    });
                } else {
                    app.growl(res.msg,null,null,'tc',null,'error')
                }
            }
        });
    }
    ,selectPerfil:function(){
        $("#btnEntrar").prop('disabled',true);
        $("#btnEntrar").addClass('disabled');
        var sqUsuarioPerfil = $("#sqUsuarioPerfil").val();
        if( !sqUsuarioPerfil ){
            return;
        }
        login.ajax('selectPerfil', {sqUsuarioPerfil:sqUsuarioPerfil}, function( res ) {
            if( res.msg ) {
                app.growl(res.msg);
            }
            if( res.status == 0 ) {
                // preencher a lista de instiucoes que o usuario possui acesso
                var $select = $("#sqUsuarioPerfilInstituicao");
                $select[0].options=[];
                $select.append( $('<option />').val('').text('-- selecione --') );
                if( res.data.instituicoes.length > 0 ){
                    res.data.instituicoes.map( function(item,i){
                        var id = item.sqUsuarioPerfilInstituicao;
                        var text = item.sgInstituicao ? item.sgInstituicao : item.noInstituicao;
                        var selected = ( i==0 && res.data.instituicoes.length == 1 ? 'selected="true"':'' );
                        $select.append( $('<option '+selected+'/>').val(id).text(text) );
                    });
                    $select.parent().show();
                    $("#btnEntrar").prop('disabled',false);
                    $("#btnEntrar").removeClass('disabled');
                } else {
                    $select.parent().hide();
                    $("#btnEntrar").prop('disabled',false);
                    $("#btnEntrar").removeClass('disabled');
                }
            }
        });
    },
    loginPerfil:function(){
        var sqUsuarioPerfil = $("#sqUsuarioPerfil").val();
        var sqUsuarioPerfilInstituicao = $("#sqUsuarioPerfilInstituicao").val();
        if( !sqUsuarioPerfil && !sqUsuarioPerfilInstituicao ){
            return;
        }
        login.ajax('selectPerfilInstituicao', {
            sqUsuarioPerfil:sqUsuarioPerfil,
            sqUsuarioPerfilInstituicao:sqUsuarioPerfilInstituicao
        }, function( res ) {
            if( res.msg ) {
                app.growl(res.msg);
            }
            if( res.status == 0 ) {
                window.location.href = baseUrl;
            }
        });
    },
    changePassword:function(){
        var senha1 = $("#inputPassword1").val();
        var senha2 = $("#inputPassword2").val();
        var hash = $("#inputHash").val();
        if( !senha1 || !senha2 || !hash ){
            app.growl('Preencha todos os campos',null,null,'tc',null,'error');
            $("#inputPassword1").focus();
            return;
        }
        if( senha1 != senha2 ){
            app.growl('Senhas digitadas estão diferentes',null,null,'tc',null,'error');
            $("#inputPassword1").focus();
            return;
        }

        login.ajax( 'changePassword',{senha1:senha1,senha2:senha2,hash:hash},function( res ) {
            if( res.status == 1 ) {
                app.growl(res.msg, null, null, 'tc', null, 'error')
            } else {
                app.growl(res.msg, 2, null, 'tc', null, 'success');
                setTimeout(function(){
                    window.location.href = baseUrl;
                },2000);
                // app.alertInfo(res.msg,'Mensagem',function(){
                //     window.location.href = baseUrl;
                // })
            }
        });
    },
    enterKey:function() {
        if( event ) {
            if (event.code === 'Enter' || event.key === 'Enter' || event.keyCode === 13) {
                login.login();
            }
        }
    }
}
setTimeout(function(){login.init();},1000);
