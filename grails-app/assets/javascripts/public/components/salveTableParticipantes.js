Vue.component("salveTableParticipantes",{
    props:['id','listData','unidadeHeader'],
    template:"#templateTableParticipantes",
    data:function(){
        return {
            idTemp : Math.random().toString(36).substring(7),
            sortData:{column:'no_pessoa',order:'asc'},
        }
    },
    methods: {    },
    watch: {
        'listData':function() {
            this.$nextTick( function(){
                applyDefaultDataTable( ( this.id ? this.id  : this.idTemp ) );
            });
        }
    }
});
