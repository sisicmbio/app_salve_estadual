Vue.component("salveTableSearch",{
    props:['id','pars'],
    template:"#templateTableSearchResult", // views/public/index.gsp
    data:function(){
        return {
            maxRowsExport:1000, // quantidade maxima de linha para exportacao das fichas
            maxRowsOccurrenceExport:100, // quantidade maxima de linhas para exportação dos registros de ocorrências das fichas
            idTemp : Math.random().toString(36).substring(7),
            scrollTopBeforeSearch  :0, // posição da tela antes de realizar a consulta
            scrollTopTableBeforeSearch  :0, // posição da tabela antes de realizar a consulta
            timeoutMinutes :1,
            exportingPlanilha:false,
            exportingOcorrencias:false,
            exportingFichasZip:false,
            searchParams : {},
            searchResults   : [],
            header          : '',
            title           : '',
            sortData:{column:'nm_cientifico',order:'asc'},
            pagination:{
                pageSize: 20
                ,page: 1
                ,pages: 1
                ,records:0
                ,params:{}
            },
            selectedRows:[], // armazenar os ids das fichas selecionadas
            selectedAllRows:false // quando clicar para selecionar todas as linhas da tabela
        }
    },
    methods: {
        stripTags : function(texto) {
            return $(texto).text()
        },
        canExportRows: function (limitMax) {
            if (!limitMax) {
                limitMax = this.maxRowsOccurrenceExport;
            }
            if (this.totalRowsSelected > 0 && this.totalRowsSelected <= limitMax) {
                return true;
            }
            if (this.totalRowsSelected > limitMax || this.pagination.records > limitMax) {
                swal2('Exportação máxima de ' + limitMax + ' fichas por vez', '', 'info');
                return false;
            }
            return true;
        },
        doClearResult: function () {
            var tableId = '#' + (this.id ? this.id : this.idTemp);
            this.searchResults = [];
            $(tableId).floatThead('destroy');
            $('body').scrollTop(this.scrollTopBeforeSearch);
        },
        doSort: function (evt) {
            var $ele = $(evt.target);
            $ele.closest('tr').find('i[data-sort-column]').removeClass('sort-active').addClass('sort-inactive')
            var data = $ele.data();
            var icons = {asc: 'fa-sort-alpha-asc', desc: 'fa-sort-alpha-desc'};
            if (data.sortOrder) {
                $ele.removeClass(icons[data.sortOrder]);
                $ele.removeClass('sort-inactive');
                $ele.addClass('sort-active');
                if (data.sortOrder == 'asc') {
                    data.sortOrder = 'desc'
                } else {
                    data.sortOrder = 'asc'
                }
                $ele.addClass(icons[data.sortOrder]);
            }
            this.sortData = {column: data.sortColumn, order: data.sortOrder};
            this.doSearch(evt, this.pagination.page);
        },
        doSelectAllRows: function () {
            if (this.pagination.records < 1) {
                return;
            }
            this.selectedAllRows = !this.selectedAllRows;
            if (!this.selectedAllRows) {
                this.selectedRows = [];
            }
        },
        doSelectTableRow: function (evt, item) {
            var sqFicha = item.sq_ficha
            var index = this.selectedRows.indexOf(sqFicha);
            var ele = evt.target;
            if (ele.checked) {
                if (index == -1) {
                    this.selectedRows.push(sqFicha);
                }
            } else {
                if (index > -1) {
                    this.selectedRows.splice(index, 1)
                }
            }
            this.selectedAllRows = (this.selectedRows.length == this.pagination.records);
        },
        /**
         * função para exibir a ficha completa no formato HTML em uma
         * janela popup
         * @param item
         */
        doFichaHtml: function (item) {
            showFichaHtml(item);
        },
        doAjaxSearch: function (params, timeoutMinutes) {
            // guardar a posição do scroll vertical para quando fichar a tabela voltar a tela
            this.scrollTopBeforeSearch = $("body").scrollTop();
            var url = urlSalve + 'publico/search';
            /*if( typeof(ga) == 'function') {
                ga('set', 'page', '/salve-estadual/publico');
                ga('send', 'pageview');
            }*/
            // adicionar os parametros das rows selecionadas ou todos os registros
            params.all = false;
            if (params.outputFormat != 'table') {
                if (!params.selectedIds) {
                    params.all = this.selectedAllRows;
                    if (!params.all) {
                        // se passou o id é porque clicou nos botões da coluna Ações da tabela
                        if (this.selectedRows.length > 0) {
                            params.selectedIds = this.selectedRows.join(',');
                        }
                    }
                }
            }
            // adicionar sortData para ordenar os dados de acordo com a coluna da tabela
            params.sortColumn = this.sortData.column;
            params.sortOrder = this.sortData.order;
            return $.ajax({
                url: url
                , data: params
                , dataType: 'json'
                , cache: true
                , timeout: (1000 * 60 * timeoutMinutes)
                , statusCode: {
                    404: function () {
                        swal2("Url de pesquisa não encotnrada.", url, 'error');
                    }
                }
            }).done(function (res) {
                // limpar parametros all e selectIds
                params.all = false;
                params.selectedIds = '';
                if (res.status == 1) {
                    if (res.msg) {
                        swal2(res.msg, (res.text ? res.text : ''), (res.type ? res.type : 'error'));
                    }
                } else {
                    if (res.msg) {
                        swal2(res.msg, (res.text ? res.text : ''), (res.type ? res.type : 'success'));
                    }
                }
            }).fail(function (jqXHR, textStatus, errorThrown) {
                if (textStatus == 'timeout') {
                    swal2('Tempo limite excedido.', 'Tente novamente.', 'error')
                }
            }).always(function () {
                unblockUI();
            });
        },

        /**
         * executar a pesquisa sempre que a propriedade pars for alterada
         * @param e
         * @param page
         */
        doSearch: function (e, page) {
            var vm = this;
            var params = this.searchParams;
            if (params.title) {
                vm.title = params.title;
                delete params.title;
            }
            if (params.header) {
                vm.header = params.header;
                delete params.header;
            }
            if (!params) {
                swal2("Nenhum valor informado para pesquisar!", "", "info");
                return;
            }

            // quando informar a pagina, ler os parametros da paginação
            if (page && this.pagination.params) {
                params = this.pagination.params;
                params.page = vm.pagination.page;
                params.records = vm.pagination.records;
            } else {
                params.page = 1;
                params.records = 0;
            }
            params.outputFormat = 'table';
            params.pageSize = vm.pagination.pageSize;
            var tableId = '#' + (vm.id ? vm.id : vm.idTemp);
            var wrapper = tableId + 'Wrapper';
            $(tableId).floatThead('destroy');
            blockUI('Aguarde...', function () {
                grecaptcha.ready(function () {
                    grecaptcha.execute(window.reCaptchaPublicKey, {action: 'submit'}).then(function (token) {
                        params.reCaptchaToken = token;
                        // inicio recaptcha
                        vm.doAjaxSearch(params, 2).then(function (res) {
                            // https://mkoryak.github.io/floatThead
                            //$( tableId ).floatThead('destroy');
                            // limpar checkboxes da linhas selecionadas
                            //vm.selectedRows=[];
                            //vm.selectedAllRows=false;
                            vm.searchResults = res.data;
                            vm.pagination = res.pagination;
                            vm.pagination.params = params;
                            params.records = res.pagination.records;
                            if (vm.pagination.records > 0) {
                                vm.$nextTick(function () {
                                    $(tableId).floatThead({
                                        scrollContainer: function ($table) {
                                            return $(wrapper);
                                            //return $table.closest('.table-search-result-wrapper');
                                        }
                                    });
                                    // posicionar a tabela no topo da tela
                                    //document.location.assign("#tableSearchResultMain")
                                    //console.log( tableId);
                                    setTimeout(function () {
                                        if ($(tableId).offset().top > 110) {
                                            //$( tableId )[0].scrollIntoView({behavior: 'smooth',block: "center"});
                                            $(tableId)[0].scrollIntoView();
                                            $('body').scrollTop($('body').scrollTop() - 140);
                                        }
                                        // posicionar na posição de rolagem que estava antes
                                        if (vm.scrollTopTableBeforeSearch > 0) {
                                            $(vm.tableDomId).scrollTop(vm.scrollTopTableBeforeSearch);
                                            vm.scrollTopTableBeforeSearch = 0;
                                        }
                                    }, 100);
                                });
                            }
                            ;
                            vm.$emit('search-end', params)
                        }).always(function () {
                            unblockUI();
                        });
                        // fim recaptcha
                    });
                });


            });
        },

        /**
         * função para exportar o resultado da pesquisa ( gride ) para planilha
         */
        doExportSearchResult: function () {
            var vm = this;
            // validar quantidade maxima de registros
            if (!this.canExportRows(this.maxRowsExport)) {
                return;
            }
            doConfirm('Confirma exportação?', 'Exportar resultado da pesquisa para planilha.', true)
                .then(function (result) {
                    if (result.dismiss) {
                        return;
                    }
                    var params = vm.searchParams;
                    params.outputFormat = 'xlsx';
                    params.email = (result.value.email ? result.value.email.trim().toLowerCase() : '');
                    if (params.email != '') {
                        localStorage.setItem('userEmail', params.email)
                    }
                    vm.exportingPlanilha = true;
                    notify('Exportação iniciada...', 2000, 'info');
                    grecaptcha.ready(function () {
                        grecaptcha.execute(window.reCaptchaPublicKey, {action: 'submit'}).then(function (token) {
                            params.reCaptchaToken = token;
                            vm.doAjaxSearch(params, 60).then(function (res) {
                                vm.exportingPlanilha = false;
                                if (res.data.fileName) {
                                    doOpenWindow(res.data.fileName);
                                    notify('Exportaçao finalizada.', 3000)
                                }
                            });
                        });
                    });
                });
        },
        doDownloadRegistros: function (item) {
            var vm = this;
            // validar quantidade maxima de fichas para exportar os registros de ocorrencias
            if (!this.canExportRows(this.maxRowsOccurrenceExport)) {
                return;
            }
            if (vm.exportingOcorrencias) {
                swal2('Já existe uma solicitação de exportação em andamento.', 'Aguarde o término.', 'info');
                return;
            }
            //doConfirm("Confirma exportação?", "<span class='text-danger'>(os registros <span class='text-help' title='Registro em carência são aqueles em que o autor define\numa data mínima para que ele se torne público.'>em carência</span> NÃO serão exportados)</span>", true)
            doConfirm("Confirma exportação?", "<span class='text-danger'>(apenas os registros públicos serão exportados)</span>", true)
                .then(function (result) {
                    if (result.dismiss) {
                        return;
                    }
                    var params = vm.searchParams;
                    params.outputFormat = 'xlsx';
                    params.email = (result.value.email ? result.value.email.trim().toLowerCase() : '');
                    if (params.email != '') {
                        localStorage.setItem('userEmail', params.email)
                    }
                    params.outputFormat = 'ocorrencias';
                    params.selectedIds = ''
                    // se nao passar o item, fazer o downlad de todos os registros de todas as fichas pesquisadas
                    if (item) {
                        params.selectedIds = [item.sq_ficha].join(',');
                    }
                    vm.exportingOcorrencias = true;
                    notify('Exportação iniciada...', 2000, 'info');
                    grecaptcha.ready(function () {
                        grecaptcha.execute(window.reCaptchaPublicKey, {action: 'submit'}).then(function (token) {
                            params.reCaptchaToken = token;
                            vm.doAjaxSearch(params, 60).then(function (res) {
                                vm.exportingOcorrencias = false;
                                notify('Exportação finalizada.', 3000);
                                if (res.data.fileName) {
                                    doOpenWindow(res.data.fileName);
                                }
                            });
                        });
                    });
                });
        },
        doDownloadFichasZip: function () {
            var vm = this;
            // validar quantidade maxima de fichas para exportar
            if (!this.canExportRows(this.maxRowsExport)) {
                return;
            }
            doConfirm('Confirma ?', 'Exportação da(s) ficha(s) no formato PDF em um arquivo zip.', true)
                .then(function (result) {
                    if (result.dismiss) {
                        return;
                    }
                    var params = vm.searchParams;
                    params.outputFormat = 'zip';
                    params.email = (result.value.email ? result.value.email.trim().toLowerCase() : '');
                    if (params.email != '') {
                        localStorage.setItem('userEmail', params.email)
                    }
                    vm.exportingFichasZip = true;
                    notify('Criação zip iniciada...', 3000, 'info');
                    grecaptcha.ready(function () {
                        grecaptcha.execute(window.reCaptchaPublicKey, {action: 'submit'}).then(function (token) {
                            params.reCaptchaToken = token;
                            vm.doAjaxSearch(params, 60).then(function (res) {
                                vm.exportingFichasZip = false;
                                if (res.data.fileName) {
                                    doOpenWindow(res.data.fileName);
                                    notify('Arquivo zip criado.', 3000)
                                }
                            });
                        });
                    });
                });
        },
        onPageChange: function (page) {
            // guardar a posição do scroll da tabela
            this.scrollTopTableBeforeSearch = 99999999;//$(this.tableDomId).scrollTop();
            this.doSearch(null, page);
        },
        isRowSelected: function (item) {
            return (this.selectedAllRows || this.selectedRows.indexOf(item.sq_ficha) > -1)
        },
        doShowJustificativaExclusao: function (item) {
            var params = {sqFicha: item.sq_ficha};
            var url = urlSalve + 'publico/getJustificativaExclusao';
            return $.ajax({
                url: url
                , data: params
                , dataType: 'json'
                , cache: true
                , timeout: (1000 * 60 * 1)
                , statusCode: {
                    404: function () {
                        swal2("Url de pesquisa não encotnrada.", url, 'error');
                    }
                }
            }).done(function (res) {
                // limpar parametros all e selectIds
                params.all = false;
                params.selectedIds = '';
                if (res.status == 1) {
                    if (res.msg) {
                        swal2(res.msg, (res.text ? res.text : ''), (res.type ? res.type : 'error'));
                    }
                } else {
                    if (res.msg) {
                        swal2(res.msg, (res.text ? res.text : ''), (res.type ? res.type : 'success'));
                    } else {
                        if( res.data.dsJustificativa ){
                            swal2('Justificativa', '<div style="border:1px solid silver;max-height: 400px;overflow-y: auto;text-align: justify">'+res.data.dsJustificativa+'</div>');
                        }
                    }

                }
            }).fail(function (jqXHR, textStatus, errorThrown) {
                if (textStatus == 'timeout') {
                    swal2('Tempo limite excedido.', 'Tente novamente.', 'error')
                }
            }).always(function () {
                unblockUI();
            });

        },
    },
    computed: {
        totalRegistros : function() {
            var selectedRows =0;
            if( this.selectedAllRows ) {
                selectedRows  = this.pagination.records;
            } else {
                if( this.selectedRows.length > 0 ) {
                    selectedRows = this.selectedRows.length
                }
            }
            return this.$options.filters.formatInt( this.pagination.records) +' registro' + ( this.pagination.records > 1 ? 's' :'') +
                ( ( selectedRows > 0 ) ? ' / ' +
                    this.$options.filters.formatInt( selectedRows ) + ' selecionado' +
                    ( selectedRows > 1 ? 's' :'' ) : '' )
        },
        totalRowsSelected:function(){
            if( this.selectedAllRows ) {
                return this.pagination.records;
            }
            return this.selectedRows.length;
        },
        tableDomId:function() {
            return "#"+(this.id ? this.id + 'Wrapper' : 'tableSearchResultWrapper'+this.idTemp );
        }

    }, watch: {
        'pars':function() {
            this.searchParams={};
            this.searchResults=[];
            this.selectedAllRows=false;
            this.selectedRows=[]
            if( ! _.isEmpty( this.pars ) ) {
                if( this.pars.params ) {
                    this.searchParams = this.pars.params;
                } else {
                    this.searchParams = this.pars;
                }
                if( this.pars.criterias ) {
                    this.searchParams.criterias =  JSON.stringify(this.pars.criterias);
                }
                this.doSearch()
            } else {
                var tableId = '#' + (this.id ? this.id : this.idTemp);
                $( tableId ).floatThead('destroy');
                this.searchResults=[];
            }
        }
    }
});
