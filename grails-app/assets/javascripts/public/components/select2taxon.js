Vue.component("select2taxon", {
    props: ["value", "params"],
    template: " <select>" +
        "<slot></slot>" +
        "</select>",
    mounted: function() {
        var vm = this;
        $(this.$el)
            // init select2
            .select2({
                //theme: "classic",
                minimumInputLength:3,
                allowClear:true,
                language: 'pt-BR',
                cache:true,
                autoHeight: true,
                placeholder:'pesquisar...',
                ajax: {
                    url: '/salve-estadual/publico/searchTaxon',
                    dataType: 'json',
                    data: function ( p ) {
                        return {'noTaxonSearch' : p.term
                            ,'sqNivelTaxonomico': vm.params.sqNivelTaxonomico
                            ,'noNivelTaxonomico': vm.params.noNivelTaxonomico
                            ,'searchType'       : vm.params.searchType
                        }
                    },
                    processResults: function (res) {
                        return {
                            results: res.data
                        };
                    }
                }
            })
            .val(this.value)
            .trigger("change")
            // emit event on change.
            .on("change", function() {
                vm.$emit("input", this.value);
            });
    },
    watch: {
        value: function(value) {
            // update value
            $(this.$el)
                .val(value)
                .trigger("change");
        },
        /*options: function(options) {
            // update options
            $(this.$el)
                .empty()
                .select2({ data: options });
        }*/
    },
    destroyed: function() {
        $(this.$el)
            .off()
            .select2("destroy");
    }
});
