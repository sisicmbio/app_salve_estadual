//# sourceURL=select2multi.js
Vue.component("select2multi", {
    props: ["value", "params"],
    template: " <select>" +
        "<slot></slot>" +
        "</select>",
    mounted: function() {
        var vm = this;
        $(this.$el)
            .select2({
                multiple:true,
                closeOnSelect:false,
                allowClear:true,
                language: 'pt-BR',
                cache:true,
                autoHeight: true,
                placeholder:'selecione...(pressione a tela CTRL para multi seleção)',
            })
            .val( this.value )
            .trigger("change")
            .on("change", function() {
                vm.$emit("input", $(this).val());
            });
    },
    destroyed: function() {
        $(this.$el)
            .off()
            .select2("destroy");
    }
});
