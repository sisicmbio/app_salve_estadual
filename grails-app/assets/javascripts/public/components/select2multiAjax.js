//# sourceURL=select2multiAjax.js
Vue.component("select2multiajax", {
    props: ["value", "tipoUc", "url"],
    template: " <select>" +
        "<slot></slot>" +
        "</select>",
    mounted: function() {
        var vm = this;
        this.$nextTick(function() {
            console.log('Select2 criado')
            $(vm.$el).select2({
                    multiple:true,
                    allowClear:true,
                    language: 'pt-BR',
                    closeOnSelect:false,
                    cache:true,
                    autoHeight:true,
                    //placeholder:'Selecione...',
                    //placeholder:'Mínimo 3 letras...(pressione a tela CTRL para multi seleção)',
                    minimumInputLength:3,
                    ajax: {
                        url: urlSalve+vm.url,
                        dataType: 'json',
                        delay: 1000, //1s
                        data: function( params ) {
                            var query = {q:params.term, tipoUc:vm.tipoUc};
                            return query;
                        },
                        processResults: function (res) {
                            return {
                                results: res.data //data.items
                            };
                        },
                    },
                    templateResult: function(state) {
                        if( state ) {
                            if (!state.id && state.loading) {
                                return $('<span>' + state.text + '</span><i class="fa fa-spinner fa-spin"></i>');
                                //return state.text+' <i class="fa fa-spinner fa-spin"></i>'
                            }
                            if (state.hint) {
                                var $state = $('<span title="' + state.hint.replace(/\|/g, '\n') + '">' + state.text + '</span>')
                                return $state;
                            }
                            return state.text
                        }
                    },
                })
                .val( vm.value )
                .trigger("change")
                .on("change", function(e) {
                    vm.$emit("input", $(this).val() );
                });
        });
    },
    watch: {
        'tipoUc' : function() {
            $(this.$el).val(null).trigger('change');
        }
    },
    destroyed: function() {
        $(this.$el)
            .off()
            .select2("destroy");
    }
});
