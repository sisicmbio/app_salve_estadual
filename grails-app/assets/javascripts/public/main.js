//# sourceURL=/public/main.js
var appVue=null;
var urlSalve = ( location.protocol ? location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '') : '' ) + '/salve-estadual/';
var scroll=null;
var map=null

// componentes VUEJS
$(document).ready( function() {
    // filtro para formatar numeros
    Vue.filter('formatInt', function (value) {
        if( ! value ) {
            return value;
        }
        return parseInt('0'+value).toLocaleString(
            undefined, // leave undefined to use the browser's locale,
            // or use a string like 'en-US' to override it.
            { minimumFractionDigits: 0 }
        );
    });

    Vue.filter('stripTags', function (value) {
        if( ! value ) {
            return value;
        }
        return $('<div>'+value+'</div>').text();
    });

    appVue = new Vue({
        name:'salve-publico',
        el:'#appVue' ,
        data:function() {
            return {
                layout:1,
                searchValue:'',
                advancedSearchVisible:false,
                searchByPublicadas:false,
                searchByGrupoTaxonomico:false,
                searchByCategoria:false,
                searchByBioma:false,
                searchByEstado:false,
                searchByBioma:false,
                searchByRegiao:false,
                searchByGrupo:false,
                searchByUc:false,
                searchByAmeaca:false,
                searchByUso:false,
                searchByBacia:false,
                searchByPan:false,
                userEmail : '',
                faleConosco: {
                    nome    :'',
                    email   :'',
                    mensagem:'',
                },
                advancedSearch:{
                    sqTaxonSearch       :'',
                    sqNivelTaxonomico   :'',
                    noNivelTaxonomico   :'Taxon',
                    searchType          :'startWith', // hasWord
                    sqCategoria         :[],
                    sqEstado            :[],
                    endemicaEstado      :false,
                    sqBioma            :[],
                    endemicaBioma      :false,
                    sqRegiao           :[],
                    endemicaRegiao     :false,
                    sqGrupo            :[],
                    sqUc               :[],
                    tipoUc             :'federal',
                    sqAmeaca           :[],
                    sqUso              :[],
                    sqBacia            :[],
                    sqPan              :[]
                },
                clickGraphMessageVisible : true,
                searchResults:[],
                searchResultsTaxon:[],
                searchingTaxon:false,
                exportingPlanilha:false,
                exportingOcorrencias:false,
                exportingFichasZip:false,
                pagination:{
                     pageSize: 20
                    ,page: 1
                    ,pages: 1
                    ,records:0
                    ,params:{}
                },
                graphAmeacasParams:{},
                graphSalveNumerosParams:{},
                searchParams:{}, // filtros utilizados para pesquisar
                searchParamsCriterias:{}, // filtros no formato textual para serem exibidos na planilha exportada
                listLivroVermelho:[
                    { 'img':'livro_vermelho_vol1.jpg','volume':'Volume I','grupo':'','url':'https://www.icmbio.gov.br/portal/images/stories/comunicacao/publicacoes/publicacoes-diversas/livro_vermelho_2018_vol1.pdf'},
                    { 'img':'livro_vermelho_vol2.jpg','volume':'Volume II','grupo':'Mamíferos','url':'https://www.icmbio.gov.br/portal/images/stories/comunicacao/publicacoes/publicacoes-diversas/livro_vermelho_2018_vol2.pdf'},
                    { 'img':'livro_vermelho_vol3.jpg','volume':'Volume III','grupo':'Aves','url':'https://www.icmbio.gov.br/portal/images/stories/comunicacao/publicacoes/publicacoes-diversas/livro_vermelho_2018_vol3.pdf'},
                    { 'img':'livro_vermelho_vol4.jpg','volume':'Volume IV','grupo':'Répteis','url':'https://www.icmbio.gov.br/portal/images/stories/comunicacao/publicacoes/publicacoes-diversas/livro_vermelho_2018_vol4.pdf'},
                    { 'img':'livro_vermelho_vol5.jpg','volume':'Volume V','grupo':'Anfíbios','url':'https://www.icmbio.gov.br/portal/images/stories/comunicacao/publicacoes/publicacoes-diversas/livro_vermelho_2018_vol5.pdf'},
                    { 'img':'livro_vermelho_vol6.jpg','volume':'Volume VI','grupo':'Peixes','url':'https://www.icmbio.gov.br/portal/images/stories/comunicacao/publicacoes/publicacoes-diversas/livro_vermelho_2018_vol6.pdf'},
                    { 'img':'livro_vermelho_vol7.jpg','volume':'Volume VII','grupo':'Invertebrados','url':'https://www.icmbio.gov.br/portal/images/stories/comunicacao/publicacoes/publicacoes-diversas/livro_vermelho_2018_vol7.pdf'},
                ],
                listImagensEspeciesDestaque:[],
                listCentros:[
                    { 'id':1,'sigla':'CBC'     , 'imgUrl':'/salve-estadual/assets/public/centros/cbc.png'   , 'site':'https://www1.icmbio.gov.br/cbc/', 'nome':'Centro Nacional de Avaliação da Biodiversidade e de Pesquisa e Conservação do Cerrado' },
                    { 'id':2,'sigla':'CEMAVE'  , 'imgUrl':'/salve-estadual/assets/public/centros/cemave.png', 'site':'https://www1.icmbio.gov.br/cemave','nome':'Centro Nacional de Pesquisa e Conservação de Aves Silvestres' },
                    { 'id':3,'sigla':'CEPSUL'  , 'imgUrl':'/salve-estadual/assets/public/centros/cepsul.png', 'site':'https://www1.icmbio.gov.br/cepsul','nome':'Centro Nacional de Pesquisa e Conservação da Biodiversidade Marinha do Sudeste e Sul'},
                    { 'id':4,'sigla':'CECAV'  , 'imgUrl':'/salve-estadual/assets/public/centros/cecav.png', 'site':'https://www.icmbio.gov.br/cecav/','nome':'Centro Nacional de Pesquisa e Conservação de Cavernas'},
                    { 'id':5,'sigla':'CENAP'  , 'imgUrl':'/salve-estadual/assets/public/centros/cenap.png', 'site':'https://www1.icmbio.gov.br/cenap/','nome':'Centro Nacional de Pesquisa e Conservação de Mamíferos Carnívoros'},
                    { 'id':6,'sigla':'CEPAM'  , 'imgUrl':'/salve-estadual/assets/public/centros/cepam.png', 'site':'https://www.icmbio.gov.br/portal/centrosdepesquisa/biodiversidade-amazonica','nome':'Centro Nacional de Pesquisa e Conservação da Biodiversidade Amazônica'},
                    { 'id':7,'sigla':'CEPENE'  , 'imgUrl':'/salve-estadual/assets/public/centros/cepene.png', 'site':'https://www.icmbio.gov.br/cepene/','nome':'Centro de Pesquisa e Conservação da Biodiversidade Marinha do Nordeste'},
                    { 'id':8,'sigla':'CEPNOR'  , 'imgUrl':'/salve-estadual/assets/public/centros/cepnor.png', 'site':'https://www.icmbio.gov.br/cepnor/','nome':'Centro Nacional de Pesquisa e Conservação da Biodiversidade Marinha do Norte'},
                    { 'id':9,'sigla':'CEPTA'  , 'imgUrl':'/salve-estadual/assets/public/centros/cepta.png', 'site':'https://www.icmbio.gov.br/cepta/','nome':'Centro Nacional de Pesquisa e Conservação da Biodiversidade Aquática Continental'},
                    { 'id':10,'sigla':'CMA'  , 'imgUrl':'/salve-estadual/assets/public/centros/cma.png', 'site':'https://www.icmbio.gov.br/cma/','nome':'Centro Nacional de Pesquisa e Conservação de Mamíferos Aquáticos'},
                    { 'id':11,'sigla':'CPB'  , 'imgUrl':'/salve-estadual/assets/public/centros/cpb.png', 'site':'https://www.icmbio.gov.br/cpb/','nome':'Centro Nacional de Pesquisa e Conservação de Primatas Brasileiros'},
                    { 'id':12,'sigla':'RAN'  , 'imgUrl':'/salve-estadual/assets/public/centros/ran.png', 'site':'https://www.icmbio.gov.br/ran/','nome':'Centro Nacional de Pesquisa e Conservação de Répteis e Anfíbios'},
                    { 'id':13,'sigla':'TAMAR'  , 'imgUrl':'/salve-estadual/assets/public/centros/tamar.png', 'site':'https://www1.icmbio.gov.br/centrotamar/','nome':'Centro Nacional de Pesquisa e Conservação de Tartarugas Marinhas e da Biodiversidade Marinha do Leste'},
                ],
                listQuemSomos:[
                    {
                        foto:'https://www.icmbio.gov.br/cbc/images/stories/thumbnails/images/stories/quem_somos/foto%20Rodrigo%202.jpg',
                        nome:'Rodrigo Silva Pinto Jorge',
                        cargo:'Analista Ambiental<br><span class="text-warning">Coordenador</span>',
                        lattes:'http://lattes.cnpq.br/1423025419733008',
                        email : 'rodrigo.jorge@icmbio.gov.br',
                        ordem : 'A'
                    },
                    {
                        foto:'https://www.icmbio.gov.br/cbc/images/stories/thumbnails/images/stories/quem_somos/foto%20Rodrigo%202.jpg',
                        nome:'Arthur Jorge Brant Caldas Pereira',
                        cargo:'Analista Ambiental',
                        lattes:'http://lattes.cnpq.br/2236753392586566',
                        email : 'arthur.pereira@icmbio.gov.br',
                        ordem : 'B'
                    },
                    {
                        foto:'',
                        nome:'Vera Nanci de Oliveira Carvalho',
                        cargo:'Analista Ambiental',
                        lattes:'',
                        email : 'vera.carvalho@icmbio.gov.br',
                        ordem : 'B'
                    },
                    /*{
                        foto:'https://www.icmbio.gov.br/cbc/images/stories/thumbnails/images/stories/quem_somos/alexandre-104x105.jpg',
                        nome:'Alexandre Bonesso Sampaio',
                        cargo:'Analista ambiental - <span class="text-warning">Coordenador substituto</span>',
                        lattes:'http://lattes.cnpq.br/4537957578176910',
                        email : 'alexandre.sampaio@icmbio.gov.br',
                        ordem : 'A2'
                    },
                    {
                        foto  : 'https://www.icmbio.gov.br/cbc/images/stories/thumbnails/images/stories/quem_somos/Foto%20Tainah%204.jpg',
                        nome  : 'Tainah Corrêa Seabra Guimarães',
                        cargo : 'Analista ambiental',
                        lattes: 'http://lattes.cnpq.br/9495734767727999',
                        email : 'tainah.guimaraes@icmbio.gov.br',
                        ordem : 'B'
                    },
                    {
                        foto  : 'https://www.icmbio.gov.br/cbc/images/stories/thumbnails/images/stories/quem_somos/onildo-102x141.jpg',
                        nome  : 'Onildo João Marini Filho',
                        cargo : 'Analista ambiental',
                        lattes: 'https://www.icmbio.gov.br/cbc/%20http:/lattes.cnpq.br/5094260240232256',
                        email : 'onildo.marini-filho@icmbio.gov.br',
                        ordem : 'B'
                    },
                    {
                        nome  : 'Suelma Ribeiro Silva',
                        foto  : 'https://www.icmbio.gov.br/cbc/images/stories/thumbnails/images/stories/quem_somos/Foto%20Suelma%204.jpg',
                        cargo : 'Analista ambiental',
                        email : 'suelma.silva@icmbio.gov.br',
                        lattes: 'http://lattes.cnpq.br/9749677449869366',
                        ordem : 'B'
                    },
                    {
                        nome  : 'Danilo do Carmo Vieira Corrêa',
                        foto  : 'https://www.icmbio.gov.br/cbc/images/stories/thumbnails/images/stories/quem_somos/Foto%20Danilo%202.jpg',
                        cargo : 'Analista ambiental',
                        email : 'danilo.correa@icmbio.gov.br',
                        lattes: 'http:/lattes.cnpq.br/4954293842174064',
                        ordem : 'B'
                    },
                    {
                        nome  : 'Marcelo Marcelino de Oliveira',
                        foto  : '',
                        cargo : 'Analista ambiental',
                        email : 'marcelo.oliveira@icmbio.gov.br',
                        lattes: 'http://lattes.cnpq.br/9713556020607687',
                        ordem : 'B'
                    },*/
                    {
                        nome  : 'Estevão Carino Fernandes de Souza',
                        foto  : 'https://www.icmbio.gov.br/cbc/images/stories/thumbnails/images/stories/quem_somos/Foto%20Estevao%20Carino%204.jpg',
                        cargo : 'Analista ambiental',
                        email : 'estevao.souza@icmbio.gov.br',
                        lattes: 'http://lattes.cnpq.br/2439446448140342',
                        ordem : 'B'
                    },
                    {
                        nome  : 'Carlos Augusto Rangel Gonçalves',
                        foto  : 'https://www.icmbio.gov.br/cbc/images/stories/thumbnails/images/stories/quem_somos/foto%20Carlos%20Rangel%202.jpg',
                        cargo : 'Bolsista',
                        email : 'carlos.rangel.bolsista@icmbio.gov.br',
                        lattes: 'http://lattes.cnpq.br/4272778233299104',
                        ordem : 'B'
                    },
                    {
                        nome  : 'Marcio Uehara Prado',
                        foto  : 'http://servicosweb.cnpq.br/wspessoa/servletrecuperafoto?tipo=1&id=K4753943D6',
                        cargo : 'Bolsista',
                        email : 'marcio.prado.bolsista@icmbio.gov.br',
                        lattes: 'http://lattes.cnpq.br/2575493809017455',
                        ordem : 'B'
                    },
                    {
                        nome  : 'Elba Militão',
                        foto  : '',
                        cargo : 'Bolsista',
                        email : 'elba.militao.bolsista@icmbio.gov.br',
                        lattes: 'http://lattes.cnpq.br/2377083787335476',
                        ordem : 'B'
                    },
                    {
                        nome  : 'Cibelle Borges Henriques',
                        foto  : '',
                        cargo : 'Bolsista',
                        email : 'cibelle.henriques.bolsista@icmbio.gov.br',
                        lattes: 'http://lattes.cnpq.br/4513746462348485',
                        ordem : 'B'
                    },
                    {
                        nome  : 'Mariana Garcez Stein',
                        foto  : '',
                        cargo : 'Bolsista',
                        email : 'mariana.stein.bolsista@icmbio.gov.br',
                        lattes: 'http://lattes.cnpq.br/6735507025048631',
                        ordem : 'B'
                    },/*
                    {
                        nome  : 'Barbara Morais Thompson',
                        foto  : 'https://www.icmbio.gov.br/cbc/images/stories/thumbnails/images/stories/quem_somos/Foto%20Barbara%203.jpg',
                        cargo : 'Bolsista',
                        email : 'barbara.thompson.bolsista@icmbio.gov.br',
                        lattes: 'http://lattes.cnpq.br/5376323956624058',
                        ordem : 'B'
                    },*/
                    {
                        nome  : 'Luis Eugênio Barbosa',
                        foto  : 'https://www.icmbio.gov.br/cbc/images/stories/thumbnails/images/stories/quem_somos/foto_eugenio_2019.jpg',
                        cargo : 'Bolsista',
                        email : 'luis.barbos.terceirizado@icmbio.gov.br',
                        lattes: 'http://lattes.cnpq.br/4244606115269181',
                        ordem : 'B'
                    },
                    {
                        nome  : 'Paula Melo',
                        foto  : '',
                        cargo : 'Técnico Administrativo',
                        email : 'paula.melo.terceirizada@icmbio.gov.br',
                        lattes: '',
                        ordem : 'B'
                    }

                ],
                searchingParticipantes:false,
                listParticipantes:[],
                listPontosFocais:[],
                listCoordenadoresTaxon:[],
                listEquipeApoio:[],
                quemSomos:{'PONTO_FOCAL':[],'COORDENADOR_TAXON':[],'COLABORADOR_EXTERNO':[]},
                salveNumeros: {
                     categorias:[]
                    ,grupos:[]
                    ,categoriasAmeacadas:[]
                    ,gruposAmeacadas:[]
                },
            }
        },
        mounted : function(){
            var vm=this;
            this.listQuemSomos = _.sortBy(this.listQuemSomos,['ordem','nome'])
            this.listCentros = _.sortBy(this.listCentros,['sigla'])
            this.$nextTick(function(){
                setTimeout(function(){
                    $(".app-loading").hide('slow',function(){

                        //$('body').css('overflow-y','scroll');
                        // iniciar mudanca automatica da imagem do fundo
                        //startBackgroundSlides();
                        vm.doGetTotals();
                        vm.doSalveNumeros();
                        vm.getRandomImagesDestaque();
                        // recuperar dados do localstorage
                        try {
                            vm.faleConosco.email    = localStorage.getItem('userEmail')    ||'';
                            vm.faleConosco.nome     = localStorage.getItem('userName')     ||'';
                            vm.faleConosco.mensagem = localStorage.getItem('emailMessage') ||''
                        } catch ( e ) {}
                        vm.searchValue = localStorage.getItem('searchValue')

                        // aplicar plugin datatable na tabela quem somos
                        applyDefaultDataTable('tableQuemSomos');
                        // ativar eventos das abas quem somos
                        $('#tabsQuemSomos a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                            var $ele    = $(e.target);
                            var idAba   = $ele.attr('href');
                            if( $ele.data('loaded') == false ) {
                                $ele.data('loaded',true);
                                vm.doLoadParticipantes( $ele.data('papel') );
                            }
                        })
                    });
                    $("body").css('margin-top',85);
                },1500);
           });
           this.userEmail = localStorage.getItem('userEmail');
           this.userEmail = this.userEmail =='undefined' ? '' : this.userEmail;
        },
        methods: {
            doSearchSalveEmNumeros : function( data ) {
                // criar criteria em função dos parametros recebido no data
                var criterias = []
                var title = '';
                var value = '';
                criterias.push({title: 'Espécies', value: 'Avaliadas'});
                if (data.categoria && !data.grupo) {
                    data.header = 'Resultado da Consulta por Categoria ' + (data.ameacadas == true ? ' Ameaçada' : '');
                    title = 'Categoria';
                    value = data.title;
                } else if (data.grupo) {
                    data.header = 'Resultado da Consulta por Grupo';
                    var titleComplement = ' - Ameaçad' + (data.grupo == 'AVAES' ? 'as' : 'os');
                    title = 'Grupo';
                    value = data.title + (data.ameacadas == true ? titleComplement : '');
                    data.title = value
                }
                criterias.push({title: title, value: value});
                this.graphSalveNumerosParams = {params: data, criterias: criterias};
            },
            doSalveNumeros : function() {
                var url = urlSalve + 'publico/salveNumeros';
                var timeoutMinutes = timeoutMinutes ? timeoutMinutes : 2;
                var vm = this;
                return $.ajax({
                    url: url
                    , data: {}
                    , dataType: 'json'
                    , cache: true
                    , timeout: (1000 * 60 * timeoutMinutes)
                    , statusCode: {
                        404: function () {
                            swal2("Url não encotnrada.", url, 'error');
                        }
                    }
                }).done(function (res) {
                    if (res.status == 1) {
                        if (res.msg) {
                            swal2(res.msg, (res.text ? res.text : ''), (res.type ? res.type : 'error'));
                        }
                        return;
                    } else {
                        if (res.msg) {
                            swal2(res.msg, (res.text ? res.text : ''), (res.type ? res.type : 'success'));
                        }
                    }
                    // alimentar array da tabela categorias
                    if( res.data.dadosGraficoCategorias.total ) {
                        var totalGeral = res.data.dadosGraficoCategorias.total;
                        res.data.dadosGraficoCategorias.data.map(function(item,i) {
                            var categoria = Object.keys(item)[0]
                            var total = item[categoria].total
                            var percentual = Number( (total / totalGeral* 100) ).toFixed(1);
                            var cor = item[categoria].color;
                            var codigo = item[categoria].categoria;
                            var ameacada =  ['VU','CR','EN'].indexOf( codigo) > -1;
                            vm.salveNumeros.categorias.push(
                                {nome:categoria,codigo:codigo,total:total,percentual:percentual,cor:cor,ameacada:ameacada},
                            )
                        });
                        var totalGeral = res.data.dadosGraficoCategoriasAmeacadas.total;
                        res.data.dadosGraficoCategoriasAmeacadas.data.map(function(item,i) {
                            var categoria = Object.keys(item)[0]
                            var total = item[categoria].total
                            var percentual = Number( (total / totalGeral * 100) ).toFixed(1);
                            var cor = item[categoria].color;
                            var codigo = item[categoria].categoria;
                            var ameacada =  true
                            vm.salveNumeros.categoriasAmeacadas.push(
                                {nome:categoria,codigo:codigo,total:total,percentual:percentual,cor:cor,ameacada:ameacada},
                            )
                        })
                    }

                    // alimentar array da tabela grupos
                    if( res.data.dadosGraficoGrupos.total ) {
                        var totalGeral = res.data.dadosGraficoGrupos.total;
                        res.data.dadosGraficoGrupos.data.map(function(item,i) {
                            var grupo = Object.keys(item)[0]
                            var total = item[grupo].total
                            var percentual = Number( (total / totalGeral* 100) ).toFixed(1);
                            var cor = item[grupo].color;
                            var codigo = item[grupo].categoria;
                            vm.salveNumeros.grupos.push(
                                {nome:grupo,codigo:codigo,total:total,percentual:percentual,cor:cor},
                            )
                        });

                        totalGeral = res.data.dadosGraficoGruposAmeacadas.total;
                        res.data.dadosGraficoGruposAmeacadas.data.map(function(item,i) {
                            var grupo = Object.keys(item)[0]
                            var total = item[grupo].total
                            var percentual = Number( (total / totalGeral* 100) ).toFixed(1);
                            var cor = item[grupo].color;
                            var codigo = item[grupo].categoria;
                            vm.salveNumeros.gruposAmeacadas.push(
                                {nome:grupo,codigo:codigo,total:total,percentual:percentual,cor:cor},
                            )
                        })
                    }

                    window.setTimeout(function () {
                        google.charts.load('current', {'packages': ['corechart']}).then(function () {
                            // construir grafico Todas as categorias
                            window.buildChart('categorias', res.data.dadosGraficoCategorias, 400, function ( chart, sliceNumber, sliceKey, sliceData ) {
                                vm.doSearchSalveEmNumeros({'categoria':sliceData.categoria
                                    ,'title'     : sliceKey
                                    ,ameacadas   : false
                                });
                            });

                            // construir grafico Todos os Grupos
                            window.buildChart('grupos', res.data.dadosGraficoGrupos, 400, function ( chart, sliceNumber, sliceKey, sliceData ) {
                                vm.doSearchSalveEmNumeros({'grupo':sliceData.categoria
                                    ,'title'     : sliceKey
                                    ,ameacadas   : false
                                });
                            });

                            // construir grafico categorias ameaçadas
                            window.buildChart('categoriasAmeacadas', res.data.dadosGraficoCategoriasAmeacadas, 400, function ( chart, sliceNumber, sliceKey, sliceData ) {
                                vm.doSearchSalveEmNumeros({'categoria':sliceData.categoria
                                    ,'title'     : sliceKey
                                    ,ameacadas   : true
                                });
                            });
                            // construir grafico grupos das ameaçadas
                            window.buildChart('gruposAmeacadas', res.data.dadosGraficoGruposAmeacadas, 400, function ( chart, sliceNumber, sliceKey, sliceData ) {
                                vm.doSearchSalveEmNumeros({'grupo':sliceData.categoria
                                    ,'categoria' :'ameacadas'
                                    ,'title'     : sliceKey
                                    ,ameacadas   : true
                                });
                            });


                        });
                    }, 1000);
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    if (textStatus == 'timeout') {
                        swal2('Tempo limite excedido.', 'Tente novamente.', 'error')
                    }
                });
            },
            doLoadParticipantes:function( papel ) {
                var vm = this;
                if( !papel ) {
                    return;
                }
                var url = urlSalve+'publico/searchParticipantes';
                $.ajax({ url: url
                    , data: { papel : papel }
                    , dataType: 'json'
                    , cache:true
                    , timeout:(1000*60*1)
                    , statusCode: {
                        404: function() {
                            swal2( "Url não encotnrada.",url,'error' );
                        }
                    }
                }).done(function ( res ) {
                    if (res.status == 1) {
                        if( res.msg ) {
                            swal2(res.msg, (res.text ? res.text : ''), (res.type ? res.type : 'error'));
                        }
                    } else {
                        if (res.msg) {
                            swal2(res.msg, (res.text ? res.text :''), (res.type ? res.type : 'success') );
                        }
                    }
                    /*res.data.map(function(item) {
                        vm.quemSomos[item.cd_papel].push( item )
                    })*/
                    vm.quemSomos[papel] = res.data;
                }).fail(function( jqXHR, textStatus, errorThrown ) {
                    if( textStatus=='timeout') {
                        swal2('Tempo limite excedido.','Tente novamente.','error')
                    }
                }).always( function() {

                });
            },
            doPopupParticipantes : function( papel ) {
                var vm = this;
                var url = urlSalve+'publico/searchParticipantes';
                this.listParticipantes = [];
                this.searchingParticipantes = true;
                var title='';
                if( papel == 'PONTO_FOCAL') {
                    title='Pontos Focais'
                } else if( papel == 'COORDENADOR_TAXON') {
                    title='Cordenadores de Taxon'
                } else if( papel == 'COLABORADOR_EXTERNO') {
                    title='Equipe de Apoio'
                }
                $("#modalParticipantes *.modal-title").html( '<i class="mr-2 text-warning fa fa-group"></i>' + title );
                $("#modalParticipantes").modal( { show : true } );
                $.ajax({ url: url
                    , data: { papel : papel }
                    , dataType: 'json'
                    , cache:true
                    , timeout:(1000*60*1)
                    , statusCode: {
                        404: function() {
                            swal2( "Url não encotnrada.",url,'error' );
                        }
                    }
                }).done(function ( res ) {
                    if (res.status == 1) {
                        if( res.msg ) {
                            swal2(res.msg, (res.text ? res.text : ''), (res.type ? res.type : 'error'));
                        }
                    } else {
                        if (res.msg) {
                            swal2(res.msg, (res.text ? res.text :''), (res.type ? res.type : 'success') );
                        }
                    }
                    vm.listParticipantes = res.data;
                    vm.$nextTick( function(){
                        applyDefaultDataTable("tableModalParticipantes");
                    });
                }).fail(function( jqXHR, textStatus, errorThrown ) {
                    if( textStatus=='timeout') {
                        swal2('Tempo limite excedido.','Tente novamente.','error')
                    }
                }).always(function(){
                    vm.searchingParticipantes=false;
                });
            },
            doSaibaMais:function() {
                var texto = "<div class='text-left'><p class='text-justify'>O processo de avaliação do risco de extinção da fauna brasileira é contínuo e, por isso, compreende diversas avaliações e reavaliações. Estas, subsidiam a atualização das listas oficiais, publicadas pelo Ministério do Meio Ambiente. Entretanto, as atualizações de normativas não seguem, obrigatoriamente, a mesma periodicidade.</p>" +
                    "<p class='text-justify'>Por isso, fique atento para eventuais discrepâncias entre as informações disponibilizadas nesta sessão (referentes às avaliações mais atuais) e aquelas apresentadas nas listas oficiais, observando as datas das últimas avaliações e das <a target='_blank' href='https://www.mma.gov.br/biodiversidade/conservacao-de-especies/fauna-ameacada/fauna.html'>portarias vigentes</a>.</p></div>";
                swal2('Cenário Atual do Risco de Extinção da Fauna no Brasil', texto, 'info')
            },
            getRandomImagesDestaque:function() {
                var vm=this;
                var $divContainerImages = $("#randomImagesDestaque");
                if ($divContainerImages.data('loaded') == true) {
                    return;
                }
                $divContainerImages.data('loaded', true);
                var url=urlSalve+'publico/getRandomImagesDestaque/20';

                $.ajax({ url: url
                    , data: {}
                    , dataType: 'json'
                    , cache:true
                    , timeout:(1000*60*1)
                    , statusCode: {
                        404: function() {
                            swal2( "Url não encotnrada.",url,'error' );
                        }
                    }
                }).done(function ( res ) {
                    if (res.status == 1) {
                        if( res.msg ) {
                            swal2(res.msg, (res.text ? res.text : ''), (res.type ? res.type : 'error'));
                        }
                    } else {
                        if (res.msg) {
                            swal2(res.msg, (res.text ? res.text :''), (res.type ? res.type : 'success') );
                        }
                    }
                    vm.listImagensEspeciesDestaque = res.data;
                    if( res.data.length == 0 ) {
                        $("#randomImagesDestaque").hide();
                        return;
                    } else {
                        $("#randomImagesDestaque").show();
                    }

                    vm.$nextTick(function(){
                        $('.sliderEspeciesDestaque').jUsualSlider({
                            slideClass: 'slider__item',
                            btns: $('.slider .slider__nav'),
                            activeIndex:1,
                            slidesPerPage:4,
                            autoSlide: true,
                            animationTime: 500,
                            timerTime: 10000,
                            afterSlide: function( indx ) {
                                // var data = $(".slider li.slider__item_active").data();
                                // $('.slider .slider__info').text( 'Autor:' + data.autor )
                            }
                        });
                    })
                }).fail(function( jqXHR, textStatus, errorThrown ) {
                    if( textStatus=='timeout') {
                        swal2('Tempo limite excedido.','Tente novamente.','error')
                    }
                }).always(function(){});
            },
            /**
             * função para limpar campo de pesquisar
             */
            doClearSearch : function() {
                this.searchValue='';
                localStorage.setItem('searchValue','');
                layout=1;
                $("#inputSearch").focus();

            },
            /**
             * função para limpar o formulário do fale conosco
             */
            doClearFaleConosco : function() {
                this.faleConosco.nome='';
                this.faleConosco.email='';
                this.faleConosco.assunto='';
                this.faleConosco.mensagem='';
                $("#fldUserName").focus();
                localStorage.setItem('userEmail','');
                localStorage.setItem('userName','');
                localStorage.setItem('assunto','');
                localStorage.setItem('emailMessage','');
            },
            /**
             * função para enviar e-mail do fale conosco
             */
            doSendEmail:function(){
                var vm=this;
                var url=urlSalve+'publico/faleConosco';
                /*if( ! this.faleConosco.email ) {
                    swal2('Campo email é obrigatorio.','','error');
                    return;
                };*/

                if( this.faleConosco.email && ! validateEmail( this.faleConosco.email ) ) {
                    swal2('Email inválido.','','error');
                    return;
                };

                if( ! this.faleConosco.assunto ) {
                    swal2('Campo assunto é obrigatorio.','','error');
                    return;
                }
                if( ! this.faleConosco.mensagem ) {
                    swal2('Campo mensagem é obrigatorio.','','error');
                    return;
                }
                doConfirm('Confirma envio do e-mail?').then(function(result){

                    if( result.dismiss ) {
                        return;
                    }
                    try {
                        if (vm.faleConosco.nome) {
                            localStorage.setItem('userName', vm.faleConosco.nome);
                        }
                        if (vm.faleConosco.email) {
                            localStorage.setItem('userEmail', vm.faleConosco.email);
                        }
                        localStorage.setItem('emailAssunto', vm.faleConosco.assunto.trim());
                        localStorage.setItem('emailMessage', vm.faleConosco.mensagem.trim());
                    } catch (e) {}

                    blockUI('Enviando e-mail...', function () {
                        grecaptcha.ready(function () {
                            grecaptcha.execute(window.reCaptchaPublicKey, {action: 'submit'}).then(function (token) {
                                var data = vm.faleConosco;
                                data.reCaptchaToken = token;
                                $.ajax({
                                    url: url
                                    , data: data
                                    , dataType: 'json'
                                    , cache: false
                                    , timeout: (1000 * 60 * 1)
                                    , statusCode: {
                                        404: function () {
                                            swal2("Url não encotnrada.", url, 'error');
                                        }
                                    }
                                }).done(function (res) {
                                    if (res.status == 1) {
                                        if (res.msg) {
                                            swal2(res.msg, (res.text ? res.text : ''), (res.type ? res.type : 'error'));
                                        }
                                    } else {
                                        if (res.msg) {
                                            swal2(res.msg, (res.text ? res.text : ''), (res.type ? res.type : 'success'));
                                        }
                                    }
                                    vm.faleConosco.mensagem = '';
                                }).fail(function (jqXHR, textStatus, errorThrown) {
                                    if (textStatus == 'timeout') {
                                        swal2('Tempo limite excedido.', 'Tente novamente.', 'error')
                                    }
                                }).always(function () {
                                    unblockUI();
                                });
                            });
                        });
                    });
                });
            },
            /**
             * função para rolar a tela para a seção informada com efeito
             * @param id
             */
            doScroll:function(id) {
                var vm = this;
                // https://plugins.compzets.com/animatescroll/#
                $(id).animatescroll({element: 'body', scrollSpeed: 1000, easing: 'easeInOutBack', padding: -5});
                //document.location.assign(id);
                //var $body = $('body');
                //$body.scrollTop($body.scrollTop()-90);
                if (id == "#inicio") {
                    this.advancedSearchVisible=false;
                    this.layout = 1;
                    this.searchParams = {};
                }
                else if (id == '#especiesAmeacadas') {}
            },
            /*onPageChange: function (page) {
                this.doSearch(null, page)
            },*/
            doGetTotals:function(){
                var vm = this;
                var url = urlSalve+'publico/totais';
                var timeoutMinutes = timeoutMinutes ? timeoutMinutes : 1;
                return $.ajax({ url: url
                    , data: {}
                    , dataType: 'json'
                    , cache:true
                    , timeout:(1000*60*timeoutMinutes)
                    , statusCode: {
                        404: function() {
                            swal2( "Url não encotnrada.",url,'error' );
                        }
                    }
                }).done(function (res) {
                    if (res.status == 1) {
                        if( res.msg ) {
                            swal2(res.msg, (res.text ? res.text : ''), (res.type ? res.type : 'error'));
                        }
                    } else {
                        if (res.msg) {
                            swal2(res.msg, (res.text ? res.text :''), (res.type ? res.type : 'success') );
                        }
                    }
                    for( var key in res.data) {
                        var ele = $("#" + key );
                        if( ele.length == 1) {
                            ele.html( res.data[ key ]);
                        }
                    }
                    vm.doOdometer();
                }).fail(function( jqXHR, textStatus, errorThrown ) {
                    if( textStatus=='timeout') {
                        swal2('Tempo limite excedido.','Tente novamente.','error')
                    }
                }).always(function(){
                    unblockUI();
                });
            },

            /**
             * função executada ao clicar no botão pesquisar
             * @param e
             * @param page
             */
            doSearch: function (e, page) {
                var vm=this;
                this.searchParams = this.doGetSearchParams()
                this.searchParams.header='Resultado da Pesquisa'
                //this.layout = 2;
                localStorage.setItem('searchValue',( this.searchValue ? this.searchValue : '') );
            },
            /**
             * função callback executa ao término da consulta
             * @param data
             */
            doOnSearchEnd:function( data ){},

            /**
             * função para atualizar a tabela dos gráficos ao clicar na imagem do titulo do gráfico
             */
            doSearchGraphByGroup: function(grupo,header) {
                this.graphAmeacasParams = {'grupo':grupo,header: header,  title:'Ameaçadas','categoria':'AMEACADAS'
                        ,criterias:[ { title:'Espécies:',value:'Ameaçadas'}
                                    ,{ title:'Categoria'    ,value:header} ]
                };
            },
            /**
             * função callback executada ao término da consulta realizada ao clicar em uma
             * das fatias do gráfico de pizza
             * @param data
             */
            doOnSearchGraphEnd:function( data ) {
                this.clickGraphMessageVisible = data.records == 0;
            },
            doOdometer: function () {
                $(".my-odometer").map(function (i, ele) {
                    var currentValue = $(ele).html().replace(/[^0-9]/, '');
                    var od = new Odometer({
                        el: ele,
                        value: 0,
                        duration: 200,
                        format: '(.ddd)',
                        theme: 'default'
                    });
                    od.update(currentValue);
                });
            },
            doGetSearchParams: function () {
                var criterias   = [];
                var params      = {q: this.searchValue};
                if( this.searchValue ) {
                    criterias.push( {title:'Contém a palavra', 'value':this.searchValue} );
                }
                if (this.advancedSearchVisible) {

                    // filtro pelas fichas publicadas
                    params.inPublicadas = ''
                    if ( this.searchByPublicadas ) {
                        params.inPublicadas = 'S'
                        criterias.push( {title:'Fichas publicadas', value: 'SIM' } );
                    }

                    // filtro pelo grupo taxonomico
                    if (this.searchByGrupoTaxonomico && this.advancedSearch.sqTaxonSearch) {
                        params.sqTaxon = this.advancedSearch.sqTaxonSearch;
                        var grupo       = $("#searchTaxonNivel option:selected").text()
                        var searchType  = $("#searchType option:selected").text()
                        var grupoNome   = $("#searchTaxon option:selected").text()
                        criterias.push({title:'Grupo taxonômico', value: grupo +' ' + searchType + ' ' + grupoNome } );
                    }

                    // filtro pelas categorias
                    if (this.searchByCategoria && this.advancedSearch.sqCategoria) {
                        params.sqCategoria = this.advancedSearch.sqCategoria.join(',');
                        var values = [];
                        $("#searchCategoria option:selected").each(function(index,option){
                            values.push( option.text );
                        });
                        criterias.push({title:'Categoria(s)', value: values.join(', ') } );
                    }

                    // filtro pelos estados
                    if (this.searchByEstado && this.advancedSearch.sqEstado) {
                        params.sqEstado = this.advancedSearch.sqEstado.join(',');
                        params.endemicaEstado = this.advancedSearch.endemicaEstado ? 'S' : 'N';
                        var title = ( params.endemicaEstado=='S' ? 'Endemica(s) do(s) ':'')  + 'Estado(s)';
                        var values = [];
                        $("#searchEstado option:selected").each(function(index,option){
                            values.push( option.text );
                        });
                        criterias.push({title:title, value: values.join(', ') } );
                    }
                    // filtro pelos biomas
                    if (this.searchByBioma && this.advancedSearch.sqBioma) {
                        params.sqBioma = this.advancedSearch.sqBioma.join(',');
                        params.endemicaBioma = this.advancedSearch.endemicaBioma ? 'S' : 'N';
                        var title = ( params.endemicaBioma == 'S' ? 'Endemica(s) do(s) ':'')  + 'Bioma(s)';
                        var values = [];
                        $("#searchBioma option:selected").each(function(index,option){
                            values.push( option.text );
                        });
                        criterias.push({title:title, value: values.join(', ') } );
                    }
                    // filtro pelas regiões
                    if (this.searchByRegiao && this.advancedSearch.sqRegiao) {
                        params.sqRegiao = this.advancedSearch.sqRegiao.join(',');
                        params.endemicaRegiao = this.advancedSearch.endemicaRegiao ? 'S' : 'N';
                        var title = ( params.endemicaRegiao == 'S' ? 'Endemica(s) da(s) ':'')  + 'Região(ões)';
                        var values = [];
                        $("#searchRegiao option:selected").each(function(index,option){
                            values.push( option.text );
                        });
                        criterias.push({title:title, value: values.join(', ') } );

                    }
                    // filtro pelos grupos
                    if (this.searchByGrupo && this.advancedSearch.sqGrupo) {
                        params.sqGrupo = this.advancedSearch.sqGrupo.join(',');
                        var title = 'Grupo(s)';
                        var values = [];
                        $("#searchGrupo option:selected").each(function(index,option){
                            values.push( option.text );
                        });
                        criterias.push({title:title, value: values.join(', ') } );
                    }

                    // filtro pelas ucs
                    if (this.searchByUc && this.advancedSearch.sqUc) {
                        params.sqUc = this.advancedSearch.sqUc.join(',');
                        params.tipoUc = this.advancedSearch.tipoUc;
                        var title = 'Unidade(s) de Conservação (' + $("input[name=radioUc]:checked").next().text()+')';
                        var values = [];
                        $("#searchUc option:selected").each(function(index,option){
                            values.push( option.text );
                        });
                        criterias.push({title:title, value: values.join(', ') } );

                    }

                    // filtro pelas ameaças
                    if (this.searchByAmeaca && this.advancedSearch.sqAmeaca) {
                        params.sqAmeaca = this.advancedSearch.sqAmeaca.join(',');
                        var title = 'Ameaça(s)';
                        var values = [];
                        $("#searchAmeaca option:selected").each(function(index,option){
                            values.push( option.text );
                        });
                        criterias.push({title:title, value: values.join(', ') } );
                    }

                    // filtro pelos usos
                    if (this.searchByUso && this.advancedSearch.sqUso) {
                        params.sqUso = this.advancedSearch.sqUso.join(',');
                        var title = 'Uso(s)';
                        var values = [];
                        $("#searchUso option:selected").each(function(index,option){
                            values.push( option.text );
                        });
                        criterias.push({title:title, value: values.join(', ') } );
                    }

                    // filtro pelas bacias
                    if (this.searchByBacia && this.advancedSearch.sqBacia) {
                        params.sqBacia = this.advancedSearch.sqBacia.join(',');
                        var title = 'Bacia(s)';
                        var values = [];
                        $("#searchBacia option:selected").each(function(index,option){
                            values.push( option.text );
                        });
                        criterias.push({title:title, value: values.join(', ') } );
                    }

                    // filtro pelos PANs
                    if (this.searchByPan && this.advancedSearch.sqPan) {
                        params.sqPan = this.advancedSearch.sqPan.join(',');
                        var title = 'Pan(s)';
                        var values = [];
                        $("#searchPan option:selected").each(function(index,option){
                            values.push( option.text );
                        });
                        criterias.push({title:title, value: values.join(', ') } );
                    }
                }
                return {params:params, criterias:criterias};
            },
            doFichaHtml: function( item ) {
                showFichaHtml(item);
            }
        },
        computed:{},
        watch:{
            'advancedSearch.sqNivelTaxonomico':function( value ) {
                var selectedText= $("#searchTaxonNivel option:selected").text();
                if( this.advancedSearch.sqNivelTaxonomico ) {
                    this.advancedSearch.noNivelTaxonomico = selectedText || 'Taxon';
                } else {
                    this.advancedSearch.noNivelTaxonomico = 'Taxon';
                }
            },
        },
    });

    window.blockUI = function( message, onBlock ) {
        $.blockUI({
            message: message + '<i class="ml-2 fa fa-spinner fa-spin"></i>',
            overlayCSS:  {
                backgroundColor: '#000',
                opacity:         0.6,
                cursor:          'wait'
            },
            css: {
                backgroundColor: 'rgb(58 71 44)',
                color:'#fcfcfc',
                borderColor:'#6e6f70;'
            },
            onBlock: onBlock,
        });
    };

    window.unblockUI = function()  {
        $.unblockUI();
    };

    /**
     * função para exibir janela popup para confirmação do usuário
     */
    window.doConfirm = function ( title, message, boolEmail ) {
        message = message ? message :'';
        var inputEmail='';
        var userEmail = localStorage.getItem('userEmail') || ''
        if ( boolEmail ) {
            message += '<br><br><b>Enviar para o seguinte e-mail: (opcional)</b>'
            inputEmail='<div class="input-group mt-1">' +
                '  <input type="text" id="doConfirmInput" class="form-control" value="'+userEmail+'" placeholder="informe seu e-mail" aria-label="informe seu e-mail">' +
                '  <div class="input-group-append">' +
                '    <span class="input-group-text"><i onClick="$(\'#doConfirmInput\').val(\'\').focus()" title="limpar e-mail" class="fa fa-times text-danger cursor-pointer"></i></span>' +
                '  </div>' +
                '</div>'
        }
        return swal({
            title: title,
            html: message+inputEmail,
            //icon: 'question',
            type: 'question',
            allowOutsideClick: false,
            allowEscapeKey: true,
            showConfirmButton: true,
            showCancelButton: true,
            confirmButtonText: 'Sim',
            cancelButtonText: 'Não',
            confirmButtonColor: '#28a745',
            cancelButtonColor: '#dc3545',
            //input: (boolEmail ? 'email' : null),
            //inputValue : userEmail,
            //inputAttributes: {'autocomplete':true},
            //inputPlaceholder: 'Informe seu email',
            preConfirm:function(){
                if( $("#doConfirmInput").val() ) {
                    return { email:$("#doConfirmInput").val() };
                }
              return {email:''}
            },
            inputValidator: function( value ) {
            },
        });
    };

    /**
     * função para exibir a ficha completa no formato HTML em uma
     * janela popup
     * @param item
     */
    window.showFichaHtml = function( item ) {
        var urlAjax =  urlSalve+'publico/fichaAjax/'+item.sq_ficha;
        var scrollPosition = $("body").scrollTop();
        $.fancybox.open({
            src  : urlAjax,
            type : 'ajax',
            opts : {
                //parentEl: "body",
                //backFocus: true,
                hideScrollbar:false, // nao funciona
                animationEffect: "circular", //"zoom-in-out",
                animationDuration: 700,
                //transitionDuration: 1000,
                //transitionEffect: "tube",
                //caption:'Ficha da Espécie',
                toolbar: "auto",
                smallBtn: false,
                modal: true,
                //trapFocus: true,
                //clickOutside: "false",
                clickSlide: false,
                afterClose: function(){
                    window.setTimeout(function(){
                        $("body").scrollTop(scrollPosition);
                    },100)

                },
                afterShow: function (instance, current) {
                    // aplicar evento open/close nos collapes para disparar
                    // as chamadas ajax de acordo com a seção aberta

                    var divContainerFicha = $('#div-'+item.sq_ficha);
                    divContainerFicha.find('div.collapse').on('hide.bs.collapse show.bs.collapse', function ( evt ) {
                        var divCollapse = $(evt.target);
                        var eventType = evt.type;
                        var faElement = $("div[data-target='#" + divCollapse.attr('id') + "']").find('i.icon-state');
                        var sectionData = $.extend({},{ 'subitem' : true }, divCollapse.data() );
                        if (eventType == 'hide') {
                            faElement.removeClass('fa-minus')
                            faElement.addClass('fa-plus');
                            return;
                        } else if (sectionData.loaded) {
                            faElement.removeClass('fa-plus');
                            faElement.addClass('fa-minus');
                            return;
                        }
                        divCollapse.data('loaded', true);
                        var cardBody = divCollapse.find('div.card-body');
                        //cardBody.html('<h4>Carregando...<i class="fa fa-spinner fa-spin"></i></h4>');
                        cardBody.html('<div class="div-loading"><h5>Carregando...</h5></div>');
                        faElement.addClass('fa-spin');
                        $.ajax({
                            url: urlAjax
                            , method: 'POST'
                            , dataType: 'HTML'
                            , data: sectionData
                            , cache: true
                            , timeout: (1000 * 60 * 1)
                            , statusCode: {
                                404: function () {
                                    cardBody.html("Url de pesquisa não encotnrada<br>" + urlAjax);
                                }
                            }
                        }).done(function (html) {
                            faElement.removeClass('fa-spin');
                            faElement.addClass('fa-minus');
                            cardBody.html(html);
                            // se for seção do mapa, inicializar o mapa na div mapOnline
                            if( $(html).find('div#onlineMap').length == 1 ) {
                                var mapOptions = {
                                    el: 'onlineMap'
                                    , height: 500
                                    , callbackActions : function (res,options) {}
                                    , sqFicha: item.sq_ficha
                                    , readOnly: true
                                    , showToolbar: false
                                    , showZee : false
                                    , urlAction: null
                                }
                                map =  new Ol3MapPublic(mapOptions);
                                map.run();
                            }
                        }).fail(function (jqXHR, textStatus, errorThrown) {
                            faElement.removeClass('fa-spin');
                            divCollapse.data('loaded', false);
                            if (textStatus == 'timeout') {
                                cardBody.html('Tempo limite excedido.<br>Tente novamente.');
                            }
                        });
                    }); // fim onOpen/Close event
                },
                ajax: {
                    settings: {
                        data: {
                            ajax: true,
                        }
                    }
                },
                btnTpl: {
                    close:
                        '<button data-fancybox-close class="fancybox-button fancybox-button--close" title="Fechar">' +
                        '<i class="text-danger red fa fa-times fa-1x"></i>' +
                        "</button>",
                },

            }
        });
    };

    /**
     * Exibor notificação no cano superior direito da tela
     * @site: https://www.jqueryscript.net/other/jQuery-Bootstrap-Based-Toast-Notification-Plugin-toaster.html
     * @param [title]
     * @param message
     * @param type - danger, success, info, warning
     */
    window.notify = function( message, timeout, type, title ){
        $.toaster({ message : message
            , title : title ? title+'<br>' : ''
            , priority : (type ? type : 'success')
            , settings:{
                timeout:timeout ? timeout : 2000,
                toaster: {
                    css: {
                        top:"90px"
                    }
                },
                toast : {
                        template:
                            '<div class="alert alert-%priority% alert-dismissible" role="alert">' +
                            '<button type="button" class="close" data-dismiss="alert">' +
                            '<span aria-hidden="true">&times;</span>' +
                            '<span class="sr-only">Fechar</span>' +
                            '</button>' +
                            '<span class="title"></span><span class="message"></span>'+
                            '</div>',
                        defaults: {
                                'title': 'Mensagem',
                                'priority': 'success'
                            },
                    }
            }
        });
    };

    window.swal2 = function( message, text, type ) {
        type = type ? type : 'success'; // warning, error, success, info, and question
        /*Swal.fire({
            title: message,
            html: text,
            icon: type,
            confirmButtonText: 'Ok'
        })*/
        swal({
            title: message,
            html: text,
            type: type,
            confirmButtonText: 'Ok'
        })
    };

    /**
     * fazer o download do arquivo
     * @param fileName
     */
    window.doOpenWindow = function( fileName ) {
        window.location.assign(urlSalve + 'download?fileName=' + fileName);
    };

    /**
     * aplicar efeito no papel de parede
     * @site: https://github.com/rewish/jquery-bgswitcher#built-in-effect-types
     */
    window.startBackgroundSlides = function() {
        $("body").bgswitcher({
            images: [
                urlSalve +'assets/public/wallpaper00.jpg',
                //urlSalve +'assets/public/wallpaper01.jpg',
                //urlSalve +'assets/public/wallpaper02.jpg',
                urlSalve +'assets/public/wallpaper03.jpg',
                urlSalve +'assets/public/wallpaper04.jpg',
                urlSalve +'assets/public/wallpaper05.jpg',
                //urlSalve +'assets/public/wallpaper06.jpg',
                urlSalve +'assets/public/wallpaper07.jpg',
                urlSalve +'assets/public/wallpaper08.jpg',
                //urlSalve +'assets/public/wallpaper09.jpg',
                //urlSalve +'assets/public/wallpaper10.jpg',
                urlSalve +'assets/public/wallpaper11.jpg',
                urlSalve +'assets/public/wallpaper12.jpg',
                urlSalve +'assets/public/wallpaper13.jpg',

            ],
            interval:60000 * 5,
            //interval:10000,
            loop:true,
            easing:'swing', // swing
            effect:'blind', // fade, blind, clip, slide,drop ou hide ( no effect)
            duration:2000,
          });
    };

    /**
     * função para renderizar o grafico na div
     * @site: https://developers.google.com/chart/interactive/docs/gallery/piechart#Configuration_Options
     * @site: https://developers.google.com/chart/interactive/docs/gallery/piechart
     * @param id
     * @param title
     * @param data - array [ ['label',valor] ]
     */
    window.buildChart=function( id, data, sizePx , onClickCallback ) {
        //console.log( data );
        //console.log( data );
        sizePx = sizePx ? sizePx : 250;
        var eleGraph   = document.getElementById('graph'+id);
        //var eleWrapper = document.getElementById('graphWrapper'+id);
        var eleTitle   = document.getElementById('graphTitle'+id);
        //var eleImage   = document.getElementById('graphImage'+id);
        if( !eleGraph ) {
            console.log( 'Elemento graph'+ id + ' não encontrado.');
            return;
        }
        if( typeof( data.total) == 'undefined' ) {
            eleGraph.innerHTML=''; // remover spinner
            return;
        }
        var titleGraph = data.title + ' ('+data.total+')';
        $(eleTitle).html( titleGraph );
        var dataTable = new google.visualization.DataTable();
        dataTable.addColumn('string', 'Categoria');
        dataTable.addColumn('number', 'Valor');
        dataTable.addColumn( {type: 'string', role: 'tooltip'});
        var rows = [];
        var colors=[];
        data.data.map( function( item ) {
            for( var key in item ) {
                colors.push( item[ key ].color.toUpperCase() )
                rows.push([ key             // vu,cr,...
                    , item[key].total             // total
                    //, data.title+'\n'+item[key].total +' ' + key   // tooltip
                    ,  item[key].total +' ' + key   // tooltip
                ] );
            }
        });
        dataTable.addRows(  rows  );
        // Set chart options
        var options = {
              width:sizePx
            , height:sizePx
            , chartArea: {left:'auto', top:'auto', width:'80%', height:'80%'}
            , legend:{ maxLines:3,position:'none', textStyle:{ whiteSpace:'normal', color:'white',fontSize:10,bold:true} }
            , pieSliceText:'percentage'//'value-and-percentage'
            , pieSliceTextStyle:{color:'black',fontSize:12,bold:true}
            , pieSliceBorderColor:'transparent'
            , is3D: false
            , pieHole:0
            , colors:colors //['red', 'orange', 'yellow']
            , backgroundColor:{ stroke:'transparent',fill:'transparent',strokeWidth:0 }
            , fontSize:12
            , slices: {  2: { offset: 0} }
            , tooltip: { ignoreBounds:false
                , isHtml: false
                , showColorCode:false
                , textStyle : { color:'black',textStyle:{fontSize:10} }
            }
            ,animation: {
                duration: 1500,
                easing: 'out',
                startup: true
            }
        };

        function selectHandler() {
            var selectedItem = chart.getSelection()[0];
            if ( selectedItem ) {
                var sliceNumber = selectedItem.row;
                var sliceKey    = Object.keys( data.data[ sliceNumber ] )[0];
                if( onClickCallback ) {
                    onClickCallback( chart, selectedItem.row, sliceKey,  data.data[ selectedItem.row ][ sliceKey ] );
                }
            }
        }
        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart( eleGraph );
        google.visualization.events.addListener(chart, 'select', selectHandler);
        // adicionar transparencia no gráfico e hover nas fatias
        /*
        google.visualization.events.addListener(chart, 'ready', function () {
            Array.prototype.forEach.call(eleGraph.getElementsByTagName('path'), function(rect) {
                if ( colors.indexOf( rect.getAttribute('fill').toUpperCase()) > -1) {
                    rect.setAttribute('fill', rect.getAttribute('fill')+'AA'); // adicionar transparencia
                }
            });
        });
        google.visualization.events.addListener(chart, 'onmouseover', function (obj) {
            Array.prototype.forEach.call(eleGraph.getElementsByTagName('path'), function(rect) {
                var colorIndex  = colors.indexOf( rect.getAttribute('fill').toUpperCase() )
                var pieColor    = colors[ colorIndex ];
                if( pieColor ) {
                    if ( colorIndex == obj.row ) {
                        rect.setAttribute('fill', pieColor); // remover transparencia
                    } else {
                        rect.setAttribute('fill', pieColor+'AA'); // adicionar transparencia
                    }
                }
            });
        });
        google.visualization.events.addListener(chart, 'onmouseout', function (obj) {
            Array.prototype.forEach.call(eleGraph.getElementsByTagName('path'), function(rect) {
                var colorIndex  = colors.indexOf( rect.getAttribute('fill').toUpperCase() )
                var pieColor    = colors[ colorIndex ];
                if( pieColor ) {
                    if ( colorIndex == obj.row ) {
                        rect.setAttribute('fill', pieColor + 'AA'); // adicionar transparencia
                    }
                }
            });
        });
         */
        chart.draw(dataTable, options);
        return chart;
    };
    // criar evento para observar quando uma secao ficar visivel
    var  options = {
        root: null,
        rootMargin: '0px',
        threshold: .1
    };

    /**
     * função para fazer a chamada ajax para criação dos graficos
     * das espécies ameaçadaa
     * @returns {*}
     */
    window.getGraphs = function() {
        var $divContainer = $("#graphs-container");
        if ($divContainer.data('loaded') == true) {
            return;
        }
        $divContainer.data('loaded', true);
        var url = urlSalve + 'publico/graficosAmeacadas';
        var timeoutMinutes = timeoutMinutes ? timeoutMinutes : 2;
        return $.ajax({
            url: url
            , data: {}
            , dataType: 'json'
            , cache: true
            , timeout: (1000 * 60 * timeoutMinutes)
            , statusCode: {
                404: function () {
                    swal2("Url não encotnrada.", url, 'error');
                }
            }
        }).done(function (res) {
            if (res.status == 1) {
                if (res.msg) {
                    swal2(res.msg, (res.text ? res.text : ''), (res.type ? res.type : 'error'));
                }
                return;
            } else {
                if (res.msg) {
                    swal2(res.msg, (res.text ? res.text : ''), (res.type ? res.type : 'success'));
                }
            }
            window.setTimeout(function () {
                // Load the Visualization API and the corechart package.
                google.charts.load('current', {'packages': ['corechart']}).then( function() {
                    if( res.data ) {
                        for ( grupo in res.data) {
                            var chart = window.buildChart( grupo, res.data[grupo], 250,  function( chart, sliceNumber, sliceKey, sliceData ) {
                                appVue.$data.graphAmeacasParams = { 'grupo':chart.grupo
                                    ,'categoria' : sliceData.categoria
                                    ,'header'    : chart.titulo
                                    ,'title'     : sliceKey
                                    // Adicionar criterio de seleção na planilha exportada
                                    ,criterias:[ { title:'Espécies:',value:'Ameaçadas'}
                                        ,{ title:'Grupo'    ,value:chart.titulo }
                                        ,{ title:'Categoria',value:sliceKey }
                                        ]
                                };
                            });
                            chart.grupo = grupo;
                            chart.titulo= res.data[grupo].title;
                        }
                    }
                    $("#especiesAmeacadas div#graphsWrapper i.fa-spinner").removeClass('fa-3x fa-spinner fa-spin').addClass('fa-2x').html('Sem registro');
                });
                // Set a callback to run when the Google Visualization API is loaded.
                //google.charts.setOnLoadCallback(drawChart);
            }, 1000);
        }).fail(function (jqXHR, textStatus, errorThrown) {
            if (textStatus == 'timeout') {
                swal2('Tempo limite excedido.', 'Tente novamente.', 'error')
            }
        });
    };

    window.validateEmail = function( email ) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase() );
    }

    /**
     * função para inicializar o plugin datatable nas tabelas basicas html que não possuem paginação
     * adicionando a funcionalidade de ordenar pelas colunas
     * @param id
     */
    window.applyDefaultDataTable = function(id) {
        setTimeout(function(){
            var t = $("#" + id.replace(/^#/,'') ).DataTable( {
                "destroy"   : true,
                "order": [ [ 0, 'asc' ] ],
                "columnDefs": [
                    { "searchable":false,"orderable": false, "targets": [0] },
                ],
                "paging"    : false,
                "scrollY"   : "500px",
                "scrollCollapse": true,
                "ordering"  : true,
                "info"      : false, // mostrar total de registros no rodape
                "searching" : true,
                buttons: [
                    'excel'
                ],
                language: {
                    search:         "Localizar&nbsp;:",
                },
            } );
            t.on( 'order.dt search.dt', function () {
                t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                    cell.innerHTML = i+1;
                } );
            } ).draw();
            //t.columns.adjust().draw();
        },500);
    }

    /**
     * criação do observer para disparar a criação dos graficos de ameaças quando a
     * pagina Especies Ameaçadas ficar visível
     * @type {IntersectionObserver}
     */
    var observer = new IntersectionObserver(function(entries, observer) {
        var ele = entries[0].target;
        var rectElem = ele.getBoundingClientRect();
        var rectContainer = $("body")[0].getBoundingClientRect();
        if (rectElem.top < rectContainer.bottom) {
            // montar os graficos quando a pagina for visualizada
            if ( ele.id == 'especiesAmeacadas' ) {
                getGraphs();
            } else if ( ele.id == 'randomImagesDestaque' ) {
                // exibir carrossel das imagens em destaque
                // appVue.getRandomImagesDestaques();
            } else if ( ele.id == 'faleConosco' ) {
                //$("#nav-coordenacao-processo-tab").click();
            } else if ( ele.id == 'livroVermelho' ) {
                //$("#nav-coordenacao-processo-tab").click();
            } else if ( ele.id == 'quemSomos' ) {
                $("#nav-coordenacao-processo-tab").click();
            }
        }
    }, options);
    var target = document.querySelector('#especiesAmeacadas');
    observer.observe( target );

    //var target = document.querySelector('#randomImagesDestaque');
    //observer.observe( target );

    var target = document.querySelector('#livroVermelho');
    observer.observe( target );

    var target = document.querySelector('#faleConosco');
    observer.observe( target );

    var target = document.querySelector('#quemSomos');
    observer.observe( target );

    // configurar lightbox  https://lokeshdhakar.com/projects/lightbox2/index.html#options
    lightbox.option({
        albumLabel: 'Imagem %1 de %2',
        fadeDuration:500,
        imageFadeDuration:500,
        resizeDuration:350,
    })
});

var onloadReCaptchaCallback = function( p ){}


function hex2rgba(x, a) {
    a = (a == undefined) ? 1 : a;
    var r = x.replace('#', '').match(/../g),
        g = [],
        i;
    for (i in r) {
        g.push(parseInt(r[i], 16));
    }
    g.push(a);
    return 'rgba(' + g.join() + ')';
}

/**
 * função para exibir dialogo com explicação sobre
 * a diferença entre a imagem do mapa de distribuição
 * e o mapa online na ficha completa
 */
function saibaMaisMapa(){
        var texto = "<div class='text-left'><p class='text-justify'>Os mapas de distribuição são produzidos a partir de registros utilizados no momento da avaliação, porém, só são disponibilizados aqueles sem carência (conforme Política de Dados estabelecida pela <a target='_blank' href='https://salve.icmbio.gov.br/salve-estadual/main/download?fileName=politica_dados_avaliacao'>IN ICMBio Nº 05/2017</a>). Dessa forma, eventualmente, novos registros podem surgir na medida em que algumas carências são vencidas; ou pontos são incluídos ou retirados das bases de dados. Trata-se, portanto, de um mapa mais dinâmico.</p></div>";
        swal2('Saiba mais', texto, 'info')
}

/**
 * função para exibir a justificativa do histórico das
 * avaliações na aba conservação
 * @param idTaxonHistorico
 */
function lerJustificativa( sqTaxonHistorico ){


    var url = urlSalve+'publico/getJustificativaTaxonHistorico';
    $.ajax({ url: url
        , data: {sqTaxonHistorico:sqTaxonHistorico}
        , dataType: 'json'
        , cache:true
        , timeout:(1000*60*1)
        , statusCode: {
            404: function() {
                swal2( "Url não encotnrada.",url,'error' );
            }
        }
    }).done(function ( res ) {
        if (res.status == 1) {
            if( res.msg ) {
                swal2(res.msg, (res.text ? res.text : ''), (res.type ? res.type : 'error'));
            }
        } else {
            if (res.data.dsJustificativa ) {
                swal2('Justificativa', '<div class="container-justificativa-historico">'+res.data.dsJustificativa+'</div>', (res.type ? res.type : 'info') );
            } else {
                swal2('Sem justificativa','', (res.type ? res.type : 'info') );
            }
        }
    }).fail(function( jqXHR, textStatus, errorThrown ) {
        if( textStatus=='timeout') {
            swal2('Tempo limite excedido.','Tente novamente.','error')
        }
    }).always( function() {
    });
}
