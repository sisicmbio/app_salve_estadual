//# sourceURL=/assets/public/fichaHtml.js
/**
 * scripts utilizado no visualização da ficha no formato HTML
 */
$( document ).ready( function() {
    $('.collapse').on('show.bs.collapse hide.bs.collapse', function ( evt ) {
        var type    = evt.type;
        var id      = '#'+evt.target.id;
        var ele     = $("div[data-target='"+id+"']").find('i.icon-state');
        if( ele ) {
            ele.removeClass('fa-plus fa-minus')
        }
        if( type == 'show') {
            ele.addClass('fa-minus')
        } else {
            ele.addClass('fa-plus')
        }
    });

    /*lightbox.option({
        'resizeDuration': 200,
        'wrapAround': true
    })*/
});
