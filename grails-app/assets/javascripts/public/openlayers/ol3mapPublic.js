/**
 * Classe para gerar o mapa utilizando OpenLayers3 com controles customizados
 * de desenhar, arrastar, adicionar camadas WMS
 *
 * Autor: Luis Eugênio
 */
function Ol3MapPublic( opt_options ) {
    var self            = this;
    var styleCache      = {};
    var urlSalve        = ( urlSalve ? urlSalve.replace('/salve-consulta/','/salve-estadual/') : '/salve-estadual/');
    //var isCbc           =  /CBC\sBrasília-DF$/.test( $("#spanLoginUserName").prop('title'))
    var turfPoligonoBrasil = null
    var turfPoligonoZee = null
    var default_options = { showToolbar: true
        , showGeocoder:true
        , sqFicha:0
        , readOnly:false
        , urlOcorrencias:urlSalve+'api/ocorrenciasModuloPublico/'
        , showExportButton:true
        , showBioma:false
        , showUcFederal:false
        , showUcEstadual:false
        , showZee:false
    };


    // gerar um webUserId ou utilizar a variavel global sessionId definada pela aplicação
    this.webUserId = ( Math.floor(Math.random() * 1000000) + 1) * -1;
    if( typeof( webUserId ) == 'string' && webUserId != '')
    {
        this.webUserId = webUserId;
    }

    this.options            = opt_options || {};
    this.options            = $.extend({},default_options,this.options);
    this.overlayPopup       = null; // camada para exibição da janela popup ao clicar sobre um ponto
    this.filters            = []; // filtros de dados ativos no mapa
    this.filteredIds        = []; // ids dos pontos filtrados para agilizar a atualização do gride
    this.sqFicha            = this.options.sqFicha; // para exibição das camdas de ocorrências
    this.contexto           = this.options.contexto||''; // contexto para fltrar ocorrencias utilizadas e nao utilizadas
    this.legendItems        = {}; // objeto contendo nome e cor para criação automática da legenda
    this.animatedPoint      = null; // ponto selecionado para efeito de animate ao mover o mapa
    this.animating          = false; // não aplicar a animação repetidamente
    this.gridId             = this.options.gridId; // id do grid para mostrar/esconder as linhas ao filtrar/desfiltrar os pontos
    this.callbackActions    = this.options.callbackActions; // método chamado após o retorno ajax
    this.urlAction          = this.options.urlAction; // url da ação ajax que será chamada para executar alguma ação no banco de dados
    this.readOnly           = this.options.readOnly; // habilitar/desabilitar ações que alteram banco de dados
    this.contextMenuItems   = [];
    this.urlOcorrencias     = this.options.urlOcorrencias.replace(/\/\/?$/,'') + '/' + this.webUserId + '/';
    this.showExportButton   = this.options.showExportButton;
    this.file               = null; // objeto arquivo atual do upload sheapeFile
    this.geocoder           = null;
    this.showZee            = this.options.showZee;
    this.showUcFederal      = this.options.showUcFederal;
    this.showUcEstadual     = this.options.showUcEstadual;
    this.showBioma          = this.options.showBioma;

    // distâncias de 2km em epsg:3587, para localizar o square onde o ponto está
    this.x2km = 2081.5665851693425;
    this.y2km = 2002.2377318836736;

    // ferramenta de medição
    this.sketch                = null; // feature criada ao inicar draw para medir distância
    this.sketchListener        = null; // evento change da linha de medição
    //----------------------------------//

    // cores padrão para as 6 camadas de ocorrências
    this.occurrenceColors = {
                     portalbioUtilizadas:"#FFFF00"
                    ,portalBioNaoUtilizadas:"#FF00FF"
                    ,portalBioExcluidas:"#8A2BE2"
                    ,salveUtilizadas:"#00ff00"
                    ,salveUtilizadasHistorico:"point_green_cross_10x10.png" // imagem deve estar no diretório /assets/images/markers
                    ,salveNaoUtilizadas:"#ff0000"
                    ,salveExcluidas:"#f4a127"
                    ,salveConsulta: "#0000ff"
                };

    // creates unique id's
    this.uid = function() {
        return 'feature_' + Math.floor(Math.random() * 10000) + 1;
    }

    if (!this.options.el) {
        // criar div para exibição do mapa se não for informado o el
        this.options.el = 'map-' + this.uid().replace(/[^0-9]/g, '');
        $('<div id="' + this.options.el + '" style="width:800px;height:600px;border:1px solid silver;"></div>').appendTo('body');
    }
    this.el = this.options.el.trim().replace(/#/g); // id do elemento html onde será criado o mapa
    this.map = null; // instancia do mapa ( ol3 )

    if( this.options && this.options.height)
    {
        $("#"+this.el).height( this.options.height );
    }
    $("#"+this.el).css('background-color','#f0f8ff');
    // serviço de mapa padrão Open Street Map
    this.layers = [];

    this.addLayer = function(layer) {
        try {
            if (layer && layer.getSource()) {
                this.layers.push(layer);
                if (this.map) {
                    this.map.addLayer(layer);
                    if( layer.get('name') ) {
                        this.updateLayerSwitch();
                    }
                }
            } else {
                console.log('layer inválido!')
            }
        } catch (e) {
            console.log(e.message);
        }
    }

    // camada OPEN STREET MAP
    var osm = new ol.layer.Tile({
        source: new ol.source.OSM({
            maxZoom: 19,
            crossOrigin: 'anonymous'
        }),
        visible: true,
        type: 'baselayer',
        name: 'Ruas (OSM)',
        id: 'osm',
        zIndex: 1
    });
    this.addLayer(osm);

    /*
    // camada BING MAP
    var bingLayer = new ol.layer.Tile({
        source: new ol.source.BingMaps({
            key: 'AnLGl1Vfs_oXbalB22KQnOifIy3w8KLQfRGMAkZ4LkxJmjeS_1VFhlxyCSiOOdVx',
            imagerySet: 'AerialWithLabels',
            preload: Infinity,
            //imagerySet: bingLayers[i],
            maxZoom: 19,
            crossOrigin: 'anonymous'
        }),
        visible: false,
        type: 'baseLayer',
        name: 'Bing Map',
        id: 'bingmap',
        zIndex: 1
    });
    this.addLayer(bingLayer);
    */


    // camada ESRI
    var esriTopografia = new ol.layer.Tile({
        source: new ol.source.XYZ({
            attributions: [
                new ol.Attribution({
                    html: 'WCMC, USGS, NASA, ESA, METI, NRCAN, GEBCO, NOAA, increment P Corp.&nbsp;<a target="_blank" href="http://services.arcgisonline.com/ArcGIS/' +
                        'rest/services/World_Topo_Map/MapServer">ESRI</a>'
                })
            ],
            url: 'https://server.arcgisonline.com/ArcGIS/rest/services/' +
                'World_Topo_Map/MapServer/tile/{z}/{y}/{x}',
            crossOrigin: 'anonymous'
        }),
        visible: false,
        type: 'baseLayer',
        name: 'Topografia Mundial (Esri)',
        id: 'esri-topografia',
        zIndex: 1
    });
    this.addLayer(esriTopografia);

    var esriGeografia = new ol.layer.Tile({
        source: new ol.source.XYZ({
            attributions: [
                new ol.Attribution({
                    html: 'WCMC, USGS, NASA, ESA, METI, NRCAN, GEBCO, NOAA, increment P Corp.&nbsp;<a target="_blank" href="http://services.arcgisonline.com/ArcGIS/' +
                        'rest/services/NatGeo_World_Map/MapServer">ESRI</a>'
                })
            ],
            url: 'https://server.arcgisonline.com/ArcGIS/rest/services/' +
                'NatGeo_World_Map/MapServer/tile/{z}/{y}/{x}',
            crossOrigin: 'anonymous'
        }),
        visible: false,
        type: 'baseLayer',
        name: 'Geografia Nacional (Esri)',
        id: 'esri-geografia',
        zIndex: 1
    });
    this.addLayer(esriGeografia);

    var esriSatelite = new ol.layer.Tile({
        source: new ol.source.XYZ({
            attributions: [
                new ol.Attribution({
                    html: 'DigitalGlobe, GeoEye, CNES/Airbus DS&nbsp;<a target="_blank" href="http://services.arcgisonline.com/ArcGIS/' +
                        'rest/services/World_Imagery/MapServer">ESRI</a>'
                })
            ],
            url: 'https://services.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}',
            crossOrigin: 'anonymous'
        }),
        visible: false,
        type: 'baseLayer',
        name: 'Satélite (Esri)',
        id: 'esri-satelite',
        zIndex: 1
    });
    this.addLayer( esriSatelite );
    if( typeof( shp ) !='undefined' ) {
        // camada batimetria
        var batimetriaLayer = new ol.layer.Vector({
            source: new ol.source.Vector(),
            style: new ol.style.Style({
                stroke: new ol.style.Stroke({
                    color: 'rgba(65,160,210, 0.8)', // azul piscina
                    width: 1
                }),
            }),
            visible: false,
            id: 'Batimetria',
            name: 'Batimetria',
            type: 'vector',
            edit: false,
            zIndex: 1
        });
        self.addLayer(batimetriaLayer);
        //var urlBatimetria = urlSalve.replace('/salve-consulta/', '/salve-estadual/') + 'api/shapefile/batimetria';
        var urlBatimetria = urlSalve + 'api/shapefile/batimetria';
        shp(urlBatimetria).then(function ( geojson ) {
            var features = self.getGeojsonValidFeatures( geojson );
            batimetriaLayer.setSource(new ol.source.Vector({
                features: features //(new ol.format.GeoJSON()).readFeatures(geojson, {featureProjection: 'EPSG:3857'})
            }));
        });
    }

    // camada BIOMAS
    var biomas = new ol.layer.Tile({
        source: new ol.source.TileWMS({
            url:'http://mapas.icmbio.gov.br/cgi-bin/mapserv?map=/geodados/icmbio/mapfile/biomas.map&version=1.1.1&service=WMS&request=GetLegendGraphic&format=image/png',
            params: { 'LAYERS': 'biomas' },
            serverType: 'mapserver',
            crossOrigin: 'anonymous'
        }),
        visible: this.showBioma,
        id: 'biomas',
        name: 'Biomas',
        type: 'wms',
        zIndex: 1
    });
    this.addLayer(biomas);


    //  uc: http://mapas.icmbio.gov.br/cgi-bin/mapserv?map=/geodados/icmbio/mapfile/uc_Federal.map&version=1.1.1&service=WMS&request=GetLegendGraphic&format=image/png&SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&FORMAT=image%2Fpng&TRANSPARENT=true&LAYERS=uc_federal&WIDTH=230&HEIGHT=230&CRS=EPSG%3A3857&STYLES=&MAP_RESOLUTION=80.99999785423279&BBOX=-7514065.628545966%2C-7514065.628545966%2C-5009377.08569731%2C-5009377.08569731
    //  uc: http://mapas.icmbio.gov.br/cgi-bin/mapserv?map=/geodados/icmbio/mapfile/uc_Federal.map&version=1.1.1&service=WMS&request=GetLegendGraphic&format=image/png&SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&FORMAT=image%2Fpng&TRANSPARENT=true&LAYERS=uc_federal&WIDTH=230&HEIGHT=230&CRS=EPSG%3A3857&STYLES=&MAP_RESOLUTION=80.99999785423279&BBOX=-7514065.628545966%2C-7514065.628545966%2C-5009377.08569731%2C-5009377.08569731
    // legenda biomas: http://mapas.mma.gov.br:80/i3geo/ogc.php?tema=bioma&layer=bioma&request=getlegendgraphic&service=wms&format=image/jpeg
    // http://mapas.mma.gov.br:80/i3geo/ogc.php?tema=bioma&layer=bioma&request=getlegendgraphic&service=wms&format=image/jpeg
    // http://mapas.mma.gov.br/cgi-bin/mapserv?map=/opt/www/html/webservices/biorregioes.map&layers=biomas&style=default&r=0.8384542120079235&SRS=EPSG:4326&CRS=EPSG:4326&FORMAT=image%2Fpng&TRANSPARENT=TRUE&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&STYLES=&BBOX=-90.738954487784,-30.492636325189,-60.985272650378,-0.73895448778376&WIDTH=256&HEIGHT=256
    // &version=1.1.1&service=WMS&request=GetLegendGraphic&format=image/png
    // http://mapas.mma.gov.br/cgi-bin/mapserv?map=/opt/www/html/webservices/biorregioes.map&layers=biomas&style=default&r=0.8384542120079235&SRS=EPSG:4326&CRS=EPSG:4326&FORMAT=image%2Fpng&TRANSPARENT=TRUE&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&STYLES=&BBOX=-90.738954487784,-30.492636325189,-60.985272650378,-0.73895448778376&WIDTH=256&HEIGHT=256
    // camada biomas
    /*
    var biomas = new ol.layer.Image({
        source: new ol.source.ImageWMS({
            //url:'http://mapas.icmbio.gov.br/cgi-bin/mapserv?map=/geodados/icmbio/mapfile/biomas.map&version=1.1.1&service=WMS&request=GetLegendGraphic&format=image/png',
            // cgi-bin mma
            // url:'http://mapas.mma.gov.br/cgi-bin/mapserv?map=/opt/www/html/webservices/biorregioes.map',
            // params: { 'LAYERS': 'biomas','SRS':'EPSG:4326','CRS':'EPSG:4326' },
            // params: { 'LAYERS': 'biomas','VERSION':'1.1.1'},
            // i3geo mma
            //http://mapas.mma.gov.br/i3geo/ogc.php?tema=ujFggztAkm
            //http://mapas.mma.gov.br/cgi-bin/mapserv?map=/opt/www/html/webservices/biorregioes.map&layers=biomas&style=default&r=0.48125924970552303&SRS=EPSG:4326&CRS=EPSG:4326&&FORMAT=image%2Fpng&TRANSPARENT=TRUE&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&STYLES=&BBOX=-55.661416178782,-21.61377889833,-49.444486987721,-15.396849707269&WIDTH=256&HEIGHT=256
            // http://mapas.mma.gov.br/i3geo/ogc.php?tema=bioma&FORMAT=image/png&transparent=true&service=wms&layers=bioma&version=1.1.1&srs=EPSG:3857&SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&FORMAT=image%2Fpng&TRANSPARENT=true&CRS=EPSG%3A3857&STYLES=&WIDTH=1071&HEIGHT=1005&BBOX=-11256422.533388197%2C-7734204.270007273%2C-777823.1998299537%2C2098655.0485977996
            // http://mapas.mma.gov.br/i3geo/ogc.php?tema=bioma&FORMAT=image/png&transparent=true&service=wms&layers=bioma&version=1.1.1&srs=EPSG:3857&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&FORMAT=image%2Fpng&TRANSPARENT=true&CRS=EPSG%3A3857&STYLES=&WIDTH=1071&HEIGHT=1005&BBOX=-11256422.533388197%2C-7734204.270007273%2C-777823.1998299537%2C2098655.0485977996
            url:'http://mapas.mma.gov.br/i3geo/ogc.php?tema=bioma&FORMAT=image/png&transparent=true&service=wms&layers=bioma&version=1.1.1&srs=EPSG:3857',
            params: { 'layers': 'bioma','version':'1.1.1'},
            serverType: 'mapserver',
            projection:'EPSG:3857',
            crossOrigin: 'anonymous',
            //tileLoadFunction:function(imageTile, src){
                // imageTile.getImage().src = src;
            //    console.log( imageTile )
             //   console.log( src )
             //   console.log('--------------------')
           // },
        }),
        visible: this.showBioma,
        id: 'biomas',
        name: 'Biomas',
        type: 'wms',
        zIndex: 1
    });*/

/*
    // camada BIOMAS XYZ
    var biomas = new ol.layer.Tile({
        source: new ol.source.XYZ({
            //url: 'http://mapas.mma.gov.br/i3geo/ogc.php/{z}/{y}/{x}?tema=bioma&LAYERS=bioma&version=1.1.1&request=GetMap&FORMAT=image/png&transparent=true&service=wms&&version=1.1.1&srs=EPSG:3857',
            crossOrigin: 'anonymous'
        }),
        visible: false,
        type: 'wms',
        name: 'Biomas',
        id: 'biomas',
        zIndex: 1
    });
    this.addLayer(biomas);
*/



    // BRASIL - MAPA
    if( typeof(brasilGeoJson) != 'undefined' ) {
        var brasilLayer = new ol.layer.Vector({
            source: new ol.source.Vector({
                features: (new ol.format.GeoJSON()).readFeatures(brasilGeoJson, {featureProjection: 'EPSG:3857'})
            }),
            style: new ol.style.Style({
                stroke: new ol.style.Stroke({
                    color: 'blue',
                    //lineDash: [1],
                    width: 1
                }),
                fill: new ol.style.Fill({
                    color: 'rgba(0, 0, 255, 0.1)'
                })
            }),
            visible: false,
            id: 'Brasil',
            name: 'Brasil',
            type: 'vector',
            edit: false,
            zIndex: 1
        });
        this.addLayer(brasilLayer);
    }

    if( typeof( shp ) != 'undefined' ) {
        var terrasIndigenasLayer = new ol.layer.Vector({
            source: new ol.source.Vector(),
            style: new ol.style.Style({
                stroke: new ol.style.Stroke({
                    color: 'rgba(203,70,19,0.8)', // azul piscina
                    width: 1
                }),
            }),
            visible: false,
            id: 'TerrrasIndigenas',
            name: 'Terras indígenas',
            type: 'vector',
            edit: false,
            zIndex: 1
        });
        self.addLayer(terrasIndigenasLayer);
        //var urlTerrasIndigenas = urlSalve.replace('/salve-consulta/', '/salve-estadual/') + 'api/shapefile/ti_sirgas2000';
        var urlTerrasIndigenas = urlSalve + 'api/shapefile/ti_sirgas2000';
        shp(urlTerrasIndigenas).then(function (geojson) {
            var features = self.getGeojsonValidFeatures( geojson );
            terrasIndigenasLayer.setSource(new ol.source.Vector({
                features: features//(new ol.format.GeoJSON()).readFeatures(geojson, {featureProjection: 'EPSG:3857'})
            }));
        });
    }

    // camada UC FEDERAL
    var ucFederal = new ol.layer.Tile({
        source: new ol.source.TileWMS({
            url:'http://mapas.icmbio.gov.br/cgi-bin/mapserv?map=/geodados/icmbio/mapfile/uc_Federal.map&version=1.1.1&service=WMS&request=GetLegendGraphic&format=image/png',
            params: { 'LAYERS': 'uc_federal' },
            serverType: 'mapserver',
            crossOrigin: 'anonymous'
        }),
        visible: this.showUcFederal,
        id: 'uc_federal',
        name: 'UC Federal',
        type: 'wms',
        zIndex: 1
    });
    this.addLayer(ucFederal);

    // camada UC ESTADUAL
    var ucEstadual = new ol.layer.Tile({
        source: new ol.source.TileWMS({
            url:'http://mapas.icmbio.gov.br/cgi-bin/mapserv?map=/geodados/icmbio/mapfile/mma_UcEstadual.map&version=1.1.1&service=WMS&request=GetLegendGraphic&format=image/png',
            params: { 'LAYERS': 'uc_estadual' },
            serverType: 'mapserver',
            crossOrigin: 'anonymous'
        }),
        visible: this.showUcEstadual,
        id: 'uc_estadual',
        name: 'UC Estadual',
        type: 'wms',
        zIndex: 1
    });
    this.addLayer(ucEstadual);



    // https://sigel.aneel.gov.br/Down/
    // http://mapas.icmbio.gov.br/cgi-bin/mapserv?map=/geodados/icmbio/mapfile/aneel_usinas_hidreletricas.map&version=1.1.1&service=WMS&request=GetLegendGraphic&format=image/png&SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&FORMAT=image%2Fpng&TRANSPARENT=true&LAYERS=aneel_usinas_hidreletricas&WIDTH=256&HEIGHT=256&CRS=EPSG%3A3857&STYLES=&BBOX=-7514065.628545966%2C-2504688.542848654%2C-5009377.08569731%2C1.3969838619232178e-9
    /*
    // camada USINAS HIDRELETRICAS - icmbio
    var usinasHidreletricas = new ol.layer.Tile({
        source: new ol.source.TileWMS({
            url:'http://mapas.icmbio.gov.br/cgi-bin/mapserv?map=/geodados/icmbio/mapfile/aneel_usinas_hidreletricas.map&version=1.1.1&service=WMS&request=GetLegendGraphic&format=image/png',
            params: { 'LAYERS': 'aneel_usinas_hidreletricas' },
            serverType: 'mapserver',
            crossOrigin: 'anonymous'
        }),
        visible: false,
        id: 'usinas_hidreletricas',
        name: 'Usinas Hidreléticas',
        type: 'wms',
        zIndex: 1
    });
    this.addLayer(usinasHidreletricas);
     */


    // https://sigel.aneel.gov.br/arcgis/services/PORTAL/Camadas/MapServer/WmsServer?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&FORMAT=image%2Fpng&TRANSPARENT=true&LAYERS=0%2C10&WIDTH=256&HEIGHT=256&CRS=EPSG%3A3857&STYLES=&BBOX=-7514065.628545966%2C-2504688.542848654%2C-5009377.08569731%2C1.3969838619232178e-9
    // https://sigel.aneel.gov.br/arcgis/services/PORTAL/Camadas/MapServer/WmsServer?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&FORMAT=image%2Fpng&TRANSPARENT=true&LAYERS=0%2C7&WIDTH=256&HEIGHT=256&CRS=EPSG%3A3857&STYLES=&BBOX=-7514065.628545966%2C-2504688.542848654%2C-5009377.08569731%2C1.3969838619232178e-9
    /*
    // camada USINAS HIDRELETRICAS ANEEL
    var usinasHidreletricas = new ol.layer.Tile({
        source: new ol.source.TileWMS({
            url:'https://sigel.aneel.gov.br/arcgis/services/PORTAL/Camadas/MapServer/WmsServer',
            params: { 'LAYERS': '0,10' },
            serverType: 'mapserver',
            crossOrigin: 'anonymous'
        }),
        visible: false,
        id: 'usinas_hidreletricas',
        name: 'Usinas Hidreléticas',
        type: 'wms',
        zIndex: 1
    });
    this.addLayer(usinasHidreletricas);
     */

    // camada ZEE
    var zee = new ol.layer.Tile({
        source: new ol.source.TileWMS({
            url:'http://mapas.icmbio.gov.br/cgi-bin/mapserv?map=/geodados/icmbio/mapfile/ibge_zee.map&version=1.1.1&service=WMS&request=GetLegendGraphic&format=image/png',
            params: { 'LAYERS': 'zee' },
            serverType: 'mapserver',
            crossOrigin: ''
        }),
        visible: this.showZee,
        id: 'zee',
        name: 'ZEE',
        type: 'wms',
        zIndex: 1
    });
    this.addLayer(zee);// camada ZEE


   // camada overlay do POPUP
    this.overlayPopup = new ol.Overlay({
        element: $('<div id="overlay-' + this.el + '" style="z-index:1;max-height:500px;max-width:400px;height:auto;overflow:hidden;overflow-y:auto;background-color: white; border-radius: 5px; border: 1px solid black; padding: 5px 10px;">').appendTo('body')[0],
        positioning: 'bottom-center'
    });

    /*
    // capturar o evento loaded do mapa
    this.layers[0].getSource().on('tileloadstart', function(event) {
        console.log(self.el+' iniciada carga do mapa...')
    });
    this.layers[0].getSource().on('tileloadend', function(event) {
        console.log(self.el+' mapa base carregado.')
    });
    this.layers[0].getSource().on('tileloaderror', function(event) {
       console.log(self.el+' mapa base carregado com erro.')
    });
    */

    this.center = [-6019690.043300373, -2814310.6931742462]; // Brasil
    this.zoom = 4;


    this.interactionMouseWheelZoom; // interação para aplicar zoom com a roda do mouse + telcla CTRL
    this.mouseWhellAlert = false;

    this.drawLayer; // camada para fazer os desenhosv
    this.interactionSelect; // interação para selecionar um desenho
    this.interactionTranslate; // interatividade de arrastar
    this.currentDrawInteraction; // modo de desenho que está selecionado no momento
    this.hiddenStyle = new ol.style.Style({ image: '' });
    this.contextMenu = null;
    this.drawing = false;
    this.clickedFeature = null;

    /**
    Método para inicialização do mapa na página
    */
    this.run = function() {
        var self = this;
        // não permitir criar o mapa mais de uma vez
        if (self.map) {
            return;
        }

        if( self.options.showToolbar ) {
            // adicionar o layer para desenhos e animate de identificação
            self.drawLayer = drawLayer = new ol.layer.Vector({
                source: new ol.source.Vector(),
                style: self.drawStyle,
                visible: true,
                type: 'vector',
                name: 'Desenhos', // nome utilizado no cadastro de ocorrencia para desenha a marker ao informar uma coordenada.
                id: 'drawlayer',
                filter: true, // retirar depois, é só para testar o filtro por intercecção
                zIndex: 4
            });
            self.addLayer(drawLayer);// adicionar o layer para desenhos e animate de identificação
        }

        // se for informado o id da ficha, exibir as camadas de ocorrências
        if (self.sqFicha) {
            // camada portalbio UTILIZADAS no processo de avaliação
            url = self.urlOcorrencias+self.sqFicha + '/geojson/portalbio/' + encodeURIComponent(self.occurrenceColors.portalbioUtilizadas) + '/s';
            self.createLayer({
                type: 'geojson'
                , name: 'Portalbio Utilizadas'
                , visible: true
                , url: url
                , style: self.occurrenceStyle
                , color: self.occurrenceColors.portalbioUtilizadas
                , filter: true
                , zIndex:5
            });

            // camada SALVE UTILIZADAS no processo de avaliação com presenca na coordendas
            url =  self.urlOcorrencias+self.sqFicha + '/geojson/salve-estadual/' +
                encodeURIComponent(self.occurrenceColors.salveUtilizadas) + '/s/t';
            self.createLayer({
                type: 'geojson'
                , name: 'Salve Utilizadas'
                , visible: true
                , url: url
                , style: self.occurrenceStyle
                , color: self.occurrenceColors.salveUtilizadas
                , filter: true
                , zIndex:5
                , layerId:'salveUtiladas'
                //, legendId:'SU'
            });

            // camada SALVE UTILIZADAS no processo de avaliação SEM presenca na coordendas ( registros historicos )
            url =  self.urlOcorrencias+self.sqFicha + '/geojson/salve-estadual/' +
                encodeURIComponent(self.occurrenceColors.salveUtilizadasHistorico) + '/sh/t';
            self.createLayer({
                type: 'geojson'
                , name: 'Registros Históricos (Salve)'
                , visible: true
                , url: url
                , style: self.occurrenceStyle
                , color: self.occurrenceColors.salveUtilizadasHistorico
                , filter: true
                , zIndex:5
                , layerId : 'salveHistorico'
            });


        }

        // INTERAÇÕES PADRÃO

        this.interactionMouseWheelZoom = new ol.interaction.MouseWheelZoom();
        //oldHandleEvent = this.interactionMouseWheelZoom.handleEvent;
        this.interactionMouseWheelZoom.handleEvent = function(e) {
            var type = e.type;
            if ( type !== "wheel" && type !== "wheel" ) {
                return true;
            }
            if ( e.originalEvent.ctrlKey || e.originalEvent.metaKey ) {
                return true
            }

            if( ! self.mouseWhellAlert ) {
                self.mouseWhellAlert=true;
                var msg = '<b>Pressione a tecla CTRL ou CMD para aumentar/diminuir o zoom do mapa ao utilizar a roda do mouse.</b>';
                self.alert( msg,8,'warning','Informação');
            }
            return false;
            //oldHandleEvent.call(this,e);
        };

        // criar as interações para possibilitar selecionar e arrastar
        this.interactionSelect = new ol.interaction.Select({style: this.selectedStyle,
            filter:function(layer) {
                // não permitir que a ferramenta de desenho selecione feature da camada de calculo da AOO
                return (layer.get('id') && layer.get('id').toLowerCase() != 'aooLayer');
            },
        });

        this.interactionTranslate = new ol.interaction.Translate({
            features: this.interactionSelect.getFeatures(),
            layers: function (layer) {
                return (layer.get('id') && layer.get('id').toLowerCase() == 'drawlayer');
            }
        });
        this.interactionTranslate.on('translateend', function (evt) {
            evt.features.forEach(function (feat) {
                // process every feature
                //console.log('translation END')
                //console.log( feat );
                self.updateFilter();
            })
        });

        this.interactionSelect.getFeatures().on('remove', function (event) {
            if (!self.drawing) {
                var id = event.element.getId() + ' / ' + event.element.get('id');
                //console.log('Remove select');
                //console.log('id ' + id + ' foi removido da seleção')
                if( self.filters.length > 0 )
                {
                    self.refresh();
                }
            }
        });

        //this.interactionSelect.on('select', function (evt) { });

        this.interactionSelect.getFeatures().on('add', function (event) {
            var feature = event.element;
            if( self.filters.length > 0 )
            {
                if( self.filters[0]['polygon'] )
                {
                    self.drawLayer.getSource().clear();
                    self.filters=[];
                    self.filters.push({'polygon':feature});
                    self.updateFilter();
                }

            }
            //gEvent = event;
            /*var feature = event.element;
            feature.on('endchange',function(){
                console.log('Feature Moved To:' + this.getGeometry().getCoordinates());
            },feature);
            */

            /*
            // enquanto estiver desenhando ignorar este evento
            if( self.currentDrawInteraction )
            {
                return;
            }

            var olCollection = self.interactionSelect.getFeatures();
            // adicionar evento keyup no documento via jQuery
            $(document).on('keyup', function(event)
            {
                if (event.keyCode == 46)
                {
                    // remove all selected features from select_interaction and my_vectorlayer
                    olCollection.forEach(function(selected_feature)
                    {
                        var selected_feature_id = selected_feature.getId();
                        // remover a copia do desenho da interactionSelect
                        olCollection.remove(selected_feature);

                        // remover o desenho da camada drawLayer
                        self.drawLayer.getSource().getFeatures().forEach( function(source_feature)
                        {
                            var source_feature_id = source_feature.getId();
                            if (source_feature_id === selected_feature_id)
                            {
                                // remove from my_vectorlayer
                                self.drawLayer.getSource().removeFeature(source_feature);
                            }
                        });
                        this.currentDrawInteraction=null;
                    });
                    // remove listener
                    $(document).off('keyup');
                }
                else if (event.keyCode == 27)
                {
                    $(document).off('keyup');
                    self.cancelDrawInteraction();
                }
            });
            */
        });

        // criação do mapa
        self.map = new ol.Map({
            layers: self.layers,
            target: self.el,
            controls: ol.control.defaults({
                attributionOptions: ({collapsible: true})
            }).extend([
                new ol.control.FullScreen()
                , new ol.control.ScaleLine()
                , new ol.control.ZoomSlider()
                , new ol.control.MousePosition({
                    projection: 'EPSG:4326',
                    //coordinateFormat: ol.coordinate.createStringXY(6),
                    coordinateFormat: function( coordinate ) {
                        return ol.coordinate.format(coordinate, 'y {y}, x {x}', 6);
                    },
                    undefinedHTML: 'Lat:? Lon:?'
                })
                //,new generateEditToolbar({ self:self })
                //,new generateLayerSwitch({ self:self })
                //,new generateDialogAddWms({ self:self })
            ]),
            interactions: ol.interaction.defaults().extend([ self.interactionMouseWheelZoom
                , self.interactionSelect
                , self.interactionTranslate]),
            view: new ol.View({
                center: self.center,
                zoom: self.zoom,
                projection: new ol.proj.Projection({
                    code: 'EPSG:3857',
                    units: 'm'
                }),
            })
        });

        // adicionar os eventos padrão ao mapa

        // evento executado uma únca vez ao carregar o mapa
        self.map.once('postrender', function (event) {
            // aplicar css padrão no mouse position
            $('#' + self.el + ' .ol-mouse-position').css({
                'padding': '3px',
                'font-size': '16px',
                'background-color': '#7c99bd',
                'color': '#fff',
                'min-width': '140px',
                'max-height': '30px',
                'border-radius': '4px',
                'text-align': 'center',
                'top': '10px',
                'bottom': 'auto',
                'left': 'auto',
                'right': '47px',
                'opacity': '0.7'
            });
            // atualizar os inputs no layer switcher
            self.addCustomControls(); // toolbar, layer switcher, add wms
            self.updateLayerSwitch();
            if( typeof(Geocoder) != 'undefined') {
                // adicionar geocoder
                self.geocoder = new Geocoder('nominatim', {
                    provider: 'osm',
                    //key: '__some_key__',
                    lang: 'pt-BR', //en-US, fr-FR
                    countrycodes: 'br',
                    placeholder: 'Informe a localidade e pressione <Enter>.',
                    targetType: 'glass-button',//'text-input',
                    limit: 15,
                    keepOpen: false,
                    preventDefault: true // não criar a marker padrão
                });
                // ao selecionar uma localidade
                self.geocoder.on('addresschosen', function (evt) {
                    /*var feature = evt.feature,
                        coord = evt.coordinate,
                        address = evt.address;
                        */
                    self.map.getView().setCenter(evt.coordinate);
                    self.setZoom(12);
                });
                self.map.addControl(self.geocoder);
                $("#gcd-button-control").attr('title', 'Encontrar local pelo nome ou parte do nome!');
                $("#gcd-button-control").parent().find('input').addClass('ignoreChange');
            }
        });


        // ao passar o mouse sobre alguma feature
        self.map.on('pointermove', function (event) {
            if (event.dragging) {
                return;
            }
            var pixel = self.map.getEventPixel(event.originalEvent);
            var hit = self.map.hasFeatureAtPixel(pixel);
            /*
            // se precisar saber qual layer está a feature encontrada
            var hit = map.forEachFeatureAtPixel(pixel, function (feature, layer) {
                return true;
            });
            */
            self.map.getViewport().style.cursor = ( hit ? 'pointer' : '');
        });

        // ao clicar uma vez no mapa
        self.map.on('singleclick', function (event) {

            if (self.drawing || self.sketch ) {
                return false;
            }
            // se for linha não precisa detectar pontos abaixo ou dentro dela
            var otherFeature = null;
            var featureDeleted=false;

            // esconder o popup aberto
            var popup = self.overlayPopup.getElement();
            popup.innerHTML = '';
            popup.style.display = 'none';

            self.clickedFeature = this.forEachFeatureAtPixel(event.pixel,
                function (feature, layer) {
                    if (feature.getGeometry().getType().toLowerCase() == 'point') {
                        return feature;
                    }
                    else if( !otherFeature && feature.get('PROFUNDIDA') && !feature.get('text') )
                    {
                        feature.set('text','Profundidade: ' + feature.get('PROFUNDIDA'));
                    }
                    else if( !otherFeature && feature.get('PROFUNDIDADE') && !feature.get('text') )
                    {
                        feature.set('text','Profundidade: ' + feature.get('PROFUNDIDADE'));
                    }
                    else if( !otherFeature && feature.get('etnia_nome') && !feature.get('text') )
                    {
                        var metadados = [{'name':'terrai_nom'   ,'label':'Nome:'}
                                       , {'name':'etnia_nome'   ,'label':'Etnia:'}
                                       , {'name':'fase_ti'      ,'label':'Fase:'}
                                       , {'name':'modalidade'   ,'label':'Modalidade:'}
                                       , {'name':'municipio_'   ,'label':'Municipio:'}
                                       , {'name':'superficie'   ,'label':'Superficie:'}
                                       , {'name':'undadm_nom'   ,'label':'Und. Adm:'}
                                       , {'name':'undadm_sig'   ,'label':'Sigla:'}
                        ];
                        var itens = [];

                        $.each( metadados, function(index,item ) {
                            itens.push( item.label+' <b>'+feature.get( item.name )+'</b>' );
                        });
                        itens.push('<div style="margin-top:5px; color:gray;border-top:1px solid black;font-size:10px;font-weight: bold;">Fonte: <a href="http://www.funai.gov.br/index.php/shape" target="_blank">Funai</a></div>');

                        feature.set('text','<div style="margin-bottom:5px;border-bottom:1px solid black;font-size:16px;font-weight: bold;">Terra Indigena</div>'+itens.join('<br>'));
                    }
                    if( !otherFeature && feature.get('text') )
                    {
                        otherFeature = feature;
                    }
                });
            // se não for clicado em cima de um ponto retornar qualquer outra feature
            if( !self.clickedFeature && otherFeature )
            {
                self.clickedFeature = otherFeature;
            }

            self.showPopup(event)
        });

        self.map.on('moveend', function (evt) {
            // identificar ponto selecionado se houver
            self.identifyPoint();
        });

        /*
         * processa os filtros ativos atualizando as camadas portalbio e salve
         */
        this.updateFilter = function() {
            //console.log('atualizando filtro..');
            this.filteredIds = [];
            // verificar quais feature estão dentro da área desenhada
            for( keyLayer in self.layers )
            {
                var layer = self.layers[keyLayer]
                var options = layer.get('options');
                if( options && options.filter )
                {
                    // ler todos os pontos do layer
                    layer.getSource().getFeatures().forEach( function(featureFound ) {
                        if( self.filters.length > 0  )
                        {
                            var coords = featureFound.getGeometry().getCoordinates();
                            self.filters.map( function( filtro,i )
                            {
                                var filterGeometry = Object.values( filtro )[0].getGeometry();
                                if ( filterGeometry.intersectsCoordinate( coords ) ) {
                                    featureFound.setStyle(null);
                                    if( featureFound.get('id') ) {
                                        self.filteredIds.push({id:featureFound.get('id'), bd: featureFound.get('bd') } );
                                    }
                                }
                                else {
                                    featureFound.setStyle(self.hiddenStyle);
                                }
                            })
                        }
                        else
                        {
                            featureFound.setStyle(null);
                            /*if( featureFound.getId() ) {
                                self.filteredIds.push({id:featureFound.getId(), bd:featureFound.get('bd') } );
                            }
                            */
                        }
                    });
                }
            }
            // mostrar/esconder as linhas do grid
            this.filterGrid();
        };

        // adicionar o menu de contexto
        // https://github.com/jonataswalker/ol3-contextmenu
        if( typeof( ContextMenu ) != 'undefined') {
            this.contextMenu = new ContextMenu({
                width: 'auto',
                defaultItems: false, // defaultItems are (for now) Zoom In/Zoom Out
                items: []
            });
            /**
             * ativar o menu de contexto ao clicar com o botão direito do mouse em um desenho
             */
            this.map.addControl(this.contextMenu);

            // exibir o menu de contexto somente se clicar sobre um desenho
            this.contextMenu.on('beforeopen', function (evt) {
                var layer = null;
                var feature = evt.target.getMap().forEachFeatureAtPixel(evt.pixel, function (ft, l) {
                    layer = l;
                    return ft;
                });

                //self.drawLayer.getSource().addFeature(feature);
                self.contextMenu.data = {feature: null, ids: []}
                if (!feature) {
                    self.contextMenu.disable();
                } else {

                    // não exibir menu de contexto para linhas de medição de distância ( measure )
                    if (feature.get('measure')) {
                        self.contextMenu.disable();
                        return;
                    }
                    //var type = feature.getGeometry().getType().toLowerCase();
                    self.contextMenu.clear();
                    self.contextMenu.data.feature = feature;

                    // guardar o(s) id(s) na varialve data do menu ao abrir para envio via ajax
                    if (feature.getGeometry().getType().toLowerCase() == 'point') {
                        if (feature.get('id')) {
                            self.contextMenu.data.ids = [{id: feature.get('id'), bd: feature.get('bd')}];
                        }
                    } else {
                        self.contextMenu.data.ids = self.filteredIds;
                    }
                    // filtrar as opções de menu de acordo com o tipo da feature clicada
                    var filteredItems = self.contextMenuItems.filter(function (item, i) {

                        var name = item.name;
                        var mostrar = false;
                        //console.log( name );
                        // nenhum id selecionado pelo filtro
                        if (self.contextMenu.data.ids.length == 0) {
                            mostrar = item.featureType.toLowerCase() == 'polygon';
                        } else {
                            mostrar = item.featureType.toLowerCase() != 'polygon';
                        }
                        if (mostrar) {
                            if (name == 'CALCULAR_AREA' && feature.getGeometry().measureTooltipElement) {
                                mostrar = false;
                            }
                        }
                        if (name == 'MARCAR_DESMARCAR_REGISTRO_HISTORICO') {
                            //if( feature.get('bd') && feature.get('bd') != 'salve') {
                            // permitir somente um ponto de cada vez
                            if (!feature.get('bd') || feature.get('bd') != 'salve') {
                                mostrar = false;
                            }
                        }
                        return mostrar

                        //return item.featureType.toLowerCase() == type || ! item.featureType
                    });
                    if (filteredItems.length > 0) {
                        self.contextMenu.extend(filteredItems);
                        self.contextMenu.enable();
                    } else {
                        self.contextMenu.disable();
                    }

                }
            });
        }

        // criar as opções do menu de contexto ao clicar sobre um ponto ou desenho com o mouse direito
        this.addContextMenuItem('Polygon','fa-filter','Filtrar registros','',function (e) {
                var tempObj = {}
                var id = 'polygon';//self.contextMenu.data.feature.getId() || 'shp';
                if( id ) {
                    tempObj[ id ] = self.contextMenu.data.feature;
                    self.filters.push(tempObj);
                    // atualizar os pontos em função dos filtros ativos em self.filters
                    self.updateFilter();
                }
            },'FILTRAR_AREA');

        // criar as opções do menu de contexto para medir area do poligono
        this.addContextMenuItem('Polygon','fa-object-ungroup','Calcular a área','',function (evt) {
              self.calcArea( self.contextMenu.data.feature, evt );
            },'CALCULAR_AREA');


        if( ! this.readOnly ) {
            this.addContextMenuItem('', 'fa-chain', 'Utilizar na avaliação', '', function (e) {
                //console.log('Atualizar pontos para utilizados na avaliação.');
                self.ajaxAction({action:'utilizarNaAvaliacao',sn:'S',ids:self.contextMenu.data.ids}, function(){
                    self.refresh(); // atualizar mapa
                });
            },'UTILIZAR_AVALIACAO');


            this.addContextMenuItem('', 'fa-chain-broken', 'Não utilizar na avaliação', '#ff0000', function (e) {
                //console.log('Atualizar pontos para NÃO utilizados na avaliação.');
                    var textoNaoUtilizado = self.contextMenu.data.feature.get('txNaoUtilizadoAvaliacao') ;
                    textoNaoUtilizado = textoNaoUtilizado ? textoNaoUtilizado : '';
                    textoNaoUtilizado = textoNaoUtilizado.replace('Registro utilizado', 'Registro não utilizado');
                    getJustificativa({txJustificativa:textoNaoUtilizado},function(params){
                    dlgJustificativa.close();
                    ultimaJustificativa = params.txJustificativa; // variavel glogal declarada no arquivo fichaCompleta.js
                    self.ajaxAction({action:'naoUtilizarNaAvaliacao', sn:'N', ids:self.contextMenu.data.ids, txJustificativa:params.txJustificativa},function(){
                        self.refresh();
                    });
                });

            },'NAO_UTILIZAR_AVALIACAO');

            this.addContextMenuItem('', 'fa-check', 'Marcar/Desmacar como registro histórico', '', function (e) {
                //console.log('Atualizar pontos para utilizados na avaliação.');
                self.ajaxAction({action:'marcarDesmarcarRegistroHistorico',ids:self.contextMenu.data.ids}, function(){
                    self.refresh(); // atualizar mapa
                });
            },'MARCAR_DESMARCAR_REGISTRO_HISTORICO','Alternar SIM/NÃO o valor do campo &quot;Presença atual na coordenada&quot;');

            this.addContextMenuItem('', 'fa-trash', 'Excluir ou Marcar como excluído ( portalbio)', '#ff0000', function (e) {
                app.confirm('Confirma a exclusão do registros?<br><br><small>obs: registros do SALVE serão excluídos da base de dados e registros do PortalBio serão apenas marcados como excluídos.</small>', function(){
                    self.ajaxAction({action:'excluir',sn:'E',ids:self.contextMenu.data.ids},function(){
                        self.refresh();// atualizar mapa
                    });
                    //console.log('Excluir pontos.');
                });
            },'EXCLUIR');

            this.addContextMenuItem('', 'fa-history', 'Restaurar registro excluído ( portalbio)', '#00FF00', function (e) {
                app.confirm('Confirma a recuperação do registros ?.</small><br><small>Obs:válido apenas para os registros do portalbio que foram excluídos.</small>', function(){
                    self.ajaxAction({action:'restaurar',sn:'R',ids:self.contextMenu.data.ids},function(){
                        self.refresh();// atualizar mapa
                    });
                    //console.log('Excluir pontos.');
                });
            },'RESTAURAR');

        }
        if( self.gridId ) {
            this.addContextMenuItem('', 'fa-bars', 'Localizar no gride', 'orange', function (e) {
                self.findGridRow(self.contextMenu.data.ids[0],true)
            },'LOCALIZAR_GRIDE');
        }
        // fim contextmenu
    } // fim run

    this.alert = function( msg, seconds, style ) {
        seconds = seconds || 3;
        seconds = (seconds > 999 ? parseInt(seconds/1000) : seconds );
        seconds = (seconds == 0 ? 3 : seconds );
        style  = style || 'danger'; // warning, info, success
        notify( msg, (seconds * 1000), style,'Mensagem' );
    };

    this.setZoom = function(intZoom) {
        this.zoom = intZoom
        this.map.getView().setZoom(this.zoom);
    };

    this.setCenter = function(x, y, in4326 ) {
        var xy=[x,y];
        if ( in4326 ) {
            xy = ol.proj.transform(xy, 'EPSG:4326', 'EPSG:3857');
        }
        this.map.getView().setCenter(xy);
    };


    // estilo dos desenhos ( cor, borda, preenchimento, transparência etc)
    this.drawStyle = function(feature) {
        var style = null;
        var featureId = feature.get('id');
        switch( featureId )
        {
            default :
                style = new ol.style.Style({
                    // cor preenchimento
                    fill: new ol.style.Fill({
                        color: 'rgba(100, 255, 255, 0.2)'
                    }),
                    // cor da borda
                    stroke: new ol.style.Stroke({
                        color: '#094696',
                        width: 1
                    }),
                    // cor e preenchimento quando o desenho for um ponto
                    image: new ol.style.Circle({
                        radius: 7,
                        fill: new ol.style.Fill({
                            color: '#ffcc33'
                        })
                    })
                });
        }
        return style;
    };

    // estilo do desenho selecionado
    this.selectedStyle = new ol.style.Style({
        // cor preenchimento
        fill: new ol.style.Fill({
            color: 'rgba(100, 255, 255, 0.2)'
        }),
        // cor da borda
        stroke: new ol.style.Stroke({
            color: '#ff0000',
            width: 2
        }),
        // cor e preenchimento quando o desenho for um ponto
        image: new ol.style.Circle({
            radius: 7,
            fill: new ol.style.Fill({
                color: '#ffcc33'
            })
        })
    });

    // desligar modo desenho
    this.cancelDrawInteraction = function() {
        if (this.currentDrawInteraction) {
            this.map.removeInteraction(this.currentDrawInteraction);
            this.currentDrawInteraction = null;
            return true;
        }
        return false;
    }

    // função generica para adicionar interações de desenho no mapa. Pode ser: Polygon, Circle, Line
    this.addDrawInteraction = function(interactionType) {
        var self = this;

        if (interactionType && /^(Polygon|Circle)$/.test(interactionType)) {

            // limpar desenho anterior
            self.clearAllDraw();
            // desativar modo desenho
            self.cancelDrawInteraction();
            self.currentDrawInteraction = new ol.interaction.Draw({
                source: self.drawLayer.getSource(),
                type: interactionType
            });
            // when a new feature has been drawn...
            self.currentDrawInteraction.on('drawstart', function (event) {
                //console.log('Draw start');
                self.drawing = true;
            });
            self.currentDrawInteraction.on('drawend', function (event) {
                //console.log('Draw end');
                self.drawing = false;
                // criar um id para o desenho para excluí-lo se for necessário
                event.feature.setId(self.uid());
                event.feature.set('id', self.uid());
                event.feature.set('edit', true); // pode ser editada
                self.cancelDrawInteraction(); // retirar modo de desenho ao fechar o poligono
            });
            self.map.addInteraction(self.currentDrawInteraction);

        } else if (interactionType.toLowerCase() == 'measure-area' || interactionType.toLowerCase() == 'measure-distance') {

            // self.createMeasureTooltip();
            if (interactionType.toLowerCase() == 'measure-distance') {
                interactionType = 'LineString'; // Polygon para m2
            }
            else {
                interactionType = 'Polygon'; // Polygon para m2
            }
            self.cancelDrawInteraction();
            self.currentDrawInteraction = new ol.interaction.Draw({
                source: self.drawLayer.getSource(),
                type: interactionType
            });
            self.currentDrawInteraction.on('drawstart', function (event) {
                //console.log('Draw measure start');
                self.sketch = event.feature;
                self.sketch.setId(self.uid());
                self.sketch.set('id', self.uid());
                self.sketch.set('edit', true); // pode ser editada
                self.sketch.set('measure', true); // diferenciar a feature das features de desenho
                self.createMeasureTooltip(self.sketch);
                var tooltipCoord = event.coordinate;
                self.sketchListener = self.sketch.getGeometry().on('change', function (evt) {
                    var geom = evt.target;
                    var output;
                    if (geom instanceof ol.geom.Polygon) {
                        output = self.formatArea(geom);
                        tooltipCoord = geom.getInteriorPoint().getCoordinates();
                    } else if (geom instanceof ol.geom.LineString) {
                        output = self.formatLength(geom);
                        tooltipCoord = geom.getLastCoordinate();
                    }
                    geom.measureTooltipElement.innerHTML = output;
                    geom.measureTooltip.setPosition(tooltipCoord);
                });
            });
            self.currentDrawInteraction.on('drawend', function (event) {
                //console.log('Draw measure end');
                self.sketch = null;
                //self.measureTooltip.feature = event.feature
                self.cancelDrawInteraction(); // retirar modo de desenho ao fechar o poligono
            });
            self.map.addInteraction(self.currentDrawInteraction);
        } else if (interactionType.toLowerCase() == 'area') {
            // limpar desenho anterior
            self.clearAllDraw();
            // a DragBox interaction used to select features by drawing boxes
            self.cancelDrawInteraction();
            //self.currentDrawInteraction = new ol.interaction.DragBox({ condition: ol.events.condition.platformModifierKeyOnly });
            self.currentDrawInteraction = new ol.interaction.DragBox();
            self.currentDrawInteraction.on('boxstart', function () {
                self.drawing = true;
                //console.log('Drag start.')
            });
            self.currentDrawInteraction.on('boxend', function (event) {
                self.drawing = false;
                //console.log('Drag end.');

                // gEvent = event;
                // console.log( event.target );
                // var extent = self.currentDrawInteraction.getGeometry().getExtent();
                // console.log( 'Área do desenho:' + extent )
                // criar o desenho na camada drawLayer
                var feature = new ol.Feature({
                    geometry: event.target.getGeometry()
                });
                feature.setId(self.uid());
                feature.set('id', self.uid());
                feature.set('edit', true); // pode ser editada

                self.drawLayer.getSource().addFeature(feature);
                self.cancelDrawInteraction();
                //verificar quais feature estão dentro da área desenhada
                //self.drawLayer.getSource().forEachFeatureIntersectingExtent(extent, function(feature) {
                //    console.log( feature.get('name') );
                //});
            });
            self.map.addInteraction(self.currentDrawInteraction);
        } else if (interactionType.toLowerCase() == 'edit') {
            self.drawEdit();
        } else if (interactionType.toLowerCase() == 'stop') {
            self.cancelDrawInteraction();
            // cancelar a seleção do item se estiver selecionado
            self.interactionSelect.getFeatures().forEach(function (selected_feature) {
                self.interactionSelect.getFeatures().remove(selected_feature);
            });
        } else if (interactionType.toLowerCase() == 'delete') {
            self.deleteDraw();
        } else if (interactionType.toLowerCase() == 'addwms') {
            var $div = $("#dlg-add-wms-" + self.el);
            $("#" + self.el).find('.ol-dialog').hide(); // fechar outras janelas
            $div.show({
                duration: 1,
                complete: function () {
                    $div.find('input:first').focus();
                }
            });
        } else if (interactionType.toLowerCase() == 'addshp') {
            var $div = $("#dlg-add-shp-" + self.el);
            $("#" + self.el).find('.ol-dialog').hide(); // fechar outras janelas
            $div.show({
                duration: 1,
                complete: function () {
                    $div.find('input:first').focus();
                }
            });
        } else if (interactionType.toLowerCase() == 'refresh') {
            self.refresh();
        }
    };

    // ativar edição do desenho
    this.drawEdit = function() {
        // desativar o modo de desenho
        if( this.cancelDrawInteraction() )
        {
            return;
        }
        var allFeatures = this.interactionSelect.getFeatures();
        if (allFeatures.getLength() == 0) {
            self.alert('Nenhum desenho selecionado.');
            return;
        }
        var edit = false;
        allFeatures.forEach(function(selected_feature) {
            // remover a copia do desenho da interactionSelect
            if (selected_feature.get('edit')) {
                edit = true;
            }
        });
        if (!edit) {
            self.alert('Polígono não editável.');
            return;
        }

        // adicionar a interação de edição dos desenhos ao mapa
        // create the modify interaction
        this.currentDrawInteraction = new ol.interaction.Modify({
            features: allFeatures,
            // delete vertices by pressing the SHIFT key
            deleteCondition: function(event) {
                return ol.events.condition.shiftKeyOnly(event) && ol.events.condition.singleClick(event);
            },
        });
        self.currentDrawInteraction.on('modifystart', function(event) {
            //console.log('Modify start.');
            //console.log( event );
        });
        self.currentDrawInteraction.on('modifyend', function(event) {
            //var features = event.features.getArray()
            //console.log('Modify end.');
            self.updateFilter();
        });

        // add it to the map
        this.map.addInteraction(this.currentDrawInteraction);
    }

    this.deleteDraw = function() {
        var self = this;
        this.cancelDrawInteraction();
        var olCollection = this.interactionSelect.getFeatures();
        var selectedId = null;
        // remove all selected features from select_interaction and my_vectorlayer
        olCollection.forEach(function(selected_feature) {
            // remover a copia do desenho da interactionSelect
            selectedId = selected_feature.getId();
            if( selected_feature.getGeometry().measureTooltip )
            {
                self.map.removeOverlay( selected_feature.getGeometry().measureTooltip );
            }
            olCollection.remove(selected_feature);

        });

        if (selectedId) {
            self.drawLayer.getSource().getFeatures().forEach(function(source_feature) {
                //self.drawLayer.getSource().removeFeature(source_feature);
                if (selectedId === source_feature.getId()) {
                    // remove from my_vectorlayer
                    if( source_feature.getGeometry().measureTooltip )
                    {
                        self.map.removeOverlay( source_feature.getGeometry().measureTooltip );
                    }

                    self.drawLayer.getSource().removeFeature(source_feature);
                    self.cancelDrawInteraction();
                    // remover do array de filtros a feature excluida
                    self.filters = self.filters.filter( function( item,i){
                        return Object.keys(item)[0] != selectedId;
                    })
                    self.updateFilter();
                    /*if (self.filters[ selectedId ]) {
                        delete self.filters[ selectedId ];
                        self.updateFilter()
                    }
                    */

                    // se for excluida uma feature de medição ( measure ), excluir tambem a tooltip com a medida
                    if( source_feature.getGeometry().measureTooltipElement )
                    {
                        source_feature.getGeometry().measureTooltipElement.parentNode.removeChild( source_feature.getGeometry().measureTooltipElement );
                    }
                }
            });
        }

    }

    /**
     * remover todos os desenhos - limpar draw layer
     **/
    this.clearAllDraw = function() {
        this.cancelDrawInteraction();
        // limpar os tooltips
        self.map.getOverlays().getArray().slice(0).forEach(function(overlay) {
            self.map.removeOverlay(overlay);
        });
        self.interactionSelect.getFeatures().clear();
        self.drawLayer.getSource().clear();
        self.filters=[];
        self.updateFilter();
    }

    /**
     * criar camadas on-line no mapa
     */
    this.createLayer = function(options) {
        var newLayer = null;
        var self=this;
        options = options || {};
        options.id = 'layer-' + this.uid();
        options.visible = options.visible == false ? false : true;
        options.filter = options.filter == true ? true : false; // layer será ignorado para aplicação de filtros
        options.zIndex = options.zIndex ? options.zIndex : 3;
        if (!options.name) {
            options.name = 'Nova camada';
        }
        if (options.type.toLowerCase() == 'geojson') {
            if (options.url) {
                if( options.shp ){
                    options.epsg = options.epsg || 'EPSG:3857';
                    var color = options.color = options.color || 'rgba(203,70,19,0.8)';
                    newLayer = new ol.layer.Vector({
                        source: new ol.source.Vector(),
                        style: new ol.style.Style({
                            stroke: new ol.style.Stroke({
                                color: color,
                                width: 1
                            }),
                        }),
                        visible: options.visible,
                        id: options.id,
                        name: options.name,
                        type: 'vector',
                        edit: false,
                        zIndex: options.zIndex,
                    });

                    //self.addLayer( terrasIndigenasLayer );
                    // importar o shp e transformar em GeoJson
                    var urlShp = options.url.replace('/salve-consulta/','/salve-estadual/');
                    urlShp = urlShp.replace(/salve\/\//,'salve/');
                    shp( urlShp ).then( function( geojson ) {
                        var validFeatures = self.getGeojsonValidFeatures( geojson );
                        newLayer.setSource( new ol.source.Vector({
                            features: validFeatures //(new ol.format.GeoJSON()).readFeatures(geojson, { featureProjection: options.epsg })
                        }));
                    });
                } else {
                    newLayer = new ol.layer.Vector({
                        source: new ol.source.Vector({
                            url: options.url,
                            format: new ol.format.GeoJSON()
                        }),
                        visible: options.visible,
                        id: options.id,
                        name: options.name,
                        edit: false,
                        type: 'vector',
                        zIndex: options.zIndex,
                        //,style  : new ol.style.Style({})
                    });
                    if (options.style) {
                        newLayer.setStyle(options.style);
                        delete options.style;
                    }
                }
                // definir a propriedade options do layer para futuras interações com a camada
                newLayer.set('options',options);
            }
            else if( options.data ) {
                // criar a propriedade text nas features se não existir
                try {
                    var data = options.data;
                    data.features.forEach(function (feature) {
                        if( ! feature.properties.text) {
                            var text = ['<b>Informações'+(options.shp ? ' (shapefile)':'')+'</b>'];
                            for ( key in feature.properties ) {
                                var prop = feature.properties[key];
                                prop = self.sanitizeHtml( prop );
                                text.push('<span style="color:#777;">' + key +'</span> = <b>' + prop + '</b>');
                            }
                            feature.properties.text = text.join('<br>');
                        }
                    });
                } catch(e){}
                var validFeatures = self.getGeojsonValidFeatures( options.data );
                newLayer = new ol.layer.Vector({
                    source: new ol.source.Vector({
                        //format: new ol.format.GeoJSON(),
                        features : validFeatures // options.data //(new ol.format.GeoJSON() ).readFeatures( options.data, { featureProjection: 'EPSG:3857' }),
                    }),
                    visible: options.visible,
                    id: options.id,
                    name: options.name,
                    type: 'vector',
                    zIndex: options.zIndex,
                });
            }
        } else if (options.type.toLowerCase() == 'wms') {
            if (options.url && options.layer && options.server) {
                newLayer = new ol.layer.Tile({
                    source: new ol.source.TileWMS({
                        url: options.url,
                        params: { 'LAYERS': options.layer },
                        serverType: options.server,
                        crossOrigin: ''
                        //crossOrigin: 'anonymous'
                    }),
                    visible: options.visible,
                    id: options.id,
                    name: options.name,
                    type: 'vector',
                    zIndex: options.zIndex,
                });
                if (options.style) {
                    newLayer.setStyle(options.style);
                    delete options.style;
                }
                // definir a propriedade options do layer para futuras interações com a camada
                newLayer.set('options',options);
            }
        }
        if (newLayer) {
            this.addLayer(newLayer);
        }
        return newLayer;
    }


    /**
     * atualizar o layer switch de acordo com os layers do mapa
     */
    this.updateLayerSwitch = function() {

        // TODO - alterar logica de criação para separar os baselayers dos vectors e depois adicionar no switcher
        var $ul = $('#layer-switch-' + this.el + ' ul')
        if (!$ul) {
            return;
        }
        // radio/check com layers
        var visibleFound = false;
        var i = 1;
        for (key in this.layers) {

            var layer = self.layers[key];
            var li = document.createElement('li');
            var input = document.createElement('input');
            var label = document.createElement('label');
            if( layer.get('name') ) {
                label.style.paddingTop = '0px';
                label.style.display = 'inline';
                var layerName = self.layers[key].get('name');
                var layerId = layer.get('id');
                var inputId = 'input-layerswitcher-' + (i++) + '-' + self.el;
                var options = (layer.get('options') ? layer.get('options') : {});
                var legendId = options.legendId;

                li.className = 'ol-layer-switch-li';
                if (!layerId) {
                    layerId = 'layer-' + self.uid();
                    layer.set('id', layerId);
                }
                // adicionar somente se não existir
                if ($ul.find('input[value=' + layerId + ']').length == 0) {
                    input.setAttribute('id', inputId);
                    label.setAttribute('for', inputId);
                    label.style.cursor = 'pointer';
                    if( options.hint ) {
                        label.setAttribute('title',options.hint);
                    }
                    if (layer.get('type')) {
                        if (layer.get('type').toLowerCase() == 'baselayer') {
                            input.setAttribute('type', 'radio');
                            input.setAttribute('name', 'radio' + self.el);
                            if (layer.getVisible() && !visibleFound) {
                                visibleFound = true;
                                input.setAttribute('checked', 'true');
                            }
                        } else {
                            input.setAttribute('type', 'checkbox');
                            if (layer.getVisible()) {
                                input.setAttribute('checked', 'true');
                            }
                        }
                        input.setAttribute('value', layerId);
                        input.addEventListener('click', function (e) {
                            var layerId = e.target.value.toLowerCase();
                            if (e.target.getAttribute('type').toLowerCase() == 'radio') {
                                for (key in self.layers) {
                                    if (self.layers[key].get('type').toLowerCase() == 'baselayer') {
                                        if (self.layers[key].get('id').toLowerCase() == layerId) {
                                            self.layers[key].setVisible(true);
                                        } else {
                                            self.layers[key].setVisible(false);
                                        }
                                    }
                                }
                            } else if (e.target.getAttribute('type').toLowerCase() == 'checkbox') {
                                for (key in self.layers) {
                                    if (self.layers[key].get('id').toLowerCase() == layerId) {
                                        self.layers[key].setVisible(e.target.checked);
                                    }
                                }
                            }
                        }, false);
                        label.innerHTML = layerName;
                        li.appendChild(input);


                        if( !legendId ) {
                            // adicionar a legenda
                            if (options.color) {
                                var legend = document.createElement('i');
                                legend.className = 'fa fa-circle';
                                legend.setAttribute('aria-hidden', "true");
                                if (options.color.indexOf('.png') == -1) {
                                    legend.style.color = options.color;
                                } else {
                                    var legend = document.createElement('img');
                                    var img = options.color.replace('_10x10', '_legend');
                                    legend.setAttribute('src', "/salve-estadual/assets/markers/" + img);
                                    legend.setAttribute('width', "14px;");
                                    legend.setAttribute('height', "14px;");
                                    legend.setAttribute('alt', layerName);
                                    legend.style.marginTop = '-4px';
                                    li.setAttribute('title', 'Ocorrências utilizadas na avaliação\nsem presença atual na coordenada.');

                                }
                                legend.style.marginLeft = '2px';
                                legend.style.marginRight = '5px';
                                li.appendChild(legend);
                            } else {
                                label.innerHTML = '&nbsp;' + label.innerHTML;
                            }
                        } else {
                            label.style.paddingLeft='6px';
                        }

                        li.appendChild(label);
                        // criar tag ui vazia para que a legenda seja criada dinamicamente pela funcao occurrenceStyle()
                        if( legendId ) {
                            var ulLegend = document.createElement('ul');
                            ulLegend.style.listStyleType='none';
                            ulLegend.style.padding='5px';
                            ulLegend.style.paddingLeft='11px';
                            ulLegend.setAttribute('id','ul-legend-'+legendId);
                            li.appendChild( ulLegend );
                        }
                        var inputType = input.getAttribute('type');
                        var $lastLi = null;
                        if (inputType == 'radio') {
                            $lastLi = $ul.find("input[type=radio]:last");
                        } else if (inputType == 'checkbox') {
                            $lastLi = $ul.find("input[type=checkbox]:last");
                            if ($lastLi.length == 0) {
                                $lastLi = $ul.find("input[type=radio]:last");
                            }
                        }
                        if ($lastLi && $lastLi.length == 1) {
                            $(li).insertAfter($lastLi.parent());
                        } else {
                            $ul.prepend(li);
                        }
                    }
                } else {
                    if (layer.getVisible()) {
                        $ul.find('input[value=' + layerId + ']').prop('checked', true);
                    }
                }
            }
        }
    };

    /**
     * adicionar controle no mapa e evitar duplicidade caso o controle já exista
     */
    this.addControl = function(Control) {
        var exists = false;
        var newInstance = new Control({ self: this });
        self.map.getControls().forEach(function(el, i) {
            if (el.element.getAttribute('name') == newInstance.element.getAttribute('name')) {
                exists = true;
            }
        });
        if (!exists) {
            self.map.addControl(newInstance);
        }
    }

    /**
     *   Criar os controles customizados para desenhar, arrastar, adicionar camada wms
     */
    this.addCustomControls = function() {
        /**
         * Criar a barra de ferramentas de desenho no mapa
         */
        function GenerateEditToolbar(opt_options) {

            var buttons = [
                { label: 'fa-mouse-pointer', title: 'Desligar modo desenho', 'type': 'Stop' }
                //,{label:'fa-object-group'    ,title:'Desenhar poligono e medir a área'           ,'type':'Polygon'}
                , { label: 'fa-bookmark-o', title: 'Desenhar poligono e calcular a área.', 'type': 'Polygon' }
                //,{label:'fa-circle-o'       ,title:'Desenhar círculo'            ,'type':'Circle'}
                , { label: 'fa-clone', title: 'Desenhar e calcular a área', 'type': 'Area' }
                , { label: 'fa-edit', title: 'Editar desenho selecionado', 'type': 'Edit' }
                , { label: 'fa-eraser', title: 'Excluir desenho', 'type': 'Delete' }
                , { label: 'fa-align-justify', title: 'Adicionar camada WMS', 'type': 'AddWms' }
                , { label: 'fa-file-image-o', title: 'Adicionar SHP', 'type': 'AddShp' }
                //, { label: 'fa-object-ungroup', title: 'Calcular a área', 'type': 'Measure-Area' }
                , { label: 'fa-arrows-h', title: 'Medir a distância', 'type': 'Measure-Distance' }
                ];
            buttons.push( { label: 'fa-retweet', title: 'Atualizar/Limpar filtro', 'type': 'Refresh' } );

            var options = opt_options || {};
            var divId = 'map-draw-toolbar-' + self.el;
            var $element = $('<div class="ol-control" style="top:8px;left:3em" name="GenerateEditToolbar" id="'+divId+'"></div>')
            for (key in buttons) {
                var text = ( buttons[key].label.indexOf('fa-')==0 ? '<i data-type="'+buttons[key].type+'" class="fa '+ buttons[key].label+'"></i>' : '<span style="font-size:14px;">'+buttons[key].label) +'<span>';
                $element.append('<button data-type="'+buttons[key].type+'" type="button" title="'+buttons[key].title+'" style="display:inline;cursor:pointer;margin-bottom:3px;">'+text+'</button>');
            }
            $element.find('button').each(function(i, button )
            {
                //$(button).css('color','black');
                $(button).on( 'click', function(e) {
                    //$(e.target).closest('div').find('button').css('color','black');
                    //$(e.target).closest('button').css('color','white');
                    e.preventDefault();
                    opt_options.self.addDrawInteraction(e.target.getAttribute('data-type'))
                });
            });

            ol.control.Control.call(this, {
                element: $element[0],
                target: options.target
            });
        };
        ol.inherits(GenerateEditToolbar, ol.control.Control);

        /**
         * Criar o tracador de camadas
         */
        function GenerateLayerSwitch(opt_options) {

            var options = opt_options || {};
            var self = options.self;
            var divContainer = document.createElement('div');
            divContainer.setAttribute('id', 'layer-switch-' + self.el);
            divContainer.setAttribute('name', 'GenerateLayerSwitch');
            divContainer.className = 'ol-control ol-layer-switch';
            divContainer.style.whiteSpace = 'nowrap';
            divContainer.style.backgroundColor = '#7c99bd';
            divContainer.style.top = '45px';
            divContainer.style.left = 'auto';
            divContainer.style.position = 'absolute';
            divContainer.style.padding = '10px 8px';
            divContainer.style.paddingTop = '2px';
            divContainer.style.fontSize = '14px';
            divContainer.style.color = '#fff';
            divContainer.style.right = '10px';
            //divContainer.style.minWidth = '120px';
            //divContainer.style.minHeight = '50px'
            divContainer.style.overflow='hidden';
            divContainer.style.margin = '0px';
            divContainer.style.opacity = '0.7';

            // titulo
            var divTitle = document.createElement('div');
            divTitle.className = 'ol-layer-switch-title';
            divTitle.style.textAlign = 'center';
            divTitle.style.borderBottom = '1px solid #fff';
            divTitle.style.marginBottom = '5px';
            //divTitle.style.fontSize      = '14px';
            divTitle.style.fontWeight = 'bold';
            divTitle.style.padding = '2px';
            divTitle.style.paddingBottom = '2px';
            divTitle.innerHTML = '<i class="fa fa-chevron-up pull-right cursor-pointer"></i><span>Camadas</span>';
            divContainer.appendChild(divTitle);
            $( divTitle ).find('i.fa-chevron-up').on('click',function(evt) {
               self.toggleLayerSwitch(evt);
            });

            var ul = document.createElement('ul');
            ul.className = 'ol-layer-switch-ul';
            ul.style.textAlign = 'left';
            ul.style.listStyle = 'none';
            ul.style.margin = '0px';
            ul.style.padding = '0px';
            ul.style.cursor = 'pointer';
            divContainer.appendChild(ul)
            // adicionar o botão de exportar o mapa
            if( self.showExportButton ) {
                var hr = document.createElement('hr')
                hr.style.padding='1px';
                hr.style.margin='4px';
                divContainer.appendChild(hr);
                var btnExportMap = document.createElement('button');
                btnExportMap.style.cursor = 'pointer';
                btnExportMap.style.width = '100%';
                btnExportMap.style.fontSize = '14px';
                btnExportMap.setAttribute('type', 'button');
                btnExportMap.setAttribute('title', 'Salvar imagem do mapa atual no formato png.');
                btnExportMap.innerHTML = 'Exportar Mapa (png)';
                divContainer.appendChild(btnExportMap)
                btnExportMap.addEventListener('click', function (e) {
                    var dateTime = new Date();
                    var nomeEspecie = $("#nmCientificoMapa").val();
                    nomeEspecie = nomeEspecie == '' ? 'Mapa-exportado-salve-' : 'Mapa '+nomeEspecie+' ';
                    var imageName = nomeEspecie + dateTime.getDate() + '-' +
                        (dateTime.getMonth()+1) + '-' +
                        dateTime.getFullYear() + '-' +
                        dateTime.getHours() + '-' +
                        dateTime.getMinutes() + '-' +
                        dateTime.getSeconds() + '.png';
                    self.map.once('postcompose', function (event) {
                        var canvas = event.context.canvas;
                        if (navigator.msSaveBlob) {
                            navigator.msSaveBlob(canvas.msToBlob(), imageName);
                        } else {
                            canvas.toBlob(function (blob) {
                                saveAs(blob, imageName);
                            });
                        }
                    });
                    self.map.renderSync();
                });
            }
            ol.control.Control.call(this, {
                element: divContainer,
                target: options.target
            });

        }
        ol.inherits(GenerateLayerSwitch, ol.control.Control);


        /**
        carregar um servico WMS externo
        ex:
        url   : https://demo.boundlessgeo.com/geoserver/ne/wms
        camada: ne:ne
        server: GeoServer
        */
        function GenerateDialogAddWms(opt_options) {
            var options = opt_options || {};
            var self = options.self;
            var divId = 'dlg-add-wms-' + self.el;
            var divContainer = document.createElement('div');
            var urlWmsSigeo  = 'http://mapas.icmbio.gov.br/cgi-bin/mapserv?map=/geodados/icmbio/mapfile/';
            var wmsVersion   = '&version=1.1.1&service=WMS&request=GetLegendGraphic&format=image/png';
            // '<option value="Uc Estadual" data-layer="uc_estadual" data-name="Uc Estadual" data-server="mapserver" data-url="'+urlWmsSigeo+'mma_UcEstadual.map'+wmsVersion+'" >UCs Estaduais</option>' +

            divContainer.setAttribute('id', divId);
            divContainer.setAttribute('name', 'GenerateDialogAddWms');
            divContainer.className = 'ol-control ol-dialog';
            divContainer.style.display = 'none';
            divContainer.style.whiteSpace = 'nowrap';
            divContainer.style.backgroundColor = '#7c99bd';
            divContainer.style.top = '45px';
            divContainer.style.left = '50px';
            divContainer.style.position = 'absolute';
            divContainer.style.padding = '10px 8px';
            divContainer.style.paddingTop = '2px';
            divContainer.style.fontSize = '14px';
            divContainer.style.color = '#fff';
            divContainer.style.width = '50%';
            divContainer.style.minWidth = '300px';
            divContainer.style.minHeight = '285px';
            divContainer.style.margin = '0px';
            divContainer.style.opacity = '0.9';

            // titulo
            var divTitle = document.createElement('div');
            divTitle.className = 'ol-dialog-title';
            divTitle.style.textAlign = 'center';
            divTitle.style.borderBottom = '1px solid #fff';
            divTitle.style.marginBottom = '5px';
            //divTitle.style.fontSize      = '14px';
            divTitle.style.fontWeight = 'bold';
            divTitle.style.padding = '2px';
            divTitle.style.paddingBottom = '2px';
            divTitle.innerHTML = 'Adicionar camada WMS';
            divContainer.appendChild(divTitle);

            ol.control.Control.call(this, {
                element: divContainer,
                target: options.target
            });

            var form = document.createElement('form');
            var formId = 'form-dlg-add-wms-' + self.el;

            form.setAttribute('id', formId );
            form.setAttribute('data-el', self.el);
            form.style.marginBottom = "0px";
            // '<option value="ZEE" data-layer="zee" data-name="ZEE" data-server="mapserver" data-url="'+urlWmsSigeo+'ibge_zee.map'+wmsVersion+'" >ZEE</option>' +
            // '<option value="Uc Federal" data-layer="uc_Federal" data-name="Uc Federal" data-server="mapserver" data-url="'+urlWmsSigeo+'uc_Federal.map'+wmsVersion+'" >UCs Federais</option>' +
            // '<option value="Biomas" data-layer="biomas" data-name="Biomas" data-server="mapserver" data-url="'+urlWmsSigeo+'biomas.map'+wmsVersion+'" >Biomas</option>' +
            // '<option value="Uc Estadual" data-layer="uc_estadual" data-name="Uc Estadual" data-server="mapserver" data-url="'+urlWmsSigeo+'mma_UcEstadual.map'+wmsVersion+'" >UCs Estaduais</option>' +
            // '<option value="Usinas Hidrelétricas" data-layer="aneel_usinas_hidreletricas" data-name="Usinas Hidrelétricas" data-server="mapserver" data-url="'+urlWmsSigeo+'aneel_usinas_hidreletricas.map'+wmsVersion+'" >Usinas Hidrelétricas (ANEEL)</option>' +
            // '<option value="Terras Indígenas (FUNAI)" data-type="geojson" data-shp="true" data-id="terrasIndigenas" data-color="rgba(203,70,19,0.8)" data-epsg="EPSG:3857" data-name="TIs out/2019" data-url="'+urlSalve+'/api/shapefile/ti_sirgas2000">Terras Indígenas (FUNAI)</option>'+
            // https://sigel.aneel.gov.br/arcgis/rest/services/PORTAL/Downloads/MapServer

            form.innerHTML = '<div style="border-bottom:1px solid white;padding-bottom:4px;margin-bottom:2px;">' +
                    '<button type="button" name="btnSelectTab1" style="border-radius:5px;cursor:pointer;font-weight:normal;padding:0px;margin:0px;display:inline-block;width:100px;float:right;">Outras</button>' +
                    '<button type="button" name="btnSelectTab2" style="border-radius:5px;cursor:default;font-weight:normal;padding:0px;margin:0px;display:inline-block;width:100px;color:silver;" disabled="true">Pré-cadastradas</button>' +
                '</div>'+
                '<div name="tab2" style="display:block;">' +
                    '<div style="display:inline-block;height:183px;">' +
                        '<label>Visualizar camada</label><br/>'+
                        '<select name="layerSigeo" class="ignoreChange" style="color:#000;height:25px;min-width:300px;padding:0px;" title="">' +
                            '<option selected value="" style="color:#000;">-- selecione --</option>' +
                            '<option value="Estados" data-layer="ibge_UF" data-name="Estados" data-server="mapserver" data-url="'+urlWmsSigeo+'ibge_Uf.map'+wmsVersion+'">Estados</option>' +
                            '<option value="Uc Municipal" data-layer="uc_municipal" data-name="Uc Municipal" data-server="mapserver" data-url="'+urlWmsSigeo+'mma_UcMunicipal.map'+wmsVersion+'" >UCs Municipais</option>' +
                            '<option value="Rppn" data-layer="rppn" data-name="RPPN" data-server="mapserver" data-url="'+urlWmsSigeo+'icmbio_Rppn.map'+wmsVersion+'">RPPN</option>' +
                            '<option value="Cavernas" data-layer="cavernas" data-name="Cavernas" data-server="mapserver" data-url="'+urlWmsSigeo+'cavernas.map'+wmsVersion+'" >Cavernas</option>' +
                            '<option value="Hidrografia 1:1.000.000 (ANA)" data-layer="hidrografia" data-name="Hidrografia 1:1.000k (ANA)" data-server="mapserver" data-url="'+urlWmsSigeo+'ana_Hidrografia_100000.map'+wmsVersion+'" >Hidrografia 1:1.000.000 (ANA)</option>' +
                            '<option value="Hidrografia 1:2.500.000 (ANA)" data-layer="hidrografia" data-name="Hidrografia 1:2.500k (ANA)" data-server="mapserver" data-url="'+urlWmsSigeo+'ana_Hidrografia_250000.map'+wmsVersion+'" >Hidrografia 1:2.500.000 (ANA)</option>' +
                            '<option value="Usinas Hidrelétricas-UHE (ANNEL)" data-layer="0,10" data-name="<i class=\'fa fa-caret-up red-dark\'></i>&nbsp;UHE (ANNEL)" data-server="mapserver" data-url="https://sigel.aneel.gov.br/arcgis/services/PORTAL/Camadas/MapServer/WmsServer" >Usinas Hidrelétricas-UHE (ANEEL)</option>' +
                            '<option value="Pequenas Centrais Hidrelétricas-PCH (ANNEL)" data-layer="0,7" data-name="<i class=\'fa fa-caret-up blue\'></i>&nbsp;PCH (ANNEL)" data-server="mapserver" data-url="https://sigel.aneel.gov.br/arcgis/services/PORTAL/Camadas/MapServer/WmsServer" >Pequenas Centrais Hidrelétricas-PCH (ANNEL)</option>'+
                '</select>' +
                    '</div>' +
                '</div>'+
                '<div name="tab1" style="display:none;">' +
                    '<div style="display:inline">Titulo&nbsp;<span style="color:red;">*</span></div>' +
                    '<div style="display:block;">' +
                    '<input name="layerName" type="text" class="ignoreChange" style="color:#000;width:100%;"></div>' +
                    '<div style="display:inline">URL<span style="color:red;">*</span></div><div style="display:block;"><input name="layerUrl" type="text" class="ignoreChange" style="color:#000;width:100%;"></div>' +
                    '<div style="display:inline">Camada<span style="color:red;">*</span></div><div style="display:block;"><input name="layerLayer" type="text" class="ignoreChange" style="color:#000;width:100%;"></div>' +
                    '<div style="display:inline-block;height:auto;">' +
                    '<label>Tipo Servidor<span style="color:red;">*</span></label><br/>' +
                    '<select name="layerServer" style="color:#000;height:25px;padding:0px;" class="ignoreChange">' +
                    '<option selected value="mapserver" style="color:#000;">MapServer</option>' +
                    '<option value="geoserver">GeoServer</option>' +
                    '</select>' +
                    '</div>'+
                '</div>'+
                '<hr style="padding:1px;margin:4px;">'+
                '<div>' +
                    '<button name="btnFechar" type="button" style="cursor:pointer;font-weight:normal;padding:0px;margin:0px;display:inline-block;width:100px;float:right;">Fechar</button>' +
                    '<button name="btnAdicionar" type="button" style="cursor:pointer;font-weight:normal;padding:0px;margin:0px;display:none;width:100px;">Adicionar</button>' +

                '</div>';
            divContainer.appendChild( form );

            // adicionar os eventos no formulário
            $(form).find('[name="btnSelectTab1"]').on('click',function( e ) {
                var $btn = $(e.target);
                var $form = $btn.closest('form');
                $form.find('div[name=tab1]').show();
                $form.find('div[name=tab2]').hide();
                var $btn1 = $form.find('button[name=btnSelectTab1]');
                $btn1.css({cursor:'default',color:'silver'});
                $btn1.prop('disabled',true);
                var $btn2 = $form.find('button[name=btnSelectTab2]');
                $btn2.css({cursor:'pointer',color:'#fff'});
                $btn2.prop('disabled',false);
                $form.find('button[name=btnAdicionar]').show();
            });
            $(form).find('[name="btnSelectTab2"]').on('click',function( e ) {
                var $btn = $(e.target);
                var $form = $btn.closest('form');
                $form.find('div[name=tab2]').show();
                $form.find('div[name=tab1]').hide();
                var $btn1 = $form.find('button[name=btnSelectTab1]');
                $btn1.css({cursor:'pointer',color:'#fff'});
                $btn1.prop('disabled',false);
                var $btn2 = $form.find('button[name=btnSelectTab2]');
                $btn2.css({cursor:'default',color:'silver'});
                $btn2.prop('disabled',true);
                $form.find('button[name=btnAdicionar]').hide();
            });

            $(form).find('[name=btnAdicionar]').on('click',function( e ) {
                var form = $(e.target).closest('form');
                var name = $(form).find('input[name=layerName]').val().trim();
                var url = $(form).find('input[name=layerUrl]').val().trim();
                var layer = $(form).find('input[name=layerLayer]').val().trim();
                var server = $(form).find('select[name=layerServer]').val().trim();
                var errors = [];
                if (!name) {
                    errors.push('Titulo é obrigatório.');
                }
                if (!url) {
                    errors.push('Url do serviço é obrigatória.');
                }

                if (!layer) {
                    errors.push('Nome da camada que será exibida é obrigatória.');
                }

                if (!server) {
                    errors.push('Tipo de servidor é obrigatório.');
                }
                if (errors.length > 0) {
                    self.alert(errors.join('\n'),6);
                    return;
                }
                var options = {
                    type: 'wms',
                    name: name,
                    url: url,
                    server: server,
                    layer: layer,
                    visible: true
                }
                // verificar se o layer já existe
                for (key in self.layers) {
                    var layer = self.layers[key];
                    if( layer.get('name') == options.name )
                    {
                        self.alert('Camada '+options.name+' já cadastrada!');
                        return;
                    }
                }
                $("#" + divId).hide();
                self.createLayer(options);
            });

            $(form).find('[name=btnFechar]').on('click',function( e ) {
                $("#" + divId).hide();
            });

            // adicionar evento no select das camadas do SIGEO
            $(divContainer).find('select[name=layerSigeo]').on('change', function(e) {
                //console.log( 'LayerSigeo change');
                var $select = $(e.target);
                var $option = $select.find('option:selected');
                var data = $option.data();
                data.type= data.type || 'wms';
                $option.prop('disabled',true);
                var options = {
                    type: data.type,
                    name: data.name,
                    url: data.url,
                    server: data.server,
                    layer: data.layer,
                    visible: true
                };
                $.extend( options,data);
                self.createLayer( options );
            });
        } // fim dialogo wms

        /**
        carregar um arquivp shp zipado
        url   : https://github.com/gipong/shp2geojson.js
        */
        function GenerateDialogAddShp(opt_options) {
            var options = opt_options || {};
            var self = options.self;
            var divId = 'dlg-add-shp-' + self.el;
            var formId = 'form-dlg-add-shp-' + self.el;
            var divContainer = $('<div id="'+divId+'" name="GenerateDialogAddShp" class="ol-control ol-dialog"' +
                'style="display:none;white-space:nowrap;background-color:#7c99bd;top:45px;left:50px;position:absolute;padding:10px 8px;padding-top:2px;' +
                'font-size:14px;color:#fff;width:50%;min-width:300px;min-height:145px;margin:0px;opacity:0.9">' +
                '<div class="ol-dialog-title" style="text-align:center;border-bottom:1px solid #fff;margin-bottom:5px;font-weight:bold;padding:2px;padding-bottom:2x;">Adicionar Shapefile (zip)</div>' +
                    '<form id="'+formId +'" data-el="'+self.el+'" style="margin-bottom:0px;">' +
                        '<label>Arquivo shapefile (zip)<span style="color:red;">*</span></label><br/>'+
                        '<input type="file" class="ignoreChange" name="shapefile" data-show-preview="false" data-show-upload="false" data-show-caption="true" data-show-remove="true" data-max-file-count="1"/>' +
                        '<label>Nome<span style="color:red;">*</span></label><br/>' +
                        '<input type="text" class="ignoreChange" name="shapefileName" style="width:100%;color:#000;" placeholder="" value=""/>' +
                        '<div style="margin-top:10px;text-align:center;">' +
                            '<button name="btnFechar" type="button" style="cursor:pointer;font-weight:normal;padding:0px;margin:0px;display:inline-block;width:100px;float:right;">Fechar</button>' +
                            '<button name="btnReset" type="button" style="cursor:pointer;font-weight:normal;padding:0px;margin:0px;display:inline-block;width:100px;">Limpar</button>' +
                            '<i class="fa fa-spin fa-spinner " style="display:none;font-size:2rem;float:left;"></i>'+
                            '<button name="btnAdicionar" type="button" style="cursor:not-allowed;font-weight:normal;padding:0px;margin:0px;display:inline-block;width:100px;float:left;" disalbled="true">Adicionar</button>' +
                        '</div>'+
                    '</form>'+
                '</div>');

            ol.control.Control.call(this, {
                element: divContainer[0],
                target: options.target
            });

            // adicionar os eventos no formulário do shpfile
            $(divContainer).find('[name=btnReset]').on('click',function( e ) {
                $(e.target).closest('form')[0].reset();
            });
                // adicionar os eventos no formulário do shpfile
            $(divContainer).find('[name=btnAdicionar]').on('click',function( e ) {
                var form = $(e.target).closest('form');
                var name = $(form).find('input[name=shapefileName]').val().trim();
                var errors = [];
                if (!name) {
                    errors.push('Informe o nome para a camada.');
                }
                if (errors.length > 0) {
                    self.alert(errors.join('\n'));
                    return
                };
                try {
                    self.loadShpZip( {layerName: name} );

                } catch( e ) {
                    self.alert('Arquivo shp inválido!\n\n'+e.message );
                }
            });

            // tratar o arquivo zip adicionado
            $(divContainer).find('[name=shapefile]').on('change',function( evt ) {
                var $btnAdicionar= $(divContainer).find('[name=btnAdicionar]')
                $btnAdicionar.prop('disabled',true);
                $btnAdicionar.css({'cursor':'not-allowed','color':'#808080'});
                self.file=null;
                if( ! evt.target.files )
                {
                    return;
                }
                var file = evt.target.files[0];

                if(file && file.size > 0) {
                    if( !/\.zip$/.test( file.name) )
                    {
                        self.alert('Selecione um arquivo zipado contendo os arquivos que compem o shapefile!');
                        return;
                    }
                    $(divContainer).find('[name=shapefileName]').focus()
                    $btnAdicionar.prop('disabled',false);
                    $btnAdicionar.css({'cursor':'pointer','color':'#fff'});
                    self.file=file;
                }
            });
           $(divContainer).find('[name=btnFechar]').on('click',function( e ) {
                $("#" + divId).hide();
            });
        } // fim dialogo shp

        ol.inherits(GenerateDialogAddWms, ol.control.Control);
        ol.inherits(GenerateDialogAddShp, ol.control.Control);
        // adicionar os controles no mapa
        if (self.options.showToolbar) {
            self.addControl(GenerateEditToolbar);
            self.addControl(GenerateDialogAddWms);
            self.addControl(GenerateDialogAddShp);
        }
        self.addControl(GenerateLayerSwitch);

    }

    /**
     * Exibir janela popup com inoformações do ponto clicado
     */
    this.showPopup = function(event) {
        var element = this.overlayPopup.getElement();
        if ( this.clickedFeature ) {
            var text = this.clickedFeature.get('text');
            if( !text )
            {
                 text = 'Não há informações.'
            }
            //text = '<div style="position:absolute;top:2px;left:376px;color:#f00;cursor:pointer;" onClick="$(this).parent().hide();">X</div>'+text;
            // extract the spatial coordinate of the click event in map projection units
            var coord = event.coordinate;
            // transform it to decimal degrees
            var degrees = ol.proj.transform(coord, 'EPSG:3857', 'EPSG:4326');
            // format a human readable version
            var hdms = ol.coordinate.toStringHDMS(degrees);
            // update the overlay element's content
            element.innerHTML = text + '<hr style="margin:4px;padding:1px;">'+hdms;
            // position the element (using the coordinate in the map's projection)
            this.overlayPopup.setPosition(coord);
            // and add it to the map
            this.map.addOverlay(this.overlayPopup);
            this.centerMap( degrees[0], degrees[1]);
            element.style.display = '';
            //$(element).show('fast');
        } else {
            element.innerHTML = '';
            element.style.display = 'none';
            //$(element).hide();
        }
    };

    /**
     * Estilo padrão das ocorrências com a criação da legenda automaticamente
     * As features devem possuir as propriedades color e legend
     * A cor padrão será vermelha se não for informada
     * @param feature
     * @param resolution
     * @returns {*|Style}
     */
    this.occurrenceStyle = function(feature, resolution ){
        var color = feature.get('color');
        var icon = feature.get('icon');
        var legend = feature.get('legend');
        var legendId = feature.get('legendId');
        var isSubespecie = feature.get('isSubespecie');
        color = color ? color : '#ff0000'; // cor padrão vermelha
        /*if (legend && color) {
            if (! self.legendItems[legend]) {
                self.legendItems[legend] = color;
                //distribuicao.updateLegend();
                //console.log('atualizar legenda...')
            }
        }*/
        var stroke;
        var fill = null;
        var radius = 5;
        var strokeWidth = 0.5; // borda preta
        var image=null;
        //icon = '/salve-estadual/assets/markers/point_green_cross_10x10.png';
        var keyStyle = color + ( isSubespecie ? '-sub' :'' );
        if( icon ) {
            keyStyle = icon
        }
        // colocar o style em cache para não ficar recriando o mesmo para cada ponto
        if( styleCache[ keyStyle ]  ) {
            return styleCache[keyStyle];
        }
        if( ! icon ) {
            // criar o estilo do ponto
            if (isSubespecie) {
                strokeWidth = 3;
                radius = 3.5;
                // borda
                stroke = new ol.style.Stroke({
                    color: color,
                    width: strokeWidth,
                });
                // preenchimento
                var fillColor = (color.toUpperCase() == '#0000FF' ? '#FFFFFF' : '#000000')
                fill = new ol.style.Fill({
                    color: hex2rgba(fillColor, "1"),
                });
            } else {
                // borda preta de 0.5 pixels
                stroke = new ol.style.Stroke({
                    color: '#000000',
                    width: strokeWidth,
                });
                // preenchimento
                fill = new ol.style.Fill({
                    color: hex2rgba(color, "1"),
                })
            }
            image = new ol.style.Circle( {
                radius: radius,
                stroke: stroke,
                fill: fill
            } )
        } else {

            image = new ol.style.Icon({
                anchor: [0, 0],
                size: [10, 10],
                scale: 1,
                opacity: 1,
                src: icon,
            })
        }
        styleCache[keyStyle] = new ol.style.Style({ image: image });

        // TODO - adicionar na legenda a cor ou a imagem aqui utilizando o layerId
        if( legendId ) {
            var $ulLegend = $("#ul-legend-"+legendId);
            if( $ulLegend.length == 1 ) {
                var img;
                var li = document.createElement('li');
                var legend = document.createElement('i');
                legend.className = 'fa fa-circle';
                legend.setAttribute('aria-hidden', "true");
                if (color.indexOf('.png') == -1) {
                    legend.style.color = color;
                } else {
                    var legend = document.createElement('img');
                    var img = icon.color.replace('_10x10', '_legend');
                    legend.setAttribute('src', "/salve-estadual/assets/markers/" + img);
                    legend.setAttribute('width', "13px;");
                    legend.setAttribute('height', "13px;");
                    legend.setAttribute('alt', '');
                    legend.style.marginTop = '-4px';
                }
                legend.style.marginLeft = '8px';
                legend.style.marginRight = '5px';
                li.appendChild(legend);
                $ulLegend.append(li)
            }
        }
        return styleCache[keyStyle];
    };
    /**
     * Localiza e adiciona uma animação no ponto informado
     * @param data - {id:1,bd:'salve']
     * @param center - true/false
     * @param pulsateCount - quantidade de piscadas
     */
    this.identifyPoint = function( data, center, pulsateCount){

        if ( self.animating ) {
            return;
        }

        // valores padrão
        pulsateCount = pulsateCount || 5;
        if( data && data.id && data.bd )  {
            if( data.bd != 'portalbio' ) {
                data.bd = data.bd = 'salve-consulta' ? 'salve' : data.bd;
            }
            center = (center == true ? true : false);
            self.animatedPoint = null;
            if ( data.id && data.bd ) {
                self.layers.map(function (layer, i) {
                    // procurar somente nos layer de ocorrencias
                    var options = layer.get('options');
                    if (options && options.type == 'geojson' && !self.animatedPoint) {
                        self.animatedPoint = layer.getSource().getFeatureById(data.id + data.bd.toLowerCase());
                        if (self.animatedPoint) {
                            self.findGridRow(data, false); // pintar a primeira coluna para identificar a linha do gride
                            if (!layer.getVisible()) {
                                layer.setVisible(true);
                                self.updateLayerSwitch();
                            }
                        }
                    }
                })
            }
        }
        if( self.animatedPoint )
        {
            self.animating = true;
            // verificar se o ponto está na área visível do mapa
            var mapExtent   = self.map.getView().calculateExtent( self.map.getSize() );
            var geomTemp     = self.animatedPoint.getGeometry();
            if ( !ol.extent.containsCoordinate(mapExtent, geomTemp.getCoordinates() ) )
            {
                if( ! center ) {
                    self.animating = false;
                    return;
                }
            }
            //console.log('Animar o ponto');
            var style, image, r, currR, maxR, sign;
            var pontoTemp = new ol.Feature({
                geometry: geomTemp,
                name: 'pulse-' + new Date()
            });
            self.drawLayer.getSource().addFeature(pontoTemp);
            pontoTemp.setStyle(new ol.style.Style({
                image: new ol.style.Circle({
                    radius: 5,
                    fill: new ol.style.Fill({
                        color: 'rgba(255,255,255,0)'
                    }),
                    stroke: new ol.style.Stroke({
                        color: 'rgba(255,0,0,0.6)',
                        width: 2
                    })
                })
            }));
            if ( center ) {
                document.location.href="#"+self.el
                self.map.getView().setCenter(geomTemp.getCoordinates());
            }
            image = pontoTemp.getStyle().getImage();
            style = pontoTemp.getStyle();
            image = style.getImage();
            r = image.getRadius();
            currR = r;
            maxR = 4 * r;
            sign = 1;
            var pulsate = function(event) {
                var vectorContext = event.vectorContext;
                if (currR > maxR) {
                    sign = -1;
                    pulsateCount--;
                } else if (currR < r) {
                    sign = 1;
                    if (!pulsateCount) {
                        self.map.un('postcompose', pulsate);
                        self.drawLayer.getSource().removeFeature(pontoTemp);
                        self.animating = false;
                        return;
                    }
                }
                currR += sign * 1.5;
                    vectorContext.drawFeature(pontoTemp, new ol.style.Style({
                    image: new ol.style.Circle({
                        radius: currR,
                        fill: image.getFill(),
                        stroke: image.getStroke()
                    })
                }));
                self.map.render();
            };
            self.map.on('postcompose', pulsate);
        }
    }

    /**
     * função para mostrar/esconder as linhas do grid em função do filtro atual
     */
    this.filterGrid = function(){
        var $grid;
        if( ! this.gridId ) {
            return;
        }
        $grid = $("#"+this.gridId.replace(/#/g,'') );
        if( $grid.length == 0 )
        {
            return;
        }
        //console.log('Atualizar linhas do gride '+this.gridId)
        //console.log( this.filteredIds)
        $grid.find('tr').map( function(i,tr){
            var id = $(tr).data('id');
            var bd = $(tr).data('bd');
            if( bd != 'portalbio' ) {
                bd = ( bd== 'salve-consulta' ? 'salve': bd );
            }
            if( id && bd ) {
                var valid = false;
                if( self.filteredIds.length == 0 )
                {
                    valid=true; // não tem filtro ativo
                }
                else {
                    valid = self.filteredIds.filter(function (item) {
                        return String(item.id) == String(id) && item.bd == bd;
                    }).length > 0;
                }
                if ( valid ) {
                    $(tr).show();
                }
                else {
                    $(tr).hide();
                }
            }
        })
    }

    /**
     * criar item do menu de contexto
     * @param featureType - Point, polygon ou vazio para todos
     * @param faIcon - nome do icone da fonte awesome. ex: fa-plus
     * @param text - texto do menu
     * @param iconColor - cor do icone
     * @param callback - função de callback
     */
    this.addContextMenuItem = function(featureType, faIcon, text, iconColor, callback, name, hint){
        var menuItem={};
        var menuIcon='';
        hint = hint ? hint : '';
        iconColor   = ( iconColor ? iconColor: '#0000ff');
        featureType = (featureType ? featureType : '');
        if( faIcon )
        {
            menuIcon = '<i style="color:'+iconColor+';" class="fa '+faIcon+'"></i>&nbsp;'
        }
        menuItem.text = '<span title="'+hint+'">'+menuIcon+text+'</span>';
        menuItem.callback = callback;
        menuItem.featureType = featureType;
        menuItem.name = name; // identificado do item do menu
        this.contextMenuItems.push( menuItem )
    }
    /**
     * Remover filtros e atualizar as camadas de ocorrencias
     */
    this.refresh = function() {
        self.filters = [];
        self.map.getLayers().getArray().map( function(layer){
            var options = layer.get('options');
            if( options && options.filter )
            {
                //console.log( 'refresh layer ' + layer.get('name'))
                layer.getSource().clear();
            }
        })
        self.updateFilter();
        // limpar os desenhos
        self.drawLayer.getSource().clear();
    }

    /**
     * excutar uma ação ajax
     * @param options
     */
    this.ajaxAction = function( options, callback ){
        options = options || {};
        if( ! self.urlAction)
        {
            return;
        }
        ajaxJson( self.urlAction,options,'Gravando...',function( res ) {
            callback && callback( res );
            setTimeout( function() {
                self.callbackActions && self.callbackActions( res, options);
            },2000);
        });
    }

    /**
     * localizar a linha do gride referente ao ponto clicado no mapa
     * @param params - {id:1,bd:'salve'}
     * @param scroll - true/false para posicionar o gride na área visivel da tela ao localizar a linha
     */
    this.findGridRow = function( params, scroll ){
        if( ! params || ! params.id )
        {
            return;
        }
        //console.log( 'FindGridRow chamada')
        //console.log( params );
        var $grid = $("#"+this.gridId.replace(/#/g,'') );
        if( $grid.length == 0  || $grid.find('tr').length == 1 )
        {
            self.alert('Gride ainda não foi visualizado!');
            return;
        }
        //console.log('Atualizar linhas do gride '+this.gridId)
        //console.log( this.filteredIds)
        var found=false;
        $grid.find('tr').map( function(i,tr) {
            var $tr = $(tr);
            // remover marcação anterior
            $tr.find('td:first').css('background-color','#fff');
            if( !found ) {
                var trData = $tr.data();
                if( trData && trData.bd )
                {
                    if( trData.bd != 'portalbio' ) {
                        trData.bd = trData.bd == 'salve-consulta' ? 'salve' : trData.bd;
                        trData.bd = trData.bd == 'consulta' ? 'salve' : trData.bd;
                    }
                }
                var idTr = ( trData.idOcorrencia ? trData.idOcorrencia : trData.id)
                if ( idTr == params.id && trData.bd == params.bd ) {
                    found = true;
                    //var idTr = $tr.prop('id');
                    if ( ! $tr.prop('id') ) {
                        idTr = 'tr-' + self.gridId + '-' + trData.id + '-' + trData.bd;
                        $tr.prop('id', idTr);
                    }
                    else
                    {
                        idTr = $tr.prop('id');
                    }
                    if( scroll ) {
                        document.location.href = "#" + idTr;
                        // retirar o item selecionado de baixo da toolbar
                        $('html').scrollTop($('html').scrollTop() - 166);
                    }

                    $tr.find('td:first').css('background-color','#d6e2ca');
                    var bgColor = $tr.css('background-color');
                    $tr.css('background-color','#a0d3e2');
                    //window.setTimeout(function() {
                    for(var i=0;i<4;i++) {
                        $tr.fadeIn(200).fadeOut(200).fadeIn(200);
                    }
                    $tr.css('background-color',bgColor);
                    //},700);


                }
            }
        });
    };

    /**
     * posicionar o mapa nas coordenadas aplicando efeito de animação
     * @param x
     * @param y
     */
    this.moveToXY = function(x,y){
        this.map.getView().animate({
            center: ol.proj.fromLonLat([x, y]),
            duration: 500,
            /*easing: function(t) {
                // An elastic easing method (from https://github.com/DmitryBaranovskiy/raphael).
                return Math.pow(2, -10 * t) * Math.sin((t - 0.075) * (2 * Math.PI) / 0.3) + 1;
            }*/
        });
    };

    /**
     * função para centralizar o mapa em um determinado ponto
     * @param x
     * @param y
     */
    this.centerMap = function( x, y , zoom){
        //zoom = (typeof(zoom) == 'undefined' ? 13 : zoom );
        var lonLat = ol.proj.fromLonLat([x, y]);
        //this.map.getView().setCenter(lonLat);
        this.moveToXY(x,y);
        if( ! typeof(zoom) == 'undefined' ) {
            this.map.getView().setZoom( parseInt( zoom ) );
        };
    };

    this.createMeasureTooltip = function( feature ) {
        if( !feature )
        {
            return;
        }
        var randomId = 'tooltip_'+this.uid()
        var div         = document.createElement('div');
        var divContent = document.createElement('span');
        var divClose   = document.createElement('span');
        divContent.innerHTML = '&nbsp;'
        divContent.className = 'content-tooltip'
        divContent.style.marginRight ='8px';
        divClose.className = 'close-tooltip';
        divClose.innerHTML = 'x';
        divClose.style.position = 'absolute';
        divClose.style.top = '-3px';
        divClose.style.color = '#fffebb';
        divClose.style.cursor = 'pointer';
        divClose.setAttribute('title','Fechar');
        divClose.addEventListener('click', function (e) {
            self.removeTooltip(randomId, feature );
        } );
        div.append(divContent);
        div.append(divClose);
        feature.getGeometry().measureTooltipElement = div;
        feature.getGeometry().measureTooltipElement.setAttribute('id',randomId);
        feature.getGeometry().measureTooltipElement.className = 'tooltip tooltip-measure';
        feature.getGeometry().measureTooltip        = new ol.Overlay({
            element: feature.getGeometry().measureTooltipElement,
            offset: [0, -15],
            positioning: 'bottom-center',
        });
        //feature.getGeometry().measureTooltip.geometry = feature.getGeometry();
        this.map.addOverlay( feature.getGeometry().measureTooltip );
    };

    /**
     * format length output
     * @param {ol.geom.LineString} line
     * @return {string}
     */
    this.formatLength = function(line) {
        var length;
        var wgs84Sphere = new ol.Sphere(6378137);

        // considerar a curvatura da terra
        if (true /*geodesicCheckbox.checked*/) {
            var coordinates = line.getCoordinates();
            length = 0;
            var sourceProj = this.map.getView().getProjection();
            for (var i = 0, ii = coordinates.length - 1; i < ii; ++i) {
                var c1 = ol.proj.transform(coordinates[i], sourceProj, 'EPSG:4326');
                var c2 = ol.proj.transform(coordinates[i + 1], sourceProj, 'EPSG:4326');
                length += wgs84Sphere.haversineDistance(c1, c2);
            }
        } else {
            length = Math.round(line.getLength() * 100) / 100;
        }
        var output;
        if (length > 100) {
            length = (Math.round(length / 1000 * 100) / 100);
            if( typeof( length.formatMoney ) != 'undefined') {
                output = length.formatMoney() + ' ' + 'km';
            } else {
                output = length +'  '+'km';
            }
        } else {
            length = (Math.round(length * 100) / 100);
            if( typeof( length.formatMoney ) != 'undefined') {
                output = length.formatMoney() + ' ' + 'm';
            } else {
                output = length +'  '+'km';
            }
        }
        return output;
    };


    /**
     * format length output
     * @param {ol.geom.Polygon} polygon
     * @return {string}
     */
    this.formatArea = function(polygon) {
        var area;
        var wgs84Sphere = new ol.Sphere(6378137);
        // considerar a curvatura da terra
        if (true /* geodesicCheckbox.checked*/ ) {
            var sourceProj = this.map.getView().getProjection();
            var geom = /** @type {ol.geom.Polygon} */(polygon.clone().transform(
                sourceProj, 'EPSG:4326'));
          area = 0;
          if (geom instanceof ol.geom.MultiPolygon) {
              geom.getPolygons().forEach( function( poligon ) {
                 area += Math.abs(wgs84Sphere.geodesicArea( poligon.getLinearRing(0).getCoordinates() ) );
              })
          }
          else {
              area = Math.abs(wgs84Sphere.geodesicArea( geom.getLinearRing(0).getCoordinates()  ));
          }
        } else {
            area = polygon.getArea();
        }
        var output;
        if ( area > 10000) {
            area = (Math.round(area / 1000000 * 100 ) / 100);
            if( typeof( length.formatMoney ) != 'undefined') {
                output = area.formatMoney(0) + ' ' + 'km<sup>2</sup>';
            } else {
                output = area + ' ' + 'km<sup>2</sup>';
            }
        } else {
            area =  (Math.round(area * 100) / 100 );
            if( typeof( length.formatMoney ) != 'undefined') {
                output = area.formatMoney(0) + ' ' + 'm<sup>2</sup>';
            } else {
                output = area + ' ' + 'm<sup>2</sup>';
            }
        }
        return output;
    };

    this.calcArea = function( feature, evt ){
      // adicionar tooltip no poligono
      if( ! feature.getGeometry().measureTooltipElement) {
          self.createMeasureTooltip(feature);
          feature.getGeometry().on('change', function (evt) {
              var geom = evt.target;
              var output;
              var tooltipCoord;
              if (geom instanceof ol.geom.Polygon) {
                  output = self.formatArea(geom);
                  /*try {
                      var turfPolygon = turf.polygon(geom.getCoordinates());
                      var bboxPolygon = turf.bbox(turfPolygon.geometry);
                      //tooltipCoord = ol.proj.fromLonLat([5.2, 52.25], "EPSG:3857")
                      tooltipCoord = ol.proj.fromLonLat([ bboxPolygon[0], bboxPolygon[1] ])
                      console.log( tooltipCoord);
                      console.log( bboxPolygon );
                  } catch(e) {
                      //tooltipCoord = geom.getInteriorPoint().getCoordinates();
                      tooltipCoord = geom.getCoordinates()[0][0];
                  }
                  */
                  // encontrar o ponto mais ao SUL
                  tooltipCoord = geom.getCoordinates()[0][0];
                  geom.getCoordinates()[0].forEach(function(coord ){
                      if( coord[1] > tooltipCoord[1] )
                      {
                          tooltipCoord = coord
                      }
                  });
              }
              else if (geom instanceof ol.geom.MultiPolygon) {
                  output = self.formatArea(geom);
                  tooltipCoord = geom.getPolygons()[0].getLastCoordinate();
              } else if (geom instanceof ol.geom.LineString) {
                  output = self.formatLength(geom);
                  tooltipCoord = geom.getLastCoordinate();
              }
              var divContent = geom.measureTooltipElement.getElementsByClassName('content-tooltip')
              if( divContent.length == 1 ) {
                  divContent[0].innerHTML = output
              } else {
                  geom.measureTooltipElement.innerHTML = output;
              }
              geom.measureTooltip.setPosition(tooltipCoord);
          });
          feature.getGeometry().changed(); // atualizar medida da área
      }
    };

    this.removeTooltip = function( id, feature ) {
        if( feature && feature.getGeometry() ) {
            feature.getGeometry().measureTooltip = null;
            feature.getGeometry().measureTooltipElement = null;
        }
        this.map.getOverlays().getArray().slice(0).forEach( function(overlay) {
            if( overlay.getElement().getAttribute('id') == id ) {
                self.map.removeOverlay( overlay );
            }
        });
    }

    this.toggleLayerSwitch = function( evt ) {
        var $lw = $("#layer-switch-"+this.el);
        if( $lw.length == 1)
        {
            var $i = $(evt.target); //$lw.find('[name=ol-layer-switch-title]').find('i');
            if( $i.hasClass('fa-chevron-down') ) // maximizar
            {
                $i.removeClass('fa-chevron-down').addClass('fa-chevron-up');
                $lw.css('height','auto');
            }
            else // minimizar
            {
                $i.removeClass('fa-chevron-up').addClass('fa-chevron-down');
                $lw.css('height','30px');
            }
        }
    };

    this.loadShpZip = function(options) {
       // var epsg = 4326;
       // var encoding ='UTF-8';
        var self=this;
        var reader;
        if( !self.file )
        {
            self.alert('Nenhum arquivo selecionado!');
            return;
        }

        if( self.file && /\.zip$/.test( self.file.name ) ) {
            var $form = $('#form-dlg-add-shp-' + self.el);
            var divId = 'dlg-add-shp-' + self.el;

            $form.find('.fa-spin').show();
            $form.find('[name=btnAdicionar]').hide();
            try {
                reader = new FileReader();
                reader.onload = function()
                {
                    if( reader.readyState == 2 ) {
                        shp( reader.result ).then( function( geojson ) {
                            self.file = null;
                            $form.find('.fa-spin').hide();
                            $form.find('[name=btnAdicionar]').show();
                            $form[0].reset();
                            $('#' + divId).hide();
                            self.createLayer({
                                type: 'geojson'
                                , name: options.layerName + ' (shp)'
                                , shp:true
                                , visible: true
                                , data: geojson
                                , zIndex: 2
                            });
                        },function() {
                            $form.find('.fa-spin').hide();
                            $form.find('[name=btnAdicionar]').show();
                            self.alert('Arquivo shapefile inválido.')

                        });
                    } else if( reader.error ) {
                        $form.find('.fa-spin').hide();
                        $form.find('[name=btnAdicionar]').show();
                        self.alert('Erro na leitura do arquivo shapefile.');
                    }
                };
                reader.readAsArrayBuffer(self.file);
/*
                loadshp({
                    url: self.file,
                    encoding: encoding,
                    EPSG: epsg
                }, function (data) {
                    if( data.error )
                    {
                        $form.find('.fa-spin').hide();
                        $form.find('[name=btnAdicionar]').show();
                        alert( data.error );
                    }
                    else {
                        self.file = null;
                        var URL = window.URL || window.webkitURL || window.mozURL || window.msURL;
                        URL.createObjectURL(new Blob([JSON.stringify(data)], {type: "application/json"}));
                        self.createLayer({
                            type: 'geojson'
                            , name: options.layerName + ' (shp)'
                            , shp:true
                            , visible: true
                            , data: data
                            , zIndex: 2
                            //, style: self.occurrenceStyle
                        });
                        // enquadrar o mapa no shapefile
                        var extent = ol.proj.transformExtent( data.bbox, "EPSG:4326", "EPSG:3857");
                        self.map.getView().fit(extent,self.map.getSize() );
                        self.map.getView().setZoom(self.map.getView().getZoom()-1)
                        $form.find('.fa-spin').hide();
                        $form.find('[name=btnAdicionar]').show();
                        $form[0].reset();
                        $('#' + divId).hide();
                    }
                });
                */
            } catch( e )
            {
                $form.find('.fa-spin').hide();
                $form.find('[name=btnAdicionar]').show();
                self.alert( e );
            }
        } else {
            $('.modal').modal('show');
        }
    };

    /**
     * método para validar coordenadas de pontos inválidos
     * ex: Infinity -Infinity, NaN
     */
    this.getGeojsonValidFeatures = function( geojson ) {
        try {
            var features = (new ol.format.GeoJSON()).readFeatures(geojson, {featureProjection: 'EPSG:3857'});
            features = features.filter(function (feature, i) {
                try {
                    var geometry = feature.getGeometry();
                    if (geometry.getType() == 'Point') {
                        var coords = geometry.getCoordinates();
                        if (isNaN(coords[1]) || isNaN(coords[0]) || Math.abs(coords[0]) === Infinity) {
                            return false;
                        } else {
                            return true;
                        }
                    } else {
                        return true;
                    }
                } catch (e) {
                    return false;
                }
            });
            return features;
        } catch (e) {
            return null;
        }
    };
    this.getTurfPoligonBrasil=function() {
        if( ! turfPoligonoBrasil ) {
            // formatador para geoJSON
            var format = new ol.format.GeoJSON();
            var brasil = format.readFeatures(brasilGeoJson)[0];
            //var brasil = format.readFeatures(brasilGeoJson2)[0];
            var geometry = brasil.getGeometry();
            if( geometry.getType() == 'MultiPolygon') {
                turfPoligonoBrasil = turf.polygon( geometry.getPolygons()[0].getCoordinates() )
                //turfPoligonoBrasil = turf.multiPolygon( geometry.getCoordinates() );
            } else if( geometry.getType() == 'Polygon') {
                turfPoligonoBrasil = turf.polygon( geometry.getCoordinates() );
            }
            // utilizar somente o primeiro poligono do mapa do brasil para o recorte
            //turfPoligonoBrasil = turf.polygon(geojson.geometry.coordinates[0]);
        }
        return turfPoligonoBrasil
    }

    this.getTurfPoligonZee = function() {
        if( ! turfPoligonoZee ) {
            var wkt = new ol.format.WKT();
            var feature = wkt.readFeature( zeePoligono, {
                dataProjection: 'EPSG:3426',
                featureProjection: 'EPSG:3426'
            });
            var geometry = feature.getGeometry();
            if( geometry.getType() == 'MultiPolygon') {
                turfPoligonoZee = turf.polygon( geometry.getPolygons()[0].getCoordinates() )
            } else if( geometry.getType() == 'Polygon') {
                turfPoligonoZee = turf.polygon( geometry.getCoordinates() );
            }
        }
        return turfPoligonoZee
    }

   /**
     * cria um patter de preenchimento com linhas diagonais na cor especificada
     */
    this.makePattern = function(color) {
        var cnv = document.createElement('canvas');
        var ctx = cnv.getContext('2d');
        cnv.width = 6;
        cnv.height = 6;
        ctx.fillStyle = color || 'rgb(255, 0, 0)';
        for(var i = 0; i < 6; ++i) {
            ctx.fillRect(i, i, 1, 1);
        }
        return ctx.createPattern(cnv, 'repeat');
    };

    this.sanitizeHtml = function( html ) {
        return String(html).replace(/<\/?[^>]+(>|$)/g, "");
    };

    /**
     * mostrar um wkt no mapa utilizando pelo layer informado.
     * Se não for informado o layer, será utilizado o drawLayer
     */
    this.drawWKT = function( userPolygonWkt, layer ) {
        var feature=null;
        try {
            wkt = new ol.format.WKT();
            feature = wkt.readFeature(userPolygonWkt, {
                dataProjection: 'EPSG:3857',
                featureProjection: 'EPSG:3857'
            });

            if ( feature  ) {
                //feature.getGeometry().transform('EPSG:4326','EPSG:3857')
                if( layer ) {
                    layer.getSource().addFeature(feature);
                } else {
                    this.drawLayer.getSource().addFeature(feature)
                }
            }
        } catch( e ) {
            console.log('Erro metodo ol3Map.drawWkt()');
            console.error( e );
        }
        return feature;
    }

    /**
     * criar wkt dos poligonos desenhados em uma camada
     */
    this.generateWKT = function( layer ) {
        var featureWkt, modifiedWkt;
        var unionFeatures = [];
        var wkt = new ol.format.WKT();
        var featuresFound=0;
        if( layer.getSource().getFeatures().length == 0 ) {
            return ''
        }
        layer.getSource().forEachFeature(function (f) {
            var featureClone = f.clone();
            featureClone.getGeometry().transform('EPSG:3857', 'EPSG:4326');
            featureWkt = wkt.writeFeature(featureClone);
            /*
            if(featureWkt.match(/MULTILINESTRING/g)) {
              modifiedWkt = (featureWkt.replace(/MULTILINESTRING/g, '')).slice(1, -1);
            } else {
              modifiedWkt = (featureWkt.replace(/,/g, ', ')).replace(/LINESTRING/g, '');
            }
            unionFeatures.push(modifiedWkt);
          });
          drawLayer.getSource().getFeatures().length ? $('#wkt').text('MULTILINESTRING(' + unionFeatures + ')') : $('#wkt').text('');
          */
            if (featureWkt.match(/MULTIPOLYGON/g)) {
                modifiedWkt = (featureWkt.replace(/MULTIPOLYGON/g, '')).slice(1, -1);
            } else {
                modifiedWkt = (featureWkt.replace(/,/g, ', ')).replace(/POLYGON/g, '');
            }
            unionFeatures.push(modifiedWkt);
            featuresFound++;
        });
        if( featuresFound==1) {
            return 'POLYGON' + unionFeatures[0]
        } else {
            return 'MULTIPOLYGON(' + unionFeatures.join(',') + ')'
        }

    }

    /**
     * recebe uma feature (polygon,line...) e retorna a feature (polygon)
     * do bbox desta feature
     */
    this.getFeatureBbox = function( feature ) {
        var bboxFeature=null;
        try {
            var bbox = feature.getGeometry().getExtent(); // ler a área do desenho especifico
            var start = [bbox[0], bbox[1]];
            var end = [bbox[2], bbox[3]];
            var geometry = new ol.geom.Polygon(null);
            geometry.setCoordinates([
                [start, [start[0], end[1]], end, [end[0], start[1]], start]
            ]);
            bboxFeature = new ol.Feature({geometry: geometry})
        } catch (e) {
            console.log('Erro ol3map.getFeatureBbox()');
            console.error(e);
        }
        return bboxFeature
    }

    // fazer uma animação
    this.flyTo = function( location, done ) {
        var duration = 1000;
        var view = this.map.getView();
        var zoom = view.getZoom();
        var parts = 2;
        var called = false;
        function callback(complete) {
            --parts;
            if (called) {
                return;
            }
            if (parts === 0 || !complete) {
                called = true;
                done( complete );
            }
        }

        view.animate({
            center: location,
            duration: duration
        }, callback);
        view.animate({
            zoom: zoom - 1,
            duration: duration / 2
        }, {
            zoom: zoom,
            duration: duration / 2
        }, callback);
    }


} // fim ol3Map
/**
 * função genérica para copiar um texto para a área de transferência
 * @param e
 * @param texto
 */
var olMapCopy2Clip = function(texto ){
    //var input = $(e).next('input');
    //console.log( input );
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val(texto).select();
    document.execCommand("copy");
    $temp.remove();
    var msg = 'Copiado para área de transferência.';
    if( typeof app != 'undefined' && typeof app.growl== 'function') {
        app.growl(msg, 1, '', 'tc','','success');
    }
    else
    {
        if( typeof notify == 'function')
        {
            notify( msg,null,'success',1000);
        }
        else
        {
            alert( msg );
        }
    }
};
var zeePoligono =""
