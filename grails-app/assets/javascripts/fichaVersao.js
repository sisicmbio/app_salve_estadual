//# sourceURL=fichaVersao.js
;
var fichaVersao = {
    panelData:{},
    $container:null,
    currentEditor:{instance:null,params:null,oldText:''},
    editorOptions:{},
    init:function() {
      // inicializar as veriaveis globais
      //fichaVersao.$container =  $("#pnlFichaVersao div#container-ficha-versao");
      //fichaVersao.panelData  = fichaVersao.$container.data();
      //var panelTitle = $("#pnlFichaVersao h3.jsPanel-title");
      //panelTitle.html( panelTitle.html() + ' ' + fichaVersao.panelData.nuVersao );
      fichaVersao.editorOptions = $.extend({}, default_editor_options,{
            height: 300,
            toolbar: 'save | ' + default_editor_options.toolbar,
            plugins: default_editor_options.plugins+',save',
            setup: function(ed) {
                ed.on('init', function() {
                fichaVersao.currentEditor.oldText = tinymce.activeEditor.getContent();
                })
            },
          /** save **/
          save_onsavecallback: function(editor) {
              // salvar texto editado
              fichaVersao.editorSave();
          },
        });

        // remover a ferramenta de cor do texto
        fichaVersao.editorOptions.plugins = fichaVersao.editorOptions.plugins.replace(/textcolor,?/,'');


        // criar o gride registros
        fichaVersao.updateGrideOcorrencias();


    },
    /**
     * remover a instância do editor
     */
    editorRemove:function(){
        if( fichaVersao.currentEditor && fichaVersao.currentEditor.instance ) {
            fichaVersao.currentEditor = {};
            tinymce.get(fichaVersao.editorOptions.selector.replace(/^#/, '')).destroy();
        }
    },
    /**
     * Gravar o texto alterado no banco de dados
     */
    editorSave: function() {
        //app.growl('Faltar implementar a ação salvar', 3, 'Editor Salvar', 'tc');
        fichaVersao.currentEditor.oldText = tinymce.activeEditor.getContent();
        var data = { sqFichaVersao : fichaVersao.panelData.sqFichaVersao,
            table:fichaVersao.currentEditor.params.table,
            column:fichaVersao.currentEditor.params.field,
            key:fichaVersao.currentEditor.params.key,
            value:tinymce.activeEditor.getContent()
        };
        app.ajax(baseUrl + 'fichaVersao/updateJson', data,
            function (res) {
                if (res.status == 0) {
                    fichaVersao.editorRemove();
                } else{
                    // manter o botão salvar ativado
                    tinymce.activeEditor.setDirty(true);
                }
            });
    },
    /**
     * Criar o editor de texto na tela
     * @param ele
     */
    showInlineEdit : function( ele ) {
        var ce = fichaVersao.currentEditor;
        if( ce.instance ) {
            // if ( tinymce.activeEditor.isDirty() ) {
            if ( fichaVersao.currentEditor.oldText != tinymce.activeEditor.getContent() ) {
                app.confirm('Texto foi alterado. Deseja gravar as alterções?',function(){
                    fichaVersao.editorSave();
                    fichaVersao.showInlineEdit(ele);
                },function(){
                    tinymce.activeEditor.setContent(fichaVersao.currentEditor.oldText);
                    fichaVersao.showInlineEdit(ele);
                });
                return;
            }
            fichaVersao.editorRemove();
            // se for o mesmo campo fechar o editor e retornar
            if( $(ele).data('target') == ce.params.target) {
                return;
            }
        }
        fichaVersao.currentEditor = {};
        fichaVersao.currentEditor.params = $(ele).data();
        $div = $("#"+fichaVersao.currentEditor.params.target+'Editor');
        if( $div.size() == 0 ) {
            fichaVersao.currentEditor = {}
            return;
        }
        fichaVersao.editorOptions.selector = "#"+fichaVersao.currentEditor.params.target+'Editor',
        fichaVersao.currentEditor.instance = tinymce.init(fichaVersao.editorOptions);
    },

    /**
     * Atualizar o gride de registros de ocorrência
     */
    updateGrideOcorrencias:function( ){
        var data = { sqFichaVersao : fichaVersao.panelData.sqFichaVersao}
        app.grid('GridOcorrenciasVersao',data,function(res){

        })
    },
}
