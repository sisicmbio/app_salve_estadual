//# sourceURL=/salve-estadual/assets/fichaCompleta.js

 /*
 configurações padrão do mapa de registro de ocorrências
render 'Id inválido!'
                    return */

 // variaveis globais
 var oMap = null;
 var contexto = '';
 var canModify = true;
 var updatingChat = false; // evitar chamadas simultaneas na atualização do chat
 var dlgJustificativa = null;
 var ultimaJustificativa = ''; // repetir o texto da justificativa para agilizar o preenchimento
 var tempParams = null;
  /*
 Configuração padrão do editor de texto tinymce
 */
 var fichaCompletaEditorOptions = $.extend({}, default_editor_options, {
     selector: '.inlineEdit',
     resize: true,
     autoresize_on_init: true,
     autoresize_overflow_padding: 1,
     autoresize_min_height: 400,
     autoresize_bottom_margin: 1,
     plugins: default_editor_options.plugins + ',save',
     toolbar: 'save | ' + default_editor_options.toolbar.replace(/ fullscreen \|/, ''),
     setup: function(editor) {
         editor.on("init", function () {
             // alterar a fonte padrão para Open Sans
             var text = editor.getContent()
             if( /Times New Roman/.test( text ) ) {
                 text = text.replace(/Times New Roman/g,'open_sansregular');
                 $(editor.targetElm).html(text);
                 editor.setContent(text, {format : 'html'});
             }
         });

         editor.on('click', function(editor) {
             //console.log(editor.id + ' - Editor was clicked');
         });
         editor.on('blur', function(evt) {
             if (evt.target.isDirty()) {
                 fichaCompletaEditorOptions.save_onsavecallback(evt.target);
             }
         });
         editor.on("paste", function(e) {
             // https://www.tinymce.com/docs/advanced/editor-command-identifiers/
             // padronizar fonte ao colar
             app.editorAfterPaste()
         });

         editor.on("keydown", function(event) {
             // tratamento para a tecla tab
             if (event.keyCode == 9) { // tab pressed
                 editor.execCommand('mceInsertContent', false, '&emsp;');
                 event.preventDefault();
                 event.stopPropagation();
                 return false;
             }
         });
     },

     /** save **/
     save_onsavecallback: function(editor) {
         // remover linhas em branco
         var valor = tinyMCE.trim(editor.getContent({
             format: 'html'
         }).replace(/<p>&nbsp;<\/p>/g, '').replace(/&nbsp;\s?/g, ' '));
         var data = $(editor.targetElm).data(); // ler todos os atributos data- da div

         // SALVE SEM CICLO
         if( data.campoFicha =='dsCitacao'){
             valor = tinyMCE.trim(editor.getContent({format: 'text'}))
         }

         data.dsTextoAlterado = valor;
         editor.setContent(valor);
         editor.save(); // alterar isDirty() para false
         app.ajax(baseUrl + 'fichaCompleta/updateTextoFicha', data, function(res) {
             /*if (tinyMCE.activeEditor.btn) {
                 $(tinyMCE.activeEditor.btn).show()
             }
             */
             fichaCompletaEditorOptions.configurarBotaoEditar();
             //tinyMCE.activeEditor.remove();
             //editor.focus();
         }, '', 'JSON');
     },
     configurarBotaoEditar: function() {
             if (tinyMCE.activeEditor.btn) {
                 var btn = $(tinyMCE.activeEditor.btn);
                 var data = btn.data();
                 if (data.active == 'S') {
                     btn.html('Editar');
                     btn.data('active', 'N');
                     btn.removeClass('btn-danger');
                     tinyMCE.activeEditor.remove();
                 } else {
                     btn.html('Fechar Editor');
                     btn.data('active', 'S');
                     btn.addClass('btn-danger');
                 }
             }
         }
         /** fim save **/
 });
 var exibirMapa = function(sqFicha, contexto) {
     divMap = 'div-mapa-ocorrencias-' + sqFicha;
     divGrid = 'div-gride-ocorrencias-' + sqFicha;

     var div = $("#" + divMap);
     if (div.size() == 0) {
         return
     }

     if (div.data('loaded') == 'S') {
         return;
     }
     div.data('loaded', 'S');
     // expander a div para a largura da tela
     div.css('width', '100%');
     // alterar os atributos width e height para o mapa funcionar
     div.width(div.innerWidth());
     div.height(div.innerHeight());
     // SALVE SEM CICLO
     var readOnly = div.data('readOnly') || !canModify;
     oMap = new Ol3Map({
         el: divMap,
         height: 670,
         gridId: divGrid,
         callbackActions: function(res, options) {
             if (options.action != '') {
                 var data = $.extend( { sqFicha:sqFicha,contexto:contexto}, options )
                 updateGridOcorrencias( data );
             }
         },
         readOnly: readOnly,
         showToolbar: true,
         sqFicha: sqFicha,
         contexto: contexto,
         urlAction: 'fichaCompleta/mapActions',
         urlOcorrencias: baseUrl + 'api/ocorrencias/'
     });
     oMap.run();


 };
 var centralizarMapa = function(x, y) {
     oMap.centerMap(x, y, 13);
 };

 var showFichaCompleta = function(sqFicha, noCientifico, verColaboracoes, contexto, onClose, canModify, sqOficina, sqOficinaFicha, sqFichaVersao) {
     if (typeof(event) != 'undefined') {
         event.preventDefault();
     }
     if (!sqFicha) {
         app.alertError('Id não informado!');
         return;
     }
     canModify = (canModify == false ? false : true);
     sqOficina = sqOficina || $("#gerenciarOficina #sqOficinaSelecionada").val();
     sqFichaVersao = sqFichaVersao || 0;
     contexto = contexto || 'consulta';
     verColaboracoes = verColaboracoes == true ? true : false;
     noCientifico = noCientifico || '';
     noCientifico = app.ellipseAutores(noCientifico);
     var data = {
         sqFicha: sqFicha,
         sqFichaVersao:sqFichaVersao,
         verColaboracoes: verColaboracoes,
         contexto: contexto
     };
     if (sqOficina) {
         data.sqOficina = sqOficina;
     }
     if (sqOficinaFicha) {
         data.sqOficinaFicha = sqOficinaFicha;
     }
     if (tinyMCE.activeEditor && tinyMCE.activeEditor.btn) {
         tinyMCE.activeEditor.btn = null;
     }
     data.canModify = canModify;
     //var win = app.panel('pnlShowFichaCompleta_' + sqFicha, 'Ficha da Espécie: ' + noCientifico, 'fichaCompleta/index', data, function() {
     // aumentar o espaço entre o autor e nome cientifico
     var titulo = ( contexto == 'validador' ? 'Validação ' + noCientifico : noCientifico);
     titulo = titulo.replace('<span>','<span class="ml15">');
     var win = app.panel('pnlShowFichaCompleta_' + sqFicha, titulo, 'fichaCompleta/index', data, function() {

         var sqFicha = data.sqFicha;

         $("#pnlShowFichaCompleta_" + sqFicha + "_body").css('overflow-x', 'auto');
         updatingChat = false;

         if (!sqFicha) {
             app.alertInfo('Id da ficha não informado.');
             return
         }
         // tratar eventos de abrir e fechar os tópicos ( accordion )
         $('#pnlShowFichaCompleta_' + sqFicha + ' .panel-colaboracao').on('hidden.bs.collapse', function(e) {
             var id = e.currentTarget.id;
             $("#" + id.replace(/panel-/, 'btn-')).hide();
         })

         $('#pnlShowFichaCompleta_' + sqFicha + ' .panel-colaboracao').on('show.bs.collapse', function(e) {
             var data = $(e.currentTarget).data();
             var id = e.currentTarget.id;
             var btnId = id.replace(/panel-/, 'btn-');
             if (data.editor == 'S') {
                 if (!tinyMCE.activeEditor || !tinyMCE.activeEditor.btn || tinyMCE.activeEditor.btn.id != btnId) {
                     $("#" + btnId).show();
                 }
             }
             if (data.urlImagem) {
                 var tagImg = $(e.currentTarget).find('img[src=""]');
                 if (tagImg.size() == 1) {
                     tagImg.attr('src', data.urlImagem);
                 }
             }
             if (data.mapa == 'S') {
                 // aguardar expander a div
                 window.setTimeout(function() {
                         exibirMapa(data.sqFicha, contexto);
                     }, 1500)
                     /*var div = $('#div-gride-ocorrencias-' + data.sqFicha);
                     if (div.size() == 1) {
                         div.html('<h4>Carregando registros...</h4>');
                         app.ajax(baseUrl + 'fichaCompleta/getGridOcorrencias', data, function(res) {
                             div.html(res);
                             updateAndamentoAvaliacaoFichaCompleta()
                         }, '', 'HTML');
                     }
                     */
             } else if (data.habitoAlimentar == 'S') {
                 var div = $('#div-gride-habito-alimentar-' + data.sqFicha);
                 div.html('<h4>Carregando tabela...</h4>');
                 app.ajax(baseUrl + 'fichaCompleta/getGridHabitoAlimentar', data, function(res) {
                     div.html(res);
                 }, '', 'HTML');
             } else if (data.habitat == 'S') {
                 var div = $('#div-gride-habitat-' + data.sqFicha);
                 div.html('<h4>Carregando tabela...</h4>');
                 app.ajax(baseUrl + 'fichaCompleta/getGridHabitat', data, function(res) {
                     div.html(res);
                 }, '', 'HTML');
             } else if (data.interacao == 'S') {
                 var div = $('#div-gride-interacao-' + data.sqFicha);
                 div.html('<h4>Carregando tabela...</h4>');
                 app.ajax(baseUrl + 'fichaCompleta/getGridInteracao', data, function(res) {
                     div.html(res);
                 }, '', 'HTML');
             }

         });

         $('#pnlShowFichaCompleta_' + sqFicha + ' .panel-grupo').on('show.bs.collapse', function(e) {
             var data = $(e.currentTarget).data();
             if (data.ameaca == 'S') {
                 if( !/imagem-mapa/.test(e.target.id) ) {
                     var divAmeacas = $('#div-gride-ameaca-' + data.sqFicha);
                     if( divAmeacas.find('table').size() == 0 ) {
                         divAmeacas.html('<h4>Carregando tabela...</h4>');
                         app.ajax(baseUrl + 'fichaCompleta/getGridAmeaca', data, function (res) {
                             divAmeacas.html(res);
                         }, '', 'HTML');
                     }

                     if( e.target.id =='panel-anexos-ameaca-body') {
                         //div = $('#div-gride-anexos-ameaca-' + data.sqFicha);
                         var divAmeacasAnexo = $(e.target);
                         if( divAmeacasAnexo.find('table').size() == 0 ) {
                             // carregar gride com os anexos
                             divAmeacasAnexo.html('<h4>Carregando anexos...</h4>');
                             app.ajax(baseUrl + 'fichaCompleta/getGridAnexosAmeaca', data, function (res) {
                                 divAmeacasAnexo.html( res );
                                 setTimeout(function(){
                                     bindDialogPreview(e.target.id);
                                 },1000);
                             }, '', 'HTML');
                         }
                     }
                 }

             }
             else if (data.uso == 'S')
             {
                 var divUsos = $('#div-gride-uso-' + data.sqFicha);
                 if( divUsos.find('table').size() == 0 ) {
                     divUsos.html('<h4>Carregando tabela...</h4>');
                     app.ajax(baseUrl + 'fichaCompleta/getGridUso', data, function (res) {
                         divUsos.html(res);
                     }, '', 'HTML');
                 }
                 if( e.target.id =='panel-anexos-usos-body') {
                     //div = $('#div-gride-anexos-ameaca-' + data.sqFicha);
                     var divAnexos = $(e.target);
                     if( divAnexos.find('table').size() == 0 ) {
                         // carregar gride com os anexos
                         divAnexos.html('<h4>Carregando anexos...</h4>');
                         app.ajax(baseUrl + 'fichaCompleta/getGridAnexosUso', data, function (res) {
                             divAnexos.html( res );
                             setTimeout(function(){
                                 bindDialogPreview(e.target.id);
                             },1000);
                         }, '', 'HTML');
                     }
                 }
             }
             else if (data.populacao == 'S') {
                 if( e.target.id =='panel-anexos-populacao-body') {
                     var divAnexos = $(e.target);
                     if( divAnexos.find('table').size() == 0 ) {
                         // carregar gride com os anexos
                         divAnexos.html('<h4>Carregando anexos...</h4>');
                         app.ajax(baseUrl + 'fichaCompleta/getGridAnexosPopulacao', data, function (res) {
                             divAnexos.html( res );
                             setTimeout(function(){
                                 bindDialogPreview(e.target.id);
                             },1000);
                         }, '', 'HTML');
                     }
                 }
             } else if (data.conservacao == 'S') {
                 app.ajax(baseUrl + 'fichaCompleta/getHistoricoAvaliacoes', data, function(res) {
                     var fieldset = $('#fieldset-ultima-avaliacao-nacional-' + data.sqFicha);
                     fieldset.find('div#avaliada').addClass('hidden');
                     fieldset.find('div#nao-avaliada').removeClass('hidden');
                     if (res.ultimaAvaliacaoNacional) {
                         fieldset.find('div#ano').html(res.ultimaAvaliacaoNacional.nuAnoAvaliacao);
                         fieldset.find('div#categoria').html(res.ultimaAvaliacaoNacional.deCategoriaIucn);
                         fieldset.find('div#criterio').html(res.ultimaAvaliacaoNacional.deCriterioAvaliacaoIucn);
                         fieldset.find('div#justificativa').html(res.ultimaAvaliacaoNacional.txJustificativaAvaliacao);
                         fieldset.find('div#avaliada').removeClass('hidden');
                         fieldset.find('div#nao-avaliada').addClass('hidden');
                     }
                     fieldset = $('#fieldset-historico-avaliacoes-' + data.sqFicha);
                     fieldset.find('div#gride-historico').html(res.gridHistorico);
                     fieldset = $('#div-outros-grides-' + data.sqFicha);
                     fieldset.find('div#gride-convencoes').html(res.gridConvencoes);
                     fieldset.find('div#gride-acoes').html(res.gridAcoes);
                     fieldset.find('div#gride-uc').html(res.gridUcs);
                 }, '', 'JSON');
             } else if (data.pesquisa == 'S') {
                 app.ajax(baseUrl + 'fichaCompleta/getGridPesquisa', data, function(res) {
                     var div = $('#panel-pesquisa-body');
                     div.find('div#gride-pesquisa').html(res);
                 }, '', 'HTML');
             } else if (data.refBib == 'S') {
                 app.ajax(baseUrl + 'fichaCompleta/getRefBibs', data, function(res) {
                     var div = $('#panel-ref-bib-body');
                     div.find('div#gride-ref-bib').html(res);
                 }, '', 'HTML');
             } else if (data.pendencia == 'S') {
                 app.ajax(baseUrl + 'fichaCompleta/getPendencias', data, function(res) {
                     var div = $('#panel-pendencia-body');
                     div.find('div#gride-pendencia').html(res);
                 }, '', 'HTML');
             } else if (data.anexosFicha == 'S') {
                 app.ajax(baseUrl + 'fichaCompleta/getAnexosFicha', data, function(res) {
                     var div = $('#panel-anexos-ficha-body');
                     div.find('div#gride-anexos-ficha').html(res);
                     setTimeout(function(){
                         // exibir mapa com o shapefile
                         bindDialogPreview('gride-anexos-ficha');
                     },1000);

                 }, '', 'HTML');
             } else if (data.fundamentacao == 'S') {
                 //console.log(data);
                 app.ajax(baseUrl + 'fichaCompleta/getInfoFundamentacao', data, function(res) {
                     $('#panel-fundamentacao-body').html(res);
                     window.setTimeout(function() {
                         $("#pnlShowFichaCompleta_" + data.sqFicha + "_body #panel-fundamentacao")[0].scrollIntoView(true);
                     }, 800);
                 }, '', 'HTML');
             }

         });
         //win.setTheme('#3a7c59');
         win.setTheme('#234734');
         data.onClose = onClose;
         win.data = data;
         window.setTimeout(function() {
             //updateGridMotivoMudanca({sqFicha : sqFicha});
             updateAndamentoAvaliacaoFichaCompleta();
             // ajustar o formulário da situacao regional
             app.categoriaIucnChange({ container: 'frmAvaliacaoRegional', sqFicha: sqFicha });
             updateGridOcorrencias({ sqFicha: sqFicha, contexto: contexto });

             if (data.contexto == 'oficina') {
                 app.categoriaIucnChange({ container: 'divResultadoOficina', sqFicha: sqFicha });
             } else if (data.contexto == 'validacao') {
                 app.categoriaIucnChange({ container: 'divResultadoOficina', sqFicha: sqFicha });
             } else if (data.contexto == 'validador') {
                 app.categoriaIucnChange({ container: 'divResultadoOficina', sqFicha: sqFicha });
                 /**
                  * configurar layout do formulário de validação
                  */
                 initFormValidacao(data);
             }
             // atualizar o gride de motivos de mudanca de categoria da avaliação final se houver
             if ($("#divContainerMotivoMudancaFinal" + sqFicha).size() == 1) {
                 app.updateGridMotivoMudancaCategoria({ container: 'divContainerMotivoMudancaFinal' + sqFicha, sqFicha: sqFicha });
             }

             updateGridAvaliacaoRegional({
                 sqFicha: data.sqFicha,
                 contexto: 'frmAvaliacaoRegional'
             });
             bindDialogPreview('#pnlShowFichaCompleta_' + sqFicha)
         }, 300);

     }, null, null, true /*modal*/ , true /*maximized*/ , fichaCompletaClose);
     // }, null, null, true /*modal*/ , false /*maximized*/ , onClose);

 };

 // callback da ficha completa ao fechar
 var fichaCompletaClose = function(panel) {
     ultimaJustificativa = '';
     // limpar campos da justificativa de não utilização de algum ponto na avaliação
     if (typeof dlgJustificativa == 'object') {
         try {
             $("div#" + dlgJustificativa.getId()).find('textarea').text('');
             $("div#" + dlgJustificativa.getId()).find('div.div-justificativa-log').html('');
         } catch (e) {}
     };

     if (panel.data && panel.data.sqFicha) {
         var sqFicha = panel.data.sqFicha;
         tinymce.remove("#pnlShowFichaCompleta_" + sqFicha + " #dsJustificativa");
         tinymce.remove("#pnlShowFichaCompleta_" + sqFicha + " #dsJustMudancaCategoria");
         tinymce.remove("#pnlShowFichaCompleta_" + sqFicha + " #txJustificativaAvaliacaoAR");
     };

     if (panel.data.onClose) {
         var cb = panel.data.onClose;
         delete panel.data.onClose;
         panel.data.modalClosed = true; // flag para indicar a função de callback que a mesma foi chamada ao fechar a janela modal
         app.eval(cb, panel.data);
     };
 };

 /**
  * função utilizada para atualizar o total de colaborações / total concluidas nos tópicos da ficha completa
  * Para que a pendência seja ignorada basta remover do select o atributo: data-campo-ficha
  */
 var updateAndamentoAvaliacaoFichaCompleta = function() {
     var subGrupo = {};
     var grupo = {};
     $("select[data-campo-ficha]").each(function(i, obj) {
         var obj = $(obj);
         var data = obj.closest('tr').data();
         if (data.fonte == 'consulta') { // contabilizar somente colaborações do módulo consulta
             var codigo = obj.find('option:selected').data('codigo');
             var aceita = true;
             if (data.contexto == 'consulta') {
                 aceita = (codigo != 'NAO_AVALIADA' && codigo != 'PENDENTE_APROVACAO' && codigo != '');
             } else {
                 aceita = (codigo != 'NAO_AVALIADA' && codigo != 'RESOLVER_OFICINA' && codigo != '');
             };
             var campoFicha = obj.data('campoFicha');
             var nomeGrupo = obj.data('grupo');
             if (!subGrupo[campoFicha]) {
                 subGrupo[campoFicha] = {
                     total: 0,
                     aceito: 0,
                 };
                 if (typeof grupo[nomeGrupo] == 'undefined') {
                     grupo[nomeGrupo] = {
                         total: 0,
                         aceito: 0,
                     }
                 }
             };
             subGrupo[campoFicha].total++;
             grupo[nomeGrupo].total++;
             if (aceita) {
                 subGrupo[campoFicha].aceito++;
                 grupo[nomeGrupo].aceito++;
             }
         }
     });



     // contabilizar validação do gride de registros de ocorrencia
     $("#container-ficha-completa span.revisor").each(function(i, obj) {
         obj = $(obj);
         var campoFicha = obj.data('campoFicha');
         var nomeGrupo = obj.data('grupo');
         if (!subGrupo[campoFicha]) {
             subGrupo[campoFicha] = {
                 total: 0,
                 aceito: 0
             }
         }
         if (typeof grupo[nomeGrupo] == 'undefined') {
             grupo[nomeGrupo] = {
                 total: 0,
                 aceito: 0
             }
         }
         var aceita = obj.hasClass('colaboracao-avaliada');
         subGrupo[campoFicha].total++;
         grupo[nomeGrupo].total++;
         if (aceita) {
             subGrupo[campoFicha].aceito++;
             grupo[nomeGrupo].aceito++;
         };
     });

     var cor;
     var ele;
     for (var key in grupo) {
         ele = '#status-' + key;
         $(ele).html(grupo[key].aceito + '/' + grupo[key].total);
         cor = (grupo[key].aceito == grupo[key].total) ? 'green' : 'red';
         if (cor == 'green') {
             $(ele).html('').removeClass('red green').addClass(cor + ' glyphicon glyphicon-ok');
         } else {
             $(ele).html(grupo[key].aceito + '/' + grupo[key].total).removeClass('red green glyphicon glyphicon-ok').addClass(cor);
         };
     };
     for (var key in subGrupo) {
         ele = '#sub-status-' + key;
         cor = (subGrupo[key].aceito == subGrupo[key].total) ? 'green' : 'red';
         $(ele).html(subGrupo[key].aceito + '/' + subGrupo[key].total).removeClass('red green').addClass(cor);
     };
 };

 /**
  * botão para somente fechar a ficha completa
  * @param params
  */
 var salvarRevisaoFechar = function(params) {
     if (!params.sqFicha) {
         return;
     };
     app.closeWindow('pnlShowFichaCompleta_' + params.sqFicha)
 };


 var consolidarFicha = function(params) {
     if (!params.sqFicha) {
         return;
     }
     var qtdPendencia = countColaboracaoPendente(params);
     var qtdMultimidiasPendentes = 0;

     /*
     var qtdMultimidiasPendentes = countColaboracaoMultimidiaPendente(params);
     // regra desabilitada - A ficha pode ser consolidade com foto pendente
     if (qtdMultimidiasPendentes > 0) {
         app.alertInfo('<br>' + (qtdMultimidiasPendentes == 1 ? 'Existe <b>' + qtdMultimidiasPendentes + '</b> foto pendente de avaliação!' : 'Existem <b>' + qtdMultimidiasPendentes + '</b> fotos pendentes de avaliação!'), 'Verificação');
         return;
     }*/

     if (qtdPendencia > 0) {
         app.alertInfo('<br>' + (qtdPendencia == 1 ? 'Existe <b>' + qtdPendencia + '</b> colaboração pendente de avaliação!' : 'Existem <b>' + qtdPendencia + '</b> colaborações pendentes de avaliação!'), 'Verificação');
         return;
     }

     var sqFicha;
     var data = {};
     app.confirm('<br>Confirma gravação?',
         function() {

             //var sqFicha = $("#container-ficha-colaboracoes #sqFicha").val();
             app.ajax(baseUrl + 'fichaCompleta/consolidarFicha', params, function(res) {
                 if (res.status == 0) {
                     if (res.descricao) {
                         var consolidada = $("#td_situacao_" + params.sqFicha + " select option[data-codigo=CONSOLIDADA]").val();
                         $("#td_situacao_" + params.sqFicha + ' select').val(consolidada);
                         var btn = $("#pnlShowFichaCompleta_" + params.sqFicha + " a.btn[data-action=consolidarFicha]");
                         btn.parent().html('<h3 style="color:#006400">Situação: ' + res.descricao + '</h3>');
                     }
                 }
             }, null, 'json');

         });
 }

 // Oficina
 var sqCategoriaIucnChange = function(params) {
     if (!params.sqFicha) {
         return;
     }
     var contexto = '#frmResultadoOficina ';
     // mostrar/esconder campos
     app.categoriaIucnChange({ container: contexto });

     return; // testar se precisa do codigo abaixo

     var sqCategoriaUltimaAvaliacao = $(contexto + "#sqCategoriaIucnUltimaAvaliacao").val();
     var sqCategoriaIucn = $(contexto + "#sqCategoriaIucn").val();
     var $divDadosMotivoMudanca = $("#divDadosMotivoMudanca" + params.sqFicha);

     if (!sqCategoriaUltimaAvaliacao || !sqCategoriaIucn || sqCategoriaIucn == sqCategoriaUltimaAvaliacao) {
         $divDadosMotivoMudanca.hide();
     } else {
         // se a categoria anterior for NE não exibir os campos da mudança de categoria
         var codigoSistema = $("#frmResultadoOficina #coCategoriaIucnUltimaAvaliacao").val();
         if (codigoSistema != 'NE') {
             $divDadosMotivoMudanca.show();
         }
     }
     if (!sqCategoriaUltimaAvaliacao || sqCategoriaIucn == sqCategoriaUltimaAvaliacao) {
         $(contexto + "#btnRepetirUltimaAvaliacao").hide();
     } else {
         $(contexto + "#btnRepetirUltimaAvaliacao").show();
     }

     /*// se a categoria for 'VU,CR,EN não exibir o campo critério'
     if (!/EN|VU|CR/.test($(contexto + "#sqCategoriaIucn option:selected").data('codigo'))) {
         $(contexto + "#dsCriterioAvalIucn").parent().addClass('hide');
     } else {
         $(contexto + "#dsCriterioAvalIucn").parent().removeClass('hide');
     }
     if (/CR/.test($(contexto + "#sqCategoriaIucn option:selected").data('codigo'))) {
         $(contexto + "#stPossivelmenteExtinta").closest('div').removeClass('hide');
     } else {
         $(contexto + "#stPossivelmenteExtinta").closest('div').addClass('hide');
         $(contexto + "#stPossivelmenteExtinta").prop('checked',false);
     }
     */

 }

 var sqCategoriaSugeridaChange = function(params) {
     contexto = "#frmRespostaValidador" + params.sqFicha + ' ';
     // se a categoria for 'VU,CR,EN não exibir o campo critério'
     if (!/EN|VU|CR/.test($(contexto + "#sqCategoriaSugerida option:selected").data('codigo'))) {
         $(contexto + "#deCriterioSugerido").parent().addClass('hide');
     } else {
         $(contexto + "#deCriterioSugerido").parent().removeClass('hide');
         if ($(contexto + "#sqCategoriaSugerida option:selected").data('codigo') == 'CR') {
             $(contexto + "#stPossivelmenteExtinta").closest('label').removeClass('hide');
         } else {
             $(contexto + "#stPossivelmenteExtinta").closest('label').addClass('hide');
         }
     }
 }

 /*
 var saveAvaliadaRevisada = function( params )
 {
     var qtdPendencia = countColaboracaoPendente();
     if (qtdPendencia > 0)
     {
         app.alertInfo('<br>' + (qtdPendencia == 1 ? 'Existe <b>' + qtdPendencia + '</b> colaboração pendente de avaliação!' : 'Existem <b>' + qtdPendencia + '</b> colaborações pendentes de avaliação!'), 'Verificação');
         return;
     }
     var data = params;
     data.sqOficina = $("#container-ficha-completa #sqOficina").val();
     log( data )
     app.confirm('<p style="color:red;font-size:16px;font-weight: bold;">Atenção,<br>fichas na situação Analisada/Revisada encontram-se automaticamente prontas para Validação.<p><br>Confirma Gravação? ',
         function ()
         {
             app.ajax(baseUrl + 'fichaCompleta/saveAvaliadaRevisada',
                 data,
                 function (res)
                 {
                     if (res.status == 0)
                     {
                     }
                 }, 'Salvando Resultado...', 'JSON');
         });
 }
 */

/**
 * função para salvar apenas o texto da justificativa durante a oficina ( Avaliação )
 * @param params
 */
var saveJustificativa = function( params ) {
    var data = app.params2data(params,{});

    var editor =  tinyMCE.get('dsJustificativa' + params.sqFicha);
    if (!params.sqFicha) {
        app.alertInfo('Ficha não informada.');
        return;
    }
    if (!params.sqOficinaFicha) {
        app.alertInfo('OficinaFicha não informada.');
        return;
    }
    editor.save();
    data.dsJustificativa = corrigirCategoriasTexto(trimEditor( editor.getContent() ));
    editor.setContent(data.dsJustificativa);
    app.ajax(app.url + 'fichaCompleta/saveJustificativaOficina',
            data,
            function(res) {
    });
}

 /**
  * A ação de gravar o encerramento da oficina ou gravar como Avaliada/Revisada é a mesma, alterando
  * apenas a situação final da ficha
  * @param params
  */
 var saveResultado = function(params) {
     var data = {};
     if (!params.container) {
         app.alertInfo('saveResultado: container não informado.');
         return;
     }
     if (!params.sqFicha) {
         app.alertInfo('saveResultado: Ficha não informada.');
         return;
     }
     if (!params.sqOficinaFicha) {
         app.alertInfo('saveResultado: OficinaFicha não informada.');
         return;
     }
     tinyMCE.triggerSave(); // atualizar as textareas com o valor do editor

     // ajustar o id do container
     params.container = '#' + params.container.replace(/^#/, '');
     var $container = $(params.container);
     var $divDadosMotivoMudanca = $container.find("#divDadosMotivoMudanca" + params.sqFicha);
     var $dsCriterioAvalIucn = $container.find('#dsCriterioAvalIucn');
     var $stPossivelmenteExtinta = $container.find('#stPossivelmenteExtinta');
     var qtdPendencia = countColaboracaoPendente(params);

    // verificar se existem alguma colaboração sem confirmação
     if (qtdPendencia > 0) {
         app.alertInfo('<br>' + (qtdPendencia == 1 ? 'Existe <b>' + qtdPendencia + '</b> colaboração pendente de avaliação!' : 'Existem <b>' + qtdPendencia + '</b> colaborações pendentes de avaliação!'), 'Verificação');
         return;
     } else {
         // verficar se tem alguma contribuição nos registros que está para resolver em oficina
         qtdPendencia = $('#tableOcorrenciasFichaCompleta-'+params.sqFicha).find('input[type=radio][data-value!=ACEITA]:checked').size();
         if (qtdPendencia > 0) {
             app.alertInfo('<br>' + (qtdPendencia == 1 ? 'Existe <b>' + qtdPendencia + '</b> colaboração nos registros para resolver em oficina!' : 'Existem <b>' + qtdPendencia + '</b> colaborações nos registros para resolver em oficina.'), 'Verificação');
             return;
         }

     }

     // ler valores dos campos
     data.sqFicha = params.sqFicha;
     data.sqOficinaFicha = params.sqOficinaFicha;
     data.cdCategoriaIucn = $container.find("select[name=sqCategoriaIucn] option:selected").data('codigo');
     data.sqCategoriaIucn = $container.find("select[name=sqCategoriaIucn] option:selected").val();
     data.dsCriterioAvalIucn = $container.find("input[name=dsCriterioAvalIucn]").val();
     data.deCategoriaOutra = $container.find("input[name=deCategoriaOutra]").val();
     data.stPossivelmenteExtinta = $container.find("input[name=stPossivelmenteExtinta]").is(':checked') ? 'S' : 'N';

     data.dsJustificativa = corrigirCategoriasTexto(trimEditor(tinyMCE.get('dsJustificativa' + params.sqFicha).getContent()));
     tinyMCE.get('dsJustificativa' + params.sqFicha).setContent(data.dsJustificativa);

     data.dsJustMudancaCategoria = '';
     if ( $container.find('#dsJustMudancaCategoria' + params.sqFicha).size() == 1 && $container.find('#divDadosMotivoMudanca'+params.sqFicha).is(':visible')) {
         data.dsJustMudancaCategoria = corrigirCategoriasTexto(trimEditor( tinyMCE.get('dsJustMudancaCategoria' + params.sqFicha).getContent()));
         tinyMCE.get('dsJustMudancaCategoria' + params.sqFicha).setContent(data.dsJustMudancaCategoria);
     }
     data.avaliadaRevisada = (params.avaliadaRevisada == 'S' ? 'S' : 'N');
     // informações do AJUSTE REGIONAL
     data.sqTipoConectividade = '';
     data.sqTendenciaImigracao = '';
     data.stDeclinioBrPopulExterior = '';
     data.dsConectividadePopExterior = '';
     if ($container.find("select[name=sqTendenciaImigracao]").size() == 1) {
         data.sqTipoConectividade = $container.find("select[name=sqTipoConectividade] option:selected").val();
         data.sqTendenciaImigracao = $container.find("select[name=sqTendenciaImigracao] option:selected").val();
         data.stDeclinioBrPopulExterior = $container.find("select[name=stDeclinioBrPopulExterior] option:selected").val();
         data.dsConectividadePopExterior = trimEditor(tinyMCE.get('dsConectividadePopExterior' + params.sqFicha).getContent());
     }

     if (data.sqCategoriaIucn) {
         // validar o critério
         if ($dsCriterioAvalIucn.is(':visible')) {
             if (!data.dsCriterioAvalIucn) {
                 app.alertInfo('Necessário informar o critério!');
                 return;
             }
         } else {
             data.dsCriterioAvalIucn = '';
         }

         // validar possivelmente extinta
         if (!$stPossivelmenteExtinta.is(':visible')) {
             data.stPossivelmenteExtinta = '';
         }

         if (!data.dsJustificativa && data.cdCategoriaIucn != 'NE') {
             app.alertInfo('Necessário informar a justificativa!');
             return;
         }

         // validar mudanca de categoria
         if ($divDadosMotivoMudanca.is(':visible')) {

             if ($divDadosMotivoMudanca.find('#divGridMotivoMudancaCategoria tbody>tr').size() == 0) {
                 app.alertInfo('Motivo da mudança deve ser informado quando houver mudança de categoria.');
                 return;
             }
             // se a categoria mudou de CR,EN ou VU para qualquer outra diferente destas 3 a justificava
             // deve ser obrigatoria
             var cdCategoriaIucnUltimaAvaliacao = $container.find("input[name=cdCategoriaIucnUltimaAvaliacao]").val();
             if (cdCategoriaIucnUltimaAvaliacao && cdCategoriaIucnUltimaAvaliacao.match(/CR|EN|VU/) &&
                 !data.cdCategoriaIucn.match(/CR|EN|VU/) && $('<p>' + data.dsJustMudancaCategoria + '</p>').text().trim() == '') {
                 app.alertInfo('Quando houver mudança de categoria AMEAÇADA para NÃO AMEAÇADA a justificativa é obrigatória.');
                 return;
             }

         } else {
             data.dsJustMudancaCategoria = '';
         }
     }

     var msg = 'Confirma gravação da avaliação?';
     if (params.avaliadaRevisada == 'S') {
         // tem que ter categoria selecionada
         if (!data.sqCategoriaIucn) {
             app.alertInfo('Selecione a categoria!');
             return;
         }
         msg = '<p style="color:red;font-size:16px;font-weight: bold;">Atenção,<br>fichas na situação Avaliada/Revisada encontram-se automaticamente prontas para Validação.<p><br><h3>Confirma Gravação?</h3>'
     }
     // gravar resultado da Avaliação ABA 11.6
     app.confirm(msg,
         function() {
             app.ajax(app.url + 'fichaCompleta/saveResultadoAvaliacao',
                 data,
                 function(res) {});
         },
         function() {

         }, params, 'Confirmação', 'warning');

 };

 /**
  * quando a oficina tiver encerrada, os dados do ajuste regional poderão ser
  * alterados isoladamente do resultado da Avaliação
  * @param params
  */
 var saveAjusteRegional = function(params) {
     var data = {};
     if (!params.container) {
         app.alertInfo('saveAjusteRegional: container não informado.');
         return;
     }
     if (!params.sqFicha) {
         app.alertInfo('saveAjusteRegional: Ficha não informada.');
         return;
     }
     if (!params.sqOficinaFicha) {
         app.alertInfo('saveAjusteRegional: OficinaFicha não informada.');
         return;
     }
     tinyMCE.triggerSave(); // atualizar as textareas com o valor do editor

     params.container = '#' + params.container.replace(/^#/, '');
     var $container = $(params.container);

     // ler os campos do ajuste regional
     data.sqFicha = params.sqFicha;
     data.sqOficinaFicha = params.sqOficinaFicha;
     // informações do AJUSTE REGIONAL
     data.sqTipoConectividade = '';
     data.sqTendenciaImigracao = '';
     data.stDeclinioBrPopulExterior = '';
     data.dsConectividadePopExterior = '';
     if ($container.find("select[name=sqTendenciaImigracao]").size() == 1) {
         data.sqTipoConectividade = $container.find("select[name=sqTipoConectividade] option:selected").val();
         data.sqTendenciaImigracao = $container.find("select[name=sqTendenciaImigracao] option:selected").val();
         data.stDeclinioBrPopulExterior = $container.find("select[name=stDeclinioBrPopulExterior] option:selected").val();
         data.dsConectividadePopExterior = trimEditor(tinyMCE.get('dsConectividadePopExterior' + params.sqFicha).getContent());
     }
     //console.table( data );
     // gravar resultado da Avaliação ABA 11.6
     app.confirm('Confirma gravação do ajuste regional?',
         function() {
             app.ajax(app.url + 'fichaCompleta/saveAjusteRegional',
                 data,
                 function(res) {});
         },
         function() {

         }, params, 'Confirmação', 'warning');


 };

 var updateGridMotivoMudanca = function(params) {
     var data = {};
     var $divMotivoMudanca;


     // na app.js tem a funcao app.updateGridMotivoMudancaCategoria({sqFicha:data.sqFicha})
     return; // verificar se ainda precisa, poque no app.categoriaIucnChange já está chamando

     if (!params.sqFicha) {
         app.alertInfo('updateGridMotivoMudanca: Id da ficha não informado!');
         return;
     }
     $divMotivoMudanca = $("#divDadosMotivoMudanca" + params.sqFicha);
     if ($divMotivoMudanca.size() == 0) {
         app.alertInfo('updateGridMotivoMudanca: Gride não definido.');
         return;
     }
     data.sqFicha = params.sqFicha;
     app.ajax(baseUrl + 'fichaCompleta/getGridMotivoMudanca',
         data,
         function(res) {
             $divMotivoMudanca.find("#divGridMotivoMudancaCategoria").html(res);
         }, '', 'HTML');

     /*
     params.contexto = params.contexto || 'frmResultadoOficina';
     if( params.contexto == 'fundamentacao' )
     {
        params.contexto = '#divDadosMotivoMudanca' + params.sqFicha+' ';
        data.sqFicha = params.sqFicha;
        data.canModify = $('#sqMotivoMudanca'+params.sqFicha).is(':visible');
        data.
     }
     else {
         params.contexto = "#" + params.contexto.replace(/^#/, '') + ' ';
         var data = {
             sqFicha: $(params.contexto + "#sqFicha").val()
         };
         if (!data.sqFicha) {
             data.sqFicha = $("#sqFicha").val();
         }
         ;
         if (!data.sqFicha && params.sqFicha) {
             data.sqFicha = params.sqFicha;
         };
         data.canModify = $(params.contexto + ' #sqMotivoMudanca').is(':visible');
     }
     app.ajax(baseUrl + 'fichaCompleta/getGridMotivoMudanca',
         data,
         function(res) {
             $(params.contexto + "#divGridMotivoMudancaCategoria").html(res);
         }, '', 'HTML');
         */
 };


 var updateGridOcorrencias = function(params) {
     if (!params.sqFicha) {
         return;
     }
     var $div = $('#div-gride-ocorrencias-' + params.sqFicha);
     if ($div.size() == 1) {
         $div.html('<h4>Carregando registros...</h4>');
         var data = $.extend({},params,$div.data());
         app.ajax(baseUrl + 'fichaCompleta/getGridOcorrencias', data, function(res) {
             $div.html(res);
             updateAndamentoAvaliacaoFichaCompleta();
             bindDialogPreview($div.attr('id'));
             if( params.id && params.bd ) {
                 setTimeout( function() {
                     if( oMap ) {
                         oMap.findGridRow(params, params.scroll, false);
                     }
                 },1000);
             }


             /* deabiltado Datatable
             window.setTimeout(function(){
                 div.find('table').DataTable( $.extend({},default_data_tables_options,
                     {
                         // desabilitar ordenação nas colunas
                         "columnDefs" : [
                             {"orderable": false, "targets": [1,10]}
                         ]
                     })
                 );
             },1000);
             */
         }, '', 'HTML');
     }
 };

 var addMotivoMudanca = function(params) {
     if (!params.sqFicha) {
         app.alertError('ID da ficha não encontrado!');
         return;
     }
     if (!params.sqMotivoMudanca) {
         app.alertInfo('Selecione o motivo da mudança!');
         return;
     }
     app.ajax(baseUrl + 'fichaCompleta/addMotivoMudanca', params,
         function(res) {
             if (res.status === 0) {
                 resetDadosMotivoMudanca(params);
                 app.updateGridMotivoMudancaCategoria(params);
             }
         }, null, 'json'
     );
 };

 var resetDadosMotivoMudanca = function(params) {

     if (!params.sqFicha) {
         return;
     }
     app.reset("#divDadosMotivoMudanca" + params.sqFicha, 'dsJustMudancaCategoria');
     app.focus("sqMotivoMudanca", "divDadosMotivoMudanca" + params.sqFicha);
     app.unselectGridRow('divGridMotivoMudancaCategoria');
     return;

     /*params.contexto = params.contexto || 'frmResultadoOficina';
     if( params.contexto == 'fundamentacao')
     {
         app.reset("#divDadosMotivoMudanca"+params.sqFicha,'dsJustMudancaCategoria'+params.sqFicha);
         app.focus('sqMotivoMudanca'+params.sqFicha);
     }
     else {
         params.contexto = params.contexto.trim() + ' ';
         app.reset('divDadosMotivoMudanca', 'dsJustMudancaCategoria');
         app.focus('sqMotivoMudanca');
         $(params.contexto + "#btnResetMotivoMudanca").addClass('hide');
         $(params.contexto + "#btnSaveMotivoMudanca").html($("#btnSaveMotivoMudanca").data('value'));
         app.unselectGridRow('divGridMotivoMudancaCategoria');
     }
     */
 };

 var editMotivoMudanca = function(params, btn) {
     app.ajax(baseUrl + 'fichaCompleta/editMotivoMudanca', params,
         function(res) {
             var contexto = '#frmFesultadoOficina '
             $(contexto + "#btnResetMotivoMudanca").removeClass('hide');
             $(contexto + "#btnSaveMotivoMudanca").data('value', $(contexto + "#btnSaveMotivoMudanca").html());
             $(contexto + "#btnSaveMotivoMudanca").html("Gravar Alteração");
             app.setFormFields(res, 'divDadosMotivoMudanca');
             app.selectGridRow(btn);
         });
 };

 var deleteMotivoMudanca = function(params, btn) {
     if (!params.id) {
         app.alertInfo('deleteMotivoMudanca: id do motivo da mudança não informada.');
         return;
     }
     if (!params.sqFicha) {
         app.alertInfo('deleteMotivoMudanca: ficha não informada.');
         return;
     }
     // deve haver pelo menos um motivo da mudanca
     var $divMotivoMudanca = $("#divDadosMotivoMudanca" + params.sqFicha);
     if ($('#divGridMotivoMudancaCategoria tbody>tr').size() == 1) {
         app.alertInfo('Necessário existir pelo menos um motivo da mudança.');
         return;
     }

     app.selectGridRow(btn);
     app.confirm('<br>Confirma exclusão do motivo <b>' + params.descricao + '</b>?',
         function() {
             app.ajax(baseUrl + 'fichaCompleta/deleteMotivoMudanca',
                 params,
                 function(res) {
                     if (res.status == 0) {
                         app.updateGridMotivoMudancaCategoria(params);
                     }
                 });
         },
         function() {
             app.unselectGridRow(btn);
         }, params, 'Confirmação', 'warning');
 };

 var repetirUltimaAvaliacao = function(params) {
     app.confirm('<br>Confirma repetição dos dados da última avaliação?',
         function() {
             app.ajax(baseUrl + 'fichaCompleta/repetirUltimaAvaliacao',
                 params,
                 function(res) {
                     if (typeof res.sqCategoriaIucn != 'undefined' && res.sqCategoriaIucn > 0) {
                         var form = '#frmResultadoOficina ';
                         $(form + ' #sqCategoriaIucn').val(res.sqCategoriaIucn);
                         $(form + ' #dsCriterioAvalIucn').val(res.deCriterioAvaliacaoIucn);
                         $(form + ' #stPossivelmenteExtinta').prop('checked', (res.stPossivelmenteExtinta == 'S'));
                         tinymce.get('dsJustificativa').setContent(corrigirCategoriasTexto( res.txJustificativaAvaliacao ) );
                         //$("#pnlShowFichaCompleta_"+params.sqFicha+" #frmResultadoOficina textarea#dsJustificativa").html(res.txJustificativaAvaliacao)
                         var $select = $("#pnlShowFichaCompleta_" + params.sqFicha + " #frmResultadoOficina select#sqCategoriaIucn")
                         if ($select.size() == 0) {
                             $select = $(form + " select#sqCategoriaIucn");
                         }
                         if ($select.size() == 1) {
                             $select.change();
                         }
                         app.growl('Dados última avaliação recuperados com SUCESSO!');
                     } else {
                         app.alertInfo('Nenhuma avaliação NACIONAL cadastrada na aba 7-Conservação.');
                     }
                 });
         });
 };

 var openCloseAllPanels = function(container) {
    var contexto = "#container-ficha-completa ";
    if(container){
        contexto = "#"+container;
     }
     var opened = $(contexto.trim()).data('opened');
     var openAll = (opened == 'N' ? true : false);
     $(contexto.trim()).data('opened', (openAll ? 'S' : 'N'));
     $(contexto + ".open-close-all").each(function(i, ele) {
         if (openAll) {
             $(this).find('i').addClass('fa-folder').removeClass('fa-folder-open');
         } else {
             $(this).find('i').removeClass('fa-folder').addClass('fa-folder-open');
         }
     });
     $(".panel-body.panel-collapse").each(function() {
         var clicar = false;
         var id = this.id;
         if (openAll) {
             if (!$(this).hasClass('in')) {
                 clicar = true;
             }
         } else {
             if ($(this).hasClass('in')) {
                 clicar = true;
             }
         }
         if (clicar) {
             //window.setTimeout( function(){
             try {
                 $(contexto+' a[data-target="#' + id + '"]')[0].click();
             } catch (e) {}
             //},100);

         }
     });
 }


 var exibirEditor = function(ele) {
     var data = $(ele).data();
     var options = fichaCompletaEditorOptions;
     var same = options.selector == '#' + data.target;
     options.selector = '#' + data.target;
     options.width = Math.max(400, $(options.selector).width());
     if ($(options.selector).length == 1) {
         if (tinyMCE.activeEditor && tinyMCE.activeEditor.abaConsolidar) {
             if (!tinyMCE.activeEditor.isDirty()) {
                 fichaCompletaEditorOptions.configurarBotaoEditar();
                 if (same) // clicou no mesmo botão
                 {
                     return;
                 }
             }
         }
         if (data.active != 'S') {
             tinyMCE.init(options);
             tinyMCE.activeEditor.btn = ele; // mostrar / esconder o botão editar
             tinyMCE.activeEditor.abaConsolidar = true; // marcar o editor com o nome da aba para não confundir a função isDirty com o editor de outras abas
             fichaCompletaEditorOptions.configurarBotaoEditar();
             //$(ele).hide();
             window.setTimeout(function() {
                 tinyMCE.activeEditor.focus();
             }, 500);
         };
     };
 };

 /**
  * Atualizar a situação da foto envida durane as consultas
  */
 var updateSituacaoMultimidia = function(params, fld, evt) {
     if (!params.sqFichaMultimidia) {
         return;
     }
     var data = { id: params.sqFichaMultimidia };
     var $select = $(fld);
     var $divTextoNaoAceito = $("#div-multimidia-nao-aceita-" + params.sqFichaMultimidia);
     var codigoSituacao = $select.find('option:selected').data('codigo');


     if (codigoSituacao == 'REPROVADA') {
         if (!params.txNaoAceito) {
             data.modalName = 'textoLivre';
             data.label = 'Justificativa';
             data.maxLength = 500;
             if ($divTextoNaoAceito) {
                 data.currentText = $divTextoNaoAceito.text();
             }
             modal.janelaTextoLivre({
                 title: 'Motivo não aceitação',
                 'url': '/main/getModal',
                 'data': data,
                 'height': 280,
                 'theme': '#00f',
                 'resizeble': false,
                 'onClose': function(panel) {
                     var $textArea = panel.content.find('textarea#fldTextoLivre' + data.id);
                     if ($textArea.size() == 1) {
                         var texto = $.trim($textArea.val());
                         if (!texto || $textArea.prop('modified') != 'S') {
                             // retornar o select para a opção anterior
                             $select.val($(fld).find('option[selected]').val());
                             if ($divTextoNaoAceito && !texto) {
                                 $divTextoNaoAceito.hide();
                             }
                         } else {
                             $divTextoNaoAceito.show();
                             if (texto != data.currentText) {
                                 data.txNaoAceito = texto;
                                 data.sqFichaMultimidia = data.id;
                                 if ($divTextoNaoAceito) {
                                     $divTextoNaoAceito.text(texto);
                                 }
                                 updateSituacaoMultimidia(data, fld, evt);
                             }
                         }
                     }
                 }
             });
             return;
         }
     } else {
         $divTextoNaoAceito.hide();
     }

     if (codigoSituacao == 'PENDENTE_APROVACAO') {
         $select.removeClass('green');
         $select.addClass('red');
     } else {
         $select.removeClass('red');
         $select.addClass('green');
     };

     var data = {
         sqFichaMultimidia: params.sqFichaMultimidia,
         sqSituacao: $select.val(),
         txNaoAceito: (params.txNaoAceito || '')
     };

     app.ajax(app.url + 'fichaCompleta/updateSituacaoMultimidia', data,
         function(res) {
             if (res == '') {
                 var $divMultimidiaNaoAceita = $("#div-multimidia-nao-aceita-" + params.sqFichaMultimidia);
                 $divMultimidiaNaoAceita.text(params.txNaoAceito);
                 app.growl('Alteração gravada com SUCESSO!', 2, '', 'tc', null, 'success');
             } else {
                 app.alertInfo(res)
             }
         });
 };



 /**
  * Atualizar a situação da colaboração com a ficha e ocorrências
  * dependendo do params.sq??? enviado pelo gride
  */
 var updateSituacaoColaboracao = function(params, fld, evt) {
     if (!fld) {
         return;
     }
     var $fld = $(fld);
     var $tr = $fld.closest('tr');
     var data = $.extend({}, params, $tr.data());
     data.sqSituacao = $fld.val();
     if (!data.contexto) {
         app.alertInfo('Contexto não informado!');
         return
     }
     if (!data.id) {
         app.alertInfo('ID não informado!');
         return
     }

     var $divWrapperTextoNaoAceito = $("#divWrapperTextoNaoAceita" + data.id);
     var $divTextoNaoAceito = null;
     if ($divWrapperTextoNaoAceito.size() == 1) {
         $divTextoNaoAceito = $divWrapperTextoNaoAceito.find('div:first');
     } else {
         $divWrapperTextoNaoAceito = null;
     }

     // informar texto da nao aceitacao
     if ($fld.find('option:selected').data('codigo') == 'NAO_ACEITA') {
         if (!params.txNaoAceita) {
             data.modalName = 'textoLivre';
             data.label = 'Justificativa';
             data.maxLength = 500;
             if ($divTextoNaoAceito) {
                 data.currentText = $divTextoNaoAceito.text();
             }
             modal.janelaTextoLivre({
                 title: 'Motivo não aceitação',
                 'url': '/main/getModal',
                 'data': data,
                 'height': 280,
                 'theme': '#00f',
                 'resizeble': false,
                 'onClose': function(panel) {
                     var $textArea = panel.content.find('textarea#fldTextoLivre' + data.id);
                     if ($textArea.size() == 1) {
                         var texto = $.trim($textArea.val());
                         if (!texto || $textArea.prop('modified') != 'S') {
                             // retornar o select para a opção anterior
                             $fld.val($fld.find('option[selected]').val());
                             if ($divWrapperTextoNaoAceito && !texto) {
                                 $divWrapperTextoNaoAceito.hide();
                             }
                         } else {
                             if ($divWrapperTextoNaoAceito) {
                                 $divWrapperTextoNaoAceito.show();
                             }
                             if (texto != data.currentText) {
                                 data.txNaoAceita = texto;
                                 if ($divWrapperTextoNaoAceito) {
                                     $divTextoNaoAceito.text(texto);
                                 }
                                 updateSituacaoColaboracao(data, fld, evt);
                             }
                         }
                     }
                 }
             });
             return;
         }
     } else {
         if ($divWrapperTextoNaoAceito) {
             $divWrapperTextoNaoAceito.hide();
         }
     }

     app.ajax(app.url + 'fichaCompleta/updateSituacaoColaboracao', data,
         function(res) {
             if ($.trim(res) != '') {
                 app.alertInfo(res);
             } else {
                 var codigo = $fld.find('option:selected').data('codigo');

                 // atualizar mapa
                 if (oMap) {
                     oMap.refresh();
                 }

                 if (codigo != 'NAO_AVALIADA') {
                     if (data.fonte == 'consulta') {
                         if (data.contexto == 'oficina' && codigo == 'RESOLVER_OFICINA') {
                             $tr.removeClass('colaboracao-avaliada');
                             $tr.addClass('colaboracao-nao-avaliada');
                             $fld.removeClass('colaboracao-avaliada');
                             $fld.addClass('colaboracao-nao-avaliada');

                         } else {
                             $tr.removeClass('colaboracao-nao-avaliada');
                             $tr.addClass('colaboracao-avaliada');
                             $fld.removeClass('colaboracao-nao-avaliada');
                             $fld.addClass('colaboracao-avaliada');
                         }
                     }
                     // nao precisa mais
                     //$fld.removeClass('colaboracao-avaliada colaboracao-nao-avaliada').addClass('colaboracao-avaliada');

                 } else {
                     if (data.fonte == 'consulta') {
                         $tr.removeClass('colaboracao-avaliada');
                         $tr.addClass('colaboracao-nao-avaliada');
                         $fld.removeClass('colaboracao-avaliada');
                         $fld.addClass('colaboracao-nao-avaliada');
                     }
                     // nao precisa mais
                     //$fld.removeClass('colaboracao-avaliada colaboracao-nao-avaliada').addClass('colaboracao-nao-avaliada');
                 }
                 // atualizar andamento das avaliações das colaborações
                 updateAndamentoAvaliacaoFichaCompleta();
             }
         }
     );
 };

 var countColaboracaoPendente = function(params) {
     //return $("#container-ficha-completa  span.bold.red[id^=sub-status-]")
     var qtd = 0
     if ( ! params.sqFicha ) {
         qtd = $("#container-ficha-completa span.red[id^=sub]").size();
     } else {
         $('#pnlShowFichaCompleta_' + params.sqFicha + ' span.red[id^=sub]').each(function() {
             var aTemp = $(this).html().split('/');
             if ( aTemp[0] !='' && parseInt(aTemp[0]) < parseInt(aTemp[1])) {
                 qtd += (parseInt(aTemp[1]) - parseInt(aTemp[0]));
             }
         })
     }
     return qtd;
 };

 var countColaboracaoMultimidiaPendente = function(params) {
     var qtd = 0;

     if (!params.sqFicha) {
         return 0;
     } else {
         return $('#div-gride-ocorrencias-' + params.sqFicha + ' select.red').size();
     }
 };

 var showFormPendencia = function(params) {
     app.panel('pnlCadastroPendencia', 'Informar pendência', '/fichaCompleta/getFormPendencia', params, function() {
         // on show
         $("#pnlCadastroPendencia #deAssunto").val(params.assunto ? params.assunto : $('#deAssunto').val() );
         // flag para indicar que alguma ação foi feita na modal
         $("#frmPendenciaChanged").val('N');
         if (!$("#deAssunto").val()) {
             app.focus('deAssunto', 'pnlCadastroPendencia');
         } else {
             window.setTimeout(function() {
                 tinyMCE.execCommand('mceFocus', false, 'txPendencia');
             }, 1000);
         }
     }, 840, 500, true /*modal*/ , false /*maximized*/ , function(e) {
         var formSaved = ( $("#frmPendenciaChanged").val() == 'S' );
         app.reset("frmPendencia");
         // on close
         tinymce.activeEditor.remove();
         // executar os callbacks somente se tiver tido alguma ação na pendência
         if( formSaved ) {
             if (params.callback) {
                 params.onClose = params.callback;
             }
             if (params.onClose) {
                 app.eval(params.onClose);
             } else {
                 updateGridPendencia();
             }
         }
     }).setTheme('#C14642');

 };

 var savePendencia = function(params) {

     var contexto = "#div-pendencia-wrapper ";
     if (!$(contexto + '#frmPendencia').valid()) {
         return false;
     }
     var texto = tinyMCE.get('txPendencia').getContent();
     // no contexto validador o texto pode ser deixado em branco para excluir a pendência pelo validador
     if (!texto && params.contexto.toLowerCase() != 'validador') {
         app.alertInfo('Descrição da pendência deve ser inforada.');
         return;
     }
     var data = $(contexto + "#frmPendencia").serializeArray();
     data = app.params2data(params, data);
     if (!data.sqFicha && !data.sqFichas && !data.sqFichaPendencia) {
         app.alertError('ID da ficha não encontrado!');
         return;
     }
     // flag para indicar que alguma ação foi feita na modal
     $("#frmPendenciaChanged").val('S');
     app.ajax(app.url + 'fichaPendencia/saveFrmPendencia', data,
         function(res) {
             if (res.status == 0) {
                 app.closeWindow(); // fecha a ultima janela aberta com jsPanel
                 if( res.data ) {
                     if( res.data.sqSituacaoFicha ) {
                        var $spanSituacaoFicha = $("#spanSituacaoFicha");
                        if( $spanSituacaoFicha.size()==1){
                            $spanSituacaoFicha.html('<b>Situação</b>:'+ res.data.deSituacaoFicha);
                        }
                     }
                 }
             }
         }, null, 'json');
 };

 var updateGridPendencia = function() {
     var contexto = "#container-ficha-completa ";
     var posicao = $("#gride-pendencia").scrollTop();
     var data = {
         sqFicha: $(contexto + " #sqFicha").val()
     };
     if ($(contexto + "#gride-pendencia").size() > 0) {
         app.ajax(app.url + 'fichaCompleta/getPendencias', data,
             function(res) {
                 $(contexto + "#gride-pendencia").html(res);
                 $(contexto + "#gride-pendencia").scrollTop(posicao);
             });
     }
 };

 var deletePendencia = function(params, btn) {
     var contexto = "#div-pendencia-wrapper "
     app.selectGridRow(btn);
     app.confirm('<br>Confirma exclusão pendência?',
         function() {
             app.ajax(app.url + 'fichaPendencia/deletePendencia',
                 params,
                 function(res) {
                     if (params.callback) {
                         app.eval(params.callback);
                     } else {
                         updateGridPendencia();
                     }
                 });
         },
         function() {
             app.unselectGridRow(btn);
         }, params, 'Confirmação', 'warning');
 };

 // validadores
 var sqResultadoValidadorChange = function(params) {
     var frm = "#" + params.contexto;
     var codigo = $(frm + " #sqResultado option:selected").data('codigo');
     var sqOficinaFicha = $("#sqOficinaFicha").val();
     if (codigo) {
         if (codigo == 'RESULTADO_A') {
             $(frm + " #divCategoria,#divCriterio,#divJustificativa").addClass('hide');
         } else if (codigo == 'RESULTADO_B') {
             $(frm + " #divCategoria,#divCriterio").addClass('hide');
             $(frm + " #divJustificativa").removeClass('hide');
         } else if (codigo == 'RESULTADO_C') {
             $(frm + " #divCategoria,#divCriterio,#divJustificativa").removeClass('hide');
             sqCategoriaSugeridaChange(params);
         }
     } else {
         $(frm + " #divCategoria,#divCriterio,#divJustificativa").addClass('hide');
     }
     var data = { sqOficinaFicha: sqOficinaFicha };
     for (key in params) {
         if (typeof(params[key]) != 'object') {
             data[key] = params[key];
         }
     }
 };

 var saveResultadoValidador = function(params) {
     if (!params.sqFicha) {
         app.alertInfo('faltou id da ficha.');
         return;
     }
     if (!params.sqOficinaFicha) {
         app.alertInfo('faltou id da oficina ficha.');
         return;
     }
     if (!params.contexto) {
         app.alertInfo('faltou contexto.');

         return;
     }

     var frm = "#" + params.contexto;
     var pnl = "#pnlShowFichaCompleta_" + params.sqFicha;
     var editorJustificativa = tinymce.get("dsJustificativaValidador" + params.sqFicha  );
     var noCientifico = $("#pnlShowFichaCompleta_" + params.sqFicha + " .jsPanel-title").html();

     if (!$(frm + ' #sqCategoriaSugerida').is(':visible')) {
         $(frm + ' #sqCategoriaSugerida').val('');
     }
     if (!$(frm + ' #deCriterioSugerido').is(':visible')) {
         $(frm + ' #deCriterioSugerido').val('');
     }
     var sqCategoriaAtual = $(pnl + ' #sqCategoriaIucn').val();
     var dsCriterioAtual = $(pnl + ' #dsCriterioAvalIucn').val();
     var sqCategoriaSugerida = $(frm + ' #sqCategoriaSugerida').val();
     var dsCriterioSugerido = $(frm + ' #deCriterioSugerido').val();
     var cdResultado = $(frm + ' #sqResultado option:selected').data('codigo');
     if (cdResultado == 'RESULTADO_C') {
         if (sqCategoriaAtual && sqCategoriaAtual == sqCategoriaSugerida && dsCriterioAtual == dsCriterioSugerido) {
             if (dsCriterioSugerido) {
                 app.alertInfo('Para resposta "C", a categoria e critério informados devem ser diferentes do resultado da oficina!');
             } else {
                 app.alertInfo('Para resposta "C", a categoria informada deve ser diferente do resultado da oficina!');
             }
             return;
         }
     }

     var data = {
         sqFicha: params.sqFicha,
         sqOficinaFicha: params.sqOficinaFicha,
         sqResultado: $(frm + ' #sqResultado').val(),
         sqCategoriaSugerida: $(frm + ' #sqCategoriaSugerida').val(),
         deCriterioSugerido: $(frm + ' #deCriterioSugerido').val(),
         stPossivelmenteExtinta: '',
         //txPendencia : $(frm + ' #txPendencia' + params.sqFicha).val(),
         dsJustificativa: corrigirCategoriasTexto( trimEditor( editorJustificativa.getContent({
             format: 'html'
            }))
         )
     }
     editorJustificativa.setContent(data.dsJustificativa);

     if ($(frm + ' #stPossivelmenteExtinta').is(':visible')) {
         if ($(frm + ' #stPossivelmenteExtinta').is(':checked')) {
             data.stPossivelmenteExtinta = 'S'
         } else {
             data.stPossivelmenteExtinta = 'N'
         }
     }

     var resposta = $(frm + " #sqResultado option:selected").text();
     var codigo = $(frm + " #sqResultado option:selected").data('codigo');
     var aTemp = [resposta];
     if (codigo) {
         if (codigo == 'RESULTADO_A') {
             $(frm + " #dsCategoriaSugerida,#deCriterioSugerido").val('');
             editorJustificativa.setContent('');
             data.sqCategoriaSugerida = '';
             data.deCriterioSugerido = '';
             data.dsJustificativa = '';
         } else if (codigo == 'RESULTADO_B') {
             $(frm + " #dsCategoriaSugerida,#deCriterioSugerido").val('');
             data.sqCategoriaSugerida = '';
             data.deCriterioSugerido = '';
             if (!data.dsJustificativa.trim()) {
                 app.alertInfo('Necessário informar a fundamentação!');
                 return;
             }
         } else if (codigo == 'RESULTADO_C') {
             var msg = []
             if (!data.sqCategoriaSugerida) {
                 msg.push('Necessário informar a categoria!');
             }
             if (!data.deCriterioSugerido && $(frm + ' #divCriterio').is(":visible")) {
                 msg.push('Necessário informar o critério!');
             }
             if (!data.dsJustificativa) {
                 msg.push('Necessário informar a fundamentação!');
             }
             if (msg.length > 0) {
                 app.alertInfo(msg.join('<br/>'));
                 return;
             }
         }
         if (data.sqCategoriaSugerida) {
             aTemp.push('Categoria: <b>' + $(frm + ' #sqCategoriaSugerida option:selected').text() + '</b>')
         }
         if (data.deCriterioSugerido) {
             aTemp.push('Critério: <b>' + data.deCriterioSugerido + '</b>')
             if (data.stPossivelmenteExtinta) {
                 if (data.stPossivelmenteExtinta == 'S') {
                     aTemp.push('Possivelmente extinta: <b>SIM</b>');
                 } else {
                     aTemp.push('Possivelmente extinta: <b>NÃO</b>');
                 }
             }
         }
     } else {
         aTemp = ['LIMPAR RESULTADO'];
     }
     resposta = aTemp.join('<br/>')
     app.confirm('<h3>Confirma sua resposta?</h3><b>' + resposta + '</b>',
         function() {
             app.ajax(baseUrl + 'fichaCompleta/saveResultadoValidador', data, function(res) {
                 if( res.data.resultados ){
                     var exibirChat = res.data.resultados == 1 || data.sqResultado > 0;
                     res.data.resultados.forEach( function( resultado ){
                         // exibir/exconder o fieldSet do resultado do validor
                         if( resultado.dsResultado ) {
                             $("#fieldsetRespostasOutrosValidadores-" + resultado.sqValidadorFicha).removeClass('hide');
                         } else {
                             $("#fieldsetRespostasOutrosValidadores-" + resultado.sqValidadorFicha).addClass('hide');
                         }
                         // alimentar o campo resposta
                         $("#dsResultado"+resultado.sqValidadorFicha).html( resultado.dsResultado );
                         // alimentar e exibir o campo categoria sugerida
                         if( resultado.dsCategoriaSugerida ){
                             $("#pCategoriaSugerida-"+resultado.sqValidadorFicha).removeClass('hide');
                             $("#dsCategoriaSugerida"+resultado.sqValidadorFicha).html(resultado.dsCategoriaSugerida);
                         } else {
                             $("#pCategoriaSugerida-"+resultado.sqValidadorFicha).addClass('hide');
                             $("#dsCategoriaSugerida"+resultado.sqValidadorFicha).html('');
                         }
                         // alimentar e exibir o campo criterio
                         if( resultado.deCriterioSugerido ) {
                             $("#divCriterioSugerido-" + resultado.sqValidadorFicha).removeClass('hide');
                             $("#deCriterioSugerido" + resultado.sqValidadorFicha).html(resultado.deCriterioSugerido);
                             $("#stPossivelmenteExtinda" + resultado.sqValidadorFicha).html(resultado.dePossivelmenteExtinta)
                             if( resultado.stPossivelmenteExtinta){
                                 $("#pStPossivelmenteExtinta-"+resultado.sqValidadorFicha).removeClass('hide');
                             } else {
                                 $("#pStPossivelmenteExtinta-"+resultado.sqValidadorFicha).addClass('hide');
                             }
                         } else {
                             $("#divCriterioSugerido-" + resultado.sqValidadorFicha).addClass('hide');
                             $("#deCriterioSugerido" + resultado.sqValidadorFicha).html('');
                             $("#stPossivelmenteExtinda"+resultado.sqValidadorFicha).html('')
                             $("#pStPossivelmenteExtinta-"+resultado.sqValidadorFicha).addClass('hide');
                         }
                         // alimentar a fundamentacao
                         if( resultado.txJustificativa ){
                             $("#txJustificativa-" + resultado.sqValidadorFicha).html(resultado.txJustificativa);
                         } else {
                             $("#txJustificativa-" + resultado.sqValidadorFicha).html('');
                         }
                     });
                     if( exibirChat ){
                         $("#divChatContainer"+data.sqFicha).removeClass('hide');
                     } else {
                         $("#divChatContainer"+data.sqFicha).addClass('hide');
                     }

                 }
           }, null, 'json');
         }, null, null, noCientifico);
 };

 var initFormValidacao = function(data) {
     var frm = "#frmRespostaValidador" + data.sqFicha;
     // chamar os metodos change dos selects
     $(frm + ' #sqResultado').change();
     salveWorker.startChat(data);
 }

 var saveChat = function(params) {
     if (!params.sqOficinaFicha) {
         return;
     }
     var pnl = '';
     if ($("#pnlTelaFundamentacao" + params.sqFicha).size() == 1) {
         pnl = "#pnlTelaFundamentacao" + params.sqFicha;
     } else if ($("#pnlShowFichaCompleta_" + params.sqFicha).size() == 1) {
         pnl = "#pnlShowFichaCompleta_" + params.sqFicha;
     } else {
         app.alertInfo('container do chat não encontrado!')
         return;
     }

     var txChat = $(pnl + " #txChat" + params.sqFicha).val().trim()
     if (!txChat) {
         return;
     }
     var data = {
         txChat: txChat
     };
     data = app.params2data(params, data);
     app.blockElement(pnl + " #frmChat", 'Enviando...');
     app.ajax(baseUrl + 'fichaCompleta/saveChatValidacao', data, function(res) {
         app.unblockElement(pnl + " #frmChat");
         if (res.status == 0) {
             $(pnl + " #txChat" + params.sqFicha).val('');
             //$(pnl + " #divGridChat" + params.sqFicha).data('scrollbottom',true);
             app.focus(pnl + " #txChat" + params.sqFicha);
             salveWorker.scrollChatToBottom();
         }

     }, null, 'json');
 };

 var convidar = function(params, btn, evt) {
     var perfil = '';
     var pergunta = '';
     if (params.codigo == 'CT') {
         pergunta = '<br>Confirma envio do e-mail para o <b>COODENADOR DE TAXON</b> participar do bate-papo?'
     } else if (params.codigo == 'VL') {
         pergunta = '<br>Confirma envio do e-mail para os <b>VALIDADORES</b> participarem do bate-papo?'
     } else {
         pergunta = '<br>Confirma envio do e-mail para o <b>PONTO FOCAL</b> participar do bate-papo?'
     }
     app.confirm(pergunta,
         function() {
             app.ajax(baseUrl + 'fichaCompleta/convidarChat', params, function(res) {
                 if (res.error) {
                     app.alertError(res.error);
                 } else if (res.status == 0) {
                     // desabilitar o botão se o email não for para os validadores
                     if (params.codigo != 'VL') {
                         $(btn).prop('disabled', true);
                         $(btn).find('span').html(params.codigo + ' convidado');
                     }
                 }
             }, null, 'JSON');
         })
 };

 var chamarAtencaoTodosChat = function(params, btn, evt) {
     var data = app.params2data(params, {});
     modal.janelaComunicarTodosChat(data, function( res ) {
         app.closeWindow('modalComunicarTodosChatValidacao');
         app.growl('Enviando e-mails...', 5);
         data = $.extend(data,res);
         app.ajax(baseUrl + 'fichaCompleta/chamarAtencaoTodosChat', data, function (res) {
                 if (res.error) {
                     app.alertError(res.error)
                 }
             }, null, 'JSON', null
             // always
             , function () {
                 $("#btnConvidarTodos i").removeClass('glyphicon-refresh spinning').addClass('glyphicon-envelope');
                 $("#btnConvidarTodos").prop('disabled', false);
             });

     });

     /*
     if ( ! params.confirmed) {
         var pergunta = 'Confirma envio do e-mail para todos?';
         app.confirm('<br>' + pergunta,
             function () {
                 data.confirmed=true;
                 chamarAtencaoTodosChat(data);
             })
         return;
     }
    */

     /*
     modal.janelaTextoLivre({
         'title': "Mensagem do email - Comunicar Todos"
         ,'btnLabel':'Enviar mensagem'
         ,'label':''
         ,width  : 800
         ,height : 600
         //,'theme':'#a3c4a3'
         ,resizeble:false
         , callback: function (winId, textArea ) {
             data.id = winId;
             // recuperar mensagem anterior
             if ( textArea.size() == 1) {
                 var txEmail = app.lsGet('mensagemEmailChatChamarAtencaoTodos');
                 textArea.val( txEmail );
             }
         }
         , onClose: function (panel) {
             data.txEmail = '';
             var $textArea = panel.content.find('textarea#fldTextoLivre' + data.id);
             if ($textArea.size() == 1) {
                 // fechou a janela sem gravar
                 if ( $textArea.prop('modified') != 'S') {
                     return;
                 }
                 data.txEmail = $.trim( $textArea.val() );
                 if (data.txEmail) {
                     app.growl('Enviando e-mails...',5);
                     app.lsSet('mensagemEmailChatChamarAtencaoTodos', data.txEmail);
                     $("#btnConvidarTodos").prop('disabled', true);
                     $("#btnConvidarTodos i").removeClass('glyphicon-envelope').addClass('glyphicon-refresh spinning');
                     app.ajax(baseUrl + 'fichaCompleta/chamarAtencaoTodosChat', data, function (res) {
                             if (res.error) {
                                 app.alertError(res.error)
                             }
                         }, null, 'JSON', null
                         // always
                         , function () {
                             $("#btnConvidarTodos i").removeClass('glyphicon-refresh spinning').addClass('glyphicon-envelope');
                             $("#btnConvidarTodos").prop('disabled', false);
                         });
                 }
             }
         }
     })*/
 };

 var chamarAtencaoChat = function(params, btn, evt) {
     var pergunta = 'Confirma envio do e-mail para o outro validador?';
     if (params.sqValidadorFicha == "0") {
         pergunta = 'Confirma envio do e-mail para os validadores?'
     }

     app.confirm('<br>' + pergunta,
         function() {
             app.ajax(baseUrl + 'fichaCompleta/chamarAtencaoChat', params, function(res) {
                 if (res.error) {
                     app.alertError(res.error)
                 } else if (res.status == 0) {

                 }
             }, null, 'JSON');
         })
 };

/*
 var btnGravarMsgEmailClick=function( data ) {
     data = data || {};
     var form = $("#frmComunicarTodosChatValidacao");
     data.listCts = [];
     form.find('input.chkCT:checked').map(function (i, item) {
         data.listCts.push(item.value);
     });
     if (form.find('input.chkCT').size() > 0 && data.listCts.length == 0) {
         app.alertInfo('Selecione um dos coordenadores de taxon.');
         return;
     }
     data.txEmail = form.find('textarea').val();
     if ( ! data.txEmail ) {
         app.alertInfo('Mensagem do e-mail deve ser informada.', '', function () {
             app.focus("fldMsgEmail");
         });
         return;
     }
     app.closeWindow('modalComunicarTodosChatValidacao');
     app.growl('Enviando e-mails...', 5);
     app.lsSet('mensagemEmailChatChamarAtencaoTodos', data.txEmail);
     $("#btnConvidarTodos").prop('disabled', true);
     $("#btnConvidarTodos i").removeClass('glyphicon-envelope').addClass('glyphicon-refresh spinning');
     data.listCts = data.listCts.join(',');
     console.log( data );
     return;
     app.ajax(baseUrl + 'fichaCompleta/chamarAtencaoTodosChat', data, function ( res ) {
             if (res.error) {
                 app.alertError(res.error)
             }
         }, null, 'JSON', null
         // always
         , function () {
             $("#btnConvidarTodos i").removeClass('glyphicon-refresh spinning').addClass('glyphicon-envelope');
             $("#btnConvidarTodos").prop('disabled', false);
         });

 }
 */
 //var updateGridChat = function(params) {

 /*
 if (!params.sqFicha || $("#divGridChat" + params.sqFicha).size() == 0) {
     return;
 }
 // evitar mais de uma requisição ao mesmo tempo
 if (updatingChat) {
     return
 }
 updatingChat = true;
 var posicao = $("#divGridChat" + params.sqFicha).scrollTop();
 app.ajax(baseUrl + 'fichaCompleta/getGridChats', params, function(res) {
     $("#divGridChat" + params.sqFicha).html(res);
     updatingChat = false;
     if (params.scrollBottom) {
         $("#divGridChat" + params.sqFicha).scrollTop($("#divGridChat" + params.sqFicha).height() * 2);
     } else {
         $("#divGridChat" + params.sqFicha).scrollTop(posicao);
     }
     window.setTimeout(function() {
         updateGridChat(params);
     }, 10000)
 }, null, 'html');
 */

 //};

 var ajustarAlturaTelaChat = function(params) {
     // var $divRespostas = $('#pnlTelaFundamentacao' + params.sqFicha + ' #divValidadoresContainer');
     // var $divMessages = $('#pnlTelaFundamentacao' + params.sqFicha + ' #divGridChat' + params.sqFicha);
     var $divRespostas  = $('#divValidadoresContainer' + params.sqFicha);
     var $divChat       = $('#divChatContainer' + params.sqFicha);
     var $divMessages   = $divChat.find('#divGridChat' + params.sqFicha);
     var $frmChat       = $divChat.find('form#frmChat');
     var frmChatHeight = 0;
     if ($frmChat.is(":visible")) {
         frmChatHeight = $frmChat.height();
     }
     // calcular a altura maxima
     var newHeight = Math.max(400,$divRespostas.height() - frmChatHeight - 27);
     $divMessages.css({'maxHeight': newHeight + 'px', 'minHeight': newHeight + 'px'})
 };


 var excluirChat = function(sqFicha, sqChat) {
     if (!sqChat || !sqFicha) {
         return;
     }
     var data = {
         sqChat: sqChat,
         sqFicha: sqFicha
     }
     app.confirm('<br>Confirma exclusão da mensagem?',
         function() {
             app.ajax(baseUrl + 'fichaCompleta/deleteChat', data, function(res) {
                 if (res.status == 0) {
                     if (res.msg) {
                         app.alertError(res.msg);
                     }
                 }
             });
         })
 };


 /**
  * nova forma utilizando a tabela abrangencia
  * @param params
  */
 var tipoAbrangenciaChange = function(params) {
     var contexto = "#" + params.contexto.replace(/^#/, '') + " ";
     var cdTipoAvaliacao = $(contexto + '#sqTipoAvaliacao').find('option:selected').data('codigo');
     var selectPai = $(contexto + ' #sqTipoAbrangencia');
     var selectFilho = $(contexto + ' #sqAbrangencia');
     var campoOutra = $(contexto + '#noRegiaoOutra');
     if (/GLOBAL|NACIONAL_BRASIL/.test(cdTipoAvaliacao)) {
         selectFilho.parent().addClass('hide');
         campoOutra.parent().addClass('hide');
     } else {
         // não pode limpar devido a edição alimentar o campo e chamar este evento
         //selectFilho.select2('data', null);
         //selectFilho.val('').trigger('change');
         selectFilho.prop('disabled', selectPai.val() == '');
         selectFilho.parent().removeClass('hide');
         // controle do campo OUTRA
         var codigo = selectPai.find('option:selected').data('codigo');
         if (campoOutra) {
             campoOutra.parent().addClass('hide');
             if (codigo == 'OUTRA') {
                 selectFilho.parent().addClass('hide');
                 campoOutra.parent().removeClass('hide');
             }
         }
     }
     if (!selectPai.val()) {
         selectFilho.select2('data', null);
         selectFilho.val('').trigger('change');
         selectFilho.prop('disabled', true);
     }
     return;
     /*
     // limpar o select
     selectFilho.find('option').each(function(k, v) {
         if (this.value) {
             $(this).remove();
         }
     });
     if (!selectPai.val()) {
         return;
     }
     var coTipoAbrangencia = $(selectPai).find('option:selected').data('codigo');
     var sqTipoAbrangencia = $(selectPai).val();
     var data = {
         contexto:contexto,
         sqTipoAbrangencia: sqTipoAbrangencia,
         coTipoAbrangencia: coTipoAbrangencia
     };
     app.ajax(app.url + 'ficha/getAbrangencias', data, function(res) {
         var tmpVal = $(contexto + " #sqAbrangencia").data('tmpVal');
         $.each(res, function(k, v) {
             selectFilho.append('<option '+( v.codigoSistema?'data-codigo="'+v.codigoSistema+'"':'') +' value="' + v.id + '"' + (v.id == tmpVal ? ' selected' : '') + '>' + v.descricao + '</option>');
         });
         selectFilho.prop('disabled', (data.codigo == ''));
     }, '', 'JSON')
     */
 };

 var saveFrmAvaliacaoRegional = function(params) {
     if (!params.contexto) {
         app.alertError('Contexto não informado!');
         return;
     }
     // colocar o ID no padrão #xxxx
     params.contexto = '#' + params.contexto.replace(/^#/, '');

     var win = "#pnlShowFichaCompleta_" + params.sqFicha;
     if ($(win).length == 0) {
         win = "#tabAvaResultado";
     }
     var formId = win + " " + params.contexto;

     if (!formId) {
         return;
     }
     if (!$(formId).valid()) {
         return;
     }
     var $tipoAbrangencia = $(formId + ' select[name=sqTipoAbrangencia]');
     var $abrangencia = $(formId + ' select[name=sqAbrangencia]');
     var $categoria = $(formId + ' select[name=sqCategoriaIucn]');
     var $criterio = $(formId + ' input[name=dsCriterioAvalIucn]');
     var $extinta = $(formId + ' input[name=stPossivelmenteExtinta]');
     if (!($tipoAbrangencia.size() > 0 && $abrangencia.size() > 0 && $categoria.size() > 0 && $criterio.size() > 0 && $extinta.size() > 0)) {
         app.alertInfo('saveFrmAvaliacaoRegional: campo com id incorreto.');
         return;
     }

     // se a categoria for 'VU,CR,EN tem que ter critério'
     if ($criterio.is(':visible')) {
         if (!$criterio.val()) {
             app.alertInfo('Necessário informar o critério!');
             return;
         }
     } else {
         $criterio.val('');
     }
     if (!$extinta.is(':visible')) {
         $extinta.prop('checked', false);
     }
     var codigoTipoSelecionado = $tipoAbrangencia.find('option:selected').data('codigo');
     if ($tipoAbrangencia.val() && !$abrangencia.val() && codigoTipoSelecionado != 'OUTRA') {
         app.alertInfo('Informe a abrangência!');
         return;
     }
     var data = $(formId).serializeAllArray();
     data = app.params2data(params, data);

     app.ajax(baseUrl + 'fichaCompleta/saveAvaliacaoRegional', data, function(res) {
         if (res.status == 0) {
             updateGridAvaliacaoRegional({
                 sqFicha: params.sqFicha,
                 contexto: formId
             });
             resetFrmAvaliacaoRegional(params);
             app.focus('sqTipoAbrangencia', formId);
         }

     }, null, 'json');
 };

 var resetFrmAvaliacaoRegional = function(params) {
     if (!params.sqFicha) {
         return;
     }
     var win = "#pnlShowFichaCompleta_" + params.sqFicha;
     if ($(win).length == 0) {
         win = "#tabAvaResultado";
     }
     if ($(win + " #btnSaveFrmAvaliacaoRegional").data('value')) {
         $(win + " #btnSaveFrmAvaliacaoRegional").html($(win + " #btnSaveFrmAvaliacaoRegional").data('value'));
     }
     app.unselectGridRow(win + ' #divGridHistoricoAvaliacaoRegional');
     app.reset(win + ' #frmAvaliacaoRegional');
 };

 var categoriaIucnChange = function(params) {
     var win, contexto;
     params = params || {}
     params.contexto = params.contexto || 'frmConHistorico'
         // não tem sq_ficha quando for chamada do módulo ficha
     if (params && params.sqFicha) {
         win = "#pnlShowFichaCompleta_" + params.sqFicha;
         contexto = win + " #" + params.contexto + ' ';
     } else {
         contexto = "#" + params.contexto + ' ';
     }

     if ($(contexto).length == 0) {
         contexto = (params.contexto ? "#" + params.contexto + ' ' : '');
     }
     if (!contexto) {
         return;
     }
     var $selectCategoria = $(contexto + ' select[name=sqCategoriaIucn]');
     var $deCriterio = $(contexto + ' input[name=deCriterioAvalIucn]');
     var $stExtinta = $(contexto + ' input[name=stPossivelmenteExtinta]');
     if (!($selectCategoria && $deCriterio && $stExtinta)) {
         return;
     }

     // se a categoria for 'VU,CR,EN não exibir o campo critério'
     if (!/EN|VU|CR/.test($selectCategoria.find("option:selected").data('codigo'))) {
         //$(contexto + "#deCriterioAvalIucn,#deCriterioAvaliacaoIucn").parent().addClass('hide');
         $deCriterio.parent().addClass('hide');
     } else {
         //$(contexto + "#deCriterioAvalIucn,#deCriterioAvaliacaoIucn").parent().removeClass('hide');
         $deCriterio.parent().removeClass('hide');
     }

     if (/CR/.test($selectCategoria.find("option:selected").data('codigo'))) {
         //$(contexto + "#stPossivelmenteExtinta").closest('div').removeClass('hide');
         $stExtinta.closest('div').removeClass('hide');
     } else {
         //$(contexto + "#stPossivelmenteExtinta").closest('div').addClass('hide');
         $stExtinta.closest('div').addClass('hide');
     }

     /*

     // se a categoria for 'VU,CR,EN não exibir o campo critério'
     if (!/EN|VU|CR/.test($(contexto + "#sqCategoriaIucn option:selected").data('codigo'))) {
         $(contexto + "#deCriterioAvalIucn,#deCriterioAvaliacaoIucn").parent().addClass('hide');
     } else {
         $(contexto + "#deCriterioAvalIucn,#deCriterioAvaliacaoIucn").parent().removeClass('hide');
     }

     if (/CR/.test($(contexto + "#sqCategoriaIucn option:selected").data('codigo'))) {
        $(contexto + "#stPossivelmenteExtinta").closest('div').removeClass('hide');
     } else {
        $(contexto + "#stPossivelmenteExtinta").closest('div').addClass('hide');
     }
     */
 };

 var updateGridAvaliacaoRegional = function(params) {
     var win = "#pnlShowFichaCompleta_" + params.sqFicha;
     if ($(win).length == 0) {
         //win = params.contexto ? "#"+params.contexto : ''
         win = "#tabAvaResultado";
     }
     if (!params.sqFicha) {
         params.sqFicha = $("#sqFicha").val();
     }
     if (typeof(params.canModify) == 'undefined') {
         params.canModify = $(win + " #canModify").val() != 'N';
     }
     if (params.canModify) {
         params.canModify = $(win + " #contexto").val() != 'consulta';
     }
     //$(win+' #divGridHistoricoAvaliacaoRegional').html('<h2>Criar o gride...</h2>');
     app.ajax(baseUrl + 'fichaCompleta/getGridAvaliacaoRegional', params, function(res) {
         var $div = $(win + ' #divGridHistoricoAvaliacaoRegional');
         if ($div.length == 0) {
             $div = $('#divGridHistoricoAvaliacaoRegional')
         }
         $div.html(res);
         if (res.indexOf('<table') == -1 && $('#container-ficha-completa #frmAvaliacaoRegional').size() == 0) {
             $("#container-ficha-completa #fsAvaliacaoRegional").hide();
         } else {
             $("#container-ficha-completa #fsAvaliacaoRegional").show();
         }
     }, '', 'HTML');
 }

 var editAvaliacaoRegional = function(params, btn) {
     if (!params.sqFicha) {
         app.alertInfo('editAvaliacaoRegional: ficha não informada.');
         return;
     }
     if (!params.container) {
         app.alertInfo('editAvaliacaoRegional: container não informado.');
         return;
     }
     // colocar o ID no padrão #xxxx
     params.container = '#' + params.container.replace(/^#/, '');

     var win = "#pnlShowFichaCompleta_" + params.sqFicha;
     if ($(win).length == 0) {
         win = "#tabAvaResultado";
     }
     if (!win) {
         return;
     }
     var formId = win + " " + params.container;
     app.ajax(app.url + 'fichaCompleta/editAvaliacaoRegional', params,
         function(res) {
             $(formId + " #btnResetFrmAvaliacaoRegional").removeClass('hide');
             $(formId + " #btnSaveFrmAvaliacaoRegional").data('value', $("#btnSaveFrmAvaliacaoRegional").html());
             $(formId + " #btnSaveFrmAvaliacaoRegional").html("Gravar alteração");
             app.setFormFields(res, formId);
             app.selectGridRow(btn);

             // expander o topico "Situacao Regional" na ficha completa
             if (!$(formId).hasClass('in')) {
                 $(win + " a[href='#frmAvaliacaoRegional']").click();
             }

             app.focus('sqCategoriaIucn', formId);
             tipoAbrangenciaChange({
                 contexto: formId
             });
             // mostrar / esconder campos de acordo com a categoria
             app.categoriaIucnChange(params);
         });
 };

 var deleteAvaliacaoRegional = function(params, btn) {
     app.confirm('Confirma exclusão da região <b>' + params.descricao + '</b>?', function() {
         app.ajax(app.url + 'fichaCompleta/deleteAvaliacaoRegional', params, function(res) {
             if (!res) {
                 updateGridAvaliacaoRegional(params);
                 resetFrmAvaliacaoRegional(params);
             }
         }, null, 'text')
     });
 };
 /**
  * exibie o dialogo para impressão da ficha em pdf ou rtf
  * @param params
  */
 var print = function(params) {
     if (!params.sqFicha) {
         return;
     }
     // integracao SIS/IUCN
     params.language = params.language ? params.language : 'pt';
     var checkComPendencia = '';
     var mostrarCheckPendencias = (params.language == 'pt');
     var mostrarButtonDoc = (params.language == 'pt');
     var title  = "Impressão da Ficha" + (params.language=='en' ? ' Traduzida':'')
     var buttons =[ {
         label: String('PDF'),
         cssClass: 'btn-primary btn100',
         action: function(dialogRef) {
             dialogRef.close();
             app.ajax(app.url + 'ficha/gerarPdf/', {
                     sqFicha: params.sqFicha,
                     language:params.language
                     ,pendencias: ( $("#checkPendenciasDlgConfirm").is(':checked') ? 'S': 'N' )
                 },
                 function(res) {
                     if (res.data.fileName) {
                         window.open(app.url + 'main/download?fileName=' + res.data.fileName + '&delete=1', '_top', "width=420,height=230,scrollbars=no,status=1");
                     }
                     return;
                 },
                 window.grails.spinner + '&nbsp;Gerando Ficha no formato PDF. Aguarde....', 'json');
         }
        }];
     if( mostrarButtonDoc ) {
         buttons.push({
                 label: 'DOC',
                 cssClass: 'btn-success btn100',
                 action: function (dialogRef) {
                     dialogRef.close();
                     app.ajax(app.url + 'ficha/gerarRtf/', {
                             sqFicha: params.sqFicha,
                             adColaboracao: (params.adColaboracao == 'S' ? 'S' : 'N'),
                             adAvaliacao: (params.adAvaliacao == 'S' ? 'S' : 'N'),
                             adValidacao: (params.adValidacao == 'S' ? 'S' : 'N'),
                         },
                         function (res) {
                             if (res.data.fileName) {
                                 window.open(app.url + "main/download?fileName=" + res.data.fileName + "&delete=1", "_top", "width=420,height=230,scrollbars=no,status=1");
                             }
                             return;
                         },
                         window.grails.spinner + '&nbsp;Gerando Ficha no formato editável. Aguarde....', 'json');
                 }
             }
         );
     }
     buttons.push({
             label: 'Cancelar',
             cssClass: 'btn-danger btn100',
             action: function (dialogRef) {dialogRef.close()}
         }
     );
     if( mostrarCheckPendencias ) {
         checkComPendencia = '<div class="form-group form-group-sm">' +
             '<label class="cursor-pointer" title="Imprimir  a(s) pendência(s) no final da ficha. (versao PDF apenas)">Imprimir a(s) pendência(s)? <input id="checkPendenciasDlgConfirm" type="checkbox" class="checkbox-lg" value="S">' +
             '</label></div>';
     }
     BootstrapDialog.show({
         message: 'Confirma a impressão da ficha da espécie <b><i>' + params.noCientifico + '</i></b>?'+checkComPendencia,
         closable: false,
         draggable: true,
         title: title,
         type: BootstrapDialog.TYPE_INFO,
         buttons: buttons
     });
 }


 var updateLegend = function(params) {
     var $div = $("#mapaFichaCompletaLegend-" + params.sqFicha + " #conteudo")
     var itens = []
     for (key in oLegend) {
         var cor = oLegend[key];
         var legenda = key;
         //itens.push('<i class="glyphicon glyphicon-minus-sign" style="color:' + cor + ';background-color:' + cor + ';"></i><span>&nbsp;' + legenda + '</span>');
         itens.push('<i class="fa fa-circle" aria-hidden="true" style="color:' + cor + ';"></i><span>&nbsp;' + legenda + '</span>');
     }
     $div.html(itens.join('&nbsp;&nbsp;&nbsp;&nbsp;'))
 }

var radioConsolidarPontoConsultaClick = function(radio) {
    try{
        event.stopPropagation();
        event.preventDefault();
    } catch(e){}
    var $radio = $( radio );
    var data = $radio.data();
    var $wrapper = $("#wrapperSugestaoPonto"+data.id);
    app.ajax(app.url + 'fichaCompleta/updateSituacaoPontoConsulta', data, function(res) {
        if( res.status == 1){
            return;
        }
        $radio.prop('checked',true);
        if (data.value) {

            if (data.value == 'RESOLVER_OFICINA' && data.contexto == 'oficina') {
                $wrapper.removeClass('colaboracao-avaliada').addClass('colaboracao-nao-avaliada');
            } else if (data.value == 'NAO_AVALIADA') {
                $wrapper.removeClass('colaboracao-avaliada').addClass('colaboracao-nao-avaliada');
            } else {
                $wrapper.removeClass('colaboracao-nao-avaliada').addClass('colaboracao-avaliada');
            }
        } else {
            $wrapper.removeClass('colaboracao-avaliada').addClass('colaboracao-nao-avaliada');
        }
        updateAndamentoAvaliacaoFichaCompleta();
    }, '', 'JSON');
};

var radioSugestaoClick = function( radio ){
    try{
        event.stopPropagation();
        event.preventDefault();
    } catch(e){}
    var $radio = $(radio);
    var data = $radio.data();
    var $wrapper = $("#wrapperSugestao"+data.sqFichaOcorrenciaValidacao);
    app.ajax(app.url + 'fichaCompleta/updateSituacaoSugestao', data, function(res) {
        if( res.status == 1){
            return;
        }
        $radio.prop('checked',true);
        if (data.value) {
            if (data.value == 'RESOLVER_OFICINA' && data.contexto == 'oficina') {
                $wrapper.removeClass('colaboracao-avaliada').addClass('colaboracao-nao-avaliada');
            } else if (data.value == 'NAO_AVALIADA') {
                $wrapper.removeClass('colaboracao-avaliada').addClass('colaboracao-nao-avaliada');
            } else {
                $wrapper.removeClass('colaboracao-nao-avaliada').addClass('colaboracao-avaliada');
            }
        } else {
            $wrapper.removeClass('colaboracao-avaliada').addClass('colaboracao-nao-avaliada');
        }
        updateAndamentoAvaliacaoFichaCompleta();
    }, '', 'JSON');
}

 var radioRevisorClick = function(radio) {
     var $radio = $(radio)
     var $tr = $radio.closest('tr');
     var data = $.extend($radio.data(), $tr.data());
     app.ajax(app.url + 'fichaCompleta/saveRevisor', data, function(res) {
         var span = $("#span_revisor" + data.id)
         var tr = $("tr[data-id=" + data.id + "]")
         if (data.value) {
             if (data.value == 'RESOLVER_OFICINA' && data.contexto == 'oficina') {
                 span.removeClass('colaboracao-avaliada').addClass('colaboracao-nao-avaliada');
                 tr.removeClass('colaboracao-avaliada').addClass('colaboracao-nao-avaliada');
             } else {
                 span.removeClass('colaboracao-nao-avaliada').addClass('colaboracao-avaliada');
                 tr.removeClass('colaboracao-nao-avaliada').addClass('colaboracao-avaliada');
             }
         } else {
             span.removeClass('colaboracao-avaliada').addClass('colaboracao-nao-avaliada');
             tr.removeClass('colaboracao-avaliada').addClass('colaboracao-nao-avaliada');
         }
         updateAndamentoAvaliacaoFichaCompleta();
     }, '', 'JSON');
 };

 var gerarCitacao = function(params) {
     var $divResultado = $("#div-dsCitacao-" + params.sqFicha);
     var $campoDsCitacao =$("#dsCitacao"+params.sqFicha);
     params.sqOficina = $("#sqOficinaSelecionada").val();
     if( $campoDsCitacao.size()==1 && String($campoDsCitacao.val()).trim() ) {
         app.alertInfo('Para utilizar este recurso o campo autoria deve estar limpo, apague o seu conteúdo e tente novamente.',"Aviso");
         return;
     }
     app.confirm('<br>Confirma criação automática da citação?',
         function() {
             app.ajax(baseUrl + 'fichaCompleta/getCitacao', params, function(res) {
                 if( $divResultado.size()==1 ) {
                     $("#div-dsCitacao-" + params.sqFicha).html('<p>' + res + '</p>');
                 } else if( $campoDsCitacao.size() == 1) {
                   tinyMCE.get('dsCitacao' + params.sqFicha).setContent( res );
                 }
             }, '', 'TEXT');
         });

 }

var gerarAutoria = function(params) {
     var $divResultado = $("#div-dsCitacao-" + params.sqFicha);
     var $campoDsCitacao =$("#dsCitacao"+params.sqFicha);
     params.sqOficina = $("#sqOficinaSelecionada").val();
     if( $campoDsCitacao.size()==1 && String($campoDsCitacao.val()).trim() ) {
         app.alertInfo('Para utilizar este recurso o campo autoria deve estar limpo, apague o seu conteúdo e tente novamente.',"Aviso");
         return;
     }
     app.confirm('<br>Confirma criação automática da autoria?',
         function() {
             app.ajax(baseUrl + 'fichaCompleta/getAutoria', params, function(res) {
                 if( res.status == 0 ) {
                     // SALVE SEM CICLO
                     if( tinyMCE.activeEditor && $(tinymce.activeEditor.getElement()).data('campoFicha') == 'dsCitacao' ) {
                         tinymce.activeEditor.setContent(res.autoria );
                     } else if ($divResultado.size() == 1) {
                         $("#div-dsCitacao-" + params.sqFicha).html(res.autoria);
                     } else if ($campoDsCitacao.size() == 1) {
                         $campoDsCitacao.val(res.autoria);
                     }
                 }
             }, '', 'JSON');
         });

 }

 var gerarEquipeTecnica = function(params) {
     var $divResultado = $("#div-dsEquipeTecnica-" + params.sqFicha);
     var $campoDsEquipeTecnica =$("#dsEquipeTecnica"+params.sqFicha);
     params.sqOficina = $("#sqOficinaSelecionada").val();
     if( $campoDsEquipeTecnica.size()==1 && String($campoDsEquipeTecnica.val()).trim() ) {
         app.alertInfo('Para utilizar este recurso o campo deve estar limpo, apague o seu conteúdo e tente novamente.',"Aviso");
         return;
     }
     app.confirm('<br>Confirma criação automática da equipe técnica?',
         function() {
             app.ajax(baseUrl + 'fichaCompleta/getEquipeTecnica', params, function(res) {
                 if( res == ''){
                     app.alertInfo('Nenhuma pessoa encontrada.')
                 }
                 if( $divResultado.size()==1 ) {
                     $("#div-dsEquipeTecnica-" + params.sqFicha).html('<p>' + res + '</p>');
                 } else if( $campoDsEquipeTecnica.size() == 1) {
                     $campoDsEquipeTecnica.val( res );
                     tinyMCE.get('dsEquipeTecnica' + params.sqFicha).setContent( res );
                 }
             }, '', 'TEXT');
         });
  }

  var gerarColaboradores = function(params) {
     var $divResultado = $("#div-dsColaboradores-" + params.sqFicha);
     var $campoDsColaboradores =$("#dsColaboradores"+params.sqFicha);
     params.sqOficina = $("#sqOficinaSelecionada").val();
     if( $campoDsColaboradores.size()==1 && String($campoDsColaboradores.val()).trim() ) {
         app.alertInfo('Para utilizar este recurso o campo deve estar limpo, apague o seu conteúdo e tente novamente.',"Aviso");
         return;
     }
     app.confirm('<br>Confirma criação automática dos colaboradores?',
         function() {
             app.ajax(baseUrl + 'fichaCompleta/getColaboradores', params, function(res) {
                 if( res.status == 0 ) {
                     if (res.colaboradores == '') {
                         app.alertInfo('Nenhum participante com a função COLABORADOR cadastrado na avaliação ou que tenha contribuição ACEITA nas consultas/revisões');
                     }
                     if ($divResultado.size() == 1) {
                         $("#div-dsColaboradores-" + params.sqFicha).html(res.colaboradores);
                     } else if ($campoDsColaboradores.size() == 1) {
                         $campoDsColaboradores.val(res.colaboradores);
                     }
                 }
             }, '', 'JSON');
         });
 }

 var identificarPonto = function(params) {
     oMap.identifyPoint({ id: params.pontoId, bd: params.fonte }, true)
         /*var features
     params.center = params.center == false ? false : true
     var td = $("#tdFichaCompletaOcorrencia" + params.fonte + params.pontoId);
     var mapExtent = map.getView().calculateExtent(map.getSize())
     td.closest('tbody').find('td').removeClass('selected');
     td.addClass('selected');
     pontosLayer.getSource().forEachFeature(function(feature) {
         if (feature.getId() == params.pontoId && feature.get('bd') == params.fonte) {
             if (params.center || ol.extent.containsCoordinate(mapExtent, feature.getGeometry().getCoordinates())) {
                 mapAnimateFeature(map, pontosLayer, feature, 5, params.center);
                 if (params.center) {
                     document.location.href = "#panel-distribuicao-geografica";
                 }
                 params.center = false;
                 selectedPoint = params;
             }
         }
     });
    */
 }

 var chkGridOcorrenciaChange = function(obj) {
     var data;
     var $tr;
     if (obj) {
         data = $(obj).data();
         $tr = $(obj).closest('tr');
         if (obj.checked) {
             $tr.addClass('tr-selected');
         } else {
             $tr.removeClass('tr-selected');
         }
         if ($("#" + data.contexto + " input:checkbox:checked").size() === 1 && !$tr.parent().hasClass('growled')) {
             if ($("html").scrollTop() > 1186) {
                 $tr.parent().addClass('growled');
                 app.growl('Clique com o botão direito do mouse no gride para visualizar o menu de opções', 0, 'Dica!');
             }
             $("#div-alert-contextmenu").removeClass('hidden');
             for (var i = 0; i < 4; i++) {
                 $("#div-alert-contextmenu").fadeIn(200).fadeOut(200).fadeIn(200);
             }
         }
     }
 }


 var createContextMenu = function(divId, callback, newOptions, params) {
     newOptions = newOptions || [];

     var menuOptions = {
         naoUsada: {
             name: "NÃO utilizado na avaliação"
                 //,callback: function(key, opt, rootMenu, originalEvent){ alert("Foo!"); }
                 ,
             disabled: function(key, opt) {
                 return false;
             },
             icon: 'fa-ban'
         },
         usada: {
             name: "Utilizado na avaliação"
                 //, callback: function(key, opt, rootMenu, originalEvent){ alert("Bar!") }
                 ,
             icon: "fa-check"
         },
         excluir: {
             name: "Excluir"
                 //, callback: function(key, opt, rootMenu, originalEvent){ alert("Bar!") }
                 ,icon: "fa-remove"
         },
         restaurar: {
             name: "Restaurar"
                 //, callback: function(key, opt, rootMenu, originalEvent){ alert("Bar!") }
                 ,
             icon: "fa-history" // fa-recycle fa-history
         }
     };

     newOptions.map(function(item) {
         menuOptions[item.id] = item;
     });

     $.contextMenu('destroy', "#" + divId);


     $.contextMenu({
         // define which elements trigger this menu
         selector: "#" + divId,
         events: {
             show: function(options) {
                 if ($("#" + divId + " input:checked").size() == 0) {
                     app.alert('Nenhuma linha selecionada!');
                     return false;
                 }
                 return true;
             },
         },
         callback: function(itemKey, opt) {
             var qtdRegistrosSelecionados = 0;
             $("#" + divId.replace(/^#/, '') + ' input:checked:visible').each(function(i, input) {
                 qtdRegistrosSelecionados++;
             });
             var msgQtdRegistros = '';
             if (qtdRegistrosSelecionados < 2) {
                 msgQtdRegistros = qtdRegistrosSelecionados + ' registro selecionado.'
             } else {
                 msgQtdRegistros = qtdRegistrosSelecionados + ' registros selecionados.'
             }
             switch (itemKey) {
                 case 'naoUsada':
                     saveInUtilizadoAvaliacao({ sn: 'N', contexto: divId, callback: callback, sqFicha:params.sqFicha  });
                     break;
                 case 'usada':
                     app.confirm('<b>' + msgQtdRegistros + '</b><br><br>Confirma utilização na avaliação?', function() {
                         saveInUtilizadoAvaliacao({ sn: 'S', contexto: divId, callback: callback, sqFicha:params.sqFicha  });
                     });
                     break;
                 case 'excluir':
                     app.confirm('<b>' + msgQtdRegistros + '</b><br><br>Confirma EXCLUSÃO dos registros?', function() {
                         saveInUtilizadoAvaliacao({ sn: 'E', contexto: divId, callback: callback, sqFicha:params.sqFicha });
                     });
                     break;
                 case 'restaurar':
                     app.confirm('<b>' + msgQtdRegistros + '</b><br><br>Confirma RESTAURAÇÃO dos registros excluídos?', function() {
                         saveInUtilizadoAvaliacao({ sn: 'R', contexto: divId, callback: callback, sqFicha:params.sqFicha  });
                     });
                     break;
             }
         },
         // define the elements of the menu
         items: menuOptions
     });
 };

 var saveInUtilizadoAvaliacao = function(params) {
     if (params.sn == 'N' && !params.txJustificativa) {
         getJustificativa(params, saveInUtilizadoAvaliacao)
         return;
     }
     var data = app.params2data(params, {})
     data.ids = [];
     //data.tables=[];
     $("#" + data.contexto + ' input:checked:visible').each(function(i, input) {
         var ocorrencia = $(input).data();
         data.ids.push({ id: ocorrencia.id, bd: ocorrencia.bd });
         //data.tables.push( ocorrencia.bd );
     });
     //data.ids    = data.ids.join(',');
     //data.tables = data.tables.join(',');
     //data = 'data='+JSON.stringify(data)
     ajaxJson(baseUrl + 'fichaCompleta/saveInUtilizadoAvaliacao', data, 'Gravando...', function(res) {
         if (res.status == 0) {
             app.growl('Dados gravados com SUCESSO!', 2, '', 'tc', '', 'success');
             app.eval(data.callback, { updateMap: true });
             if (dlgJustificativa && dlgJustificativa.isOpened()) {
                 dlgJustificativa.close();
             }
         } else {
             alert('Atenção. Operação não autorizada!');
         }
     });
     /*app.ajax(baseUrl + 'fichaCompleta/saveInUtilizadoAvaliacao', data, function(res) {
         if( res )
         {
             app.alertError(res);
         }
         else
         {
             app.eval( data.callback );
             if( dlgJustificativa && dlgJustificativa.isOpened() )
             {
                 dlgJustificativa.close();
             }
         }
     }, 'Gravando...', 'TEXT');
     */
 }

 var getJustificativa = function(params, callback) {
     tempParams = {params: params, callback: callback};
     params.txJustificativa ? params.txJustificativa : '';
     params.sqMotivoNaoUtilizado ? params.sqMotivoNaoUtilizado : '';
     params.logJustificativa = '&nbsp;';
     if (params.txJustificativa) {
         var arrTemp = params.txJustificativa.trim().replace(/<br ?\/>/ig, '<br>').split('<br>');
         if (arrTemp.length > 1) {
             params.logJustificativa = arrTemp[arrTemp.length - 1];
             arrTemp.pop(); // remover o ultimo elemento do array
         }
         params.txJustificativa = arrTemp.join('\n');
     }
     if (!dlgJustificativa) {
         var html = '<div class="mt10 text-center">' +
             '<div style="text-align:left;display:block;padding:0px;margin:5px;margin-left:-5px">' +
             '<label>Motivo:</label> <select id="selectMotivoNaoUtilizacao" class="w100p" style=";"></select></div>' +
             '<textarea id="textoJustificativa" placeholder="Digite o texto da justificativa ou selecione um motivo" class="w100p" style="font-size: 1.7rem;padding:5px;min-height:200px;height:300px;margin-left:-10px;"></textarea><br/></div>' +
             '<div id="textoJustificativa-log" class="div-justificativa-log"></div>' +
             '<button class="btn btn-success" data-action="confirmJustificativa">Gravar Justificativa</button>';

         dlgJustificativa = app.dialog(html, 'Justificativa Não Utilização do(s) Registro(s)', null
             , 'default', 'Cancelar');
     } else {
         dlgJustificativa.open();
     }
     // aproveitar o texto da última justificativa
     if (params.txJustificativa) {
         $("div#" + dlgJustificativa.getId()).find('textarea').text(params.txJustificativa);
         ultimaJustificativa = params.txJustificativa;
     } else {
         $("div#" + dlgJustificativa.getId()).find('textarea').text(ultimaJustificativa);
     }
     $("div#" + dlgJustificativa.getId()).find('div.div-justificativa-log').html(params.logJustificativa);
     dlgJustificativa.getModal()[0].style.zIndex = 1050;
     app.focus('textoJustificativa');
     var $select = $("#selectMotivoNaoUtilizacao");
     if ( $select.size() == 1 && $select.find('option').size() == 0 ) {
         ajaxJson('/salve-estadual/fichaCompleta/getOpcoesNaoUtilizacaoOcorrencia', null, null, function (res) {
             if (res.data) {
                 res.data.map(function (item) {
                     var selected = ( ( ( item.codigoSistema == 'OUTRO'&& !params.sqMotivoNaoUtilizado) || String(params.sqMotivoNaoUtilizado) == String(item.id) )  ? ' selected' : '');
                     $select.append('<option' + selected + ' data-codigo="' + item.codigoSistema + '" value="' + item.id + '">' + item.descricao + '</option>');
                 });
                 if( $("#selectMotivoNaoUtilizacao option:selected").text() == $("#textoJustificativa").val() ) {
                     $("#textoJustificativa").val('');
                 }
             };
         });
     }
 };

 var confirmJustificativa = function(params) {
     var $select = $("#selectMotivoNaoUtilizacao");
     if( $select.find('option:selected').data('codigo') =='OUTRO') {
         if( $("#textoJustificativa").val().trim() == '' ) {
             app.growl('Necessário informar a justificativa.',3,'Mensagem','tc','large','error');
             app.focus('textoJustificativa');
             return;
         }
     }
     app.confirm('Confirma gravação da justificativa?', function() {
         tempParams.params.txJustificativa = $("#textoJustificativa").val().trim()
         tempParams.params.sqMotivoNaoUtilizado = $select.val()
         app.lsSet('textoJustificativa', tempParams.params.txJustificativa);
         app.eval(tempParams.callback, tempParams.params);
         tempParams = null;
     })
 };

 var checkRespostaPadraoAjusteRegionalClick = function(params, ele, evt) {
     app.setDefaultText(params, ele, evt);
     if (ele.checked) {
         $("select[name=sqTipoConectividade] option[data-codigo^=DESCONHECID]").prop('selected', true);
         $("select[name=sqTendenciaImigracao] option[data-codigo^=DESCONHECID]").prop('selected', true);
         $("select[name=stDeclinioBrPopulExterior] option[data-codigo=D]").prop('selected', true);
     } else {
         $("select[name=sqTipoConectividade]").val('');
         $("select[name=sqTendenciaImigracao]").val('');
         $("select[name=stDeclinioBrPopulExterior]").val('');
     }

 };


 /**
  *  MÓDULOS DA ABA AVALIAÇÃO DA FICHA E DA FICHA COMPLETA
  */

 var avaliacao = {
     // distribuicao
     updateGrafico: function(idForm,idInputVlTempoGeracional,idSelectSqMedidaTempoGeracional) {
         var $form = $('#'+idForm);
         if( $form.size()==0) {
             return;
         }
         var $grafico = $form.find('div#grafico');
         if( $grafico.size()==0) {
             return;
         }
         var $input = $form.find('input#'+idInputVlTempoGeracional);
         if( $input.size()==0) {
             return;
         }
         var medida='';
         var $select = $form.find('select#'+idSelectSqMedidaTempoGeracional);
         if( $select.size()==0) {
             var $inputMedida = $form.find('input#'+idSelectSqMedidaTempoGeracional);
             if( $inputMedida.size() == 1 ) {
                 medida = $inputMedida.data('codigo');
             }  else {
                 return;
             }
         } else {
             medida = $select.find("option:selected").data('codigo');
         }
         //$grafico.hide();
         var val = parseFloat($input.val().replace(',', '.'));
         var anoAvaliacao = parseInt($grafico.find("#divAnoAvaliacao").html());
         if (medida && ! isNaN(val) && !isNaN(anoAvaliacao)) {
             if ( medida ) {
                 if (medida == 'DECADA') {
                     val /= 10;
                 } else if (medida == 'DIA') {
                     val /= 365;
                 } else if (medida == 'MES') {
                     val /= 12;
                 } else if (medida == 'HORA') {
                     val /= 8760;
                 }
                 val = (val*3).toFixed()
                 if (val < 10) {
                     val = 10;
                 }
                 val = parseInt( val );
                 anoAvaliacao = parseInt( anoAvaliacao );
                 $grafico.find("#divAnoInicial").html( anoAvaliacao - val );
                 $grafico.find("#divAnoFinal").html( anoAvaliacao + val );
                 $grafico.find("#divMenosAnos").html(String(val * -1) + ' anos');
                 $grafico.find("#divMaisAnos").html('+' + String(val) + ' anos');
                 //$grafico.show('fast');
             }
         } else {
             $grafico.find("#divAnoInicial").html('?');
             $grafico.find("#divAnoFinal").html('?');
             $grafico.find("#divMenosAnos").html('+?');
             $grafico.find("#divMaisAnos").html('-?');
         }
     },

     saveFrmDistribuicao: function(params) {
         var data = $("#frmAvaDistribuicao").serializeArray();
         if (!data) {
             return;
         }
         data = app.params2data(params, data);
         if (!data.sqFicha) {
             app.alertError('ID da ficha não encontrado!');
             return;
         }
         app.ajax(app.url + 'fichaAvaliacao/saveFrmDistribuicao', data,
             function(res) {
                 if (res.status == 0) {
                     app.resetChanges('frmAvaDistribuicao');
                 }
             }, null, 'json'
         );
     },


     // populacao
     tabAvaPopulacaoInit: function(params) {
         avaliacao.updateGridDeclinioPopulacional(params);
         avaliacao.updateGrafico('frmPopulacaoAba112','vlTempoGeracional','sqMedidaTempoGeracional')
         /*
         if (!$("#sqMedidaTempoGeracional").val()) {
             $("#sqMedidaTempoGeracional option").each(function() {
                 if (/^Ano/.test($(this).text())) {
                     $("#sqMedidaTempoGeracional").val(this.value);
                 }
             });
         }
         avaliacao.updateGrafico();
         if (params.contexto == 'ficha') {
             app.focus('vlTempoGeracional');
             $("#vlTempoGeracional").on('blur', avaliacao.updateGrafico);
             $("#vlTempoGeracional").on('focus', function() {
                 $("#grafico").hide();
             });
         }*/
     },
     /**
      * Salvar formulário 11.2 - População
      * @param params
      */
     saveFrmPopulacao: function(params) {
         var data = $("#frmPopulacao").serializeArray();
         if ( !data || data.length == 0) {
             data = $("#frmPopulacaoAba112").serializeArray();
             if ( !data || data.length == 0 ) {
                 return;
             }
         }
         data = app.params2data(params, data);
         if (!data.sqFicha) {
             app.alertError('ID da ficha não encontrado!');
             return;
         }
         app.ajax(app.url + 'fichaAvaliacao/saveFrmPopulacao', data,
             function(res) {
                 if (res.status == 0) {
                     app.resetChanges('frmPopulacao')
                 }
             }, null, 'json'
         );
     },
     addDeclinioPopulacional: function(params) {
         var data = app.params2data(params, {});
         if (!data.sqDeclinioPopulacional) {
             app.alertInfo('Selecine um declínio populacional!')
             return;
         }
         app.ajax(app.url + 'fichaAvaliacao/addDeclinioPopulacional', params,
             function(res) {
                 if (res.status == 0) {
                     avaliacao.updateGridDeclinioPopulacional();
                     $("#sqDeclinioPopulacional").data('changed', false).val('');
                     app.isTabModified('liTabAvaPopulacao');

                 }
             }, null, 'json'
         );

     },
     deleteDeclinioPopulacional: function(params, btn) {
         app.selectGridRow(btn);
         app.confirm('<br>Confirma exclusão do declínio populacional <b>' + params.descricao + '</b>?',
             function() {
                 app.ajax(app.url + 'fichaAvaliacao/deleteDeclinioPopulacional',
                     params,
                     function(res) {
                         avaliacao.updateGridDeclinioPopulacional();
                     });
             },
             function() {
                 app.unselectGridRow(btn);
             }, params, 'Confirmação', 'warning');
     },
     updateGridDeclinioPopulacional: function(params) {
         params = params || {};
         if (!params.sqFicha) {
             params.sqFicha = $("#sqFicha").val();
             params.contexto = 'ficha';
             params.canModify = canModify;
         }

         var data = {
             sqFicha: params.sqFicha,
             canModify: params.canModify,
             contexto: params.contexto
         }
         app.ajax(baseUrl + 'fichaCompleta/getGridDeclinioPopulacional',
             data,
             function(res) {
                 var winPanel = '';
                 if (params.contexto != 'ficha') {
                     winPanel = '#pnlShowFichaCompleta_' + data.sqFicha + ' ';
                 }
                 $(winPanel + "#divGridDeclinioPopulacional").html(res);
             }, '', 'HTML');


     },
     saveFrmAnaliseQuantitativa: function(params) {
         var data = $("#frmAnaliseQuantitativa").serializeArray();
         if (!data) {
             return;
         }
         data = app.params2data(params, data);
         if (!data.sqFicha) {
             app.alertError('ID da ficha não encontrado!');
             return;
         }
         app.ajax(app.url + 'fichaAvaliacao/saveFrmAnaliseQuantitativa', data,
             function(res) {
                 if (res.status == 0) {
                     app.resetChanges('frmAnaliseQuantitativa');
                 }
             }, null, 'json'
         );
     },
     saveFrmAjusteRegional: function(params) {
         var data = $("#frmAjusteRegional").serializeArray();
         if (!data) {
             return;
         }
         data = app.params2data(params, data);
         if (!data.sqFicha) {
             app.alertError('ID da ficha não encontrado!');
             return;
         }
         app.ajax(app.url + 'fichaAvaliacao/saveFrmAjusteRegional', data,
             function(res) {
                 if (res.status == 0) {
                     app.resetChanges('frmAjusteRegional')
                 }
             }, null, 'json'
         );
     },

     // avaliação expedita
     tabAvaExpeditaInit: function() {
         avaliacao.stChangeExpedita();
     },
     stChangeExpedita: function(params) {
         // apenas exibir / esconder campos
         var st1 = $("#stFavorecidoConversaoHabitats")
         var st2 = $("#stTemRegistroAreasAmplas")
         var st3 = $("#stPossuiAmplaDistGeografica")
         var st4 = $("#stFrequenteInventarioEoo")
         st2.parent().addClass('hidden');
         st3.parent().addClass('hidden');
         st4.parent().addClass('hidden');
         if (!st1.val()) {
             return;
         }
         // pegunta 1 não é SIM
         if (st1.val() != 'S') {
             // exibir pergunta 2
             st2.parent().removeClass('hidden');
             if (!st2.val()) {
                 return;
             }
             if (st2.val() != 'S') {
                 // Exibir perguntas 3a e 3b somente se a reposta 1 for DESCONHECIDO ou a dois for DESCONHECIDO
                 if (st1.val() == 'D' || st2.val() == 'D') {
                     st3.parent().removeClass('hidden');
                     st4.parent().removeClass('hidden');
                 }
             }
         }
     },
     saveAvaliacaoExpedita: function(params) {
         // regras para alterar a categoria
         var st1 = $("#stFavorecidoConversaoHabitats");
         var st2 = $("#stTemRegistroAreasAmplas");
         var st3 = $("#stPossuiAmplaDistGeografica");
         var st4 = $("#stFrequenteInventarioEoo");
         var val;

         // aba resultado está aberta
         if ($("#sqCategoriaIucn").size() > 0) {
             if (st1.val() == 'S' || st2.val() == 'S' || (st3.val() == 'S' && st4.val() == 'S')) {
                 val = $("#sqCategoriaIucn").find('option[data-codigo=LC]')[0].value;
                 $("#sqCategoriaIucn").val(val);
                 $("#sqCategoriaIucn").change();
                 $("#sqCategoriaIucn").prop("disabled", true);
             } else if (st1.val() == 'N' && st2.val() == 'D' && st3.val() == 'N' && st4.val() == 'D') {
                 val = $("#sqCategoriaIucn").find('option[data-codigo=DD]')[0].value;
                 $("#sqCategoriaIucn").val(val);
                 $("#sqCategoriaIucn").change();
                 $("#sqCategoriaIucn").prop("disabled", true);
             } else if (st1.val() == 'N' && st2.val() == 'D' && st3.val() == 'D' && st4.val() == 'N') {
                 val = $("#sqCategoriaIucn").find('option[data-codigo=DD]')[0].value;
                 $("#sqCategoriaIucn").val(val);
                 $("#sqCategoriaIucn").change();
                 $("#sqCategoriaIucn").prop("disabled", true);
             } else if (st1.val() == 'N' && st2.val() == 'D' && st3.val() == 'D' && st4.val() == 'D') {
                 val = $("#sqCategoriaIucn").find('option[data-codigo=DD]')[0].value;
                 $("#sqCategoriaIucn").val(val);
                 $("#sqCategoriaIucn").change();
                 $("#sqCategoriaIucn").prop("disabled", true);
             } else {
                 $("#sqCategoriaIucn").prop("disabled", false);
             }
         }
         if (!st1.val() || st1.val() == 'S') {
             $("#stTemRegistroAreasAmplas").val('');
             $("#stPossuiAmplaDistGeografica").val('');
             $("#stFrequenteInventarioEoo").val('');
         }

         if (st1.val() != 'S') {

             if (!st2.val() || st2.val() == 'S') {
                 $("#stPossuiAmplaDistGeografica").val('');
                 $("#stFrequenteInventarioEoo").val('');
             } else {
                 if ((!st3.val() || !st4.val()) && (st1.val() == 'D' || st2.val() == 'D')) {
                     app.alertInfo('Campos 3a e 3b devem ser informados!')
                     return;
                 }
             }
         }
         var data = $("#frmAvaExpedita").serializeArray();
         if (!data) {
             return;
         }
         data = app.params2data(params, data);
         if (!data.sqFicha) {
             app.alertError('ID da ficha não encontrado!');
             return;
         }
         app.ajax(app.url + 'fichaAvaliacao/saveFrmAvaExpedita', data,
             function(res) {
                 if (res.status == 0) {
                     app.resetChanges('frmAvaExpedita');
                 }
             }, null, 'json'
         );
     },
     //------------------------------------------------------------------------

     // 11.6 - Resultado Avaliacao (oficina)
     tabAvaResultadoInit: function(params) {
         if (!params.container) {
             return;
         }
         app.resetChanges(params.container);
         // mostrar / esconder os campos de acordo com a categoria
         app.categoriaIucnChange(params);
     },

     // aba 11.7 - Resultado Validação
     tabValidacaoFinalInit: function(params) {
         if (!params.container) {
             return;
         }
         app.resetChanges(params.container);
         // mostrar / esconder os campos de acordo com a categoria
         app.categoriaIucnChange(params);
     },
     saveFrmResultadoFinal: function(params) {
         var data = $("#frmResultadoFinal").serializeArray();
         if (!data) {
             return;
         }
         data = app.params2data(params, data);
         if (!data.sqFicha) {
             app.alertError('ID da ficha não encontrado!');
             return;
         }
         app.ajax(app.url + 'fichaAvaliacao/saveFrmResultadoFinal', data,
             function(res) {
                 if (res.status == 0) {
                     app.resetChanges('frmResultadoFinal');
                 }
             }, null, 'json'
         );
     },

     //  Resultado validacao
     tabValidacaoInit: function() {
         // mostrar/esconder campos
         avaliacao.stCategoriaOkChange();
         avaliacao.stCriterioAvalIucnOkChange();
     },
     saveFrmValidacao: function(params) {
         var data = $("#frmValidacao").serializeArray();
         if (!data) {
             return;
         }
         data = app.params2data(params, data);
         if (!data.sqFicha) {
             app.alertError('ID da ficha não encontrado!');
             return;
         }
         if ($("#stCategoriaOk").val() == 'N' && $("#dsJustificativaValidacao").val().trim() == '') {
             app.alertInfo('Necessário informar a justificativa!')
             return;
         }
         if ($("#stCriterioAvalIucnOk").val() == 'S') {
             $("#dsCriterioAvalIucnSugerido").val('')
         }
         data.dsJustificativaValidacao = corrigirCategoriasTexto(data.dsJustificativaValidacao);
         app.ajax(app.url + 'fichaAvaliacao/saveFrmValidacao', data,
             function(res) {
                 if (res.status === 0) {
                     tinyMCE.get('dsJustificativaValidacao').setContent(res.data.dsJustificativaValidacao);
                 }
             }, null, 'json'
         );
     },
     sqCategoriaIucnSugestaoChange: function(params) {
         // se a categoria for 'VU,CR,EN não exibir o campo critério'
         if (($("#stCategoriaOk").val() == 'S') || !/EN|VU|CR/.test($("#frmValidacao #sqCategoriaIucnSugestao option:selected").data('codigo'))) {
             $("#dsCriterioAvalIucnSugerido").parent().addClass('hide');
         } else {
             if ($("#stCriterioAvalIucnOk").val() == 'N') {
                 $("#dsCriterioAvalIucnSugerido").parent().removeClass('hide');
             } else {
                 $("#dsCriterioAvalIucnSugerido").parent().addClass('hide');
             }
         }

     },
     stCategoriaOkChange: function(params) {
         if ($("#stCategoriaOk").val() == 'N') {
             $("#sqCategoriaIucnSugestao").parent().removeClass('hide');
         } else {
             $("#sqCategoriaIucnSugestao").parent().addClass('hide');
         }
         avaliacao.sqCategoriaIucnSugestaoChange();
     },
     stCriterioAvalIucnOkChange: function(params) {
         if ($("#stCriterioAvalIucnOk").val() == 'N') {
             $("#dsCriterioAvalIucnSugerido").parent().removeClass('hide');
         } else {
             $("#dsCriterioAvalIucnSugerido").parent().addClass('hide');;
         }
     },

     sqCategoriaFinalChange: function(params) {
         alert('Ops, chamou fichaCompleta.js avaliacao.sqCategoriaFinalChange() +- linha 2563')
     },

     // modal comentário validadores
     openModalComentario: function(params) {
         if (!params.id) {
             app.alertInfo('Id não informado');
             return;
         }

         function _adjustHeight(panel) {
             var footerH = panel.content.find('.panel-footer').height();
             var headerH = panel.header.height();
             var desconto = 76;
             var h = (parseInt(panel.content.height() - (headerH + footerH + desconto))) / 2;
             var fld1 = panel.find('#fldComentarioAvaliador_ifr');
             var fld2 = panel.find('#fldNovoComentrioChat_ifr');
             fld1.css('height', h + "px");
             fld2.css('height', h + "px")
                 //$("#fldComentarioAvaliador_ifr").css('height',h+"px")
                 //$("#fldNovoComentrioChat_ifr").css('height',h+"px")
         }
         params.modalName = "cadComentarioValidador";
         $.jsPanel({
             id: "ficha_comentario_valiador" + params.id,
             theme: "#6FA04B",
             modal: true,
             headerTitle: 'Comentário Valiador - [nome do validador aqui]',
             contentSize: {
                 width: 700,
                 height: 600
             },
             theme: "bootstrap-success",
             content: '<div id="div_content_cad_comentario_validador' + params.id + '" style="border:none;display:block;min-height:100%;"<br><br><p>Carregando...' + window.grails.spinner + '</p></div>',
             resizable: true,
             onnormalized: function() {
                 _adjustHeight(this);
             },
             onmaximized: function() {
                 _adjustHeight(this);
             },
             callback: function(panel) {
                 this.find('.jsglyph-minimize').hide();
                 this.content.css("padding", "15px");
                 this.headerTitle(params.noPessoa);
                 panel.find('.jsPanel-content').prop('id', panel.attr('id') + '_body' + params.id)
                 app.loadModule(app.url + 'ficha/getModal', params, panel.attr('id') + '_body' + params.id, function(res) {

                 }, '');
             },
         });
     },
     saveComentario: function(params) {
         app.alertInfo('Ainda não implementado!')
     }
 };

/**
 * fazer a paginação do gride de ocorrencias da ficha completa
 * @param params
 * @param fld
 * @param evt
 */
var updatePaginaGridOcorrencias = function(params, fld, evt) {
    if( params.contexto && params.sqFicha ) {
        var paginaSelecionada = fld.value
        var data = {sqContexto: params.contexto, sqFicha: params.sqFicha
            , paginationTotalPages  : params.paginationTotalPages
            , paginationTotalRecords: params.paginationTotalRecords
            , paginationPageNumber  : paginaSelecionada
        };
        data.filtroSelecionado = $('input[name=radioFiltroGrideOcorrencias]:checked').val();
        var $div = $("#div-gride-ocorrencias-"+data.sqFicha);
        $div.css('min-height', $div.height() + 'px');
        $div.css('height', $div.height() + 'px');
        updateGridOcorrencias(data);
    }
}

/**
 * filtrar o gride de registros da ficha completa, registros com ou sem colaboração
 * @param params
 * @param fld
 * @param evt
 */
var radioFiltroGrideOcorrenciasClick = function(params, fld, evt ) {
    $("#selectPaginacaoGrideOcorrencias").data('paginationTotalRecords','');
    $("#selectPaginacaoGrideOcorrencias").data('paginationTotalPages','');
    $("#selectPaginacaoGrideOcorrencias").change();
}
