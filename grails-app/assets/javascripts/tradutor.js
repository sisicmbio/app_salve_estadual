//# sourceURL=/salve-estadual/assets/tradutor.js
;var tradutor = {

    txTraduzido:'',
    /**
     * fazer a requisição ajax para as APIs de tradução com tempo de timeout
     * @param resource
     * @param options
     * @returns {Promise<Response>}
     */
    fetchWithTimeout: async function (resource, options = {}) {
        var {timeout = 10000} = options;
        var controller = new AbortController();
        var id = setTimeout(() => controller.abort(), timeout);
        var response = await fetch(resource, {...options, signal: controller.signal});
        clearTimeout(id);
        return response;
    },


    /**
     * traduzir texto
     * @param params
     * @param fld
     * @param evt
     */
    traduzir: function (txOriginal,callback) {
        txOriginal = tradutor.prepararTextoAntesTraduzir( txOriginal );
        tradutor.txTraduzido= '';
        if ( ! txOriginal ) {
            app.alertInfo('Informe a palavra ou o texto para traduzir');
            return;
        }
        app.blockUI('Traduzindo...', async function () {
            try {
                // utilizar o google PRIMEIRO
                await tradutor.useGoogle(txOriginal);
                if( ! tradutor.txTraduzido ){
                    // utilizar o LibreTranslate
                    traduzir.useLibre(txOriginal);
                }
            } catch (e1) {
                try {
                    // utilizar o LibreTranslate
                    await tradutor.useLibre(txOriginal);
                } catch( e2 ){
                    app.unblockUI();
                    app.alertError('Não foi possível fazer a tradução.\n\nErro: ' + e2.message );
                    return;
                }
            }
            app.unblockUI();
            if( callback ) {
                tradutor.txTraduzido = tradutor.prepararTextoDepoisTraduzir(tradutor.txTraduzido );
                setTimeout(function(){
                    callback(tradutor.txTraduzido);
                },1000);

            }
        }, null, null, 10002)
    },

    /**
     * utilizar o serviço google para fazer a tradução
     * @param text
     * @returns {Promise<string|*>}
     */
    useGoogle: async function ( sourceText ) {
        /*var sourceLang = 'pt-br';
        var targetLang = 'en';
        var url = "https://translate.googleapis.com/translate_a/single?client=gtx&sl="
            + sourceLang + "&tl=" + targetLang + "&dt=t&q=" + encodeURI(sourceText);*/
        var url = grails.urlGoogleTranslate + encodeURI(sourceText);
        tradutor.txTraduzido = '';
        try {
            var res = await tradutor.fetchWithTimeout(url);
            var dados = await res.json();

            var paragrafos = [];
            if (dados[0][0][0]) {
                dados[0].forEach(function (item) {
                    paragrafos.push(item[0])
                });
                tradutor.txTraduzido = paragrafos.join('\n')
            }
        } catch (e) {
            throw e;
        }
    },

    /**
     * Utilizar o serviço do LibreTranslate para fazer a tradução
     * @param sourceText
     * @returns {Promise<void>}
     */
    useLibre: async function ( sourceText ) {
        var format='text';
        tradutor.txTraduzido = '';
        if( /<|>/.test( sourceText) ){
            format='html'
        }
        try {
            // UTILIZAR O SALVE/TRADUZIR porque o servico libretranslate.icmbio.gov.br só é acessado via VPN
            var url = baseUrl + 'traduzir'
            var res = await tradutor.fetchWithTimeout(url+'?texto='+sourceText,{
                method: "GET"
            });
            var jsonData = await res.json();
            if( jsonData.data.traducao ){
                tradutor.txTraduzido = jsonData.data.traducao
            }
            /*
            // accessar o servico libretranslate.icmbio.gov.br/translate diretamente
            //var url = grails.urlLibreTranslate
            var res = await tradutor.fetchWithTimeout(url,{
                method: "POST",
                body: JSON.stringify({
                    q: sourceText,
                    source: "pt",
                    target: "en",
                    format: format
                }),
                headers: {"Content-Type": "application/json"}
            });
            var jsonData = await res.json();
            tradutor.txTraduzido = jsonData.translatedText
             */
        } catch( e ){
            throw e;
        }
   },
    /**
     * Remover tags html desnecessárias, espaços em branco, quebra de linhas etc
     * @param texto
     * @returns {*}
     */
   prepararTextoAntesTraduzir:function( texto ) {
       // remover css font-xx
       texto = texto.replace(/ ?font[a-zA-Z0-9-]{0,}: ?[0-9a-zA-Z#!% "',-]{1,}; ?/gim,'');

       // substituir os & para eamp; para não dar problema na requisição get
       texto = texto.replace(/&amp;/gm, '&');
       texto = texto.replace(/&/gm, 'eamp;');

       // mascarar hashtag
       texto = texto.replace(/#/gm, 'enum;');

       // remover style=""
       texto = texto.replace(/style=""/gim,'');
       return texto
   },
    /**
     * voltar os caracteres e tags html que foram preservadas para o formato original
     * @param texto
     * @returns {*}
     */
   prepararTextoDepoisTraduzir:function( texto ) {
       // voltar os eamp; para &
       texto = texto.replace(/eamp;/gm, '&');
       texto = texto.replace(/enum;/gm, '#');

       // se for uma palavra simples ou composta, apolicar capitalização
       if( /^[a-z]/.test( texto ) ){
           texto = texto.ucFirst();
       }
       return texto
   }
}
