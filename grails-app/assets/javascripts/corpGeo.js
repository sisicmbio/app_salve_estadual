//# sourceURL=corpGeo.js
/**
 * funções de GEO para manutenção das tabelas corporativas que possuem upload de shapefile
 */
;var corpGeo = {
    centerBrazil: [-6382868.225332894, -1872948.6151603064],
    /**
     * função para inicializar e exibir o mapa base na div informada
     * @param divId
     * @returns map
     */
    initMap:function( divId ) {
        if( ! divId ) {
            alert('Id da DIV para criação do mapa não informado');
            return;
        }

        if( $("#"+divId ).size()== 0){
            alert('Id da DIV ' + divId + ' para criação do mapa é inexistente');
            return;

        }
        // criar a camada open street map
        var osm = new ol.layer.Tile({
            source: new ol.source.OSM({
                maxZoom: 19,
                crossOrigin: 'anonymous'
            }),
            visible: true,
            zIndex: 1
        });

        // criar o estilo dos poligonos
        var style = new ol.style.Style({
            // cor preenchimento
            fill: new ol.style.Fill({
                color: 'rgba(0, 0, 255, 0.1)'
            }),
            // cor da borda
            stroke: new ol.style.Stroke({
                color: '#000079',
                width: 1
            }),
        });

        // criar layer para desenhar os poligonos do shapefile. o id padrão do layer será "layerShapefile"
        var shapeFileLayer = new ol.layer.Vector({
            source: new ol.source.Vector({}),
            style:style,
            id:'layerShapefile',
            zIndex:2,
            visible: true
        });

        // criar mapa base
        return new ol.Map({
            view: new ol.View({
                center:corpGeo.centerBrazil,
                zoom :4,
                projection: new ol.proj.Projection({
                    code: 'EPSG:3857',
                    units: 'm'
                }),
            }),
            layers: [osm,shapeFileLayer],
            target: divId
        })
    },
    /**
     * função para exibir shapefile a partir de um arquivo zip
     * carregado no navegador por um input type file
     * @param objFile
     * @param objMap
     * @param layerId
     */
    loadShapefile : function( objFile, objMap, layerId, fit ) {
        var layer;
        var reader;
        if( ! objFile ) {
            alert('Arquivo shapefile não selecionado');
            return;
        }
        if( ! objMap ) {
            alert('Mapa não informado!');
            return;
        }
        // criar estilo para preenchimento do polígono
        var style = new ol.style.Style({
            // cor preenchimento
            fill: new ol.style.Fill({
                color: 'rgba(0, 0, 255, 0.1)'
            }),
            // cor da borda
            stroke: new ol.style.Stroke({
                color: '#000079',
                width: 1
            }),
        });

        objMap.getLayers().forEach( function( lyr ) {
            if (lyr.get('id') == layerId ) {
                layer = lyr;
            }
        });
        if( !layer ){
            alert('Layer ' + layerId + ' não encontrado');
        }

        if( objFile && /\.zip$/.test( objFile.name ) ) {
            try {
                reader = new FileReader();
                reader.onload = function() {
                    if( reader.readyState == 2 ) {
                        shp( reader.result ).then( function( geojson ) {
                            var features = corpGeo.parseGeojson( geojson );
                            layer.getSource().clear();
                            layer.getSource().addFeatures( features );
                            if( fit ){
                                corpGeo.fit( objMap, layerId );
                            }
                        },function() {
                            alert('Arquivo shapefile inválido.')
                        });
                    } else if( reader.error ) {
                        alert('Erro na leitura do arquivo shapefile.');
                    }
                };
                reader.readAsArrayBuffer(objFile);
            } catch( e ) {
                alert( e );
            }
        }
    },
    /**
     * função para limpar o shapefile carregado no mapa
     * @param objMap
     * @param layerId
     */
    clearLayer:function( objMap, layerId ){
        if( objMap && layerId ) {
            objMap.getLayers().forEach(function (layer) {
                if (layer.get('id') == layerId) {
                    layer.getSource().clear()
                    objMap.getView().setZoom(4);
                    objMap.getView().setCenter( corpGeo.centerBrazil );
                }
            });
        }
    },
    /**
     * função para criar o wkt do(s) poligono(s) carregado(s) via upload do shapefile
     * @param objMap
     * @param layerId
     * @returns string
     */
    generateLayerWKT : function( objMap, layerId ) {
        var featureWkt, modifiedWkt;
        var unionFeatures = [];
        var wkt = new ol.format.WKT();
        var featuresFound = 0;
        var isMultipolygon = false;
        var layer;
        var maxFeatures = -1;
        if (!objMap) {
            return ''
        }
        // localizar o layer
        objMap.getLayers().forEach(function (l) {
            if (l.get('id') == layerId) {
                layer = l
            }
        });
        if (layer.getSource().getFeatures().length == 0) {
            return ''
        }
        layer.getSource().forEachFeature(function (f) {
            if ( maxFeatures < 1 || unionFeatures.length < maxFeatures) {
                var featureClone = f.clone();
                featureClone.getGeometry().transform('EPSG:3857', 'EPSG:4326');
                featureWkt = wkt.writeFeature(featureClone);
                if (featureWkt.match(/GEOMETRYCOLLECTION/g)) {
                    isMultipolygon = true;
                    featureWkt = (featureWkt.replace(/GEOMETRYCOLLECTION/g, '')).slice(1, -1);
                }
                if (featureWkt.match(/MULTIPOLYGON/g)) {
                    isMultipolygon = true;
                    modifiedWkt = (featureWkt.replace(/MULTIPOLYGON/g, '')).slice(1, -1);
                } else {
                    modifiedWkt = (featureWkt.replace(/,/g, ', ')).replace(/POLYGON/g, '');
                }
                unionFeatures.push(modifiedWkt);
            }
        });
        if ( unionFeatures.length == 0) {
            return ''
        }
        // sempre gravar como multipolygon mesmo sendo um poligono
        /*if ( ! isMultipolygon ) {
            return 'POLYGON' + unionFeatures[0]
        }*/
        return 'MULTIPOLYGON(' + unionFeatures.join(',') + ')'
    },
    /**
     * função para exibir no mapa o geojson retornado do banco de dados
     * @param objMap
     * @param layerId
     * @param geojson
     */
    showGeojson : function( objMap, layerId, geojson, fit ) {
        if( !objMap ){
            return;
        }
        var targetDiv = objMap.getTarget();
        app.blockElement(targetDiv,'Atualizando...',3000,function(){
            corpGeo.clearLayer(objMap,'layerShapefile');
            var validFeatures = corpGeo.parseGeojson( geojson );
            if( validFeatures ) {
                objMap.getLayers().forEach(function (layer) {
                    if (layer.get('id') == layerId ) {
                        layer.getSource().addFeatures( validFeatures )
                    }
                });
            }
            app.unblockElement(targetDiv);
            // encaixar o shape no mapa
            if( fit ) {
                corpGeo.fit(objMap, layerId );
            }
        })
    },
    /**
     * função para ajustar a projeção e eliminar pontos inválidos
     * @param geojson
     */
    parseGeojson : function( geojson ) {
        try {
            var features = (new ol.format.GeoJSON()).readFeatures(geojson, {featureProjection: 'EPSG:3857'});
            features = features.filter(function (feature, i) {
                try {
                    var geometry = feature.getGeometry();
                    if (geometry.getType() == 'Point') {
                        var coords = geometry.getCoordinates();
                        if (isNaN(coords[1]) || isNaN(coords[0]) || Math.abs(coords[0]) === Infinity) {
                            return false;
                        } else {
                            return true;
                        }
                    } else {
                        return true;
                    }
                } catch (e) {
                    return false;
                }
            });
            return features;
        } catch (e) {
            return null;
        }
    },
    /**
     * enquadrar o poligono no mapa e ajustar o zoom para caber
     * @param objMap
     * @param layerId
     */
    fit:function(objMap, layerId){
        var layer;
        setTimeout(function(){
            // localizar o layer
            objMap.getLayers().forEach(function (l) {
                if (l.get('id') == layerId) {
                    layer = l
                }
            });
            if (layer.getSource().getFeatures().length == 0) {
                return ''
            }
            //var center = ol.proj.transform(ol.extent.getCenter( layer.getSource().getExtent()), 'EPSG:3857', 'EPSG:4326');
            var extent = layer.getSource().getExtent();
            if( extent ) {
                objMap.getView().fit(layer.getSource().getExtent(), objMap.getSize() );
            }
        },1000);
    }
}