// This is a manifest file that'll be compiled into application.js.
//
// Any JavaScript file within this directory can be referenced here using a relative path.
//
// You're free to add application-wide JavaScript to this file, but it's generally better
// to create separate JavaScript files as needed.
//
// -------------- JQUERY -------------------------
//=require jquery/v224/jquery.min

// -------------- VUE JS -------------------------
//=require vuejs/vue.global.prod.min.js

// -------------- MODERNIZR ------------------------- https://johannburkard.de/blog/programming/javascript/highlight-javascript-text-higlighting-jquery-plugin.html
//=require modernizr/modernizr-custom

//--------------- JQUERY HIGHLIGHT ------------------ https://johannburkard.de/blog/programming/javascript/highlight-javascript-text-higlighting-jquery-plugin.html
//=require jquery/plugins/highlight/jquery.highlight.min

//--------------- DATATABLES ------------------ https://datatables.net/
//=require jquery/plugins/datatables/datatables.min
//=require jquery/plugins/datatables/dataTables.searchHighlight.min
//=require jquery/plugins/datatables/datatables_customizacoes // ajustes para pesquisar/ordenar pelos campos com input selects

// -------------- BOOTSTRAP-----------------------
//=require bootstrap/v336/bootstrap.min // https://blog.getbootstrap.com/2015/11/24/bootstrap-3-3-6-released/
// utializado pelo jquery.dataTable()
//=require bootstrap/plugins/multiselect/bootstrap-multiselect.min // https://davidstutz.de/projects/twitter-bootstrap-plugins/

// -------------- JSTORAGE -----------------------
//=require jstorage/jstorage.min // http://www.jstorage.info/#download ou https://raw.githubusercontent.com/andris9/jStorage/master/jstorage.js
//=require FileSaver.min.js

// ----------------  jsPanel 3.6 ------------------------
//=require jquery/plugins/jsPanel/jquery.jspanel.min   // site: http://jspanel.de/ ou http://jspanel.de/api/#gettingstarted

// -------------- JS PARA FANCYTREE -----------------------
//=require fancytree/jquery-ui.min
//=require fancytree/jquery.fancytree-all.min
// require fancytree/jquery.contextMenu.min
//=require jquery/plugins/contextmenu/jquery.contextMenu.min
// require jquery/plugins/contextmenu/jquery.ui.position.min
//=require fancytree/jquery.fancytree.contextMenu.min

// -------------- PLUGINS -----------------------
//=require jquery/plugins/msg/msg.min                   // site: http://dreamerslab.com/blog/en/jquery-blockui-alternative-with-jquery-msg-plugin/
//=require jquery/plugins/cachedScript/cachedScript     // site: https://api.jquery.com/jquery.getscript/
//=require jquery/plugins/center/center.min             // site: http://dreamerslab.com/blog/en/centralize-html-dom-element-with-jquery-center-plugin/
//=require jquery/plugins/blockUI/blockUI.min           // site: http://jquery.malsup.com/block/
//=require bootstrap/plugins/dialog/dialog.min          // site: http://nakupanda.github.io/bootstrap3-dialog/
//=require jquery/plugins/growl/growl.min               // site: https://github.com/ksylvest/jquery-growl
//=require jquery/plugins/validation/jquery.validate.min // site: https://jqueryvalidation.org/validate ou https://jqueryvalidation.org/documentation/
//=require jquery/plugins/tooltipster/tooltipster.bundle.min // site: http://iamceege.github.io/tooltipster/#demos
//=require jquery/plugins/mask/jquery.mask.min          // site http://igorescobar.github.io/jQuery-Mask-Plugin/ ou site: http://www.igorescobar.com/blog/2012/03/13/mascaras-com-jquery-mask-plugin/
//=require jquery/plugins/select2/select2.full.min                  // site: https://select2.github.io/
//=require jquery/plugins/select2/i18n/pt-BR.min
//=require jquery/plugins/floathead/jquery.floatThead.min   // site: http://mkoryak.github.io/floatThead/

// --- plugin file-input ---
//=require jquery/plugins/fileinput/plugins/purify.min
//=require jquery/plugins/fileinput/fileinput.min       // site  http://plugins.krajee.com/file-input
//=require jquery/plugins/fileinput/locales/pt-BR.min
// -----------------------------------------------

// trocar a gijgo para a JsPanel
//=require gijgo/dialog/js/dialog.min

// -------------- OPENLAYERS -------------------------
//=require openlayers/ol465.min
//=require openlayers/ol3-contextmenu.min
//=require openlayers/geocoder/ol-geocoder.min // https://github.com/jonataswalker/ol-geocoder

// ---------- TURF -----------------
//=require turf/turf.min

// ------------- importar SHP
// require openlayers/zip2shp/proj4
// require openlayers/zip2shp/lib/jszip
// require openlayers/zip2shp/lib/jszip-utils
// require openlayers/zip2shp/preprocess.js
// require openlayers/zip2shp/preview
//------------------------------ outra biblioteca para importar shp
//=require openlayers/shp.min.js

// -------------- JSPDF -------------------------
//=require jspdf/jspdf.min.js                   // site https://parall.ax/products/jspdf/examples/basic.html


// ---------------- MENDELEY-BIBTEXT ---------------------
//=require mendeley/bibtexParse.min


// -------------------- INTROJS -----------------
// require introjs/intro.min // https://introjs.com/docs/


// -------------------- Salve WebWorker -----------------
//=require workers/createWorker

// Integração SIS/IUCN
// -------------------- Serviços de Tradução -----------------
//=require tradutor.js


// -------------------- ENJOY HINT ---------------
// require enjoyhint/enjoyhint.min.js // https://github.com/xbsoftware/enjoyhint/
// outros exemplos // http://ninodezign.com/25-free-jquery-plugins-for-doing-guided-tours-through-a-website/
// http://eragonj.github.io/Trip.js/demo/basic/
// http://linkedin.github.io/hopscotch/
// http://bootstraptour.com/api/
// http://iamdanfox.github.io/anno.js/
// http://clu3.github.io/bootstro.js/index.html#
// https://www.jqueryscript.net/demo/Easy-Interactive-Visual-Tour-Plugin-For-jQuery-mytour/
// passo a passo explicando cada campo
// http://keaplogik.github.io/Bootstrap-Clean-Dashboard-Theme/demo/form.html#vtour


// ---------------- SELF -------------------------
// require_tree .
//= require app
//= require_self
var appPing;
window.onload = function(event) {
    function appStart() {
        if ( $("#divAppLoading").size() > 0) {
            $("#divAppLoading").remove();
            setTimeout(appStart, 500);
            return;
        }

        // Centralizar oreplace(/;$/,'');s dialogos modal do boostrap
        $('.modal').on('show.bs.modal', function () {
            $(this).show();
            setModalMaxHeight(this);
        });
        $(window).resize(function () {
            if ($('.modal.in').length != 0) {
                setModalMaxHeight($('.modal.in'));
            }
        });

        startPing();

        // exibir a logomarca e a animação inicial.
        try {
            app.run();
        } catch (e) {
            //document.write(e.message);
            $("#divAppLoading h3").html('<span class="red">Erro no arquivo app.js</span>');
            return;
        }
        if (typeof (validador) == 'object' && typeof (validador.init) == 'function') {
            validador.init();
        }
        try {
            $("div#divNewSession").show('slow');
        } catch (e) {
        }
    }
    appStart();
}

function startPing(){
    // evitar que a sessão caia por inatividade
    stopPing();
    appPing = window.setInterval(function () {
        $.post(baseUrl + 'main/ping',function (res) {
            if (res != '') {
                if ( $("#divNewSession").size() == 0 && $("#frmLogin").size() == 0 && ! $("#appDivMainMenu").text() == '') {
                    if( !grails.sisicmbio) {
                        window.clearInterval( appPing );
                        app.alert(res);
                    } else {
                        restoreSession({silent:true});
                    }
                }
            }
        }).error(function (xhr, status, exception) {
            if (xhr.status == 0 && $("#divNewSession").size() == 0 && $("#frmLogin").size() == 0) {
                if (!grails.sisicmbio) {
                    window.clearInterval(appPing);
                    app.alert('Sessão expirou ou sistema está em manutenção.<br>Efetue login novamente.', 'Ops!!')
                } else {
                    restoreSession({silent:true});
                }
            }
        });
    }, 60000 * 5); // 5 minutos
}

function stopPing(){
    window.clearInterval(appPing);
}
/*
$(function() {
    return;
    // Centralizar oreplace(/;$/,'');s dialogos modal do boostrap
    $('.modal').on('show.bs.modal', function() {
        $(this).show();
        setModalMaxHeight(this);
    });
    $(window).resize(function() {
        if ($('.modal.in').length != 0) {
            setModalMaxHeight($('.modal.in'));
        }
    });

    // evitar que a sessão caia por inatividade
    appPing = window.setInterval(function() {
        //console.log( 'Ping...');
        $.post(baseUrl + 'main/ping', function(res) {
            if (res != '') {
                window.clearInterval(appPing);
                if ($("#divNewSession").size() == 0) {
                    app.alert(res);
                }
            }
        }).error(function(xhr, status, exception) {
            log(xhr.status)
            log(status)
            log(exception)
            window.clearInterval(appPing);
            if (xhr.status == 0 && $("#divNewSession").size() == 0) {
                app.alert('Sessão expirou ou sistema está em manutenção.', 'Ops!!', function() {
                    //window.location.replace(baseUrl);
                });
            }
        });
    }, 60000 * 5); // 5 minutos

    // exibir a logomarca e a animação inicial.
    window.setTimeout(function(){
        try {
            app.run();
        } catch( e ){
            //document.write(e.message);
            $("#divAppLoading h3").html('<span class="red">Erro no arquivo app.js</span>');
            return;
        }
        if( typeof(validador) == 'object' && typeof( validador.init) == 'function' ) {
            validador.init();
        }
        try {
            $("div#divNewSession").show('slow');
        } catch (e) {
        }
    },2000); // tempo para exibir a logo

});
*/

/* funções de ajuste para as janelas modal do bootstrap */
function setModalMaxHeight(element) {
    this.$element = $(element);
    this.$content = this.$element.find('.modal-content');
    var borderWidth = this.$content.outerHeight() - this.$content.innerHeight();
    var dialogMargin = $(window).width() < 768 ? 20 : 60;
    var contentHeight = window.innerHeight - (dialogMargin + borderWidth);
    var headerHeight = this.$element.find('.modal-header').outerHeight() || 0;
    var footerHeight = this.$element.find('.modal-footer').outerHeight() || 0;
    var maxHeight = contentHeight - (headerHeight + footerHeight);
    this.$content.css({
        'overflow': 'hidden'
    });
    this.$element.find('.modal-body').css({
        'max-height': maxHeight,
        'overflow-y': 'auto'
    });
}
/**
 * [formValidate description]
 *
 * @param {[type]}
 *            formId [description]
 * @param {[type]}
 *            rules [description]
 * @param {[type]}
 *            onPass [description]
 * @param {[type]}
 *            onError [description]
 * @param {[type]}
 *            cleanFocus [description]
 * @param {[type]}
 *            debug [description]
 * @return {[type]} [description]
 * @site https://jqueryvalidation.org/validate
 */
function formValidate(formId, rules, messages, onSuccess, onError, cleanFocus, debug) {
    debug = (debug === true ? true : false);
    cleanFocus = (cleanFocus === true ? true : false);
    formId = '#' + formId.replace('#', '');
    $(formId).validate({
        debug: debug,
        focusCleanup: cleanFocus,
        errorClass: 'error',
        validClass: 'valid',
        success: function(label, element) {
            try {
                // http://iamceege.github.io/tooltipster/#demos
                $(element).tooltipster('hide');
            } catch (e) {}
            label.addClass("valid");
        },
        errorPlacement: function(error, element) {
            if ($(error).text()) {
                try {
                    $(element).tooltipster('content', $(error).text());
                    $(element).tooltipster('show');
                } catch (e) {}
            }
            // var name = $element.attr("name");
            // $("#error" + name).append($error);
        },
        submitHandler: function(form) {
            onSuccess && onSuccess(form)
        },
        invalidHandler: function(event, validator) {
            if (onError) {
                onError(event, validator);
            } else {
                // var errors = validator.numberOfInvalids();
                // app.alertError('Existe(m) ' + errors + ' erro(s) no
                // formulário.');
            }
        },
        rules: rules,
        messages: messages
    });
}

// adicionar o método isCNPJ ao tipo String
String.prototype.isCNPJ = function() {
    var b = [6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2],
        c = this;
    if ((c = c.replace(/[^\d]/g, "").split("")).length != 14) return false;
    for (var i = 0, n = 0; i < 12; n += c[i] * b[++i])
    ;
    if (c[12] != (((n %= 11) < 2) ? 0 : 11 - n)) return false;
    for (var i = 0, n = 0; i <= 12; n += c[i] * b[i++])
    ;
    if (c[13] != (((n %= 11) < 2) ? 0 : 11 - n)) return false;
    return true;
};

// adicionar o método isCPF ao tipo String
String.prototype.isCPF = function() {
    var c = this;
    if ((c = c.replace(/[^\d]/g, "").split("")).length != 11) return false;
    if (new RegExp("^" + c[0] + "{11}$").test(c.join(""))) return false;
    for (var s = 10, n = 0, i = 0; s >= 2; n += c[i++] * s--)
    ;
    if (c[9] != (((n %= 11) < 2) ? 0 : 11 - n)) return false;
    for (var s = 11, n = 0, i = 0; s >= 2; n += c[i++] * s--)
    ;
    if (c[10] != (((n %= 11) < 2) ? 0 : 11 - n)) return false;
    return true;
};

String.prototype.isEmail = function(){
    var c = this;
    if( c.trim() == '' ) {
        return true;
    } else {
        var res = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return res.test(c.toLowerCase());
    }
}

String.prototype.stripHTML = function() {return this.replace(/<.*?>/g, '');}

// Exemplo de utilização
var txt = "<p>este é <u>apenas</u> um <b>teste</b> para a função <i>stripHTML</i>.</p>";
txt = txt.stripHTML();
Number.prototype.formatMoney = function(c, d, t){
    var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t)
        + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

function msieversion() {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");
    if (msie > 0) // If Internet Explorer, return version number
    {
        return true;
        //alert(parseInt(ua.substring(msie + 5, ua.indexOf(".", msie))));
    } else // If another browser, return 0
    {
        return false;

    }
    return false;
}

function hex2rgba(x, a) {
    a = (a == undefined) ? 1 : a;
    var r = x.replace('#', '').match(/../g),
        g = [],
        i;
    for (i in r) {
        g.push(parseInt(r[i], 16));
    }
    g.push(a);
    return 'rgba(' + g.join() + ')';
}

/**
 *  remover os parágrafos vazios do inicio e fim do texto devolvido pelo tinyMce
 */
function trimEditor(texto) {
    if (typeof tinyMCE != 'undefined') {
        texto = tinyMCE.trim(texto);
    }
    texto = texto.trim();
    return texto.replace(/^<p>&nbsp;<\/p>/gm, '')
    .replace(/<p>&nbsp;<\/p>?$/gm, '')
    .replace(/^<div>&nbsp;<\/div>/gm,'')
    .replace(/<div>&nbsp;<\/div>$/gm,'')
    .replace(/^<p>&nbsp;<\/p>/gm,'').trim();
}

/**
 * Gerar cor randomica
 * @return {[type]} [description]
 */
function getRandomColor() {
    var hex = Math.floor(Math.random() * 0xFFFFFF);
    return "#" + ("000000" + hex.toString(16)).substr(-6);
}

/**
 * Gerar sequencia de caracteres randomica
 * @param length
 * @returns {string}
 */
function getRandomString(length) {
    length = length || 10;
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

/** codificar e decodificar caracteres utf-8
 * @type {{encode: UTF8.encode, decode: UTF8.decode}}
 */
UTF8 = {
    encode: function(s){
        for(var c, i = -1, l = (s = s.split("")).length, o = String.fromCharCode; ++i < l;
            s[i] = (c = s[i].charCodeAt(0)) >= 127 ? o(0xc0 | (c >>> 6)) + o(0x80 | (c & 0x3f)) : s[i]
        );
        return s.join("");
    },
    decode: function(s){
        for(var a, b, i = -1, l = (s = s.split("")).length, o = String.fromCharCode, c = "charCodeAt"; ++i < l;
            ((a = s[i][c](0)) & 0x80) &&
            (s[i] = (a & 0xfc) == 0xc0 && ((b = s[i + 1][c](0)) & 0xc0) == 0x80 ?
                o(((a & 0x03) << 6) + (b & 0x3f)) : o(128), s[++i] = "")
        );
        return s.join("");
    }
};

/**
 * função para evitar que uma função seja executada repetidas vezes antes do intervalo determinado
 * pelo usuário
 * @example var minhaFuncao = debounce(apertarTecla,2000); minhaFuncao('teste');
 * @param func
 * @param timeout
 * @returns {(function(...[*]): void)|*}
 */
function debounce(func, timeout = 500){
    let timer;
    return (...args) => {
        clearTimeout(timer);
        timer = setTimeout(() => { func.apply(this, args); }, timeout);
    };
}


/**
 * função para trocar os acentos pelo seu correspondente sem acento
 * @param texto
 * @returns {string}
 */
var removerAcento = function( texto ) {
    return String(texto).normalize('NFD').replace(/[\u0300-\u036f]/g, "");
}


/**
 * função para localizar e filtrar as linhas do gride que possuirem o texto digitado pelo usuario
 * A table a ser processada deve possuir o id no formato "tableNomeDoGride". Ex: tableGridFichas
 * e o input do texto deve passar o id do gride na propriedade data-grid-id. Ex: data-grid-id="GridFichas"
 * @param ele
 */
var gridSelectRows = debounce(function(ele) {
    var data = $(ele).data();
    $(ele).prop('disabled',true);
    $("table#table"+data.gridId.capitalize()).find('tbody tr').map(function(index,tr){
        var linhaValida = false;
        $(tr).find('td').map(function(index2,td ){
            if( !linhaValida) {
                var texto = removerAcento($(td).text().trim().replace(/(\n|\s{2,})/gm,' ') );
                if ( String( texto ).toLowerCase().indexOf( removerAcento( String(ele.value).trim().toLowerCase() ) ) > -1) {
                    linhaValida = true;
                    return false;
                }
            }
        })
        if( linhaValida ){
            $(tr).show();
        } else {
            $(tr).hide();
        }
    })
    $(ele).prop('disabled',false);
    $(ele).focus();

},700 );



/*function startTour() {
    var tour = introJs();
    tour.setOption('tooltipPosition', 'right');
    //tour.setOption('positionPrecedence', ['right', 'left',  'bottom', 'top']).
    tour.setOption('nextLabel', 'Próximo');
    tour.setOption('prevLabel', 'Anterior');
    tour.setOption('skipLabel', 'Pular');
    tour.setOption('doneLabel', 'Encerrar');
    tour.start();
}
*/
/*
function getTiff(div,fileName)
{
    if(msieversion()==false)
    {
        var xhr = new XMLHttpRequest();
        xhr.responseType = 'arraybuffer';
        xhr.open('GET', baseUrl+"ficha/getAnexo/31");
        xhr.onload = function (e)
        {
            var tiff = new Tiff({buffer: xhr.response});
            var canvas = tiff.toCanvas();
            canvas.width=60;
            canvas.height=50;
            $("#xyz").html('').append( canvas )
            //document.body.append(canvas);
        };
        xhr.send();
    }
}
*/
/*
$(function(){
    if(msieversion()==false){
        var xhr = new XMLHttpRequest();
        xhr.responseType = 'arraybuffer';
        xhr.open('GET', "./image.tif");
        xhr.onload = function (e) {
            var tiff = new Tiff({buffer: xhr.response});
            var canvas = tiff.toCanvas();
            document.body.append(canvas);
        };
        xhr.send();
    }else{
        var imgElem=$('<img src="image.tif" />');
        $('body').append(imgElem);
    }
})
 */
