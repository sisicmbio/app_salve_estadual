/**
 * javascript para módulo de assinatura eletrônica do documento final
 * https://github.com/mozilla/pdf.js/blob/master/examples/learning/helloworld.html
 */
var objEmbed;

var pdfDoc = null,
    pageNum = 1,
    pageRendering = false,
    pageNumPending = null,
    scale = 2,
    canvas = '',
    ctx = null;

// The workerSrc property shall be specified.
pdfjsLib.GlobalWorkerOptions.workerSrc = '/salve-estadual/assets/jspdf/pdf.worker.js';

(function () {
    var maxH = window.innerHeight - (150 + $(".app-navbar").height() );
    var fileName = $("#fileName").val();
    var body = $("#panelBody");
    var maxW = body.width();
    var url = '/salve-estadual/main/download?fileName='+fileName;
    //var url = '/salve-estadual/main/download?fileName='+fileName+'&disposition=filename&#page=1&toolbar=0&navpanes=0&scrollbar=0&view=FitH';
    objEmbed = null;

    canvas = document.getElementById('the-canvas');
    ctx = canvas.getContext('2d');
    document.getElementById('prev').addEventListener('click', onPrevPage);
    document.getElementById('last').addEventListener('click', onLastPage);
    document.getElementById('next').addEventListener('click', onNextPage);

    var updating=false;
    var $btnAssinar = $("#btnAssinar");
    if( $btnAssinar.size() == 0 && objEmbed ) {
        maxH += 40;
        window.setTimeout(function(){
            BootstrapDialog.alert({size:BootstrapDialog.SIZE_SMALL, closable:true,title:'Informação',message:'Assinatura já registrada!'});
        },1500);
    }
    var maxH = window.innerHeight - ( 150 + $(".app-navbar").height() );
    //var maxW = $(window).width()-50;
    $("#viewPdf").css({'height':maxH+'px','maxHeight':maxH+'px'})

    /*
    // ajustar a altura do pdf ao redimensionar a janela
    $( window ).on('resize', function(){
        var win = $(this);
        try {
            maxH = $(win).height() - ( 150 + $(".app-navbar").height() );
            if ($btnAssina.size() == 0) {
                maxH += 40;
            }
            if (!updating) {
                updatePdf();
            }
        } catch ( e ) {}
    });*/

    var updatePdf = function( params ){
        /**
         * Asynchronously downloads PDF.
         */
        pdfjsLib.getDocument(url).promise.then(function(pdfDoc_) {
            pdfDoc = pdfDoc_;
            document.getElementById('page_count').textContent = pdfDoc.numPages;
            // Initial/first page rendering
            renderPage(pageNum);
        });
    }

    /**
     * atualizar o pdf na tela
     */
    var updatePdfOld = function(params){
        if(params && params.page )
        {
            url = url.replace(/page=1&/,'page='+params.page+'&');
        }
        var htmlEmbed = '<embed type="application/pdf"  id="objPdf" ' +
            'style="overflow-y: visible; width:'+maxW+'px; ' +
            'min-height:'+maxH+'px" ' +
            'src="'+ url +'" alt="pdf" ' +
            'pluginspage="http://www.adobe.com/products/acrobat/readstep2.html">';

        //var htmlEmbed = '<div id="xx" style="overflow-y: visible;background-color: blue;width:'+maxW+'px;min-height:'+maxH+'px">';
        //htmlEmbed = '<iframe src="http://docs.google.com/gview?url=http://localhost:8080/salve-estadual/main/download?fileName='+fileName+'&embedded=true" style="width:'+maxW+'px; height:'+maxH+'px;" frameborder="0"></iframe>';
        //htmlEmbed = '<iframe src="http://localhost:8080/salve-estadual/main/download?fileName='+fileName+'&embedded=true" style="width:'+maxW+'px; height:'+maxH+'px;" frameborder="0"></iframe>';
        /*var x='http://localhost:8080/salve-estadual/main/download?fileName='+fileName;
        htmlEmbed ='<object data="'+x+'">' +
            '<p>Oops! You don\'t support PDFs!</p>' +
            '<p><a href="'+x+'">Download Instead</a>' +
            '</object>';*/

        //updating = true;
        //$("embed").remove();
        //objEmbed = body.append( htmlEmbed );
        //updating=false;
    };

    /**
     * adicionar o evento click no botao btnAssinar
     */
    $btnAssinar.on('click',function(ele){
        var dialog = BootstrapDialog.confirm({title:'Confirmação'
            , message  : 'Confirma assinatura eletrônica?'
            , btnCancelLabel:'Sim'
            , btnOKLabel:'Não'
            , btnOKClass:'btn-danger'
            , btnCancelClass:'btn-success'
            , closable: true
            , animate: false
            , draggable: true
            , closeByBackdrop: true
            , closeByKeyboard: true
            , size : BootstrapDialog.SIZE_SMALL
            , type : BootstrapDialog.TYPE_PRIMARY
            , callback : function( result ) {
                // os botões Sim e Não foram trocados para que o sim fique na esquerda
                // então ! result é SIM
                if( ! result) {
                    $.ajax({
                        method: "POST",
                        url: "/salve-estadual/api/assinarDocFinalOficina",
                        dataType: 'JSON',
                        data: {hash: $('#hash').val(), assinar:'S'},
                        beforeSend : function() {
                            blockUi('Assinando. Aguarde...');
                        }
                    }).done(function( res ) {
                        $.unblockUI({
                            onUnblock: function () {
                                $btnAssinar.remove();
                                maxH += 40;
                                if( res.status == 0 )
                                {
                                    updatePdf();
                                    onLastPage();
                                }
                                if ( res.msg ) {
                                    blockUi(res.msg, res.status == 0 ? 2000:5000,res.status==0?"#050":"#500");
                                }
                            }
                        });
                    }).fail(function(jqXHR,textStatus,msg) {
                        $.unblockUI({
                            onUnblock: function () {
                                blockUi('Erro: ' + jqXHR.status + ' - ' + jqXHR.statusText, 10000, "#500");
                            }
                        });
                    });
                }
            }
        });
    });

    /**
     * desbloquear a tela
     */
    var unblockUi = function()
    {
        $.unblockUI();
    };

    /**
     * bloquear a tela com mensagem para o usuário
     * @param msg
     * @param timeout
     */
    var blockUi = function( msg, timeout, color )
    {
        timeout = (typeof( timeout) == 'undefined') ? 10000 : timeout
        color = (typeof( color ) == 'undefined'?'#000':color);
        $.blockUI({
            message: '<span style="font-size:1.5rem !important;">'+msg+'</span>',
            overlayCSS: {backgroundColor: '#000'},
            timeout: timeout,
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: color,
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .6,
                color: '#fff'
            }
        });
    };
    updatePdf();
})();


/**
 * Get page info from document, resize canvas accordingly, and render page.
 * @param num Page number.
 */
function renderPage(num) {
    pageRendering = true;
    // Using promise to fetch the page
    pdfDoc.getPage(num).then(function(page) {
        var viewport = page.getViewport({scale: scale});
        canvas.height = viewport.height;
        canvas.width = viewport.width;

        // Render PDF page into canvas context
        var renderContext = {
            canvasContext: ctx,
            viewport: viewport
        };
        var renderTask = page.render(renderContext);

        // Wait for rendering to finish
        renderTask.promise.then(function() {
            pageRendering = false;
            if (pageNumPending !== null) {
                // New page rendering is pending
                renderPage(pageNumPending);
                pageNumPending = null;
            }
        });
    });

    // Update page counters
    document.getElementById('page_num').textContent = num;
}

/**
 * If another page rendering in progress, waits until the rendering is
 * finised. Otherwise, executes rendering immediately.
 */
function queueRenderPage(num) {
    if (pageRendering) {
        pageNumPending = num;
    } else {
        renderPage(num);
    }
}

/**
 * Displays previous page.
 */
function onPrevPage() {
    if (pageNum <= 1) {
        return;
    }
    pageNum--;
    queueRenderPage(pageNum);
}

/**
 * Displays next page.
 */
function onNextPage() {
    if (pageNum >= pdfDoc.numPages) {
        return;
    }
    pageNum++;
    queueRenderPage(pageNum);
}

/**
 * Displays last page.
 */
function onLastPage() {
    if (pageNum == pdfDoc.numPages) {
        return;
    }
    pageNum = pdfDoc.numPages;
    queueRenderPage(pageNum);
}
