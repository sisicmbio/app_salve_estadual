/**
 * javascript para módulo de validação de documento com codigo de acesso
 */
(function () {
    var maxH = window.innerHeight - 165;
    var $body = $("#panelBody");
    $body.height( maxH );
    var $codigoAcesso   = $("#codigoAcesso");
    var $btnValidar     = $("#btnValidar");
    var fileName        = '';
    var apiKey          ='6Lc76qQUAAAAAFWXugs0BTxi5pLvPS2MbhQZsZYy';

    // adicionar evento click no botão validar
    $btnValidar.on('click',function(){
        $codigoAcesso.parent().removeClass('has-error');
        if( ! $.trim( $codigoAcesso.val() ) )
        {
            $codigoAcesso.parent().addClass('has-error');
            blockUi('Código de acesso não informado',2000,'#f00');
            $codigoAcesso.focus();
            return;
        }

        blockUi('Verificando. Aguarde...',10000,'#000');
        grecaptcha.ready(function() {
            try {
                grecaptcha.execute(apiKey, {action: 'homepage'}).then(function (token) {
                    if (!token) {
                        alert('Erro token Recaptcha!');
                        return;
                    }
                    // fazer a requisição para validar o codigo informado
                    $.ajax({
                        method: "POST",
                        url: "/salve-estadual/validarDoc",
                        dataType: 'JSON',
                        data: {codigoAcesso: $codigoAcesso.val(), 'token': token},
                        beforeSend: function () {
                        }
                    }).done(function (res) {
                        $.unblockUI({
                            onUnblock: function () {
                                if (res.status == 0) {
                                    if (res.fileName) {
                                        fileName = res.fileName;
                                        $body.html('');
                                        blockUi('Carregando PDF...');
                                        updatePdf();
                                        unblockUi();
                                        window.setTimeout(function () {
                                            $("#btnVoltar").show('slow');
                                        }, 1500);
                                    }
                                    ;
                                }
                                ;
                                if (res.msg) {
                                    blockUi(res.msg, res.status == 0 ? 2000 : 3000, res.status == 0 ? "#050" : "#500");
                                };
                            }
                        });
                    });
                });
            } catch ( e ) {
                blockUi(e, 10000, "#500");
            }
        });
    });


    // efeturar a consulta com a tecla <enter>
    $codigoAcesso.on('keyup',function(evt){
        $codigoAcesso.parent().removeClass('has-error');
        if( evt && evt.keyCode == 13 )
        {
            if( (evt.keyCode && evt.keyCode == 13) || (evt.which && evt.which == 13 ) ) {
                $btnValidar.click();
            }
        }
    });

    // ajustar a altura do panel ao redimensionar a janela
    $( window ).on('resize', function(){
        var win = $(this);
        maxH = $(win).height() - 165;
        $body.height( maxH );
    });

    /**
     * atualizar o pdf na tela
     */
    var updatePdf = function()
    {
        var url = '/salve-estadual/main/download?fileName='+fileName+'&disposition=filename&#toolbar=0&navpanes=0&scrollbar=0&view=FitH';
        var htmlEmbed = '<embed id="objPdf" src="' + url +'" width="100%" height="100%" alt="pdf" pluginspage="http://www.adobe.com/products/acrobat/readstep2.html">'
        $("embed").remove();
        $body.append( htmlEmbed );
    };

     /**
     * desbloquear a tela
     */
    var unblockUi = function()
    {
        $.unblockUI();
    };

    /**
     * bloquear a tela com mensagem para o usuário
     * @param msg
     * @param timeout
     */
    var blockUi = function( msg, timeout, color )
    {
        timeout = (typeof( timeout) == 'undefined') ? 10000 : timeout
        color = (typeof( color ) == 'undefined'?'#000':color);
        $.blockUI({
            message: '<span style="font-size:1.5rem !important;">'+msg+'</span>',
            overlayCSS: {backgroundColor: '#000'},
            timeout: timeout,
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: color,
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .6,
                color: '#fff'
            }
        });
    };

    // quando passar o codigo de acess pela url, disparar a consulta automaticamente.
    if( $codigoAcesso.val() )
    {
        $btnValidar.click();
    }
})();
