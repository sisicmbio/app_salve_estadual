//# sourceURL=/salve-estadual/assets/javascripts/workers/salveWorker.js
//console.log( 'SALVE WORKER CARREGADO COM SUCESSO!');

/**
 * variáveis globais do webworker
*/
var updateChatSetTimeout=null;
var chatUpdateInSeconds = 5;
var chatStopped = true;

/**
 * receber as tarefas que deverão ser executadas em background
 * A mensagem deve ser o nome de uma função a ser executada.
 * @param event
 */
self.onmessage = function( event){
    var data = event.data;
    if( typeof self[data.message] == 'function' ) {
        self[data.message]( data );
    }
}

/**
 * interromper as requisições de verificação de novas mensagens no chat
 */
var stopChat = function(){
    chatStopped=true;
    // cancelar a requisição agendada
    clearTimeout(updateChatSetTimeout);
}

/**
 * atualizar as mensagens do chat na tela do usuário
 * @param params
 */
var updateChat = function( params ) {

    // canclear a requisição agendada se houver
    stopChat();

    var url = '/salve-estadual/fichaCompleta/getGridChats';

    // proxima requisição está valendo
    chatStopped=false;

    try {

        // lert todas as conversas novamente
        /*if( params.data && params.data.starting && params.data.lastId > 0) {
            console.log(' LER TODAS AS CONVERSAR NOVAMENTE');
            params.data.lastId=0;
        }*/
        postData( url, params.data
            ,function(res){
                if( res.error ) {
                    throw new DOMException(res.error);
                }
                // enviar o lastId nas próximas requisicoes para ler somente as novas mensagens
                params.data.lastId = res.lastId;
                params.data.lastUpdate = res.lastUpdate;
                params.data.hashRespostas = res.hashRespostas;

                // retornar para a aplicação o valor de message, sqOficinaFicha e sqFicha
                res.message = params.message;
                res.sqFicha = params.data.sqFicha;
                res.sqOficinaFicha = params.data.sqOficinaFicha;
                res.starting = params.data.starting;

                // chat pode ter sido parado durante a requisição
                if( ! chatStopped) {
                    // agendar a pŕoxima requisição
                    updateChatSetTimeout = setTimeout(function () {
                        params.data.starting = false;
                        updateChat(params);
                    }, chatUpdateInSeconds * 1000);

                    // remover tabs,retorno de linhas  e comentários html da renderizaçao da view fichaCompleta/divGridChats
                    res.htmlChat = res.htmlChat.trim().replace(/(\t|\n)/g, '').replace(/>\s+</g, '><').replace(/<!--(.+)-->/g, '');
                    postMessage(res);
                }
        },function( error ){
           params.error = error;
           postMessage( params );
        });
    } catch( e ) {
        params.error = e.message;
        postMessage(params);
    }
}

/**
 * realizar as requições ajax via POST
 * @param url
 * @param data
 * @param onLoad
 * @param onError
 */
function postData(url = '', data = {}, onLoad, onError ) {
    var xmlhttp=null
    if (typeof XMLHttpRequest == 'function' ) {
        xmlhttp = new XMLHttpRequest();
    } else {
        // code for old IE browsers
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    if( ! xmlhttp ) {
        throw  new DOMException('Erro na criação do objeto XMLHttpRequest');
    }

    //xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xmlhttp.onerror = function(error){
        onError( error );
    };

    xmlhttp.onload = function() {
        try {
            onLoad( JSON.parse( xmlhttp.responseText ) )
        } catch( e ) {
            onLoad({ error : e })
        }
    };

    xmlhttp.onreadystatechange = function(evt) {
        if ( this.status == 404) {
            onError( url + ' não encontrada!');
        }
    };

    var formData = new FormData();
    for( key in data) {
        if ( key != 'data') {
            formData.append(key, data[ key ]);
        }
    }
    xmlhttp.open("POST", url, true);
    xmlhttp.send(formData);
}
