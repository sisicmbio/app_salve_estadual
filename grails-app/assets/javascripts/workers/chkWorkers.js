//# sourceURL=chkWorkders.js
var window  = self;
var interval= 3; // segundos
var tries   = 0; // tentativas antes de cancelar
var url;
if( window.origin ){
    url = window.origin + '/salve-estadual/main/getWorkers';
}
else {
    url = window.location.origin + '/salve-estadual/main/getWorkers';
}

var running = false;
var firstTime = true;
self.onmessage = function( event ) {
    switch (event.data.command) {
        case 'start': {
            if( running )
            {
                return;
            }
            // tempo para o worker ser criado na sessão antes de começar
            running=true;
            firstTime=true;
            setTimeout('run()', 2000);
            break;
        }
    }
};

function run() {
    var xmlhttp = _getXmlhttp();
    if (!xmlhttp) {
        //clearInterval(timmer);
        postMessage({error: 'Erro: chkWorkers.js, navegador não suporta AJAX.'});
        return;
    }
    ;

    //xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xmlhttp.onerror = function () {
        //clearInterval(timmer);
        postMessage({error: 'Ocorreu um erro de ajax no chkWorkers.js'});
    };

    xmlhttp.onload = function () {
        var timer=null;
        try {
            var obj = eval(xmlhttp.responseText);
            if (obj.length == 0 && firstTime ) {
                // aguardar 5s para o worker ser criado na sessão
                timer = setTimeout('run()', 5000);
                firstTime=false;
                return;
            }
            postMessage(obj);
            // enquanto houver worker processando, chamar run() novamente
            if ( obj.length > 0 )  {
                timer = setTimeout('run()', interval * 1000);
            }
            else
            {
                running =false;
            }
        } catch (e) {
            //console.log(e);
            if( timer ) {
                clearTimeout(timer);
            }
            running =false;
            postMessage({error: e});
        }
    };

    xmlhttp.onreadystatechange = function (evt) {
        sending = false;
        if (this.readyState == 4 && this.status == 200) {
            tries = 0;
        } else if (this.status == 404) {
            if (tries++ > 5) {
                postMessage({error: 'chkWorkers.js url ' + url + ' não encontrada!'})
            }
        }
    };
    xmlhttp.open("POST", url, true);
    xmlhttp.send();
}

function _getXmlhttp()
{
    var xmlhttp=null
    if (typeof XMLHttpRequest == 'function' ) {
        xmlhttp = new XMLHttpRequest();
    } else {
        // code for old IE browsers
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    return xmlhttp;
}
