//# sourceURL=/salve-estadual/assets/javascripts/workers/createWorker.js
window.salveWorker = {
    worker : null,
    divMessages: null,
    scrollToBottom:true,
    scrollChatMax:0,
    create : function(){
        if( salveWorker.worker instanceof Worker) {
            salveWorker.worker.terminate();
        }
        salveWorker.worker = new Worker(baseUrl + 'assets/workers/salveWorker.js');
        salveWorker.worker.onmessage = salveWorker.onMessage;
    },
    onMessage : function( event ) {
        var res = event.data
        if( res.error ){
            app.alertError(String(res.error),'Mensagem');
        } else {
            // padrão: funcao callback sempre será a mensagem original + Callback
            try {
                var callback = res.message + 'Callback';
                if (typeof (salveWorker[callback]) == 'function') {
                    salveWorker[callback](res);
                } else if (typeof (window[callback]) == 'function') {
                    window[callback](res);
                }
            } catch( e ) {
                console.info( e.message )
            }
        }
    },

    // iniciar leitura das mensagens do chat
    startChat:function( data ){
        salveWorker.stopChat();
        data.starting=true;
        salveWorker.worker.postMessage({"message":"updateChat",'data' : data } );
        salveWorker.divMessages = $("#divGridChat"+data.sqFicha);
        if( salveWorker.divMessages.size() == 0 ) {
            return;
        }
        salveWorker.divMessages.off('scroll').on('scroll', function(event){
            salveWorker.scrollToBottom = salveWorker.scrollTopPosition(event.target) >= salveWorker.scrollChatMax;
        })
    },
    scrollTopPosition:function( ele ) {
        return  ele.pageYOffset !== undefined ? ele.pageYOffset : ele.scrollTop;
    },
    // parar as requisições do chat
    stopChat:function() {
        salveWorker.worker.postMessage({"message": "stopChat"});
    },
    scrollChatToBottom:function(){
        salveWorker.scrollToBottom=true;
    }

}
salveWorker.create();

/**
 * função executada com o retorno do salveWorker.updateChat()
 * @example salveWorker.startChat({sqFicha:1,sqOficinaFicha:1});
 * @param data
 */
var updateChatCallback = function( params ) {

    //salveWorker.stopChat(); // clearTimeout
    /*if ( params.sugestoes && params.sugestoes.length > 0) {
        salveWorker.stopChat(); // clearTimeout
        console.log( params);
    }*/
    //salveWorker.stopChat(); // clearTimeout
    //console.log( params);
    if( params.sqFicha && salveWorker.divMessages && salveWorker.divMessages.size() == 1 ) {
        var $wrapper = $("#divContainerChat" + params.sqFicha);
        var $formChat = $wrapper.find('form#frmChat');
        var $divChatMessages = $wrapper.find("div.chat");

        // verificar se formulario foi fechado
        if ( $divChatMessages.size() == 0 ) {
            if( salveWorker.divMessages.size() == 1 ) {
                salveWorker.divMessages.off('scroll');
            }
            salveWorker.stopChat(); // clearTimeout
            return;
        }
        // adicionar a nova mensagem no final
        if (params.htmlChat) {
            if( params.starting ) {
                $divChatMessages.html('');
            }
            $divChatMessages.append(params.htmlChat);
        }

        // mostrar/esconder os botões
        if( params.exibirBotaoEnviar === false ) {
            if( $formChat.is(':visible') ) {
                $formChat.hide();
                params.starting=true;
            }
        } else {
            if( ! $formChat.is(':visible') ) {
                $formChat.show();
                params.starting=true;
            }
        }

        // aguardar a renderização da tela
        window.setTimeout(function(){
            // se for a primeira vez ajustar a altura da tela do chat
            if( params.starting ) {
                ajustarAlturaTelaChat( params );
            }
            // verificar se tem que posicionar na última conversa
            if( salveWorker.scrollToBottom ) {
                $divChatMessages.scrollTop( $divChatMessages[0].scrollHeight );
                salveWorker.scrollChatMax = $divChatMessages.scrollTop();
            }
        },(params.starting ? 2000 : 100 ) );

        // proteger os campos de acordo com o contexto que o chat está sendo exibido
        $btnSaveResultadoFinal = $("#btnSaveResultadoFinal" + params.sqFicha ); // se existir o botão é porque ainda pode ser modificada
        var validada = false;
        if( $btnSaveResultadoFinal.size() == 1 ) {
            if ( /^VALIDADA/.test( params.cdSituacaoFichaSistema ) ) {
                validada=true;
                $("#sqCategoriaFinal" + params.sqFicha+',#stPossivelmenteExtinta'+ params.sqFicha).prop('disabled', true);
                $("#btnSelCriterioAvaliacao" + params.sqFicha).hide();
                $("#dsCriterioAvalIucnFinal" + params.sqFicha).prop('disabled',true);
                //$btnSaveResultadoFinal.hide();
                //tinymce.get("dsJustificativaFinal" + params.sqFicha).setMode('readonly');
            } else if (params.cdSituacaoFichaSistema == 'EM_VALIDACAO') {
                $("#sqCategoriaFinal" + params.sqFicha+',#stPossivelmenteExtinta'+ params.sqFicha).prop('disabled', false);
                $("#btnSelCriterioAvaliacao" + params.sqFicha).show();
                $("#dsCriterioAvalIucnFinal" + params.sqFicha).prop('disabled',false);
                //$btnSaveResultadoFinal.show();
                //tinymce.get("dsJustificativaFinal" + params.sqFicha).setMode('design');
            }

            if ( params.sugestoes && params.sugestoes.length > 0) {
                //console.log( params )
                // atualizar os campos com a resposta do validador
                var msg = [];
                var qtdA=0;
                var qtdB=0;
                params.sugestoes.map(function (item, i) {
                    var $div = $("#divValidadorFicha-" + item.sqValidadorFicha);
                    if( item.coResultado=='RESULTADO_A')
                    {
                        qtdA++;
                    }
                    else if( item.coResultado=='RESULTADO_B')
                    {
                        qtdB++;
                    }
                    if ($div.size() > 0) {
                        var num = $div.data('num');
                        var logAlteracoesRealizadas = [];
                        for (key in item) {
                            //console.log( key );
                            var $ele = $("#" + key + num);
                            if ($ele.size() > 0) {
                                var valorItem = String(item[key]).trim();
                                if ( /^tx/.test(key)) {
                                    if( String($ele.text()).trim() != $(valorItem).text().trim() ) {
                                        if( key == 'txJustificativa') {
                                            logAlteracoesRealizadas.push('o texto da fundamentação');
                                        }
                                        $ele.html(valorItem);
                                    }
                                } else if(/^stPossivelmenteExtinta/.test(key) ) {
                                    var oldValue = $ele.html().trim()
                                    if( oldValue != valorItem ) {
                                        if( valorItem == '' ) {
                                            logAlteracoesRealizadas.push('desmarcou PEX');
                                        } else {
                                            logAlteracoesRealizadas.push('marcou PEX');
                                        }
                                    }
                                    $ele.html(valorItem);
                                    if( valorItem ){
                                        $ele.removeClass('hide');
                                    } else {
                                        $ele.addClass('hide');
                                    }
                                } else {
                                    if( String($ele.val()).trim() != valorItem ) {

                                        if( key == 'dsResultado'){
                                            var oldValue = String( $("#dsResultado"+num).val() ).trim();
                                            logAlteracoesRealizadas.push('o resultado' + (oldValue ? ' de '+oldValue:'')  +' para ' + (item.dsResultado ? item.dsResultado : 'nenhum') );
                                        } else if( key == 'sqCategoriaSugerida'){
                                            var oldValue = String( $("#dsCategoriaSugerida"+num).val() ).trim();
                                            logAlteracoesRealizadas.push('a categoria' + (oldValue ? ' de '+oldValue : '') +' para ' + ( item.dsCategoriaSugerida ? item.dsCategoriaSugerida : 'nenhuma') );
                                        } else if( key == 'dsCriterioSugerido'){
                                            var oldValue = String( $("#dsCriterioSugerido"+num).val() ).trim();
                                            logAlteracoesRealizadas.push('o critério' + (oldValue ? ' de '+oldValue : '' )  +' para ' + (item.dsCriterioSugerido ? item.dsCriterioSugerido : 'nenhum') );
                                        }
                                        $ele.val(valorItem);
                                    }
                                }
                            }
                        }

                        if( logAlteracoesRealizadas.length > 0 ) {
                            msg.push('Avalidor(a) nº 0'+num+' alterou o(s) seguinte(s) dado(s):<br>- '+logAlteracoesRealizadas.join('<br>- ') );
                        }
                    }
                });
                if (msg.length > 0) {
                    //app.growl(msg.join('<br>'), 5, 'Atenção', 'tc', '', 'error')
                    app.growl(msg.join('<br>'),0,'Mensagem sobre a validação - '+params.lastUpdate,'tc','large','error',true);
                }
                // bloquear campos do resultado final dependendo das respostas
                if( (qtdA == 1 && qtdB == 1) || (qtdA == 2 || qtdB == 2 ) ) // marcou A-B ou A-A ou B-B
                {
                    $("#sqCategoriaFinal" + params.sqFicha+',#stPossivelmenteExtinta'+ params.sqFicha).prop('disabled', true);
                    $("#btnSelCriterioAvaliacao" + params.sqFicha).hide();
                    $("#dsCriterioAvalIucnFinal" + params.sqFicha).prop('disabled',true);
                } else if( ! validada ) {
                    $("#sqCategoriaFinal" + params.sqFicha+',#stPossivelmenteExtinta'+ params.sqFicha).prop('disabled', false);
                    $("#btnSelCriterioAvaliacao" + params.sqFicha).show();
                    $("#dsCriterioAvalIucnFinal" + params.sqFicha).prop('disabled',false);
                }
            }
        }
    }
}
