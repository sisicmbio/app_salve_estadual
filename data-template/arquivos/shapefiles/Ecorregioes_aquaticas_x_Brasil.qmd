<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="3.16.0-Hannover">
  <identifier>D:\Geo_CEPTA\Guena\especies_ameacadas_2_ciclo\Ecorregioes_aquaticas_x_Brasil.shp</identifier>
  <parentidentifier></parentidentifier>
  <language>PRT</language>
  <type>feature</type>
  <title>Ecorregiões aquáticas continentais vizinhas ou sobrepostas ao Brasil</title>
  <abstract>Este arquivo vetorial, shapefile, apresenta os polígonos dos Ecossistemas Aquáticos Continentais vizinhos ou sobrepostos ao territorio brasileiro, sendo resultado da estração de feições do arquivo vetorial feow (obtido de Freshwhater Ecoregions of the World, em www.feow.org) sobrepostas ou vizinhas aos limites do território brasileiro obtido em IBGE (www.ibge.gov.br).
</abstract>
  <keywords vocabulary="gmd:topicCategory">
    <keyword>Ambiente</keyword>
    <keyword>Inland Waters</keyword>
    <keyword>Margem corpos de água</keyword>
  </keywords>
  <contact>
    <contactAddress>
      <type>postal</type>
      <address>Rodovia SP-201 (Pref. Euberto Nemésio Pereira de Godoy),

Km 7,5</address>
      <city>Pirassununga</city>
      <administrativearea>São Paulo</administrativearea>
      <postalcode>13.630-970</postalcode>
      <country>Brasil</country>
    </contactAddress>
    <name>Marcelo Guena de Oliveira</name>
    <organization>CEPTA/ICMBio/Ministério do Meio Ambiente/Governo Federal do Brasil</organization>
    <position>Analista Ambiental</position>
    <voice>(19) 3565-1212</voice>
    <fax></fax>
    <email>cepta.sp@icmbio.gov.br</email>
    <role>owner</role>
  </contact>
  <links>
    <link format="" description="" size="" type="" url="" name="Não definido 1" mimeType=""/>
  </links>
  <fees>Ausente</fees>
  <constraints type="Não definido 1">Uso livre desde que citada a fonte.</constraints>
  <constraints type="Não definido 1">Uso livre desde que citada a fonte.</constraints>
  <constraints type="Não definido 1">Uso livre desde que citada a fonte.</constraints>
  <constraints type="Não definido 1">Uso livre desde que citada a fonte.</constraints>
  <constraints type="Não definido 1">Uso livre desde que citada a fonte.</constraints>
  <constraints type="Não definido 1">Uso livre desde que citada a fonte.</constraints>
  <constraints type="Não definido 1">Uso livre desde que citada a fonte.</constraints>
  <constraints type="Não definido 1">Uso livre desde que citada a fonte.</constraints>
  <constraints type="Não definido 1">Uso livre desde que citada a fonte.</constraints>
  <constraints type="Não definido 1">Uso livre desde que citada a fonte.</constraints>
  <constraints type="Não definido 1">Uso livre desde que citada a fonte.</constraints>
  <constraints type="Não definido 1">Uso livre desde que citada a fonte.</constraints>
  <constraints type="Não definido 1">Uso livre desde que citada a fonte.</constraints>
  <constraints type="Não definido 1">Uso livre desde que citada a fonte.</constraints>
  <constraints type="Não definido 1">Uso livre desde que citada a fonte.</constraints>
  <constraints type="Não definido 1">Uso livre desde que citada a fonte.</constraints>
  <rights></rights>
  <rights>CEPTA/ICMBio/MMA/Governo Federal do Brasil</rights>
  <license></license>
  <encoding></encoding>
  <crs>
    <spatialrefsys>
      <wkt>GEOGCRS["SIRGAS 2000",DATUM["Sistema de Referencia Geocentrico para las AmericaS 2000",ELLIPSOID["GRS 1980",6378137,298.257222101,LENGTHUNIT["metre",1]]],PRIMEM["Greenwich",0,ANGLEUNIT["degree",0.0174532925199433]],CS[ellipsoidal,2],AXIS["geodetic latitude (Lat)",north,ORDER[1],ANGLEUNIT["degree",0.0174532925199433]],AXIS["geodetic longitude (Lon)",east,ORDER[2],ANGLEUNIT["degree",0.0174532925199433]],USAGE[SCOPE["unknown"],AREA["Latin America - SIRGAS 2000 by country"],BBOX[-59.87,-122.19,32.72,-25.28]],ID["EPSG",4674]]</wkt>
      <proj4>+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs</proj4>
      <srsid>3517</srsid>
      <srid>4674</srid>
      <authid>EPSG:4674</authid>
      <description>SIRGAS 2000</description>
      <projectionacronym>longlat</projectionacronym>
      <ellipsoidacronym>EPSG:7019</ellipsoidacronym>
      <geographicflag>true</geographicflag>
    </spatialrefsys>
  </crs>
  <extent>
    <spatial dimensions="2" maxy="-25.01117042199999929" crs="EPSG:4674" minz="0" maxx="-50.87086438600000093" minx="-54.53445088200000157" maxz="0" miny="-26.70251920200000129"/>
    <temporal>
      <instant>2020-08-11T03:00:00Z</instant>
    </temporal>
  </extent>
</qgis>
