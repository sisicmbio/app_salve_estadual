package br.gov.icmbio;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;

import java.io.IOException;
import java.util.Date;
import java.text.SimpleDateFormat;

import static com.itextpdf.tool.xml.html.HTML.Tag.FONT;

public class HeaderFooter extends PdfPageEventHelper {
    /*Font font;
    PdfTemplate t;
    Image total;
    */
    private boolean printLogo       = true;
    private String logoPath        = "/data/salve-estadual/arquivos/logo.png";
    private String autor            = "";
    private String dataHoraEmissao  = "";
    private String ciclo            = "";
    private String marcaDagua       = "";
    private String titulo           = "Instituto Chico Mendes de Conservação da Biodiversidade - ICMBio";
    private String subTitulo        = "Processo de Avaliação do Risco de Extinção da Fauna Brasileira";
    private String rotuloEmitidoPor = "Emitido por";
    private String rotuloEmitidoEm  = "Emitido em";
    private String dateFormat       = "dd/MM/yyyy";
    private String firstPageFooterText = "";
    protected float startpos = -1;
    protected boolean title = true;

    public void setAutor( String newValue ){
        SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(dateFormat+" HH:mm:ss");
        autor = newValue;
        dataHoraEmissao = DATE_FORMAT.format( new Date() );
    }

    // ALTERAÇÕES PARA A INTEGRACAO SIS/IUCN
    public void setDateFormat( String newValue ){this.dateFormat = newValue;}
    public void setRotuloEmitidoPor( String newValue ){this.rotuloEmitidoPor = newValue;}
    public void setRotuloEmitidoEm( String newValue ){this.rotuloEmitidoEm = newValue;}
    public void setTitulo( String newValue ){ this.titulo = newValue;}
    public void setSubtitulo( String newValue ) { this.subTitulo = newValue;}
    // FIM ALTERAÇÕES PARA A INTEGRACAO SIS/IUCN

    public void setPrintLogotipo( boolean newValue )
    {
        printLogo = newValue;
    }
    public void setLogotipoPath( String newValue ){ logoPath = newValue;}
    public void setMarcaDagua( String newValue ){marcaDagua = newValue;}
    public void setCiclo( String newValue )
    {
        ciclo = newValue;
    }
    public void setLogo( String newValue ){ this.logoPath = newValue;}
    public void setFirstPageFooterText(String newValue ) {
        this.firstPageFooterText=newValue;
    }

    //ALTERAÇÕES PARA A INTEGRACAO SIS/IUCN
    public String getTitulo(){
        return this.titulo;
    }
    public String getSubTitulo(){
        return this.subTitulo;
    }
    public String getRotuloEmitidoPor(){
        return this.rotuloEmitidoPor;
    }
    public String getRotuloEmitidoEm(){
        return this.rotuloEmitidoEm;
    }
    public String getFirstPageFooterText(){
        return this.firstPageFooterText;
    }
    // FIM ALTERAÇÕES PARA INTEGRAÇÃO SIS/IUCN


/*
    public void onParagraphEnd(PdfWriter writer, Document document, float paragraphPosition) {

        if (!title) return;
        PdfContentByte canvas = writer.getDirectContentUnder();
        Rectangle pagesize = document.getPageSize();
        canvas.saveState();
        canvas.setColorStroke(BaseColor.BLUE);
        canvas.rectangle(
                pagesize.getLeft(document.leftMargin()),
                paragraphPosition - 3,
                pagesize.getWidth() - document.leftMargin() - document.rightMargin(),
                startpos - paragraphPosition);
        canvas.stroke();
        canvas.restoreState();
    }
    */

    // OUTRO
    public void onEndPage(PdfWriter writer, Document document) {
        Rectangle pagesize = document.getPageSize();
        float marginTop = pagesize.getTop()-20f;
        Font f10 = new Font(Font.FontFamily.TIMES_ROMAN, 10);
        Font f11 = new Font(Font.FontFamily.TIMES_ROMAN, 11);
        Font f12 = new Font(Font.FontFamily.TIMES_ROMAN, 12);
        //Font f9 = new Font(Font.FontFamily.TIMES_ROMAN, 9);
        Font f6 = new Font(Font.FontFamily.TIMES_ROMAN, 6);
        Font f3 = new Font(Font.FontFamily.TIMES_ROMAN, 4.8f);

        if( printLogo == true && ! logoPath.equals("") ) {
            marginTop -= 60f;
        }

        // imprimir a marca d`agua
        if( ! marcaDagua.equals("") ) {
            Phrase watermark = new Phrase(marcaDagua, new Font(Font.FontFamily.TIMES_ROMAN, 80, Font.NORMAL, new BaseColor(255, 170, 170)));
            PdfContentByte canvas = writer.getDirectContent();
            canvas.saveState();
            PdfGState gs1 = new PdfGState();
            gs1.setFillOpacity(0.5f);
            canvas.setGState(gs1);
            ColumnText.showTextAligned(
                    canvas,
                    Element.ALIGN_CENTER,
                    watermark,
                    ((pagesize.getLeft() + pagesize.getRight()) / 2) + 45,
                    (pagesize.getTop() + pagesize.getBottom()) / 2,
                    55);
            canvas.restoreState();
        }

        // titulo
        ColumnText.showTextAligned(
                writer.getDirectContent(),
                Element.ALIGN_CENTER,
                new Phrase(String.valueOf(titulo),f12),
                (pagesize.getLeft() + pagesize.getRight()) / 2,
                marginTop,
                0);

        // subtitulo
        ColumnText.showTextAligned(
                writer.getDirectContent(),
                Element.ALIGN_CENTER,
                //new Phrase(String.valueOf(subTitulo+" ("+ciclo+")"),f9),
                new Phrase(String.valueOf(subTitulo),f11),
                (pagesize.getLeft() + pagesize.getRight()) / 2,
                marginTop-13f,
                0);

        // texto complementar no rodape da primeira pagina
        if( ! firstPageFooterText.equals("") && writer.getPageNumber() == 1 ) {
            ColumnText.showTextAligned(
                writer.getDirectContent(),
                Element.ALIGN_LEFT,
                new Phrase(this.firstPageFooterText, f3)
                , document.leftMargin()
                , pagesize.getBottom() + 25
                , 0);
        }

        ColumnText.showTextAligned(
                writer.getDirectContent(),
                Element.ALIGN_CENTER,
                new Phrase(String.valueOf(writer.getPageNumber()),f3),
                (pagesize.getLeft() + pagesize.getRight()) / 2,
                pagesize.getBottom() + 15,
                0);


        ColumnText.showTextAligned(
                writer.getDirectContent(),
                Element.ALIGN_LEFT,
                new Phrase(rotuloEmitidoPor + ": "+autor,f6)
                ,document.leftMargin()
                ,pagesize.getBottom() + 15
                ,0);

        ColumnText.showTextAligned(
                writer.getDirectContent(),
                Element.ALIGN_RIGHT,
                new Phrase(rotuloEmitidoEm + ": " + dataHoraEmissao,f6)
                ,pagesize.getRight()-50
                ,pagesize.getBottom() + 15
                ,0);

        // logotipo
        if( printLogo == true && ! logoPath.equals("") ) {
            try {
                Image image = Image.getInstance(logoPath);
                float larguraLogo = 55f;
                float x = ((pagesize.getRight() - (document.rightMargin()) - larguraLogo) / 2);
                float y = pagesize.getTop() - ( larguraLogo + 10f );
                image.setScaleToFitHeight(true);
                //image.scaleToFit(35.6f, 32f);
                image.scaleToFit(larguraLogo+3.6f,larguraLogo);
                image.setAbsolutePosition(x, y);
                PdfContentByte canvas = writer.getDirectContent();
                canvas.addImage(image);
            } catch (Exception e) {
                System.out.println(e);
            }
        }


        if (startpos != -1)
        {
            onParagraphEnd(writer, document, pagesize.getBottom(document.bottomMargin()));
        }
        startpos = pagesize.getTop(document.topMargin());

    }

    public void onParagraph(PdfWriter writer, Document document, float paragraphPosition) {
        startpos = paragraphPosition;
    }

    /*
    // Exemplo como colocar pagina 1 de X
    public void onOpenDocument(PdfWriter writer, Document document) {
        t = writer.getDirectContent().createTemplate(30, 16);
        try {
            total = Image.getInstance(t);
            font = new Font(Font.FontFamily.TIMES_ROMAN, 10);
        } catch (DocumentException de) {
            throw new ExceptionConverter(de);
        }

    }

    public void onEndPage(PdfWriter writer, Document document) {
        PdfPTable table = new PdfPTable( 3 );
        try {
            table.setWidths(new int[]{24, 24, 2});
            table.setTotalWidth(537);
            table.getDefaultCell().setFixedHeight(20);
            table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
            table.addCell(new Phrase("Sistema de Avaliação do Risco de Extinção da Biodiversidade - SALVE", font));
            table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(new Phrase(String.format("Page %d of", writer.getPageNumber()), font));
            PdfPCell cell = new PdfPCell(total);
            cell.setBorder(Rectangle.NO_BORDER );
            table.addCell(cell);
            PdfContentByte canvas = writer.getDirectContent();
            canvas.beginMarkedContentSequence(PdfName.ARTIFACT);
            table.writeSelectedRows(0, -1, 36, 25, canvas);
            canvas.endMarkedContentSequence();
        } catch (DocumentException de) {
            throw new ExceptionConverter(de);
        }
    }

    public void onCloseDocument(PdfWriter writer, Document document) {
        ColumnText.showTextAligned(t, Element.ALIGN_LEFT,
                new Phrase(String.valueOf(writer.getPageNumber()), font),
                2, 4, 0);
    }
    */
}
