package br.gov.icmbio;


import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPTable;
/*
class RoundedBorder implements PdfPCellEvent {
    @Override
    public void cellLayout(PdfPCell pdfPCell, Rectangle rectangle, PdfContentByte[] pdfContentBytes) {
        PdfContentByte cb=pdfContentBytes[PdfPTable.BACKGROUNDCANVAS];
        cb.roundRectangle( rectangle.getLeft()+1.5f, rectangle.getBottom()+1.5f, rectangle.getWidth()-3, rectangle.getHeight()-3,4 );
        cb.stroke();
    }
}
*/
class RoundedBorder implements PdfPCellEvent {
    @Override
    public void cellLayout(PdfPCell cell, Rectangle rect,
                           PdfContentByte[] canvas) {
        PdfContentByte cb = canvas[PdfPTable.LINECANVAS];
        // desenhar um retangulo para limpar o fundo
        cb.rectangle(rect.getLeft()-1f, rect.getBottom(), rect.getWidth()+1f,
                rect.getHeight());
        cb.setColorFill(BaseColor.WHITE);
        cb.fill();

        if( cell.getBackgroundColor() != null ) {
            cb.setColorFill( cell.getBackgroundColor() );
        }
        else {
            cb.setColorFill( BaseColor.WHITE );
        }

        // redesenar com as bordas arredondadas
        cb.roundRectangle(
                rect.getLeft() + 1.5f, rect.getBottom() + 1.5f, rect.getWidth() - 3,
                rect.getHeight() - 3, 10);
        if( cell.getBorder() == Rectangle.NO_BORDER ) {
            cb.fill();
        }
        else {
            cb.fillStroke();
        }
    }
}