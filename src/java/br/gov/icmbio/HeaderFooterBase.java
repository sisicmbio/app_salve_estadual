package br.gov.icmbio;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfImage;

public class HeaderFooterBase extends PdfPageEventHelper {
    private String autor    = "";
    private String dataHoraEmissao = "";
    private String marcaDagua = "";
    private String titulo = "";
    private String subTitulo = "";
    private boolean headerOnAllPges=true;
    protected float startpos = -1;
    protected boolean title = true;

    public void setAutor( String newValue )
    {
        SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        autor = newValue;
        dataHoraEmissao = DATE_FORMAT.format( new Date() );
    }

    public void setHeaderOnAllPges( boolean newValue )
    {
        headerOnAllPges = newValue;
    }
    public void setMarcaDagua( String newValue )
    {
        marcaDagua = newValue;
    }

    public void setTitulo( String newValue )
    {
        titulo = newValue;
    }

    public void setSubTitulo( String newValue )
    {
        subTitulo = newValue;
    }

    public void onEndPage(PdfWriter writer, Document document) {
        Rectangle pagesize = document.getPageSize();
        Font f12 = new Font(Font.FontFamily.TIMES_ROMAN, 12);
        Font f10 = new Font(Font.FontFamily.TIMES_ROMAN, 10);
        Font f9 = new Font(Font.FontFamily.TIMES_ROMAN, 9);
        Font f6 = new Font(Font.FontFamily.TIMES_ROMAN, 6);

        // imprimir a marca d`agua
        if( ! marcaDagua.equals("") ) {
            Phrase watermark = new Phrase(marcaDagua, new Font(Font.FontFamily.TIMES_ROMAN, 80, Font.NORMAL, new BaseColor(255, 170, 170)));
            PdfContentByte canvas = writer.getDirectContent();
            canvas.saveState();
            PdfGState gs1 = new PdfGState();
            gs1.setFillOpacity(0.5f);
            canvas.setGState(gs1);
            ColumnText.showTextAligned(
                    canvas,
                    Element.ALIGN_CENTER,
                    watermark,
                    ((pagesize.getLeft() + pagesize.getRight()) / 2) + 45,
                    (pagesize.getTop() + pagesize.getBottom()) / 2,
                    55);
            canvas.restoreState();
        }

            // ICMBIO
            ColumnText.showTextAligned(
                    writer.getDirectContent(),
                    Element.ALIGN_CENTER,
                    new Phrase(String.valueOf("Instituto Chico Mendes de Conservação da Biodiversidade - ICMBio"), f12),
                    (pagesize.getLeft() + pagesize.getRight()) / 2,
                    pagesize.getTop() - 18,
                    0);

            // titulo
            ColumnText.showTextAligned(
                    writer.getDirectContent(),
                    Element.ALIGN_CENTER,
                    new Phrase(String.valueOf(titulo), f10),
                    (pagesize.getLeft() + pagesize.getRight()) / 2,
                    pagesize.getTop() - 30,
                    0);

            // subtitulo
            ColumnText.showTextAligned(
                    writer.getDirectContent(),
                    Element.ALIGN_CENTER,
                    new Phrase(String.valueOf(subTitulo), f9),
                    (pagesize.getLeft() + pagesize.getRight()) / 2,
                    pagesize.getTop() - 40,
                    0);


            ColumnText.showTextAligned(
                    writer.getDirectContent(),
                    Element.ALIGN_CENTER,
                    new Phrase(String.valueOf(writer.getPageNumber())),
                    (pagesize.getLeft() + pagesize.getRight()) / 2,
                    pagesize.getBottom() + 15,
                    0);

            ColumnText.showTextAligned(
                    writer.getDirectContent(),
                    Element.ALIGN_LEFT,
                    new Phrase("Emitido por: " + autor, f6)
                    , document.leftMargin()
                    , pagesize.getBottom() + 15
                    , 0);

            ColumnText.showTextAligned(
                    writer.getDirectContent(),
                    Element.ALIGN_RIGHT,
                    new Phrase("Emitido em: " + dataHoraEmissao, f6)
                    , pagesize.getRight() - 50
                    , pagesize.getBottom() + 15
                    , 0);

            // logotipo ICMBIO
            try {
                Image image = Image.getInstance("/data/salve-estadual/arquivos/logo.png");
                float x = pagesize.getRight() - (document.rightMargin() + 28);
                float y = pagesize.getTop() - 39;
                image.setScaleToFitHeight(true);
                image.scaleToFit(35.6f, 32f);

                image.setAbsolutePosition(x, y);
                PdfContentByte canvas = writer.getDirectContent();
                canvas.addImage(image);
            } catch (Exception e) {
                System.out.println(e);
            }

        /*
        PdfImage stream = new PdfImage(image, "", null);
        stream.put(new PdfName("ITXT_SpecialId"), new PdfName("123456789"));
        PdfIndirectObject ref = stamper.getWriter().addToBody(stream);
        image.setDirectReference(ref.getIndirectReference());
        image.setAbsolutePosition(36, 400);
        PdfContentByte over = stamper.getOverContent(1);
        over.addImage(image);
        */

        if (startpos != -1)
        {
            onParagraphEnd(writer, document, pagesize.getBottom(document.bottomMargin()));
        }
        startpos = pagesize.getTop(document.topMargin());
    }
}
