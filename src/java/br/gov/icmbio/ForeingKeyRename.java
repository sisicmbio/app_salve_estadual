package br.gov.icmbio;

import java.util.Collection;
import java.util.Iterator;

import org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsAnnotationConfiguration;
import org.hibernate.MappingException;
import org.hibernate.mapping.ForeignKey;
import org.hibernate.mapping.PersistentClass;
import org.hibernate.mapping.RootClass;

public class ForeingKeyRename extends GrailsAnnotationConfiguration {

	private static final long serialVersionUID = 1;

	private boolean _alreadyProcessed;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	protected void secondPassCompile() throws MappingException {
		super.secondPassCompile();

		if (_alreadyProcessed) {
			return;
		}

		for (PersistentClass pc : (Collection<PersistentClass>) classes
				.values()) {

			// alterar os nomes de todas as fks
			for (Iterator iter = pc.getTable().getForeignKeyIterator(); iter
					.hasNext();) {
				ForeignKey fk = (ForeignKey) iter.next();
				String tableName = fk.getTable().getName();
				if( ! tableName.isEmpty() ) {
					String[] parts = tableName.split("\\.");
					if (parts.length > 0) {
						tableName = parts[parts.length - 1];
					}
					fk.setName("fk_" + tableName + "_" + fk.getColumn(0).getName());
				}
			}

			/*
			 * if (pc instanceof RootClass) { RootClass root = (RootClass)pc; if
			 * ("br.gov.icmbio.UnidadeOrg".equals(root.getClassName())) { for
			 * (Iterator iter = root.getTable().getForeignKeyIterator();
			 * iter.hasNext();) { ForeignKey fk = (ForeignKey)iter.next();
			 * fk.setName("FK_UNID_ORG_PESSOA"); } } } else { for (Iterator iter
			 * = pc.getTable().getForeignKeyIterator(); iter.hasNext();) {
			 * ForeignKey fk = (ForeignKey)iter.next(); String tableName =
			 * fk.getTable().getName(); String[] parts = tableName.split("\\.");
			 * if( parts.length > 0 ){ tableName = parts[parts.length-1]; }
			 * fk.setName( "fk_"+tableName+"_"+fk.getColumn(0).getName() ); }
			 * 
			 * }
			 */
		}

		_alreadyProcessed = true;
	}
}
