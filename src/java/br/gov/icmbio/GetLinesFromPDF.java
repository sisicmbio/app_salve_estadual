package br.gov.icmbio;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.TextPosition;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
/**
 * This is an example on how to extract text line by line from pdf document
 */
class GetLinesFromPDF extends PDFTextStripper {

    //static List<String> lines = new ArrayList<String>();
    static float posY = 0f;

    public GetLinesFromPDF() throws IOException {
    }
    /**
     * @throws IOException If there is an error parsing the document.
     */
    public float getPosY(String fileName) throws IOException {
        PDDocument document = null;
        try {
            document = PDDocument.load( new File(fileName) );
            PDFTextStripper stripper = new GetLinesFromPDF();
            stripper.setSortByPosition( true );
            stripper.setStartPage( document.getNumberOfPages() );
            stripper.setEndPage( document.getNumberOfPages() );
            Writer dummy = new OutputStreamWriter(new ByteArrayOutputStream());
            stripper.writeText(document, dummy);

            // print lines
            /*for(String line:lines){
                System.out.println(line);
            }
             */
        }
        finally {
            if( document != null ) {
                document.close();
            }
        }
        return posY;
    }
    /**
     * Override the default functionality of PDFTextStripper.writeString()
     */
    @Override
    protected void writeString(String str, List<TextPosition> textPositions) throws IOException {
        float y = textPositions.get(0).getY();
        if( y < 803f ) {
            posY = y;
        }
        //lines.add( posY + " = "+ str);
    }
}
