package br.gov.icmbio

import com.lowagie.text.pdf.PdfCell
import com.sun.xml.internal.ws.util.ReadAllStream

// https://www.manning.com/books/itext-in-action-second-edition
//import com.sun.xml.internal.ws.util.ReadAllStream
// import com.itextpdf.text.pdf.ColumnTextimport org.jsoup.Connection
// import java.lang.reflect.ParameterizedType
// import java.nio.charset.Charset
// import com.itextpdf.text.pdf.PdfPTable
import org.apache.commons.io.IOUtils
import com.itextpdf.text.*
import com.itextpdf.text.pdf.*
import com.itextpdf.tool.xml.*
import com.itextpdf.text.pdf.draw.*
import org.codehaus.groovy.grails.web.servlet.mvc.GrailsHttpSession

import org.jsoup.Jsoup
import org.jsoup.safety.Whitelist

import java.nio.charset.Charset

/**
 * Created by luis on 31/03/17.
 */
class PdfFicha
{
    /**
     * - tudo que for texto: times, tam 12
     * - tudo que for título: times, tam 14 e negrito
     * - tudo que for subtítulo: times, tam 12, em negrito.
     */
    private String textoPadraoDiferencaCategoria = "(*) Resultado da avaliação mais recente da espécie. Entretanto, essa categoria ainda não foi oficializada por meio de Portaria do MMA. Para fins legais, deve-se utilizar a categoria constante nas Portarias MMA 444/2014 ou 445/2014."
    private boolean contextoPublico = false
    private boolean imprimirPendencias = true
    private boolean translateToEnglish = false
    private String msgDiferencaCategorias=''
    private fontNameCss = 'Times'
    private fichaService = null
    private logo= ''
    private titulo= ''
    private subTitulo=''
    private Integer sqFicha
    private String unidade
    private String ciclo
    private Ficha ficha
    private String fileName = ''
    private java.util.List refBibs = []
    private String ufs = ''
    private String biomas = ''
    private String bacias = ''
    private java.util.List ucs = []

    private BaseColor colorTitle = new BaseColor(167, 201, 127) // verde escuro
    private BaseColor colorTopic = new BaseColor(211, 224, 198) // verde claro
    private BaseColor colorTopic2 = new BaseColor(235, 243, 227,1) // verde mais claro
    private BaseColor colorHeader = new BaseColor(211, 224, 198) // verde claro
    private BaseColor colorLink = new BaseColor(0, 0, 200) // azul

    private BaseFont globalBaseFont
    private Font f6 = new Font(Font.FontFamily.TIMES_ROMAN, 6);
    private Font f8 = new Font(Font.FontFamily.TIMES_ROMAN, 8);
    private Font f10 = new Font(Font.FontFamily.TIMES_ROMAN, 10);
    private Font f10Link = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL, colorLink )
    private Font f10LinkI = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.ITALIC, colorLink )

    private Font f12 = new Font(Font.FontFamily.TIMES_ROMAN, 12);
    private Font f12I = new Font(Font.FontFamily.TIMES_ROMAN, 12,Font.ITALIC);
    private Font f12B = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD)

    private Font f14 = new Font(Font.FontFamily.TIMES_ROMAN, 14);
    private Font f14B = new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.BOLD);
    private Font f14I = new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.ITALIC);
    private PdfWriter writer
    private Document document
    private Boolean throwError = false

    PdfFicha(Integer sqFicha = 0, String dir='', Boolean throwError = false, GrailsHttpSession currentSession = null ) {
        dir = (dir ?: '/data/salve-estadual/temp/')
        String diaHora = new Date().format('dd-MM-yyyy-HH-mm-ss')
        this.throwError = throwError
        this.sqFicha = sqFicha
        this.ficha = Ficha.get(sqFicha)
        this.fileName = dir+ficha.taxon.noCientifico.toString().replaceAll(/ /,'-') +'-'+ diaHora +'.pdf'
        this.fichaService = new FichaService(currentSession)

        // criar fonte especifica para o salve
        String pathFontOpenSansRegular = "/data/salve-estadual/fonts/open-sans-regular.ttf";
        if( new File( pathFontOpenSansRegular ).exists()) {
            fontNameCss = 'Opensansregular'
            globalBaseFont = BaseFont.createFont( pathFontOpenSansRegular, BaseFont.WINANSI, true )
            //println 'Fonte ' + pathFontOpenSansRegular+' encontrada'
            f6   = new Font(globalBaseFont, 6)
            f8   = new Font(globalBaseFont, 8)
            f10  = new Font(globalBaseFont, 10)
            f10Link  = new Font(globalBaseFont, 10,Font.NORMAL,colorLink)
            f10LinkI  = new Font(globalBaseFont, 10,Font.ITALIC,colorLink)
            f12  = new Font(globalBaseFont, 12)
            f12I = new Font(globalBaseFont, 12, Font.ITALIC)
            f12B = new Font(globalBaseFont, 12, Font.BOLD)
            f14  = new Font(globalBaseFont, 14)
            f14B = new Font(globalBaseFont, 14, Font.BOLD)
            f14I = new Font(globalBaseFont, 14, Font.ITALIC)
        } else {
            globalBaseFont = f12.getCalculatedBaseFont( false );
        }
    }

    /**
     * imprimir a ficha em Inglês ou Português
     * @param newValue
     */
    void setTranslateToEnglish(boolean newValue = true ){
        this.translateToEnglish = newValue
    }
    void setTitulo(String newValue = '' ){
        this.titulo = newValue
    }
    String getTitulo(){
        return this.titulo
    }
    void setSubTitulo(String newValue = '' ){
        this.subTitulo = newValue
    }
    String getSubTitulo(){
        return this.subTitulo
    }
    void setLogo( newValue = '' ){
        this.logo = newValue
    }
    String getLogo(){
        String path=''
        if( ! this.logo ){
            return ''
        }
        if( new File(path+this.logo).exists() ) {
            return path+this.logo
        }
        path = '/data/salve-estadual/arquivos/'
        if( new File(path+this.logo).exists() ) {
            return path+this.logo
        }
        path = '/data/salve-estadual/arquivos/brasoes-estados/'
        if( new File(path+this.logo).exists() ) {
            return path+this.logo
        }
        return ''
    }

    /**
     * o contexto pode alterar algumas informações na ficha como por exemplo a categoria que sai
     * no inicio da ficha será lida da aba 11.7 e se não for igual a da última avaliação oficial mma
     * será exibido um texto no final da página.
     * @param newValue
     */
    void setContextoPublico( boolean newValue = true ) {
        this.contextoPublico = newValue
    }
    void setImprimirPendencias( boolean newValue = true ) {
        this.imprimirPendencias = newValue
    }
    void setMsgDiferencaCategorias( String newValue = '' ) {
        this.msgDiferencaCategorias = newValue
    }

    String getMsgDiferencaCategorias( String newValue = '' ) {
        return this.msgDiferencaCategorias ?: textoPadraoDiferencaCategoria
    }

    void setUnidade( String newValue = '' ) {
        this.unidade = newValue
    }
    void setCiclo( String newValue = '' ) {
        this.ciclo = newValue
    }
    void setRefBibs( java.util.List refBibs = [] ){
        this.refBibs = refBibs
    }
    void setUfs( String ufs = '' ) {
        this.ufs = ufs
    }
    void setBiomas( String biomas = '' ) {
        this.biomas = biomas
    }
    void setBacias( String bacias = '' ) {
        this.bacias = bacias
    }
    void setUcs( java.util.List ucs = [] ) {
        this.ucs = ucs
    }

    /**
     * Gerar o pdf da ficha em disco
     * @return
     */
    String run() {
        String strTemp
        ArrayList lista
        ArrayList headers
        PdfPTable table = new PdfPTable()
        PdfPCell cell
        Map structure = ficha?.taxon?.getStructure()
        // contextoPublico = true
        if( translateToEnglish ) {
            fileName = fileName.replaceAll(/\.pdf$/,'_en.pdf')
        }

        try {
            // para colocar cor de fundo é preciso da classe Rectangle
            Rectangle pageSize = new Rectangle(PageSize.A4);
            pageSize.setBackgroundColor(new BaseColor(255, 255, 255))
            document = new Document(pageSize);
            document.setMargins(/*left*/ 36,/*right*/ 36,/*top*/ 120,/*bottom*/ 36);
            //document = new Document(PageSize.A4)
            writer = PdfWriter.getInstance(document, new FileOutputStream(fileName))

            // eventos
            HeaderFooter headerFooter = new HeaderFooter()
            if( getTitulo() ) {
                headerFooter.setTitulo( getTitulo() )
            }
            if( getSubTitulo() ) {
                headerFooter.setSubtitulo(getSubTitulo())
            }
            if( getLogo()){
                headerFooter.setLogo(getLogo())
            }
            headerFooter.setRotuloEmitidoEm(_translate(headerFooter.getRotuloEmitidoEm()))
            headerFooter.setRotuloEmitidoPor(_translate(headerFooter.getRotuloEmitidoPor()))
            headerFooter.setFirstPageFooterText(_translate(headerFooter.getFirstPageFooterText()))
            headerFooter.setTitulo(_translate(headerFooter.getTitulo()))
            headerFooter.setSubtitulo(_translate(headerFooter.getSubTitulo()))

            headerFooter.setAutor(_translate(unidade))
            headerFooter.setCiclo(_translate(ciclo))
            if (ficha.situacaoFicha.codigoSistema != 'PUBLICADA') {
                headerFooter.setMarcaDagua(_translate('NÃO PUBLICADA'))
            }
            writer.setPageEvent(headerFooter);
            document.open()

            //String strTitle  = "Avaliação do Risco de Extinção de "
            String strTitle = ""
            String strNoCientifico = ficha?.taxon?.noCientifico // italico
            String strNoAutor = ficha?.taxon?.noAutor // sem italico

            // calcular tamanho da fonte
            BaseFont bf = BaseFont.createFont()
            int width = pageSize.getWidth() - (document.leftMargin() + document.rightMargin());
            float measureWidth = 1;
            float fontSize = 0.1f;
            while (measureWidth < width) {
                measureWidth = bf.getWidthPoint(strTitle + strNoCientifico + strNoAutor, fontSize);
                fontSize += 0.1f;
            }
            if (fontSize > 18f) {
                fontSize = 18f
            }
            if (fontSize < 12f) {
                fontSize = 12f
            }

            //************************************************************************
            // titulo
            //************************************************************************
            table = new PdfPTable(1)
            table.setHorizontalAlignment(Element.ALIGN_CENTER)
            table.setWidthPercentage(100)
            Font titleFontBold = new Font(globalBaseFont, fontSize, Font.BOLD, BaseColor.BLACK)
            Font titleFontItalic = new Font(globalBaseFont, fontSize, Font.BOLDITALIC, BaseColor.BLACK)
            Phrase phrase = new Phrase();
            if (strTitle) {
                phrase.add(new Chunk(strTitle, titleFontBold))
            }
            //phrase.add(new Chunk(strNoCientifico, new Font(Font.FontFamily.TIMES_ROMAN, fontSize, Font.BOLDITALIC,BaseColor.BLACK)))
            phrase.add(new Chunk(strNoCientifico, titleFontItalic))
            if (strNoAutor) {
                phrase.add(new Chunk(' ' + strNoAutor, titleFontBold))
            }

            cell = new PdfPCell(phrase)
            cell.setCellEvent(new RoundedBorder())
            cell.setPadding(8)
            cell.setUseAscender(true)
            cell.setBackgroundColor(colorTitle)
            cell.setBorder(Rectangle.NO_BORDER)
            cell.setHorizontalAlignment(Element.ALIGN_CENTER)
            table.addCell(cell)
            document.add(table)

            //************************************************************************
            // Autoria
            //************************************************************************
            if (ficha.dsCitacao) {
                String textoCitacao = ficha.dsCitacao.replaceAll(/&amp;/, '&').replaceAll(/&nbsp;/, ' ').replaceAll(/ ;/, ';').replaceAll(/(?i)<br ?\/?>/, ', ').replaceAll(/-/, ' ')
                if( translateToEnglish ) {
                    textoCitacao = _translate(textoCitacao, 'ficha', 'dsCitacao')
                }
                addP(Util.stripTagsKeepLineFeed(textoCitacao), f10, 'c')
            }

            //************************************************************************
            // Número do DOI
            //************************************************************************
            if (ficha.dsDoi) {
                String textDOI = 'Digital Object Identifier - '
                String numDoi = ficha.dsDoi
                //String urlDoi = 'https://doi.org/' + DOI
                Chunk chunk = new Chunk(textDOI,f10LinkI)
                Anchor anchor = new Anchor(numDoi, f10LinkI)
                //anchor.setReference(urlDoi) // o link esta indo para o proprio pdf no crossref
                Paragraph paragraph = new Paragraph()
                paragraph.add(chunk)
                paragraph.add(anchor)
                paragraph.setAlignment(Element.ALIGN_RIGHT)
                paragraph.setIndentationRight(5f)
                document.add(paragraph)
                /*
                String numDoi = ficha.dsDoi
                // a url do doi remete para o proprio pdf então não precisa ser um link
                //String urlDoi = 'https://doi.org/' + numDoi
                String urlDoi = ''
                Anchor anchor = new Anchor(numDoi, f10Link);
                anchor.setReference(urlDoi)
                Paragraph paragraph = new Paragraph()
                paragraph.add(anchor)
                paragraph.setAlignment(Element.ALIGN_RIGHT)
                paragraph.setIndentationRight(5f)
                document.add(paragraph)*/
            }

            //************************************************************************
            // Categoria e Justificattiva
            //************************************************************************
            Map ultimaAvaliacao = ficha.ultimaAvaliacaoNacional
            Map ultimaAvaliacaoOficial = ficha.ultimaAvaliacaoNacionalHist
            String textoCategoria
            String textoDataAvaliacao
            String textoJustificativa
            // quando for a impressão traduzida para o inglês a ficha tem que estar publicada então ler a justificativa, categoria e critério da aba validação
            if (!this.contextoPublico && !translateToEnglish) {
                textoJustificativa = ultimaAvaliacao.txJustificativaAvaliacao
                if (ultimaAvaliacao?.deCategoriaIucn) {
                    textoCategoria = ultimaAvaliacao.deCategoriaIucn + ' ' + ultimaAvaliacao.deCriterioAvaliacaoIucn
                    if (ultimaAvaliacao.mesAnoUltimaAvaliacao) {
                        //pCategoria.add( Chunk.NEWLINE)
                        if (ultimaAvaliacao.mesAnoUltimaAvaliacao.toString().size() == 4) {
                            textoDataAvaliacao = 'Ano da avaliação: ' + ultimaAvaliacao.mesAnoUltimaAvaliacao
                        } else {
                            textoDataAvaliacao = 'Data da avaliação: ' + ultimaAvaliacao.mesAnoUltimaAvaliacao
                        }
                    }
                }
            } else {
                boolean houveMudancaCategoria = false
                if (ultimaAvaliacaoOficial.coCategoriaIucn && ficha.categoriaFinal && ultimaAvaliacaoOficial.coCategoriaIucn != ficha.categoriaFinal.codigoSistema) {
                    String deCategoriaIucnSemSigla = ultimaAvaliacao.deCategoriaIucn.toString().replaceAll(' (' + ultimaAvaliacaoOficial.coCategoriaIucn + ')', '')
                    String textoDiferencaCategoria = sprintf(_translate(this.getMsgDiferencaCategorias()), _translate(deCategoriaIucnSemSigla) + ' (' + ultimaAvaliacaoOficial.coCategoriaIucn + ')')
                    houveMudancaCategoria = true
                    headerFooter.setFirstPageFooterText(textoDiferencaCategoria)
                }
                textoCategoria = _translate(ficha?.categoriaFinal?.descricao, 'apoio', 'dsDadosApoio') + ' (' + ficha?.categoriaFinal?.codigo + ')' + (houveMudancaCategoria ? '*' : '')
                textoJustificativa = _translate(ficha.dsJustificativaFinal, 'ficha', 'dsJustificativaFinal')
                if (ultimaAvaliacao.mesAnoUltimaAvaliacao) {
                    if (ultimaAvaliacao.mesAnoUltimaAvaliacao.toString().size() == 4) {
                        textoDataAvaliacao = _translate('Ano da avaliação') + ': ' + ultimaAvaliacao.mesAnoUltimaAvaliacao
                    } else {
                        textoDataAvaliacao = _translate('Data da avaliação') + ': ' + ultimaAvaliacao.mesAnoUltimaAvaliacao
                    }
                }
            }
            br()
            if (textoCategoria) {
                Paragraph pCategoria = new Paragraph()
                pCategoria.add(new Chunk(_translate('Categoria') + ': ', f12B))
                pCategoria.add(new Chunk(textoCategoria, f12))
                Paragraph pAno = new Paragraph()
                if (textoDataAvaliacao) {
                    pAno.add(new Chunk(textoDataAvaliacao, f8))
                }
                table = new PdfPTable(1)

                table.setHorizontalAlignment(Element.ALIGN_CENTER)
                table.setWidthPercentage(100);
                cell = new PdfPCell()
                cell.setHorizontalAlignment(Element.ALIGN_CENTER)
                cell.setUseAscender(true)
                cell.setPaddingBottom(4f) // espaco entre a categoria e data da avaliacao
                cell.setBorder(Rectangle.NO_BORDER)
                pCategoria.setAlignment(Element.ALIGN_CENTER)
                cell.addElement(pCategoria)
                table.addCell(cell)
                cell = new PdfPCell()
                cell.setUseAscender(true)
                cell.setBorder(Rectangle.NO_BORDER)
                pAno.setAlignment(Element.ALIGN_CENTER)
                cell.addElement(pAno)
                table.addCell(cell)
                addTopic(table, colorTitle)
                if (textoJustificativa) {
                    addP(_translate('Justificativa'), f12B)
                    addHtml(textoJustificativa, true)
                }
            } else {
                addTopic(_translate('Categoria') + ': ' + _translate('Não Avaliada', 'apoio', 'dsDadosApoio') + ' (NE)', colorTopic, f12)
            }
            // **************************************************************


            //************************************************************************
            // Classificação Taxonomica / FOTO
            //************************************************************************

            br()
            addTopic(_translate('Classificação Taxonômica'))
            br()

            // guardar a posição para colocar a imagem abaixo do tópico
            //float vp = writer.getVerticalPosition(false) - ( document.bottomMargin() + document.topMargin() + 50 )
            String nomeCientifico = structure.nmEspecie
            Map imagemPrincipal = fichaService.getFichaImagemPrincipal(ficha)
            table = new PdfPTable(1)
            table.setHorizontalAlignment(Element.ALIGN_CENTER)
            table.setWidthPercentage(100)
            table.resetColumnCount(2)
            table.setWidths((Float[]) [1.3f, 2f])
            cell = new PdfPCell()
            cell.setBorder(Rectangle.NO_BORDER)
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE)
            cell.addElement(new Paragraph(_translate('Filo') + ': ' + structure.nmFilo, f12))
            cell.addElement(new Paragraph(_translate('Classe') + ': ' + structure.nmClasse, f12))
            cell.addElement(new Paragraph(_translate('Ordem') + ': ' + structure.nmOrdem, f12))
            cell.addElement(new Paragraph(_translate('Família') + ': ' + structure.nmFamilia, f12))

            // genero deve ser em italico
            Paragraph tempParagrafo = new Paragraph()
            tempParagrafo.add(new Chunk(_translate('Gênero') + ': ', f12))
            tempParagrafo.add(new Chunk(structure.nmGenero, f12I))
            cell.addElement(tempParagrafo)

            // os nomes da espécie / subespecie devem ser em italico
            if (structure.nmSubespecie) {
                nomeCientifico = structure.nmSubespecie
                cell.addElement(new Paragraph(_translate('Espécie') + ': ' + structure.nmEspecie, f12))
                br()
                Chunk chunkLabel = new Chunk(_translate('Subespécie') + ': ', f12)
                Chunk chunkItalico = new Chunk(structure.nmSubespecie, f12I)
                tempParagrafo = new Paragraph()
                tempParagrafo.add(chunkLabel)
                tempParagrafo.add(chunkItalico)
                cell.addElement(tempParagrafo)
            } else {
                Chunk chunkLabel = new Chunk(_translate('Espécie') + ': ', f12)
                Chunk chunkItalico = new Chunk(structure.nmEspecie, f12I)
                tempParagrafo = new Paragraph()
                tempParagrafo.add(chunkLabel)
                tempParagrafo.add(chunkItalico)
                cell.addElement(tempParagrafo)
            }
            //cell.setPadding( 5 )
            cell.setPaddingLeft(0)
            cell.setUseAscender(true)
            cell.setHorizontalAlignment(Element.ALIGN_LEFT)
            table.addCell(cell)

            //************************************************************************
            // FOTO com autor e legenda da foto
            //************************************************************************
            if (imagemPrincipal) {
                String credits = '';
                if (imagemPrincipal.autor) {
                    credits = _translate('Autor') + ': ' + imagemPrincipal.autor + '   '
                }
                if (imagemPrincipal.legenda && imagemPrincipal.legenda.toLowerCase().trim() != nomeCientifico.toLowerCase().trim()) {
                    credits += '\n' + imagemPrincipal.legenda
                }
                PdfPCell cellDireita = new PdfPCell()
                // SUBTABELA com foto e autor
                PdfPTable tableImagemAutor = new PdfPTable(1)
                tableImagemAutor.setHorizontalAlignment(Element.ALIGN_RIGHT)
                cell = createImageCell(imagemPrincipal.localArquivo, true, 300, 300)
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT)
                cell.setBorder(Rectangle.NO_BORDER)
                tableImagemAutor.addCell(cell)
                Paragraph pCreditos = new Paragraph(6, credits, f6)
                pCreditos.setAlignment(Element.ALIGN_RIGHT)
                cell = new PdfPCell(pCreditos)
                cell.setBorder(Rectangle.NO_BORDER)
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT)
                tableImagemAutor.addCell(cell)
                cellDireita.addElement(tableImagemAutor)
                cellDireita.setBorder(Rectangle.NO_BORDER)
                table.addCell(cellDireita)
            } else {
                // cell vazia para imprimir a table
                cell = new PdfPCell()
                cell.setBorder(Rectangle.NO_BORDER)
                table.addCell(cell)
            }
            document.add(table)

            //************************************************************************
            // nomes comuns
            //************************************************************************
            strTemp = fichaService.getFichaNomesComunsText(ficha)

            strTemp = strTemp.toString().trim()
            if (strTemp) {
                br()
                addP(_translate('Nomes comuns'), f12B)
                addP(strTemp, f12, 'l')
            }

            //************************************************************************
            // sinonímias
            //************************************************************************
            strTemp = fichaService.getFichaSinonimiasHtml(ficha)
            strTemp = strTemp.toString().trim()
            if (strTemp) {
                br()
                addP(_translate('Nomes antigos'), f12B)
                addHtml(strTemp)
            }

            //************************************************************************
            // Notas Taxonomicas
            //************************************************************************
            br()
            addP(_translate('Notas taxonômicas'), f12B)
            addHtml(_translate(ficha.dsNotasTaxonomicas, 'ficha', 'dsNotasTaxonomicas'), true)

            //************************************************************************
            // Notas Morfológicas
            //************************************************************************
            if (ficha.dsDiagnosticoMorfologico) {
                br()
                addP(_translate('Notas morfológicas'), f12B)
                addHtml(_translate(ficha.dsDiagnosticoMorfologico, 'ficha', 'dsDiagnosticoMorfologico'), true)
            }


            //************************************************************************
            br()
            addTopic(_translate('Distribuição'))
            //************************************************************************


            //************************************************************************
            // Endêmica do Brasi
            //************************************************************************

            addP2(_translate('Endêmica do Brasil') + ': ', _translate(snd(ficha.stEndemicaBrasil)), f12B, f12, 5, true)

            //************************************************************************
            // Distribuição Global
            //************************************************************************
            br()
            addP(_translate('Distribuição global'), f12B)
            /*String html = Util.stripTagsFichaPdf( _translate(ficha.dsDistribuicaoGeoGlobal,'ficha','dsDistribuicaoGeoGlobal' ) ) // ja tem  o trimEditor()
            html = html.replaceAll(/font-size:/,'X:'); // anular fonte-size
            html = html.replaceAll(/line-height:/,'X:'); // anular font-height
            html = html.replaceAll(/font-family:/,'X:'); // anular font-family*/
            addHtml( _translate( ficha.dsDistribuicaoGeoGlobal , 'ficha', 'dsDistribuicaoGeoGlobal'), true)


            //************************************************************************
            // Distribuição Nacional
            //************************************************************************
            if (trim(ficha.dsDistribuicaoGeoNacional)) {
                br()
                addP(_translate('Distribuição nacional'), f12B)
                addHtml(_translate(ficha.dsDistribuicaoGeoNacional, 'ficha', 'dsDistribuicaoGeoNacional'), true)
            }

            //************************************************************************
            // UFs
            //************************************************************************
            //strTemp = fichaService.getUfsNameText(ficha)

            /*
            strTemp = fichaService.getUfsNameByRegiaoText(ficha)
            strTemp = strTemp.toString().trim()
            if( strTemp ) {
                br()
                float avs = writer.getVerticalPosition(false) - document.bottomMargin();
                if( avs < 52f ) {
                    document.newPage()
                }
                addP( 'Estados', f12B )
                addP( strTemp, f12,'j' )
            }*/
            if (this.ufs) {
                br()
                float avs = writer.getVerticalPosition(false) - document.bottomMargin();
                if (avs < 52f) {
                    document.newPage()
                }
                addP(_translate('Estados'), f12B)
                addP(ufs, f12, 'j')
            }


            //************************************************************************
            // Biomas
            //************************************************************************
            if (this.biomas) {
                br()
                addP(_translate('Biomas'), f12B)
                addP(this.biomas, f12, 'j')
            }

            //************************************************************************
            // Bacias Hidrográficas
            //************************************************************************
            if (this.bacias) {
                br()
                addP(_translate('Bacias hidrográficas'), f12B)
                addP(this.bacias, f12, 'j')
            }

            //************************************************************************
            // Areas Relevantes
            //************************************************************************
            lista = fichaService.getAreasRelevantesList(ficha)
            if (lista) {
                br()
                addP(_translate(_translate('Áreas relevantes')), f12B)
                headers = [_translate('Tipo'), _translate('Local'), _translate('Estado'), _translate('Município'), _translate('Referência bibliográfica')]
                table = new PdfPTable()
                table.setSpacingBefore(5)
                table.setWidthPercentage(100);
                table.setHorizontalAlignment(Element.ALIGN_CENTER)
                drawHeader(table, headers, colorHeader, [1.0f, 1.0f, 1.0f, 1.0f, 2.0f])
                lista.each {
                    // tipo
                    table.addCell(getCell(_translate(it?.tipoRelevancia?.descricao, 'apoio', 'dsDadosApoio')))

                    // local
                    table.addCell(getCell(_translate(it.localHtml)))

                    // Estado
                    table.addCell(getCell(it.estado.noEstado))
                    // Municipio
                    table.addCell(getCell(it.municipiosHtml))
                    // referência bibliografica
                    table.addCell(getCell(it.refBibText))
                }
                document.add(table)
            }

            //************************************************************************
            // Mapa Distribuição
            //************************************************************************
            addMap(fichaService.getFichaDistribuicaoMapaPrincipal(ficha, (this.translateToEnglish ? 'en' : 'pt')))

            //************************************************************************
            br()
            addTopic(_translate('História natural'))
            //************************************************************************


            //************************************************************************
            // Espécie migratória:
            //************************************************************************
            if (ficha.stMigratoria && ficha.stMigratoria != 'D') {
                addP2(_translate('Espécie migratória') + '? ', _translate(snd(ficha.stMigratoria)) +
                    (ficha.stMigratoria == 'S' ? ', ' + _translate('padrão de deslocamento') + ' ' + _translate(ficha?.padraoDeslocamento?.descricao, 'apoio', 'dsDadosApoio') : '')
                    , f12B, f12, 5, true)
            }

            if (ficha.dsHistoriaNatural) {
                addHtml(_translate(ficha.dsHistoriaNatural, 'ficha', 'dsHistoriaNatural'), true)
            }

            //************************************************************************
            //Hábito Alimentar
            //************************************************************************
            lista = FichaHabitoAlimentar.findAllByFicha(ficha)
            if (lista) {
                br()
                addP(_translate('Hábito alimentar'), f12B)
                table = new PdfPTable()
                table.setSpacingBefore(5)
                table.setWidthPercentage(100);
                table.setHorizontalAlignment(Element.ALIGN_LEFT)
                headers = [_translate('Tipo'), _translate('Referência bibliográfica')]
                drawHeader(table, headers, colorHeader, [1.0f, 2.0f])
                lista.each {
                    // Tipo
                    table.addCell(getCell(_translate(it?.tipoHabitoAlimentar?.descricao, 'apoio', 'dsDadosApoio')))
                    // Ref Bibliográfica
                    table.addCell(getCell(it?.refBibText))
                }
                document.add(table)
            }

            //************************************************************************
            // Habito alimentar especialista
            //************************************************************************
            if (ficha.stHabitoAlimentEspecialista == 'S') {
                br()
                addP2(_translate('Hábito alimentar especialista') + '? ', _translate(snd(ficha.stHabitoAlimentEspecialista)), f12B, f12)
                lista = FichaHabitoAlimentarEsp.findAllByFicha(ficha)
                if (lista) {
                    br()
                    table = new PdfPTable()
                    table.setSpacingBefore(5)
                    table.setWidthPercentage(100);
                    table.setHorizontalAlignment(Element.ALIGN_LEFT)
                    headers = [_translate('Taxon'), _translate('Categoria')]
                    drawHeader(table, headers, colorHeader, [1.0f, 1.0f])
                    lista.each {
                        // Taxon
                        table.addCell(getCell(it?.taxon?.noCientifico))
                        // Categoria
                        if (it.categoriaIucn) {
                            table.addCell(getCell(_translate(it.categoriaIucn.descricao) + ' (' + it.categoriaIucn.codigo + ')'))
                        } else {
                            table.addCell(getCell(''))
                        }
                    }
                    document.add(table)
                }
            }

            if (ficha.dsHabitoAlimentar) {
                br()
                addP(_translate('Observações sobre o hábito alimentar'), f12B)
                addHtml(_translate(ficha.dsHabitoAlimentar, 'ficha', 'dsHabitoAlimentar'), true)
            }


            //************************************************************************
            // Restrito a habitat primário?  (se não for preenchido não aparece)
            //************************************************************************
            addP2(_translate('Restrito a habitat primário') + '? ', _translate(snd(ficha.stRestritoHabitatPrimario)), f12B, f12, 5f, true)


            //************************************************************************
            // Especialista em micro habitat? (se não for preenchido não aparece)
            //************************************************************************
            addP2(_translate('Especialista em micro habitat') + '? ', _translate(snd(ficha.stEspecialistaMicroHabitat)), f12B, f12, 5f, true)


            if (ficha.dsEspecialistaMicroHabitat && ficha.stEspecialistaMicroHabitat == 'S') {
                br()
                addP(_translate('Micro habitat'), f12B)
                addHtml(_translate(ficha.dsEspecialistaMicroHabitat, 'ficha', 'dsEspecialistaMicroHabitat'))
            }

            // descrição geral sobre o habitat
            if (ficha.dsUsoHabitat) {
                br()
                addP(_translate('Observações sobre o habitat'), f12B)
                addHtml(_translate(ficha.dsUsoHabitat, 'ficha', 'dsUsoHabitat'), true)
            }

            // Iterações com outras espécies (se não for preenchido não aparece)
            lista = FichaInteracao.findAllByFicha(ficha)
            if (lista) {
                br()
                addP(_translate('Interações com outras espécies'), f12B)
                table = new PdfPTable()
                table.setSpacingBefore(5)
                table.setWidthPercentage(100)
                table.setHorizontalAlignment(Element.ALIGN_CENTER)
                headers = [_translate('Tipo'), _translate('Taxon'), _translate('Categoria'), _translate('Referência bibliográfica')]
                drawHeader(table, headers, colorHeader, [1.2f, 1.3f, 1.2f, 1.8f])
                lista.each {
                    // Tipo
                    table.addCell(getCell(_translate(it?.tipoInteracao?.descricao, 'apoio', 'dsDadosApoio'), 'c'))
                    // Taxon
                    Paragraph px = new Paragraph(it?.taxon?.noCientifico)
                    px.setFont(f12I)
                    px.setLeading(13) // line height
                    px.setAlignment(Element.ALIGN_CENTER)
                    cell = getCell('')
                    cell.addElement(px)
                    table.addCell(cell)

                    // Categoria
                    if (it.categoriaIucn) {
                        table.addCell(getCell(_translate(it.categoriaIucn.descricao, 'apoio', 'dsDadosApoio') + '(' + it.categoriaIucn.codigo + ')', 'c'))
                    } else {
                        table.addCell(getCell('', 'c'))
                    }

                    // Ref. Bibliográfica
                    table.addCell(getCell(it?.refBibText, 'c'))
                }
                document.add(table)
                br()
                addHtml('<div style="text-align: justify;margin-top:-10px;">' + trim(_translate(ficha?.dsInteracao, 'ficha', 'dsInteracao')) + '</div>')
            }

            // REPRODUÇÃO
            if (ficha.vlMaturidadeSexualFemea || ficha.vlMaturidadeSexualMacho || ficha.vlPesoMacho
                || ficha.vlPesoFemea || ficha.vlComprimentoMacho || ficha.vlComprimentoFemea
                || ficha.vlSenilidadeReprodutivaMacho || ficha.vlSenilidadeReprodutivaFemea
                || ficha.vlLongevidadeMacho || ficha.vlLongevidadeFemea
                || ficha.vlIntervaloNascimento || ficha.vlTempoGestacao) {


                br()
                addP(_translate('Reprodução'), f12B)
                br()
                //************************************************************************
                // Intervalo de nascimentos: (se não for preenchido não aparece)
                //************************************************************************
                if (ficha.vlIntervaloNascimento) {
                    addP2(_translate('Intervalo de nascimentos') + ': ', _translate( valorComVirgula(ficha.vlIntervaloNascimento), 'regex' ) + ' ' + _translate(ficha?.unidIntervaloNascimento?.descricao, 'apoio', 'dsDadosApoio'), f12B, f12)
                }

                //************************************************************************
                // Tempo de gestação: (se não for preenchido não aparece)
                //************************************************************************
                if (ficha.vlTempoGestacao) {
                    addP2(_translate('Tempo de gestação') + ': ', _translate( valorComVirgula(ficha.vlTempoGestacao), 'regex'  ) + ' ' + _translate(ficha?.unidTempoGestacao?.descricao, 'apoio', 'dsDadosApoio'), f12B, f12)
                }

                //************************************************************************
                // Tamanho da prole: (se não for preenchido não aparece)
                //************************************************************************
                if (ficha.vlTamanhoProle) {
                    addP2(_translate('Tamanho da prole') + ': ', _translate(valorComVirgula(ficha.vlTamanhoProle),'regex') +' '+ _translate('Individuo(s)'), f12B, f12)
                }
            }
            //************************************************************************
            // quadro macho e femea
            //************************************************************************
            if (ficha.vlMaturidadeSexualFemea || ficha.vlMaturidadeSexualMacho || ficha.vlPesoMacho
                || ficha.vlPesoFemea || ficha.vlComprimentoMacho || ficha.vlComprimentoFemea
                || ficha.vlSenilidadeReprodutivaMacho || ficha.vlSenilidadeReprodutivaFemea
                || ficha.vlLongevidadeMacho || ficha.vlLongevidadeFemea) {
                br()
                table = new PdfPTable(2, 1, 1)
                //table.setSpacingBefore(8)
                table.setWidthPercentage(100)
                table.setHorizontalAlignment(Element.ALIGN_LEFT)

                headers = [_translate('Campo'), _translate('Macho'), _translate('Fêmea')]
                drawHeader(table, headers, colorHeader, [2.0f, 1.0f, 1.0f])

                // maturidade sexual
                if (ficha.vlMaturidadeSexualMacho || ficha.vlMaturidadeSexualFemea) {
                    table.addCell(getCell(_translate('Maturidade sexual')))
                    // macho
                    table.addCell(getCell(ficha?.vlMaturidadeSexualMacho ? valorComVirgula(ficha?.vlMaturidadeSexualMacho) + ' ' +
                        _translate(ficha?.unidMaturidadeSexualMacho?.descricao, 'apoio', 'dsDadosApoio') : '', 'c'))
                    // femea
                    table.addCell(getCell(ficha?.vlMaturidadeSexualFemea ? valorComVirgula(ficha?.vlMaturidadeSexualFemea) + ' ' +
                        _translate(ficha?.unidMaturidadeSexualFemea?.descricao, 'apoio', 'dsDadosApoio') : '', 'c'))
                }

                // peso
                if (ficha.vlPesoMacho || ficha.vlPesoFemea) {
                    table.addCell(getCell(_translate('Peso médio (adulto)')))
                    // macho
                    table.addCell(getCell(ficha?.vlPesoMacho ? valorComVirgula(ficha.vlPesoMacho) + ' ' +
                        _translate(ficha?.unidadePesoMacho?.descricao, 'apoio', 'dsDadosApoio') : '', 'c'))
                    // femea
                    table.addCell(getCell(ficha?.vlPesoFemea ? valorComVirgula(ficha.vlPesoFemea) + ' ' +
                        _translate(ficha?.unidadePesoFemea?.descricao, 'apoio', 'dsDadosApoio') : '', 'c'))
                }

                // comprimento max
                if (ficha.vlComprimentoMachoMax || ficha.vlComprimentoFemeaMax) {
                    table.addCell(getCell(_translate('Comprimento máximo')))
                    // macho
                    table.addCell(getCell(ficha?.vlComprimentoMachoMax ? valorComVirgula(ficha.vlComprimentoMachoMax) + ' ' +
                        _translate(ficha?.medidaComprimentoMacho?.descricao, 'apoio', 'dsDadosApoio') : '', 'c'))
                    // femea
                    table.addCell(getCell(ficha?.vlComprimentoFemeaMax ? valorComVirgula(ficha.vlComprimentoFemeaMax) + ' ' +
                        _translate(ficha?.medidaComprimentoFemea?.descricao, 'apoio', 'dsDadosApoio') : '', 'c'))
                }

                // comprimento na maturidade sexual
                if (ficha.vlComprimentoMacho || ficha.vlComprimentoFemea) {
                    table.addCell(getCell(_translate('Comprimento na maturidade sexual')))
                    // macho
                    table.addCell(getCell(ficha?.vlComprimentoMacho ? valorComVirgula(ficha.vlComprimentoMacho) + ' ' +
                        _translate(ficha?.medidaComprimentoMacho?.descricao, 'apoio', 'dsDadosApoio') : '', 'c'))
                    // femea
                    table.addCell(getCell(ficha?.vlComprimentoFemea ? valorComVirgula(ficha.vlComprimentoFemea) + ' ' +
                        _translate(ficha?.medidaComprimentoFemea?.descricao, 'apoio', 'dsDadosApoio') : '', 'c'))
                }

                // senilidade reprodutiva
                if (ficha.vlSenilidadeReprodutivaMacho || ficha.vlSenilidadeReprodutivaFemea) {
                    table.addCell(getCell(_translate('Senilidade reprodutiva')))
                    // macho
                    table.addCell(getCell(ficha?.vlSenilidadeReprodutivaMacho ? valorComVirgula(ficha.vlSenilidadeReprodutivaMacho) + ' ' +
                        _translate(ficha?.unidadeSenilidRepMacho?.descricao, 'apoio', 'dsDadosApoio') : '', 'c'))
                    // femea
                    table.addCell(getCell(ficha?.vlSenilidadeReprodutivaFemea ? valorComVirgula(ficha.vlSenilidadeReprodutivaFemea) + ' ' +
                        _translate(ficha?.unidadeSenilidRepFemea?.descricao, 'apoio', 'dsDadosApoio') : '', 'c'))
                }

                // longevidade
                if (ficha.vlLongevidadeMacho || ficha.vlLongevidadeFemea) {
                    table.addCell(getCell(_translate('Longevidade')))
                    // macho
                    table.addCell(getCell(ficha?.vlLongevidadeMacho ? valorComVirgula(ficha.vlLongevidadeMacho) + ' ' +
                        _translate(ficha?.unidadeLongevidadeMacho?.descricao, 'apoio', 'dsDadosApoio') : '', 'c'))
                    // femea
                    table.addCell(getCell(ficha?.vlLongevidadeFemea ? valorComVirgula(ficha.vlLongevidadeFemea) + ' ' +
                        _translate(ficha?.unidadeLongevidadeFemea?.descricao, 'apoio', 'dsDadosApoio') : '', 'c'))
                }
                document.add(table)
                br()
                addHtml('<div style="text-align: justify;margin-top:-10px;">' + trim(_translate(ficha?.dsReproducao, 'ficha', 'dsReproducao')) + '</div>')
            }
            //---------------------------------------------------------------------------------------------

            //************************************************************************
            br()
            addTopic(_translate('População'))
            br()
            //************************************************************************

            // Tempo geracional
            if (ficha.vlTempoGeracional) {
                addP2(_translate('Tempo geracional') + ': ', (valorComVirgula(ficha.vlTempoGeracional.toString()) + ' ' +
                    (ficha?.medidaTempoGeracional?.descricao ? _translate(ficha?.medidaTempoGeracional?.descricao, 'apoio', 'dsDadosApoio') : '')), f12B, f12)
            }

            // Tendência populacional
            addP2(_translate('Tendência populacional') + ': ', (ficha?.tendenciaPopulacional?.descricao ? _translate(ficha?.tendenciaPopulacional?.descricao, 'apoio', 'dsDadosApoio') : ''), f12B, f12)

            // Razão sexual: (se não for preenchido não aparece)
            if (ficha?.dsRazaoSexual) {
                addP2(_translate('Razão sexual') + ': ', trim(_translate(ficha?.dsRazaoSexual, 'ficha', 'dsRazaoSexual')), f12B, f12,)
            }

            // Taxa de mortalidade natural: (se não for preenchido não aparece)
            if (ficha?.dsTaxaMortalidade) {
                addP2(_translate('Taxa de mortalidade natural') + ': ', trim(_translate(ficha?.dsTaxaMortalidade, 'ficha', 'dsTaxaMortalidade')), f12B, f12)
            }

            if (ficha.dsCaracteristicaGenetica) {
                addP(_translate('Características genéticas'), f12B)
                addHtml(_translate(ficha.dsCaracteristicaGenetica, 'ficha', 'dsCaracteristicaGenetica'), true)
            }

            if (ficha.dsPopulacao) {
                br()
                addP(_translate('Observações sobre a população'), f12B)
                addHtml(_translate(ficha.dsPopulacao, 'ficha', 'dsPopulacao'), true)
            }

            // adicionar imagens - População
            fichaService.getFichaImages(ficha, 'GRAFICO_POPULACAO').each {
                addMap(it.localArquivo, it.legenda)
            }


            //************************************************************************
            // Ameaças
            //************************************************************************
            br()
            addTopic(_translate('Ameaças'), null, null, 100f)
            if (ficha.dsAmeaca) {
                addHtml(trim(_translate(ficha.dsAmeaca, 'ficha', 'dsAmeaca')), true)
            }
            lista = FichaAmeaca.findAllByFicha(ficha)
            if (lista) {
                br()
                table = new PdfPTable()
                table.setSpacingAfter(5)
                table.setWidthPercentage(100)
                table.setHorizontalAlignment(Element.ALIGN_CENTER)
                headers = [_translate('Tipo de ameaça'), _translate('Referência bibliográfica')]
                drawHeader(table, headers, colorHeader, [3.0f, 3.0f])
                lista.sort { it.criterioAmeacaIucn.ordem }.each {
                    // Ameaca
                    String ameacas = it?.criterioAmeacaIucn?.descricaoHieraquia?.replaceAll(/\s\/\s/, "\n")
                    // integracao SIS/IUCN
                    // fazer a tradução separando o codigo da descrição do item. Exemplo: 7.1 - Incêndios e supressão de incêndios
                    String listAmeacasTraduzidas = ameacas.split('\n').collect { it2 ->
                        ArrayList parts = it2.split(' - ')
                        try {
                            if (parts && parts.size() > 0) {
                                String codigo = parts[0]
                                String descricao = parts[1..-1].join(' - ') // remover o primeiro item do array (codigo)
                                return codigo + ' - ' + _translate(descricao, 'apoio', 'dsDadosApoio')
                            } else {
                                it2
                            }
                        } catch (ex) {
                            it2
                        }
                    }.join('\n')
                    table.addCell(getCell(listAmeacasTraduzidas))
                    //table.addCell( getCell( it?.criterioAmeacaIucn?.descricaoHieraquia.replaceAll(/\s\/\s/,"\n") ) )
                    // Ref. Bibliográfica
                    table.addCell(getCell(it?.refBibText))
                    // Regioes
                    if (it?.regioesText) {
                        cell = getCell(it.regioesText)
                        cell.setColspan(2)
                        cell.setPaddingLeft(30f)
                        table.addCell(cell)
                    }
                }
                document.add(table)
            }

            // adicionar imagens - ameaça
            fichaService.getFichaImages(ficha, 'MAPA_AMEACA').each {
                addMap(it.localArquivo, it.legenda)
            }

            //************************************************************************
            // Uso
            //************************************************************************
            lista = FichaUso.findAllByFicha(ficha)
            br()
            addTopic(_translate('Usos'), null, null, 100f)
            if (ficha.dsUso) {
                addHtml(trim(_translate(ficha.dsUso, 'ficha', 'dsUso')), true)
            }
            if (lista) {
                // --------------------------------------------------------------------------------------------------------------
                br()
                table = new PdfPTable(3, 3)
                table.setSpacingAfter(5)
                table.setWidthPercentage(100)
                table.setHorizontalAlignment(Element.ALIGN_CENTER)
                headers = [_translate('Tipo de uso'), _translate('Referência bibliográfica')]
                drawHeader(table, headers, colorHeader, [3.0f, 3.0f])
                lista.each {
                    // Uso
                    String usos = it?.uso?.descricaoHieraquia?.replaceAll(/\s\/\s/, "\n")
                    // integracao SIS/IUCN
                    // fazer a tradução separando o codigo da descrição do item. Exemplo:
                    String listUsosTraduzidos = usos.split('\n').collect { it2 ->
                        ArrayList parts = it2.split(' - ')
                        try {
                            if (parts && parts.size() > 0) {
                                String codigo = parts[0]
                                String descricao = parts[1..-1].join(' - ') // remover o primeiro item do array (codigo)
                                return codigo + ' - ' + _translate(descricao, 'uso', 'dsUso')
                            } else {
                                it2
                            }
                        } catch (ex) {
                            it2
                        }
                    }.join('\n')
                    table.addCell(getCell(listUsosTraduzidos))
                    //table.addCell( getCell( it?.uso?.descricaoHieraquia.replaceAll(/\s\/\s/,"\n") ) )

                    // Ref. Bibliográfica
                    table.addCell(getCell(it?.refBibText))
                    // Regiao
                    if (it?.regioesText) {
                        cell = getCell(it.regioesText)
                        cell.setColspan(2)
                        cell.setPaddingLeft(30f)
                        table.addCell(cell)
                    }
                }
                document.add(table)
            }


            //************************************************************************
            // CONSERVAÇÃO
            //************************************************************************
            br()
            addTopic(_translate('Conservação'))
            lista = TaxonHistoricoAvaliacao.createCriteria().list {
                eq('taxon.id', ficha.taxon.id)
                le('nuAnoAvaliacao', (ficha.cicloAvaliacao.nuAno + 4))
                tipoAvaliacao {
                    order('ordem')
                }
                order('nuAnoAvaliacao', 'desc')

            }
            //lista = TaxonHistoricoAvaliacao.findAllByTaxonAndNuAnoAvaliacaoLessThanEquals(ficha.taxon,ficha.cicloAvaliacao.nuAno+4).sort { it.nuAnoAvaliacao.toString() + it.tipoAvaliacao.ordem }
            if (lista) {
                br()
                addP(_translate('Histórico de avaliação'), f12B)
                boolean registroImpresso = false
                table = new PdfPTable()
                table.setSpacingBefore(5)
                table.setSpacingAfter(5)
                table.setWidthPercentage(100)
                table.setHorizontalAlignment(Element.ALIGN_CENTER)
                headers = [_translate('Tipo'), _translate('Ano'), _translate('Abrangência'), _translate('Categoria')
                           , _translate('Critério'), _translate('Referência bibliográfica')]
                drawHeader(table, headers, colorHeader, [1.1f, 1.0f, 1.0f, 1.2f, 1.0f, 1.0f])
                Boolean existeCategoriaOutra = false
                lista.each {
                    // a lista de historico contem todas as avaliações nacionais, estaduais e globais e no gride
                    // nao pode ser listada a ultima avaliação nacional novamente
                    boolean registroValido = true
                    /*if( it.tipoAvaliacao?.codigoSistema == 'NACIONAL_BRASIL'
                            && ultimaAvaliacao.nuAnoAvaliacao
                            && it.nuAnoAvaliacao.toInteger() >= ultimaAvaliacao.nuAnoAvaliacao.toInteger() ) {
                        registroValido = false
                    }*/
                    if (registroValido) {
                        registroImpresso = true
                        // tipo
                        table.addCell(getCell(_translate(it.tipoAvaliacao?.descricao, 'apoio', 'dsDadosApoio'), 'c'))
                        // Ano
                        table.addCell(getCell(it?.nuAnoAvaliacao.toString(), 'c'))
                        // Abrangencia
                        String abrangencia = it?.noRegiaoOutra ? _translate(it.noRegiaoOutra) : it?.abrangencia?.descricao
                        if (it?.abrangencia?.estado) {
                            abrangencia += ' (' + it.abrangencia.estado.sgEstado + ')'
                        }
                        table.addCell(getCell(abrangencia, 'c'))
                        // Categoria
                        if (it?.categoriaIucn?.codigoSistema != 'OUTRA') {
                            if (it?.categoriaIucn) {
                                table.addCell(getCell(_translate(it?.categoriaIucn.descricao) + ' (' + it.categoriaIucn.codigo + ')', 'c'))
                            } else {
                                table.addCell(getCell('', 'c'))
                            }
                            //table.addCell(getCell(it?.categoriaIucn?.descricaoCompleta, 'c'))
                        } else {
                            table.addCell(getCell(_translate(it?.deCategoriaOutra) + '*', 'c'))
                            existeCategoriaOutra = true
                        }
                        // Critério
                        table.addCell(getCell(it?.deCriterioAvaliacaoIucn, 'c'))

                        // Ref. Bibliográfica
                        table.addCell(getCell(it?.getRefBibText(ficha.id.toLong())))
                    }
                }
                if (existeCategoriaOutra) {
                    cell = getCell('* ' + _translate('Categoria não utilizada no método IUCN') + '.')
                    cell.setColspan(6)
                    cell.setPaddingLeft(5f)
                    table.addCell(cell)
                }
                document.add(table)
                if (ficha.dsHistoricoAvaliacao) {
                    br()
                    addP(_translate('Observações gerais sobre o histórico de avaliação'), f12B)
                    addHtml(_translate(ficha.dsHistoricoAvaliacao, 'ficha', 'dsHistoricoAvaliacao'), true)

                }
            }


            addP2(_translate('Presença em lista nacional oficial de espécies ameaçadas de extinção') + '? ', _translate(snd(ficha.stPresencaListaVigente)), f12B, f12, 5, true)


            // Presença em convenção (se não for preenchido não aparece)
            lista = FichaListaConvencao.findAllByFicha(ficha)
            if (lista) {
                br()
                addP(_translate('Presença em convenção'), f12B)
                table = new PdfPTable()
                table.setSpacingBefore(5)
                table.setSpacingAfter(5)
                table.setWidthPercentage(100)
                table.setHorizontalAlignment(Element.ALIGN_CENTER)
                headers = [_translate('Convenção'), _translate('Ano')]
                drawHeader(table, headers, colorHeader, [5.0f, 1.0f])
                lista.each {
                    // convencao
                    table.addCell(getCell(it.listaConvencao?.descricao))
                    // Ano
                    table.addCell(getCell(it?.nuAno.toString()))
                }
                document.add(table)
            }

            //************************************************************************
            // Ações de Conservação
            //************************************************************************
            br()
            addP(_translate('Ações de conservação'), f12B)

            if (trim(ficha.dsAcaoConservacao)) {
                addHtml(trim(_translate(ficha.dsAcaoConservacao, 'ficha', 'dsAcaoConservacao')), true)
            }
            lista = FichaAcaoConservacao.findAllByFicha(ficha)
            if (lista) {
                br()
                table = new PdfPTable()
                table.setSpacingAfter(5)
                table.setWidthPercentage(100)
                table.setHorizontalAlignment(Element.ALIGN_CENTER)
                headers = [_translate('Ação'), _translate('Situação'), _translate('Referência bibliográfica')]
                drawHeader(table, headers, colorHeader, [2.0f, 1.5f, 2.0f])
                lista.each {
                    // Acao
                    String acao = it.acaoConservacao?.descricaoCompleta?.replaceAll(/\s\/\s/, "\n")
                    // integracao SIS/IUCN
                    // fazer a tradução separando o codigo da descrição do item. Exemplo: 7.1 - Incêndios e supressão de incêndios
                    String listAcoesTraduzidas = acao.split('\n').collect { it2 ->
                        ArrayList parts = it2.split(' - ')
                        try {
                            if (parts && parts.size() > 0) {
                                String codigo = parts[0]
                                String descricao = parts[1..-1].join(' - ') // remover o primeiro item do array (codigo)

                                return codigo + ' - ' + _translate(descricao, 'apoio', 'dsDadosApoio')
                            } else {
                                it2
                            }
                        } catch (ex) {
                            it2
                        }
                    }.join('\n')
                    table.addCell(getCell(listAcoesTraduzidas))

                    // cell = getCell( it.acaoConservacao?.descricaoCompleta)
                    // cell.setVerticalAlignment(Element.ALIGN_MIDDLE)
                    //table.addCell(cell)

                    // situacao
                    table.addCell(getCell(_translate(it.situacaoAcaoConservacao?.descricao, 'apoio', 'dsDadosApoio'), 'c'))

                    // Ref. Bibliográfica
                    table.addCell(getCell(it?.refBibText))

                    // plano de ação ou ordenamento
                    strTemp = (it?.planoAcao?.sgPlanoAcao ? _translate(it.planoAcao.sgPlanoAcao, 'pan', 'sgPlanoAcao') :
                        (it?.tipoOrdenamento?.descricao ? _translate(it.tipoOrdenamento.descricao, 'apoio', 'dsDadosApoio') : ''))

                    if (strTemp != '') {
                        cell = getCell(strTemp)
                        cell.setColspan(3)
                        cell.setPaddingLeft(30f)
                        table.addCell(cell)
                    }
                }
                document.add(table)
            }

            // Presenca em UC
            strTemp = ficha.dsPresencaUc
            strTemp = strTemp.toString().trim()
            if (this.ucs || strTemp) {
                br()
                addP(_translate('Presença em UC'), f12B)
                if (ficha.dsPresencaUc) {
                    addHtml(trim(ficha.dsPresencaUc), true)
                }
                if (this.ucs) {
                    br()
                    table = new PdfPTable()
                    table.setSpacingBefore(5)
                    table.setSpacingAfter(5)
                    table.setWidthPercentage(100)
                    headers = ['UC', _translate('Referência bibliográfica')]
                    drawHeader(table, headers, colorHeader, [3.0f, 2.0f])
                    this.ucs.each {
                        // UC
                        table.addCell(getCell(it.no_uc, 'l'))
                        // Ref. Bibliográfica
                        String refs = br.gov.icmbio.Util.parseRefBib2Grid(it?.json_ref_bib?.toString(), true)
                        table.addCell(getCell(refs, 'l'))
                    }
                    document.add(table)
                }
            }


            // --------------------------------------------------------------------------------------------------------------
            // pesquisas
            lista = FichaPesquisa.findAllByFicha(ficha)
            strTemp = ficha.dsPesquisaExistNecessaria ?: ''
            strTemp = strTemp.toString().trim()
            if (lista || strTemp) {
                br()
                addTopic(_translate('Pesquisa'), null, null, 83)
                if (strTemp) {
                    addHtml(trim(_translate(ficha.dsPesquisaExistNecessaria, 'ficha', 'dsPesquisaExistNecessaria')), true)
                }
                if (lista) {
                    br()
                    table = new PdfPTable()
                    table.setSpacingBefore(5)
                    table.setSpacingAfter(5)
                    table.setWidthPercentage(100)
                    headers = [_translate('Tema'), _translate('Situação'), _translate('Referência bibliográfica')]
                    drawHeader(table, headers, colorHeader, [3.0f, 1.0f, 2.0f])
                    lista.each {
                        // Tema
                        table.addCell(getCell(_translate(it?.tema?.descricao, 'apoio', 'dsDadosApoio'), 'c'))

                        // Situacao
                        table.addCell(getCell(_translate(it?.situacao?.descricao, 'apoio', 'dsDadosApoio'), 'c'))

                        // Ref. Bibliográfica
                        table.addCell(getCell(it?.refBibText, 'c'))
                    }
                    document.add(table)
                }
            }

            // equipe tecnica
            if (ficha.dsEquipeTecnica) {
                table = new PdfPTable()
                table.setSpacingBefore(10)
                table.setSpacingAfter(5)
                table.setWidthPercentage(100)
                headers = [_translate('Equipe técnica')]
                drawHeader(table, headers, colorHeader, [1.0f])
                table.addCell(getCell(ficha.dsEquipeTecnica, 'j'))
                document.add(table)
            }

            // colaboradores
            if (ficha.dsColaboradores) {
                table = new PdfPTable()
                table.setSpacingBefore(10)
                table.setSpacingAfter(5)
                table.setWidthPercentage(100)
                headers = [_translate('Colaboradores')]
                drawHeader(table, headers, colorHeader, [1.0f])
                table.addCell(getCell(ficha.dsColaboradores, 'j'))
                document.add(table)
            }


            String comoCitar = fichaService.comoCitarFicha(ficha, ultimaAvaliacao?.mesAnoUltimaAvaliacao,false)
            table = new PdfPTable()
            table.setSpacingBefore(10)
            table.setSpacingAfter(5)
            table.setWidthPercentage(100)
            headers = [_translate('Como citar')]
            drawHeader(table, headers, colorHeader, [1.0f])
            table.addCell(getCell(comoCitar, 'j'))
            document.add(table)


            float avs = writer.getVerticalPosition(false) - document.bottomMargin();
            if (avs < 90f) {
                document.newPage()
            }

            // separar as referencias do SISBIO para uma página no final separada
            java.util.List refsTaxon = []
            java.util.List refsRegistrosOcorrencias = []

            if (this.refBibs.size() > 0) {
                this.refBibs.each { row ->
                    if (row.no_origem == 'ficha') {
                        if (!refsTaxon.contains(row.de_ref_bibliografica)) {
                            refsTaxon.push(row.de_ref_bibliografica)
                        }
                    } else {
                        if (!refsRegistrosOcorrencias.contains(row.de_ref_bibliografica)) {
                            refsRegistrosOcorrencias.push(row.de_ref_bibliografica)
                        }
                    }
                }
                if (refsTaxon) {
                    //br()
                    document.newPage()
                    addP(_translate('Referências bibliográficas'), f12B)
                    //lista.sort{ stu1, stu2 -> Util.removeAccents(stu1.publicacao.noAutor).compareToIgnoreCase( Util.removeAccents( stu2.publicacao.noAutor) ) }
                    addLine()
                    refsTaxon.each {
                        br()
                        addHtml(it, true, 12)
                    }
                }
                refsTaxon = [] // limpar da memoria

                if (refsRegistrosOcorrencias) {
                    try {
                        document.newPage()
                        addP(_translate('Referências dos registros'), f12B)
                        //lista.sort{ stu1, stu2 -> Util.removeAccents(stu1.publicacao.noAutor).compareToIgnoreCase( Util.removeAccents( stu2.publicacao.noAutor) ) }
                        addLine()
                        refsRegistrosOcorrencias.eachWithIndex { it, index ->
                            br()
                            addHtml(it, true, 12)
                        }
                    } catch (Exception e) {
                    }
                }
            }

            if (!contextoPublico && imprimirPendencias) {
                // imprimir as pendências
                lista = FichaPendencia.findAllByFichaAndStPendente(ficha, 'S', ['sort': 'dtPendencia', 'order': 'desc']);

                if (lista) {
                    document.newPage()
                    addP('Pendências', f12B)
                    addLine()
                    br()
                    table = new PdfPTable()
                    table.setSpacingBefore(5)
                    table.setSpacingAfter(5)
                    table.setWidthPercentage(100)
                    headers = ['Data', 'Assunto', 'Autor']
                    drawHeader(table, headers, colorHeader, [1.0f, 3.0f, 2.0f])
                    PdfPCell htmlCell
                    lista.each {
                        // Data
                        table.addCell(getCell(it?.dtPendencia?.format('dd/MM/yyyy'), 'c'))

                        // Assunto
                        table.addCell(getCell(it?.deAssunto, 'c'))

                        // Autor
                        table.addCell(getCell(it?.usuario.noPessoa, 'c'))

                        // Pendência
                        htmlCell = getCell('', 'j')
                        htmlCell.setColspan(3)
                        Paragraph px = new Paragraph()
                        px.setAlignment(Element.ALIGN_JUSTIFIED)
                        px.setFirstLineIndent(20)
                        px.setIndentationLeft(5)

                        try {
                            for (Element e : XMLWorkerHelper.parseToElementList(it?.txPendencia, null)) {
                                px.add(new Paragraph(e.join('')))
                            }
                            htmlCell.addElement(px)
                            table.addCell(htmlCell)
                        } catch (Exception e) {
                        }
                    }
                    document.add(table)
                }
            }


            /** /
             // Lista Ordenada
             https://www.mikesdotnetting.com/article/83/lists-with-itextsharp
             RomanList romanlist = new RomanList(true, 10);
             romanlist.indentationLeft = 10f;
             romanlist.add("One");
             romanlist.add("Two");
             romanlist.add("Three");
             romanlist.add("Four");
             romanlist.add("Five");
             document.add(romanlist);
             */

            /** /
             // Link - http://developers.itextpdf.com/examples/itext5-building-blocks/chunk-examples
             Font link = FontFactory.getFont("Arial", 12, Font.UNDERLINE, new BaseColor(0, 0, 255));
             Anchor anchor = new Anchor("www.mikesdotnetting.com", link);
             anchor.reference = "http://www.mikesdotnetting.com";
             document.add(anchor);
             /**/

            /** /
             // Bookmarks
             Font link = FontFactory.getFont("Arial", 12, Font.UNDERLINE, new BaseColor(0, 0, 255) );
             Paragraph p4 = new Paragraph();
             p4.add(new Chunk("Click "));
             p4.add(new Chunk("here", link).setLocalGoto("GOTO"))
             p4.add(new Chunk(" to find local goto"))
             p4.add(new Chunk("\n\n\n\n\n\n\n\n\nFim"))
             Paragraph p5 = new Paragraph()
             p5.add(new Chunk("Local Goto Destination").setLocalDestination("GOTO") )
             document.add(p4)
             document.add(p5)
             /**/

            /** /
             // Cor de fundo em um texto
             Font f = new Font(Font.FontFamily.TIMES_ROMAN, 25.0f, Font.BOLD, BaseColor.WHITE);
             Chunk c = new Chunk("White text on red background", f);
             c.setBackground(BaseColor.RED);
             Paragraph p = new Paragraph(c);
             document.add(p);
             /**/

            /** /
             // exibir alert ao clicar
             PdfAction action = PdfAction.javaScript("app.alert('Think before you print!');", writer );
             PdfAction action = PdfAction.javaScript("printCurrentPage(this.pageNum);", stamper.getWriter()));
             Chunk chunk = new Chunk("print this page");
             chunk.setAction(action);
             document.add( chunk )
             /**/


            document.close()
        }
        catch (Exception e)
        {
            String error = ''
            try {
                document.close()

                if( fileName )
                {
                    File file  = File( fileName.toString() )
                    if( file.exists() )
                    {
                        file.delete()
                    }
                }
            } catch( Exception e2 ) {}
            if( throwError ) {
                throw e;
            }
            //e.printStackTrace()
            e.getStackTrace().each {
                if (it.getFileName() == 'PdfFicha.groovy' && it.getLineNumber() != -1)
                {
                    println 'Erro:'
                    println 'Arquivo: ' + it.getFileName()
                    println 'Linha: ' + it.getLineNumber()
                    println 'Causa: ' + e.getMessage()
                    error = 'Erro: ' + it.getFileName() + '<br>' +
                        'Linha: ' + it.getLineNumber() + '<br>' +
                        'Causa: <span style="color:red;">' + e?.getMessage()?.replaceAll(/\n/, '<br>') + '</span>'
                }
            }
            return error
        }
        return fileName
    }

    /**
     * Imprimir texto com formatação HTML
     * @param html
     *
     */
    void addHtml( def html= null, Boolean justify = false, Integer fontSize = 14 ) {

        if( ! html )
        {
            //html = '<div><font size="1" color="silver">Não informado!</font></div>'
            html = ''
        }
        /*
        esse funcionou mas o proximo parece ser mais eficiente
        StringBuilder sb = new StringBuilder()
        sb.append(html);
        //def myString = sb.toString();
        Paragraph par = new Paragraph()
        ElementList list2 = XMLWorkerHelper.parseToElementList(sb.toString(), null);
        for (Element element : list2) {
            par.add(element);
        }
        document.add( par )
        */
        /** /
         println '----------------------------------------------------------------'
         println html
         println '----------------------------------------------------------------'
         /**/

        html = Util.stripTagsFichaPdf(html); // ja tem  o trimEditor()

        /*println '-'*70
        println html
        println '-'*70*/

        /*html = html.replaceAll(/font size/,'font X'); // anular <font size="">
        html = html.replaceAll(/font-size:/,'X:'); // anular fonte-size
        html = html.replaceAll(/line-height:/,'X:'); // anular font-height
        html = html.replaceAll(/font-family:/,'X:'); // anular font-family
        */

        if( html.trim() != '' ) {
            String css =''
            //html = '<div style="line-height:1.3;color=#000000;font-family:Times;font-size:' + (fontSize+2) + 'px;' + (justify ? 'text-align: justify;' : '') + '">' + html + '</div>'
            html = '<div style="line-height:1.3;color=#000000;font-family:'+fontNameCss+';font-size:' + (fontSize+2) + 'px;' + (justify ? 'text-align: justify;' : '') + '">' + html + '</div>'
            //html = '<div style="line-height:1.3;color=#000000;font-family:url(\'open-sans-regular.ttf\');font-size:' + (fontSize+2) + 'px;' + (justify ? 'text-align: justify;' : '') + '">' + html + '</div>'
            if( fontNameCss != 'Times') {
                css = """<style>
                           @font-face {font-family: '${ fontNameCss }'; src: url("open-sans-regular.ttf") format('truetype');}
                           body, div { font-family: '${ fontNameCss }'; color: #000000; font-size: 16px;}
                           </style>
                           """
            }
            //html = '<div style="line-height:1.3;color=#000000;font-family:url(\'Antroposofia.ttf\');font-size:' + (fontSize+2) + 'px;' + (justify ? 'text-align: justify;' : '') + '">' + html + '</div>'
            //html = '<div>' + html + '</div>'
            //html = style + html

            /*XMLWorkerHelper worker2 = XMLWorkerHelper.getInstance();
            worker2.parseXHtml(writer,document, html)
            InputStream cssInput2 = new ByteArrayInputStream("".getBytes(Charset.forName("UTF-8")));
            InputStream htmlInput2 = new ByteArrayInputStream(html.getBytes(Charset.forName("UTF-8")));
            worker2.parseXHtml(writer,document, htmlInput2, cssInput2, Charset.forName("UTF-8"), new XMLWorkerFontProvider("/data/salve-estadual/fonts/"))
            */



            //String css = '.xx { line-height:1.3;color=#000000;font-family:url(\'Antroposofia.ttf\');font-size:' + (fontSize+2) + 'px;' + (justify ? 'text-align: justify;' : '') + '}'
            //String css = ''
            html = html.replaceAll(/<br data-mce-bogus="1">/, '').replaceAll(/<p><\/p>/, '')
            // remover simbolos 'fl' 'ﬁ' do texto
            html = html.replaceAll(/ﬂ/, 'fl')
            html = html.replaceAll(/ﬁ/, 'fi')
            try {
                //InputStream inputStream = IOUtils.toInputStream(html, "UTF-8")
                //ReadAllStream.MemoryStream css = new ReadAllStream.MemoryStream().getBytes(  )
                //ByteArrayOutputStream css = new ByteArrayOutputStream();

                //XMLWorkerFontProvider fontProvider = new XMLWorkerFontProvider();
                //fontProvider.register("/data/salve-estadual/fonts/open-sans-regular.ttf");
                //fontProvider.register("/data/salve-estadual/fonts/");
                //fontProvider.register("resources/fonts/Cardo-Bold.ttf");
                //fontProvider.register("resources/fonts/Cardo-Italic.ttf");
                //fontProvider.addFontSubstitute("Times", "Arial");
                //String css = ""
                XMLWorkerHelper worker = XMLWorkerHelper.getInstance();
                InputStream cssInput = new ByteArrayInputStream(css.getBytes(Charset.forName("UTF-8")));
                InputStream htmlInput = new ByteArrayInputStream(html.getBytes(Charset.forName("UTF-8")));
                worker.parseXHtml(writer, document, htmlInput, cssInput, Charset.forName("UTF-8"), new XMLWorkerFontProvider("/data/salve-estadual/fonts/") );
                document.add(new Chunk(''))// atualizar posicionamento vertical do cursor na página
                //worker.parseXHtml(writer, document, htmlInput, cssInput, Charset.forName("UTF-8"), fontProvider );
                //XMLWorkerHelper.getInstance().parseXHtml(writer, document, inputStream,Charset.forName("UTF8"), new XMLWorkerFontProvider("/data/salve-estadual/fonts/") )
            }
            catch (Exception e) {
                String msg = '<div style="color:red;font-size:12px">html com erro!<p>'+e.getMessage()+'</p></div>'
                InputStream inputStream = IOUtils.toInputStream(msg, "UTF-8")
                XMLWorkerHelper.getInstance().parseXHtml(writer, document, inputStream)
                f10.setColor(255, 0, 0)
                addP(html, f10)
                f10.setColor(0, 0, 0)
            }
        }
        // ler arquivo html
        //XMLWorkerHelper.getInstance().parseXHtml(writer, document, new FileInputStream("/data/salve-estadual/temp/teste.html") );

        /*
        Primeira versão
        // tem itálico no texto
        //StringBuilder sb = new StringBuilder()
        sb.append(html)
        Paragraph p0 = new Paragraph('')
        ElementList list = XMLWorkerHelper.parseToElementList(sb.toString(), null)
        for (Element element : list) {
            p0.add(element)
        }
        document.add( p0 )
        */
    }

    void addLine(def text = null, def font = null){
        if( text )
        {
            if( ! font )
            {
                font = f12B
            }
            Paragraph p = new Paragraph(text, font );
            document.add(p)
            document.add(new LineSeparator(0.3f, 100, null, 0, -4));
            br()
        }
        else
        {
            document.add(new LineSeparator(0.3f, 100, null, 0, -5));
        }
    }

    void addP(def text = null, Font font = f12, String alignment = 'l', Integer marginRight = 0, Float lineHeight=14f)
    {
        if( !text || text == 'null' ){

            addHtml('') // imprimir não informado
            return;
        }
        text = trim( text )
        text = text.replaceAll(/null/, '')
        Paragraph p = new Paragraph(text, font)
        switch (alignment.toLowerCase())
        {
            case 'r':
                p.setAlignment(Element.ALIGN_RIGHT)
                break
            case 'c':
                p.setAlignment(Element.ALIGN_CENTER)
                break
            case 'j':
                p.setAlignment(Element.ALIGN_JUSTIFIED)
                break
            default:
                p.setAlignment(Element.ALIGN_LEFT)
        }
        p.setLeading(lineHeight) // line height
        p.setIndentationRight(marginRight)
        float vp = writer.getVerticalPosition(false) - ( document.bottomMargin() + document.topMargin() + 50 )
        if( vp < -133.75995 ) {
            document.newPage()
        }
        document.add(p)
    }

    void br(){
        addP(' ',f12);
    }

    // Sim,Não,Desconhecido
    String snd(def SND = null){
        switch (SND)
        {
            case 'S':
                return 'Sim'
                break;
            case 'N':
                return 'Não'
            case 'D':
                return 'Desconhecido'
            default:
                return ''
        }
    }
    /**
     * Fazer a impressão dos cabeçalhos das tabelas em negrito com fundo preenchido
     * @param table
     * @param headers
     * @param bgColor
     * @param colWidths
     */
    void drawHeader(PdfPTable table, ArrayList headers, BaseColor bgColor, ArrayList colWidths)
    {
        //table = new PdfPTable()
        table.resetColumnCount(colWidths.size())
        table.setWidths((Float[]) colWidths);
        table.setHeaderRows(1) // repetir o cabecalho se a tabela passar para a página seguinte
        // table.setSkipFirstHeader(true) // quando for preciso remover os titulos
        headers.each {
            PdfPCell cell = new PdfPCell(new Phrase(it))
            cell.setPadding(5)
            cell.setUseAscender(true) // posicionar corretamente o texto no centro verticalmente
            cell.setVerticalAlignment(PdfPCell.ALIGN_MIDDLE)
            cell.setHorizontalAlignment(Element.ALIGN_CENTER)
            cell.setBackgroundColor(bgColor)
            table.addCell(cell)
        }

    }
    /**
     * Remover os parágrafos e espaços em branco do início e final do texto
     * @param text
     * @return
     */
    String trim(def text = null)
    {
        text = text.toString().replaceAll(/^<p>&nbsp;<\/p>|<p>&nbsp;<\/p>$/, '').replaceAll(/<p><\/p>/,'')
        if (!text || text == 'null')
        {
            return '' // se não retornar espaço da erro no Phrase
        }
        return text
    }

    /**
     * Criar linha de tópico com fonte maior e em negrito
     * @param topic
     * @param bgColor
     * @param font
     * @example: addTopic ( ' Classificação Taxonômica ' )
     *               addTopic( 'Distribuição')
     */
    void addTopic( PdfPTable content, BaseColor bgColor = null, Float minFreeSpace = 65.0 ) {
        if( !bgColor ) {
            bgColor = colorTopic
        }
        float avs = writer.getVerticalPosition( false ) - document.bottomMargin();
        if( avs < minFreeSpace ) {
            document.newPage()
        }
        PdfPTable table = new PdfPTable( 1 )
        table.setSpacingBefore( 5 )
        table.setSpacingAfter( 5 )
        table.setWidthPercentage( 100 )
        table.setHorizontalAlignment( Element.ALIGN_CENTER )
        PdfPCell cell = new PdfPCell( content )
        cell.setCellEvent( new RoundedBorder() )
        cell.setBackgroundColor( bgColor )
        cell.setPadding( 8 )
        cell.setBorder( Rectangle.NO_BORDER )
        cell.setUseAscender( true ) // posicionar corretamente o texto no centro verticalmente
        cell.setVerticalAlignment( PdfPCell.ALIGN_MIDDLE )
        cell.setHorizontalAlignment( PdfPCell.ALIGN_CENTER )
        table.addCell( cell )
        document.add( table )
    }
    void addTopic( Phrase phrase, BaseColor bgColor = null, Font font = null, Float minFreeSpace = 65.0 ) {
        if( !bgColor ) {
            bgColor = colorTopic
        }
        if( !font ) {
            font = f14
        }
        float avs = writer.getVerticalPosition( false ) - document.bottomMargin();
        if( avs < minFreeSpace ) {
            document.newPage()
        }
        //topic +='     (' + avs.toString() + ')'
        PdfPTable table = new PdfPTable( 1 )
        table.setSpacingBefore( 5 )
        table.setSpacingAfter( 5 )
        table.setWidthPercentage( 100 )
        table.setHorizontalAlignment( Element.ALIGN_CENTER )
        PdfPCell cell = new PdfPCell( phrase )
        cell.setCellEvent( new RoundedBorder() )
        cell.setBackgroundColor( bgColor )
        cell.setPadding( 8 )
        cell.setBorder( Rectangle.NO_BORDER )
        cell.setUseAscender( true ) // posicionar corretamente o texto no centro verticalmente
        cell.setVerticalAlignment( PdfPCell.ALIGN_MIDDLE )
        cell.setHorizontalAlignment( PdfPCell.ALIGN_CENTER )
        table.addCell( cell )
        document.add( table )
    }

    void addTopic(String topic, BaseColor bgColor = null, Font font = null, Float minFreeSpace = 65.0){
        //Phrase phrase = new Phrase(topic, font)
        //addTopic( phrase ,bgColor, font, minFreeSpace )
        //return
        if (!bgColor)
        {
            bgColor = colorTopic
        }
        if (!font)
        {
            font = f14
        }
        float avs = writer.getVerticalPosition(false) - document.bottomMargin();
        if (avs < minFreeSpace)
        {
            document.newPage()
        }
        //topic +='     (' + avs.toString() + ')'
        PdfPTable table = new PdfPTable(1)
        table.setSpacingBefore(5)
        table.setSpacingAfter(1)
        table.setWidthPercentage(100)
        table.setHorizontalAlignment(Element.ALIGN_CENTER)
        PdfPCell cell = new PdfPCell(new Phrase(topic, font))
        cell.setCellEvent(new RoundedBorder())
        cell.setBackgroundColor(bgColor)
        cell.setPadding(8)
        cell.setBorder(Rectangle.NO_BORDER)
        cell.setUseAscender(true) // posicionar corretamente o texto no centro verticalmente
        cell.setVerticalAlignment(PdfPCell.ALIGN_MIDDLE)
        cell.setHorizontalAlignment(PdfPCell.ALIGN_CENTER)
        table.addCell(cell)
        document.add(table)
    }

/*    void addSubTopic(String topic, Font font = null)
    {
        if (!font)
        {
            font = f14B
        }
        float avs = writer.getVerticalPosition(true) - document.bottomMargin();
        if (avs < 65.0f)
        {
            document.newPage()
        }
        addLine( topic, font )
    }
*/

    /**
     * Imprimir Label:valor com fontes distintas
     * @param label
     * @param value
     * @param fontLabel
     * @param fontValue
     */

    void addP2(String label=null, String value=null, Font fontLabel=f12, Font fontValue=f12, Float padding = 5f, Boolean addBr = false ) {
        value = value.toString().replaceAll( /null/, '' )
        if( value.isEmpty() ) {
            return
        }
        // adicionar ponto final nos valores
        value = trim(value)
        if( ! ( value =~/\.$/ ) ) {
            value += '.'
        }
        PdfPTable table = new PdfPTable( 1 )
        table.setWidthPercentage( 100 )
        table.setSpacingAfter(0)
        table.setSpacingAfter(0)
        Phrase phrase = new Phrase();
        phrase.add( new Chunk( label, fontLabel ) )
        phrase.add( new Chunk( value, fontValue ) )
        PdfPCell cell = new PdfPCell( phrase )
        cell.setPadding( padding )
        cell.setPaddingLeft( 0 )
        cell.setUseAscender( true )
        cell.setBorder( Rectangle.NO_BORDER )
        cell.setHorizontalAlignment( Element.ALIGN_LEFT )
        table.addCell( cell )
        if( addBr ) {
            br()
        }
        float vp = writer.getVerticalPosition(false) - ( document.bottomMargin() + document.topMargin() + 50 )
        if( vp < -133.75995 ) {
            document.newPage()
        }
        document.add( table )

    }

    /**
     * Criar linha com rotulo:valor na mesma linha, sendo o rótulo em negrito e o valor normal.
     * @param label
     * @param value
     * @example: printLabelValue ( ' Reino : ' , ' Animália ' )
     */
    void printLabelValue(String label, String value, boolean italic = false){

        value = value.toString().replaceAll(/null/,'')
        value = trim( value )

        if( value.trim() == '' )
        {
            value = 'Não informado.'
        }
        PdfPTable table = new PdfPTable(1)
        table.setWidthPercentage(100)
        Phrase phrase = new Phrase();
        if( italic )
        {
            phrase.add(new Chunk(label, f12B))
            phrase.add(new Chunk(' ' + value, f12I))
        }
        else
        {
            phrase.add(new Chunk(label, f12B))
            phrase.add(new Chunk(' ' + value, f12))
        }
        PdfPCell cell = new PdfPCell(phrase)
        cell.setPadding(5)

        cell.setPaddingLeft(0)
        cell.setUseAscender(true)
        cell.setBorder(Rectangle.NO_BORDER)
        cell.setHorizontalAlignment(Element.ALIGN_LEFT)
        table.addCell(cell)
        document.add(table)

    }

    /**
     * Criar uma celula da tabela com as formatações padrão
     * @param text
     * @return
     */
    PdfPCell getCell(def text = null, def align = null, Font font = f12) {   // text = trim( text )
        if (! text || text== 'null')
        {
            text = ''
        }
        Phrase frase
        PdfPCell cellTemp
        text = text.replaceAll(/#NL#|\n/, "<br>")
        if( text =~ /<i>|<em>|<p>|<span>|<br>|\/>/) {
            try {
                cellTemp = new PdfPCell()
                text = '<span>' + text + '</span>'
                text = Util.stripTagsFichaPdf(text)
                frase = new Phrase( '', font )
                frase.setLeading(13) // line-height
                // frase.setFont(f12I)
                for (Element e : XMLWorkerHelper.parseToElementList(text.toString(), "")) {
                    //cellTemp.addElement(e);
                    frase.add( e )
                }
                cellTemp.addElement(frase)
                text=null
            } catch( Exception e ){
            }
        }

        if( text != null ) {
            text = Util.stripTags( text )
            frase = new Phrase( text, font )
            frase.setLeading(13) // line-height
            cellTemp = new PdfPCell( frase )
        }

        align = align ? align.toLowerCase() : 'l'
        switch( align )
        {
            case 'c':
                cellTemp.setHorizontalAlignment (Element.ALIGN_CENTER)
                break;
            case 'r':
                cellTemp.setHorizontalAlignment (Element.ALIGN_RIGHT)
                break;
            case 'j':
                cellTemp.setHorizontalAlignment (Element.ALIGN_JUSTIFIED)
                break;
            default:
                cellTemp.setHorizontalAlignment (Element.ALIGN_LEFT)
        }
        cellTemp.setBackgroundColor(BaseColor.WHITE)

        cellTemp.setPadding(5)
        cellTemp.setUseAscender(true)
        cellTemp.setUseDescender(true)
        cellTemp.setVerticalAlignment(Element.ALIGN_MIDDLE)
        return cellTemp
    }

    PdfPCell createImageCell(String path,Boolean fit = false, Float maxW = null,Float maxH = null )
    {
        Image img = Image.getInstance(path)
        if( !img )
        {
            return new PdfPCell()
        }
        Float imgH = img.getHeight()
        Float imgW = img.getWidth()
        if( maxH && imgH > maxH )
        {
            imgH = maxH
        }
        if( maxW && imgW > maxW )
        {
            imgW = maxW
            img.scaleToFit(imgW,imgH)
        }

        float avs = writer.getVerticalPosition(false) - document.bottomMargin();
        if( avs < imgH )
        {
            document.newPage()
        }


        PdfPCell cell = new PdfPCell(img, fit)
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE)
        cell.setHorizontalAlignment(Element.ALIGN_CENTER)
        return cell
    }

    void addMap( String imagePath = null, String legenda = null)
    {
        if ( ! imagePath || ! (imagePath =~ /\.(jpg|bmp|jpeg|png|tif?)$/)  )
        {
            return;
        }
        // calcular o tamanho da imagem
        Float leftMargin = document.leftMargin()
        Float rightMargin = document.rightMargin()
        Float maxH = writer.getPageSize().getHeight() / 2
        Float maxW = writer.getPageSize().getWidth() - (leftMargin + rightMargin)

        /*println 'left margin: ' + leftMargin
        println 'right margin: ' + rightMargin
        println 'maxH margin: ' + maxH
        println 'masW margin: ' + maxW
        */
        PdfPTable tbImg = new PdfPTable(1);
        tbImg.setWidthPercentage(100);
        PdfPCell imgCell = createImageCell(imagePath, false, maxW, maxH)
        imgCell.setMinimumHeight(100)
        imgCell.setBorder(Rectangle.NO_BORDER)
        tbImg.addCell(imgCell);
        /*
        // não é para sair a legenda do mapa  - reunião dia 29/06/2022
        if( legenda ) {
            Phrase frase = new Phrase( new Chunk(legenda,f6 ) )
            PdfPCell cell = new PdfPCell( frase )
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE)
            cell.setHorizontalAlignment(Element.ALIGN_LEFT)
            cell.setPadding(1)
            cell.setBorder(Rectangle.NO_BORDER)
            tbImg.addCell( cell )
        }*/
        br()
        document.add(tbImg)
    }

    /**
     * Alterar a virgula por ponto
     * @param valor
     * @return
     */
    String valorComVirgula( String valor = '' ){
        valor = valor.replaceAll(/\.0$/, '')
        return valor.replaceAll(/\./,',')
    }
    String valorComVirgula( Double valor ){
        return valorComVirgula( valor ? valor.toString() :'' )
    }


    /**
     * metodo para localizar a tradução do texto nas tabelas de tradução
     * Apoio, Pan, Uso, ficha revisão ou texto fixo
     *
     */
    private String _translate( String text='', String table='', String column='', String context = '' ) {
        // se não encontrar a tradução devolve o texto original
        String txTranslated = text ?: ''

        if (!text) {
            return text
        }
        // remover espaços do início e fim
        text = text.trim()
        if (this.translateToEnglish) {
            //println ' '
            //println text
            //println 'Ler a tradução de: ' + text
            // valores padrão
            table = table ? table.toLowerCase() : ''
            column = column ? Util.toSnakeCase(column) : ''

            // se não for informada a tabela assumir como texto fixo do glossario
            if (!table) {
                //println '  - tipo: Texto fixo'
                TraducaoTexto tt = TraducaoTexto.findByTxOriginal(text)
                if (tt) {
                    if (tt.txRevisado) {
                        //println '  - ok, lida a revisao'
                        txTranslated = tt.txRevisado
                    } else if (tt.txTraduzido) {
                        //println '  - ok, lida a traducao automatica'
                        txTranslated = tt.txTraduzido
                    }
                }
            } else {
                // localizar a tradução na tabela correta // traducao_revisao, traducao_ameaca, traducao_uso, traducao_pan
                if (table == 'apoio') {
                    // println '  - tipo: APOIO'
                    DadosApoio apoio
                    if (context) {
                        DadosApoio levelUp = DadosApoio.findByCodigoSistema(context.toUpperCase())
                        apoio = DadosApoio.findByDescricaoAndPai(text, levelUp)
                    } else {
                        apoio = DadosApoio.findByDescricao(text)
                    }
                    if (apoio) {
                        TraducaoDadosApoio tda = TraducaoDadosApoio.findByDadosApoio(apoio)
                        if (tda) {
                            if (tda.txRevisado) {
                                //println '  - ok, lida a revisao'
                                txTranslated = tda.txRevisado
                            } else if (tda.txTraduzido) {
                                //println '  - ok, lida a traducao automatica'
                                txTranslated = tda.txTraduzido
                            }
                        }
                    }
                } else if (table == 'ficha') {
                    //println '  - tipo: FICHA'
                    //println Util.toSnakeCase(column)
                    TraducaoTabela tb = TraducaoTabela.findByNoTabelaAndNoColuna(table, column)
                    if (tb) {
                        //println '  - Tb:'+tb.id
                        TraducaoRevisao tr = TraducaoRevisao.findByTraducaoTabelaAndSqRegistro(tb, sqFicha)
                        if (tr) {
                            if (tr.txTraduzido) {
                                txTranslated = tr.txTraduzido
                            }
                        }
                    }
                } else if (table == 'pan') {
                    PlanoAcao pan
                    if (column == 'sg_plano_acao') {
                        pan = PlanoAcao.findBySgPlanoAcao(text)
                    } else {
                        pan = PlanoAcao.findByTxPlanoAcao(text)
                    }
                    if (pan) {
                        TraducaoPan tb = TraducaoPan.findByPlanoAcao(pan)
                        if (tb) {
                            if (tb.txTraduzido) {
                                txTranslated = tb.txTraduzido
                            }
                        }
                    }
                } else if (table == 'uso') {
                    Uso uso
                    uso = Uso.findByDescricao( text )
                    if ( uso ) {
                        TraducaoUso tb = TraducaoUso.findByUso( uso )
                        if (tb) {
                            if (tb.txTraduzido) {
                                txTranslated = tb.txTraduzido
                            }
                        }
                    }
                } else if( table == 'regex' ){
                    // traduzir valores: 1 a 10 em 1 to 10
                    txTranslated = Util.traducaoRegex( text )
                }
            }
        }
        return txTranslated
    }
}
