package br.gov.icmbio

import com.lowagie.text.Chunk
import com.lowagie.text.Element

// https://coderanch.com/how-to/javadoc/itext-2.1.7/com/lowagie/text/Table.html

import com.lowagie.text.Paragraph
import com.lowagie.text.Table

class RtfOficinaMemoria extends RtfBase {
    private OficinaMemoria oficinaMemoria
    private String unidade
    private String unidadeSigla

    RtfOficinaMemoria( Long sqOficinaMemoria = 0,String unidade = '',String unidadeSigla='', String tmpDir ) {
        super(tmpDir)
        this.oficinaMemoria = OficinaMemoria.get( sqOficinaMemoria )
        this.unidade=unidade
        this.unidadeSigla=unidadeSigla
    }

    String run() {
        String agora = new Date().format('dd-MM-yyyy-HH-mm-ss')
        this.fileName = this.tmpDir + 'ata-memoria-reuniao-preparatoria-' + agora + '.rtf'
        Paragraph par
        try {
            this.dataImpressao = new Date().format('dd/MM/yyy HH:mm:ss')
            // criar e abrir o documento e criar o cabeçalho e o rodapé
            initDoc()
            setHeader('MINISTÉRIO DO MEIO AMBIENTE\n'
                    + 'INSTITUTO CHICO MENDES DE CONSERVAÇÃO DA BIODIVERSIDADE\n'
                    + 'DIRETORIA DE PESQUISA, AVALIAÇÃO E MONITORAMENTO DA BIODIVERSIDADE\n'
                    + this.unidade.toUpperCase()
                    , true
            )
            setFooter(this.unidadeSigla)

            // titulo
            par = new Paragraph( oficinaMemoria.oficina.noOficina, titleFont)
            par.setAlignment(Element.ALIGN_CENTER)
            document.add( par)

            br()

            doRvBold('Local: ', oficinaMemoria.deLocalOficina )
            doRvBold('Data: ', oficinaMemoria.periodo)

            br()

            // participantes da reunião
            par = new Paragraph('Participantes da Reunião:', textBoldFont)
            par.setAlignment(Element.ALIGN_LEFT)
            document.add( par )
            List participantes = []
            if( oficinaMemoria.oficina.participantes ) {

                /*
                oficinaMemoria.oficina.participantes.sort{it.pessoa.noPessoa}.each {
                    participantes.push(Util.capitalize( it.pessoa.noPessoa )
                            + ( it?.email ? ' / ' + it?.email?.toLowerCase() :'' )
                            + ( it?.instituicao?.sgInstituicao ? ' / ' + it.instituicao.sgInstituicao  : '')
                    )
                }
                doTexto(participantes.join('<br>'))*/
                Table table = doTable(['Nome', 'E-mail','Instituição'])
                oficinaMemoria.oficina.participantes.sort{it.pessoa.noPessoa}.each {
                    doRow(table, [ Util.capitalize( it.pessoa.noPessoa )
                                 , ( it?.email?.toLowerCase() )
                                 , ( it?.instituicao?.sgInstituicao ?: '')
                             ]
                    )
                }
                document.add(table)
            } else {
                doTexto('Nenhum participante informado.')
            }

            br()

            Integer oldFontSize = par.getFont().getSize();
            par = new Paragraph('Encaminhamentos', textBoldFont)
            par.getFont().setSize(oldFontSize + 3i)
            par.setAlignment(Element.ALIGN_CENTER)
            document.add(par)
            par.getFont().setSize(oldFontSize)

            br()

            doRvBold('Local da oficina: ', oficinaMemoria.deLocalOficina )
            doRvBold('Data proposta: ', oficinaMemoria.dtFimOficina.format('dd/MM/yyyy') )

            br()

            // participantes da oficina / convidados
            par = new Paragraph('Participantes da Oficina:', textBoldFont)
            par.setAlignment(Element.ALIGN_LEFT)
            document.add( par )

            // gride participantes
            /*oficinaMemoria.participantes.sort{it.noParticipante}.each {
                doTexto( it.noParticipante + ' / ' + it.papel.descricao )
            }*/
            Table table = doTable(['Nome', 'E-mail'])
            oficinaMemoria.participantes.sort{it.noParticipante}.each {
                doRow(table, [ it.noParticipante, it.papel.descricao ])
            }
            document.add(table)
            br()
            doRvBold('Nº salas: ', oficinaMemoria.deSalas)


            /*
            Table table = doTable(['Nome','Função'], 0, [2f, 1f])
            table.setWidth( 80 )
            oficinaMemoria.participantes.each {
                doRow(table, [it.noParticipante, it.papel.descricao], ['left', 'center'])
            }
            document.add(table)
            */
            br()
            par = new Paragraph('Outros Comentários', textBoldFont)
            par.setAlignment(Element.ALIGN_LEFT)
            document.add( par )
            doTexto( oficinaMemoria.txMemoria )

            br()

            // fim
            document.close()
        }
        catch (Exception e) {
            fileName = ''
            println e.getMessage()
            //e.printStackTrace()
        }
        return fileName
    }

    /**
     *
     * @param dia
     * @param periodo
     * @param mapPapeis
     * @param tipo - todos, somente LCs
     * @param considerarUAN - considerar Ultima Avaliação Nacional quando a ficha não tiver sido validada
     * @return
     */
    String runDocFinal(String dia = '', String periodo = '', Map mapPapeis = [:], String tipo = 'todos', String considerarUAN = 'N') {
        String agora = new Date().format('dd-MM-yyyy-hh-mm-ss')
        String nomeArquivo = 'final'
        Table table
        if (dia) {
            nomeArquivo = 'diario_de_' + dia.replaceAll('/', '-')
        }
        if (tipo == 'final-lc') {
            nomeArquivo = 'especies_mantidas_lc'
        }
        if (tipo == 'final-nao-lc') {
            nomeArquivo = 'especies_avaliadas'
        }
        if (tipo == 'final-todas') {
            nomeArquivo = 'todas_especies'
        }
        this.fileName = this.tmpDir + 'rel_' + nomeArquivo + '-oficina-' + agora + '.rtf'
        String strTemp
        List lstTemp
        Paragraph par

            this.dataImpressao = new Date().format('dd/MM/yyy HH:mm:ss')

        try {
            // criar e abrir o documento e criar o cabeçalho e o rodapé
            initDoc()

            //String headerText = 'MINISTÉRIO DO MEIO AMBIENTE\n INSTITUTO CHICO MENDES DE CONSERVAÇÃO DA BIODIVERSIDADE\n' + this.unidade.toUpperCase()
            //setHeader(headerText,true)
            //setFooter(this.unidadeSigla)

            /**
             * Adicionar o cabeçalho somente na primeira página
             */
            doPageHeader('MINISTÉRIO DO MEIO AMBIENTE\nINSTITUTO CHICO MENDES DE CONSERVAÇÃO DA BIODIVERSIDADE\nDIRETORIA DE PESQUISA, AVALIAÇÃO E MONITORAMENTO DA BIODIVERSIDADE\n' + this.unidade.toUpperCase())

            //document.setHeader(null)
            setFooter('')
            String titulo = oficina.noOficina
            switch (tipo) {
                case 'final-nao-lc':
                    titulo = 'Relatório Final\n' + titulo + ': Espécies Avaliadas'
                    break;
                case 'final-lc':
                    titulo = 'Relatório Final\n' + titulo + ': Espécies Mantidas LCs'
                    break;
                case 'diario':
                    titulo = 'Relatório Diário\n' + titulo + ': ' + dia
                    break;
            }

            br()
            // titulo
            par = new Paragraph(titulo, titleFont)
            par.setAlignment(Element.ALIGN_CENTER)
            document.add(par)

            br()
            br()

            doRvBold('Data: ', periodo)
            doRvBold('Local: ', Util.capitalize( oficina.deLocal ) )

            br()
            br()

            // gride totais
            table = doTable(['Categorias Avaliadas'], 2, [3f, 1f])
            table.setWidth(70)
            mapTotais.qtdCategoria.each {
                if (it.key.indexOf('Total') == 0) {
                    doRow(table, [it.key, it.value], ['left', 'center'], corCinzaClaro, textBoldFont)
                } else {
                    doRow(table, [it.key, it.value], ['left', 'center'])
                }
            }
            document.add(table)

            br()
            br()
            br()

            // resultado das fichas avaliadas
            //
            table = doTable(['#', 'Taxon', 'Categoria', 'Critério', 'Justificativa'], 0, [0.5f, 1.5f, 1f, 1f, 5f], textBoldFont)
            table.setCellsFitPage(true);

            //Map dadosUltimaAvaliacao
            int row = 1
            String categoriaImprimir
            String criterioImprimir
            String possivelmenteExtinta
            String justificativaImprimir
            //oficina.oficinaFichas.sort{ it.vwFicha.nmCientifico }.each {
            oficina.oficinaFichas.findAll {
                !it.stTransferida && it.vwFicha?.situacaoFicha?.codigoSistema != 'EXCLUIDA'
            }.sort { it.vwFicha.nmCientifico }.each {
                categoriaImprimir = it?.categoriaIucn?.codigo ?: ''
                criterioImprimir = it.dsCriterioAvalIucn ?: ''
                possivelmenteExtinta = ""
                if (categoriaImprimir == 'CR') {
                    possivelmenteExtinta = (it.stPossivelmenteExtinta == 'S' ? "\n(PEX)" : "")
                }
                justificativaImprimir = it.dsJustificativa ?: ''

                // imprimir as fichas que possuem avaliação anterior
                if ((!justificativaImprimir || !categoriaImprimir) && considerarUAN == 'S') {
                    Map dadosUltimaAvaliacao = it.vwFicha.ultimaAvaliacaoNacional
                    if (dadosUltimaAvaliacao.deCategoriaIucn) {
                        categoriaImprimir = dadosUltimaAvaliacao.coCategoriaIucn
                        criterioImprimir = dadosUltimaAvaliacao.deCriterioAvaliacaoIucn
                        justificativaImprimir = dadosUltimaAvaliacao.txJustificativaAvaliacao
                        if (categoriaImprimir == 'CR') {
                            possivelmenteExtinta = (dadosUltimaAvaliacao?.stPossivelmenteExtinta == 'S' ? "\n(PEX)" : "")
                        }
                    }
                }
                if (categoriaImprimir) {
                    /*dadosUltimaAvaliacao=[:]
                    if( tipo=='final-lc' || tipo=='final-nao-lc')
                    {
                        dadosUltimaAvaliacao = it.vwFicha.ultimaAvaliacaoNacional
                    }
                    */
                    boolean valido = false
                    //println tipo+' / ' + it.dtAvaliacao.format('dd/MM/yyyy')+ ' = ' + dia
                    //println ' '
                    // mantiveram LC
                    if (tipo == 'final-lc') {
                        /*if( dadosUltimaAvaliacao.coCategoriaIucn == 'LC' && it?.categoriaIucn?.codigoSistema=='LC')
                        {
                            valido = true
                        }
                        */
                        valido = (it.vwFicha.stManterLc == true)
                    } else if (tipo == 'final-nao-lc') {
                        // POS_OFICINA = AVALIADA
                        //valido = ( it.vwFicha.cdSituacaoFichaSistema == 'POS_OFICINA' && it.vwFicha.stManterLc != true )
                        valido = true
                    } else if (tipo == 'final-revisadas') {
                        valido = (it.vwFicha.cdSituacaoFichaSistema == 'AVALIADA_REVISADA')
                    } else if (tipo == 'diario') {
                        if (it.dtAvaliacao.format('dd/MM/yyyy') == dia) {
                            valido = true
                        }
                    } else {
                        valido = true
                    }

                    if (valido) {
                        // nome científico
                        Paragraph parNoCientifico = new Paragraph('', textFont)
                        Chunk chunk = new Chunk(it.vwFicha.taxon.noCientifico, textItalicFont)
                        chunk.getFont().setColor(0, 0, 0)
                        parNoCientifico.add(chunk)

                        // autor do Taxon
                        chunk.getFont().setColor(0, 0, 0) // remover cor
                        chunk = new Chunk(' ' + (it.vwFicha?.taxon?.noAutor ?: ''), textFont)
                        parNoCientifico.add( chunk )
                        doRow(table, [(row++),
                                      parNoCientifico
                                      , categoriaImprimir + possivelmenteExtinta //it.categoriaIucn.codigo
                                      , criterioImprimir // it.dsCriterioAvalIucn
                                      , justificativaImprimir //it.dsJustificativa
                        ]
                        , ['center', 'left', 'center', 'center', 'justify'], null, textFont)
                    }
                }
            }
            document.add(table)

            if (table.size() == 1) {
                if (tipo == 'diario') {
                    par = new Paragraph('Nenhuma ficha avaliada em ' + dia, textBoldFont)
                } else {
                    par = new Paragraph('Nenhuma ficha avaliada', textBoldFont)
                }
                par.setAlignment(Element.ALIGN_CENTER)
                document.add(par)
            }

            br()

            // fim relatorio final
            par = new Paragraph(Util.capitalize( oficina.deLocal ) +', '+ Util.capitalize( new Date().format("dd 'de' MMMM 'de' yyyy") )  + '.', textFont)
            par.setAlignment(Element.ALIGN_RIGHT)
            document.add(par)

            br()

            // participantes
            Integer oldFontSize = par.getFont().getSize();
            par = new Paragraph('Participantes', textBoldFont)
            par.getFont().setSize(oldFontSize + 4i)
            par.setAlignment(Element.ALIGN_CENTER)
            document.add(par)
            par.getFont().setSize(oldFontSize)
            mapPapeis.each {
                br()
                par = new Paragraph(it.key + (it.value.size() > 1 && (it.key =~ /or$/) ? 'es' : ''), textBoldFont)
                //par = new Paragraph( it.key, textBoldFont )
                par.setAlignment(Element.ALIGN_LEFT)
                document.add(par)
                it.value.eachWithIndex { nome, i ->
                    // complementar com linhas no final para assinar
                    //par = new Paragraph("\t" + (i + 1) + ') ' + (nome + ' ').toString().padRight(75, '_'), textFont)

                    par = new Paragraph("\t" + (i + 1) + ') ' + (nome + ' ').toString(), textFont)
                    par.setAlignment(Element.ALIGN_LEFT)
                    document.add(par)
                    //br()
                }
            }

            // fim
            document.close()
        }
        catch (Exception e) {
            fileName = ''
            println e.getMessage()
            //e.printStackTrace()
        }
        return fileName
    }

    /**
     * Relatório das fichas mantidas LC
     * @return
     */
    String runDocLC( Map data = [:]) {
        String agora = new Date().format('dd-MM-yyyy-hh-mm-ss')
        String nomeArquivo = 'lcs'
        this.fileName = this.tmpDir + 'rel_' + nomeArquivo + '-oficina-' + agora + '.rtf'
        String strTemp
        List lstTemp
        Paragraph par
        try {
            this.dataImpressao = new Date().format('dd/MM/yyy HH:mm:ss')

            // criar e abrir o documento e criar o cabeçalho e o rodapé
            initDoc()
            setHeader('MINISTÉRIO DO MEIO AMBIENTE\n'
                    + 'INSTITUTO CHICO MENDES DE CONSERVAÇÃO DA BIODIVERSIDADE\n'
                    + 'DIRETORIA DE PESQUISA, AVALIAÇÃO E MONITORAMENTO DA BIODIVERSIDADE\n'
                    + this.unidade.toUpperCase(),true
            )
            //setFooter(this.unidadeSigla)
            setFooter('')
            String titulo = 'Grade de Espécies com Indicação de Manutenção da Categoria LC\n' + oficina.noOficina

            // titulo
            par = new Paragraph(titulo, titleFont)
            par.setAlignment(Element.ALIGN_CENTER)
            document.add(par)

            br()

            doRvBold('Data.: ', new Date().format('dd/MM/yyyy').toUpperCase() )
            doRvBold('Local: ', Util.capitalize( oficina.deLocal) )

            br()

            // selecionar as fichas
            Table table = doTable(['#', 'Taxon', 'Justificativa','Observação'], 0, [0.5f, 1.5f, 4f , 1f], textBoldFont)
            //Map dadosUltimaAvaliacao
            int row = 1

            List fichasMarcadasPeloUsuario = []
            if( data.ids ) {
                fichasMarcadasPeloUsuario = data.ids.split(',').collect { it.toLong() }
            }


            oficina.oficinaFichas.findAll {
                ! it.stTransferida &&
                it.vwFicha?.situacaoFicha?.codigoSistema != 'EXCLUIDA' &&
                it.vwFicha.stManterLc == true &&
                ( fichasMarcadasPeloUsuario.size() == 0 || fichasMarcadasPeloUsuario.contains( it.vwFicha.id.toLong() ) )
            }.sort { it.vwFicha.nmCientifico }.each {

                String justificativaImprimir = ''
                if( it.categoriaIucn )
                {
                    justificativaImprimir = it.dsJustificativa?:''
                }
                else
                {
                    Map ultimaAvaliacao = it.ficha.ultimaAvaliacaoNacional
                    justificativaImprimir = ultimaAvaliacao.txJustificativaAvaliacao ?:''
                }

                // nome científico
                Paragraph parNoCientifico = new Paragraph('', textFont)
                Chunk chunk = new Chunk(it.vwFicha.taxon.noCientifico, textItalicFont)
                chunk.getFont().setColor(0, 0, 0)
                parNoCientifico.add( chunk )

                // autor do Taxon
                chunk.getFont().setColor(0, 0, 0) // remover cor
                chunk = new Chunk(' ' + (it.vwFicha?.taxon?.noAutor ?: ''), textFont)
                parNoCientifico.add( chunk )

                doRow(table, [(row++),
                              parNoCientifico,
                              justificativaImprimir
                              ,''
                ]
                , ['center', 'left', 'justify', 'center'], null, textFont)
            }
            document.add( table )

            // fim
            document.close()
        } catch (Exception e) {
            fileName = ''
            println e.getMessage()
            //e.printStackTrace()
        }
        return fileName
    }
}
