package br.gov.icmbio

import com.itextpdf.text.BaseColor
import com.itextpdf.text.Element
import com.itextpdf.text.Font
import com.itextpdf.text.Image
import com.itextpdf.text.Paragraph
import com.itextpdf.text.Rectangle
import com.itextpdf.text.pdf.BarcodeQRCode
import com.itextpdf.text.pdf.BaseFont
import com.itextpdf.text.pdf.ColumnText
import com.itextpdf.text.pdf.PdfContentByte
import com.itextpdf.text.pdf.PdfGState
import com.itextpdf.text.pdf.PdfPCell
import com.itextpdf.text.pdf.PdfPTable
import com.itextpdf.text.pdf.PdfReader
import com.itextpdf.text.pdf.PdfStamper
import com.itextpdf.text.pdf.RandomAccessFileOrArray
import com.lowagie.text.rtf.document.RtfDocument
import grails.converters.JSON

import grails.util.Environment
import groovy.json.JsonBuilder
import groovy.json.JsonSlurper
import groovy.json.JsonOutput
import jdk.nashorn.internal.runtime.regexp.RegExp
import org.apache.commons.logging.LogFactory
import org.codehaus.groovy.grails.web.json.JSONArray
import org.codehaus.groovy.grails.web.json.JSONObject

//import org.codehaus.groovy.grails.web.servlet.mvc.GrailsParameterMap
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.safety.Whitelist
import org.springframework.web.context.request.RequestContextHolder
import org.springframework.web.context.request.ServletRequestAttributes

import java.nio.channels.FileChannel
import java.nio.channels.FileLock
import java.nio.channels.OverlappingFileLockException
import java.text.Normalizer
import javax.swing.text.MaskFormatter
import java.text.DecimalFormat
import java.nio.charset.StandardCharsets

// calcular MD5 e SHA1
import java.security.*
import java.util.regex.Matcher
import java.util.regex.Pattern
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream

class Util {

    //simply
    private static final log = LogFactory.getLog(this)
    private static String comPessText = '(com. pess.)'

    static List specialChars = [["&lt;", "<"]
                                , ["&gt;", ">"]
                                , ["&rdquo;", '”']
                                , ["&ldquo;", '“']
                                , ["&amp;", "&"]
                                , ["&quot;", "\""]
                                , ["&agrave;", "à"]
                                , ["&Agrave;", "À"]
                                , ["&acirc;", "â"]
                                , ["&atilde;", "ã"]
                                , ["&Atilde;", "ã"]
                                , ["&auml;", "ä"]
                                , ["&Auml;", "Ä"]
                                , ["&Acirc;", "Â"]
                                , ["&aring;", "å"]
                                , ["&Aring;", "Å"]
                                , ["&aelig;", "æ"]
                                , ["&AElig;", "Æ"]
                                , ["&aacute;", "á"]
                                , ["&Aacute;", "Á"]
                                , ["&ccedil;", "ç"]
                                , ["&Ccedil;", "Ç"]
                                , ["&eacute;", "é"]
                                , ["&Eacute;", "É"]
                                , ["&egrave;", "è"]
                                , ["&Egrave;", "È"]
                                , ["&ecirc;", "ê"]
                                , ["&Ecirc;", "Ê"]
                                , ["&euml;", "ë"]
                                , ["&Euml;", "Ë"]
                                , ["&iuml;", "ï"]
                                , ["&Iuml;", "Ï"]
                                , ["&iacute;", "í"]
                                , ["&Iacute;", "í"]
                                , ["&oacute;", "ó"]
                                , ["&Oacute;", "Ó"]
                                , ["&otilde;", "õ"]
                                , ["&Otilde;", "Õ"]
                                , ["&ocirc;", "ô"]
                                , ["&Ocirc;", "Ô"]
                                , ["&uacute;", "ú"]
                                , ["&Uacute;", "Ú"]
                                , ["&ouml;", "ö"]
                                , ["&Ouml;", "Ö"]
                                , ["&oslash;", "ø"]
                                , ["&Oslash;", "Ø"]
                                , ["&szlig;", "ß"]
                                , ["&ugrave;", "ù"]
                                , ["&Ugrave;", "Ù"]
                                , ["&ucirc;", "û"]
                                , ["&Ucirc;", "Û"]
                                , ["&uuml;", "ü"]
                                , ["&Uuml;", "Ü"]
                                , ["&nbsp;", " "]
                                , ["&copy;", "\u00a9"]
                                , ["&reg;", "\u00ae"]
                                , ["&euro;", "\u20a0"]]

    static int dateDiff(Date d1, Date d2, String what) {
        d2 = d2 ?: new Date();
        Calendar dataInicial = Calendar.getInstance();
        Calendar dataFinal = Calendar.getInstance();
        dataInicial.setTime(d1);
        dataFinal.setTime(d2);
        long millis = dataFinal.getTimeInMillis() - dataInicial.getTimeInMillis();
        long seconds = millis / 1000;
        long minutes = ((seconds / 60) < 0) ? 0 : (seconds / 60);
        long hours = ((minutes / 60) < 0) ? 0 : (minutes / 60);
        long days = ((hours / 24) < 0) ? 0 : (hours / 24);
        /*  println '############################'
          println d2;
          println d1;
          println "dias: " + days.toString();
          println "Horas: " + hours.toString();
          println "Minutos: " + minutes.toString();
          println "Segundos: " + seconds.toString();
          println "Milisegundos:" + millis.toString();
          println '############################'
          */
        switch (what.toUpperCase()) {
            case 'D':
                return days;
            case 'H':
                return hours;
            case 'M':
                return minutes;
            case 'S':
                return seconds;
        }
        return 0;

    }

    static int dateDiff(String startDate, Date endDate, String what) {
        endDate = endDate ?: new Date();
        Date d2 = Date.parse("dd/MM/yyyy HH:mm:ss", endDate.getDateTimeString());
        Date d1 = Date.parse("dd/MM/yyyy HH:mm:ss", startDate);
        return dateDiff(d1, d2, what);
    }

    /**
     * Retorna a data atual sem as horas
     * @return
     */
    static Date hoje() {
        return Date.parse("dd/MM/yyyy", new Date().format('dd/MM/yyyy'))
    }

    /**
     * Retorna a data atual com as horas até segundos
     * @return
     */
    static Date agora() {
        return Date.parse("dd/MM/yyyy HH:mm:ss", new Date().format('dd/MM/yyyy HH:mm:ss'))
    }

    /**
     * Método para remover os acentos para caractere sem acento correspondente.
     * @param String - palavra com acentos
     * @return String
     */
    static String removeAccents(String str) {
        if (str) {
            try {
                str = URLDecoder.decode( str, StandardCharsets.UTF_8.name() )
                //return sun.text.Normalizer.normalize(str, sun.text.Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
                str = Normalizer.normalize(str, Normalizer.Form.NFD);
                return str.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
            } catch (Exception e) {
                return str
            }
        }
        return str
    }

    static String _getHash(String txt = '', String hashType = 'SHA1') {
        try {
            MessageDigest md = MessageDigest.getInstance(hashType);
            byte[] array = md.digest(txt.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            //error action
        }
        return ''
    }

    static String md5(String txt) {
        return this._getHash(txt.toString(), "MD5")
    }

    static String sha1(String txt) {
        return this._getHash(txt.toString(), "SHA1")
    }

    static void d(Object value) {
        value = (value == null ? 'null' : value)
        //def msg = Environment.current.toString() +' ['+this.getClass().toString().replace('class br.gov.icmbio.','')+'] ' + value.toString();
        String msg = value.toString()
        log.info(msg)
        /*
        if (Environment.current == Environment.DEVELOPMENT) {
            if (msg != '') {
                println msg
                log.info( msg )
            }
        }*/
    }

    /**
     * Método para formatar strings
     * @url http://docs.oracle.com/javase/6/docs/api/javax/swing/text/MaskFormatter.html
     * @param String value - string a ser formatada
     * @param String pattern - máscara de formatação
     * @return String
     */
    static String formatStr(String value, String pattern) {
        MaskFormatter mask
        try {
            if( !value.trim() ) {
                return ''
            }
            mask = new MaskFormatter(pattern)
            mask.setValueContainsLiteralCharacters(false)
            return mask.valueToString(value)
        }
        catch (Exception e) {
            return value
        }
    }

    static String formatCpf(String cpf = '' ) {
        return formatStr(cpf, '###.###.###-##')
    }

    static String formatCnpj(String cnpj = '') {
        return formatStr(cnpj, '##.###.###/####-##')
    }

    static String formatNumber(def valor, def mascara) {
        def f = new DecimalFormat(mascara);
        return f.format(valor);
    }

    static String formatInt(def valor) {
        return String.format("%,d", valor)
    }

    static String getFileExtension(String fileName) {
        if (!fileName) {
            return ''
        }
        Integer pos = fileName.lastIndexOf('.')
        if (pos > 0) {
            return fileName.substring(pos + 1)
        }
        return ''
    }

    static getFileNameOnly(String fileName=''){
        if( fileName ) {
            int  lastBar = fileName.lastIndexOf('/')
            fileName = fileName.substring(lastBar + 1 )
        }
        return fileName
    }

    // request.getRequestURL().toString()
    static String getBaseUrl(String requestUrl = '' ) {
        try {
            if ( requestUrl ) {
                // remover a porta quando for producao
                if ( ( requestUrl.toString() =~ /\/\/salve-estadual\.icmbio\.gov\.br/) ) {
                    requestUrl = requestUrl.replaceAll(':8080', '').replaceAll('http:', 'https:')
                }
                return requestUrl.substring(0, requestUrl.indexOf("/", 8)) + '/salve-estadual/'
            } else {
                return 'https://salve.icmbio.gov.br/salve-estadual/'
            }
        } catch (Exception e) {
            return 'https://salve.icmbio.gov.br/salve-estadual/'
        }
    }

    static String trimEditor(String texto) {
        if (texto) {

            // estes caracteres foram encontrados no texto e possuem uma codificação diferente escondida (fantasma)
            texto = texto.replaceAll(/çã/,'çã');
            texto = texto.replaceAll(/ça/,'ça');
            texto = texto.replaceAll(/á/,'á');
            texto = texto.replaceAll(/é/,'é');
            texto = texto.replaceAll(/ã/,'ã');
            texto = texto.replaceAll(/ /,' ');
            texto = texto.replaceAll(/ﬀ/,'ff');


            return texto.replaceAll(/^<p><\/p>$|<p><span><\/span><\/p>|<p>&nbsp;<\/p>|<p style="text-align: justify;">&nbsp;<\/p>/, '').
                replaceAll(/&nbsp;/, ' ').trim()
        }
        return texto
    }

    static String getRandomColor() {
        String[] letters = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F"];
        String color = "#";
        for (int i = 0; i < 6; i++) {
            color += letters[(int) Math.round(Math.random() * 15)];
        }
        return color;
    }

    /**
     * método para converter acentos no formato html ex: &ccedil em ç
     * @param texto [description]
     * @return [description]
     */
    static String html2str(String texto = '') {
        if( texto ) {
            specialChars.each {
                texto = texto.replace(it[0], it[1]);
            }
        }
        return texto
    }

    /**
     * método para converter acentos no formato texto para html ex: ç em &ccedil
     * @param texto [description]
     * @return [description]
     */
    static String str2html(String texto = '') {
        if( texto ) {
            specialChars.each {
                texto = texto.replace(it[1], it[0]);
            }
        }
        return texto
    }

    /**
     * método para formatar o texto do parágrafo: "Como citar"
     * @param noCientifico
     * @param autores
     * @param anoComoCitar
     * @return
     */
    static getComoCitar(String noCientifico = null, String autores = null, String anoComoCitar = null, Boolean etAl = true ){
        String resultado
        String data = new Date().format("dd 'de 'MMM'.' ' de' yyyy").replaceAll(/ mai\. /, ' maio ')
        String nomeSistema = 'Sistema de Avaliação do Risco de Extinção da Biodiversidade - SALVE-ESTADUAL'
        noCientifico = noCientifico ? '<i>'+Util.stripTags(noCientifico)+'</i>' : ''
        // se não enviar os autores e o ano é porque a ficha ainda não está publicada
        if( autores && anoComoCitar ) {
            autores = autores.replaceAll(/\.$/,'') // remover ponto do final
            if( etAl ) {
                autores = getNomesEtAll(autores)
            } else {
                autores.split(';').each { autor ->
                    autor = Util.nome2citacao(autor)
                }
            }
            autores += autores ? '.' : ''
            anoComoCitar = anoComoCitar ? ' ' + anoComoCitar?.toString() + '.' : ''
            resultado = "${autores+anoComoCitar} ${noCientifico}. ${nomeSistema}. Disponível em: <a target=\"_blank\" href=\"https://salve.icmbio.gov.br\">https://salve.icmbio.gov.br</a>. Acesso em: ${data}."
        } else {
            anoComoCitar = anoComoCitar ?: new Date().format("yyyy")
            resultado = "ICMBio, ${anoComoCitar}. ${nomeSistema}. Dados não publicados. Acesso em: ${data}."
        }
        return resultado
    }

    /**
     * método para os nomes dos autores no formato utilizado para citação.
     * @param nomes
     * @param anoPublicacao
     * @param dtPublicacao
     * @return
     */
    static String getCitacao(String nomes = null, Integer anoPublicacao = null, Date dtPublicacao = null) {
        if (!nomes) {
            return ''
        }
        nomes = Util.getNomesEtAll(nomes)
        if (anoPublicacao) {
            String nuAno = anoPublicacao.toString() == '9999' ? 's.d.' : anoPublicacao.toString()
            nomes += ', ' + nuAno
        } else if (dtPublicacao) {
            nomes += ', ' + dtPublicacao.format('dd/MM/yyyy')
        }
        return nomes
    }


    /**
     * método para retornar os nomes dos autores utilizando Et. Al quando tiver mais de 2 autores
     * @param nomes
     * @return
     */
    static String getNomesEtAll(String nomes = '') {
        if ( ! nomes ) {
            return ''
        }
        // trocar todos os '; & \n' para ';'
        List listNomes = nomes.replaceAll(/\s+;\s+|\s+&\s+|\n/, '; ').split(';');
        // ler somente o sobrenome dos nomes;
        listNomes.eachWithIndex { nome, i ->
            if ( nome.indexOf(',') ) {
                listNomes[i] = nome.split(',')[0]
            }

        }
        switch (listNomes.size()) {
            case 1:
                return listNomes[0].trim();
                break;
            case 2:
                return listNomes[0].trim() + ' & ' + listNomes[1].trim();
                break;
            default:
                return listNomes[0].trim() + ' <i>et al.</i>';
        }
        return nomes;
    }


    /**
     * método para colocar o nome completo no formato da citação.
     * @example: Joaquim José da Silva Xavier = Xavier, J.J.S
     * @param nome
     * @return
     */
    static String nome2citacao(String nome ) {
        /*nome = nome.replaceAll( /\n\r/,';')
        nome = nome.replaceAll( /\n/,';')
        nome = nome.replaceAll( /\r/,';')*/
        if( nome=~/(?i)\/|\(|plano|relatório|estado|ltda|ambiental|reserva|conserva|ameaça|florestal?|técnico|comunidade|[0-9]{4}|[\.,&]/){
            return nome.trim();
        }
        List partes = nome?.toString()?.trim()?.split(' ')
        String resultado = partes.pop().toString().toLowerCase().capitalize().trim() + ', ' // ultimo  nome
        partes.each {
            if ( ! ( it.toLowerCase() ==~ /^d.s?$/ || it.trim() == '')) {
                resultado += it.substring(0, 1).capitalize() + '.'
            }
        }
        return resultado.trim()
    }

    static String nome2autor(String nome ) {
        return nome2citacao(nome )
    }

    /**
     * Abreviar nomes para que o tamanho nao ultrapasse 20 caracteres
     * Exemplo: Joaquim Jose da Silva -> Joaquim J. da Silva
     * @param text
     * @return String
     */

    static String nomeAbreviado(String text = null) {
        Integer maxLengh = 20
        if ( ! text ) {
            return ''
        }
        try {
            text = capitalize(text)
            List listNames = text?.split(' ')
            if (text.length() <= maxLengh) {
                return text;
            }
            listNames.eachWithIndex { name, index ->
                if (index > 0 && index < (listNames.size() - 1)) {
                    if (name.toString().trim() != '' && !(name.toString().toLowerCase() =~ /^d.s?$/)) {
                        String nomeTemp = text.replace(' ' + name, ' ' + name.toString().substring(0, 1) + '.')
                        if (nomeTemp.length() > maxLengh) {
                            text = nomeTemp
                        }
                    }
                }
            }
        } catch (Exception e) {
        };
        return text
    }

    /**
     * Abreviar nomes para que o tamanho nao ultrapasse 20 caracteres
     * Exemplo: Joaquim Jose da Silva -> Joaquim Jose da S.
     * @param text
     * @return String
     */
    static String nomeAbreviadoReverse(String text = null) {
        if (!text) {
            return ''
        }
        List result = []
        text = capitalize(text)
        text?.split(' ')?.each { nome ->

            if (nome.trim() != '') {
                if (result.join(' ').length() < 13 || nome.toLowerCase() ==~ /^d.s?$/) {
                    result.push(nome)
                } else {
                    result.push(nome.substring(0, 1) + '.')
                }
            }
        }
        return result.join(' ')
    }

    /**
     * retornar uma string envolvida entre aspas
     * @param text
     * @param quote
     * @return
     */
    static String quote(String text = '', String quote = '"') {

        try {
            return quote + text.toString().trim().replaceAll(/"/, '') + quote
        }
        catch (Exception e) {
            return text
        }
    }

    static String stripTagsFichaPdf(String texto = '') {
        texto = texto ?: ''
        try {
            // lista de tags permitidas nas justificativas/textos da ficha
            texto = stripTags3(texto, 'span', 'pre', 'sup', 'sub', 'p', 'em', 'i', 'b', 'strong', 'a', 'br')
            texto = texto.replaceAll('<sup>2</sup>', '²').replaceAll('<sup>3</sup>', '³')
            // limpar tags <a> sem href
            if (texto =~ /<a>/) {
                texto = texto.replaceAll(/%(\s|\.|,|\)|;)/, '&percnt;$1');
                try {
                    texto = URLDecoder.decode(texto, StandardCharsets.UTF_8.name())
                } catch( Exception e){
                    println ' '
                    println 'Erro SALVE - Util.stripTagsFichaPdf() na linha 519: texto = URLDecoder.decode(texto, StandardCharsets.UTF_8.name()) '
                    println e.getMessage()
                    println 'Texto recebido:'
                    println texto
                }
            }
            texto = texto.replaceAll(/&percnt;/,'%')
            // remover css font-xxxx
            texto = texto.replaceAll(/ ?font[a-zA-Z0-9-]{0,}: ?[0-9a-zA-Z#!% ",-]{1,}; ?/, '')
            // remover line-xxxx
            texto = texto.replaceAll(/ ?line[a-zA-Z0-9-]{0,}: ?[0-9a-zA-Z#!% ",-]{1,}; ?/, '')
            texto = Jsoup.clean(texto, Whitelist.relaxed()
                .addAttributes('span', 'style')
                .addAttributes('div', 'style'))
            texto = texto.replaceAll(/(?i)<br ?>/, '<br />')
            // remover fundo branco e fonte preta padrão
            texto = texto.replaceAll(/background-color: #ffffff; color: #000000;/, '')
        } catch ( Exception e ){
            println ' '
            println 'Erro Util.stripTagsFichaPdf()'
            println e.getMessage()
            println 'Texto recebido:'
            println texto
            println ' '
            texto = stripTags3( texto )
        }
        return Util.trimEditor(texto)
    }

    static String stripTagsKeepLineFeed(String texto = '') {
        texto = texto ?: ''
        return Jsoup.clean(texto, "", Whitelist.none(), new Document.OutputSettings().prettyPrint(false))
    }

    static String stripTags(String texto = '') {
        texto = texto ?: ''
        return Jsoup.parse( trimEditor(texto )).text().replaceAll(/ /, ' ').trim()
    }

    static String stripTags2(String texto = '') {
        texto = texto ?: ''
        return Util.html2str(Jsoup.clean(texto, Whitelist.none().addTags('i', 'b', 'strong', 'em','br','sub','sup'))).trim()
        // exemplo preservando as quebras de linhas
        //Jsoup.clean( texto, "", Whitelist.none().addTags('b','i') , new Document.OutputSettings().prettyPrint(false) )
    }

    static String stripTagsTraducao(String texto = '') {
        texto = texto ?: ''
        // ajustar as tags com espaço < /em> para </em>
        texto = texto.replaceAll(/< ?(\/[a-z]{1,3})>/,'<$1>')
        texto = Util.html2str(Jsoup.clean(texto, Whitelist.none().addTags('i', 'b', 'br', 'strong', 'em', 'p','sup','sub'))).trim()

        // limpar estilo fontxxx
        texto = texto.replaceAll(/ ?font[a-zA-Z0-9-]{0,}: ?[0-9a-zA-Z#!% "',_-]{1,}; ?/,'')
        return trimEditor(texto)
    }
    /**
     * método para tradução de texto utilizando padrões com expressões regulares
     * @example 1 a 20 -> 1 to 20
     * @param texto
     * @return
     */
    static String traducaoRegex(String txOriginal = '' ){
        String txTraduzido = txOriginal
        if ( txOriginal ) {
            // println ' '
            // println 'REGEX:' + txOriginal
            if( txOriginal =~ /[0-9]{1,}\sa\s[0-9]{1,}/ ){
                txTraduzido = txOriginal.replaceAll(/ a /,' to ')
            }
            // println txTraduzido
        }
        return txTraduzido
    }

    /**
     * remover lista de tagas especificas
     * @example stripTags3( 'textohtml' , 'a' , 'b' , 'div' )
     * @param texto
     * @param tags
     * @return
     */
    static String stripTags3(String texto = '', String... tags) {
        texto = texto ?: ''
        return Util.html2str(Jsoup.clean(texto, Whitelist.none().addTags(tags))).trim()
    }

    /**
     * metodo para colocar o nome cientifico em itálico
     * @param nomeCientifico
     * @return
     */
    static String ncItalico(String nomeCientifico = '', Boolean semAutor = false, Boolean italico = true, String coNivelTaxonomico = '') {
        String resultado = ''
        try {
            nomeCientifico = nomeCientifico.replaceAll(/(?i)(<i>|<\/i>)/, '')
            nomeCientifico = nomeCientifico.replaceAll(/'|`/, '’')
            List parts = nomeCientifico.split(' ')
            String autor = ''
            if (parts.size() == 2) {
                resultado = parts[0] + ' ' + parts[1]
            } else if (parts.size() > 2) {
                if (coNivelTaxonomico == 'ESPECIE') {
                    resultado = parts[0] + ' ' + parts[1]
                    parts.remove(0)
                    parts.remove(0)
                } else if (coNivelTaxonomico == 'SUBESPECIE') {
                    resultado = parts[0] + ' ' + parts[1] + ' ' + parts[2]
                    parts.remove(0)
                    parts.remove(0)
                    parts.remove(0)
                } else {
                    // ex: Adelpha cytherea aea, Lophostoma silvicola d’Orbigny, 1836
                    if (parts[2] && parts[2].size() > 2 && (parts[2] =~ /^[a-z]/) && !(parts[2] =~ /^[a-z]’/) ) {
                        resultado = parts[0] + ' ' + parts[1] + ' ' + parts[2]
                        // remover os 3 primeiros itens do array
                        parts.remove(0)
                        parts.remove(0)
                        parts.remove(0)
                    } else {
                        resultado = parts[0] + ' ' + parts[1]
                        // remover os 2 primeiros itens do array
                        parts.remove(0)
                        parts.remove(0)
                    }
                }
                // o que sobrou no array parts é o autor
                autor = parts.join(' ')
            } else {
                resultado = nomeCientifico
            }
            if (italico) {
                resultado = '<i>' + resultado + '</i>'
            }
            resultado += (semAutor ? '' : '&nbsp;<span>' + autor.trim() + '</span>')
            /*
            for (int i = 1; i <= nomeCientifico.length() - 1; i++) {
                if (resultado == '' && (Character.isUpperCase(nomeCientifico.charAt(i)) || !(nomeCientifico.charAt(i) ==~ /[A-Za-z\s]/))) {
                    resultado = nomeCientifico.substring(0, i).trim()
                    // remover final com até 3 letras
                    //resultado = resultado.replaceAll( / .{1,3}$/,'')
                    if( italico ) {
                        resultado = '<i>'+resultado+'</i>'
                    }
                    resultado += (semAutor ? '':'&nbsp;<span>'+ capitalize( nomeCientifico.substring(i).trim()) + '</span>')
                }
            }*/
        } catch (Exception e) {
        }
        if (!resultado) {
            if (italico) {
                resultado = nomeCientifico ? '<i>' + nomeCientifico + '</i>' : ''
            } else {
                resultado = nomeCientifico ?: ''
            }
        }
        return resultado
    }

    /**
     * Padronizar o nome colocando as primeiras letras em maiúlcula
     * @param nome
     * @return
     */
    static String capitalize(String nome = '') {
        return nome.toString().replaceAll(/\s\s/, ' ').split(' ').collect {

            if (it.toUpperCase() =~ /^(?=[MDCLXVI])M*(C[MD]|D?C*)(X[CL]|L?X*)(I[XV]|V?I*)$/) {
                it.toUpperCase()
            } else if (it.toLowerCase() =~ /^(ou|as|em)$/) {
                it.toLowerCase()
            } else if (it.toUpperCase() =~ /^("|\()?(RE|EW|CR|VU|EN|NT|DD|NA|NE|EX|LC)(\)|,|")?$/) {
                it.toUpperCase()
            } else if (it.toLowerCase() =~ /^[aeiou]$/) {
                it.toLowerCase()
            } else if (it.toLowerCase() =~ /^[a-z]\.$/) {
                it.toUpperCase()
            } else if (it.toLowerCase() =~ /^d(a|e|o)s?$/) {
                it.toLowerCase()
            } else if (it.toLowerCase() =~ /^d(f|t)$/) {
                it.toUpperCase()
            } else if (it.toLowerCase() =~ /^parna|rppn|esec|apa$/) {
                it.toUpperCase()
            } else {
                if (it.indexOf('/') > 0) {
                    List listTemp = []
                    it.split('/').each {
                        listTemp.push(capitalize(it))
                    }
                    listTemp.join('/')
                } else if (it.indexOf('(') == 0) {
                    '(' + it.toLowerCase().substring(1).capitalize()
                } else {

                    it.toLowerCase().capitalize()
                }
            }
        }.join(' ')
    }

    /**
     * método para limpar as tags html, converte acentos para RTF e presenva o negrito e italico do html
     * @param texto
     * @return
     */
    static String escape2Rtf(String texto) {

        texto = texto.replaceAll("</p>", "#NEW_LINE#")
        texto = texto.replaceAll("<p.*?>", "")

        // remover caracteres especiais html ( &acute; etc...)
        texto = html2str(texto)

        // limpar tags do word textos antigos deixando somente italico e negrito
        texto = Jsoup.clean(texto, Whitelist.none().addTags('i', 'b', 'strong', 'em'))

        // presevar italico e negrito
        List dePara = []
        dePara.push([regex: ~/<i>|<em>/, rtfCode: '{\\\\i1 '])
        dePara.push([regex: ~/<\/i>|<\/em>/, rtfCode: '}'])
        dePara.push([regex: ~/<strong>|<b>/, rtfCode: '{\\\\b1 '])
        dePara.push([regex: ~/<\/strong>|<\/b>/, rtfCode: '}'])
        dePara.push([regex: ~/<\/strong>|<\/b>/, rtfCode: '}'])
        dePara.push([regex: ~/#NEW_LINE#/, rtfCode: '{\\\\par}'])
        dePara.each {
            texto = texto.replaceAll(it.regex, md5(it.regex.toString()))
        }

        // limpar todas as tags html
        texto = stripTags(trimEditor(texto))

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            new RtfDocument().filterSpecialChar(baos, texto, false, false);
        } catch (IOException e) {
            // will never happen for ByteArrayOutputStream
        }
        texto = new String(baos.toByteArray())
        dePara.each {
            texto = texto.replaceAll(md5(it.regex.toString()), it.rtfCode)
        }
        texto = "{\\ " + texto.trim() + '}' // tem que adicionar o espaco na frente para não cortar palavras em alguns textos
        return texto
    }

    /**
     * exibir o nome cientifico em itálico e adicionar link para pesquisar no portalbio
     * @example: Puma concolor (PortalBio)
     * @param taxon
     * @return
     */
    static String linkSearchPortalbio(String taxon = '') {
        if (taxon) {
            String taxonSearch = ncItalico(taxon, true, false).replaceAll(/\s/, '%20')
            taxon = '<i>' + taxon + '</i>&nbsp;&nbsp;<a title="Pesquisar no Portal da Biodiversidade" target="_blank" href="https://portaldabiodiversidade.icmbio.gov.br/portal/search?text=' + taxonSearch + '">(PortalBio)</a>'
        }
        return '<i>' + taxon + '</i>'
    }

    /**
     * retornar os valores Sim, Não , Desconhecido ou Excluído
     * @param value
     * @return
     */
    static String snd(String value) {
        switch (value) {
            case 'S': return 'Sim'; break
            case 'N': return 'Não'; break
            case 'D': return 'Desconhecido'; break
            case 'E': return 'Excluído'; break
            default:
                return ''
        }
    }
    /**
     * salvar Objeto em disco
     * @param content
     * @param filePath
     */
    static void saveHDD(Object content, String filePath) {
        new File(filePath).write(new JsonBuilder(content).toPrettyString())
    }

    /**
     * ler objeto salvo do disco
     * @param filePath
     * @return
     */
    static Object loadHDD(String filePath) {
        try {
            return new JsonSlurper().parseText(new File(filePath).text)
        } catch (Exception e) {
            return null
        }

    }

    static String removeBgWhite(String texto = '') {
        texto = texto ?: ''
        texto = texto.replaceAll(/color: ?black;/, '')
        texto = texto.replaceAll(/color: ?#000000;/, '')
        texto = texto.replaceAll(/color: ?#000;/, '')
        return texto.replaceAll(/background-color: ?#ffffff;/, 'background-color: transparent;')
    }

    /**
     * Método para adicionar assinatura eletrônica em um pdf.
     * Retorna o nome do arquivo assinado ou '' se não conseguir aplicar a assinatura
     * @param fileName - nome do arquivo pdf que será assinado
     * @param assinatura - texto com as informações da assinatura
     * @param qrCode - (opcional) - texto com as informações para gerar o qr code no documento
     * @params unidadeResponsavel - nome da unidade para impressão do cabeçalho quando houver quebra de página
     * @return String fileName or ''
     */
    static Map assinarPdf(String pdfFileName = '', String assinatura = '', String qrCode = '', String salveDir = '', String unidadeResponsavel = '') {
        // utilizar channel para restringir acesso a um usuário por vez ao documento de assinatura
        File flagFile = new File('/data/salve-estadual/anexos/flag_file.lock')
        FileChannel channel = new RandomAccessFile(flagFile, "rw").getChannel();
        FileLock lock
        Map res = [pdfFileName: '', error: true, msg: '']
        try {
            try {
                // 1ª tentativa de assinar
                lock = channel.tryLock()
            } catch ( OverlappingFileLockException e1 ) {
                // 2ª tentativa de assinar
                try {
                    sleep(Math.abs(new Random().nextInt() % 4000) + 1000)
                    lock = channel.tryLock()
                } catch ( OverlappingFileLockException e2) {
                    // 3ª tentativa de assinar
                    sleep(Math.abs(new Random().nextInt() % 8000) + 1000)
                    lock = channel.tryLock()
                }
            }
            flagFile.write('Assinatura eletronica em andamento ' + new Date().format('dd/MM/yyyy HH:mm:ss') + '\n' + assinatura)
            Image image
            Rectangle pdfPageSize
            float pdfLeftMargin = 36f
            float pdfRightMargin = 36f
            float pdfPageWidth
            Rectangle rectangle
            Paragraph paragrafo
            PdfReader pdfReader
            PdfStamper pdfStamper
            String pdfOutputFileName
            // criar a fonte Times New Roman para o texto da assinatura
            BaseFont pdfBaseFont = BaseFont.createFont(BaseFont.TIMES_ROMAN, "Cp1252", false);
            // criar as fontes utilzadas
            Font fontAssinatura = new Font(pdfBaseFont, 10, Font.BOLDITALIC, new BaseColor(150, 150, 150))
            // fonte cinza claro para texto do rodapé
            Font fontFooter = new Font(pdfBaseFont, 9.5, Font.NORMAL, BaseColor.DARK_GRAY)
            // fonte para o cabeçalho
            Font fontCabecalho = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD)

            // fonte texto continuação assinaturas
            Font fontContinuacao = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.ITALIC, BaseColor.LIGHT_GRAY)

            // se não for informado o diretorio temporário, assumir o padrão
            if (!salveDir) {
                salveDir = '/data/salve'
            }

            // altura padrão da página
            float pdfHeight = 816.0 // ver como pegar este 816 dinamicamente do itext

            // verificar se o arquivo existe

            try {
                // validar os parâmeetros
                if (!pdfFileName || !assinatura) {
                    throw new Exception('Parâmetros insuficientes, necessário informar o nome do arquivo pdf e o texto da assinatura.')
                }

                // verificar se o arquivo tem a extensão .pdf
                if (!Util.getFileExtension(pdfFileName) == 'pdf') {
                    throw new Exception('Arquivo não possui extensção .pdf')
                }

                // verificar se o arquivo existe no disco
                File pdfFile = new File(pdfFileName)
                if (!pdfFile.exists()) {
                    throw new Exception('Arquivo ' + pdfFileName + ' não existe.')
                }

                // criar flag de acesso unico
                /*flagFile = new File(pdfFileName + '.lock') // criar arquivo em branco para flag de que uma assinatura está em andamento
                try {
                    if (flagFile.exists()) {
                        // interromper de 1 a 5 segundos e tentar novamente
                        sleep(Math.abs(new Random().nextInt() % 4000) + 1000)
                        if (flagFile.exists()) {
                            // esperar mais um tempo
                            sleep(Math.abs(new Random().nextInt() % 8000) + 1000)
                            if (flagFile.exists()) {
                                throw new Exception('Arquivo já está sendo assinado. Tente novamente em instantes.')
                            }
                        }
                    }
                } catch (Exception e) {
                    throw new Exception('Erro arquivo de controle assinatura eletrônica.')
                    return
                }
                // caso 2 consigam passar no mesmo segundo, somente 1 vai conseguir escrever no arquivo
                try {
                    // gravar alguma coisa no arquivo temporario para ele ser criado no disco
                    flagFile.write('Assinatura eletronica em andamento ' + new Date().format('dd/MM/yyyy HH:mm:ss') + '\n' + assinatura)
                } catch (e) {
                    throw new Exception('Arquivo já está sendo assinado. Tente novamente em instantes.')
                }
                */

                // criar uma copia do documento original antes das assinaturas
                String pdfBackupName = pdfFileName + '.bkp'
                if (!new File(pdfBackupName).exists()) {
                    new File(pdfBackupName).bytes = pdfFile.bytes
                }

                // cópia temporária do arquivo que será assinado ( stamper )
                pdfOutputFileName = pdfFileName + '.ass.pdf'

                // encontrar a posição vertical (y) da última linha de texto no arquivo pdf
                float pdfPosYLastWord = new GetLinesFromPDF().getPosY(pdfFileName)
                if (pdfPosYLastWord < 1) {
                    throw new Exception('Arquivo sem conteúdo!')
                }
                // posição onde será impressa a assinatura
                float pdfCurrentPosY = (pdfHeight - pdfPosYLastWord - 40f).toFloat()

                // carregar o arquivo original
                pdfReader = new PdfReader(new RandomAccessFileOrArray(pdfFileName), null)

                // ler a quantidade de páginas
                int pdfNumPages = pdfReader.getNumberOfPages()
                if (pdfNumPages < 1) {
                    throw new Exception('Arquivo em branco!')
                }

                // ler o tamanho da pagina 1 para ser utilizado ao adicionar novas paginas se for necessário
                pdfPageSize = pdfReader.getPageSize(1)
                pdfPageWidth = pdfPageSize.getWidth() - pdfRightMargin

                // criar o objeto pdfStamper
                pdfStamper = new PdfStamper(pdfReader, new FileOutputStream(pdfOutputFileName), pdfReader.getPdfVersion(), true)

                /*
            // adicionar o qr code se foi informado
            if( qrCode )
            {
                String textoQrCode = 'Visualizar a cópia eletrônica deste documento no formato PDF.'
                // url do site com exemplo : https://www.youtube.com/watch?v=uzF8E4h7Evo
                BarcodeQRCode bc = new BarcodeQRCode(qrCode,(int)50,(int)50,[:])
                image = bc.getImage()
                // canto superior direito
                image.setAbsolutePosition((pdfPageWidth-50).toFloat(), (pdfHeight-36).toFloat())
                // cando superior esquerdo
                //image.setAbsolutePosition(36f, 735f)
                // canto inferior esquerdo
                //image.setAbsolutePosition(36f, 10f)

                // adicioanar o qr code na primeira página
                PdfContentByte pdfContentByte = pdfStamper.getOverContent( (int)1 )
                pdfContentByte.addImage( image )
            }
             */

                // ler o conteúdo binario da última página para adicionar mais conteúdo
                PdfContentByte pdfContentByte = pdfStamper.getOverContent(pdfNumPages)

                // definir o tamanho da fonte  para o texto da assinatura no objeto binario
                pdfContentByte.setFontAndSize(pdfBaseFont, 14)

                String imageFileName = salveDir + '/arquivos/salve-assinatura-eletronica.png'
                image = null
                if (!qrCode) {
                    // ler a imagem do carimbo da assinatura eletrôncia do SALVE
                    if (new File(imageFileName).exists()) {
                        image = Image.getInstance(imageFileName)
                        if (!image) {
                            throw new Exception('Imagem para a assinatura eletrônica localizada em ' + imageFileName + ' está inválida!')
                        }
                        // redimensionar a imagem para o tamanho máximo permitido
                        image.scaleAbsolute(82, 47)
                    } else {
                        throw new Exception('Imagem para a assinatura ' + imageFileName + ' inexistente!')
                    }
                } else {
                    // url do site com exemplo : https://www.youtube.com/watch?v=uzF8E4h7Evo
                    BarcodeQRCode bc = new BarcodeQRCode(qrCode, (int) 100, (int) 100, [:])
                    image = bc.getImage()
                    image.scaleAbsolute(77, 77)
                }

                // Usar o objeto simple column box para criar texto com quebra de linha
                ColumnText columnText = new ColumnText(pdfContentByte)

                // verificar se a assinatura cabe na página atual e se não couber adicionar nova página
                if (pdfCurrentPosY < 60f) {
                    // incrementar o número da página
                    pdfNumPages++

                    // inserir nova página
                    pdfStamper.insertPage(pdfNumPages, pdfPageSize)

                    // ler o conteúdo binario da nova pagina
                    pdfContentByte = pdfStamper.getOverContent(pdfNumPages)

                    // posicionar no inicio da pagina
                    pdfCurrentPosY = pdfReader.getPageSize(pdfNumPages).getHeight() - 72

                    // não imprimir o cabecalho
                    if (false) {
                        pdfCurrentPosY = pdfReader.getPageSize(pdfNumPages).getHeight() - 190
                        // adicionar cabecalho na pagina
                        //(pdfPageSize.getLeft() + pdfPageSize.getRight()) / 2,
                        //pdfPageSize.getTop()-20,0);
                        float headerPositionX = pdfLeftMargin
                        float headerPositionY = (pdfHeight - 56.2).toFloat()
                        // brasão
                        try {
                            Image img = Image.getInstance("/data/salve-estadual/arquivos/brasao.png")
                            if (img) {
                                //25,22,17,15
                                float x = (pdfPageSize.getRight() - (pdfRightMargin + 15)) / 2
                                img.setAbsolutePosition((x).toFloat(), (headerPositionY - 4).toFloat())
                                pdfContentByte.addImage(img)
                            }
                        } catch (Exception e) {
                        }
                        PdfPTable table = new PdfPTable(1)
                        table.setTotalWidth((pdfPageWidth - pdfRightMargin).toFloat())
                        table.setLockedWidth(true)
                        paragrafo = new Paragraph(15f, 'MINISTÉRIO DO MEIO AMBIENTE\n' +
                            'INSTITUTO CHICO MENDES DE CONSERVAÇÃO DA BIODIVERSIDADE\n' +
                            'DIRETORIA DE PESQUISA, AVALIAÇÃO E MONITORAMENTO DA BIODIVERSIDADE' +
                            (unidadeResponsavel ? '\n' + unidadeResponsavel.toUpperCase() : ''), fontCabecalho)
                        paragrafo.setAlignment(Element.ALIGN_CENTER)
                        PdfPCell cell = new PdfPCell()
                        cell.addElement(paragrafo)
                        cell.setBorder(Rectangle.NO_BORDER)
                        table.addCell(cell)
                        table.writeSelectedRows(0, -1, headerPositionX, headerPositionY, pdfContentByte)
                        // fim do cabecalho

                    }

                    // texto continuação
                    if (true) {
                        rectangle = new Rectangle((pdfLeftMargin + 2).toFloat(), (pdfCurrentPosY + 2).toFloat(), (pdfPageWidth - 4).toFloat(), (pdfCurrentPosY + 52).toFloat())
                        paragrafo = new Paragraph('continuação assinaturas...', fontContinuacao)
                        paragrafo.setAlignment(Element.ALIGN_RIGHT)
                        columnText = new ColumnText(pdfContentByte)
                        columnText.setSimpleColumn(rectangle)
                        columnText.setText(paragrafo)
                        columnText.setAlignment(Element.ALIGN_RIGHT)
                        columnText.go()
                        pdfCurrentPosY -= 20
                    }

                    // adicionar numero de página e a hora no rodapé
                    rectangle = new Rectangle(pdfLeftMargin, 0, pdfPageWidth, pdfRightMargin)
                    paragrafo = new Paragraph("Página: " + pdfNumPages + '        Em: ' + (new Date().format('dd/MM/yyyy HH:mm:ss')), fontFooter)
                    paragrafo.setAlignment(Element.ALIGN_CENTER)
                    columnText = new ColumnText(pdfContentByte)
                    columnText.setSimpleColumn(rectangle)
                    columnText.setText(paragrafo)
                    columnText.setAlignment(Element.ALIGN_CENTER);
                    columnText.go()
                }

                // rectangle = new Rectangle(x, top, w, (top+50).toFloat() )
                rectangle = new Rectangle((pdfLeftMargin + 2).toFloat(), (pdfCurrentPosY + 2).toFloat(), (pdfPageWidth - 4).toFloat(), (pdfCurrentPosY + 52).toFloat())
                columnText.setAlignment(Element.ALIGN_JUSTIFIED)

                // o texto do paragrafo deve possuir no mínimo 250 caracteres caso contrário adicioanr
                // espaços e o caratere chr(255) no final para cálculo da proxima posição de impressão
                if (assinatura.length() < 249) {
                    assinatura = assinatura.padRight(250, ' ') + ' ' // chr(255)
                }
                paragrafo = new Paragraph(assinatura.toString(), fontAssinatura)
                columnText.setSimpleColumn(125f, (pdfCurrentPosY + 2).toFloat(), (pdfPageWidth - 4).toFloat(), (pdfCurrentPosY + 52).toFloat())
                columnText.setText(paragrafo)
                columnText.go()

                // desenhar a image com o cadeado
                float diferenca = 0f
                if (qrCode) {
                    diferenca = 15f
                }

                if (image) {
                    image.setAbsolutePosition((pdfLeftMargin + 2).toFloat(), (pdfCurrentPosY - diferenca).toFloat())
                    pdfContentByte.addImage(image)
                }

                // desenhar borda com preenchimento
                pdfContentByte.saveState()
                PdfGState state = new PdfGState()
                state.setFillOpacity(0.1f)
                pdfContentByte.setGState(state)
                pdfContentByte.setRGBColorFill(255, 255, 255)
                pdfContentByte.setColorStroke(BaseColor.LIGHT_GRAY)
                pdfContentByte.setLineWidth(1f)
                pdfContentByte.rectangle((pdfLeftMargin - 2).toFloat(), (pdfCurrentPosY - 2).toFloat(), (pdfPageWidth - 34).toFloat(), (rectangle.getHeight() + 2).toFloat());
                pdfContentByte.fillStroke()
                pdfContentByte.restoreState()
                pdfStamper.close()
                pdfReader.close()

                // excluir o arquivo original e renomear o arquivo assinado para o nome do arquivo original
                pdfFile.delete()
                new File(pdfOutputFileName).renameTo(pdfFileName)
                try {
                    flagFile.delete()
                } catch (Exception e) {
                }

                // incrementar posição para adicionar a próxima assinatura
                //pdfCurrentPosY -= (rectangle.getHeight()+8f).toFloat()

                res.error = false
                res.msg = 'Arquivo assinado com SUCESSO às ' + new Date().format('dd/MM/yyyy HH:mm:ss') + '!'
                res.fileName = pdfOutputFileName


            } catch (Exception e) {
                try {
                    flagFile.delete()
                } catch (Exception err) {
                }
                res.msg = e.getMessage()
                try {
                    if (pdfStamper) {
                        pdfStamper.close()
                    }
                } catch (Exception err) {
                }

                try {
                    if (pdfReader) {
                        pdfReader.close()
                    }
                } catch (Exception err) {
                }
            }
        } catch ( OverlappingFileLockException e ) {
            res.msg     = 'Documento já está sendo assinado. Tente novamente em alguns segundos.'
        }
        if( lock != null ) {
            lock.release()
        }
        channel.close();
        return res
    }

    static String randomString(Integer lenght = 6) {
        def generator = { String alphabet, int n ->
            new Random().with {
                (1..n).collect { alphabet[nextInt(alphabet.length())] }.join()
            }
        }
        generator((('A'..'Z') + ('0'..'9')).join(), lenght)
    }

    static String pesoEfeito(Integer nuPeso = 0) {
        if (nuPeso == 1)
            return 'Secundário (1)'
        else if (nuPeso == 2)
            return 'Primário (2)'
        else return ''
    }

    static String encryptStr(String value = '', String secretKey = '') {
        return AESCryption.encrypt(value, secretKey)
    }

    static String decryptStr(String value = '', String secretKey = '') {
        return AESCryption.decrypt(value, secretKey)
    }

    static String getFirstName(String name = '', int maxSize = 20) {
        try {
            if (name.length() <= maxSize) {
                return name
            }
            List parts = []
            Integer charsLeft = maxSize
            name.split(' ').each {
                charsLeft -= it.length()
                if (charsLeft > 0) {
                    parts.push(it)
                } else {
                    if (!parts) {
                        parts.push(it.substring(0, maxSize))
                    }
                }
            }
            return parts.join(' ').replaceAll(/ [a-zA-Z]{2}$/, '')
        } catch (Exception e) {
            return name
        }
    }


    /*
        static String encryptStr(String str='', String key='de32wssw2edffhr56') {
            try {
                // Encode the string into bytes using utf-8
                byte[] utf8 = str.getBytes("UTF8");
                // Encrypt
                SecretKeySpec skeyspec = new SecretKeySpec(key.getBytes(), "Blowfish");
                Cipher ecipher = Cipher.getInstance("Blowfish");
                ecipher.init(Cipher.ENCRYPT_MODE, skeyspec);
                byte[] enc = ecipher.doFinal(utf8);
                // Encode bytes to base64 to get a string
                return new sun.misc.BASE64Encoder().encode(enc);
            } catch (javax.crypto.BadPaddingException e) {
            } catch (IllegalBlockSizeException e) {
            } catch (UnsupportedEncodingException e) {
            } catch (java.io.IOException e) {
            }
            return null;
        }

        static String decryptStr(String str ='',String key='de32wssw2edffhr56') {
            try {
                // Decode base64 to get bytes
                byte[] dec = new sun.misc.BASE64Decoder().decodeBuffer(str);
                // Decrypt
                SecretKeySpec skeyspec = new SecretKeySpec(key.getBytes(), "Blowfish");
                Cipher dcipher = Cipher.getInstance("Blowfish");
                dcipher.init( Cipher.DECRYPT_MODE, skeyspec);
                byte[] utf8 = dcipher.doFinal(dec);
                // Decode using utf-8
                return new String(utf8, "UTF8");
            } catch (javax.crypto.BadPaddingException e) {
            } catch (IllegalBlockSizeException e) {
            } catch (UnsupportedEncodingException e) {
            } catch (java.io.IOException e) {
            }
            return null;
        }

         */

    /**
     * Identiricar o ip do usuário
     * @return
     */
    static String getRequestIp(javax.servlet.http.HttpServletRequestWrapper request) {
        String ipAddress = ''
        try {
            if (request) {
                ipAddress = request.getHeader("Client-IP")
                if (!ipAddress) {
                    ipAddress = request.getHeader("X-Forwarded-For")
                } else if (!ipAddress) {
                    ipAddress = request.remoteAddr
                } else if (!ipAddress) {
                    ipAddress = request.getRemoteAddr();
                }
            }
            if (!ipAddress) {
                ipAddress = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getRemoteAddr();
            }
            ipAddress = (ipAddress == '0:0:0:0:0:0:0:1') ? '127.0.0.1' : ipAddress
            ipAddress = (ipAddress == 'localhost' ? '127.0.0.1' : ipAddress)
            return ipAddress
        } catch (Exception e) {
            return '0'
        }
    }

    static String formatarComunicacaoPessoal( String noAutor='', Integer nuAno=0, Boolean addComPess = true){
        if( noAutor ) {
            noAutor = noAutor.trim()
            String comPessSemParentesis  = Util.comPessText.replaceAll(/\(|\)/,'')
            String comPess = noAutor + (addComPess ? ', ' + comPessSemParentesis : '') + (nuAno ? ', ' + nuAno.toString() : '')
            // colocar a virgula antes do ano. Ex: Cipola et al., 2017 e adicoinar (com.pess)
            return Util.nome2citacao(comPess.replaceAll(/\.(\s[0-9]{4})$/, '.,$1'))
        }
        return ''
    }

    static String formatRefBib(List refBibs = [], boolean html = false ) {
        List nomes = [];
        if( refBibs ) {
            refBibs.each {
                if ( it.publicacao ) {
                    String citacao = it.publicacao.citacao
                    if( citacao.trim() != '' ) {
                        if (!html) {
                            nomes.push(citacao)
                        } else {
                            nomes.push('<li><i class="fa fa-commenting green" title="'+it.publicacao.deTitulo+'"></i>&nbsp;' + citacao + '</li>' )
                        }
                    }
                } else {
                    String comPess = Util.formatarComunicacaoPessoal( it.noAutor, it.nuAno, false )
                    if ( ! nomes.find { item -> item.indexOf( comPess ) > -1 } ) {
                        comPess += ' ' + comPessText
                        if (!html) {
                            nomes.push(comPess)
                        } else {
                            nomes.push('<li><i class="fa fa-user green" title="Comunicação pessoal"></i>&nbsp;' + comPess + '</li>')
                        }
                    }
                }
            }
        }
        if( html ) {
            return '<ul class="ul-ref-bibs text-nowrap">' + nomes.unique().sort().join('') + '</ul>'
        }
        return nomes.unique().sort().join( ';\n' )
    }

    /**
     * Recebe as referencias bibliográficas do banco de dados no formato json
     * e retorna no formato de exibição nos grides
     * @param json_ref_bib = '{ "643187" : {"de_titulo" : "Guia dos Sphingidae da Serra dos Órgãos, Sudeste do Brasil.", "no_autor" : "Martin, A.\nSoares, A.\nBizarro, J.", "nu_ano" : 2012} }'
     * @return <ul><li>ref</li></ul>
     */
    static String parseRefBib2Grid( List refBibs , Boolean gridPdf = false) {
        // trasnformar o array em json no formato retornado pelo postgres PGobject
        List itens = []
        refBibs.eachWithIndex{item,index ->
            if( item.nu_ano && item.nu_ano.toString() == '9999' ) {
                item.nu_ano = 's.d.'
            }
            Map mapTemp = ['de_titulo':item.de_titulo
                           ,'no_autor':item.no_autor
                           ,'nu_ano': item.nu_ano
                          ]
            itens.push('"' + index.toString() + '" : ' + ( mapTemp as JSON ).toString() )
        }
        return parseRefBib2Grid( '{'+itens.join(',') + '}' ,gridPdf)
    }
    static String parseRefBib2Grid( org.postgresql.util.PGobject jsonRefBibs, Boolean gridPdf = false ) {
        return parseRefBib2Grid( jsonRefBibs.toString(), gridPdf )
    }
    static String parseRefBib2Grid( String jsonRefBibs = '', Boolean gridPdf = false) {
        List refBibs = []
        String html = ''
        try {
            if ( jsonRefBibs ) {
                def dadosJson = JSON.parse(jsonRefBibs)

                if( dadosJson instanceof JSONArray ) {
                    try {
                        dadosJson.each { value ->
                            String autor = ''
                            // publicacao tem de_titulo
                            if( value.de_titulo ) {
                                if (value.no_autor && value.nu_ano && value.de_titulo) {
                                    autor = getCitacao(value.no_autor, value.nu_ano) ?: value.de_titulo
                                    if ( ! gridPdf ) {
                                        autor = '<li><i class="fa fa-commenting green" style="cursor: pointer;" title="'+
                                            value.de_titulo+'"></i>&nbsp;' +
                                            autor + '</li>'
                                    }
                                }
                            } else {
                                // comunicação pessoal
                                autor = formatarComunicacaoPessoal(value.no_autor, value.nu_ano)
                                if ( ! gridPdf) {
                                    autor = '<li><i class="fa fa-user green" style="cursor: pointer;" title="Comunicação pessoal"></i>&nbsp;'+autor+'</li>'
                                }
                            }
                            if (autor && ! refBibs.contains( autor ) ) {
                                refBibs.push(autor)
                            }
                        }
                        if (!gridPdf) {
                            html = '<ul class="ul-none">' + refBibs.join('') + '</ul>'
                        } else {
                            html = refBibs.join('#NL#')
                        }
                    } catch (Exception e) {}
                    return html
                } else {
                    try {
                        dadosJson = dadosJson.sort {
                            it?.value?.no_autor + it?.value?.nu_ano.toString()
                        }
                    } catch (Exception e) {
                    }
                    dadosJson.each { key, value ->
                        String citacao = ''
                        // se tiver o titulo é uma publicação senão é uma comunicação pessoal
                        if (value.de_titulo) {
                            value.de_titulo = value.de_titulo.replaceAll(/"/, '&quot;');
                            citacao = (value.no_autor ? Util.getCitacao(value.no_autor, value.nu_ano) : value.de_titulo)
                            if (!gridPdf) {
                                citacao = '<li><i class="fa fa-commenting green" style="cursor: pointer;" title="' + (value.de_titulo ?: '') + '"></i>&nbsp;' +
                                    citacao + '</li>'

                            }
                        } else if (value.no_autor) {
                            citacao = Util.formatarComunicacaoPessoal(value.no_autor, value.nu_ano)
                            if (!gridPdf) {
                                citacao = '<li><i class="fa fa-user green" style="cursor: pointer;" title="Comunicação pessoal"></i>&nbsp;' +
                                    citacao + '</li>'
                            }
                        }

                        if (citacao && !refBibs.contains(citacao)) {
                            refBibs.push(citacao)
                        }
                    }
                }
                if( ! gridPdf ) {
                    html = '<ul class="ul-none">' + refBibs.join('') + '</ul>'
                } else {
                    html = refBibs.join('#NL#')
                }
            }
        } catch( Exception e ) {}
        return html
    }

    /**
     * Compactar o arquivo no formato ZIP
     * @param fileName
     * @return
     */
    static Map zipFile(String fileName = '' ) {
        Map resultado = [zipFileName: '', errors: []]
        try {
            File fileOrigem = new File( fileName )
            if( ! fileOrigem.exists() ){
                throw  new Exception('Arquivo '+fileName+' não existe')
            }
            resultado.zipFileName = fileName.replaceFirst(~/\.[^\.]+$/, '') + '.zip'
            byte[] buffer = new byte[1024]
            FileOutputStream fos = new FileOutputStream(resultado.zipFileName)
            ZipOutputStream zos = new ZipOutputStream(fos)
            String entryName = fileOrigem.getName()
            ZipEntry ze = new ZipEntry(entryName)
            zos.putNextEntry(ze)
            FileInputStream fis = new FileInputStream(fileName)
            int len
            while ((len = fis.read(buffer)) > 0) {
                zos.write(buffer, 0, len)
            }
            fis.close()
            zos.closeEntry()
            zos.close()
        } catch (Exception e) {
            resultado.errors.push(e.getMessage())
            resultado.zipFileName=''
        }
        return resultado
    }

    /**
     * Método para retornar as informações da referência bibliográfica
     * formatado de acordo com o tipo da referência
     * @param rowPublicacao
     * @return
     */
    static String getReferenciaHtml( Publicacao publicacao, String tag='') {
        if (!publicacao) {
            return ''
        }
        // campos necessários para gerar a referencia bibliografica
        Map rowPublicacao = [:]
        rowPublicacao.no_autor = publicacao.noAutor
        rowPublicacao.de_titulo = publicacao.deTitulo
        rowPublicacao.dt_publicacao = publicacao.dtPublicacao
        rowPublicacao.co_tipo_publicacao = publicacao?.tipoPublicacao?.coTipoPublicacao
        rowPublicacao.de_editores = publicacao.deEditores
        rowPublicacao.de_issue = publicacao.deIssue
        rowPublicacao.de_paginas = publicacao.dePaginas
        rowPublicacao.de_titulo = publicacao.deTitulo
        rowPublicacao.de_titulo_livro = publicacao.deTituloLivro
        rowPublicacao.de_url = publicacao.deUrl
        rowPublicacao.de_volume = publicacao.deVolume
        rowPublicacao.dt_acesso_url = publicacao.dtAcessoUrl
        rowPublicacao.dt_publicacao = publicacao.dtPublicacao
        rowPublicacao.no_autor = publicacao.noAutor
        rowPublicacao.no_cidade = publicacao.noCidade
        rowPublicacao.no_editora = publicacao.noEditora
        rowPublicacao.no_revista_cientifica = publicacao.noRevistaCientifica
        rowPublicacao.no_universidade = publicacao.noUniversidade
        rowPublicacao.nu_ano = publicacao.nuAnoPublicacao
        rowPublicacao.nu_ano_publicacao = publicacao.nuAnoPublicacao
        rowPublicacao.nu_edicao_livro = publicacao.nuEdicaoLivro
       return getReferenciaHtml(rowPublicacao, tag)
    }

    static String getReferenciaHtml(Map rowPublicacao = [:], String tag='') {
        if( !rowPublicacao ){
            return null
        }
        // transformar as chaves no formato snakeCase
        Map tempMap = [:]
        rowPublicacao.each{ key, value ->
            // verificar se esta iniciando no formato camelCase aaBccc
            if( ( key =~ /^[a-z]{2}[A-Z]{1}/) ) {
               tempMap[toSnakeCase(key)] = value
            } else{
                tempMap[key] = value
            }
        }
        rowPublicacao = tempMap
        String negritoOpen = '<b>';
        String negritoClose = '</b>';
        String titulo = Util.stripTags3( rowPublicacao.de_titulo.toString(), 'a')
        if ( !rowPublicacao.no_autor && ! (rowPublicacao.co_tipo_publicacao =~ /SITE/) ) {
            return (titulo ? negritoOpen + ' ' + titulo + negritoClose : '')
        }

        // remover espaços duplos, quebras e converter para ponto e virgula as quebras

        String autorAno = ''
        if( rowPublicacao.no_autor ) {
            String noAutores
            noAutores = rowPublicacao.no_autor.toString().trim().replaceAll(/\n\r?/, '|');
            noAutores = noAutores.replaceAll(/\s{2,}/, ' ');
            noAutores = noAutores.replaceAll(/\s+&\s+/, ';');
            noAutores = noAutores.replaceAll(/\s?\|/, ';');
            List autores = noAutores.trim().replaceAll(/\s+;\s+|\s+&\s+|\n|\r/, '; ').split(';');
            String lastAutor
            if (autores.size() > 1) {
                autorAno = autores.init().join('; ')
                lastAutor = autores.last()
                autorAno = autorAno.toString().trim() + ' &amp; ' + lastAutor.toString().trim()
            } else {
                autorAno = autores.join('; ');
            }
        }
        if (rowPublicacao.nu_ano_publicacao) {
            if( rowPublicacao.nu_ano_publicacao && rowPublicacao.nu_ano_publicacao.toString() == '9999') {
                rowPublicacao.nu_ano_publicacao = 's.d.'
            }
            autorAno += ' ' + ( rowPublicacao.nu_ano_publicacao + (tag?:'')).trim() + '.'
        } else if (rowPublicacao.dt_publicacao) {
            autorAno += ' ' + ( rowPublicacao.dt_publicacao.format('dd/MM/yyyy') + (tag?:'')).trim()+ '.'
        }
        autorAno = autorAno.trim()

        String url = '<a target="_blank" href="' + rowPublicacao.de_url + '">' + rowPublicacao.de_url + '</a>.'

        switch (rowPublicacao.co_tipo_publicacao) {
            case 'RESUMO_CONGRESSO':
                return (autorAno +
                    (rowPublicacao.de_titulo ? ' ' + rowPublicacao.de_titulo + '.' : '') +
                    (rowPublicacao.no_revista_cientifica ? ' <b>' + rowPublicacao.no_revista_cientifica + '</b>,' : '') +
                    (rowPublicacao.de_volume ? ' ' + rowPublicacao.de_volume : '') +
                    (rowPublicacao.de_issue ? ' (' + rowPublicacao.de_issue + ')' : '') + (rowPublicacao.de_volume ? ':' : '') +
                    (rowPublicacao.de_paginas ? ' p.' + rowPublicacao.de_paginas + '.' : '') +
                    (rowPublicacao.de_editores ? ' <i>In</i>: ' + getNomesEtAll(rowPublicacao.de_editores) + '.' : '') +
                    (rowPublicacao.de_titulo_livro ? '<i>In</i>: <b>' + rowPublicacao.de_titulo_livro + '</b>.' : '') +
                    (rowPublicacao.no_universidade ? rowPublicacao.no_universidade + '.' : '') +
                    (rowPublicacao.no_cidade ? ' ' + rowPublicacao.no_cidade + '.' : '') +
                    (rowPublicacao.de_url ? ' Disponível em: ' + url : '') +
                    (rowPublicacao.dt_acesso_url ? ' Acessado em: ' + rowPublicacao.dt_acesso_url.format('dd/MM/YYYY') + '.' : '') + ' (Resumo)'
                ).trim()
                break;
            case 'CAPITULO_LIVRO':
            case 'ARTIGO_CIENTIFICO':
            case 'SITE':
            case 'ATO_OFICIAL':
                return ( autorAno +
                    (rowPublicacao.de_titulo ? ' ' + rowPublicacao.de_titulo + '.' : '') +
                    (rowPublicacao.no_revista_cientifica ? ' <b>' + rowPublicacao.no_revista_cientifica + '</b>,' : '') +
                    (rowPublicacao.de_volume ? ' ' + rowPublicacao.de_volume : '') +
                    (rowPublicacao.de_issue ? ' (' + rowPublicacao.de_issue + ')' : '') + (rowPublicacao.de_volume ? ':' : '') +
                    (rowPublicacao.de_paginas ? ' p.' + rowPublicacao.de_paginas + '.' : '') +
                    (rowPublicacao.de_editores ? ' <i>In</i>: ' + getNomesEtAll( rowPublicacao.de_editores ?: '') + '.' : '') +
                    (rowPublicacao.de_titulo_livro ? ' <b>' + rowPublicacao.de_titulo_livro + '</b>.' : '') +
                    (rowPublicacao.no_editora ? ' ' + rowPublicacao.no_editora : '') +
                    (rowPublicacao.no_universidade ? rowPublicacao.no_universidade + '.' : '') +
                    (rowPublicacao.no_cidade ? ' ' + rowPublicacao.no_cidade + '.' : '') +
                    (rowPublicacao.de_url ? ' Disponível em: ' + url : '') +
                    (rowPublicacao.dt_acesso_url ? ' Acessado em: ' + rowPublicacao.dt_acesso_url.format('dd/MM/YYYY') + '.' : '')
                ).trim()
                break;

                //------------------------------------------------------------
            case 'LIVRO':
                return (autorAno +
                    (rowPublicacao.de_titulo ? ' <b>' + rowPublicacao.de_titulo + '</b>.' : '') +
                    (rowPublicacao.no_revista_cientifica ? ' <b>' + rowPublicacao.no_revista_cientifica + '</b>,' : '') +
                    (rowPublicacao.de_volume ? ' ' + rowPublicacao.de_volume : '') +
                    (rowPublicacao.de_issue ? ' (' + rowPublicacao.de_issue + ')' : '') + (rowPublicacao.de_volume ? ':' : '') +
                    (rowPublicacao.de_paginas ? ' p.' + rowPublicacao.de_paginas + '.' : '') +
                    (rowPublicacao.de_editores ? ' <i>In</i>: ' + getNomesEtAll(rowPublicacao.de_editores) + ' (eds.).' : '') +
                    (rowPublicacao.de_titulo_livro ? ' ' + rowPublicacao.de_titulo_livro : '') +
                    (rowPublicacao.no_editora ? ' ' + rowPublicacao.no_editora : '') +
                    (rowPublicacao.no_cidade ? ' ' + rowPublicacao.no_cidade + '.' : '') +
                    (rowPublicacao.de_url ? ' Disponível em: ' + url : '') +
                    (rowPublicacao.dt_acesso_url ? ' Acessado em: ' + rowPublicacao.dt_acesso_url.format('dd/MM/YYYY') + '.' : '')
                ).trim()
                break;
            case 'MIDIA_DIGITAL':
            case 'PLANO_MANEJO':
            case 'RELATORIO_TECNICO':
                if (rowPublicacao.co_tipo_publicacao != 'LIVRO' && rowPublicacao.co_tipo_publicacao != 'PLANO_MANEJO') {
                    negritoOpen = '';
                    negritoClose = '';
                }
                return (autorAno +
                    (rowPublicacao.de_titulo ? negritoOpen + ' ' + rowPublicacao.de_titulo + negritoClose + '.' : '') +
                    (rowPublicacao.nu_edicao_livro ? ' ' + rowPublicacao.nu_edicao_livro : '') +
                    (rowPublicacao.de_editores ? ' ' + getNomesEtAll(rowPublicacao.de_editores) + '.' : '') +
                    (rowPublicacao.no_cidade ? ' ' + rowPublicacao.no_cidade + '.' : '') +
                    (rowPublicacao.de_volume ? ' ' + rowPublicacao.de_volume + '.' : '') +
                    (rowPublicacao.de_issue ? ' (' + rowPublicacao.de_issue + ')' : '') + (rowPublicacao.de_volume ? ':' : '') +
                    (rowPublicacao.de_paginas ? ' p.' + rowPublicacao.de_paginas + '.' : '') +
                    (rowPublicacao.de_url ? ' Disponível em:' + url : '') +
                    (rowPublicacao.dt_acesso_url ? 'Acessado em: ' + rowPublicacao.dt_acesso_url.format('dd/MM/YYYY') + '.' : '')
                ).trim()
                break;
                //------------------------------------------------------------
            case 'DISSERTACAO_MESTRADO':
                return (autorAno +
                    (rowPublicacao.de_titulo ? ' ' + negritoOpen + rowPublicacao.de_titulo + negritoClose + '. Dissertação de Mestrado.' : '') +
                    (rowPublicacao.no_universidade ? ' ' + rowPublicacao.no_universidade + '.' : '') +
                    (rowPublicacao.no_cidade ? ' ' + rowPublicacao.no_cidade + '.' : '') +
                    (rowPublicacao.de_paginas ? ' p.' + rowPublicacao.de_paginas + '.' : '')
                ).trim()

                break;
                //------------------------------------------------------------
            case 'BANCO_DADOS_INSTITUCIONAL':
                    return (autorAno + (rowPublicacao.de_titulo ? ' '+rowPublicacao.de_titulo : '')).trim()
                    break;
                //------------------------------------------------------------
            case 'TESE_DOUTORADO':
                return (autorAno +
                    (rowPublicacao.de_titulo ? ' ' + negritoOpen + rowPublicacao.de_titulo + negritoClose + '. Tese de Doutorado.' : '') +
                    (rowPublicacao.no_universidade ? ' ' + rowPublicacao.no_universidade + '.' : '') +
                    (rowPublicacao.no_cidade ? ' ' + rowPublicacao.no_cidade + '.' : '') +
                    (rowPublicacao.de_paginas ? ' p.' + rowPublicacao.de_paginas + '.' : '')
                ).trim()
                break;
                //------------------------------------------------------------
            default:
                //return autorAno + (rowPublicacao.de_titulo ? negritoOpen + ' ' + rowPublicacao.de_titulo + negritoClose : '');
                return (autorAno + (rowPublicacao.de_titulo ? ' ' + rowPublicacao.de_titulo : '')).trim()
        }
    } // fim getReferenciaHtml

    /**
     * Padronizar as coordenadas geográficas com 6 casas decimais
     * @param latY
     * @param lonX
     * @return Map
     */
    static Map roundCoords( Double latY, Double  lonX ) {
        Map result= [latY:latY, lonX:lonX, hash:'']
        try {
            result.lonX = Math.round(lonX * 100000000) / 100000000
            result.latY = Math.round(latY * 100000000) / 100000000
            result.hash = (result.lonX.toString() + '/' + result.latY.toString()).encodeAsMD5()
        } catch (Exception e) {}
        return result
    }

    /**
     * receber o codigo da esfera e retornar o nome da esfera
     * @param codigo
     * @return
     */
    static String nomeEsfera( String codigo = '',Boolean plural=false) {
        codigo = codigo?.toUpperCase()
        switch (codigo) {
            case 'F':
                return plural ? 'Federais' : 'Federal'
            case 'E':
                return plural ? 'Estaduais' : 'Estadual'
            case 'R':
                return plural ? 'RPPN' : 'RPPN'
            case 'T':
                return plural ? 'Terras Indígenas' : 'Terra Indígena'
            default:
                return ''
        }
    }

    /**
     * Metodo para formatar o campos json referentes as colaboracoes
     * recebidas pela ficha durante as consultas e revisões
     * para serem exibidos no gride na coluna colaboracoes
     * @param row
     * @readonly boolean
     * @return
     */
    static String formatarColaboracoes(Map row = [:], boolean readonly=false ) {

        if (!row) {
            return ''
        }
        Map secoes = [:]
        Long sqFicha = row?.sq_ficha?.toLong()
        Long sqFichaVersao = row?.sq_ficha_versao ?: 0l
        if (!sqFicha) {
            return ''
        }
        // ler as colaboracoes da ficha
        if (row.json_colaboracoes) {
            /** /
             println ' '
             println 'JSONCOLABORACOES'
             println 'ROW'
             println row
             println 'COLABORACOES'
             println row.json_colaboracoes
             /**/
            try {
                JSONObject json = JSON.parse(row.json_colaboracoes.toString())
                json.sort { it.value.ds_tipo + it.value.no_usuario }
                json.each { key, value ->
                    String tipo = Util.removeAccents(value.ds_tipo).toLowerCase()
                    String nome = Util.nomeAbreviado(value.no_usuario)
                    String sqWebUsuario = value.sq_web_usuario.toString()
                    boolean avaliada = value.nu_nao_avaliada.toInteger() == 0
                    if (!secoes[tipo]) {
                        secoes[tipo] = [:]
                    }
                    if (!secoes[tipo]['nomes']) {
                        secoes[tipo]['nomes'] = []
                    }
                    String link = """<a href="javascript:void(0);" onClick="gerenciarConsulta.showColaboracoes(${sqFicha},${sqWebUsuario},${readonly}, ${sqFichaVersao});"><label title="Clique para visualizar as colaborações" class="cursor-pointer ${avaliada ? 'green' : 'red'}">${nome}</label></a>"""
                    secoes[tipo]['nomes'].push(link)
                }
            } catch (Exception e) {
                println ' '
                println 'ERRO Uitl.formatarColaboracoes()'
                println 'Dados row.json_colaboracoes recebido:'
                println row.json_colaboracoes.toString()
            }
        }

        if (row.json_planilha) {
            /** /
             println ' '
             println row.json_planilha
             /**/
            try {
                JSONObject json = JSON.parse(row.json_planilha.toString())
                json.sort { it.value.ds_tipo + it.value.no_usuario }
                json.each { key, value ->
                    String tipo = Util.removeAccents(value.ds_tipo).toLowerCase()
                    String nome = Util.nomeAbreviado(value.no_usuario)
                    String arquivo = value.no_arquivo
                    String local = value.de_local_arquivo
                    String id = value.sq_ficha_consulta_anexo
                    boolean avaliada = value.nu_nao_avaliada.toInteger() == 0
                    if (!secoes[tipo]) {
                        secoes[tipo] = [:]
                    }
                    if (!secoes[tipo]['planilhas']) {
                        secoes[tipo]['planilhas'] = []
                    }
                    /** /
                     println nome
                     println arquivo
                     println local
                     println id
                     println avaliada
                     /**/

                    String html = """<div style="display:flex;flex-direction: row;justify-content:space-between;">
                                       <div>
                                           <label class="${(avaliada ? 'green' : 'red')}">${nome}&nbsp;
                                                <a title="Baixar arquivo ${arquivo}" href="javascript:void()"
                                                    id="link-planilha-${id}"
                                                    onClick="downloadWithProgress({'fileName':'${local}','originalFileName':'${arquivo}'})">
                                                    <i class="fa fa-download"></i>
                                                </a>
                                            </label>
                                            <label class="ml20 ${readonly ? '' : 'cursor-pointer'}"
                                                   title="${readonly ? '' :'Marcar/desmarcar com redebido o arquivo enviado.\nObs: ao marcar como recebido, a importação dos registros NÃO É automática pelo sistema.'}" style="padding:0">
                                                   <input ${readonly ? 'disabled ' : 'data-change="gerenciarConsulta.updateSituacaoColaboracaoPlanilha"'}
                                                   data-sq-ficha-consulta-anexo="${id}"
                                                   ${ avaliada ? ' checked' : ''}
                                                   type="checkbox" value="S" class="checkbox-lg"> recebido
                                            </label>
                                          </div>
                                        </div>"""
                    secoes[tipo]['planilhas'].push(html)
                }
            } catch (Exception e) {
                println ' '
                println 'ERRO Uitl.formatarColaboracoes()'
                println 'Dados row.json_planilha recebido:'
                println row.json_planilha.toString()
            }
        }


        if (row.json_ocorrencias) {
            /** /
             println ' '
             println 'JSON OCORRENCIAS'
             println row.json_ocorrencias
             //{ "rnd-1455428" : {"ds_tipo" : "Revisão", "no_usuario" : "Carlos Carvalho", "nu_nao_avaliada" : 1, "nu_avaliada" : 0}, "rnd-9073718" : {"ds_tipo" : "Ampla", "no_usuario" : "Luis Eugênio Barbosa TBA", "nu_nao_avaliada" : 2, "nu_avaliada" : 0}, "rnd-8163282" : {"ds_tipo" : "Direta", "no_usuario" : "Luis Eugênio Barbosa TBA", "nu_nao_avaliada" : 1, "nu_avaliada" : 0}, "rnd-5171315" : {"ds_tipo" : "Direta", "no_usuario" : "Thaís Melo de Almeida", "nu_nao_avaliada" : 1, "nu_avaliada" : 0} }
             /**/

            try {
                JSONObject json = JSON.parse(row.json_ocorrencias.toString())
                json.sort { it.value.ds_tipo + it.value.no_usuario }
                json.each { key, value ->
                    String tipo = "Registros" //Util.removeAccents(value.ds_tipo).toLowerCase()
                    String nome = Util.nomeAbreviado(value.no_usuario)
                    boolean avaliada = value.nu_nao_avaliada.toInteger() == 0
                    if (!secoes[tipo]) {
                        secoes[tipo] = [:]
                    }
                    if (!secoes[tipo]['ocorrencias']) {
                        secoes[tipo]['ocorrencias'] = []
                    }
                    String html = """<label class="${avaliada ? 'green' : 'red'}">${nome}</label>"""
                    if( !secoes[tipo]['ocorrencias'].contains(html)) {
                        secoes[tipo]['ocorrencias'].push(html)
                    }
                }
            } catch (Exception e ) {
                println ' '
                println 'ERRO Uitl.formatarColaboracoes()'
                println 'Dados row.json_ocorrencias recebido:'
                println row.json_ocorrencias.toString()
            }
        }

        try {
            // criar html das seções
            String htmlResultado = ''
            secoes = secoes.sort {
                return it.key.toString().toUpperCase()
            }
            secoes.each { tipo, value ->
                String tipoLabel = tipo == 'ampla' ? 'Consulta ampla' : tipo
                tipoLabel = tipo == 'direta' ? 'Consulta direta' : tipoLabel
                tipoLabel = tipo == 'revisao' ? 'Revisão' : tipoLabel
                htmlResultado += htmlResultado ? '<hr class="hr-colaboracao">' : ''
                htmlResultado += """<li class="li-colaboracao-tipo">${tipoLabel}"""
                if (secoes[tipo].nomes) {
                    secoes[tipo].nomes = secoes[tipo].nomes.sort()
                    htmlResultado += '<ol class="ol-colaboracao">'
                    secoes[tipo].nomes.each {
                        htmlResultado += '<li class="li-colaboracao-item">' + it + '</li>'
                    }
                    htmlResultado += '</ol>'
                }
                if (secoes[tipo].planilhas) {
                    htmlResultado += '<ul class="ul-colaboracao">'
                    htmlResultado += '<li class="li-colaboracao-subtipo">Planilha de registros'
                    htmlResultado += '<ol class="ol-colaboracao">'
                    secoes[tipo].planilhas.each {
                        htmlResultado += '<li class="li-colaboracao-item">' + it + '</li>'
                    }
                    htmlResultado += '</ol>'
                    htmlResultado += '</li>'
                    htmlResultado += '</ul>'
                }

                if (secoes[tipo].ocorrencias) {

                    htmlResultado += '<ul class="ul-colaboracao">'
                    htmlResultado += '<li class="li-colaboracao-subtipo">Ocorrências'
                    htmlResultado += '<ol class="ol-colaboracao">'
                    secoes[tipo].ocorrencias.each {
                        htmlResultado += '<li class="li-colaboracao-item">' + it + '</li>'
                    }
                    htmlResultado += '</ol>'
                    htmlResultado += '</li>'
                    htmlResultado += '</ul>'
                }
                htmlResultado += '</li>'
            }
            String collapse = ""
            if (htmlResultado) {
                htmlResultado = '<ul class="ul-colaboracao">' + htmlResultado + '</ul>'
                collapse = """<div class="text-center"><a title="Mostrar/esconder colaborações" class="btn btn-link btn-sm" data-toggle="collapse" onclick="toggleImage(this)" href="#collapse-${row.sq_ficha}"><i class="fa fa-chevron-up" style="font-size:20px !important;"></i></a></div>
        <div class="collapse in" id="collapse-${row.sq_ficha}">${htmlResultado}</div>"""
            }
            return collapse
       } catch ( Exception e ) {
            println ' '
            println 'ERRO SALVE - Util.formatarColaboracoes() ' + new Date().format('dd/MM/yyyy HH:mm:ss')
            println 'ROW'
            println row
            return '<span>Erro ao exibir colaboração</span>'
        }
    }

    /**
     * transoforar nomes de variavies no formato camelCase
     * @example nome_do_usuario -> nomeDoUsuario
     * @param text
     * @return
     */
    static String toCamelCase( String text, boolean capitalized = false ) {
        text = text.replaceAll( "(_)([A-Za-z0-9])", { Object[] it -> it[2].toUpperCase() } )
        return capitalized ? capitalize(text) : text
    }

    /**
     * transoforar nomes de variavies no formato snakeCase
     * @example nomeDoUsuario -> nome_do_usuario
     * @param text
     * @return
     */
    static String toSnakeCase( String text ) {
        return text.replaceAll(/([A-Z])/, /_$1/).toLowerCase().replaceAll(/^_/, '')
    }

     /** metodo para criar a lista de nomes a partir do retorno agregado do banco de dados com a função
     * array_to_string( array_agg( cte_bacias.no_bacia order by cte_bacias.ordem),';')
     * @Exemplo entrada: 1 - Região Hidrográfica Amazônica|1.3 - Sub-bacia Madeira;1 - Região Hidrográfica Amazônica|1.6 - Sub-bacia Purus
     * @param nomes
     * @return
     */
    static List criarListaNomesFromArrayAgg( String nomes = '' ){
        if( ! nomes ){
            return []
        }
        List result=[]
        try {
            nomes.split(';').each { n1 ->
                n1.split('\\|').each { n2 ->
                    n2 = n2.toString().trim()
                    if (!result.contains(n2)) {
                        result.push(n2);
                    }
                }
            }
        } catch( Exception  e ){
            println ' '
            println 'ERRO SALVE - Util.criarListaNomesFromArrayAgg() - ' + new Date().format('dd/MM/yyyy HH:mm:ss')
            println 'Valor recebido:' + nomes
            println e.getMessage();
            println '-'*50
        }
        return result
    }

    /**
     * método para gerar uma lista html <ul><li></li></ul> a partir de uma lista de string
     * @param lista
     * @return
     */
    static String array2Ul( List lista = [] ) {
        if( !lista  ) {
            return ''
        }
        String result = ''
        try {
            lista = lista.findAll{ it && it.toString() !='null' }
            if( lista ) {
                result = '<ul>' + lista.collect { '<li>' + it.toString().trim() + '</li>' }.join('') + '</ul>'
            }
        } catch( Exception  e ){
            println ' '
            println 'ERRO SALVE - Util.array2Ul() - ' + new Date().format('dd/MM/yyyy HH:mm:ss')
            println 'Valor recebido:' + lista
            println e.getMessage();
            println '-'*50
        }
        return result
    }

    /**
     * método para gerar uma lista de itens separados com quebra de linha \n
     * @param lista
     * @return
     */
    static String array2Lf( List lista = [] ) {
        if( !lista ) {
            return ''
        }
        lista = lista.findAll{ it && it.toString() !='null' }
        if( lista ) {
            return lista.collect { it.toString().trim() }.join('\n')
        }
        return ''
    }

    /**
     * método para gerar uma lista de itens separados com quebra de linha html <br>
     * @param lista
     * @return
     */
    static String array2Br( List lista = [] ) {
        if( !lista ) {
            return ''
        }
        lista = lista.findAll{ it && it.toString() != 'null' }
        if( lista ) {
            return lista.collect { it.toString().trim() }.join('<br>')
        }
        return ''
    }

    /**
     * Método para gerar um print/log
     * @param String contexto, String mensagem, String mensagemErro
     * @return String
     */
    static String printLog(String contexto = '', String mensagem = '', String mensagemErro = '') {
        println ' '
        println '-' * 60
        println(contexto ?: '')
        println(mensagem ?: '')
        if ( mensagemErro ) {
            println( mensagemErro ?: '' )
        }
        println new Date().format('dd/MM/yyyy HH:mm:ss')
        println '-' * 60
    }

    /**
     * metodo para verificar se o arquivo em cache ja expirou
     * caso o tempo de vida tenha expirado, o arquivo é excluido
     * Se for passado um valor negativo o cache será desconsiderado.
     * @param fullFileName
     * @params lifeTimeInMinutes
     * @return
     */
    static boolean cacheExists(File fullFileName, Long lifeTimeInMinutes = 5) {
        // return true;
        if( ! fullFileName.exists() ) {
            return false
        }
        if ( lifeTimeInMinutes < 1) {
            fullFileName.delete();
            return false // sem cache
        }
        Date modifiedAt = new Date(fullFileName.lastModified())
        int minutes = dateDiff(modifiedAt, new Date(), 'M')
        if ( minutes >= lifeTimeInMinutes) {
            fullFileName.delete()
            return false
        }
        return true
    }

    /**
     * Método para recuperar os dados armazenados no cache - /data/salve-estadual/cache/xxx
     * @param fullFileName - ex: /data/salve-estadual/cache/fileName.pdf
     * @param lifeTimeInMinutes
     * @return
     */
    static List loadFromCache(File file, Long lifeTimeInMinutes = 5) {
        List cacheData = []
        if( cacheExists( file, lifeTimeInMinutes ) ) {
            try{
                def jsonSlurper = new JsonSlurper()
                cacheData = jsonSlurper.parse(file)
            } catch( Exception e ) {
                printLog('SALVE-API','Metodo: Util.getCache()', e.getMessage() )
            }
        }
        return cacheData
    }

    /**
     * Método para gravar dados no cache
     * @param fullFileName
     * @param data
     */
    static void saveToCache(File fullFileName, List data = [] ) {
        if (data && data.size() > 0) {
            def json_str = JsonOutput.toJson(data)
            //def json_beauty = JsonOutput.prettyPrint( json_str )
            fullFileName.write(json_str)
        }
    }

    /**
     * metodo para remover o nome do autor do nome cientifico e retornar
     * somente o nome do taxon puro
     * @param cientificName
     * @return
     */
    static String clearCientificName(String cientificName = '' ) {
        List nameParts = []
        cientificName.trim().replaceAll(/\s{2,}/,' ').split(/\s/).eachWithIndex { it, index ->
            if (nameParts.size() < 3)
                if ( it.toString() =~ /^[A-Z]/) {
                    if (index == 0) {
                        nameParts.push(it)
                    }
                } else {
                    if ( it.toString() =~ /^[a-z]/) {
                        nameParts.push(it)
                    } else {
                        nameParts.push('')
                    }
                }
        }
        return nameParts.join(' ')
    }

    /**
     * função para extrair o texto, o nome do usuário e a data da gravação do texto da justificativa
     * da não utilização do registro na avaliação
     * @example Justificativa por nao utilizar o registro na avaliação<br/>Cadastrado por Luís Eugênio Barbosa em 30/01/2022 15:43:23
     * @return ['texto':'Justificativa por nao utilizar....','nome':'Luis Eugênio Barbosa', data:'30/01/2022 15:43:23']
     * @param txNaoUtilizadoAvaliacao
     * @return
     */
    static Map extrairTextoUsuarioDataTxNaoUtilizadoAvaliacao( String txNaoUtilizadoAvaliacao = '') {
        Map result = [texto:'',nome:'',data:null]
        String text = txNaoUtilizadoAvaliacao ?: ''
        try {
            if (text) {
                List parts = text.split('Cadastrado por')
                if( parts.size() == 2 ) {
                    result.texto =  parts[0].trim().replaceAll(/<br\/?>/, '')
                    parts = parts[1].trim().split(/ em /)
                    if (parts.size() == 2) {
                        result.nome = parts[0].trim();
                        result.data = parts[1].trim().replaceAll(/[^0-9:\s\/]/, '')
                    }
                }
            }
        } catch( Exception e ){}
        return result
    }

    /**
     * Calcular percentual de execução
     * @param position - valor atual
     * @param max - valor máximo
     * @param precision - número de casas decimais
     */
    static Double calcularPercentual( Integer position = 0, Integer max = 0, Integer precision = 1) {
        double percent = 0.0
        if( max > 0 ) {
            percent = ( position / max ) * 100
        }
        return percent.round( precision )
    }

    /**
     * calcular o codigo_sistema para as tabelas de apoio
     */
    static String calcularCodigoSistema( String descricao = '' ) {
        return Util.removeAccents(descricao)
            .replaceAll(/[^a-zA-Z0-9&\/\s_-]/,'') // somente letras, numeros, espacos e hifen
            .replaceAll(/[&\/\s-]/,'_').toUpperCase() // trocar espaco,barras,hifem e & por underline e converter para maiusculas
            .replaceAll(/_D[A-U]S?_/,'_') // remover DA, DE, DI, DO e DU seguidos ou não de S
            .replaceAll(/^[0-9]{1,}_/,'')
            .replaceAll(/_{2,}/, '_') //remover undelines duplos por simples
    }

    /**
     * formatar o codigo 1.1 em 001.001 para ordenar os itens com hierarquia corretamente na tabela de apoio
     * @param codigo
     * @return
     */
    static String formatarCodigoOrdem(String codigo = '' ){
        if( codigo =~ /^([0-9]{1,}\.?){1,}$/ ) {
            codigo = codigo.split(/\./ ).collect{it.toString().padLeft(3,'0')}.join('.')
        }
        return codigo
    }

    /**
     * método para ajustar nos campos abertos os nomes das categorias da iucn para que
     * fiquem no mesmo padrão de nomeclatura. Ex: Nome da Categoria (SIGLA)
     * @param texto
     * @return
     */
    static String corrigirCategoriasTexto( String texto = '' ){
        if( ! texto ) {
            return texto
        }
        // não refazer a correção aqui porque esta lógica
        // ja esta sendo feito via javascript
        return texto
        /*
        List categorias = [
            [nome:'Possivelmente Extinta',sigla:'PEX'],
            [nome:'Regionalmente Extinta',sigla:'RE'],
            [nome:'Extinta na Natureza',sigla:'EW'],
            [nome:'Criticamente em Perigo',sigla:'CR'],
            [nome:'Vulnerável',sigla:'VU'],
            [nome:'Em Perigo',sigla:'EN'],
            [nome:'Quase Ameaçada',sigla:'NT'],
            [nome:'Dados Insuficientes',sigla:'DD'],
            [nome:'Não Aplicável',sigla:'NA'],
            [nome:'Não Avaliada',sigla:'NE'],
            [nome:'Extinta',sigla:'EX'],
            [nome:'Menos Preocupante',sigla:'LC']
        ]
        // adicionar um espaço no final e remover caracteres especiaisl como &ccedil;
        texto = html2str(texto) + ' '
        categorias.each {categoria ->
            Pattern pattern = ~"(?i)${categoria.nome} ?-? ?(\\(?${categoria.sigla}\\)?([> ,.<]))"
            texto = texto.replaceAll(pattern, categoria.nome +' ('+categoria.sigla+')' + '$2' )
        }
        // remover espaço do final
        return texto.trim()*/
    }

    /**
     * colocar o nome antigo em italico seguido pelo nome do autor no formato correto
     * @example - Crax cumanensis Jacquin, 1784, Pipile cumanensis (Jacquin, 1784)
     * @param nomeAntigo
     * @param autor
     * @return
     */
    static String formatarNomeAntigo( String nomeAntigo, String autor ){
        autor = autor ? ', ' + autor.trim() :  ''
        // ajustar nomes com: virgula + espaco + parentesis para somente espaço + parentesis
        nomeAntigo = '<i>'+nomeAntigo.trim()+'</i>' + autor
        return nomeAntigo.replaceAll(/,\s\(/,' (')
    }


    /**
     * receber lista de nomes antigos e formatar para impressao da ficha
     * @param nomesComuns
     * @return
     */
    static String formatarNomesAntigos( List nomesAntigos = [] ) {
        if (!nomesAntigos) {
            return ''
        }
        List nomes = []
        nomesAntigos.each { item ->
            String nome = ''
            String autor= ''
            if( item.no_antigo){
                nome = item.no_antigo
                autor = item.no_autor ?: ''
            } else if( item.noAntigo){
                nome = item.noAntigo
                autor = item.noAutor ?: ''
            } else if( item.noSinonimia){
                nome = item.noSinonimia
                autor = item.noAutor ?: ''
            } else if( item.no_sinonimia){
                nome = item.no_sinonimia
                autor = item.no_autor ?: ''
            }
            nomes.push('<span>'+formatarNomeAntigo(nome, autor) + '</span>')
        }
        return nomes.join(', ') + ( nomes ? '.' : '')
    }

    /**
     * receber lista de nomes comuns e formatar para impressao da ficha
     * @param nomesComuns
     * @return
     */
    static String formatarNomesComuns( List nomesComuns = [] ){
        if( !nomesComuns ){
            return ''
        }
        List nomes = []
        nomesComuns.each { item ->
            String nome = ''
            String regiao=''
            if( item.no_comum ) {
                nome = item.no_comum ?: ''
                regiao = item.de_regiao_lingua ?: ''
            } else if( item.noComum ) {
                nome = item.noComum ?: ''
                regiao = item.deRegiaoLingua ?:''
            }
            if ( regiao && regiao.indexOf('(') == -1) {
                regiao = '(' + regiao + ')'
            }
            nomes.push( nome.trim() +' '+ regiao.trim()  )
        }
        return nomes.join(', ') + (nomes ? '.' : '')
    }

    /**
     * criar as páginas de dados utilizando o disco rígido
     * @param data
     * @param pageSize
     * @return
     */
    static Map paginateArrayToDisk( List data = [], Integer pageSize = 100, Integer currentPage = 1, Integer cacheMinutes = 60 ){
        // TODO - IMPLEMENTAR CACHEMINUTES
        //String hash = Util.md5(new Date().format('ddmmyyyyHHmmss')+Util.randomString() )
        String hash = data.toString().encodeAsMD5()
/** /
println ' '
println ' '
println ' '
println 'hash:' +hash
println 'current page :' +currentPage
/**/
        String tempFile = '/data/salve-estadual/cache/pagination-'+hash
//println 'tempFile: '+tempFile
        Map paginationData =[ hash: hash, page:currentPage, pages:1, rowNum:0, pageSize:pageSize
                              , totalRows   : 0, rows:[]]
        if( ! data ){
            paginationData.rows = []
            paginationData.pages = 0
            return paginationData
        }
        if( data.size() <= pageSize ) {
            paginationData.rows = data
            paginationData.totalRows = data.size()
            paginationData.pages = 1
            return paginationData
        }
        // gerar as paginas em disco
        paginationData.totalRows = data.size()

        data.collate(pageSize).eachWithIndex { rows, page->
            String fileName = tempFile+'.p'+(page+1)
            File file = new File( fileName )
            if( rows ) {
                String json_str = JsonOutput.toJson(rows)
                file.write(json_str)
                paginationData.pages = page + 1
            }
            // retornar a pagina corrente
            if( paginationData.pages == currentPage ) {
                paginationData.rows = rows
                paginationData.page = paginationData.pages
                paginationData.rowNum =  paginationData.pageSize * ( currentPage - 1 )
            }
        }
        // se a pagina corrente não exitir, pergar a última criada
        if( paginationData.pages < currentPage ){
            paginationData.page = paginationData.pages;
            paginationData.rows = paginateArrayGetPageData(hash, paginationData.pages )
            paginationData.rowNum =  paginationData.pageSize * ( paginationData.pages - 1 )
        }
        return paginationData
    }

    static List paginateArrayGetPageData( String hash='', Integer page = 1){
    /*
    println ' '
    println ' '
    println ' '
    println 'hash:' +hash
    */
        String tempFile = '/data/salve-estadual/cache/pagination-'+hash
        //println 'tempFile: '+tempFile
        List data = []
        if( ! hash ){
            return data
        }
        String fileName = tempFile + '.p' + page
        File file = new File(fileName)
        if( ! file.exists() ) {
            return data
        }
        def jsonSlurper = new JsonSlurper()
        List cachedPage = jsonSlurper.parse( file )
        // é preciso retornar os valores numericos e data para seus tipos originais
        return  parseRows( cachedPage )
    }

    /**
     * método para retonar para os tipos originais os campos date e number depois de recuperados do cache em disco
     * @param rows
     * @return
     */
    static List parseRows( List rows = [] ){
        if( !rows ) {
            return []
        }
        // se o array estiver no formato [1,2,3,4...] não precisaa fazer o parse
        if(  ! ( rows[0] instanceof Map) ){
            return rows
        }

        rows.eachWithIndex { row, index ->
            row.each { key, value ->
                try {
                    // converter campos data 2021-08-27T16:48:52+0000
                    if (key =~ /^dt_/) {
                        if (value =~ /^[0-9]{4}(-|\/)[0-9]{2}(-|\/)[0-9]{2}/) {
                            value = value.replaceAll(/\//, '-')
                            try {
                                row[key] = new Date().parse("yyyy-MM-dd'T'HH:mm:ss", value)
                            } catch (Exception e1) {
                                try {
                                    row[key] = new Date().parse("yyyy-MM-dd HH:mm:ss", value)
                                } catch (Exception e2) {
                                }
                            }
                        }
                    } else if (key =~ /^sq_/) {
                        // converter campos sequences
                        if (value.toString().isNumber()) {
                            row[key] = value.toString().toLong()
                        }
                    } else if (key =~ /^vl_/) {
                        // converter campos numericos
                        if (value.toString().isNumber()) {
                            row[key] = value.toString().toLong()
                        }
                    } else if (key =~ /^nu_/) {
                        // converter campos numericos
                        if (value.toString().isNumber()) {
                            row[key] = value.toString().toInteger()
                        }
                    } else if (key =~ /^json_/) {
                        try {
                            if( value?.value ) {
                                row[key] = value.value
                            }
                        } catch ( Exception e ) {
                        }
                    }
                } catch (Exception e) {
                }
            }
        }
        return rows
    }


/**
     * método para retonar para os tipos originais os campos date e number depois de recuperados do cache em disco
     * @param rows
     * @return
     */
    static boolean isCpf( String cpf ='' ){
        cpf = cpf.replaceAll(/[^0-9]/, "");
        if( !cpf) {
            return true
        }
        int d1, d2;
        int digito1, digito2, resto;
        int digitoCPF;
        String nDigResult;
        d1 = d2 = 0;
        digito1 = digito2 = resto = 0;
        for (int nCount = 1; nCount < cpf.length() - 1; nCount++) {
            digitoCPF = Integer.valueOf(cpf.substring(nCount - 1, nCount)).intValue();
            // multiplique a ultima casa por 2 a seguinte por 3 a seguinte por 4
            // e assim por diante.
            d1 = d1 + (11 - nCount) * digitoCPF;
            // para o segundo digito repita o procedimento incluindo o primeiro
            // digito calculado no passo anterior.
            d2 = d2 + (12 - nCount) * digitoCPF;
        };

        // Primeiro resto da divisão por 11.
        resto = (d1 % 11);
        // Se o resultado for 0 ou 1 o digito é 0 caso contrário o digito é 11
        // menos o resultado anterior.
        if (resto < 2)
            digito1 = 0;
        else
            digito1 = 11 - resto;
        d2 += 2 * digito1;
        // Segundo resto da divisão por 11.
        resto = (d2 % 11);
        // Se o resultado for 0 ou 1 o digito é 0 caso contrário o digito é 11
        // menos o resultado anterior.
        if (resto < 2)
            digito2 = 0;
        else
            digito2 = 11 - resto;
        // Digito verificador do CPF que está sendo validado.
        String nDigVerific = cpf.substring(cpf.length() - 2, cpf.length());
        // Concatenando o primeiro resto com o segundo.
        nDigResult = String.valueOf(digito1) + String.valueOf(digito2);
        // comparar o digito verificador do cpf com o primeiro resto + o segundo
        return nDigVerific.equals(nDigResult);
    }

    static boolean isCnpj( String cnpj = '' ) {
        cnpj = cnpj.replaceAll(/[^0-9]/, '')
        if (!cnpj) {
            return true
        }

        // considera-se erro CNPJ's formados por uma sequencia de numeros iguais
        if (cnpj.equals("00000000000000") || cnpj.equals("11111111111111")
                || cnpj.equals("22222222222222") || cnpj.equals("33333333333333")
                || cnpj.equals("44444444444444") || cnpj.equals("55555555555555")
                || cnpj.equals("66666666666666") || cnpj.equals("77777777777777")
                || cnpj.equals("88888888888888") || cnpj.equals("99999999999999") || (cnpj.length() != 14))
            return (false);
        char dig13, dig14;
        int sm, i, r, num, peso; // "try" - protege o código para eventuais
        // erros de conversao de tipo (int)
        try {
            // Calculo do 1o. Digito Verificador
            sm = 0;
            peso = 2;
            for (i = 11; i >= 0; i--) {
                // converte o i-ésimo caractere do CNPJ em um número: // por
                // exemplo, transforma o caractere '0' no inteiro 0 // (48 eh a
                // posição de '0' na tabela ASCII)
                num = (int) (cnpj.charAt(i) - 48);
                sm = sm + (num * peso);
                peso = peso + 1;
                if (peso == 10)
                    peso = 2;
            }

            r = sm % 11;
            if ((r == 0) || (r == 1))
                dig13 = '0';
            else
                dig13 = (char) ((11 - r) + 48);

            // Calculo do 2o. Digito Verificador
            sm = 0;
            peso = 2;
            for (i = 12; i >= 0; i--) {
                num = (int) (cnpj.charAt(i) - 48);
                sm = sm + (num * peso);
                peso = peso + 1;
                if (peso == 10)
                    peso = 2;
            }
            r = sm % 11;
            if ((r == 0) || (r == 1))
                dig14 = '0';
            else
                dig14 = (char) ((11 - r) + 48);
            // Verifica se os dígitos calculados conferem com os dígitos
            // informados.
            if ((dig13 == cnpj.charAt(12)) && (dig14 == cnpj.charAt(13)))
                return (true);
            else
                return (false);
        } catch (InputMismatchException erro) {
            return (false);
        }
    }

    /**
     * validar e-mail
     * @param email
     * @return
     */
    static boolean isEmail( String email = '' ){
        if( email == 'admin@salve-estadual'){
            return true
        }
        boolean isEmailIdValid = false;
        if (email != null && email.length() > 0) {
            String expression = '^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$'
            Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(email);
            if (matcher.matches()) {
                isEmailIdValid = true;
            }
        }
        return isEmailIdValid;
    }

    // fim util
}
