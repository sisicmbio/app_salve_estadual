package br.gov.icmbio

import com.itextpdf.text.pdf.PdfPTable

/**
 * https://www.programcreek.com/java-api-examples/?api=com.lowagie.text.rtf.RtfWriter2
 * https://www.javatips.net/api/com.lowagie.text.rtf.rtfwriter2
 */
import com.lowagie.text.Anchor
import com.lowagie.text.Cell
import com.lowagie.text.Image
//import Image
import com.lowagie.text.Table
import com.lowagie.text.rtf.RtfWriter2
import com.lowagie.text.rtf.direct.RtfDirectContent
import com.lowagie.text.rtf.field.RtfPageNumber
import com.lowagie.text.rtf.headerfooter.RtfHeaderFooter
import com.lowagie.text.rtf.headerfooter.RtfHeaderFooterGroup
import com.lowagie.text.rtf.style.RtfFont
import org.codehaus.groovy.grails.web.json.JSONObject
import org.codehaus.groovy.grails.web.servlet.mvc.GrailsHttpSession
import javax.swing.text.BadLocationException
import javax.swing.text.html.HTMLEditorKit
import javax.swing.text.rtf.RTFEditorKit
import java.awt.Color
import com.lowagie.text.Element
import com.lowagie.text.pdf.BaseFont
import com.lowagie.text.Font
import com.lowagie.text.HeaderFooter
import com.lowagie.text.Rectangle
import com.lowagie.text.Document
import com.lowagie.text.Chunk
import com.lowagie.text.Paragraph
import com.lowagie.text.PageSize

class RtfFicha
{
    private fichaService = null
    private dadosApoioService = null

    private boolean contextoPublico = false
    private String msgDiferencaCategorias=''

    private Ficha ficha
    private String unidade
    private String ciclo
    private String diaHora
    private String dataImpressao
    private String fileName

    private Rectangle pageSize
    private Document document
    private RtfWriter2 writer
    private Color corTopicos // verde clara
    private Color corTopicos2 // verde mais clara
    private Color corVermelha // verde mais clara
    private Map config

    // fontes utilizadas no relatório
    private headerFont
    private headerSubtitle
    private footerFont
    private footerFontSmall
    private titleFont
    private textFont
    private textFont6
    private textFont10
    private creditsFont
    private textItalicFont
    private textBoldFont
    private linkFont
    private Boolean throwError = false
    private java.util.List refBibs = []
    private String ufs = ''
    private String biomas = ''
    private String bacias = ''
    private java.util.List ucs = []

    RtfFicha(Integer sqFicha = -1, String dir='', Map config = [:], Boolean throwError = false, GrailsHttpSession currentSession = null ) {
        dir = (dir ?: '/data/salve-estadual/temp/')
        this.config = (config ?: [:])
        this.ficha = Ficha.get( sqFicha.toLong() )
        this.throwError = throwError

        this.diaHora = new Date().format('dd-MM-yyyy-HH-mm-ss');
        this.fileName = dir+'ficha_' + ficha.taxon.noCientifico.toString().replaceAll(/ /, '-') + '-' + diaHora + '.rtf'
        File file = new File( this.fileName )
        if( file.exists() )
        {
            file.delete()
        }
        this.fichaService = new FichaService(currentSession)
        this.dadosApoioService = new DadosApoioService()
    }

    /**
     * o contexto pode alterar algumas informações na ficha como por exemplo a categoria que sai
     * no inicio da ficha será lida da aba 11.7 e se não for igual a da última avaliação oficial mma
     * será exibido um texto no final da página.
     * @param newValue
     */
    void setContextoPublico( boolean newValue = true ) {
        this.contextoPublico = newValue
    }
    void setMsgDiferencaCategorias( String newValue = '' ) {
        this.msgDiferencaCategorias = newValue
    }
    void setUnidade( String newValue = '' ) {
        this.unidade = newValue
    }
    void setCiclo( String newValue = '' ) {
        this.ciclo = newValue
    }
    void setRefBibs( java.util.List refBibs = [] ){
        this.refBibs = refBibs
    }
    void setUfs( String ufs = '' ){
        this.ufs = ufs
    }
    void setBiomas( String biomas = '' ){
        this.biomas = biomas
    }
    void setBacias( String bacias = '' ){
        this.bacias = bacias
    }
    void setUcs( java.util.List ucs = [] ){
        this.ucs = ucs
    }

    String run() {
        String strTemp
        List lstTemp

        try {
            ciclo = ficha.cicloAvaliacao.deCicloAvaliacao
            dataImpressao = new Date().format('dd/MM/yyy HH:mm:ss')
            corVermelha  = new Color(255, 0, 0) // vermelha
            corTopicos = new Color(211, 224, 198) // verde claro
            corTopicos2 = new Color(235, 243, 227, 1) // verde claro

            Map ultimaAvaliacao = ficha.ultimaAvaliacaoNacional
            Map ultimaAvaliacaoOficial = ficha.ultimaAvaliacaoNacionalHist
            boolean houveMudancaCategoria = false
            if ( ultimaAvaliacaoOficial.coCategoriaIucn && ficha.categoriaFinal && ultimaAvaliacaoOficial.coCategoriaIucn != ficha.categoriaFinal.codigoSistema) {
                houveMudancaCategoria=true
                this.setMsgDiferencaCategorias( sprintf(msgDiferencaCategorias, ultimaAvaliacaoOficial.deCategoriaIucn))
            }

            // criar e abrir o documento e criar o cabeçalho e o rodapé
            initDoc()

            if( ficha.situacaoFicha.codigoSistema != 'PUBLICADA') {
                Paragraph par = new Paragraph('FICHA NÃO PUBLICADA.', headerFont)
                par.setAlignment(Element.ALIGN_CENTER)
                par.getFont().setColor(255, 0, 0)
                par.getFont().setStyle(Font.BOLD)
                document.add(par)
            }

            // imprimir o titulo com o nome da espécies e autor
            doTitle()
            //marcaDagua();


            //************************************************************************
            // Autoria
            //************************************************************************
            if ( ficha.dsCitacao ) {
                String nomes = ficha.dsCitacao.replaceAll(/(?i)<br ?\/?>/,', ').replaceAll(/-/,' ')
                br()
                doTexto( Util.stripTagsKeepLineFeed( nomes ),textFont10, 'c' )
            }

            //************************************************************************
            // Número do DOI
            //************************************************************************
            if( ficha.dsDoi ) {
                String numDoi   = ficha.dsDoi
                String urlDoi   = 'https://doi.org/'+numDoi
                Anchor anchor = new Anchor(numDoi,linkFont)
                anchor.setReference(urlDoi)
                Paragraph paragraph = new Paragraph()
                paragraph.setAlignment( Element.ALIGN_RIGHT )
                paragraph.add( Chunk.NEWLINE )
                paragraph.add(' ')
                paragraph.add(anchor)
                document.add( paragraph )
            }


            //************************************************************************
            // CATEGORIA ATUAL
            //************************************************************************
            String textoCategoria
            String textoDataAvaliacao
            String textoJustificativa
            if( ! this.contextoPublico ) {
                textoJustificativa = ultimaAvaliacao.txJustificativaAvaliacao
                if( ultimaAvaliacao?.deCategoriaIucn ) {
                    textoCategoria = ultimaAvaliacao.deCategoriaIucn + ' ' + ultimaAvaliacao.deCriterioAvaliacaoIucn
                    if( ultimaAvaliacao.mesAnoUltimaAvaliacao ) {
                        if( ultimaAvaliacao.mesAnoUltimaAvaliacao.toString().size() == 4 ) {
                            textoDataAvaliacao = 'Ano da avaliação: ' + ultimaAvaliacao.mesAnoUltimaAvaliacao
                        }
                        else {
                            textoDataAvaliacao = 'Data da avaliação: ' + ultimaAvaliacao.mesAnoUltimaAvaliacao
                        }
                    }
                }
            } else {
                houveMudancaCategoria = false
                if( ultimaAvaliacaoOficial.coCategoriaIucn && ficha.categoriaFinal && ultimaAvaliacaoOficial.coCategoriaIucn != ficha.categoriaFinal.codigoSistema ) {
                    String textoDiferencaCategoria = sprintf( this.msgDiferencaCategorias, ultimaAvaliacao.deCategoriaIucn )
                    houveMudancaCategoria = true
                    //headerFooter.setFirstPageFooterText( textoDiferencaCategoria )
                }
                textoCategoria = ficha.categoriaFinal.descricaoCompleta + ( houveMudancaCategoria ? '*' : '' )
                textoJustificativa = ficha.dsJustificativaFinal
                if( ultimaAvaliacao.mesAnoUltimaAvaliacao ) {
                    if( ultimaAvaliacao.mesAnoUltimaAvaliacao.toString().size() == 4 ) {
                        textoDataAvaliacao = 'Ano da avaliação: ' + ultimaAvaliacao.mesAnoUltimaAvaliacao
                    }
                    else {
                        textoDataAvaliacao = 'Data da avaliação: ' + ultimaAvaliacao.mesAnoUltimaAvaliacao
                    }
                }
            }
            doTopicCategoria( textoCategoria,textoDataAvaliacao,textoJustificativa)

            //Taxonomia
            Map imagemPrincipal = fichaService.getFichaImagemPrincipal(ficha)
            Map arvoreTaxonomica = ficha?.taxon?.getStructure()
            doClassificacaoTaxonomica(arvoreTaxonomica,imagemPrincipal)

            br()
            doSubtopic('Nomes Comuns', fichaService.getFichaNomesComunsText(ficha))

            br()
            doSubtopic('Nomes Antigos', fichaService.getFichaSinonimiasHtml(ficha))

            br()
            doSubtopic('Notas Taxonômicas', ficha?.dsNotasTaxonomicas)

            br()
            doSubtopic('Notas Morfológicas', ficha.dsDiagnosticoMorfologico)

            br()
            doTopic('Distribuição')

            br()
            doRvBold('Endêmica do Brasil: ', parseSnd(ficha.stEndemicaBrasil))

            // Distribuição Global
            br()
            doSubtopic('Distribuição Global', ficha.dsDistribuicaoGeoGlobal)

            // Distribuição Nacional
            if (ficha.dsDistribuicaoGeoNacional) {
                br()
                doSubtopic('Distribuição Nacional', ficha.dsDistribuicaoGeoNacional)
            }

            //************************************************************************
            // UFs
            //************************************************************************
            br()
            //doSubtopic('Estados', fichaService.getUfsNameByRegiaoText(ficha))
            doSubtopic('Estados', this.ufs)

            //************************************************************************
            // Biomas
            //************************************************************************
            br()
            doSubtopic('Biomas', this.biomas)

            // Bacias Hidrográficas

            if ( this.bacias ) {
                // Bacias Hidrográficas
                br()
                doSubtopic('Bacias Hidrográficas', this.bacias)
            }

            lstTemp = fichaService.getAreasRelevantesList(ficha)
            if (true || lstTemp) {
                br()
                doSubtopic('Áreas Relevantes')
                Table table = doTable(['Tipo', 'Local', 'Estado', 'Município', 'Referência Bibliográfica'])
                //table.setPadding(1)
                table.setOffset(1)
                lstTemp.each {
                    doRow(table, [it?.tipoRelevancia?.descricao
                                  , it?.localHtml
                                  , it?.estado?.noEstado
                                  , it?.municipiosHtml
                                  , it?.refBibText])
                }

                document.add(table)
            }

            // Mapa Distribuição
            doImage(fichaService.getFichaDistribuicaoMapaPrincipal(ficha))

            // História Natural
            br()
            doTopic('História Natural')

            // Espécie migratória:
            if (true || (ficha.stMigratoria && ficha.stMigratoria != 'D')) {
                br()
                String padraoDeslocamento = (ficha.padraoDeslocamento ? ficha.padraoDeslocamento.descricao : '')
                doRvBold('Espécie migratória? ', parseSnd(ficha.stMigratoria) + (ficha.stMigratoria == 'S' ? ', padrão de deslocamento ' + padraoDeslocamento : ''))
            }

            br()
            doTexto(ficha.dsHistoriaNatural)

            // habito Alimentar
            br()
            doSubtopic('Hábito Alimentar')

            lstTemp = FichaHabitoAlimentar.findAllByFicha(ficha)
            if (lstTemp) {
                Table table = doTable(['Tipo', 'Referência Bibliográfica'])
                //table.setPadding(1)
                table.setOffset(1)
                lstTemp.each {
                    doRow(table, [it?.tipoHabitoAlimentar?.descricao
                                  , it?.refBibText])
                }
                document.add(table)
            }

            br()
            doRvBold('Hábito alimentar especialista? ', parseSnd(ficha.stHabitoAlimentEspecialista))
            if (ficha.stHabitoAlimentEspecialista == 'S') {
                lstTemp = FichaHabitoAlimentarEsp.findAllByFicha(ficha)
                if (lstTemp) {
                    Table table = doTable(['Taxon', 'Categoria'])
                    lstTemp.each {
                        doRow(table, [it?.taxon?.noCientifico
                                      , it?.categoriaIucn?.descricaoCompleta
                        ])
                    }
                    document.add(table)
                }
            }
            if (true || ficha.dsHabitoAlimentar) {
                br()
                doSubtopic('Observações sobre o hábito alimentar', ficha.dsHabitoAlimentar)
            }

            // habitat
            br()
            doSubtopic('Habitat')
            lstTemp = FichaHabitat.findAllByFicha(ficha)
            if (lstTemp) {
                Table table = doTable(['Tipo', 'Referência Bibliográfica'])
                lstTemp.each {
                    doRow(table, [it?.habitat?.descricao
                                  , it?.refBibText])
                }
                document.add(table)
            }

            //************************************************************************
            // Restrito a habitat primário?  (se não for preenchido não aparece)
            //************************************************************************
            br()
            doRvBold('Restrito a habitat primário? ', parseSnd(ficha.stRestritoHabitatPrimario))

            // Especialista em micro habitat?
            doRvBold('Especialista em micro habitat? ', parseSnd(ficha.stEspecialistaMicroHabitat))

            // micro habitat
            if (true || (ficha.dsEspecialistaMicroHabitat && ficha.stEspecialistaMicroHabitat == 'S')) {
                br()
                doSubtopic('Micro Habitat', ficha.dsEspecialistaMicroHabitat)
            }

            // texto descrição geral sobre o habitat
            if (true || ficha.dsUsoHabitat) {
                br()
                doSubtopic('Observações sobre o habitat', ficha.dsUsoHabitat)
            }

            // Interações com outras espécies (se não for preenchido não aparece)
            lstTemp = FichaInteracao.findAllByFicha(ficha)
            if (true || lstTemp) {
                br()
                doSubtopic('Interações com outras espécies')
                Table table = doTable(['Tipo', 'Taxon', 'Categoria', 'Referência Bibliográfica'])
                table.setOffset(1)
                lstTemp.each {
                    Paragraph parNoCientifico = new Paragraph('', textFont)
                    Chunk chunk = new Chunk(it.taxon.noCientifico, textItalicFont)
                    chunk.getFont().setColor(0, 0, 0)
                    parNoCientifico.add(chunk)

                    doRow(table, [it?.tipoInteracao?.descricao
                                  , parNoCientifico // it?.taxon?.noCientificoItalico
                                  , it?.categoriaIucn?.descricaoCompleta
                                  , it?.refBibText])
                }
                document.add(table)
                br()
                doTexto(ficha?.dsInteracao)
            }

            doSubtopic('Reprodução')

            // Intervalo de nascimentos: (se não for preenchido não aparece)
            if (true || ficha.vlIntervaloNascimento) {
                doRvBold('Intervalo de nascimentos: ', (ficha?.vlIntervaloNascimento ? valorComVirgula(ficha.vlIntervaloNascimento) + ' ' + ficha?.unidIntervaloNascimento?.descricao : ''))
            }

            // Tempo de gestação: (se não for preenchido não aparece)
            if (true || ficha.vlTempoGestacao) {
                doRvBold('Tempo de gestação: ', ficha?.vlTempoGestacao ? valorComVirgula(ficha.vlTempoGestacao) + ' ' + ficha?.unidTempoGestacao?.descricao : '')
            }

            // Tamanho da prole: (se não for preenchido não aparece)
            if (true || ficha.vlTamanhoProle) {
                doRvBold('Tamanho da prole: ', (ficha?.vlTamanhoProle ? valorComVirgula(ficha.vlTamanhoProle) + ' individuo(s)' : ''))
            }
            /*if (ficha.vlMaturidadeSexualFemea || ficha.vlMaturidadeSexualMacho || ficha.vlPesoMacho
                    || ficha.vlPesoFemea || ficha.vlComprimentoMacho || ficha.vlComprimentoFemea
                    || ficha.vlSenilidadeReprodutivaMacho || ficha.vlSenilidadeReprodutivaFemea
                    || ficha.vlLongevidadeMacho || ficha.vlLongevidadeFemea)
                    */
            if (true) {
                Table table = doTable(['Campo', 'Macho', 'Fêmea'])
                // maturidade sexual
                if (true || ficha.vlMaturidadeSexualMacho || ficha.vlMaturidadeSexualFemea) {
                    doRow(table, ['Maturidade Sexual',
                                  // macho
                                  ficha?.vlMaturidadeSexualMacho ? valorComVirgula(ficha?.vlMaturidadeSexualMacho )+ ' ' + ficha?.unidMaturidadeSexualMacho?.descricao : '',
                                  // femea
                                  ficha?.vlMaturidadeSexualFemea ? valorComVirgula(ficha?.vlMaturidadeSexualFemea) + ' ' + ficha?.unidMaturidadeSexualFemea?.descricao : ''
                    ])
                }
                // peso
                if (true || ficha.vlPesoMacho || ficha.vlPesoFemea) {
                    doRow(table, ['Peso',
                                  // macho
                                  ficha?.vlPesoMacho ? valorComVirgula(ficha.vlPesoMacho) + ' ' + ficha?.unidadePesoMacho?.descricao : '',
                                  // femea
                                  ficha?.vlPesoFemea ? valorComVirgula(ficha.vlPesoFemea) + ' ' + ficha?.unidadePesoFemea?.descricao : ''
                    ])
                }

                // comprimento máximo
                if (true || ficha.vlComprimentoMachoMax || ficha.vlComprimentoFemeaMax) {
                    doRow(table, ['Comprimento Máximo',
                                  // macho
                                  ficha?.vlComprimentoMachoMax ? valorComVirgula(ficha.vlComprimentoMachoMax) + ' ' + ficha?.medidaComprimentoMachoMax?.descricao : '',
                                  // femea
                                  ficha?.vlComprimentoFemeaMax ? valorComVirgula(ficha.vlComprimentoFemeaMax) + ' ' + ficha?.medidaComprimentoFemeaMax?.descricao : ''
                    ])
                }

                // comprimento na maturidade sexual
                if (true || ficha.vlComprimentoMacho || ficha.vlComprimentoFemea) {
                    doRow(table, ['Comprimento na Maturidade Sexual',
                                  // macho
                                  ficha?.vlComprimentoMacho ? valorComVirgula(ficha.vlComprimentoMacho) + ' ' + ficha?.medidaComprimentoMacho?.descricao : '',
                                  // femea
                                  ficha?.vlComprimentoFemea ? valorComVirgula(ficha.vlComprimentoFemea) + ' ' + ficha?.medidaComprimentoFemea?.descricao : ''
                    ])
                }

                // senilidade reprodutiva
                if (true || ficha.vlSenilidadeReprodutivaMacho || ficha.vlSenilidadeReprodutivaFemea) {
                    doRow(table, ['Senilidade Reprodutiva',
                                  // macho
                                  ficha?.vlSenilidadeReprodutivaMacho ? valorComVirgula(ficha.vlSenilidadeReprodutivaMacho) + ' ' + ficha?.unidadeSenilidRepMacho?.descricao : '',
                                  // femea
                                  ficha?.vlSenilidadeReprodutivaFemea ? valorComVirgula(ficha.vlSenilidadeReprodutivaFemea) + ' ' + ficha?.unidadeSenilidRepFemea?.descricao : ''
                    ])
                }

                // logevidade
                if (true || ficha.vlLongevidadeMacho || ficha.vlLongevidadeFemea) {
                    doRow(table, ['Longevidade',
                                  // macho
                                  ficha?.vlLongevidadeMacho ? valorComVirgula(ficha.vlLongevidadeMacho) + ' ' + ficha?.unidadeLongevidadeMacho?.descricao : '',
                                  // femea
                                  ficha?.vlLongevidadeFemea ? valorComVirgula(ficha.vlLongevidadeFemea) + ' ' + ficha?.unidadeLongevidadeFemea?.descricao : ''
                    ])
                }
                document.add(table)
                br()
                doTexto(ficha?.dsReproducao)
            }

            // ---------------------------------------------------------------------------------------

            // População
            br()
            doTopic('População')

            if( ficha.vlTempoGeracional ) {
                br()
                doRvBold('Tempo geracional: ', ( valorComVirgula( ficha.vlTempoGeracional.toString() ) + ' ' + ficha?.medidaTempoGeracional?.descricao ?: '') )
            }
            // Tendência populacional
            br()
            doRvBold('Tendência populacional: ', (ficha?.tendenciaPopulacional?.descricao ?: ''))

            // Razão sexual: (se não for preenchido não aparece)
            if (ficha?.dsRazaoSexual) {
                doRvBold('Razão sexual: ', ficha?.dsRazaoSexual)
            }

            // Taxa de mortalidade natural: (se não for preenchido não aparece)
            if (ficha?.dsTaxaMortalidade) {
                br()
                doSubtopic('Taxa de mortalidade natural: ', ficha?.dsTaxaMortalidade)
            }

            if (ficha.dsCaracteristicaGenetica) {
                br()
                doSubtopic('Características Genéticas', ficha.dsCaracteristicaGenetica)
            }

            if (ficha.dsPopulacao) {
                br()
                doSubtopic('Observações Sobre a População', ficha.dsPopulacao)
            }

            // Adicionar a(s) imagem(ns) - População
            fichaService.getFichaImages(ficha, 'GRAFICO_POPULACAO').each {
                doImage(it.localArquivo, it.legenda)
            }

            // Ameaças
            br()
            doTopic('Ameaças')
            if (ficha.dsAmeaca) {
                br()
                doTexto(ficha.dsAmeaca)
            }
            lstTemp = FichaAmeaca.findAllByFicha(ficha)
            if (lstTemp) {
                br()
                Table table = doTable(['Ameaça', 'Referência Bibliográfica'])
                table.setOffset(1)
                lstTemp.sort { it.criterioAmeacaIucn.ordem }.each {
                    doRow(table, [it?.criterioAmeacaIucn?.descricaoHieraquia.replaceAll(/\s\/\s/, "\n"), it?.refBibText], ['left', 'center'])
                    //doRow(table, [it?.criterioAmeacaIucn?.descricaoCompleta, it?.refBibText], ['left', 'center'])
                    // Regiao
                    if (it?.regioesText) {
                        Chunk chunk = new Chunk("\t" + it.regioesText, textFont)
                        Cell cell = new Cell(chunk)
                        cell.setVerticalAlignment(Element.ALIGN_MIDDLE)
                        cell.setHorizontalAlignment(Element.ALIGN_LEFT)
                        cell.setColspan(2)
                        table.addCell(cell)
                    }
                }
                document.add(table)
            }


            // Adicionar a(s) imagem(ns) - Ameaça
            fichaService.getFichaImages(ficha, 'MAPA_AMEACA').each {
                doImage(it.localArquivo, it.legenda)
            }

            // Uso
            br()
            doTopic('Usos')
            if (ficha.dsUso) {
                br()
                doTexto(ficha.dsUso)
            }
            lstTemp = FichaUso.findAllByFicha(ficha)
            if (lstTemp) {
                // --------------------------------------------------------------------------------------------------------------
                Table table = doTable(['Uso', 'Referência Bibliográfica'])
                table.setOffset(1)
                lstTemp.each {
                    // Uso
                    doRow(table, [it?.uso?.descricaoHieraquia?.replaceAll(/\s\/\s/, "\n"),
                                  it?.refBibText
                    ], ['left', 'center'])

                    // Regiao
                    if (it?.regioesText) {
                        Chunk chunk = new Chunk("\t" + it.regioesText, textFont)
                        Cell cell = new Cell(chunk)
                        cell.setVerticalAlignment(Element.ALIGN_MIDDLE)
                        cell.setHorizontalAlignment(Element.ALIGN_LEFT)
                        cell.setColspan(2)
                        table.addCell(cell)
                    }
                }
                document.add(table)
            }


            br()
            doTopic('Conservação')
            br()


            // ler a ultima avaliação nacional se não houver preenchido o resultado final na aba 10.8
            // imprimir as informações da última avaliação nacional
            doSubtopic('Última Avaliação Nacional')
            /*if (ultimaAvaliacao) {
                doRv('Ano: ', ultimaAvaliacao.nuAnoAvaliacao.toString())
                doRv('Categoria: ', ultimaAvaliacao.deCategoriaIucn)
                doRv('Critério: ', ( ultimaAvaliacao.deCriterioAvaliacaoIucn ?: 'não se aplica.') )
                br()
                doSubtopic('Justificativa', ultimaAvaliacao.txJustificativaAvaliacao)
            } else {
                doRv('', 'Não Avaliada')
            }*/

           //lstTemp = TaxonHistoricoAvaliacao.findAllByTaxonAndNuAnoAvaliacaoLessThanEquals(ficha.taxon, ficha.cicloAvaliacao.nuAno + 4).sort {
           //     it.nuAnoAvaliacao.toString() + it.tipoAvaliacao.ordem
           // }
            lstTemp  = TaxonHistoricoAvaliacao.createCriteria().list {
                eq('taxon.id', ficha.taxon.id)
                le('nuAnoAvaliacao', (ficha.cicloAvaliacao.nuAno+4) )
                tipoAvaliacao {
                    order('ordem')
                }
                order('nuAnoAvaliacao','desc')
            }

            if (lstTemp) {
                boolean registroImpresso = false
                Table table = doTable(['Tipo', 'Ano', 'Abrangência', 'Categoria', 'Critério', 'Referência Bibliográfica'])
                table.setOffset(1)
                Boolean existeAvaliacaoOutra = false
                lstTemp.each {
                    boolean registroValido = true
                    /*if (it.tipoAvaliacao?.codigoSistema == 'NACIONAL_BRASIL'
                            && ultimaAvaliacao.nuAnoAvaliacao
                            && it.nuAnoAvaliacao.toInteger() >= ultimaAvaliacao.nuAnoAvaliacao.toInteger()) {
                        registroValido = false
                    }*/
                    if (registroValido) {
                        if (it?.categoriaIucn?.codigoSistema == 'OUTRA') {
                            existeAvaliacaoOutra = true
                        }
                        registroImpresso = true
                        String abrangencia = it?.noRegiaoOutra ? it.noRegiaoOutra : it?.abrangencia?.descricao
                        if( it?.abrangencia?.estado ) {
                            abrangencia+=' ('+it.abrangencia.estado.sgEstado+')'
                        }
                        doRow(table, [it.tipoAvaliacao?.descricao,
                                      it?.nuAnoAvaliacao.toString(),
                                      abrangencia,
                                      (it?.categoriaIucn?.codigoSistema != 'OUTRA' ? it?.categoriaIucn?.descricaoCompleta : it?.deCategoriaOutra + '*'),
                                      it?.deCriterioAvaliacaoIucn,
                                      it?.getRefBibText(ficha.id.toLong() )
                        ])
                    }
                }
                if (registroImpresso) {
                    doSubtopic('Histórico de Avaliações')
                    document.add(table)
                    if( ficha.dsHistoricoAvaliacao){
                        br()
                        doSubtopic('Observações gerais sobre o histórico de avaliação', ficha.dsHistoricoAvaliacao)
                    }
                }

                if (existeAvaliacaoOutra) {
                    doRv('', '*Categoria não utilizada no método IUCN.')
                }
            }

            br()
            doRvBold('Presença em lista nacional oficial de espécies ameaçadas de extinção? ', parseSnd(ficha.stPresencaListaVigente))
            // Presença em convenção (se não for preenchido não aparece)
            lstTemp = FichaListaConvencao.findAllByFicha(ficha)
            if (lstTemp) {
                br()
                doSubtopic('Presença em Convenções', '')
                Table table = doTable(['Convenção', 'Ano'])
                table.setOffset(1)
                lstTemp.each {
                    doRow(table, [it.listaConvencao?.descricao, it?.nuAno.toString()])
                }
                document.add(table)
            }

            // Ações de Conservação
            br()
            doSubtopic('Ações de Conservação')
            if (ficha.dsAcaoConservacao) {
                br()
                doTexto(ficha.dsAcaoConservacao)
            }
            lstTemp = FichaAcaoConservacao.findAllByFicha(ficha)
            if (lstTemp) {
                Table table = doTable(['Ação', 'Situação', 'Referência Bibliográfica'])
                table.setOffset(1)
                lstTemp.each {
                    doRow(table, [it.acaoConservacao?.descricaoCompleta, it.situacaoAcaoConservacao?.descricao, it?.refBibText], ['left'])
                    // plano de ação ou ordenamento
                    strTemp = (it?.planoAcao?.sgPlanoAcao ? it.planoAcao.sgPlanoAcao :
                            (it?.tipoOrdenamento?.descricao ?: ''))
                    if (strTemp != '') {
                        Chunk chunk = new Chunk("\t" + '- ' + strTemp)
                        Cell cell = new Cell(chunk)
                        cell.setVerticalAlignment(Element.ALIGN_MIDDLE)
                        cell.setHorizontalAlignment(Element.ALIGN_LEFT)
                        cell.setColspan(3)
                        table.addCell(cell)
                    }
                }
                document.add(table)
            }


            // Presenca em UC
            //lstTemp = fichaService.getUcsOcorrencia(ficha.id)
            /*
            br()
            doSubtopic('Presença em UC')
            if (ficha.dsPresencaUc) {
                br()
                doTexto(ficha.dsPresencaUc)
            }
            if (lstTemp) {
                Table table = doTable(['UC', 'Referência Bibliográfica'])
                table.setOffset(1)
                lstTemp.each {
                    doRow(table, [it.fichaOcorrencia?.ucHtml, it.fichaOcorrencia?.refBibText], ['left', 'left'])
                }
                document.add(table)
            }*/

            // Presenca em UC
            strTemp = ficha.dsPresencaUc
            strTemp = strTemp.toString().trim()

            if( this.ucs || strTemp ){
                br()
                doSubtopic('Presença em UC')
                if( ficha.dsPresencaUc ) {
                    br()
                    doTexto(ficha.dsPresencaUc)
                }
                if( this.ucs ) {
                    br()
                    Table table = doTable(['UC', 'Referência Bibliográfica'])
                    table.setOffset(1)
                    this.ucs.each {
                        String refs = br.gov.icmbio.Util.parseRefBib2Grid( it?.json_ref_bib?.toString(), true )
                        doRow(table, [it.no_uc, refs], ['left', 'left'])
                    }
                    document.add(table)
                }
            }
            /*
            Map ucs = ficha.getUcs()
            lstTemp = []
            ucs.each{ key, value ->
                String noUc = key
                String refBib = value.refs.join('\n')
                lstTemp.push([noUc:noUc, refBib:refBib])
            }
            if( lstTemp || strTemp ) {
                br()
                doSubtopic('Presença em UC')
                if( ficha.dsPresencaUc ) {
                    br()
                    doTexto(ficha.dsPresencaUc)
                }
                if( lstTemp ) {
                    br()
                    Table table = doTable(['UC', 'Referência Bibliográfica'])
                    table.setOffset(1)
                    lstTemp.each {
                        doRow(table, [it.noUc, it.refBib], ['left', 'left'])
                    }
                    document.add(table)
                }
            }*/



            // pesquisas
            br()
            doTopic('Pesquisa')
            if (ficha.dsPesquisaExistNecessaria) {
                br()
                doTexto(ficha.dsPesquisaExistNecessaria)
            }
            lstTemp = FichaPesquisa.findAllByFicha(ficha)
            if (lstTemp) {
                Table table = doTable(['Tema', 'Situação', 'Referência Bibliográfica'])
                table.setOffset(1)
                lstTemp.each {
                    doRow(table, [it?.tema?.descricao, it?.situacao.descricao, it?.refBibText])
                }
                document.add(table)
            }

            // COMO CITAR
            /*
            br()
            doSubtopic('Como citar')
            doTexto( fichaService.comoCitarFicha( ficha) )
             */
            // equipe técnica
            if( ficha.dsEquipeTecnica ){
                br()
                Table tableEquipeTecnica = doTable(['Equipe Técnica'])
                tableEquipeTecnica.setOffset(1)
                doRow(tableEquipeTecnica, [ficha.dsEquipeTecnica],['justify'])
                document.add(tableEquipeTecnica)
            }
            // colaboradores
            if( ficha.dsColaboradores ){
                br()
                Table tableColaboradores = doTable(['Colaboradores'])
                tableColaboradores.setOffset(1)
                doRow(tableColaboradores, [ficha.dsColaboradores],['justify'])
                document.add(tableColaboradores)
            }


            br()
            String comoCitar = fichaService.comoCitarFicha( ficha,ultimaAvaliacao?.mesAnoUltimaAvaliacao)
            Table tableComoCitar = doTable(['Como Citar'])
            tableComoCitar.setOffset(1)
            doRow(tableComoCitar, [comoCitar],['justify'])
            document.add(tableComoCitar)
            java.util.List refsTaxon = []
            java.util.List refsRegistrosOcorrencias = []

            // Referencias Bibliograficas
            /*

            java.util.List refsValidas = FichaRefBib.executeQuery("""select new map( a.id as id )
                           from FichaRefBib a, FichaOcorrencia b
                    where a.ficha.id = ${ficha.id.toString()}
                      and b.id = a.sqRegistro
                      and a.publicacao is not null
                      and a.noTabela = 'ficha_ocorrencia'
                      and a.noColuna = 'sq_ficha_ocorrencia'
                      and ( b.inUtilizadoAvaliacao = 'S' or b.inUtilizadoAvaliacao is null )
                """)
            refsValidas += FichaRefBib.executeQuery("""select new map( a.id as id )
                           from FichaRefBib a, FichaOcorrenciaPortalbio b
                    where a.ficha.id = ${ficha.id.toString()}
                      and b.id = a.sqRegistro
                      and a.publicacao is not null
                      and a.noTabela = 'ficha_ocorrencia_portalbio'
                      and a.noColuna = 'sq_ficha_ocorrencia_portalbio'
                      and b.inUtilizadoAvaliacao = 'S'
                """)

            //println 'ficha:' + ficha.nmCientifico
            // Referencias Bibliograficas
            // Referencias Bibliograficas
            lstTemp = FichaRefBib.createCriteria().list() {
                createAlias('ficha', 'fi')
                createAlias('fi.situacaoFicha', 'situacao')
                createAlias('fi.taxon', 'tx')
                ne('situacao.codigoSistema', 'EXCLUIDA')
                and {
                    or {
                        eq('ficha', ficha)
                        eq('tx.pai', ficha.taxon)
                    }
                    ne('noTabela', 'ficha_ocorrencia')
                    ne('noTabela', 'ficha_ocorrencia_portalbio')
                }
                isNotNull('publicacao') // não incluir a comunicação pessoal
            }.sort { Util.removeAccents(it.publicacao?.noAutor) }

            if( refsValidas ) {
                lstTemp += FichaRefBib.createCriteria().list() {
                    createAlias('ficha', 'fi')
                    createAlias('fi.situacaoFicha', 'situacao')
                    createAlias('fi.taxon', 'tx')
                    ne('situacao.codigoSistema', 'EXCLUIDA')
                    and {
                        or {
                            eq('ficha', ficha)
                            eq('tx.pai', ficha.taxon)
                        }
                    }
                    isNotNull('publicacao') // não incluir a comunicação pessoal
                    'in'('id', refsValidas?.id?.collect { it.toLong() })
                }.sort { Util.removeAccents(it.publicacao?.noAutor) }
            }

            if ( lstTemp ) {
                lstTemp.each {
                    //String htmlRefBib = it.referenciaHtml
                    String htmlRefBib = it.publicacao.deRefBibliografica ? it.publicacao.deRefBibliografica : it.publicacao.referenciaHtml // it.publicacao.deTitulo
                    if ( ( it.noTabela =~ /^ficha_ocorrencia/ )  ) {
                        if (!refsRegistrosOcorrencias.contains(htmlRefBib)) {
                            refsRegistrosOcorrencias.push(htmlRefBib);
                        }
                    } else {
                        if (!refsTaxon.contains(htmlRefBib) ) {
                            refsTaxon.push(htmlRefBib)
                        }
                    }
                }
                // se a referencia estiver duplicada na ficha e nos registros, exibir somente a da ficha
                refsRegistrosOcorrencias = refsRegistrosOcorrencias.findAll{ it1 ->
                    return  ! refsTaxon.find{ it2 -> it2 == it1 }
                }
            }
             */

            if( this.refBibs.size() > 0 ) {
                this.refBibs.each { row ->
                    if (row.no_origem == 'ficha') {
                        if( !refsTaxon.contains(row.de_ref_bibliografica) ) {
                            refsTaxon.push( row.de_ref_bibliografica )
                        }
                    } else {
                        if( !refsRegistrosOcorrencias.contains( row.de_ref_bibliografica) ) {
                            refsRegistrosOcorrencias.push(row.de_ref_bibliografica)
                        }
                    }
                }
                if( refsTaxon || refsRegistrosOcorrencias){
                    document.newPage()
                }

                if (refsTaxon) {
                    //br()
                    //br()
                    doSubtopic('Referências Bibliográficas')
                    //lista.sort{ stu1, stu2 -> Util.removeAccents(stu1.publicacao.noAutor).compareToIgnoreCase( Util.removeAccents( stu2.publicacao.noAutor) ) }
                    refsTaxon.each {
                        br()
                        doTexto(it)
                    }
                }
                refsTaxon = [] // limpar da memoria
                if (refsRegistrosOcorrencias) {
                    br()
                    br()
                    doSubtopic('Referências dos Registros')
                    refsRegistrosOcorrencias.eachWithIndex { it, index ->
                        br()
                        doTexto(it)
                    }
                }
            }


            if ( config.adAvaliacao == 'S' ) {
                document.newPage()
                doTopic('Avaliação')
                br()
                /** ****************************************************************/
                // CONTEUDO DAS ABAS 11.1 A 11.4
                doSubtopic('Aplicação dos critérios')
                Table table = new Table(1)
                table.setBorderWidth(0)
                table.setOffset(0)
                table.setWidth(100)
                table.setComplete(true)
                table.setSpacing(0)
                table.setPadding(5)
                Cell cell = new Cell();

                Paragraph p = new Paragraph()
                //----------------------------
                p.setFont(textBoldFont)
                p.add("Distribuição e taxonomia")
                cell.add(p)
                cellAddSNDChunk(cell, 'a) Ocorrência marginal no Brasil')
                cellAddSNDChunk(cell, 'b) Espécie elegível para avaliação nacional')
                cellAddSNDChunk(cell, 'c) Limitação taxonômica para avaliação')
                cellAddSNDChunk(cell, 'd) Conhecida apenas da localidade tipo ou poucas localidades')
                cellAddSNDChunk(cell, 'e) Região de ocorrência bem amostrada')
                cellAddSNDChunk(cell, 'f) Extensão da ocorrência(EOO)', '[                 km² ]')
                cellAddSNDChunk(cell, 'g) Tendência da EOO', '(  )Declinando   (  )Aumentando   (  )Estável   (  )Desconhecida')
                cellAddSNDChunk(cell, 'h) Flutuação Extrema na EOO')
                cellAddSNDChunk(cell, 'i) Área de Ocupação(AOO)', '[                  km² ]')
                cellAddSNDChunk(cell, 'j) Tendência da AOO', '(  )Declinando   (  )Aumentando   (  )Estável   (  )Desconhecida')
                cellAddSNDChunk(cell, 'k) Flutuação Extrema na AOO')
                cellAddSNDChunk(cell, 'l) Explicação (EOO/AOO)', '\n\n')

                p = new Paragraph()
                //------------------------------------------------
                p.setFont(textBoldFont)
                p.add("População")
                cell.add(p)
                cellAddSNDChunk(cell, 'a) Tempo geracional', '[                           ]')
                cellAddSNDChunk(cell, 'b) Método de cálculo', '\n\n')
                cellAddSNDChunk(cell, 'c) Redução populacional passada com causas reversíveis, compreendidas ou cessadas', '[         %]')
                cellAddSNDChunk(cell, '\tTipo de Dados', '(   )Observado   (   )Estimado   (   )Projetado   (   )Inferido   (   )Suspeitado')
                cellAddSNDChunk(cell, 'd) Redução populacional passada com causas não reversíveis, não compreendidas ou não\n\tcessadas', '[            %]')
                cellAddSNDChunk(cell, '\tTipo de Dados', '(   )Observado   (   )Estimado   (   )Projetado   (   )Inferido   (   )Suspeitado')
                cellAddSNDChunk(cell, 'e) Redução populacional futuras', '[         %]')
                cellAddSNDChunk(cell, '\tTipo de Dados', '(   )Observado   (   )Estimado   (   )Projetado   (   )Inferido   (   )Suspeitado')
                cellAddSNDChunk(cell, 'f) Redução populacional passada e futura', '[         %]')
                cellAddSNDChunk(cell, '\tTipo de Dados', '(   )Observado   (   )Estimado   (   )Projetado   (   )Inferido   (   )Suspeitado')
                cellAddSNDChunk(cell, 'g) Declínio populacional baseado em', '(   ) (a) Observação Direta\n\t\t(   ) (b) Índice de abundância apropriado para o táxon\n\t\t(   ) (c) Declínio na área de ocupação (AOO), extensão de ocorrência (EOO) ou qualidade\n\t\tdo habitat\n\t\t(   ) (d) Níveis reais ou potenciais de exploração\n\t\t(   ) (e) Efeitos de taxon introduzidos, hibridação, patógenos, poluentes, competidores\n\tou parasitas')
                cellAddSNDChunk(cell, 'h) % e período do declínio populacional', '\n\t\t(   )10% 10 anos ou 3 gerações\n\t\t(   )20% em 5 anos ou 2 gerações\n\t\t(   )25% em 3 anos ou 1 geração')
                cellAddSNDChunk(cell, 'i) População severamente fragmentada')
                cellAddSNDChunk(cell, 'j) Nº de indivíduos maduros', '[                ]')
                cellAddSNDChunk(cell, 'k) Tendência do nº de indivíduos maduros', '\n\t\t(  )Declinando   (  )Aumentando   (  )Estável   (  )Desconhecida')
                cellAddSNDChunk(cell, 'l) Flutuação extrema do nº de indivíduos maduros')
                cellAddSNDChunk(cell, 'm) Nº de subpopulação', '[              ]')
                cellAddSNDChunk(cell, 'n) Tendência do nº de subpopulação', '\n\t\t(  )Declinando   (  )Aumentando   (  )Estável   (  )Desconhecida')
                cellAddSNDChunk(cell, 'o) Flutuação extrema do nº de subpopulação')
                cellAddSNDChunk(cell, 'p) Nº Max de indivíduos em cada subpopulação', '[                    ]')
                cellAddSNDChunk(cell, 'q) % de indivíduos em cada subpopulação', '[             %]')


                p = new Paragraph()
                //------------------------------------------------
                p.setFont(textBoldFont)
                p.add("\nAmeaças e análise quantitativa")
                cell.add(p)
                cellAddSNDChunk(cell, 'a) Nº de Localizações', '[                           ]')
                cellAddSNDChunk(cell, 'b) Tendência do número de localizações', '\n\t\t(  )Declinando   (  )Aumentando   (  )Estável   (  )Desconhecida')
                cellAddSNDChunk(cell, 'c) Flutuação extrema do nº de localizações')
                cellAddSNDChunk(cell, 'd) Tendência da qualidade do habitat', '\n\t\t(  )Declinando   (  )Aumentando   (  )Estável   (  )Desconhecida')
                cellAddSNDChunk(cell, 'e) AOO ou número de localizações restritas sob ameaça futura plausível de levar a espécie\n\tà condição de CR ou EX em curto prazo')
                cellAddSNDChunk(cell, 'f) Probabilidade de Extinção no Brasil', '\n\t\t(   )Maior ou igual a 10% em 100 anos\n\t\t(   )Maior ou igual a 20% em 20 anos ou 5 gerações\n\t\t(   )Maior ou igual a 50% em 10 anos ou 3 gerações')
                cellAddSNDChunk(cell, 'b) Método de cálculo', '\n\n')

                p = new Paragraph()
                //------------------------------------------------
                p.setFont(textBoldFont)
                p.add("Situação regional")
                cell.add(p)
                cellAddSNDChunk(cell, 'a) Conectividade com populações de outros países', '\n\t\t(   )Imigração  (   )Emigração   (   )Imigração e Emigração\n\t\t(   )Mesma População   (   )Populações Isoladas   (   )Desconhecida')
                cellAddSNDChunk(cell, 'b) Tendência de imigração no brasil', '\n\t\t(  )Declinando   (  )Aumentando   (  )Estável   (  )Desconhecida')
                cellAddSNDChunk(cell, 'c) População Brasileira Pode Declinar')
                cellAddSNDChunk(cell, 'd) Observações', '\n\n')

                table.addCell(cell)
                document.add(table)
                br()
                /** ****************************************************************/

                doSubtopic('Categoria / critério', '')
                dadosApoioService.getTable('TB_CATEGORIA_IUCN').each {
                    if (it.codigoSistema == 'CR') {
                        doTexto('(   ) ' + it.descricaoCompleta + '\t\tPossivelmente extinta?     (   )Sim      (   )Não')
                    }else if (it.codigoSistema == 'EN') {
                        doTexto('(   ) ' + it.descricaoCompleta + '\t\t\t|')
                    }else if (it.codigoSistema == 'NT') {
                        doTexto('(   ) ' + it.descricaoCompleta + '\t\t|')
                    }else if (it.codigoSistema == 'VU') {
                        doTexto('(   ) ' + it.descricaoCompleta + '\t\t\t>\tCritério [' + (' ' * 56) + ']')
                    }else {
                        doTexto('(   ) ' + it.descricaoCompleta)
                    }

                }
                br()
                doSubtopic('Justificativa', '\n\n')

                //br()
                //doSubtopic( 'Critério','['+(' '*100)+']')
                br()
                List listMotivoMudanca = dadosApoioService.getTable('TB_MUDANCA_CATEGORIA')
                doSubtopic('Motivo(s) mudança categoria', '')
                listMotivoMudanca.each {
                    doTexto('(   ) ' + it.descricao)
                }
                br()
                doSubtopic('Justificativa da mudança de categtoria', '')

            }

            if (config.adColaboracao == 'S') {
                document.newPage()
                doTopic('Colaborações')
                br()
                Table tablex = doTable(['Campo', 'Colaborador', 'Colaboração', 'Ref. Bib'], 0, [], textBoldFont)
                fichaService.getColaboracoes(ficha).each {
                    try {
                        doRow(tablex, [it?.dsRotulo,
                                       it?.usuario?.username,
                                       '<p>' + (it?.dsColaboracao ?: '') + '</p>',
                                       '<p>' + (it?.dsRefBib ?: '') + '</p>',
                        ], [], null, textFont)

                    } catch (Exception e) {
                        doRow(tablex, [it.dsRotulo,
                                       it?.usuario?.username,
                                       '<p>Conteúdo da colaboração possui formatação inválida. Código do registro é: ' + it.id + '</p>',
                                       '<p>' + it?.dsRefBib + '</p>',
                        ], [], null, textFont)

                    }
                }
                document.add(tablex)
            }

            // OFICINA DE VALIDACAO
            if ( config.adValidacao == 'S' && config.sqOficina ) {

                document.newPage()

                doTopic('Validação')
                br()

                doSubtopic('Resultado da Avaliação', '')
                //doTexto('Categoria: ' + ficha.categoriaIucnText)
                doRvBold('Categoria: ', ficha.categoriaIucnText)
                if( ficha.dsCriterioAvalIucn) {
                    //doTexto('Criterio: ' + ficha.dsCriterioAvalIucn)
                    doRvBold('Critério: ', ficha.dsCriterioAvalIucn )
                }
                //doTexto('Justificativa: ' + ficha.dsJustificativa )
                doRvBold('Justificativa:', '' )
                doTexto(ficha.dsJustificativa )
                br()
                br()

               // ler os validadores
               JSONObject objJson = ficha.getValidadoresJson( config.sqOficina.toLong() )

               for ( item in objJson) {
                   doSubtopic('Resposta do(a) Validador(a): '+item.key, '')
                   doLinha()
                   br()
                   doTexto('(   ) A) Aceito a categoria, o critério e a justificativa')
                   doTexto('(   ) B) Aceito a categoria e o critério, com ajustes na justificativa')
                   doTexto('(   ) C) Não aceita a categoria e/ou o critério')
                   Table table = doTable(['Justificativa: '+item.key])
                   doRow(table, ['\n\n\n'],['left'])
                   document.add(table)
                   br()
                   br()
                   br()

               }
                Table table = doTable(['Pendências'])
                doRow(table, ['\n\n\n'],['left'])
                document.add(table)
                br()
                br()
                table = doTable(['Observações'])
                doRow(table, ['\n\n\n'],['left'])
                document.add(table)
            }

            // fim
            document.close()
        }
        catch (Exception e) {
            println 'Erro RtfFicha(): ' +  e.getMessage()
            //e.printStackTrace()
            fileName = ''
            if( throwError ) {
                throw e;
            }
        }
        return fileName
    }

    private String trim(def texto = null){
        texto = texto.toString().replaceAll(/^<p>&nbsp;<\/p>|<p>&nbsp;<\/p>$/, '').replaceAll(/<p><\/p>/,'')
        if (!texto || texto == 'null')
        {
            return '' // se não retornar espaço da erro no Phrase
        }
        return texto
    }
    private void br(){
        document.add( new Chunk(Chunk.NEWLINE))
    }
    private void doLinha(){
        /*if( texto )
        {
            texto = """{\\pard ${texto}\\par}"""
        } else {
            texto =''
        }*/
        //String dc = """{\\rtf1{${texto}\\pard {\\*\\do\\dobxcolumn\\dobypara\\dodhgt\\dpline\\dpxsize9800\\dplinesolid\\dplinew30}\\par}}"""
        String dc = """{\\pard \\brdrb \\brdrs\\brdrw10\\brsp20 {\\fs4\\~}\\par \\pard}"""
        RtfDirectContent rdc = new RtfDirectContent( dc )
        document.add( rdc )
    }
    private void initDoc(){
        pageSize = new Rectangle(PageSize.A4);
        document = new Document(pageSize,36,36,46,36);
        writer = RtfWriter2.getInstance(document, new FileOutputStream( fileName ) )
        document.addAuthor("ICMBIo");
        document.addSubject("Sistema de Avaliação - SALVE")
        document.open()

        // criar as fontes utilizadas nos textos
        // fonte do cabecalho
        headerFont          = new RtfFont("Times New Roman", 14, Font.BOLD, Color.BLACK);
        headerSubtitle      = new RtfFont("Times New Roman", 13, Font.BOLD, Color.BLACK);

        // fonte do rodape
        footerFont          = new RtfFont("Times New Roman", 10, Font.NORMAL, Color.GRAY);
        footerFontSmall     = new RtfFont("Times New Roman", 5, Font.NORMAL, Color.GRAY);

        // fonte dos titulos, tópicos relevantes
        titleFont           = new RtfFont("Times New Roman", 12, Font.BOLD, Color.BLACK);

        // Fonte normal dos textos
        textFont          = new RtfFont("Times New Roman", 12, Font.NORMAL, Color.BLACK);
        textFont6          = new RtfFont("Times New Roman", 6, Font.NORMAL, Color.BLACK);
        textFont10        = new RtfFont("Times New Roman"   , 10, Font.NORMAL, Color.BLACK);
        textItalicFont    = new RtfFont("Times New Roman", 12, Font.ITALIC, Color.BLACK);
        textBoldFont      = new RtfFont("Times New Roman", 12, Font.BOLD, Color.BLACK);
        creditsFont       = new RtfFont("Times New Roman", 8, Font.NORMAL, Color.BLACK);
        linkFont          = new RtfFont("Times New Roman", 10, Font.NORMAL, Color.BLUE);

        doHeaderAndFooter()
    }
    private doHeaderAndFooter(){
        String dataHoraEmissao =  new Date().format('dd/MM/yyyy HH:mm:ss')
        String textoRodape = 'Emitido em'
        if( ficha && ficha.situacaoFicha.codigoSistema != 'PUBLICADA') {
                textoRodape ='Ficha não publicada, emitida em'
        }
        textoRodape += ' ' + dataHoraEmissao + ' - ' + unidade + ' - pág:'


        Paragraph par = new Paragraph('',headerFont)
        par.setLeading(15f,0) // não tem efeito no espacamento

        Chunk chunk

        //TODO - IMPLEMENTAR FUNCOES SET/GET PARA TITULO, SISTEMA E LOGO DO ESTADO
        /*
        // logo
        Image img = Image.getInstance('/data/salve-estadual/arquivos/logo.png' )
        img.scaleAbsolute(50,55)

        Chunk chunkLogo = new Chunk( img,0f,0f )
        par.add( chunkLogo )

        // titulo
        chunk = new Chunk('\nInstituto Chico Mendes de Conservação da Biodiversidade - ICMBio',headerFont)
        par.add( chunk )
        */

        // subtitulo
        chunk = new Chunk('\nSistema de Avaliação do Risco de Extinção da Biodiversidade',headerSubtitle)
        par.add( chunk )

        // cabecalho
        HeaderFooter header = new HeaderFooter(par, false)
        header.setAlignment(Element.ALIGN_CENTER)
        document.setHeader(header)

        //Phrase frase = new Phrase()
        //Chunk c2 = new Chunk('Emitido por ' + unidade + (' '*10)+'Página: ', footerFont )

        // rodape
        Paragraph firstPageOnlyFooter = new Paragraph(msgDiferencaCategorias,footerFontSmall);
        firstPageOnlyFooter.setAlignment(Element.ALIGN_CENTER);

        Paragraph allFooters = new Paragraph(textoRodape,footerFont);
        allFooters.add(new RtfPageNumber());
        allFooters.setAlignment(Element.ALIGN_CENTER);

        // criar grupo de rodapes
        RtfHeaderFooterGroup footerGroup = new RtfHeaderFooterGroup();
        if( msgDiferencaCategorias ) {
            firstPageOnlyFooter.add( new Chunk(Chunk.NEWLINE ) )
            firstPageOnlyFooter.add(new Chunk(textoRodape,footerFont) );
            firstPageOnlyFooter.add( new RtfPageNumber() );
            footerGroup.setHeaderFooter(new RtfHeaderFooter(firstPageOnlyFooter), RtfHeaderFooter.DISPLAY_FIRST_PAGE);
        }
        footerGroup.setHeaderFooter(new RtfHeaderFooter( allFooters ), RtfHeaderFooter.DISPLAY_ALL_PAGES );

        // Set the document footer
        document.setFooter( footerGroup );

        /*
        if( this.msgDiferencaCategorias ) {
            frase.add( new Chunk(this.msgDiferencaCategorias,footerFontSmall ) )
            frase.add( new Chunk(Chunk.NEWLINE ) )
        }
        frase.add( c2 )
        // Phrase frase = new Phrase('Emitido por ' + unidade + (' '*10)+'Página: ', footerFont )
        HeaderFooter footer = new HeaderFooter(frase,new Phrase((' '*10)+"Em: "+dataImpressao))
        footer.setAlignment( Element.ALIGN_CENTER)
        document.setFooter(footer)
         */
    }
    private void doTitle() {
        String strTitle         = "" // normal
        String strNoCientifico  = ficha.taxon.noCientifico // italico
        String strNoAutor       = ficha?.taxon?.noAutor ?: '' // normal
        Chunk chunk

        // calcular tamanho da fonte para caber o titulo +  cientifico + autor na mesma linha
        BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1252,false)
        int width = pageSize.getWidth() - ( document.leftMargin() + document.rightMargin() );
        float measureWidth = 1
        float fontSize = 0.1f
        while(measureWidth < width){
            measureWidth = bf.getWidthPoint(strTitle+' '+strNoCientifico+' '+strNoAutor, fontSize)
            fontSize += 0.1f
        }
        if( fontSize > 18f )
        {
            fontSize = 18f
        }
        if( fontSize < 12f )
        {
            fontSize = 12f
        }
        // fonte com tamanho variável de acordo com o tamanho do titulo
        RtfFont fonteTemp = new RtfFont("Times New Roman", fontSize, Font.BOLD, Color.BLACK);

        Paragraph par  = new Paragraph('',fonteTemp)
        //par.setAlignment( Element.ALIGN_CENTER )


        // titulo
        chunk =  new Chunk (strTitle)
        par.add( chunk )

        // nome científico
        chunk = new Chunk (' ' + strNoCientifico)
        chunk.getFont().setStyle(Font.BOLDITALIC)
        chunk.getFont().setColor(0,0,0)
        par.add( chunk )

        // autor do Taxon
        chunk.getFont().setStyle(Font.BOLD) // remover italico
        chunk.getFont().setColor(0,0,0) // remover cor
        chunk = new Chunk(' ' + strNoAutor,fonteTemp)
        par.add( chunk )

        // colocar o paragrafo dentro de uma tabela para colorir o fundo
        Table table = new Table(1)
        table.setBorderWidth(0)
        table.setWidth(100)
        table.setPadding(8)
        Cell cell = new Cell(par)
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE)
        cell.setBackgroundColor(corTopicos)
        cell.setBorderWidth(0)
        table.addCell(cell )
        document.add( table )

    }
    private void doTopic(String texto, Color corTexto= corTopicos, Font fonte) { doTopic( texto , corTexto, textFont )}
    private void doTopic(String texto , Color corTexto= corTopicos, RtfFont fonte = titleFont ) {
        //println 'Iniciando topico ' + texto
        texto = Util.stripTags( texto?:'' )
        // titulo
        Chunk chunk = new Chunk(texto, fonte)
        Table table = new Table(1)
        table.setWidth(100)
        table.setPadding(5)
        Cell cell = new Cell(chunk)
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE)
        cell.setBackgroundColor(corTexto)
        cell.setBorderWidth(0)
        table.addCell(cell )
        document.add( table )
/*        Paragraph p = new Paragraph('',fonteTopicos)
        p.setAlignment(Element.ALIGN_CENTER)
        Chunk chunk = new Chunk (texto,fonteTopicos)
        chunk.setBackground(corTopicos )
        p.add( chunk )
        document.add( p )
  */
    }
    private void doTopicCategoria(String categoria , String dataAvaliacao, String justificativa ) {
        Chunk chunkLabel = new Chunk('Categoria: ', textBoldFont)
        Chunk chunkCategoria = new Chunk(categoria ?: '', textFont)
        Chunk chunkDataAvaliacao = new Chunk(dataAvaliacao ?: '',textFont10)
        Paragraph paragraph = new Paragraph()
        paragraph.add( chunkLabel )
        paragraph.add( chunkCategoria )
        if( dataAvaliacao ) {
            paragraph.add(Chunk.NEWLINE)
            paragraph.add(chunkDataAvaliacao)
        }
        Table table = new Table(1)
        table.setWidth(100)
        table.setPadding(5)
        Cell cell = new Cell(paragraph)
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE)
        cell.setBackgroundColor(corTopicos)
        cell.setBorderWidth(0)
        table.addCell(cell )
        br()
        document.add( table )
        if( justificativa ) {
            br()
            doSubtopic('Justificativa', justificativa)
        }
    }
    private void doClassificacaoTaxonomica( Map arvoreTaxonomica = [:], Map imagemPrincipal = [:] ) {
        doTopic('Classificação Taxonômica')
        String nomeCientifico = arvoreTaxonomica.nmEspecie
        Table table = new Table( 2 )
        table.setWidth( 100f )
        table.setBorder(Rectangle.NO_BORDER)
        table.setPadding( 5f)
        table.setOffset(0)
        table.setComplete(true)
        table.setSpacing(0)
        table.setWidths( (Float[]) [1.0f, 2f] )
        Cell cellEsquerda = new Cell()
        cellEsquerda.setBorder( Rectangle.NO_BORDER )
        cellEsquerda.setVerticalAlignment( Element.ALIGN_MIDDLE )
        cellEsquerda.setHorizontalAlignment( Element.ALIGN_CENTER)
        cellEsquerda.addElement( new Paragraph('Filo: '+ arvoreTaxonomica.nmFilo,textFont ))
        cellEsquerda.addElement( new Paragraph('Classe: '+ arvoreTaxonomica.nmClasse,textFont ))
        cellEsquerda.addElement( new Paragraph('Ordem: '+ arvoreTaxonomica.nmOrdem,textFont ))
        cellEsquerda.addElement( new Paragraph('Família: '+ arvoreTaxonomica.nmFamilia,textFont ))
        //cellEsquerda.addElement( new Paragraph('Gênero: '+ arvoreTaxonomica.nmGenero,textFont ))
        Chunk chunkLabelGenero = new Chunk('Gênero: ', textFont)
        Chunk chunkItalicoGenero = new Chunk(arvoreTaxonomica.nmGenero, textItalicFont)
        Paragraph tempParagrafoGenero = new Paragraph()
        tempParagrafoGenero.add( chunkLabelGenero )
        tempParagrafoGenero.add( chunkItalicoGenero )
        cellEsquerda.addElement( tempParagrafoGenero )

        // os nomes da espécie / subespecie devem ser em italico
        if( arvoreTaxonomica.nmSubespecie ){
            nomeCientifico = arvoreTaxonomica.nmSubespecie
            cellEsquerda.addElement( new Paragraph('Espécie: '+ arvoreTaxonomica.nmEspecie,textFont))
            br()
            Chunk chunkLabel = new Chunk('Subespécie: ', textFont)
            Chunk chunkItalico = new Chunk(arvoreTaxonomica.nmSubespecie, textItalicFont)
            Paragraph tempParagrafo = new Paragraph()
            tempParagrafo.add( chunkLabel )
            tempParagrafo.add( chunkItalico )
            cellEsquerda.addElement( tempParagrafo )
        } else {
            Chunk chunkLabel = new Chunk('Espécie: ', textFont)
            Chunk chunkItalico = new Chunk(arvoreTaxonomica.nmEspecie, textItalicFont)
            Paragraph tempParagrafo = new Paragraph()
            tempParagrafo.add( chunkLabel )
            tempParagrafo.add( chunkItalico )
            cellEsquerda.addElement( tempParagrafo )
        }
        cellEsquerda.setUseAscender( true )
        cellEsquerda.setHorizontalAlignment( Element.ALIGN_LEFT )
        table.addCell( cellEsquerda )

        //************************************************************************
        // FOTO com autor e legenda da foto
        //************************************************************************
        Cell cellDireita = new Cell()
        cellEsquerda.setUseAscender( true )
        cellDireita.setHorizontalAlignment( Element.ALIGN_RIGHT )
        if( imagemPrincipal ) {
            String credits = '';
            if( imagemPrincipal.autor ) {
                credits = 'Autor: ' + imagemPrincipal.autor + '   '
            }
            if( imagemPrincipal.legenda && imagemPrincipal.legenda.toLowerCase().trim() != nomeCientifico.toLowerCase().trim() ) {
                credits += '\n' + imagemPrincipal.legenda
            }

            // SUBTABELA com foto e autor
            Image img = Image.getInstance(imagemPrincipal.localArquivo )
            img.scaleToFit(300,280)
            cellDireita.add( img )
            Paragraph pCreditos = new Paragraph( 6.5f, credits, textFont6 )
            pCreditos.setAlignment( Element.ALIGN_RIGHT )
            cellDireita.add( pCreditos)
            cellDireita.setBorder( Rectangle.NO_BORDER )
        }
        table.addCell( cellDireita )
        table.setBorder(Rectangle.NO_BORDER)
        document.add( table )
   }


    private void doSubtopic(String topico, String texto ='',boolean italic = false ) {
        // titulo
        Paragraph p = new Paragraph('', textBoldFont )
        p.setAlignment( Element.ALIGN_LEFT )
        Chunk c = new Chunk( topico )
        if( italic )
        {
            c.setFont(textItalicFont)
        }
        else
        {
            c.setFont( textBoldFont )
        }
        p.add( c )
        document.add( p )
        doTexto( texto )
    }
    /**
     * imprimir Rotulo: Valor
     * @param rotulo
     * @param valor
     */
    private void doRv(String rotulo, String valor){
        Paragraph px = new Paragraph('',textFont)
        Chunk cx = new Chunk(rotulo,textFont)
        px.add( cx )
        cx = new Chunk( ( valor ? valor + '.':''), textFont)
        px.add( cx )
        document.add( px )
    }
    private void doRvItalic(String rotulo, String valor){
        Paragraph px = new Paragraph('',textFont)
        Chunk cx = new Chunk(rotulo,textFont)
        px.add( cx )
        cx = new Chunk( ( valor ? valor + '.':''), textItalicFont)
        px.add( cx )
        document.add( px )
    }
    private void doRvBold(String rotulo, String valor,Cell cell=null){
        Paragraph px = new Paragraph('',textFont)
        Chunk cx = new Chunk(rotulo,textBoldFont)
        px.add( cx )
        cx = new Chunk((valor ? valor :''), textFont)
        px.add( cx )
        if( cell )
        {
            cell.add( px )
        }
        else
        {
            document.add(px)
        }
    }
    void doTexto( String texto ='', RtfFont fonte = textFont, String textAlign ='j' ) {
        if( texto == null || ! texto || texto == 'null' )
        {
            return
        }
        textAlign = textAlign.toLowerCase()
        texto = Util.trimEditor( texto )
        Paragraph paragrafo = new Paragraph('',fonte)
        if( textAlign == 'j' ){
            paragrafo.setAlignment(Element.ALIGN_JUSTIFIED_ALL)
        } else if( textAlign == 'c' ){
            paragrafo.setAlignment(Element.ALIGN_CENTER)
        } else if( textAlign == 'l' ){
            paragrafo.setAlignment(Element.ALIGN_LEFT)
        } else if( textAlign == 'r' ){
            paragrafo.setAlignment(Element.ALIGN_RIGHT)
        }

        if( texto.indexOf('</') == -1 )
        {
            // substituir htmlEntities &acute; etc...
            texto = Util.html2str( texto )
            paragrafo.add( texto )
            //paragrafo.setAlignment(Element.ALIGN_LEFT)
        }
        else {
            //paragrafo.setAlignment(Element.ALIGN_JUSTIFIED_ALL)
            /** /
            texto = Util.stripTags( texto )
            println texto
            println "-"*100
            paragrafo.add( texto )
            /**/
            /**/
            texto = Util.escape2Rtf( texto )
            RtfDirectContent rdc = new RtfDirectContent( texto )
            paragrafo.add( rdc )
            /**/
            //paragrafo.setAlignment(Element.ALIGN_JUSTIFIED_ALL)
        }
        document.add( paragrafo )
    }
    private String parseSnd(def SND = null) {
        switch (SND) {
            case 'S':
                return 'Sim'
                break
            case 'N':
                return 'Não'
            case 'D':
                return 'Desconhecido'
            default:
                return ''
        }
    }
    private Table doTable(List headers,Integer colSpan = 0, List widths=[], Font font ){
        doTable(headers,colSpan,widths=[], textFont)
    }
    private Table doTable(List headers,Integer colSpan = 0, List widths=[], RtfFont font=null){
        int cols = ( ( colSpan > 0 ) ? colSpan : headers.size() )
        Table table = new Table( cols )
        table.setBorderWidth(0)
        table.setOffset(0)
        table.setWidth(100)
        table.setComplete(true)
        table.setSpacing(0)
        table.setPadding(5)
        if( widths.size() > 0 )
        {
            table.setWidths((Float[])widths)
        }
        headers.eachWithIndex { it, i ->
            Chunk c = new Chunk(it, textBoldFont )
            Cell cell = new Cell(c)
            cell.setHeader(true)
            cell.setUseBorderPadding(false)
            cell.setHorizontalAlignment(Element.ALIGN_CENTER)
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE)
            cell.setBackgroundColor( corTopicos )
            if( colSpan > 0 )
            {
                cell.setColspan(colSpan)
            }
            table.addCell(cell)
        }
        return table
    }
    private void doRow( Table table, List data,List aligns=[], Color bgColor = null,Font font ){
        doRow( table, data,aligns, bgColor,textFont)
    }
    private void doRow( Table table, List data,List aligns=[], Color bgColor = null,RtfFont font=null){
        data.eachWithIndex{ it, i ->
            String texto = trim(it.toString())
            texto = texto.replaceAll('#NL#','\n')
            Chunk c = new Chunk(texto, ( font ?: textFont ) )
            Cell cell = new Cell(c)

            if( it.getClass() == com.lowagie.text.Paragraph || it.getClass() == com.lowagie.text.Chunk )
            {
                cell = new Cell( it )
            }

            if( bgColor )
            {
                cell.setBackgroundColor( bgColor )
            }

            if( aligns[i] )
            {
                if( aligns[i] == 'center')
                {
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER)
                }
                else if( aligns[i]=='left')
                {
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT)
                }
                else if( aligns[i]=='justify')
                {
                    cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED)
                }
                else if( aligns[i]=='right')
                {
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT)
                }
            }
            else
            {
                cell.setHorizontalAlignment(Element.ALIGN_CENTER)
            }
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE)

            if( texto.indexOf('</') > -1 )
            {
                Paragraph valueToParagragh = new Paragraph();
                valueToParagragh.setFont( textFont )
                // https://coderanch.com/how-to/javadoc/itext-2.1.7/com/lowagie/text/Paragraph.html
                valueToParagragh.setFirstLineIndent(0f)
                texto = Util.escape2Rtf( texto )
                RtfDirectContent rdc = new RtfDirectContent( texto )
                rdc.setInTable(true)
                cell = new Cell( rdc )
                cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED_ALL)
            }
            table.addCell(cell)
        }
    }
    private void doImage( String imagePath = null, String legenda = null ){
        if ( ! imagePath || !(imagePath =~ /(?i)(jpe?g|png|bmp)$/) ) {
            return
        }
        try {
            Image img = Image.getInstance(imagePath )
            // verificar se a imagem cabe na página
            Float leftMargin = document.leftMargin()
            Float rightMargin = document.rightMargin()
            Float maxH = pageSize.getHeight() / 2
            Float maxW = pageSize.getWidth() - (leftMargin + rightMargin)
            Float imgH = img.getHeight()
            Float imgW = img.getWidth()
            if (maxH && imgH > maxH) {
                imgH = maxH
            }
            if (maxW && imgW > maxW) {
                imgW = maxW
            }
            img.scaleToFit(imgW, imgH)
            Chunk chunk = new Chunk( img, 0, 0)
            Paragraph p1 = new Paragraph(' ')
            p1.setAlignment(Element.ALIGN_CENTER)
            p1.add(chunk)
            br()
            document.add(p1)
            /*
            // não é para sair a legenda no doc - reunião dia 29/06/2022
            if (legenda) {
                Paragraph p2 = new Paragraph(' ', textFont)
                p1.setAlignment(Element.ALIGN_LEFT)
                chunk = new Chunk('Legenda: ' + legenda, textFont)
                p2.add(chunk)
                document.add(p2)
            }*/
        } catch(Exception e ){
            println 'Erro exibir a imagem '+imagePath+' na ficha em RTF'
            println e.getMessage()
        }
    }
    private void cellAddSNDChunk( Cell cell=null, String text='', String snd = '(  )Sim  (  )Não  (  )Desconhecido' ){
        Paragraph p = new Paragraph('',textFont)
        p.setLeading(18f,18f)
        Chunk chunk = new Chunk('\t'+text+': ',textItalicFont)
        p.add( chunk )
        chunk = new Chunk(snd)
        chunk.setFont(textFont)
        p.add( chunk )
        cell.add( p )
    }
    private String convertToRTF(String htmlStr) {

        OutputStream os = new ByteArrayOutputStream();
        HTMLEditorKit htmlEditorKit = new HTMLEditorKit();
        RTFEditorKit rtfEditorKit = new RTFEditorKit();
        String rtfStr = null;
        htmlStr = htmlStr.replaceAll("<br.*?>","#NEW_LINE#");
        htmlStr = htmlStr.replaceAll("</p>","#NEW_LINE#");
        htmlStr = htmlStr.replaceAll("<p.*?>","");
        InputStream is = new ByteArrayInputStream(htmlStr.getBytes());
        try {
            javax.swing.text.html.HTMLDocument doc = htmlEditorKit.createDefaultDocument();
            htmlEditorKit.read(is, doc, 0);
            rtfEditorKit.write(os, doc, 0, doc.getLength());
            rtfStr = os.toString();
            rtfStr = rtfStr.replaceAll("#NEW_LINE#","\\\\par ");

        } catch (IOException e) {
            e.printStackTrace();
        } catch (BadLocationException e) {
            e.printStackTrace();
        }
        return rtfStr;
    }
    private void marcaDagua()
    {
        return;
        Paragraph paragrafo = new Paragraph('',textFont)
        String texto = ''

        RtfDirectContent rdc = new RtfDirectContent( texto )
        //paragrafo.add( rdc )
        //paragrafo.setAlignment(Element.ALIGN_JUSTIFIED_ALL)
        document.add( rdc )
    }

    /**
     * Alterar a virgula por ponto
     * @param valor
     * @return
     */
    String valorComVirgula( String valor = '' ){
        valor = valor.replaceAll(/\.0$/, '')
        return valor.replaceAll(/\./,',')
    }
    String valorComVirgula( Double valor ){
        return valorComVirgula( valor ? valor.toString() :'' )
    }
}
