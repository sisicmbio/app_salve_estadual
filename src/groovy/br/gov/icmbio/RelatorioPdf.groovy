package br.gov.icmbio

import com.itextpdf.text.BaseColor
import com.itextpdf.text.Document
import com.itextpdf.text.Element
import com.itextpdf.text.Font
import com.itextpdf.text.PageSize
import com.itextpdf.text.Paragraph
import com.itextpdf.text.Phrase
import com.itextpdf.text.Rectangle
import com.itextpdf.text.pdf.PdfPCell
import com.itextpdf.text.pdf.PdfPTable
import com.itextpdf.text.pdf.PdfWriter

class RelatorioPdf
{
    private tmpDir
    private fileName
    private unidadeSigla
    private unidade
    private agora
    private titulo
    private subTitulo

    private PdfWriter writer
    private Document document
    private BaseColor colorHeader = new BaseColor(211, 224, 198) // verde claro

    private Font fontBase = new Font(Font.FontFamily.TIMES_ROMAN, 12);
    private Font fontGrande = new Font(Font.FontFamily.TIMES_ROMAN, 14);



    RelatorioPdf( String tmpDir, String unidade, String unidadeSigla )
    {
        this.tmpDir = tmpDir
        this.unidade = unidade
        this.unidadeSigla = unidadeSigla
        this.agora = new Date().format('dd-MM-yyyy-hh-mm-ss')
        // para colocar cor de fundo é preciso da classe Rectangle
        Rectangle pageSize = new Rectangle(PageSize.A4);
        pageSize.setBackgroundColor(new BaseColor(255, 255, 255))
        document = new Document(pageSize);
        document.setMargins(/*left*/36,/*right*/ 36,/*top*/ 44,/*bottom*/ 36);

    }
    private void start()
    {
        try
        {
            writer = PdfWriter.getInstance(this.document, new FileOutputStream( this.fileName ) )
            HeaderFooterBase headerFooter = new HeaderFooterBase()
            headerFooter.setAutor(this.unidade)
            if( titulo )
            {
                headerFooter.setTitulo(this.titulo)
            }
            headerFooter.setSubTitulo(this.subTitulo)
            //headerFooter.setMarcaDagua('NÃO PUBLICADA')
            writer.setPageEvent( headerFooter )
            document.open()
        }
        catch (Exception e)
        {
            fileName=''
            println e.getMessage()
        }
    }

    /**
     * Fazer a impressão dos cabeçalhos das tabelas em negrito com fundo preenchido
     * @param table
     * @param headers
     * @param bgColor
     * @param colWidths
     */
    void drawHeader(PdfPTable table, ArrayList headers, BaseColor bgColor, ArrayList colWidths)
    {
        //table = new PdfPTable()
        table.resetColumnCount(colWidths.size())
        table.setWidths((Float[]) colWidths);
        table.setHeaderRows(1) // repetir o cabecalho se a tabela passar para a página seguinte
        // table.setSkipFirstHeader(true) // quando for preciso remover os titulos
        headers.each {
            PdfPCell cell = new PdfPCell(new Phrase(it))
            cell.setPadding(5)
            cell.setUseAscender(true) // posicionar corretamente o texto no centro verticalmente
            cell.setVerticalAlignment(PdfPCell.ALIGN_MIDDLE)
            cell.setHorizontalAlignment(Element.ALIGN_CENTER)
            cell.setBackgroundColor(bgColor)
            table.addCell(cell)
        }
    }


    /**
     * Criar uma celula da tabela com as formatações padrão
     * @param text
     * @return
     */
    PdfPCell getCell(def text = null, def align = null)
    {   // text = trim( text )
        if (! text || text== 'null')
        {
            text = ''
        }
        PdfPCell cellTemp = new PdfPCell( new Phrase( text ) )
        align = align ? align.toLowerCase() : 'l'
        switch( align )
        {
            case 'c':
                cellTemp.setHorizontalAlignment (Element.ALIGN_CENTER)
                break;
            case 'r':
                cellTemp.setHorizontalAlignment (Element.ALIGN_RIGHT)
            case 'j':
                cellTemp.setHorizontalAlignment (Element.ALIGN_JUSTIFIED_ALL)
            default:
                cellTemp.setHorizontalAlignment (Element.ALIGN_LEFT)
        }
        cellTemp.setBackgroundColor(BaseColor.WHITE)
        cellTemp.setPadding(5)
        cellTemp.setUseAscender(true)
        cellTemp.setUseDescender(true)
        cellTemp.setVerticalAlignment(Element.ALIGN_MIDDLE)
        return cellTemp
    }

    /**
     * criar linha em branco
     */
    void br()
    {
        addP(' ',fontBase);
    }

    /**
     * adicionar paragrafo
     * @param text
     * @param font
     * @param alignment
     */
    void addP(def text = null, Font font = fontBase, String alignment = 'l')
    {
        if( !text || text == 'null' )
        {
            return
        }
        text = trim( text )
        text = text.replaceAll(/null/, '')
        Paragraph p = new Paragraph(text, font)
        switch (alignment.toLowerCase())
        {
            case 'r':
                p.setAlignment(Element.ALIGN_RIGHT)
                break
            case 'c':
                p.setAlignment(Element.ALIGN_CENTER)
                break
            default:
                p.setAlignment(Element.ALIGN_LEFT)
        }
        p.setLeading(13) // line height
        document.add(p)
    }

    /**
     * Remover os parágrafos e espaços em branco do início e final do texto
     * @param text
     * @return
     */
    String trim(def text = null)
    {
        text = text.toString().replaceAll(/^<p>&nbsp;<\/p>|<p>&nbsp;<\/p>$/, '').replaceAll(/<p><\/p>/,'')
        if (!text || text == 'null')
        {
            return '' // se não retornar espaço da erro no Phrase
        }
        return text
    }

    /**
     * Relatório dos Pontos Focais e Coordenadores de Taxon por ciclo
     *
     * @param sqCicloAvaliacao
     * @return
     */

    String run001(String sqCicloAvaliacao = '', String sqPapel = '',CicloAvaliacao ciclo=null, List rows=[] )
    {
        ArrayList headers
        PdfPTable table = new PdfPTable()
        this.fileName = this.tmpDir + 'rel-pf-ct-'+this.agora+'.pdf'
        this.titulo = 'Relatório de Pontos Focais e/ou Coordenadores de Taxon'
        this.subTitulo = ciclo.deCicloAvaliacao
        this.start()
        try
        {
            headers = ['Nome', 'Papel', 'Unidade', 'Fichas']
            table = new PdfPTable()
            table.setSpacingBefore(5)
            table.setWidthPercentage(100)
            table.setHorizontalAlignment(Element.ALIGN_CENTER)
            drawHeader(table, headers, colorHeader, [2.0f, 2.0f, 2.0f, 1.0f])
            if( rows ) {
                rows.each { row ->
                    table.addCell(getCell(Util.capitalize(row.no_pessoa)))
                    table.addCell(getCell(row.no_papel, 'C'))
                    table.addCell(getCell(row.sg_unidade_org, 'C'))
                    table.addCell(getCell(row.nu_fichas.toString(), 'C'))
                }
            }
            else {
                br()
                br()
                addP('Nenhum registro encontrado', fontGrande, 'C')
            }
            document.add(table)
            br()
            document.close()
        }
        catch (Exception e)
        {
            fileName=''
            println e.getMessage()
        }
        return fileName
    }
}
