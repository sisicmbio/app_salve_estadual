package br.gov.icmbio

import org.apache.poi.hssf.util.HSSFColor
import org.apache.poi.sl.usermodel.FillStyle

// Exemplos de utilização
// https://www.programcreek.com/java-api-examples/?api=org.apache.poi.xssf.streaming.SXSSFWorkbook
// https://www.roytuts.com/handling-large-data-writing-to-excel-using-sxssf-apache-poi/
/* para acompanhar o arquivo temporario sendo criado:
    watch -n0,1 "ls -lrtgGk /tmp/poifiles | tail"
*/


import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.CellStyle
import org.apache.poi.ss.usermodel.BorderStyle
import org.apache.poi.ss.usermodel.FillPatternType
import org.apache.poi.ss.usermodel.HorizontalAlignment
import org.apache.poi.ss.usermodel.IndexedColors
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.usermodel.VerticalAlignment
import org.apache.poi.ss.util.CellRangeAddress
import org.apache.poi.xssf.streaming.SXSSFSheet
import org.apache.poi.xssf.streaming.SXSSFWorkbook
import org.apache.poi.xssf.usermodel.DefaultIndexedColorMap
import org.apache.poi.xssf.usermodel.XSSFColor
import org.apache.poi.xssf.usermodel.XSSFFont
import org.apache.poi.xssf.usermodel.XSSFRichTextString
import javax.swing.text.Document
import javax.swing.text.html.HTMLEditorKit
import javax.swing.text.rtf.RTFEditorKit
import java.awt.Color

class PlanilhaExcel {
    def userJobService
    private WorkerService workerService
    private Map worker
    private Boolean debug = false
    private String fileName
    private SXSSFWorkbook workbook
    private SXSSFSheet sheet
    private String error
    private String citation
    private String title = ''
    private int currentRowNumber = -1
    //private Row row
    private List columns = []
    private List data = []
    private List criterias=[] // lista de filtros utilizados para gerar a planilha
    private String obs=''
    private XSSFFont fontNormal
    private XSSFFont fontItalic
    private XSSFFont fontItalicBold
    private long defaultFontSize

    /**
     * método construtor da classe
     * @param fileName - nome do arquivo em disco
     * @param sheetName - nome da planilha ( aba )
     */
    PlanilhaExcel( String fileName = '', String sheetName = 'Exportação salve', Boolean debug = false, String title = '' ) {
        this.debug = debug
        this.fileName = fileName
        this.columns = []

        // -1 turn off auto-flushing and accumulate all rows in memory
        // this.workbook = new SXSSFWorkbook(-1)
        this.workbook = new SXSSFWorkbook(100) // flush each 100 lines
        this.workbook.setCompressTempFiles(true)
        this.createSheet( sheetName )
        this.title = title ?: ''
        this.defaultFontSize = 11l

        // fontes padrão
        this.fontNormal = workbook.createFont()

        this.fontItalic = workbook.createFont()
        this.fontItalicBold = workbook.createFont()
        this.fontItalic.setItalic(true)
        this.fontItalicBold.setItalic(true)
        this.fontItalicBold.setBold(true)

        this.fontNormal.setBold(false)
        this.fontNormal.setItalic(false)
        this.fontItalic.setFontHeight(defaultFontSize)
        this.fontItalicBold.setFontHeight(defaultFontSize)
        this.fontNormal.setFontHeight(defaultFontSize)



        d(' ')
        d('-'*40)
        d('SALVE - Exportacao de dados para planilha')
        d('-'*40)
        d(' - inicio')
        d(' - arquivo saida: ' + fileName)
        d(' - planilha: ' + sheetName)
    }

    /**
     * método para imprimir logs no terminal quando for passado o parametro debug=true
     * @param text
     */
    private void d(String text ){
        if( debug ) {
            if( text.trim() != '' ) {
                String h = new Date().format('HH:mm:ss')
                println h + ':' + text
            } else {
                println ' '
            }
        }
    }

    /**
     * definir texto de observação
     * @param obs
     */
    void setObs( String obs ='') {
        this.obs = obs
    }

    void setCitation( String newCitation = '') {
        this.citation=newCitation
    }

    String getCitation() {
        return this.citation;
    }

    /**
     * adicionar critério/filtro utilizado para gerar a planilha
     * @example: addCriteria('Estado(s)','GO,DF')
     * @param title
     * @param value
     */
    List addCriteria(String title='', String value='') {
        if( title.trim() && value.trim() ){
            this.criterias.push([title:title,value:value]);
        }
        return this.criterias
    }
    /**
     * Definr o array de critérios utilizados para gerar a planilha
     * @param criterias
     * @return
     */
    List setCriteria( List newList = [] ) {
        this.criterias = newList
        return this.criterias
    }

    /**
     * retornar o array de critérios/filtros utilizados para gerar planilha
     * @return
     */
    List getCriterias() {
        return this.criterias
    }

    /**
     * gravar a planilha em disco
     */
    boolean save() {
        if( fileName ) {
            try {
                d(' - inicio Save()')
                FileOutputStream outputStream = new FileOutputStream( fileName )
                workbook.write(outputStream)
                workbook.dispose()
                workbook.close();
                sheet   = null
                workbook= null
                data    = null
                d(' - fim Save()')
                return true
            } catch ( Exception e ) {
                e.printStackTrace()
                error = e.getMessage()
                workbook = null
            }
        }
        return false
    }

    /**
     * criar uma aba na planilha
     * @param sheetName
     */
    void createSheet( String sheetName = '' ) {
        try {
            if (sheetName) {
                sheet = workbook.createSheet(sheetName)
                currentRowNumber=-1
                sheet.setZoom(110)
            }
        } catch ( Exception e ) {
            error = e.getMessage()
            println error
        }
    }

    /**
     * criar linha na planilha
     * @param rowNum
     * @return
     */
    Row nextRow() {
        if( ! sheet ) {
            createSheet('Planilha-1')
        }
       currentRowNumber++
       return sheet.createRow( currentRowNumber )
    }

    /**
     * definir o valor de uma célula da planilha na linha corrente
     * @param rowNum - numero da linha iniciado com Zero
     * @param colNum - número da coluna iniciado com Zero
     * @param value - valor a ser gravado na célula
     * @params spanColsFromTro = array com a coluna inicial e coluna final para apalicar o span
     * @return
     */
    Cell setCell( int colNum, String value, Row row, List spanColsFromTo ) {
        value = value ?: '(vazio)';
        Cell cell = setCell( colNum, value, row );
        CellRangeAddress region = new CellRangeAddress(currentRowNumber, currentRowNumber, spanColsFromTo[0], spanColsFromTo[1] );
        sheet.addMergedRegion(region);
        return cell
    }
    Cell setCell( int colNum, String value, Row row ) {
        Cell cell = row.createCell( colNum )
        try {
            value = value ?: '';

            // tranformar htmlentities em caracteres normais
            value = Util.html2str( value )

            if( value.indexOf('<') >-1 ) {

                value = value.replaceAll('\n','#LF#')

                // nao funcionou
                //value = value.replaceAll('<i>','#NEGS#')
                //value = value.replaceAll('</i>','#NEGE#')

                // tratamento nome cientifico
                int posStart = value.indexOf('<i>');
                int posEnd   = value.indexOf('</i>');

                // remover tags html
                value = Util.stripTags(value);
                value = value.replaceAll('#LF#','\n')

                // criar texto rico
                XSSFRichTextString textString = new XSSFRichTextString( value );

                // aplicar italico no trecho
                if( posStart == 0 && posEnd > posStart ) {
                    textString.applyFont( posStart, Math.min( value.length(), posEnd ), fontItalicBold )
                }
                cell.setCellValue( textString )
            } else {
                cell.setCellValue((String) value ?:'')
            }
        } catch ( Exception e ) {
            if( e.getMessage() ) {
                error = e.getMessage()
            }
        }
        return cell
    }

    /**
     * imprimir a instância da classe
     * @return
     */
    String toString() {
        return 'Classe: PlanilhaExcel ' + (fileName?:'')
    }

    /**
     * retorna o último error ocorrido
     * @return
     */
    String getError() {
        return error ?:''
    }

    /**
     * Adicionar coluna na planilha
     * Formato: [title:'Nome cientifico',column:'nm_cientifico']
     * @param column
     * @return
     */
    void addColumn( Map column = [:]) {
        column.bgColor = column.bgColor ?: '#FFFFFE'
        column.color = column.color ?: '#000000'
        if( column.title ) {
            column.width = column.width ? column.width : Math.max(20, Math.min(80, column.title.size() + 2 ) )
            this.columns.push( column )
        }
    }

    /**
     * retorna as colunas adicionadas na planilha
     * @return
     */
    List getColumns() {
        return this.columns
    }

    /**
     * definir o titulo da planilha (1ªlinha)
     */
    void setTitle(String newValue = '' ) {
        this.title = newValue
    }

    /**
     *  ler o titulo da planilha (1ªlinha)
     */
    String getTitle() {
        return this.title ? this.title  : 'Exportação SALVE em ' + new Date().format('dd/MM/yyyy HH:mm:ss')
    }

    /**
     * definir lista para gerar a planilha
     * @param newData
     * @return
     */
    void setData( List newData = [] ) {
        data = newData
    }

    /**
     * retorna a lista de dados da planilha
     * @return
     */
    List getData() {
        return data
    }

    String convertToRTF(String htmlStr) {

        OutputStream os = new ByteArrayOutputStream();
        HTMLEditorKit htmlEditorKit = new HTMLEditorKit();
        RTFEditorKit rtfEditorKit = new RTFEditorKit();
        String rtfStr = null;

        htmlStr = htmlStr.replaceAll("<br.*?>","#NEW_LINE#");
        htmlStr = htmlStr.replaceAll("</p>","#NEW_LINE#");
        htmlStr = htmlStr.replaceAll("<p.*?>","");
        InputStream is = new ByteArrayInputStream(htmlStr.getBytes());
        try {
            Document doc = htmlEditorKit.createDefaultDocument();
            htmlEditorKit.read(is, doc, 0);
            rtfEditorKit .write(os, doc, 0, doc.getLength());
            rtfStr = os.toString();
            rtfStr = rtfStr.replaceAll("#NEW_LINE#","\\\\par ");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rtfStr;
    }

   int calculateColWidth(int width){
        if(width > 254)
            return 65280; // Maximum allowed column width.
        if(width > 1){
            int floor = (int)(Math.floor(((double)width)/5));
            int factor = (30*floor);
            int value = 450 + factor + ((width-1) * 250);
            return value;
        }
        else
            return 450; // default to column size 1 if zero, one or negative number is passed.
    }

    void setWorker( Map newValue =null , WorkerService newWorkerService = null) {
        this.worker = newValue
        d(' - worker carregado - sqPessoa: '+worker?.sqPessoa)
        this.workerService = newWorkerService
        if( this.worker && ! this.workerService ) {
            d( '  - worker service criado');
            this.workerService = new WorkerService();
        }
    }
    Map getWorker(){
        return this.worker;
    }

    /**
     * processa lista de dados em função das colunas adicionadas e cria a planilha
     * @return boolean
     */
    boolean run(UserJobs job = null, Map config = [:] ) {
        if( columns.size() == 0 ) {
            error ='Colunas não foram defindas. Utilize o método addColumn() para adicionar colunas.'
            return false
        }
        Row row

        d(' - PlanilhaExcel.run() iniciado')

        Map styles = [:]
        Map fonts  = [:]

        // altura padrão das linhas
        sheet.setDefaultRowHeightInPoints(20)

        // estilo para cada celula de dados
        CellStyle cellStyle = workbook.createCellStyle()
        cellStyle.setBorderTop(BorderStyle.THIN)
        cellStyle.setBorderLeft(BorderStyle.THIN)
        cellStyle.setBorderBottom(BorderStyle.THIN)
        cellStyle.setBorderRight(BorderStyle.THIN)
        cellStyle.setVerticalAlignment(VerticalAlignment.TOP)
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyle.setFillBackgroundColor( IndexedColors.BLACK.getIndex() )
        cellStyle.setFillForegroundColor( IndexedColors.WHITE.getIndex() )
        cellStyle.setWrapText(true)
        cellStyle.setFont(fontNormal)
        styles.normal = cellStyle

        CellStyle cellStyleI = workbook.createCellStyle()
        cellStyleI.cloneStyleFrom( cellStyle )
        cellStyleI.setFont(fontItalic)
        styles.italic = cellStyleI

        // estilo para celulas centralizadas
        CellStyle cellStyleCenter = workbook.createCellStyle()
        cellStyleCenter.cloneStyleFrom( cellStyle )
        cellStyleCenter.setAlignment(HorizontalAlignment.CENTER)
        cellStyleCenter.setVerticalAlignment(VerticalAlignment.TOP)
        styles.center = cellStyleCenter

        // estilo para celulas alinhadas a direita
        CellStyle cellStyleRight = workbook.createCellStyle()
        cellStyleRight.cloneStyleFrom( cellStyle )
        cellStyleRight.setAlignment(HorizontalAlignment.RIGHT);
        cellStyleRight.setVerticalAlignment(VerticalAlignment.TOP);
        styles.right = cellStyleRight


        // estilo para o cabeçalho da planilha
        CellStyle headerStyle = workbook.createCellStyle()
        headerStyle.setBorderTop(BorderStyle.THIN)
        headerStyle.setBorderLeft(BorderStyle.THIN)
        headerStyle.setBorderBottom(BorderStyle.THIN)
        headerStyle.setBorderRight(BorderStyle.THIN)
        headerStyle.setVerticalAlignment(VerticalAlignment.CENTER)
        headerStyle.setAlignment(HorizontalAlignment.CENTER)
        headerStyle.setFillForegroundColor( _makeXSSColor("#F3F6F3" ) )
        headerStyle.setFont(fontNormal)
        headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);


        // estilo para o texto observação
        CellStyle obsStyle = workbook.createCellStyle()
        obsStyle.setBorderTop(BorderStyle.THIN)
        obsStyle.setBorderLeft(BorderStyle.THIN)
        obsStyle.setBorderBottom(BorderStyle.THIN)
        obsStyle.setBorderRight(BorderStyle.THIN)
        XSSFFont obsFont=workbook.createFont()
        obsFont.setFontHeight(defaultFontSize)
        obsFont.setColor( _makeXSSColor('#ff0000') )
        obsStyle.setFont(obsFont)
        obsStyle.setVerticalAlignment(VerticalAlignment.TOP)

        // estilo para os dados de log no final da planilha
        CellStyle logStyle = workbook.createCellStyle()
        logStyle.setBorderTop(BorderStyle.THIN)
        logStyle.setBorderLeft(BorderStyle.THIN)
        logStyle.setBorderBottom(BorderStyle.THIN)
        logStyle.setBorderRight(BorderStyle.THIN)
        XSSFFont logFont=workbook.createFont();
        logFont.setFontHeight(defaultFontSize)
        logFont.setColor(new XSSFColor(new Color(0,0,200 ) ) )
        logStyle.setFont(logFont)
        logStyle.setVerticalAlignment(VerticalAlignment.TOP)

        if( this.getTitle() != ' ') {
            // adicionar data e hora da exortação
            d(' - criada linha data/hora de impressao da planilha')
            row = nextRow()
            setCell(0, this.getTitle(), row, [0, 5]).setCellStyle(logStyle)
        }
        // adicionar linhas com os critérios/filtros utilizados para extração dos dados
        if( getCriterias() ) {

            d(' - criada linhas com os critérios utilizados')
            row = nextRow()
            setCell(0, (String) 'Filtro(s) utilizado(s):', row, [0,5]).setCellStyle( logStyle )

            for( criteria in getCriterias() ){
                // os valores dos filtros veem do browser por isso tem que docodificar o UTF-8
                //String titleDecoded  = new String( criteria.title.toString().getBytes(), "utf-8");
                //String valueDecoded  = new String( criteria.value.toString().getBytes('UTF-8') )
                String titleDecoded  = criteria.title.toString()
                String valueDecoded  = criteria.value.toString()

                //String decoded = new String( criteria.value.getBytes("ISO-8859-1") );
                //println "ISO-8859-1 : " + decoded
                /*CharsetDetector detector = new CharsetDetector();
                detector.setText( criteria.value.toString().getBytes() );
                println '-'*80
                println '-'*80
                println '-'*80
                println 'CharsetDetector:' + detector.detect();*/
                //d('   - ' + criteria.title + ' = ' + criteria.value );
                d('   -  ' + titleDecoded + ' = ' + valueDecoded );
                row = nextRow()
                //setCell(0, (String) criteria.title, row).setCellStyle(logStyle)
                //setCell(1, (String) criteria.value, row,[1,5]).setCellStyle(logStyle)
                setCell(0, titleDecoded, row).setCellStyle(logStyle)
                setCell(1, valueDecoded, row,[1,5]).setCellStyle(logStyle)
                // colspan
                //CellRangeAddress region = new CellRangeAddress(currentRowNumber, currentRowNumber, 1, 5);
                //sheet.addMergedRegion(region);
            }


            //Row row = nextRow()
            //getCriterias().each { criteria ->
            //    d(' - '+criteria.title+'='+criteria.value);
            /*setCell(0, (String) this.obs, row).setCellStyle(obsStyle)
            // colspan
            CellRangeAddress region = new CellRangeAddress(currentRowNumber, currentRowNumber, 0, 5);
            sheet.addMergedRegion(region);*/
            //}
        }


        //obsStyle.setFillBackgroundColor( IndexedColors.GREY_25_PERCENT.getIndex() )
        //obsStyle.setFillPattern(CellStyle.SOLID_FOREGROUND)
        //obsStyle.setAlignment(HorizontalAlignment.CENTER)
        List autoFitColumns=[]
        Row headerRow = nextRow()
        //headerRow.setHeight((short) 600)
        //Cell cell
        /*if( this.obs ) {
            d(' - criada linha de observacao')
            cell = setCell( startLine, ( String ) this.obs, headerRow )
            cell.setCellStyle(obsStyle)
            headerRow = nextRow()
        }*/
        // filtrar as colunas visiveis
        List visibleColumns = columns.findAll { col->
            if( col?.alwaysVisible ) {
                return true
            }
            return col?.visible == false ? false : true;
        }

        visibleColumns.eachWithIndex { item, index ->
            Cell cell = setCell(index, (String) item?.title, headerRow)
            cell.setCellStyle(headerStyle)
            // definir a larugra padrão para o tamanho do titulo
            if (item.title) {
                String cleanTitle = item.title.toString().replaceAll(/(?i)[^a-z]/, '')
                sheet.setColumnWidth(index, calculateColWidth(cleanTitle.size().toInteger() + 4));
            }
            if (item.width) {
                sheet.setColumnWidth(index, calculateColWidth(item.width.toInteger()));
            } else {
                if (item?.autoFit != false) {
                    autoFitColumns.push(index)
                }
            }
            Map style = _createStyleId( item )

            // criar ou aplicar o estilo na celua
            CellStyle newStyle = workbook.createCellStyle();
            newStyle.cloneStyleFrom(cellStyle)
            if (style.color) {
                newStyle.setFillBackgroundColor(_makeXSSColor(style.color))
                if (!fonts[style.color]) {
                    XSSFFont newFont = workbook.createFont()
                    newFont.setFontHeight(defaultFontSize)
                    newFont.setColor(_makeXSSColor(style.color))
                    fonts[style.color] = newFont
                    newStyle.setFont(newFont)
                } else {
                    newStyle.setFont(fonts[style.color])
                }
            }
            if (style.bgColor) {
                newStyle.setFillForegroundColor(_makeXSSColor(style.bgColor))
            }
            newStyle.setAlignment(HorizontalAlignment.LEFT);
            if (style.align) {
                if (style.align == 'center') {
                    newStyle.setAlignment(HorizontalAlignment.CENTER);
                } else if (style.align == 'right') {
                    newStyle.setAlignment(HorizontalAlignment.RIGHT);
                }
                newStyle.setVerticalAlignment(VerticalAlignment.TOP);
            }
            if( ! styles[style.id] ) {
                styles[style.id] = newStyle
            }
        }
        d(' - adicionado cabecalho')
        d(' - inicio loop ' + data.size() + ' registros')
        if( worker?.id ) {
            workerService.setMax(worker, worker.vlMax + data.size() )
        }

        // utilizar gerenciamento dos jobs
        if( job ){
            job.vlMax = data.size()
            job.vlAtual = 0
            job.deAndamento = job.deAndamento ?: '0 de ' + data.size()  // %s é onde será exibido o percentual executado
            job.save( flush:true )
            if( job.vlMax < 1 ) {
                throw new Exception('Nenhum registro encontrado')
            }
        }

        data.eachWithIndex { dataTemp, rowIndex ->
            //Thread.sleep(2000 )
            /** /
            int percent = 0
            try {
                percent = Math.round((rowIndex + 1) / data.size() * 100)
            } catch( Exception e ){}

            if ( ( rowIndex + 1 ) % 1000 == 0) {
                //d(' - adicionadas +1000 linhas na planilha ' + ( (rowIndex+1).toString() + ' / ' + data.size() ) + ' ( '+percent+'% )' )
                println ' - adicionadas 1000 linhas na planilha ' + ( (rowIndex+1).toString() + ' / ' + data.size() ) + ' ( '+percent+'% )'
            }
             /**/
            /*
            if ( ( rowIndex + 1 ) % 100 == 0) {
                // retain 100 last rows and flush all others
               sheet.flushRows(100)
            }*/
            if( worker?.id ) {
                try {
                    workerService.setTitle(worker, 'Linha: ' + (rowIndex + 1) + ' de ' + data.size())
                } catch( Exception e ){}
            }
            row = nextRow() // adicionar linha em branco
            visibleColumns.eachWithIndex { item, colIndex ->
                String value
                try {
                    value = (String) ( dataTemp[ item.column ] ?: '' )
                } catch ( Exception e ){
                    value = ''
                }
                // se tiver valor fixo, imprimir o valor fixo
                if( item?.value  ) {
                    value = (String) item.value
                } else {
                    if( item.type /*&& value*/  ) {
                        if( item.type == 'integer') {
                            try {
                                value = value.toInteger()
                            } catch (e) {
                            }
                        } else if( item.type =='float') {
                            value=value?.replaceAll('\\.',',')
                        } else if( item.type == 'csv' ) {
                            String delmiter = (item.delimiter ?: ';')
                            value = value?.replaceAll(delmiter,'\n')
                        } else if( item.type == 'tree' ) {
                            value = Util.array2Lf(Util.criarListaNomesFromArrayAgg( value ) )
                        }
                        else if( item.type == 'date') {
                            try {
                                value = value?.replaceAll(' 00:00:00\\.0','')
                                if( value.indexOf(':') == -1) {
                                    value = new Date().parse('yyyy-MM-dd', value).format('dd/MM/yyyy')
                                } else {
                                    value = new Date().parse('yyyy-MM-dd HH:mm:ss', value).format('dd/MM/yyyy HH:mm:ss')
                                }
                            } catch( Exception e ) {
                                //println 'Erro valor da coluna dataTemp ('+item.column+'): '+value
                            }
                        }
                    } else {
                        if( value ) {
                            // por padrão os campos data serão impressos sem a hora
                            if ( (item.column =~ /^dt_/) ) {
                                try {
                                    if (value =~ /^[0-9]{4}(-|\/)[0-9]{2}(-|\/)[0-9]{2}/) {
                                        value = value?.replaceAll(/\//, '-')
                                        value = new Date().parse('yyyy-MM-dd', value).format('dd/MM/yyyy')
                                    }
                                } catch (Exception e) {
                                }
                            }
                        }
                    }
                }
                //setCell(colIndex, value, row)
                //Cell cell =  setCell(colIndex, value, row)
                Map style = _createStyleId( item, value )
                if( styles[style.id] && value ) {
                    setCell(colIndex, value, row).setCellStyle(styles[style.id])
                } else {
                    setCell(colIndex, value, row).setCellStyle(styles.normal)
                }
            }

            if( worker?.id ) {
                workerService.stepIt( worker )
            }

            // pular a primeira linha para não deixar completar 100 antes de salvar o arquiv em disco
            if( rowIndex > 0 ) {
                if ( job ) {
                    //job.stepIt(rowIndex + ' de ' + data.size() + ' (%s)')
                    job.stepIt(rowIndex + ' de ' + data.size() )
                    if( job.stCancelado ) {
                        throw new Exception ('Cancelado pelo usuário')
                    }
                }
            }
        }
        d(' - fim loop')

        /*println 'Largura atual da coluna 55'
         println sheet.getColumnWidth(55)
         println 'Largura para 10'
         println calculateColWidth(10);
         println ' '
         //sheet.setColumnWidth( 0, calculateColWidth(50) );*/
        /*
        if( data.size() < 2000 ) {
            d(' - inicio ajuste largura colunas')
            // ajustar a largura das colunas definidas com autoFit:true
            autoFitColumns.eachWithIndex { it, index ->
                sheet.autoSizeColumn(it);
                int colWidth = sheet.getColumnWidth(index)
                if (colWidth > 10000) {
                    //println 'col '+ index + ' w:'+colWidth
                    sheet.setColumnWidth(index, calculateColWidth(70))
                }
            }
            d(' - fim ajuste largura colunas')
        }
        */

        // adicionar o texto de observação no final
        if( this.obs ) {
            d(' - criada linha de observacao')
            row = nextRow()
            setCell(0, (String) this.obs, row,[0,visibleColumns.size()-1]).setCellStyle(obsStyle)
            //CellUtil.setAlignment(obsCell, workbook, CellStyle.ALIGN_CENTER);
            // colspan
            //CellRangeAddress region = new CellRangeAddress( currentRowNumber, currentRowNumber, 0, 5);
            //sheet.addMergedRegion(region);
        }

        // adicionar citação
        if( this.citation ) {
            this.createSheet( 'Como citar')
            d(' - Adicionando a Citacao')
            row = nextRow()

            String currentDay = new Date().format("dd 'de' MMM'.' 'de' yyyy").replaceAll(/ mai\. /, 'maio')
            setCell(0, 'Como citar: ' + this.citation + ', em Sistema de Avaliação do Risco de Extinção - SALVE em ' + currentDay
                , row,[0,visibleColumns.size()-1],).setCellStyle(logStyle)
        }

        // salvar planilha em disco
        boolean resultSave = save() // gravar a planilha no disco
        if ( job ) {
           // finalizar job
           job.stepIt()
        }

        return resultSave
    }

    /**
     * transformar coores no formato hexadecimal para rgb
     * @example _parseColor('#efaadd')
     * @param color
     * @return
     */
    protected Color _parseColor(String color = '' ){
        Color colorRGB = null
        try {
            colorRGB = Color.decode(color)
        } catch ( Exception e ) {
            colorRGB = Color.decode('#000000')
        }
        return colorRGB
    }

    /**
     * converter cor no formato hexadecimal para classe XSSFColor
     * @example _makeXSSColor('#efefef')
     * @param color
     * @return
     */
    protected XSSFColor _makeXSSColor( String color = '' ){
        // new XSSFColor( new Color(255,0,0) )
        Color colorRGB = _parseColor( color );
        XSSFColor newColor = null
        try {
            newColor = new XSSFColor( colorRGB, new DefaultIndexedColorMap());
        } catch ( Exception e ) {
            newColor = new XSSFColor( IndexedColors.BLACK, new DefaultIndexedColorMap());
        }
        return newColor
    }

    /**
     * método para gerar um id unico para o style definido na coluna
     * @param item
     * @return
     */
    protected Map _createStyleId( Map item = [:], String value = '') {
        Map style = [id: 'normal']

        if( (value =~ /^<i>/)  ) {
            style.id = 'italic'
        }
        // criar os estilos que serão utilizados nas colunas
        if (item?.align) {
            if (item.align.toLowerCase() == 'center') {
                style.id = 'center'
                style.align = 'center'
            } else if (item.align.toLowerCase() == 'right') {
                style.id = 'right'
                style.align = 'right'
            }
        }
        if (item?.bgColor) {
            style.id += 'bg' + item.bgColor
            style.bgColor = item.bgColor
        }
        if (item?.color) {
            style.id += 'fg' + item.color
            style.color = item.color
        }
        return style
    }
}
