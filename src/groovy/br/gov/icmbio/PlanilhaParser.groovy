package br.gov.icmbio

import jxl.CellType
import jxl.NumberCell
import jxl.WorkbookSettings
import org.apache.commons.fileupload.disk.DiskFileItem
import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.CellType
import org.apache.poi.ss.usermodel.DateUtil
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.usermodel.WorkbookFactory
import org.springframework.web.multipart.commons.CommonsMultipartFile

import java.io.*
import java.text.SimpleDateFormat

// exemplos
// http://jexcelapi.sourceforge.net/resources/javadocs/2_6_10/docs/jxl/Workbook.html
// https://poi.apache.org/apidocs/org/apache/poi/ss/usermodel/Workbook.html
// http://www.codejava.net/coding/how-to-read-excel-files-in-java-using-apache-poi
// http://www.avajava.com/tutorials/lessons/how-do-i-read-from-an-excel-file-using-poi.html
// http://stackoverflow.com/questions/5578535/get-cell-value-from-excel-sheet-with-apache-poi

class PlanilhaParser
{

    private headerRowNum; // indicar a linha que tem os titulos das colunas default=0
    private String error;
    private File file;
    //private CommonsMultipartFile file;
    private org.apache.poi.ss.usermodel.Workbook workbookPoi;
    private jxl.Workbook workbookJxl;
    private org.apache.poi.ss.usermodel.Sheet sheetPoi
    private jxl.Sheet sheetJxl
    private String sheetName
    private List sheetNames
    private int sheetCount

    PlanilhaParser(CommonsMultipartFile newFile )
    {
      this.file = toFile( newFile );
    }

    PlanilhaParser(File newFile )
    {
      this.file = newFile
    }

    void close(){
        if (this.workbookPoi) {
            this.workbookPoi.close()
        }
        if (this.workbookJxl) {
            this.workbookJxl.close()
        }
    }

    void setHeaderRowNum( int newValue )
    {
        this.headerRowNum = newValue;
    }
    int getHeaderRowNum()
    {
        if( ! this.headerRowNum )
        {
            return 1;
        }
        else
        {
            return ( this.headerRowNum > 0 ? this.headerRowNum : 1 )
        }
    }

    private File toFile(CommonsMultipartFile newFile ) {
        DiskFileItem diskFileItem = (DiskFileItem) newFile.getFileItem();
        String absPath = diskFileItem.getStoreLocation().getAbsolutePath();
        return new File(absPath)
    }

    void saveAs(String newFileName )
    {
        try
        {
            // gravar o arquivo no diretorio
            if (newFileName)
            {
                //this.file.transferTo(new File(newFileName));
                //println 'rename to ' + newFileName
                this.file.renameTo(new File(newFileName));
            }
        }
        catch (Exception e )
        {
            this.error = e.getMessage();
        }
    }

    void setWorkbook()
    {
        if( ! this.workbookPoi && !this.workbookJxl )
        {
            // forçar jxl
            /** /
            this.workbookPoi=null;
            // tentar abrir nativamente sem POI
            try
            {
                WorkbookSettings wbSettings = new WorkbookSettings();
                wbSettings.setSuppressWarnings(true);
                wbSettings.setLocale(new Locale("pt", "BR"));
                wbSettings.setEncoding("Cp1252");
                this.workbookJxl = jxl.Workbook.getWorkbook( this.toFile(), wbSettings );

             } catch (Exception e2 )
            {
                this.workbookJxl=null;
                this.error = e2.getMessage();
            }
            return;
            /**/

            try
            {
                println 'Abrindo planilha com Apache POI'
                this.workbookPoi = WorkbookFactory.create(this.file)
                println 'Apache POI OK.'
                println '----------------------'
                //println this.workbookPoi.getClass();
                this.sheetCount = this.workbookPoi.getNumberOfSheets()
                this.sheetNames = this.getSheetNames()
            }
            catch (Exception e)
            {
                //println e.getMessage();
                this.workbookPoi=null;
                // tentar abrir nativamente sem POI
                try
                {
                    println 'Abrindo planilha com JXL'
                    WorkbookSettings wbSettings = new WorkbookSettings();
                    wbSettings.setSuppressWarnings(true);
                    wbSettings.setEncoding("Cp1252");
                    wbSettings.setLocale(new Locale("pt", "BR"));
                    this.workbookJxl = jxl.Workbook.getWorkbook( this.toFile(), wbSettings );
                    println 'JXL OK'
                    println '----------------------'
                    this.sheetCount = this.workbookJxl.getNumberOfSheets()
                    this.sheetNames = this.workbookJxl.getSheetNames();
                }
                catch (Exception e2 )
                {
                    //println ' '
                    //println e2.getMessage()
                    this.workbookJxl=null;
                    this.error = e2.getMessage();
                }
            }
        }
    }

    void setSheet(int sheetNumber)
    {
        try
        {
            this.setWorkbook();

            if( this.workbookPoi )
            {
                this.sheetPoi = this.workbookPoi.getSheetAt(sheetNumber);
                this.sheetName = this.workbookPoi.getSheetName( sheetNumber);
            }
            else if( this.workbookJxl )
            {
                this.sheetJxl = this.workbookJxl.getSheet(sheetNumber);
                this.sheetName = this.sheetJxl.getName();
            }
        }
        catch (Exception e)
        {
            this.error = e.getMessage();
        }
    }

    int getRows()
    {
        int qtdRows = 0;
        String conteudo='';
        if( this.sheetPoi )
        {
            for (int i = 0; i <= this.sheetPoi.getLastRowNum(); i++)
            {
                Row row = this.sheetPoi.getRow(i);
                if( row )
                {
                    // verificar coluna 1
                    conteudo=row.getCell(0).toString().trim().toLowerCase();
                    if ( conteudo != '' && conteudo != 'null')
                    {
                        qtdRows = i+1;
                    } else {
                        // verificar coluna 2
                        conteudo=row.getCell(1).toString().trim().toLowerCase();
                        if ( conteudo != '' && conteudo != 'null')
                        {
                            qtdRows = i+1;
                        }
                    }
                }
            }
        }
        else if( this.sheetJxl )
        {
            for (int i = 0; i < this.sheetJxl.getRows(); i++)
            {
                conteudo=this.sheetJxl.getCell(0, i).getContents().toString().trim().toLowerCase();
                if( conteudo != '' && conteudo != 'null' )
                {
                    qtdRows = i+1;
                } else {
                    conteudo=this.sheetJxl.getCell(1, i).getContents().toString().trim().toLowerCase();
                    if( conteudo != '' && conteudo != 'null' )
                    {
                        qtdRows = i+1;
                    }
                }
            }
        }
        return qtdRows;
    }

    int getCols(int rowNum = -1 )
    {
        int qtdCols = 0;
        if( rowNum < 0 )
        {
            rowNum = this.getHeaderRowNum();
        }
        rowNum--
        rowNum = (rowNum < 0 ? 0 : rowNum )
        if( this.sheetPoi )
            {
            Row row = this.sheetPoi.getRow( rowNum );
            for (int i = 0; i < row.getPhysicalNumberOfCells(); i++)
            {
                if ( row.getCell(i).toString().trim() != '')
                {
                    //println 'v:' + row.getCell(i).toString()
                    qtdCols = i+1;
                }
            }
        }
        else if( this.sheetJxl )
        {
            for (int j = 0; j < this.sheetJxl.getColumns(); j++)
            {
                // para contar a quantidade de colunas considerar uma linha apos e uma linha antes para garantir
                if( this.sheetJxl.getCell( j, rowNum -1 ).getContents() || this.sheetJxl.getCell( j, rowNum ).getContents() || this.sheetJxl.getCell( j, rowNum ).getContents() -1  )
                {
                    qtdCols = j+1;
                }
            }
        }
        return qtdCols;
    }

    List getTitles()
    {
        return this.getRowData( this.getHeaderRowNum() )
    }

    List getRowData(int rowNum = 1)
    {
        TimeZone gmtZone = TimeZone.getTimeZone("GMT");
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        format.setTimeZone(gmtZone);
        if( rowNum == null )
        {
            rowNum=1;
        }
        rowNum--;
        rowNum = (rowNum<0?0:rowNum)
        List data = [];
        int cols = this.getCols();
        if( this.sheetPoi ) {
            for (int i = 0; i < cols; i++)
            {
                try {
                    Cell cell = this.sheetPoi.getRow(rowNum).getCell(i);
                    if ( cell )
                    {
                        switch ( cell.getCellType() )
                        {
                            case CellType.NUMERIC:
                                if (DateUtil.isCellDateFormatted(cell)) {
                                    data.push(cell.getDateCellValue().format('dd/MM/yyyy'))
                                } else {
                                    // remover notação cientifica do numero
                                    String num = cell.toString().trim();
                                    if( num.indexOf('E') > -1 )
                                    {
                                        num =  String.format("%.0f", Double.parseDouble(cell.toString().trim())).padLeft(11, '0');
                                    }
                                    data.push(num)
                                }
                                break;
                            case CellType.BLANK:
                                data.push('')
                                break;
                            case CellType.FORMULA:
                                switch(cell.getCachedFormulaResultType())
                                {
                                    case CellType.BLANK:
                                        if (DateUtil.isCellDateFormatted(cell)) {
                                            data.push(cell.getDateCellValue().format('dd/MM/yyyy'))
                                        } else {
                                            data.push( cell.getNumericCellValue() )
                                        }
                                        break;
                                    case CellType.STRING:
                                        data.push( cell.toString().trim() )
                                        break;
                                }
                                break;
                            default:
                                data.push( cell.toString().trim())
                        }

                    }
                    else
                    {
                        data.push('');
                    }
                }
                catch(Exception e)
                {
                    data.push( '' )
                }
            }
        }
        else if( this.sheetJxl )
        {
            for (int cnt = 0; cnt < cols; cnt++)
            {
                try {
                   switch ( this.sheetJxl.getCell(cnt, rowNum).getType() )
                   {
                       case jxl.CellType.LABEL:
                           data.push(this.sheetJxl.getCell(cnt, rowNum).getContents());
                           break;
                       case jxl.CellType.NUMBER:
                           data.push(Double.toString( ( (NumberCell) this.sheetJxl.getCell(cnt, rowNum) ).getValue() ) )
                           break;
                       case jxl.CellType.DATE:
                           data.push(format.format(this.sheetJxl.getCell(cnt, rowNum).getDate()))
                           break;
                       default:
                           data.push(this.sheetJxl.getCell(cnt, rowNum).getContents());
                   }
                }   catch(Exception e)
                {
                    data.push( '' )
                }
            }
        }
        return data;
    }

    String getError()
    {
        return this.error
    }

    String getSheetName()
    {
       return this.sheetName
    }

    List getSheetNames()
    {
        if( this.workbookJxl )
        {
            return this.sheetNames
        }
        else if( this.workbookPoi)
        {
            List data = []
            for(int i=0;i<this.sheetCount;i++)
            {
                data.push( this.workbookPoi.getSheetAt(i).getSheetName() )
            }
            return data
        }
    }

    int getSheetCount()
    {
        return this.sheetCount
    }

}
