package br.gov.icmbio

// https://coderanch.com/how-to/javadoc/itext-2.1.7/com/lowagie/text/Paragraph.html
// https://www.java-tips.org/other-api-tips-100035/62-itext/130-creating-pdf-rtf-or-html-document-from-a-java-class-at-runtime.html

/*
import com.lowagie.text.FontFactory
import com.lowagie.text.rtf.document.RtfDocument
import jdk.nashorn.internal.runtime.regexp.joni.Regex
import javax.swing.text.html.HTMLEditorKit
import javax.swing.text.rtf.RTFEditorKit
import javax.swing.text.BadLocationException
import com.lowagie.text.html.simpleparser.HTMLWorker
import com.lowagie.text.html.simpleparser.StyleSheet
import com.lowagie.text.rtf.RtfBasicElement
import com.lowagie.text.rtf.direct.RtfDirectContent
import com.lowagie.text.rtf.style.RtfParagraphStyle
import com.lowagie.text.pdf.BaseFont
import com.lowagie.text.html.HtmlParser
import com.lowagie.text.rtf.table.RtfBorderGroup
import com.lowagie.text.rtf.table.RtfTable
import com.itextpdf.tool.xml.XMLWorkerHelper;
*/

import com.lowagie.text.Cell
import com.lowagie.text.DocListener
import com.lowagie.text.DocumentException
import com.lowagie.text.Image
import com.lowagie.text.Table
import com.lowagie.text.rtf.direct.RtfDirectContent
import com.lowagie.text.rtf.style.RtfFont
import java.awt.Color
import com.lowagie.text.Element
import com.lowagie.text.Font
import com.lowagie.text.HeaderFooter
import com.lowagie.text.Rectangle
import com.lowagie.text.Document
import com.lowagie.text.Chunk
import com.lowagie.text.Phrase
import com.lowagie.text.Paragraph
import com.lowagie.text.PageSize
import com.lowagie.text.rtf.RtfWriter2

class RtfBase {
    public String tmpDir
    public String diaHora
    public String dataImpressao
    public String fileName
    public Rectangle pageSize
    public Document document
    public RtfWriter2 writer
    public Color corFundoVerde
    public Color corCinzaClaro

    public RtfFont footerFont
    public RtfFont headerFont
    public RtfFont titleFont

    public RtfFont textFont
    public RtfFont textItalicFont
    public RtfFont textBoldFont

    RtfBase(String tmpDir) {
        this.diaHora = new Date().format('dd-MM-yyyy-HH-mm-ss');
        this.fileName = ''
        this.tmpDir = tmpDir
        corFundoVerde = new Color(211, 224, 198) // verde claro
        corCinzaClaro = new Color(220, 220, 220) // cinza claro
    }

    void initDoc() {

        // https://www.programcreek.com/java-api-examples/?api=com.lowagie.text.rtf.RtfWriter2
        pageSize = new Rectangle(PageSize.A4);
        document = new Document(pageSize, 36, 36, 46, 36);
        writer = RtfWriter2.getInstance(document, new FileOutputStream(fileName))


        document.addAuthor("ICMBIo");
        document.addSubject("Sistema de Avaliação - SALVE")
        document.open()


        // configuração das fontes
        /*RtfParagraphStyle.STYLE_HEADING_1.setFontName("Times New Roman");
        RtfParagraphStyle.STYLE_HEADING_1.setColor(Color.BLACK)
        RtfParagraphStyle.STYLE_HEADING_1.setSize(12);
        */

        // fonte do cabecalho
        headerFont = new RtfFont("Times New Roman", 12, Font.BOLD, Color.BLACK);

        // fonte do rodape
        footerFont = new RtfFont("Times New Roman", 10, Font.NORMAL, Color.GRAY);

        // fonte dos titulos, tópicos relevantes
        titleFont = new RtfFont("Times New Roman", 14, Font.BOLD, Color.BLACK);

        // Fonte normal dos textos
        textFont = new RtfFont("Times New Roman", 12, Font.NORMAL, Color.BLACK);
        textItalicFont = new RtfFont("Times New Roman", 12, Font.ITALIC, Color.BLACK);
        textBoldFont = new RtfFont("Times New Roman", 12, Font.BOLD, Color.BLACK);

    }

    public setHeader(String headerText = '', boolean brasao = false) {
        if (headerText) {
            // titulo
            //Font fontHeader   = FontFactory.getFont(FontFactory.TIMES_ROMAN, 12,Font.BOLD,new Color(0,0,0));
            //Paragraph par = new Paragraph( new Chunk(headerText, fontHeader) )
            //Paragraph par = new Paragraph(headerText, RtfParagraphStyle.STYLE_HEADING_1)
            Paragraph par = new Paragraph('', headerFont)
            if ( brasao ) {
                if( new File( "/data/salve-estadual/arquivos/brasao.png" ).exists() ) {
                    Image img = Image.getInstance( "/data/salve-estadual/arquivos/brasao.png" )
                    if( !img ) {
                        println 'Imagem /data/salve-estadual/arquivos/brasao.png nao encontrada.'
                    }
                    else {
                        par.add( img )
                    }

                    /*Double fator = img.getScaledWidth() / img.getScaledHeight()
                if ((int) img.getScaledWidth() > (int) maxWidth || (int) img.getScaledHeight() > (int) maxWidth) {
                    img.scaleToFit((float) maxWidth, (float) (maxWidth * fator))
                }
                */
                    img.setAlignment( Image.ALIGN_CENTER | Image.TEXTWRAP )
                }
            }
            par.add(new Chunk(Chunk.NEWLINE))
            Chunk texto = new Chunk(headerText, headerFont)
            par.add(texto)

            // cabecalho
            HeaderFooter header = new HeaderFooter(par, false)
            header.setAlignment(Element.ALIGN_CENTER)
            document.setHeader(header)

        }
    }

    public setFooter(String unidadeOrganizacional = '') {
        //Font fontFooter   = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10,Font.NORMAL,new Color(0,0,0));
        // rodape
        String text
        if (unidadeOrganizacional) {
            text = 'Emitido por ' + unidadeOrganizacional + (' ' * 10) + 'Página: '
        } else {
            text = 'Pagina: '
        }
        Phrase par1 = new Phrase(text, footerFont);
        Phrase par2 = new Phrase((' ' * 10) + "Em: " + dataImpressao, footerFont);
        HeaderFooter footer = new HeaderFooter(par1, par2)
        footer.setAlignment(Element.ALIGN_CENTER)
        document.setFooter(footer)
    }

    public String trim(def texto = null) {
        texto = texto.toString().replaceAll(/^<p>&nbsp;<\/p>|<p>&nbsp;<\/p>$/, '').replaceAll(/<p><\/p>/, '')
        if (!texto || texto == 'null') {
            return '' // se não retornar espaço da erro no Phrase
        }
        return texto
    }

    public void br() {
        document.add(new Chunk(Chunk.NEWLINE))
    }


    public void doRv(String rotulo, String valor) {
        Paragraph paragrafo = new Paragraph('', textFont)
        Chunk texto = new Chunk(rotulo, textItalicFont)
        paragrafo.add(texto)
        texto = new Chunk(valor + '.', textFont)
        paragrafo.add(texto)
        document.add(paragrafo)
    }

    public void doRvBold(String rotulo, String valor, Cell cell = null) {
        Paragraph paragrafo = new Paragraph('', textFont)
        Chunk texto = new Chunk(rotulo, textBoldFont)
        paragrafo.add(texto)
        texto = new Chunk(valor + '.', textFont)
        paragrafo.add(texto)
        if (cell) {
            cell.add(paragrafo)
        } else {
            document.add(paragrafo)
        }
    }

    public void doTexto(String texto) {

        if (texto == null || !texto || texto == 'null') {
            return
        }
        Paragraph paragrafo = new Paragraph('', textFont)
        if (texto.indexOf('</') == -1) {
            texto = Util.html2str( texto )
            texto = texto.replaceAll('<br>','\n')
            texto = texto.replaceAll('<br/>','\n')
            paragrafo.add(texto)
            paragrafo.setAlignment(Element.ALIGN_LEFT)
        } else {
            texto = Util.escape2Rtf( texto )
            paragrafo.setAlignment(Element.ALIGN_JUSTIFIED_ALL)
            RtfDirectContent rdc = new RtfDirectContent(texto)
            paragrafo.add(rdc)
            paragrafo.setAlignment(Element.ALIGN_JUSTIFIED_ALL)
        }
        document.add(paragrafo)
    }

    public String parseSnd(def SND = null) {
        switch (SND) {
            case 'S':
                return 'Sim'
                break
            case 'N':
                return 'Não'
            case 'D':
                return 'Desconhecido'
            default:
                return 'Não informado'
        }
    }

    public Chunk doImage(String imagePath, boolean print) {
        if (!imagePath) {
            return
        }
        Image img = Image.getInstance(imagePath)
        if (img) {
            // verificar se a imagem cabe na página
            Float leftMargin = document.leftMargin()
            Float rightMargin = document.rightMargin()
            Float maxH = pageSize.getHeight() / 2
            Float maxW = pageSize.getWidth() - (leftMargin + rightMargin)
            Float imgH = img.getHeight()
            Float imgW = img.getWidth()
            if (maxH && imgH > maxH) {
                imgH = maxH
            }
            if (maxW && imgW > maxW) {
                imgW = maxW
            }
            img.scaleToFit(imgW, imgH)
            Chunk chunk = new Chunk(img, 0, 0)
            Paragraph p1 = new Paragraph(' ')
            p1.setAlignment(Element.ALIGN_CENTER)
            p1.add(chunk)
            if (print) {
                br()
                document.add(p1)
            }
            return chunk
        }
    }

    public Table doTable(List headers, Integer colSpan = 0, List widths = [], RtfFont font = null) {
        int cols = ((colSpan > 0) ? colSpan : headers.size())
        Table table = new Table(cols)
        table.setBorderWidth(0)
        table.setOffset(0)
        table.setWidth(100)
        table.setComplete(true)
        table.setSpacing(0)
        table.setPadding(5)
        if (widths.size() > 0) {
            table.setWidths((Float[]) widths)
        }
        // criar o cabeçalho da tabela em negrito
        headers.eachWithIndex { it, i ->
            Chunk c = new Chunk(it, textBoldFont)
            Cell cell = new Cell(c)
            cell.setHeader(true)
            cell.setUseBorderPadding(false)
            cell.setHorizontalAlignment(Element.ALIGN_CENTER)
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE)
            cell.setBackgroundColor(corFundoVerde)
            if (colSpan > 0) {
                cell.setColspan(colSpan)
            }
            table.addCell(cell)
        }
        return table
    }

    public void doRow(Table table, List data, List aligns = [], Color bgColor = null, RtfFont font = null) {

        //Util.html2str( justificativaImprimir )

        data.eachWithIndex { it, i ->
            // aplicar a função html2Str para remover o caracteres epeciais do html do texto.
            // ex: &ccedil;,&eacute; etc
            String texto = Util.html2str(trim(it.toString()))
            Chunk c = new Chunk(texto, (font ?: textFont))
            Cell cell = new Cell(c)

            if (it.getClass() == com.lowagie.text.Paragraph || it.getClass() == com.lowagie.text.Chunk) {
                cell = new Cell(it)
            }

            if (bgColor) {
                cell.setBackgroundColor(bgColor)
            }

            if (aligns[i]) {
                if (aligns[i] == 'center') {
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER)
                } else if (aligns[i] == 'left') {
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT)
                } else if (aligns[i] == 'right') {
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT)
                } else if (aligns[i] == 'justify' || aligns[i] == 'justified') {
                    cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED_ALL)
                }
            } else {
                cell.setHorizontalAlignment(Element.ALIGN_CENTER)
            }
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE)

            if (texto.indexOf('</') > -1) {
                Paragraph valueToParagragh = new Paragraph();
                valueToParagragh.setFont(textFont)
                // https://coderanch.com/how-to/javadoc/itext-2.1.7/com/lowagie/text/Paragraph.html
                valueToParagragh.setFirstLineIndent(0f)
                texto = Util.escape2Rtf(texto)
                RtfDirectContent rdc = new RtfDirectContent(texto)
                rdc.setInTable(true)
                cell = new Cell(rdc)
                cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED_ALL)
            }
            table.addCell(cell)
        }
    }

    /*private String convertToRTF(String htmlStr) {
        OutputStream os = new ByteArrayOutputStream();
        HTMLEditorKit htmlEditorKit = new HTMLEditorKit();
        RTFEditorKit rtfEditorKit = new RTFEditorKit();
        String rtfStr = null
        htmlStr = htmlStr.replaceAll("<br.*?>","#NEW_LINE#");
        htmlStr = htmlStr.replaceAll("</p>","#NEW_LINE#");
        htmlStr = htmlStr.replaceAll("<p.*?>","");
        InputStream is = new ByteArrayInputStream(htmlStr.getBytes());
        try {
            javax.swing.text.html.HTMLDocument doc = htmlEditorKit.createDefaultDocument();
            htmlEditorKit.read(is, doc, 0);
            rtfEditorKit.write(os, doc, 0, doc.getLength());
            rtfStr = os.toString();
            rtfStr = rtfStr.replaceAll("#NEW_LINE#","\\\\par ");

        } catch (IOException e) {
            e.printStackTrace();
        } catch (BadLocationException e) {
            e.printStackTrace();
        }
        return rtfStr;
    }
    */
    /**
     * imprimir o cabecalho na página
     * @param text
     * @param brasao
     */
    public void doPageHeader(String text = '', boolean brasao = true) {
        if( text ) {
            Paragraph par = new Paragraph('', headerFont)
            par.setAlignment(Element.ALIGN_CENTER)
            if (brasao) {
                if( new File( "/data/salve-estadual/arquivos/brasao.png" ).exists() ) {
                    Image img = Image.getInstance( "/data/salve-estadual/arquivos/brasao.png" )
                    if( !img ) {
                        println 'Imagem /data/salve-estadual/arquivos/brasao.png nao encontrada.'
                    }
                    else {
                        par.add( img )
                    }
                    img.setAlignment( Image.ALIGN_CENTER | Image.TEXTWRAP )
                    par.add( new Chunk( Chunk.NEWLINE ) )
                }
            }
            Chunk texto = new Chunk(text, headerFont)
            par.add(texto)
            document.add(par)
        }
    }
}
