package br.gov.icmbio

import org.apache.commons.codec.binary.Hex
import sun.misc.BASE64Decoder
import sun.misc.BASE64Encoder
import javax.crypto.Cipher
import javax.crypto.spec.SecretKeySpec
import java.security.Key
import java.security.MessageDigest

class AESCryption {

    static String encrypt(String Data='',String secretKey = '') throws Exception {
        Key key = generateKey(secretKey);
        Cipher c = Cipher.getInstance('AES');
        c.init(Cipher.ENCRYPT_MODE, key);
        byte[] encVal = c.doFinal(Data.getBytes("UTF-8"));
        // tranformar para base64 para evitar caracteres invalidos na url
        String stringBase64 = new BASE64Encoder().encode( encVal )
        return new String(Hex.encodeHex(stringBase64.getBytes()));
    }

    static String decrypt(String encryptedData='', String secretKey = '') throws Exception {
        // voltar da base 64
        encryptedData =  new String( Hex.decodeHex( encryptedData.toCharArray() ) )
        byte[] stringDecoded = new BASE64Decoder().decodeBuffer( encryptedData )
        Key key = generateKey(secretKey);
        Cipher c = Cipher.getInstance('AES');
        c.init(Cipher.DECRYPT_MODE, key);
        byte[] decValue = c.doFinal(stringDecoded);
        return new String(decValue);
    }

    private static Key generateKey( String secretKey = '') throws Exception {
        secretKey =  secretKey  ?: 'SALVE-NDQFSDNDJQJFUDTPTSPAVVEMOCM'
        byte[] key = (secretKey).getBytes("UTF-8");
        MessageDigest sha = MessageDigest.getInstance("SHA-1");
        key = sha.digest(key);
        key = Arrays.copyOf(key, 16); // use only first 128 bit
        SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");
        return secretKeySpec
    }
}