package br.gov.icmbio

import com.lowagie.text.Cell

// https://coderanch.com/how-to/javadoc/itext-2.1.7/com/lowagie/text/Table.html
import com.lowagie.text.Element
import com.lowagie.text.Font
import com.lowagie.text.Image
import com.lowagie.text.Table
import com.lowagie.text.Paragraph
import com.lowagie.text.Chunk
import com.lowagie.text.rtf.direct.RtfDirectContent
import com.lowagie.text.rtf.style.RtfFont
import grails.converters.JSON
import org.codehaus.groovy.grails.web.json.JSONObject
import org.codehaus.groovy.grails.web.servlet.mvc.GrailsParameterMap

import java.awt.Color

class RtfOficina extends RtfBase {
    private Oficina oficina
    private unidadeSigla
    private unidade
    private mapTotais

    RtfOficina(Integer sqOficina = 0, String tmpDir, String unidade, String unidadeSigla, Map mapTotais=[:]) {
        super(tmpDir)
        this.oficina = Oficina.get(sqOficina)
        this.unidade = unidade
        this.unidadeSigla = unidadeSigla
        this.mapTotais =  mapTotais
    }

    String runMemoriaOficina(String periodo = '', Map mapTotais = [:], Map mapPapeis = [:]) {
        String agora = new Date().format('dd-MM-yyyy-hh-mm-ss')
        this.fileName = this.tmpDir + 'rel-momoria-oficina-' + agora + '.rtf'
        String strTemp
        List lstTemp
        Paragraph par
        try {
            this.dataImpressao = new Date().format('dd/MM/yyy HH:mm:ss')

            // criar e abrir o documento e criar o cabeçalho e o rodapé
            initDoc()
            setHeader('MINISTÉRIO DO MEIO AMBIENTE\n'
                    + 'INSTITUTO CHICO MENDES DE CONSERVAÇÃO DA BIODIVERSIDADE\n'
                    + 'DIRETORIA DE PESQUISA, AVALIAÇÃO E MONITORAMENTO DA BIODIVERSIDADE\n'
                    + this.unidade.toUpperCase()
                    , true
            )
            setFooter(unidadeSigla)

            // titulo
            par = new Paragraph('Memória da ' + oficina.noOficina, titleFont)
            par.setAlignment(Element.ALIGN_CENTER)
            document.add(par)

            br()

            doRvBold('Data: ', periodo)
            doRvBold('Local: ', Util.capitalize( oficina.deLocal ) )

            br()

            // gride totais
            Table table = doTable(['Categorias Avaliadas'], 2, [3f, 1f])
            table.setWidth(70)
            mapTotais.qtdCategoria.each {
                if (it.key.indexOf('Total') == 0) {
                    doRow(table, [it.key, it.value], ['left', 'center'], corCinzaClaro, textBoldFont)
                } else {
                    doRow(table, [it.key, it.value], ['left', 'center'])
                }
            }
            document.add(table)

            br()

            par = new Paragraph('Encaminhamentos', textBoldFont)
            par.setAlignment(Element.ALIGN_LEFT)
            document.add(par)
            doTexto(oficina.txEncaminhamento)
            br()

            // final memoria oficina
            par = new Paragraph(Util.capitalize( oficina.deLocal )+ ', '+ ( new Date().format("dd 'de' MMMM 'de' yyyy") ).toLowerCase()+'.', textFont)
            par.setAlignment(Element.ALIGN_RIGHT)
            document.add(par)

            br()

            // participantes
            Integer oldFontSize = par.getFont().getSize();
            par = new Paragraph('Participantes', textBoldFont)
            par.getFont().setSize(oldFontSize + 4i)
            par.setAlignment(Element.ALIGN_CENTER)
            document.add(par)
            par.getFont().setSize(oldFontSize)
            br()
            mapPapeis.each {
                par = new Paragraph(it.key + (it.value.size() > 1 && it.key =~ /or$/ ? 'es' : ''), textBoldFont)
                par.setAlignment(Element.ALIGN_LEFT)
                document.add(par)
                it.value.eachWithIndex { nome, i ->
                    par = new Paragraph("\t" + (i + 1) + ') ' + (nome + ' ').toString().padRight(75, '_'), textFont)
                    par.setAlignment(Element.ALIGN_LEFT)
                    document.add(par)
                    br()
                }
                br()
            }

            // fim
            document.close()
        }
        catch (Exception e) {
            fileName = ''
            println e.getMessage()
            //e.printStackTrace()
        }
        return fileName
    }

    /**
     *
     * @param dia
     * @param periodo
     * @param mapPapeis
     * @param tipo - todos, somente LCs
     * @param considerarUAN - considerar Ultima Avaliação Nacional quando a ficha não tiver sido validada
     * @param ordenadoPorOrdemFamilia
     * @return
     */
    String runDocFinal(String dia = '', String periodo = '', Map mapPapeis = [:], String tipo = 'todos', String considerarUAN = 'N', String ordenadoPorOrdemFamilia = 'N' ) {
        String agora = new Date().format('dd-MM-yyyy-hh-mm-ss')
        String nomeArquivo = 'final'
        Table table
        if (dia) {
            nomeArquivo = 'diario_de_' + dia.replaceAll('/', '-')
        }
        if (tipo == 'final-lc') {
            nomeArquivo = 'especies_mantidas_lc'
        }
        if (tipo == 'final-nao-lc') {
            nomeArquivo = 'especies_avaliadas'
        }
        if (tipo == 'final-todas') {
            nomeArquivo = 'todas_especies'
        }
        this.fileName = this.tmpDir + 'rel_' + nomeArquivo + '-oficina-' + agora + '.rtf'
        String strTemp
        List lstTemp
        Paragraph par

            this.dataImpressao = new Date().format('dd/MM/yyy HH:mm:ss')

        try {
            // criar e abrir o documento e criar o cabeçalho e o rodapé
            initDoc()

            //String headerText = 'MINISTÉRIO DO MEIO AMBIENTE\n INSTITUTO CHICO MENDES DE CONSERVAÇÃO DA BIODIVERSIDADE\n' + this.unidade.toUpperCase()
            //setHeader(headerText,true)
            //setFooter(this.unidadeSigla)

            /**
             * Adicionar o cabeçalho somente na primeira página
             */
            doPageHeader('MINISTÉRIO DO MEIO AMBIENTE\nINSTITUTO CHICO MENDES DE CONSERVAÇÃO DA BIODIVERSIDADE\nDIRETORIA DE PESQUISA, AVALIAÇÃO E MONITORAMENTO DA BIODIVERSIDADE\n' + this.unidade.toUpperCase())

            //document.setHeader(null)
            setFooter('')
            String titulo = oficina.noOficina
            switch (tipo) {
                case 'final-nao-lc':
                    titulo = 'Relatório Final\n' + titulo + ': Espécies Avaliadas'
                    break;
                case 'final-lc':
                    titulo = 'Relatório Final\n' + titulo + ': Espécies Mantidas LCs'
                    break;
                case 'diario':
                    titulo = 'Relatório Diário\n' + titulo + ': ' + dia
                    break;
            }

            br()
            // titulo
            par = new Paragraph(titulo, titleFont)
            par.setAlignment(Element.ALIGN_CENTER)
            document.add(par)

            br()
            br()

            doRvBold('Data: ', periodo)
            doRvBold('Local: ', Util.capitalize( oficina.deLocal ) )

            br()
            br()

            // gride totais
            table = doTable(['Categorias Avaliadas'], 2, [3f, 1f])
            table.setWidth(70)
            mapTotais.qtdCategoria.each {
                if (it.key.indexOf('Total') == 0) {
                    doRow(table, [it.key, it.value], ['left', 'center'], corCinzaClaro, textBoldFont)
                } else {
                    doRow(table, [it.key, it.value], ['left', 'center'])
                }
            }
            document.add(table)

            br()
            br()
            br()

            // resultado das fichas avaliadas
            //
            table = doTable(['#', 'Taxon', 'Categoria', 'Critério', 'Justificativa'], 0, [0.5f, 1.5f, 1f, 1f, 5f], textBoldFont)
            table.setCellsFitPage(true);

            //Map dadosUltimaAvaliacao
            int row = 1
            String categoriaImprimir
            String criterioImprimir
            String possivelmenteExtinta
            String justificativaImprimir
            Integer quebraFamilia = 0

            OficinaFicha.executeQuery("""select new map(
                  a.ficha.id as sqFicha
                , a.ficha.nmCientifico as noCientifico
                , a.dtAvaliacao as dtAvaliacao
                , ficha.stTransferida as stTransferida
                , taxon.jsonTrilha as jsonTrilha
                , categoria.codigo as cdCategoria
                , a.dsCriterioAvalIucn as dsCriterio
                , a.stPossivelmenteExtinta as stPossivelmenteExtinta
                , a.dsJustificativa as dsJustificativa
                , taxon.noAutor as noAutor
                , taxon.nuAno as nuAno
                , ficha.stManterLc as stManterLc
                , situacaoFicha.codigoSistema as cdSituacaoFicha
                )
                from OficinaFicha a
                join a.ficha ficha
                join a.ficha.taxon taxon
                join a.ficha.situacaoFicha as situacaoFicha
                left join a.categoriaIucn categoria
                where a.oficina.id = ${oficina.id.toString()}
                and ( ficha.stTransferida is null or ficha.stTransferida = false )
                and situacaoFicha.codigoSistema != 'EXCLUIDA'
                """).sort {
                if (ordenadoPorOrdemFamilia == 'S' ) {
                    try {
                        JSONObject dadosTrilha = JSON.parse( it.jsonTrilha )
                        dadosTrilha?.ordem?.no_taxon + ' ' + dadosTrilha?.familia?.no_taxon + '  ' + it.noCientifico
                    } catch (Exception e) {
                        it.noCientifico
                    }
                } else {
                    it.noCientifico
                }
            }.each { item ->
                     categoriaImprimir = item?.cdCategoria ?: ''
                     criterioImprimir = item?.dsCriterio ?: ''
                     possivelmenteExtinta = ""
                     if (categoriaImprimir == 'CR') {
                         possivelmenteExtinta = (item?.stPossivelmenteExtinta == 'S' ? "\n(PEX)" : "")
                     }
                     justificativaImprimir = item?.dsJustificativa ?: ''

                     // imprimir as fichas que possuem avaliação anterior
                     if ((!justificativaImprimir || !categoriaImprimir) && considerarUAN == 'S') {
                         VwFicha vwFicha = VwFicha.get( item.sqFicha )
                         Map dadosUltimaAvaliacao = vwFicha.ultimaAvaliacaoNacional
                         if (dadosUltimaAvaliacao.deCategoriaIucn) {
                             categoriaImprimir = dadosUltimaAvaliacao.coCategoriaIucn
                             criterioImprimir = dadosUltimaAvaliacao.deCriterioAvaliacaoIucn
                             justificativaImprimir = dadosUltimaAvaliacao.txJustificativaAvaliacao
                             if (categoriaImprimir == 'CR') {
                                 possivelmenteExtinta = (dadosUltimaAvaliacao?.stPossivelmenteExtinta == 'S' ? "\n(PEX)" : "")
                             }
                         }
                     }
                     if (categoriaImprimir) {
//                    dadosUltimaAvaliacao=[:]
//                    if( tipo=='final-lc' || tipo=='final-nao-lc')
//                    {
//                        dadosUltimaAvaliacao = it.vwFicha.ultimaAvaliacaoNacional
//                    }

                         boolean valido = false
                         //println tipo+' / ' + it.dtAvaliacao.format('dd/MM/yyyy')+ ' = ' + dia
                         //println ' '
                         // mantiveram LC
                         if (tipo == 'final-lc') {
//                        if( dadosUltimaAvaliacao.coCategoriaIucn == 'LC' && it?.categoriaIucn?.codigoSistema=='LC')
//                        {
//                            valido = true
//                        }
                             valido = (item.stManterLc == true)
                         } else if (tipo == 'final-nao-lc') {
                             // POS_OFICINA = AVALIADA
                             //valido = ( it.vwFicha.cdSituacaoFichaSistema == 'POS_OFICINA' && it.vwFicha.stManterLc != true )
                             valido = true
                         } else if (tipo == 'final-revisadas') {
                             valido = (item?.cdSituacaoFichaSistema == 'AVALIADA_REVISADA')
                         } else if (tipo == 'diario') {
                             if (item?.dtAvaliacao?.format('dd/MM/yyyy') == dia) {
                                 valido = true
                             }
                         } else {
                             valido = true
                         }

                         if (valido) {
                             // nome científico
                             JSONObject dadosJson = JSON.parse( item.jsonTrilha )
                             String noCientifico = dadosJson.subespecie ? dadosJson.subespecie.no_taxon : dadosJson.especie.no_taxon
                             Paragraph parNoCientifico = new Paragraph('', textFont)
                             Chunk chunk = new Chunk(noCientifico, textItalicFont)
                             chunk.getFont().setColor(0, 0, 0)
                             parNoCientifico.add(chunk)

                             // autor do Taxon
                             chunk.getFont().setColor(0, 0, 0) // remover cor
                             chunk = new Chunk(' ' + (item?.noAutor ?: ''), textFont)
                             parNoCientifico.add(chunk)

                             //fazer a quebra por familia
                             if( ordenadoPorOrdemFamilia == 'S' ){
                               if( dadosJson.familia.sq_taxon.toInteger() != quebraFamilia.toInteger() ){
                                doQuebraFamilia( table, dadosJson.familia.no_taxon )
                                quebraFamilia = dadosJson.familia.sq_taxon.toInteger()
                               }
                             }


                             doRow(table, [(row++),
                                           parNoCientifico
                                           , categoriaImprimir + possivelmenteExtinta //it.categoriaIucn.codigo
                                           , criterioImprimir // it.dsCriterioAvalIucn
                                           , justificativaImprimir //it.dsJustificativa
                             ]
                                 , ['center', 'left', 'center', 'center', 'justify'], null, textFont)
                         }
                     }
//--------------------------------------------
                 }
            /*
                oficina.oficinaFichas.sort { it.vwFicha.nmCientifico }.each {
                    !it.stTransferida && it.vwFicha?.situacaoFicha?.codigoSistema != 'EXCLUIDA'
                }.sort { it.vwFicha.nmCientifico }.each {
                    categoriaImprimir = it?.categoriaIucn?.codigo ?: ''
                    criterioImprimir = it.dsCriterioAvalIucn ?: ''
                    possivelmenteExtinta = ""
                    if (categoriaImprimir == 'CR') {
                        possivelmenteExtinta = (it.stPossivelmenteExtinta == 'S' ? "\n(PEX)" : "")
                    }
                    justificativaImprimir = it.dsJustificativa ?: ''

                    // imprimir as fichas que possuem avaliação anterior
                    if ((!justificativaImprimir || !categoriaImprimir) && considerarUAN == 'S') {
                        Map dadosUltimaAvaliacao = it.vwFicha.ultimaAvaliacaoNacional
                        if (dadosUltimaAvaliacao.deCategoriaIucn) {
                            categoriaImprimir = dadosUltimaAvaliacao.coCategoriaIucn
                            criterioImprimir = dadosUltimaAvaliacao.deCriterioAvaliacaoIucn
                            justificativaImprimir = dadosUltimaAvaliacao.txJustificativaAvaliacao
                            if (categoriaImprimir == 'CR') {
                                possivelmenteExtinta = (dadosUltimaAvaliacao?.stPossivelmenteExtinta == 'S' ? "\n(PEX)" : "")
                            }
                        }
                    }
                    if (categoriaImprimir) {
//                    dadosUltimaAvaliacao=[:]
//                    if( tipo=='final-lc' || tipo=='final-nao-lc')
//                    {
//                        dadosUltimaAvaliacao = it.vwFicha.ultimaAvaliacaoNacional
//                    }

                        boolean valido = false
                        //println tipo+' / ' + it.dtAvaliacao.format('dd/MM/yyyy')+ ' = ' + dia
                        //println ' '
                        // mantiveram LC
                        if (tipo == 'final-lc') {
//                        if( dadosUltimaAvaliacao.coCategoriaIucn == 'LC' && it?.categoriaIucn?.codigoSistema=='LC')
//                        {
//                            valido = true
//                        }

                            valido = (it.vwFicha.stManterLc == true)
                        } else if (tipo == 'final-nao-lc') {
                            // POS_OFICINA = AVALIADA
                            //valido = ( it.vwFicha.cdSituacaoFichaSistema == 'POS_OFICINA' && it.vwFicha.stManterLc != true )
                            valido = true
                        } else if (tipo == 'final-revisadas') {
                            valido = (it.vwFicha.cdSituacaoFichaSistema == 'AVALIADA_REVISADA')
                        } else if (tipo == 'diario') {
                            if (it.dtAvaliacao.format('dd/MM/yyyy') == dia) {
                                valido = true
                            }
                        } else {
                            valido = true
                        }

                        if (valido) {
                            // nome científico
                            Paragraph parNoCientifico = new Paragraph('', textFont)
                            Chunk chunk = new Chunk(it.vwFicha.taxon.noCientifico, textItalicFont)
                            chunk.getFont().setColor(0, 0, 0)
                            parNoCientifico.add(chunk)

                            // autor do Taxon
                            chunk.getFont().setColor(0, 0, 0) // remover cor
                            chunk = new Chunk(' ' + (it.vwFicha?.taxon?.noAutor ?: ''), textFont)
                            parNoCientifico.add(chunk)
                            doRow(table, [(row++),
                                          parNoCientifico
                                          , categoriaImprimir + possivelmenteExtinta //it.categoriaIucn.codigo
                                          , criterioImprimir // it.dsCriterioAvalIucn
                                          , justificativaImprimir //it.dsJustificativa
                            ]
                                , ['center', 'left', 'center', 'center', 'justify'], null, textFont)
                        }
                    }
                }
            }*/
            document.add(table)

            if (table.size() == 1) {
                if (tipo == 'diario') {
                    par = new Paragraph('Nenhuma ficha avaliada em ' + dia, textBoldFont)
                } else {
                    par = new Paragraph('Nenhuma ficha avaliada', textBoldFont)
                }
                par.setAlignment(Element.ALIGN_CENTER)
                document.add(par)
            }

            br()

            // fim relatorio final
            par = new Paragraph(Util.capitalize( oficina.deLocal ) +', '+ Util.capitalize( new Date().format("dd 'de' MMMM 'de' yyyy") )  + '.', textFont)
            par.setAlignment(Element.ALIGN_RIGHT)
            document.add(par)

            br()

            // participantes
            Integer oldFontSize = par.getFont().getSize();
            par = new Paragraph('Participantes', textBoldFont)
            par.getFont().setSize(oldFontSize + 4i)
            par.setAlignment(Element.ALIGN_CENTER)
            document.add(par)
            par.getFont().setSize(oldFontSize)
            mapPapeis.each {
                br()
                par = new Paragraph(it.key + (it.value.size() > 1 && (it.key =~ /or$/) ? 'es' : ''), textBoldFont)
                //par = new Paragraph( it.key, textBoldFont )
                par.setAlignment(Element.ALIGN_LEFT)
                document.add(par)
                it.value.eachWithIndex { nome, i ->
                    // complementar com linhas no final para assinar
                    //par = new Paragraph("\t" + (i + 1) + ') ' + (nome + ' ').toString().padRight(75, '_'), textFont)

                    par = new Paragraph("\t" + (i + 1) + ') ' + (nome + ' ').toString(), textFont)
                    par.setAlignment(Element.ALIGN_LEFT)
                    document.add(par)
                    //br()
                }
            }

            // fim
            document.close()
        }
        catch (Exception e) {
            fileName = ''
            println e.getMessage()
            //e.printStackTrace()
        }
        return fileName
    }

    /**
     * Relatório das fichas mantidas LC
     * @return
     */
    String runDocLC( Map data = [:]) {
        String agora = new Date().format('dd-MM-yyyy-hh-mm-ss')
        String nomeArquivo = 'lcs'
        this.fileName = this.tmpDir + 'rel_' + nomeArquivo + '-oficina-' + agora + '.rtf'
        String strTemp
        List lstTemp
        Paragraph par
        try {
            this.dataImpressao = new Date().format('dd/MM/yyyy HH:mm:ss')

            // criar e abrir o documento e criar o cabeçalho e o rodapé
            initDoc()
            setHeader('MINISTÉRIO DO MEIO AMBIENTE\n'
                    + 'INSTITUTO CHICO MENDES DE CONSERVAÇÃO DA BIODIVERSIDADE\n'
                    + 'DIRETORIA DE PESQUISA, AVALIAÇÃO E MONITORAMENTO DA BIODIVERSIDADE\n'
                    + this.unidade.toUpperCase(),true
            )
            //setFooter(this.unidadeSigla)
            setFooter('')
            String titulo = 'Grade de Espécies com Indicação de Manutenção da Categoria LC\n' + oficina.noOficina

            // titulo
            par = new Paragraph(titulo, titleFont)
            par.setAlignment(Element.ALIGN_CENTER)
            document.add(par)

            br()

            doRvBold('Data.: ', new Date().format('dd/MM/yyyy').toUpperCase() )
            doRvBold('Local: ', Util.capitalize( oficina.deLocal) )

            br()

            // selecionar as fichas
            String tituloColunaJustificativa = (data.imprimirJustificativaOficina =='S'?'Justificativa / Minuta':'Justificativa')
            Table table = doTable(['#', 'Taxon', tituloColunaJustificativa,'Observação'], 0, [0.5f, 1.5f, 4f , 1f], textBoldFont)
            //Map dadosUltimaAvaliacao
            int row = 1

            List fichasMarcadasPeloUsuario = []
            if( data.ids ) {
                fichasMarcadasPeloUsuario = data.ids.split(',').collect { it.toLong() }
            }
            oficina.oficinaFichas.findAll {
                ! it.stTransferida &&
                it.vwFicha?.situacaoFicha?.codigoSistema != 'EXCLUIDA' &&
                it.vwFicha.stManterLc == true &&
                ( fichasMarcadasPeloUsuario.size() == 0 || fichasMarcadasPeloUsuario.contains( it.vwFicha.id.toLong() ) )
            }.sort { it.vwFicha.nmCientifico }.each {

                String justificativaImprimir = ''
                if( data.imprimirJustificativaOficina == 'P' ) {
                    if (it.categoriaIucn) {
                        justificativaImprimir = it.dsJustificativa ?: ''
                    } else {
                        Map ultimaAvaliacao = it.ficha.ultimaAvaliacaoNacional
                        justificativaImprimir = ultimaAvaliacao.txJustificativaAvaliacao ?: ''
                    }
                } else if( data.imprimirJustificativaOficina =='S') {
                        justificativaImprimir = it.dsJustificativa ?: ''
                } else if( data.imprimirJustificativaOficina =='N') {
                    Map ultimaAvaliacao = it.ficha.ultimaAvaliacaoNacional
                    justificativaImprimir = ultimaAvaliacao.txJustificativaAvaliacao ?: ''
                }

                // nome científico
                Paragraph parNoCientifico = new Paragraph('', textFont)
                Chunk chunk = new Chunk(it.vwFicha.taxon.noCientifico, textItalicFont)
                chunk.getFont().setColor(0, 0, 0)
                parNoCientifico.add( chunk )

                // autor do Taxon
                chunk.getFont().setColor(0, 0, 0) // remover cor
                chunk = new Chunk(' ' + (it.vwFicha?.taxon?.noAutor ?: ''), textFont)
                parNoCientifico.add( chunk )

                doRow(table, [(row++),
                              parNoCientifico,
                              justificativaImprimir
                              ,''
                ]
                , ['center', 'left', 'justify', 'center'], null, textFont)
            }
            document.add( table )

            // fim
            document.close()
        } catch (Exception e) {
            fileName = ''
            println e.getMessage()
            //e.printStackTrace()
        }
        return fileName
    }

    void doQuebraFamilia(Table table, String familia ='' ) {
        Chunk chunk1 = new Chunk(' ')
        Cell cell1 = new Cell(chunk1)
        cell1.setBackgroundColor(corFundoVerde)
        Chunk chunk2 = new Chunk(familia, textBoldFont)
        Cell cell2 = new Cell(chunk2)
        cell2.setColspan(4)
        cell2.setBackgroundColor(corFundoVerde)
        cell2.setHorizontalAlignment(Element.ALIGN_LEFT)
        cell2.setVerticalAlignment(Element.ALIGN_MIDDLE)
        table.addCell(cell1)
        table.addCell(cell2)
    }

}
