--
-- PostgreSQL database dump
--

-- Dumped from database version 10.23
-- Dumped by pg_dump version 14.6 (Ubuntu 14.6-0ubuntu0.22.04.1)

-- Started on 2023-02-05 18:49:13 -03

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 6 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA if not exists public;


--
-- TOC entry 7874 (class 0 OID 0)
-- Dependencies: 6
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- TOC entry 7 (class 2615 OID 16386)
-- Name: salve; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA if not exists salve;


--
-- TOC entry 12 (class 2615 OID 19374)
-- Name: taxonomia; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA if not exists taxonomia;


--
-- TOC entry 1767 (class 1255 OID 17987)
-- Name: fn_remove_acentuacao(character varying); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.fn_remove_acentuacao(character varying) RETURNS character varying
    LANGUAGE sql IMMUTABLE COST 1
    AS $_$
SELECT TRANSLATE($1, 'áéíóúàèìòùãõâêîôôäëïöüçÁÉÍÓÚÀÈÌÒÙÃÕÂÊÎÔÛÄËÏÖÜÇ', 'aeiouaeiouaoaeiooaeioucAEIOUAEIOUAOAEIOOAEIOUC')
$_$;


--
-- TOC entry 1778 (class 1255 OID 28771)
-- Name: calc_base_dados_portalbio(text, text); Type: FUNCTION; Schema: salve; Owner: -
--

CREATE FUNCTION salve.calc_base_dados_portalbio(tx_ocorrencia text, uuid text) RETURNS text
    LANGUAGE sql
    AS $$
SELECT case when tx_ocorrencia ilike '%Atlas de Registros de Aves Brasileiras%' then 'ARA' else
    case when tx_ocorrencia ilike '%SIMMAM%' then 'SIMMAM' else
        case when tx_ocorrencia ilike '%Sisquelonios%' then 'SISQUELONIOS' else
            case when tx_ocorrencia ilike '%SITAMAR%' then 'SITAMAR' else
                case when tx_ocorrencia ilike '%SISBIO%' then 'SISBIO' else
                    case when uuid ilike '%SISBIO%' then 'SISBIO' else '' end
                    end
                end
            end
        end
           end
$$;


--
-- TOC entry 7875 (class 0 OID 0)
-- Dependencies: 1778
-- Name: FUNCTION calc_base_dados_portalbio(tx_ocorrencia text, uuid text); Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON FUNCTION salve.calc_base_dados_portalbio(tx_ocorrencia text, uuid text) IS 'Função calcular o nome da base dados original do registro de ocorrencia importados do PORTALBIO.';


--
-- TOC entry 1779 (class 1255 OID 28772)
-- Name: calc_carencia(date, text); Type: FUNCTION; Schema: salve; Owner: -
--

CREATE FUNCTION salve.calc_carencia(data date, prazo text) RETURNS date
    LANGUAGE sql
    AS $$
SELECT CASE
           WHEN prazo IS NOT NULL
               AND prazo <> 'SEM_CARENCIA'::text
               AND data IS NOT NULL THEN CASE
                                             WHEN prazo = '1_ANO'::text
                                                 THEN data + '1 year'::interval
                                             ELSE CASE
                                                      WHEN prazo = '2_ANOS'::text
                                                          THEN data + '2 years'::interval
                                                      ELSE CASE
                                                               WHEN prazo = '3_ANOS'::text
                                                                   THEN data + '3 years'::interval
                                                               ELSE NULL::TIMESTAMP WITHOUT TIME ZONE
                                                          END
                                                 END
               END
           ELSE NULL::TIMESTAMP WITHOUT TIME ZONE
           END :: date
$$;


--
-- TOC entry 7876 (class 0 OID 0)
-- Dependencies: 1779
-- Name: FUNCTION calc_carencia(data date, prazo text); Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON FUNCTION salve.calc_carencia(data date, prazo text) IS 'Função calcular a data final da carencia baseado no periodo.';


--
-- TOC entry 1793 (class 1255 OID 29106)
-- Name: entity2char(text); Type: FUNCTION; Schema: salve; Owner: -
--

CREATE FUNCTION salve.entity2char(t text) RETURNS text
    LANGUAGE plpgsql IMMUTABLE
    AS $$
declare
    r record;
begin
    for r in
        select distinct ce.ch, ce.name
        from
            salve.character_entity ce
                inner join (
                select name[1] "name"
                from regexp_matches(t, '&([A-Za-z]+?);', 'g') r(name)
            ) s on ce.name = s.name
        loop
            t := replace(t, '&' || r.name || ';', r.ch);
        end loop;

    for r in
        select distinct
            hex[1] hex,
            ('x' || repeat('0', 8 - length(hex[1])) || hex[1])::bit(32)::int codepoint
        from regexp_matches(t, '&#x([0-9a-f]{1,8}?);', 'gi') s(hex)
        loop
            t := regexp_replace(t, '&#x' || r.hex || ';', chr(r.codepoint), 'gi');
        end loop;

    for r in
        select distinct
            chr(codepoint[1]::int) ch,
            codepoint[1] codepoint
        from regexp_matches(t, '&#([0-9]{1,10}?);', 'g') s(codepoint)
        loop
            t := replace(t, '&#' || r.codepoint || ';', r.ch);
        end loop;

    return t;
end;
$$;


--
-- TOC entry 7877 (class 0 OID 0)
-- Dependencies: 1793
-- Name: FUNCTION entity2char(t text); Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON FUNCTION salve.entity2char(t text) IS 'Converte todas as entidades HTML para os seus caracteres correspondentes';


--
-- TOC entry 1815 (class 1255 OID 29730)
-- Name: fn_array_unique(text[]); Type: FUNCTION; Schema: salve; Owner: -
--

CREATE FUNCTION salve.fn_array_unique(a text[]) RETURNS text[]
    LANGUAGE sql
    AS $$
select array (
               select distinct v from unnest(a) as b(v)
           )
$$;


--
-- TOC entry 1790 (class 1255 OID 29059)
-- Name: fn_calc_situacao_registro(text, boolean); Type: FUNCTION; Schema: salve; Owner: -
--

CREATE FUNCTION salve.fn_calc_situacao_registro(in_utilizado_avaliacao text, st_adicionado_apos_avaliacao boolean) RETURNS text
    LANGUAGE plpgsql
    AS $$
begin
    if in_utilizado_avaliacao='S' then
        if st_adicionado_apos_avaliacao then
            return 'Registro adicionado após a avaliação';
        else
            return 'Registro utilizado na avaliação';
        end if;
    else
        if in_utilizado_avaliacao='N' then
            return 'Registro não utilizado na avaliação';
        elseif in_utilizado_avaliacao='' then
            return 'Registro não confirmado';
        else
            return '';
        end if;
    end if;
end;
$$;


--
-- TOC entry 7878 (class 0 OID 0)
-- Dependencies: 1790
-- Name: FUNCTION fn_calc_situacao_registro(in_utilizado_avaliacao text, st_adicionado_apos_avaliacao boolean); Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON FUNCTION salve.fn_calc_situacao_registro(in_utilizado_avaliacao text, st_adicionado_apos_avaliacao boolean) IS 'Função para calcular a situação do registro em relação a sua utilização na avaliação';


--
-- TOC entry 1810 (class 1255 OID 29610)
-- Name: fn_ficha2json(bigint); Type: FUNCTION; Schema: salve; Owner: -
--

CREATE FUNCTION salve.fn_ficha2json(p_sq_ficha bigint) RETURNS json
    LANGUAGE plpgsql
    AS $$begin
    return  (
        select json_build_object(
                       'sq_ficha', ficha.sq_ficha
                   , '_id', ficha.sq_ficha
                   , 'ds_doi',ficha.ds_doi
                   , 'sq_unidade_responsavel',ficha.sq_unidade_org
                   , 'sg_unidade_responsavel',uo.sg_unidade_org
                   , 'no_unidade_responsavel',uo.no_pessoa
                   , 'nm_cientifico',ficha.nm_cientifico
                   , 'taxon_trilha', taxon.json_trilha
                   , 'no_autor_taxon',taxon.no_autor
                   , 'nu_ano_taxon',taxon.nu_ano
                   , 'st_manter_lc',ficha.st_manter_lc
                   , 'dt_aceite_validacao',ficha.dt_aceite_validacao
                   -- aba 1
                   , 'classificacao_taxonomica', json_build_object('nomes_comuns',
                                                                   (select json_agg(json_build_object(
                                                                           'no_comum', ficha_nome_comum.no_comum
                                                                       ,'de_regiao_lingua',salve.ficha_nome_comum.de_regiao_lingua
                                                                       , 'referencias', salve.fn_ref_bib_json(
                                                                                   ficha_nome_comum.sq_ficha,
                                                                                   ficha_nome_comum.sq_ficha_nome_comum)
                                                                       ))
                                                                    from salve.ficha_nome_comum
                                                                    where ficha_nome_comum.sq_ficha = ficha.sq_ficha
                                                                   )
                           -- sinonimias
                           , 'nomes_antigos',
                                                                   (select json_agg(json_build_object('no_antigo',
                                                                                                      sinonimia.no_sinonimia
                                                                       , 'no_autor', sinonimia.no_autor
                                                                       , 'nu_ano', sinonimia.nu_ano)) as json_sinonimia
                                                                    from salve.ficha_sinonimia as sinonimia
                                                                    where sinonimia.sq_ficha = ficha.sq_ficha)
                           , 'notas_taxonomicas', salve.entity2char(ficha.ds_notas_taxonomicas)
                           , 'notas_morfologicas', salve.entity2char(ficha.ds_diagnostico_morfologico)
                           , 'grupo',
                                                                   json_build_object('sq_grupo', grupo.sq_dados_apoio,
                                                                                     'ds_grupo', grupo.ds_dados_apoio,
                                                                                     'de_grupo_ordem', grupo.de_dados_apoio_ordem,
                                                                                     'cd_grupo_sistema', grupo.cd_sistema)
                           , 'subgrupo', json_build_object('sq_subgrupo', subgrupo.sq_dados_apoio, 'ds_subgrupo',
                                                           subgrupo.ds_dados_apoio)
                           , 'recorte_publico', json_build_object('sq_recorte_publico', grupo_recorte.sq_dados_apoio,
                                                                  'ds_recorte_publico', grupo_recorte.ds_dados_apoio,
                                                                  'de_recorte_publico_ordem', grupo_recorte.de_dados_apoio_ordem,
                                                                  'cd_recorte_publico_sistema', grupo_recorte.cd_sistema)
                           )
                   -- aba 2

                   ,'distribuicao',
                       json_build_object('st_endemica_brasil', ficha.st_endemica_brasil
                           , 'ds_distribuicao_geo_global', salve.entity2char(ficha.ds_distribuicao_geo_global)
                           , 'ds_distribuicao_geo_nacional', salve.entity2char(ficha.ds_distribuicao_geo_nacional)
                           , 'nu_min_altitude', ficha.nu_min_altitude::text
                           , 'nu_max_altitude', ficha.nu_max_altitude::text
                           , 'nu_min_batimetria', ficha.nu_min_batimetria::text
                           , 'nu_max_batimetria', ficha.nu_max_batimetria::text
                           -- mapas distribuicao
                           , 'mapas_distribuicao', (select json_agg(json_build_object('de_legenda', anexo.de_legenda
                           , 'no_arquivo', anexo.no_arquivo
                           , 'de_tipo_conteudo', anexo.de_tipo_conteudo
                           , 'de_local_arquivo', anexo.de_local_arquivo
                           , 'in_principal', anexo.in_principal
                           , 'dt_anexo', anexo.dt_anexo
                           ))
                                                    from salve.ficha_anexo anexo
                                                    where sq_ficha = ficha.sq_ficha
                                                      and anexo.sq_contexto = 678
                                         )
                           -- ufs
                           , 'estados', (select json_agg( json_build_object('no_contexto',no_contexto
                           ,'no_origem',no_origem
                           ,'sq_estado',sq_estado
                           ,'no_estado',no_estado
                           ,'sg_estado',sg_estado
                           ,'no_regiao',no_regiao
                           ,'st_adicionado_apos_avaliacao',st_adicionado_apos_avaliacao
                           ,'st_em_carencia',st_em_carencia
                           ,'dt_carencia',dt_carencia
                           ,'referencias',js_ref_bibs
                           ) ) from salve.fn_ficha_estados(ficha.sq_ficha)
                                         )
                           -- biomas
                           , 'biomas', (select json_agg( json_build_object('no_contexto',no_contexto
                           ,'no_origem',no_origem
                           ,'sq_bioma',sq_bioma_salve
                           ,'sq_bioma_corp',sq_bioma_corp
                           ,'no_bioma',no_bioma
                           ,'st_adicionado_apos_avaliacao',st_adicionado_apos_avaliacao
                           ,'st_em_carencia',st_em_carencia
                           ,'dt_carencia',dt_carencia
                           ,'referencias',js_ref_bibs
                           ) ) from salve.fn_ficha_biomas(ficha.sq_ficha)
                                         )
                           -- bacias
                           , 'bacias', (select json_agg( json_build_object('no_contexto',no_contexto
                           ,'no_origem',no_origem
                           ,'sq_bacia',sq_bacia
                           ,'cd_sistema',cd_sistema
                           ,'cd_bacia',cd_bacia
                           ,'no_bacia',no_bacia
                           ,'tx_trilha',tx_trilha
                           ,'referencias',js_ref_bibs
                           ,'st_adicionado_apos_avaliacao',st_adicionado_apos_avaliacao
                           ,'st_em_carencia',st_em_carencia
                           ,'dt_carencia',dt_carencia
                           ) ) from salve.fn_ficha_bacias(ficha.sq_ficha))

                           -- ambiente restrito
                           , 'ambiente_restrito',( select json_agg(json_build_object('sq_restricao_habitat', f.sq_restricao_habitat
                           , 'no_restricao_habitat', apoio.ds_dados_apoio
                           , 'tx_ficha_restricao_habitat', f.tx_ficha_restricao_habitat
                           ,'cavernas',( select json_agg(json_build_object('sq_caverna',far.sq_caverna, 'no_caverna',cav.no_caverna, 'sq_estado',cav.sg_estado, 'sg_estado',cav.no_estado))
                                         from salve.ficha_amb_restrito_caverna far
                                                  inner join canie.vw_caverna_localidade cav on cav.sq_caverna = far.sq_caverna
                                         where far.sq_ficha_ambiente_restrito = f.sq_ficha_ambiente_restrito
                                                                                     )
                           , 'referencias', salve.fn_ref_bib_json ( f.sq_ficha, f.sq_ficha_ambiente_restrito)
                           ) )
                                                   from salve.ficha_ambiente_restrito f
                                                            inner join salve.dados_apoio as apoio on apoio.sq_dados_apoio = f.sq_restricao_habitat
                                                   where f.sq_ficha = ficha.sq_ficha
                                         )
                           -- areas relevantes
                           , 'areas_relevantes',( select json_agg(json_build_object('sq_tipo_relevancia', far.sq_tipo_relevancia
                           , 'ds_area_relevante', apoio.ds_dados_apoio
                           , 'sq_estado', uf.sq_estado
                           , 'no_estado', uf.no_estado
                           , 'sg_estado', uf.sg_estado
                           , 'tx_local', far.tx_local
                           , 'referencias', salve.fn_ref_bib_json ( far.sq_ficha, far.sq_ficha_area_relevancia)
                           , 'municipios', (select json_agg(json_build_object('sq_municipio', m.sq_municipio
                                   , 'no_municipio', m.no_municipio
                                   , 'tx_relevancia_municipio', salve.entity2char(farm.tx_relevancia_municipio)
                                   ))
                                            from salve.ficha_area_relevancia_municipio farm
                                                     inner join corporativo.vw_municipio m on m.sq_municipio = farm.sq_municipio
                                            where farm.sq_ficha_area_relevancia = far.sq_ficha_area_relevancia
                                                                                    )
                           ))
                                                  from salve.ficha_area_relevancia far
                                                           inner join salve.dados_apoio as apoio on apoio.sq_dados_apoio = far.sq_tipo_relevancia
                                                           left outer join corporativo.vw_estado uf on uf.sq_estado = far.sq_estado
                                                  where sq_ficha = ficha.sq_ficha
                                         )
                           -- registros de ocorrencia
                           , 'ocorrencias', 'gravadas na tabela ocorrencias_json'
                           )
                   -- fim  aba 2

                   ,'historia_natural',json_build_object( 'st_migratoria', ficha.st_migratoria
                           , 'ds_historia_natural', salve.entity2char(ficha.ds_historia_natural)
                           -- aba 3.1
                           , 'st_habito_aliment_especialista', ficha.st_habito_aliment_especialista
                           , 'tipos_habitos_alimentares', (select json_agg( json_build_object( 'sq_tipo_habito_alimentar', a.sq_tipo_habito_alimentar
                            , 'ds_tipo_habito_alimentar', apoio.ds_dados_apoio
                            , 'referencias',salve.fn_ref_bib_json( a.sq_ficha, a.sq_ficha_habito_alimentar )
                            ))
                                                           from salve.ficha_habito_alimentar a
                                                                    inner join salve.dados_apoio as apoio on apoio.sq_dados_apoio = a.sq_tipo_habito_alimentar
                                                           where a.sq_ficha = ficha.sq_ficha
                                                          )
                           , 'ds_habito_alimentar', salve.entity2char(ficha.ds_habito_alimentar)
                           -- aba 3.2
                           , 'habitats', (select json_agg( json_build_object('sq_habitat_pai', apoio.sq_dados_apoio_pai, 'sq_habitat', a.sq_habitat
                            , 'ds_habitat', apoio.ds_dados_apoio
                            , 'referencias', salve.fn_ref_bib_json( a.sq_ficha,a.sq_ficha_habitat)
                            ))
                                          from salve.ficha_habitat a
                                                   inner join salve.dados_apoio as apoio on apoio.sq_dados_apoio = a.sq_habitat
                                          where a.sq_ficha = ficha.sq_ficha
                                                          )
                           , 'st_restrito_habitat_primario', ficha.st_restrito_habitat_primario
                           , 'st_especialista_micro_habitat', ficha.st_especialista_micro_habitat
                           , 'st_variacao_sazonal_habitat', ficha.st_variacao_sazonal_habitat
                           , 'st_difer_macho_femea_habitat', ficha.st_difer_macho_femea_habitat
                           , 'ds_uso_habitat', salve.entity2char(ficha.ds_uso_habitat)
                           -- aba 3.3
                           , 'interacao',json_build_object( 'ds_interacao', salve.entity2char(ficha.ds_interacao)
                                                              , 'interacoes', ( select json_agg(json_build_object('sq_tipo_interacao', a.sq_tipo_interacao
                                    , 'ds_tipo_interacao', apoio.ds_dados_apoio
                                    , 'taxon', a.sq_taxon
                                    , 'taxon_trilha', taxon.json_trilha
                                    , 'taxon_autor', taxon.no_autor
                                    , 'taxon_ano', taxon.nu_ano
                                    , 'sq_categoria_iucn', a.sq_categoria_iucn
                                    , 'ds_ficha_interacao', a.ds_ficha_interacao
                                    , 'referencias', salve.fn_ref_bib_json(   a.sq_ficha,a.sq_ficha_interacao)
                                    ))
                                                                                from salve.ficha_interacao a
                                                                                         inner join salve.dados_apoio as apoio on apoio.sq_dados_apoio = a.sq_tipo_interacao
                                                                                         left outer join taxonomia.taxon on taxon.sq_taxon = a.sq_taxon
                                                                                where sq_ficha = ficha.sq_ficha
                                                            )
                                                              )
                           -- aba 3.4
                           ,'reproducao',json_build_object('ds_reproducao', salve.entity2char(ficha.ds_reproducao)
                                                              , 'sq_modo_reproducao', ficha.sq_modo_reproducao::text
                                                              , 'ds_modo_reproducao', modo_reproducao.ds_dados_apoio
                                                              , 'sq_sistema_acasalamento', ficha.sq_sistema_acasalamento::text
                                                              , 'ds_sistema_acasalamento', sistema_acasalamento.ds_dados_apoio
                                                              , 'vl_intervalo_nascimento', ficha.vl_intervalo_nascimento::text::text
                                                              , 'sq_unid_intervalo_nascimento', ficha.sq_unid_intervalo_nascimento::text
                                                              , 'ds_unid_intervalo_nascimento', unid_intervalo_nascimento.ds_dados_apoio
                                                              , 'vl_tempo_gestacao', ficha.vl_tempo_gestacao::text
                                                              , 'sq_unid_tempo_gestacao', ficha.sq_unid_tempo_gestacao::text
                                                              , 'vl_tamanho_prole', ficha.vl_tamanho_prole::text
                                                              -- macho
                                                              , 'vl_maturidade_sexual_macho', ficha.vl_maturidade_sexual_macho::text
                                                              , 'sq_unid_maturidade_sexual_macho', ficha.sq_unid_maturidade_sexual_macho::text
                                                              , 'ds_unid_maturidade_sexual_macho', unid_maturidade_sexual_macho.ds_dados_apoio
                                                              , 'vl_peso_macho', ficha.vl_peso_macho::text
                                                              , 'sq_unidade_peso_macho', ficha.sq_unidade_peso_macho::text
                                                              , 'ds_unidade_peso_macho', unid_peso_macho.ds_dados_apoio
                                                              , 'vl_comprimento_macho_max', ficha.vl_comprimento_macho_max::text
                                                              , 'sq_medida_comprimento_macho_max', ficha.sq_medida_comprimento_macho_max::text
                                                              , 'ds_medida_comprimento_macho_max', unid_medida_comprimento_macho_max.ds_dados_apoio
                                                              , 'vl_comprimento_macho', ficha.vl_comprimento_macho::text
                                                              , 'sq_medida_comprimento_macho', ficha.sq_medida_comprimento_macho::text
                                                              , 'ds_medida_comprimento_macho', unid_medida_comprimento_macho.ds_dados_apoio
                                                              , 'vl_senilidade_reprodutiva_macho', ficha.vl_senilidade_reprodutiva_macho::text
                                                              , 'sq_unidade_senilid_rep_macho', ficha.sq_unidade_senilid_rep_macho::text
                                                              , 'ds_unidade_senilid_rep_macho', unid_senilid_rep_macho.ds_dados_apoio
                                                              , 'vl_senilidade_reprodutiva_macho', ficha.vl_senilidade_reprodutiva_macho::text
                                                              , 'sq_unidade_longevidade_macho', ficha.sq_unidade_senilid_rep_macho::text
                                                              , 'sq_unidade_longevidade_macho', unid_longevidade_macho.ds_dados_apoio

                                                              -- femea
                                                              , 'vl_maturidade_sexual_femea', ficha.vl_maturidade_sexual_femea::text
                                                              , 'sq_unid_maturidade_sexual_femea', ficha.sq_unid_maturidade_sexual_femea::text
                                                              , 'ds_unid_maturidade_sexual_femea', unid_maturidade_sexual_femea.ds_dados_apoio
                                                              , 'vl_peso_femea', ficha.vl_peso_femea::text
                                                              , 'sq_unidade_peso_femea', ficha.sq_unidade_peso_femea::text
                                                              , 'ds_unidade_peso_femea', unid_peso_femea.ds_dados_apoio
                                                              , 'vl_comprimento_femea_max', ficha.vl_comprimento_femea_max::text
                                                              , 'sq_medida_comprimento_femea_max', ficha.sq_medida_comprimento_femea_max::text
                                                              , 'ds_medida_comprimento_femea_max', unid_medida_comprimento_femea_max.ds_dados_apoio
                                                              , 'vl_comprimento_femea', ficha.vl_comprimento_femea::text
                                                              , 'sq_medida_comprimento_femea', ficha.sq_medida_comprimento_femea::text
                                                              , 'ds_medida_comprimento_femea', unid_medida_comprimento_femea.ds_dados_apoio
                                                              , 'vl_senilidade_reprodutiva_femea', ficha.vl_senilidade_reprodutiva_femea::text
                                                              , 'sq_unidade_senilid_rep_femea', ficha.sq_unidade_senilid_rep_femea::text
                                                              , 'ds_unidade_senilid_rep_femea', unid_senilid_rep_femea.ds_dados_apoio
                                                              , 'vl_senilidade_reprodutiva_femea', ficha.vl_senilidade_reprodutiva_femea::text
                                                              , 'sq_unidade_longevidade_femea', ficha.sq_unidade_senilid_rep_femea::text
                                                              , 'sq_unidade_longevidade_femea', unid_longevidade_femea.ds_dados_apoio
                                                              )
                           -- 3.5
                           ,'sazonalidade',( select json_agg( json_build_object(
                                'sq_fase_vida', fase_vida.sq_dados_apoio,
                                'ds_fase_vida', fase_vida.ds_dados_apoio,
                                'sq_epoca', epoca.sq_dados_apoio,
                                'ds_epoca', epoca.ds_dados_apoio,
                                'sq_habitat_pai',habitat.sq_dados_apoio_pai,
                                'sq_habitat', habitat.sq_dados_apoio,
                                'ds_habitat',habitat.ds_dados_apoio,
                                'referencias',salve.fn_ref_bib_json( a.sq_ficha, a.sq_ficha_variacao_sazonal)
                            ))
                                             from salve.ficha_variacao_sazonal a
                                                      inner join salve.dados_apoio as fase_vida on fase_vida.sq_dados_apoio = a.sq_fase_vida
                                                      inner join salve.dados_apoio as epoca on epoca.sq_dados_apoio = a.sq_epoca
                                                      inner join salve.dados_apoio as habitat on habitat.sq_dados_apoio = a.sq_habitat
                                             where a.sq_ficha = ficha.sq_ficha
                                                          )
                           )
                   -- fim aba 3
                   -- inicio aba 4
                   ,'populacao',json_build_object('sq_tendencia_populacional',ficha.sq_tendencia_populacional
                           ,'ds_tendencia_populacional',tendencia_populacional.ds_dados_apoio
                           ,'ds_populacao', salve.entity2char(ficha.ds_populacao)
                           ,'vl_tempo_geracional',ficha.vl_tempo_geracional
                           ,'sq_medida_tempo_geracional',ficha.sq_medida_tempo_geracional
                           ,'ds_unidade_tempo_geracional',unid_tempo_geracional.ds_dados_apoio
                           ,'ds_metod_calc_tempo_geracional',salve.entity2char(ficha.ds_metod_calc_tempo_geracional)
                           ,'anexos', ( select json_agg( json_build_object('de_legenda', a.de_legenda, 'no_arquivo', a.no_arquivo
                            , 'dt_anexo', a.dt_anexo
                            , 'de_local_arquivo', a.de_local_arquivo
                            , 'de_tipo_conteudo', a.de_tipo_conteudo
                            , 'in_principal', a.in_principal
                            ))
                                        from salve.ficha_anexo a
                                        where a.sq_ficha = ficha.sq_ficha
                                          and a.sq_contexto = 677
                                                  )
                           -- aba 4.1
                           ,'informacao_local_regional',( select json_agg(json_build_object('sq_abrangencia', a.sq_abrangencia
                            , 'ds_unidade_abrangencia', unid.ds_dados_apoio
                            , 'ds_local', a.ds_local
                            , 'ds_mes_ano_inicio', a.ds_mes_ano_inicio
                            , 'ds_mes_ano_fim', a.ds_mes_ano_fim
                            , 'ds_estimativa_populacao', a.ds_estimativa_populacao
                            , 'de_valor_abundancia', a.de_valor_abundancia
                            , 'referencias', salve.fn_ref_bib_json(a.sq_ficha, a.sq_ficha_popul_estimada_local)
                            ) )
                                                          from salve.ficha_popul_estimada_local a
                                                                   inner join salve.abrangencia abrangencia on abrangencia.sq_abrangencia = a.sq_abrangencia
                                                                   left outer join salve.dados_apoio unid on unid.sq_dados_apoio = a.sq_unid_abundancia_populacao
                                                          where a.sq_ficha = ficha.sq_ficha
                                                  )
                           -- aba 4.2
                           ,'area_vida',( select json_agg( json_build_object('sq_unidade_area',a.sq_unidade_area
                            ,'ds_unidade_area',unid.ds_dados_apoio
                            ,'tx_local',a.tx_local
                            ,'tx_area', a.tx_area
                            ,'vl_area', a.vl_area
                            ,'nu_ano',a.nu_ano
                            ,'referencia',salve.fn_ref_bib_json(a.sq_ficha, a.sq_ficha_area_vida)
                            ) )
                                          from salve.ficha_area_vida a
                                                   left outer join salve.dados_apoio unid on unid.sq_dados_apoio = a.sq_unidade_area
                                          where a.sq_ficha = ficha.sq_ficha
                                                  )
                           ) -- fim aba 4

            -- inicio aba 5
                   ,'ameacas',json_build_object('tx_ameaca',salve.entity2char(ficha.ds_ameaca)
                           ,'vetores',(select json_agg( json_build_object('sq_criterio_ameaca_iucn', a.sq_criterio_ameaca_iucn
                            , 'ds_ameaca', vw_ameacas.descricao
                            , 'cd_ameaca', vw_ameacas.codigo
                            , 'de_ordem', vw_ameacas.ordem
                            , 'nu_nivel', vw_ameacas.nivel
                            , 'tx_ameaca_trilha',vw_ameacas.trilha
                            , 'tx_ficha_ameaca', a.tx_ficha_ameaca
                            , 'nu_peso', a.nu_peso
                            , 'sq_ref_termporal',ref_temporal.sq_dados_apoio
                            , 'ds_ref_termporal',ref_temporal.ds_dados_apoio
                            , 'cd_ref_termporal_sistema',ref_temporal.cd_sistema
                            , 'regioes', (
                                                                              select json_agg( json_build_object(
                                                                                      'sq_abrangencia',fo.sq_abrangencia
                                                                                  ,'tx_local,',fo.tx_local) )
                                                                              from salve.ficha_ameaca_regiao b
                                                                                       left outer join salve.ficha_ocorrencia fo on fo.sq_ficha_ocorrencia = b.sq_ficha_ocorrencia
                                                                                       inner join salve.dados_apoio as contexto on contexto.sq_dados_apoio = fo.sq_contexto
                                                                                       left outer join salve.abrangencia on abrangencia.sq_abrangencia = fo.sq_abrangencia
                                                                              where b.sq_ficha_ameaca = a.sq_ficha_ameaca
                                                                                and contexto.cd_sistema='AMEACA_REGIAO'
                                                                          )
                            , 'georeferencia', (
                                                                              select json_agg( json_build_object('tx_local',fo.tx_local
                                                                                  ,'ge_ocorrencia',fo.ge_ocorrencia
                                                                                  ,'x',st_x(fo.ge_ocorrencia)
                                                                                  ,'y',st_y(fo.ge_ocorrencia) ) )
                                                                              from salve.ficha_ameaca_regiao b
                                                                                       left outer join salve.ficha_ocorrencia fo on fo.sq_ficha_ocorrencia = b.sq_ficha_ocorrencia
                                                                                       inner join salve.dados_apoio as contexto on contexto.sq_dados_apoio = fo.sq_contexto
                                                                                       left outer join salve.abrangencia on abrangencia.sq_abrangencia = fo.sq_abrangencia
                                                                              where b.sq_ficha_ameaca = a.sq_ficha_ameaca
                                                                                and contexto.cd_sistema = 'AMEACA_GEO'
                                                                          )
                            , 'referencias', salve.fn_ref_bib_json(a.sq_ficha, a.sq_ficha_ameaca)
                            ) )

                                       from salve.ficha_ameaca a
                                                inner join salve.vw_ameacas on vw_ameacas.id = a.sq_criterio_ameaca_iucn
                                                left outer join salve.dados_apoio as ref_temporal on ref_temporal.sq_dados_apoio = a.sq_referencia_temporal
                                            --left outer join salve.dados_apoio ameaca on ameaca.sq_dados_apoio = a.sq_criterio_ameaca_iucn
                                       where a.sq_ficha = ficha.sq_ficha )
                           )
                   -- fim  aba 5



                   -- inicio aba 6
                   ,'usos',json_build_object('tx_uso',salve.entity2char(ficha.ds_uso)
                           ,'usos',(
                                                 select json_agg( json_build_object('sq_uso', a.sq_uso
                                                     , 'ds_uso', uso.descricao
                                                     , 'cd_uso',uso.codigo
                                                     , 'de_ordem',uso.ordem
                                                     , 'nu_nivel',uso.nivel
                                                     , 'tx_uso_trilha', uso.trilha
                                                     , 'sq_ameaca_iucn',uso.id_ameaca_iucn
                                                     , 'tx_ficha_uso', a.tx_ficha_uso
                                                     , 'tx_ameaca_trilha',ameaca.trilha
                                                     , 'regioes', (
                                                                                        select json_agg( json_build_object(
                                                                                                'sq_abrangencia',fo.sq_abrangencia
                                                                                            ,'tx_local,',fo.tx_local) )
                                                                                        from salve.ficha_uso_regiao b
                                                                                                 left outer join salve.ficha_ocorrencia fo on fo.sq_ficha_ocorrencia = b.sq_ficha_ocorrencia
                                                                                                 left outer join salve.abrangencia on abrangencia.sq_abrangencia = fo.sq_abrangencia
                                                                                        where b.sq_ficha_uso = a.sq_ficha_uso
                                                                                          and fo.sq_contexto = 497
                                                                                    )
                                                     , 'georeferencia', (
                                                                                        select json_agg( json_build_object('tx_local',fo.tx_local
                                                                                            ,'ge_ocorrencia',fo.ge_ocorrencia
                                                                                            ,'x',st_x(fo.ge_ocorrencia)
                                                                                            ,'y',st_y(fo.ge_ocorrencia) ) )
                                                                                        from salve.ficha_uso_regiao b
                                                                                                 left outer join salve.ficha_ocorrencia fo on fo.sq_ficha_ocorrencia = b.sq_ficha_ocorrencia
                                                                                                 left outer join salve.abrangencia on abrangencia.sq_abrangencia = fo.sq_abrangencia
                                                                                        where b.sq_ficha_uso = a.sq_ficha_uso
                                                                                          and fo.sq_contexto = 1006
                                                                                    )
                                                     , 'referencias', salve.fn_ref_bib_json(a.sq_ficha, a.sq_ficha_uso)
                                                     )
                                                            )
                                                 from salve.ficha_uso a
                                                          left outer join salve.vw_usos as uso on uso.id = a.sq_uso
                                                          left outer join salve.vw_ameacas as ameaca on ameaca.id = uso.id_ameaca_iucn
                                                 where a.sq_ficha = ficha.sq_ficha
                                             )
                           ,'anexos',( select json_agg( json_build_object('de_legenda',fa.de_legenda
                            ,'no_arquivo',fa.no_arquivo
                            ,'de_local_arquivo',fa.de_local_arquivo
                            ,'de_tipo_documento',fa.de_tipo_conteudo
                            ,'in_principal', fa.in_principal
                            ,'dt_anexo',fa.dt_anexo ) )
                                       from salve.ficha_anexo fa
                                       where fa.sq_ficha = ficha.sq_ficha
                                         and fa.sq_contexto = 874
                                             )
                           )
                   -- fim aba 6



                   -- inicio aba 7
                   ,'historico_avaliacao',json_build_object('st_presenca_lista_vigente',ficha.st_presenca_lista_vigente
                           ,'ds_historico_avaliacao',salve.entity2char(ficha.ds_historico_avaliacao)
                           ,'avaliacoes',( select json_agg( json_build_object('sq_tipo_avaliacao',h.sq_tipo_avaliacao
                            ,'ds_tipo_avaliacao',tipo.ds_dados_apoio
                            ,'cd_tipo_avaliacao_sistema',tipo.cd_sistema
                            ,'sq_abrangencia',h.sq_abrangencia
                            ,'sq_categoria_iucn',sq_categoria_iucn
                            ,'ds_categoria_iucn',categoria.ds_dados_apoio
                            ,'cd_categoria_iucn',categoria.cd_dados_apoio
                            ,'cd_categoria_iucn_sistema',categoria.cd_sistema
                            ,'nu_ano_avaliacao',h.nu_ano_avaliacao
                            ,'de_criterio_avaliacao_iucn',h.de_criterio_avaliacao_iucn
                            ,'st_possivelmente_extinta',h.st_possivelmente_extinta
                            ,'tx_justificativa_avaliacao',h.tx_justificativa_avaliacao
                            ,'no_regiao_outra',h.no_regiao_outra
                            ,'de_categoria_outra',h.de_categoria_outra))
                                           from salve.taxon_historico_avaliacao h
                                                    inner join salve.dados_apoio as categoria on categoria.sq_dados_apoio = h.sq_categoria_iucn
                                                    inner join salve.dados_apoio as tipo on tipo.sq_dados_apoio = h.sq_tipo_avaliacao
                                                    left outer join salve.abrangencia on categoria.sq_dados_apoio = h.sq_abrangencia
                                           where h.sq_taxon =ficha.sq_taxon
                                                            )),
                       'listas_convencoes',(
                           select json_agg( json_build_object('sq_lista_convencao', a.sq_lista_convencao, 'ds_lista_convencao',listas.ds_dados_apoio, 'nu_ano', a.nu_ano) )
                           from salve.ficha_lista_convencao a
                                    inner join salve.dados_apoio as listas on listas.sq_dados_apoio = a.sq_lista_convencao
                           where a.sq_ficha =ficha.sq_ficha
                       ),'acoes_conservacao',json_build_object('ds_acao_conservacao', salve.entity2char( ficha.ds_acao_conservacao)
                           , 'acoes_conservacao',(
                                                                   select json_agg(
                                                                                  json_build_object('sq_acao_conservacao', a.sq_acao_conservacao
                                                                                      , 'ds_acao_conservacao', acao.ds_dados_apoio
                                                                                      , 'sq_situacao_acao_conservacao', a.sq_situacao_acao_conservacao
                                                                                      , 'ds_situacao_acao_conservacao', situacao.ds_dados_apoio
                                                                                      , 'sq_plano_acao', a.sq_plano_acao
                                                                                      , 'tx_plano_acao', pan.tx_plano_acao
                                                                                      , 'sq_tipo_ordenamento', a.sq_tipo_ordenamento
                                                                                      , 'ds_tipo_ordenamento', ordenamento.ds_dados_apoio
                                                                                      ,'referencias',salve.fn_ref_bib_json(a.sq_ficha, a.sq_ficha_acao_conservacao ) )
                                                                              )
                                                                   from salve.ficha_acao_conservacao a
                                                                            inner join salve.dados_apoio as acao on acao.sq_dados_apoio = a.sq_acao_conservacao
                                                                            inner join salve.dados_apoio as situacao on situacao.sq_dados_apoio = a.sq_situacao_acao_conservacao
                                                                            left outer join salve.plano_acao as pan on pan.sq_plano_acao = a.sq_plano_acao
                                                                            left outer join salve.dados_apoio as ordenamento on ordenamento.sq_dados_apoio = a.sq_tipo_ordenamento
                                                                   where a.sq_ficha = ficha.sq_ficha
                                                               ))
                   ,'presenca_uc',json_build_object('ds_presenca_uc', salve.entity2char(ficha.ds_presenca_uc )
                           ,'ucs',( select json_agg(json_build_object('sq_uc',a.sq_uc
                            ,'no_uc',a.no_uc
                            ,'cd_esfera',a.cd_esfera
                            ,'in_presenca_atual',a.in_presenca_atual
                            ,'no_estado',a.no_estado
                            ,'sg_estado',a.sg_estado
                            ,'nu_ordem',a.nu_ordem
                            ,'co_nivel_taxonomico',a.co_nivel_taxonomico
                            ,'st_utilizado_avaliacao',a.st_utilizado_avaliacao
                            ,'st_adicionado_apos_avaliacao',a.st_adicionado_apos_avaliacao
                            ,'st_em_carencia',a.st_em_carencia
                            ,'co_cnuc',a.co_cnuc
                            ,'referencias',a.json_ref_bib::jsonb))
                                    from salve.fn_ficha_ucs(ficha.sq_ficha) a
                                                    ))
                   -- fim aba 7



                   -- inicio aba 8
                   ,'pesquisas',(
                           select json_agg( json_build_object('tx_ficha_pesquisa',a.tx_ficha_pesquisa
                               ,'sq_tema_pesquisa',a.sq_tema_pesquisa
                               ,'ds_tema_pesquisa',tema.ds_dados_apoio
                               ,'sq_situacao_pesquisa',a.sq_situacao_pesquisa
                               ,'ds_situacao_pesquisa',situacao.ds_dados_apoio
                               ,'referencias',salve.fn_ref_bib_json(a.sq_ficha, a.sq_ficha_pesquisa)
                               ) )
                           from salve.ficha_pesquisa a
                                    inner join salve.dados_apoio as tema on tema.sq_dados_apoio = a.sq_tema_pesquisa
                                    inner join salve.dados_apoio as situacao on situacao.sq_dados_apoio = a.sq_situacao_pesquisa
                           where a.sq_ficha = ficha.sq_ficha )
                   -- fim aba 8

                   -- inicio aba 9
                   ,'referencias',( select json_agg( json_build_object('sq_publicacao',r.sq_publicacao
                    ,'no_origem', r.no_origem
                    ,'de_ref_bibliografica',r.de_ref_bibliografica
                    ,'json_anexos',r.json_anexos::jsonb
                    ,'no_tags',r.no_tags
                    ,'no_tabela',r.no_tabela
                    ,'no_coluna',r.no_coluna
                    ))
                                    from salve.fn_ficha_ref_bibs(ficha.sq_ficha) as r
                       )
                   -- fim aba 9

                   -- inicio aba 10
                   ,'multimidia',( select json_agg( json_build_object(
                        'sq_tipo',a.sq_tipo,'ds_tipo',tipo.ds_dados_apoio,
                        'sq_datum',a.sq_datum,'ds_datum',datum.ds_dados_apoio,
                        'sq_situacao',a.sq_situacao,'ds_situacao', situacao.ds_dados_apoio,
                        'de_legenda',a.de_legenda,
                        'tx_multimidia',a.tx_multimidia,
                        'no_autor',a.no_autor,
                        'de_email_autor',a.de_email_autor,
                        'dt_elaboracao',a.dt_elaboracao,
                        'no_arquivo',a.no_arquivo,
                        'no_arquivo_disco',a.no_arquivo_disco,
                        'dt_inclusao',a.dt_inclusao,
                        'in_principal',a.in_principal,
                        'de_url',a.de_url,
                        'ge_multimidia',a.ge_multimidia,
                        'dt_aprovacao',a.dt_aprovacao,
                        'sq_pessoa_aprovou',a.sq_pessoa_aprovou,
                        'tx_nao_aceito',a.tx_nao_aceito,
                        'dt_reprovacao',a.dt_reprovacao,
                        'sq_pessoa_reprovou',a.sq_pessoa_reprovou,
                        'in_destaque',a.in_destaque) )
                                   from salve.ficha_multimidia  a
                                            left outer join salve.dados_apoio as tipo on tipo.sq_dados_apoio = a.sq_tipo
                                            left outer join salve.dados_apoio as datum on datum.sq_dados_apoio = a.sq_datum
                                            left outer join salve.dados_apoio as situacao on situacao.sq_dados_apoio = a.sq_situacao
                                   where sq_ficha = ficha.sq_ficha
                       )
                   -- fim aba 10



                   -- inicio aba 11
                   ,'avaliacao',json_build_object(
                           -- 11.1
                               'distribuicao_taxonomica', json_build_object (
                                'st_ocorrencia_marginal', ficha.st_ocorrencia_marginal
                            , 'st_elegivel_avaliacao', ficha.st_elegivel_avaliacao
                            , 'st_limitacao_taxonomica_aval', ficha.st_limitacao_taxonomica_aval
                            , 'st_localidade_tipo_conhecida', ficha.st_localidade_tipo_conhecida
                            , 'st_regiao_bem_amostrada', ficha.st_regiao_bem_amostrada
                            , 'vl_eoo', ficha.vl_eoo
                            , 'sq_tendencia_eoo', ficha.sq_tendencia_eoo
                            , 'ds_tendencia_eoo', tendencia_eoo.ds_dados_apoio
                            , 'st_flutuacao_eoo', ficha.st_flutuacao_eoo
                            , 'vl_aoo', ficha.vl_aoo
                            , 'sq_tendencia_aoo', ficha.sq_tendencia_aoo
                            , 'ds_tendencia_aoo', tendencia_aoo.ds_dados_apoio
                            , 'ds_justificativa_aoo_eoo', salve.entity2char(ficha.ds_justificativa_aoo_eoo)
                            )

                           -- aba 11.2
                           ,'populacao',json_build_object('nu_reducao_popul_passada_rev',ficha.nu_reducao_popul_passada_rev
                                   ,'sq_tipo_redu_popu_pass_rev',ficha.sq_tipo_redu_popu_pass_rev
                                   ,'ds_tipo_redu_popu_pass_rev',tipo_redu_popu_pass_rev.ds_dados_apoio
                                   ,'nu_reducao_popul_passada',ficha.nu_reducao_popul_passada
                                   ,'sq_tipo_redu_popu_pass',ficha.sq_tipo_redu_popu_pass
                                   ,'ds_tipo_redu_popu_pass',tipo_redu_popu_pass.ds_dados_apoio
                                   ,'nu_reducao_popul_futura',ficha.nu_reducao_popul_futura
                                   ,'sq_tipo_redu_popu_futura',ficha.sq_tipo_redu_popu_futura
                                   ,'ds_tipo_redu_popu_futura',tipo_redu_popu_futura.ds_dados_apoio
                                   ,'declinios_populacionais',( select json_agg( json_build_object('sq_declinio_populacional', a.sq_declinio_populacional
                                    ,'ds_declinio_populacional', declinio.ds_dados_apoio
                                    ))
                                                                from salve.ficha_declinio_populacional a
                                                                         inner join salve.dados_apoio as declinio on declinio.sq_dados_apoio = a.sq_declinio_populacional
                                                                where sq_ficha = ficha.sq_ficha
                                                          )
                                   ,'sq_perc_declinio_populacional',ficha.sq_perc_declinio_populacional
                                   ,'ds_perc_declinio_populacional',perc_declinio_populacional.ds_dados_apoio
                                   ,'st_populacao_fragmentada',ficha.st_populacao_fragmentada
                                   ,'nu_individuo_maduro',ficha.nu_individuo_maduro
                                   ,'sq_tenden_num_individuo_maduro',ficha.sq_tenden_num_individuo_maduro
                                   ,'ds_tenden_num_individuo_maduro',tenden_num_individuo_maduro.ds_dados_apoio
                                   ,'st_flut_extrema_indiv_maduro',ficha.st_flut_extrema_indiv_maduro
                                   ,'nu_subpopulacao',ficha.nu_subpopulacao
                                   ,'sq_tendencia_num_subpopulacao',ficha.sq_tendencia_num_subpopulacao
                                   ,'ds_tendencia_num_subpopulacao',tendencia_num_subpopulacao.ds_dados_apoio
                                   ,'st_flut_extrema_sub_populacao',ficha.st_flut_extrema_sub_populacao
                                   ,'nu_indivi_subpopulacao_max',ficha.nu_indivi_subpopulacao_max
                                   ,'nu_indivi_subpopulacao_perc',ficha.nu_indivi_subpopulacao_perc
                                   )

                           -- aba 11.3
                           ,'ameacas_analise_quantitativa',json_build_object(
                                       'nu_localizacoes', ficha.nu_localizacoes
                                   ,'sq_tenden_num_localizacoes', ficha.sq_tenden_num_localizacoes
                                   ,'ds_tenden_num_localizacoes', tenden_num_localizacoes.ds_dados_apoio
                                   ,'st_flutuacao_num_localizacoes', ficha.st_flutuacao_num_localizacoes
                                   ,'sq_tenden_qualidade_habitat',ficha.sq_tenden_qualidade_habitat
                                   ,'ds_tenden_qualidade_habitat',tenden_qualidade_habitat.ds_dados_apoio
                                   ,'st_ameaca_futura',ficha.st_ameaca_futura
                                   ,'sq_prob_extincao_brasil',ficha.sq_prob_extincao_brasil
                                   ,'ds_prob_extincao_brasil',prob_extincao_brasil.ds_dados_apoio
                                   ,'ds_metodo_calc_perc_extin_br', salve.entity2char(ficha.ds_metodo_calc_perc_extin_br )
                                   )


                           -- aba 11.4 - esta na aba 11-avaliacao
                           -- aba 11.5
                           ,'avaliacao_expressa',json_build_object (
                                       'st_favorecido_conversao_habitats',ficha.st_favorecido_conversao_habitats
                                   ,'st_tem_registro_areas_amplas',ficha.st_tem_registro_areas_amplas
                                   ,'st_possui_ampla_dist_geografica',ficha.st_possui_ampla_dist_geografica
                                   ,'st_frequente_inventario_eoo',ficha.st_frequente_inventario_eoo)

                           -- aba 11.6
                           ,'resultado_avaliacao',json_build_object('sq_categoria_iucn',ficha.sq_categoria_iucn
                                   ,'ds_categoria_iucn',categoria_iucn.ds_dados_apoio
                                   ,'cd_categoria_iucn', categoria_iucn.cd_dados_apoio
                                   ,'cd_categoria_iucn_sistema', categoria_iucn.cd_sistema
                                   ,'de_categoria_ordem', categoria_iucn.de_dados_apoio_ordem
                                   ,'ds_justificativa', salve.entity2char( ficha.ds_justificativa )
                                   ,'st_possivelmente_extinta',ficha.st_possivelmente_extinta
                                   ,'ds_criterio_aval_iucn',ficha.ds_criterio_aval_iucn
                                   ,'ajuste_regional',json_build_object('sq_tipo_conectividade',ficha.sq_tipo_conectividade
                                                                        ,'ds_tipo_conectividade',tipo_conectividade.ds_dados_apoio
                                                                        ,'sq_tendencia_imigracao',ficha.sq_tendencia_imigracao
                                                                        ,'ds_tendencia_imigracao',tendencia_imigracao.ds_dados_apoio
                                                                        ,'st_declinio_br_popul_exterior',ficha.st_declinio_br_popul_exterior
                                                                        ,'ds_conectividade_pop_exterior',salve.entity2char(ficha.ds_conectividade_pop_exterior )
                                                                        )
                                   ,'ds_citacao',salve.entity2char(ficha.ds_citacao)
                                   ,'ds_colaboradores',salve.entity2char(ficha.ds_colaboradores)
                                   ,'ds_equipe_tecnica',salve.entity2char(ficha.ds_equipe_tecnica )
                                   ,'dt_fim_avaliacao',oficina_avaliacao.dt_fim
                                   ,'nu_mes_ano_ultima_avaliacao', to_char(oficina_avaliacao.dt_fim,'MM/YYYY')
                                   )

                           -- aba 11.7
                           ,'resultado_validacao',json_build_object('sq_categoria_final',ficha.sq_categoria_final
                                   ,'ds_categoria_final',categoria_iucn_final.ds_dados_apoio
                                   ,'cd_categoria_final',categoria_iucn_final.cd_dados_apoio
                                   ,'cd_categoria_final_sitema',categoria_iucn_final.cd_sistema
                                   ,'de_categoria_final_ordem',categoria_iucn_final.de_dados_apoio_ordem
                                   ,'ds_justificativa_final',salve.entity2char(ficha.ds_justificativa_final)
                                   ,'st_possivelmente_extinta_final',ficha.st_possivelmente_extinta_final
                                   ,'ds_criterio_aval_iucn_final',ficha.ds_criterio_aval_iucn_final
                                   )
                           ,'motivos_mudanca_categoria',( select json_agg( json_build_object('sq_motivo_mudanca',a.sq_motivo_mudanca,'ds_motivo_mudanca', motivo_mudanca_categoria.ds_dados_apoio ) )
                                                          from salve.ficha_mudanca_categoria a
                                                                   inner join salve.dados_apoio as motivo_mudanca_categoria on motivo_mudanca_categoria.sq_dados_apoio = a.sq_motivo_mudanca
                                                          where a.sq_ficha = ficha.sq_ficha
                               )
                           )
                   -- fim aba 11

                   -- aba 12 - pendencias
                   , 'pendencias',
                       (  select json_agg(
                                         json_build_object(
                                                 'sq_ficha_pendencia',pendencia.sq_ficha_pendencia
                                             , 'sq_contexto'       , pendencia.sq_contexto
                                             , 'sq_usuario', pendencia.sq_usuario
                                             , 'ds_contexto', contexto.ds_dados_apoio
                                             , 'dt_pendencia',pendencia.dt_pendencia
                                             , 'de_assunto', pendencia.de_assunto
                                             , 'sq_usuario_resolveu', pendencia.sq_usuario_resolveu
                                             , 'dt_resolvida',pendencia.dt_resolvida
                                             , 'sq_usuario_revisou', pendencia.sq_usuario_revisou
                                             , 'dt_revisao',pendencia.dt_revisao
                                             , 'st_pendente', pendencia.st_pendente
                                             , 'tx_pendencia',salve.remover_html_tags(salve.entity2char(pendencia.tx_pendencia ) )
                                             )
                                     )

                          from salve.ficha_pendencia as pendencia
                                   left outer join salve.dados_apoio as contexto on contexto.sq_dados_apoio = pendencia.sq_contexto
                          where pendencia.sq_ficha = ficha.sq_ficha
                       ) -- fim aba 12

            -- Modulo Função / papéis
                   , 'funcoes',
                       (  select json_agg(
                                         json_build_object(
                                                 'sq_ficha_pessoa',ficha_pessoa.sq_ficha_pessoa
                                             ,'sq_papel',ficha_pessoa.sq_papel
                                             ,'ds_papel',papel.ds_dados_apoio
                                             ,'cd_papel_sistema',papel.cd_sistema
                                             ,'sq_pessoa',ficha_pessoa.sq_pessoa
                                             ,'no_pessoa',pf.no_pessoa
                                             ,'in_ativo',ficha_pessoa.in_ativo
                                             ,'sq_web_instituicao',ficha_pessoa.sq_web_instituicao
                                             ,'no_instituicao',web_instituicao.no_instituicao
                                             ,'sg_instituicao',web_instituicao.sg_instituicao
                                             ,'sq_grupo',ficha_pessoa.sq_grupo
                                             ,'ds_grupo',grupo.ds_dados_apoio
                                             ,'cd_grupo_sistema',grupo.cd_sistema
                                             )
                                     )
                          from salve.ficha_pessoa
                                   inner join corporativo.vw_pessoa_fisica pf on pf.sq_pessoa = ficha_pessoa.sq_pessoa
                                   left outer join salve.web_instituicao on web_instituicao.sq_web_instituicao = ficha_pessoa.sq_web_instituicao
                                   left outer join salve.dados_apoio as grupo on grupo.sq_dados_apoio = ficha_pessoa.sq_grupo
                                   left outer join salve.dados_apoio as papel on papel.sq_dados_apoio = ficha_pessoa.sq_papel
                          where ficha_pessoa.sq_ficha = ficha.sq_ficha
                            and ficha_pessoa.in_ativo = 'S'
                       ) -- fim funcoes/papeis

                   )::json
        from salve.ficha
                 left outer join corporativo.vw_unidade_org uo on uo.sq_pessoa = ficha.sq_unidade_org
                 left outer join salve.dados_apoio as grupo on grupo.sq_dados_apoio = ficha.sq_grupo
                 left outer join salve.dados_apoio as subgrupo on subgrupo.sq_dados_apoio = ficha.sq_subgrupo
                 left outer join salve.dados_apoio as grupo_recorte on grupo_recorte.sq_dados_apoio = ficha.sq_grupo_salve
                 left outer join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                 left outer join salve.dados_apoio as modo_reproducao on modo_reproducao.sq_dados_apoio = ficha.sq_modo_reproducao
                 left outer join salve.dados_apoio as sistema_acasalamento on sistema_acasalamento.sq_dados_apoio = ficha.sq_sistema_acasalamento
                 left outer join salve.dados_apoio as unid_intervalo_nascimento on unid_intervalo_nascimento.sq_dados_apoio = ficha.sq_unid_intervalo_nascimento
                 left outer join salve.dados_apoio as unid_tempo_gestacao on unid_tempo_gestacao.sq_dados_apoio = ficha.sq_unid_tempo_gestacao

                 left outer join salve.dados_apoio as unid_maturidade_sexual_macho on unid_maturidade_sexual_macho.sq_dados_apoio = ficha.sq_unid_maturidade_sexual_macho
                 left outer join salve.dados_apoio as unid_peso_macho on unid_peso_macho.sq_dados_apoio = ficha.sq_unidade_peso_macho
                 left outer join salve.dados_apoio as unid_medida_comprimento_macho_max on unid_medida_comprimento_macho_max.sq_dados_apoio = ficha.sq_medida_comprimento_macho_max
                 left outer join salve.dados_apoio as unid_medida_comprimento_macho on unid_medida_comprimento_macho.sq_dados_apoio = ficha.sq_medida_comprimento_macho
                 left outer join salve.dados_apoio as unid_senilid_rep_macho on unid_senilid_rep_macho.sq_dados_apoio = ficha.sq_unidade_senilid_rep_macho
                 left outer join salve.dados_apoio as unid_longevidade_macho on unid_longevidade_macho.sq_dados_apoio = ficha.sq_unidade_longevidade_macho

                 left outer join salve.dados_apoio as unid_maturidade_sexual_femea on unid_maturidade_sexual_femea.sq_dados_apoio = ficha.sq_unid_maturidade_sexual_femea
                 left outer join salve.dados_apoio as unid_peso_femea on unid_peso_femea.sq_dados_apoio = ficha.sq_unidade_peso_femea
                 left outer join salve.dados_apoio as unid_medida_comprimento_femea_max on unid_medida_comprimento_femea_max.sq_dados_apoio = ficha.sq_medida_comprimento_femea_max
                 left outer join salve.dados_apoio as unid_medida_comprimento_femea on unid_medida_comprimento_femea.sq_dados_apoio = ficha.sq_medida_comprimento_femea
                 left outer join salve.dados_apoio as unid_senilid_rep_femea on unid_senilid_rep_femea.sq_dados_apoio = ficha.sq_unidade_senilid_rep_femea
                 left outer join salve.dados_apoio as unid_longevidade_femea on unid_longevidade_femea.sq_dados_apoio = ficha.sq_unidade_longevidade_femea

                 left outer join salve.dados_apoio as tendencia_populacional on tendencia_populacional.sq_dados_apoio = ficha.sq_tendencia_populacional
                 left outer join salve.dados_apoio as unid_tempo_geracional on unid_tempo_geracional.sq_dados_apoio = ficha.sq_medida_tempo_geracional

                 left outer join salve.dados_apoio as tendencia_eoo on tendencia_eoo.sq_dados_apoio = ficha.sq_tendencia_eoo
                 left outer join salve.dados_apoio as tendencia_aoo on tendencia_aoo.sq_dados_apoio = ficha.sq_tendencia_aoo

                 left outer join salve.dados_apoio as tipo_redu_popu_pass_rev on tipo_redu_popu_pass_rev.sq_dados_apoio = ficha.sq_tipo_redu_popu_pass_rev
                 left outer join salve.dados_apoio as tipo_redu_popu_pass on tipo_redu_popu_pass.sq_dados_apoio = ficha.sq_tipo_redu_popu_pass
                 left outer join salve.dados_apoio as tipo_redu_popu_futura on tipo_redu_popu_futura.sq_dados_apoio = ficha.sq_tipo_redu_popu_futura
                 left outer join salve.dados_apoio as perc_declinio_populacional on perc_declinio_populacional.sq_dados_apoio = ficha.sq_perc_declinio_populacional
                 left outer join salve.dados_apoio as tenden_num_individuo_maduro on tenden_num_individuo_maduro.sq_dados_apoio = ficha.sq_tenden_num_individuo_maduro
                 left outer join salve.dados_apoio as tendencia_num_subpopulacao on tendencia_num_subpopulacao.sq_dados_apoio = ficha.sq_tendencia_num_subpopulacao

                 left outer join salve.dados_apoio as tenden_num_localizacoes on tenden_num_localizacoes.sq_dados_apoio = ficha.sq_tenden_num_localizacoes
                 left outer join salve.dados_apoio as tenden_qualidade_habitat on tenden_qualidade_habitat.sq_dados_apoio = ficha.sq_tenden_qualidade_habitat
                 left outer join salve.dados_apoio as prob_extincao_brasil on prob_extincao_brasil.sq_dados_apoio = ficha.sq_prob_extincao_brasil

                 left outer join salve.dados_apoio as categoria_iucn on categoria_iucn.sq_dados_apoio = ficha.sq_categoria_iucn
                 left outer join salve.dados_apoio as categoria_iucn_final on categoria_iucn_final.sq_dados_apoio = ficha.sq_categoria_final
                 left outer join salve.dados_apoio as tipo_conectividade on tipo_conectividade.sq_dados_apoio = ficha.sq_tipo_conectividade
                 left outer join salve.dados_apoio as tendencia_imigracao on tendencia_imigracao.sq_dados_apoio = ficha.sq_tendencia_imigracao
            -- ler a última oficina de avaliação
                 left join lateral (
            select oficina.dt_fim
            from salve.oficina_ficha
                     inner join salve.oficina on oficina.sq_oficina = oficina_ficha.sq_oficina
                     inner join salve.dados_apoio tipo on tipo.sq_dados_apoio = oficina.sq_tipo_oficina
            where oficina_ficha.sq_ficha = ficha.sq_ficha
              and tipo.cd_sistema = 'OFICINA_AVALIACAO'
            order by oficina.dt_fim desc limit 1
            ) as oficina_avaliacao on true
        where ficha.sq_ficha = p_sq_ficha )::json;
end;
$$;


--
-- TOC entry 1785 (class 1255 OID 28790)
-- Name: fn_ficha_bacias(bigint); Type: FUNCTION; Schema: salve; Owner: -
--

CREATE FUNCTION salve.fn_ficha_bacias(p_sq_ficha bigint) RETURNS TABLE(no_contexto text, no_origem text, no_bacia text, cd_bacia text, cd_sistema text, sq_bacia bigint, tx_trilha text, de_bacia_ordem text, st_utilizado_avalicao boolean, st_adicionado_apos_avaliacao boolean, sq_ficha_bacia bigint, co_nivel_taxonomico text, dt_carencia date, js_ref_bibs jsonb, st_em_carencia boolean)
    LANGUAGE plpgsql
    AS $$
begin
    return query
        with passo1 as (select ficha.sq_taxon
                             , ficha.sq_ficha
                             , ficha.sq_ciclo_avaliacao
                             , ficha.sq_grupo
                             , case
                                   when ficha.sq_grupo is not null and (grupo.cd_sistema like '%MARINHO%'
                                       OR  grupo.cd_sistema in
                                           ('CEFALOPODAS', 'EQUINODERMAS', 'BRAQUIOPODAS', 'CNIDARIAS', 'ENTEROPNEUSTAS',
                                            'POLIQUETAS', 'SIPUNCULAS')
                                       )
                                       THEN true
                                   else false end as st_marinho
                        from salve.ficha
                                 left outer join salve.dados_apoio as grupo on grupo.sq_dados_apoio = ficha.sq_grupo
                        where ficha.sq_ficha = p_sq_ficha
        ),
             passo2 as (select ficha.sq_ficha,
                               passo1.sq_ciclo_avaliacao,
                               taxon.sq_taxon,
                               nivel.co_nivel_taxonomico,
                               passo1.sq_grupo,
                               passo1.st_marinho
                        from passo1
                                 inner join taxonomia.taxon
                                            on (taxon.sq_taxon = passo1.sq_taxon or taxon.sq_taxon_pai = passo1.sq_taxon)
                                 inner join taxonomia.nivel_taxonomico as nivel
                                            on nivel.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
                                 inner join salve.ficha on ficha.sq_taxon = taxon.sq_taxon
                            and ficha.sq_ciclo_avaliacao = passo1.sq_ciclo_avaliacao

             ),
             bacias_ficha as (select 'ficha'::text                                              as no_contexto,
                                     'salve'::text                                              as no_origem,
                                     bacia.descricao::text                                      as no_bacia,
                                     bacia.codigo::text                                         as cd_bacia,
                                     bacia.codigo_sistema::text                                 as cd_sistema,
                                     bacia.id::bigint                                           as sq_bacia,
                                     bacia.trilha::text                                         as tx_trilha,
                                     bacia.ordem::text                                          as de_bacia_ordem,

                                     -- situacao avaliacao
                                     true::boolean                                               as st_utilizado_avalicao,
                                     false::boolean                                              as st_adicionado_apos_avaliacao,

                                     -- ids
                                     fb.sq_ficha_bacia::bigint                                  as sq_ficha_bacia,

                                     -- complemento
                                     passo2.co_nivel_taxonomico::text                         as co_nivel_taxonomico,

                                     null::date as dt_carencia,

                                     -- ref bib
                                     salve.fn_ref_bib_json(fb.sq_ficha,fb.sq_ficha_bacia,'sq_ficha_bacia')::jsonb as js_ref_bibs

                              from salve.ficha_bacia fb
                                       inner join passo2 on passo2.sq_ficha = fb.sq_ficha
                                       inner join salve.vw_bacia_hidrografica bacia on bacia.id = fb.sq_bacia

             ), bacias_ocorrencias_salve as (
            select 'ocorrencia'::text                                as no_contexto,
                   'salve'::text                                             as no_origem,
                   bacia.descricao::text                             as no_bacia,
                   bacia.codigo::text                                as cd_bacia,
                   bacia.codigo_sistema::text                        as cd_sistema,
                   bacia.id::bigint                                  as sq_bacia,
                   bacia.trilha::text                                as tx_trilha,
                   bacia.ordem::text                                  as de_bacia_ordem,
                   -- situacao avaliacao
                   (situacao.cd_sistema = 'REGISTRO_UTILIZADO_AVALIACAO')::boolean                  as st_utilizado_avalicao,
                   (situacao.cd_sistema = 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')::boolean as st_adicionado_apos_avaliacao,

                   -- ids
                   null::bigint                                  as sq_ficha_bacia,

                   -- complemento
                   passo2.co_nivel_taxonomico::text                         as co_nivel_taxonomico,

                   salve.calc_carencia(fo.dt_inclusao::date, carencia.cd_sistema)::date as dt_carencia,

                   -- ref bib
                   salve.fn_ref_bib_json(fo.sq_ficha,fo.sq_ficha_ocorrencia,'sq_ficha_ocorrencia')::jsonb as js_ref_bibs


            from salve.ficha_ocorrencia_registro a
                     inner join salve.registro_bacia b on b.sq_registro = a.sq_registro
                     inner join salve.dados_apoio_geo bacia_geo on bacia_geo.sq_dados_apoio_geo = b.sq_bacia_geo
                     inner join salve.vw_bacia_hidrografica as bacia on bacia.id = bacia_geo.sq_dados_apoio
                     inner join salve.ficha_ocorrencia fo on fo.sq_ficha_ocorrencia = a.sq_ficha_ocorrencia
                     inner join passo2 on passo2.sq_ficha = fo.sq_ficha
                     inner join salve.dados_apoio as situacao on situacao.sq_dados_apoio = fo.sq_situacao_avaliacao
                     left join salve.dados_apoio as carencia on carencia.sq_dados_apoio = fo.sq_prazo_carencia
                     left join salve.dados_apoio as ref_aprox on ref_aprox.sq_dados_apoio = fo.sq_ref_aproximacao
                     left join salve.dados_apoio as met_aprox on met_aprox.sq_dados_apoio = fo.sq_metodo_aproximacao
            where  (fo.in_presenca_atual is null or fo.in_presenca_atual <> 'N')
              and situacao.cd_sistema in ('REGISTRO_UTILIZADO_AVALIACAO', 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')
              -- não considerar registros que forem centroide do Estado
              and (fo.sq_metodo_aproximacao IS NULL OR met_aprox.cd_sistema <> 'CENTROIDE' OR ref_aprox.cd_sistema <> 'ESTADO')
              -- bioma marinho não possui pontos em Estados
              and passo2.st_marinho = false

        ), bacias_ocorrencias_portalbio as (
            select 'ocorrencia'::text                                as no_contexto,
                   'salve'::text                                             as no_origem,
                   bacia.descricao::text                             as no_bacia,
                   bacia.codigo::text                                as cd_bacia,
                   bacia.codigo_sistema::text                        as cd_sistema,
                   bacia.id::bigint                                  as sq_bacia,
                   bacia.trilha::text                                as tx_trilha,
                   bacia.ordem::text                                  as de_bacia_ordem,
                   -- situacao avaliacao
                   (situacao.cd_sistema = 'REGISTRO_UTILIZADO_AVALIACAO')::boolean                  as st_utilizado_avalicao,
                   (situacao.cd_sistema = 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')::boolean as st_adicionado_apos_avaliacao,

                   -- ids
                   null::bigint                                  as sq_ficha_bacia,

                   -- complemento
                   passo2.co_nivel_taxonomico::text                         as co_nivel_taxonomico,

                   fo.dt_carencia::date as dt_carencia,

                   -- ref bib
                   salve.fn_ref_bib_json(fo.sq_ficha,fo.sq_ficha_ocorrencia_portalbio,'sq_ficha_ocorrencia_portalbio')::jsonb as js_ref_bibs
            from salve.ficha_ocorrencia_portalbio_reg a
                     inner join salve.registro_bacia b on b.sq_registro = a.sq_registro
                     inner join salve.dados_apoio_geo bacia_geo on bacia_geo.sq_dados_apoio_geo = b.sq_bacia_geo
                     inner join salve.vw_bacia_hidrografica as bacia on bacia.id = bacia_geo.sq_dados_apoio
                     inner join salve.ficha_ocorrencia_portalbio fo on fo.sq_ficha_ocorrencia_portalbio = a.sq_ficha_ocorrencia_portalbio
                     inner join passo2 on passo2.sq_ficha = fo.sq_ficha
                     inner join salve.dados_apoio as situacao on situacao.sq_dados_apoio = fo.sq_situacao_avaliacao
                     left join salve.ficha_ref_bib as frb on frb.sq_ficha = fo.sq_ficha
                and frb.no_tabela = 'ficha_ocorrencia_portalbio'
                and frb.no_coluna = 'sq_ficha_ocorrencia_portalbio'
                and frb.sq_registro = fo.sq_ficha_ocorrencia_portalbio
                     left join taxonomia.publicacao on publicacao.sq_publicacao = frb.sq_publicacao
            where  (fo.in_presenca_atual is null or fo.in_presenca_atual <> 'N')
              and situacao.cd_sistema in ('REGISTRO_UTILIZADO_AVALIACAO', 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')
              -- bioma marinho não possui pontos em Estados
              and passo2.st_marinho = false

        )
        select a.*, false as st_em_carencia from bacias_ficha as a
        union
        select b.*, case when b.dt_carencia is null or b.dt_carencia < now() then false else true end as st_em_carencia from bacias_ocorrencias_salve as b
        union
        select c.*, case when c.dt_carencia is null or c.dt_carencia < now() then false else true end as st_em_carencia from bacias_ocorrencias_portalbio as c;
end;
$$;


--
-- TOC entry 1784 (class 1255 OID 28787)
-- Name: fn_ficha_biomas(bigint); Type: FUNCTION; Schema: salve; Owner: -
--

CREATE FUNCTION salve.fn_ficha_biomas(p_sq_ficha bigint) RETURNS TABLE(no_bioma text, no_contexto text, no_origem text, st_utilizado_avalicao boolean, st_adicionado_apos_avaliacao boolean, sq_ficha_bioma bigint, sq_bioma_salve bigint, sq_bioma_corp bigint, co_nivel_taxonomico text, dt_carencia date, js_ref_bibs jsonb, st_em_carencia boolean)
    LANGUAGE plpgsql
    AS $$
begin
    return query
        with passo1 as (select ficha.sq_taxon
                             , ficha.sq_ficha
                             , ficha.sq_ciclo_avaliacao
                             , ficha.sq_grupo
                             , case
                                   when ficha.sq_grupo is not null and (grupo.cd_sistema like '%MARINHO%'
                                       OR  grupo.cd_sistema in
                                           ('CEFALOPODAS', 'EQUINODERMAS', 'BRAQUIOPODAS', 'CNIDARIAS', 'ENTEROPNEUSTAS',
                                            'POLIQUETAS', 'SIPUNCULAS')
                                       )
                                       THEN true
                                   else false end as st_marinho
                        from salve.ficha
                                 left outer join salve.dados_apoio as grupo on grupo.sq_dados_apoio = ficha.sq_grupo
                        where ficha.sq_ficha = p_sq_ficha
        ),

             passo2 as (select ficha.sq_ficha,
                               passo1.sq_ciclo_avaliacao,
                               taxon.sq_taxon,
                               nivel.co_nivel_taxonomico,
                               passo1.sq_grupo,
                               passo1.st_marinho
                        from passo1
                                 inner join taxonomia.taxon
                                            on (taxon.sq_taxon = passo1.sq_taxon or taxon.sq_taxon_pai = passo1.sq_taxon)
                                 inner join taxonomia.nivel_taxonomico as nivel
                                            on nivel.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
                                 inner join salve.ficha on ficha.sq_taxon = taxon.sq_taxon
                            and ficha.sq_ciclo_avaliacao = passo1.sq_ciclo_avaliacao
             ),
             -- ler biomas adicionados pelo usuario na ficha
             biomas_ficha as (

                 -- ler biomas das ocorrencias SALVE
                 select bioma_salve.ds_dados_apoio::text                              as no_bioma
                      , 'ficha'::text                                            as no_contexto
                      , 'salve'::text                                            as no_origem

                      -- situacao avaliacao
                      , true::boolean                                               as st_utilizado_avalicao
                      , false::boolean                                              as st_adicionado_apos_avaliacao

                      -- ids
                      , fb.sq_ficha_bioma::bigint                                  as sq_ficha_bioma
                      , bioma_salve.sq_dados_apoio::bigint                         as sq_bioma_salve
                      , bioma_corp.sq_bioma::bigint                               as sq_bioma_corp

                      -- complemento
                      , passo2.co_nivel_taxonomico::text                         as co_nivel_taxonomico

                      ,null::date as dt_carencia

                      -- ref bib
                      ,salve.fn_ref_bib_json(fb.sq_ficha,fb.sq_ficha_bioma,'sq_ficha_bioma')::jsonb as js_ref_bibs

                 from salve.ficha_bioma fb
                          inner join passo2 on passo2.sq_ficha = fb.sq_ficha
                          inner join salve.dados_apoio as bioma_salve on bioma_salve.sq_dados_apoio = fb.sq_bioma
                          left outer join salve.bioma as bioma_corp on bioma_corp.no_bioma = bioma_salve.ds_dados_apoio
             ),

             biomas_ocorrencias_salve as (
                 select bioma_corp.no_bioma                                                     as no_bioma,
                        'ocorrencia'                                                            as no_contexto,
                        'salve'::text                                                           as no_origem,
                        -- situacao avaliacao
                        (situacao.cd_sistema = 'REGISTRO_UTILIZADO_AVALIACAO')                  as st_utilizado_avalicao,
                        (situacao.cd_sistema = 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO') as st_adicionado_apos_avaliacao,
                        -- ids
                        null::bigint                                                            as sq_ficha_bioma,
                        bioma_salve.sq_dados_apoio                                              as sq_bioma_salve,
                        bioma_corp.sq_bioma                                                     as sq_bioma_corp,
                        -- complemento
                        passo2.co_nivel_taxonomico                                              as co_nivel_taxonomico,

                        salve.calc_carencia(fo.dt_inclusao::date, carencia.cd_sistema)::date as dt_carencia,

                        -- ref bib
                        salve.fn_ref_bib_json(fo.sq_ficha,fo.sq_ficha_ocorrencia,'sq_ficha_ocorrencia')::jsonb as js_ref_bibs

                 from salve.ficha_ocorrencia_registro a
                          inner join salve.registro_bioma b on b.sq_registro = a.sq_registro
                          inner join salve.bioma as bioma_corp on bioma_corp.sq_bioma = b.sq_bioma
                          inner join salve.ficha_ocorrencia fo on fo.sq_ficha_ocorrencia = a.sq_ficha_ocorrencia
                          inner join passo2 on passo2.sq_ficha = fo.sq_ficha
                          inner join salve.dados_apoio as situacao on sq_dados_apoio = fo.sq_situacao_avaliacao
                          left join salve.dados_apoio as carencia on carencia.sq_dados_apoio = fo.sq_prazo_carencia
                          left join salve.dados_apoio as bioma_salve on bioma_salve.ds_dados_apoio = bioma_corp.no_bioma
                          left join salve.dados_apoio as ref_aprox on ref_aprox.sq_dados_apoio = fo.sq_ref_aproximacao
                          left join salve.dados_apoio as met_aprox on met_aprox.sq_dados_apoio = fo.sq_metodo_aproximacao
                 where  (fo.in_presenca_atual is null or fo.in_presenca_atual <> 'N')
                   -- não considerar registros que forem centroide do Estado
                   and (fo.sq_metodo_aproximacao IS NULL OR met_aprox.cd_sistema <> 'CENTROIDE' OR                   ref_aprox.cd_sistema <> 'ESTADO')
                   and situacao.cd_sistema in ('REGISTRO_UTILIZADO_AVALIACAO', 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')
                   -- se nao for do grupo marinho não exibir o bioma marinho
                   and ( not bioma_corp.no_bioma ilike '%MARINHO%' or passo2.st_marinho )
             )
                , biomas_ocorrencias_portalbio as (
            select bioma_corp.no_bioma                                                     as no_bioma,
                   'ocorrencia'                                                            as no_contexto,
                   'portalbio'::text                                                       as no_origem,
                   -- situacao avaliacao
                   (situacao.cd_sistema = 'REGISTRO_UTILIZADO_AVALIACAO')                  as st_utilizado_avalicao,
                   (situacao.cd_sistema = 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO') as st_adicionado_apos_avaliacao,
                   -- ids
                   null::bigint                                                            as sq_ficha_bioma,
                   bioma_salve.sq_dados_apoio                                              as sq_bioma_salve,
                   bioma_corp.sq_bioma                                                     as sq_bioma_corp,
                   -- complemento
                   passo2.co_nivel_taxonomico                                              as co_nivel_taxonomico,

                   fo.dt_carencia::date as dt_carencia,

                   -- ref bib
                   salve.fn_ref_bib_json(fo.sq_ficha,fo.sq_ficha_ocorrencia_portalbio,'sq_ficha_ocorrencia_portalbio')::jsonb as js_ref_bibs

            from salve.ficha_ocorrencia_portalbio_reg a
                     inner join salve.registro_bioma b on b.sq_registro = a.sq_registro
                     inner join salve.bioma as bioma_corp on bioma_corp.sq_bioma = b.sq_bioma
                     inner join salve.ficha_ocorrencia_portalbio fo on fo.sq_ficha_ocorrencia_portalbio = a.sq_ficha_ocorrencia_portalbio
                     inner join passo2 on passo2.sq_ficha = fo.sq_ficha
                     inner join salve.dados_apoio as situacao on sq_dados_apoio = fo.sq_situacao_avaliacao
                     left join salve.dados_apoio as bioma_salve on bioma_salve.ds_dados_apoio = bioma_corp.no_bioma
            where  (fo.in_presenca_atual is null or fo.in_presenca_atual <> 'N')
              and situacao.cd_sistema in ('REGISTRO_UTILIZADO_AVALIACAO', 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')
              -- se nao for do grupo marinho não exibir o bioma marinho
              and ( not bioma_corp.no_bioma ilike '%MARINHO%' or passo2.st_marinho )

        )
        select a.*, false as st_em_carencia from biomas_ficha as a
        union
        select b.*, case when b.dt_carencia is null or b.dt_carencia < now() then false else true end as st_em_carencia from biomas_ocorrencias_salve as b
        union
        select c.*, case when c.dt_carencia is null or c.dt_carencia < now() then false else true end as st_em_carencia from biomas_ocorrencias_portalbio as c;


end;
$$;


--
-- TOC entry 1783 (class 1255 OID 28784)
-- Name: fn_ficha_estados(bigint); Type: FUNCTION; Schema: salve; Owner: -
--

CREATE FUNCTION salve.fn_ficha_estados(p_sq_ficha bigint) RETURNS TABLE(no_contexto text, no_origem text, sq_estado bigint, no_estado text, sg_estado text, sq_regiao bigint, no_regiao text, nu_ordem_regiao integer, st_utilizado_avalicao boolean, st_adicionado_apos_avaliacao boolean, sq_ficha_uf bigint, co_nivel_taxonomico text, dt_carencia date, js_ref_bibs jsonb, st_em_carencia boolean)
    LANGUAGE plpgsql
    AS $$
begin
    return query
        with passo1 as (select ficha.sq_taxon
                             , ficha.sq_ficha
                             , ficha.sq_ciclo_avaliacao
                             , ficha.sq_grupo
                             , grupo.cd_sistema
                             , case
                                   when ficha.sq_grupo is not null and (grupo.cd_sistema like '%MARINHO%'
                                       OR  grupo.cd_sistema in
                                           ('CEFALOPODAS', 'EQUINODERMAS', 'BRAQUIOPODAS', 'CNIDARIAS', 'ENTEROPNEUSTAS',
                                            'POLIQUETAS', 'SIPUNCULAS')
                                       )
                                       THEN true
                                   else false end as st_marinho
                        from salve.ficha
                                 left outer join salve.dados_apoio as grupo on grupo.sq_dados_apoio = ficha.sq_grupo
                        where ficha.sq_ficha = p_sq_ficha
        ),
             passo2 as (select ficha.sq_ficha,
                               passo1.sq_ciclo_avaliacao,
                               taxon.sq_taxon,
                               nivel.co_nivel_taxonomico,
                               passo1.sq_grupo,
                               passo1.st_marinho
                        from passo1
                                 inner join taxonomia.taxon
                                            on (taxon.sq_taxon = passo1.sq_taxon or taxon.sq_taxon_pai = passo1.sq_taxon)
                                 inner join taxonomia.nivel_taxonomico as nivel
                                            on nivel.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
                                 inner join salve.ficha on ficha.sq_taxon = taxon.sq_taxon
                            and ficha.sq_ciclo_avaliacao = passo1.sq_ciclo_avaliacao
             ),
             estados_ficha as (select 'ficha'::text                                             as no_contexto,
                                      'salve'::text                                             as no_origem,
                                      uf.sq_estado::bigint                                      as sq_estado,
                                      uf.no_estado::text                                        as no_estado,
                                      uf.sg_estado::text                                        as no_estado,
                                      regiao.sq_regiao::bigint                                  as sq_regiao,
                                      regiao.no_regiao::text                                    as no_regiao,
                                      case when lower(regiao.no_regiao) = 'norte' then 1 else
                                          case when lower(regiao.no_regiao) = 'nordeste' then 2 else
                                              case when lower(regiao.no_regiao) = 'centro-oeste' then 3 else
                                                  case when lower(regiao.no_regiao) = 'sudeste' then 4 else
                                                      case when lower(regiao.no_regiao) = 'sul' then 5 else 6
                                                          end end end end end::integer                                      as nu_ordem_regiao
                                      -- situacao avaliacao
                                       , true::boolean                                               as st_utilizado_avalicao
                                       , false::boolean                                              as st_adicionado_apos_avaliacao

                                      -- ids
                                       , fu.sq_ficha_uf::bigint                                  as sq_ficha_uf

                                      -- complemento
                                       , passo2.co_nivel_taxonomico::text                         as co_nivel_taxonomico
                                       , null::date as dt_carencia

                                      -- ref bib
                                       , salve.fn_ref_bib_json(fu.sq_ficha,fu.sq_ficha_uf,'sq_ficha_uf')::jsonb as js_ref_bibs
                               from salve.ficha_uf fu
                                        inner join passo2 on passo2.sq_ficha = fu.sq_ficha
                                        inner join salve.estado as uf on uf.sq_estado = fu.sq_estado
                                        left outer join salve.regiao as regiao on regiao.sq_regiao = uf.sq_regiao
             ), estados_ocorrencias_salve as (
            select 'ocorrencia'::text                                as no_contexto,
                   'salve'::text                                             as no_origem,
                   uf.sq_estado::bigint                                      as sq_estado,
                   uf.no_estado::text                                        as no_estado,
                   uf.sg_estado::text                                        as sg_estado,
                   regiao.sq_regiao::bigint                                  as sq_regiao,
                   regiao.no_regiao::text                                    as no_regiao,
                   case when lower(regiao.no_regiao) = 'norte' then 1 else
                       case when lower(regiao.no_regiao) = 'nordeste' then 2 else
                           case when lower(regiao.no_regiao) = 'centro-oeste' then 3 else
                               case when lower(regiao.no_regiao) = 'sudeste' then 4 else
                                   case when lower(regiao.no_regiao) = 'sul' then 5 else 6
                                       end end end end end::integer                                      as nu_ordem_regiao,
                   -- situacao avaliacao
                   (situacao.cd_sistema = 'REGISTRO_UTILIZADO_AVALIACAO')::boolean                  as st_utilizado_avalicao,
                   (situacao.cd_sistema = 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')::boolean as st_adicionado_apos_avaliacao,

                   -- ids
                   null::bigint                                  as sq_ficha_uf,

                   -- complemento
                   passo2.co_nivel_taxonomico::text                         as co_nivel_taxonomico,
                   salve.calc_carencia(fo.dt_inclusao::date, carencia.cd_sistema)::date as dt_carencia,

                   -- ref bib
                   salve.fn_ref_bib_json(fo.sq_ficha,fo.sq_ficha_ocorrencia,'sq_ficha_ocorrencia')::jsonb as js_ref_bibs

            from salve.ficha_ocorrencia_registro a
                     inner join salve.registro_estado b on b.sq_registro = a.sq_registro
                     inner join salve.estado as uf on uf.sq_estado = b.sq_estado
                     inner join salve.regiao as regiao on regiao.sq_regiao = uf.sq_regiao
                     inner join salve.ficha_ocorrencia fo on fo.sq_ficha_ocorrencia = a.sq_ficha_ocorrencia
                     inner join passo2 on passo2.sq_ficha = fo.sq_ficha
                     inner join salve.dados_apoio as situacao on sq_dados_apoio = fo.sq_situacao_avaliacao
                     left join salve.dados_apoio as carencia on carencia.sq_dados_apoio = fo.sq_prazo_carencia
                     left join salve.dados_apoio as ref_aprox on ref_aprox.sq_dados_apoio = fo.sq_ref_aproximacao
                     left join salve.dados_apoio as met_aprox on met_aprox.sq_dados_apoio = fo.sq_metodo_aproximacao

            where  (fo.in_presenca_atual is null or fo.in_presenca_atual <> 'N')
              and situacao.cd_sistema in ('REGISTRO_UTILIZADO_AVALIACAO', 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')
              -- bioma marinho não possui pontos em Estados
              and passo2.st_marinho = false

        ), estados_ocorrencias_portalbio as (

            select 'ocorrencia'                                                            as no_contexto,
                   'salve'::text                                                           as no_origem,
                   uf.sq_estado::bigint                                      as sq_estado,
                   uf.no_estado::text                                        as no_estado,
                   uf.sg_estado::text                                        as no_estado,
                   regiao.sq_regiao                                          as sq_regiao,
                   regiao.no_regiao                                          as no_regiao,
                   case when lower(regiao.no_regiao) = 'norte' then 1 else
                       case when lower(regiao.no_regiao) = 'nordeste' then 2 else
                           case when lower(regiao.no_regiao) = 'centro-oeste' then 3 else
                               case when lower(regiao.no_regiao) = 'sudeste' then 4 else
                                   case when lower(regiao.no_regiao) = 'sul' then 5 else 6
                                       end end end end end::integer                                      as nu_ordem_regiao,
                   -- situacao avaliacao
                   (situacao.cd_sistema = 'REGISTRO_UTILIZADO_AVALIACAO')                  as st_utilizado_avalicao,
                   (situacao.cd_sistema = 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO') as st_adicionado_apos_avaliacao,

                   -- ids
                   null::bigint                                  as sq_ficha_uf,

                   -- complemento
                   passo2.co_nivel_taxonomico::text                         as co_nivel_taxonomico,
                   fo.dt_carencia::date as dt_carencia,

                   -- ref bib
                   salve.fn_ref_bib_json(fo.sq_ficha,fo.sq_ficha_ocorrencia_portalbio,'sq_ficha_ocorrencia_portalbio')::jsonb as js_ref_bibs
            from salve.ficha_ocorrencia_portalbio_reg a
                     inner join salve.registro_estado b on b.sq_registro = a.sq_registro
                     inner join salve.estado as uf on uf.sq_estado = b.sq_estado
                     inner join salve.regiao as regiao on regiao.sq_regiao = uf.sq_regiao
                     inner join salve.ficha_ocorrencia_portalbio fo on fo.sq_ficha_ocorrencia_portalbio = a.sq_ficha_ocorrencia_portalbio
                     inner join passo2 on passo2.sq_ficha = fo.sq_ficha
                     inner join salve.dados_apoio as situacao on sq_dados_apoio = fo.sq_situacao_avaliacao
            where  (fo.in_presenca_atual is null or fo.in_presenca_atual <> 'N')
              and situacao.cd_sistema in ('REGISTRO_UTILIZADO_AVALIACAO', 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')
              -- bioma marinho não possui pontos em Estados
              and passo2.st_marinho = false
        )
        select a.*, false as st_em_carencia from estados_ficha as a
        union
        select b.*, case when b.dt_carencia is null or b.dt_carencia < now() then false else true end as st_em_carencia from estados_ocorrencias_salve as b
        union
        select c.*, case when c.dt_carencia is null or c.dt_carencia < now() then false else true end as st_em_carencia from estados_ocorrencias_portalbio as c;
end;
$$;


--
-- TOC entry 1782 (class 1255 OID 28775)
-- Name: fn_ficha_grid_registros(bigint); Type: FUNCTION; Schema: salve; Owner: -
--

CREATE FUNCTION salve.fn_ficha_grid_registros(p_sq_ficha bigint) RETURNS TABLE(id bigint, id_row text, sq_ficha bigint, nm_cientifico text, cd_contexto text, no_bd text, no_fonte text, nu_x double precision, nu_y double precision, ds_precisao_coord text, no_datum text, nu_altitude integer, in_sensivel text, ds_sensivel text, sq_situacao_avaliacao bigint, ds_situacao_avaliacao text, cd_situacao_avaliacao text, ds_motivo_nao_utilizado text, tx_justificativa_nao_utilizado text, dt_carencia date, no_prazo_carencia text, no_localidade text, no_estado text, no_municipio text, in_presenca_atual text, ds_presenca_atual text, json_ref_bib text, id_origem text, co_nivel_taxonomico text, nu_grau_taxonomico integer, ds_data_hora text, dt_alteracao date, no_usuario_alteracao text, tx_observacao text, dt_ordenar text, no_autor text, no_projeto text, st_centroide boolean, sn_utilizado_avaliacao text, in_utilizado_avaliacao text, st_adicionado_apos_avaliacao boolean)
    LANGUAGE plpgsql
    AS $$
begin
    return query
        -- ler o taxon para poder processar as especies e subespecies
        with passo1 as (
            select ficha.sq_taxon, ficha.sq_ciclo_avaliacao
            from salve.ficha
            where ficha.sq_ficha = p_sq_ficha
        ),
             passo2 as (
                 select ficha.sq_ficha
                 from salve.ficha
                          inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                 where (ficha.sq_taxon = (select passo1.sq_taxon from passo1) or
                        taxon.sq_taxon_pai = (select passo1.sq_taxon from passo1))
                   and ficha.sq_ciclo_avaliacao = (select passo1.sq_ciclo_avaliacao from passo1)
             ),
             passoFinal as (
                 SELECT fo.sq_ficha_ocorrencia
                      , fo.sq_ficha
                      , fo.sq_situacao_avaliacao
                      , 'salve'                                        as no_bd
                      , case
                            when consulta.sq_ficha_ocorrencia_consulta is null then 'SALVE'
                            else 'SALVE / Consulta' END                  as no_fonte
                      , 'REGISTRO_OCORRENCIA'                          as cd_contexto
                      , ge_ocorrencia                                  as ge_ocorrencia
                      , precisao.ds_dados_apoio                        as ds_precisao_coord
                      , datum.ds_dados_apoio                           as no_datum
                      , fo.nu_altitude                                 as nu_altitude
                      , fo.in_sensivel                                 as in_sensivel
                      , motivo.ds_dados_apoio                          as ds_motivo_nao_utilizado
                      , case
                            when fo.in_utilizado_avaliacao = 'N'
                                then salve.remover_html_tags(fo.tx_nao_utilizado_avaliacao)
                            else ''::text end                          as tx_justificativa_nao_utilizado
                      , salve.calc_carencia(fo.dt_inclusao::date,
                                            prazo_carencia.cd_sistema) as dt_carencia
                      , prazo_carencia.ds_dados_apoio                  as no_prazo_carencia
                      , fo.no_localidade
                      , uf.sg_estado                                   as no_estado
                      , case
                            when coalesce(metodologia.cd_sistema, ''::text) = 'CENTROIDE'::text and
                                 coalesce(referencia.cd_sistema, ''::text) = 'ESTADO'::text then ''::text
                            ELSE municipio.no_municipio end            as no_municipio
                      , fo.in_presenca_atual::text
                      , salve.snd(fo.in_presenca_atual)                as ds_presenca_atual
                      , ref_bib.json_ref_bib::text
                      , case
                            when fo.id_origem is null then concat('SALVE:', fo.sq_ficha_ocorrencia::text)
                            ELSE fo.id_origem::text end                as id_origem
                      , case
                            when fo.nu_dia_registro is not null and fo.nu_mes_registro is not null and
                                 fo.nu_ano_registro is not null then concat(lpad(fo.nu_dia_registro::text, 2, '0'), '/',
                                                                            lpad(fo.nu_mes_registro::text, 2, '0'), '/',
                                                                            fo.nu_ano_registro::text)
                            else
                                case
                                    when fo.nu_mes_registro is not null and fo.nu_ano_registro is not null
                                        then concat(lpad(fo.nu_mes_registro::text, 2, '0'), '/',
                                                    fo.nu_ano_registro::text)
                                    else case
                                             when fo.nu_ano_registro is not null then fo.nu_ano_registro::text
                                             else null end
                                    end
                     end                                               as ds_dia_mes_ano_registro
                      , fo.hr_registro                                 as ds_hora_registro
                      , fo.dt_alteracao::date
                      , fo.sq_pessoa_alteracao
                      , fo.tx_observacao::text
                      ,concat(fo.nu_ano_registro,'-',lpad(fo.nu_mes_registro::text,2,'0'),'-',fo.nu_dia_registro) as dt_ordenar
                      ,'' as no_autor
                      ,'' as no_projeto
                      , case when coalesce(metodologia.cd_sistema, ''::text) = 'CENTROIDE'::text and
                                  coalesce(referencia.cd_sistema, ''::text) = 'ESTADO'::text then true else false end as st_centroide
                      , fo.in_utilizado_avaliacao::text
                      , fo.st_adicionado_apos_avaliacao::boolean

                 from salve.ficha_ocorrencia fo
                          left join salve.ficha_ocorrencia_registro a on a.sq_ficha_ocorrencia = fo.sq_ficha_ocorrencia
                          inner join passo2 on passo2.sq_ficha = fo.sq_ficha
                     -- precisao
                          left join salve.dados_apoio precisao on precisao.sq_dados_apoio = fo.sq_precisao_coordenada
                     -- datum
                          left join salve.dados_apoio datum on datum.sq_dados_apoio = fo.sq_datum
                     -- prazo carencia
                          LEFT JOIN salve.dados_apoio prazo_carencia
                                    ON prazo_carencia.sq_dados_apoio = fo.sq_prazo_carencia
                     -- Estado
                          left join salve.registro_estado reguf on reguf.sq_registro = a.sq_registro
                          left join salve.estado uf on uf.sq_estado = reguf.sq_estado
                     -- Municipio
                          left join salve.registro_municipio regmun on regmun.sq_registro = a.sq_registro
                          left join salve.municipio municipio on municipio.sq_municipio = regmun.sq_municipio
                     -- referencia
                          LEFT JOIN salve.dados_apoio referencia ON referencia.sq_dados_apoio = fo.sq_ref_aproximacao
                     -- metodologia
                          LEFT JOIN salve.dados_apoio metodologia
                                    ON metodologia.sq_dados_apoio = fo.sq_metodo_aproximacao
                     -- motivo não utilizado
                          left join salve.dados_apoio motivo
                                    on motivo.sq_dados_apoio = fo.sq_motivo_nao_utilizado_avaliacao
                     -- ocorrencia da consulta
                          left join salve.ficha_ocorrencia_consulta as consulta
                                    on consulta.sq_ficha_ocorrencia = a.sq_ficha_ocorrencia
                     -- referencia(s) bibliográfica(s)
                          left join lateral (
                     select json_object_agg(concat('rnd', trunc((random() * 100000))::text),
                                            json_build_object('de_titulo', coalesce(publicacao.de_titulo, '')
                                                , 'no_autor', coalesce(publicacao.no_autor, x.no_autor)
                                                , 'nu_ano', coalesce(publicacao.nu_ano_publicacao, x.nu_ano)
                                                )) as json_ref_bib
                     from salve.ficha_ref_bib x
                              left outer join taxonomia.publicacao on publicacao.sq_publicacao = x.sq_publicacao
                     where x.sq_ficha = fo.sq_ficha
                       and x.sq_registro = a.sq_ficha_ocorrencia
                       and x.no_tabela = 'ficha_ocorrencia'
                       and x.no_coluna = 'sq_ficha_ocorrencia' ) as ref_bib on true
                 where fo.ge_ocorrencia is not null

                 union
-- 1
                 select fo.sq_ficha_ocorrencia_portalbio as sq_ficha_ocorrencia
                      , fo.sq_ficha
                      , fo.sq_situacao_avaliacao
                      , 'portalbio'                                     as no_bd
                      --, coalesce(coalesce(fo.no_base_dados, concat('Portalbio<br>', no_instituicao)),'Portalbio')  as no_fonte
                      , salve.calc_base_dados_portalbio(concat(fo.tx_ocorrencia, ' ', fo.no_instituicao),fo.de_uuid) as no_fonte
                      , 'PORTALBIO'                                     as cd_contexto
                      , ge_ocorrencia                                   as ge_ocorrencia
                      , case
                            when left(fo.tx_ocorrencia, 1) = '{' then fo.tx_ocorrencia::jsonb ->> 'precisaoCoord'::text
                            else null end                               as ds_precisao_coord
                      , null                                            as no_datum
                      , null                                            as nu_altitude
                      , null                                            as in_sensivel
                      , motivo.ds_dados_apoio                           as ds_motivo_nao_utilizado
                      , salve.remover_html_tags(fo.tx_nao_utilizado_avaliacao)::text as tx_justificativa_nao_utilizado
                      , fo.dt_carencia
                      , null                                            as no_prazo_carencia
                      , fo.no_local
                      , case
                            when reguf.sq_estado is not null then uf.sg_estado
                            else
                                case
                                    when left(tx_ocorrencia, 1) = '{' then coalesce(
                                            coalesce(tx_ocorrencia::jsonb ->> 'estado',
                                                     tx_ocorrencia::jsonb ->> 'stateProvince')::text, uf.sg_estado)::text
                                    else uf.sg_estado end end           as no_estado
                      , case
                            when municipio.no_municipio is not null then municipio.no_municipio
                            else
                                case
                                    when left(fo.tx_ocorrencia, 1) = '{' then coalesce(
                                            coalesce(fo.tx_ocorrencia::jsonb ->> 'municipio',
                                                     fo.tx_ocorrencia::jsonb ->> 'municipality')::text,
                                            municipio.no_municipio)::text
                                    else municipio.no_municipio end end as no_municipio


                      , fo.in_presenca_atual::text
                      , salve.snd(fo.in_presenca_atual)                  as ds_presenca_atual
                      , ref_bib.json_ref_bib::text
                      , fo.de_uuid                                      as id_origem
                      , case
                            when to_char(fo.dt_ocorrencia, 'dd') is not null and
                                 to_char(fo.dt_ocorrencia, 'MM') is not null and
                                 to_char(fo.dt_ocorrencia, 'yyyy') is not null then concat(
                                    lpad(to_char(fo.dt_ocorrencia, 'dd')::text, 2, '0'), '/',
                                    lpad(to_char(fo.dt_ocorrencia, 'MM')::text, 2, '0'), '/',
                                    to_char(fo.dt_ocorrencia, 'yyyy')::text)
                            else
                                case
                                    when to_char(fo.dt_ocorrencia, 'MM') is not null and
                                         to_char(fo.dt_ocorrencia, 'yyyy') is not null
                                        then concat(lpad(to_char(fo.dt_ocorrencia, 'MM')::text, 2, '0'), '/',
                                                    to_char(fo.dt_ocorrencia, 'yyyy')::text)
                                    else case
                                             when to_char(fo.dt_ocorrencia, 'yyyy') is not null
                                                 then to_char(fo.dt_ocorrencia, 'yyyy')::text
                                             else null end
                                    end
                     end                                                as ds_dia_mes_ano_registro
                      , null                                            as ds_hora_registro
                      , fo.dt_alteracao::date
                      , null                                            as sq_pessoa_alteracao
                      , fo.tx_observacao::text
                      , to_char(fo.dt_ocorrencia, 'yyyy-MM-dd') as dt_ordenar
                      , fo.no_autor
                      , fo.tx_ocorrencia::jsonb->>'projeto'::text as no_projeto
                      , false as st_centroide
                      , fo.in_utilizado_avaliacao::text
                      , fo.st_adicionado_apos_avaliacao::boolean

                 from salve.ficha_ocorrencia_portalbio_reg a
                          join salve.ficha_ocorrencia_portalbio fo on fo.sq_ficha_ocorrencia_portalbio = a.sq_ficha_ocorrencia_portalbio
                          inner join passo2 on passo2.sq_ficha = fo.sq_ficha
                     -- Estado
                          left join salve.registro_estado reguf on reguf.sq_registro = a.sq_registro
                          left join salve.estado uf on uf.sq_estado = reguf.sq_estado
                     -- Municipio
                          left join salve.registro_municipio regmun on regmun.sq_registro = a.sq_registro
                          left join salve.municipio municipio on municipio.sq_municipio = regmun.sq_municipio
                     -- motivo não utilizado
                          left join salve.dados_apoio motivo
                                    on motivo.sq_dados_apoio = fo.sq_motivo_nao_utilizado_avaliacao

                     -- referencia(s) bibliográfica(s)
                          left join lateral (
                     select json_object_agg(concat('rnd', trunc((random() * 100000))::text),
                                            json_build_object('de_titulo', coalesce(publicacao.de_titulo, '')
                                                , 'no_autor', coalesce(publicacao.no_autor, x.no_autor)
                                                , 'nu_ano', coalesce(publicacao.nu_ano_publicacao, x.nu_ano)
                                                )) as json_ref_bib
                     from salve.ficha_ref_bib x
                              left outer join taxonomia.publicacao on publicacao.sq_publicacao = x.sq_publicacao
                     where x.sq_ficha = fo.sq_ficha
                       and x.sq_registro = fo.sq_ficha_ocorrencia_portalbio
                       and x.no_tabela = 'ficha_ocorrencia_portalbio'
                       and x.no_coluna = 'sq_ficha_ocorrencia_portalbio' ) as ref_bib on true

                 union


                 select fo.sq_ficha_ocorrencia_portalbio as sq_ficha_ocorrencia
                      , fo.sq_ficha
                      , fo.sq_situacao_avaliacao
                      , 'portalbio'                                     as no_bd
                      , salve.calc_base_dados_portalbio(concat(fo.tx_ocorrencia, ' ', fo.no_instituicao),fo.de_uuid) as no_fonte
                      --, coalesce(coalesce(fo.no_base_dados, concat('Portalbio<br>', no_instituicao)),                              'Portalbio') as no_fonte
                      , 'PORTALBIO'                                     as cd_contexto
                      , ge_ocorrencia                                   as ge_ocorrencia
                      , case
                            when left(fo.tx_ocorrencia, 1) = '{' then fo.tx_ocorrencia::jsonb ->> 'precisaoCoord'::text
                            else null end                               as ds_precisao_coord
                      , null                                            as no_datum
                      , null                                            as nu_altitude
                      , null                                            as in_sensivel
                      , motivo.ds_dados_apoio                           as ds_motivo_nao_utilizado
                      , salve.remover_html_tags(fo.tx_nao_utilizado_avaliacao) as tx_justificativa_nao_utilizado
                      , fo.dt_carencia
                      , null                                            as no_prazo_carencia
                      , fo.no_local
                      , case
                            when reguf.sq_estado is not null then uf.sg_estado
                            else
                                case
                                    when left(tx_ocorrencia, 1) = '{' then coalesce(
                                            coalesce(tx_ocorrencia::jsonb ->> 'estado',
                                                     tx_ocorrencia::jsonb ->> 'stateProvince')::text, uf.sg_estado)::text
                                    else uf.sg_estado end end           as no_estado
                      , case
                            when municipio.no_municipio is not null then municipio.no_municipio
                            else
                                case
                                    when left(fo.tx_ocorrencia, 1) = '{' then coalesce(
                                            coalesce(fo.tx_ocorrencia::jsonb ->> 'municipio',
                                                     fo.tx_ocorrencia::jsonb ->> 'municipality')::text,
                                            municipio.no_municipio)::text
                                    else municipio.no_municipio end end as no_municipio

                      , fo.in_presenca_atual::text
                      , salve.snd(fo.in_presenca_atual)                  as ds_presenca_atual
                      , ref_bib.json_ref_bib::text
                      , fo.de_uuid                                      as id_origem
                      , case
                            when to_char(fo.dt_ocorrencia, 'dd') is not null and
                                 to_char(fo.dt_ocorrencia, 'MM') is not null and
                                 to_char(fo.dt_ocorrencia, 'yyyy') is not null then concat(
                                    lpad(to_char(fo.dt_ocorrencia, 'dd')::text, 2, '0'), '/',
                                    lpad(to_char(fo.dt_ocorrencia, 'MM')::text, 2, '0'), '/',
                                    to_char(fo.dt_ocorrencia, 'yyyy')::text)
                            else
                                case
                                    when to_char(fo.dt_ocorrencia, 'MM') is not null and
                                         to_char(fo.dt_ocorrencia, 'yyyy') is not null
                                        then concat(lpad(to_char(fo.dt_ocorrencia, 'MM')::text, 2, '0'), '/',
                                                    to_char(fo.dt_ocorrencia, 'yyyy')::text)
                                    else case
                                             when to_char(fo.dt_ocorrencia, 'yyyy') is not null
                                                 then to_char(fo.dt_ocorrencia, 'yyyy')::text
                                             else null end
                                    end
                     end                                                as ds_dia_mes_ano_registro
                      , null                                            as ds_hora_registro
                      , fo.dt_alteracao::date
                      , null                                            as sq_pessoa_alteracao
                      , fo.tx_observacao::text
                      , to_char(fo.dt_ocorrencia, 'yyyy-MM-dd') as dt_ordenar
                      , fo.no_autor
                      , fo.tx_ocorrencia::jsonb->>'projeto'::text as no_projeto
                      , false as st_centroide
                      , fo.in_utilizado_avaliacao::text
                      , fo.st_adicionado_apos_avaliacao::boolean

                 from salve.ficha_ocorrencia_portalbio fo
                          left join salve.ficha_ocorrencia_portalbio_reg a
                                    on fo.sq_ficha_ocorrencia_portalbio = a.sq_ficha_ocorrencia_portalbio
                          inner join passo2 on passo2.sq_ficha = fo.sq_ficha
                     -- Estado
                          left join salve.registro_estado reguf on reguf.sq_registro = a.sq_registro
                          left join salve.estado uf on uf.sq_estado = reguf.sq_estado
                     -- Municipio
                          left join salve.registro_municipio regmun on regmun.sq_registro = a.sq_registro
                          left join salve.municipio municipio on municipio.sq_municipio = regmun.sq_municipio
                     -- motivo não utilizado
                          left join salve.dados_apoio motivo
                                    on motivo.sq_dados_apoio = fo.sq_motivo_nao_utilizado_avaliacao

                     -- referencia(s) bibliográfica(s)
                          left join lateral (
                     select json_object_agg(concat('rnd', trunc((random() * 100000))::text),
                                            json_build_object('de_titulo', coalesce(publicacao.de_titulo, '')
                                                , 'no_autor', coalesce(publicacao.no_autor, x.no_autor)
                                                , 'nu_ano', coalesce(publicacao.nu_ano_publicacao, x.nu_ano)
                                                )) as json_ref_bib
                     from salve.ficha_ref_bib x
                              left outer join taxonomia.publicacao on publicacao.sq_publicacao = x.sq_publicacao
                     where x.sq_ficha = fo.sq_ficha
                       and x.sq_registro = fo.sq_ficha_ocorrencia_portalbio
                       and x.no_tabela = 'ficha_ocorrencia_portalbio'
                       and x.no_coluna = 'sq_ficha_ocorrencia_portalbio' ) as ref_bib on true
                 where a.sq_ficha_ocorrencia_portalbio_reg is null
             )
        select passoFinal.sq_ficha_ocorrencia::bigint                                    as id
             , concat(passoFinal.no_bd, '-', passoFinal.sq_ficha_ocorrencia::text)::text as id_row
             , passoFinal.sq_ficha::bigint
             , ficha.nm_cientifico::text
             , passoFinal.cd_contexto::text
             , passoFinal.no_bd::text
             , passoFinal.no_fonte::text
             , st_x(passoFinal.ge_ocorrencia)::double precision                          as nu_x
             , st_y(passoFinal.ge_ocorrencia)::double precision                          as nu_y
             , passoFinal.ds_precisao_coord::text
             , passoFinal.no_datum::text
             , passoFinal.nu_altitude::integer
             , passoFinal.in_sensivel::text
             , salve.snd(passoFinal.in_sensivel)::text                                   as ds_sensivel
             , passoFinal.sq_situacao_avaliacao
             , situacao_avaliacao.ds_dados_apoio as ds_situacao_avaliacao
             , situacao_avaliacao.cd_sistema as cd_situacao_avaliacao
             , passoFinal.ds_motivo_nao_utilizado::text
             , passoFinal.tx_justificativa_nao_utilizado::text
             , passoFinal.dt_carencia::date
             , passoFinal.no_prazo_carencia ::text
             , passoFinal.no_localidade::text
             , passoFinal.no_estado::text
             , passoFinal.no_municipio::text
             , passoFinal.in_presenca_atual::text
             , passoFinal.ds_presenca_atual::text
             , passoFinal.json_ref_bib::text
             , passoFinal.id_origem::text
             , nivel_taxonomico.co_nivel_taxonomico::text
             , nivel_taxonomico.nu_grau_taxonomico::integer
             , replace(trim(concat(passoFinal.ds_dia_mes_ano_registro, ' ', passoFinal.ds_hora_registro)), '00:00',
                       '')::text                                                         as ds_data_hora
             , passoFinal.dt_alteracao::date
             , pf.no_pessoa::text                                                        as no_usuario_alteracao
             , passoFinal.tx_observacao::text
             , passoFinal.dt_ordenar
             , passoFinal.no_autor
             , passoFinal.no_projeto
             , passoFinal.st_centroide
             , salve.snd(passoFinal.in_utilizado_avaliacao)::text as sn_utilizado_avaliacao
             , passoFinal.in_utilizado_avaliacao::text
             , passoFinal.st_adicionado_apos_avaliacao::boolean

        from passoFinal
                 inner join salve.ficha on ficha.sq_ficha = passoFinal.sq_ficha
                 inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                 inner join taxonomia.nivel_taxonomico on nivel_taxonomico.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
                 inner join salve.dados_apoio as situacao_avaliacao on situacao_avaliacao.sq_dados_apoio = passoFinal.sq_situacao_avaliacao
                 left outer join salve.vw_pessoa_fisica pf on pf.sq_pessoa = passoFinal.sq_pessoa_alteracao;
end ;
$$;


--
-- TOC entry 1808 (class 1255 OID 29608)
-- Name: fn_ficha_limpar_avaliacao(bigint); Type: FUNCTION; Schema: salve; Owner: -
--

CREATE FUNCTION salve.fn_ficha_limpar_avaliacao(p_sq_ficha bigint) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
/*
  Função para limpar todas as informações referentes a Avaliação (aba 11) gravadas na ficha
*/
BEGIN
    -- limpar a aba/subabas 11-Avaliação
    update salve.ficha set
                         -- aba 11.1
        st_ocorrencia_marginal=null
                         ,st_elegivel_avaliacao=null
                         ,st_limitacao_taxonomica_aval=NULL
                         ,st_localidade_tipo_conhecida=null
                         ,st_regiao_bem_amostrada=NULL
                         ,vl_eoo=NULL
                         ,sq_tendencia_eoo=null
                         ,st_flutuacao_eoo=NULL
                         ,vl_aoo=NULL
                         ,sq_tendencia_aoo=null
                         ,st_flutuacao_aoo=null
                         ,ds_justificativa_eoo=null
                         ,ds_justificativa_aoo=null
                         -- aba 11.2
                         ,nu_reducao_popul_passada_rev=null
                         ,sq_tipo_redu_popu_pass_rev=NULL
                         ,nu_reducao_popul_passada=NULL
                         ,sq_tipo_redu_popu_pass=null
                         ,nu_reducao_popul_futura=NULL
                         , sq_tipo_redu_popu_futura=NULL
                         ,nu_reducao_popul_pass_futura=NULL
                         ,sq_tipo_redu_popu_pass_futura=NULL
                         ,sq_declinio_populacional=NULL
                         ,sq_perc_declinio_populacional=NULL
                         ,st_populacao_fragmentada=NULL
                         ,nu_individuo_maduro=NULL
                         ,sq_tenden_num_individuo_maduro=NULL
                         ,st_flut_extrema_indiv_maduro=NULL
                         ,nu_subpopulacao=NULL
                         ,sq_tendencia_num_subpopulacao=NULL
                         ,st_flut_extrema_sub_populacao=NULL
                         ,nu_indivi_subpopulacao_max=NULL
                         ,nu_indivi_subpopulacao_perc=null
                         -- aba 11.3
                         ,nu_localizacoes=NULL
                         ,sq_tenden_num_localizacoes=null
                         ,st_flutuacao_num_localizacoes=NULL
                         ,sq_tenden_qualidade_habitat=NULL
                         ,st_ameaca_futura=NULL
                         ,sq_prob_extincao_brasil=NULL
                         ,ds_metodo_calc_perc_extin_br=NULL
                         -- aba 11.4
                         ,sq_tipo_conectividade=NULL
                         ,sq_tendencia_imigracao=NULL
                         ,st_declinio_br_popul_exterior=null
                         ,ds_conectividade_pop_exterior=NULL
                         -- aba 11.5
                         ,st_favorecido_conversao_habitats=NULL
                         ,st_tem_registro_areas_amplas=NULL
                         ,st_possui_ampla_dist_geografica=NULL
                         ,st_frequente_inventario_eoo=NULL
                         -- abas 11.6 e 11.7
                         , ds_justificativa=null
                         , ds_justificativa_final=null
                         , sq_categoria_iucn=NULL
                         , sq_categoria_final=NULL
                         , st_possivelmente_extinta=NULL
                         , st_possivelmente_extinta_final=NULL
                         , ds_criterio_aval_iucn=NULL
                         , ds_criterio_aval_iucn_final=null
                         , ds_just_mudanca_categoria=null
                         , st_manter_lc=null
                         , ds_citacao = NULL
                         , ds_colaboradores = NULL
                         , ds_equipe_tecnica= null
    where sq_ficha = p_sq_ficha;
    RAISE NOTICE '  - Campos da avaliação, citacao, colaboradores e equipe técnica foram limpos';

    -- Limpar mudança de categoria
    delete from salve.ficha_mudanca_categoria where sq_ficha = p_sq_ficha;
    RAISE NOTICE '  - Motivos mudança de categoria foram excluídos';

    -- Limpar declinio populacional
    delete from salve.ficha_declinio_populacional where sq_ficha = p_sq_ficha;
    RAISE NOTICE '  - Declínio populacional foi excluído';

    RETURN TRUE;
EXCEPTION WHEN OTHERS THEN
    RAISE NOTICE '  - ERRO: %', SQLERRM;
    RETURN FALSE;
END;
$$;


--
-- TOC entry 7879 (class 0 OID 0)
-- Dependencies: 1808
-- Name: FUNCTION fn_ficha_limpar_avaliacao(p_sq_ficha bigint); Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON FUNCTION salve.fn_ficha_limpar_avaliacao(p_sq_ficha bigint) IS 'Função para limpar todos os campo das subabas 11.x referentes a avaliação';


--
-- TOC entry 1795 (class 1255 OID 29156)
-- Name: fn_ficha_ref_bibs(bigint); Type: FUNCTION; Schema: salve; Owner: -
--

CREATE FUNCTION salve.fn_ficha_ref_bibs(p_sq_ficha bigint) RETURNS TABLE(sq_publicacao bigint, no_origem text, de_ref_bibliografica text, json_anexos text, no_tags text, no_tabela character varying, no_coluna character varying, co_nivel_taxonomico text, cd_situacao_avaliacao text, dt_carencia date, st_em_crencia boolean)
    LANGUAGE plpgsql
    AS $$
begin
    return query
        with cte as (
            select ficha.sq_taxon,ficha.sq_ciclo_avaliacao
            from salve.ficha
            where ficha.sq_ficha = p_sq_ficha
        ),
             cte2 as (
                 select ficha.sq_ficha, nivel.co_nivel_taxonomico
                 from salve.ficha
                          inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                          inner join taxonomia.nivel_taxonomico as nivel on nivel.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
                 where (ficha.sq_taxon = (select cte.sq_taxon from cte) or
                        taxon.sq_taxon_pai = (select cte.sq_taxon from cte))
                   and ficha.sq_ciclo_avaliacao = (select cte.sq_ciclo_avaliacao from cte )
             ),
             refFichas as (
                 select a.sq_publicacao
                      , case when a.no_tabela in ('ficha_uf','ficha_bioma','ficha_bacia') and
                                  p.de_titulo like '% - SISBIO' then 'ocorrencia'::text else 'ficha'::text end as no_origem
                      , p.de_ref_bibliografica
                      , anexos.json_anexos
                      , tags.no_tags
                      , a.no_tabela
                      , a.no_coluna
                      , cte2.co_nivel_taxonomico
                      , null as cd_situacao_sistema
                      , null::date as dt_carencia
                 from salve.ficha_ref_bib a
                          inner join cte2 on cte2.sq_ficha = a.sq_ficha
                          inner join taxonomia.publicacao p on p.sq_publicacao = a.sq_publicacao

                          left join lateral (
                     select array_to_string(array_agg(dados_apoio.ds_dados_apoio), ',') AS no_tags
                     from salve.ficha_ref_bib_tag tag
                              inner join salve.dados_apoio on dados_apoio.sq_dados_apoio = tag.sq_tag
                     where tag.sq_ficha_ref_bib = a.sq_ficha_ref_bib
                     ) as tags on true
                          left join lateral (
                     select json_object_agg(anexo.sq_publicacao_arquivo::text, json_build_object('sq_publicacao_anexo',
                                                                                                 anexo.sq_publicacao_arquivo,
                                                                                                 'no_arquivo',
                                                                                                 anexo.no_publicacao_arquivo))::text as json_anexos
                     from taxonomia.publicacao_arquivo anexo
                     where anexo.sq_publicacao = a.sq_publicacao
                     ) as anexos on true
                 where a.sq_publicacao is not null
                   and p.de_ref_bibliografica is not null
                   and not a.no_tabela in ('ficha_ocorrencia', 'ficha_ocorrencia_portalbio','ficha_historico_avaliacao')
             ),
             refsOcoFicha as (
                 select a.sq_publicacao
                      , 'ocorrencia' as no_origem
                      , p.de_ref_bibliografica
                      , anexos.json_anexos
                      , tags.no_tags
                      , a.no_tabela
                      , a.no_coluna
                      , cte2.co_nivel_taxonomico
                      , situacao.cd_sistema as cd_situacao_sistema
                      , salve.calc_carencia(o.dt_inclusao::date, carencia.cd_sistema)::date as dt_carencia

                 from salve.ficha_ref_bib a
                          inner join cte2 on cte2.sq_ficha = a.sq_ficha
                          inner join taxonomia.publicacao p on p.sq_publicacao = a.sq_publicacao
                          inner join salve.ficha_ocorrencia o on o.sq_ficha_ocorrencia = a.sq_registro
                          inner join salve.dados_apoio situacao on situacao.sq_dados_apoio = o.sq_situacao_avaliacao
                          left join salve.dados_apoio as carencia on carencia.sq_dados_apoio = o.sq_prazo_carencia

                          left join lateral (
                     select array_to_string(array_agg(dados_apoio.ds_dados_apoio), ',') AS no_tags
                     from salve.ficha_ref_bib_tag tag
                              inner join salve.dados_apoio on dados_apoio.sq_dados_apoio = tag.sq_tag
                     where tag.sq_ficha_ref_bib = a.sq_ficha_ref_bib
                     ) as tags on true
                          left join lateral (
                     select json_object_agg(anexo.sq_publicacao_arquivo::text, json_build_object('sq_publicacao_anexo',
                                                                                                 anexo.sq_publicacao_arquivo,
                                                                                                 'no_arquivo',
                                                                                                 anexo.no_publicacao_arquivo))::text as json_anexos
                     from taxonomia.publicacao_arquivo anexo
                     where anexo.sq_publicacao = a.sq_publicacao
                     ) as anexos on true
                 where a.sq_publicacao is not null
                   and p.de_ref_bibliografica is not null
                   and a.no_tabela = 'ficha_ocorrencia'
                   and a.no_coluna = 'sq_ficha_ocorrencia'
                   and situacao.cd_sistema in ( 'REGISTRO_UTILIZADO_AVALIACAO','REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')
                   and not exists(select null from refFichas where refFichas.sq_publicacao = p.sq_publicacao limit 1)
             ),
             refsOcoPortalbio as
                 (
                     select a.sq_publicacao
                          , 'ocorrencia' as no_origem
                          , p.de_ref_bibliografica
                          , anexos.json_anexos
                          , tags.no_tags
                          , a.no_tabela
                          , a.no_coluna
                          , cte2.co_nivel_taxonomico
                          , situacao.cd_sistema as cd_situacao_sistema
                          , o.dt_carencia as dt_carencia
                     from salve.ficha_ref_bib a
                              inner join cte2 on cte2.sq_ficha = a.sq_ficha
                              inner join taxonomia.publicacao p on p.sq_publicacao = a.sq_publicacao
                              inner join salve.ficha_ocorrencia_portalbio o on o.sq_ficha_ocorrencia_portalbio = a.sq_registro
                              inner join salve.dados_apoio situacao on situacao.sq_dados_apoio = o.sq_situacao_avaliacao
                              left join lateral (
                         select array_to_string(array_agg(dados_apoio.ds_dados_apoio), ',') AS no_tags
                         from salve.ficha_ref_bib_tag tag
                                  inner join salve.dados_apoio on dados_apoio.sq_dados_apoio = tag.sq_tag
                         where tag.sq_ficha_ref_bib = a.sq_ficha_ref_bib
                         ) as tags on true
                              left join lateral (
                         select json_object_agg(anexo.sq_publicacao_arquivo::text,
                                                json_build_object('sq_publicacao_anexo',
                                                                  anexo.sq_publicacao_arquivo,
                                                                  'no_arquivo',
                                                                  anexo.no_publicacao_arquivo))::text as json_anexos
                         from taxonomia.publicacao_arquivo anexo
                         where anexo.sq_publicacao = a.sq_publicacao
                         ) as anexos on true
                     where p.de_ref_bibliografica is not null
                       and a.no_tabela = 'ficha_ocorrencia_portalbio'
                       and a.no_coluna = 'sq_ficha_ocorrencia_portalbio'
                       and situacao.cd_sistema in ( 'REGISTRO_UTILIZADO_AVALIACAO','REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')
                       and not exists(select null
                                      from refFichas
                                      where refFichas.sq_publicacao = p.sq_publicacao
                                      limit 1)
                 )
        select a.*, false as st_em_carencia from refFichas as a
        union
        select b.*,case when b.dt_carencia is null or b.dt_carencia < now() then false else true end as st_em_carencia from refsOcoFicha as b
        union
        select c.*,case when c.dt_carencia is null or c.dt_carencia < now() then false else true end as st_em_carencia from refsOcoPortalbio as c
        order by no_origem, de_ref_bibliografica;
end;
$$;


--
-- TOC entry 1794 (class 1255 OID 29107)
-- Name: fn_ficha_registros(bigint); Type: FUNCTION; Schema: salve; Owner: -
--

CREATE FUNCTION salve.fn_ficha_registros(p_sq_ficha bigint) RETURNS TABLE(bo_salve boolean, id_ocorrencia bigint, sq_ciclo_avaliacao bigint, sq_ficha bigint, sq_taxon bigint, nm_cientifico text, sq_categoria_iucn bigint, sq_categoria_final bigint, no_autor_taxon text, no_reino text, no_filo text, no_classe text, no_ordem text, no_familia text, no_genero text, no_especie text, no_subespecie text, ds_categoria_anterior text, ds_categoria_avaliada text, ds_categoria_validada text, ds_pan text, nu_longitude double precision, nu_latitude double precision, nu_altitude integer, de_endemica_brasil text, ds_sensivel text, sq_situacao_avaliacao bigint, ds_situacao_avaliacao text, cd_situacao_avaliacao text, tx_justificativa_nao_usado_avaliacao text, ds_motivo_nao_utilizado text, dt_carencia date, ds_prazo_carencia text, no_datum text, no_formato_original text, no_precisao text, no_referencia_aproximacao text, no_metodologia_aproximacao text, tx_metodologia_aproximacao text, no_taxon_citado text, in_presenca_atual text, ds_presenca_atual text, no_tipo_registro text, nu_dia_registro text, nu_mes_registro text, nu_ano_registro text, hr_registro text, nu_dia_registro_fim text, nu_mes_registro_fim text, nu_ano_registro_fim text, ds_data_desconhecida text, no_localidade text, ds_caracteristica_localidade text, no_uc_federal text, no_uc_estadual text, no_rppn text, no_pais text, no_estado text, no_municipio text, no_bioma text, no_ambiente text, no_habitat text, vl_elevacao_min numeric, vl_elevacao_max numeric, vl_profundidade_min numeric, vl_profundidade_max numeric, de_identificador text, dt_identificacao date, no_identificacao_incerta text, ds_tombamento text, ds_instituicao_tombamento text, no_compilador text, dt_compilacao date, no_revisor text, dt_revisao date, no_validador text, dt_validacao date, in_sensivel text, no_base_dados text, id_origem text, nu_autorizacao text, tx_observacao text, tx_ref_bib text, no_autor_registro text, no_projeto text, ds_utilizado_avaliacao text, ds_situacao_registro text, in_utilizado_avaliacao text)
    LANGUAGE plpgsql
    AS $$
begin
    return query

        -- ler o taxon para poder processar as especies e subespecies
        with passo1 as (
            select ficha.sq_taxon, ficha.sq_ciclo_avaliacao
            from salve.ficha
            where ficha.sq_ficha = p_sq_ficha
        ),
             passo2 as (
                 select ficha.sq_ficha
                 from salve.ficha
                          inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                 where (ficha.sq_taxon = (select passo1.sq_taxon from passo1) or
                        taxon.sq_taxon_pai = (select passo1.sq_taxon from passo1))
                   and ficha.sq_ciclo_avaliacao = (select passo1.sq_ciclo_avaliacao from passo1)
             ),

             passo3 as (

                 SELECT TRUE                                                                                              AS bo_salve,
                        fo.sq_ficha_ocorrencia                                                                            AS id_ocorrencia,
                        ficha.sq_ciclo_avaliacao,
                        ficha.sq_ficha,
                        ficha.sq_taxon,
                        ficha.nm_cientifico,
                        ficha.sq_categoria_iucn,
                        ficha.sq_categoria_final,
                        taxon.no_autor                                                                                    AS no_autor_taxon,
                        (taxon.json_trilha -> 'reino'::text) ->> 'no_taxon'::text                                         AS no_reino,
                        (taxon.json_trilha -> 'filo'::text) ->> 'no_taxon'::text                                          AS no_filo,
                        (taxon.json_trilha -> 'classe'::text) ->> 'no_taxon'::text                                        AS no_classe,
                        (taxon.json_trilha -> 'ordem'::text) ->> 'no_taxon'::text                                         AS no_ordem,
                        (taxon.json_trilha -> 'familia'::text) ->> 'no_taxon'::text                                       AS no_familia,
                        (taxon.json_trilha -> 'genero'::text) ->> 'no_taxon'::text                                        AS no_genero,
                        (taxon.json_trilha -> 'especie'::text) ->> 'no_taxon'::text                                       AS no_especie,
                        (taxon.json_trilha -> 'subespecie'::text) ->> 'no_taxon'::text                                    AS no_subespecie,
                        categoria_anterior.ds_categoria_anterior,
                        CASE
                            WHEN ficha.sq_categoria_iucn IS NOT NULL THEN CONCAT(cat_avaliada.ds_dados_apoio, ' (', cat_avaliada.cd_dados_apoio, ')')
                            ELSE ''::text
                            END                                                                              AS ds_categoria_avaliada,
                        CASE
                            WHEN ficha.sq_categoria_final IS NOT NULL THEN CONCAT(cat_validada.ds_dados_apoio, ' (', cat_validada.cd_dados_apoio, ')')
                            ELSE ''::text
                            END                                                                              AS ds_categoria_validada,
                        pans.ds_pan,
                        st_x(fo.ge_ocorrencia)                                                                            AS nu_longitude,
                        st_y(fo.ge_ocorrencia)                                                                            AS nu_latitude,
                        fo.nu_altitude,
                        salve.snd(ficha.st_endemica_brasil::text)                                                         AS de_endemica_brasil,
                        salve.snd(fo.in_sensivel::text)                                                                   AS ds_sensivel,
                        fo.sq_situacao_avaliacao,
                        situacao_avaliacao.ds_dados_apoio                                                                 AS ds_situacao_avaliacao,
                        situacao_avaliacao.cd_sistema                                                                     AS cd_situacao_avaliacao,
                        fo.tx_nao_utilizado_avaliacao                                                                     AS tx_justificativa_nao_usado_avaliacao,
                        motivo.ds_dados_apoio                                                                             AS ds_motivo_nao_utilizado,
                        salve.calc_carencia(fo.dt_inclusao::date, prazo_carencia.cd_sistema)                              AS dt_carencia,
                        prazo_carencia.ds_dados_apoio                                                                     AS ds_prazo_carencia,
                        datum.ds_dados_apoio                                                                              AS no_datum,
                        formato_original.ds_dados_apoio                                                                   AS no_formato_original,
                        precisao_coord.ds_dados_apoio                                                                     AS no_precisao,
                        referencia_aprox.ds_dados_apoio                                                                   AS no_referencia_aproximacao,
                        metodo_aprox.ds_dados_apoio                                                                       AS no_metodologia_aproximacao,
                        fo.tx_metodo_aproximacao                                                                          AS tx_metodologia_aproximacao,
                        (taxon_citado.json_trilha -> 'especie'::text) ->> 'no_taxon'::text                                AS no_taxon_citado,
                        fo.in_presenca_atual,
                        salve.snd(fo.in_presenca_atual::text)                                                             AS ds_presenca_atual,
                        tipo_registro.ds_dados_apoio                                                                      AS no_tipo_registro,
                        fo.nu_dia_registro::text                                                                          AS nu_dia_registro,
                        fo.nu_mes_registro::text                                                                          AS nu_mes_registro,
                        fo.nu_ano_registro::text                                                                          AS nu_ano_registro,
                        fo.hr_registro::text,
                        fo.nu_dia_registro_fim::text                                                                      AS nu_dia_registro_fim,
                        fo.nu_mes_registro_fim::text                                                                      AS nu_mes_registro_fim,
                        fo.nu_ano_registro_fim::text                                                                      AS nu_ano_registro_fim,
                        salve.snd(fo.in_data_desconhecida::text)                                                          AS ds_data_desconhecida,
                        fo.no_localidade,
                        salve.remover_html_tags(fo.tx_local)                                                              AS ds_caracteristica_localidade,
                        uc_federal.no_uc_federal,
                        uc_estadual.no_uc_estadual,
                        INITCAP(rppn_pessoa.no_pessoa::text)                                                                       AS no_rppn,
                        pais.no_pais,
                        estado.no_estado,
                        municipio.no_municipio,
                        bioma.no_bioma,
                        BTRIM(REGEXP_REPLACE(habitat.ds_dados_apoio, '[0-9.]'::text, ''::text, 'g'::text))                AS no_ambiente,
                        habitat.ds_dados_apoio                                                                            AS no_habitat,
                        fo.vl_elevacao_min,
                        fo.vl_elevacao_max,
                        fo.vl_profundidade_min,
                        fo.vl_profundidade_max,
                        fo.de_identificador,
                        fo.dt_identificacao,
                        qualificador.ds_dados_apoio                                                                       AS no_identificacao_incerta,
                        fo.tx_tombamento                                                                                  AS ds_tombamento,
                        fo.tx_instituicao                                                                                 AS ds_instituicao_tombamento,
                        compilador.no_pessoa                                                                              AS no_compilador,
                        fo.dt_compilacao,
                        revisor.no_pessoa                                                                                 AS no_revisor,
                        fo.dt_revisao,
                        validador.no_pessoa                                                                               AS no_validador,
                        fo.dt_validacao,
                        fo.in_sensivel,
                        'salve'::text                                                                                     AS no_base_dados,
                        CASE
                            WHEN fo.id_origem IS NULL THEN CONCAT('SALVE:', fo.sq_ficha_ocorrencia::text)
                            ELSE fo.id_origem::text
                            END                                                                              AS id_origem,
                        ''::text                                                                                          AS nu_autorizacao,
                        salve.remover_html_tags(fo.tx_observacao)                                                         AS tx_observacao,
                        refbibs.json_ref_bibs                                                                             AS tx_ref_bib,
                        NULL::text                                                                                        AS no_autor_registro,
                        NULL::text                                                                                        AS no_projeto,
                        CASE
                            WHEN fo.st_adicionado_apos_avaliacao = TRUE THEN 'Não'::text
                            ELSE salve.snd(fo.in_utilizado_avaliacao::text)
                            END                                                                              AS ds_utilizado_avaliacao,
                        salve.fn_calc_situacao_registro(fo.in_utilizado_avaliacao::text, fo.st_adicionado_apos_avaliacao) AS ds_situacao_registro,
                        fo.in_utilizado_avaliacao
                 FROM salve.ficha_ocorrencia fo
                          JOIN passo2 on passo2.sq_ficha = fo.sq_ficha
                          JOIN salve.ficha_ocorrencia_registro freg ON freg.sq_ficha_ocorrencia = fo.sq_ficha_ocorrencia
                          JOIN salve.ficha ON ficha.sq_ficha = fo.sq_ficha
                          JOIN salve.ciclo_avaliacao ciclo ON ciclo.sq_ciclo_avaliacao = ficha.sq_ciclo_avaliacao
                          JOIN taxonomia.taxon ON taxon.sq_taxon = ficha.sq_taxon
                          JOIN salve.dados_apoio situacao_avaliacao ON situacao_avaliacao.sq_dados_apoio = fo.sq_situacao_avaliacao
                          LEFT JOIN salve.dados_apoio motivo ON motivo.sq_dados_apoio = fo.sq_motivo_nao_utilizado_avaliacao
                          LEFT JOIN salve.dados_apoio cat_avaliada ON cat_avaliada.sq_dados_apoio = ficha.sq_categoria_iucn
                          LEFT JOIN salve.dados_apoio cat_validada ON cat_validada.sq_dados_apoio = ficha.sq_categoria_final
                          LEFT JOIN salve.dados_apoio tipo_registro ON tipo_registro.sq_dados_apoio = fo.sq_tipo_registro
                          LEFT JOIN salve.dados_apoio prazo_carencia ON prazo_carencia.sq_dados_apoio = fo.sq_prazo_carencia
                          LEFT JOIN salve.dados_apoio datum ON datum.sq_dados_apoio = fo.sq_datum
                          LEFT JOIN salve.dados_apoio formato_original ON formato_original.sq_dados_apoio = fo.sq_formato_coord_original
                          LEFT JOIN salve.dados_apoio precisao_coord ON precisao_coord.sq_dados_apoio = fo.sq_precisao_coordenada
                          LEFT JOIN salve.dados_apoio referencia_aprox ON referencia_aprox.sq_dados_apoio = fo.sq_ref_aproximacao
                          LEFT JOIN salve.dados_apoio metodo_aprox ON metodo_aprox.sq_dados_apoio = fo.sq_metodo_aproximacao
                          LEFT JOIN taxonomia.taxon taxon_citado ON taxon_citado.sq_taxon = fo.sq_taxon_citado
                          LEFT JOIN LATERAL ( SELECT h.sq_taxon,
                                                     CONCAT(cat_hist.ds_dados_apoio, ' (', cat_hist.cd_dados_apoio, ')') AS ds_categoria_anterior
                                              FROM salve.taxon_historico_avaliacao h
                                                       JOIN salve.dados_apoio cat_hist ON cat_hist.sq_dados_apoio = h.sq_categoria_iucn
                                              WHERE h.sq_taxon = ficha.sq_taxon
                                                AND h.sq_tipo_avaliacao = 311
                                                AND h.nu_ano_avaliacao <= (ciclo.nu_ano + 4)
                                              ORDER BY h.nu_ano_avaliacao DESC
                                              OFFSET 0 LIMIT 1) categoria_anterior ON TRUE
                          LEFT JOIN LATERAL ( SELECT acoes.sq_ficha,
                                                     ARRAY_TO_STRING(ARRAY_AGG(CONCAT(plano_acao.tx_plano_acao, ' (', apoio_acao_situacao.ds_dados_apoio, ')')), ', '::text) AS ds_pan
                                              FROM salve.ficha_acao_conservacao acoes
                                                       JOIN salve.plano_acao ON plano_acao.sq_plano_acao = acoes.sq_plano_acao
                                                       JOIN salve.dados_apoio apoio_acao_situacao ON apoio_acao_situacao.sq_dados_apoio = acoes.sq_situacao_acao_conservacao
                                              WHERE acoes.sq_ficha = ficha.sq_ficha
                                              GROUP BY acoes.sq_ficha) pans ON TRUE
                          LEFT JOIN salve.registro_rppn regrppn ON regrppn.sq_registro = freg.sq_registro
                          LEFT JOIN salve.unidade_conservacao as rppn ON rppn.sq_pessoa = regrppn.sq_rppn
                          LEFT JOIN salve.pessoa as rppn_pessoa ON rppn_pessoa.sq_pessoa = regrppn.sq_rppn
                          LEFT JOIN salve.registro_pais regpais ON regpais.sq_registro = freg.sq_registro
                          LEFT JOIN salve.pais pais ON pais.sq_pais = regpais.sq_pais
                          LEFT JOIN salve.registro_estado reguf ON reguf.sq_registro = freg.sq_registro
                          LEFT JOIN salve.estado estado ON estado.sq_estado = reguf.sq_estado
                          LEFT JOIN salve.registro_municipio regmun ON regmun.sq_registro = freg.sq_registro
                          LEFT JOIN salve.municipio municipio ON municipio.sq_municipio = regmun.sq_municipio
                          LEFT JOIN salve.dados_apoio habitat ON habitat.sq_dados_apoio = fo.sq_habitat
                          LEFT JOIN salve.dados_apoio ambiente ON ambiente.sq_dados_apoio = habitat.sq_dados_apoio_pai
                          LEFT JOIN salve.dados_apoio qualificador ON qualificador.sq_dados_apoio = fo.sq_qualificador_val_taxon
                          LEFT JOIN salve.pessoa compilador ON compilador.sq_pessoa = fo.sq_pessoa_compilador
                          LEFT JOIN salve.pessoa revisor ON revisor.sq_pessoa = fo.sq_pessoa_revisor
                          LEFT JOIN salve.pessoa validador ON validador.sq_pessoa = fo.sq_pessoa_validador
                          LEFT JOIN salve.registro_bioma regbio ON regbio.sq_registro = freg.sq_registro
                          LEFT JOIN salve.bioma bioma ON bioma.sq_bioma = regbio.sq_bioma
                          LEFT JOIN LATERAL ( SELECT JSON_OBJECT_AGG(ficha_ref_bib.sq_ficha_ref_bib, JSON_BUILD_OBJECT('de_ref_bib', salve.entity2char(publicacao.de_ref_bibliografica), 'no_autores_publicacao', REPLACE(publicacao.no_autor, '
'::text, '; '::text), 'nu_ano_publicacao', publicacao.nu_ano_publicacao, 'no_autor', ficha_ref_bib.no_autor, 'nu_ano', ficha_ref_bib.nu_ano))::text AS json_ref_bibs
                                              FROM salve.ficha_ref_bib
                                                       LEFT JOIN taxonomia.publicacao ON publicacao.sq_publicacao = ficha_ref_bib.sq_publicacao
                                              WHERE ficha_ref_bib.no_tabela::text = 'ficha_ocorrencia'::text
                                                AND ficha_ref_bib.no_coluna::text = 'sq_ficha_ocorrencia'::text
                                                AND ficha_ref_bib.sq_registro = fo.sq_ficha_ocorrencia) refbibs ON TRUE
                          LEFT JOIN LATERAL ( SELECT ARRAY_TO_STRING(ARRAY_AGG(COALESCE(ucf_instituicao.sg_instituicao, ucf_pessoa.no_pessoa::character varying)), ', '::text) AS no_uc_federal
                                              FROM salve.registro_uc_federal x
                                                       JOIN salve.unidade_conservacao ucf ON ucf.sq_pessoa = x.sq_uc_federal
                                                       join salve.instituicao as ucf_instituicao on ucf_instituicao.sq_pessoa = x.sq_uc_federal
                                                       join salve.pessoa as ucf_pessoa on ucf_pessoa.sq_pessoa = x.sq_uc_federal
                                              WHERE x.sq_registro = freg.sq_registro) uc_federal ON TRUE
                          LEFT JOIN LATERAL ( SELECT ARRAY_TO_STRING(ARRAY_AGG(uce_pessoa.no_pessoa), ', '::text) AS no_uc_estadual
                                              FROM salve.registro_uc_estadual x
                                                       JOIN salve.unidade_conservacao uce ON uce.sq_pessoa = x.sq_uc_estadual
                                                       JOIN salve.pessoa as uce_pessoa on uce_pessoa.sq_pessoa = x.sq_uc_estadual
                                              WHERE x.sq_registro = freg.sq_registro) uc_estadual ON TRUE
                 UNION ALL
                 SELECT FALSE                                                                                             AS bo_salve,
                        oc.sq_ficha_ocorrencia_portalbio                                                                  AS id_ocorrencia,
                        ficha.sq_ciclo_avaliacao,
                        ficha.sq_ficha,
                        ficha.sq_taxon,
                        ficha.nm_cientifico,
                        ficha.sq_categoria_iucn,
                        ficha.sq_categoria_final,
                        taxon.no_autor                                                                                    AS no_autor_taxon,
                        (taxon.json_trilha -> 'reino'::text) ->> 'no_taxon'::text                                         AS no_reino,
                        (taxon.json_trilha -> 'filo'::text) ->> 'no_taxon'::text                                          AS no_filo,
                        (taxon.json_trilha -> 'classe'::text) ->> 'no_taxon'::text                                        AS no_classe,
                        (taxon.json_trilha -> 'ordem'::text) ->> 'no_taxon'::text                                         AS no_ordem,
                        (taxon.json_trilha -> 'familia'::text) ->> 'no_taxon'::text                                       AS no_familia,
                        (taxon.json_trilha -> 'genero'::text) ->> 'no_taxon'::text                                        AS no_genero,
                        (taxon.json_trilha -> 'especie'::text) ->> 'no_taxon'::text                                       AS no_especie,
                        (taxon.json_trilha -> 'subespecie'::text) ->> 'no_taxon'::text                                    AS no_subespecie,
                        categoria_anterior.ds_categoria_anterior,
                        CASE
                            WHEN ficha.sq_categoria_iucn IS NOT NULL THEN CONCAT(cat_avaliada.ds_dados_apoio, ' (', cat_avaliada.cd_dados_apoio, ')')
                            ELSE ''::text
                            END                                                                                           AS ds_categoria_avaliada,
                        CASE
                            WHEN ficha.sq_categoria_final IS NOT NULL THEN CONCAT(cat_validada.ds_dados_apoio, ' (', cat_validada.cd_dados_apoio, ')')
                            ELSE ''::text
                            END                                                                                           AS ds_categoria_validada,
                        pans.ds_pan,
                        st_x(oc.ge_ocorrencia)                                                                            AS nu_longitude,
                        st_y(oc.ge_ocorrencia)                                                                            AS nu_latitude,
                        NULL::integer                                                                                     AS nu_altitude,
                        salve.snd(ficha.st_endemica_brasil::text)                                                         AS de_endemica_brasil,
                        NULL::text                                                                                        AS ds_sensivel,
                        oc.sq_situacao_avaliacao,
                        situacao_avaliacao.ds_dados_apoio                                                                 AS ds_situacao_avaliacao,
                        situacao_avaliacao.cd_sistema                                                                     AS cd_situacao_avaliacao,
                        oc.tx_nao_utilizado_avaliacao                                                                     AS tx_justificativa_nao_usado_avaliacao,
                        motivo.ds_dados_apoio                                                                             AS ds_motivo_nao_utilizado,
                        oc.dt_carencia,
                        NULL::text                                                                                        AS ds_prazo_carencia,
                        NULL::text                                                                                        AS no_datum,
                        NULL::text                                                                                        AS no_formato_original,
                        CASE
                            WHEN "left"(oc.tx_ocorrencia, 1) = '{'::text THEN oc.tx_ocorrencia::json ->> 'precisaoCoord'::text
                            ELSE ''::text
                            END                                                                                           AS no_precisao,
                        NULL::text                                                                                        AS no_referencia_aproximacao,
                        NULL::text                                                                                        AS no_metodologia_aproximacao,
                        NULL::text                                                                                        AS tx_metodologia_aproximacao,
                        NULL::text                                                                                        AS no_taxon_citado,
                        oc.in_presenca_atual,
                        salve.snd(oc.in_presenca_atual::text)                                                             AS ds_presenca_atual,
                        NULL::text                                                                                        AS no_tipo_registro,
                        TO_CHAR(oc.dt_ocorrencia::timestamp WITH TIME ZONE, 'DD'::text)                                   AS nu_dia_registro,
                        TO_CHAR(oc.dt_ocorrencia::timestamp WITH TIME ZONE, 'MM'::text)                                   AS nu_mes_registro,
                        TO_CHAR(oc.dt_ocorrencia::timestamp WITH TIME ZONE, 'yyyy'::text)                                 AS nu_ano_registro,
                        NULL::text                                                                                        AS hr_registro,
                        NULL::text                                                                                        AS nu_dia_registro_fim,
                        NULL::text                                                                                        AS nu_mes_registro_fim,
                        NULL::text                                                                                        AS nu_ano_registro_fim,
                        salve.snd(oc.in_data_desconhecida::text)                                                          AS ds_data_desconhecida,
                        oc.no_local                                                                                       AS no_localidade,
                        NULL::text                                                                                        AS ds_caracteristica_localidade,
                        uc_federal.no_uc_federal,
                        uc_estadual.no_uc_estadual,
                        INITCAP(rppn_pessoa.no_pessoa::text)                                                                       AS no_rppn,
                        pais.no_pais,
                        estado.no_estado,
                        municipio.no_municipio,
                        bioma.no_bioma,
                        NULL::text                                                                                        AS no_ambiente,
                        NULL::text                                                                                        AS no_habitat,
                        NULL::numeric                                                                                     AS vl_elevacao_min,
                        NULL::numeric                                                                                     AS vl_elevacao_max,
                        NULL::numeric                                                                                     AS vl_profundidade_min,
                        NULL::numeric                                                                                     AS vl_profundidade_max,
                        NULL::text                                                                                        AS de_identificador,
                        NULL::date                                                                                        AS dt_identificacao,
                        NULL::text                                                                                        AS no_identificacao_incerta,
                        NULL::text                                                                                        AS ds_tombamento,
                        NULL::text                                                                                        AS ds_instituicao_tombamento,
                        NULL::text                                                                                        AS no_compilador,
                        NULL::date                                                                                        AS dt_compilacao,
                        revisor.no_pessoa                                                                                 AS no_revisor,
                        oc.dt_revisao,
                        validador.no_pessoa                                                                               AS no_validador,
                        oc.dt_validacao,
                        NULL::bpchar                                                                                      AS in_sensivel,
                        salve.calc_base_dados_portalbio(oc.tx_ocorrencia, oc.de_uuid)                                     AS no_base_dados,
                        CASE
                            WHEN oc.id_origem IS NULL THEN
                                CASE
                                    WHEN "left"(oc.tx_ocorrencia, 1) = '{'::text THEN oc.tx_ocorrencia::json ->> 'uuid'::text
                                    ELSE oc.de_uuid
                                    END
                            ELSE oc.id_origem::text
                            END                                                                                           AS id_origem,
                        CASE
                            WHEN "left"(oc.tx_ocorrencia, 1) = '{'::text THEN oc.tx_ocorrencia::json ->> 'recordNumber'::text
                            ELSE ''::text
                            END                                                                                           AS nu_autorizacao,
                        NULL::text                                                                                        AS tx_observacao,
                        refbibs.json_ref_bibs                                                                             AS tx_ref_bib,
                        REPLACE(
                                CASE
                                    WHEN oc.no_autor IS NULL THEN
                                        CASE
                                            WHEN "left"(oc.tx_ocorrencia, 1) = '{'::text THEN
                                                CASE
                                                    WHEN (oc.tx_ocorrencia::json ->> 'autor'::text) IS NULL THEN oc.tx_ocorrencia::json ->> 'collector'::text
                                                    ELSE oc.tx_ocorrencia::json ->> 'autor'::text
                                                    END
                                            ELSE ''::text
                                            END
                                    ELSE oc.no_autor
                                    END, '
'::text, '; '::text)                                                             AS no_autor_registro,
                        oc.tx_ocorrencia::jsonb ->> 'projeto'::text                                                       AS no_projeto,
                        CASE
                            WHEN oc.st_adicionado_apos_avaliacao = TRUE THEN 'Não'::text
                            ELSE salve.snd(oc.in_utilizado_avaliacao::text)
                            END                                                                                           AS ds_utilizado_avaliacao,
                        salve.fn_calc_situacao_registro(oc.in_utilizado_avaliacao::text, oc.st_adicionado_apos_avaliacao) AS ds_situacao_registro,
                        oc.in_utilizado_avaliacao
                 FROM salve.ficha_ocorrencia_portalbio oc
                          JOIN passo2 on  passo2.sq_ficha = oc.sq_ficha
                          JOIN salve.ficha ON ficha.sq_ficha = oc.sq_ficha
                          JOIN salve.ciclo_avaliacao ciclo ON ciclo.sq_ciclo_avaliacao = ficha.sq_ciclo_avaliacao
                          JOIN taxonomia.taxon ON taxon.sq_taxon = ficha.sq_taxon
                          JOIN salve.dados_apoio situacao_avaliacao ON situacao_avaliacao.sq_dados_apoio = oc.sq_situacao_avaliacao
                          LEFT JOIN salve.dados_apoio motivo ON motivo.sq_dados_apoio = oc.sq_motivo_nao_utilizado_avaliacao
                          LEFT JOIN salve.dados_apoio cat_avaliada ON cat_avaliada.sq_dados_apoio = ficha.sq_categoria_iucn
                          LEFT JOIN salve.dados_apoio cat_validada ON cat_validada.sq_dados_apoio = ficha.sq_categoria_final
                          LEFT JOIN LATERAL ( SELECT h.sq_taxon,
                                                     CONCAT(cat_hist.ds_dados_apoio, ' (', cat_hist.cd_dados_apoio, ')') AS ds_categoria_anterior
                                              FROM salve.taxon_historico_avaliacao h
                                                       JOIN salve.dados_apoio cat_hist ON cat_hist.sq_dados_apoio = h.sq_categoria_iucn
                                              WHERE h.sq_taxon = ficha.sq_taxon
                                                AND h.sq_tipo_avaliacao = 311
                                                AND h.nu_ano_avaliacao <= (ciclo.nu_ano + 4)
                                              ORDER BY h.nu_ano_avaliacao DESC
                                              OFFSET 0 LIMIT 1) categoria_anterior ON TRUE
                          LEFT JOIN LATERAL ( SELECT acoes.sq_ficha,
                                                     ARRAY_TO_STRING(ARRAY_AGG(CONCAT(plano_acao.tx_plano_acao, ' (', apoio_acao_situacao.ds_dados_apoio, ')')), ', '::text) AS ds_pan
                                              FROM salve.ficha_acao_conservacao acoes
                                                       JOIN salve.plano_acao ON plano_acao.sq_plano_acao = acoes.sq_plano_acao
                                                       JOIN salve.dados_apoio apoio_acao_situacao ON apoio_acao_situacao.sq_dados_apoio = acoes.sq_situacao_acao_conservacao
                                              WHERE acoes.sq_ficha = ficha.sq_ficha
                                              GROUP BY acoes.sq_ficha) pans ON TRUE
                          LEFT JOIN salve.ficha_ocorrencia_portalbio_reg freg ON freg.sq_ficha_ocorrencia_portalbio = oc.sq_ficha_ocorrencia_portalbio
                          LEFT JOIN salve.registro_rppn regrppn ON regrppn.sq_registro = freg.sq_registro
                          LEFT JOIN salve.unidade_conservacao rppn ON rppn.sq_pessoa = regrppn.sq_rppn
                          LEFT JOIN salve.pessoa as rppn_pessoa on rppn_pessoa.sq_pessoa = regrppn.sq_rppn
                          LEFT JOIN salve.registro_pais regpais ON regpais.sq_registro = freg.sq_registro
                          LEFT JOIN salve.pais pais ON pais.sq_pais = regpais.sq_pais
                          LEFT JOIN salve.registro_estado reguf ON reguf.sq_registro = freg.sq_registro
                          LEFT JOIN salve.estado estado ON estado.sq_estado = reguf.sq_estado
                          LEFT JOIN salve.registro_municipio regmun ON regmun.sq_registro = freg.sq_registro
                          LEFT JOIN salve.municipio municipio ON municipio.sq_municipio = regmun.sq_municipio
                          LEFT JOIN salve.pessoa revisor ON revisor.sq_pessoa = oc.sq_pessoa_revisor
                          LEFT JOIN salve.pessoa validador ON validador.sq_pessoa = oc.sq_pessoa_validador
                          LEFT JOIN salve.registro_bioma regbio ON regbio.sq_registro = freg.sq_registro
                          LEFT JOIN salve.bioma bioma ON bioma.sq_bioma = regbio.sq_bioma
                          LEFT JOIN LATERAL ( SELECT JSON_OBJECT_AGG(ficha_ref_bib.sq_ficha_ref_bib, JSON_BUILD_OBJECT('de_ref_bib', salve.entity2char(publicacao.de_ref_bibliografica), 'no_autores_publicacao', REPLACE(publicacao.no_autor, '
'::text, '; '::text), 'nu_ano_publicacao', publicacao.nu_ano_publicacao, 'no_autor', ficha_ref_bib.no_autor, 'nu_ano', ficha_ref_bib.nu_ano))::text AS json_ref_bibs
                                              FROM salve.ficha_ref_bib
                                                       LEFT JOIN taxonomia.publicacao ON publicacao.sq_publicacao = ficha_ref_bib.sq_publicacao
                                              WHERE ficha_ref_bib.no_tabela::text = 'ficha_ocorrencia_portalbio'::text
                                                AND ficha_ref_bib.no_coluna::text = 'sq_ficha_ocorrencia_portalbio'::text
                                                AND ficha_ref_bib.sq_registro = oc.sq_ficha_ocorrencia_portalbio) refbibs ON TRUE
                          LEFT JOIN LATERAL ( SELECT ARRAY_TO_STRING(ARRAY_AGG(COALESCE(ucf_instituicao.sg_instituicao, ucf_pessoa.no_pessoa::character varying)), ', '::text) AS no_uc_federal
                                              FROM salve.registro_uc_federal x
                                                       JOIN salve.pessoa ucf_pessoa ON ucf_pessoa.sq_pessoa = x.sq_uc_federal
                                                       JOIN salve.unidade_conservacao ucf ON ucf.sq_pessoa = x.sq_uc_federal
                                                       JOIN salve.instituicao as ucf_instituicao on ucf_instituicao.sq_pessoa = x.sq_uc_federal
                                              WHERE x.sq_registro = freg.sq_registro) uc_federal ON TRUE
                          LEFT JOIN LATERAL ( SELECT ARRAY_TO_STRING(ARRAY_AGG(uce_pessoa.no_pessoa), ', '::text) AS no_uc_estadual
                                              FROM salve.registro_uc_estadual x
                                                       JOIN salve.unidade_conservacao uce ON uce.sq_pessoa = x.sq_uc_estadual
                                                       JOIN salve.pessoa as uce_pessoa on uce_pessoa.sq_pessoa = x.sq_uc_estadual
                                              WHERE x.sq_registro = freg.sq_registro) uc_estadual ON TRUE
             )
        select passo3.bo_salve::boolean
             ,passo3.id_ocorrencia::bigint
             ,passo3.sq_ciclo_avaliacao::bigint
             ,passo3.sq_ficha::bigint
             ,passo3.sq_taxon::bigint
             ,passo3.nm_cientifico::text
             ,passo3.sq_categoria_iucn::bigint
             ,passo3.sq_categoria_final::bigint
             ,passo3.no_autor_taxon::text
             ,passo3.no_reino::text
             ,passo3.no_filo::text
             ,passo3.no_classe::text
             ,passo3.no_ordem::text
             ,passo3.no_familia::text
             ,passo3.no_genero::text
             ,passo3.no_especie::text
             ,passo3.no_subespecie::text
             ,passo3.ds_categoria_anterior::text
             ,passo3.ds_categoria_avaliada::text
             ,passo3.ds_categoria_validada::text
             ,passo3.ds_pan::text
             ,passo3.nu_longitude::double precision
             ,passo3.nu_latitude::double precision
             ,passo3.nu_altitude::integer
             ,passo3.de_endemica_brasil::text
             ,passo3.ds_sensivel::text
             ,passo3.sq_situacao_avaliacao::bigint
             ,passo3.ds_situacao_avaliacao::text
             ,passo3.cd_situacao_avaliacao::text
             ,passo3.tx_justificativa_nao_usado_avaliacao::text
             ,passo3.ds_motivo_nao_utilizado::text
             ,passo3.dt_carencia::date
             ,passo3.ds_prazo_carencia::text
             ,passo3.no_datum::text
             ,passo3.no_formato_original::text
             ,passo3.no_precisao::text
             ,passo3.no_referencia_aproximacao::text
             ,passo3.no_metodologia_aproximacao::text
             ,passo3.tx_metodologia_aproximacao::text
             ,passo3.no_taxon_citado::text
             ,passo3.in_presenca_atual::text
             ,passo3.ds_presenca_atual::text
             ,passo3.no_tipo_registro::text
             ,passo3.nu_dia_registro::text
             ,passo3.nu_mes_registro::text
             ,passo3.nu_ano_registro::text
             ,passo3.hr_registro::text
             ,passo3.nu_dia_registro_fim::text
             ,passo3.nu_mes_registro_fim::text
             ,passo3.nu_ano_registro_fim::text
             ,passo3.ds_data_desconhecida::text
             ,passo3.no_localidade::text
             ,passo3.ds_caracteristica_localidade::text
             ,passo3.no_uc_federal::text
             ,passo3.no_uc_estadual::text
             ,passo3.no_rppn::text
             ,passo3.no_pais::text
             ,passo3.no_estado::text
             ,passo3.no_municipio::text
             ,passo3.no_bioma::text
             ,passo3.no_ambiente::text
             ,passo3.no_habitat::text
             ,passo3.vl_elevacao_min::numeric
             ,passo3.vl_elevacao_max::numeric
             ,passo3.vl_profundidade_min::numeric
             ,passo3.vl_profundidade_max::numeric
             ,passo3.de_identificador::text
             ,passo3.dt_identificacao::date
             ,passo3.no_identificacao_incerta::text
             ,passo3.ds_tombamento::text
             ,passo3.ds_instituicao_tombamento::text
             ,passo3.no_compilador::text
             ,passo3.dt_compilacao::date
             ,passo3.no_revisor::text
             ,passo3.dt_revisao::date
             ,passo3.no_validador::text
             ,passo3.dt_validacao::date
             ,passo3.in_sensivel::text
             ,passo3.no_base_dados::text
             ,passo3.id_origem::text
             ,passo3.nu_autorizacao::text
             ,passo3.tx_observacao::text
             ,passo3.tx_ref_bib::text
             ,passo3.no_autor_registro::text
             ,passo3.no_projeto::text
             ,passo3.ds_utilizado_avaliacao::text
             ,passo3.ds_situacao_registro::text
             ,passo3.in_utilizado_avaliacao::text
        from passo3;
end;
$$;


--
-- TOC entry 1816 (class 1255 OID 29731)
-- Name: fn_ficha_select(jsonb); Type: FUNCTION; Schema: salve; Owner: -
--

CREATE FUNCTION salve.fn_ficha_select(p_json jsonb DEFAULT NULL::jsonb) RETURNS TABLE(sq_ficha bigint, sq_taxon bigint, nm_cientifico text, st_transferida boolean, st_manter_lc boolean)
    LANGUAGE plpgsql
    AS $$
    /*
      funcao com parametros dinamicos no formato json para filtragem das fichas. Exemplos:
      - selecionar as fichas do primeiro ciclo
        select * from salve.fn_ficha_select('{"sq_ciclo_avaliacao":1}');
      - selecionar as fichas do segundo ciclo das categorias 133, 134
        select * from salve.fn_ficha_select('{"sq_ciclo_avaliacao":2, 'sq_categoria_iucn','133,134'}');
    */
declare
    cmdSql text;
    sqlTemp text;
    strTemp text;
    arrCamposAbas text ARRAY;
    sqlWhere text ARRAY;
    sqlOr text ARRAY;
    sqlInnerJoin text ARRAY;
begin
    SET client_min_messages TO WARNING;
    cmdSql = 'select ficha.sq_ficha::bigint' ||
             ',ficha.sq_taxon::bigint' ||
             ',ficha.nm_cientifico::text' ||
             ',ficha.st_transferida::boolean' ||
             ',ficha.st_manter_lc::boolean' ||
             ' from salve.ficha';

    -- FILTRO PELO CICLO
    if p_json->>'sq_ciclo_avaliacao' is not null then
        sqlWhere = array_append(sqlWhere,format('ficha.sq_ciclo_avaliacao = %s',p_json->>'sq_ciclo_avaliacao'));
    end if;

    -- FILTRO PELO SQ_FICHA
    if p_json->>'sq_ficha' is not null then
        sqlWhere = array_append(sqlWhere,format('ficha.sq_ficha = %s',p_json->>'sq_ficha'));
    end if;

    -- FILTRO PELA UNIDADE ORG
    if p_json->>'sq_unidade_org' is not null then
        sqlWhere = array_append(sqlWhere, format('ficha.sq_unidade_org = %s',p_json->>'sq_unidade_org') );
    end if;

    -- FILTRO PELA(s) OFICINA(s) e PELA CATEGORIA DA OFICINA DE AVALIAÇÃO
    if p_json->>'sq_oficina' is not null  then
        sqlInnerJoin = array_append( sqlInnerJoin,'inner join salve.oficina_ficha on oficina_ficha.sq_ficha = ficha.sq_ficha');
        sqlWhere = array_append(sqlWhere, format('oficina_ficha.sq_oficina in (%s)',p_json->>'sq_oficina' ) );
        if p_json->>'sq_categoria_oficina' is not null then
            sqlWhere = array_append(sqlWhere, format('oficina_ficha.sq_categoria_iucn in (%s)',p_json->>'sq_categoria_oficina' ) );
        END IF;
        if p_json->>'ds_agrupamento' is not null then
            sqlWhere = array_append(sqlWhere, format('oficina_ficha.ds_agrupamento ILIKE ''%%%s%%''',trim( replace( p_json->>'ds_agrupamento','%','') ) ) );
        END IF;

    end if;

    -- FILTRAR SOMENTE AS FICHAS ATRIBUIDAS PARA A PESSOA NO MODULO FUNÇÃO
    if p_json->>'sq_pessoa' is not null then
        sqlTemp = format('ficha.sq_ficha in ( select ficha_pessoa.sq_ficha from salve.ficha_pessoa'
                             || ' inner join salve.dados_apoio papel on papel.sq_dados_apoio = ficha_pessoa.sq_papel'
                             || ' where ficha_pessoa.sq_pessoa = %s and ficha_pessoa.in_ativo = ''S'')',p_json->>'sq_pessoa');
        sqlWhere = array_append(sqlWhere,sqlTemp);
    END IF;

    -- FILTRAR PELO(s) NOME(s) CIENTIFICO(s) OU PARTE DO NOME CIENTIFICO
    if p_json->>'nm_cientifico' is not null then
        -- o nome cientifico pode ser 1 ou vários separados por ;
        sqlOr = null;
        FOREACH strTemp IN ARRAY string_to_array(replace(p_json->>'nm_cientifico',',',';'), ';')
            LOOP
                sqlTemp = format('ficha.nm_cientifico ILIKE ''%%%s%%''',trim( replace( strTemp,'%','') ) );
                sqlOr = array_append(sqlOr, sqlTemp);
            END LOOP;
        sqlTemp = concat('(', array_to_string( salve.fn_array_unique(sqlOr ),chr(13)||' OR ' ) , ')');
        sqlWhere = array_append(sqlWhere, sqlTemp);
    END IF;

    -- FILTRAR PELO NOME COMUM
    if p_json->>'no_comum' is not null  then
        sqlTemp = format('exists( select null from salve.ficha_nome_comum where ficha_nome_comum.sq_ficha = ficha.sq_ficha ' ||
                         'and salve.fn_contem_palavra(''%s'',ficha_nome_comum.no_comum ) limit 1)',trim( replace( p_json->>'no_comum','%','' ) ) );
        sqlWhere = array_append(sqlWhere, sqlTemp);
    end if;

    -- FILTRAR PELO NIVEL TAXONOMICO
    if p_json->>'co_nivel_taxonomico' is not null and p_json->>'sq_taxon' is not null then
        sqlInnerJoin = array_append( sqlInnerJoin,'inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon');
        sqlTemp = format('(json_trilha->''%s''->>''sq_taxon'')::int in ( %s )',lower(p_json->>'co_nivel_taxonomico'), p_json->>'sq_taxon');
        sqlWhere = array_append(sqlWhere, sqlTemp);
    END IF;

    -- FILTRAR COM / SEM PENDENCIA
    if p_json->>'st_com_pendencia' is not null then
        sqlInnerJoin = array_append( sqlInnerJoin,E'left join lateral (
                                select ficha_pendencia.sq_ficha_pendencia from salve.ficha_pendencia
                                where ficha_pendencia.sq_ficha = ficha.sq_ficha
                                and st_pendente = ''S''
                                limit 1
                                ) as pendencias ON TRUE
                            ');
        if (p_json->>'st_com_pendencia')::boolean = true then
            sqlTemp = 'pendencias.sq_ficha_pendencia is null';
        ELSE
            sqlTemp = 'pendencias.sq_ficha_pendencia is not null';
        END IF;
        sqlWhere = array_append(sqlWhere, sqlTemp);
    END IF;

    -- FILTRAR PELA(s) SITUACAO(oes) DA FICHA
    if p_json->>'sq_situacao_ficha' is not null then
        sqlTemp = format('ficha.sq_situacao_ficha in (%s)',trim( replace( p_json->>'sq_situacao_ficha',' ','') ) );
        sqlWhere = array_append(sqlWhere, sqlTemp);
    END IF;

    -- FILTRAR PELO(s) GRUPO(s) AVALIADOS FICHA
    if p_json->>'sq_grupo_avaliado' is not null then
        sqlTemp = format('ficha.sq_grupo in (%s)',trim( replace( p_json->>'sq_grupo_avaliado',' ','') ) );
        sqlWhere = array_append(sqlWhere, sqlTemp);
        if p_json->>'sq_subgrupo_avaliado' is not null then
            sqlTemp = format('ficha.sq_subgrupo in (%s)',trim( replace( p_json->>'sq_subgrupo_avaliado',' ','') ) );
            sqlWhere = array_append(sqlWhere, sqlTemp);
        END IF;
    END IF;

    -- FILTRAR PELO(s) GRUPO(s) SALVE (RECORTE MODULO PUBLICO)
    if p_json->>'sq_grupo_salve' is not null then
        sqlTemp = format('ficha.sq_grupo_salve in (%s)',trim( replace( p_json->>'sq_grupo_salve',' ','') ) );
        sqlWhere = array_append(sqlWhere, sqlTemp);
    END IF;

    -- PRESENÇA NA LISTA NACIONAL OFICIAL VIGENTE
    if p_json->>'st_lista_oficial' is not null then
        sqlTemp = format('ficha.st_presenca_lista_vigente = ''%s''',upper(trim( replace( p_json->>'st_lista_oficial',' ','') ) ) );
        sqlWhere = array_append(sqlWhere, sqlTemp);
    END IF;

    -- FILTRAR POR PALAVRA CHAVE NOS CAMPOS ABERTOS DA ABA INFORMADA
    if p_json->>'nu_aba_localizar' is not null and p_json->>'ds_palavra_localizar' is not null then
        if p_json->>'nu_aba_localizar' = '1' then
            arrCamposAbas = ARRAY ['ds_notas_taxonomicas', 'ds_diagnostico_morfologico'];
        ELSEIF p_json->>'nu_aba_localizar' = '2' then
            arrCamposAbas = ARRAY ['ds_distribuicao_geo_global', 'ds_distribuicao_geo_nacional'];
        ELSEIF p_json->>'nu_aba_localizar' = '3' then
            arrCamposAbas = ARRAY ['ds_historia_natural', 'ds_habito_alimentar','ds_uso_habitat','ds_interacao','ds_reproducao'];
        ELSEIF p_json->>'nu_aba_localizar' = '4' then
            arrCamposAbas = ARRAY ['ds_populacao','ds_metod_calc_tempo_geracional','ds_caracteristica_genetica'];
        ELSEIF p_json->>'nu_aba_localizar' = '5' then
            arrCamposAbas = ARRAY ['ds_ameaca'];
        ELSEIF p_json->>'nu_aba_localizar' = '6' then
            arrCamposAbas = ARRAY ['ds_uso'];
        ELSEIF p_json->>'nu_aba_localizar' = '7' then
            arrCamposAbas = ARRAY ['ds_historico_avaliacao','ds_acao_conservacao','ds_presenca_uc'];
        ELSEIF p_json->>'nu_aba_localizar' = '8' then
            arrCamposAbas = ARRAY ['ds_pesquisa_exist_necessaria'];
        ELSEIF p_json->>'nu_aba_localizar' = '11' then
            arrCamposAbas = ARRAY ['ds_justificativa','ds_justificativa_final','ds_conectividade_pop_exterior','ds_justificativa_aoo_eoo'];
        END IF;
        if arrCamposAbas is not null then
            sqlOr = null;
            FOREACH strTemp IN ARRAY arrCamposAbas
                LOOP
                    sqlTemp = format('salve.fn_contem_palavra(''%s'',ficha.%s) = TRUE',trim( replace( p_json->>'ds_palavra_localizar','%','') ), strTemp );
                    sqlOr = array_append(sqlOr, sqlTemp);
                END LOOP;
            sqlTemp = concat('(', array_to_string( salve.fn_array_unique(sqlOr ),chr(13)||' OR ' ) , ')');
            sqlWhere = array_append(sqlWhere, sqlTemp);
        END IF;
    END IF;

    -- FILTRAR PELA CATEGORIA FINAL DA FICHA OU CATEGORIA DO HISTORICO DO TAXON
    if p_json->>'sq_categoria_final_ou_historico' is not null then
        sqlInnerJoin = array_append( sqlInnerJoin,E'left join lateral (
                                    SELECT th.sq_categoria_iucn
                                     FROM salve.taxon_historico_avaliacao th
                                    INNER JOIN salve.dados_apoio AS tipo_th ON tipo_th.sq_dados_apoio = th.sq_tipo_avaliacao
                                    INNER JOIN salve.dados_apoio AS categoria_th ON categoria_th.sq_dados_apoio = th.sq_categoria_iucn
                                    WHERE th.sq_taxon = ficha.sq_taxon
                                      AND tipo_th.cd_sistema = ''NACIONAL_BRASIL''
                                    ORDER BY th.nu_ano_avaliacao DESC LIMIT 1
                                    ) as taxon_historico on true');
        sqlTemp = format('( ficha.sq_categoria_final in (%s) or taxon_historico.sq_categoria_iucn in (%s) )', p_json->>'sq_categoria_final_ou_historico', p_json->>'sq_categoria_final_ou_historico');
        sqlWhere = array_append(sqlWhere, sqlTemp);
    ELSEIF p_json->>'sq_categoria_final' is not null then
        sqlTemp = format('ficha.sq_categoria_final in (%s)', p_json->>'sq_categoria_final' );
        sqlWhere = array_append(sqlWhere, sqlTemp);
    END IF;

    -- FILTRAR PELA CATEGORIA AVALIADA NO CICLO
    if p_json->>'sq_categoria_ficha' is not null then
        sqlTemp = format('ficha.sq_categoria_iucn in (%s)',p_json->>'sq_categoria_ficha') ;
        sqlWhere = array_append(sqlWhere, sqlTemp);
    END IF;

    -- FILTRAR PELO GRUPO AVALIADO
    if p_json->>'sq_grupo' is not null then
        sqlTemp = format('ficha.sq_grupo in (%s)',p_json->>'sq_grupo') ;
        sqlWhere = array_append(sqlWhere, sqlTemp);
        if p_json->>'sq_subgrupo' is not null then
            sqlTemp = format('ficha.sq_subgrupo in (%s)',p_json->>'sq_subgrupo') ;
            sqlWhere = array_append(sqlWhere, sqlTemp);
        END IF;
    END IF;

    -- FILTRO MANTER LC
    if p_json->>'st_manter_lc' is not null then
        if (p_json->>'st_manter_lc')::boolean = true THEN
            sqlWhere = array_append(sqlWhere,format('ficha.st_manter_lc = %s',p_json->>'st_manter_lc'));
        ELSE
            sqlWhere = array_append(sqlWhere,format('(ficha.st_manter_lc is null OR ficha.st_manter_lc = %s)',p_json->>'st_manter_lc'));
        END IF;
    end if;

    -- -------------------------------------------------------------------------------------------------
    -- -------------------------------------------------------------------------------------------------
    -- -------------------------------------------------------------------------------------------------
    -- -------------------------------------------------------------------------------------------------
    -- FIM FILTROS
    if sqlInnerJoin is not null then
        cmdSql = concat(cmdSql, chr(13), array_to_string( salve.fn_array_unique(sqlInnerJoin ),chr(13) ) );
    END IF;

    if sqlWhere is not null then
        cmdSql = concat(cmdSql, chr(13),'where ', array_to_string( salve.fn_array_unique(sqlWhere ),chr(13)||' AND ') );
        RAISE NOTICE ' ';
        RAISE NOTICE ' ';
        RAISE NOTICE 'sqlWhere (%)',sqlWhere;
    END IF;

    RAISE NOTICE ' ';
    RAISE NOTICE 'SQL FINAL';
    RAISE NOTICE '%',cmdSql;

    return QUERY EXECUTE cmdSql;
end
$$;


--
-- TOC entry 1789 (class 1255 OID 29022)
-- Name: fn_ficha_ucs(bigint); Type: FUNCTION; Schema: salve; Owner: -
--

CREATE FUNCTION salve.fn_ficha_ucs(p_sq_ficha bigint) RETURNS TABLE(sq_ficha bigint, sq_uc bigint, no_uc text, sg_uc text, cd_esfera text, in_presenca_atual text, de_presenca_atual text, sg_estado text, no_estado text, nu_ordem integer, co_nivel_taxonomico text, st_adicionado_apos_avaliacao boolean, st_utilizado_avaliacao boolean, st_em_carencia boolean, json_ref_bib text, co_cnuc text)
    LANGUAGE plpgsql
    AS $$
begin
    return query
        with passo0 as (
            select ficha.sq_taxon,ficha.sq_ciclo_avaliacao
            from salve.ficha
            where ficha.sq_ficha = p_sq_ficha
        ), passo1 as (
            select ficha.sq_ficha, nivel_taxonomico.co_nivel_taxonomico
            from salve.ficha
                     inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                     inner join taxonomia.nivel_taxonomico on nivel_taxonomico.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
            where (ficha.sq_taxon = (select passo0.sq_taxon from passo0) or
                   taxon.sq_taxon_pai = (select passo0.sq_taxon from passo0))
              and ficha.sq_ciclo_avaliacao = (select passo0.sq_ciclo_avaliacao from passo0 )
        ), cte1 AS (
            WITH cte AS (
                SELECT c.sq_ficha,
                       a.sq_ficha_ocorrencia,
                       uc.sq_pessoa                         AS sq_uc,
                       instituicao.sg_instituicao           AS no_uc,
                       instituicao.sg_instituicao           AS sg_uc,
                       c.in_presenca_atual,
                       salve.snd(c.in_presenca_atual::text) AS de_presenca_atual,
                       uf.no_estado,
                       uf.sg_estado,
                       'F'::text                            AS cd_esfera,
                       1                                    AS nu_ordem,
                       passo1.co_nivel_taxonomico,
                       'ficha_ocorrencia' as no_tabela,
                       (situacao_avaliacao.cd_sistema = 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')::boolean as st_adicionado_apos_avaliacao,
                       (situacao_avaliacao.cd_sistema = 'REGISTRO_UTILIZADO_AVALIACAO')::boolean as st_utilizado_avaliacao,
                       salve.calc_carencia(c.dt_inclusao::date, carencia.cd_sistema) AS dt_carencia,
                       uc.cd_cnuc::text as co_cnuc
                FROM salve.ficha_ocorrencia_registro a
                         JOIN salve.registro_uc_federal b ON b.sq_registro = a.sq_registro
                         JOIN salve.unidade_conservacao uc ON uc.sq_pessoa = b.sq_uc_federal
                         JOIN salve.instituicao ON instituicao.sq_pessoa = b.sq_uc_federal
                         JOIN salve.ficha_ocorrencia c ON c.sq_ficha_ocorrencia = a.sq_ficha_ocorrencia
                         JOIN passo1 on passo1.sq_ficha = c.sq_ficha
                         JOIN salve.dados_apoio as situacao_avaliacao on situacao_avaliacao.sq_dados_apoio = c.sq_situacao_avaliacao
                         LEFT JOIN salve.dados_apoio as carencia on carencia.sq_dados_apoio = c.sq_prazo_carencia
                         LEFT JOIN salve.registro_estado d ON d.sq_registro = a.sq_registro
                         LEFT JOIN salve.estado uf ON uf.sq_estado = d.sq_estado
                WHERE ( c.in_presenca_atual is null or c.in_presenca_atual <> 'N' )
                  and situacao_avaliacao.cd_sistema in ('REGISTRO_UTILIZADO_AVALIACAO','REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')
                  and (c.sq_metodo_aproximacao is null or c.sq_metodo_aproximacao <> 71 or c.sq_ref_aproximacao <> 62)

                UNION

                SELECT c.sq_ficha,
                       a.sq_ficha_ocorrencia,
                       uc.sq_pessoa                               AS sq_uc,
                       pessoa.no_pessoa                           AS no_uc,
                       null::text                                 AS sg_uc,
                       c.in_presenca_atual,
                       salve.snd(c.in_presenca_atual::text) AS de_presenca_atual,
                       uf.no_estado,
                       uf.sg_estado,
                       'E'::text                            AS cd_esfera,
                       2                                    AS nu_ordem,
                       passo1.co_nivel_taxonomico,
                       'ficha_ocorrencia' as no_tabela,
                       (situacao_avaliacao.cd_sistema = 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')::boolean as st_adicionado_apos_avaliacao,
                       (situacao_avaliacao.cd_sistema = 'REGISTRO_UTILIZADO_AVALIACAO')::boolean as st_utilizado_avaliacao,
                       salve.calc_carencia(c.dt_inclusao::date, carencia.cd_sistema) AS dt_carencia
                        ,uc.cd_cnuc::text as co_cnuc
                FROM salve.ficha_ocorrencia_registro a
                         JOIN salve.registro_uc_estadual b ON b.sq_registro = a.sq_registro
                         JOIN salve.unidade_conservacao uc ON uc.sq_pessoa = b.sq_uc_estadual
                         JOIN salve.pessoa ON pessoa.sq_pessoa = b.sq_uc_estadual
                         JOIN salve.ficha_ocorrencia c ON c.sq_ficha_ocorrencia = a.sq_ficha_ocorrencia
                         JOIN passo1 on passo1.sq_ficha = c.sq_ficha
                         JOIN salve.dados_apoio as situacao_avaliacao on situacao_avaliacao.sq_dados_apoio = c.sq_situacao_avaliacao
                         LEFT JOIN salve.dados_apoio as carencia on carencia.sq_dados_apoio = c.sq_prazo_carencia
                         LEFT JOIN salve.registro_estado d ON d.sq_registro = a.sq_registro
                         LEFT JOIN salve.estado uf ON uf.sq_estado = d.sq_estado
                WHERE ( c.in_presenca_atual is null or c.in_presenca_atual <> 'N' )
                  and situacao_avaliacao.cd_sistema in ('REGISTRO_UTILIZADO_AVALIACAO','REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')
                  and (c.sq_metodo_aproximacao is null or c.sq_metodo_aproximacao <> 71 or c.sq_ref_aproximacao <> 62)

                UNION

                SELECT c.sq_ficha,
                       a.sq_ficha_ocorrencia,
                       uc.sq_pessoa                         AS sq_uc,
                       pessoa.no_pessoa                     AS no_uc,
                       null::text                           AS sg_uc,
                       c.in_presenca_atual,
                       salve.snd(c.in_presenca_atual::text) AS de_presenca_atual,
                       uf.no_estado,
                       uf.sg_estado,
                       'R'::text                            AS cd_esfera,
                       3                                    AS nu_ordem,
                       passo1.co_nivel_taxonomico,
                       'ficha_ocorrencia' as no_tabela,
                       (situacao_avaliacao.cd_sistema = 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')::boolean as st_adicionado_apos_avaliacao,
                       (situacao_avaliacao.cd_sistema = 'REGISTRO_UTILIZADO_AVALIACAO')::boolean as st_utilizado_avaliacao,
                       salve.calc_carencia(c.dt_inclusao::date, carencia.cd_sistema) AS dt_carencia
                        ,null::text as co_cnuc
                FROM salve.ficha_ocorrencia_registro a
                         JOIN salve.registro_rppn b ON b.sq_registro = a.sq_registro
                         JOIN salve.unidade_conservacao uc ON uc.sq_pessoa = b.sq_rppn
                         JOIN salve.pessoa on pessoa.sq_pessoa = b.sq_rppn
                         JOIN salve.ficha_ocorrencia c ON c.sq_ficha_ocorrencia = a.sq_ficha_ocorrencia
                         JOIN passo1 on passo1.sq_ficha = c.sq_ficha
                         JOIN salve.dados_apoio as situacao_avaliacao on situacao_avaliacao.sq_dados_apoio = c.sq_situacao_avaliacao
                         LEFT JOIN salve.dados_apoio as carencia on carencia.sq_dados_apoio = c.sq_prazo_carencia
                         LEFT JOIN salve.registro_estado d ON d.sq_registro = a.sq_registro
                         LEFT JOIN salve.estado uf ON uf.sq_estado = d.sq_estado
                WHERE ( c.in_presenca_atual is null or c.in_presenca_atual <> 'N' )
                  and situacao_avaliacao.cd_sistema in ('REGISTRO_UTILIZADO_AVALIACAO','REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')
                  and (c.sq_metodo_aproximacao is null or c.sq_metodo_aproximacao <> 71 or c.sq_ref_aproximacao <> 62)

                UNION

                -- terras indigenas pontos SALVE

                SELECT c.sq_ficha,
                       a.sq_ficha_ocorrencia,
                       uc.sq_pessoa                              AS sq_uc,
                       pessoa.no_pessoa                          AS no_uc,
                       null::text                                AS sg_uc,
                       c.in_presenca_atual,
                       salve.snd(c.in_presenca_atual::text) AS de_presenca_atual,
                       uf.no_estado,
                       uf.sg_estado,
                       'T'::text                            AS cd_esfera,
                       4                                    AS nu_ordem,
                       passo1.co_nivel_taxonomico,
                       'ficha_ocorrencia' as no_tabela,
                       (situacao_avaliacao.cd_sistema = 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')::boolean as st_adicionado_apos_avaliacao,
                       (situacao_avaliacao.cd_sistema = 'REGISTRO_UTILIZADO_AVALIACAO')::boolean as st_utilizado_avaliacao,
                       salve.calc_carencia(c.dt_inclusao::date, carencia.cd_sistema) AS dt_carencia
                        ,NULL::TEXT AS co_cnuc
                FROM salve.ficha_ocorrencia_registro a
                         JOIN salve.registro_terra_indigena b ON b.sq_registro = a.sq_registro
                         JOIN salve.unidade_conservacao uc ON uc.sq_pessoa = b.sq_terra_indigena
                         JOIN salve.pessoa ON pessoa.sq_pessoa = b.sq_terra_indigena
                         JOIN salve.ficha_ocorrencia c ON c.sq_ficha_ocorrencia = a.sq_ficha_ocorrencia
                         JOIN passo1 on passo1.sq_ficha = c.sq_ficha
                         JOIN salve.dados_apoio as situacao_avaliacao on situacao_avaliacao.sq_dados_apoio = c.sq_situacao_avaliacao
                         LEFT JOIN salve.dados_apoio as carencia on carencia.sq_dados_apoio = c.sq_prazo_carencia
                         LEFT JOIN salve.registro_estado d ON d.sq_registro = a.sq_registro
                         LEFT JOIN salve.estado uf ON uf.sq_estado = d.sq_estado
                WHERE ( c.in_presenca_atual is null or c.in_presenca_atual <> 'N' )
                  and situacao_avaliacao.cd_sistema in ('REGISTRO_UTILIZADO_AVALIACAO','REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')
                  and (c.sq_metodo_aproximacao is null or c.sq_metodo_aproximacao <> 71 or c.sq_ref_aproximacao <> 62)

                UNION


                SELECT c.sq_ficha,
                       a.sq_ficha_ocorrencia_portalbio,
                       uc.sq_pessoa      AS sq_uc,
                       instituicao.sg_instituicao AS no_uc,
                       null              as sg_uc,
                       null              as in_presenca_atual,
                       ''                as de_presenca_atual,
                       uf.no_estado,
                       uf.sg_estado,
                       'F'::text         AS cd_esfera,
                       1                 AS nu_ordem,
                       passo1.co_nivel_taxonomico,
                       'ficha_ocorrencia_portalbio' as no_tabela,
                       (situacao_avaliacao.cd_sistema = 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')::boolean as st_adicionado_apos_avaliacao,
                       (situacao_avaliacao.cd_sistema = 'REGISTRO_UTILIZADO_AVALIACAO')::boolean as st_utilizado_avaliacao,
                       c.dt_carencia AS dt_carencia
                        ,uc.cd_cnuc::text as co_cnuc
                FROM salve.ficha_ocorrencia_portalbio_reg a
                         JOIN salve.registro_uc_federal b ON b.sq_registro = a.sq_registro
                         JOIN salve.unidade_conservacao uc ON uc.sq_pessoa = b.sq_uc_federal
                         JOIN salve.instituicao ON instituicao.sq_pessoa = b.sq_uc_federal
                         JOIN salve.ficha_ocorrencia_portalbio c ON c.sq_ficha_ocorrencia_portalbio = a.sq_ficha_ocorrencia_portalbio
                         JOIN passo1 on passo1.sq_ficha = c.sq_ficha
                         JOIN salve.dados_apoio as situacao_avaliacao on situacao_avaliacao.sq_dados_apoio = c.sq_situacao_avaliacao
                         LEFT JOIN salve.registro_estado d ON d.sq_registro = a.sq_registro
                         LEFT JOIN salve.estado uf ON uf.sq_estado = d.sq_estado
                WHERE situacao_avaliacao.cd_sistema in ('REGISTRO_UTILIZADO_AVALIACAO','REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')

                UNION

                SELECT c.sq_ficha,
                       a.sq_ficha_ocorrencia_portalbio,
                       uc.sq_pessoa               AS sq_uc,
                       instituicao.sg_instituicao AS no_uc,
                       null        as sg_uc,
                       null        as in_presenca_atual,
                       ''          as de_presenca_atual,
                       uf.no_estado,
                       uf.sg_estado,
                       'E'::text   AS cd_esfera,
                       2           AS nu_ordem,
                       passo1.co_nivel_taxonomico,
                       'ficha_ocorrencia_portalbio' as no_tabela,
                       (situacao_avaliacao.cd_sistema = 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')::boolean as st_adicionado_apos_avaliacao,
                       (situacao_avaliacao.cd_sistema = 'REGISTRO_UTILIZADO_AVALIACAO')::boolean as st_utilizado_avaliacao,
                       c.dt_carencia AS dt_carencia
                        ,uc.cd_cnuc::text as co_cnuc
                FROM salve.ficha_ocorrencia_portalbio_reg a
                         JOIN salve.registro_uc_estadual b ON b.sq_registro = a.sq_registro
                         JOIN salve.unidade_conservacao uc ON uc.sq_pessoa = b.sq_uc_estadual
                         JOIN salve.instituicao ON instituicao.sq_pessoa = b.sq_uc_estadual
                         JOIN salve.ficha_ocorrencia_portalbio c ON c.sq_ficha_ocorrencia_portalbio = a.sq_ficha_ocorrencia_portalbio
                         JOIN passo1 on passo1.sq_ficha = c.sq_ficha
                         JOIN salve.dados_apoio as situacao_avaliacao on situacao_avaliacao.sq_dados_apoio = c.sq_situacao_avaliacao
                         LEFT JOIN salve.registro_estado d ON d.sq_registro = a.sq_registro
                         LEFT JOIN salve.estado uf ON uf.sq_estado = d.sq_estado
                WHERE situacao_avaliacao.cd_sistema in ('REGISTRO_UTILIZADO_AVALIACAO','REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')

                UNION

                SELECT c.sq_ficha,
                       a.sq_ficha_ocorrencia_portalbio,
                       uc.sq_pessoa AS sq_uc,
                       instituicao.sg_instituicao   AS no_uc,
                       null::text   AS sg_uc,
                       null         as in_presenca_atual,
                       ''           as de_presenca_atual,
                       uf.no_estado,
                       uf.sg_estado,
                       'R'::text    AS cd_esfera,
                       3            AS nu_ordem,
                       passo1.co_nivel_taxonomico,
                       'ficha_ocorrencia_portalbio' as no_tabela,
                       (situacao_avaliacao.cd_sistema = 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')::boolean as st_adicionado_apos_avaliacao,
                       (situacao_avaliacao.cd_sistema = 'REGISTRO_UTILIZADO_AVALIACAO')::boolean as st_utilizado_avaliacao,
                       c.dt_carencia AS dt_carencia
                        ,null::text as co_cnuc
                FROM salve.ficha_ocorrencia_portalbio_reg a
                         JOIN salve.registro_rppn b ON b.sq_registro = a.sq_registro
                         JOIN salve.unidade_conservacao uc ON uc.sq_pessoa = b.sq_rppn
                         JOIN salve.instituicao ON instituicao.sq_pessoa = b.sq_rppn
                         JOIN salve.ficha_ocorrencia_portalbio c ON c.sq_ficha_ocorrencia_portalbio = a.sq_ficha_ocorrencia_portalbio
                         JOIN passo1 on passo1.sq_ficha = c.sq_ficha
                         JOIN salve.dados_apoio as situacao_avaliacao on situacao_avaliacao.sq_dados_apoio = c.sq_situacao_avaliacao
                         LEFT JOIN salve.registro_estado d ON d.sq_registro = a.sq_registro
                         LEFT JOIN salve.estado uf ON uf.sq_estado = d.sq_estado
                WHERE situacao_avaliacao.cd_sistema in ('REGISTRO_UTILIZADO_AVALIACAO','REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')

                UNION

                -- TERRAS INDIGENAS PONTOS PORTALBIO
                SELECT c.sq_ficha,
                       a.sq_ficha_ocorrencia_portalbio,
                       uc.sq_pessoa AS sq_uc,
                       pessoa.no_pessoa AS no_uc,
                       null         as sg_uc,
                       null         as in_presenca_atual,
                       ''           as de_presenca_atual,
                       uf.no_estado,
                       uf.sg_estado,
                       'T'::text    AS cd_esfera,
                       4            AS nu_ordem,
                       passo1.co_nivel_taxonomico,
                       'ficha_ocorrencia_portalbio' as no_tabela,
                       (situacao_avaliacao.cd_sistema = 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')::boolean as st_adicionado_apos_avaliacao,
                       (situacao_avaliacao.cd_sistema = 'REGISTRO_UTILIZADO_AVALIACAO')::boolean as st_utilizado_avaliacao,
                       c.dt_carencia AS dt_carencia
                        ,null::text as co_cnuc
                FROM salve.ficha_ocorrencia_portalbio_reg a
                         JOIN salve.registro_terra_indigena b ON b.sq_registro = a.sq_registro
                         JOIN salve.unidade_conservacao uc ON uc.sq_pessoa = b.sq_terra_indigena
                         JOIN salve.pessoa ON pessoa.sq_pessoa = b.sq_terra_indigena
                         JOIN salve.ficha_ocorrencia_portalbio c ON c.sq_ficha_ocorrencia_portalbio = a.sq_ficha_ocorrencia_portalbio
                         JOIN passo1 on passo1.sq_ficha = c.sq_ficha
                         JOIN salve.dados_apoio as situacao_avaliacao on situacao_avaliacao.sq_dados_apoio = c.sq_situacao_avaliacao
                         LEFT JOIN salve.registro_estado d ON d.sq_registro = a.sq_registro
                         LEFT JOIN salve.estado uf ON uf.sq_estado = d.sq_estado
                WHERE situacao_avaliacao.cd_sistema in ('REGISTRO_UTILIZADO_AVALIACAO','REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')


            )
            SELECT cte.sq_ficha,
                   cte.sq_uc,
                   cte.no_uc,
                   cte.sg_uc,
                   cte.cd_esfera,
                   cte.in_presenca_atual,
                   cte.de_presenca_atual,
                   cte.no_estado,
                   cte.sg_estado,
                   cte.nu_ordem,
                   COALESCE(publicacao.de_titulo, ''::text)                  AS de_titulo,
                   COALESCE(publicacao.no_autor, x.no_autor::text)           AS no_autor,
                   COALESCE(publicacao.nu_ano_publicacao::integer, x.nu_ano) AS nu_ano,
                   cte.co_nivel_taxonomico,
                   cte.no_tabela,
                   cte.st_adicionado_apos_avaliacao,
                   cte.st_utilizado_avaliacao,
                   case when cte.dt_carencia is null or cte.dt_carencia < now() then false else true end as st_em_carencia,
                   cte.co_cnuc::text

            FROM cte
                     LEFT JOIN salve.ficha_ref_bib x
                               ON x.sq_ficha = cte.sq_ficha AND x.sq_registro = cte.sq_ficha_ocorrencia AND
                                  x.no_tabela::text = cte.no_tabela::text AND
                                  x.no_coluna::text = concat('sq_',cte.no_tabela)::text
                     LEFT JOIN taxonomia.publicacao ON publicacao.sq_publicacao = x.sq_publicacao
        ),
             passoFinal as (
                 SELECT distinct cte1.sq_ficha::bigint,
                                 cte1.sq_uc::bigint,
                                 cte1.no_uc::text,
                                 cte1.sg_uc::text,
                                 cte1.cd_esfera::text,
                                 cte1.in_presenca_atual::text,
                                 cte1.de_presenca_atual::text,
                                 cte1.sg_estado::text,
                                 cte1.no_estado::text,
                                 cte1.nu_ordem::integer,
                                 cte1.co_nivel_taxonomico,
                                 cte1.st_adicionado_apos_avaliacao,
                                 cte1.st_utilizado_avaliacao,
                                 cte1.st_em_carencia,
                                 json_object_agg(concat('rnd', trunc(random() * 100000::double precision)::text),
                                                 json_build_object('de_titulo', cte1.de_titulo, 'no_autor',
                                                                   cte1.no_autor, 'nu_ano',
                                                                   cte1.nu_ano))::text AS json_ref_bib,
                                 cte1.co_cnuc
                 FROM cte1

                 GROUP BY cte1.sq_ficha, cte1.nu_ordem, cte1.sq_uc, cte1.sg_uc, cte1.no_uc, cte1.cd_esfera, cte1.in_presenca_atual,
                          cte1.de_presenca_atual, cte1.sg_estado, cte1.no_estado, cte1.co_nivel_taxonomico,
                          cte1.st_adicionado_apos_avaliacao, cte1.st_utilizado_avaliacao, cte1.st_em_carencia,
                          cte1.co_cnuc
             )
        select *
        from passoFinal
        order by passoFinal.nu_ordem, public.fn_remove_acentuacao(upper(passoFinal.no_uc));

end;
$$;


--
-- TOC entry 1791 (class 1255 OID 29060)
-- Name: fn_info_registro_portalbio(bigint); Type: FUNCTION; Schema: salve; Owner: -
--

CREATE FUNCTION salve.fn_info_registro_portalbio(p_sq_ficha_ocorrencia_portalbio bigint) RETURNS TABLE(no_base_dados text, sq_ficha_ocorrencia_portalbio integer, sq_ficha bigint, sq_motivo_nao_utilizado_avaliacao bigint, nu_lat double precision, nu_lon double precision, no_local text, ds_precisao text, ds_autorizacao_sisbio text, no_pessoa_inclusao text, dt_carencia text, ds_situacao_avaliacao text, cd_situacao_avaliacao text, tx_justificativa_nao_utilizado text, dt_inclusao text, ds_uuid text, tx_ocorrencia text, tx_observacao text, nm_cientifico text, no_estado text, no_municipio text, ds_carencia text, ds_motivo_nao_utilizado text, in_subespecie text, json_ref_bib text, in_utilizado_avaliacao text, st_adicionado_apos_avaliacao boolean, ds_utilizado_avaliacao text, in_centroide_estado boolean, ds_presenca_atual_coordenada text, in_presenca_atual text)
    LANGUAGE plpgsql
    AS $$
declare existeRegistro bigint;
begin
    -- verificar se o id existe na nova tabela
    select sq_ficha_ocorrencia_portalbio_reg into existeRegistro from salve.ficha_ocorrencia_portalbio_reg where ficha_ocorrencia_portalbio_reg.sq_ficha_ocorrencia_portalbio = p_sq_ficha_ocorrencia_portalbio limit 1;
    if existeRegistro is not null then
        return query
            with cte as (
                SELECT salve.calc_base_dados_portalbio(concat(a.tx_ocorrencia,' ',a.no_instituicao),a.de_uuid) as no_base_dados
                     , a.sq_ficha_ocorrencia_portalbio
                     , a.sq_ficha
                     , a.sq_motivo_nao_utilizado_avaliacao
                     , st_y(a.ge_ocorrencia)                                                                           as nu_lat
                     , st_x(a.ge_ocorrencia)                                                                           as nu_lon
                     , a.no_local
                     , ((case when position('{' in a.tx_ocorrencia) < 1 then '{}' else a.tx_ocorrencia end ::jsonb) ->>
                        'precisaoCoord'::text)                                                                         as ds_precisao
                     , ((case when position('{' in a.tx_ocorrencia) < 1 then '{}' else a.tx_ocorrencia end ::jsonb) ->>
                        'recordNumber'::text)                                                                          as ds_autorizacao_sisbio
                     , a.no_autor                                                                                      as no_pessoa_inclusao
                     , to_char(a.dt_carencia, 'DD/MM/YYYY')                                                            as dt_carencia
                     , situacao_avaliacao.ds_dados_apoio                            as ds_situacao_avaliacao
                     , situacao_avaliacao.cd_sistema                                as cd_situacao_avaliacao
                     , coalesce(a.tx_nao_utilizado_avaliacao, '')                                                      as tx_justificativa_nao_utilizado
                     , to_char(a.dt_ocorrencia, 'DD/MM/YYYY')                                                          as dt_inclusao
                     , a.de_uuid                                                                                       as ds_uuid
                     , a.tx_ocorrencia
                     , a.tx_observacao
                     , ficha.nm_cientifico
                     , case
                           when a.sq_estado is null then
                                   (case when position('{' in a.tx_ocorrencia) < 1 then '{}' else a.tx_ocorrencia end::jsonb) ->>
                                   'estado'
                           else concat(estado.no_estado, ' (', estado.sg_estado, ')') end                              as no_estado
                     , case
                           when municipio.no_municipio is null then
                                   (case when position('{' in a.tx_ocorrencia) < 1 then '{}' else a.tx_ocorrencia end::jsonb) ->>
                                   'municipio'
                           else municipio.no_municipio end                                                             as no_municipio
                     , coalesce(motivo.ds_dados_apoio, '')                                                             as ds_motivo_nao_utilizado
                     , case when nivel.co_nivel_taxonomico = 'SUBESPECIE' then 'S' else 'N' end                        as in_subespecie
                     , ref_bib.json_ref_bib::text
                     , a.in_utilizado_avaliacao::text                                                                  as in_utilizado_avaliacao
                     , a.st_adicionado_apos_avaliacao::boolean                                                         as st_adicionado_apos_avaliacao
                     , false as in_centroide_estado
                     , a.in_presenca_atual
                from salve.ficha_ocorrencia_portalbio as a
                         inner join salve.ficha on ficha.sq_ficha = a.sq_ficha
                         inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                         inner join taxonomia.nivel_taxonomico as nivel on nivel.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
                         inner join salve.dados_apoio as situacao_avaliacao on situacao_avaliacao.sq_dados_apoio = a.sq_situacao_avaliacao
                         left outer join salve.estado estado on estado.sq_estado = a.sq_estado
                         left outer join salve.municipio municipio on municipio.sq_municipio = a.sq_municipio
                         left outer join salve.dados_apoio as motivo on motivo.sq_dados_apoio = a.sq_motivo_nao_utilizado_avaliacao
                         left join lateral (
                    select json_object_agg(x.sq_ficha_ref_bib,
                                           json_build_object('de_titulo', coalesce(publicacao.de_titulo, '')
                                               , 'no_autor', coalesce(publicacao.no_autor, x.no_autor)
                                               , 'nu_ano', coalesce(publicacao.nu_ano_publicacao, x.nu_ano))) as json_ref_bib
                    from salve.ficha_ref_bib x
                             left outer join taxonomia.publicacao on publicacao.sq_publicacao = x.sq_publicacao
                    where x.sq_ficha = a.sq_ficha
                      and x.sq_registro = a.sq_ficha_ocorrencia_portalbio
                      and x.no_tabela = 'ficha_ocorrencia_portalbio'
                      and x.no_coluna = 'sq_ficha_ocorrencia_portalbio'
                    ) as ref_bib on true
                where a.sq_ficha_ocorrencia_portalbio = p_sq_ficha_ocorrencia_portalbio
            )
            select
                case when cte.no_base_dados ilike '%sisbio%' then 'ICMBIo/SISBio' else cte.no_base_dados end as no_base_dados,
                cte.sq_ficha_ocorrencia_portalbio,
                cte.sq_ficha,
                cte.sq_motivo_nao_utilizado_avaliacao,
                cte.nu_lat,
                cte.nu_lon,
                cte.no_local,
                cte.ds_precisao,
                cte.ds_autorizacao_sisbio,
                cte.no_pessoa_inclusao,
                cte.dt_carencia,
                cte.ds_situacao_avaliacao,
                cte.cd_situacao_avaliacao,
                cte.tx_justificativa_nao_utilizado,
                cte.dt_inclusao,
                cte.ds_uuid,
                cte.tx_ocorrencia,
                cte.tx_observacao,
                cte.nm_cientifico,
                cte.no_estado,
                cte.no_municipio,
                coalesce(cte.dt_carencia::text, 'Sem carência') as ds_carencia,
                cte.ds_motivo_nao_utilizado,
                cte.in_subespecie,
                cte.json_ref_bib,
                cte.in_utilizado_avaliacao,
                cte.st_adicionado_apos_avaliacao,
                salve.fn_calc_situacao_registro(cte.in_utilizado_avaliacao,cte.st_adicionado_apos_avaliacao) as ds_utilizado_avaliacao,
                cte.in_centroide_estado,
                salve.snd(cte.in_presenca_atual)                                               as ds_presenca_atual_coordenada,
                cte.in_presenca_atual::text
            from cte;
    else
        return query
            with cte as (
                SELECT  salve.calc_base_dados_portalbio(concat(a.tx_ocorrencia,' ',a.no_instituicao),a.de_uuid ) as no_base_dados
                     , a.sq_ficha_ocorrencia_portalbio
                     , a.sq_ficha
                     , a.sq_motivo_nao_utilizado_avaliacao
                     , st_y(a.ge_ocorrencia)                                                                           as nu_lat
                     , st_x(a.ge_ocorrencia)                                                                           as nu_lon
                     , a.no_local
                     , ((case when position('{' in a.tx_ocorrencia) < 1 then '{}' else a.tx_ocorrencia end ::jsonb) ->>
                        'precisaoCoord'::text)                                                                         as ds_precisao
                     , ((case when position('{' in a.tx_ocorrencia) < 1 then '{}' else a.tx_ocorrencia end ::jsonb) ->>
                        'recordNumber'::text)                                                                          as ds_autorizacao_sisbio
                     , a.no_autor                                                                                      as no_pessoa_inclusao
                     , to_char(a.dt_carencia, 'DD/MM/YYYY')                                                            as dt_carencia
                     , situacao_avaliacao.ds_dados_apoio                            as ds_situacao_avaliacao
                     , situacao_avaliacao.cd_sistema                                as cd_situacao_avaliacao
                     , coalesce(a.tx_nao_utilizado_avaliacao, '')                                                      as tx_justificativa_nao_utilizado
                     , to_char(a.dt_ocorrencia, 'DD/MM/YYYY')                                                          as dt_inclusao
                     , a.de_uuid                                                                                       as ds_uuid
                     , a.tx_ocorrencia
                     , a.tx_observacao
                     , ficha.nm_cientifico
                     , case
                           when a.sq_estado is null then
                                   (case when position('{' in a.tx_ocorrencia) < 1 then '{}' else a.tx_ocorrencia end::jsonb) ->>
                                   'estado'
                           else concat(estado.no_estado, ' (', estado.sg_estado, ')') end                              as no_estado
                     , case
                           when municipio.no_municipio is null then
                                   (case when position('{' in a.tx_ocorrencia) < 1 then '{}' else a.tx_ocorrencia end::jsonb) ->>
                                   'municipio'
                           else municipio.no_municipio end                                                             as no_municipio
                     , coalesce(motivo.ds_dados_apoio, '')                                                             as ds_motivo_nao_utilizado
                     , case when nivel.co_nivel_taxonomico = 'SUBESPECIE' then 'S' else 'N' end                        as in_subespecie
                     , ref_bib.json_ref_bib::text
                     , a.in_utilizado_avaliacao::text                                                                  as in_utilizado_avaliacao
                     , a.st_adicionado_apos_avaliacao::boolean                                                         as st_adicionado_apos_avaliacao
                     , false as in_centroide_estado
                     , a.in_presenca_atual
                from salve.ficha_ocorrencia_portalbio as a
                         inner join salve.ficha on ficha.sq_ficha = a.sq_ficha
                         inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                         inner join taxonomia.nivel_taxonomico as nivel on nivel.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
                         inner join salve.dados_apoio as situacao_avaliacao on situacao_avaliacao.sq_dados_apoio = a.sq_situacao_avaliacao
                         left outer join salve.estado estado on estado.sq_estado = a.sq_estado
                         left outer join salve.municipio municipio on municipio.sq_municipio = a.sq_municipio
                         left outer join salve.dados_apoio as motivo on motivo.sq_dados_apoio = a.sq_motivo_nao_utilizado_avaliacao

                         left join lateral (
                    select json_object_agg(x.sq_ficha_ref_bib,
                                           json_build_object('de_titulo', coalesce(publicacao.de_titulo, '')
                                               , 'no_autor', coalesce(publicacao.no_autor, x.no_autor)
                                               , 'nu_ano', coalesce(publicacao.nu_ano_publicacao, x.nu_ano))) as json_ref_bib
                    from salve.ficha_ref_bib x
                             left outer join taxonomia.publicacao on publicacao.sq_publicacao = x.sq_publicacao
                    where x.sq_ficha = a.sq_ficha
                      and x.sq_registro = a.sq_ficha_ocorrencia_portalbio
                      and x.no_tabela = 'ficha_ocorrencia_portalbio'
                      and x.no_coluna = 'sq_ficha_ocorrencia_portalbio'
                    ) as ref_bib on true
                where a.sq_ficha_ocorrencia_portalbio = p_sq_ficha_ocorrencia_portalbio
            )
            select
                case when cte.no_base_dados ilike '%sisbio%' then 'SISBIO' else cte.no_base_dados end as no_base_dados,
                cte.sq_ficha_ocorrencia_portalbio,
                cte.sq_ficha,
                cte.sq_motivo_nao_utilizado_avaliacao,
                cte.nu_lat,
                cte.nu_lon,
                cte.no_local,
                cte.ds_precisao,
                cte.ds_autorizacao_sisbio,
                cte.no_pessoa_inclusao,
                cte.dt_carencia,
                cte.ds_situacao_avaliacao,
                cte.cd_situacao_avaliacao,
                cte.tx_justificativa_nao_utilizado,
                cte.dt_inclusao,
                cte.ds_uuid,
                cte.tx_ocorrencia,
                cte.tx_observacao,
                cte.nm_cientifico,
                cte.no_estado,
                cte.no_municipio,
                coalesce(cte.dt_carencia::text, 'Sem carência') as ds_carencia,
                cte.ds_motivo_nao_utilizado,
                cte.in_subespecie,
                cte.json_ref_bib,
                cte.in_utilizado_avaliacao,
                cte.st_adicionado_apos_avaliacao,
                salve.fn_calc_situacao_registro(cte.in_utilizado_avaliacao,cte.st_adicionado_apos_avaliacao) as ds_utilizado_avaliacao,
                cte.in_centroide_estado,
                salve.snd(cte.in_presenca_atual)                                               as ds_presenca_atual_coordenada,
                cte.in_presenca_atual::text

            from cte;

    end if;
end;
$$;


--
-- TOC entry 1792 (class 1255 OID 29063)
-- Name: fn_info_registro_salve(bigint); Type: FUNCTION; Schema: salve; Owner: -
--

CREATE FUNCTION salve.fn_info_registro_salve(p_sq_ficha_ocorrencia bigint) RETURNS TABLE(no_base_dados text, sq_ficha_ocorrencia bigint, sq_ficha bigint, sq_pessoa_inclusao bigint, sq_precisao_coordenada bigint, sq_prazo_carencia bigint, sq_motivo_nao_utilizado_avaliacao bigint, nu_lat double precision, nu_lon double precision, in_sensivel text, ds_presenca_atual_coordenada text, ds_tombamento text, no_instituicao_tombamento text, dt_carencia date, ds_situacao_avaliacao text, cd_situacao_avaliacao text, tx_justificativa_nao_utilizado text, ds_motivo_nao_utilizado text, dt_coleta text, hr_coleta text, dt_inclusao text, id_origem text, in_centroide_estado boolean, tx_observacao text, nm_cientifico text, in_subespecie text, no_estado text, no_municipio text, ds_carencia text, ds_sensivel text, no_usuario_consulta text, ds_tipo_consulta text, tx_observacao_usuario_web text, no_consulta text, sg_consulta text, ds_periodo text, no_pessoa_inclusao text, json_ref_bib text, ds_precisao text, in_utilizado_avaliacao text, ds_utilizado_avaliacao text, in_presenca_atual text)
    LANGUAGE plpgsql
    AS $$
begin
    return query
        with cte2 as (
            select fo.sq_ficha_ocorrencia
            from salve.ficha_ocorrencia as fo
            where fo.sq_ficha_ocorrencia = p_sq_ficha_ocorrencia
        ),
             cte as (
                 SELECT fo.sq_ficha_ocorrencia
                      , fo.sq_ficha
                      , fo.sq_pessoa_inclusao
                      , fo.sq_precisao_coordenada
                      , fo.sq_prazo_carencia
                      , fo.sq_motivo_nao_utilizado_avaliacao
                      , fo.sq_situacao_avaliacao
                      , st_y(fo.ge_ocorrencia)                                                        as nu_lat
                      , st_x(fo.ge_ocorrencia)                                                        as nu_lon
                      , upper(coalesce(fo.in_sensivel, 'S'))                                          as in_sensivel
                      , salve.snd(fo.in_presenca_atual)                                               as ds_presenca_atual_coordenada
                      , coalesce(fo.tx_tombamento, '')                                                as ds_tombamento
                      , coalesce(fo.tx_instituicao,'')                                                as no_instituicao_tombamento
                      , salve.calc_carencia(fo.dt_inclusao::date, prazo_carencia.cd_sistema) as dt_carencia
                      , coalesce(fo.tx_nao_utilizado_avaliacao, '')         as tx_justificativa_nao_utilizado
                      , case
                            when fo.nu_dia_registro is not null then
                                concat(lpad(fo.nu_dia_registro::text, 2, '0'), '/',
                                       lpad(fo.nu_mes_registro::text, 2, '0'), '/',
                                       fo.nu_ano_registro)
                            else case
                                     when fo.nu_mes_registro is not null then
                                         concat(lpad(fo.nu_mes_registro::text, 2, '0'), '/', fo.nu_ano_registro)
                                     else fo.nu_ano_registro::text end
                     end                                                                              as dt_coleta
                      , case
                            when fo.nu_dia_registro_fim is not null then
                                concat(lpad(fo.nu_dia_registro_fim::text, 2, '0'), '/',
                                       lpad(fo.nu_mes_registro_fim::text, 2, '0'), '/', fo.nu_ano_registro_fim)
                            else case
                                     when fo.nu_mes_registro_fim is not null then
                                         concat(lpad(fo.nu_mes_registro_fim::text, 2, '0'), '/', fo.nu_ano_registro_fim)
                                     else fo.nu_ano_registro_fim::text end
                     end                                                                              as dt_coleta_fim
                      , fo.hr_registro                                                                as hr_coleta
                      , to_char(fo.dt_inclusao, 'DD/MM/YYYY')                                         as dt_inclusao
                      , coalesce(fo.id_origem::text, concat('SALVE: ', fo.sq_ficha_ocorrencia::text)) as id_origem
                      , case
                            when coalesce(metodoAproximacao.cd_sistema, '') = 'CENTROIDE' and
                                 coalesce(refAproximacao.cd_sistema, '') = 'ESTADO' then true
                            else false end                                                            as in_centroide_estado
                      , fo.tx_observacao
                      , upper(case when fo.sq_contexto = 420 then
                                       coalesce( fo.in_utilizado_avaliacao, 'S') else
                                       coalesce( fo.in_utilizado_avaliacao ,'') end )
                                                                                                      as in_utilizado_avaliacao
                      ,fo.st_adicionado_apos_avaliacao
                      ,fo.in_presenca_atual
                 from salve.ficha_ocorrencia as fo
                          inner join cte2 on cte2.sq_ficha_ocorrencia = fo.sq_ficha_ocorrencia
                          left outer join salve.dados_apoio as prazo_carencia
                                          on prazo_carencia.sq_dados_apoio = fo.sq_prazo_carencia
                          left outer join salve.dados_apoio as metodoAproximacao
                                          on metodoAproximacao.sq_dados_apoio = fo.sq_metodo_aproximacao
                          left outer join salve.dados_apoio as refAproximacao
                                          on refAproximacao.sq_dados_apoio = fo.sq_ref_aproximacao
             )
        select case when consulta.sq_ficha_ocorrencia is null then 'ICMBio/SALVE' else 'ICMBio/SALVE/CONSULTA' end as no_base_dados
             , cte.sq_ficha_ocorrencia::bigint
             , cte.sq_ficha::bigint
             , cte.sq_pessoa_inclusao::bigint
             , cte.sq_precisao_coordenada::bigint
             , cte.sq_prazo_carencia::bigint
             , cte.sq_motivo_nao_utilizado_avaliacao::bigint
             , cte.nu_lat::double precision
             , cte.nu_lon::double precision
             , cte.in_sensivel::text
             , cte.ds_presenca_atual_coordenada::text
             , cte.ds_tombamento::text
             , cte.no_instituicao_tombamento::text
             , cte.dt_carencia::date
             , situacao_avaliacao.ds_dados_apoio::text                   as ds_situacao_avaliacao
             , situacao_avaliacao.cd_sistema::text                       as cd_situacao_avaliacao
             , cte.tx_justificativa_nao_utilizado::text                  as tx_justificativa_nao_utilizado
             , coalesce(motivo.ds_dados_apoio, '')::text                 as ds_motivo_nao_utilizado
             , case
                   when cte.dt_coleta is not null and cte.dt_coleta_fim is not null
                       then concat(cte.dt_coleta, ' a ', cte.dt_coleta_fim)
                   else
                       case when cte.dt_coleta is not null then cte.dt_coleta else '' end::text
            end                                                                          as dt_coleta
             , cte.hr_coleta::text
             , cte.dt_inclusao::text
             , cte.id_origem::text
             , cte.in_centroide_estado::boolean
             , cte.tx_observacao::text
             , ficha.nm_cientifico::text
             , case when nivel.co_nivel_taxonomico = 'SUBESPECIE' then 'S' else 'N' end::text  as in_subespecie
             , case
                   when regUf.sq_estado is null then ''
                   else concat(estado.no_estado, ' (', estado.sg_estado, ')') end::text        as no_estado
             , case when cte.in_centroide_estado then '' else municipio.no_municipio end::text as no_municipio
             , case when cte.dt_carencia is not null then to_char(cte.dt_carencia,'DD/MM/YYYY')  else 'Sem carência' end as ds_carencia
             , case when cte.in_sensivel = 'S' then 'Sim' else 'Não' end::text                 as ds_sensivel
             , usuario_pessoa.no_pessoa::text                                                        as no_usuario_consulta
             , tipo_consulta.ds_dados_apoio::text                                              as ds_tipo_consulta
             , consulta.tx_observacao::text                                                    as tx_observacao_usuario_web
             , ciclo_consulta.de_titulo_consulta::text                                         as no_consulta
             , ciclo_consulta.sg_consulta::text                                                as sg_consulta
             , case
                   when ciclo_consulta.dt_inicio is not null then
                       concat(to_char(ciclo_consulta.dt_inicio, 'DD/MM/YYYY'), ' a ',
                              to_char(ciclo_consulta.dt_fim, 'DD/MM/YYYY'))
                   else '' end::text                                                           as ds_periodo
             , pf_inclusao.no_pessoa::text                                                              as no_pessoa_inclusao
             , ref_bib.json_ref_bib::text                                                as json_ref_bib
             , coalesce(precisao.ds_dados_apoio, '')::text                                     as ds_precisao
             , cte.in_utilizado_avaliacao
             , salve.fn_calc_situacao_registro(cte.in_utilizado_avaliacao,cte.st_adicionado_apos_avaliacao) as ds_utilizado_avaliacao
             , cte.in_presenca_atual::text

        from cte
                 inner join salve.dados_apoio as situacao_avaliacao on situacao_avaliacao.sq_dados_apoio = cte.sq_situacao_avaliacao
                 left outer join salve.ficha_ocorrencia_consulta as consulta on consulta.sq_ficha_ocorrencia = cte.sq_ficha_ocorrencia
                 left outer join salve.usuario as usuario on usuario.sq_pessoa = consulta.sq_web_usuario
                 left outer join salve.pessoa as usuario_pessoa on usuario_pessoa.sq_pessoa = usuario.sq_pessoa
                 left outer join salve.ciclo_consulta_ficha as ciclo_consulta_ficha
                                 on ciclo_consulta_ficha.sq_ciclo_consulta_ficha = consulta.sq_ciclo_consulta_ficha
                 left outer join salve.ciclo_consulta as ciclo_consulta
                                 on ciclo_consulta.sq_ciclo_consulta = ciclo_consulta_ficha.sq_ciclo_consulta
                 left outer join salve.dados_apoio as tipo_consulta
                                 on tipo_consulta.sq_dados_apoio = ciclo_consulta.sq_tipo_consulta
                 left join salve.ficha_ocorrencia_registro freg on freg.sq_ficha_ocorrencia = cte.sq_ficha_ocorrencia
                 left join salve.registro_estado regUf on regUf.sq_registro = freg.sq_registro
                 left join salve.registro_municipio regMu on regMu.sq_registro = freg.sq_registro
                 left outer join salve.estado estado on estado.sq_estado = regUf.sq_estado
                 left outer join salve.municipio municipio on municipio.sq_municipio = regMu.sq_municipio
                 inner join salve.ficha on ficha.sq_ficha = cte.sq_ficha
                 inner join taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
                 inner join taxonomia.nivel_taxonomico as nivel on nivel.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
                 left outer join salve.pessoa_fisica as pf on pf.sq_pessoa = cte.sq_pessoa_inclusao
                 left outer join salve.pessoa as pf_inclusao on pf_inclusao.sq_pessoa = cte.sq_pessoa_inclusao
                 left outer join salve.dados_apoio as precisao on precisao.sq_dados_apoio = cte.sq_precisao_coordenada
                 left outer join salve.dados_apoio as motivo   on motivo.sq_dados_apoio = cte.sq_motivo_nao_utilizado_avaliacao

                 left join lateral (
            select json_object_agg(x.sq_ficha_ref_bib,
                                   json_build_object('de_titulo', coalesce(publicacao.de_titulo, '')
                                       , 'no_autor', coalesce(publicacao.no_autor, x.no_autor)
                                       , 'nu_ano', coalesce(publicacao.nu_ano_publicacao, x.nu_ano))) as json_ref_bib
            from salve.ficha_ref_bib x
                     left outer join taxonomia.publicacao on publicacao.sq_publicacao = x.sq_publicacao
            where x.sq_ficha = cte.sq_ficha
              and x.sq_registro = cte.sq_ficha_ocorrencia
              and x.no_tabela = 'ficha_ocorrencia'
              and x.no_coluna = 'sq_ficha_ocorrencia'
            ) as ref_bib on true;
end;
$$;


--
-- TOC entry 1814 (class 1255 OID 29626)
-- Name: fn_json_get_csv(jsonb, text); Type: FUNCTION; Schema: salve; Owner: -
--

CREATE FUNCTION salve.fn_json_get_csv(p_jsonb jsonb, p_column text) RETURNS text
    LANGUAGE plpgsql
    AS $$
BEGIN
    IF p_jsonb::text = 'null' then
        return '';
    end if;
    return (select array_to_string( array_agg(values::text),', ') as values from
        (SELECT JSONB_ARRAY_ELEMENTS( p_jsonb ) ->> p_column) as t(values));
END
$$;


--
-- TOC entry 1800 (class 1255 OID 29459)
-- Name: fn_ocorrencia_bacias(bigint, boolean); Type: FUNCTION; Schema: salve; Owner: -
--

CREATE FUNCTION salve.fn_ocorrencia_bacias(p_sq_ficha_ocorrencia bigint, p_portalbio boolean DEFAULT false) RETURNS TABLE(sq_bacia bigint, sq_bacia_pai bigint, cd_bacia_pai text, cd_bacia_pai_sistema text, cd_bacia text, cd_bacia_sistema text, no_bacia_pai text, no_bacia text, cd_geo text, de_ordem text, tx_geo text)
    LANGUAGE plpgsql
    AS $$
    /*
     select * from salve.fn_ocorrencia_bacias(289287)
    */
begin
    if not p_portalbio then
        return query
            select bacia.sq_dados_apoio::bigint as sq_bacia
                 ,bacia_pai.sq_dados_apoio::bigint as sq_bacia_pai
                 ,bacia_pai.cd_dados_apoio::text as cd_bacia_pai
                 ,bacia_pai.cd_sistema::text as cd_bacia_pai_sistema
                 ,bacia.cd_dados_apoio::text as cd_bacia
                 ,bacia.cd_sistema::text as cd_bacia_sistema
                 ,bacia_pai.ds_dados_apoio::text as no_bacia_pai
                 ,bacia.ds_dados_apoio::text as no_bacia
                 ,geo.cd_geo::text as cd_geo
                 ,bacia.de_dados_apoio_ordem::text as de_ordem
                 ,geo.tx_geo::text as tx_geo
            from salve.ficha_ocorrencia fo
                     inner join salve.ficha_ocorrencia_registro foreg on foreg.sq_ficha_ocorrencia = fo.sq_ficha_ocorrencia
                     inner join salve.registro_bacia reg on reg.sq_registro=foreg.sq_registro
                     inner join salve.dados_apoio_geo geo on geo.sq_dados_apoio_geo = reg.sq_bacia_geo
                     inner join salve.dados_apoio bacia on bacia.sq_dados_apoio = geo.sq_dados_apoio
                     inner join salve.dados_apoio bacia_pai on bacia_pai.sq_dados_apoio = bacia.sq_dados_apoio_pai
            where fo.sq_ficha_ocorrencia = p_sq_ficha_ocorrencia;
    else
        return query
            select bacia.sq_dados_apoio::bigint as sq_bacia
                 ,bacia_pai.sq_dados_apoio::bigint as sq_bacia_pai
                 ,bacia_pai.cd_dados_apoio::text as cd_bacia_pai
                 ,bacia_pai.cd_sistema::text as cd_bacia_pai_sistema
                 ,bacia.cd_dados_apoio::text as cd_bacia
                 ,bacia.cd_sistema::text as cd_bacia_sistema
                 ,bacia_pai.ds_dados_apoio::text as no_bacia_pai
                 ,bacia.ds_dados_apoio::text as no_bacia
                 ,geo.cd_geo::text as cd_geo
                 ,bacia.de_dados_apoio_ordem::text as de_ordem
                 ,geo.tx_geo::text as tx_geo
            from salve.ficha_ocorrencia_portalbio fo
                     inner join salve.ficha_ocorrencia_portalbio_reg foreg on foreg.sq_ficha_ocorrencia_portalbio = fo.sq_ficha_ocorrencia_portalbio
                     inner join salve.registro_bacia reg on reg.sq_registro=foreg.sq_registro
                     inner join salve.dados_apoio_geo geo on geo.sq_dados_apoio_geo = reg.sq_bacia_geo
                     inner join salve.dados_apoio bacia on bacia.sq_dados_apoio = geo.sq_dados_apoio
                     inner join salve.dados_apoio bacia_pai on bacia_pai.sq_dados_apoio = bacia.sq_dados_apoio_pai
            where fo.sq_ficha_ocorrencia_portalbio = p_sq_ficha_ocorrencia;
    end if;
end
$$;


--
-- TOC entry 7880 (class 0 OID 0)
-- Dependencies: 1800
-- Name: FUNCTION fn_ocorrencia_bacias(p_sq_ficha_ocorrencia bigint, p_portalbio boolean); Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON FUNCTION salve.fn_ocorrencia_bacias(p_sq_ficha_ocorrencia bigint, p_portalbio boolean) IS 'Função para listar as bacias de um determiando registro de ocorrencia';


--
-- TOC entry 1801 (class 1255 OID 29460)
-- Name: fn_ocorrencia_bacias_json(bigint, boolean); Type: FUNCTION; Schema: salve; Owner: -
--

CREATE FUNCTION salve.fn_ocorrencia_bacias_json(p_sq_ocorrencia bigint, p_portalbio boolean DEFAULT false) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
    /*
     select * from salve.fn_ocorrencia_bacias_json(289287)
     */
begin
    return (
        select json_agg(json_build_object('sq_bacia_pai',bacia.sq_bacia_pai
            ,'cd_bacia_pai',bacia.cd_bacia_pai
            ,'cd_sistema_pai',bacia.cd_bacia_pai_sistema
            ,'sq_bacia',bacia.sq_bacia
            ,'cd_bacia',bacia.cd_bacia
            ,'cd_sistema',bacia.cd_bacia_sistema
            ,'no_bacia_pai',bacia.no_bacia_pai
            ,'no_bacia',bacia.no_bacia
            ,'cd_geo', bacia.cd_geo
            ,'de_ordem',bacia.de_ordem
            ,'tx_geo',bacia.tx_geo
            )) as js_bacias
        from salve.fn_ocorrencia_bacias(p_sq_ocorrencia, p_portalbio) as bacia
    )::jsonb;
end;
$$;


--
-- TOC entry 7881 (class 0 OID 0)
-- Dependencies: 1801
-- Name: FUNCTION fn_ocorrencia_bacias_json(p_sq_ocorrencia bigint, p_portalbio boolean); Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON FUNCTION salve.fn_ocorrencia_bacias_json(p_sq_ocorrencia bigint, p_portalbio boolean) IS 'Função para gerar objeto json das bacias de um registro de ocorrencia';


--
-- TOC entry 1802 (class 1255 OID 29461)
-- Name: fn_ocorrencia_bioma_json(bigint, boolean); Type: FUNCTION; Schema: salve; Owner: -
--

CREATE FUNCTION salve.fn_ocorrencia_bioma_json(p_sq_ocorrencia bigint, p_portalbio boolean DEFAULT false) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
    /*
     select * from salve.fn_ocorrencia_bioma_json(289287)
     */
begin

    if not p_portalbio then
        return (

            select json_agg(json_build_object('sq_bioma',bioma.sq_bioma
                ,'no_bioma', bioma.no_bioma
                )) as js_bioma
            from salve.ficha_ocorrencia fo
                     inner join salve.ficha_ocorrencia_registro foreg on foreg.sq_ficha_ocorrencia = fo.sq_ficha_ocorrencia
                     inner join salve.registro_bioma reg on reg.sq_registro=foreg.sq_registro
                     inner join salve.bioma bioma on bioma.sq_bioma = reg.sq_bioma
            where fo.sq_ficha_ocorrencia = p_sq_ocorrencia

        )::jsonb;
    else
        return (

            select json_agg(json_build_object('sq_bioma',bioma.sq_bioma
                ,'no_bioma', bioma.no_bioma
                )) as js_bioma
            from salve.ficha_ocorrencia_portalbio fo
                     inner join salve.ficha_ocorrencia_portalbio_reg foreg on foreg.sq_ficha_ocorrencia_portalbio = fo.sq_ficha_ocorrencia_portalbio
                     inner join salve.registro_bioma reg on reg.sq_registro=foreg.sq_registro
                     inner join salve.bioma bioma on bioma.sq_bioma = reg.sq_bioma
            where fo.sq_ficha_ocorrencia_portalbio = p_sq_ocorrencia
        )::jsonb;
    end if;
end
$$;


--
-- TOC entry 7882 (class 0 OID 0)
-- Dependencies: 1802
-- Name: FUNCTION fn_ocorrencia_bioma_json(p_sq_ocorrencia bigint, p_portalbio boolean); Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON FUNCTION salve.fn_ocorrencia_bioma_json(p_sq_ocorrencia bigint, p_portalbio boolean) IS 'Função para gerar objeto json do Bioma de um registro de ocorrencia';


--
-- TOC entry 1804 (class 1255 OID 29463)
-- Name: fn_ocorrencia_estado_json(bigint, boolean); Type: FUNCTION; Schema: salve; Owner: -
--

CREATE FUNCTION salve.fn_ocorrencia_estado_json(p_sq_ocorrencia bigint, p_portalbio boolean DEFAULT false) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
    /*
     select * from salve.fn_ocorrencia_estado_json(289287)
     */
begin

    if not p_portalbio then
        return (

            select json_agg(json_build_object('sq_estado',uf.sq_estado
                ,'no_estado', uf.no_estado
                ,'sg_estado',uf.sg_estado
                ,'sq_regiao',uf.sq_regiao
                ,'no_regiao',regiao.no_regiao
                ,'co_ibge'  ,''
                )) as js_estado
            from salve.ficha_ocorrencia fo
                     inner join salve.ficha_ocorrencia_registro foreg on foreg.sq_ficha_ocorrencia = fo.sq_ficha_ocorrencia
                     inner join salve.registro_estado reg on reg.sq_registro=foreg.sq_registro
                     inner join salve.estado uf on uf.sq_estado = reg.sq_estado
                     left join salve.regiao regiao on regiao.sq_regiao =uf.sq_regiao
            where fo.sq_ficha_ocorrencia = p_sq_ocorrencia

        )::jsonb;
    else
        return (

            select json_agg(json_build_object('sq_estado',uf.sq_estado
                ,'no_estado', uf.no_estado
                ,'sg_estado',uf.sg_estado
                ,'sq_regiao',uf.sq_regiao
                ,'no_regiao',regiao.no_regiao
                ,'co_ibge'  ,''
                )) as js_estado
            from salve.ficha_ocorrencia_portalbio fo
                     inner join salve.ficha_ocorrencia_portalbio_reg foreg on foreg.sq_ficha_ocorrencia_portalbio = fo.sq_ficha_ocorrencia_portalbio
                     inner join salve.registro_estado reg on reg.sq_registro=foreg.sq_registro
                     inner join salve.estado uf on uf.sq_estado = reg.sq_estado
                     left join salve.regiao regiao on regiao.sq_regiao =uf.sq_regiao
            where fo.sq_ficha_ocorrencia_portalbio = p_sq_ocorrencia
        )::jsonb;
    end if;
end
$$;


--
-- TOC entry 7883 (class 0 OID 0)
-- Dependencies: 1804
-- Name: FUNCTION fn_ocorrencia_estado_json(p_sq_ocorrencia bigint, p_portalbio boolean); Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON FUNCTION salve.fn_ocorrencia_estado_json(p_sq_ocorrencia bigint, p_portalbio boolean) IS 'Função para gerar objeto json do Estado de um registro de ocorrencia';


--
-- TOC entry 1803 (class 1255 OID 29462)
-- Name: fn_ocorrencia_municipio_json(bigint, boolean); Type: FUNCTION; Schema: salve; Owner: -
--

CREATE FUNCTION salve.fn_ocorrencia_municipio_json(p_sq_ocorrencia bigint, p_portalbio boolean DEFAULT false) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
    /*
     select * from salve.fn_ocorrencia_municipio_json(289287)
     */
begin

    if not p_portalbio then
        return (

            select json_agg(json_build_object('sq_municipio',municipio.sq_municipio
                ,'no_municipio', municipio.no_municipio
                ,'co_ibge'  ,municipio.co_ibge
                )) as js_municipio
            from salve.ficha_ocorrencia fo
                     inner join salve.ficha_ocorrencia_registro foreg on foreg.sq_ficha_ocorrencia = fo.sq_ficha_ocorrencia
                     inner join salve.registro_municipio reg on reg.sq_registro=foreg.sq_registro
                     inner join salve.municipio municipio on municipio.sq_municipio = reg.sq_municipio
            where fo.sq_ficha_ocorrencia = p_sq_ocorrencia

        )::jsonb;
    else
        return (

            select json_agg(json_build_object('sq_municipio',municipio.sq_municipio
                ,'no_municipio', municipio.no_municipio
                ,'co_ibge'  ,municipio.co_ibge
                )) as js_municipio
            from salve.ficha_ocorrencia_portalbio fo
                     inner join salve.ficha_ocorrencia_portalbio_reg foreg on foreg.sq_ficha_ocorrencia_portalbio = fo.sq_ficha_ocorrencia_portalbio
                     inner join salve.registro_municipio reg on reg.sq_registro=foreg.sq_registro
                     inner join salve.municipio municipio on municipio.sq_municipio = reg.sq_municipio
            where fo.sq_ficha_ocorrencia_portalbio = p_sq_ocorrencia
        )::jsonb;
    end if;
end;
$$;


--
-- TOC entry 7884 (class 0 OID 0)
-- Dependencies: 1803
-- Name: FUNCTION fn_ocorrencia_municipio_json(p_sq_ocorrencia bigint, p_portalbio boolean); Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON FUNCTION salve.fn_ocorrencia_municipio_json(p_sq_ocorrencia bigint, p_portalbio boolean) IS 'Função para gerar objeto json do Municipio de um registro de ocorrencia';


--
-- TOC entry 1805 (class 1255 OID 29464)
-- Name: fn_ocorrencia_uc(bigint, boolean); Type: FUNCTION; Schema: salve; Owner: -
--

CREATE FUNCTION salve.fn_ocorrencia_uc(p_sq_ocorrencia bigint, p_portalbio boolean DEFAULT false) RETURNS TABLE(sq_uc bigint, in_esfera text, no_uc text, sg_uc text)
    LANGUAGE plpgsql
    AS $$
    /*
    select * from salve.fn_ocorrencia_uc(588907)
     */
begin

    if not p_portalbio then
        return query
            select ucf.sq_uc_federal::bigint as sq_uc
                 , 'F'::text as in_esfera
                 , instituicao.sg_instituicao::text as sg_uc
                 , pessoa.no_pessoa::text as no_uc
            from salve.registro_uc_federal ucf
                     inner join salve.ficha_ocorrencia_registro foreg on foreg.sq_registro = ucf.sq_registro
                     inner join salve.unidade_conservacao un on un.sq_pessoa = ucf.sq_uc_federal
                     inner join salve.instituicao on instituicao.sq_pessoa = ucf.sq_uc_federal
                     inner join salve.pessoa on pessoa.sq_pessoa = ucf.sq_uc_federal
            where foreg.sq_ficha_ocorrencia = p_sq_ocorrencia
            UNION
            select ucf.sq_uc_estadual::bigint as sq_uc
                 , 'E'::text as in_esfera
                 , instituicao.sg_instituicao::text as sg_uc
                 , pessoa.no_pessoa::text as no_uc
            from salve.registro_uc_estadual ucf
                     inner join salve.ficha_ocorrencia_registro foreg on foreg.sq_registro = ucf.sq_registro
                     inner join salve.unidade_conservacao un on un.sq_pessoa = ucf.sq_uc_estadual
                     inner join salve.instituicao on instituicao.sq_pessoa = ucf.sq_uc_estadual
                     inner join salve.pessoa on pessoa.sq_pessoa = ucf.sq_uc_estadual
            where foreg.sq_ficha_ocorrencia = p_sq_ocorrencia
            UNION
            select ucf.sq_rppn::bigint as sq_uc
                 , 'R'::text as in_esfera
                 , instituicao.sg_instituicao::text as sg_uc
                 , pessoa.no_pessoa::text as no_uc
            from salve.registro_rppn ucf
                     inner join salve.ficha_ocorrencia_registro foreg on foreg.sq_registro = ucf.sq_registro
                     inner join salve.unidade_conservacao un on un.sq_pessoa = ucf.sq_rppn
                     inner join salve.instituicao on instituicao.sq_pessoa = ucf.sq_rppn
                     inner join salve.pessoa on pessoa.sq_pessoa = ucf.sq_rppn
            where foreg.sq_ficha_ocorrencia = p_sq_ocorrencia;

    else
        return query
            select ucf.sq_uc_federal::bigint
                 , 'F'::text as in_esfera
                 , instituicao.sg_instituicao::text as sg_uc
                 , pessoa.no_pessoa::text as no_uc
            from salve.registro_uc_federal ucf
                     inner join salve.ficha_ocorrencia_portalbio_reg foreg on foreg.sq_registro = ucf.sq_registro
                     inner join salve.unidade_conservacao un on un.sq_pessoa = ucf.sq_uc_federal
                     inner join salve.instituicao on instituicao.sq_pessoa = ucf.sq_uc_federal
                     inner join salve.pessoa on pessoa.sq_pessoa = ucf.sq_uc_federal
            where foreg.sq_ficha_ocorrencia_portalbio = p_sq_ocorrencia

            UNION

            select ucf.sq_uc_estadual::bigint
                 , 'E'::text as in_esfera
                 , instituicao.sg_instituicao::text as sg_uc
                 , pessoa.no_pessoa::text as no_uc
            from salve.registro_uc_estadual ucf
                     inner join salve.ficha_ocorrencia_portalbio_reg foreg on foreg.sq_registro = ucf.sq_registro
                     inner join salve.unidade_conservacao un on un.sq_pessoa = ucf.sq_uc_estadual
                     inner join salve.instituicao on instituicao.sq_pessoa = ucf.sq_uc_estadual
                     inner join salve.pessoa on pessoa.sq_pessoa = ucf.sq_uc_estadual
            where foreg.sq_ficha_ocorrencia_portalbio = p_sq_ocorrencia

            UNION

            select ucf.sq_rppn::bigint
                 , 'R'::text as in_esfera
                 , instituicao.sg_instituicao::text as sg_uc
                 , pessoa.no_pessoa::text as no_uc
            from salve.registro_rppn ucf
                     inner join salve.ficha_ocorrencia_portalbio_reg foreg on foreg.sq_registro = ucf.sq_registro
                     inner join salve.unidade_conservacao un on un.sq_pessoa = ucf.sq_rppn
                     inner join salve.instituicao on instituicao.sq_pessoa = ucf.sq_rppn
                     inner join salve.pessoa on pessoa.sq_pessoa = ucf.sq_rppn
            where foreg.sq_ficha_ocorrencia_portalbio = p_sq_ocorrencia;

    end if;
end
$$;


--
-- TOC entry 7885 (class 0 OID 0)
-- Dependencies: 1805
-- Name: FUNCTION fn_ocorrencia_uc(p_sq_ocorrencia bigint, p_portalbio boolean); Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON FUNCTION salve.fn_ocorrencia_uc(p_sq_ocorrencia bigint, p_portalbio boolean) IS 'Função para listar as unidades de conservacao de um registro de ocorrencia';


--
-- TOC entry 1806 (class 1255 OID 29465)
-- Name: fn_ocorrencia_uc_json(bigint, boolean); Type: FUNCTION; Schema: salve; Owner: -
--

CREATE FUNCTION salve.fn_ocorrencia_uc_json(p_sq_ocorrencia bigint, p_portalbio boolean DEFAULT false) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
    /*
        select * from salve.fn_ocorrencia_uc_json(588907)
     */
begin
    return (
        select json_agg(json_build_object(
                'sq_uc',uc.sq_uc
            ,'in_esfera',uc.in_esfera
            ,'no_uc',uc.no_uc
            )) as js_uc
        from salve.fn_ocorrencia_uc(p_sq_ocorrencia, p_portalbio) as uc
    )::jsonb;
end;
$$;


--
-- TOC entry 7886 (class 0 OID 0)
-- Dependencies: 1806
-- Name: FUNCTION fn_ocorrencia_uc_json(p_sq_ocorrencia bigint, p_portalbio boolean); Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON FUNCTION salve.fn_ocorrencia_uc_json(p_sq_ocorrencia bigint, p_portalbio boolean) IS 'Função para gerar objeto json das unidades de conservacao e rppn de um registro de ocorrencia';


--
-- TOC entry 1807 (class 1255 OID 29466)
-- Name: fn_ocorrencias2json(bigint); Type: FUNCTION; Schema: salve; Owner: -
--

CREATE FUNCTION salve.fn_ocorrencias2json(p_sq_ficha bigint) RETURNS json
    LANGUAGE plpgsql
    AS $$begin
    return  (
        select json_build_object(
                       'sq_ficha', ficha.sq_ficha
                   ,'ocorrencias_salve', (
                           select json_agg(
                                          json_build_object(
                                                  'sq_ficha_ocorrencia', fo.sq_ficha_ocorrencia
                                              , 'in_utilizado_avaliacao', fo.in_utilizado_avaliacao
                                              , 'sq_motivo_nao_utilizado_avaliacao', fo.sq_motivo_nao_utilizado_avaliacao
                                              , 'ds_motivo_nao_utilizado_avaliacao', motivo_nao_utilizado.ds_dados_apoio
                                              , 'tx_nao_utilizado_avaliacao', case when fo.sq_motivo_nao_utilizado_avaliacao is null then null::text else salve.entity2char(fo.tx_nao_utilizado_avaliacao) end
                                              , 'dt_invalidado', fo.dt_invalidado
                                              , 'tx_nao_aceita', salve.entity2char(fo.tx_nao_aceita )
                                              , 'dt_alteracao',fo.dt_alteracao
                                              , 'sq_pessoa_alteracao',fo.sq_pessoa_alteracao
                                              , 'no_pessoa_alteracao', pf_alteracao.no_pessoa
                                              , 'dt_inclusao',fo.dt_inclusao
                                              , 'sq_pessoa_inclusao',fo.sq_pessoa_inclusao
                                              , 'no_pessoa_inclusao', pf_inclusao.no_pessoa
                                              , 'sq_situacao',fo.sq_situacao
                                              , 'ds_situacao',situacao.ds_dados_apoio
                                              , 'cd_situacao_sistema',situacao.cd_sistema
                                              , 'in_data_desconhecida',fo.in_data_desconhecida
                                              , 'sq_contexto',fo.sq_contexto
                                              , 'de_contexto', contexto.ds_dados_apoio
                                              , 'cd_contexto_sistema',contexto.cd_sistema
                                              -- aba 2.6.1
                                              ,'georeferenciamento', json_build_object('sq_tipo_registro'     ,fo.sq_tipo_registro
                                                      ,'ds_tipo_registro'     ,tipo_registro.ds_dados_apoio
                                                      ,'nu_dia_registro'      ,fo.nu_dia_registro
                                                      ,'nu_mes_registro'      ,fo.nu_mes_registro
                                                      ,'nu_ano_registro'      ,fo.nu_ano_registro
                                                      ,'nu_dia_registro_fim'  ,fo.nu_dia_registro_fim
                                                      ,'nu_mes_registro_fim'  ,fo.nu_mes_registro_fim
                                                      ,'nu_ano_registro_fim'  ,fo.nu_ano_registro_fim
                                                      ,'dt_registro'          ,fo.dt_registro
                                                      ,'hr_registro'          ,fo.hr_registro
                                                      ,'in_presenca_atual'    ,fo.in_presenca_atual
                                                      ,'in_sensivel'          ,fo.in_sensivel
                                                      ,'sq_prazo_carencia'    ,fo.sq_prazo_carencia
                                                      ,'ds_prazo_carencia'    ,carencia.ds_dados_apoio
                                                      ,'dt_carencia'          ,salve.calc_carencia(fo.dt_inclusao::date,carencia.cd_sistema)
                                                      ,'sq_datum'             ,fo.sq_datum
                                                      ,'ds_datum'             ,datum.ds_dados_apoio
                                                      ,'nu_lat'               , st_y(fo.ge_ocorrencia)
                                                      ,'nu_lon'               , st_x(fo.ge_ocorrencia)
                                                      ,'ge_ocorrencia'        , fo.ge_ocorrencia
                                                      ,'sq_formato_coord_original', fo.sq_formato_coord_original
                                                      ,'ds_formato_coord_original', formato_original.ds_dados_apoio
                                                      ,'sq_precisao_coordenada'   , fo.sq_precisao_coordenada
                                                      ,'ds_precisao_coordenada'   , precisao_coord.ds_dados_apoio
                                                      ,'sq_ref_aproximacao'       , fo.sq_ref_aproximacao
                                                      ,'ds_ref_aproximacao'       , ref_aproximacao.ds_dados_apoio
                                                      ,'sq_metodo_aproximacao', fo.sq_metodo_aproximacao
                                                      ,'ds_metodo_aproximacao', met_aproximacao.ds_dados_apoio
                                                      ,'tx_metodo_aproximacao', salve.entity2char(fo.tx_metodo_aproximacao )
                                                      ,'sq_situacao_avaliacao', fo.sq_situacao_avaliacao
                                                      ,'ds_situacao_avaliacao', situacao_avaliacao.ds_dados_apoio
                                                      ,'cd_situacao_avaliacao_sistema', situacao_avaliacao.cd_sistema
                                                      ,'id_origem', fo.id_origem
                                                      ,'tx_observacao',salve.entity2char(fo.tx_observacao )
                                                      ,'sq_estado', fo.sq_estado
                                                      ,'sq_municipio', fo.sq_municipio
                                                      ,'no_localidade', fo.no_localidade
                                                      ,'estado',salve.fn_ocorrencia_estado_json(fo.sq_ficha_ocorrencia)
                                                      ,'municipio',salve.fn_ocorrencia_municipio_json(fo.sq_ficha_ocorrencia)
                                                      ,'bioma',salve.fn_ocorrencia_bioma_json(fo.sq_ficha_ocorrencia)
                                                      ,'bacias',salve.fn_ocorrencia_bacias_json(fo.sq_ficha_ocorrencia)
                                                      ,'uc',salve.fn_ocorrencia_uc_json(fo.sq_ficha_ocorrencia)
                                                      ) -- fim aba 2.6.1
                                          -- aba 2.6.2
                                              ,'dados_registro',json_build_object(
                                                          'sq_taxon_citado', fo.sq_taxon_citado
                                                      ,'json_trilha_taxon_citado',taxon_citado.json_trilha
                                                      )

                                              -- aba 2.6.3
                                              ,'localidade',json_build_object('de_local', fo.tx_local
                                                      ,'vl_elevacao_min', fo.vl_elevacao_min
                                                      ,'vl_elevacao_max', fo.vl_elevacao_max
                                                      ,'vl_profundidade_min', fo.vl_profundidade_min
                                                      ,'vl_profundidade_max', fo.vl_profundidade_max
                                                      ,'nu_altitude',fo.nu_altitude
                                                      ,'sq_pais', fo.sq_pais
                                                      ,'no_pais', pais.no_pais
                                                      ,'sq_estado', fo.sq_estado
                                                      ,'no_estado', estado.no_estado
                                                      ,'sg_estado', estado.sg_estado
                                                      ,'sq_municipio', fo.sq_municipio
                                                      ,'no_municipio', municipio.no_municipio
                                                      ,'sq_habitat', fo.sq_habitat
                                                      ,'no_habitat', habitat.ds_dados_apoio

                                                      ) -- fim aba 2.6.3
                                          -- aba 2.6.4
                                              ,'qualificador',json_build_object( 'de_identificador'         , fo.de_identificador
                                                      ,'dt_identificacao'         , fo.dt_identificacao
                                                      ,'sq_qualificador_val_taxon', fo.sq_qualificador_val_taxon
                                                      ,'ds_qualificador_val_taxon', qualificador.ds_dados_apoio
                                                      ,'tx_tombamento'            , fo.tx_tombamento
                                                      ,'tx_instituicao'           , fo.tx_instituicao
                                                      ) -- fim aba 2.6.4

                                          -- aba 2.6.5
                                              ,'responsaveis',json_build_object('sq_pessoa_compilador', fo.sq_pessoa_compilador
                                                      , 'dt_compilacao', fo.dt_compilacao
                                                      , 'sq_pessoa_revisor', fo.sq_pessoa_revisor
                                                      , 'dt_revisao', fo.dt_revisao
                                                      , 'sq_pessoa_validador', fo.sq_pessoa_validador
                                                      , 'no_pessoa_validador', pf_validador.no_pessoa
                                                      , 'dt_validacao', fo.dt_validacao
                                                      ) -- fim aba 2.6.5
                                              ,'referencias',( salve.fn_ref_bib_json(fo.sq_ficha,fo.sq_ficha_ocorrencia,'sq_ficha_ocorrencia'))
                                              ,'imagens',(
                                                      select json_agg( json_build_object('sq_ficha_ocorrencia_multimidia', fom.sq_ficha_ocorrencia_multimidia,
                                                                                         'sq_ficha_multimidia', fom.sq_ficha_multimidia) )
                                                      from salve.ficha_ocorrencia_multimidia fom
                                                      where fom.sq_ficha_ocorrencia = fo.sq_ficha_ocorrencia
                                                  )

                                              ) )
                           from salve.ficha_ocorrencia fo
                                    left outer join salve.dados_apoio as carencia on carencia.sq_dados_apoio = fo.sq_prazo_carencia
                                    left outer join salve.dados_apoio as motivo_nao_utilizado on motivo_nao_utilizado.sq_dados_apoio = fo.sq_motivo_nao_utilizado_avaliacao
                                    left outer join corporativo.vw_pessoa_fisica as pf_alteracao on pf_alteracao.sq_pessoa = fo.sq_pessoa_alteracao
                                    left outer join corporativo.vw_pessoa_fisica as pf_inclusao on pf_inclusao.sq_pessoa = fo.sq_pessoa_inclusao
                                    left outer join corporativo.vw_pessoa_fisica as pf_validador on pf_validador.sq_pessoa = fo.sq_pessoa_validador
                                    left outer join salve.dados_apoio as contexto on contexto.sq_dados_apoio = fo.sq_contexto
                                    left outer join salve.dados_apoio as situacao on situacao.sq_dados_apoio = fo.sq_situacao
                                    left outer join salve.dados_apoio as situacao_avaliacao on situacao_avaliacao.sq_dados_apoio = fo.sq_situacao_avaliacao
                                    left outer join salve.dados_apoio as formato_original on formato_original.sq_dados_apoio = fo.sq_formato_coord_original
                                    left outer join salve.dados_apoio as ref_aproximacao on ref_aproximacao.sq_dados_apoio = fo.sq_ref_aproximacao
                                    left outer join salve.dados_apoio as met_aproximacao on met_aproximacao.sq_dados_apoio = fo.sq_metodo_aproximacao
                                    left outer join salve.dados_apoio as precisao_coord on precisao_coord.sq_dados_apoio = fo.sq_precisao_coordenada
                                    left outer join taxonomia.taxon   as taxon_citado on taxon_citado.sq_taxon = fo.sq_taxon_citado
                                    left outer join corporativo.vw_pais as pais on pais.sq_pais = fo.sq_pais
                                    left outer join corporativo.vw_estado as estado on estado.sq_estado = fo.sq_estado
                                    left outer join corporativo.vw_municipio as municipio on municipio.sq_municipio = fo.sq_municipio
                                    left outer join salve.dados_apoio as habitat on habitat.sq_dados_apoio = fo.sq_habitat
                                    left outer join salve.dados_apoio as qualificador on qualificador.sq_dados_apoio = fo.sq_qualificador_val_taxon
                                    left outer join salve.dados_apoio as datum on datum.sq_dados_apoio = fo.sq_datum
                                    left outer join salve.dados_apoio as tipo_registro on tipo_registro.sq_dados_apoio = fo.sq_tipo_registro


                           where fo.sq_ficha = ficha.sq_ficha
                       )

                   ,'ocorrencias_portalbio',(
                           select json_agg(
                                          json_build_object(
                                                  'sq_ficha_ocorrencia_portalbio', fop.sq_ficha_ocorrencia_portalbio
                                              , 'no_base_dados',coalesce(fop.no_base_dados,salve.calc_base_dados_portalbio( fop.tx_ocorrencia, fop.de_uuid ))
                                              , 'de_uuid', fop.de_uuid
                                              , 'no_cientifico', fop.no_cientifico
                                              , 'no_local', fop.no_local
                                              , 'no_instituicao', fop.no_instituicao
                                              , 'no_autor', fop.no_autor
                                              , 'dt_ocorrencia', fop.dt_ocorrencia
                                              , 'dt_carencia', fop.dt_carencia
                                              , 'de_ameaca', fop.de_ameaca
                                              , 'nu_lat', st_y(fop.ge_ocorrencia)
                                              , 'nu_lon', st_x(fop.ge_ocorrencia)
                                              , 'ge_ocorrencia', fop.ge_ocorrencia
                                              , 'sq_situacao', fop.sq_situacao
                                              , 'ds_situacao', situacao.ds_dados_apoio
                                              , 'cd_situacao_sistema', situacao.cd_sistema
                                              , 'sq_pessoa_validador', fop.sq_pessoa_validador
                                              , 'no_pessoa_validador', pf_validador.no_pessoa
                                              , 'dt_validacao', fop.dt_validacao
                                              , 'sq_pessoa_revisor', fop.sq_pessoa_revisor
                                              , 'no_pessoa_revisor', pf_revisor.no_pessoa
                                              , 'dt_revisao', fop.dt_revisao
                                              , 'in_utilizado_avaliacao', fop.in_utilizado_avaliacao
                                              , 'tx_nao_utilizado_avaliacao', case when fop.sq_motivo_nao_utilizado_avaliacao is null then null::text else salve.entity2char(fop.tx_nao_utilizado_avaliacao) end
                                              , 'tx_observacao', fop.tx_observacao
                                              , 'id_origem', fop.id_origem
                                              , 'dt_alteracao', fop.dt_alteracao
                                              , 'sq_pais', fop.sq_pais
                                              , 'sq_estado', fop.sq_estado
                                              , 'sq_municipio', fop.sq_municipio
                                              , 'sq_bioma', fop.sq_bioma
                                              , 'sq_uc_federal', fop.sq_uc_federal
                                              , 'sq_uc_estadual', fop.sq_uc_estadual
                                              , 'sq_rppn', fop.sq_rppn
                                              , 'in_processamento', fop.in_processamento
                                              , 'sq_bacia_hidrografica', fop.sq_bacia_hidrografica
                                              , 'sq_motivo_nao_utilizado_avaliacao', fop.sq_motivo_nao_utilizado_avaliacao
                                              , 'ds_motivo_nao_utilizado_avaliacao', motivo_nao_utilizado.ds_dados_apoio
                                              , 'in_data_desconhecida', fop.in_data_desconhecida
                                              , 'tx_nao_aceita', fop.tx_nao_aceita
                                              , 'st_adicionado_apos_avaliacao', fop.st_adicionado_apos_avaliacao
                                              , 'sq_situacao_avaliacao', fop.sq_situacao_avaliacao
                                              , 'ds_situacao_avaliacao', situacao_avaliacao.ds_dados_apoio
                                              , 'cd_situacao_avaliacao_sistema', situacao_avaliacao.cd_sistema
                                              , 'in_presenca_atual', fop.in_presenca_atual)
                                      )
                           from salve.ficha_ocorrencia_portalbio as fop
                                    left outer join salve.dados_apoio as situacao on situacao.sq_dados_apoio = fop.sq_situacao
                                    left outer join salve.dados_apoio as situacao_avaliacao on situacao_avaliacao.sq_dados_apoio = fop.sq_situacao_avaliacao
                                    left outer join corporativo.vw_pessoa_fisica as pf_validador on pf_validador.sq_pessoa = fop.sq_pessoa_validador
                                    left outer join corporativo.vw_pessoa_fisica as pf_revisor on pf_revisor.sq_pessoa = fop.sq_pessoa_validador
                                    left outer join salve.dados_apoio as motivo_nao_utilizado on motivo_nao_utilizado.sq_dados_apoio = fop.sq_motivo_nao_utilizado_avaliacao
                           where fop.sq_ficha = ficha.sq_ficha )
                   )::jsonb
        from salve.ficha
        where ficha.sq_ficha = p_sq_ficha )::json;
end;
$$;


--
-- TOC entry 1809 (class 1255 OID 29609)
-- Name: fn_ref_bib_json(bigint, bigint, text); Type: FUNCTION; Schema: salve; Owner: -
--

CREATE FUNCTION salve.fn_ref_bib_json(p_sq_ficha bigint, p_sq_registro bigint DEFAULT NULL::bigint, p_no_coluna text DEFAULT NULL::text) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
begin

    return ( select json_agg(json_build_object(
            'sq_publicacao', refs.sq_publicacao,
            'de_titulo',publicacao.de_titulo,
            'de_ref_bibliografica',publicacao.de_ref_bibliografica,
            'no_autor', coalesce( publicacao.no_autor, refs.no_autor),
            'nu_ano'  , coalesce( publicacao.nu_ano_publicacao, refs.nu_ano),
            'tags', ( select json_agg(json_build_object(
                    'sq_tag',ref_tag.sq_tag,
                    'ds_tag',tag.ds_dados_apoio))
                      from salve.ficha_ref_bib_tag as ref_tag
                               inner join salve.dados_apoio as tag on tag.sq_dados_apoio = ref_tag.sq_tag
                      where ref_tag.sq_ficha_ref_bib = refs.sq_ficha_ref_bib)
        )
                        )
             from salve.ficha_ref_bib as refs
                      left outer join taxonomia.publicacao on publicacao.sq_publicacao = refs.sq_publicacao
             where refs.sq_ficha = p_sq_ficha
               and (refs.sq_registro = p_sq_registro or p_sq_registro is null)
               and (refs.no_coluna = p_no_coluna or p_no_coluna is null)
    )::jsonb;


end;
$$;


--
-- TOC entry 7887 (class 0 OID 0)
-- Dependencies: 1809
-- Name: FUNCTION fn_ref_bib_json(p_sq_ficha bigint, p_sq_registro bigint, p_no_coluna text); Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON FUNCTION salve.fn_ref_bib_json(p_sq_ficha bigint, p_sq_registro bigint, p_no_coluna text) IS 'Função para gerar objeto json das referencias bibliograficas';


--
-- TOC entry 1787 (class 1255 OID 28997)
-- Name: fn_taxon_bacias(bigint); Type: FUNCTION; Schema: salve; Owner: -
--

CREATE FUNCTION salve.fn_taxon_bacias(p_sq_taxon bigint) RETURNS TABLE(no_contexto text, no_origem text, no_bacia text, cd_bacia text, cd_sistema text, sq_bacia bigint, tx_trilha text, de_bacia_ordem text, st_utilizado_avalicao boolean, st_adicionado_apos_avaliacao boolean, sq_ficha_bacia bigint, sq_ficha_ref_bib bigint, sq_publicacao bigint, de_ref_bib text, de_titulo text, no_autor text, nu_ano integer, co_nivel_taxonomico text, dt_carencia date, st_em_carencia boolean)
    LANGUAGE plpgsql
    AS $$
begin
    return query
        with passo1 as (select ficha.sq_taxon
                             , ficha.sq_ficha
                             , ficha.sq_ciclo_avaliacao
                             , ficha.sq_grupo
                             , case
                                   when ficha.sq_grupo is not null and (grupo.cd_sistema like '%MARINHO%'
                                       OR  grupo.cd_sistema in
                                           ('CEFALOPODAS', 'EQUINODERMAS', 'BRAQUIOPODAS', 'CNIDARIAS', 'ENTEROPNEUSTAS',
                                            'POLIQUETAS', 'SIPUNCULAS')
                                       )
                                       THEN true
                                   else false end as st_marinho
                        from salve.ficha
                                 left outer join salve.dados_apoio as grupo on grupo.sq_dados_apoio = ficha.sq_grupo
                        where ficha.sq_taxon = p_sq_taxon
        ),
             passo2 as (select ficha.sq_ficha,
                               passo1.sq_ciclo_avaliacao,
                               taxon.sq_taxon,
                               nivel.co_nivel_taxonomico,
                               passo1.sq_grupo,
                               passo1.st_marinho
                        from passo1
                                 inner join taxonomia.taxon
                                            on (taxon.sq_taxon = passo1.sq_taxon or taxon.sq_taxon_pai = passo1.sq_taxon)
                                 inner join taxonomia.nivel_taxonomico as nivel
                                            on nivel.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
                                 inner join salve.ficha on ficha.sq_taxon = taxon.sq_taxon
                            and ficha.sq_ciclo_avaliacao = passo1.sq_ciclo_avaliacao
                             -- bioma marinho não possui pontos em Estados
                        where passo1.st_marinho = false
             )
                ,
             bacias_ficha as (select 'ficha'::text                                              as no_contexto,
                                     'salve'::text                                              as no_origem,
                                     bacia.descricao::text                                      as no_bacia,
                                     bacia.codigo::text                                         as cd_bacia,
                                     bacia.codigo_sistema::text                                 as cd_sistema,
                                     bacia.id::bigint                                           as sq_bacia,
                                     bacia.trilha::text                                         as tx_trilha,
                                     bacia.ordem::text                                          as de_bacia_ordem,

                                     -- situacao avaliacao
                                     true::boolean                                               as st_utilizado_avalicao,
                                     false::boolean                                              as st_adicionado_apos_avaliacao,

                                     -- ids
                                     fb.sq_ficha_bacia::bigint                                  as sq_ficha_bacia

                                     -- ref bib
                                      , frb.sq_ficha_ref_bib::bigint                             as sq_ficha_ref_bib
                                      , publicacao.sq_publicacao::bigint                         as sq_publicacao
                                      , publicacao.de_ref_bibliografica::text                    as de_ref_bib
                                      , publicacao.de_titulo::text                               as de_titulo
                                      , coalesce(publicacao.no_autor, frb.no_autor)::text        as no_autor
                                      , coalesce(publicacao.nu_ano_publicacao, frb.nu_ano)::integer as nu_ano
                                     -- complemento
                                      , passo2.co_nivel_taxonomico::text                         as co_nivel_taxonomico
                                      , null::date as dt_carencia

                              from salve.ficha_bacia fb
                                       inner join passo2 on passo2.sq_ficha = fb.sq_ficha
                                       inner join salve.vw_bacia_hidrografica bacia on bacia.id = fb.sq_bacia
                                       left outer join salve.ficha_ref_bib frb on frb.sq_ficha = fb.sq_ficha
                                  and frb.no_coluna = 'sq_ficha_bacia' and frb.no_tabela = 'ficha_bacia'
                                  and frb.sq_registro = fb.sq_ficha_bacia
                                       left outer join taxonomia.publicacao on publicacao.sq_publicacao = frb.sq_publicacao

             ), bacias_ocorrencias_salve as (
            select 'ocorrencia'::text                                as no_contexto,
                   'salve'::text                                             as no_origem,
                   bacia.descricao::text                             as no_bacia,
                   bacia.codigo::text                                as cd_bacia,
                   bacia.codigo_sistema::text                        as cd_sistema,
                   bacia.id::bigint                                  as sq_bacia,
                   bacia.trilha::text                                as tx_trilha,
                   bacia.ordem::text                                  as de_bacia_ordem,
                   -- situacao avaliacao
                   (situacao.cd_sistema = 'REGISTRO_UTILIZADO_AVALIACAO')::boolean                  as st_utilizado_avalicao,
                   (situacao.cd_sistema = 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')::boolean as st_adicionado_apos_avaliacao,

                   -- ids
                   null::bigint                                  as sq_ficha_bacia,

                   -- ref bib
                   frb.sq_ficha_ref_bib::bigint                             as sq_ficha_ref_bib,
                   publicacao.sq_publicacao::bigint                         as sq_publicacao,
                   publicacao.de_ref_bibliografica::text                    as de_ref_bib,
                   publicacao.de_titulo::text                               as de_titulo,
                   coalesce(publicacao.no_autor, frb.no_autor)::text        as no_autor,
                   coalesce(publicacao.nu_ano_publicacao, frb.nu_ano)::integer as nu_ano,

                   -- complemento
                   passo2.co_nivel_taxonomico::text                         as co_nivel_taxonomico,
                   salve.calc_carencia(fo.dt_inclusao::date, carencia.cd_sistema)::date as dt_carencia

            from salve.ficha_ocorrencia_registro a
                     inner join salve.registro_bacia b on b.sq_registro = a.sq_registro
                     inner join salve.dados_apoio_geo bacia_geo on bacia_geo.sq_dados_apoio_geo = b.sq_bacia_geo
                     inner join salve.vw_bacia_hidrografica as bacia on bacia.id = bacia_geo.sq_dados_apoio
                     inner join salve.ficha_ocorrencia fo on fo.sq_ficha_ocorrencia = a.sq_ficha_ocorrencia
                     inner join passo2 on passo2.sq_ficha = fo.sq_ficha
                     inner join salve.dados_apoio as situacao on situacao.sq_dados_apoio = fo.sq_situacao_avaliacao
                     left join salve.dados_apoio as carencia on carencia.sq_dados_apoio = fo.sq_prazo_carencia
                     left join salve.dados_apoio as ref_aprox on ref_aprox.sq_dados_apoio = fo.sq_ref_aproximacao
                     left join salve.dados_apoio as met_aprox on met_aprox.sq_dados_apoio = fo.sq_metodo_aproximacao
                     left join salve.ficha_ref_bib as frb on frb.sq_ficha = fo.sq_ficha

                and frb.no_tabela = 'ficha_ocorrencia'
                and frb.no_coluna = 'sq_ficha_ocorrencia'
                and frb.sq_registro = fo.sq_ficha_ocorrencia
                     left join taxonomia.publicacao on publicacao.sq_publicacao = frb.sq_publicacao
            where  (fo.in_presenca_atual is null or fo.in_presenca_atual <> 'N')
              and situacao.cd_sistema in ('REGISTRO_UTILIZADO_AVALIACAO', 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')
              -- não considerar registros que forem centroide do Estado
              and (fo.sq_metodo_aproximacao IS NULL OR met_aprox.cd_sistema <> 'CENTROIDE' OR ref_aprox.cd_sistema <> 'ESTADO')

        ), bacias_ocorrencias_portalbio as (
            select 'ocorrencia'::text                                as no_contexto,
                   'salve'::text                                             as no_origem,
                   bacia.descricao::text                             as no_bacia,
                   bacia.codigo::text                                as cd_bacia,
                   bacia.codigo_sistema::text                        as cd_sistema,
                   bacia.id::bigint                                  as sq_bacia,
                   bacia.trilha::text                                as tx_trilha,
                   bacia.ordem::text                                  as de_bacia_ordem,
                   -- situacao avaliacao
                   (situacao.cd_sistema = 'REGISTRO_UTILIZADO_AVALIACAO')::boolean                  as st_utilizado_avalicao,
                   (situacao.cd_sistema = 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')::boolean as st_adicionado_apos_avaliacao,

                   -- ids
                   null::bigint                                  as sq_ficha_bacia,

                   -- ref bib
                   frb.sq_ficha_ref_bib::bigint                             as sq_ficha_ref_bib,
                   publicacao.sq_publicacao::bigint                         as sq_publicacao,
                   publicacao.de_ref_bibliografica::text                    as de_ref_bib,
                   publicacao.de_titulo::text                               as de_titulo,
                   coalesce(publicacao.no_autor, frb.no_autor)::text        as no_autor,
                   coalesce(publicacao.nu_ano_publicacao, frb.nu_ano)::integer as nu_ano,

                   -- complemento
                   passo2.co_nivel_taxonomico::text                         as co_nivel_taxonomico,
                   fo.dt_carencia::date as dt_carencia
            from salve.ficha_ocorrencia_portalbio_reg a
                     inner join salve.registro_bacia b on b.sq_registro = a.sq_registro
                     inner join salve.dados_apoio_geo bacia_geo on bacia_geo.sq_dados_apoio_geo = b.sq_bacia_geo
                     inner join salve.vw_bacia_hidrografica as bacia on bacia.id = bacia_geo.sq_dados_apoio
                     inner join salve.ficha_ocorrencia_portalbio fo on fo.sq_ficha_ocorrencia_portalbio = a.sq_ficha_ocorrencia_portalbio
                     inner join passo2 on passo2.sq_ficha = fo.sq_ficha
                     inner join salve.dados_apoio as situacao on situacao.sq_dados_apoio = fo.sq_situacao_avaliacao
                     left join salve.ficha_ref_bib as frb on frb.sq_ficha = fo.sq_ficha
                and frb.no_tabela = 'ficha_ocorrencia_portalbio'
                and frb.no_coluna = 'sq_ficha_ocorrencia_portalbio'
                and frb.sq_registro = fo.sq_ficha_ocorrencia_portalbio
                     left join taxonomia.publicacao on publicacao.sq_publicacao = frb.sq_publicacao
            where  (fo.in_presenca_atual is null or fo.in_presenca_atual <> 'N')
              and situacao.cd_sistema in ('REGISTRO_UTILIZADO_AVALIACAO', 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')
        )
        select a.*, false as st_em_carencia from bacias_ficha as a
        union
        select b.*, case when b.dt_carencia is null or b.dt_carencia < now() then false else true end as st_em_carencia from bacias_ocorrencias_salve as b
        union
        select c.*, case when c.dt_carencia is null or c.dt_carencia < now() then false else true end as st_em_carencia from bacias_ocorrencias_portalbio as c;
end;
$$;


--
-- TOC entry 1786 (class 1255 OID 28988)
-- Name: fn_taxon_biomas(bigint); Type: FUNCTION; Schema: salve; Owner: -
--

CREATE FUNCTION salve.fn_taxon_biomas(p_sq_taxon bigint) RETURNS TABLE(no_bioma text, no_contexto text, no_origem text, st_utilizado_avalicao boolean, st_adicionado_apos_avaliacao boolean, sq_ficha_bioma bigint, sq_bioma_salve bigint, sq_bioma_corp bigint, sq_ficha_ref_bib bigint, sq_publicacao bigint, de_ref_bib text, de_titulo text, no_autor text, nu_ano integer, co_nivel_taxonomico text, dt_carencia date, st_em_carencia boolean)
    LANGUAGE plpgsql
    AS $$
begin
    return query
        with passo1 as (select ficha.sq_taxon
                             , ficha.sq_ficha
                             , ficha.sq_ciclo_avaliacao
                             , ficha.sq_grupo
                             , case
                                   when ficha.sq_grupo is not null and (grupo.cd_sistema like '%MARINHO%'
                                       OR  grupo.cd_sistema in
                                           ('CEFALOPODAS', 'EQUINODERMAS', 'BRAQUIOPODAS', 'CNIDARIAS', 'ENTEROPNEUSTAS',
                                            'POLIQUETAS', 'SIPUNCULAS')
                                       )
                                       THEN true
                                   else false end as st_marinho
                        from salve.ficha
                                 left outer join salve.dados_apoio as grupo on grupo.sq_dados_apoio = ficha.sq_grupo
                        where ficha.sq_taxon = p_sq_taxon
        ),
             passo2 as (select ficha.sq_ficha,
                               passo1.sq_ciclo_avaliacao,
                               taxon.sq_taxon,
                               nivel.co_nivel_taxonomico,
                               passo1.sq_grupo,
                               passo1.st_marinho
                        from passo1
                                 inner join taxonomia.taxon
                                            on (taxon.sq_taxon = passo1.sq_taxon or taxon.sq_taxon_pai = passo1.sq_taxon)
                                 inner join taxonomia.nivel_taxonomico as nivel
                                            on nivel.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
                                 inner join salve.ficha on ficha.sq_taxon = taxon.sq_taxon
                            and ficha.sq_ciclo_avaliacao = passo1.sq_ciclo_avaliacao
             ),
             biomas_ficha as (select bioma_salve.ds_dados_apoio::text                              as no_bioma
                                   , 'ficha'::text                                            as no_contexto
                                   , 'salve'::text                                            as no_origem
                                   -- situacao avaliacao
                                   , true::boolean                                               as st_utilizado_avalicao
                                   , false::boolean                                              as st_adicionado_apos_avaliacao
                                   -- ids
                                   , fb.sq_ficha_bioma::bigint                                  as sq_ficha_bioma
                                   , bioma_salve.sq_dados_apoio::bigint                         as sq_bioma_salve
                                   , bioma_corp.sq_bioma::bigint                               as sq_bioma_corp
                                   -- ref bib
                                   , frb.sq_ficha_ref_bib::bigint                               as sq_ficha_ref_bib
                                   , publicacao.sq_publicacao::bigint                           as sq_publicacao
                                   , publicacao.de_ref_bibliografica::text                   as de_ref_bib
                                   , publicacao.de_titulo::text                               as de_titulo
                                   , coalesce(publicacao.no_autor, frb.no_autor)::text        as no_autor
                                   , coalesce(publicacao.nu_ano_publicacao, frb.nu_ano)::integer as nu_ano
                                   -- complemento
                                   , passo2.co_nivel_taxonomico::text                         as co_nivel_taxonomico
                                   , null::date as dt_carencia
                              from salve.ficha_bioma fb
                                       inner join passo2 on passo2.sq_ficha = fb.sq_ficha
                                       inner join salve.dados_apoio as bioma_salve on bioma_salve.sq_dados_apoio = fb.sq_bioma
                                       left outer join salve.ficha_ref_bib frb on frb.sq_ficha = fb.sq_ficha
                                  and frb.no_coluna = 'sq_ficha_bioma' and frb.no_tabela = 'ficha_bioma' and frb.sq_registro = fb.sq_ficha_bioma
                                       left outer join taxonomia.publicacao on publicacao.sq_publicacao = frb.sq_publicacao
                                       left outer join salve.bioma as bioma_corp on bioma_corp.no_bioma = bioma_salve.ds_dados_apoio
             ), biomas_ocorrencias_salve as (


            select bioma_corp.no_bioma                                                     as no_bioma,
                   'ocorrencia'                                                            as no_contexto,
                   'salve'::text                                                           as no_origem,
                   -- situacao avaliacao
                   (situacao.cd_sistema = 'REGISTRO_UTILIZADO_AVALIACAO')                  as st_utilizado_avalicao,
                   (situacao.cd_sistema = 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO') as st_adicionado_apos_avaliacao,
                   -- ids
                   null::bigint                                                            as sq_ficha_bioma,
                   bioma_salve.sq_dados_apoio                                              as sq_bioma_salve,
                   bioma_corp.sq_bioma                                                     as sq_bioma_corp,
                   -- ref bib
                   frb.sq_ficha_ref_bib                                                    as sq_ficha_ref_bib,
                   publicacao.sq_publicacao                                                as sq_publicacao,
                   publicacao.de_ref_bibliografica                                         as de_ref_bib,
                   publicacao.de_titulo                                                    as de_titulo,
                   coalesce(publicacao.no_autor, frb.no_autor)                             as no_autor,
                   coalesce(publicacao.nu_ano_publicacao, frb.nu_ano)                      as nu_ano,
                   -- complemento
                   passo2.co_nivel_taxonomico                                              as co_nivel_taxonomico,
                   salve.calc_carencia(fo.dt_inclusao::date, carencia.cd_sistema)::date as dt_carencia

            from salve.ficha_ocorrencia_registro a
                     inner join salve.registro_bioma b on b.sq_registro = a.sq_registro
                     inner join salve.bioma as bioma_corp on bioma_corp.sq_bioma = b.sq_bioma
                     inner join salve.ficha_ocorrencia fo on fo.sq_ficha_ocorrencia = a.sq_ficha_ocorrencia
                     inner join passo2 on passo2.sq_ficha = fo.sq_ficha
                     inner join salve.dados_apoio as situacao on sq_dados_apoio = fo.sq_situacao_avaliacao
                     left join salve.dados_apoio as carencia on carencia.sq_dados_apoio = fo.sq_prazo_carencia
                     left join salve.dados_apoio as bioma_salve on bioma_salve.ds_dados_apoio = bioma_corp.no_bioma
                     left join salve.dados_apoio as ref_aprox on ref_aprox.sq_dados_apoio = fo.sq_ref_aproximacao
                     left join salve.dados_apoio as met_aprox on met_aprox.sq_dados_apoio = fo.sq_metodo_aproximacao
                     left join salve.ficha_ref_bib as frb on frb.sq_ficha = fo.sq_ficha
                and frb.no_tabela = 'ficha_ocorrencia'
                and frb.no_coluna = 'sq_ficha_ocorrencia'
                and frb.sq_registro = fo.sq_ficha_ocorrencia
                     left join taxonomia.publicacao on publicacao.sq_publicacao = frb.sq_publicacao
            where  (fo.in_presenca_atual is null or fo.in_presenca_atual <> 'N')
              -- não considerar registros que forem centroide do Estado
              and (fo.sq_metodo_aproximacao IS NULL OR met_aprox.cd_sistema <> 'CENTROIDE' OR
                   ref_aprox.cd_sistema <> 'ESTADO')
              and situacao.cd_sistema in ('REGISTRO_UTILIZADO_AVALIACAO', 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')
              -- se nao for do grupo marinho não exibir o bioma marinho
              and ( not bioma_corp.no_bioma ilike '%MARINHO%' or passo2.st_marinho )

        )
                , biomas_ocorrencias_portalbio as (
            select bioma_corp.no_bioma                                                     as no_bioma,
                   'ocorrencia'                                                            as no_contexto,
                   'portalbio'::text                                                       as no_origem,
                   -- situacao avaliacao
                   (situacao.cd_sistema = 'REGISTRO_UTILIZADO_AVALIACAO')                  as st_utilizado_avalicao,
                   (situacao.cd_sistema = 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO') as st_adicionado_apos_avaliacao,
                   -- ids
                   null::bigint                                                            as sq_ficha_bioma,
                   bioma_salve.sq_dados_apoio                                              as sq_bioma_salve,
                   bioma_corp.sq_bioma                                                     as sq_bioma_corp,
                   -- ref bib
                   frb.sq_ficha_ref_bib                                                    as sq_ficha_ref_bib,
                   publicacao.sq_publicacao                                                as sq_publicacao,
                   publicacao.de_ref_bibliografica                                         as de_ref_bib,
                   publicacao.de_titulo                                                    as de_titulo,
                   coalesce(publicacao.no_autor, frb.no_autor)                             as no_autor,
                   coalesce(publicacao.nu_ano_publicacao, frb.nu_ano)                      as nu_ano,
                   -- complemento
                   passo2.co_nivel_taxonomico                                              as co_nivel_taxonomico,
                   fo.dt_carencia::date as dt_carencia
            from salve.ficha_ocorrencia_portalbio_reg a
                     inner join salve.registro_bioma b on b.sq_registro = a.sq_registro
                     inner join salve.bioma as bioma_corp on bioma_corp.sq_bioma = b.sq_bioma
                     inner join salve.ficha_ocorrencia_portalbio fo on fo.sq_ficha_ocorrencia_portalbio = a.sq_ficha_ocorrencia_portalbio
                     inner join passo2 on passo2.sq_ficha = fo.sq_ficha
                     inner join salve.dados_apoio as situacao on sq_dados_apoio = fo.sq_situacao_avaliacao
                     left join salve.dados_apoio as bioma_salve on bioma_salve.ds_dados_apoio = bioma_corp.no_bioma
                     left join salve.ficha_ref_bib as frb on frb.sq_ficha = fo.sq_ficha
                and frb.no_tabela = 'ficha_ocorrencia_portalbio'
                and frb.no_coluna = 'sq_ficha_ocorrencia_portalbio'
                and frb.sq_registro = fo.sq_ficha_ocorrencia_portalbio
                     left join taxonomia.publicacao on publicacao.sq_publicacao = frb.sq_publicacao
            where  (fo.in_presenca_atual is null or fo.in_presenca_atual <> 'N')
              and situacao.cd_sistema in ('REGISTRO_UTILIZADO_AVALIACAO', 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')
              -- se nao for do grupo marinho não exibir o bioma marinho
              and ( not bioma_corp.no_bioma ilike '%MARINHO%' or passo2.st_marinho )
        )
        select a.*, false as st_em_carencia from biomas_ficha as a
        union
        select b.*, case when b.dt_carencia is null or b.dt_carencia < now() then false else true end as st_em_carencia from biomas_ocorrencias_salve as b
        union
        select c.*, case when c.dt_carencia is null or c.dt_carencia < now() then false else true end as st_em_carencia from biomas_ocorrencias_portalbio as c;
end;
$$;


--
-- TOC entry 1788 (class 1255 OID 29006)
-- Name: fn_taxon_estados(bigint); Type: FUNCTION; Schema: salve; Owner: -
--

CREATE FUNCTION salve.fn_taxon_estados(p_sq_taxon bigint) RETURNS TABLE(no_contexto text, no_origem text, sq_estado bigint, no_estado text, sg_estado text, sq_regiao bigint, no_regiao text, nu_ordem_regiao integer, st_utilizado_avalicao boolean, st_adicionado_apos_avaliacao boolean, sq_ficha_uf bigint, sq_ficha_ref_bib bigint, sq_publicacao bigint, de_ref_bib text, de_titulo text, no_autor text, nu_ano integer, co_nivel_taxonomico text, dt_carencia date, st_em_carencia boolean)
    LANGUAGE plpgsql
    AS $$
begin
    return query
        with passo1 as (select ficha.sq_taxon
                             , ficha.sq_ficha
                             , ficha.sq_ciclo_avaliacao
                             , ficha.sq_grupo
                             , case
                                   when ficha.sq_grupo is not null and (grupo.cd_sistema like '%MARINHO%'
                                       OR  grupo.cd_sistema in
                                           ('CEFALOPODAS', 'EQUINODERMAS', 'BRAQUIOPODAS', 'CNIDARIAS', 'ENTEROPNEUSTAS',
                                            'POLIQUETAS', 'SIPUNCULAS')
                                       )
                                       THEN true
                                   else false end as st_marinho
                        from salve.ficha
                                 left outer join salve.dados_apoio as grupo on grupo.sq_dados_apoio = ficha.sq_grupo
                        where ficha.sq_taxon = p_sq_taxon
        ),
             passo2 as (select ficha.sq_ficha,
                               passo1.sq_ciclo_avaliacao,
                               taxon.sq_taxon,
                               nivel.co_nivel_taxonomico,
                               passo1.sq_grupo,
                               passo1.st_marinho
                        from passo1
                                 inner join taxonomia.taxon
                                            on (taxon.sq_taxon = passo1.sq_taxon or taxon.sq_taxon_pai = passo1.sq_taxon)
                                 inner join taxonomia.nivel_taxonomico as nivel
                                            on nivel.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
                                 inner join salve.ficha on ficha.sq_taxon = taxon.sq_taxon
                            and ficha.sq_ciclo_avaliacao = passo1.sq_ciclo_avaliacao
             ),
             estados_ficha as (select 'ficha'::text                                             as no_contexto,
                                   'salve'::text                                             as no_origem,
                                   uf.sq_estado::bigint                                      as sq_estado,
                                   uf.no_estado::text                                        as no_estado,
                                   uf.sg_estado::text                                        as no_estado,
                                   regiao.sq_regiao::bigint                                  as sq_regiao,
                                   regiao.no_regiao::text                                    as no_regiao,
                                   case when lower(regiao.no_regiao) = 'norte' then 1 else
                                       case when lower(regiao.no_regiao) = 'nordeste' then 2 else
                                           case when lower(regiao.no_regiao) = 'centro-oeste' then 3 else
                                               case when lower(regiao.no_regiao) = 'sudeste' then 4 else
                                                   case when lower(regiao.no_regiao) = 'sul' then 5 else 6
                                                       end end end end end::integer                                      as nu_ordem_regiao
                                    -- situacao avaliacao
                                    , true::boolean                                               as st_utilizado_avalicao
                                    , false::boolean                                              as st_adicionado_apos_avaliacao

                                    -- ids
                                    , fu.sq_ficha_uf::bigint                                  as sq_ficha_uf

                                    -- ref bib
                                    , frb.sq_ficha_ref_bib::bigint                             as sq_ficha_ref_bib
                                    , publicacao.sq_publicacao::bigint                         as sq_publicacao
                                    , publicacao.de_ref_bibliografica::text                    as de_ref_bib
                                    , publicacao.de_titulo::text                               as de_titulo
                                    , coalesce(publicacao.no_autor, frb.no_autor)::text        as no_autor
                                    , coalesce(publicacao.nu_ano_publicacao, frb.nu_ano)::integer as nu_ano
                                    -- complemento
                                    , passo2.co_nivel_taxonomico::text                         as co_nivel_taxonomico
                                    , null::date as dt_carencia
                               from salve.ficha_uf fu
                                        inner join passo2 on passo2.sq_ficha = fu.sq_ficha
                                        inner join salve.estado as uf on uf.sq_estado = fu.sq_estado
                                        left outer join salve.regiao as regiao on regiao.sq_regiao = uf.sq_regiao
                                        left outer join salve.ficha_ref_bib frb on frb.sq_ficha = fu.sq_ficha
                                   and frb.no_coluna = 'sq_ficha_uf' and frb.no_tabela = 'ficha_uf'
                                   and frb.sq_registro = fu.sq_ficha_uf
                                        left outer join taxonomia.publicacao on publicacao.sq_publicacao = frb.sq_publicacao

             ), estados_ocorrencias_salve as (
            select 'ocorrencia'::text                                as no_contexto,
                   'salve'::text                                             as no_origem,
                   uf.sq_estado::bigint                                      as sq_estado,
                   uf.no_estado::text                                        as no_estado,
                   uf.sg_estado::text                                        as sg_estado,
                   regiao.sq_regiao::bigint                                  as sq_regiao,
                   regiao.no_regiao::text                                    as no_regiao,
                   case when lower(regiao.no_regiao) = 'norte' then 1 else
                       case when lower(regiao.no_regiao) = 'nordeste' then 2 else
                           case when lower(regiao.no_regiao) = 'centro-oeste' then 3 else
                               case when lower(regiao.no_regiao) = 'sudeste' then 4 else
                                   case when lower(regiao.no_regiao) = 'sul' then 5 else 6
                                       end end end end end::integer                                      as nu_ordem_regiao,
                   -- situacao avaliacao
                   (situacao.cd_sistema = 'REGISTRO_UTILIZADO_AVALIACAO')::boolean                  as st_utilizado_avalicao,
                   (situacao.cd_sistema = 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')::boolean as st_adicionado_apos_avaliacao,

                   -- ids
                   null::bigint                                  as sq_ficha_uf,

                   -- ref bib
                   frb.sq_ficha_ref_bib::bigint                             as sq_ficha_ref_bib,
                   publicacao.sq_publicacao::bigint                         as sq_publicacao,
                   publicacao.de_ref_bibliografica::text                    as de_ref_bib,
                   publicacao.de_titulo::text                               as de_titulo,
                   coalesce(publicacao.no_autor, frb.no_autor)::text        as no_autor,
                   coalesce(publicacao.nu_ano_publicacao, frb.nu_ano)::integer as nu_ano,
                   -- complemento
                   passo2.co_nivel_taxonomico::text                         as co_nivel_taxonomico,
                   salve.calc_carencia(fo.dt_inclusao::date, carencia.cd_sistema)::date as dt_carencia

            from salve.ficha_ocorrencia_registro a
                     inner join salve.registro_estado b on b.sq_registro = a.sq_registro
                     inner join salve.estado as uf on uf.sq_estado = b.sq_estado
                     inner join salve.regiao as regiao on regiao.sq_regiao = uf.sq_regiao
                     inner join salve.ficha_ocorrencia fo on fo.sq_ficha_ocorrencia = a.sq_ficha_ocorrencia
                     inner join passo2 on passo2.sq_ficha = fo.sq_ficha
                     inner join salve.dados_apoio as situacao on sq_dados_apoio = fo.sq_situacao_avaliacao
                     left join salve.dados_apoio as carencia on carencia.sq_dados_apoio = fo.sq_prazo_carencia
                     left join salve.dados_apoio as ref_aprox on ref_aprox.sq_dados_apoio = fo.sq_ref_aproximacao
                     left join salve.dados_apoio as met_aprox on met_aprox.sq_dados_apoio = fo.sq_metodo_aproximacao
                     left join salve.ficha_ref_bib as frb on frb.sq_ficha = fo.sq_ficha
                and frb.no_tabela = 'ficha_ocorrencia'
                and frb.no_coluna = 'sq_ficha_ocorrencia'
                and frb.sq_registro = fo.sq_ficha_ocorrencia
                     left join taxonomia.publicacao on publicacao.sq_publicacao = frb.sq_publicacao
            where  (fo.in_presenca_atual is null or fo.in_presenca_atual <> 'N')
              and situacao.cd_sistema in ('REGISTRO_UTILIZADO_AVALIACAO', 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')
              -- bioma marinho não possui pontos em Estados
              and passo2.st_marinho = false


        ), estados_ocorrencias_portalbio as (

            select 'ocorrencia'                                                            as no_contexto,
                   'salve'::text                                                           as no_origem,
                   uf.sq_estado::bigint                                      as sq_estado,
                   uf.no_estado::text                                        as no_estado,
                   uf.sg_estado::text                                        as no_estado,
                   regiao.sq_regiao                                          as sq_regiao,
                   regiao.no_regiao                                          as no_regiao,
                   case when lower(regiao.no_regiao) = 'norte' then 1 else
                       case when lower(regiao.no_regiao) = 'nordeste' then 2 else
                           case when lower(regiao.no_regiao) = 'centro-oeste' then 3 else
                               case when lower(regiao.no_regiao) = 'sudeste' then 4 else
                                   case when lower(regiao.no_regiao) = 'sul' then 5 else 6
                                       end end end end end::integer                                      as nu_ordem_regiao,
                   -- situacao avaliacao
                   (situacao.cd_sistema = 'REGISTRO_UTILIZADO_AVALIACAO')                  as st_utilizado_avalicao,
                   (situacao.cd_sistema = 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO') as st_adicionado_apos_avaliacao,

                   -- ids
                   null::bigint                                  as sq_ficha_uf,

                   -- ref bib
                   frb.sq_ficha_ref_bib::bigint                             as sq_ficha_ref_bib,
                   publicacao.sq_publicacao::bigint                         as sq_publicacao,
                   publicacao.de_ref_bibliografica::text                    as de_ref_bib,
                   publicacao.de_titulo::text                               as de_titulo,
                   coalesce(publicacao.no_autor, frb.no_autor)::text        as no_autor,
                   coalesce(publicacao.nu_ano_publicacao, frb.nu_ano)::integer as nu_ano,
                   -- complemento
                   passo2.co_nivel_taxonomico::text                         as co_nivel_taxonomico,
                   fo.dt_carencia::date as dt_carencia
            from salve.ficha_ocorrencia_portalbio_reg a
                     inner join salve.registro_estado b on b.sq_registro = a.sq_registro
                     inner join salve.estado as uf on uf.sq_estado = b.sq_estado
                     inner join salve.regiao as regiao on regiao.sq_regiao = uf.sq_regiao
                     inner join salve.ficha_ocorrencia_portalbio fo on fo.sq_ficha_ocorrencia_portalbio = a.sq_ficha_ocorrencia_portalbio
                     inner join passo2 on passo2.sq_ficha = fo.sq_ficha
                     inner join salve.dados_apoio as situacao on sq_dados_apoio = fo.sq_situacao_avaliacao
                     left join salve.ficha_ref_bib as frb on frb.sq_ficha = fo.sq_ficha
                and frb.no_tabela = 'ficha_ocorrencia_portalbio'
                and frb.no_coluna = 'sq_ficha_ocorrencia_portalbio'
                and frb.sq_registro = fo.sq_ficha_ocorrencia_portalbio
                     left join taxonomia.publicacao on publicacao.sq_publicacao = frb.sq_publicacao
            where  (fo.in_presenca_atual is null or fo.in_presenca_atual <> 'N')
              and situacao.cd_sistema in ('REGISTRO_UTILIZADO_AVALIACAO', 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO')
              -- bioma marinho não possui pontos em Estados
              and passo2.st_marinho = false
        )
        select a.*, false as st_em_carencia from estados_ficha as a
        union
        select b.*, case when b.dt_carencia is null or b.dt_carencia < now() then false else true end as st_em_carencia from estados_ocorrencias_salve as b
        union
        select c.*, case when c.dt_carencia is null or c.dt_carencia < now() then false else true end as st_em_carencia from estados_ocorrencias_portalbio as c;
end;
$$;


--
-- TOC entry 1796 (class 1255 OID 29161)
-- Name: fn_traducao_calcular_percentuais(text); Type: FUNCTION; Schema: salve; Owner: -
--

CREATE FUNCTION salve.fn_traducao_calcular_percentuais(p_sq_fichas text) RETURNS TABLE(sq_ficha bigint, nu_percentual_traduzido integer, nu_percentual_revisado integer)
    LANGUAGE plpgsql
    AS $$
    /*
      funcao para calcular o percentual traduzido e revisado de uma ou da lista de fichas
      Testar: select * from salve.fn_traducao_calcular_percentuais('19160,35824');
    */
declare
    contaColunas text;
    cmdSql text;
begin
    SET client_min_messages TO WARNING;

    -- cada coluna traduzivel da ficha vale 1 se estiver preenchida
    select  array_to_string( array_agg( distinct concat('case when ficha.',tt.no_coluna,' is null then 0 else 1 end') ),' + ' ) into contaColunas
    from salve.traducao_tabela tt
    where tt.no_tabela = 'ficha'
      and tt.no_coluna like 'ds_%';
    /*
    RAISE NOTICE ' ';
    RAISE NOTICE ' ';
    RAISE NOTICE ' ';
    RAISE NOTICE 'Fichas %',p_sq_fichas;
    RAISE NOTICE 'Colunas %',contaColunas;
    */
    cmdSql = 'with passo1 as (
                    select ficha.sq_ficha
                         , 11 as nu_colunas_preenchidas
                         , coalesce(andamento.qtd_revisado,0) as qtd_revisado
                         , coalesce(andamento.qtd_traduzido,0) as qtd_traduzido
                    from salve.ficha
                        inner join salve.dados_apoio as situacao on situacao.sq_dados_apoio = ficha.sq_situacao_ficha
                        left join lateral (
                        select sum(case when tr.tx_traduzido is not null then 1 else 0 end) as qtd_traduzido
                             , sum(case when tr.dt_revisao is null then 0 else 1 end)         as qtd_revisado
                        from salve.traducao_revisao tr
                                 inner join salve.traducao_tabela tt on tt.sq_traducao_tabela = tr.sq_traducao_tabela
                        where tr.sq_registro = ficha.sq_ficha
                          and tt.no_tabela = ''ficha''
                          and tt.no_coluna like ''ds_%''
                        ) as andamento on true
                        where
                         situacao.cd_sistema in (''PUBLICADA'',''FINALIZADA'')
                         and ficha.sq_ficha = any( array['||p_sq_fichas||'])
                    ), passo2 as (
                        select ficha.nm_cientifico
                             , passo1.sq_ficha
                             , (case when nu_colunas_preenchidas = 0 then 1 else nu_colunas_preenchidas end)::decimal as nu_colunas_preenchidas
                             , qtd_traduzido::decimal
                             , qtd_revisado::decimal
                        from passo1
                                 inner join salve.ficha on ficha.sq_ficha = passo1.sq_ficha
                    )
                    select passo2.sq_ficha::bigint
                          , least(100,(passo2.qtd_traduzido / nu_colunas_preenchidas * 100)::int) as percentual_traduzido
                          , least(case when passo2.qtd_traduzido=0 then 0 else passo2.qtd_revisado / passo2.qtd_traduzido * 100 end::int) as percentual_revisado
                          from passo2;';

    return QUERY EXECUTE cmdSql;
end
$$;


--
-- TOC entry 7888 (class 0 OID 0)
-- Dependencies: 1796
-- Name: FUNCTION fn_traducao_calcular_percentuais(p_sq_fichas text); Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON FUNCTION salve.fn_traducao_calcular_percentuais(p_sq_fichas text) IS 'funcao para calcular o percentual traduzido e revisado de uma ou varias fichas';


--
-- TOC entry 1813 (class 1255 OID 29625)
-- Name: fn_versionar_ficha(bigint, bigint, bigint); Type: FUNCTION; Schema: salve; Owner: -
--

CREATE FUNCTION salve.fn_versionar_ficha(p_sq_ficha bigint, p_sq_pessoa bigint, p_sq_contexto bigint) RETURNS json
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_status integer;
    v_sq_ciclo_avaliacao bigint;
    v_nm_cientifico text;
    v_resultado JSON;
    v_msg text;
    v_array text ARRAY;
    v_type text = 'success';
BEGIN
    SET client_min_messages TO NOTICE;--WARNING;

    select sq_ciclo_avaliacao, nm_cientifico into v_sq_ciclo_avaliacao, v_nm_cientifico from salve.ficha where sq_ficha = p_sq_ficha;

    RAISE NOTICE ' ';
    RAISE NOTICE ' ';
    RAISE NOTICE ' ';
    RAISE NOTICE ' ';
    RAISE NOTICE '--------------------------------------------------- ';
    v_msg =  format('Iniciando versionamento da ficha %s / sq_ficha: %s / %sº ciclo',TRIM(v_nm_cientifico), p_sq_ficha, v_sq_ciclo_avaliacao);
    RAISE NOTICE '%',v_msg;
    v_array = array_append(v_array,v_msg);
    v_status = 0;
    if v_sq_ciclo_avaliacao = 1 then
        v_msg = '  - versionando ficha do ciclo 1...';
        RAISE NOTICE '%',v_msg;
        v_array = array_append(v_array,v_msg);
        v_resultado = salve.fn_versionar_ficha_ciclo_1(p_sq_ficha, p_sq_pessoa , p_sq_contexto);
    ELSEIF v_sq_ciclo_avaliacao = 2 then
        v_msg = '  - versionando ficha do ciclo 2...';
        RAISE NOTICE '%',v_msg;
        v_array = array_append(v_array,v_msg);
        v_resultado = salve.fn_versionar_ficha_ciclo_2(p_sq_ficha, p_sq_pessoa , p_sq_contexto);
    END IF;

    if v_resultado->>'status'::text = '0'  then
        v_status = 0;
        v_type = 'success';
        v_msg = format('  - ficha do ciclo %s versionada com SUCESSO', v_sq_ciclo_avaliacao);
        RAISE NOTICE '%',v_msg;
        v_array = array_append(v_array,v_msg);
    ELSE
        v_status = 1;
        v_type = 'error';
        v_msg = format('  - ERRO ao versionar a ficha do ciclo %s', v_sq_ciclo_avaliacao);
        RAISE NOTICE '%',v_msg;
        v_array = array_append(v_array,v_msg);
    END IF;
    SET client_min_messages TO NOTICE;

    -- RAISE EXCEPTION 'TESTE fn_versionar_ficha()';


    return json_build_object('status',v_status
        ,'type',v_type
        ,'msg', array_to_string(v_array,chr(13))
        ,'data',v_resultado);

EXCEPTION WHEN OTHERS THEN
    RAISE NOTICE '  - ERRO: %', SQLERRM;
    get stacked diagnostics
        v_msg = pg_exception_context;
    raise notice E'Got exception:
            context: %', v_msg;
    SET client_min_messages TO NOTICE;
    return json_build_object('status',1
        ,'type',v_type
        ,'msg', CONCAT( SQLERRM,CHR(13),v_msg,CHR(13),array_to_string(v_array,chr(13)) )
        ,'data',v_resultado );
END
$$;


--
-- TOC entry 1811 (class 1255 OID 29617)
-- Name: fn_versionar_ficha_ciclo_1(bigint, bigint, bigint); Type: FUNCTION; Schema: salve; Owner: -
--

CREATE FUNCTION salve.fn_versionar_ficha_ciclo_1(p_sq_ficha bigint, p_sq_pessoa bigint, p_sq_contexto bigint) RETURNS json
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_context TEXT;
    v_msg TEXT;
    v_log text ARRAY;
    v_array_fichas text ARRAY;
    v_resultado JSON;

    -- ------------
    v_nu_versao_atual       integer;
    v_publicar boolean = false;
    v_sq_taxon bigint;
    v_sq_ficha_versao bigint;
    v_sq_taxon_hist_aval bigint;
    v_cd_contexto text;
    v_sq_avaliacao_naciona_brasil bigint;
    v_sq_categoria_validada bigint;
    v_ds_justificativa_validada text;
    v_cd_categoria_validada text;
    v_ds_criterio_validado text;
    v_pex_validado text;
    v_nu_ano integer;
    v_sq_ficha_ciclo_2 bigint;
    v_sq_ficha_json bigint;
    v_cd_situacao_ficha_ciclo_2 text;
    v_versionar_ficha_ciclo_2 boolean = false;

    --v_sq_situacao_compilacao bigint;
    v_sq_nova_situacao bigint;
    v_mesmo_taxon boolean;
BEGIN

    SET client_min_messages TO NOTICE;

    /*
    -- TESTE ARRAY
    -- select array_agg( json_build_array(sq_ficha,nm_cientifico) ) as fichas
    select array_agg(json_build_object('sq_ficha',sq_ficha,'nm_cientifico', nm_cientifico)) into v_array_fichas
    -- select array_agg( array[sq_ficha::text,nm_cientifico::text] ) into v_array_fichas
    from salve.ficha
    inner join salve.dados_apoio as situacao on situacao.sq_dados_apoio = ficha.sq_situacao_ficha
    where sq_ficha_ciclo_anterior=25128;

    RAISE NOTICE  ' ';
    RAISE NOTICE  ' ';
    RAISE NOTICE  ' ';
    raise notice 'len: %', array_length(v_array_fichas,1);
    --raise NOTICE 'array: %', v_array_fichas;
    raise NOTICE '1 id: %', (v_array_fichas[1]::json)->'sq_ficha';
    raise NOTICE '1 especie: %', (v_array_fichas[1]::json)->'nm_cientifico';
    raise NOTICE '2 id: %', (v_array_fichas[2]::json)->'sq_ficha';
    raise NOTICE '2 especie: %', (v_array_fichas[2]::json)->'nm_cientifico';
    v_msg = '';
    FOR counter IN 1..2
        LOOP
            if counter > 1 then
                v_msg = v_msg || ', ';
            END IF;
            v_msg = v_msg || ( (v_array_fichas[counter]::json )->>'nm_cientifico')::text;
        END LOOP;
    RAISE NOTICE 'Especies: %', v_msg;
    return
        */


    -- NO PRIMEIRO CICLO A VERSÃO SERÁ SEMPRE A PRIMEIRA
    v_nu_versao_atual = 0;-- ( SELECT COALESCE(MAX(nu_versao), 0) FROM salve.ficha_versao WHERE sq_ficha = p_sq_ficha);
    v_msg = format('  - Versão atual: %s',v_nu_versao_atual);
    v_log = array_append(v_log,v_msg);
    RAISE NOTICE '%',v_msg;

    -- ler o contexto do versionamento: Publicação, Exclusão etc
    v_cd_contexto = ( SELECT cd_sistema FROM salve.dados_apoio where sq_dados_apoio = p_sq_contexto);
    v_msg = format('  - Contexto: %s',v_cd_contexto);
    v_log = array_append(v_log,v_msg);
    RAISE NOTICE '%',v_msg;

    -- ler o taxon da ficha
    SELECT sq_taxon INTO v_sq_taxon FROM salve.ficha WHERE sq_ficha = p_sq_ficha;
    v_msg = format('  - Taxon (%s)', v_sq_taxon);
    v_log = array_append(v_log,v_msg);
    RAISE NOTICE '%', v_msg;

    IF v_cd_contexto = 'PUBLICACAO' THEN

        -- PARA O 1º CICLO NÃO TEM AS REGRAS PARA PUBLICAÇÃO RELACIONADA AS CATEGORIAS

        -- ler as informações da validação para gerar o registro no taxon_historico_avaliacao no final
        SELECT x.sq_categoria_final, cat_validada.cd_sistema, x.ds_justificativa_final
             , x.ds_criterio_aval_iucn_final, x.st_possivelmente_extinta_final, to_char(x.dt_aceite_validacao,'yyyy')::int
        INTO v_sq_categoria_validada, v_cd_categoria_validada, v_ds_justificativa_validada, v_ds_criterio_validado, v_pex_validado, v_nu_ano
        FROM salve.ficha x
                 LEFT JOIN salve.dados_apoio as cat_validada on cat_validada.sq_dados_apoio = x.sq_categoria_final
        WHERE x.sq_ficha = p_sq_ficha;

        v_msg = format( '  - Categoria Validada (%s) %s', v_cd_categoria_validada, v_sq_categoria_validada);
        v_log = array_append(v_log,v_msg);
        RAISE NOTICE '%',v_msg;

        v_msg = format( '  - Categoria Validada (%s) %s', v_cd_categoria_validada, v_sq_categoria_validada);
        RAISE NOTICE '%',v_msg;
        v_log = array_append(v_log,v_msg);

        v_msg = format( '  - Critério (%s)', v_ds_criterio_validado);
        RAISE NOTICE '%',v_msg;
        v_log = array_append(v_log,v_msg);

        v_msg = format( '  - PEX: %s', v_pex_validado);
        RAISE NOTICE '%',v_msg;
        v_log = array_append(v_log,v_msg);

        v_msg = format( '  - Ano aceite validação: %s', v_nu_ano);
        RAISE NOTICE '%',v_msg;
        v_log = array_append(v_log,v_msg);

        -- publicar as fichas do 1º ciclo se ela estiver publicada na visao mv_dados_modulo_publico
        SELECT exists ( select null from salve.mv_dados_modulo_publico
                        WHERE sq_ficha = p_sq_ficha limit 1 ) into v_publicar;

        -- não pode existir outra versão publicada para a ficha do 1º ciclo poder ser publicada
        if v_publicar  then
            SELECT not exists ( select null from salve.ficha_versao
                                WHERE sq_ficha = p_sq_ficha and st_publico = true limit 1 ) into v_publicar;
        END IF;
        v_msg = format('  - Publicar a versão? (%s)',v_publicar);
        v_log = array_append(v_log,v_msg);
        RAISE NOTICE  '%',v_msg;
    END IF; -- FIM CONTEXTO = PUBLICACAO

    -- REGRAS PARA VERSIONAMENTO DA FICHA DO 1º CICLO

    -- se a ficha tiver uma correspondente no ciclo 2, a versão deve ser gerada para esta ficha do ciclo 2
    -- e se a ficha do 2º ciclo estiver publicada tambem tem que ser versionada gerando a V2
    --SELECT sq_ficha, true into v_sq_ficha_ciclo_2, v_mesmo_taxon FROM salve.ficha
    SELECT array_agg(json_build_object('st_mesmo_taxon',true,'sq_ficha',sq_ficha,'nm_cientifico'
        , coalesce(taxon.json_trilha->'subespecie'->>'no_taxon',taxon.json_trilha->'especie'->>'no_taxon')
        ,'cd_situacao_sistema',situacao.cd_sistema)) into v_array_fichas
    FROM salve.ficha
             INNER JOIN salve.dados_apoio as situacao on situacao.sq_dados_apoio = ficha.sq_situacao_ficha
             INNER JOIN taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
    WHERE ficha.sq_ciclo_avaliacao = 2
      AND sq_ficha_ciclo_anterior = p_sq_ficha
      AND ficha.sq_taxon = v_sq_taxon;
    --AND situacao.cd_sistema <> 'EXCLUIDA';

    -- se existir somente 1 ficha no 2º ciclo inicializar as variaveis
    v_mesmo_taxon               = false;
    v_sq_ficha_ciclo_2          = null;
    v_cd_situacao_ficha_ciclo_2 = null;

    if v_array_fichas is not null then
        RAISE NOTICE 'MESMO TAXON %',((v_array_fichas[1])::json->>'st_mesmo_taxon');
        v_mesmo_taxon = ((v_array_fichas[1])::json->>'st_mesmo_taxon')::boolean;
        v_sq_ficha_ciclo_2 = ((v_array_fichas[1])::json->>'sq_ficha')::bigint;
        v_cd_situacao_ficha_ciclo_2 = ((v_array_fichas[1])::json->>'cd_situacao_sistema')::text;
    END IF;

    -- se não encontrou a ficha no 2º ciclo com o mesmo taxon, verificar se tem com outro taxon
    if v_array_fichas is null then
        SELECT array_agg(json_build_object('st_mesmo_taxon',false,'sq_ficha',sq_ficha,'nm_cientifico'
            , coalesce(taxon.json_trilha->'subespecie'->>'no_taxon',taxon.json_trilha->'especie'->>'no_taxon')
            )) into v_array_fichas
        FROM salve.ficha
                 INNER JOIN salve.dados_apoio as situacao on situacao.sq_dados_apoio = ficha.sq_situacao_ficha
                 INNER JOIN taxonomia.taxon on taxon.sq_taxon = ficha.sq_taxon
        WHERE ficha.sq_ciclo_avaliacao = 2
          AND sq_ficha_ciclo_anterior = p_sq_ficha
          AND ficha.sq_taxon <> v_sq_taxon;
        --AND situacao.cd_sistema <> 'EXCLUIDA';
    END IF;

    v_msg = format( '  - Ficha(s) correspondente(s) no 2º ciclo: %s',v_array_fichas);
    v_log = array_append(v_log,v_msg);
    RAISE NOTICE '%',v_msg;

    v_msg = format( '  - Mesmo táxon? %s',v_mesmo_taxon);
    v_log = array_append(v_log,v_msg);
    RAISE NOTICE '%',v_msg;

    -- se não existir ficha correspondente no 2º ciclo, a ficha do primeiro ciclo deve ser versionadas com V1 apenas
    if v_sq_ficha_ciclo_2 is null then
        SELECT sq_ficha_versao into v_sq_ficha_versao from salve.ficha_versao
        WHERE sq_ficha = p_sq_ficha  and nu_versao = 1 limit 1;
    ELSE
        v_msg = format( '  - Situação da ficha do 2º ciclo %s',v_cd_situacao_ficha_ciclo_2);
        v_log = array_append(v_log,v_msg);
        RAISE NOTICE '%',v_msg;

        -- se existir a ficha correspondente no 2º ciclo com o mesmo taxon, considerar a V1 como sendo a ficha do 1º ciclo que já foi versionada 1 vez
        SELECT sq_ficha_versao into v_sq_ficha_versao from salve.ficha_versao
        WHERE sq_ficha = v_sq_ficha_ciclo_2  and nu_versao = 1 limit 1;
    END IF;

    -- criar a versão JSON da ficha quando não existir
    IF v_sq_ficha_versao is null THEN
        INSERT INTO salve.ficha_versao (sq_ficha, nu_versao, js_ficha, sq_usuario_inclusao, st_publico, sq_contexto)
        SELECT p_sq_ficha,
               (v_nu_versao_atual + 1),
               salve.fn_ficha2json(p_sq_ficha)
                ,
               p_sq_pessoa
                ,
               v_publicar
                ,
               p_sq_contexto
        RETURNING sq_ficha_versao INTO v_sq_ficha_versao;

        v_msg = format( '  - Versão json criada id: %s', v_sq_ficha_versao);
        v_log = array_append(v_log,v_msg);
        RAISE NOTICE '%', v_msg;

        -- se não existir a ficha no 2ºciclo, apenas transferir a ficha para o 2º ciclo e alterar a situação para excluida
        if v_sq_ficha_ciclo_2 is null then

            /*
            SELECT dados_apoio.sq_dados_apoio into v_sq_situacao_compilacao
            FROM salve.dados_apoio
            INNER JOIN salve.dados_apoio pai ON pai.sq_dados_apoio = dados_apoio.sq_dados_apoio_pai
            WHERE pai.cd_sistema = 'TB_SITUACAO_FICHA'
              AND dados_apoio.cd_sistema = 'COMPILACAO'
            LIMIT 1;*/
            SELECT dados_apoio.sq_dados_apoio into v_sq_nova_situacao
            FROM salve.dados_apoio
                     INNER JOIN salve.dados_apoio pai ON pai.sq_dados_apoio = dados_apoio.sq_dados_apoio_pai
            WHERE pai.cd_sistema = 'TB_SITUACAO_FICHA'
              AND dados_apoio.cd_sistema = 'EXCLUIDA'
            LIMIT 1;

            -- transferir a ficha do 1º para o 2º ciclo
            --update salve.ficha set sq_ciclo_avaliacao = 2, sq_situacao_ficha = v_sq_situacao_compilacao where sq_ficha = p_sq_ficha;
            update salve.ficha set sq_ciclo_avaliacao = 2, sq_situacao_ficha = v_sq_nova_situacao where sq_ficha = p_sq_ficha;
            --v_msg = '  - Ficha transferida para o 2º ciclo e alterada a situação para compilação';
            v_msg = '  - Ficha transferida para o 2º ciclo na situação EXCLUIDA';
            v_log = array_append(v_log,v_msg);
            RAISE NOTICE '%',v_msg;


            -- gerar a mensagem do motivo da exclusão como : Alteração do nome científico para x, y, z...
            v_msg = '';
            if v_array_fichas is not null then
                FOR counter IN 1..array_length(v_array_fichas,1)
                    LOOP
                        if counter > 1 then
                            v_msg = v_msg || ', ';
                        END IF;
                        v_msg = v_msg || concat( '<i>',( (v_array_fichas[counter]::json )->>'nm_cientifico')::text,'</i>');
                    END LOOP;


                -- registrar o motivo da exclusção na tabela ficha_hist_situacao_excluida
                v_msg = concat('Alteração do nome científico para ',v_msg,'.');
                v_log = array_append(v_log,concat('Registrando histórico da exclusão como: ', v_msg ) );
                RAISE NOTICE '%',v_msg;

                INSERT INTO salve.ficha_hist_situacao_excluida( sq_ficha, sq_usuario_inclusao, ds_justificativa )
                VALUES (p_sq_ficha, p_sq_pessoa, v_msg );
            END IF;


            -- a ficha deve passar para o 2º ciclo sem a limpeza das abas e
            /*
            -- limpar dados da avaliação - aba 11, mudanca de categoria, citacao, autoria, colaboradores e equipe tecnica
            if not salve.fn_ficha_limpar_avaliacao(p_sq_ficha) then
                RAISE EXCEPTION 'Não foi possível limpar os dados da avaliação';
            END IF;

            -- desmarcar "Mapa distribuicao principal" da ficha
            update salve.ficha_anexo set in_principal = 'N'
            FROM salve.ficha_anexo fa
                     INNER JOIN salve.dados_apoio as contexto ON contexto.sq_dados_apoio = fa.sq_contexto
            WHERE fa.sq_ficha = p_sq_ficha
              AND contexto.cd_sistema = 'MAPA_DISTRIBUICAO'
              AND fa.in_principal = 'S'
              and fa.sq_ficha_anexo = ficha_anexo.sq_ficha_anexo;

            v_msg = '  - Mapa de distribuição principal foi desmarcado';
            v_log = array_append(v_log,v_msg);
            RAISE NOTICE '%', v_msg;
            */

        ELSE -- existe a ficha no 2º ciclo
        -- se existir a ficha no 2ºciclo com o mesmo taxon criar a V1 dela
            if v_mesmo_taxon then
                -- se existir apenas 1 ficha no 2º criar a V1 dela
                update salve.ficha_versao set sq_ficha = v_sq_ficha_ciclo_2
                                            , js_ficha = jsonb_set(js_ficha,'{_id}',to_jsonb(v_sq_ficha_ciclo_2),false)
                where sq_ficha_versao = v_sq_ficha_versao;
                v_msg = '  - JSON da ficha alterado para a ficha do 2º ciclo';
                v_log = array_append(v_log,v_msg);
                RAISE NOTICE '%',v_msg;

                -- se a ficha estiver publicada, gerar a v2 dela
                if v_cd_situacao_ficha_ciclo_2 = 'PUBLICADA' then
                    v_versionar_ficha_ciclo_2 = true;
                END IF;
            ELSE

                -- se existir mais de 1 ficha no 2º ciclo com taxons diferentes, passar a ficha para o 2º ciclo como excluida
                -- e criar o historico do motivo da exclusão com o texto padrão
                if array_length(v_array_fichas,1) > 1 then
                    SELECT dados_apoio.sq_dados_apoio into v_sq_nova_situacao
                    FROM salve.dados_apoio
                             INNER JOIN salve.dados_apoio pai ON pai.sq_dados_apoio = dados_apoio.sq_dados_apoio_pai
                    WHERE pai.cd_sistema = 'TB_SITUACAO_FICHA'
                      AND dados_apoio.cd_sistema = 'EXCLUIDA'
                    LIMIT 1;

                    -- transferir a ficha do 1º para o 2º ciclo como excluida
                    update salve.ficha set sq_ciclo_avaliacao = 2, sq_situacao_ficha = v_sq_nova_situacao where sq_ficha = p_sq_ficha;
                    v_msg = '  - Ficha transferida para o 2º ciclo e alterada a situação para excluída';
                    v_log = array_append(v_log,v_msg);
                    RAISE NOTICE '%',v_msg;

                    -- criar o motivo da exclusão
                    -- concatenar os nomes das especies que ela virou
                    v_msg = '';
                    FOR counter IN 1..array_length(v_array_fichas,1)
                        LOOP
                            if counter > 1 then
                                v_msg = v_msg || ', ';
                            END IF;
                            v_msg = v_msg || concat( '<i>',( (v_array_fichas[counter]::json )->>'nm_cientifico')::text,'</i>');
                        END LOOP;
                    v_msg = '  - Registrando histórico do motivo da exclusão';
                    v_log = array_append(v_log,v_msg);
                    RAISE NOTICE '%',v_msg;

                    -- v_msg = concat('A espécie foi dividida em ',v_msg,'.');
                    v_msg = concat('Alteração do nome científico para ',v_msg,'.');
                    INSERT INTO salve.ficha_hist_situacao_excluida( sq_ficha, sq_usuario_inclusao, ds_justificativa )
                    VALUES (p_sq_ficha, p_sq_pessoa, v_msg);
                    v_log = array_append(v_log,'  - Motivao da exclusão: '||v_msg);
                    RAISE NOTICE '%',v_msg;
                end if;

            end if;
        END IF;
    ELSE -- existe a v_sq_ficha_versao

    -- se a ficha versionada não for a mesma, não sobrescrever o versionamento
        select (js_ficha->>'sq_ficha')::bigint into v_sq_ficha_json from salve.ficha_versao where sq_ficha_versao = v_sq_ficha_versao;
        if v_sq_ficha_json is not null AND v_sq_ficha_json <> p_sq_ficha then
            RAISE EXCEPTION 'Já existe a versão 1 de outra ficha no 2º ciclo';
        END IF;

        update salve.ficha_versao set js_ficha = salve.fn_ficha2json(p_sq_ficha)
                                    ,sq_usuario_inclusao = p_sq_pessoa
                                    ,dt_inclusao = current_timestamp
        where sq_ficha_versao = v_sq_ficha_versao;
        v_msg = format('  - Versão JSON atualizada id: %s',v_sq_ficha_versao);
        v_log = array_append(v_log,v_msg);
        RAISE NOTICE '%',v_msg;
    END IF;

    -- garantir somente uma versão publicada
    IF v_publicar = TRUE THEN
        UPDATE salve.ficha_versao
        SET st_publico = FALSE
        WHERE sq_ficha = coalesce(v_sq_ficha_ciclo_2, p_sq_ficha)
          AND st_publico = TRUE
          AND nu_versao <> (v_nu_versao_atual+1);

        v_msg =  '  - Executado update para garantir somente uma versao publicada';
        v_log = array_append(v_log,v_msg);
        RAISE NOTICE '%', v_msg;
    END IF;

    -- versionar / atualizar o versionamento dos registros de ocorrencia
    if not exists ( select null from salve.ocorrencias_json where sq_ficha_versao = v_sq_ficha_versao limit 1) then
        INSERT INTO salve.ocorrencias_json (sq_ficha_versao, js_ocorrencias)  SELECT v_sq_ficha_versao, salve.fn_ocorrencias2json(p_sq_ficha);

        v_msg = '  - Ocorrências versionadas';
        v_log = array_append(v_log,v_msg);
        RAISE NOTICE '%',v_msg;
    ELSE
        UPDATE salve.ocorrencias_json set js_ocorrencias = salve.fn_ocorrencias2json(p_sq_ficha) where sq_ficha_versao = v_sq_ficha_versao;
        v_msg = '  - Ocorrências versionadas atualizadas';
        v_log = array_append(v_log,v_msg);
        RAISE NOTICE '%', v_msg;
    END IF;


    -- bloquear as consultas da ficha
    INSERT INTO salve.ciclo_consulta_ficha_versao(sq_ficha_versao, sq_ciclo_consulta_ficha)
    SELECT v_sq_ficha_versao, ciclo_consulta_ficha.sq_ciclo_consulta_ficha
    FROM salve.ciclo_consulta_ficha
    WHERE sq_ficha = p_sq_ficha
      AND NOT EXISTS(
            SELECT NULL FROM salve.ciclo_consulta_ficha_versao x WHERE x.sq_ciclo_consulta_ficha = ciclo_consulta_ficha.sq_ciclo_consulta_ficha LIMIT 1
        );

    v_msg =  '  - Consultas/Revisões bloqueadas pelo versionamento';
    v_log = array_append(v_log,v_msg);
    RAISE NOTICE '%',v_msg;

    -- bloquear as oficinas
    insert into salve.oficina_ficha_versao( sq_ficha_versao, sq_oficina_ficha)
    select v_sq_ficha_versao, oficina_ficha.sq_oficina_ficha
    from salve.oficina_ficha
    where oficina_ficha.sq_ficha = p_sq_ficha
      AND NOT EXISTS (
            SELECT NULL FROM salve.oficina_ficha_versao x where x.sq_oficina_ficha = oficina_ficha.sq_oficina_ficha limit 1
        );
    v_msg = '  - Oficinas bloqueadas pelo versionamento';
    v_log = array_append(v_log,v_msg);
    RAISE NOTICE '%',v_msg;


    -- bloquear as colaborações no gride das ocorrencias (Concordo/Discordo) durante as consultas
    insert into salve.ficha_ocorrencia_validacao_versao( sq_ficha_versao, sq_ficha_ocorrencia_validacao)
    select v_sq_ficha_versao, fov.sq_ficha_ocorrencia_validacao
    from salve.ficha_ocorrencia_validacao fov
             inner join salve.ficha_ocorrencia fo on fo.sq_ficha_ocorrencia = fov.sq_ficha_ocorrencia
    where fo.sq_ficha = p_sq_ficha and not exists (
            select null from salve.ficha_ocorrencia_validacao_versao x where x.sq_ficha_ocorrencia_validacao = fov.sq_ficha_ocorrencia_validacao limit 1
        );
    v_msg =  '  - Colaborações Concordo/Discordo do gride de registros SALVE da consulta bloqueadas pelo versionamento';
    v_log = array_append(v_log,v_msg);
    RAISE NOTICE '%',v_msg;


    -- bloquear as colaborações nas ocorrencias do portalbio
    insert into salve.ficha_ocorrencia_validacao_versao( sq_ficha_versao, sq_ficha_ocorrencia_validacao)
    select v_sq_ficha_versao, fov.sq_ficha_ocorrencia_validacao
    from salve.ficha_ocorrencia_validacao fov
             inner join salve.ficha_ocorrencia_portalbio fo on fo.sq_ficha_ocorrencia_portalbio = fov.sq_ficha_ocorrencia_portalbio
    where fo.sq_ficha = p_sq_ficha and not exists (
            select null from salve.ficha_ocorrencia_validacao_versao x where x.sq_ficha_ocorrencia_validacao = fov.sq_ficha_ocorrencia_validacao limit 1
        );

    v_msg= '  - Colaborações Concordo/Discordo do gride de registros PORTALBIO da consulta bloqueadas pelo versionamento';
    v_log = array_append(v_log,v_msg);
    RAISE NOTICE '%',v_msg;


    -- bloquear ficha_hist_situacao_excluida
    IF v_cd_contexto = 'EXCLUSAO_OFICINA' or v_cd_contexto = 'EXCLUSAO_LOTE' or v_cd_contexto = 'EXCLUSAO_FICHA' THEN
        INSERT INTO salve.ficha_hist_sit_exc_versao( sq_ficha_versao, sq_ficha_hist_situacao_excluida)
        SELECT v_sq_ficha_versao, hist.sq_ficha_hist_situacao_excluida
        FROM salve.ficha_hist_situacao_excluida hist
        WHERE hist.sq_ficha = p_sq_ficha
        ORDER BY hist.sq_ficha_hist_situacao_excluida desc limit 1;

        v_msg= '  - Histórico dos motivos das exclusões da ficha versionado';
        v_log = array_append(v_log,v_msg);
        RAISE NOTICE '%',v_msg;
    END IF;


    -- garar registro na tabela taxon_historico_avaliacao
    IF v_cd_contexto = 'PUBLICACAO' THEN

        -- se existir o ano a categoria e a justificativa, adicionar no taxon_historico_avaliacao se não existir
        if v_sq_categoria_validada is not null AND v_ds_justificativa_validada is not null then

            -- pegar o ano da data final da(s) oficina(s) de validação da mesma versão
            if v_nu_ano is null then
                SELECT Max(to_char( oficina_ficha.dt_avaliacao,'yyyy'))::int into v_nu_ano
                FROM salve.oficina_ficha_versao a
                         INNER JOIN salve.oficina_ficha ON oficina_ficha.sq_oficina_ficha = a.sq_oficina_ficha
                         INNER JOIN salve.oficina ON oficina.sq_oficina = oficina_ficha.sq_oficina
                         INNER JOIN salve.dados_apoio AS tipo ON tipo.sq_dados_apoio = oficina.sq_tipo_oficina
                WHERE a.sq_ficha_versao = v_sq_ficha_versao
                  AND tipo.cd_sistema = 'OFICINA_VALIDACAO';
                v_msg = format( '  - Ano da validação lido da ultima oficina de validação: %s',v_nu_ano);
                v_log = array_append(v_log,v_msg);
                RAISE NOTICE '%',v_msg;
            END IF;

            if v_nu_ano is not null then
                SELECT dados_apoio.sq_dados_apoio into v_sq_avaliacao_naciona_brasil
                FROM salve.dados_apoio
                         INNER JOIN salve.dados_apoio pai ON pai.sq_dados_apoio = dados_apoio.sq_dados_apoio_pai
                WHERE pai.cd_sistema = 'TB_TIPO_AVALIACAO'
                  AND dados_apoio.cd_sistema = 'NACIONAL_BRASIL'
                LIMIT 1;

                if not exists( select null from salve.taxon_historico_avaliacao th
                               where th.sq_taxon = v_sq_taxon
                                 and th.nu_ano_avaliacao = v_nu_ano
                                 and th.sq_tipo_avaliacao = v_sq_avaliacao_naciona_brasil
                                 and th.sq_categoria_iucn = v_sq_categoria_validada
                                 and coalesce( th.de_criterio_avaliacao_iucn,'') = coalesce( v_ds_criterio_validado,'')
                                 and coalesce(th.st_possivelmente_extinta,'') = coalesce(v_pex_validado,'')
                                 and th.tx_justificativa_avaliacao = v_ds_justificativa_validada
                               limit 1
                    ) then
                    INSERT INTO salve.taxon_historico_avaliacao ( sq_taxon,  nu_ano_avaliacao, sq_tipo_avaliacao
                                                                , sq_categoria_iucn, de_criterio_avaliacao_iucn
                                                                , st_possivelmente_extinta, tx_justificativa_avaliacao)
                    values ( v_sq_taxon, v_nu_ano, v_sq_avaliacao_naciona_brasil,v_sq_categoria_validada, v_ds_criterio_validado, v_pex_validado, v_ds_justificativa_validada)
                    RETURNING sq_taxon_historico_avaliacao into v_sq_taxon_hist_aval;
                    v_msg =  format('  - Criado registro na tabela Taxon Historico Avaliação (%s)', v_sq_taxon_hist_aval) ;
                    v_log = array_append(v_log,v_msg);
                    RAISE NOTICE '%',v_msg;
                    IF v_sq_taxon_hist_aval IS NOT NULL THEN
                        insert into salve.taxon_historico_avaliacao_versao(sq_taxon_historico_avaliacao, sq_ficha_versao) values (v_sq_taxon_hist_aval,v_sq_ficha_versao);
                    END IF;
                END IF;
            END IF;
        END IF;
    END IF;

    -- se a ficha do 2º ciclo ainda não estiver versionada, gerar a versão
    if v_versionar_ficha_ciclo_2 then
        v_msg = format( '  - iniciando o versionamento da ficha do 2º ciclo PUBLICADA e com o mesmo táxon');
        v_log = array_append(v_log,v_msg);
        RAISE NOTICE '%',v_msg;
    END IF;

    v_msg = format( '  - FIM versionamento da ficha %s as %s',p_sq_ficha, current_timestamp);
    v_log = array_append(v_log,v_msg);
    RAISE NOTICE '%',v_msg;


    -- CANCELAR TUDO - PARA TESTE APENAS
    -- RAISE EXCEPTION 'Teste fn_versionar_ficha_ciclo_1';

    -- registrar log
    DELETE FROM salve.ficha_versionamento where sq_ficha = p_sq_ficha;
    INSERT INTO salve.ficha_versionamento (sq_ficha, in_status, ds_log, dt_inclusao) VALUES (p_sq_ficha, 0, array_to_string(v_log,chr(13)), current_timestamp);


    -- se a ficha do 2º ciclo ainda não estiver versionada, gerar a versão
    if v_versionar_ficha_ciclo_2 then
        if not exists( select null from salve.ficha_versao
                       where sq_ficha = v_sq_ficha_ciclo_2
                         and sq_ficha_versao <> v_sq_ficha_versao LIMIT 1) then
            v_resultado = salve.fn_versionar_ficha_ciclo_2(v_sq_ficha_ciclo_2,p_sq_pessoa,p_sq_contexto);
        end if;
    END IF;


    return json_build_object('status',0,'msg', 'Ficha versionada com SUCESSO','type','success','log',array_to_string(v_log,chr(13)) );

EXCEPTION WHEN OTHERS THEN
    RAISE NOTICE '  - ERRO: %', SQLERRM;
    get stacked diagnostics
        v_context = pg_exception_context;
    raise notice E'Got exception:
        context: %', v_context;
    return json_build_object('status',1,'type','error','msg', concat('Mensagem de erro: ',SQLERRM,CHR(13),v_context),'log',array_to_string(v_log,chr(13)) );
END
$$;


--
-- TOC entry 1812 (class 1255 OID 29621)
-- Name: fn_versionar_ficha_ciclo_2(bigint, bigint, bigint); Type: FUNCTION; Schema: salve; Owner: -
--

CREATE FUNCTION salve.fn_versionar_ficha_ciclo_2(p_sq_ficha bigint, p_sq_pessoa bigint, p_sq_contexto bigint) RETURNS json
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_context text;
    v_msg TEXT;
    v_log text ARRAY;
    -- -----------------
    v_nu_versao_atual       integer;
    v_publicar boolean = false;
    v_sq_taxon bigint;
    v_sq_ficha_versao bigint;
    v_sq_taxon_hist_aval bigint;
    v_cd_contexto text;
    v_cd_categoria_hist text;
    v_sq_categoria_avaliada bigint;
    v_sq_categoria_validada bigint;
    v_cd_categoria_avaliada text;
    v_cd_categoria_validada text;
    v_ds_criterio_validado text;
    v_pex_validado text;
    v_nu_ano integer;
    v_sq_nacional_brasil bigint;
    v_sq_situacao_utilizado bigint; -- registros utilizado na avaliacao
    v_sq_situacao_apos_avaliacao bigint; -- registro adicionado apos a avaliação
    v_sq_situacao_publicada bigint;
    v_sq_situacao_compilacao bigint;

BEGIN

    -- FICHAS DO 2º CICLO CALCULAM A PROXIMA VERSAO
    v_nu_versao_atual = ( SELECT COALESCE(MAX(nu_versao), 0) FROM salve.ficha_versao WHERE sq_ficha = p_sq_ficha);
    v_msg = format('  - Versão atual: %s',v_nu_versao_atual);
    v_log = array_append(v_log,v_msg);
    RAISE NOTICE '%',v_msg;

    -- ler o contexto do versionamento: Publicação, Exclusão etc
    v_cd_contexto = ( SELECT cd_sistema FROM salve.dados_apoio where sq_dados_apoio = p_sq_contexto);
    v_msg = format('  - Contexto: %s',v_cd_contexto);
    v_log = array_append(v_log,v_msg);
    RAISE NOTICE '%',v_msg;

    -- ler o taxon da ficha
    SELECT sq_taxon INTO v_sq_taxon FROM salve.ficha WHERE sq_ficha = p_sq_ficha;
    v_msg= format('  - Taxon (%s)', v_sq_taxon);


    /************************************************************************************************
    REGRAS PARA PUBLICAR AUTOMATICAMENTE APÓS O VERSIONAMENTO
        - ficha tem que ser do segundo ciclo
        - se a ficha era ameaçada (EN, VU, CR) e permanece ameaçada na MESMA CATEGORIA pode publicar
        - se a ficha era não ameaçada e PERMANECE não ameaçada pode publicar
        - se a ficha era ameaçada e permanece ameaçada em outra categoria NÃO pode publicar
        - demais situações (entrada e saida da lista oficial ) não pode publicar
    **************************************************************************************************/

    IF v_cd_contexto = 'PUBLICACAO' THEN
        -- ler as categorias avaliada e validada
        v_msg = '  - Ler categorias Avaliada e Validada para aplicar as regras da publicação automática';
        v_log = array_append(v_log,v_msg);
        RAISE NOTICE '%',v_msg;

        SELECT ficha.sq_categoria_iucn, ficha.sq_categoria_final
             , ficha.st_possivelmente_extinta_final, cat_avaliada.cd_sistema, cat_validada.cd_sistema
        INTO v_sq_categoria_avaliada, v_sq_categoria_validada, v_pex_validado, v_cd_categoria_avaliada, v_cd_categoria_validada
        FROM salve.ficha
                 LEFT JOIN salve.dados_apoio as cat_avaliada on cat_avaliada.sq_dados_apoio = ficha.sq_categoria_iucn
                 LEFT JOIN salve.dados_apoio as cat_validada on cat_validada.sq_dados_apoio = ficha.sq_categoria_final
        WHERE ficha.sq_ficha = p_sq_ficha;

        v_msg= format('  - Categoria Avaliada (%s) %s', v_cd_categoria_avaliada, v_sq_categoria_avaliada);
        v_log = array_append(v_log,v_msg);
        RAISE NOTICE '%',v_msg;

        v_msg= format('  - Categoria Validada (%s) %s', v_cd_categoria_validada, v_sq_categoria_validada);
        v_log = array_append(v_log,v_msg);
        RAISE NOTICE '%',v_msg;

        v_msg= format('  - Critério (%s)', v_ds_criterio_validado);
        v_log = array_append(v_log,v_msg);
        RAISE NOTICE '%',v_msg;

        v_msg= format('  - PEX: %s', v_pex_validado);
        v_log = array_append(v_log,v_msg);
        RAISE NOTICE '%',v_msg;

        v_msg= format('  - Ano aceite validação: %s', v_nu_ano);
        v_log = array_append(v_log,v_msg);
        RAISE NOTICE '%',v_msg;

        IF v_sq_categoria_validada IS NULL THEN
            -- RAISE EXCEPTION 'Ficha não está validada (ABA 11.2 em branco)';
        END IF;

        -- encontrar a categoria atual no historico
        select a.sq_dados_apoio into v_sq_nacional_brasil from salve.dados_apoio a
                                                                   inner join salve.dados_apoio pai on pai.sq_dados_apoio = a.sq_dados_apoio_pai
        where a.cd_sistema = 'NACIONAL_BRASIL'
          and pai.cd_sistema = 'TB_TIPO_AVALIACAO'
        LIMIT 1;
        v_msg = format( '  - Encontrado tipo NACIONAL BRASIL (%s)', v_sq_nacional_brasil);
        v_log = array_append(v_log,v_msg);
        RAISE NOTICE '%',v_msg;

        select cat.cd_sistema into v_cd_categoria_hist from salve.taxon_historico_avaliacao h
                                                                inner join salve.dados_apoio cat on cat.sq_dados_apoio = h.sq_categoria_iucn
        where h.sq_taxon = v_sq_taxon
          and h.sq_tipo_avaliacao = v_sq_nacional_brasil
        order by h.nu_ano_avaliacao desc, h.sq_taxon_historico_avaliacao desc limit 1;
        v_msg = format( '  - Categoria historico atual (%s)',v_cd_categoria_hist);
        v_log = array_append(v_log,v_msg);
        RAISE NOTICE '%',v_msg;

        if v_cd_categoria_hist in ( 'CR','VU','EN') then
            -- AMEAÇADA
            if v_cd_categoria_validada = v_cd_categoria_hist then
                -- PERMENECE AMEACADA NA MESMA CATEGORIA
                v_publicar=true;
            END IF;

        ELSE
            -- NÃO AMEAÇADA
            if not v_cd_categoria_validada in ( 'CR','VU','EN') then
                -- PERMANECE NÃO AMEAÇADA
                v_publicar=true;
            END IF;

        END IF;
        v_msg =   format('  - Publicar a versão? (%s)',v_publicar);
        v_log = array_append(v_log,v_msg);
        RAISE NOTICE '%',v_msg;

    END IF; -- FIM CONTEXTO = PUBLICACAO

    -- se a versão já existir apenas fazer o update do json
    v_sq_ficha_versao=null;

    -- verificar se ja tem a versão criada
    select sq_ficha_versao into v_sq_ficha_versao from salve.ficha_versao
    where sq_ficha = p_sq_ficha
      and nu_versao = (v_nu_versao_atual+1) limit 1;

    if v_sq_ficha_versao is null then
        INSERT INTO salve.ficha_versao (sq_ficha, nu_versao, js_ficha, sq_usuario_inclusao, st_publico, sq_contexto)
        SELECT p_sq_ficha,
               (v_nu_versao_atual + 1),
               salve.fn_ficha2json(p_sq_ficha)
                ,
               p_sq_pessoa
                ,
               v_publicar
                ,
               p_sq_contexto
        RETURNING sq_ficha_versao INTO v_sq_ficha_versao;

        v_msg =  format('  - Versão json CRIADA id: %s', v_sq_ficha_versao);
        v_log = array_append(v_log,v_msg);
        RAISE NOTICE '%',v_msg;

        -- garantir somente uma versão publicada
        IF v_publicar = TRUE THEN
            UPDATE salve.ficha_versao
            SET st_publico = FALSE
            WHERE sq_ficha = p_sq_ficha
              AND st_publico = TRUE
              AND nu_versao <> (v_nu_versao_atual+1);

            v_msg = '  - Executado update para garantir somente uma versao publicada';
            v_log = array_append(v_log,v_msg);
            RAISE NOTICE '%',v_msg;

        END IF;

    ELSE
        update salve.ficha_versao set js_ficha = salve.fn_ficha2json(p_sq_ficha)
                                    ,sq_usuario_inclusao = p_sq_pessoa
                                    ,dt_inclusao = current_timestamp
        where sq_ficha_versao = v_sq_ficha_versao;

        v_msg = format('  - Versão json ATUALIZADA id: %s', v_sq_ficha_versao);
        v_log = array_append(v_log,v_msg);
        RAISE NOTICE '%',v_msg;

    END IF;

    -- versionar / atualizar o versionamento dos registros de ocorrencia
    if not exists ( select null from salve.ocorrencias_json where sq_ficha_versao = v_sq_ficha_versao limit 1) then
        INSERT INTO salve.ocorrencias_json (sq_ficha_versao, js_ocorrencias)  SELECT v_sq_ficha_versao, salve.fn_ocorrencias2json(p_sq_ficha);

        v_msg = '  - Ocorrências versionadas';
        v_log = array_append(v_log,v_msg);
        RAISE NOTICE '%',v_msg;
    ELSE
        UPDATE salve.ocorrencias_json set js_ocorrencias = salve.fn_ocorrencias2json(p_sq_ficha) where sq_ficha_versao = v_sq_ficha_versao;

        v_msg =  '  - Ocorrências versionadas atualizadas';
        v_log = array_append(v_log,v_msg);
        RAISE NOTICE '%',v_msg;

    END IF;


    -- bloquear as consultas da ficha
    INSERT INTO salve.ciclo_consulta_ficha_versao(sq_ficha_versao, sq_ciclo_consulta_ficha)
    SELECT v_sq_ficha_versao, ciclo_consulta_ficha.sq_ciclo_consulta_ficha
    FROM salve.ciclo_consulta_ficha
    WHERE sq_ficha = p_sq_ficha
      AND NOT EXISTS(
            SELECT NULL FROM salve.ciclo_consulta_ficha_versao x WHERE x.sq_ciclo_consulta_ficha = ciclo_consulta_ficha.sq_ciclo_consulta_ficha LIMIT 1
        );

    v_msg =  '  - Consultas/Revisões bloqueadas pelo versionamento';
    v_log = array_append(v_log,v_msg);
    RAISE NOTICE '%',v_msg;


    -- bloquear as oficinas
    insert into salve.oficina_ficha_versao( sq_ficha_versao, sq_oficina_ficha)
    select v_sq_ficha_versao, oficina_ficha.sq_oficina_ficha
    from salve.oficina_ficha
    where oficina_ficha.sq_ficha = p_sq_ficha
      AND NOT EXISTS (
            SELECT NULL FROM salve.oficina_ficha_versao x where x.sq_oficina_ficha = oficina_ficha.sq_oficina_ficha limit 1
        );

    v_msg= '  - Oficinas bloqueadas pelo versionamento';
    v_log = array_append(v_log,v_msg);
    RAISE NOTICE '%',v_msg;

    -- bloquear as colaborações no gride das ocorrencias (Concordo/Discordo) durante as consultas
    insert into salve.ficha_ocorrencia_validacao_versao( sq_ficha_versao, sq_ficha_ocorrencia_validacao)
    select v_sq_ficha_versao, fov.sq_ficha_ocorrencia_validacao
    from salve.ficha_ocorrencia_validacao fov
             inner join salve.ficha_ocorrencia fo on fo.sq_ficha_ocorrencia = fov.sq_ficha_ocorrencia
    where fo.sq_ficha = p_sq_ficha and not exists (
            select null from salve.ficha_ocorrencia_validacao_versao x where x.sq_ficha_ocorrencia_validacao = fov.sq_ficha_ocorrencia_validacao limit 1
        );

    v_msg = '  - Colaborações Concordo/Discordo do gride de registros SALVE da consulta bloqueadas pelo versionamento';
    v_log = array_append(v_log,v_msg);
    RAISE NOTICE '%',v_msg;


    -- bloquear as colaborações nas ocorrencias do portalbio
    insert into salve.ficha_ocorrencia_validacao_versao( sq_ficha_versao, sq_ficha_ocorrencia_validacao)
    select v_sq_ficha_versao, fov.sq_ficha_ocorrencia_validacao
    from salve.ficha_ocorrencia_validacao fov
             inner join salve.ficha_ocorrencia_portalbio fo on fo.sq_ficha_ocorrencia_portalbio = fov.sq_ficha_ocorrencia_portalbio
    where fo.sq_ficha = p_sq_ficha and not exists (
            select null from salve.ficha_ocorrencia_validacao_versao x where x.sq_ficha_ocorrencia_validacao = fov.sq_ficha_ocorrencia_validacao limit 1
        );

    v_msg = '  - Colaborações Concordo/Discordo do gride de registros PORTALBIO da consulta bloqueadas pelo versionamento';
    v_log = array_append(v_log,v_msg);
    RAISE NOTICE '%',v_msg;

    -- bloquear ficha_hist_situacao_excluida
    IF v_cd_contexto = 'EXCLUSAO_OFICINA' or v_cd_contexto = 'EXCLUSAO_LOTE' or v_cd_contexto = 'EXCLUSAO_FICHA' THEN
        INSERT INTO salve.ficha_hist_sit_exc_versao( sq_ficha_versao, sq_ficha_hist_situacao_excluida)
        SELECT v_sq_ficha_versao, hist.sq_ficha_hist_situacao_excluida
        FROM salve.ficha_hist_situacao_excluida hist
        WHERE hist.sq_ficha = p_sq_ficha
        ORDER BY hist.sq_ficha_hist_situacao_excluida desc limit 1;

        v_msg = '  - Histórico dos motivos das exclusões da ficha versionado';
        v_log = array_append(v_log,v_msg);
        RAISE NOTICE '%',v_msg;

    END IF;

    if v_cd_contexto = 'PUBLICACAO' then
        /*
        - CALCULAR O ANO DA AVALIAÇAO OU DA VALIDAÇÃO - VER REGRAS
        - ANO da gravação da aba 7.1 deve ser calculado com a seguinte regra:
        - aba 11.6 = aba 11.7 -> ano da gravação da justificativa da avaliação
        - aba 11.6 <> aba 11.7 -> ano data da validação da ficha
        */
        if v_sq_categoria_avaliada is not null and v_sq_categoria_validada is not null then
            if v_sq_categoria_avaliada = v_sq_categoria_validada then
                RAISE NOTICE '  - Categoria Aba 11.6 = aba 11.7 -> pegar o ano da ÚLTIMA avaliação';
                v_nu_ano = ( SELECT Max(to_char(oficina_ficha.dt_avaliacao,'yyyy'))::int
                             FROM salve.oficina_ficha_versao a
                                      INNER JOIN salve.oficina_ficha ON oficina_ficha.sq_oficina_ficha = a.sq_oficina_ficha
                                      INNER JOIN salve.oficina ON oficina.sq_oficina = oficina_ficha.sq_oficina
                                      INNER JOIN salve.dados_apoio AS tipo ON tipo.sq_dados_apoio = oficina.sq_tipo_oficina
                             WHERE a.sq_ficha_versao = v_sq_ficha_versao
                               AND tipo.cd_sistema = 'OFICINA_AVALIACAO');

                v_msg = format( '  - Ano lido da ultima oficina de avaliação: %s',v_nu_ano);
                v_log = array_append(v_log,v_msg);
                RAISE NOTICE '%',v_msg;
            ELSE
                v_nu_ano = (SELECT to_char(x.dt_aceite_validacao,'yyyy')::int from salve.ficha x where x.sq_ficha = p_sq_ficha);

                v_msg = format('  - Ano lido da ficha dt_aceite_validacao: %s',v_nu_ano);
                v_log = array_append(v_log,v_msg);
                RAISE NOTICE '%',v_msg;
            END IF;

            if v_nu_ano > 0 then
                -- criar historico se não existir ainda
                with dados_historico as (SELECT ficha.sq_taxon
                                              , v_nu_ano as nu_ano -- TO_CHAR(NOW(), 'YYYY')::int as nu_ano_avaliacao
                                              , v_sq_nacional_brasil as sq_tipo_avaliacao
                                              , ficha.sq_categoria_final
                                              , ficha.ds_criterio_aval_iucn_final
                                              , ficha.st_possivelmente_extinta_final
                                              , ficha.ds_justificativa_final
                                         FROM salve.ficha
                                         WHERE sq_ficha = p_sq_ficha
                )
                INSERT INTO salve.taxon_historico_avaliacao ( sq_taxon,  nu_ano_avaliacao, sq_tipo_avaliacao
                                                            , sq_categoria_iucn, de_criterio_avaliacao_iucn
                                                            , st_possivelmente_extinta, tx_justificativa_avaliacao)
                select dh.sq_taxon,dh.nu_ano,dh.sq_tipo_avaliacao
                     ,dh.sq_categoria_final
                     ,dh.ds_criterio_aval_iucn_final
                     ,dh.st_possivelmente_extinta_final
                     ,dh.ds_justificativa_final
                from dados_historico dh
                where NOT exists (
                        select null from salve.taxon_historico_avaliacao th
                        where th.sq_taxon = dh.sq_taxon
                          and th.nu_ano_avaliacao = dh.nu_ano
                          and th.sq_tipo_avaliacao = dh.sq_tipo_avaliacao
                          and th.sq_categoria_iucn = dh.sq_categoria_final
                          and coalesce( th.de_criterio_avaliacao_iucn,'') = coalesce( dh.ds_criterio_aval_iucn_final,'')
                          and coalesce(th.st_possivelmente_extinta,'') = coalesce(dh.st_possivelmente_extinta_final,'')
                          and th.tx_justificativa_avaliacao = dh.ds_justificativa_final
                        limit 1
                    ) RETURNING sq_taxon_historico_avaliacao into v_sq_taxon_hist_aval;
                v_msg = format('  - Criado registro na tabela Taxon Historico Avaliação (%s)', v_sq_taxon_hist_aval) ;
                IF v_sq_taxon_hist_aval IS NOT NULL THEN
                    insert into salve.taxon_historico_avaliacao_versao(sq_taxon_historico_avaliacao, sq_ficha_versao) values (v_sq_taxon_hist_aval,v_sq_ficha_versao);
                END IF;
            END IF;
        END IF;
        -- Regras após o versionamento

        -- 1) Desmarcar "Mapa distribuicao principal" da ficha
        update salve.ficha_anexo set in_principal = 'N'
        FROM salve.ficha_anexo fa
                 INNER JOIN salve.dados_apoio as contexto ON contexto.sq_dados_apoio = fa.sq_contexto
        WHERE fa.sq_ficha = p_sq_ficha
          AND contexto.cd_sistema = 'MAPA_DISTRIBUICAO'
          AND fa.in_principal = 'S'
          and fa.sq_ficha_anexo = ficha_anexo.sq_ficha_anexo;

        select a.sq_dados_apoio into v_sq_situacao_utilizado from salve.dados_apoio a
                                                                      inner join salve.dados_apoio pai on pai.sq_dados_apoio = a.sq_dados_apoio_pai
        where a.cd_sistema = 'REGISTRO_UTILIZADO_AVALIACAO'
          and pai.cd_sistema = 'TB_SITUACAO_REGISTRO_OCORRENCIA'
        LIMIT 1;
        RAISE  NOTICE '  - Lendo situação registro utilizado (%)',v_sq_situacao_utilizado;

        select a.sq_dados_apoio into v_sq_situacao_apos_avaliacao from salve.dados_apoio a
                                                                           inner join salve.dados_apoio pai on pai.sq_dados_apoio = a.sq_dados_apoio_pai
        where a.cd_sistema = 'REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO'
          and pai.cd_sistema = 'TB_SITUACAO_REGISTRO_OCORRENCIA'
        LIMIT 1;
        RAISE  NOTICE '  - Lendo situação adicionado após avaliação (%)',v_sq_situacao_apos_avaliacao;

        -- registro "Adicionados após a avaliação" para "Utilizados na avaliação"
        update salve.ficha_ocorrencia
        set sq_situacao_avaliacao = v_sq_situacao_utilizado
        where sq_ficha = p_sq_ficha
          and sq_situacao_avaliacao = v_sq_situacao_apos_avaliacao;
        RAISE  NOTICE '  - Registros adicionado após avaliação convertidos para utilizados na avaliação';

        -- limpar dados da avaliação - aba 11, mudanca de categoria, citacao, autoria, colaboradores e equipe tecnica
        if not salve.fn_ficha_limpar_avaliacao(p_sq_ficha) then
            RAISE EXCEPTION 'Não foi possível limpar os dados da avaliação';
        END IF;

        -- se a ficha estiver PUBLICADA voltar para COMPILAÇÃO
        select a.sq_dados_apoio into v_sq_situacao_compilacao from salve.dados_apoio a
                                                                       inner join salve.dados_apoio pai on pai.sq_dados_apoio = a.sq_dados_apoio_pai
        where a.cd_sistema = 'COMPILACAO'
          and pai.cd_sistema = 'TB_SITUACAO_FICHA'
        LIMIT 1;
        select a.sq_dados_apoio into v_sq_situacao_publicada from salve.dados_apoio a
                                                                      inner join salve.dados_apoio pai on pai.sq_dados_apoio = a.sq_dados_apoio_pai
        where a.cd_sistema = 'PUBLICADA'
          and pai.cd_sistema = 'TB_SITUACAO_FICHA'
        LIMIT 1;
        update salve.ficha set sq_situacao_ficha = v_sq_situacao_compilacao
        Where ficha.sq_ficha = p_sq_ficha
          and ficha.sq_situacao_ficha = v_sq_situacao_publicada;

    END IF; -- fim if PUBLICACAO
    v_msg = format( '  - FIM versionamento da ficha %s as %s',p_sq_ficha, current_timestamp);
    v_log = array_append(v_log,v_msg);
    RAISE NOTICE '%',v_msg;

    -- registrar log
    DELETE FROM salve.ficha_versionamento where sq_ficha = p_sq_ficha;
    INSERT INTO salve.ficha_versionamento (sq_ficha, in_status, ds_log, dt_inclusao) VALUES (p_sq_ficha, 0, array_to_string(v_log,chr(13)), current_timestamp);


    -- RAISE EXCEPTION 'OK TUDO CERTO FICHA 2º CICLO';
    return json_build_object('status',0,'msg', 'Ficha versionada com SUCESSO','type','success','log',array_to_string(v_log,chr(13)) );

EXCEPTION WHEN OTHERS THEN
    RAISE NOTICE '  - ERRO: %', SQLERRM;
    get stacked diagnostics
        v_context = pg_exception_context;
    --- UPDATE SALVE.FICHA SET ds_diagnostico_morfologico = v_context WHERE SQ_FICHA=p_sq_ficha;
    raise notice E'Got exception:
            context: %', v_context;
    return json_build_object('status',1,'type','error','msg', concat('Mensagem de erro: ',SQLERRM,CHR(13),v_context),'log',array_to_string(v_log,chr(13)) );

END
$$;


--
-- TOC entry 1817 (class 1255 OID 29772)
-- Name: json_validadores(bigint, bigint); Type: FUNCTION; Schema: salve; Owner: -
--

CREATE FUNCTION salve.json_validadores(v_sq_ficha bigint, v_sq_oficina bigint) RETURNS text
    LANGUAGE sql
    AS $$
/**
testar: select salve.json_validadores(16729,57 );
        select salve.json_validadores(10064,114 );
*/
select
    coalesce(json_object_agg( coalesce( vw_pessoa.no_pessoa,''), json_build_object(
            'cd_situacao_convite',situacao_convite.cd_sistema
        ,'dt_ultimo_comunicado',to_char( validador_ficha.dt_ultimo_comunicado,'dd/mm/yyyy HH24:MI:SS')
        ,'dt_alteracao',case when validador_ficha.dt_alteracao is not null then to_char( validador_ficha.dt_alteracao,'dd/mm/yyyy HH24:MI:SS') else '' end
        ,'in_respondido', case when validador_ficha.sq_resultado is not null then 'S' else 'N' end
        ,'in_convite_vencido',case when ciclo_avaliacao_validador.dt_validade_convite > current_date then 'N' else 'S' end
        ,'in_comunicar_alteracao',case validador_ficha.in_comunicar_alteracao when true then 'S' else 'N' end
        ,'de_resposta', coalesce( resultado.ds_dados_apoio,'')
        ,'de_categoria_sugerida', case when categoria_sugerida.sq_dados_apoio is null then '' else categoria_sugerida.ds_dados_apoio||' ('|| categoria_sugerida.cd_dados_apoio ||')' end
        ,'de_criterio_sugerido', case when validador_ficha.de_criterio_sugerido is null then '' else validador_ficha.de_criterio_sugerido end
        ,'st_possivelmente_extinta',salve.snd( validador_ficha.st_possivelmente_extinta)
        ,'sq_revisao', coalesce( revisao.sq_validador_ficha_revisao,0)
        ,'de_resposta_revisao', coalesce( resultado_revisao.ds_dados_apoio,'')
        ,'de_categoria_revisao', case when categoria_revisao.sq_dados_apoio is null then '' else categoria_revisao.ds_dados_apoio||' ('|| categoria_revisao.cd_dados_apoio||')' end
        ,'de_criterio_revisao', case when revisao.de_criterio_sugerido is null then '' else revisao.de_criterio_sugerido end
        ,'st_possivelmente_extinta_revisao',salve.snd( revisao.st_possivelmente_extinta)
        ,'nu_chats',oficina_ficha_chat.nu_chats
        ,'sq_validador_ficha',validador_ficha.sq_validador_ficha
        )
                 )::text,'{}')

from salve.oficina_ficha
         inner join salve.validador_ficha on validador_ficha.sq_oficina_ficha = oficina_ficha.sq_oficina_ficha
         inner join salve.ciclo_avaliacao_validador on ciclo_avaliacao_validador.sq_ciclo_avaliacao_validador = validador_ficha.sq_ciclo_avaliacao_validador
         inner join salve.vw_pessoa on vw_pessoa.sq_pessoa = ciclo_avaliacao_validador.sq_validador
         left outer join salve.dados_apoio as situacao_convite on situacao_convite.sq_dados_apoio = ciclo_avaliacao_validador.sq_situacao_convite
         left outer join salve.dados_apoio as resultado on resultado.sq_dados_apoio= validador_ficha.sq_resultado
         left outer join salve.dados_apoio as categoria_sugerida on categoria_sugerida.sq_dados_apoio = validador_ficha.sq_categoria_sugerida
         left outer join salve.validador_ficha_revisao as revisao on revisao.sq_validador_ficha = validador_ficha.sq_validador_ficha
         left outer join salve.dados_apoio as resultado_revisao on resultado_revisao.sq_dados_apoio= revisao.sq_resultado
         left outer join salve.dados_apoio as categoria_revisao on categoria_revisao.sq_dados_apoio = revisao.sq_categoria_sugerida
         LEFT JOIN lateral (
    select count(x.sq_oficina_ficha_chat) as nu_chats from salve.oficina_ficha_chat x
    where x.sq_oficina_ficha = oficina_ficha.sq_oficina_ficha
      and x.sq_pessoa = ciclo_avaliacao_validador.sq_validador
    ) oficina_ficha_chat on true
where oficina_ficha.sq_ficha = v_sq_ficha
  and oficina_ficha.sq_oficina = v_sq_oficina


$$;


--
-- TOC entry 7889 (class 0 OID 0)
-- Dependencies: 1817
-- Name: FUNCTION json_validadores(v_sq_ficha bigint, v_sq_oficina bigint); Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON FUNCTION salve.json_validadores(v_sq_ficha bigint, v_sq_oficina bigint) IS 'Funcao para recuperar as respostas dos validadores e a revisao no formato json';


--
-- TOC entry 1780 (class 1255 OID 28773)
-- Name: remover_html_tags(text); Type: FUNCTION; Schema: salve; Owner: -
--

CREATE FUNCTION salve.remover_html_tags(text) RETURNS text
    LANGUAGE sql
    AS $_$
SELECT regexp_replace(regexp_replace($1, E'<.*?>', '', 'g' ), E'&nbsp;', ' ', 'g')
$_$;


--
-- TOC entry 7890 (class 0 OID 0)
-- Dependencies: 1780
-- Name: FUNCTION remover_html_tags(text); Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON FUNCTION salve.remover_html_tags(text) IS 'Função para remover as tags HTML do texto.';


--
-- TOC entry 1781 (class 1255 OID 28774)
-- Name: snd(text); Type: FUNCTION; Schema: salve; Owner: -
--

CREATE FUNCTION salve.snd(text) RETURNS text
    LANGUAGE sql
    AS $_$
SELECT case when $1='S' then 'Sim' else
    case when $1='N' then 'Não' else
        case when $1='D' then 'Desconhecido' else '' end
        end
           end
$_$;


--
-- TOC entry 7891 (class 0 OID 0)
-- Dependencies: 1781
-- Name: FUNCTION snd(text); Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON FUNCTION salve.snd(text) IS 'Função retornar os valores S,N ou D em Sim, Não ou Desconhecido.';


--
-- TOC entry 1798 (class 1255 OID 29276)
-- Name: criar_trilha(bigint); Type: FUNCTION; Schema: taxonomia; Owner: -
--

CREATE FUNCTION taxonomia.criar_trilha(v_sq_taxon bigint) RETURNS json
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_trilha json;
BEGIN
    WITH RECURSIVE subir_arvore_taxon(
                                      nivel,
                                      key,
                                      ds_caminho,
                                      sq_taxon,
                                      in_ocorre_brasil,
                                      sq_nivel_taxonomico,
                                      no_autor, no_taxon,
                                      nu_ano,
                                      sq_situacao_nome,
                                      st_ativo,
                                      sq_taxon_pai)
                       AS (
            SELECT 1::integer as nivel, taxon.sq_taxon as key, taxonomia.formatar_nome(taxon.sq_taxon)::text as ds_caminho, taxon.sq_taxon, taxon.in_ocorre_brasil,
                   taxon.sq_nivel_taxonomico, taxon.no_autor,  taxonomia.formatar_nome(taxon.sq_taxon) as no_taxon
                    , taxon.nu_ano, taxon.sq_situacao_nome, taxon.st_ativo, taxon.sq_taxon_pai
            FROM taxonomia.taxon
            where taxon.sq_taxon  = v_sq_taxon
            UNION
            SELECT t.nivel+1::integer as nivel, taxon.sq_taxon as key,
                   (t.ds_caminho::text) ||' < ' ||taxonomia.formatar_nome(taxon.sq_taxon)::text as ds_caminho,
                   taxon.sq_taxon, taxon.in_ocorre_brasil, taxon.sq_nivel_taxonomico, taxon.no_autor , taxonomia.formatar_nome(taxon.sq_taxon) as no_taxon ,
                   taxon.nu_ano, taxon.sq_situacao_nome, taxon.st_ativo, taxon.sq_taxon_pai
            FROM taxonomia.taxon
                     JOIN  subir_arvore_taxon t  on taxon.sq_taxon = t.sq_taxon_pai
        )
    SELECT json_object_agg(lower(nivel_taxonomico.co_nivel_taxonomico), json_build_object(
            'sq_taxon',  t.sq_taxon,
            'no_taxon', t.no_taxon,
            'sq_taxon_pai', t.sq_taxon_pai,
            'st_ativo', t.st_ativo,
            'in_ocorre_brasil', t.in_ocorre_brasil,
            'sq_nivel_taxonomico', t.sq_nivel_taxonomico,
            'de_nivel_taxonomico', nivel_taxonomico.de_nivel_taxonomico,
            'nu_grau_taxonomico', nivel_taxonomico.nu_grau_taxonomico,
            'in_nivel_intermediario', nivel_taxonomico.in_nivel_intermediario,
            'co_nivel_taxonomico', nivel_taxonomico.co_nivel_taxonomico
        )
               )

               as trilha into v_trilha
    FROM subir_arvore_taxon t
             inner join taxonomia.nivel_taxonomico
                        on nivel_taxonomico.sq_nivel_taxonomico = t.sq_nivel_taxonomico;

    return v_trilha;
END;
$$;


--
-- TOC entry 1799 (class 1255 OID 29277)
-- Name: criar_trilha2(bigint); Type: FUNCTION; Schema: taxonomia; Owner: -
--

CREATE FUNCTION taxonomia.criar_trilha2(v_sq_taxon bigint) RETURNS json
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_trilha json;
BEGIN
    select array_to_json(array_agg(row_to_json(tb1))) into v_trilha
    from (
             WITH RECURSIVE subir_arvore_taxon(
                                               nivel,
                                               key,
                                               ds_caminho,
                                               sq_taxon,
                                               in_ocorre_brasil,
                                               sq_nivel_taxonomico,
                                               no_autor, no_taxon,
                                               nu_ano,
                                               sq_situacao_nome,
                                               st_ativo,
                                               sq_taxon_pai,
                                               sq_taxon_alvo,
                                               nome_cientifico)
                                AS (
                     SELECT 1::integer                                    as nivel,
                            taxon.sq_taxon                                as key,
                            taxonomia.formatar_nome(taxon.sq_taxon)::text as ds_caminho,
                            taxon.sq_taxon,
                            taxon.in_ocorre_brasil,
                            taxon.sq_nivel_taxonomico,
                            taxon.no_autor,
                            taxonomia.formatar_nome(taxon.sq_taxon)       as no_taxon,
                            taxon.nu_ano,
                            taxon.sq_situacao_nome,
                            taxon.st_ativo,
                            taxon.sq_taxon_pai,
                            sq_taxon                                      as sq_taxon_alvo,
                            taxonomia.formatar_nome(sq_taxon)             as nome_cientifico
                     FROM taxonomia.taxon
                     where taxon.sq_taxon = v_sq_taxon
                     UNION
                     SELECT t.nivel + 1::integer                                                           as nivel,
                            taxon.sq_taxon                                                                 as key,
                            (t.ds_caminho::text) || ' < ' ||
                            taxonomia.formatar_nome(taxon.sq_taxon)::text                                  as ds_caminho,
                            taxon.sq_taxon,
                            taxon.in_ocorre_brasil,
                            taxon.sq_nivel_taxonomico,
                            taxon.no_autor,
                            taxonomia.formatar_nome(taxon.sq_taxon)                                        as no_taxon,
                            taxon.nu_ano,
                            taxon.sq_situacao_nome,
                            taxon.st_ativo,
                            taxon.sq_taxon_pai,
                            t.sq_taxon_alvo,
                            taxonomia.formatar_nome(taxon.sq_taxon)                                        as nome_cientifico
                     FROM taxonomia.taxon
                              JOIN subir_arvore_taxon t on taxon.sq_taxon = t.sq_taxon_pai
                 )
             SELECT t.nivel as nivel_recursivo,
                    t.key,
                    t.ds_caminho,
                    t.sq_taxon,
                    t.in_ocorre_brasil,
                    t.sq_nivel_taxonomico,
                    t.no_autor,
                    no_taxon,
                    t.nu_ano,
                    t.sq_situacao_nome,
                    t.st_ativo,
                    t.sq_taxon_pai,
                    t.sq_taxon_alvo,
                    t.nome_cientifico,
                    nivel_taxonomico.de_nivel_taxonomico,
                    nivel_taxonomico.nu_grau_taxonomico,
                    nivel_taxonomico.in_nivel_intermediario,
                    nivel_taxonomico.co_nivel_taxonomico
             FROM subir_arvore_taxon t
                      inner join taxonomia.nivel_taxonomico
                                 on nivel_taxonomico.sq_nivel_taxonomico = t.sq_nivel_taxonomico
         ) tb1;


    return v_trilha;
END;
$$;


--
-- TOC entry 1797 (class 1255 OID 29275)
-- Name: formatar_nome(bigint); Type: FUNCTION; Schema: taxonomia; Owner: -
--

CREATE FUNCTION taxonomia.formatar_nome(v_sq_taxon bigint) RETURNS text
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_nome_cientifico text := '';
BEGIN
    WITH RECURSIVE nome_cientifico(sq_taxon_pai,
                                   nivel,
                                   formatacao_nome,
                                   sq_taxon,
                                   sq_taxon_search,
                                   no_taxon,
                                   in_ocorre_brasil,
                                   sq_nivel_taxonomico,
                                   no_autor,
                                   nu_ano,
                                   sq_situacao_nome,
                                   st_ativo,
                                   co_nivel_taxonomico,
                                   de_nivel_taxonomico,
                                   in_nivel_intermediario,
                                   nu_grau_taxonomico
        )
                       AS (
            SELECT taxon.sq_taxon_pai, 1::integer as nivel,  taxon.no_taxon::text as formatacao_nome,
                   taxon.sq_taxon,
                   taxon.sq_taxon as sq_taxon_search,
                   taxon.no_taxon,
                   taxon.in_ocorre_brasil, taxon.sq_nivel_taxonomico, taxon.no_autor,
                   taxon.nu_ano, taxon.sq_situacao_nome, taxon.st_ativo,
                   nivel_taxonomico.co_nivel_taxonomico,
                   nivel_taxonomico.de_nivel_taxonomico,
                   nivel_taxonomico.in_nivel_intermediario,
                   nivel_taxonomico.nu_grau_taxonomico
            FROM taxonomia.taxon
                     INNER JOIN taxonomia.nivel_taxonomico ON nivel_taxonomico.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
            WHERE taxon.sq_taxon = v_sq_taxon
            UNION
            SELECT  taxon.sq_taxon_pai,
                    t.nivel+1::integer as nivel,
                    taxon.no_taxon::text  ||' ' || t.formatacao_nome as formatacao_nome,
                    taxon.sq_taxon,
                    t.sq_taxon_search,
                    taxon.no_taxon as no_taxon,
                    taxon.in_ocorre_brasil,
                    taxon.sq_nivel_taxonomico,
                    taxon.no_autor ,
                    taxon.nu_ano,
                    taxon.sq_situacao_nome,
                    taxon.st_ativo,
                    nivel_taxonomico.co_nivel_taxonomico,
                    nivel_taxonomico.de_nivel_taxonomico,
                    nivel_taxonomico.in_nivel_intermediario,
                    nivel_taxonomico.nu_grau_taxonomico
            FROM taxonomia.taxon
                     INNER JOIN taxonomia.nivel_taxonomico
                                ON nivel_taxonomico.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
                                    AND nivel_taxonomico.nu_grau_taxonomico in (100,90,80)
                     INNER JOIN  nome_cientifico t on taxon.sq_taxon = t.sq_taxon_pai
        )
    SELECT t.formatacao_nome into v_nome_cientifico
    FROM nome_cientifico t
    WHERE t.nivel = (select max(t2.nivel) from nome_cientifico t2 Where t2.sq_taxon_search =t.sq_taxon_search);
    return v_nome_cientifico;
END;
$$;


SET default_tablespace = '';

--
-- TOC entry 262 (class 1259 OID 22932)
-- Name: nivel_taxonomico; Type: TABLE; Schema: taxonomia; Owner: -
--

CREATE TABLE taxonomia.nivel_taxonomico (
    sq_nivel_taxonomico integer NOT NULL,
    de_nivel_taxonomico text NOT NULL,
    nu_grau_taxonomico integer NOT NULL,
    in_nivel_intermediario boolean DEFAULT false NOT NULL,
    co_nivel_taxonomico text NOT NULL,
    CONSTRAINT chk_nivel_taxonomico_nivel CHECK ((in_nivel_intermediario = ANY (ARRAY[true, false])))
);


--
-- TOC entry 7892 (class 0 OID 0)
-- Dependencies: 262
-- Name: COLUMN nivel_taxonomico.nu_grau_taxonomico; Type: COMMENT; Schema: taxonomia; Owner: -
--

COMMENT ON COLUMN taxonomia.nivel_taxonomico.nu_grau_taxonomico IS 'Squencia numérica que identifica a ordem hierárquica entre os níveis taxonômicos 10,20,30,40...';


--
-- TOC entry 7893 (class 0 OID 0)
-- Dependencies: 262
-- Name: COLUMN nivel_taxonomico.in_nivel_intermediario; Type: COMMENT; Schema: taxonomia; Owner: -
--

COMMENT ON COLUMN taxonomia.nivel_taxonomico.in_nivel_intermediario IS 'Identifica se o nivel taxonômico é um nivel fora da sequência padrão REFICOFAGE';


--
-- TOC entry 7894 (class 0 OID 0)
-- Dependencies: 262
-- Name: COLUMN nivel_taxonomico.co_nivel_taxonomico; Type: COMMENT; Schema: taxonomia; Owner: -
--

COMMENT ON COLUMN taxonomia.nivel_taxonomico.co_nivel_taxonomico IS 'Código do nível taxonômico sem acento e em caixa alta para ser utilizado como constantes nas consultas no lugar de do sq_nivel e nu_ordem_taxonomica';


--
-- TOC entry 261 (class 1259 OID 22930)
-- Name: nivel_taxonomico_sq_nivel_taxonomico_seq; Type: SEQUENCE; Schema: taxonomia; Owner: -
--

CREATE SEQUENCE taxonomia.nivel_taxonomico_sq_nivel_taxonomico_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7895 (class 0 OID 0)
-- Dependencies: 261
-- Name: nivel_taxonomico_sq_nivel_taxonomico_seq; Type: SEQUENCE OWNED BY; Schema: taxonomia; Owner: -
--

ALTER SEQUENCE taxonomia.nivel_taxonomico_sq_nivel_taxonomico_seq OWNED BY taxonomia.nivel_taxonomico.sq_nivel_taxonomico;


--
-- TOC entry 473 (class 1259 OID 28343)
-- Name: nivel_taxonomico; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.nivel_taxonomico (
    sq_nivel_taxonomico integer DEFAULT nextval('taxonomia.nivel_taxonomico_sq_nivel_taxonomico_seq'::regclass) NOT NULL,
    de_nivel_taxonomico text NOT NULL,
    nu_grau_taxonomico integer NOT NULL,
    in_nivel_intermediario boolean DEFAULT false NOT NULL,
    co_nivel_taxonomico text NOT NULL,
    CONSTRAINT chk_nivel_taxonomico_nivel CHECK ((in_nivel_intermediario = ANY (ARRAY[true, false])))
);


--
-- TOC entry 7896 (class 0 OID 0)
-- Dependencies: 473
-- Name: COLUMN nivel_taxonomico.nu_grau_taxonomico; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.nivel_taxonomico.nu_grau_taxonomico IS 'Squencia numérica que identifica a ordem hierárquica entre os níveis taxonômicos 10,20,30,40...';


--
-- TOC entry 7897 (class 0 OID 0)
-- Dependencies: 473
-- Name: COLUMN nivel_taxonomico.in_nivel_intermediario; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.nivel_taxonomico.in_nivel_intermediario IS 'Identifica se o nivel taxonômico é um nivel fora da sequência padrão REFICOFAGE';


--
-- TOC entry 7898 (class 0 OID 0)
-- Dependencies: 473
-- Name: COLUMN nivel_taxonomico.co_nivel_taxonomico; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.nivel_taxonomico.co_nivel_taxonomico IS 'Código do nível taxonômico sem acento e em caixa alta para ser utilizado como constantes nas consultas no lugar de do sq_nivel e nu_ordem_taxonomica';


--
-- TOC entry 253 (class 1259 OID 22766)
-- Name: abrangencia; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.abrangencia (
    sq_abrangencia integer NOT NULL,
    sq_municipio bigint,
    sq_uc_federal bigint,
    sq_estado bigint,
    sq_uc_nao_federal bigint,
    sq_rppn bigint,
    sq_pais bigint,
    sq_regiao bigint,
    sq_dados_apoio bigint,
    sq_abrangencia_pai bigint,
    sq_tipo bigint
);


--
-- TOC entry 7899 (class 0 OID 0)
-- Dependencies: 253
-- Name: TABLE abrangencia; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON TABLE salve.abrangencia IS 'Tabela para criacao das abrangencias. Ex: Estado, Municipio, Bioma etc';


--
-- TOC entry 7900 (class 0 OID 0)
-- Dependencies: 253
-- Name: COLUMN abrangencia.sq_abrangencia_pai; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.abrangencia.sq_abrangencia_pai IS 'Auto relacionamento';


--
-- TOC entry 252 (class 1259 OID 22764)
-- Name: abrangencia_sq_abrangencia_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.abrangencia_sq_abrangencia_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7901 (class 0 OID 0)
-- Dependencies: 252
-- Name: abrangencia_sq_abrangencia_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.abrangencia_sq_abrangencia_seq OWNED BY salve.abrangencia.sq_abrangencia;


--
-- TOC entry 247 (class 1259 OID 20778)
-- Name: bioma; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.bioma (
    sq_bioma integer NOT NULL,
    no_bioma character varying(50) NOT NULL,
    ge_bioma public.geometry,
    dt_inclusao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    dt_alteracao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    CONSTRAINT enforce_dims_the_geom CHECK ((public.st_ndims(ge_bioma) = 2)),
    CONSTRAINT enforce_geotype_the_geom CHECK (((public.geometrytype(ge_bioma) = 'MULTIPOLYGON'::text) OR (ge_bioma IS NULL))),
    CONSTRAINT enforce_srid_the_geom CHECK ((public.st_srid(ge_bioma) = 4674))
);


--
-- TOC entry 246 (class 1259 OID 20776)
-- Name: bioma_sq_bioma_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.bioma_sq_bioma_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7902 (class 0 OID 0)
-- Dependencies: 246
-- Name: bioma_sq_bioma_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.bioma_sq_bioma_seq OWNED BY salve.bioma.sq_bioma;


--
-- TOC entry 284 (class 1259 OID 23429)
-- Name: caverna; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.caverna (
    sq_caverna integer NOT NULL,
    sq_canie bigint,
    no_caverna character varying(120) NOT NULL
);


--
-- TOC entry 7903 (class 0 OID 0)
-- Dependencies: 284
-- Name: TABLE caverna; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON TABLE salve.caverna IS 'Tabela oriunda do sistema canie (ICMBio) contento o cadastro das cavernas';


--
-- TOC entry 7904 (class 0 OID 0)
-- Dependencies: 284
-- Name: COLUMN caverna.sq_caverna; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.caverna.sq_caverna IS 'Chave primária da tabela';


--
-- TOC entry 7905 (class 0 OID 0)
-- Dependencies: 284
-- Name: COLUMN caverna.sq_canie; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.caverna.sq_canie IS 'Integração com sistema CANIEe ICMBio';


--
-- TOC entry 7906 (class 0 OID 0)
-- Dependencies: 284
-- Name: COLUMN caverna.no_caverna; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.caverna.no_caverna IS 'Coluna que armazena o nome da caverna';


--
-- TOC entry 283 (class 1259 OID 23427)
-- Name: caverna_sq_caverna_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.caverna_sq_caverna_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7907 (class 0 OID 0)
-- Dependencies: 283
-- Name: caverna_sq_caverna_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.caverna_sq_caverna_seq OWNED BY salve.caverna.sq_caverna;


--
-- TOC entry 254 (class 1259 OID 22820)
-- Name: character_entity; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.character_entity (
    name text NOT NULL,
    ch character(1)
);


--
-- TOC entry 7908 (class 0 OID 0)
-- Dependencies: 254
-- Name: TABLE character_entity; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON TABLE salve.character_entity IS 'Tabela de entidades html utilizada na convesão para caracteres convencionais';


--
-- TOC entry 249 (class 1259 OID 22742)
-- Name: ciclo_avaliacao; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ciclo_avaliacao (
    sq_ciclo_avaliacao integer NOT NULL,
    de_ciclo_avaliacao text,
    nu_ano smallint NOT NULL,
    in_situacao character(1) NOT NULL,
    in_oficina character(1) DEFAULT 'S'::bpchar NOT NULL,
    in_publico character(1)
);


--
-- TOC entry 7909 (class 0 OID 0)
-- Dependencies: 249
-- Name: COLUMN ciclo_avaliacao.in_oficina; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ciclo_avaliacao.in_oficina IS 'Indica se as fichas do ciclo poderão iniciar fase consulta ou oficina';


--
-- TOC entry 7910 (class 0 OID 0)
-- Dependencies: 249
-- Name: COLUMN ciclo_avaliacao.in_publico; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ciclo_avaliacao.in_publico IS 'Informa se as inforamcoes do ciclo estao disponiveis para o modulo publico do SALVE.';


--
-- TOC entry 248 (class 1259 OID 22740)
-- Name: ciclo_avaliacao_sq_ciclo_avaliacao_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ciclo_avaliacao_sq_ciclo_avaliacao_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7911 (class 0 OID 0)
-- Dependencies: 248
-- Name: ciclo_avaliacao_sq_ciclo_avaliacao_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ciclo_avaliacao_sq_ciclo_avaliacao_seq OWNED BY salve.ciclo_avaliacao.sq_ciclo_avaliacao;


--
-- TOC entry 258 (class 1259 OID 22871)
-- Name: ciclo_avaliacao_validador; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ciclo_avaliacao_validador (
    sq_ciclo_avaliacao_validador integer NOT NULL,
    sq_validador bigint NOT NULL,
    sq_situacao_convite bigint NOT NULL,
    sq_ciclo_avaliacao bigint NOT NULL,
    de_email text,
    dt_email timestamp with time zone,
    dt_resposta timestamp with time zone,
    dt_validade_convite date,
    no_usuario text,
    de_hash text,
    sq_oficina bigint
);


--
-- TOC entry 7912 (class 0 OID 0)
-- Dependencies: 258
-- Name: COLUMN ciclo_avaliacao_validador.dt_resposta; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ciclo_avaliacao_validador.dt_resposta IS 'Data em que o validador respondeu ao convite aceitando ou n?';


--
-- TOC entry 7913 (class 0 OID 0)
-- Dependencies: 258
-- Name: COLUMN ciclo_avaliacao_validador.de_hash; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ciclo_avaliacao_validador.de_hash IS 'Hash de resposta automática pela api';


--
-- TOC entry 7914 (class 0 OID 0)
-- Dependencies: 258
-- Name: COLUMN ciclo_avaliacao_validador.sq_oficina; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ciclo_avaliacao_validador.sq_oficina IS 'Chave estrangeira da relação com a tabela oficina';


--
-- TOC entry 257 (class 1259 OID 22869)
-- Name: ciclo_avaliacao_validador_sq_ciclo_avaliacao_validador_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ciclo_avaliacao_validador_sq_ciclo_avaliacao_validador_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7915 (class 0 OID 0)
-- Dependencies: 257
-- Name: ciclo_avaliacao_validador_sq_ciclo_avaliacao_validador_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ciclo_avaliacao_validador_sq_ciclo_avaliacao_validador_seq OWNED BY salve.ciclo_avaliacao_validador.sq_ciclo_avaliacao_validador;


--
-- TOC entry 260 (class 1259 OID 22904)
-- Name: ciclo_consulta; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ciclo_consulta (
    sq_ciclo_consulta integer NOT NULL,
    sq_ciclo_avaliacao bigint NOT NULL,
    sq_tipo_consulta bigint NOT NULL,
    dt_inicio date NOT NULL,
    dt_fim date NOT NULL,
    tx_email text,
    de_consulta text,
    de_titulo_consulta text,
    sq_unidade_org bigint DEFAULT 3164 NOT NULL,
    sg_consulta text
);


--
-- TOC entry 7916 (class 0 OID 0)
-- Dependencies: 260
-- Name: COLUMN ciclo_consulta.sq_tipo_consulta; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ciclo_consulta.sq_tipo_consulta IS 'Ampla - todos podem contribuir ou Restrita-somente para os especialistas';


--
-- TOC entry 7917 (class 0 OID 0)
-- Dependencies: 260
-- Name: COLUMN ciclo_consulta.sq_unidade_org; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ciclo_consulta.sq_unidade_org IS 'Id da unidade organizacional responsÃ¡vel pela consulta';


--
-- TOC entry 7918 (class 0 OID 0)
-- Dependencies: 260
-- Name: COLUMN ciclo_consulta.sg_consulta; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ciclo_consulta.sg_consulta IS 'Nome da consulta que será exibido na consulta Ampla / Direta';


--
-- TOC entry 272 (class 1259 OID 23290)
-- Name: ciclo_consulta_ficha; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ciclo_consulta_ficha (
    sq_ciclo_consulta_ficha integer NOT NULL,
    sq_ciclo_consulta bigint NOT NULL,
    sq_ficha bigint
);


--
-- TOC entry 271 (class 1259 OID 23288)
-- Name: ciclo_consulta_ficha_sq_ciclo_consulta_ficha_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ciclo_consulta_ficha_sq_ciclo_consulta_ficha_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7919 (class 0 OID 0)
-- Dependencies: 271
-- Name: ciclo_consulta_ficha_sq_ciclo_consulta_ficha_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ciclo_consulta_ficha_sq_ciclo_consulta_ficha_seq OWNED BY salve.ciclo_consulta_ficha.sq_ciclo_consulta_ficha;


--
-- TOC entry 519 (class 1259 OID 29512)
-- Name: ciclo_consulta_ficha_versao; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ciclo_consulta_ficha_versao (
    sq_ciclo_consulta_ficha integer NOT NULL,
    sq_ficha_versao integer NOT NULL
);


--
-- TOC entry 7920 (class 0 OID 0)
-- Dependencies: 519
-- Name: TABLE ciclo_consulta_ficha_versao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON TABLE salve.ciclo_consulta_ficha_versao IS 'Armazenar qual foi a versão da ficha utilizada na consulta';


--
-- TOC entry 7921 (class 0 OID 0)
-- Dependencies: 519
-- Name: COLUMN ciclo_consulta_ficha_versao.sq_ciclo_consulta_ficha; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ciclo_consulta_ficha_versao.sq_ciclo_consulta_ficha IS 'Chave estrangeira e primário recebida da tabela ciclo_consulta_ficha';


--
-- TOC entry 7922 (class 0 OID 0)
-- Dependencies: 519
-- Name: COLUMN ciclo_consulta_ficha_versao.sq_ficha_versao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ciclo_consulta_ficha_versao.sq_ficha_versao IS 'Chave estrangeira da tabela ficha_versao';


--
-- TOC entry 274 (class 1259 OID 23310)
-- Name: ciclo_consulta_pessoa; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ciclo_consulta_pessoa (
    sq_ciclo_consulta_pessoa integer NOT NULL,
    sq_ciclo_consulta bigint NOT NULL,
    sq_pessoa bigint NOT NULL,
    sq_situacao bigint NOT NULL,
    de_email text,
    dt_envio date,
    de_observacao text,
    sq_instituicao bigint
);


--
-- TOC entry 7923 (class 0 OID 0)
-- Dependencies: 274
-- Name: COLUMN ciclo_consulta_pessoa.de_observacao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ciclo_consulta_pessoa.de_observacao IS 'Pode ser utilizado para  dizer que a pessoa ?um especialista na hora de avaliar o seu coment?io.';


--
-- TOC entry 7924 (class 0 OID 0)
-- Dependencies: 274
-- Name: COLUMN ciclo_consulta_pessoa.sq_instituicao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ciclo_consulta_pessoa.sq_instituicao IS 'Informar a instituicao do convidado da consulta ou revisao';


--
-- TOC entry 276 (class 1259 OID 23342)
-- Name: ciclo_consulta_pessoa_ficha; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ciclo_consulta_pessoa_ficha (
    sq_ciclo_consulta_pessoa_ficha integer NOT NULL,
    sq_ciclo_consulta_pessoa bigint NOT NULL,
    sq_ciclo_consulta_ficha bigint NOT NULL
);


--
-- TOC entry 275 (class 1259 OID 23340)
-- Name: ciclo_consulta_pessoa_ficha_sq_ciclo_consulta_pessoa_ficha_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ciclo_consulta_pessoa_ficha_sq_ciclo_consulta_pessoa_ficha_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7925 (class 0 OID 0)
-- Dependencies: 275
-- Name: ciclo_consulta_pessoa_ficha_sq_ciclo_consulta_pessoa_ficha_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ciclo_consulta_pessoa_ficha_sq_ciclo_consulta_pessoa_ficha_seq OWNED BY salve.ciclo_consulta_pessoa_ficha.sq_ciclo_consulta_pessoa_ficha;


--
-- TOC entry 273 (class 1259 OID 23308)
-- Name: ciclo_consulta_pessoa_sq_ciclo_consulta_pessoa_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ciclo_consulta_pessoa_sq_ciclo_consulta_pessoa_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7926 (class 0 OID 0)
-- Dependencies: 273
-- Name: ciclo_consulta_pessoa_sq_ciclo_consulta_pessoa_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ciclo_consulta_pessoa_sq_ciclo_consulta_pessoa_seq OWNED BY salve.ciclo_consulta_pessoa.sq_ciclo_consulta_pessoa;


--
-- TOC entry 259 (class 1259 OID 22902)
-- Name: ciclo_consulta_sq_ciclo_consulta_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ciclo_consulta_sq_ciclo_consulta_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7927 (class 0 OID 0)
-- Dependencies: 259
-- Name: ciclo_consulta_sq_ciclo_consulta_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ciclo_consulta_sq_ciclo_consulta_seq OWNED BY salve.ciclo_consulta.sq_ciclo_consulta;


--
-- TOC entry 227 (class 1259 OID 18112)
-- Name: dados_apoio; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.dados_apoio (
    sq_dados_apoio integer NOT NULL,
    sq_dados_apoio_pai bigint,
    ds_dados_apoio text NOT NULL,
    cd_dados_apoio text,
    de_dados_apoio_ordem text,
    cd_sistema text,
    cd_dados_apoio_nivel character varying(20)
);


--
-- TOC entry 7928 (class 0 OID 0)
-- Dependencies: 227
-- Name: COLUMN dados_apoio.de_dados_apoio_ordem; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.dados_apoio.de_dados_apoio_ordem IS 'Valor utilizado para definr a ordem de listagem dos registros';


--
-- TOC entry 7929 (class 0 OID 0)
-- Dependencies: 227
-- Name: COLUMN dados_apoio.cd_sistema; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.dados_apoio.cd_sistema IS 'Código único utilizado pelo sistema para aplicação de regras de negócio';


--
-- TOC entry 7930 (class 0 OID 0)
-- Dependencies: 227
-- Name: COLUMN dados_apoio.cd_dados_apoio_nivel; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.dados_apoio.cd_dados_apoio_nivel IS 'Campo utilizado para criar hierarquia entre os registros, quando necessário, os itens da tabela independente da ordem de apresentação definida pela coluna cd_dados_apoio_ordem';


--
-- TOC entry 229 (class 1259 OID 18133)
-- Name: dados_apoio_geo; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.dados_apoio_geo (
    sq_dados_apoio_geo integer NOT NULL,
    sq_dados_apoio bigint,
    cd_geo text,
    tx_geo text,
    ge_geo public.geometry,
    CONSTRAINT enforce_dims_geom CHECK ((public.st_ndims(ge_geo) = 2)),
    CONSTRAINT enforce_geotype_geom CHECK ((public.geometrytype(ge_geo) = 'MULTIPOLYGON'::text)),
    CONSTRAINT enforce_srid_geom CHECK ((public.st_srid(ge_geo) = 4674))
);


--
-- TOC entry 7931 (class 0 OID 0)
-- Dependencies: 229
-- Name: TABLE dados_apoio_geo; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON TABLE salve.dados_apoio_geo IS 'Tabela para armazenamento das informações georefenciadas da tabela de apoio.';


--
-- TOC entry 7932 (class 0 OID 0)
-- Dependencies: 229
-- Name: COLUMN dados_apoio_geo.sq_dados_apoio_geo; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.dados_apoio_geo.sq_dados_apoio_geo IS 'Chave primaria e sequencial';


--
-- TOC entry 7933 (class 0 OID 0)
-- Dependencies: 229
-- Name: COLUMN dados_apoio_geo.sq_dados_apoio; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.dados_apoio_geo.sq_dados_apoio IS 'Chave estrangeira da tabela dados_apoio';


--
-- TOC entry 7934 (class 0 OID 0)
-- Dependencies: 229
-- Name: COLUMN dados_apoio_geo.cd_geo; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.dados_apoio_geo.cd_geo IS 'Código identificador do shape ou polígono';


--
-- TOC entry 7935 (class 0 OID 0)
-- Dependencies: 229
-- Name: COLUMN dados_apoio_geo.tx_geo; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.dados_apoio_geo.tx_geo IS 'Informações textuais sobre o shape ou polígono';


--
-- TOC entry 7936 (class 0 OID 0)
-- Dependencies: 229
-- Name: COLUMN dados_apoio_geo.ge_geo; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.dados_apoio_geo.ge_geo IS 'Polígono';


--
-- TOC entry 228 (class 1259 OID 18131)
-- Name: dados_apoio_geo_sq_dados_apoio_geo_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.dados_apoio_geo_sq_dados_apoio_geo_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7937 (class 0 OID 0)
-- Dependencies: 228
-- Name: dados_apoio_geo_sq_dados_apoio_geo_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.dados_apoio_geo_sq_dados_apoio_geo_seq OWNED BY salve.dados_apoio_geo.sq_dados_apoio_geo;


--
-- TOC entry 226 (class 1259 OID 18110)
-- Name: dados_apoio_sq_dados_apoio_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.dados_apoio_sq_dados_apoio_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7938 (class 0 OID 0)
-- Dependencies: 226
-- Name: dados_apoio_sq_dados_apoio_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.dados_apoio_sq_dados_apoio_seq OWNED BY salve.dados_apoio.sq_dados_apoio;


--
-- TOC entry 278 (class 1259 OID 23362)
-- Name: efeito_ameaca; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.efeito_ameaca (
    sq_efeito_ameaca integer NOT NULL,
    sq_efeito bigint,
    sq_ameaca bigint
);


--
-- TOC entry 7939 (class 0 OID 0)
-- Dependencies: 278
-- Name: TABLE efeito_ameaca; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON TABLE salve.efeito_ameaca IS 'Tabela para restringir os efeitos por ameaca.';


--
-- TOC entry 7940 (class 0 OID 0)
-- Dependencies: 278
-- Name: COLUMN efeito_ameaca.sq_efeito_ameaca; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.efeito_ameaca.sq_efeito_ameaca IS 'Chave primaria sequencial';


--
-- TOC entry 7941 (class 0 OID 0)
-- Dependencies: 278
-- Name: COLUMN efeito_ameaca.sq_efeito; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.efeito_ameaca.sq_efeito IS 'Chave estrangeira da tabela apoio TB_EFEITO_PRIM';


--
-- TOC entry 7942 (class 0 OID 0)
-- Dependencies: 278
-- Name: COLUMN efeito_ameaca.sq_ameaca; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.efeito_ameaca.sq_ameaca IS 'Chave estrangeira da tabela TB_CRITERIO_AMEACA_IUCN';


--
-- TOC entry 277 (class 1259 OID 23360)
-- Name: efeito_ameaca_sq_efeito_ameaca_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.efeito_ameaca_sq_efeito_ameaca_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7943 (class 0 OID 0)
-- Dependencies: 277
-- Name: efeito_ameaca_sq_efeito_ameaca_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.efeito_ameaca_sq_efeito_ameaca_seq OWNED BY salve.efeito_ameaca.sq_efeito_ameaca;


--
-- TOC entry 251 (class 1259 OID 22755)
-- Name: esfera; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.esfera (
    sq_esfera integer NOT NULL,
    no_esfera text NOT NULL
);


--
-- TOC entry 7944 (class 0 OID 0)
-- Dependencies: 251
-- Name: TABLE esfera; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON TABLE salve.esfera IS 'Tabela que armazena a esfera de atividade, ou seja, o domínio de ação das Unidades Organizacionais. A esfera das Unidades Organizacionais e Unidades de Conservação pode ser federal, estadual e municipal.';


--
-- TOC entry 7945 (class 0 OID 0)
-- Dependencies: 251
-- Name: COLUMN esfera.sq_esfera; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.esfera.sq_esfera IS 'Chave primária da tabela';


--
-- TOC entry 7946 (class 0 OID 0)
-- Dependencies: 251
-- Name: COLUMN esfera.no_esfera; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.esfera.no_esfera IS 'Coluna que armazena o nome da esfera da Unidade Organizacional.';


--
-- TOC entry 250 (class 1259 OID 22753)
-- Name: esfera_sq_esfera_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.esfera_sq_esfera_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7947 (class 0 OID 0)
-- Dependencies: 250
-- Name: esfera_sq_esfera_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.esfera_sq_esfera_seq OWNED BY salve.esfera.sq_esfera;


--
-- TOC entry 221 (class 1259 OID 18026)
-- Name: estado; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.estado (
    sq_estado integer NOT NULL,
    sg_estado character(2) NOT NULL,
    sq_pais bigint NOT NULL,
    no_estado character varying(50) NOT NULL,
    ge_estado public.geometry,
    sq_regiao bigint,
    dt_inclusao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    dt_alteracao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    CONSTRAINT enforce_geotype_ge_estado CHECK (((public.geometrytype(ge_estado) = 'MULTIPOLYGON'::text) OR (ge_estado IS NULL))),
    CONSTRAINT enforce_srid_ge_estado CHECK ((public.st_srid(ge_estado) = 4674)),
    CONSTRAINT estado_ge_estado_check CHECK ((public.st_ndims(ge_estado) = 2))
);


--
-- TOC entry 220 (class 1259 OID 18024)
-- Name: estado_sq_estado_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.estado_sq_estado_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7948 (class 0 OID 0)
-- Dependencies: 220
-- Name: estado_sq_estado_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.estado_sq_estado_seq OWNED BY salve.estado.sq_estado;


--
-- TOC entry 270 (class 1259 OID 23019)
-- Name: ficha; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha (
    sq_ficha integer NOT NULL,
    sq_unidade_org bigint NOT NULL,
    sq_usuario integer NOT NULL,
    sq_taxon bigint NOT NULL,
    sq_situacao_ficha bigint NOT NULL,
    sq_ciclo_avaliacao bigint NOT NULL,
    nm_cientifico text NOT NULL,
    ds_notas_taxonomicas text,
    ds_diagnostico_morfologico text,
    st_endemica_brasil character(1),
    ds_distribuicao_geo_nacional text,
    ds_distribuicao_geo_global text,
    nu_min_altitude integer,
    nu_max_altitude integer,
    nu_min_batimetria integer,
    nu_max_batimetria integer,
    ds_historia_natural text,
    st_habito_aliment_especialista character(1),
    ds_habito_alimentar text,
    st_restrito_habitat_primario character(1),
    st_especialista_micro_habitat character(1),
    ds_especialista_micro_habitat text,
    st_variacao_sazonal_habitat character(1),
    st_difer_macho_femea_habitat character(1),
    ds_diferenca_macho_femea text,
    ds_uso_habitat text,
    ds_interacao text,
    vl_intervalo_nascimento text,
    vl_tempo_gestacao text,
    vl_tamanho_prole text,
    vl_maturidade_sexual_femea numeric(4,1),
    vl_maturidade_sexual_macho numeric(4,1),
    sq_unid_maturidade_sexual_macho bigint,
    vl_peso_femea numeric(6,1),
    vl_peso_macho bigint,
    vl_comprimento_femea numeric(6,1),
    sq_grupo bigint,
    sq_posicao_trofica bigint,
    sq_modo_reproducao bigint,
    sq_sistema_acasalamento bigint,
    sq_unid_intervalo_nascimento bigint,
    sq_unid_tempo_gestacao bigint,
    sq_unid_maturidade_sexual_femea bigint,
    sq_unidade_peso_femea bigint,
    sq_unidade_peso_macho bigint,
    sq_medida_comprimento_femea bigint,
    sq_medida_comprimento_macho bigint,
    sq_unidade_senilid_rep_femea bigint,
    sq_unidade_senilid_rep_macho bigint,
    sq_unidade_longevidade_femea bigint,
    sq_unidade_longevidade_macho bigint,
    sq_tendencia_populacional bigint,
    sq_periodo_taxa_cresc_popul bigint,
    sq_tendencia_eoo bigint,
    sq_tendencia_aoo bigint,
    sq_medida_tempo_geracional bigint,
    sq_tipo_redu_popu_pass_rev bigint,
    sq_tipo_redu_popu_pass bigint,
    sq_tipo_redu_popu_futura bigint,
    sq_tipo_redu_popu_pass_futura bigint,
    sq_declinio_populacional bigint,
    sq_tenden_num_individuo_maduro bigint,
    sq_perc_declinio_populacional bigint,
    sq_tendencia_num_subpopulacao bigint,
    sq_tenden_num_localizacoes bigint,
    sq_tenden_qualidade_habitat bigint,
    sq_prob_extincao_brasil bigint,
    sq_tipo_conectividade bigint,
    sq_categoria_final bigint,
    sq_categoria_iucn bigint,
    sq_categoria_iucn_sugestao bigint,
    sq_tendencia_imigracao bigint,
    sq_padrao_deslocamento bigint,
    vl_comprimento_macho numeric(6,1),
    vl_senilidade_reprodutiva_femea numeric(5,1),
    vl_senilidade_reprodutiva_macho numeric(5,1),
    vl_longevidade_femea numeric(5,1),
    vl_longevidade_macho numeric(5,1),
    ds_reproducao text,
    ds_razao_sexual text,
    ds_taxa_mortalidade text,
    vl_taxa_cresc_populacional integer,
    ds_caracteristica_genetica text,
    ds_populacao text,
    ds_ameaca text,
    ds_uso text,
    st_presenca_lista_vigente character(1),
    ds_acao_conservacao text,
    ds_presenca_uc text,
    ds_pesquisa_exist_necessaria text,
    st_ocorrencia_marginal character(1),
    st_elegivel_avaliacao character(1),
    st_limitacao_taxonomica_aval character(1),
    vl_eoo numeric(10,2),
    st_flutuacao_eoo character(1),
    vl_aoo numeric(10,2),
    st_flutuacao_aoo character(1),
    ds_justificativa_aoo_eoo text,
    st_localidade_tipo_conhecida character(1),
    st_regiao_bem_amostrada character(1),
    vl_tempo_geracional numeric(6,2),
    ds_metod_calc_tempo_geracional text,
    nu_reducao_popul_passada_rev integer,
    nu_reducao_popul_passada integer,
    nu_reducao_popul_futura bigint,
    nu_reducao_popul_pass_futura bigint,
    st_populacao_fragmentada character(1),
    nu_individuo_maduro integer,
    st_flut_extrema_indiv_maduro character(1),
    nu_subpopulacao integer,
    st_flut_extrema_sub_populacao character(1),
    nu_indivi_subpopulacao_max integer,
    nu_indivi_subpopulacao_perc integer,
    nu_localizacoes integer,
    st_flutuacao_num_localizacoes character(1),
    st_ameaca_futura character(1),
    ds_metodo_calc_perc_extin_br text,
    st_declinio_br_popul_exterior character(1),
    ds_conectividade_pop_exterior text,
    ds_criterio_aval_iucn text,
    ds_justificativa text,
    ds_just_mudanca_categoria text,
    ds_pendencias text,
    st_categoria_ok character(1),
    st_criterio_aval_iucn_ok character(1),
    ds_criterio_aval_iucn_sugerido text,
    ds_justificativa_validacao text,
    ds_criterio_aval_iucn_final text,
    ds_justificativa_final text,
    ds_issn text,
    ds_doi text,
    ds_citacao text,
    st_migratoria character(1),
    st_possivelmente_extinta character(1),
    st_favorecido_conversao_habitats character(1),
    st_tem_registro_areas_amplas character(1),
    st_possui_ampla_dist_geografica character(1),
    st_frequente_inventario_eoo character(1),
    sq_medida_comprimento_femea_max bigint,
    sq_medida_comprimento_macho_max bigint,
    vl_comprimento_macho_max numeric(6,1),
    vl_comprimento_femea_max numeric(6,1),
    dt_aceite_validacao timestamp without time zone,
    sq_usuario_alteracao bigint,
    dt_alteracao timestamp without time zone,
    vl_percentual_preenchimento integer DEFAULT 0,
    dt_publicacao timestamp without time zone,
    sq_usuario_publicacao bigint,
    st_manter_lc boolean,
    st_transferida boolean,
    st_possivelmente_extinta_final character(1),
    sq_ficha_ciclo_anterior bigint,
    sq_subgrupo bigint,
    sq_grupo_salve bigint,
    st_localidade_desconhecida character(1),
    ds_historico_avaliacao text,
    ds_equipe_tecnica text,
    ds_colaboradores text,
    st_sem_uf character(1) DEFAULT 'N'::bpchar NOT NULL,
    ds_justificativa_eoo text,
    ds_justificativa_aoo text,
    CONSTRAINT chk_ficha_pex_final CHECK ((st_possivelmente_extinta_final = ANY (ARRAY['S'::bpchar, 'N'::bpchar, 'D'::bpchar]))),
    CONSTRAINT ckc_ficha_st_categoria_ok CHECK (((st_categoria_ok IS NULL) OR (st_categoria_ok = ANY (ARRAY['S'::bpchar, 'N'::bpchar, 'D'::bpchar])))),
    CONSTRAINT ckc_ficha_st_crit_aval_iucn_ok CHECK (((st_criterio_aval_iucn_ok IS NULL) OR (st_criterio_aval_iucn_ok = ANY (ARRAY['S'::bpchar, 'N'::bpchar, 'D'::bpchar])))),
    CONSTRAINT ckc_ficha_st_dif_macho_femea_habit CHECK ((st_difer_macho_femea_habitat = ANY (ARRAY['S'::bpchar, 'N'::bpchar, 'D'::bpchar]))),
    CONSTRAINT ckc_ficha_st_espec_micro_habitat CHECK ((st_especialista_micro_habitat = ANY (ARRAY['S'::bpchar, 'N'::bpchar, 'D'::bpchar]))),
    CONSTRAINT ckc_ficha_st_flut_extrema_sub_pop CHECK ((st_flut_extrema_sub_populacao = ANY (ARRAY['S'::bpchar, 'N'::bpchar, 'D'::bpchar]))),
    CONSTRAINT ckc_ficha_st_flutuacao_aoo CHECK ((st_flutuacao_aoo = ANY (ARRAY['S'::bpchar, 'N'::bpchar, 'D'::bpchar]))),
    CONSTRAINT ckc_ficha_st_flutuacao_eoo CHECK ((st_flutuacao_eoo = ANY (ARRAY['S'::bpchar, 'N'::bpchar, 'D'::bpchar]))),
    CONSTRAINT ckc_ficha_st_presenca_lista_vigente CHECK ((st_presenca_lista_vigente = ANY (ARRAY['S'::bpchar, 'N'::bpchar]))),
    CONSTRAINT ckc_ficha_st_var_sazo_habit CHECK ((st_variacao_sazonal_habitat = ANY (ARRAY['S'::bpchar, 'N'::bpchar, 'D'::bpchar]))),
    CONSTRAINT ckc_st_declinio_br_po_ficha CHECK (((st_declinio_br_popul_exterior IS NULL) OR (st_declinio_br_popul_exterior = ANY (ARRAY['S'::bpchar, 'N'::bpchar, 'D'::bpchar])))),
    CONSTRAINT ckc_st_endemica_brasi_ficha CHECK ((st_endemica_brasil = ANY (ARRAY['S'::bpchar, 'N'::bpchar, 'D'::bpchar])))
);


--
-- TOC entry 7949 (class 0 OID 0)
-- Dependencies: 270
-- Name: TABLE ficha; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON TABLE salve.ficha IS 'Os campos nu_max_altitude, nu_min_altitude, nu_min_batimetria, nu_max_batimetria são informados em metros';


--
-- TOC entry 7950 (class 0 OID 0)
-- Dependencies: 270
-- Name: COLUMN ficha.st_especialista_micro_habitat; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha.st_especialista_micro_habitat IS 'Valores aceitos: S,N,D sendo S=Sim, N=Não e D=Desconhecido';


--
-- TOC entry 7951 (class 0 OID 0)
-- Dependencies: 270
-- Name: COLUMN ficha.st_variacao_sazonal_habitat; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha.st_variacao_sazonal_habitat IS 'Valores aceitos: S,N,D sendo S=Sim, N=Não e D=Desconhecido';


--
-- TOC entry 7952 (class 0 OID 0)
-- Dependencies: 270
-- Name: COLUMN ficha.st_difer_macho_femea_habitat; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha.st_difer_macho_femea_habitat IS 'Valores aceitos: S,N,D sendo S=Sim, N=Não e D=Desconhecido';


--
-- TOC entry 7953 (class 0 OID 0)
-- Dependencies: 270
-- Name: COLUMN ficha.sq_grupo; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha.sq_grupo IS 'Coluna para informar o grupo avaliado. (tb_grupo)';


--
-- TOC entry 7954 (class 0 OID 0)
-- Dependencies: 270
-- Name: COLUMN ficha.sq_padrao_deslocamento; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha.sq_padrao_deslocamento IS 'Padrão de deslocamento das aves migratórias';


--
-- TOC entry 7955 (class 0 OID 0)
-- Dependencies: 270
-- Name: COLUMN ficha.ds_razao_sexual; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha.ds_razao_sexual IS '1:2, 1:1,';


--
-- TOC entry 7956 (class 0 OID 0)
-- Dependencies: 270
-- Name: COLUMN ficha.st_presenca_lista_vigente; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha.st_presenca_lista_vigente IS 'S/N';


--
-- TOC entry 7957 (class 0 OID 0)
-- Dependencies: 270
-- Name: COLUMN ficha.vl_eoo; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha.vl_eoo IS 'em Km2';


--
-- TOC entry 7958 (class 0 OID 0)
-- Dependencies: 270
-- Name: COLUMN ficha.st_flutuacao_eoo; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha.st_flutuacao_eoo IS 'S/N/D';


--
-- TOC entry 7959 (class 0 OID 0)
-- Dependencies: 270
-- Name: COLUMN ficha.vl_aoo; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha.vl_aoo IS 'em Km2';


--
-- TOC entry 7960 (class 0 OID 0)
-- Dependencies: 270
-- Name: COLUMN ficha.st_flutuacao_aoo; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha.st_flutuacao_aoo IS 'S/N/D';


--
-- TOC entry 7961 (class 0 OID 0)
-- Dependencies: 270
-- Name: COLUMN ficha.st_flut_extrema_sub_populacao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha.st_flut_extrema_sub_populacao IS 'S/N/D';


--
-- TOC entry 7962 (class 0 OID 0)
-- Dependencies: 270
-- Name: COLUMN ficha.st_declinio_br_popul_exterior; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha.st_declinio_br_popul_exterior IS 'S/N/D';


--
-- TOC entry 7963 (class 0 OID 0)
-- Dependencies: 270
-- Name: COLUMN ficha.st_categoria_ok; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha.st_categoria_ok IS 'S/N - Se não tem que informar a sugestão';


--
-- TOC entry 7964 (class 0 OID 0)
-- Dependencies: 270
-- Name: COLUMN ficha.st_criterio_aval_iucn_ok; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha.st_criterio_aval_iucn_ok IS 'S/N - Se não tem que informar a sugestão';


--
-- TOC entry 7965 (class 0 OID 0)
-- Dependencies: 270
-- Name: COLUMN ficha.st_migratoria; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha.st_migratoria IS 'A espécie é migratória (a população inteira, ou parte significativa, migra para outros países)?';


--
-- TOC entry 7966 (class 0 OID 0)
-- Dependencies: 270
-- Name: COLUMN ficha.st_possivelmente_extinta; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha.st_possivelmente_extinta IS 'Preenchido com S/N/D durante a oficina - para Sim, Não ou Desconhecido';


--
-- TOC entry 7967 (class 0 OID 0)
-- Dependencies: 270
-- Name: COLUMN ficha.dt_aceite_validacao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha.dt_aceite_validacao IS 'Data de encerramento da validação da ficha, quando a categoria, critério são definitivamente aceitos';


--
-- TOC entry 7968 (class 0 OID 0)
-- Dependencies: 270
-- Name: COLUMN ficha.sq_usuario_alteracao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha.sq_usuario_alteracao IS 'Id do usuário que fez a última alteração';


--
-- TOC entry 7969 (class 0 OID 0)
-- Dependencies: 270
-- Name: COLUMN ficha.dt_alteracao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha.dt_alteracao IS 'Data da última alteração';


--
-- TOC entry 7970 (class 0 OID 0)
-- Dependencies: 270
-- Name: COLUMN ficha.vl_percentual_preenchimento; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha.vl_percentual_preenchimento IS 'guardar o percentual de preenchimento da ficha já calculado';


--
-- TOC entry 7971 (class 0 OID 0)
-- Dependencies: 270
-- Name: COLUMN ficha.dt_publicacao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha.dt_publicacao IS 'Data de publicação da ficha no portal da biodiversidade';


--
-- TOC entry 7972 (class 0 OID 0)
-- Dependencies: 270
-- Name: COLUMN ficha.sq_usuario_publicacao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha.sq_usuario_publicacao IS 'Id do usuário que fez a publicação';


--
-- TOC entry 7973 (class 0 OID 0)
-- Dependencies: 270
-- Name: COLUMN ficha.st_manter_lc; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha.st_manter_lc IS 'Informa se a ficha manteve a categoria LC após a oficina de validação';


--
-- TOC entry 7974 (class 0 OID 0)
-- Dependencies: 270
-- Name: COLUMN ficha.st_transferida; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha.st_transferida IS 'Informar que a ficha foi transferida de uma oficina para a outra';


--
-- TOC entry 7975 (class 0 OID 0)
-- Dependencies: 270
-- Name: COLUMN ficha.st_possivelmente_extinta_final; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha.st_possivelmente_extinta_final IS 'Preenchido com S/N/D na avaliação final - para Sim, Não ou Desconhecido';


--
-- TOC entry 7976 (class 0 OID 0)
-- Dependencies: 270
-- Name: COLUMN ficha.sq_ficha_ciclo_anterior; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha.sq_ficha_ciclo_anterior IS 'Identificar a ficha do ciclo anterior quando hover mudança no taxon da ficha atual.';


--
-- TOC entry 7977 (class 0 OID 0)
-- Dependencies: 270
-- Name: COLUMN ficha.sq_subgrupo; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha.sq_subgrupo IS 'Coluna para informar o subgrupo avaliado. (tb_subgrupo)';


--
-- TOC entry 7978 (class 0 OID 0)
-- Dependencies: 270
-- Name: COLUMN ficha.sq_grupo_salve; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha.sq_grupo_salve IS 'Coluna para informar o grupo taxonomico no salve. (tb_grupo_salve)';


--
-- TOC entry 7979 (class 0 OID 0)
-- Dependencies: 270
-- Name: COLUMN ficha.st_localidade_desconhecida; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha.st_localidade_desconhecida IS 'A especie nao possui uma localizacao conhecida no mapa';


--
-- TOC entry 7980 (class 0 OID 0)
-- Dependencies: 270
-- Name: COLUMN ficha.ds_historico_avaliacao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha.ds_historico_avaliacao IS 'Texto livre para informacoes sobre o historico da avaliacoes ocorridas';


--
-- TOC entry 7981 (class 0 OID 0)
-- Dependencies: 270
-- Name: COLUMN ficha.ds_equipe_tecnica; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha.ds_equipe_tecnica IS 'Registrar os nomes das pessoas que fizeram parte da equipe técnica na elaboração da ficha';


--
-- TOC entry 7982 (class 0 OID 0)
-- Dependencies: 270
-- Name: COLUMN ficha.ds_colaboradores; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha.ds_colaboradores IS 'Registrar os nomes das pessoas que contribuiram na elaboração da ficha';


--
-- TOC entry 7983 (class 0 OID 0)
-- Dependencies: 270
-- Name: COLUMN ficha.st_sem_uf; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha.st_sem_uf IS 'Informar que a espécie pode nao pussuir um Estado definido. Dominio S ou N';


--
-- TOC entry 7984 (class 0 OID 0)
-- Dependencies: 270
-- Name: COLUMN ficha.ds_justificativa_eoo; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha.ds_justificativa_eoo IS 'Informar a memoria de calculo ou a justificativa do valor da EOO';


--
-- TOC entry 7985 (class 0 OID 0)
-- Dependencies: 270
-- Name: COLUMN ficha.ds_justificativa_aoo; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha.ds_justificativa_aoo IS 'Informar a memoria de calculo ou a justificativa do valor da AOO';


--
-- TOC entry 282 (class 1259 OID 23392)
-- Name: ficha_acao_conservacao; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_acao_conservacao (
    sq_ficha_acao_conservacao integer NOT NULL,
    sq_ficha bigint NOT NULL,
    sq_acao_conservacao bigint NOT NULL,
    sq_situacao_acao_conservacao bigint NOT NULL,
    sq_plano_acao bigint,
    sq_tipo_ordenamento bigint,
    tx_ficha_acao_conservacao text
);


--
-- TOC entry 281 (class 1259 OID 23390)
-- Name: ficha_acao_conservacao_sq_ficha_acao_conservacao_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ficha_acao_conservacao_sq_ficha_acao_conservacao_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7986 (class 0 OID 0)
-- Dependencies: 281
-- Name: ficha_acao_conservacao_sq_ficha_acao_conservacao_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ficha_acao_conservacao_sq_ficha_acao_conservacao_seq OWNED BY salve.ficha_acao_conservacao.sq_ficha_acao_conservacao;


--
-- TOC entry 288 (class 1259 OID 23460)
-- Name: ficha_amb_restrito_caverna; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_amb_restrito_caverna (
    sq_ficha_amb_restrito_caverna integer NOT NULL,
    sq_ficha_ambiente_restrito bigint NOT NULL,
    sq_caverna bigint NOT NULL
);


--
-- TOC entry 7987 (class 0 OID 0)
-- Dependencies: 288
-- Name: TABLE ficha_amb_restrito_caverna; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON TABLE salve.ficha_amb_restrito_caverna IS 'Armazenar a(s) caverna(s) quando o ambiente restrito da espeice for em caverna';


--
-- TOC entry 7988 (class 0 OID 0)
-- Dependencies: 288
-- Name: COLUMN ficha_amb_restrito_caverna.sq_ficha_amb_restrito_caverna; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_amb_restrito_caverna.sq_ficha_amb_restrito_caverna IS 'chave primária sequencial da tabela';


--
-- TOC entry 7989 (class 0 OID 0)
-- Dependencies: 288
-- Name: COLUMN ficha_amb_restrito_caverna.sq_ficha_ambiente_restrito; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_amb_restrito_caverna.sq_ficha_ambiente_restrito IS 'chave estrangeira da tabela ficha_ambiente_restrito';


--
-- TOC entry 7990 (class 0 OID 0)
-- Dependencies: 288
-- Name: COLUMN ficha_amb_restrito_caverna.sq_caverna; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_amb_restrito_caverna.sq_caverna IS 'chave estrangeira da tabela salve.caverna';


--
-- TOC entry 287 (class 1259 OID 23458)
-- Name: ficha_amb_restrito_caverna_sq_ficha_amb_restrito_caverna_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ficha_amb_restrito_caverna_sq_ficha_amb_restrito_caverna_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7991 (class 0 OID 0)
-- Dependencies: 287
-- Name: ficha_amb_restrito_caverna_sq_ficha_amb_restrito_caverna_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ficha_amb_restrito_caverna_sq_ficha_amb_restrito_caverna_seq OWNED BY salve.ficha_amb_restrito_caverna.sq_ficha_amb_restrito_caverna;


--
-- TOC entry 286 (class 1259 OID 23438)
-- Name: ficha_ambiente_restrito; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_ambiente_restrito (
    sq_ficha_ambiente_restrito integer NOT NULL,
    sq_ficha bigint NOT NULL,
    sq_restricao_habitat bigint NOT NULL,
    tx_ficha_restricao_habitat text,
    de_restricao_habitat_outro text
);


--
-- TOC entry 285 (class 1259 OID 23436)
-- Name: ficha_ambiente_restrito_sq_ficha_ambiente_restrito_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ficha_ambiente_restrito_sq_ficha_ambiente_restrito_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7992 (class 0 OID 0)
-- Dependencies: 285
-- Name: ficha_ambiente_restrito_sq_ficha_ambiente_restrito_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ficha_ambiente_restrito_sq_ficha_ambiente_restrito_seq OWNED BY salve.ficha_ambiente_restrito.sq_ficha_ambiente_restrito;


--
-- TOC entry 292 (class 1259 OID 23540)
-- Name: ficha_ameaca; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_ameaca (
    sq_ficha_ameaca integer NOT NULL,
    sq_ficha bigint NOT NULL,
    sq_criterio_ameaca_iucn bigint NOT NULL,
    tx_ficha_ameaca text,
    nu_peso integer,
    sq_referencia_temporal bigint
);


--
-- TOC entry 7993 (class 0 OID 0)
-- Dependencies: 292
-- Name: COLUMN ficha_ameaca.nu_peso; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ameaca.nu_peso IS 'Peso atribuido ao item para gerar informacoes para o PRIM. Ex: 1=Secundario ou 2=Principal';


--
-- TOC entry 7994 (class 0 OID 0)
-- Dependencies: 292
-- Name: COLUMN ficha_ameaca.sq_referencia_temporal; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ameaca.sq_referencia_temporal IS 'Referencia temporal da ameaca, atual, passada ou potencial';


--
-- TOC entry 294 (class 1259 OID 23586)
-- Name: ficha_ameaca_efeito; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_ameaca_efeito (
    sq_ficha_ameaca_efeito integer NOT NULL,
    sq_ficha_ameaca bigint NOT NULL,
    sq_efeito bigint NOT NULL,
    nu_peso integer
);


--
-- TOC entry 7995 (class 0 OID 0)
-- Dependencies: 294
-- Name: COLUMN ficha_ameaca_efeito.sq_ficha_ameaca; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ameaca_efeito.sq_ficha_ameaca IS 'Id da ameaca atribuida na ficha';


--
-- TOC entry 7996 (class 0 OID 0)
-- Dependencies: 294
-- Name: COLUMN ficha_ameaca_efeito.sq_efeito; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ameaca_efeito.sq_efeito IS 'Id do efeito cadastrado na tabela dados_apoio';


--
-- TOC entry 7997 (class 0 OID 0)
-- Dependencies: 294
-- Name: COLUMN ficha_ameaca_efeito.nu_peso; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ameaca_efeito.nu_peso IS 'Peso atribuido ao item para gerar informacoes para o PRIM. Ex: 1=Secundario ou 2=Principal';


--
-- TOC entry 293 (class 1259 OID 23584)
-- Name: ficha_ameaca_efeito_sq_ficha_ameaca_efeito_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ficha_ameaca_efeito_sq_ficha_ameaca_efeito_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7998 (class 0 OID 0)
-- Dependencies: 293
-- Name: ficha_ameaca_efeito_sq_ficha_ameaca_efeito_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ficha_ameaca_efeito_sq_ficha_ameaca_efeito_seq OWNED BY salve.ficha_ameaca_efeito.sq_ficha_ameaca_efeito;


--
-- TOC entry 298 (class 1259 OID 24126)
-- Name: ficha_ameaca_regiao; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_ameaca_regiao (
    sq_ficha_ameaca_regiao integer NOT NULL,
    sq_ficha_ameaca bigint,
    sq_ficha_ocorrencia bigint NOT NULL
);


--
-- TOC entry 7999 (class 0 OID 0)
-- Dependencies: 298
-- Name: TABLE ficha_ameaca_regiao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON TABLE salve.ficha_ameaca_regiao IS 'Registrar as regiões da Ameaça';


--
-- TOC entry 297 (class 1259 OID 24124)
-- Name: ficha_ameaca_regiao_sq_ficha_ameaca_regiao_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ficha_ameaca_regiao_sq_ficha_ameaca_regiao_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8000 (class 0 OID 0)
-- Dependencies: 297
-- Name: ficha_ameaca_regiao_sq_ficha_ameaca_regiao_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ficha_ameaca_regiao_sq_ficha_ameaca_regiao_seq OWNED BY salve.ficha_ameaca_regiao.sq_ficha_ameaca_regiao;


--
-- TOC entry 291 (class 1259 OID 23538)
-- Name: ficha_ameaca_sq_ficha_ameaca_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ficha_ameaca_sq_ficha_ameaca_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8001 (class 0 OID 0)
-- Dependencies: 291
-- Name: ficha_ameaca_sq_ficha_ameaca_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ficha_ameaca_sq_ficha_ameaca_seq OWNED BY salve.ficha_ameaca.sq_ficha_ameaca;


--
-- TOC entry 300 (class 1259 OID 24240)
-- Name: ficha_anexo; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_anexo (
    sq_ficha_anexo integer NOT NULL,
    sq_ficha bigint NOT NULL,
    sq_contexto bigint NOT NULL,
    de_legenda text NOT NULL,
    no_arquivo character varying(255) NOT NULL,
    de_tipo_conteudo character varying(50) NOT NULL,
    de_local_arquivo character varying(255) NOT NULL,
    in_principal character(1) DEFAULT 'N'::bpchar NOT NULL,
    dt_anexo timestamp without time zone
);


--
-- TOC entry 8002 (class 0 OID 0)
-- Dependencies: 300
-- Name: COLUMN ficha_anexo.in_principal; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_anexo.in_principal IS 'No caso de um mapa este campo indica se ele vai ou não ser impresso ou visto na avaliação';


--
-- TOC entry 8003 (class 0 OID 0)
-- Dependencies: 300
-- Name: COLUMN ficha_anexo.dt_anexo; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_anexo.dt_anexo IS 'Data do arquivo anexado';


--
-- TOC entry 299 (class 1259 OID 24238)
-- Name: ficha_anexo_sq_ficha_anexo_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ficha_anexo_sq_ficha_anexo_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8004 (class 0 OID 0)
-- Dependencies: 299
-- Name: ficha_anexo_sq_ficha_anexo_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ficha_anexo_sq_ficha_anexo_seq OWNED BY salve.ficha_anexo.sq_ficha_anexo;


--
-- TOC entry 302 (class 1259 OID 24302)
-- Name: ficha_aoo; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_aoo (
    sq_ficha_aoo integer NOT NULL,
    sq_ficha bigint NOT NULL,
    ge_aoo public.geometry,
    dt_inclusao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    dt_alteracao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


--
-- TOC entry 8005 (class 0 OID 0)
-- Dependencies: 302
-- Name: COLUMN ficha_aoo.ge_aoo; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_aoo.ge_aoo IS 'Colecao de poligons 2x2 e desenhos feitos pelo usuario para calcular a Area de Ocupacao da especie';


--
-- TOC entry 301 (class 1259 OID 24300)
-- Name: ficha_aoo_sq_ficha_aoo_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ficha_aoo_sq_ficha_aoo_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8006 (class 0 OID 0)
-- Dependencies: 301
-- Name: ficha_aoo_sq_ficha_aoo_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ficha_aoo_sq_ficha_aoo_seq OWNED BY salve.ficha_aoo.sq_ficha_aoo;


--
-- TOC entry 304 (class 1259 OID 24366)
-- Name: ficha_area_relevancia; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_area_relevancia (
    sq_ficha_area_relevancia integer NOT NULL,
    sq_ficha bigint NOT NULL,
    sq_tipo_relevancia bigint NOT NULL,
    sq_estado bigint NOT NULL,
    tx_relevancia text,
    tx_local text,
    sq_uc_federal bigint,
    sq_uc_estadual bigint,
    sq_rppn bigint
);


--
-- TOC entry 306 (class 1259 OID 24402)
-- Name: ficha_area_relevancia_municipio; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_area_relevancia_municipio (
    sq_ficha_area_relevancia_municipio integer NOT NULL,
    sq_municipio bigint NOT NULL,
    sq_ficha_area_relevancia bigint NOT NULL,
    tx_relevancia_municipio text
);


--
-- TOC entry 305 (class 1259 OID 24400)
-- Name: ficha_area_relevancia_municip_sq_ficha_area_relevancia_muni_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ficha_area_relevancia_municip_sq_ficha_area_relevancia_muni_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8007 (class 0 OID 0)
-- Dependencies: 305
-- Name: ficha_area_relevancia_municip_sq_ficha_area_relevancia_muni_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ficha_area_relevancia_municip_sq_ficha_area_relevancia_muni_seq OWNED BY salve.ficha_area_relevancia_municipio.sq_ficha_area_relevancia_municipio;


--
-- TOC entry 303 (class 1259 OID 24364)
-- Name: ficha_area_relevancia_sq_ficha_area_relevancia_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ficha_area_relevancia_sq_ficha_area_relevancia_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8008 (class 0 OID 0)
-- Dependencies: 303
-- Name: ficha_area_relevancia_sq_ficha_area_relevancia_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ficha_area_relevancia_sq_ficha_area_relevancia_seq OWNED BY salve.ficha_area_relevancia.sq_ficha_area_relevancia;


--
-- TOC entry 308 (class 1259 OID 24445)
-- Name: ficha_area_vida; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_area_vida (
    sq_ficha_area_vida integer NOT NULL,
    sq_ficha bigint NOT NULL,
    sq_unidade_area bigint NOT NULL,
    tx_local text NOT NULL,
    vl_area text NOT NULL,
    nu_ano smallint,
    tx_area text
);


--
-- TOC entry 307 (class 1259 OID 24443)
-- Name: ficha_area_vida_sq_ficha_area_vida_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ficha_area_vida_sq_ficha_area_vida_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8009 (class 0 OID 0)
-- Dependencies: 307
-- Name: ficha_area_vida_sq_ficha_area_vida_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ficha_area_vida_sq_ficha_area_vida_seq OWNED BY salve.ficha_area_vida.sq_ficha_area_vida;


--
-- TOC entry 310 (class 1259 OID 24488)
-- Name: ficha_autor; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_autor (
    sq_ficha_autor integer NOT NULL,
    sq_autor bigint NOT NULL,
    sq_ficha bigint NOT NULL
);


--
-- TOC entry 309 (class 1259 OID 24486)
-- Name: ficha_autor_sq_ficha_autor_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ficha_autor_sq_ficha_autor_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8010 (class 0 OID 0)
-- Dependencies: 309
-- Name: ficha_autor_sq_ficha_autor_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ficha_autor_sq_ficha_autor_seq OWNED BY salve.ficha_autor.sq_ficha_autor;


--
-- TOC entry 312 (class 1259 OID 24536)
-- Name: ficha_avaliacao_regional; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_avaliacao_regional (
    sq_ficha_avaliacao_regional integer NOT NULL,
    sq_ficha bigint NOT NULL,
    sq_abrangencia bigint,
    sq_categoria_iucn bigint NOT NULL,
    nu_ano_avaliacao integer NOT NULL,
    de_criterio_avaliacao_iucn text,
    tx_justificativa_avaliacao text,
    st_possivelmente_extinta character varying(1)
);


--
-- TOC entry 8011 (class 0 OID 0)
-- Dependencies: 312
-- Name: TABLE ficha_avaliacao_regional; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON TABLE salve.ficha_avaliacao_regional IS 'Informacoes sobre as avaliacoes regionais realizadas para a especie';


--
-- TOC entry 8012 (class 0 OID 0)
-- Dependencies: 312
-- Name: COLUMN ficha_avaliacao_regional.st_possivelmente_extinta; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_avaliacao_regional.st_possivelmente_extinta IS 'informar se a espécie está possivelmente extinta. S ou N';


--
-- TOC entry 311 (class 1259 OID 24534)
-- Name: ficha_avaliacao_regional_sq_ficha_avaliacao_regional_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ficha_avaliacao_regional_sq_ficha_avaliacao_regional_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8013 (class 0 OID 0)
-- Dependencies: 311
-- Name: ficha_avaliacao_regional_sq_ficha_avaliacao_regional_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ficha_avaliacao_regional_sq_ficha_avaliacao_regional_seq OWNED BY salve.ficha_avaliacao_regional.sq_ficha_avaliacao_regional;


--
-- TOC entry 314 (class 1259 OID 24586)
-- Name: ficha_bacia; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_bacia (
    sq_ficha_bacia integer NOT NULL,
    sq_ficha bigint NOT NULL,
    sq_bacia bigint NOT NULL
);


--
-- TOC entry 8014 (class 0 OID 0)
-- Dependencies: 314
-- Name: TABLE ficha_bacia; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON TABLE salve.ficha_bacia IS 'tabela de bacias hidrograficas onde a especie ocorre';


--
-- TOC entry 313 (class 1259 OID 24584)
-- Name: ficha_bacia_sq_ficha_bacia_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ficha_bacia_sq_ficha_bacia_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8015 (class 0 OID 0)
-- Dependencies: 313
-- Name: ficha_bacia_sq_ficha_bacia_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ficha_bacia_sq_ficha_bacia_seq OWNED BY salve.ficha_bacia.sq_ficha_bacia;


--
-- TOC entry 316 (class 1259 OID 24626)
-- Name: ficha_bioma; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_bioma (
    sq_ficha_bioma integer NOT NULL,
    sq_ficha bigint NOT NULL,
    sq_bioma bigint NOT NULL
);


--
-- TOC entry 8016 (class 0 OID 0)
-- Dependencies: 316
-- Name: TABLE ficha_bioma; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON TABLE salve.ficha_bioma IS 'tabela de biomas onde a especie ocorre';


--
-- TOC entry 315 (class 1259 OID 24624)
-- Name: ficha_bioma_sq_ficha_bioma_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ficha_bioma_sq_ficha_bioma_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8017 (class 0 OID 0)
-- Dependencies: 315
-- Name: ficha_bioma_sq_ficha_bioma_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ficha_bioma_sq_ficha_bioma_seq OWNED BY salve.ficha_bioma.sq_ficha_bioma;


--
-- TOC entry 318 (class 1259 OID 24731)
-- Name: ficha_colaboracao; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_colaboracao (
    sq_ficha_colaboracao integer NOT NULL,
    sq_ciclo_consulta_ficha bigint,
    sq_situacao bigint NOT NULL,
    sq_web_usuario bigint NOT NULL,
    no_campo_ficha text NOT NULL,
    ds_colaboracao text,
    ds_ref_bib text,
    dt_inclusao timestamp without time zone DEFAULT now(),
    dt_alteracao timestamp without time zone,
    ds_rotulo text,
    de_rotulo text,
    sq_tipo_consulta bigint,
    tx_nao_aceita text
);


--
-- TOC entry 8018 (class 0 OID 0)
-- Dependencies: 318
-- Name: COLUMN ficha_colaboracao.ds_rotulo; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_colaboracao.ds_rotulo IS 'Nome que será exibido na tela';


--
-- TOC entry 8019 (class 0 OID 0)
-- Dependencies: 318
-- Name: COLUMN ficha_colaboracao.de_rotulo; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_colaboracao.de_rotulo IS 'Nome do campo que será exibido na tela';


--
-- TOC entry 8020 (class 0 OID 0)
-- Dependencies: 318
-- Name: COLUMN ficha_colaboracao.sq_tipo_consulta; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_colaboracao.sq_tipo_consulta IS 'código do tipo da consulta em que a colaboração foi feita. Ex: Direta, Ampla ou Revisão';


--
-- TOC entry 8021 (class 0 OID 0)
-- Dependencies: 318
-- Name: COLUMN ficha_colaboracao.tx_nao_aceita; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_colaboracao.tx_nao_aceita IS 'Campo utilizado para justificar a nao aceitacao da colaboracao.';


--
-- TOC entry 327 (class 1259 OID 24891)
-- Name: ficha_colaboracao_ocorrencia; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_colaboracao_ocorrencia (
    sq_ficha_colaboracao_ocorrencia integer NOT NULL,
    sq_ciclo_consulta_ficha integer NOT NULL,
    sq_situacao bigint NOT NULL,
    sq_web_usuario bigint NOT NULL,
    sq_datum bigint NOT NULL,
    sq_precisao_coordenada bigint NOT NULL,
    sq_ref_aproximacao bigint,
    sq_publicacao bigint,
    no_localidade text,
    tx_ref_bib text,
    tx_observacao text,
    ge_ocorrencia public.geometry NOT NULL,
    dt_inclusao timestamp without time zone DEFAULT now() NOT NULL,
    dt_alteracao timestamp without time zone DEFAULT now() NOT NULL,
    nu_dia_registro integer,
    nu_mes_registro integer,
    nu_ano_registro integer,
    hr_registro text,
    in_data_desconhecida character(1),
    tx_nao_aceita text
);


--
-- TOC entry 8022 (class 0 OID 0)
-- Dependencies: 327
-- Name: COLUMN ficha_colaboracao_ocorrencia.tx_nao_aceita; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_colaboracao_ocorrencia.tx_nao_aceita IS 'Campo utilizado para justificar a nao aceitacao da colaboracao.';


--
-- TOC entry 326 (class 1259 OID 24889)
-- Name: ficha_colaboracao_ocorrencia_sq_ciclo_consulta_ficha_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ficha_colaboracao_ocorrencia_sq_ciclo_consulta_ficha_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8023 (class 0 OID 0)
-- Dependencies: 326
-- Name: ficha_colaboracao_ocorrencia_sq_ciclo_consulta_ficha_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ficha_colaboracao_ocorrencia_sq_ciclo_consulta_ficha_seq OWNED BY salve.ficha_colaboracao_ocorrencia.sq_ciclo_consulta_ficha;


--
-- TOC entry 325 (class 1259 OID 24887)
-- Name: ficha_colaboracao_ocorrencia_sq_ficha_colaboracao_ocorrenci_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ficha_colaboracao_ocorrencia_sq_ficha_colaboracao_ocorrenci_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8024 (class 0 OID 0)
-- Dependencies: 325
-- Name: ficha_colaboracao_ocorrencia_sq_ficha_colaboracao_ocorrenci_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ficha_colaboracao_ocorrencia_sq_ficha_colaboracao_ocorrenci_seq OWNED BY salve.ficha_colaboracao_ocorrencia.sq_ficha_colaboracao_ocorrencia;


--
-- TOC entry 317 (class 1259 OID 24729)
-- Name: ficha_colaboracao_sq_ficha_colaboracao_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ficha_colaboracao_sq_ficha_colaboracao_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8025 (class 0 OID 0)
-- Dependencies: 317
-- Name: ficha_colaboracao_sq_ficha_colaboracao_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ficha_colaboracao_sq_ficha_colaboracao_seq OWNED BY salve.ficha_colaboracao.sq_ficha_colaboracao;


--
-- TOC entry 329 (class 1259 OID 25050)
-- Name: ficha_consulta_anexo; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_consulta_anexo (
    sq_ficha_consulta_anexo integer NOT NULL,
    sq_ciclo_consulta_ficha bigint,
    sq_web_usuario bigint NOT NULL,
    sq_situacao bigint NOT NULL,
    no_arquivo character varying(200) NOT NULL,
    de_local_arquivo character varying(200) NOT NULL,
    de_tipo_conteudo character varying(100) NOT NULL,
    dt_inclusao timestamp without time zone DEFAULT CURRENT_TIMESTAMP
);


--
-- TOC entry 8026 (class 0 OID 0)
-- Dependencies: 329
-- Name: COLUMN ficha_consulta_anexo.sq_ficha_consulta_anexo; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_consulta_anexo.sq_ficha_consulta_anexo IS 'chave primaria sequencial';


--
-- TOC entry 8027 (class 0 OID 0)
-- Dependencies: 329
-- Name: COLUMN ficha_consulta_anexo.sq_ciclo_consulta_ficha; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_consulta_anexo.sq_ciclo_consulta_ficha IS 'chave estrangeira referente a consulta que a ficha esta';


--
-- TOC entry 8028 (class 0 OID 0)
-- Dependencies: 329
-- Name: COLUMN ficha_consulta_anexo.sq_web_usuario; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_consulta_anexo.sq_web_usuario IS 'chave estrangeira do usuario web que enviou o anexo';


--
-- TOC entry 8029 (class 0 OID 0)
-- Dependencies: 329
-- Name: COLUMN ficha_consulta_anexo.sq_situacao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_consulta_anexo.sq_situacao IS 'chave estrangeira da situacao do anexo, se ja foi aceito ou nao';


--
-- TOC entry 8030 (class 0 OID 0)
-- Dependencies: 329
-- Name: COLUMN ficha_consulta_anexo.no_arquivo; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_consulta_anexo.no_arquivo IS 'nome do arquivo enviado';


--
-- TOC entry 8031 (class 0 OID 0)
-- Dependencies: 329
-- Name: COLUMN ficha_consulta_anexo.de_local_arquivo; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_consulta_anexo.de_local_arquivo IS 'nome do arquivo no disco';


--
-- TOC entry 8032 (class 0 OID 0)
-- Dependencies: 329
-- Name: COLUMN ficha_consulta_anexo.de_tipo_conteudo; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_consulta_anexo.de_tipo_conteudo IS 'tipo mime do arquivo:doc,xls,pdf etc';


--
-- TOC entry 8033 (class 0 OID 0)
-- Dependencies: 329
-- Name: COLUMN ficha_consulta_anexo.dt_inclusao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_consulta_anexo.dt_inclusao IS 'data de inclusao na tabela';


--
-- TOC entry 328 (class 1259 OID 25048)
-- Name: ficha_consulta_anexo_sq_ficha_consulta_anexo_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ficha_consulta_anexo_sq_ficha_consulta_anexo_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8034 (class 0 OID 0)
-- Dependencies: 328
-- Name: ficha_consulta_anexo_sq_ficha_consulta_anexo_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ficha_consulta_anexo_sq_ficha_consulta_anexo_seq OWNED BY salve.ficha_consulta_anexo.sq_ficha_consulta_anexo;


--
-- TOC entry 333 (class 1259 OID 25172)
-- Name: ficha_consulta_foto; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_consulta_foto (
    sq_ficha_consulta_foto bigint NOT NULL,
    sq_ciclo_consulta_ficha bigint NOT NULL,
    sq_web_usuario bigint NOT NULL,
    sq_ficha_multimidia bigint NOT NULL,
    in_tipo_termo_resp text DEFAULT 'USO_IRRESTRITO'::text,
    dt_aceite_termo_resp timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    nu_ip_computador text NOT NULL,
    no_navegador text NOT NULL
);


--
-- TOC entry 8035 (class 0 OID 0)
-- Dependencies: 333
-- Name: TABLE ficha_consulta_foto; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON TABLE salve.ficha_consulta_foto IS 'Tabela para registrar as fotos enviadas pelas consultas diretas/amplas';


--
-- TOC entry 8036 (class 0 OID 0)
-- Dependencies: 333
-- Name: COLUMN ficha_consulta_foto.sq_ciclo_consulta_ficha; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_consulta_foto.sq_ciclo_consulta_ficha IS 'chave extrangeira da tabela ciclo_consulta_ficha';


--
-- TOC entry 8037 (class 0 OID 0)
-- Dependencies: 333
-- Name: COLUMN ficha_consulta_foto.sq_web_usuario; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_consulta_foto.sq_web_usuario IS 'chave extrangeira da tabela web_usuario';


--
-- TOC entry 8038 (class 0 OID 0)
-- Dependencies: 333
-- Name: COLUMN ficha_consulta_foto.sq_ficha_multimidia; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_consulta_foto.sq_ficha_multimidia IS 'chave extrangeira da tabela ficha_multimidia';


--
-- TOC entry 8039 (class 0 OID 0)
-- Dependencies: 333
-- Name: COLUMN ficha_consulta_foto.in_tipo_termo_resp; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_consulta_foto.in_tipo_termo_resp IS 'informar qual tipo de termo de responsabilidade o usuario concordou';


--
-- TOC entry 8040 (class 0 OID 0)
-- Dependencies: 333
-- Name: COLUMN ficha_consulta_foto.dt_aceite_termo_resp; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_consulta_foto.dt_aceite_termo_resp IS 'data em que o usuario concordou com o termo de responsabilidade';


--
-- TOC entry 8041 (class 0 OID 0)
-- Dependencies: 333
-- Name: COLUMN ficha_consulta_foto.nu_ip_computador; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_consulta_foto.nu_ip_computador IS 'o ip do computador que o usuario estava utilizando quando concordou com o termo de responsabilidade';


--
-- TOC entry 8042 (class 0 OID 0)
-- Dependencies: 333
-- Name: COLUMN ficha_consulta_foto.no_navegador; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_consulta_foto.no_navegador IS 'o navegador web que o usuario estava utilizando quando concordou com o termo de responsabilidade';


--
-- TOC entry 332 (class 1259 OID 25170)
-- Name: ficha_consulta_foto_sq_ficha_consulta_foto_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ficha_consulta_foto_sq_ficha_consulta_foto_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8043 (class 0 OID 0)
-- Dependencies: 332
-- Name: ficha_consulta_foto_sq_ficha_consulta_foto_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ficha_consulta_foto_sq_ficha_consulta_foto_seq OWNED BY salve.ficha_consulta_foto.sq_ficha_consulta_foto;


--
-- TOC entry 337 (class 1259 OID 25291)
-- Name: ficha_consulta_multimidia; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_consulta_multimidia (
    sq_ficha_consulta_multimidia integer NOT NULL,
    sq_ficha_ocorrencia_consulta bigint NOT NULL,
    sq_ficha_multimidia bigint NOT NULL
);


--
-- TOC entry 336 (class 1259 OID 25289)
-- Name: ficha_consulta_multimidia_sq_ficha_consulta_multimidia_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ficha_consulta_multimidia_sq_ficha_consulta_multimidia_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8044 (class 0 OID 0)
-- Dependencies: 336
-- Name: ficha_consulta_multimidia_sq_ficha_consulta_multimidia_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ficha_consulta_multimidia_sq_ficha_consulta_multimidia_seq OWNED BY salve.ficha_consulta_multimidia.sq_ficha_consulta_multimidia;


--
-- TOC entry 339 (class 1259 OID 25314)
-- Name: ficha_declinio_populacional; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_declinio_populacional (
    sq_ficha_declinio_populacional integer NOT NULL,
    sq_declinio_populacional bigint NOT NULL,
    sq_ficha bigint
);


--
-- TOC entry 338 (class 1259 OID 25312)
-- Name: ficha_declinio_populacional_sq_ficha_declinio_populacional_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ficha_declinio_populacional_sq_ficha_declinio_populacional_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8045 (class 0 OID 0)
-- Dependencies: 338
-- Name: ficha_declinio_populacional_sq_ficha_declinio_populacional_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ficha_declinio_populacional_sq_ficha_declinio_populacional_seq OWNED BY salve.ficha_declinio_populacional.sq_ficha_declinio_populacional;


--
-- TOC entry 341 (class 1259 OID 25333)
-- Name: ficha_eoo; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_eoo (
    sq_ficha_eoo integer NOT NULL,
    sq_ficha bigint NOT NULL,
    ge_eoo public.geometry,
    dt_inclusao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    dt_alteracao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    nu_hydroshed_level integer
);


--
-- TOC entry 8046 (class 0 OID 0)
-- Dependencies: 341
-- Name: TABLE ficha_eoo; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON TABLE salve.ficha_eoo IS 'Tabela para armazenamento do polígono da extensao de ocorrencia - EOO da especie';


--
-- TOC entry 8047 (class 0 OID 0)
-- Dependencies: 341
-- Name: COLUMN ficha_eoo.ge_eoo; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_eoo.ge_eoo IS 'Poligono da Extensao de Ocorrencia da especie';


--
-- TOC entry 8048 (class 0 OID 0)
-- Dependencies: 341
-- Name: COLUMN ficha_eoo.nu_hydroshed_level; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_eoo.nu_hydroshed_level IS 'Nivel da hydroshed utilizada no calculo da EOO';


--
-- TOC entry 340 (class 1259 OID 25331)
-- Name: ficha_eoo_sq_ficha_eoo_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ficha_eoo_sq_ficha_eoo_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8049 (class 0 OID 0)
-- Dependencies: 340
-- Name: ficha_eoo_sq_ficha_eoo_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ficha_eoo_sq_ficha_eoo_seq OWNED BY salve.ficha_eoo.sq_ficha_eoo;


--
-- TOC entry 343 (class 1259 OID 25357)
-- Name: ficha_habitat; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_habitat (
    sq_ficha_habitat integer NOT NULL,
    sq_ficha bigint NOT NULL,
    sq_habitat bigint NOT NULL
);


--
-- TOC entry 342 (class 1259 OID 25355)
-- Name: ficha_habitat_sq_ficha_habitat_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ficha_habitat_sq_ficha_habitat_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8050 (class 0 OID 0)
-- Dependencies: 342
-- Name: ficha_habitat_sq_ficha_habitat_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ficha_habitat_sq_ficha_habitat_seq OWNED BY salve.ficha_habitat.sq_ficha_habitat;


--
-- TOC entry 345 (class 1259 OID 25378)
-- Name: ficha_habito_alimentar; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_habito_alimentar (
    sq_ficha_habito_alimentar integer NOT NULL,
    sq_tipo_habito_alimentar bigint NOT NULL,
    sq_ficha bigint NOT NULL,
    ds_ficha_habito_alimentar text
);


--
-- TOC entry 347 (class 1259 OID 25401)
-- Name: ficha_habito_alimentar_esp; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_habito_alimentar_esp (
    sq_ficha_habito_alimentar_esp integer NOT NULL,
    sq_ficha bigint NOT NULL,
    sq_taxon bigint NOT NULL,
    sq_categoria_iucn bigint,
    ds_ficha_habito_alimentar_esp text
);


--
-- TOC entry 346 (class 1259 OID 25399)
-- Name: ficha_habito_alimentar_esp_sq_ficha_habito_alimentar_esp_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ficha_habito_alimentar_esp_sq_ficha_habito_alimentar_esp_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8051 (class 0 OID 0)
-- Dependencies: 346
-- Name: ficha_habito_alimentar_esp_sq_ficha_habito_alimentar_esp_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ficha_habito_alimentar_esp_sq_ficha_habito_alimentar_esp_seq OWNED BY salve.ficha_habito_alimentar_esp.sq_ficha_habito_alimentar_esp;


--
-- TOC entry 344 (class 1259 OID 25376)
-- Name: ficha_habito_alimentar_sq_ficha_habito_alimentar_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ficha_habito_alimentar_sq_ficha_habito_alimentar_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8052 (class 0 OID 0)
-- Dependencies: 344
-- Name: ficha_habito_alimentar_sq_ficha_habito_alimentar_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ficha_habito_alimentar_sq_ficha_habito_alimentar_seq OWNED BY salve.ficha_habito_alimentar.sq_ficha_habito_alimentar;


--
-- TOC entry 523 (class 1259 OID 29577)
-- Name: ficha_hist_sit_exc_versao; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_hist_sit_exc_versao (
    sq_ficha_hist_situacao_excluida integer NOT NULL,
    sq_ficha_versao bigint NOT NULL
);


--
-- TOC entry 8053 (class 0 OID 0)
-- Dependencies: 523
-- Name: COLUMN ficha_hist_sit_exc_versao.sq_ficha_hist_situacao_excluida; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_hist_sit_exc_versao.sq_ficha_hist_situacao_excluida IS 'Chave estrangeira e primaria';


--
-- TOC entry 8054 (class 0 OID 0)
-- Dependencies: 523
-- Name: COLUMN ficha_hist_sit_exc_versao.sq_ficha_versao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_hist_sit_exc_versao.sq_ficha_versao IS 'Chave estrangeira da tabela ficha_versao';


--
-- TOC entry 349 (class 1259 OID 25431)
-- Name: ficha_hist_situacao_excluida; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_hist_situacao_excluida (
    sq_ficha_hist_situacao_excluida integer NOT NULL,
    sq_ficha bigint NOT NULL,
    sq_usuario_inclusao bigint NOT NULL,
    ds_justificativa text NOT NULL,
    dt_inclusao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


--
-- TOC entry 8055 (class 0 OID 0)
-- Dependencies: 349
-- Name: COLUMN ficha_hist_situacao_excluida.sq_ficha_hist_situacao_excluida; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_hist_situacao_excluida.sq_ficha_hist_situacao_excluida IS 'Chave primaria sequencial da tabela';


--
-- TOC entry 8056 (class 0 OID 0)
-- Dependencies: 349
-- Name: COLUMN ficha_hist_situacao_excluida.sq_ficha; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_hist_situacao_excluida.sq_ficha IS 'Chave estrangeira da tabela ficha';


--
-- TOC entry 8057 (class 0 OID 0)
-- Dependencies: 349
-- Name: COLUMN ficha_hist_situacao_excluida.sq_usuario_inclusao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_hist_situacao_excluida.sq_usuario_inclusao IS 'Chave estrangeira da tabela corporativa pessoa_fisica da pessoa que fez a inclusao';


--
-- TOC entry 8058 (class 0 OID 0)
-- Dependencies: 349
-- Name: COLUMN ficha_hist_situacao_excluida.ds_justificativa; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_hist_situacao_excluida.ds_justificativa IS 'Texto da justificativa da exclusao';


--
-- TOC entry 8059 (class 0 OID 0)
-- Dependencies: 349
-- Name: COLUMN ficha_hist_situacao_excluida.dt_inclusao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_hist_situacao_excluida.dt_inclusao IS 'Data de inclusao no banco de dados';


--
-- TOC entry 348 (class 1259 OID 25429)
-- Name: ficha_hist_situacao_excluida_sq_ficha_hist_situacao_excluid_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ficha_hist_situacao_excluida_sq_ficha_hist_situacao_excluid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8060 (class 0 OID 0)
-- Dependencies: 348
-- Name: ficha_hist_situacao_excluida_sq_ficha_hist_situacao_excluid_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ficha_hist_situacao_excluida_sq_ficha_hist_situacao_excluid_seq OWNED BY salve.ficha_hist_situacao_excluida.sq_ficha_hist_situacao_excluida;


--
-- TOC entry 351 (class 1259 OID 25451)
-- Name: ficha_historico_avaliacao; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_historico_avaliacao (
    sq_ficha_historico_avaliacao integer NOT NULL,
    sq_ficha bigint NOT NULL,
    sq_tipo_avaliacao bigint NOT NULL,
    sq_abrangencia bigint,
    sq_tipo_abrangencia bigint,
    sq_categoria_iucn bigint,
    nu_ano_avaliacao smallint NOT NULL,
    de_criterio_avaliacao_iucn text,
    tx_justificativa_avaliacao text,
    tx_regiao_abrangencia text,
    no_regiao_outra text,
    sq_taxon bigint,
    st_possivelmente_extinta character(1),
    de_categoria_outra text
);


--
-- TOC entry 8061 (class 0 OID 0)
-- Dependencies: 351
-- Name: COLUMN ficha_historico_avaliacao.sq_tipo_abrangencia; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_historico_avaliacao.sq_tipo_abrangencia IS 'Registra o tipo da abrangencia ( Estado, Bioma, costa, ecoregiao...) em dados_apoio tb_tipo_abrangencia';


--
-- TOC entry 8062 (class 0 OID 0)
-- Dependencies: 351
-- Name: COLUMN ficha_historico_avaliacao.sq_taxon; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_historico_avaliacao.sq_taxon IS 'Chave estrangeira tabela taxonomia.taxon';


--
-- TOC entry 8063 (class 0 OID 0)
-- Dependencies: 351
-- Name: COLUMN ficha_historico_avaliacao.st_possivelmente_extinta; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_historico_avaliacao.st_possivelmente_extinta IS 'Preenchido com S/N durante a oficina - para Sim ou NÃ£o';


--
-- TOC entry 8064 (class 0 OID 0)
-- Dependencies: 351
-- Name: COLUMN ficha_historico_avaliacao.de_categoria_outra; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_historico_avaliacao.de_categoria_outra IS 'Quando a categoria nÃ£o for existir na tabela de categorias.';


--
-- TOC entry 350 (class 1259 OID 25449)
-- Name: ficha_historico_avaliacao_sq_ficha_historico_avaliacao_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ficha_historico_avaliacao_sq_ficha_historico_avaliacao_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8065 (class 0 OID 0)
-- Dependencies: 350
-- Name: ficha_historico_avaliacao_sq_ficha_historico_avaliacao_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ficha_historico_avaliacao_sq_ficha_historico_avaliacao_seq OWNED BY salve.ficha_historico_avaliacao.sq_ficha_historico_avaliacao;


--
-- TOC entry 353 (class 1259 OID 25501)
-- Name: ficha_interacao; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_interacao (
    sq_ficha_interacao integer NOT NULL,
    sq_ficha bigint NOT NULL,
    sq_tipo_interacao bigint NOT NULL,
    sq_classificacao bigint,
    sq_taxon bigint NOT NULL,
    sq_categoria_iucn bigint,
    ds_ficha_interacao text
);


--
-- TOC entry 352 (class 1259 OID 25499)
-- Name: ficha_interacao_sq_ficha_interacao_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ficha_interacao_sq_ficha_interacao_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8066 (class 0 OID 0)
-- Dependencies: 352
-- Name: ficha_interacao_sq_ficha_interacao_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ficha_interacao_sq_ficha_interacao_seq OWNED BY salve.ficha_interacao.sq_ficha_interacao;


--
-- TOC entry 355 (class 1259 OID 25541)
-- Name: ficha_lista_convencao; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_lista_convencao (
    sq_ficha_lista_convencao integer NOT NULL,
    sq_ficha bigint NOT NULL,
    sq_lista_convencao bigint NOT NULL,
    nu_ano smallint
);


--
-- TOC entry 354 (class 1259 OID 25539)
-- Name: ficha_lista_convencao_sq_ficha_lista_convencao_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ficha_lista_convencao_sq_ficha_lista_convencao_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8067 (class 0 OID 0)
-- Dependencies: 354
-- Name: ficha_lista_convencao_sq_ficha_lista_convencao_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ficha_lista_convencao_sq_ficha_lista_convencao_seq OWNED BY salve.ficha_lista_convencao.sq_ficha_lista_convencao;


--
-- TOC entry 357 (class 1259 OID 25592)
-- Name: ficha_modulo_publico; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_modulo_publico (
    sq_ficha_modulo_publico bigint NOT NULL,
    sq_ficha bigint NOT NULL,
    st_exibir boolean DEFAULT false NOT NULL,
    dt_alteracao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    sq_pessoa bigint
);


--
-- TOC entry 8068 (class 0 OID 0)
-- Dependencies: 357
-- Name: COLUMN ficha_modulo_publico.sq_ficha_modulo_publico; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_modulo_publico.sq_ficha_modulo_publico IS 'Chave primaria sequencial da tabela ';


--
-- TOC entry 8069 (class 0 OID 0)
-- Dependencies: 357
-- Name: COLUMN ficha_modulo_publico.sq_ficha; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_modulo_publico.sq_ficha IS 'Chave estrangeira da tabela ficha';


--
-- TOC entry 8070 (class 0 OID 0)
-- Dependencies: 357
-- Name: COLUMN ficha_modulo_publico.st_exibir; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_modulo_publico.st_exibir IS 'Indicar se a ficha esta ou nao visivel no modulo publico';


--
-- TOC entry 8071 (class 0 OID 0)
-- Dependencies: 357
-- Name: COLUMN ficha_modulo_publico.dt_alteracao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_modulo_publico.dt_alteracao IS 'Data que a ficha foi colocada ou retirada do modulo publico';


--
-- TOC entry 8072 (class 0 OID 0)
-- Dependencies: 357
-- Name: COLUMN ficha_modulo_publico.sq_pessoa; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_modulo_publico.sq_pessoa IS 'Id do usuario que colocou ou retirou a ficha do modulo publico';


--
-- TOC entry 356 (class 1259 OID 25590)
-- Name: ficha_modulo_publico_sq_ficha_modulo_publico_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ficha_modulo_publico_sq_ficha_modulo_publico_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8073 (class 0 OID 0)
-- Dependencies: 356
-- Name: ficha_modulo_publico_sq_ficha_modulo_publico_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ficha_modulo_publico_sq_ficha_modulo_publico_seq OWNED BY salve.ficha_modulo_publico.sq_ficha_modulo_publico;


--
-- TOC entry 359 (class 1259 OID 25615)
-- Name: ficha_mudanca_categoria; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_mudanca_categoria (
    sq_ficha_mudanca_categoria integer NOT NULL,
    sq_ficha bigint NOT NULL,
    sq_motivo_mudanca bigint NOT NULL,
    tx_ficha_mudanca_categoria text
);


--
-- TOC entry 358 (class 1259 OID 25613)
-- Name: ficha_mudanca_categoria_sq_ficha_mudanca_categoria_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ficha_mudanca_categoria_sq_ficha_mudanca_categoria_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8074 (class 0 OID 0)
-- Dependencies: 358
-- Name: ficha_mudanca_categoria_sq_ficha_mudanca_categoria_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ficha_mudanca_categoria_sq_ficha_mudanca_categoria_seq OWNED BY salve.ficha_mudanca_categoria.sq_ficha_mudanca_categoria;


--
-- TOC entry 331 (class 1259 OID 25137)
-- Name: ficha_multimidia; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_multimidia (
    sq_ficha_multimidia integer NOT NULL,
    sq_ficha bigint NOT NULL,
    sq_tipo bigint NOT NULL,
    sq_datum bigint,
    sq_situacao bigint NOT NULL,
    de_legenda text NOT NULL,
    tx_multimidia text,
    no_autor text NOT NULL,
    de_email_autor text,
    dt_elaboracao timestamp(4) without time zone,
    no_arquivo text NOT NULL,
    no_arquivo_disco text NOT NULL,
    dt_inclusao timestamp without time zone NOT NULL,
    in_principal boolean DEFAULT false NOT NULL,
    de_url text,
    ge_multimidia public.geometry,
    dt_aprovacao timestamp(4) without time zone,
    sq_pessoa_aprovou bigint,
    tx_nao_aceito text,
    dt_reprovacao timestamp without time zone,
    sq_pessoa_reprovou bigint,
    in_destaque boolean DEFAULT false NOT NULL
);


--
-- TOC entry 8075 (class 0 OID 0)
-- Dependencies: 331
-- Name: COLUMN ficha_multimidia.sq_tipo; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_multimidia.sq_tipo IS 'Tipo som, imagem ou video';


--
-- TOC entry 8076 (class 0 OID 0)
-- Dependencies: 331
-- Name: COLUMN ficha_multimidia.de_legenda; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_multimidia.de_legenda IS 'Descricao curta da imagem, som ou video';


--
-- TOC entry 8077 (class 0 OID 0)
-- Dependencies: 331
-- Name: COLUMN ficha_multimidia.tx_multimidia; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_multimidia.tx_multimidia IS 'Descricao detalhada da imagem, som ou video';


--
-- TOC entry 8078 (class 0 OID 0)
-- Dependencies: 331
-- Name: COLUMN ficha_multimidia.no_arquivo; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_multimidia.no_arquivo IS 'Nome do arquivo original';


--
-- TOC entry 8079 (class 0 OID 0)
-- Dependencies: 331
-- Name: COLUMN ficha_multimidia.no_arquivo_disco; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_multimidia.no_arquivo_disco IS 'Hash do arquivo salvo no disco';


--
-- TOC entry 8080 (class 0 OID 0)
-- Dependencies: 331
-- Name: COLUMN ficha_multimidia.in_principal; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_multimidia.in_principal IS 'Cada ficha podera ter somente uma foto marcada como principal. Esta sera a foto impressa na ficha.';


--
-- TOC entry 8081 (class 0 OID 0)
-- Dependencies: 331
-- Name: COLUMN ficha_multimidia.de_url; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_multimidia.de_url IS 'Url da imagem, video no youtube ou do som quando a mídia não ficar armazenada no icmbio';


--
-- TOC entry 8082 (class 0 OID 0)
-- Dependencies: 331
-- Name: COLUMN ficha_multimidia.ge_multimidia; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_multimidia.ge_multimidia IS 'Coordenada da foto, video ou som do animal';


--
-- TOC entry 8083 (class 0 OID 0)
-- Dependencies: 331
-- Name: COLUMN ficha_multimidia.dt_aprovacao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_multimidia.dt_aprovacao IS 'Data e hora que a foto foi aprovada para visualizacao publica';


--
-- TOC entry 8084 (class 0 OID 0)
-- Dependencies: 331
-- Name: COLUMN ficha_multimidia.sq_pessoa_aprovou; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_multimidia.sq_pessoa_aprovou IS 'Id da pessoa que aprovou a foto';


--
-- TOC entry 8085 (class 0 OID 0)
-- Dependencies: 331
-- Name: COLUMN ficha_multimidia.tx_nao_aceito; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_multimidia.tx_nao_aceito IS 'Descricao do motivo porque o registro nao foi aceito.';


--
-- TOC entry 8086 (class 0 OID 0)
-- Dependencies: 331
-- Name: COLUMN ficha_multimidia.dt_reprovacao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_multimidia.dt_reprovacao IS 'Data e hora que o registro foi reprovado.';


--
-- TOC entry 8087 (class 0 OID 0)
-- Dependencies: 331
-- Name: COLUMN ficha_multimidia.sq_pessoa_reprovou; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_multimidia.sq_pessoa_reprovou IS 'Id da pessoa que reprovou o registro';


--
-- TOC entry 330 (class 1259 OID 25135)
-- Name: ficha_multimidia_sq_ficha_multimidia_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ficha_multimidia_sq_ficha_multimidia_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8088 (class 0 OID 0)
-- Dependencies: 330
-- Name: ficha_multimidia_sq_ficha_multimidia_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ficha_multimidia_sq_ficha_multimidia_seq OWNED BY salve.ficha_multimidia.sq_ficha_multimidia;


--
-- TOC entry 361 (class 1259 OID 25636)
-- Name: ficha_nome_comum; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_nome_comum (
    sq_ficha_nome_comum integer NOT NULL,
    sq_ficha bigint NOT NULL,
    no_comum text NOT NULL,
    de_regiao_lingua text
);


--
-- TOC entry 360 (class 1259 OID 25634)
-- Name: ficha_nome_comum_sq_ficha_nome_comum_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ficha_nome_comum_sq_ficha_nome_comum_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8089 (class 0 OID 0)
-- Dependencies: 360
-- Name: ficha_nome_comum_sq_ficha_nome_comum_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ficha_nome_comum_sq_ficha_nome_comum_seq OWNED BY salve.ficha_nome_comum.sq_ficha_nome_comum;


--
-- TOC entry 296 (class 1259 OID 23952)
-- Name: ficha_ocorrencia; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_ocorrencia (
    sq_ficha_ocorrencia integer NOT NULL,
    sq_ficha bigint NOT NULL,
    sq_tipo_registro bigint,
    sq_ref_aproximacao bigint,
    sq_datum bigint,
    sq_precisao_coordenada bigint,
    sq_formato_coord_original bigint,
    sq_continente bigint,
    sq_pais bigint,
    sq_qualificador_val_taxon bigint,
    sq_prazo_carencia bigint,
    sq_estado bigint,
    sq_municipio bigint,
    sq_metodo_aproximacao bigint,
    sq_taxon_citado bigint,
    sq_bacia_hidrografica bigint,
    sq_bioma bigint,
    sq_uc_federal bigint,
    sq_uc_estadual bigint,
    sq_rppn bigint,
    sq_habitat bigint,
    sq_pessoa_compilador bigint,
    sq_pessoa_revisor bigint,
    sq_pessoa_validador bigint,
    sq_contexto bigint,
    sq_tipo_abrangencia bigint,
    sq_divisao_administrativa bigint,
    sq_ecoregiao bigint,
    tx_metodo_aproximacao text,
    tx_local text,
    no_localidade text,
    dt_registro date,
    hr_registro text,
    vl_elevacao_min numeric(10,2),
    vl_elevacao_max numeric(10,2),
    vl_profundidade_min numeric(10,2),
    vl_profundidade_max numeric(10,2),
    de_identificador text,
    dt_identificacao date,
    de_centro_responsavel text,
    in_presenca_atual character(1),
    tx_tombamento text,
    tx_instituicao text,
    dt_compilacao date,
    dt_revisao date,
    dt_validacao date,
    nu_dia_registro integer,
    nu_mes_registro integer,
    nu_ano_registro integer,
    in_data_desconhecida character(1),
    tx_nao_utilizado_avaliacao text,
    in_utilizado_avaliacao character(1),
    dt_invalidado timestamp without time zone,
    ge_ocorrencia public.geometry,
    sq_abrangencia bigint,
    dt_inclusao timestamp without time zone,
    sq_pessoa_inclusao bigint,
    sq_situacao bigint,
    dt_alteracao timestamp without time zone,
    sq_pessoa_alteracao bigint,
    nu_dia_registro_fim integer,
    nu_mes_registro_fim integer,
    nu_ano_registro_fim integer,
    id_origem character varying(50),
    tx_observacao text,
    tx_nao_aceita text,
    nu_altitude integer,
    in_sensivel character(1) DEFAULT 'N'::bpchar,
    in_processamento character varying(10),
    sq_motivo_nao_utilizado_avaliacao bigint,
    st_adicionado_apos_avaliacao boolean DEFAULT false NOT NULL,
    sq_situacao_avaliacao bigint DEFAULT 1331 NOT NULL
);


--
-- TOC entry 8090 (class 0 OID 0)
-- Dependencies: 296
-- Name: COLUMN ficha_ocorrencia.sq_contexto; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ocorrencia.sq_contexto IS 'Definir o contexto da ocorrência, sendo: uf, bioma, bacia hidrografica ou registro de ocorrência';


--
-- TOC entry 8091 (class 0 OID 0)
-- Dependencies: 296
-- Name: COLUMN ficha_ocorrencia.sq_tipo_abrangencia; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ocorrencia.sq_tipo_abrangencia IS 'Registrar o tipo da abrangência. ex: uf,bioma,bacia etc ';


--
-- TOC entry 8092 (class 0 OID 0)
-- Dependencies: 296
-- Name: COLUMN ficha_ocorrencia.tx_local; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ocorrencia.tx_local IS 'Observações sobre o local ou descrição do local';


--
-- TOC entry 8093 (class 0 OID 0)
-- Dependencies: 296
-- Name: COLUMN ficha_ocorrencia.in_presenca_atual; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ocorrencia.in_presenca_atual IS 'Indicação da presença atual da espécie na coordenada.';


--
-- TOC entry 8094 (class 0 OID 0)
-- Dependencies: 296
-- Name: COLUMN ficha_ocorrencia.in_data_desconhecida; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ocorrencia.in_data_desconhecida IS 'informação se a data/ano do registro da ocorrência são desconhecidos ou não.';


--
-- TOC entry 8095 (class 0 OID 0)
-- Dependencies: 296
-- Name: COLUMN ficha_ocorrencia.tx_nao_utilizado_avaliacao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ocorrencia.tx_nao_utilizado_avaliacao IS 'Justificativa quando não for utilizado na avaliação';


--
-- TOC entry 8096 (class 0 OID 0)
-- Dependencies: 296
-- Name: COLUMN ficha_ocorrencia.in_utilizado_avaliacao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ocorrencia.in_utilizado_avaliacao IS 'Informa se o registro foi ou não utilizado na avaliação';


--
-- TOC entry 8097 (class 0 OID 0)
-- Dependencies: 296
-- Name: COLUMN ficha_ocorrencia.dt_invalidado; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ocorrencia.dt_invalidado IS 'Data que a coordenada foi marcada como inválida';


--
-- TOC entry 8098 (class 0 OID 0)
-- Dependencies: 296
-- Name: COLUMN ficha_ocorrencia.sq_situacao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ocorrencia.sq_situacao IS 'informar se o registro esta aceito, nao aceito, nao validado ou resolver em oficina';


--
-- TOC entry 8099 (class 0 OID 0)
-- Dependencies: 296
-- Name: COLUMN ficha_ocorrencia.nu_dia_registro_fim; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ocorrencia.nu_dia_registro_fim IS 'Dia final do período do registro';


--
-- TOC entry 8100 (class 0 OID 0)
-- Dependencies: 296
-- Name: COLUMN ficha_ocorrencia.nu_mes_registro_fim; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ocorrencia.nu_mes_registro_fim IS 'Mês final do período do registro';


--
-- TOC entry 8101 (class 0 OID 0)
-- Dependencies: 296
-- Name: COLUMN ficha_ocorrencia.nu_ano_registro_fim; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ocorrencia.nu_ano_registro_fim IS 'Ano final do período de registro';


--
-- TOC entry 8102 (class 0 OID 0)
-- Dependencies: 296
-- Name: COLUMN ficha_ocorrencia.id_origem; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ocorrencia.id_origem IS 'Identificador único do registro na tabela de origem para as ocorrências importadas de outros bancos de dados';


--
-- TOC entry 8103 (class 0 OID 0)
-- Dependencies: 296
-- Name: COLUMN ficha_ocorrencia.tx_observacao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ocorrencia.tx_observacao IS 'Campo para informações adicionais sobre o registro de ocorrência.';


--
-- TOC entry 8104 (class 0 OID 0)
-- Dependencies: 296
-- Name: COLUMN ficha_ocorrencia.tx_nao_aceita; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ocorrencia.tx_nao_aceita IS 'Campo utilizado para justificar a nao aceitacao da colaboracao.';


--
-- TOC entry 8105 (class 0 OID 0)
-- Dependencies: 296
-- Name: COLUMN ficha_ocorrencia.nu_altitude; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ocorrencia.nu_altitude IS 'valor da altitude em que a especie foi localizada';


--
-- TOC entry 8106 (class 0 OID 0)
-- Dependencies: 296
-- Name: COLUMN ficha_ocorrencia.in_sensivel; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ocorrencia.in_sensivel IS 'informar se o ponto da ocorrência é um dado sensivel para divulgacao publica';


--
-- TOC entry 8107 (class 0 OID 0)
-- Dependencies: 296
-- Name: COLUMN ficha_ocorrencia.in_processamento; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ocorrencia.in_processamento IS 'informação para controle do processamento de atualizacao do estado, bioma e ucs na ficha da especie.';


--
-- TOC entry 8108 (class 0 OID 0)
-- Dependencies: 296
-- Name: COLUMN ficha_ocorrencia.sq_motivo_nao_utilizado_avaliacao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ocorrencia.sq_motivo_nao_utilizado_avaliacao IS 'Id do motivo do registro nao ter sido utilizado na avaliacao.';


--
-- TOC entry 8109 (class 0 OID 0)
-- Dependencies: 296
-- Name: COLUMN ficha_ocorrencia.st_adicionado_apos_avaliacao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ocorrencia.st_adicionado_apos_avaliacao IS 'true/false se o registro foi adicionado após a avaliação';


--
-- TOC entry 8110 (class 0 OID 0)
-- Dependencies: 296
-- Name: COLUMN ficha_ocorrencia.sq_situacao_avaliacao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ocorrencia.sq_situacao_avaliacao IS 'Informar a situação do registro durante a avaliação. Ex: utilizado, nao utilizado etc';


--
-- TOC entry 335 (class 1259 OID 25251)
-- Name: ficha_ocorrencia_consulta; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_ocorrencia_consulta (
    sq_ficha_ocorrencia_consulta integer NOT NULL,
    sq_ciclo_consulta_ficha bigint NOT NULL,
    sq_ficha_ocorrencia bigint NOT NULL,
    sq_web_usuario bigint NOT NULL,
    tx_observacao text,
    sq_tipo_consulta bigint,
    sq_situacao bigint
);


--
-- TOC entry 8111 (class 0 OID 0)
-- Dependencies: 335
-- Name: COLUMN ficha_ocorrencia_consulta.sq_tipo_consulta; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ocorrencia_consulta.sq_tipo_consulta IS 'código do tipo da consulta em que a colaboração foi feita. Ex: Direta, Ampla ou Revisão';


--
-- TOC entry 8112 (class 0 OID 0)
-- Dependencies: 335
-- Name: COLUMN ficha_ocorrencia_consulta.sq_situacao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ocorrencia_consulta.sq_situacao IS 'Situacao da colaboracao do ponto antes e apos a consolidacao';


--
-- TOC entry 334 (class 1259 OID 25249)
-- Name: ficha_ocorrencia_consulta_sq_ficha_ocorrencia_consulta_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ficha_ocorrencia_consulta_sq_ficha_ocorrencia_consulta_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8113 (class 0 OID 0)
-- Dependencies: 334
-- Name: ficha_ocorrencia_consulta_sq_ficha_ocorrencia_consulta_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ficha_ocorrencia_consulta_sq_ficha_ocorrencia_consulta_seq OWNED BY salve.ficha_ocorrencia_consulta.sq_ficha_ocorrencia_consulta;


--
-- TOC entry 363 (class 1259 OID 25658)
-- Name: ficha_ocorrencia_multimidia; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_ocorrencia_multimidia (
    sq_ficha_ocorrencia_multimidia integer NOT NULL,
    sq_ficha_ocorrencia bigint NOT NULL,
    sq_ficha_multimidia bigint NOT NULL
);


--
-- TOC entry 8114 (class 0 OID 0)
-- Dependencies: 363
-- Name: COLUMN ficha_ocorrencia_multimidia.sq_ficha_ocorrencia; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ocorrencia_multimidia.sq_ficha_ocorrencia IS 'Chave extrangeira da tabela ficha_ocorrencia';


--
-- TOC entry 8115 (class 0 OID 0)
-- Dependencies: 363
-- Name: COLUMN ficha_ocorrencia_multimidia.sq_ficha_multimidia; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ocorrencia_multimidia.sq_ficha_multimidia IS 'Chave extrangeira da tabela ficha_multimidia';


--
-- TOC entry 362 (class 1259 OID 25656)
-- Name: ficha_ocorrencia_multimidia_sq_ficha_ocorrencia_multimidia_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ficha_ocorrencia_multimidia_sq_ficha_ocorrencia_multimidia_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8116 (class 0 OID 0)
-- Dependencies: 362
-- Name: ficha_ocorrencia_multimidia_sq_ficha_ocorrencia_multimidia_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ficha_ocorrencia_multimidia_sq_ficha_ocorrencia_multimidia_seq OWNED BY salve.ficha_ocorrencia_multimidia.sq_ficha_ocorrencia_multimidia;


--
-- TOC entry 365 (class 1259 OID 25704)
-- Name: ficha_ocorrencia_portalbio; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_ocorrencia_portalbio (
    sq_ficha_ocorrencia_portalbio integer NOT NULL,
    sq_ficha bigint NOT NULL,
    de_uuid text NOT NULL,
    no_cientifico text NOT NULL,
    no_local text,
    no_instituicao text,
    no_autor text,
    dt_ocorrencia date,
    dt_carencia date,
    de_ameaca text,
    tx_ocorrencia text NOT NULL,
    ge_ocorrencia public.geometry NOT NULL,
    sq_situacao bigint,
    sq_pessoa_validador bigint,
    dt_validacao timestamp without time zone,
    sq_pessoa_revisor bigint,
    dt_revisao timestamp without time zone,
    in_utilizado_avaliacao character varying(1),
    tx_nao_utilizado_avaliacao text,
    tx_observacao text,
    id_origem bigint,
    dt_alteracao timestamp without time zone,
    no_base_dados character varying(50),
    sq_pais bigint,
    sq_estado bigint,
    sq_municipio bigint,
    sq_bioma bigint,
    sq_uc_federal bigint,
    sq_uc_estadual bigint,
    sq_rppn bigint,
    in_processamento character varying(10),
    sq_bacia_hidrografica bigint,
    sq_motivo_nao_utilizado_avaliacao bigint,
    in_data_desconhecida character(1),
    tx_nao_aceita text,
    st_adicionado_apos_avaliacao boolean DEFAULT false NOT NULL,
    sq_situacao_avaliacao bigint DEFAULT 1331 NOT NULL,
    in_presenca_atual character(1) DEFAULT 'S'::bpchar NOT NULL
);


--
-- TOC entry 8117 (class 0 OID 0)
-- Dependencies: 365
-- Name: TABLE ficha_ocorrencia_portalbio; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON TABLE salve.ficha_ocorrencia_portalbio IS 'Registrar as ocorrêcias do portalbio que foram utilizadas ou não na avaliação';


--
-- TOC entry 8118 (class 0 OID 0)
-- Dependencies: 365
-- Name: COLUMN ficha_ocorrencia_portalbio.sq_situacao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ocorrencia_portalbio.sq_situacao IS 'Situação do registro';


--
-- TOC entry 8119 (class 0 OID 0)
-- Dependencies: 365
-- Name: COLUMN ficha_ocorrencia_portalbio.sq_pessoa_validador; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ocorrencia_portalbio.sq_pessoa_validador IS 'Pessoa que validou o registro';


--
-- TOC entry 8120 (class 0 OID 0)
-- Dependencies: 365
-- Name: COLUMN ficha_ocorrencia_portalbio.dt_validacao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ocorrencia_portalbio.dt_validacao IS 'Data e hora da validacao';


--
-- TOC entry 8121 (class 0 OID 0)
-- Dependencies: 365
-- Name: COLUMN ficha_ocorrencia_portalbio.sq_pessoa_revisor; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ocorrencia_portalbio.sq_pessoa_revisor IS 'Pessoa que fez a revisao do registro';


--
-- TOC entry 8122 (class 0 OID 0)
-- Dependencies: 365
-- Name: COLUMN ficha_ocorrencia_portalbio.dt_revisao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ocorrencia_portalbio.dt_revisao IS 'Data e hora da revisao';


--
-- TOC entry 8123 (class 0 OID 0)
-- Dependencies: 365
-- Name: COLUMN ficha_ocorrencia_portalbio.in_utilizado_avaliacao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ocorrencia_portalbio.in_utilizado_avaliacao IS 'Indica se o registro foi ou nao utilizado na avaliacao da especie';


--
-- TOC entry 8124 (class 0 OID 0)
-- Dependencies: 365
-- Name: COLUMN ficha_ocorrencia_portalbio.tx_nao_utilizado_avaliacao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ocorrencia_portalbio.tx_nao_utilizado_avaliacao IS 'Justificativa de não utilização do registro no processo de avaliação';


--
-- TOC entry 8125 (class 0 OID 0)
-- Dependencies: 365
-- Name: COLUMN ficha_ocorrencia_portalbio.tx_observacao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ocorrencia_portalbio.tx_observacao IS 'Campo para informações adicionais sobre o registro de ocorrência.';


--
-- TOC entry 8126 (class 0 OID 0)
-- Dependencies: 365
-- Name: COLUMN ficha_ocorrencia_portalbio.id_origem; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ocorrencia_portalbio.id_origem IS 'Identificador Ãºnico do registro na base de dados de origem.';


--
-- TOC entry 8127 (class 0 OID 0)
-- Dependencies: 365
-- Name: COLUMN ficha_ocorrencia_portalbio.dt_alteracao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ocorrencia_portalbio.dt_alteracao IS 'Data da Ãºltima dt_alteracao do registro';


--
-- TOC entry 8128 (class 0 OID 0)
-- Dependencies: 365
-- Name: COLUMN ficha_ocorrencia_portalbio.no_base_dados; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ocorrencia_portalbio.no_base_dados IS 'Nome da base de dados de origem da informaÃ§Ã£o. Ex: SISBIO,ARA,JBRJ';


--
-- TOC entry 8129 (class 0 OID 0)
-- Dependencies: 365
-- Name: COLUMN ficha_ocorrencia_portalbio.in_processamento; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ocorrencia_portalbio.in_processamento IS 'Informação para controle do processamento de atualizacao do estado, bioma e ucs na ficha da especie.';


--
-- TOC entry 8130 (class 0 OID 0)
-- Dependencies: 365
-- Name: COLUMN ficha_ocorrencia_portalbio.sq_bacia_hidrografica; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ocorrencia_portalbio.sq_bacia_hidrografica IS 'Bacia hidrografica que o ponto pertence';


--
-- TOC entry 8131 (class 0 OID 0)
-- Dependencies: 365
-- Name: COLUMN ficha_ocorrencia_portalbio.sq_motivo_nao_utilizado_avaliacao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ocorrencia_portalbio.sq_motivo_nao_utilizado_avaliacao IS 'Id do motivo do registro nao ter sido utilizado na avaliacao.';


--
-- TOC entry 8132 (class 0 OID 0)
-- Dependencies: 365
-- Name: COLUMN ficha_ocorrencia_portalbio.in_data_desconhecida; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ocorrencia_portalbio.in_data_desconhecida IS 'Informar se a data do registro da ocorrência é desconhecida ou não: S/N.';


--
-- TOC entry 8133 (class 0 OID 0)
-- Dependencies: 365
-- Name: COLUMN ficha_ocorrencia_portalbio.tx_nao_aceita; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ocorrencia_portalbio.tx_nao_aceita IS 'Motivo pelo qual a ocorrencia nao foi aceita na avaliacao';


--
-- TOC entry 8134 (class 0 OID 0)
-- Dependencies: 365
-- Name: COLUMN ficha_ocorrencia_portalbio.st_adicionado_apos_avaliacao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ocorrencia_portalbio.st_adicionado_apos_avaliacao IS 'True/False se o registro foi adicionado após a avaliação';


--
-- TOC entry 8135 (class 0 OID 0)
-- Dependencies: 365
-- Name: COLUMN ficha_ocorrencia_portalbio.sq_situacao_avaliacao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ocorrencia_portalbio.sq_situacao_avaliacao IS 'Informar a situação do registro durante a avaliação. Ex: utilizado, nao utilizado etc';


--
-- TOC entry 369 (class 1259 OID 25788)
-- Name: ficha_ocorrencia_portalbio_reg; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_ocorrencia_portalbio_reg (
    sq_ficha_ocorrencia_portalbio_reg bigint NOT NULL,
    sq_ficha_ocorrencia_portalbio bigint NOT NULL,
    sq_registro bigint NOT NULL,
    dt_inclusao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


--
-- TOC entry 8136 (class 0 OID 0)
-- Dependencies: 369
-- Name: TABLE ficha_ocorrencia_portalbio_reg; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON TABLE salve.ficha_ocorrencia_portalbio_reg IS 'tabela para integração da ocorrencia do portalbio com a tabela de pontos';


--
-- TOC entry 368 (class 1259 OID 25786)
-- Name: ficha_ocorrencia_portalbio_re_sq_ficha_ocorrencia_portalbio_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ficha_ocorrencia_portalbio_re_sq_ficha_ocorrencia_portalbio_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8137 (class 0 OID 0)
-- Dependencies: 368
-- Name: ficha_ocorrencia_portalbio_re_sq_ficha_ocorrencia_portalbio_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ficha_ocorrencia_portalbio_re_sq_ficha_ocorrencia_portalbio_seq OWNED BY salve.ficha_ocorrencia_portalbio_reg.sq_ficha_ocorrencia_portalbio_reg;


--
-- TOC entry 364 (class 1259 OID 25702)
-- Name: ficha_ocorrencia_portalbio_sq_ficha_ocorrencia_portalbio_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ficha_ocorrencia_portalbio_sq_ficha_ocorrencia_portalbio_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8138 (class 0 OID 0)
-- Dependencies: 364
-- Name: ficha_ocorrencia_portalbio_sq_ficha_ocorrencia_portalbio_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ficha_ocorrencia_portalbio_sq_ficha_ocorrencia_portalbio_seq OWNED BY salve.ficha_ocorrencia_portalbio.sq_ficha_ocorrencia_portalbio;


--
-- TOC entry 371 (class 1259 OID 25819)
-- Name: ficha_ocorrencia_registro; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_ocorrencia_registro (
    sq_ficha_ocorrencia_registro bigint NOT NULL,
    sq_ficha_ocorrencia bigint NOT NULL,
    sq_registro bigint NOT NULL,
    dt_inclusao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


--
-- TOC entry 8139 (class 0 OID 0)
-- Dependencies: 371
-- Name: TABLE ficha_ocorrencia_registro; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON TABLE salve.ficha_ocorrencia_registro IS 'tabela para integração da ocorrencia com a tabela pontos (registro)';


--
-- TOC entry 370 (class 1259 OID 25817)
-- Name: ficha_ocorrencia_registro_sq_ficha_ocorrencia_registro_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ficha_ocorrencia_registro_sq_ficha_ocorrencia_registro_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8140 (class 0 OID 0)
-- Dependencies: 370
-- Name: ficha_ocorrencia_registro_sq_ficha_ocorrencia_registro_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ficha_ocorrencia_registro_sq_ficha_ocorrencia_registro_seq OWNED BY salve.ficha_ocorrencia_registro.sq_ficha_ocorrencia_registro;


--
-- TOC entry 295 (class 1259 OID 23950)
-- Name: ficha_ocorrencia_sq_ficha_ocorrencia_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ficha_ocorrencia_sq_ficha_ocorrencia_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8141 (class 0 OID 0)
-- Dependencies: 295
-- Name: ficha_ocorrencia_sq_ficha_ocorrencia_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ficha_ocorrencia_sq_ficha_ocorrencia_seq OWNED BY salve.ficha_ocorrencia.sq_ficha_ocorrencia;


--
-- TOC entry 373 (class 1259 OID 25876)
-- Name: ficha_ocorrencia_validacao; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_ocorrencia_validacao (
    sq_ficha_ocorrencia_validacao integer NOT NULL,
    sq_ficha_ocorrencia bigint,
    sq_ficha_ocorrencia_portalbio bigint,
    sq_web_usuario bigint,
    in_valido character(1),
    tx_observacao text,
    dt_inclusao timestamp without time zone NOT NULL,
    sq_ciclo_consulta_ficha bigint,
    sq_situacao bigint,
    CONSTRAINT ck_ficha_oco_in_valido CHECK (((in_valido IS NULL) OR (in_valido = ANY (ARRAY['S'::bpchar, 'N'::bpchar]))))
);


--
-- TOC entry 8142 (class 0 OID 0)
-- Dependencies: 373
-- Name: TABLE ficha_ocorrencia_validacao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON TABLE salve.ficha_ocorrencia_validacao IS 'tabela para registrar as sugestoes de validacao da coordenada se e valida ou nao pelos validadores no modulo consulta externa';


--
-- TOC entry 8143 (class 0 OID 0)
-- Dependencies: 373
-- Name: COLUMN ficha_ocorrencia_validacao.in_valido; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ocorrencia_validacao.in_valido IS 'S/N';


--
-- TOC entry 8144 (class 0 OID 0)
-- Dependencies: 373
-- Name: COLUMN ficha_ocorrencia_validacao.sq_situacao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ocorrencia_validacao.sq_situacao IS 'Situacao da colaboracao antes e apos a consolidacao';


--
-- TOC entry 372 (class 1259 OID 25874)
-- Name: ficha_ocorrencia_validacao_sq_ficha_ocorrencia_validacao_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ficha_ocorrencia_validacao_sq_ficha_ocorrencia_validacao_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8145 (class 0 OID 0)
-- Dependencies: 372
-- Name: ficha_ocorrencia_validacao_sq_ficha_ocorrencia_validacao_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ficha_ocorrencia_validacao_sq_ficha_ocorrencia_validacao_seq OWNED BY salve.ficha_ocorrencia_validacao.sq_ficha_ocorrencia_validacao;


--
-- TOC entry 522 (class 1259 OID 29560)
-- Name: ficha_ocorrencia_validacao_versao; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_ocorrencia_validacao_versao (
    sq_ficha_ocorrencia_validacao bigint NOT NULL,
    sq_ficha_versao bigint
);


--
-- TOC entry 8146 (class 0 OID 0)
-- Dependencies: 522
-- Name: COLUMN ficha_ocorrencia_validacao_versao.sq_ficha_ocorrencia_validacao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ocorrencia_validacao_versao.sq_ficha_ocorrencia_validacao IS 'Chave estrangeira e primaria da tabela ficha_ocorrencia_validacao';


--
-- TOC entry 8147 (class 0 OID 0)
-- Dependencies: 522
-- Name: COLUMN ficha_ocorrencia_validacao_versao.sq_ficha_versao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ocorrencia_validacao_versao.sq_ficha_versao IS 'Chave estrangeira da tabela ficha_versao';


--
-- TOC entry 375 (class 1259 OID 25925)
-- Name: ficha_pendencia; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_pendencia (
    sq_ficha_pendencia integer NOT NULL,
    sq_usuario bigint NOT NULL,
    sq_ficha bigint NOT NULL,
    de_assunto text,
    tx_pendencia text NOT NULL,
    dt_pendencia date NOT NULL,
    st_pendente character(1) DEFAULT 'S'::bpchar NOT NULL,
    sq_contexto bigint,
    sq_usuario_resolveu bigint,
    dt_resolvida timestamp without time zone,
    sq_usuario_revisou bigint,
    dt_revisao timestamp without time zone,
    CONSTRAINT ckc_st_pendente_ficha_pe CHECK ((st_pendente = ANY (ARRAY['S'::bpchar, 'N'::bpchar])))
);


--
-- TOC entry 8148 (class 0 OID 0)
-- Dependencies: 375
-- Name: COLUMN ficha_pendencia.sq_contexto; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_pendencia.sq_contexto IS 'Informa em qual contexto a pendência foi gerada. Ex: cadastro, validação, oficina etc.';


--
-- TOC entry 8149 (class 0 OID 0)
-- Dependencies: 375
-- Name: COLUMN ficha_pendencia.sq_usuario_resolveu; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_pendencia.sq_usuario_resolveu IS 'id do usuario que marcou a pendência como resolvida';


--
-- TOC entry 8150 (class 0 OID 0)
-- Dependencies: 375
-- Name: COLUMN ficha_pendencia.dt_resolvida; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_pendencia.dt_resolvida IS 'data que a pendência foi resolvida';


--
-- TOC entry 8151 (class 0 OID 0)
-- Dependencies: 375
-- Name: COLUMN ficha_pendencia.sq_usuario_revisou; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_pendencia.sq_usuario_revisou IS 'id do usuario que confirmou se a pendencia foi resolvida';


--
-- TOC entry 8152 (class 0 OID 0)
-- Dependencies: 375
-- Name: COLUMN ficha_pendencia.dt_revisao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_pendencia.dt_revisao IS 'data que o usuario confirmou que a pendencia foi removida';


--
-- TOC entry 377 (class 1259 OID 25966)
-- Name: ficha_pendencia_email; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_pendencia_email (
    sq_ficha_pendencia_email bigint NOT NULL,
    sq_ficha_pendencia bigint NOT NULL,
    dt_envio date,
    tx_log text,
    dt_inclusao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


--
-- TOC entry 8153 (class 0 OID 0)
-- Dependencies: 377
-- Name: COLUMN ficha_pendencia_email.sq_ficha_pendencia_email; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_pendencia_email.sq_ficha_pendencia_email IS 'Chave primaria sequencial da tabela ficha_pendencia_email';


--
-- TOC entry 8154 (class 0 OID 0)
-- Dependencies: 377
-- Name: COLUMN ficha_pendencia_email.sq_ficha_pendencia; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_pendencia_email.sq_ficha_pendencia IS 'Chave estrangeira da tabela ficha_pendencia';


--
-- TOC entry 8155 (class 0 OID 0)
-- Dependencies: 377
-- Name: COLUMN ficha_pendencia_email.dt_envio; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_pendencia_email.dt_envio IS 'Data de envio do email, se null significa que nao houve envio';


--
-- TOC entry 8156 (class 0 OID 0)
-- Dependencies: 377
-- Name: COLUMN ficha_pendencia_email.tx_log; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_pendencia_email.tx_log IS 'Log do envio realizado com o nome e email para quem foi enviado';


--
-- TOC entry 8157 (class 0 OID 0)
-- Dependencies: 377
-- Name: COLUMN ficha_pendencia_email.dt_inclusao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_pendencia_email.dt_inclusao IS 'Data de criacao da pendencia';


--
-- TOC entry 376 (class 1259 OID 25964)
-- Name: ficha_pendencia_email_sq_ficha_pendencia_email_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ficha_pendencia_email_sq_ficha_pendencia_email_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8158 (class 0 OID 0)
-- Dependencies: 376
-- Name: ficha_pendencia_email_sq_ficha_pendencia_email_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ficha_pendencia_email_sq_ficha_pendencia_email_seq OWNED BY salve.ficha_pendencia_email.sq_ficha_pendencia_email;


--
-- TOC entry 374 (class 1259 OID 25923)
-- Name: ficha_pendencia_sq_ficha_pendencia_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ficha_pendencia_sq_ficha_pendencia_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8159 (class 0 OID 0)
-- Dependencies: 374
-- Name: ficha_pendencia_sq_ficha_pendencia_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ficha_pendencia_sq_ficha_pendencia_seq OWNED BY salve.ficha_pendencia.sq_ficha_pendencia;


--
-- TOC entry 379 (class 1259 OID 25985)
-- Name: ficha_pesquisa; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_pesquisa (
    sq_ficha_pesquisa integer NOT NULL,
    sq_ficha bigint NOT NULL,
    sq_tema_pesquisa bigint NOT NULL,
    sq_situacao_pesquisa bigint NOT NULL,
    tx_ficha_pesquisa text
);


--
-- TOC entry 378 (class 1259 OID 25983)
-- Name: ficha_pesquisa_sq_ficha_pesquisa_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ficha_pesquisa_sq_ficha_pesquisa_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8160 (class 0 OID 0)
-- Dependencies: 378
-- Name: ficha_pesquisa_sq_ficha_pesquisa_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ficha_pesquisa_sq_ficha_pesquisa_seq OWNED BY salve.ficha_pesquisa.sq_ficha_pesquisa;


--
-- TOC entry 381 (class 1259 OID 26042)
-- Name: ficha_pessoa; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_pessoa (
    sq_ficha_pessoa integer NOT NULL,
    sq_ficha bigint NOT NULL,
    sq_papel bigint NOT NULL,
    sq_pessoa bigint NOT NULL,
    in_ativo character(1) DEFAULT 'S'::bpchar NOT NULL,
    sq_web_instituicao bigint,
    sq_grupo bigint
);


--
-- TOC entry 8161 (class 0 OID 0)
-- Dependencies: 381
-- Name: COLUMN ficha_pessoa.in_ativo; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_pessoa.in_ativo IS 'Informação se a pessoa está ativa ou não';


--
-- TOC entry 8162 (class 0 OID 0)
-- Dependencies: 381
-- Name: COLUMN ficha_pessoa.sq_web_instituicao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_pessoa.sq_web_instituicao IS 'Chave estrangeira da relação da pessoa_ficha com a instituição do coordenador de taxon';


--
-- TOC entry 8163 (class 0 OID 0)
-- Dependencies: 381
-- Name: COLUMN ficha_pessoa.sq_grupo; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_pessoa.sq_grupo IS 'Chave estrangeira da tabela salve.dados_apoio - TB_GRUPO';


--
-- TOC entry 380 (class 1259 OID 26040)
-- Name: ficha_pessoa_sq_ficha_pessoa_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ficha_pessoa_sq_ficha_pessoa_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8164 (class 0 OID 0)
-- Dependencies: 380
-- Name: ficha_pessoa_sq_ficha_pessoa_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ficha_pessoa_sq_ficha_pessoa_seq OWNED BY salve.ficha_pessoa.sq_ficha_pessoa;


--
-- TOC entry 383 (class 1259 OID 26080)
-- Name: ficha_popul_estimada_local; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_popul_estimada_local (
    sq_ficha_popul_estimada_local integer NOT NULL,
    sq_ficha bigint NOT NULL,
    sq_unid_abundancia_populacao bigint,
    ds_local text,
    ds_mes_ano_inicio text,
    ds_mes_ano_fim text,
    ds_estimativa_populacao text,
    de_valor_abundancia text,
    sq_abrangencia bigint
);


--
-- TOC entry 8165 (class 0 OID 0)
-- Dependencies: 383
-- Name: COLUMN ficha_popul_estimada_local.de_valor_abundancia; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_popul_estimada_local.de_valor_abundancia IS 'valor ou faixa da abundância ex. 0,53 a 0,79';


--
-- TOC entry 8166 (class 0 OID 0)
-- Dependencies: 383
-- Name: COLUMN ficha_popul_estimada_local.sq_abrangencia; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_popul_estimada_local.sq_abrangencia IS 'tipo da abrangencia';


--
-- TOC entry 382 (class 1259 OID 26078)
-- Name: ficha_popul_estimada_local_sq_ficha_popul_estimada_local_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ficha_popul_estimada_local_sq_ficha_popul_estimada_local_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8167 (class 0 OID 0)
-- Dependencies: 382
-- Name: ficha_popul_estimada_local_sq_ficha_popul_estimada_local_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ficha_popul_estimada_local_sq_ficha_popul_estimada_local_seq OWNED BY salve.ficha_popul_estimada_local.sq_ficha_popul_estimada_local;


--
-- TOC entry 385 (class 1259 OID 26108)
-- Name: ficha_ref_bib; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_ref_bib (
    sq_ficha_ref_bib integer NOT NULL,
    sq_ficha bigint NOT NULL,
    sq_publicacao bigint,
    no_tabela character varying(50),
    sq_registro bigint,
    no_coluna character varying(50),
    de_rotulo character varying(200),
    no_autor character varying(100),
    nu_ano integer
);


--
-- TOC entry 8168 (class 0 OID 0)
-- Dependencies: 385
-- Name: TABLE ficha_ref_bib; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON TABLE salve.ficha_ref_bib IS 'Relação com a tabela taxonomia.publicacao';


--
-- TOC entry 8169 (class 0 OID 0)
-- Dependencies: 385
-- Name: COLUMN ficha_ref_bib.no_tabela; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ref_bib.no_tabela IS 'Registro o nome da tabela da tabela relacionada com a referência bibliográfica';


--
-- TOC entry 8170 (class 0 OID 0)
-- Dependencies: 385
-- Name: COLUMN ficha_ref_bib.sq_registro; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ref_bib.sq_registro IS 'id do registro da tabela que foi informada na coluna no_tabela';


--
-- TOC entry 8171 (class 0 OID 0)
-- Dependencies: 385
-- Name: COLUMN ficha_ref_bib.no_coluna; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ref_bib.no_coluna IS 'nome da coluna da tabela informada na coluna no_tabela';


--
-- TOC entry 8172 (class 0 OID 0)
-- Dependencies: 385
-- Name: COLUMN ficha_ref_bib.de_rotulo; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ref_bib.de_rotulo IS 'descrição do rótulo do campo referenciado';


--
-- TOC entry 8173 (class 0 OID 0)
-- Dependencies: 385
-- Name: COLUMN ficha_ref_bib.no_autor; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ref_bib.no_autor IS 'Nome do autor quando for uma comunicação pessoal';


--
-- TOC entry 8174 (class 0 OID 0)
-- Dependencies: 385
-- Name: COLUMN ficha_ref_bib.nu_ano; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_ref_bib.nu_ano IS 'Ano da publicação quando for uma comunicação pessoal';


--
-- TOC entry 384 (class 1259 OID 26106)
-- Name: ficha_ref_bib_sq_ficha_ref_bib_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ficha_ref_bib_sq_ficha_ref_bib_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8175 (class 0 OID 0)
-- Dependencies: 384
-- Name: ficha_ref_bib_sq_ficha_ref_bib_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ficha_ref_bib_sq_ficha_ref_bib_seq OWNED BY salve.ficha_ref_bib.sq_ficha_ref_bib;


--
-- TOC entry 387 (class 1259 OID 26132)
-- Name: ficha_ref_bib_tag; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_ref_bib_tag (
    sq_ficha_ref_bib_tag integer NOT NULL,
    sq_ficha_ref_bib bigint NOT NULL,
    sq_tag bigint NOT NULL
);


--
-- TOC entry 386 (class 1259 OID 26130)
-- Name: ficha_ref_bib_tag_sq_ficha_ref_bib_tag_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ficha_ref_bib_tag_sq_ficha_ref_bib_tag_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8176 (class 0 OID 0)
-- Dependencies: 386
-- Name: ficha_ref_bib_tag_sq_ficha_ref_bib_tag_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ficha_ref_bib_tag_sq_ficha_ref_bib_tag_seq OWNED BY salve.ficha_ref_bib_tag.sq_ficha_ref_bib_tag;


--
-- TOC entry 389 (class 1259 OID 26153)
-- Name: ficha_sincronismo; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_sincronismo (
    sq_ficha_sincronismo integer NOT NULL,
    sq_ficha bigint NOT NULL,
    sq_tipo_sincronismo bigint NOT NULL,
    dt_sincronismo timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    nu_minutos_intervalo integer DEFAULT 1440 NOT NULL
);


--
-- TOC entry 8177 (class 0 OID 0)
-- Dependencies: 389
-- Name: TABLE ficha_sincronismo; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON TABLE salve.ficha_sincronismo IS 'Tabela para controle dos intervalos dos sincronismos da ficha';


--
-- TOC entry 8178 (class 0 OID 0)
-- Dependencies: 389
-- Name: COLUMN ficha_sincronismo.sq_ficha_sincronismo; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_sincronismo.sq_ficha_sincronismo IS 'Chave primaria sequencial';


--
-- TOC entry 8179 (class 0 OID 0)
-- Dependencies: 389
-- Name: COLUMN ficha_sincronismo.sq_ficha; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_sincronismo.sq_ficha IS 'Chave estrangeira da tabela Ficha';


--
-- TOC entry 8180 (class 0 OID 0)
-- Dependencies: 389
-- Name: COLUMN ficha_sincronismo.sq_tipo_sincronismo; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_sincronismo.sq_tipo_sincronismo IS 'Tipo do sincronismo realizado. Ex: ocorrencias do portalbio';


--
-- TOC entry 8181 (class 0 OID 0)
-- Dependencies: 389
-- Name: COLUMN ficha_sincronismo.dt_sincronismo; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_sincronismo.dt_sincronismo IS 'Data que foi realizado pela última vez';


--
-- TOC entry 8182 (class 0 OID 0)
-- Dependencies: 389
-- Name: COLUMN ficha_sincronismo.nu_minutos_intervalo; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_sincronismo.nu_minutos_intervalo IS 'Número mínimo de minutos permitido para o próximo sincronismo. Padrao 1 dia';


--
-- TOC entry 388 (class 1259 OID 26151)
-- Name: ficha_sincronismo_sq_ficha_sincronismo_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ficha_sincronismo_sq_ficha_sincronismo_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8183 (class 0 OID 0)
-- Dependencies: 388
-- Name: ficha_sincronismo_sq_ficha_sincronismo_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ficha_sincronismo_sq_ficha_sincronismo_seq OWNED BY salve.ficha_sincronismo.sq_ficha_sincronismo;


--
-- TOC entry 391 (class 1259 OID 26176)
-- Name: ficha_sinonimia; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_sinonimia (
    sq_ficha_sinonimia integer NOT NULL,
    sq_ficha bigint NOT NULL,
    no_sinonimia text NOT NULL,
    no_autor text NOT NULL,
    nu_ano smallint
);


--
-- TOC entry 390 (class 1259 OID 26174)
-- Name: ficha_sinonimia_sq_ficha_sinonimia_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ficha_sinonimia_sq_ficha_sinonimia_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8184 (class 0 OID 0)
-- Dependencies: 390
-- Name: ficha_sinonimia_sq_ficha_sinonimia_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ficha_sinonimia_sq_ficha_sinonimia_seq OWNED BY salve.ficha_sinonimia.sq_ficha_sinonimia;


--
-- TOC entry 269 (class 1259 OID 23017)
-- Name: ficha_sq_ficha_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ficha_sq_ficha_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8185 (class 0 OID 0)
-- Dependencies: 269
-- Name: ficha_sq_ficha_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ficha_sq_ficha_seq OWNED BY salve.ficha.sq_ficha;


--
-- TOC entry 393 (class 1259 OID 26193)
-- Name: ficha_uf; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_uf (
    sq_ficha_uf integer NOT NULL,
    sq_ficha bigint NOT NULL,
    sq_estado bigint NOT NULL
);


--
-- TOC entry 8186 (class 0 OID 0)
-- Dependencies: 393
-- Name: TABLE ficha_uf; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON TABLE salve.ficha_uf IS 'Tabela das Unidades da Federacao que a especie ocorre';


--
-- TOC entry 8187 (class 0 OID 0)
-- Dependencies: 393
-- Name: COLUMN ficha_uf.sq_ficha_uf; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_uf.sq_ficha_uf IS 'Chave primaria sequencial';


--
-- TOC entry 8188 (class 0 OID 0)
-- Dependencies: 393
-- Name: COLUMN ficha_uf.sq_ficha; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_uf.sq_ficha IS 'Chave estrangeira da tabela Ficha';


--
-- TOC entry 8189 (class 0 OID 0)
-- Dependencies: 393
-- Name: COLUMN ficha_uf.sq_estado; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_uf.sq_estado IS 'Chave estrangeira da tabela Estado/UF';


--
-- TOC entry 392 (class 1259 OID 26191)
-- Name: ficha_uf_sq_ficha_uf_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ficha_uf_sq_ficha_uf_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8190 (class 0 OID 0)
-- Dependencies: 392
-- Name: ficha_uf_sq_ficha_uf_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ficha_uf_sq_ficha_uf_seq OWNED BY salve.ficha_uf.sq_ficha_uf;


--
-- TOC entry 395 (class 1259 OID 26259)
-- Name: ficha_uso; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_uso (
    sq_ficha_uso integer NOT NULL,
    sq_uso bigint NOT NULL,
    sq_ficha bigint NOT NULL,
    tx_ficha_uso text
);


--
-- TOC entry 401 (class 1259 OID 26379)
-- Name: ficha_uso_regiao; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_uso_regiao (
    sq_ficha_uso_regiao integer NOT NULL,
    sq_ficha_uso bigint NOT NULL,
    sq_ficha_ocorrencia bigint
);


--
-- TOC entry 8191 (class 0 OID 0)
-- Dependencies: 401
-- Name: TABLE ficha_uso_regiao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON TABLE salve.ficha_uso_regiao IS 'Registrar as regiões do Uso';


--
-- TOC entry 400 (class 1259 OID 26377)
-- Name: ficha_uso_regiao_sq_ficha_uso_regiao_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ficha_uso_regiao_sq_ficha_uso_regiao_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8192 (class 0 OID 0)
-- Dependencies: 400
-- Name: ficha_uso_regiao_sq_ficha_uso_regiao_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ficha_uso_regiao_sq_ficha_uso_regiao_seq OWNED BY salve.ficha_uso_regiao.sq_ficha_uso_regiao;


--
-- TOC entry 394 (class 1259 OID 26257)
-- Name: ficha_uso_sq_ficha_uso_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ficha_uso_sq_ficha_uso_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8193 (class 0 OID 0)
-- Dependencies: 394
-- Name: ficha_uso_sq_ficha_uso_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ficha_uso_sq_ficha_uso_seq OWNED BY salve.ficha_uso.sq_ficha_uso;


--
-- TOC entry 403 (class 1259 OID 26432)
-- Name: ficha_variacao_sazonal; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_variacao_sazonal (
    sq_ficha_variacao_sazonal integer NOT NULL,
    sq_ficha bigint NOT NULL,
    sq_fase_vida bigint,
    sq_epoca bigint,
    sq_habitat bigint,
    ds_ficha_variacao_sazonal text
);


--
-- TOC entry 402 (class 1259 OID 26430)
-- Name: ficha_variacao_sazonal_sq_ficha_variacao_sazonal_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ficha_variacao_sazonal_sq_ficha_variacao_sazonal_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8194 (class 0 OID 0)
-- Dependencies: 402
-- Name: ficha_variacao_sazonal_sq_ficha_variacao_sazonal_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ficha_variacao_sazonal_sq_ficha_variacao_sazonal_seq OWNED BY salve.ficha_variacao_sazonal.sq_ficha_variacao_sazonal;


--
-- TOC entry 516 (class 1259 OID 29471)
-- Name: ficha_versao; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_versao (
    sq_ficha_versao bigint NOT NULL,
    sq_ficha bigint NOT NULL,
    sq_contexto bigint,
    nu_versao integer DEFAULT 1 NOT NULL,
    js_ficha jsonb NOT NULL,
    dt_inclusao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    sq_usuario_inclusao bigint NOT NULL,
    st_publico boolean DEFAULT false
);


--
-- TOC entry 8195 (class 0 OID 0)
-- Dependencies: 516
-- Name: COLUMN ficha_versao.sq_ficha_versao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_versao.sq_ficha_versao IS 'chave primaria sequencial da tabela ficha_versao';


--
-- TOC entry 8196 (class 0 OID 0)
-- Dependencies: 516
-- Name: COLUMN ficha_versao.sq_ficha; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_versao.sq_ficha IS 'chave estrangeira da tabela ficha';


--
-- TOC entry 8197 (class 0 OID 0)
-- Dependencies: 516
-- Name: COLUMN ficha_versao.sq_contexto; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_versao.sq_contexto IS 'Registrar o contexto/motivo em que a ficha foi versionada';


--
-- TOC entry 8198 (class 0 OID 0)
-- Dependencies: 516
-- Name: COLUMN ficha_versao.nu_versao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_versao.nu_versao IS 'Numero sequencial para controlar as versoes da versao da ficha';


--
-- TOC entry 8199 (class 0 OID 0)
-- Dependencies: 516
-- Name: COLUMN ficha_versao.js_ficha; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_versao.js_ficha IS 'Armazenar copia completa da ficha no formato jsonb';


--
-- TOC entry 8200 (class 0 OID 0)
-- Dependencies: 516
-- Name: COLUMN ficha_versao.dt_inclusao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_versao.dt_inclusao IS 'Data de inclusao do registro';


--
-- TOC entry 8201 (class 0 OID 0)
-- Dependencies: 516
-- Name: COLUMN ficha_versao.sq_usuario_inclusao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_versao.sq_usuario_inclusao IS 'Id do usuario que incluiu o regisrtro';


--
-- TOC entry 8202 (class 0 OID 0)
-- Dependencies: 516
-- Name: COLUMN ficha_versao.st_publico; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ficha_versao.st_publico IS 'Informar se a versao pode ser exibida no modulo publico';


--
-- TOC entry 515 (class 1259 OID 29469)
-- Name: ficha_versao_sq_ficha_versao_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ficha_versao_sq_ficha_versao_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8203 (class 0 OID 0)
-- Dependencies: 515
-- Name: ficha_versao_sq_ficha_versao_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ficha_versao_sq_ficha_versao_seq OWNED BY salve.ficha_versao.sq_ficha_versao;


--
-- TOC entry 524 (class 1259 OID 29593)
-- Name: ficha_versionamento; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ficha_versionamento (
    sq_ficha bigint NOT NULL,
    in_status character(1),
    ds_log text,
    dt_inclusao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


--
-- TOC entry 243 (class 1259 OID 18337)
-- Name: informativo; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.informativo (
    sq_informativo integer NOT NULL,
    nu_informativo integer,
    dt_informativo date NOT NULL,
    in_publicado character(1),
    de_tema text,
    tx_informativo text,
    no_arquivo character varying(255),
    de_local_arquivo character varying(255),
    de_tipo_conteudo character varying(50),
    dt_inicio_alerta date,
    dt_fim_alerta date,
    dt_inclusao timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    dt_alteracao timestamp without time zone DEFAULT CURRENT_TIMESTAMP
);


--
-- TOC entry 8204 (class 0 OID 0)
-- Dependencies: 243
-- Name: TABLE informativo; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON TABLE salve.informativo IS 'Tabela para armazenar os informativos sobre os sistema';


--
-- TOC entry 8205 (class 0 OID 0)
-- Dependencies: 243
-- Name: COLUMN informativo.nu_informativo; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.informativo.nu_informativo IS 'Numero do informativo informado pelo usuario';


--
-- TOC entry 8206 (class 0 OID 0)
-- Dependencies: 243
-- Name: COLUMN informativo.dt_informativo; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.informativo.dt_informativo IS 'Data elaboracao do informativo';


--
-- TOC entry 8207 (class 0 OID 0)
-- Dependencies: 243
-- Name: COLUMN informativo.in_publicado; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.informativo.in_publicado IS 'Infomacao S/N se o informativo esta publicado para ser visualizado pelos usuarios';


--
-- TOC entry 8208 (class 0 OID 0)
-- Dependencies: 243
-- Name: COLUMN informativo.de_tema; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.informativo.de_tema IS 'Breve descricao ou o tema do informativo';


--
-- TOC entry 8209 (class 0 OID 0)
-- Dependencies: 243
-- Name: COLUMN informativo.tx_informativo; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.informativo.tx_informativo IS 'Descricao completa do informativo';


--
-- TOC entry 8210 (class 0 OID 0)
-- Dependencies: 243
-- Name: COLUMN informativo.no_arquivo; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.informativo.no_arquivo IS 'Nome do arquivo pdf anexado quando houver';


--
-- TOC entry 8211 (class 0 OID 0)
-- Dependencies: 243
-- Name: COLUMN informativo.de_local_arquivo; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.informativo.de_local_arquivo IS 'Nome do arquivo no disco, gerado a partir do hash do conteudo do arquivo';


--
-- TOC entry 8212 (class 0 OID 0)
-- Dependencies: 243
-- Name: COLUMN informativo.de_tipo_conteudo; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.informativo.de_tipo_conteudo IS 'Conteudo mime baseado no conteudo do arquivo';


--
-- TOC entry 8213 (class 0 OID 0)
-- Dependencies: 243
-- Name: COLUMN informativo.dt_inicio_alerta; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.informativo.dt_inicio_alerta IS 'Data inicio para exibicao automatica ao efetuar login no sistema';


--
-- TOC entry 8214 (class 0 OID 0)
-- Dependencies: 243
-- Name: COLUMN informativo.dt_fim_alerta; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.informativo.dt_fim_alerta IS 'Data final para exibicao automatica ao efetuar login no sistema';


--
-- TOC entry 8215 (class 0 OID 0)
-- Dependencies: 243
-- Name: COLUMN informativo.dt_inclusao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.informativo.dt_inclusao IS 'Data de inclusao do informativo no sistema';


--
-- TOC entry 8216 (class 0 OID 0)
-- Dependencies: 243
-- Name: COLUMN informativo.dt_alteracao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.informativo.dt_alteracao IS 'Data da ultima alteracao do informativo';


--
-- TOC entry 290 (class 1259 OID 23484)
-- Name: informativo_pessoa; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.informativo_pessoa (
    sq_informativo_pessoa integer NOT NULL,
    sq_informativo bigint NOT NULL,
    sq_pessoa bigint NOT NULL,
    dt_visualizacao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


--
-- TOC entry 8217 (class 0 OID 0)
-- Dependencies: 290
-- Name: TABLE informativo_pessoa; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON TABLE salve.informativo_pessoa IS 'Tabela de controle das visualizacoes dos informativos pelos usuarios';


--
-- TOC entry 8218 (class 0 OID 0)
-- Dependencies: 290
-- Name: COLUMN informativo_pessoa.sq_informativo; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.informativo_pessoa.sq_informativo IS 'Id do informativo - chave estrangeira';


--
-- TOC entry 8219 (class 0 OID 0)
-- Dependencies: 290
-- Name: COLUMN informativo_pessoa.sq_pessoa; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.informativo_pessoa.sq_pessoa IS 'Chave estrangeira da tabela usuario. Usuario que visualizou o informativo';


--
-- TOC entry 8220 (class 0 OID 0)
-- Dependencies: 290
-- Name: COLUMN informativo_pessoa.dt_visualizacao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.informativo_pessoa.dt_visualizacao IS 'Data de visualizacao do informativo pela pessoa';


--
-- TOC entry 289 (class 1259 OID 23482)
-- Name: informativo_pessoa_sq_informativo_pessoa_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.informativo_pessoa_sq_informativo_pessoa_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8221 (class 0 OID 0)
-- Dependencies: 289
-- Name: informativo_pessoa_sq_informativo_pessoa_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.informativo_pessoa_sq_informativo_pessoa_seq OWNED BY salve.informativo_pessoa.sq_informativo_pessoa;


--
-- TOC entry 242 (class 1259 OID 18335)
-- Name: informativo_sq_informativo_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.informativo_sq_informativo_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8222 (class 0 OID 0)
-- Dependencies: 242
-- Name: informativo_sq_informativo_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.informativo_sq_informativo_seq OWNED BY salve.informativo.sq_informativo;


--
-- TOC entry 230 (class 1259 OID 18152)
-- Name: instituicao; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.instituicao (
    sq_pessoa bigint NOT NULL,
    sq_pessoa_pai bigint,
    sq_tipo bigint,
    sg_instituicao text,
    nu_cnpj character varying(14),
    no_contato text,
    nu_telefone text,
    dt_alteracao timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    st_interna boolean DEFAULT false NOT NULL
);


--
-- TOC entry 8223 (class 0 OID 0)
-- Dependencies: 230
-- Name: TABLE instituicao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON TABLE salve.instituicao IS 'Tabela para armazenar as instituições, unidades organizacionais, pessoas jurídicas, ucs etc';


--
-- TOC entry 8224 (class 0 OID 0)
-- Dependencies: 230
-- Name: COLUMN instituicao.sq_pessoa; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.instituicao.sq_pessoa IS 'Chave primária estrangeira da tabela pessoa';


--
-- TOC entry 8225 (class 0 OID 0)
-- Dependencies: 230
-- Name: COLUMN instituicao.sq_pessoa_pai; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.instituicao.sq_pessoa_pai IS 'Chave auto-relacionamento para criação da hierarquia entre as instituições';


--
-- TOC entry 8226 (class 0 OID 0)
-- Dependencies: 230
-- Name: COLUMN instituicao.sq_tipo; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.instituicao.sq_tipo IS 'Chave estrangeira da tabela dados_apoio';


--
-- TOC entry 8227 (class 0 OID 0)
-- Dependencies: 230
-- Name: COLUMN instituicao.sg_instituicao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.instituicao.sg_instituicao IS 'Sigla da intituição/departamento ou da unidade organizacional';


--
-- TOC entry 8228 (class 0 OID 0)
-- Dependencies: 230
-- Name: COLUMN instituicao.nu_cnpj; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.instituicao.nu_cnpj IS 'Número do CNPJ';


--
-- TOC entry 8229 (class 0 OID 0)
-- Dependencies: 230
-- Name: COLUMN instituicao.no_contato; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.instituicao.no_contato IS 'Nome da pessoa para contato na instituição';


--
-- TOC entry 8230 (class 0 OID 0)
-- Dependencies: 230
-- Name: COLUMN instituicao.nu_telefone; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.instituicao.nu_telefone IS 'Número do(s) telefone(s) da instituição';


--
-- TOC entry 8231 (class 0 OID 0)
-- Dependencies: 230
-- Name: COLUMN instituicao.dt_alteracao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.instituicao.dt_alteracao IS 'Data da última alteração';


--
-- TOC entry 8232 (class 0 OID 0)
-- Dependencies: 230
-- Name: COLUMN instituicao.st_interna; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.instituicao.st_interna IS 'Indicar se a instituição faz ou não parte da estrutura organizacional';


--
-- TOC entry 405 (class 1259 OID 26465)
-- Name: iucn_acao_conservacao; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.iucn_acao_conservacao (
    sq_iucn_acao_conservacao integer NOT NULL,
    cd_iucn_acao_conservacao text NOT NULL,
    ds_iucn_acao_conservacao text NOT NULL,
    ds_iucn_acao_conservacao_ordem text,
    dt_inclusao timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    dt_alteracao timestamp without time zone DEFAULT CURRENT_TIMESTAMP
);


--
-- TOC entry 8233 (class 0 OID 0)
-- Dependencies: 405
-- Name: COLUMN iucn_acao_conservacao.cd_iucn_acao_conservacao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_acao_conservacao.cd_iucn_acao_conservacao IS 'Codigo da acao de conservacao. Ex: 1, 1.1, 1.2 etc';


--
-- TOC entry 8234 (class 0 OID 0)
-- Dependencies: 405
-- Name: COLUMN iucn_acao_conservacao.ds_iucn_acao_conservacao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_acao_conservacao.ds_iucn_acao_conservacao IS 'Descricao da acao de conservacao';


--
-- TOC entry 8235 (class 0 OID 0)
-- Dependencies: 405
-- Name: COLUMN iucn_acao_conservacao.ds_iucn_acao_conservacao_ordem; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_acao_conservacao.ds_iucn_acao_conservacao_ordem IS 'Coluna para definir a ordem de apresentacao dos itens com suas hierarquias';


--
-- TOC entry 8236 (class 0 OID 0)
-- Dependencies: 405
-- Name: COLUMN iucn_acao_conservacao.dt_inclusao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_acao_conservacao.dt_inclusao IS 'Data e hora que o registro foi criado no banco de dados';


--
-- TOC entry 8237 (class 0 OID 0)
-- Dependencies: 405
-- Name: COLUMN iucn_acao_conservacao.dt_alteracao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_acao_conservacao.dt_alteracao IS 'Data e hora da ultima alteracao do registro';


--
-- TOC entry 407 (class 1259 OID 26478)
-- Name: iucn_acao_conservacao_apoio; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.iucn_acao_conservacao_apoio (
    sq_iucn_acao_conservacao_apoio integer NOT NULL,
    sq_iucn_acao_conservacao bigint NOT NULL,
    sq_dados_apoio bigint NOT NULL,
    dt_inclusao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    dt_alteracao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


--
-- TOC entry 8238 (class 0 OID 0)
-- Dependencies: 407
-- Name: COLUMN iucn_acao_conservacao_apoio.sq_iucn_acao_conservacao_apoio; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_acao_conservacao_apoio.sq_iucn_acao_conservacao_apoio IS 'Chave primaria sequencial da tabela iucn_acao_conservacao_apoio';


--
-- TOC entry 8239 (class 0 OID 0)
-- Dependencies: 407
-- Name: COLUMN iucn_acao_conservacao_apoio.sq_iucn_acao_conservacao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_acao_conservacao_apoio.sq_iucn_acao_conservacao IS 'Chave estrangeira da tabela iucn_acao_conservacao';


--
-- TOC entry 8240 (class 0 OID 0)
-- Dependencies: 407
-- Name: COLUMN iucn_acao_conservacao_apoio.sq_dados_apoio; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_acao_conservacao_apoio.sq_dados_apoio IS 'Chave estrangeira da tabela dados_apoio';


--
-- TOC entry 8241 (class 0 OID 0)
-- Dependencies: 407
-- Name: COLUMN iucn_acao_conservacao_apoio.dt_inclusao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_acao_conservacao_apoio.dt_inclusao IS 'Data e hora que o registro foi adicionado no banco de dados';


--
-- TOC entry 8242 (class 0 OID 0)
-- Dependencies: 407
-- Name: COLUMN iucn_acao_conservacao_apoio.dt_alteracao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_acao_conservacao_apoio.dt_alteracao IS 'Data e hora da ultima alteracao do registro';


--
-- TOC entry 406 (class 1259 OID 26476)
-- Name: iucn_acao_conservacao_apoio_sq_iucn_acao_conservacao_apoio_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.iucn_acao_conservacao_apoio_sq_iucn_acao_conservacao_apoio_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8243 (class 0 OID 0)
-- Dependencies: 406
-- Name: iucn_acao_conservacao_apoio_sq_iucn_acao_conservacao_apoio_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.iucn_acao_conservacao_apoio_sq_iucn_acao_conservacao_apoio_seq OWNED BY salve.iucn_acao_conservacao_apoio.sq_iucn_acao_conservacao_apoio;


--
-- TOC entry 404 (class 1259 OID 26463)
-- Name: iucn_acao_conservacao_sq_iucn_acao_conservacao_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.iucn_acao_conservacao_sq_iucn_acao_conservacao_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8244 (class 0 OID 0)
-- Dependencies: 404
-- Name: iucn_acao_conservacao_sq_iucn_acao_conservacao_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.iucn_acao_conservacao_sq_iucn_acao_conservacao_seq OWNED BY salve.iucn_acao_conservacao.sq_iucn_acao_conservacao;


--
-- TOC entry 409 (class 1259 OID 26498)
-- Name: iucn_ameaca; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.iucn_ameaca (
    sq_iucn_ameaca integer NOT NULL,
    cd_iucn_ameaca text NOT NULL,
    ds_iucn_ameaca text NOT NULL,
    ds_iucn_ameaca_ordem text,
    dt_inclusao timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    dt_alteracao timestamp without time zone DEFAULT CURRENT_TIMESTAMP
);


--
-- TOC entry 8245 (class 0 OID 0)
-- Dependencies: 409
-- Name: COLUMN iucn_ameaca.cd_iucn_ameaca; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_ameaca.cd_iucn_ameaca IS 'Codigo da ameaca. Ex: 1, 1.1, 1.2 etc';


--
-- TOC entry 8246 (class 0 OID 0)
-- Dependencies: 409
-- Name: COLUMN iucn_ameaca.ds_iucn_ameaca; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_ameaca.ds_iucn_ameaca IS 'Descricao da ameaca';


--
-- TOC entry 8247 (class 0 OID 0)
-- Dependencies: 409
-- Name: COLUMN iucn_ameaca.ds_iucn_ameaca_ordem; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_ameaca.ds_iucn_ameaca_ordem IS 'Coluna para definir a ordem de apresentacao dos itens com suas hierarquias';


--
-- TOC entry 8248 (class 0 OID 0)
-- Dependencies: 409
-- Name: COLUMN iucn_ameaca.dt_inclusao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_ameaca.dt_inclusao IS 'Data e hora que o registro foi criado no banco de dados';


--
-- TOC entry 8249 (class 0 OID 0)
-- Dependencies: 409
-- Name: COLUMN iucn_ameaca.dt_alteracao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_ameaca.dt_alteracao IS 'Data e hora da ultima alteracao do registro';


--
-- TOC entry 411 (class 1259 OID 26511)
-- Name: iucn_ameaca_apoio; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.iucn_ameaca_apoio (
    sq_iucn_ameaca_apoio integer NOT NULL,
    sq_iucn_ameaca bigint NOT NULL,
    sq_dados_apoio bigint NOT NULL,
    dt_inclusao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    dt_alteracao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


--
-- TOC entry 8250 (class 0 OID 0)
-- Dependencies: 411
-- Name: COLUMN iucn_ameaca_apoio.sq_iucn_ameaca_apoio; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_ameaca_apoio.sq_iucn_ameaca_apoio IS 'Chave primaria sequencial da tabela iucn_ameaca_apoio';


--
-- TOC entry 8251 (class 0 OID 0)
-- Dependencies: 411
-- Name: COLUMN iucn_ameaca_apoio.sq_iucn_ameaca; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_ameaca_apoio.sq_iucn_ameaca IS 'Chave estrangeira da tabela iucn_ameaca';


--
-- TOC entry 8252 (class 0 OID 0)
-- Dependencies: 411
-- Name: COLUMN iucn_ameaca_apoio.sq_dados_apoio; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_ameaca_apoio.sq_dados_apoio IS 'Chave estrangeira da tabela dados_apoio';


--
-- TOC entry 8253 (class 0 OID 0)
-- Dependencies: 411
-- Name: COLUMN iucn_ameaca_apoio.dt_inclusao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_ameaca_apoio.dt_inclusao IS 'Data e hora que o registro foi adicionado no banco de dados';


--
-- TOC entry 8254 (class 0 OID 0)
-- Dependencies: 411
-- Name: COLUMN iucn_ameaca_apoio.dt_alteracao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_ameaca_apoio.dt_alteracao IS 'Data e hora da ultima alteracao do registro';


--
-- TOC entry 410 (class 1259 OID 26509)
-- Name: iucn_ameaca_apoio_sq_iucn_ameaca_apoio_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.iucn_ameaca_apoio_sq_iucn_ameaca_apoio_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8255 (class 0 OID 0)
-- Dependencies: 410
-- Name: iucn_ameaca_apoio_sq_iucn_ameaca_apoio_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.iucn_ameaca_apoio_sq_iucn_ameaca_apoio_seq OWNED BY salve.iucn_ameaca_apoio.sq_iucn_ameaca_apoio;


--
-- TOC entry 408 (class 1259 OID 26496)
-- Name: iucn_ameaca_sq_iucn_ameaca_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.iucn_ameaca_sq_iucn_ameaca_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8256 (class 0 OID 0)
-- Dependencies: 408
-- Name: iucn_ameaca_sq_iucn_ameaca_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.iucn_ameaca_sq_iucn_ameaca_seq OWNED BY salve.iucn_ameaca.sq_iucn_ameaca;


--
-- TOC entry 413 (class 1259 OID 26571)
-- Name: iucn_habitat; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.iucn_habitat (
    sq_iucn_habitat integer NOT NULL,
    cd_iucn_habitat text NOT NULL,
    ds_iucn_habitat text NOT NULL,
    ds_iucn_habitat_ordem text,
    dt_inclusao timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    dt_alteracao timestamp without time zone DEFAULT CURRENT_TIMESTAMP
);


--
-- TOC entry 8257 (class 0 OID 0)
-- Dependencies: 413
-- Name: COLUMN iucn_habitat.cd_iucn_habitat; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_habitat.cd_iucn_habitat IS 'Codigo da habitat. Ex: 1, 1.1, 1.2 etc';


--
-- TOC entry 8258 (class 0 OID 0)
-- Dependencies: 413
-- Name: COLUMN iucn_habitat.ds_iucn_habitat; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_habitat.ds_iucn_habitat IS 'Descricao da habitat';


--
-- TOC entry 8259 (class 0 OID 0)
-- Dependencies: 413
-- Name: COLUMN iucn_habitat.ds_iucn_habitat_ordem; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_habitat.ds_iucn_habitat_ordem IS 'Coluna para definir a ordem de apresentacao dos itens com suas hierarquias';


--
-- TOC entry 8260 (class 0 OID 0)
-- Dependencies: 413
-- Name: COLUMN iucn_habitat.dt_inclusao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_habitat.dt_inclusao IS 'Data e hora que o registro foi criado no banco de dados';


--
-- TOC entry 8261 (class 0 OID 0)
-- Dependencies: 413
-- Name: COLUMN iucn_habitat.dt_alteracao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_habitat.dt_alteracao IS 'Data e hora da ultima alteracao do registro';


--
-- TOC entry 415 (class 1259 OID 26584)
-- Name: iucn_habitat_apoio; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.iucn_habitat_apoio (
    sq_iucn_habitat_apoio integer NOT NULL,
    sq_iucn_habitat bigint NOT NULL,
    sq_dados_apoio bigint NOT NULL,
    dt_inclusao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    dt_alteracao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


--
-- TOC entry 8262 (class 0 OID 0)
-- Dependencies: 415
-- Name: COLUMN iucn_habitat_apoio.sq_iucn_habitat_apoio; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_habitat_apoio.sq_iucn_habitat_apoio IS 'Chave primaria sequencial da tabela iucn_habitat_apoio';


--
-- TOC entry 8263 (class 0 OID 0)
-- Dependencies: 415
-- Name: COLUMN iucn_habitat_apoio.sq_iucn_habitat; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_habitat_apoio.sq_iucn_habitat IS 'Chave estrangeira da tabela iucn_habitat';


--
-- TOC entry 8264 (class 0 OID 0)
-- Dependencies: 415
-- Name: COLUMN iucn_habitat_apoio.sq_dados_apoio; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_habitat_apoio.sq_dados_apoio IS 'Chave estrangeira da tabela dados_apoio';


--
-- TOC entry 8265 (class 0 OID 0)
-- Dependencies: 415
-- Name: COLUMN iucn_habitat_apoio.dt_inclusao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_habitat_apoio.dt_inclusao IS 'Data e hora que o registro foi adicionado no banco de dados';


--
-- TOC entry 8266 (class 0 OID 0)
-- Dependencies: 415
-- Name: COLUMN iucn_habitat_apoio.dt_alteracao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_habitat_apoio.dt_alteracao IS 'Data e hora da ultima alteracao do registro';


--
-- TOC entry 414 (class 1259 OID 26582)
-- Name: iucn_habitat_apoio_sq_iucn_habitat_apoio_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.iucn_habitat_apoio_sq_iucn_habitat_apoio_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8267 (class 0 OID 0)
-- Dependencies: 414
-- Name: iucn_habitat_apoio_sq_iucn_habitat_apoio_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.iucn_habitat_apoio_sq_iucn_habitat_apoio_seq OWNED BY salve.iucn_habitat_apoio.sq_iucn_habitat_apoio;


--
-- TOC entry 412 (class 1259 OID 26569)
-- Name: iucn_habitat_sq_iucn_habitat_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.iucn_habitat_sq_iucn_habitat_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8268 (class 0 OID 0)
-- Dependencies: 412
-- Name: iucn_habitat_sq_iucn_habitat_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.iucn_habitat_sq_iucn_habitat_seq OWNED BY salve.iucn_habitat.sq_iucn_habitat;


--
-- TOC entry 417 (class 1259 OID 26604)
-- Name: iucn_motivo_mudanca; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.iucn_motivo_mudanca (
    sq_iucn_motivo_mudanca integer NOT NULL,
    cd_iucn_motivo_mudanca text NOT NULL,
    ds_iucn_motivo_mudanca text NOT NULL,
    ds_iucn_motivo_mudanca_ordem text,
    dt_inclusao timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    dt_alteracao timestamp without time zone DEFAULT CURRENT_TIMESTAMP
);


--
-- TOC entry 8269 (class 0 OID 0)
-- Dependencies: 417
-- Name: COLUMN iucn_motivo_mudanca.cd_iucn_motivo_mudanca; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_motivo_mudanca.cd_iucn_motivo_mudanca IS 'Codigo do motivo da mudanca. Ex: 1, 1.1, 1.2 etc';


--
-- TOC entry 8270 (class 0 OID 0)
-- Dependencies: 417
-- Name: COLUMN iucn_motivo_mudanca.ds_iucn_motivo_mudanca; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_motivo_mudanca.ds_iucn_motivo_mudanca IS 'Descricao do motivo da mudanca';


--
-- TOC entry 8271 (class 0 OID 0)
-- Dependencies: 417
-- Name: COLUMN iucn_motivo_mudanca.ds_iucn_motivo_mudanca_ordem; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_motivo_mudanca.ds_iucn_motivo_mudanca_ordem IS 'Coluna para definir a ordem de apresentacao dos itens com suas hierarquias';


--
-- TOC entry 8272 (class 0 OID 0)
-- Dependencies: 417
-- Name: COLUMN iucn_motivo_mudanca.dt_inclusao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_motivo_mudanca.dt_inclusao IS 'Data e hora que o registro foi criado no banco de dados';


--
-- TOC entry 8273 (class 0 OID 0)
-- Dependencies: 417
-- Name: COLUMN iucn_motivo_mudanca.dt_alteracao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_motivo_mudanca.dt_alteracao IS 'Data e hora da ultima alteracao do registro';


--
-- TOC entry 419 (class 1259 OID 26617)
-- Name: iucn_motivo_mudanca_apoio; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.iucn_motivo_mudanca_apoio (
    sq_iucn_motivo_mudanca_apoio integer NOT NULL,
    sq_iucn_motivo_mudanca bigint NOT NULL,
    sq_dados_apoio bigint NOT NULL,
    dt_inclusao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    dt_alteracao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


--
-- TOC entry 8274 (class 0 OID 0)
-- Dependencies: 419
-- Name: COLUMN iucn_motivo_mudanca_apoio.sq_iucn_motivo_mudanca_apoio; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_motivo_mudanca_apoio.sq_iucn_motivo_mudanca_apoio IS 'Chave primaria sequencial da tabela iucn_motivo_mudanca_apoio';


--
-- TOC entry 8275 (class 0 OID 0)
-- Dependencies: 419
-- Name: COLUMN iucn_motivo_mudanca_apoio.sq_iucn_motivo_mudanca; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_motivo_mudanca_apoio.sq_iucn_motivo_mudanca IS 'Chave estrangeira da tabela iucn_motivo_mudanca';


--
-- TOC entry 8276 (class 0 OID 0)
-- Dependencies: 419
-- Name: COLUMN iucn_motivo_mudanca_apoio.sq_dados_apoio; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_motivo_mudanca_apoio.sq_dados_apoio IS 'Chave estrangeira da tabela dados_apoio';


--
-- TOC entry 8277 (class 0 OID 0)
-- Dependencies: 419
-- Name: COLUMN iucn_motivo_mudanca_apoio.dt_inclusao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_motivo_mudanca_apoio.dt_inclusao IS 'Data e hora que o registro foi adicionado no banco de dados';


--
-- TOC entry 8278 (class 0 OID 0)
-- Dependencies: 419
-- Name: COLUMN iucn_motivo_mudanca_apoio.dt_alteracao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_motivo_mudanca_apoio.dt_alteracao IS 'Data e hora da ultima alteracao do registro';


--
-- TOC entry 418 (class 1259 OID 26615)
-- Name: iucn_motivo_mudanca_apoio_sq_iucn_motivo_mudanca_apoio_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.iucn_motivo_mudanca_apoio_sq_iucn_motivo_mudanca_apoio_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8279 (class 0 OID 0)
-- Dependencies: 418
-- Name: iucn_motivo_mudanca_apoio_sq_iucn_motivo_mudanca_apoio_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.iucn_motivo_mudanca_apoio_sq_iucn_motivo_mudanca_apoio_seq OWNED BY salve.iucn_motivo_mudanca_apoio.sq_iucn_motivo_mudanca_apoio;


--
-- TOC entry 416 (class 1259 OID 26602)
-- Name: iucn_motivo_mudanca_sq_iucn_motivo_mudanca_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.iucn_motivo_mudanca_sq_iucn_motivo_mudanca_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8280 (class 0 OID 0)
-- Dependencies: 416
-- Name: iucn_motivo_mudanca_sq_iucn_motivo_mudanca_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.iucn_motivo_mudanca_sq_iucn_motivo_mudanca_seq OWNED BY salve.iucn_motivo_mudanca.sq_iucn_motivo_mudanca;


--
-- TOC entry 421 (class 1259 OID 26637)
-- Name: iucn_pesquisa; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.iucn_pesquisa (
    sq_iucn_pesquisa integer NOT NULL,
    cd_iucn_pesquisa text NOT NULL,
    ds_iucn_pesquisa text NOT NULL,
    ds_iucn_pesquisa_ordem text,
    dt_inclusao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    dt_alteracao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


--
-- TOC entry 8281 (class 0 OID 0)
-- Dependencies: 421
-- Name: COLUMN iucn_pesquisa.cd_iucn_pesquisa; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_pesquisa.cd_iucn_pesquisa IS 'Codigo. Ex: 1, 1.1, 1.2 etc';


--
-- TOC entry 8282 (class 0 OID 0)
-- Dependencies: 421
-- Name: COLUMN iucn_pesquisa.ds_iucn_pesquisa; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_pesquisa.ds_iucn_pesquisa IS 'Descricao da pesquisa';


--
-- TOC entry 8283 (class 0 OID 0)
-- Dependencies: 421
-- Name: COLUMN iucn_pesquisa.ds_iucn_pesquisa_ordem; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_pesquisa.ds_iucn_pesquisa_ordem IS 'Coluna para definir a ordem de apresentacao dos itens com suas hierarquias';


--
-- TOC entry 8284 (class 0 OID 0)
-- Dependencies: 421
-- Name: COLUMN iucn_pesquisa.dt_inclusao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_pesquisa.dt_inclusao IS 'Data e hora que o registro foi adicionado no banco de dados';


--
-- TOC entry 8285 (class 0 OID 0)
-- Dependencies: 421
-- Name: COLUMN iucn_pesquisa.dt_alteracao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_pesquisa.dt_alteracao IS 'Data e hora da ultima alteracao do registro';


--
-- TOC entry 423 (class 1259 OID 26650)
-- Name: iucn_pesquisa_apoio; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.iucn_pesquisa_apoio (
    sq_iucn_pesquisa_apoio integer NOT NULL,
    sq_iucn_pesquisa bigint NOT NULL,
    sq_dados_apoio bigint NOT NULL,
    dt_inclusao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    dt_alteracao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


--
-- TOC entry 8286 (class 0 OID 0)
-- Dependencies: 423
-- Name: COLUMN iucn_pesquisa_apoio.sq_iucn_pesquisa_apoio; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_pesquisa_apoio.sq_iucn_pesquisa_apoio IS 'Chave primaria sequencial da tabela iucn_pesquisa_apoio';


--
-- TOC entry 8287 (class 0 OID 0)
-- Dependencies: 423
-- Name: COLUMN iucn_pesquisa_apoio.sq_iucn_pesquisa; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_pesquisa_apoio.sq_iucn_pesquisa IS 'Chave estrangeira da tabela iucn_pesquisa';


--
-- TOC entry 8288 (class 0 OID 0)
-- Dependencies: 423
-- Name: COLUMN iucn_pesquisa_apoio.sq_dados_apoio; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_pesquisa_apoio.sq_dados_apoio IS 'Chave estrangeira da tabela dados_apoio';


--
-- TOC entry 8289 (class 0 OID 0)
-- Dependencies: 423
-- Name: COLUMN iucn_pesquisa_apoio.dt_inclusao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_pesquisa_apoio.dt_inclusao IS 'Data e hora que o registro foi adicionado no banco de dados';


--
-- TOC entry 8290 (class 0 OID 0)
-- Dependencies: 423
-- Name: COLUMN iucn_pesquisa_apoio.dt_alteracao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_pesquisa_apoio.dt_alteracao IS 'Data e hora da ultima alteracao do registro';


--
-- TOC entry 422 (class 1259 OID 26648)
-- Name: iucn_pesquisa_apoio_sq_iucn_pesquisa_apoio_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.iucn_pesquisa_apoio_sq_iucn_pesquisa_apoio_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8291 (class 0 OID 0)
-- Dependencies: 422
-- Name: iucn_pesquisa_apoio_sq_iucn_pesquisa_apoio_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.iucn_pesquisa_apoio_sq_iucn_pesquisa_apoio_seq OWNED BY salve.iucn_pesquisa_apoio.sq_iucn_pesquisa_apoio;


--
-- TOC entry 420 (class 1259 OID 26635)
-- Name: iucn_pesquisa_sq_iucn_pesquisa_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.iucn_pesquisa_sq_iucn_pesquisa_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8292 (class 0 OID 0)
-- Dependencies: 420
-- Name: iucn_pesquisa_sq_iucn_pesquisa_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.iucn_pesquisa_sq_iucn_pesquisa_seq OWNED BY salve.iucn_pesquisa.sq_iucn_pesquisa;


--
-- TOC entry 425 (class 1259 OID 26682)
-- Name: iucn_transmissao; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.iucn_transmissao (
    sq_iucn_transmissao integer NOT NULL,
    sq_pessoa bigint NOT NULL,
    dt_transmissao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    no_arquivo_zip text,
    tx_log text
);


--
-- TOC entry 8293 (class 0 OID 0)
-- Dependencies: 425
-- Name: COLUMN iucn_transmissao.sq_iucn_transmissao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_transmissao.sq_iucn_transmissao IS 'Chave primária sequencial da tabela iucn_transmissao';


--
-- TOC entry 8294 (class 0 OID 0)
-- Dependencies: 425
-- Name: COLUMN iucn_transmissao.sq_pessoa; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_transmissao.sq_pessoa IS 'Nome da pessoa/usuario que realizou a transmissao';


--
-- TOC entry 8295 (class 0 OID 0)
-- Dependencies: 425
-- Name: COLUMN iucn_transmissao.dt_transmissao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_transmissao.dt_transmissao IS 'Data que foi feita a transmissão do arquivo zip para o sis/iucn';


--
-- TOC entry 8296 (class 0 OID 0)
-- Dependencies: 425
-- Name: COLUMN iucn_transmissao.no_arquivo_zip; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_transmissao.no_arquivo_zip IS 'Nome do arquivo zip transmitido localizado no diretorio /data/salve/iucn';


--
-- TOC entry 8297 (class 0 OID 0)
-- Dependencies: 425
-- Name: COLUMN iucn_transmissao.tx_log; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_transmissao.tx_log IS 'Lista de erros encontrados na criacao do arquivo zip para envio ao sis/iucn';


--
-- TOC entry 427 (class 1259 OID 26699)
-- Name: iucn_transmissao_ficha; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.iucn_transmissao_ficha (
    sq_iucn_transmissao_ficha integer NOT NULL,
    sq_iucn_transmissao bigint NOT NULL,
    sq_ficha bigint NOT NULL
);


--
-- TOC entry 8298 (class 0 OID 0)
-- Dependencies: 427
-- Name: COLUMN iucn_transmissao_ficha.sq_iucn_transmissao_ficha; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_transmissao_ficha.sq_iucn_transmissao_ficha IS 'Chave primaria sequencial da tabela iucn_transmissao_ficha';


--
-- TOC entry 8299 (class 0 OID 0)
-- Dependencies: 427
-- Name: COLUMN iucn_transmissao_ficha.sq_ficha; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_transmissao_ficha.sq_ficha IS 'Chave estrangeira da tabela iucn_transmissao';


--
-- TOC entry 426 (class 1259 OID 26697)
-- Name: iucn_transmissao_ficha_sq_iucn_transmissao_ficha_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.iucn_transmissao_ficha_sq_iucn_transmissao_ficha_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8300 (class 0 OID 0)
-- Dependencies: 426
-- Name: iucn_transmissao_ficha_sq_iucn_transmissao_ficha_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.iucn_transmissao_ficha_sq_iucn_transmissao_ficha_seq OWNED BY salve.iucn_transmissao_ficha.sq_iucn_transmissao_ficha;


--
-- TOC entry 424 (class 1259 OID 26680)
-- Name: iucn_transmissao_sq_iucn_transmissao_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.iucn_transmissao_sq_iucn_transmissao_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8301 (class 0 OID 0)
-- Dependencies: 424
-- Name: iucn_transmissao_sq_iucn_transmissao_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.iucn_transmissao_sq_iucn_transmissao_seq OWNED BY salve.iucn_transmissao.sq_iucn_transmissao;


--
-- TOC entry 397 (class 1259 OID 26287)
-- Name: iucn_uso; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.iucn_uso (
    sq_iucn_uso integer NOT NULL,
    cd_iucn_uso text NOT NULL,
    ds_iucn_uso text NOT NULL,
    ds_iucn_uso_ordem text NOT NULL,
    dt_inclusao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    dt_alteracao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


--
-- TOC entry 8302 (class 0 OID 0)
-- Dependencies: 397
-- Name: COLUMN iucn_uso.cd_iucn_uso; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_uso.cd_iucn_uso IS 'Codigo do uso. Ex: 1, 1.1, 1.2 etc';


--
-- TOC entry 8303 (class 0 OID 0)
-- Dependencies: 397
-- Name: COLUMN iucn_uso.ds_iucn_uso; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_uso.ds_iucn_uso IS 'Descricao do uso';


--
-- TOC entry 8304 (class 0 OID 0)
-- Dependencies: 397
-- Name: COLUMN iucn_uso.ds_iucn_uso_ordem; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_uso.ds_iucn_uso_ordem IS 'Coluna para definir a ordem de apresentacao dos itens com suas hierarquias';


--
-- TOC entry 8305 (class 0 OID 0)
-- Dependencies: 397
-- Name: COLUMN iucn_uso.dt_inclusao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_uso.dt_inclusao IS 'Data e hora que o registro foi adicionado no banco de dados';


--
-- TOC entry 8306 (class 0 OID 0)
-- Dependencies: 397
-- Name: COLUMN iucn_uso.dt_alteracao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.iucn_uso.dt_alteracao IS 'Data e hora da ultima alteracao do registro';


--
-- TOC entry 396 (class 1259 OID 26285)
-- Name: iucn_uso_sq_iucn_uso_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.iucn_uso_sq_iucn_uso_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8307 (class 0 OID 0)
-- Dependencies: 396
-- Name: iucn_uso_sq_iucn_uso_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.iucn_uso_sq_iucn_uso_seq OWNED BY salve.iucn_uso.sq_iucn_uso;


--
-- TOC entry 223 (class 1259 OID 18055)
-- Name: municipio; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.municipio (
    sq_municipio integer NOT NULL,
    sq_estado bigint NOT NULL,
    no_municipio text NOT NULL,
    co_ibge integer NOT NULL,
    ge_municipio public.geometry,
    dt_inclusao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    dt_alteracao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    CONSTRAINT enforce_dims_the_geom CHECK ((public.st_ndims(ge_municipio) = 2)),
    CONSTRAINT enforce_geotype_the_geom CHECK (((public.geometrytype(ge_municipio) = 'MULTIPOLYGON'::text) OR (ge_municipio IS NULL))),
    CONSTRAINT enforce_srid_the_geom CHECK ((public.st_srid(ge_municipio) = 4674))
);


--
-- TOC entry 222 (class 1259 OID 18053)
-- Name: municipio_sq_municipio_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.municipio_sq_municipio_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8308 (class 0 OID 0)
-- Dependencies: 222
-- Name: municipio_sq_municipio_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.municipio_sq_municipio_seq OWNED BY salve.municipio.sq_municipio;


--
-- TOC entry 256 (class 1259 OID 22832)
-- Name: oficina; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.oficina (
    sq_oficina integer NOT NULL,
    sq_unidade_org bigint,
    sq_ciclo_avaliacao bigint NOT NULL,
    sq_tipo_oficina bigint NOT NULL,
    sq_pessoa bigint,
    no_oficina text NOT NULL,
    sg_oficina text NOT NULL,
    de_local text NOT NULL,
    dt_inicio date NOT NULL,
    dt_fim date NOT NULL,
    tx_encaminhamento text,
    tx_email_participantes text,
    de_email_ponto_focal text,
    tx_email_validadores text,
    sq_situacao bigint DEFAULT 1041 NOT NULL,
    de_link_sei text,
    sq_fonte_recurso bigint
);


--
-- TOC entry 8309 (class 0 OID 0)
-- Dependencies: 256
-- Name: COLUMN oficina.sq_unidade_org; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.oficina.sq_unidade_org IS 'Id do unidade organizacional que está organizando a oficina. Oficina de validação não terá unidade.';


--
-- TOC entry 8310 (class 0 OID 0)
-- Dependencies: 256
-- Name: COLUMN oficina.sq_pessoa; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.oficina.sq_pessoa IS 'Id do ponto focal. Oficina de validação não terá o ponto focal';


--
-- TOC entry 8311 (class 0 OID 0)
-- Dependencies: 256
-- Name: COLUMN oficina.tx_email_participantes; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.oficina.tx_email_participantes IS 'Cópia do email enviado aos participantes';


--
-- TOC entry 8312 (class 0 OID 0)
-- Dependencies: 256
-- Name: COLUMN oficina.de_email_ponto_focal; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.oficina.de_email_ponto_focal IS 'email do ponto focal que enviou os emails aos participantes';


--
-- TOC entry 8313 (class 0 OID 0)
-- Dependencies: 256
-- Name: COLUMN oficina.sq_situacao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.oficina.sq_situacao IS 'Situação da oficina: aberta ou encerrada';


--
-- TOC entry 8314 (class 0 OID 0)
-- Dependencies: 256
-- Name: COLUMN oficina.de_link_sei; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.oficina.de_link_sei IS 'Link para visualizar documento no sistema SEI';


--
-- TOC entry 8315 (class 0 OID 0)
-- Dependencies: 256
-- Name: COLUMN oficina.sq_fonte_recurso; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.oficina.sq_fonte_recurso IS 'Id da fonte de recurso utilizada para realizacao da oficina.';


--
-- TOC entry 435 (class 1259 OID 27028)
-- Name: oficina_ficha; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.oficina_ficha (
    sq_oficina_ficha integer NOT NULL,
    sq_oficina bigint NOT NULL,
    sq_ficha bigint NOT NULL,
    sq_categoria_iucn bigint,
    ds_criterio_aval_iucn text,
    ds_justificativa text,
    ds_agrupamento text,
    dt_avaliacao timestamp without time zone,
    dt_ultima_notificacao timestamp without time zone,
    st_transferida boolean DEFAULT false NOT NULL,
    st_pf_convidado boolean DEFAULT false,
    st_ct_convidado boolean DEFAULT false,
    st_possivelmente_extinta character(1),
    st_chat_alterado boolean,
    sq_situacao_ficha bigint,
    CONSTRAINT chk_oficina_ficha_pex CHECK ((st_possivelmente_extinta = ANY (ARRAY['S'::bpchar, 'N'::bpchar, 'D'::bpchar])))
);


--
-- TOC entry 8316 (class 0 OID 0)
-- Dependencies: 435
-- Name: COLUMN oficina_ficha.sq_categoria_iucn; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.oficina_ficha.sq_categoria_iucn IS 'Manter histórico do critério iucn da ficha ao final da oficina';


--
-- TOC entry 8317 (class 0 OID 0)
-- Dependencies: 435
-- Name: COLUMN oficina_ficha.ds_criterio_aval_iucn; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.oficina_ficha.ds_criterio_aval_iucn IS 'Manter histórico do criterio da ficha ao finalizar a oficina';


--
-- TOC entry 8318 (class 0 OID 0)
-- Dependencies: 435
-- Name: COLUMN oficina_ficha.ds_justificativa; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.oficina_ficha.ds_justificativa IS 'Manter histórico da justificativa da ficha ao finalizar a oficina';


--
-- TOC entry 8319 (class 0 OID 0)
-- Dependencies: 435
-- Name: COLUMN oficina_ficha.dt_avaliacao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.oficina_ficha.dt_avaliacao IS 'Data da avaliação';


--
-- TOC entry 8320 (class 0 OID 0)
-- Dependencies: 435
-- Name: COLUMN oficina_ficha.dt_ultima_notificacao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.oficina_ficha.dt_ultima_notificacao IS 'Quando a ficha esitver na situação AVALIADA/REVISADA, notificar semanalmente os administradores';


--
-- TOC entry 8321 (class 0 OID 0)
-- Dependencies: 435
-- Name: COLUMN oficina_ficha.st_transferida; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.oficina_ficha.st_transferida IS 'Indicar quando a ficha for transferida para uma outra oficina.';


--
-- TOC entry 8322 (class 0 OID 0)
-- Dependencies: 435
-- Name: COLUMN oficina_ficha.st_pf_convidado; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.oficina_ficha.st_pf_convidado IS 'Campo para indicar se o PONTO FOCAL está ou não convidado para participar do chat com os validadores.';


--
-- TOC entry 8323 (class 0 OID 0)
-- Dependencies: 435
-- Name: COLUMN oficina_ficha.st_ct_convidado; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.oficina_ficha.st_ct_convidado IS 'Campo para indicar se o COORDENADOR DE TAXON está ou não convidado para participar do chat com os validadores.';


--
-- TOC entry 8324 (class 0 OID 0)
-- Dependencies: 435
-- Name: COLUMN oficina_ficha.st_possivelmente_extinta; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.oficina_ficha.st_possivelmente_extinta IS 'Preenchido com S/N/D durante a oficina - para Sim, Não ou Desconhecido';


--
-- TOC entry 8325 (class 0 OID 0)
-- Dependencies: 435
-- Name: COLUMN oficina_ficha.st_chat_alterado; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.oficina_ficha.st_chat_alterado IS 'Campo para indentificar que existe nova mensagem no módulo bate papo.';


--
-- TOC entry 225 (class 1259 OID 18097)
-- Name: pessoa; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.pessoa (
    sq_pessoa integer NOT NULL,
    no_pessoa text NOT NULL,
    de_email text,
    dt_inclusao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    dt_alteracao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


--
-- TOC entry 8326 (class 0 OID 0)
-- Dependencies: 225
-- Name: TABLE pessoa; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON TABLE salve.pessoa IS 'Tabela para armazenar as pessoas físicas e jurídicas do sistema';


--
-- TOC entry 8327 (class 0 OID 0)
-- Dependencies: 225
-- Name: COLUMN pessoa.sq_pessoa; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.pessoa.sq_pessoa IS 'Chave primária e sequencial';


--
-- TOC entry 8328 (class 0 OID 0)
-- Dependencies: 225
-- Name: COLUMN pessoa.no_pessoa; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.pessoa.no_pessoa IS 'Nome completo da pessoa';


--
-- TOC entry 8329 (class 0 OID 0)
-- Dependencies: 225
-- Name: COLUMN pessoa.de_email; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.pessoa.de_email IS 'Email eletrônico da pessoa';


--
-- TOC entry 8330 (class 0 OID 0)
-- Dependencies: 225
-- Name: COLUMN pessoa.dt_inclusao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.pessoa.dt_inclusao IS 'Data de inclusão no banco de dados';


--
-- TOC entry 8331 (class 0 OID 0)
-- Dependencies: 225
-- Name: COLUMN pessoa.dt_alteracao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.pessoa.dt_alteracao IS 'Data da última alteração';


--
-- TOC entry 268 (class 1259 OID 22972)
-- Name: taxon; Type: TABLE; Schema: taxonomia; Owner: -
--

CREATE TABLE taxonomia.taxon (
    sq_taxon integer NOT NULL,
    sq_taxon_pai bigint,
    sq_nivel_taxonomico bigint NOT NULL,
    sq_situacao_nome bigint NOT NULL,
    no_taxon text NOT NULL,
    st_ativo boolean NOT NULL,
    no_autor text,
    nu_ano integer,
    in_ocorre_brasil boolean DEFAULT false NOT NULL,
    de_autor_ano text,
    json_trilha json,
    ts_ultima_alteracao timestamp without time zone DEFAULT now() NOT NULL,
    CONSTRAINT chk_taxon_ocorre_brasil CHECK ((in_ocorre_brasil = ANY (ARRAY[true, false])))
);


--
-- TOC entry 8332 (class 0 OID 0)
-- Dependencies: 268
-- Name: COLUMN taxon.json_trilha; Type: COMMENT; Schema: taxonomia; Owner: -
--

COMMENT ON COLUMN taxonomia.taxon.json_trilha IS 'Coluna para registrar a estrutura taxonomica no formato json para otimização em consultas e exibição da hierarquia taxonômica';


--
-- TOC entry 8333 (class 0 OID 0)
-- Dependencies: 268
-- Name: COLUMN taxon.ts_ultima_alteracao; Type: COMMENT; Schema: taxonomia; Owner: -
--

COMMENT ON COLUMN taxonomia.taxon.ts_ultima_alteracao IS 'Adiciona a data alteracao do registro.';


--
-- TOC entry 525 (class 1259 OID 29627)
-- Name: vw_ficha_modulo_publico_ssc; Type: VIEW; Schema: salve; Owner: -
--

CREATE VIEW salve.vw_ficha_modulo_publico_ssc AS
 SELECT (ficha.sq_ficha)::bigint AS sq_ficha,
    (fv.js_ficha ->> 'nm_cientifico'::text) AS nm_cientifico,
    fv.sq_ficha_versao,
    fv.nu_versao,
    'Publicada'::text AS ds_situacao_ficha,
    'PUBLICADA'::text AS cd_situacao_ficha_sistema,
    ((fv.js_ficha -> 'distribuicao'::text) ->> 'st_endemica_brasil'::text) AS st_endemica_brasil,
    ((fv.js_ficha -> 'historico_avaliacao'::text) ->> 'st_presenca_lista_vigente'::text) AS st_presenca_lista_vigente,
    versao_anterior.sq_taxon_anterior,
    versao_anterior.no_cientifico_anterior,
    versao_anterior.no_taxon_anterior,
    (COALESCE((((fv.js_ficha -> 'taxon_trilha'::text) -> 'subespecie'::text) ->> 'sq_taxon'::text), (((fv.js_ficha -> 'taxon_trilha'::text) -> 'especie'::text) ->> 'sq_taxon'::text)))::bigint AS sq_taxon,
    COALESCE((((fv.js_ficha -> 'taxon_trilha'::text) -> 'subespecie'::text) ->> 'no_taxon'::text), (((fv.js_ficha -> 'taxon_trilha'::text) -> 'especie'::text) ->> 'no_taxon'::text)) AS no_taxon,
    COALESCE((((fv.js_ficha -> 'taxon_trilha'::text) -> 'subespecie'::text) ->> 'co_nivel_taxonomico'::text), (((fv.js_ficha -> 'taxon_trilha'::text) -> 'especie'::text) ->> 'co_nivel_taxonomico'::text)) AS co_nivel_taxonomico,
    (COALESCE((((fv.js_ficha -> 'taxon_trilha'::text) -> 'subespecie'::text) ->> 'sq_nivel_taxonomico'::text), (((fv.js_ficha -> 'taxon_trilha'::text) -> 'especie'::text) ->> 'sq_nivel_taxonomico'::text)))::bigint AS sq_nivel_taxonomico,
    (COALESCE((((fv.js_ficha -> 'taxon_trilha'::text) -> 'subespecie'::text) ->> 'nu_grau_taxonomico'::text), (((fv.js_ficha -> 'taxon_trilha'::text) -> 'especie'::text) ->> 'nu_grau_taxonomico'::text)))::integer AS nu_grau_taxonomico,
    ((fv.js_ficha ->> 'sq_unidade_responsavel'::text))::bigint AS sq_unidade_org,
    (fv.js_ficha ->> 'sg_unidade_responsavel'::text) AS sg_unidade_org,
    (fv.js_ficha ->> 'no_unidade_responsavel'::text) AS no_unidade_org,
    ((((fv.js_ficha -> 'avaliacao'::text) -> 'resultado_validacao'::text) ->> 'sq_categoria_final'::text))::bigint AS sq_categoria_final,
    (((fv.js_ficha -> 'avaliacao'::text) -> 'resultado_validacao'::text) ->> 'ds_categoria_final'::text) AS ds_categoria_final,
    (((fv.js_ficha -> 'avaliacao'::text) -> 'resultado_validacao'::text) ->> 'cd_categoria_final'::text) AS cd_categoria_final,
    (((fv.js_ficha -> 'avaliacao'::text) -> 'resultado_validacao'::text) ->> 'cd_categoria_final_sitema'::text) AS cd_categoria_final_sistema,
    (((fv.js_ficha -> 'avaliacao'::text) -> 'resultado_validacao'::text) ->> 'ds_criterio_aval_iucn_final'::text) AS ds_criterio_aval_iucn_final,
    (((fv.js_ficha -> 'avaliacao'::text) -> 'resultado_validacao'::text) ->> 'st_possivelmente_extinta_final'::text) AS st_possivelmente_extinta_final,
    (((fv.js_ficha -> 'avaliacao'::text) -> 'resultado_validacao'::text) ->> 'de_categoria_final_ordem'::text) AS de_categoria_final_ordem,
    (((fv.js_ficha -> 'avaliacao'::text) -> 'resultado_avaliacao'::text) ->> 'ds_citacao'::text) AS ds_citacao,
    ((((fv.js_ficha -> 'avaliacao'::text) -> 'resultado_avaliacao'::text) ->> 'dt_fim_avaliacao'::text))::date AS dt_fim_avaliacao,
    (((fv.js_ficha -> 'avaliacao'::text) -> 'resultado_avaliacao'::text) ->> 'nu_mes_ano_ultima_avaliacao'::text) AS de_periodo_avaliacao,
    ((((fv.js_ficha -> 'classificacao_taxonomica'::text) -> 'grupo'::text) ->> 'sq_grupo'::text))::bigint AS sq_grupo_avaliado,
    (((fv.js_ficha -> 'classificacao_taxonomica'::text) -> 'grupo'::text) ->> 'ds_grupo'::text) AS ds_grupo_avaliado,
    (((fv.js_ficha -> 'classificacao_taxonomica'::text) -> 'grupo'::text) ->> 'cd_grupo_sistema'::text) AS cd_grupo_avaliado_sistema,
    (((fv.js_ficha -> 'classificacao_taxonomica'::text) -> 'grupo'::text) ->> 'de_grupo_ordem'::text) AS de_grupo_avaliado_ordem,
    ((((fv.js_ficha -> 'classificacao_taxonomica'::text) -> 'recorte_publico'::text) ->> 'sq_recorte_publico'::text))::bigint AS sq_grupo_salve,
    (((fv.js_ficha -> 'classificacao_taxonomica'::text) -> 'recorte_publico'::text) ->> 'ds_recorte_publico'::text) AS ds_grupo_salve,
    (((fv.js_ficha -> 'classificacao_taxonomica'::text) -> 'recorte_publico'::text) ->> 'de_recorte_publico_ordem'::text) AS de_grupo_salve_ordem,
    (((fv.js_ficha -> 'classificacao_taxonomica'::text) -> 'recorte_publico'::text) ->> 'cd_recorte_publico_sistema'::text) AS cd_grupo_salve_sistema,
    ((fv.js_ficha -> 'distribuicao'::text) ->> 'ds_distribuicao_geo_global'::text) AS ds_distribuicao_geo_global,
    ((fv.js_ficha -> 'distribuicao'::text) ->> 'ds_distribuicao_geo_nacional'::text) AS ds_distribuicao_geo_nacional,
    (((fv.js_ficha -> 'distribuicao'::text) ->> 'nu_min_altitude'::text))::integer AS nu_min_altitude,
    (((fv.js_ficha -> 'distribuicao'::text) ->> 'nu_max_altitude'::text))::integer AS nu_max_altitude,
    (((fv.js_ficha -> 'distribuicao'::text) ->> 'nu_min_batimetria'::text))::integer AS nu_min_batimetria,
    (((fv.js_ficha -> 'distribuicao'::text) ->> 'nu_max_batimetria'::text))::integer AS nu_max_batimetria,
    nomes_comuns.no_comuns AS no_comum,
    nomes_antigos.no_antigos AS no_antigo
   FROM ((((salve.ficha
     JOIN salve.ficha_versao fv ON (((fv.sq_ficha = ficha.sq_ficha) AND (fv.st_publico = true))))
     LEFT JOIN LATERAL ( SELECT array_to_string(array_agg(DISTINCT
                CASE
                    WHEN (((nomes_comuns_1.value ->> 'de_regiao_lingua'::text) ~~* 'portu%'::text) OR ((nomes_comuns_1.value ->> 'de_regiao_lingua'::text) IS NULL) OR ((nomes_comuns_1.value ->> 'de_regiao_lingua'::text) = ''::text)) THEN (nomes_comuns_1.value ->> 'no_comum'::text)
                    ELSE NULL::text
                END), ', '::text) AS no_comuns
           FROM jsonb_array_elements((((fv.js_ficha -> 'classificacao_taxonomica'::text) ->> 'nomes_comuns'::text))::jsonb) nomes_comuns_1(value)) nomes_comuns ON (true))
     LEFT JOIN LATERAL ( SELECT array_to_string(array_agg(DISTINCT (nomes_antigos_1.value ->> 'no_antigo'::text)), ', '::text) AS no_antigos
           FROM jsonb_array_elements((((fv.js_ficha -> 'classificacao_taxonomica'::text) ->> 'nomes_antigos'::text))::jsonb) nomes_antigos_1(value)) nomes_antigos ON (true))
     LEFT JOIN LATERAL ( SELECT x.nu_versao AS nu_versao_anterior,
            (x.js_ficha ->> 'nm_cientifico'::text) AS no_cientifico_anterior,
            (COALESCE((((x.js_ficha -> 'taxon_trilha'::text) -> 'subespecie'::text) ->> 'sq_taxon'::text), (((x.js_ficha -> 'taxon_trilha'::text) -> 'especie'::text) ->> 'sq_taxon'::text)))::bigint AS sq_taxon_anterior,
            COALESCE((((x.js_ficha -> 'taxon_trilha'::text) -> 'subespecie'::text) ->> 'no_taxon'::text), (((x.js_ficha -> 'taxon_trilha'::text) -> 'especie'::text) ->> 'no_taxon'::text)) AS no_taxon_anterior
           FROM salve.ficha_versao x
          WHERE ((x.sq_ficha = fv.sq_ficha) AND (x.nu_versao < fv.nu_versao) AND (x.sq_contexto = fv.sq_contexto))
          ORDER BY x.sq_ficha, x.nu_versao DESC
         LIMIT 1) versao_anterior ON (true))
  WHERE (ficha.sq_ciclo_avaliacao = 2)
UNION
 SELECT (ficha.sq_ficha)::bigint AS sq_ficha,
    ficha.nm_cientifico,
    NULL::bigint AS sq_ficha_versao,
    NULL::integer AS nu_versao,
    situacao_ficha.ds_dados_apoio AS ds_situacao_ficha,
    situacao_ficha.cd_sistema AS cd_situacao_ficha_sistema,
    (ficha.st_endemica_brasil)::text AS st_endemica_brasil,
    (ficha.st_presenca_lista_vigente)::text AS st_presenca_lista_vigente,
    ficha_anterior.sq_taxon AS sq_taxon_anterior,
    ficha_anterior.nm_cientifico AS no_cientifico_anterior,
    COALESCE(((taxon_anterior.json_trilha -> 'subespecie'::text) ->> 'no_taxon'::text), ((taxon_anterior.json_trilha -> 'especie'::text) ->> 'no_taxon'::text)) AS no_taxon_anterior,
    ficha.sq_taxon,
    COALESCE(((taxon.json_trilha -> 'subespecie'::text) ->> 'no_taxon'::text), ((taxon.json_trilha -> 'especie'::text) ->> 'no_taxon'::text)) AS no_taxon,
    nivel_taxonomico.co_nivel_taxonomico,
    (nivel_taxonomico.sq_nivel_taxonomico)::bigint AS sq_nivel_taxonomico,
    nivel_taxonomico.nu_grau_taxonomico,
    ficha.sq_unidade_org,
    instituicao.sg_instituicao AS sg_unidade_org,
    pessoa.no_pessoa AS no_unidade_org,
    ficha.sq_categoria_final,
    categoria_final.ds_dados_apoio AS ds_categoria_final,
    categoria_final.cd_dados_apoio AS cd_categoria_final,
    categoria_final.cd_sistema AS cd_categoria_final_sistema,
    ficha.ds_criterio_aval_iucn_final,
    (ficha.st_possivelmente_extinta_final)::text AS st_possivelmente_extinta_final,
    categoria_final.de_dados_apoio_ordem AS de_categoria_final_ordem,
    ficha.ds_citacao,
    oficina_avaliacao.dt_fim_avaliacao,
    oficina_avaliacao.de_periodo_avaliacao,
    ficha.sq_grupo AS sq_grupo_avaliado,
    grupo.ds_dados_apoio AS ds_grupo_avaliado,
    grupo.cd_sistema AS cd_grupo_avaliado_sistema,
    grupo.de_dados_apoio_ordem AS de_grupo_avaliado_ordem,
    ficha.sq_grupo_salve,
    grupo_salve.ds_dados_apoio AS ds_grupo_salve,
    grupo_salve.de_dados_apoio_ordem AS de_grupo_salve_ordem,
    grupo_salve.cd_sistema AS cd_grupo_salve_sistema,
    ficha.ds_distribuicao_geo_global,
    ficha.ds_distribuicao_geo_nacional,
    ficha.nu_min_altitude,
    ficha.nu_max_altitude,
    ficha.nu_min_batimetria,
    ficha.nu_max_batimetria,
    nome_comum.no_comum,
    nome_antigo.no_antigo
   FROM ((((((((((((((salve.ficha
     JOIN salve.dados_apoio situacao_ficha ON ((situacao_ficha.sq_dados_apoio = ficha.sq_situacao_ficha)))
     JOIN taxonomia.taxon ON ((taxon.sq_taxon = ficha.sq_taxon)))
     JOIN taxonomia.nivel_taxonomico ON ((nivel_taxonomico.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico)))
     LEFT JOIN salve.ficha ficha_anterior ON ((ficha_anterior.sq_ficha = ficha.sq_ficha_ciclo_anterior)))
     LEFT JOIN taxonomia.taxon taxon_anterior ON ((taxon_anterior.sq_taxon = ficha_anterior.sq_taxon)))
     JOIN salve.instituicao instituicao ON ((instituicao.sq_pessoa = ficha.sq_unidade_org)))
     JOIN salve.pessoa pessoa ON ((pessoa.sq_pessoa = ficha.sq_unidade_org)))
     LEFT JOIN salve.dados_apoio categoria_avaliada ON ((categoria_avaliada.sq_dados_apoio = ficha.sq_categoria_iucn)))
     LEFT JOIN salve.dados_apoio categoria_final ON ((categoria_final.sq_dados_apoio = ficha.sq_categoria_final)))
     LEFT JOIN salve.dados_apoio grupo ON ((grupo.sq_dados_apoio = ficha.sq_grupo)))
     LEFT JOIN salve.dados_apoio grupo_salve ON ((grupo_salve.sq_dados_apoio = ficha.sq_grupo_salve)))
     LEFT JOIN LATERAL ( SELECT array_to_string(array_agg(DISTINCT fnc.no_comum), ', '::text) AS no_comum
           FROM (((salve.ficha_nome_comum fnc
             JOIN salve.ficha ficha_1 ON ((ficha_1.sq_ficha = fnc.sq_ficha)))
             JOIN salve.ciclo_avaliacao ciclo_1 ON ((ciclo_1.sq_ciclo_avaliacao = ficha_1.sq_ciclo_avaliacao)))
             JOIN taxonomia.taxon taxon_1 ON ((taxon_1.sq_taxon = ficha_1.sq_taxon)))
          WHERE (((taxon_1.sq_taxon = ficha.sq_taxon) OR (taxon_1.sq_taxon_pai = ficha.sq_taxon)) AND (ciclo_1.in_publico = 'S'::bpchar) AND ((fnc.de_regiao_lingua ~~* 'portu%'::text) OR (fnc.de_regiao_lingua IS NULL) OR (fnc.de_regiao_lingua = ''::text)))) nome_comum ON (true))
     LEFT JOIN LATERAL ( SELECT array_to_string(array_agg(DISTINCT fnc.no_sinonimia), ', '::text) AS no_antigo
           FROM (((salve.ficha_sinonimia fnc
             JOIN salve.ficha ficha_1 ON ((ficha_1.sq_ficha = fnc.sq_ficha)))
             JOIN salve.ciclo_avaliacao ciclo_1 ON ((ciclo_1.sq_ciclo_avaliacao = ficha_1.sq_ciclo_avaliacao)))
             JOIN taxonomia.taxon taxon_1 ON ((taxon_1.sq_taxon = ficha_1.sq_taxon)))
          WHERE (((taxon_1.sq_taxon = ficha.sq_taxon) OR (taxon_1.sq_taxon_pai = ficha.sq_taxon)) AND (ciclo_1.in_publico = 'S'::bpchar))) nome_antigo ON (true))
     LEFT JOIN LATERAL ( SELECT to_char((oficina.dt_fim)::timestamp with time zone, 'MM/yyyy'::text) AS de_periodo_avaliacao,
            oficina.dt_fim AS dt_fim_avaliacao
           FROM ((((salve.oficina_ficha
             JOIN salve.oficina ON ((oficina.sq_oficina = oficina_ficha.sq_oficina)))
             JOIN salve.dados_apoio ON ((oficina.sq_tipo_oficina = dados_apoio.sq_dados_apoio)))
             JOIN salve.ficha ficha_1 ON ((ficha_1.sq_ficha = oficina_ficha.sq_ficha)))
             JOIN salve.ciclo_avaliacao ciclo ON ((ciclo.sq_ciclo_avaliacao = ficha_1.sq_ciclo_avaliacao)))
          WHERE ((oficina_ficha.sq_ficha = ficha.sq_ficha) AND (dados_apoio.cd_sistema = 'OFICINA_AVALIACAO'::text) AND (ciclo.in_publico = 'S'::bpchar))
          ORDER BY oficina.dt_fim DESC
         OFFSET 0
         LIMIT 1) oficina_avaliacao ON (true))
  WHERE ((ficha.sq_ciclo_avaliacao = 2) AND (situacao_ficha.cd_sistema = ANY (ARRAY['POS-OFICINA'::text, 'VALIDADA'::text, 'FINALIZADA'::text])));


--
-- TOC entry 526 (class 1259 OID 29632)
-- Name: mv_dados_modulo_publico_ssc; Type: MATERIALIZED VIEW; Schema: salve; Owner: -
--

CREATE MATERIALIZED VIEW salve.mv_dados_modulo_publico_ssc AS
 SELECT vfmpv.sq_ficha,
    vfmpv.sq_ficha_versao,
    vfmpv.nu_versao,
        CASE
            WHEN (vfmpv.nu_versao IS NULL) THEN NULL::bigint
            ELSE vfmpv.sq_ficha
        END AS sq_ficha_publicada,
    NULL::bigint AS sq_ficha_ciclo_anterior,
    vfmpv.sq_taxon,
    vfmpv.sq_taxon_anterior,
    NULL::text AS sq_ciclo_avaliacao,
    NULL::text AS nu_ano_ciclo,
    ((((ficha_versao.js_ficha -> 'taxon_trilha'::text) -> 'reino'::text) ->> 'sq_taxon'::text))::bigint AS sq_reino,
    ((((ficha_versao.js_ficha -> 'taxon_trilha'::text) -> 'filo'::text) ->> 'sq_taxon'::text))::bigint AS sq_filo,
    ((((ficha_versao.js_ficha -> 'taxon_trilha'::text) -> 'subfilo'::text) ->> 'sq_taxon'::text))::bigint AS sq_subfilo,
    ((((ficha_versao.js_ficha -> 'taxon_trilha'::text) -> 'classe'::text) ->> 'sq_taxon'::text))::bigint AS sq_classe,
    ((((ficha_versao.js_ficha -> 'taxon_trilha'::text) -> 'ordem'::text) ->> 'sq_taxon'::text))::bigint AS sq_ordem,
    ((((ficha_versao.js_ficha -> 'taxon_trilha'::text) -> 'familia'::text) ->> 'sq_taxon'::text))::bigint AS sq_familia,
    ((((ficha_versao.js_ficha -> 'taxon_trilha'::text) -> 'subfamilia'::text) ->> 'sq_taxon'::text))::bigint AS sq_subfamilia,
    ((((ficha_versao.js_ficha -> 'taxon_trilha'::text) -> 'tribo'::text) ->> 'sq_taxon'::text))::bigint AS sq_tribo,
    ((((ficha_versao.js_ficha -> 'taxon_trilha'::text) -> 'genero'::text) ->> 'sq_taxon'::text))::bigint AS sq_genero,
    ((((ficha_versao.js_ficha -> 'taxon_trilha'::text) -> 'especie'::text) ->> 'sq_taxon'::text))::bigint AS sq_especie,
    ((((ficha_versao.js_ficha -> 'taxon_trilha'::text) -> 'subespecie'::text) ->> 'sq_taxon'::text))::bigint AS sq_subespecie,
    (((ficha_versao.js_ficha -> 'taxon_trilha'::text) -> 'reino'::text) ->> 'no_taxon'::text) AS no_reino,
    (((ficha_versao.js_ficha -> 'taxon_trilha'::text) -> 'filo'::text) ->> 'no_taxon'::text) AS no_filo,
    (((ficha_versao.js_ficha -> 'taxon_trilha'::text) -> 'subfilo'::text) ->> 'no_taxon'::text) AS no_subfilo,
    (((ficha_versao.js_ficha -> 'taxon_trilha'::text) -> 'classe'::text) ->> 'no_taxon'::text) AS no_classe,
    (((ficha_versao.js_ficha -> 'taxon_trilha'::text) -> 'ordem'::text) ->> 'no_taxon'::text) AS no_ordem,
    (((ficha_versao.js_ficha -> 'taxon_trilha'::text) -> 'familia'::text) ->> 'no_taxon'::text) AS no_familia,
    (((ficha_versao.js_ficha -> 'taxon_trilha'::text) -> 'subfamilia'::text) ->> 'no_taxon'::text) AS no_subfamilia,
    (((ficha_versao.js_ficha -> 'taxon_trilha'::text) -> 'tribo'::text) ->> 'no_taxon'::text) AS no_tribo,
    (((ficha_versao.js_ficha -> 'taxon_trilha'::text) -> 'genero'::text) ->> 'no_taxon'::text) AS no_genero,
    (((ficha_versao.js_ficha -> 'taxon_trilha'::text) -> 'especie'::text) ->> 'no_taxon'::text) AS no_especie,
    (((ficha_versao.js_ficha -> 'taxon_trilha'::text) -> 'subespecie'::text) ->> 'no_taxon'::text) AS no_subespecie,
    (ficha_versao.js_ficha ->> 'no_autor_taxon'::text) AS no_autor_taxon,
    (ficha_versao.js_ficha ->> 'nu_ano_taxon'::text) AS nu_ano_taxon,
    vfmpv.no_taxon,
    NULL::text AS no_taxon_posterior,
    vfmpv.nm_cientifico,
    vfmpv.no_cientifico_anterior,
    NULL::text AS nm_cientifico_posterior,
    vfmpv.no_comum,
    vfmpv.no_antigo,
    estados.sq_estados_avaliados AS sq_estado,
    estados.no_estados_avaliados AS no_estado,
    estados.sg_estados_avaliados AS sg_estado,
    estados.sq_regiao_avaliadas AS sq_regiao,
    estados.no_regiao_avaliadas AS no_regiao,
    estados.sq_estados_nao_avaliados AS sq_estado_nao_avaliado,
    estados.no_estados_nao_avaliados AS no_estado_nao_avaliado,
    estados.sg_estados_nao_avaliados AS sg_estado_nao_avaliado,
    estados.sq_regiao_nao_avaliadas AS sq_regiao_nao_avaliada,
    estados.no_regiao_nao_avaliadas AS no_regiao_nao_avaliada,
    biomas.sq_biomas_nao_avaliados AS sq_bioma_nao_avaliado,
    biomas.no_biomas_nao_avaliados AS no_bioma_nao_avaliado,
    biomas.sq_biomas_avaliados AS sq_bioma,
    biomas.no_biomas_avaliados AS no_bioma,
    bacias.sq_bacias_avaliadas AS sq_bacia,
    bacias.no_bacias_avaliadas AS no_bacia,
    bacias.sq_bacias_nao_avaliadas AS sq_bacia_nao_avaliada,
    bacias.no_bacias_nao_avaliadas AS no_bacia_nao_avaliada,
    ucs.sq_uc_federal_avaliada AS sq_uc_federal,
    ucs.no_uc_federal_avaliada AS no_uc_federal,
    ''::text AS sg_uc_federal,
    ucs.sq_uc_estadual_avaliada AS sq_uc_estadual,
    ucs.no_uc_estadual_avaliada AS no_uc_estadual,
    ucs.sq_rppn_avaliada AS sq_rppn,
    ucs.no_rppn_avaliada AS no_rppn,
    ucs.sq_uc_federal_nao_avaliada,
    ucs.no_uc_federal_nao_avaliada,
    ''::text AS sg_uc_federal_nao_avaliada,
    ucs.sq_uc_estadual_nao_avaliada,
    ucs.no_uc_estadual_nao_avaliada,
    ucs.sq_rppn_nao_avaliada,
    ucs.no_rppn_nao_avaliada,
    vfmpv.sq_categoria_final,
    vfmpv.ds_categoria_final,
    vfmpv.cd_categoria_final,
    vfmpv.cd_categoria_final_sistema,
    vfmpv.ds_criterio_aval_iucn_final,
    vfmpv.de_categoria_final_ordem,
    vfmpv.st_possivelmente_extinta_final,
    vfmpv.dt_fim_avaliacao,
    vfmpv.de_periodo_avaliacao,
    vfmpv.sq_grupo_avaliado,
    vfmpv.ds_grupo_avaliado,
    vfmpv.cd_grupo_avaliado_sistema,
    vfmpv.de_grupo_avaliado_ordem,
    vfmpv.sq_grupo_salve,
    vfmpv.ds_grupo_salve,
    vfmpv.de_grupo_salve_ordem,
    vfmpv.cd_grupo_salve_sistema,
    vfmpv.st_presenca_lista_vigente,
    vfmpv.cd_situacao_ficha_sistema AS cd_situacao_ficha,
    vfmpv.co_nivel_taxonomico,
    vfmpv.nu_grau_taxonomico,
    vfmpv.st_endemica_brasil,
    salve.snd(vfmpv.st_endemica_brasil) AS de_endemica_brasil,
    historico.nu_ano_avaliacao_hist AS nu_ano_avaliacao_oficial,
    historico.sq_categoria_avaliacao_hist AS sq_categoria_oficial,
    historico.ds_categoria_avaliacao_hist AS ds_categoria_oficial,
    historico.cd_categoria_avaliacao_hist AS cd_categoria_oficial,
    historico.cd_categoria_avaliacao_sistema_hist AS cd_categoria_oficial_sistema,
    historico.de_criterio_avaliacao_hist AS ds_criterio_iucn_oficial,
    ameacas.no_ameacas AS no_ameaca,
    ameacas.sq_ameacas AS sq_ameaca,
    usos.no_usos AS no_uso,
    usos.sq_usos AS sq_uso,
    acoes.no_acoes_conservacao AS no_acao_conservacao,
    acoes.sq_acoes_conservacao AS sq_acao_conservacao,
    acoes.sq_planos_acao AS sq_plano_acao,
    acoes.tx_planos_acao AS no_plano_acao,
    vfmpv.ds_citacao,
    vfmpv.ds_distribuicao_geo_global,
    vfmpv.ds_distribuicao_geo_nacional,
    replace(lower((public.fn_remove_acentuacao((concat(vfmpv.nm_cientifico, ' ', vfmpv.no_cientifico_anterior, ' ', vfmpv.no_comum, ' ', vfmpv.no_antigo))::character varying))::text), '-'::text, ' '::text) AS tx_nomes_taxon
   FROM (((((((((salve.vw_ficha_modulo_publico_ssc vfmpv
     LEFT JOIN salve.ficha_versao ON ((ficha_versao.sq_ficha_versao = vfmpv.sq_ficha_versao)))
     LEFT JOIN LATERAL ( SELECT array_to_string(array_agg(DISTINCT
                CASE
                    WHEN (((estados_1.value ->> 'st_adicionado_apos_avaliacao'::text))::boolean = true) THEN (estados_1.value ->> 'no_estado'::text)
                    ELSE NULL::text
                END), ', '::text) AS no_estados_nao_avaliados,
            array_to_string(array_agg(DISTINCT
                CASE
                    WHEN (((estados_1.value ->> 'st_adicionado_apos_avaliacao'::text))::boolean = true) THEN (estados_1.value ->> 'sg_estado'::text)
                    ELSE NULL::text
                END), ', '::text) AS sg_estados_nao_avaliados,
            array_to_string(array_agg(DISTINCT
                CASE
                    WHEN (((estados_1.value ->> 'st_adicionado_apos_avaliacao'::text))::boolean = true) THEN (estados_1.value ->> 'sq_estado'::text)
                    ELSE NULL::text
                END), '|'::text) AS sq_estados_nao_avaliados,
            array_to_string(array_agg(DISTINCT
                CASE
                    WHEN (((estados_1.value ->> 'st_adicionado_apos_avaliacao'::text))::boolean = true) THEN (estados_1.value ->> 'sq_regiao'::text)
                    ELSE NULL::text
                END), '|'::text) AS sq_regiao_nao_avaliadas,
            array_to_string(array_agg(DISTINCT
                CASE
                    WHEN (((estados_1.value ->> 'st_adicionado_apos_avaliacao'::text))::boolean = true) THEN (estados_1.value ->> 'no_regiao'::text)
                    ELSE NULL::text
                END), '|'::text) AS no_regiao_nao_avaliadas,
            array_to_string(array_agg(DISTINCT
                CASE
                    WHEN (((estados_1.value ->> 'st_adicionado_apos_avaliacao'::text))::boolean = false) THEN (estados_1.value ->> 'no_estado'::text)
                    ELSE NULL::text
                END), ', '::text) AS no_estados_avaliados,
            array_to_string(array_agg(DISTINCT
                CASE
                    WHEN (((estados_1.value ->> 'st_adicionado_apos_avaliacao'::text))::boolean = false) THEN (estados_1.value ->> 'sg_estado'::text)
                    ELSE NULL::text
                END), ', '::text) AS sg_estados_avaliados,
            array_to_string(array_agg(DISTINCT
                CASE
                    WHEN (((estados_1.value ->> 'st_adicionado_apos_avaliacao'::text))::boolean = false) THEN (estados_1.value ->> 'sq_estado'::text)
                    ELSE NULL::text
                END), '|'::text) AS sq_estados_avaliados,
            array_to_string(array_agg(DISTINCT
                CASE
                    WHEN (((estados_1.value ->> 'st_adicionado_apos_avaliacao'::text))::boolean = false) THEN (estados_1.value ->> 'sq_regiao'::text)
                    ELSE NULL::text
                END), '|'::text) AS sq_regiao_avaliadas,
            array_to_string(array_agg(DISTINCT
                CASE
                    WHEN (((estados_1.value ->> 'st_adicionado_apos_avaliacao'::text))::boolean = false) THEN (estados_1.value ->> 'no_regiao'::text)
                    ELSE NULL::text
                END), '|'::text) AS no_regiao_avaliadas
           FROM jsonb_array_elements((((ficha_versao.js_ficha -> 'distribuicao'::text) ->> 'estados'::text))::jsonb) estados_1(value)) estados ON (true))
     LEFT JOIN LATERAL ( SELECT array_to_string(array_agg(DISTINCT
                CASE
                    WHEN (((biomas_1.value ->> 'st_adicionado_apos_avaliacao'::text))::boolean = true) THEN (biomas_1.value ->> 'no_bioma'::text)
                    ELSE NULL::text
                END), ', '::text) AS no_biomas_nao_avaliados,
            array_to_string(array_agg(DISTINCT
                CASE
                    WHEN (((biomas_1.value ->> 'st_adicionado_apos_avaliacao'::text))::boolean = true) THEN (biomas_1.value ->> 'sq_bioma'::text)
                    ELSE NULL::text
                END), '|'::text) AS sq_biomas_nao_avaliados,
            array_to_string(array_agg(DISTINCT
                CASE
                    WHEN (((biomas_1.value ->> 'st_adicionado_apos_avaliacao'::text))::boolean = false) THEN (biomas_1.value ->> 'no_bioma'::text)
                    ELSE NULL::text
                END), ', '::text) AS no_biomas_avaliados,
            array_to_string(array_agg(DISTINCT
                CASE
                    WHEN (((biomas_1.value ->> 'st_adicionado_apos_avaliacao'::text))::boolean = false) THEN (biomas_1.value ->> 'sq_bioma'::text)
                    ELSE NULL::text
                END), '|'::text) AS sq_biomas_avaliados
           FROM jsonb_array_elements((((ficha_versao.js_ficha -> 'distribuicao'::text) ->> 'biomas'::text))::jsonb) biomas_1(value)) biomas ON (true))
     LEFT JOIN LATERAL ( SELECT array_to_string(array_agg(DISTINCT
                CASE
                    WHEN (((bacias_1.value ->> 'st_adicionado_apos_avaliacao'::text))::boolean = true) THEN (bacias_1.value ->> 'no_bacia'::text)
                    ELSE NULL::text
                END), ', '::text) AS no_bacias_nao_avaliadas,
            array_to_string(array_agg(DISTINCT
                CASE
                    WHEN (((bacias_1.value ->> 'st_adicionado_apos_avaliacao'::text))::boolean = true) THEN (bacias_1.value ->> 'sq_bacia'::text)
                    ELSE NULL::text
                END), '|'::text) AS sq_bacias_nao_avaliadas,
            array_to_string(array_agg(DISTINCT
                CASE
                    WHEN (((bacias_1.value ->> 'st_adicionado_apos_avaliacao'::text))::boolean = false) THEN (bacias_1.value ->> 'no_bacia'::text)
                    ELSE NULL::text
                END), ', '::text) AS no_bacias_avaliadas,
            array_to_string(array_agg(DISTINCT
                CASE
                    WHEN (((bacias_1.value ->> 'st_adicionado_apos_avaliacao'::text))::boolean = false) THEN (bacias_1.value ->> 'sq_bacia'::text)
                    ELSE NULL::text
                END), '|'::text) AS sq_bacias_avaliadas
           FROM jsonb_array_elements((((ficha_versao.js_ficha -> 'distribuicao'::text) ->> 'bacias'::text))::jsonb) bacias_1(value)) bacias ON (true))
     LEFT JOIN LATERAL ( SELECT array_to_string(array_agg(DISTINCT
                CASE
                    WHEN (((ucs_1.value ->> 'cd_esfera'::text) = 'F'::text) AND (((ucs_1.value ->> 'st_adicionado_apos_avaliacao'::text))::boolean = true)) THEN (ucs_1.value ->> 'no_uc'::text)
                    ELSE NULL::text
                END), ', '::text) AS no_uc_federal_nao_avaliada,
            array_to_string(array_agg(DISTINCT
                CASE
                    WHEN (((ucs_1.value ->> 'cd_esfera'::text) = 'F'::text) AND (((ucs_1.value ->> 'st_adicionado_apos_avaliacao'::text))::boolean = true)) THEN (ucs_1.value ->> 'sq_uc'::text)
                    ELSE NULL::text
                END), '|'::text) AS sq_uc_federal_nao_avaliada,
            array_to_string(array_agg(DISTINCT
                CASE
                    WHEN (((ucs_1.value ->> 'cd_esfera'::text) = 'F'::text) AND (((ucs_1.value ->> 'st_adicionado_apos_avaliacao'::text))::boolean = false)) THEN (ucs_1.value ->> 'no_uc'::text)
                    ELSE NULL::text
                END), ', '::text) AS no_uc_federal_avaliada,
            array_to_string(array_agg(DISTINCT
                CASE
                    WHEN (((ucs_1.value ->> 'cd_esfera'::text) = 'F'::text) AND (((ucs_1.value ->> 'st_adicionado_apos_avaliacao'::text))::boolean = false)) THEN (ucs_1.value ->> 'sq_uc'::text)
                    ELSE NULL::text
                END), '|'::text) AS sq_uc_federal_avaliada,
            array_to_string(array_agg(DISTINCT
                CASE
                    WHEN (((ucs_1.value ->> 'cd_esfera'::text) = 'E'::text) AND (((ucs_1.value ->> 'st_adicionado_apos_avaliacao'::text))::boolean = true)) THEN (ucs_1.value ->> 'no_uc'::text)
                    ELSE NULL::text
                END), ', '::text) AS no_uc_estadual_nao_avaliada,
            array_to_string(array_agg(DISTINCT
                CASE
                    WHEN (((ucs_1.value ->> 'cd_esfera'::text) = 'E'::text) AND (((ucs_1.value ->> 'st_adicionado_apos_avaliacao'::text))::boolean = true)) THEN (ucs_1.value ->> 'sq_uc'::text)
                    ELSE NULL::text
                END), '|'::text) AS sq_uc_estadual_nao_avaliada,
            array_to_string(array_agg(DISTINCT
                CASE
                    WHEN (((ucs_1.value ->> 'cd_esfera'::text) = 'E'::text) AND (((ucs_1.value ->> 'st_adicionado_apos_avaliacao'::text))::boolean = false)) THEN (ucs_1.value ->> 'no_uc'::text)
                    ELSE NULL::text
                END), ', '::text) AS no_uc_estadual_avaliada,
            array_to_string(array_agg(DISTINCT
                CASE
                    WHEN (((ucs_1.value ->> 'cd_esfera'::text) = 'E'::text) AND (((ucs_1.value ->> 'st_adicionado_apos_avaliacao'::text))::boolean = false)) THEN (ucs_1.value ->> 'sq_uc'::text)
                    ELSE NULL::text
                END), '|'::text) AS sq_uc_estadual_avaliada,
            array_to_string(array_agg(DISTINCT
                CASE
                    WHEN (((ucs_1.value ->> 'cd_esfera'::text) = 'R'::text) AND (((ucs_1.value ->> 'st_adicionado_apos_avaliacao'::text))::boolean = true)) THEN (ucs_1.value ->> 'no_uc'::text)
                    ELSE NULL::text
                END), ', '::text) AS no_rppn_nao_avaliada,
            array_to_string(array_agg(DISTINCT
                CASE
                    WHEN (((ucs_1.value ->> 'cd_esfera'::text) = 'R'::text) AND (((ucs_1.value ->> 'st_adicionado_apos_avaliacao'::text))::boolean = true)) THEN (ucs_1.value ->> 'sq_uc'::text)
                    ELSE NULL::text
                END), '|'::text) AS sq_rppn_nao_avaliada,
            array_to_string(array_agg(DISTINCT
                CASE
                    WHEN (((ucs_1.value ->> 'cd_esfera'::text) = 'R'::text) AND (((ucs_1.value ->> 'st_adicionado_apos_avaliacao'::text))::boolean = false)) THEN (ucs_1.value ->> 'no_uc'::text)
                    ELSE NULL::text
                END), ', '::text) AS no_rppn_avaliada,
            array_to_string(array_agg(DISTINCT
                CASE
                    WHEN (((ucs_1.value ->> 'cd_esfera'::text) = 'R'::text) AND (((ucs_1.value ->> 'st_adicionado_apos_avaliacao'::text))::boolean = false)) THEN (ucs_1.value ->> 'sq_uc'::text)
                    ELSE NULL::text
                END), '|'::text) AS sq_rppn_avaliada
           FROM jsonb_array_elements((((ficha_versao.js_ficha -> 'presenca_uc'::text) ->> 'ucs'::text))::jsonb) ucs_1(value)) ucs ON (true))
     LEFT JOIN LATERAL ( SELECT ((historico_1.value ->> 'sq_categoria_iucn'::text))::bigint AS sq_categoria_avaliacao_hist,
            ((historico_1.value ->> 'nu_ano_avaliacao'::text))::integer AS nu_ano_avaliacao_hist,
            (historico_1.value ->> 'ds_categoria_iucn'::text) AS ds_categoria_avaliacao_hist,
            (historico_1.value ->> 'cd_categoria_iucn'::text) AS cd_categoria_avaliacao_hist,
            (historico_1.value ->> 'cd_categoria_iucn_sistema'::text) AS cd_categoria_avaliacao_sistema_hist,
            (historico_1.value ->> 'de_criterio_avaliacao_iucn'::text) AS de_criterio_avaliacao_hist
           FROM jsonb_array_elements((((ficha_versao.js_ficha -> 'historico_avaliacao'::text) ->> 'avaliacoes'::text))::jsonb) historico_1(value)
          WHERE ((historico_1.value ->> 'cd_tipo_avaliacao_sistema'::text) = 'NACIONAL_BRASIL'::text)
          ORDER BY ((historico_1.value ->> 'nu_ano_avaliacao'::text))::integer DESC
         LIMIT 1) historico ON (true))
     LEFT JOIN LATERAL ( SELECT array_to_string(array_agg(DISTINCT (ameacas_1.value ->> 'tx_ameaca_trilha'::text)), ', '::text) AS no_ameacas,
            array_to_string(array_agg(DISTINCT (ameacas_1.value ->> 'sq_criterio_ameaca_iucn'::text)), '|'::text) AS sq_ameacas
           FROM jsonb_array_elements((((ficha_versao.js_ficha -> 'ameacas'::text) ->> 'vetores'::text))::jsonb) ameacas_1(value)) ameacas ON (true))
     LEFT JOIN LATERAL ( SELECT array_to_string(array_agg(DISTINCT (usos_1.value ->> 'tx_uso_trilha'::text)), ', '::text) AS no_usos,
            array_to_string(array_agg(DISTINCT (usos_1.value ->> 'sq_uso'::text)), '|'::text) AS sq_usos
           FROM jsonb_array_elements((((ficha_versao.js_ficha -> 'usos'::text) ->> 'usos'::text))::jsonb) usos_1(value)) usos ON (true))
     LEFT JOIN LATERAL ( SELECT array_to_string(array_agg(DISTINCT (acoes_1.value ->> 'ds_acao_conservacao'::text)), ', '::text) AS no_acoes_conservacao,
            array_to_string(array_agg(DISTINCT (acoes_1.value ->> 'sq_acao_conservacao'::text)), '|'::text) AS sq_acoes_conservacao,
            array_to_string(array_agg(DISTINCT
                CASE
                    WHEN (((acoes_1.value ->> 'sq_plano_acao'::text))::bigint IS NOT NULL) THEN (acoes_1.value ->> 'tx_plano_acao'::text)
                    ELSE NULL::text
                END), ', '::text) AS tx_planos_acao,
            array_to_string(array_agg(DISTINCT
                CASE
                    WHEN (((acoes_1.value ->> 'sq_plano_acao'::text))::bigint IS NOT NULL) THEN ((acoes_1.value ->> 'sq_plano_acao'::text))::bigint
                    ELSE NULL::bigint
                END), '|'::text) AS sq_planos_acao
           FROM jsonb_array_elements((((ficha_versao.js_ficha -> 'acoes_conservacao'::text) ->> 'acoes_conservacao'::text))::jsonb) acoes_1(value)) acoes ON (true))
  WITH NO DATA;


--
-- TOC entry 217 (class 1259 OID 17990)
-- Name: pais; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.pais (
    sq_pais integer NOT NULL,
    no_pais character varying(50) NOT NULL,
    co_pais character varying(3),
    ge_pais public.geometry,
    dt_inclusao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    dt_alteracao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    CONSTRAINT enforce_dims_the_geom CHECK ((public.st_ndims(ge_pais) = 2)),
    CONSTRAINT enforce_geotype_the_geom CHECK (((public.geometrytype(ge_pais) = 'MULTIPOLYGON'::text) OR (ge_pais IS NULL))),
    CONSTRAINT enforce_srid_the_geom CHECK ((public.st_srid(ge_pais) = 4674))
);


--
-- TOC entry 280 (class 1259 OID 23381)
-- Name: plano_acao; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.plano_acao (
    sq_plano_acao integer NOT NULL,
    sg_plano_acao text,
    tx_plano_acao text
);


--
-- TOC entry 456 (class 1259 OID 28067)
-- Name: registro_bioma; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.registro_bioma (
    sq_registro bigint NOT NULL,
    sq_bioma bigint NOT NULL,
    dt_inclusao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


--
-- TOC entry 8334 (class 0 OID 0)
-- Dependencies: 456
-- Name: TABLE registro_bioma; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON TABLE salve.registro_bioma IS 'tabela para registro do Bioma que o ponto intersecta';


--
-- TOC entry 8335 (class 0 OID 0)
-- Dependencies: 456
-- Name: COLUMN registro_bioma.sq_registro; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.registro_bioma.sq_registro IS 'chave estrangeira tabela Registro';


--
-- TOC entry 8336 (class 0 OID 0)
-- Dependencies: 456
-- Name: COLUMN registro_bioma.sq_bioma; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.registro_bioma.sq_bioma IS 'chave estrangeira tabela Bioma';


--
-- TOC entry 8337 (class 0 OID 0)
-- Dependencies: 456
-- Name: COLUMN registro_bioma.dt_inclusao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.registro_bioma.dt_inclusao IS 'data de inclusão no banco de dados';


--
-- TOC entry 457 (class 1259 OID 28095)
-- Name: registro_estado; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.registro_estado (
    sq_registro bigint NOT NULL,
    sq_estado bigint NOT NULL,
    dt_inclusao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


--
-- TOC entry 8338 (class 0 OID 0)
-- Dependencies: 457
-- Name: TABLE registro_estado; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON TABLE salve.registro_estado IS 'tabela para registro do Estado que o ponto intersecta';


--
-- TOC entry 8339 (class 0 OID 0)
-- Dependencies: 457
-- Name: COLUMN registro_estado.sq_registro; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.registro_estado.sq_registro IS 'chave estrangeira tabela Registro';


--
-- TOC entry 8340 (class 0 OID 0)
-- Dependencies: 457
-- Name: COLUMN registro_estado.sq_estado; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.registro_estado.sq_estado IS 'chave estrangeira tabela Estado';


--
-- TOC entry 8341 (class 0 OID 0)
-- Dependencies: 457
-- Name: COLUMN registro_estado.dt_inclusao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.registro_estado.dt_inclusao IS 'data de inclusão no banco de dados';


--
-- TOC entry 459 (class 1259 OID 28131)
-- Name: registro_municipio; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.registro_municipio (
    sq_registro bigint NOT NULL,
    sq_municipio bigint NOT NULL,
    dt_inclusao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


--
-- TOC entry 8342 (class 0 OID 0)
-- Dependencies: 459
-- Name: COLUMN registro_municipio.sq_registro; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.registro_municipio.sq_registro IS 'Chave estrangeira tabela Registro';


--
-- TOC entry 8343 (class 0 OID 0)
-- Dependencies: 459
-- Name: COLUMN registro_municipio.sq_municipio; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.registro_municipio.sq_municipio IS 'Chave estrangeira tabela Municipio';


--
-- TOC entry 8344 (class 0 OID 0)
-- Dependencies: 459
-- Name: COLUMN registro_municipio.dt_inclusao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.registro_municipio.dt_inclusao IS 'Data de inclusão no banco de dados';


--
-- TOC entry 460 (class 1259 OID 28153)
-- Name: registro_pais; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.registro_pais (
    sq_registro bigint NOT NULL,
    sq_pais bigint NOT NULL,
    dt_inclusao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


--
-- TOC entry 8345 (class 0 OID 0)
-- Dependencies: 460
-- Name: COLUMN registro_pais.sq_registro; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.registro_pais.sq_registro IS 'chave estrangeira tabela Registro';


--
-- TOC entry 8346 (class 0 OID 0)
-- Dependencies: 460
-- Name: COLUMN registro_pais.sq_pais; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.registro_pais.sq_pais IS 'chave estrangeira tabela Pais';


--
-- TOC entry 8347 (class 0 OID 0)
-- Dependencies: 460
-- Name: COLUMN registro_pais.dt_inclusao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.registro_pais.dt_inclusao IS 'data de inclusão no banco de dados';


--
-- TOC entry 461 (class 1259 OID 28175)
-- Name: registro_rppn; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.registro_rppn (
    sq_registro bigint NOT NULL,
    sq_rppn bigint NOT NULL,
    dt_inclusao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


--
-- TOC entry 8348 (class 0 OID 0)
-- Dependencies: 461
-- Name: TABLE registro_rppn; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON TABLE salve.registro_rppn IS 'Tabela para registro da RPPN que o ponto intersecta';


--
-- TOC entry 8349 (class 0 OID 0)
-- Dependencies: 461
-- Name: COLUMN registro_rppn.sq_registro; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.registro_rppn.sq_registro IS 'Chave primária sequencial';


--
-- TOC entry 8350 (class 0 OID 0)
-- Dependencies: 461
-- Name: COLUMN registro_rppn.sq_rppn; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.registro_rppn.sq_rppn IS 'Chave estrangeira tabela RPPN';


--
-- TOC entry 8351 (class 0 OID 0)
-- Dependencies: 461
-- Name: COLUMN registro_rppn.dt_inclusao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.registro_rppn.dt_inclusao IS 'Data de inclusão no banco de dados';


--
-- TOC entry 470 (class 1259 OID 28291)
-- Name: registro_uc_estadual; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.registro_uc_estadual (
    sq_registro bigint NOT NULL,
    sq_uc_estadual bigint NOT NULL,
    dt_inclusao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    sq_registro_uc_estadual bigint NOT NULL
);


--
-- TOC entry 8352 (class 0 OID 0)
-- Dependencies: 470
-- Name: TABLE registro_uc_estadual; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON TABLE salve.registro_uc_estadual IS 'tabela para registro da Unidade de Conservação Estadual que o ponto intersecta';


--
-- TOC entry 8353 (class 0 OID 0)
-- Dependencies: 470
-- Name: COLUMN registro_uc_estadual.sq_registro; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.registro_uc_estadual.sq_registro IS 'chave estrangeira tabela Registro';


--
-- TOC entry 8354 (class 0 OID 0)
-- Dependencies: 470
-- Name: COLUMN registro_uc_estadual.sq_uc_estadual; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.registro_uc_estadual.sq_uc_estadual IS 'chave estrangeira tabela MMA_uc_nao_federal ';


--
-- TOC entry 8355 (class 0 OID 0)
-- Dependencies: 470
-- Name: COLUMN registro_uc_estadual.dt_inclusao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.registro_uc_estadual.dt_inclusao IS 'data de inclusão no banco de dados';


--
-- TOC entry 472 (class 1259 OID 28319)
-- Name: registro_uc_federal; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.registro_uc_federal (
    sq_registro_uc_federal integer NOT NULL,
    sq_registro bigint NOT NULL,
    sq_uc_federal bigint NOT NULL,
    dt_inclusao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


--
-- TOC entry 8356 (class 0 OID 0)
-- Dependencies: 472
-- Name: TABLE registro_uc_federal; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON TABLE salve.registro_uc_federal IS 'Tabela para registro da(s) Unidade(s) de Conservação Federal que o ponto intersecta';


--
-- TOC entry 8357 (class 0 OID 0)
-- Dependencies: 472
-- Name: COLUMN registro_uc_federal.sq_registro_uc_federal; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.registro_uc_federal.sq_registro_uc_federal IS 'Chave primária sequencial';


--
-- TOC entry 8358 (class 0 OID 0)
-- Dependencies: 472
-- Name: COLUMN registro_uc_federal.sq_registro; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.registro_uc_federal.sq_registro IS 'Chave estrangeira tabela Registro';


--
-- TOC entry 8359 (class 0 OID 0)
-- Dependencies: 472
-- Name: COLUMN registro_uc_federal.sq_uc_federal; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.registro_uc_federal.sq_uc_federal IS 'Chave estrangeira tabela Unidade_org';


--
-- TOC entry 8360 (class 0 OID 0)
-- Dependencies: 472
-- Name: COLUMN registro_uc_federal.dt_inclusao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.registro_uc_federal.dt_inclusao IS 'Data de inclusão no banco de dados';


--
-- TOC entry 476 (class 1259 OID 28369)
-- Name: taxon_historico_avaliacao; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.taxon_historico_avaliacao (
    sq_taxon_historico_avaliacao integer NOT NULL,
    sq_taxon bigint NOT NULL,
    sq_tipo_avaliacao bigint NOT NULL,
    sq_abrangencia bigint,
    sq_categoria_iucn bigint,
    nu_ano_avaliacao smallint NOT NULL,
    de_criterio_avaliacao_iucn text,
    st_possivelmente_extinta character(1),
    tx_justificativa_avaliacao text,
    no_regiao_outra text,
    de_categoria_outra text
);


--
-- TOC entry 8361 (class 0 OID 0)
-- Dependencies: 476
-- Name: COLUMN taxon_historico_avaliacao.sq_taxon_historico_avaliacao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.taxon_historico_avaliacao.sq_taxon_historico_avaliacao IS 'chave primaria auto increment';


--
-- TOC entry 8362 (class 0 OID 0)
-- Dependencies: 476
-- Name: COLUMN taxon_historico_avaliacao.sq_taxon; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.taxon_historico_avaliacao.sq_taxon IS 'chave estrangeira da tabela taxonomia.taxon';


--
-- TOC entry 8363 (class 0 OID 0)
-- Dependencies: 476
-- Name: COLUMN taxon_historico_avaliacao.sq_tipo_avaliacao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.taxon_historico_avaliacao.sq_tipo_avaliacao IS 'chave estrangeira da tabela salve.dados_apoio para informar o tipo da avaliacao. Ex.Nacional, estadual, municipal etc';


--
-- TOC entry 8364 (class 0 OID 0)
-- Dependencies: 476
-- Name: COLUMN taxon_historico_avaliacao.sq_abrangencia; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.taxon_historico_avaliacao.sq_abrangencia IS 'chave estrangeira da tabela salve.dados_apoio para informar a abrangencia quando a avaliacao nao for nacional';


--
-- TOC entry 8365 (class 0 OID 0)
-- Dependencies: 476
-- Name: COLUMN taxon_historico_avaliacao.sq_categoria_iucn; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.taxon_historico_avaliacao.sq_categoria_iucn IS 'chave estrangeira da tabela salve.dados_apoio para informar a categoria iucn que foi avaliada';


--
-- TOC entry 8366 (class 0 OID 0)
-- Dependencies: 476
-- Name: COLUMN taxon_historico_avaliacao.nu_ano_avaliacao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.taxon_historico_avaliacao.nu_ano_avaliacao IS 'informar o ano da avaliacao';


--
-- TOC entry 8367 (class 0 OID 0)
-- Dependencies: 476
-- Name: COLUMN taxon_historico_avaliacao.de_criterio_avaliacao_iucn; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.taxon_historico_avaliacao.de_criterio_avaliacao_iucn IS 'informar o criterio da iucn recebido na avaliacao';


--
-- TOC entry 8368 (class 0 OID 0)
-- Dependencies: 476
-- Name: COLUMN taxon_historico_avaliacao.st_possivelmente_extinta; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.taxon_historico_avaliacao.st_possivelmente_extinta IS 'informar se o taxon pode estar ou não extinto';


--
-- TOC entry 8369 (class 0 OID 0)
-- Dependencies: 476
-- Name: COLUMN taxon_historico_avaliacao.tx_justificativa_avaliacao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.taxon_historico_avaliacao.tx_justificativa_avaliacao IS 'texto com a justificativa da avaliacao';


--
-- TOC entry 8370 (class 0 OID 0)
-- Dependencies: 476
-- Name: COLUMN taxon_historico_avaliacao.no_regiao_outra; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.taxon_historico_avaliacao.no_regiao_outra IS 'informar uma outra abrangencia no caso das avaliacoes regionais';


--
-- TOC entry 8371 (class 0 OID 0)
-- Dependencies: 476
-- Name: COLUMN taxon_historico_avaliacao.de_categoria_outra; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.taxon_historico_avaliacao.de_categoria_outra IS 'informar a categoria e criterio especificos quando a avaliacao for estadual';


--
-- TOC entry 324 (class 1259 OID 24864)
-- Name: publicacao; Type: TABLE; Schema: taxonomia; Owner: -
--

CREATE TABLE taxonomia.publicacao (
    sq_publicacao integer NOT NULL,
    sq_tipo_publicacao bigint,
    de_titulo text NOT NULL,
    no_autor text,
    nu_ano_publicacao smallint,
    no_revista_cientifica text,
    de_volume text,
    de_issue text,
    de_paginas text,
    de_ref_bibliografica text,
    in_lista_oficial_vigente boolean,
    de_titulo_livro text,
    de_editores text,
    no_editora text,
    no_cidade text,
    nu_edicao_livro text,
    de_url text,
    dt_acesso_url date,
    no_universidade text,
    de_doi text,
    de_issn text,
    de_isbn text,
    dt_publicacao date
);


--
-- TOC entry 8372 (class 0 OID 0)
-- Dependencies: 324
-- Name: COLUMN publicacao.in_lista_oficial_vigente; Type: COMMENT; Schema: taxonomia; Owner: -
--

COMMENT ON COLUMN taxonomia.publicacao.in_lista_oficial_vigente IS 'Este campo informará se é ou não uma lista oficial nacional vigente para alimentar as opções do campo SQ_REF_BIB_LISTA_VIGENTE da tabela especies.ficha';


--
-- TOC entry 8373 (class 0 OID 0)
-- Dependencies: 324
-- Name: COLUMN publicacao.dt_publicacao; Type: COMMENT; Schema: taxonomia; Owner: -
--

COMMENT ON COLUMN taxonomia.publicacao.dt_publicacao IS 'Data de publicação do documento';


--
-- TOC entry 527 (class 1259 OID 29671)
-- Name: mv_registros; Type: MATERIALIZED VIEW; Schema: salve; Owner: -
--

CREATE MATERIALIZED VIEW salve.mv_registros AS
 SELECT true AS bo_salve,
    fo.sq_ficha_ocorrencia AS id_ocorrencia,
    ficha.sq_ciclo_avaliacao,
    ficha.sq_ficha,
    ficha.sq_taxon,
    ficha.nm_cientifico,
    ficha.sq_categoria_iucn,
    ficha.sq_categoria_final,
    taxon.no_autor AS no_autor_taxon,
    ((taxon.json_trilha -> 'reino'::text) ->> 'no_taxon'::text) AS no_reino,
    ((taxon.json_trilha -> 'filo'::text) ->> 'no_taxon'::text) AS no_filo,
    ((taxon.json_trilha -> 'classe'::text) ->> 'no_taxon'::text) AS no_classe,
    ((taxon.json_trilha -> 'ordem'::text) ->> 'no_taxon'::text) AS no_ordem,
    ((taxon.json_trilha -> 'familia'::text) ->> 'no_taxon'::text) AS no_familia,
    ((taxon.json_trilha -> 'genero'::text) ->> 'no_taxon'::text) AS no_genero,
    ((taxon.json_trilha -> 'especie'::text) ->> 'no_taxon'::text) AS no_especie,
    ((taxon.json_trilha -> 'subespecie'::text) ->> 'no_taxon'::text) AS no_subespecie,
    categoria_anterior.ds_categoria_anterior,
        CASE
            WHEN (ficha.sq_categoria_iucn IS NOT NULL) THEN concat(cat_avaliada.ds_dados_apoio, ' (', cat_avaliada.cd_dados_apoio, ')')
            ELSE ''::text
        END AS ds_categoria_avaliada,
        CASE
            WHEN (ficha.sq_categoria_final IS NOT NULL) THEN concat(cat_validada.ds_dados_apoio, ' (', cat_validada.cd_dados_apoio, ')')
            ELSE ''::text
        END AS ds_categoria_validada,
    pans.ds_pan,
    public.st_x(fo.ge_ocorrencia) AS nu_longitude,
    public.st_y(fo.ge_ocorrencia) AS nu_latitude,
    fo.nu_altitude,
    salve.snd((ficha.st_endemica_brasil)::text) AS de_endemica_brasil,
    salve.snd((fo.in_sensivel)::text) AS ds_sensivel,
    fo.sq_situacao_avaliacao,
    situacao_avaliacao.ds_dados_apoio AS ds_situacao_avaliacao,
    situacao_avaliacao.cd_sistema AS cd_situacao_avaliacao,
    fo.tx_nao_utilizado_avaliacao AS tx_justificativa_nao_usado_avaliacao,
    motivo.ds_dados_apoio AS ds_motivo_nao_utilizado,
    salve.calc_carencia((fo.dt_inclusao)::date, prazo_carencia.cd_sistema) AS dt_carencia,
    prazo_carencia.ds_dados_apoio AS ds_prazo_carencia,
    datum.ds_dados_apoio AS no_datum,
    formato_original.ds_dados_apoio AS no_formato_original,
    precisao_coord.ds_dados_apoio AS no_precisao,
    referencia_aprox.ds_dados_apoio AS no_referencia_aproximacao,
    metodo_aprox.ds_dados_apoio AS no_metodologia_aproximacao,
    fo.tx_metodo_aproximacao AS tx_metodologia_aproximacao,
    ((taxon_citado.json_trilha -> 'especie'::text) ->> 'no_taxon'::text) AS no_taxon_citado,
    fo.in_presenca_atual,
    salve.snd((fo.in_presenca_atual)::text) AS ds_presenca_atual,
    tipo_registro.ds_dados_apoio AS no_tipo_registro,
    (fo.nu_dia_registro)::text AS nu_dia_registro,
    (fo.nu_mes_registro)::text AS nu_mes_registro,
    (fo.nu_ano_registro)::text AS nu_ano_registro,
    fo.hr_registro,
    (fo.nu_dia_registro_fim)::text AS nu_dia_registro_fim,
    (fo.nu_mes_registro_fim)::text AS nu_mes_registro_fim,
    (fo.nu_ano_registro_fim)::text AS nu_ano_registro_fim,
    salve.snd((fo.in_data_desconhecida)::text) AS ds_data_desconhecida,
    fo.no_localidade,
    salve.remover_html_tags(fo.tx_local) AS ds_caracteristica_localidade,
    uc_federal.no_uc_federal,
    uc_estadual.no_uc_estadual,
    initcap(rppn.no_pessoa) AS no_rppn,
    pais.no_pais,
    estado.no_estado,
    municipio.no_municipio,
    bioma.no_bioma,
    btrim(regexp_replace(habitat.ds_dados_apoio, '[0-9.]'::text, ''::text, 'g'::text)) AS no_ambiente,
    habitat.ds_dados_apoio AS no_habitat,
    fo.vl_elevacao_min,
    fo.vl_elevacao_max,
    fo.vl_profundidade_min,
    fo.vl_profundidade_max,
    fo.de_identificador,
    fo.dt_identificacao,
    qualificador.ds_dados_apoio AS no_identificacao_incerta,
    fo.tx_tombamento AS ds_tombamento,
    fo.tx_instituicao AS ds_instituicao_tombamento,
    compilador.no_pessoa AS no_compilador,
    fo.dt_compilacao,
    revisor.no_pessoa AS no_revisor,
    fo.dt_revisao,
    validador.no_pessoa AS no_validador,
    fo.dt_validacao,
    fo.in_sensivel,
    'salve'::text AS no_base_dados,
        CASE
            WHEN (fo.id_origem IS NULL) THEN concat('SALVE:', (fo.sq_ficha_ocorrencia)::text)
            ELSE (fo.id_origem)::text
        END AS id_origem,
    ''::text AS nu_autorizacao,
    salve.remover_html_tags(fo.tx_observacao) AS tx_observacao,
    refbibs.json_ref_bibs AS tx_ref_bib,
    NULL::text AS no_autor_registro,
    NULL::text AS no_projeto,
        CASE
            WHEN (fo.st_adicionado_apos_avaliacao = true) THEN 'Não'::text
            ELSE salve.snd((fo.in_utilizado_avaliacao)::text)
        END AS ds_utilizado_avaliacao,
    salve.fn_calc_situacao_registro((fo.in_utilizado_avaliacao)::text, fo.st_adicionado_apos_avaliacao) AS ds_situacao_registro,
    fo.in_utilizado_avaliacao
   FROM (((((((((((((((((((((((((((((((((((((salve.ficha_ocorrencia fo
     JOIN salve.ficha_ocorrencia_registro freg ON ((freg.sq_ficha_ocorrencia = fo.sq_ficha_ocorrencia)))
     JOIN salve.ficha ON ((ficha.sq_ficha = fo.sq_ficha)))
     JOIN salve.ciclo_avaliacao ciclo ON ((ciclo.sq_ciclo_avaliacao = ficha.sq_ciclo_avaliacao)))
     JOIN taxonomia.taxon ON ((taxon.sq_taxon = ficha.sq_taxon)))
     JOIN salve.dados_apoio situacao_avaliacao ON ((situacao_avaliacao.sq_dados_apoio = fo.sq_situacao_avaliacao)))
     LEFT JOIN salve.dados_apoio motivo ON ((motivo.sq_dados_apoio = fo.sq_motivo_nao_utilizado_avaliacao)))
     LEFT JOIN salve.dados_apoio cat_avaliada ON ((cat_avaliada.sq_dados_apoio = ficha.sq_categoria_iucn)))
     LEFT JOIN salve.dados_apoio cat_validada ON ((cat_validada.sq_dados_apoio = ficha.sq_categoria_final)))
     LEFT JOIN salve.dados_apoio tipo_registro ON ((tipo_registro.sq_dados_apoio = fo.sq_tipo_registro)))
     LEFT JOIN salve.dados_apoio prazo_carencia ON ((prazo_carencia.sq_dados_apoio = fo.sq_prazo_carencia)))
     LEFT JOIN salve.dados_apoio datum ON ((datum.sq_dados_apoio = fo.sq_datum)))
     LEFT JOIN salve.dados_apoio formato_original ON ((formato_original.sq_dados_apoio = fo.sq_formato_coord_original)))
     LEFT JOIN salve.dados_apoio precisao_coord ON ((precisao_coord.sq_dados_apoio = fo.sq_precisao_coordenada)))
     LEFT JOIN salve.dados_apoio referencia_aprox ON ((referencia_aprox.sq_dados_apoio = fo.sq_ref_aproximacao)))
     LEFT JOIN salve.dados_apoio metodo_aprox ON ((metodo_aprox.sq_dados_apoio = fo.sq_metodo_aproximacao)))
     LEFT JOIN taxonomia.taxon taxon_citado ON ((taxon_citado.sq_taxon = fo.sq_taxon_citado)))
     LEFT JOIN LATERAL ( SELECT h.sq_taxon,
            concat(cat_hist.ds_dados_apoio, ' (', cat_hist.cd_dados_apoio, ')') AS ds_categoria_anterior
           FROM (salve.taxon_historico_avaliacao h
             JOIN salve.dados_apoio cat_hist ON ((cat_hist.sq_dados_apoio = h.sq_categoria_iucn)))
          WHERE ((h.sq_taxon = ficha.sq_taxon) AND (h.sq_tipo_avaliacao = 311) AND (h.nu_ano_avaliacao <= (ciclo.nu_ano + 4)))
          ORDER BY h.nu_ano_avaliacao DESC
         OFFSET 0
         LIMIT 1) categoria_anterior ON (true))
     LEFT JOIN LATERAL ( SELECT acoes.sq_ficha,
            array_to_string(array_agg(concat(plano_acao.tx_plano_acao, ' (', apoio_acao_situacao.ds_dados_apoio, ')')), ', '::text) AS ds_pan
           FROM ((salve.ficha_acao_conservacao acoes
             JOIN salve.plano_acao ON ((plano_acao.sq_plano_acao = acoes.sq_plano_acao)))
             JOIN salve.dados_apoio apoio_acao_situacao ON ((apoio_acao_situacao.sq_dados_apoio = acoes.sq_situacao_acao_conservacao)))
          WHERE (acoes.sq_ficha = ficha.sq_ficha)
          GROUP BY acoes.sq_ficha) pans ON (true))
     LEFT JOIN salve.registro_rppn regrppn ON ((regrppn.sq_registro = freg.sq_registro)))
     LEFT JOIN salve.pessoa rppn ON ((rppn.sq_pessoa = regrppn.sq_rppn)))
     LEFT JOIN salve.registro_pais regpais ON ((regpais.sq_registro = freg.sq_registro)))
     LEFT JOIN salve.pais pais ON ((pais.sq_pais = regpais.sq_pais)))
     LEFT JOIN salve.registro_estado reguf ON ((reguf.sq_registro = freg.sq_registro)))
     LEFT JOIN salve.estado estado ON ((estado.sq_estado = reguf.sq_estado)))
     LEFT JOIN salve.registro_municipio regmun ON ((regmun.sq_registro = freg.sq_registro)))
     LEFT JOIN salve.municipio municipio ON ((municipio.sq_municipio = regmun.sq_municipio)))
     LEFT JOIN salve.dados_apoio habitat ON ((habitat.sq_dados_apoio = fo.sq_habitat)))
     LEFT JOIN salve.dados_apoio ambiente ON ((ambiente.sq_dados_apoio = habitat.sq_dados_apoio_pai)))
     LEFT JOIN salve.dados_apoio qualificador ON ((qualificador.sq_dados_apoio = fo.sq_qualificador_val_taxon)))
     LEFT JOIN salve.pessoa compilador ON ((compilador.sq_pessoa = fo.sq_pessoa_compilador)))
     LEFT JOIN salve.pessoa revisor ON ((revisor.sq_pessoa = fo.sq_pessoa_revisor)))
     LEFT JOIN salve.pessoa validador ON ((validador.sq_pessoa = fo.sq_pessoa_validador)))
     LEFT JOIN salve.registro_bioma regbio ON ((regbio.sq_registro = freg.sq_registro)))
     LEFT JOIN salve.bioma bioma ON ((bioma.sq_bioma = regbio.sq_bioma)))
     LEFT JOIN LATERAL ( SELECT (json_object_agg(ficha_ref_bib.sq_ficha_ref_bib, json_build_object('de_ref_bib', salve.entity2char(publicacao.de_ref_bibliografica), 'no_autores_publicacao', replace(publicacao.no_autor, '
'::text, '; '::text), 'nu_ano_publicacao', publicacao.nu_ano_publicacao, 'no_autor', ficha_ref_bib.no_autor, 'nu_ano', ficha_ref_bib.nu_ano)))::text AS json_ref_bibs
           FROM (salve.ficha_ref_bib
             LEFT JOIN taxonomia.publicacao ON ((publicacao.sq_publicacao = ficha_ref_bib.sq_publicacao)))
          WHERE (((ficha_ref_bib.no_tabela)::text = 'ficha_ocorrencia'::text) AND ((ficha_ref_bib.no_coluna)::text = 'sq_ficha_ocorrencia'::text) AND (ficha_ref_bib.sq_registro = fo.sq_ficha_ocorrencia))) refbibs ON (true))
     LEFT JOIN LATERAL ( SELECT array_to_string(array_agg(COALESCE(instituicao.sg_instituicao, ((ucf.no_pessoa)::character varying)::text)), ', '::text) AS no_uc_federal
           FROM ((salve.registro_uc_federal x
             JOIN salve.pessoa ucf ON ((ucf.sq_pessoa = x.sq_uc_federal)))
             JOIN salve.instituicao ON ((instituicao.sq_pessoa = x.sq_uc_federal)))
          WHERE (x.sq_registro = freg.sq_registro)) uc_federal ON (true))
     LEFT JOIN LATERAL ( SELECT array_to_string(array_agg(uce.no_pessoa), ', '::text) AS no_uc_estadual
           FROM (salve.registro_uc_estadual x
             JOIN salve.pessoa uce ON ((uce.sq_pessoa = x.sq_uc_estadual)))
          WHERE (x.sq_registro = freg.sq_registro)) uc_estadual ON (true))
UNION ALL
 SELECT false AS bo_salve,
    oc.sq_ficha_ocorrencia_portalbio AS id_ocorrencia,
    ficha.sq_ciclo_avaliacao,
    ficha.sq_ficha,
    ficha.sq_taxon,
    ficha.nm_cientifico,
    ficha.sq_categoria_iucn,
    ficha.sq_categoria_final,
    taxon.no_autor AS no_autor_taxon,
    ((taxon.json_trilha -> 'reino'::text) ->> 'no_taxon'::text) AS no_reino,
    ((taxon.json_trilha -> 'filo'::text) ->> 'no_taxon'::text) AS no_filo,
    ((taxon.json_trilha -> 'classe'::text) ->> 'no_taxon'::text) AS no_classe,
    ((taxon.json_trilha -> 'ordem'::text) ->> 'no_taxon'::text) AS no_ordem,
    ((taxon.json_trilha -> 'familia'::text) ->> 'no_taxon'::text) AS no_familia,
    ((taxon.json_trilha -> 'genero'::text) ->> 'no_taxon'::text) AS no_genero,
    ((taxon.json_trilha -> 'especie'::text) ->> 'no_taxon'::text) AS no_especie,
    ((taxon.json_trilha -> 'subespecie'::text) ->> 'no_taxon'::text) AS no_subespecie,
    categoria_anterior.ds_categoria_anterior,
        CASE
            WHEN (ficha.sq_categoria_iucn IS NOT NULL) THEN concat(cat_avaliada.ds_dados_apoio, ' (', cat_avaliada.cd_dados_apoio, ')')
            ELSE ''::text
        END AS ds_categoria_avaliada,
        CASE
            WHEN (ficha.sq_categoria_final IS NOT NULL) THEN concat(cat_validada.ds_dados_apoio, ' (', cat_validada.cd_dados_apoio, ')')
            ELSE ''::text
        END AS ds_categoria_validada,
    pans.ds_pan,
    public.st_x(oc.ge_ocorrencia) AS nu_longitude,
    public.st_y(oc.ge_ocorrencia) AS nu_latitude,
    NULL::integer AS nu_altitude,
    salve.snd((ficha.st_endemica_brasil)::text) AS de_endemica_brasil,
    NULL::text AS ds_sensivel,
    oc.sq_situacao_avaliacao,
    situacao_avaliacao.ds_dados_apoio AS ds_situacao_avaliacao,
    situacao_avaliacao.cd_sistema AS cd_situacao_avaliacao,
    oc.tx_nao_utilizado_avaliacao AS tx_justificativa_nao_usado_avaliacao,
    motivo.ds_dados_apoio AS ds_motivo_nao_utilizado,
    oc.dt_carencia,
    NULL::text AS ds_prazo_carencia,
    NULL::text AS no_datum,
    NULL::text AS no_formato_original,
        CASE
            WHEN ("left"(oc.tx_ocorrencia, 1) = '{'::text) THEN ((oc.tx_ocorrencia)::json ->> 'precisaoCoord'::text)
            ELSE ''::text
        END AS no_precisao,
    NULL::text AS no_referencia_aproximacao,
    NULL::text AS no_metodologia_aproximacao,
    NULL::text AS tx_metodologia_aproximacao,
    NULL::text AS no_taxon_citado,
    oc.in_presenca_atual,
    salve.snd((oc.in_presenca_atual)::text) AS ds_presenca_atual,
    NULL::text AS no_tipo_registro,
    to_char((oc.dt_ocorrencia)::timestamp with time zone, 'DD'::text) AS nu_dia_registro,
    to_char((oc.dt_ocorrencia)::timestamp with time zone, 'MM'::text) AS nu_mes_registro,
    to_char((oc.dt_ocorrencia)::timestamp with time zone, 'yyyy'::text) AS nu_ano_registro,
    NULL::text AS hr_registro,
    NULL::text AS nu_dia_registro_fim,
    NULL::text AS nu_mes_registro_fim,
    NULL::text AS nu_ano_registro_fim,
    salve.snd((oc.in_data_desconhecida)::text) AS ds_data_desconhecida,
    oc.no_local AS no_localidade,
    NULL::text AS ds_caracteristica_localidade,
    uc_federal.no_uc_federal,
    uc_estadual.no_uc_estadual,
    initcap(rppn.no_pessoa) AS no_rppn,
    pais.no_pais,
    estado.no_estado,
    municipio.no_municipio,
    bioma.no_bioma,
    NULL::text AS no_ambiente,
    NULL::text AS no_habitat,
    NULL::numeric AS vl_elevacao_min,
    NULL::numeric AS vl_elevacao_max,
    NULL::numeric AS vl_profundidade_min,
    NULL::numeric AS vl_profundidade_max,
    NULL::text AS de_identificador,
    NULL::date AS dt_identificacao,
    NULL::text AS no_identificacao_incerta,
    NULL::text AS ds_tombamento,
    NULL::text AS ds_instituicao_tombamento,
    NULL::text AS no_compilador,
    NULL::date AS dt_compilacao,
    revisor.no_pessoa AS no_revisor,
    oc.dt_revisao,
    validador.no_pessoa AS no_validador,
    oc.dt_validacao,
    NULL::bpchar AS in_sensivel,
    salve.calc_base_dados_portalbio(oc.tx_ocorrencia, oc.de_uuid) AS no_base_dados,
        CASE
            WHEN (oc.id_origem IS NULL) THEN
            CASE
                WHEN ("left"(oc.tx_ocorrencia, 1) = '{'::text) THEN ((oc.tx_ocorrencia)::json ->> 'uuid'::text)
                ELSE oc.de_uuid
            END
            ELSE (oc.id_origem)::text
        END AS id_origem,
        CASE
            WHEN ("left"(oc.tx_ocorrencia, 1) = '{'::text) THEN ((oc.tx_ocorrencia)::json ->> 'recordNumber'::text)
            ELSE ''::text
        END AS nu_autorizacao,
    NULL::text AS tx_observacao,
    refbibs.json_ref_bibs AS tx_ref_bib,
    replace(
        CASE
            WHEN (oc.no_autor IS NULL) THEN
            CASE
                WHEN ("left"(oc.tx_ocorrencia, 1) = '{'::text) THEN
                CASE
                    WHEN (((oc.tx_ocorrencia)::json ->> 'autor'::text) IS NULL) THEN ((oc.tx_ocorrencia)::json ->> 'collector'::text)
                    ELSE ((oc.tx_ocorrencia)::json ->> 'autor'::text)
                END
                ELSE ''::text
            END
            ELSE oc.no_autor
        END, '
'::text, '; '::text) AS no_autor_registro,
    ((oc.tx_ocorrencia)::jsonb ->> 'projeto'::text) AS no_projeto,
        CASE
            WHEN (oc.st_adicionado_apos_avaliacao = true) THEN 'Não'::text
            ELSE salve.snd((oc.in_utilizado_avaliacao)::text)
        END AS ds_utilizado_avaliacao,
    salve.fn_calc_situacao_registro((oc.in_utilizado_avaliacao)::text, oc.st_adicionado_apos_avaliacao) AS ds_situacao_registro,
    oc.in_utilizado_avaliacao
   FROM (((((((((((((((((((((((((salve.ficha_ocorrencia_portalbio oc
     JOIN salve.ficha ON ((ficha.sq_ficha = oc.sq_ficha)))
     JOIN salve.ciclo_avaliacao ciclo ON ((ciclo.sq_ciclo_avaliacao = ficha.sq_ciclo_avaliacao)))
     JOIN taxonomia.taxon ON ((taxon.sq_taxon = ficha.sq_taxon)))
     JOIN salve.dados_apoio situacao_avaliacao ON ((situacao_avaliacao.sq_dados_apoio = oc.sq_situacao_avaliacao)))
     LEFT JOIN salve.dados_apoio motivo ON ((motivo.sq_dados_apoio = oc.sq_motivo_nao_utilizado_avaliacao)))
     LEFT JOIN salve.dados_apoio cat_avaliada ON ((cat_avaliada.sq_dados_apoio = ficha.sq_categoria_iucn)))
     LEFT JOIN salve.dados_apoio cat_validada ON ((cat_validada.sq_dados_apoio = ficha.sq_categoria_final)))
     LEFT JOIN LATERAL ( SELECT h.sq_taxon,
            concat(cat_hist.ds_dados_apoio, ' (', cat_hist.cd_dados_apoio, ')') AS ds_categoria_anterior
           FROM (salve.taxon_historico_avaliacao h
             JOIN salve.dados_apoio cat_hist ON ((cat_hist.sq_dados_apoio = h.sq_categoria_iucn)))
          WHERE ((h.sq_taxon = ficha.sq_taxon) AND (h.sq_tipo_avaliacao = 311) AND (h.nu_ano_avaliacao <= (ciclo.nu_ano + 4)))
          ORDER BY h.nu_ano_avaliacao DESC
         OFFSET 0
         LIMIT 1) categoria_anterior ON (true))
     LEFT JOIN LATERAL ( SELECT acoes.sq_ficha,
            array_to_string(array_agg(concat(plano_acao.tx_plano_acao, ' (', apoio_acao_situacao.ds_dados_apoio, ')')), ', '::text) AS ds_pan
           FROM ((salve.ficha_acao_conservacao acoes
             JOIN salve.plano_acao ON ((plano_acao.sq_plano_acao = acoes.sq_plano_acao)))
             JOIN salve.dados_apoio apoio_acao_situacao ON ((apoio_acao_situacao.sq_dados_apoio = acoes.sq_situacao_acao_conservacao)))
          WHERE (acoes.sq_ficha = ficha.sq_ficha)
          GROUP BY acoes.sq_ficha) pans ON (true))
     LEFT JOIN salve.ficha_ocorrencia_portalbio_reg freg ON ((freg.sq_ficha_ocorrencia_portalbio = oc.sq_ficha_ocorrencia_portalbio)))
     LEFT JOIN salve.registro_rppn regrppn ON ((regrppn.sq_registro = freg.sq_registro)))
     LEFT JOIN salve.pessoa rppn ON ((rppn.sq_pessoa = regrppn.sq_rppn)))
     LEFT JOIN salve.registro_pais regpais ON ((regpais.sq_registro = freg.sq_registro)))
     LEFT JOIN salve.pais pais ON ((pais.sq_pais = regpais.sq_pais)))
     LEFT JOIN salve.registro_estado reguf ON ((reguf.sq_registro = freg.sq_registro)))
     LEFT JOIN salve.estado estado ON ((estado.sq_estado = reguf.sq_estado)))
     LEFT JOIN salve.registro_municipio regmun ON ((regmun.sq_registro = freg.sq_registro)))
     LEFT JOIN salve.municipio municipio ON ((municipio.sq_municipio = regmun.sq_municipio)))
     LEFT JOIN salve.pessoa revisor ON ((revisor.sq_pessoa = oc.sq_pessoa_revisor)))
     LEFT JOIN salve.pessoa validador ON ((validador.sq_pessoa = oc.sq_pessoa_validador)))
     LEFT JOIN salve.registro_bioma regbio ON ((regbio.sq_registro = freg.sq_registro)))
     LEFT JOIN salve.bioma ON ((bioma.sq_bioma = regbio.sq_bioma)))
     LEFT JOIN LATERAL ( SELECT (json_object_agg(ficha_ref_bib.sq_ficha_ref_bib, json_build_object('de_ref_bib', salve.entity2char(publicacao.de_ref_bibliografica), 'no_autores_publicacao', replace(publicacao.no_autor, '
'::text, '; '::text), 'nu_ano_publicacao', publicacao.nu_ano_publicacao, 'no_autor', ficha_ref_bib.no_autor, 'nu_ano', ficha_ref_bib.nu_ano)))::text AS json_ref_bibs
           FROM (salve.ficha_ref_bib
             LEFT JOIN taxonomia.publicacao ON ((publicacao.sq_publicacao = ficha_ref_bib.sq_publicacao)))
          WHERE (((ficha_ref_bib.no_tabela)::text = 'ficha_ocorrencia_portalbio'::text) AND ((ficha_ref_bib.no_coluna)::text = 'sq_ficha_ocorrencia_portalbio'::text) AND (ficha_ref_bib.sq_registro = oc.sq_ficha_ocorrencia_portalbio))) refbibs ON (true))
     LEFT JOIN LATERAL ( SELECT array_to_string(array_agg(COALESCE(instituicao.sg_instituicao, ((ucf.no_pessoa)::character varying)::text)), ', '::text) AS no_uc_federal
           FROM ((salve.registro_uc_federal x
             JOIN salve.pessoa ucf ON ((ucf.sq_pessoa = x.sq_uc_federal)))
             JOIN salve.instituicao ON ((instituicao.sq_pessoa = x.sq_uc_federal)))
          WHERE (x.sq_registro = freg.sq_registro)) uc_federal ON (true))
     LEFT JOIN LATERAL ( SELECT array_to_string(array_agg(uce.no_pessoa), ', '::text) AS no_uc_estadual
           FROM (salve.registro_uc_estadual x
             JOIN salve.pessoa uce ON ((uce.sq_pessoa = x.sq_uc_estadual)))
          WHERE (x.sq_registro = freg.sq_registro)) uc_estadual ON (true))
  WITH NO DATA;


--
-- TOC entry 518 (class 1259 OID 29497)
-- Name: ocorrencias_json; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.ocorrencias_json (
    sq_ocorrencias_json bigint NOT NULL,
    sq_ficha_versao bigint NOT NULL,
    js_ocorrencias jsonb NOT NULL
);


--
-- TOC entry 8374 (class 0 OID 0)
-- Dependencies: 518
-- Name: COLUMN ocorrencias_json.sq_ocorrencias_json; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ocorrencias_json.sq_ocorrencias_json IS 'chave primaria sequencial da tabela ocorrencias_json';


--
-- TOC entry 8375 (class 0 OID 0)
-- Dependencies: 518
-- Name: COLUMN ocorrencias_json.sq_ficha_versao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ocorrencias_json.sq_ficha_versao IS 'chave estrangeira da tabela ficha_versao';


--
-- TOC entry 8376 (class 0 OID 0)
-- Dependencies: 518
-- Name: COLUMN ocorrencias_json.js_ocorrencias; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.ocorrencias_json.js_ocorrencias IS 'Armazenar copia completa dos registros de ocorrencias da ficha no formato jsonb';


--
-- TOC entry 517 (class 1259 OID 29495)
-- Name: ocorrencias_json_sq_ocorrencias_json_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.ocorrencias_json_sq_ocorrencias_json_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8377 (class 0 OID 0)
-- Dependencies: 517
-- Name: ocorrencias_json_sq_ocorrencias_json_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.ocorrencias_json_sq_ocorrencias_json_seq OWNED BY salve.ocorrencias_json.sq_ocorrencias_json;


--
-- TOC entry 429 (class 1259 OID 26725)
-- Name: oficina_anexo; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.oficina_anexo (
    sq_oficina_anexo integer NOT NULL,
    sq_oficina bigint NOT NULL,
    sq_pessoa bigint NOT NULL,
    no_arquivo text NOT NULL,
    de_tipo_conteudo text NOT NULL,
    de_local_arquivo text NOT NULL,
    de_legenda text,
    dt_inclusao timestamp with time zone DEFAULT now() NOT NULL,
    in_doc_final boolean DEFAULT false
);


--
-- TOC entry 8378 (class 0 OID 0)
-- Dependencies: 429
-- Name: TABLE oficina_anexo; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON TABLE salve.oficina_anexo IS 'Anexos da oficina, docs, fotos';


--
-- TOC entry 8379 (class 0 OID 0)
-- Dependencies: 429
-- Name: COLUMN oficina_anexo.in_doc_final; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.oficina_anexo.in_doc_final IS 'Informar se o documento anexo é o documento final da oficina ou não';


--
-- TOC entry 433 (class 1259 OID 26927)
-- Name: oficina_anexo_assinatura; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.oficina_anexo_assinatura (
    sq_oficina_anexo_assinatura integer NOT NULL,
    sq_oficina_anexo bigint,
    sq_oficina_participante bigint,
    de_hash text,
    dt_assinatura timestamp without time zone,
    dt_email timestamp without time zone,
    tx_observacao text
);


--
-- TOC entry 8380 (class 0 OID 0)
-- Dependencies: 433
-- Name: TABLE oficina_anexo_assinatura; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON TABLE salve.oficina_anexo_assinatura IS 'Tabela para registrar a assinatura eletronica dos participantes da oficina';


--
-- TOC entry 8381 (class 0 OID 0)
-- Dependencies: 433
-- Name: COLUMN oficina_anexo_assinatura.sq_oficina_anexo_assinatura; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.oficina_anexo_assinatura.sq_oficina_anexo_assinatura IS 'Chave primaria auto incrementada';


--
-- TOC entry 8382 (class 0 OID 0)
-- Dependencies: 433
-- Name: COLUMN oficina_anexo_assinatura.sq_oficina_anexo; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.oficina_anexo_assinatura.sq_oficina_anexo IS 'Chave estrangeira da tabela oficina_anexo';


--
-- TOC entry 8383 (class 0 OID 0)
-- Dependencies: 433
-- Name: COLUMN oficina_anexo_assinatura.sq_oficina_participante; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.oficina_anexo_assinatura.sq_oficina_participante IS 'Chave estrangeira da tabela oficina_participante';


--
-- TOC entry 8384 (class 0 OID 0)
-- Dependencies: 433
-- Name: COLUMN oficina_anexo_assinatura.de_hash; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.oficina_anexo_assinatura.de_hash IS 'Hash para confirmacao da assinatura via link enviado por email';


--
-- TOC entry 8385 (class 0 OID 0)
-- Dependencies: 433
-- Name: COLUMN oficina_anexo_assinatura.dt_assinatura; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.oficina_anexo_assinatura.dt_assinatura IS 'Data que o participante assinou o documento eletronicamente.';


--
-- TOC entry 8386 (class 0 OID 0)
-- Dependencies: 433
-- Name: COLUMN oficina_anexo_assinatura.dt_email; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.oficina_anexo_assinatura.dt_email IS 'Data do envio do email para os participante com o link da assinatura eletronica.';


--
-- TOC entry 8387 (class 0 OID 0)
-- Dependencies: 433
-- Name: COLUMN oficina_anexo_assinatura.tx_observacao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.oficina_anexo_assinatura.tx_observacao IS 'Campo para registro de observacoes sobre a assinatura eletronica';


--
-- TOC entry 432 (class 1259 OID 26925)
-- Name: oficina_anexo_assinatura_sq_oficina_anexo_assinatura_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.oficina_anexo_assinatura_sq_oficina_anexo_assinatura_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8388 (class 0 OID 0)
-- Dependencies: 432
-- Name: oficina_anexo_assinatura_sq_oficina_anexo_assinatura_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.oficina_anexo_assinatura_sq_oficina_anexo_assinatura_seq OWNED BY salve.oficina_anexo_assinatura.sq_oficina_anexo_assinatura;


--
-- TOC entry 428 (class 1259 OID 26723)
-- Name: oficina_anexo_sq_oficina_anexo_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.oficina_anexo_sq_oficina_anexo_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8389 (class 0 OID 0)
-- Dependencies: 428
-- Name: oficina_anexo_sq_oficina_anexo_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.oficina_anexo_sq_oficina_anexo_seq OWNED BY salve.oficina_anexo.sq_oficina_anexo;


--
-- TOC entry 437 (class 1259 OID 27072)
-- Name: oficina_ficha_chat; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.oficina_ficha_chat (
    sq_oficina_ficha_chat integer NOT NULL,
    sq_oficina_ficha bigint NOT NULL,
    sq_pessoa bigint NOT NULL,
    dt_mensagem timestamp without time zone NOT NULL,
    de_mensagem text NOT NULL,
    sg_perfil text NOT NULL
);


--
-- TOC entry 8390 (class 0 OID 0)
-- Dependencies: 437
-- Name: TABLE oficina_ficha_chat; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON TABLE salve.oficina_ficha_chat IS 'Tabela para registro do bate-papo entre os validadores, ponto focal e coordenador de taxon durante a validação';


--
-- TOC entry 8391 (class 0 OID 0)
-- Dependencies: 437
-- Name: COLUMN oficina_ficha_chat.sg_perfil; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.oficina_ficha_chat.sg_perfil IS 'Gravar a sigla do perfil que o usuario estava logado ao registrar a conversa.';


--
-- TOC entry 436 (class 1259 OID 27070)
-- Name: oficina_ficha_chat_sq_oficina_ficha_chat_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.oficina_ficha_chat_sq_oficina_ficha_chat_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8392 (class 0 OID 0)
-- Dependencies: 436
-- Name: oficina_ficha_chat_sq_oficina_ficha_chat_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.oficina_ficha_chat_sq_oficina_ficha_chat_seq OWNED BY salve.oficina_ficha_chat.sq_oficina_ficha_chat;


--
-- TOC entry 439 (class 1259 OID 27097)
-- Name: oficina_ficha_participante; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.oficina_ficha_participante (
    sq_oficina_ficha_participante integer NOT NULL,
    sq_oficina_ficha bigint NOT NULL,
    sq_oficina_participante bigint NOT NULL,
    sq_papel_participante bigint NOT NULL
);


--
-- TOC entry 438 (class 1259 OID 27095)
-- Name: oficina_ficha_participante_sq_oficina_ficha_participante_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.oficina_ficha_participante_sq_oficina_ficha_participante_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8393 (class 0 OID 0)
-- Dependencies: 438
-- Name: oficina_ficha_participante_sq_oficina_ficha_participante_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.oficina_ficha_participante_sq_oficina_ficha_participante_seq OWNED BY salve.oficina_ficha_participante.sq_oficina_ficha_participante;


--
-- TOC entry 434 (class 1259 OID 27026)
-- Name: oficina_ficha_sq_oficina_ficha_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.oficina_ficha_sq_oficina_ficha_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8394 (class 0 OID 0)
-- Dependencies: 434
-- Name: oficina_ficha_sq_oficina_ficha_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.oficina_ficha_sq_oficina_ficha_seq OWNED BY salve.oficina_ficha.sq_oficina_ficha;


--
-- TOC entry 520 (class 1259 OID 29528)
-- Name: oficina_ficha_versao; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.oficina_ficha_versao (
    sq_oficina_ficha integer NOT NULL,
    sq_ficha_versao integer NOT NULL
);


--
-- TOC entry 8395 (class 0 OID 0)
-- Dependencies: 520
-- Name: COLUMN oficina_ficha_versao.sq_oficina_ficha; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.oficina_ficha_versao.sq_oficina_ficha IS 'Chave estrangeira e primária recebida da tabela oficina_ficha';


--
-- TOC entry 8396 (class 0 OID 0)
-- Dependencies: 520
-- Name: COLUMN oficina_ficha_versao.sq_ficha_versao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.oficina_ficha_versao.sq_ficha_versao IS 'Chave estrangeira da tabela ficha_versao';


--
-- TOC entry 441 (class 1259 OID 27127)
-- Name: oficina_memoria; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.oficina_memoria (
    sq_oficina_memoria integer NOT NULL,
    sq_oficina bigint NOT NULL,
    tx_memoria text,
    dt_inicio_oficina date NOT NULL,
    dt_fim_oficina date NOT NULL,
    de_local_oficina text,
    de_salas text
);


--
-- TOC entry 8397 (class 0 OID 0)
-- Dependencies: 441
-- Name: TABLE oficina_memoria; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON TABLE salve.oficina_memoria IS 'Tabela para registro da memória da reunião preparatória';


--
-- TOC entry 8398 (class 0 OID 0)
-- Dependencies: 441
-- Name: COLUMN oficina_memoria.dt_inicio_oficina; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.oficina_memoria.dt_inicio_oficina IS 'Data proposta para inicio da oficina de validacao';


--
-- TOC entry 8399 (class 0 OID 0)
-- Dependencies: 441
-- Name: COLUMN oficina_memoria.dt_fim_oficina; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.oficina_memoria.dt_fim_oficina IS 'Data proposta para finalizar a oficina de validacao';


--
-- TOC entry 8400 (class 0 OID 0)
-- Dependencies: 441
-- Name: COLUMN oficina_memoria.de_local_oficina; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.oficina_memoria.de_local_oficina IS 'Registrar o local de realizaÃ§Ã£o da oficina';


--
-- TOC entry 8401 (class 0 OID 0)
-- Dependencies: 441
-- Name: COLUMN oficina_memoria.de_salas; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.oficina_memoria.de_salas IS 'Número de salas reservadas para a realizaÃ§Ã£o da oficina';


--
-- TOC entry 444 (class 1259 OID 27863)
-- Name: oficina_memoria_anexo; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.oficina_memoria_anexo (
    sq_oficina_memoria_anexo integer NOT NULL,
    sq_oficina_memoria bigint NOT NULL,
    sq_pessoa bigint NOT NULL,
    no_arquivo text NOT NULL,
    de_tipo_conteudo text NOT NULL,
    de_local_arquivo text NOT NULL,
    de_legenda text,
    dt_inclusao timestamp with time zone DEFAULT now() NOT NULL
);


--
-- TOC entry 8402 (class 0 OID 0)
-- Dependencies: 444
-- Name: TABLE oficina_memoria_anexo; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON TABLE salve.oficina_memoria_anexo IS 'Tabela para armazenamento dos anexos da memória da reunião preparatória.';


--
-- TOC entry 443 (class 1259 OID 27861)
-- Name: oficina_memoria_anexo_sq_oficina_memoria_anexo_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.oficina_memoria_anexo_sq_oficina_memoria_anexo_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8403 (class 0 OID 0)
-- Dependencies: 443
-- Name: oficina_memoria_anexo_sq_oficina_memoria_anexo_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.oficina_memoria_anexo_sq_oficina_memoria_anexo_seq OWNED BY salve.oficina_memoria_anexo.sq_oficina_memoria_anexo;


--
-- TOC entry 446 (class 1259 OID 27898)
-- Name: oficina_memoria_participante; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.oficina_memoria_participante (
    sq_oficina_memoria_participante integer NOT NULL,
    sq_oficina_memoria bigint NOT NULL,
    sq_papel bigint NOT NULL,
    no_participante text NOT NULL
);


--
-- TOC entry 8404 (class 0 OID 0)
-- Dependencies: 446
-- Name: TABLE oficina_memoria_participante; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON TABLE salve.oficina_memoria_participante IS 'Tabela para armazenamento dos participantes da memoria da reunião preparatória.';


--
-- TOC entry 445 (class 1259 OID 27896)
-- Name: oficina_memoria_participante_sq_oficina_memoria_participant_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.oficina_memoria_participante_sq_oficina_memoria_participant_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8405 (class 0 OID 0)
-- Dependencies: 445
-- Name: oficina_memoria_participante_sq_oficina_memoria_participant_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.oficina_memoria_participante_sq_oficina_memoria_participant_seq OWNED BY salve.oficina_memoria_participante.sq_oficina_memoria_participante;


--
-- TOC entry 440 (class 1259 OID 27125)
-- Name: oficina_memoria_sq_oficina_memoria_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.oficina_memoria_sq_oficina_memoria_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8406 (class 0 OID 0)
-- Dependencies: 440
-- Name: oficina_memoria_sq_oficina_memoria_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.oficina_memoria_sq_oficina_memoria_seq OWNED BY salve.oficina_memoria.sq_oficina_memoria;


--
-- TOC entry 431 (class 1259 OID 26893)
-- Name: oficina_participante; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.oficina_participante (
    sq_oficina_participante integer NOT NULL,
    sq_oficina bigint NOT NULL,
    sq_pessoa bigint NOT NULL,
    sq_situacao bigint,
    sq_instituicao bigint NOT NULL,
    de_email text NOT NULL,
    dt_email timestamp without time zone,
    de_assunto_email text
);


--
-- TOC entry 430 (class 1259 OID 26891)
-- Name: oficina_participante_sq_oficina_participante_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.oficina_participante_sq_oficina_participante_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8407 (class 0 OID 0)
-- Dependencies: 430
-- Name: oficina_participante_sq_oficina_participante_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.oficina_participante_sq_oficina_participante_seq OWNED BY salve.oficina_participante.sq_oficina_participante;


--
-- TOC entry 255 (class 1259 OID 22830)
-- Name: oficina_sq_oficina_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.oficina_sq_oficina_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8408 (class 0 OID 0)
-- Dependencies: 255
-- Name: oficina_sq_oficina_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.oficina_sq_oficina_seq OWNED BY salve.oficina.sq_oficina;


--
-- TOC entry 216 (class 1259 OID 17988)
-- Name: pais_sq_pais_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.pais_sq_pais_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8409 (class 0 OID 0)
-- Dependencies: 216
-- Name: pais_sq_pais_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.pais_sq_pais_seq OWNED BY salve.pais.sq_pais;


--
-- TOC entry 234 (class 1259 OID 18212)
-- Name: perfil; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.perfil (
    sq_perfil integer NOT NULL,
    no_perfil text NOT NULL,
    cd_sistema text NOT NULL,
    dt_inclusao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    dt_alteracao timestamp without time zone DEFAULT CURRENT_TIMESTAMP
);


--
-- TOC entry 8410 (class 0 OID 0)
-- Dependencies: 234
-- Name: TABLE perfil; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON TABLE salve.perfil IS 'Tabela para armazenar os perfis de acesso do sistema';


--
-- TOC entry 8411 (class 0 OID 0)
-- Dependencies: 234
-- Name: COLUMN perfil.sq_perfil; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.perfil.sq_perfil IS 'Chave primária e sequencial';


--
-- TOC entry 8412 (class 0 OID 0)
-- Dependencies: 234
-- Name: COLUMN perfil.no_perfil; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.perfil.no_perfil IS 'Nome do perfil';


--
-- TOC entry 8413 (class 0 OID 0)
-- Dependencies: 234
-- Name: COLUMN perfil.cd_sistema; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.perfil.cd_sistema IS 'Código interno para utilização do sistema';


--
-- TOC entry 8414 (class 0 OID 0)
-- Dependencies: 234
-- Name: COLUMN perfil.dt_inclusao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.perfil.dt_inclusao IS 'Data de inclusão no sistema';


--
-- TOC entry 8415 (class 0 OID 0)
-- Dependencies: 234
-- Name: COLUMN perfil.dt_alteracao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.perfil.dt_alteracao IS 'Data da última alteração';


--
-- TOC entry 233 (class 1259 OID 18210)
-- Name: perfil_sq_perfil_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.perfil_sq_perfil_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8416 (class 0 OID 0)
-- Dependencies: 233
-- Name: perfil_sq_perfil_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.perfil_sq_perfil_seq OWNED BY salve.perfil.sq_perfil;


--
-- TOC entry 231 (class 1259 OID 18178)
-- Name: pessoa_fisica; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.pessoa_fisica (
    sq_pessoa bigint NOT NULL,
    sq_instituicao bigint,
    nu_cpf character varying(11),
    dt_alteracao timestamp without time zone DEFAULT CURRENT_TIMESTAMP
);


--
-- TOC entry 8417 (class 0 OID 0)
-- Dependencies: 231
-- Name: TABLE pessoa_fisica; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON TABLE salve.pessoa_fisica IS 'Tabela para armazenar as pessoas físicas do sistema';


--
-- TOC entry 8418 (class 0 OID 0)
-- Dependencies: 231
-- Name: COLUMN pessoa_fisica.sq_pessoa; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.pessoa_fisica.sq_pessoa IS 'Chave estrangeira e primária';


--
-- TOC entry 8419 (class 0 OID 0)
-- Dependencies: 231
-- Name: COLUMN pessoa_fisica.sq_instituicao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.pessoa_fisica.sq_instituicao IS 'Chave estrangeira da tabela instituicao';


--
-- TOC entry 8420 (class 0 OID 0)
-- Dependencies: 231
-- Name: COLUMN pessoa_fisica.nu_cpf; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.pessoa_fisica.nu_cpf IS 'Número do CPF';


--
-- TOC entry 8421 (class 0 OID 0)
-- Dependencies: 231
-- Name: COLUMN pessoa_fisica.dt_alteracao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.pessoa_fisica.dt_alteracao IS 'Data da última alteração';


--
-- TOC entry 224 (class 1259 OID 18095)
-- Name: pessoa_sq_pessoa_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.pessoa_sq_pessoa_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8422 (class 0 OID 0)
-- Dependencies: 224
-- Name: pessoa_sq_pessoa_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.pessoa_sq_pessoa_seq OWNED BY salve.pessoa.sq_pessoa;


--
-- TOC entry 448 (class 1259 OID 27918)
-- Name: planilha; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.planilha (
    sq_planilha integer NOT NULL,
    sq_ciclo_avaliacao bigint,
    sq_unidade_org bigint NOT NULL,
    sq_pessoa bigint NOT NULL,
    no_arquivo text NOT NULL,
    de_caminho text NOT NULL,
    de_comentario text,
    dt_envio timestamp without time zone NOT NULL,
    no_contexto text,
    tx_log text,
    nu_percentual integer,
    in_processando boolean DEFAULT false NOT NULL
);


--
-- TOC entry 452 (class 1259 OID 27982)
-- Name: planilha_ficha; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.planilha_ficha (
    sq_planilha_linha integer NOT NULL,
    tx_autor_ano text,
    tx_grupo text,
    tx_centro text,
    tx_cpf_pf text,
    tx_cpf_ct text,
    tx_nome_comum text,
    tx_sinonimia text,
    tx_notas_taxonomicas text,
    tx_notas_morfologicas text,
    tx_endemica_brasil text,
    tx_distribuicao_global text,
    tx_distribuicao_nacional text,
    tx_altitude_maxima text,
    tx_altitude_minima text,
    tx_batimetria_maxima text,
    tx_batimetria_minima text,
    tx_historia_natural text,
    tx_especie_migratoria text,
    tx_padrao_deslocamento text,
    tx_tendencia_populacional text,
    tx_populacao text,
    tx_ameaca text,
    tx_uso text,
    tx_presenca_lista_oficial text,
    tx_acao_conservacao text,
    tx_presenca_uc text,
    tx_pesquisa text,
    tx_tipo_avaliacao text,
    tx_ano_avaliacao text,
    tx_estado_avaliacao text,
    tx_categoria_avaliacao text,
    tx_criterio_avaliacao text,
    tx_justificativa_avaliacao text,
    sq_ficha bigint,
    tx_possivelmente_extinta text,
    tx_pendencias text,
    tx_subgrupo text
);


--
-- TOC entry 8423 (class 0 OID 0)
-- Dependencies: 452
-- Name: COLUMN planilha_ficha.tx_cpf_pf; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.planilha_ficha.tx_cpf_pf IS 'CPF do ponto focal ( PF )';


--
-- TOC entry 8424 (class 0 OID 0)
-- Dependencies: 452
-- Name: COLUMN planilha_ficha.tx_cpf_ct; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.planilha_ficha.tx_cpf_ct IS 'CPF do coordenador de taxon ( CT )';


--
-- TOC entry 451 (class 1259 OID 27965)
-- Name: planilha_linha; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.planilha_linha (
    sq_planilha_linha integer NOT NULL,
    sq_planilha bigint NOT NULL,
    tx_nome_cientifico text,
    nu_linha bigint,
    tx_erros text
);


--
-- TOC entry 450 (class 1259 OID 27963)
-- Name: planilha_linha_sq_planilha_linha_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.planilha_linha_sq_planilha_linha_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8425 (class 0 OID 0)
-- Dependencies: 450
-- Name: planilha_linha_sq_planilha_linha_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.planilha_linha_sq_planilha_linha_seq OWNED BY salve.planilha_linha.sq_planilha_linha;


--
-- TOC entry 453 (class 1259 OID 28003)
-- Name: planilha_ocorrencia; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.planilha_ocorrencia (
    sq_planilha_linha integer NOT NULL,
    tx_latitude text,
    tx_longitude text,
    tx_prazo_carencia text,
    tx_datum text,
    tx_formato_original text,
    tx_precisao_coordenada text,
    tx_referencia_aproximacao text,
    tx_metodo_aproximacao text,
    tx_descricao_metodo_aproximacao text,
    tx_taxon_original_citado text,
    tx_presenca_atual text,
    tx_tipo_registro text,
    tx_dia text,
    tx_mes text,
    tx_ano text,
    tx_hora text,
    tx_data_ano_desconhecidos text,
    tx_localidade text,
    tx_caracteristica_localidade text,
    tx_uc_federal text,
    tx_uc_estadual text,
    tx_rppn text,
    tx_pais text,
    tx_estado text,
    tx_municipio character varying(255),
    tx_ambiente text,
    tx_habitat character varying(255),
    tx_elevacao_minima text,
    tx_elevacao_maxima text,
    tx_profundidade_minima text,
    tx_profundidade_maxima text,
    tx_identificador text,
    tx_data_identificacao text,
    tx_indicacao_incerta text,
    tx_tombamento text,
    tx_instituicao_tombamento text,
    tx_compilador text,
    tx_data_compilacao text,
    tx_revisor text,
    tx_data_revisao text,
    tx_validador text,
    tx_data_validacao text,
    tx_usado_avaliacao text,
    tx_justificativa_nao_uso text,
    sq_ficha_ocorrencia bigint,
    sq_ocorrencia bigint,
    id_origem text,
    tx_titulo_ref_bib text,
    tx_autor_ref_bib text,
    tx_ano_ref_bib text,
    tx_comunicacao_pessoal text,
    tx_observacao text,
    id_ref_bib_salve text,
    de_doi text
);


--
-- TOC entry 8426 (class 0 OID 0)
-- Dependencies: 453
-- Name: COLUMN planilha_ocorrencia.id_origem; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.planilha_ocorrencia.id_origem IS 'Identificador único do registro na base de dados de origem';


--
-- TOC entry 8427 (class 0 OID 0)
-- Dependencies: 453
-- Name: COLUMN planilha_ocorrencia.tx_titulo_ref_bib; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.planilha_ocorrencia.tx_titulo_ref_bib IS 'titulo da referencia bibliografica';


--
-- TOC entry 8428 (class 0 OID 0)
-- Dependencies: 453
-- Name: COLUMN planilha_ocorrencia.tx_autor_ref_bib; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.planilha_ocorrencia.tx_autor_ref_bib IS 'autor/autores da referencia bibliografica';


--
-- TOC entry 8429 (class 0 OID 0)
-- Dependencies: 453
-- Name: COLUMN planilha_ocorrencia.tx_ano_ref_bib; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.planilha_ocorrencia.tx_ano_ref_bib IS 'ano da referencia bibliografica';


--
-- TOC entry 8430 (class 0 OID 0)
-- Dependencies: 453
-- Name: COLUMN planilha_ocorrencia.tx_comunicacao_pessoal; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.planilha_ocorrencia.tx_comunicacao_pessoal IS 'comunicacao pessoal sobre a referencia bibliografica';


--
-- TOC entry 8431 (class 0 OID 0)
-- Dependencies: 453
-- Name: COLUMN planilha_ocorrencia.id_ref_bib_salve; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.planilha_ocorrencia.id_ref_bib_salve IS 'Identificador da referencia bibliografica.';


--
-- TOC entry 8432 (class 0 OID 0)
-- Dependencies: 453
-- Name: COLUMN planilha_ocorrencia.de_doi; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.planilha_ocorrencia.de_doi IS 'Numero do DOI da referência bibliográfica.';


--
-- TOC entry 447 (class 1259 OID 27916)
-- Name: planilha_sq_planilha_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.planilha_sq_planilha_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8433 (class 0 OID 0)
-- Dependencies: 447
-- Name: planilha_sq_planilha_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.planilha_sq_planilha_seq OWNED BY salve.planilha.sq_planilha;


--
-- TOC entry 279 (class 1259 OID 23379)
-- Name: plano_acao_sq_plano_acao_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.plano_acao_sq_plano_acao_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8434 (class 0 OID 0)
-- Dependencies: 279
-- Name: plano_acao_sq_plano_acao_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.plano_acao_sq_plano_acao_seq OWNED BY salve.plano_acao.sq_plano_acao;


--
-- TOC entry 219 (class 1259 OID 18009)
-- Name: regiao; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.regiao (
    sq_regiao integer NOT NULL,
    no_regiao character varying(20) NOT NULL,
    nu_ordem integer,
    ge_regiao public.geometry,
    dt_inclusao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    dt_alteracao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    CONSTRAINT enforce_dims_the_geom CHECK ((public.st_ndims(ge_regiao) = 2)),
    CONSTRAINT enforce_geotype_the_geom CHECK (((public.geometrytype(ge_regiao) = 'MULTIPOLYGON'::text) OR (ge_regiao IS NULL))),
    CONSTRAINT enforce_srid_the_geom CHECK ((public.st_srid(ge_regiao) = 4674))
);


--
-- TOC entry 218 (class 1259 OID 18007)
-- Name: regiao_sq_regiao_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.regiao_sq_regiao_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8435 (class 0 OID 0)
-- Dependencies: 218
-- Name: regiao_sq_regiao_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.regiao_sq_regiao_seq OWNED BY salve.regiao.sq_regiao;


--
-- TOC entry 367 (class 1259 OID 25773)
-- Name: registro; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.registro (
    sq_registro integer NOT NULL,
    ge_registro public.geometry NOT NULL,
    dt_inclusao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


--
-- TOC entry 8436 (class 0 OID 0)
-- Dependencies: 367
-- Name: TABLE registro; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON TABLE salve.registro IS 'tabela das coordenadas do registro de ocorrência (ponto) cadastrado no SALVE';


--
-- TOC entry 8437 (class 0 OID 0)
-- Dependencies: 367
-- Name: COLUMN registro.sq_registro; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.registro.sq_registro IS 'chave primária sequencial';


--
-- TOC entry 455 (class 1259 OID 28032)
-- Name: registro_bacia; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.registro_bacia (
    sq_registro_bacia integer NOT NULL,
    sq_registro bigint NOT NULL,
    sq_bacia_geo bigint NOT NULL,
    dt_inclusao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


--
-- TOC entry 8438 (class 0 OID 0)
-- Dependencies: 455
-- Name: TABLE registro_bacia; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON TABLE salve.registro_bacia IS 'Tabela para registro da Bacia Hidrográfica que o ponto intersecta';


--
-- TOC entry 8439 (class 0 OID 0)
-- Dependencies: 455
-- Name: COLUMN registro_bacia.sq_registro_bacia; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.registro_bacia.sq_registro_bacia IS 'Chave primária sequencial';


--
-- TOC entry 8440 (class 0 OID 0)
-- Dependencies: 455
-- Name: COLUMN registro_bacia.sq_registro; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.registro_bacia.sq_registro IS 'Chave estrangeira tabela Registro';


--
-- TOC entry 8441 (class 0 OID 0)
-- Dependencies: 455
-- Name: COLUMN registro_bacia.sq_bacia_geo; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.registro_bacia.sq_bacia_geo IS 'Chave estrangeira tabela Dados_apoio_geo';


--
-- TOC entry 8442 (class 0 OID 0)
-- Dependencies: 455
-- Name: COLUMN registro_bacia.dt_inclusao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.registro_bacia.dt_inclusao IS 'Data de inclusão no banco de dados';


--
-- TOC entry 454 (class 1259 OID 28030)
-- Name: registro_bacia_sq_registro_bacia_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.registro_bacia_sq_registro_bacia_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8443 (class 0 OID 0)
-- Dependencies: 454
-- Name: registro_bacia_sq_registro_bacia_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.registro_bacia_sq_registro_bacia_seq OWNED BY salve.registro_bacia.sq_registro_bacia;


--
-- TOC entry 366 (class 1259 OID 25771)
-- Name: registro_sq_registro_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.registro_sq_registro_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8444 (class 0 OID 0)
-- Dependencies: 366
-- Name: registro_sq_registro_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.registro_sq_registro_seq OWNED BY salve.registro.sq_registro;


--
-- TOC entry 467 (class 1259 OID 28262)
-- Name: registro_terra_indigena; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.registro_terra_indigena (
    sq_registro_terra_indigena bigint NOT NULL,
    sq_registro bigint NOT NULL,
    sq_terra_indigena bigint NOT NULL,
    dt_inclusao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


--
-- TOC entry 8445 (class 0 OID 0)
-- Dependencies: 467
-- Name: TABLE registro_terra_indigena; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON TABLE salve.registro_terra_indigena IS 'Tabela para registro da Terra Indigena que o ponto intersecta';


--
-- TOC entry 8446 (class 0 OID 0)
-- Dependencies: 467
-- Name: COLUMN registro_terra_indigena.sq_registro; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.registro_terra_indigena.sq_registro IS 'Chave estrangeira tabela Registro';


--
-- TOC entry 8447 (class 0 OID 0)
-- Dependencies: 467
-- Name: COLUMN registro_terra_indigena.sq_terra_indigena; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.registro_terra_indigena.sq_terra_indigena IS 'Chave estrangeira tabela geo.terra_indigena';


--
-- TOC entry 8448 (class 0 OID 0)
-- Dependencies: 467
-- Name: COLUMN registro_terra_indigena.dt_inclusao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.registro_terra_indigena.dt_inclusao IS 'Data de inclusão no banco de dados';


--
-- TOC entry 466 (class 1259 OID 28260)
-- Name: registro_terra_indigena_sq_registro_terra_indigena_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.registro_terra_indigena_sq_registro_terra_indigena_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8449 (class 0 OID 0)
-- Dependencies: 466
-- Name: registro_terra_indigena_sq_registro_terra_indigena_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.registro_terra_indigena_sq_registro_terra_indigena_seq OWNED BY salve.registro_terra_indigena.sq_registro_terra_indigena;


--
-- TOC entry 469 (class 1259 OID 28289)
-- Name: registro_uc_estadual_sq_registro_uc_estadual_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.registro_uc_estadual_sq_registro_uc_estadual_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8450 (class 0 OID 0)
-- Dependencies: 469
-- Name: registro_uc_estadual_sq_registro_uc_estadual_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.registro_uc_estadual_sq_registro_uc_estadual_seq OWNED BY salve.registro_uc_estadual.sq_registro_uc_estadual;


--
-- TOC entry 471 (class 1259 OID 28317)
-- Name: registro_uc_federal_sq_registro_uc_federal_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.registro_uc_federal_sq_registro_uc_federal_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8451 (class 0 OID 0)
-- Dependencies: 471
-- Name: registro_uc_federal_sq_registro_uc_federal_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.registro_uc_federal_sq_registro_uc_federal_seq OWNED BY salve.registro_uc_federal.sq_registro_uc_federal;


--
-- TOC entry 475 (class 1259 OID 28367)
-- Name: taxon_historico_avaliacao_sq_taxon_historico_avaliacao_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.taxon_historico_avaliacao_sq_taxon_historico_avaliacao_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8452 (class 0 OID 0)
-- Dependencies: 475
-- Name: taxon_historico_avaliacao_sq_taxon_historico_avaliacao_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.taxon_historico_avaliacao_sq_taxon_historico_avaliacao_seq OWNED BY salve.taxon_historico_avaliacao.sq_taxon_historico_avaliacao;


--
-- TOC entry 521 (class 1259 OID 29544)
-- Name: taxon_historico_avaliacao_versao; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.taxon_historico_avaliacao_versao (
    sq_taxon_historico_avaliacao integer NOT NULL,
    sq_ficha_versao integer NOT NULL
);


--
-- TOC entry 8453 (class 0 OID 0)
-- Dependencies: 521
-- Name: COLUMN taxon_historico_avaliacao_versao.sq_taxon_historico_avaliacao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.taxon_historico_avaliacao_versao.sq_taxon_historico_avaliacao IS 'Chave estrangeira e primária recebida da tabela taxon_historico_avaliacao';


--
-- TOC entry 8454 (class 0 OID 0)
-- Dependencies: 521
-- Name: COLUMN taxon_historico_avaliacao_versao.sq_ficha_versao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.taxon_historico_avaliacao_versao.sq_ficha_versao IS 'Chave estrangeira da tabela ficha_versao';


--
-- TOC entry 465 (class 1259 OID 28239)
-- Name: terra_indigena; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.terra_indigena (
    gid integer NOT NULL,
    the_geom public.geometry(MultiPolygon,4674),
    terrai_cod numeric(16,6),
    terrai_nom character varying(254),
    terrai_pro character varying(254),
    status_ter character varying(254),
    fase_descr character varying(254),
    tipo_terra character varying(254),
    terrai_fai character varying(254),
    terrai_obs character varying(254),
    terrai_a_1 character varying(254),
    situa_desc character varying(254),
    tipo_area_ character varying(254),
    terrai_per character varying(254),
    terrai_pop character varying(254)
);


--
-- TOC entry 464 (class 1259 OID 28237)
-- Name: terra_indigena_gid_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.terra_indigena_gid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8455 (class 0 OID 0)
-- Dependencies: 464
-- Name: terra_indigena_gid_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.terra_indigena_gid_seq OWNED BY salve.terra_indigena.gid;


--
-- TOC entry 478 (class 1259 OID 28403)
-- Name: traducao_dados_apoio; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.traducao_dados_apoio (
    sq_traducao_dados_apoio integer NOT NULL,
    sq_dados_apoio bigint NOT NULL,
    tx_traduzido text NOT NULL,
    tx_revisado text NOT NULL,
    dt_inclusao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    dt_alteracao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


--
-- TOC entry 8456 (class 0 OID 0)
-- Dependencies: 478
-- Name: TABLE traducao_dados_apoio; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON TABLE salve.traducao_dados_apoio IS 'tabela para registrar as traducoes realizadas no registros da tabela de apoio';


--
-- TOC entry 8457 (class 0 OID 0)
-- Dependencies: 478
-- Name: COLUMN traducao_dados_apoio.sq_traducao_dados_apoio; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.traducao_dados_apoio.sq_traducao_dados_apoio IS 'chave primaria da tabela dados_apoio_traducao';


--
-- TOC entry 8458 (class 0 OID 0)
-- Dependencies: 478
-- Name: COLUMN traducao_dados_apoio.sq_dados_apoio; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.traducao_dados_apoio.sq_dados_apoio IS 'chave estrangeira da tabela dados_apoio';


--
-- TOC entry 8459 (class 0 OID 0)
-- Dependencies: 478
-- Name: COLUMN traducao_dados_apoio.tx_traduzido; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.traducao_dados_apoio.tx_traduzido IS 'texto, palarva ou frase apos a traducao';


--
-- TOC entry 8460 (class 0 OID 0)
-- Dependencies: 478
-- Name: COLUMN traducao_dados_apoio.tx_revisado; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.traducao_dados_apoio.tx_revisado IS 'texto, palarva ou frase apos a revisao da traducao automatica';


--
-- TOC entry 8461 (class 0 OID 0)
-- Dependencies: 478
-- Name: COLUMN traducao_dados_apoio.dt_inclusao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.traducao_dados_apoio.dt_inclusao IS 'data da criacao do registro no banco de dados';


--
-- TOC entry 8462 (class 0 OID 0)
-- Dependencies: 478
-- Name: COLUMN traducao_dados_apoio.dt_alteracao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.traducao_dados_apoio.dt_alteracao IS 'data da ultima alteracao realizada no registro';


--
-- TOC entry 477 (class 1259 OID 28401)
-- Name: traducao_dados_apoio_sq_traducao_dados_apoio_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.traducao_dados_apoio_sq_traducao_dados_apoio_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8463 (class 0 OID 0)
-- Dependencies: 477
-- Name: traducao_dados_apoio_sq_traducao_dados_apoio_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.traducao_dados_apoio_sq_traducao_dados_apoio_seq OWNED BY salve.traducao_dados_apoio.sq_traducao_dados_apoio;


--
-- TOC entry 513 (class 1259 OID 29162)
-- Name: traducao_ficha_percentual; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.traducao_ficha_percentual (
    sq_ficha bigint NOT NULL,
    nu_percentual_traduzido integer DEFAULT 0 NOT NULL,
    nu_percentual_revisado integer DEFAULT 0 NOT NULL
);


--
-- TOC entry 8464 (class 0 OID 0)
-- Dependencies: 513
-- Name: TABLE traducao_ficha_percentual; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON TABLE salve.traducao_ficha_percentual IS 'Registrar o percentual traduzido e revisado da ficha';


--
-- TOC entry 8465 (class 0 OID 0)
-- Dependencies: 513
-- Name: COLUMN traducao_ficha_percentual.sq_ficha; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.traducao_ficha_percentual.sq_ficha IS 'chave estrangeira e primaria da tabela';


--
-- TOC entry 8466 (class 0 OID 0)
-- Dependencies: 513
-- Name: COLUMN traducao_ficha_percentual.nu_percentual_traduzido; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.traducao_ficha_percentual.nu_percentual_traduzido IS 'valor percentual traduzido da ficha';


--
-- TOC entry 8467 (class 0 OID 0)
-- Dependencies: 513
-- Name: COLUMN traducao_ficha_percentual.nu_percentual_revisado; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.traducao_ficha_percentual.nu_percentual_revisado IS 'valor percentual revisado das traducoes';


--
-- TOC entry 480 (class 1259 OID 28423)
-- Name: traducao_fila; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.traducao_fila (
    sq_traducao_fila bigint NOT NULL,
    sq_ficha bigint NOT NULL,
    dt_inclusao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    dt_alteracao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    nu_percentual integer DEFAULT 0 NOT NULL,
    st_executando boolean DEFAULT false NOT NULL
);


--
-- TOC entry 8468 (class 0 OID 0)
-- Dependencies: 480
-- Name: TABLE traducao_fila; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON TABLE salve.traducao_fila IS 'fila de fichas a serem traduzidas em lote';


--
-- TOC entry 8469 (class 0 OID 0)
-- Dependencies: 480
-- Name: COLUMN traducao_fila.sq_ficha; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.traducao_fila.sq_ficha IS 'chave estrangeira da tabela ficha';


--
-- TOC entry 8470 (class 0 OID 0)
-- Dependencies: 480
-- Name: COLUMN traducao_fila.dt_inclusao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.traducao_fila.dt_inclusao IS 'data e hora que a ficha entrou na fila';


--
-- TOC entry 8471 (class 0 OID 0)
-- Dependencies: 480
-- Name: COLUMN traducao_fila.nu_percentual; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.traducao_fila.nu_percentual IS 'percentual da traducao concluido';


--
-- TOC entry 479 (class 1259 OID 28421)
-- Name: traducao_fila_sq_traducao_fila_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.traducao_fila_sq_traducao_fila_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8472 (class 0 OID 0)
-- Dependencies: 479
-- Name: traducao_fila_sq_traducao_fila_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.traducao_fila_sq_traducao_fila_seq OWNED BY salve.traducao_fila.sq_traducao_fila;


--
-- TOC entry 482 (class 1259 OID 28444)
-- Name: traducao_pan; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.traducao_pan (
    sq_traducao_pan integer NOT NULL,
    sq_plano_acao bigint NOT NULL,
    tx_traduzido text NOT NULL,
    tx_revisado text NOT NULL,
    dt_inclusao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    dt_alteracao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


--
-- TOC entry 8473 (class 0 OID 0)
-- Dependencies: 482
-- Name: COLUMN traducao_pan.sq_traducao_pan; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.traducao_pan.sq_traducao_pan IS 'Chave primária sequencial da tabela traducao_pan';


--
-- TOC entry 8474 (class 0 OID 0)
-- Dependencies: 482
-- Name: COLUMN traducao_pan.sq_plano_acao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.traducao_pan.sq_plano_acao IS 'Chave estrangeira da tabela plano acao';


--
-- TOC entry 8475 (class 0 OID 0)
-- Dependencies: 482
-- Name: COLUMN traducao_pan.tx_traduzido; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.traducao_pan.tx_traduzido IS 'Texto, palarva ou frase apos a traducao';


--
-- TOC entry 8476 (class 0 OID 0)
-- Dependencies: 482
-- Name: COLUMN traducao_pan.tx_revisado; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.traducao_pan.tx_revisado IS 'Texto, palarva ou frase apos a revisao da traducao automatica';


--
-- TOC entry 8477 (class 0 OID 0)
-- Dependencies: 482
-- Name: COLUMN traducao_pan.dt_inclusao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.traducao_pan.dt_inclusao IS 'Data da criacao do registro no banco de dados';


--
-- TOC entry 8478 (class 0 OID 0)
-- Dependencies: 482
-- Name: COLUMN traducao_pan.dt_alteracao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.traducao_pan.dt_alteracao IS 'Data da ultima alteracao realizada no registro';


--
-- TOC entry 481 (class 1259 OID 28442)
-- Name: traducao_pan_sq_traducao_pan_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.traducao_pan_sq_traducao_pan_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8479 (class 0 OID 0)
-- Dependencies: 481
-- Name: traducao_pan_sq_traducao_pan_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.traducao_pan_sq_traducao_pan_seq OWNED BY salve.traducao_pan.sq_traducao_pan;


--
-- TOC entry 486 (class 1259 OID 28500)
-- Name: traducao_revisao; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.traducao_revisao (
    sq_traducao_revisao bigint NOT NULL,
    sq_traducao_tabela bigint NOT NULL,
    sq_registro bigint NOT NULL,
    sq_pessoa bigint,
    tx_traduzido text NOT NULL,
    dt_revisao timestamp without time zone,
    dt_inclusao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    dt_alteracao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


--
-- TOC entry 8480 (class 0 OID 0)
-- Dependencies: 486
-- Name: TABLE traducao_revisao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON TABLE salve.traducao_revisao IS 'tabela para armazenamento do autor da traducao realizadas no campo de uma trabela';


--
-- TOC entry 8481 (class 0 OID 0)
-- Dependencies: 486
-- Name: COLUMN traducao_revisao.sq_traducao_revisao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.traducao_revisao.sq_traducao_revisao IS 'chave primaria sequencial da tabela traducao_revisao';


--
-- TOC entry 8482 (class 0 OID 0)
-- Dependencies: 486
-- Name: COLUMN traducao_revisao.sq_traducao_tabela; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.traducao_revisao.sq_traducao_tabela IS 'chave primaria sequencial da tabela traducao_tabela';


--
-- TOC entry 8483 (class 0 OID 0)
-- Dependencies: 486
-- Name: COLUMN traducao_revisao.sq_registro; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.traducao_revisao.sq_registro IS 'id do registro traduzido';


--
-- TOC entry 8484 (class 0 OID 0)
-- Dependencies: 486
-- Name: COLUMN traducao_revisao.sq_pessoa; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.traducao_revisao.sq_pessoa IS 'id do usuario que realizou a revisao da traducao automática';


--
-- TOC entry 8485 (class 0 OID 0)
-- Dependencies: 486
-- Name: COLUMN traducao_revisao.tx_traduzido; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.traducao_revisao.tx_traduzido IS 'texto traduzido';


--
-- TOC entry 8486 (class 0 OID 0)
-- Dependencies: 486
-- Name: COLUMN traducao_revisao.dt_revisao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.traducao_revisao.dt_revisao IS 'data que a traducao automatica foi revisada';


--
-- TOC entry 8487 (class 0 OID 0)
-- Dependencies: 486
-- Name: COLUMN traducao_revisao.dt_inclusao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.traducao_revisao.dt_inclusao IS 'data da criacao do registro no banco de dados';


--
-- TOC entry 8488 (class 0 OID 0)
-- Dependencies: 486
-- Name: COLUMN traducao_revisao.dt_alteracao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.traducao_revisao.dt_alteracao IS 'data da ultima alteracao realizada no registro';


--
-- TOC entry 488 (class 1259 OID 28538)
-- Name: traducao_revisao_hist; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.traducao_revisao_hist (
    sq_traducao_revisao_hist bigint NOT NULL,
    sq_traducao_revisao bigint NOT NULL,
    sq_pessoa bigint NOT NULL,
    tx_traduzido text NOT NULL,
    dt_revisao timestamp without time zone,
    dt_inclusao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


--
-- TOC entry 8489 (class 0 OID 0)
-- Dependencies: 488
-- Name: TABLE traducao_revisao_hist; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON TABLE salve.traducao_revisao_hist IS 'tabela para armazenamento do historico das revisões dos textos da ficha';


--
-- TOC entry 8490 (class 0 OID 0)
-- Dependencies: 488
-- Name: COLUMN traducao_revisao_hist.sq_traducao_revisao_hist; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.traducao_revisao_hist.sq_traducao_revisao_hist IS 'chave primaria sequencial da tabela traducao_revisao_hist';


--
-- TOC entry 8491 (class 0 OID 0)
-- Dependencies: 488
-- Name: COLUMN traducao_revisao_hist.sq_traducao_revisao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.traducao_revisao_hist.sq_traducao_revisao IS 'chave estrangeira da tabela traducao_revisa';


--
-- TOC entry 8492 (class 0 OID 0)
-- Dependencies: 488
-- Name: COLUMN traducao_revisao_hist.sq_pessoa; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.traducao_revisao_hist.sq_pessoa IS 'id do usuario que realizou a alteracao no texto ou na data da revisao';


--
-- TOC entry 8493 (class 0 OID 0)
-- Dependencies: 488
-- Name: COLUMN traducao_revisao_hist.tx_traduzido; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.traducao_revisao_hist.tx_traduzido IS 'texto traduzido que foi alterado';


--
-- TOC entry 8494 (class 0 OID 0)
-- Dependencies: 488
-- Name: COLUMN traducao_revisao_hist.dt_revisao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.traducao_revisao_hist.dt_revisao IS 'data que a traducao automatica foi revisada';


--
-- TOC entry 8495 (class 0 OID 0)
-- Dependencies: 488
-- Name: COLUMN traducao_revisao_hist.dt_inclusao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.traducao_revisao_hist.dt_inclusao IS 'data da criacao do registro no banco de dados';


--
-- TOC entry 487 (class 1259 OID 28536)
-- Name: traducao_revisao_hist_sq_traducao_revisao_hist_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.traducao_revisao_hist_sq_traducao_revisao_hist_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8496 (class 0 OID 0)
-- Dependencies: 487
-- Name: traducao_revisao_hist_sq_traducao_revisao_hist_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.traducao_revisao_hist_sq_traducao_revisao_hist_seq OWNED BY salve.traducao_revisao_hist.sq_traducao_revisao_hist;


--
-- TOC entry 485 (class 1259 OID 28498)
-- Name: traducao_revisao_sq_traducao_revisao_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.traducao_revisao_sq_traducao_revisao_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8497 (class 0 OID 0)
-- Dependencies: 485
-- Name: traducao_revisao_sq_traducao_revisao_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.traducao_revisao_sq_traducao_revisao_seq OWNED BY salve.traducao_revisao.sq_traducao_revisao;


--
-- TOC entry 484 (class 1259 OID 28482)
-- Name: traducao_tabela; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.traducao_tabela (
    sq_traducao_tabela bigint NOT NULL,
    no_tabela text NOT NULL,
    no_coluna text NOT NULL,
    ds_coluna text NOT NULL,
    dt_inclusao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    dt_alteracao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


--
-- TOC entry 8498 (class 0 OID 0)
-- Dependencies: 484
-- Name: TABLE traducao_tabela; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON TABLE salve.traducao_tabela IS 'tabela com relacao de tabelas e suas colunas que deverao ser traduzidas para possibilitar o acompanhamento do andamento da revisao';


--
-- TOC entry 8499 (class 0 OID 0)
-- Dependencies: 484
-- Name: COLUMN traducao_tabela.sq_traducao_tabela; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.traducao_tabela.sq_traducao_tabela IS 'chave primaria sequencial da tabela traducao_tabela';


--
-- TOC entry 8500 (class 0 OID 0)
-- Dependencies: 484
-- Name: COLUMN traducao_tabela.no_tabela; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.traducao_tabela.no_tabela IS 'nome fisico da tabela no banco de dados';


--
-- TOC entry 8501 (class 0 OID 0)
-- Dependencies: 484
-- Name: COLUMN traducao_tabela.no_coluna; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.traducao_tabela.no_coluna IS 'nome fisico da coluna tera seu conteudo traduzido';


--
-- TOC entry 8502 (class 0 OID 0)
-- Dependencies: 484
-- Name: COLUMN traducao_tabela.ds_coluna; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.traducao_tabela.ds_coluna IS 'descricao da coluna para identificacao no contexto da ficha e exibir mensagem ao usuario';


--
-- TOC entry 8503 (class 0 OID 0)
-- Dependencies: 484
-- Name: COLUMN traducao_tabela.dt_inclusao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.traducao_tabela.dt_inclusao IS 'data da criacao do registro no banco de dados';


--
-- TOC entry 8504 (class 0 OID 0)
-- Dependencies: 484
-- Name: COLUMN traducao_tabela.dt_alteracao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.traducao_tabela.dt_alteracao IS 'data da ultima alteracao realizada no registro';


--
-- TOC entry 483 (class 1259 OID 28480)
-- Name: traducao_tabela_sq_traducao_tabela_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.traducao_tabela_sq_traducao_tabela_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8505 (class 0 OID 0)
-- Dependencies: 483
-- Name: traducao_tabela_sq_traducao_tabela_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.traducao_tabela_sq_traducao_tabela_seq OWNED BY salve.traducao_tabela.sq_traducao_tabela;


--
-- TOC entry 490 (class 1259 OID 28562)
-- Name: traducao_texto; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.traducao_texto (
    sq_traducao_texto bigint NOT NULL,
    tx_original text NOT NULL,
    tx_traduzido text NOT NULL,
    tx_revisado text NOT NULL,
    dt_inclusao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    dt_alteracao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


--
-- TOC entry 8506 (class 0 OID 0)
-- Dependencies: 490
-- Name: TABLE traducao_texto; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON TABLE salve.traducao_texto IS 'tabela para armazenamento das traducoes dos textos, palavras e frases utilizadas no sistema salve para integração com o sistema sis/iucn';


--
-- TOC entry 8507 (class 0 OID 0)
-- Dependencies: 490
-- Name: COLUMN traducao_texto.sq_traducao_texto; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.traducao_texto.sq_traducao_texto IS 'chave primaria sequencial da tabela traducao_texto';


--
-- TOC entry 8508 (class 0 OID 0)
-- Dependencies: 490
-- Name: COLUMN traducao_texto.tx_original; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.traducao_texto.tx_original IS 'texto, palavra ou frase no formato original antes da traducao';


--
-- TOC entry 8509 (class 0 OID 0)
-- Dependencies: 490
-- Name: COLUMN traducao_texto.tx_traduzido; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.traducao_texto.tx_traduzido IS 'texto, palarva ou frase apos a traducao';


--
-- TOC entry 8510 (class 0 OID 0)
-- Dependencies: 490
-- Name: COLUMN traducao_texto.tx_revisado; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.traducao_texto.tx_revisado IS 'texto, palarva ou frase apos a revisao da traducao automatica';


--
-- TOC entry 8511 (class 0 OID 0)
-- Dependencies: 490
-- Name: COLUMN traducao_texto.dt_inclusao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.traducao_texto.dt_inclusao IS 'data da criacao do registro no banco de dados';


--
-- TOC entry 8512 (class 0 OID 0)
-- Dependencies: 490
-- Name: COLUMN traducao_texto.dt_alteracao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.traducao_texto.dt_alteracao IS 'data da ultima alteracao realizada no registro';


--
-- TOC entry 489 (class 1259 OID 28560)
-- Name: traducao_texto_sq_traducao_texto_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.traducao_texto_sq_traducao_texto_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8513 (class 0 OID 0)
-- Dependencies: 489
-- Name: traducao_texto_sq_traducao_texto_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.traducao_texto_sq_traducao_texto_seq OWNED BY salve.traducao_texto.sq_traducao_texto;


--
-- TOC entry 492 (class 1259 OID 28578)
-- Name: traducao_uso; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.traducao_uso (
    sq_traducao_uso integer NOT NULL,
    sq_uso bigint NOT NULL,
    tx_traduzido text NOT NULL,
    tx_revisado text NOT NULL,
    dt_inclusao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    dt_alteracao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


--
-- TOC entry 8514 (class 0 OID 0)
-- Dependencies: 492
-- Name: TABLE traducao_uso; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON TABLE salve.traducao_uso IS 'tabela para registrar as traducoes realizadas na tabela de usos';


--
-- TOC entry 8515 (class 0 OID 0)
-- Dependencies: 492
-- Name: COLUMN traducao_uso.sq_traducao_uso; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.traducao_uso.sq_traducao_uso IS 'chave primaria da tabelatraducao_uso';


--
-- TOC entry 8516 (class 0 OID 0)
-- Dependencies: 492
-- Name: COLUMN traducao_uso.sq_uso; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.traducao_uso.sq_uso IS 'chave estrangeira da tabela uso';


--
-- TOC entry 8517 (class 0 OID 0)
-- Dependencies: 492
-- Name: COLUMN traducao_uso.tx_traduzido; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.traducao_uso.tx_traduzido IS 'texto apos a traducao';


--
-- TOC entry 8518 (class 0 OID 0)
-- Dependencies: 492
-- Name: COLUMN traducao_uso.tx_revisado; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.traducao_uso.tx_revisado IS 'texto, palarva ou frase apos a revisao da traducao automatica';


--
-- TOC entry 8519 (class 0 OID 0)
-- Dependencies: 492
-- Name: COLUMN traducao_uso.dt_inclusao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.traducao_uso.dt_inclusao IS 'data da criacao do registro no banco de dados';


--
-- TOC entry 8520 (class 0 OID 0)
-- Dependencies: 492
-- Name: COLUMN traducao_uso.dt_alteracao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.traducao_uso.dt_alteracao IS 'data da ultima alteracao realizada no registro';


--
-- TOC entry 491 (class 1259 OID 28576)
-- Name: traducao_uso_sq_traducao_uso_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.traducao_uso_sq_traducao_uso_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8521 (class 0 OID 0)
-- Dependencies: 491
-- Name: traducao_uso_sq_traducao_uso_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.traducao_uso_sq_traducao_uso_seq OWNED BY salve.traducao_uso.sq_traducao_uso;


--
-- TOC entry 245 (class 1259 OID 20385)
-- Name: trilha_auditoria; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.trilha_auditoria (
    sq_trilha_auditoria bigint NOT NULL,
    dt_log timestamp without time zone NOT NULL,
    de_ip character varying(15),
    de_env character varying(20) NOT NULL,
    no_pessoa character varying(50) NOT NULL,
    nu_cpf character varying(15),
    no_modulo character varying(100) NOT NULL,
    no_acao character varying(50) NOT NULL,
    tx_parametros text
);


--
-- TOC entry 8522 (class 0 OID 0)
-- Dependencies: 245
-- Name: TABLE trilha_auditoria; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON TABLE salve.trilha_auditoria IS 'Tabela para registro das acoes de inclusoes, alteracoes e exclusoes realizadas pelos usuarios.';


--
-- TOC entry 8523 (class 0 OID 0)
-- Dependencies: 245
-- Name: COLUMN trilha_auditoria.sq_trilha_auditoria; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.trilha_auditoria.sq_trilha_auditoria IS 'Chave primaria e sequencial';


--
-- TOC entry 8524 (class 0 OID 0)
-- Dependencies: 245
-- Name: COLUMN trilha_auditoria.dt_log; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.trilha_auditoria.dt_log IS 'Data e hora da acao realizada pelo usuario';


--
-- TOC entry 8525 (class 0 OID 0)
-- Dependencies: 245
-- Name: COLUMN trilha_auditoria.de_ip; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.trilha_auditoria.de_ip IS 'Endereco IP do usuario';


--
-- TOC entry 8526 (class 0 OID 0)
-- Dependencies: 245
-- Name: COLUMN trilha_auditoria.de_env; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.trilha_auditoria.de_env IS 'Ambiente em que a acao ocorreu. Ex: PRODUCAO/DESENV/TEST/HOMOLOG';


--
-- TOC entry 8527 (class 0 OID 0)
-- Dependencies: 245
-- Name: COLUMN trilha_auditoria.no_pessoa; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.trilha_auditoria.no_pessoa IS 'Nome do usuario que realizou a acao';


--
-- TOC entry 8528 (class 0 OID 0)
-- Dependencies: 245
-- Name: COLUMN trilha_auditoria.nu_cpf; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.trilha_auditoria.nu_cpf IS 'Numero do CPF do usuario que realiou a acao';


--
-- TOC entry 8529 (class 0 OID 0)
-- Dependencies: 245
-- Name: COLUMN trilha_auditoria.no_modulo; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.trilha_auditoria.no_modulo IS 'Nome do modulo do sistema que recebeu a acao';


--
-- TOC entry 8530 (class 0 OID 0)
-- Dependencies: 245
-- Name: COLUMN trilha_auditoria.no_acao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.trilha_auditoria.no_acao IS 'Nome da acao executada';


--
-- TOC entry 8531 (class 0 OID 0)
-- Dependencies: 245
-- Name: COLUMN trilha_auditoria.tx_parametros; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.trilha_auditoria.tx_parametros IS 'Nomes e valores dos parametros enviados para execucao da acao no formato JSON';


--
-- TOC entry 244 (class 1259 OID 20383)
-- Name: trilha_auditoria_sq_trilha_auditoria_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.trilha_auditoria_sq_trilha_auditoria_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8532 (class 0 OID 0)
-- Dependencies: 244
-- Name: trilha_auditoria_sq_trilha_auditoria_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.trilha_auditoria_sq_trilha_auditoria_seq OWNED BY salve.trilha_auditoria.sq_trilha_auditoria;


--
-- TOC entry 239 (class 1259 OID 18263)
-- Name: unidade_conservacao; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.unidade_conservacao (
    sq_pessoa bigint NOT NULL,
    sq_esfera bigint NOT NULL,
    sq_tipo bigint NOT NULL,
    cd_cnuc text,
    ge_uc public.geometry,
    dt_alteracao timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT enforce_geotype_geom CHECK (((public.geometrytype(ge_uc) = 'MULTIPOLYGON'::text) OR (ge_uc IS NULL))),
    CONSTRAINT enforce_srid_geom CHECK ((public.st_srid(ge_uc) = 4674))
);


--
-- TOC entry 8533 (class 0 OID 0)
-- Dependencies: 239
-- Name: COLUMN unidade_conservacao.sq_pessoa; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.unidade_conservacao.sq_pessoa IS 'chave primária sequencial';


--
-- TOC entry 8534 (class 0 OID 0)
-- Dependencies: 239
-- Name: COLUMN unidade_conservacao.sq_esfera; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.unidade_conservacao.sq_esfera IS 'chave estrangeira da tabela dados_apoio';


--
-- TOC entry 8535 (class 0 OID 0)
-- Dependencies: 239
-- Name: COLUMN unidade_conservacao.sq_tipo; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.unidade_conservacao.sq_tipo IS 'chave estrangeira da tabela dados_apoio';


--
-- TOC entry 8536 (class 0 OID 0)
-- Dependencies: 239
-- Name: COLUMN unidade_conservacao.cd_cnuc; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.unidade_conservacao.cd_cnuc IS 'codigo do cadastro nacional de unidades de conservacao - cnuc';


--
-- TOC entry 8537 (class 0 OID 0)
-- Dependencies: 239
-- Name: COLUMN unidade_conservacao.ge_uc; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.unidade_conservacao.ge_uc IS 'polígono da unidade de conservacao';


--
-- TOC entry 8538 (class 0 OID 0)
-- Dependencies: 239
-- Name: COLUMN unidade_conservacao.dt_alteracao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.unidade_conservacao.dt_alteracao IS 'data da última alteração';


--
-- TOC entry 241 (class 1259 OID 18314)
-- Name: user_jobs; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.user_jobs (
    sq_user_jobs bigint NOT NULL,
    sq_pessoa bigint NOT NULL,
    de_rotulo text,
    de_andamento text,
    vl_max integer,
    vl_atual integer,
    js_executar text,
    de_erro text,
    de_mensagem text,
    dt_inclusao timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    dt_inicio timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    dt_fim timestamp without time zone,
    dt_ultima_atualizacao timestamp without time zone,
    id_elemento text,
    de_email text,
    de_email_assunto text,
    no_arquivo_anexo text,
    st_email_enviado boolean,
    st_cancelado boolean DEFAULT false,
    st_executado boolean DEFAULT false
);


--
-- TOC entry 8539 (class 0 OID 0)
-- Dependencies: 241
-- Name: COLUMN user_jobs.sq_user_jobs; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.user_jobs.sq_user_jobs IS 'Chave primaria e sequencial da tabela user_jobs';


--
-- TOC entry 8540 (class 0 OID 0)
-- Dependencies: 241
-- Name: COLUMN user_jobs.sq_pessoa; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.user_jobs.sq_pessoa IS 'Chave estrangeira da tabela pessoa_fisica';


--
-- TOC entry 8541 (class 0 OID 0)
-- Dependencies: 241
-- Name: COLUMN user_jobs.de_rotulo; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.user_jobs.de_rotulo IS 'Descricao do trabalho que esta sendo realizado em segundo plano';


--
-- TOC entry 8542 (class 0 OID 0)
-- Dependencies: 241
-- Name: COLUMN user_jobs.de_andamento; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.user_jobs.de_andamento IS 'Texto do andamento que sera exibida quando o usuario colocar o mouse sobre a tarefa e que sera atualizada durante a execucao da tarefa';


--
-- TOC entry 8543 (class 0 OID 0)
-- Dependencies: 241
-- Name: COLUMN user_jobs.vl_max; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.user_jobs.vl_max IS 'Valor maximo utilizado para calcular o percentual executado';


--
-- TOC entry 8544 (class 0 OID 0)
-- Dependencies: 241
-- Name: COLUMN user_jobs.vl_atual; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.user_jobs.vl_atual IS 'Valor atual utilizado para calcular o percentual executado';


--
-- TOC entry 8545 (class 0 OID 0)
-- Dependencies: 241
-- Name: COLUMN user_jobs.js_executar; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.user_jobs.js_executar IS 'Codigo javascript que sera executado no navegador quando terminar';


--
-- TOC entry 8546 (class 0 OID 0)
-- Dependencies: 241
-- Name: COLUMN user_jobs.de_erro; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.user_jobs.de_erro IS 'Descricao do erro encontrado durante a execucao';


--
-- TOC entry 8547 (class 0 OID 0)
-- Dependencies: 241
-- Name: COLUMN user_jobs.de_mensagem; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.user_jobs.de_mensagem IS 'Mensagem de retorno ou email';


--
-- TOC entry 8548 (class 0 OID 0)
-- Dependencies: 241
-- Name: COLUMN user_jobs.dt_inclusao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.user_jobs.dt_inclusao IS 'Data e hora de criacao da tarefa';


--
-- TOC entry 8549 (class 0 OID 0)
-- Dependencies: 241
-- Name: COLUMN user_jobs.dt_inicio; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.user_jobs.dt_inicio IS 'Data e hora que a tarefa foi iniciada';


--
-- TOC entry 8550 (class 0 OID 0)
-- Dependencies: 241
-- Name: COLUMN user_jobs.dt_fim; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.user_jobs.dt_fim IS 'Data e hora que a tarefa foi finalizada';


--
-- TOC entry 8551 (class 0 OID 0)
-- Dependencies: 241
-- Name: COLUMN user_jobs.dt_ultima_atualizacao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.user_jobs.dt_ultima_atualizacao IS 'Data e hora que a tarefa sofreu a ultima atualizacao';


--
-- TOC entry 8552 (class 0 OID 0)
-- Dependencies: 241
-- Name: COLUMN user_jobs.id_elemento; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.user_jobs.id_elemento IS 'Id do elemento html que devera ser atualizdo com o andamento atual da tarefa';


--
-- TOC entry 8553 (class 0 OID 0)
-- Dependencies: 241
-- Name: COLUMN user_jobs.de_email; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.user_jobs.de_email IS 'Email do usuario para envio do arquivo/mensagem ao finalizar a tarefa';


--
-- TOC entry 8554 (class 0 OID 0)
-- Dependencies: 241
-- Name: COLUMN user_jobs.de_email_assunto; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.user_jobs.de_email_assunto IS 'Assunto do email';


--
-- TOC entry 8555 (class 0 OID 0)
-- Dependencies: 241
-- Name: COLUMN user_jobs.no_arquivo_anexo; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.user_jobs.no_arquivo_anexo IS 'Nome do arquivo que devera ser enviado como anexo para o usuario';


--
-- TOC entry 8556 (class 0 OID 0)
-- Dependencies: 241
-- Name: COLUMN user_jobs.st_email_enviado; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.user_jobs.st_email_enviado IS 'Indica se o e-mail ja foi enviado ou nao';


--
-- TOC entry 8557 (class 0 OID 0)
-- Dependencies: 241
-- Name: COLUMN user_jobs.st_cancelado; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.user_jobs.st_cancelado IS 'Informar se a terefa foi cancelada pelo usuario';


--
-- TOC entry 8558 (class 0 OID 0)
-- Dependencies: 241
-- Name: COLUMN user_jobs.st_executado; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.user_jobs.st_executado IS 'Informar se o job ja esta executado para evitar reprocessamento';


--
-- TOC entry 240 (class 1259 OID 18312)
-- Name: user_jobs_sq_user_jobs_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.user_jobs_sq_user_jobs_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8559 (class 0 OID 0)
-- Dependencies: 240
-- Name: user_jobs_sq_user_jobs_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.user_jobs_sq_user_jobs_seq OWNED BY salve.user_jobs.sq_user_jobs;


--
-- TOC entry 399 (class 1259 OID 26300)
-- Name: uso; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.uso (
    sq_uso integer NOT NULL,
    sq_uso_pai bigint,
    sq_criterio_ameaca_iucn bigint,
    ds_uso text NOT NULL,
    co_uso text,
    nu_ordem text,
    sq_iucn_uso bigint
);


--
-- TOC entry 8560 (class 0 OID 0)
-- Dependencies: 399
-- Name: COLUMN uso.nu_ordem; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.uso.nu_ordem IS 'Ordem de exibição na tela';


--
-- TOC entry 8561 (class 0 OID 0)
-- Dependencies: 399
-- Name: COLUMN uso.sq_iucn_uso; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.uso.sq_iucn_uso IS 'Chave estrangeira da tabela iucn_uso';


--
-- TOC entry 398 (class 1259 OID 26298)
-- Name: uso_sq_uso_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.uso_sq_uso_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8562 (class 0 OID 0)
-- Dependencies: 398
-- Name: uso_sq_uso_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.uso_sq_uso_seq OWNED BY salve.uso.sq_uso;


--
-- TOC entry 232 (class 1259 OID 18195)
-- Name: usuario; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.usuario (
    sq_pessoa bigint NOT NULL,
    de_senha text NOT NULL,
    st_ativo boolean DEFAULT false NOT NULL,
    de_hash text,
    dt_alteracao timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    dt_ultimo_acesso timestamp without time zone,
    no_instituicao text,
    sg_instituicao text
);


--
-- TOC entry 8563 (class 0 OID 0)
-- Dependencies: 232
-- Name: TABLE usuario; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON TABLE salve.usuario IS 'Tabela para armazenar os usuários do sistema';


--
-- TOC entry 8564 (class 0 OID 0)
-- Dependencies: 232
-- Name: COLUMN usuario.sq_pessoa; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.usuario.sq_pessoa IS 'Chave primária e estrangeira da tabela pessoa física';


--
-- TOC entry 8565 (class 0 OID 0)
-- Dependencies: 232
-- Name: COLUMN usuario.de_senha; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.usuario.de_senha IS 'Senha de acesso ao sistema no formato MD5';


--
-- TOC entry 8566 (class 0 OID 0)
-- Dependencies: 232
-- Name: COLUMN usuario.dt_alteracao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.usuario.dt_alteracao IS 'Data da última alteração';


--
-- TOC entry 8567 (class 0 OID 0)
-- Dependencies: 232
-- Name: COLUMN usuario.dt_ultimo_acesso; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.usuario.dt_ultimo_acesso IS 'Data do último acesso ao sistema';


--
-- TOC entry 8568 (class 0 OID 0)
-- Dependencies: 232
-- Name: COLUMN usuario.no_instituicao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.usuario.no_instituicao IS 'Nome da instituicao que o usuario pertence';


--
-- TOC entry 8569 (class 0 OID 0)
-- Dependencies: 232
-- Name: COLUMN usuario.sg_instituicao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.usuario.sg_instituicao IS 'Sigla da instituicao que o usuario pertence';


--
-- TOC entry 236 (class 1259 OID 18226)
-- Name: usuario_perfil; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.usuario_perfil (
    sq_usuario_perfil integer NOT NULL,
    sq_pessoa bigint NOT NULL,
    sq_perfil bigint NOT NULL,
    dt_inclusao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


--
-- TOC entry 8570 (class 0 OID 0)
-- Dependencies: 236
-- Name: TABLE usuario_perfil; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON TABLE salve.usuario_perfil IS 'Tabela para armazenar os perfis de acesso dos usuários';


--
-- TOC entry 8571 (class 0 OID 0)
-- Dependencies: 236
-- Name: COLUMN usuario_perfil.sq_usuario_perfil; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.usuario_perfil.sq_usuario_perfil IS 'Chave primária e sequencial';


--
-- TOC entry 8572 (class 0 OID 0)
-- Dependencies: 236
-- Name: COLUMN usuario_perfil.sq_pessoa; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.usuario_perfil.sq_pessoa IS 'Chave estrangeira da tabela pessoa física';


--
-- TOC entry 8573 (class 0 OID 0)
-- Dependencies: 236
-- Name: COLUMN usuario_perfil.sq_perfil; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.usuario_perfil.sq_perfil IS 'Chave estrangeira da tabela perfil';


--
-- TOC entry 8574 (class 0 OID 0)
-- Dependencies: 236
-- Name: COLUMN usuario_perfil.dt_inclusao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.usuario_perfil.dt_inclusao IS 'Data de inclusão no sistema';


--
-- TOC entry 238 (class 1259 OID 18245)
-- Name: usuario_perfil_instituicao; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.usuario_perfil_instituicao (
    sq_usuario_perfil_instituicao integer NOT NULL,
    sq_usuario_perfil bigint NOT NULL,
    sq_instituicao bigint NOT NULL,
    dt_inclusao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


--
-- TOC entry 8575 (class 0 OID 0)
-- Dependencies: 238
-- Name: COLUMN usuario_perfil_instituicao.sq_usuario_perfil; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.usuario_perfil_instituicao.sq_usuario_perfil IS 'Chave primária e sequencial';


--
-- TOC entry 8576 (class 0 OID 0)
-- Dependencies: 238
-- Name: COLUMN usuario_perfil_instituicao.sq_instituicao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.usuario_perfil_instituicao.sq_instituicao IS 'Chave estrangeira da tabela instituição';


--
-- TOC entry 8577 (class 0 OID 0)
-- Dependencies: 238
-- Name: COLUMN usuario_perfil_instituicao.dt_inclusao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.usuario_perfil_instituicao.dt_inclusao IS 'Data de inclusão no banco de dados';


--
-- TOC entry 237 (class 1259 OID 18243)
-- Name: usuario_perfil_instituicao_sq_usuario_perfil_instituicao_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.usuario_perfil_instituicao_sq_usuario_perfil_instituicao_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8578 (class 0 OID 0)
-- Dependencies: 237
-- Name: usuario_perfil_instituicao_sq_usuario_perfil_instituicao_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.usuario_perfil_instituicao_sq_usuario_perfil_instituicao_seq OWNED BY salve.usuario_perfil_instituicao.sq_usuario_perfil_instituicao;


--
-- TOC entry 235 (class 1259 OID 18224)
-- Name: usuario_perfil_sq_usuario_perfil_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.usuario_perfil_sq_usuario_perfil_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8579 (class 0 OID 0)
-- Dependencies: 235
-- Name: usuario_perfil_sq_usuario_perfil_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.usuario_perfil_sq_usuario_perfil_seq OWNED BY salve.usuario_perfil.sq_usuario_perfil;


--
-- TOC entry 494 (class 1259 OID 28598)
-- Name: validador_ficha; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.validador_ficha (
    sq_validador_ficha integer NOT NULL,
    sq_ciclo_avaliacao_validador bigint NOT NULL,
    sq_oficina_ficha bigint NOT NULL,
    sq_categoria_sugerida bigint,
    de_criterio_sugerido text,
    tx_justificativa text,
    dt_alteracao timestamp without time zone,
    sq_resultado bigint,
    in_comunicar_alteracao boolean DEFAULT false,
    dt_ultimo_comunicado timestamp without time zone,
    st_possivelmente_extinta character varying(1)
);


--
-- TOC entry 8580 (class 0 OID 0)
-- Dependencies: 494
-- Name: COLUMN validador_ficha.dt_alteracao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.validador_ficha.dt_alteracao IS 'Data que o validador alterou o registro';


--
-- TOC entry 8581 (class 0 OID 0)
-- Dependencies: 494
-- Name: COLUMN validador_ficha.sq_resultado; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.validador_ficha.sq_resultado IS 'Resultado a,b ou c selecionado pelo validador';


--
-- TOC entry 8582 (class 0 OID 0)
-- Dependencies: 494
-- Name: COLUMN validador_ficha.in_comunicar_alteracao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.validador_ficha.in_comunicar_alteracao IS 'informar ao validador que houve uma resposta e ele precisa rever a sua validacao';


--
-- TOC entry 8583 (class 0 OID 0)
-- Dependencies: 494
-- Name: COLUMN validador_ficha.dt_ultimo_comunicado; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.validador_ficha.dt_ultimo_comunicado IS 'data de envio do ultimo email comunicando alteracoes na validacao';


--
-- TOC entry 8584 (class 0 OID 0)
-- Dependencies: 494
-- Name: COLUMN validador_ficha.st_possivelmente_extinta; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.validador_ficha.st_possivelmente_extinta IS 'informar se a espécie está possivelmente extinta. S ou N';


--
-- TOC entry 496 (class 1259 OID 28634)
-- Name: validador_ficha_chat; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.validador_ficha_chat (
    sq_validador_ficha_chat integer NOT NULL,
    sq_validador_ficha bigint,
    sq_pessoa bigint NOT NULL,
    dt_mensagem timestamp without time zone NOT NULL,
    de_mensagem text NOT NULL,
    sg_perfil character varying(2)
);


--
-- TOC entry 8585 (class 0 OID 0)
-- Dependencies: 496
-- Name: COLUMN validador_ficha_chat.sg_perfil; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.validador_ficha_chat.sg_perfil IS 'Sigla do perfil do usuário que enviou a mensagem. Ex: PF, AD, VL';


--
-- TOC entry 495 (class 1259 OID 28632)
-- Name: validador_ficha_chat_sq_validador_ficha_chat_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.validador_ficha_chat_sq_validador_ficha_chat_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8586 (class 0 OID 0)
-- Dependencies: 495
-- Name: validador_ficha_chat_sq_validador_ficha_chat_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.validador_ficha_chat_sq_validador_ficha_chat_seq OWNED BY salve.validador_ficha_chat.sq_validador_ficha_chat;


--
-- TOC entry 498 (class 1259 OID 28658)
-- Name: validador_ficha_revisao; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.validador_ficha_revisao (
    sq_validador_ficha_revisao integer NOT NULL,
    sq_validador_ficha bigint NOT NULL,
    sq_resultado bigint NOT NULL,
    sq_categoria_sugerida bigint,
    st_possivelmente_extinta character varying(1),
    tx_justificativa text NOT NULL,
    de_criterio_sugerido text,
    dt_inclusao timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    dt_alteracao timestamp without time zone NOT NULL
);


--
-- TOC entry 8587 (class 0 OID 0)
-- Dependencies: 498
-- Name: TABLE validador_ficha_revisao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON TABLE salve.validador_ficha_revisao IS 'Tabela utilizada quando o validador autoriza a coordenação a alterar a sua resposta ou por algum motivo não pode mais acessar o sistema';


--
-- TOC entry 8588 (class 0 OID 0)
-- Dependencies: 498
-- Name: COLUMN validador_ficha_revisao.sq_validador_ficha_revisao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.validador_ficha_revisao.sq_validador_ficha_revisao IS 'Chave primária sequencial';


--
-- TOC entry 8589 (class 0 OID 0)
-- Dependencies: 498
-- Name: COLUMN validador_ficha_revisao.sq_validador_ficha; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.validador_ficha_revisao.sq_validador_ficha IS 'Chave estrangeira da tabela VALIDOR_FICHA';


--
-- TOC entry 8590 (class 0 OID 0)
-- Dependencies: 498
-- Name: COLUMN validador_ficha_revisao.sq_resultado; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.validador_ficha_revisao.sq_resultado IS 'Chave estrangeira da tabela DADOS_APOIO';


--
-- TOC entry 8591 (class 0 OID 0)
-- Dependencies: 498
-- Name: COLUMN validador_ficha_revisao.sq_categoria_sugerida; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.validador_ficha_revisao.sq_categoria_sugerida IS 'Chave estrangeira da tabela DADOS_APOIO';


--
-- TOC entry 8592 (class 0 OID 0)
-- Dependencies: 498
-- Name: COLUMN validador_ficha_revisao.tx_justificativa; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.validador_ficha_revisao.tx_justificativa IS 'Justificativa da coordenação para interfência na resposta do validador';


--
-- TOC entry 8593 (class 0 OID 0)
-- Dependencies: 498
-- Name: COLUMN validador_ficha_revisao.de_criterio_sugerido; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.validador_ficha_revisao.de_criterio_sugerido IS 'Critério avaliado';


--
-- TOC entry 8594 (class 0 OID 0)
-- Dependencies: 498
-- Name: COLUMN validador_ficha_revisao.dt_inclusao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.validador_ficha_revisao.dt_inclusao IS 'Data de inclusão da revisão';


--
-- TOC entry 8595 (class 0 OID 0)
-- Dependencies: 498
-- Name: COLUMN validador_ficha_revisao.dt_alteracao; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.validador_ficha_revisao.dt_alteracao IS 'Data da última alteracao da revisão';


--
-- TOC entry 497 (class 1259 OID 28656)
-- Name: validador_ficha_revisao_sq_validador_ficha_revisao_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.validador_ficha_revisao_sq_validador_ficha_revisao_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8596 (class 0 OID 0)
-- Dependencies: 497
-- Name: validador_ficha_revisao_sq_validador_ficha_revisao_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.validador_ficha_revisao_sq_validador_ficha_revisao_seq OWNED BY salve.validador_ficha_revisao.sq_validador_ficha_revisao;


--
-- TOC entry 493 (class 1259 OID 28596)
-- Name: validador_ficha_sq_validador_ficha_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.validador_ficha_sq_validador_ficha_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8597 (class 0 OID 0)
-- Dependencies: 493
-- Name: validador_ficha_sq_validador_ficha_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.validador_ficha_sq_validador_ficha_seq OWNED BY salve.validador_ficha.sq_validador_ficha;


--
-- TOC entry 512 (class 1259 OID 29150)
-- Name: vw_ameacas; Type: VIEW; Schema: salve; Owner: -
--

CREATE VIEW salve.vw_ameacas AS
 WITH RECURSIVE arvore(sq_dados_apoio, sq_dados_apoio_pai, cd_dados_apoio, ds_dados_apoio, de_dados_apoio_ordem, cd_sistema, depth, ds_trilha, ds_trilha_csv) AS (
         SELECT da.sq_dados_apoio,
            da.sq_dados_apoio_pai,
            da.cd_dados_apoio,
            da.ds_dados_apoio,
            da.de_dados_apoio_ordem,
            da.cd_sistema,
            0 AS depth,
            da.ds_dados_apoio AS ds_trilha,
            ''::text AS ds_trilha_csv
           FROM salve.dados_apoio da
          WHERE (da.cd_dados_apoio = 'TB_CRITERIO_AMEACA_IUCN'::text)
        UNION ALL
         SELECT dc.sq_dados_apoio,
            dc.sq_dados_apoio_pai,
            dc.cd_dados_apoio,
            dc.ds_dados_apoio,
            dc.de_dados_apoio_ordem,
            dc.cd_sistema,
            (arvore_1.depth + 1),
            (
                CASE
                    WHEN (arvore_1.depth = 0) THEN ''::text
                    ELSE (arvore_1.ds_trilha || '|'::text)
                END || ((dc.cd_dados_apoio || ' - '::text) || dc.ds_dados_apoio)) AS ds_trilha,
            (
                CASE
                    WHEN (arvore_1.depth = 0) THEN ''::text
                    ELSE (arvore_1.ds_trilha_csv || '|'::text)
                END || ((((dc.cd_dados_apoio || ';'::text) || replace(dc.ds_dados_apoio, ';'::text, ','::text)) || ';'::text) || dc.sq_dados_apoio)) AS ds_trilha_csv
           FROM (arvore arvore_1
             JOIN salve.dados_apoio dc ON ((dc.sq_dados_apoio_pai = arvore_1.sq_dados_apoio)))
        )
 SELECT arvore.sq_dados_apoio AS id,
    arvore.sq_dados_apoio_pai AS id_pai,
    arvore.cd_dados_apoio AS codigo,
    arvore.ds_dados_apoio AS descricao,
    ((arvore.cd_dados_apoio || ' - '::text) || arvore.ds_dados_apoio) AS descricao_completa,
    public.fn_remove_acentuacao((arvore.ds_dados_apoio)::character varying) AS descricao_sem_acento,
    arvore.depth AS nivel,
    arvore.de_dados_apoio_ordem AS ordem,
    arvore.ds_trilha AS trilha,
    arvore.ds_trilha_csv AS trilha_csv
   FROM arvore
  WHERE (arvore.depth > 0);


--
-- TOC entry 507 (class 1259 OID 28778)
-- Name: vw_bacia_hidrografica; Type: VIEW; Schema: salve; Owner: -
--

CREATE VIEW salve.vw_bacia_hidrografica AS
 WITH RECURSIVE arvore(sq_dados_apoio, sq_dados_apoio_pai, cd_dados_apoio, ds_dados_apoio, de_dados_apoio_ordem, cd_sistema, depth, ds_trilha) AS (
         SELECT da.sq_dados_apoio,
            da.sq_dados_apoio_pai,
            da.cd_dados_apoio,
            da.ds_dados_apoio,
            da.de_dados_apoio_ordem,
            da.cd_sistema,
            0 AS depth,
            da.ds_dados_apoio AS ds_trilha
           FROM salve.dados_apoio da
          WHERE (da.cd_dados_apoio = 'TB_BACIA_HIDROGRAFICA'::text)
        UNION ALL
         SELECT dc.sq_dados_apoio,
            dc.sq_dados_apoio_pai,
            dc.cd_dados_apoio,
            dc.ds_dados_apoio,
            dc.de_dados_apoio_ordem,
            dc.cd_sistema,
            (arvore_1.depth + 1),
            (
                CASE
                    WHEN (arvore_1.depth = 0) THEN ''::text
                    ELSE (COALESCE(arvore_1.ds_trilha, ''::text) || '|'::text)
                END || ((COALESCE(dc.cd_dados_apoio, ''::text) || ' - '::text) || COALESCE(dc.ds_dados_apoio, ''::text))) AS ds_trilha
           FROM (arvore arvore_1
             JOIN salve.dados_apoio dc ON ((dc.sq_dados_apoio_pai = arvore_1.sq_dados_apoio)))
        )
 SELECT arvore.sq_dados_apoio AS id,
    arvore.sq_dados_apoio_pai AS id_pai,
    arvore.cd_dados_apoio AS codigo,
    arvore.cd_sistema AS codigo_sistema,
    arvore.ds_dados_apoio AS descricao,
    concat(arvore.cd_dados_apoio, ' - ', arvore.ds_dados_apoio) AS descricao_completa,
    public.fn_remove_acentuacao((arvore.ds_dados_apoio)::character varying) AS descricao_sem_acento,
    arvore.depth AS nivel,
    arvore.de_dados_apoio_ordem AS ordem,
    arvore.ds_trilha AS trilha
   FROM arvore
  WHERE (arvore.depth > 0);


--
-- TOC entry 514 (class 1259 OID 29228)
-- Name: vw_efeito_prim; Type: VIEW; Schema: salve; Owner: -
--

CREATE VIEW salve.vw_efeito_prim AS
 WITH RECURSIVE arvore(sq_dados_apoio, sq_dados_apoio_pai, cd_dados_apoio, ds_dados_apoio, de_dados_apoio_ordem, depth, ds_trilha) AS (
         SELECT da.sq_dados_apoio,
            da.sq_dados_apoio_pai,
            da.cd_dados_apoio,
            da.ds_dados_apoio,
            da.de_dados_apoio_ordem,
            0 AS depth,
            da.ds_dados_apoio AS ds_trilha
           FROM salve.dados_apoio da
          WHERE (da.cd_sistema = 'TB_EFEITO_PRIM'::text)
        UNION ALL
         SELECT dc.sq_dados_apoio,
            dc.sq_dados_apoio_pai,
            dc.cd_dados_apoio,
            dc.ds_dados_apoio,
            dc.de_dados_apoio_ordem,
            (arvore_1.depth + 1),
            (((dc.cd_dados_apoio || ' - '::text) || dc.ds_dados_apoio) ||
                CASE
                    WHEN (arvore_1.depth = 0) THEN ''::text
                    ELSE ('|'::text || arvore_1.ds_trilha)
                END) AS ds_trilha
           FROM (arvore arvore_1
             JOIN salve.dados_apoio dc ON ((dc.sq_dados_apoio_pai = arvore_1.sq_dados_apoio)))
        )
 SELECT arvore.sq_dados_apoio AS sq_efeito,
    arvore.sq_dados_apoio_pai AS sq_efeito_pai,
    ((arvore.cd_dados_apoio || ' - '::text) || arvore.ds_dados_apoio) AS ds_efeito,
    arvore.ds_trilha,
    arvore.depth AS nu_nivel,
    arvore.de_dados_apoio_ordem AS de_ordem
   FROM arvore
  WHERE (arvore.depth > 0);


--
-- TOC entry 458 (class 1259 OID 28121)
-- Name: vw_email; Type: VIEW; Schema: salve; Owner: -
--

CREATE VIEW salve.vw_email AS
 SELECT pessoa.sq_pessoa,
    2 AS sq_tipo_email,
    pessoa.de_email AS tx_email
   FROM salve.pessoa;


--
-- TOC entry 8598 (class 0 OID 0)
-- Dependencies: 458
-- Name: VIEW vw_email; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON VIEW salve.vw_email IS 'Visão para manter compatiblidade das queries existentes';


--
-- TOC entry 510 (class 1259 OID 28976)
-- Name: vw_estado; Type: VIEW; Schema: salve; Owner: -
--

CREATE VIEW salve.vw_estado AS
 SELECT estado.sq_estado,
    estado.ge_estado,
    estado.no_estado,
    estado.sq_pais,
    estado.sg_estado,
    estado.sq_regiao
   FROM salve.estado;


--
-- TOC entry 474 (class 1259 OID 28355)
-- Name: vw_ficha; Type: VIEW; Schema: salve; Owner: -
--

CREATE VIEW salve.vw_ficha AS
 SELECT f.sq_ficha,
    f.sq_taxon,
    f.sq_ciclo_avaliacao,
    f.sq_unidade_org,
    f.sq_usuario_alteracao,
    f.sq_situacao_ficha,
    sit.ds_dados_apoio AS ds_situacao_ficha,
    sit.cd_sistema AS cd_situacao_ficha_sistema,
    f.sq_grupo_salve,
    f.sq_grupo,
    f.sq_subgrupo,
    f.sq_categoria_iucn,
    f.ds_criterio_aval_iucn,
    f.st_possivelmente_extinta,
    f.sq_categoria_final,
    f.ds_criterio_aval_iucn_final,
    f.st_possivelmente_extinta_final,
    f.nm_cientifico,
    u.sg_instituicao AS sg_unidade_org,
    initcap(p.no_pessoa) AS no_usuario_alteracao,
    f.dt_alteracao,
    f.dt_publicacao,
    f.sq_usuario_publicacao,
    f.vl_percentual_preenchimento,
    f.st_manter_lc,
    f.st_transferida,
    f.dt_aceite_validacao,
    f.st_localidade_desconhecida,
    nivel.co_nivel_taxonomico,
    nivel.nu_grau_taxonomico,
    count(fp.sq_ficha_pendencia) AS nu_pendencia
   FROM ((((((salve.ficha f
     JOIN salve.instituicao u ON ((u.sq_pessoa = f.sq_unidade_org)))
     JOIN taxonomia.taxon t ON ((t.sq_taxon = f.sq_taxon)))
     JOIN taxonomia.nivel_taxonomico nivel ON ((nivel.sq_nivel_taxonomico = t.sq_nivel_taxonomico)))
     JOIN salve.dados_apoio sit ON ((sit.sq_dados_apoio = f.sq_situacao_ficha)))
     LEFT JOIN salve.ficha_pendencia fp ON (((fp.sq_ficha = f.sq_ficha) AND (fp.st_pendente = 'S'::bpchar))))
     LEFT JOIN salve.pessoa p ON ((p.sq_pessoa = f.sq_usuario_alteracao)))
  GROUP BY f.sq_ficha, f.sq_taxon, f.sq_ciclo_avaliacao, f.sq_unidade_org, f.sq_usuario_alteracao, f.sq_situacao_ficha, sit.ds_dados_apoio, sit.cd_sistema, f.sq_grupo_salve, f.sq_grupo, f.sq_subgrupo, f.sq_categoria_iucn, f.ds_criterio_aval_iucn, f.st_possivelmente_extinta, f.sq_categoria_final, f.ds_criterio_aval_iucn_final, f.st_possivelmente_extinta_final, f.nm_cientifico, u.sg_instituicao, p.no_pessoa, f.dt_alteracao, f.dt_publicacao, f.sq_usuario_publicacao, f.vl_percentual_preenchimento, f.st_manter_lc, f.st_transferida, f.dt_aceite_validacao, f.st_localidade_desconhecida, nivel.co_nivel_taxonomico, nivel.nu_grau_taxonomico;


--
-- TOC entry 528 (class 1259 OID 29725)
-- Name: vw_oficina_participante; Type: VIEW; Schema: salve; Owner: -
--

CREATE VIEW salve.vw_oficina_participante AS
 SELECT oficina_participante.sq_oficina_participante,
    oficina_participante.sq_oficina,
    oficina_participante.sq_pessoa,
    oficina_participante.sq_situacao,
    oficina_participante.sq_instituicao,
    oficina_participante.de_email,
    oficina_participante.dt_email,
    oficina_participante.de_assunto_email,
    situacao.ds_dados_apoio AS de_situacao,
    situacao.cd_sistema AS cd_situacao_sistema,
    pessoa.no_pessoa,
    pessoa_instituicao.no_pessoa AS no_instituicao,
    instituicao.sg_instituicao,
    oficina_anexo_assinatura.dt_email AS dt_email_assinatura,
    oficina_anexo_assinatura.dt_assinatura,
    oficina_anexo_assinatura.de_hash AS de_hash_assinatura
   FROM (((((salve.oficina_participante
     LEFT JOIN salve.oficina_anexo_assinatura ON ((oficina_anexo_assinatura.sq_oficina_participante = oficina_participante.sq_oficina_participante)))
     LEFT JOIN salve.instituicao instituicao ON ((instituicao.sq_pessoa = oficina_participante.sq_instituicao)))
     LEFT JOIN salve.pessoa pessoa_instituicao ON ((pessoa_instituicao.sq_pessoa = oficina_participante.sq_instituicao)))
     LEFT JOIN salve.pessoa pessoa ON ((pessoa.sq_pessoa = oficina_participante.sq_pessoa)))
     LEFT JOIN salve.dados_apoio situacao ON ((situacao.sq_dados_apoio = oficina_participante.sq_situacao)));


--
-- TOC entry 463 (class 1259 OID 28222)
-- Name: vw_pessoa; Type: VIEW; Schema: salve; Owner: -
--

CREATE VIEW salve.vw_pessoa AS
 SELECT pessoa.sq_pessoa,
    pessoa.no_pessoa
   FROM salve.pessoa;


--
-- TOC entry 8599 (class 0 OID 0)
-- Dependencies: 463
-- Name: VIEW vw_pessoa; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON VIEW salve.vw_pessoa IS 'Visão da tabela pessoa para manter compatibilidade com queries existentes';


--
-- TOC entry 442 (class 1259 OID 27840)
-- Name: vw_pessoa_fisica; Type: VIEW; Schema: salve; Owner: -
--

CREATE VIEW salve.vw_pessoa_fisica AS
 SELECT pessoa.sq_pessoa,
    pessoa.no_pessoa,
    pf.nu_cpf
   FROM (salve.pessoa
     JOIN salve.pessoa_fisica pf ON ((pf.sq_pessoa = pessoa.sq_pessoa)));


--
-- TOC entry 8600 (class 0 OID 0)
-- Dependencies: 442
-- Name: VIEW vw_pessoa_fisica; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON VIEW salve.vw_pessoa_fisica IS 'Visão tabela pessoa física';


--
-- TOC entry 511 (class 1259 OID 29000)
-- Name: vw_uc; Type: VIEW; Schema: salve; Owner: -
--

CREATE VIEW salve.vw_uc AS
 SELECT uc.sq_pessoa AS sq_uc,
    uc.sq_pessoa AS sq_controle,
    pessoa.no_pessoa AS no_uc,
    instituicao.sg_instituicao AS sg_uc,
    instituicao.sg_instituicao AS sg_unidade,
    uc.sq_esfera,
    esfera.cd_dados_apoio AS cd_esfera,
    esfera.cd_dados_apoio AS cd_esfera_sistema,
    esfera.cd_dados_apoio AS in_esfera,
    uc.cd_cnuc AS co_cnuc
   FROM (((salve.unidade_conservacao uc
     JOIN salve.instituicao ON ((instituicao.sq_pessoa = uc.sq_pessoa)))
     JOIN salve.pessoa ON ((pessoa.sq_pessoa = uc.sq_pessoa)))
     JOIN salve.dados_apoio esfera ON ((esfera.sq_dados_apoio = uc.sq_esfera)));


--
-- TOC entry 468 (class 1259 OID 28285)
-- Name: vw_unidade_org; Type: VIEW; Schema: salve; Owner: -
--

CREATE VIEW salve.vw_unidade_org AS
 SELECT instituicao.sq_pessoa,
    instituicao.sq_pessoa_pai,
    instituicao.sq_tipo,
    instituicao.sg_instituicao,
    instituicao.sg_instituicao AS sg_unidade_org,
    instituicao.nu_cnpj,
    instituicao.no_contato,
    instituicao.nu_telefone,
    instituicao.dt_alteracao,
    pessoa.no_pessoa
   FROM (salve.instituicao
     JOIN salve.pessoa ON ((pessoa.sq_pessoa = instituicao.sq_pessoa)))
  WHERE (instituicao.st_interna = true);


--
-- TOC entry 449 (class 1259 OID 27959)
-- Name: vw_usuario; Type: VIEW; Schema: salve; Owner: -
--

CREATE VIEW salve.vw_usuario AS
 SELECT usuario.sq_pessoa,
    pessoa.no_pessoa,
    usuario.st_ativo,
    usuario.de_hash,
    usuario.de_senha,
    usuario.dt_ultimo_acesso
   FROM (salve.usuario
     JOIN salve.pessoa ON ((pessoa.sq_pessoa = usuario.sq_pessoa)));


--
-- TOC entry 462 (class 1259 OID 28202)
-- Name: vw_usuario_externo; Type: VIEW; Schema: salve; Owner: -
--

CREATE VIEW salve.vw_usuario_externo AS
 SELECT pessoa.sq_pessoa,
    pessoa.no_pessoa,
    pessoa.de_email,
    pessoa.de_email AS tx_email,
    pf.nu_cpf,
    pf.sq_instituicao,
    instituicao.sg_instituicao,
    instituicao.nu_cnpj,
    usuario.st_ativo
   FROM (((salve.usuario
     JOIN salve.pessoa ON ((usuario.sq_pessoa = pessoa.sq_pessoa)))
     JOIN salve.pessoa_fisica pf ON ((pf.sq_pessoa = usuario.sq_pessoa)))
     JOIN salve.instituicao ON ((instituicao.sq_pessoa = pf.sq_instituicao)))
  WHERE (instituicao.st_interna = false);


--
-- TOC entry 500 (class 1259 OID 28687)
-- Name: web_instituicao; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.web_instituicao (
    sq_web_instituicao integer NOT NULL,
    no_instituicao text NOT NULL,
    sq_unidade_org bigint,
    sg_instituicao text,
    dt_inclusao timestamp without time zone NOT NULL
);


--
-- TOC entry 499 (class 1259 OID 28685)
-- Name: web_instituicao_sq_web_instituicao_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.web_instituicao_sq_web_instituicao_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8601 (class 0 OID 0)
-- Dependencies: 499
-- Name: web_instituicao_sq_web_instituicao_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.web_instituicao_sq_web_instituicao_seq OWNED BY salve.web_instituicao.sq_web_instituicao;


--
-- TOC entry 502 (class 1259 OID 28703)
-- Name: web_role; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.web_role (
    sq_web_role integer NOT NULL,
    no_role text NOT NULL
);


--
-- TOC entry 501 (class 1259 OID 28701)
-- Name: web_role_sq_web_role_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.web_role_sq_web_role_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8602 (class 0 OID 0)
-- Dependencies: 501
-- Name: web_role_sq_web_role_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.web_role_sq_web_role_seq OWNED BY salve.web_role.sq_web_role;


--
-- TOC entry 504 (class 1259 OID 28731)
-- Name: web_usuario; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.web_usuario (
    sq_web_usuario integer NOT NULL,
    sq_web_instituicao bigint,
    no_usuario text NOT NULL,
    de_email text NOT NULL,
    de_senha text NOT NULL,
    de_hash_ativacao text,
    st_conta_bloqueada boolean DEFAULT false NOT NULL,
    st_conta_expirada boolean DEFAULT false NOT NULL,
    st_senha_expirada boolean DEFAULT false NOT NULL,
    st_conta_ativada boolean DEFAULT false NOT NULL,
    dt_ultimo_acesso timestamp without time zone,
    dt_inclusao timestamp without time zone NOT NULL,
    nu_tentativas integer
);


--
-- TOC entry 8603 (class 0 OID 0)
-- Dependencies: 504
-- Name: COLUMN web_usuario.nu_tentativas; Type: COMMENT; Schema: salve; Owner: -
--

COMMENT ON COLUMN salve.web_usuario.nu_tentativas IS 'Número de tentativas de acesso com erro de senha ou usuário';


--
-- TOC entry 506 (class 1259 OID 28754)
-- Name: web_usuario_role; Type: TABLE; Schema: salve; Owner: -
--

CREATE TABLE salve.web_usuario_role (
    sq_web_usuario_role integer NOT NULL,
    sq_web_usuario bigint,
    sq_web_role bigint NOT NULL,
    dt_inclusao timestamp without time zone NOT NULL
);


--
-- TOC entry 505 (class 1259 OID 28752)
-- Name: web_usuario_role_sq_web_usuario_role_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.web_usuario_role_sq_web_usuario_role_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8604 (class 0 OID 0)
-- Dependencies: 505
-- Name: web_usuario_role_sq_web_usuario_role_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.web_usuario_role_sq_web_usuario_role_seq OWNED BY salve.web_usuario_role.sq_web_usuario_role;


--
-- TOC entry 503 (class 1259 OID 28729)
-- Name: web_usuario_sq_web_usuario_seq; Type: SEQUENCE; Schema: salve; Owner: -
--

CREATE SEQUENCE salve.web_usuario_sq_web_usuario_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8605 (class 0 OID 0)
-- Dependencies: 503
-- Name: web_usuario_sq_web_usuario_seq; Type: SEQUENCE OWNED BY; Schema: salve; Owner: -
--

ALTER SEQUENCE salve.web_usuario_sq_web_usuario_seq OWNED BY salve.web_usuario.sq_web_usuario;


--
-- TOC entry 264 (class 1259 OID 22945)
-- Name: grupo; Type: TABLE; Schema: taxonomia; Owner: -
--

CREATE TABLE taxonomia.grupo (
    sq_grupo integer NOT NULL,
    de_grupo text NOT NULL,
    co_grupo text
);


--
-- TOC entry 8606 (class 0 OID 0)
-- Dependencies: 264
-- Name: TABLE grupo; Type: COMMENT; Schema: taxonomia; Owner: -
--

COMMENT ON TABLE taxonomia.grupo IS 'Identificar o grupo em que a situação se aplica.  Ex:
a) "HISTORICO" para Situações ref. aos históricos
b) "NOME" para Situações ref. ao nomes dos Taxons
';


--
-- TOC entry 263 (class 1259 OID 22943)
-- Name: grupo_sq_grupo_seq; Type: SEQUENCE; Schema: taxonomia; Owner: -
--

CREATE SEQUENCE taxonomia.grupo_sq_grupo_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8607 (class 0 OID 0)
-- Dependencies: 263
-- Name: grupo_sq_grupo_seq; Type: SEQUENCE OWNED BY; Schema: taxonomia; Owner: -
--

ALTER SEQUENCE taxonomia.grupo_sq_grupo_seq OWNED BY taxonomia.grupo.sq_grupo;


--
-- TOC entry 509 (class 1259 OID 28961)
-- Name: publicacao_arquivo; Type: TABLE; Schema: taxonomia; Owner: -
--

CREATE TABLE taxonomia.publicacao_arquivo (
    sq_publicacao_arquivo integer NOT NULL,
    sq_publicacao integer NOT NULL,
    no_publicacao_arquivo text,
    de_tipo_conteudo text NOT NULL,
    de_local_arquivo text NOT NULL
);


--
-- TOC entry 8608 (class 0 OID 0)
-- Dependencies: 509
-- Name: COLUMN publicacao_arquivo.no_publicacao_arquivo; Type: COMMENT; Schema: taxonomia; Owner: -
--

COMMENT ON COLUMN taxonomia.publicacao_arquivo.no_publicacao_arquivo IS 'Nome original do arquivo adicionado pelo usuário';


--
-- TOC entry 8609 (class 0 OID 0)
-- Dependencies: 509
-- Name: COLUMN publicacao_arquivo.de_tipo_conteudo; Type: COMMENT; Schema: taxonomia; Owner: -
--

COMMENT ON COLUMN taxonomia.publicacao_arquivo.de_tipo_conteudo IS 'Tipo do conteúdo ( mime type) para informar ao navegador para exibir o preview';


--
-- TOC entry 8610 (class 0 OID 0)
-- Dependencies: 509
-- Name: COLUMN publicacao_arquivo.de_local_arquivo; Type: COMMENT; Schema: taxonomia; Owner: -
--

COMMENT ON COLUMN taxonomia.publicacao_arquivo.de_local_arquivo IS 'Local  de armazenamento no disco';


--
-- TOC entry 508 (class 1259 OID 28959)
-- Name: publicacao_arquivo_sq_publicacao_arquivo_seq; Type: SEQUENCE; Schema: taxonomia; Owner: -
--

CREATE SEQUENCE taxonomia.publicacao_arquivo_sq_publicacao_arquivo_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8611 (class 0 OID 0)
-- Dependencies: 508
-- Name: publicacao_arquivo_sq_publicacao_arquivo_seq; Type: SEQUENCE OWNED BY; Schema: taxonomia; Owner: -
--

ALTER SEQUENCE taxonomia.publicacao_arquivo_sq_publicacao_arquivo_seq OWNED BY taxonomia.publicacao_arquivo.sq_publicacao_arquivo;


--
-- TOC entry 323 (class 1259 OID 24862)
-- Name: publicacao_sq_publicacao_seq; Type: SEQUENCE; Schema: taxonomia; Owner: -
--

CREATE SEQUENCE taxonomia.publicacao_sq_publicacao_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8612 (class 0 OID 0)
-- Dependencies: 323
-- Name: publicacao_sq_publicacao_seq; Type: SEQUENCE OWNED BY; Schema: taxonomia; Owner: -
--

ALTER SEQUENCE taxonomia.publicacao_sq_publicacao_seq OWNED BY taxonomia.publicacao.sq_publicacao;


--
-- TOC entry 266 (class 1259 OID 22956)
-- Name: situacao; Type: TABLE; Schema: taxonomia; Owner: -
--

CREATE TABLE taxonomia.situacao (
    sq_situacao integer NOT NULL,
    sq_grupo bigint NOT NULL,
    de_situacao text NOT NULL,
    co_situacao text
);


--
-- TOC entry 8613 (class 0 OID 0)
-- Dependencies: 266
-- Name: TABLE situacao; Type: COMMENT; Schema: taxonomia; Owner: -
--

COMMENT ON TABLE taxonomia.situacao IS 'Registra a situação do registro. Ex:
a) Inclusão
b) Alteração no nome do táxon
c) Desativação
d) Reclassificação
e) Cadastramento anterior
f) Cadastro equivalente
g) Alteração de outros dados
h) Reativação
i) Sinonímia
j) Alteração nos dados de autoria da descrição (autor/ano)
etc...';


--
-- TOC entry 265 (class 1259 OID 22954)
-- Name: situacao_sq_situacao_seq; Type: SEQUENCE; Schema: taxonomia; Owner: -
--

CREATE SEQUENCE taxonomia.situacao_sq_situacao_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8614 (class 0 OID 0)
-- Dependencies: 265
-- Name: situacao_sq_situacao_seq; Type: SEQUENCE OWNED BY; Schema: taxonomia; Owner: -
--

ALTER SEQUENCE taxonomia.situacao_sq_situacao_seq OWNED BY taxonomia.situacao.sq_situacao;


--
-- TOC entry 267 (class 1259 OID 22970)
-- Name: taxon_sq_taxon_seq; Type: SEQUENCE; Schema: taxonomia; Owner: -
--

CREATE SEQUENCE taxonomia.taxon_sq_taxon_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8615 (class 0 OID 0)
-- Dependencies: 267
-- Name: taxon_sq_taxon_seq; Type: SEQUENCE OWNED BY; Schema: taxonomia; Owner: -
--

ALTER SEQUENCE taxonomia.taxon_sq_taxon_seq OWNED BY taxonomia.taxon.sq_taxon;


--
-- TOC entry 322 (class 1259 OID 24848)
-- Name: tipo_publicacao; Type: TABLE; Schema: taxonomia; Owner: -
--

CREATE TABLE taxonomia.tipo_publicacao (
    sq_tipo_publicacao integer NOT NULL,
    sq_tipo_uso bigint NOT NULL,
    de_tipo_publicacao text NOT NULL,
    co_tipo_publicacao text
);


--
-- TOC entry 8616 (class 0 OID 0)
-- Dependencies: 322
-- Name: TABLE tipo_publicacao; Type: COMMENT; Schema: taxonomia; Owner: -
--

COMMENT ON TABLE taxonomia.tipo_publicacao IS 'Tipos de Publicação:
1) Artigo
2) Revista
3) Jornal 
etc...';


--
-- TOC entry 8617 (class 0 OID 0)
-- Dependencies: 322
-- Name: COLUMN tipo_publicacao.co_tipo_publicacao; Type: COMMENT; Schema: taxonomia; Owner: -
--

COMMENT ON COLUMN taxonomia.tipo_publicacao.co_tipo_publicacao IS 'código único do tipo de publicação para uso interno nas regras de negócio';


--
-- TOC entry 321 (class 1259 OID 24846)
-- Name: tipo_publicacao_sq_tipo_publicacao_seq; Type: SEQUENCE; Schema: taxonomia; Owner: -
--

CREATE SEQUENCE taxonomia.tipo_publicacao_sq_tipo_publicacao_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8618 (class 0 OID 0)
-- Dependencies: 321
-- Name: tipo_publicacao_sq_tipo_publicacao_seq; Type: SEQUENCE OWNED BY; Schema: taxonomia; Owner: -
--

ALTER SEQUENCE taxonomia.tipo_publicacao_sq_tipo_publicacao_seq OWNED BY taxonomia.tipo_publicacao.sq_tipo_publicacao;


--
-- TOC entry 320 (class 1259 OID 24821)
-- Name: tipo_uso; Type: TABLE; Schema: taxonomia; Owner: -
--

CREATE TABLE taxonomia.tipo_uso (
    sq_tipo_uso integer NOT NULL,
    de_tipo_uso text,
    co_tipo_uso text
);


--
-- TOC entry 8619 (class 0 OID 0)
-- Dependencies: 320
-- Name: TABLE tipo_uso; Type: COMMENT; Schema: taxonomia; Owner: -
--

COMMENT ON TABLE taxonomia.tipo_uso IS 'Exemplo de tipos de uso:
1) Científico
2) Não Científico
etc...';


--
-- TOC entry 319 (class 1259 OID 24819)
-- Name: tipo_uso_sq_tipo_uso_seq; Type: SEQUENCE; Schema: taxonomia; Owner: -
--

CREATE SEQUENCE taxonomia.tipo_uso_sq_tipo_uso_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 8620 (class 0 OID 0)
-- Dependencies: 319
-- Name: tipo_uso_sq_tipo_uso_seq; Type: SEQUENCE OWNED BY; Schema: taxonomia; Owner: -
--

ALTER SEQUENCE taxonomia.tipo_uso_sq_tipo_uso_seq OWNED BY taxonomia.tipo_uso.sq_tipo_uso;


--
-- TOC entry 6202 (class 2604 OID 22769)
-- Name: abrangencia sq_abrangencia; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.abrangencia ALTER COLUMN sq_abrangencia SET DEFAULT nextval('salve.abrangencia_sq_abrangencia_seq'::regclass);


--
-- TOC entry 6193 (class 2604 OID 20781)
-- Name: bioma sq_bioma; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.bioma ALTER COLUMN sq_bioma SET DEFAULT nextval('salve.bioma_sq_bioma_seq'::regclass);


--
-- TOC entry 6238 (class 2604 OID 23432)
-- Name: caverna sq_caverna; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.caverna ALTER COLUMN sq_caverna SET DEFAULT nextval('salve.caverna_sq_caverna_seq'::regclass);


--
-- TOC entry 6199 (class 2604 OID 22745)
-- Name: ciclo_avaliacao sq_ciclo_avaliacao; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ciclo_avaliacao ALTER COLUMN sq_ciclo_avaliacao SET DEFAULT nextval('salve.ciclo_avaliacao_sq_ciclo_avaliacao_seq'::regclass);


--
-- TOC entry 6205 (class 2604 OID 22874)
-- Name: ciclo_avaliacao_validador sq_ciclo_avaliacao_validador; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ciclo_avaliacao_validador ALTER COLUMN sq_ciclo_avaliacao_validador SET DEFAULT nextval('salve.ciclo_avaliacao_validador_sq_ciclo_avaliacao_validador_seq'::regclass);


--
-- TOC entry 6206 (class 2604 OID 22907)
-- Name: ciclo_consulta sq_ciclo_consulta; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ciclo_consulta ALTER COLUMN sq_ciclo_consulta SET DEFAULT nextval('salve.ciclo_consulta_sq_ciclo_consulta_seq'::regclass);


--
-- TOC entry 6232 (class 2604 OID 23293)
-- Name: ciclo_consulta_ficha sq_ciclo_consulta_ficha; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ciclo_consulta_ficha ALTER COLUMN sq_ciclo_consulta_ficha SET DEFAULT nextval('salve.ciclo_consulta_ficha_sq_ciclo_consulta_ficha_seq'::regclass);


--
-- TOC entry 6233 (class 2604 OID 23313)
-- Name: ciclo_consulta_pessoa sq_ciclo_consulta_pessoa; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ciclo_consulta_pessoa ALTER COLUMN sq_ciclo_consulta_pessoa SET DEFAULT nextval('salve.ciclo_consulta_pessoa_sq_ciclo_consulta_pessoa_seq'::regclass);


--
-- TOC entry 6234 (class 2604 OID 23345)
-- Name: ciclo_consulta_pessoa_ficha sq_ciclo_consulta_pessoa_ficha; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ciclo_consulta_pessoa_ficha ALTER COLUMN sq_ciclo_consulta_pessoa_ficha SET DEFAULT nextval('salve.ciclo_consulta_pessoa_ficha_sq_ciclo_consulta_pessoa_ficha_seq'::regclass);


--
-- TOC entry 6164 (class 2604 OID 18115)
-- Name: dados_apoio sq_dados_apoio; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.dados_apoio ALTER COLUMN sq_dados_apoio SET DEFAULT nextval('salve.dados_apoio_sq_dados_apoio_seq'::regclass);


--
-- TOC entry 6165 (class 2604 OID 18136)
-- Name: dados_apoio_geo sq_dados_apoio_geo; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.dados_apoio_geo ALTER COLUMN sq_dados_apoio_geo SET DEFAULT nextval('salve.dados_apoio_geo_sq_dados_apoio_geo_seq'::regclass);


--
-- TOC entry 6235 (class 2604 OID 23365)
-- Name: efeito_ameaca sq_efeito_ameaca; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.efeito_ameaca ALTER COLUMN sq_efeito_ameaca SET DEFAULT nextval('salve.efeito_ameaca_sq_efeito_ameaca_seq'::regclass);


--
-- TOC entry 6201 (class 2604 OID 22758)
-- Name: esfera sq_esfera; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.esfera ALTER COLUMN sq_esfera SET DEFAULT nextval('salve.esfera_sq_esfera_seq'::regclass);


--
-- TOC entry 6149 (class 2604 OID 18029)
-- Name: estado sq_estado; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.estado ALTER COLUMN sq_estado SET DEFAULT nextval('salve.estado_sq_estado_seq'::regclass);


--
-- TOC entry 6217 (class 2604 OID 23022)
-- Name: ficha sq_ficha; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ficha ALTER COLUMN sq_ficha SET DEFAULT nextval('salve.ficha_sq_ficha_seq'::regclass);


--
-- TOC entry 6237 (class 2604 OID 23395)
-- Name: ficha_acao_conservacao sq_ficha_acao_conservacao; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ficha_acao_conservacao ALTER COLUMN sq_ficha_acao_conservacao SET DEFAULT nextval('salve.ficha_acao_conservacao_sq_ficha_acao_conservacao_seq'::regclass);


--
-- TOC entry 6240 (class 2604 OID 23463)
-- Name: ficha_amb_restrito_caverna sq_ficha_amb_restrito_caverna; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ficha_amb_restrito_caverna ALTER COLUMN sq_ficha_amb_restrito_caverna SET DEFAULT nextval('salve.ficha_amb_restrito_caverna_sq_ficha_amb_restrito_caverna_seq'::regclass);


--
-- TOC entry 6239 (class 2604 OID 23441)
-- Name: ficha_ambiente_restrito sq_ficha_ambiente_restrito; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ficha_ambiente_restrito ALTER COLUMN sq_ficha_ambiente_restrito SET DEFAULT nextval('salve.ficha_ambiente_restrito_sq_ficha_ambiente_restrito_seq'::regclass);


--
-- TOC entry 6243 (class 2604 OID 23543)
-- Name: ficha_ameaca sq_ficha_ameaca; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ficha_ameaca ALTER COLUMN sq_ficha_ameaca SET DEFAULT nextval('salve.ficha_ameaca_sq_ficha_ameaca_seq'::regclass);


--
-- TOC entry 6244 (class 2604 OID 23589)
-- Name: ficha_ameaca_efeito sq_ficha_ameaca_efeito; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ficha_ameaca_efeito ALTER COLUMN sq_ficha_ameaca_efeito SET DEFAULT nextval('salve.ficha_ameaca_efeito_sq_ficha_ameaca_efeito_seq'::regclass);


--
-- TOC entry 6249 (class 2604 OID 24129)
-- Name: ficha_ameaca_regiao sq_ficha_ameaca_regiao; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ficha_ameaca_regiao ALTER COLUMN sq_ficha_ameaca_regiao SET DEFAULT nextval('salve.ficha_ameaca_regiao_sq_ficha_ameaca_regiao_seq'::regclass);


--
-- TOC entry 6250 (class 2604 OID 24243)
-- Name: ficha_anexo sq_ficha_anexo; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ficha_anexo ALTER COLUMN sq_ficha_anexo SET DEFAULT nextval('salve.ficha_anexo_sq_ficha_anexo_seq'::regclass);


--
-- TOC entry 6252 (class 2604 OID 24305)
-- Name: ficha_aoo sq_ficha_aoo; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ficha_aoo ALTER COLUMN sq_ficha_aoo SET DEFAULT nextval('salve.ficha_aoo_sq_ficha_aoo_seq'::regclass);


--
-- TOC entry 6255 (class 2604 OID 24369)
-- Name: ficha_area_relevancia sq_ficha_area_relevancia; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ficha_area_relevancia ALTER COLUMN sq_ficha_area_relevancia SET DEFAULT nextval('salve.ficha_area_relevancia_sq_ficha_area_relevancia_seq'::regclass);


--
-- TOC entry 6256 (class 2604 OID 24405)
-- Name: ficha_area_relevancia_municipio sq_ficha_area_relevancia_municipio; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ficha_area_relevancia_municipio ALTER COLUMN sq_ficha_area_relevancia_municipio SET DEFAULT nextval('salve.ficha_area_relevancia_municip_sq_ficha_area_relevancia_muni_seq'::regclass);


--
-- TOC entry 6257 (class 2604 OID 24448)
-- Name: ficha_area_vida sq_ficha_area_vida; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ficha_area_vida ALTER COLUMN sq_ficha_area_vida SET DEFAULT nextval('salve.ficha_area_vida_sq_ficha_area_vida_seq'::regclass);


--
-- TOC entry 6258 (class 2604 OID 24491)
-- Name: ficha_autor sq_ficha_autor; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ficha_autor ALTER COLUMN sq_ficha_autor SET DEFAULT nextval('salve.ficha_autor_sq_ficha_autor_seq'::regclass);


--
-- TOC entry 6259 (class 2604 OID 24539)
-- Name: ficha_avaliacao_regional sq_ficha_avaliacao_regional; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ficha_avaliacao_regional ALTER COLUMN sq_ficha_avaliacao_regional SET DEFAULT nextval('salve.ficha_avaliacao_regional_sq_ficha_avaliacao_regional_seq'::regclass);


--
-- TOC entry 6260 (class 2604 OID 24589)
-- Name: ficha_bacia sq_ficha_bacia; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ficha_bacia ALTER COLUMN sq_ficha_bacia SET DEFAULT nextval('salve.ficha_bacia_sq_ficha_bacia_seq'::regclass);


--
-- TOC entry 6261 (class 2604 OID 24629)
-- Name: ficha_bioma sq_ficha_bioma; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ficha_bioma ALTER COLUMN sq_ficha_bioma SET DEFAULT nextval('salve.ficha_bioma_sq_ficha_bioma_seq'::regclass);


--
-- TOC entry 6262 (class 2604 OID 24734)
-- Name: ficha_colaboracao sq_ficha_colaboracao; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ficha_colaboracao ALTER COLUMN sq_ficha_colaboracao SET DEFAULT nextval('salve.ficha_colaboracao_sq_ficha_colaboracao_seq'::regclass);


--
-- TOC entry 6267 (class 2604 OID 24894)
-- Name: ficha_colaboracao_ocorrencia sq_ficha_colaboracao_ocorrencia; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ficha_colaboracao_ocorrencia ALTER COLUMN sq_ficha_colaboracao_ocorrencia SET DEFAULT nextval('salve.ficha_colaboracao_ocorrencia_sq_ficha_colaboracao_ocorrenci_seq'::regclass);


--
-- TOC entry 6268 (class 2604 OID 24895)
-- Name: ficha_colaboracao_ocorrencia sq_ciclo_consulta_ficha; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ficha_colaboracao_ocorrencia ALTER COLUMN sq_ciclo_consulta_ficha SET DEFAULT nextval('salve.ficha_colaboracao_ocorrencia_sq_ciclo_consulta_ficha_seq'::regclass);


--
-- TOC entry 6271 (class 2604 OID 25053)
-- Name: ficha_consulta_anexo sq_ficha_consulta_anexo; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ficha_consulta_anexo ALTER COLUMN sq_ficha_consulta_anexo SET DEFAULT nextval('salve.ficha_consulta_anexo_sq_ficha_consulta_anexo_seq'::regclass);


--
-- TOC entry 6276 (class 2604 OID 25175)
-- Name: ficha_consulta_foto sq_ficha_consulta_foto; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ficha_consulta_foto ALTER COLUMN sq_ficha_consulta_foto SET DEFAULT nextval('salve.ficha_consulta_foto_sq_ficha_consulta_foto_seq'::regclass);


--
-- TOC entry 6280 (class 2604 OID 25294)
-- Name: ficha_consulta_multimidia sq_ficha_consulta_multimidia; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ficha_consulta_multimidia ALTER COLUMN sq_ficha_consulta_multimidia SET DEFAULT nextval('salve.ficha_consulta_multimidia_sq_ficha_consulta_multimidia_seq'::regclass);


--
-- TOC entry 6281 (class 2604 OID 25317)
-- Name: ficha_declinio_populacional sq_ficha_declinio_populacional; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ficha_declinio_populacional ALTER COLUMN sq_ficha_declinio_populacional SET DEFAULT nextval('salve.ficha_declinio_populacional_sq_ficha_declinio_populacional_seq'::regclass);


--
-- TOC entry 6282 (class 2604 OID 25336)
-- Name: ficha_eoo sq_ficha_eoo; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ficha_eoo ALTER COLUMN sq_ficha_eoo SET DEFAULT nextval('salve.ficha_eoo_sq_ficha_eoo_seq'::regclass);


--
-- TOC entry 6285 (class 2604 OID 25360)
-- Name: ficha_habitat sq_ficha_habitat; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ficha_habitat ALTER COLUMN sq_ficha_habitat SET DEFAULT nextval('salve.ficha_habitat_sq_ficha_habitat_seq'::regclass);


--
-- TOC entry 6286 (class 2604 OID 25381)
-- Name: ficha_habito_alimentar sq_ficha_habito_alimentar; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ficha_habito_alimentar ALTER COLUMN sq_ficha_habito_alimentar SET DEFAULT nextval('salve.ficha_habito_alimentar_sq_ficha_habito_alimentar_seq'::regclass);


--
-- TOC entry 6287 (class 2604 OID 25404)
-- Name: ficha_habito_alimentar_esp sq_ficha_habito_alimentar_esp; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ficha_habito_alimentar_esp ALTER COLUMN sq_ficha_habito_alimentar_esp SET DEFAULT nextval('salve.ficha_habito_alimentar_esp_sq_ficha_habito_alimentar_esp_seq'::regclass);


--
-- TOC entry 6288 (class 2604 OID 25434)
-- Name: ficha_hist_situacao_excluida sq_ficha_hist_situacao_excluida; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ficha_hist_situacao_excluida ALTER COLUMN sq_ficha_hist_situacao_excluida SET DEFAULT nextval('salve.ficha_hist_situacao_excluida_sq_ficha_hist_situacao_excluid_seq'::regclass);


--
-- TOC entry 6290 (class 2604 OID 25454)
-- Name: ficha_historico_avaliacao sq_ficha_historico_avaliacao; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ficha_historico_avaliacao ALTER COLUMN sq_ficha_historico_avaliacao SET DEFAULT nextval('salve.ficha_historico_avaliacao_sq_ficha_historico_avaliacao_seq'::regclass);


--
-- TOC entry 6291 (class 2604 OID 25504)
-- Name: ficha_interacao sq_ficha_interacao; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ficha_interacao ALTER COLUMN sq_ficha_interacao SET DEFAULT nextval('salve.ficha_interacao_sq_ficha_interacao_seq'::regclass);


--
-- TOC entry 6292 (class 2604 OID 25544)
-- Name: ficha_lista_convencao sq_ficha_lista_convencao; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ficha_lista_convencao ALTER COLUMN sq_ficha_lista_convencao SET DEFAULT nextval('salve.ficha_lista_convencao_sq_ficha_lista_convencao_seq'::regclass);


--
-- TOC entry 6293 (class 2604 OID 25595)
-- Name: ficha_modulo_publico sq_ficha_modulo_publico; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ficha_modulo_publico ALTER COLUMN sq_ficha_modulo_publico SET DEFAULT nextval('salve.ficha_modulo_publico_sq_ficha_modulo_publico_seq'::regclass);


--
-- TOC entry 6296 (class 2604 OID 25618)
-- Name: ficha_mudanca_categoria sq_ficha_mudanca_categoria; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ficha_mudanca_categoria ALTER COLUMN sq_ficha_mudanca_categoria SET DEFAULT nextval('salve.ficha_mudanca_categoria_sq_ficha_mudanca_categoria_seq'::regclass);


--
-- TOC entry 6273 (class 2604 OID 25140)
-- Name: ficha_multimidia sq_ficha_multimidia; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ficha_multimidia ALTER COLUMN sq_ficha_multimidia SET DEFAULT nextval('salve.ficha_multimidia_sq_ficha_multimidia_seq'::regclass);


--
-- TOC entry 6297 (class 2604 OID 25639)
-- Name: ficha_nome_comum sq_ficha_nome_comum; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ficha_nome_comum ALTER COLUMN sq_ficha_nome_comum SET DEFAULT nextval('salve.ficha_nome_comum_sq_ficha_nome_comum_seq'::regclass);


--
-- TOC entry 6245 (class 2604 OID 23955)
-- Name: ficha_ocorrencia sq_ficha_ocorrencia; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ficha_ocorrencia ALTER COLUMN sq_ficha_ocorrencia SET DEFAULT nextval('salve.ficha_ocorrencia_sq_ficha_ocorrencia_seq'::regclass);


--
-- TOC entry 6279 (class 2604 OID 25254)
-- Name: ficha_ocorrencia_consulta sq_ficha_ocorrencia_consulta; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ficha_ocorrencia_consulta ALTER COLUMN sq_ficha_ocorrencia_consulta SET DEFAULT nextval('salve.ficha_ocorrencia_consulta_sq_ficha_ocorrencia_consulta_seq'::regclass);


--
-- TOC entry 6298 (class 2604 OID 25661)
-- Name: ficha_ocorrencia_multimidia sq_ficha_ocorrencia_multimidia; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ficha_ocorrencia_multimidia ALTER COLUMN sq_ficha_ocorrencia_multimidia SET DEFAULT nextval('salve.ficha_ocorrencia_multimidia_sq_ficha_ocorrencia_multimidia_seq'::regclass);


--
-- TOC entry 6301 (class 2604 OID 25707)
-- Name: ficha_ocorrencia_portalbio sq_ficha_ocorrencia_portalbio; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ficha_ocorrencia_portalbio ALTER COLUMN sq_ficha_ocorrencia_portalbio SET DEFAULT nextval('salve.ficha_ocorrencia_portalbio_sq_ficha_ocorrencia_portalbio_seq'::regclass);


--
-- TOC entry 6305 (class 2604 OID 25791)
-- Name: ficha_ocorrencia_portalbio_reg sq_ficha_ocorrencia_portalbio_reg; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ficha_ocorrencia_portalbio_reg ALTER COLUMN sq_ficha_ocorrencia_portalbio_reg SET DEFAULT nextval('salve.ficha_ocorrencia_portalbio_re_sq_ficha_ocorrencia_portalbio_seq'::regclass);


--
-- TOC entry 6307 (class 2604 OID 25822)
-- Name: ficha_ocorrencia_registro sq_ficha_ocorrencia_registro; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ficha_ocorrencia_registro ALTER COLUMN sq_ficha_ocorrencia_registro SET DEFAULT nextval('salve.ficha_ocorrencia_registro_sq_ficha_ocorrencia_registro_seq'::regclass);


--
-- TOC entry 6309 (class 2604 OID 25879)
-- Name: ficha_ocorrencia_validacao sq_ficha_ocorrencia_validacao; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ficha_ocorrencia_validacao ALTER COLUMN sq_ficha_ocorrencia_validacao SET DEFAULT nextval('salve.ficha_ocorrencia_validacao_sq_ficha_ocorrencia_validacao_seq'::regclass);


--
-- TOC entry 6311 (class 2604 OID 25928)
-- Name: ficha_pendencia sq_ficha_pendencia; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ficha_pendencia ALTER COLUMN sq_ficha_pendencia SET DEFAULT nextval('salve.ficha_pendencia_sq_ficha_pendencia_seq'::regclass);


--
-- TOC entry 6314 (class 2604 OID 25969)
-- Name: ficha_pendencia_email sq_ficha_pendencia_email; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ficha_pendencia_email ALTER COLUMN sq_ficha_pendencia_email SET DEFAULT nextval('salve.ficha_pendencia_email_sq_ficha_pendencia_email_seq'::regclass);


--
-- TOC entry 6316 (class 2604 OID 25988)
-- Name: ficha_pesquisa sq_ficha_pesquisa; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ficha_pesquisa ALTER COLUMN sq_ficha_pesquisa SET DEFAULT nextval('salve.ficha_pesquisa_sq_ficha_pesquisa_seq'::regclass);


--
-- TOC entry 6317 (class 2604 OID 26045)
-- Name: ficha_pessoa sq_ficha_pessoa; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ficha_pessoa ALTER COLUMN sq_ficha_pessoa SET DEFAULT nextval('salve.ficha_pessoa_sq_ficha_pessoa_seq'::regclass);


--
-- TOC entry 6319 (class 2604 OID 26083)
-- Name: ficha_popul_estimada_local sq_ficha_popul_estimada_local; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ficha_popul_estimada_local ALTER COLUMN sq_ficha_popul_estimada_local SET DEFAULT nextval('salve.ficha_popul_estimada_local_sq_ficha_popul_estimada_local_seq'::regclass);


--
-- TOC entry 6320 (class 2604 OID 26111)
-- Name: ficha_ref_bib sq_ficha_ref_bib; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ficha_ref_bib ALTER COLUMN sq_ficha_ref_bib SET DEFAULT nextval('salve.ficha_ref_bib_sq_ficha_ref_bib_seq'::regclass);


--
-- TOC entry 6321 (class 2604 OID 26135)
-- Name: ficha_ref_bib_tag sq_ficha_ref_bib_tag; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ficha_ref_bib_tag ALTER COLUMN sq_ficha_ref_bib_tag SET DEFAULT nextval('salve.ficha_ref_bib_tag_sq_ficha_ref_bib_tag_seq'::regclass);


--
-- TOC entry 6322 (class 2604 OID 26156)
-- Name: ficha_sincronismo sq_ficha_sincronismo; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ficha_sincronismo ALTER COLUMN sq_ficha_sincronismo SET DEFAULT nextval('salve.ficha_sincronismo_sq_ficha_sincronismo_seq'::regclass);


--
-- TOC entry 6325 (class 2604 OID 26179)
-- Name: ficha_sinonimia sq_ficha_sinonimia; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ficha_sinonimia ALTER COLUMN sq_ficha_sinonimia SET DEFAULT nextval('salve.ficha_sinonimia_sq_ficha_sinonimia_seq'::regclass);


--
-- TOC entry 6326 (class 2604 OID 26196)
-- Name: ficha_uf sq_ficha_uf; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ficha_uf ALTER COLUMN sq_ficha_uf SET DEFAULT nextval('salve.ficha_uf_sq_ficha_uf_seq'::regclass);


--
-- TOC entry 6327 (class 2604 OID 26262)
-- Name: ficha_uso sq_ficha_uso; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ficha_uso ALTER COLUMN sq_ficha_uso SET DEFAULT nextval('salve.ficha_uso_sq_ficha_uso_seq'::regclass);


--
-- TOC entry 6332 (class 2604 OID 26382)
-- Name: ficha_uso_regiao sq_ficha_uso_regiao; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ficha_uso_regiao ALTER COLUMN sq_ficha_uso_regiao SET DEFAULT nextval('salve.ficha_uso_regiao_sq_ficha_uso_regiao_seq'::regclass);


--
-- TOC entry 6333 (class 2604 OID 26435)
-- Name: ficha_variacao_sazonal sq_ficha_variacao_sazonal; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ficha_variacao_sazonal ALTER COLUMN sq_ficha_variacao_sazonal SET DEFAULT nextval('salve.ficha_variacao_sazonal_sq_ficha_variacao_sazonal_seq'::regclass);


--
-- TOC entry 6445 (class 2604 OID 29474)
-- Name: ficha_versao sq_ficha_versao; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ficha_versao ALTER COLUMN sq_ficha_versao SET DEFAULT nextval('salve.ficha_versao_sq_ficha_versao_seq'::regclass);


--
-- TOC entry 6189 (class 2604 OID 18340)
-- Name: informativo sq_informativo; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.informativo ALTER COLUMN sq_informativo SET DEFAULT nextval('salve.informativo_sq_informativo_seq'::regclass);


--
-- TOC entry 6241 (class 2604 OID 23487)
-- Name: informativo_pessoa sq_informativo_pessoa; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.informativo_pessoa ALTER COLUMN sq_informativo_pessoa SET DEFAULT nextval('salve.informativo_pessoa_sq_informativo_pessoa_seq'::regclass);


--
-- TOC entry 6334 (class 2604 OID 26468)
-- Name: iucn_acao_conservacao sq_iucn_acao_conservacao; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.iucn_acao_conservacao ALTER COLUMN sq_iucn_acao_conservacao SET DEFAULT nextval('salve.iucn_acao_conservacao_sq_iucn_acao_conservacao_seq'::regclass);


--
-- TOC entry 6337 (class 2604 OID 26481)
-- Name: iucn_acao_conservacao_apoio sq_iucn_acao_conservacao_apoio; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.iucn_acao_conservacao_apoio ALTER COLUMN sq_iucn_acao_conservacao_apoio SET DEFAULT nextval('salve.iucn_acao_conservacao_apoio_sq_iucn_acao_conservacao_apoio_seq'::regclass);


--
-- TOC entry 6340 (class 2604 OID 26501)
-- Name: iucn_ameaca sq_iucn_ameaca; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.iucn_ameaca ALTER COLUMN sq_iucn_ameaca SET DEFAULT nextval('salve.iucn_ameaca_sq_iucn_ameaca_seq'::regclass);


--
-- TOC entry 6343 (class 2604 OID 26514)
-- Name: iucn_ameaca_apoio sq_iucn_ameaca_apoio; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.iucn_ameaca_apoio ALTER COLUMN sq_iucn_ameaca_apoio SET DEFAULT nextval('salve.iucn_ameaca_apoio_sq_iucn_ameaca_apoio_seq'::regclass);


--
-- TOC entry 6346 (class 2604 OID 26574)
-- Name: iucn_habitat sq_iucn_habitat; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.iucn_habitat ALTER COLUMN sq_iucn_habitat SET DEFAULT nextval('salve.iucn_habitat_sq_iucn_habitat_seq'::regclass);


--
-- TOC entry 6349 (class 2604 OID 26587)
-- Name: iucn_habitat_apoio sq_iucn_habitat_apoio; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.iucn_habitat_apoio ALTER COLUMN sq_iucn_habitat_apoio SET DEFAULT nextval('salve.iucn_habitat_apoio_sq_iucn_habitat_apoio_seq'::regclass);


--
-- TOC entry 6352 (class 2604 OID 26607)
-- Name: iucn_motivo_mudanca sq_iucn_motivo_mudanca; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.iucn_motivo_mudanca ALTER COLUMN sq_iucn_motivo_mudanca SET DEFAULT nextval('salve.iucn_motivo_mudanca_sq_iucn_motivo_mudanca_seq'::regclass);


--
-- TOC entry 6355 (class 2604 OID 26620)
-- Name: iucn_motivo_mudanca_apoio sq_iucn_motivo_mudanca_apoio; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.iucn_motivo_mudanca_apoio ALTER COLUMN sq_iucn_motivo_mudanca_apoio SET DEFAULT nextval('salve.iucn_motivo_mudanca_apoio_sq_iucn_motivo_mudanca_apoio_seq'::regclass);


--
-- TOC entry 6358 (class 2604 OID 26640)
-- Name: iucn_pesquisa sq_iucn_pesquisa; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.iucn_pesquisa ALTER COLUMN sq_iucn_pesquisa SET DEFAULT nextval('salve.iucn_pesquisa_sq_iucn_pesquisa_seq'::regclass);


--
-- TOC entry 6361 (class 2604 OID 26653)
-- Name: iucn_pesquisa_apoio sq_iucn_pesquisa_apoio; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.iucn_pesquisa_apoio ALTER COLUMN sq_iucn_pesquisa_apoio SET DEFAULT nextval('salve.iucn_pesquisa_apoio_sq_iucn_pesquisa_apoio_seq'::regclass);


--
-- TOC entry 6364 (class 2604 OID 26685)
-- Name: iucn_transmissao sq_iucn_transmissao; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.iucn_transmissao ALTER COLUMN sq_iucn_transmissao SET DEFAULT nextval('salve.iucn_transmissao_sq_iucn_transmissao_seq'::regclass);


--
-- TOC entry 6366 (class 2604 OID 26702)
-- Name: iucn_transmissao_ficha sq_iucn_transmissao_ficha; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.iucn_transmissao_ficha ALTER COLUMN sq_iucn_transmissao_ficha SET DEFAULT nextval('salve.iucn_transmissao_ficha_sq_iucn_transmissao_ficha_seq'::regclass);


--
-- TOC entry 6328 (class 2604 OID 26290)
-- Name: iucn_uso sq_iucn_uso; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.iucn_uso ALTER COLUMN sq_iucn_uso SET DEFAULT nextval('salve.iucn_uso_sq_iucn_uso_seq'::regclass);


--
-- TOC entry 6155 (class 2604 OID 18058)
-- Name: municipio sq_municipio; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.municipio ALTER COLUMN sq_municipio SET DEFAULT nextval('salve.municipio_sq_municipio_seq'::regclass);


--
-- TOC entry 6449 (class 2604 OID 29500)
-- Name: ocorrencias_json sq_ocorrencias_json; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.ocorrencias_json ALTER COLUMN sq_ocorrencias_json SET DEFAULT nextval('salve.ocorrencias_json_sq_ocorrencias_json_seq'::regclass);


--
-- TOC entry 6203 (class 2604 OID 22835)
-- Name: oficina sq_oficina; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.oficina ALTER COLUMN sq_oficina SET DEFAULT nextval('salve.oficina_sq_oficina_seq'::regclass);


--
-- TOC entry 6367 (class 2604 OID 26728)
-- Name: oficina_anexo sq_oficina_anexo; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.oficina_anexo ALTER COLUMN sq_oficina_anexo SET DEFAULT nextval('salve.oficina_anexo_sq_oficina_anexo_seq'::regclass);


--
-- TOC entry 6371 (class 2604 OID 26930)
-- Name: oficina_anexo_assinatura sq_oficina_anexo_assinatura; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.oficina_anexo_assinatura ALTER COLUMN sq_oficina_anexo_assinatura SET DEFAULT nextval('salve.oficina_anexo_assinatura_sq_oficina_anexo_assinatura_seq'::regclass);


--
-- TOC entry 6372 (class 2604 OID 27031)
-- Name: oficina_ficha sq_oficina_ficha; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.oficina_ficha ALTER COLUMN sq_oficina_ficha SET DEFAULT nextval('salve.oficina_ficha_sq_oficina_ficha_seq'::regclass);


--
-- TOC entry 6377 (class 2604 OID 27075)
-- Name: oficina_ficha_chat sq_oficina_ficha_chat; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.oficina_ficha_chat ALTER COLUMN sq_oficina_ficha_chat SET DEFAULT nextval('salve.oficina_ficha_chat_sq_oficina_ficha_chat_seq'::regclass);


--
-- TOC entry 6378 (class 2604 OID 27100)
-- Name: oficina_ficha_participante sq_oficina_ficha_participante; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.oficina_ficha_participante ALTER COLUMN sq_oficina_ficha_participante SET DEFAULT nextval('salve.oficina_ficha_participante_sq_oficina_ficha_participante_seq'::regclass);


--
-- TOC entry 6379 (class 2604 OID 27130)
-- Name: oficina_memoria sq_oficina_memoria; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.oficina_memoria ALTER COLUMN sq_oficina_memoria SET DEFAULT nextval('salve.oficina_memoria_sq_oficina_memoria_seq'::regclass);


--
-- TOC entry 6380 (class 2604 OID 27866)
-- Name: oficina_memoria_anexo sq_oficina_memoria_anexo; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.oficina_memoria_anexo ALTER COLUMN sq_oficina_memoria_anexo SET DEFAULT nextval('salve.oficina_memoria_anexo_sq_oficina_memoria_anexo_seq'::regclass);


--
-- TOC entry 6382 (class 2604 OID 27901)
-- Name: oficina_memoria_participante sq_oficina_memoria_participante; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.oficina_memoria_participante ALTER COLUMN sq_oficina_memoria_participante SET DEFAULT nextval('salve.oficina_memoria_participante_sq_oficina_memoria_participant_seq'::regclass);


--
-- TOC entry 6370 (class 2604 OID 26896)
-- Name: oficina_participante sq_oficina_participante; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.oficina_participante ALTER COLUMN sq_oficina_participante SET DEFAULT nextval('salve.oficina_participante_sq_oficina_participante_seq'::regclass);


--
-- TOC entry 6137 (class 2604 OID 17993)
-- Name: pais sq_pais; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.pais ALTER COLUMN sq_pais SET DEFAULT nextval('salve.pais_sq_pais_seq'::regclass);


--
-- TOC entry 6174 (class 2604 OID 18215)
-- Name: perfil sq_perfil; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.perfil ALTER COLUMN sq_perfil SET DEFAULT nextval('salve.perfil_sq_perfil_seq'::regclass);


--
-- TOC entry 6161 (class 2604 OID 18100)
-- Name: pessoa sq_pessoa; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.pessoa ALTER COLUMN sq_pessoa SET DEFAULT nextval('salve.pessoa_sq_pessoa_seq'::regclass);


--
-- TOC entry 6383 (class 2604 OID 27921)
-- Name: planilha sq_planilha; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.planilha ALTER COLUMN sq_planilha SET DEFAULT nextval('salve.planilha_sq_planilha_seq'::regclass);


--
-- TOC entry 6385 (class 2604 OID 27968)
-- Name: planilha_linha sq_planilha_linha; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.planilha_linha ALTER COLUMN sq_planilha_linha SET DEFAULT nextval('salve.planilha_linha_sq_planilha_linha_seq'::regclass);


--
-- TOC entry 6236 (class 2604 OID 23384)
-- Name: plano_acao sq_plano_acao; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.plano_acao ALTER COLUMN sq_plano_acao SET DEFAULT nextval('salve.plano_acao_sq_plano_acao_seq'::regclass);


--
-- TOC entry 6143 (class 2604 OID 18012)
-- Name: regiao sq_regiao; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.regiao ALTER COLUMN sq_regiao SET DEFAULT nextval('salve.regiao_sq_regiao_seq'::regclass);


--
-- TOC entry 6303 (class 2604 OID 25776)
-- Name: registro sq_registro; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.registro ALTER COLUMN sq_registro SET DEFAULT nextval('salve.registro_sq_registro_seq'::regclass);


--
-- TOC entry 6386 (class 2604 OID 28035)
-- Name: registro_bacia sq_registro_bacia; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.registro_bacia ALTER COLUMN sq_registro_bacia SET DEFAULT nextval('salve.registro_bacia_sq_registro_bacia_seq'::regclass);


--
-- TOC entry 6394 (class 2604 OID 28265)
-- Name: registro_terra_indigena sq_registro_terra_indigena; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.registro_terra_indigena ALTER COLUMN sq_registro_terra_indigena SET DEFAULT nextval('salve.registro_terra_indigena_sq_registro_terra_indigena_seq'::regclass);


--
-- TOC entry 6397 (class 2604 OID 28295)
-- Name: registro_uc_estadual sq_registro_uc_estadual; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.registro_uc_estadual ALTER COLUMN sq_registro_uc_estadual SET DEFAULT nextval('salve.registro_uc_estadual_sq_registro_uc_estadual_seq'::regclass);


--
-- TOC entry 6398 (class 2604 OID 28322)
-- Name: registro_uc_federal sq_registro_uc_federal; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.registro_uc_federal ALTER COLUMN sq_registro_uc_federal SET DEFAULT nextval('salve.registro_uc_federal_sq_registro_uc_federal_seq'::regclass);


--
-- TOC entry 6403 (class 2604 OID 28372)
-- Name: taxon_historico_avaliacao sq_taxon_historico_avaliacao; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.taxon_historico_avaliacao ALTER COLUMN sq_taxon_historico_avaliacao SET DEFAULT nextval('salve.taxon_historico_avaliacao_sq_taxon_historico_avaliacao_seq'::regclass);


--
-- TOC entry 6393 (class 2604 OID 28242)
-- Name: terra_indigena gid; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.terra_indigena ALTER COLUMN gid SET DEFAULT nextval('salve.terra_indigena_gid_seq'::regclass);


--
-- TOC entry 6404 (class 2604 OID 28406)
-- Name: traducao_dados_apoio sq_traducao_dados_apoio; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.traducao_dados_apoio ALTER COLUMN sq_traducao_dados_apoio SET DEFAULT nextval('salve.traducao_dados_apoio_sq_traducao_dados_apoio_seq'::regclass);


--
-- TOC entry 6407 (class 2604 OID 28426)
-- Name: traducao_fila sq_traducao_fila; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.traducao_fila ALTER COLUMN sq_traducao_fila SET DEFAULT nextval('salve.traducao_fila_sq_traducao_fila_seq'::regclass);


--
-- TOC entry 6412 (class 2604 OID 28447)
-- Name: traducao_pan sq_traducao_pan; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.traducao_pan ALTER COLUMN sq_traducao_pan SET DEFAULT nextval('salve.traducao_pan_sq_traducao_pan_seq'::regclass);


--
-- TOC entry 6418 (class 2604 OID 28503)
-- Name: traducao_revisao sq_traducao_revisao; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.traducao_revisao ALTER COLUMN sq_traducao_revisao SET DEFAULT nextval('salve.traducao_revisao_sq_traducao_revisao_seq'::regclass);


--
-- TOC entry 6421 (class 2604 OID 28541)
-- Name: traducao_revisao_hist sq_traducao_revisao_hist; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.traducao_revisao_hist ALTER COLUMN sq_traducao_revisao_hist SET DEFAULT nextval('salve.traducao_revisao_hist_sq_traducao_revisao_hist_seq'::regclass);


--
-- TOC entry 6415 (class 2604 OID 28485)
-- Name: traducao_tabela sq_traducao_tabela; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.traducao_tabela ALTER COLUMN sq_traducao_tabela SET DEFAULT nextval('salve.traducao_tabela_sq_traducao_tabela_seq'::regclass);


--
-- TOC entry 6423 (class 2604 OID 28565)
-- Name: traducao_texto sq_traducao_texto; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.traducao_texto ALTER COLUMN sq_traducao_texto SET DEFAULT nextval('salve.traducao_texto_sq_traducao_texto_seq'::regclass);


--
-- TOC entry 6426 (class 2604 OID 28581)
-- Name: traducao_uso sq_traducao_uso; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.traducao_uso ALTER COLUMN sq_traducao_uso SET DEFAULT nextval('salve.traducao_uso_sq_traducao_uso_seq'::regclass);


--
-- TOC entry 6192 (class 2604 OID 20388)
-- Name: trilha_auditoria sq_trilha_auditoria; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.trilha_auditoria ALTER COLUMN sq_trilha_auditoria SET DEFAULT nextval('salve.trilha_auditoria_sq_trilha_auditoria_seq'::regclass);


--
-- TOC entry 6184 (class 2604 OID 18317)
-- Name: user_jobs sq_user_jobs; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.user_jobs ALTER COLUMN sq_user_jobs SET DEFAULT nextval('salve.user_jobs_sq_user_jobs_seq'::regclass);


--
-- TOC entry 6331 (class 2604 OID 26303)
-- Name: uso sq_uso; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.uso ALTER COLUMN sq_uso SET DEFAULT nextval('salve.uso_sq_uso_seq'::regclass);


--
-- TOC entry 6177 (class 2604 OID 18229)
-- Name: usuario_perfil sq_usuario_perfil; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.usuario_perfil ALTER COLUMN sq_usuario_perfil SET DEFAULT nextval('salve.usuario_perfil_sq_usuario_perfil_seq'::regclass);


--
-- TOC entry 6179 (class 2604 OID 18248)
-- Name: usuario_perfil_instituicao sq_usuario_perfil_instituicao; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.usuario_perfil_instituicao ALTER COLUMN sq_usuario_perfil_instituicao SET DEFAULT nextval('salve.usuario_perfil_instituicao_sq_usuario_perfil_instituicao_seq'::regclass);


--
-- TOC entry 6429 (class 2604 OID 28601)
-- Name: validador_ficha sq_validador_ficha; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.validador_ficha ALTER COLUMN sq_validador_ficha SET DEFAULT nextval('salve.validador_ficha_sq_validador_ficha_seq'::regclass);


--
-- TOC entry 6431 (class 2604 OID 28637)
-- Name: validador_ficha_chat sq_validador_ficha_chat; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.validador_ficha_chat ALTER COLUMN sq_validador_ficha_chat SET DEFAULT nextval('salve.validador_ficha_chat_sq_validador_ficha_chat_seq'::regclass);


--
-- TOC entry 6432 (class 2604 OID 28661)
-- Name: validador_ficha_revisao sq_validador_ficha_revisao; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.validador_ficha_revisao ALTER COLUMN sq_validador_ficha_revisao SET DEFAULT nextval('salve.validador_ficha_revisao_sq_validador_ficha_revisao_seq'::regclass);


--
-- TOC entry 6434 (class 2604 OID 28690)
-- Name: web_instituicao sq_web_instituicao; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.web_instituicao ALTER COLUMN sq_web_instituicao SET DEFAULT nextval('salve.web_instituicao_sq_web_instituicao_seq'::regclass);


--
-- TOC entry 6435 (class 2604 OID 28706)
-- Name: web_role sq_web_role; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.web_role ALTER COLUMN sq_web_role SET DEFAULT nextval('salve.web_role_sq_web_role_seq'::regclass);


--
-- TOC entry 6436 (class 2604 OID 28734)
-- Name: web_usuario sq_web_usuario; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.web_usuario ALTER COLUMN sq_web_usuario SET DEFAULT nextval('salve.web_usuario_sq_web_usuario_seq'::regclass);


--
-- TOC entry 6441 (class 2604 OID 28757)
-- Name: web_usuario_role sq_web_usuario_role; Type: DEFAULT; Schema: salve; Owner: -
--

ALTER TABLE ONLY salve.web_usuario_role ALTER COLUMN sq_web_usuario_role SET DEFAULT nextval('salve.web_usuario_role_sq_web_usuario_role_seq'::regclass);


--
-- TOC entry 6211 (class 2604 OID 22948)
-- Name: grupo sq_grupo; Type: DEFAULT; Schema: taxonomia; Owner: -
--

ALTER TABLE ONLY taxonomia.grupo ALTER COLUMN sq_grupo SET DEFAULT nextval('taxonomia.grupo_sq_grupo_seq'::regclass);


--
-- TOC entry 6208 (class 2604 OID 22935)
-- Name: nivel_taxonomico sq_nivel_taxonomico; Type: DEFAULT; Schema: taxonomia; Owner: -
--

ALTER TABLE ONLY taxonomia.nivel_taxonomico ALTER COLUMN sq_nivel_taxonomico SET DEFAULT nextval('taxonomia.nivel_taxonomico_sq_nivel_taxonomico_seq'::regclass);


--
-- TOC entry 6266 (class 2604 OID 24867)
-- Name: publicacao sq_publicacao; Type: DEFAULT; Schema: taxonomia; Owner: -
--

ALTER TABLE ONLY taxonomia.publicacao ALTER COLUMN sq_publicacao SET DEFAULT nextval('taxonomia.publicacao_sq_publicacao_seq'::regclass);


--
-- TOC entry 6442 (class 2604 OID 28964)
-- Name: publicacao_arquivo sq_publicacao_arquivo; Type: DEFAULT; Schema: taxonomia; Owner: -
--

ALTER TABLE ONLY taxonomia.publicacao_arquivo ALTER COLUMN sq_publicacao_arquivo SET DEFAULT nextval('taxonomia.publicacao_arquivo_sq_publicacao_arquivo_seq'::regclass);


--
-- TOC entry 6212 (class 2604 OID 22959)
-- Name: situacao sq_situacao; Type: DEFAULT; Schema: taxonomia; Owner: -
--

ALTER TABLE ONLY taxonomia.situacao ALTER COLUMN sq_situacao SET DEFAULT nextval('taxonomia.situacao_sq_situacao_seq'::regclass);


--
-- TOC entry 6213 (class 2604 OID 22975)
-- Name: taxon sq_taxon; Type: DEFAULT; Schema: taxonomia; Owner: -
--

ALTER TABLE ONLY taxonomia.taxon ALTER COLUMN sq_taxon SET DEFAULT nextval('taxonomia.taxon_sq_taxon_seq'::regclass);


--
-- TOC entry 6265 (class 2604 OID 24851)
-- Name: tipo_publicacao sq_tipo_publicacao; Type: DEFAULT; Schema: taxonomia; Owner: -
--

ALTER TABLE ONLY taxonomia.tipo_publicacao ALTER COLUMN sq_tipo_publicacao SET DEFAULT nextval('taxonomia.tipo_publicacao_sq_tipo_publicacao_seq'::regclass);


--
-- TOC entry 6264 (class 2604 OID 28807)
-- Name: tipo_uso sq_tipo_uso; Type: DEFAULT; Schema: taxonomia; Owner: -
--

ALTER TABLE ONLY taxonomia.tipo_uso ALTER COLUMN sq_tipo_uso SET DEFAULT nextval('taxonomia.tipo_uso_sq_tipo_uso_seq'::regclass);


-- Completed on 2023-02-05 18:54:15 -03

--
-- PostgreSQL database dump complete
--

