#!/bin/bash
set -e

#mkdir -p /data/indexes
#chown postgres:postgres /data/indexes

# localizacao padrão das tablespaces
# /var/lib/postgresql/data
mkdir -p /etc/postgresql/tbs_index_ssd
mkdir -p /etc/postgresql/tbs_geo
chown postgres:postgres /etc/postgresql/tbs_index_ssd
chown postgres:postgres /etc/postgresql/tbs_geo

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
	CREATE USER usr_salve;
	CREATE DATABASE db_salve_estadual;
	GRANT ALL PRIVILEGES ON DATABASE db_salve_estadual TO usr_salve;
	GRANT ALL PRIVILEGES ON DATABASE db_salve_estadual TO postgres;
  CREATE TABLESPACE tbs_index_ssd owner postgres location '/etc/postgresql/tbs_index_ssd';
  CREATE TABLESPACE tbs_geo owner postgres location '/etc/postgresql/tbs_geo';
EOSQL
