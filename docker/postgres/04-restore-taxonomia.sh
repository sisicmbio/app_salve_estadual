#!/bin/bash

set -e

echo "Restaurando schema taxonomia"
pg_restore -v -h localhost -p 5432 -U postgres --dbname="db_salve_estadual" --clean /temp/dump-schema-taxonomia.backup

rm -rf /temp/dump-schema-taxonomia.backup
echo "Arquivo dump-schema-taxonomia.backup foi excluido"

echo "Removendo as tabelas desnecessarias do esquema taxonomia"
psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "db_salve_estadual" <<-EOSQL
  grant all on schema taxonomia to usr_salve;
  grant all on all tables in schema  taxonomia to usr_salve;

  drop table if exists etl_cmd_sql cascade;
  drop table if exists etl_operacao cascade;
  drop table if exists etl_plan cascade;
  drop table if exists etl_taxon cascade;
  drop table if exists taxonomia.tmp_sql;
  drop table if exists taxonomia.tmp_upd_nome_verongida_upd_trilha;
  drop table if exists carga202008190128;
  drop table if exists carga202008190131;
  drop table if exists carga202008190131_bkp_taxon;
  drop table if exists carga202008190131_erro;
  drop table if exists carga202008190131_historico;
  drop table if exists carga202008190131_taxon;
  drop table if exists carga202008190131_taxon_erro;
  drop table if exists carga202008190131_taxon_sucesso;
  drop table if exists carga202008190131_upd_trilha;
  drop table if exists carga202011050436;
  drop table if exists carga202011050436_ins_hist;
  drop table if exists carga202011050436_ins_hist_resultado;
  drop table if exists carga202011050436_sel;
  drop table if exists bkp_taxon_genero_megasoma;
  drop table if exists bkp_taxon_ordem_rodentia_20210724151137;
  drop table if exists tmp_sql;
  drop table if exists tmp_upd_nome_verongida_upd_trilha;
EOSQL
