--
-- PostgreSQL database dump
--

-- Dumped from database version 11.7 (Debian 11.7-2.pgdg100+1)
-- Dumped by pg_dump version 14.6 (Ubuntu 14.6-0ubuntu0.22.04.1)

-- Started on 2023-02-05 13:20:08 -03

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 17 (class 2615 OID 20961)
-- Name: taxonomia; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA taxonomia;


--
-- TOC entry 4638 (class 1255 OID 23796)
-- Name: create_bkp_tabela_taxon_pela_trilha(text, text); Type: FUNCTION; Schema: taxonomia; Owner: -
--

CREATE FUNCTION taxonomia.create_bkp_tabela_taxon_pela_trilha(p_co_nivel_taxonomico text, p_no_taxon text) RETURNS text
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_nome_tabela_bkp text;
    v_sql text;
BEGIN
    v_nome_tabela_bkp:='bkp_taxon_'||lower(p_co_nivel_taxonomico)||'_'||lower(p_no_taxon)||'_'||to_char(now(), 'YYYYMMDDhh24MISS');
    v_sql:='create table if not exists taxonomia.'||v_nome_tabela_bkp||' as (
with t as (select  sq_taxon, sq_taxon_pai, taxon.sq_nivel_taxonomico, sq_situacao_nome,
       no_taxon, st_ativo, no_autor, nu_ano, in_ocorre_brasil, de_autor_ano,
       json_trilha, ts_ultima_alteracao, de_nivel_taxonomico, nu_grau_taxonomico,
       in_nivel_intermediario, co_nivel_taxonomico
  from taxonomia.taxon
  inner join taxonomia.nivel_taxonomico on nivel_taxonomico.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
 where taxon.json_trilha->'''||lower(p_co_nivel_taxonomico)||'''->>''no_taxon'' = '''||p_no_taxon||'''
), sel_top_nivel as (
select sq_nivel_taxonomico, sq_taxon, sq_taxon_pai
  from t
 order by  nu_grau_taxonomico asc
 limit 1
 ), sel_taxon_superior as (
 select  sq_taxon, sq_taxon_pai, taxon.sq_nivel_taxonomico, sq_situacao_nome,
       no_taxon, st_ativo, no_autor, nu_ano, in_ocorre_brasil, de_autor_ano,
       json_trilha, ts_ultima_alteracao, de_nivel_taxonomico, nu_grau_taxonomico,
       in_nivel_intermediario, co_nivel_taxonomico
  from taxonomia.taxon
  inner join taxonomia.nivel_taxonomico on nivel_taxonomico.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
  where taxon.sq_taxon in (
SELECT sq_taxon
  from taxonomia.gettreeup(
	(select sq_taxon_pai
  from sel_top_nivel
 )
)
)
)
select  sq_taxon, sq_taxon_pai, t.sq_nivel_taxonomico, sq_situacao_nome,
       no_taxon, st_ativo, no_autor, nu_ano, in_ocorre_brasil, de_autor_ano,
       json_trilha, ts_ultima_alteracao, de_nivel_taxonomico, nu_grau_taxonomico,
       in_nivel_intermediario, co_nivel_taxonomico
  from t
union all
select  sq_taxon, sq_taxon_pai, sel_taxon_superior.sq_nivel_taxonomico, sq_situacao_nome,
       no_taxon, st_ativo, no_autor, nu_ano, in_ocorre_brasil, de_autor_ano,
       json_trilha, ts_ultima_alteracao, de_nivel_taxonomico, nu_grau_taxonomico,
       in_nivel_intermediario, co_nivel_taxonomico
from sel_taxon_superior
);';


 EXECUTE v_sql ;

 return 'Backup taxon em '||v_nome_tabela_bkp||' realizado com sucesso!!!';

EXCEPTION WHEN OTHERS THEN 
  BEGIN 
 
  RAISE EXCEPTION 'Erro: taxonomia.create_bkp_tabela_taxon_pela_trilha % %', E'\n',SQLERRM;

  END;

END;
$$;


--
-- TOC entry 4639 (class 1255 OID 23797)
-- Name: create_sql_upd_old_to_new_sq_taxon(integer, integer, boolean, boolean); Type: FUNCTION; Schema: taxonomia; Owner: -
--

CREATE FUNCTION taxonomia.create_sql_upd_old_to_new_sq_taxon(v_sq_taxon_old integer, v_sq_taxon_new integer, v_delete_taxon boolean DEFAULT false, v_disable_trigger_sisbio boolean DEFAULT true) RETURNS TABLE(cmd_sql text)
    LANGUAGE plpgsql
    AS $_$
DECLARE
    cmd_sql_delete_taxon text;

begin
    if v_delete_taxon is false then
        cmd_sql_delete_taxon := '';
    else
        cmd_sql_delete_taxon := ' DO $do$ BEGIN IF NOT EXISTS ( SELECT FROM taxonomia.vw_discovery_fk_taxon  	WHERE valor_coluna_fk='||v_sq_taxon_old||' ) THEN DELETE FROM taxonomia.taxon WHERE sq_taxon = '||v_sq_taxon_old||'; END IF; END; $do$; ';
    end if;
    RETURN QUERY select 'select ''-- Atualizando '||vw_discovery_fk_taxon.esquema_nome_tabela_pk||'.'||vw_discovery_fk_taxon.nome_tabela_pk||' de ('||v_sq_taxon_old||') para ('||v_sq_taxon_new||')'';'||
                        (case
                             WHEN vw_discovery_fk_taxon.esquema_nome_tabela_pk = 'sisbio'
                                       AND vw_discovery_fk_taxon.nome_tabela_pk ='rel_taxon'
                                       AND v_disable_trigger_sisbio is true
                                 THEN ' ALTER TABLE sisbio.rel_taxon DISABLE TRIGGER tg_rel_taxon_before_delete; ALTER TABLE sisbio.rel_taxon  DISABLE TRIGGER tg_rel_taxon_incluir_historico; '
                             WHEN vw_discovery_fk_taxon.esquema_nome_tabela_pk = 'sisbio'
                                      AND vw_discovery_fk_taxon.nome_tabela_pk ='solicitacao_taxon'
                                      AND v_disable_trigger_sisbio is true
                                 THEN ' ALTER TABLE sisbio.solicitacao_taxon DISABLE TRIGGER tg_solicitacao_taxon_after_insert_update; '
                             ELSE ' '
                            END
                            )
                            ||'UPDATE '||vw_discovery_fk_taxon.esquema_nome_tabela_pk||'.'||vw_discovery_fk_taxon.nome_tabela_pk||' SET '||vw_discovery_fk_taxon.nome_coluna_fk||'='||v_sq_taxon_new||' WHERE '||vw_discovery_fk_taxon.nome_coluna_pk||' = '||vw_discovery_fk_taxon.valor_pk||' AND  '||vw_discovery_fk_taxon.nome_coluna_fk||'<>'||v_sq_taxon_new||';'||
                        (case
                             WHEN vw_discovery_fk_taxon.esquema_nome_tabela_pk = 'sisbio'
                               AND vw_discovery_fk_taxon.nome_tabela_pk ='rel_taxon'
                                 AND v_disable_trigger_sisbio is true
                                 THEN ' ALTER TABLE sisbio.rel_taxon ENABLE TRIGGER tg_rel_taxon_before_delete; ALTER TABLE sisbio.rel_taxon  ENABLE TRIGGER tg_rel_taxon_incluir_historico; '
                             WHEN vw_discovery_fk_taxon.esquema_nome_tabela_pk = 'sisbio'
                                AND vw_discovery_fk_taxon.nome_tabela_pk ='solicitacao_taxon'
                                 AND v_disable_trigger_sisbio is true
                                 THEN ' ALTER TABLE sisbio.solicitacao_taxon ENABLE TRIGGER tg_solicitacao_taxon_after_insert_update; '
                             ELSE ''
                            END
                            )||' '|| cmd_sql_delete_taxon as cmd_sql
                 FROM taxonomia.vw_discovery_fk_taxon WHERE vw_discovery_fk_taxon.valor_coluna_fk = v_sq_taxon_old;
END;
$_$;


--
-- TOC entry 4640 (class 1255 OID 23798)
-- Name: create_vw_discovery_fk_taxon(); Type: FUNCTION; Schema: taxonomia; Owner: -
--

CREATE FUNCTION taxonomia.create_vw_discovery_fk_taxon() RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    cmd_sql_discovery_vw text;
BEGIN

    select 'DROP VIEW IF EXISTS taxonomia.vw_sql_upd_taxon_antigo_novo_all_schemas; 
            DROP MATERIALIZED VIEW IF EXISTS taxonomia.mv_arvore_taxon_em_uso; 
            DROP MATERIALIZED VIEW IF EXISTS taxonomia.mv_discovery_fk_taxon; 
           DROP VIEW IF EXISTS taxonomia.vw_discovery_fk_taxon ;
    CREATE OR REPLACE VIEW taxonomia.vw_discovery_fk_taxon AS  '||
           string_agg( tb1.sql_pesquisa, ' UNION ') || ';

GRANT ALL ON TABLE taxonomia.vw_discovery_fk_taxon TO usr_sintax;
GRANT ALL ON TABLE taxonomia.vw_discovery_fk_taxon TO usr_jenkins WITH GRANT OPTION;'
               as sql_discovery_vw into cmd_sql_discovery_vw
    from (
             SELECT
                 nf.nspname AS origem_esquema,
                 clf.relname AS origem_tabela,
                 'SELECT '''||a.attname||'''::text as nome_coluna_fk ,'||a.attname||' AS valor_coluna_fk, '''||n.nspname||'''::text as esquema_nome_tabela_pk , '''||cl.relname||'''::text as nome_tabela_pk ,'''||pk.tabela_chave_pk||'''::text as nome_coluna_pk ,'||pk.tabela_chave_pk||'::bigint as valor_pk  FROM '||n.nspname||'.'||cl.relname||' WHERE '||a.attname||' is not null '
                     as sql_pesquisa
             FROM pg_catalog.pg_attribute a
                      JOIN pg_catalog.pg_class cl ON (a.attrelid = cl.oid AND cl.relkind = 'r' AND cl.relname <> 'taxon_hist')
                      JOIN pg_catalog.pg_namespace n ON (n.oid = cl.relnamespace)
                      JOIN pg_catalog.pg_constraint ct ON (a.attrelid = ct.conrelid AND
                                                           ct.confrelid != 0 AND ct.conkey[1] = a.attnum)
                      JOIN pg_catalog.pg_class clf ON (ct.confrelid = clf.oid AND clf.relkind = 'r' AND clf.relname = 'taxon')
                      JOIN pg_catalog.pg_namespace nf ON (nf.oid = clf.relnamespace AND nf.nspname = 'taxonomia')
                      JOIN pg_catalog.pg_attribute af ON (af.attrelid = ct.confrelid AND
                                                          af.attnum = ct.confkey[1])
                      JOIN (
                 SELECT
                     cl.relname AS tabela_pk,
                     n.nspname AS tabela_esquema_pk,
                     a.attname AS tabela_chave_pk
                 FROM pg_class c
                          INNER JOIN pg_attribute a ON (c.oid = a.attrelid)
                          INNER JOIN pg_index i ON (c.oid = i.indrelid)
                          JOIN pg_catalog.pg_class cl ON (a.attrelid = cl.oid )
                          JOIN pg_catalog.pg_namespace n ON (n.oid = cl.relnamespace)
                 WHERE
                         i.indkey[0] = a.attnum AND
                         i.indisprimary = 't'

             ) pk ON  tabela_esquema_pk = n.nspname AND  tabela_pk =  cl.relname
             WHERE clf.relname = 'taxon'
               and nf.nspname = 'taxonomia'
               and (cl.relname <> 'taxon') -- adicionado para nao realizar a insercao do autorelacionamento ta tabela taxon sq_taxon_pai
              -- and (cl.relname <> 'core_registro') -- adicionado temporariamente ate verificar com o luiz. ticker #https://sistemascotec.icmbio.gov.br/issues/9117
         ) tb1
    group by tb1.origem_esquema, tb1.origem_tabela;

    cmd_sql_discovery_vw:= cmd_sql_discovery_vw ||
                           '--DROP MATERIALIZED VIEW IF EXISTS taxonomia.mv_discovery_fk_taxon CASCADE;
                           CREATE MATERIALIZED VIEW taxonomia.mv_discovery_fk_taxon AS
                           SELECT vw_discovery_fk_taxon.nome_coluna_fk,
                                  vw_discovery_fk_taxon.valor_coluna_fk,
                                  vw_discovery_fk_taxon.esquema_nome_tabela_pk,
                                  vw_discovery_fk_taxon.nome_tabela_pk,
                                  vw_discovery_fk_taxon.nome_coluna_pk,
                                  vw_discovery_fk_taxon.valor_pk
                           FROM taxonomia.vw_discovery_fk_taxon
                               WITH DATA; ';

    cmd_sql_discovery_vw:= cmd_sql_discovery_vw ||' DO
    $do$
        BEGIN
            IF NOT EXISTS(
                    SELECT                       -- SELECT list can stay empty for this
                    FROM   pg_catalog.pg_roles
                    WHERE  rolname = ''usr_monitora'') THEN

                create user usr_monitora with password ''usr_monitora'';
            END IF;
            IF NOT EXISTS(
                    SELECT                       -- SELECT list can stay empty for this
                    FROM   pg_catalog.pg_roles
                    WHERE  rolname = ''usr_salve'') THEN

                create user usr_salve with password ''usr_salve'';
            END IF; ' ||
                           ' IF NOT EXISTS(
                                   SELECT                       -- SELECT list can stay empty for this
                                   FROM   pg_catalog.pg_roles
                                   WHERE  rolname = ''usr_sintax'') THEN

                               create user usr_salve with password ''usr_sintax'';
                           END IF;
                       END;
                   $do$; ';

    cmd_sql_discovery_vw:= cmd_sql_discovery_vw ||' GRANT SELECT ON TABLE taxonomia.mv_discovery_fk_taxon TO usr_monitora; ';
    cmd_sql_discovery_vw:= cmd_sql_discovery_vw ||' GRANT SELECT ON TABLE taxonomia.mv_discovery_fk_taxon TO usr_salve; ';
    cmd_sql_discovery_vw:= cmd_sql_discovery_vw ||' GRANT SELECT ON TABLE taxonomia.mv_discovery_fk_taxon TO usr_sintax; ';

    --  GRANT SELECT ON TABLE taxonomia.mv_discovery_fk_taxon TO usr_monitora;
    --  GRANT SELECT ON TABLE taxonomia.mv_discovery_fk_taxon TO usr_salve;
    --  GRANT SELECT ON TABLE taxonomia.mv_discovery_fk_taxon TO usr_sintax;
    --GRANT SELECT ON TABLE taxonomia.mv_discovery_fk_taxon TO developers;

-- Index: taxonomia.idx_mv_discovery_fk_sq_taxon

-- DROP INDEX taxonomia.idx_mv_discovery_fk_sq_taxon;

    cmd_sql_discovery_vw:= cmd_sql_discovery_vw ||'     ' ||
                           ' CREATE INDEX idx_mv_discovery_fk_sq_taxon
        ON taxonomia.mv_discovery_fk_taxon
            USING btree
            (valor_coluna_fk); ';

    -- Index: taxonomia.idx_mv_discovery_pk_valor

-- DROP INDEX taxonomia.idx_mv_discovery_pk_valor;

    cmd_sql_discovery_vw:= cmd_sql_discovery_vw ||'    '||
                           '    CREATE INDEX idx_mv_discovery_pk_valor
        ON taxonomia.mv_discovery_fk_taxon
            USING btree
            (valor_pk); ';

    cmd_sql_discovery_vw:= cmd_sql_discovery_vw ||'    COMMENT ON MATERIALIZED VIEW taxonomia.mv_discovery_fk_taxon
        IS ''View materializada que possui os schemas com as tabelas que estao referenciando o sq_taxon ta tabela taxon. ''; ';

    cmd_sql_discovery_vw:= cmd_sql_discovery_vw ||' CREATE MATERIALIZED VIEW taxonomia.mv_arvore_taxon_em_uso AS
        WITH taxon_em_uso AS (
            select distinct (valor_coluna_fk ) as valor_coluna_fk
            from taxonomia.mv_discovery_fk_taxon
        )
        select distinct tb_gettreeup.sq_taxon, tb_gettreeup.sq_nivel_taxonomico, tb_gettreeup.nivel from taxon_em_uso
                                                                                                       , taxonomia.gettreeup(taxon_em_uso.valor_coluna_fk) as tb_gettreeup
        WITH DATA;

    GRANT SELECT ON TABLE taxonomia.mv_arvore_taxon_em_uso TO usr_monitora;
    GRANT SELECT ON TABLE taxonomia.mv_arvore_taxon_em_uso TO usr_salve;
    GRANT SELECT ON TABLE taxonomia.mv_arvore_taxon_em_uso TO usr_sintax;
--GRANT SELECT ON TABLE taxonomia.mv_discovery_fk_taxon TO developers;

    CREATE INDEX idx_mv_discovery_sq_taxon
        ON taxonomia.mv_arvore_taxon_em_uso
            USING btree
            (sq_taxon);

    CREATE INDEX idx_mv_discovery_sq_nivel_taxonomico
        ON taxonomia.mv_arvore_taxon_em_uso
            USING btree
            (sq_nivel_taxonomico);

    CREATE INDEX idx_mv_discovery_nivel
        ON taxonomia.mv_arvore_taxon_em_uso
            USING btree
            (nivel);

    COMMENT ON MATERIALIZED VIEW taxonomia.mv_arvore_taxon_em_uso
        IS ''Arvore taxonomica onde um dos seus taxon esta sendo usado por alguem''; ';

    EXECUTE cmd_sql_discovery_vw;

    return 'Visao taxonomia.vw_discovery_fk_taxon criada';

END;
$_$;


--
-- TOC entry 4641 (class 1255 OID 23801)
-- Name: criar_trilha(bigint); Type: FUNCTION; Schema: taxonomia; Owner: -
--

CREATE FUNCTION taxonomia.criar_trilha(v_sq_taxon bigint) RETURNS json
    LANGUAGE plpgsql
    AS $$
DECLARE
   			v_trilha json;
        BEGIN
	WITH RECURSIVE subir_arvore_taxon(
nivel,
key,
ds_caminho,
sq_taxon,
in_ocorre_brasil,
sq_nivel_taxonomico,
no_autor, no_taxon,
nu_ano,
sq_situacao_nome,
st_ativo,
sq_taxon_pai)
    AS (
	SELECT 1::integer as nivel, taxon.sq_taxon as key, taxonomia.formatar_nome(taxon.sq_taxon)::text as ds_caminho, taxon.sq_taxon, taxon.in_ocorre_brasil,
	taxon.sq_nivel_taxonomico, taxon.no_autor,  taxonomia.formatar_nome(taxon.sq_taxon) as no_taxon
	, taxon.nu_ano, taxon.sq_situacao_nome, taxon.st_ativo, taxon.sq_taxon_pai
	FROM taxonomia.taxon
	where taxon.sq_taxon  = v_sq_taxon
	UNION
	SELECT t.nivel+1::integer as nivel, taxon.sq_taxon as key,
	(t.ds_caminho::text) ||' < ' ||taxonomia.formatar_nome(taxon.sq_taxon)::text as ds_caminho,
	taxon.sq_taxon, taxon.in_ocorre_brasil, taxon.sq_nivel_taxonomico, taxon.no_autor , taxonomia.formatar_nome(taxon.sq_taxon) as no_taxon ,
	taxon.nu_ano, taxon.sq_situacao_nome, taxon.st_ativo, taxon.sq_taxon_pai
	FROM taxonomia.taxon
	JOIN  subir_arvore_taxon t  on taxon.sq_taxon = t.sq_taxon_pai
    )
    SELECT json_object_agg(lower(nivel_taxonomico.co_nivel_taxonomico), json_build_object(
							'sq_taxon',  t.sq_taxon,
							'no_taxon', t.no_taxon,
		                    'sq_taxon_pai', t.sq_taxon_pai,
							'st_ativo', t.st_ativo,
							'in_ocorre_brasil', t.in_ocorre_brasil,
							'sq_nivel_taxonomico', t.sq_nivel_taxonomico,
							'de_nivel_taxonomico', nivel_taxonomico.de_nivel_taxonomico,
							'nu_grau_taxonomico', nivel_taxonomico.nu_grau_taxonomico,
							'in_nivel_intermediario', nivel_taxonomico.in_nivel_intermediario,
							'co_nivel_taxonomico', nivel_taxonomico.co_nivel_taxonomico
				)
			    )

		as trilha into v_trilha
      FROM subir_arvore_taxon t
      inner join taxonomia.nivel_taxonomico
	      on nivel_taxonomico.sq_nivel_taxonomico = t.sq_nivel_taxonomico;

        return v_trilha;
        END;
$$;


--
-- TOC entry 4642 (class 1255 OID 23802)
-- Name: criar_trilha2(bigint); Type: FUNCTION; Schema: taxonomia; Owner: -
--

CREATE FUNCTION taxonomia.criar_trilha2(v_sq_taxon bigint) RETURNS json
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_trilha json;
BEGIN
    select array_to_json(array_agg(row_to_json(tb1))) into v_trilha
    from (
             WITH RECURSIVE subir_arvore_taxon(
                                               nivel,
                                               key,
                                               ds_caminho,
                                               sq_taxon,
                                               in_ocorre_brasil,
                                               sq_nivel_taxonomico,
                                               no_autor, no_taxon,
                                               nu_ano,
                                               sq_situacao_nome,
                                               st_ativo,
                                               sq_taxon_pai,
                                               sq_taxon_alvo,
                                               nome_cientifico)
                                AS (
                     SELECT 1::integer                                    as nivel,
                            taxon.sq_taxon                                as key,
                            taxonomia.formatar_nome(taxon.sq_taxon)::text as ds_caminho,
                            taxon.sq_taxon,
                            taxon.in_ocorre_brasil,
                            taxon.sq_nivel_taxonomico,
                            taxon.no_autor,
                            taxonomia.formatar_nome(taxon.sq_taxon)       as no_taxon,
                            taxon.nu_ano,
                            taxon.sq_situacao_nome,
                            taxon.st_ativo,
                            taxon.sq_taxon_pai,
                            sq_taxon                                      as sq_taxon_alvo,
                            taxonomia.formatar_nome(sq_taxon)             as nome_cientifico
                     FROM taxonomia.taxon
                     where taxon.sq_taxon = v_sq_taxon
                     UNION
                     SELECT t.nivel + 1::integer                                                           as nivel,
                            taxon.sq_taxon                                                                 as key,
                            (t.ds_caminho::text) || ' < ' ||
                            taxonomia.formatar_nome(taxon.sq_taxon)::text                                  as ds_caminho,
                            taxon.sq_taxon,
                            taxon.in_ocorre_brasil,
                            taxon.sq_nivel_taxonomico,
                            taxon.no_autor,
                            taxonomia.formatar_nome(taxon.sq_taxon)                                        as no_taxon,
                            taxon.nu_ano,
                            taxon.sq_situacao_nome,
                            taxon.st_ativo,
                            taxon.sq_taxon_pai,
                            t.sq_taxon_alvo,
                            taxonomia.formatar_nome(taxon.sq_taxon)                                        as nome_cientifico
                     FROM taxonomia.taxon
                              JOIN subir_arvore_taxon t on taxon.sq_taxon = t.sq_taxon_pai
                 )
             SELECT t.nivel as nivel_recursivo,
                    t.key,
                    t.ds_caminho,
                    t.sq_taxon,
                    t.in_ocorre_brasil,
                    t.sq_nivel_taxonomico,
                    t.no_autor,
                    no_taxon,
                    t.nu_ano,
                    t.sq_situacao_nome,
                    t.st_ativo,
                    t.sq_taxon_pai,
                    t.sq_taxon_alvo,
                    t.nome_cientifico,
                    nivel_taxonomico.de_nivel_taxonomico,
                    nivel_taxonomico.nu_grau_taxonomico,
                    nivel_taxonomico.in_nivel_intermediario,
                    nivel_taxonomico.co_nivel_taxonomico
             FROM subir_arvore_taxon t
                      inner join taxonomia.nivel_taxonomico
                                 on nivel_taxonomico.sq_nivel_taxonomico = t.sq_nivel_taxonomico
         ) tb1;


    return v_trilha;
END;
$$;


--
-- TOC entry 4643 (class 1255 OID 23803)
-- Name: etl_create_cmd_sql(integer); Type: FUNCTION; Schema: taxonomia; Owner: -
--

CREATE FUNCTION taxonomia.etl_create_cmd_sql(v_sq_etl_plan integer) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    v_registro record;
    v_registro_discovery record;
    v_existe integer;
    js_row_data json;
    
    
BEGIN

--    select taxonomia.create_vw_discovery_fk_taxon() into cmd_sql_discovery_vw;


select count(*)  into v_existe from taxonomia.etl_plan where sq_etl_plan=v_sq_etl_plan;

if v_existe = 0 then
  RAISE EXCEPTION ' % v_sq_etl_plan não existe % % %',v_sq_etl_plan, E'\n',E'\n',E'\n';
end if;

 FOR v_registro IN SELECT sq_etl_taxon,
        etl_operacao.sq_etl_operacao,
        etl_operacao.co_etl_operacao,
        etl_operacao.ds_etl_operacao,
        etl_operacao.st_update_fk,
       etl_operacao.st_ativo_sq_taxon_de,
       etl_operacao.st_del_sq_taxon_de,
    etl_plan.co_etl_plan,
    etl_plan.nm_etl_plan,
    etl_plan.ts_etl_plan,
    etl_plan.obs_etl_plan,
            etl_taxon.sq_taxon_de,
        etl_taxon.sq_taxon_para
  FROM taxonomia.etl_taxon
   INNER JOIN taxonomia.etl_plan ON etl_plan.sq_etl_plan = etl_taxon.sq_etl_plan
   INNER JOIN taxonomia.etl_operacao ON etl_operacao.sq_etl_operacao = etl_plan.sq_etl_operacao
   WHERE etl_taxon.sq_etl_plan = v_sq_etl_plan
  LOOP

    select count(*) into v_existe FROM taxonomia.mv_discovery_fk_taxon WHERE mv_discovery_fk_taxon.valor_coluna_fk = v_registro.sq_taxon_de;

    if v_existe = 0 then

    RAISE INFO 'Não existe em outros sistemas a vinculacao de   %  %', v_registro.sq_taxon_de, E'\n';
    
    else
    
        select count(*) into v_existe from taxonomia.etl_cmd_sql WHERE sq_etl_taxon = v_registro.sq_etl_taxon;
        if v_existe > 0 then
            RAISE INFO 'Apagando os % comandos ja gerados para o sq_taxon_de   %  para este plano  % . % ',v_existe, v_registro.sq_taxon_de , v_sq_etl_plan, E'\n';
            DELETE FROM taxonomia.etl_cmd_sql WHERE sq_etl_taxon = v_registro.sq_etl_taxon;
        end if;


        RAISE INFO 'Gerando script ELT para sq_taxon_de  %  => sq_taxon_para % %',v_registro.sq_taxon_de , v_registro.sq_taxon_para, E'\n';
	       IF v_registro.st_update_fk is true THEN

	       RAISE INFO 'Gerando script de atualizacao de fk  (st_update_fk=%) %',v_registro.st_update_fk, E'\n';

			FOR v_registro_discovery IN 

			select mv_discovery_fk_taxon.esquema_nome_tabela_pk as esquema_nome_tabela_pk,
			mv_discovery_fk_taxon.nome_tabela_pk as nome_tabela_pk,
			mv_discovery_fk_taxon.nome_coluna_pk as nome_coluna_pk,
			mv_discovery_fk_taxon.valor_pk as valor_pk,
			mv_discovery_fk_taxon.nome_coluna_fk as nome_coluna_fk,
			'Atualizando '||mv_discovery_fk_taxon.esquema_nome_tabela_pk||'.'||mv_discovery_fk_taxon.nome_tabela_pk||' de ('||v_registro.sq_taxon_de||') para ('||v_registro.sq_taxon_para||') . ' as cmd_sql_print,
			'UPDATE '||mv_discovery_fk_taxon.esquema_nome_tabela_pk||'.'||mv_discovery_fk_taxon.nome_tabela_pk||' SET '||mv_discovery_fk_taxon.nome_coluna_fk||'='||v_registro.sq_taxon_para||' WHERE '||mv_discovery_fk_taxon.nome_coluna_pk||' = '||mv_discovery_fk_taxon.valor_pk||' ' 
			as cmd_sql_update,
			'UPDATE '||mv_discovery_fk_taxon.esquema_nome_tabela_pk||'.'||mv_discovery_fk_taxon.nome_tabela_pk||' SET '||mv_discovery_fk_taxon.nome_coluna_fk||'='||v_registro.sq_taxon_de||' WHERE '||mv_discovery_fk_taxon.nome_coluna_pk||' = '||mv_discovery_fk_taxon.valor_pk||' ' 
			as cmd_sql_rollback,
			'SELECT * FROM '||mv_discovery_fk_taxon.esquema_nome_tabela_pk||'.'||mv_discovery_fk_taxon.nome_tabela_pk||' WHERE '||mv_discovery_fk_taxon.nome_coluna_pk||' = '||mv_discovery_fk_taxon.valor_pk||' ' 
			as cmd_sql_select,
			null as cmd_sql_delete_taxon
			FROM taxonomia.mv_discovery_fk_taxon WHERE mv_discovery_fk_taxon.valor_coluna_fk = v_registro.sq_taxon_de
			
			  LOOP
			  
			--    RAISE INFO 'sq_taxon_de  %  => sq_taxon_para % % sql => % %',v_registro.sq_taxon_de , v_registro.sq_taxon_para, E'\n', v_registro_discovery.cmd_sql_select, E'\n';

		--	    v_sql_row_data:= format('select array_to_json(array_agg(row_to_json(tb1)))::json  from ( %1$s ) tb1' , v_registro_discovery.cmd_sql_select);
			    
		-- RAISE INFO 'v_sql_row_data %', v_sql_row_data;
			 IF v_registro_discovery.cmd_sql_select IS NOT NULL THEN
			     EXECUTE 'select array_to_json(array_agg(row_to_json(tb1)))  from ( '|| v_registro_discovery.cmd_sql_select||' ) tb1'  INTO js_row_data;
			 ELSE
			     js_row_data:=null;
			 END IF;

		--RAISE INFO 'js_row_data %', js_row_data::text;



	   
	   
			INSERT INTO taxonomia.etl_cmd_sql(
			    sq_etl_cmd_sql,
			    nu_etl_cmd_sql,
			    nm_esquema_pk,
			    nm_tabela_pk,
			    nm_coluna_pk,
			    vl_coluna_pk,
			    nm_coluna_fk,
			    co_cmd,
			    sql_cmd,
			    sql_rollback,
			    sql_print,
			    sql_select,
			    js_row_data,
			    sq_etl_taxon)
		    VALUES (nextval('taxonomia.etl_cmd_sql_sq_etl_cmd_sql_seq'::regclass),
			    (SELECT COALESCE (MAX(nu_etl_cmd_sql),0)+1
				  FROM taxonomia.etl_taxon
				   INNER JOIN taxonomia.etl_plan ON etl_plan.sq_etl_plan = etl_taxon.sq_etl_plan
				   LEFT JOIN  taxonomia.etl_cmd_sql ON etl_cmd_sql.sq_etl_taxon = etl_taxon.sq_etl_taxon
				   WHERE etl_taxon.sq_etl_plan = v_sq_etl_plan
				   AND etl_taxon.sq_taxon_de = v_registro.sq_taxon_de),
			    v_registro_discovery.esquema_nome_tabela_pk, --nm_esquema_pk
			    v_registro_discovery.nome_tabela_pk, --nm_tabela_pk
			    v_registro_discovery.nome_coluna_pk, --nm_coluna_pk
			    v_registro_discovery.valor_pk, --vl_coluna_pk
			    v_registro_discovery.nome_coluna_fk, --nm_coluna_fk
			    'UPDATE', --co_cmd
			    v_registro_discovery.cmd_sql_update, --sql_cmd
			    v_registro_discovery.cmd_sql_rollback, --sql_rollback
			    v_registro_discovery.cmd_sql_print, --sql_print
			    v_registro_discovery.cmd_sql_select, --sql_select
			    js_row_data,
			    v_registro.sq_etl_taxon --sq_etl_taxon
			    );         
			    
			  END LOOP;
		END IF;
		


		  RAISE INFO 'v_registro.st_ativo_sq_taxon_de =>  % %',v_registro.st_ativo_sq_taxon_de , E'\n';
		  
		  IF v_registro.st_ativo_sq_taxon_de IS FALSE THEN

			RAISE INFO 'Desativar o taxon sq_taxon=>  %  % sql => % %',v_registro.sq_taxon_de , E'\n', v_registro_discovery.cmd_sql_select, E'\n';

			js_row_data:=null;
			
			EXECUTE 'select array_to_json(array_agg(row_to_json(tb1)))  from ( SELECT *  FROM taxonomia.taxon WHERE sq_taxon = '||v_registro.sq_taxon_de||' ) tb1'  INTO js_row_data;

			INSERT INTO taxonomia.etl_cmd_sql(
				    sq_etl_cmd_sql, 
				    nu_etl_cmd_sql, 
				    nm_esquema_pk, 
				    nm_tabela_pk, 
				    nm_coluna_pk, 
				    vl_coluna_pk, 
				    nm_coluna_fk,  
				    co_cmd, 
				    sql_cmd, 
				    sql_rollback, 
				    sql_print, 
				    sql_select,
				    js_row_data,
				    sq_etl_taxon)
			    VALUES (nextval('taxonomia.etl_cmd_sql_sq_etl_cmd_sql_seq'::regclass), 
				  (SELECT COALESCE (MAX(nu_etl_cmd_sql),0)+1
				  FROM taxonomia.etl_taxon
				   INNER JOIN taxonomia.etl_plan ON etl_plan.sq_etl_plan = etl_taxon.sq_etl_plan
				   LEFT JOIN  taxonomia.etl_cmd_sql ON etl_cmd_sql.sq_etl_taxon = etl_taxon.sq_etl_taxon
				   WHERE etl_taxon.sq_etl_plan = v_sq_etl_plan
				   AND etl_taxon.sq_taxon_de = v_registro.sq_taxon_de), --nu_etl_cmd_sql
				    'taxonomia', --nm_esquema_pk
				    'taxon', --nm_tabela_pk
				    'sq_taxon', --nm_coluna_pk
				    v_registro.sq_taxon_de, --vl_coluna_pk
				    null, --nm_coluna_fk
				    'UPDATE', --co_cmd
				    'UPDATE taxonomia.taxon SET st_ativo=false WHERE sq_taxon='||v_registro.sq_taxon_de||'', --sql_cmd
				    'UPDATE taxonomia.taxon SET st_ativo=true WHERE sq_taxon='||v_registro.sq_taxon_de||'', --sql_rollback
				    'Desativado o TAXON sq_taxon = '||v_registro.sq_taxon_de||'. ', --sql_print
				    'SELECT *  FROM taxonomia.taxon WHERE sq_taxon = '||v_registro.sq_taxon_de||'', --sql_select
				    js_row_data,
				    v_registro.sq_etl_taxon --sq_etl_taxon
				    );
		    

		  END IF;

			  RAISE INFO 'v_registro.st_del_sq_taxon_de =>  % %',v_registro.st_del_sq_taxon_de , E'\n';
		  
		  IF v_registro.st_del_sq_taxon_de IS TRUE THEN

			RAISE INFO 'Deletando taxon caso nao esteja sendo usado por ninguem sq_taxon=>  %  % sql => % %',v_registro.sq_taxon_de , E'\n', v_registro_discovery.cmd_sql_select, E'\n';

			js_row_data:=null;
			
			EXECUTE 'select array_to_json(array_agg(row_to_json(tb1)))  from ( SELECT *  FROM taxonomia.taxon WHERE sq_taxon = '||v_registro.sq_taxon_de||' ) tb1'  INTO js_row_data;	        

			INSERT INTO taxonomia.etl_cmd_sql(
				    sq_etl_cmd_sql, 
				    nu_etl_cmd_sql, 
				    nm_esquema_pk, 
				    nm_tabela_pk, 
				    nm_coluna_pk, 
				    vl_coluna_pk, 
				    nm_coluna_fk,  
				    co_cmd, 
				    sql_cmd, 
				    sql_rollback, 
				    sql_print, 
				    sql_select,
				    js_row_data, 
				    sq_etl_taxon)
			    VALUES (nextval('taxonomia.etl_cmd_sql_sq_etl_cmd_sql_seq'::regclass), 
				     (SELECT COALESCE (MAX(nu_etl_cmd_sql),0)+1
				  FROM taxonomia.etl_taxon
				   INNER JOIN taxonomia.etl_plan ON etl_plan.sq_etl_plan = etl_taxon.sq_etl_plan
				   LEFT JOIN  taxonomia.etl_cmd_sql ON etl_cmd_sql.sq_etl_taxon = etl_taxon.sq_etl_taxon
				   WHERE etl_taxon.sq_etl_plan = v_sq_etl_plan
				   AND etl_taxon.sq_taxon_de = v_registro.sq_taxon_de),
				    'taxonomia', --nm_esquema_pk
				    'taxon', --nm_tabela_pk
				    'sq_taxon', --nm_coluna_pk
				    v_registro.sq_taxon_de, --vl_coluna_pk
				    null, --nm_coluna_fk
				    'DELETE', --co_cmd
				    'IF NOT EXISTS ( SELECT FROM taxonomia.vw_discovery_fk_taxon WHERE valor_coluna_fk='||v_registro.sq_taxon_de||' UNION SELECT FROM taxonomia.taxon WHERE sq_taxon_pai='||v_registro.sq_taxon_de||' ) THEN DELETE FROM taxonomia.taxon WHERE sq_taxon = '||v_registro.sq_taxon_de||'; END IF ', --sql_cmd
				    null, --sql_rollback
				    'DELETE TAXON sq_taxon = '||v_registro.sq_taxon_de||' ', --sql_print
				    'SELECT *  FROM taxonomia.taxon WHERE sq_taxon = '||v_registro.sq_taxon_de||'', --sql_select
				    js_row_data,
				    v_registro.sq_etl_taxon --sq_etl_taxon
				    );
		  END IF;
     END IF;
  END LOOP;




RETURN 'Comandos sql gerados para plano '||v_sq_etl_plan||' com sucesso.';

EXCEPTION WHEN OTHERS THEN 
  BEGIN 
  
  RAISE EXCEPTION 'Erro: %', SQLERRM; 

  END;

END;
$_$;


--
-- TOC entry 4644 (class 1255 OID 23806)
-- Name: etl_create_plan(text, text, text, integer); Type: FUNCTION; Schema: taxonomia; Owner: -
--

CREATE FUNCTION taxonomia.etl_create_plan(v_co_etl_plan text, v_nm_etl_plan text, v_obs_etl_plan text, v_sq_etl_operacao integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_existe integer;
    v_sq_etl_plan integer;    
BEGIN

select count(*) into  v_existe from  taxonomia.etl_plan where co_etl_plan = v_co_etl_plan;

if v_existe = 0 then
	INSERT INTO taxonomia.etl_plan(
		    sq_etl_plan, 
		    co_etl_plan, 
		    nm_etl_plan, 
		    ts_etl_plan, 
		    obs_etl_plan, 
		    sq_etl_operacao)
	    VALUES (nextval('taxonomia.etl_plan_sq_etl_plan_seq'::regclass), 
	    v_co_etl_plan, 
	    v_nm_etl_plan, 
	    now(), 
	    v_obs_etl_plan, 
	    v_sq_etl_operacao) returning sq_etl_plan into v_sq_etl_plan;
else 
	select sq_etl_plan into  v_sq_etl_plan from  taxonomia.etl_plan where co_etl_plan = v_co_etl_plan;
end if;

return v_sq_etl_plan;

EXCEPTION WHEN OTHERS THEN 
  BEGIN 
  
  RAISE EXCEPTION 'Erro: %', SQLERRM; 

  END;

END;
$$;


--
-- TOC entry 4645 (class 1255 OID 23807)
-- Name: etl_exec_sql_plan(integer); Type: FUNCTION; Schema: taxonomia; Owner: -
--

CREATE FUNCTION taxonomia.etl_exec_sql_plan(v_sq_etl_plan integer) RETURNS text
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_sql_plan text;
BEGIN
 v_sql_plan:=null;

 select taxonomia.etl_get_sql_plan(v_sq_etl_plan)  into v_sql_plan;

 EXECUTE v_sql_plan ;

 return 'Script do plano '||v_sq_etl_plan||' rodado com sucesso!!!';

EXCEPTION WHEN OTHERS THEN 
  BEGIN 
 
  RAISE EXCEPTION 'Erro: taxonomia.etl_exec_sql_plan % %', E'\n',SQLERRM;

  END;

END;
$$;


--
-- TOC entry 4646 (class 1255 OID 23808)
-- Name: etl_get_sql_plan(integer); Type: FUNCTION; Schema: taxonomia; Owner: -
--

CREATE FUNCTION taxonomia.etl_get_sql_plan(v_sq_etl_plan integer) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    v_sql_plan text;
    v_registro record;
    v_controle_taxon_de integer;
    v_controle_taxon_para integer;
    --v_msg_st_update_fk text;
    v_msg text;
    v_registro_taxon_de record;
    v_registro_taxon_para record;
    v_print boolean;
    v_etl_plan_get_obs_etl_plan text;
    
BEGIN
 v_sql_plan:= '';

 v_controle_taxon_de := 0;
 v_controle_taxon_para := 0;
 v_print := false;
	 
 FOR v_registro IN SELECT row_number_plan, porcent_conclusao_plan, row_number_plan_taxon, 
       porcent_conclusao_etl_taxon, sq_etl_operacao, co_etl_operacao, 
       ds_etl_operacao, st_update_fk, st_ativo_sq_taxon_de, st_del_sq_taxon_de, 
       etl_operacao_obs, sq_etl_plan, co_etl_plan, nm_etl_plan, ts_etl_plan, 
       obs_etl_plan, sq_etl_taxon, ts_etl_taxon_ins, obs_etl_taxon, 
       sq_taxon_de, sq_taxon_para, sq_lista_referencia, total_cmd_por_plan, 
       total_cmd_plan_por_taxon, row_number_plan_por_taxon, sq_etl_cmd_sql, 
       nu_etl_cmd_sql, nm_esquema_pk, nm_tabela_pk, nm_coluna_pk, vl_coluna_pk, 
       nm_coluna_fk, co_cmd, sql_cmd, sql_rollback, sql_print, sql_select, 
       js_row_data
	  FROM taxonomia.vw_sel_plan_taxon_cmd
       WHERE sq_etl_plan = v_sq_etl_plan
  LOOP

        
    

	   RAISE INFO '-- Gerando script linha % (%) plano . %',v_registro.row_number_plan, v_registro.porcent_conclusao_plan, E'\n';
	   RAISE INFO '-- Taxon de  % , taxon para % , comando # % (%) .% ',v_registro.sq_taxon_de, v_registro.sq_taxon_para, v_registro.row_number_plan_taxon, v_registro.porcent_conclusao_etl_taxon ,E'\n';

	    --, 

	--    SELECT '' Rodando script '||v_registro.nu_etl_cmd_sql||' / '||v_registro.max_nu_etl_cmd_sql|| '''.
	    v_sql_plan:=v_sql_plan||'   
	    -- OPERACAO '||v_registro.co_cmd||'
	    RAISE INFO  '' [ '||v_registro.row_number_plan||'/'||v_registro.total_cmd_por_plan||' ('||v_registro.porcent_conclusao_plan||')  ] Rodando script de  '||v_registro.sq_taxon_de||' para '||v_registro.sq_taxon_para ||' [ # '||v_registro.row_number_plan_taxon||'/'||v_registro.total_cmd_plan_por_taxon
	    ||' ('||v_registro.porcent_conclusao_etl_taxon||') ] %'', ''%'' , ''%'',E''\n'';
	    -- sql_select
	    --  '||v_registro.sql_select||';
	    -- end_sql_select
	    '||v_registro.sql_cmd||';
	    -- sql_rollback
	    --  '||v_registro.sql_cmd||';
	    -- end_sql_rollback
	    
	    ';
	    
	  END LOOP;

 -- END LOOP;

select  taxonomia.etl_plan_get_obs_etl_plan(v_sq_etl_plan) into v_etl_plan_get_obs_etl_plan;

RAISE INFO 'v_etl_plan_get_obs_etl_plan  =>  %  %',E'\n', v_etl_plan_get_obs_etl_plan;
RAISE INFO 'sql  =>  %  %',E'\n', v_sql_plan;
--RETURN v_sql_plan;
IF v_sql_plan = '' THEN
	RETURN 'ssss';
ELSE
return  'DO '||E'\n'||
        '  $do$ '||E'\n'||
        '    BEGIN '||E'\n'||E'\n'|| 
	    v_etl_plan_get_obs_etl_plan||E'\n'||E'\n'||
        v_sql_plan ||E'\n'||E'\n'||
        '    END;'||E'\n'||
	'  $do$;';

END IF;

EXCEPTION WHEN OTHERS THEN 
  BEGIN 
 
  RAISE EXCEPTION 'Erro: taxonomia.etl_get_sql_plan % %', E'\n',SQLERRM; 

  END;

END;
$_$;


--
-- TOC entry 4647 (class 1255 OID 23809)
-- Name: etl_ins_etl_taxon(integer, integer, integer, text); Type: FUNCTION; Schema: taxonomia; Owner: -
--

CREATE FUNCTION taxonomia.etl_ins_etl_taxon(v_sq_etl_plan integer, v_sq_taxon_de integer, v_sq_taxon_para integer, v_obs_etl_taxon text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
     r_etl_plan record;
     v_existe integer;
     v_msg text;

BEGIN
v_msg:=null;
	SELECT count(*)  into v_existe  FROM taxonomia.etl_plan where sq_etl_plan = v_sq_etl_plan;
	if v_existe = 0 THEN

	RAISE EXCEPTION 'Código da sq_etl_plan ssss nao existe :% ',v_sq_etl_plan;

	end if;
	    
	FOR r_etl_plan IN SELECT etl_plan.sq_etl_plan, 
	etl_plan.co_etl_plan, 
	etl_plan.nm_etl_plan, 
	etl_plan.ts_etl_plan, 
	etl_plan.obs_etl_plan,
	etl_plan.sq_etl_operacao,
	etl_operacao.co_etl_operacao, 
	etl_operacao.ds_etl_operacao, 
	etl_operacao.st_update_fk,
        etl_operacao.st_ativo_sq_taxon_de,
        etl_operacao.st_del_sq_taxon_de
		FROM taxonomia.etl_plan 
		INNER JOIN taxonomia.etl_operacao ON etl_operacao.sq_etl_operacao = etl_plan.sq_etl_operacao
		WHERE  etl_plan.sq_etl_plan = v_sq_etl_plan
                      
	    LOOP


		   RAISE NOTICE 'Registrando co_etl_plan no plano: % %', r_etl_plan.co_etl_plan, E'\n';      -- single quotes!
		   RAISE NOTICE 'Registrando nm_etl_plan no plano: % %', r_etl_plan.nm_etl_plan, E'\n';      -- single quotes!
		 

		SELECT count(*)  into v_existe  FROM taxonomia.etl_taxon 
		where sq_etl_plan = v_sq_etl_plan AND sq_taxon_de = v_sq_taxon_de AND sq_taxon_para = v_sq_taxon_para ;
			
		if v_existe > 0 THEN

		RAISE INFO 'Relacao taxon de % taxon para % no plano %, já foi cadastrado',v_sq_taxon_de, v_sq_taxon_para, r_etl_plan.nm_etl_plan;

		v_msg:= 'Registro já cadastrado';

		else 

		RAISE INFO 'Registro do taxon de % e taxon para % no plano %, já foi cadastrado',v_sq_taxon_de, v_sq_taxon_para, r_etl_plan.nm_etl_plan;
		v_msg:= 'Registro cadastrado';

		INSERT INTO taxonomia.etl_taxon(
			    sq_etl_taxon, ts_etl_taxon_ins, obs_etl_taxon, sq_taxon_de, sq_taxon_para, 
			    sq_lista_referencia, sq_etl_plan)
		    VALUES (nextval('taxonomia.etl_taxon_sq_etl_taxon_seq'::regclass), 
		    now(), v_obs_etl_taxon, v_sq_taxon_de, v_sq_taxon_para, 
			    1, v_sq_etl_plan);
			    
		end if;
		   
		RETURN v_msg;

	    END LOOP;

--         end loop;
--
--     SELECT sq_etl_operacao into v_sq_etl_operacao,
--         ds_etl_operacao into v_ds_etl_operacao,
--         st_update_fk into v_st_update_fk,
--         st_ativo_sq_taxon_de into v_st_ativo_sq_taxon_de,
--         st_del_sq_taxon_de into v_st_del_sq_taxon_de,
--         etl_operacao_obs into  v_etl_operacao_obs
--     FROM taxonomia.etl_operacao
--     WHERE co_etl_operacao = v_co_etl_operacao;
--
--     SELECT sq_etl_taxon,
--            etl_operacao.sq_etl_operacao,
--            sq_taxon_de,
--            sq_taxon_para,
--
--            co_etl_operacao, ds_etl_operacao, st_update_fk,
--                              st_ativo_sq_taxon_de, st_del_sq_taxon_de
--                       FROM taxonomia.etl_taxon


    --REFRESH MATERIALIZED VIEW taxonomia.mv_discovery_fk_taxon;
--RAISE INFO 'ALTER TABLE  %.%  OWNER TO usr_jenkins %',  r.schemaname , r.tablename, E'\n';
--RETURN QUERY
--
--     select taxonomia.create_vw_discovery_fk_taxon() into cmd_sql_discovery_vw;
--
--     TRUNCATE taxonomia.etl_plan CASCADE;
--     TRUNCATE taxonomia.etl_cmd_sql cascade;
--
--     FOR v_registro IN SELECT sq_etl_taxon,  etl_operacao.sq_etl_operacao, sq_taxon_de, sq_taxon_para,
--                              co_etl_operacao, ds_etl_operacao, st_update_fk,
--                              st_ativo_sq_taxon_de, st_del_sq_taxon_de
--                       FROM taxonomia.etl_taxon
--                                INNER JOIN taxonomia.etl_operacao ON etl_operacao.sq_etl_operacao = etl_taxon.sq_etl_operacao AND etl_operacao.co_etl_operacao = v_co_etl_operacao
--                       LIMIT 1
--         LOOP
--
--
--
--
--             RAISE INFO 'sq_taxon_de  %  => sq_taxon_para % %',v_registro.sq_taxon_de , v_registro.sq_taxon_para, E'\n';
--
--
--             v_co_etl_plan:=v_registro.co_etl_operacao||'['||v_registro.sq_taxon_de||']to['||v_registro.sq_taxon_para||']  ';
--
--             RAISE INFO 'v_co_etl_plan  % %',v_co_etl_plan, E'\n';
--
--             SELECT count(*) into v_existe FROM taxonomia.etl_plan where co_etl_plan = v_co_etl_plan ;
--
--             RAISE INFO 'v_existe  % %',v_existe, E'\n';
--
--             IF v_registro.st_update_fk IS TRUE THEN
--                 v_msg_st_update_fk:='Todas as referencias (chaves estrangeiras) serão atualizadas para o novo valor.';
--
--                 v_msg:='';
--                 FOR v_registro_taxon_de IN
--                     SELECT sq_taxon, sq_taxon_pai, no_taxon, no_taxon_formatado, st_ativo,
--                            json_trilha, sq_nivel_taxonomico, ds_caminho_trilha, in_ocorre_brasil,
--                            de_nivel_taxonomico, nu_grau_taxonomico, in_nivel_intermediario,
--                            co_nivel_taxonomico
--                     FROM taxonomia.mv_taxonomia where sq_taxon=v_registro.sq_taxon_de
--
--                     LOOP
--                         v_msg:= '
-- 		    Taxon de:
-- 		      - ds_caminho_trilha: '||v_registro_taxon_de.ds_caminho_trilha::text||'
-- 		      - sq_taxon: '||v_registro_taxon_de.sq_taxon::text||'
-- 		      - sq_taxon_pai: '||v_registro_taxon_de.sq_taxon_pai::text||'
-- 		      - no_taxon_formatado: '||v_registro_taxon_de.no_taxon_formatado::text||'
-- 		      - json_trilha: '||v_registro_taxon_de.json_trilha::text||'
-- 		      -de_nivel_taxonomico: '||v_registro_taxon_de.de_nivel_taxonomico::text||'
-- 		      -sq_nivel_taxonomico: '||v_registro_taxon_de.sq_nivel_taxonomico::text||'
-- 	      ';
--                     END LOOP;
--
--                 if v_registro.sq_taxon_para IS NOT NULL THEN
--                     FOR v_registro_taxon_para IN
--                         SELECT sq_taxon, sq_taxon_pai, no_taxon, no_taxon_formatado, st_ativo,
--                                json_trilha, sq_nivel_taxonomico, ds_caminho_trilha, in_ocorre_brasil,
--                                de_nivel_taxonomico, nu_grau_taxonomico, in_nivel_intermediario,
--                                co_nivel_taxonomico
--                         FROM taxonomia.mv_taxonomia where sq_taxon=v_registro.sq_taxon_para
--
--                         LOOP
--                             v_msg:= v_msg||'
--
-- 		    Taxon para:
-- 		      - ds_caminho_trilha: '||v_registro_taxon_de.ds_caminho_trilha::text||'
-- 		      - sq_taxon: '||v_registro_taxon_de.sq_taxon::text||'
-- 		      - sq_taxon_pai: '||v_registro_taxon_de.sq_taxon_pai::text||'
-- 		      - no_taxon_formatado: '||v_registro_taxon_de.no_taxon_formatado::text||'
-- 		      - json_trilha: '||v_registro_taxon_de.json_trilha::text||'
-- 		      -de_nivel_taxonomico: '||v_registro_taxon_de.de_nivel_taxonomico::text||'
-- 		      -sq_nivel_taxonomico: '||v_registro_taxon_de.sq_nivel_taxonomico::text||'
-- 		      ';
--                         END LOOP;
--                 END IF;
--
--
--             ELSE
--                 v_msg_st_update_fk:= '';
--             END IF;
--
--
--
--             if v_existe = 0 THEN
--                 INSERT INTO taxonomia.etl_plan(
--                     sq_etl_plan, co_etl_plan, nm_etl_plan, ts_etl_plan, obs_etl_plan)
--                 VALUES (nextval('taxonomia.etl_plan_sq_etl_plan_seq'::regclass), v_co_etl_plan , v_registro.co_etl_operacao||' de '||v_registro.sq_taxon_de||' para '||v_registro.sq_taxon_para||'.', now(), 'Adotado de '||v_registro.sq_taxon_de||' para '||v_registro.sq_taxon_para||'.
-- 	    st_ativo_sq_taxon_de => '||v_registro.st_ativo_sq_taxon_de||'
-- 	    st_del_sq_taxon_de => '||v_registro.st_del_sq_taxon_de||'
-- 	    st_update_fk => '||v_registro.st_update_fk||'
-- 	    '||v_msg_st_update_fk||'
-- 	    '||v_msg||'
-- 	    ...') RETURNING sq_etl_plan  into v_sq_etl_plan;
--
--             ELSE
--                 SELECT sq_etl_plan into v_sq_etl_plan FROM taxonomia.etl_plan where co_etl_plan = v_co_etl_plan ;
--             END IF;
--
--             RAISE INFO 'v_sq_etl_plan  % %',v_sq_etl_plan, E'\n';
--
--             --v_co_etl_operacao
--
--             IF v_registro.st_update_fk is true THEN
--
--                 RAISE INFO 'Gerando script de atualizacao de fk  (st_update_fk=%) %',v_registro.st_update_fk, E'\n';
--
--                 FOR v_registro_discovery IN
--
--                     select vw_discovery_fk_taxon.esquema_nome_tabela_pk as esquema_nome_tabela_pk,
--                            vw_discovery_fk_taxon.nome_tabela_pk as nome_tabela_pk,
--                            vw_discovery_fk_taxon.nome_coluna_pk as nome_coluna_pk,
--                            vw_discovery_fk_taxon.valor_pk as valor_pk,
--                            vw_discovery_fk_taxon.nome_coluna_fk as nome_coluna_fk,
--                            'select ''-- Atualizando '||vw_discovery_fk_taxon.esquema_nome_tabela_pk||'.'||vw_discovery_fk_taxon.nome_tabela_pk||' de ('||v_registro.sq_taxon_de||') para ('||v_registro.sq_taxon_para||')'' ' as cmd_sql_print,
--                            'UPDATE '||vw_discovery_fk_taxon.esquema_nome_tabela_pk||'.'||vw_discovery_fk_taxon.nome_tabela_pk||' SET '||vw_discovery_fk_taxon.nome_coluna_fk||'='||v_registro.sq_taxon_para||' WHERE '||vw_discovery_fk_taxon.nome_coluna_pk||' = '||vw_discovery_fk_taxon.valor_pk||' ' as cmd_sql_update,
--                            'UPDATE '||vw_discovery_fk_taxon.esquema_nome_tabela_pk||'.'||vw_discovery_fk_taxon.nome_tabela_pk||' SET '||vw_discovery_fk_taxon.nome_coluna_fk||'='||v_registro.sq_taxon_de||' WHERE '||vw_discovery_fk_taxon.nome_coluna_pk||' = '||vw_discovery_fk_taxon.valor_pk||' ' as cmd_sql_rollback,
--                            'SELECT * FROM '||vw_discovery_fk_taxon.esquema_nome_tabela_pk||'.'||vw_discovery_fk_taxon.nome_tabela_pk||' WHERE '||vw_discovery_fk_taxon.nome_coluna_pk||' = '||vw_discovery_fk_taxon.valor_pk||' ' as cmd_sql_select,
--                            'DO $do$ BEGIN IF NOT EXISTS ( SELECT FROM taxonomia.vw_discovery_fk_taxon  	WHERE valor_coluna_fk='||v_registro.sq_taxon_de||' ) THEN DELETE FROM taxonomia.taxon WHERE sq_taxon = '||v_registro.sq_taxon_de||'; END IF; END; $do$; ' as cmd_sql_delete_taxon
--                     FROM taxonomia.vw_discovery_fk_taxon WHERE vw_discovery_fk_taxon.valor_coluna_fk = v_registro.sq_taxon_de
--
--                     LOOP
--
--                         RAISE INFO 'sq_taxon_de  %  => sq_taxon_para % % sql => % %',v_registro.sq_taxon_de , v_registro.sq_taxon_para, E'\n', v_registro_discovery.cmd_sql_select, E'\n';
--
--                         --	    v_sql_row_data:= format('select array_to_json(array_agg(row_to_json(tb1)))::json  from ( %1$s ) tb1' , v_registro_discovery.cmd_sql_select);
--
--                         -- RAISE INFO 'v_sql_row_data %', v_sql_row_data;
--                         IF v_registro_discovery.cmd_sql_select IS NOT NULL THEN
--                             EXECUTE 'select array_to_json(array_agg(row_to_json(tb1)))  from ( '|| v_registro_discovery.cmd_sql_select||' ) tb1'  INTO js_row_data;
--                         ELSE
--                             js_row_data:=null;
--                         END IF;
--
--                         --RAISE INFO 'js_row_data %', js_row_data::text;
--
--                         INSERT INTO taxonomia.etl_cmd_sql(
--                             sq_etl_cmd_sql,
--                             nu_etl_cmd_sql,
--                             nm_esquema_pk,
--                             nm_tabela_pk,
--                             nm_coluna_pk,
--                             vl_coluna_pk,
--                             nm_coluna_fk,
--                             co_cmd,
--                             sql_cmd,
--                             sql_rollback,
--                             sql_print,
--                             sql_select,
--                             js_row_data,
--                             sq_etl_plan,
--                             sq_etl_taxon)
--                         VALUES (nextval('taxonomia.etl_cmd_sql_sq_etl_cmd_sql_seq'::regclass),
--                                 (SELECT COALESCE (MAX(nu_etl_cmd_sql),0)+1  FROM taxonomia.etl_plan
--                                                                                      LEFT JOIN  taxonomia.etl_cmd_sql ON etl_cmd_sql.sq_etl_plan = etl_plan.sq_etl_plan and etl_plan.sq_etl_plan = v_sq_etl_plan), --nu_etl_cmd_sql
--                                 v_registro_discovery.esquema_nome_tabela_pk, --nm_esquema_pk
--                                 v_registro_discovery.nome_tabela_pk, --nm_tabela_pk
--                                 v_registro_discovery.nome_coluna_pk, --nm_coluna_pk
--                                 v_registro_discovery.valor_pk, --vl_coluna_pk
--                                 v_registro_discovery.nome_coluna_fk, --nm_coluna_fk
--                                 'UPDATE', --co_cmd
--                                 v_registro_discovery.cmd_sql_update, --sql_cmd
--                                 v_registro_discovery.cmd_sql_rollback, --sql_rollback
--                                 v_registro_discovery.cmd_sql_print, --sql_print
--                                 v_registro_discovery.cmd_sql_select, --sql_select
--                                 js_row_data,
--                                 v_sq_etl_plan, --sq_etl_plan
--                                 v_registro.sq_etl_taxon --sq_etl_taxon
--                                );
--
--                     END LOOP;
--             END IF;
--
--
--
--             RAISE INFO 'v_registro.st_ativo_sq_taxon_de =>  % %',v_registro.st_ativo_sq_taxon_de , E'\n';
--
--             IF v_registro.st_ativo_sq_taxon_de IS FALSE THEN
--
--                 RAISE INFO 'Desativar o taxon sq_taxon=>  %  % sql => % %',v_registro.sq_taxon_de , E'\n', v_registro_discovery.cmd_sql_select, E'\n';
--
--                 js_row_data:=null;
--
--                 EXECUTE 'select array_to_json(array_agg(row_to_json(tb1)))  from ( SELECT *  FROM taxonomia.taxon WHERE sq_taxon = '||v_registro.sq_taxon_de||' ) tb1'  INTO js_row_data;
--
--                 INSERT INTO taxonomia.etl_cmd_sql(
--                     sq_etl_cmd_sql,
--                     nu_etl_cmd_sql,
--                     nm_esquema_pk,
--                     nm_tabela_pk,
--                     nm_coluna_pk,
--                     vl_coluna_pk,
--                     nm_coluna_fk,
--                     co_cmd,
--                     sql_cmd,
--                     sql_rollback,
--                     sql_print,
--                     sql_select,
--                     js_row_data,
--                     sq_etl_plan,
--                     sq_etl_taxon)
--                 VALUES (nextval('taxonomia.etl_cmd_sql_sq_etl_cmd_sql_seq'::regclass),
--                         (SELECT COALESCE (MAX(nu_etl_cmd_sql),0)+1  FROM taxonomia.etl_plan
--                                                                              LEFT JOIN  taxonomia.etl_cmd_sql ON etl_cmd_sql.sq_etl_plan = etl_plan.sq_etl_plan and etl_plan.sq_etl_plan = v_sq_etl_plan), --nu_etl_cmd_sql
--                         'taxonomia', --nm_esquema_pk
--                         'taxon', --nm_tabela_pk
--                         'sq_taxon', --nm_coluna_pk
--                         v_registro.sq_taxon_de, --vl_coluna_pk
--                         null, --nm_coluna_fk
--                         'UPDATE', --co_cmd
--                         'UPDATE taxonomia.taxon SET st_ativo=false WHERE sq_taxon='||v_registro.sq_taxon_de||'', --sql_cmd
--                         'UPDATE taxonomia.taxon SET st_ativo=true WHERE sq_taxon='||v_registro.sq_taxon_de||'', --sql_rollback
--                         'SELECT ''Desativado o TAXON sq_taxon = '||v_registro.sq_taxon_de||'''', --sql_print
--                         'SELECT *  FROM taxonomia.taxon WHERE sq_taxon = '||v_registro.sq_taxon_de||'', --sql_select
--                         js_row_data,
--                         v_sq_etl_plan, --sq_etl_plan
--                         v_registro.sq_etl_taxon --sq_etl_taxon
--                        );
--
--
--             END IF;
--
--             RAISE INFO 'v_registro.st_del_sq_taxon_de =>  % %',v_registro.st_del_sq_taxon_de , E'\n';
--
--             IF v_registro.st_del_sq_taxon_de IS TRUE THEN
--
--                 RAISE INFO 'Deletando taxon caso nao esteja sendo usado por ninguem sq_taxon=>  %  % sql => % %',v_registro.sq_taxon_de , E'\n', v_registro_discovery.cmd_sql_select, E'\n';
--
--                 js_row_data:=null;
--
--                 EXECUTE 'select array_to_json(array_agg(row_to_json(tb1)))  from ( SELECT *  FROM taxonomia.taxon WHERE sq_taxon = '||v_registro.sq_taxon_de||' ) tb1'  INTO js_row_data;
--
--                 INSERT INTO taxonomia.etl_cmd_sql(
--                     sq_etl_cmd_sql,
--                     nu_etl_cmd_sql,
--                     nm_esquema_pk,
--                     nm_tabela_pk,
--                     nm_coluna_pk,
--                     vl_coluna_pk,
--                     nm_coluna_fk,
--                     co_cmd,
--                     sql_cmd,
--                     sql_rollback,
--                     sql_print,
--                     sql_select,
--                     js_row_data,
--                     sq_etl_plan,
--                     sq_etl_taxon)
--                 VALUES (nextval('taxonomia.etl_cmd_sql_sq_etl_cmd_sql_seq'::regclass),
--                         (SELECT COALESCE (MAX(nu_etl_cmd_sql),0)+1  FROM taxonomia.etl_plan
--                                                                              LEFT JOIN  taxonomia.etl_cmd_sql ON etl_cmd_sql.sq_etl_plan = etl_plan.sq_etl_plan and etl_plan.sq_etl_plan = v_sq_etl_plan), --nu_etl_cmd_sql
--                         'taxonomia', --nm_esquema_pk
--                         'taxon', --nm_tabela_pk
--                         'sq_taxon', --nm_coluna_pk
--                         v_registro.sq_taxon_de, --vl_coluna_pk
--                         null, --nm_coluna_fk
--                         'DELETE', --co_cmd
--                         'IF NOT EXISTS ( SELECT FROM taxonomia.vw_discovery_fk_taxon WHERE valor_coluna_fk='||v_registro.sq_taxon_de||' ) THEN DELETE FROM taxonomia.taxon WHERE sq_taxon = '||v_registro.sq_taxon_de||'; END IF; ', --sql_cmd
--                         null, --sql_rollback
--                         'SELECT ''DELETE TAXON sq_taxon = '||v_registro.sq_taxon_de||'', --sql_print
--                         'SELECT *  FROM taxonomia.taxon WHERE sq_taxon = '||v_registro.sq_taxon_de||'', --sql_select
--                         js_row_data,
--                         v_sq_etl_plan, --sq_etl_plan
--                         v_registro.sq_etl_taxon --sq_etl_taxon
--                        );
--             END IF;
--
--         END LOOP;



EXCEPTION WHEN OTHERS THEN
    BEGIN

        RAISE EXCEPTION 'Erro: %', SQLERRM;

    END;

END;
$_$;


--
-- TOC entry 4648 (class 1255 OID 23812)
-- Name: etl_plan_get_obs_etl_plan(integer); Type: FUNCTION; Schema: taxonomia; Owner: -
--

CREATE FUNCTION taxonomia.etl_plan_get_obs_etl_plan(v_sq_etl_plan integer) RETURNS text
    LANGUAGE plpgsql
    AS $$
DECLARE
v_registro record;
v_de_msg_erro varchar;
v_co_etl_plan text;
v_msg_st_update_fk text;
v_msg_st_ativo_sq_taxon_de text;
v_msg_st_del_sq_taxon_de text;
v_msg text;
v_registro_taxon_de record;
v_registro_taxon_para record;
v_obs_etl_plan text;
    
BEGIN

 v_msg:='';

 FOR v_registro IN SELECT sq_etl_taxon,  etl_operacao.sq_etl_operacao, sq_taxon_de, sq_taxon_para,
        co_etl_operacao, ds_etl_operacao, st_update_fk, 
       st_ativo_sq_taxon_de, st_del_sq_taxon_de, etl_plan.obs_etl_plan,etl_plan.sq_etl_plan
  FROM taxonomia.etl_taxon
   INNER JOIN taxonomia.etl_plan ON etl_plan.sq_etl_plan =  etl_taxon.sq_etl_plan
   INNER JOIN taxonomia.etl_operacao ON etl_operacao.sq_etl_operacao = etl_plan.sq_etl_operacao
    where etl_plan.sq_etl_plan = v_sq_etl_plan
   LIMIT 1
  LOOP

  v_obs_etl_plan := v_registro.obs_etl_plan;  
  v_msg_st_update_fk := null;
  v_msg_st_ativo_sq_taxon_de := null;
  v_msg_st_del_sq_taxon_de:= null;

RAISE INFO 'v_msg  => >>> % %', v_msg, E'\n';

    v_co_etl_plan:=v_registro.co_etl_operacao||'['||v_registro.sq_taxon_de||']to['||v_registro.sq_taxon_para||']  ';

    RAISE INFO 'v_co_etl_plan  % %',v_co_etl_plan, E'\n'; 
      v_msg:= v_msg ||' -- co_etl_plan => '|| v_co_etl_plan ||' '|| E'\n'; 


	  FOR v_registro_taxon_de IN 
	    SELECT sq_taxon, sq_taxon_pai, no_taxon, no_taxon_formatado, st_ativo, 
	       json_trilha, sq_nivel_taxonomico, ds_caminho_trilha, in_ocorre_brasil, 
	       de_nivel_taxonomico, nu_grau_taxonomico, in_nivel_intermediario, 
	       co_nivel_taxonomico
	    FROM taxonomia.mv_taxonomia where sq_taxon=v_registro.sq_taxon_de

	  LOOP
	  --RAISE INFO ' --1) v_registro_taxon_de  % %',v_registro_taxon_de.sq_taxon, E'\n'; 
	  
	    v_msg:= v_msg||E'\n'||E'\n'||E'\n'
		    ||'-- Taxon de:'||E'\n' 
		    ||'--  - ds_caminho_trilha: '||v_registro_taxon_de.ds_caminho_trilha::text||E'\n' 
		    ||'--  - sq_taxon: '||v_registro_taxon_de.sq_taxon::text||E'\n' 
		    ||'--  - sq_taxon_pai: '||v_registro_taxon_de.sq_taxon_pai::text||E'\n' 
		    ||'--  - no_taxon_formatado: '||v_registro_taxon_de.no_taxon_formatado::text||E'\n' 
		    ||'--  - json_trilha: '||v_registro_taxon_de.json_trilha::text||E'\n'    
		    ||'--  - de_nivel_taxonomico: '||v_registro_taxon_de.de_nivel_taxonomico::text||E'\n' 
		    ||'--  - sq_nivel_taxonomico: '||v_registro_taxon_de.sq_nivel_taxonomico::text||E'\n'
		    ;
	  END LOOP;

	if v_registro.sq_taxon_para IS NOT NULL THEN
		FOR v_registro_taxon_para IN 
		    SELECT sq_taxon, sq_taxon_pai, no_taxon, no_taxon_formatado, st_ativo, 
		       json_trilha, sq_nivel_taxonomico, ds_caminho_trilha, in_ocorre_brasil, 
		       de_nivel_taxonomico, nu_grau_taxonomico, in_nivel_intermediario, 
		       co_nivel_taxonomico
		    FROM taxonomia.mv_taxonomia where sq_taxon=v_registro.sq_taxon_para

		  LOOP
		    v_msg:= v_msg||E'\n'
		    ||'-- Taxon de:'||E'\n' 
		    ||'--  - ds_caminho_trilha: '||v_registro_taxon_para.ds_caminho_trilha::text||E'\n' 
		    ||'--  - sq_taxon: '||v_registro_taxon_para.sq_taxon::text||E'\n' 
		    ||'--  - sq_taxon_pai: '||v_registro_taxon_para.sq_taxon_pai::text||E'\n' 
		    ||'--  - no_taxon_formatado: '||v_registro_taxon_para.no_taxon_formatado::text||E'\n' 
		    ||'--  - json_trilha: '||v_registro_taxon_para.json_trilha::text||E'\n'    
		    ||'--  - de_nivel_taxonomico: '||v_registro_taxon_para.de_nivel_taxonomico::text||E'\n' 
		    ||'--  - sq_nivel_taxonomico: '||v_registro_taxon_para.sq_nivel_taxonomico::text||E'\n'
		    ;
		    
		  END LOOP;
	END IF;
	
	IF v_msg_st_update_fk IS NULL AND v_registro.st_update_fk IS TRUE THEN
		v_msg_st_update_fk:='-- Todas as referencias (chaves estrangeiras) serão atualizadas para o novo valor. [ st_update_fk => '||v_registro.st_update_fk||']';
		RAISE INFO '-- Gerando script de atualizacao de fk  (st_update_fk=%) %',v_registro.st_update_fk, E'\n';
	END IF;

  
	IF v_msg_st_ativo_sq_taxon_de IS NULL AND v_registro.st_ativo_sq_taxon_de IS FALSE THEN
	        v_msg_st_ativo_sq_taxon_de:='-- Desativar o taxon sq_taxon_de =>  '|| v_registro.sq_taxon_de || E'\n';
	        RAISE INFO '-- Desativar o taxon sq_taxon_de =>  % . % ',v_registro.sq_taxon_de , E'\n';

	END IF;


	IF v_msg_st_del_sq_taxon_de IS NULL AND  v_registro.st_del_sq_taxon_de IS TRUE THEN
		v_msg_st_del_sq_taxon_de:='-- Habilitado a operação de deletar o taxon caso nao esteja sendo usado por ninguem sq_taxon_de=>  '||v_registro.sq_taxon_de||' '||E'\n';

		RAISE INFO '-- Habilitado a operação de deletar o taxon caso nao esteja sendo usado por ninguem sq_taxon=>  % % ',v_registro.sq_taxon_de , E'\n';

	END IF;

	
    
END LOOP;
    return v_msg_st_update_fk ||E'\n'||v_msg_st_ativo_sq_taxon_de ||E'\n'||v_msg_st_del_sq_taxon_de ||' ' ||E'\n'||E'\n'|| v_msg;
--RETURN 'v_obs_etl_plan => '||v_obs_etl_plan || E'\n' || 'v_msg => '||v_msg;

EXCEPTION WHEN OTHERS THEN 
  BEGIN 
  v_de_msg_erro:='ERRO EM EXECUTAR exec_etl_taxon';

    
  RAISE NOTICE 'Detalhamento do erro no banco de dados exec_etl_taxon :%',SQLERRM;  
  
	RAISE EXCEPTION 'Erro: %', v_de_msg_erro; 

  END;

END;
$$;


--
-- TOC entry 4649 (class 1255 OID 23813)
-- Name: fn_get_sq_taxon_search_trilha(text, text, text, text, text, text, text, text); Type: FUNCTION; Schema: taxonomia; Owner: -
--

CREATE FUNCTION taxonomia.fn_get_sq_taxon_search_trilha(subespecie text DEFAULT ''::text, especie text DEFAULT ''::text, genero text DEFAULT ''::text, familia text DEFAULT ''::text, ordem text DEFAULT ''::text, classe text DEFAULT ''::text, filo text DEFAULT ''::text, reino text DEFAULT ''::text) RETURNS json
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_sql text;
    v_where text;
    v_sq_nivel_taxonomico integer;
    v_row_json json;
BEGIN
v_where:='';
v_sq_nivel_taxonomico:=null;

if subespecie IS NOT NULL 
 AND especie IS NOT NULL  
  AND genero IS NOT NULL  THEN 
        v_where:=v_where||' taxon.json_trilha ->''subespecie''->>''no_taxon''='''||genero||' '||especie||' '||subespecie||'''';
        if v_sq_nivel_taxonomico is null then
		v_sq_nivel_taxonomico:=8;
        end if; 
end if;

if especie IS NOT NULL  AND genero IS NOT NULL THEN  
	if v_where <> '' then
		v_where:=v_where||' AND ';
	end if;
	v_where:=v_where||' taxon.json_trilha ->''especie''->>''no_taxon''='''||genero||' '||especie||'''';
        if v_sq_nivel_taxonomico is null then
		v_sq_nivel_taxonomico:=7;
        end if; 
end if;

if genero IS NOT NULL THEN  
	if v_where <> '' then
		v_where:=v_where||' AND ';
	end if;
            v_where:=v_where||' taxon.json_trilha->''genero''->>''no_taxon''='''||genero||'''';
        if v_sq_nivel_taxonomico is null then
		v_sq_nivel_taxonomico:=6;
        end if; 
end if;

if familia IS NOT NULL THEN  
	if v_where <> '' then
		v_where:=v_where||' AND ';
	end if;
              v_where:=v_where||' taxon.json_trilha->''familia''->>''no_taxon''='''||familia||'''';
       if v_sq_nivel_taxonomico is null then
		v_sq_nivel_taxonomico:=5;
        end if; 
end if;

if ordem IS NOT NULL THEN  
	if v_where <> '' then
		v_where:=v_where||' AND ';
	end if;
              v_where:=v_where||' taxon.json_trilha->''ordem''->>''no_taxon''='''||ordem||'''';
        if v_sq_nivel_taxonomico is null then
		v_sq_nivel_taxonomico:=4;
        end if; 
end if;

if classe IS NOT NULL THEN
  
	if v_where <> '' then
		v_where:=v_where||' AND ';
	end if; 
              v_where:=v_where||' taxon.json_trilha->''classe''->>''no_taxon''='''||classe||'''';
       if v_sq_nivel_taxonomico is null then
		v_sq_nivel_taxonomico:=3;
        end if; 
end if;

if filo IS NOT NULL THEN
  
	if v_where <> '' then
		v_where:=v_where||' AND ';
	end if;
              v_where:=v_where||' taxon.json_trilha->''filo''->>''no_taxon''='''||filo||'''';
        if v_sq_nivel_taxonomico is null then
		v_sq_nivel_taxonomico:=2;
        end if; 
end if;

if reino IS NOT NULL THEN
  
	if v_where <> '' then
		v_where:=v_where||' AND ';
	end if;
              v_where:=v_where||' taxon.json_trilha->''reino''->>''no_taxon''='''||reino||'''';
        if v_sq_nivel_taxonomico is null then
		v_sq_nivel_taxonomico:=1;
        end if; 
end if;


v_sql:= ' select array_to_json(array_agg( row_to_json(tb1) ) )
  FROM (
  select sq_taxon, sq_taxon_pai, json_trilha
  FROM taxonomia.taxon
  WHERE '||v_where ||' 
  AND sq_nivel_taxonomico='||v_sq_nivel_taxonomico||'
  ) as tb1'
  ;

if  subespecie is null
    AND especie is null
    AND genero is null
    AND familia is null
    AND ordem is null
    AND classe is null
    AND filo is null
    AND reino is null THEN

    return null;

else

    execute v_sql into v_row_json;

       return json_build_object(
            'status',  0,
            'msg', '',
            'total_sq_taxon_nivel',coalesce(JSON_ARRAY_LENGTH(v_row_json),0),
            'data',v_row_json);

end if;
EXCEPTION WHEN OTHERS THEN
    BEGIN

        RAISE EXCEPTION 'taxonomia.Erro fn_get_sq_taxon_search_trilha : [ % ] %', v_sql, SQLERRM;

    END;

END;
$$;


--
-- TOC entry 13238 (class 0 OID 0)
-- Dependencies: 4649
-- Name: FUNCTION fn_get_sq_taxon_search_trilha(subespecie text, especie text, genero text, familia text, ordem text, classe text, filo text, reino text); Type: COMMENT; Schema: taxonomia; Owner: -
--

COMMENT ON FUNCTION taxonomia.fn_get_sq_taxon_search_trilha(subespecie text, especie text, genero text, familia text, ordem text, classe text, filo text, reino text) IS 'creação do sql para pesquisar os taxon no sintax.';


--
-- TOC entry 4650 (class 1255 OID 23814)
-- Name: fn_planilha_sintax_add_taxon(text); Type: FUNCTION; Schema: taxonomia; Owner: -
--

CREATE FUNCTION taxonomia.fn_planilha_sintax_add_taxon(p_no_tmp_table text) RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_sql text;
    v_no_tmp text;
BEGIN

    v_no_tmp:=p_no_tmp_table||'_erro';

    v_sql:='CREATE UNLOGGED TABLE IF NOT EXISTS taxonomia.'||p_no_tmp_table||'_taxon AS (
with sel AS (
	select (m.value->>''planilha_linha'')::integer AS planilha_linha
		    , (m.value)::jsonb as value
		    FROM
		    json_array_elements(
			(select array_to_json(array_agg( row_to_json(tb1) ) )   from  (
										      select *
										      from taxonomia.'||p_no_tmp_table||'

										      where planilha_linha not in (SELECT distinct planilha_linha
											  FROM taxonomia.'||p_no_tmp_table||'_erro
											)
											--and genero = ''Petrosia''
										      ) tb1
			)::json
		    ) AS m
	)
, sel_planilha_trilha AS (
	select planilha_linha,
          nivel_taxonomico.sq_nivel_taxonomico,
	  nivel_taxonomico.de_nivel_taxonomico,
	  nivel_taxonomico.nu_grau_taxonomico,
	  nivel_taxonomico.in_nivel_intermediario,
	  lower(nivel_taxonomico.co_nivel_taxonomico) as co_nivel_taxonomico,
           sel.value as linha_json,
		(CASE
			when t.key = ''subespecie'' then concat_ws('' < '',concat(INITCAP(trim(sel.value->>''genero'')), '' '',lower(trim(sel.value->>''especie'')), '' '',lower(trim(sel.value->>''subespecie''))), concat(INITCAP(trim(sel.value->>''genero'')), '' '',lower(trim(sel.value->>''especie''))), INITCAP(trim(sel.value->>''genero'')),INITCAP(trim(sel.value->>''familia'')),  INITCAP(trim(sel.value->>''ordem'')),INITCAP(trim(sel.value->>''classe'')), INITCAP(trim(sel.value->>''filo'')) , INITCAP(trim(sel.value->>''reino'' )))
			when t.key = ''especie'' then  concat_ws('' < '', concat(INITCAP(trim(sel.value->>''genero'')), '' '',lower(trim(sel.value->>''especie''))), INITCAP(trim(sel.value->>''genero'')),INITCAP(trim(sel.value->>''familia'')),  INITCAP(trim(sel.value->>''ordem'')),INITCAP(trim(sel.value->>''classe'')), INITCAP(trim(sel.value->>''filo'')) , INITCAP(trim(sel.value->>''reino'' )))
			when t.key = ''genero'' then concat_ws('' < '', INITCAP(trim(sel.value->>''genero'')),INITCAP(trim(sel.value->>''familia'')),  INITCAP(trim(sel.value->>''ordem'')),INITCAP(trim(sel.value->>''classe'')), INITCAP(trim(sel.value->>''filo'')) , INITCAP(trim(sel.value->>''reino'' )))
			when t.key = ''familia'' then concat_ws('' < '', INITCAP(trim(sel.value->>''familia'')),  INITCAP(trim(sel.value->>''ordem'')),INITCAP(trim(sel.value->>''classe'')), INITCAP(trim(sel.value->>''filo'')) , INITCAP(trim(sel.value->>''reino'' )))
			when t.key = ''ordem'' then concat_ws('' < '', INITCAP(trim(sel.value->>''ordem'')),INITCAP(trim(sel.value->>''classe'')), INITCAP(trim(sel.value->>''filo'')) , INITCAP(trim(sel.value->>''reino'' )))
			when t.key = ''classe'' then concat_ws('' < '',INITCAP(trim(sel.value->>''classe'')), INITCAP(trim(sel.value->>''filo'')) , INITCAP(trim(sel.value->>''reino'' )))
			when t.key = ''filo'' then  concat_ws('' < '', INITCAP(trim(sel.value->>''filo'')) , INITCAP(trim(sel.value->>''reino'' )))
			when t.key = ''reino'' then INITCAP(trim(sel.value->>''reino'' ))
			else null
			end
			) as trilha_planilha ,
		(CASE
			when t.key = ''subespecie'' then concat_ws('' < '', concat(INITCAP(trim(sel.value->>''genero'')), '' '',lower(trim(sel.value->>''especie''))), INITCAP(trim(sel.value->>''genero'')),INITCAP(trim(sel.value->>''familia'')),  INITCAP(trim(sel.value->>''ordem'')),INITCAP(trim(sel.value->>''classe'')), INITCAP(trim(sel.value->>''filo'')) , INITCAP(trim(sel.value->>''reino'' )))
			when t.key = ''especie'' then concat_ws('' < '', INITCAP(trim(sel.value->>''genero'')),INITCAP(trim(sel.value->>''familia'')),  INITCAP(trim(sel.value->>''ordem'')),INITCAP(trim(sel.value->>''classe'')), INITCAP(trim(sel.value->>''filo'')) , INITCAP(trim(sel.value->>''reino'' )))
			when t.key = ''genero'' then concat_ws('' < '', INITCAP(trim(sel.value->>''familia'')),  INITCAP(trim(sel.value->>''ordem'')),INITCAP(trim(sel.value->>''classe'')), INITCAP(trim(sel.value->>''filo'')) , INITCAP(trim(sel.value->>''reino'' )))
			when t.key = ''familia'' then concat_ws('' < '', INITCAP(trim(sel.value->>''ordem'')),INITCAP(trim(sel.value->>''classe'')), INITCAP(trim(sel.value->>''filo'')) , INITCAP(trim(sel.value->>''reino'' )))
			when t.key = ''ordem'' then concat_ws('' < '', INITCAP(trim(sel.value->>''classe'')), INITCAP(trim(sel.value->>''filo'')) , INITCAP(trim(sel.value->>''reino'' )))
			when t.key = ''classe'' then concat_ws('' < '', INITCAP(trim(sel.value->>''filo'')) , INITCAP(trim(sel.value->>''reino'' )))
			when t.key = ''filo'' then  INITCAP(trim(sel.value->>''reino'' ))
			when t.key = ''reino'' then null
			else null
			end
			) as trilha_planilha_pai ,

		t.key as coluna,
		case
		   when t.key in (''especie'',''subespecie'') then lower(trim(t.value))
		   else INITCAP(trim(t.value))
		 end as no_taxon
	from sel
	CROSS JOIN LATERAL  jsonb_each_text(sel.value) t
	INNER JOIN taxonomia.nivel_taxonomico on lower(co_nivel_taxonomico) = t.key
	WHERE in_nivel_intermediario is false

)

--select * from sel_planilha_trilha --where trilha_planilha ilike ''Desmacellidae%''

, sel_taxon as (
	select
	tb1.planilha_linhas,
	case
	  when tb1.co_nivel_taxonomico in (''subespecie'',''especie'',''genero'',''familia'',''ordem'',''classe'',''filo'',''reino'') then  sel.value->>''reino''
	end  as reino
	,case
	  when tb1.co_nivel_taxonomico in (''subespecie'',''especie'',''genero'',''familia'',''ordem'',''classe'',''filo'') then  sel.value->>''filo''
	end  as filo
	,case
	  when tb1.co_nivel_taxonomico in (''subespecie'',''especie'',''genero'',''familia'',''ordem'',''classe'') then  sel.value->>''classe''
	end  as classe
	,case
	  when tb1.co_nivel_taxonomico in (''subespecie'',''especie'',''genero'',''familia'',''ordem'') then  sel.value->>''ordem''
	end  as ordem
	,case
	  when tb1.co_nivel_taxonomico in (''subespecie'',''especie'',''genero'',''familia'') then  sel.value->>''familia''
	end  as familia
	,case
	  when tb1.co_nivel_taxonomico in (''subespecie'',''especie'',''genero'') then  sel.value->>''genero''
	end  as genero
	,case
	  when tb1.co_nivel_taxonomico in (''subespecie'',''especie'') then  sel.value->>''especie''
	end  as especie
	,case
	  when tb1.co_nivel_taxonomico = ''subespecie'' then  sel.value->>''subespecie''
	end  as subespecie,
	sel.value->>''sintax_recorte'' as sintax_recorte
	,tb1.sq_nivel_taxonomico,
	  tb1.de_nivel_taxonomico,
	  tb1.nu_grau_taxonomico,
	  tb1.in_nivel_intermediario,
	  tb1.co_nivel_taxonomico,
	  tb1.trilha_planilha,
	  --tb1.trilha_taxon,,
	  tb1.trilha_planilha_pai,
	  tb1.no_taxon
	from
	 (
	 select array_agg(distinct planilha_linha)  as planilha_linhas,
	sel_planilha_trilha.sq_nivel_taxonomico,
		  sel_planilha_trilha.de_nivel_taxonomico,
		  sel_planilha_trilha.nu_grau_taxonomico,
		  sel_planilha_trilha.in_nivel_intermediario,
		  sel_planilha_trilha.co_nivel_taxonomico,
		  trilha_planilha,
		 -- string_to_array(trilha_planilha,'' < '') as trilha_taxon,
		  trilha_planilha_pai,
		  no_taxon
	  from sel_planilha_trilha
         --where trilha_planilha ilike ''Desmacellidae%''
	  group by sel_planilha_trilha.sq_nivel_taxonomico,
		  sel_planilha_trilha.de_nivel_taxonomico,
		  sel_planilha_trilha.nu_grau_taxonomico,
		  sel_planilha_trilha.in_nivel_intermediario,
		  sel_planilha_trilha.co_nivel_taxonomico,
		  trilha_planilha,
		  trilha_planilha_pai,
		  no_taxon
		  ) as tb1
		  inner join sel on sel.planilha_linha = tb1.planilha_linhas[1]


)
--select * from sel_taxon

, sel_sq_taxon as (
select
planilha_linhas,
(CASE
			when sel_taxon.co_nivel_taxonomico = ''subespecie'' then  taxonomia.fn_get_sq_taxon_search_trilha( lower(trim(sel_taxon.subespecie)),
												 lower(trim(sel_taxon.especie)),
												 INITCAP(trim(sel_taxon.genero)),
											      case
												when sel_taxon.sintax_recorte =''familia'' then sel_taxon.familia
												else null
											      end,
												 case
												when sel_taxon.sintax_recorte =''ordem'' then sel_taxon.ordem
												else null
											      end,
												 case
												when sel_taxon.sintax_recorte =''classe'' then sel_taxon.classe
												else null
											      end,
												 case
												when sel_taxon.sintax_recorte =''filo'' then sel_taxon.filo
												else null
											      end,
												 case
												when sel_taxon.sintax_recorte =''reino'' then sel_taxon.reino
												else null
											      end
			    )
			when sel_taxon.co_nivel_taxonomico = ''especie'' then  taxonomia.fn_get_sq_taxon_search_trilha( null,
											       lower(trim(sel_taxon.especie)),
											      INITCAP(trim(sel_taxon.genero)),
											      case
												when sel_taxon.sintax_recorte =''familia'' then sel_taxon.familia
												else null
											      end,
											       case
												when sel_taxon.sintax_recorte =''ordem'' then sel_taxon.ordem
												else null
											      end,
											      case
												when sel_taxon.sintax_recorte =''classe'' then sel_taxon.classe
												else null
											      end,
											      case
												when sel_taxon.sintax_recorte =''filo'' then sel_taxon.filo
												else null
											      end,
												 case
												when sel_taxon.sintax_recorte =''reino'' then sel_taxon.reino
												else null
											      end
			    )
			when sel_taxon.co_nivel_taxonomico = ''genero'' then  taxonomia.fn_get_sq_taxon_search_trilha( null,
											     null,
											     trim(INITCAP(trim(sel_taxon.genero))),

											      case
												when sel_taxon.sintax_recorte =''familia'' then sel_taxon.familia
												else null
											      end,
											      case
												when sel_taxon.sintax_recorte =''ordem'' then sel_taxon.ordem
												else null
											      end,
											     case
												when sel_taxon.sintax_recorte =''classe'' then sel_taxon.classe
												else null
											      end,
											     case
												when sel_taxon.sintax_recorte =''filo'' then sel_taxon.filo
												else null
											      end,
												case
												when sel_taxon.sintax_recorte =''reino'' then sel_taxon.reino
												else null
											      end
			    )
			when sel_taxon.co_nivel_taxonomico = ''familia'' then  taxonomia.fn_get_sq_taxon_search_trilha( null,
											      null,
											      null,
											      INITCAP(trim(sel_taxon.familia)),
											       case
												when sel_taxon.sintax_recorte =''ordem'' then sel_taxon.ordem
												else null
											      end,
											      case
												when sel_taxon.sintax_recorte =''classe'' then sel_taxon.classe
												else null
											      end,
											      case
												when sel_taxon.sintax_recorte =''filo'' then sel_taxon.filo
												else null
											      end,
												 case
												when sel_taxon.sintax_recorte =''reino'' then sel_taxon.reino
												else null
											      end
			    )
			when sel_taxon.co_nivel_taxonomico = ''ordem'' then  taxonomia.fn_get_sq_taxon_search_trilha( null,
											    null,
											    null,
											    null,
											    INITCAP(trim(sel_taxon.ordem)),
											    case
												when sel_taxon.sintax_recorte =''classe'' then sel_taxon.classe
												else null
											    end,
											    case
												when sel_taxon.sintax_recorte =''filo'' then sel_taxon.filo
												else null
											      end,
											       case
												when sel_taxon.sintax_recorte =''reino'' then sel_taxon.reino
												else null
											      end
			    )
			when sel_taxon.co_nivel_taxonomico = ''classe'' then  taxonomia.fn_get_sq_taxon_search_trilha( null,
											     null,
											     null,
											     null,
											     null,
											     INITCAP(trim(sel_taxon.classe)),
											     case
												when sel_taxon.sintax_recorte =''filo'' then sel_taxon.filo
												else null
											      end,
												case
												when sel_taxon.sintax_recorte =''reino'' then sel_taxon.reino
												else null
											      end
			    )
			when sel_taxon.co_nivel_taxonomico = ''filo'' then  taxonomia.fn_get_sq_taxon_search_trilha( null,
											   null,
											   null,
											   null,
											   null,
											   null,
											   INITCAP(trim(sel_taxon.filo)),
											      case
												when sel_taxon.sintax_recorte =''reino'' then sel_taxon.reino
												else null
											      end
			    )

			when sel_taxon.co_nivel_taxonomico = ''reino'' then  taxonomia.fn_get_sq_taxon_search_trilha( null,
											   null,
											   null,
											   null,
											   null,
											   null,
											   null,
											   INITCAP(trim(sel_taxon.reino))
			    )
			else null
			end
		    )::jsonb
		    --|| sel.value::jsonb )
		    as json_taxon
		    --(sel.value::jsonb || json_taxon::jsonb )::json as taxon_json
,
reino,
filo,
classe,
ordem,
familia,
genero,
especie,
subespecie,
sintax_recorte,
sq_nivel_taxonomico,
de_nivel_taxonomico,
nu_grau_taxonomico,
in_nivel_intermediario,
co_nivel_taxonomico,
trilha_planilha,
trilha_planilha_pai,
no_taxon
from sel_taxon

)
,sel_taxon_new  as (
  select
  sel_sq_taxon.planilha_linhas,
sel_sq_taxon.json_taxon,

(sel_sq_taxon.json_taxon->''data''->0->>''sq_taxon'')::integer as sintax_sq_taxon,
case
  when (count(sel_sq_taxon.json_taxon->''data''->0->>''sq_taxon'') OVER (PARTITION BY sel_sq_taxon.json_taxon->''data''->0->>''sq_taxon'')) <2 then coalesce((sel_sq_taxon.json_taxon->''data''->0->>''sq_taxon'')::integer,nextval(''taxonomia.taxon_sq_taxon_seq''))
end
 as sq_taxon
,count(sel_sq_taxon.json_taxon->''data''->0->>''sq_taxon'') OVER (PARTITION BY sel_sq_taxon.json_taxon->''data''->0->>''sq_taxon'') as depois_carga_total_taxon_duplicado_nivel
,taxonomia.formatar_trilha_taxon((sel_sq_taxon.json_taxon->''data''->0->>''json_trilha'')::json) as sintax_trilha_de
	,(((sel_sq_taxon.json_taxon->''data''->0->>''sq_taxon'')::integer is null and ((sel_sq_taxon.json_taxon->>''total_sq_taxon_nivel'')::integer) = 0)) as new_taxon
	, (sel_sq_taxon.json_taxon->>''total_sq_taxon_nivel'')::integer as total_sq_taxon_nivel
	, trilha_planilha
	,(sel_sq_taxon.json_taxon->''data''->0->>''sq_taxon_pai'')::integer as sq_taxon_pai
	,trilha_planilha_pai
	,co_nivel_taxonomico
	,no_taxon,
--	,sel_sq_taxon.m_value::jsonb as m_value
reino,
filo,
classe,
ordem,
familia,
genero,
especie,
subespecie,
sintax_recorte,
sq_nivel_taxonomico,
de_nivel_taxonomico,
nu_grau_taxonomico,
in_nivel_intermediario
from sel_sq_taxon

)
select
sel_taxon_new.planilha_linhas,
sel_taxon_new.json_taxon,
sel_taxon_new.sintax_sq_taxon,
sel_taxon_new.sq_taxon,
sel_taxon_new.new_taxon,
sel_taxon_new.sintax_trilha_de,
sel_taxon_new.depois_carga_total_taxon_duplicado_nivel,
sel_taxon_new.total_sq_taxon_nivel as antes_carga_total_taxon_duplicado_nivel,
sel_taxon_new.trilha_planilha,
sel_taxon_pai.sq_taxon as new_sq_taxon_pai,
sel_taxon_new.sq_taxon_pai as old_sq_taxon_pai,
sel_taxon_pai.sq_taxon as sq_taxon_pai,
sel_taxon_new.trilha_planilha_pai,
sel_taxon_new.co_nivel_taxonomico,
sel_taxon_new.no_taxon,
sel_taxon_new.reino,
sel_taxon_new.filo,
sel_taxon_new.classe,
sel_taxon_new.ordem,
sel_taxon_new.familia,
sel_taxon_new.genero,
sel_taxon_new.especie,
sel_taxon_new.subespecie,
sel_taxon_new.sintax_recorte,
sel_taxon_new.sq_nivel_taxonomico,
sel_taxon_new.de_nivel_taxonomico,
sel_taxon_new.nu_grau_taxonomico,
sel_taxon_new.in_nivel_intermediario,
case
  when sel_taxon_new.depois_carga_total_taxon_duplicado_nivel > 1 then ''ERRO: Não será possivel migrar este taxon porque o nível taxonômico estará duplicado apos a carga.''


end as OBS
  from sel_taxon_new
left join sel_taxon_new as sel_taxon_pai on sel_taxon_pai.trilha_planilha = sel_taxon_new.trilha_planilha_pai
 );





-- INSERT INTO taxonomia.' || p_no_tmp_table || '_erro(erro_descricao, erro_coluna, erro_valor, planilha_linha)
-- with sel AS (
-- SELECT obs,co_nivel_taxonomico,no_taxon,unnest(planilha_linhas) as planilha_linha
--   FROM taxonomia.' || p_no_tmp_table || '_taxon
--   where depois_carga_total_taxon_duplicado_nivel > 1
-- )
-- select
-- sel.obs
-- ,sel.co_nivel_taxonomico
-- ,sel.no_taxon
-- ,sel.planilha_linha
--  from
-- sel
--   LEFT JOIN taxonomia.' || p_no_tmp_table || '_erro
--          ON ' || p_no_tmp_table || '_erro.erro_descricao = sel.obs
--         AND ' || p_no_tmp_table || '_erro.planilha_linha =  sel.planilha_linha
-- WHERE ' || p_no_tmp_table || '_erro.erro_descricao is null;



INSERT INTO taxonomia.'||p_no_tmp_table||'_erro(erro_descricao, erro_coluna, erro_valor, planilha_linha)
    with sel AS (
        SELECT obs,co_nivel_taxonomico,no_taxon,unnest(planilha_linhas) as planilha_linha, trilha_planilha
        FROM taxonomia.'||p_no_tmp_table||'_taxon
        where antes_carga_total_taxon_duplicado_nivel > 1
          AND '||p_no_tmp_table||'_taxon.co_nivel_taxonomico in (''especie'',''subespecie'')' ||
           'AND no_taxon is not null
    )
    select
        concat(''ERRO: O táxon "'', trilha_planilha,''" se encontra duplicado no sintax, no nível de "'',(nivel_taxonomico.de_nivel_taxonomico),''". Favor proceder com a etapa de definição do táxon adotado por lista de referência.'')
         ,sel.co_nivel_taxonomico
         ,sel.no_taxon
         ,sel.planilha_linha
    from
        sel
    LEFT JOIN taxonomia.'||p_no_tmp_table||'_erro as erro
           ON erro.erro_descricao = sel.obs
          AND erro.planilha_linha =  sel.planilha_linha
    INNER JOIN taxonomia.nivel_taxonomico on nivel_taxonomico.co_nivel_taxonomico = upper(sel.co_nivel_taxonomico)
WHERE  erro.erro_descricao is null;



CREATE UNLOGGED TABLE IF NOT EXISTS taxonomia.'||p_no_tmp_table||'_bkp_taxon as (
select *
  from taxonomia.taxon
 where taxon.sq_taxon in (
	SELECT distinct sintax_sq_taxon
	  FROM taxonomia.'||p_no_tmp_table||'_taxon
	   WHERE (planilha_linhas <@ (coalesce((SELECT array_agg(distinct planilha_linha)
                               FROM taxonomia.'||p_no_tmp_table||'_erro),''{0}'')
				    )
		) IS FALSE
	   AND new_taxon is false
	)
);



CREATE UNLOGGED TABLE IF NOT EXISTS  taxonomia.'||p_no_tmp_table||'_historico as (
WITH sel as (
SELECT planilha_linhas, json_taxon, sintax_sq_taxon, sq_taxon, new_taxon,
       sintax_trilha_de, depois_carga_total_taxon_duplicado_nivel as depois_carga_total_taxon_nivel, antes_carga_total_taxon_duplicado_nivel as antes_carga_total_taxon_nivel,
       trilha_planilha, new_sq_taxon_pai, old_sq_taxon_pai, sq_taxon_pai,
       trilha_planilha_pai, co_nivel_taxonomico, no_taxon, reino, filo,
       classe, ordem, familia, genero, especie, subespecie, sintax_recorte,
       sq_nivel_taxonomico, de_nivel_taxonomico, nu_grau_taxonomico,
       in_nivel_intermediario, obs
  FROM taxonomia.'||p_no_tmp_table||'_taxon
   WHERE (planilha_linhas <@ (coalesce((SELECT array_agg(distinct planilha_linha)
                               FROM taxonomia.'||p_no_tmp_table||'_erro),''{0}'')
                            )
	) IS FALSE
    AND no_taxon is not null
), sel_ins AS (
INSERT INTO taxonomia.taxon(sq_taxon, sq_taxon_pai, sq_nivel_taxonomico, sq_situacao_nome, no_taxon, st_ativo,  in_ocorre_brasil, ts_ultima_alteracao)
select
sel.sq_taxon
 , sel.sq_taxon_pai
 , sel.sq_nivel_taxonomico
 , 1  as sq_situacao_nome
 , sel.no_taxon
 , true
 , true
 , now()
from sel
left join taxonomia.taxon t ON  t.sq_taxon = sel.sq_taxon
where new_taxon is true
  and  t.sq_taxon is null
order by nu_grau_taxonomico
returning sq_taxon
), sel_upd_sq_pai AS (
UPDATE taxonomia.taxon
   SET sq_taxon_pai=tb1.sq_taxon_pai
from (
select
   sel.sq_taxon
 , sel.sq_taxon_pai
 , sel.sq_nivel_taxonomico
 , 1  as sq_situacao_nome
 , sel.no_taxon
 , true
 , true
 , now()
 ,sel.sintax_sq_taxon, sel.new_taxon,
       sel.sintax_trilha_de,
       sel.new_sq_taxon_pai,
       sel.old_sq_taxon_pai
from sel
--left join taxonomia.taxon t ON  t.sq_taxon = sel.sq_taxon AND t.sq_taxon_pai <> sel.new_sq_taxon_pai
where new_taxon is false
  and  sel.new_sq_taxon_pai <>  sel.old_sq_taxon_pai
--   and  t.sq_taxon is null
  ) as tb1
  where taxon.sq_taxon = tb1.sq_taxon
  RETURNING tb1.sq_taxon,tb1.sq_taxon_pai,tb1.old_sq_taxon_pai,tb1.new_sq_taxon_pai
)
, sel_upd_autor_ano AS (
UPDATE taxonomia.taxon
   SET no_autor=tb1.autor
     , nu_ano=tb1.ano::integer
FROM (
with sel as (
SELECT unnest(planilha_linhas) as planilha_linha, sq_taxon, especie, subespecie, co_nivel_taxonomico
  FROM taxonomia.'||p_no_tmp_table||'_taxon
  where ((co_nivel_taxonomico = ''especie'' and subespecie is null)
    or  (co_nivel_taxonomico = ''subespecie'' and subespecie is not null))
    AND (planilha_linhas <@ (coalesce((SELECT array_agg(distinct planilha_linha)
                               FROM taxonomia.'||p_no_tmp_table||'_erro),''{0}'')
                            )
	) IS FALSE
 )
select sel.sq_taxon, '||p_no_tmp_table||'.autor, '||p_no_tmp_table||'.ano
from sel
INNER JOIN taxonomia.'||p_no_tmp_table||' ON '||p_no_tmp_table||'.planilha_linha = sel.planilha_linha
) as tb1
where tb1.sq_taxon = taxon.sq_taxon
RETURNING tb1.sq_taxon,tb1.autor, tb1.ano
)
select
 now(),
 ''Adicionado novo taxon sq_taxon => ''||sq_taxon||'''' as operacao
 from sel_ins
 UNION ALL
select
now(),
''UPD SQ_TAXON_PAI new_sq_taxon_pai=> ''||sel_upd_sq_pai.new_sq_taxon_pai||'' old_sq_taxon_pai=> ''||sel_upd_sq_pai.old_sq_taxon_pai as operacao
from sel_upd_sq_pai
 UNION ALL
select
now(),
''UPD AUTOR E ANO  sq_taxon => ''||sel_upd_autor_ano.sq_taxon||'' autor => ''||sel_upd_autor_ano.autor||'' ano => ''||sel_upd_autor_ano.ano as operacao
from sel_upd_autor_ano
)
;


CREATE UNLOGGED TABLE IF NOT EXISTS taxonomia.'||p_no_tmp_table||'_upd_trilha AS (
with sel as (
WITH RECURSIVE subir_arvore_taxon(nivel, sq_taxon_planilha, ds_caminho, sq_taxon, sq_nivel_taxonomico,   sq_taxon_pai)
                    AS (
                        SELECT 1::integer as nivel, taxon.sq_taxon as sq_taxon_planilha,  taxon.no_taxon as ds_caminho, taxon.sq_taxon, taxon.sq_nivel_taxonomico, taxon.sq_taxon_pai
                        FROM taxonomia.taxon
                        where taxon.sq_taxon in (SELECT distinct sq_taxon
                                                  FROM taxonomia.'||p_no_tmp_table||'_taxon
                                                 WHERE sq_taxon is not null
                                                   AND  (planilha_linhas <@
									                                coalesce((SELECT array_agg(distinct planilha_linha)
                                                                    FROM taxonomia.'||p_no_tmp_table||'_erro),''{0}'')
                                                        ) IS FALSE
                                                   AND (new_sq_taxon_pai <> old_sq_taxon_pai
                                                        OR
                                                        new_taxon is true)

                                                 )

                        UNION
                        SELECT t.nivel+1::integer as nivel, t.sq_taxon_planilha , (t.ds_caminho::text) ||'' < '' ||taxon.no_taxon as ds_caminho,
                        taxon.sq_taxon, taxon.sq_nivel_taxonomico, taxon.sq_taxon_pai
                        FROM taxonomia.taxon
                        JOIN  subir_arvore_taxon t  on taxon.sq_taxon_pai = t.sq_taxon
                    )
                    SELECT t.nivel, sq_taxon_planilha, t.ds_caminho, t.sq_taxon, t.sq_nivel_taxonomico, t.sq_taxon_pai
                      FROM subir_arvore_taxon t

), sel_dist AS (
select  distinct *
  from sel
 ), sel_trilha AS (
select sel_dist.* , taxonomia.criar_trilha(sel_dist.sq_taxon)::json as criar_trilha , taxonomia.formatar_trilha_taxon(t.json_trilha) as ds_trilha_trigger
  from sel_dist
  inner join taxonomia.taxon as t on t.sq_taxon = sel_dist.sq_taxon_planilha
)
select * from sel_trilha
);


UPDATE taxonomia.taxon
   SET json_trilha = '||p_no_tmp_table||'_upd_trilha.criar_trilha
    , ts_ultima_alteracao = now()
  FROM taxonomia.'||p_no_tmp_table||'_upd_trilha
   WHERE '||p_no_tmp_table||'_upd_trilha.sq_taxon = taxon.sq_taxon;

INSERT INTO taxonomia.'||p_no_tmp_table||'_historico(now, operacao)
SELECT now() , concat(ds_trilha_trigger::text,'' Atualizando trilha por hierarquia: '', ds_caminho::text, ''.'')
  FROM taxonomia.'||p_no_tmp_table||'_upd_trilha;

CREATE UNLOGGED TABLE IF NOT EXISTS  taxonomia.'||p_no_tmp_table||'_taxon_erro as (
with sel as (
SELECT planilha_linhas,
       tb.sintax_sq_taxon,
       tb.sq_taxon,
       tb.new_taxon,
       tb.sintax_trilha_de,
       tb.depois_carga_total_taxon_duplicado_nivel,
       tb.antes_carga_total_taxon_duplicado_nivel,
       tb.trilha_planilha, tb.new_sq_taxon_pai, tb.old_sq_taxon_pai, tb.sq_taxon_pai,
       trilha_planilha_pai, co_nivel_taxonomico, no_taxon, tb.reino, tb.filo,
       tb.classe, tb.ordem, tb.familia, tb.genero, tb.especie, tb.subespecie, tb.sintax_recorte,
       tb.sq_nivel_taxonomico, tb.de_nivel_taxonomico, tb.nu_grau_taxonomico,
       tb.in_nivel_intermediario, tb.obs
  FROM taxonomia.'||p_no_tmp_table||'_taxon as tb


 WHERE (planilha_linhas <@ (coalesce((SELECT array_agg(distinct planilha_linha)
                               FROM taxonomia.'||p_no_tmp_table||'_erro),''{0}''))
	) IS true
)select
'||p_no_tmp_table||'_erro.erro_descricao,
'||p_no_tmp_table||'_erro.erro_coluna,
'||p_no_tmp_table||'_erro.erro_valor,
  sel.*
from sel
INNER JOIN taxonomia.'||p_no_tmp_table||'_erro
        ON '||p_no_tmp_table||'_erro.planilha_linha  = any( sel.planilha_linhas)
order by nu_grau_taxonomico, trilha_planilha_pai, trilha_planilha

           );



CREATE UNLOGGED TABLE IF NOT EXISTS  taxonomia.'||p_no_tmp_table||'_taxon_sucesso as (
with sel as (
SELECT planilha_linhas as planilha_linha,
       tb.sintax_sq_taxon,
       tb.sq_taxon,
       tb.new_taxon,
       tb.sintax_trilha_de,
       tb.depois_carga_total_taxon_duplicado_nivel,
       tb.antes_carga_total_taxon_duplicado_nivel,
       tb.trilha_planilha, tb.new_sq_taxon_pai, tb.old_sq_taxon_pai, tb.sq_taxon_pai,
       trilha_planilha_pai, co_nivel_taxonomico, no_taxon, tb.reino, tb.filo,
       tb.classe, tb.ordem, tb.familia, tb.genero, tb.especie, tb.subespecie, tb.sintax_recorte,
       tb.sq_nivel_taxonomico, tb.de_nivel_taxonomico, tb.nu_grau_taxonomico,
       tb.in_nivel_intermediario, tb.obs
  FROM taxonomia.'||p_no_tmp_table||'_taxon as tb


 WHERE (planilha_linhas <@ (coalesce((SELECT array_agg(distinct planilha_linha)
                               FROM taxonomia.'||p_no_tmp_table||'_erro),''{0}''))
	) IS FALSE
   AND (tb.new_taxon is true
    OR
    (tb.old_sq_taxon_pai <> tb.sq_taxon_pai and tb.old_sq_taxon_pai is not null))
)select
  sel.*
from sel
order by nu_grau_taxonomico, trilha_planilha_pai, trilha_planilha
);


INSERT INTO taxonomia.taxon_hist(
            sq_taxon, sq_pessoa, sq_situacao, sq_taxon_pai,
            sq_nivel_taxonomico, no_taxon, st_ativo, no_autor, nu_ano, in_ocorre_brasil,
            de_historico, dt_historico)
select taxon.sq_taxon, 2533 , 1,taxon.sq_taxon_pai,  taxon.sq_nivel_taxonomico , taxon.no_taxon , true , taxon.no_autor ,  taxon.nu_ano,  taxon.in_ocorre_brasil , ''Carga através de planilha ('||p_no_tmp_table||').'', now()
                        FROM taxonomia.taxon
                        where taxon.sq_taxon in (SELECT distinct sq_taxon
                                                  FROM taxonomia.'||p_no_tmp_table||'_taxon
                                                 WHERE new_taxon is true
                                                 );




INSERT INTO taxonomia.taxon_hist(
            sq_taxon, sq_pessoa, sq_situacao, sq_taxon_pai,
            sq_nivel_taxonomico, no_taxon, st_ativo, no_autor, nu_ano, in_ocorre_brasil,
            de_historico, dt_historico)
    SELECT
        taxon.sq_taxon, 2533 , 7,taxon.sq_taxon_pai,  taxon.sq_nivel_taxonomico , taxon.no_taxon , true , taxon.no_autor ,  taxon.nu_ano,  taxon.in_ocorre_brasil
         ,concat(''Reclassificação realizado por planilha ('||p_no_tmp_table||'). Mudança de nivel de "'', sintax_trilha_de , ''" para "'',  taxonomia.formatar_trilha_taxon(taxon.json_trilha),''" .''), now()
--new_sq_taxon_pai, old_sq_taxon_pai, sintax_trilha_de , sq_taxon
    FROM taxonomia.'||p_no_tmp_table||'_taxon
             INNER JOIN taxonomia.taxon ON taxon.sq_taxon = '||p_no_tmp_table||'_taxon.sq_taxon AND taxon.sq_taxon_pai = '||p_no_tmp_table||'_taxon.new_sq_taxon_pai
    WHERE new_taxon is false
      AND new_sq_taxon_pai <> old_sq_taxon_pai;


INSERT INTO taxonomia.taxon_hist(
            sq_taxon, sq_pessoa, sq_situacao, sq_taxon_pai,
            sq_nivel_taxonomico, no_taxon, st_ativo, no_autor, nu_ano, in_ocorre_brasil,
            de_historico, dt_historico)
SELECT  u.sq_taxon, 2533 , 7,null,  null , null , null , null ,  null,  null
         ,concat(''Reclassificação atraves de planilha ('||p_no_tmp_table||'). Mudança de nível de ( '', sintax_trilha_de , '') para ( '',  taxonomia.formatar_trilha_taxon(taxon.json_trilha),'' ) .''), now()
--new_sq_taxon_pai, old_sq_taxon_pai, sintax_trilha_de , u.sq_taxon
    FROM taxonomia.'||p_no_tmp_table||'_taxon
    INNER JOIN taxonomia.'||p_no_tmp_table||'_upd_trilha as u on u.sq_taxon_planilha = '||p_no_tmp_table||'_taxon.sq_taxon
    INNER JOIN taxonomia.taxon ON taxon.sq_taxon = '||p_no_tmp_table||'_taxon.sq_taxon AND taxon.sq_taxon_pai = '||p_no_tmp_table||'_taxon.new_sq_taxon_pai
    WHERE new_taxon is false
      AND new_sq_taxon_pai <> old_sq_taxon_pai;

';

     RAISE INFO 'v_sql  : % [ % ] %', E'\n',  v_sql, E'\n';
     execute v_sql;

EXCEPTION WHEN OTHERS THEN
    BEGIN
        RAISE EXCEPTION 'Erro: sql %', SQLERRM;
    END;

END;
$$;


--
-- TOC entry 4651 (class 1255 OID 23817)
-- Name: fn_planilha_sintax_carga(text); Type: FUNCTION; Schema: taxonomia; Owner: -
--

CREATE FUNCTION taxonomia.fn_planilha_sintax_carga(p_no_tmp_table text) RETURNS json
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_existe integer;
    v_txt_complemento text;
    v_msg text;
    v_row_json json;
BEGIN
    DROP TABLE IF EXISTS taxonomia.tmp_carga_msg_validacao;

    CREATE UNLOGGED TABLE IF NOT EXISTS taxonomia.tmp_carga_msg_validacao
    (
        msg text,
        data timestamp with time zone NOT NULL DEFAULT now()
    );

    DROP TABLE IF EXISTS  taxonomia.tmp_sintax_import_taxon_processo;

    CREATE UNLOGGED TABLE taxonomia.tmp_sintax_import_taxon_processo
    (
        msg text,
        sql text,
        sql_rollback text,
        tabela_json json[],
        sq_taxon integer
    );


    UPDATE taxonomia.tmp_sintax_import_taxon set ordem=INITCAP(trim(ordem)) , familia=INITCAP(trim(familia)) ,subfamilia=INITCAP(trim(subfamilia)) , genero=INITCAP(trim(genero)) , especie=lower(trim(especie)) ,autor=trim(autor) ,ano=trim(ano);


    INSERT INTO lafsisbio.tmp_sintax_import_taxon_processo(msg, sql, sql_rollback, tabela_json,sq_taxon)
    with sel AS (
        SELECT ordem, familia, subfamilia,
               lafsisbio.fn_get_sq_taxon_search_trilha(null,null,genero,null,null,null,null,null) sintax_sq_genero,
               genero,
               contagem_genero.genero_total,
               lafsisbio.fn_get_sq_taxon_search_trilha(null,especie,genero,familia,null,null,null,null) as sintax_especie,
               especie,
               especie_array_to_json,
               json_array_length(especie_array_to_json) as especie_total ,
               autor, ano
        FROM taxonomia.tmp_sintax_import_taxon
                 LEFT JOIN LATERAL (SELECT array_to_json(array_agg(json_trilha))   as especie_array_to_json  FROM  taxonomia.taxon WHERE json_trilha->'especie'->>'no_taxon' = genero||' '||especie  and json_trilha->'ordem'->>'no_taxon' = 'Chiroptera') contagem_especie ON TRUE
                 LEFT JOIN LATERAL (SELECT count(*) as genero_total
                                    FROM  taxonomia.taxon
                                    WHERE json_trilha->'genero'->>'no_taxon' = genero AND  json_trilha->'ordem'->>'no_taxon' = ordem
/**
    AND taxon.sq_nivel_taxonomico = (
				    SELECT sq_nivel_taxonomico
					  FROM taxonomia.nivel_taxonomico
					  where co_nivel_taxonomico = UPPER('ordem')
	)
	*/
            ) contagem_genero ON TRUE
    ), sel_cmd AS
             (
                 select 'Update autor e ano' as msg,
                        FORMAT('UPDATE taxonomia.taxon SET no_autor=trim(''%s''::text), nu_ano=%L WHERE sq_taxon = %s ;',autor, ano,taxon.sq_taxon) as sql,
                        FORMAT('UPDATE taxonomia.taxon SET no_autor=''%s''::text, nu_ano=%L WHERE sq_taxon = %s ;',taxon.no_autor, coalesce(taxon.nu_ano,null),taxon.sq_taxon) as sql_rollback,
                        (select array_agg( row_to_json(tb1) )
                         from (
                                  SELECT * FROM taxonomia.taxon WHERE taxon.sq_taxon = sel.sintax_especie

                              ) tb1) as tabela_json,
                        taxon.sq_taxon
                        --,sel.*,  no_taxon, json_trilha->'genero'->>'no_taxon' as genero_sintax , json_trilha
                 FROM  sel
                           LEFT JOIN taxonomia.taxon ON taxon.sq_taxon = sel.sintax_especie

--WHERE sel.especie_total = 1
                 ORDER BY ordem, familia, subfamilia,genero,especie
             )
    select sel_cmd.msg, sel_cmd.sql, sel_cmd.sql_rollback, sel_cmd.tabela_json,sel_cmd.sq_taxon
    from sel_cmd
             LEFT JOIN  taxonomia.tmp_sintax_import_taxon_processo as verif_exist
                        ON  verif_exist.msg=sel_cmd.msg
                            AND verif_exist.sql=sel_cmd.sql
                            AND verif_exist.sql_rollback=sel_cmd.sql_rollback
                            AND verif_exist.sq_taxon=sel_cmd.sq_taxon
    WHERE verif_exist.sq_taxon is null;


    return json_build_object(
            'status',  0,
            'msg', ''
        );

EXCEPTION WHEN OTHERS THEN
    BEGIN
        RAISE EXCEPTION 'Erro: %', SQLERRM;
    END;


END;
$$;


--
-- TOC entry 13239 (class 0 OID 0)
-- Dependencies: 4651
-- Name: FUNCTION fn_planilha_sintax_carga(p_no_tmp_table text); Type: COMMENT; Schema: taxonomia; Owner: -
--

COMMENT ON FUNCTION taxonomia.fn_planilha_sintax_carga(p_no_tmp_table text) IS 'Função para validar a tabela de importacao de taxon no sintax. ';


--
-- TOC entry 4652 (class 1255 OID 23818)
-- Name: fn_planilha_sintax_carga_subfamilia_tribo(text); Type: FUNCTION; Schema: taxonomia; Owner: -
--

CREATE FUNCTION taxonomia.fn_planilha_sintax_carga_subfamilia_tribo(p_no_tmp_table text) RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_sql text;
BEGIN

  if p_no_tmp_table is null then
   RAISE EXCEPTION 'Informe o nome da tabela temporária';
  end if;

    v_sql:='create table if not exists taxonomia.'||p_no_tmp_table||'_sel  AS  (
with sel AS (
SELECT reino, filo, classe, ordem, familia, subfamilia, tribo, genero,
       especie, autor, ano, operacao_sintax, sintax_recorte, planilha_linha
        ,taxonomia.fn_get_sq_taxon_search_trilha(null,
                                                                 lower(trim(especie)),
                                                                 INITCAP(trim(genero)),
                                                                 null, -- familia
                                                                 null, -- ordem
								 null, -- classe
                                                                 null, -- filo
                                                                 null -- reino
                    ) as get_sq_taxon
  FROM taxonomia.'||p_no_tmp_table||'
  ), new_subfamilia AS (
  select subfamilia, nextval(''taxonomia.taxon_sq_taxon_seq'')::integer as new_subfamilia_sq_taxon
  from sel
  where subfamilia is not null
  AND sel.get_sq_taxon->>''total_sq_taxon_nivel'' = 1::text
  group by subfamilia
  ), new_subtribo AS (
  select subfamilia, tribo, nextval(''taxonomia.taxon_sq_taxon_seq'')::integer as new_tribo_sq_taxon
  from sel
  where tribo is not null
    AND sel.get_sq_taxon->>''total_sq_taxon_nivel'' = 1::text
  group by subfamilia, tribo
  )
  select
   (sel.get_sq_taxon->>''total_sq_taxon_nivel'')::integer as total_sq_taxon_nivel
  ,(get_sq_taxon->''data''->0->''json_trilha''->''familia''->>''sq_taxon'')::integer as familia_sq_taxon
  , sel.familia
  , exists_subfamilia.sq_taxon_pai as subfamilia_sq_taxon_pai
  , exists_subfamilia.sq_taxon as subfamilia_sq_taxon
  , new_subfamilia.new_subfamilia_sq_taxon
  , sel.subfamilia
  , exists_tribo.sq_taxon_pai as tribo_sq_taxon_pai
  , exists_tribo.sq_taxon as tribo_sq_taxon
    , new_subtribo.new_tribo_sq_taxon
  , sel.tribo
  , (get_sq_taxon->''data''->0->>''sq_taxon_pai'')::integer as especie_sq_taxon_pai
  , sel.genero
  , (get_sq_taxon->''data''->0->>''sq_taxon'')::integer as especie_sq_taxon
  , sel.especie
    from sel
    INNER JOIN new_subfamilia ON new_subfamilia.subfamilia = sel.subfamilia
    LEFT JOIN new_subtribo ON new_subtribo.tribo = sel.tribo
    --LEFT JOIN taxonomia.taxon as exists_subfamilia ON exists_subfamilia.sq_nivel_taxonomico = 9 AND json_trilha->''subfamilia''->>''no_taxon'' = sel.familia
      LEFT JOIN LATERAL ( SELECT array_agg(sq_taxon) as  sq_taxon , array_agg(sq_taxon_pai) as  sq_taxon_pai
			FROM taxonomia.taxon
			WHERE taxon.sq_nivel_taxonomico = 9
			AND taxon.json_trilha->''subfamilia''->>''no_taxon'' = sel.familia
			GROUP BY no_taxon
		) as exists_subfamilia ON TRUE
      LEFT JOIN LATERAL ( SELECT array_agg(sq_taxon) as  sq_taxon , array_agg(sq_taxon_pai) as  sq_taxon_pai
		FROM taxonomia.taxon
		WHERE taxon.sq_nivel_taxonomico = 10
		AND taxon.json_trilha->''tribo''->>''no_taxon'' = sel.tribo
		GROUP BY no_taxon
	) as exists_tribo ON TRUE
    where sel.get_sq_taxon->>''total_sq_taxon_nivel'' = 1::text
  );

create table if not exists taxonomia.'||p_no_tmp_table||'_ins_hist  AS  (
with sel as (
  select * from taxonomia.'||p_no_tmp_table||'_sel
)
,sel_ins_taxon as (
	select
	concat(''Inserção do táxon '',tb1.no_taxon,'' no nível ''||nivel_taxonomico.de_nivel_taxonomico||''. A pedido do Arthur do CBC. '') as msg,
	tb1.sq_taxon,
	tb1.sq_taxon_pai ,
	tb1.sq_nivel_taxonomico ,
	tb1.sq_situacao_nome,
	tb1.no_taxon,
	tb1.st_ativo,
	tb1.no_autor,
	tb1.nu_ano,
	tb1.in_ocorre_brasil,
	tb1.de_autor_ano,
	tb1.ts_ultima_alteracao
	from (  select distinct
		  new_subfamilia_sq_taxon as sq_taxon, --sq_taxon
		  familia_sq_taxon as sq_taxon_pai , --sq_taxon_pai
		  (SELECT sq_nivel_taxonomico
			FROM taxonomia.nivel_taxonomico
			WHERE co_nivel_taxonomico = UPPER(''SUBFAMILIA'')
			) as sq_nivel_taxonomico , --sq_nivel_taxonomico
		  1 as sq_situacao_nome, --sq_situacao_nome
		  subfamilia as no_taxon, -- no_taxon
		  true as st_ativo, --st_ativo
		  null as no_autor, --no_autor
		  null as nu_ano, --nu_ano
		  null as in_ocorre_brasil, --in_ocorre_brasil
		  null as de_autor_ano, --de_autor_ano
		 --json_trilha
		  now() as ts_ultima_alteracao --ts_ultima_alteracao
		  from taxonomia.'||p_no_tmp_table||'_sel
		  WHERE subfamilia is not null
		UNION ALL
		select distinct
		  new_tribo_sq_taxon as sq_taxon, --sq_taxon
		  new_subfamilia_sq_taxon as sq_taxon_pai , --sq_taxon_pai
		  (SELECT sq_nivel_taxonomico
		  FROM taxonomia.nivel_taxonomico
		  WHERE co_nivel_taxonomico = UPPER(''TRIBO'')
		  ) as sq_nivel_taxonomico , --sq_nivel_taxonomico
		  1 as sq_situacao_nome, --sq_situacao_nome
		  tribo as no_taxon, -- no_taxon
		true as st_ativo, --st_ativo
		null as no_autor, --no_autor
		null as nu_ano, --nu_ano
		null as in_ocorre_brasil, --in_ocorre_brasil
		null as de_autor_ano, --de_autor_ano
		 --json_trilha
		now() as ts_ultima_alteracao --ts_ultima_alteracao
		  from taxonomia.'||p_no_tmp_table||'_sel
		  WHERE tribo is not null
		order by sq_nivel_taxonomico, sq_taxon_pai, no_taxon
	) as tb1
	INNER JOIN taxonomia.nivel_taxonomico ON  nivel_taxonomico.sq_nivel_taxonomico = tb1.sq_nivel_taxonomico
),
ins_taxon as (
	INSERT INTO taxonomia.taxon(
		sq_taxon,
		sq_taxon_pai,
		sq_nivel_taxonomico,
		sq_situacao_nome,
		no_taxon,
		st_ativo,
		no_autor,
		nu_ano,
		in_ocorre_brasil,
		de_autor_ano,
		ts_ultima_alteracao
		)
		select
			sq_taxon,
			sq_taxon_pai ,
			sq_nivel_taxonomico ,
			sq_situacao_nome,
			no_taxon,
			st_ativo,
			no_autor,
			nu_ano::integer,
			true::boolean,
			de_autor_ano,
			ts_ultima_alteracao
			from sel_ins_taxon
		WHERE NOT EXISTS (
		   select 1 from  taxonomia.taxon where taxon.sq_taxon = sel_ins_taxon.sq_taxon
		)
	returning ''ins_taxon'' as operacao , sq_taxon, no_taxon

), upd_sq_taxon_pai AS (
	UPDATE taxonomia.taxon
	   SET sq_taxon_pai=tb1.sq_taxon_pai
	  FROM (
             select distinct
		taxonomia.formatar_trilha_taxon(taxon.json_trilha) as trilha_genero,
		concat(	case
		  when tribo is not null AND new_tribo_sq_taxon is not null then ''Adicionado o nível intermediário "tribo", imediatamente superior ao nível de(o) ''
		  when subfamilia is not null AND new_subfamilia_sq_taxon is not null  then ''Adicionado o nível intermediário "subfamilia", imediatamente superior ao nível de(o) ''
		end,'' '', nivel_taxonomico.de_nivel_taxonomico,'' do táxon '',genero,''. Demanda do Arthur CBC.'') as msg,

		--concat(''Atualizado nível(is) superiores na árvore taxônomica do táxon '',genero,'' do nível '', nivel_taxonomico.de_nivel_taxonomico,'' . Demanda do Arthur CBC.'') as msg,
		concat(''UPDATE taxonomia.taxon SET sq_taxon_pai='',coalesce(sel.new_tribo_sq_taxon,sel.new_subfamilia_sq_taxon),'' WHERE sq_taxon = '',especie_sq_taxon_pai) as sql,
		sel.genero
		,especie_sq_taxon_pai as sq_taxon
		,coalesce(sel.new_tribo_sq_taxon,sel.new_subfamilia_sq_taxon) as sq_taxon_pai
		,sel.new_tribo_sq_taxon,sel.new_subfamilia_sq_taxon
		FROM sel
		INNER JOIN taxonomia.taxon ON taxon.sq_taxon = sel.especie_sq_taxon_pai
		INNER JOIN taxonomia.nivel_taxonomico ON nivel_taxonomico.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
		WHERE taxon.sq_taxon_pai <> coalesce(sel.new_tribo_sq_taxon,sel.new_subfamilia_sq_taxon)
		order by sq_taxon_pai
	) as tb1
	 WHERE taxon.sq_taxon = tb1.sq_taxon
   Returning tb1.sql, taxon.sq_taxon, tb1.msg,tb1.trilha_genero,tb1.genero
), sel_ins_hist as (
select
  tb2.sq_taxon,
  2533 as sq_pessoa ,
  case
    when tb2.operacao = ''ins_taxon'' then 1
    when tb2.operacao = ''upd_sq_taxon_pai'' then 2
  end  as sq_situacao,
  null as sq_taxon_pai,
  null as sq_nivel_taxonomico,
  tb2.no_taxon ,
  true as st_ativo,
  null as no_autor,
  null as nu_ano,
  null as in_ocorre_brasil,
   concat(''Carga através de planilha. '',msg) as de_historico, now() as dt_historico
from (
select ins_taxon.operacao
,sel_ins_taxon.sq_taxon
,sel_ins_taxon.msg
,ins_taxon.no_taxon
  from ins_taxon
 inner join sel_ins_taxon on sel_ins_taxon.sq_taxon=ins_taxon.sq_taxon
 UNION ALL
select ''upd_sq_taxon_pai'' as operacao
,upd_sq_taxon_pai.sq_taxon
,concat(upd_sq_taxon_pai.msg,'' Trilha antes da atualização: '',trilha_genero) as msg
,upd_sq_taxon_pai.genero as no_taxon
FROM upd_sq_taxon_pai
) as tb2
), ins_hist as (
INSERT INTO taxonomia.taxon_hist(
             sq_taxon, sq_pessoa, sq_situacao, sq_taxon_pai,
             sq_nivel_taxonomico, no_taxon, st_ativo, no_autor, nu_ano, in_ocorre_brasil,
             de_historico, dt_historico
  )
select
	sq_taxon,
	sq_pessoa ,
	sq_situacao,
	sq_taxon_pai::integer,
	sq_nivel_taxonomico::integer,
	no_taxon ,
	st_ativo,
	no_autor,
	nu_ano::integer,
	in_ocorre_brasil::boolean,
	de_historico,
	dt_historico
from sel_ins_hist
returning sq_taxon, sq_taxon_hist
)
select sq_taxon, sq_taxon_hist from ins_hist
)
;


UPDATE taxonomia.taxon
   SET json_trilha = taxonomia.criar_trilha(tb1.sq_taxon)
  FROM (
  select distinct sq_taxon from (
	select sel.new_tribo_sq_taxon as sq_taxon
	FROM taxonomia.'||p_no_tmp_table||'_sel as sel
	UNION ALL
	select sel.new_subfamilia_sq_taxon as sq_taxon
	FROM taxonomia.'||p_no_tmp_table||'_sel as sel
	UNION ALL
	select distinct especie_sq_taxon_pai as sq_taxon from taxonomia.'||p_no_tmp_table||'_sel
	) as t
	where sq_taxon is not null
) as tb1
  WHERE taxon.sq_taxon = tb1.sq_taxon;



create table if not exists taxonomia.'||p_no_tmp_table||'_ins_hist_resultado  AS  (
select taxon.no_taxon as taxon, taxonomia.formatar_trilha_taxon(taxon.json_trilha),
taxon_hist.*
 from taxonomia.'||p_no_tmp_table||'_ins_hist
 INNER JOIN taxonomia.taxon ON taxon.sq_taxon = '||p_no_tmp_table||'_ins_hist.sq_taxon
 INNER JOIN taxonomia.taxon_hist ON taxon_hist.sq_taxon = taxon.sq_taxon
) ;


/**




Prova real da atualizacao

                         FROM taxonomia.taxon
                         where taxon.sq_taxon in (SELECT distinct sq_taxon
                                                   FROM taxonomia.'' || p_no_tmp_table || ''_taxon
                                                  WHERE new_taxon is true
                                                  );

select pai.sq_nivel_taxonomico, pai.no_taxon , taxon.sq_nivel_taxonomico, taxon.no_taxon, '||p_no_tmp_table||'_sel.*
  from taxonomia.'||p_no_tmp_table||'_sel
 INNER JOIN taxonomia.taxon  ON taxon.sq_taxon='||p_no_tmp_table||'_sel.especie_sq_taxon_pai
 INNER JOIN taxonomia.taxon as  pai ON pai.sq_taxon = taxon.sq_taxon_pai
 WHERE taxon.sq_taxon_pai = coalesce('||p_no_tmp_table||'_sel.new_tribo_sq_taxon,'||p_no_tmp_table||'_sel.new_subfamilia_sq_taxon)
*/
/**

-- INSERT INTO taxonomia.taxon_hist(
--             sq_taxon, sq_pessoa, sq_situacao, sq_taxon_pai,
--             sq_nivel_taxonomico, no_taxon, st_ativo, no_autor, nu_ano, in_ocorre_brasil,
--             de_historico, dt_historico)
-- select taxon.sq_taxon, 2533 , 1,taxon.sq_taxon_pai,  taxon.sq_nivel_taxonomico , taxon.no_taxon , true , taxon.no_autor ,  taxon.nu_ano,  taxon.in_ocorre_brasil , ''''Carga através de planilha ('' ||
           p_no_tmp_table || '').'''', now()
--                         FROM taxonomia.taxon
--                         where taxon.sq_taxon in (SELECT distinct sq_taxon
--                                                   FROM taxonomia.'' || p_no_tmp_table || ''_taxon
--                                                  WHERE new_taxon is true
--                                                  );

**/


--)
/**
COMANDO ROLLBACK


UPDATE taxonomia.taxon
   SET sq_taxon_pai=sel.familia_sq_taxon
  FROM taxonomia.'||p_no_tmp_table||'_sel as sel
  WHERE taxon.sq_taxon = sel.especie_sq_taxon_pai;


DELETE FROM taxonomia.taxon_hist  WHERE sq_taxon in (
	select sel.new_tribo_sq_taxon
	--,sel.new_subfamilia_sq_taxon
	FROM taxonomia.'||p_no_tmp_table||'_sel as sel
	UNION ALL
	select sel.new_subfamilia_sq_taxon
	--,sel.new_subfamilia_sq_taxon
	FROM taxonomia.'||p_no_tmp_table||'_sel as sel
);


DELETE FROM taxonomia.taxon  WHERE sq_taxon in (
	select sel.new_tribo_sq_taxon
	--,sel.new_subfamilia_sq_taxon
	FROM taxonomia.'||p_no_tmp_table||'_sel as sel
	UNION ALL
	select sel.new_subfamilia_sq_taxon
	--,sel.new_subfamilia_sq_taxon
	FROM taxonomia.'||p_no_tmp_table||'_sel as sel
);

drop table if exists taxonomia.'||p_no_tmp_table||'_sel;
drop table if exists taxonomia.'||p_no_tmp_table||'_ins_hist;
**/
';

  --RAISE INFO 'v_sql  : % [ % ] %', E'\n',  v_sql, E'\n';

  RAISE INFO '% Criando tabela temporario da arvore taxonomica. %', E'\n', E'\n';
    execute v_sql;


EXCEPTION WHEN OTHERS THEN
    BEGIN

        RAISE EXCEPTION 'taxonomia.Erro fn_planilha_sintax_carga_subfamilia_tribo : [ % ] %', v_sql, SQLERRM;

    END;

END;
$$;


--
-- TOC entry 13240 (class 0 OID 0)
-- Dependencies: 4652
-- Name: FUNCTION fn_planilha_sintax_carga_subfamilia_tribo(p_no_tmp_table text); Type: COMMENT; Schema: taxonomia; Owner: -
--

COMMENT ON FUNCTION taxonomia.fn_planilha_sintax_carga_subfamilia_tribo(p_no_tmp_table text) IS 'Função para importar planilha taxonomica atualizando subfamilia e tribo';


--
-- TOC entry 4653 (class 1255 OID 23821)
-- Name: fn_planilha_sintax_export_arvore_partir_de(text, text, text); Type: FUNCTION; Schema: taxonomia; Owner: -
--

CREATE FUNCTION taxonomia.fn_planilha_sintax_export_arvore_partir_de(p_no_tmp_table text, p_no_taxon text, p_co_nivel_taxonomico text) RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_sql text;
BEGIN

  if p_no_tmp_table is null then
   RAISE EXCEPTION 'Informe o nome da tabela temporária';
  end if;

  if p_no_taxon is null then
    RAISE EXCEPTION 'Informe o nome  do taxon';
  end if;

  if p_co_nivel_taxonomico is null then
     RAISE EXCEPTION 'Informe o codigo do nivel';
  end if;

    v_sql:='DROP TABLE IF EXISTS  taxonomia.'||p_no_tmp_table||';
CREATE UNLOGGED TABLE IF NOT EXISTS taxonomia.'||p_no_tmp_table||' AS (
SELECT
taxon.sq_nivel_taxonomico
,de_nivel_taxonomico
,co_nivel_taxonomico
,coalesce(
        json_trilha->''subespecie''->>''sq_taxon_pai''
        ,json_trilha->''especie''->>''sq_taxon_pai''
        ,json_trilha->''genero''->>''sq_taxon_pai''
        ,json_trilha->''tribo''->>''sq_taxon_pai''
        ,json_trilha->''subfamilia''->>''sq_taxon_pai''
        ,json_trilha->''familia''->>''sq_taxon_pai''
        ,json_trilha->''ordem''->>''sq_taxon_pai''
        ,json_trilha->''classe''->>''sq_taxon_pai''
        ,json_trilha->''subfilo''->>''sq_taxon_pai''
        ,json_trilha->''filo''->>''sq_taxon_pai''
        ,json_trilha->''reino''->>''sq_taxon_pai'') as sq_taxon_pai
,coalesce(
        json_trilha->''subespecie''->>''sq_taxon''
        ,json_trilha->''especie''->>''sq_taxon''
        ,json_trilha->''genero''->>''sq_taxon''
        ,json_trilha->''tribo''->>''sq_taxon''
        ,json_trilha->''subfamilia''->>''sq_taxon''
        ,json_trilha->''familia''->>''sq_taxon''
        ,json_trilha->''ordem''->>''sq_taxon''
        ,json_trilha->''classe''->>''sq_taxon''
        ,json_trilha->''subfilo''->>''sq_taxon''
        ,json_trilha->''filo''->>''sq_taxon''
        ,json_trilha->''reino''->>''sq_taxon'') as sq_taxon
,coalesce(
        json_trilha->''subespecie''->>''no_taxon''
        ,json_trilha->''especie''->>''no_taxon''
        ,json_trilha->''genero''->>''no_taxon''
        ,json_trilha->''tribo''->>''no_taxon''
        ,json_trilha->''subfamilia''->>''no_taxon''
        ,json_trilha->''familia''->>''no_taxon''
        ,json_trilha->''ordem''->>''no_taxon''
        ,json_trilha->''classe''->>''no_taxon''
        ,json_trilha->''subfilo''->>''no_taxon''
        ,json_trilha->''filo''->>''no_taxon''
        ,json_trilha->''reino''->>''no_taxon'') as no_taxon    
,taxonomia.formatar_trilha_taxon(
    json_trilha,
    true,
    false
)::text as trilha_taxon
, json_trilha->''reino''->>''sq_taxon'' AS sq_taxon_reino
, json_trilha->''reino''->>''no_taxon'' AS reino
, json_trilha->''filo''->>''sq_taxon'' AS sq_taxon_filo
, json_trilha->''filo''->>''no_taxon'' AS filo
, json_trilha->''subfilo''->>''sq_taxon'' AS sq_taxon_subfilo
, json_trilha->''subfilo''->>''no_taxon'' AS subfilo
, json_trilha->''classe''->>''sq_taxon'' AS sq_taxon_classe
, json_trilha->''classe''->>''no_taxon'' AS classe
, json_trilha->''ordem''->>''sq_taxon'' AS sq_taxon_ordem
, json_trilha->''ordem''->>''no_taxon'' AS ordem
, json_trilha->''familia''->>''sq_taxon'' AS sq_taxon_familia
, json_trilha->''familia''->>''no_taxon'' AS familia
, json_trilha->''subfamilia''->>''sq_taxon'' AS sq_taxon_subfamilia
, json_trilha->''subfamilia''->>''no_taxon'' AS subfamilia
, json_trilha->''tribo''->>''sq_taxon'' AS sq_taxon_tribo
, json_trilha->''tribo''->>''no_taxon'' AS tribo
, json_trilha->''genero''->>''sq_taxon'' AS sq_taxon_genero
, json_trilha->''genero''->>''no_taxon'' AS genero
, json_trilha->''especie''->>''sq_taxon'' AS sq_taxon_especie
, json_trilha->''especie''->>''no_taxon'' AS especie
, json_trilha->''subespecie''->>''sq_taxon'' AS sq_taxon_subespecie
, json_trilha->''subespecie''->>''no_taxon'' AS subespecie
, no_autor
, nu_ano
--,json_trilha
, null as OPERACAO_SINTAX
, '''||lower(p_co_nivel_taxonomico)||''' as SINTAX_RECORTE
  FROM taxonomia.taxon
   INNER JOIN taxonomia.nivel_taxonomico
          ON nivel_taxonomico.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
  WHERE json_trilha->'''||lower(p_co_nivel_taxonomico)||'''->>''no_taxon'' = '''||p_no_taxon||'''
  ORDER BY nu_grau_taxonomico, coalesce(
        json_trilha->''subespecie''->>''no_taxon''
        ,json_trilha->''especie''->>''no_taxon''
        ,json_trilha->''genero''->>''no_taxon''
        ,json_trilha->''tribo''->>''no_taxon''
        ,json_trilha->''subfamilia''->>''no_taxon''
        ,json_trilha->''familia''->>''no_taxon''
        ,json_trilha->''ordem''->>''no_taxon''
        ,json_trilha->''classe''->>''no_taxon''
        ,json_trilha->''subfilo''->>''no_taxon''
        ,json_trilha->''filo''->>''no_taxon''
        ,json_trilha->''reino''->>''no_taxon'')
  );';

  --RAISE INFO 'v_sql  : % [ % ] %', E'\n',  v_sql, E'\n';

  RAISE INFO '% Criando tabela temporario da arvore taxonomica. %', E'\n', E'\n';
    execute v_sql;

  RAISE INFO '% Tabela temporario da arvore taxonomica criada com sucesso. %', E'\n', E'\n';

	v_sql:='DROP TABLE IF EXISTS  taxonomia.'||p_no_tmp_table||'_duplicado;
		CREATE UNLOGGED TABLE IF NOT EXISTS taxonomia.'||p_no_tmp_table||'_duplicado AS (
		with sel as (
		select count(*) OVER (PARTITION BY co_nivel_taxonomico, no_taxon) as total , * 
		  from taxonomia.'||p_no_tmp_table||' 
		)
		select total, sq_nivel_taxonomico, de_nivel_taxonomico, co_nivel_taxonomico, 
		       sq_taxon_pai, sq_taxon,no_taxon, sq_taxon_reino, reino, sq_taxon_filo, 
		       filo, sq_taxon_subfilo, subfilo, sq_taxon_classe, classe, sq_taxon_ordem, 
		       ordem, sq_taxon_familia, familia, sq_taxon_subfamilia, subfamilia, 
		       sq_taxon_tribo, tribo, sq_taxon_genero, genero, sq_taxon_especie, 
		       especie, sq_taxon_subespecie, subespecie, no_autor, nu_ano, trilha_taxon, 
		       operacao_sintax, sintax_recorte
		from sel 
		where  total > 1
		ORDER BY trilha_taxon
		);';
		
  RAISE INFO '% Criando tabela tmp de duplicidade. %', E'\n', E'\n';
    execute v_sql;

  RAISE INFO '% Tabela tmp de duplidade criada com sucesso. %', E'\n', E'\n';
  
EXCEPTION WHEN OTHERS THEN
    BEGIN

        RAISE EXCEPTION 'taxonomia.Erro fn_planilha_sintax_export_arvore_partir_de : [ % ] %', v_sql, SQLERRM;

    END;

END;
$$;


--
-- TOC entry 13241 (class 0 OID 0)
-- Dependencies: 4653
-- Name: FUNCTION fn_planilha_sintax_export_arvore_partir_de(p_no_tmp_table text, p_no_taxon text, p_co_nivel_taxonomico text); Type: COMMENT; Schema: taxonomia; Owner: -
--

COMMENT ON FUNCTION taxonomia.fn_planilha_sintax_export_arvore_partir_de(p_no_tmp_table text, p_no_taxon text, p_co_nivel_taxonomico text) IS 'Função para exporta planilha taxonomica a partir de um taxon para realizar analise';


--
-- TOC entry 4654 (class 1255 OID 23822)
-- Name: fn_planilha_sintax_get_sq_taxon(text); Type: FUNCTION; Schema: taxonomia; Owner: -
--

CREATE FUNCTION taxonomia.fn_planilha_sintax_get_sq_taxon(p_no_tmp_table text) RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_sql text;
    v_record record;
    v_no_tmp_sel_sq_taxon text;
BEGIN
    v_no_tmp_sel_sq_taxon:=p_no_tmp_table||'_sq_taxon';

v_sql:='DROP TABLE IF EXISTS taxonomia.'||v_no_tmp_sel_sq_taxon||';
        CREATE UNLOGGED TABLE IF NOT EXISTS taxonomia.'||v_no_tmp_sel_sq_taxon||' AS (
             with sel AS (
                    select row_number() OVER () AS planilha_linha , m.* FROM
                        json_array_elements(
                                (select array_to_json(array_agg( row_to_json(tb1) ) )   from  (
                                                                                              select *
                                                                                              from taxonomia.'||p_no_tmp_table||'
                                                                                              ) tb1
                                )::json
                            ) AS m
                ), sel_sq_taxon AS (
                select sel.value as linha_json,
                        sel.planilha_linha,
                        t.key,t.value, --array_agg(planilha_linha),
                        sel.value->>''especie'' as especie,
                        sel.value->>''genero'' as genero,
                        sel.value->>''familia'' as familia,
                        sel.value->>''ordem'' as ordem,
                        sel.value->>''classe'' as classe,
                        sel.value->>''filo'' as filo,
                        sel.value->>''reino'' as reino,
                        (CASE
                                when t.key = ''subespecie'' then  lafsisbio.fn_get_sq_taxon_search_trilha( lower(trim(sel.value->>''subespecie'')),
                                                                                                         lower(trim(sel.value->>''especie'')),
                                                                                                         INITCAP(trim(sel.value->>''genero'')),
                                                                                                         null,
                                                                                                         null,
                                                                                                         null,
                                                                                                         null,
                                                                                                         null
                                    )
                                when t.key = ''especie'' then  lafsisbio.fn_get_sq_taxon_search_trilha( null,
                                                                                                       lower(trim(sel.value->>''especie'')),
                                                                                                      INITCAP(trim(sel.value->>''genero'')),
                                                                                                      null,
                                                                                                      null,
                                                                                                      null,
                                                                                                      null,
                                                                                                      null
                                    )
                                when t.key = ''genero'' then  lafsisbio.fn_get_sq_taxon_search_trilha( null,
                                                                                                     null,
                                                                                                     INITCAP(trim(sel.value->>''genero'')),
                                                                                                     null,
                                                                                                     null,
                                                                                                     null,
                                                                                                     null,
                                                                                                     null
                                    )
                                when t.key = ''familia'' then  lafsisbio.fn_get_sq_taxon_search_trilha( null,
                                                                                                      null,
                                                                                                      null,
                                                                                                      INITCAP(trim(sel.value->>''familia'')),
                                                                                                      null,
                                                                                                      null,
                                                                                                      null,
                                                                                                      null
                                    )
                                when t.key = ''ordem'' then  lafsisbio.fn_get_sq_taxon_search_trilha( null,
                                                                                                    null,
                                                                                                    null,
                                                                                                    null,
                                                                                                    INITCAP(trim(sel.value->>''ordem'')),
                                                                                                    null,
                                                                                                    null,
                                                                                                    null
                                    )
                                when t.key = ''classe'' then  lafsisbio.fn_get_sq_taxon_search_trilha( null,
                                                                                                     null,
                                                                                                     null,
                                                                                                     null,
                                                                                                     null,
                                                                                                     INITCAP(trim(sel.value->>''classe'')),
                                                                                                     null,
                                                                                                     null
                                    )
                                when t.key = ''filo'' then  lafsisbio.fn_get_sq_taxon_search_trilha( null,
                                                                                                   null,
                                                                                                   null,
                                                                                                   null,
                                                                                                   null,
                                                                                                   null,
                                                                                                   INITCAP(trim(sel.value->>''filo'')),
                                                                                                   null
                                    )
                                else null
                                end 
                            )::integer as sq_taxon
                from sel,
                     json_each_text(sel.value) t
                where t.value is not null
                  and t.key in (
                    SELECT lower(co_nivel_taxonomico)
                    FROM taxonomia.nivel_taxonomico
                    WHERE in_nivel_intermediario is false

                )
            ), sel_json  AS (
               select planilha_linha,
                json_object_agg(''sq_''||key::text, sq_taxon) as data
               from sel_sq_taxon
            group by planilha_linha
            )select tb2.planilha_linha, taxon_json,
INITCAP(trim((taxon_json->>''reino'')::text)::text)::text as reino,
(taxon_json->>''sq_reino'')::integer as sq_reino,
INITCAP(trim((taxon_json->>''filo'')::text)::text)::text as filo,
(taxon_json->>''sq_filo'')::integer as sq_filo,
INITCAP(trim((taxon_json->>''classe'')::text)::text)::text as classe,
(taxon_json->>''sq_classe'')::integer as sq_classe,
INITCAP(trim((taxon_json->>''ordem'')::text)::text)::text as ordem,
(taxon_json->>''sq_ordem'')::integer as sq_ordem,
INITCAP(trim((taxon_json->>''familia'')::text)::text)::text as familia,
(taxon_json->>''sq_familia'')::integer as sq_familia,
INITCAP(trim((taxon_json->>''genero'')::text)::text)::text as genero,
(taxon_json->>''sq_genero'')::integer as sq_genero,
lower(trim((taxon_json->>''especie'')::text)::text)::text as especie,
(taxon_json->>''sq_especie'')::integer as sq_especie,
lower(trim((taxon_json->>''subespecie'')::text)::text)::text as subespecie,
(taxon_json->>''sq_subespecie'')::integer as sq_subespecie,
trim((taxon_json->>''autor'')::text)::text as autor,
trim((taxon_json->>''ano'')::text)::text as ano,
taxon_json->>''operacao_sintax'' as operacao_sintax
   from  (
select sel.planilha_linha, (data::jsonb || value::jsonb )::json as taxon_json
from sel_json
inner JOIN sel ON sel.planilha_linha = sel_json.planilha_linha) as  tb2
       );';

    RAISE INFO 'sql sq_taxon tmp % % %',E'\n',v_sql,E'\n';

    execute v_sql;


EXCEPTION WHEN OTHERS THEN
    BEGIN
        RAISE EXCEPTION 'Erro: fn_planilha_sintax_get_sq_taxon => %', SQLERRM;
    END;

END;
$$;


--
-- TOC entry 4655 (class 1255 OID 23823)
-- Name: fn_planilha_sintax_ver_mudar_nivel(text); Type: FUNCTION; Schema: taxonomia; Owner: -
--

CREATE FUNCTION taxonomia.fn_planilha_sintax_ver_mudar_nivel(p_no_tmp_table text) RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_sql text;
    v_record record;
    v_no_tmp_ver_mudar_nivel text;
    rec record;
    v_sql_ver_mudar_nivel text;
     v_planilha_linha integer;
     v_sintax_recorte text;
     v_sintax_recorte_value text;
     v_json_taxon json;
     v_existe integer;
BEGIN
    v_no_tmp_ver_mudar_nivel:=p_no_tmp_table||'_mudar_nivel';


    v_sql:='DROP TABLE IF EXISTS taxonomia.'||v_no_tmp_ver_mudar_nivel||';
            CREATE UNLOGGED TABLE taxonomia.'||v_no_tmp_ver_mudar_nivel||'
            (
		  planilha_linha integer,
		  sintax_nivel_is_null text,
		  total_duplic_nivel bigint,
		  ia_sintax_sugestao_operacao text,
		  reino text,
		  sq_reino integer,
		  filo text,
		  sq_filo integer,
		  classe text,
		  sq_classe integer,
		  ordem text,
		  sq_ordem integer,
		  familia text,
		  sq_familia integer,
		  genero text,
		  sq_genero integer,
		  especie text,
		  sq_especie integer,
		  subespecie text,
		  sq_subespecie integer,
		  autor text,
		  ano text,
		  operacao_sintax text
            );';

    execute v_sql;

--     for v_record in (
--         SELECT reino, filo, classe, ordem, familia, genero, subgenero, especie,
--                autor, ano, operacao_sintax, sintax_recorte, planilha_linha
--         FROM taxonomia.tmp_sintax_68
--     )
--     loop
--
--     end loop;
/**
  TODO utilizar o recorte informado na planilha para a hierarquia
 */

    FOR v_record IN  EXECUTE format('SELECT * FROM taxonomia.%I',p_no_tmp_table)
        LOOP
            RAISE INFO '%', v_record;

               
			execute 'select count(*) from taxonomia.'||p_no_tmp_table||'_erro where planilha_linha = '||v_record.planilha_linha||';' into v_existe;

			if v_existe = 0 then 

				    v_sql:='select (m.value->>''planilha_linha'')::integer AS planilha_linha
				     , (select lower(sintax_recorte) from taxonomia.'||p_no_tmp_table||' where planilha_linha = '||v_record.planilha_linha||') AS sintax_recorte
				     , (
					select value from json_each_text(m.value) j 
						 where j.key = (select lower(sintax_recorte) from taxonomia.'||p_no_tmp_table||' where planilha_linha = '||v_record.planilha_linha||')
				     ) as sintax_recorte_value
				     , m.value as json_taxon
				     FROM
					json_array_elements(
						(select array_to_json(array_agg( row_to_json(tb1) ) )   from  (
													      select *
													      from taxonomia.'||p_no_tmp_table||'
													      where planilha_linha = '||v_record.planilha_linha||'
													      ) tb1
						)::json
					    ) AS m;
				';
			    RAISE INFO 'v_sql % %', v_sql, E'\n';
			    execute  ''||v_sql||'' into v_planilha_linha, v_sintax_recorte, v_sintax_recorte_value,v_json_taxon;
			    
				    v_sql:='INSERT INTO taxonomia.'||v_no_tmp_ver_mudar_nivel||'
					       with sel AS (
						    select (m.value->>''planilha_linha'')::integer AS planilha_linha
							    , m.* 
							    FROM
							    json_array_elements(
								(select array_to_json(array_agg( row_to_json(tb1) ) )   from  (
															      select *
															      from taxonomia.'||p_no_tmp_table||'
															      where planilha_linha = '||v_record.planilha_linha||'
															      ) tb1
								)::json
							    ) AS m
						), sel_sq_taxon AS (
						select sel.value as linha_json,
							sel.planilha_linha,
							t.key,t.value, --array_agg(planilha_linha),
							sel.value->>''subespecie'' as subespecie,
							sel.value->>''especie'' as especie,
							sel.value->>''genero'' as genero,
							sel.value->>''familia'' as familia,
							sel.value->>''ordem'' as ordem,
							sel.value->>''classe'' as classe,
							sel.value->>''filo'' as filo,
							sel.value->>''reino'' as reino,
							(CASE
								when t.key = ''subespecie'' then  taxonomia.fn_get_sq_taxon_search_trilha( lower(trim(sel.value->>''subespecie'')),
																	 lower(trim(sel.value->>''especie'')),
																	 INITCAP(trim(sel.value->>''genero'')),
																      case
																	when sel.value->>''sintax_recorte'' = ''familia'' then '''||v_sintax_recorte_value||'''
																	else null
																      end,
																	 case
																	when sel.value->>''sintax_recorte'' = ''ordem'' then '''||v_sintax_recorte_value||'''
																	else null
																      end,
																	 case
																	when sel.value->>''sintax_recorte'' = ''classe'' then '''||v_sintax_recorte_value||'''
																	else null
																      end,
																	 case
																	when sel.value->>''sintax_recorte'' = ''filo'' then '''||v_sintax_recorte_value||'''
																	else null
																      end,
																	 case
																	when sel.value->>''sintax_recorte'' = ''reino'' then '''||v_sintax_recorte_value||'''
																	else null
																      end
								    )
								when t.key = ''especie'' then  taxonomia.fn_get_sq_taxon_search_trilha( null,
																       lower(trim(sel.value->>''especie'')),
																      INITCAP(trim(sel.value->>''genero'')),
																      case
																	when sel.value->>''sintax_recorte'' = ''familia'' then '''||v_sintax_recorte_value||'''
																	else null
																      end,
																       case
																	when sel.value->>''sintax_recorte'' = ''ordem'' then '''||v_sintax_recorte_value||'''
																	else null
																      end,
																      case
																	when sel.value->>''sintax_recorte'' = ''classe'' then '''||v_sintax_recorte_value||'''
																	else null
																      end,
																      case
																	when sel.value->>''sintax_recorte'' = ''filo'' then '''||v_sintax_recorte_value||'''
																	else null
																      end,
																	 case
																	when sel.value->>''sintax_recorte'' = ''reino'' then '''||v_sintax_recorte_value||'''
																	else null
																      end
								    )
								when t.key = ''genero'' then  taxonomia.fn_get_sq_taxon_search_trilha( null,
																     null,
																     INITCAP(trim(sel.value->>''genero'')),

																      case
																	when sel.value->>''sintax_recorte'' = ''familia'' then '''||v_sintax_recorte_value||'''
																	else null
																      end,
																      case
																	when sel.value->>''sintax_recorte'' = ''ordem'' then '''||v_sintax_recorte_value||'''
																	else null
																      end,
																     case
																	when sel.value->>''sintax_recorte'' = ''classe'' then '''||v_sintax_recorte_value||'''
																	else null
																      end,
																     case
																	when sel.value->>''sintax_recorte'' = ''filo'' then '''||v_sintax_recorte_value||'''
																	else null
																      end,
																	case
																	when sel.value->>''sintax_recorte'' = ''reino'' then '''||v_sintax_recorte_value||'''
																	else null
																      end
								    )
								when t.key = ''familia'' then  taxonomia.fn_get_sq_taxon_search_trilha( null,
																      null,
																      null,
																      INITCAP(trim(sel.value->>''familia'')),
																       case
																	when sel.value->>''sintax_recorte'' = ''ordem'' then '''||v_sintax_recorte_value||'''
																	else null
																      end,
																      case
																	when sel.value->>''sintax_recorte'' = ''classe'' then '''||v_sintax_recorte_value||'''
																	else null
																      end,
																      case
																	when sel.value->>''sintax_recorte'' = ''filo'' then '''||v_sintax_recorte_value||'''
																	else null
																      end,
																	 case
																	when sel.value->>''sintax_recorte'' = ''reino'' then '''||v_sintax_recorte_value||'''
																	else null
																      end
								    )
								when t.key = ''ordem'' then  taxonomia.fn_get_sq_taxon_search_trilha( null,
																    null,
																    null,
																    null,
																    INITCAP(trim(sel.value->>''ordem'')),
																    case
																	when sel.value->>''sintax_recorte'' = ''classe'' then '''||v_sintax_recorte_value||'''
																	else null
																    end,
																    case
																	when sel.value->>''sintax_recorte'' = ''filo'' then '''||v_sintax_recorte_value||'''
																	else null
																      end,
																       case
																	when sel.value->>''sintax_recorte'' = ''reino'' then '''||v_sintax_recorte_value||'''
																	else null
																      end
								    )
								when t.key = ''classe'' then  taxonomia.fn_get_sq_taxon_search_trilha( null,
																     null,
																     null,
																     null,
																     null,
																     INITCAP(trim(sel.value->>''classe'')),
																     case
																	when sel.value->>''sintax_recorte'' = ''filo'' then null
																	else null
																      end,
																	case
																	when sel.value->>''sintax_recorte'' = ''reino'' then '''||v_sintax_recorte_value||'''
																	else null
																      end
								    )
								when t.key = ''filo'' then  taxonomia.fn_get_sq_taxon_search_trilha( null,
																   null,
																   null,
																   null,
																   null,
																   null,
																   INITCAP(trim(sel.value->>''filo'')),
																      case
																	when sel.value->>''sintax_recorte'' = ''reino'' then '''||v_sintax_recorte_value||'''
																	else null
																      end
								    )
								when t.key = ''reino'' then  taxonomia.fn_get_sq_taxon_search_trilha( null,
																   null,
																   null,
																   null,
																   null,
																   null,
																   null,
																   INITCAP(trim(sel.value->>''reino''))
								    )
								else null
								end
							    )::json as json_taxon
						from sel,
						     json_each_text(sel.value) t
						where t.value is not null
						  and t.key in (
						    SELECT lower(co_nivel_taxonomico)
						    FROM taxonomia.nivel_taxonomico
						    WHERE in_nivel_intermediario is false

						)
					    ), sel_json  AS (
							    select planilha_linha
								 ,json_object_agg(''sq_''||key::text, (json_taxon->''data''->0->>''sq_taxon'')::integer) as json_object_agg_niveis
								 --,json_taxon->>''total_sq_taxon_nivel'' as total_sq_taxon_nivel
								 ,(select count(*) from sel_sq_taxon where ( json_taxon->>''total_sq_taxon_nivel'')::integer>1) as total_duplic_nivel
							    from sel_sq_taxon


							    group by planilha_linha
							)
							select tb2.planilha_linha
								,tb2.sintax_nivel_is_null
								,tb2.total_duplic_nivel
							       ,case
								   when total_duplic_nivel = 0 and tb2.sintax_nivel_is_null is not null  then ''mudar_nivel(''||tb2.sintax_nivel_is_null||'')''
								   else null
							       end as IA_SINTAX_SUGESTAO_OPERACAO
								,INITCAP(trim((taxon_json->>''reino'')::text)::text)::text as reino,
							       (taxon_json->>''sq_reino'')::integer as sq_reino,
							       INITCAP(trim((taxon_json->>''filo'')::text)::text)::text as filo,
							       (taxon_json->>''sq_filo'')::integer as sq_filo,
							       INITCAP(trim((taxon_json->>''classe'')::text)::text)::text as classe,
							       (taxon_json->>''sq_classe'')::integer as sq_classe,
							       INITCAP(trim((taxon_json->>''ordem'')::text)::text)::text as ordem,
							       (taxon_json->>''sq_ordem'')::integer as sq_ordem,
							       INITCAP(trim((taxon_json->>''familia'')::text)::text)::text as familia,
							       (taxon_json->>''sq_familia'')::integer as sq_familia,
							       INITCAP(trim((taxon_json->>''genero'')::text)::text)::text as genero,
							       (taxon_json->>''sq_genero'')::integer as sq_genero,
							       lower(trim((taxon_json->>''especie'')::text)::text)::text as especie,
							       (taxon_json->>''sq_especie'')::integer as sq_especie,
							       lower(trim((taxon_json->>''subespecie'')::text)::text)::text as subespecie,
							       (taxon_json->>''sq_subespecie'')::integer as sq_subespecie,
							       trim((taxon_json->>''autor'')::text)::text as autor,
							       trim((taxon_json->>''ano'')::text)::text as ano,
							       taxon_json->>''operacao_sintax'' as operacao_sintax
							from (
								 select sel_json.planilha_linha
								      --sel_json.sintax_nivel_is_null
								      ,total_duplic_nivel
								     ,(
								select array_to_string(array_agg(replace(m.key,''sq_'','''')),''|'') as sintax_nivel_is_null
								from json_each_text(sel_json.json_object_agg_niveis)
								     as m
								where m.value is null
								  ) as sintax_nivel_is_null
								     ,(json_object_agg_niveis::jsonb || value::jsonb )::json as taxon_json

								 from sel_json
									  inner JOIN sel ON sel.planilha_linha = sel_json.planilha_linha
							     ) tb2
				       ;';

				    RAISE INFO 'sql sq_taxon tmp % % %',E'\n',v_sql,E'\n';

				    execute v_sql;

                    end if;
        END LOOP;

EXCEPTION WHEN OTHERS THEN
    BEGIN
        RAISE EXCEPTION 'Erro: fn_planilha_sintax_get_sq_taxon => %', SQLERRM;
    END;

END;
$$;


--
-- TOC entry 4656 (class 1255 OID 23826)
-- Name: fn_planilha_sintax_verif_previa(text); Type: FUNCTION; Schema: taxonomia; Owner: -
--

CREATE FUNCTION taxonomia.fn_planilha_sintax_verif_previa(p_no_tmp_table text) RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_sql text;
    v_no_tmp text;
BEGIN

    v_no_tmp:=p_no_tmp_table||'_erro';

    v_sql:='DROP TABLE IF EXISTS taxonomia.'||v_no_tmp||';
    CREATE UNLOGGED TABLE taxonomia.'||v_no_tmp||'
    (
        erro_descricao text,
        erro_coluna text,
        erro_valor text,
        planilha_linha integer,
        reino text,
        filo text,
        classe text,
        ordem text,
        familia text,
        genero text,
        especie text,
        subespecie text
    );';

    execute v_sql;

    v_sql:='INSERT INTO taxonomia.'||v_no_tmp||'
     with sel AS (
        select (m.value->>''planilha_linha'')::integer AS planilha_linha
             ,m.*
        FROM
            json_array_elements(
                    (select array_to_json(array_agg( row_to_json(tb1) ) )   from  (
                                                                                      select *
                                                                                      from taxonomia.'||p_no_tmp_table||'
                                                                                  ) tb1
                    )::json
                ) AS m
    )select
         ''Na linha ''||sel.planilha_linha||'' da planilha "SINTAX". A coluna "''||t.key||''" possui ''||array_length(STRING_TO_ARRAY(REGEXP_REPLACE(trim(t.value),  ''[^\w\s]'', '''', ''g''), '' ''),1)||'' nomes. Permitido um nome por celuna.'' as erro_descricao,
         t.key as erro_coluna,
         t.value as erro_valor, --array_agg(planilha_linha),
         (sel.planilha_linha)::integer,
         sel.value->>''reino'' as reino,
         sel.value->>''filo'' as filo,
         sel.value->>''classe'' as classe,
         sel.value->>''ordem'' as ordem,
         sel.value->>''familia'' as familia,
         sel.value->>''genero'' as genero,
         sel.value->>''especie'' as especie,
         sel.value->>''subespecie'' as subespecie
    from sel,
         json_each_text(sel.value) t
    where t.value is not null
      and  array_length(STRING_TO_ARRAY(REGEXP_REPLACE(trim(t.value),  ''[^\w\s]'', '''', ''g''), '' ''),1) > 1
      and t.key in (
        SELECT lower(co_nivel_taxonomico)
        FROM taxonomia.nivel_taxonomico
        WHERE in_nivel_intermediario is false

    );';
    RAISE INFO 'v_sql  : % [ % ] %', E'\n',  v_sql, E'\n';
    execute v_sql;

-------------------------------------------------------------------------------------------------------------------
    /**
      Verificar se os valores do recorte_taxonomico esta permitido.
     */


    v_sql:='INSERT INTO taxonomia.'||v_no_tmp||'
      with sel AS (
        select m.value->>''planilha_linha'' AS planilha_linha
             ,m.*
        FROM
            json_array_elements(
                    (select array_to_json(array_agg( row_to_json(tb1) ) )   from  (
                                                                                      select *
                                                                                      from taxonomia.'||p_no_tmp_table||'
                                                                                  ) tb1
                    )::json
                ) AS m
    )select
         ''Na linha ''||planilha_linha||'' da planilha "SINTAX". Na coluna sintax_recorte: Apenas um destes valores (''||(
SELECT array_to_string(array_agg(lower(co_nivel_taxonomico)),''|'')
        FROM taxonomia.nivel_taxonomico
        WHERE in_nivel_intermediario is false)||'') são permitidos '',
         t.key as erro_coluna,
         t.value as erro_valor, --array_agg(planilha_linha),
         (sel.planilha_linha)::integer,
         sel.value->>''reino'' as reino,
         sel.value->>''filo'' as filo,
         sel.value->>''classe'' as classe,
         sel.value->>''ordem'' as ordem,
         sel.value->>''familia'' as familia,
         sel.value->>''genero'' as genero,
         sel.value->>''especie'' as especie,
         sel.value->>''subespecie'' as subespecie
    from sel,
         json_each_text(sel.value) t
    where t.value is not null
      and t.key = ''sintax_recorte''
      and lower(t.value) not in (
        SELECT lower(co_nivel_taxonomico)
        FROM taxonomia.nivel_taxonomico
        WHERE in_nivel_intermediario is false

    );';
    RAISE INFO 'v_sql  : % [ % ] %', E'\n',  v_sql, E'\n';
    execute v_sql;

EXCEPTION WHEN OTHERS THEN
    BEGIN

        RAISE EXCEPTION 'taxonomia.Erro fn_planilha_sintax_verif_previa : [ % ] %', v_sql, SQLERRM;

    END;

END;
$$;


--
-- TOC entry 13242 (class 0 OID 0)
-- Dependencies: 4656
-- Name: FUNCTION fn_planilha_sintax_verif_previa(p_no_tmp_table text); Type: COMMENT; Schema: taxonomia; Owner: -
--

COMMENT ON FUNCTION taxonomia.fn_planilha_sintax_verif_previa(p_no_tmp_table text) IS 'Realiza verificação previa para verificar erros de preenchimento ou negocial';


--
-- TOC entry 4657 (class 1255 OID 23827)
-- Name: formatar_nome(bigint); Type: FUNCTION; Schema: taxonomia; Owner: -
--

CREATE FUNCTION taxonomia.formatar_nome(v_sq_taxon bigint) RETURNS text
    LANGUAGE plpgsql
    AS $$
		DECLARE
   			v_nome_cientifico text := '';
        BEGIN
        WITH RECURSIVE nome_cientifico(sq_taxon_pai,
                               nivel,
                               formatacao_nome,
                               sq_taxon,
                               sq_taxon_search,
                               no_taxon,
                               in_ocorre_brasil,
                               sq_nivel_taxonomico,
                               no_autor,
                               nu_ano,
                               sq_situacao_nome,
                               st_ativo,
                               co_nivel_taxonomico,
                               de_nivel_taxonomico,
                               in_nivel_intermediario,
                               nu_grau_taxonomico
            )
        AS (
            SELECT taxon.sq_taxon_pai, 1::integer as nivel,  taxon.no_taxon::text as formatacao_nome,
            taxon.sq_taxon,
            taxon.sq_taxon as sq_taxon_search,
            taxon.no_taxon,
                taxon.in_ocorre_brasil, taxon.sq_nivel_taxonomico, taxon.no_autor,
                taxon.nu_ano, taxon.sq_situacao_nome, taxon.st_ativo,
            nivel_taxonomico.co_nivel_taxonomico,
            nivel_taxonomico.de_nivel_taxonomico,
            nivel_taxonomico.in_nivel_intermediario,
            nivel_taxonomico.nu_grau_taxonomico
            FROM taxonomia.taxon
            INNER JOIN taxonomia.nivel_taxonomico ON nivel_taxonomico.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
           WHERE taxon.sq_taxon = v_sq_taxon
            UNION
            SELECT  taxon.sq_taxon_pai,
            t.nivel+1::integer as nivel,
            taxon.no_taxon::text  ||' ' || t.formatacao_nome as formatacao_nome,
            taxon.sq_taxon,
            t.sq_taxon_search,
            taxon.no_taxon as no_taxon,
            taxon.in_ocorre_brasil,
            taxon.sq_nivel_taxonomico,
            taxon.no_autor ,
            taxon.nu_ano,
            taxon.sq_situacao_nome,
            taxon.st_ativo,
            nivel_taxonomico.co_nivel_taxonomico,
            nivel_taxonomico.de_nivel_taxonomico,
            nivel_taxonomico.in_nivel_intermediario,
            nivel_taxonomico.nu_grau_taxonomico
                    FROM taxonomia.taxon
                    INNER JOIN taxonomia.nivel_taxonomico
                            ON nivel_taxonomico.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
                           AND nivel_taxonomico.nu_grau_taxonomico in (100,90,80)
                    INNER JOIN  nome_cientifico t on taxon.sq_taxon = t.sq_taxon_pai
        )
        SELECT t.formatacao_nome into v_nome_cientifico
             FROM nome_cientifico t
             WHERE t.nivel = (select max(t2.nivel) from nome_cientifico t2 Where t2.sq_taxon_search =t.sq_taxon_search);
        return v_nome_cientifico;
        END;
$$;


--
-- TOC entry 4658 (class 1255 OID 23828)
-- Name: formatar_trilha_taxon(json, boolean, boolean); Type: FUNCTION; Schema: taxonomia; Owner: -
--

CREATE FUNCTION taxonomia.formatar_trilha_taxon(v_json_trilha json, exibe_nivel_taxonomico boolean DEFAULT true, p_exibe_sq_taxon boolean DEFAULT false) RETURNS text
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_trilha_formatada text := '';
BEGIN

    if exibe_nivel_taxonomico is true then

        with tx AS (
            select taxon.key,
                   taxon.value,
                   nu_grau_taxonomico,
                   de_nivel_taxonomico
            from json_each(v_json_trilha::json) as taxon
                     inner join taxonomia.nivel_taxonomico on nivel_taxonomico.co_nivel_taxonomico = UPPER(taxon.key)
            ORDER BY nivel_taxonomico.nu_grau_taxonomico desc
        )
        select '( ' || (taxon_top.value ->> 'de_nivel_taxonomico')::text || ' )  ' || string_agg(
                case
                    when p_exibe_sq_taxon is false then tx.value ->> 'no_taxon'
                    else '(' || (tx.value ->> 'sq_taxon')::text || ') ' || (tx.value ->> 'no_taxon')::text
                    end
            , ' < ')
        into v_trilha_formatada
        from tx
                 INNER JOIN LATERAL (select * from tx limit 1) AS taxon_top on true
        group by taxon_top.value ->> 'de_nivel_taxonomico';

	else
       select string_agg(
       case 
	   when p_exibe_sq_taxon is false then taxon.value->>'no_taxon'
	   else '('||(taxon.value->>'sq_taxon')::text||') '|| (taxon.value->>'no_taxon')::text
        end
        , ' < '  order by nu_grau_taxonomico desc ) into v_trilha_formatada -- taxon.key, taxon.value, nu_grau_taxonomico
	     from  json_each(v_json_trilha::json) as taxon
	     inner join taxonomia.nivel_taxonomico on nivel_taxonomico.co_nivel_taxonomico = UPPER(taxon.key);
	end if;


	return v_trilha_formatada;
        END;
$$;


--
-- TOC entry 13243 (class 0 OID 0)
-- Dependencies: 4658
-- Name: FUNCTION formatar_trilha_taxon(v_json_trilha json, exibe_nivel_taxonomico boolean, p_exibe_sq_taxon boolean); Type: COMMENT; Schema: taxonomia; Owner: -
--

COMMENT ON FUNCTION taxonomia.formatar_trilha_taxon(v_json_trilha json, exibe_nivel_taxonomico boolean, p_exibe_sq_taxon boolean) IS 'Função para retornar a trilha taxonômica do taxon conforme trilha.
com opção  para mostrar o nivel taxonomico
com opção para mostrar o sq_taxon
';


--
-- TOC entry 4659 (class 1255 OID 23829)
-- Name: gettreeup(bigint); Type: FUNCTION; Schema: taxonomia; Owner: -
--

CREATE FUNCTION taxonomia.gettreeup(v_sq_taxon bigint) RETURNS TABLE(nivel integer, key bigint, ds_caminho text, sq_taxon bigint, in_ocorre_brasil boolean, sq_nivel_taxonomico bigint, no_autor text, no_taxon text, nu_ano integer, sq_situacao_nome bigint, st_ativo boolean, sq_taxon_pai bigint, de_nivel_taxonomico text)
    LANGUAGE plpgsql
    AS $$

DECLARE

BEGIN


    RETURN QUERY WITH RECURSIVE subir_arvore_taxon(nivel, key, ds_caminho, sq_taxon, in_ocorre_brasil, sq_nivel_taxonomico, no_autor, no_taxon, nu_ano, sq_situacao_nome,
     st_ativo, sq_taxon_pai)
                    AS (
                        SELECT 1::integer as nivel, taxon.sq_taxon as key, taxonomia.formatar_nome(taxon.sq_taxon)::text as ds_caminho, taxon.sq_taxon, taxon.in_ocorre_brasil,
                        taxon.sq_nivel_taxonomico, taxon.no_autor,  taxonomia.formatar_nome(taxon.sq_taxon) as no_taxon
                        , taxon.nu_ano, taxon.sq_situacao_nome, taxon.st_ativo, taxon.sq_taxon_pai
                        FROM taxonomia.taxon
                        where taxon.sq_taxon = v_sq_taxon
                        UNION
                        SELECT t.nivel+1::integer as nivel, taxon.sq_taxon as key,
                        (t.ds_caminho::text) ||' < ' ||taxonomia.formatar_nome(taxon.sq_taxon)::text as ds_caminho,
                        taxon.sq_taxon, taxon.in_ocorre_brasil, taxon.sq_nivel_taxonomico, taxon.no_autor , taxonomia.formatar_nome(taxon.sq_taxon) as no_taxon ,
                        taxon.nu_ano, taxon.sq_situacao_nome, taxon.st_ativo, taxon.sq_taxon_pai
                        FROM taxonomia.taxon
                        JOIN  subir_arvore_taxon t  on taxon.sq_taxon = t.sq_taxon_pai
                    )
                    SELECT t.nivel, t.key::bigint, t.ds_caminho, t.sq_taxon::bigint, t.in_ocorre_brasil, t.sq_nivel_taxonomico, t.no_autor::text, t.no_taxon::text, t.nu_ano, t.sq_situacao_nome,
     t.st_ativo, t.sq_taxon_pai, nivel_taxonomico.de_nivel_taxonomico::text
                      FROM subir_arvore_taxon t
                      inner join taxonomia.nivel_taxonomico
                              on nivel_taxonomico.sq_nivel_taxonomico = t.sq_nivel_taxonomico
                      order by t.nivel desc;
END;


$$;


--
-- TOC entry 4660 (class 1255 OID 23830)
-- Name: gettreeupwithbrother(bigint); Type: FUNCTION; Schema: taxonomia; Owner: -
--

CREATE FUNCTION taxonomia.gettreeupwithbrother(v_sq_taxon bigint) RETURNS TABLE(sq_taxon_top bigint, sq_taxon_pai bigint, sq_taxon bigint, sq_nivel_taxonomico bigint, no_taxon text, no_taxon_sem_formatar text, co_nivel_taxonomico text, de_nivel_taxonomico text, in_nivel_intermediario boolean, nu_grau_taxonomico integer, in_ocorre_brasil boolean, no_autor text, nu_ano integer, sq_situacao_nome bigint, st_ativo boolean, taxonomia_hierarquia text)
    LANGUAGE plpgsql
    AS $$

DECLARE

BEGIN

    RETURN QUERY  SELECT
    tbf.array_sq_taxon_pai_subida[array_length(tbf.array_sq_taxon_pai_subida,1)]::bigint as sq_taxon_top ,
    tbf.sq_taxon_pai::bigint,
    tbf.sq_taxon::bigint,
    tbf.sq_nivel_taxonomico::bigint,
    tbf.no_taxon,
    tbf.no_taxon_sem_formatar,
    nivel_taxonomico.co_nivel_taxonomico::text,
    nivel_taxonomico.de_nivel_taxonomico::text,
    nivel_taxonomico.in_nivel_intermediario,
    nivel_taxonomico.nu_grau_taxonomico,
    tbf.in_ocorre_brasil,
    tbf.no_autor,
    tbf.nu_ano,
    tbf.sq_situacao_nome,
    tbf.st_ativo,
    COALESCE((
  WITH RECURSIVE descer_arvore_taxon(
            sq_taxon_topo,
            sq_taxon_pai,
            nivel,
            key,
            sq_taxon,
            ds_caminho,
            no_taxon,
            sq_nivel_taxonomico,
            no_autor,
            nu_ano,
            sq_situacao_nome,
            in_ocorre_brasil,
            st_ativo)
             AS (
                SELECT taxon.sq_taxon as sq_taxon_topo,
                    taxon.sq_taxon_pai, 1::integer as nivel,
                    taxon.sq_taxon as key,
                    taxon.sq_taxon,
                    taxon.no_taxon::text as ds_caminho,
                    taxon.no_taxon,
                    taxon.sq_nivel_taxonomico,
                    taxon.no_autor,
                    taxon.nu_ano,
                    taxon.sq_situacao_nome,
                    taxon.in_ocorre_brasil,
                    taxon.st_ativo
                FROM taxonomia.taxon
                WHERE taxon.sq_taxon in ( tbf.array_sq_taxon_pai_subida[array_length(array_sq_taxon_pai_subida,1)]) --top
                UNION
                    SELECT  t.sq_taxon_topo,taxon.sq_taxon_pai, t.nivel+1::integer as nivel, taxon.sq_taxon as key,
                        taxon.sq_taxon, ds_caminho ||' => ' ||taxon.no_taxon  as ds_caminho,taxon.no_taxon as no_taxon,
                        taxon.sq_nivel_taxonomico,
                        taxon.no_autor,
                        taxon.nu_ano,
                        taxon.sq_situacao_nome,
                        taxon.in_ocorre_brasil,
                        taxon.st_ativo
                     FROM taxonomia.taxon
                     JOIN  descer_arvore_taxon t ON taxon.sq_taxon_pai = t.sq_taxon
                     WHERE taxon.sq_taxon = ANY((array_append(tbf.array_sq_taxon_pai_subida, tbf.sq_taxon)))
             ) SELECT ds_caminho FROM descer_arvore_taxon t
                WHERE t.sq_taxon = tbf.sq_taxon
    ),tbf.no_taxon) as taxonomia_hierarquia
        FROM (
        WITH arvore_taxonomica_irmao(sq_taxon_pai, nivel, key, sq_taxon_irmao,
                                     sq_taxon,
                                     no_taxon,
                                     in_ocorre_brasil,
                                     sq_nivel_taxonomico,
                                     no_autor,
                                     nu_ano,
                                     sq_situacao_nome,
                                     st_ativo)
            AS (
            WITH RECURSIVE subir_arvore_taxon(sq_taxon_pai, nivel, key, sq_taxon_irmao,
                                              sq_taxon,
                                              no_taxon,
                                              in_ocorre_brasil, sq_nivel_taxonomico, no_autor,
                                              nu_ano, sq_situacao_nome, st_ativo)
                                    AS (
                                        SELECT taxon.sq_taxon_pai, 1::integer as nivel, taxon.sq_taxon as key,  taxon.sq_taxon as sq_taxon_irmao,
                                        taxon.sq_taxon,
                                        taxon.no_taxon,
                                        taxon.in_ocorre_brasil,
                                        taxon.sq_nivel_taxonomico,
                                        taxon.no_autor,
                                        taxon.nu_ano,
                                        taxon.sq_situacao_nome,
                                        taxon.st_ativo
                                        FROM taxonomia.taxon
                                        LEFT JOIN taxonomia.taxon as tp ON tp.sq_taxon = taxon.sq_taxon_pai
                                       where taxon.sq_taxon = v_sq_taxon
                                        UNION
                                        SELECT  taxon.sq_taxon_pai,
						t.nivel+1::integer as nivel,
						taxon.sq_taxon as key,
						t.sq_taxon_irmao as sq_taxon_irmao,
						taxon.sq_taxon,
						taxon.no_taxon as no_taxon,
						taxon.in_ocorre_brasil,
						taxon.sq_nivel_taxonomico,
						taxon.no_autor ,
						taxon.nu_ano,
						taxon.sq_situacao_nome,
						taxon.st_ativo
					FROM taxonomia.taxon
					LEFT JOIN taxonomia.taxon as tp ON tp.sq_taxon = taxon.sq_taxon_pai
					JOIN  subir_arvore_taxon t
					  on taxon.sq_taxon = t.sq_taxon_pai
					 AND taxon.sq_taxon <> t.sq_taxon
                                    )
                                   SELECT
				      t.sq_taxon_pai,
				      t.nivel,
				      t.key,
				      t.sq_taxon_irmao,
				      t.sq_taxon,
				      t.no_taxon,
				      t.in_ocorre_brasil,
				      t.sq_nivel_taxonomico,
				      t.no_autor,
				      t.nu_ano,
				      t.sq_situacao_nome,
				      t.st_ativo
				 FROM subir_arvore_taxon t
					UNION
                                      SELECT
                                         taxon.sq_taxon_pai,
                                         nivel,
                                         taxon.sq_taxon as key,
                                         t2.sq_taxon_irmao as sq_taxon_irmao,
                                         taxon.sq_taxon,
                                         taxon.no_taxon,
                                         taxon.in_ocorre_brasil,
                                         taxon.sq_nivel_taxonomico,
                                         taxon.no_autor,
                                         taxon.nu_ano,
                                         taxon.sq_situacao_nome,
                                         taxon.st_ativo
                                       FROM subir_arvore_taxon t2
                                       INNER JOIN taxonomia.taxon
                                          ON
					CASE
					    WHEN  t2.sq_taxon_pai is not null
						THEN   taxon.sq_taxon_pai = t2.sq_taxon_pai
					    ELSE  taxon.sq_taxon_pai is null
					END
                                        AND taxon.sq_taxon <> t2.sq_taxon
            )select
              ( WITH RECURSIVE subir_arvore_taxon2(
                      sq_taxon_pai,
                      nivel,
                      key,
                      sq_taxon_irmao,
                      sq_taxon,
                      no_taxon,
                      in_ocorre_brasil,
                      sq_nivel_taxonomico,
                      no_autor,
                      nu_ano,
                      sq_situacao_nome,
                      st_ativo)
                     AS (
                        SELECT taxon.sq_taxon_pai,
                         1::integer as nivel,
                         taxon.sq_taxon as key,
                         taxon.sq_taxon as sq_taxon_irmao,
                         taxon.sq_taxon,
                         taxon.no_taxon,
                         taxon.in_ocorre_brasil,
                         taxon.sq_nivel_taxonomico,
                         taxon.no_autor,
                         taxon.nu_ano,
                         taxon.sq_situacao_nome,
                         taxon.st_ativo
                        FROM taxonomia.taxon
                        LEFT JOIN taxonomia.taxon as tp ON tp.sq_taxon = taxon.sq_taxon_pai
                           where taxon.sq_taxon = arvore_taxonomica_irmao.sq_taxon_pai
                        UNION
                        SELECT  taxon.sq_taxon_pai,
                                t.nivel+1::integer as nivel,
                                taxon.sq_taxon as key,
                                t.sq_taxon_irmao as sq_taxon_irmao,
                                taxon.sq_taxon,
                                taxon.no_taxon as no_taxon,
                                taxon.in_ocorre_brasil,
                                taxon.sq_nivel_taxonomico,
                                taxon.no_autor ,
                                taxon.nu_ano,
                                taxon.sq_situacao_nome,
                                taxon.st_ativo
                         FROM taxonomia.taxon
                         LEFT JOIN taxonomia.taxon as tp ON tp.sq_taxon = taxon.sq_taxon_pai
                         JOIN  subir_arvore_taxon2 t
                           ON taxon.sq_taxon = t.sq_taxon_pai
                           AND taxon.sq_taxon <> t.sq_taxon
                    )
                   SELECT
                         array_agg( t2.sq_taxon) as array_sq_taxon_pai_subida
                     FROM subir_arvore_taxon2 t2
                    group by sq_taxon_irmao
                            ) as array_sq_taxon_pai_subida,
                          arvore_taxonomica_irmao.sq_taxon_pai,
                          arvore_taxonomica_irmao.nivel,
                          arvore_taxonomica_irmao.sq_taxon,
                          taxonomia.formatar_nome(arvore_taxonomica_irmao.sq_taxon) as no_taxon ,
                          arvore_taxonomica_irmao.no_taxon::text as no_taxon_sem_formatar,
                          arvore_taxonomica_irmao.sq_nivel_taxonomico,
                          arvore_taxonomica_irmao.in_ocorre_brasil,
                          arvore_taxonomica_irmao.no_autor::text,
                          arvore_taxonomica_irmao.nu_ano,
                          arvore_taxonomica_irmao.sq_situacao_nome,
                          arvore_taxonomica_irmao.st_ativo
                        from arvore_taxonomica_irmao ) tbf
                        INNER JOIN taxonomia.nivel_taxonomico
                           ON nivel_taxonomico.sq_nivel_taxonomico = tbf.sq_nivel_taxonomico
                        ORDER BY taxonomia_hierarquia;
END;


$$;


--
-- TOC entry 4661 (class 1255 OID 23833)
-- Name: json_diff(jsonb, jsonb); Type: FUNCTION; Schema: taxonomia; Owner: -
--

CREATE FUNCTION taxonomia.json_diff(p_json_trilha jsonb, p_json_taxon jsonb) RETURNS jsonb
    LANGUAGE sql
    AS $$
    SELECT jsonb_object_agg(a.key, a.value) FROM
        ( SELECT key, value FROM jsonb_each(p_json_trilha) ) a LEFT OUTER JOIN
        ( SELECT key, value FROM jsonb_each(p_json_taxon) ) b ON a.key = b.key
    WHERE a.value != b.value OR b.key IS NULL;
$$;


--
-- TOC entry 4662 (class 1255 OID 23834)
-- Name: json_only_hierarquia_taxon(json); Type: FUNCTION; Schema: taxonomia; Owner: -
--

CREATE FUNCTION taxonomia.json_only_hierarquia_taxon(p_json json) RETURNS json
    LANGUAGE sql
    AS $$
	select json_object_agg(key::text, value::text ORDER BY nivel_taxonomico.nu_grau_taxonomico) 
	FROM  
	json_each_text(p_json)
	as m
	INNER JOIN  taxonomia.nivel_taxonomico ON lower(nivel_taxonomico.co_nivel_taxonomico)  = m.key
	--WHERE in_nivel_intermediario is true
$$;


--
-- TOC entry 13244 (class 0 OID 0)
-- Dependencies: 4662
-- Name: FUNCTION json_only_hierarquia_taxon(p_json json); Type: COMMENT; Schema: taxonomia; Owner: -
--

COMMENT ON FUNCTION taxonomia.json_only_hierarquia_taxon(p_json json) IS 'Função para compara diferenca de dois objetos json';


--
-- TOC entry 4663 (class 1255 OID 23835)
-- Name: search_taxon(character varying, bigint); Type: FUNCTION; Schema: taxonomia; Owner: -
--

CREATE FUNCTION taxonomia.search_taxon(v_no_taxon character varying, p_sq_nivel_taxonomico bigint) RETURNS TABLE(total bigint, qtd_nivel bigint, no_taxon_hierarquia text, sq_taxon_pai bigint, nivel bigint, sq_taxon bigint, sq_taxon_search bigint, no_taxon text, in_ocorre_brasil boolean, sq_nivel_taxonomico bigint, no_autor text, nu_ano integer, sq_situacao_nome bigint, st_ativo boolean, co_nivel_taxonomico text, de_nivel_taxonomico text, in_nivel_intermediario boolean, nu_grau_taxonomico integer, qtd_paramentro integer, v_array text[])
    LANGUAGE plpgsql
    AS $$
DECLARE
--v_array text[];
--v_qtd_paramentro int;
--v_taxon taxonomia.taxon.sq_taxon%TYPE;
BEGIN
--select  regexp_split_to_array(trim(v_no_taxon), E'\\s+') into v_array;
--select  array_length(v_array, 1) into v_qtd_paramentro;

RETURN QUERY  select  t.total as r_total,
		t.qtd_nivel as r_qtd_nivel,
		t.no_taxon_hierarquia as r_no_taxon_hierarquia, 
--		t.formatacao_nome as r_formatacao_nome, 
--		t.formatacao_trilha_com_nivel as r_formatacao_trilha_com_nivel, 
		t.sq_taxon_pai as r_sq_taxon_pai,
		t.nivel  as r_nivel,
		t.sq_taxon as r_sq_taxon,
		t.sq_taxon_search as r_sq_taxon_search,
		t.no_taxon as r_no_taxon,
		t.in_ocorre_brasil as r_in_ocorre_brasil,
		t.sq_nivel_taxonomico as r_sq_nivel_taxonomico,
		t.no_autor as r_no_autor,
		t.nu_ano as r_nu_ano ,
		t.sq_situacao_nome as r_sq_situacao_nome,
		t.st_ativo as r_st_ativo,
		t.co_nivel_taxonomico as r_co_nivel_taxonomico,
		t.de_nivel_taxonomico as r_de_nivel_taxonomico,
		t.in_nivel_intermediario as r_in_nivel_intermediario,
		t.nu_grau_taxonomico as r_nu_grau_taxonomico,
		t.qtd_paramentro as r_qtd_paramentro,
		t.v_array as r_v_array
		from taxonomia.search_taxon_trilha(v_no_taxon,p_sq_nivel_taxonomico) as t
		order by t.nu_grau_taxonomico;
/**
    RETURN QUERY WITH RECURSIVE search_taxon(sq_taxon_pai,
                               nivel,
                               formatacao_nome,
                               sq_taxon,
                               sq_taxon_search,
                               no_taxon,
                               in_ocorre_brasil,
                               sq_nivel_taxonomico,
                               no_autor,
                               nu_ano,
                               sq_situacao_nome,
                               st_ativo,
                               co_nivel_taxonomico,
                               de_nivel_taxonomico,
                               in_nivel_intermediario,
                               nu_grau_taxonomico
    )
AS (
    SELECT
    taxon.sq_taxon_pai,
    1::integer as nivel,
    taxonomia.formatar_nome(taxon.sq_taxon)::text as formatacao_nome,
    taxon.sq_taxon,
    taxon.sq_taxon as sq_taxon_search,
    taxon.no_taxon,
    taxon.in_ocorre_brasil, taxon.sq_nivel_taxonomico, taxon.no_autor,
    taxon.nu_ano, taxon.sq_situacao_nome, taxon.st_ativo,
    nivel_taxonomico.co_nivel_taxonomico,
    nivel_taxonomico.de_nivel_taxonomico,
    nivel_taxonomico.in_nivel_intermediario,
    nivel_taxonomico.nu_grau_taxonomico
    FROM taxonomia.taxon
    INNER JOIN taxonomia.nivel_taxonomico ON nivel_taxonomico.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
   WHERE
    CASE
         WHEN v_qtd_paramentro = 2 then
           taxon.sq_taxon in  (select tb3.sq_taxon
                              from taxonomia.taxon as tb3
                              INNER join taxonomia.taxon as superio ON superio.sq_taxon = tb3.sq_taxon_pai
                                     AND superio.no_taxon = initcap(v_array[1])
                               where
                                 tb3.no_taxon = lower(v_array[2])
                           )
       WHEN v_qtd_paramentro > 2 then
           taxon.sq_taxon in  (select tb3.sq_taxon
                              from taxonomia.taxon as tb3
                                       INNER join taxonomia.taxon as superio
                                               ON superio.sq_taxon = tb3.sq_taxon_pai
                                              AND superio.no_taxon = lower(v_array[2])
                                       INNER join taxonomia.taxon as avo
                                               ON avo.sq_taxon = superio.sq_taxon_pai
                                              AND avo.no_taxon = initcap(v_array[1])
                               where
                                  tb3.no_taxon =  lower(v_array[3])
                           )
        ELSE
        taxon.sq_taxon in  (select tb3.sq_taxon
                              from taxonomia.taxon as tb3
                             where
                                    CASE
                                        WHEN v_sq_nivel_taxonomico IS NOT NULL THEN
                                          tb3.no_taxon ilike '%'||v_array[1]||'%' and tb3.sq_nivel_taxonomico = v_sq_nivel_taxonomico
                                        ELSE
                                          tb3.no_taxon ilike '%'||v_array[1]||'%'
                                     END
                             )
    END



--     2025882
--     6802
--        3570
    UNION
    SELECT  taxon.sq_taxon_pai,
            t.nivel+1::integer as nivel,
            t.formatacao_nome ||' < ' ||taxonomia.formatar_nome(taxon.sq_taxon)::text  as formatacao_nome,
            t.sq_taxon,
            t.sq_taxon_search,
            t.no_taxon as no_taxon,
            t.in_ocorre_brasil,
            t.sq_nivel_taxonomico,
            t.no_autor,
            t.nu_ano,
            t.sq_situacao_nome,
            t.st_ativo,
            t.co_nivel_taxonomico,
            t.de_nivel_taxonomico,
            t.in_nivel_intermediario,
            t.nu_grau_taxonomico
            FROM taxonomia.taxon
            INNER JOIN taxonomia.nivel_taxonomico
                    ON nivel_taxonomico.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
            INNER JOIN  search_taxon t on taxon.sq_taxon = t.sq_taxon_pai
)
SELECT (ROW_NUMBER() OVER(
    PARTITION BY t.co_nivel_taxonomico
    ORDER BY  t.nu_grau_taxonomico , t.formatacao_nome))::bigint as sequencia_nivel,
     (COUNT(t.co_nivel_taxonomico) OVER (PARTITION BY t.co_nivel_taxonomico) )::bigint as  qtd_nivel,
      t.formatacao_nome::text as r_formatacao_nome,
      t.sq_taxon_pai::bigint as r_sq_taxon_pai ,
      t.nivel::bigint as r_nivel ,
      t.sq_taxon::bigint as r_sq_taxon ,
      t.sq_taxon_search::bigint as r_sq_taxon_search ,
      t.no_taxon::text as r_no_taxon ,
      t.in_ocorre_brasil as r_in_ocorre_brasil ,
      t.sq_nivel_taxonomico::bigint as r_sq_nivel_taxonomico ,
      t.no_autor::text as r_no_autor ,
      t.nu_ano::integer as r_nu_ano ,
      t.sq_situacao_nome::bigint as r_sq_situacao_nome ,
      t.st_ativo as r_st_ativo ,
      t.co_nivel_taxonomico::text as r_co_nivel_taxonomico ,
      t.de_nivel_taxonomico::text as r_de_nivel_taxonomico ,
      t.in_nivel_intermediario as r_in_nivel_intermediario ,
      t.nu_grau_taxonomico::integer as r_nu_grau_taxonomico,
      v_qtd_paramentro::integer as qtd_paramentro,
      v_array as v_array
     FROM search_taxon t
  WHERE t.nivel = (select max(t2.nivel) from search_taxon t2 where t2.sq_taxon_search = t.sq_taxon_search )
 ORDER BY t.nu_grau_taxonomico , t.formatacao_nome ;
**/
END;

$$;


--
-- TOC entry 4664 (class 1255 OID 23838)
-- Name: search_taxon_trilha(character varying, bigint); Type: FUNCTION; Schema: taxonomia; Owner: -
--

CREATE FUNCTION taxonomia.search_taxon_trilha(v_no_taxon character varying, p_sq_nivel_taxonomico bigint) RETURNS TABLE(total bigint, qtd_nivel bigint, no_taxon_hierarquia text, formatacao_nome text, formatacao_trilha_com_nivel text, sq_taxon_pai bigint, nivel bigint, sq_taxon bigint, sq_taxon_search bigint, no_taxon text, in_ocorre_brasil boolean, sq_nivel_taxonomico bigint, no_autor text, nu_ano integer, sq_situacao_nome bigint, st_ativo boolean, co_nivel_taxonomico text, de_nivel_taxonomico text, in_nivel_intermediario boolean, nu_grau_taxonomico integer, qtd_paramentro integer, v_array text[])
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_existe integer;
    v_array text[];
    v_qtd_paramentro int;
    subespecie text;
    especie text;
    genero text;
    v_where text ;
    v_co_nivel_taxonomico text;
    v_sq_nivel_taxonomico taxonomia.taxon.sq_nivel_taxonomico%TYPE;
    v_sql text;
	v_count_letras integer;
BEGIN
    select  regexp_split_to_array(trim(v_no_taxon), E'\\s+') into v_array;
    select  array_length(v_array, 1) into v_qtd_paramentro;

    v_where:='';
    v_sq_nivel_taxonomico:=null;
    subespecie:=null;
    especie:=null;
    genero:=null;

    /**
	Verifica se p_sq_nivel_taxonomico existe na tabela de nivel taxonomico.

	**/
	if p_sq_nivel_taxonomico is not null then

	SELECT count(*) into v_existe
	FROM taxonomia.nivel_taxonomico
	WHERE nivel_taxonomico.sq_nivel_taxonomico = p_sq_nivel_taxonomico;

	if v_existe = 0 then
	    RAISE exception 'Valor informado para o parametro p_sq_nivel_taxonomico não existe na tabela de nível taxonômico.';
	end if ;

	end if;

    if v_qtd_paramentro = 3 then
        subespecie:=lower(v_array[3]);
        especie:=lower(v_array[2]);
        genero:=initcap(v_array[1]);
        v_co_nivel_taxonomico:='subespecie';
        v_where:=' taxon.json_trilha->'''||v_co_nivel_taxonomico||'''->>''no_taxon''='''||genero||' '||especie||' '||subespecie||'''';
        RAISE INFO 'v_qtd_paramentro % (%) %', v_qtd_paramentro,v_co_nivel_taxonomico, E'\n';
        v_sq_nivel_taxonomico:=8;
    end if;

    if v_qtd_paramentro = 2 then
        especie:=lower(v_array[2]);
        genero:=initcap(v_array[1]);
        v_co_nivel_taxonomico:='especie';
        v_where:=' taxon.json_trilha->'''||v_co_nivel_taxonomico||'''->>''no_taxon''='''||genero||' '||especie||'''';
        RAISE INFO 'v_qtd_paramentro % (%)  %', v_qtd_paramentro,v_co_nivel_taxonomico,E'\n';
        v_sq_nivel_taxonomico:=7;
    end if;

   if v_qtd_paramentro = 1 and  p_sq_nivel_taxonomico = 8 THEN
        v_co_nivel_taxonomico:='subespecie';
        RAISE INFO 'v_qtd_paramentro % (%) p_sq_nivel_taxonomico %  %', v_qtd_paramentro,v_co_nivel_taxonomico, p_sq_nivel_taxonomico, E'\n';
        v_where:=' taxon.json_trilha->'''||v_co_nivel_taxonomico||'''->>''no_taxon'' ilike '''||initcap(trim(v_no_taxon))||'%''';
        v_sq_nivel_taxonomico:=8;

    end if;

   if v_qtd_paramentro = 1 and  p_sq_nivel_taxonomico = 7 THEN
        v_co_nivel_taxonomico:='especie';
        RAISE INFO 'v_qtd_paramentro % (%) p_sq_nivel_taxonomico %  %', v_qtd_paramentro,v_co_nivel_taxonomico, p_sq_nivel_taxonomico, E'\n';
        v_where:=' taxon.json_trilha->'''||v_co_nivel_taxonomico||'''->>''no_taxon'' ilike '''||initcap(trim(v_no_taxon))||'%''';
        v_sq_nivel_taxonomico:=7;

    end if;

    if v_qtd_paramentro = 1 and  p_sq_nivel_taxonomico = 6 THEN
        v_co_nivel_taxonomico:='genero';
        RAISE INFO 'v_qtd_paramentro % (%) p_sq_nivel_taxonomico %  %', v_qtd_paramentro,v_co_nivel_taxonomico, p_sq_nivel_taxonomico, E'\n';
        v_where:=' taxon.json_trilha->'''||v_co_nivel_taxonomico||'''->>''no_taxon''='''||initcap(trim(v_no_taxon))||'''';
        v_sq_nivel_taxonomico:=6;

    end if;

    if v_qtd_paramentro = 1 and p_sq_nivel_taxonomico = 10 THEN
        v_co_nivel_taxonomico:='tribo';
        RAISE INFO 'v_qtd_paramentro % (%) p_sq_nivel_taxonomico %  %', v_qtd_paramentro,v_co_nivel_taxonomico, p_sq_nivel_taxonomico, E'\n';

        v_where:=' taxon.json_trilha->'''||v_co_nivel_taxonomico||'''->>''no_taxon''='''||initcap(trim(v_no_taxon))||'''';
        v_sq_nivel_taxonomico:=10;
	end if;
	if v_qtd_paramentro = 1 and p_sq_nivel_taxonomico = 9 THEN
        v_co_nivel_taxonomico:='subfamilia';
        RAISE INFO 'v_qtd_paramentro % (%) p_sq_nivel_taxonomico %  %', v_qtd_paramentro,v_co_nivel_taxonomico, p_sq_nivel_taxonomico, E'\n';

        v_where:=' taxon.json_trilha->'''||v_co_nivel_taxonomico||'''->>''no_taxon''='''||initcap(trim(v_no_taxon))||'''';
        v_sq_nivel_taxonomico:=9;
	end if;

    if v_qtd_paramentro = 1 and p_sq_nivel_taxonomico = 5 THEN
        v_co_nivel_taxonomico:='familia';
        RAISE INFO 'v_qtd_paramentro % (%) p_sq_nivel_taxonomico %  %', v_qtd_paramentro,v_co_nivel_taxonomico, p_sq_nivel_taxonomico, E'\n';

        v_where:=' taxon.json_trilha->'''||v_co_nivel_taxonomico||'''->>''no_taxon''='''||initcap(trim(v_no_taxon))||'''';
        v_sq_nivel_taxonomico:=5;
	end if;

    if v_qtd_paramentro = 1 and p_sq_nivel_taxonomico = 4 THEN

        v_co_nivel_taxonomico:='ordem';
        RAISE INFO 'v_qtd_paramentro % (%) p_sq_nivel_taxonomico %  %', v_qtd_paramentro,v_co_nivel_taxonomico, p_sq_nivel_taxonomico, E'\n';
        v_where:=' taxon.json_trilha->'''||v_co_nivel_taxonomico||'''->>''no_taxon''='''||initcap(trim(v_no_taxon))||'''';

        v_sq_nivel_taxonomico:=4;
    end if;

    if v_qtd_paramentro = 1 and p_sq_nivel_taxonomico = 3 THEN

        v_co_nivel_taxonomico:='classe';
        RAISE INFO 'v_qtd_paramentro % (%) p_sq_nivel_taxonomico %  %', v_qtd_paramentro,v_co_nivel_taxonomico, p_sq_nivel_taxonomico, E'\n';
        v_where:=' taxon.json_trilha->'''||v_co_nivel_taxonomico||'''->>''no_taxon''='''||initcap(trim(v_no_taxon))||'''';

        v_sq_nivel_taxonomico:=3;

    end if;

    if v_qtd_paramentro = 1 and p_sq_nivel_taxonomico = 11 THEN

        v_co_nivel_taxonomico:='subfilo';
        RAISE INFO 'v_qtd_paramentro % (%) p_sq_nivel_taxonomico %  %', v_qtd_paramentro,v_co_nivel_taxonomico, p_sq_nivel_taxonomico, E'\n';
        v_where:=' taxon.json_trilha->'''||v_co_nivel_taxonomico||'''->>''no_taxon''='''||initcap(trim(v_no_taxon))||'''';
        v_sq_nivel_taxonomico:=11;
    end if;

	if v_qtd_paramentro = 1 and p_sq_nivel_taxonomico = 2 THEN

        v_co_nivel_taxonomico:='filo';
        RAISE INFO 'v_qtd_paramentro % (%) p_sq_nivel_taxonomico %  %', v_qtd_paramentro,v_co_nivel_taxonomico, p_sq_nivel_taxonomico, E'\n';
        v_where:=' taxon.json_trilha->'''||v_co_nivel_taxonomico||'''->>''no_taxon''='''||initcap(trim(v_no_taxon))||'''';
        v_sq_nivel_taxonomico:=2;
    end if;

    if v_qtd_paramentro = 1 and  p_sq_nivel_taxonomico = 1  THEN

        v_co_nivel_taxonomico:='reino';
        RAISE INFO 'v_qtd_paramentro % (%) p_sq_nivel_taxonomico %  %', v_qtd_paramentro,v_co_nivel_taxonomico, p_sq_nivel_taxonomico, E'\n';
        v_where:=' taxon.json_trilha->'''||v_co_nivel_taxonomico||'''->>''no_taxon''='''||initcap(trim(v_no_taxon))||'''';
        v_sq_nivel_taxonomico:=1;

    end if;

    if v_qtd_paramentro = 1 and  p_sq_nivel_taxonomico is null  THEN
        v_co_nivel_taxonomico:='';
        RAISE INFO 'v_qtd_paramentro % (%) p_sq_nivel_taxonomico %  %', v_qtd_paramentro,v_co_nivel_taxonomico, p_sq_nivel_taxonomico, E'\n';

		select  LENGTH(v_no_taxon) into v_count_letras;
		if v_count_letras < 3 then

			v_where := 'false' ;

		else
        v_where:=' taxon.json_trilha->''genero''->>''no_taxon''='''||initcap(trim(v_no_taxon))||'''
 OR
taxon.json_trilha->''tribo''->>''no_taxon''='''||initcap(trim(v_no_taxon))||'''
 OR
taxon.json_trilha->''subfamilia''->>''no_taxon''='''||initcap(trim(v_no_taxon))||'''
 OR
taxon.json_trilha->''familia''->>''no_taxon''='''||initcap(trim(v_no_taxon))||'''
 OR
taxon.json_trilha->''ordem''->>''no_taxon''='''||initcap(trim(v_no_taxon))||'''
 OR
taxon.json_trilha->''classe''->>''no_taxon''='''||initcap(trim(v_no_taxon))||'''
 OR
taxon.json_trilha->''subfilo''->>''no_taxon''='''||initcap(trim(v_no_taxon))||'''
 OR
taxon.json_trilha->''filo''->>''no_taxon''='''||initcap(trim(v_no_taxon))||'''
 OR
taxon.json_trilha->''reino''->>''no_taxon''='''||initcap(trim(v_no_taxon))||'''
';
end if;

        v_sql:= 'select
 (ROW_NUMBER()
   OVER(
     PARTITION BY nivel_taxonomico.co_nivel_taxonomico
     ORDER BY  nivel_taxonomico.nu_grau_taxonomico ,
            (coalesce(
		(taxon.json_trilha->''genero''->>''no_taxon'')::text,
		(taxon.json_trilha->''tribo''->>''no_taxon'')::text,
		(taxon.json_trilha->''subfamilia''->>''no_taxon'')::text,
		(taxon.json_trilha->''familia''->>''no_taxon'')::text,
		(taxon.json_trilha->''ordem''->>''no_taxon'')::text,
		(taxon.json_trilha->''classe''->>''no_taxon'')::text,
		(taxon.json_trilha->''subfilo''->>''no_taxon'')::text,
		(taxon.json_trilha->''filo''->>''no_taxon'')::text,
		(taxon.json_trilha->''reino''->>''no_taxon'')::text
	    )
      )::text))::bigint 							  as r_sequencia_nivel,
     (COUNT(nivel_taxonomico.co_nivel_taxonomico) OVER (PARTITION BY nivel_taxonomico.co_nivel_taxonomico) )::bigint as  r_qtd_nivel,
	taxonomia.formatar_trilha_taxon(taxon.json_trilha,false,false)::text      as r_formatacao_trilha,
	(taxon.json_trilha->'''||v_co_nivel_taxonomico||'''->>''no_taxon'')::text as r_formatacao_nome,
	taxonomia.formatar_trilha_taxon(taxon.json_trilha,true,false)::text       as r_formatacao_trilha_com_nivel,
	taxon.sq_taxon_pai::bigint                                                as r_sq_taxon_pai ,
	null::bigint                                                              as r_nivel ,
	taxon.sq_taxon::bigint                                                    as r_sq_taxon ,
	null::bigint                                                              as r_sq_taxon_search ,
	taxon.no_taxon                                                            as r_no_taxon ,
	taxon.in_ocorre_brasil                                                    as r_in_ocorre_brasil ,
	taxon.sq_nivel_taxonomico::bigint                                         as r_sq_nivel_taxonomico ,
	taxon.no_autor                                                            as r_no_autor ,
        taxon.nu_ano                                                              as r_nu_ano ,
	taxon.sq_situacao_nome                                                    as r_sq_situacao_nome ,
	taxon.st_ativo                                                            as r_st_ativo ,
	nivel_taxonomico.co_nivel_taxonomico                                      as r_co_nivel_taxonomico ,
	nivel_taxonomico.de_nivel_taxonomico                                      as r_de_nivel_taxonomico ,
	nivel_taxonomico.in_nivel_intermediario                                   as r_in_nivel_intermediario ,
	nivel_taxonomico.nu_grau_taxonomico                                       as r_nu_grau_taxonomico,
	'||v_qtd_paramentro||'::integer                                           as qtd_paramentro,
	''{"'||array_to_string(v_array,'","')||'"}''::text[]                      as v_array
	FROM taxonomia.taxon
	INNER JOIN taxonomia.nivel_taxonomico
	       ON nivel_taxonomico.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
	WHERE '||v_where ||'
	ORDER BY nivel_taxonomico.nu_grau_taxonomico DESC , taxonomia.formatar_trilha_taxon(taxon.json_trilha,false,false)
';

    else

		select  LENGTH(v_no_taxon) into v_count_letras;

		if v_count_letras < 2 then
		    RAISE INFO 'Pesquisa não pode ser realizada com menos de % caracteres.  %',v_count_letras::text, E'\n';
			v_where := 'false' ;
		end if;

        RAISE INFO 'v_qtd_paramentro % (%) p_sq_nivel_taxonomico %  %', v_qtd_paramentro,v_co_nivel_taxonomico, p_sq_nivel_taxonomico, E'\n';
        v_sql:= 'select
 (ROW_NUMBER() OVER(
    PARTITION BY nivel_taxonomico.co_nivel_taxonomico
    ORDER BY  nivel_taxonomico.nu_grau_taxonomico , (taxon.json_trilha->'''||v_co_nivel_taxonomico||'''->>''no_taxon'')::text))::bigint as r_sequencia_nivel,
     (COUNT(nivel_taxonomico.co_nivel_taxonomico) OVER (PARTITION BY nivel_taxonomico.co_nivel_taxonomico) )::bigint as  r_qtd_nivel,
	taxonomia.formatar_trilha_taxon(taxon.json_trilha,false,false)::text      as r_formatacao_trilha,
	(taxon.json_trilha->'''||v_co_nivel_taxonomico||'''->>''no_taxon'')::text as r_formatacao_nome,
	taxonomia.formatar_trilha_taxon(taxon.json_trilha,true,false)::text       as r_formatacao_trilha_com_nivel,
	taxon.sq_taxon_pai::bigint                                                as r_sq_taxon_pai ,
	null::bigint                                                              as r_nivel ,
	taxon.sq_taxon::bigint                                                    as r_sq_taxon ,
	null::bigint                                                              as r_sq_taxon_search ,
	taxon.no_taxon                                                            as r_no_taxon ,
	taxon.in_ocorre_brasil                                                    as r_in_ocorre_brasil ,
	taxon.sq_nivel_taxonomico::bigint                                         as r_sq_nivel_taxonomico ,
	taxon.no_autor                                                            as r_no_autor ,
        taxon.nu_ano                                                              as r_nu_ano ,
	taxon.sq_situacao_nome                                                    as r_sq_situacao_nome ,
	taxon.st_ativo                                                            as r_st_ativo ,
	nivel_taxonomico.co_nivel_taxonomico                                      as r_co_nivel_taxonomico ,
	nivel_taxonomico.de_nivel_taxonomico                                      as r_de_nivel_taxonomico ,
	nivel_taxonomico.in_nivel_intermediario                                   as r_in_nivel_intermediario ,
	nivel_taxonomico.nu_grau_taxonomico                                       as r_nu_grau_taxonomico,
	'||v_qtd_paramentro||'::integer                                                             as qtd_paramentro,
	''{"'||array_to_string(v_array,'","')||'"}''::text[]                                                              as v_array
	FROM taxonomia.taxon
	INNER JOIN taxonomia.nivel_taxonomico
	       ON nivel_taxonomico.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
	WHERE '||v_where ||'
	  AND taxon.sq_nivel_taxonomico='||v_sq_nivel_taxonomico||'
		ORDER BY nivel_taxonomico.nu_grau_taxonomico , taxon.json_trilha->'''||v_co_nivel_taxonomico||'''->>''no_taxon''
';

    end if;

	IF v_sql IS null then

	v_where := 'false' ;

    RAISE INFO ' sql nulo ';
        v_sql:= 'select
    null::bigint as r_sequencia_nivel,
    null::bigint as  r_qtd_nivel,
	taxonomia.formatar_trilha_taxon(taxon.json_trilha,false,false)::text      as r_formatacao_trilha,
	null::text as r_formatacao_nome,
	taxonomia.formatar_trilha_taxon(taxon.json_trilha,true,false)::text       as r_formatacao_trilha_com_nivel,
	taxon.sq_taxon_pai::bigint                                                as r_sq_taxon_pai ,
	null::bigint                                                              as r_nivel ,
	taxon.sq_taxon::bigint                                                    as r_sq_taxon ,
	null::bigint                                                              as r_sq_taxon_search ,
	taxon.no_taxon                                                            as r_no_taxon ,
	taxon.in_ocorre_brasil                                                    as r_in_ocorre_brasil ,
	taxon.sq_nivel_taxonomico::bigint                                         as r_sq_nivel_taxonomico ,
	taxon.no_autor                                                            as r_no_autor ,
        taxon.nu_ano                                                              as r_nu_ano ,
	taxon.sq_situacao_nome                                                    as r_sq_situacao_nome ,
	taxon.st_ativo                                                            as r_st_ativo ,
	nivel_taxonomico.co_nivel_taxonomico                                      as r_co_nivel_taxonomico ,
	nivel_taxonomico.de_nivel_taxonomico                                      as r_de_nivel_taxonomico ,
	nivel_taxonomico.in_nivel_intermediario                                   as r_in_nivel_intermediario ,
	nivel_taxonomico.nu_grau_taxonomico                                       as r_nu_grau_taxonomico,
	0::integer                                                             as qtd_paramentro,
	''{"'||array_to_string(v_array,'","')||'"}''::text[]                                                              as v_array
	FROM taxonomia.taxon
	INNER JOIN taxonomia.nivel_taxonomico
	       ON nivel_taxonomico.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico
	WHERE false LIMIT 1
';

	end if;

    RAISE INFO 'v_sql % %', v_sql, E'\n';

    RETURN QUERY EXECUTE v_sql;

END;
$$;


--
-- TOC entry 4665 (class 1255 OID 23841)
-- Name: sql_table_taxon_dupl_sq_diferente(text); Type: FUNCTION; Schema: taxonomia; Owner: -
--

CREATE FUNCTION taxonomia.sql_table_taxon_dupl_sq_diferente(p_param_where text) RETURNS TABLE(nivel_pai_trilha_formatada text, permanece_sq_taxon_pai bigint, substitui_sq_taxon_pai bigint[], total_sq_taxon_pai_nivel integer, sq_taxon_pai_agg bigint[], co_nivel_taxonomico text, nu_grau_taxonomico integer, no_taxon text, total_sq_taxon_nivel integer, sq_taxon_agg text[], permanece_sq_taxon text, substitui_sq_taxon text[], nivel_trilha_formatada text[], nivel_total_trilha_formatada integer, alvo_sq_taxon integer[])
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_sql text;
BEGIN

RETURN QUERY EXECUTE
    (SELECT format('with sel as (
select
taxonomia.formatar_trilha_taxon(taxon.json_trilha,true,true) as trilha_taxon_formatada_com_sq,

    taxon.json_trilha->''familia''->>''no_taxon'' as taxon_avo,  taxon.json_trilha->''ordem''->>''no_taxon'' as taxon_pai, taxon.json_trilha->''classe''->>''no_taxon'' as taxon_filho
,  taxon.*
	   from taxonomia.taxon
	where %s

/**
	 and taxon.sq_nivel_taxonomico = (
							SELECT sq_nivel_taxonomico FROM taxonomia.nivel_taxonomico
							  where co_nivel_taxonomico in (''ESPECIE'')
							  )

**/
							  --2301814
							  --"( Espécie )  Rhinodrilus fafner < Rhinodrilus < Rhinodrilidae < Haplotaxida < Clitellata < Annelida < Animalia"
), sel_nivel_dup as (
select

array_length(array_agg(distinct nivel_taxon.nivel_sq_taxon_pai),1) as total_sq_taxon_pai_nivel,
array_agg(distinct nivel_taxon.nivel_sq_taxon_pai) as sq_taxon_pai_agg,
lower(nivel_taxonomico.co_nivel_taxonomico) as co_nivel_taxonomico,
nivel_taxonomico.nu_grau_taxonomico as nu_grau_taxonomico,
taxon.value->>''no_taxon'' as no_taxon,
array_length(array_agg(distinct taxon.value->>''sq_taxon''),1) as total_sq_taxon_nivel,
array_agg(distinct taxon.value->>''sq_taxon'') as sq_taxon_agg,
array_agg(distinct nivel_trilha_formatada) as nivel_trilha_formatada,
array_length(array_agg(distinct nivel_trilha_formatada),1) as nivel_total_trilha_formatada,
array_agg(distinct sel.sq_taxon) as alvo_sq_taxon
  from sel
  inner join json_each(sel.json_trilha) as taxon on true
   inner join  lateral (select taxonomia.formatar_trilha_taxon(t.json_trilha,true,false) as nivel_trilha_formatada, t.sq_taxon_pai as nivel_sq_taxon_pai from taxonomia.taxon as t where t.sq_taxon = (taxon.value->>''sq_taxon'')::integer ) as nivel_taxon on true
   INNER JOIN taxonomia.nivel_taxonomico on nivel_taxonomico.sq_nivel_taxonomico = (taxon.value->>''sq_nivel_taxonomico'')::integer
group by nivel_taxonomico.nu_grau_taxonomico,nivel_taxonomico.co_nivel_taxonomico,taxon.value->>''no_taxon''
order by nivel_taxonomico.nu_grau_taxonomico
)select
nivel_pai_trilha_formatada
,coalesce(
(array_remove(sel_nivel_dup.sq_taxon_pai_agg,sel_nivel_dup.sq_taxon_pai_agg[1]))[1]
,sel_nivel_dup.sq_taxon_pai_agg[1]) as permanece_sq_taxon_pai
,array_remove(sel_nivel_dup.sq_taxon_pai_agg,(array_remove(sel_nivel_dup.sq_taxon_pai_agg,sel_nivel_dup.sq_taxon_pai_agg[1]))[1]) AS substitui_sq_taxon_pai
,total_sq_taxon_pai_nivel,
sq_taxon_pai_agg,
co_nivel_taxonomico,
nu_grau_taxonomico,
no_taxon,
total_sq_taxon_nivel,
sq_taxon_agg
,coalesce(
(array_remove(sel_nivel_dup.sq_taxon_agg,sel_nivel_dup.sq_taxon_agg[1]))[1]
,sel_nivel_dup.sq_taxon_agg[1]) as permanece_sq_taxon
,array_remove(sel_nivel_dup.sq_taxon_agg,(array_remove(sel_nivel_dup.sq_taxon_agg,sel_nivel_dup.sq_taxon_agg[1]))[1]) AS substitui_sq_taxon
,nivel_trilha_formatada,
nivel_total_trilha_formatada
,alvo_sq_taxon
from sel_nivel_dup
 left join  lateral (
       select taxonomia.formatar_trilha_taxon(t.json_trilha,true,false) as nivel_pai_trilha_formatada, t.sq_taxon_pai as nivel_sq_taxon_avo
	 from taxonomia.taxon as t
	 where t.sq_taxon = sel_nivel_dup.sq_taxon_pai_agg[1]
	   AND  total_sq_taxon_pai_nivel = 1
	 ) as nivel_pai on true

where nivel_total_trilha_formatada = 1 and total_sq_taxon_nivel > 1'
                , p_param_where
        )
     );
END;

$$;


--
-- TOC entry 4666 (class 1255 OID 23842)
-- Name: sql_upd_fk_sq_taxon_sistemas(text); Type: FUNCTION; Schema: taxonomia; Owner: -
--

CREATE FUNCTION taxonomia.sql_upd_fk_sq_taxon_sistemas(p_no_tmp_table text) RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_sql text;
BEGIN
--     UPDATE taxonomia.taxon
--     set sq_taxon_pai = bkp.sq_taxon_pai
--       , no_autor = bkp.no_autor
--       , nu_ano = bkp.nu_ano
--       , in_ocorre_brasil = bkp.in_ocorre_brasil
--       , de_autor_ano = bkp.de_autor_ano
--       , json_trilha = bkp.json_trilha
--       , ts_ultima_alteracao = bkp.ts_ultima_alteracao
--     FROM taxonomia.carga202008191202_bkp_taxon as bkp
--     WHERE bkp.sq_taxon = taxon.sq_taxon;
--drop table if exists taxonomia.teste_dinam_trilha_dupli_sq_diferente;
--create table taxonomia.teste_dinam_trilha_dupli_sq_diferente as (
  --  select * from taxonomia.sql_table_taxon_dupl_sq_diferente('taxon.json_trilha->''reino''->>''no_taxon'' = ''Animalia'' AND taxon.json_trilha->''classe''->>''no_taxon'' = ''Clitellata''')
--)
--;

-- v_sql:='CREATE UNLOGGED TABLE IF NOT EXISTS  taxonomia.'||p_no_tmp_table||'_trilha_dupli_sq_diferente as (
-- select * from taxonomia.sql_table_taxon_dupl_sq_diferente(''taxon.json_trilha->''''reino''->>''''no_taxon'''' = ''''Animalia'''' AND taxon.json_trilha->''''classe''->>''''no_taxon'''' = ''''Clitellata'''')
-- )
-- ;';

--RAISE INFO 'v_sql  : % [ % ] %', E'\n',  v_sql, E'\n';
--execute v_sql;

--UNION ALL
 
-- select 'CREATE UNLOGGED TABLE IF NOT EXISTS taxonomia._bkp_upd_sq_taxon_taxon as (
--select * from taxonomia.taxon where sq_taxon in (select distinct  unnest(sq_taxon_duplicados) from sel)
--);

v_sql:='CREATE UNLOGGED TABLE IF NOT EXISTS taxonomia.'||p_no_tmp_table||'_fk_sq_taxon_sistemas AS (
SELECT nivel_pai_trilha_formatada
, permanece_sq_taxon_pai
, substitui_sq_taxon_pai
, total_sq_taxon_pai_nivel
, sq_taxon_pai_agg
, co_nivel_taxonomico
, nu_grau_taxonomico
, no_taxon
, total_sq_taxon_nivel
, sq_taxon_agg
, permanece_sq_taxon
, substitui_sq_taxon
, nivel_trilha_formatada
, nivel_total_trilha_formatada
, alvo_sq_taxon
, esquema_nome_tabela_pk 
, nome_coluna_fk 
, valor_coluna_fk 
, nome_tabela_pk
, nome_coluna_pk
, valor_pk
  FROM taxonomia.'||p_no_tmp_table||'_trilha_dupli_sq_diferente as sel
  left join lateral (select * from taxonomia.vw_discovery_fk_taxon Where valor_coluna_fk = any(sel.substitui_sq_taxon::integer[])) as tb1  on true
group by nivel_pai_trilha_formatada
, permanece_sq_taxon_pai
, substitui_sq_taxon_pai
, total_sq_taxon_pai_nivel
, sq_taxon_pai_agg
, co_nivel_taxonomico
, nu_grau_taxonomico
, no_taxon
, total_sq_taxon_nivel
, sq_taxon_agg
, permanece_sq_taxon
, substitui_sq_taxon
, nivel_trilha_formatada
, nivel_total_trilha_formatada
, alvo_sq_taxon 
, esquema_nome_tabela_pk 
, nome_coluna_fk 
, valor_coluna_fk 
, nome_tabela_pk
, nome_coluna_pk
, valor_pk
)';

RAISE INFO 'v_sql  : % [ % ] %', E'\n',  v_sql, E'\n';
execute v_sql;

-- v_sql:='with sel as (
-- select
--
-- --string_agg('', '',
-- concat(''upd_'',nome_tabela_pk,''_'',permanece_sq_taxon,''_'',valor_coluna_fk  ,'' AS ( UPDATE '',esquema_nome_tabela_pk,''.'',nome_tabela_pk, '' SET '', nome_coluna_fk, '' = '',permanece_sq_taxon, '' WHERE '', nome_coluna_fk, '' = '',valor_coluna_fk, '' AND '', nome_coluna_pk , '' = any('''''', array_agg(valor_pk) ,'''''') RETURNING '''''',esquema_nome_tabela_pk , '''''' as pk ) '')
-- --)
-- as sql_upd
-- ,
-- --string_agg('' UNION ALL '' ,
-- concat(''SELECT '''' Atualizado a FK em '',esquema_nome_tabela_pk,''.'',nome_tabela_pk ,''.'',nome_coluna_fk,''  de '',substitui_sq_taxon,'' para '',permanece_sq_taxon,'' com PK '',nome_coluna_pk,'' com os valores '', array_agg(valor_pk),'' as msg '''' FROM upd_'',nome_tabela_pk,''_'',permanece_sq_taxon,''_'',valor_coluna_fk)
-- --)
-- as sql_select
-- ,
-- esquema_nome_tabela_pk
-- , nome_coluna_fk
-- , valor_coluna_fk
-- , nome_tabela_pk
-- , nome_coluna_pk
-- , array_agg(valor_pk) as valor_pk_agg
-- , permanece_sq_taxon
-- , substitui_sq_taxon
-- from taxonomia.'||p_no_tmp_table||'_fk_sq_taxon_sistemas
-- where esquema_nome_tabela_pk is not null
-- group by esquema_nome_tabela_pk
-- , nome_coluna_fk
-- , valor_coluna_fk
-- , nome_tabela_pk
-- , nome_coluna_pk
-- , permanece_sq_taxon
-- , substitui_sq_taxon
--
-- ), sel_sql as (
--
-- select ''CREATE UNLOGGED TABLE IF NOT EXISTS taxonomia._upd_sq_taxon_log as ( WITH '' as sql
-- UNION ALL
-- select array_to_string(array_agg(sql_upd),'', '') as sql
--    from sel
--  UNION ALL
--  select array_to_string(array_agg(sql_select),'' UNION ALL '') as sql
--    from sel
--  UNION ALL
--  select '');''
--  UNION ALL
--  select ''CREATE UNLOGGED TABLE IF NOT EXISTS taxonomia._bkp_upd_sq_taxon_taxon as (
-- select * from taxonomia.taxon where sq_taxon in (select distinct  unnest(sq_taxon_duplicados) from sel)
-- );
-- ''
-- )
-- select array_to_string(
-- array_agg(sql),'' '')  from sel_sql' ;
v_sql:=' with sel as (
select
concat(''upd_'',nome_tabela_pk,''_'',permanece_sq_taxon,''_'',valor_coluna_fk  ,'' AS ( UPDATE '',esquema_nome_tabela_pk,''.'',nome_tabela_pk, '' SET '', nome_coluna_fk, '' = '',permanece_sq_taxon, '' WHERE '', nome_coluna_fk, '' = '',valor_coluna_fk, '' AND '', nome_coluna_pk , '' in ('', array_to_string(array_agg(valor_pk),'','')  ,'') RETURNING '''''',esquema_nome_tabela_pk , '''''' as pk ) '')
as sql_upd
,
concat(''upd_taxon_sq_pai_'',permanece_sq_taxon_pai,''_'',array_to_string(substitui_sq_taxon_pai,''_'')  ,'' AS ( UPDATE taxonomia.taxon SET sq_taxon_pai = '',permanece_sq_taxon_pai,'' WHERE sq_taxon_pai in ('', array_to_string(substitui_sq_taxon_pai,'','')  ,'') RETURNING ''''taxon'''' as pk ) '')
--)
as sql_upd_sq_taxon_pai
,
--string_agg('' UNION ALL '' ,
concat(''SELECT ''''Atualizado a FK em '',esquema_nome_tabela_pk,''.'',nome_tabela_pk ,''.'',nome_coluna_fk,''  de '',array_to_string(substitui_sq_taxon,''_'')  ,'' para '',permanece_sq_taxon,'' com PK '',nome_coluna_pk,'' com os valores '', array_to_string(array_agg(valor_pk),'','') ,'' as msg '''' FROM upd_'',nome_tabela_pk,''_'',permanece_sq_taxon,''_'',valor_coluna_fk)
--)
as sql_select
,
--string_agg('' UNION ALL '' ,
concat(''SELECT ''''Movidos os táxon com o sq_pai de '',array_to_string(substitui_sq_taxon_pai,''_''),'' para '',permanece_sq_taxon_pai,''.  as msg ''''  FROM upd_taxon_sq_pai_'',permanece_sq_taxon_pai,''_'',array_to_string(substitui_sq_taxon_pai,'',''))
--)
as sql_select_upd_sq_pai
,
concat(''ins_h_merge_sq_pai'',permanece_sq_taxon_pai,''_'',array_to_string(substitui_sq_taxon_pai,''_'')  ,'' AS ( INSERT INTO taxonomia.taxon_hist(sq_taxon, sq_pessoa, sq_situacao, sq_taxon_pai,de_historico, dt_historico) VALUES ('',permanece_sq_taxon_pai,'', 2533, 7, null,''''Mergiado via planilha os táxons  sq_taxon_pai de ('',array_to_string(substitui_sq_taxon_pai,'',''),'')''
,'' para '',permanece_sq_taxon_pai,''.'''',now())  RETURNING ''''taxon'''' as pk ) '')
as sql_ins_h_merge_sq_pai
,
--string_agg('' UNION ALL '' ,
concat(''SELECT ''''Registrado no histórico do táxon o merge com o sq_pai de ('',array_to_string(substitui_sq_taxon_pai,''_''),'') com o sq_pai '',permanece_sq_taxon_pai,''.  as msg ''''  FROM ins_h_merge_sq_pai'',permanece_sq_taxon_pai,''_'',array_to_string(substitui_sq_taxon_pai,''_'') )
as sql_select_ins_h_merge_sq_pai
, esquema_nome_tabela_pk
, permanece_sq_taxon_pai
, substitui_sq_taxon_pai
, nome_coluna_fk
, valor_coluna_fk
, nome_tabela_pk
, nome_coluna_pk
, array_agg(valor_pk) as valor_pk_agg
, permanece_sq_taxon
, substitui_sq_taxon
from taxonomia.'||p_no_tmp_table||'_fk_sq_taxon_sistemas
where esquema_nome_tabela_pk is not null
group by esquema_nome_tabela_pk
, nome_coluna_fk
, valor_coluna_fk
, nome_tabela_pk
, nome_coluna_pk
, permanece_sq_taxon
, substitui_sq_taxon
,permanece_sq_taxon_pai
,substitui_sq_taxon_pai
)
,sel_sql_upd_ins as (
select array_to_string(array_agg(distinct sql_upd),'', '') as sql from sel
UNION ALL
select array_to_string(array_agg(distinct sql_upd_sq_taxon_pai),'', '') from sel
UNION ALL
select array_to_string(array_agg(distinct sql_ins_h_merge_sq_pai),'', '') from sel
),sel_sql_sel as (
select array_to_string(array_agg(distinct sql_select),'' UNION ALL '') as sql  from sel
UNION ALL
select array_to_string(array_agg(distinct sql_select_upd_sq_pai),'' UNION ALL '') as sql  from sel
UNION ALL
select array_to_string(array_agg(distinct sql_select_ins_h_merge_sq_pai),'' UNION ALL '') as sql  from sel

)
select concat_ws('' ''
,
 (select concat(''CREATE UNLOGGED TABLE IF NOT EXISTS taxonomia._upd_sq_taxon_log as ( WITH '', array_to_string(array_agg(sql),'', ''))  from sel_sql_upd_ins)
,
 (select array_to_string(array_agg(sql),'' UNION ALL '')  from sel_sql_sel)
,
'');
''
)
';
RAISE INFO 'v_sql  : % [ % ] %', E'\n',  v_sql, E'\n';
execute v_sql into v_sql;


RAISE INFO 'v_sql  : % [ % ] %', E'\n',  v_sql, E'\n';
execute v_sql;


END;
$$;


SET default_tablespace = '';

--
-- TOC entry 1432 (class 1259 OID 28473)
-- Name: nivel_taxonomico; Type: TABLE; Schema: taxonomia; Owner: -
--

CREATE TABLE taxonomia.nivel_taxonomico (
    sq_nivel_taxonomico integer NOT NULL,
    de_nivel_taxonomico text NOT NULL,
    nu_grau_taxonomico integer NOT NULL,
    in_nivel_intermediario boolean DEFAULT false NOT NULL,
    co_nivel_taxonomico text NOT NULL,
    CONSTRAINT chk_nivel_taxonomico_nivel CHECK ((in_nivel_intermediario = ANY (ARRAY[true, false])))
);


--
-- TOC entry 13245 (class 0 OID 0)
-- Dependencies: 1432
-- Name: COLUMN nivel_taxonomico.nu_grau_taxonomico; Type: COMMENT; Schema: taxonomia; Owner: -
--

COMMENT ON COLUMN taxonomia.nivel_taxonomico.nu_grau_taxonomico IS 'Squencia numérica que identifica a ordem hierárquica entre os níveis taxonômicos 10,20,30,40...';


--
-- TOC entry 13246 (class 0 OID 0)
-- Dependencies: 1432
-- Name: COLUMN nivel_taxonomico.in_nivel_intermediario; Type: COMMENT; Schema: taxonomia; Owner: -
--

COMMENT ON COLUMN taxonomia.nivel_taxonomico.in_nivel_intermediario IS 'Identifica se o nivel taxonômico é um nivel fora da sequência padrão REFICOFAGE';


--
-- TOC entry 13247 (class 0 OID 0)
-- Dependencies: 1432
-- Name: COLUMN nivel_taxonomico.co_nivel_taxonomico; Type: COMMENT; Schema: taxonomia; Owner: -
--

COMMENT ON COLUMN taxonomia.nivel_taxonomico.co_nivel_taxonomico IS 'Código do nível taxonômico sem acento e em caixa alta para ser utilizado como constantes nas consultas no lugar de do sq_nivel e nu_ordem_taxonomica';


--
-- TOC entry 1433 (class 1259 OID 28481)
-- Name: taxon; Type: TABLE; Schema: taxonomia; Owner: -
--

CREATE TABLE taxonomia.taxon (
    sq_taxon integer NOT NULL,
    sq_taxon_pai bigint,
    sq_nivel_taxonomico bigint NOT NULL,
    sq_situacao_nome bigint NOT NULL,
    no_taxon text NOT NULL,
    st_ativo boolean NOT NULL,
    no_autor text,
    nu_ano integer,
    in_ocorre_brasil boolean DEFAULT false NOT NULL,
    de_autor_ano text,
    json_trilha json,
    ts_ultima_alteracao timestamp without time zone DEFAULT now() NOT NULL,
    CONSTRAINT chk_taxon_ocorre_brasil CHECK ((in_ocorre_brasil = ANY (ARRAY[true, false])))
);


--
-- TOC entry 13248 (class 0 OID 0)
-- Dependencies: 1433
-- Name: COLUMN taxon.json_trilha; Type: COMMENT; Schema: taxonomia; Owner: -
--

COMMENT ON COLUMN taxonomia.taxon.json_trilha IS 'Coluna para registrar a estrutura taxonomica no formato json para otimização em consultas e exibição da hierarquia taxonômica';


--
-- TOC entry 13249 (class 0 OID 0)
-- Dependencies: 1433
-- Name: COLUMN taxon.ts_ultima_alteracao; Type: COMMENT; Schema: taxonomia; Owner: -
--

COMMENT ON COLUMN taxonomia.taxon.ts_ultima_alteracao IS 'Adiciona a data alteracao do registro.';


--
-- TOC entry 1729 (class 1259 OID 29634)
-- Name: publicacao; Type: TABLE; Schema: taxonomia; Owner: -
--

CREATE TABLE taxonomia.publicacao (
    sq_publicacao integer NOT NULL,
    sq_tipo_publicacao bigint,
    de_titulo text NOT NULL,
    no_autor text,
    nu_ano_publicacao smallint,
    no_revista_cientifica text,
    de_volume text,
    de_issue text,
    de_paginas text,
    de_ref_bibliografica text,
    in_lista_oficial_vigente boolean,
    de_titulo_livro text,
    de_editores text,
    no_editora text,
    no_cidade text,
    nu_edicao_livro text,
    de_url text,
    dt_acesso_url date,
    no_universidade text,
    de_doi text,
    de_issn text,
    de_isbn text,
    dt_publicacao date
);


--
-- TOC entry 13250 (class 0 OID 0)
-- Dependencies: 1729
-- Name: COLUMN publicacao.in_lista_oficial_vigente; Type: COMMENT; Schema: taxonomia; Owner: -
--

COMMENT ON COLUMN taxonomia.publicacao.in_lista_oficial_vigente IS 'Este campo informará se é ou não uma lista oficial nacional vigente para alimentar as opções do campo SQ_REF_BIB_LISTA_VIGENTE da tabela especies.ficha';


--
-- TOC entry 13251 (class 0 OID 0)
-- Dependencies: 1729
-- Name: COLUMN publicacao.dt_publicacao; Type: COMMENT; Schema: taxonomia; Owner: -
--

COMMENT ON COLUMN taxonomia.publicacao.dt_publicacao IS 'Data de publicação do documento';


--
-- TOC entry 2297 (class 1259 OID 31710)
-- Name: nome_comum; Type: TABLE; Schema: taxonomia; Owner: -
--

CREATE TABLE taxonomia.nome_comum (
    sq_nome_comum integer NOT NULL,
    sq_taxon bigint NOT NULL,
    de_nome_comum text NOT NULL,
    de_regiao text
);


--
-- TOC entry 2298 (class 1259 OID 31716)
-- Name: sensivel; Type: TABLE; Schema: taxonomia; Owner: -
--

CREATE TABLE taxonomia.sensivel (
    sq_sensivel integer NOT NULL,
    sq_taxon integer NOT NULL,
    sq_pessoa_inclusao integer NOT NULL,
    ts_data_inclusao timestamp without time zone DEFAULT now() NOT NULL,
    st_ativo boolean DEFAULT true NOT NULL,
    ts_data_desativado boolean DEFAULT false NOT NULL,
    sq_pessoa_desativado integer,
    obs_sensivel text
);


--
-- TOC entry 2299 (class 1259 OID 31725)
-- Name: vw_red_list_taxon; Type: VIEW; Schema: taxonomia; Owner: -
--

CREATE VIEW taxonomia.vw_red_list_taxon AS
 SELECT ficha.sq_taxon,
        CASE
            WHEN (historico.co_categoria = 'EX'::text) THEN 'Espécie Extinta'::text
            WHEN (historico.co_categoria = 'EW'::text) THEN 'Espécie Extinta'::text
            WHEN (historico.co_categoria = 'CR'::text) THEN 'Espécie Ameaçada'::text
            WHEN (historico.co_categoria = 'EN'::text) THEN 'Espécie Ameaçada'::text
            WHEN (historico.co_categoria = 'VU'::text) THEN 'Espécie Ameaçada'::text
            WHEN (historico.co_categoria = 'CD'::text) THEN 'Espécie não Ameaçada (Baixo risco)'::text
            WHEN (historico.co_categoria = 'NT'::text) THEN 'Espécie não Ameaçada (Baixo risco)'::text
            WHEN (historico.co_categoria = 'LC'::text) THEN 'Espécie não Ameaçada (Baixo risco)'::text
            WHEN (historico.co_categoria = 'DD'::text) THEN 'Espécie não Avaliadas'::text
            WHEN (historico.co_categoria = 'NE'::text) THEN 'Espécie não Avaliadas'::text
            ELSE NULL::text
        END AS de_situacao_ameaca,
        CASE
            WHEN (historico.co_categoria = 'EX'::text) THEN 'EE'::text
            WHEN (historico.co_categoria = 'EW'::text) THEN 'EE'::text
            WHEN (historico.co_categoria = 'CR'::text) THEN 'EA'::text
            WHEN (historico.co_categoria = 'EN'::text) THEN 'EA'::text
            WHEN (historico.co_categoria = 'VU'::text) THEN 'EA'::text
            WHEN (historico.co_categoria = 'CD'::text) THEN 'NM'::text
            WHEN (historico.co_categoria = 'NT'::text) THEN 'NM'::text
            WHEN (historico.co_categoria = 'LC'::text) THEN 'NM'::text
            WHEN (historico.co_categoria = 'DD'::text) THEN 'NV'::text
            WHEN (historico.co_categoria = 'NE'::text) THEN 'NV'::text
            ELSE NULL::text
        END AS co_situacao_ameaca,
    historico.co_categoria AS co_ameaca_oficial,
    historico.ds_categoria AS ds_ameaca_oficial,
    COALESCE(categoria_final.cd_sistema, '(ainda não validada no SALVE)'::text) AS co_ameaca_nao_oficial,
    COALESCE(categoria_final.ds_dados_apoio, '(ainda não validada no SALVE)'::text) AS ds_ameaca_nao_oficial
   FROM ((salve.ficha
     CROSS JOIN LATERAL ( SELECT categoria.cd_sistema AS co_categoria,
            categoria.ds_dados_apoio AS ds_categoria
           FROM (salve.taxon_historico_avaliacao hist
             JOIN salve.dados_apoio categoria ON ((categoria.sq_dados_apoio = hist.sq_categoria_iucn)))
          WHERE ((hist.sq_tipo_avaliacao = 311) AND (hist.sq_taxon = ficha.sq_taxon) AND (hist.nu_ano_avaliacao = ( SELECT max(x.nu_ano_avaliacao) AS max
                   FROM salve.taxon_historico_avaliacao x
                  WHERE ((x.sq_taxon = ficha.sq_taxon) AND (x.sq_tipo_avaliacao = 311)))))
          ORDER BY hist.sq_taxon_historico_avaliacao DESC
         LIMIT 1) historico)
     LEFT JOIN salve.dados_apoio categoria_final ON ((categoria_final.sq_dados_apoio = ficha.sq_categoria_final)))
  WHERE (ficha.sq_ciclo_avaliacao = 2)
UNION ALL
 SELECT ficha.sq_taxon,
        CASE
            WHEN (categoria_final.cd_sistema = 'EX'::text) THEN 'Espécie Extinta'::text
            WHEN (categoria_final.cd_sistema = 'EW'::text) THEN 'Espécie Extinta'::text
            WHEN (categoria_final.cd_sistema = 'CR'::text) THEN 'Espécie Ameaçada'::text
            WHEN (categoria_final.cd_sistema = 'EN'::text) THEN 'Espécie Ameaçada'::text
            WHEN (categoria_final.cd_sistema = 'VU'::text) THEN 'Espécie Ameaçada'::text
            WHEN (categoria_final.cd_sistema = 'CD'::text) THEN 'Espécie não Ameaçada (Baixo risco)'::text
            WHEN (categoria_final.cd_sistema = 'NT'::text) THEN 'Espécie não Ameaçada (Baixo risco)'::text
            WHEN (categoria_final.cd_sistema = 'LC'::text) THEN 'Espécie não Ameaçada (Baixo risco)'::text
            WHEN (categoria_final.cd_sistema = 'DD'::text) THEN 'Espécie não Avaliadas'::text
            WHEN (categoria_final.cd_sistema = 'NE'::text) THEN 'Espécie não Avaliadas'::text
            ELSE NULL::text
        END AS de_situacao_ameaca,
        CASE
            WHEN (categoria_final.cd_sistema = 'EX'::text) THEN 'EE'::text
            WHEN (categoria_final.cd_sistema = 'EW'::text) THEN 'EE'::text
            WHEN (categoria_final.cd_sistema = 'CR'::text) THEN 'EA'::text
            WHEN (categoria_final.cd_sistema = 'EN'::text) THEN 'EA'::text
            WHEN (categoria_final.cd_sistema = 'VU'::text) THEN 'EA'::text
            WHEN (categoria_final.cd_sistema = 'CD'::text) THEN 'NM'::text
            WHEN (categoria_final.cd_sistema = 'NT'::text) THEN 'NM'::text
            WHEN (categoria_final.cd_sistema = 'LC'::text) THEN 'NM'::text
            WHEN (categoria_final.cd_sistema = 'DD'::text) THEN 'NV'::text
            WHEN (categoria_final.cd_sistema = 'NE'::text) THEN 'NV'::text
            ELSE NULL::text
        END AS co_situacao_ameaca,
    categoria_final.cd_sistema AS co_ameaca_oficial,
    categoria_final.ds_dados_apoio AS ds_ameaca_oficial,
    NULL::text AS co_ameaca_nao_oficial,
    NULL::text AS ds_ameaca_nao_oficial
   FROM (salve.ficha
     LEFT JOIN salve.dados_apoio categoria_final ON ((categoria_final.sq_dados_apoio = ficha.sq_categoria_final)))
  WHERE ((ficha.sq_ciclo_avaliacao = 1) AND (NOT (EXISTS ( SELECT NULL::text AS text
           FROM salve.ficha x
          WHERE ((x.sq_ciclo_avaliacao = 2) AND (x.sq_taxon = ficha.sq_taxon))))) AND (ficha.sq_categoria_final IS NOT NULL));


--
-- TOC entry 2300 (class 1259 OID 31730)
-- Name: vw_red_list_status_gategory; Type: VIEW; Schema: taxonomia; Owner: -
--

CREATE VIEW taxonomia.vw_red_list_status_gategory AS
 SELECT vw_red_list_taxon.co_situacao_ameaca,
    vw_red_list_taxon.de_situacao_ameaca,
    vw_red_list_taxon.co_ameaca_oficial,
    vw_red_list_taxon.ds_ameaca_oficial
   FROM taxonomia.vw_red_list_taxon
  GROUP BY vw_red_list_taxon.co_situacao_ameaca, vw_red_list_taxon.de_situacao_ameaca, vw_red_list_taxon.co_ameaca_oficial, vw_red_list_taxon.ds_ameaca_oficial;


--
-- TOC entry 2301 (class 1259 OID 31734)
-- Name: vw_status_ameaca; Type: VIEW; Schema: taxonomia; Owner: -
--

CREATE VIEW taxonomia.vw_status_ameaca AS
 SELECT ficha.sq_taxon,
    historico.co_categoria AS co_ameaca_oficial,
    historico.ds_categoria AS ds_ameaca_oficial,
    COALESCE(categoria_final.cd_sistema, '(ainda não validada no SALVE)'::text) AS co_ameaca_nao_oficial,
    COALESCE(categoria_final.ds_dados_apoio, '(ainda não validada no SALVE)'::text) AS ds_ameaca_nao_oficial
   FROM ((salve.ficha
     CROSS JOIN LATERAL ( SELECT categoria.cd_sistema AS co_categoria,
            categoria.ds_dados_apoio AS ds_categoria
           FROM (salve.taxon_historico_avaliacao hist
             JOIN salve.dados_apoio categoria ON ((categoria.sq_dados_apoio = hist.sq_categoria_iucn)))
          WHERE ((hist.sq_tipo_avaliacao = 311) AND (hist.sq_taxon = ficha.sq_taxon) AND (hist.nu_ano_avaliacao = ( SELECT max(x.nu_ano_avaliacao) AS max
                   FROM salve.taxon_historico_avaliacao x
                  WHERE ((x.sq_taxon = ficha.sq_taxon) AND (x.sq_tipo_avaliacao = 311)))))
          ORDER BY hist.sq_taxon_historico_avaliacao DESC
         LIMIT 1) historico)
     LEFT JOIN salve.dados_apoio categoria_final ON ((categoria_final.sq_dados_apoio = ficha.sq_categoria_final)))
  WHERE (ficha.sq_ciclo_avaliacao = 2)
UNION ALL
 SELECT ficha.sq_taxon,
    categoria_final.cd_sistema AS co_ameaca_oficial,
    categoria_final.ds_dados_apoio AS ds_ameaca_oficial,
    NULL::text AS co_ameaca_nao_oficial,
    NULL::text AS ds_ameaca_nao_oficial
   FROM (salve.ficha
     LEFT JOIN salve.dados_apoio categoria_final ON ((categoria_final.sq_dados_apoio = ficha.sq_categoria_final)))
  WHERE ((ficha.sq_ciclo_avaliacao = 1) AND (NOT (EXISTS ( SELECT NULL::text AS text
           FROM salve.ficha x
          WHERE ((x.sq_ciclo_avaliacao = 2) AND (x.sq_taxon = ficha.sq_taxon))))) AND (ficha.sq_categoria_final IS NOT NULL));


--
-- TOC entry 2365 (class 1259 OID 32058)
-- Name: caracteristica; Type: TABLE; Schema: taxonomia; Owner: -
--

CREATE TABLE taxonomia.caracteristica (
    sq_caracteristica integer NOT NULL,
    de_caracteristica text NOT NULL
);


--
-- TOC entry 13252 (class 0 OID 0)
-- Dependencies: 2365
-- Name: TABLE caracteristica; Type: COMMENT; Schema: taxonomia; Owner: -
--

COMMENT ON TABLE taxonomia.caracteristica IS 'Exemplos de características:
1) Habitat
2) Descrição alimentar
3) Bioma
etc...';


--
-- TOC entry 2366 (class 1259 OID 32064)
-- Name: caracteristica_sq_caracteristica_seq; Type: SEQUENCE; Schema: taxonomia; Owner: -
--

CREATE SEQUENCE taxonomia.caracteristica_sq_caracteristica_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 13253 (class 0 OID 0)
-- Dependencies: 2366
-- Name: caracteristica_sq_caracteristica_seq; Type: SEQUENCE OWNED BY; Schema: taxonomia; Owner: -
--

ALTER SEQUENCE taxonomia.caracteristica_sq_caracteristica_seq OWNED BY taxonomia.caracteristica.sq_caracteristica;


--
-- TOC entry 2367 (class 1259 OID 32165)
-- Name: categoria_ameaca; Type: TABLE; Schema: taxonomia; Owner: -
--

CREATE TABLE taxonomia.categoria_ameaca (
    sq_categoria_ameaca integer NOT NULL,
    co_categoria_ameaca text NOT NULL,
    de_categoria_ameaca text NOT NULL
);


--
-- TOC entry 13254 (class 0 OID 0)
-- Dependencies: 2367
-- Name: TABLE categoria_ameaca; Type: COMMENT; Schema: taxonomia; Owner: -
--

COMMENT ON TABLE taxonomia.categoria_ameaca IS 'Categorias de ameaça:
a) Extinta - EX
b) Extinta na Natureza - EW
c) Regionalmente Extinta - RE
d) Criticamente em Perigo - CR
e) Em Perigo - EN
f) Vulnerável - VU
g) Quase Ameaçada - NT
h) Menos Preocupante - LC
i) Dados Insuficientes - DD
j) Não Aplicável - NA
k) Não Avaliada - NE';


--
-- TOC entry 2368 (class 1259 OID 32171)
-- Name: categoria_ameaca_sq_categoria_ameaca_seq; Type: SEQUENCE; Schema: taxonomia; Owner: -
--

CREATE SEQUENCE taxonomia.categoria_ameaca_sq_categoria_ameaca_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 13255 (class 0 OID 0)
-- Dependencies: 2368
-- Name: categoria_ameaca_sq_categoria_ameaca_seq; Type: SEQUENCE OWNED BY; Schema: taxonomia; Owner: -
--

ALTER SEQUENCE taxonomia.categoria_ameaca_sq_categoria_ameaca_seq OWNED BY taxonomia.categoria_ameaca.sq_categoria_ameaca;


--
-- TOC entry 2369 (class 1259 OID 32210)
-- Name: grupo; Type: TABLE; Schema: taxonomia; Owner: -
--

CREATE TABLE taxonomia.grupo (
    sq_grupo integer NOT NULL,
    de_grupo text NOT NULL,
    co_grupo text
);


--
-- TOC entry 13256 (class 0 OID 0)
-- Dependencies: 2369
-- Name: TABLE grupo; Type: COMMENT; Schema: taxonomia; Owner: -
--

COMMENT ON TABLE taxonomia.grupo IS 'Identificar o grupo em que a situação se aplica.  Ex:
a) "HISTORICO" para Situações ref. aos históricos
b) "NOME" para Situações ref. ao nomes dos Taxons
';


--
-- TOC entry 2370 (class 1259 OID 32216)
-- Name: grupo_sq_grupo_seq; Type: SEQUENCE; Schema: taxonomia; Owner: -
--

CREATE SEQUENCE taxonomia.grupo_sq_grupo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 13257 (class 0 OID 0)
-- Dependencies: 2370
-- Name: grupo_sq_grupo_seq; Type: SEQUENCE OWNED BY; Schema: taxonomia; Owner: -
--

ALTER SEQUENCE taxonomia.grupo_sq_grupo_seq OWNED BY taxonomia.grupo.sq_grupo;


--
-- TOC entry 2371 (class 1259 OID 32218)
-- Name: instituicao; Type: TABLE; Schema: taxonomia; Owner: -
--

CREATE TABLE taxonomia.instituicao (
    sq_instituicao integer NOT NULL,
    no_instituicao text NOT NULL,
    sg_instituicao text,
    no_sistema text,
    de_url text,
    no_catalogo_sistema text
);


--
-- TOC entry 13258 (class 0 OID 0)
-- Dependencies: 2371
-- Name: COLUMN instituicao.no_catalogo_sistema; Type: COMMENT; Schema: taxonomia; Owner: -
--

COMMENT ON COLUMN taxonomia.instituicao.no_catalogo_sistema IS 'Identificar o nome do catálogo ou do sistema onde o taxon foi validado';


--
-- TOC entry 2372 (class 1259 OID 32224)
-- Name: instituicao_sq_instituicao_seq; Type: SEQUENCE; Schema: taxonomia; Owner: -
--

CREATE SEQUENCE taxonomia.instituicao_sq_instituicao_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 13259 (class 0 OID 0)
-- Dependencies: 2372
-- Name: instituicao_sq_instituicao_seq; Type: SEQUENCE OWNED BY; Schema: taxonomia; Owner: -
--

ALTER SEQUENCE taxonomia.instituicao_sq_instituicao_seq OWNED BY taxonomia.instituicao.sq_instituicao;


--
-- TOC entry 2373 (class 1259 OID 32226)
-- Name: lista_abrangencia; Type: TABLE; Schema: taxonomia; Owner: -
--

CREATE TABLE taxonomia.lista_abrangencia (
    sq_lista_abrangencia integer NOT NULL,
    de_lista_abrangencia text NOT NULL
);


--
-- TOC entry 13260 (class 0 OID 0)
-- Dependencies: 2373
-- Name: TABLE lista_abrangencia; Type: COMMENT; Schema: taxonomia; Owner: -
--

COMMENT ON TABLE taxonomia.lista_abrangencia IS 'Exemplos de abrangências:
1) Nacional
2) Estadual
3) Municipal
4) Internacional
etc..';


--
-- TOC entry 2374 (class 1259 OID 32232)
-- Name: lista_abrangencia_sq_lista_abrangencia_seq; Type: SEQUENCE; Schema: taxonomia; Owner: -
--

CREATE SEQUENCE taxonomia.lista_abrangencia_sq_lista_abrangencia_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 13261 (class 0 OID 0)
-- Dependencies: 2374
-- Name: lista_abrangencia_sq_lista_abrangencia_seq; Type: SEQUENCE OWNED BY; Schema: taxonomia; Owner: -
--

ALTER SEQUENCE taxonomia.lista_abrangencia_sq_lista_abrangencia_seq OWNED BY taxonomia.lista_abrangencia.sq_lista_abrangencia;


--
-- TOC entry 2375 (class 1259 OID 32234)
-- Name: lista_especie_ameacada; Type: TABLE; Schema: taxonomia; Owner: -
--

CREATE TABLE taxonomia.lista_especie_ameacada (
    sq_lista_especie_ameacada integer NOT NULL,
    sq_lista_abrangencia integer NOT NULL,
    de_lista_especie_ameacada text NOT NULL,
    dt_publicacao date NOT NULL,
    de_url character varying(255),
    no_instituicao character varying(255) NOT NULL
);


--
-- TOC entry 2376 (class 1259 OID 32240)
-- Name: lista_especie_ameacada_sq_lista_especie_ameacada_seq; Type: SEQUENCE; Schema: taxonomia; Owner: -
--

CREATE SEQUENCE taxonomia.lista_especie_ameacada_sq_lista_especie_ameacada_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 13262 (class 0 OID 0)
-- Dependencies: 2376
-- Name: lista_especie_ameacada_sq_lista_especie_ameacada_seq; Type: SEQUENCE OWNED BY; Schema: taxonomia; Owner: -
--

ALTER SEQUENCE taxonomia.lista_especie_ameacada_sq_lista_especie_ameacada_seq OWNED BY taxonomia.lista_especie_ameacada.sq_lista_especie_ameacada;


--
-- TOC entry 2377 (class 1259 OID 32242)
-- Name: lista_referencia; Type: TABLE; Schema: taxonomia; Owner: -
--

CREATE TABLE taxonomia.lista_referencia (
    sq_lista_referencia integer NOT NULL,
    no_lista_referencia text NOT NULL,
    co_lista_referencia text NOT NULL,
    url_ipt text,
    de_obs smallint,
    xml_eml xml
);


--
-- TOC entry 13263 (class 0 OID 0)
-- Dependencies: 2377
-- Name: TABLE lista_referencia; Type: COMMENT; Schema: taxonomia; Owner: -
--

COMMENT ON TABLE taxonomia.lista_referencia IS 'COL
JArdim botanico
ICMBio
';


--
-- TOC entry 13264 (class 0 OID 0)
-- Dependencies: 2377
-- Name: COLUMN lista_referencia.no_lista_referencia; Type: COMMENT; Schema: taxonomia; Owner: -
--

COMMENT ON COLUMN taxonomia.lista_referencia.no_lista_referencia IS 'Nome da lista de referencia
Catalog da vida
Jardim Botanico';


--
-- TOC entry 13265 (class 0 OID 0)
-- Dependencies: 2377
-- Name: COLUMN lista_referencia.co_lista_referencia; Type: COMMENT; Schema: taxonomia; Owner: -
--

COMMENT ON COLUMN taxonomia.lista_referencia.co_lista_referencia IS 'Codigo da lista de referencia';


--
-- TOC entry 13266 (class 0 OID 0)
-- Dependencies: 2377
-- Name: COLUMN lista_referencia.url_ipt; Type: COMMENT; Schema: taxonomia; Owner: -
--

COMMENT ON COLUMN taxonomia.lista_referencia.url_ipt IS 'Url do Arquive Darwin Core  ADwC';


--
-- TOC entry 13267 (class 0 OID 0)
-- Dependencies: 2377
-- Name: COLUMN lista_referencia.xml_eml; Type: COMMENT; Schema: taxonomia; Owner: -
--

COMMENT ON COLUMN taxonomia.lista_referencia.xml_eml IS 'Metadata as an EML file em XML';


--
-- TOC entry 2378 (class 1259 OID 32248)
-- Name: lista_referencia_sq_lista_referencia_seq; Type: SEQUENCE; Schema: taxonomia; Owner: -
--

CREATE SEQUENCE taxonomia.lista_referencia_sq_lista_referencia_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 13268 (class 0 OID 0)
-- Dependencies: 2378
-- Name: lista_referencia_sq_lista_referencia_seq; Type: SEQUENCE OWNED BY; Schema: taxonomia; Owner: -
--

ALTER SEQUENCE taxonomia.lista_referencia_sq_lista_referencia_seq OWNED BY taxonomia.lista_referencia.sq_lista_referencia;


--
-- TOC entry 2379 (class 1259 OID 32250)
-- Name: nivel_taxonomico_sq_nivel_taxonomico_seq; Type: SEQUENCE; Schema: taxonomia; Owner: -
--

CREATE SEQUENCE taxonomia.nivel_taxonomico_sq_nivel_taxonomico_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 13269 (class 0 OID 0)
-- Dependencies: 2379
-- Name: nivel_taxonomico_sq_nivel_taxonomico_seq; Type: SEQUENCE OWNED BY; Schema: taxonomia; Owner: -
--

ALTER SEQUENCE taxonomia.nivel_taxonomico_sq_nivel_taxonomico_seq OWNED BY taxonomia.nivel_taxonomico.sq_nivel_taxonomico;


--
-- TOC entry 2380 (class 1259 OID 32252)
-- Name: nome_comum_sq_nome_comum_seq; Type: SEQUENCE; Schema: taxonomia; Owner: -
--

CREATE SEQUENCE taxonomia.nome_comum_sq_nome_comum_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 13270 (class 0 OID 0)
-- Dependencies: 2380
-- Name: nome_comum_sq_nome_comum_seq; Type: SEQUENCE OWNED BY; Schema: taxonomia; Owner: -
--

ALTER SEQUENCE taxonomia.nome_comum_sq_nome_comum_seq OWNED BY taxonomia.nome_comum.sq_nome_comum;


--
-- TOC entry 2381 (class 1259 OID 32254)
-- Name: publicacao_arquivo; Type: TABLE; Schema: taxonomia; Owner: -
--

CREATE TABLE taxonomia.publicacao_arquivo (
    sq_publicacao_arquivo integer NOT NULL,
    sq_publicacao integer NOT NULL,
    no_publicacao_arquivo text,
    de_tipo_conteudo text NOT NULL,
    de_local_arquivo text NOT NULL
);


--
-- TOC entry 13271 (class 0 OID 0)
-- Dependencies: 2381
-- Name: COLUMN publicacao_arquivo.no_publicacao_arquivo; Type: COMMENT; Schema: taxonomia; Owner: -
--

COMMENT ON COLUMN taxonomia.publicacao_arquivo.no_publicacao_arquivo IS 'Nome original do arquivo adicionado pelo usuário';


--
-- TOC entry 13272 (class 0 OID 0)
-- Dependencies: 2381
-- Name: COLUMN publicacao_arquivo.de_tipo_conteudo; Type: COMMENT; Schema: taxonomia; Owner: -
--

COMMENT ON COLUMN taxonomia.publicacao_arquivo.de_tipo_conteudo IS 'Tipo do conteúdo ( mime type) para informar ao navegador para exibir o preview';


--
-- TOC entry 13273 (class 0 OID 0)
-- Dependencies: 2381
-- Name: COLUMN publicacao_arquivo.de_local_arquivo; Type: COMMENT; Schema: taxonomia; Owner: -
--

COMMENT ON COLUMN taxonomia.publicacao_arquivo.de_local_arquivo IS 'Local  de armazenamento no disco';


--
-- TOC entry 2382 (class 1259 OID 32260)
-- Name: publicacao_arquivo_sq_publicacao_arquivo_seq; Type: SEQUENCE; Schema: taxonomia; Owner: -
--

CREATE SEQUENCE taxonomia.publicacao_arquivo_sq_publicacao_arquivo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 13274 (class 0 OID 0)
-- Dependencies: 2382
-- Name: publicacao_arquivo_sq_publicacao_arquivo_seq; Type: SEQUENCE OWNED BY; Schema: taxonomia; Owner: -
--

ALTER SEQUENCE taxonomia.publicacao_arquivo_sq_publicacao_arquivo_seq OWNED BY taxonomia.publicacao_arquivo.sq_publicacao_arquivo;


--
-- TOC entry 2383 (class 1259 OID 32262)
-- Name: publicacao_sq_publicacao_seq; Type: SEQUENCE; Schema: taxonomia; Owner: -
--

CREATE SEQUENCE taxonomia.publicacao_sq_publicacao_seq
    START WITH 13126
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 13275 (class 0 OID 0)
-- Dependencies: 2383
-- Name: publicacao_sq_publicacao_seq; Type: SEQUENCE OWNED BY; Schema: taxonomia; Owner: -
--

ALTER SEQUENCE taxonomia.publicacao_sq_publicacao_seq OWNED BY taxonomia.publicacao.sq_publicacao;


--
-- TOC entry 2384 (class 1259 OID 32264)
-- Name: publicacao_taxon; Type: TABLE; Schema: taxonomia; Owner: -
--

CREATE TABLE taxonomia.publicacao_taxon (
    sq_publicacao_taxon integer NOT NULL,
    sq_publicacao bigint NOT NULL,
    sq_taxon bigint NOT NULL
);


--
-- TOC entry 2385 (class 1259 OID 32267)
-- Name: publicacao_taxon_sq_publicacao_taxon_seq; Type: SEQUENCE; Schema: taxonomia; Owner: -
--

CREATE SEQUENCE taxonomia.publicacao_taxon_sq_publicacao_taxon_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 13276 (class 0 OID 0)
-- Dependencies: 2385
-- Name: publicacao_taxon_sq_publicacao_taxon_seq; Type: SEQUENCE OWNED BY; Schema: taxonomia; Owner: -
--

ALTER SEQUENCE taxonomia.publicacao_taxon_sq_publicacao_taxon_seq OWNED BY taxonomia.publicacao_taxon.sq_publicacao_taxon;


--
-- TOC entry 2386 (class 1259 OID 32269)
-- Name: sensivel_sq_sensivel_seq; Type: SEQUENCE; Schema: taxonomia; Owner: -
--

CREATE SEQUENCE taxonomia.sensivel_sq_sensivel_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 13277 (class 0 OID 0)
-- Dependencies: 2386
-- Name: sensivel_sq_sensivel_seq; Type: SEQUENCE OWNED BY; Schema: taxonomia; Owner: -
--

ALTER SEQUENCE taxonomia.sensivel_sq_sensivel_seq OWNED BY taxonomia.sensivel.sq_sensivel;


--
-- TOC entry 2387 (class 1259 OID 32271)
-- Name: sinonimia; Type: TABLE; Schema: taxonomia; Owner: -
--

CREATE TABLE taxonomia.sinonimia (
    sq_sinonimia integer NOT NULL,
    sq_taxon_aceito bigint NOT NULL,
    sq_taxon_sinonimo bigint NOT NULL,
    sq_situacao bigint NOT NULL,
    sq_lista_referencia integer NOT NULL
);


--
-- TOC entry 2388 (class 1259 OID 32274)
-- Name: sinonimia_hist; Type: TABLE; Schema: taxonomia; Owner: -
--

CREATE TABLE taxonomia.sinonimia_hist (
    sq_sinonimia bigint NOT NULL,
    sq_situacao bigint NOT NULL,
    sq_taxon_aceito bigint NOT NULL,
    sq_taxon_sinonimo bigint NOT NULL,
    dt_historico timestamp without time zone DEFAULT now() NOT NULL
);


--
-- TOC entry 2389 (class 1259 OID 32278)
-- Name: sinonimia_sq_sinonimia_seq; Type: SEQUENCE; Schema: taxonomia; Owner: -
--

CREATE SEQUENCE taxonomia.sinonimia_sq_sinonimia_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 13278 (class 0 OID 0)
-- Dependencies: 2389
-- Name: sinonimia_sq_sinonimia_seq; Type: SEQUENCE OWNED BY; Schema: taxonomia; Owner: -
--

ALTER SEQUENCE taxonomia.sinonimia_sq_sinonimia_seq OWNED BY taxonomia.sinonimia.sq_sinonimia;


--
-- TOC entry 2390 (class 1259 OID 32280)
-- Name: situacao; Type: TABLE; Schema: taxonomia; Owner: -
--

CREATE TABLE taxonomia.situacao (
    sq_situacao integer NOT NULL,
    sq_grupo bigint NOT NULL,
    de_situacao text NOT NULL,
    co_situacao text
);


--
-- TOC entry 13279 (class 0 OID 0)
-- Dependencies: 2390
-- Name: TABLE situacao; Type: COMMENT; Schema: taxonomia; Owner: -
--

COMMENT ON TABLE taxonomia.situacao IS 'Registra a situação do registro. Ex:
a) Inclusão
b) Alteração no nome do táxon
c) Desativação
d) Reclassificação
e) Cadastramento anterior
f) Cadastro equivalente
g) Alteração de outros dados
h) Reativação
i) Sinonímia
j) Alteração nos dados de autoria da descrição (autor/ano)
etc...';


--
-- TOC entry 2391 (class 1259 OID 32286)
-- Name: situacao_sq_situacao_seq; Type: SEQUENCE; Schema: taxonomia; Owner: -
--

CREATE SEQUENCE taxonomia.situacao_sq_situacao_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 13280 (class 0 OID 0)
-- Dependencies: 2391
-- Name: situacao_sq_situacao_seq; Type: SEQUENCE OWNED BY; Schema: taxonomia; Owner: -
--

ALTER SEQUENCE taxonomia.situacao_sq_situacao_seq OWNED BY taxonomia.situacao.sq_situacao;


--
-- TOC entry 2392 (class 1259 OID 32288)
-- Name: status_ameaca; Type: TABLE; Schema: taxonomia; Owner: -
--

CREATE TABLE taxonomia.status_ameaca (
    sq_status_ameaca integer NOT NULL,
    sq_lista_referencia integer NOT NULL,
    sq_categoria_ameaca_oficial integer,
    sq_categoria_ameaca_nao_oficial integer,
    ts_ultima_alteracao_ameaca timestamp without time zone DEFAULT now() NOT NULL
);


--
-- TOC entry 13281 (class 0 OID 0)
-- Dependencies: 2392
-- Name: TABLE status_ameaca; Type: COMMENT; Schema: taxonomia; Owner: -
--

COMMENT ON TABLE taxonomia.status_ameaca IS 'status da ameaca
threat Status
countryCode';


--
-- TOC entry 2393 (class 1259 OID 32292)
-- Name: status_ameaca_sq_status_ameaca_seq; Type: SEQUENCE; Schema: taxonomia; Owner: -
--

CREATE SEQUENCE taxonomia.status_ameaca_sq_status_ameaca_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 13282 (class 0 OID 0)
-- Dependencies: 2393
-- Name: status_ameaca_sq_status_ameaca_seq; Type: SEQUENCE OWNED BY; Schema: taxonomia; Owner: -
--

ALTER SEQUENCE taxonomia.status_ameaca_sq_status_ameaca_seq OWNED BY taxonomia.status_ameaca.sq_status_ameaca;


--
-- TOC entry 2394 (class 1259 OID 32294)
-- Name: taxon_ameacado; Type: TABLE; Schema: taxonomia; Owner: -
--

CREATE TABLE taxonomia.taxon_ameacado (
    sq_taxon_ameacado integer NOT NULL,
    sq_taxon bigint NOT NULL,
    sq_lista_especie_ameacada bigint NOT NULL,
    sq_categoria_ameaca bigint NOT NULL
);


--
-- TOC entry 2395 (class 1259 OID 32297)
-- Name: taxon_ameacado_sq_taxon_ameacado_seq; Type: SEQUENCE; Schema: taxonomia; Owner: -
--

CREATE SEQUENCE taxonomia.taxon_ameacado_sq_taxon_ameacado_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 13283 (class 0 OID 0)
-- Dependencies: 2395
-- Name: taxon_ameacado_sq_taxon_ameacado_seq; Type: SEQUENCE OWNED BY; Schema: taxonomia; Owner: -
--

ALTER SEQUENCE taxonomia.taxon_ameacado_sq_taxon_ameacado_seq OWNED BY taxonomia.taxon_ameacado.sq_taxon_ameacado;


--
-- TOC entry 2396 (class 1259 OID 32299)
-- Name: taxon_caracteristica; Type: TABLE; Schema: taxonomia; Owner: -
--

CREATE TABLE taxonomia.taxon_caracteristica (
    sq_taxon_caracteristica integer NOT NULL,
    sq_taxon bigint NOT NULL,
    sq_caracteristica bigint NOT NULL,
    de_taxon_caracteristica text NOT NULL
);


--
-- TOC entry 2397 (class 1259 OID 32305)
-- Name: taxon_caracteristica_sq_taxon_caracteristica_seq; Type: SEQUENCE; Schema: taxonomia; Owner: -
--

CREATE SEQUENCE taxonomia.taxon_caracteristica_sq_taxon_caracteristica_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 13284 (class 0 OID 0)
-- Dependencies: 2397
-- Name: taxon_caracteristica_sq_taxon_caracteristica_seq; Type: SEQUENCE OWNED BY; Schema: taxonomia; Owner: -
--

ALTER SEQUENCE taxonomia.taxon_caracteristica_sq_taxon_caracteristica_seq OWNED BY taxonomia.taxon_caracteristica.sq_taxon_caracteristica;


--
-- TOC entry 2398 (class 1259 OID 32307)
-- Name: taxon_hist; Type: TABLE; Schema: taxonomia; Owner: -
--

CREATE TABLE taxonomia.taxon_hist (
    sq_taxon_hist integer NOT NULL,
    sq_taxon bigint NOT NULL,
    sq_pessoa bigint NOT NULL,
    sq_situacao bigint NOT NULL,
    sq_taxon_pai bigint,
    sq_nivel_taxonomico bigint,
    no_taxon text,
    st_ativo boolean,
    no_autor text,
    nu_ano integer,
    in_ocorre_brasil boolean,
    de_historico text,
    dt_historico timestamp without time zone NOT NULL,
    CONSTRAINT chk_taxon_hist_ativo CHECK ((st_ativo = ANY (ARRAY[true, false]))),
    CONSTRAINT chk_taxon_hist_ocorre_brasil CHECK ((in_ocorre_brasil = ANY (ARRAY[true, false])))
);


--
-- TOC entry 13285 (class 0 OID 0)
-- Dependencies: 2398
-- Name: COLUMN taxon_hist.sq_taxon_hist; Type: COMMENT; Schema: taxonomia; Owner: -
--

COMMENT ON COLUMN taxonomia.taxon_hist.sq_taxon_hist IS 'Chave primária da tabela';


--
-- TOC entry 13286 (class 0 OID 0)
-- Dependencies: 2398
-- Name: COLUMN taxon_hist.in_ocorre_brasil; Type: COMMENT; Schema: taxonomia; Owner: -
--

COMMENT ON COLUMN taxonomia.taxon_hist.in_ocorre_brasil IS 'Informa se a espécie ocorre ou não no Brasil';


--
-- TOC entry 2399 (class 1259 OID 32315)
-- Name: taxon_hist_sq_taxon_hist_seq; Type: SEQUENCE; Schema: taxonomia; Owner: -
--

CREATE SEQUENCE taxonomia.taxon_hist_sq_taxon_hist_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 13287 (class 0 OID 0)
-- Dependencies: 2399
-- Name: taxon_hist_sq_taxon_hist_seq; Type: SEQUENCE OWNED BY; Schema: taxonomia; Owner: -
--

ALTER SEQUENCE taxonomia.taxon_hist_sq_taxon_hist_seq OWNED BY taxonomia.taxon_hist.sq_taxon_hist;


--
-- TOC entry 2400 (class 1259 OID 32317)
-- Name: taxon_instituicao; Type: TABLE; Schema: taxonomia; Owner: -
--

CREATE TABLE taxonomia.taxon_instituicao (
    sq_taxon_instituicao integer NOT NULL,
    sq_instituicao bigint NOT NULL,
    sq_taxon bigint NOT NULL,
    de_url_taxon text,
    dt_ultima_consulta timestamp without time zone,
    st_encontrado boolean
);


--
-- TOC entry 13288 (class 0 OID 0)
-- Dependencies: 2400
-- Name: TABLE taxon_instituicao; Type: COMMENT; Schema: taxonomia; Owner: -
--

COMMENT ON TABLE taxonomia.taxon_instituicao IS 'Registra o identificador do taxon de outra instituição e o identificador do taxon no icmbio.';


--
-- TOC entry 13289 (class 0 OID 0)
-- Dependencies: 2400
-- Name: COLUMN taxon_instituicao.de_url_taxon; Type: COMMENT; Schema: taxonomia; Owner: -
--

COMMENT ON COLUMN taxonomia.taxon_instituicao.de_url_taxon IS 'url utilizada para consultar o taxon quando não existir um id definido';


--
-- TOC entry 13290 (class 0 OID 0)
-- Dependencies: 2400
-- Name: COLUMN taxon_instituicao.dt_ultima_consulta; Type: COMMENT; Schema: taxonomia; Owner: -
--

COMMENT ON COLUMN taxonomia.taxon_instituicao.dt_ultima_consulta IS 'Data e hora da última validação realizada';


--
-- TOC entry 2401 (class 1259 OID 32323)
-- Name: taxon_instituicao_sq_taxon_instituicao_seq; Type: SEQUENCE; Schema: taxonomia; Owner: -
--

CREATE SEQUENCE taxonomia.taxon_instituicao_sq_taxon_instituicao_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 13291 (class 0 OID 0)
-- Dependencies: 2401
-- Name: taxon_instituicao_sq_taxon_instituicao_seq; Type: SEQUENCE OWNED BY; Schema: taxonomia; Owner: -
--

ALTER SEQUENCE taxonomia.taxon_instituicao_sq_taxon_instituicao_seq OWNED BY taxonomia.taxon_instituicao.sq_taxon_instituicao;


--
-- TOC entry 2402 (class 1259 OID 32325)
-- Name: taxon_sq_taxon_seq; Type: SEQUENCE; Schema: taxonomia; Owner: -
--

CREATE SEQUENCE taxonomia.taxon_sq_taxon_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 13292 (class 0 OID 0)
-- Dependencies: 2402
-- Name: taxon_sq_taxon_seq; Type: SEQUENCE OWNED BY; Schema: taxonomia; Owner: -
--

ALTER SEQUENCE taxonomia.taxon_sq_taxon_seq OWNED BY taxonomia.taxon.sq_taxon;


--
-- TOC entry 2403 (class 1259 OID 32327)
-- Name: tipo_publicacao; Type: TABLE; Schema: taxonomia; Owner: -
--

CREATE TABLE taxonomia.tipo_publicacao (
    sq_tipo_publicacao integer NOT NULL,
    sq_tipo_uso bigint NOT NULL,
    de_tipo_publicacao text NOT NULL,
    co_tipo_publicacao text
);


--
-- TOC entry 13293 (class 0 OID 0)
-- Dependencies: 2403
-- Name: TABLE tipo_publicacao; Type: COMMENT; Schema: taxonomia; Owner: -
--

COMMENT ON TABLE taxonomia.tipo_publicacao IS 'Tipos de Publicação:
1) Artigo
2) Revista
3) Jornal 
etc...';


--
-- TOC entry 13294 (class 0 OID 0)
-- Dependencies: 2403
-- Name: COLUMN tipo_publicacao.co_tipo_publicacao; Type: COMMENT; Schema: taxonomia; Owner: -
--

COMMENT ON COLUMN taxonomia.tipo_publicacao.co_tipo_publicacao IS 'código único do tipo de publicação para uso interno nas regras de negócio';


--
-- TOC entry 2404 (class 1259 OID 32333)
-- Name: tipo_publicacao_sq_tipo_publicacao_seq; Type: SEQUENCE; Schema: taxonomia; Owner: -
--

CREATE SEQUENCE taxonomia.tipo_publicacao_sq_tipo_publicacao_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 13295 (class 0 OID 0)
-- Dependencies: 2404
-- Name: tipo_publicacao_sq_tipo_publicacao_seq; Type: SEQUENCE OWNED BY; Schema: taxonomia; Owner: -
--

ALTER SEQUENCE taxonomia.tipo_publicacao_sq_tipo_publicacao_seq OWNED BY taxonomia.tipo_publicacao.sq_tipo_publicacao;


--
-- TOC entry 2405 (class 1259 OID 32335)
-- Name: tipo_uso; Type: TABLE; Schema: taxonomia; Owner: -
--

CREATE TABLE taxonomia.tipo_uso (
    sq_tipo_uso integer NOT NULL,
    de_tipo_uso text,
    co_tipo_uso text
);


--
-- TOC entry 13296 (class 0 OID 0)
-- Dependencies: 2405
-- Name: TABLE tipo_uso; Type: COMMENT; Schema: taxonomia; Owner: -
--

COMMENT ON TABLE taxonomia.tipo_uso IS 'Exemplo de tipos de uso:
1) Científico
2) Não Científico
etc...';


--
-- TOC entry 2406 (class 1259 OID 32341)
-- Name: tipo_uso_sq_tipo_uso_seq; Type: SEQUENCE; Schema: taxonomia; Owner: -
--

CREATE SEQUENCE taxonomia.tipo_uso_sq_tipo_uso_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 13297 (class 0 OID 0)
-- Dependencies: 2406
-- Name: tipo_uso_sq_tipo_uso_seq; Type: SEQUENCE OWNED BY; Schema: taxonomia; Owner: -
--

ALTER SEQUENCE taxonomia.tipo_uso_sq_tipo_uso_seq OWNED BY taxonomia.tipo_uso.sq_tipo_uso;


--
-- TOC entry 2407 (class 1259 OID 32355)
-- Name: vw_discovery_fk_taxon; Type: VIEW; Schema: taxonomia; Owner: -
--

CREATE VIEW taxonomia.vw_discovery_fk_taxon AS
 SELECT 'sq_taxon'::text AS nome_coluna_fk,
    planilha_registro.sq_taxon AS valor_coluna_fk,
    'lafsisbio'::text AS esquema_nome_tabela_pk,
    'planilha_registro'::text AS nome_tabela_pk,
    'sq_planilha_registro'::text AS nome_coluna_pk,
    (planilha_registro.sq_planilha_registro)::bigint AS valor_pk
   FROM lafsisbio.planilha_registro
  WHERE (planilha_registro.sq_taxon IS NOT NULL)
UNION
 SELECT 'taxon_id'::text AS nome_coluna_fk,
    core_coletataxon.taxon_id AS valor_coluna_fk,
    'monitora'::text AS esquema_nome_tabela_pk,
    'core_coletataxon'::text AS nome_tabela_pk,
    'id'::text AS nome_coluna_pk,
    (core_coletataxon.id)::bigint AS valor_pk
   FROM monitora.core_coletataxon
  WHERE (core_coletataxon.taxon_id IS NOT NULL)
UNION
 SELECT 'taxon_id'::text AS nome_coluna_fk,
    core_protocolotaxon.taxon_id AS valor_coluna_fk,
    'monitora'::text AS esquema_nome_tabela_pk,
    'core_protocolotaxon'::text AS nome_tabela_pk,
    'id'::text AS nome_coluna_pk,
    (core_protocolotaxon.id)::bigint AS valor_pk
   FROM monitora.core_protocolotaxon
  WHERE (core_protocolotaxon.taxon_id IS NOT NULL)
UNION
 SELECT 'taxon_id'::text AS nome_coluna_fk,
    core_registro.taxon_id AS valor_coluna_fk,
    'monitora'::text AS esquema_nome_tabela_pk,
    'core_registro'::text AS nome_tabela_pk,
    'id'::text AS nome_coluna_pk,
    core_registro.id AS valor_pk
   FROM monitora.core_registro
  WHERE (core_registro.taxon_id IS NOT NULL)
UNION
 SELECT 'sq_taxon'::text AS nome_coluna_fk,
    ficha_historico_avaliacao.sq_taxon AS valor_coluna_fk,
    'salve'::text AS esquema_nome_tabela_pk,
    'ficha_historico_avaliacao'::text AS nome_tabela_pk,
    'sq_ficha_historico_avaliacao'::text AS nome_coluna_pk,
    (ficha_historico_avaliacao.sq_ficha_historico_avaliacao)::bigint AS valor_pk
   FROM salve.ficha_historico_avaliacao
  WHERE (ficha_historico_avaliacao.sq_taxon IS NOT NULL)
UNION
 SELECT 'sq_taxon_citado'::text AS nome_coluna_fk,
    ficha_ocorrencia.sq_taxon_citado AS valor_coluna_fk,
    'salve'::text AS esquema_nome_tabela_pk,
    'ficha_ocorrencia'::text AS nome_tabela_pk,
    'sq_ficha_ocorrencia'::text AS nome_coluna_pk,
    (ficha_ocorrencia.sq_ficha_ocorrencia)::bigint AS valor_pk
   FROM salve.ficha_ocorrencia
  WHERE (ficha_ocorrencia.sq_taxon_citado IS NOT NULL)
UNION
 SELECT 'sq_taxon'::text AS nome_coluna_fk,
    ficha.sq_taxon AS valor_coluna_fk,
    'salve'::text AS esquema_nome_tabela_pk,
    'ficha'::text AS nome_tabela_pk,
    'sq_ficha'::text AS nome_coluna_pk,
    (ficha.sq_ficha)::bigint AS valor_pk
   FROM salve.ficha
  WHERE (ficha.sq_taxon IS NOT NULL)
UNION
 SELECT 'sq_taxon'::text AS nome_coluna_fk,
    taxon_historico_avaliacao.sq_taxon AS valor_coluna_fk,
    'salve'::text AS esquema_nome_tabela_pk,
    'taxon_historico_avaliacao'::text AS nome_tabela_pk,
    'sq_taxon_historico_avaliacao'::text AS nome_coluna_pk,
    (taxon_historico_avaliacao.sq_taxon_historico_avaliacao)::bigint AS valor_pk
   FROM salve.taxon_historico_avaliacao
  WHERE (taxon_historico_avaliacao.sq_taxon IS NOT NULL)
UNION
 SELECT 'sq_taxon'::text AS nome_coluna_fk,
    ficha_habito_alimentar_esp.sq_taxon AS valor_coluna_fk,
    'salve'::text AS esquema_nome_tabela_pk,
    'ficha_habito_alimentar_esp'::text AS nome_tabela_pk,
    'sq_ficha_habito_alimentar_esp'::text AS nome_coluna_pk,
    (ficha_habito_alimentar_esp.sq_ficha_habito_alimentar_esp)::bigint AS valor_pk
   FROM salve.ficha_habito_alimentar_esp
  WHERE (ficha_habito_alimentar_esp.sq_taxon IS NOT NULL)
UNION
 SELECT 'sq_taxon'::text AS nome_coluna_fk,
    ficha_interacao.sq_taxon AS valor_coluna_fk,
    'salve'::text AS esquema_nome_tabela_pk,
    'ficha_interacao'::text AS nome_tabela_pk,
    'sq_ficha_interacao'::text AS nome_coluna_pk,
    (ficha_interacao.sq_ficha_interacao)::bigint AS valor_pk
   FROM salve.ficha_interacao
  WHERE (ficha_interacao.sq_taxon IS NOT NULL)
UNION
 SELECT 'sq_taxon'::text AS nome_coluna_fk,
    rel_taxon_historico.sq_taxon AS valor_coluna_fk,
    'sisbio'::text AS esquema_nome_tabela_pk,
    'rel_taxon_historico'::text AS nome_tabela_pk,
    'sq_rel_taxon_historico'::text AS nome_coluna_pk,
    (rel_taxon_historico.sq_rel_taxon_historico)::bigint AS valor_pk
   FROM sisbio.rel_taxon_historico
  WHERE (rel_taxon_historico.sq_taxon IS NOT NULL)
UNION
 SELECT 'sq_taxon_pai'::text AS nome_coluna_fk,
    rel_taxon_catalogar.sq_taxon_pai AS valor_coluna_fk,
    'sisbio'::text AS esquema_nome_tabela_pk,
    'rel_taxon_catalogar'::text AS nome_tabela_pk,
    'sq_rel_taxon'::text AS nome_coluna_pk,
    rel_taxon_catalogar.sq_rel_taxon AS valor_pk
   FROM sisbio.rel_taxon_catalogar
  WHERE (rel_taxon_catalogar.sq_taxon_pai IS NOT NULL)
UNION
 SELECT 'sq_taxon'::text AS nome_coluna_fk,
    rel_taxon.sq_taxon AS valor_coluna_fk,
    'sisbio'::text AS esquema_nome_tabela_pk,
    'rel_taxon'::text AS nome_tabela_pk,
    'sq_rel_taxon'::text AS nome_coluna_pk,
    (rel_taxon.sq_rel_taxon)::bigint AS valor_pk
   FROM sisbio.rel_taxon
  WHERE (rel_taxon.sq_taxon IS NOT NULL)
UNION
 SELECT 'sq_taxon'::text AS nome_coluna_fk,
    sol_taxon_historico.sq_taxon AS valor_coluna_fk,
    'sisbio'::text AS esquema_nome_tabela_pk,
    'sol_taxon_historico'::text AS nome_tabela_pk,
    'sq_sol_taxon_historico'::text AS nome_coluna_pk,
    (sol_taxon_historico.sq_sol_taxon_historico)::bigint AS valor_pk
   FROM sisbio.sol_taxon_historico
  WHERE (sol_taxon_historico.sq_taxon IS NOT NULL)
UNION
 SELECT 'sq_taxon'::text AS nome_coluna_fk,
    solicitacao_taxon.sq_taxon AS valor_coluna_fk,
    'sisbio'::text AS esquema_nome_tabela_pk,
    'solicitacao_taxon'::text AS nome_tabela_pk,
    'sq_solicitacao_taxon'::text AS nome_coluna_pk,
    (solicitacao_taxon.sq_solicitacao_taxon)::bigint AS valor_pk
   FROM sisbio.solicitacao_taxon
  WHERE (solicitacao_taxon.sq_taxon IS NOT NULL)
UNION
 SELECT 'sq_taxon'::text AS nome_coluna_fk,
    taxonomia_ope_taxon.sq_taxon AS valor_coluna_fk,
    'sisbio'::text AS esquema_nome_tabela_pk,
    'taxonomia_ope_taxon'::text AS nome_tabela_pk,
    'sq_taxonomia_ope_taxon'::text AS nome_coluna_pk,
    (taxonomia_ope_taxon.sq_taxonomia_ope_taxon)::bigint AS valor_pk
   FROM sisbio.taxonomia_ope_taxon
  WHERE (taxonomia_ope_taxon.sq_taxon IS NOT NULL)
UNION
 SELECT 'sq_taxon'::text AS nome_coluna_fk,
    nome_comum.sq_taxon AS valor_coluna_fk,
    'taxonomia'::text AS esquema_nome_tabela_pk,
    'nome_comum'::text AS nome_tabela_pk,
    'sq_nome_comum'::text AS nome_coluna_pk,
    (nome_comum.sq_nome_comum)::bigint AS valor_pk
   FROM taxonomia.nome_comum
  WHERE (nome_comum.sq_taxon IS NOT NULL)
UNION
 SELECT 'sq_taxon_aceito'::text AS nome_coluna_fk,
    sinonimia.sq_taxon_aceito AS valor_coluna_fk,
    'taxonomia'::text AS esquema_nome_tabela_pk,
    'sinonimia'::text AS nome_tabela_pk,
    'sq_sinonimia'::text AS nome_coluna_pk,
    (sinonimia.sq_sinonimia)::bigint AS valor_pk
   FROM taxonomia.sinonimia
  WHERE (sinonimia.sq_taxon_aceito IS NOT NULL)
UNION
 SELECT 'sq_taxon_sinonimo'::text AS nome_coluna_fk,
    sinonimia.sq_taxon_sinonimo AS valor_coluna_fk,
    'taxonomia'::text AS esquema_nome_tabela_pk,
    'sinonimia'::text AS nome_tabela_pk,
    'sq_sinonimia'::text AS nome_coluna_pk,
    (sinonimia.sq_sinonimia)::bigint AS valor_pk
   FROM taxonomia.sinonimia
  WHERE (sinonimia.sq_taxon_sinonimo IS NOT NULL)
UNION
 SELECT 'sq_taxon'::text AS nome_coluna_fk,
    taxon_ameacado.sq_taxon AS valor_coluna_fk,
    'taxonomia'::text AS esquema_nome_tabela_pk,
    'taxon_ameacado'::text AS nome_tabela_pk,
    'sq_taxon_ameacado'::text AS nome_coluna_pk,
    (taxon_ameacado.sq_taxon_ameacado)::bigint AS valor_pk
   FROM taxonomia.taxon_ameacado
  WHERE (taxon_ameacado.sq_taxon IS NOT NULL)
UNION
 SELECT 'sq_taxon'::text AS nome_coluna_fk,
    taxon_caracteristica.sq_taxon AS valor_coluna_fk,
    'taxonomia'::text AS esquema_nome_tabela_pk,
    'taxon_caracteristica'::text AS nome_tabela_pk,
    'sq_taxon_caracteristica'::text AS nome_coluna_pk,
    (taxon_caracteristica.sq_taxon_caracteristica)::bigint AS valor_pk
   FROM taxonomia.taxon_caracteristica
  WHERE (taxon_caracteristica.sq_taxon IS NOT NULL)
UNION
 SELECT 'sq_taxon'::text AS nome_coluna_fk,
    taxon_instituicao.sq_taxon AS valor_coluna_fk,
    'taxonomia'::text AS esquema_nome_tabela_pk,
    'taxon_instituicao'::text AS nome_tabela_pk,
    'sq_taxon_instituicao'::text AS nome_coluna_pk,
    (taxon_instituicao.sq_taxon_instituicao)::bigint AS valor_pk
   FROM taxonomia.taxon_instituicao
  WHERE (taxon_instituicao.sq_taxon IS NOT NULL)
UNION
 SELECT 'sq_taxon'::text AS nome_coluna_fk,
    publicacao_taxon.sq_taxon AS valor_coluna_fk,
    'taxonomia'::text AS esquema_nome_tabela_pk,
    'publicacao_taxon'::text AS nome_tabela_pk,
    'sq_publicacao_taxon'::text AS nome_coluna_pk,
    (publicacao_taxon.sq_publicacao_taxon)::bigint AS valor_pk
   FROM taxonomia.publicacao_taxon
  WHERE (publicacao_taxon.sq_taxon IS NOT NULL)
UNION
 SELECT 'sq_taxon'::text AS nome_coluna_fk,
    sensivel.sq_taxon AS valor_coluna_fk,
    'taxonomia'::text AS esquema_nome_tabela_pk,
    'sensivel'::text AS nome_tabela_pk,
    'sq_sensivel'::text AS nome_coluna_pk,
    (sensivel.sq_sensivel)::bigint AS valor_pk
   FROM taxonomia.sensivel
  WHERE (sensivel.sq_taxon IS NOT NULL);


--
-- TOC entry 2408 (class 1259 OID 32360)
-- Name: vw_nivel_taxonomico; Type: VIEW; Schema: taxonomia; Owner: -
--

CREATE VIEW taxonomia.vw_nivel_taxonomico AS
 SELECT nivel_taxonomico.sq_nivel_taxonomico,
    nivel_taxonomico.de_nivel_taxonomico,
    nivel_taxonomico.nu_grau_taxonomico,
    nivel_taxonomico.in_nivel_intermediario,
    nivel_taxonomico.co_nivel_taxonomico
   FROM taxonomia.nivel_taxonomico;


--
-- TOC entry 2409 (class 1259 OID 32364)
-- Name: vw_nome_comum; Type: VIEW; Schema: taxonomia; Owner: -
--

CREATE VIEW taxonomia.vw_nome_comum AS
 SELECT nome_comum.sq_nome_comum,
    nome_comum.sq_taxon,
    nome_comum.de_nome_comum,
    nome_comum.de_regiao
   FROM taxonomia.nome_comum;


--
-- TOC entry 2410 (class 1259 OID 32368)
-- Name: vw_red_list_status; Type: VIEW; Schema: taxonomia; Owner: -
--

CREATE VIEW taxonomia.vw_red_list_status AS
 SELECT vw_red_list_taxon.co_situacao_ameaca,
    vw_red_list_taxon.de_situacao_ameaca
   FROM taxonomia.vw_red_list_taxon
  GROUP BY vw_red_list_taxon.co_situacao_ameaca, vw_red_list_taxon.de_situacao_ameaca;


--
-- TOC entry 2411 (class 1259 OID 32377)
-- Name: vw_sensivel; Type: VIEW; Schema: taxonomia; Owner: -
--

CREATE VIEW taxonomia.vw_sensivel AS
 SELECT sensivel.sq_sensivel,
    sensivel.sq_taxon,
    sensivel.sq_pessoa_inclusao,
    sensivel.ts_data_inclusao,
    sensivel.st_ativo,
    sensivel.ts_data_desativado,
    sensivel.sq_pessoa_desativado,
    sensivel.obs_sensivel
   FROM taxonomia.sensivel;


--
-- TOC entry 2412 (class 1259 OID 32381)
-- Name: vw_taxonomia; Type: VIEW; Schema: taxonomia; Owner: -
--

CREATE VIEW taxonomia.vw_taxonomia AS
 SELECT taxon.sq_taxon,
    taxon.sq_taxon_pai,
    taxon.sq_nivel_taxonomico,
    taxon.sq_situacao_nome,
    taxon.no_taxon,
    taxon.st_ativo,
    taxon.no_autor,
    taxon.nu_ano,
    taxon.in_ocorre_brasil,
    taxon.de_autor_ano,
    taxonomia.formatar_nome((taxon.sq_taxon)::bigint) AS no_taxon_formatado,
    (taxon.json_trilha)::jsonb AS json_trilha,
    nivel_taxonomico.de_nivel_taxonomico,
    nivel_taxonomico.nu_grau_taxonomico,
    nivel_taxonomico.in_nivel_intermediario,
    nivel_taxonomico.co_nivel_taxonomico
   FROM (taxonomia.taxon
     JOIN taxonomia.nivel_taxonomico ON ((nivel_taxonomico.sq_nivel_taxonomico = taxon.sq_nivel_taxonomico)));


--
-- TOC entry 12507 (class 2604 OID 33147)
-- Name: caracteristica sq_caracteristica; Type: DEFAULT; Schema: taxonomia; Owner: -
--

ALTER TABLE ONLY taxonomia.caracteristica ALTER COLUMN sq_caracteristica SET DEFAULT nextval('taxonomia.caracteristica_sq_caracteristica_seq'::regclass);


--
-- TOC entry 12508 (class 2604 OID 33151)
-- Name: categoria_ameaca sq_categoria_ameaca; Type: DEFAULT; Schema: taxonomia; Owner: -
--

ALTER TABLE ONLY taxonomia.categoria_ameaca ALTER COLUMN sq_categoria_ameaca SET DEFAULT nextval('taxonomia.categoria_ameaca_sq_categoria_ameaca_seq'::regclass);


--
-- TOC entry 12509 (class 2604 OID 33156)
-- Name: grupo sq_grupo; Type: DEFAULT; Schema: taxonomia; Owner: -
--

ALTER TABLE ONLY taxonomia.grupo ALTER COLUMN sq_grupo SET DEFAULT nextval('taxonomia.grupo_sq_grupo_seq'::regclass);


--
-- TOC entry 12510 (class 2604 OID 33157)
-- Name: instituicao sq_instituicao; Type: DEFAULT; Schema: taxonomia; Owner: -
--

ALTER TABLE ONLY taxonomia.instituicao ALTER COLUMN sq_instituicao SET DEFAULT nextval('taxonomia.instituicao_sq_instituicao_seq'::regclass);


--
-- TOC entry 12511 (class 2604 OID 33158)
-- Name: lista_abrangencia sq_lista_abrangencia; Type: DEFAULT; Schema: taxonomia; Owner: -
--

ALTER TABLE ONLY taxonomia.lista_abrangencia ALTER COLUMN sq_lista_abrangencia SET DEFAULT nextval('taxonomia.lista_abrangencia_sq_lista_abrangencia_seq'::regclass);


--
-- TOC entry 12512 (class 2604 OID 33159)
-- Name: lista_especie_ameacada sq_lista_especie_ameacada; Type: DEFAULT; Schema: taxonomia; Owner: -
--

ALTER TABLE ONLY taxonomia.lista_especie_ameacada ALTER COLUMN sq_lista_especie_ameacada SET DEFAULT nextval('taxonomia.lista_especie_ameacada_sq_lista_especie_ameacada_seq'::regclass);


--
-- TOC entry 12513 (class 2604 OID 33160)
-- Name: lista_referencia sq_lista_referencia; Type: DEFAULT; Schema: taxonomia; Owner: -
--

ALTER TABLE ONLY taxonomia.lista_referencia ALTER COLUMN sq_lista_referencia SET DEFAULT nextval('taxonomia.lista_referencia_sq_lista_referencia_seq'::regclass);


--
-- TOC entry 12494 (class 2604 OID 33161)
-- Name: nivel_taxonomico sq_nivel_taxonomico; Type: DEFAULT; Schema: taxonomia; Owner: -
--

ALTER TABLE ONLY taxonomia.nivel_taxonomico ALTER COLUMN sq_nivel_taxonomico SET DEFAULT nextval('taxonomia.nivel_taxonomico_sq_nivel_taxonomico_seq'::regclass);


--
-- TOC entry 12502 (class 2604 OID 33162)
-- Name: nome_comum sq_nome_comum; Type: DEFAULT; Schema: taxonomia; Owner: -
--

ALTER TABLE ONLY taxonomia.nome_comum ALTER COLUMN sq_nome_comum SET DEFAULT nextval('taxonomia.nome_comum_sq_nome_comum_seq'::regclass);


--
-- TOC entry 12501 (class 2604 OID 33163)
-- Name: publicacao sq_publicacao; Type: DEFAULT; Schema: taxonomia; Owner: -
--

ALTER TABLE ONLY taxonomia.publicacao ALTER COLUMN sq_publicacao SET DEFAULT nextval('taxonomia.publicacao_sq_publicacao_seq'::regclass);


--
-- TOC entry 12514 (class 2604 OID 33164)
-- Name: publicacao_arquivo sq_publicacao_arquivo; Type: DEFAULT; Schema: taxonomia; Owner: -
--

ALTER TABLE ONLY taxonomia.publicacao_arquivo ALTER COLUMN sq_publicacao_arquivo SET DEFAULT nextval('taxonomia.publicacao_arquivo_sq_publicacao_arquivo_seq'::regclass);


--
-- TOC entry 12515 (class 2604 OID 33165)
-- Name: publicacao_taxon sq_publicacao_taxon; Type: DEFAULT; Schema: taxonomia; Owner: -
--

ALTER TABLE ONLY taxonomia.publicacao_taxon ALTER COLUMN sq_publicacao_taxon SET DEFAULT nextval('taxonomia.publicacao_taxon_sq_publicacao_taxon_seq'::regclass);


--
-- TOC entry 12506 (class 2604 OID 33166)
-- Name: sensivel sq_sensivel; Type: DEFAULT; Schema: taxonomia; Owner: -
--

ALTER TABLE ONLY taxonomia.sensivel ALTER COLUMN sq_sensivel SET DEFAULT nextval('taxonomia.sensivel_sq_sensivel_seq'::regclass);


--
-- TOC entry 12516 (class 2604 OID 33167)
-- Name: sinonimia sq_sinonimia; Type: DEFAULT; Schema: taxonomia; Owner: -
--

ALTER TABLE ONLY taxonomia.sinonimia ALTER COLUMN sq_sinonimia SET DEFAULT nextval('taxonomia.sinonimia_sq_sinonimia_seq'::regclass);


--
-- TOC entry 12518 (class 2604 OID 33168)
-- Name: situacao sq_situacao; Type: DEFAULT; Schema: taxonomia; Owner: -
--

ALTER TABLE ONLY taxonomia.situacao ALTER COLUMN sq_situacao SET DEFAULT nextval('taxonomia.situacao_sq_situacao_seq'::regclass);


--
-- TOC entry 12519 (class 2604 OID 33169)
-- Name: status_ameaca sq_status_ameaca; Type: DEFAULT; Schema: taxonomia; Owner: -
--

ALTER TABLE ONLY taxonomia.status_ameaca ALTER COLUMN sq_status_ameaca SET DEFAULT nextval('taxonomia.status_ameaca_sq_status_ameaca_seq'::regclass);


--
-- TOC entry 12499 (class 2604 OID 33170)
-- Name: taxon sq_taxon; Type: DEFAULT; Schema: taxonomia; Owner: -
--

ALTER TABLE ONLY taxonomia.taxon ALTER COLUMN sq_taxon SET DEFAULT nextval('taxonomia.taxon_sq_taxon_seq'::regclass);


--
-- TOC entry 12521 (class 2604 OID 33171)
-- Name: taxon_ameacado sq_taxon_ameacado; Type: DEFAULT; Schema: taxonomia; Owner: -
--

ALTER TABLE ONLY taxonomia.taxon_ameacado ALTER COLUMN sq_taxon_ameacado SET DEFAULT nextval('taxonomia.taxon_ameacado_sq_taxon_ameacado_seq'::regclass);


--
-- TOC entry 12522 (class 2604 OID 33172)
-- Name: taxon_caracteristica sq_taxon_caracteristica; Type: DEFAULT; Schema: taxonomia; Owner: -
--

ALTER TABLE ONLY taxonomia.taxon_caracteristica ALTER COLUMN sq_taxon_caracteristica SET DEFAULT nextval('taxonomia.taxon_caracteristica_sq_taxon_caracteristica_seq'::regclass);


--
-- TOC entry 12523 (class 2604 OID 33173)
-- Name: taxon_hist sq_taxon_hist; Type: DEFAULT; Schema: taxonomia; Owner: -
--

ALTER TABLE ONLY taxonomia.taxon_hist ALTER COLUMN sq_taxon_hist SET DEFAULT nextval('taxonomia.taxon_hist_sq_taxon_hist_seq'::regclass);


--
-- TOC entry 12526 (class 2604 OID 33174)
-- Name: taxon_instituicao sq_taxon_instituicao; Type: DEFAULT; Schema: taxonomia; Owner: -
--

ALTER TABLE ONLY taxonomia.taxon_instituicao ALTER COLUMN sq_taxon_instituicao SET DEFAULT nextval('taxonomia.taxon_instituicao_sq_taxon_instituicao_seq'::regclass);


--
-- TOC entry 12527 (class 2604 OID 33175)
-- Name: tipo_publicacao sq_tipo_publicacao; Type: DEFAULT; Schema: taxonomia; Owner: -
--

ALTER TABLE ONLY taxonomia.tipo_publicacao ALTER COLUMN sq_tipo_publicacao SET DEFAULT nextval('taxonomia.tipo_publicacao_sq_tipo_publicacao_seq'::regclass);


--
-- TOC entry 12528 (class 2604 OID 33176)
-- Name: tipo_uso sq_tipo_uso; Type: DEFAULT; Schema: taxonomia; Owner: -
--

ALTER TABLE ONLY taxonomia.tipo_uso ALTER COLUMN sq_tipo_uso SET DEFAULT nextval('taxonomia.tipo_uso_sq_tipo_uso_seq'::regclass);


-- Completed on 2023-02-05 13:20:08 -03

--
-- PostgreSQL database dump complete
--

