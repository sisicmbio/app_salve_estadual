#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "db_salve_estadual" <<-EOSQL
	CREATE EXTENSION if not exists postgis;
	CREATE EXTENSION if not exists pg_trgm;
EOSQL


